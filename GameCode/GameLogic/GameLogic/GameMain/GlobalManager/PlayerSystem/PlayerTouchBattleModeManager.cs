﻿using ACTSystem;
using Common;
using Common.Events;
using Common.Utils;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using Mogo.AI;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// 玩家触屏战斗模式管理
    /// </summary>
    public class PlayerTouchBattleModeManager
    {
        private static PlayerTouchBattleModeManager _instance = null;
        public static PlayerTouchBattleModeManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new PlayerTouchBattleModeManager();
            }
            return _instance;
        }

        private int _touchModeAIInCity = 0;
        private int _touchModeAIInCombat = 0;
        private List<string> _cacheList;
        private bool _handleDrivingTouchBattleModeCache = false;
        private uint _visualFxUID = 0;

        private bool _inTouchModeState;
        public bool inTouchModeState
        {
            get
            {
                return _inTouchModeState;
            }
            private set
            {
                if (_inTouchModeState != value)
                {
                    _inTouchModeState = value;
                    OnInTouchModeStateChanged();
                }
            }
        }

        private Vector3 _touchPosition = Vector3.zero;
        public Vector3 touchPosition
        {
            get
            {
                return _touchPosition;
            }
        }

        private PlayerTouchBattleModeManager()
        {
            _cacheList = new List<string>();
            _cacheList.Add("0");
            _inTouchModeState = false;
        }

        public void Enter()
        {
            inTouchModeState = true;
        }

        public void Exit()
        {
            DeleteTouchPosition();
            inTouchModeState = false;
        }

        public void OnPlayerEnterWorld()
        {
            inTouchModeState = ReadCache();
            EventDispatcher.AddEventListener<Vector3>(TouchEvents.ON_TOUCH_TERRAIN, OnTouchTerrain);
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_MANUAL_LOCK_ENTITY, OnManualLockEntity);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.AddEventListener<bool>(CGEvent.ON_VIEWING_MODE_CHANGED, OnViewingModeChanged);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnFightTypeChanged);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDrivingVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorcingVehicle);
            EventDispatcher.AddEventListener(PlayerEvents.ON_DEATH, OnDeath);
        }

        public void OnPlayerLeaveWorld()
        {
            EventDispatcher.RemoveEventListener<Vector3>(TouchEvents.ON_TOUCH_TERRAIN, OnTouchTerrain);
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_MANUAL_LOCK_ENTITY, OnManualLockEntity);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.RemoveEventListener<bool>(CGEvent.ON_VIEWING_MODE_CHANGED, OnViewingModeChanged);
            EventDispatcher.RemoveEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnFightTypeChanged);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDrivingVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorcingVehicle);
            EventDispatcher.RemoveEventListener(PlayerEvents.ON_DEATH, OnDeath);
        }

        public void OnPlayerEnterScene()
        {
            if (_handleDrivingTouchBattleModeCache)
            {
                _handleDrivingTouchBattleModeCache = false;
                Enter();
            }
        }

        public void OnPlayerLeaveScene()
        {
            if (!inTouchModeState)
            {
                return;
            }
            ChangeAI(0);
            DeleteTouchPosition();
            DeleteCurrentVisualFx();
        }

        public void DeleteTouchPosition()
        {
            UpdateTouchPosition(Vector3.zero);
        }

        private void OnTouchTerrain(Vector3 point)
        {
            if (!CurrentCanTouch())
            {
                return;
            }
            if (!CanTouchTerrain())
            {
                return;
            }
            DeleteCurrentVisualFx();
            bool bModifyLayer = PlayerAvatar.Player.actor.modifyLayer;
            if (GameSceneManager.GetInstance().CheckCurrPointIsCanMove(point.x, point.z, bModifyLayer))
            {
                string prefabPath = "Assets/Resources/Fx/Scene/touch_screem/fx_scene_touch_qianwang.prefab";
                _visualFxUID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(prefabPath, point);
            }
            else
            {
                string prefabPath = "Assets/Resources/Fx/Scene/touch_screem/fx_scene_touch_bukeqianwang.prefab";
                _visualFxUID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(prefabPath, point);
                point = GameSceneManager.GetInstance().CheckPointAroundCanMove(point.x, point.z, bModifyLayer);
            }
            UpdateTouchPosition(point);
            if (PlayerAvatar.Player.moveManager.accordingMode == AccordingMode.AccordingLine ||
                PlayerAvatar.Player.moveManager.accordingMode == AccordingMode.AccordingPath)
            {
                PlayerAvatar.Player.moveManager.Stop();
            }
            if (PlayerAvatar.Player.entityAI != null)
            {
                PlayerAvatar.Player.entityAI.blackBoard.inFightingState = false;
            }
        }

        public bool CurrentCanTouch()
        {
            return platform_helper.InPCPlatform() || inTouchModeState;
        }

        private void OnManualLockEntity(uint id)
        {
            if (!CurrentCanTouch())
            {
                return;
            }
            if (id > 0)
            {
                DeleteTouchPosition();
                var entity = MogoWorld.GetEntity(id);
                Vector3 position = entity.position;
                if (entity is EntityCreature && (entity as EntityCreature).actor != null)
                {
                    position = (entity as EntityCreature).actor.position;
                }
                DeleteCurrentVisualFx();
                string prefabPath = "Assets/Resources/Fx/Scene/touch_screem/fx_scene_touch_gongji.prefab";
                _visualFxUID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(prefabPath, position);
            }
        }

        private void OnEnterMap(int mapId)
        {
            if (!inTouchModeState)
            {
                return;
            }
            if (!InAutoFight())
            {
                ChangeAI(GetCurrentSceneAIID());
            }
        }

        private void OnViewingModeChanged(bool visible)
        {
            if (!inTouchModeState)
            {
                return;
            }
            if (!visible)
            {
                DeleteTouchPosition();
            }
        }

        private void OnFightTypeChanged(FightType fightType)
        {
            if (!inTouchModeState)
            {
                return;
            }
            if (fightType == FightType.NONE)
            {
                ChangeAI(GetCurrentSceneAIID());
            }
            else
            {
                DeleteTouchPosition();
            }
        }

        private void OnDrivingVehicle(ControlVehicle value)
        {
            if (!inTouchModeState)
            {
                return;
            }
            if (!_handleDrivingTouchBattleModeCache)
            {
                _handleDrivingTouchBattleModeCache = true;
                Exit();
            }
        }

        private void OnDivorcingVehicle(ControlVehicle value)
        {
            if (_handleDrivingTouchBattleModeCache)
            {
                _handleDrivingTouchBattleModeCache = false;
                Enter();
            }
        }

        private void OnDeath()
        {
            if (!inTouchModeState)
            {
                return;
            }
            DeleteTouchPosition();
        }

        private void OnInTouchModeStateChanged()
        {
            if (CanWriteCache())
            {
                WriteCache();
            }
            EventDispatcher.TriggerEvent<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, inTouchModeState);
            if (inTouchModeState)
            {
                if (!InAutoFight())
                {
                    ChangeAI(GetCurrentSceneAIID());
                }
                ChangeMovingMode(AccordingMode.AccordingTouch);
            }
            else
            {
                if (!InAutoFight())
                {
                    ChangeAI(0);
                }
                if (!platform_helper.InPCPlatform())
                {
                    ChangeMovingMode(AccordingMode.AccordingStick);
                }
            }
        }

        private void ChangeMovingMode(AccordingMode accordingMode)
        {
            PlayerAvatar.Player.moveManager.defaultAccordingMode = accordingMode;
            PlayerAvatar.Player.moveManager.accordingMode = accordingMode;
        }

        private void DeleteCurrentVisualFx()
        {
            if (_visualFxUID > 0)
            {
                ACTVisualFXManager.GetInstance().StopVisualFX(_visualFxUID);
                _visualFxUID = 0;
            }
        }

        private bool CanWriteCache()
        {
            if (PlayerAvatar.Player.vehicleManager.isDrivering)
            {
                return false;
            }
            return true;
        }

        private void UpdateTouchPosition(Vector3 newPosition)
        {
            _touchPosition = newPosition;
        }

        private bool CanTouchTerrain()
        {
            if (DramaManager.GetInstance().IsInViewingMode())
            {
                return false;
            }
            return true;
        }

        private void ChangeAI(int aiId)
        {
            PlayerAvatar.Player.aiID = aiId;
            PlayerAvatar.Player.ChangeAI();
        }

        private int GetCurrentSceneAIID()
        {
            if (_touchModeAIInCombat == 0)
            {
                InitAIParams();
            }
            return InCity() ? 0 : _touchModeAIInCombat;
        }

        private void InitAIParams()
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.press_mode_ai).Split(',');
            _touchModeAIInCity = int.Parse(args[0]);
            _touchModeAIInCombat = int.Parse(args[1]);
        }

        private bool InCity()
        {
            return GameSceneManager.GetInstance().IsInCity();
        }

        private bool InAutoFight()
        {
            return PlayerAutoFightManager.GetInstance().fightType != FightType.NONE;
        }

        private bool ReadCache()
        {
            List<string> cache = LocalCache.Read<List<string>>(LocalName.TouchBattleMode);
            if (cache == null)
            {
                return false;
            }
            return Convert.ToBoolean(int.Parse(cache[0]));
        }

        private void WriteCache()
        {
            int inTouchModeStateInt = _inTouchModeState ? 1 : 0;
            _cacheList[0] = inTouchModeStateInt.ToString();
            LocalCache.Write(LocalName.TouchBattleMode, _cacheList, CacheLevel.Permanent);
        }
    }
}
