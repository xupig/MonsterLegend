using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;


namespace GameMain.GlobalManager
{
    public class PlayerAttrManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerAttrManager
    {

        private static PlayerAttrManager s_instance = null;
        public static PlayerAttrManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerAttrManager();
            }
            return s_instance;
        }

        //private uint _syncPosTimerID;
        private int _syncPosSpaceFrame = 3; //属性同步间隔时间（单位：帧）。
        private float _syncPosNextTime = 0; 

        private uint _recoverAttrTimerID;
        private int _recoverAttrSpaceTime = 100; //属性恢复间隔时间（单位：毫秒）。
        private Vector3 _prePos = new Vector3();
        private byte _lastLevel = 0;

        private PlayerAttrManager() 
        {
            
        }

        public void OnPlayerEnterWorld()
        {
            _lastLevel = PlayerAvatar.Player.level;
            MogoWorld.RegisterUpdate<PlayerAttrManagerUpdateDelegate>("PlayerAttrManager.Update", Update);
            _recoverAttrTimerID = TimerHeap.AddTimer(1000, _recoverAttrSpaceTime, RecoverAttr);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, LevelChange);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
            EventDispatcher.AddEventListener(PlayerEvents.ON_RELIVE, OnPlayerRelive);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PlayerAttrManager.Update", Update);
            TimerHeap.DelTimer(_recoverAttrTimerID);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, LevelChange);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
            EventDispatcher.RemoveEventListener(PlayerEvents.ON_RELIVE, OnPlayerRelive);
        }

        public void OnSyncOneBa(UInt32 entityId, byte attrId, Int32 value)
        {
            EntityCreature creature = MogoWorld.GetEntity(entityId) as EntityCreature;
            if (creature != null)
            {
                creature.OnSyncOneBa(attrId, value);
            }
        }

        public void OnSyncMultiBa(UInt32 entityId, byte[] byteArr)
        {
            EntityCreature creature = MogoWorld.GetEntity(entityId) as EntityCreature;
            if (creature != null)
            {
                creature.OnSyncMultiBa(byteArr);
            }
        }

        public void Update()
        {
            _syncPosNextTime--;
            if (_syncPosNextTime <= 0)
            {
                _syncPosNextTime = _syncPosSpaceFrame;
                SyncPos();
            }
        }

        public void SyncPos()
        {
            if (GameSceneManager.GetInstance().IsInClientMap())
            {
                return;
            }
            PlayerAvatar player = MogoWorld.Player as PlayerAvatar;
            if (player == null || player.actor == null)
            {
                return;
            }
            if (player.elevatorState)
            {
                return;
            }
            Vector3 position = player.actor.position;
            if (Mathf.Abs(position.x - _prePos.x) < 0.2f && Mathf.Abs(position.z - _prePos.z) < 0.2f)
            {
                return;
            }
            _prePos.x = position.x;
            _prePos.z = position.z;
            System.Int16 x = (System.Int16)(position.x * RPCConfig.POSITION_SCALE);
            System.Int16 z = (System.Int16)(position.z * RPCConfig.POSITION_SCALE);
            byte face = (byte)(player.actor.localEulerAngles.y * 0.5);
            MogoEngine.RPC.ServerProxy.Instance.Move(face, (System.UInt16)x, (System.UInt16)z);
            //var time = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            //var ms = PlayerTimerManager.GetInstance().GetNowServerMillisecond();
            //Debug.LogError(player.id + "发送同步坐标:" + time + "::" + ms + "::" + x + "," + z);
        }

        public void RecoverAttr()
        {
            var player = PlayerAvatar.Player;
            var rate = _recoverAttrSpaceTime * 0.001f;
            if (player.cur_ep < player.max_ep)
            {
                var epRecoverSpeed = player.GetAttribute(fight_attri_config.EP_RECOVER_SPEED) * rate;
                var epRecoverRate = player.GetAttribute(fight_attri_config.EP_RECOVER_RATE) * rate;
                var epRecoverReduceRate = player.GetAttribute(fight_attri_config.EP_RECOVER_REDUCE_RATE);
                Int16 newCurEp = (Int16)Mathf.Min(player.max_ep, player.cur_ep + (epRecoverRate * player.max_ep + epRecoverSpeed) * (1 - epRecoverReduceRate));
                MogoWorld.SynEntityAttr(player, EntityPropertyDefine.cur_ep, newCurEp);
            }

            if (!player.skillManager.inServerMode)
            {
                if (player.cur_hp < player.max_hp)
                {
                    var hpRecoverSpeed = player.GetAttribute(fight_attri_config.HP_RECOVER_SPEED) * rate;
                    var hpRecoverRate = player.GetAttribute(fight_attri_config.HP_RECOVER_RATE) * rate;
                    var newCurHp = (int)Mathf.Min(player.max_hp, player.cur_hp + hpRecoverRate * player.max_hp + hpRecoverSpeed);
                    MogoWorld.SynEntityAttr(player, EntityPropertyDefine.cur_hp, newCurHp);

                }
            }
        }

        public void LevelChange()
        {
            var player = PlayerAvatar.Player;
            if (_lastLevel == player.level) return;
            EventDispatcher.TriggerEvent<byte, byte>(RoleEvent.PLAYER_LEVEL_UP, player.level, _lastLevel);
            _lastLevel = player.level;
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                DockingService.RoleLevelChangeLog();
            }
            if (player.actor == null)return;
            ACTSystem.ACTVisualFXManager.GetInstance().PlayACTVisualFX(InstanceDefine.LEVEL_UP_VISUAL_FX_ID, player.actor, -1, player.qualitySettingValue);
        }

        public void OnPlayerEnterMap(int mapID)
        {
            _syncPosSpaceFrame = map_helper.GetSynPosSpaceTime(mapID);
            var player = PlayerAvatar.Player;
            if (player.actor == null) return;
            ACTSystem.ACTVisualFXManager.GetInstance().PlayACTVisualFX(InstanceDefine.BORN_VISUAL_FX_ID, player.actor, -1, player.qualitySettingValue);
            player.actor.ToGround();
        }

        public void OnPlayerRelive()
        {
            var player = PlayerAvatar.Player;
            if (player.actor == null) return;
            ACTSystem.ACTVisualFXManager.GetInstance().PlayACTVisualFX(InstanceDefine.BORN_VISUAL_FX_ID, player.actor, -1, player.qualitySettingValue);
        }
    }
}