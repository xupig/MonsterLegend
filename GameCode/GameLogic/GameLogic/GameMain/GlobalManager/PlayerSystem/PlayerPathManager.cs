
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class PointRelation
    {
        public float lineDistance = 0;
        public List<float> path = null;
    }
    
    public class CachePathData
    {
        private Dictionary<int, Dictionary<int, PointRelation>> _pointDistanceMap;
        private Dictionary<int, MovePathData> _pointIdMap;
        private List<MovePathData> _movePathData;
        public CachePathData(List<MovePathData> movePathData)
        {
            _movePathData = movePathData;
            Init();
        }

        private void Init()
        {
            InitPointIdMap();
            InitPointDistanceMap();
        }

        private void InitPointIdMap()
        {
            _pointIdMap = new Dictionary<int, MovePathData>();
            for (int i = 0; i < _movePathData.Count; i++)
            {
                var data = _movePathData[i];
                if (!_pointIdMap.ContainsKey(data.id))
                {
                    _pointIdMap[data.id] = data;
                }
                else
                {
                    LoggerHelper.Error("路点存在重复ID!");
                }
            }
        }

        private void InitPointDistanceMap()
        {
            _pointDistanceMap = new Dictionary<int, Dictionary<int, PointRelation>>();
            for (int i = 0; i < _movePathData.Count; i++)
            {
                var data = _movePathData[i];
                var tempList = data.relationId.Split(',');
                for (var j = 0; j < tempList.Length; j++)
                {
                    var otherId = int.Parse(tempList[j]);
                    var dis = Vector3.Distance(_pointIdMap[otherId].pathPos, data.pathPos);
                    AddPointDistanceMap(data, _pointIdMap[otherId]);
                }
            }
        }

        private void AddPointDistanceMap(MovePathData point1, MovePathData point2)
        {
            if (GetOnePointDistanceMap(point1.id).ContainsKey(point2.id)) return;
            var dis = Vector3.Distance(point2.pathPos, point1.pathPos);
            var relation = new PointRelation();
            relation.lineDistance = dis;
            GetOnePointDistanceMap(point1.id)[point2.id] = relation;
            relation = new PointRelation();
            relation.lineDistance = dis;
            GetOnePointDistanceMap(point2.id)[point1.id] = relation;
        }

        private Dictionary<int, PointRelation> GetOnePointDistanceMap(int id)
        {
            if (!_pointDistanceMap.ContainsKey(id))
            {
                _pointDistanceMap[id] = new Dictionary<int, PointRelation>();
            }
            return _pointDistanceMap[id];
        }
    }

    public class PlayerPathManager
    {
        private Dictionary<string, CachePathData> _cachePathMap; 
        private static PlayerPathManager s_instance = null;
        public static PlayerPathManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerPathManager();
            }
            return s_instance;
        }

        private PlayerPathManager() 
        {
            _cachePathMap = new Dictionary<string, CachePathData>(); 
        }

        private CachePathData GetCurCachePathData()
        {
            CachePathData result = null;
            var curSpaceName = GameSceneManager.GetInstance().curSpaceName;
            if (!_cachePathMap.ContainsKey(curSpaceName))
            {
                var scene = GameSceneManager.GetInstance().scene;
                var posInfo = (scene as GameScene).GetFindPathInfo();
                if (posInfo != null && posInfo.Count > 0)
                {
                    result = new CachePathData(posInfo);
                }
                _cachePathMap[curSpaceName] = result;
            }
            return _cachePathMap[curSpaceName];
        }

        public List<float> GetPath(Vector3 srcPosition, Vector3 tarPosition)
        {
            List<float> result = null;
            bool canLine = PathUtils.CanLine(srcPosition, tarPosition, PlayerAvatar.Player.actor.modifyLayer);
            CachePathData cachePathData = GetCurCachePathData();
            //分段寻路
            if (canLine)
            {
                result = GenerateAStarPath(srcPosition, tarPosition);
            }
            else if (cachePathData != null)
            {
                result = GenerateRoadPointPath(srcPosition, tarPosition);
            }
            else
            {
                result = GenerateAStarPath(srcPosition, tarPosition);
            }
            return result;
        }

        private List<float> GenerateAStarPath(Vector3 srcPosition, Vector3 tarPosition)
        {
            List<float> result = null;
            result = GameSceneManager.GetInstance().GetPath(srcPosition.x, srcPosition.z, tarPosition.x, tarPosition.z, PlayerAvatar.Player.actor.modifyLayer);
            if (result.Count == 0)
            {
                LoggerHelper.Error("寻路失败，找不到到达该点的路径:" + " srcPosition:" + srcPosition + " tarPosition:" + tarPosition);
            }
            result = PathUtils.FixPath(result);
            return result;
        }

        private List<float> GenerateRoadPointPath(Vector3 srcPosition, Vector3 tarPosition)
        {
            
            return null;
        }
    }
}