
using Common.ClientConfig;
using Common.Events;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;

namespace GameMain.GlobalManager
{
    public enum FightType
    {
        NONE = 0,
        AUTO_FIGHT,
        FOLLOW_FIGHT
    }

    public class PlayerAutoFightManager
    {
        public bool isPause = false;
        private FightType _fightType = FightType.NONE;
        public FightType fightType
        {
            get { return _fightType; }
            private set
            {
                if (_fightType != value)
                {
                    _fightType = value;
                    EventDispatcher.TriggerEvent<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, _fightType);
                }
            }
        }


        private static PlayerAutoFightManager s_instance = null;
        public static PlayerAutoFightManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerAutoFightManager();
            }
            return s_instance;
        }

        private PlayerAutoFightManager() 
        {

        }

        public bool Start(FightType fightType = FightType.AUTO_FIGHT)
        {
            if (_fightType == fightType) return false;
            if (_fightType != FightType.NONE)
            {
                Stop();
            }
            if (fightType != FightType.NONE && GetAiId(fightType, GameSceneManager.GetInstance().curInstanceID) <= 0)
            {
                LoggerHelper.Info(string.Format("Start自动战斗失败，aiid = 0, 不允许做{0}的自动（跟随）战斗。", fightType));
                return false;
            }
            if(fightType == FightType.FOLLOW_FIGHT)
            {
                PlayerAvatar.Player.canAutoTask = false;
            }
            this.fightType = fightType;
            StartAI();
            return true;
        }

        public void AutoFight(int aIid, FightType fightType = FightType.AUTO_FIGHT)
        {
            this.fightType = fightType;
            if (aIid > 0)
            {
                var player = GameMain.Entities.PlayerAvatar.Player as EntityAvatar;
                player.aiID = aIid;
                player.ChangeAI();
            }
            else
            {
                StartAI();
            }
        }

        public void Stop()
        {
            if (_fightType == FightType.NONE) return;
			StopAI();
			this.fightType = FightType.NONE;
        }

        private void StartAI()
        {
            var player = GameMain.Entities.PlayerAvatar.Player as EntityAvatar;
            player.aiID = GetAiId(_fightType, GameSceneManager.GetInstance().curInstanceID);
            player.ChangeAI();
            if (player.aiID <= 0)
            {
                LoggerHelper.Error("@思宇 當前場景：" + GameSceneManager.GetInstance().curMapID + " 不存在掛機AI！");
            }
        }

        private int GetAiId(FightType fightType, int instanceID)
        {
            if (fightType == FightType.AUTO_FIGHT)
            {
                return GameSceneManager.GetInstance().GetAutoFightAiId();
            }
            else if (fightType == FightType.FOLLOW_FIGHT)
            {
                InstanceTeamType teamType = (InstanceTeamType)instance_helper.GetTeamType(instanceID);
                if (teamType == InstanceTeamType.Team)
                {
                    return GameSceneManager.GetInstance().GetAutoFightAiId();
                }
                else
                {
                    return GameSceneManager.GetInstance().GetFollowFightAiId();
                }
            }
            else
            {
                return -1;
            }
        }

        private void StopAI()
        {
            var player = GameMain.Entities.PlayerAvatar.Player as EntityAvatar;
            player.moveManager.Stop();
            player.aiID = -1;
            player.ChangeAI();
        }

        public void Pause()
        {
            if (_fightType != FightType.NONE) 
            {
                StopAI();
            }
        }

        public void Resume()
        {
            if (_fightType != FightType.NONE)
            {
                StartAI();
            }
        }

        public bool CanAutoFight()
        {
            return GameSceneManager.GetInstance().GetAutoFightAiId() > 0;
        }

        public bool CanFollowFight()
        {
            return GameSceneManager.GetInstance().GetFollowFightAiId() > 0;
        }

        public void OnLoadSceneStart()
        {
            Pause();
        }

        public void OnLoadSceneFinish()
        {
            Resume();
        }

        public void OnLeaveFollow()
        {
        }

        public void OnEnterFollow()
        {
            this.fightType = FightType.FOLLOW_FIGHT;
        }
    }
}