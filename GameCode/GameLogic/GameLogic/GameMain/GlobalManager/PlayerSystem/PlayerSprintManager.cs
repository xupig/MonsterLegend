using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameMain.PlayerAction;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;
using MogoEngine.Mgrs;
using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    //��Ҽ��ܹ���
    public class PlayerSprintManager
    {
        private int ENTER_SPRINT_BUFF = 4100; //����bufferID

        #region PlayAvatar ����
        private static PlayerSprintManager s_Instance;
        public static PlayerSprintManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new PlayerSprintManager();
                }
                return s_Instance;
            }
        }
        #endregion

        private int _playerLastCurHP;
        private PlayerAvatar _owner;
        private bool _sprintState = false;
        private float _moveBeginTime = 0;
        private float _enterSprintTime = 3f;
        public bool sprintState
        {
            get { return _sprintState; }
        }

        private PlayerSprintManager() 
        {
            _enterSprintTime = float.Parse(global_params_helper.GetGlobalParam(79)) * 0.001f;
        }

        public void OnPlayerEnterWorld(PlayerAvatar player)
        {
            _owner = player;
            _playerLastCurHP = _owner.cur_hp;
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.cur_hp, this.OnPlayerCurHPChange);
        }

        public void OnPlayerLeaveWorld(PlayerAvatar player)
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.cur_hp, this.OnPlayerCurHPChange);
            _owner = null;
        }

        private void OnPlayerCurHPChange()
        {
            if (_owner.cur_hp < _playerLastCurHP)
            {
                StopSprint();
            }
            _playerLastCurHP = _owner.cur_hp;
        }

        public void StartSprint()
        {
            if (_sprintState) return;
            _sprintState = true;
            AddSprintBuff();
            PlayerSoundManager.Instance.OnStartSprint();
        }

        public void StopSprint()
        {
            if (!_sprintState) return;
            _sprintState = false;
            RemoveSprintBuff();
            PlayerSoundManager.Instance.OnStopSprint();
        }

        public void TryStartSprint()
        {
            if (PlayerAvatar.Player.actor.isRiding)
            {
                return;
            }
            if (sprintState)
            {
                _moveBeginTime = 0;
                return;
            }
            if (_moveBeginTime == 0)
            {
                _moveBeginTime = Time.realtimeSinceStartup;
            }
            else if ((Time.realtimeSinceStartup - _moveBeginTime) > _enterSprintTime)
            {
                StartSprint();
                _moveBeginTime = 0;
            }
        }

        public void TryStopSprint()
        {
            _moveBeginTime = 0;
            if (!sprintState) return;
            StopSprint();
        }

        private void AddSprintBuff()
        {
            if (GameSceneManager.GetInstance().IsInClientMap())
            {
                _owner.bufferManager.AddBuffer(ENTER_SPRINT_BUFF, 0f, true);
            }
            else
            {
                _owner.RpcCall("buff_action_req", 1, ENTER_SPRINT_BUFF);
            }
        }

        private void RemoveSprintBuff()
        {
            if (GameSceneManager.GetInstance().IsInClientMap())
            {
                _owner.bufferManager.RemoveBuffer(ENTER_SPRINT_BUFF);
            }
            else
            {
                _owner.RpcCall("buff_action_req", 0, ENTER_SPRINT_BUFF);
            }
        }
    }
}