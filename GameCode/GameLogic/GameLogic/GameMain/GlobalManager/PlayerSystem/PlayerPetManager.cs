using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameMain.PlayerAction;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;
using Common.Data;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.Utils;

namespace GameMain.GlobalManager
{
    public class PlayerPetManager
    {
        private static PlayerPetManager s_instance = null;
        public static PlayerPetManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerPetManager();
            }
            return s_instance;
        }

        private uint _syncPosTimerID;
        private uint _curPetEntityID;
        public uint curPetEntityID
        {
            get { return _curPetEntityID; }
        }

        private PlayerPetManager() 
        {
        }

        public void ClientCopyAddPet(PetInfo petInfo)
        {
            if (_curPetEntityID != 0)
            {
                Debug.LogError("_curPetEntityID != 0");
                return;
            }
            MogoWorld.CreateEntity(EntityConfig.ENTITY_TYPE_NAME_PET, (entity) =>
            {
                var petEntity = entity as EntityPet;
                petEntity.pet_id = (byte)petInfo.Id;
                petEntity.level = (byte)petInfo.Level;
                petEntity.quality = (byte)petInfo.Quality;
                petEntity.star = (byte)petInfo.Star;
                petEntity.avatar_id = PlayerAvatar.Player.id;
                petEntity.isClientPet = true;
                petEntity.Teleport(PlayerAvatar.Player.position);
            });
        }

        public void ClientCopyDelPet()
        {
            if (_curPetEntityID == 0)
            {
                Debug.LogError("_curPetEntityID == 0");
                return;
            }
            MogoWorld.DestroyEntity(_curPetEntityID);
            _curPetEntityID = 0;
        }

        public void OnPetEnterWorld(EntityPet pet)
        {
            if (_curPetEntityID != 0)
            {
                LoggerHelper.Info("Warning: _curPetID is not 0:" + _curPetEntityID);
            }
            _curPetEntityID = pet.id;
            RemoveSyncPosTimer();
            AddSyncPosTimer();
            AddListener(pet);
        }

        private void AddSyncPosTimer()
        {
            _syncPosTimerID = TimerHeap.AddTimer(1000, 300, SyncPos);
        }

        private void RemoveSyncPosTimer()
        {
            if (_syncPosTimerID != 0) 
            {
                TimerHeap.DelTimer(_syncPosTimerID); 
            }
        }

        public void OnPetLeaveWorld(EntityPet pet)
        {
            if (_curPetEntityID == pet.id)
            {
                _curPetEntityID = 0;
                RemoveSyncPosTimer();
                RemoveListener(pet);
            }
            else
            {
                RemoveListener(pet);
            }
        }

        private void AddListener(EntityPet pet)
        {
            EntityPropertyManager.Instance.AddEventListener(pet, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.AddEventListener(pet, EntityPropertyDefine.max_hp, RefreshMaxHp);
        }

        private void RemoveListener(EntityPet pet)
        {
            EntityPropertyManager.Instance.RemoveEventListener(pet, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.RemoveEventListener(pet, EntityPropertyDefine.max_hp, RefreshMaxHp);
        }

        private void RefreshHp()
        {
            Entity pet = MogoWorld.GetEntity(_curPetEntityID);
            if (pet == null || !(pet is EntityPet))
            {
                return;
            }
            EventDispatcher.TriggerEvent<int>(BattleUIEvents.BATTLE_PET_HP_CHANGE, (pet as EntityPet).cur_hp);
        }

        private void RefreshMaxHp()
        {
            Entity pet = MogoWorld.GetEntity(_curPetEntityID);
            if (pet == null || !(pet is EntityPet))
            {
                return;
            }
            EventDispatcher.TriggerEvent<int>(BattleUIEvents.BATTLE_PET_MAX_HP_CHANGE, (pet as EntityPet).max_hp);
        }

        private Vector3 _prePos = new Vector3();
        public void SyncPos()
        {
            if (GameSceneManager.GetInstance().IsInClientMap()) return;
            if (_curPetEntityID == 0) return;
            var pet = MogoWorld.GetEntity(_curPetEntityID) as EntityPet;
            if (pet == null || pet.actor == null)
            {
                return;
            }
            Vector3 position = pet.actor.position;
            if (Mathf.Abs(position.x - _prePos.x) < 3f && Mathf.Abs(position.z - _prePos.z) < 3f)
            {
                return;
            }
            _prePos.x = position.x;
            _prePos.z = position.z;
            System.Int16 x = (System.Int16)(position.x * RPCConfig.POSITION_SCALE);
            System.Int16 z = (System.Int16)(position.z * RPCConfig.POSITION_SCALE);
            MogoEngine.RPC.ServerProxy.Instance.OthersMove(_curPetEntityID, (System.UInt16)x, (System.UInt16)z);
        }

        public void OnPlayerEnterScene()
        {
        }
    }
}