using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using Common.Events;
using Common.States;
using GameMain.PlayerAction;
using GameData;

using Mogo.Util;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
namespace GameMain.GlobalManager
{
    public class PlayerCommandManager
    {
        private static PlayerCommandManager s_instance = null;
        public static PlayerCommandManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerCommandManager();
            }
            return s_instance;
        }

        private PlayerCommandManager() 
        {
            EventDispatcher.AddEventListener<int, bool>(PlayerCommandEvents.FIND_NPC, FindNPC);
            EventDispatcher.AddEventListener<string[], int,bool>(PlayerCommandEvents.FIND_NPC_AND_ENTER_FB, FindNpcAndEnterFb);
            EventDispatcher.AddEventListener<int, int, bool>(TaskEvent.FIND_MONSTER, FindMonster);
            EventDispatcher.AddEventListener<int, bool>(PlayerCommandEvents.FIND_TREASURE, FindTreasure);
            EventDispatcher.AddEventListener<Vector3>(PlayerCommandEvents.FIND_POSITION, FindPosition);
            EventDispatcher.AddEventListener<Vector3>(PlayerCommandEvents.FIND_TRANSFER_POINT, FindTransferPoint);
            EventDispatcher.AddEventListener<SharePostionData>(PlayerCommandEvents.FIND_SHARE_POSITION, FindSharePosition);
            EventDispatcher.AddEventListener(CameraEvents.HIDE_MAIN_CAMERA, OnHideMainCamera);
            EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, OnLeaveMap);
        }

        public void OnPlayerEnterWorld()
        {
            //MogoWorld.RegisterUpdate(Update);
        }

        public void OnPlayerLeaveWorld()
        {
            //MogoWorld.UnregisterUpdate(Update);
        }

        void OnLeaveMap(int mapID)
        {
            PlayerActionManager.GetInstance().StopAllActionExcept(ActionDefine.MOVE_BY_MULTI_MAP);
        }

        public void OnHideMainCamera()
        {
            if (PlayerAvatar.Player != null && GameSceneManager.GetInstance().IsInCity())
			{
				PlayerActionManager.GetInstance().StopCurrentAction();  
            }
        }

        float _nextClearTime = 0;
        private void TryMoveByStick()
        {
            if (_nextClearTime < Time.realtimeSinceStartup)
            {
                PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = Time.realtimeSinceStartup + 0.0f;
            }
        }

        #region 各个玩家操作命令接口

        MoveByStick _singleMoveByStick;
        public void MoveByStick()
        {
            if (_singleMoveByStick == null) 
            {
                _singleMoveByStick = new MoveByStick(MogoWorld.Player as PlayerAvatar, 0); 
            }
            if (PlayerActionManager.GetInstance().currentAction == _singleMoveByStick) return;
            var queue = PlayerActionManager.CreateActionQueue();
            queue.Add(_singleMoveByStick);
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        MoveByTouchPoint _singleMoveByTouchPoint;
        public void MoveByTouchPoint()
        {
            if (_singleMoveByTouchPoint == null)
            {
                _singleMoveByTouchPoint = new MoveByTouchPoint(MogoWorld.Player as PlayerAvatar, 0);
            }
            if (PlayerActionManager.GetInstance().currentAction == _singleMoveByTouchPoint) return;
            var queue = PlayerActionManager.CreateActionQueue();
            queue.Add(_singleMoveByTouchPoint);
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        public void PlaySkill(int skillPos)
        {
            if (PlayerAvatar.Player.banBeControlled) return;

            if (!GameSceneManager.GetInstance().inCombatScene) return;

            if (!platform_helper.InPCPlatform() && PlayerTouchBattleModeManager.GetInstance().inTouchModeState == false)
            {
                if (PlayerActionManager.GetInstance().GetCurrentAction() is PlaySkill) return;
            }
            else
            {
                if (!(PlayerActionManager.GetInstance().GetCurrentAction() is PlaySkill))
                {
                    int skillID = PlayerAvatar.Player.skillManager.GetSkillIDByPos(skillPos);
                    if (PlayerAvatar.Player.skillManager.CanDo(skillID) > 0)
                    {
                        return;
                    }
                }
            }
            
            var queue = PlayerActionManager.CreateActionQueue();
            queue.Add(new PlaySkill(MogoWorld.Player as PlayerAvatar, skillPos, 1000));
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private bool CheckIsOnFollowFight(Action callback)
        {
            /*
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
          //ari MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(Common.Global.LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                    TeamManager.Instance.CancelFollowCaptain();
                    callback();
                });
                return true;
            }*/
            return false;
        }


        private void FindTransferPoint(Vector3 targetPosition)
        {
            if (CheckIsOnFollowFight(() => { FindTransferPoint(targetPosition); }) == true) return;
            var queue = PlayerActionManager.CreateActionQueue();
            queue.Add(new FindTransferPoint(MogoWorld.Player as PlayerAvatar, targetPosition, 0));
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindPosition(Vector3 targetPosition)
        {
            if (CheckIsOnFollowFight(() => { FindPosition(targetPosition); }) == true) return;
            var queue = PlayerActionManager.CreateActionQueue();
            queue.Add(new FindPosition(MogoWorld.Player as PlayerAvatar, targetPosition, 0));
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindNpcAndEnterFb(string[] dataList,int taskId, bool multiNeedConfirm = true)
        {
            
            int instId = 0;
            if(dataList.Length>4)
            {
                instId = int.Parse(dataList[4]);
            }
            if (CheckIsOnFollowFight(() => { FindNpcAndEnterFb(dataList, taskId, multiNeedConfirm); }) == true) return;
            //UnityEngine.Debug.Log("Npc Id : " + npcID );
            var curInsID = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            int insID = int.Parse(dataList[0]);
            if (insID != curInsID && multiNeedConfirm)
            {
                /*ari ChangeScenePanel.Show(insID, () =>
                {
                    FindNpcAndEnterFb(dataList, taskId, false);
                });*/
                return;
            }
            Vector3 pos = new Vector3();
            pos.x = int.Parse(dataList[1]);
            pos.y = int.Parse(dataList[2]);
            pos.z = int.Parse(dataList[3]);

            var dis = Vector3.Distance(PlayerAvatar.Player.position, pos);

            if (dis < 0.5f)
            {
                TaskManager.Instance.FinishClientTask(10002, taskId);
                if (instId != 0)
                {
                    MissionManager.Instance.RequsetEnterMission(instId);
                }
                return;
            }

            var queue = PlayerActionManager.CreateActionQueue();
            if (insID != curInsID)
            {
                queue.Add(new MoveByMultiMap(MogoWorld.Player as PlayerAvatar, insID, -1, 0));
            }
            queue.Add(new FindNpcAndEnterFb(MogoWorld.Player as PlayerAvatar, pos, 0,instId,taskId));

            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindNPC(int npcID, bool multiNeedConfirm = true)
        {
            if (CheckIsOnFollowFight(() => { FindNPC(npcID, multiNeedConfirm); }) == true) return;
            //UnityEngine.Debug.Log("Npc Id : " + npcID );
            var mapID = npc_helper.GetNpcMap(npcID);
            var insID = map_helper.GetInstanceIDByMapID(mapID);
            var curInsID = map_helper.GetInstanceIDByMapID( GameSceneManager.GetInstance().curMapID);
            if (insID != curInsID && multiNeedConfirm)
            {
                /*
                ChangeScenePanel.Show(insID, () =>
                {
                    FindNPC(npcID, false);
                });*/
                return;
            }
            var queue = PlayerActionManager.CreateActionQueue();
            if (insID != curInsID)
            {
                queue.Add(new MoveByMultiMap(MogoWorld.Player as PlayerAvatar, insID, -1, 0));
            }  
            queue.Add(new FindNPC(MogoWorld.Player as PlayerAvatar, npcID, 0));

            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindMonster(int insID, int monsterID, bool multiNeedConfirm = true)
        {
            if (CheckIsOnFollowFight(() => { FindMonster(insID, monsterID, multiNeedConfirm); }) == true) return;
            var curInsID = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            if (insID != curInsID && multiNeedConfirm)
            {/*
                ChangeScenePanel.Show(insID, () =>
                {
                    FindMonster(insID, monsterID, false);
                });*/
                return;
            }
            var queue = PlayerActionManager.CreateActionQueue();
            if (insID != curInsID)
            {
                queue.Add(new MoveByMultiMap(MogoWorld.Player as PlayerAvatar, insID, -1, 0));
            }
            var targetPosition = wild_helper.GetMonsterPosition(insID, monsterID);
            queue.Add(new FindMonster(MogoWorld.Player as PlayerAvatar, targetPosition, 0, monsterID));
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindTreasure(int treasureMapId, bool multiNeedConfirm = true)
        {
            if (CheckIsOnFollowFight(() => { FindTreasure(treasureMapId, multiNeedConfirm); }) == true) return;
            KeyValuePair<int, float> kvp = item_helper.GetUseEffectKvp(treasureMapId);
            int insID = (int)kvp.Value;
            var curInsID = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            if (insID != curInsID && multiNeedConfirm)
            {
                /*
                ChangeScenePanel.Show(insID, () =>
                {
                    FindTreasure(treasureMapId, false);
                });*/
                return;
            }
            var queue = PlayerActionManager.CreateActionQueue();
            if (insID != curInsID)
            {
                queue.Add(new MoveByMultiMap(MogoWorld.Player as PlayerAvatar, insID, -1, 0));
            }
            var targetPosition = wild_helper.GetTreasurePosition(treasureMapId);
            queue.Add(new FindTreasure(MogoWorld.Player as PlayerAvatar, targetPosition, treasureMapId, 0));
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        private void FindSharePosition(SharePostionData sharePosData)
        {
            if (CheckIsOnFollowFight(() => { FindSharePosition(sharePosData); }) == true) return;
            var curInsID = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            int insID = map_helper.GetInstanceIDByMapID(sharePosData.mapId);
            if ((insID != curInsID) && sharePosData.multiNeedConfirm)
            {
                /*
                ChangeScenePanel.Show(insID, () =>
                {
                    sharePosData.multiNeedConfirm = false;
                    FindSharePosition(sharePosData);
                });*/
                return;
            }
            var queue = PlayerActionManager.CreateActionQueue();
            if (insID != curInsID || sharePosData.mapLine != PlayerAvatar.Player.curLine)
            {
                queue.Add(new MoveByMultiMap(MogoWorld.Player as PlayerAvatar, insID, sharePosData.mapLine, 0));
            }
            if (sharePosData.type == 2) //2- boss 1-宝箱
            {
                queue.Add(new FindMonster(MogoWorld.Player as PlayerAvatar, sharePosData.targetPosition, 0, 0));
            }
            else
            {
                queue.Add(new FindPosition(MogoWorld.Player as PlayerAvatar, sharePosData.targetPosition, 0));
            }
            
            PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        public void Follow(string name)
        {
            return;
            //var queue = PlayerActionManager.CreateActionQueue();
            //uint entityID = AvatarManager.GetInstance().GetEntityIDByName(name);
            //if (MogoWorld.GetEntity(entityID) == null) 
            //{
            //    MogoUtils.FloatTips(0);
            //    return; 
            //}
            //queue.Add(new Follow(MogoWorld.Player as PlayerAvatar, entityID, 0));
            //PlayerActionManager.GetInstance().AddActionQueue(queue);
        }

        #endregion

        #region 玩家当前操作状态判断

        public bool InFindNPC()
        {
            return PlayerActionManager.GetInstance().HasActionByDefine(ActionDefine.FIND_NPC);
        }

        #endregion
    }
}