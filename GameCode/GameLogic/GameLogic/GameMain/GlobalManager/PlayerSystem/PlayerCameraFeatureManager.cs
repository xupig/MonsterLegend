﻿
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using System;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class PlayerCameraFeatureManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum CameraFeatureStatus : byte
    {
        NORMAL = 0,
        ROTATING,
        ROTATE_COMPLETE,
        RECOVERING
    }

    public class CameraFeatureParams
    {
        private int _paramsID = 0;

        public float rotationX = 0;
        public float rotationDuration = 0;
        public float distance = 0;
        public float distanceDuration = 0;
        public float rotationOffset = 0;
        public string slotTransformName = string.Empty;
        public float recoverDuration = 0;
        public float featureDuration = 0;
        public CameraFeatureParams(int paramsID)
        {
            _paramsID = paramsID;
            string paramsString = XMLManager.global_params[paramsID].__value;
            string[] paramsStringArray = paramsString.Split(',');
            if (paramsStringArray.Length < 4)
            {
                LoggerHelper.Error(string.Format("@方晓之，配置全局参数{0}错误，参数少于4个。", paramsID));
                return;
            }
            rotationX = float.Parse(paramsStringArray[0]);
            rotationDuration = float.Parse(paramsStringArray[1]);
            distance = float.Parse(paramsStringArray[2]);
            distanceDuration = float.Parse(paramsStringArray[3]);
            rotationOffset = float.Parse(paramsStringArray[4]);
            slotTransformName = paramsStringArray[5];
            recoverDuration = float.Parse(paramsStringArray[6]);
            featureDuration = Mathf.Max(rotationDuration, distanceDuration);
        }
    }

    public class PlayerCameraFeatureManager : Singleton<PlayerCameraFeatureManager>
    {
        private CameraFeatureStatus _status = CameraFeatureStatus.NORMAL;
        private CameraFeatureParams _normalParams;
        private CameraFeatureParams _ridingParams;
        private CameraFeatureParams _currentParams;
        private float _originRotationX = 0;
        private float _originRotationY = 0;
        private float _originRotationZ = 0;
        private float _originDistance = 0;
        private float _featureTimeStamp = 0;
        private float _recoverTimeStamp = 0;
        private const int GLOBAL_PARAMS_ID = 143;
        private const int RIDING_GLOBAL_PARAMS_ID = 175;

        public void OnPlayerEnterWorld()
        {
            InitParams();
            EventDispatcher.AddEventListener(TouchEvents.ON_TOUCH_SCREEN, OnTouchScreen);
            EventDispatcher.AddEventListener<uint>(TouchEvents.ON_DOUBLE_TOUCH_ENTITY, OnTouchEntity);
            MogoWorld.RegisterUpdate<PlayerCameraFeatureManagerUpdateDelegate>("PlayerCameraFeatureManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            EventDispatcher.RemoveEventListener(TouchEvents.ON_TOUCH_SCREEN, OnTouchScreen);
            EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_DOUBLE_TOUCH_ENTITY, OnTouchEntity);
            MogoWorld.UnregisterUpdate("PlayerCameraFeatureManager.Update", Update);
        }

        private void InitParams()
        {
            try
            {
                _normalParams = new CameraFeatureParams(GLOBAL_PARAMS_ID);
                _ridingParams = new CameraFeatureParams(RIDING_GLOBAL_PARAMS_ID);
            }
            catch (Exception e)
            {
                LoggerHelper.Error(string.Format("@方晓之，解析主角特写全局参数报错，报错信息: {0}", e.Message));
            }
        }

        private void OnTouchEntity(uint id)
        {
            if (id == PlayerAvatar.Player.id)
            {
                OnCameraFeature();
            }
        }

        private void OnCameraFeature()
        {
            if (!CanRotate())
            {
                return;
            }
            SetRidingState();
            SaveCameraAngle();
			HideUI();
            RotatePlayer();
            RotateCamera();
            _status = CameraFeatureStatus.ROTATING;
        }

        private bool CanRotate()
        {
            if (_status != CameraFeatureStatus.NORMAL)
            {
                return false;
            }
            if (GameSceneManager.GetInstance().curMapType != public_config.MAP_TYPE_CITY)
            {
                return false;
            }
            if (PlayerAvatar.Player.actor.isRiding)
            {
                return false;
            }
            if (CameraManager.GetInstance().Camera == null || !CameraManager.GetInstance().isShow)
            {
                return false;
            }
            if (DramaManager.GetInstance().IsInViewingMode())
            {
                return false;
            }
            if (CameraManager.GetInstance().CurrentIsScaled())
            {
                return false;
            }
            return true;
        }

        private void SetRidingState()
        {
            _currentParams = PlayerAvatar.Player.actor.isRiding ? _ridingParams : _normalParams;
        }

        private void SaveCameraAngle()
        {
            _originRotationX = CameraManager.GetInstance().mainCamera.rotationX;
            _originRotationY = CameraManager.GetInstance().mainCamera.rotationY;
            _originRotationZ = CameraManager.GetInstance().mainCamera.rotationZ;
            _originDistance = CameraManager.GetInstance().mainCamera.distance;
        }

        private void HideUI()
        {
            UIManager.Instance.HideAllLayer();
        }

        private void RotatePlayer()
        {
            Vector3 destAngle = GetRotatePlayerEulerAngles();
            PlayerAvatar.Player.actor.TurnTo(destAngle);
        }

        private Vector3 GetRotatePlayerEulerAngles()
        {
            Vector3 cameraAngle = CameraManager.GetInstance().CameraTransform.localEulerAngles;
            Vector3 playerAngle = PlayerAvatar.Player.actor.localEulerAngles;
            float faceYAngle = (cameraAngle.y >= 180) ? cameraAngle.y - 180 : cameraAngle.y + 180;
            float minYAngle = faceYAngle - _currentParams.rotationOffset;
            float maxYAngle = faceYAngle + _currentParams.rotationOffset;
            float destYAngle = playerAngle.y;
            if (!InRange(minYAngle, maxYAngle, destYAngle))
            {
                destYAngle = GetBoundYAngle(cameraAngle.y, minYAngle, maxYAngle, destYAngle);
            }
            Vector3 destAngle = new Vector3(playerAngle.x, destYAngle, playerAngle.z);
            return destAngle;
        }

        private bool InRange(float minYAngle, float maxYAngle, float destYAngle)
        {
            if (minYAngle < 0 && destYAngle >= minYAngle + 360)
            {
                return true;
            }
            if (maxYAngle >= 360 && destYAngle <= maxYAngle - 360)
            {
                return true;
            }
            return destYAngle >= minYAngle && destYAngle <= maxYAngle;
        }

        private float GetBoundYAngle(float cameraAngle, float minYAngle, float maxYAngle, float destYAngle)
        {
            if (minYAngle >= 0 && maxYAngle <= 360)
            {
                float angle = (360 - _currentParams.rotationOffset * 2) / 2;
                if (360 - maxYAngle > angle)
                {
                    if (destYAngle <= minYAngle && destYAngle >= 0)
                    {
                        return minYAngle;
                    }
                }
                else if (360 - maxYAngle < angle)
                {
                    if (destYAngle >= maxYAngle && destYAngle < 360)
                    {
                        return maxYAngle;
                    }
                }
            }
            
            if (destYAngle <= cameraAngle)
            {
                return maxYAngle;
            }
            return minYAngle;

        }

        private void RotateCamera()
        {
            Transform target = GetTransform(PlayerAvatar.Player, _currentParams.slotTransformName);
            Vector3 localEulerAngles = GetRotateCameraEulerAngles();
			CameraManager.GetInstance().ChangeToRotationTargetMotion(target, localEulerAngles, _currentParams.rotationDuration, 0, _currentParams.distance, _currentParams.distanceDuration);
            _featureTimeStamp = Time.realtimeSinceStartup;
        }

        private Transform GetTransform(EntityCreature creature, string slotName = "")
        {
            if (creature != null && creature.actor != null)
            {
                Transform slotTrans = creature.actor.boneController.GetBoneByName(slotName);
                if (slotTrans != null)
                {
                    return slotTrans;
                }
                return creature.actor.transform;
            }
            return null;
        }

        private Vector3 GetRotateCameraEulerAngles()
        {
            Vector3 localEulerAngles = Vector3.zero;
            localEulerAngles.x = _currentParams.rotationX;
            float destY = PlayerAvatar.Player.actor.localEulerAngles.y;
            destY += 180;
            if (destY >= 360)
            {
                destY -= 360;
            }
            float startY = CameraManager.GetInstance().CameraTransform.localEulerAngles.y;
            if (Mathf.Abs(destY - startY) > 180)
            {
                if (startY > destY)
                {
                    destY += 360;
                }
                else
                {
                    destY -= 360;
                }
            }
            localEulerAngles.y = destY;
            return localEulerAngles;
        }

        private void OnTouchScreen()
        {
            if (!CanRecover())
            {
                return;
            }
			ShowUI();
            RecoverCamera();
            _status = CameraFeatureStatus.RECOVERING;
        }

        private bool CanRecover()
        {
            return _status == CameraFeatureStatus.ROTATE_COMPLETE;
        }

        private void ShowUI()
        {
            UIManager.Instance.ShowAllLayer();
        }

        private void RecoverCamera()
        {
            float startY = CameraManager.GetInstance().CameraTransform.localEulerAngles.y;
            if (Mathf.Abs(_originRotationY - startY) > 180)
            {
                if (startY > _originRotationY)
                {
                    _originRotationY += 360;
                }
                else
                {
                    _originRotationY -= 360;
                }
            }
            CameraManager.GetInstance().ChangeToRotationTargetMotion(PlayerAvatar.Player.actor.transform, new Vector3(_originRotationX, _originRotationY, _originRotationZ), _currentParams.recoverDuration, 0, _originDistance, _currentParams.recoverDuration);
            _recoverTimeStamp = Time.realtimeSinceStartup;
        }

        private void Update()
        {
            float now = Time.realtimeSinceStartup;
            if (_status == CameraFeatureStatus.ROTATING && _featureTimeStamp > 0 && now - _featureTimeStamp >= _currentParams.featureDuration)
            {
                _status = CameraFeatureStatus.ROTATE_COMPLETE;
                _featureTimeStamp = 0;
            }
            if (_status == CameraFeatureStatus.RECOVERING && _recoverTimeStamp > 0 && now - _recoverTimeStamp >= _currentParams.recoverDuration)
            {
                _status = CameraFeatureStatus.NORMAL;
                _recoverTimeStamp = 0;
            }
        }
    }
}
