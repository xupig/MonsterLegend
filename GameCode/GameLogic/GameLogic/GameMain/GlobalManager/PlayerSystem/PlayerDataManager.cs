﻿#region 模块信息
/*==========================================
// 模块名：PlayerDataManager
// 命名空间: GameMain.GlobalManager
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：保存玩家身上数据  1.需要与服务器同步类型（如BagData）   2.客户端数据（todo）
// 其他：目录:GameLogic.Common.Data.Modules.BagData
//==========================================*/
#endregion

using System;

using GameMain.Entities;


using Common.Data;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameMain.GlobalSystem;
using Common.Global;
using UnityEngine;
using Common.ServerConfig;
using Common;
using System.Collections.Generic;
using GameLoader.LoadingBar;

namespace GameMain.GlobalManager
{
    public class PlayerDataManager
    {
        #region PlayAvatar 单例
        private static PlayerDataManager s_Instance;
        public static PlayerDataManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new PlayerDataManager();
                }
                return s_Instance;
            }
        }
        #endregion
        public bool serverDataInited = false;

        public SettingData SettingData { get; set; }
        
        public AchievementData AchievementData { get; set; }
        public EquipEffectData EquipEffectData { get; set; }
        public BagDataManager BagData { get; set; }
        public BattlePetData BattlePetData { get; set; }
        public MallDataManager MallDataManager { get; set; }
        public TeamData TeamData { get; set; }
        public ChatData ChatData { get; set; }
        public FriendData FriendData { get; set; }
        public TaskData TaskData { get; set; }
        public WingData WingData { get; set; }
        public MailData MailData { get; set; }
        public FashionData FashionData { get; set; }
        public StarSoulData StarSoulData { get; set; }
        public EquipStrengthenData EquipStrengthenData { get; set; }
        public EnchantData EnchantData { get; set; }
        public SmelterData SmelterData { get; set; }
        public TokenData TokenData { get; set; }
        public WanderLandData WanderLandData { get; set; }
        public VipDataManager VipData { get; set; }
        public AuctionData AuctionData { get; set; }
        public MakeData MakeData { get; set; }
        public CopyData CopyData { get; set; }
        public PlayAgainData PlayAgainData { get; set; }
        public PetData PetData { get; set; }
        public TeamInstanceData TeamInstanceData { get; set; }
        //ari public RankingListManager RankingListManager { get; set; }
        //ari public EquipUpgradeTipManager EquipUpgradeTipManager { get; set; }
        public SingleInstanceData SingleInstanceData { get; set; }
        public InstanceAvatarData InstanceAvatarData { get; set; }
        public ActivityData ActivityData { get; set; }
        public GemData gemData { get; set; }
        public MountData mountData { get; set; }
        public NoticeData NoticeData { get; set; }
        public ChargeActivityData ChargeActivityData { get; set; }
        public SpellData SpellData { get; set; }
        public DreamlandData DreamlandData { get; set; }
        public OtherPlayerData OtherPlayerData { get; set; }
        public DemonGateData DemonGateData { get; set; }
        
        public RewardDataManager RewardData { get; set; }
        public OpenServerData OpenServerData { get; set; }
        public GuildData GuildData { get; set; }
        public WelfareActiveTabData WelfareActiveTabData { get; set; }
        public FunctionData FunctionData { get; set; }
        public EnergyBuyingData EnergyBuyingData { get; set; }
        public TownTipsData TownTipsData { get; set; }
        //ari public FightTipsManager FightTipsManager { get; set; }

        //ari public EquipPointManager EquipPointManager { get; set; }
        //ari public InteractivePointManager InteractivePointManager { get; set; }
        public RankInfo rankInfo { get; set; }
        public PlayerReturnData playerReturnData { get; set; }

        public PKData PKData { get; set; }

        public TimeAltarData TimeAltarData { get; set; }
        public RedEnvelopeData RedEnvelopeData { get; set; }
        
        public PlayerDataManager()
        {
            this.RegisterBeforeEnterWorld();
        }
        
        public PutOutData PutOutData { get; set; }
        public FavorData FavorData { get; set; }
        public TitleData TitleData { get; set; }

        public AlchemyData AlchemyData { get; set; }

        public GuildWarData GuildWarData { get; set; }
        public LuckyTurntableData LuckyTurntableData { get; set; }
        public GuildShopData GuildShopData { get; set; }
        public DuelData DuelData { get; set; }

        public PlayingCopyStarData PlayingCopyStarData { get; set; }
       
        public UInt64 GetTimeStamp()
        {
            return Global.serverTimeStamp;
        }

        private void RegisterBeforeEnterWorld()
        {
            LocalCacheHelper.InitCommonLocalCache();
            this.SettingData = new SettingData();
        }

        /**注册玩家身上的数据**/
        private void Register()
        {
            LocalCacheHelper.Init(PlayerAvatar.Player.dbid);
            PlayerAvatar.Player.target = this;
            PlayerAvatar.Player.timeStamp = GetType().GetMethod("GetTimeStamp");
            /*Ari
            FunctionData = new FunctionData();
            this.BagData = new BagDataManager();
            this.BattlePetData = new BattlePetData();
            this.MallDataManager = new MallDataManager();
            this.TeamData = new TeamData();
            this.ChatData = new ChatData();
            this.FriendData = new FriendData();
            this.TaskData = new TaskData();
            this.WingData = new WingData();
            this.MailData = new MailData();
            this.FashionData = new FashionData();
            this.TokenData = new TokenData();
            this.StarSoulData = new StarSoulData();
            this.EquipStrengthenData = new EquipStrengthenData();
            this.EnchantData = new EnchantData();
            this.SmelterData = new SmelterData();
            MakeData = new MakeData();
            this.WanderLandData = new WanderLandData();
            this.VipData = new VipDataManager();
            this.AuctionData = new AuctionData();
            this.PutOutData = new PutOutData();
            this.FavorData = new FavorData();
            this.CopyData = new CopyData();
            WelfareActiveTabData = new WelfareActiveTabData();
            ActivityData = new ActivityData();
            ChargeActivityData = new ChargeActivityData();
            OpenServerData = new OpenServerData();
            SpellData = new SpellData();
            PetData = new PetData();
            TeamInstanceData = new TeamInstanceData();
            OtherPlayerData = new OtherPlayerData();
            DemonGateData = new DemonGateData();
            DreamlandData = new DreamlandData();
            this.RewardData = new RewardDataManager();
            GuildData = new GuildData();
            EquipUpgradeTipManager = new EquipUpgradeTipManager();
            RankingListManager = new RankingListManager();
            SingleInstanceData = new SingleInstanceData();
            this.EnergyBuyingData = new EnergyBuyingData();
            InstanceAvatarData = new InstanceAvatarData();
            NoticeData = new NoticeData();
            TownTipsData = new TownTipsData();
            FightTipsManager = new FightTipsManager();
            EquipPointManager = new EquipPointManager();
            rankInfo = new RankInfo();
            gemData = new GemData();
            TimeAltarData = new TimeAltarData();
            TitleData = new TitleData();
            InteractivePointManager = new InteractivePointManager();
            PKData = new PKData();
            AchievementData = new AchievementData();
            EquipEffectData = new EquipEffectData();
            PlayAgainData = new PlayAgainData();
            AlchemyData = new AlchemyData();
            GuildWarData = new GuildWarData();
            LuckyTurntableData = new LuckyTurntableData();
            mountData = new MountData();
            GuildShopData = new GuildShopData();
            PlayingCopyStarData = new PlayingCopyStarData();
            RedEnvelopeData = new RedEnvelopeData();
            DuelData = new DuelData();
            playerReturnData = new PlayerReturnData();
             * */
        }


        public void RequestInitData()
        {
            Register();
            /*Ari
            AchievementManager.Instance.RequestGetAchievementRecord();
            BagManager.Instance.RequestGetAllData();
            MissionManager.Instance.RequsetGetMissionRecord();
            MissionManager.Instance.RequsetGetChapterRecord();
            MissionManager.Instance.RequestResetTimesInfo();
            GuildManager.Instance.GetOfflineNotice();
            WingManager.Instance.GetWingData();
            VipManager.Instance.RequestGetRewardsRecord();
            PetManager.Instance.RequestPetList();
            DemonGateManager.Instance.RequestWorldLevel();
            DemonGateManager.Instance.RequestDemonGateInfo();
            FunctionManager.Instance.GetFunctionOpenQuery();
            ChatManager.Instance.GetBlackList();
            MallManager.Instance.RequestAllLimitData();
            SpellManager.Instance.GetLearnedSpells();
            //SpellManager.Instance.GetSpellProficientList();
            SpellManager.Instance.GetSpellSlots();
            RankingListManager.RequestGetMyWorshipRecord();
            EnergyBuyingManager.Instance.RequestBuyTimes();
            DreamlandManager.Instance.RequestChapterList();
            EquipManager.Instance.RequestStrengthenList();
            EquipManager.Instance.RequestAllBodyEnchantInfo();
            WanderLandManager.Instance.RequestInfo();
            StarSoulManager.Instance.GetRecord();
            FavorManager.Instance.RequestAllNpcFavor();
            RuneManager.Instance.GetWishInfo();
            FightForceChannelManager.Instance.Init();
            ///好友相关
            FriendManager.Instance.GetReceivedList();
            FriendManager.Instance.GetBlessedList();
            FriendManager.Instance.GetApplicationList();
            //ChargeActivityManager.Instance.RequesetInfo(public_config.ACTIVITY_CHARGE_FIRST);
            
            AlchemyManager.Instance.RequestAlchemyAssistList();
            RoleManager.Instance.ReqGetFashionInfo();
            EquipEffectManager.Instance.RequestGetFashionInfo();
            //检测是否有充值记录
            ChargeManager.Instance.CheckChargeInfo();
            // 请求学习过的配方
            MakeManager.Instance.GetFormulaBag();
            MakeManager.Instance.RequestGetCDList();
            // 请求坐骑列表
            MountManager.Instance.MountInfoReq();
            TaskManager.Instance.GetTeamTaskOnLineData();
            SettingManager.Instance.SyncPushSettingInfo();
            OpenServerManager.Instance.RequestInfo();
            ChargeActivityManager.Instance.RequesetAllInfo();
            AntiAddictionManager.Instance.AddListener();
            // 这个放最后
            DramaManager.GetInstance().GetTriggerCountDict();
            TutorManager.Instance.Init();
            RedEnvelopeManager.Instance.RequestGetRedEnvelopeList();
            FieldTaskManager.Instance.QueryTaskInfo();
            GuildTaskManager.Instance.GetMyGuildScore();
             * */
        }

        public void RequestDataAfterSyncTime()
        {
            /*Ari
            RewardManager.Instance.RequestGetAllRewardInfo();
            GuildManager.Instance.GetMyGuildInformation();
            MailManager.Instance.RequestMailList(0);
            TradeMarketManager.Instance.RequestSellItems();
            TradeMarketManager.Instance.RequestLeftMoney();
            TradeMarketManager.Instance.RequestAuctionInfo();
            SystemChatMsgManager.Instance.StartShowMsg();
            RewardBuffManager.Instance.RequestRewardBuff();
            RewardData.UpdateOpenServerLoginRewardData();
             * */
            ///TitleManager.Instance.RequestTitleBag();
            //初始化本地推送信息
            if (Application.platform == RuntimePlatform.Android)
            {
                SettingData.SetupLoginPushNotification();
            }
#if UNITY_IPHONE && !UNITY_EDITOR
                IOSPlugins.SetUpNotification();
#endif
        }

        public void OnLastReqResped()
        {
            serverDataInited = true;
            ProgressBar.Instance.Close();
        }

        public void DestoryData()
        {

        }
    }
}
