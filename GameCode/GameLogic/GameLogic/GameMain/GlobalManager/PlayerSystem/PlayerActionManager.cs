using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using GameMain.PlayerAction;
using GameData;
namespace GameMain.GlobalManager
{
    public class PlayerActionManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerActionManager
    {
        private BaseAction _currentAction;
        public BaseAction currentAction
        {
            get { return _currentAction; }
        }
        private List<BaseAction> _waitingQueue = new List<BaseAction>();
        public List<BaseAction> waitingQueue
        {
            get { return _waitingQueue; }
        }
        private static float _lastActionTime = 0;

        private int _actionQueueMax = 5;
        private float _actionLifeTime = 1f;

        private static PlayerActionManager s_instance = null;
        public static PlayerActionManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerActionManager();
            }
            return s_instance;
        }

        public PlayerActionManager()
        {
            LoadGlobalSetting();
        }

        public void LoadGlobalSetting()
        {
            _actionQueueMax = int.Parse(global_params_helper.GetGlobalParam(85));
            _actionLifeTime = float.Parse(global_params_helper.GetGlobalParam(86)) * 0.001f;
        }

        public void OnPlayerEnterWorld()
        {
            MogoWorld.RegisterUpdate<PlayerActionManagerUpdateDelegate>("PlayerActionManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PlayerActionManager.Update", Update);
        }

        private static Queue<List<BaseAction>> m_emptyQueuePool = new Queue<List<BaseAction>>();
        public static List<BaseAction> CreateActionQueue()
        {
            if (m_emptyQueuePool.Count != 0)
            {
                return m_emptyQueuePool.Dequeue();
            }
            return new List<BaseAction>();
        }

        public static void ReturnActionQueue(List<BaseAction> queue)
        {
            queue.Clear();
            m_emptyQueuePool.Enqueue(queue);
        }

        void Update()
        {
            if ((MogoWorld.Player as PlayerAvatar) == null) { return; }
            if (_currentAction != null)
            {
                uint result = _currentAction.Does();
                _lastActionTime = Time.realtimeSinceStartup;
                if (result == PlayerActionState.ACTION_STATE_DOING)
                {
                    return;
                }
                else if (result == PlayerActionState.ACTION_STATE_FAIL)
                {
                    OnActionFail();
                }
                else if (result == PlayerActionState.ACTION_STATE_END)
                {
                    OnActionEnd();
                }
            }
            else
            {
                _currentAction = GetNextAction();
                if (_currentAction == null)
                {
                    OnIdle();
                }
                else
                {
                    OnActionStart();
                }
            }
            CheckWaitingQueue();
        }

        void CheckWaitingQueue()
        {
            while (_waitingQueue.Count > 0)
            {
                var action = _waitingQueue[0];
                if (!action.canWait) break;
                if (action.overtime > Time.realtimeSinceStartup) break;
                _waitingQueue.RemoveAt(0);
            }
        }

        public void AddActionQueue(List<BaseAction> queue)
        {
            if (this._currentAction != null)
            {
                var newPri = this.GetActionQueuePri(queue);
                if (this._currentAction.priority < newPri)
                {
                    this.StopCurrentAction();
                    this._currentAction = null;
                }
                else if (this._currentAction.priority == newPri)
                {
                    if (this._currentAction.canBreak)
                    {
                        this.StopCurrentAction();
                        this._currentAction = null;
                    }
                }
            }
            AddWaitingQueue(queue);
            Update();
        }

        public void AddWaitingQueue(List<BaseAction> queue)
        {
            if (!GetActionCanWait(this._waitingQueue) || !GetActionCanWait(queue))
            {
                this._waitingQueue.Clear();
            }
            for (int i = 0; i < queue.Count; i++)
            {
                var action = queue[i];
                if (action.canWait) action.overtime = Time.realtimeSinceStartup + _actionLifeTime;
                this._waitingQueue.Add(action);
            }
            if (GetActionCanWait(this._waitingQueue))
            {
                while (this._waitingQueue.Count > _actionQueueMax)
                {
                    this._waitingQueue.RemoveAt(0);
                }
            }
            ReturnActionQueue(queue);
        }

        public void CleanWaitingQueue()
        {
            this._waitingQueue.Clear();
        }

        public List<BaseAction> GetWaitingQueue()
        {
            return this._waitingQueue;
        }

        public BaseAction GetFirstWaitting()
        {
            if (_waitingQueue.Count > 0)
            {
                return _waitingQueue[0];
            }
            return null;
        }

        private BaseAction GetNextAction()
        {
            BaseAction result = null;
            int removeFromIdx = -1;
            for (int i = 0; i < _waitingQueue.Count; i++)
            {
                var action = _waitingQueue[i];
                if (!action.canWait) 
                {  
                    removeFromIdx = i;
                    result = action;
                    break;
                }
                if (!action.NeedWaiting())
                {
                    removeFromIdx = i;
                    result = action;
                    break;
                }
            }
            if (removeFromIdx >= 0)
            {
                for (int i = removeFromIdx; i >=0; i--)
                {
                    _waitingQueue.RemoveAt(i);
                }
            }
            return result;
        }

        private uint GetActionQueuePri(List<BaseAction> queue)
        {
            uint maxPri = ActionPriority.PRIORITY_UNKNOW;
            for (int i = 0; i < queue.Count; i++)
            {
                maxPri = Math.Max(queue[i].priority, maxPri);
            }
            return maxPri;
        }

        private bool GetActionCanWait(List<BaseAction> queue)
        {
            bool result = false;
            for (int i = 0; i < queue.Count; i++)
            {
                result |= queue[i].canWait;
            }
            return result;
        }

        public void StopCurrentAction()
        {
            if (_currentAction != null)
            {
                _currentAction.Stop();
            }
        }

        public void StopAllAction()
        {
            StopCurrentAction();
            CleanWaitingQueue();
        }

        public void StopAllActionExcept(ActionDefine actionType)
        {
            if (_currentAction != null && _currentAction.define != actionType)
            {
                StopAllAction();
            }
        }

        public void StopActionByFeature(string feature)
        {
            if (_currentAction != null && this._currentAction.IsFeature(feature))
            {
                _currentAction.Stop();
            }
        }

        public bool HasActionByDefine(ActionDefine define)
        {
            if (_currentAction != null && this._currentAction.define == define)
            {
                return true;
            }
            foreach (BaseAction action in this._waitingQueue)
            {
                if (action.define == define) 
                {
                    return true;
                }
            }
            return false;
        }

        public BaseAction GetCurrentAction()
        {
            return _currentAction;
        }

        private void OnActionStart()
        {
            _currentAction.Start();
        }

        private void OnActionFail()
        {
            _currentAction = null;
        }

        private void OnActionEnd()
        {
            _currentAction = null;
        }

        private void OnIdle()
        {

        }
        public static float GetlastActionTime()
        {
            return _lastActionTime;
        }
    }
}