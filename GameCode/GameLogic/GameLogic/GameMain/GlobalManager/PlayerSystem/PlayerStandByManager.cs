﻿
using Common.ServerConfig;
using Common.States;
using Common.Utils;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine;
using System.Collections.Generic;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class PlayerStandByManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    //玩家待机管理
    class PlayerStandByManager
    {
        #region PlayAvatar 单例
        private static PlayerStandByManager s_Instance;
        public static PlayerStandByManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new PlayerStandByManager();
                }
                return s_Instance;
            }
        }
        #endregion

        private bool _standByState = false;
        private bool _inStandByMap = false;
        private float _standByTimeStamp = 0;
        private float _standByDuration = 0;

        private float _minStandByDuration = 0;
        private float _maxStandByDuration = 0;
        private List<int> _standByActionList;

        public void OnPlayerEnterWorld()
        {
            InitArgs();
            MogoWorld.RegisterUpdate<PlayerStandByManagerUpdateDelegate>("PlayerStandBy", OnUpdate);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PlayerStandBy", OnUpdate);
        }

        public void OnPlayerEnterSpace()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            if (mapType == public_config.MAP_TYPE_CITY)
            {
                _inStandByMap = true;
            }
            else
            {
                _inStandByMap = false;
            }
        }

        public void OnPlayerLeaveSpace()
        {
            _inStandByMap = false;
            _standByTimeStamp = 0;
            _standByDuration = 0;
        }

        private void InitArgs()
        {
            string[] args = global_params_helper.GetGlobalParam(154).Split(',');
            _minStandByDuration = float.Parse(args[0]);
            _maxStandByDuration = float.Parse(args[1]);
            _standByActionList = new List<int>();
            for (int i = 2; i < args.Length; ++i)
            {
                _standByActionList.Add(int.Parse(args[i]));
            }
        }

        private void OnUpdate()
        {
            if (!_inStandByMap) return;
            PlayerAvatar player = PlayerAvatar.Player;
            if (player.actor == null) return;
            if (player.actor.isRiding) return;
            if (_standByState)
            {
				if (!CanPlayStandByAction())
				{
					StopStandByAction();
					return;
				}
            }
            if (!CanPlayStandByAction())
            {
                _standByTimeStamp = 0;
                _standByDuration = 0;
                return;
            }
            if (_standByTimeStamp == 0)
            {
                _standByTimeStamp = Time.realtimeSinceStartup;
                _standByDuration = GetRandomDuration();
                return;
            }
            float nowTime = Time.realtimeSinceStartup;
            if (nowTime - _standByTimeStamp >= _standByDuration)
            {
                PlayStandByAction();
            }
        }

        private bool CanPlayStandByAction()
        {
            return !IsMoving() &&
                !DramaManager.GetInstance().IsInViewingMode();
        }

        private bool IsMoving()
        {
            Vector3 speed = PlayerAvatar.Player.actor.actorController.horizontalSpeed;
            return speed.x != 0 || speed.z != 0;
        }

        private void PlayStandByAction()
        {
			_standByState = true;
            int actionId = GetRandomStandByAction();
            PlayerAvatar.Player.actor.animationController.PlayAction(actionId);
            _standByTimeStamp = Time.realtimeSinceStartup;
            _standByDuration = GetRandomDuration();
        }

        private void StopStandByAction()
        {
            _standByState = false;
            PlayerAvatar.Player.actor.animationController.ReturnReady();
			_standByTimeStamp = 0;
			_standByDuration = 0;
        }

        private float GetRandomDuration()
        {
            return MogoMathUtils.Random(_minStandByDuration, _maxStandByDuration);
        }

        private int GetRandomStandByAction()
        {
            return _standByActionList[MogoMathUtils.Random(0, _standByActionList.Count)];
        }
    }
}
