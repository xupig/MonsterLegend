﻿
using Common.Events;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
namespace GameMain.GlobalManager
{
    /// <summary>
    /// 任务击杀怪物管理器，处理开始寻找怪物，击杀怪物中等情况
    /// </summary>
    public class PlayerTaskFindMonsterManager
    {
        private static PlayerTaskFindMonsterManager s_instance = null;
        public static PlayerTaskFindMonsterManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerTaskFindMonsterManager();
            }
            return s_instance;
        }

        private int _targetMonsterID = 0;
        public int targetMonsterID
        {
            get
            {
                return _targetMonsterID;
            }
            private set
            {
                _targetMonsterID = value;
            }
        }

        public void OnPlayerEnterWorld()
        {
            EventDispatcher.AddEventListener<int>(PlayerActionEvents.ON_START_FIND_MONSTER, OnStartFindMonster);
            EventDispatcher.AddEventListener<int>(PlayerActionEvents.ON_END_FIND_MONSTER, OnEndFindMonster);
            EventDispatcher.AddEventListener<int>(PlayerActionEvents.ON_FAIL_FIND_MONSTER, OnFailFindMonster);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnFightTypeChanged);
        }

        public void OnPlayerLeaveWorld()
        {
            EventDispatcher.RemoveEventListener<int>(PlayerActionEvents.ON_START_FIND_MONSTER, OnStartFindMonster);
            EventDispatcher.RemoveEventListener<int>(PlayerActionEvents.ON_END_FIND_MONSTER, OnEndFindMonster);
            EventDispatcher.RemoveEventListener<int>(PlayerActionEvents.ON_FAIL_FIND_MONSTER, OnFailFindMonster);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnFightTypeChanged);
        }

        private void OnStartFindMonster(int monsterID)
        {
            targetMonsterID = monsterID;
        }

        private void OnEndFindMonster(int monsterID)
        {
        }

        private void OnFailFindMonster(int monsterID)
        {
            targetMonsterID = 0;
        }

        private void OnFightTypeChanged(FightType fightType)
        {
            if (fightType == FightType.AUTO_FIGHT)
            {
                return;
            }
            targetMonsterID = 0;
        }
    }
}
