using System;

using GameMain.Entities;
using MogoEngine.Events;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;

namespace GameMain.GlobalManager
{
    public class BuffEvents
    {
        public const string BUFF_REFRESH = "BuffEvents_REFRESH";
    }

    public class PlayerBuffManager
    {
        private static PlayerBuffManager s_instance = null;
        public static PlayerBuffManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerBuffManager();
            }
            return s_instance;
        }

        private PlayerBuffManager()
        {

        }

        public void BuffResp(UInt32 entityId, UInt32 buffId, byte addOrRemove, UInt32 lastTime)
        {
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityId);
            if (creature == null) return;
            if ((int)addOrRemove == 1)
            {
                if (entityId == PlayerAvatar.Player.id)
                {
                    ShowBuffTip((int)buffId);
                }
                creature.bufferManager.AddBuffer((int)buffId, (float)lastTime, false, false);
            }
            else
            {
                if (entityId == PlayerAvatar.Player.id)
                {
                    ShowBuffRemoveTip((int)buffId);
                }
                creature.bufferManager.RemoveBuffer((int)buffId);
            }
            EventDispatcher.TriggerEvent(BuffEvents.BUFF_REFRESH);
        }

        private void ShowBuffRemoveTip(int buffId)
        {
            if (XMLManager.buff[buffId].__end_notice != 0)
            {
                string content = string.Format(MogoLanguageUtil.GetContent(XMLManager.buff[buffId].__end_notice), buff_helper.GetName(buffId));
                UIManager.Instance.ShowPanel(PanelIdEnum.BuffTips, content);
            }
        }

        private const int SHOW_MESSAGE_BOX = 2;
        private const int SHOW_FLOAT_TIP = 3;
        private void ShowBuffTip(int buffId)
        {
            if (XMLManager.buff[buffId].__need_show == SHOW_MESSAGE_BOX)
            {
                string content1 = string.Format(MogoLanguageUtil.GetContent(71485), buff_helper.GetName(buffId));
                UIManager.Instance.ShowPanel(PanelIdEnum.BuffTips, content1);
            }
            if (XMLManager.buff[buffId].__need_show == SHOW_FLOAT_TIP)
            {
                string content2 = string.Format(MogoLanguageUtil.GetContent(71485), buff_helper.GetName(buffId));
                //ariToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content2);
            }
        }

        private void OpenBuffListTip()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BuffDetail);
        }

        public void BuffClientResp(UInt32 entityId, byte[] bytes)
        {
            PbBuffList buffList = MogoProtoUtils.ParseProto<PbBuffList>(bytes);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityId);
            if (creature == null) return;
            creature.bufferManager.ResetServerBuffList(buffList);
        }

        public void OnBuffHpChangeResp(uint buffId, int hp)
        {
            PlayerAvatar.Player.bufferManager.OnBuffHpChange(hp);
        }
    }
}