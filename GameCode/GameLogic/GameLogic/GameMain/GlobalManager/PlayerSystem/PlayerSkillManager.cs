using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.Asset;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class PlayerSkillData
    {
        public int skillID = 0;
        public float endTime = 0;
    }

    public class PlayerSkillManager
    {
        private static int PLAYER_SKILL_POS_MAX = 4;
        private static PlayerSkillManager s_instance = null;
        public static PlayerSkillManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerSkillManager();
            }
            return s_instance;
        }

        private Dictionary<int, PlayerSkillData> _posCDEndTimeMap = new Dictionary<int, PlayerSkillData>();

        private PlayerSkillManager()
        {

        }

        public void OnPlayerEnterWorld()
        {
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL1_ON_CLICK, OnPlaySkill1);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL2_ON_CLICK, OnPlaySkill2);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL3_ON_CLICK, OnPlaySkill3);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL4_ON_CLICK, OnPlaySkill4);
            EntityPropertyManager.Instance.AddEventListener(MogoWorld.Player, EntityPropertyDefine.cur_ep, OnEPChange);
            PlayerAvatar.Player.skillManager.onPosChange = OnPlayerSkillInfoChange;
        }

        public void OnPlayerLeaveWorld()
        {
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL1_ON_CLICK, OnPlaySkill1);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL2_ON_CLICK, OnPlaySkill2);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL3_ON_CLICK, OnPlaySkill3);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL4_ON_CLICK, OnPlaySkill4);
            EntityPropertyManager.Instance.RemoveEventListener(MogoWorld.Player, EntityPropertyDefine.cur_ep, OnEPChange);
            PlayerAvatar.Player.skillManager.onPosChange -= OnPlayerSkillInfoChange;
        }

        public void OnPlayerDriveVehicle(EntityCreature vehicle)
        {
            if (!(vehicle is EntityGroundVehicle))
            {
                return;
            }
            vehicle.skillManager.onPosChange = OnVehicleSkillInfoChange;
        }

        public void OnPlayerDivorceVehicle(EntityCreature vehicle)
        {
            if (!(vehicle is EntityGroundVehicle))
            {
                return;
            }
            vehicle.skillManager.onPosChange -= OnVehicleSkillInfoChange;
        }

        public void OnPlayerEnterMap(int mapID)
        {
            _posCDEndTimeMap.Clear();
            OnPlayerSkillInfoChange();
        }

        public void OnPlayerSkillInfoChange()
        {
            OnSkillInfoChange(PlayerAvatar.Player);
        }

        public void OnVehicleSkillInfoChange()
        {
            OnSkillInfoChange(PlayerAvatar.Player.vehicleManager.vehicle);
        }

        private void OnSkillInfoChange(EntityCreature creature)
        {
            var posSkillIDMap = creature.skillManager.GetCurPosSkillIDMap();
            for (int i = 1; i <= PLAYER_SKILL_POS_MAX; i++)
            {
                if (!_posCDEndTimeMap.ContainsKey(i))
                {
                    _posCDEndTimeMap[i] = new PlayerSkillData();
                }
                if (posSkillIDMap.ContainsKey(i))
                {
                    var curSkillID = posSkillIDMap[i];
                    var endTime = creature.skillManager.GetCDEndTime(curSkillID);
                    if (_posCDEndTimeMap[i].skillID != curSkillID)
                    {
                        _posCDEndTimeMap[i].skillID = curSkillID;
                        EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.SET_SKILL_ID, i, curSkillID);
                    }
                    if (_posCDEndTimeMap[i].endTime != endTime)
                    {
                        _posCDEndTimeMap[i].endTime = endTime;
                        if (endTime >= 0)
                        {
                            var cdTime = endTime - RealTime.time;
                            if (cdTime > 0)
                            {
                                EventDispatcher.TriggerEvent<int, float>(BattleUIEvents.SKILL_COOL_DOWN, i, cdTime * 1000);
                            }
                        }
                        else
                        {
                            //公共CD     
                            float cdTime = -endTime - RealTime.time;
                            int curCommonCdSkillId = creature.skillManager.GetCurCommonCdSkillId();
                            if (spell_helper.GetPos(curCommonCdSkillId) > 1)
                            {
                                EventDispatcher.TriggerEvent<int, float>(BattleUIEvents.SKILL_COOL_DOWN, i, -cdTime * 1000);
                            }
                        }
                    }
                }
            }
        }

        public void OnEPChange()
        {
            var posSkillIDMap = PlayerAvatar.Player.skillManager.GetCurPosSkillIDMap();
            for (int i = 1; i <= PLAYER_SKILL_POS_MAX; i++)
            {
                if (posSkillIDMap.ContainsKey(i))
                {
                    var curSkillID = posSkillIDMap[i];
                    var skillData = CombatLogicObjectPool.GetSkillData(curSkillID);
                    float epCost = 0;
                    float rate = 1;
                    if (skillData.useCosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
                    {
                        epCost = skillData.useCosts[public_config.ITEM_SPECIAL_SPELL_ENERGY];
                    }
                    if (epCost != 0)
                    {
                        rate = Mathf.Min(PlayerAvatar.Player.cur_ep / epCost, 1f);
                    }
                    EventDispatcher.TriggerEvent<int, float>(BattleUIEvents.SET_SKILL_EP, i, rate);
                }
            }
        }

        public void SetLearnedSkillList(List<int> learnedList)
        {
            PlayerAvatar.Player.skillManager.SetLearnedSkillList(learnedList);
            for (int i = 0; i < learnedList.Count; i++)
            {
                var path_list = ACTResource.GetSkillResourcePaths(learnedList[i]);
                ObjectPool.Instance.DontDestroy(path_list.ToArray());
            }
        }

        private void OnPlaySkill1()
        {
            PlayerCommandManager.GetInstance().PlaySkill(SkillData.SKILL_POS_ONE);
        }

        private void OnPlaySkill2()
        {
            PlayerCommandManager.GetInstance().PlaySkill(SkillData.SKILL_POS_TWO);
        }

        private void OnPlaySkill3()
        {
            PlayerCommandManager.GetInstance().PlaySkill(SkillData.SKILL_POS_THREE);
        }

        private void OnPlaySkill4()
        {
            PlayerCommandManager.GetInstance().PlaySkill(SkillData.SKILL_POS_FOUR);
        }

        #region  响应服务器释放技能操作

        public void SelfCastSpellResp(UInt16 errorCode, UInt16 skillID, UInt32 targetID)
        {
            if (errorCode > 0)
            {
                PlayerAvatar.Player.OnActionResp(0, errorCode, new LuaTable());
                if (errorCode == (ushort)error_code.ERR_CLIENT_TIME_TICK)
                {
                    LoggerHelper.Info(string.Format("RefreshLocalTimeStamp, localTimeStamp = {0}, serverTimeStamp = {1}",
                           Global.localTimeStamp, Global.serverTimeStamp));
                    RefreshLocalTimeStamp();
                }
            }
            else
            {
                var creature = PlayerAvatar.Player;
                if (creature.BeControlled())
                {
                    var pos = creature.position;
                    creature.skillManager.PlaySkillByServer(skillID, (short)(pos.x * 100), (short)(pos.z * 100), creature.face, targetID);
                }
                else
                {
                    creature.skillManager.OnSkillPlayByServer(skillID, targetID);
                }
            }
        }

        public void OtherCastSpellResp(UInt32 entityID, UInt16 skillID, Int16 x, Int16 z, byte face, UInt32 targetID)
        {
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityID);
            if (creature == null) return;
            if (IsFollower(creature))
            {
                if (creature is EntityPet)	//自己的宠物走Follower协议
                {
                    return;
                }
                //Debug.LogError("FollowerCastSpellResp " + skillID + " " + RealTime.time);
                creature.skillManager.OnFollowerSkillPlayByServer(skillID, x, z, face, targetID); //其它的跟随者走该协议并指向Follower处理
                return;
            }
            creature.skillManager.PlaySkillByServer(skillID, x, z, face, targetID); //不是跟随者走创建新技能流程
        }

        private bool IsFollower(EntityCreature creature)
        {
            return (creature is IEntityFollower) && (creature as IEntityFollower).IsFollower();
        }

        public void SpellActionResp(byte[] pbSpellActionByte)
        {
            PbSpellAction spellAction = MogoProtoUtils.ParseProto<PbSpellAction>(pbSpellActionByte);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(spellAction.caster_id);
            if (creature == null) return;
            creature.skillManager.ActiveSkillActionByServer(spellAction);
        }

        public void SpellDamageResp(UInt32 attackerID, UInt16 skillID, UInt16 skillActionID, byte[] skillDamageByte)
        {
            PbSpellDamageInfo skillDamageInfo = MogoProtoUtils.ParseProto<PbSpellDamageInfo>(skillDamageByte);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(attackerID);
            if (creature == null) return;
            creature.skillManager.JudgeSkillActionByServer(skillID, skillActionID, skillDamageInfo);
        }

        public void OtherSpellActionResp(byte[] pbOtherSpellAction)
        {
            //PbOtherSpellAction otherSpellAction = MogoProtoUtils.ParseProto<PbOtherSpellAction>(pbOtherSpellAction);
        }

        public void SpellActionOriginResp(byte[] pbSpellActionOrigin)
        {
            PbSpellActionOrigin spellActionOrigin = MogoProtoUtils.ParseProto<PbSpellActionOrigin>(pbSpellActionOrigin);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(spellActionOrigin.caster_id);
            if (creature == null) return;
            creature.skillManager.OnSetActionOriginByServer(spellActionOrigin);
        }

        public void FollowerCastSpellResp(UInt16 errorCode, UInt32 attackerID, UInt16 skillID, UInt32 targetID)
        {
            if (errorCode > 0)
            {
                PlayerAvatar.Player.OnActionResp(0, errorCode, new LuaTable());
                if (errorCode == (ushort)error_code.ERR_CLIENT_TIME_TICK)
                {
                    LoggerHelper.Info(string.Format("RefreshLocalTimeStamp, localTimeStamp = {0}, serverTimeStamp = {1}",
                        Global.localTimeStamp, Global.serverTimeStamp));
                    RefreshLocalTimeStamp();
                }
                return;
            }
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(attackerID);
            if (creature == null) return;
            var pos = creature.position;
            creature.skillManager.OnFollowerSkillPlayByServer(skillID, (short)(pos.x * 100), (short)(pos.z * 100), creature.face, targetID);
        }

        public void SpellCdChangeResp(byte[] data)
        {
            PbCdChangeSpells pbCdChangeSpells = MogoProtoUtils.ParseProto<PbCdChangeSpells>(data);
            var cdDataList = pbCdChangeSpells.cd_change_spells;
            for (int i = 0; i < cdDataList.Count; i++)
            {
                var unit = cdDataList[i];
                switch (unit.type)
                {
                    case 0://技能ID
                        PlayerAvatar.Player.skillManager.ChangeSkillGroupCD((int)unit.spell_id, unit.change_cd);
                        break;
                    case 1://组ID
                        PlayerAvatar.Player.skillManager.ChangeCD((int)unit.spell_id, unit.change_cd);
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        //刷新客户端时间戳
        private void RefreshLocalTimeStamp()
        {
            PlayerTimerManager.GetInstance().RefreshLocalTimeStamp();
        }

        //通知玩家技能特殊效果
        public void SpellSpecialEffectsResp(byte[] data)
        {
            PbSpellSpecialEffect pbSpellSpecialEffect = MogoProtoUtils.ParseProto<PbSpellSpecialEffect>(data);
            if (pbSpellSpecialEffect.caster_id != 0 && pbSpellSpecialEffect.caster_id != PlayerAvatar.Player.id)
            {
                return;
            }

            uint targetId = 0;
            uint damageValue = 0;
            EntityCreature creature;
            for (int i = 0; i < pbSpellSpecialEffect.ex_arg.Count; i += 2)
            {
                targetId = pbSpellSpecialEffect.ex_arg[i];
                damageValue = pbSpellSpecialEffect.ex_arg[i + 1];
                var entity = MogoWorld.GetEntity(targetId);
                if (entity != null && entity is EntityCreature)
                {
                    creature = entity as EntityCreature;
                    if (creature.actor != null)
                    {
                        PerformaceManager.GetInstance().CreateDamageNumber(creature.actor.position, (int)damageValue, AttackType.ATTACK_HIT, false);
                    }
                }
            }
        }
    }
}
