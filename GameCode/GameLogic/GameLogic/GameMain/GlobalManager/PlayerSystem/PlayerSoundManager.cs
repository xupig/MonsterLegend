﻿
using Common.ClientConfig;
using Common.Events;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class PlayerSoundManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerSoundDefine
    {
        public const int LEVEL_UP = 10000;
        public const int RELIVE = 10001;
        public const int NEAR_DEATH = 10002;
        public const int WALKING = 10003;
        public const int RIDING = 10009;

        public const int WALKING_COUNT = 6;
        public const int RIDING_COUNT = 5;
    }

    public class PlayerSoundManager
    {
        private static PlayerSoundManager s_Instance;
        public static PlayerSoundManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new PlayerSoundManager();
                }
                return s_Instance;
            }
        }

        private bool _loaded = false;
        public void OnActorLoaded()
        {
            _loaded = true;
            InitArgs();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            EventDispatcher.AddEventListener(PlayerEvents.ON_DEATH, OnDeath);
            EventDispatcher.AddEventListener(PlayerEvents.ON_RELIVE, OnRelive);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_SCENE, OnEnterScene);
            EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, OnLeaveScene);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, OnHPChange);
            EventDispatcher.AddEventListener(SpaceActionEvents.SpaceActionEvents_MissionEnd, OnMissionEnd);
            MogoWorld.RegisterUpdate<PlayerSoundManagerUpdateDelegate>("PlayerSoundManager", OnUpdate);
        }

        public void OnPlayerLeaveWorld()
        {
            if (!_loaded) return;
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            EventDispatcher.RemoveEventListener(PlayerEvents.ON_DEATH, OnDeath);
            EventDispatcher.RemoveEventListener(PlayerEvents.ON_RELIVE, OnRelive);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_SCENE, OnEnterScene);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.LEAVE_SCENE, OnLeaveScene);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, OnHPChange);
            EventDispatcher.RemoveEventListener(SpaceActionEvents.SpaceActionEvents_MissionEnd, OnMissionEnd);
            MogoWorld.UnregisterUpdate("PlayerSoundManager", OnUpdate);
        }

        public void OnStartSprint()
        {
            _sprintState = true;
        }

        public void OnStopSprint()
        {
            _sprintState = false;
        }

        private void OnUpdate()
        {
            CheckWalkingSound();
            WalkingSoundUpdate();
        }

        private float _walkingStartDuration = 0;
        private float _walkingInterval = 0;
        private float _walkingSprintInterval = 0;
        private bool _sprintState = false;
        private float _ridingStartDuration = 0;
        private float[] _ridingIntervalArray;
        private void InitArgs()
        {
            if (_walkingStartDuration == 0)
            {
                string[] args = global_params_helper.GetGlobalParam(GlobalParamId.walking_sound).Split(',');
                _walkingStartDuration = float.Parse(args[0]) * 0.001f;
                _walkingInterval = float.Parse(args[1]) * 0.001f;
                _walkingSprintInterval = float.Parse(args[2]) * 0.001f;
			}

            if (_ridingStartDuration == 0)
            {
                string[] args = global_params_helper.GetGlobalParam(GlobalParamId.riding_sound).Split(',');
                _ridingStartDuration = float.Parse(args[0]) * 0.001f;
                _ridingIntervalArray = new float[]{float.Parse(args[1]) * 0.001f, float.Parse(args[2]) * 0.001f, float.Parse(args[3]) * 0.001f};
            }
		}

        #region 升级音效
        private byte _level = 0;
        private void OnLevelChange()
        {
            byte level = PlayerAvatar.Player.level;
            if (level > _level)
            {
                PlayLevelUpSound();
            }
            _level = level;
        }

        private void PlayLevelUpSound()
        {
            SoundInfoManager.Instance.PlaySound(PlayerSoundDefine.LEVEL_UP);
        }
        #endregion

        #region 移动音效
        private bool _isWalking = false;
        private bool _isActiveMoving = false;
        private float _startWalkingSoundTimeStamp = 0;
        private float _nextWalkingSoundTimeStamp = 0;
        private bool _isRiding = false;
        private float _startRidingSoundTimeStamp = 0;
        private float _nextRidingSoundTimeStamp = 0;
        private int _ridingIntervalIndex = 0;
        private void CheckWalkingSound()
        {
            _isActiveMoving = PlayerAvatar.Player.isActiveMoving;
            if (_isActiveMoving && !_isWalking)
            {
                StartWalking();
            }
            else if (!_isActiveMoving && _isWalking)
            {
                EndWalking();
            }
        }

        private void StartWalking()
        {
            _isWalking = true;
            _isRiding = IsRiding();
            _startWalkingSoundTimeStamp = Time.realtimeSinceStartup + _walkingStartDuration;
            _startRidingSoundTimeStamp = Time.realtimeSinceStartup + _ridingStartDuration;
            _ridingIntervalIndex = 0;
        }

        private void EndWalking()
        {
            _isWalking = false;
            _startWalkingSoundTimeStamp = 0;
            _nextWalkingSoundTimeStamp = 0;
            _startRidingSoundTimeStamp = 0;
            _nextRidingSoundTimeStamp = 0;
            _ridingIntervalIndex = 0;
        }

        private void WalkingSoundUpdate()
        {
            if (!_isWalking)
            {
                return;
            }
            float now = Time.realtimeSinceStartup;
            bool isRiding = IsRiding();
            if (isRiding != _isRiding)
            {
                if (isRiding)
                {
                    _startWalkingSoundTimeStamp = 0;
                    _nextWalkingSoundTimeStamp = 0; 
                    _startRidingSoundTimeStamp = Time.realtimeSinceStartup + _ridingStartDuration;
                    _ridingIntervalIndex = 0;
                }
                else
                {
                    _startRidingSoundTimeStamp = 0;
                    _nextRidingSoundTimeStamp = 0;
                    _ridingIntervalIndex = 0;
                    _startWalkingSoundTimeStamp = Time.realtimeSinceStartup + _walkingStartDuration;
                }
                _isRiding = isRiding;
            }
            if (isRiding)
            {
                OnRidingSoundUpdate(now);
            }
            else
            {
                OnWalkingSoundUpdate(now);
            }
        }

        private bool IsRiding()
        {
            return PlayerAvatar.Player.actor.isRiding;
        }

        private void OnRidingSoundUpdate(float now)
        {
            if (_nextRidingSoundTimeStamp > 0 && now >= _nextRidingSoundTimeStamp)
            {
                ChangeRidingSound();
                return;
            }
            if (_startRidingSoundTimeStamp > 0 && now >= _startRidingSoundTimeStamp)
            {
                ChangeRidingSound();
                _startRidingSoundTimeStamp = 0;
            }
        }

        public void ChangeRidingSound()
        {
            int randomSoundID = PlayerSoundDefine.RIDING + MogoMathUtils.Random(0, PlayerSoundDefine.RIDING_COUNT);
            SoundInfoManager.Instance.PlaySound(randomSoundID);
            _nextRidingSoundTimeStamp = Time.realtimeSinceStartup + GetRidingInterval();
        }

        private float GetRidingInterval()
        {
            float result = _ridingIntervalArray[_ridingIntervalIndex++];
            if (_ridingIntervalIndex >= _ridingIntervalArray.Length)
            {
                _ridingIntervalIndex = 0;
            }
            return result;
        }

        private void OnWalkingSoundUpdate(float now)
        {
            if (_nextWalkingSoundTimeStamp > 0 && now >= _nextWalkingSoundTimeStamp)
            {
                ChangeWalkingSound();
                return;
            }
            if (_startWalkingSoundTimeStamp > 0 && now >= _startWalkingSoundTimeStamp)
            {
                ChangeWalkingSound();
                _startWalkingSoundTimeStamp = 0;
            }
        }

        private void ChangeWalkingSound()
        {
            int randomSoundID = PlayerSoundDefine.WALKING + MogoMathUtils.Random(0, PlayerSoundDefine.WALKING_COUNT);
            SoundInfoManager.Instance.PlaySound(randomSoundID);
            _nextWalkingSoundTimeStamp = Time.realtimeSinceStartup + GetWalkingInterval();
        }

        private float GetWalkingInterval()
        {
            return (_sprintState) ? _walkingSprintInterval : _walkingInterval;
        }
        #endregion

        #region 复活音效
        private bool _dieFlag = false;
        private void OnDeath()
        {
            _dieFlag = true;
        }

        private void OnRelive()
        {
            if (_dieFlag)
            {
                SoundInfoManager.Instance.PlaySound(PlayerSoundDefine.RELIVE);
                _dieFlag = false;
            }
        }

        private void OnLeaveScene(int mapId)
        {
            _dieFlag = false;
        }
        #endregion

        #region 濒死音效
        private bool _isPlayingNearDeathSound = false;
        private uint _nearDeathSoundID = 0;
        private bool _nearDeathSoundSwitch = false;
        private void OnEnterScene(int mapId)
        {
            if (GameSceneManager.GetInstance().IsInCity())
            {
                _nearDeathSoundSwitch = false;
            }
            else
            {
                _nearDeathSoundSwitch = true;
            }
            if (_isPlayingNearDeathSound)
            {
                StopNearDeathSound();
            }
        }

        private void OnHPChange()
        {
            if (!_nearDeathSoundSwitch)
            {
                return;
            }
            int cur_hp = PlayerAvatar.Player.cur_hp;
            int max_hp = PlayerAvatar.Player.max_hp;
            float percent = (float)cur_hp / max_hp;
            if (percent <= 0.3f && percent > 0)
            {
                if (!_isPlayingNearDeathSound)
                {
                    PlayNearDeathSound();
                }
            }
            else
            {
                if (_isPlayingNearDeathSound)
                {
                    StopNearDeathSound();
                }
            }
        }

        public void PlayNearDeathSound()
        {
            _nearDeathSoundID = SoundInfoManager.Instance.PlaySound(PlayerSoundDefine.NEAR_DEATH, true);
            _isPlayingNearDeathSound = true;
            LoggerHelper.Debug(string.Format("PlayNearDeathSound, id = {0}, cur_hp = {1}, max_hp = {2}.", _nearDeathSoundID, PlayerAvatar.Player.cur_hp, PlayerAvatar.Player.max_hp));
        }

        public void StopNearDeathSound()
        {
            bool result = SoundInfoManager.Instance.StopSound(_nearDeathSoundID);
            _isPlayingNearDeathSound = false;
            LoggerHelper.Debug(string.Format("StopNearDeathSound, id = {0}, result = {1}, cur_hp = {2}, max_hp = {3}.", _nearDeathSoundID, result, PlayerAvatar.Player.cur_hp, PlayerAvatar.Player.max_hp));
        }

        private void OnMissionEnd()
        {
            if (_isPlayingNearDeathSound)
            {
                StopNearDeathSound();
            }
            _nearDeathSoundSwitch = false;
        }
        #endregion
    }
}
