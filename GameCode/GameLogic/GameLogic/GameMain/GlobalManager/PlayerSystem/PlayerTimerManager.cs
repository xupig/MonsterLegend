﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 17:50:33
 * 描述说明：
 * *******************************************************/

using Common.Data;
using Common.Events;
using Common.Global;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.PlatformSdk;
using GameLoader.Utils;
using GameLoader.Utils.Network;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;

using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class PlayerTimerManager
    {
        private static PlayerTimerManager s_instance = null;
        public static PlayerTimerManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerTimerManager();
            }
            return s_instance;
        }

        private PlayerTimerManager()
        {
            openActivitySet = new HashSet<int>();
        }

        private uint m_timeId;
        private uint m_heartBeatTimerId;  //心跳定时器ID
        //private float _currentClientRealTime;
        //private ulong _currentServerTimeStamp;
        private Action _syncServerTimeCallback;
        private bool _checkLocalRate = false;
        private ulong _maxLocalTimeOffset = 0;

        public void OnPlayerEnterWorld()
        {
            StartHeartBeat();                                                                     //请求进入游戏成功，开启心跳
            enableNetTimeoutCheck(true);                                                          //开启网络断线检查
            Global.localTimeZone = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            EventDispatcher.AddEventListener<string>(RPCEvents.NetStatus, onNetStatusHandler);    //ServerProxy.cs抛出   
        }

        public void onNetStatusHandler(string status)
        {
            /*
            //PlayerTimerManager.GetInstance().OnPlayerLeaveWorld();
            LoggerHelper.Info("[PlayerTimerManager] 网络状态:" + status);
            if (status == TCPClientWorker.LINK_FAIL)
            {
          //ari SystemAlert.Show(true, "网络失败", "无法连接服务器！", true);
            }
            else if (status == TCPClientWorker.LINKING)
            {
          //ari SystemAlert.Show(true, "网络重连", "服务器重连中...", true);
            }
            else SystemAlert.CloseMessageBox();
             * */
        }

        public void OnPlayerLeaveWorld()
        {
            StopHeartBeat();
            StopSimulateServerTime();
            EventDispatcher.RemoveEventListener<string>(RPCEvents.NetStatus, onNetStatusHandler);
        }

        /// <summary>
        /// 开启网络断线检查
        /// </summary>
        /// <param name="isEnable"></param>
        public void enableNetTimeoutCheck(bool isEnable)
        {
            int timeout = int.Parse(XMLManager.global_params[80].__value);
            TCPClientWorker.CLOSE_TIMEOUT = timeout;                          //网络断线超时（毫秒）60 * 1000;
            ServerProxy.Instance.enableTimeout(isEnable);                         //开启断线检查
            LoggerHelper.Info(string.Format("[网络超时设置] closeTimeout:{0}ms,检查开启状态:{1}", timeout, isEnable));
        }

        /// <summary>
        /// 开启心跳
        /// </summary>
        public void StartHeartBeat()
        {           
            if (m_heartBeatTimerId <1)
            {
                m_heartBeatTimerId = TimerHeap.AddTimer(0, 60000, HeartBeat);
            }
        }

        /// <summary>
        /// 关闭心跳
        /// </summary>
        private void StopHeartBeat()
        {
            if (m_heartBeatTimerId != 0)
            {
                TimerHeap.DelTimer(m_heartBeatTimerId);
                m_heartBeatTimerId = 0;
            }
        }

        private float _lastTimeSyncReq = 0;
        //发送心跳包
        public void HeartBeat()
        {
            if ((Time.realtimeSinceStartup - _lastTimeSyncReq) < 2) return;
            _lastTimeSyncReq = Time.realtimeSinceStartup;
            if (ServerProxy.Instance.Connected)
            {
                if (GMState.showheartbeatinfo)
                {
                    LoggerHelper.Info(string.Format("sync_time_req, timeStamp = {0}.", Global.serverTimeStamp));
                }
                MogoWorld.Player.RpcCall("sync_time_req", Global.serverTimeStamp);
            }
        }

        /// <summary>
        /// 启动模拟服务端时间戳
        /// </summary>
        private void StartSimulateServerTime()
        {
            if (m_timeId == 0)
            {
                m_timeId = TimerHeap.AddTimer(0, 1000, handler);
            }
        }

        private void StopSimulateServerTime()
        {
            if (m_timeId != 0)
            {
                TimerHeap.DelTimer(m_timeId);
                m_timeId = 0;
            }
        }

        public int GetCurrentSecond()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
            return dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second;
        }

        private void handler()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            if (timeStamp % 15 == 0)
            {
                dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
                Global.currentSecond = dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second;
                if (GameSceneManager.GetInstance().CanShowNotice())
                {
                    CheckActivity();
                    CheckPortalNotice();
                }
            }
            if (GameSceneManager.GetInstance().CanShowNotice() && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.rune))
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(3, RuneManager.Instance.CheckRune());
            }
            PlayerDataManager.Instance.TeamData.OnStep();
            TaskManager.Instance.CheckAutoTask();
        }
       

        public DateTime GetNowServerDateTime()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            return MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
        }

        public int GetNowServerMillisecond()
        {
            return (int)(Common.Global.Global.serverTimeStamp % 1000);
        }

        private bool _isCheckTime = true;

        public void SyncTimeResp(UInt64 time)
        {
            if (CanCheckLocalRate())
            {
                CheckLocalRate(time);
            }
            Global.HeartBeat = false;
            Global.serverTimeStamp = time;
            StartSimulateServerTime();
            if (_syncServerTimeCallback != null)
            {
                _syncServerTimeCallback();
                _syncServerTimeCallback = null;
            }
            if(_isCheckTime)
            {
                _isCheckTime = false;
                HeartBeat();
            }
        }

        private ulong _startServerTimeStamp = 0;
        private float _startClientTimeStamp = 0;
        private bool _hasShowedAlert = false;
        private bool _firstSync = true;
        private bool CheckLocalRate(ulong serverTime)
        {
            if (_startServerTimeStamp == 0)
            {
                if (_firstSync)
                {
                    _firstSync = false;
                }
                else
                {
                    _startServerTimeStamp = serverTime;
                    _startClientTimeStamp = Time.realtimeSinceStartup;
                }
                return true;
            }
            ulong clientDuration = (ulong)((Time.realtimeSinceStartup - _startClientTimeStamp) * 1000);
            ulong serverDuration = serverTime - _startServerTimeStamp;
            if (clientDuration > serverDuration)
            {
                ulong offset = clientDuration - serverDuration;
                if (offset >= GetMaxLocalTimeOffset())
                {
                    if (_hasShowedAlert == false)
                    {
                        _hasShowedAlert = true;
                        string txt1 = XMLManager.chinese[103062].__content;
                        string txt2 = XMLManager.chinese[103063].__content;
                        string txt3 = XMLManager.chinese[103022].__content;
                  //ari SystemAlert.Show(true, txt1, txt2, OnExitGame, null, txt3, null, null, true, false);
                    } 
                    LoggerHelper.Error(string.Format(string.Format("CheckLocalRate, clientDuration = {0}, serverDuration = {1}, offset = {2}.", clientDuration, serverDuration, offset)));
                    return false;
                }
                return false;
            }
            return true;
        }

        private void OnExitGame()
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                PlatformWindows.allowQuitting = true;
            }
            //如果接入SDK：重启游戏，否则退出应用
            PlatformSdkMgr.Instance.RestartGame();
        }

        public ulong GetLocalTimeStamp()
        {
            if (_startServerTimeStamp == 0) return 0;
            return _startServerTimeStamp + (ulong)((Time.realtimeSinceStartup - _startClientTimeStamp) * 1000);
        }

        private bool CanCheckLocalRate()
        {
            if (_maxLocalTimeOffset == 0)
            {
                InitLocalTimeParams();
            }
            return _checkLocalRate;
        }

        private ulong GetMaxLocalTimeOffset()
        {
            if (_maxLocalTimeOffset == 0)
            {
                InitLocalTimeParams();
            }
            return _maxLocalTimeOffset;
        }

        private void InitLocalTimeParams()
        {
            string[] localTimeParams = global_params_helper.GetGlobalParam(164).Split(',');
            _checkLocalRate = Convert.ToBoolean(int.Parse(localTimeParams[0]));
            _maxLocalTimeOffset = ulong.Parse(localTimeParams[1]);
        }

        //刷新客户端时间戳
        public void RefreshLocalTimeStamp()
        {
            Global.RefreshLocalTimeStamp();
            HeartBeat();
        }

        public void SetSyncTimeCallback(Action syncTimeCallback)
        {
            _syncServerTimeCallback = syncTimeCallback;
        }

        private DateTime dateTime;

        private HashSet<int> openActivitySet;

        private void CheckActivity()
        {
            List<LimitActivityData> dataList = activity_helper.GetLimitActivityList();
            for(int i=0;i<dataList.Count;i++)
            {
                LimitActivityData dataItem = dataList[i];
                if(activity_helper.IsOpen(dataItem))
                {
                    if(openActivitySet.Contains(dataItem.openData.__id) == false)
                    {
                        PlayerDataManager.Instance.ActivityData.hasActiveActivity = true;
                    }
                    openActivitySet.Add(dataItem.openData.__id);
                   // EventDispatcher.TriggerEvent(ActivityEvents.ACTIVITY_CHANGE_STATE,dataItem.openData.__id,ActivityState.UNDERWAY);
                }
                else if(openActivitySet.Contains(dataItem.openData.__id))
                {
                    openActivitySet.Remove(dataItem.openData.__id);
                    //EventDispatcher.TriggerEvent(ActivityEvents.ACTIVITY_CHANGE_STATE, dataItem.openData.__id, ActivityState.FINISHED);
                }
                activity_helper.SendNotice(dataItem, Global.currentSecond);
            }
            if(openActivitySet.Count == 0)
            {
                PlayerDataManager.Instance.ActivityData.hasActiveActivity = false;
            }
            EventDispatcher.TriggerEvent(ActivityEvents.ACTIVITY_TIME_UPDATE_LIST);
        }

        private void CheckPortalNotice()
        {
            List<ActivityNoticeData> dataList = transmit_portal_helper.GetNoticeDataList();
            for (int i = 0; i < dataList.Count; i++)
            {
                transmit_portal_helper.SendNotice(dataList[i], Global.currentSecond);
            }
        }


    }
}
