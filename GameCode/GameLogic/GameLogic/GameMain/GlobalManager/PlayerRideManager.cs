﻿
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class PlayerRideManager
    {
        private static PlayerRideManager s_Instance;
        public static PlayerRideManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new PlayerRideManager();
                }
                return s_Instance;
            }
        }

        private float _operationTimeStamp = 0f;

        public void RequestFly()
        {
            byte rideId = (byte)PlayerAvatar.Player.ride_id;
            if (rideId == 0)
            {
                return;
            }
            if (mount_helper.GetMountType(rideId) != MountType.FLY)
            {
                return;
            }
            if (!CheckCD())
            {
                //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, 
                //ari     MogoLanguageUtil.GetContent(global_params_helper.GetFlyChineseID()));
                return;
            }
            if (PlayerAvatar.Player.actor.isFlyingOnRide)
            {
                //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(112523));
                return;
            }
            _operationTimeStamp = Time.realtimeSinceStartup;
            PlayerAvatar.Player.RpcCall("fly_req", 1);
        }

        public void RequestLand()
        {
            byte rideId = (byte)PlayerAvatar.Player.ride_id;
            if (rideId == 0)
            {
                return;
            }
            if (mount_helper.GetMountType(rideId) != MountType.FLY)
            {
                return;
            }
            if (!CheckCD())
            {
                //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips,
                //ari     MogoLanguageUtil.GetContent(global_params_helper.GetFlyChineseID()));
                return;
            }
            if (!PlayerAvatar.Player.actor.isFlyingOnRide)
            {
                //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(112524));
                return;
            }
            _operationTimeStamp = Time.realtimeSinceStartup;
            PlayerAvatar.Player.RpcCall("fly_req", 0);
        }

        private bool CheckCD()
        {
            return Time.realtimeSinceStartup - _operationTimeStamp >= global_params_helper.GetFlyOperation();
        }
    }
}
