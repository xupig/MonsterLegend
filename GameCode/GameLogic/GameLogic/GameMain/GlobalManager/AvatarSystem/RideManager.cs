﻿using ACTSystem;
using Common.ClientConfig;
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Mgrs;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class RideManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class RideManager
    {
        private const float RIDE_DOWN_DURATION = 1f;

        private EntityAvatar _owner;
        private byte _rideId;
        private int _actorID = 0;
        private float _slotCameraOffset = 0;
        private float _dismountTimeStamp = 0;
        private bool _actorLoaded = false;
        private bool _switchingSpace = false;
		private bool _enterClientSpace = false;
        private bool _firstActorLoaded = true;

        public RideManager(EntityAvatar owner)
        {
            _owner = owner;
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("RideManager.Update", Update);
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.ride_id, OnRideIdChange);
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.active_ride_id, OnActiveRideIdChange);
            if (_rideId > 0)
            {
                DestroyActor();
                _rideId = 0;
            }
            _owner = null;
        }

        public void OnControllerLoaded()
        {
            if (_owner == null)
            {
                return;
            }
            if (_firstActorLoaded)
            {
                OnActorLoaded();
                _firstActorLoaded = false;
            }
            else
            {
                if (IsSelf())
                {
                    OnPlayerEnterSpace();
                }
			}
			if (IsSelf())
			{
				_switchingSpace = false;
			}
        }

        private bool IsSelf()
        {
            return _owner is PlayerAvatar;
        }

        private void OnActorLoaded()
        {
            _actorLoaded = true;
            OnRideIdChange();
            OnActiveRideIdChange();
            _actorLoaded = false;
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.ride_id, OnRideIdChange);
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.active_ride_id, OnActiveRideIdChange);
            MogoWorld.RegisterUpdate<RideManagerUpdateDelegate>("RideManager.Update", Update);
        }

        public void OnPlayerEnterSpace()
        {
            if (_rideId > 0)
            {
                if (IsClientScene() && !CheckInstanceCanRide())
                {
                    _rideId = 0;
					_enterClientSpace = true;
                }
                else
                {
                    CreateActor();
                }
            }
			else
			{
				if (_owner.ride_id > 0 && _enterClientSpace)
				{
					Mount(_owner.ride_id);
					_enterClientSpace = false;
				}
			}
        }

        public void OnPlayerLeaveSpace()
        {
            _dismountTimeStamp = 0;
            _switchingSpace = true;
            if (_rideId > 0)
            {
                DestroyActor();
            }
        }

        public void OnDeath()
        {
            Dismount();
        }

        //上坐骑
        public bool Mount(byte rideId)
        {
			if (_rideId == rideId)
            {
                return false;
            }
            //Debug.LogError("Mount " + rideId + " " + RealTime.time);
            _dismountTimeStamp = 0;
            _rideId = rideId;
            CreateActor();
            return true;
        }

        //下坐骑
        public bool Dismount()
        {
            _dismountTimeStamp = RealTime.time;
			if (_rideId == 0)
            {
                return false;
            }
            //Debug.LogError("Dismount ");
            DestroyActor();
            _rideId = 0;
            return true;
        }

        //起飞
        public void Fly()
        {
            if (!CheckFlyOrLand())
            {
                return;
            }
            _owner.actor.rideController.Fly(map_helper.CanFly(GameSceneManager.GetInstance().curMapID));
        }

        //降落
        public void Land()
        {
            if (!CheckFlyOrLand())
            {
                _owner.actor.isFlyingOnRide = false;
                return;
            }
            _owner.actor.rideController.Land();
            Vector3 position = _owner.actor.position;
            if (!GameSceneManager.GetInstance().CheckCurrPointIsCanMove(position.x, position.z))
            {
                Vector3 teleportPoint = GameSceneManager.GetInstance().CheckPointAroundCanMove(position.x, position.z, false, true);
                if (teleportPoint == Vector3.zero)
                {
                    LoggerHelper.Error(string.Format("飞行降落点寻找最近的可降落点失败，请检查是否障碍信息问题。@谢思宇，降落前的点为x = {0}, z = {1}.", position.x, position.z));
                    return;
                }
                _owner.Teleport(teleportPoint);
            }
        }

        public ACTActor ride
        {
            get
            {
                if (_owner.actor == null)
                    return null;
                return _owner.actor.ride;
            }
        }

        private void CreateActor()
        {
            _actorID = mount_helper.GetModelID(_rideId);
            GameObjectPoolManager.GetInstance().CreateActorGameObject(_actorID, (actor) =>
            {
                if (actor == null)
                {
                    LoggerHelper.Error(string.Format("创建坐骑模型出错，坐骑模型id = {0}找不到对应的模型。", _actorID));
                    return;
                }
                if (_owner == null || !_owner.isInWorld || _actorID != actor.actorData.ID)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(_actorID, actor.gameObject);
                    return;
                }
                ACTActor rideActor = ride;
                _owner.actor.rideController.Mount(actor, !_actorLoaded || IsInBornTime(), InCity());
                CameraManager.GetInstance().mainCamera.enabled = false;
                CameraManager.GetInstance().mainCamera.enabled = true;
                ride.name = string.Format("Ride:{0}-{1}-{2}", _owner.name, _owner.id, _rideId);
                if (rideActor != null)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(rideActor.actorData.ID, rideActor.gameObject);
                }
                if (InFlyState())
                {
                    Fly();
                }
                else if (IsPlayer() && IsFlyType())
                {
                    FlyReq();
                }
                _owner.UpdateEntityComponents();
            });
        }

        private void DestroyActor()
        {
            if (ride == null)
            {
                _actorID = 0;
                return;
            }
            ACTActor rideActor = ride;
            _owner.actor.rideController.Dismount(InCity());
            _actorID = 0;
            if (rideActor != null)
            {
                GameObjectPoolManager.GetInstance().ReleaseActorGameObject(rideActor.actorData.ID, rideActor.gameObject);
            }
            _owner.UpdateEntityComponents();
        }

        private void OnRideIdChange()
        {
            byte rideId = _owner.ride_id;
            //Debug.LogError("OnRideIdChange " + rideId + " " + RealTime.time);
            if (rideId == _rideId)
            {
                return;
            }
            if (rideId == 0)
            {
                Dismount();
            }
            else
            {
                TryAutoMount(rideId);
            }
        }

        private void OnActiveRideIdChange()
        {
            //Debug.LogError("OnActiveRideIdChange " + _owner.active_ride_id);
            _dismountTimeStamp = 0;
        }

        private void Update()
        {
            if (!CheckAutoMount())
            {
                return;
            }

            if (TryAutoMount(_owner.active_ride_id))
            {
                if (IsPlayer() && _owner.ride_id == 0)
                {
                    //Debug.LogError("MountReq");
                    MountManager.Instance.MountReq();
                }
            }
        }

        private bool CheckAutoMount()
        {
            if (!CheckInstanceCanRide())
            {
                return false;
            }
            if (_owner.active_ride_id == 0)
            {
                return false;
            }
            if (_dismountTimeStamp == 0)
            {
                return false;
            }
            if (_switchingSpace)
            {
                return false;
            }
            if (!CheckRideState())
            {
                return false;
            }
            if (CheckInViewingMode())
            {
                return false;
            }
            return true;
        }

        private bool TryAutoMount(byte rideId)
        {
            if (rideId == 0 || rideId == _rideId)
            {
                return false;
            }
            float nowTime = RealTime.time;
            if (_dismountTimeStamp == 0 ||
                nowTime - _dismountTimeStamp >= mount_helper.GetAutoMountDuration())
            {
                if (rideId > 0)
                {
                    Mount(rideId);
                    return true;
                }
            }
            return false;
        }

        private void FlyReq()
        {
            PlayerRideManager.Instance.RequestFly();
        }

        private bool IsPlayer()
        {
            return _owner is PlayerAvatar;
        }

        private bool IsInBornTime()
        {
            return (int)(Common.Global.Global.serverTimeStampSecond) - (int)_owner.born_time <= 3;
        }

        private bool CheckInstanceCanRide()
        {
            return inst_type_operate_helper.CanRide((ChapterType)GameSceneManager.GetInstance().curMapType);
        }

        private bool IsClientScene()
        {
            return GameSceneManager.GetInstance().IsInClientMap();
        }

        private bool CheckRideState()
        {
            return _owner.stateManager.CanDO(CharacterAction.AUTO_MOUNT_ABLE);
        }

        private bool CheckInViewingMode()
        {
            return DramaManager.GetInstance().IsInViewingMode();
        }

        private bool InCity()
        {
            return GameSceneManager.GetInstance().IsInCity();
        }

        private bool InFlyState()
        {
            return _owner.stateManager.InState(state_config.AVATAR_STATE_FLY);
        }

        public bool IsFlyType()
        {
            return mount_helper.GetMountType(_rideId) == MountType.FLY;
        }

        private bool CheckFlyOrLand()
        {
            if (ride == null)
            {
                LoggerHelper.Debug(string.Format("无法起飞和降落，因为实体{0}当前没有坐骑。", _owner.id));
                return false;
            }
            if (!IsFlyType())
            {
                LoggerHelper.Debug(string.Format("无法起飞和降落，因为坐骑{0}不是飞行坐骑。", _rideId));
                return false;
            }
            return true;
        }
    }
}
