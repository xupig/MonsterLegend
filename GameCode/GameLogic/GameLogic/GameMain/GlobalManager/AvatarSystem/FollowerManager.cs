﻿using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;

namespace GameMain.GlobalManager
{
    public class FollowerManager
    {
        private EntityAvatar _owner;
        public FollowerManager(EntityAvatar owner)
        {
            _owner = owner;
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.owner_id, this.OnOwnerChange);
            EventDispatcher.AddEventListener<uint, int>(TeamInstanceEvents.Teammate_Online_Refresh, OnOnlineChange);
        }

        public void Release()
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.owner_id, this.OnOwnerChange);
            EventDispatcher.RemoveEventListener<uint, int>(TeamInstanceEvents.Teammate_Online_Refresh, OnOnlineChange);
            _owner = null;
        }

        public void OnActorLoaded()
        {
            OnOwnerChange();
        }

        public void OnEnterSpace()
        {
            if (!IsSelf())
            {
                return;
            }
            OnOwnerChange();
        }

        private void OnOwnerChange()
        {
            if (_owner.owner_id > 0)
            {
                if (IsSelf())
                {
                    if (ShouldEnterControlled())
                    {
                        if (_owner.EnterBeControlled())
                        {
                            EnterControlled();
                        }
                    }
                    else
                    {
                        if (_owner.LeaveBeControlled())
                        {
                            LeaveControlled();
                        }
                    }
                }
                else 
                {
                    if (ShouldEnterFollower())
                    {
                        if (_owner.EnterFollower())
                        {
                            EnterFollower();
                        }
                    }
                    else
                    {
                        if (_owner.LeaveFollower())
                        {
                            LeaveFollower();
                        }
                    }
                }
            }
            else
            {
                if (IsSelf())
                {
                    if (_owner.LeaveBeControlled())
                    {
                        LeaveControlled();
                    }
                }
                else
                {
                    if (_owner.LeaveFollower())
                    {
                        LeaveFollower();
                    }
                }
            }
        }

        private void OnOnlineChange(uint eid, int online)
        {
            if (_owner.id != eid)
            {
                return;
            }
            if (InCity() || InWild())
            {
                return;
            }
            if (IsSelf())
            {
                return;
            }
            OnOwnerChange();
        }

        private void EnterFollower()
        {
            ChangeMovingMode(AccordingMode.AccordingActor);
            AddFollowerAI();
        }

        private void LeaveFollower()
        {
            ChangeMovingMode(AccordingMode.AccordingEntity);
            RemoveFollowerAI();
        }

        private bool ShouldEnterFollower()
        {
            if (!FollowSelf())
            {
                return false;
            }
            if (!InWild() && !InCity())
            {
				if (IsOnline())
				{
					return false;
				}
            }
            return true;
        }

        private void AddFollowerAI()
        {
            int aiID = (InCity() || InWild()) ? GetCityAI() : GetCombatAI();
			if (_owner.entityAI != null && !(_owner.entityAI is EntityPuppetAI))
            {
				_owner.entityAI = null;
            }
			if (_owner.entityAI == null)
            {
				_owner.entityAI = new EntityPuppetAI(_owner);
            }
			_owner.entityAI.SetBehaviorTree((uint)aiID);
            _owner.aiID = aiID;
        }

        private void RemoveFollowerAI()
        {
            _owner.entityAI = null;
            _owner.aiID = 0;
        }

        private void EnterControlled()
        {
            ChangeMovingMode(AccordingMode.AccordingEntity);
        }

        private void LeaveControlled()
        {
            var accordingMode = PlayerTouchBattleModeManager.GetInstance().CurrentCanTouch() ? AccordingMode.AccordingTouch : AccordingMode.AccordingStick;
            ChangeMovingMode(accordingMode);
        }

        private bool ShouldEnterControlled()
        {
            if (!InWild() && !InCity())
            {
                return false;
            }
            return true;
        }

        private void ChangeMovingMode(AccordingMode accordingMode)
        {
            _owner.moveManager.defaultAccordingMode = accordingMode;
            _owner.moveManager.accordingMode = accordingMode;
        }

        private bool InCity()
        {
            return GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_CITY;
        }

        private bool InWild()
        {
            return GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD;
        }

        private int GetCityAI()
        {
            return global_params_helper.GetSpaceTimeRiftCityAI();
        }

        private int GetCombatAI()
        {
            return map_helper.GetPlayerAI(GameSceneManager.GetInstance().curMapID);
        }

        private bool SelfIsCaptain()
        {
            return PlayerDataManager.Instance.TeamData.IsCaptain();
        }

        private bool IsSelf()
        {
            return _owner is PlayerAvatar;
        }

        private bool FollowSelf()
        {
            return _owner.owner_id == PlayerAvatar.Player.id;
        }

        private bool IsOnline()
        {
            return PlayerDataManager.Instance.TeamData.IsOnline(_owner.id);
        }
    }
}
