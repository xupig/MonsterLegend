﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using GameMain.Entities;
using Common.Structs;
using GameData;
using GameLoader.Utils.Timer;
using Common.ClientConfig;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using MogoEngine;

namespace GameMain.GlobalManager
{
    public class AvatarEquipManager
    {
        private EntityAvatar  _owner;
        private string _curEquipInfo = string.Empty;
        private AvatarModelData _curAvatarModelData = new AvatarModelData();
        private bool _dirty = false;

        private int _fashionId = 0; //当前时装id(只对玩家, 若=0则穿装备，否则穿时装)

        public AvatarEquipManager(EntityAvatar ower)
        {
            _owner = ower;
            _curAvatarModelData.vocation = this._owner.vocation;
            ResetCurEquipDictByDefault();
            RefreshEquip();
            EntityPropertyManager.Instance.AddEventListener(this._owner, EntityPropertyDefine.facade, OnFacadeChange);
            EventDispatcher.AddEventListener<int>(SettingEvents.UPDATE_RENDER_QUALITY_SETTING, OnQualityChange);
            if (_owner == PlayerAvatar.Player)
            {
                EventDispatcher.AddEventListener<BagType>(BagEvents.Init, OnBagInit);
                EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
                EventDispatcher.AddEventListener(WingEvents.UPDATE_PUT_ON_WING, RefreshEquip);
                EventDispatcher.AddEventListener<int>(FashionEvents.PUT_ON_FASHION, RefreshFashion);
                EventDispatcher.AddEventListener(FashionEvents.CHANGE_EQUIP_EFFECT, RefreshEquip);
            }
        }

        public void Release()
        {
            EventDispatcher.RemoveEventListener<int>(SettingEvents.UPDATE_RENDER_QUALITY_SETTING, OnQualityChange);
        }

        private void OnQualityChange(int quality)
        {
            UpdateSettingType();
        }

        private void UpdateSettingType()
        {
            if (_owner.actor == null) return;
            _owner.actor.equipController.equipWing.UpdateSettingType();
        }

        void SetDefaultEquip()
        {
            if (_owner.actor == null) return;
            _owner.actor.equipController.equipCloth.defaultEquipID = role_data_helper.GetDefaultCloth((int)this._owner.vocation);
            _owner.actor.equipController.equipWeapon.defaultEquipID = role_data_helper.GetDefaultWeapon((int)this._owner.vocation);
        }

        void OnFacadeChange()
        {
            if (_owner.actor == null) return;
            if (_owner != PlayerAvatar.Player)
            {
                RefreshAvatarEquip();
            } 
        }
        
        public void RefreshEquip()
        {
            if (_owner.actor == null) return;
            if (_owner == PlayerAvatar.Player)
            {
                RefreshPlayerEquip();
            }
            else
            {
                RefreshAvatarEquip();
            }
        }

        public void OnActorLoaded()
        {
            SetDefaultEquip();
            RefreshEquip();
        }

        public void ReloadCloth()
        {
            var clothInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
            if (clothInfo.equipID > 0)
            {
                _owner.actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow, _owner.modelNormalQuality);
            }
            else
            {
                _owner.actor.equipController.equipCloth.PutOn(0);
            }
        }

        public void ReloadWeapon()
        {
            var weaponInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
            if (weaponInfo.equipID > 0)
            {
                _owner.actor.equipController.equipWeapon.PutOn(weaponInfo.equipID, weaponInfo.particle, weaponInfo.flow, _owner.modelNormalQuality);
            }
            else
            {
                _owner.actor.equipController.equipWeapon.PutOn(0);
            }
        }

        public void ReloadWing()
        {
            var wingInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing);
            if (wingInfo.equipID > 0)
            {
                if (_owner is PlayerAvatar ||
                    inst_type_operate_helper.IsShowWing((ChapterType)GameSceneManager.GetInstance().curMapType))
                    _owner.actor.equipController.equipWing.PutOn(wingInfo.equipID, wingInfo.particle, wingInfo.flow, _owner.modelNormalQuality);
            }
            else
            {
                _owner.actor.equipController.equipWing.PutOn(0);
            }
        }

        void CheckDirty()
        {
            if (_dirty)
            {
                if (_owner.actor != null)
                {
                    ReloadEquip();
                }
                _dirty = false;
            }
        }

        public void ReloadEquip()
        {
            try
            {
                ReloadCloth();
                ReloadWeapon();
                ReloadWing();
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public string GetEquipInfoStr()
        {
            return ModelEquipTools.CalculateEquipInfoStr(this._curAvatarModelData);
        }

        void ResetCurEquipDictByDefault()
        {
            ModelEquipTools.ResetAvatarModelDataByDefault(_curAvatarModelData);
            _dirty = true;
        }

        void ResetCurEquipDictByString(string equipInfo)
        {
            ModelEquipTools.ResetAvatarModelDataByString(_curAvatarModelData, equipInfo);
            _dirty = true;
        }

        #region avatar

        public void RefreshAvatarEquip()
        {
            if (_curEquipInfo.Equals(_owner.facade) && !string.IsNullOrEmpty(_curEquipInfo)) return;
            _curEquipInfo = _owner.facade;
            _dirty = true;
            ResetCurEquipDictByString(_curEquipInfo);
            CheckDirty();
        }

        #endregion

        #region player
        void OnBagInit(BagType bagType)
        {
            if (bagType == BagType.BodyEquipBag)
            {
                RefreshEquip();
            }
        }

        void OnBagUpdate(BagType bagType, uint position)
        {
            if (bagType == BagType.BodyEquipBag)
            {
                RefreshEquip();
            }
        }

        void RefreshFashion(int fashionId)
        {
            if (_fashionId == fashionId)
            {
                return;
            }
            _fashionId = fashionId;
            RefreshEquip();
        }

        public void RefreshPlayerEquip()
        {
            ResetCurCloth();
            ResetCurWeapon();
            ResetCurWing();
            CheckSynInfoToServer();
            CheckDirty();
        }

        void SynInfoToServer()
        {
            var curInfo = GetEquipInfoStr();
            if (!curInfo.Equals(this._owner.facade))
            {
                this._owner.facade = curInfo;
                PlayerAvatar.Player.RpcCall("set_facade_req", curInfo);
                MogoWorld.SynEntityAttr(this._owner, EntityPropertyDefine.facade, this._owner.facade);
            }
            _dirty = false;
        }

        uint synTimerID = 0;
        void CheckSynInfoToServer()
        {
            if (_dirty)
            {
                if (synTimerID != 0) TimerHeap.DelTimer(synTimerID);
                synTimerID = TimerHeap.AddTimer(3000, 0, SynInfoToServer);
            }
        }

        void ResetCurCloth()
        {
            int curClothID = GetCurClothID();
            if (curClothID == 0) curClothID = role_data_helper.GetDefaultCloth((int)this._owner.vocation);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID != curClothID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID = curClothID;
            }
            if (curClothID == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = 0;
                return;
            }
            int curClothParticeID = GetCurParticleID(ACTEquipmentType.Cloth);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle != curClothParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = curClothParticeID;
            }
            int curClothFlowID = GetCurFlowID(ACTEquipmentType.Cloth);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow != curClothFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = curClothFlowID;
            }
        }

        void ResetCurWeapon()
        {
            int curWeapon = GetCurWeaponID();
            if (curWeapon == 0) curWeapon = role_data_helper.GetDefaultWeapon((int)this._owner.vocation);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID != curWeapon)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID = curWeapon;
            }
            if (curWeapon == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = 0;
                return;
            }
            int curClothParticeID = GetCurParticleID(ACTEquipmentType.Weapon);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle != curClothParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = curClothParticeID;
            }
            int curClothFlowID = GetCurFlowID(ACTEquipmentType.Weapon);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow != curClothFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = curClothFlowID;
            }
        }

        void ResetCurWing()
        {
            int curWing = GetCurWingID();
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID != curWing)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID = curWing;
            }
            //if (curWing == 0)
            //{
            //    _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = 0;
            //    _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = 0;
            //return;
            //}
            //int curClothParticeID = GetCurParticleID(ACTEquipmentType.Wing);
            //if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle != curClothParticeID)
            //{
            //    _dirty = true;
            //    _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = curClothParticeID;
            //}
            //int curClothFlowID = GetCurFlowID(ACTEquipmentType.Wing);
            //if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow != curClothFlowID)
            //{
            //    _dirty = true;
            //    _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = curClothFlowID;
            //}
        }

        private int GetCurClothID()
        {
            int curCloth = 0;
            if (UseFashionEquipment())
            {
                curCloth = PlayerDataManager.Instance.FashionData.GetEquipId(ACTEquipmentType.Cloth);
            }
            else
            {
                var equipInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.breastPlate);
                curCloth = equipInfo != null ? item_helper.GetEquipModelID(equipInfo.Id) : 0;
            }
            return curCloth;
        }

        private int GetCurWeaponID()
        {
            int curWeapon = 0;
            if (UseFashionEquipment())
            {
                curWeapon = PlayerDataManager.Instance.FashionData.GetEquipId(ACTEquipmentType.Weapon);
            }
            else
            {
                var equipInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.weapon);
                curWeapon = equipInfo != null ? item_helper.GetEquipModelID(equipInfo.Id) : 0;
            }
            return curWeapon;
        }

        private int GetCurWingID()
        {
            var wingInfo = PlayerDataManager.Instance.WingData.GetPutOnWingData();
            int curWing = 0;
            if (wingInfo != null)
            {
                curWing = wingInfo.ModelId;
            }
            return curWing;
        }

        private int GetCurParticleID(ACTEquipmentType type)
        {
            return PlayerDataManager.Instance.EquipEffectData.GetCurrentEffectParticleID(type);
        }

        private int GetCurFlowID(ACTEquipmentType type)
        {
            return PlayerDataManager.Instance.EquipEffectData.GetCurrentEffectFlowID(type);
        }

        private bool UseFashionEquipment()
        {
            return _fashionId > 0;
        }

        #endregion

        public void TestChangeCloth(int equipID, int particleID, int flowID)
        {
            var info = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
            if (info.equipID != equipID || info.particle != particleID || info.flow != flowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID = equipID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = particleID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = flowID;
				CheckDirty();
            }
        }

        public void TestChangeWeapon(int weaponID, int particleID, int flowID)
        {
            var info = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
			if (info.equipID != weaponID || info.particle != particleID || info.flow != flowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID = weaponID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = particleID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = flowID;
                CheckDirty();
            }
        }

        public void TestChangeWing(int wingID, int particleID, int flowID)
        {
            var info = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing);
			if (info.equipID != wingID || info.particle != particleID || info.flow != flowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID = wingID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = particleID;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = flowID;
                CheckDirty();
            }
        }
    }
}
