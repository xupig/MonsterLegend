﻿using GameData;
using GameLoader.Utils;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class GameObjectPoolDriver : MonoBehaviour
    {
        void OnLevelWasLoaded()
        {
            GameObjectPoolManager.GetInstance().Clear();
        }
    }
}
