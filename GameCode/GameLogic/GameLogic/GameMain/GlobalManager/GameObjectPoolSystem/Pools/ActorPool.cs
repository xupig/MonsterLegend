using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using GameData;
using GameMain.CombatSystem;
using GameLoader.Utils;
using ACTSystem;
using InspectorSetting;
namespace GameMain.GlobalManager
{
    public class ActorPoolUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ActorPool
    {
        GameObject _poolObj;
        Dictionary<int, Queue<GameObject>> _typeIdGameObjectMap = new Dictionary<int, Queue<GameObject>>();
        Queue<Action> _delayCreateCallbacks = new Queue<Action>();
        public ActorPool(GameObject mainPoolObj)
        {
            _poolObj = new GameObject("ActorPool");
            _poolObj.SetActive(false);
            _poolObj.transform.SetParent(mainPoolObj.transform, false);
            MogoWorld.RegisterUpdate<ActorPoolUpdateDelegate>("ActorPool.Update", Update);
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("ActorPool.Update", Update);
        }

        void Update()
        {
            if (_delayCreateCallbacks.Count > 0)
            {
                Action action = _delayCreateCallbacks.Dequeue();
                action();
            }
        }

        Queue<GameObject> GetGameObjectQueue(int typeId)
        {
            if (!_typeIdGameObjectMap.ContainsKey(typeId))
            {
                _typeIdGameObjectMap[typeId] = new Queue<GameObject>();
            }
            return _typeIdGameObjectMap[typeId];
        }

        public void CreateActorGameObject(int actorId, Action<ACTActor> callback, bool immediately = false)
        {
            if (immediately)
            {
                CreateActorGameObjectImmediately(actorId, callback, immediately);
            }
            else {
                _delayCreateCallbacks.Enqueue(() =>
                {
                    CreateActorGameObjectImmediately(actorId, callback, immediately);
                });
            }
        }

        void CreateActorGameObjectImmediately(int actorId, Action<ACTActor> callback, bool immediately = false)
        {
            UnityEngine.Profiler.BeginSample("CreateActorGameObject");
            var queue = GetGameObjectQueue(actorId);
            while (queue.Count > 0)
            {
                var gameObject = queue.Dequeue();
                if (gameObject == null) continue;
                gameObject.transform.SetParent(null);
                //gameObject.SetActive(true);
                var actor = gameObject.GetComponent<ACTActor>();
                actor.Reset();
                actor.enabled = true;
                callback(actor);
                return;
            }
            ACTSystemAdapter.CreateActor(actorId, (actor) =>
            {
                callback(actor);
            }, immediately);
            UnityEngine.Profiler.EndSample();
        }

        public void ReleaseActorGameObject(int actorId, GameObject gameObject)
        {
            var queue = GetGameObjectQueue(actorId);
            gameObject.transform.SetParent(_poolObj.transform);
            gameObject.transform.position = Vector3.zero;
            //gameObject.SetActive(false);
            gameObject.name = actorId.ToString();
            ClearInspectorObjects(gameObject);
            ClearComponents(gameObject);
            var actor = gameObject.GetComponent<ACTActor>();
            actor.enabled = false;
            queue.Enqueue(gameObject);
        }

        public void ClearComponents(GameObject gameObject)
        {
            var deathController = gameObject.GetComponent<ActorDeathController>();
            if (deathController != null) GameObject.Destroy(deathController);
            var actHandler = gameObject.GetComponent<ActorACTHandler>();
            if (actHandler != null) GameObject.Destroy(actHandler);
        }

        public void ClearInspectorObjects(GameObject gameObject)
        {
            if (Application.isEditor)
            {
                var coms = gameObject.GetComponents<BeInspectedBehaviour>();
                for(int i = 0; i < coms.Length; i++)
                {
                    GameObject.Destroy(coms[i]);
                    coms[i] = null;
                }
            }
        }

        public void Clear()
        {
            var childCount = _poolObj.transform.childCount;
            for (int i = childCount - 1; i >= 0; i--)
            {
                var go = _poolObj.transform.GetChild(i);
                GameObject.Destroy(go.gameObject);
            }
            _typeIdGameObjectMap.Clear();
        }
    }
}