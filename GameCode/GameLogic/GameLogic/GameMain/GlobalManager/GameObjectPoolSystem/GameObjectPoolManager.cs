using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameMain.PlayerAction;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using ACTSystem;

namespace GameMain.GlobalManager
{
    public class GameObjectPoolManager
    {
        private static GameObjectPoolManager s_instance = null;
        public static GameObjectPoolManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameObjectPoolManager();
            }
            return s_instance;
        }

        GameObject _poolObj;
        DropItemPool _dropItemPool;
        ActorPool _actorPool;
        private GameObjectPoolManager() 
        {
            _poolObj = new GameObject("GameObjectPool");
            _poolObj.AddComponent<GameObjectPoolDriver>();
            GameObject.DontDestroyOnLoad(_poolObj);
            _dropItemPool = new DropItemPool(_poolObj);
            _actorPool = new ActorPool(_poolObj);
        }

        public void CreateDropItemGameObject(int typeId, bool canBePicked,  Action<GameObject> callback)
        {
            _dropItemPool.CreateDropItemGameObject(typeId, canBePicked, callback);
        }

        public void ReleaseDropItemGameObject(int typeId, GameObject gameObject)
        {
            _dropItemPool.ReleaseDropItemGameObject(typeId, gameObject);
        }

        public void CreateActorGameObject(int actorId, Action<ACTActor> callback, bool immediately = false)
        {
            _actorPool.CreateActorGameObject(actorId, callback, immediately);
        }

        public void ReleaseActorGameObject(int actorId, GameObject gameObject)
        {
            _actorPool.ReleaseActorGameObject(actorId, gameObject);
        }

        public void Clear()
        {
            _dropItemPool.Clear();
            _actorPool.Clear();
        }

    }
}