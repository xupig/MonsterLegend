﻿using System;
using System.Collections.Generic;
using System.Text;

using Common;
using Common.IManager;
using Common.Events;
using GameLoader.Utils;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using MogoEngine.Mgrs;
using UnityEngine;
using GameLoader.Config;

using Common.Base;
using MogoEngine.RPC;
using MogoEngine;
using GameMain.Entities;
using Common.ServerConfig;
using Common.ClientConfig;
using ModuleMainUI;
using GameLoader;
using GameLoader.LoadingBar;
using GameData;
//using ssjjsy.com;

namespace GameMain.GlobalManager
{
    //游戏主要状态管理，以场景转变为依据切换状态。
    public class GameStateManager : IGameStateManager
    {
        public static int beginTime;
        private Dictionary<string, List<Action>> m_stateEnterLeaveMap;
        
        public GameStateManager()
        {
            m_stateEnterLeaveMap = new Dictionary<string, List<Action>>();
            m_stateEnterLeaveMap.Add(GameDefine.STATE_LOGIN, new List<Action>() { OnEnterLogin, OnLeaveLogin });
            m_stateEnterLeaveMap.Add(GameDefine.STATE_CREATE_CHARACTER, new List<Action>() { OnEnterCreateCharacter, OnLeaveCreateCharacter });
            m_stateEnterLeaveMap.Add(GameDefine.STATE_CHOOSE_CHARACTER, new List<Action>() { OnEnterChooseCharacter, OnLeaveChooseCharacter });
            m_stateEnterLeaveMap.Add(GameDefine.STATE_SCENE, new List<Action>() { OnEnterScene, OnLeaveScene});
        }

        private static GameStateManager s_instance = null;
        public static GameStateManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameStateManager();
            }
            return s_instance;
        }

        private string m_curState = string.Empty;
        public void SetState(string state)
        {
            if (!m_stateEnterLeaveMap.ContainsKey(state)) return;
            if (m_stateEnterLeaveMap.ContainsKey(m_curState))
            {
                m_stateEnterLeaveMap[m_curState][1]();
                LoggerHelper.Debug("Ari:skill:Leave State:" + m_curState);
            }
            m_curState = state;
            m_stateEnterLeaveMap[m_curState][0]();
        }

        public string GetState()
        {
            return m_curState;
        }

        /// <summary>
        /// 加载登陆界面以及登陆场景
        /// </summary>
        private void OnEnterLogin()
        {
            if (MogoWorld.Player != null)
            {
                MogoWorld.DestroyEntity(MogoWorld.Player.id);
            }
            beginTime = Environment.TickCount;
            if (Main.Instance.isUpdateProgress)
            {//应用启动时--加载登录界面
                ProgressBar.Instance.UpdateProgress(0.7f);
            }
            else 
            {
                ProgressBar.Instance.Show();
            }
            //ProgressBar.Instance.UpdateTip("正在载入登录场景");
            LoadingContentManager.Instance.ResetLoadingContent();
            //GameSceneManager.GetInstance().LoadLoginScene(() =>
            GameSceneManager.GetInstance().EnterScene(SceneType.LOGIN_SCENE, ()=>
            {
                //加载音乐
                LoginMusic.GetInstance().SetClipAndPlay();

                /*if (Main.Instance.isUpdateProgress)
                {
                    ProgressBar.Instance.UpdateProgress(0.9f);
                }
                else
                {
                    Main.Instance.isUpdateProgress = false;
                    ProgressBar.Instance.UpdateProgress(0);
                }
                ProgressBar.Instance.UpdateTip("正在加载登录界面");*/
                ProgressBar.Instance.Close();
                LoadingContentManager.Instance.CloseLoadingContent();
                UIManager.Instance.ShowPanel(PanelIdEnum.Login);

                if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
                {
                    //UIManager.Instance.ShowPanel(PanelIdEnum.PlatformLogin);
                }                
                //ProgressBar.Instance.Close();
            });
        }

        /// <summary>
        /// 返回到登录面板
        /// </summary>
        public void OnReturnLogin()
        {
            SetState(GameDefine.STATE_LOGIN);
        }

        private void OnLeaveLogin()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Login);
            LoginMusic.GetInstance().FadeOut();
        }

        private void OnEnterCreateCharacter()
        {
            ProgressBar.Instance.Show();
            //ProgressBar.Instance.UpdateTip("正在载入创角场景");
            LoadingContentManager.Instance.ResetLoadingContent();
            GameSceneManager.GetInstance().EnterScene(SceneType.CHARACTER_SCENE, () =>
            {
                ProgressBar.Instance.Close();
                LoadingContentManager.Instance.CloseLoadingContent();
                UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                UIManager.Instance.ShowPanel(PanelIdEnum.CreateRole);
                //ari EventDispatcher.TriggerEvent(SelectRoleEvents.ASK_NEW_ROLE);
            });
        }

        private void OnLeaveCreateCharacter()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
        }

        private void OnEnterChooseCharacter()
        {
            ProgressBar.Instance.Show();
            //ProgressBar.Instance.UpdateTip("正在载入选角场景");
            LoadingContentManager.Instance.ResetLoadingContent();
            GameSceneManager.GetInstance().EnterScene(SceneType.CHARACTER_SCENE, ()=>
            {
                ProgressBar.Instance.Close();
                LoadingContentManager.Instance.CloseLoadingContent();
                UIManager.Instance.ShowPanel(PanelIdEnum.SelectRole);
                //ari EventDispatcher.TriggerEvent(SelectRoleEvents.ASK_CREATE_ROLE);
            });
        }

        private void OnLeaveChooseCharacter()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
        }

        private void OnEnterScene()
        {
            int instanceId = GameSceneManager.GetInstance().curInstanceID;
            ChapterType chapterType = instance_helper.GetChapterType(instanceId);
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleElement);
            UIManager.Instance.ShowPanel(PanelIdEnum.ChatEntry);
            UIManager.Instance.ShowPanel(PanelIdEnum.MainBattleControl);
            switch (chapterType)
            {
                case ChapterType.MainTown:
                case ChapterType.Field:
                    UIManager.Instance.ShowPanel(PanelIdEnum.MainUIField);
                    break;
                case ChapterType.DuelWait:
                    UIManager.Instance.ShowPanel(PanelIdEnum.MainUIField);
                    UIManager.Instance.ShowPanel(PanelIdEnum.DuelScene);
                    break;
                case ChapterType.GuildWaitSingle:
                case ChapterType.GuildWaitDouble:
                    UIManager.Instance.ShowPanel(PanelIdEnum.MainUIField);
                    UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarMainUI);
                    break;
                default:
                    UIManager.Instance.ShowPanel(PanelIdEnum.MainUIBattle);
                    MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, PlayerAvatar.Player.max_hp);
                    break;
            }
        }

        private void CloseAll()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MainUIField);
            UIManager.Instance.ClosePanel(PanelIdEnum.MainUIBattle);
        }

        private void OnLeaveScene()
        {
            int preMapId = GameSceneManager.GetInstance().preMapID;
            int preInstanceId = map_helper.GetInstanceIDByMapID(preMapId);
            ChapterType chapterType = instance_helper.GetChapterType(preInstanceId);
            //Debug.LogError(string.Format("instanceId:{0} chapterType:{1}", preInstanceId, chapterType));
            CloseAll();
            CameraManager.GetInstance().ResetCameraInfo();
            switch (chapterType)
            {
                case ChapterType.MainTown:
                case ChapterType.GuildWaitSingle:
                case ChapterType.GuildWaitDouble:
                case ChapterType.DuelWait:
                    break;
                case ChapterType.Field:
                    OnLeaveCombatScene();
                    break;
                default:
                    OnLeaveCombatScene();
                    break;
            }
        }

        private void OnLeaveCombatScene()
        {
            ClientDropItemManager.Instance.ClearDropItemList();
            MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, PlayerAvatar.Player.max_hp);
            PlayerAvatar.Player.stateManager.SetState(state_config.AVATAR_STATE_DEATH, false);
        }
    }
}
