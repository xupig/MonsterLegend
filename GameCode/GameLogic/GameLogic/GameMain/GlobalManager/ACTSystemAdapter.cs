﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using GameMain.Entities;
using MogoEngine;
using GameData;
using Game.Asset;

namespace GameMain.GlobalManager
{
    public class ACTActorLoaderUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ACTActorData
    {
        public int actorID;
        public Action<ACTActor> callback;
        public ACTActorData(int id, Action<ACTActor> cb)
        {
            actorID = id;
            callback = cb;
        }
    }

    public class ACTActorLoader
    {
        Queue<ACTActorData> _actorIDList = new Queue<ACTActorData>(); 
        public ACTActorLoader()
        {
            MogoWorld.RegisterUpdate<ACTActorLoaderUpdateDelegate>("ACTActorData.Update", Update);
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("ACTActorData.Update", Update);
        }

        public void LoadActorNotImmediately(int actorID, Action<ACTActor> cb)
        {
            _actorIDList.Enqueue(new ACTActorData(actorID, cb));
        }

        int _loadingMax = 1;
        int _loadingCount = 0;
        public void LoadActor(int actorID, Action<ACTActor> cb)
        {
            var actorData = ACTRunTimeData.GetActorData(actorID);
            if (actorData == null)
            {
                LoggerHelper.Error("(@谢思宇)数据配置错误 actorID = " + actorID);
                if (cb != null) cb(null);
                return;
            }
            string modelPath = actorData.GetModelAssetPath();
            _loadingCount++;
            Game.Asset.ObjectPool.Instance.GetGameObjects(new string[] { modelPath }, (objs) =>
            {
                _loadingCount--;
                if (objs[0] == null)
                {
                    LoggerHelper.Error("CreateActor modelName:" + modelPath + " is null");
                    return;
                }
                var actor = ACTSystemDriver.GetInstance().CreateActor(objs[0], actorID);
                if (cb != null) cb(actor);
            });
        }

        void Update()
        {
            if (_loadingCount >= _loadingMax) return;
            if (_actorIDList.Count > 0)
            {
                var data = _actorIDList.Dequeue();
                LoadActor(data.actorID, data.callback);
            }
        }
    }

    public class ACTSystemAdapter
    {
        private static ACTActorLoader _actorLoader = null;
        private static ACTActorLoader actorLoader
        { 
            get{    
                if (_actorLoader == null)
                {
                    _actorLoader = new ACTActorLoader();
                }
                return _actorLoader;
            }
        }

        public static void CreateActor(int id, Action<ACTActor> cb, bool immediately = false)
        {
            if (immediately)
            {
                actorLoader.LoadActor(id, cb);
            }
            else
            {
                actorLoader.LoadActorNotImmediately(id, cb);
            }
        }

        public static string GetActorModelPath(int id)
        {
            var actorData = ACTRunTimeData.GetActorData(id);
            if (actorData == null)
            {
                return "";
            }
            return actorData.GetModelAssetPath();
        } 

        public static EntityCreature GetOwner(ACTActor actor)
        {
            uint entityID = actor.owner.GetActorOwnerID();
            EntityCreature entity = MogoWorld.GetEntity(entityID) as EntityCreature;
            return entity;
        }

        static ACTRuntimeLoader _loader = null;
        public static ACTRuntimeLoader GetRuntimeLoader()
        {
            if (_loader == null)
            {
                _loader = new ACTRuntimeLoader();
            }
            return _loader;
        }

        public static void SetActorCityState(ACTActor actor, bool inCity, int vocation,Action callBack = null)
        {
            if (actor == null) return;
            string controllerPath = string.Empty;
            if (!inCity)
            {
                controllerPath = role_data_helper.GetCombatControllerPath(vocation);
            }
            else
            {
                controllerPath = role_data_helper.GetCityControllerPath(vocation);
            }
            actor.equipController.equipWeapon.InCity(inCity);
            var animatorProxy = actor.gameObject.GetComponent<AnimatorProxy>();
            animatorProxy.SetController(controllerPath, () => {
                if (callBack != null) callBack(); 
            });
        }

        public static void SetUIActorController(ACTActor actor, bool inCity, int vocation, Action callBack = null)
        {
            if (actor == null) return;
            string controllerPath = string.Empty;
            if (!inCity)
            {
                controllerPath = role_data_helper.GetUIControllerPath(vocation);
            }
            else
            {
                controllerPath = role_data_helper.GetCityControllerPath(vocation);
            }
            actor.equipController.equipWeapon.InCity(inCity);
            var animatorProxy = actor.gameObject.GetComponent<AnimatorProxy>();
            animatorProxy.SetController(controllerPath, () =>
            {
                if (callBack != null) callBack();
            });
        }
    }

    public class ACTRuntimeLoader : IACTAssetLoader
    {
        public void GetObject(string strPath, ACT_VOID_OBJ callback)
        {
            strPath = transformPath(strPath);
            Game.Asset.ObjectPool.Instance.GetObject(strPath, (obj) =>
            {
                if (callback != null) callback(obj);
            });
        }

        public void GetObjects(string[] strNames, ACT_VOID_OBJS callback)
        {
            transformPaths(strNames);
            Game.Asset.ObjectPool.Instance.GetObjects(strNames, (objs) =>
            {
                if (callback != null) callback(objs);
            });
        }

        public void GetGameObject(string strPath, ACT_VOID_OBJ callback)
        {
            strPath = transformPath(strPath);
            Game.Asset.ObjectPool.Instance.GetGameObject(strPath, (obj) =>
            {
                if(callback != null) callback(obj);
            });
        }

        public void GetGameObjects(string[] strPaths, ACT_VOID_OBJS callback)
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                strPaths[i] = transformPath(strPaths[i]);
            }
            Game.Asset.ObjectPool.Instance.GetGameObjects(strPaths, (objs) =>
            {
                if (callback != null) callback(objs);
            });
        }

        public void PreloadAsset(string[] strPaths, ACT_VOID callback)
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                strPaths[i] = transformPath(strPaths[i]);
            }
            Game.Asset.ObjectPool.Instance.PreloadAsset(strPaths, () =>
            {
                if (callback != null) callback();
            });
        }

        public void ReleaseAsset(string strPath) 
        {
            if (string.IsNullOrEmpty(strPath)) return;
            ObjectPool.Instance.Release(strPath);
        }

        public void ReleaseAssets(string[] strPaths) 
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                ReleaseAsset(strPaths[i]);
            }
        }

        public string transformPath(string srcPath)
        {
            return srcPath.Substring("Assets/Resources/".Length);
        }

        void transformPaths(string[] srcPaths)
        {
            for (int i = 0; i < srcPaths.Length; i++)
            {
                srcPaths[i] = transformPath(srcPaths[i]);
            }
        }
    }

    public class ACTRuntimeSoundPlayer : IACTSoundPlayer
    {
        public void Play(AudioSource source, string strPath, float volume, bool loop = false)
        {
           strPath = strPath.Substring("Assets/Resources/".Length);
           GameMain.GlobalManager.SoundManager.GetInstance().Play(source,source.gameObject, strPath, volume, loop);
        }
    }
}
