﻿using Common.Data;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public abstract class RoleSkillData
    {
        public List<SkillEventData> skillEvents;

        public abstract void GetSkillData();
        

    }
}
