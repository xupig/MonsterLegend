﻿using ACTSystem;
using Common.Data;
using GameMain.CombatSystem;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.GlobalManager
{
    public class RoleSkillUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class RoleSkill
    {
        private ACTHandler _actHandler;
        private bool _isPlaying = false;
        private ACTActor _actor = null;
        public RoleSkillData skillData;
        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();
        protected float _pastTime = 0;
        public int vocation { get; set; }


        public RoleSkill(ACTActor actor, int vocation)
        {
			this._actHandler = new ACTHandler();
			_actHandler.qualitySettingValue = 2;
            _actHandler.priority = VisualFXPriority.H;
            this.vocation = vocation;
            this._actor = actor;
            MogoWorld.RegisterUpdate<RoleSkillUpdateDelegate>("RoleSkill.Update", Update);
        }

        private void Update()
        {
            DoingEvents();

        }

        public void Reset()
        {
            _pastTime = 0;

        }

        public void CastSpell()
        {
            Reset();
            GetSkillData();
            foreach (var item in skillData.skillEvents)
            {
                _skillEventList.Add(item);
                //LoggerHelper.Error("[NewRoleSkill:CastSpell]=>1____mainID: " + item.mainID + ",eventIdx: " + item.eventIdx + ",item.delay:   " + item.delay);
            }
            //
        }

        public virtual void GetSkillData()
        {
            if (skillData == null) return;
            skillData.GetSkillData();
        }

        private void DoingEvents()
        {
            if (_skillEventList.Count <= 0) return;
            _pastTime += Time.deltaTime;
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {

                var eventData = _skillEventList[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                if (eventData.delay < _pastTime)
                {
                    _actHandler.OnEvent(_actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }


        #region 数据读取
        public List<SkillEventData> GetSkillEvents()
        {
            List<SkillEventData> result = skillData.skillEvents;
            return result;
        }
        #endregion

    }
    
}
