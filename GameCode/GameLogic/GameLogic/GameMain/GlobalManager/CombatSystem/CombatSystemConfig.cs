﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace GameMain.CombatSystem
{
    //判断区域类型
    public enum TargetRangeType
    {
        SectorRange = 1,
        CircleRange = 2,
        ForwardRectangleRange = 3,
        ForwardAndBackSectorRange = 4,
    }

    //判断目标类型
    public enum TargetType
    {
        None = 0,   //None和Enemy都表示Enemy
        Enemy = 1,
        Friend = 2,
        EnemyAndFriend = 3,
        MySelf = 4,
        NoTarget = 5,
    }

    //判断目标类型
    public enum TargetFilterType
    {
        MinAngle = 0,
        Closest = 1,
        HaveBuffer = 2,
        RangeAndAngle = 3,
        Random = 4,
        Farthest = 5,
        EntityType = 6
    }

    //判断原点类型
    public enum OriginType
    { 
        Self = 0,
        Target = 1,
        Auto = 2,
        Custom = 3,
    }

    //判定攻击区域的时机
    public enum RegionJudgeTime
    {
        OnSkillActive = 1,
        OnActionActive = 2,
        OnActionJudge = 3,
        OnFixTime = 4,
    }

    //位移定位类型
    public enum MovementType
    { 
        None = 0,
        AccordingSelf = 1,
        AccordingTarget = 2,
        AccordingAuto = 3,
        AccordingJudgePoint = 4,
        AccordingJudgeTarget = 5,
    }

    //调整定位类型
    public enum AdjustType
    {
        None = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
        SIX = 6,
        SEVEN = 7,
        EIGHT = 8,
        NINE = 9,
    }

    //目标锁定
    public enum FaceLockMode
    {
        None = 0,
        LockOnSkillActive = 1,
        LockDuringSkill = 2,
    }

    public enum SearchTargetRepeatType
    { 
        Default = 0,
        ForEveryAction = 1,
        ForEveryActionUnique = 2,
    }

    public enum AttackType
    {
        ATTACK_HIT = 1,
        ATTACK_CRITICAL = 2,
        ATTACK_STRIKE = 3,
        ATTACK_MISS = 4,
    }

    public enum EarlyWarningType
    { 
        None = 0,
        All = 1,
        Self = 2
    }

    //部位解锁条件
    public enum PartUnlockType
    {
        HP = 1
    }

    public enum SkillDropItemOriginalPointType
    {
        ATTACKER = 0,       //施法者
        RANDOM_TARGET,      //随机判定目标
        RANDOM_TARGET_EDGE  //随机判定目标边缘，向施法者方向偏移hitRadius长度
    }

    public enum BuffRemoveMode
    {
        NONE = 0,
        DELETE_ON_DEATH
    }
}
