﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameData;
using MogoEngine.RPC;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.States;
using MogoEngine;
using Common.Global;
using Common.Structs.ProtoBuf;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.CombatSystem
{
    public class SkillManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillManager
    {
        private int BUFF_SKILL_DEFALUT = 0;

        public delegate void VoidDelegate();
        public VoidDelegate onPosChange;

        private EntityCreature  _owner;

        private Dictionary<int, float> _skillCD = new Dictionary<int, float>();

        private HashSet<int> _slottedSkillList = new HashSet<int>();
        public HashSet<int> slottedSkillList { get { return _slottedSkillList; } }

        private List<int> _learnedSkillList = new List<int>();

        private Dictionary<int, Dictionary<int, int>> _posToSkillIDListMap = new Dictionary<int,Dictionary<int,int>>();
        public Dictionary<int, Dictionary<int, int>> posToSkillIDListMap { get { return _posToSkillIDListMap; } }

        private HashSet<int> _skillBuffers = new HashSet<int>();

        private List<int> _curAllPosList = new List<int>();

        private Dictionary<int, int> _curPosSkillIDMap = new Dictionary<int, int>();

        //调整技能ID列表
        //private List<int> _adjustSkillIDList = new List<int>();
        //公共CD
        private float _commonCD = 0f;
        //造成公共CD的技能ID
        private int _curCommonCdSkillId = 0;
        private int _useSkillNum = 0;
        private bool _inServerMode = false;
        public bool inServerMode { get { return _inServerMode; } }

        private SkillSubjectManager _skillSubjectManager;
        public SkillSubjectManager skillSubjectManager { get { return _skillSubjectManager; } }

        private SkillSubjectManagerServer _skillSubjectManagerServer;
        public SkillSubjectManagerServer skillSubjectManagerServer { get { return _skillSubjectManagerServer; } }

        private bool _isSkillPreview = false;
		private bool _isDramaSkill = false;

        public SkillManager(EntityCreature ower)
        {
            _owner = ower;
            _isSkillPreview = ower is EntityPreview;
			_isDramaSkill = ower.isDramaEntity && !(ower is PlayerAvatar);
            SetInServerMode(GameSceneManager.GetInstance().IsServerMonster());
            MogoEngine.MogoWorld.RegisterUpdate<SkillManagerUpdateDelegate>("SkillManager.Update", Update);
            SetLearnedSkillList(_learnedSkillList);
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("SkillManager.Update", Update);
            if (_skillSubjectManager !=null) _skillSubjectManager.Release();
            if (_skillSubjectManagerServer != null) _skillSubjectManagerServer.Release();
        }

        public void SetSlottedSkillList(List<int> slottedList)
        {
            for (int i = 0; i < slottedList.Count; i++)
            { 
                var skillID = slottedList[i];
                _slottedSkillList.Add(skillID);
            }
        }

        public HashSet<int> GetSlottedSkillList()
        {
            return _slottedSkillList;
        }

        public void SetLearnedSkillList(List<int> learnedList)
        {
            _learnedSkillList = learnedList;
            try
            {
                ResetPosIDMap(_learnedSkillList);
            }
            catch (Exception ex) { LoggerHelper.Error(ex.Message); }
        }

        public void AddLearnedSkillList(List<int> learnedList)
        {
            if (_learnedSkillList == null)
            {
                _learnedSkillList = learnedList;
            }
            else
            {
                for (int i = 0; i < learnedList.Count; i++)
                {
                    int newID = learnedList[i];
                    if (!_learnedSkillList.Contains(newID))
                    {
                        _learnedSkillList.Add(newID);
                    }
                }
            }
            try
            {
                ResetPosIDMap(_learnedSkillList);
            }
            catch (Exception ex) { LoggerHelper.Error(ex.Message); }
        }

        public List<int> GetLearnedSkillList()
        {
            return _learnedSkillList;
        }

        private void ResetPosIDMap(List<int> skillIDList)
        {
            _posToSkillIDListMap.Clear();
            _skillBuffers.Clear();
            _curAllPosList.Clear();
            _curPosSkillIDMap.Clear();
            for (int i = 0; i < skillIDList.Count; i++)
            {
                var skillID = skillIDList[i];
                var skillData = CombatLogicObjectPool.GetSkillData(skillID);//XMLManager.spell[skillID];
                if (!_posToSkillIDListMap.ContainsKey(skillData.pos))
                {
                    _posToSkillIDListMap.Add(skillData.pos, new Dictionary<int, int>());
                }
                var bufferID = Mathf.Abs(skillData.showType);
                if (!_posToSkillIDListMap[skillData.pos].ContainsKey(bufferID))
                {
                    _posToSkillIDListMap[skillData.pos].Add(bufferID, skillID);
                }
                if (!_skillBuffers.Contains(bufferID))
                { 
                    _skillBuffers.Add(bufferID); 
                }
                if (bufferID == 0)
                {
                    _curPosSkillIDMap[skillData.pos] = skillID;
                }
                if (!_curAllPosList.Contains(skillData.pos))
                {
                    _curAllPosList.Add(skillData.pos);
                }
            }
            OnPosChange();
        }

        public void AddSkillActionToSkillSubject(int skillID, int skillActionID, Matrix4x4 ownerWorldMatrixOnSkillActive, uint mainTargetID)
        {
            if (!_inServerMode)
            {
                _skillSubjectManager.CreateSkillAction(skillID, skillActionID, ownerWorldMatrixOnSkillActive, mainTargetID);
            } 
        }

        public void BreakCurSkill()
        {
            if (!_inServerMode)
            {
                _skillSubjectManager.BreakCurSkill();
            }
            else {
                _skillSubjectManagerServer.BreakCurSkill();
            }
        }

        public bool PlaySkill(int skillID)
        {
            int errorID = CanDo(skillID);
            if (errorID > 0)
            {
                return false;
            }
            if (_learnedSkillList.Contains(skillID))
            {
                this._useSkillNum++;
            }
            if (_inServerMode)
            {
                _skillSubjectManagerServer.AddSkill(skillID);
            }
            else 
            {
                _skillSubjectManager.AddSkill(skillID);
            }
            return true;
        }

        public int GetSkillIDByIndex(int index)
        {
            index = index - 1;
            if (index < _learnedSkillList.Count)
            {
                return _learnedSkillList[index];
            }
            return 0;
        }

        public void OnBufferChange(int bufferID, bool isAdded = false)
        {
            if (_skillBuffers.Contains(bufferID))
            {
                for (int i = 0; i < _curAllPosList.Count; i++)
                {
                    var pos = _curAllPosList[i];
                    if (_posToSkillIDListMap.ContainsKey(pos) && _posToSkillIDListMap[pos].ContainsKey(bufferID))
                    {
                        _curPosSkillIDMap[pos] = GetSkillIDByPos(pos);
                    }
                }
                OnPosChange();
            }
        }

        public Dictionary<int, int> GetCurPosSkillIDMap()
        {
            return _curPosSkillIDMap;
        }

        public int GetSkillIDByPos(int pos)
        {
            int canPlaySkill = 0;
            while(_posToSkillIDListMap.ContainsKey(pos))
            {
                var buffList = _owner.bufferManager.GetCurBufferIDList();
                var buffSkillMap = _posToSkillIDListMap[pos];
                if (!buffSkillMap.ContainsKey(BUFF_SKILL_DEFALUT))
                {
                    LoggerHelper.Error(string.Format("{0}在Pos{1}上没有对应可用的默认技能!", this._owner.id, pos));
                    break;
                }
                canPlaySkill = buffSkillMap[BUFF_SKILL_DEFALUT];
                var canPlaySkillData = CombatLogicObjectPool.GetSkillData(canPlaySkill);
                for (int i = 0; i < buffList.Count; i++)
                {
                    var bufferID = buffList[i];
                    if (buffSkillMap.ContainsKey(bufferID))
                    {
                        var skillID = buffSkillMap[bufferID];
                        var skillData = CombatLogicObjectPool.GetSkillData(skillID);
                        if (skillData.showPirority > canPlaySkillData.showPirority)
                        {
                            canPlaySkill = skillID;
                            canPlaySkillData = CombatLogicObjectPool.GetSkillData(canPlaySkill);
                        }
                    }
                }
                break;
            }
            return canPlaySkill;
        }

        public int PlaySkillByPos(int pos)
        {
            int canPlaySkill = GetSkillIDByPos(pos);
            if (canPlaySkill > 0)
            {
//                Debug.LogError("[SkillManager:PlaySkillByPos]=>pos:    " + pos + ",canPlaySkill:    " + canPlaySkill);
                return PlaySkill(canPlaySkill)?canPlaySkill:0;
            }
            return 0;
        }

        private void SetInServerMode(bool state)
        {
            _inServerMode = (_isSkillPreview || _isDramaSkill) ? false : state;
            if (_inServerMode)
            {
                if (_skillSubjectManagerServer == null) _skillSubjectManagerServer = new SkillSubjectManagerServer(this._owner);
            }
            else
            {
                if (_skillSubjectManager == null) _skillSubjectManager = new SkillSubjectManager(this._owner);
            }
        }

        private void CheckServerMode()
        {
            if (_isSkillPreview || _isDramaSkill)
            {
                return;
            }
            if (_inServerMode != GameSceneManager.GetInstance().IsServerMonster())
            {
                SetInServerMode(GameSceneManager.GetInstance().IsServerMonster());
            }
        }

        int _spaceTime = 0;
        private void Update()
        {
            _spaceTime--;
            if (_spaceTime > 0) { return; }
            _spaceTime = 30; //这里要修改设置方式，不能每帧都调。
            CheckServerMode();
        }

        

        public int CanDo(int skillID)
        {
            int result = 0;
            while (true)
            {
                if (IsInCD(skillID) && !_isSkillPreview && !_isDramaSkill) { result = (int)error_code.ERR_SPELL_IN_CD; break; }
                if (IsInCommonCD(skillID)) { result = (int)error_code.ERR_SPELL_IN_COMMON_CD; break; }
                if (!EPEnough(skillID)) { result = (int)error_code.ERR_NOT_ENOUGH_SPELL_ENERGY; break; }
				if (!_owner.stateManager.CanDO(CharacterAction.ATTACK_ABLE)) 
				   { result = (int)error_code.ERR_SPELL_PREVIA_NOT_CAST; break; }
                break;
            }
            return result;
        }

        public void SetCommonCD(int skillID, int cd)
        {
            this._commonCD = RealTime.time + cd * 0.001f;
            _curCommonCdSkillId = skillID;
            OnPosChange();
        }

        public int GetCurCommonCdSkillId()
        {
            return _curCommonCdSkillId;
        }

        public float GetCommonCD()
        {
            return this._commonCD;
        }

        public Dictionary<int, float> GetSkillCDs()
        {
            return _skillCD;
        }

        public int GetCD(int skillID)
        {
            int result = 0;
            if (_skillCD.ContainsKey(skillID))
            {
                result = (int)(Mathf.Max(_skillCD[skillID] - RealTime.time, 0) * 1000);
            }
            return result;
        }

        public float GetCDEndTime(int skillID)
        {
            float result = 0;
            if (_commonCD - RealTime.time > 0)
            {
                result = -_commonCD;
            }
            if (_skillCD.ContainsKey(skillID) && _skillCD[skillID] > RealTime.time)
            {
                result = _skillCD[skillID];
            }
            return result;
        }

        public void SetCD(int skillID, int cd)
        {
            _skillCD[skillID] = RealTime.time + cd * 0.001f;
            OnPosChange();
        }

        public void ChangeCD(int skillID, int cd)
        {
            SetCD(skillID, GetCD(skillID) + cd);
        }

        public void ChangeSkillGroupCD(int skillGroup, int cd)
        {
            List<int> skillGroupList = skill_helper.GetSpellGroupList(skillGroup);
            if (skillGroupList == null)
            {
                return;
            }
            for (int i = 0; i < skillGroupList.Count; ++i)
            {
                if (_learnedSkillList.Contains(skillGroupList[i]))
                {
                    ChangeCD(skillGroupList[i], cd);
                }
            }
        }

        public bool IsInCommonCD(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.cd.Count >= 3 && skillData.cd[2] > 0) return false;
            return (this._commonCD - RealTime.time) > 0;
        }

        public bool IsInCD(int skillID)
        {
            bool result = false;
            if (_skillCD.ContainsKey(skillID))
            {
                result = (_skillCD[skillID] - RealTime.time) > 0;
            }
            return result;
        }

        public void ClearAllCD()
        {
            _skillCD.Clear();
            _commonCD = 0;
            OnPosChange();
        }

        public bool EPEnough(int skillID)
        {
            bool result = true;
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (_owner is EntityAvatar && skillData.useCosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
            {
                return _owner.cur_ep >= skillData.useCosts[public_config.ITEM_SPECIAL_SPELL_ENERGY];
            }
            return result;
        }

        //调整技能ID列表
        private List<int> _adjustSkillIDList = new List<int>();
        public void AddAdjustSkillID(int skillID)
        {
            _adjustSkillIDList.Add(skillID);
        }

        public void RemoveAdjustSkillID(int skillID)
        {
            if (_adjustSkillIDList.Contains(skillID))
            {
                _adjustSkillIDList.Remove(skillID);
            }
        }

        public List<int> GetCurAdjustSkillIDList()
        {
            return _adjustSkillIDList;
        }

        //调整技能行为ID列表
        private List<int> _adjustSkillActionIDList = new List<int>();
        public void AddAdjustSkillActionID(int skillActionID)
        {
            _adjustSkillActionIDList.Add(skillActionID);
        }

        public void RemoveAdjustSkillActionID(int skillActionID)
        {
            if (_adjustSkillActionIDList.Contains(skillActionID))
            {
                _adjustSkillActionIDList.Remove(skillActionID);
            }
        }

        public List<int> GetCurAdjustSkillActionIDList()
        {
            return _adjustSkillActionIDList;
        }

        public void ClearUseSkillNum()
        {
            this._useSkillNum = 0;
        }

        public int GetUseSkillNum()
        {
            return _useSkillNum;
        }

        public void OnPosChange()
        {
            if (onPosChange != null) onPosChange();
        }

        #region 服务器同步技能

        private float _minFixDis = 0.5f;
        public void FixPosition(float x, float y, float z)
        {
            var targetPosition = new Vector3();
            targetPosition.x = x;
            targetPosition.y = this._owner.position.y;
            targetPosition.z = z;
            var dis = (targetPosition - this._owner.position).magnitude;
            if (dis > _minFixDis)
            {
                this._owner.Teleport(targetPosition);
            }
        }

        public void PlaySkillByServer(int skillID, Int16 x, Int16 z, byte face, UInt32 targetID)
        {
            if (_skillSubjectManagerServer != null)
            {
                FixPosition(x * 0.01f, this._owner.position.y, z * 0.01f);
                this._owner.face = face;
                _skillSubjectManagerServer.AddSkill(skillID, targetID);
            }
        }

        public void ActiveSkillActionByServer(PbSpellAction skillActionData)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.ActiveSkillActionByServer(skillActionData);
            }
        }

        public void JudgeSkillActionByServer(UInt16 skillID, UInt16 skillActionID, PbSpellDamageInfo skillDamageInfo)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.JudgeSkillActionByServer(skillID, skillActionID, skillDamageInfo);
            }
        }

        public void OnSkillPlayByServer(UInt16 skillID, UInt32 targetID)
        {
            if (_skillSubjectManagerServer != null)
            {
                var skillSubject = _skillSubjectManagerServer.GetCurSkillSubjectByID(skillID);
                if (skillSubject == null)
                {
                    if (Application.isEditor) LoggerHelper.Warning("skillSubject is null. " + skillID);
                    return;
                }
                skillSubject.CastSpellResp(targetID);
                AdjustTools.AdjustSkill(skillSubject, GetCurAdjustSkillIDList());
                var cd = skillSubject.GetCD();
                SetCD(skillID, cd[0]);
            }
        }

        public void OnFollowerSkillPlayByServer(UInt16 skillID, Int16 x, Int16 z, byte face, UInt32 targetID)
        {
            if (_skillSubjectManagerServer != null)
            {
                var skillSubject = _skillSubjectManagerServer.GetCurSkillSubjectByID(skillID);
                if (skillSubject == null)
                {
                    if (Application.isEditor) LoggerHelper.Warning("OnFollowerSkillPlayByServer skillSubject is null." + skillID);
                    return;
                }
                FixPosition(x * 0.01f, this._owner.position.y, z * 0.01f);
                this._owner.face = face;
                skillSubject.FollowerCastSpellResp(targetID);
                AdjustTools.AdjustSkill(skillSubject, GetCurAdjustSkillIDList());
                var cd = skillSubject.GetCD();
                SetCD(skillID, cd[0]);
            }
        }

        public void OnSetActionOriginByServer(PbSpellActionOrigin spellActionOrigin)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.OnSetActionOriginByServer(spellActionOrigin);
            }
        }

        #endregion
    }
}
