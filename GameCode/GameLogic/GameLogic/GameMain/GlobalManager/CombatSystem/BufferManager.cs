﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.RPC;
using GameMain.Entities;
using Common.Data;
using Common.Structs.ProtoBuf;
using UnityEngine;
using GameMain.GlobalManager;
using Common.ServerConfig;

namespace GameMain.CombatSystem
{
    public class BufferManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    enum CreateRemoveMode
    { 
       removeThisAndCondition = 1,
       keepThisAndremoveCondition = 2,
       removeThisAndkeepCondition = 3,
       keepThisAndCondition = 4,
    }

    enum CreateTimeMode
    {
        defaultTime = 1,
        ConditionRemainTime = 2,
        defaultTimeAddConditionRemainTime = 3,
        buffTime = 4,
        buffTimeAndConditionRemainTime = 5,
    }

    public class BufferManager
    {
        private EntityCreature _owner;

        private Dictionary<int, BufferSubject> _bufferSubjects = new Dictionary<int, BufferSubject>();
        private List<int> _bufferSubjectsList = new List<int>();
        private bool _isClearingBuffList = false; 

        public BufferManager(EntityCreature owner)
        {
            _owner = owner;
            MogoEngine.MogoWorld.RegisterUpdate<BufferManagerUpdateDelegate>("BufferManager.Update", Update);
        }

        public void OnActorLoaded()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.PlayBuffFX();
                }
            }
        }

        public void OnActorBeDestroyed()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.StopBuffFX();
                }
            }
        }

        public void PlaySpecialFX()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.PlaySpecialFX();
                }
            }
        }

        public void RemoveBuffsOnDeath()
        {
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var bufferID = _bufferSubjectsList[i];
                var bufferSubject = _bufferSubjects[bufferID];
                if (!bufferSubject.isClientBuff) continue;
                if (bufferSubject.bufferData.removeMode == (int)BuffRemoveMode.DELETE_ON_DEATH)
                {
                    RemoveBuffer(bufferID);
                }
            }
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("BufferManager.Update", Update);
            OnActorBeDestroyed();
        }

        public void ResetServerBuffList(PbBuffList buffList)
        {
            ClearBufferSubjects();
            var bList = buffList.buffList;
            for (int i = 0; i < bList.Count; i++)
            {
                var buffData = bList[i];
                AddBuffer((int)buffData.buff_id, (float)(buffData.remain_tick), false, false);
            }
        }

        public void ClearBufferSubjects()
        {
            _isClearingBuffList = true;
            List<int> removeList = new List<int>();
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                removeList.Add(en.Current.Key);
            }
            for (int i = 0; i < removeList.Count; i++)
            {
                RemoveBuffer(removeList[i]);
            }
            _isClearingBuffList = false;
        }

        private int _spaceTime = 0;
        private List<int> _removeList = new List<int>();
        private void Update()
        {
            //_spaceTime++;
            //if (_spaceTime < 10) return;
            //_spaceTime = 0;
            if (_bufferSubjects.Count == 0) return;
            
            _removeList.Clear();
            /*
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var result = en.Current.Value.Update();
                if (!result)
                {
                    _removeList.Add(en.Current.Key);
                }
            }
            for (int i = 0; i < _removeList.Count; i++)
            {
                RemoveBuffer(_removeList[i]);
            }
             */
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var bufferID = _bufferSubjectsList[i];
                var bufferSubject = _bufferSubjects[bufferID];
                var result = bufferSubject.Update();
                if (!result)
                {
                    _removeList.Add(bufferID);
                }
            }
            for (int i = 0; i < _removeList.Count; i++)
            {
                RemoveBuffer(_removeList[i]);
            }
        }

        public bool CanAddBuffer(int bufferID)
        {
            if (_isClearingBuffList)    //移除全部buff时不可加buff操作
            {
                return false;
            }
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            var excludeBuffs = bufferData.excludeBuffs;
            for (int i = 0; i < excludeBuffs.Count; i++)
            {
                if (_bufferSubjects.ContainsKey(excludeBuffs[i])) return false;
            }

            var excludeBuffsGroup = bufferData.excludeBuffsGroup;
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                var curBufferData = CombatLogicObjectPool.GetBufferData(_bufferSubjectsList[i]);
                if (excludeBuffsGroup.Contains(curBufferData.group))
                {
                    return false;
                }
                if (curBufferData.group != 0 && curBufferData.group == bufferData.group)
                {
                    if (curBufferData.groupLevel <= bufferData.groupLevel)
                    {
                        RemoveBuffer(curBufferID);
                    }
                    else  //curBufferData.groupLevel <= bufferData.groupLevel
                    {
                        return false;
                        //break;    
                    }
                }
            }   
            return true;
        }

        private void CheckReplaceBuffer(int bufferID)
        {
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            var replaceBuffsGroup = bufferData.replaceBuffsGroup;
            var replaceBuffs = bufferData.replaceBuffs;
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                var curBufferData = CombatLogicObjectPool.GetBufferData(_bufferSubjectsList[i]);
                if (replaceBuffs.Contains(curBufferID) || replaceBuffsGroup.Contains(curBufferData.group))
                {
                    RemoveBuffer(curBufferID);
                }
            }
        }

        private bool CreateBuffersByCondition(int bufferID, float duration)
        {
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            bool keepOnCurrentBuffer = true;
            List<int> _removeConditionBufferList = new List<int>();
            Dictionary<int, float> _addCreatedBufferDict = new Dictionary<int, float>();
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                if (bufferData.conditionBuffsMap.ContainsKey(curBufferID))
                {
                    var createData = bufferData.conditionBuffsMap[curBufferID];
                    var curBufferData = CombatLogicObjectPool.GetBufferData(curBufferID);
                    int delay = curBufferData.delayTime;
                    switch ((CreateTimeMode)createData.createTimeMode)
                    {
                        case CreateTimeMode.ConditionRemainTime:
                            delay = (int)_bufferSubjects[curBufferID].remainTime * 1000;
                            break;
                        case CreateTimeMode.defaultTimeAddConditionRemainTime:
                            delay += (int)_bufferSubjects[curBufferID].remainTime * 1000;
                            break;
                        case CreateTimeMode.buffTime:
                            delay = (int)duration;
                            break;
                        case CreateTimeMode.buffTimeAndConditionRemainTime:
                            delay = (int)(duration + _bufferSubjects[curBufferID].remainTime * 1000);
                            break;
                        default:
                            break;
                    }
                    switch((CreateRemoveMode)createData.createRemoveMode)
                    {
                        case CreateRemoveMode.removeThisAndCondition:
                            _removeConditionBufferList.Add(curBufferID);
                            keepOnCurrentBuffer = false;
                            break;
                        case CreateRemoveMode.keepThisAndremoveCondition:
                            _removeConditionBufferList.Add(curBufferID);
                            break;
                        case CreateRemoveMode.removeThisAndkeepCondition:
                            keepOnCurrentBuffer = false;
                            break;
                        case CreateRemoveMode.keepThisAndCondition:
                            break;
                    }
                    if (!_addCreatedBufferDict.ContainsKey(createData.bufferID))
                    {
                        _addCreatedBufferDict.Add(createData.bufferID, delay);
                    }
                    else
                    {
                        _addCreatedBufferDict[createData.bufferID] = delay;
                    }
                }
            }
            for (int i = 0; i < _removeConditionBufferList.Count; ++i)
            {
                RemoveBuffer(_removeConditionBufferList[i]);
            }
            foreach (KeyValuePair<int, float> createBuffer in _addCreatedBufferDict)
            {
                AddBuffer(createBuffer.Key, createBuffer.Value, true, false);
            }
            return keepOnCurrentBuffer;
        }

        public bool AddBuffer(int bufferID, float duration = -1f, bool isClient = true, bool conditionCreate = true, bool canReplaceByServer = true)
        {
            if (duration == -2f)    //配置时间为-2即为删除该buff
            {
                RemoveBuffer(bufferID);
                return false;
            }
            if (!CanAddBuffer(bufferID)) { return false; }
            CheckReplaceBuffer(bufferID);
            if (conditionCreate && !CreateBuffersByCondition(bufferID, duration)) { return false; }
            if (!TryRemoveOldBuffer(bufferID, isClient)) return false;
            var buffer = CombatLogicObjectPool.CreateBufferSubject(bufferID);
            buffer.SetOwner(_owner);
            buffer.isClientBuff = isClient;
            buffer.canReplaceByServer = canReplaceByServer;
            _bufferSubjects.Add(bufferID, buffer);
            _bufferSubjectsList.Add(bufferID);
            buffer.Start(duration);
            this._owner.skillManager.OnBufferChange(bufferID, true);//设置按钮位置
            return true;
        }

        //部分buff会被标记不容许服务器同ID的buff所顶替，这个会尝试做个顶替的操作
        public bool TryRemoveOldBuffer(int newBufferID, bool isClient = false)
        {
            if (_bufferSubjects.ContainsKey(newBufferID))
            {
                var bufferSubject = _bufferSubjects[newBufferID];
                if (!bufferSubject.canReplaceByServer && !isClient) return false;
                RemoveBuffer(newBufferID);
            }
            return true;
        }

        public void RemoveBuffer(int bufferID) //暂时假设同一个buffID只有有一个实例
        {
            if (_bufferSubjects.ContainsKey(bufferID)) 
            {
                var buff = _bufferSubjects[bufferID];
                _bufferSubjects.Remove(bufferID);
                _bufferSubjectsList.Remove(bufferID);
                if (!buff.IsOver())
                {
                    buff.Stop();
                }
                else
                {
                    buff.End();
                }
                this._owner.skillManager.OnBufferChange(bufferID, false);
            }
        }

        public List<int> GetCurBufferIDList()
        {
            return _bufferSubjectsList;
        }

        public bool ContainsBuffer(int bufferID)
        {
            return _bufferSubjects.ContainsKey(bufferID);
        }

        public float GetBufferRemainTime(int bufferID)
        {
            return _bufferSubjects.ContainsKey(bufferID)?_bufferSubjects[bufferID].remainTime:0f;
        }

        public void OnBuffHpChange(int hp)
        {
            if (_owner is PlayerAvatar)
            {
                PerformaceManager.GetInstance().CreateDamageNumber(_owner.actor.position, hp, AttackType.ATTACK_HIT, true);
            }
            if (hp > 0)
            {
                _owner.OnAddStatisticsDamage(public_config.MISSION_ACTION_GET_CURE_ALL, hp);
            }
        }
    }
}
