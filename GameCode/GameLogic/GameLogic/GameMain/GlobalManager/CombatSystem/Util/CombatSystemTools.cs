﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain.Entities;
using Common.Data;
using MogoEngine;
using MogoEngine.RPC;
using Common.ServerConfig;
using GameMain.GlobalManager;

namespace GameMain.CombatSystem
{
    public static class CombatSystemTools
    {
        public static EntityCreature GetCreatureByID(uint entityID)
        {
            var entity = MogoWorld.GetEntity(entityID);
            if (entity == null) return null;
            return entity as EntityCreature;
        }

        public static AttackType DamageMode2AttackType(UInt32 mode)
        {
            if ((mode & 1 << public_config.SPELL_ATTACK_CRITICAL) > 0)
            {
                return AttackType.ATTACK_CRITICAL;
            }
            if ((mode & 1 << public_config.SPELL_ATTACK_STRIKE) > 0)
            {
                return AttackType.ATTACK_STRIKE;
            }
            if ((mode & 1 << public_config.SPELL_ATTACK_HIT) > 0)
            {
                return AttackType.ATTACK_HIT;
            }
            return AttackType.ATTACK_MISS;
        }

        public static float CalculateDistanceBetween(EntityCreature creature1, EntityCreature creature2)
        {
            var pos1 = creature1.GetPosition();
            pos1.y = 0;
            var pos2 = creature2.GetPosition();
            pos2.y = 0;
            return Mathf.Abs((pos1 - pos2).magnitude);
        }

        public static bool CanShowDamage(EntityCreature attacker, EntityCreature target)
        {
            uint playerID = PlayerAvatar.Player.id;
            if (target.id == playerID || attacker.id == playerID)
            {
                return true;
            }
            uint petID = PlayerPetManager.GetInstance().curPetEntityID;
            if (target.id == petID || attacker.id == petID)
            {
                return true;
            }
            //
            if (attacker is EntityVehicle)
            {
                if ((attacker as EntityVehicle).driver.id == playerID)
                {
                    //Debug.LogError("[CombatSystemTools:CanShowDamage]=>playerID:    " + playerID);
                    return true;
                }

            }


            return false;
        }
    }
}
