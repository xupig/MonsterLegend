﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain.Entities;
using Common.Data;
using MogoEngine;
using MogoEngine.RPC;

namespace GameMain.CombatSystem
{
    public enum EntityFilterType
    {
        SKILL_SUBJECT_FILTER = 1,
        SKILL_ACTION_FILTER = 2,
    }

    public class EntityFilter
    {

        static private Dictionary<EntityFilterType, Color> _filterTypeColorMap 
            = new Dictionary<EntityFilterType, Color>() 
            {
                {EntityFilterType.SKILL_SUBJECT_FILTER, Color.green},
                {EntityFilterType.SKILL_ACTION_FILTER, Color.red}
            };

        #region 范围判断

        static public List<uint> GetEntitiesByRangeType(Matrix4x4 srcWorldMatrix, TargetRangeType targetRangeType, List<int> targetRangeParam, List<int> originAdjust, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            float offsetX = 0;
            float offsetY = 0;
            float angleOffset = 0;
            if (originAdjust != null && originAdjust.Count>0)
            {
                offsetX = originAdjust[0] * 0.01f;
                offsetY = originAdjust[1] * 0.01f;
                if (originAdjust.Count>=3) 
                { 
                    angleOffset = originAdjust[2]; 
                }
            }
            List<uint> entities = new List<uint>();
            switch (targetRangeType)
            {
                case TargetRangeType.CircleRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        entities = EntityFilter.GetEntitiesInCircleRange(srcWorldMatrix, radius, offsetX, offsetY, 0, filterType);
                    }
                    break;
                case TargetRangeType.SectorRange:
                    if (targetRangeParam.Count >= 2)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        float angle = targetRangeParam[1];
                        entities = EntityFilter.GetEntitiesInSectorRange(srcWorldMatrix, radius, angle, offsetX, offsetY, angleOffset, filterType);
                    }
                    break;
                case TargetRangeType.ForwardRectangleRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float length = targetRangeParam[0] * 0.01f;
                        float width = targetRangeParam[1] * 0.01f;
                        entities = EntityFilter.GetEntitiesInRectangleRange(srcWorldMatrix, length, width, offsetX, offsetY, angleOffset, filterType);
                    }
                    break;
                case TargetRangeType.ForwardAndBackSectorRange:
                    if (targetRangeParam.Count >= 2)
                    {
                        //未实现
                    }
                    break;
            }
            return entities;
        }

        static public List<uint> GetEntitiesInCircleRange(Matrix4x4 srcWorldMatrix, float radius, float offsetX = 0, float offsetY = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            //Debug.LogError("GetEntitiesInCircleRange:");
            //Matrix4x4 ltwM = transform.localToWorldMatrix;
            List<uint> list = new List<uint>();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, 0, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            var enumerator = MogoWorld.Entities.GetEnumerator();
            while (enumerator.MoveNext())
            {
                EntityCreature entity = enumerator.Current.Value as EntityCreature;
                if (entity == null) continue;
				posi.y = entity.position.y;
                if ((posi - entity.position).magnitude > (radius + entity.hitRadius)) continue;
                list.Add(entity.id);
            }
            DrawCircleRange(posi, radius, filterType);
            return list;
        }

        static public List<uint> GetEntitiesInSectorRange(Matrix4x4 srcWorldMatrix, float radius, float angle = 180f, float offsetX = 0, float offsetY = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            //Debug.LogError("GetEntitiesInSectorRange:");
            //Matrix4x4 ltwM = transform.localToWorldMatrix;
            List<uint> list = new List<uint>();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, 0, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            var enumerator = MogoWorld.Entities.GetEnumerator();
            float fixAngle = angle * 0.5f;
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            Vector3 forward = srcWorldMatrix.forward();
            forward = matrixAngleOffset.MultiplyVector(forward).normalized;
            while (enumerator.MoveNext())
            {
                EntityCreature entity = enumerator.Current.Value as EntityCreature;
                if (entity == null) continue;
                var eTransform = entity.GetTransform();
                if (entity.GetTransform() == null)
                {
                    continue;
                }
                float entityRadius = entity.hitRadius;
				posi.y = eTransform.position.y;
                if ((posi - eTransform.position).magnitude > radius + entityRadius) continue;
                //得到切线与（目标物体到人物线）的夹角a
                float a = Mathf.Asin(entityRadius / (eTransform.position - posi).magnitude);
                //得到目标点与人物正前方的夹角b
                float b = Vector3.Angle((eTransform.position - posi), forward);
                //判断b - a 是否在 angle/2内
                if ((b - a) > fixAngle) continue;
                list.Add(entity.id);
            }
            DrawSectorRange(posi, forward, radius, angle, filterType);
            return list;
        }

        static public List<uint> GetEntitiesInRectangleRange(Matrix4x4 srcWorldMatrix, float length, float width, float offsetX = 0, float offsetY = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            
            List<uint> list = new List<uint>();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, 0, offsetX, 1));
            //Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 1, 0), Vector3.one);
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            var enumerator = MogoWorld.Entities.GetEnumerator();
            var right = srcWorldMatrix.right().normalized;
            var forward = srcWorldMatrix.forward().normalized;
            var rightBottomPoint = posi + right * (width * 0.5f);
            rightBottomPoint.y = posi.y;
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            forward = matrixAngleOffset.MultiplyVector(forward).normalized;
            right = matrixAngleOffset.MultiplyVector(right).normalized;
            while (enumerator.MoveNext())
            {
                EntityCreature entity = enumerator.Current.Value as EntityCreature;
                if (entity == null) continue;
                float entityRadius = entity.hitRadius;
                var entityPos = entity.position;
                entityPos.y = posi.y;
                var dir = entityPos - rightBottomPoint;
                var dotL = Vector3.Dot(dir, forward);
                var dotW = Vector3.Dot(dir, -right);
                if (dotL < -entityRadius || dotL > length + entityRadius
                    || dotW < -entityRadius || dotW > width + entityRadius) continue;
                list.Add(entity.id);
            }
            DrawRectangleRange(posi, forward, right, length, width, filterType);
            return list;
        }

        #endregion

        #region 范围显示

        static private float DEBUG_SHOW_TIME = 1;
        static public bool ShowDebugRange = false;
        static public void DrawCircleRange(Vector3 origin, float radius, EntityFilterType filterType)
        {
            if (!Application.isEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            Vector3 direction = new Vector3(0,0,1);
            Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 1, 0), Vector3.one);
            for (int i = 0; i < 360; i++)
            {
                direction = m1.MultiplyVector(direction).normalized * radius;
                Debug.DrawLine(origin, origin + direction, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
            }
        }

        static public void DrawSectorRange(Vector3 origin, Vector3 forward, float radius, float angle, EntityFilterType filterType)
        {
            if (!Application.isEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            Vector3 direction1 = forward;
            Vector3 direction2 = forward;
            Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 1, 0), Vector3.one);
            Matrix4x4 m2 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, -1, 0), Vector3.one);
            for (int i = 0; i < angle/2; i++)
            {
                direction1 = m1.MultiplyVector(direction1).normalized * radius;
                Debug.DrawLine(origin, origin + direction1, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
                direction2 = m2.MultiplyVector(direction2).normalized * radius;
                Debug.DrawLine(origin, origin + direction2, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
            }
        }

        static public void DrawRectangleRange(Vector3 origin, Vector3 forward, Vector3 right, float length, float width, EntityFilterType filterType)
        {
            if (!Application.isEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            right = right.normalized;
            forward = forward.normalized;
            var rightBottomPoint = origin + right * (width / 2f);
            rightBottomPoint.y = origin.y;
            var leftBottomPoint = origin - right * (width / 2f);
            leftBottomPoint.y = origin.y;
            var space = 0.1f;
            var count = (int)(width/space);
            var newOrigin = leftBottomPoint;
            for (int i = 0; i < count; i++)
            {
                Debug.DrawLine(newOrigin, newOrigin + forward * length, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
                newOrigin = newOrigin + right * space;
                
            }
            Debug.DrawLine(rightBottomPoint, rightBottomPoint + forward * length, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
        }

        #endregion
    }
}
