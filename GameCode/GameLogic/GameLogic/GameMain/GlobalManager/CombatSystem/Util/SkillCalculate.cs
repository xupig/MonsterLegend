﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

using MogoEngine.RPC;
using GameMain.Entities;
using Common.Data;
using Common.ServerConfig;
using Common.States;

namespace GameMain.CombatSystem
{
    public struct SkillDamage
    {
        public int value;
        public UInt32 mode;
    }

    public struct SkillTypeDamage
    {
        public float curDmg;
        public byte damageFlag;
        public SkillTypeDamage(float curDmg, byte damageFlag)
        {
            this.curDmg = curDmg;
            this.damageFlag = damageFlag;
        }
    }

    public class SkillCalculate
    {
        public static SkillDamage CalculateDamage(EntityCreature attacker, EntityCreature defender, int skillID, SkillAction skillAction, int targetCnt)
        {
            var result = new SkillDamage();
            List<int> damageTypes = skillAction.GetDmgTypes();
            if (damageTypes.Count == 0) //不计算伤害
            {
                result.mode = 0;
                result.value = 0;
                return result;
            }

            var hitRate = attacker.GetAttribute(fight_attri_config.HIT_RATE);
            var missRate = defender.GetAttribute(fight_attri_config.MISS_RATE);
            //Debug.LogError("hitRate::" + hitRate + ":" + missRate );
            if ((hitRate - missRate) < UnityEngine.Random.Range(0f, 1f))
            {
                return result;
            }
            result.mode = (UInt32)1 << public_config.SPELL_ATTACK_HIT;
            SkillData skillData = CombatLogicObjectPool.GetSkillData(skillID);
            //判断暴击
            float critFactor = 1;
            float critProb = attacker.GetAttribute(fight_attri_config.CRIT_RATE) - defender.GetAttribute(fight_attri_config.ANTI_CRIT_RATE);
            if (UnityEngine.Random.Range(0f, 1f) <= critProb)
            {
                result.mode = result.mode + ((UInt32)1 << public_config.SPELL_ATTACK_CRITICAL);
                critFactor = 1 + attacker.GetAttribute(fight_attri_config.CRIT_DMG_ADD_RATE);
                critFactor += SkillCalculate.CalcDmgCritRate(attacker, skillData, skillAction);
            }
            //判断破击
            float strikeFactor = 1;
            float strikeProb = attacker.GetAttribute(fight_attri_config.TRUE_STRIKE_RATE) - defender.GetAttribute(fight_attri_config.ANTI_TRUE_STRIKE_RATE);
            if (UnityEngine.Random.Range(0f, 1f) <= strikeProb)
            {
                result.mode = result.mode + ((UInt32)1 << public_config.SPELL_ATTACK_STRIKE);
                strikeFactor = 1 + attacker.GetAttribute(fight_attri_config.TURE_STRIKE_DMG_ADD_RATE);
                strikeFactor += SkillCalculate.CalcDmgStrikeRate(attacker, skillData, skillAction);
            }
            //计算基础伤害
            //判断是否发生幸运一击
            float damageBase = 0;
            if (UnityEngine.Random.Range(0f, 1f) <= attacker.GetAttribute(fight_attri_config.LUCK_RATE))
            {
                damageBase = attacker.GetAttribute(fight_attri_config.DMG_MAX);
            }
            else
            {
                damageBase = UnityEngine.Random.Range(attacker.GetAttribute(fight_attri_config.DMG_MIN), attacker.GetAttribute(fight_attri_config.DMG_MAX));
            }
            //计算技能各种类型的伤害
            float totalDamage = 0;
            List<float> damageTypesRate = CalcDmgTypesRate(attacker, skillData, skillAction);
            List<float> damageTypesAdd = CalcDmgTypesAdd(attacker, skillData, skillAction);
            float addRate = attacker.GetAttribute(fight_attri_config.ALL_DMG_ADD_RATE);
            addRate = addRate + GetDmgARCorrect(attacker, defender);
            float reduceRate = defender.GetAttribute(fight_attri_config.ALL_DMG_REDUCE_RATE);
            reduceRate = reduceRate + GetDmgRRCorrect(attacker, defender);
            float totalDamageFactor = (1 + addRate) * (1 - reduceRate);
            float pvpConrrect = GetPvpCorrect(attacker, defender);
            float lvlConrrect = GetLevelCorrect(attacker, defender);
            float specialFactor = GetActionSpecialFactor(attacker, defender, skillID, skillAction, targetCnt);
            totalDamageFactor = totalDamageFactor * pvpConrrect * lvlConrrect * specialFactor;

            for (int index = 0; index < damageTypes.Count; index++)
            {
                SkillTypeDamage skillTypeDamage = CalculateTypeDamage(attacker, defender, skillAction, damageTypes[index], damageTypesRate[index], damageTypesAdd[index],
                                                   damageBase, critFactor, strikeFactor);
                totalDamage = totalDamage + skillTypeDamage.curDmg;
                StatisticsDamage(attacker, defender, damageTypes[index], skillTypeDamage.damageFlag, skillTypeDamage.curDmg * totalDamageFactor);
            }

            totalDamage = Mathf.Ceil(totalDamage * totalDamageFactor);
            totalDamage = Math.Max(totalDamage, 1);
            result.value = (int)totalDamage;
            return result;
        }

        //-----------------------
        //统计物，魔，火，冰，电，毒类型，暴击，破击伤害
        //@param attacker: 攻击者
        //@param defender: 受击者
        //-----------------------
        public static void StatisticsDamage(EntityCreature attacker, EntityCreature defender, int damageType, byte damageFlag, float totalDamage)
        {
            SetDamage(attacker, public_config.MISSION_ACTION_OUTPUT_DAMAGE, totalDamage);
            SetDamage(defender, public_config.MISSION_ACTION_GET_DAMAGE, totalDamage);
            //--统计六种技能攻击类型伤害
            int index = 1;
            if (damageType >= public_config.SPELL_DAMAGE_PHY && damageType <= public_config.SPELL_DAMAGE_POISON)
            {
                //--六种元素伤害
                index =  public_config.SPELL_DAMAGE_OUTPUT_OFFSET + damageType;
                SetDamage(attacker, index, totalDamage);
                index = public_config.SPELL_DAMAGE_GET_OFFSET + damageType;
                SetDamage(defender, index, totalDamage);
                //--暴击，破击伤害统计
                if (damageFlag == 1)
                {
                    index = public_config.MISSION_ACTION_OUTPUT_CRIT_DAMAGE;
                    SetDamage(attacker, index, totalDamage);
                    index = public_config.MISSION_ACTION_GET_CRIT_DAMAGE;
                    SetDamage(defender, index, totalDamage);
                }
                else if (damageFlag == 2)
                {
                    index = public_config.MISSION_ACTION_OUTPUT_STRIKE_DAMAGE;
                    SetDamage(attacker, index, totalDamage);
                    index = public_config.MISSION_ACTION_GET_STRIKE_DAMAGE;
                    SetDamage(defender, index, totalDamage);
                }
            }
        }

        //设置统计的伤害值
        public static void SetDamage(EntityCreature player, int damage_type, float totalDamage)
        {
            player.OnAddStatisticsDamage(damage_type, totalDamage);
        }

        //----------------------------
        //--获取技能行为对所有类型的伤害加成系数
        //----------------------------
        public static List<float> CalcDmgTypesRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> typesDmgRateCfg = skillAction.GetDmgTypesRate();
            List<float> dmgRateBases = skillAction.GetDmgRateLvlFactors();
            float dmgRateBase = 0;
            if (dmgRateBases.Count > 0)
            {
                dmgRateBase = dmgRateBases.Count >= skillLevel ? dmgRateBases[skillLevel - 1] : dmgRateBases[dmgRateBases.Count - 1];
            }
            List<float> typesDmgRate = new List<float>();
            for (int i = 0; i < typesDmgRateCfg.Count; i++)
            {
                typesDmgRate.Add(typesDmgRateCfg[i] * dmgRateBase);
            }
            return typesDmgRate;
        }

        //----------------------------
        //--获取技能行为对所有类型的伤害追加系数
        //----------------------------
        public static List<float> CalcDmgTypesAdd(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> typesDmgAddCfg = skillAction.GetDmgTypesAdd();
            List<float> dmgAddBases = skillAction.GetDmgAddLvlFactors();
            float dmgAddBase = 0 ;
            if (dmgAddBases.Count > 0)
            {
                dmgAddBase = dmgAddBases.Count >= skillLevel ? dmgAddBases[skillLevel - 1] : dmgAddBases[dmgAddBases.Count - 1];
            }
            List<float> typesDmgAdd = new List<float>();
            for (int i = 0; i < typesDmgAddCfg.Count; i++)
            {
                typesDmgAdd.Add(typesDmgAddCfg[i] * dmgAddBase);
            }
            return typesDmgAdd;
        }

        //----------------------------
        //--获取技能行为的暴击追加系数
        //----------------------------
        public static float CalcDmgCritRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> dmgCritRateBases = skillAction.GetDmgCritRateLvlFactors();
            float critRate = 0;
            if (dmgCritRateBases.Count > 0)
            {
                critRate = dmgCritRateBases.Count >= skillLevel ? dmgCritRateBases[skillLevel - 1] : dmgCritRateBases[dmgCritRateBases.Count - 1];
            }
            return critRate;
        }

        //----------------------------
        //--获取技能行为的破击追加系数
        //----------------------------
        public static float CalcDmgStrikeRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> dmgStrikeRateBases = skillAction.GetDmgStrikeRateLvlFactors();
            float strikeRate = 0;
            if (dmgStrikeRateBases.Count > 0)
            {
                strikeRate = dmgStrikeRateBases.Count >= skillLevel ? dmgStrikeRateBases[skillLevel - 1] : dmgStrikeRateBases[dmgStrikeRateBases.Count - 1];
            }
            return strikeRate;
        }

        /*
        -----------------------
        --计算各种类型伤害
        --@param attacker: 攻击者
        --@param defender: 受击者
        --@param spell_per: 伤害加成比例
        --@param spell_add: 伤害增加数值
        --@param damage_type: 伤害类型
        --@param damage_base: 伤害基础值
        --@param crit_factor: 暴击加成系数
        --@param stirke_factor: 破击加成系数
        -----------------------*/
        public static SkillTypeDamage CalculateTypeDamage(EntityCreature attacker, EntityCreature defender, SkillAction skillAction, int damageType, 
                                                float spellPer, float spellAdd, float damageBase, float critFactor, float stirkeFactor)
        {
            float damageAddRate = 0;
            float damageReduceRate = 0;
            float damagePernRate = 0;
            //--获取各类伤害对应的加成值
            if (damageType == public_config.SPELL_DAMAGE_PHY)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.PHY_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.PHY_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.PHY_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_MAG)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.MAG_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.MAG_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.MAG_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_FIRE)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.FIRE_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.FIRE_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.FIRE_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_ICE)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.ICE_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.ICE_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.ICE_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_LIGHNING)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.LIGHNING_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.LIGHNING_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.LIGHNING_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_POISON)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.POISON_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.POISON_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.POISON_PENETRATION_RATE);
            }

            float damage = damageBase * spellPer + spellAdd;
            //判断玩家的专精伤害加成
            if (attacker is PlayerAvatar)
            {
                float proficient_factor = skillAction.GetDmgProficientFactor();
                if (proficient_factor > 0)
                {
                    List<int> typesDmgCfg = skillAction.GetDmgTypes();
                    float damageTypeFactor = 0;
                    for (int i = 0; i < typesDmgCfg.Count; ++i)
                    {
                        if (typesDmgCfg[i] == damageType)
                        {
                            List<float> dmgTypesRate = skillAction.GetDmgTypesRate();
                            damageTypeFactor = dmgTypesRate[i];
                        }
                    }
                    int proficient_id = (attacker as PlayerAvatar).spell_proficient;
                    fight_attri_config proficient_attri_id = fight_attri_config.SPELL_PROFICIENT1_FACTOR + proficient_id - 1;
                    damage += proficient_factor * attacker.GetAttribute(proficient_attri_id) * damageTypeFactor;
                }
            }
            //判断buff对该行为的加成
            damage = damage + attacker.GetSpellActionExDmg(skillAction.actionID, damageType);

            damage = damage * (1 + damageAddRate);
            float damageFactor = Mathf.Min(1 - damageReduceRate + damagePernRate, 1);
            damage = damage * damageFactor;
            float dmgAddFactor = critFactor;
            //damage_flag:0：没有发生额外的伤害。1：发生了暴击。2：发生了破击
            byte damageFlag = 0;
            if (critFactor > 1)
            {
                damageFlag = 1 ;
            }
            if (stirkeFactor > 1)  //发生破击
            {
                dmgAddFactor = Mathf.Max(dmgAddFactor, stirkeFactor / damageFactor);
                if (dmgAddFactor > stirkeFactor)
                {
                    damageFlag = 2;
                }
            }
            damage = damage * dmgAddFactor;
            return new SkillTypeDamage(damage, damageFlag);
        }

        //--获取伤害加成修正系数
        public static float GetDmgARCorrect(EntityCreature attacker, EntityCreature defender)
        {
            if (defender is EntityAvatar)
            {
                var vocation = (int)(defender as EntityAvatar).vocation;
                if (vocation == public_config.VOC_WARRIOR)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_WARRIOR_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_WIZARD)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_WIZARD_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_ARCHER)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_ARCHER_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_ASSASSIN)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_ASSASSIN_DMG_ADD_RATE);
                }
            }
            else if (defender is EntityDummy)
            {
                var monsterData = GameData.monster_helper.GetMonster((int)(defender as EntityDummy).monster_id);
                var mstType = monsterData.__type;
                var mstModelType = monsterData.__model_type;
                var addRate = attacker.GetAttribute(fight_attri_config.TO_VEHICON_DMG_ADD_RATE + mstType - 1);
                addRate = addRate + attacker.GetAttribute(fight_attri_config.TO_HUMAN_DMG_ADD_RATE + mstModelType);
                return addRate;
            }
            return 0;
        }

        //--获取伤害减免修正系数
        public static float GetDmgRRCorrect(EntityCreature attacker, EntityCreature defender)
        {
            if (defender is EntityAvatar)
            {
                var vocation = (int)(defender as EntityAvatar).vocation;
                if (vocation == public_config.VOC_WARRIOR)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_WARRIOR_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_WIZARD)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_WIZARD_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_ARCHER)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_ARCHER_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_ASSASSIN)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_ASSASSIN_DMG_REDUCE_RATE);
                }
            }
            else if (defender is EntityDummy)
            {
                var monsterData = GameData.monster_helper.GetMonster((int)(defender as EntityDummy).monster_id);
                var mstType = monsterData.__type;
                var mstModelType = monsterData.__model_type;
                var reduceRate = attacker.GetAttribute(fight_attri_config.FROM_VEHICON_DMG_REDUCE_RATE + mstType - 1);
                reduceRate = reduceRate + attacker.GetAttribute(fight_attri_config.FROM_VEHICON_DMG_REDUCE_RATE + mstModelType);
                return reduceRate;
            }
            return 0;
        }

        //--获取PVP修正值
        public static float GetPvpCorrect(EntityCreature attacker, EntityCreature defender)
        {
            var attkEntType = attacker.entityType;
            var dfdEntType = defender.entityType;
            if ((attkEntType == dfdEntType) && (attkEntType == EntityConfig.ENTITY_TYPE_AVATAR))
            {
                var pvpAddRate = attacker.GetAttribute(fight_attri_config.PVP_DMG_ADD_RATE);
                var pvpReduceRate = attacker.GetAttribute(fight_attri_config.PVP_DMG_REDUCE_RATE);
                return (1 + pvpAddRate) * (1 - pvpReduceRate);
            }
            return 1;
        }

        //--获取等级修正值
        public static float GetLevelCorrect(EntityCreature attacker, EntityCreature defender)
        {
            var dfdEntType = defender.entityType;
            if (dfdEntType == EntityConfig.ENTITY_TYPE_NAME_DUMMY)
            {
                var levelFactors = Common.Global.GlobalParams.DmgLevelFactors;
                var lvlDiff = defender.level - attacker.level - levelFactors[0];
                if (lvlDiff <= 0)
                {
                    return 1;
                }
                else
                {
                    return Mathf.Max(1 - levelFactors[1] * lvlDiff, levelFactors[2]);
                }
            }
            return 1;
        }

        // 计算技能行为的特殊
        public static float GetActionSpecialFactor(EntityCreature attacker, EntityCreature defender, int skillID, SkillAction skillAction, int targetCnt)
        {
            int dmgSpecialType = skillAction.GetDmgSpecialType();
            List<float> dmgSpecialArg = skillAction.GetDmgSpecialArg();
            float factor;
            int partionCnt;
            switch (dmgSpecialType)
            {
                case 1:  //按距离计算伤害系数
                    float distance = CombatSystemTools.CalculateDistanceBetween(attacker, defender) * 100; //换算为以cm为单位
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = partionCnt - 1; i >= 0; --i)
                    {
                        if (distance >= dmgSpecialArg[i])
                        {
                            return dmgSpecialArg[i + partionCnt];
                        }
                    }
                    break;
                case 2:  //按目标buff加成
                    factor = 1;
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = 0; i < partionCnt; ++i)
                    {
                        if (defender.bufferManager.ContainsBuffer((int)dmgSpecialArg[i]))
                        {
                            factor = factor * dmgSpecialArg[partionCnt + i];
                        }
                    }
                    return factor;
                case 3:     //按搜集目标数量进行调整
                    factor = 1f / targetCnt;
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = 0; i < partionCnt; ++i)
                    {
                        if ((int)dmgSpecialArg[i] == targetCnt)
                        {
                            factor = dmgSpecialArg[partionCnt + i];
                            break;
                        }
                    }
                    return factor;
                default:
                    break;
            }
            return 1;
        }
    }
}
