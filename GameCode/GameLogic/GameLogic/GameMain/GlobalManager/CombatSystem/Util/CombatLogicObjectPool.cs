﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;

namespace GameMain.CombatSystem
{
    public class CombatLogicObjectPool
    {
        #region skillsubject
        //skilldata缓冲池
        private static Dictionary<int, SkillData> s_skillDataDict = new Dictionary<int, SkillData>();
        public static SkillData GetSkillData(int skillID)
        {
            SkillData skillData;
            if (s_skillDataDict.ContainsKey(skillID))
            {
                skillData = s_skillDataDict[skillID];
            }else
            {
                skillData = new SkillData(skillID);
                s_skillDataDict.Add(skillID, skillData);
            }
            return skillData;
        }

        //skillsubject对象池
        private static Queue<SkillSubject> s_skillSubjectPool = new Queue<SkillSubject>();
        public static SkillSubject CreateSkillSubject(int skillID)
        {
            SkillSubject result;
            var skillData = GetSkillData(skillID);
            if (s_skillSubjectPool.Count > 0)
            {
                result = s_skillSubjectPool.Dequeue();
            }
            else
            {
                result = new SkillSubject();
            }
            result.Reset(skillData);
            return result;
        }

        public static void ReleaseSkillSubject(SkillSubject skillSubject)
        {
            s_skillSubjectPool.Enqueue(skillSubject);
        }

        //SkillSubjectServer对象池
        private static Queue<SkillSubjectServer> s_skillSubjectServerPool = new Queue<SkillSubjectServer>();
        public static SkillSubjectServer CreateSkillSubjectServer(int skillID)
        {
            SkillSubjectServer result;
            var skillData = GetSkillData(skillID);
            if (s_skillSubjectServerPool.Count > 0)
            {
                result = s_skillSubjectServerPool.Dequeue();
            }
            else
            {
                result = new SkillSubjectServer();
            }
            result.Reset(skillData);
            return result;
        }

        public static void ReleaseSkillSubjectServer(SkillSubjectServer skillSubject)
        {
            s_skillSubjectServerPool.Enqueue(skillSubject);
        }

        #endregion

        #region skillaction
        private static Dictionary<int, SkillActionData> s_skillActionDataDict = new Dictionary<int, SkillActionData>();
        public static SkillActionData GetSkillActionData(int actionID)
        {
            SkillActionData skillActionData = null;
            if (s_skillActionDataDict.ContainsKey(actionID))
            {
                skillActionData = s_skillActionDataDict[actionID];
            }
            else
            {
                //try
                //{
                    skillActionData = new SkillActionData(actionID);
                    s_skillActionDataDict.Add(actionID, skillActionData);
                //}
                //catch (Exception ex)
                //{
                //    GameLoader.Utils.LoggerHelper.Error("SkillActionData init error:" + actionID);
                //    GameLoader.Utils.LoggerHelper.Error(ex);
                //}
            }
            return skillActionData;
        }

        //skillsubject对象池
        private static Queue<SkillAction> s_skillActionPool = new Queue<SkillAction>();
        public static SkillAction CreateSkillAction(int actionID)
        {
            SkillAction result;
            var actionData = GetSkillActionData(actionID);
            if (s_skillActionPool.Count > 0)
            {
                result = s_skillActionPool.Dequeue();
            }
            else
            {
                result = new SkillAction();
            }
            result.Reset(actionData);
            return result;
        }

        public static void ReleaseSkillAction(SkillAction skillAction)
        {
            skillAction.Clear();
            s_skillActionPool.Enqueue(skillAction);
        }

        //skillActionServer对象池
        private static Queue<SkillActionServer> s_skillActionServerPool = new Queue<SkillActionServer>();
        public static SkillActionServer CreateSkillActionServer(int actionID)
        {
            SkillActionServer result;
            var actionData = GetSkillActionData(actionID);
            if (s_skillActionServerPool.Count > 0)
            {
                result = s_skillActionServerPool.Dequeue();
            }
            else
            {
                result = new SkillActionServer();
            }
            result.Reset(actionData);
            return result;
        }

        public static void ReleaseSkillActionServer(SkillActionServer skillAction)
        {
            skillAction.Clear();
            s_skillActionServerPool.Enqueue(skillAction);
        }
        #endregion


        #region buffer
        //BufferData缓冲池
        private static Dictionary<int, BufferData> s_bufferDataDict = new Dictionary<int, BufferData>();
        public static BufferData GetBufferData(int bufferID)
        {
            BufferData data;
            if (s_bufferDataDict.ContainsKey(bufferID))
            {
                data = s_bufferDataDict[bufferID];
            }
            else
            {
                data = new BufferData(bufferID);
                s_bufferDataDict.Add(bufferID, data);
            }
            return data;
        }

        //skillsubject对象池
        private static Queue<BufferSubject> s_bufferSubjectPool = new Queue<BufferSubject>();
        public static BufferSubject CreateBufferSubject(int skillID)
        {
            BufferSubject result;
            var bufferData = GetBufferData(skillID);
            if (s_bufferSubjectPool.Count > 0)
            {
                result = s_bufferSubjectPool.Dequeue();
            }
            else
            {
                result = new BufferSubject();
            }
            result.Reset(bufferData);
            return result;
        }
        #endregion

        #region BossPartSubject
        //BossPartData缓冲池
        private static Dictionary<int, PartData> s_bossPartDataDict = new Dictionary<int, PartData>();
        public static PartData GetBossPartData(int id)
        {
            PartData data;
            if (s_bossPartDataDict.ContainsKey(id))
            {
                data = s_bossPartDataDict[id];
            }
            else
            {
                data = new PartData(id);
                s_bossPartDataDict.Add(id, data);
            }
            return data;
        }

        //BossPartSubjectClient对象池
        private static Queue<PartSubjectClient> s_bossPartSubjectClientPool = new Queue<PartSubjectClient>();
        public static PartSubjectClient CreatePartSubjectClient(int id)
        {
            PartSubjectClient result;
            var bossPartData = GetBossPartData(id);
            if (s_bossPartSubjectClientPool.Count > 0)
            {
                result = s_bossPartSubjectClientPool.Dequeue();
            }
            else
            {
                result = new PartSubjectClient();
            }
            result.Initialize(bossPartData);
            return result;
        }

        //BossPartSubjectServer对象池
        private static Queue<PartSubjectServer> s_bossPartSubjectServerPool = new Queue<PartSubjectServer>();
        public static PartSubjectServer CreatePartSubjectServer(int id)
        {
            PartSubjectServer result;
            var bossPartData = GetBossPartData(id);
            if (s_bossPartSubjectServerPool.Count > 0)
            {
                result = s_bossPartSubjectServerPool.Dequeue();
            }
            else
            {
                result = new PartSubjectServer();
            }
            result.Initialize(bossPartData);
            return result;
        }
        #endregion

        public static void ClearAllData()
        {
            s_skillDataDict.Clear();
            s_skillActionDataDict.Clear();
            s_bufferDataDict.Clear();
            s_bossPartDataDict.Clear();
        }
    }
}
