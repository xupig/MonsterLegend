﻿using Common.ClientConfig;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/8 15:11:31 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartManagerUpdateDelegate : MogoEngine.UpdateDelegateBase { }

    public class PartManager
    {
        protected EntityMonsterBase _owner;
        protected List<int> _partIdList;
        protected Dictionary<int, PartSubject> _partSubjects;
        protected PartSubject _currentSelectedSubject;

        public PartManager(EntityMonsterBase owner)
        {
            _owner = owner;
            Initialize();
        }

        virtual protected void Initialize()
        {
            InitPartIdList();
            InitPartSubjects();
            SortPartIdList();
            _currentSelectedSubject = null;
            MogoEngine.MogoWorld.RegisterUpdate<PartManagerUpdateDelegate>("PartManager.Update", Update); 
        }

        private void InitPartIdList()
        {
            if (_partIdList == null)
            {
                _partIdList = new List<int>();
            }
            else
            {
                _partIdList.Clear();
            }
        }

        private void InitPartSubjects()
		{
			if (_partSubjects == null)
			{
				_partSubjects = new Dictionary<int, PartSubject>();
			}
			else
			{
				_partSubjects.Clear();
			}
            var partList = monster_helper.GetBossPart((int)_owner.monster_id);
            if (partList == null || partList.Count == 0 || (partList.Count == 1 && partList[0] == 0))
            {
                return;
            }
            foreach (int id in partList)
            {
                PartSubject subject = CreatePartSubject(id);
                subject.SetOwner(_owner);
                _partIdList.Add(id);
                _partSubjects.Add(id, subject);
                _owner.CreateEntityPart(subject);
            }
        }

        private void SortPartIdList()
        {
            _partIdList.Sort((a1, a2) =>
            {
                return _partSubjects[a2].GetPriority() - _partSubjects[a1].GetPriority();
            });
        }

        virtual protected PartSubject CreatePartSubject(int id)
        {
            return CombatLogicObjectPool.CreatePartSubjectClient(id);
        }

        virtual public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("PartManager.Update", Update);

            _currentSelectedSubject = null;

            _partIdList.Clear();
            _partIdList = null;

            foreach (var node in _partSubjects)
            {
                //TODO: 回收Subject
                node.Value.Release();
            }
            _partSubjects.Clear();
            _partSubjects = null;
        }

        public Dictionary<int, PartSubject> PartSubjects
        {
            get { return _partSubjects; }
        }

        public PartSubject CurrentSelectedSubject
        {
            get { return _currentSelectedSubject; }
        }

        public bool HasPart()
        {
            return _partIdList != null || _partIdList.Count > 0;
        }

        public List<int> PartIdList
        {
            get { return _partIdList; }
        }

        public bool CanBeSelected(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                LoggerHelper.Warning(string.Format("Current id can't be selected because monster id {0} doesn't have a part id {1}", _owner.monster_id, partId));
                return false;
            }
            return _partSubjects[partId].CanBeSelected();
        }

        virtual public bool SelectPart(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                LoggerHelper.Warning(string.Format("无法选中怪物id为{0}的部位{1}，因为怪物部位列表中不包含该部位", _owner.monster_id, partId));
                return false;
            }
            if (!_partSubjects[partId].CanBeSelected())
            {
                LoggerHelper.Warning(string.Format("无法选中怪物id为{0}的部位{1}，因为该部位状态为{2}，无法被选中", _owner.monster_id, partId, _partSubjects[partId].Status));
                return false;
            }
            if (_currentSelectedSubject != null && _currentSelectedSubject.Id == partId)
            {
                LoggerHelper.Warning(string.Format("怪物id为{0}的部位{1}已经是选中态了.", _owner.monster_id, partId));
                return false;
            }
            return true;
        }

        virtual public bool CancelCurrentSelectedPartManually()
        {
            if (_currentSelectedSubject == null)
            {
                LoggerHelper.Warning("取消选中操作错误，当前没有选中的部位。");
                return false;
            }
            return true;
        }

        public bool CancelCurrentSelectedPartWhenBorken()
        {
            if (_currentSelectedSubject == null)
            {
                LoggerHelper.Warning("取消选中操作错误，当前没有选中的部位。");
                return false;
            }
            _currentSelectedSubject = null;
            return true;
        }

        virtual public bool RecoverPart(int partId)
        {
            return false;
        }

        virtual public SkillDamage CalculateDamage(EntityCreature attacker, int skillID, SkillAction skillAction, int targetCnt)
        {
            return new SkillDamage();
        }

        private void Update()
        {
            if (_partSubjects == null)
            {
                return;
            }
            foreach (var node in _partSubjects)
            {
                node.Value.Update();
            }
        }

        virtual public bool PartHasBeenBroken(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                return false;
            }
            int status = _partSubjects[partId].Status;
            return status == public_config.BODY_PART_STATE_DESTROY || status == public_config.BODY_PART_STATE_RECOVER;
        }

        virtual public bool PartCanBeRecovered(int partId)
        {
            return false;
        }

        virtual public void ResponseBodyPartList(PbBodyPartList bodyPartList)
        {
        }

        virtual public void ResponseBodyPartUpdate(PbBodyPart pbBodyPart)
        {
        }

        virtual public void ResponseBodyPartSelect(byte partId)
        {
        }
    }
}
