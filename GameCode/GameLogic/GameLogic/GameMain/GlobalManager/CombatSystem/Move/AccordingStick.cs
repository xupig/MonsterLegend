﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain.Entities;
using Common.Data;
using ACTSystem;
using Common.States;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.GlobalManager;
using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    //player专用，常规状态下根据摇杆状态移动
    public class AccordingStick : AccordingStateBase
    {
        public AccordingStick(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingStick;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            CheckingStick();
            bool sprint = false;
            while (true)
            {
                if (ControlStickState.strength == 0)
                {
                    actor.actorController.Stop();
                    sprint = false;
                    break;
                }
                Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                if (_owner.CanTurn())
                {
                    _owner.actor.forward = moveDirection;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed;
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    actor.actorController.Move(movement);
                    sprint = true;
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                    sprint = false;
                }
                break;
            }
            TrySprint(sprint);
            return true;
        }

        public override void OnLeave()
        {
            TrySprint(false);
        }

        public override void Stop()
        {
            base.Stop();
        }

        private void TrySprint(bool state)
        {
            if (state)
            {
                PlayerSprintManager.Instance.TryStartSprint();
            }
            else
            {
                PlayerSprintManager.Instance.TryStopSprint();
            }
        }

        float _nextClearTime = 0;
        private void TryClearPlayerAction()
        {
            if (_nextClearTime < Time.realtimeSinceStartup)
            {
                PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = Time.realtimeSinceStartup + 0.0f;
            }
        }

        private float _lastStickStrength = 0;
        private void CheckingStick()
        {
            var actor = movingActor;
            if (actor == null) return;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return;

            if (_lastStickStrength == 0 && _lastStickStrength != ControlStickState.strength)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.CLOSE_PLAYERINFO);
            }
            _lastStickStrength = ControlStickState.strength;
        }
    }
}
