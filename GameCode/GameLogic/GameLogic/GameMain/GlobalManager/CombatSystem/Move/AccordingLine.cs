﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain.Entities;
using Common.Data;
using ACTSystem;

namespace GameMain.CombatSystem
{
    public class AccordingLine : AccordingStateBase
    {
        private Vector3 _moveTargetPosition = Vector3.zero;
        public Vector3 moveTargetPosition
        {
            get { return _moveTargetPosition; }
        }
        private float _moveBaseSpeed = 0f;
        private float _moveTime = 0;

        public AccordingLine(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingLine;
        }

        public bool Reset(Vector3 targetPosition, float speed)
        {
            var actor = movingActor;
            if (actor == null) return false;
            if (speed == 0) return false;

            var dis = (targetPosition - actor.position).magnitude;
            _moveTargetPosition = targetPosition;
            _moveBaseSpeed = speed;
            _moveTime = dis * 2 / speed;
            return true;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            //服务器应该遵循的规则：
            //    当技能行为带位移：同步行为的同时同步“最终”目标地点信息，服务器首先将玩家位置移动到该位置
            //    行为位移结束，自动恢复默认同步方式。
            //问题：坐标有玩家自身同步到服务器，技能相关位移又该如何选择？
            var actor = movingActor;
            if (actor == null) return false;
            if (_moveTargetPosition == Vector3.zero) return false;
            //_moveTargetPosition.y = actor.position.y;
            var moveDirection = _moveTargetPosition - actor.position;
            moveDirection.y = 0;
            var distance = moveDirection.magnitude;
            if (!_owner.CanActiveMove())
            {
                actor.actorController.Stop();
                return false;
            }
            if (_moveTime <= 0)
            {
                actor.actorController.Stop();
                _owner.position = actor.position;
                return false;
            }
            if (distance < (_moveBaseSpeed * Time.deltaTime))
            {
                actor.actorController.Stop();
                _moveTargetPosition.y = actor.position.y;
                actor.position = _moveTargetPosition;
                _owner.position = actor.position;
                return false;
            }
            _moveTime = _moveTime - Time.deltaTime;
            Vector3 movement = moveDirection.normalized * _moveBaseSpeed;
            movement.y = 0;
            actor.actorController.Move(movement);
            _owner.position = actor.position;
            actor.FaceTo(_moveTargetPosition);
            return true;
        }

        public override void OnLeave()
        {
            _moveTargetPosition = Vector3.zero;
        }
    }
}
