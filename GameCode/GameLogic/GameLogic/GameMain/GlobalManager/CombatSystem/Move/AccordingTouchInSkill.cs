﻿using Common.Events;
using Common.States;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class AccordingTouchInSkill : AccordingSkill
    {
        private Vector3 _targetPoint = Vector3.zero;
        private List<float> _path;
        private Vector3 _startPoint = Vector3.zero;     //每段寻路的起始点
        private Vector3 _nextPoint = Vector3.zero;      //每段寻路的终点
        private float _currentPartDistance = 0f;        //每段寻路的距离

        private float _distanceCache = 0;
        private float _startLockTimeStamp = 0;

        private float _curMoveSpeed;

        public AccordingTouchInSkill(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingTouchInSkill;
        }

        public override void OnEnter()
        {
            _owner.isMovingByTouch = true;
        }

        public override bool Update()
        {
            if (!base.Update())
            {
                return false;
            }
            var actor = movingActor;
            if (actor == null) return false;
            Vector3 touchPoint = PlayerTouchBattleModeManager.GetInstance().touchPosition;
            bool result = false;

            while (true)
            {
                if (!_owner.CanActiveMove())
                {
                    result = false;
                    break;
                }
                if (touchPoint == Vector3.zero)
                {
                    result = false;
                    break;
                }
                _curMoveSpeed = _owner.moveManager.curMoveSpeed;
                if (!UpdatePathInfo(touchPoint))
                {
                    result = false;
                    break;
                }
                Vector3 moveDirection = _nextPoint - actor.position;
                moveDirection.y = 0;
                float distance = moveDirection.magnitude;
                if (ReachTargetPoint(distance))
                {
                    result = false;
                    PlayerTouchBattleModeManager.GetInstance().DeleteTouchPosition();
                    break;
                }
                bool setNextPointResult = true;
                var distanceToStartPoint = Vector3.Distance(_startPoint, actor.position);
                if (Mathf.Abs(_currentPartDistance - distanceToStartPoint) <= 0.5f && HasNextPoint())
                {
                    setNextPointResult = SetNextPoint();
                }
                if (CheckLockedInBlock(distance))//卡障碍的问题 3秒后直接跳点
                {
                    _nextPoint.y = actor.position.y;
                    actor.position = _nextPoint;
                    setNextPointResult = SetNextPoint();
                }
                if (!setNextPointResult)
                {
                    result = false;
                    break;
                }

                Vector3 movement = moveDirection.normalized * _curMoveSpeed;
                movement.y = 0;
                actor.actorController.Move(movement);
                actor.actorController.SetSlowRate(_curMoveSpeed);
                _owner.position = actor.position;
                if (_owner.CanTurn())
                {
                    actor.FaceTo(_nextPoint);
                }
                result = true;
                break;
            }
            if (!result)
            {
                actor.actorController.Stop();
            }
            _owner.isMovingByTouch = result;
            return result;
        }

        public override void OnLeave()
        {
            _owner.isMovingByTouch = false;
            _targetPoint = Vector3.zero;
            if (_path != null)
            {
                _path.Clear();
                _path = null;
            }
        }

        private bool ReachTargetPoint(float distance)
        {
            return !HasNextPoint() && distance < _curMoveSpeed * Time.deltaTime;
        }

        private bool UpdatePathInfo(Vector3 touchPoint)
        {
            if (_targetPoint != Vector3.zero)
            {
                float distance = (_targetPoint - touchPoint).magnitude;
                if (distance < _curMoveSpeed * Time.deltaTime)
                {
                    return true;
                }
            }
            _targetPoint = touchPoint;
            var actor = movingActor;
            var sPosition = actor.position;
            if (_path != null)
            {
                _path.Clear();
                _path = null;
            }
            if (!PathUtils.CanLine(actor.position, _targetPoint, _owner.actor.isFlyingOnRide))
            {
                _path = GameSceneManager.GetInstance().GetPath(sPosition.x, sPosition.z, _targetPoint.x, _targetPoint.z, _owner.actor.modifyLayer);
                _path = PathUtils.FixPath(_path);
            }
            else
            {
                _path = new List<float>();
                _path.Add(sPosition.x);
                _path.Add(sPosition.z);
                _path.Add(_targetPoint.x);
                _path.Add(_targetPoint.z);
            }
            _currentPartDistance = 0;
            if (_path.Count > 2)
            {
                SetNextPoint();//路径中的第一个点为当前玩家所在位置，可以不要
            }
            if (!SetNextPoint())
            {
                _targetPoint = Vector3.zero;
                return false;
            }
            return true;
        }

        private bool HasNextPoint()
        {
            return _path.Count > 0;
        }

        private bool SetNextPoint()
        {
            if (_path == null || _path.Count == 0)
            {
                return false;
            }
            if (_nextPoint != Vector3.zero)
            {
                _startPoint = _nextPoint;
            }
            _nextPoint.x = _path[0];
            _nextPoint.y = movingActor.position.y;
            _nextPoint.z = _path[1];
            _path.RemoveAt(0);
            _path.RemoveAt(0);
            if (_startPoint != Vector3.zero)
            {
                _currentPartDistance = Vector3.Distance(_startPoint, _nextPoint);
            }
            return true;
        }

        private bool CheckLockedInBlock(float distance)
        {
            bool result = false;
            if (distance == 0)
            {
                _distanceCache = distance;
                return result;
            }
            if (Mathf.Abs(distance - _distanceCache) < 0.01f)
            {
                if (_startLockTimeStamp == 0)
                {
                    _startLockTimeStamp = Time.realtimeSinceStartup;
                    return result;
                }
                if (Time.realtimeSinceStartup - _startLockTimeStamp > 3f)
                {
                    result = true;
                    _startLockTimeStamp = 0;
                    _distanceCache = 0;
                    return result;
                }
                _distanceCache = distance;
                return result;
            }
            _distanceCache = distance;
            _startLockTimeStamp = 0;
            return result;
        }
    }
}
