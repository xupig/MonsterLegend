﻿using Common.Events;
using Common.States;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class AccordingSteeringWheel : AccordingStateBase
    {
        public AccordingSteeringWheel(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingSteeringWheel;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            CheckingStick();
            while (true)
            {
                if (ControlStickState.strength == 0)
                {
                    actor.actorController.Stop();
                    break;
                }
                float angle = 0f;
                Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                if (_owner.CanTurn())
                {
                    Vector3 currentDirection = _owner.actor.forward;
                    angle = Vector3.Angle(currentDirection, moveDirection);
                    float rotateAngle = Mathf.Min(_owner.rotateSpeed * Time.deltaTime, angle);
                    float lerp = (angle == 0) ? 1 : rotateAngle / angle;
                    _owner.actor.forward = Vector3.Slerp(currentDirection, moveDirection, lerp);
                }
                if (_owner.CanActiveMove())
                {
                    if (angle >= 90)
                    {
                        actor.actorController.Stop();
                        break;
                    }
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed * Mathf.Cos(MogoMathUtils.Angle2Radian(angle));
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    actor.actorController.Move(movement);
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                }
                break;
            }
            return true;
        }

        float _nextClearTime = 0;
        private void TryClearPlayerAction()
        {
            if (_nextClearTime < Time.realtimeSinceStartup)
            {
                PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = Time.realtimeSinceStartup + 0.0f;
            }
        }

        private float _lastStickStrength = 0;
        private void CheckingStick()
        {
            var actor = movingActor;
            if (actor == null) return;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return;

            if (_lastStickStrength == 0 && _lastStickStrength != ControlStickState.strength)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.CLOSE_PLAYERINFO);
            }
            _lastStickStrength = ControlStickState.strength;
        }
    }
}
