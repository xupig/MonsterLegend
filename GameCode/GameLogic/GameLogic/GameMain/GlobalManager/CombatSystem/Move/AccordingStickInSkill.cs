﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain.Entities;
using Common.Data;
using ACTSystem;
using Common.States;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.GlobalManager;

namespace GameMain.CombatSystem
{
    //player专用，释放技能状态下根据摇杆状态移动
    public class AccordingStickInSkill : AccordingSkill
    {
        public AccordingStickInSkill(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingStickInSkill;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            if (!base.Update())
            {
                return false;
            } 
            var actor = movingActor;
            if (ControlStickState.strength < 0) return false;
            while (true)
            {
                if (ControlStickState.strength == 0)
                {
                    actor.actorController.Stop();
                    break;
                }

                Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                if (_owner.CanTurn())
                {
                    _owner.actor.forward = moveDirection;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed;
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    actor.actorController.Move(movement);
                }
                else
                {
                    actor.actorController.Stop();
                }
                break;
            }
            return true;
        }

        public override void OnLeave()
        {

        }
    }
}
