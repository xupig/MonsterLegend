﻿#region 模块信息
/*==========================================
// 模块名：AccordingAnimationClip
// 命名空间: GameMain.CombatSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/17
// 描述说明：根据动画片段移动
// 其他：
//==========================================*/
#endregion

using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMain.CombatSystem
{
    public class AccordingAnimationClip : AccordingStateBase
    {
        public AccordingAnimationClip(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingAnimationClip;
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor != null)
            {
                _owner.position = actor.position;
            }
            return true;
        }

    }
}
