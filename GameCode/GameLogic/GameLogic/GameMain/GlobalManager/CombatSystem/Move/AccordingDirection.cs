﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain.Entities;
using Common.Data;
using ACTSystem;
using MogoEngine;

namespace GameMain.CombatSystem
{
    public class AccordingDirection : AccordingStateBase
    {
        private float _moveBaseSpeed = 0f;

        private float _moveTime = 0;

        private bool _right = false;

        private uint _targetID = 0;

        public AccordingDirection(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingDirection;
        }

        public bool Reset(uint target, float speed, float moveTime, bool right = false)
        {
            if (movingActor == null) return false;
            if (speed == 0) return false;
            _right = right;
            _moveBaseSpeed = speed;
            _targetID = target;
            _moveTime = moveTime;
            return true;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            //服务器应该遵循的规则：
            //    当技能行为带位移：同步行为的同时同步“最终”目标地点信息，服务器首先将玩家位置移动到该位置
            //    行为位移结束，自动恢复默认同步方式。
            //问题：坐标有玩家自身同步到服务器，技能相关位移又该如何选择？
            var actor = movingActor;
            if (actor == null) return false;
            FaceToTarget();
            var moveDirection = _right ? actor.transform.right : actor.transform.forward;
            if (!_owner.CanActiveMove())
            {
                actor.actorController.Stop();
                return false;
            }
            if (_moveTime <= 0)
            {
                actor.actorController.Stop();
                _owner.position = actor.position;
                return false;
            }
            _moveTime = _moveTime - Time.deltaTime;
            Vector3 movement = moveDirection.normalized * _moveBaseSpeed;
            movement.y = 0;
            actor.actorController.Move(movement, _right);
            _owner.position = actor.position;
            return true;
        }

        public override void OnLeave()
        {
             
        }

        void FaceToTarget()
        {
            var actor = movingActor;
            var target = MogoWorld.GetEntity(_targetID);
            if (target != null)
            {
                //actor.actorController.FaceToPosition(target.position);
                var forward = target.position - actor.position ;
                _owner.actor.forward = forward;
                //Debug.DrawRay(actor.position, forward, Color.green, 3);
            }
        }
    }
}
