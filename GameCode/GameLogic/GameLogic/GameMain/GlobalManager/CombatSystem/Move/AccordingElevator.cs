﻿
using GameMain.Entities;
namespace GameMain.CombatSystem
{
    public class AccordingElevator : AccordingStateBase
    {
        public AccordingElevator(EntityCreature owner)
            :base(owner)
        {
            modeType = AccordingMode.AccordingElevator;
        }

        public override void OnEnter()
        {
            _owner.actor.actorController.Stop();
        }

        public override bool Update()
        {
            if (movingActor != null)
            {
                _owner.position = movingActor.position;
            }
            return true;
        }

        public override void OnLeave()
        {

        }
    }
}
