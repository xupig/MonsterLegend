﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain.Entities;
using Common.Data;
using ACTSystem;
using GameMain.GlobalManager;
using Common.Utils;

namespace GameMain.CombatSystem
{
    public class AccordingPath : AccordingStateBase
    {
        private Vector3 _targetPosition = Vector3.zero;
        private float _moveSpeed = 0;
        private List<float> _path;
        private Vector3 _startPoint = Vector3.zero;     //每段寻路的起始点
        private Vector3 _nextPoint = Vector3.zero;      //每段寻路的终点
        private float _currentPartDistance = 0f;        //每段寻路的距离
        private Vector3 moveByPathTargetPosition = Vector3.zero;
        private float _moveTime = 0;
        private float _distanceCache = 0;
        private float _startLockTimeStamp = 0;
        public AccordingPath(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingPath;
        }

        public bool Reset(Vector3 targetPosition,float speed)
        {
            var actor = movingActor;
            if (actor == null) return false;
            _moveSpeed = speed;
            _targetPosition = targetPosition;
            if ((moveByPathTargetPosition - _targetPosition).magnitude < 0.5f) return false;
            moveByPathTargetPosition = _targetPosition;

            var sPosition = actor.position;
            _path = GameSceneManager.GetInstance().GetPath(sPosition.x, sPosition.z, _targetPosition.x, _targetPosition.z, _owner.actor.modifyLayer);
            _path = PathUtils.FixPath(_path);
			_currentPartDistance = 0;
            if (_path.Count > 2) {
                SetNextPoint();//路径中的第一个点为当前玩家所在位置，可以不要
            }
            if (sPosition.x == _targetPosition.x && sPosition.z == _targetPosition.z)
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            if (!SetNextPoint())
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            _distanceCache = 0;
            _startLockTimeStamp = 0;
            return true;
        }

        public override void OnEnter()
        {

        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            bool result = true;
            while (true)
            {
                if (!_owner.CanActiveMove())
                {
                    actor.actorController.Stop();
                    return false;
                }
                var moveDirection = _nextPoint - actor.position;
                moveDirection.y = 0;
                var distance = moveDirection.magnitude;
                bool setNextPointResult = true;
                var distanceToStartPoint = Vector3.Distance(_startPoint, actor.position);
                if (distance < _moveSpeed * Time.deltaTime && !HasNextPoint())
                {
                    actor.position = _nextPoint;
                    _owner.position = actor.position;
                    return false;
                }
                if (Mathf.Abs(_currentPartDistance - distanceToStartPoint) <= 0.5f && HasNextPoint())
                {
                    setNextPointResult = SetNextPoint();
                }
                if (CheckLockedInBlock(distance))//卡障碍的问题 3秒后直接跳点
                {
                    _nextPoint.y = actor.position.y;
                    actor.position = _nextPoint;
                    setNextPointResult = SetNextPoint();
                }
                if (!setNextPointResult)
                {
                    result = false;
                    break;
                }
                _moveTime = _moveTime - Time.deltaTime;
                Vector3 movement = moveDirection.normalized * _moveSpeed;
                movement.y = 0;
                actor.actorController.Move(movement);
                actor.actorController.SetSlowRate(_moveSpeed);
                _owner.position = actor.position;
                actor.FaceTo(_nextPoint);
                break;
            }
            return result;
        }

        private bool CheckLockedInBlock(float distance)
        {
            bool result = false;
            if (distance == 0)
            {
                _distanceCache = distance;
                return result;
            }
            if (Mathf.Abs(distance - _distanceCache) < 0.01f)
            {
                if (_startLockTimeStamp == 0)
                {
                    _startLockTimeStamp = Time.realtimeSinceStartup;
                    return result;
                }
                if (Time.realtimeSinceStartup - _startLockTimeStamp > 3f)
                {
                    result = true;
                    _startLockTimeStamp = 0;
                    _distanceCache = 0;
                    return result;
                }
                _distanceCache = distance;
                return result;
            }
            _distanceCache = distance;
            _startLockTimeStamp = 0;
            return result;
        }

        public override void OnLeave()
        {
            if (_path != null) _path.Clear();
            moveByPathTargetPosition = Vector3.zero;
        }

        private bool HasNextPoint()
        {
            return _path.Count > 0;
        }

        private bool SetNextPoint()
        {
            if (_path == null || _path.Count == 0)
            {
                return false;
            }
            if (_nextPoint != Vector3.zero)
            {
                _startPoint = _nextPoint;
            }
            _nextPoint.x = _path[0];
            _nextPoint.y = movingActor.position.y;
            _nextPoint.z = _path[1];
            _path.RemoveAt(0);
            _path.RemoveAt(0);
            if (_startPoint != Vector3.zero)
            {
                _currentPartDistance = Vector3.Distance(_startPoint, _nextPoint);
            }
            return true;
        }
    }
}
