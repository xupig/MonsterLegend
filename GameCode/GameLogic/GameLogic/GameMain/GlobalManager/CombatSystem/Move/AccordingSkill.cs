﻿using GameMain.Entities;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class AccordingSkill : AccordingStateBase
    {
        float _endTime = 0;

        public AccordingSkill(EntityCreature owner)
            : base(owner)
        {
        }
        public bool Reset(float duration)
        {
            _endTime = Time.realtimeSinceStartup + duration;
            return true;
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false; 
            if (_endTime < Time.realtimeSinceStartup)
            {
                return false;
            }
            return true;
        }
    }
}
