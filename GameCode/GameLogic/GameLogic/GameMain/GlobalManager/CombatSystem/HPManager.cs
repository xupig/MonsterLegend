﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using MogoEngine.Mgrs;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;
using Common.ServerConfig;
using GameData;
using Common.ClientConfig;
using MogoEngine.Events;
using Common.Events;
namespace GameMain.CombatSystem
{
    public class HPManager
    {
        private EntityCreature _owner;
        private int _curHpCache = 0;

        public HPManager(EntityCreature ower)
        {
            _owner = ower;
            EntityPropertyManager.Instance.AddEventListener( _owner, EntityPropertyDefine.cur_hp, this.OnHPChange );
        }

        public void Release()
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.cur_hp, this.OnHPChange);
        }

        private void OnHPChange()
        {
            if (_owner is EntityMonsterBase)
            {
                HandleHitFlyBuff();
            }
            if (_owner is EntityDummy)
            {
                HandleCondTrigger();
            }
        }

        private void HandleHitFlyBuff()
        {
            if (_curHpCache == 0)
            {
                _curHpCache = _owner.max_hp;
            }
            float hitFlyHpPercent = hit_fly_params_helper.GetHitFlyHpPercent();
            if (GetPercent(_curHpCache, _owner.max_hp) > hitFlyHpPercent &&
                GetPercent(_owner.cur_hp, _owner.max_hp) < hitFlyHpPercent)
            {
                AddHitFlyHpBuff();
            }
            _curHpCache = _owner.cur_hp;
        }

        private void HandleCondTrigger()
        {
            EntityDummy dummy = _owner as EntityDummy;
            string args = string.Concat(dummy.id, ":", dummy.monster_id);
            EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_HP_CHANG, args);
        }

        private float GetPercent(int curHp, int maxHp)
        {
            return (maxHp == 0) ? 0 : (curHp * 1.0f / maxHp);
        }

        private void AddHitFlyHpBuff()
        {
            _owner.bufferManager.AddBuffer(hit_fly_params_helper.GetHitFlyHpBuffID(), 0, true);
        }
    }
}
