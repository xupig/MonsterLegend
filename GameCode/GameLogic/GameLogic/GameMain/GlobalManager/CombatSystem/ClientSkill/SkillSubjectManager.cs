﻿using Common.States;
using GameMain.Entities;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SkillSubjectManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillSubjectManager
    {
        private EntityCreature _owner;

        private List<SkillSubject> _skillSubjects = new List<SkillSubject>();

        private SkillActionManager _skillActionManager;

        public SkillSubjectManager(EntityCreature ower)
        {
            _owner = ower;
            _skillActionManager = new SkillActionManager(this._owner);
            MogoEngine.MogoWorld.RegisterUpdate<SkillSubjectManagerUpdateDelegate>("SkillSubjectManager,Update", Update);
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("SkillSubjectManager,Update", Update);
            _skillActionManager.Release();
        }

        public void AddSkill(int skillID)
        {
            var skillSubject = CombatLogicObjectPool.CreateSkillSubject(skillID);
            AddSkillSubject(skillSubject);
        }

        public void AddSkillSubject(SkillSubject skillSubject)
        {
            if (skillSubject.GetActionCutTime() > 0)
            {
                if (_owner is EntityAvatar)
                {
                    (_owner as EntityAvatar).rideManager.Dismount();
                }
                BreakCurSkill();
            }
            AdjustTools.AdjustSkill(skillSubject, _owner.skillManager.GetCurAdjustSkillIDList());
            skillSubject.SetOwner(_owner);
            skillSubject.SetSkillActionManager(_skillActionManager);
            _skillSubjects.Add(skillSubject);
            skillSubject.Update();
        }

        public void BreakCurSkill()
        {
            if (_skillSubjects == null) return;
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                var perSkillSubject = _skillSubjects[i];
                perSkillSubject.Break();
            }
        }

        private void Update()
        {
            UpdateSkillSubjects();
        }

        void UpdateSkillSubjects()
        {
            for (int i = _skillSubjects.Count - 1; i >= 0; i--)
            {
                var skillSubject = _skillSubjects[i];
                if (skillSubject.Update() == SkillState.ENDED)
                {
                    _skillSubjects.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillSubject(skillSubject);
                }
            }
        }

        public List<SkillSubject> GetCurSkillSubjects()
        {
            return _skillSubjects;
        }

        public void CreateSkillAction(int skillID, int skillActionID, Matrix4x4 ownerWorldMatrixOnSkillActive, uint mainTargetID)
        {
            var skillAction = CombatLogicObjectPool.CreateSkillAction(skillActionID);
            skillAction.SetOwner(_owner);
            skillAction.skillID = skillID;
            skillAction.ownerWorldMatrixOnSkillActive = ownerWorldMatrixOnSkillActive;
            skillAction.delay = 0;
            if (mainTargetID != 0)
            {
                skillAction.mainTargetID = mainTargetID;
            }
            _skillActionManager.AddSkillAction(skillAction);
        }
    }
}
