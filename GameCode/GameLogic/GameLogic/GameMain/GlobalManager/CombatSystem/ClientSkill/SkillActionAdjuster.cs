﻿using Common.Data;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    static public class SkillActionAdjuster
    {
        static public Dictionary<int, int> GetChgSpellCD(this SkillAction skillAction)
        {
            Dictionary<int, int> result = skillAction.skillActionData.chgSpellCD;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.chgSpellCD, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSelfMovType(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.selfMovType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.selfMovType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetSelfMovArg(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.selfMovArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.selfMovArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<float>> GetAddBuffSelf(this SkillAction skillAction)
        {
            Dictionary<int, List<float>> result = skillAction.skillActionData.addBuffSelf;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.addBuffSelf, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, int> GetDelBuffSelf(this SkillAction skillAction)
        {
            Dictionary<int, int> result = skillAction.skillActionData.delBuffSelf;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.delBuffSelf, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<float>> GetAddBuffTarget(this SkillAction skillAction)
        {
            Dictionary<int, List<float>> result = skillAction.skillActionData.addBuffTarget;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.addBuffTarget, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, int> GetDelBuffTarget(this SkillAction skillAction)
        {
            Dictionary<int, int> result = skillAction.skillActionData.delBuffTarget;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.delBuffTarget, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public float GetFireDelayTime(this SkillAction skillAction)
        {
            float result = skillAction.skillActionData.fireDelayTime;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.fireDelayTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetOriginType(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.originType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.originType, (AdjustType)adjustData.adjustType);
            }

            //Debug.LogError("[SkillAction:ResetActiveOriginMatrix]=>0___Time:    " + Time.realtimeSinceStartup + ",actionID:  " + skillAction.actionID + ",result:  " + result + ",skillID:  " + skillAction.skillID);
            return result;
        }

        static public int GetAttackRegionType(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.attackRegionType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.attackRegionType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetAttackRegionArg(this SkillAction skillAction)
        {
            List<int> result = skillAction.skillActionData.attackRegionArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.attackRegionArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetOriginAdjust(this SkillAction skillAction)
        {
            List<int> result = skillAction.skillActionData.originAdjust;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.originAdjust, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetTargetFilterOrders(this SkillAction skillAction)
        {
            List<int> result = skillAction.skillActionData.targetFilterOrders;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterOrders, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<int>> GetTargetFilterArgs(this SkillAction skillAction)
        {
            Dictionary<int, List<int>> result = skillAction.skillActionData.targetFilterArgs;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterArgs, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetTargetMinCount(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.targetMinCount;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMinCount, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetTargetMaxCount(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.targetMaxCount;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMaxCount, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetRegionJudgeTime(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.regionJudgeTime;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.regionJudgeTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetDmgTypes(this SkillAction skillAction)
        {
            List<int> result = skillAction.skillActionData.dmgTypes;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgTypes, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgTypesRate(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgTypesRate;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgTypesRate, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgTypesAdd(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgTypesAdd;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgTypesAdd, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgRateLvlFactors(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgRateLvlFactors;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgRateLvlFactors, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgAddLvlFactors(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgAddLvlFactors;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgAddLvlFactors, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgCritRateLvlFactors(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgCritRateLvlFactors;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgCritRateLvlFactors, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgStrikeRateLvlFactors(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgStrikeRateLvlFactors;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgStrikeRateLvlFactors, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        //获取该行为的专精伤害系数
        static public float GetDmgProficientFactor(this SkillAction skillAction)
        {
            float result = skillAction.skillActionData.dmgProficientFactor;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgProficientFactor, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<float>> GetTargetMovPreBuff(this SkillAction skillAction)
        {
            Dictionary<int, List<float>> result = skillAction.skillActionData.targetMovPreBuff;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovPreBuff, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
        
        static public int GetTargetMovType(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.targetMovType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetTargetMovArg(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.targetMovArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, int> GetFireCosts(this SkillAction skillAction)
        {
            Dictionary<int, int> result = skillAction.skillActionData.fireCosts;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.fireCosts, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, float> GetFireAction(this SkillAction skillAction)
        {
            Dictionary<int, float> result = skillAction.skillActionData.fireAction;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.fireAction, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<SkillEventData> GetSkillEvents(this SkillAction skillAction)
        {
            List<SkillEventData> result = skillAction.skillActionData.skillEvents;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                if (adjustData.skillEvents != null && adjustData.skillEvents.Count != 0)
                {
                    result = adjustData.skillEvents;
                }
            }
            return result;
        }

        static public int GetDmgSpecialType(this SkillAction skillAction)
        {
            int result = skillAction.skillActionData.dmgSpecialType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgSpecialType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetDmgSpecialArg(this SkillAction skillAction)
        {
            List<float> result = skillAction.skillActionData.dmgSpecialArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.dmgSpecialArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<float>> GetFireDropItem(this SkillAction skillAction)
        {
            Dictionary<int, List<float>> result = skillAction.skillActionData.fireDropItem;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.fireDropItem, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
    }
}
