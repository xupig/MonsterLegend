﻿using GameMain.Entities;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public class SkillActionManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillActionManager
    {
        private EntityCreature _owner;

        private List<SkillAction> _skillActions = new List<SkillAction>();
        
        public SkillActionManager(EntityCreature ower)
        {
            _owner = ower;
            MogoEngine.MogoWorld.RegisterUpdate<SkillActionManagerUpdateDelegate>("SkillActionManager.Update", Update);
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("SkillActionManager.Update", Update);
        }

        public void AddSkillAction(SkillAction skillAction)
        {
            AdjustTools.AdjustSkillAction(skillAction, _owner.skillManager.GetCurAdjustSkillActionIDList());
            _skillActions.Insert(0, skillAction);
            UpdateSkillActions();
        }

        private void Update()
        {
            UpdateSkillActions();
        }

        void UpdateSkillActions()
        {
            for (int i = _skillActions.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActions[i];
                var result = skillAction.Update();
                if (result == SkillActionStage.ENDED)
                {
                    _skillActions.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillAction(skillAction);
                }
            }
        }
    }
}
