﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    public enum SkillActionStage
    {
        STANDBY,
        ACTIVE,
        ENDED
    }

    public class SkillActionMovement
    {
        public float startTime;

        public float baseSpeed;
        public MovementType movementType;
        public uint moveEntityID;
        public Vector3 targetPosition;
        public float offsetX;
        public float offsetY;
        public float accelerate;
        public float upSpeed;
        public float upAccelerate;
        public float maxMoveDistance;
    }

    public class SkillDropItem
    {
        public int id;
        public float rate;
        public uint minCount;
        public uint maxCount;
        public float minRadius;
        public float maxRadius;
        public int pickType;
        public float duration;
        public SkillDropItemOriginalPointType originalPointType;

        private Vector3 originalPosition;

        private const int DROP_ITEM_MAX_COUNT = 12;
        private const float DROP_ITEM_PER_ANGLE = 30;

        public SkillDropItem(int id, List<float> dataList)
        {
            this.id = id;
            rate = dataList[0];
            minCount = (uint)dataList[1];
            maxCount = (uint)dataList[2];
            minRadius = dataList[3] * 0.01f;
            maxRadius = dataList[4] * 0.01f;
            pickType = (int)dataList[5];
            duration = dataList[6];
            originalPointType = (SkillDropItemOriginalPointType)dataList[7];
        }

        public void SetOriginalPosition(Vector3 originalPosition)
        {
            this.originalPosition = originalPosition;
        }

        public bool CanDrop()
        {
            return UnityEngine.Random.Range(0f, 1f) <= rate;
        }

        public List<Vector2> GetRandomPositionList(bool bFly)
        {
            float startAngle = MogoMathUtils.Random(0, 360f);
            uint count = GetRandomCount();
            List<Vector2> randomPositionList = new List<Vector2>();
            int[] randomSequence = MogoMathUtils.RandomSequence(0, DROP_ITEM_MAX_COUNT - 1);

            float angle;
            float distance;
            Vector2 position;
            int index = 0;
            for (int i = 0; i < count; ++i)
            {
                if (index >= randomSequence.Length)
                {
                    index = 0;
                }
                angle = startAngle + randomSequence[index] * DROP_ITEM_PER_ANGLE;
                distance = GetRandomRadius();
                position = GetPositionByAngleAndDistance(angle, distance);
                index++;
                if (!IsCanMove(position, bFly))
                {
                    randomPositionList.Add(GetAroundCanMovePoint(position, bFly));
                }
                else
                {
                    randomPositionList.Add(position);
                }
            }
            return randomPositionList;
        }

        private Vector2 GetAroundCanMovePoint(Vector2 position, bool bFly)
        {
            Vector3 posVec3 = GameSceneManager.GetInstance().CheckPointAroundCanMove(position.x, position.y, bFly);
            return new Vector2(posVec3.x, posVec3.z);
        }

        private bool IsCanMove(Vector2 position, bool bFly)
        {
            return GameSceneManager.GetInstance().CheckCurrPointIsCanMove(position.x, position.y, bFly);
        }

        private Vector2 GetPositionByAngleAndDistance(float angle, float distance)
        {
            float x = originalPosition.x + distance * Mathf.Cos(angle);
            float y = originalPosition.z + distance * Mathf.Sin(angle);
            return new Vector2(x, y);
        }

        private uint GetRandomCount()
        {
            return (uint)UnityEngine.Random.Range(minCount, maxCount + 1);
        }

        private float GetRandomRadius()
        {
            return UnityEngine.Random.Range(minRadius, maxRadius);
        }
    }

    public class SkillAction
    {
        static public float DEFAULT_DURATION = 1;
        public bool isServer = false; 
        public int skillID;
        public int actionID;
        public float delay = 0f;

        public Matrix4x4 ownerWorldMatrixOnSkillActive;
        protected Matrix4x4 _ownerWorldMatrixOnActive;
        protected Matrix4x4 _targetWorldMatrixOnActive;
        protected Matrix4x4 _ownerWorldMatrixOnFixTime;
        protected Matrix4x4 _targetWorldMatrixOnFixTime;
        protected Matrix4x4 _ownerWorldMatrixOnJudge;
        protected Matrix4x4 _targetWorldMatrixOnJudge;
        protected Matrix4x4 _activeOriginMatrix;    //Action激活时的判定矩阵
        protected Matrix4x4 _judgePointMatrix;

        protected bool _haveTarget;
        protected List<int> _fixOriginAdjust;

        protected uint _mainTargetID;
        public uint mainTargetID
        {
            set 
            { 
                _mainTargetID = value;
                if (_actHandler != null) _actHandler.mainTargetID = value;
            }
        }

        protected EntityCreature _owner;
        protected ACTHandler _actHandler;

        protected SkillActionStage _curStage = SkillActionStage.STANDBY;
        public SkillActionStage curState
        {
            get { return this._curStage; }
        }

        protected SkillActionData _skillActionData;
        public SkillActionData skillActionData { get { return _skillActionData; } }
        protected List<SkillActionData> _adjustSkillActionDataList = new List<SkillActionData>();
        public List<SkillActionData> adjustSkillActionDataList { get { return _adjustSkillActionDataList; } }

        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();

        protected List<SkillActionMovement> _skillActionMovements = new List<SkillActionMovement>();

        protected List<uint> _targetIDList = new List<uint>();
        protected Dictionary<uint, hit_fly_params> _hitFlyTargetDict = new Dictionary<uint, hit_fly_params>();
        protected float _duration = 0;
        protected float _pastTime = 0;
        private float _startTimeStamp = 0;
        protected float _fireTime = 0;
        public float fireTime
        {
            get { return this._fireTime; }
        }

        protected RegionJudgeTime _regionJudgeTime;
        protected float _regionJudgeDelayTime = 0;
        public float regionJudgeDelayTime
        {
            get { return this._regionJudgeDelayTime; }
        }

        public SkillAction()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillActionData data)
        {
            //Debug.LogError("Reset:" + data);
            _duration = DEFAULT_DURATION;
            _pastTime = 0;
            actionID = data.actionID;
            mainTargetID = 0;
            _curStage = SkillActionStage.STANDBY;
            _skillActionData = data;
            _adjustSkillActionDataList.Clear();

            _ownerWorldMatrixOnActive = Matrix4x4.identity;
            _targetWorldMatrixOnActive = Matrix4x4.identity;
            _ownerWorldMatrixOnFixTime = Matrix4x4.identity;
            _targetWorldMatrixOnFixTime = Matrix4x4.identity;
            _ownerWorldMatrixOnJudge = Matrix4x4.identity;
            _targetWorldMatrixOnJudge = Matrix4x4.identity;
            _activeOriginMatrix = Matrix4x4.identity;
            _judgePointMatrix = Matrix4x4.identity;
        }

        private void CalculateDuration()
        {
            for (int i = 0; i < _skillActionMovements.Count; i++)
            {
                _duration = Mathf.Max(_skillActionMovements[i].startTime - RealTime.time, _duration);
            }
            for (int i = 0; i < _skillEventList.Count; i++)
            {
                _duration = Mathf.Max(_skillEventList[i].delay, _duration);
            }
        }

        private void ShowEarlyWarning()
        {
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self 
                && this._owner != PlayerAvatar.Player) 
            { 
                return; 
            }
            OriginType originType = (OriginType)this.GetOriginType();
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust,duration);
        }

        private void AttackRangePreview()
        {
            //LoggerHelper.Error("[SkillAction:AttackRangePreview]=>1_________________________fireTime:   " + _fireTime);
            float duration = 0.5f;// _fireTime - RealTime.time;
            if (duration <= 0) return;
            //
            OriginType originType = (OriginType)this.GetOriginType();
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            TargetRangeType targetRangeType = TargetRangeType.CircleRange;
            List<int> targetRangeParam = new List<int>() { 500};// this.GetAttackRegionArg();
            //LoggerHelper.Error("[SkillAction:AttackRangePreview]=>2_________________________fireTime:   " + _fireTime);
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust,duration);
        }


        public void AddAdjustSkillActionData(SkillActionData skillActionData)
        {
            if (_adjustSkillActionDataList.Contains(skillActionData)) return;
            _adjustSkillActionDataList.Add(skillActionData);
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        
        private void ResetActiveOriginMatrix()
        {          
            var originType = (OriginType)this.GetOriginType();
            _activeOriginMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            if (!_haveTarget && originType != OriginType.Self)
            {
                _activeOriginMatrix = _ownerWorldMatrixOnActive;
            }
            float offsetX = 0;
            float offsetY = 0;
            if (_fixOriginAdjust == null)
            {
                LoggerHelper.Error(string.Format("ResetActiveOriginMatrix, fixOriginAdjust is null, ActionID = {0}.", actionID));
            }
            if (_fixOriginAdjust.Count > 0)
            {
                offsetX = _fixOriginAdjust[0] * 0.01f;
                offsetY = _fixOriginAdjust[1] * 0.01f;
            }
            Matrix4x4 matrixPositionOffset = Matrix4x4.identity;
            matrixPositionOffset.SetColumn(3, new Vector4(offsetY, 0, offsetX, 1));
            _activeOriginMatrix = _activeOriginMatrix * matrixPositionOffset;
            if (originType != OriginType.Custom)
            {                                      
                _actHandler.actionActiveOrigin = _activeOriginMatrix.position();
            }
            //记录原点类型
            if (originType == OriginType.Custom
                && _actHandler.actionActiveOrigin == Vector3.zero)
            {
                _actHandler.actionActiveOrigin = (_owner as EntityAirVehicle).attackTargetPosition;
            }
        }

        private void ResetFireTime()
        {
            var fireDelayTime = this.GetFireDelayTime();
            if (fireDelayTime >= 0)
            {
                _fireTime = RealTime.time + fireDelayTime * 0.001f;
            }
            else if ((OriginType)this.GetOriginType() != OriginType.Custom)
            {
                var dis = (_activeOriginMatrix.position() - _owner.position).magnitude;
                float flyTime = Mathf.Abs(fireDelayTime * 0.01f);
                _fireTime = RealTime.time + dis / flyTime;   
            }
            else if ((OriginType)this.GetOriginType() == OriginType.Custom)
            {
				if (_owner == null || !(_owner is EntityAirVehicle) || (_owner as EntityAirVehicle).airVehiclePath.riderSlot == null) return;     //考虑到空中载具销毁了,它的技能应该无效
                //计算到目标点距离
                var dis = (_activeOriginMatrix.position() - _owner.position).magnitude;
                float flyTime = Mathf.Abs(fireDelayTime * 0.01f);
                _fireTime = RealTime.time + dis / flyTime;
            }
        }

        private void ResetRegionJudgeTime()
        {
            var data = this.GetRegionJudgeTime();
            if (data < 0)
            {
                _regionJudgeDelayTime = RealTime.time + Mathf.Abs(data) * 0.001f;
                _regionJudgeTime = RegionJudgeTime.OnFixTime;
            }
            else
            {
                _regionJudgeTime = (RegionJudgeTime)data;
            }
        }

        private void ResetTargetWorldMatrixOnActive()
        {
            _targetWorldMatrixOnActive = Matrix4x4.identity;
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnActive = targetEntity.GetTransform().localToWorldMatrix;
                _haveTarget = true;
            }
            else
            {
                _haveTarget = false;
            }
            //根据
            if ((OriginType)this.GetOriginType() == OriginType.Custom)
            {
                _actHandler.actionActiveOrigin = (_owner as EntityAirVehicle).attackTargetPosition;
                _targetWorldMatrixOnActive = Matrix4x4.TRS(_actHandler.actionActiveOrigin, Quaternion.identity, Vector3.one);
            }
        }

        private void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = this.GetSkillEvents();
            for (int i = 0; i < skillEvents.Count; i++)
            {
                var eventData = skillEvents[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                _skillEventList.Add(eventData);
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.Clear();
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public void Clear()
        {
            _owner = null;
            _skillActionData = null;
            _skillActionMovements.Clear();
        }

        bool IsAllEnd()
        {
            if (_skillActionMovements.Count > 0) return false;
            if (_skillEventList.Count > 0) return false;
            if (_fireTime > 0) return false;
            return true;
        }

        public SkillActionStage Update()
        {
            if (_curStage == SkillActionStage.STANDBY)
            {
                Begin();
            }
            else if (_curStage == SkillActionStage.ACTIVE)
            {
                //_pastTime += Time.deltaTime;
                _pastTime = RealTime.time - _startTimeStamp;
                if (_pastTime >= _duration && IsAllEnd())
                {
                    End();
                }
                else
                {
                    Doing();
                }
            }
            return _curStage;
        }

        void Begin()
        {
            _curStage = SkillActionStage.ACTIVE;
            OnBegin();
            EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_SKILL_ACTION, this.actionID.ToString());
            if (this._mainTargetID == 0 && ((OriginType)this.GetOriginType()) == OriginType.Target)
            {
                End();
            }
        }

        void Doing()
        {
            DoingEvents();
            Moving();
            CheckRegionJudgeDelayTime();
            Judge();
        }
        
        virtual protected void OnBegin()
        {
            _startTimeStamp = RealTime.time;
            if (_owner.GetTransform() == null) return;//这里不合理，临时加
            _ownerWorldMatrixOnActive = _owner.GetTransform().localToWorldMatrix;
            ResetRegionJudgeTime();
            ResetTargetWorldMatrixOnActive();
            ResetActiveOriginMatrix();
            ResetFireTime();
            ResetSkillEventList();
            ChangeCD();
            SetSelfMovement();
            AddSelfBuffer();
            DelSelfBuffer();
            CalculateDuration();
            //AttackRangePreview();
            if (_regionJudgeTime == RegionJudgeTime.OnSkillActive
                ||  _regionJudgeTime == RegionJudgeTime.OnActionActive)
                ShowEarlyWarning();
            Doing();
        }

        public void End()
        {
            _curStage = SkillActionStage.ENDED;
        }


        #region 持续效果

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void Moving()
        {
            for (int i = _skillActionMovements.Count - 1; i >= 0; i--)
            {
                var movement = _skillActionMovements[i];
                if (movement.startTime < RealTime.time)
                {
                    _skillActionMovements.RemoveAt(i);
                    _owner.checkCrashWall = false;
                    ExecuteMovement(movement);
                }
            }
        }

        void ExecuteMovement(SkillActionMovement movement)
        {
            var moveEntity = CombatSystemTools.GetCreatureByID(movement.moveEntityID);
            if (moveEntity == null) return;
            var srcPosition = moveEntity.position;

            Vector3 targetPosition = movement.targetPosition;
            var dir = targetPosition - srcPosition;
			float maxMoveDistance = Mathf.Abs(movement.maxMoveDistance);
            var dis = Mathf.Min(dir.magnitude, maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
			}
			if (_hitFlyTargetDict.ContainsKey(moveEntity.id))
			{
                moveEntity.actor.GetComponent<ActorDeathController>().MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate, false, false);
			}
		}
		
		void CheckRegionJudgeDelayTime()
		{
            if (_regionJudgeDelayTime == 0 || _regionJudgeDelayTime > RealTime.time) return;
            _regionJudgeDelayTime = 0;
            if (_owner.GetTransform() != null)
            {
                _ownerWorldMatrixOnFixTime = _owner.GetTransform().localToWorldMatrix;
            }
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnFixTime = targetEntity.GetTransform().localToWorldMatrix;
            }
            ResetActiveOriginMatrix();
            //AttackRangePreview();
            ShowEarlyWarning();
            _actHandler.OnFixTime(this._owner.actor);
        }

        void Judge()
        {
            if (_fireTime == 0 || _fireTime > RealTime.time) return;
            _fireTime = 0;
            ResetWorldMatrixOnJudge();
            DetectTargets();
            CalculateDamage();
            TriggerAction();
            AddBufferTarget();
            DelBuffTarget();
            SetTargetMovPreBuff();
            SetEP();
            HandleDropItems();
        }

        void ResetWorldMatrixOnJudge()
        {
            if (_owner.GetTransform() != null)
            {
                _ownerWorldMatrixOnJudge = _owner.GetTransform().localToWorldMatrix;
            }
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnJudge = targetEntity.GetTransform().localToWorldMatrix;
            } 
        }

        Matrix4x4 FixSrcMatrix(Matrix4x4 srcMatrix, Matrix4x4 fixMatrix)
        {
            var targetPosition = fixMatrix.position();
			var sourcePosition = srcMatrix.position();
			var direction = targetPosition - sourcePosition;
            var quaternion = Quaternion.identity;
            if (direction != Vector3.zero)
            {
                quaternion = Quaternion.LookRotation(direction.normalized);
            }
			var result = Matrix4x4.identity;
			result.SetTRS (sourcePosition + direction, quaternion, Vector3.one);
			return result;
        }

        void ResetFixOriginAdjust(bool fix = false)
        {
            var originAdjust = this.GetOriginAdjust();
            _fixOriginAdjust = originAdjust;
            if (fix && originAdjust.Count >= 6)
            {
                _fixOriginAdjust = new List<int>();
                for (int i = 3; i < 6; i++)
                { //截取后六个参数设置
                    _fixOriginAdjust.Add(originAdjust[i]);
                }
            }
        }

        Matrix4x4 GetRegionJudgeMatrix(RegionJudgeTime regionJudgeTime, OriginType originType)
        {
            //LoggerHelper.Error("[SkillAction:GetRegionJudgeMatrix]=>11_________regionJudgeTime: " + regionJudgeTime + ",originType: " + originType + ",skillID:  " + skillID);
            if (this._owner.GetTransform() == null)
            {
                return ownerWorldMatrixOnSkillActive;
            }
            Matrix4x4 curOwnerMatrix = this._owner.GetTransform().localToWorldMatrix;
            Matrix4x4 srcMatrix = curOwnerMatrix;
            Matrix4x4 tarMatrix;
            ResetFixOriginAdjust();
            switch (regionJudgeTime)
            {
                case RegionJudgeTime.OnSkillActive:
                    srcMatrix = ownerWorldMatrixOnSkillActive;
                    break;
                case RegionJudgeTime.OnActionActive:
                    srcMatrix = _ownerWorldMatrixOnActive;
                    if (originType == OriginType.Target)
                    {
                        tarMatrix = _targetWorldMatrixOnActive;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto
                        || originType == OriginType.Custom)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else 
                        {
                            tarMatrix = _targetWorldMatrixOnActive;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    
                    break;
                case RegionJudgeTime.OnActionJudge:
                    srcMatrix = this._ownerWorldMatrixOnJudge;
                    if (originType == OriginType.Target)
                    {
                        tarMatrix = this._targetWorldMatrixOnJudge;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else
                        {
                            tarMatrix = _targetWorldMatrixOnJudge;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    break;
                case RegionJudgeTime.OnFixTime:
                    srcMatrix = this._ownerWorldMatrixOnFixTime;
                    if (originType == OriginType.Target)
                    { 
                        tarMatrix = this._targetWorldMatrixOnFixTime;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else
                        {
                            tarMatrix = _targetWorldMatrixOnFixTime;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    else if (originType == OriginType.Custom)
                    {
                        //LoggerHelper.Error("[SkillAction:GetRegionJudgeMatrix]=>//////////////////////////////////////////");
                    }
                    break;
            }
            return srcMatrix;
        }

        #endregion 持续效果

        #region 瞬间效果

        void ChangeCD()
        {
            var changeData = this.GetChgSpellCD();
            if (changeData == null) return;
            foreach (var kv in changeData)
            {
                var skillGroup = kv.Key;
                var time = kv.Value;
                this._owner.skillManager.ChangeSkillGroupCD(skillGroup, time);
            }
        }

        List<float> FixMoveArgs(MovementType movementType, List<float> srcMoveArgs)
        {
            List<float> curMoveArgs = srcMoveArgs;
            if (MovementType.AccordingAuto == movementType)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target == null && srcMoveArgs.Count >= 12)
                {
                    curMoveArgs = new List<float>();
                    for (int i = 6; i < 12; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            return curMoveArgs;
        }

        void SetSelfMovement()
        {
            MovementType moveType = (MovementType)this.GetSelfMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetSelfMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            Matrix4x4 moveTarget = Matrix4x4.identity;
            Matrix4x4 moveSource = _ownerWorldMatrixOnActive;
            var mainTargetEntity = CombatSystemTools.GetCreatureByID(this._mainTargetID);
            switch(moveType){
                case MovementType.AccordingSelf:
                    moveTarget = moveSource;
                    break;
                case MovementType.AccordingTarget:
                    if (mainTargetEntity != null && mainTargetEntity.GetTransform() != null) moveTarget = mainTargetEntity.GetTransform().localToWorldMatrix;
                    break;
                case MovementType.AccordingAuto:
                    if (mainTargetEntity == null)
                    {
                        moveTarget = moveSource;
                    }
                    else {
                        if (mainTargetEntity != null && mainTargetEntity.GetTransform() != null) moveTarget = mainTargetEntity.GetTransform().localToWorldMatrix;
                    }
                    break;
                default:
                    return;
            }

            if (moveTarget == Matrix4x4.identity) return;
            var movement = GetActionMovement(_owner, moveSource, moveTarget, moveType, curMoveArgs, true);
            _skillActionMovements.Add(movement);
        }

        SkillActionMovement GetActionMovement(EntityCreature moveEntity, Matrix4x4 moveSource, Matrix4x4 moveTarget, MovementType moveType, List<float> curMoveArgs, bool notifyForwardDirection = false)
        {
            SkillActionMovement movement = new SkillActionMovement();
            try
            {
                movement.offsetX = curMoveArgs[0] * 0.01f;
                movement.offsetY = curMoveArgs[1] * 0.01f;
                movement.maxMoveDistance = curMoveArgs[2] * 0.01f;
                var delay = curMoveArgs[3] * 0.001f;
                movement.startTime = RealTime.time + delay;
                movement.baseSpeed = curMoveArgs[4] * 0.01f;
                movement.accelerate = curMoveArgs[5] * 0.01f;
                if (curMoveArgs.Count >= 7) movement.upSpeed = curMoveArgs[6] * 0.01f;
                if (curMoveArgs.Count >= 8) movement.upAccelerate = curMoveArgs[7] * 0.01f;
                movement.movementType = moveType;
                movement.moveEntityID = moveEntity.id;
                if (moveSource == moveTarget)
                {
                    Matrix4x4 m1 = Matrix4x4.identity;

                    //纠正Forward方向
                    if (notifyForwardDirection && Mathf.Sign(movement.offsetX) != Mathf.Sign(movement.maxMoveDistance))
                    {
                        movement.offsetX = 0;
                    }

                    m1.SetColumn(3, new Vector4(movement.offsetY, 0, movement.offsetX, 1));
                    moveSource = moveSource * m1;
                    movement.targetPosition = new Vector3(moveSource.m03, moveSource.m13, moveSource.m23);
                }
                else
                {
                    var direction = (moveTarget.position() - moveSource.position()).normalized;
					var fontDriection = direction * movement.offsetX;

                    //纠正Forward方向
                    if (notifyForwardDirection)
                    {
                        float fontDistance = fontDriection.magnitude;
                        float distance = (moveTarget.position() - moveSource.position()).magnitude;
                        if (Mathf.Sign(movement.maxMoveDistance) != Mathf.Sign(distance - fontDistance))
                        {
                            fontDriection = moveSource.position() - moveTarget.position();
                        }
                    }

                    var rightDirection = (new Vector3(-direction.z, direction.y, direction.x)).normalized * movement.offsetY;
                    var offset = fontDriection + rightDirection;
                    var tarPosition = moveTarget.position();
                    movement.targetPosition = new Vector3(tarPosition.x + offset.x, tarPosition.y + offset.y, tarPosition.z + offset.z);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("Movement Error: in skillAction:" + this.actionID + ":" + ex.Message);
            }
            return movement;
        }

        void AddSelfBuffer()
        {
            var buffData = this.GetAddBuffSelf();
            if (buffData == null) return;
            var en = buffData.GetEnumerator();
            while (en.MoveNext())
            {
                int buffID = en.Current.Key;
                var rate = en.Current.Value[0];
                var duration = en.Current.Value[1];
                if (UnityEngine.Random.Range(0f, 1f) < rate)
                {
                    _owner.bufferManager.AddBuffer(buffID, duration, true);
                }
            }
        }

        void DelSelfBuffer()
        {
            var buffData = this.GetDelBuffSelf();
            if (buffData == null) return;
            var en = buffData.GetEnumerator();
            while (en.MoveNext())
            {
                int buffID = en.Current.Key;
                var rate = en.Current.Value;
                if (UnityEngine.Random.Range(0f, 1f) < rate)
                {
                    _owner.bufferManager.RemoveBuffer(buffID);
                }
            }
        }

        void DetectTargets()
        {
            //Debug.LogError("SkillAction DetectTargets:");
            
            //找目标
            _targetIDList.Clear();
            var targetType = (TargetType)_skillActionData.targetType;
            if (targetType == TargetType.MySelf)
            {
                _targetIDList.Add(_owner.id);
                return;
            }
            var originType = (OriginType)this.GetOriginType();
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            List<int> targetFilterOrders = this.GetTargetFilterOrders();
            Dictionary<int, List<int>> targetFilterArgs = this.GetTargetFilterArgs();
            var targetMinCount = this.GetTargetMinCount();
            var targetMaxCount = this.GetTargetMaxCount();
            _judgePointMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            List<uint> entities = EntityFilter.GetEntitiesByRangeType(_judgePointMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust, EntityFilterType.SKILL_ACTION_FILTER);
            var candidateList = TargetFilter.GetCreaturesByTargetType(_owner, entities, targetType);
            _targetIDList = TargetFilter.FilterTarget(_owner, candidateList, targetFilterOrders, targetFilterArgs, targetMinCount, targetMaxCount);
            _actHandler.actionJudgeOrigin = _judgePointMatrix.position();
            _actHandler.OnJudge(this._owner.actor);
        }

        void CalculateDamage()
        {
            _hitFlyTargetDict.Clear();
            for (int i = 0; i < _targetIDList.Count; i++)
            {
                uint entityID = _targetIDList[i];
                EntityCreature target = MogoWorld.GetEntity(entityID) as EntityCreature;
                if (target != null)
                {
                    SkillDamage result;
                    if (CheckTargetHasSelectedBossPart(target))
                    {
                        result = CalculateDamageByBossPart(target, _targetIDList.Count);
                    }
                    else
                    {
                        result = SkillCalculate.CalculateDamage(this._owner, target, this.skillID, this, _targetIDList.Count);
                    }
                    if (_owner is PlayerAvatar) //GM指令
                    {
                        if (PlayerAvatarState.isPowerfully)
                        {
                            result.value = 100000;
                        }
                        else if (PlayerAvatarState.isWeakly)
                        {
                            result.value = 0;
                        }
                    }
                    if (result.mode == 0)   //不计算伤害
                    {
                        return;
                    }
                    _actHandler.OnTargetBeHit(_owner.actor, target, delegate()
					{
						if (result.value > 0)
						{
							TargetBeHit(target);
						}
                    });
                    bool isPlayer = target == PlayerAvatar.Player;
                    AttackType attackType = CombatSystemTools.DamageMode2AttackType(result.mode);
                    if (CombatSystemTools.CanShowDamage(this._owner, target) && !(attackType == AttackType.ATTACK_HIT && result.value == 0))
                    {
                        if (target.actor != null)
                        {
                            PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, -result.value, attackType, isPlayer);
                        }
                    }
                    var cur_hp = target.cur_hp < result.value ? 0 : target.cur_hp - result.value;
                    MogoWorld.SynEntityAttr(target, EntityPropertyDefine.cur_hp, cur_hp);
                    if (target.cur_hp == 0)
                    {
                        HandleTargetBeHitFlyOnDeath(target);
                        target.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
                        if (target is EntityDummy)
                        {
                            (target as EntityDummy).BeKilledBy(this._owner);
                        }
                    }
                }
            }
        }

        private void HandleTargetBeHitFlyOnDeath(EntityCreature target)
        {
            if (!(target is EntityMonsterBase)) return;
            EntityMonsterBase monster = target as EntityMonsterBase;
            bool needToBeHitFly = CheckTargetNeedToBeHitFly(monster);
            if (needToBeHitFly)
            {
                hit_fly_params hitFlyParams = hit_fly_params_helper.GetRandomHitFlyParams();
                monster.actorDeathController.flyingTime = hitFlyParams.__duration * 0.001f;
                _hitFlyTargetDict.Add(target.id, hitFlyParams);
            }
        }

        private bool CheckTargetNeedToBeHitFly(EntityCreature target)
        {
            if (!(_owner is EntityAvatar) && !(_owner is EntityPet) && !(_owner is EntityVehicle)) return false;
            if (!target.bufferManager.ContainsBuffer(hit_fly_params_helper.GetHitFlyHpBuffID()))
            {
                return false;
            }
            return target.bufferManager.CanAddBuffer(hit_fly_params_helper.GetHitFlyBuffID());
        }

        private bool CheckTargetHasSelectedBossPart(EntityCreature target)
        {
            if (!(target is EntityMonsterBase))
            {
                return false;
            }
            EntityMonsterBase targetMonster = target as EntityMonsterBase;
            return targetMonster.partManager.CurrentSelectedSubject != null;
        }

        private SkillDamage CalculateDamageByBossPart(EntityCreature target, int targetCnt)
        {
            EntityMonsterBase targetMonster = target as EntityMonsterBase;
            SkillDamage result = targetMonster.partManager.CalculateDamage(this._owner, this.skillID, this, targetCnt);
            return result;
        }

        void TargetBeHit(EntityCreature target)
        {
            if (target.actor != null)
            {
                //Debug.LogError("TargetBeHit:" + target.id);
                if (CombatSystemTools.CanShowDamage(this._owner, target))
                {
                    var hlSetting = global_params_helper.GetHighLightSetting();
                    target.actor.equipController.HighLight(hlSetting[0], hlSetting[1]);
                }
                target.OnHit();
            }
        }

        void TriggerAction()
        {
			if (_targetIDList.Count > 0 && this.GetFireAction() != null) //受击目标数大于0就代表可以触发新行为
            {
                foreach (var kv in this.GetFireAction())
                {
                    if (kv.Value >= UnityEngine.Random.Range(0f, 1f))
                    {
                        this._owner.skillManager.AddSkillActionToSkillSubject(this.skillID, kv.Key, ownerWorldMatrixOnSkillActive, _mainTargetID);
                    }
                }
            }
        }

        void AddBufferTarget()
        {
            if (_targetIDList.Count == 0 || this.GetAddBuffTarget().Count == 0) return;
            var data = this.GetAddBuffTarget();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0];
                var duration = en.Current.Value[1];
                if (UnityEngine.Random.Range(0f, 1f) > rate) continue;
                for (int i = 0; i < _targetIDList.Count; i++)
                {
                    var target = CombatSystemTools.GetCreatureByID(_targetIDList[i]);
                    if (target != null)
                    {
                        if (target.stateManager.InState(state_config.AVATAR_STATE_DEATH)) continue;
                        target.bufferManager.AddBuffer(buffid, duration, true);
                    }
                }
            }       
        }

        void DelBuffTarget()
        {
            if (_targetIDList.Count == 0 || this.GetDelBuffTarget().Count == 0) return;
            var data = this.GetDelBuffTarget();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                for (int i = 0; i < _targetIDList.Count; i++)
                {
                    var target = CombatSystemTools.GetCreatureByID(_targetIDList[i]);
                    if (target.stateManager.InState(state_config.AVATAR_STATE_DEATH)) continue;
                    if (target != null)
                    {
                        target.bufferManager.RemoveBuffer(buffid);
                    }
                }
            }       
        }
        
        void AddBufferSelf()
        {
            if (this.GetAddBuffSelf().Count <= 0) return;
            var data = this.GetAddBuffSelf();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0];
                var duration = en.Current.Value[1];
                if (UnityEngine.Random.Range(0f, 1f) <= rate)
                {
                    _owner.bufferManager.AddBuffer(buffid, duration, true);
                }
            } 
        }

        void DelBufferSelf()
        {
            if (this.GetDelBuffSelf() == null) return;
            var data = this.GetDelBuffSelf();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value;
                if (UnityEngine.Random.Range(0f, 1f) <= rate)
                {
                    _owner.bufferManager.RemoveBuffer(buffid);
                }
            } 
        }

        void SetTargetMovement(EntityCreature moveEntity)
        {
            MovementType moveType = (MovementType)GetTargetMovType(moveEntity.id);
            if (moveType == MovementType.None) return;
            var moveArgs = GetTargetMovArg(moveEntity.id);
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            Matrix4x4 moveTarget = Matrix4x4.identity;
            Matrix4x4 moveSource = _owner.GetTransform().localToWorldMatrix;
            //LoggerHelper.Error("[SkillAction:SetTargetMovement]=>1_______________________moveType:  " + moveType + ",_owner:    " + _owner);
            switch (moveType)
            {
                case MovementType.None:
                    return;
                case MovementType.AccordingSelf:
                    moveTarget = moveSource;
                    break;
                case MovementType.AccordingTarget:
                    moveTarget = moveEntity.GetTransform().localToWorldMatrix;
                    moveEntity.checkCrashWall = true;
                    break;
                case MovementType.AccordingJudgePoint:
                    moveSource = _judgePointMatrix;
                    moveTarget = _judgePointMatrix;
                    break;
                case MovementType.AccordingJudgeTarget:
                    moveSource = _judgePointMatrix;
                    moveTarget = moveEntity.GetTransform().localToWorldMatrix;
                    break;
                default:
                    return;
            }
            if (moveTarget == Matrix4x4.identity) return;
            var movement = GetActionMovement(moveEntity, moveSource, moveTarget, moveType, curMoveArgs);
            var dir = movement.targetPosition - moveEntity.position;
            moveEntity.actor.FaceTo(moveEntity.position - dir);
            this.ExecuteMovement(movement);
        }

        int GetTargetMovType(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return _hitFlyTargetDict[id].__target_mov_type;
            }
            return this.GetTargetMovType();
        }

        List<float> GetTargetMovArg(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return data_parse_helper.ParseListFloat(_hitFlyTargetDict[id].__target_mov_arg);
            }
            return this.GetTargetMovArg();
        }

        void SetTargetMovPreBuff()
        {
            var targetMovPreBuff = this.GetTargetMovPreBuff();
            for (int i = 0; i < _targetIDList.Count; i++)
            {
                bool isHitFlyTarget = false;
                var target = CombatSystemTools.GetCreatureByID(_targetIDList[i]);
                if (target != null && target.actor != null)
                {
                    var movePreBuff = targetMovPreBuff;
                    if (_hitFlyTargetDict.ContainsKey(_targetIDList[i]))
                    {
                        //死亡击飞时，用击飞的BUFF代替当前的受击BUFF
                        movePreBuff = hit_fly_params_helper.GetHitFlyTargetMovPreBuff();
                        isHitFlyTarget = true;
                    }
                    if (!isHitFlyTarget && target.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                        continue;
                    float rateMax = 1f;
                    foreach (var pair in movePreBuff)
                    {
                        var buffid = pair.Key;
                        var rate = pair.Value[0];
                        var duration = pair.Value[1];
                        if (UnityEngine.Random.Range(0f, rateMax) <= rate)
                        {
                            if (target.bufferManager.AddBuffer(buffid, duration, true))
                            {
                                target.actor.FaceTo(_owner.position);
                                if (pair.Value.Count > 2) //
                                {
                                    SetTargetMovement(target);
                                }
                            }
                            break;
                        }
                        rateMax -= rate;
                    }
                }
            }
        }

        protected void SetEP()
        {
            if (_targetIDList.Count > 0) //受击目标数大于0就代表可以触发新行为
            {
                var usecosts = this.GetFireCosts();
                if (_owner is EntityAvatar && usecosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
                {
                    int oldEp = _owner.cur_ep;
                    Int16 cur_ep = (Int16)(_owner.cur_ep - usecosts[public_config.ITEM_SPECIAL_SPELL_ENERGY]);
                    cur_ep = (Int16)Mathf.Min(_owner.max_ep, Mathf.Max(0, cur_ep));
                    MogoWorld.SynEntityAttr(_owner, EntityPropertyDefine.cur_ep, cur_ep);
                    if (_owner == PlayerAvatar.Player)
                    {
                        EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.ADD_EP, oldEp, cur_ep);
                    }
                }
            }
        }

        private void HandleDropItems()
        {
            if (_targetIDList.Count == 0) return;
            var fireDropItem = this.GetFireDropItem();
            if (fireDropItem.Count == 0)
            {
                return;
            }
            var en = fireDropItem.GetEnumerator();
            while (en.MoveNext())
            {
                HandleSingleDropItem(en.Current.Key, en.Current.Value);
            }
        }

        private void HandleSingleDropItem(int id, List<float> args)
        {
            var skillDropItem = new SkillDropItem(id, args);
            if (!skillDropItem.CanDrop())
            {
                //LoggerHelper.Info(string.Format("处理SkillAction为{0}的掉落id为{1}的掉落物，概率计算不掉落", this.actionID, id));
                return;
            }

            Vector3 originalPosition = GetOriginalPosition(skillDropItem.originalPointType);
            skillDropItem.SetOriginalPosition(originalPosition);
            List<Vector2> randomPositionList = skillDropItem.GetRandomPositionList(_owner.actor.modifyLayer);
            //LoggerHelper.Info(string.Format("处理SkillAction为{0}的掉落id为{1}的掉落物，掉落{2}个", this.actionID, id, randomPositionList.Count));
            Vector2 position;
            for (int i = 0; i < randomPositionList.Count; ++i)
            {
                position = randomPositionList[i];
                MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_DROP_ITEM, (entity) =>
                {
                    var dropItem = entity as GameMain.Entities.EntityDropItem;
                    dropItem.type_id = (UInt32)id;
                    dropItem.owner_id = (UInt32)_owner.id;
                    if (_owner is EntityAvatar)
                    {
                        dropItem.owner_type = public_config.ENTITY_TYPE_AVATAR;
                    }
                    else if (_owner is EntityMonsterBase)
                    {
                        dropItem.owner_type = public_config.ENTITY_TYPE_MONSTER;
                    }
                    dropItem.owner_pve_camp = (byte)_owner.GetCampPveType();
                    dropItem.owner_pvp_camp = (byte)_owner.GetCampPvpType();
                    dropItem.pickup_type = (byte)skillDropItem.pickType;
                    dropItem.isServerItem = false;
                    dropItem.duration = skillDropItem.duration;
                    dropItem.SetPosition(new Vector3(position.x, originalPosition.y, position.y));
                });
            }
        }

        private Vector3 GetOriginalPosition(SkillDropItemOriginalPointType originalPointType)
        {
            if (originalPointType == SkillDropItemOriginalPointType.ATTACKER)
            {
                return _owner.position;
            }
            else
            {
                if (_targetIDList.Count == 0)
                {
                    return _owner.position;
                }
                int randomTargetIndex = MogoMathUtils.Random(0, _targetIDList.Count);
                if (MogoWorld.GetEntity(_targetIDList[randomTargetIndex]) == null)
                {
                    return _owner.position;
                }
                else
                {
                    Vector3 direction = Vector3.zero;
                    EntityCreature creature = MogoWorld.GetEntity(_targetIDList[randomTargetIndex]) as EntityCreature;
                    if (originalPointType == SkillDropItemOriginalPointType.RANDOM_TARGET_EDGE)
                    {
                        direction = (_owner.position - creature.position).normalized * creature.hitRadius;
                    }
                    return creature.position + direction;
                }
            }
        }
        #endregion
    }
}
