﻿using Common.ClientConfig;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.States;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public enum SkillState
    {
        STANDBY,
        ACTIVE,
        ENDED
    }

    public struct SkillBuffer
    {
        public float delay;
        public int bufferID;
        public float duration;
    }

    public class SkillSubject
    {
        public int skillID;

        public Matrix4x4 ownerWorldMatrixOnActive;

        EntityCreature _owner;
        SkillActionManager _skillActionManager;
        ACTHandler _actHandler;

        uint _mainTargetID;
        public uint mainTargetID 
        { 
            get { return _mainTargetID; }
            private set 
            {
                _mainTargetID = value;
                if (_actHandler != null) _actHandler.mainTargetID = value;
            }
        }

        uint _lockTargetID;

        float _alwaysFaceTargetEndTime = 0;

        SkillState _curState = SkillState.STANDBY;
        public SkillState curState
        {
            get { return _curState; }
        }

        bool _isBreaked = false;
        
        SkillData _skillData;
        public SkillData skillData { get { return _skillData; } }

        List<SkillData> _adjustSkillDataList = new List<SkillData>();
        public List<SkillData> adjustSkillDataList { get { return _adjustSkillDataList; } }

        List<SkillEventData> _skillEventList = new List<SkillEventData>();
        List<SkillAction> _skillActionList = new List<SkillAction>();
        List<SkillBuffer> _skillBufferList = new List<SkillBuffer>();

        List<uint> _oldTargetIDList = new List<uint>();

        float _duration = 1;
        public float duration { get { return _duration; } }
        float _startTimeStamp = 0;
        float _pastTime = 0; 
        bool _checkCut = false;

        public SkillSubject()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillData data)
        {
            _curState = SkillState.STANDBY;
            _isBreaked = false;
            _pastTime = 0;       
            _skillData = data;
            mainTargetID = 0;
            _alwaysFaceTargetEndTime = this.GetFaceAlways() * 0.001f + RealTime.time;
            _duration = this.GetActionCutTime() * 0.001f;
            skillID = _skillData.skillID;
            _adjustSkillDataList.Clear();
            _oldTargetIDList.Clear();
            _checkCut = false;
        }

        public void AddAdjustSkillData(SkillData skillData)
        {
            if (_adjustSkillDataList.Contains(skillData)) return;
            _adjustSkillDataList.Add(skillData);
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        public List<SkillAction> GetSkillActionList()
        {
            return _skillActionList;
        }

        void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = this.GetSkillEvents();
            for (int i = 0; i < skillEvents.Count; i++)
            {
                _skillEventList.Add(skillEvents[i]);
            }
        }

        void ResetSkillActionList()
        {
            _skillActionList.Clear();
            var actionIDList = this.GetSkillActions();
            var skillActionProbs = this.GetSkillActionProbs();
            var skillActionActiveTimes = this.GetSkillActionActiveTimes();
            for (int i = 0; i < actionIDList.Count; i++)
            {
                float prob = skillActionProbs.Count > i ? skillActionProbs[i] : 1;
                if (UnityEngine.Random.Range(0f, 1f) > prob) continue;
                float delay = skillActionActiveTimes[i] * 0.001f;
                AddSkillAction(actionIDList[i], delay);
            }
        }

        void ResetSkillBufferList()
        { 
            _skillBufferList.Clear();
            var addBuffsOnCast = this.GetAddBuffsOnCast();
            if (addBuffsOnCast == null) return;
            foreach (var kv in addBuffsOnCast)
            {              
                var skillBuffer = new SkillBuffer();
                skillBuffer.bufferID = kv.Key;
                skillBuffer.delay = kv.Value[0] * 0.001f;
                skillBuffer.duration = kv.Value[1];
                _skillBufferList.Add(skillBuffer);
            }
        }

        void AddSkillAction(int skillActionID, float delay = 0)
        {
            var skillAction = CombatLogicObjectPool.CreateSkillAction(skillActionID);
            skillAction.SetOwner(_owner);
            skillAction.skillID = this.skillID;
            skillAction.ownerWorldMatrixOnSkillActive = this.ownerWorldMatrixOnActive;
            skillAction.delay = delay;
            if (mainTargetID != 0)
            {
                this.mainTargetID = mainTargetID;
            }
            _skillActionList.Insert(0, skillAction);
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public void SetSkillActionManager(SkillActionManager skillActionManager)
        {
            _skillActionManager = skillActionManager;
        }

        public SkillState Update()
        {
            if (_curState == SkillState.STANDBY)
            {
                Begin();
            }
            else if (_curState == SkillState.ACTIVE)
            {
                _pastTime = RealTime.time - _startTimeStamp;
                if (CheckEnd())
                {
                    End();
                }
                else
                {
                    Doing();
                }
                CheckCut();
            }
            return _curState;
        }

        public void Break()
        {
            if (!_isBreaked)
            {
                if (_curState != SkillState.ENDED)
                {
                    OnBreak();
                }
                _actHandler.BreakAllVisualFX();
            }
            _isBreaked = true;
        }

        void Begin()
        {
            _curState = SkillState.ACTIVE;
            _startTimeStamp = RealTime.time; 
            if (this._owner == PlayerAvatar.Player)
            {
                ChangeMoveMode();
                if (platform_helper.InPCPlatform())
                {
                    FaceAccordingMouse();
                }
                else
                {
                    FaceAccordingStick();
                } 
            }
            if (_owner.GetTransform() != null)
            {
                ownerWorldMatrixOnActive = _owner.GetTransform().localToWorldMatrix;
            }
            ResetSkillEventList();
            ResetSkillActionList();
            ResetSkillBufferList();
            SetCD();
            SetEP();
            FindSkillForSkillBegin();
            AttackRangePreview();
            RemoveConditionBuff();
            Doing();
        }

        void Doing()
        {
            DoingBuffers();
            DoingEvents();
            DoingActions();
            FacingTarget();
        }

        void End()
        {
            if (_curState == SkillState.ENDED) return;
            _curState = SkillState.ENDED;
            if (!_isBreaked)
            {
                OnBreak();
                CheckCut();
            }
            _actHandler.Clear();
            _owner = null;
            _skillActionManager = null;
            _skillData = null;
        }

        void CheckCut()
        {
            if (_checkCut) return;
            if (_owner == null || _owner.actor == null) return;
            if (_pastTime >= _duration && InMovingState())
            {
                _checkCut = true;
                _owner.actor.animationController.ReturnReady();
                if (!_isBreaked)
                {
                    OnBreak();
                }
            }
        }

        void OnBreak()
        {
            DelBuffsOnBreakOrEnd();
            _actHandler.ResetDefaultState(this._owner.actor);
            _skillBufferList.Clear();
            _skillEventList.Clear();
            ClearAllAction();
        }

        bool CheckEnd()
        {
            return _isBreaked || (_pastTime >= _duration && _skillActionList.Count == 0);
        }

        bool InMovingState()
        {
            if (_owner is PlayerAvatar)
            {
                return ControlStickState.strength > 0 || _owner.actor.actorState.MovingSpeedState != 0;
            }
            return _owner.actor.actorState.MovingSpeedState != 0;
        }

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            } 
        }

        void DoingActions()
        {
            for (int i = _skillActionList.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActionList[i];
                if (skillAction.delay <= _pastTime)
                {
                    if (skillAction.curState == SkillActionStage.STANDBY)
                    {
                        OnSkillActionActive(skillAction);
                        _skillActionList.RemoveAt(i);
                        _skillActionManager.AddSkillAction(skillAction);
                    }
                }
            }
        }

        void DoingBuffers()
        {
            for (int i = _skillBufferList.Count - 1; i >= 0; i--)
            {
                var bufferData = _skillBufferList[i];
                if (bufferData.delay <= _pastTime)
                {
                    _owner.bufferManager.AddBuffer(bufferData.bufferID, bufferData.duration, true, true);
                    _skillBufferList.RemoveAt(i);
                }
            }
        }

        void FacingTarget()
        {
            if (_alwaysFaceTargetEndTime > RealTime.time)
            {
                LockDirection();
            }
        }

        void RemoveConditionBuff()
        {
            if (this._skillData.showType > 0)
            {
                _owner.bufferManager.RemoveBuffer(this._skillData.showType);
            }
        }

        void FaceAccordingStick()
        {
            if (this.GetAccordingStick() == 0) return;
            if (ControlStickState.strength <= 0) return;
            Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
            _owner.actor.forward = moveDirection;
        }

        void FaceAccordingMouse()
        {
            var mousePos = ControlStickState.GetMoveDirectionByMouse();
            PlayerAvatar.Player.actor.FaceTo(mousePos);  
        }

        void ChangeMoveMode()
        {
            if (this._owner == PlayerAvatar.Player)
            {
                this._owner.moveManager.MoveBySkill(_duration);
            }
        }

        void FindSkillForSkillBegin()
        {
            FindSkillTarget();
            var faceLockMode = (FaceLockMode)this.GetFaceLockMode();
            if (faceLockMode == FaceLockMode.LockOnSkillActive
                || faceLockMode == FaceLockMode.LockDuringSkill)
            {
                _lockTargetID = this._mainTargetID;
                LockDirection();
            }
        }

        void OnSkillActionActive(SkillAction skillAction)
        {
            SearchTargetRepeat();
            SetTargetToSkillAction(skillAction);
        }

        bool SearchTargetRepeat()
        {
            var searchTargetRepeat = (SearchTargetRepeatType)this.GetSearchTargetRepeat();
            bool searchTarget = false;
            bool searchTargetUnique = false;
            if (searchTargetRepeat == SearchTargetRepeatType.ForEveryAction)
            {
                searchTarget = true;
            }
            else if (searchTargetRepeat == SearchTargetRepeatType.ForEveryActionUnique)
            {
                searchTarget = true;
                searchTargetUnique = true;
            }
            if (searchTarget)
            { 
                FindSkillTarget(searchTargetUnique);
                return true;
            }
            return false;
        }

        void SetTargetToSkillAction(SkillAction skillAction)
        {
            var faceLockMode = (FaceLockMode)this.GetFaceLockMode();
            if (faceLockMode == FaceLockMode.LockDuringSkill) {
                _lockTargetID = this._mainTargetID;
                LockDirection(); 
            }
            skillAction.mainTargetID = mainTargetID;
        }

        void ClearAllAction()
        {
            for (int i = _skillActionList.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActionList[i];
                skillAction.Clear();
                _skillActionList.RemoveAt(i);
                CombatLogicObjectPool.ReleaseSkillAction(skillAction);
            }   
        }

        void DelBuffsOnBreakOrEnd()
        {
            var delBuffsOnBreak = this.GetDelBuffsOnBreak();
            if (delBuffsOnBreak != null)
            {
                for (int i = 0; i < delBuffsOnBreak.Count; i++)
                {
                    _owner.bufferManager.RemoveBuffer(delBuffsOnBreak[i]);
                }
            }
        }

        void SetCD()
        {
            var cd = this.GetCD();
            _owner.skillManager.SetCommonCD(skillID, cd[1]);
            _owner.skillManager.SetCD(skillID, cd[0]);
        }

        void SetEP()
        {
            var usecosts = this.GetUseCosts();
            if (_owner is EntityAvatar && usecosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
            {
                Int16 cur_ep = (Int16)(_owner.cur_ep - usecosts[public_config.ITEM_SPECIAL_SPELL_ENERGY]);
                cur_ep = (Int16)Mathf.Min(_owner.max_ep, Mathf.Max(0, cur_ep));
                MogoWorld.SynEntityAttr(_owner, EntityPropertyDefine.cur_ep, cur_ep);
            }
        }

        void FindSkillTarget(bool isUnique = false)
        {
            var targetType = (TargetType)this.GetSearchTargetType();
            TargetRangeType targetRangeType = (TargetRangeType)this.GetSearchRegionType();
            List<int> targetRangeParam = this.GetSearchRegionArg();
            List<int> originAdjust = this.GetOriginAdjust();
            if (targetType == TargetType.MySelf)
            {
                mainTargetID = _owner.id;
                return;
            }
            var matrix = _owner.GetTransform().localToWorldMatrix;
            List<uint> entities = EntityFilter.GetEntitiesByRangeType(matrix, targetRangeType, targetRangeParam, originAdjust, EntityFilterType.SKILL_SUBJECT_FILTER);
            if (entities.Contains(this._owner.id)) entities.Remove(this._owner.id);
            if (isUnique)
            {
                mainTargetID = 0;
                for (int i = entities.Count - 1; i >=0; i--)
                {
                    if (_oldTargetIDList.Contains(entities[i]))
                    {
                        entities.RemoveAt(i);
                    }
                }
            }
            List<int> targetFilterOrders = this.GetTargetFilterOrders();
            Dictionary<int, List<int>> targetFilterArgs = this.GetTargetFilterArgs();
            entities = TargetFilter.GetCreaturesByTargetType(_owner, entities, targetType);
            entities = TargetFilter.FilterTarget(_owner, entities, targetFilterOrders, targetFilterArgs, 1, 1);
            if (entities.Count>0)
            {
                mainTargetID = entities[0];
                if (isUnique)
                {
                    _oldTargetIDList.Add(mainTargetID);
                }
            }
        }

        void LockDirection()
        {
            var entity = CombatSystemTools.GetCreatureByID(_lockTargetID);
            if (entity != null && _owner.actor != null)
            {
                _owner.actor.FaceTo(entity.actor);
            }
        }

        void AttackRangePreview()
        {
            int radius = GameData.XMLManager.spell[_skillData.skillID].__skill_preview_Radius;
            if (radius <= 0) return;

            string[] previewAttackGrgArr = GameData.XMLManager.global_params[GlobalParamsEnum.PREVIEW_ATTACK_RANGE_ARGS].__value.Split(',');
            if (previewAttackGrgArr.Length != 6)
            {
                LoggerHelper.Error("[SkillSubject:AttackRangePreview]=>预览攻击范围参数格式不对或者没有填写，请@方晓之！");
                return;
            }
            Color clor = new Color(float.Parse(previewAttackGrgArr[0]), float.Parse(previewAttackGrgArr[1]), float.Parse(previewAttackGrgArr[2]), float.Parse(previewAttackGrgArr[3]));
            float duration = float.Parse(previewAttackGrgArr[4]);
            if (duration <= 0) return;
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix();
            List<int> targetRangeParam = new List<int>() { radius };
            PreviewAttackRange.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeParam, new List<int>() { 0, 0, 0 }, duration, clor, float.Parse(previewAttackGrgArr[5]));
        }

        void TestAttackRangePreview()
        {
            int radius = 600;

            string[] previewAttackGrgArr = GameData.XMLManager.global_params[GlobalParamsEnum.PREVIEW_ATTACK_RANGE_ARGS].__value.Split(',');
            if (previewAttackGrgArr.Length != 6)
            {
                LoggerHelper.Error("[SkillSubject:AttackRangePreview]=>预览攻击范围参数格式不对或者没有填写，请@方晓之！");
                return;
            }
            //Color clor = new Color(float.Parse(previewAttackGrgArr[0]), float.Parse(previewAttackGrgArr[1]), float.Parse(previewAttackGrgArr[2]), float.Parse(previewAttackGrgArr[3]));
            Color clor = new Color(0, 1, 0, 0.01f);
            float duration = 1f;// _fireTime - RealTime.time;
            if (duration <= 0) return;
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix();
            List<int> targetRangeParam = new List<int>() { radius };// this.GetAttackRegionArg();
            PreviewAttackRange.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeParam, new List<int>() { 0, 0, 0 }, duration, clor, 0.5f);
        }

        Matrix4x4 GetRegionJudgeMatrix()
        {
            return ownerWorldMatrixOnActive;
        }
    }
}
