﻿using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public enum CharacterAction
    {
        ACTIVE_MOVE_ABLE,
        MOVE_ABLE,
        ATTACK_ABLE,
        HIT_ABLE,
        TURN_ABLE,
        VISIBLE,
        SHOW,
        THINK_ABLE,
        COLLIDE_ABLE,
        TRANSMIT_ABLE,
        AUTO_MOUNT_ABLE
    }

    //枚举类型对比类，避免使用枚举为字典Key时，通过Key获取Value时调用Equals和GetHashCode方法时的装箱操作
    public struct CharacterActionComparer : IEqualityComparer<CharacterAction>
    {
        public bool Equals(CharacterAction x, CharacterAction y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(CharacterAction obj)
        {
            return (int)obj;
        }
    }

    public class StateManager
    {
        private static UInt32 ONE = 1;

        private EntityCreature _owner;
        private Dictionary<int, List<Action>> _stateFuncMap;
        private Dictionary<CharacterAction, int> _characterActionStateMap;
        public Dictionary<CharacterAction, int> characterActionStateMap
        {
            get { return _characterActionStateMap; }
        }
        private UInt32 _currentState = 0;
        public UInt32 currentState
        {
            get { return _currentState; }
        }

        Dictionary<int, int> _stateAccumulationMap = new Dictionary<int, int>();

        public StateManager(EntityCreature ower)
        {
            _owner = ower;
            InitStateFuncs();
            InitCharacterActionState();
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.state_cell, this.OnStateChange);
        }

        public void Release()
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.state_cell, this.OnStateChange);
        }

        public void InitStateFuncs()
        {
            _stateFuncMap = new Dictionary<int, List<Action>>();
            //return;//暂时没有状态变换需要处理 AVATAR_STATE_DEATH
            _stateFuncMap.Add(state_config.AVATAR_STATE_DEATH, new List<Action>() { OnEnterDeath, OnLeaveDeath });
            _stateFuncMap.Add(state_config.AVATAR_STATE_TEAM_FOLLOW, new List<Action>() { OnEnterFollow, OnLeaveFollow });
            _stateFuncMap.Add(state_config.AVATAR_STATE_DEAD_GHOST, new List<Action>() { OnEnterGhost, OnLeaveGhost });
            _stateFuncMap.Add(state_config.AVATAR_STATE_DUEL_BET, new List<Action>() { OnEnterDuelBet, OnLeaveDuelBet });
            _stateFuncMap.Add(state_config.AVATAR_STATE_DUEL_FIGHT, new List<Action>() { OnEnterDuelFight, OnLeaveDuelFight });
            _stateFuncMap.Add(state_config.AVATAR_STATE_FLY, new List<Action>() { OnEnterFly, OnLeaveFly });

        }

        public void InitCharacterActionState()
        {
            _characterActionStateMap = new Dictionary<CharacterAction, int>(new CharacterActionComparer());
            _characterActionStateMap.Add(CharacterAction.ACTIVE_MOVE_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.MOVE_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.ATTACK_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.HIT_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.TURN_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.VISIBLE, 0);
            _characterActionStateMap.Add(CharacterAction.SHOW, 0);
            _characterActionStateMap.Add(CharacterAction.THINK_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.COLLIDE_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.TRANSMIT_ABLE, 0);
            _characterActionStateMap.Add(CharacterAction.AUTO_MOUNT_ABLE, 0);
        }

        public void OnStateChange()
        {
            if (MogoWorld.IsClientEntity(_owner)) return;
            UInt32 newState = _owner.state_cell;
            for (int pos = 0; pos < state_config.AVATAR_STATE_SPELL_MAX - 1; pos++)
            {
                UInt32 cursor = ONE << pos;
                bool newSubState = (newState & cursor) != 0;
                SetState(pos, newSubState, false);
            }
            _currentState = newState;
            RefreshCharacterActionState();
        }

        public void SetState(int pos, bool newSubState, bool refresh = true)
        {
            UInt32 cursor = ONE << pos;
            bool oldSubState = (_currentState & cursor) != 0;
            if (newSubState)
            {
                _currentState |= cursor;
            }
            else
            {
                _currentState &= ~cursor;
            }
            if (oldSubState != newSubState)
            {
                CallStateChangeHandler(pos, newSubState);
            }
            if (refresh) RefreshCharacterActionState();
        }

        public void AccumulateState(int pos, int value)
        {
            if (!_stateAccumulationMap.ContainsKey(pos))
            {
                _stateAccumulationMap[pos] = 0;
            }
            _stateAccumulationMap[pos] += value;
            if (_stateAccumulationMap[pos] < 0)//这种情况不应该发生的，做容错处理，以免影响后续状态
            {
                _stateAccumulationMap[pos] = 0;
            }
            if (_stateAccumulationMap[pos] > 0)
            {
                SetState(pos, true);
            }
            else
            {
                SetState(pos, false);
            }
        }

        public void RefreshCharacterActionState()
        {
            int __active_move_able = 0;
            int __move_able = 0;
            int __attack_able = 0;
            int __hit_able = 0;
            int __turn_able = 0;
            int __visible = 0;
            int __show = 0;
            int __think_able = 0;
            int __collide_able = 0;
            int __transmit_able = 0;
            int __auto_mount_able = 0;
            for (int pos = 0; pos < state_config.AVATAR_STATE_SPELL_MAX; pos++)
            {
                if (!XMLManager.spell_state.ContainsKey(pos))
                {
                    continue;
                }
                var data = XMLManager.spell_state[pos];
                UInt32 cursor = ONE << pos;
                int coefficient = (_currentState & cursor) != 0 ? 1 : 0;
                __active_move_able |= coefficient * data.__active_move_able;
                __move_able |= coefficient * data.__move_able;
                __attack_able |= coefficient * data.__attack_able;
                __hit_able |= coefficient * data.__hit_able;
                __turn_able |= coefficient * data.__turn_able;
                __visible |= coefficient * data.__visible;
                __show |= coefficient * data.__show;
                __think_able |= coefficient * data.__think_able;
                __collide_able |= coefficient * data.__collide_able;
                __transmit_able |= coefficient * data.__transmit_able;
                __auto_mount_able |= coefficient * data.__auto_mount_able;
            }
            _characterActionStateMap[CharacterAction.ACTIVE_MOVE_ABLE] = __active_move_able;
            _characterActionStateMap[CharacterAction.MOVE_ABLE] = __move_able;
            _characterActionStateMap[CharacterAction.ATTACK_ABLE] = __attack_able;
            _characterActionStateMap[CharacterAction.HIT_ABLE] = __hit_able;
            _characterActionStateMap[CharacterAction.TURN_ABLE] = __turn_able;
            _characterActionStateMap[CharacterAction.VISIBLE] = __visible;
            _characterActionStateMap[CharacterAction.SHOW] = __show;
            _characterActionStateMap[CharacterAction.THINK_ABLE] = __think_able;
            _characterActionStateMap[CharacterAction.COLLIDE_ABLE] = __collide_able;
            _characterActionStateMap[CharacterAction.TRANSMIT_ABLE] = __transmit_able;
            _characterActionStateMap[CharacterAction.AUTO_MOUNT_ABLE] = __auto_mount_able;
            OnCharacterActionStateChange();
        }

        public void CallStateChangeHandler(int pos, bool on)
        {
            if (_stateFuncMap.ContainsKey(pos))
            {
                if (on)
                {
                    _stateFuncMap[pos][0]();
                }
                else
                {
                    _stateFuncMap[pos][1]();
                }
            }
        }

        public bool InState(int state)
        {
            return (_currentState & (ONE << state)) > 0;
        }

        public bool CanDO(CharacterAction action)
        {
            return _characterActionStateMap[action] == 0;
        }

        #region 子状态处理

        void OnCharacterActionStateChange()
        {
            this._owner.collidable = CanDO(CharacterAction.COLLIDE_ABLE);
            this._owner.visible = CanDO(CharacterAction.VISIBLE);
            this._owner.show = CanDO(CharacterAction.SHOW);
        }

        #endregion

        #region 状态处理

        public void OnEnterDeath()
        {
            this._owner.moveManager.Stop();
            this._owner.OnDeath();
            this._owner.skillManager.ClearAllCD();
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_DEATH, _owner.id);
        }

        public void OnLeaveDeath()
        {
            if (this._owner.actor == null) return;
            this._owner.actor.animationController.Relive();
            this._owner.OnRelive();
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_RELIVE, _owner.id);
        }

        public void OnEnterFollow()
        {
            if (this._owner is PlayerAvatar)
            {
                PlayerAutoFightManager.GetInstance().OnEnterFollow();
            }
        }

        public void OnLeaveFollow()
        {
            if (this._owner is PlayerAvatar)
            {
                PlayerAutoFightManager.GetInstance().OnLeaveFollow();
            }
        }

        public void OnEnterGhost()
        {
            _owner.OnEnterGhost();
        }

        public void OnLeaveGhost()
        {
            _owner.OnLeaveGhost();
        }

        //决斗场可下注效果（头顶按钮）
        public void OnEnterDuelBet()
        {
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_DUEL_STAKE, _owner.id);
        }

        //决斗场可下注效果（头顶按钮）
        public void OnLeaveDuelBet()
        {
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_DUEL_STAKE, _owner.id);
        }

        //决斗场可下注效果（头顶刀）
        public void OnEnterDuelFight()
        {
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_DUEL_FIGHT, _owner.id);
        }

        //决斗场可下注效果（头顶刀）
        public void OnLeaveDuelFight()
        {
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_DUEL_FIGHT, _owner.id);
        }

        //飞行状态(飞行坐骑)
        public void OnEnterFly()
        {
            if (_owner is EntityAvatar)
            {
                (_owner as EntityAvatar).rideManager.Fly();
            }
        }

        //飞行状态(飞行坐骑)
        public void OnLeaveFly()
        {
            if (_owner is EntityAvatar)
            {
                (_owner as EntityAvatar).rideManager.Land();
            }
        }
        #endregion 状态处理


    }
}
