﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using Common.States;
using GameLoader.Utils;
using Common.ClientConfig;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.Utils;
using GameData;

namespace GameMain.CombatSystem
{
    public class MoveManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class MoveManager
    {
        private EntityCreature  _owner;
        private Action<EntityCreature> _moveEndCallBack = null;
        
        private List<float> _speedMlps = new List<float>();
        public List<float> speedMlps
        {
            get { return _speedMlps; }
        }

        private float _curMoveSpeed;
        private float _curClientMoveSpeed;
        public float curMoveSpeed
        {
            get
            {
                if (_owner is PlayerAvatar && GameSceneManager.GetInstance().IsInClientMap())
                {
                    return _curClientMoveSpeed;
                }
                return _curMoveSpeed; 
            }
        }

        private AccordingStateBase _curAccordingState;

        private Dictionary<AccordingMode, AccordingStateBase> _accordingModeActionMap = new Dictionary<AccordingMode, AccordingStateBase>(new AccordingModeComparer());

        public MoveManager(EntityCreature ower)
        {
            _owner = ower;
            MogoEngine.MogoWorld.RegisterUpdate<MoveManagerUpdateDelegate>("MoveManager.Update", Update);
            reCalculateMoveSpeed();
            EntityPropertyManager.Instance.AddEventListener(ower, EntityPropertyDefine.speed, reCalculateMoveSpeed);
            if (_owner.IsPlayer())
            {
                EntityPropertyManager.Instance.AddEventListener(ower, EntityPropertyDefine.base_speed, reCalculateMoveSpeed);
            }
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("MoveManager.Update", Update);
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.speed, reCalculateMoveSpeed);
            if (_owner.IsPlayer())
            {
                EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.base_speed, reCalculateMoveSpeed);
            }
        }

        public AccordingMode _accordingMode = AccordingMode.AccordingEntity;
        public AccordingMode accordingMode
        {
            set
            {
                _accordingMode = value;
                var newAccordingState = GetAccordingState(_accordingMode);
                if (_curAccordingState != newAccordingState)
                {
                    if (_curAccordingState != null) _curAccordingState.OnLeave();
                    _curAccordingState = newAccordingState;
                    newAccordingState.OnEnter();
                }
            }
            get { return _accordingMode; }
        }
        public AccordingMode defaultAccordingMode = AccordingMode.AccordingEntity;

        private AccordingStateBase CreateAccordingState(AccordingMode mode)
        {
            AccordingStateBase result = null;
            switch (mode)
            { 
                case AccordingMode.AccordingActor:
                    result = new AccordingActor(_owner);
                    break;
                case AccordingMode.AccordingEntity:
                    result = new AccordingEntity(_owner);
                    break;
                case AccordingMode.AccordingMovement:
                    result = new AccordingMovement(_owner);
                    break;
                case AccordingMode.AccordingPath:
                    result = new AccordingPath(_owner);
                    break;
                case AccordingMode.AccordingStick:
                    result = new AccordingStick(_owner);
                    break;
                case AccordingMode.AccordingStickInSkill:
                    result = new AccordingStickInSkill(_owner);
                    break;
                case AccordingMode.AccordingLine:
                    result = new AccordingLine(_owner);
                    break;
                case AccordingMode.AccordingDirection:
                    result = new AccordingDirection(_owner);
                    break;
                case AccordingMode.AccordingAnimationClip :
                    result = new AccordingAnimationClip(_owner);
                    break;
                case AccordingMode.AccordingElevator:
                    result = new AccordingElevator(_owner);
                    break;
                case AccordingMode.AccordingSteeringWheel:
                    result = new AccordingSteeringWheel(_owner);
                    break;
                case AccordingMode.AccordingTouch:
                    result = new AccordingTouch(_owner);
                    break;
                case AccordingMode.AccordingTouchInSkill:
                    result = new AccordingTouchInSkill(_owner);
                    break;
            }
            return result;
        }

        private AccordingStateBase GetAccordingState(AccordingMode mode)
        {
            if (!_accordingModeActionMap.ContainsKey(mode))
            {
                _accordingModeActionMap.Add(mode, CreateAccordingState(mode));
            }
            return _accordingModeActionMap[mode];
        }

        private void reCalculateMoveSpeed()
        {
            _curMoveSpeed = _owner.speed * 0.01f;

            _curClientMoveSpeed = _owner.base_speed * 0.01f;
            for (int i = 0; i < _speedMlps.Count; i++)
            {
                _curClientMoveSpeed *= _speedMlps[i];
            }
        }

        public void AddSpeedMlp(float value)
        {
            _speedMlps.Add(value);
            reCalculateMoveSpeed();
        }

        public void RemoveSpeedMlp(float value)
        {
            if (_speedMlps.Contains(value))
            {
                _speedMlps.Remove(value);
            }
            reCalculateMoveSpeed();
        }

        public void ClearSpeedMlp()
        {
            _speedMlps.Clear();
        }

        private void Update()
        {
            if (_owner.banBeControlled) return;
                                    

            if (_curAccordingState != null)
            {
                if (!_curAccordingState.Update() 
                    && _curAccordingState.modeType != defaultAccordingMode)
                {
                    Stop();
                }
            }
        }

        public void Stop()
        {
            if (_curAccordingState != null)
            {
                _curAccordingState.Stop();
                if (_moveEndCallBack != null && (accordingMode == AccordingMode.AccordingLine || accordingMode == AccordingMode.AccordingPath))
                {
                    _moveEndCallBack.Invoke(_owner);
                }
                _moveEndCallBack = null;
            }
            accordingMode = defaultAccordingMode;
        }

        public void Move(Vector3 targetPosition, float speed, Action<EntityCreature> moveEndCallBack = null)
        {
            if (!PathUtils.CanLine(_owner.position, targetPosition, _owner.actor.modifyLayer)) 
            {
                MoveByPath(targetPosition, speed, moveEndCallBack);
            }
            else
            {
                MoveByLine(targetPosition, speed, moveEndCallBack);
            }
        }

        public void MoveByLine(Vector3 targetPosition, float baseSpeed, Action<EntityCreature> moveEndCallBack = null)
        {
            var accordingLine = GetAccordingState(AccordingMode.AccordingLine) as AccordingLine;
            if (!accordingLine.Reset(targetPosition, baseSpeed)) return;
            accordingMode = AccordingMode.AccordingLine;
            _moveEndCallBack = moveEndCallBack;
        }

        public void MoveByMovement(Vector3 targetPosition, float baseSpeed, float accelerate = 0, bool needFix = true)
        {
            var accordingMovement = GetAccordingState(AccordingMode.AccordingMovement) as AccordingMovement;
            if (!accordingMovement.Reset(targetPosition, baseSpeed, accelerate, needFix)) return;
            accordingMode = AccordingMode.AccordingMovement;
            Update();
        }

        public void MoveByPath(Vector3 targetPosition, float speed, Action<EntityCreature> moveEndCallBack = null)
        {
            var accordingPath = GetAccordingState(AccordingMode.AccordingPath) as AccordingPath;
            reCalculateMoveSpeed();//临时这里处理一下
            if (!accordingPath.Reset(targetPosition, speed)) return;
            
            accordingMode = AccordingMode.AccordingPath;
            _moveEndCallBack = moveEndCallBack;

        }

        public void MoveBySkill(float duration)
        {
            AccordingSkill accordingSkill;
            if (!PlayerTouchBattleModeManager.GetInstance().CurrentCanTouch())
            {
                accordingSkill = GetAccordingState(AccordingMode.AccordingStickInSkill) as AccordingSkill;
            }
            else
            {
                accordingSkill = GetAccordingState(AccordingMode.AccordingTouchInSkill) as AccordingSkill;
            }
            if (!accordingSkill.Reset(duration)) return;
			accordingMode = accordingSkill.modeType;
        }

        public void MoveByDirection(uint targetID, float speed, float moveTime = 1, bool right = false)
        {
            var accordingDirection = GetAccordingState(AccordingMode.AccordingDirection) as AccordingDirection;
            if (!accordingDirection.Reset(targetID, speed, moveTime, right)) return;
            accordingMode = AccordingMode.AccordingDirection;
        }
    }
}
