﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.RPC;
using GameMain.Entities;
using Common.Data;
using MogoEngine;
using Common.ClientConfig;
using Common.ServerConfig;
using UnityEngine;
using GameMain.GlobalManager;
using GameLoader.Utils;
using GameData;
using MogoEngine.Events;
using Common.Events;
using ACTSystem;
using GameMain.GlobalManager.SubSystem;

namespace GameMain.CombatSystem
{
    public class BufferEffectHandler
    {
        private static BufferEffectHandler s_instance = null;
        public static BufferEffectHandler GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new BufferEffectHandler();
            }
            return s_instance;
        }
        private delegate bool EffectHandleFun(EntityCreature entity, string[] args);
        private Dictionary<string, EffectHandleFun> _clientEffectFunMap = new Dictionary<string, EffectHandleFun>();
        private Dictionary<string, EffectHandleFun> _serverEffectFunMap = new Dictionary<string, EffectHandleFun>();
        private BufferEffectHandler()
        {
            InitClientEffectFunMap();
            InitServerEffectFunMap();
        }

        void InitClientEffectFunMap()
        {
            _clientEffectFunMap.Add("add_hp", AddHP);
            _clientEffectFunMap.Add("add_hp_per", AddHPPer);
            _clientEffectFunMap.Add("add_attri", AddAttri);
            _clientEffectFunMap.Add("set_state", SetState);
            _clientEffectFunMap.Add("unset_state", UnsetState);
            _clientEffectFunMap.Add("cast_spell", CastSpell);
            _clientEffectFunMap.Add("add_adjust_spell", AddAdjustSpell);
            _clientEffectFunMap.Add("del_adjust_spell", DelAdjustSpell);
            _clientEffectFunMap.Add("add_spell_action", AddSpellAction);
            _clientEffectFunMap.Add("del_spell_action", DelSpellAction);
            _clientEffectFunMap.Add("play_action", PlayAction);
            _clientEffectFunMap.Add("stop_action", StopAction);
            _clientEffectFunMap.Add("add_speed_rate", AddSpeedRate);
            _clientEffectFunMap.Add("del_speed_rate", DelSpeedRate);
            _clientEffectFunMap.Add("shake", CameraShake);
            _clientEffectFunMap.Add("recover_part", RecoverPart);
            _clientEffectFunMap.Add("summon_vehicle", SummonVehicle);
            _clientEffectFunMap.Add("divorce_vehicle", DivorceVhicle);
            _clientEffectFunMap.Add("summon_airvehicle", SummonAirVehicle);
            _clientEffectFunMap.Add("divorce_airvehicle", DivorceAirVhicle);
            _clientEffectFunMap.Add("divorce_all_vehicle", DivorceAirVhicle);
            _clientEffectFunMap.Add("vehicle_path_anim", AddVehiclePathAnimation);
            _clientEffectFunMap.Add("add_shader", AddShader);
            _clientEffectFunMap.Add("del_shader", DelShader);
            _clientEffectFunMap.Add("add_buff", AddBuff);
            _clientEffectFunMap.Add("add_notice", AddNotice);
            _clientEffectFunMap.Add("del_notice", DelNotice);
            _clientEffectFunMap.Add("add_spell_ex_dmg", AddSpellExDmg);
            _clientEffectFunMap.Add("add_ep", AddEP);
            _clientEffectFunMap.Add("add_timescale", AddTimeScale);
            _clientEffectFunMap.Add("del_timescale", DelTimeScale);
			_clientEffectFunMap.Add("add_scale_rate", AddScaleRate);
			_clientEffectFunMap.Add("del_scale_rate", DelScaleRate);
            _clientEffectFunMap.Add("break_spells", BreakSpells);
        }

        void InitServerEffectFunMap()
        {
            _serverEffectFunMap.Add("set_state", SetState);
            _serverEffectFunMap.Add("unset_state", UnsetState);
            _serverEffectFunMap.Add("cast_spell", CastSpell);
            _serverEffectFunMap.Add("add_adjust_spell", AddAdjustSpell);
            _serverEffectFunMap.Add("del_adjust_spell", DelAdjustSpell);
            _serverEffectFunMap.Add("add_spell_action", AddSpellAction);
            _serverEffectFunMap.Add("del_spell_action", DelSpellAction);
            _serverEffectFunMap.Add("play_action", PlayAction);
            _serverEffectFunMap.Add("stop_action", StopAction);
            _serverEffectFunMap.Add("add_speed_rate", AddSpeedRate);
            _serverEffectFunMap.Add("del_speed_rate", DelSpeedRate);
            _serverEffectFunMap.Add("shake", CameraShake);
            _serverEffectFunMap.Add("summon_vehicle", SummonVehicle);
            _serverEffectFunMap.Add("divorce_vehicle", DivorceVhicle);
            _serverEffectFunMap.Add("summon_airvehicle", SummonAirVehicle);
            _serverEffectFunMap.Add("divorce_airvehicle", DivorceAirVhicle);
            _serverEffectFunMap.Add("divorce_all_vehicle", DivorceAirVhicle);
            _serverEffectFunMap.Add("vehicle_path_anim", AddVehiclePathAnimation);
            _serverEffectFunMap.Add("add_shader", AddShader);
            _serverEffectFunMap.Add("del_shader", DelShader);
            _serverEffectFunMap.Add("add_notice", AddNotice);
            _serverEffectFunMap.Add("del_notice", DelNotice);
            _serverEffectFunMap.Add("add_timescale", AddTimeScale);
            _serverEffectFunMap.Add("del_timescale", DelTimeScale);
            _serverEffectFunMap.Add("add_scale_rate", AddScaleRate);
            _serverEffectFunMap.Add("del_scale_rate", DelScaleRate);
            _serverEffectFunMap.Add("break_spells", BreakSpells);
            _serverEffectFunMap.Add("add_ep", ShowAddEp);
        }

        public void ExecEffects(EntityCreature entity, bool isClientBuff, List<BufferEffectData> effects)
        {           
            if (effects == null) return;
            for (int i = 0; i < effects.Count;i++ )
            {
                var effect = effects[i];
                ExecEffect(entity, isClientBuff, effect.effectName, effect.effectValue);
            }
        }

        public void ExecEffect(EntityCreature entity, bool isClientBuff, string effectName, string[] args)
        {
            if (isClientBuff)
            {
                ExecEffectClient(entity, effectName, args);
            }
            else
            {
                ExecEffectServer(entity, effectName, args);
            }
        }

        public bool ExecEffectClient(EntityCreature entity, string effectName, string[] args)
        {
            if (_clientEffectFunMap.ContainsKey(effectName))
            {
                return _clientEffectFunMap[effectName](entity, args);
            }
            return false;
        }

        public bool ExecEffectServer(EntityCreature entity, string effectName, string[] args)
        {
            if (_serverEffectFunMap.ContainsKey(effectName))
            {
                return _serverEffectFunMap[effectName](entity, args);
            }
            return false;
        }

        #region 修改属性
        private bool AddHP(EntityCreature entity, string[] args)
        {
            int hpChange = int.Parse(args[0]);
            int newHP = entity.cur_hp + hpChange;
            newHP = Mathf.Max(0, Mathf.Min(newHP, entity.max_hp));
            MogoWorld.SynEntityAttr(entity, EntityPropertyDefine.cur_hp, newHP);
            if (entity.cur_hp == 0)
            {
                entity.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
            }
            if (hpChange != 0)
            {
                entity.bufferManager.OnBuffHpChange(hpChange);
            }
            return true;
        }

        private bool AddHPPer(EntityCreature entity, string[] args)
        {
            int hpChange = (int)(float.Parse(args[0]) * entity.max_hp);
            int newHP = entity.cur_hp + hpChange;
            newHP = Mathf.Max(0, Mathf.Min(newHP, entity.max_hp));
            MogoWorld.SynEntityAttr(entity, EntityPropertyDefine.cur_hp, newHP);
            if (entity.cur_hp == 0)
            {
                entity.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
            }
            if (hpChange != 0)
            {
                entity.bufferManager.OnBuffHpChange(hpChange);
            }
            return true;
        }

        private bool AddEP(EntityCreature entity, string[] args)
        {
            ShowAddEp(entity, args);
            int newEP = entity.cur_ep + int.Parse(args[0]);
            newEP = Mathf.Max(0, Mathf.Min(newEP, entity.max_ep));
            MogoWorld.SynEntityAttr(entity, EntityPropertyDefine.cur_ep, newEP);
            return true;
        }

        private bool ShowAddEp(EntityCreature entity, string[] args)
        {
            int addEP = int.Parse(args[0]);
            if (entity == PlayerAvatar.Player)
            {
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.ADD_EP, entity.cur_ep, entity.cur_ep + addEP);
            }
            return true;
        }

        private bool AddAttri(EntityCreature entity, string[] args)
        {
            var attriID = int.Parse(args[0]);
            var attriAddValue = float.Parse(args[1]);
			var oldValue = entity.GetAttribute((fight_attri_config)attriID);
            if (attri_config_helper.IsFloat(attriID))
            {
                attriAddValue *= 10000;
				oldValue *= 10000;
            }
			int newValue = (int)(oldValue + attriAddValue);
            entity.OnSyncOneBa((byte)attriID, newValue);
            return true;
        }

        private bool AddSpeedRate(EntityCreature entity, string[] args)
        {
            entity.moveManager.AddSpeedMlp(float.Parse(args[0]));
            return true;
        }

        private bool DelSpeedRate(EntityCreature entity, string[] args)
        {
            entity.moveManager.RemoveSpeedMlp(float.Parse(args[0]));
            return true;
        }
        #endregion

        #region 设置状态
        private bool SetState(EntityCreature entity, string[] args)
        {
            entity.stateManager.AccumulateState(int.Parse(args[0]), 1);
            return true;
        }

        private bool UnsetState(EntityCreature entity, string[] args)
        {
            entity.stateManager.AccumulateState(int.Parse(args[0]), -1);
            return true;
        }
        #endregion

        #region 技能相关
        private bool CastSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.PlaySkill(int.Parse(args[0]));
            return true;
        }

        private bool AddAdjustSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.AddAdjustSkillID(int.Parse(args[0]));
            return true;
        }

        private bool DelAdjustSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.RemoveAdjustSkillID(int.Parse(args[0]));
            return true;
        }

        private bool AddSpellAction(EntityCreature entity, string[] args)
        {
            entity.skillManager.AddAdjustSkillActionID(int.Parse(args[0]));
            return true;
        }

        private bool DelSpellAction(EntityCreature entity, string[] args)
        {
            entity.skillManager.RemoveAdjustSkillActionID(int.Parse(args[0]));
            return true;
        }

        private bool AddSpellExDmg(EntityCreature entity, string[] args)
        {
            int count = args.Length / 3;
            int index = 0;
            for (int i = 0; i < count; ++i)
            {
                entity.AddSpellActionExDmg(int.Parse(args[index++]), int.Parse(args[index++]), float.Parse(args[index++]));
            }
            return true;
        }

        private bool BreakSpells(EntityCreature entity, string[] args)
        {
            entity.skillManager.BreakCurSkill();
            if (entity is EntityAvatar)
            {
                (entity as EntityAvatar).rideManager.Dismount();
            }
            return true;
        }
        #endregion

        #region 播放动作
        private bool PlayAction(EntityCreature entity, string[] args)
        {
            if (entity.actor != null)
            {
                entity.actor.animationController.PlayAction(int.Parse(args[0]));
            }
            return true;
        }

        private bool StopAction(EntityCreature entity, string[] args)
        {
            string stateName = args[0];
            if (entity.actor != null && entity.actor.animationController.IsInState(stateName))
            {
                entity.actor.animationController.ReturnReady();
            }
            return true;
        }
        #endregion

        #region 部位恢复
        private bool RecoverPart(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is EntityMonsterBase))
            {
                return false;
            }
            EntityMonsterBase entityMonsterBase = entity as EntityMonsterBase;
            entityMonsterBase.partManager.RecoverPart(int.Parse(args[0]));
            return true;
        }
        #endregion

        #region 载具相关
        #region 召唤载具
        private bool SummonVehicle(EntityCreature owner, string[] args)
        {
            if (owner.vehicleManager == null)
            {
                LoggerHelper.Info(string.Format("You can't summon vehicle because owner's vehicleManager is null, owner's type is {0}, id is {1}", owner.entityType, owner.id));
                return false;
            }
            MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_GROUND_VEHICLE, (entity) =>
            {
                var vehicle = entity as GameMain.Entities.EntityGroundVehicle;
                vehicle.monster_id = uint.Parse(args[0]);
                vehicle.SetPosition(owner.position);
                vehicle.SetRotation(owner.rotation);
                vehicle.driver = owner;
                //临时设计数据
                vehicle.speed = 400;
                vehicle.camp_pve_type = 1;
                vehicle.camp_pvp_type = 1;
                vehicle.rotateSpeed = float.Parse(args[2]);
                bool showButton = int.Parse(args[3]) != 0 ? true : false;
                owner.vehicleManager.vehicleType = VehicleType.VEHICLE;
                owner.vehicleManager.Summon(vehicle, int.Parse(args[1]), showButton);
            });

            return true;
        }

        private bool SummonAirVehicle(EntityCreature owner, string[] args)
        {
            if (owner.vehicleManager == null)
            {
                LoggerHelper.Info(string.Format("You can't summon air vehicle because owner's vehicleManager is null, owner's type is {0}, id is {1}", owner.entityType, owner.id));
                return false;
            }
            MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_AIRVEHICLE, (entity) =>
            {
                var vehicle = entity as GameMain.Entities.EntityAirVehicle;
                vehicle.monster_id = uint.Parse(args[0]);
                //记录载具Buffer
                if (int.TryParse(args[3], out owner.vehicleBufferID) == false)
                {
                    owner.vehicleBufferID = 0;
                }
                
                vehicle.SetPosition(owner.position);
                vehicle.SetRotation(Quaternion.Euler(0, 0, 0));
                vehicle.driver = owner;
                vehicle.visualFXPriority = owner.visualFXPriority;
                //临时设计数据
                vehicle.speed = 400;
                vehicle.camp_pve_type = 1;
                vehicle.camp_pvp_type = 1;

                bool showButton = false;
                if (args.Length != 4)
                {
                    LoggerHelper.Error("加空中载具参数填写错误请@小之确认！args.Length:    " + args.Length);
                }
                else
                {
                    showButton = int.Parse(args[2]) != 0 ? true : false;
                }
                owner.vehicleManager.vehicleType = VehicleType.AIRVEHICLE;
                owner.vehicleManager.Summon(vehicle, int.Parse(args[1]), showButton);
            });

            return true;
        }
        #endregion

        #region 脱离载具
        private bool DivorceVhicle(EntityCreature owner, string[] args)
        {
            if (owner.vehicleManager == null)
            {
                LoggerHelper.Info(string.Format("You can't divorce vehicle because owner's vehicleManager is null, owner's type is {0}, id is {1}", owner.entityType, owner.id));
                return false;
            }
            owner.vehicleManager.Divorce();
            return true;
        }

        private bool DivorceAirVhicle(EntityCreature owner, string[] args)
        {
            
            if (owner.vehicleManager == null)
            {
                LoggerHelper.Info(string.Format("You can't divorce air vehicle because owner's vehicleManager is null, owner's type is {0}, id is {1}", owner.entityType, owner.id));
                return false;
            }
            owner.vehicleManager.Divorce();
            return true;
        }

        #endregion

        #region 增加载具路点动画
        private bool AddVehiclePathAnimation(EntityCreature owner, string[] args)
        {
            if (owner.vehicleManager != null)
            {
                owner.vehicleManager.vehiclePath = args[0];
            }
            return true;
        }

        #endregion 
        #endregion

        #region 怪物shader
        private bool AddShader(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is EntityMonsterBase))
            {
                return false;
            }
            EntityMonsterBase entityMonsterBase = entity as EntityMonsterBase;
            entityMonsterBase.SetModelShader(int.Parse(args[0]));
            return true;
        }

        private bool DelShader(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is EntityMonsterBase))
            {
                return false;
            }
            EntityMonsterBase entityMonsterBase = entity as EntityMonsterBase;
            entityMonsterBase.ResetModelShader();
            return true;
        }
        #endregion

        #region buff相关
        private bool AddBuff(EntityCreature entity, string[] args)
        {
            int buffId = int.Parse(args[0]);
            float duration = float.Parse(args[1]);
            entity.bufferManager.AddBuffer(buffId, duration);
            return true;
        }
        #endregion

        #region 相机震屏
        private bool CameraShake(EntityCreature entity, string[] args)
        {
            var type = int.Parse(args[0]);
            var shakeID = int.Parse(args[1]);
            var duration = float.Parse(args[2]) * 0.001f; ;
            if (type == 0)//自己
            {
                if (entity == PlayerAvatar.Player)
                {
                    CameraManager.GetInstance().ShakeCamera(shakeID, duration);
                }
            }
            else if (type == 1)//所有人
            {
                CameraManager.GetInstance().ShakeCamera(shakeID, duration);
            }
            return true;
        }
        #endregion

        #region 系统提示
        private bool AddNotice(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is PlayerAvatar))
            {
                return false;
            }
            SystemNoticeDisplayData notice = new SystemNoticeDisplayData();
            notice.type = int.Parse(args[0]);
            notice.content = int.Parse(args[1]);
            notice.time = uint.Parse(args[2]);
            notice.priority = int.Parse(args[3]);
            SpaceNoticeActionManager.Instance.AddSystemNotice(notice);
            return true;
        }

        private bool DelNotice(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is PlayerAvatar))
            {
                return false;
            }
            int type = int.Parse(args[0]);
            int contentId = int.Parse(args[1]);
            EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.HIDE_SYSTEM_NOTICE_DISPLAY, type, contentId);
            return true;
        }
        #endregion

        #region 慢放效果
        private bool AddTimeScale(EntityCreature entity, string[] args)
        {
            float scale = float.Parse(args[0]);
            int duration = int.Parse(args[1]);
            scale = (scale < 0) ? 0 : scale;
            ACTTimeScaleManager.GetInstance().KeepTimeScale(scale, duration * 0.001f);
            return true;
        }

        private bool DelTimeScale(EntityCreature entity, string[] args)
        {
            ACTTimeScaleManager.GetInstance().CancelTimeScale();
            return true;
        }
        #endregion

        #region 缩放实体
        private bool AddScaleRate(EntityCreature entity, string[] args)
        {
            entity.AddScaleMlp(float.Parse(args[0]));
            return true;
        }

        private bool DelScaleRate(EntityCreature entity, string[] args)
        {
            entity.RemoveScaleMlp(float.Parse(args[0]));
            return true;
        }
        #endregion
    }
}
