﻿using Common.Events;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class AirVehicleInputUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class AirVehicleInput : VehicleInputBase,IVehicleInput
    {

        public AirVehicleInput()
        {
            
        }

        public void AddEventListener(EntityCreature vehicle)
        {
            this.vehicle = vehicle;
            MogoEngine.MogoWorld.RegisterUpdate<AirVehicleInputUpdateDelegate>("AirVehicleInput.Update", Update);
            SetSkillIcon();
        }

        public void RemoveEventListener()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("AirVehicleInput.Update", Update);
            this.vehicle = null;
        }

        public override void SetSkillIcon()
        {
            this.vehicle.SetSkillIcon();
        }

        public void Update()
        {
            //
            if(Input.GetMouseButtonUp(0))
            {


                CastSkill(GetSkillIDByIdx(0));               //26001
            }
            //
            if (Input.GetKeyUp(KeyCode.H) && Input.GetKey(KeyCode.LeftControl))
            {
                //Debug.LogError("[EntityAirVehicle:PauseFlyAnimation]=>1_____________airVehiclePath: ");
                (vehicle as EntityAirVehicle).PauseFlyAnimation();
            }
            if (Input.GetKeyUp(KeyCode.J) && Input.GetKey(KeyCode.LeftControl))
            {
                (vehicle as EntityAirVehicle).ResumeFlyAnimation();
            }
            //
            
        }

        private void CastSkill(int skillID)
        {
            if (CheckCanPlaySkill() == false)
            {
                //技能使用失败
                EventDispatcher.TriggerEvent(VehicleEvents.VEHICLE_ATTACK_FAIL);

                //
                return;
            }


            OnPlaySkill(skillID);
        }

        private bool CheckCanPlaySkill()
        {
            if ((vehicle as EntityAirVehicle).airVehiclePath.riderSlot == null) return false;
            return (vehicle as EntityAirVehicle).GenRayFromCamera();
        }

        public void Release()
        {
            RemoveEventListener();
        }

        

    }
}
