﻿using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;


public class BHAirVehiclePath : MonoBehaviour
{

    void Update()
    {
        UpdateDummyVisible();


    }

    private void UpdateDummyVisible()
    {
        EntityDummy dummy = null;
        if (MogoWorld.Entities == null) return;
        if (MogoWorld.Entities.Count <= 0) return;
        
        foreach (var entity in MogoWorld.Entities)
        {
            if (entity.Value is EntityDummy)
            {
                dummy = entity.Value as EntityDummy;
                if(dummy.actor == null)
                {
                    continue;
                }
                string dummyLayer = dummy.GetActorLayer();
                if(string.IsNullOrEmpty(dummyLayer) == false)
                {
                    if (Common.ClientConfig.PhysicsLayerDefine.Dummy == dummyLayer)
                    {
                        //dummy.actor.visible = enable;
                        dummy.actor.gameObject.SetActive(InCameraViewport(dummy));
                    }
                }
                //
            }
        }
    }

    private bool InCameraViewport(EntityDummy dummy)
    {

        Vector3 viewportPos = CameraManager.GetInstance().Camera.WorldToViewportPoint(dummy.actor.gameObject.transform.position);
        if (viewportPos.x >= 0 && viewportPos.x <= 1
            && viewportPos.y >= 0 && viewportPos.y <= 1)
        {
            //LoggerHelper.Error("[InCameraViewport]=>viewportPos.x:    " + viewportPos.x + ",_viewportPos.y:  " + viewportPos.y + ",name:   " + dummy.actor.gameObject.transform.position);
            return true;
        }
        return false;
    }

}
