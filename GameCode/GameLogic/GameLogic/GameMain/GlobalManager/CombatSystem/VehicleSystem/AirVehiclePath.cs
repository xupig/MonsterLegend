﻿using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendTools;


namespace GameMain.CombatSystem
{
    public class AirVehiclePathUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class AirVehiclePath
    {
        private static string VEHICLE_ASSET_PATH = "Fx/AirVehicle/";
        private EntityCreature _theAirVehicle = null;
        private AnimationEvent _bhAirVehiclePath = null;
        private AudioListener _audioListener = null;

        private GameObject _pathGo = null;
        public GameObject pathGo
        {
            get { return _pathGo; }
        }
        private Animation _animation = null;
        private Animator _animator = null;

        private Transform _riderSlot;
        public Transform riderSlot
        {
            get
            {
                return _riderSlot;
            }
        }
        private Action _animationOverCallBack = null;

        public string vehiclePath
        {
            get
            {
                return VEHICLE_ASSET_PATH + this._theAirVehicle.driver.vehicleManager.vehiclePath + ".prefab";
            }

        }

        public AirVehiclePath(EntityCreature _airVehicle)
        {
            this._theAirVehicle = _airVehicle;
        }

        private void CheckAnimationOver()
        {
            if (AnimationStillPlaying() == false)
            {
                if (_animationOverCallBack != null)
                {
                    _animationOverCallBack();
                    _animationOverCallBack = null;
                }
                MogoWorld.UnregisterUpdate("AirVehiclePath.CheckAnimationOver", CheckAnimationOver);
            }
        }

        public void GetPathInfo(EntityAirVehicle vehicle)
        {
            if (_pathGo == null)
            {
                CreatePathInfo(vehicle, OnCreatePathCallback);
            }
            else
            {
                SetActive(true);
            }
        }

        public void SetActive(bool isActive)
        {
            if (_pathGo == null) return;
            if (_pathGo.gameObject == null) return;
            _pathGo.gameObject.SetActive(isActive);
            //LoggerHelper.Error("[SetActive]=>1__________pathGo: " + _pathGo + ",gameObject: " + _pathGo.gameObject.name + ",isActive:   " + isActive);
        }

        private void OnCreatePathCallback()
        {
            SetActive(true);
        }

        private void CreatePathInfo(EntityAirVehicle vehicle, Action callBack = null)
        {
            //以后改成读表
            CameraManager.GetInstance().ChangeToCameraAnimation(vehiclePath, (obj) =>
            {
                OnChangeToCameraAnimCallback(vehicle, obj, callBack);
            });
        }

        private void OnChangeToCameraAnimCallback(EntityAirVehicle vehicle, GameObject obj, Action callBack = null)
        {
            _pathGo = obj;
            _animation = obj.GetComponent<Animation>();
            _animator = obj.GetComponent<Animator>();
            _bhAirVehiclePath = obj.GetComponent<AnimationEvent>();
            if (vehicle.driver is PlayerAvatar)
            {
                obj.AddComponent<BHAirVehiclePath>();
            }
            
            if (_bhAirVehiclePath == null)
            {
                LoggerHelper.Error(string.Format("添加动画脚本{0}错误，飞行载具动画应该挂AnimationEvent脚本", vehiclePath));
                _bhAirVehiclePath = obj.AddComponent<AnimationEvent>();
            }
            _bhAirVehiclePath.onEvent = vehicle.OnAnimationEvent;
            _bhAirVehiclePath.onRiderEvent = vehicle.OnRiderEvent;

            _riderSlot = obj.transform.FindChildByName("rider_slot/slot");
            //挂接载具
            vehicle.actor.gameObject.transform.SetParent(_riderSlot, false);
            vehicle.actor.gameObject.transform.ResetPositionAngles();

            vehicle.driver.UpdateEntityComponents();
            //动画做的空中载具会消失一段时间,那么此时玩家身上的AudioLisner会无效,为了保证场景中有一个有效的lisner有效，所以挂到载具路径上
            //vehicle.rider.SetAudioListener(false);
            //if(vehicle.actor.gameObject.GetComponent<AudioListener>() == null)
            //{
            //    vehicle.actor.gameObject.AddComponent<AudioListener>();
            //}

            //vehicle.rider.SetBMGSlot(_pathGo.transform);
            //_audioListener = _pathGo.AddComponent<AudioListener>();

            if (callBack != null) { callBack(); }
        }

        public void SetAudioListener(bool setActive)
        {
            if (_audioListener == null) return;
            _audioListener.enabled = setActive;
        }

        public void DestroyPathModel()
        {
            SetActive(false);   //暂时
            //LoggerHelper.Error("[DestroyPathModel]=>1_______________________SetActive(false);");
        }

        public void PlayAnimation(Action animOverCallBack = null)
        {
            this._animationOverCallBack = animOverCallBack;
            if (_animation == null) return;
            _animation.Play();
            MogoWorld.RegisterUpdate<AirVehiclePathUpdateDelegate>("AirVehiclePath.CheckAnimationOver", CheckAnimationOver);

        }

        public void PauseAnimation()
        {
            if (_animator == null) return;
            _animator.speed = 0;

        }

        public void ResumeAnimation()
        {
            if (_animator == null) return;
            _animator.speed = 1f;
        }

        private bool AnimationStillPlaying()
        {
            if (_animation == null) return false;
            return _animation.IsPlaying(_animation.clip.name) && _animation[_animation.clip.name].normalizedTime < 1.0f;
        }

        public Vector3 GenRayFromCamera()
        {
            return Common.Utils.MogoRayUtils.MouseRaycastFromCamera(1000, (int)Mogo.Util.LayerMask.Terrain).point;
        }

    }
}
