﻿#region 模块信息
/*==========================================
// 模块名：IVehicleInput
// 命名空间: GameMain.CombatSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/11
// 描述说明：载具输入接口
// 其他：
//==========================================*/
#endregion

using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMain.CombatSystem
{
    public interface IVehicleInput
    {
        void AddEventListener(EntityCreature vehicle);
        void RemoveEventListener();
        void Update();

        void Release();
        
    }
}
