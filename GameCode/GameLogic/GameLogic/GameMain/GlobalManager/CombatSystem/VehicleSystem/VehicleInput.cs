﻿#region 模块信息
/*==========================================
// 模块名：VehicleInput
// 命名空间: GameMain.CombatSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/11
// 描述说明：处理玩家输入对载具控制
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMain.CombatSystem
{
    public class VehicleInputUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class VehicleInput : VehicleInputBase,IVehicleInput
    {
        
        public VehicleInput()
        {
            
            
        }

        public void AddEventListener(EntityCreature vehicle)
        {
            this.vehicle = vehicle;
            MogoEngine.MogoWorld.RegisterUpdate<VehicleInputUpdateDelegate>("VehicleInput.Update", Update);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL1_ON_CLICK, OnPlaySkill1);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL2_ON_CLICK, OnPlaySkill2);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL3_ON_CLICK, OnPlaySkill3);
            EventDispatcher.AddEventListener(MainUIEvents.MAINUI_T_BTN_SKILL4_ON_CLICK, OnPlaySkill4);
            SetSkillIcon();
        }

        public void RemoveEventListener()
        {

            MogoEngine.MogoWorld.UnregisterUpdate("VehicleInput.Update", Update);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL1_ON_CLICK, OnPlaySkill1);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL2_ON_CLICK, OnPlaySkill2);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL3_ON_CLICK, OnPlaySkill3);
            EventDispatcher.RemoveEventListener(MainUIEvents.MAINUI_T_BTN_SKILL4_ON_CLICK, OnPlaySkill4);
            this.vehicle = null;

        }

        public override void SetSkillIcon()
        {
            this.vehicle.SetSkillIcon();
        }

        public void Release()
        {
            RemoveEventListener();
        }

        public void Update()
        {

        }
    }
}
