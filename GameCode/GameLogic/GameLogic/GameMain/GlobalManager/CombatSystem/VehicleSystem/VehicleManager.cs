﻿#region 模块信息
/*==========================================
// 模块名：VehicleManager
// 命名空间: GameMain.CombatSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/10
// 描述说明：载具管理器
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public enum VehicleType
    {
        /// <summary>
        /// 地面载具
        /// </summary>
        VEHICLE = 0,

        /// <summary>
        /// 空中载具
        /// </summary>
        AIRVEHICLE

    }

    public enum VehicleStatus
    {
        NONE = 0,

        RIDING

    }

    public class ControlVehicle
    {
       private uint _ownerId;
       private uint _vehicleId;
       private int _buffId;
       public uint ownerId { get { return _ownerId; } }
       public uint vehicleId { get { return _vehicleId; } }
       public int buffId { get { return _buffId; } }

       public ControlVehicle(uint ownerId, uint vehicleId, int buffId)
       {
           this._ownerId = ownerId;
           this._vehicleId = vehicleId;
           this._buffId = buffId;
       }
    }

    /// <summary>
    /// 载具管理器
    /// </summary>
    public class VehicleManager
    {
        private EntityCreature _owner;

        private EntityCreature _vehicle;
        public EntityCreature vehicle
        {
            get
            {
                return _vehicle;
            }
            set
            {
                _vehicle = value;
            }
        }

        private IVehicleInput _vehicleInput = null;
        public IVehicleInput vehicleInput
        {
            get
            {
                return _vehicleInput;
            }
        }

        private VehicleStatus _status;
        public VehicleStatus status
        {
            get
            {
                return _status;
            }
            set 
            {
                _status = value;
                if(value == VehicleStatus.NONE)
                {
                    _buffID = 0;
                    _vehicle = null;
                    _vehicleInput = null;
                }

            }
        }

        public string vehiclePath;

        private int _buffID = 0;

        private VehicleType _vehicleType;
        public VehicleType vehicleType
        {
            get
            {
                return _vehicleType;
            }
            set
            {
                _vehicleType = value;
            }
        }

        private uint _vehicleID = 0;
        public bool isDrivering
        {
            get { return _vehicleID > 0; }
        }

        private bool _showDriveringButton = false;
        public bool showDriveringButton
        {
            get { return isDrivering && _showDriveringButton; }
        }

        private Dictionary<VehicleType, IVehicleInput> _VehicleDic = new Dictionary<VehicleType, IVehicleInput>();
        private Dictionary<VehicleType, Action> _CreateVhicleDic = new Dictionary<VehicleType, Action>();                                                                                            

        public VehicleManager(EntityCreature owner)
        {
            this._owner = owner;
            _CreateVhicleDic.Add(VehicleType.VEHICLE, CreateVehicle);
            _CreateVhicleDic.Add(VehicleType.AIRVEHICLE, CreateAirVehicle);
            status = VehicleStatus.NONE;
            AddEventListener();
        }

        public void OnLeaveSpace()
        {
            if (vehicle == null) return;
            uint vehicleId = vehicle.id;
            int buffId = _buffID;
            _owner.DivorceVehicle(vehicle);
            _vehicleInput.Release();
            status = VehicleStatus.NONE;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(VehicleEvents.ONREQUEST_DIVORCE_VEHICLE, RequestDivorce);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(VehicleEvents.ONREQUEST_DIVORCE_VEHICLE, RequestDivorce);
        }

        /// <summary>
        /// 请求召唤载具
        /// </summary>
        public void RequestSummon()
        {
            if(status != VehicleStatus.NONE) return;
            //临时设定一个BuffID
            _buffID = 6001;
            //给玩家上一个Buff,通过buffer召唤载具
            _owner.bufferManager.AddBuffer(_buffID, -1f, true);

        }

        public void RequestDivorce()
        {
            //Debug.LogError("[VehicleManager:RequestDivorce]=>1______________status: " + status + ",_buffID: " + _buffID);
            if (status == VehicleStatus.NONE) return;
            //移除原来的Buffer
            PlayerAvatar.Player.bufferManager.RemoveBuffer(_buffID);
            //_buffID = 6002;
            //给玩家上一个Buff,通过buffer召唤载具
            //_owner.bufferManager.AddBuffer(_buffID, 1000, true);
        }


        /// <summary>
        /// 召唤载具
        /// </summary>
        /// <param name="vehicle"></param>
        public void Summon(EntityCreature vehicle, int buffID, bool showButton)
        {
            _buffID = buffID;
            this._vehicle = vehicle;
            status = VehicleStatus.RIDING;
            _vehicleID = vehicle.id;
            _showDriveringButton = showButton;
            _owner.actor.actorController.SetControllerEnabled(false);
            EventDispatcher.TriggerEvent<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, new ControlVehicle(_owner.id, vehicle.id, _buffID));
        }

        public void Divorce()
        {
            //Debug.LogError("[VehicleManager:Divorce]=>1______________status: " + status + ",_buffID: " + _buffID);
            _vehicleID = 0;
            _showDriveringButton = false;
            _owner.actor.actorController.SetControllerEnabled(true);
            if (vehicle == null) return;
            uint vehicleId = vehicle.id;
            int buffId = _buffID;
            _owner.DivorceVehicle(vehicle);
            (vehicle as EntityVehicle).DestroyEntityModel();
            //vehicle.OnDeath();
            //vehicle.stateManager.SetState(Common.ServerConfig.state_config.AVATAR_STATE_DEATH, true);
            _vehicleInput.Release();
            status = VehicleStatus.NONE;

            EventDispatcher.TriggerEvent<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, new ControlVehicle(_owner.id, vehicleId, buffId));
        }

        public void AddVehicleEventListener(EntityCreature vehicle,VehicleType vehicleType = VehicleType.VEHICLE)
        {
            if (!_VehicleDic.ContainsKey(vehicleType))
            {
                _CreateVhicleDic[vehicleType]();
                _VehicleDic.Add(vehicleType, _vehicleInput);
            }
            else
            {
                _vehicleInput = _VehicleDic[vehicleType];
            }
            this.vehicle = vehicle;
            this.vehicle.driver = _owner;
            vehicleInput.AddEventListener(vehicle);
        }

        private void CreateVehicle()
        {
            _vehicleInput = new VehicleInput();
        }

        private void CreateAirVehicle()
        {
            _vehicleInput = new AirVehicleInput();
        }

        public void Release()
        {
            RemoveEventListener();
        }
    }
}
