﻿#region 模块信息
/*==========================================
// 模块名：BHAirVehicleAnimatorEvent
// 命名空间: SpaceSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/11
// 描述说明：处理玩家输入对载具控制
// 其他：
//==========================================*/
#endregion

using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceSystem
{
    public class BHAirVehicleAnimatorEvent : MonoBehaviour
    {
        private void fly(int actionID)
        {
            //Debug.Log(string.Format("<color=#ffff00>now actionID:   {0}</color>",actionID));
            Animator animator = gameObject.GetComponent<Animator>();
            if (animator == null)
            {
                LoggerHelper.Error("[BHAirVehicleAnimatorEvent:fly]=>animator不存在,请检查是否缺少次组件!");
                return;
            }
            animator.SetInteger("Action", actionID);
        }

        

    }
}
