﻿#region 模块信息
/*==========================================
// 模块名：VehicleInputBase
// 命名空间: GameMain.CombatSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/11
// 描述说明：处理玩家输入对载具控制
// 其他：
//==========================================*/
#endregion

using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class VehicleInputBase
    {
        private EntityCreature _vehicle;
        public EntityCreature vehicle
        {
            get
            {
                return _vehicle;
            }
            set
            {
                _vehicle = value;
            }
        }

        public void OnPlaySkill(int skillID)
        {
            vehicle.skillManager.PlaySkill(skillID);
        }
                                                                  
        public virtual void OnPlaySkill1()
        {
            int skillID = GetSkillIDByIdx(1);
            //Debug.LogError("[VehicleInput:OnPlaySkill1]=>1____________________skillID:  " + skillID);
            if (skillID <= 0) return;
            //Debug.LogError("[VehicleInput:OnPlaySkill1]=>2____________________skillID:  " + skillID);
            vehicle.skillManager.PlaySkill(skillID);

        }

        public virtual void OnPlaySkill2()
        {
            int skillID = GetSkillIDByIdx(2);
            if (skillID <= 0) return;

            if(skillID != 0) vehicle.skillManager.PlaySkill(skillID);
        }

        public virtual void OnPlaySkill3()
        {
            int skillID = GetSkillIDByIdx(3);
            if (skillID <= 0) return;
            vehicle.skillManager.PlaySkill(skillID);
        }

        public virtual void OnPlaySkill4()
        {
            int skillID = GetSkillIDByIdx(4);
            if (skillID <= 0) return;

            vehicle.skillManager.PlaySkill(skillID);
        }

        public int GetSkillIDByIdx(int skillIdx)
        {
            return _vehicle.GetSkillIDByIdx(skillIdx);
        }

        public virtual void SetSkillIcon()
        {

        }

    }
}
