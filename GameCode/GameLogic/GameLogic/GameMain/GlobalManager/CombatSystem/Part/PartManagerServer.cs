﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/14 11:10:13 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartManagerServer : PartManager
    {
        public PartManagerServer(EntityMonsterBase owner)
            : base(owner)
        {
        }

        protected override PartSubject CreatePartSubject(int id)
        {
            return CombatLogicObjectPool.CreatePartSubjectServer(id);
        }

        public override bool SelectPart(int partId)
        {
            if (!base.SelectPart(partId))
            {
                return false;
            }
            SelectPartReq(partId);
            return false;
        }

        public override bool CancelCurrentSelectedPartManually()
        {
            if (!base.CancelCurrentSelectedPartManually())
            {
                return false;
            }
            CancelPartReq();
            return false;
        }

        private void SelectPartReq(int partId)
        {
            PlayerAvatar.Player.RpcCall("body_part_select_req", _owner.id, partId);
        }

        private void CancelPartReq()
        {
            PlayerAvatar.Player.RpcCall("body_part_select_req", _owner.id, 0);
        }

        public override void ResponseBodyPartSelect(byte partId)
        {
            bool result = true;
            if (partId == 0)
            {
                CancelPartResp();
            }
			else
			{
				result = SelectPartResp(partId);
			}
            if (result)
            {
                EventDispatcher.TriggerEvent<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, partId);
            }
        }

        private void CancelPartResp()
        {
            _currentSelectedSubject = null;
        }

        private bool SelectPartResp(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                LoggerHelper.Error(string.Format("接收部位选中信息处理失败,id为{0}的怪物没有id为{1}的部位.", _owner.monster_id, partId));
                return false;
            }
            _currentSelectedSubject = _partSubjects[partId];
            return true;
        }

        public override void ResponseBodyPartList(PbBodyPartList bodyPartList)
        {
            for (int i = 0; i < bodyPartList.bodyPartList.Count; ++i)
            {
                UpdateBodyPartInfo(bodyPartList.bodyPartList[i]);
            }
            ResponseBodyPartSelect((byte)bodyPartList.avatar_select_body);
        }

        private void UpdateBodyPartInfo(PbBodyPart pbBodyPart)
        {
            if (!_partSubjects.ContainsKey(pbBodyPart.body_part_id))
            {
                LoggerHelper.Error(string.Format("接收全部部位信息处理失败, id为{0}的怪物没有id为{1}的部位.", _owner.monster_id, pbBodyPart.body_part_id));
                return;
            }
            PartSubject partSubject = _partSubjects[pbBodyPart.body_part_id];
            (partSubject as PartSubjectServer).UpdateBodyPartInfo(pbBodyPart);
        }

        public override void ResponseBodyPartUpdate(PbBodyPart pbBodyPart)
        {
            UpdateBodyPartInfo(pbBodyPart);
        }
    }
}
