﻿using Common.Data;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/8 15:17:31 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartSubject
    {
        protected int _id;
        protected PartData _data;
        protected int _status;
        protected EntityMonsterBase _owner;
        protected List<int> _triggerHpList;
        protected int _currentBoneNameKey;

        public PartSubject()
        {
            _triggerHpList = new List<int>();
        }

        virtual public void Initialize(PartData data)
        {
            _id = data.id;
            _data = data;
            InitModels();
            if (!_data.models.ContainsKey(100))
            {
                LoggerHelper.Error("初始化Boss部位失败，model字段没有配置100的骨骼.");
            }
            else
            {
                _currentBoneNameKey = 100;
            }
        }

        virtual public void SetOwner(EntityMonsterBase owner)
        {
			_owner = owner;
            InitStatus(); 
        }

        virtual protected void InitStatus()
        {
			_status = public_config.BODY_PART_STATE_LOCK;
        }

        private void InitModels()
        {
            _triggerHpList.Clear();
            for (int i = 0; i < _data._hpList.Count; ++i)
            {
                if (_data._hpList[i] > 0 && _data._hpList[i] < 100)
                {
                    _triggerHpList.Add(_data._hpList[i]);
                }
            }
        }

        virtual public void Release()
        {
        }

        public int GetPriority()
        {
            return _data.sort;
        }

        public int Id
        {
            get { return _id; }
        }

        public PartData Data
        {
            get { return _data; }
        }

        virtual public int Status
        {
            get { return _status; }
            protected set
            {
                if (_status != value)
                {
                    //LoggerHelper.Info(string.Format("BossPart Change Status, last status = {0}, current status = {1}, boss id = {2}, part id = {3}", _status, value, _owner.monster_id, _id));
                    _status = value;
                    EventDispatcher.TriggerEvent<int, int>(SpaceActionEvents.SpaceActionEvents_Change_Boss_Part, _id, value);
                }
            }
        }

        public bool CanBeSelected()
        {
            return _status == public_config.BODY_PART_STATE_NORMAL;
        }

        protected void ChangeModel(int hpPercent)
        {
            int boneNameKey = -1;
            if (hpPercent == 0 || hpPercent == 100)
            {
                if (!_data.models.ContainsKey(hpPercent))
                {
                    LoggerHelper.Error(string.Format("转换模型失败，boss_part的model字段没有配置血量为{0}的骨骼.", hpPercent));
                    return;
                }
                boneNameKey = hpPercent;
            }
            else
            {
                int index = _triggerHpList.Count - 1;
                for (int i = 0; i < _triggerHpList.Count; ++i)
                {
                    if (_triggerHpList[i] <= hpPercent)
                    {
                        boneNameKey = _triggerHpList[i];
                        index = i;
                        break;
                    }
                }
                for (int i = _triggerHpList.Count - 1; i >= index; --i)
                {
                    _triggerHpList.Remove(i);
                }
            }
            if (boneNameKey != -1 && _currentBoneNameKey != boneNameKey)
            {
                SetPartGameObjectVisible(_currentBoneNameKey, false);
                SetPartGameObjectVisible(boneNameKey, true);
                _currentBoneNameKey = boneNameKey;
            }
        }

        private void SetPartGameObjectVisible(int boneNameKey, bool visible)
        {
            List<string> boneNameList = _data.models[boneNameKey];
            if (boneNameList == null)
            {
                //LoggerHelper.Error(string.Format("设置Boss部位显示错误，血量为{0}没有配置部位", boneNameKey));
            }

            string boneName;
            for (int i = 0; i < boneNameList.Count; ++i)
            {
                boneName = boneNameList[i];
                Transform partTrans = _owner.actor.boneController.GetBoneByName(boneName);
                if (partTrans == null)
                {
                    //LoggerHelper.Error(string.Format("设置Boss部位显示错误，找不到名为{0}的部位", boneName));
                    return;
                }
                partTrans.gameObject.SetActive(visible);
            }
        }

        virtual public void Update()
        {
        }

        virtual protected void Broken()
        {
            ChangeReadyDamagedState(true);
        }

        virtual protected void Recover()
        {
            ChangeReadyDamagedState(false);
        }

        private void ChangeReadyDamagedState(bool isDamaged)
        {
            if (!_data.readyDamaged)
            {
                return;
            }
            _owner.ChangeReadyDamagedState(isDamaged);
        }
    }
}
