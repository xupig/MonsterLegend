﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/14 10:15:55 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartSubjectClient : PartSubject
    {
        private bool _isRecoveringFinish;
        private float _brokenTimeStamp = 0;
        private float _unlockTimeStamp = 0;
        private float _unlockDuration = 0;
        private delegate bool CheckUnlockFun(List<float> args);
        private Dictionary<PartUnlockType, CheckUnlockFun> _checkUnlockFuncDict;

        public PartSubjectClient()
            : base()
        {
            InitCheckUnlockFunc();
        }

        private void InitCheckUnlockFunc()
        {
            _checkUnlockFuncDict = new Dictionary<PartUnlockType, CheckUnlockFun>();
            _checkUnlockFuncDict.Add(PartUnlockType.HP, CheckHpUnlockFunc);
        }

        public override void Initialize(PartData data)
        {
            _isRecoveringFinish = false;
            base.Initialize(data);
        }

        public override void SetOwner(EntityMonsterBase owner)
        {
            base.SetOwner(owner);
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.cur_hp, OwnerHpUpdatedHandler);
        }

        public override int Status
        {
            get
            {
                return base.Status;
            }
            protected set
            {
                if (_status != value && value == public_config.BODY_PART_STATE_NORMAL)
                {
                    AddNormalBuffs();
                }
                base.Status = value;
            }
        }

        protected override void InitStatus()
        {
            base.InitStatus();
            if (CheckSatisfyUnlockConditions())
            {
                Status = public_config.BODY_PART_STATE_NORMAL;
            }
        }

        public override void Release()
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.cur_hp, OwnerHpUpdatedHandler);
        }

        private bool CheckHpUnlockFunc(List<float> args)
        {
            float hpPercent = args[0];
            float curHpPercent = _owner.cur_hp * 1.0f / _owner.max_hp;
            return curHpPercent <= hpPercent;
        }

        private bool CheckSatisfyUnlockConditions()
        {
            if (_status != public_config.BODY_PART_STATE_LOCK)
            {
                return false;
            }
            if (_data.unlockConditions == null || _data.unlockConditions.Count == 0)
            {
                return true;
            }
            foreach (var node in _data.unlockConditions)
            {
                if (CheckSatisfySingleUnlockCondition((PartUnlockType)node.Key, node.Value))
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckSatisfySingleUnlockCondition(PartUnlockType conditionType, List<float> args)
        {
            if (_checkUnlockFuncDict.ContainsKey(conditionType))
            {
                return _checkUnlockFuncDict[conditionType](args);
            }
            return false;
        }

        private void OwnerHpUpdatedHandler()
        {
            if (_status != public_config.BODY_PART_STATE_LOCK)
            {
                return;
            }
            foreach (var node in _data.unlockConditions)
            {
                if ((PartUnlockType)node.Key == PartUnlockType.HP &&
                    CheckSatisfySingleUnlockCondition((PartUnlockType)node.Key, node.Value))
                {
                    Unlock(node.Value[1] * 0.001f);
                    return;
                }
            }
        }

        public SkillDamage CalculateDamage(Entities.EntityCreature attacker, Entities.EntityMonsterBase owner, int skillID, SkillAction skillAction, int targetCnt)
        {
            EntityPart entityBossPart = owner.GetEntityPartById(_id);
            SkillDamage result = SkillCalculate.CalculateDamage(attacker, entityBossPart, skillID, skillAction, targetCnt);
            var cur_hp = entityBossPart.cur_hp < result.value ? 0 : entityBossPart.cur_hp - result.value;
            entityBossPart.cur_hp = cur_hp;
            if (result.value > 0)
            {
                EventDispatcher.TriggerEvent<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed,
                                                            _id, cur_hp * 1f / entityBossPart.max_hp);
            }
            if (cur_hp <= 0)
            {
                Broken();
            }
            else
            {
                int hpPercent = Mathf.CeilToInt(cur_hp * 100.0f / entityBossPart.max_hp);
                ChangeModel(hpPercent);
            }
            return result;
        }

        public void AddNormalBuffs()
        {
            var buffs = _data.ownBuffs.GetEnumerator();
            while (buffs.MoveNext())
            {
                var rate = buffs.Current.Value[0];
                if (Random.Range(0f, 1f) > rate)
                {
                    continue;
                }
                //LoggerHelper.Info(string.Format("怪物id为{0}的部位{1}添加正常态buff，id为{2}", _owner.monster_id, _id, buffs.Current.Key));
                _owner.bufferManager.AddBuffer(buffs.Current.Key, buffs.Current.Value[1], true);
            }
        }

        protected void AddBrokenBuffs()
        {
            var buffs = _data.addBuffs.GetEnumerator();
            while (buffs.MoveNext())
            {
                var rate = buffs.Current.Value[0];
                if (Random.Range(0f, 1f) > rate)
                {
                    continue;
                }
                //LoggerHelper.Info(string.Format("怪物id为{0}的部位{1}添加破损buff，id为{2}", _owner.monster_id, _id, buffs.Current.Key));
                _owner.bufferManager.AddBuffer(buffs.Current.Key, buffs.Current.Value[1], true);
            }
        }

        private void CancelSelected()
        {
            _owner.partManager.CancelCurrentSelectedPartWhenBorken();
        }

        private void ChangeStatus()
        {
            if (_data.recoverDuration > 0)
            {
                Status = public_config.BODY_PART_STATE_RECOVER;
                _isRecoveringFinish = false;
            }
            else
            {
                Status = public_config.BODY_PART_STATE_DESTROY;
            }
        }

        public bool RecoverPart()
        {
            if (!IsRecoveringFinish())
            {
                //LoggerHelper.Error(string.Format("部位id{0}无法恢复，因为该部位不在恢复完成状态", _id));
                return false;
            }
            Recover();
            return true;
        }

        public bool IsRecoveringFinish()
        {
            return _status == public_config.BODY_PART_STATE_RECOVER && _isRecoveringFinish;
        }

        public override void Update()
        {
            switch (_status)
            {
                case public_config.BODY_PART_STATE_RECOVER:
                    UpdateRecoverTime();
                    break;
                case public_config.BODY_PART_STATE_NORMAL:
                    UpdateUnlockTime();
                    break;
                default:
                    break;
            }
        }

        private void UpdateRecoverTime()
        {
            if (!_isRecoveringFinish &&
                Time.realtimeSinceStartup - _brokenTimeStamp >= _data.recoverDuration)
            {
                _isRecoveringFinish = true;
            }
        }

        private void UpdateUnlockTime()
        {
            if (_unlockDuration == 0 || _unlockTimeStamp == 0)
            {
                return;
            }
            if (Time.realtimeSinceStartup - _unlockTimeStamp >= _unlockDuration)
            {
                Lock();
            }
        }

        protected override void Broken()
        {
 	        base.Broken();
            _brokenTimeStamp = Time.realtimeSinceStartup;
            ChangeStatus();
            ChangeModel(0);
            AddBrokenBuffs();
            CancelSelected();
            EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_BOSS_PART_DESTROY, _owner.monster_id + ":" + _id);
        }

        protected override void Recover()
        {
 	        base.Recover();
            Status = public_config.BODY_PART_STATE_NORMAL;
            EntityPart entityBossPart = _owner.GetEntityPartById(_id);
            entityBossPart.cur_hp = entityBossPart.max_hp;
            ChangeModel(100);
        }

        private void Unlock(float duration)
        {
            Status = public_config.BODY_PART_STATE_NORMAL;
            _unlockTimeStamp = Time.realtimeSinceStartup;
            _unlockDuration = duration;
        }

        private void Lock()
        {
            Status = public_config.BODY_PART_STATE_LOCK;
            _unlockTimeStamp = 0;
            _unlockDuration = 0;
        }
    }
}
