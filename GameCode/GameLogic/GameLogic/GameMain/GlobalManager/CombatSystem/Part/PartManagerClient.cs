﻿using Common.ClientConfig;
using Common.Events;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/14 10:03:28 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartManagerClient : PartManager
    {
        public PartManagerClient(EntityMonsterBase owner)
            : base(owner)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.max_hp, OwnerMaxHpUpdatedHandler);
        }

        public override void Release()
        {
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.max_hp, OwnerMaxHpUpdatedHandler);
            base.Release();
        }

        protected override PartSubject CreatePartSubject(int id)
        {
            return CombatLogicObjectPool.CreatePartSubjectClient(id);
        }

        private void OwnerMaxHpUpdatedHandler()
        {
            for (int i = 0; i < _partIdList.Count; ++i)
            {
                _owner.GetEntityPartById(_partIdList[i]).UpdateMaxHp();
            }
        }

        public override SkillDamage CalculateDamage(EntityCreature attacker, int skillID, SkillAction skillAction, int targetCnt)
        {
            if (_currentSelectedSubject == null)
            {
                LoggerHelper.Error(string.Format("You can't calculate current boss part's damage because current selected part is null. SkillID is {0}, SkillAction id is {1}", skillID, skillAction.actionID));
                return new SkillDamage();
            }
            return (_currentSelectedSubject as PartSubjectClient).CalculateDamage(attacker, _owner, skillID, skillAction, targetCnt);
        }

        public override bool SelectPart(int partId)
        {
            if (!base.SelectPart(partId))
            {
                return false;
            }
            _currentSelectedSubject = _partSubjects[partId];
            EventDispatcher.TriggerEvent<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, partId);
            return true;
        }

        public override bool CancelCurrentSelectedPartManually()
        {
            if (!base.CancelCurrentSelectedPartManually())
            {
                return false;
            }
            _currentSelectedSubject = null;
            return true;
        }

        public override bool RecoverPart(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                LoggerHelper.Error(string.Format("恢复部位失败，怪物id{0}没有部位{1}", _owner.monster_id, partId));
                return false;
            }
            return (_partSubjects[partId] as PartSubjectClient).RecoverPart();
        }

        public override bool PartCanBeRecovered(int partId)
        {
            if (!_partSubjects.ContainsKey(partId))
            {
                return false;
            }
            return (_partSubjects[partId] as PartSubjectClient).IsRecoveringFinish();
        }
    }
}
