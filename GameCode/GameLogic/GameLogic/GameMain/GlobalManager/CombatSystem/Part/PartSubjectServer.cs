﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using MogoEngine.Events;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/14 11:14:12 
 * function: 
 * *******************************************************/

namespace GameMain.CombatSystem
{
    public class PartSubjectServer : PartSubject
    {
        public void UpdateBodyPartInfo(PbBodyPart pbBodyPart)
        {
            UpdateStatusInfo(pbBodyPart.state);
            UpdateHpInfo(pbBodyPart.hp, pbBodyPart.max_hp);
        }

        private void UpdateStatusInfo(int status)
        {
            if (_status == public_config.BODY_PART_STATE_NORMAL &&
                status == public_config.BODY_PART_STATE_DESTROY)
            {
                CancelSelected();
            }
            if (status == public_config.BODY_PART_STATE_DESTROY && _data.recoverDuration > 0)
            {
                status = public_config.BODY_PART_STATE_RECOVER;
            }
            if (_status != status)
            {
                if (status == public_config.BODY_PART_STATE_NORMAL &&
                    _status == public_config.BODY_PART_STATE_RECOVER)
                {
                    Recover();
                }
                Status = status;
            }
        }

        private void CancelSelected()
        {
            _owner.partManager.CancelCurrentSelectedPartWhenBorken();
        }

        private void UpdateHpInfo(int hp, int max_hp)
        {
            EntityPart entityBossPart = _owner.GetEntityPartById(_id);
            if (hp != entityBossPart.cur_hp || max_hp != entityBossPart.max_hp)
            {
                EventDispatcher.TriggerEvent<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed,
                                                            _id, hp * 1f / max_hp);
            }
            if (hp <= 0)
            {
                Broken();
            }
            else
            {
                int hpPercent = Mathf.CeilToInt(hp * 100.0f / max_hp);
                ChangeModel(hpPercent);
            }
            entityBossPart.cur_hp = hp;
            entityBossPart.max_hp = max_hp;
        }

        protected override void Broken()
        {
            base.Broken();
            ChangeModel(0);
        }

        protected override void Recover()
        {
 	        base.Recover();
            ChangeModel(100);
        }
    }
}
