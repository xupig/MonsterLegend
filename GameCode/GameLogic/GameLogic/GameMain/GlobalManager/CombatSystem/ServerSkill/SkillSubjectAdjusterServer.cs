﻿using Common.Data;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    static public class SkillSubjectAdjusterServer
    {
        static public List<int> GetCD(this SkillSubjectServer skillSubject)
        {
            List<int> result = skillSubject.skillData.cd;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.cd, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<int>> GetAddBuffsOnCast(this SkillSubjectServer skillSubject)
        {
            Dictionary<int, List<int>> result = skillSubject.skillData.addBuffsOnCast;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.addBuffsOnCast, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<SkillEventData> GetSkillEvents(this SkillSubjectServer skillSubject)
        {
            List<SkillEventData> result = skillSubject.skillData.skillEvents;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
				if (adjustData.skillEvents != null && adjustData.skillEvents.Count!=0)
                {
                    result = adjustData.skillEvents;
                }
            }
            return result;
        }

        static public int GetAccordingStick(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.accordingStick;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.accordingStick, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceLockMode(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.faceLockMode;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceLockMode, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceAlways(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.faceAlways;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceAlways, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchTargetRepeat(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.searchTargetRepeat;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchTargetRepeat, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public float GetActionCutTime(this SkillSubjectServer skillSubject)
        {
            float result = skillSubject.skillData.actionCutTime;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.actionCutTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
    }
}
