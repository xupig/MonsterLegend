﻿using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    public class SkillActionServer
    {
        static public float DEFAULT_DURATION = 5;
        public int skillID;
        public int actionID;

        protected uint _mainTargetID;
        public uint mainTargetID
        {
            set {
                _mainTargetID = value;
                if (_actHandler != null) _actHandler.mainTargetID = value;
            }
        }

        protected EntityCreature _owner;
        protected ACTHandler _actHandler;

        public PbSpellAction pbSpellAction;

        protected SkillActionStage _curStage = SkillActionStage.STANDBY;
        public SkillActionStage curState
        {
            get { return this._curStage; }
        }

        protected SkillActionData _skillActionData;
        public SkillActionData skillActionData { get { return _skillActionData; } }
        protected List<SkillActionData> _adjustSkillActionDataList = new List<SkillActionData>();
        public List<SkillActionData> adjustSkillActionDataList { get { return _adjustSkillActionDataList; } }

        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();

        protected List<SkillActionMovement> _skillActionMovements = new List<SkillActionMovement>();

        protected RegionJudgeTime _regionJudgeTime;

        protected float _duration = 0;
        protected float _pastTime = 0;
        private float _startTimeStamp = 0;
        protected float _fireTime = 0;
        public float fireTime
        {
            get { return this._fireTime; }
        }

        public SkillActionServer()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillActionData data)
        {
            _duration = DEFAULT_DURATION;
            _pastTime = 0;
            actionID = data.actionID;
            _mainTargetID = 0;
            _curStage = SkillActionStage.STANDBY;
            _skillActionData = data;
            _adjustSkillActionDataList.Clear();
        }


        private void ShowEarlyWarning(PbSpellActionOrigin spellActionOrigin)
        {
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self
                && this._owner != PlayerAvatar.Player)
            {
                return;
            }
            var transform = this._owner.GetTransform();
            if (transform == null)return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = spellActionOrigin.origin_x * 0.01f;
            curOwnerPosition.z = spellActionOrigin.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(0, spellActionOrigin.origin_face * 2, 0), Vector3.one);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, duration);
        }

        private void ShowEarlyWarning(PbSpellAction spellAction)
        {
            
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if (spellAction.origin_face == 361) //代表服务端找不到判定原点
            {
                return;
            }
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self
                && this._owner != PlayerAvatar.Player)
            {
                return;
            }
            var transform = this._owner.GetTransform();
            if (transform == null) return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = curOwnerPosition.x + spellAction.origin_x * 0.01f;
            curOwnerPosition.z = curOwnerPosition.z + spellAction.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(0, spellAction.origin_face * 2, 0), Vector3.one);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, duration);
        }

        public void OnFixTime(PbSpellActionOrigin spellActionOrigin)
        {
            var transform = this._owner.GetTransform();
            if (transform == null) return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = spellActionOrigin.origin_x * 0.01f;
            curOwnerPosition.z = spellActionOrigin.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(0, spellActionOrigin.origin_face * 2, 0), Vector3.one);
            _actHandler.actionActiveOrigin = judgeMatrix.position();
            _actHandler.OnFixTime(this._owner.actor);
        }

        public void AddAdjustSkillActionData(SkillActionData skillActionData)
        {
            if (_adjustSkillActionDataList.Contains(skillActionData)) return;
            _adjustSkillActionDataList.Add(skillActionData);
        }

        public void SetActionOriginForDelay(PbSpellActionOrigin spellActionOrigin)
        {
            ShowEarlyWarning(spellActionOrigin);
            OnFixTime(spellActionOrigin);
        }

        private void SetACTHandlerActiveOrigin()
        { 
            if(pbSpellAction != null){
                var curOwnerPosition = _owner.position;
                curOwnerPosition.x = curOwnerPosition.x + pbSpellAction.origin_x * 0.01f;
                curOwnerPosition.z = curOwnerPosition.z + pbSpellAction.origin_y * 0.01f;
                _actHandler.actionActiveOrigin = curOwnerPosition;
            }
        }

        private void CalculateDuration()
        {
            for (int i = 0; i < _skillActionMovements.Count; i++)
            {
                _duration = Mathf.Max(_skillActionMovements[i].startTime - RealTime.time, _duration);
            }
            for (int i = 0; i < _skillEventList.Count; i++)
            {
                _duration = Mathf.Max(_skillEventList[i].delay, _duration);
            }
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        private void ResetSkillEventList()
        {
            _skillEventList.Clear();
            for (int i = 0; i < _skillActionData.skillEvents.Count; i++)
            {
                var eventData = _skillActionData.skillEvents[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                _skillEventList.Add(_skillActionData.skillEvents[i]);
            }
        }

        private void ResetRegionJudgeTime()
        {
            var data = this.GetRegionJudgeTime();
            if (data < 0)
            {
                _regionJudgeTime = RegionJudgeTime.OnFixTime;
            }
            else
            {
                _regionJudgeTime = (RegionJudgeTime)data;
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.Clear();
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public void Clear()
        {
            _owner = null;
            _skillActionData = null;
            pbSpellAction = null;
            _skillActionMovements.Clear();
        }

        bool IsAllEnd()
        {
            if (_skillActionMovements.Count > 0) return false;
            if (_skillEventList.Count > 0) return false;
            if (_fireTime > 0) return false;
            return true;
        }

        public SkillActionStage Update()
        {
            if (_curStage == SkillActionStage.STANDBY)
            {
                Begin();
                DoingEvents();
            }
            else if (_curStage == SkillActionStage.ACTIVE)
            {
                //_pastTime += Time.deltaTime;
                _pastTime = RealTime.time - _startTimeStamp;
                if (_pastTime >= _duration && IsAllEnd())
                {
                    End();
                }
                else
                {
                    DoingEvents();
                    Moving();
                }
            }
            return _curStage;
        }

        void Begin()
        {
            _curStage = SkillActionStage.ACTIVE;
            OnBegin();
        }

        protected void OnBegin()
        {
            _startTimeStamp = RealTime.time;
            ResetFireTime();
            ResetRegionJudgeTime();
            ResetSkillEventList();
            SetSelfMovement();
            CalculateDuration();
            SetACTHandlerActiveOrigin();
            if (_regionJudgeTime == RegionJudgeTime.OnSkillActive
                || _regionJudgeTime == RegionJudgeTime.OnActionActive)
                ShowEarlyWarning(this.pbSpellAction);
        }

        public void End()
        {
            _curStage = SkillActionStage.ENDED;
        }

        private void ResetFireTime()
        {
            _fireTime = this.pbSpellAction.fire_delay_time * 0.001f + RealTime.time;
        }

        public void Judge(PbSpellDamageInfo skillDamageInfo)
        {
            var damages = skillDamageInfo.damages;
            for (int i = 0; i < damages.Count; i++)
            {
                PbSpellDamageUnit damageInfo = damages[i];
                var events = damageInfo.events;
                EntityCreature target = MogoWorld.GetEntity(damageInfo.target_id) as EntityCreature;
                if (target != null && target.actor != null)
                {
                    _actHandler.OnTargetBeHit(_owner.actor, target, delegate()
                    {
                        if (damageInfo.damage > 0)
                        {
                            TargetBeHit(target);
                        }
                    });
                    bool isPlayer = target == PlayerAvatar.Player;
                    AttackType attackType = CombatSystemTools.DamageMode2AttackType(events);
                    if (CombatSystemTools.CanShowDamage(this._owner, target) && !(attackType == AttackType.ATTACK_HIT && (int)damageInfo.damage == 0))
                    {
                        PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, -(int)damageInfo.damage, attackType, isPlayer);
                    }
                    if (isPlayer && GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD)
                    {
                        EventDispatcher.TriggerEvent(BattleUIEvents.BATTLE_PLAYER_ATTACKED);
                    }
                    if (damageInfo.can_move == 1)   //水平位移
                    {
                        SetTargetMovement(target, skillDamageInfo, damageInfo);
                        target.actor.FaceTo(_owner.position);
                    }
                    else if (damageInfo.can_move == 2) //没有水平位移
                    {
                        SetTargetUpSpeedMovement(target);
                        target.actor.FaceTo(_owner.position);
                    }
                }
            }
            var originPosX = (skillDamageInfo.ent_x + skillDamageInfo.origin_x) * 0.01f;
            var originPosY = _owner.position.y;
            var originPosZ = (skillDamageInfo.ent_y + skillDamageInfo.origin_y) * 0.01f;
            _actHandler.actionJudgeOrigin = new Vector3(originPosX, originPosY, originPosZ);
            _actHandler.OnJudge(this._owner.actor);
            if (_duration == DEFAULT_DURATION) _duration = 0;
            CalculateAddEp();
        }

        private void CalculateAddEp()
        {
            var usecosts = this.GetFireCosts();
            if (_owner == PlayerAvatar.Player && usecosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
            {
                int oldEp = _owner.cur_ep;
                Int16 cur_ep = (Int16)(_owner.cur_ep - usecosts[public_config.ITEM_SPECIAL_SPELL_ENERGY]);
                cur_ep = (Int16)Mathf.Min(_owner.max_ep, Mathf.Max(0, cur_ep));
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.ADD_EP, oldEp, cur_ep);
            }
        }

        void TargetBeHit(EntityCreature target)
        {
            if (target.actor != null)
            {
                if (CombatSystemTools.CanShowDamage(this._owner, target))
                {
                    var hlSetting = global_params_helper.GetHighLightSetting();
                    target.actor.equipController.HighLight(hlSetting[0], hlSetting[1]);
                }
                target.OnHit();
            }
        }

        #region 持续效果

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void Moving()
        {
            for (int i = _skillActionMovements.Count - 1; i >= 0; i--)
            {
                var movement = _skillActionMovements[i];
                if (movement.startTime < RealTime.time)
                {
                    _skillActionMovements.RemoveAt(i);
                    _owner.checkCrashWall = false;
                    ExecuteMovement(movement);
                }
            }
        }

        void ExecuteMovement(SkillActionMovement movement)
        {
            var moveEntity = CombatSystemTools.GetCreatureByID(movement.moveEntityID);
            if (moveEntity == null) return;
            var srcPosition = moveEntity.position;

            Vector3 targetPosition = movement.targetPosition;
            var dir = targetPosition - srcPosition;
            var dis = Mathf.Min(dir.magnitude, movement.maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
            }
        }

        #endregion 持续效果

        #region 瞬间效果

        List<float> FixMoveArgs(MovementType movementType, List<float> srcMoveArgs)
        {
            List<float> curMoveArgs = srcMoveArgs;
            if (MovementType.AccordingAuto == movementType)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target == null && srcMoveArgs.Count >= 12)
                {
                    curMoveArgs = new List<float>();
                    for (int i = 6; i < 12; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            return srcMoveArgs;
        }

        SkillActionMovement GetSelfActionMovement(List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            movement.offsetX = curMoveArgs[0] * 0.01f;
            movement.offsetY = curMoveArgs[1] * 0.01f;
            movement.maxMoveDistance = curMoveArgs[2] * 0.01f;
            var delay = curMoveArgs[3] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.baseSpeed = curMoveArgs[4] * 0.01f;
            movement.accelerate = curMoveArgs[5] * 0.01f;
            if (curMoveArgs.Count >= 7) movement.upSpeed = curMoveArgs[6] * 0.01f;
            if (curMoveArgs.Count >= 8) movement.upAccelerate = curMoveArgs[7] * 0.01f;
            return movement;
        }

        void SetSelfMovement()
        {
            if (pbSpellAction.can_move == 0) return;
            MovementType moveType = (MovementType)this.GetSelfMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetSelfMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            var movement = GetSelfActionMovement(curMoveArgs);
            movement.moveEntityID = _owner.id;
            movement.targetPosition = _owner.position;
            movement.targetPosition.x = movement.targetPosition.x + pbSpellAction.dest_x * 0.01f;
            movement.targetPosition.z = movement.targetPosition.z + pbSpellAction.dest_y * 0.01f;
            movement.startTime = RealTime.time + pbSpellAction.delay_time * 0.001f;
            _skillActionMovements.Add(movement);
        }

        SkillActionMovement GetActionMovement(EntityCreature moveEntity, Matrix4x4 moveSource, Matrix4x4 moveTarget, MovementType moveType, List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            movement.offsetX = curMoveArgs[0] * 0.01f;
            movement.offsetY = curMoveArgs[1] * 0.01f;
            movement.maxMoveDistance = curMoveArgs[2] * 0.01f;
            var delay = curMoveArgs[3] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.baseSpeed = curMoveArgs[4] * 0.01f;
            movement.accelerate = curMoveArgs[5] * 0.01f;
            if (curMoveArgs.Count >= 7) movement.upSpeed = curMoveArgs[6] * 0.01f;
            if (curMoveArgs.Count >= 8) movement.upAccelerate = curMoveArgs[7] * 0.01f;

            movement.movementType = moveType;
            movement.moveEntityID = moveEntity.id;

            if (moveSource == moveTarget)
            {
                Matrix4x4 m1 = Matrix4x4.identity;
                m1.SetColumn(3, new Vector4(movement.offsetY, 0, movement.offsetX, 1));
                moveSource = moveSource * m1;
                movement.targetPosition = new Vector3(moveSource.m03, moveSource.m13, moveSource.m23);
            }
            else
            {
                var direction = (moveTarget.position() - moveSource.position()).normalized * movement.offsetX;
                var rightDirection = (new Vector3(-direction.z, direction.y, direction.x)).normalized * movement.offsetY;
                var offset = direction + rightDirection;
                var tarPosition = moveTarget.position();
                movement.targetPosition = new Vector3(tarPosition.x + offset.x, tarPosition.y + offset.y, tarPosition.z + offset.z);
            }
            return movement;
        }

        SkillActionMovement GetTargetActionMovement(List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            movement.offsetX = curMoveArgs[0] * 0.01f;
            movement.offsetY = curMoveArgs[1] * 0.01f;
            movement.maxMoveDistance = curMoveArgs[2] * 0.01f;
            var delay = curMoveArgs[3] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.baseSpeed = curMoveArgs[4] * 0.01f;
            movement.accelerate = curMoveArgs[5] * 0.01f;
            if (curMoveArgs.Count >= 7) movement.upSpeed = curMoveArgs[6] * 0.01f;
            if (curMoveArgs.Count >= 8) movement.upAccelerate = curMoveArgs[7] * 0.01f;
            return movement;
        }

        void SetTargetMovement(EntityCreature moveEntity, PbSpellDamageInfo skillDamageInfo, PbSpellDamageUnit pbSpellDamageUnit)
        {
            MovementType moveType = (MovementType)this.GetTargetMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetTargetMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);

            Vector3 moveSource = Vector3.zero;
            Vector3 moveTarget = Vector3.zero;

            float casterFace = skillDamageInfo.ent_face * 2f;
            var casterPosX = skillDamageInfo.ent_x * 0.01f;
            var casterPosY = _owner.position.y;
            var casterPosZ = skillDamageInfo.ent_y * 0.01f;
            var casterPos = new Vector3(casterPosX, casterPosY, casterPosZ);
            var targetPosX = casterPosX + pbSpellDamageUnit.offset_x * 0.01f;
            var targetPosY = _owner.position.y;
            var targetPosZ = casterPosZ + pbSpellDamageUnit.offset_y * 0.01f;
            var targetPos = new Vector3(targetPosX, targetPosY, targetPosZ);
            var movement = GetTargetActionMovement(curMoveArgs);
            Vector3 targetPosition;
            switch (moveType)
            {
                case MovementType.AccordingSelf:
                case MovementType.AccordingJudgePoint:
                    moveSource = casterPos;
                    moveTarget = casterPos;
                    Matrix4x4 m = Matrix4x4.TRS(moveSource, Quaternion.Euler(0, casterFace, 0), Vector3.one);
                    Matrix4x4 m1 = Matrix4x4.identity;
                    m1.SetColumn(3, new Vector4(movement.offsetY, 0, movement.offsetX, 1));
                    m = m * m1;
                    targetPosition = new Vector3(m.m03, m.m13, m.m23);
                    break;
                case MovementType.AccordingTarget:
                case MovementType.AccordingJudgeTarget:
                    moveSource = casterPos;
                    moveTarget = targetPos;
                    var direction = (moveTarget - moveSource).normalized * movement.offsetX;
                    var rightDirection = (new Vector3(-direction.z, direction.y, direction.x)).normalized * movement.offsetY;
                    var offset = direction + rightDirection;
                    targetPosition = new Vector3(moveTarget.x + offset.x, moveTarget.y + offset.y, moveTarget.z + offset.z);
                    break;
                default:
                    return;
            }
            
            var srcPosition = moveEntity.position;
            var dir = targetPosition - srcPosition;
            var dis = Mathf.Min(dir.magnitude, movement.maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.checkCrashWall = true;
            if (moveEntity is EntityMonsterBase)
            {
                targetPosition.x = casterPosX + pbSpellDamageUnit.dest_x * 0.01f;
                targetPosition.y = _owner.position.y;
                targetPosition.z = casterPosZ + pbSpellDamageUnit.dest_y * 0.01f;
                moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate);
            }
            else {
                moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate);
            }
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
            }
        }

        void SetTargetUpSpeedMovement(EntityCreature moveEntity)
        {
            MovementType moveType = (MovementType)this.GetTargetMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetTargetMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            var movement = GetTargetActionMovement(curMoveArgs);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
            }
        }
        #endregion
    }
}
