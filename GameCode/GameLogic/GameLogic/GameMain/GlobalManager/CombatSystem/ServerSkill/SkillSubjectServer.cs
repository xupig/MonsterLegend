﻿using Common.Data;
using Common.Global;
using Common.States;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SkillSubjectServer
    {
        public int skillID;

        EntityCreature _owner;
        ACTHandler _actHandler;

        uint _mainTargetID;
        public uint mainTargetID
        { 
            get { return _mainTargetID; }
            set { _mainTargetID = value; }
        }

        uint _lockTargetID;

        float _alwaysFaceTargetEndTime = 0;

        SkillState _curState = SkillState.STANDBY;
        public SkillState curState
        {
            get { return _curState; }
        }

        bool _isBreaked = false;

        SkillData _skillData;
        public SkillData skillData { get { return _skillData; } }

        List<SkillData> _adjustSkillDataList = new List<SkillData>();
        public List<SkillData> adjustSkillDataList { get { return _adjustSkillDataList; } }

        List<SkillEventData> _skillEventList = new List<SkillEventData>();
        List<SkillBuffer> _skillBufferList = new List<SkillBuffer>();

        float _duration = 1;
        public float duration { get { return _duration; } }
        float _startTimeStamp = 0;
        float _pastTime = 0;
        bool _checkCut = false;

        public SkillSubjectServer()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillData data)
        {
            _curState = SkillState.STANDBY;
            _pastTime = 0;
            _isBreaked = false;
            _checkCut = false;
            _skillData = data;
            _mainTargetID = 0;
            _alwaysFaceTargetEndTime = this.GetFaceAlways() * 0.001f + RealTime.time;
            _duration = this.GetActionCutTime() * 0.001f;

            skillID = _skillData.skillID;
            ClearAdjustSkillData();
        }

        public void ClearAdjustSkillData()
        {
            _adjustSkillDataList.Clear();
        }

        public void AddAdjustSkillData(SkillData skillData)
        {
            if (_adjustSkillDataList.Contains(skillData)) return;
            _adjustSkillDataList.Add(skillData);
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = this.GetSkillEvents();
            for (int i = 0; i < skillEvents.Count; i++)
            {
                _skillEventList.Add(skillEvents[i]);
            }
        }

        void ResetSkillBufferList()
        {
            _skillBufferList.Clear();
            var addBuffsOnCast = this.GetAddBuffsOnCast();
            if (addBuffsOnCast == null) return;
            foreach (var kv in addBuffsOnCast)
            {
                var skillBuffer = new SkillBuffer();
                skillBuffer.bufferID = kv.Key;
                skillBuffer.delay = kv.Value[0] * 0.001f;
                skillBuffer.duration = kv.Value[1];
                _skillBufferList.Add(skillBuffer);
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public SkillState Update()
        {
            if (_curState == SkillState.STANDBY)
            {
                Begin();
            }
            else if (_curState == SkillState.ACTIVE)
            {
                _pastTime = RealTime.time - _startTimeStamp;
                if (_isBreaked || _pastTime >= _duration)
                {
                    End();
                }
                else
                {
                    Doing();
                }
            }
            CheckCut();
            return _curState;
        }

        public void Break()
        {
            if (!_isBreaked)
            {
                if (_curState != SkillState.ENDED)
                {
                    OnBreak();
                }
                _actHandler.BreakAllVisualFX();
            }
            _isBreaked = true;
        }

        public void Stop()
        {
			End();
			_owner = null;
            _skillData = null;
        }

        void Begin()
        {
            _curState = SkillState.ACTIVE;
            _startTimeStamp = RealTime.time;
            ChangeMoveMode();
            ResetSkillEventList();
            FindSkillForSkillBegin();
            if (_owner == PlayerAvatar.Player && !(_owner as PlayerAvatar).BeControlled())
            {
                OnBeginByPlayer();
            }
            else if (_owner is IEntityFollower)
            {
                OnBeginByFollower();
            }
            Doing();
        }

        void Doing()
        {
            DoingBuffers();
            DoingEvents();
            FacingTarget();
        }

        void End()
        {
            if (_curState == SkillState.ENDED) return;
            _curState = SkillState.ENDED;
            if (!_isBreaked)
            {
                OnBreak();
                CheckCut();
            }
            _actHandler.Clear();
        }

        void OnBreak()
        {
            _actHandler.ResetDefaultState(this._owner.actor);
            _skillBufferList.Clear();
            _skillEventList.Clear();
        }

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void DoingBuffers()
        {
            for (int i = _skillBufferList.Count - 1; i >= 0; i--)
            {
                var bufferData = _skillBufferList[i];
                if (bufferData.delay <= _pastTime)
                {
                    _owner.bufferManager.AddBuffer(bufferData.bufferID, bufferData.duration, true, false, false);
                    _skillBufferList.RemoveAt(i);
                }
            }
        }

        void FacingTarget()
        {
            if (_alwaysFaceTargetEndTime > RealTime.time)
            {
                LockDirection();
            }
        }

        void CheckCut()
        {
            if (_isBreaked) return;
            if (_checkCut) return;
            if (_owner.actor == null) return;
            if (_pastTime >= _duration && InMovingState())
            {
                _checkCut = true;
                _owner.actor.animationController.ReturnReady();
            }
        }

        bool InMovingState()
        {
            if (_owner is PlayerAvatar)
            {
                return ControlStickState.strength > 0 || _owner.actor.actorState.MovingSpeedState != 0;
            }
            return _owner.actor.actorState.MovingSpeedState != 0;
        }

        void FindSkillForSkillBegin()
        {
            var faceLockMode = (FaceLockMode)this.GetFaceLockMode();
            if (faceLockMode == FaceLockMode.LockOnSkillActive
                || faceLockMode == FaceLockMode.LockDuringSkill)
            {
                _lockTargetID = this._mainTargetID;
                LockDirection();
            }
        }

        void OnBeginByPlayer()
        {
            if (platform_helper.InPCPlatform())
            {
                FaceAccordingMouse();
            }
            else
            {
                FaceAccordingStick();
            }
            SetCommonCD();
            ResetSkillBufferList();
            CastSpellReq();
            RemoveConditionBuff();
        }

        void OnBeginByFollower()
        {
            if (!(_owner as IEntityFollower).IsFollower())
            {
                return;
            }
            SetCommonCD();
            ResetSkillBufferList();
            FollowerCastSpellReq();
            RemoveConditionBuff();
        }

        void ChangeMoveMode()
        {
            if (this._owner == PlayerAvatar.Player)
            {
                this._owner.moveManager.MoveBySkill(_duration);
            }
        }

        void FaceAccordingStick()
        {
            if (this.GetAccordingStick() == 0) return;
            if (ControlStickState.strength <= 0) return;
            Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
            _owner.actor.forward = moveDirection;
        }

        void FaceAccordingMouse()
        {
            var mousePos = ControlStickState.GetMoveDirectionByMouse();
            PlayerAvatar.Player.actor.FaceTo(mousePos);    
        }

        void SetCommonCD()
        {
            var cd = this.GetCD();
            _owner.skillManager.SetCommonCD(skillID, cd[1]);
        }

        void RemoveConditionBuff()
        {
            if (this._skillData.showType > 0)
            {
                _owner.bufferManager.RemoveBuffer(this._skillData.showType);
            }
        }

        void LockDirection()
        {
            var entity = CombatSystemTools.GetCreatureByID(_lockTargetID);
            if (entity != null && _owner.actor != null)
            {
                _owner.actor.FaceTo(entity.actor);
            }
        }

        #region player相关操作
        void CastSpellReq()
        {
            Int16 playerPosX = (Int16)(this._owner.position.x * 100);
            Int16 playerPosZ = (Int16)(this._owner.position.z * 100);
			PlayerAvatar.Player.RpcCall("cast_spell_req", skillID, playerPosX, playerPosZ, this._owner.face, Global.localTimeStamp);
        }

        public void CastSpellResp(uint mainTargetID)
        {
            _mainTargetID = mainTargetID;
            FindSkillForSkillBegin();
        }
        #endregion

        #region follower相关操作
        void FollowerCastSpellReq()
        {
            Int16 playerPosX = (Int16)(this._owner.position.x * 100);
            Int16 playerPosZ = (Int16)(this._owner.position.z * 100);
            PlayerAvatar.Player.RpcCall("follower_cast_spell_req", _owner.id, skillID, playerPosX, playerPosZ, this._owner.face, Global.localTimeStamp);
        }

        public void FollowerCastSpellResp(uint mainTargetID)
        {
            _mainTargetID = mainTargetID;
            FindSkillForSkillBegin();
        }
        #endregion
    }


}
