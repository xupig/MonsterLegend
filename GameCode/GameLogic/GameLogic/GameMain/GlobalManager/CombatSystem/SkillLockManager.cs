﻿
using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using UnityEngine;
namespace GameMain.CombatSystem
{
    public class SkillLockManager
    {
        private uint _manualLockId = 0;
        public uint manualLockId
        {
            get
            {
                return _manualLockId;
            }
            private set
            {
                if (_manualLockId != value)
                {
                    _manualLockId = value;
                    OnManualLockIDChanged();
                }
            }
        }

        private uint _skillLockId = 0;
        public uint skillLockId
        {
            get
            {
                return _skillLockId;
            }
            set
            {
                _skillLockId = value;
            }
        }

        EntityCreature _owner;
        public SkillLockManager(EntityCreature owner)
        {
            _owner = owner;
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_DEATH, OnEntityDeath);
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_LEAVE_WORLD, OnLeaveWorld);
            EventDispatcher.AddEventListener<uint, uint>(AIEvents.AIEvents_OnChangeAITarget, OnChangeAITarget);
            if (_owner is PlayerAvatar)
            {
                EventDispatcher.AddEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnTouchEntity);
                EventDispatcher.AddEventListener(TouchEvents.ON_DOUBLE_TOUCH_BLANK_SPACE, OnDoubleTouchBlankSpace);
            }
        }

        public void Release()
        {
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_DEATH, OnEntityDeath);
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_LEAVE_WORLD, OnLeaveWorld);
            EventDispatcher.RemoveEventListener<uint, uint>(AIEvents.AIEvents_OnChangeAITarget, OnChangeAITarget);
            if (_owner is PlayerAvatar)
            {
                EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnTouchEntity);
                EventDispatcher.RemoveEventListener(TouchEvents.ON_DOUBLE_TOUCH_BLANK_SPACE, OnDoubleTouchBlankSpace);
            }
            _owner = null;
        }

        /// <summary>
        /// 实体加载完的初始化
        /// </summary>
        public void InitSkillLockBuff()
        {
            if (PlayerAvatar.Player.manualLockId == _owner.id)
            {
                _owner.bufferManager.AddBuffer(InstanceDefine.BUFF_ID_SKILL_LOCK);
            }
        }

        /// <summary>
        /// 锁定技能目标
        /// </summary>
        /// <param name="id">实体id</param>
        public bool ManualLockEntity(uint id)
        {
            if (!CanManualLockEntity(id))
            {
                return false;
            }
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_MANUAL_LOCK_ENTITY, id);
            if (_owner.entityAI != null)
            {
                _owner.entityAI.blackBoard.inFightingState = true;
            }
            if (manualLockId == id)
            {
                return false;
            }
            bool isServerMonster = GameSceneManager.GetInstance().IsServerMonster();
            if (isServerMonster)
            {
                ManualLockEntityReq(id);
            }
            else
            {
                ManualLockEntityResp(id);
            }
            return true;
        }

        /// <summary>
        /// 解锁技能目标
        /// </summary>
        public bool ManualUnlockEntity()
        {
            if (manualLockId == 0)
            {
                return false;
            }
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_MANUAL_LOCK_ENTITY, 0);
            bool isServerMonster = GameSceneManager.GetInstance().IsServerMonster();
            if (isServerMonster)
            {
                ManualUnlockEntityReq();
            }
            else
            {
                ManualUnlockEntityResp();
            }
            return true;
        }

        private bool CanManualLockEntity(uint id)
        {
            if (id == 0)
            {
                return false;
            }
            EntityCreature creature = GetCreature(id);
            if (creature != null)
            {
                if (creature.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                {
                    return false;
                }
                return TargetFilter.IsEnemy(_owner, creature);
            }
            return false;
        }

        private void ManualLockEntityReq(uint id)
        {
            PlayerAvatar.Player.RpcCall("spell_target_req", id);
        }

        public void ManualLockEntityResp(uint id)
        {
            RemoveManualLockBuff();
            manualLockId = id;
            EntityCreature creature = GetCreature(id);
            if (creature != null)
            {
                creature.bufferManager.AddBuffer(InstanceDefine.BUFF_ID_SKILL_LOCK);
                if (creature.entityAI != null)
                {
                    creature.entityAI.blackBoard.enemyId = manualLockId;
                }
            }
        }

        private void ManualUnlockEntityReq()
        {
            PlayerAvatar.Player.RpcCall("spell_target_req", 0);
        }

        public void ManualUnlockEntityResp()
        {
            if (manualLockId == 0)
            {
                return;
            }
            RemoveManualLockBuff();
            manualLockId = 0;
        }

        private void RemoveManualLockBuff()
        {
            EntityCreature creature = GetCreature(manualLockId);
            if (creature != null)
            {
                creature.bufferManager.RemoveBuffer(InstanceDefine.BUFF_ID_SKILL_LOCK);
            }
        }

        private void OnEntityDeath(uint id)
        {
            if (manualLockId != 0 && manualLockId == id)
            {
                ManualUnlockEntity();
            }
        }

        private void OnLeaveWorld(uint id)
        {
            if (manualLockId != 0 && manualLockId == id)
            {
                ManualUnlockEntity();
            }
        }

        private void OnChangeAITarget(uint entityId, uint targetId)
        {
            if (entityId != _owner.id || targetId == skillLockId)
            {
                return;
            }
            skillLockId = targetId;
        }

        private void OnTouchEntity(uint id)
        {
            if (!CanTouchEntity())
            {
                return;
            }
            if (id != 0)
            {
                ManualLockEntity(id);
            }
            else
            {
                ManualUnlockEntity();
            }
        }

        private void OnDoubleTouchBlankSpace()
        {
            if (platform_helper.InTouchPlatform() && PlayerTouchBattleModeManager.GetInstance().inTouchModeState)
            {
                return;
            }
            if (manualLockId != 0)
            {
                ManualUnlockEntity();
            }
        }

        private void OnManualLockIDChanged()
        {
            EventDispatcher.TriggerEvent<uint, uint>(EntityEvents.ON_MANUAL_LOCK_ID_CHANGED, _owner.id, _manualLockId);
            //ChangeCameraMotion();
        }

        private bool CanTouchEntity()
        {
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_CITY)
            {
                return false;
            }
            if (CameraManager.GetInstance().mainCamera.accordingNode == CameraAccordingMode.AccordingAnimation)
            {
                return false;
            }
            if (DramaManager.GetInstance().IsInViewingMode())
            {
                return false;
            }
            return true;
        }

        private EntityCreature GetCreature(uint id)
        {
            Entity entity = MogoWorld.GetEntity(id);
            if (entity == null && !(entity is EntityCreature))
            {
                return null;
            }
            return entity as EntityCreature;
        }

        private void ChangeCameraMotion()
        {
            if (!ShouldChangeCameraMotion())
            {
                return;
            }
            if (manualLockId > 0)
            {
                EntityCreature creature = GetCreature(manualLockId);
                if (creature != null)
                {
                    Transform self = _owner.actor.boneController.GetBoneByName("slot_camera");
                    Transform oppenent = creature.actor.boneController.GetBoneByName("slot_camera");
                    if (self != null && oppenent != null)
                    {
                        CameraManager.GetInstance().ChangeToOneVsOneMotion(self, oppenent);
                    }
                }
            }
            else
            {
                CameraManager.GetInstance().ChangeBackFromOneVsOneMotion();
            }
        }

        private bool ShouldChangeCameraMotion()
        {
            return map_helper.GetUseLockEntityCameraMotion(GameSceneManager.GetInstance().curMapID) == 0;
        }
    }
}
