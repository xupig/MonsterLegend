﻿using GameLoader.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class SkillPreviewScene
    {
        private GameObject _sceneObj = null;
        private Camera _camera = null;
        private bool _created = false;
        public bool create
        {
            get { return _created; }
            set { _created = value; }
        }
        public SkillPreviewScene()
        {
 
        }

        public void CreateSceneModel(Action<GameObject> callBack = null)
        {
            _created = true;
            if (IsActorNull(callBack)) return;
            Game.Asset.ObjectPool.Instance.GetGameObject("Scenes/1003_Skill/1003_Skill.prefab", (obj) => 
            {
                _sceneObj = obj;
                obj.transform.position = new Vector3(20000,1000,20000);
                obj.name = "SkillPreviewScene";
                //obj.layer = UnityEngine.LayerMask.NameToLayer("SkillPreview");
                _camera = obj.transform.GetComponentInChildren<Camera>();
                //LoggerHelper.Error("[SkillPreviewManager:CreateSceneModel]=>1______obj.layer: " + obj.layer);
                _camera.transform.localPosition = new Vector3(-33.7f, 11.2f, 66.6f);
                _camera.transform.localEulerAngles = new Vector3(20f, 180f, 2.854f);


                if(callBack != null)
                {
                    callBack(_sceneObj);
                }
            });
        }

        private bool IsActorNull(Action<GameObject> callBack = null)
        {
            if (_sceneObj != null)
            {
                if (callBack != null) callBack(_sceneObj);
                return true;
            }
            return false;
        }

    }
}
