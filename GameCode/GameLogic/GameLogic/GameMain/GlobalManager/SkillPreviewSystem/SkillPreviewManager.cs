﻿using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.GlobalManager
{
    public class SkillPreviewManager
    {
        private static SkillPreviewManager _instance = null;
        public static SkillPreviewManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new SkillPreviewManager();
            }
            return _instance;
        }

        private EntityPreview _player = null;
        private SkillPreviewScene _scene = null;
        private GameObject _sceneGameObject = null;

        public SkillPreviewManager()
        {
            _scene = new SkillPreviewScene();

        }

        private void CreateModel(spell setSpell)
        {
            if (_sceneGameObject != null)
            {
                SetActiveSceneGameTrue();
            }

            if (_scene.create == true) return;
            _scene.CreateSceneModel((sceneGameObject) =>
                {
                    //_role.CreateRoleModel(sceneGameObject, (actor) =>
                    //    {
                    //        _role.SetSpell(setSpell);
                    //        _role.CastSpell();
                    //    });
                    _sceneGameObject = sceneGameObject as GameObject;
                    CreateRole(sceneGameObject, setSpell, () => { SetActiveSceneGameTrue(); _scene.create = false; });
                });
        }

        private void SetActiveSceneGameTrue()
        {
            PlayerAvatar.Player.actor.GetComponent<AudioListener>().enabled = false;
            _sceneGameObject.SetActive(true); 
            
        }


        private void SetActiveSceneGameFalse()
        {
            if (_sceneGameObject == null) return;
            PlayerAvatar.Player.actor.GetComponent<AudioListener>().enabled = true;
            _sceneGameObject.SetActive(false);
        }

        private void CreateRole(GameObject sceneGameObject, spell setSpell,Action callBack)
        {
            //LoggerHelper.Error("[SkillPreviewManager:CreateRole]=>1____________________player:  " + _player);
            if (_player != null)
            {
                if(_player.actor != null)
                {
                    // 将预览界面的人物模型调整到和场景人物一致
                    PutOnScenePlayerFacade();
                    _player.ResetLocalTransform();
                    _player.SetSkillID(setSpell.__id);
                    _player.DelayPlaySkill();
                    callBack();
                    return;
                }
                
            }

            MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_PREVIEW, (entity) =>
            {
                //LoggerHelper.Error("[SkillPreviewManager:CreateRole]=>2____________________player:  " + _player);
                _player = entity as GameMain.Entities.EntityPreview;
                _player.vocation = GameMain.Entities.PlayerAvatar.Player.vocation;
                _player.facade = GameMain.Entities.PlayerAvatar.Player.equipManager.GetEquipInfoStr();
                _player.canBeHiddenInCamera   = false;
                //
                _player.SetSceneSlot(sceneGameObject);
                _player.SetSkillID(setSpell.__id);
                //_player.DelayPlaySkill();
                callBack();
            });
        }

        private void PutOnScenePlayerFacade()
        {
            if (PlayerAvatar.Player.actor.equipController.equipCloth.curEquipData != null)
            {
                int playerClothId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipData.ID;
                int playerClothParticleId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipParticleID;
                int playerClothFlowId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipFlowID;
                _player.actor.equipController.equipCloth.PutOn(playerClothId, playerClothParticleId, playerClothFlowId);
            }

            if (PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipData != null)
            {
                int playerWeaponId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipData.ID;
                int playerWeaponParticleId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipParticleID;
                int playerWeaponFlowId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipFlowID;
                _player.actor.equipController.equipWeapon.PutOn(playerWeaponId, playerWeaponParticleId, playerWeaponFlowId);
            }
            if (PlayerAvatar.Player.actor.equipController.equipWing.curEquipData != null)
            {
                int playerWingId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipData.ID;
                int playerWingParticleId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipParticleID;
                int playerWingFlowId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipFlowID;
                _player.actor.equipController.equipWing.PutOn(playerWingId, playerWingParticleId, playerWingFlowId);
            }
        }


        public void Show(spell setSpell)
        {
            CreateModel(setSpell);
        }

        public void Close()
        {
            SetActiveSceneGameFalse();

        }



    }
}
