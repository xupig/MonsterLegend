﻿#region 模块信息
/*==========================================
// 文件名：KParticleManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/10/28 10:12:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain.GlobalManager
{
    public class KParticleManager
    {
        private static KParticleManager s_instance = null;
        public static KParticleManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new KParticleManager();
            }
            return s_instance;
        }

        private KParticleManager()
        {
            panelParticleDict = new Dictionary<PanelIdEnum, HashSet<KParticle>>();
            panelParticleLayerDict = new Dictionary<PanelIdEnum, int>();
        }

        private Dictionary<PanelIdEnum, HashSet<KParticle>> panelParticleDict;
        private Dictionary<PanelIdEnum, int> panelParticleLayerDict;

        public void RegisterParticle(PanelIdEnum panelId,KParticle particle)
        {
            if(panelParticleDict.ContainsKey(panelId) == false)
            {
                panelParticleDict.Add(panelId, new HashSet<KParticle>());
            }
            if(panelParticleDict[panelId].Contains(particle) == false)
            {
                panelParticleDict[panelId].Add(particle);
            }
            if(panelParticleLayerDict.ContainsKey(panelId))
            {
                MogoGameObjectHelper.ChangeGameObjectAllLayer(particle.gameObject, panelParticleLayerDict[panelId]);
            }
            else
            {
                MogoGameObjectHelper.ChangeGameObjectAllLayer(particle.gameObject, UIManager.UI_LAYER);
            }
        }

        public void UnregisterParticle(PanelIdEnum panelId, KParticle particle)
        {
            if (panelParticleDict.ContainsKey(panelId) && panelParticleDict[panelId].Contains(particle))
            {
                panelParticleDict[panelId].Remove(particle);
            }
        }

        public void ShowPanelParticle(PanelIdEnum panelId)
        {
            if (panelParticleLayerDict.ContainsKey(panelId) == false)
            {
                panelParticleLayerDict.Add(panelId, UIManager.UI_LAYER);
            }
            else
            {
                panelParticleLayerDict[panelId] = UIManager.UI_LAYER;
            }
            if (panelParticleDict.ContainsKey(panelId))
            {
                HashSet<KParticle> particleSet = panelParticleDict[panelId];
                foreach(KParticle particle in particleSet)
                {
                    MogoGameObjectHelper.ChangeGameObjectAllLayer(particle.gameObject, UIManager.UI_LAYER);
                }
            }
        }

        public void HidePanelParticle(PanelIdEnum panelId)
        {
            if (panelParticleLayerDict.ContainsKey(panelId) == false)
            {
                panelParticleLayerDict.Add(panelId, UIManager.TransparentFX_LAYER);
            }
            else
            {
                panelParticleLayerDict[panelId] = UIManager.TransparentFX_LAYER;
            }
            if (panelParticleDict.ContainsKey(panelId))
            {
                HashSet<KParticle> particleSet = panelParticleDict[panelId];
                foreach (KParticle particle in particleSet)
                {
                    MogoGameObjectHelper.ChangeGameObjectAllLayer(particle.gameObject, UIManager.TransparentFX_LAYER);
                }
            }
        }

    }
}
