﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class SoundRecoverBase
    {
        private Dictionary<int, string> _recordClipDict = null;

        public SoundRecoverBase()
        {
            _recordClipDict = new Dictionary<int, string>();
        }

        public void AddToRecordDict(int id,string musicFilePath)
        {
            if (_recordClipDict.ContainsKey(id)) return;
            _recordClipDict.Add(id, musicFilePath);
        }

        public void Release()
        {
            if (_recordClipDict.Count <= 0) return;
            //var enumerator = _recordClipDict.GetEnumerator();
            //while (enumerator.MoveNext())
            //{
            //    Game.Asset.ObjectPool.Instance.Release(enumerator.Current.Value);
            //}
            foreach (var item in _recordClipDict)
            {
                Game.Asset.ObjectPool.Instance.Release(item.Value);
            }
            _recordClipDict.Clear();
        }

    }
}
