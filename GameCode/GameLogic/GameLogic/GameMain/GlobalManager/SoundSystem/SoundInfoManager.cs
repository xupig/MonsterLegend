﻿using Game.Asset;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendTools;
using Common.Utils;


namespace GameMain.GlobalManager
{
    public class SoundInfoManager : Singleton<SoundInfoManager>
    {

        private GameObject _gameObject = null;
        //private bool _clickVoicePlaying = false;

        public SoundInfoManager()
            : base()
        {
 
        }

        private void GetGameObject()
        {
            if (_gameObject == null)
            {
                _gameObject = new GameObject();
                _gameObject.name = "SND_SLOT";
                _gameObject.transform.SetParent( GameMain.Entities.PlayerAvatar.Player.actor.gameObject.transform, false);
                _gameObject.transform.ResetPositionAngles();
            }
        }

        private SoundBase GetSound(int soundID)
        {
            GetGameObject();
            return SoundPool.GetInstance().GetSound(_gameObject, soundID);
        }

        public uint PlaySound(int soundID,bool loop = false, Action endCallback = null)
        {
            float settingSoundVolume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume;
            if (Mathf.Approximately(settingSoundVolume, 0))
            {
                return 0;
            }
            SoundBase sound = GetSound(soundID);
            //添加优先级判定
            if (sound.JudgePriority(sound_helper.Priority(soundID)) == false) return 0;
            sound.Play(soundID, loop, endCallback);
            return sound.id;
        }

        public bool StopSound(uint id)
        {
            SoundBase sound = SoundPool.GetInstance().GetSound(id);
            if (sound == null)
            {
                return false;
            }
            sound.Stop();
            return true;
        }

        /// <summary>
        /// 播放点击语音
        /// 语音播放后通过定时器设置一个CD时间
        /// </summary>
        /// <param name="soundID"></param>
        /// <param name="loop"></param>
        public void PlayClickVoice(int soundID, bool loop = false)
        {
            PlaySound(soundID, loop);
        }

    }
}
