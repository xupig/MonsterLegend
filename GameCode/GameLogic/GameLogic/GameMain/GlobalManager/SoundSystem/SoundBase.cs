﻿using Common.Data;
using Game.Asset;
using GameData;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class SoundBaseUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum SoundBaseStatus
    {
        IDLE = 0,
        LOADING,
        USING
    }
    public class SoundBase
    {
        private static uint _idCounter = 0;
        private static uint GetID()
        {
            return ++_idCounter;
        }

        private uint _id = 0;
        public uint id
        {
            get { return _id; }
        }

        private AudioSource _audioSource = null;
        public AudioSource audioSource
        {
            get { return _audioSource; }
            set { _audioSource = value; }
        }

        private int _priority = 0;
        public int priority
        {
            get 
            {
                return _priority;
            }
        }

        private Action _endCallback;

        private bool _isLoading = false;

        private SoundBaseStatus _status;
        public SoundBaseStatus status
        {
            get { return _status; }
            set
            {
                _status = value;
                switch (value)
                {
                    case SoundBaseStatus.IDLE:
				    case SoundBaseStatus.LOADING:
                        MogoWorld.UnregisterUpdate("SoundBase.Update", Update);
                        break;
                    case SoundBaseStatus.USING:
                        MogoWorld.RegisterUpdate<SoundBaseUpdateDelegate>("SoundBase.Update", Update);
                        break;
                    default:
                        break;
                }
            }
        }

        private float _volumeBak;

        public SoundBase(GameObject soundSlot)
        {
            _id = GetID();
            _audioSource = soundSlot.AddComponent<AudioSource>();
            status = SoundBaseStatus.IDLE;
            _isLoading = false;
        }

        internal void SetAudioSource(AudioClip clip, bool loop)
        {
            if (_audioSource == null) return;
            _audioSource.clip = clip;
            _audioSource.loop = loop;
            _audioSource.volume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume;
        }

        private void Update()
        {
            if (CheckIsPlaying() == false)
            {
                End();
            }
            
        }

        private bool CheckIsPlaying()
        {
            if (_audioSource == null) return false;
            return _audioSource.isPlaying;
        }

        public void Play(int soundID, bool loop, Action endCallback)
        {
            if (_audioSource == null) return;
            _endCallback = endCallback;
            status = SoundBaseStatus.LOADING;
            _isLoading = true;
            ObjectPool.Instance.GetAudioClip(sound_helper.Path(soundID), (clip) =>
            {
                _isLoading = false;
                SoundRecoverManager.instance.GetSoundRecover(SoundRecoverManager.VOICE).AddToRecordDict(clip.GetInstanceID(), sound_helper.Path(soundID));
                if (status == SoundBaseStatus.IDLE)
                {
                    return;
                }
                status = SoundBaseStatus.USING;
                SetAudioSource(clip, loop);
                this._priority = priority;
                _audioSource.Play();
            });
        }

        public void Stop()
        {
            if (_audioSource != null)
            {
                _audioSource.Stop();
            }
            status = SoundBaseStatus.IDLE;
        }

        public void End()
        {
            status = SoundBaseStatus.IDLE;
            if (_audioSource != null)
            {
                _audioSource.Stop();
            }
            if (_endCallback != null)
            {
                _endCallback();
                _endCallback = null;
            }
        }

        public void Reset()
        {
            if (_audioSource != null)
            {
                _audioSource.Stop();
            }
            status = SoundBaseStatus.IDLE;
            _isLoading = false;
        }

        public bool CanUse()
        {
            return status == SoundBaseStatus.IDLE && !_isLoading;
        }

        public bool JudgePriority(int targetPriority)
        {
            if (audioSource == null) return false;
            if (audioSource.isPlaying == false) return true;

            return this._priority < targetPriority ? true : false;
        }

        #region 临时音量控制
        public void TempCloseVolume()
        {
            if (audioSource == null) return;
            _volumeBak = audioSource.volume;
            audioSource.volume = 0f;
        }

        public void ResetTempVolume()
        {
            if (audioSource == null) return;
            audioSource.volume = _volumeBak;
        }
        #endregion

    }
}
