﻿
using Common.Data;
using GameData;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class CreatureDataManager
    {
        private List<uint> _entityIDList = new List<uint>();
        private List<string> _showEntityTypeList = null;
        private HashSet<uint> _taskMonsterIdSet = new HashSet<uint>();
        private List<uint> _outofEntityIdList = new List<uint>();

        //用于管理任务怪
        private Dictionary<uint, uint> _entityIdToMonsterIdDic = new Dictionary<uint, uint>();

        private static CreatureDataManager s_instance = null;
        public static CreatureDataManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new CreatureDataManager();
            }
            return s_instance;
        }

        public void ResetEntityTypeList()
        {
            _showEntityTypeList = null;
        }

        public List<uint> GetOutofEntityIdList()
        {
            return _outofEntityIdList;
        }

        public void ClearOutofEntityIdList()
        {
            _outofEntityIdList.Clear();
        }

        public List<uint> GetEntityIDList()
        {
            return _entityIDList;
        }

        public void ClearEntityIdList()
        {
            _entityIDList.Clear();
        }

        public void RefreshMonsterSet()
        {
            List<int> taskIdList = PlayerDataManager.Instance.TaskData.GetAcceptTaskIdList();
            HashSet<uint> taskMonsterIdSet = new HashSet<uint>();
            for(int i = 0; i < taskIdList.Count; i++)
            {
                Dictionary<TaskEventEnum, TaskEventData> data = task_data_helper.GetTaskEventData(taskIdList[i]);
                if (data.ContainsKey(TaskEventEnum.MonsterId) == true)
                {
                    taskMonsterIdSet.Add((uint)data[TaskEventEnum.MonsterId].value);
                }
            }
            _taskMonsterIdSet.ExceptWith(taskMonsterIdSet);
            for (int i = 0; i < _entityIDList.Count; i++)
            {
                if (_entityIdToMonsterIdDic.ContainsKey(_entityIDList[i]) == true && _taskMonsterIdSet.Contains(_entityIdToMonsterIdDic[_entityIDList[i]]) == true)
                {
                    RemoveEntityID(_entityIDList[i]);
                }
            }
            _taskMonsterIdSet = taskMonsterIdSet;
        }

        public void AddEntityID(uint entityId)
        {
            if (!_entityIDList.Contains(entityId))
            {
                /*将机器人和木桩添加视为玩家*/
                Entity entity = MogoWorld.GetEntity(entityId);
                    
                if (entity == null) return;
                string entityType = EntityConfig.ENTITY_TYPE_CREATURE;
                if (entity.entityType == EntityConfig.ENTITY_TYPE_PUPPET
                    || entity.entityType == EntityConfig.ENTITY_TYPE_AGENT)
                {
                    entityType = EntityConfig.ENTITY_TYPE_AVATAR;
                }
                else
                {
                    entityType = entity.entityType;
                }
                if (_showEntityTypeList == null)
                {
                    ChapterType chapterType = (ChapterType)GameSceneManager.GetInstance().curMapType;
                    _showEntityTypeList = inst_type_operate_helper.GetShowEntityTypeList(chapterType);
                }

                if (entity is EntityMonsterBase)
                {
                    EntityMonsterBase monster = entity as EntityMonsterBase;
                    if (_taskMonsterIdSet.Contains(monster.monster_id) == false)
                    {
                        return;
                    }
                    _entityIdToMonsterIdDic[entityId] = monster.monster_id;
                }
                else if (_showEntityTypeList.Contains(entityType) == false)
                {
                    return;
                }
                _entityIDList.Add(entityId);
            }
        }

        public void RemoveEntityID(uint entityId)
        {
            if (_entityIDList.Contains(entityId))
            {
                _entityIDList.Remove(entityId);
                if (_entityIdToMonsterIdDic.ContainsKey(entityId) == true)
                {
                    _entityIdToMonsterIdDic.Remove(entityId);
                }
            }
            _outofEntityIdList.Add(entityId);
        }

    }
}
