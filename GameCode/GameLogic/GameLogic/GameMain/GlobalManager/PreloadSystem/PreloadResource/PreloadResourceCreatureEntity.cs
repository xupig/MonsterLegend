﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;
using MogoEngine;
using GameMain.Entities;

namespace GameMain.GlobalManager
{
    public class PreloadResourceCreatureEntity : PreloadResourceBase
    {
        public PreloadResourceCreatureEntity(uint entityID)
        {
            priority = PreloadPriority.PRIORITY_TWO; 
            var creature = MogoWorld.GetEntity(entityID) as EntityCreature;
            ExtractSkillResourcePath(creature);
        }

        void ExtractSkillResourcePath(EntityCreature creature)
        {
            var skillIdsList = creature.skillManager.GetLearnedSkillList();
            for(int i = 0;i<skillIdsList.Count;i++)
            {
                this._paths.AddRange(ACTResource.GetSkillResourcePaths(skillIdsList[i]));
            }
        }
    }
}
