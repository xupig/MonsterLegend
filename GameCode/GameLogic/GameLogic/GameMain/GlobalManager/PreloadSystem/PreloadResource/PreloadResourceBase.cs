﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;

namespace GameMain.GlobalManager
{
    public class PreloadPriority
    {
        public const uint PRIORITY_ONE = 1;
        public const uint PRIORITY_TWO = 2;
        public const uint PRIORITY_THREE = 3;
    }

    public class PreloadResourceBase
    {
        public uint priority = PreloadPriority.PRIORITY_ONE;  //策略优先级

        protected List<string> _paths = new List<string>();

        public PreloadResourceBase()
        { 
            
        }

        public bool Update()
        {
            if (_paths.Count == 0) return false;
            if (!ObjectPool.Instance.HasLoadingAsset())
            {
                try
                {
                    ObjectPool.Instance.PreloadAsset(new string[] { _paths[0] });
                }
                catch (Exception e)
                {
                    LoggerHelper.Error("Preload Error:" + e.Message);
                }
                _paths.RemoveAt(0);
            }
            return true;
        }
    }
}
