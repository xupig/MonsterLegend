﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;
using MogoEngine;
using GameMain.Entities;

namespace GameMain.GlobalManager
{
    public class PreloadResourceDrama : PreloadResourceBase
    {
        public PreloadResourceDrama(string eventName, string eventValue)
        {
            priority = PreloadPriority.PRIORITY_THREE;  
            DramaResource.GetDramaPreloadResource(this._paths, eventName, eventValue);
        }
    }
}
