﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;

namespace GameMain.GlobalManager
{
    public class PreloadResourceMonster : PreloadResourceBase
    {
        public PreloadResourceMonster(int monsterID)
        {
            ExtractModelPath(monsterID);
            ExtractSkillResourcePath(monsterID);
        }

        void ExtractModelPath(int monsterID)
        {
            var actorID = monster_helper.GetMonsterModel(monsterID);
            string modelPath = ACTSystemAdapter.GetActorModelPath(actorID);
            if (string.IsNullOrEmpty(modelPath)) return;
            this._paths.Add(modelPath);
        }

        void ExtractSkillResourcePath(int monsterID)
        {
            var skillIdsList = monster_helper.GetMonsterSkills(monsterID);
            if (skillIdsList == null) return;
            for(int i = 0;i<skillIdsList.Count;i++)
            {
                this._paths.AddRange(ACTResource.GetSkillResourcePaths(skillIdsList[i]));
            }
        }
    }
}
