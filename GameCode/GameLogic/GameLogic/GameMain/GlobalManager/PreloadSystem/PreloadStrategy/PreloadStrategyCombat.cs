﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;
using SpaceSystem;
using MogoEngine.Events;
using Common.Events;
using MogoEngine;
using GameMain.Entities;

namespace GameMain.GlobalManager
{
    public class PreloadStrategyCombat : PreloadStrategyBase
    {
        public PreloadStrategyCombat(int mapID)
        {
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_ENTER_SPACE, OnEntityEnterSpace);
            AddToResources(new PreloadResourceCreatureEntity(PlayerAvatar.Player.id));
            HandleSpaceData();
        }

        public override void Stop()
        {
            base.Stop();
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_ENTER_SPACE, OnEntityEnterSpace);
        }

        void OnEntityEnterSpace(uint entityID)
        {
            var entity = MogoWorld.GetEntity(entityID);
            if (entity == null) return;
            switch (entity.entityType)
            {
                case EntityConfig.ENTITY_TYPE_AVATAR:
                case EntityConfig.ENTITY_TYPE_PET:
                case EntityConfig.ENTITY_TYPE_PUPPET:
                case EntityConfig.ENTITY_TYPE_MONSTER:
                    AddToResources(new PreloadResourceCreatureEntity(entityID));
                    break;
                default:
                    break;
            }
        }

        void HandleSpaceData()
        {
            var spaceData = GameSceneManager.GetInstance().curSpaceData;
            var listData = spaceData.entities.entityList;
            for (int i = 0; i < listData.Count; i++)
            {
                var type = listData[i].type;
                if (type == EntityType.SPAWN_MONSTER_ACTION)
                {
                    //HandleEntitySpawnMonsterActionData(listData[i]); 
                    //怪物预加载放在刷的时候，后面加更资源的资源加载预测。
                }
                else if (type == EntityType.PLAY_CG_ACTION)
                {
                    HandleEntityPlayCGActionData(listData[i]);
                }
            }
        }

        void HandleEntitySpawnMonsterActionData(EntityData data)
        {
            var actionData = data as EntitySpawnMonsterActionData;
            var monsterIDStrList = actionData.monster_ids_l.Split(',');
            for (int j = 0; j < monsterIDStrList.Length; j++)
            {
                if (string.IsNullOrEmpty(monsterIDStrList[j])) break;
                int monsterID = int.Parse(monsterIDStrList[j]);
                AddToResources(new PreloadResourceMonster(monsterID));
            }
        }

        void HandleEntityPlayCGActionData(EntityData data)
        {
            var actionData = data as EntityPlayCGActionData;
            AddToResources(new PreloadResourceDrama(actionData.cg_event_name, actionData.cg_event_value));
        }
    }
}
