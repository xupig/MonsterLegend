﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;
using SpaceSystem;
using MogoEngine.Events;
using Common.Events;
using MogoEngine;
using GameMain.Entities;

namespace GameMain.GlobalManager
{
    public class PreloadStrategyWild : PreloadStrategyBase
    {
        public PreloadStrategyWild(int mapID)
        {
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_ENTER_SPACE, OnEntityEnterSpace);
            AddToResources(new PreloadResourceCreatureEntity(PlayerAvatar.Player.id));
        }

        public override void Stop()
        {
            base.Stop();
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_ENTER_SPACE, OnEntityEnterSpace);
        }

        void OnEntityEnterSpace(uint entityID)
        {
            var entity = MogoWorld.GetEntity(entityID);
            if (entity == null) return;
            switch (entity.entityType)
            {
                case EntityConfig.ENTITY_TYPE_MONSTER:
                    AddToResources(new PreloadResourceCreatureEntity(entityID));
                    break;
                default:
                    break;
            }
        }
    }
}
