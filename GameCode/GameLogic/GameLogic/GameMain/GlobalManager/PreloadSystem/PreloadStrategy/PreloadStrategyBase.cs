﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using Game.Asset;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.GlobalManager
{
    public class PreloadStrategyBase
    {
        protected List<PreloadResourceBase> _resources = new List<PreloadResourceBase>();

        public PreloadStrategyBase()
        {

        }

        protected virtual void AddToResources(PreloadResourceBase resource)
        {
            int idx = 0;
            for (int i = 0; i < _resources.Count; i++)
            {
                if (_resources[i].priority < resource.priority)
                {
                    break;
                }
                idx++;
            }
            _resources.Insert(idx, resource);
        }

        public virtual bool Update()
        {
            if (_resources.Count == 0) return true;
            if (!_resources[0].Update())
            {
                _resources.RemoveAt(0);
            }
            return true;
        }

        public virtual void Stop()
        { 
            
        }
    }
}
