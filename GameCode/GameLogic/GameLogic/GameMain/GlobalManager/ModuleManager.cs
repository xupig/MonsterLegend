﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

using Common.IManager;
using Common.Base;
//using Common.IModules;
using GameLoader.Config;
using GameLoader.Utils;
using UnityEngine;
using Common;

namespace GameMain.GlobalManager
{
    public class ModuleManager : IModuleManager
    {
        public static string MODULE_MARK = "Module";
        private Dictionary<string, BaseModule> moduleMap;

        private static ModuleManager s_instance = null;
        public static ModuleManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new ModuleManager();
            }
            return s_instance;
        }

        public ModuleManager()
        {
            moduleMap = new Dictionary<string, BaseModule>();
        }

        /**初始化**/
        public void Init()
        {
            HashSet<string> modulesSet = new HashSet<string>();
            if (!string.IsNullOrEmpty(SystemConfig.CloseModules))
            {
                string[] moduleNames = SystemConfig.CloseModules.Split(',');
                for (int j = 0; j < moduleNames.Length; j++)
                {
                    string modulesName = moduleNames[j];
                    if (!string.IsNullOrEmpty(modulesName)) modulesSet.Add(modulesName);
                }
            }
            for (int i = 0; i < ModuleConfig.PreLoadLibList.Count; i++)
            {
                string name = ModuleConfig.PreLoadLibList[i];
                if (modulesSet.Contains(name))continue;
                try
                {
                    RegisterModule(name);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Except(ex);
                }
            }
        }

        /**移除模块
         * @param moduleName 模块名
         * **/
        public void RemoveModule(string moduleName)
        {
            if (moduleMap.ContainsKey(moduleName))
            {
                //moduleMap[moduleName].Dispose();
                moduleMap.Remove(moduleName);
            }
        }

        /**获取模块
         * @param moduleName 模块名
         * **/
        public BaseModule GetModule(string moduleName)
        {
            if (!moduleMap.ContainsKey(moduleName))
            {
                return RegisterModule(moduleName);
            }
            return moduleMap[moduleName];
        }

        /**根据模块实例--注册模块
         * @param module 模块实例
         * **/
        public BaseModule RegisterModule(BaseModule module)
        {
            if (module == null) return null;
            return RegisterModule(module.GetType().ToString(), module);
        }

        public BaseModule RegisterModule(string moduleName)
        {
            if(moduleMap.ContainsKey(moduleName) == true)
            {
                return moduleMap[moduleName];
            } 
            string className = MODULE_MARK + moduleName + "." + moduleName + MODULE_MARK;
            Assembly ass = LoaderDriver.gameModulesAssembly;
            Type _type = ass.GetType(className);
            if (_type == null)
            {
                LoggerHelper.Error("RegisterModule  _type == null  please check your ModuleConfig.cs! className = " + className);
            }
            return RegisterModule(moduleName, (BaseModule)Activator.CreateInstance(ass.GetType(className)));
        }

        /**根据模块实例--注册模块
         * @param moduleName    module对应名称
         * @param module 模块实例
         * **/
        public BaseModule RegisterModule(string moduleName, BaseModule module)
        {
            //LoggerHelper.Debug("RegisterModule:" + moduleName + ":" + module);
            if (moduleName == null || module == null) return null;
            if (moduleMap.ContainsKey(moduleName)) return moduleMap[moduleName];
            module.moduleManager = this;
            module.Init();
            moduleMap[moduleName] = module;

            return module;
        }
    }
}
