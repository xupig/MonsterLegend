using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using ACTSystem;

namespace GameMain.GlobalManager
{
    public class NPCStateManager
    {
        private EntityNPC _owner;
        private uint _curFXGUID = 0;
        private int _curTaskType = 0;
        private int _curTaskState = 0;

        private Dictionary<int, Dictionary<int, int>> _taskStatusFXMap;
        private Dictionary<int, Dictionary<int, int>> taskStatusFXMap
        {
            get {
                if (_taskStatusFXMap == null)
                {
                    _taskStatusFXMap = new Dictionary<int, Dictionary<int, int>>();
                    _taskStatusFXMap[public_config.TASK_TYPE_MAIN] = new Dictionary<int, int>();
                    _taskStatusFXMap[public_config.TASK_TYPE_BRANCH] = new Dictionary<int, int>();
                    _taskStatusFXMap[public_config.TASK_TYPE_MAIN][public_config.TASK_STATE_NEW] = 10005;
                    _taskStatusFXMap[public_config.TASK_TYPE_MAIN][public_config.TASK_STATE_ACCOMPLISH] = 10007;
                    _taskStatusFXMap[public_config.TASK_TYPE_BRANCH][public_config.TASK_STATE_NEW] = 10004;
                    _taskStatusFXMap[public_config.TASK_TYPE_BRANCH][public_config.TASK_STATE_ACCOMPLISH] = 10006;
                }
                return _taskStatusFXMap;
            }
        }
        
        public NPCStateManager(EntityNPC ower)
        {
            _owner = ower;
        }

        public void SetTaskState(int taskType, int taskState)
        {
            _curTaskType = taskType;
            _curTaskState = taskState;
            ResetTaskState();
        }

        public void ResetTaskState()
        { 
            int fxID = 0;
            if (taskStatusFXMap.ContainsKey(_curTaskType)
                && taskStatusFXMap[_curTaskType].ContainsKey(_curTaskState))
            {
                fxID = taskStatusFXMap[_curTaskType][_curTaskState];
            }
            SetFXID(fxID);
        }

        void SetFXID(int fxID)
        {
            if (_owner.actor == null) return;
            if (_curFXGUID != 0)
            {
                ACTVisualFXManager.GetInstance().StopVisualFX(_curFXGUID);
                _curFXGUID = 0;
            }
            if (fxID > 0)
            {
                _curFXGUID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(fxID, _owner.actor, -1, 3);
            }
        }
    }

}