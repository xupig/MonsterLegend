﻿
using GameMain.Entities;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class CameraUtil
    {
        public static Transform GetDefaultCameraTarget()
        {
            return PlayerAvatar.Player.actor.boneController.GetBoneByName("slot_camera");
        }
    }
}
