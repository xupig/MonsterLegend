﻿using Common.States;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TouchRotateMotion
    {
        protected TouchStatus _status = TouchStatus.IDLE;
        public TouchStatus status { get { return _status; } }
        protected float _touchDistanceToRotation;
        private float _touchLeftRange = 0f;
        private float _touchRightRange = 0f;
        private float _touchTopRange = 0f;
        private float _touchBottomRange = 0f;
        protected float _originalRotationY = 0f;
        protected float _rotationYOffset = 0f;
        protected float _rotationY = 0f;
        public float rotationY { get { return _rotationY; } }

        public TouchRotateMotion(float touchDistanceToRotation,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            _touchDistanceToRotation = touchDistanceToRotation;
            _touchLeftRange = touchLeftRange;
            _touchRightRange = touchRightRange;
            _touchTopRange = touchTopRange;
            _touchBottomRange = touchBottomRange;
        }

        public void Start(float targetRotationY)
        {
            _originalRotationY = targetRotationY;
            _rotationYOffset = 0f;
            _rotationY = 0;
            _status = TouchStatus.IDLE;
            OnStart();
        }

        virtual protected void OnStart()
        {
        }

        virtual public void OnUpdate()
        {
        }

        public void UpdateArgs(float touchDistanceToRotation)
        {
            _touchDistanceToRotation = touchDistanceToRotation;
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchLeftRange = Screen.width * left;
            _touchRightRange = Screen.width * right;
            _touchTopRange = Screen.height * top;
            _touchBottomRange = Screen.height * bottom;
        }

        protected bool CheckRange(float x, float y)
        {
            return x >= _touchLeftRange && x <= _touchRightRange &&
                   y >= _touchBottomRange && y <= _touchTopRange;
        }

        protected int GetStickFingerId()
        {
            return ControlStickState.fingerId;
        }

        protected bool CheckFullUI()
        {
            return !UIManager.HasFullScreenPanel();
        }

        protected bool InControlStick()
        {
            return ControlStickState.strength > 0;
        }
    }
}
