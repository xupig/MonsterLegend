﻿
using Common;
using Common.States;
using Common.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TouchScaleMotion
    {
        protected TouchStatus _status = TouchStatus.IDLE;
        public TouchStatus status { get { return _status; } }
        protected float _maxDistance = 0;
        protected float _maxRotationX = 0f;
        protected float _minDistance = 0;
        protected float _minRotationX = 0f;
        private float _touchLeftRange = 0f;
        private float _touchRightRange = 0f;
        private float _touchTopRange = 0f;
        private float _touchBottomRange = 0f;
        protected float _curDistance = 0f;
        public float curDistance { get { return _curDistance; } }
        protected float _distance = 0f;
        public float distance { get { return _distance; } }
        protected float _rotationX = 0f;
        public float rotationX { get { return _rotationX; } }
        protected float _distanceScale = 1f;
        public float distanceScale { get { return _distanceScale; } }
        private List<string> _cacheList;

        public TouchScaleMotion(float distanceDefaultScale, float minDistance, float minRotationX,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            _cacheList = new List<string>();
            float localCache = ReadLocalCache();
            _distanceScale = (localCache < 0) ? distanceDefaultScale : localCache;
            _cacheList.Add(_distanceScale.ToString());
            _minDistance = minDistance;
            _minRotationX = minRotationX;
            _touchLeftRange = touchLeftRange;
            _touchRightRange = touchRightRange;
            _touchTopRange = touchTopRange;
            _touchBottomRange = touchBottomRange;
        }

        public void Start(float targetDistance, float targetRotationX, bool keepTouchesScale)
        {
            _maxDistance = targetDistance;
            _maxRotationX = targetRotationX;
            if (!keepTouchesScale)
            {
                _distanceScale = 1f;
                _curDistance = _maxDistance;
                WriteLocalCache();
            }
            else
            {
                _curDistance = (_maxDistance - _minDistance) * _distanceScale + _minDistance;
            }
            _status = TouchStatus.IDLE;
            OnStart();
        }

        virtual protected void OnStart()
        {
        }

        virtual public void OnUpdate()
        {
        }

        virtual public void UpdateArgs(float touchDistance2Distance, float minDistance, float minRotationX)
        {
            _minDistance = minDistance;
            _minRotationX = minRotationX;
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchLeftRange = Screen.width * left;
            _touchRightRange = Screen.width * right;
            _touchTopRange = Screen.height * top;
            _touchBottomRange = Screen.height * bottom;
        }

        public float GetRotationXByDistance(float distance)
        {
            //（rotationX(max)-rotationX）/（rotationX(max)-rotationX(min)）=
            //（distance(max)-distance）/（distance(max)-distance(min)）
            if (_maxRotationX == _minRotationX) return _maxRotationX;
            return _maxRotationX - (_maxRotationX - _minRotationX) * (_maxDistance - distance) / (_maxDistance - _minDistance);
        }

        protected bool CheckRange(float x, float y)
        {
            return x >= _touchLeftRange && x <= _touchRightRange &&
                   y >= _touchBottomRange && y <= _touchTopRange;
        }

        protected int GetStickFingerId()
        {
            return ControlStickState.fingerId;
        }

        protected bool CheckFullUI()
        {
            return !UIManager.HasFullScreenPanel();
        }

        protected float ReadLocalCache()
        {
            List<string> cache = LocalCache.Read<List<string>>(LocalName.TouchCameraScale);
            if (cache == null)
            {
                return -1f;
            }
            return float.Parse(cache[0]);
        }

        protected void WriteLocalCache()
        {
            _cacheList[0] = _distanceScale.ToString();
            LocalCache.Write(LocalName.TouchCameraScale, _cacheList, CacheLevel.Permanent);
        }
    }
}
