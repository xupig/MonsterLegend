﻿
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public enum TouchStatus
    {
        IDLE = 0,
        TOUCHING
    }

    public class BaseTouchesMotion
    {
        protected TouchScaleMotion _touchScaleMotion;
        public TouchStatus scaleStatus { get { return _touchScaleMotion.status; } }
        public float distance { get { return _touchScaleMotion.distance; } }
        public float rotationX { get { return _touchScaleMotion.rotationX; } }
        public float distanceScale { get { return _touchScaleMotion.distanceScale; } }
        public float curDistance { get { return _touchScaleMotion.curDistance; } }

        protected TouchRotateMotion _touchRotateMotion;
        public TouchStatus rotateStatus { get { return _touchRotateMotion.status; } }
        public float rotationY { get { return _touchRotateMotion.rotationY; } }

        public BaseTouchesMotion()
        {
            string[] screenArgs = global_params_helper.GetGlobalParam(GlobalParamId.touches_camera_screen_region).Split(',');
			float touchTopRange = Screen.height * float.Parse(screenArgs[0]);
            float touchRightRange = Screen.width * float.Parse(screenArgs[1]);
			float touchBottomRange = Screen.height * float.Parse(screenArgs[2]);
            float touchLeftRange = Screen.width * float.Parse(screenArgs[3]);

            float distanceDefaultScale = float.Parse(global_params_helper.GetGlobalParam(GlobalParamId.touches_camera_default_rate));
            OnInit(distanceDefaultScale, touchLeftRange, touchRightRange, touchTopRange, touchBottomRange);
        }

        virtual protected void OnInit(float distanceDefaultScale, float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            
        }

        public void OnStart(float targetDistance, float targetRotationX, float targetRotationY, bool keepTouchesScale)
        {
            _touchScaleMotion.Start(targetDistance, targetRotationX, keepTouchesScale);
            _touchRotateMotion.Start(targetRotationY);
        }

        public void OnUpdate()
        {
            _touchScaleMotion.OnUpdate();
            _touchRotateMotion.OnUpdate();
        }

        public void UpdateArgs(float touchDistance2Distance, float minDistance, float minRotationX, float touchDistanceToRotation)
        {
            _touchScaleMotion.UpdateArgs(touchDistance2Distance, minDistance, minRotationX);
            _touchRotateMotion.UpdateArgs(touchDistanceToRotation);
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchScaleMotion.UpdateRange(top, right, bottom, left);
            _touchRotateMotion.UpdateRange(top, right, bottom, left);
        }

        public float GetRotationXByDistance(float distance)
        {
            return _touchScaleMotion.GetRotationXByDistance(distance);
        }
    }
}
