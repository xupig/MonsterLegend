﻿using GameData;

namespace GameMain.GlobalManager
{
    public class FingerTouchesMotion : BaseTouchesMotion
    {
        protected override void OnInit(float distanceDefaultScale, float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.touches_camera_params).Split(',');
            float touchDistance2Distance = float.Parse(args[0]);
            float minDistance = float.Parse(args[1]);
            float minRotationX = float.Parse(args[2]);
            float touchDistance2Rotation = float.Parse(args[3]);
            float startRotateMinDistance = float.Parse(args[4]);

            _touchScaleMotion = new FingerScaleMotion(distanceDefaultScale, touchDistance2Distance, minDistance, minRotationX,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange);
            _touchRotateMotion = new FingerRotateMotion(touchDistance2Rotation,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange, startRotateMinDistance);
        }
    }
}
