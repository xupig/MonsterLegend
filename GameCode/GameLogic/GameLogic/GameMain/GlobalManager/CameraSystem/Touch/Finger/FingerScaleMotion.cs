﻿using UnityEngine;

namespace GameMain.GlobalManager
{
    public class FingerScaleMotion : TouchScaleMotion
    {
        private Touch _touch1;
        private Touch _touch2;
        private float _beginTouchDistance = 0f;
        private float _touchDistance2Distance = 0;

        public FingerScaleMotion(float distanceDefaultScale, float touchDistance2Distance, float minDistance, float minRotationX,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
            : base(distanceDefaultScale, minDistance, minRotationX,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange)
        {
            _touchDistance2Distance = touchDistance2Distance;
        }

        public override void OnUpdate()
        {
            if (CheckTouchCount())
            {
                if (_touch1.phase == TouchPhase.Moved || _touch2.phase == TouchPhase.Moved)
                {
                    if (_status == TouchStatus.IDLE)
                    {
                        if (CheckTouchesRange() && CheckFullUI())
                        {
                            Begin();
                        }
                    }
                    else
                    {
                        Touching();
                    }
                }
            }
            else
            {
                if (_status == TouchStatus.TOUCHING)
                {
                    End();
                }
            }
        }

        private void Begin()
        {
            _beginTouchDistance = Vector2.Distance(_touch1.position, _touch2.position);
            _distance = _curDistance;
            _rotationX = GetRotationXByDistance(_distance);
            _status = TouchStatus.TOUCHING;
        }

        private void Touching()
        {
            float touchDistance = Vector2.Distance(_touch1.position, _touch2.position);
            float distanceOffset = (touchDistance - _beginTouchDistance) * _touchDistance2Distance;
            _distance = _curDistance + distanceOffset;
            _distance = (_distance > _maxDistance) ? _maxDistance : _distance;
            _distance = (_distance < _minDistance) ? _minDistance : _distance;
            _distanceScale = (_distance - _minDistance) / (_maxDistance - _minDistance);
            _rotationX = GetRotationXByDistance(_distance);
        }

        private void End()
        {
            _status = TouchStatus.IDLE;
            _curDistance = _distance;
            WriteLocalCache();
        }

        private bool CheckTouchCount()
        {
            int count = Input.touchCount;
            if (count <= 1)
            {
                return false;
            }

            int fingerId = GetStickFingerId();
            Touch touch;
            int index = 0;
            bool result = false;
            for (int i = 0; i < count; ++i)
            {
                touch = Input.GetTouch(i);
                if (touch.fingerId != fingerId)
                {
                    if (index == 0)
                    {
                        _touch1 = touch;
                        index++;
                    }
                    else
                    {
                        _touch2 = touch;
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        private bool CheckTouchesRange()
        {
            return CheckTouchRange(_touch1) && CheckTouchRange(_touch2);
        }

        private bool CheckTouchRange(Touch touch)
        {
            return CheckRange(touch.position.x, touch.position.y);
        }

        public override void UpdateArgs(float touchDistance2Distance, float minDistance, float minRotationX)
        {
            _touchDistance2Distance = touchDistance2Distance;
            base.UpdateArgs(touchDistance2Distance, minDistance, minRotationX);
        }
    }
}
