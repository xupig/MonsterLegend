﻿using Common.Base;
using Common.Events;
using Common.ExtendTools;
using Game.Asset;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/19 14:26:51 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
	public class CameraManager
	{
		public static string MAIN_CAMERA_PREFAB = "Global/MainCamera.prefab";
		private bool _isInited = false;
		private Camera _cameraComponent;
		private Transform _curTarget; 
		private bool _isShow = true;
		private float _initRotationX;
		private float _initRotationY;
		private float _initDistance;
		private int _initFov;
		private Dictionary<CameraMotionType, BaseCameraMotion> _cameraMotionDict;
		private CameraAccordingMode _accordingMode = CameraAccordingMode.AccordingMotion;
        private float _oneVsOneDistanceDuration = 0f;
        private float _oneVsOnePositionDuration = 0f;
        private float _oneVsOneRotationDuration = 0f;
        private float _doubleTouchDuration = 0f;
		
		public MogoMainCamera mainCamera;
		
		private static CameraManager s_instance = null;
		public static CameraManager GetInstance()
		{
			if (s_instance == null)
			{
				s_instance = new CameraManager();
			}
			return s_instance;
		}
		
		private CameraManager()
		{
		}
		
		private void InitCameraGameObject()
		{
			if (_isInited) return;
			_isInited = true;
			if (GameObject.Find("MainCamera") == null)
			{
				string globalParams = global_params_helper.GetGlobalParam(76);
				
				_initDistance = float.Parse(globalParams.Split(',')[0]);
				_initRotationX = float.Parse(globalParams.Split(',')[1]);
				_initRotationY = float.Parse(globalParams.Split(',')[2]);
				_initFov = int.Parse(globalParams.Split(',')[3]);
				
				Game.Asset.ObjectPool.Instance.GetGameObject(MAIN_CAMERA_PREFAB, (obj) =>
				                                             {
					if (obj == null)
					{
						LoggerHelper.Error("CreateActor modelName:" + MAIN_CAMERA_PREFAB + " is null");
						return;
					}
					mainCamera = obj.AddComponent<MogoMainCamera>();
                    AddInteractionCompoment(obj);
                    //obj.AddComponent<KeyboardCamera>();
					Camera cam = obj.GetComponent<Camera>();
					if (cam != null) { cam.farClipPlane = 40f; }
					UnityEngine.Object.DontDestroyOnLoad(obj);
					OnMainCameraLoaded();
					mainCamera.gameObject.SetActive(_isShow);
				});
			}
		}

        private void AddInteractionCompoment(GameObject cameraGameObject)
        {
            if (platform_helper.InTouchPlatform())
            {
                cameraGameObject.AddComponent<FingerResponse>();
            }
            else
            {
                cameraGameObject.AddComponent<MouseResponse>();
            }
        }
		
		private void OnMainCameraLoaded()
		{
			InitCameraMotion();
            InitOneVsOneParams();
            InitDoubleTouchParams();
			if (_curTarget != null)
			{
				SetFollowingTarget(this._curTarget);
			}
		}
		
		private void InitCameraMotion()
		{
            CameraData data = new CameraData();
			_cameraMotionDict = new Dictionary<CameraMotionType, BaseCameraMotion>();
            _cameraMotionDict.Add(CameraMotionType.ROTATION_TARGET, new RotationTargetMotion(data));
            _cameraMotionDict.Add(CameraMotionType.ROTATION_POSITION, new RotationPositionMotion(data));
            _cameraMotionDict.Add(CameraMotionType.TARGET_POSITION, new TargetPositionMotion(data));
            _cameraMotionDict.Add(CameraMotionType.ONE_VS_ONE, new OneVsOneMotion(data));
			var enumerator = _cameraMotionDict.GetEnumerator();
			while (enumerator.MoveNext())
			{
				enumerator.Current.Value.camera = mainCamera.transform;
			}
		}

        private void InitOneVsOneParams()
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.one_vs_one_back_params).Split(',');
            _oneVsOnePositionDuration = float.Parse(args[0]);
            _oneVsOneRotationDuration = float.Parse(args[1]);
            _oneVsOneDistanceDuration = float.Parse(args[2]);
        }

        private void InitDoubleTouchParams()
        {
            _doubleTouchDuration = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.touches_camera_back_duration));
        }

        public void OnDoubleTouchBlankSpace()
        {
            if (platform_helper.InTouchPlatform() && PlayerTouchBattleModeManager.GetInstance().inTouchModeState)
            {
                return;
            }
            OnRecoverTouchesCamera();
        }

        public void OnRecoverTouchesCamera()
        {
            if (mainCamera.accordingNode == CameraAccordingMode.AccordingMotion && mainCamera.currentMotionType == CameraMotionType.ROTATION_TARGET)
            {
                RotationTargetMotion motion = _cameraMotionDict[mainCamera.currentMotionType] as RotationTargetMotion;
                if (!motion.IsAdjusting())
                {
                    ChangeToRotationTargetMotion(null, motion.originalRotation, _doubleTouchDuration, 0,
                                                 motion.originalDistance, _doubleTouchDuration);
                }
            }
        }
		
		//主相机是否显示
		public bool isShow { get { return _isShow; } }
		
		//获取当前主相机
		public Camera Camera
		{
			get
			{
                if (_cameraComponent == null && mainCamera != null)
				{
					_cameraComponent = mainCamera.gameObject.GetComponent<Camera>();
				}
				return _cameraComponent;
			}
		}
		
		//获取当前主相机的显示对象
		public Transform CameraTransform
		{
			get
			{
				return mainCamera.transform;
			}
		}
		
		public void SetFollowingTarget(Transform target)
		{
			InitCameraGameObject();
			this._curTarget = target;
			if (mainCamera != null)
			{
				mainCamera.target = target;
				ChangeCameraFov(_initFov);
				ChangeToRotationTargetMotion(_curTarget, new Vector3(_initRotationX, _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
			}
		}

        public void ShowMainCameraWithOutAI()
        {
            if (mainCamera != null)
            {
                Camera.enabled = true;
            }
            _isShow = true;
        }

		public void ShowMainCamera()
		{
            if (mainCamera != null)
            {
                Camera.enabled = true;
            }
            _isShow = true;
            EventDispatcher.TriggerEvent(CameraEvents.SHOW_MAIN_CAMERA);
		}
		
		public void HideMainCamera()
		{
			if (mainCamera != null)
			{
				Camera.enabled = false;
			}
			_isShow = false;
			EventDispatcher.TriggerEvent(CameraEvents.HIDE_MAIN_CAMERA);
		}
		
		public void ResetCameraInfo()
		{
            ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), new Vector3(_initRotationX, _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
			mainCamera.GetComponent<Camera>().fieldOfView = _initFov;
			mainCamera.ResetCurrShakeInfo();
		}

        public bool CurrentIsScaled()
        {
            return mainCamera.IsScaled();
        }
		
		public CameraInfoBackup GetCameraInfoBackup()
		{
			var backup = new CameraInfoBackup();
			backup.accordingMode = _accordingMode;
			if (_accordingMode == CameraAccordingMode.AccordingMotion)
			{
				backup.motionType = mainCamera.currentMotionType;
			}
            BaseCameraMotion currentMotion = _cameraMotionDict[mainCamera.currentMotionType];
            backup.position = currentMotion.data.targetPosition;
            backup.localEulerAngles = currentMotion.data.targetRotation;
            backup.distance = currentMotion.data.targetDistance;
            if (backup.motionType == CameraMotionType.ROTATION_TARGET)
            {
                backup.localEulerAngles.x = (currentMotion as RotationTargetMotion).originalRotation.x;
                backup.distance = (currentMotion as RotationTargetMotion).originalDistance;
            }
            backup.target = currentMotion.data.target;
            backup.rotationMinSpeed = currentMotion.data.rotationMinSpeed;
			return backup;
		}
		
		//相机震动
		public void ShakeCamera(int id, float length, float shakeCountodds = 1.0f)
		{
			var value = XMLManager.camera_anim[id];
			int currUsePriority = 0;
			if (mainCamera.CurrPlayShakeId > 0)
			{
				currUsePriority = XMLManager.camera_anim[mainCamera.CurrPlayShakeId].__priority;
			}
			
			mainCamera.SetCurrShakeInfo(value.__priority, id, length);
			if (value.__priority <= currUsePriority) return;
			mainCamera.ShakeCamera(id, value.__xSwing, value.__ySwing, value.__zSwing, value.__xRate, value.__yRate, value.__zRate, length, value.__attenuateOddsX, value.__attenuateOddsY, value.__attenuateOddsZ, value.__xDir, value.__yDir, value.__zDir, value.__xRateA, value.__yRateA, value.__zRateA, shakeCountodds);
		}
		
		//改变相机视角
		public void ChangeCameraFov(int cameraFov = 0)
		{
			if (cameraFov == 0)
			{
				cameraFov = _initFov;
			}
			mainCamera.GetComponent<Camera>().fieldOfView = cameraFov;
		}
		
		/// <summary>
		/// 切换为锁定角度，目标，距离模式
		/// </summary>
		/// <param name="target">目标，传null表示保持当前目标不变</param>
		/// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
		/// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
		/// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
		/// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
		/// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
		/// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
		public void ChangeToRotationTargetMotion(Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
		{
			StopCurrentCameraMotion();
			if (mainCamera == null) return;
			mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ROTATION_TARGET]);
            mainCamera.ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
		}
		
		/// <summary>
		/// 切换为锁定角度，位置模式
		/// </summary>
		/// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
		/// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
		/// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
		/// <param name="position">目标位置，-Vector3.one表示保持当前位置不变</param>
		/// <param name="positionDuration">位置缓动时间, 0表示瞬间完成</param>
		/// <param name="ignoreTimeScale">是否忽略TimeScale</param>
		public void ChangeToRotationPositionMotion(Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
		{
			StopCurrentCameraMotion();
			if (mainCamera == null) return;
			mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ROTATION_POSITION]);
			mainCamera.ChangeToRotationPositionMotion(localEulerAngle, rotationDuration, rotationAccelerateRate, position, positionDuration, ignoreTimeScale);
		}
		
		/// <summary>
		/// 切换为锁定目标，位置模式
		/// </summary>
		/// <param name="target">目标，传null表示保持当前目标不变</param>
		/// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
		/// <param name="rotationMinSpeed">锁定最小角度</param>
		/// <param name="position">目标位置，-Vector3.one表示保持当前位置不变</param>
		/// <param name="positionDuration">位置缓动时间, 0表示瞬间完成</param>
		/// <param name="ignoreTimeScale">是否忽略TimeScale</param>
		public void ChangeToTargetPositionMotion(Transform target, float rotationDuration, float rotationMinSpeed, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
		{
			StopCurrentCameraMotion();
			if (mainCamera == null) return;
			mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.TARGET_POSITION]);
			mainCamera.ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration, ignoreTimeScale);
		}

        private CameraInfoBackup _oneVsOneBackUp;
        public void ChangeToOneVsOneMotion(Transform self, Transform opponent)
        {
            if (_accordingMode == CameraAccordingMode.AccordingAnimation)
            {
                LoggerHelper.Info("Change to one vs one motion error, current in according animation mode.");
                return;
            }
			if (mainCamera == null) return;
			if (mainCamera.currentMotionType != CameraMotionType.ONE_VS_ONE)
			{
				_oneVsOneBackUp = GetCameraInfoBackup();
			}
			StopCurrentCameraMotion();
            Vector3 startRotation;
            BaseCameraMotion currentCameraMotion = _cameraMotionDict[mainCamera.currentMotionType];
            if (currentCameraMotion is OneVsOneMotion)
            {
                OneVsOneMotion motion = currentCameraMotion as OneVsOneMotion;
                startRotation = motion.rotation;
            }
            else
            {
                startRotation = mainCamera.transform.localEulerAngles;
            }
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ONE_VS_ONE]);
            mainCamera.ChangeToOneVsOneMotion(self, opponent, startRotation);
        }

        public void ChangeBackFromOneVsOneMotion()
        {
            if (_oneVsOneBackUp.accordingMode == CameraAccordingMode.AccordingAnimation)
            {
                return;
            }
            switch (_oneVsOneBackUp.motionType)
            {
                case CameraMotionType.ROTATION_TARGET:
                    ChangeToRotationTargetMotion(_oneVsOneBackUp.target,
                        _oneVsOneBackUp.localEulerAngles, _oneVsOneRotationDuration, 0,
                        _oneVsOneBackUp.distance, _oneVsOneDistanceDuration, true, true);
                    break;
                case CameraMotionType.ROTATION_POSITION:
                    ChangeToRotationPositionMotion(_oneVsOneBackUp.localEulerAngles, _oneVsOneRotationDuration, 0,
                        _oneVsOneBackUp.position, _oneVsOnePositionDuration);
                    break;
                case CameraMotionType.TARGET_POSITION:
                    ChangeToTargetPositionMotion(_oneVsOneBackUp.target, _oneVsOneRotationDuration, _oneVsOneBackUp.rotationMinSpeed,
                        _oneVsOneBackUp.position, _oneVsOnePositionDuration);
                    break;
                default:
                    break;
            }
        }

		/// <summary>
		/// 切换为动画模式
		/// </summary>
		/// <param name="path">动画资源路径</param>
		/// <param name="createdCallback">创建后的回调</param>
		public void ChangeToCameraAnimation(string path, Action<GameObject> createdCallback = null)
		{
			StopCurrentCameraMotion();
			ObjectPool.Instance.GetGameObject(path, (obj) =>
			{
				if (obj == null)
				{
					LoggerHelper.Error(string.Format("切换为相机动画模式失败，因为路径为{0}找不到资源", path));
					return;
				}
				
				_accordingMode = CameraAccordingMode.AccordingAnimation;
				if (createdCallback != null)
				{
					createdCallback(obj);
				}
				Transform cameraSlot = obj.transform.FindChildByName("camera_slot");
				mainCamera.transform.SetParent(cameraSlot, false);
				mainCamera.transform.localPosition = Vector3.zero;
				mainCamera.transform.localEulerAngles = Vector3.zero;
				mainCamera.accordingNode = _accordingMode;
			});
		}
		
		/// <summary>
		/// 停止当前的动画模式
		/// </summary>
		public void StopCurrentCameraAnimation()
		{
			_accordingMode = CameraAccordingMode.AccordingMotion;
			mainCamera.transform.SetParent(null, false);
			mainCamera.accordingNode = _accordingMode;
		}
		
		/// <summary>
		/// 停止当前的相机效果
		/// </summary>
		public void StopCurrentCameraMotion()
		{
			switch (_accordingMode)
			{
			case CameraAccordingMode.AccordingMotion:
				mainCamera.StopCurrentCameraMotion();
				break;
			case CameraAccordingMode.AccordingAnimation:
				StopCurrentCameraAnimation();
				break;
			default:
				break;
			}
		}
		
		/// <summary>
		/// 相机缓动方法(兼容旧接口)
		/// </summary>
		/// <param name="cameraType">缓动类型</param>
		/// <param name="rotationX">目标角度X(用于类型0,1)</param>
		/// <param name="rotationY">目标角度Y(用于类型0,1)</param>
		/// <param name="rotateTime">缓动时间(没用到)</param>
		/// <param name="distance">缓动与目标距离(用于类型0,1)</param>
		/// <param name="targetPostion">目标位置(用于类型4)</param>
		/// <param name="moveTime">缓动时间(用于类型4)</param>
		/// <param name="speed">缓动频率(用于类型0)</param>
		/// <param name="odds">缓动步长(用于类型0)</param>
		/// <param name="concuss">震动参数(用于类型2,3)</param>
		/// <param name="cameraFov">视野改变(用于类型0,1)</param>
		public void ChangeCamera(int cameraType, float rotationX, float rotationY, float rotateTime, float distance, Vector3 targetPostion, float moveTime, float speed, float odds, string concuss, int cameraFov = 0)
		{
			ChangeCameraFov(cameraFov);
			switch (cameraType)
			{
			case CameraConfig.CAMERA_TYPE_ROTATE:
				ChangeToRotationTargetMotion(null, new Vector3(rotationX, rotationY, 0), 2f, 0, distance, 2f, true, true);
				break;
			case CameraConfig.CAMERA_TYPE_ROTATE_DIRECTLY:
				ChangeToRotationTargetMotion(null, new Vector3(rotationX, rotationY, 0), 0f, 0, distance, 0f, true, true);
				break;
			case CameraConfig.CAMERA_TYPE_SHAKE_1:
			case CameraConfig.CAMERA_TYPE_SHAKE_2:
				string[] str = concuss.Split(',');
				int id = System.Convert.ToInt32(str[0]);
				float length = float.Parse(str[1]);
				ShakeCamera(id, length);
				break;
			case CameraConfig.CAMERA_TYPE_MOVE:
				ChangeToRotationPositionMotion(-1 * Vector3.one, moveTime, 0, targetPostion, moveTime);
				break;
			default:
				break;
			}
		}
		
		public void SetScreenCamera(int cameraType, Vector3 targetPostion, float moveTime)
		{
			ChangeCamera(cameraType, 0, 0, 0, 0, targetPostion, moveTime, 0, 0, "");
		}
		
		public void SetPlayerCamera(int cameraType, float rotationX, float rotationY, float distance, float speed, float odds)
		{
			ChangeCamera(cameraType, rotationX, rotationY, 0, distance, -1 * Vector3.one, 0, speed, odds, "");
		}
    }
}
