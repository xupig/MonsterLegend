﻿using System;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 21:13:18 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public class RotationPositionMotion : BaseCameraMotion
    {
        private bool _isAdjustingRotation = false;
        private bool _isAdjustingPosition = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _positionSpeed;

        private Vector3 _rotation;
        private Vector3 _position;

        public override bool IsAdjusting()
        {
            return _isAdjustingPosition || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.ROTATION_POSITION;
        }

        public RotationPositionMotion(CameraData data)
            : base(data)
        {
        }

        public override void Start()
        {
            _updateImmediately = false;
            _isAdjustingPosition = true;
            _isAdjustingRotation = true;
            _duration = 0;
            _rotation = camera.localEulerAngles;
            _position = camera.position;
            if (data.rotationDuration > 0)
            {
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.positionDuration > 0)
            {
                _positionSpeed = (data.targetPosition - _position) / data.positionDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            if (!IsAdjusting())
            {
                return;
            }
            _duration += deltaTime;
            if (_isAdjustingRotation)
            {
                AdjustRotation();
            }
            if (_isAdjustingPosition)
            {
                AdjustPosition();
            }
            UpdateCamera();
        }

        private void AdjustRotation()
        {
            if (data.rotationDuration == 0 || _duration >= data.rotationDuration || _rotation == data.targetRotation)
            {
                _rotation = data.targetRotation;
                _isAdjustingRotation = false;
                return;
            }
            UpdateRotation();
        }

        private void UpdateRotation()
        {
            if (_rotationAccelerateDuration == 0)
            {
                _rotation += _rotationSpeed * deltaTime;
                return;
            }

            if (data.rotationDuration - _duration <= _rotationAccelerateDuration)
            {
                float t = (data.rotationDuration - _duration);
                Vector3 offset = 0.5f * _rotationAcceleration * t * t;
                _rotation = data.targetRotation - offset;
            }
            else if (_duration <= _rotationAccelerateDuration)
            {
                Vector3 offset = 0.5f * _rotationAcceleration * _duration * _duration;
                _rotation = offset;
            }
            else
            {
                _rotation += _rotationSpeed * deltaTime;
            }
        }

        private void AdjustPosition()
        {
            if (data.positionDuration == 0 || _duration >= data.positionDuration || _position == data.targetPosition)
            {
                _position = data.targetPosition;
                _isAdjustingPosition = false;
                return;
            }
            _position += _positionSpeed * deltaTime;
        }

        protected override void UpdateCamera()
        {
            camera.position = _position;
            camera.localEulerAngles = _rotation;
        }

        public override void Stop()
        {
            _isAdjustingPosition = _isAdjustingRotation = false;
        }
    }
}
