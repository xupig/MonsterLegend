﻿using System;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 21:20:15 
 * function: 锁定跟随目标，锁定位置
 * targetPosition为锁定的位置，positionDuration为从当前位置缓动到目标位置的时间，缓动到该位置后相机不再移动。
 * target为锁定的目标，角度每帧缓动距离的 rotationDuration为角度缓动时间，角度/时间为每帧缓动速度，若该速度小于最小缓动速度(rotationMinSpeed)
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public class TargetPositionMotion : BaseCameraMotion
    {
        private Vector3 _positionSpeed;

        private Vector3 _position;

        private bool _isAdjustingPosition = false;

        public override bool IsAdjusting()
        {
            return _isAdjustingPosition;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.TARGET_POSITION;
        }

        public TargetPositionMotion(CameraData data)
            : base(data)
        {
        }

        public override void Start()
        {
            _updateImmediately = false;
            _isAdjustingPosition = true;
            _duration = 0;
            _position = camera.position;
            if (data.positionDuration > 0)
            {
                _positionSpeed = (data.targetPosition - _position) / data.positionDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            if (IsAdjusting())
            {
                _duration += deltaTime;
                AdjustPosition();
            }
            UpdateCamera();
        }

        private void AdjustPosition()
        {
            if (data.positionDuration == 0 || _duration >= data.positionDuration || _position == data.targetPosition)
            {
                _position = data.targetPosition;
                _isAdjustingPosition = false;
                return;
            }
            _position += _positionSpeed * deltaTime;
        }

        protected override void UpdateCamera()
        {
            if (IsAdjusting())
            {
                camera.position = _position;
            }

            Vector3 originalLocalEulerAngles = camera.localEulerAngles;
            camera.LookAt(data.target);
            Vector3 targetLocalEulerAngles = camera.localEulerAngles;
            float angle = Vector3.Angle(originalLocalEulerAngles, targetLocalEulerAngles);
            float lerp = 1f;
            if (data.rotationDuration > 0 && angle > 0 && angle < 0.05f)
            {
                var offset = angle / data.rotationDuration;
                if (offset > data.rotationMinSpeed)
                {
                    lerp = (angle - offset) / angle;
                }
            }
            camera.localEulerAngles = Vector3.Lerp(originalLocalEulerAngles, targetLocalEulerAngles, lerp);
        }

        public override void Stop()
        {
            _isAdjustingPosition = false;
        }
    }
}
