﻿using UnityEngine;

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 21:11:41 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public abstract class BaseCameraMotion
    {
        protected float _duration = 0;
        protected bool _updateImmediately = false;
        public bool ignoreTimeScale;
        public Transform camera;
        public CameraData data;

        protected float deltaTime
        {
            get
            {
                if (ignoreTimeScale) return RealTime.deltaTime;
                return Time.deltaTime;
            }
        }

        public BaseCameraMotion(CameraData data)
        {
            this.data = data;
        }

        public abstract bool IsAdjusting();

        public abstract CameraMotionType GetCameraType();

        public abstract void Start();

        public abstract void OnUpdate();

        protected abstract void UpdateCamera();

        public abstract void Stop();
    }
}
