﻿using GameData;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class OneVsOneParams
    {
        public QuadraticFunctionParams distanceParams;
        public QuadraticFunctionParams rotationXParams;
        public QuadraticFunctionParams thetaParams;
        public float disSelfToCenterMax;
        public float threshold;
        public float thetaConstant;
        public OneVsOneAdjustParams nearAdjustParams;
        public OneVsOneAdjustParams farAdjustParams;
        public OneVsOneParams(List<float> args)
        {
            int i = 0;
            distanceParams = new QuadraticFunctionParams(args[i++], args[i++], args[i++]);
            rotationXParams = new QuadraticFunctionParams(args[i++], args[i++], args[i++]);
            thetaParams = new QuadraticFunctionParams(args[i++], args[i++], args[i++]);
            disSelfToCenterMax = args[i++];
            threshold = args[i++];
            thetaConstant = args[i++];
            farAdjustParams = new OneVsOneAdjustParams(args[i++], args[i++], args[i++], args[i++],
                args[i++], args[i++], args[i++], args[i++]);
            nearAdjustParams = new OneVsOneAdjustParams(args[i++], args[i++], args[i++], args[i++],
                args[i++], args[i++], args[i++], args[i++]);
        }

        public float GetDistance(float value)
        {
            return distanceParams.GetFx(value);
        }

        public float GetRotationX(float value)
        {
            return rotationXParams.GetFx(value);
        }

        public float GetTheta(float value)
        {
            return (value < threshold) ? thetaConstant : thetaParams.GetFx(value);
        }

        public OneVsOneAdjustParams GetAdjustParams(float value)
        {
            return (value > threshold) ? farAdjustParams : nearAdjustParams;
        }
    }

    public class QuadraticFunctionParams
    {
        public float secondOrder;
        public float firstOrder;
        public float constant;
        public QuadraticFunctionParams(float arg0, float arg1, float arg2)
        {
            this.secondOrder = arg0;
            this.firstOrder = arg1;
            this.constant = arg2;
        }

        public float GetFx(float x)
        {
            return secondOrder * Mathf.Pow(x, 2) + firstOrder * x + constant;
        }
    }

    public class OneVsOneAdjustParams
    {
        public float centerPointAdjustTime;
        public float centerPointMinSpeed;
        public float distanceAdjustTime;
        public float distanceMinSpeed;
        public float rotationXAdjustTime;
        public float rotationXMinSpeed;
        public float rotationYAdjustTime;
        public float rotationYMinSpeed;
        public OneVsOneAdjustParams(float arg0, float arg1, float arg2, float arg3,
            float arg4, float arg5, float arg6, float arg7)
        {
            centerPointAdjustTime = arg0;
            centerPointMinSpeed = arg1;
            distanceAdjustTime = arg2;
            distanceMinSpeed = arg3;
            rotationXAdjustTime = arg4;
            rotationXMinSpeed = arg5;
            rotationYAdjustTime = arg6;
            rotationYMinSpeed = arg7;
        }
    }

    public class OneVsOneMotion : BaseCameraMotion
    {
        private static OneVsOneParams _params;

        public Transform self;
        public Transform opponent;
        public Vector3 startFocusPoint;
        public Vector3 startRotation;

        private Vector3 _rotation;
        private float _distance;
        private float _disSelfToCenter;
        private float _theta;

        public float theta { get { return _theta; } }
        public float selfToCenter { get { return _disSelfToCenter; } }
        public Vector3 rotation { get { return _rotation; } }
        public float distance { get { return _distance; } }
        public Vector3 focusPoint { get { return data.focusPoint; } }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.ONE_VS_ONE;
        }

        public OneVsOneMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            _rotation = startRotation;
            data.focusPoint = startFocusPoint;
            _distance = Vector3.Distance(data.focusPoint, camera.position);
        }

        public override void OnUpdate()
        {
            Vector3 centerPoint = (self.position + opponent.position) * 0.5f;
            float disSelfToCenter = Vector3.Distance(self.position, centerPoint);
            if (disSelfToCenter > _params.disSelfToCenterMax)
            {
                disSelfToCenter = _params.disSelfToCenterMax;
                centerPoint = self.position + (opponent.position - self.position).normalized * disSelfToCenter;
            }

            float distance = _params.GetDistance(disSelfToCenter);
            float rotationX = _params.GetRotationX(disSelfToCenter);
            float theta = _params.GetTheta(disSelfToCenter);
            _disSelfToCenter = disSelfToCenter;
            _theta = theta;
            OneVsOneAdjustParams adjustParams = _params.GetAdjustParams(disSelfToCenter);
            UpdateDistance(distance, adjustParams);
            UpdateRotationX(rotationX, adjustParams);
            UpdateRotationY(centerPoint, theta, adjustParams);
            UpdateFocusPoint(centerPoint, adjustParams);
            UpdateCamera();
        }

        private void UpdateDistance(float distance, OneVsOneAdjustParams adjustParams)
        {
            float lerp = Mathf.Lerp(_distance, distance, deltaTime / adjustParams.distanceAdjustTime);
            float lerpOffset = lerp - _distance;
            float minOffset = adjustParams.distanceMinSpeed * deltaTime;
            minOffset = (lerpOffset < 0) ? (-minOffset) : minOffset;
            float offset = Mathf.Abs(lerpOffset) > Mathf.Abs(minOffset) ? lerpOffset : minOffset;
            _distance = Mathf.Abs(_distance - distance) < Mathf.Abs(offset) ? distance : _distance + offset;
        }

        private void UpdateRotationX(float rotationX, OneVsOneAdjustParams adjustParams)
        {
            float lerp = Mathf.Lerp(_rotation.x, rotationX, deltaTime / adjustParams.distanceAdjustTime);
            float lerpOffset = lerp - _rotation.x;
            float minOffset = adjustParams.distanceMinSpeed * deltaTime;
            minOffset = (lerpOffset < 0) ? (-minOffset) : minOffset;
            float offset = Mathf.Abs(lerpOffset) > Mathf.Abs(minOffset) ? lerpOffset : minOffset;
            _rotation.x = Mathf.Abs(_rotation.x - rotationX) < Mathf.Abs(offset) ? rotationX : _rotation.x + offset;
        }

        private void UpdateRotationY(Vector3 centerPoint, float theta, OneVsOneAdjustParams adjustParams)
        {
            Vector3 selfDirection = self.position - centerPoint;
            Vector3 cameraDirection = camera.position - centerPoint;

            Vector2 selfDirection2D = new Vector2(selfDirection.x, selfDirection.z).normalized;
            Vector2 cameraDirection2D = new Vector2(cameraDirection.x, cameraDirection.z).normalized;
            float angle = Vector2.Angle(selfDirection2D, cameraDirection2D);
            if (angle <= theta)
            {
                return;
            }

            float lerpOffset = Mathf.Lerp(0, angle - theta, deltaTime / adjustParams.rotationYAdjustTime);
            float minOffset = deltaTime * adjustParams.rotationYMinSpeed;
            float offset = Mathf.Max(lerpOffset, minOffset);

            float angle0 = Vector2.Angle(Vector2.up, cameraDirection2D);
            Vector3 original = self.position;
            self.position = new Vector3(selfDirection2D.x, 0, selfDirection2D.y);
            self.RotateAround(Vector3.zero, Vector3.up, (cameraDirection2D.x < 0) ? angle0 : -angle0);
            float rotateDirection = (self.position.x < 0) ? -1 : 1;
            self.position = original;

            _rotation.y = _rotation.y + offset * rotateDirection;
        }

        private void UpdateFocusPoint(Vector3 centerPoint, OneVsOneAdjustParams adjustParams)
        {
            Vector3 lerp = Vector3.Lerp(data.focusPoint, centerPoint, deltaTime / adjustParams.centerPointAdjustTime);

            float minOffset = adjustParams.centerPointMinSpeed * deltaTime;
            Vector3 lerpOffset = lerp - data.focusPoint;
            if ((centerPoint - data.focusPoint).magnitude < minOffset)
            {
                data.focusPoint = centerPoint;
            }
            else
            {
                if (lerpOffset.magnitude < minOffset)
                {
                    data.focusPoint = data.focusPoint + lerpOffset.normalized * minOffset;
                }
                else
                {
                    data.focusPoint = lerp;
                }
            }
        }

        protected override void UpdateCamera()
        {
            camera.position = data.focusPoint - Vector3.forward * _distance;
            camera.RotateAround(data.focusPoint, new Vector3(1, 0, 0), _rotation.x);
            camera.RotateAround(data.focusPoint, new Vector3(0, 1, 0), _rotation.y);
            camera.LookAt(data.focusPoint);
        }

        public override bool IsAdjusting()
        {
            return false;
        }

        public override void Stop()
        {
        }

        private void InitParams()
        {
            List<float> oneVsOneParamsList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.one_vs_one_params).Split(','));
            _params = new OneVsOneParams(oneVsOneParamsList);
        }
    }
}
