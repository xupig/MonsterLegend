﻿using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 21:12:08 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public class RotationTargetMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private float _focusPointAdjustTime;
        private float _focusPointMinSpeed;
        private float _focusPointOffset;

        private BaseTouchesMotion _touchesMotion;

        public bool IsScaled()
        {
            return _touchesMotion != null && _touchesMotion.distanceScale < 1;
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.ROTATION_TARGET;
        }

        public RotationTargetMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            if (_touchesMotion == null)
            {
                if (platform_helper.InTouchPlatform())
                {
                    _touchesMotion = new FingerTouchesMotion();
                }
                else
                {
                    _touchesMotion = new MouseTouchesMotion();
                }
            }
            bool canTouch = CanTouch();
            if (canTouch)
            {
                _touchesMotion.OnStart(data.targetDistance, data.targetRotation.x, data.targetRotation.y, keepTouchesScale);
            }
            _originalDistance = data.targetDistance;
            _originalRotation = data.targetRotation;
            if (keepTouchesScale && canTouch)
            {
                data.targetDistance = _touchesMotion.curDistance;
                data.targetRotation.x = _touchesMotion.GetRotationXByDistance(data.targetDistance);
            }
            _updateImmediately = false;
            _isAdjustingDistance = true;
            _isAdjustingRotation = true;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            _distance = Vector3.Distance(data.target.position, camera.position);
            if (data.rotationDuration > 0)
            {
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            if (IsAdjusting())
            {
                _duration += deltaTime;
                if (_isAdjustingRotation)
                {
                    AdjustRotation();
                }
                if (_isAdjustingDistance)
                {
                    AdjustDistance();
                }
            }
            else if (CanTouch())
            {
                _touchesMotion.OnUpdate();
                if (_touchesMotion.scaleStatus == TouchStatus.TOUCHING)
                {
                    _distance = _touchesMotion.distance;
                    _rotation.x = _touchesMotion.rotationX;
                }
                if (_touchesMotion.rotateStatus == TouchStatus.TOUCHING)
                {
                    _rotation.y = _touchesMotion.rotationY;
                    data.targetRotation.y = _rotation.y;
                }
            }
            UpdateFocusPoint();
            UpdateCamera();
        }

        protected override void UpdateCamera()
        {
            camera.position = data.focusPoint - Vector3.forward * _distance;
            camera.RotateAround(data.focusPoint, new Vector3(1, 0, 0), _rotation.x);
            camera.RotateAround(data.focusPoint, new Vector3(0, 1, 0), _rotation.y);
            camera.LookAt(data.focusPoint);
        }

        private void UpdateFocusPoint()
        {
            InitParams();
            Vector3 targetFocusPoint = data.target.position;
            if ((targetFocusPoint - data.focusPoint).magnitude >= _focusPointOffset)
            {
                data.focusPoint = targetFocusPoint;
                return;
            }
            Vector3 lerp = Vector3.Lerp(data.focusPoint, targetFocusPoint, deltaTime / _focusPointAdjustTime);

            float minOffset = _focusPointMinSpeed * deltaTime;
            Vector3 lerpOffset = lerp - data.focusPoint;
            if ((targetFocusPoint - data.focusPoint).magnitude < minOffset)
            {
                data.focusPoint = targetFocusPoint;
            }
            else
            {
                if (lerpOffset.magnitude < minOffset)
                {
                    data.focusPoint = data.focusPoint + lerpOffset.normalized * minOffset;
                }
                else
                {
                    data.focusPoint = lerp;
                }
            }
        }

        private void AdjustRotation()
        {
            if (data.rotationDuration == 0 || _duration >= data.rotationDuration || _rotation == data.targetRotation)
            {
                _rotation = data.targetRotation;
                _isAdjustingRotation = false;
                return;
            }
            UpdateRotation();
        }

        private void UpdateRotation()
        {
            if (_rotationAccelerateDuration == 0)
            {
                _rotation += _rotationSpeed * deltaTime;
                return;
            }

            if (data.rotationDuration - _duration <= _rotationAccelerateDuration)
            {
                float t = (data.rotationDuration - _duration);
                Vector3 offset = 0.5f * _rotationAcceleration * t * t;
                _rotation = data.targetRotation - offset;
            }
            else if (_duration <= _rotationAccelerateDuration)
            {
                Vector3 offset = 0.5f * _rotationAcceleration * _duration * _duration;
                _rotation = _startRotation + offset;
            }
            else
            {
                _rotation += _rotationSpeed * deltaTime;
            }
        }

        private void AdjustDistance()
        {
            if (data.distanceDuration == 0 || _duration >= data.distanceDuration || _distance == data.targetDistance)
            {
                _distance = data.targetDistance;
                _isAdjustingDistance = false;
                return;
            }
            _distance += _distanceSpeed * deltaTime;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        public void UpdateArgs(float touchDistance2Distance, float minDistance, float minRotationX, float touchDistanceToRotation)
        {
            _touchesMotion.UpdateArgs(touchDistance2Distance, minDistance, minRotationX, touchDistanceToRotation);
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchesMotion.UpdateRange(top, right, bottom, left);
        }

        private bool CanTouch()
        {
            if (DramaManager.GetInstance().IsInViewingMode())
            {
                return false;
            }
            if (!inst_type_operate_helper.CanTouchCamera((ChapterType)GameSceneManager.GetInstance().curMapType))
            {
                return false;
            }
            return true;
        }

        private void InitParams()
        {
            List<float> argList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.rotate_camera_params).Split(','));
            _focusPointAdjustTime = argList[0];
            _focusPointMinSpeed = argList[1];
            _focusPointOffset = argList[2];
        }
    }
}
