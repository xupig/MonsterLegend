﻿using UnityEngine;

namespace GameMain.GlobalManager
{
    public class CameraData
    {
        public Transform target;
        public Vector3 focusPoint;

        public Vector3 targetRotation;
        public float rotationDuration;
        public float rotationAccelerateRate;
        public float rotationMinSpeed;

        public float targetDistance;
        public float distanceDuration;

        public Vector3 targetPosition;
        public float positionDuration;
    }
}
