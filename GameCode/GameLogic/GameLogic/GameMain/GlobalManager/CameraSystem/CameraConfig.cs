﻿
/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 20:32:30 
 * function: 摄像机常量
 * *******************************************************/

using UnityEngine;
namespace GameMain.GlobalManager
{
    public enum CameraMotionType : byte
    {
        ROTATION_TARGET = 1,        //锁定角度，目标，距离
        ROTATION_POSITION,          //锁定角度，位置
        TARGET_POSITION,            //锁定目标，位置
        ONE_VS_ONE                  //1V1越肩视角
    }

    public enum CameraAccordingMode : byte
    {
        AccordingMotion = 0,        //缓动模式
        AccordingAnimation          //动画模式
    }

    public class CameraConfig
    {
        public const int CAMERA_TYPE_ROTATE = 0;
        public const int CAMERA_TYPE_ROTATE_DIRECTLY = 1;
        public const int CAMERA_TYPE_SHAKE_1 = 2;
        public const int CAMERA_TYPE_SHAKE_2 = 3;
        public const int CAMERA_TYPE_MOVE = 4;
    }

    public struct CameraInfoBackup
    {
        public CameraAccordingMode accordingMode;
        public CameraMotionType motionType;
        public Vector3 position;
        public Vector3 localEulerAngles;
        public float distance;
        public float distanceScale;
        public Transform target;
        public float rotationMinSpeed;
    }
}
