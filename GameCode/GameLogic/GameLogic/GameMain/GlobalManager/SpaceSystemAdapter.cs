﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class SpaceSystemAdapter
    {

        private static Dictionary<string, Type> entityTypeRuntimeBehaviourMap = new Dictionary<string, Type>();

        public static Dictionary<string, Type> GetEntityTypeRuntimeBehaviourMap()
        {
            if (entityTypeRuntimeBehaviourMap.Count == 0)
            {
                entityTypeRuntimeBehaviourMap.Add(EntityType.NPC_ACTION, typeof(BHEntityNPCAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SPAWN_MONSTER_ACTION, typeof(BHEntitySpawnMonsterAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.COLLECT_MONSTER_ACTION, typeof(BHEntityCollectMonsterAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.REGION_TRIGGER_ACTION, typeof(BHEntityRegionTriggerAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SPAWN_ITEM_ACTION, typeof(BHEntitySpawnItemAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.COLLECT_ITEM_ACTION, typeof(BHEntityCollectItemAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.COUNTER_CHECK_ACTION, typeof(BHEntityCountercheckAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.TIMER_ACTION, typeof(BHEntityTimerAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.TIMER_ALTER_ACTION, typeof(BHEntityTimerAlterAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.TERMINATE_ACTION, typeof(BHEntityTerminateAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.COND_TRIGGER_ACTION, typeof(BHEntityCondTriggerAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PLAY_EFFECT_ACTION, typeof(BHEntityPlayEffectAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.TELEPORT_SPACE_ACTION, typeof(BHEntityTeleportSpaceAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PASS_WIN_ACTION, typeof(BHEntityPassWinAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PASS_LOSE_ACTION, typeof(BHEntityPassLoseAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CAMP_ACTION, typeof(BHEntityCampAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CAMERA_CHANGE_ACTION, typeof(BHEntityCameraChangeAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SET_BEGIN_STATE_ACTION, typeof(BHEntitySetBeginStateAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SEND_BUFF_ACTION, typeof(BHEntitySendBuffAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.JUDGE_ACTION, typeof(BHEntityJudgeAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.RANDOM_ACTION, typeof(BHEntityRandomAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CTRL_EFFECT_ACTION, typeof(BHEntityCtrlEffectAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SYSTEM_NOTICE_ACTION, typeof(BHEntitySystemNoticeAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CHANGE_AI_ACTION, typeof(BHEntityChangAiAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.AI_MOVE_ACTION, typeof(BHEntityAiMoveAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PLAY_CG_ACTION, typeof(BHEntityPlayCGAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.COUNT_ACTION, typeof(BHEntityCountAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CHOICE_ACTION, typeof(BHEntityChoiceAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.DISPATCH_ACTION, typeof(BHEntityDispatchAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CREATE_ELEVATOR_ACTION, typeof(BHEntityCreateElevatorAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.TIME_SCALE_ACTION, typeof(BHEntityTimeScaleAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.ENTER_INSTANCE_ACTION, typeof(BHEnterInstanceAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.ROTATE_CAMERA_ACTION, typeof(BHEntityRotateCameraAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.MOVE_CAMERA_ACTION, typeof(BHEntityMoveCameraAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CAMERA_LOOK_AT_ACTION, typeof(BHEntityCameraLookAtAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CHANGE_CAMERA_FOV_ACTION, typeof(BHEntityChangeCameraFovAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.SHAKE_CAMERA_ACTION, typeof(BHEntityShakeCameraAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PLAY_BULLET_VISUAL_FX_ACTION, typeof(BHEntityPlayBulletVisualFXAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.CREATE_UI_ACTION, typeof(BHEntityCreateUIAction));
                entityTypeRuntimeBehaviourMap.Add(EntityType.PLAY_TALK_ACTION, typeof(BHEntityPlayTalkAction));
            }
            return entityTypeRuntimeBehaviourMap;
        }
    }
}
