﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using GameData;
using GameLoader.IO;
using System.Collections;
using GameLoader.Config;
using GameMain.GlobalManager.SubSystem;

namespace GameMain.GlobalManager
{
    public class ReloadXmlCallbacks
    {
        static Dictionary<string, Action> _curXMLNameCallbackMap;
        static Dictionary<string, Action> curXMLNameCallbackMap
        {
            get {
                if (_curXMLNameCallbackMap == null)
                {
                    _curXMLNameCallbackMap = new Dictionary<string, Action>() 
                    {
                        {"global_params.xml", OnReloadGlobalParams},
                        {"market_data.xml", OnReloadMarketData},
                        {"item_reward.xml", OnReloadItemReward},
                        {"monster.xml", OnReloadMonster},
                        {"trademarket.xml", OnTradeMarket},
                        {"trade_items.xml", OnTradeItems},
                        {"active_reward.xml", OnActiveReward},
                        {"daily_task.xml", OnDailyTask},
                    };
                }
                return _curXMLNameCallbackMap; }
        }

        public static void OnReload(Dictionary<string, string> hotInfo)
        {
            HashSet<Action> actions = new HashSet<Action>();
            foreach (var pair in hotInfo)
            {
                string fileName = pair.Key;
                if (curXMLNameCallbackMap.ContainsKey(fileName) && !actions.Contains(curXMLNameCallbackMap[fileName]))
                {
                    actions.Add(curXMLNameCallbackMap[fileName]);
                }
            }
            foreach (var action in actions)
            {
                action();
            }
        }

        static void OnReloadGlobalParams()
        {
            global_params_helper.OnReloadData();
        }

        static void OnReloadMarketData()
        {
            PlayerDataManager.Instance.MallDataManager.OnReloadData();
        }

        static void OnReloadItemReward()
        {
            item_reward_helper.OnReloadData();
        }

        static void OnReloadMonster()
        {
            monster_helper.OnReloadData();
        }

        static void OnTradeMarket()
        {
            trademarket_helper.OnReloadData();
        }
        
        static void OnTradeItems()
        {
            PlayerDataManager.Instance.AuctionData.OnReloadData();
        }

        static void OnActiveReward()
        {
            RewardManager.Instance.OnReloadDailyActiveData();
        }

        static void OnDailyTask()
        {
            RewardManager.Instance.OnReloadDailyTaskData();
        }
    }
}
