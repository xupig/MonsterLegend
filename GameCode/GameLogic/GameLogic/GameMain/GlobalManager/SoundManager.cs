﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;

namespace GameMain.GlobalManager
{

    public class SoundManager
    {
        private static SoundManager s_instance = null;
        public static SoundManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new SoundManager();
            }
            return s_instance;
        }

        public void Play(GameObject gameObject, string name, float volume, bool loop = false)
        {
            var soundHander = gameObject.GetComponent<SoundHander>();
            if (soundHander == null) soundHander = gameObject.AddComponent<SoundHander>();
            Game.Asset.ObjectPool.Instance.GetAudioClips(new string[]{name}, (objs) =>
                {
                    if(objs[0] != null)
                    {
                        soundHander.Play(objs[0], volume, loop);
                    }
                });
        }

        public void Play(AudioSource source,GameObject gameObject, string name, float volume, bool loop = false)
        {
            Game.Asset.ObjectPool.Instance.GetAudioClips(new string[] { name }, (objs) =>
            {
                if (objs[0] != null)
                {
                    source.clip = objs[0];
                    source.volume = AllSoundManger.Instance.isTempLoseVolume == true ? 0f : volume;
                    source.loop = loop;
                    if (source.enabled)source.Play();
                }
            });
        }

    }

    [RequireComponent(typeof(AudioSource))]
    public class SoundHander : MonoBehaviour
    {
        private AudioSource m_source;
        void Awake()
        {
            m_source = this.GetComponent<AudioSource>();
        }

        public void Play(AudioClip audioClip, float volume, bool loop = false)
        {
            //Debug.Log("SoundHander Play");
            m_source.clip = audioClip;
            m_source.volume = volume;
            m_source.loop = loop;
            m_source.Play();
        }
        

    }
}
