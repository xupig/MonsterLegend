using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using Common.Base;
using GameMain.GlobalManager.SubSystem;


namespace GameMain.GlobalManager
{
    public class NPCManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    class NPCTaskStateData
    {
        public int taskType;
        public int taskState;
    }

    public class NPCManager
    {
        private static NPCManager s_instance = null;
        public static NPCManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new NPCManager();
            }
            return s_instance;
        }

        private Dictionary<int, uint> _npcs = new Dictionary<int, uint>();
        private List<int> _activityNpcs = new List<int>();
        private Dictionary<int, NPCTaskStateData> _npcTaskState = new Dictionary<int, NPCTaskStateData>();
        private NPCManager()
        {
            
        }

        public void OnPlayerEnterWorld()
        {
            EventDispatcher.AddEventListener<int, int, int>(TaskEvent.NPC_TASK_STATE_CHANGE, OnNPCTaskStatusChange);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.AddEventListener<int, bool>(TaskEvent.TALK_TO_NPC, OnTalkToNpc);
            EventDispatcher.AddEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnTriggerNPC);
            MogoWorld.RegisterUpdate<NPCManagerUpdateDelegate>("NPCManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("NPCManager.Update", Update);
            EventDispatcher.RemoveEventListener<int, bool>(TaskEvent.TALK_TO_NPC, OnTalkToNpc);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.RemoveEventListener<int, int, int>(TaskEvent.NPC_TASK_STATE_CHANGE, OnNPCTaskStatusChange);
            EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnTriggerNPC);

        }

        public void OnPlayerInitPosition()
        {
            foreach (var pair in _npcs)
            {
                var npcEntity = MogoWorld.GetEntity(pair.Value) as EntityNPC;
                if (npcEntity == null) continue;
                npcEntity.ResetPlayerEnterOnly();
            }
        }

        int _spaceTime = 0;
        void Update()
        {
            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = 30;
            CheckActivityNPC();
        }

        public void ResetNPCTaskState(EntityNPC npc)
        {
            if (npc == null) return;
            var data = GetNPCTaskSate(npc.npc_id);
            npc.npcStateManager.SetTaskState(data.taskType, data.taskState);
        }

        public EntityNPC FindNPCByNPCID(int npcID)
        {
            EntityNPC result = null;
            if (_npcs.ContainsKey(npcID)){
                result = MogoWorld.GetEntity(_npcs[npcID]) as EntityNPC;
            }
            return result;
        }

        NPCTaskStateData GetNPCTaskSate(int npcID)
        {
            if (!_npcTaskState.ContainsKey(npcID))
            {
                _npcTaskState.Add(npcID, new NPCTaskStateData());
            }
            return _npcTaskState[npcID];
        }

        void OnNPCTaskStatusChange(int npcID, int taskType, int taskState)
        {
            var data = GetNPCTaskSate(npcID);
            data.taskType = taskType;
            data.taskState = taskState;
            var npc = FindNPCByNPCID(npcID);
            ResetNPCTaskState(npc);
        }

        void CreateNPCEntity(int npcId, int activityId = 0)
        {
            try
            {
                Entity npcEntity = MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_NPC, (entity) =>
                {
                    var npc = entity as GameMain.Entities.EntityNPC;
                    npc.npc_id = npcId;
                    npc.activityID = activityId;
                    var location = npc_helper.GetNpcLocation(npcId);
                    entity.Teleport(new Vector3(location[0] * 0.01f, location[1] * 0.01f, location[2] * 0.01f));
                    entity.SetRotation(Quaternion.Euler(0, location[3] * 0.01f, 0));
                });
                _npcs.Add(npcId, npcEntity.id);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("CreateNPC Error：npcID：" + npcId + ":"+ ex.Message);
            }
        }

        void DestroyNPCEntity(int npcId)
        {
            if (_npcs.ContainsKey(npcId))
            {
                MogoEngine.MogoWorld.DestroyEntity(_npcs[npcId]);
                _npcs.Remove(npcId);
            }
        }

        void OnEnterMap(int mapID)
        {
            _npcs.Clear();
            _activityNpcs.Clear();
            var npcList = map_helper.GetNPCListByMapID(mapID);
            if (npcList == null) {
                return; 
            }
            for (int i = 0; i < npcList.Count; i++)
            {
                int npcid = npcList[i];
                if (npc_helper.IsActivityNpc(npcid))
                {
                    _activityNpcs.Add(npcid);
                    continue;
                }
                CreateNPCEntity(npcid);
            }
        }

        void OnTalkToNpc(int npcID, bool checkTask)
        {
            if (_npcs.ContainsKey(npcID))
            {
                var npcEntity = MogoWorld.GetEntity(_npcs[npcID]) as EntityNPC;
                if (npcEntity == null) return;
                HandleActivity(npcEntity.activityID);
            }
        }

        private void HandleActivity(int activityID)
        {
            switch (activityID)
            {
                // 当前的NPC是活动配置表中的某个NPC
                case 1:
                    if (PlayerDataManager.Instance.TeamData.TeammateDic.Count <= 0)
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.DemonGatePop);
                    }
                    break;
            }
        }

        private void OnTriggerNPC(uint id)
        {
            var entity = MogoWorld.GetEntity(id);
            if (entity == null || !(entity is EntityNPC))
            {
                return;
            }
            var npc = entity as EntityNPC;
            if (_npcs.ContainsKey(npc.npc_id))
            {
                HandleVoice(npc.npc_id);
            }
        }

        private void HandleVoice(int npcID)
        {
            int soundID = npc_helper.GetRandomNPCVoice(npcID);
            if (soundID != -1)
            {
                SoundInfoManager.Instance.PlayClickVoice(soundID);
            }
        }

        void CheckActivityNPC()
        {
            for (int i = 0; i < _activityNpcs.Count; i++)
            {
                var npcID = _activityNpcs[i];
                var activityID = activity_helper.NpcContainsActivity(npcID);
                if (activityID > 0 && !_npcs.ContainsKey(npcID))
                {
                    CreateNPCEntity(npcID, activityID);
                }
                else if (activityID == 0 && _npcs.ContainsKey(npcID))
                {
                    DestroyNPCEntity(npcID);
                }
            }
        }


    }

}