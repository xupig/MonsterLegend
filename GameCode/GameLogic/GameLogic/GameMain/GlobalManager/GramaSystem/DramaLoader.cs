﻿using Common.Base;
using Common.Events;
using Game.Asset;
using GameLoader.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class DramaLoader
    {
        private bool _isLoading = false;
        private List<string> _pathList;
        private Action _callback;
        private int _spaceTime = 0;
        private const int DEFAULT_SPACE_TIME = 5; //加载间隔时间

        public DramaLoader()
        {
            _pathList = new List<string>();
        }

        public void Load(string name, string mark, Action callback)
        {
            if (_isLoading)
            {
                callback();
                return;
            }

            _pathList.Clear();
            DramaResource.GetDramaMarkPreloadResource(_pathList, name, mark);
            if (_pathList.Count == 0)
            {
                callback();
                return;
            }

            _isLoading = true;
            _callback = callback;
            //TODO: 开始播放云
            UIManager.Instance.ShowPanel(PanelIdEnum.CloudPanel);
            LoggerHelper.Info("show cloud panel");
            ObjectPool.Instance.PreloadAsset(_pathList.ToArray(), End);
        }

        private void End()
        {
            _callback();
            _callback = null;
            _isLoading = false;
            //TODO: 停止播放云
            LoggerHelper.Info("hide cloud panel");
            if (BaseModule.IsPanelShowing(PanelIdEnum.CloudPanel) == false)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.CloudPanel);
            }
            else
            {
                EventDispatcher.TriggerEvent(CloudEvents.CloseCloudPanel);
            }
        }
    }
}
