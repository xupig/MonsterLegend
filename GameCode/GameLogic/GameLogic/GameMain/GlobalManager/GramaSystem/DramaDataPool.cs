﻿using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DramaData
    {
        private const string DATA_DRAMAS = "data/dramas/";
        //当前指令队列
        private List<Dictionary<string, string>> _actions;
        public List<Dictionary<string, string>> actions
        {
            get { return _actions; }
        }

        private Dictionary<string, int> _marks;
        public Dictionary<string, int> marks
        {
            get { return _marks; }
        }

        public DramaData(string dramaName)
        {
            _actions = new List<Dictionary<string, string>>();
            _marks = new Dictionary<string, int>();
            Init(dramaName);
        }

        private void Init(string dramaName)
        {
           
            SecurityElement xmlDoc = LoadData(dramaName);
            //Debug.Log("dramaName:" + dramaName + "," + xmlDoc);
            Parse(xmlDoc);
        }

        private SecurityElement LoadData(string dramaName)
        {
            string strContent = string.Empty;
            switch (Application.platform)
            { 
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                case RuntimePlatform.WindowsWebPlayer:
                    strContent = LoadDramaDataAtAndorid("发布模式", dramaName);
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
                        strContent = LoadDramaDataAtAndorid("开发模式下--发布模式", dramaName);
                    }
                    else
                    {
                        string path = string.Concat(SystemConfig.ResPath, DATA_DRAMAS, dramaName, ConstString.XML_SUFFIX);
                        LoggerHelper.Info(string.Format("[开发模式下][LoadDramaData] 加载文件:{0},{1}", path, dramaName));
                        strContent = path.LoadFile();
                    }
                    break;
            }
            //Debug.Log("strContent:" + strContent);
            return XMLParser.LoadXML(strContent);
        }

        /// <summary>
        /// 发布模式下--加载DramaData
        /// </summary>
        /// <param name="logTitle"></param>
        /// <param name="dramaName"></param>
        /// <returns></returns>
        private string LoadDramaDataAtAndorid(string logTitle, string dramaName)
        {
            string fileName = string.Concat(DATA_DRAMAS, dramaName, ConstString.XML_SUFFIX);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            LoggerHelper.Info(string.Format("[{0}][LoadDramaData] 文件:{1},{2}", logTitle, fileName, isExistFile ? "存在" : "找不到"));
            return isExistFile ? FileAccessManager.LoadText(fileName) : null;
        }

        public void Parse(SecurityElement xmlDoc)
        {
            _actions.Clear();
            if (xmlDoc == null) return;
            ArrayList dataList = xmlDoc.Children;
            int pos = 0;
            foreach (SecurityElement data in dataList)
            {
                if (data == null || data.Children==null || data.Children.Count == 0)
                {
                    return;
                }
                Dictionary<string, string> action = new Dictionary<string, string>();
                ArrayList subDataList = data.Children;
                foreach (SecurityElement subData in subDataList)
                {
                    if(action.ContainsKey(subData.Tag))
                    {
                        Debug.LogError("重复的tag:" + subData.Tag);
                    }
                    action.Add(subData.Tag, subData.Text);
                }
                _actions.Add(action);
                string actionName = action[DramaConfig.TAG_ACTION];
                if (DramaConfig.IsMark(actionName))
                {
                    if (_marks.ContainsKey(actionName))
                    {
                        Debug.LogError("重复的mark:" + actionName);
                        _marks.Remove(actionName);
                    }
                    _marks.Add(actionName, pos);
                }
                //Debug.Log("actionName:" + actionName);
                pos++;
            }
            //Debug.Log("Parse4");
        }

        public int GetMark(string mark)
        {
            return _marks.ContainsKey(mark) ? _marks[mark] : -1;
        }
    }

    public class DramaDataPool
    {
        //DramaData缓冲池
        private static Dictionary<string, DramaData> s_dramaDataDict = new Dictionary<string, DramaData>();
        public static DramaData GetDramaData(string dramaName)
        {
            DramaData dramaData;
            if (s_dramaDataDict.ContainsKey(dramaName))
            {
                dramaData = s_dramaDataDict[dramaName];
            }
            else
            {
                dramaData = new DramaData(dramaName);
                s_dramaDataDict.Add(dramaName, dramaData);
            }
            return dramaData;
        }

        public static void ClearPool()
        {
            s_dramaDataDict.Clear();
        }

    }
}
