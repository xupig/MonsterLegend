﻿using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DramaUtils
    {
        static private Dictionary<uint, uint> _entityIDEntityDramaIDMap = new Dictionary<uint, uint>(); //<DramaID, entityID>
        static public void BindEntityIDAndEntityDramaID(uint entityID, uint entityDramaID)
        {
            _entityIDEntityDramaIDMap[entityDramaID] = entityID;
        }

        static public void DestroyEntities()
        {
            var enumerator = _entityIDEntityDramaIDMap.GetEnumerator();
            while (enumerator.MoveNext())
            {
                MogoWorld.DestroyEntity(enumerator.Current.Value);
            }
            _entityIDEntityDramaIDMap.Clear();
        }

        static public uint FindEntityIDByEntityDramaID(string entityDramaID)
        {
            if (entityDramaID.Equals("Player"))
            {
                return PlayerAvatar.Player.id;
            }
            if (entityDramaID.Equals("Pet"))
            {
                return PlayerPetManager.GetInstance().curPetEntityID;
            }
            uint dramaIDUInt = uint.Parse(entityDramaID);
            uint entityID = 0;
            if (_entityIDEntityDramaIDMap.ContainsKey(dramaIDUInt))
            {
                entityID = _entityIDEntityDramaIDMap[dramaIDUInt];
            }
            return entityID;
        }

        static public EntityCreature GetCreatureByEntityDramaID(string entityDramaID)
        {
            var entity = GetEntityByEntityDramaID(entityDramaID);
            if (entity != null && entity is EntityCreature)
            {
                return entity as EntityCreature;
            }
            return null;
        }

        static public Entity GetEntityByEntityDramaID(string entityDramaID)
        {
            var entityID = FindEntityIDByEntityDramaID(entityDramaID);
            if (entityID == 0)
            {
                return null;
            }
            return MogoWorld.GetEntity(entityID);
        }

        static public Vector3 StringToVector3(string value)
        {
            var splited = value.Split(',');
            return new Vector3(float.Parse(splited[0]), float.Parse(splited[1]), float.Parse(splited[2]));
        }

        static public Dictionary<string, string> StringToAction(string actionStr)
        {
            Dictionary<string, string> action = new Dictionary<string,string>();
            var splited = actionStr.Split(':');
            action.Add(DramaConfig.TAG_ACTION, splited[0]);
            if (splited.Length > 1){
                var args = splited[1].Split('|');
                for (int i = 0; i < args.Length; i++)
                {
                    action.Add(DramaConfig.TAG_ARG + (i+1), args[i]);
                }
            }
            return action;
        }

        static public List<Dictionary<string, string>> StringToActions(string actionsStr)
        {
            List<Dictionary<string, string>> actions = new List<Dictionary<string, string>>();
            var splited = actionsStr.Split(';');
            if (string.IsNullOrEmpty(splited[0]))
            {
                return actions;
            }
            for (int i = 0; i < splited.Length; i++)
            {
                if (splited[i] == "null")
                {
                    continue;
                }
                actions.Add(StringToAction(splited[i]));
                if (splited[i] == "End")
                {
                    break;
                }
            }
            return actions;
        }

    }
}
