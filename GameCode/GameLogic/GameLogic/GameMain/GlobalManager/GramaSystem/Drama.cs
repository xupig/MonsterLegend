﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.GlobalManager
{
    public class DramaWaitingEvent
    { 
        public string eventName = string.Empty;
        public string eventValue = string.Empty;
        public float waitingTime = 0;

        public DramaWaitingEvent(string setEventName, string setEventValue, float setWaitingTime)
        {    
            eventName = setEventName;
            eventValue = setEventValue;
            waitingTime = setWaitingTime;
        }
    }

    public enum DramaState : byte
    {
        IDLE = 0,
        LOADING,
        RUNNING
    }

    public class Drama
    {
        //單幀運行劇情命令的最大時間值
        private const float MAX_EXECUTE_ACTION_TIME = 0.010f;

        //剧情名称，对应数据表名称
        private string _name;
        public string name { get { return _name; } }
        //剧情Mark
        private string _markName;
        public string markName { get { return _markName; } }

        //当前运行状态
        private DramaState _state = DramaState.IDLE;
        public bool running { get { return _state == DramaState.RUNNING; } }
        //处于结束状态
        private bool _onEnd = false;

        //用于观察的当前运行action名称和位置
        public string runningActionName;
        public int runningPos;

        //当前指令队列
        private List<Dictionary<string, string>> _actions;
        //当前Mark位置
        private Dictionary<string, int> _marks;
        //当前指令进行位置
        private int _curActionPos = 0;
        //睡眠结束时间戳
        private float _sleepEndTime = 0;
        //等待事件，用于记录剧情执行期间需要玩家进行操作后才能继续进行剧情的事件数据对象。
        private DramaWaitingEvent _waitingEvent = null;
        //剧情时间
        private float _pastTime = 0;

        private GameObject _cameraAnimationObj = null;

        private DramaLoader _loader;

        private string _dramaKey;
        public string dramaKey { get { return _dramaKey; } }
        private int _id;
        public int id { get { return _id; } }

        private bool _isShowingSkip = false;
        public bool isShowingSkip
        {
            get
            {
                return _isShowingSkip;
            }
            set
            {
                _isShowingSkip = value;
            }
        }

        private bool _isShowingBlackEdge = false;
        public bool isShowingBlackEdge
        {
            get
            {
                return _isShowingBlackEdge;
            }
            set
            {
                _isShowingBlackEdge = value;
            }
        }

        public Drama(int id, string dramaName)
        {
            _id = id;
            _name = dramaName;
            var dramaData = DramaDataPool.GetDramaData(dramaName);
            _actions = dramaData.actions;
            _marks = dramaData.marks;
            _state = DramaState.IDLE;
        }

        public void Start(DramaLoader loader, string markName = "")
        {
            if (_state != DramaState.IDLE) return;
            Reset();
            if (markName == null) markName = "";
            _markName = markName;
            _dramaKey = _name + markName;
            if (!markName.Equals(""))
            {
                SetCurActionPos(markName);
            }
            else
            {
                _curActionPos = 0;
            }
            _loader = loader;
            _state = DramaState.RUNNING;
            DramaDriver.Instance.onUpdate += OnUpdate;
            OnUpdate();
        }

        private int GetNextEndPos()
        {
            int curEndPos = 0;
            for (int i = _curActionPos + 1; i < _actions.Count; ++i)
            {
                string actionName = _actions[i][DramaConfig.TAG_ACTION];
                if (actionName.Equals("End"))
                {
                    curEndPos = i;
                    break;
                }
            }
            if (curEndPos == 0)
            {
                LoggerHelper.Error(string.Format("当前剧情配置错误，没有对应的End指令。剧情key为: {0}", _dramaKey));
            }
            return curEndPos;
        }

        private void Reset()
        {
            _onEnd = false;
            _waitingEvent = null;
            _sleepEndTime = 0;
            _pastTime = 0;
        }

        public void Stop()
        {
            if (!running) return;
            DramaDriver.Instance.onUpdate -= OnUpdate;
            _state = DramaState.IDLE;
            DramaManager.GetInstance().DramaOnEnd(this);
            //PlayerDataManager.Instance.TaskData.CheckAcceptableTask();
            _loader = null;
        }

        void OnUpdate()
        {
            if (DramaManager.GetInstance().isBlockingUpdate)
            {
                return;
            }
            _pastTime += Time.deltaTime;
            //var beginTime = Time.realtimeSinceStartup;
            while (_curActionPos < _actions.Count)
            {
                if (!running) break;
                if (CheckWaitingEvent()) break;
                if (CheckSleepTime()) break;
                runningPos = _curActionPos;
                runningActionName = _actions[_curActionPos][DramaConfig.TAG_ACTION];
                ExecuteAction(_actions[_curActionPos]);
                _curActionPos++;
                if (runningActionName != "Judge" && !DramaConfig.IsMark(runningActionName))
                {
                    break;
                }
                //if ((Time.realtimeSinceStartup - beginTime) > MAX_EXECUTE_ACTION_TIME) break;
            }
            if (_curActionPos == _actions.Count)
            {
                Stop();
            }
        }

        private void HideCGDialog()
        {
            EventDispatcher.TriggerEvent(CGEvent.HIDE_DIALOG);
        }

        bool CheckWaitingEvent()
        {
            if (_waitingEvent == null) return false;
            if (!CheckTimeOut(_waitingEvent.waitingTime)) return true; //存在事件等待
            //事件等待超时，取消等待
            _waitingEvent = null;
            return false;
        }

        bool CheckSleepTime()
        {
            return !CheckTimeOut(_sleepEndTime);
        }

        bool CheckTimeOut(float targetTime)
        {
            return _pastTime >= targetTime;
        }

        void ExecuteAction(Dictionary<string, string> action)
        {
            string actionName = action[DramaConfig.TAG_ACTION];
            //Debug.LogError("ExecuteAction dramaKey = " + _dramaKey + ", name = " + actionName + ", pos = " + _curActionPos);
            if (DramaConfig.IsMark(actionName))
            {
                if (_onEnd) Stop();
                return;
            }
            try
            {
                var actionMethod = this.GetType().GetMethod(actionName);
                if (actionMethod != null)
                {
                    actionMethod.Invoke(this, new object[] { action });
                }
                else
                {
                    LoggerHelper.Error("ExecuteAction Error in: " + this._name + ":" + actionName + " pos:" + _curActionPos );
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("ExecuteAction Error in: " + this._name + ":" + actionName + " pos:" + _curActionPos + " ex:" + ex);
            }
        }

        void ExecuteActionString(string actionStr)
        {
            var actions = DramaUtils.StringToActions(actionStr);
            for (int i = 0; i < actions.Count; i++)
            {
                ExecuteAction(actions[i]);
            }
        }

        bool ExecuteConditions(string conditions)
        {
            bool result = true;
            var splited = conditions.Split(';');
            if (string.IsNullOrEmpty(splited[0]))
            {
                return true;
            }
            for (int i = 0; i < splited.Length; i++)
            {
                string condition = splited[i];
                var conditionSP = condition.Split(':');
                string conditionName = conditionSP[0];
                bool isTrue = true;
                if(conditionName.IndexOf("!")!=-1)
                {
                    conditionName = conditionName.Replace("!", string.Empty);
                    isTrue = false;
                }

                string conditionValue = "";
                if (conditionSP.Length > 1)
                {
                    conditionValue = conditionSP[1];
                }
                result = DramaJudgment.ExecuteCondition(conditionName, conditionValue);
                if(isTrue == false)
                {
                    result = !result;
                }
                //Debug.Log("result:" + result);
                if (!result) break;
            }
            return result;
        }

        public void AddWaitEvent(string eventName, string eventValue)
        {
            float overtime = float.MaxValue * 0.001f + _pastTime;
            _waitingEvent = new DramaWaitingEvent(eventName, eventValue, overtime);
        }

        public void TriggerWaitEvent(string eventName, string eventValue)
        {
            if (_waitingEvent != null &&
                _waitingEvent.eventName == eventName &&
                _waitingEvent.eventValue == eventValue)
            {
                _waitingEvent = null;
                OnUpdate();
            }
            else
            {
                //當事件涉及到場景切換時，終止所有非等待場景事件的劇情
                if (eventName == DramaTriggerType.LeaveMap &&
				    (_waitingEvent == null || _waitingEvent.eventName != DramaTriggerType.EnterMap) &&
                    !DramaManager.GetInstance().isBlockingUpdate)
                {
                    Stop();
                }
            }
        }

        //************************************以下为各个行为命令实现方法*****************************************
        
        
        public void CreateEntity(Dictionary<string, string> action)
        {
            string entityType = action[DramaConfig.TAG_ARG1];
            uint entityDramaID = uint.Parse(action[DramaConfig.TAG_ARG2]);
            uint typeID = uint.Parse(action[DramaConfig.TAG_ARG3]);
            Vector3 position = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG4]);
            float rotationY = float.Parse(action[DramaConfig.TAG_ARG5]);
            bool needToPlayBornAnimation = Convert.ToBoolean(int.Parse(action[DramaConfig.TAG_ARG6]));
            var newEntity = MogoWorld.CreateEntity(entityType, (entity) =>
            {
                entity.SetPosition(position);
                Quaternion rotation = Quaternion.Euler(0, rotationY, 0);
                entity.SetRotation(rotation);
                if (entity is EntityCreature)
                {
                    var creature = entity as EntityCreature;
                    creature.camp_pve_type = creature.camp_pvp_type = 1;
                }
                if (entity is EntityMonsterBase)
                {
                    var monster = entity as EntityMonsterBase;
                    monster.monster_id = typeID;
                    if (!needToPlayBornAnimation)
                    {
                        monster.is_not_play_spawn_anim = 1;
                    }
                    monster.InitDramaEntity();
                }
                else if (entity is EntityNPC)
                {
                    var npc = entity as EntityNPC;
                    npc.npc_id = (int)typeID;
                    npc.InitDramaEntity();
                }
                else if (entity is EntityPet)
                {
                    var pet = entity as EntityPet;
                    pet.pet_id = (byte)typeID;
                    pet.isClientPet = true;
                    pet.InitDramaEntity();
                }
            });
            DramaUtils.BindEntityIDAndEntityDramaID(newEntity.id, entityDramaID);
        }

        public void DestroyEntities(Dictionary<string, string> action)
        {
            DramaUtils.DestroyEntities();
        }

        public void TeleportEntity(Dictionary<string, string> action)
        {
            Vector3 position = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG2]);
            Entity entity = DramaUtils.GetEntityByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            if (entity != null)
            {
                entity.Teleport(position);
            }
        }

        public void DestroyEntity(Dictionary<string, string> action)
        {
            uint entityID = DramaUtils.FindEntityIDByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            if (entityID != 0) { MogoWorld.DestroyEntity(entityID); }
        }

        public void SetAttr(Dictionary<string, string> action)
        {
            string attrName = action[DramaConfig.TAG_ARG2];
            string attrValue = action[DramaConfig.TAG_ARG3];
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            if (creature != null)
            {
                var propertyInfo = creature.GetType().GetProperty(attrName);
                if (propertyInfo == null) return;
                var type = propertyInfo.PropertyType;
                if (type == DramaConfig.INT) { propertyInfo.SetValue(creature, int.Parse(attrValue), null); }
                if (type == DramaConfig.FLOAT) { propertyInfo.SetValue(creature, float.Parse(attrValue), null); }
                else if (type == DramaConfig.STRING) { propertyInfo.SetValue(creature, attrValue, null); }
            }
        }

        public void RotateEntity(Dictionary<string, string> action)
        {
            EntityCreature entity = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            Vector3 localEulerAngles = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG2]);
            if (entity != null)
            {
                entity.actor.TurnTo(localEulerAngles);
            }
        }

        public void Sleep(Dictionary<string, string> action)
        {
            float sleepTime = int.Parse(action[DramaConfig.TAG_ARG1]) * 0.001f;
            _sleepEndTime = sleepTime + _pastTime;
        }

        public void WaitingEvent(Dictionary<string, string> action)
        {
           
            string eventName = action[DramaConfig.TAG_ARG1];
            string eventValue = action[DramaConfig.TAG_ARG2];
            float overtime = 0;
            if(action.ContainsKey(DramaConfig.TAG_ARG3))
            {
                overtime = float.Parse(action[DramaConfig.TAG_ARG3]) * 0.001f + _pastTime;
            }
            else
            {
                overtime = float.MaxValue * 0.001f + _pastTime;
            }
            _waitingEvent = new DramaWaitingEvent(eventName, eventValue, overtime);
        }


        public void Goto(Dictionary<string, string> action)
        {
            string markName = action[DramaConfig.TAG_ARG1];
            SetCurActionPos(markName);
        }

        private void SetCurActionPos(string markName)
        {
            if (this._marks.ContainsKey(markName))
            {
                this._curActionPos = this._marks[markName];
            }
            else {
                LoggerHelper.Error("DramaError markName:[" + markName + "] doesn't exsit in " + this._name);
            }
        }

        public void Judge(Dictionary<string, string> action)
        {
            string conditions = action[DramaConfig.TAG_ARG1];
            string action1 = action[DramaConfig.TAG_ARG2];
            string action2 = action[DramaConfig.TAG_ARG3];
            if (ExecuteConditions(conditions))
            {
                ExecuteActionString(action1);
            }
            else 
            {
                ExecuteActionString(action2);
            }
        }

        public void ExAction(Dictionary<string, string> action)
        {
            string exActionName = action[DramaConfig.TAG_ARG1];
            string exActionArg = action[DramaConfig.TAG_ARG2];
            DramaExAction.ExecuteExAction(this, exActionName, exActionArg);
        }

        public void MoveTo(Dictionary<string, string> action)
        {
            Vector3 position = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG2]);
            EntityCreature entity = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            float moveSpeed = float.Parse(action[DramaConfig.TAG_ARG3]);
            if (moveSpeed == -1)
            {
                moveSpeed = entity.moveManager.curMoveSpeed;
            }
            if (entity != null)
            {
                entity.moveManager.MoveByLine(position, moveSpeed);
            }
        }

        public void MoveToAndForward(Dictionary<string, string> action)
        {
            Vector3 position = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG2]);
            EntityCreature entity = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            float moveSpeed = float.Parse(action[DramaConfig.TAG_ARG3]);
            int forward = int.Parse(action[DramaConfig.TAG_ARG4]);
            if (moveSpeed == -1)
            {
                moveSpeed = entity.moveManager.curMoveSpeed;
            }
            if (entity != null)
            {
                entity.moveManager.MoveByLine(position, moveSpeed, (ent) =>
                {
                    entity.face = (byte)(forward / 2);
                });
            }
        }

        public void ShowPanel(Dictionary<string, string> action)
        {
            string panelName = action[DramaConfig.TAG_ARG1];
            bool isShow = int.Parse(action[DramaConfig.TAG_ARG2]) == 1;
            PanelIdEnum panelId = (PanelIdEnum)Enum.Parse(typeof(PanelIdEnum), panelName);
            if (isShow)
            {
                UIManager.Instance.ShowPanel(panelId);
            }
            else
            {
                UIManager.Instance.ClosePanel(panelId);
            }
        }

        public void ShakeCamera(Dictionary<string, string> action)
        {
            int shakeID = int.Parse(action[DramaConfig.TAG_ARG1]);
            float duration = int.Parse(action[DramaConfig.TAG_ARG2]) * 0.001f;
            CameraManager.GetInstance().ShakeCamera(shakeID, duration);
        }

        public void RotateCamera(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            string slotName = action[DramaConfig.TAG_ARG2];
            string rotationString = action[DramaConfig.TAG_ARG3];
            float rotationDuration = float.Parse(action[DramaConfig.TAG_ARG4]);
            float distance = float.Parse(action[DramaConfig.TAG_ARG5]);
            float distanceDuration = float.Parse(action[DramaConfig.TAG_ARG6]);
            Transform target = GetTransform(creature, slotName);
            Vector3 rotation = GetVector3(rotationString);
            float rotationAccelerateRate = 0;
            if (!rotationString.Equals("0"))
            {
                string[] rotationStrList = rotationString.Split(',');
                if (rotationStrList.Length >= 4) rotationAccelerateRate = float.Parse(rotationStrList[3]);
            }
            CameraManager.GetInstance().ChangeToRotationTargetMotion(target, rotation, rotationDuration, rotationAccelerateRate, distance, distanceDuration);
        }

        public void MoveCamera(Dictionary<string, string> action)
        {
            string rotationString = action[DramaConfig.TAG_ARG1];
            float rotationDuration = float.Parse(action[DramaConfig.TAG_ARG2]);
            string positionString = action[DramaConfig.TAG_ARG3];
            float positionDuration = float.Parse(action[DramaConfig.TAG_ARG4]);
            Vector3 rotation = GetVector3(rotationString);
            float rotationAccelerateRate = 0;
            if (!rotationString.Equals("0"))
            {
                string[] rotationStrList = rotationString.Split(',');
                if (rotationStrList.Length >= 4) rotationAccelerateRate = float.Parse(rotationStrList[3]);
            }
            Vector3 position = GetVector3(positionString);
            CameraManager.GetInstance().ChangeToRotationPositionMotion(rotation, rotationDuration, rotationAccelerateRate, position, positionDuration);
        }

        public void CameraLookAt(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            string slotName = action[DramaConfig.TAG_ARG2];
            float rotationDuration = float.Parse(action[DramaConfig.TAG_ARG3]);
            float rotationMinSpeed = float.Parse(action[DramaConfig.TAG_ARG4]);
            string positionString = action[DramaConfig.TAG_ARG5];
            float positionDuration = float.Parse(action[DramaConfig.TAG_ARG6]);
            Transform target = GetTransform(creature, slotName);
            Vector3 position = GetVector3(positionString);
            CameraManager.GetInstance().ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration);
        }

        public void ChangeCameraFov(Dictionary<string, string> action)
        {
            CameraManager.GetInstance().ChangeCameraFov(int.Parse(action[DramaConfig.TAG_ARG1]));
        }

        public void PlayCameraAnimation(Dictionary<string, string> action)
        {
            string animationPath = action[DramaConfig.TAG_ARG1];
            CameraManager.GetInstance().ChangeToCameraAnimation(animationPath, (obj) =>
            {
                DestroyCameraAnimationObj();
                _cameraAnimationObj = obj;
                AnimationEvent bhAirVehiclePath = obj.GetComponent<AnimationEvent>();
                if (bhAirVehiclePath != null)
                {
                    bhAirVehiclePath.onEvent = (animationEvent) =>
                    {
                        if (animationEvent == "over")
                        {
                            CameraManager.GetInstance().StopCurrentCameraMotion();
                            DestroyCameraAnimationObj();
                        }
                    };
                }
            });
        }

        private void DestroyCameraAnimationObj()
        {
            if (_cameraAnimationObj != null)
            {
                GameObject.Destroy(_cameraAnimationObj);
                _cameraAnimationObj = null;
            }
        }

        public void RecoverCamera(Dictionary<string, string> action)
        {
            CameraManager.GetInstance().ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), _localEulerAngles, 0, 0, _distance, 0, true, true);
            DestroyCameraAnimationObj();
        }

        public void PreloadAssets(Dictionary<string, string> action)
        {
            _state = DramaState.LOADING;
            _loader.Load(_name, markName, OnDramaLoaded);
        }
        
        private void OnDramaLoaded()
        {
            _state = DramaState.RUNNING;
        }

        public void PlayVisualFX(Dictionary<string, string> action)
        {
            string prefabPath = string.Concat("Assets/Resources/", action[DramaConfig.TAG_ARG1]);
            Vector3 position = DramaUtils.StringToVector3(action[DramaConfig.TAG_ARG2]);
			float duration = float.Parse(action[DramaConfig.TAG_ARG3]);
            ACTVisualFXManager.GetInstance().PlayACTVisualFX(prefabPath, position, duration * 0.001f);
        }

        public void PlayUIFX(Dictionary<string, string> action)
        {
            string prefabPath = action[DramaConfig.TAG_ARG1];
            string param = string.Concat(prefabPath, ",", action[DramaConfig.TAG_ARG2], ",", action[DramaConfig.TAG_ARG3]);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGParticle, param);
        }

        public void PlaySound(Dictionary<string, string> action)
        {
            string soundIdListStr = action[DramaConfig.TAG_ARG1];
            bool isLoop = Convert.ToBoolean(action[DramaConfig.TAG_ARG2]);
            if (string.IsNullOrEmpty(soundIdListStr))
            {
                return;
            }
            string[] soundList = soundIdListStr.Split(',');
            for (int i = 0; i < soundList.Length; ++i)
            {
                SoundInfoManager.Instance.PlaySound(int.Parse(soundList[i]), isLoop);
            }
        }

        public void PlayMusic(Dictionary<string, string> action)
        {
            string musicIdListStr = action[DramaConfig.TAG_ARG1];
            bool isLoop = Convert.ToBoolean(action[DramaConfig.TAG_ARG2]);
            if (string.IsNullOrEmpty(musicIdListStr))
            {
                return;
            }
            string[] musicList = musicIdListStr.Split(',');
            for (int i = 0; i < musicList.Length; ++i)
            {
                BgMusicManager.Instance.PlayAreaTriggerMusic(int.Parse(musicList[i]), isLoop);
            }
        }

        public void PlayTalk(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            int speechType = int.Parse(action[DramaConfig.TAG_ARG2]);
            bool showSpeechValue = Convert.ToBoolean(speechType & 1);
            bool playSound = Convert.ToBoolean(speechType & 2);
            List<int> speechValueList = data_parse_helper.ParseListInt(action[DramaConfig.TAG_ARG3].Split(','));
            int speechValue = speechValueList[MogoMathUtils.Random(0, speechValueList.Count)];
            creature.SpeakInSpeechValue(speechValue, false, playSound);
            if (showSpeechValue)
            {
                EventDispatcher.TriggerEvent<uint, int>(CGEvent.SHOW_CG_SPEECH, creature.id, speechValue);
            }
        }

        public void ShowDialog(Dictionary<string, string> action)
        {
            string entityType = action[DramaConfig.TAG_ARG1];
            string content = action[DramaConfig.TAG_ARG2];
            float duration = float.Parse(action[DramaConfig.TAG_ARG3]);
            bool isWaitingEvent = Convert.ToBoolean(int.Parse(action[DramaConfig.TAG_ARG4]));

            //LoggerHelper.Info(string.Format("Drama:ShowDialog, entityType = {0}, content = {1}, duration = {2}, dramaKey = {3}.", entityType, content, duration, _dramaKey));
            CGDialogData data = new CGDialogData(_dramaKey, entityType, content, duration, isWaitingEvent);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, data);
            if (isWaitingEvent)
            {
                _waitingEvent = new DramaWaitingEvent(DramaConfig.SKIP_EVENT_NAME, DramaConfig.SKIP_EVENT_VALUE, duration + _pastTime);
            }
        }

        public void CloseDialog(Dictionary<string, string> action)
        {
            //LoggerHelper.Info(string.Format("Drama:CloseDialog, dramaKey = {0}.", _dramaKey));
            UIManager.Instance.ClosePanel(PanelIdEnum.CGDialog);
        }

        public void ShowSubtitles(Dictionary<string, string> action)
        {
            int content = int.Parse(action[DramaConfig.TAG_ARG1]);
            int contentEng = int.Parse(action[DramaConfig.TAG_ARG2]);
            float duration = float.Parse(action[DramaConfig.TAG_ARG3]);
            CGDialogShowSubtitles data = new CGDialogShowSubtitles(_dramaKey, content, contentEng, duration);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, data);
        }

        public void PlaySkill(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            if (creature != null)
            {
                int skillID = int.Parse(action[DramaConfig.TAG_ARG2]);
                creature.skillManager.PlaySkill(skillID);
            }
        }

        public void PlayAction(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            int actionID = int.Parse(action[DramaConfig.TAG_ARG2]);
            if (creature != null)
            {
                creature.actor.animationController.PlayAction(actionID);
            }
        }

        public void AddBuff(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            bool needToSendToSvr = Convert.ToBoolean(int.Parse(action[DramaConfig.TAG_ARG4]));
            if (creature != null)
            {
                int buffID = int.Parse(action[DramaConfig.TAG_ARG2]);
                float duration = float.Parse(action[DramaConfig.TAG_ARG3]);
                creature.bufferManager.AddBuffer(buffID, duration, true);
                if (needToSendToSvr && action[DramaConfig.TAG_ARG1].Equals("Player"))
                {
                    PlayerAvatar.Player.RpcCall("buff_action_req", 1, buffID);
                }
            }
        }

        public void RemoveBuff(Dictionary<string, string> action)
        {
            EntityCreature creature = DramaUtils.GetCreatureByEntityDramaID(action[DramaConfig.TAG_ARG1]);
            bool needToSendToSvr = Convert.ToBoolean(int.Parse(action[DramaConfig.TAG_ARG3]));
            if (creature != null)
            {
                int buffID = int.Parse(action[DramaConfig.TAG_ARG2]);
                creature.bufferManager.RemoveBuffer(buffID);
                if (needToSendToSvr && action[DramaConfig.TAG_ARG1].Equals("Player"))
                {
                    PlayerAvatar.Player.RpcCall("buff_action_req", 0, buffID);
                }
            }
        }

        public void ShowBossIntroduce(Dictionary<string, string> action)
        {
            int monsterId = int.Parse(action[DramaConfig.TAG_ARG1]);
            List<GameObject> goList = new List<GameObject>();
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityMonsterBase monster;
            while (enumerator.MoveNext())
            {
                var entity = enumerator.Current.Value;
                if (entity is EntityMonsterBase)
                {
                    monster = entity as EntityMonsterBase;
                    if (monster.CanDisplay() && monster.actor != null && monster.actor.gameObject != null)
                    {
                        goList.Add(monster.actor.gameObject);
                    }
                }
            }
            RenderTexture texture = ScreenShot.GetBackgroundBlurScreenShot(CameraManager.GetInstance().Camera, goList);
            //LoggerHelper.Info(string.Format("Drama:ShowBossIntroduce, id = {0}, dramaKey = {1}.", monsterId, _dramaKey));
            UIManager.Instance.ShowPanel(PanelIdEnum.BossIntroduce, new BossIntroduceData(monsterId, texture));
        }

        public void HideBossIntroduce(Dictionary<string, string> action)
        {
            //LoggerHelper.Info(string.Format("Drama:HideBossIntroduce, dramaKey = {0}.", _dramaKey));
            EventDispatcher.TriggerEvent(CGEvent.HIDE_BOSS_INTRODUCE);
        }

        public void ElevatorMove(Dictionary<string, string> action)
        {
            int id = int.Parse(action[DramaConfig.TAG_ARG1]);
            float[] durationArray = new float[action.Count - 3];
            for (int i = 3; i <= action.Count - 1; ++i)
            {
                durationArray[i - 3] = int.Parse(action["a" + i]) * 0.001f;
            }
            GameSceneManager.GetInstance().ElevatorMove(id, durationArray, action[DramaConfig.TAG_ARG2]);
        }

        public void ElevatorMoveBack(Dictionary<string, string> action)
        {
            int id = int.Parse(action[DramaConfig.TAG_ARG1]);
            float[] durationArray = new float[action.Count - 3];
            for (int i = 3; i <= action.Count - 1; ++i)
            {
                durationArray[i - 3] = int.Parse(action["a" + i]) * 0.001f;
            }
            GameSceneManager.GetInstance().ElevatorMoveBack(id, durationArray, action[DramaConfig.TAG_ARG2]);
        }

        public void End(Dictionary<string, string> action)
        {
            bool flag = true;
            if (_curActionPos + 1 < _actions.Count &&
                _actions[_curActionPos + 1][DramaConfig.TAG_ACTION].Equals(DramaConfig.MARK_ON_END))
            {
                this._curActionPos = _curActionPos + 1;
                flag = false;
                _onEnd = true;
            }
            if (flag) {
                
                Stop();
            }
        }

        private Vector3 GetVector3(string vector3String)
        {
            if (vector3String.Equals("0"))
            {
                return -1 * Vector3.one;
            }
            return DramaUtils.StringToVector3(vector3String);
        }

        private Transform GetTransform(EntityCreature creature, string slotName = "")
        {
            if (creature != null && creature.actor != null)
            {
                Transform slotTrans = creature.actor.boneController.GetBoneByName(slotName);
                if (slotTrans != null)
                {
                    return slotTrans;
                }
                return creature.actor.boneController.GetBoneByName("slot_camera");
            }
            return null;
        }

        private Vector3 _localEulerAngles;
        private float _distance = 0;
        public void RecordCameraInfo()
        {
            CameraInfoBackup backup = CameraManager.GetInstance().GetCameraInfoBackup();
            _localEulerAngles = backup.localEulerAngles;
            _distance = backup.distance;
        }

        public void SkipDrama()
        {
            if (!running)
            {
                return;
            }
            if (_onEnd)
            {
                return;
            }
            int curEndPos = 0;
            Reset();
            if (_actions[_curActionPos][DramaConfig.TAG_ACTION].Equals("End"))
            {
                curEndPos = _curActionPos;
            }
            else
            {
                curEndPos = GetNextEndPos();
            }
            if (curEndPos == 0)
            {
                LoggerHelper.Error(string.Format("无法跳过，因当前剧情配置错误，没有对应的End指令。剧情key为: {0}", _dramaKey));
                return;
            }
            _curActionPos = curEndPos;
            End(null);
            _curActionPos++;
        }

        public bool CanRemove()
        {
            return _state == DramaState.IDLE;
        }
    }
}
