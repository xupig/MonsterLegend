﻿using Common.Base;
using Common.Data;
using Common.Structs;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace GameMain.GlobalManager
{
    public class DramaJudgment
    {
        static Dictionary<string, MethodInfo> _judgmentMethodMap;
        static Dictionary<string, MethodInfo> judgmentMethodMap
        {
            get {
                if (_judgmentMethodMap == null)
                {
                    Type type = typeof(DramaJudgment);
                    _judgmentMethodMap = new Dictionary<string, MethodInfo>();
                    var methodInfos = type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic);
                    for (int i = 0; i < methodInfos.Length; i++)
                    {
                        var methodInfo = methodInfos[i];
                        if (methodInfo.ReturnType != DramaConfig.BOOL)continue;
                        _judgmentMethodMap.Add(methodInfo.Name, methodInfo);
                        //Debug.LogError("judgmentMethodMap:" + methodInfo.Name);
                    }
                }
                return _judgmentMethodMap;
            }
        }

        static object[] fastObjectArray = new object[1];
        static public bool ExecuteCondition(string conditionName, string conditionValue)
        {
            if (judgmentMethodMap.ContainsKey(conditionName))
            {
                MethodInfo methodInfo = judgmentMethodMap[conditionName];
                fastObjectArray[0] = conditionValue;
                return (bool)methodInfo.Invoke(null, fastObjectArray);
            }
            return false;
        }

        //************************************以下为各个判断命令实现方法*****************************************

        static bool IsPanelInited(string panelId)
        {
            return BasePanel.initialedPanelSet.Contains((PanelIdEnum)int.Parse(panelId));
        }

        /// <summary>
        /// 章节有可领宝箱
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        static bool ChapterHasBox(string content)
        {
            return PlayerDataManager.Instance.CopyData.ChapterHaveCanGetBox(int.Parse(content));
        }

        /// <summary>
        /// 关卡是否三星通关
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        static bool IsInstanceThreeStarPass(string content)
        {
            return PlayerDataManager.Instance.CopyData.IsInstanceThreeStarPass(int.Parse(content));
        }

        /// <summary>
        /// 试炼秘境关卡有可领宝箱
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        static bool WanderLandInstanceHasBox(string content)
        {
            return PlayerDataManager.Instance.WanderLandData.InstanceHaveCanGetBox(int.Parse(content));
        }

        /// <summary>E:\project\trunk\GameCode\GameAsset\GameAsset\AssetCreator.cs
        /// 章节的N星宝箱是否可以领取
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        static bool ChapterHasBoxInPos(string content)
        {
            string[] data = content.Split(split);
            return PlayerDataManager.Instance.CopyData.BoxStateIsCanGet(int.Parse(data[0]), int.Parse(data[1]));
        }


        static bool IsStarSoulAccepted(string content)
        {
            return false;
        }

        /// <summary>
        /// 星魂宝箱有没有领
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        static bool IsStarSoulReceived(string content)
        {
            return false;
        }

        static bool ItemNumMoreThan(string content)
        {
            string[] data = content.Split(split);
            int id = int.Parse(data[0]);
            int num = int.Parse(data[1]);
            int result = PlayerAvatar.Player.CheckCostLimit(id, num, false);
            if(result == -1)
            {
                return PlayerAvatar.Player.money_bind_diamond + PlayerAvatar.Player.money_diamond >= num;
            }
            else
            {
                return result == 0;
            }
        }

        static bool IsStarSoulActive(string content)
        {
            string[] data = content.Split(split);
            StarItemData starSoulData = PlayerDataManager.Instance.StarSoulData.GetItemDataByPageAndLevel(int.Parse(data[0]), int.Parse(data[1]));
            return starSoulData != null && starSoulData.StarState == StarState.IsActivated;
        }

        static bool IsActive(string content)
        {
            GameObject host = UIManager.Instance.UIRoot;
            Transform childTransform = host.transform.Find(content);
            return childTransform != null && childTransform.gameObject.activeInHierarchy == true;
        }

        static bool PlayerIsLevel(string conditionValue)
        {
            bool result = PlayerAvatar.Player.level == int.Parse(conditionValue);
            return result;
        }

        static bool PlayerLevelBetween(string content)
        {
            string[] values = content.Split(split);
            return PlayerAvatar.Player.level >= int.Parse(values[0]) && PlayerAvatar.Player.level <= int.Parse(values[1]);
        }

        static bool AcceptedQuest(string taskId)
        {
            bool result = PlayerDataManager.Instance.TaskData.IsTaskAccepted(int.Parse(taskId));
            return result;
        }

        static bool FinishQuest(string taskId)
        {
            bool result = PlayerDataManager.Instance.TaskData.IsTaskFinished(int.Parse(taskId));
            return result;
        }

        static bool CompletedQuest(string taskId)
        {
            bool result = PlayerDataManager.Instance.TaskData.IsCompleted(int.Parse(taskId));
            return result;
        }

        static bool VipSmallerThan(string vipLevel)
        {
            return PlayerAvatar.Player.vip_level < int.Parse(vipLevel);
        }

        static bool VipBiggerThan(string vipLevel)
        {
            return PlayerAvatar.Player.vip_level > int.Parse(vipLevel);
        }

        static bool ChargeSmallerThan(string charge)
        {
            return PlayerAvatar.Player.accumulated_charge < int.Parse(charge);
        }

        static bool ChargeBiggerThan(string charge)
        {
            return PlayerAvatar.Player.accumulated_charge > int.Parse(charge);
        }

        static bool InMap(string mapId)
        {
            bool result = GameSceneManager.GetInstance().curMapID == int.Parse(mapId);
            return result;
        }

        static bool InInstance(string instranceId)
        {
            return map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID) == int.Parse(instranceId);
        }

        static bool ElevatorInStartPos(string id)
        {
            return GameSceneManager.GetInstance().IsElevatorInStartPos(int.Parse(id));
        }

        private static char[] split = new char[] { '_'};
        static bool TriggerCount(string count)
        {
            string[] list = count.Split(split);
            //Debug.LogError("----------TriggerCount" + int.Parse(list[0]) + " " + DramaManager.GetInstance().GetTriggerCount(int.Parse(list[0])));
            if (DramaManager.GetInstance().GetTriggerCount(int.Parse(list[0])) < int.Parse(list[1]))
            {
                //DramaManager.GetInstance().AddTiggerCount(int.Parse(list[0]));
                return true;
            }
            return false;
        }

        static bool IsFirstEnterCopy(string id)
        {
            return PlayerDataManager.Instance.CopyData.GetHistoryMaxStar(int.Parse(id)) == 0;
        }

        static bool IsDramaRunning(string argStr)
        {
            string[] args = argStr.Split(',');
            return DramaManager.GetInstance().IsRunning(args[0], args[1]);
        }

        static bool SecondFightingHolderCanFight(string id)
        {
            return PlayerDataManager.Instance.PetData.SecondFightingHolderCanFight();
        }

        static bool FirstAssistHolderCanAssist(string id)
        {
            return PlayerDataManager.Instance.PetData.FirstAssistHolderCanAssist();
        }

        static bool InTouchBattleMode(string value)
        {
            return PlayerTouchBattleModeManager.GetInstance().inTouchModeState;
        }

        static bool InPlatform(string value)
        {
            return platform_helper.InPlatformMode((PlatformMode)int.Parse(value));
        }

        static bool IsPCPlatform(string content)
        {
            if (content.Equals("PC"))
            {
                return Application.platform == RuntimePlatform.WindowsPlayer 
                    || Application.platform == RuntimePlatform.WindowsEditor;
            }
            return false;
        }

        static bool MouseClickMode(string value)
        {
            return KeyboardManager.GetInstance().MouseClickMode == (GlobalManager.MouseClickMode)(int.Parse(value));
        }
    }
}
