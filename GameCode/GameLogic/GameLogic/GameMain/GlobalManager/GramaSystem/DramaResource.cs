﻿using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;


namespace GameMain.GlobalManager
{
    public class DramaResource
    {
        static Dictionary<string, Action<List<string>, Dictionary<string, string>>> _dramaGetMap;
        static Dictionary<string, Action<List<string>, Dictionary<string, string>>> dramaGetMap
        {
            get
            {
                if (_dramaGetMap == null)
                {
                    _dramaGetMap = new Dictionary<string, Action<List<string>, Dictionary<string, string>>>();
                    _dramaGetMap.Add("CreateEntity", GetCreateEntityResource);
                    _dramaGetMap.Add("PlayCameraAnimation", GetPlayCameraAnimationResource);
                    _dramaGetMap.Add("PlayVisualFX", GetPlayVisualFXResource);
                    _dramaGetMap.Add("ShowDialog", GetShowDialogResource);
                }
                return _dramaGetMap;
            }
        }

        public static void GetDramaPreloadResource(List<string> paths, string eventName, string eventValue)
        {
            var triggers = drama_trigger_helper.GetDramaTriggers(eventName, eventValue);
            if (triggers == null) return;
            for (int i = 0; i < triggers.Count; i++)
            {
                var triggerData = triggers[i];
                var dramaName = triggerData.drama;
                GetDramaPreloadResource(paths, dramaName);
            }
        }

        public static void GetDramaPreloadResource(List<string> paths, string dramaName)
        {
            var dramaData = DramaDataPool.GetDramaData(dramaName);
            var actions = dramaData.actions;
            for (int i = 0; i < actions.Count; i++)
            {
                var actionName = actions[i][DramaConfig.TAG_ACTION];
                if (dramaGetMap.ContainsKey(actionName))
                {
                    dramaGetMap[actionName](paths, actions[i]);
                }
            }
        }

        public static void GetDramaMarkPreloadResource(List<string> paths, string dramaName, string dramaMark)
        {
            var dramaData = DramaDataPool.GetDramaData(dramaName);
            var actions = dramaData.actions;
            int mark = dramaData.GetMark(dramaMark);
            mark = (mark == -1) ? 0 : mark;
            bool end = false;
            for (int i = mark; i < actions.Count; i++)
            {
                var actionName = actions[i][DramaConfig.TAG_ACTION];
                if (actionName.Equals("End"))
                {
                    end = true;
                }
                if (end && dramaData.GetMark(actionName) > -1)
                {
                    break;
                }
                if (dramaGetMap.ContainsKey(actionName))
                {
                    dramaGetMap[actionName](paths, actions[i]);
                }
            }
        }

        static void AddToPaths(List<string> paths, string path)
        {
            if (!string.IsNullOrEmpty(path) && !paths.Contains(path))
            {
                paths.Add(path);
            }
        }

        static void GetCreateEntityResource(List<string> paths, Dictionary<string, string> action)
        {
            string entityType = action[DramaConfig.TAG_ARG1];
            uint typeID = uint.Parse(action[DramaConfig.TAG_ARG3]);
            int actorID = 0;
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY || entityType == EntityConfig.ENTITY_TYPE_MONSTER)
            {
                actorID = monster_helper.GetMonsterModel((int)typeID);
            }
            else if (entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
            {
                actorID = npc_helper.GetNpcModelID((int)typeID);
            }
            else if (entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
            {
                actorID = npc_helper.GetNpcModelID((int)typeID);
            }
            string modelPath = ACTSystemAdapter.GetActorModelPath(actorID);
            AddToPaths(paths, modelPath);
        }

        static void GetPlayCameraAnimationResource(List<string> paths, Dictionary<string, string> action)
        {
            string animationPath = action[DramaConfig.TAG_ARG1];
            AddToPaths(paths, animationPath);
        }

        static void GetPlayVisualFXResource(List<string> paths, Dictionary<string, string> action)
        {
            string prefabPath = action[DramaConfig.TAG_ARG1];
            AddToPaths(paths, prefabPath);
        }

        static void GetShowDialogResource(List<string> paths, Dictionary<string, string> action)
        {
            string entityType = action[DramaConfig.TAG_ARG1];
            string content = action[DramaConfig.TAG_ARG2];
            int actorID = 0;
            if (string.IsNullOrEmpty(entityType) || string.IsNullOrEmpty(content)) return;
            var strList = content.Split(',');
            if (entityType == "NPC")
            {
                actorID = npc_helper.GetNpcModelID(int.Parse(strList[0]));
            }
            else if (entityType == "Dummy")
            {
                actorID = monster_helper.GetMonsterModel(int.Parse(strList[0]));
            }
            string modelPath = ACTSystemAdapter.GetActorModelPath(actorID);
            AddToPaths(paths, modelPath);
        }
    }
}
