﻿#region 模块信息
/*==========================================
// 文件名：DramaTrigger
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.GramaSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/19 15:45:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;

namespace GameMain.GlobalManager
{

    public class DramaTriggerType
    {
        public const string AcceptTask = "ACCEPT_QUEST";//2
        public const string ReachTask = "FINISH_QUEST";//3
        public const string CommitTask = "COMMIT_QUEST";//4

        public const string EnterMap = "ENTER_MAP";
        public const string LeaveMap = "LEAVE_MAP";
        public const string CombatWin = "COMBAT_INS_WIN";
        public const string CombatLose = "COMBAT_INS_LOSE";
        public const string ClickButton = "CLICK_BTN";
        public const string FingerGuideEnd = "FINGER_GUIDE_END";
        public const string GetItem = "GET_ITEM";
        public const string HpChange = "HP_CHANGE";
        public const string LevelUp = "LEVEL_UP";
        public const string FightForceChange = "Fight_FORCE_CHANGE";
        public const string Dead = "DEAD";
        public const string OpenFunction = "OPEN_FUNCTION";
        public const string JoinActivity = "JOIN_ACTIVITY";
        public const string PanelDataResp = "PANEL_DATA_RESP";
        public const string ShowPanel = "SHOW_PANEL";
        public const string ClosePanel = "CLOSE_PANEL";
        public const string FinishFindWay = "FINISH_FIND_WAY";
        public const string MonsterDead = "MONSTER_DEAD";
        public const string MonsterEnterGame = "MONSTER_ENTER_GAME";

        public const string EQUIP_UPGRADE = "EQUIP_UPGRADE";
        public const string CGEnd = "CG_END";
        public const string WAIT_SERVER_RESP = "WAIT_SERVER_RESP";
        public const string RuneWishEnd = "RuneWishEnd";
        public const string StarSoulSystem = "StarSoulSystem";
        public const string PetSystem = "PetSystem";
    }


    public class DramaTrigger
    {

        public static void Start()
        {
            if (DramaManager.GetInstance().GetTriggerCount(1) == 0)//还没触发过血量变化的剧情
            {
                EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, HpChange);
            }
            
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, LevelChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, FightForceChange);
           
            EventDispatcher.AddEventListener(SpaceActionEvents.SpaceActionEvents_PlayerDeath, Dead);
            EventDispatcher.AddEventListener<int>(BagEvents.AddItem, GetItem);

            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_ACCEPTED, TaskAccepted);//2
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, TaskCompleted);//4
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_REACHED, TaskReached);//3

            EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, LeaveMap);//离开某场景，目前是mapid
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, EnterMap);//进入某场景 

            EventDispatcher.AddEventListener<int, CopyResult>(CopyEvents.Result, OnCopyResult);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.CLOSE_PANEL, ClosePanel);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.SHOW_PANEL, ShowPanel);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.ON_PANEL_DATA_RESP, OnPanelDataResp);

            EventDispatcher.AddEventListener<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, MonsterDie);
            EventDispatcher.AddEventListener<uint>(MonsterEvents.ON_ENTER_WORLD, MonsterEnterGame);
            EventDispatcher.AddEventListener<int, string>(DramaEvents.DRAMA_END, CGEnd);
            EventDispatcher.AddEventListener(PetEvents.GetPanelShowPetModel, OnPetModelShow);
            EventDispatcher.AddEventListener(StarSoulEvents.ON_STAR_PAGE_OPEN, StarSoulOnPageChange);
        }

        public static void WaitServerResp(int actionId)
        {
            Trigger(DramaTriggerType.WAIT_SERVER_RESP, actionId.ToString());
        }

        public static void EquipUpgrade(int equipId)
        {
            Trigger(DramaTriggerType.EQUIP_UPGRADE, equipId.ToString());
        }

        /// <summary>
        /// 血量变化
        /// </summary>
        /// <param name="hp"></param>
        private static void HpChange()
        {
            Trigger(DramaTriggerType.HpChange, PlayerAvatar.Player.cur_hp_string);
        }

        /// <summary>
        /// 等级变化
        /// </summary>
        /// <param name="level"></param>
        private static void LevelChange()
        {
            Trigger(DramaTriggerType.LevelUp,PlayerAvatar.Player.level.ToString());
        }

        /// <summary>
        /// 角色死亡
        /// </summary>
        private static void Dead()
        {
            int instanceID = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            Trigger(DramaTriggerType.LevelUp, instanceID.ToString());
        }
        
        /// <summary>
        /// 获得道具
        /// </summary>
        /// <param name="itemId"></param>
        private static void GetItem(int itemId)
        {
            Trigger(DramaTriggerType.GetItem, itemId.ToString());
        }

        /// <summary>
        /// 战斗力变化
        /// </summary>
        private static void FightForceChange()
        {
            Trigger(DramaTriggerType.FightForceChange, PlayerAvatar.Player.fight_force.ToString());
        }

        public static void OpenFunction(int functionId)
        {
            Trigger(DramaTriggerType.OpenFunction,functionId.ToString());
        }

        /// <summary>
        /// 参加活动
        /// </summary>
        /// <param name="activityId"></param>
        private static void JoinActivity(int activityId)
        {
            Trigger(DramaTriggerType.JoinActivity, activityId.ToString());
        }

        public static void ClickButton(string buttonName)
        {
            Trigger(DramaTriggerType.ClickButton, buttonName);
        }

        public static void FingerGuideEnd(string guideName)
        {
            Trigger(DramaTriggerType.FingerGuideEnd, guideName);
        }

        public static void OnPetModelShow()
        {
            Trigger(DramaTriggerType.PetSystem, "PetModelShow");
        }

        /// <summary>
        /// 任务已接受(2)
        /// </summary>
        /// <param name="taskId"></param>
        private static void TaskAccepted(int taskId)
        {
            Trigger(DramaTriggerType.AcceptTask, taskId.ToString());
        }

        /// <summary>
        /// 任务目标已达成(3)
        /// </summary>
        /// <param name="taskId"></param>
        private static void TaskReached(int taskId)
        {
            Trigger(DramaTriggerType.ReachTask, taskId.ToString());
        }

        /// <summary>
        /// 任务已完成(4)
        /// </summary>
        /// <param name="taskId"></param>
        private static void TaskCompleted(int taskId)
        {
            
            Trigger(DramaTriggerType.CommitTask,taskId.ToString());
        }

        

        /// <summary>
        /// 进入地图
        /// </summary>
        /// <param name="mapId"></param>
        private static void EnterMap(int mapId)
        {
            Trigger(DramaTriggerType.EnterMap, mapId.ToString());

            TaskData taskData = PlayerDataManager.Instance.TaskData;
            if (taskData.preTaskInfo!=null && taskData.mainTaskInfo != null && taskData.mainTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH && taskData.preTaskInfo.status != public_config.TASK_STATE_ACCOMPLISH)
            {
                TaskManager.Instance.ContinueAutoTask();
            }
        }

        /// <summary>
        /// 离开地图
        /// </summary>
        /// <param name="mapId"></param>
        private static void LeaveMap(int mapId)
        {
            TaskManager.Instance.IdleAutoTask();
            Trigger(DramaTriggerType.LeaveMap, mapId.ToString());
        }

        private static void OnCopyResult(int instanceId, CopyResult copyResult)
        {
            if (copyResult == CopyResult.Win)
            {
                CombatInstanceWin(instanceId);
            }
            else if (copyResult == CopyResult.Lose)
            {
                CombatInstanceLose(instanceId);
            }
        }

        /// <summary>
        /// 通关关卡
        /// </summary>
        private static void CombatInstanceWin(int instanceId)
        {
            Trigger(DramaTriggerType.CombatWin, instanceId.ToString());
        }

        /// <summary>
        /// 关卡失败
        /// </summary>
        /// <param name="instanceId"></param>
        private static void CombatInstanceLose(int instanceId)
        {
            Trigger(DramaTriggerType.CombatLose, instanceId.ToString());
        }

        private static void OnPanelDataResp(PanelIdEnum panelId)
        {
            Debug.Log("OnPanelDataResp:"+ DramaTriggerType.PanelDataResp + "," + panelId);
            Trigger(DramaTriggerType.PanelDataResp, ((int)panelId).ToString());
        }

        /// <summary>
        /// 打开界面
        /// </summary>
        /// <param name="panelId"></param>
        private static void ShowPanel(PanelIdEnum panelId)
        {
            Trigger(DramaTriggerType.ShowPanel,((int)panelId).ToString());
        }

        /// <summary>
        /// 关闭界面
        /// </summary>
        /// <param name="panelId"></param>
        private static void ClosePanel(PanelIdEnum panelId)
        {
            Trigger(DramaTriggerType.ClosePanel, ((int)panelId).ToString());
        }

        /// <summary>
        /// 寻路结束
        /// </summary>
        private static void FinishFindWay()
        {
            Trigger(DramaTriggerType.FinishFindWay, string.Empty);
        }

        /// <summary>
        /// 怪物死亡
        /// </summary>
        /// <param name="monsterId"></param>
        private static void MonsterDie(uint monsterId,int type)
        {
            Trigger(DramaTriggerType.MonsterDead, monsterId.ToString());
        }

        /// <summary>
        /// 怪物出现
        /// </summary>
        private static void MonsterEnterGame(uint monsterId)
        {
            Trigger(DramaTriggerType.MonsterEnterGame, monsterId.ToString());
        }

        /// <summary>
        /// CG结束
        /// </summary>
        private static void CGEnd(int id, string dramaKey)
        {
            Trigger(DramaTriggerType.CGEnd, id.ToString());
        }

        public static void RuneWishEnd()
        {
            Trigger(DramaTriggerType.RuneWishEnd, "1403");
        }

        private static void StarSoulOnPageChange()
        {
            Trigger(DramaTriggerType.StarSoulSystem, "StarSoulOnPageChange");
        }

        private static void Trigger(string type,string content)
        {
            DramaManager.GetInstance().Trigger(type, content);
        }

    }
}
