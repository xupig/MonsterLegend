﻿using Common.Base;
using GameLoader.Utils;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DramaDriver : MonoBehaviour
    {
        public bool play = false;
        public string eventName = "PLAY_CG";
        public string eventValue = "1001";
        private int _playSpaceTime;
        private static DramaDriver _instance;
        public static DramaDriver Instance
        {
            get { return _instance; }
        }

        public bool openDrama = true;

        void Awake()
        {
            _instance = this;
        }

        public delegate void VoidDelegate();
        public VoidDelegate onUpdate;
        void Update()
        {
            if (onUpdate != null)
            {
                onUpdate();
            }
            CheckingPlay();
        }

        void OnDestroy()
        {
            DramaManager.GetInstance().DriverOnDestroy();
        }

        void CheckingPlay()
        {
            if (_playSpaceTime > 0) 
            { 
                _playSpaceTime--;
                return; 
            }
            if (play)
            {
                play = false;
                _playSpaceTime = 30;
                DramaManager.GetInstance().Trigger(eventName, eventValue);
                LoggerHelper.Debug(string.Concat("DramaDriver Trigger:" + eventName + eventValue));
            }
        }
    }
}
