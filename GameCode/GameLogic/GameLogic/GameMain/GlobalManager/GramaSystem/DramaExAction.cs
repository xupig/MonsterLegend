﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using GameData;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DramaExAction
    {

        private static char[] split = new char[] { ','};

        static Dictionary<string, MethodInfo> _exActionMethodMap;
        static Dictionary<string, MethodInfo> exActionMethodMap
        {
            get
            {
                if (_exActionMethodMap == null)
                {
                    Type type = typeof(DramaExAction);
                    _exActionMethodMap = new Dictionary<string, MethodInfo>();
                    var methodInfos = type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic);
                    for (int i = 0; i < methodInfos.Length; i++)
                    {
                        var methodInfo = methodInfos[i];
                        //if (methodInfo.ReturnType != DramaConfig.BOOL) continue;
                        _exActionMethodMap.Add(methodInfo.Name, methodInfo);
                        //Debug.LogError("exActionMethodMap:" + methodInfo.Name);
                    }
                }
                return _exActionMethodMap;
            }
        }

        static Drama _drama;

        static object[] fastObjectArray = new object[1];
        static public void ExecuteExAction(Drama drama, string actionName, string actionValue)
        {
            try
            {
                if (exActionMethodMap.ContainsKey(actionName))
                {
                    MethodInfo methodInfo = exActionMethodMap[actionName];
                    fastObjectArray[0] = actionValue;
                    _drama = drama;
                    methodInfo.Invoke(null, fastObjectArray);
                    _drama = null;
                }
            }
            catch (Exception e)
            {
                Debug.LogError("ExecuteExAction Error:[ " + actionName + ":" + actionValue + " ]\n" + e.Message);
            }
        }

        static void EnterMap(string instId)
        {
            var table = new GameLoader.Utils.CustomType.LuaTable();
            table.Add(1, int.Parse(instId));
            PlayerAvatar.Player.RpcCall("mission_action_req", Common.ServerConfig.action_config.ACTION_ENTER_MISSION, table);
        }

        /// <summary>
        /// 任务追踪
        /// </summary>
        /// <param name="taskId"></param>
        static void OnQuestEvent(string taskId)
        {
            task_data data = task_data_helper.GetTaskData(int.Parse(taskId));
            //ari TaskFollowHandle.TaskFollow(data);
        }

        static void ShowPanel(string viewId)
        {
            view_helper.OpenView(int.Parse(viewId));
        }

        static void ClosePanel(string viewId)
        {
            ((PanelIdEnum)int.Parse(viewId)).Close();
        }

        static void ShowGuideArrow(string guide)
        {
            //Debug.Log("ShowGuideArrow:"+guide);
            PanelIdEnum.GuideArrow.Show(guide.Split(split));
        }

        static void ShowTouchBattleGuide(string guide)
        {
            PanelIdEnum.TouchBattleMode.Show();
        }

        static void ShowFingerGuide(string guide)
        {
            if(platform_helper.InPCPlatform())
            {
                PanelIdEnum.PCFingerGuide.Show(guide.Split(split));
            }
            else
            {
                PanelIdEnum.FingerGuide.Show(guide.Split(split));
            }
            
        }

        static void SelectChallenge(string content)
        {
            string[] result = content.Split(split);
            EventDispatcher.TriggerEvent(ActivityEvents.SELECT_CHALLENGE, int.Parse(result[1]), result[0], result[2]);
        }

        static void SelectRisingStar(string content)
        {
            string[] result = content.Split(split);
            EventDispatcher.TriggerEvent(EquipEvents.SELECT_RISING_STAR, int.Parse(result[1]), result[0], result[2]);
        }

        static void SelectLimitActivity(string content)
        {
            string[] result = content.Split(split);
            EventDispatcher.TriggerEvent(ActivityEvents.SELECT_LIMIT_ACTIVITY, int.Parse(result[1]), result[0], result[2]);
        }

        static void SetTriggerCount(string triggerId)
        {
            DramaManager.GetInstance().StoreTriggerCount(int.Parse(triggerId));
        }

        static void ShowSceneChangeMask(string param)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.MaskPanel, param);
        }

        public static void Show(string visibleStr)
        {
            ShowMainUI(visibleStr);
        }

        static void ShowMainUI(string visibleStr)
        {
            bool visible = Convert.ToBoolean(int.Parse(visibleStr));
            if (visible)
            {
                UIManager.Instance.ShowOtherLayer(MogoUILayer.LayerHideOther);
                BaseModule.CGEndAndShow();
				AddCGBuff("0");
            }
            else
            {
                UIManager.Instance.HideOtherLayer(MogoUILayer.LayerHideOther);
                AddCGBuff("1");
            }
            EventDispatcher.TriggerEvent<bool>(CGEvent.ON_VIEWING_MODE_CHANGED, visible);
        }

		static void AddCGBuff(string addStr)
		{
			bool add = Convert.ToBoolean(int.Parse(addStr));
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            while (enumerator.MoveNext())
            {
                if (!(enumerator.Current.Value is EntityCreature)) continue;
                creature = enumerator.Current.Value as EntityCreature;
				if (add)
				{
					creature.bufferManager.AddBuffer(InstanceDefine.BUFF_ID_VIEWING_MODE, -1f, true);
				}
				else
				{
					creature.bufferManager.RemoveBuffer(InstanceDefine.BUFF_ID_VIEWING_MODE);
				}
            }
        }

        static void ShowMonstersAndNPCs(string visibleStr)
        {
            bool visible = Convert.ToBoolean(int.Parse(visibleStr));
            DramaManager.GetInstance().currentShowOtherEntities = visible;
            var enumerator = MogoWorld.Entities.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entity = enumerator.Current.Value;
                if (entity is EntityCreature && !(entity as EntityCreature).isDramaEntity)
                {
                    (entity as EntityCreature).UpdateDramaVisible(visible);
                }
            }
        }

        static void ShowDialogBtnSkip(string isShowStr)
        {
            bool isShow = Convert.ToBoolean(int.Parse(isShowStr));
            CGDialogShowSkip data = new CGDialogShowSkip(_drama.dramaKey, isShow);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, data);
            _drama.isShowingSkip = isShow;
        }

        static void ShowDialogBlackEdge(string isShowStr)
        {
            bool isShow = Convert.ToBoolean(int.Parse(isShowStr));
            CGDialogShowBlackEdge data = new CGDialogShowBlackEdge(_drama.dramaKey, isShow);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, data);
            _drama.isShowingBlackEdge = isShow;
        }

        static void HideSkillBtn(string skillBtnIndex)
        {
            skill_guide_helper.AddBeforeGuide(int.Parse(skillBtnIndex));
            EventDispatcher.TriggerEvent<int, bool>(BattleUIEvents.SET_SKILL_BEFORE_NOTICE, int.Parse(skillBtnIndex), true);
        }

        static void PlaySkillBtnAnimation(string skillBtnIndex)
        {
            skill_guide_helper.RemoveBeforeGuide(int.Parse(skillBtnIndex));
            EventDispatcher.TriggerEvent<int>(BattleUIEvents.ADD_SKILL, int.Parse(skillBtnIndex));
        }

        static void SetSceneObjectActive(string content)
        {
            string[] result = content.Split(split);
            GameSceneManager.GetInstance().SetSceneObjVisible(result[0], result[1] != "0");
        }

        static void SetLODBias(string content)
        {
            QualitySettings.lodBias = float.Parse(content);
        }

        static void ShowElevator(string idStr)
        {
            GameSceneManager.GetInstance().ShowElevator(int.Parse(idStr), true);
        }

        static void HideElevator(string idStr)
        {
            GameSceneManager.GetInstance().ShowElevator(int.Parse(idStr), false);
        }

        static void ResetElevator(string idStr)
        {
            GameSceneManager.GetInstance().ResetElevator(int.Parse(idStr));
        }

        static void SetElevatorState(string stateStr)
        {
            PlayerAvatar.Player.SetElevatorState(Convert.ToBoolean(int.Parse(stateStr)));
        }

        static void ShowFindingPathFx(string isShowStr)
        {
            bool isShow = Convert.ToBoolean(int.Parse(isShowStr));
            PlayerAvatar.Player.SetFindPathFxActive(isShow);
        }

        static void RecoverTouchesCamera(string value)
        {
            CameraManager.GetInstance().OnRecoverTouchesCamera();
        }

        static void SetPlayerAI(string value)
        {
            bool aiState = Convert.ToBoolean(int.Parse(value));
            if (PlayerAvatar.Player.entityAI != null)
            {
                if (!aiState)
                {
                    PlayerAvatar.Player.entityAI.blackBoard.AddThinkableCount();
                }
                else
                {
                    PlayerAvatar.Player.entityAI.blackBoard.SubtractThinkableCount();
                }
            }
        }
    }
}
