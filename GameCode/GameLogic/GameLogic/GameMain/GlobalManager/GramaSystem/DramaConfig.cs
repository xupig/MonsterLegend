﻿using System;


namespace GameMain.GlobalManager
{
    public enum DramaType : byte
    {
        CG = 0,
        GUIDE
    }

    public class DramaConfig
    {
        static public string TAG_ACTION = "act";
        static public string TAG_ARG1 = "a1";
        static public string TAG_ARG2 = "a2";
        static public string TAG_ARG3 = "a3";
        static public string TAG_ARG4 = "a4";
        static public string TAG_ARG5 = "a5";
        static public string TAG_ARG6 = "a6";
        static public string TAG_ARG7 = "a7";
        static public string TAG_ARG = "a";

        static public string MARK_ON_END = "_ON_END";

        public static Type INT = typeof(int);
        public static Type FLOAT = typeof(float);
        public static Type STRING = typeof(string);
        public static Type BOOL = typeof(bool);

        public const string SKIP_EVENT_NAME = "SKIP_DIALOG";
        public const string SKIP_EVENT_VALUE = "0";

        static public bool IsInternalKeyWord(string actionName)
        {
            if (actionName.Equals(MARK_ON_END)) 
            { 
                return true; 
            }
            return false;
        }

        static public bool IsMark(string actionName)
        {
            if (actionName.Substring(0, 2).Equals("__"))
            {
                return true;
            }
            return false;
        }
    }
}
