﻿using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DramaManager
    {
        private Dictionary<string, Drama> _dramas = new Dictionary<string,Drama>();
        private List<string> _dramasNameList = new List<string>();

        private string _curTriggerEventName;
        private string _curTriggerEventValue;

        private Dictionary<int, int> _triggerCountDict;

        private bool _isBlockingUpdate = true;  //当前是否阻止剧情的进行(用于剧情计数数据初始化前阻止更新)
        public bool isBlockingUpdate
        {
            get { return _isBlockingUpdate; }
            set { _isBlockingUpdate = value; }
        }

        private bool _currentShowOtherEntities = true; //当前是否显示非剧情的实体(为false时，剧情播放过程中任何已存在或刚出生的非剧情实体会被隐藏)
        public bool currentShowOtherEntities
        {
            get { return _currentShowOtherEntities; }
            set { _currentShowOtherEntities = value; }
        }

        private DramaDriver _driver;
        private DramaLoader _loader;

        private static DramaManager _instance;
        
        public static DramaManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DramaManager();
            }
            return _instance;
        }

        private void AddTiggerCount(int triggerId)
        {
            if(_triggerCountDict.ContainsKey(triggerId) == false)
            {
                _triggerCountDict.Add(triggerId,1);
            }
            else
            {
                _triggerCountDict[triggerId] += 1;
            }
        }

        public void StoreTriggerCount(int triggerId)
        {
            AddTiggerCount(triggerId);
            if(_triggerCountDict.ContainsKey(triggerId))
            {
                PlayerAvatar.Player.RpcCall("set_trigger_count_req", triggerId, _triggerCountDict[triggerId]);
            }
        }

        //是否正在播放剧情
        public bool IsRunning()
        {
            if(_dramas!=null)
            {
                foreach (KeyValuePair<string, Drama> kvp in _dramas)
                {
                    if (kvp.Value.running)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //某个剧情是否正在播放
        public bool IsRunning(string drama, string mark)
        {
            string key = string.Concat(drama, mark);
            if (_dramas != null && _dramas.ContainsKey(key) && _dramas[key].running)
            {
                return true;
            }
            return false;
        }

        //是否处于观影模式(只能观看不能操作的CG)
        public bool IsInViewingMode()
        {
            return PlayerAvatar.Player.bufferManager.ContainsBuffer(InstanceDefine.BUFF_ID_VIEWING_MODE);
        }

        public void GetTriggerCountDict()
        {
            PlayerAvatar.Player.RpcCall("get_trigger_count_req");
        }

        public int GetTriggerCount(int triggerId)
        {
           
            if(_triggerCountDict.ContainsKey(triggerId))
            {
                return _triggerCountDict[triggerId];
            }
            _triggerCountDict.Add(triggerId, 0);
            return 0;
        }

        private DramaManager()
        {
            var obj = new UnityEngine.GameObject("Drama");
            _driver = obj.AddComponent<DramaDriver>();
            UnityEngine.Object.DontDestroyOnLoad(obj);
            _triggerCountDict = new Dictionary<int, int>();
            _loader = new DramaLoader();
            if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingDrama(null), obj);
        }

        public void DriverOnDestroy()
        {
            _driver = null;
        }

        public void OnDramaResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            if (actionId == (UInt32)action_config.ACTION_DRAMA_GUIDE_GET)
            {
                _isBlockingUpdate = false;
                PbTriggerCountList list = MogoProtoUtils.ParseProto<PbTriggerCountList>(bytes);
                for (int i = 0; i < list.trigger_count_info.Count; i++)
                {
                    PbTriggerCountInfo info = list.trigger_count_info[i];
					int id = (int)info.id;
					if (_triggerCountDict.ContainsKey(id))
					{
						_triggerCountDict.Remove(id);
					}
                    _triggerCountDict.Add(id, (int)info.count);
                }
            }
        }

        public void SkipDrama(string dramaKey)
        {
            if (!_dramas.ContainsKey(dramaKey))
            {
                LoggerHelper.Info(string.Format("跳过剧情失败，当前没有剧情{0}在播放", dramaKey));
                return;
            }
            _dramas[dramaKey].SkipDrama();
        }

        public bool Trigger(string eventName, string eventValue)
        {
            if (_driver != null && !_driver.openDrama) return false;
            _curTriggerEventName = eventName;
            _curTriggerEventValue = eventValue;
            TriggerWaitEvent(eventName, eventValue);
            var triggers = drama_trigger_helper.GetDramaTriggers(eventName, eventValue);
            if (triggers == null || triggers.Count == 0) return false;
            for (int i = 0; i < triggers.Count; i++)
            {
                var triggerData = triggers[i];
                RunDrama(triggerData.id, triggerData.drama, triggerData.mark);
            }
            return true;
        }

        private void RunDrama(int id, string dramaName, string dramaMark)
        {
            ClearDramas();
            string dramaKey = dramaName + dramaMark;
            Drama newDrama = null;
            if (!_dramas.ContainsKey(dramaKey))
            {
                newDrama = new Drama(id, dramaName);
                _dramas.Add(dramaKey, newDrama);
                _dramasNameList.Add(dramaKey);
                DoBeforeStartDrama(newDrama);
                newDrama.Start(_loader, dramaMark);
                EventDispatcher.TriggerEvent<int>(StatisticsEvent.ON_TRIGGER_GUIDE_OR_CG, id);
            }
            else
            {
                LoggerHelper.Info(string.Format("@林绍东 @严永康 播放剧情失败。当前已有相同的名称为{0}，mark为{1}的剧情正在播放。", dramaName, dramaMark));
            }
        }

        public void DramaOnEnd(Drama drama)
        {
            DoAfterEndDrama(drama);
            _dramasNameList.Remove(drama.dramaKey);
            _dramas.Remove(drama.dramaKey);
        }

        private void DoBeforeStartDrama(Drama drama)
        {
            drama.RecordCameraInfo();
            PauseAutoFight();
            PausePetAI();
        }

        private void PauseAutoFight()
        {
            PlayerAutoFightManager.GetInstance().Pause();
        }

        private void PausePetAI()
        {
            uint curPetEntID = PlayerPetManager.GetInstance().curPetEntityID;
            if (curPetEntID == 0)
            {
                return;
            }
            EntityCreature pet = MogoWorld.GetEntity(curPetEntID) as EntityCreature;
            pet.StopThinking();
        }

        private void DoAfterEndDrama(Drama drama)
        {
            if (PlayerAutoFightManager.GetInstance().fightType != FightType.NONE)
            {
                ResumeAutoFight();
            }
            ResumePetAI();
            if (drama.isShowingSkip)
            {
                EventDispatcher.TriggerEvent(CGEvent.HIDE_SKIP);
            }
            if (drama.isShowingBlackEdge)
            {
                EventDispatcher.TriggerEvent(CGEvent.HIDE_BLACK_EDGE);
            }
            EventDispatcher.TriggerEvent<int, string>(DramaEvents.DRAMA_END,
                drama.id, drama.dramaKey);
        }

        private void ResumeAutoFight()
        {
            PlayerAutoFightManager.GetInstance().Resume();
        }

        private void ResumePetAI()
        {
            uint curPetEntID = PlayerPetManager.GetInstance().curPetEntityID;
            if (curPetEntID == 0)
            {
                return;
            }
            EntityCreature pet = MogoWorld.GetEntity(curPetEntID) as EntityCreature;
            pet.RecoveryThinking();
        }

        private void ClearDramas()
        {
            for (int i = _dramasNameList.Count - 1; i >= 0; i--)
            {
                string dramaName = _dramasNameList[i];
                if (_dramas[dramaName].CanRemove())
                {
                    _dramasNameList.RemoveAt(i);
                    _dramas.Remove(dramaName);
                }
            }
        }

        private void TriggerWaitEvent(string eventName, string eventValue)
        {
            for (int i = _dramasNameList.Count - 1; i >= 0; i--)
            {
                string dramaName = _dramasNameList[i];
                if (_dramas[dramaName].running)
                {
                    _dramas[dramaName].TriggerWaitEvent(eventName, eventValue);
                }
            }
        }

        public Dictionary<string, Drama> GetDramas()
        {
            return _dramas;
        }
    }
}
 