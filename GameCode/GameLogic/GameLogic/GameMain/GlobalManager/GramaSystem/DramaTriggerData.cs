﻿#region 模块信息
/*==========================================
// 文件名：DramaTriggerData
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.GramaSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/23 14:04:18
// 描述说明：
// 其他：
//==========================================*/
#endregion


namespace GameMain.GlobalManager
{
    public class DramaTriggerData
    {
        public string drama;
        public string eventname;
        public string eventvalue;
        public int id;
        public string mark;
        public DramaType dramaType;
    }
}
