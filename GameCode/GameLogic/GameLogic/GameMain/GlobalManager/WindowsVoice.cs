﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
//using System.Media;

namespace GameMain.GlobalManager
{
    public class WindowsVoice
    {
        private static WindowsVoice s_instance = null;
        public static WindowsVoice GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new WindowsVoice();
            }
            return s_instance;
        }

        public void Init()
        {
            Initialize();
        }

        public void Play(string strFile)
        {
            /*SoundPlayer S = new SoundPlayer(strFile);
            S.Play();*/
            PlaySound(strFile,0,1);
        }

        //private const string PluginPath = "../../WindowsVoiceDLL.dll";
        private const string PluginPath = @"C:\Windows\System32\winmm.dll";
        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void StartVonveration(string strFile);

        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlaySound(string strFile, int mod, int flag);

        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void Initialize();
    }
}
