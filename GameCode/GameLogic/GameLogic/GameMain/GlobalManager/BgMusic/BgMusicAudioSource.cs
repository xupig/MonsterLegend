﻿#region 模块信息
/*==========================================
// 模块名：BgMusicAudioSource
// 命名空间: GameMain.GlobalManager
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/07/02
// 描述说明：背景音乐源
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public enum MusicType
    {
        /// <summary>
        /// 通用音乐
        /// </summary>
        COMMUSIC = 0,

        /// <summary>
        /// 轻音乐
        /// </summary>
        LIGHTMUSIC = 1
    }

    public class BgMusicAudioSource : Singleton<BgMusicAudioSource>
    {
        public float settingVolume = 0f;
        private Dictionary<int, BgMusicInfo> _comMusic;    //通用声道源字典
        private BgMusicInfo _lightMusic = null;            //轻音乐

        private GameObject _audioSourceSlot = null;             

        static BgMusicAudioSource()
        {
            _instance = new BgMusicAudioSource();
                       
        }

        public BgMusicAudioSource()
            : base()
        {
            _comMusic = new Dictionary<int, BgMusicInfo>();
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<float>(Common.Events.BgMusicEvent.CHANGE_MUSIC_VOLUME, OnChangeMusicVolume);
        }

        public BgMusicInfo GetAudioSource(EntityCreature target, GameData.music musicData, bool isLightMusic)
        {
            AddAudioSourceObject(target);
            //LoggerHelper.Error("[BgMusicAudioSource:GetAudioSource]=>1_______musicData.__type:  " + (MusicType)musicData.__type);
            return isLightMusic == true ? GetLightMusicAudioSource(musicData) : GetComAudioSource(musicData);
        }

        private BgMusicInfo GetComAudioSource(GameData.music musicData)
        {
            //LoggerHelper.Error("[BgMusicAudioSource:GetLightMusicAudioSource]=>2________audioSourceSlot:    " + _audioSourceSlot);
            if (_comMusic.ContainsKey(musicData.__audioNum)) return _comMusic[musicData.__audioNum];
            BgMusicInfo music = new BgMusicInfo();
            music.audioSource = _audioSourceSlot.AddComponent<AudioSource>();
            music.SetInfo(musicData);
            _comMusic.Add(musicData.__audioNum, music);
            return _comMusic[musicData.__audioNum];
        }

        private BgMusicInfo GetLightMusicAudioSource(GameData.music musicData)
        {
            //LoggerHelper.Error("[BgMusicAudioSource:GetLightMusicAudioSource]=>3________audioSourceSlot:    " + _audioSourceSlot);
            if (_lightMusic != null) return _lightMusic;
            _lightMusic = new LightMusic();
            _lightMusic.audioSource = _audioSourceSlot.AddComponent<AudioSource>();
            _lightMusic.SetInfo(musicData);
            return _lightMusic;
        }

        private void AddAudioSourceObject(EntityCreature target)
        {
            if (_audioSourceSlot != null) return;
            _audioSourceSlot = new GameObject();
            _audioSourceSlot.name = "BGM_Slot";
            target.RecordBGMSlot(_audioSourceSlot.transform);
            target.SetBMGSlot();
            
        }

        /// <summary>
        /// 关闭所有背景音乐
        /// </summary>
        public void StopAllBgMusic()
        {
            if (_lightMusic != null)
            {
                _lightMusic.FadeOut(BgMusicManager.Instance.changScenefadeTime);
            }
            foreach (var music in _comMusic)
            {
                //music.Value.Stop();
                music.Value.FadeOut(BgMusicManager.Instance.changScenefadeTime);
            }
        }

        #region 检查音乐通道
        /// <summary>
        /// 只是检查通用通道
        /// </summary>
        public bool CheckComIsPlaying()
        {
            foreach (var music in _comMusic)
            {
                if (music.Value.IsPlaying() == true
                    || music.Value.status == MusicStatus.READY)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckLightMusicIsPlaying()
        {
            return _lightMusic.IsPlaying();
        }

        public void RandomPlayLightMusic()
        {
            if(_lightMusic == null) return;
            (_lightMusic as LightMusic).RandomPlay();
        }
        #endregion

        /// <summary>
        /// 统计正在播放背景音乐个数
        /// </summary>
        public void CalcMusicPlayingNum()
        {
            int num = 0;
            foreach (var music in _comMusic)
            {
                if (music.Value.IsPlaying() == true)
                {
                    num++;
                    LoggerHelper.Error("[CalcMusicPlayingNum:_comMusic]=>1___num:  " + num + ",name:  " + music.Value.audioSource.clip.name + ",ID:   " + _lightMusic.audioSource.GetInstanceID());
                }
            }
            if (_lightMusic.IsPlaying() == true)
            {
                num++;
                LoggerHelper.Error("[CalcMusicPlayingNum:_lightMusic]=>2___num:  " + num + ",name:  " + _lightMusic.audioSource.clip.name + ",ID:   " + _lightMusic.audioSource.GetInstanceID());
            }
            LoggerHelper.Error("[BgMusicAudioSource:CalcMusicPlayingNum]=>3___num:  " + num);
        }

        #region 事件
        private void OnChangeMusicVolume(float volume)
        {
            if (_lightMusic != null)
            {
                _lightMusic.volume = volume;
            }
            foreach (var audioSource in _comMusic)
            {
                audioSource.Value.volume = volume;
            }
            settingVolume = volume;
            
        }
        #endregion

        #region 外部音量控制
        public void TempCloseVolume()
        {
            if (_lightMusic != null)
            {
                _lightMusic.TempCloseVolume();
            }
            foreach (var audioSource in _comMusic)
            {
                audioSource.Value.TempCloseVolume();
            }
        }

        public void ResetTempVolume()
        {
            if (_lightMusic != null)
            {
                _lightMusic.ResetTempVolume();
            }
            foreach (var audioSource in _comMusic)
            {
                audioSource.Value.ResetTempVolume();
            }
        }
        #endregion

    }
}
