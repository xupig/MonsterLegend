﻿using Common.Data;
using Game.Asset;
using GameLoader.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class LoginMusic
    {
        private static string SLOT_NAME = "LogionMusicSlot";
        private static string LISTENER_NAME = "LogionMusicListener";

        private static LoginMusic _instance = null;
        private AudioSource _audioSource = null;
        private MusicTween _musicTween;
        private GameObject musicGo;
        private GameObject _listenerGo = null;
        private BHRecoverMusic _recoverMusic = null;
        public float settingVolume
        {
            get { return PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Music) / 100f; }
        }

        public static LoginMusic GetInstance()
        {
            if (_instance == null)
            {
                _instance = new LoginMusic();
            }
            return _instance;
        }

        public LoginMusic()
        {
            _musicTween = new MusicTween();
        }
                                           
        private void GetLoginMusicSlot()
        {
            musicGo = GameObject.Find(SLOT_NAME);
            if (musicGo == null)
            {
                musicGo = new GameObject();
                musicGo.name = SLOT_NAME;
                _recoverMusic = musicGo.AddComponent<BHRecoverMusic>();
                GameObject.DontDestroyOnLoad(musicGo);
                musicGo.AddComponent<AudioSource>();
            }
            _audioSource = musicGo.GetComponent<AudioSource>();
            GetLoginListener();
            SetAudioClip(() =>
                {
                    //_audioSource.Play();
                    FadeIn(1f);
                });
        }

        private void GetLoginListener()
        {
            _listenerGo = GameObject.Find(LISTENER_NAME);
            if (_listenerGo == null)
            {
                _listenerGo = new GameObject();
                _listenerGo.name = LISTENER_NAME;
                GameObject.DontDestroyOnLoad(_listenerGo);
                _listenerGo.AddComponent<AudioListener>();
            }
        }

        public void RemoveAudioListener()
        {
            if (_listenerGo == null) return;
            Transform.Destroy(_listenerGo);
            _listenerGo = null;
        }

        public void SetClipAndPlay()
        {
            GetLoginMusicSlot();
        }

        string loginMusicName = "Sound/Music/login_main.mp3";
         void SetAudioClip(Action callBack = null)
        {
            ObjectPool.Instance.GetAudioClip(loginMusicName, (clip) =>
            {
                _recoverMusic.AddAudioClip(loginMusicName);
                _audioSource.clip = clip;
                if (callBack != null)
                {
                    callBack();
                }
            });
        }

         

        #region 淡入淡出
        public void FadeIn(float fadeTime, Action fadeinFinishedACT = null)
        {
            if (_audioSource == null) return;
            _musicTween.Fade(0, settingVolume,
                fadeTime, 20, FadeInFinished, FadeInInterval);
            _audioSource.volume = 0;
            _audioSource.loop = true;
            _audioSource.Play();
        }

        private void FadeInInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            //LoggerHelper.Error("1______________audioSource.volume:  " + _audioSource.volume);
        }

        private void FadeInFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            //LoggerHelper.Error("2______________audioSource.volume:  " + _audioSource.volume);
        }


        public void FadeOut(float fadeTime = 1.5f, Action fadeOutFinishedACT = null)
        {
            if (_audioSource == null) return;
            if (_audioSource.isPlaying == false) return;
            _musicTween.Fade(_audioSource.volume, 0, fadeTime, 20, FadeOutFinished, FadeOutInterval);
        }

        private void FadeOutInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            //LoggerHelper.Error("3______________audioSource.volume:  " + _audioSource.volume);
        }

        private void FadeOutFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            _audioSource.Stop();
            //LoggerHelper.Error("4______________audioSource.volume:  " + _audioSource.volume);
        }
        #endregion



    }
}
