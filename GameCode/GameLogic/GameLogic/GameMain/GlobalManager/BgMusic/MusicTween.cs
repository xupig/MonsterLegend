﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.GlobalManager
{
    public class MusicTween
    {
        private uint _timerID = 0;

        public void Fade(float startValue,float endValue,float fadeTime,int interval = 20,Action<float> finishedFunc = null,Action<float> intervalFunc = null)
        {
            int _intervalTime = (int)(fadeTime * 1000) / interval;  //计算定时次数
            float _changeValue = Mathf.Abs(startValue - endValue) / (float)_intervalTime;          //计算改变量
            if (startValue >= endValue) _changeValue = -_changeValue;
            //LoggerHelper.Error("[MusicTween:Fade]=>1______intervalTime: " + _intervalTime + ",_changeValue: " + _changeValue);
            DeleteTimer();
            _timerID = GameLoader.Utils.Timer.TimerHeap.AddTimer(0, interval, () => 
            {
                _intervalTime--;
                startValue += _changeValue;
                
                if (Mathf.Abs(startValue - endValue) <= 0.0001f)
                {
                    startValue = endValue;
                }
                if (intervalFunc != null) intervalFunc(startValue);
                if (_intervalTime <= 0)
                {
                    DeleteTimer();
                    startValue = endValue;
                    if (finishedFunc != null) finishedFunc(startValue);
                    
                }
            });

        }


        private void DeleteTimer()
        {
            if (_timerID != 0)
            {
                GameLoader.Utils.Timer.TimerHeap.DelTimer(_timerID);
                _timerID = 0;
            }
        }

        public void ResetTimer()
        {
            if (_timerID != 0)
            {
                GameLoader.Utils.Timer.TimerHeap.DelTimer(_timerID);
                _timerID = 0;
            }
        }

    }
}
