﻿#region 模块信息
/*==========================================
// 模块名：MusicTriger
// 命名空间: GameMain.GlobalManager
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/07/02
// 描述说明：音乐触发器
// 其他：
//==========================================*/
#endregion

using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class MusicTrigerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum MonsterType
    {
        NONE = 0,
        SMALL_MONSTER = 1,
        BIG_MONSTER = 2,
        BOSS = 3     
    }

    public class MusicTriger
    {
        private int _mapID;
        private bool _isCheckBossMusic = false;
        private bool _isCheckMonsterMusic = false;
        private Action<int> _playBossMusicAct = null;
        private Action<int> _playMonsterMusicAct = null;

        public MusicTriger()
        {
            //
        }

        private void CheckMusic()
        {

            BgMusicAudioSource.Instance.RandomPlayLightMusic();
            //
            if (BgMusicAudioSource.Instance.CheckComIsPlaying() == true) return;
            //监测怪物类型,通知播放相应的背景音乐
            MonsterType monsterType = CheckMonsterType();
            if (monsterType == MonsterType.NONE) return;

            Action<int> act = monsterType == MonsterType.BOSS ? _playBossMusicAct : _playMonsterMusicAct;
            act(_mapID);
            
        }

        /// <summary>
        /// 设置监测信息
        /// </summary>
        public void SetCheckInfo(int mapID,bool checkBossMusic = false,bool checkMonsterMusic = false,
            Action<int> playBossMusicAct = null,Action<int> playMonsterMusicAct = null)
        {
            this._isCheckBossMusic = checkBossMusic;
            this._isCheckMonsterMusic = checkMonsterMusic;
            if (_isCheckBossMusic == true || checkMonsterMusic == true)
            {
                StartCheck();
                this._mapID = mapID;
                this._playBossMusicAct = playBossMusicAct;
                this._playMonsterMusicAct = playMonsterMusicAct;
            }
        }

        private void StartCheck()
        {
            MogoWorld.RegisterUpdate<MusicTrigerUpdateDelegate>("MusicTriger.CheckMusic", CheckMusic);
        }

        private void StopCheck()
        {
            MogoWorld.UnregisterUpdate("MusicTriger.CheckMusic", CheckMusic);
        }


        public MonsterType CheckMonsterType()
        {
            MonsterType retMonsterType = MonsterType.NONE;
            if(_isCheckBossMusic == true)
            {
                retMonsterType = CheckMonster(MonsterType.BOSS, MonsterType.BOSS);
            }
            if (_isCheckMonsterMusic)
            {
                if (retMonsterType == MonsterType.NONE)
                {
                    retMonsterType = CheckMonster(MonsterType.BIG_MONSTER, MonsterType.SMALL_MONSTER);
                }
            }
            return retMonsterType;
        }

        private MonsterType CheckMonster(MonsterType targetType1, MonsterType targetType2)
        {
            foreach (var entity in MogoWorld.Entities)
            {
                if (entity.Value is EntityMonsterBase)
                {
                    EntityMonsterBase monster = entity.Value as EntityMonsterBase;
                    var monsterType = monster_helper.GetMonsterType((int)monster.monster_id);
                    int pveType = monster.GetCampPveType();
                    if ((monsterType == (int)targetType1 || monsterType == (int)targetType2)
                        && pveType != PlayerAvatar.Player.GetCampPveType())
                    {
                        return targetType1;
                    }
                } 
            }
            return MonsterType.NONE;
        }

        public void Stop()
        {
            StopCheck();
        }



    }
}
