﻿using Game.Asset;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// 加状态的目的
    /// 当要随机出播放一个音乐时候，音乐在加载还没有播放，此时不用再随机
    /// </summary>
    public enum MusicStatus
    {
        IDLE = 0,
        READY
    }
    public class BgMusicInfo
    {
        private Action _fadeOutFinishedACT = null;
        private Action _fadeInFinishedACT = null;

        private AudioSource _audioSource = null;
        public AudioSource audioSource
        {
            get { return _audioSource; }
            set { _audioSource = value; }
        }

        private MusicStatus _status;
        public MusicStatus status
        {
            get {return _status;}
            set
            {
                _status = value;
            }

        }

        private music _music;

        private bool _isRecover = false;
        public bool isRecover
        {
            get { return _isRecover; }
            set { _isRecover = value; }
        }

        public int ID
        {
            get 
            {
                //LoggerHelper.Error("[BgMusicInfo:ID]=>1_________________music:   " + _music);
                return _music.__id; 
            }
        }

        public float volume
        {
            get 
            { 
                return _audioSource.volume; 
            }
            set 
            {
                _audioSource.volume = value;
            }
        }

        private float _volumeBak = 0f;

        private MusicTween _musicTween;
        public BgMusicInfo()
        {
            _musicTween = new MusicTween();
            _status = MusicStatus.IDLE;
            _isRecover = false;
        }

        public void Stop()
        {
            if (_audioSource == null) return;
            _audioSource.Stop();

        }

        public void Play()
        {
            if (_audioSource == null) return;
                _audioSource.volume = PlayerDataManager.Instance.SettingData.GetSettingValue(Common.Data.SettingType.Music) / 100f;
                //LoggerHelper.Error("20______________________audioSource.volume:" + _audioSource.volume);
            _audioSource.Play();
        }

        public float GetSettingVolume()
        {
            return PlayerDataManager.Instance.SettingData.GetSettingValue(Common.Data.SettingType.Music) / 100f;
        }

        public bool IsPlaying()
        {
            if (_audioSource == null) return false;
            return _audioSource.isPlaying;
        }

        public void SetInfo(music setMusic)
        {
            this._music = setMusic;
        }

        public void SetAudioClip(music musicData,Action callBack)
        {
            ObjectPool.Instance.GetAudioClip(musicData.__path, (clip) =>
            {
                audioSource.clip = clip;
                //LoggerHelper.Error("[BgMusicInfo:SetAudioClip]=>musicData.__path:   " + musicData.__path);
                callBack();
            });
        }

        public void SetClipAndFadeIn(music musicData, float fadeTime)
        {
            SetAudioClip(musicData, () =>
            {
                FadeIn(fadeTime);
            });
        }

        #region 淡入淡出
        public void FadeOut(float fadeTime,Action fadeOutFinishedACT = null)
        {
            if (_audioSource == null) return;
            this._fadeOutFinishedACT = fadeOutFinishedACT;
            _musicTween.Fade(_audioSource.volume, 0, fadeTime, 20, FadeOutFinished, FadeOutInterval); 
        }

        /// <summary>
        /// 淡出采样回调,间隔一段时间回调一次(采样频率)
        /// </summary>
        /// <param name="volume"></param>
        private void FadeOutInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
        }

        /// <summary>
        /// 淡出结束回调
        /// </summary>
        /// <param name="volume"></param>
        private void FadeOutFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            _audioSource.Stop();
            if (_fadeOutFinishedACT != null)
            {
                _fadeOutFinishedACT();
            }
            Recover();
        }

        private void Recover()
        {
            if (_isRecover == false) return;
            if (_audioSource != null)
            {
                _audioSource.clip = null;
            }
            //if (_music == null) return;
            //Game.Asset.ObjectPool.Instance.Release(_music.__path);
        }

        public void FadeIn(float fadeTime, Action fadeinFinishedACT = null)
        {
            if (_audioSource == null) return;
            this._fadeInFinishedACT = fadeinFinishedACT;
            _musicTween.Fade(0, PlayerDataManager.Instance.SettingData.GetSettingValue(Common.Data.SettingType.Music) / 100f, 
                fadeTime, 20, FadeInFinished, FadeInInterval);
            _audioSource.volume = 0;
            _audioSource.Play();
        }

        /// <summary>
        /// 淡入采样回调,间隔一段时间回调一次(采样频率)
        /// </summary>
        /// <param name="volume"></param>
        private void FadeInInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
        }

        /// <summary>
        /// 淡入结束回调
        /// </summary>
        /// <param name="volume"></param>
        private void FadeInFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            if (_fadeInFinishedACT != null)
            {
                _fadeInFinishedACT();
            }
        }
        #endregion

        public void TempCloseVolume()
        {
            if (_audioSource == null) return;
            _volumeBak = _audioSource.volume;
            _audioSource.volume = 0f;

        }

        public void ResetTempVolume()
        {
            if (_audioSource == null) return;
            //_audioSource.volume = _volumeBak;
            _audioSource.volume = GetSettingVolume();
        }

    }
}
