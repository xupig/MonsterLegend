﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.GlobalManager
{
    /// <summary>
    /// 具备MusicTween功能
    /// </summary>
    public class MogoAudioSource
    {
        private AudioSource _audioSource = null;
        private MusicTween _musicTween = null;

        private Action _fadeOutFinishedACT = null;
        private Action _fadeInFinishedACT = null;

        public MogoAudioSource(AudioSource audioSource)
        {
            this._audioSource = audioSource;
        }

        private void CheckMusicTween()
        {
            if (_musicTween == null) _musicTween = new MusicTween();
        }

        #region 淡出
        public void FadeOut(float fadeTime, Action fadeOutFinishedACT = null)
        {
            if (_audioSource == null)
            {
                LoggerHelper.Error("[MogoAudioSource:FadeOut]=>_audioSource == null!");     
                return;
            }
            CheckMusicTween();

            this._fadeOutFinishedACT = fadeOutFinishedACT;
            _musicTween.Fade(_audioSource.volume, 0, fadeTime, 20, FadeOutFinished, FadeOutInterval);
        }

        private void FadeOutInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
        }

        private void FadeOutFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            _audioSource.Stop();
            if (_fadeOutFinishedACT != null)
            {
                _fadeOutFinishedACT();
            }
        }
        #endregion

        #region 淡入
        public void FadeIn(float fadeTime, Action fadeinFinishedACT = null)
        {
            if (_audioSource == null)
            {
                LoggerHelper.Error("[MogoAudioSource:FadeOut]=>_audioSource == null!");
                return;
            }

            CheckMusicTween();
            this._fadeInFinishedACT = fadeinFinishedACT;
            _musicTween.Fade(0, PlayerDataManager.Instance.SettingData.GetSettingValue(Common.Data.SettingType.Music) / 100f,
                fadeTime, 20, FadeInFinished, FadeInInterval);
            _audioSource.volume = 0;
            _audioSource.Play();
        }

        /// <summary>
        /// 淡入采样回调,间隔一段时间回调一次(采样频率)
        /// </summary>
        /// <param name="volume"></param>
        private void FadeInInterval(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
        }

        /// <summary>
        /// 淡入结束回调
        /// </summary>
        /// <param name="volume"></param>
        private void FadeInFinished(float volume)
        {
            if (_audioSource == null) return;
            _audioSource.volume = volume;
            if (_fadeInFinishedACT != null)
            {
                _fadeInFinishedACT();
            }
        } 
        #endregion

        /// <summary>
        /// 直接播放
        /// </summary>
        public virtual void Play()
        {
            if (_audioSource == null) return;
            _audioSource.volume = PlayerDataManager.Instance.SettingData.GetSettingValue(Common.Data.SettingType.Music) / 100f;
            _audioSource.Play();
        }

        public void Stop()
        {
            if (_audioSource == null) return;
            _audioSource.Stop();
        }

        public void Pause()
        {
            if (_audioSource == null) return;
            _audioSource.Pause();
        }

        

    }
}
