﻿using Game.Asset;
using GameData;
using Mogo.Util;
using System;
using System.Collections.Generic;


namespace GameMain.GlobalManager
{
    public class LightMusic  : BgMusicInfo
    {
        private float _lightMusicRndNum = 0;
        private music musicData;
        public void RandomPlay()
        {
            int curMapID = GameSceneManager.GetInstance().curMapID;
            musicData = music_helper.GetInstance().GetBgMusicData(curMapID);
            //UnityEngine.Debug.LogError("11___________________________musicData.__type:  " + musicData.__type);
            //if (musicData.__type != 0) return;  //不是轻音乐就不随机
            if (musicData == null) return;

            bool isPlaying = IsPlaying();
            if (isPlaying == false)
            {
                if (_lightMusicRndNum == 0)
                {
                    _lightMusicRndNum = 1;
                    uint time = (uint)RandomHelper.GetRandomInt(3000, 6000);
                    GameLoader.Utils.Timer.TimerHeap.AddTimer(time, 0, () =>
                    {
                        _lightMusicRndNum = 0;
                        //重新播放轻音乐                                      
                        if (IsPlaying() == false)
                        {
                            //GameLoader.Utils.LoggerHelper.Error("[LightMusic:RandomPlay]=>time: " + time);               
                            //重新播放,这样的话就是副本结束了,定时器没有销毁继续播放也行,这两个判断会拦截掉
                            RandomGetClip(() =>
                                {
                                    Play();
                                });
                            
                        }
                    });
                }
            }
        }

        private void RandomGetClip(Action callBack)
        {
            if (audioSource == null) return;
            //GameLoader.Utils.LoggerHelper.Error("[LightMusic:RandomGetClip]=>1________musicData:  " + musicData + ",ID: " + audioSource.GetInstanceID());
            if (musicData == null) return;
            //GameLoader.Utils.LoggerHelper.Error("[LightMusic:RandomGetClip]=>2________musicData.__path:  " + musicData.__path + ",ID: " + audioSource.GetInstanceID());
            ObjectPool.Instance.GetAudioClip(musicData.__path, (clip) =>
            {
                audioSource.clip = clip;
                callBack();
            });
        }
    }
}
