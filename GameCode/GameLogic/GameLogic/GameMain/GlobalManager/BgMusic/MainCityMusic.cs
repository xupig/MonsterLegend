﻿#region 模块信息
/*==========================================
// 模块名：MainCityMusic
// 命名空间: GameMain.GlobalManager
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/07/27
// 描述说明：主城区域触发音乐
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Game.Asset;
using GameData;
using GameLoader.Utils;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class MainCityMusicUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class MainCityMusic
    {
        private static float _fadeTime = 3f;     //淡出时间
        private MusicTween _musicTween;


        public MainCityMusic()
        {
            _musicTween = new MusicTween();
            MogoWorld.RegisterUpdate<MainCityMusicUpdateDelegate>("MainCityMusic.Update", Update);
        }

        public void PlayAreaTriggerMusic(int vocationMusicID)
        {
            if (!GameSceneManager.GetInstance().IsInCity()) return;

            music preFadeInMusicData = music_helper.GetInstance().GetBgMusicByMusicID(vocationMusicID);
            BgMusicInfo music = GetAudioSource(preFadeInMusicData);

            if (music == null) 
            {
                LoggerHelper.Error("[MainCityMusic:PlayAreaTriggerMusic]=>music == null");
                return;
            }
            //
            if (vocationMusicID == music.ID) return;    //重复的背景音乐不播放
            if (music.IsPlaying() == true)
            {
                 //在播放就把原来的淡出
                music.FadeOut(_fadeTime, () =>
                {
                    music.SetClipAndFadeIn(preFadeInMusicData, _fadeTime);
                });
            }
            else          
            {
                //没有播放直接淡入播放
                music.SetClipAndFadeIn(preFadeInMusicData, _fadeTime);
            }

        }

        private BgMusicInfo GetAudioSource(music musicData)
        {
            return BgMusicManager.Instance.GetSpareAudioSource(musicData,true);
        }

        private void Update()
        {
            //只有轻音乐才检查循环
            BgMusicAudioSource.Instance.RandomPlayLightMusic();
        }


    }
}
