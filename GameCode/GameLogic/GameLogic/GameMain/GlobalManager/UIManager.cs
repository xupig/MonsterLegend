﻿#region 模块信息
/*==========================================
// 模块名：UIManager
// 命名空间: GameMain.GlobalManager
// 创建者：Ari
// 修改者列表：YZ, LYX
// 创建日期：2014/12/08
// 描述说明：UI控制
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Audio;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.Asset;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameMain.GlobalManager
{
    public class UIManager
    {
        public const float PANEL_WIDTH = 1280;
        public const float PANEL_HEIGHT = 720;

        public static float CANVAS_WIDTH = 1280;
        public static float CANVAS_HEIGHT = 720;

        private static Vector2 m_startPosition = Vector2.zero;

        private static UIManager m_instance;

        public static float scale = 0;

        private HashSet<PanelIdEnum> ClosingViewSet;
        //private static List<MogoUILayer> ExclusiveLayer = new List<MogoUILayer>() { MogoUILayer.LayerUIPanel, MogoUILayer.LayerSecondUIPanel, MogoUILayer.LayerThirdUIPanel };
        public static int UI_LAYER = UnityEngine.LayerMask.NameToLayer("UI");
        public static int TransparentFX_LAYER = UnityEngine.LayerMask.NameToLayer("TransparentFX");

        public static UIManager Instance
        {
            get
            {
                if (m_instance == null) m_instance = new UIManager();
                return m_instance;
            }
        }

        private EventSystem _eventSystem;
        private GameObject m_uiRoot;
        private GameObject m_uiCamera;
        private GameObject m_uiAudio;
        private GameObject m_underlayPlane;

        private UIAudioManager _audioManager;
        private Camera camera;

        private static RectTransform rect;
        private static CanvasScaler scaler;

        public GameObject UIRoot
        {
            get { return m_uiRoot; }
        }

        public Camera UICamera
        {
            get { return camera; }
        }

        public UIManager()
        {
            ClosingViewSet = new HashSet<PanelIdEnum>();
            InitUIAssetProvider();
            CreateEventSystem();
            CreateUICamera();
            CreateUIRoot();
            CreateUIAudio();
            CreateUnderlay();
            UILayerManager.CreateUILayers(m_uiRoot);
            AddSdkInteractiveComponent();
        }

        private void InitUIAssetProvider()
        {
            ImageWrapper.GetMaterial = ObjectPool.Instance.GetAssemblyObject;
            ImageWrapper.GetSprite = ObjectPool.Instance.GetAssemblyObject;
            TextWrapper.GetFont = ObjectPool.Instance.GetAssemblyObject;
            TextWrapper.GetLanguage = MogoLanguageUtil.GetContent;
        }

        private void CreateUICamera()
        {
            GameObject go = new GameObject("UICamera");
            camera = go.AddComponent<Camera>();
            camera.clearFlags = CameraClearFlags.Depth;
            camera.orthographic = true;
            int layer = UI_LAYER;
            camera.cullingMask = 1 << layer;
            go.transform.position = new Vector3(1000, 1000, 1000);
            GameObject.DontDestroyOnLoad(go);
            m_uiCamera = go;
        }

        private BlurColorRimComponent _blurColorRimeComponent;
        private void CreateUnderlay()
        {
            m_underlayPlane = new GameObject("Container_Underlay");
            m_underlayPlane.AddComponent<Canvas>();
            m_underlayPlane.AddComponent<GraphicRaycaster>();
            m_underlayPlane.AddComponent<SortOrderedRenderAgent>();
            m_underlayPlane.SetActive(false);
            m_underlayPlane.transform.SetParent(m_uiRoot.transform);
            m_underlayPlane.transform.localPosition = Vector3.zero;
            m_underlayPlane.layer = UI_LAYER;
            _blurColorRimeComponent = m_underlayPlane.AddComponent<BlurColorRimComponent>();
        }

        private void CreateUIRoot()
        {
            GameObject go = new GameObject("UIRoot");
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                go.AddComponent<ShortcutKeyManager>();
            }
            Canvas canvas = go.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = m_uiCamera.GetComponent<Camera>();
            canvas.planeDistance = 100.0f;
            canvas.pixelPerfect = false;
            canvas.scaleFactor = CalculateCanvasScaleFactor();
            go.AddComponent<GraphicRaycaster>();
            go.AddComponent<SortOrderedRenderAgent>();
            go.layer = UI_LAYER;
            scale = 10.0f / (float)Screen.height * canvas.scaleFactor;
            rect = go.GetComponent<RectTransform>();
            scaler = go.AddComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            scaler.referenceResolution = new Vector2(PANEL_WIDTH, PANEL_HEIGHT);
            OnResize();
            go.AddComponent<UIResizer>();
            GameObject.DontDestroyOnLoad(go);
            m_uiRoot = go;
        }

        public static void OnResize()
        {
            CalculateCanvasScaleFactor();
            CANVAS_WIDTH = Convert.ToInt32(rect.sizeDelta.x);
            CANVAS_HEIGHT = Convert.ToInt32(rect.sizeDelta.y);
            if (CANVAS_WIDTH / CANVAS_HEIGHT >= PANEL_WIDTH / PANEL_HEIGHT)
            {
                scaler.matchWidthOrHeight = 1;
            }
            else
            {
                scaler.matchWidthOrHeight = 0;
            }
        }

        //public void OnResizeAndRefreshBlurPanel()
        //{
            //if (blurPanel != PanelIdEnum.Empty && _blurColorRimeComponent != null && _blurColorRimeComponent.IsCreated() == false)
            //{
                // RefreshBlurUnderlay(blurPanel);
            //}
        //}

        private void CreateUIAudio()
        {
            GameObject go = new GameObject("UIAudio");
            GameObject.DontDestroyOnLoad(go);
            m_uiAudio = go;
            _audioManager = go.AddComponent<UIAudioManager>();
            EnableAudio = true;
            KComponentAudioCollective.audioVolume = 1f;
        }

        private void AddSdkInteractiveComponent()
        {
            GameObject layerMainGo = UILayerManager.GetUILayerTransform(MogoUILayer.LayerUIMain).gameObject;
            layerMainGo.AddComponent<VoiceDriver>();
            layerMainGo.AddComponent<HalfScreenInputDriver>();
        }

        public void PlayAudio(string audioPath)
        {
            _audioManager.PlayAudio(audioPath);
        }

        private PanelIdEnum blurPanel = PanelIdEnum.Empty;
        public void ShowBlurUnderlay(BasePanel panel)
        {
            if(change_map_flag)
            {
                return;
            }
            blurPanel = panel.ID;
            m_underlayPlane.transform.SetParent(panel.transform.parent,false);
            m_underlayPlane.transform.SetSiblingIndex(panel.transform.GetSiblingIndex());
            m_underlayPlane.SetActive(true);
            SortOrderedRenderAgent.RebuildAll();
        }

        public void RefreshBlurUnderlay(PanelIdEnum panelId)
        {
            m_underlayPlane.SetActive(true);
            _blurColorRimeComponent.Execute(panelId);
            m_underlayPlane.SetActive(false);
        }

        public void HideBlurUnderlay()
        {
            blurPanel = PanelIdEnum.Empty;
            m_underlayPlane.SetActive(false);
        }

        public bool EnableAudio
        {
            get
            {
                return _audioManager.EnableAudio;
            }
            set
            {
                _audioManager.EnableAudio = value;
            }
        }

        private static float CalculateCanvasScaleFactor()
        {
            float scaleWidth = (float)Screen.width / PANEL_WIDTH;
            float scaleHeight = (float)Screen.height / PANEL_HEIGHT;
            Global.Scale = scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
            if (Global.Scale == scaleWidth)
            {
                m_startPosition = new Vector2(0, -(Screen.height / Global.Scale - PANEL_HEIGHT) * 0.5f);
            }
            else
            {
                m_startPosition = new Vector2((Screen.width / Global.Scale - PANEL_WIDTH) * 0.5f, 0);
            }
            return Global.Scale;
        }

        private void CreateEventSystem()
        {
            GameObject go = new GameObject("EventSystem");
            _eventSystem = go.AddComponent<EventSystem>();
            _eventSystem.sendNavigationEvents = false;
            _eventSystem.pixelDragThreshold = 15;
            go.AddComponent<StandaloneInputModule>();
            go.AddComponent<TouchInputModule>();
            GameObject.DontDestroyOnLoad(go);
        }

        public bool IsPointerOverGameObject
        {
            get
            {
                return _eventSystem.IsPointerOverGameObject();
            }
        }

        public static Vector2 GetUIStartPosition()
        {
            return m_startPosition;
        }

        public void DestroyLayer(MogoUILayer layer)
        {

        }

        public void DestroyPanel(PanelIdEnum panelId)
        {
            EventDispatcher.TriggerEvent(BaseModule.GetDestroyPanelCanvasEventName(panelId), panelId);
        }

        public string GetPanelPrefabPath(PanelIdEnum panelId)
        {
            if (BaseModule.PanelPrefabPathDict.ContainsKey(panelId))
            {
                return BaseModule.PanelPrefabPathDict[panelId];
            }
            return string.Empty;
        }

        /// <summary>
        /// 有全屏界面打开着
        /// </summary>
        /// <returns></returns>
        public static bool HasFullScreenPanel()
        {
            return BaseModule.HasFullScreenPanel();
        }

        #region 界面打开、关闭操作

        #region 变量
        PanelIdEnum m_strCurrentUIName = PanelIdEnum.Empty;
        public PanelIdEnum CurrentUIName
        {
            get
            {
                return m_strCurrentUIName;
            }
        }
        #endregion


        public void ShowPanel(PanelIdEnum panelId, object data = null)
        {
            if (panelId == PanelIdEnum.Empty)
            {
                return;
            }
            if(BaseModule.IsPanelMutexWithMainUILayer(panelId))//全屏界面停止寻路
            {
                PlayerCommandManager.GetInstance().OnHideMainCamera(); 
            }
            BaseModule.CloseMutexPanel(panelId);
            BaseModule.CloseAllLoadingMutexPanel(panelId);
            DoShowView(panelId, data);
        }

        public void ClosePanel(PanelIdEnum panelId,bool isExclusive = false)
        {
            //MogoUtils.PrintLog("ClosePanel:" + panelId);
            if(panelId == PanelIdEnum.Empty)
            {
                return;
            }
            if (change_map_flag)
            {
                DoCloseView(panelId, change_map_flag);
            }
            else
            {
                DoCloseView(panelId, isExclusive);
            }
        }
        public static bool change_map_flag = false;//切换地图期间不能EnableCanvas
        public void ResetAllLayer()
        {
            //Debug.LogError("ResetAllLayer");
            change_map_flag = true;
            layerVisibleDict.Clear();
            CameraManager.GetInstance().ShowMainCamera();
            HideBlurUnderlay();
        }

        public void ShowAllLayer()
        {
            Array _arrEnum = Enum.GetValues(typeof(MogoUILayer));
            for (int i = 0; i < _arrEnum.Length; i++)
            {
                ShowLayer((MogoUILayer)_arrEnum.GetValue(i));
            }
        }

        public void HideAllLayer()
        {
            Array _arrEnum = Enum.GetValues(typeof(MogoUILayer));
            for (int i = 0; i < _arrEnum.Length; i++)
            {
                HideLayer((MogoUILayer)_arrEnum.GetValue(i));
            }
            HideBlurPanel();
        }

        public void ShowOtherLayer(MogoUILayer _layer)
        {
            Array _arrEnum = Enum.GetValues(typeof(MogoUILayer));
            int layerIndex = (int)_layer;
            for (int i = 0; i < _arrEnum.Length; i++)
            {
                if (layerIndex != (int)_arrEnum.GetValue(i))
                {
                    ShowLayer((MogoUILayer)_arrEnum.GetValue(i));
                }
            }
        }

        public void HideOtherLayer(MogoUILayer _layer)
        {
            Array _arrEnum = Enum.GetValues(typeof(MogoUILayer));
            int layerIndex = (int)_layer;
            for (int i = 0; i < _arrEnum.Length; i++)
            {
                if (layerIndex != (int)_arrEnum.GetValue(i))
                {
                    HideLayer((MogoUILayer)_arrEnum.GetValue(i));
                }
            }
            if(_layer == MogoUILayer.LayerHideOther)
            {
                HideBlurPanel();
            }
        }

        private void HideBlurPanel()
        {
            if(blurPanel == PanelIdEnum.Empty)
            {
                return;
            }
            if (BaseModule.IsBlurUnderlay(blurPanel))
            {
                m_underlayPlane.SetActive(false);
            }
            if (BaseModule.IsPanelMutexWithMainUILayer(blurPanel))
            {
                CameraManager.GetInstance().ShowMainCameraWithOutAI();
            }
        }

        private static Dictionary<MogoUILayer, bool> layerVisibleDict = new Dictionary<MogoUILayer, bool>(new MogoUILayerComparer());

        public void HideLayer(MogoUILayer layer)
        {
            if (layer != MogoUILayer.LayerEffect && layer != MogoUILayer.LayerGuide && layer != MogoUILayer.LayerHideOther && layer != MogoUILayer.LayerUITop)
            {
                if (BaseModule.layerOpenedPanelSet.ContainsKey(layer))
                {
                    HashSet<PanelIdEnum> panelSet = BaseModule.layerOpenedPanelSet[layer];
                    foreach (PanelIdEnum panelId in panelSet)
                    {
                        DisablePanelCanvas(panelId);
                    }
                    EventDispatcher.TriggerEvent(LayerEvents.HideLayer, layer);
                }
                if (layerVisibleDict.ContainsKey(layer) == false)
                {
                    layerVisibleDict.Add(layer, false);
                }
                else
                {
                    layerVisibleDict[layer] = false;
                }
                BaseModule.CloseLayerLoadingPanel(layer);
            }
        }

        public static bool GetLayerVisibility(MogoUILayer _layer)
        {
            if (layerVisibleDict.ContainsKey(_layer))
            {
                return layerVisibleDict[_layer];
            }
            return true;
        }

        public void ShowLayer(MogoUILayer layer)
        {
            if (layer != MogoUILayer.LayerEffect && layer != MogoUILayer.LayerGuide && layer != MogoUILayer.LayerHideOther && layer != MogoUILayer.LayerUITop)
            {
                if(layer == MogoUILayer.LayerUIMain || layer == MogoUILayer.LayerUnderPanel)
                {
                    if (BaseModule.CanShowMainUILayer() == false)
                    {
                        return;
                    }
                }
                
                if (layerVisibleDict.ContainsKey(layer) == false)
                {
                    layerVisibleDict.Add(layer, true);
                }
                else
                {
                    layerVisibleDict[layer] = true;
                }
                if (layer != MogoUILayer.LayerUIPanel && layer != MogoUILayer.LayerSpecialUIPanel && layer != MogoUILayer.LayerUIPopPanel)
                {
                    if (BaseModule.layerOpenedPanelSet.ContainsKey(layer))
                    {
                        HashSet<PanelIdEnum> panelSet = BaseModule.layerOpenedPanelSet[layer];
                        foreach (PanelIdEnum panelId in panelSet)
                        {
                            EnablePanelCanvas(panelId);
                        }
                    }
                }
            }
        }

        public void EnablePanelCanvas(PanelIdEnum panelId)
        {
            if (change_map_flag)
            {
                return;
            }
            EventDispatcher.TriggerEvent(BaseModule.GetEnablePanelCanvasEventName(panelId), panelId);
        }

        public void DisablePanelCanvas(PanelIdEnum panelId)
        {
            EventDispatcher.TriggerEvent(BaseModule.GetDisablePanelCanvasEventName(panelId), panelId);
        }


        private void DoCloseView(PanelIdEnum panelId, bool isExclusive = false)
        {
            m_strCurrentUIName = PanelIdEnum.Empty;

            EventDispatcher.TriggerEvent(BaseModule.GetCloseEventName(panelId), panelId, isExclusive);
            EventDispatcher.TriggerEvent(PanelEvents.CLOSE_PANEL, panelId);
        }

        private void DoShowView(PanelIdEnum panelId, object data)
        {
            m_strCurrentUIName = panelId;
            EventDispatcher.TriggerEvent(BaseModule.GetShowEventName(panelId), panelId, data);
        }

        #endregion
    }

    public class ShortcutKeyManager : MonoBehaviour
    {
        private Dictionary<KeyCode, PanelIdEnum> m_shortcutDict;

        protected void Awake()
        {
            m_shortcutDict = new Dictionary<KeyCode, PanelIdEnum>();
            //m_shortcutDict.Add(KeyCode.LeftArrow, PanelIdEnum.Chat);
            //m_shortcutDict.Add(KeyCode.B, PanelIdEnum.Equip);
            //m_shortcutDict.Add(KeyCode.RightArrow,PanelName.Role);
            //m_shortcutDict.Add(KeyCode.A, PanelName.Gem);
            //for test, 打开交易市场

        }

        public static bool onLine = false;//临时用来处理没登陆就可以使用快捷键的问题

        protected void Update()
        {
            if (!onLine)
            {
                return;
            }
            foreach(KeyValuePair<KeyCode, PanelIdEnum> kvp in m_shortcutDict)
            {
                KeyCode code = kvp.Key;
                if (Input.GetKeyDown(code) == true)
                {
                    if (UIManager.Instance.CurrentUIName == m_shortcutDict[code])
                    {
                        UIManager.Instance.ClosePanel(m_shortcutDict[code]);
                    }
                    else
                    {
                        UIManager.Instance.ShowPanel(m_shortcutDict[code]);
                    }
                }
            }
        }
    }
}


