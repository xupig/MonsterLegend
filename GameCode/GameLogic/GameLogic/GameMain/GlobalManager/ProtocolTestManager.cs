﻿
using Common.ServerConfig;
using GameLoader.Utils.CustomType;
//using GameMain.GlobalManager.SubSystem;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
namespace GameMain.GlobalManager
{
    public class ProtocolTestManager
    {
        private static ProtocolTestManager s_instance = null;
        public static ProtocolTestManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new ProtocolTestManager();
            }
            return s_instance;
        }

        private const string HELP_COMMAND = "help";
        public void RPCCall(string methodName, string args)
        {
            if (args.Equals(HELP_COMMAND))
            {
                RPCCallHelp(methodName);
            }
            else
            {
                string[] argList = args.Split(';');
                RPCCall(methodName, argList);
            }
        }

        private void RPCCallHelp(string methodName)
        {
            EntityDefMethod method = GetMethod(methodName);
            string outputCommand = string.Empty;
            if (method == null)
            {
                outputCommand = string.Format("ERROR: rpc {0} is not a valid method.", methodName);
            }
            else
            {
                outputCommand = string.Format("@rpc {0} ", methodName);

                for (int i = 0; i < method.ArgsType.Count; ++i)
                {
                    if (i < method.ArgsType.Count - 1)
                    {
                        outputCommand = string.Concat(outputCommand, method.ArgsType[i].VValueType.Name, ";");
                    }
                    else
                    {
                        outputCommand = string.Concat(outputCommand, method.ArgsType[i].VValueType.Name);
                    }
                }
            }
            SendChatMsg(outputCommand);
        }

        private EntityDefMethod GetMethod(string methodName)
        {
            Dictionary<string, EntityDefMethod> baseMethodsByName = Pluto.CurrentEntity.BaseMethodsByName;
            return baseMethodsByName.ContainsKey(methodName) ? baseMethodsByName[methodName] : null;
        }

        private void RPCCall(string methodName, string[] args)
        {
            EntityDefMethod method = GetMethod(methodName);
            string outputCommand = string.Empty;
            if (method == null)
            {
                outputCommand = string.Format("ERROR: rpc {0} is not a valid method.", methodName); 
                SendChatMsg(outputCommand);
                return;
            }
            if (args.Length != method.ArgsType.Count)
            {
                outputCommand = string.Format("ERROR: rpc {0} parameters do not match protocol.", methodName);
                SendChatMsg(outputCommand);
                return;
            }

            outputCommand = string.Format("@rpc {0} ", methodName);
            List<object> argList = new List<object>();
            object value = null;
            for (int i = 0; i < args.Length; ++i)
            {
                value = ParseValue(method.ArgsType[i].VValueType, args[i]);
                if (value == null)
                {
                    outputCommand = string.Format("ERROR: rpc {0} has an unvalid parameter {1}.", methodName, args[1]);
                    SendChatMsg(outputCommand);
                    return;
                }
                argList.Add(value);
                if (i < method.ArgsType.Count - 1)
                {
                    outputCommand = string.Concat(outputCommand, args[i], ";");
                }
                else
                {
                    outputCommand = string.Concat(outputCommand, args[i]);
                }
            }
            object[] argArray = argList.ToArray();
            ServerProxy.Instance.RpcCall(methodName, argArray);
        }

        private object ParseValue(Type type, string valueStr)
        {
            object value = null;
            try
            {
                if (type == typeof(Boolean))
                {
                    value = Boolean.Parse(valueStr);
                }
                else if (type == typeof(Double))
                {
                    value = Double.Parse(valueStr);
                }
                else if (type == typeof(object))
                {
                    value = valueStr;
                }
                else if (type == typeof(float))
                {
                    value = float.Parse(valueStr);
                }
                else if (type == typeof(Int16))
                {
                    value = Int16.Parse(valueStr);
                }
                else if (type == typeof(Int32))
                {
                    value = Int32.Parse(valueStr);
                }
                else if (type == typeof(Int64))
                {
                    value = Int64.Parse(valueStr);
                }
                else if (type == typeof(sbyte))
                {
                    value = sbyte.Parse(valueStr);
                }
                else if (type == typeof(LuaTable))
                {
                    string valueString = valueStr.Replace("{", "");
                    valueString = valueString.Replace("}", "");
                    string[] values = valueString.Split(',');
                    LuaTable result = new LuaTable();
                    foreach (string val in values)
                    {
                        string[] vals = val.Split(':');
                        result.Add(int.Parse(vals[0]), Parse(vals[1]));
                    }
                    value = result;
                }
                else if (type == typeof(String))
                {
                    value = valueStr;
                }
                else if (type == typeof(UInt16))
                {
                    value = UInt16.Parse(valueStr);
                }
                else if (type == typeof(UInt32))
                {
                    value = UInt32.Parse(valueStr);
                }
                else if (type == typeof(UInt64))
                {
                    value = UInt64.Parse(valueStr);
                }
                else if (type == typeof(byte))
                {
                    value = byte.Parse(valueStr);
                }
            }
            catch (Exception ex)
            {
                value = null;
            }
            return value;
        }

        private object Parse(string value)
        {
            if (value.IndexOf("s") == 0)
            {
                return value.Substring(1);
            }
            return ulong.Parse(value);
        }

        private void SendChatMsg(string msg)
        {
            //Ari ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_WORLD, msg);
        }
    }
}
