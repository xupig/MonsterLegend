﻿using Game.Asset;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TestAirVehiceGate : ITest
    {
        private List<Transform> _sceneComponentList = null;
        private GameObject _oldscene = null;
        private GameObject _newscene = null;

        public void Update()
        {
            

        }


        public void ReplaceScene(string sourceScene,string targetSceneAllPath,int cmd = 0)
        {
            if (_oldscene ==null)
            {
                _oldscene = GameObject.Find(sourceScene);
            }
            if (_oldscene == null) return;
            
            //Transform mesh = source.transform.FindChild("Mesh");
            //mesh.gameObject.SetActive(false);

            if (string.IsNullOrEmpty(targetSceneAllPath)) return;
            LoadScene(targetSceneAllPath, (gameObject) =>
                {
                    SetSceneInfo(gameObject, cmd);
                });
            

        }

        private void LoadScene(string targetSceneAllPath, Action<GameObject> callback)
        {
            if (_newscene == null)
            {
                ObjectPool.Instance.GetGameObject(targetSceneAllPath, (gameObject) =>
                {
                    gameObject.transform.position = _oldscene.transform.position;
                    gameObject.transform.rotation = _oldscene.transform.rotation;
                    _oldscene.SetActive(false);
                    _newscene = gameObject;
                    callback(_newscene);
                });
            }
            else
            {
                callback(_newscene);
            }
            
        }

        private void SetSceneInfo(GameObject gameObject, int cmd)
        {
            if (_sceneComponentList == null)
            {
                _sceneComponentList = new List<Transform>();
                Transform mesh = gameObject.transform.FindChild("Mesh");
                for (int i = 0; i < 7; i++)
                {
                    Transform go = mesh.FindChild("GameObject" + (i + 1).ToString());
                    go.gameObject.SetActive(false);
                    _sceneComponentList.Add(go);
                }

            }
            //LoggerHelper.Error("[SetSceneInfo]=>1_______________cmd:    " + cmd);

            if (cmd == 0) return;
            if (_sceneComponentList == null) return;
            _sceneComponentList[cmd-1].gameObject.SetActive(true);



        }


    }
}
