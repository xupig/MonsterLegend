﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using GameMain.Entities;
using MogoEngine;

namespace GameMain.GlobalManager
{

    public class GameStatisticsManager
    {
        private static GameStatisticsManager s_instance = null;
        public static GameStatisticsManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameStatisticsManager();
            }
            return s_instance;
        }

        GameStatisticsManager()
        {
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_ACCEPTED, OnAcceptTask);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.AddEventListener<int>(StatisticsEvent.ON_TRIGGER_GUIDE_OR_CG, OnTriggerGuideOrCG);
        }

        void OnAcceptTask(int taskID)
        {
            int newStep = dict_link_step_helper.GetStep(1, taskID.ToString());
            CheckNewStepInAvatar((UInt16)newStep);
        }

        void OnEnterMap(int mapID)
        {
            var instanceID = map_helper.GetInstanceIDByMapID(mapID);
            int newStep = dict_link_step_helper.GetStep(2, instanceID.ToString());
            CheckNewStepInAvatar((UInt16)newStep);
        }

        void OnTriggerGuideOrCG(int triggerID)
        {
            int newStep = dict_link_step_helper.GetStep(3, triggerID.ToString());
            CheckNewStepInAvatar((UInt16)newStep);
        }

        void CheckNewStepInAccount(UInt16 newStep)
        {
            var account = MogoWorld.Player as PlayerAccount;
            if (account == null) return;
            if (newStep > account.game_step)
            {
                account.game_step = newStep;
                LoggerHelper.Info(string.Format("[account.record_game_step_req:{0}]", newStep));
                account.RpcCall("record_game_step_req", account.game_step);
            }
        }

        void CheckNewStepInAvatar(UInt16 newStep)
        {
            var avatar = MogoWorld.Player as PlayerAvatar;
            if (avatar == null) return;
            if (newStep > avatar.game_step)
            {
                avatar.game_step = newStep;
                LoggerHelper.Info(string.Format("[avatar.record_game_step_req:{0}]", newStep));
                avatar.RpcCall("record_game_step_req", avatar.game_step);
            }
        }
    }
}
