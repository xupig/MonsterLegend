/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：PlayerActionManager
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：行为管理器
//==========================================*/

using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameMain.PlayerAction;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;

namespace GameMain.GlobalManager
{
    public class SyncPosManager
    {
        EntityCreature _owner;

        private uint _syncPosTimerID = 0;

        public SyncPosManager(EntityCreature owner) 
        {
            _owner = owner;
        }

        public void Release()
        {
            if (_syncPosTimerID > 0)
            {
                TimerHeap.DelTimer(_syncPosTimerID);
                _syncPosTimerID = 0;
            }
        }

        public void OnEnterFollower()
        {
            if (_syncPosTimerID == 0)
            {
                _syncPosTimerID = TimerHeap.AddTimer(1000, 300, SyncPos);
            }
        }

        public void OnLeaveFollower()
        {
            if (_syncPosTimerID > 0)
            {
                TimerHeap.DelTimer(_syncPosTimerID);
                _syncPosTimerID = 0;
            }
        }

        private Vector3 _prePos = new Vector3();
        public void SyncPos()
        {
            if (_owner.actor == null)
            {
                return;
            }
            Vector3 position = _owner.actor.position;
            if (Mathf.Abs(position.x - _prePos.x) < 3f && Mathf.Abs(position.z - _prePos.z) < 3f)
            {
                return;
            }
            _prePos.x = position.x;
            _prePos.z = position.z;
            System.Int16 x = (System.Int16)(position.x * RPCConfig.POSITION_SCALE);
            System.Int16 z = (System.Int16)(position.z * RPCConfig.POSITION_SCALE);
            MogoEngine.RPC.ServerProxy.Instance.OthersMove(_owner.id, (System.UInt16)x, (System.UInt16)z);
        }
    }
}