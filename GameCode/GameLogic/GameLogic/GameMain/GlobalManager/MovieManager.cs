﻿using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class MovieManager
    {
        private static MovieManager _instance;
        public static MovieManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new MovieManager();
                return _instance;
            }
        }

        private GameObject _movie;
        private MMT.MobileMovieTexture _movieTexture;
        private Action _onFinished;
        private bool _isPlaying = false;
        public void Play(string fileName, int loopCount, Action onFinished)
        {
            if (_isPlaying)
            {
                return;
            }
            _isPlaying = true;
            _onFinished = onFinished;
            if (_movie == null)
            {
                UnityEngine.Object obj = Resources.Load("Dialog/MovieCamera", typeof(GameObject));
                _movie = GameObject.Instantiate(obj) as GameObject;
                _movieTexture = _movie.transform.FindChild("MovieTexture").GetComponent<MMT.MobileMovieTexture>();
            }
            _movieTexture.Path = fileName;
            _movieTexture.LoopCount = loopCount;
            _movieTexture.onFinished += OnFinished;
            HideUICamera();
        }

        private void OnFinished(MMT.MobileMovieTexture sender)
        {
            Destroy();
            if (_onFinished != null)
            {
                _onFinished();
            }
        }

        public void Stop()
        {
            if (_isPlaying)
            {
                _movieTexture.Stop();
            }
        }

        public void Pause()
        {
            if (_isPlaying)
            {
                _movieTexture.Pause = true;
            }
        }

        public void Resume()
        {
            if (_isPlaying)
            {
                _movieTexture.Pause = false;
            }
        }

        public void Destroy()
        {
            if (_isPlaying)
            {
                _movieTexture = null;
                _isPlaying = false;
                GameObject.Destroy(_movie);
                _movie = null;
                ShowUICamera();
            }
        }

        private void ShowUICamera()
        {
            UIManager.Instance.UICamera.enabled = true;
        }

        private void HideUICamera()
        {
            UIManager.Instance.UICamera.enabled = false;
        }
    }
}
