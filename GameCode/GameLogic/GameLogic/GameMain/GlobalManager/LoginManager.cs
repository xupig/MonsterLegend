﻿using System;
using System.Collections.Generic;

using MogoEngine.Events;
using MogoEngine.RPC;
using GameLoader.Config;
using GameLoader.Utils;
using Common.Events;
using ServerInfo = GameLoader.Config.ServerInfo;

using UnityEngine;
using System.Net;
using GameLoader.PlatformSdk;
using Common;
using Common.ServerConfig;
using GameLoader.Utils.Timer;
using Common.Base;
using GameData;
using Common.Data;
using LitJson;
using GameMain.Entities;
using System.Collections;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// 登陆创号--服务器数据管理类
    /// </summary>
    public class LoginManager
    {
        /// <summary>
        /// 1、快速登陆和普通登陆的区别：
        ///    1-1、快速登陆成功后不会进入选角界面，直接到‘创号界面’或‘进入游戏’
        ///    1-2、普通登陆登陆成功后，必需先进入‘选角界面’，然后再到‘创号界面’或‘进入游戏’
        /// </summary>
        public bool isQuickLogin = false;
        /// <summary>
        /// 账号首次登陆状态[true:首次登录,false:非首次登录]
        /// </summary>
        //public bool isFristLogin = false;

        /// <summary>
        /// 最近登录服务器ID
        /// </summary>
        public int nearLoginServerId;
        public bool isInvokePlatformLogin = false;

        private int beginTime = 0;                                           
        private bool isLoging = false;                                        //是否发送登录请求中[true:是,false:否]
        private static Dictionary<LoginResult, string> loginTipDict;          //账号登录失败提示
        private WindowsPlatformLoginData _windowsPlatformLoginData;
        private WindowsWebPlatformLoginData _webPlatformLoginData;
        private static LoginManager _instance;

        static LoginManager()
        {
            _instance = new LoginManager();
            loginTipDict = new Dictionary<LoginResult, string>();
        }

        public static void InistTipDict()
        {
            //账号登录失败提示
            loginTipDict.Add(LoginResult.RET_ACCOUNT_PASSWD_NOMATCH, XMLManager.chinese[103001].__content);  //"帐号密码不匹配"
            loginTipDict.Add(LoginResult.NO_SERVICE, XMLManager.chinese[103002].__content);                  //"服务器未开放登陆"
            loginTipDict.Add(LoginResult.FORBIDDEN_LOGIN, XMLManager.chinese[103003].__content);             //"被禁止登陆"
            loginTipDict.Add(LoginResult.TOO_MUCH, XMLManager.chinese[103004].__content);                    //"服务器人数超过最大数量，不可登录" 
            loginTipDict.Add(LoginResult.TIME_ILLEGAL, XMLManager.chinese[103005].__content);                //"本次登录超时"
            loginTipDict.Add(LoginResult.SIGN_ILLEGAL, XMLManager.chinese[103006].__content);                //"签名非法"  
            loginTipDict.Add(LoginResult.SERVER_BUSY, XMLManager.chinese[103007].__content);                 //"sdk服务器验证超时，请重新平台登录！"
            loginTipDict.Add(LoginResult.SDK_VERIFY_FAILED, XMLManager.chinese[103008].__content);           //"sdk服务器验证失败，请重新平台登录！" 
            loginTipDict.Add(LoginResult.ACCOUNT_ILLEGAL, XMLManager.chinese[103009].__content);             //"sdk验证成功但是帐号不一样"
            loginTipDict.Add(LoginResult.MULTILOGIN, XMLManager.chinese[103010].__content);                  //"重复登陆"
            loginTipDict.Add(LoginResult.INNER_ERR, XMLManager.chinese[103011].__content);                   //"服务器内部错误"
            loginTipDict.Add(LoginResult.ADDR_NOT_TRUST, XMLManager.chinese[103012].__content);              //"非信任ip请求"
            loginTipDict.Add(LoginResult.PLAT_NAME_ERROR, XMLManager.chinese[103015].__content);             //"平台错误"
            loginTipDict.Add(LoginResult.PLAT_RETURN_ERROR, XMLManager.chinese[103015].__content);           //"平台返回错误" 
        }

        public static LoginManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private LoginManager()
        {
            AdddEventListener();
        }

        private void AdddEventListener()
        {
            EventDispatcher.AddEventListener(RPCEvents.DefuseLogin, OnDefuseLogin);
            EventDispatcher.AddEventListener<LoginResult>(RPCEvents.Login, OnLoginResp);
            EventDispatcher.AddEventListener<DefCheckResult>(RPCEvents.CheckDef, OnCheckDefMD5);
            //EventDispatcher.AddEventListener(LoginEvents.ON_XMLDATA_INITED, Login); //不能多次调用登陆
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(RPCEvents.DefuseLogin, OnDefuseLogin);              //账号被拒绝登陆
            EventDispatcher.RemoveEventListener<LoginResult>(RPCEvents.Login, OnLoginResp);
            EventDispatcher.RemoveEventListener<DefCheckResult>(RPCEvents.CheckDef, OnCheckDefMD5);
        }

        public void ResetLoginDefaultValue()
        {
            isLoging = false;
        }

        public void LoadServerList()
        {
            LoginManager.Instance.ResetLoginDefaultValue();  //设置为可发送登录请求状态
            ServerListManager.Init(ServerListInitCallback);  //加载server.xml文件(服务器列表配置文件)     
        }

        private void ServerListInitCallback()
        {
            LoginManager.Instance.Login();
            //加载服务器信息配置表(server.xml), 并转化为ServerInfo
            ServerListManager.StartTimer();   //开启定时加载servers.xml文件
        }

        /// <summary>
        /// 发起登陆请求
        /// </summary>
        public void Login()
        {
            if (isLoging) return;  //登录中
            if (SystemConfig.IsUsePlatformSdk && (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer))
            {
                if (!PlatformSdkMgr.Instance.IsLoginDone)
                {//平台登录
                    isLoging = true;
                    beginTime = Environment.TickCount;
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        LoggerHelper.Info("[Android-Platform Login] [Start]");
                        PlatformSdkMgr.Instance.Login();
                    }
                    else
                    {
                        LoggerHelper.Info("[IOS-Platform Login] [Start]");
                        IosPlatformLogin();
                    }
                }
                else
                {//Web登录服-登陆
                    LoggerHelper.Info(string.Format("[Sdk-Login] cost:{0}", Environment.TickCount - beginTime));
                    isLoging = true;
                    LoginWeb(CurrentServer.ip);     //暂时屏蔽Web登录
                }
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                isLoging = true;
                LoginWeb(CurrentServer.ip);     
            }
            else
            {//PC登录
                isLoging = true;
                LoginPc(CurrentServer.ip, CurrentServer.port);
            }
        }

        //IOS平台登录
        private void IosPlatformLogin()
        {
            #if UNITY_IPHONE && !UNITY_EDITOR
                LoggerHelper.Info(string.Format("[IOSPlugins] ", IOSPlugins.IsLoginDone ? "已登录" : "未登录"));
                if (IOSPlugins.IsLoginDone)
                {//已登录 
                    LoginWebFromIOS("");
                }
                else
                {
                    IOSPlugins.LoginAccountCenterWithCallBack(LoginWebFromIOS);
                }
            #endif
        }

        //IOSWeb登录
        private void LoginWebFromIOS(string a10)
        {
            ReloginFromIOS(a10);
            if (CurrentServer != null) LoginWeb(CurrentServer.ip);
        }

        //重新登录4399-IOS成功回调
        private void ReloginFromIOS(string a10)
        {
            LoggerHelper.Info(string.Format("[IOSPlugins] 平台登录[OK] a10:{0},CurrentServer:{1}", a10, CurrentServer));
            EventDispatcher.TriggerEvent<string>(LoginEvents.REFRESH_LOGIN_ACCOUNTNAME, string.Empty);
        }

        //Web登录--后台登录服务器
        private void LoginWeb(string ip)
        {
            string url = string.Empty;
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                //http://%@/cgi-bin/login/appstore?suid=%@&timestamp=%@&plataccount=%@&sign=%@&tocken=%@&port=%@&serverid=s%@
                string LOGIN_URLX = "http://{0}/cgi-bin/login/appstore?suid={1}&timestamp={2}&plataccount={3}&sign={4}&tocken={5}&port={6}&serverid=s{7}";
                url = string.Format(LOGIN_URLX,
                                            ip,
                                            Uri.EscapeDataString(LoginInfo.platformUid),
                                            Uri.EscapeDataString(LoginInfo.timestamp),
                                            Uri.EscapeDataString(LoginInfo.platformAccount),
                                            Uri.EscapeDataString(LoginInfo.sign),
                                            Uri.EscapeDataString(LoginInfo.token),
                                            CurrentServer.port,
                                            CurrentServer.id);
                LoggerHelper.Info(string.Format("[IOS-WebLogin] HttpGetUrl:{0}", url));
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                string LOGIN_URLX = "http://{0}/cgi-bin/login/{1}?username={2}&time={3}&flag={4}&cm={5}&server={6}&loginserver=s{7}&fnpid={8}";
                url = string.Format(LOGIN_URLX,
                                            ip,
                                            "pc_4399",
                                            _windowsPlatformLoginData.username,
                                            _windowsPlatformLoginData.time,
                                            _windowsPlatformLoginData.flag,
                                            _windowsPlatformLoginData.cm,
                                            _windowsPlatformLoginData.server,
                                            LocalSetting.settingData.SelectedServerID,
                                            _windowsPlatformLoginData.fnpid);   // 1 - 安卓 2- ios
                LoggerHelper.Info(string.Format("[PCLogin] HttpGetUrl:{0}", url));
            }
            else if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                _webPlatformLoginData = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData();

                string LOGIN_URLX = "http://{0}/cgi-bin/login/{1}?username={2}&time={3}&flag={4}&cm={5}&server={6}&loginserver=s{7}&fnpid={8}";
                url = string.Format(LOGIN_URLX,
                                            ip,
                                            "pc_4399",
                                            Uri.EscapeDataString(_webPlatformLoginData.username),
                                            Uri.EscapeDataString(_webPlatformLoginData.time),
                                            Uri.EscapeDataString(_webPlatformLoginData.flag),
                                            Uri.EscapeDataString(_webPlatformLoginData.cm),
                                            Uri.EscapeDataString(_webPlatformLoginData.server),
                                            LocalSetting.settingData.SelectedServerID,
                                            Uri.EscapeDataString(_webPlatformLoginData.fnpid));   // 1 - 安卓 2- ios);
                LoggerHelper.Info(string.Format("[WebLogin] HttpGetUrl:{0}", url));
                Driver.Instance.StartCoroutine(WebLoginInfoManager.GetInstance().GetHttpByWWW(url, OnWebLogin, OnWebLoginFail));
                beginTime = Environment.TickCount;
                return;
            }
            else
            {
                if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidFn)
				{
	                string LOGIN_URLX = "http://{0}/cgi-bin/login/{1}?name={2}&uid={3}&ext={4}&serverid=s{5}&platform={6}";
	                url = string.Format(LOGIN_URLX,
	                                            ip,
	                                            "fn",
	                                            Uri.EscapeDataString(LoginInfo.platformAccount),
	                                            Uri.EscapeDataString(LoginInfo.platformUid),
	                                            Uri.EscapeDataString(LoginInfo.ext),
	                                            LocalSetting.settingData.SelectedServerID,
	                                            LoginInfo.platformName);
				}
				else if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidKorea)
                {
                    //web登陆游戏服务器的url请求，korea也在这里加
                    string loginUrlFormat = "http://{0}/cgi-bin/login/{1}?username={2}&timestamp={3}&signStr={4}&serverid=s{5}";
                    url = string.Format(loginUrlFormat,
                                                ip,
                                                "korea",
                                                Uri.EscapeDataString(LoginInfo.userName),
                                                Uri.EscapeDataString(LoginInfo.ExtJsonData.timestamp),
                                                Uri.EscapeDataString(LoginInfo.ExtJsonData.signStr),
                                                LocalSetting.settingData.SelectedServerID.ToString());
                }
                LoggerHelper.Info(string.Format("[WebLogin] HttpGetUrl:{0}", url));
            }
            beginTime = Environment.TickCount;
            HttpUtils.Get(url, OnWebLogin, OnWebLoginFail);
        }
    
        //Web请求后台登录服返回
        private void OnWebLogin(string result)
        {
            string[] baseappInfo = result.Split(',');
            int errorId = int.Parse(baseappInfo[0]);
            LoggerHelper.Info(string.Format("[WebLogin] [{0}],cost:{1}ms,isReturnEmptyAccount:{2}", errorId == 0 ? "OK" : "Fail", Environment.TickCount - beginTime, LoginInfo.isReturnEmptyAccount));
            if (errorId == 0)
            {
                //PlatformSdkMgr.Instance.LoginGameLog();  //平台和Web都登录成功才统计
                string baseIP = baseappInfo[1];
                string baseLoginKey = baseappInfo[3];
                int basePort = int.Parse(baseappInfo[2]);
                if (baseappInfo.Length > 4 && RuntimePlatform.Android == Application.platform)
                {//platformId_uid
                    string[] arry = baseappInfo[4].Split('_');
                    LoginInfo.platformId_uid = baseappInfo[4];
                    if (arry != null && arry.Length == 1)
                    {
                        LoginInfo.platformUid = arry[1];                                                                          //注意：uid以服务端返回的uid为准
                        if (LoginInfo.isReturnEmptyAccount)
                        {//返回空账号 
                            LoginInfo.userName = LoginInfo.platformUid;                                                           //如果没有用户名，则用uid作为用户名(像应用包是没有返回用户名的) 
                            LoginInfo.platformAccount = LoginInfo.platformUid;                                                    //同用户名
                            EventDispatcher.TriggerEvent<string>(LoginEvents.REFRESH_LOGIN_ACCOUNTNAME, "");                      //刷新登陆界面
                        }
                    }
                    LoggerHelper.Info(string.Format("[WebLogin] Server Return platformId_uid:{0},platformUid:{1}", LoginInfo.platformId_uid, LoginInfo.platformUid));
                }
                //if (baseappInfo.Length > 5) { string username = baseappInfo[5]; }  //username
                LoginPc(baseIP, basePort, baseLoginKey);
            }
            else
            {//Web账号验证失败
                string tips = getLoginTip(errorId, true);
                if (errorId == (int)LoginResult.SERVER_BUSY || errorId == (int)LoginResult.SDK_VERIFY_FAILED)
                {//SDK验证失败，弹出平台登录框
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, tips, PanelIdEnum.MainUIField);
                    PlatformSdkMgr.Instance.OnSwitchAccount();
                    #if UNITY_IPHONE && !UNITY_EDITOR
                        IOSPlugins.LoginAccountCenterWithCallBack(ReloginFromIOS);
                    #endif
                }
                else
                {
                    //ari SystemAlert.Show(true, XMLManager.chinese[103044].__content, tips, true);  //"验证失败"
                }
                isLoging = false;
            }
        }

        //Web请求后台登录服失败
        private void OnWebLoginFail(HttpStatusCode errorCode)
        {
            isLoging = false;
            LoggerHelper.Info(string.Format("[WebLogin] [Fail],errorCode:{0},cost:{1}ms", errorCode, Environment.TickCount - beginTime));
            //SystemAlert.Show(true, "登录失败", string.Format("Web登录失败:{0}", errorCode), true);
            string txt1=XMLManager.chinese[103045].__content;
      //ari SystemAlert.Show(true, txt1, string.Format("Web{0}:{1}", txt1, errorCode), true);
        }

        /// <summary>
        /// PC方式登录--直接采用Socket方式
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <param name="baseLoginKey">base登录的key值</param>
        private void LoginPc(string ip, int port, string baseLoginKey = null)
        {
            Action<bool> connectedCB = (result) =>
            {
                if (result)
                {
                    //A-1、如果开启断线重连机制，这里加断线重连或者底层通讯做处理
                    //PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);    //开启网络断线超时检查
                    if (!string.IsNullOrEmpty(baseLoginKey))
                    {//平台和Web登录验证都已通过，这里发起进入游戏服务器请求
                        LoggerHelper.Info(string.Format("[BaseLogin] [{0}:{1}] baseLoginKey:{2}", ip, port, baseLoginKey));
                        RemoteProxy.baseIP = ip;
                        RemoteProxy.basePort = port;
                        PlatformSdkMgr.Instance.Log("begin baseLogin");
                        ServerProxy.Instance.BaseLogin(baseLoginKey);
                        PlatformSdkMgr.Instance.Log("begin baseLogin done");
                        PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);
                        CheckDefMD5();
                    }
                    else
                    {//PC端登录验证已通过，这里发起进入游戏服务器请求
                        LoggerHelper.Info(string.Format("[PcLogin] [{0}:{1}]", ip, port));
                        ServerProxy.Instance.Login(LoginInfo.GetPCStrList());
                        PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);
                        CheckDefMD5();
                    }
                }
                else
                {
                    isLoging = false;
                    //SystemAlert.Show(true, "网络异常", string.Format("无法连接服务器[{0}:{1}]", ip, port));
              //ari SystemAlert.Show(true, XMLManager.chinese[103020].__content, string.Format(XMLManager.chinese[103052].__content, ip, port));  //"无法连接服务器[{0}:{1}]"
                }
            };
            ServerProxy.Instance.IsEnableReconnect(false);
            ServerProxy.Instance.Connect(ip, port, connectedCB);
        }

        private void CheckDefMD5()
        {
            LoggerHelper.Info(string.Format("[CheckDefMD5] [{0}]", CommonUtils.FormatMD5(MogoEngine.RPC.DefParser.Instance.m_defContentMD5)));
            ServerProxy.Instance.CheckDefMD5(MogoEngine.RPC.DefParser.Instance.m_defContentMD5);
        }

        private void OnCheckDefMD5(DefCheckResult result)
        {
            switch(result)
            {
                case DefCheckResult.ENUM_LOGIN_CHECK_NO_SERVICE:
                    LoggerHelper.Error("检查DEF时服务器链接不成功！");
                    //SystemAlert.Show(true, "版本不匹配", "检查DEF时服务器链接不成功！", true);
              //ari SystemAlert.Show(true, XMLManager.chinese[103047].__content, XMLManager.chinese[103049].__content, true);
                    break;

                case DefCheckResult.ENUM_LOGIN_CHECK_ENTITY_DEF_NOMATCH:
                    LoggerHelper.Error("检查DEF时MD5码不正确！");
                    //SystemAlert.Show(true, "版本不匹配", "检查DEF时MD5码不正确！", true);
              //ari SystemAlert.Show(true, XMLManager.chinese[103047].__content, XMLManager.chinese[103048].__content, true);
                    break;
            }
            LoggerHelper.Info("OnCheckDefMD5: " + result);
        }

        //顶号：提示玩家已在其他地方登陆
        private void OnMultiLogin()
        {
            isLoging = false;
            ServerProxy.Instance.enableTimeout(false);
            ServerProxy.Instance.Disconnect();
            string txt1 = XMLManager.chinese[103050].__content;    //"顶号提示"
            string txt2 = XMLManager.chinese[103051].__content;    //"您的账号已在别处登录！"
            string txt3 = XMLManager.chinese[103022].__content;    //重启游戏
      //ari SystemAlert.Show(true, txt1, txt2, null, null, txt3, null, null, true, false);
        }

        //登陆被拒绝
        private void OnDefuseLogin()
        {
            PlayerTimerManager.GetInstance().enableNetTimeoutCheck(false);
            ServerProxy.Instance.Disconnect();
            TimerHeap.AddTimer(1000, 0, () => 
            {//顶号重连
                isLoging = false;
                EventDispatcher.TriggerEvent(LoginEvents.C_RELOGIN_REQ);  //派发事件到LoginPanel.cs,发送重新登录
            });
        }

        //账号登录返回
        private void OnLoginResp(LoginResult result)
        {
            if (result != LoginResult.SUCCESS)
            {
                //这里是后端到平台验证失败或后端登录处理失败返回
                isLoging = false;
                PlatformSdkMgr.Instance.IsLoginDone = false;
                //SystemAlert.Show(true, "账号登录失败", getLoginTip((int)result), true);
          //ari SystemAlert.Show(true, XMLManager.chinese[103045].__content, getLoginTip((int)result), true);
                return;
            }
           
        }

        private ServerInfo CurrentServer
        {
            get
            {
                ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
                if(serverInfo == null)
                {
                    serverInfo = ServerListManager.GetRecommentServer();
                }
                return serverInfo;
            }
            set
            {
                LocalSetting.settingData.SelectedServerID = value.id;
                LocalSetting.Save();
            }
        }

        /// <summary>
        /// 根据错误码ID--取得登录失败提示
        /// </summary>
        /// <param name="errorId"></param>
        /// <returns></returns>
        public string getLoginTip(int errorId, bool isWeb = false)
        {
            if (loginTipDict.ContainsKey((LoginResult)errorId)) return loginTipDict[(LoginResult)errorId];
            if(isWeb) return string.Format("Web{0}:{1}", XMLManager.chinese[103045].__content, errorId);
            return string.Format("{0}:{1}", XMLManager.chinese[103045].__content, errorId);
        }

        /// <summary>
        /// 根据错误码ID--取得创角失败提示
        /// </summary>
        /// <param name="errorId"></param>
        /// <returns></returns>
        public string getRegisterTip(int errorId)
        {
            system_info data = XMLManager.system_info.ContainsKey(errorId) ? XMLManager.system_info[errorId] : null;
            return data != null ? data.__descript : "错误码:" + errorId;
        }

        public void SetWindowsPlatformLoginInfo(string json)
        {
            _windowsPlatformLoginData = new WindowsPlatformLoginData();
            //格式化json串
            /*json = json.Replace("{", "");
            json = json.Replace("}", "");
            string _copyJson = "";
            string[] test = json.Split(',');
            for (int i = 0; i < test.Length; i++)
            {
                string[] _chars = test[i].Split(':');
                for (int j = 0; j < _chars.Length; j++)
                {
                    string _char = "|" + _chars[j] + "|" + ":";
                    if ((j + 1) == _chars.Length)
                    {
                        _char = _char.Substring(0, _char.Length - 1);
                    }
                    _copyJson += _char;
                }
                _copyJson += ",";
            }
            _copyJson = "{" + _copyJson.Substring(0, _copyJson.Length - 1) + "}";
            json = _copyJson.Replace("|", "\"");*/
            LoggerHelper.Info("SetWindowsPlatformLoginInfo:" + json);
            _windowsPlatformLoginData = JsonMapper.ToObject<WindowsPlatformLoginData>(json);
            //LoggerHelper.Info("SetWindowsPlatformLoginInfo:" + _windowsPlatformLoginData.username + ":" + _windowsPlatformLoginData.server);
            LocalSetting.settingData.UserAccount = _windowsPlatformLoginData.name;
            //LoggerHelper.Info("SetWindowsPlatformLoginInfo:" + _windowsPlatformLoginData.ext);
            //LocalSetting.settingData.SelectedServerID = int.Parse(_windowsPlatformLoginData.server.Split('S')[1]);
        }

        public WindowsPlatformLoginData GetWindowsPlatformLoginData()
        {
            return _windowsPlatformLoginData;
        }

        public WindowsWebPlatformLoginData GetWebPlatformLoginData()
        {
            return _webPlatformLoginData;
        }
    }
}
