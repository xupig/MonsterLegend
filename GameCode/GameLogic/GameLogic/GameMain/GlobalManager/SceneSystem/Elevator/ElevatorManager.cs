﻿using Common.Utils;
using GameLoader.Utils;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/1 9:57:34 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public class ElevatorManager
    {
        private Dictionary<int, ElevatorController> _elevatorDict;
        private Transform _elevatorRoot;
        public void Enter()
        {
            _elevatorDict = new Dictionary<int, ElevatorController>();
            GameObject[] sceneObjs = GameSceneManager.GetInstance().sceneGameObjects;
            for (int i = 0; i < sceneObjs.Length; ++i)
            {
                _elevatorRoot = sceneObjs[i].transform.FindChild("Elevator");
                if (_elevatorRoot != null)
                {
                    break;
                }
            }
            if (_elevatorRoot == null)
            {
                _elevatorRoot = new GameObject("Elevator").transform;
                _elevatorRoot.SetParent(sceneObjs[0].transform);
            }
        }

        public void Exit()
        {
            foreach (KeyValuePair<int, ElevatorController> kv in _elevatorDict)
            {
                GameObject go = kv.Value.gameObject;
                GameObject.Destroy(go);
            }
            _elevatorDict.Clear();
            _elevatorDict = null;
            _elevatorRoot = null;
        }

        public void AddElevator(int id, GameObject elevator, string openWall, string positionListString)
        {
            if (_elevatorDict.ContainsKey(id))
            {
                LoggerHelper.Error(string.Format("@谢思宇添加电梯失败，场景中已存在id = {0}的电梯。", id));
                return;
            }
            GameObject elevatorGo = new GameObject(string.Concat("elevator", id));
            elevatorGo.transform.SetParent(_elevatorRoot);
            ElevatorController elevatorController = elevatorGo.AddComponent<ElevatorController>();
            _elevatorDict.Add(id, elevatorController);
            elevatorController.AddElevatorGameObject(id, elevator, openWall, positionListString);
        }

        public void OnElevatorDestroy(int id)
        {
            _elevatorDict.Remove(id);
        }

        public ElevatorController GetElevator(int id)
        {
            if (_elevatorDict.ContainsKey(id))
            {
                return _elevatorDict[id];
            }
            return null;
        }

        public void ElevatorMove(int id, float[] durationArray, string openWall)
        {
            if (!_elevatorDict.ContainsKey(id))
            {
                LoggerHelper.Error(string.Format("电梯移动失败，不存在id = {0}的电梯。", id));
                return;
            }
            _elevatorDict[id].Move(durationArray, openWall);
        }

        public void ElevatorMoveBack(int id, float[] durationArray, string openWall)
        {
            if (!_elevatorDict.ContainsKey(id))
            {
                LoggerHelper.Error(string.Format("电梯移动失败，不存在id = {0}的电梯。", id));
                return;
            }
            _elevatorDict[id].MoveBack(durationArray, openWall);
        }

        public bool IsElevatorInStartPos(int id)
        {
            if (!_elevatorDict.ContainsKey(id))
            {
                return false;
            }
            return _elevatorDict[id].IsInStartPos();
        }

        public void ShowElevator(int id, bool visible)
        {
            if (!_elevatorDict.ContainsKey(id))
            {
                return;
            }
            _elevatorDict[id].ShowElevator(visible);
        }

        public void ResetElevator(int id)
        {
            if (!_elevatorDict.ContainsKey(id))
            {
                return;
            }
            _elevatorDict[id].ResetElevator();
        }

        public void ResetAllElevators()
        {
            if (_elevatorDict == null)
            {
                return;
            }
            foreach (var node in _elevatorDict)
            {
                ResetElevator(node.Key);
            }
        }
    }
}
