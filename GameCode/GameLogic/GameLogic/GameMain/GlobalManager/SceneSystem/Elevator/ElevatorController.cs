﻿using Common.ClientConfig;
using Common.States;
using GameLoader.Utils;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/1 10:36:22 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public enum ElevatorMovingState
    {
        NONE = 0,
        MOVING,
        MOVING_BACK
    }

    public class ElevatorController : MonoBehaviour
    {
        private int _id = 0;
        private GameObject _elevatorGameObject;
        private Vector3 _startPosition;
        private Vector3 _endPosition;
        private ElevatorMovingState _movingState;
        private float _stepTime;
        private float _pastTime = 0;
        private Vector3 _movingSpeed;
        private Vector3 _characterElevatorOffset;
        private bool _isInStartPos = false;
        private List<Vector3> _movePositionList;
        private List<float> _moveDurationList;
        private int _currStep = 0;
        private int _maxStep = 0;
        private Dictionary<string, Transform> _walls;
        private string _openWallNameDefault = string.Empty;
        private string _openWallName = string.Empty;
        private string _openWallNameOnMoveEnd = string.Empty;
        private bool _isRising = false;

        void Start()
        {
            _moveDurationList = new List<float>();
            _movingState = ElevatorMovingState.NONE;
            _isInStartPos = true;
        }

        void Update()
        {
            if (_movingState == ElevatorMovingState.NONE)
            {
                return;
            }
            _pastTime += Time.deltaTime;
            OnMoveUpdate(Time.deltaTime);
        }

        void OnDestroy()
        {
            if (_movingState != ElevatorMovingState.NONE)
            {
                RecoverAccordingMode();
            }
            _elevatorGameObject = null;
            _movePositionList.Clear();
            _movePositionList = null;
            _walls.Clear();
            _walls = null;
        }

        private void RecoverAccordingMode()
        {
            PlayerAvatar.Player.moveManager.accordingMode = PlayerAvatar.Player.moveManager.defaultAccordingMode;
        }

        private void SettingAccordingMode()
        {
            PlayerAvatar.Player.moveManager.accordingMode = AccordingMode.AccordingElevator;
        }

        public void AddElevatorGameObject(int id, GameObject elevatorGameObject, string openWallNameDefault, string positionListString)
        {
            _id = id;
            _openWallNameDefault = openWallNameDefault;
            InitPositionList(positionListString);
            _elevatorGameObject = elevatorGameObject;
            elevatorGameObject.transform.SetParent(gameObject.transform, true);
            _elevatorGameObject.isStatic = false;
            _elevatorGameObject.transform.position = _startPosition;
            InitWalls();
            OpenWall(openWallNameDefault);
        }

        public bool Move(float[] durationArray, string openWall)
        {
            return OnMoveBegin(durationArray, openWall, ElevatorMovingState.MOVING);
        }

        public bool MoveBack(float[] durationArray, string openWall)
        {
            return OnMoveBegin(durationArray, openWall, ElevatorMovingState.MOVING_BACK);
        }

        public bool IsInStartPos()
        {
            return _movingState == ElevatorMovingState.NONE && _isInStartPos;
        }

        public void ShowElevator(bool visible)
        {
            gameObject.SetActive(visible);
        }

        public void ResetElevator()
        {
            _elevatorGameObject.transform.position = _startPosition;
            OpenWall(_openWallNameDefault);
            _isInStartPos = true;
        }

        private void InitWalls()
        {
            _walls = new Dictionary<string, Transform>();
            Transform wallParent = _elevatorGameObject.transform.FindChild("Walls");
            if (wallParent != null)
            {
                for (int i = 0; i < wallParent.childCount; ++i)
                {
                    _walls.Add(wallParent.GetChild(i).name, wallParent.GetChild(i));
                }
            }
        }

        private void InitPositionList(string positionListString)
        {
            string[] positionList = positionListString.Split(',');
            _movePositionList = new List<Vector3>();
            int index = 0;
            for (int i = 0; i < positionList.Length / 3; ++i)
            {
                _movePositionList.Add(new Vector3(float.Parse(positionList[index++]), float.Parse(positionList[index++]), float.Parse(positionList[index++])));
            }
            _maxStep = _movePositionList.Count - 1;
            if (_movePositionList.Count < 2)
            {
                LoggerHelper.Error(string.Format("初始化id = {0}的电梯有错误，配置的坐标点不能小于2个", _id));
            }
            _startPosition = _movePositionList[0];
            _endPosition = _movePositionList[_maxStep];
        }

        private void OpenWall(string openWallName)
        {
            if (_openWallName != "")
            {
                CloseWall(_openWallName);
            }
            if (!string.IsNullOrEmpty(openWallName) && _walls.ContainsKey(openWallName))
            {
                _walls[openWallName].gameObject.SetActive(false);
                _openWallName = openWallName;
            }
        }

        private void CloseWall(string openWallName)
        {
            if (_walls.ContainsKey(openWallName))
            {
                _walls[openWallName].gameObject.SetActive(true);
            }
        }

        private bool OnMoveBegin(float[] durationArray, string openWall, ElevatorMovingState movingState)
        {
            if (movingState == ElevatorMovingState.NONE)
            {
                return false;
            }
            if (_elevatorGameObject == null)
            {
                LoggerHelper.Error(string.Format("电梯id = {0}移动错误，因为没有副本事件添加该电梯", _id));
                return false;
            }
            if (_movingState != ElevatorMovingState.NONE)
            {
                return false;
            }
            if (durationArray.Length != _movePositionList.Count - 1)
            {
                LoggerHelper.Error(string.Format("电梯id = {0}移动错误，因为没有电梯坐标点数为{1}，配置时间数为{2}，应为时间数 = 坐标点数 - 1", _id, _movePositionList.Count, durationArray));
                return false;
            }
            _movingState = movingState;
            _stepTime = 0;
            _pastTime = 0;
            _moveDurationList.Clear();
            for (int i = 0; i < durationArray.Length; ++i)
            {
                _moveDurationList.Add(durationArray[i]);
            }
            _currStep = 0;
            if (_movingState == ElevatorMovingState.MOVING)
            {
                _elevatorGameObject.transform.position = _startPosition;
                _movingSpeed = (_movePositionList[1] - _movePositionList[0]) / _moveDurationList[0];
                _characterElevatorOffset = PlayerAvatar.Player.actor.position - _startPosition;
            }
            else
            {
                _elevatorGameObject.transform.position = _endPosition;
                _movingSpeed = (_movePositionList[_maxStep - 1] - _movePositionList[_maxStep]) / _moveDurationList[0];
                _characterElevatorOffset = PlayerAvatar.Player.actor.position - _endPosition;
            }
            _openWallNameOnMoveEnd = openWall;
            _isRising = (_movingSpeed.y > 0);
            SettingAccordingMode();
            return true;
        }

        private void OnMoveEnd()
        {
            if (_movingState == ElevatorMovingState.MOVING)
            {
                _elevatorGameObject.transform.position = _endPosition;
                _isInStartPos = false;
            }
            else
            {
                _elevatorGameObject.transform.position = _startPosition;
                _isInStartPos = true;
            }
            _movingState = ElevatorMovingState.NONE;
            OpenWall(_openWallNameOnMoveEnd);
            RecoverAccordingMode();
        }

        private void OnMoveUpdate(float deltaTime)
        {
            if (_pastTime - _stepTime >= _moveDurationList[_currStep])
            {
                OnMoveToNextStep();
            }
            else
            {
                _elevatorGameObject.transform.position += _movingSpeed * deltaTime;
                Vector3 newActorPos = _elevatorGameObject.transform.position + _characterElevatorOffset;
                float y = (_isRising) ? PlayerAvatar.Player.actor.position.y : newActorPos.y;
                PlayerAvatar.Player.actor.position = new Vector3(newActorPos.x, y, newActorPos.z);
            }
        }

        private void OnMoveToNextStep()
        {
            if (_currStep >= _moveDurationList.Count - 1)
            {
                OnMoveEnd();
                return;
            }
            _currStep++;
            _stepTime = _pastTime;
            if (_movingState == ElevatorMovingState.MOVING)
            {
                _movingSpeed = (_movePositionList[_currStep + 1] - _movePositionList[_currStep]) / _moveDurationList[_currStep];
                _elevatorGameObject.transform.position = _movePositionList[_currStep];
            }
            else
            {
                _movingSpeed = (_movePositionList[_maxStep - 1 - _currStep] - _movePositionList[_maxStep - _currStep]) / _moveDurationList[_currStep];
                _elevatorGameObject.transform.position = _movePositionList[_maxStep - _currStep];
            }
            PlayerAvatar.Player.actor.position = _elevatorGameObject.transform.position + _characterElevatorOffset;
            _isRising = (_movingSpeed.y > 0);
        }
    }
}
