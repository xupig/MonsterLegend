﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using MogoEngine.Events;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class ConditionInfo
    {
        public int id;
        public int count;
        public float hpOdds;
        public int triggeredCount;
        public ConditionInfo()
        {
            id = 0;
            count = 0;
            hpOdds = 0;
            triggeredCount = 0;
        }
    }

    public class SpaceConditionBase
    {
        protected Dictionary<int, ConditionInfo> conditionInfoList = new Dictionary<int, ConditionInfo>();
        protected string[] _eventTriggerNum;
        protected string[] _eventAction;
        protected EntityCondTriggerActionData _data;
        protected int _id;

        virtual public int type
        {
            get { return 0; }
        }

        public SpaceConditionBase()
        {
        }

        virtual public void Enter(EntityCondTriggerActionData data)
        {
            _data = data;
            _eventTriggerNum = _data.event_trigger_num_l.Split(',');
            if (!string.IsNullOrEmpty(_data.event_action_s))
            {
                _eventAction = _data.event_action_s.Split(';');
            }
            InitCondInfo();
        }

        virtual protected void InitCondInfo()
        {
            ConditionInfo info;
            for (int i = 0; i < _eventAction.Length; i++)
            {
                info = new ConditionInfo();
                info.id = int.Parse(_eventAction[i]);
                info.count = int.Parse(_eventTriggerNum[i]);
                conditionInfoList.Add(info.id, info);
                if (_data.is_can_trigger_notice_i == (int)ShowNoticeType.SHOW_NOTICE)
                {
                    ShowNotice(0, info.count, info.id);
                }
            }
            int totalScore = 0;
            if (_data.is_can_trigger_notice_i == (int)ShowNoticeType.SHOW_COUNT_NOTICE)
            {
                totalScore = int.Parse(_data.event_trigger_num_l);
                ShowNotice(0, totalScore);
            }
        }

        protected void ShowNotice(int currCount, int totalCount, int targetId = 0)
        {
            if (_data.is_can_trigger_notice_i != (int)ShowNoticeType.SHOW_UNNOTICE)
            {
                SystemNoticeRightUpData systemNoticeData = new SystemNoticeRightUpData();
                systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE;
                systemNoticeData.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE;
                systemNoticeData.priority = _data.show_priority_i;
                systemNoticeData.content = _data.notice_content_id_i;
                systemNoticeData.rightUpNoticeId = _data.id_i;
                systemNoticeData.killMonsterNum = currCount;
                systemNoticeData.monsterTotalNum = totalCount;
                systemNoticeData.rightUpTargetId = targetId;
                //Ari SpaceNoticeActionManager.Instance.AddSystemNotice(systemNoticeData);
            }
        }

        virtual public void Exit()
        {
        }

        virtual public bool IsSatisfy()
        {
            return false;
        }

        virtual public bool IsSatisfy(string args)
        {
            ParseArgs(args);
            if (!CheckInConditionInfo())
            {
                return false;
            }
            if (_data.is_and_type_i == (int)ConditionType.CONDITION_AND_TYPE)
            {
                return IsSatisfyAndType();
            }
            else
            {
                return IsSatisfyOrType();
            }
        }

        virtual protected void ParseArgs(string args)
        {
            string[] argsArray = args.Split(':');
            _id = int.Parse(argsArray[1]);
        }

        //查找是否为条件行为触发前置条件
        virtual public bool CheckInConditionInfo()
        {
            return conditionInfoList.ContainsKey(_id);
        }

        private bool IsSatisfyAndType()
        {
			bool result = true;
            foreach (var node in conditionInfoList)
			{
                if (!CheckSingleIsSatisfy(node.Value))
                {
					result = false;
                }
            }
			return result;
        }

        private bool IsSatisfyOrType()
        {
            return CheckSingleIsSatisfy(conditionInfoList[_id]);
        }

        virtual protected bool CheckSingleIsSatisfy(ConditionInfo conditionInfo)
        {
            if (conditionInfo.id == _id)
            {
                conditionInfo.triggeredCount++;
                ShowNotice(conditionInfo.triggeredCount, conditionInfo.count, conditionInfo.id);
            }
            return conditionInfo.triggeredCount >= conditionInfo.count;
        }

        virtual public void ResetTriggerCount()
        {
            foreach (var node in conditionInfoList)
            {
                node.Value.triggeredCount = 0;
            }
        }
    }
}
