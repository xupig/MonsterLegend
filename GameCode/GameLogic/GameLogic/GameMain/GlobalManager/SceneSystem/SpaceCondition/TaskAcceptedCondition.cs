﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class TaskAcceptedCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_TASK_ACCEPTED;
            }
        }

        protected override void ParseArgs(string args)
        {
            _id = int.Parse(args);
        }

        public override bool IsSatisfy()
        {
            //TODO:之后改成不止判断一个
            return PlayerDataManager.Instance.TaskData.IsTaskAccepted(int.Parse(_eventAction[0]));
        }

        protected override bool CheckSingleIsSatisfy(ConditionInfo conditionInfo)
        {
            if (conditionInfo.id != _id)
            {
                return false;
            }
            return true;    //任务接受没有次数
        }
    }
}
