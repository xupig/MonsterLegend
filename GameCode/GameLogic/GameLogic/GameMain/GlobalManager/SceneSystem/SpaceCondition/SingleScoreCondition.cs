﻿using Common.ClientConfig;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class SingleScoreCondition : SpaceConditionBase
    {
        private int _reachScore = 0;
        private int _score = 0;
        private int _scoreFlag = 0;
        private int _currScore = 1;
        private string _eventActionId = "";
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_SCORE;
            }
        }

        protected override void InitCondInfo()
        {
            _reachScore = int.Parse(_data.event_trigger_num_l);
            ShowNotice(_score, _reachScore);
        }

        protected override void ParseArgs(string args)
        {
            string[] argsArray = args.Split(':');
            _scoreFlag = int.Parse(argsArray[0]);
            _currScore = int.Parse(argsArray[1]);
            _eventActionId = argsArray[2];
        }

        public override bool IsSatisfy(string args)
        {
            ParseArgs(args);
            if (_eventActionId != _data.event_action_s)
            {
                return false;
            }
            if (_score == 0)
            {
                _score += _currScore;
            }
            else
            {
                switch (_scoreFlag)
                {
                    case 0:
                        _score = _score + _currScore;
                        break;
                    case 1:
                        _score = _score - _currScore;
                        break;
                    case 2:
                        _score = _score * _currScore;
                        break;
                    case 3:
                        _score = System.Convert.ToInt32(_score / _currScore);
                        break;
                    default:
                        break;
                }
            }
            ShowNotice(_score, _reachScore);
            return _score >= _reachScore;
        }

        public override void ResetTriggerCount()
        {
            _score = 0;
        }
    }
}
