﻿using Common.ClientConfig;
using GameMain.Entities;
using MogoEngine;

namespace GameMain.GlobalManager
{
    public class MonsterHpChangeCondition : SpaceConditionBase
    {
        protected uint _entityId = 0;
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_HP_CHANG;
            }
        }

        protected override void InitCondInfo()
        {
            ConditionInfo info;
            for (int i = 0; i < _eventAction.Length; i++)
            {
                info = new ConditionInfo();
                info.id = int.Parse(_eventAction[i].Split(',')[0]);
                info.hpOdds = float.Parse(_eventAction[i].Split(',')[1]);
                info.count = int.Parse(_eventTriggerNum[i]);
                conditionInfoList.Add(info.id, info);
                if (_data.is_can_trigger_notice_i == 1)
                {
                    ShowNotice(0, info.count, info.id);
                }
            }
        }

        protected override void ParseArgs(string args)
        {
            string[] argsArray = args.Split(':');
            _entityId = uint.Parse(argsArray[0]);
            _id = int.Parse(argsArray[1]);
        }

        protected override bool CheckSingleIsSatisfy(ConditionInfo conditionInfo)
        {
            if (conditionInfo.id == _id)
            {
                var entity = MogoWorld.GetEntity(_entityId) as EntityDummy;
                float hpodds = (float)entity.cur_hp / (float)entity.max_hp;
                if (hpodds <= conditionInfo.hpOdds)
                {
                    conditionInfo.triggeredCount++;
                    ShowNotice(conditionInfo.triggeredCount, conditionInfo.count, conditionInfo.count);
                }
            }
            return conditionInfo.triggeredCount >= conditionInfo.count;
        }
    }
}
