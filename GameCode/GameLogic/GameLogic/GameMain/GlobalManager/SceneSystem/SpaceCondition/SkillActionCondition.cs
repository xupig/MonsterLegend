﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class SkillActionCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_SKILL_ACTION;
            }
        }

        protected override void ParseArgs(string args)
        {
            _id = int.Parse(args);
        }
    }
}
