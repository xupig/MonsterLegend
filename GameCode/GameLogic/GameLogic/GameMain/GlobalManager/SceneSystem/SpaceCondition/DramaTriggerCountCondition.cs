﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class DramaTriggerCountCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_TASK_FINISH;
            }
        }

        public override bool IsSatisfy()
        {
            return DramaManager.GetInstance().GetTriggerCount(int.Parse(_eventAction[0])) < int.Parse(_eventTriggerNum[0]);
        }
    }
}
