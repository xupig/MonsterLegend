﻿using Common.ClientConfig;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class KillMonsterCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_KILL_MONSTER;
            }
        }
    }
}
