﻿using Common.ClientConfig;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class space_condition_helper
    {
        //private static Dictionary<int, Type> _spaceConditionDict;
        public static SpaceConditionBase CreateSpaceCondition(int type)
        {
            SpaceConditionBase condition = null;
            switch (type)
            {
                case InstanceDefine.COND_TYPE_KILL_MONSTER:
                    condition = Create<KillMonsterCondition>();
                    break;
                case InstanceDefine.COND_TYPE_HP_CHANG:
                    condition = Create<MonsterHpChangeCondition>();
                    break;
                case InstanceDefine.COND_TYPE_SKILL_ACTION:
                    condition = Create<SkillActionCondition>();
                    break;
                case InstanceDefine.COND_TYPE_DROP:
                    condition = Create<DropItemCondition>();
                    break;
                case InstanceDefine.COND_TYPE_ACTION:
                    condition = Create<SpaceActionCondition>();
                    break;
                case InstanceDefine.COND_TYPE_SCORE:
                case InstanceDefine.COND_TYPE_PVE_SCORE:
                    condition = Create<SingleScoreCondition>();
                    break;
                case InstanceDefine.COND_TYPE_BOSS_PART_DESTROY:
                    condition = Create<PartDestroyCondition>();
                    break;
                case InstanceDefine.COND_TYPE_TASK_ACCEPTED:
                    condition = Create<TaskAcceptedCondition>();
                    break;
                case InstanceDefine.COND_TYPE_TASK_FINISH:
                    condition = Create<TaskFinishCondition>();
                    break;
                case InstanceDefine.COND_TYPE_DRAMA_TRIGGER_COUNT:
                    condition = Create<DramaTriggerCountCondition>();
                    break;
                default:
                    break;
            }
            return condition;
        }

        public static T Create<T>() where T : SpaceConditionBase, new()
        {
            return new T();
        }
    }
}
