﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class PartDestroyCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_BOSS_PART_DESTROY;
            }
        }
    }
}
