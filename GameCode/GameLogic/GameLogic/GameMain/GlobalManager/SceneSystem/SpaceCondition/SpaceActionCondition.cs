﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class SpaceActionCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_ACTION;
            }
        }

        protected override void ParseArgs(string args)
        {
            _id = int.Parse(args);
        }
    }
}
