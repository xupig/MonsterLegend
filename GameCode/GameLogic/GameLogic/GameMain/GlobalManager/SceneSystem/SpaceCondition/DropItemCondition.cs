﻿using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    public class DropItemCondition : SpaceConditionBase
    {
        public override int type
        {
            get
            {
                return InstanceDefine.COND_TYPE_DROP;
            }
        }
    }
}
