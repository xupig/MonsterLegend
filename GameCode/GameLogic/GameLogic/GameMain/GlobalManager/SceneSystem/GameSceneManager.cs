﻿using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using SpaceSystem;
using System;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class GameSceneManager
    {
        private Dictionary<int, Dictionary<string, List<float>>> _mapPathInfo = new Dictionary<int, Dictionary<string, List<float>>>();
        private Dictionary<string, SecurityElement> _cacheSpaceData = new Dictionary<string, SecurityElement>();
		private Dictionary<Vector3, int> _sceneDummyInfo = new Dictionary<Vector3, int>();
        private static GameSceneManager s_instance = null;
        public static GameSceneManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameSceneManager();
            }
            return s_instance;
        }

        GameSceneManager()
        {
        }

        private int _preMapID;
        public int preMapID
        {
            get
            {
                return _preMapID;
            }
        }

        private int _mapID;
        public int curMapID
        {
            set
            {
                _preMapID = _mapID;
                _mapID = value;
            }
            get
            {
                return _mapID;
            }
        }

        public int curInstanceID
        {
            get
            {
                return map_helper.GetInstanceIDByMapID(_mapID);
            }
        }

        public ChapterType chapterType
        {
            get
            {
                return instance_helper.GetChapterType(curInstanceID);
            }
        }
        
        private string _mapPath;
        public string mapPath
        {
            set
            {
                _mapPath = value;
            }
            get
            {
                return _mapPath;
            }
        }

        private EntityGroundVehicle _entityVehicle = null;
        public EntityGroundVehicle Vehicle
        {
            set
            {
                _entityVehicle = value;
            }
            get
            {
                return _entityVehicle;
            }
        }

        private Vector3 _enterPosition;

        private IScene _scene;

        public IScene scene
        {
            get
            {
                return _scene;
            }
        }

        public void ExitScene()
        {
            if (_scene != null)
            {
                _scene.Exit();
                _scene = null;
            }
        }

        public void EnterScene(SceneType type, Action loadCallback = null)
        {
            ExitScene();
            _scene = scene_helper.CreateScene(type);
            _scene.Enter(_mapID, _enterPosition, loadCallback);
        }

        public void EnterSceneByID(int mapId, bool changeLine, Vector3 enterPos, Action loadCallback = null)
        {
            if (_mapID == mapId && !changeLine)
            {
                LoggerHelper.Info("mapID is already :" + mapId);
                return;
            }
            curMapID = mapId;
            _enterPosition = enterPos;
            int mapType = map_helper.GetMapType(mapId);
            EnterScene(scene_helper.GetSceneType(mapType), loadCallback);
        }

        public Vector3 GetServerEnterPos()
        {
            return _enterPosition;
        }

        public void SyncEnterPos()
        {
            if (MogoWorld.Player == null) return;
            var player = MogoWorld.Player as PlayerAvatar;
            //开始显示读条界面、进入场景前同步坐标，避免触发区域触发器
            if (player != null && player.actor != null)
            {
                player.actor.position = _enterPosition;
            }
        }

        public string curSpaceName
        {
            get
            {
                return _scene.GetSpaceName();
            }
        }

        public SpaceData curSpaceData
        {
            get { return _scene.GetSpaceData(); }
        }

        public bool inLoading
        {
            get { return _scene.IsInLoading(); }
        }

        public GameObject[] sceneGameObjects
        {
            get { return SceneMgr.Instance.sceneGameObjects; }
        }

        public bool CanShowNotice()
        {
            return curMapType == public_config.MAP_TYPE_CITY || curMapType == public_config.MAP_TYPE_WILD;
        }

        public bool IsCanTouch()
        {
            return curMapType == public_config.MAP_TYPE_CITY || curMapType == public_config.MAP_TYPE_WILD || curMapType == public_config.MAP_TYPE_DUEL;
        }

        public int curMapType
        {
            get
            {
                return map_helper.GetMapType(curMapID);
            }
        }

        public bool inCombatScene
        {
            get
            {
                return GameData.map_helper.GetMapType(curMapID) != public_config.MAP_TYPE_CITY;
            }
        }

        public bool IsInCity()
        {
            return map_helper.GetMapType(curMapID) == public_config.MAP_TYPE_CITY ? true : false;
        }

        public bool IsInClientMap()
        {
            return map_helper.IsClientCtrl(curMapID);
        }

        public Vector3 GetCurEnterPoint()
        {
            return _scene.GetEnterPoint();
        }

        public void SetCurrMapPathInfo(int mapId, string key, List<float> path)
        {
            string[] keys = key.Split('-');
            string reverseKey = keys[1] + "-" + keys[0];
            var pathInfo = new Dictionary<string, List<float>>();
            if (_mapPathInfo.ContainsKey(mapId))
            {
                if (!_mapPathInfo[mapId].ContainsKey(key))
                {
                    _mapPathInfo[mapId].Add(key, path);
                }
            }
            else
            {
                pathInfo.Add(key, path);
                _mapPathInfo.Add(mapId, pathInfo);
            }
        }

        public List<float> GetMapPathInfo(int mapId, string key)
        {
            if (_mapPathInfo.ContainsKey(mapId))
            {
                var currpathInfo = _mapPathInfo[mapId];
                if (currpathInfo.ContainsKey(key))
                {
                    return currpathInfo[key];
                }
            }
            return null;
        }

		public List<int> GetNearestPath(Dictionary<int, MovePathData> pathDataDict, int start, int end)
		{
			Dictionary<int, float> distanceDict = new Dictionary<int, float>();
			Dictionary<int, int> previousDict = new Dictionary<int, int>();
			HashSet<int> visitSet = new HashSet<int>();
            int nearest2Start = start;
            int nearest2End = end;
            InitData(pathDataDict, distanceDict, previousDict, start, end, ref nearest2Start, ref nearest2End);

			int pointCount = pathDataDict.Count;
            //Debug.LogError("pointCount:" + pointCount);
			for (int i = 1; i < pointCount; i++)
			{
				float minDistance = float.MaxValue;
				int currentIndex = start;
				foreach (KeyValuePair<int, float> kvp in distanceDict)
				{   //在未访问的点中找出离源点距离最小的点
					if (minDistance > kvp.Value && !visitSet.Contains(kvp.Key))
					{
						minDistance = kvp.Value;
						currentIndex = kvp.Key;
					}
				}
				visitSet.Add(currentIndex);
				if (currentIndex == end)
				{
					break;
				}
				float currentDistance = distanceDict[currentIndex];
				if (!pathDataDict.ContainsKey(currentIndex)) continue;
				MovePathData currentPathData = pathDataDict[currentIndex];
                RecalculateDistance(pathDataDict, start, end, distanceDict, previousDict, nearest2Start, nearest2End, currentIndex, currentDistance, currentPathData);
			}
			return GetPathList(start, end, previousDict);
		}

        private static void RecalculateDistance(Dictionary<int, MovePathData> pathDataDict, int start, int end, Dictionary<int, float> distanceDict, Dictionary<int, int> previousDict, int nearest2Start, int nearest2End, int currentIndex, float currentDistance, MovePathData currentPathData)
        {
            if (currentPathData.id == start)
            {
                int visitIndex = nearest2Start;
                if (currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos) < distanceDict[visitIndex])
                {
                    distanceDict[visitIndex] = currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos);
                    previousDict[visitIndex] = currentIndex;
                }
            }
            else if (currentPathData.id == nearest2End)
            {
                int visitIndex = end;
                if (currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos) < distanceDict[visitIndex])
                {
                    distanceDict[visitIndex] = currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos);
                    previousDict[visitIndex] = currentIndex;
                }
            }
            else
            {
                string[] nextPointList = currentPathData.relationId.Split(',');
                foreach (string relationId in nextPointList)
                {
                    int visitIndex = int.Parse(relationId);
                    if (pathDataDict.ContainsKey(visitIndex) == false) continue;
                    if (currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos) < distanceDict[visitIndex])
                    {
                        distanceDict[visitIndex] = currentDistance + Vector3.Distance(pathDataDict[visitIndex].pathPos, currentPathData.pathPos);
                        previousDict[visitIndex] = currentIndex;
                    }
                }
            }
        }
		
		private static List<int> GetPathList(int start, int end, Dictionary<int, int> previousDict)
		{
			List<int> pathList = new List<int>();
			int pathIndex = end;
			
			while (true)
			{
				pathList.Add(pathIndex);
				pathIndex = previousDict[pathIndex];
				
				if (pathIndex == start)
				{
					break;
				}
			}
			pathList.Add(start);
			pathList.Reverse();
			return pathList;
		}

        private static void InitData(Dictionary<int, MovePathData> pathDataDict, Dictionary<int, float> distanceDict, Dictionary<int, int> previousDict, int start, int end, ref int nearest2Start, ref int nearest2End)
		{
            if (!pathDataDict.ContainsKey(start) || !pathDataDict.ContainsKey(end))
            {
                LoggerHelper.Error("寻路失败，找不到Key值:" + "pathDataDict不存在该key值");
                PlayerActionManager.GetInstance().StopAllAction();
                return;
            }
			MovePathData startData = pathDataDict[start];
			MovePathData endData = pathDataDict[end];
			float distance2Start = float.MaxValue;
			float distance2End = float.MaxValue;
			foreach (KeyValuePair<int, MovePathData> kvp in pathDataDict)
			{
				distanceDict.Add(kvp.Key, float.MaxValue);
				previousDict.Add(kvp.Key, start);
				if (kvp.Key != start && Vector3.Distance(kvp.Value.pathPos, startData.pathPos) < distance2Start)
				{
					distance2Start = Vector3.Distance(kvp.Value.pathPos, startData.pathPos);
					nearest2Start = kvp.Key;
				}
				if (kvp.Key != end && Vector3.Distance(kvp.Value.pathPos, endData.pathPos) < distance2End)
				{
					distance2End = Vector3.Distance(kvp.Value.pathPos, endData.pathPos);
					nearest2End = kvp.Key;
				}
			}
			distanceDict[start] = 0;
		}

        public List<float> GetPath(float x1, float z1, float x2, float z2, bool bFly = false)
        {
            List<float> result = null;
            if (!(_scene is GameScene)) return result;
            result = (_scene as GameScene).GetPath(x1, z1, x2, z2, bFly);
            return result;
        }

        public bool CheckCurrPointIsCanMove(float posx, float posz, bool bFly = false)
        {
            if (!(_scene is GameScene)) return true;
            return (_scene as GameScene).CheckCurrPointIsCanMove(posx, posz, bFly);
        }

        public Vector3 CheckPointAroundCanMove(float posx, float posz, bool bFly = false, bool largeRangeSearch = false)
        {
            if (!(_scene is GameScene)) return Vector3.zero;
            return (_scene as GameScene).CheckPointAroundCanMove(posx, posz, bFly, largeRangeSearch);
        }

        public byte[,] GetBlockMapData()
        {
            if (!(_scene is GameScene)) return null;
            return (_scene as GameScene).GetBlockMapData();
        }

        public EntityData CurrActionEntityData(int id)
        {
            return _scene.GetActionData(id);
        }

        public void CreateActions(string actionId, Vector3 postion, uint entityId = 0)
        {
            _scene.CreateActions(actionId, postion, entityId);
        }

        public void TermiateAction(int actionId)
        {
            if (_scene is GameScene)
            {
                (_scene as GameScene).TerminateActions(actionId);
            }
        }

        public void TerminateAllActions()
        {
            if (_scene is GameScene)
            {
                (_scene as GameScene).TerminateAllActions();
            }
        }

        public void LeaveCombatScene()
        {
            /*
            //Debug.LogError("LeaveCombatScene");
            if (PlayerDataManager.Instance.TaskData.isTeammateInTeamTask())//在时空裂隙中，并且自己不是队长
            {
                string content =  MogoLanguageUtil.GetContent(115020);
          //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, delegate()
                {
                    if (_scene is CombatScene)
                    {
                        (_scene as CombatScene).RequestLeaveCombat();
                    }
                });
            }
            else
            {
                if (_scene is CombatScene)
                {
                    (_scene as CombatScene).RequestLeaveCombat();
                }
            }*/
        }
        
        public EntityCampData GetCampInfo(int key)
        {
            if (_scene is GameScene)
            {
                return (_scene as GameScene).GetCampInfo(key);
            }
            return null;
        }

        public void SetCurrAutoMovePoint(string point)
        {
            if (_scene is GameScene)
            {
                (_scene as GameScene).SetCurrAutoMovePoint(point);
            }
        }

        public Vector3 GetAutoMovePoint()
        {
            if (_scene is GameScene)
            {
                return (_scene as GameScene).GetAutoMovePoint();
            }
            return Vector3.zero;
        }

        public void DoRelive()
        {
            LuaTable table = new LuaTable();
            table.Add(1, 1);
            PlayerAvatar.Player.RpcCall("mission_action_req", action_config.ACTION_REVIVE, table);
        }

        public void NoticeServePlayerDeath()
        {
            LuaTable table = new LuaTable();
            PlayerAvatar.Player.RpcCall("mission_action_req", action_config.ACTION_DIE_CLIENT, table);
        }

        public void SetRelive()
        {
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).SetRelive();
            }
        }

        public void Relive(UInt16 cd, int reliveRemainNum)
        {
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).OnShowRelivePanel(cd, reliveRemainNum);
            }
        }

        public void AddInspectingCache(int type, int actionId)
        {
            if (_scene is GameScene)
            {
                (_scene as GameScene).AddInspectingCache(type, actionId);
            }
        }

        public int GetAutoFightAiId()
        {
            return map_helper.GetMap(GameSceneManager.GetInstance().curMapID).__play_ai;
        }

        public int GetFollowFightAiId()
        {
            return map_helper.GetMap(GameSceneManager.GetInstance().curMapID).__follow_fight_ai;
        }

        public bool IsServerMonster()
        {
            if (_scene is GameScene)
            {
                return (_scene as GameScene).IsServerMonster();
            }
            return false;
        }

        public void SetSceneObjVisible(string name, bool state)
        {
            var sceneGameObjectList = sceneGameObjects;
            if (sceneGameObjectList == null) return;
            for (int i = 0; i < sceneGameObjectList.Length; i++)
            {
                var obj = sceneGameObjectList[i];
                if (obj == null)continue;
                var child = obj.transform.FindChild(name);
                if (child) child.gameObject.SetActive(state);
            }
        }

        public void SetTerrainEnabled( bool state)
        {
            var sceneGameObjectList = sceneGameObjects;
            if (sceneGameObjectList == null) return;
            for (int i = 0; i < sceneGameObjectList.Length; i++)
            {
                var obj = sceneGameObjectList[i];
                if (obj == null) continue;
                var terrain = obj.GetComponentInChildren<Terrain>();
                if (terrain) terrain.enabled = state;
            }
        }

        public void ElevatorMove(int id, float[] durationArray, string openWall)
        {
            if (!(_scene is GameScene) || (_scene as GameScene).elevatorManager == null)
            {
                return;
            }
            (_scene as GameScene).elevatorManager.ElevatorMove(id, durationArray, openWall);
        }

        public void ElevatorMoveBack(int id, float[] durationArray, string openWall)
        {
            if (!(_scene is GameScene) || (_scene as GameScene).elevatorManager == null)
            {
                return;
            }
            (_scene as GameScene).elevatorManager.ElevatorMoveBack(id, durationArray, openWall);
        }

        public bool IsElevatorInStartPos(int id)
        {
            if (!(_scene is GameScene) || (_scene as GameScene).elevatorManager == null)
            {
                return false;
            }
            return (_scene as GameScene).elevatorManager.IsElevatorInStartPos(id);
        }

        public void ShowElevator(int id, bool visible)
        {
            if (!(_scene is GameScene) || (_scene as GameScene).elevatorManager == null)
            {
                return;
            }
            (_scene as GameScene).elevatorManager.ShowElevator(id, visible);
        }

        public void ResetElevator(int id)
        {
            if (!(_scene is GameScene) || (_scene as GameScene).elevatorManager == null)
            {
                return;
            }
            (_scene as GameScene).elevatorManager.ResetElevator(id);
        }

        public void OnRecieveCopyResult()
        {
            if (_scene.IsClientScene())
            {
                return;
            }
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).TerminateAllActions();
            }
        }

        public int GetQualitySetting(int type)
        {
            if (_scene is GameScene)
            {
                return (_scene as GameScene).GetQualitySetting(type);
            }
            return 0;
        }

        //初始化当前场景所有怪物的坐标信息
        protected string[] point = null;
        
        public void SetSceneDummyPosInfo(EntitySpawnMonsterActionData data)
        {
            Dictionary<int, int> randomNums = new Dictionary<int, int>();
            string[] monsterIds = data.monster_ids_l.Split(',');
            string[] monsterNums = data.monster_nums_l.Split(',');
            string[] str = data.monster_ponits_l.Split(',');
            string[] ais = null;
            Vector3 vectorPos = Vector3.zero;
            if (!string.IsNullOrEmpty(data.monster_ais_l))
            {
                ais = data.monster_ais_l.Split(',');
            }

            int currMonsterNums = 0;
            for (int currMonsterNumsIndex = 0; currMonsterNumsIndex < monsterNums.Length; currMonsterNumsIndex++)
            {
                currMonsterNums += System.Convert.ToInt32(monsterNums[currMonsterNumsIndex]);
            }

            int total = str.Length / 3;
            int randomTotal = total;
            if (currMonsterNums > total)
            {
                LoggerHelper.Error("刷怪点数量不足，请添加" + (currMonsterNums - total) + "组坐标" + "ID:" + data.id_i);
            }
            int k = 0;
            //保持格式只有“,”号，在此重新整理数据，3个为一组重新存储
            for (int index = 0; index < total; index++)
            {
                str[index] = str[k] + "," + str[k + 1] + "," + str[k + 2];
                k += 3;
            }
            
            int key = 0;
            point = null;
            for (int j = 0; j < monsterIds.Length; j++)
            {
                if (string.IsNullOrEmpty(monsterIds[j]))
                {
                    continue;
                }

                for (int i = 0; i < System.Convert.ToInt32(monsterNums[j]); i++)
                {
                    key += 1;
                        
                    switch (data.monster_point_type_i)
                    {
                        case (int)MonsterPointType.POSITION:       //定点刷怪
                            SetPoint(str, key);
                            break;
                        case (int)MonsterPointType.RANDOM_POSITION:
                            SetRandomPoint(str, key, randomTotal,randomNums);              //随机刷怪
                            break;
                        default:
                            LoggerHelper.Error("刷怪类型错误，请假查配置");
                            return;
                    }
                   
                    vectorPos = new Vector3(float.Parse(point[0]), float.Parse(point[1]), float.Parse(point[2]));
                    if (_sceneDummyInfo.ContainsKey(vectorPos))
                    {
                        _sceneDummyInfo.Remove(vectorPos);
                    }
                    _sceneDummyInfo.Add(vectorPos, int.Parse(monsterIds[j]));
                }
            }
        }

        public Dictionary<Vector3, int> GetCurrSceneDummyPosInfo()
        {
            return _sceneDummyInfo;
        }

        public void ClearSceneDummyPosInfo()
        {
            if (_sceneDummyInfo != null)
            {
                _sceneDummyInfo.Clear();
				_sceneDummyInfo = new Dictionary<Vector3, int>();
            }
        }

        private void SetPoint(string[] str, int key)
        {
            point = str[key - 1].Split(',');
        }

        private void SetRandomPoint(string[] str, int key, int randomTotal, Dictionary<int, int> randomNums)
        {
            System.Random random = new System.Random();
            int tmp = random.Next(0, randomTotal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, randomTotal, random, tmp, randomNums);
            }
            randomNums.Add(tmp, tmp);
            point = str[tmp].Split(',');
        }
        
        //检测随机数是否重复，避免坐标
        public int getNum(int minVal, int maxVal, System.Random ra, int tmp, Dictionary<int, int> randomNums)
        {
            tmp = ra.Next(minVal, maxVal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, maxVal, ra, tmp, randomNums);
            }

            return tmp;
        }

        public void CacheSpaceData(string spaceName, SecurityElement xmlDoc)
        {
            if (_cacheSpaceData.ContainsKey(spaceName))
            {
                _cacheSpaceData.Remove(spaceName);
            }
            _cacheSpaceData.Add(spaceName, xmlDoc);
        }

        public SecurityElement GetCacheSpaceData(string spaceName)
        {
            if (_cacheSpaceData.ContainsKey(spaceName))
            {
                return _cacheSpaceData[spaceName];
            }
            else
            {
                return null;
            }
        }

        public void NoticeMissionEnd()
        {
            EventDispatcher.TriggerEvent(SpaceActionEvents.SpaceActionEvents_MissionEnd);
        }
    }
}
