﻿using GameMain.Entities;
using MogoEngine;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceSystem
{
    public enum EntityActionType    //行为类型
    {
        SERVER = 0,                 //服务端行为
        CLIENT = 1,                 //客户端行为
        SERVER_TO_CLIENT = 2,       //服务端记录并通知客户端行为
    }

    public enum MonsterPointType    //刷怪类型
    {
        POSITION = 0,               //对应坐标刷怪
        RANDOM_POSITION = 1         //随机坐标刷怪
    }

    /*public enum ConditionType       //条件类型
    {
        KILL_MONSTER = 1,
        PICK_UP_ITEM = 16,
        USE_SKILL_ACTION = 81,
        SPACE_ACTION = 101,
        MONSTER_HP = 6,
        RELIVE = 41,
        PART_BROKEN = 56,
        ACCEPT_TASK = 998,
        FINISH_TASK = 999
    }*/

    public enum ConditionType       //条件类型
    {
        CONDITION_OR_TYPE = 0,
        CONDITION_AND_TYPE = 1
    }

    public enum ChangeAiType     //更改Ai
    {
        CHANGE_AI_ALL_DUMMY = 0,
        CHANGE_AI_APPOINT_DUMMY = 1,
        CHANGE_AI_UNAPPOINT_DUMMY = 2,
        CHANGE_AI_APPOINT_ACTION_DUMMY = 3
    }

    public enum CollectItemType  //回收物品
    {
        COLLECT_APPOINT_ITEM = 0,
        COLLECT_UNAPPOINT_ITEM = 1
    }

    public enum CollectMonsterType  //回收怪物
    {
        COLLECT_TRIGER_DUMMY =0,
        COLLECT_APPOINT_DUMMY = 1,
        COLLECT_UNAPPOINT_DUMMY =2,
        COLLECT_APPOINT_ACTION_DUMMY =3
    }

    public enum CollectMonsterStateType //回收怪物状态
    {
        COLLECT_DUMMY_DIE = 0,
        COLLECT_DUMMY_ALIVE = 1
    }

    public enum ShowNoticeType //是否提示公告
    {
        SHOW_UNNOTICE = 0,
        SHOW_NOTICE = 1,
        SHOW_COUNT_NOTICE =2
    }

    public enum JudgeType //判断行为类型
    {
        JUDGE_CAMP_ID = 0,
        JUDGE_CAMP_PVE = 1,
        JUDGE_CAMP_PVP = 2
    }

    public enum CreatNpcType //创建NPC
    {
        CREAT_NPC_APPOINT_POINT = 0,
        CREAT_NPC_RANDOM_POINT = 1
    }

    public enum RandomType //随机行为
    {
        RANDOM_IS_CAN_REPEAT = 0,
        RANDOM_IS_NOT_REPEAT = 1
    }

    public enum SendBuffTypeType  //发送BUFF
    {
        SEND_BUFF_APPONIT_CAMP_ID = 0,
        SEND_BUFF_APPONIT_MONSTER_ID = 1,
        SEND_BUFF_UNAPPONIT_MONSTER_ID = 2,
        SEND_BUFF_PLAYER = 5,
        SEND_BUFF_APPONIT_REGION = 6
    }

    public enum TeleportSpaceType  //传送
    {
        TELEPORT_TRIGER = 0,
        TELEPORT_APPOINT_CAMP_ID = 1,
        TELEPORT_APPOINT_PVE = 2,
        TELEPORT_APPOINT_PVP = 3
    }

    public enum PlayTalkTargetType  //说话行为目标类型
    {
        TRIGGER_ENTITY = 0,
        NPC,
        MONSTER,
        PLAYER,
        PLAYER_PET
    }
}
