﻿using GameMain.Entities;
using MogoEngine;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class ActionEntitiesUtils
    {
        //20,20,20的字符串格式转Vector3
        static public Vector3 StringToVector3(string value)
        {
            var splited = value.Split(',');
            return new Vector3(float.Parse(splited[0]), float.Parse(splited[1]), float.Parse(splited[2]));
        }

        //通过怪物配置id拿到场景中找到的第一只怪物
        static public EntityMonsterBase GetFirstMonsterFromMonsterID(uint monsterId)
        {
            var enumevator = MogoWorld.Entities.GetEnumerator();
            EntityMonsterBase monster;
            while (enumevator.MoveNext())
            {
                if (!(enumevator.Current.Value is EntityMonsterBase))
                {
                    continue;
                }
                monster = enumevator.Current.Value as EntityMonsterBase;
                if (monster.monster_id == monsterId)
                {
                    return monster;
                }
            }
            return null;
        }

        //相机通用获取跟踪目标方法
        public static Transform GetCameraTargetTransform(int targetId, string slotName)
        {
            EntityCreature creature;
            if (targetId == 0)
            {
                creature = PlayerAvatar.Player as EntityCreature;
            }
            else if (targetId < 0)
            {
                creature = null;
            }
            else
            {
                creature = ActionEntitiesUtils.GetFirstMonsterFromMonsterID((uint)targetId);
            }
            if (string.IsNullOrEmpty(slotName))
            {
                slotName = "slot_camera";
            }
            Transform targetTransform = null;
            Transform targetSlotTransform = null;
            if (creature != null && creature.actor != null)
            {
                targetTransform = creature.actor.boneController.GetBoneByName(slotName);
                if (targetSlotTransform != null)
                {
                    targetTransform = targetSlotTransform;
                }
            }
            return targetTransform;
        }

        public static List<int> ParseListInt(string source)
        {
            List<int> result = null;
            if (!string.IsNullOrEmpty(source))
            {
                string[] strList = source.Split(',');
                result = new List<int>();
                for (int i = 0; i < strList.Length; i++)
                {
                    result.Add(int.Parse(strList[i]));
                }
            }
            return result;
        }

        static public List<EntityCreature> GetNPCListFromNPCID(uint npcId, uint count = 0)
        {
            List<EntityCreature> npcList = new List<EntityCreature>();
            if (count == 0)
            {
                return npcList;
            }
            var enumevator = MogoWorld.Entities.GetEnumerator();
            EntityNPC npc;
            while (enumevator.MoveNext())
            {
                if (!(enumevator.Current.Value is EntityNPC))
                {
                    continue;
                }
                npc = enumevator.Current.Value as EntityNPC;
                if (npc.npc_id == npcId)
                {
                    npcList.Add(npc);
                    if (npcList.Count >= count) break;
                }
            }
            return npcList;
        }

        static public List<EntityCreature> GetMonsterListFromMonsterID(uint monsterId, uint count = 0)
        {
            List<EntityCreature> monsterList = new List<EntityCreature>();
            if (count == 0)
            {
                return monsterList;
            }
            var enumevator = MogoWorld.Entities.GetEnumerator();
            EntityMonsterBase monster;
            while (enumevator.MoveNext())
            {
                if (!(enumevator.Current.Value is EntityMonsterBase))
                {
                    continue;
                }
                monster = enumevator.Current.Value as EntityMonsterBase;
                if (monster.monster_id == monsterId)
                {
                    monsterList.Add(monster);
                    if (monsterList.Count >= count) break;
                }
            }
            return monsterList;
        }
    }
}
