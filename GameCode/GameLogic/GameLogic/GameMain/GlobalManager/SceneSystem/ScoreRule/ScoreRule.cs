﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public enum CompareType
    {
        LESS_OR_EQUAL = 1,
        GREATER_OR_EQUAL
    }

    public enum SpaceActionTriggerState
    {
        NOT_TRIGGER = 1,
        TRIGGER
    }

    public class ScoreRule
    {
        protected int _id;
        protected CompareType _type;
        protected bool _result;
        protected SystemNoticeCopyStarData _noticeData;

        public ScoreRule()
        {
            _noticeData = new SystemNoticeCopyStarData();
            _noticeData.type = InstanceDefine.COPY_STAR_CHAGED;
        }

        virtual public void Enter(int id, string[] args)
        {
            _id = id;
            _noticeData.actionId = _id;
            _type = (CompareType)(int.Parse(args[1]));
            _result = (_type == CompareType.LESS_OR_EQUAL) ? true : false;
        }

        virtual public void Exit()
        {
        }

        virtual public void Start()
        {
        }

        virtual public void Update()
        {
        }

        virtual public void Update(object data)
        {
        }

        virtual public object GetData()
        {
            return null;
        }

        public int Id
        {
            get
            {
                return _id;
            }
        }

        protected bool Compare(int value, int targetValue)
        {
            if (_type == CompareType.LESS_OR_EQUAL)
            {
                return value <= targetValue;
            }
            else
            {
                return value >= targetValue;
            }
        }

        protected bool Compare(float value, float targetValue)
        {
            if (_type == CompareType.LESS_OR_EQUAL)
            {
                return value <= targetValue;
            }
            else
            {
                return value >= targetValue;
            }
        }

        protected void OnResultChange()
        {
            _noticeData.state = (_result ? 1 : 0);
            SpaceNoticeActionManager.Instance.AddSystemNotice(_noticeData);
        }
    }

    public class ScoreRulePassTime : ScoreRule
    {
        private float _startTime;
        private float _targetDuration;

        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetDuration = float.Parse(args[0]);
        }

        public override void Start()
        {
            _startTime = Time.realtimeSinceStartup;
        }

        public override void Update()
        {
            float duration = Time.realtimeSinceStartup - _startTime;
            bool result = Compare(duration, _targetDuration);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        public override object GetData()
        {
            return Time.realtimeSinceStartup - _startTime;
        }
    }

    public class ScoreRuleKillMonNum : ScoreRule
    {
        private int _killMonsterCount;
        private int _targetKillMonsterCount;

        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetKillMonsterCount = int.Parse(args[0]);
        }

        public void UpdateKillMonsterCount(int killMonsterCount)
        {
            _killMonsterCount = killMonsterCount;
            bool result = Compare(_killMonsterCount, _targetKillMonsterCount);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        public override object GetData()
        {
            return _killMonsterCount;
        }
    }

    public class ScoreRuleBloodRate : ScoreRule
    {
        private float _targetPercent;
        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetPercent = Mathf.Abs(float.Parse(args[0]));
        }

        public override void Update()
        {
            float percent = GetPercent();
            bool result = Compare(percent, _targetPercent);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        private float GetPercent()
        {
            if (PlayerAvatar.Player.max_hp == 0)
            {
                return 0;
            }
            return (float)PlayerAvatar.Player.cur_hp / (float)PlayerAvatar.Player.max_hp;
        }

        public override object GetData()
        {
            return GetPercent();
        }
    }

    public class ScoreRuleCastSpellNum : ScoreRule
    {
        private int _targetSpellNum;
        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetSpellNum = int.Parse(args[0]);
        }

        public override void Update()
        {
            int spellNum = GetSpellNum();
            bool result = Compare(spellNum, _targetSpellNum);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        private int GetSpellNum()
        {
            return PlayerAvatar.Player.skillManager.GetUseSkillNum();
        }

        public override object GetData()
        {
            return GetSpellNum();
        }
    }

    public class ScoreRuleDeathNum : ScoreRule
    {
        private int _deathCount;
        private int _targetDeathCount;

        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetDeathCount = int.Parse(args[0]);
        }

        public void UpdateDeathCount(int deathCount)
        {
            _deathCount = deathCount;
            bool result = Compare(_deathCount, _targetDeathCount);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        public override object GetData()
        {
            return _deathCount;
        }
    }

    public class ScoreRuleBossBloodRate : ScoreRule
    {
        private uint _id;
        private float _targetPercent;

        public override void Enter(int id, string[] args)
        {
            base.Enter(id, args);
            _targetPercent = Mathf.Abs(float.Parse(args[0]));
        }

        public override void Update()
        {
            float percent = GetPercent();
            bool result = Compare(percent, _targetPercent);
            if (result != _result)
            {
                _result = result;
                OnResultChange();
            }
        }

        private float GetPercent()
        {
            EntityCreature creature = MogoWorld.GetEntity(_id) as EntityCreature;
            if (creature.max_hp == 0)
            {
                return 0;
            }
            return (float)creature.cur_hp / (float)creature.max_hp;
        }

        public override object GetData()
        {
            return GetPercent();
        }

        public void OnSpawnMonster(uint id)
        {
            if (_id > 0)
            {
                return;
            }
            _id = id;
        }
    }

    public class ScoreRuleSpaceAction : ScoreRule
    {
        public int _actionId = 0;
        private bool _reach;
        private SpaceActionTriggerState _triggerState;

        public override void Enter(int id, string[] args)
        {
            _id = id;
            _noticeData.actionId = id;
            _actionId = int.Parse(args[0]);
            _triggerState = (SpaceActionTriggerState)int.Parse(args[1]);
            _result = (_triggerState == SpaceActionTriggerState.NOT_TRIGGER) ? true : false;
            _reach = false;
        }

        public void UpdateCreateActionState()
        {
            if (_reach) return;
            _result = (_triggerState == SpaceActionTriggerState.TRIGGER) ? true : false;
            _reach = true;
        }
    }
}
