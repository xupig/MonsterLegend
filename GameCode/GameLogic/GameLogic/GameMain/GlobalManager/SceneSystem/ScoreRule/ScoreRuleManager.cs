﻿using Common.ServerConfig;
using GameData;
using GameLoader.Utils.CustomType;
using MogoEngine;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class ScoreRuleActionCounter
    {
        private int _actionId;
        private bool _isTriggered;
        private List<int> _ruleIdList;

        public ScoreRuleActionCounter(int actionId)
        {
            _actionId = actionId;
            _isTriggered = false;
            _ruleIdList = new List<int>();
        }

        ~ScoreRuleActionCounter()
        {
            _ruleIdList.Clear();
            _ruleIdList = null;
        }

        public void SetTriggered()
        {
            _isTriggered = true;
        }

        public bool isTriggered
        {
            get
            {
                return _isTriggered;
            }
        }

        public List<int> ruleIdList
        {
            get
            {
                return _ruleIdList;
            }
        }

        public void AddRuleId(int ruleId)
        {
            _ruleIdList.Add(ruleId);
        }
    }

    public class ScoreRuleData
    {
        public static ScoreRuleData s_Instance;
        public static ScoreRuleData Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new ScoreRuleData();
                }
                return s_Instance;
            }
        }

        public LuaTable tableNormal;
        public LuaTable tableSpecial;

        ScoreRuleData()
        {
            tableNormal = new LuaTable();
            tableSpecial = new LuaTable();
        }

        public void AddNormalMsg(int ruleId, object data)
        {
            tableNormal.Add(ruleId, data);
        }

        public void AddSpecialMsg(int actionId, int ruleId)
        {
            tableSpecial.Add(actionId, ruleId);
        }

        public void Clear()
        {
            tableNormal.Clear();
            tableSpecial.Clear();
        }
    }

    public class ScoreRuleManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ScoreRuleManager
    {
        private CombatScene _scene;
        private Dictionary<int, ScoreRule> _scoreRuleDict;
        private Dictionary<int, ScoreRuleActionCounter> _scoreRuleActionCounterDict;
        private int _killMonsterCount = 0;
        private int _playerDeathCount = 0;

        public ScoreRuleManager(CombatScene scene)
        {
            _scene = scene;
            _scoreRuleDict = new Dictionary<int, ScoreRule>();
            _scoreRuleActionCounterDict = new Dictionary<int, ScoreRuleActionCounter>();
            Init();
        }

        public void Exit()
        {
            MogoWorld.UnregisterUpdate("ScoreRuleManager.Update", Update);
            _scene = null;
            foreach (var node in _scoreRuleDict)
            {
                node.Value.Exit();
            }
            _scoreRuleDict.Clear();
            _scoreRuleDict = null;
            _scoreRuleActionCounterDict.Clear();
            _scoreRuleActionCounterDict = null;
        }

        public void OnCombatStart()
        {
            _killMonsterCount = 0;
            _playerDeathCount = 0;
            if (_scene.IsClientScene())
            {
                foreach (var node in _scoreRuleDict)
                {
                    node.Value.Start();
                }
            }
            MogoWorld.RegisterUpdate<ScoreRuleManagerUpdateDelegate>("ScoreRuleManager.Update", Update);
        }

        private void Update()
        {
            foreach (var node in _scoreRuleDict)
            {
                node.Value.Update();
            }
        }

        private void Init()
        {
            ScoreRuleData.Instance.Clear();
            Dictionary<string, string[]> scoleRuleData = map_helper.GetScoreRule(_scene.GetMapID());
            if (scoleRuleData == null) return;
            int ruleId = 0;
            ScoreRule rule;
            foreach (var info in scoleRuleData)
            {
                ruleId = int.Parse(info.Key);
                if (score_rule_helper.CheckIsActionRuleId(ruleId))
                {
                    rule = score_rule_helper.Create<ScoreRuleSpaceAction>(ruleId, info.Value);
                    int actionId = int.Parse(info.Value[0]);
                    ScoreRuleActionCounter actionCounter;
                    if (_scoreRuleActionCounterDict.ContainsKey(actionId) && _scoreRuleActionCounterDict[actionId] != null)
                    {
                        actionCounter = _scoreRuleActionCounterDict[actionId];
                    }
                    else
                    {
                        actionCounter = new ScoreRuleActionCounter(actionId);
                        _scoreRuleActionCounterDict.Add(actionId, actionCounter);
                    }
                    actionCounter.AddRuleId(ruleId);
                }
                else
                {
                    rule = score_rule_helper.CreateScoreRule(ruleId, info.Value);
                }
                if (rule != null)
                {
                    _scoreRuleDict.Add(ruleId, rule);
                }
            }
        }

        public ScoreRuleData GetBattleResultData()
        {
            foreach (var node in _scoreRuleDict)
            {
                if (score_rule_helper.CheckIsActionRuleId(node.Key))
                {
                    continue;
                }
                ScoreRuleData.Instance.AddNormalMsg(node.Key, node.Value.GetData());
            }
            foreach (var node in _scoreRuleActionCounterDict)
            {
                if (node.Value.isTriggered)
                {
                    List<int> ruleIdList = node.Value.ruleIdList;
                    for (int i = 0; i < ruleIdList.Count; ++i)
                    {
                        ScoreRuleData.Instance.AddSpecialMsg(node.Key, ruleIdList[i]);
                    }
                }
            }
            return ScoreRuleData.Instance;
        }

        public void SetKillMonsterCount(int monsterNum)
        {
            _killMonsterCount += monsterNum;
            foreach (var node in _scoreRuleDict)
            {
                if (node.Value.Id == public_config.MISSION_ACTION_KILL_MON_NUM)
                {
                    (node.Value as ScoreRuleKillMonNum).UpdateKillMonsterCount(_killMonsterCount);
                }
            }
        }

        public void SetPlayerDeathCount()
        {
            _playerDeathCount++;
            foreach (var node in _scoreRuleDict)
            {
                if (node.Value.Id == public_config.MISSION_ACTION_DIE_NUM)
                {
                    (node.Value as ScoreRuleDeathNum).UpdateDeathCount(_playerDeathCount);
                }
            }
        }

        public void OnSpawnMonster(uint id, uint monsterId)
        {
            if (!IsBoss(monsterId))
            {
                return;
            }
            foreach (var node in _scoreRuleDict)
            {
                if (node.Value.Id == public_config.MISSION_ACTION_BOSS_BLOOD_RATE)
                {
                    (node.Value as ScoreRuleBossBloodRate).OnSpawnMonster(id);
                }
            }
        }

        private bool IsBoss(uint id)
        {
            int monsterType = monster_helper.GetMonsterType((int)id);
            return monsterType == public_config.MONSTER_TYPE_BOSS;
        }

        public void OnCreateAction(int actionId)
        {
            if (_scoreRuleActionCounterDict.ContainsKey(actionId))
            {
                _scoreRuleActionCounterDict[actionId].SetTriggered();
                List<int> ruleIdList = _scoreRuleActionCounterDict[actionId].ruleIdList;
                for (int i = 0; i < ruleIdList.Count; ++i)
                {
                    _scoreRuleDict[ruleIdList[i]].Update();
                }
            }
        }
    }
}
