﻿using Common.ServerConfig;
using GameData;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class score_rule_helper
    {
        public static ScoreRule CreateScoreRule(int type, string[] args)
        {
            ScoreRule condition = null;
            switch (type)
            {
                case public_config.MISSION_ACTION_PASS_TIME:
                    condition = Create<ScoreRulePassTime>(type, args);
                    break;
                case public_config.MISSION_ACTION_KILL_MON_NUM:
                    condition = Create<ScoreRuleKillMonNum>(type, args);
                    break;
                case public_config.MISSION_ACTION_BLOOD_RATE:
                    condition = Create<ScoreRuleBloodRate>(type, args);
                    break;
                case public_config.MISSION_ACTION_CAST_SPELL_NUM:
                    condition = Create<ScoreRuleCastSpellNum>(type, args);
                    break;
                default:
                    break;
            }
            return condition;
        }

        public static int GetScoreRuleInitState(int type)
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);

            switch (type)
            {
                case public_config.MISSION_ACTION_PASS_TIME:
                case public_config.MISSION_ACTION_DIE_NUM:
                case public_config.MISSION_ACTION_KILL_MON_NUM:
                case public_config.MISSION_ACTION_CAST_SPELL_NUM:
                case public_config.MISSION_ACTION_CAMP_SCORE:
                    return dicScoreRule[type.ToString()][1] == "1" ? 1 : 0;
                case public_config.MISSION_ACTION_BLOOD_RATE:
                    return dicScoreRule[type.ToString()][1] == "2" ? 1 : 0;
                default:
                    return dicScoreRule[type.ToString()][1] == "1" ? 1 : 0;
            }
        }

        public static T Create<T>(int ruleId, string[] args) where T : ScoreRule, new()
        {
            T t = new T();
            t.Enter(ruleId, args);
            return t;
        }

        public static bool CheckIsActionRuleId(int ruleId)
        {
            return ruleId >= 1000;
        }
    }
}
