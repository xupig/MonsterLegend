﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityAiMoveAction : BHEntityBase
    {
        new public EntityAiMoveActionData data { get { return _data as EntityAiMoveActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            //Debug.LogError("ai_move_point_l:" + data.ai_move_point_l);
            GameMain.GlobalManager.GameSceneManager.GetInstance().SetCurrAutoMovePoint(data.ai_move_point_l);
            Finish();
        }

    }
}
