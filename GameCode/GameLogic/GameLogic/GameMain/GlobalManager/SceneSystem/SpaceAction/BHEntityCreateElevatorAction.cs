﻿using Game.Asset;
using SpaceSystem;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityCreateElevatorAction : BHEntityBase
    {
        new public EntityCreateElevatorActionData data { get { return _data as EntityCreateElevatorActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            string path = data.model_path_s + ".prefab";
            var gObj = this.gameObject;
            Action<GameObject> elevatorGoCallback = delegate(GameObject obj)
            {
                if (gObj == null) 
                {
                    GameObject.Destroy(obj);
                    return; 
                }
                _scene.elevatorManager.AddElevator(data.elevator_id_i, obj, data.open_wall_s, data.positions_l);
                Finish();
            };
            ObjectPool.Instance.GetGameObject(path, elevatorGoCallback);
        }
    }
}
