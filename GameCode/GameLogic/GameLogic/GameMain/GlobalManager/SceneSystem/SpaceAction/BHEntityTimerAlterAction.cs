﻿using Common.Events;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityTimerAlterAction : BHEntityBase
    {
        new public EntityTimerAlterActionData data { get { return _data as EntityTimerAlterActionData; } }
        protected bool hasTriggered = false;

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered) return;
            hasTriggered = true;
            if (_owner.ExistAction(data.target_id_i))
            {
                string[] str = new string[3];
                str[0] = (data.target_id_i).ToString();
                str[1] = data.change_type_i.ToString();
                str[2] = data.delay_time_i.ToString();
                EventDispatcher.TriggerEvent<string[]>(SpaceActionEvents.SpaceActionEvents_ChangeTimer, str);
                _owner.CreateActions(data.success_actions_l, gameObject.transform.position);
            }
            else
            {
                _owner.CreateActions(data.fail_actions_l, gameObject.transform.position);
            }
            Finish();
        }
    }
}
