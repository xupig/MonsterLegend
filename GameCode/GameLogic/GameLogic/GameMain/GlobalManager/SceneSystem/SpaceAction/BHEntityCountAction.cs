﻿using Common.ClientConfig;
using Common.Events;
using GameMain.Entities;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityCountAction : BHEntityBase
    {
        new public EntityCountActionData data { get { return _data as EntityCountActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            switch(data.target_type_i)
            {
                case 0:
                    EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_SCORE, data.count_type_i.ToString() + ":" + data.count_type_arg_i.ToString() + ":" + data.target_type_arg_i.ToString());
                    break;
                case 2:
                    var player = GameMain.Entities.PlayerAvatar.Player as EntityAvatar;
                    if (player.GetCampPveType() == data.target_type_arg_i)
                    {
                        EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_PVE_SCORE, data.count_type_i.ToString() + ":" + data.count_type_arg_i.ToString() + ":" + data.target_type_arg_i.ToString());
                    }
                    break;
            }
            
            Finish();
        }
    }
}
