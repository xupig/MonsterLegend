﻿using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityTeleportSpaceAction : BHEntityBase
    {
        new public EntityTeleportSpaceActionData data { get { return _data as EntityTeleportSpaceActionData; } }
        protected Dictionary<uint, Entity> entities = new Dictionary<uint, Entity>();

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            //string[] monsterIds = null;
            entities = MogoEngine.MogoWorld.Entities;
            //if (!string.IsNullOrEmpty(data.target_ids_l))
            //{
            //    monsterIds = data.target_ids_l.Split(',');
            //}
            Vector2 pos = new Vector2(0, 0);
            if (data.origin_point_type_i == 0)
            {
                pos.x = data.pos_x_i / 100;
                pos.y = data.pos_z_i / 100;
            }
            else
            {
                pos.x = MogoEngine.MogoWorld.GetEntity(data.entity_id_i).position.x;
                pos.y = MogoEngine.MogoWorld.GetEntity(data.entity_id_i).position.z;
            }
            switch (data.target_type_i)
            {
                case (int)TeleportSpaceType.TELEPORT_TRIGER:
                    TeleportTriger(pos);
                    break;
                case (int)TeleportSpaceType.TELEPORT_APPOINT_CAMP_ID:
                    TeleportAppointCampId(pos);
                    break;
                case (int)TeleportSpaceType.TELEPORT_APPOINT_PVE:
                    TeleportAppointPve(pos);
                    break;
                case (int)TeleportSpaceType.TELEPORT_APPOINT_PVP:
                    TeleportAppointPvp(pos);
                    break;
                default:
                    break;
            }
            _owner.CreateActions(data.success_actions_l, gameObject.transform.position);
            Finish();
        }

        void TeleportTriger(Vector2 pos)
        {
            EntityCreature entity = MogoEngine.MogoWorld.GetEntity(data.entity_id_i) as EntityCreature;
            var entityPet = MogoEngine.MogoWorld.GetEntity(PlayerPetManager.GetInstance().curPetEntityID) as EntityCreature;
            var entityVehicle = GameSceneManager.GetInstance().Vehicle;
            if (entity != null)
            {
                entity.Teleport(new Vector3(pos.x, 0, pos.y));
            }
            if (entityVehicle != null)
            {
                entityVehicle.Teleport(new Vector3(pos.x, 0, pos.y));
            }
            if (entityPet != null)
            {
                entityPet.Teleport(new Vector3(pos.x + 1, 0, pos.y));
            }
        }

        void TeleportAppointCampId(Vector2 pos)
        {
            if (string.IsNullOrEmpty(data.target_ids_l))
            {
                LoggerHelper.Error("传送失败，缺少CAMPID");
                return;
            }
            List<int> campList = data_parse_helper.ParseListInt(data.target_ids_l.Split(','));
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (!(entity is EntityCreature))
                {
                    continue;
                }
                var creature = entity as EntityCreature;
                if (campList.Contains(creature.map_camp_id))
                {
                    creature.Teleport(new Vector3(pos.x, 0, pos.y));
                }
            }
        }

        void TeleportAppointPve(Vector2 pos)
        {
            if (string.IsNullOrEmpty(data.target_ids_l))
            {
                LoggerHelper.Error("传送失败，缺少PVEID");
                return;
            }

            List<int> pveList = data_parse_helper.ParseListInt(data.target_ids_l.Split(','));
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (!(entity is EntityCreature))
                {
                    continue;
                }
                var creature = entity as EntityCreature;
                if (pveList.Contains(creature.camp_pve_type))
                {
                    creature.Teleport(new Vector3(pos.x, 0, pos.y));
                }
            }
        }

        void TeleportAppointPvp(Vector2 pos)
        {
            if (string.IsNullOrEmpty(data.target_ids_l))
            {
                LoggerHelper.Error("传送失败，缺少PVPID");
                return;
            }

            List<int> pvpList = data_parse_helper.ParseListInt(data.target_ids_l.Split(','));
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (!(entity is EntityCreature))
                {
                    continue;
                }
                var creature = entity as EntityCreature;
                if (pvpList.Contains(creature.camp_pvp_type))
                {
                    creature.Teleport(new Vector3(pos.x, 0, pos.y));
                }
            }
        }

        protected override void OnExit()
        {
            data.entity_id_i = 0;
            base.OnExit();
        }
    }
}
