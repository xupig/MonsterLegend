﻿using Common.Data;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityPassWinAction : BHEntityBase
    {
        new public EntityPassWinActionData data { get { return _data as EntityPassWinActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).OnCombatComplete(true);
            }
            Finish();
        }
    }
}
