﻿using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;
using SpaceSystem;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityPlayTalkAction : BHEntityBase
    {
        new public EntityPlayTalkActionData data { get { return _data as EntityPlayTalkActionData; } }
        private bool _isShowSpeech = true;
        private bool _isPlaySound = true;

        protected override void OnStart()
        {
            if (!string.IsNullOrEmpty(data.talk_arg_l))
            {
                string[] talkArgs = data.talk_arg_l.Split(',');
                _isShowSpeech = Convert.ToBoolean(int.Parse(talkArgs[0]));
                _isPlaySound = Convert.ToBoolean(int.Parse(talkArgs[1]));
            }
            data.talk_num_i = (data.talk_num_i == 0) ? 1 : data.talk_num_i;
            Active();
        }

        protected override void OnActive()
        {
            if (!_isShowSpeech && !_isPlaySound)
            {
                Finish();
                return;
            }
            List<EntityCreature> entityList = GetEntityList();
            for (int i = 0; i < entityList.Count; ++i)
            {
                entityList[i].Speak(data.talk_content_i, _isShowSpeech, _isPlaySound);
            }
            Finish();
        }

        private List<EntityCreature> GetEntityList()
        {
            List<EntityCreature> entityList = new List<EntityCreature>();
            switch (data.target_type_i)
            {
                case (int)PlayTalkTargetType.TRIGGER_ENTITY:
                    EntityCreature creature = GetTriggerEntity();
                    if (creature != null) entityList.Add(creature);
                    break;
                case (int)PlayTalkTargetType.NPC:
                    List<EntityCreature> npcList = ActionEntitiesUtils.GetNPCListFromNPCID((uint)data.target_type_arg_i, (uint)data.talk_num_i);
                    entityList.AddRange(npcList);
                    break;
                case (int)PlayTalkTargetType.MONSTER:
                    List<EntityCreature> monsterList = ActionEntitiesUtils.GetMonsterListFromMonsterID((uint)data.target_type_arg_i, (uint)data.talk_num_i);
                    entityList.AddRange(monsterList);
                    break;
                case (int)PlayTalkTargetType.PLAYER:
                    entityList.Add(PlayerAvatar.Player);
                    break;
                case (int)PlayTalkTargetType.PLAYER_PET:
                    EntityPet pet = PlayerAvatar.Player.GetPet();
                    if (pet != null) entityList.Add(pet);
                    break;
                default:
                    break;
            }
            return entityList;
        }

        private EntityCreature GetTriggerEntity()
        {
            Entity entity = MogoWorld.GetEntity(data.entity_id_i);
            if (entity == null || !(entity is EntityCreature))
            {
                return null;
            }
            return entity as EntityCreature;
        }
    }
}
