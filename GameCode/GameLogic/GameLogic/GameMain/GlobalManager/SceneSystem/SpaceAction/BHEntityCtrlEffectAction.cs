﻿using Common.Events;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityCtrlEffectAction : BHEntityBase
    {
        new public EntityCtrlEffectActionData data { get { return _data as EntityCtrlEffectActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            EventDispatcher.TriggerEvent<int, int>(SpaceActionEvents.SpaceActionEvents_PlayEffect, data.target_effect_action_id_i, data.play_param_i);
            Finish();
        }
    }
}
