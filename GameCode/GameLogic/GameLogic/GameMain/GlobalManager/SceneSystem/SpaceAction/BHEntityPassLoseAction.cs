﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityPassLoseAction: BHEntityBase
    {
        new public EntityPassLoseActionData data { get { return _data as EntityPassLoseActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).OnCombatComplete(false);
            }
            Finish();
        }
    }
}
