﻿using ACTSystem;
using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityPlayBulletVisualFXAction : BHEntityBase
    {
        new public EntityPlayBulletVisualFXActionData data { get { return _data as EntityPlayBulletVisualFXActionData; } }
        private Vector3 _targetPosition;
        private bool _hasTriggered = false;

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (_hasTriggered)
            {
                return;
            }
            _hasTriggered = true;
            PlayBulletVisualFX();
        }

        private void PlayBulletVisualFX()
        {
            Vector3 startPosition = gameObject.transform.position;
            _targetPosition = ActionEntitiesUtils.StringToVector3(data.target_position_l);
            ACTVisualFXManager.GetInstance().PlayACTBullet(data.visual_fx_id_i,
                data.speed_f, startPosition, _targetPosition, data.acceleration_f, OnPlayBulletVisualFXFinish);
        }

        private void OnPlayBulletVisualFXFinish()
        {
            ACTVisualFXManager.GetInstance().PlayACTVisualFX(data.hit_visual_fx_i,
                _targetPosition, data.hit_visual_fx_duration_i);
            Finish();
        }
    }
}
