﻿using Common.ServerConfig;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using SpaceSystem;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public enum SpaceActionStatus : byte
    {
        NONE = 0,
        ON_ENTER,
        ON_START,
        ON_ACTIVE,
        ON_FINISH,
        ON_EXIT
    }

    public interface ISpaceAction
    {
        //Action唯一id
        int GetID();
        //Action行为id
        int GetActionID();
        //创建Action
        void Enter(int id, EntityData data, SpaceActionManager owner);
        //退出Action
        void Exit();
        //获取Action当前状态
        SpaceActionStatus GetStatus();
        //当前Action是否持续
        bool IsPersistent();
    }

    public class BHEntityBase : MonoBehaviour, ISpaceAction
    {
        protected SpaceActionManager _owner;
        protected GameScene _scene;
        protected int _id = 0;
        public static int TOLLGATE_SERVER_TYPE = 0;
        public static int TOLLGATE_CLINET_TYPE = 1;
        protected EntityData _data;
        public EntityData data { get { return _data; } }
        protected Vector3 _nextActionPosition = Vector3.zero;
        protected uint _nextActionEntityId = 0;
        protected SpaceActionStatus _status = SpaceActionStatus.NONE;
        public SpaceActionStatus status { get { return _status; } }
        protected bool _isPersistent = false;

        public int GetID()
        {
            return _id;
        }

        public int GetActionID()
        {
            return data.id_i;
        }

        public void Enter(int id, EntityData data, SpaceActionManager owner)
        {
            _id = id;
            _nextActionEntityId = data.entity_id_i;
            _data = data;
            _owner = owner;
            _scene = _owner.Scene;
            ChangeStatus(SpaceActionStatus.ON_ENTER);
            OnEnter();
        }

        public void Active()
        {
            ChangeStatus(SpaceActionStatus.ON_ACTIVE);
            OnActive();
        }

        public void Finish()
        {
            ChangeStatus(SpaceActionStatus.ON_FINISH);
            OnFinish();
            if (!_isPersistent)
            {
                Exit();
            }
        }

        public void Exit()
        {
            ChangeStatus(SpaceActionStatus.ON_EXIT);
            OnExit();
            _owner = null;
            GameObject.Destroy(gameObject);
        }

        public SpaceActionStatus GetStatus()
        {
            return _status;
        }

        public bool IsPersistent()
        {
            return _isPersistent;
        }

        virtual protected void OnEnter()
        {
        }

        virtual protected void OnStart()
        {
        }

        virtual protected void OnActive()
        {

        }

        virtual protected void OnFinish()
        {
        }

        virtual protected void OnExit()
        {
        }

        void Start()
        {
            _nextActionPosition = gameObject.transform.position;
            ChangeStatus(SpaceActionStatus.ON_START);
            OnStart();
        }

        void OnDestroy()
        {
        }

        virtual protected void TriggerNextActions()
        {
            if (NeedToInformNextActionsToServer())
            {
                InformNextActionsToServer();
                return;
            }

            _owner.AddInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_TRIGGER, data.id_i);
            _owner.TriggerNextActions(data.next_actions_l, _nextActionPosition, _nextActionEntityId, (uint)data.next_actions_delay_time_i);
        }

        private bool NeedToInformNextActionsToServer()
        {
            if (_scene.IsClientScene())
            {
                return false;
            }
            if (data.action_type_i == (int)EntityActionType.SERVER)
            {
                return false;
            }
            return data.is_finish_inform_sever_i > 0;
        }

        private void InformNextActionsToServer()
        {
            LuaTable table = new LuaTable();
            table.Add(1, data.id_i);
            PlayerAvatar.Player.RpcCall("space_action_req", action_config.ACTION_TRIGGER_NEXT_ACTIONS, table);
        }

        protected void ChangeStatus(SpaceActionStatus status)
        {
            if (_status != status)
            {
                _status = status;
				if (_owner != null)
				{
					_owner.OnActionStatusChange(_id, data.id_i, _status);
				}
			}
        }
    }
}
