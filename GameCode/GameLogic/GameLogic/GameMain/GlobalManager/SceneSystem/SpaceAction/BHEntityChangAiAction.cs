﻿using GameMain.Entities;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityChangAiAction : BHEntityBase
    {
        new public EntityChangeAiActionData data { get { return _data as EntityChangeAiActionData; } }

        protected string[] monsterIds = null;
        protected string[] aiIds = null;
        protected Dictionary<uint, Entity> entities = new Dictionary<uint, Entity>();

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            monsterIds = null;
            aiIds = null;
            entities = MogoEngine.MogoWorld.Entities;
                
            if (!string.IsNullOrEmpty(data.target_ids_l))
            {
                monsterIds = data.target_ids_l.Split(',');
            }
            if (!string.IsNullOrEmpty(data.target_args_l))
            {
                aiIds = data.target_args_l.Split(',');
            }

            switch (data.target_type_i)
            {
                case (int)ChangeAiType.CHANGE_AI_ALL_DUMMY:
                    ChangeAllDummyAI();
                    break;
                case (int)ChangeAiType.CHANGE_AI_APPOINT_DUMMY:
                    ChangeAppointDummyAI();
                    break;
                case (int)ChangeAiType.CHANGE_AI_UNAPPOINT_DUMMY:
                    ChangeUnAppointDummyAI();
                    break;
                case (int)ChangeAiType.CHANGE_AI_APPOINT_ACTION_DUMMY:
                    ChangeAppointActionDummyAI();
                    break;
            }

            Finish();
        }

        private void ChangeAllDummyAI()
        {
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (entity is EntityDummy)
                {
                    var dummy = entity as EntityDummy;
                    dummy.aiID = System.Convert.ToInt32(aiIds[0]);
                    dummy.ChangeAI();
                }
            }
        }

        private void ChangeAppointDummyAI()
        {
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (entity is EntityDummy)
                {
                    var dummy = entity as EntityDummy;
                    for (int j = 0; j < monsterIds.Length; j++)
                    {
                        if (monsterIds[j] == null || monsterIds[j] == "")
                        {
                            continue;
                        }
                        int monsterId = System.Convert.ToInt32(monsterIds[j]);
                        int aiID;
                        if (j + 1 > aiIds.Length)
                        {
                            aiID = System.Convert.ToInt32(aiIds[aiIds.Length - 1]);
                        }
                        else
                        {
                            aiID = System.Convert.ToInt32(aiIds[j]);
                        }
                        if (dummy.monster_id == monsterId)
                        {
                            dummy.aiID = aiID;
                            dummy.ChangeAI();
                        }
                    }
                }
            }
        }

        //改变非指定怪物的AI
        private void ChangeUnAppointDummyAI()
        {
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (entity is EntityDummy)
                {
                    var dummy = entity as EntityDummy;

                    for (int j = 0; j < monsterIds.Length; j++)
                    {
                        if (monsterIds[j] == null || monsterIds[j] == "")
                        {
                            continue;
                        }
                        int monsterId = System.Convert.ToInt32(monsterIds[j]);
                        if (dummy.monster_id != monsterId)
                        {
                            dummy.aiID = System.Convert.ToInt32(aiIds[0]);
                            dummy.ChangeAI();
                        }
                    }
                }
            }
        }

        private void ChangeAppointActionDummyAI()
        {
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (entity is EntityDummy)
                {
                    var dummy = entity as EntityDummy;
                    for (int j = 0; j < monsterIds.Length; j++)
                    {
                        if (monsterIds[j] == null || monsterIds[j] == "")
                        {
                            continue;
                        }
                        int actionId = System.Convert.ToInt32(monsterIds[j]);
                        int aiID;
                        if (j + 1 > aiIds.Length)
                        {
                            aiID = System.Convert.ToInt32(aiIds[aiIds.Length - 1]);
                        }
                        else
                        {
                            aiID = System.Convert.ToInt32(aiIds[j]);
                        }
                        if (dummy.actionId == actionId)
                        {
                            dummy.aiID = aiID;
                            dummy.ChangeAI();
                        }
                    }
                }
            }
        }
    }
}
