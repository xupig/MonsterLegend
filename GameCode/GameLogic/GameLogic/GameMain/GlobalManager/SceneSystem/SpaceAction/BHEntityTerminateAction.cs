﻿using Common.Events;
using GameData;
using MogoEngine.Events;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityTerminateAction : BHEntityBase
    {
        new public EntityTerminateActionData data { get { return _data as EntityTerminateActionData; } }
        protected bool hasTriggered = false;

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered) return;
            hasTriggered = true;
            List<int> targetIdList = data_parse_helper.ParseListInt(data.target_ids_l.Split(','));
            bool result = true;
            for (int i = 0; i < targetIdList.Count; ++i)
            {
                if (!_owner.TerminateActions(targetIdList[i]))
                {
                    result = false;
                }
                else
                {
                    EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_RIGHT_UP_INFOMATION, targetIdList[i]);
                }
            }

            if (result)
            {
                _owner.CreateActions(data.success_actions_l, gameObject.transform.position);
            }
            else
            {
                _owner.CreateActions(data.fail_actions_l, gameObject.transform.position);
            }

            Finish();
        }
    }
}
