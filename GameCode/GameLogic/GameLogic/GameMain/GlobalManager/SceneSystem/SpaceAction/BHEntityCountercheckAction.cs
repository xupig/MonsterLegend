﻿using ACTSystem;
using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityCountercheckAction : BHEntityBase
    {
        new public EntityCountercheckData data { get { return _data as EntityCountercheckData; } }
        GameObject obj;
        string[] boxSize = null;
        protected bool hasTriggered = false;

        protected override void OnStart()
        {
            if (data.boxSize != null && data.boxSize != "")
            {
                boxSize = data.boxSize.Split(',');
            }
            var _box = gameObject.AddComponent<BoxCollider>();

            if (boxSize != null)
            {
                _box.size = new Vector3(int.Parse(boxSize[0]), int.Parse(boxSize[1]), int.Parse(boxSize[2]));
            }
            else
            {
                _box.size = Vector3.one;
            }
            _box.isTrigger = true;

            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered)
            {
                return;
            }
            hasTriggered = true;
            /*if (data.conterCheck_tragetType_i == 0)
            {

            }*/
            string[] rotation = data.rotation_pos_l.Split(',');
            obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            obj.layer = UnityEngine.LayerMask.NameToLayer("Wall");
            obj.gameObject.transform.SetParent(gameObject.transform, false);
            var box = obj.GetComponent<BoxCollider>();
            obj.AddComponent<MeshFilter>();
            obj.AddComponent<MeshCollider>();
            obj.GetComponent<MeshRenderer>().enabled = false;
            obj.transform.position = Vector3.zero;
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localEulerAngles = new Vector3(float.Parse(rotation[0]), float.Parse(rotation[1]), float.Parse(rotation[2]));
            box.isTrigger = false;
            if (boxSize != null)
            {
                box.size = new Vector3(int.Parse(boxSize[0]), int.Parse(boxSize[1]), int.Parse(boxSize[2]));
            }
            else
            {
                box.size = Vector3.one;
            }

            Finish();
        }
    }
}
