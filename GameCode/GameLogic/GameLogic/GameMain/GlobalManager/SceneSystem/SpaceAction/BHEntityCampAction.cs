﻿using Common.Events;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityCampAction : BHEntityBase
    {
        new public EntityCampData data { get { return _data as EntityCampData; } }

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            AddEventListener();
            Active();
        }

        protected override void OnActive()
        {
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).SetReliveType(data.relive_type_i);
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(SpaceActionEvents.SpaceActionEvents_ReliveUseOut, Finish);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(SpaceActionEvents.SpaceActionEvents_ReliveUseOut, Finish);
        }

        protected override void OnFinish()
        {
            TriggerNextActions();
        }

        protected override void OnExit()
        {
            RemoveEventListener();
        }
    }
}
