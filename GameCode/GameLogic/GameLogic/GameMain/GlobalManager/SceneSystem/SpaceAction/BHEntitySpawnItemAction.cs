﻿using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntitySpawnItemAction : BHEntityBase
    {
        new public EntitySpawnItemActionData data { get { return _data as EntitySpawnItemActionData; } }
        protected Dictionary<int, int> randomNums = new Dictionary<int, int>();
        protected bool hasTriggered = false;

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered) return;
            hasTriggered = true;

            string[] itemIds = data.item_ids_l.Split(',');
            string[] monsterNums = data.item_nums_l.Split(',');
            string[] str = data.item_points_l.Split(',');

            int total = str.Length / 3;
            int k = 0;
            for (int index = 0; index < total; index++)
            {
                str[index] = str[k] + "," + str[k + 1] + "," + str[k + 2];
                k += 3;
            }

            randomNums = new Dictionary<int, int>();
            for (int j = 0; j < itemIds.Length; j++)
            {
                if (itemIds[j] == null || itemIds[j] == "")
                {
                    continue;
                }
                for (int i = 0; i < System.Convert.ToInt32(monsterNums[j]); i++)
                {
                    System.Random random = new System.Random();

                    int tmp = random.Next(0, total);
                    if (randomNums.ContainsKey(tmp))
                    {
                        tmp = getNum(0, total, random, tmp);
                    }
                    randomNums.Add(tmp, tmp);
                    string[] point = str[tmp].Split(',');
                    Vector3 pos = new Vector3(float.Parse(point[0]), float.Parse(point[1]), float.Parse(point[2]));

                    ClientEntityManager.Instance.CreateDropItemByClient(System.Convert.ToInt32(itemIds[j]), 1, 0, pos, 1);
                }
            }
            Finish();
        }

        //检测随机数是否重复，避免坐标
        private int getNum(int minVal, int maxVal, System.Random ra, int tmp)
        {
            tmp = ra.Next(minVal, maxVal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, maxVal, ra, tmp);
            }

            return tmp;
        }

        protected override void OnExit()
        {
            randomNums.Clear();
            randomNums = null;
            base.OnExit();
        }
    }
}
