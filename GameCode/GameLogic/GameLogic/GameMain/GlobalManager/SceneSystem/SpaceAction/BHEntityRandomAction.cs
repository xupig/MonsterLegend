﻿using GameLoader.Utils;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityRandomAction : BHEntityBase
    {
        new public EntityRandomActionData data { get { return _data as EntityRandomActionData; } }
        protected Dictionary<int, int> randomNums = new Dictionary<int, int>();
        protected Dictionary<int, string> randomActions = new Dictionary<int, string>();
        protected Dictionary<int, int> odds = new Dictionary<int, int>();

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            odds = new Dictionary<int, int>();
            string[] str = data.random_weight_l.Split(',');
            int randomTotal = 0;
            for (int i = 0; i < str.Length; i++)
            {
                int currOdds = System.Convert.ToInt32(str[i]) + randomTotal;
                odds.Add(i + 1, currOdds);
                randomTotal += System.Convert.ToInt32(str[i]);
            }

            randomNums = new Dictionary<int, int>();
            randomActions = new Dictionary<int, string>();
            string[] actions = data.random_actions_l.Split(',');
            for (int k = 0; k < actions.Length; k++)
            {
                randomActions.Add(k + 1, actions[k]);
            }
            int tmp = 0;
            int key = 0;
            System.Random random = new System.Random();
            //0- 随机数可重复 1-不可重复
            for (int j = 0; j < data.random_num_i; j++)
            {
                switch (data.random_type_i)
                {
                    case (int)RandomType.RANDOM_IS_CAN_REPEAT:
                        tmp = random.Next(0, randomTotal);
                        key = getKey(tmp, randomTotal);
                        if (randomActions.ContainsKey(key))
                        {
                            _owner.CreateActions(randomActions[key], gameObject.transform.position);
                        }
                        break;
                    case (int)RandomType.RANDOM_IS_NOT_REPEAT:
                        tmp = random.Next(0, randomTotal);

                        if (randomNums.ContainsKey(getKey(tmp, randomTotal)))
                        {
                            tmp = getNum(0, randomTotal, random, tmp, key);
                        }
                        key = getKey(tmp, randomTotal);
                        randomNums.Add(key, tmp);
                        if (randomActions.ContainsKey(key))
                        {
                            _owner.CreateActions(randomActions[key], gameObject.transform.position);
                        }
                        break;
                    default:
                        break;
                }
            }
            Finish();
        }

        int getKey(int tmp, int randomTotal)
        {
            int key = 0;
            foreach (KeyValuePair<int, int> kv in odds)
            {
                if (tmp <= kv.Value)
                {
                    key = kv.Key;
                    break;
                }
            }

            if (key == 0)
            {
                LoggerHelper.Error("副本随机行为查找不到行为Key值，请检查当前行为和权值配置对应关系");
            }
            return key;
        }

        //检测随机数是否重复，避免坐标
        public int getNum(int minVal, int maxVal, System.Random ra, int tmp, int key)
        {
            tmp = ra.Next(minVal, maxVal);
            key = getKey(tmp, maxVal);
            if (randomNums.ContainsKey(key))
            {
                tmp = getNum(0, maxVal, ra, tmp, key);
            }

            return tmp;
        }

        protected override void OnExit()
        {
            randomNums.Clear();
            randomNums = null;
            randomActions.Clear();
            randomActions = null;
            odds.Clear();
            odds = null;
        }
    }
}
