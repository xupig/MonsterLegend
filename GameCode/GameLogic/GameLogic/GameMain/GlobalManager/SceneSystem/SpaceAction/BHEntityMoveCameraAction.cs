﻿using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityMoveCameraAction : BHCameraActionBase
    {
        new public EntityMoveCameraData data { get { return _data as EntityMoveCameraData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            CameraManager.GetInstance().ChangeToRotationPositionMotion(
                ActionEntitiesUtils.StringToVector3(data.camera_rotation_l), data.camera_rotation_duration_f,
                data.camera_rotation_accelerate_rate_f,
                ActionEntitiesUtils.StringToVector3(data.camera_position_l), data.camera_position_duration_f);
            StartNextActionsTimer();
        }

        override protected uint GetChangeDuration()
        {
            return (uint)(Mathf.Max(data.camera_position_duration_f, data.camera_rotation_duration_f) * 1000);
        }
    }
}
