﻿using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntitySendBuffAction : BHEntityBase
    {
        new public EntitySendBuffTriggerActionData data { get { return _data as EntitySendBuffTriggerActionData; } }
        private Dictionary<int, string> buffInfo = new Dictionary<int, string>();
        private List<int> keyList = new List<int>();
        string[] targetIds;
        protected override void OnStart()
        {
            string[] str = data.buff_ids_n.Split(';');
            for (int i = 0; i < str.Length; i++)
            {
                string[] dicInfo = str[i].Split(':');
                if (buffInfo.ContainsKey(System.Convert.ToInt32(dicInfo[0])))
                {
                    buffInfo.Remove(System.Convert.ToInt32(dicInfo[0]));
                }
                buffInfo.Add(System.Convert.ToInt32(dicInfo[0]), dicInfo[1]);
                keyList.Add(System.Convert.ToInt32(dicInfo[0]));
            }
            if (data.target_ids_l != null && data.target_ids_l != "")
            {
                targetIds = data.target_ids_l.Split(',');
            }
            Active();
        }

        protected override void OnActive()
        {
            var entities = MogoWorld.Entities;
            Dictionary<uint, Entity> currEntites = new Dictionary<uint, Entity>();
            foreach (KeyValuePair<uint, Entity> kv in entities)
            {
                currEntites.Add(kv.Key, kv.Value);
            }
            foreach (KeyValuePair<uint, Entity> kv in currEntites)
            {
                switch (data.target_type_i)
                {
                    case (int)SendBuffTypeType.SEND_BUFF_APPONIT_CAMP_ID:
                        if (kv.Value is EntityAvatar)
                        {
                            var entityAcatar = kv.Value as EntityAvatar;
                            SendEntityAvatar(entityAcatar);
                        }
                        break;
                    case (int)SendBuffTypeType.SEND_BUFF_APPONIT_MONSTER_ID:
                        if (kv.Value is EntityDummy)
                        {
                            var entityDummy = kv.Value as EntityDummy;
                            SendAppointDummy(entityDummy);
                        }
                        break;
                    case (int)SendBuffTypeType.SEND_BUFF_UNAPPONIT_MONSTER_ID:
                        if (kv.Value is EntityDummy)
                        {
                            var entityDummy = kv.Value as EntityDummy;
                            SendAppointBeyongDummy(entityDummy);
                        }
                        break;
                    case (int)SendBuffTypeType.SEND_BUFF_PLAYER:
                        if (kv.Value is PlayerAvatar)
                        {
                            var entityAcatar = kv.Value as EntityAvatar;
                            SendEntityPlayerAvatar(entityAcatar);
                        }
                        break;
                    case (int)SendBuffTypeType.SEND_BUFF_APPONIT_REGION:
                        if (data.target_ids_l == "0" && kv.Value is EntityAvatar)
                        {
                            SendRegionEntity(kv.Value);
                        }
                        else if (data.target_ids_l == "1" && kv.Value is EntityDummy)
                        {
                            SendRegionEntity(kv.Value);
                        }
                        break;
                    default:
                        break;
                }
            }
            Finish();
        }

        void SendEntityAvatar(EntityAvatar entityAcatar)
        {
            var entityPet = MogoEngine.MogoWorld.GetEntity(PlayerPetManager.GetInstance().curPetEntityID) as EntityCreature;
            for (int j = 0; j < targetIds.Length; j++)
            {
                if (entityAcatar.map_camp_id == System.Convert.ToByte(targetIds[j]))
                {
                    for (int i = 0; i < keyList.Count; i++)
                    {
                        string buffValue = GetBuffInfo(keyList[i]);
                        if (buffValue == "") break;
                        int buffId = keyList[i];
                        float odds = float.Parse(buffValue.Split(',')[0]);
                        float time = float.Parse(buffValue.Split(',')[1]);
                        System.Random random = new System.Random();
                        int tmp = random.Next(0, 10);
                        float currOdds = tmp / 10;
                        if (currOdds <= odds)
                        {
                            if (time < -1.0f)
                            {
                                entityAcatar.bufferManager.RemoveBuffer(buffId);
                                if (entityPet != null)
                                {
                                    entityPet.bufferManager.RemoveBuffer(buffId);
                                }
                            }
                            else
                            {
                                entityAcatar.bufferManager.AddBuffer(buffId, time, true);
                                if (entityPet != null)
                                {
                                    entityPet.bufferManager.AddBuffer(buffId, time, true);
                                }
                            }
                        }
                    }
                }
            }
        }

        void SendEntityPlayerAvatar(EntityAvatar entityAcatar)
        {
            var entityPet = MogoEngine.MogoWorld.GetEntity(PlayerPetManager.GetInstance().curPetEntityID) as EntityCreature;

            for (int i = 0; i < keyList.Count; i++)
            {
                string buffValue = GetBuffInfo(keyList[i]);
                if (buffValue == "") break;
                int buffId = keyList[i];
                float odds = float.Parse(buffValue.Split(',')[0]);
                float time = float.Parse(buffValue.Split(',')[1]);
                System.Random random = new System.Random();
                int tmp = random.Next(0, 10);
                float currOdds = tmp / 10;
                if (currOdds <= odds)
                {
                    if (time < -1.0f)
                    {
                        entityAcatar.bufferManager.RemoveBuffer(buffId);
                        if (entityPet != null)
                        {
                            entityPet.bufferManager.RemoveBuffer(buffId);
                        }
                    }
                    else
                    {
                        entityAcatar.bufferManager.AddBuffer(buffId, time, true);
                        if (entityPet != null)
                        {
                            entityPet.bufferManager.AddBuffer(buffId, time, true);
                        }
                    }
                }
            }
        }

        void SendAppointDummy(EntityDummy entityDummy)
        {
            for (int j = 0; j < targetIds.Length; j++)
            {
                if (entityDummy.monster_id == System.Convert.ToUInt32(targetIds[j]))
                {
                    for (int i = 0; i < keyList.Count; i++)
                    {
                        string buffValue = GetBuffInfo(keyList[i]);
                        if (buffValue == "") break;
                        int buffId = keyList[i];
                        float odds = float.Parse(buffValue.Split(',')[0]);
                        float time = float.Parse(buffValue.Split(',')[1]);
                        System.Random random = new System.Random();
                        int tmp = random.Next(0, 10);
                        float currOdds = tmp / 10;
                        if (currOdds <= odds)
                        {
                            if (time < -1.0f)
                            {
                                entityDummy.bufferManager.RemoveBuffer(buffId);
                            }
                            else
                            {
                                entityDummy.bufferManager.AddBuffer(buffId, time, true);
                            }
                        }
                    }
                }
            }
        }

        void SendAppointBeyongDummy(EntityDummy entityDummy)
        {
            for (int j = 0; j < targetIds.Length; j++)
            {
                if (entityDummy.monster_id != System.Convert.ToUInt32(targetIds[j]))
                {
                    for (int i = 0; i < keyList.Count; i++)
                    {
                        string buffValue = GetBuffInfo(keyList[i]);
                        if (buffValue == "") break;
                        int buffId = keyList[i];
                        float odds = float.Parse(buffValue.Split(',')[0]);
                        float time = float.Parse(buffValue.Split(',')[1]);
                        System.Random random = new System.Random();
                        int tmp = random.Next(0, 10);
                        float currOdds = tmp / 10;
                        if (currOdds <= odds)
                        {
                            if (time < -1.0f)
                            {
                                entityDummy.bufferManager.RemoveBuffer(buffId);
                            }
                            else
                            {
                                entityDummy.bufferManager.AddBuffer(buffId, time, true);
                            }
                        }
                    }
                }
            }
        }

        void SendRegionEntity(Entity entity)
        {
            var entityPet = MogoEngine.MogoWorld.GetEntity(PlayerPetManager.GetInstance().curPetEntityID) as EntityCreature;
            if (gameObject.GetComponent<BoxCollider>().bounds.Contains(entity.position))
            {
                for (int i = 0; i < keyList.Count; i++)
                {
                    string buffValue = GetBuffInfo(keyList[i]);
                    if (buffValue == "") break;
                    int buffId = keyList[i];
                    float odds = float.Parse(buffValue.Split(',')[0]);
                    float time = float.Parse(buffValue.Split(',')[1]);
                    System.Random random = new System.Random();
                    int tmp = random.Next(0, 10);
                    float currOdds = tmp / 10;
                    if (currOdds <= odds)
                    {
                        if (entity is EntityAvatar)
                        {
                            var entityAvatar = entity as EntityAvatar;
                            if (time < -1.0f)
                            {
                                entityAvatar.bufferManager.RemoveBuffer(buffId);
                                if (entityPet != null)
                                {
                                    entityPet.bufferManager.RemoveBuffer(buffId);
                                }
                            }
                            else
                            {
                                entityAvatar.bufferManager.AddBuffer(buffId, time, true);
                                if (entityPet != null)
                                {
                                    entityPet.bufferManager.AddBuffer(buffId, time, true);
                                }
                            }
                        }
                        if (entity is EntityDummy)
                        {
                            var entityDummy = entity as EntityDummy;
                            if (time < -1.0f)
                            {
                                entityDummy.bufferManager.RemoveBuffer(buffId);
                            }
                            else
                            {
                                entityDummy.bufferManager.AddBuffer(buffId, time, true);
                            }
                        }
                    }
                }
            }
        }

        string GetBuffInfo(int key)
        {
            if (buffInfo.ContainsKey(key))
            {
                return buffInfo[key];
            }
            return "";
        }
    }
}
