﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEnterInstanceAction : BHEntityBase
    {
        new public EnterInstanceActionData data { get { return _data as EnterInstanceActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Finish();
        }
    }
}
