﻿using Common.ClientConfig;
using Common.Events;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntitySpawnMonsterAction : BHEntityBase
    {
        new public EntitySpawnMonsterActionData data { get { return _data as EntitySpawnMonsterActionData; } }
        protected Dictionary<int, int> randomNums = new Dictionary<int, int>();
        protected Dictionary<int, uint> checkEntityIds = new Dictionary<int, uint>();
        protected Dictionary<int, uint> collectInfo = new Dictionary<int, uint>();
        protected string[] forwards = null;
        protected string[] point = null;
        protected bool hasTriggered = false;
        private bool _needToTriggerNextActions = false;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            AddEventListener();
            Active();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, string>(SpaceActionEvents.SpaceActionEvents_Collect_Action, CheckIsCollectAction);
            EventDispatcher.AddEventListener<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, CheckMonsterNums);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, string>(SpaceActionEvents.SpaceActionEvents_Collect_Action, CheckIsCollectAction);
            EventDispatcher.RemoveEventListener<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, CheckMonsterNums);
        }

        void CheckIsCollectAction(int collectType, string actionId)
        {
            if (checkEntityIds == null || checkEntityIds.Count == 0)
            {
                return;
            }
            string[] str = actionId.Split(',');
            for (int i = 0; i < str.Length; i++)
            {
                if (data.id_i == System.Convert.ToInt32(str[i]))
                {
                    foreach (KeyValuePair<int, uint> pair in collectInfo)
                    {
                        CheckMonsterNums(pair.Value, collectType);
                    }
                    break;
                }
            }
        }

        public void CheckMonsterNums(uint entityId, int type)
        {
            if (checkEntityIds == null || checkEntityIds.Count == 0)
            {
                return;
            }
            if (checkEntityIds.ContainsValue(entityId))
            {
                int key = 0;
                foreach (KeyValuePair<int, uint> pair in checkEntityIds)
                {
                    if (pair.Value == entityId)
                    {
                        key = pair.Key;
                        break;
                    }
                }
                var entity = MogoEngine.MogoWorld.GetEntity(entityId);
                if (entity == null) return;

                MogoEngine.MogoWorld.DestroyEntity(entityId);
                if (type == InstanceDefine.DESTROY_SPAWN_DIE_MOSTER_TYPE)
                {
                    if (_scene is CombatScene)
                    {
                        (_scene as CombatScene).SetKillMonsterCount(1);
                    }
                }

                checkEntityIds.Remove(key);
                if (checkEntityIds.Count == 0)
                {
                    //TODO：完成
                    if (type == InstanceDefine.DESTROY_SPAWN_DIE_MOSTER_TYPE)   //正常死亡，触发next_action后结束
                    {
                        //uint activationEntityId = 0;
                        if (data.trigger_type_i == 0) //0 - 取怪物坐标 1-最后攻击者坐标
                        {
                            //activationEntityId = entity.id;
                            _nextActionPosition = entity.position;
                        }
                        else
                        {
                            //activationEntityId = MogoEngine.MogoWorld.Player.id;
                            _nextActionPosition = MogoEngine.MogoWorld.Player.position;
                        }
                        _nextActionEntityId = MogoEngine.MogoWorld.Player.id;
                        _needToTriggerNextActions = true;
                    }
                    else
                    {
                        _needToTriggerNextActions = false;
                    }
                    Finish();
                }
            }
        }

        protected override void OnFinish()
        {
            if (_needToTriggerNextActions)
            {
                TriggerNextActions();
                Exit();
            }
            else
            {
                Exit();
            }
        }

        protected override void OnExit()
        {
            RemoveEventListener();
            base.OnExit();
        }

        protected override void OnActive()
        {
            if (!_scene.IsClientScene())
            {
                Finish();
                return;
            }
            if (hasTriggered) return;
            hasTriggered = true;
            //TempTest();
            //return;
            forwards = null;
            string[] monsterIds = data.monster_ids_l.Split(',');
            string[] monsterNums = data.monster_nums_l.Split(',');
            string[] str = data.monster_ponits_l.Split(',');
            string[] ais = null;
            if (!string.IsNullOrEmpty(data.monster_ais_l))
            {
                ais = data.monster_ais_l.Split(',');
            }
            if (!string.IsNullOrEmpty(data.monster_forward_l))
            {
                forwards = data.monster_forward_l.Split(',');
            }

            int currMonsterNums = 0;
            for (int currMonsterNumsIndex = 0; currMonsterNumsIndex < monsterNums.Length; currMonsterNumsIndex++)
            {
                currMonsterNums += System.Convert.ToInt32(monsterNums[currMonsterNumsIndex]);
            }
                
            int total = str.Length / 3;
            int randomTotal = total;
            if (currMonsterNums > total)
            {
                LoggerHelper.Error("刷怪点数量不足，请添加" + (currMonsterNums - total) + "组坐标");
            }
            int k = 0;
            //保持格式只有“,”号，在此重新整理数据，3个为一组重新存储
            for (int index = 0; index < total; index++)
            {
                str[index] = str[k] + "," + str[k + 1] + "," + str[k + 2];
                k += 3;
            }

            randomNums = new Dictionary<int, int>();
            checkEntityIds = new Dictionary<int, uint>();
            collectInfo = new Dictionary<int, uint>();
            int key = 0;
            point = null;
            for (int j = 0; j < monsterIds.Length; j++)
            {
                if (string.IsNullOrEmpty(monsterIds[j]))
                {
                    continue;
                }

                for (int i = 0; i < System.Convert.ToInt32(monsterNums[j]); i++)
                {
                    key += 1;
                    MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_DUMMY, (entity) =>
                    {
                        var dummy = entity as EntityDummy;
                        dummy.monster_id = uint.Parse(monsterIds[j]);

                        //LoggerHelper.Error("[BHEntitySpawnMonsterAction:EnterAction]=>dummy.monster_id: " + dummy.monster_id + ",");
                        dummy.camp_pve_type = (byte)data.camp_pve_type_i;
                        dummy.isNoDrop = data.is_nodrop_i;
                        dummy.actionId = data.id_i;
                        dummy.is_not_play_spawn_anim = (byte)data.is_not_play_spawn_anim_i;
                        switch(data.monster_point_type_i)
                        {
                            case (int)MonsterPointType.POSITION:       //定点刷怪
                                SetPoint(str, key, entity);
                                break;
                            case (int)MonsterPointType.RANDOM_POSITION:
                                SetRandomPoint(str, key, entity, randomTotal);              //随机刷怪
                                break;
                            default:
                                LoggerHelper.Error("刷怪类型错误，请假查配置");
                                return;
                        }
                        checkEntityIds.Add(key, entity.id);
                        collectInfo.Add(key, entity.id);
                        if (data.origin_point_type_i == 1)  //使用配置中的偏移
                        {
                            float x = gameObject.transform.position.x - (data.pos_x_i / 100);
                            float y = gameObject.transform.position.y - (data.pos_y_i / 100);
                            float z = gameObject.transform.position.z - (data.pos_z_i / 100);
                            entity.SetPosition(new Vector3((float.Parse(point[0]) + x), (float.Parse(point[1]) + y), (float.Parse(point[2]) + z)));
                        }
                        else
                        {
                            entity.SetPosition(new Vector3(float.Parse(point[0]), float.Parse(point[1]), float.Parse(point[2])));
                        }

                        if (ais != null)
                        {
                            SetDummyAi(key, ais, dummy);
                        }

                        if (_scene is CombatScene)
                        {
                            (_scene as CombatScene).OnSpawnMonster(dummy.id, dummy.monster_id);
                        }
                    });
                }
            }
        }

        private void SetDummyAi(int key, string[] ais, EntityDummy dummy)
        {
            int AIID = 0;
            int index = key - 1;
            if (index >= ais.Length)
            {
                AIID = System.Convert.ToInt32(ais[ais.Length - 1]);
            }
            else
            {
                AIID = System.Convert.ToInt32(ais[index]);
            }
            dummy.aiID = AIID;
            dummy.ChangeAI();
        }

        private void SetPoint(string[] str, int key, Entity entity)
        {
            point = str[key - 1].Split(',');
            if (forwards != null)
            {
                if (key <= forwards.Length)
                {
                    float rotatioY = float.Parse(forwards[key - 1]);
                    Quaternion rotation = Quaternion.Euler(0, rotatioY, 0);
                    entity.SetRotation(rotation);
                }
                else
                {
                    entity.SetRotation(Quaternion.Euler(0, 0, 0));
                }
            }
            else
            {
                entity.SetRotation(Quaternion.Euler(0, 0, 0));
            }
        }

        private void SetRandomPoint(string[] str, int key, Entity entity, int randomTotal)
        {
            System.Random random = new System.Random();
            int tmp = random.Next(0, randomTotal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, randomTotal, random, tmp);
            }
            randomNums.Add(tmp, tmp);
            point = str[tmp].Split(',');
            if (forwards != null)
            {
                if (tmp <= forwards.Length)
                {
                    float rotatioY = float.Parse(forwards[tmp]);
                    Quaternion rotation = Quaternion.Euler(0, rotatioY, 0);
                    entity.SetRotation(rotation);
                }
                else
                {
                    entity.SetRotation(Quaternion.Euler(0, 0, 0));
                }
            }
            else
            {
                entity.SetRotation(Quaternion.Euler(0, 0, 0));
            }
        }

        //检测随机数是否重复，避免坐标
        public int getNum(int minVal, int maxVal, System.Random ra, int tmp)
        {
            tmp = ra.Next(minVal, maxVal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, maxVal, ra, tmp);
            }

            return tmp;
        }
    }
}
