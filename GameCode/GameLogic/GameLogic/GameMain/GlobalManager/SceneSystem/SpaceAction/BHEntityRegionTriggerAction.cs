﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityRegionTriggerAction : BHEntityBase
    {
        new public EntityRegionTriggerActionData data { get { return _data as EntityRegionTriggerActionData; } }
        protected const int REGION_TRIGER_TYPE_PLAYER = 0;
        protected const int REGION_TRIGER_TYPE_MONSTER = 1;
        protected const int REGION_TRIGER_TYPE_MONSTRID = 2;
        protected const int REGION_TRIGER_TYPE_STAY = 0;
        protected const int REGION_TRIGER_TYPE_EXIT = 1;

        protected EntityCreature entity;
        protected bool isHasPostion = false;
        private bool bo = false;
        private Collider _collider;
        private int _currentTriggeredCount = 0;
        private uint _triggerTimesTimerId = 0;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            CreateBoxCollider();
        }

        private void CreateBoxCollider()
        {
            string[] boxSize = null;
            if (data.boxSize != null && data.boxSize != "")
            {
                boxSize = data.boxSize.Split(',');
            }
            string[] rotation = data.rotation_pos_l.Split(',');
            gameObject.transform.localEulerAngles = new Vector3(float.Parse(rotation[0]), float.Parse(rotation[1]), float.Parse(rotation[2]));
            var box = gameObject.AddComponent<BoxCollider>();
            if (boxSize != null)
            {
                box.size = new Vector3(float.Parse(boxSize[0]), float.Parse(boxSize[1]), float.Parse(boxSize[2]));
            }
            else
            {
                box.size = Vector3.one;
            }
            box.isTrigger = true;
        }

        void OnTriggerEnter(Collider boxCollider)
        {
            if (data.trig_type_i == REGION_TRIGER_TYPE_EXIT)
            {
                DelTriggerTimes();
                return;
            }
            _collider = boxCollider;
            TriggerCurrentActions();
            if (!bo)
            {
                bo = CheckTriggerEntityType(_collider);
            }
            if (!bo) return;
            bo = false;
            Active();
        }

        void OnTriggerExit(Collider boxCollider)
        {
            DeleteCurrActions();
			if (data.trig_type_i == REGION_TRIGER_TYPE_STAY)
            {
                DelTriggerTimes();
                EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_PROGRESSBAR_INFOMATION);
                return;
            }
            _collider = boxCollider;
            TriggerCurrentActions();
            if (!bo)
            {
                bo = CheckTriggerEntityType(_collider);
            }
            if (!bo) return;
            bo = false;
            Active();
        }

        protected override void OnActive()
        {
            if (IsReachTriggeredCount())
            {
                return;
            }
            ShowNotice();
            if (data.trig_type_arg_i == 0)
            {
                TriggerOnce();
            }
            else
            {
                TriggerTimes();
            }
        }

        private void TriggerOnce()
        {
            var entityInfo = MogoEngine.MogoWorld.GetEntity(entity.id);
            
            if (entityInfo != null)
            {
                _nextActionPosition = entityInfo.position;
            }
            //Debug.LogError("TriggerOnce:" + entityInfo);
            TriggerNextActions();
            DeleteCurrActions();
            _currentTriggeredCount++;
            if (IsReachTriggeredCount())
            {
                Finish();
            }
        }

        private void TriggerTimes()
        {
            DelTriggerTimes();
            _triggerTimesTimerId = TimerHeap.AddTimer((uint)data.trig_type_arg_i, data.trig_type_arg_i, OnTriggerTimesClock);
        }

        private void OnTriggerTimesClock()
        {
            TriggerOnce();
        }

        private void DelTriggerTimes()
        {
            if (_triggerTimesTimerId > 0)
            {
                TimerHeap.DelTimer(_triggerTimesTimerId);
                _triggerTimesTimerId = 0;
            }
        }

        private bool IsReachTriggeredCount()
        {
            return data.close_flag_i > 0 && _currentTriggeredCount >= data.close_flag_i;
        }

        private void TriggerCurrentActions()
        {
            if (data.trig_type_arg_i != 0 && !string.IsNullOrEmpty(data.trig_curr_actions_l))
            {
                _owner.CreateActions(data.trig_curr_actions_l, gameObject.transform.position);
            }
        }

        private void DeleteCurrActions()
        {
            if (!string.IsNullOrEmpty(data.trig_curr_actions_l))
            {
                string[] actions = data.trig_curr_actions_l.Split(',');
                for (int i = 0; i < actions.Length; i++)
                {
                    _owner.TerminateActions(System.Convert.ToInt32(actions[i]));
                }
            }
        }

        private bool CheckTriggerEntityType(Collider boxCollider)
        {
            var currEntity = GameMain.GlobalManager.ACTSystemAdapter.GetOwner(boxCollider.gameObject.GetComponent<ACTSystem.ACTActor>());
            switch (data.trig_qualification_i)
            {
                case REGION_TRIGER_TYPE_PLAYER:
                    if (IsOrBelongsToPlayer(currEntity))
                    {
                        entity = currEntity;
                        break;
                    }
                    else
                    {
                        return false;
                    }
                case REGION_TRIGER_TYPE_MONSTER:
                    if (currEntity is EntityMonsterBase)
                    {
                        entity = currEntity;
                        break;
                    }
                    else
                    {
                        return false;
                    }
                case REGION_TRIGER_TYPE_MONSTRID:
                    if (currEntity is EntityMonsterBase)
                    {
                        bool isMonsterId = false;
                        EntityMonsterBase entityMon = entity as EntityMonsterBase;
                        string[] str = data.trig_qualificationArg_l.Split(',');
                        for(int j = 0; j< str.Length; j++)
                        {
                            if (entityMon.monster_id == System.Convert.ToUInt32(str[j]))
                            {
                                isMonsterId = true;
                                break;
                            }
                        }
                        if (isMonsterId)
                        {
                            entity = currEntity;
                            break;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }
            if (entity == null)
            {
                LoggerHelper.Error("找不到触发者实体");
            }
            if (!_scene.IsClientScene() && data.action_type_i == (int)EntityActionType.CLIENT)
            {
                if (!isHasPostion)
                {
                    _nextActionPosition = MogoEngine.MogoWorld.GetEntity(entity.id).position;
                    isHasPostion = true;
                }
                _nextActionEntityId = entity.id;
                return true;
            }
            else if (_scene.IsClientScene())
            {
                if (!isHasPostion)
                {
                    _nextActionPosition = MogoEngine.MogoWorld.GetEntity(entity.id).position;
                    isHasPostion = true;
                }
                _nextActionEntityId = entity.id;
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsOrBelongsToPlayer(EntityCreature creature)
        {
            return creature is PlayerAvatar || (creature is EntityVehicle && (creature as EntityVehicle).BelongsToPlayer());
        }

        void ShowNotice()
        {
            if (data.is_can_trigger_notice_i == (int)ShowNoticeType.SHOW_UNNOTICE)
            {
                return;
            }
            SystemNoticeProgressData systemNoticeData = new SystemNoticeProgressData();
            systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE;
            systemNoticeData.content = data.notice_content_id_i;
            systemNoticeData.time = (uint)(data.trig_type_arg_i - 1);
            systemNoticeData.priority = data.show_priority_i;
            systemNoticeData.progressbarPlayType = data.is_can_trigger_notice_i;
            SpaceNoticeActionManager.Instance.AddSystemNotice(systemNoticeData);
        }

        protected override void OnFinish()
        {
            Exit();
        }

        protected override void OnExit()
        {
            DelTriggerTimes();
            entity = null;
        }
    }
}
