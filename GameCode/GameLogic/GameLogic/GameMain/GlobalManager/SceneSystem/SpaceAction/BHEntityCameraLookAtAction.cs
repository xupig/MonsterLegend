﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityCameraLookAtAction : BHCameraActionBase
    {
        new public EntityCameraLookAtData data { get { return _data as EntityCameraLookAtData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            var targetTransform = ActionEntitiesUtils.GetCameraTargetTransform(data.target_i, data.target_slot_name_s);
            CameraManager.GetInstance().ChangeToTargetPositionMotion(targetTransform,
                data.camera_rotation_duration_f, data.camera_rotation_min_speed_f,
                ActionEntitiesUtils.StringToVector3(data.camera_position_l), data.camera_position_duration_f);
            StartNextActionsTimer();
        }

        override protected uint GetChangeDuration()
        {
            return (uint)(data.camera_position_duration_f * 1000);
        }
    }
}
