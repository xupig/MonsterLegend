﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using MogoEngine.Events;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityChoiceAction : BHEntityBase
    {
        new public EntityChoiceActionData data { get { return _data as EntityChoiceActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            List<PortalItemData> choiceDataList = new List<PortalItemData>();
            string[] choiceActionID = data.action_ids_l.Split(',');
            string[] contentIds = data.action_content_l.Split(',');
            for (int i = 0; i < choiceActionID.Length; i++)
            {
                PortalItemData portalItemData = new PortalItemData();
                portalItemData.choiceId = data.id_i;
                portalItemData.choiceActionId = choiceActionID[i];
                portalItemData.contentId = System.Convert.ToInt32(contentIds[i]);
                portalItemData.choiceType = InstanceDefine.CHOICE_CLIENT_ACTION;
                choiceDataList.Add(portalItemData);
            }

            EventDispatcher.TriggerEvent<List<PortalItemData>>(BattleUIEvents.SHOW_PORTAL_PANEL, choiceDataList);
            Finish();
        }
    }
}
