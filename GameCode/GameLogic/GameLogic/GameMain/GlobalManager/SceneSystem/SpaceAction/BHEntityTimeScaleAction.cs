﻿using ACTSystem;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityTimeScaleAction : BHEntityBase
    {
        new public EntityTimeScaleActionData data { get { return _data as EntityTimeScaleActionData; } }
        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            ACTTimeScaleManager.GetInstance().KeepTimeScale(data.time_scale_f, data.duration_i * 0.001f);
            Finish();
        }
    }
}
