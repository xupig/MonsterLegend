﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityShakeCameraAction : BHCameraActionBase
    {
        new public EntityShakeCameraData data { get { return _data as EntityShakeCameraData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            string[] str = data.camera_concuss_id_l.Split(',');
            int id = System.Convert.ToInt32(str[0]);
            float length = float.Parse(str[1]);
            CameraManager.GetInstance().ShakeCamera(id, length);
            StartNextActionsTimer();
        }

        override protected uint GetChangeDuration()
        {
            string[] str = data.camera_concuss_id_l.Split(',');
            return (uint)(float.Parse(str[1]) * 1000);
        }
    }
}
