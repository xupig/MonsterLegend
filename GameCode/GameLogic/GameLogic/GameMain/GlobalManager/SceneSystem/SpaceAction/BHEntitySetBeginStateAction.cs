﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntitySetBeginStateAction : BHEntityBase
    {
        new public EntitySetStateData data { get { return _data as EntitySetStateData; } }

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Finish();
        }

        protected override void OnFinish()
        {
            var nextActionList = ActionEntitiesUtils.ParseListInt(data.next_actions_l);
            if (!_scene.IsClientScene() && (EntityActionType)data.action_type_i == EntityActionType.CLIENT)
            {
                if (nextActionList != null)
                {
                    for (int i = 0; i < nextActionList.Count; i++)
                    {
                        EntityData entityData = _owner.GetEntityData(nextActionList[i]);
                        if ((EntityActionType)entityData.action_type_i == EntityActionType.CLIENT)
                        {
                            _owner.CreateActions(nextActionList[i].ToString(), gameObject.transform.position);
                        }
                    }
                }
                Exit();
            }
            else if (_scene.IsClientScene())
            {
                TriggerNextActions();
                Exit();
            }
        }
    }
}
