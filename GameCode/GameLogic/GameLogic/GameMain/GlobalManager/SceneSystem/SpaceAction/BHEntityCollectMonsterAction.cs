﻿using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using GameMain.Entities;
using Mogo.AI;
using MogoEngine.Events;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityCollectMonsterAction : BHEntityBase
    {
        new public EntityCollectMonsterActionData data { get { return _data as EntityCollectMonsterActionData; } }
        protected bool hasTriggered = false;
        protected List<uint> removeList = new List<uint>();
        protected Dictionary<uint, Entity> entities = new Dictionary<uint, Entity>();
        protected Dictionary<uint, Entity> entitiesDummy = new Dictionary<uint, Entity>();
        protected string[] monsterIds = null;

        protected override void OnStart()
        {
            string[] boxSize = null;
            if (data.boxSize != null && data.boxSize != "")
            {
                boxSize = data.boxSize.Split(',');
            }
            string[] rotation = data.rotation_pos_l.Split(',');
            gameObject.transform.localEulerAngles = new Vector3(float.Parse(rotation[0]), float.Parse(rotation[1]), float.Parse(rotation[2]));
            var box = gameObject.AddComponent<BoxCollider>();
            if (boxSize != null)
            {
                box.size = new Vector3(int.Parse(boxSize[0]), int.Parse(boxSize[1]), int.Parse(boxSize[2]));
            }
            else
            {
                box.size = Vector3.one;
            }
            box.isTrigger = true;

            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered) return;
            hasTriggered = true;
            monsterIds = null;
            entitiesDummy = new Dictionary<uint, Entity>();
            entities = MogoEngine.MogoWorld.Entities;
            foreach (var kv in entities)
            {
                var entity = kv.Value;
                if (entity is EntityDummy)
                {
                    entitiesDummy.Add(kv.Key, kv.Value);
                }
            }
            if (!string.IsNullOrEmpty(data.target_ids_l))
            {
                monsterIds = data.target_ids_l.Split(',');
            }

            removeList = new List<uint>();
            switch (data.target_type_i)
            {
                case (int)CollectMonsterType.COLLECT_TRIGER_DUMMY:
                    var entity = MogoEngine.MogoWorld.GetEntity(data.entity_id_i);
                    if (entity == null)
                    {
                        Exit();
                        return;
                    }
                    //Debug.LogError("怪物回收：" + data.entity_id_i);
                    EntityCreature monEnitty = entity as EntityCreature;
                    //Debug.LogError("entityId:" + data.entity_id_i);
                    if (gameObject.GetComponent<BoxCollider>().bounds.Contains(monEnitty.position))
                    {
                        removeList.Add(monEnitty.id);
                    }
                    break;
                case (int)CollectMonsterType.COLLECT_APPOINT_DUMMY:
                    CollectAppointDummy();
                    break;
                case (int)CollectMonsterType.COLLECT_UNAPPOINT_DUMMY:
                    CollectUnAppointDummy();
                    break;
                case (int)CollectMonsterType.COLLECT_APPOINT_ACTION_DUMMY:
                    CollectActionMonster();
                    return;
            }
            
            int index = removeList.Count;
            for (int i = 0; i < removeList.Count; i++)
            {
                index--;
                var dummy = MogoEngine.MogoWorld.GetEntity(removeList[i]) as EntityDummy;
                if (dummy.entityAI != null)
                {
                    dummy.entityAI.blackBoard.aiState = AIState.CD_STATE;
                    dummy.entityAI.Destroy();
                }
                if (data.target_handle_type_i == (int)CollectMonsterStateType.COLLECT_DUMMY_DIE)
                {
                    dummy.cur_hp = 0;
                    dummy.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
                }
                else if (data.target_handle_type_i == (int)CollectMonsterStateType.COLLECT_DUMMY_ALIVE)
                {
                    EventDispatcher.TriggerEvent<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, removeList[i], InstanceDefine.DESTROY_SPAWN_COLLECT_MOSTER_TYPE);
                }
            }
            if (index == 0)
            {
                Finish();
            }
        }

        void CollectAppointDummy()
        {
            foreach (var kv in entitiesDummy)
            {
                var entity = kv.Value;
                var dummy = entity as EntityDummy;
                for (int j = 0; j < monsterIds.Length; j++)
                {
                    if (string.IsNullOrEmpty(monsterIds[j]))
                    {
                        continue;
                    }
                    int monsterId = System.Convert.ToInt32(monsterIds[j]);
                    if (dummy.monster_id == monsterId && gameObject.GetComponent<BoxCollider>().bounds.Contains(dummy.position))
                    {
                        removeList.Add(dummy.id);
                    }
                }
            }
        }

        void CollectUnAppointDummy()
        {
            foreach (var kv in entitiesDummy)
            {
                var entity = kv.Value;

                var dummy = entity as EntityDummy;
                if (monsterIds == null && gameObject.GetComponent<BoxCollider>().bounds.Contains(dummy.position))
                {
                    removeList.Add(dummy.id);
                }
                else
                {
                    for (int j = 0; j < monsterIds.Length; j++)
                    {
                        if (monsterIds[j] == null || monsterIds[j] == "")
                        {
                            continue;
                        }
                        int monsterId = System.Convert.ToInt32(monsterIds[j]);
                        if (dummy.monster_id != monsterId && gameObject.GetComponent<BoxCollider>().bounds.Contains(dummy.position))
                        {
                            removeList.Add(dummy.id);
                        }
                    }
                }
            }
        }

        void CollectActionMonster()
        {
            if (data.target_handle_type_i == (int)CollectMonsterStateType.COLLECT_DUMMY_DIE)
            {
                EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Collect_Action, InstanceDefine.DESTROY_SPAWN_DIE_MOSTER_TYPE, data.target_ids_l);
            }
            else if (data.target_handle_type_i == (int)CollectMonsterStateType.COLLECT_DUMMY_ALIVE)
            {
                EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Collect_Action, InstanceDefine.DESTROY_SPAWN_COLLECT_MOSTER_TYPE, data.target_ids_l);
                //MogoEngine.MogoWorld.DestroyEntity(removeList[i]);
            }
            Finish();
        }
    }
}
