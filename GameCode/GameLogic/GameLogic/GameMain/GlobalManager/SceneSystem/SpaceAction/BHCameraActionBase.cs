﻿using GameLoader.Utils.Timer;
using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHCameraActionBase : BHEntityBase
    {
        protected uint _changeCameraTimer = 0;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected void StartNextActionsTimer()
        {
            uint duration = GetChangeDuration();
            DeleteCameraTimer();
            if (duration == 0)
            {
                Finish();
            }
            else
            {
                _changeCameraTimer = TimerHeap.AddTimer(duration, 0, Finish);
            }
        }

        protected override void OnFinish()
        {
            TriggerNextActions();
            Exit();
        }

        virtual protected uint GetChangeDuration()
        {
            return 0;
        }

        protected void DeleteCameraTimer()
        {
            if (_changeCameraTimer > 0)
            {
                TimerHeap.DelTimer(_changeCameraTimer);
                _changeCameraTimer = 0;
            }
        }

        protected override void OnExit()
        {
            DeleteCameraTimer();
        }
    }
}
