﻿using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityNPCAction : BHEntityBase
    {
        new public EntityNPCActionData data { get { return _data as EntityNPCActionData; } }
        protected Dictionary<int, int> randomNums = new Dictionary<int, int>();
        protected string[] point = null;
        protected string[] forwards = null;

        protected override void OnStart()
        {
            var box = gameObject.AddComponent<BoxCollider>();
            box.size = new Vector3(5, 5, 5);
            box.isTrigger = true;
            Active();
        }

        protected override void OnActive()
        {
            forwards = null;
            string[] npcIds = data.npc_id_l.Split(',');
            string[] npcNums = data.npc_nums_l.Split(',');
            string[] str = data.npc_points_l.Split(',');
            if (data.npc_forward_l != null && data.npc_forward_l != "")
            {
                forwards = data.npc_forward_l.Split(',');
            }
            
            int currNpcNums = 0;
            for (int currNpcNumsIndex = 0; currNpcNumsIndex < npcNums.Length; currNpcNumsIndex++)
            {
                currNpcNums += System.Convert.ToInt32(npcNums[currNpcNumsIndex]);
            }

            int total = str.Length / 3;
            int randomTotal = total;
            if (currNpcNums > total)
            {
                LoggerHelper.Error("刷NPC点数量不足，请添加" + (currNpcNums - total) + "组坐标");
            }
            int k = 0;
            //保持格式只有“,”号，在此重新整理数据，3个为一组重新存储
            for (int index = 0; index < total; index++)
            {
                str[index] = str[k] + "," + str[k + 1] + "," + str[k + 2];
                k += 3;
            }

            randomNums = new Dictionary<int, int>();
            int key = 0;
            point = null;

            for (int j = 0; j < npcIds.Length; j++)
            {
                if (npcIds[j] == null || npcIds[j] == "")
                {
                    continue;
                }

                for (int i = 0; i < System.Convert.ToInt32(npcNums[j]); i++)
                {
                    key += 1;
                    MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_NPC, (entity) =>
                    {
                        var npc = entity as EntityNPC;
                        npc.npc_id = int.Parse(npcIds[j]);
                        switch (data.npc_point_type_i)
                        {
                            case (int)MonsterPointType.POSITION:       //定点刷NPC
                                SetPoint(str, key, entity);
                                break;
                            case (int)MonsterPointType.RANDOM_POSITION:
                                SetRandomPoint(str, key, entity, randomTotal);              //随机刷NPC
                                break;
                            default:
                                LoggerHelper.Error("刷NPC类型错误，请检查查配置");
                                return;
                        }
                        
                        if (data.origin_point_type_i == 1)
                        {
                            float x = gameObject.transform.position.x - (data.pos_x_i / 100);
                            float y = gameObject.transform.position.y - (data.pos_y_i / 100);
                            float z = gameObject.transform.position.z - (data.pos_z_i / 100);
                            entity.SetPosition(new Vector3((float.Parse(point[0]) + x), (float.Parse(point[1]) + y), (float.Parse(point[2]) + z)));
                        }
                        else
                        {
                            entity.SetPosition(new Vector3(float.Parse(point[0]), float.Parse(point[1]), float.Parse(point[2])));
                        }
                    });
                }
            }
            Finish();
        }

        private void SetPoint(string[] str, int key, Entity entity)
        {
            point = str[key - 1].Split(',');
            if (forwards != null)
            {
                if (key <= forwards.Length)
                {
                    float rotatioY = float.Parse(forwards[key - 1]);
                    Quaternion rotation = Quaternion.Euler(0, rotatioY, 0);
                    entity.SetRotation(rotation);
                }
                else
                {
                    entity.SetRotation(Quaternion.Euler(0, 0, 0));
                }
            }
            else
            {
                entity.SetRotation(Quaternion.Euler(0, 0, 0));
            }
        }

        private void SetRandomPoint(string[] str, int key, Entity entity, int randomTotal)
        {
            System.Random random = new System.Random();
            int tmp = random.Next(0, randomTotal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, randomTotal, random, tmp);
            }
            randomNums.Add(tmp, tmp);
            point = str[tmp].Split(',');
            if (forwards != null)
            {
                if (tmp <= forwards.Length)
                {
                    float rotatioY = float.Parse(forwards[tmp]);
                    Quaternion rotation = Quaternion.Euler(0, rotatioY, 0);
                    entity.SetRotation(rotation);
                }
                else
                {
                    entity.SetRotation(Quaternion.Euler(0, 0, 0));
                }
            }
            else
            {
                entity.SetRotation(Quaternion.Euler(0, 0, 0));
            }
        }

        //检测随机数是否重复，避免坐标
        public int getNum(int minVal, int maxVal, System.Random ra, int tmp)
        {
            tmp = ra.Next(minVal, maxVal);
            if (randomNums.ContainsKey(tmp))
            {
                tmp = getNum(0, maxVal, ra, tmp);
            }

            return tmp;
        }
    }
}
