﻿using Common.Events;
using MogoEngine.Events;
using SpaceSystem;
using System;

namespace GameMain.GlobalManager
{
    public class BHEntityCondTriggerAction : BHEntityBase
    {
        new public EntityCondTriggerActionData data { get { return _data as EntityCondTriggerActionData; } }
        private SpaceConditionBase _condition;
        private int _currentTriggeredCount = 0;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            _condition = space_condition_helper.CreateSpaceCondition(data.event_type_i);
            if (_condition == null)
            {
                return;
            }
            AddEventListener();
            _condition.Enter(data);
            if (_condition.IsSatisfy())
            {
                Active();
            }
        }

        protected override void OnActive()
        {
            if (IsReachTriggeredCount())
            {
                return;
            }
            TriggerOnce();
        }

        private void TriggerOnce()
        {
            _nextActionPosition = MogoEngine.MogoWorld.Player.position;
            _nextActionEntityId = MogoEngine.MogoWorld.Player.id;
            TriggerNextActions();
            _currentTriggeredCount++;
            _condition.ResetTriggerCount();
            if (IsReachTriggeredCount())
            {
                Finish();
            }
        }

        protected override void OnFinish()
        {
            Exit();
        }

        protected override void OnExit()
        {
            RemoveEventListener();
            if (_condition != null)
            {
                _condition.Exit();
                _condition = null;
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, CheckIsTarget);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, CheckIsTarget);
        }

        private void CheckIsTarget(int condType, string trgetIds)
        {
            if (condType != data.event_type_i) return;
            if (_condition.IsSatisfy(trgetIds))
            {
                Active();
            }
        }

        private bool IsReachTriggeredCount()
        {
            return data.close_flag_i > 0 && _currentTriggeredCount >= data.close_flag_i;
        }
    }
}
