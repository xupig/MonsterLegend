﻿using GameMain.Entities;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityCollectItemAction : BHEntityBase
    {
        new public EntityCollectItemActionData data { get { return _data as EntityCollectItemActionData; } }
        protected bool hasTriggered = false;

        protected override void OnStart()
        {
            string[] boxSize = null;
            if (data.boxSize != null && data.boxSize != "")
            {
                boxSize = data.boxSize.Split(',');
            }
            var box = gameObject.AddComponent<BoxCollider>();
            if (boxSize != null)
            {
                box.size = new Vector3(int.Parse(boxSize[0]), int.Parse(boxSize[1]), int.Parse(boxSize[2]));
            }
            else
            {
                box.size = Vector3.one;
            }
            box.isTrigger = true;

            Active();
        }

        protected override void OnActive()
        {
            if (hasTriggered) return;
            hasTriggered = true;

            var entities = MogoEngine.MogoWorld.Entities;
            string[] itemIds = data.target_ids_l.Split(',');
            List<uint> removeList = new List<uint>();
            foreach (var kv in entities)
            {
                var entityID = kv.Key;
                var entity = kv.Value;
                if (entity is EntityClientDropItem)
                {
                    var dropItem = entity as EntityClientDropItem;

                    for (int j = 0; j < itemIds.Length; j++)
                    {
                        if (itemIds[j] == null || itemIds[j] == "")
                        {
                            continue;
                        }
                        int itemId = System.Convert.ToInt32(itemIds[j]);
                        if (dropItem.type_id == itemId)
                        {
                            if (gameObject.GetComponent<BoxCollider>().bounds.Contains(dropItem.position))
                            {
                                var monEntity = MogoEngine.MogoWorld.GetEntity(entityID) as MogoEngine.RPC.Entity;
                                removeList.Add(monEntity.id);
                            }
                        }
                    }
                }
            }
            int index = removeList.Count;
            for (int i = 0; i < removeList.Count; i++)
            {
                if (data.target_handle_type_i == (int)CollectItemType.COLLECT_APPOINT_ITEM)
                {
                    var dropItem = MogoEngine.MogoWorld.GetEntity(removeList[i]) as EntityClientDropItem;
                    dropItem.Pick();
                }
                else if (data.target_handle_type_i == (int)CollectItemType.COLLECT_UNAPPOINT_ITEM)
                {
                    MogoEngine.MogoWorld.DestroyEntity(removeList[i]);
                }
                index--;
            }
            if (index == 0)
            {
                //暂未区分成功失败行为ID，先暂定为成功(目测只会成功)
                _owner.CreateActions(data.success_actions_l, gameObject.transform.position);
            }
            Finish();
        }
    }
}
