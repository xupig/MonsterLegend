﻿using SpaceSystem;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityCameraChangeAction : BHCameraActionBase
    {
        new public EntityCameraChangeData data { get { return _data as EntityCameraChangeData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Vector3 pos = Vector3.zero;
            if (data.camera_type_i == 4)
            {
                pos = new Vector3(data.pos_x_i / 100, data.pos_y_i / 100, data.pos_z_i / 100);
            }
            else if (data.camera_type_i == 5)
            {
                string[] ros = data.rotation_pos_l.Split(',');
                pos = new Vector3(System.Convert.ToInt32(ros[0]), System.Convert.ToInt32(ros[1]), System.Convert.ToInt32(ros[2]));
            }
            CameraManager.GetInstance().ChangeCamera(data.camera_type_i, data.camera_rotationX_f, data.camera_rotationY_f, data.camera_rotateTime_f, data.camera_distance_f, pos, data.camera_moveTime_f, data.camera_move_speep_f, data.camera_move_odds_f, data.camera_concuss_id_l, data.camera_fov_i);
            StartNextActionsTimer();
        }

        override protected uint GetChangeDuration()
        {
            uint duration = 0;
            switch (data.camera_type_i)
            {
                case CameraConfig.CAMERA_TYPE_ROTATE:
                    duration = 2000;
                    break;
                case CameraConfig.CAMERA_TYPE_SHAKE_1:
                case CameraConfig.CAMERA_TYPE_SHAKE_2:
                    string[] str = data.camera_concuss_id_l.Split(',');
                    duration = (uint)(float.Parse(str[1]) * 1000);
                    break;
                case CameraConfig.CAMERA_TYPE_MOVE:
                    duration = (uint)(data.camera_moveTime_f * 1000);
                    break;
                default:
                    break;
            }
            return duration;
        }
    }
}
