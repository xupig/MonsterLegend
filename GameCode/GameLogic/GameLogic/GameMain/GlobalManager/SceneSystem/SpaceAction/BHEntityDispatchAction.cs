﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityDispatchAction : BHEntityBase
    {
        new public EntityDispatchActionData data { get { return _data as EntityDispatchActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Finish();
        }
    }
}
