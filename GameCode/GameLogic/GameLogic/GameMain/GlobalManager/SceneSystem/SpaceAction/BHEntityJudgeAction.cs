﻿using GameMain.Entities;
using MogoEngine.RPC;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityJudgeAction : BHEntityBase
    {
        new public EntityJudgeActionData data { get { return _data as EntityJudgeActionData; } }
        private Dictionary<int, string> judegeInfo = new Dictionary<int, string>();

        protected override void OnStart()
        {
            string[] str = data.next_actions_n.Split(';');
            for (int i = 0; i < str.Length; i++)
            {
                string[] dicInfo = str[i].Split(':');
                if (judegeInfo.ContainsKey(System.Convert.ToInt32(dicInfo[0])))
                {
                    judegeInfo.Remove(System.Convert.ToInt32(dicInfo[0]));
                }
                judegeInfo.Add(System.Convert.ToInt32(dicInfo[0]), dicInfo[1]);
            }
            Active();
        }

        protected override void OnActive()
        {
            int key = 0;
            Entity entity = MogoEngine.MogoWorld.GetEntity(data.entity_id_i);

            EntityCreature entityAvatar = entity as EntityCreature;
            // 0-campId 1-pve 2-pvp
            switch (data.target_type_i)
            {
                case (int)JudgeType.JUDGE_CAMP_ID:
                    key = (int)entityAvatar.map_camp_id;
                    break;
                case (int)JudgeType.JUDGE_CAMP_PVE:
                    key = entityAvatar.GetCampPveType();
                    break;
                case (int)JudgeType.JUDGE_CAMP_PVP:
                    key = entityAvatar.GetCampPvpType();
                    break;
                default:
                    key = 0;
                    break;
            }

            if (judegeInfo.ContainsKey(key))
            {
                _owner.CreateActions(judegeInfo[key], entity.position, entity.id);
            }

            Finish();
        }

        protected override void OnExit()
        {
            judegeInfo.Clear();
            judegeInfo = null;
        }
    }
}
