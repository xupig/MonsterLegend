﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntitySystemNoticeAction : BHEntityBase
    {
        new public EntitySystemNoticeActionData data { get { return _data as EntitySystemNoticeActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            switch (data.notice_mode_type_i)
            {
                case InstanceDefine.SYSTEM_NOTICE_ACTION_UP_TYPE:
                case InstanceDefine.SYSTEM_NOTICE_ACTION_MIDDLE_TYPE:
                    ShowSystemNoticeFloatView(data);
                    break;
                //case InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE:
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE:
                    ShowSystemNoticeRightUpView(data);
                    break;
                default:
                    break;
            }
            Finish();
        }

        private void ShowSystemNoticeRightUpView(EntitySystemNoticeActionData data)
        {
            SystemNoticeRightUpData notice = new SystemNoticeRightUpData();
            notice.type = data.notice_mode_type_i;
            notice.time = (uint)data.notice_countdown_i * 1000;
            notice.content = data.notice_content_id_i;
            notice.priority = data.show_priority_i;
            notice.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_CONTENT_TYPE;
            notice.rightUpNoticeId = data.id_i;
            //Ari SpaceNoticeActionManager.Instance.AddSystemNotice(notice);
        }

        private void ShowSystemNoticeFloatView(EntitySystemNoticeActionData data)
        {
            SystemNoticeDisplayData notice = new SystemNoticeDisplayData();
            notice.type = data.notice_mode_type_i;
            notice.time = (uint)data.notice_countdown_i * 1000;
            notice.content = data.notice_content_id_i;
            notice.priority = data.show_priority_i;
            //Ari SpaceNoticeActionManager.Instance.AddSystemNotice(notice);
        }

    }
}
