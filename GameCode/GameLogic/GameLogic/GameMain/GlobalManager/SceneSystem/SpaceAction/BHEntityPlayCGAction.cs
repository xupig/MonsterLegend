﻿using Common.ClientConfig;
using Common.Events;
using GameData;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityPlayCGAction : BHEntityBase
    {
        new public EntityPlayCGActionData data { get { return _data as EntityPlayCGActionData; } }
        private string _dramaKey = "";

        protected override void OnEnter()
        {
            data.next_actions_delay_time_i = 0;
            if (!_scene.IsClientScene())
            {
                data.next_actions_l = string.Empty;
            }
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            AddEventListener();
            Active();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, string>(DramaEvents.DRAMA_END, OnDramaEnd);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, string>(DramaEvents.DRAMA_END, OnDramaEnd);
        }

        void OnDramaEnd(int dramaId, string dramaKey)
        {
            if (_dramaKey == dramaKey)
            {
                Finish();
            }
        }

        protected override void OnActive()
        {
            var triggers = drama_trigger_helper.GetDramaTriggers(data.cg_event_name, data.cg_event_value);
            if (triggers == null || triggers.Count == 0 || triggers.Count > 1)
            {
                //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, string.Format("PLAYCG报错，找不到CG{0}，或触发的CG不止一个，@谢思宇", data.cg_event_value));
                Finish();
                return;
            }

            _dramaKey = triggers[0].drama + triggers[0].mark;
            if (!DramaManager.GetInstance().Trigger(data.cg_event_name, data.cg_event_value))
            {
                Finish();
                return;
            }
            //if (_scene.IsClientScene())
            //{
            //    AddCGBuff();
            //}
        }

        private void AddCGBuff()
        {
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            while (enumerator.MoveNext())
            {
                if (!(enumerator.Current.Value is EntityCreature)) continue;
                creature = enumerator.Current.Value as EntityCreature;
                creature.bufferManager.AddBuffer(InstanceDefine.BUFF_ID_VIEWING_MODE, 999999f, true);
            }
        }

        private void RemoveCGBuff()
        {
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            while (enumerator.MoveNext())
            {
                if (!(enumerator.Current.Value is EntityCreature)) continue;
                creature = enumerator.Current.Value as EntityCreature;
                creature.bufferManager.RemoveBuffer(InstanceDefine.BUFF_ID_VIEWING_MODE);
            }
        }

        protected override void OnFinish()
        {
            _nextActionEntityId = data.entity_id_i;
            TriggerNextActions();
            Exit();
        }

        protected override void OnExit()
        {
            RemoveEventListener();
            DramaManager.GetInstance().SkipDrama(_dramaKey);
            //if (_scene.IsClientScene())
            //{
            //    RemoveCGBuff();
            //}
        }
    }
}
