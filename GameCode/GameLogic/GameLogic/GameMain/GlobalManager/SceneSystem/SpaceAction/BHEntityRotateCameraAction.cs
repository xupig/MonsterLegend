﻿
using SpaceSystem;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityRotateCameraAction : BHCameraActionBase
    {
        new public EntityRotateCameraData data { get { return _data as EntityRotateCameraData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            bool keepDistanceScale = !(Convert.ToBoolean(data.reset_distance_scale_i));
            var targetTransform = ActionEntitiesUtils.GetCameraTargetTransform(data.target_i, data.target_slot_name_s);
            CameraManager.GetInstance().ChangeToRotationTargetMotion(targetTransform,
                ActionEntitiesUtils.StringToVector3(data.camera_rotation_l), data.camera_rotation_duration_f,
                data.camera_rotation_accelerate_rate_f,
                data.camera_distance_f, data.camera_distance_duration_f, true, keepDistanceScale);
            StartNextActionsTimer();
        }

        override protected uint GetChangeDuration()
        {
            return (uint)(Mathf.Max(data.camera_distance_duration_f, data.camera_rotation_duration_f) * 1000);
        }
    }
}
