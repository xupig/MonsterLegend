﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityChangeCameraFovAction : BHEntityBase
    {
        new public EntityChangeCameraFovData data { get { return _data as EntityChangeCameraFovData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            CameraManager.GetInstance().ChangeCameraFov(data.camera_fov_i);
            Finish();
        }
    }
}
