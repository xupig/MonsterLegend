﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHEntityTimerAction : BHEntityBase
    {
        new public EntityTimerActionData data { get { return _data as EntityTimerActionData; } }
        private float m_actionTime = 0f;
        private uint m_actionTimeId;
        private int m_currTime;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            m_currTime = GetCurrTime(); 
            AddEventListener();
            Active();
        }

        private int GetCurrTime()
        {
            int time = 0;
            string[] args = data.time_type_args_l.Split(',');
            switch (data.time_type_i)
            {
                case InstanceDefine.TIMER_TYPE_NORMAL:
                case InstanceDefine.TIMER_TYPE_SPECIAL:
                    time = int.Parse(args[0]);
                    break;
                case InstanceDefine.TIMER_TYPE_RANDOM_RANGE:
                    time = MogoMathUtils.Random(int.Parse(args[0]), int.Parse(args[1]) + 1);
                    break;
                case InstanceDefine.TIMER_TYPE_RANDOM_COUNT:
                    time = int.Parse(args[MogoMathUtils.Random(0, args.Length)]);
                    break;
                default:
                    break;
            }
            return time;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<string[]>(SpaceActionEvents.SpaceActionEvents_ChangeTimer, CheckIsTarget);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<string[]>(SpaceActionEvents.SpaceActionEvents_ChangeTimer, CheckIsTarget);
        }

        void CheckIsTarget(string[] str)
        {
            if (System.Convert.ToInt32(str[0]) == data.id_i)
            {
                if (str[1] == "0") //0-重置剩余时间  1-修改剩余时间
                {
                    m_currTime = System.Convert.ToInt32(str[2]);
                    m_actionTime = 0;
                }
                else if (str[1] == "1")
                {
                    m_actionTime -= System.Convert.ToInt32(str[2]);
                }
                if (data.is_can_trigger_notice_i == 1)
                {
                    ShowNotice();
                }
            }
        }

        protected override void OnActive()
        {
            if (data.is_can_trigger_notice_i == 1)
            {
                ShowNotice();
            }
            m_actionTimeId = TimerHeap.AddTimer(0, 100, ActionTimeClock);
        }

        void ActionTimeClock()
        {
            m_actionTime += 100;
            if ((m_currTime - m_actionTime) <= 0f)
            {
                DeleteActionTimeClick();
                Finish();
            }
        }

        void ShowNotice()
        {
            SystemNoticeRightUpData systemNoticeData = new SystemNoticeRightUpData();
            systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE;
            systemNoticeData.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TIME_TYPE;
            systemNoticeData.priority = data.show_priority_i;
            systemNoticeData.time = (uint)(m_currTime);
            systemNoticeData.content = data.notice_content_id_i;
            systemNoticeData.rightUpNoticeId = data.id_i;
            SpaceNoticeActionManager.Instance.AddSystemNotice(systemNoticeData);
        }

        protected override void OnExit()
        {
            DeleteActionTimeClick();
            RemoveEventListener();
            base.OnExit();
        }

        public void DeleteActionTimeClick()
        {
            TimerHeap.DelTimer(m_actionTimeId);
            m_actionTime = 0;
            m_actionTimeId = 0;
        }

        protected override void OnFinish()
        {
            TriggerNextActions();
            Exit();
        }
    }
}
