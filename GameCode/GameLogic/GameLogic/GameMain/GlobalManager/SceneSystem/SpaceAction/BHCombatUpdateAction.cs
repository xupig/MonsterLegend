﻿using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class BHCombatUpdateAction : BHEntityBase
    {
        new public CombatUpdateActionData data { get { return _data as CombatUpdateActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Finish();
        }
    }
}
