﻿using Common.ClientConfig;
using Common.Events;
using MogoEngine.Events;
using SpaceSystem;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class BHEntityPlayEffectAction : BHEntityBase
    {
        new public EntityPlayEffectActionData data { get { return _data as EntityPlayEffectActionData; } }
        protected bool hasTriggered = false;
        GameObject _effectObj;

        protected override void OnEnter()
        {
            _isPersistent = true;
        }

        protected override void OnStart()
        {
            string[] boxSize = null;
            if (data.boxSize != null && data.boxSize != "")
            {
                boxSize = data.boxSize.Split(',');
                string[] rotation = data.rotation_pos_l.Split(',');
                gameObject.transform.localEulerAngles = new Vector3(float.Parse(rotation[0]), float.Parse(rotation[1]), float.Parse(rotation[2]));
                if (boxSize != null)
                {
                    gameObject.transform.localScale = new Vector3(float.Parse(boxSize[0]), float.Parse(boxSize[1]), float.Parse(boxSize[2]));
                }
                else
                {
                    gameObject.transform.localScale = Vector3.one;
                }
            }
            AddEventListener();
            Active();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_PlayEffect, CheckIsTarget);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.AddEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_PlayEffect, CheckIsTarget);
        }

        void CheckIsTarget(int currId, int effectState)
        {
            if (currId == data.id_i)
            {
                SetEffectState(effectState);
            }
        }

        void SetEffectState(int effectState)
        {
            if (_effectObj != null)
            {
                _effectObj.GetComponent<Animator>().SetInteger("Action", effectState);
            }
        }

        protected override void OnActive()
        {
            PlaySound();
            PlayMusic();
            if (hasTriggered)
            {
                return;
            }
            hasTriggered = true;
            PlayEffect();
            Finish();
        }

        private void PlaySound()
        {
            if (string.IsNullOrEmpty(data.sound_id_l))
            {
                return;
            }
            string[] soundList = data.sound_id_l.Split(',');
            for (int i = 0; i < soundList.Length; ++i)
            {
                SoundInfoManager.Instance.PlaySound(int.Parse(soundList[i]), data.sound_loop_b);
            }
        }

        private void PlayMusic()
        {
            if (string.IsNullOrEmpty(data.music_id_l))
            {
                return;
            }
            string[] musicList = data.music_id_l.Split(',');
            for (int i = 0; i < musicList.Length; ++i)
            {
                BgMusicManager.Instance.PlayAreaTriggerMusic(int.Parse(musicList[i]), data.music_loop_b);
            }
        }

        private void PlayEffect()
        {
            if (string.IsNullOrEmpty(data.effect_id_i))
            {
                return;
            }
            string path = data.effect_id_i + ".prefab";
            var gObj = this.gameObject;
            Action<GameObject> effectGoCallback = delegate(GameObject obj)
            {
                if (gObj == null) 
                {
                    GameObject.Destroy(obj);
                    return;
                }
                _effectObj = obj;
                _effectObj.transform.SetParent(gameObject.transform, false);
                _effectObj.transform.position = Vector3.zero;
                //effectObj.transform.localEulerAngles = new Vector3(float.Parse(rotation[0]), float.Parse(rotation[1]), float.Parse(rotation[2]));
                _effectObj.transform.localPosition = Vector3.zero;
            };
            Game.Asset.ObjectPool.Instance.GetGameObject(path, effectGoCallback);
        }

        protected override void OnExit()
        {
            _effectObj = null;
        }
    }
}
