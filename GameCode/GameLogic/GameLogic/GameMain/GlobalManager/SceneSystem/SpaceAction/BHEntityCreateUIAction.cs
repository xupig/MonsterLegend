﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using MogoEngine.Events;
using SpaceSystem;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class BHEntityCreateUIAction : BHEntityBase
    {
        new public EntityCreateUIActionData data { get { return _data as EntityCreateUIActionData; } }

        protected override void OnStart()
        {
            Active();
        }

        protected override void OnActive()
        {
            Dictionary<int, List<int>> uiTypeDic = MogoStringUtils.Convert2OneToMany(data.ui_type_n);
            foreach (KeyValuePair<int, List<int>> kvp in uiTypeDic)
            {
                SystemNoticeTopData systemNoticeData = new SystemNoticeTopData();
                systemNoticeData.type = InstanceDefine.COUNTER_NOTICE_ACTION_INIT;
                systemNoticeData.notice_type = kvp.Key;
                systemNoticeData.timerCount = kvp.Value[0];
                systemNoticeData.maxCount = kvp.Value[1];
                //Ari SpaceNoticeActionManager.Instance.AddSystemNotice(systemNoticeData);
            }
            Finish();
        }
    }
}
