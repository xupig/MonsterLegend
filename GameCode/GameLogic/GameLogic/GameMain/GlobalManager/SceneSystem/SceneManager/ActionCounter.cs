﻿using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class ActionIDCounter
    {
        private int _id = 0;
        public int GetID()
        {
            return ++_id;
        }
    }

    public class ActionCounter
    {
        private int _actionId;
        private List<int> _spaceActionList;
        private int _triggeredCounter;    //被创建次数计数器
        public ActionCounter(int actionId)
        {
            _actionId = actionId;
            _spaceActionList = new List<int>();
        }

        ~ActionCounter()
        {
            _spaceActionList.Clear();
            _spaceActionList = null;
        }

        public void AddSpaceActionID(int id)
        {
            _spaceActionList.Add(id);
        }

        public void RemoveSpaceActionID(int id)
        {
            _spaceActionList.Remove(id);
        }

        public void AddTriggeredCount()
        {
            _triggeredCounter++;
        }

        public int GetTriggeredCount()
        {
            return _triggeredCounter;
        }

        public List<int> GetSpaceActionList()
        {
            return _spaceActionList;
        }

        public void ClearSpaceActionList()
        {
            _spaceActionList.Clear();
        }
    }
}
