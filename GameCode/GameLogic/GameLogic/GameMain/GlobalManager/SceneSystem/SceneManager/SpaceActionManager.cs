﻿using ACTSystem;
using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils;
using MogoEngine.Events;
using SpaceSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class SpaceActionManager
    {
        public static string ACTION_ENTITIES = "ActionEntities";
        private GameScene _scene;
        private SpaceData _spaceData;
        private GameObject _spaceSettingObj;
        private Dictionary<int, ISpaceAction> _spaceActionDict;
        private Dictionary<int, ActionCounter> _actionCounterList;    
        private ActionIDCounter _counter;
        private NextActionsManager _nextActionCreator;
        private SpaceActionInspectingManager _spaceActionInspectingCache;

        public GameScene Scene
        {
            get { return _scene; }
        }

        public SpaceActionManager()
        {
            _spaceActionDict = new Dictionary<int, ISpaceAction>();
            _actionCounterList = new Dictionary<int, ActionCounter>();
            _counter = new ActionIDCounter();
            _nextActionCreator = new NextActionsManager(this);
            _spaceActionInspectingCache = new SpaceActionInspectingManager();
        }

        public void Enter(GameScene scene)
        {
            _scene = scene;
            _spaceData = scene.GetSpaceData();
            CreateSpaceSetting();
            CreateSetBeginStateAction();
        }

        public void Exit()
        {
            TerminateAllActions();
            _nextActionCreator.Exit();
            _nextActionCreator = null;
            _spaceActionInspectingCache.Exit();
            _spaceActionInspectingCache = null;
            _counter = null;
            _spaceSettingObj = null;
            _spaceData = null;
            _scene = null;
        }

        //获取行为数据
        public EntityData GetEntityData(int actionId)
        {
            if (!_spaceData.entities.entityDic.ContainsKey(actionId))
            {
                LoggerHelper.Error(string.Format("获取行为失败，space名为{0}不包含id为{1}的行为。", _spaceData.globalParams.mapname, actionId));
                return null;
            }
            return _spaceData.entities.entityDic[actionId] as EntityData;
        }

        //创建行为
        public void CreateAction(int actionId, Vector3 position, uint entityId = 0)
        {
            EntityData data = GetEntityData(actionId);
            if (data == null)
            {
                return;
            }
            CreateAction(data, position, entityId);
        }

        //创建多个行为
        public void CreateActions(string actionIdList, Vector3 position, uint entityId = 0)
        {
            if (string.IsNullOrEmpty(actionIdList))
            {
                return;
            }
            string[] actions = actionIdList.Split(',');
            for (int i = 0; i < actions.Length; ++i)
            {
                CreateAction(int.Parse(actions[i]), position, entityId);
            }
        }

        //行为是否创建过
        public bool ExistAction(int actionId)
        {
            return GetTriggeredCount(actionId) > 0;
        }

        //删除某id的行为
        public void DeleteAction(int id)
        {
            if (_spaceActionDict.ContainsKey(id))
            {
                ISpaceAction action = _spaceActionDict[id];
                action.Exit();
            }
        }

        //终止某actionId的所有行为
        public bool TerminateActions(int actionId)
        {
            if (!_actionCounterList.ContainsKey(actionId))
            {
                return false;
            }
            List<int> spaceActionList = _actionCounterList[actionId].GetSpaceActionList();
            if (spaceActionList.Count == 0)
            {
                return false;
            }
            //for (int i = spaceActionList.Count - 1; i >= 0; --i)
            while (spaceActionList.Count > 0)
            {
                DeleteAction(spaceActionList[spaceActionList.Count - 1]);
            }
            _actionCounterList[actionId].ClearSpaceActionList();
            return true;
        }

        //终止全部行为
        public void TerminateAllActions()
        {
            if (_actionCounterList != null)
            {
                foreach (var node in _actionCounterList)
                {
                    TerminateActions(node.Key);
                }
                _actionCounterList.Clear();
                _actionCounterList = null;
            }
            if (_spaceActionDict != null)
            {
                _spaceActionDict.Clear();
                _spaceActionDict = null;
            }
        }

        //获得某actionId被创建的次数
        public int GetTriggeredCount(int actionId)
        {
            if (_actionCounterList.ContainsKey(actionId))
            {
                return _actionCounterList[actionId].GetTriggeredCount();
            }
            return 0;
        }

        //行为状态改变触发
        public void OnActionStatusChange(int id, int actionId, SpaceActionStatus status)
        {
            switch (status)
            {
                case SpaceActionStatus.ON_ENTER:
                    _scene.OnCreateAction(id, actionId);
                    AddInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_CREATE, actionId);
                    break;
                case SpaceActionStatus.ON_FINISH:
                    EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_ACTION, actionId.ToString());
                    break;
                case SpaceActionStatus.ON_EXIT:
                    RemoveFromSpaceActionList(id);
                    AddInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_DESTROY, actionId);
                    break;
                default:
                    break;
            }
        }

        //触发next_actions
        public void TriggerNextActions(string actionIdList, Vector3 position, uint entityId, uint nextActionDelayTime)
        {
            _nextActionCreator.CreateNextActions(actionIdList, position, entityId, nextActionDelayTime);
        }

        //添加行为状态显示监听
        public void AddInspectingCache(int type, int actionId)
        {
            if (_spaceActionInspectingCache != null)
            {
                _spaceActionInspectingCache.AddInspectingCache(type, actionId);
            }
        }

        private void CreateSpaceSetting()
        {
            while (true)
            {
                _spaceSettingObj = GameObject.Find(ACTION_ENTITIES);
                if (_spaceSettingObj != null) GameObject.DestroyImmediate(_spaceSettingObj);
                else break;
            }
            _spaceSettingObj = new GameObject(ACTION_ENTITIES);
            _spaceSettingObj.layer = UnityEngine.LayerMask.NameToLayer("SpaceAction");
            if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new GameMain.Inspectors.InspectingActionEntities(_spaceActionInspectingCache), _spaceSettingObj);
        }

        private void CreateSetBeginStateAction()
        {
            foreach (var entityData in _scene.GetSpaceData().entities.entityList)
            {
                if (entityData.type == EntityType.SET_BEGIN_STATE_ACTION)
                {
                    CreateAction(entityData, Vector3.zero);
                    break;
                }
            }
        }

        private void CreateAction(EntityData data, Vector3 position, uint entityId = 0)
        {
            EntityData entityData = data.Clone();
            if (!CheckCreatedCount(data.id_i, entityData.max_trig_num_i))
            {
                return;
            }
            if (!CheckActionType(data.action_type_i))
            {
                return;
            }
            entityData.entity_id_i = entityId;
            GameObject settingObj = GameObject.Find(ACTION_ENTITIES);
            var obj = new GameObject(entityData.type + "(" + entityData.id_i + ")");
            entityData.editorObj = obj;

            obj.layer = UnityEngine.LayerMask.NameToLayer("SpaceAction");
            if (entityData.origin_point_type_i == 0)
            {
                entityData.editorObj.transform.position
                = new Vector3(SpaceDataConfig.ScaleToReal(entityData.pos_x_i),
                              SpaceDataConfig.ScaleToReal(entityData.pos_y_i),
                              SpaceDataConfig.ScaleToReal(entityData.pos_z_i));
            }
            else if (entityData.origin_point_type_i == 1)
            {
                if (entityData.origin_point_args_l != null && entityData.origin_point_args_l != "")
                {
                    float xoffset = float.Parse(entityData.origin_point_args_l.Split(',')[0]) * 0.01f;
                    float zoffset = float.Parse(entityData.origin_point_args_l.Split(',')[1]) * 0.01f;
                    position.x = position.x + xoffset;
                    position.z = position.z + zoffset;
                }
                position.y = MogoSceneUtils.GetGoundYPosition(position);
                entityData.editorObj.transform.position = position;
            }
            int id = _counter.GetID();
            var t = SpaceSystemAdapter.GetEntityTypeRuntimeBehaviourMap()[entityData.type];
            ISpaceAction com = obj.AddComponent(t) as ISpaceAction;
            if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new GameMain.Inspectors.InspectingActionEntity(com), obj);
            try
            {
                com.Enter(id, entityData, this);
            }
            catch (Exception ex){
                LoggerHelper.Error("Action Enter Error:" + ex.Message);
            }
            obj.transform.SetParent(settingObj.transform);
            AddToSpaceActionList(id, entityData.id_i, com);
            AddTriggeredCount(data.id_i);
        }

        private bool CheckActionType(int actionType)
        {
            bool bo = false;
            if (!_scene.IsClientScene() && 
                (actionType == (int)EntityActionType.CLIENT || actionType == (int)EntityActionType.SERVER_TO_CLIENT))
            {
                bo = true;
            }
            else if (_scene.IsClientScene())
            {
                bo = true;
            }
            return bo;
        }

        private bool CheckCreatedCount(int actionId, int maxCount)
        {
            int createdCount = GetTriggeredCount(actionId);
            if (maxCount > 0 && createdCount >= maxCount)
            {
                LoggerHelper.Error(string.Format("行为id = {0}已被激活过{1}次，超过最大次数{2}。", actionId, createdCount, maxCount));
                return false;
            }
            return true;
        }

        private void AddToSpaceActionList(int id, int actionId, ISpaceAction spaceAction)
        {
            if (_spaceActionDict.ContainsKey(id))
            {
                _spaceActionDict.Remove(id);
            }
            _spaceActionDict.Add(id, spaceAction);
            ActionCounter actionCounter;
            if (_actionCounterList.ContainsKey(actionId) && _actionCounterList[actionId] != null)
            {
                actionCounter = _actionCounterList[actionId];
            }
            else
            {
                actionCounter = new ActionCounter(actionId);
                _actionCounterList.Add(actionId, actionCounter);
            }
            actionCounter.AddSpaceActionID(id);
        }

        private void RemoveFromSpaceActionList(int id)
        {
            int actionId = 0;
            if (_spaceActionDict.ContainsKey(id))
            {
                actionId = _spaceActionDict[id].GetActionID();
                _spaceActionDict.Remove(id);
            }
            if (_actionCounterList.ContainsKey(actionId))
            {
                _actionCounterList[actionId].RemoveSpaceActionID(id);
            }
        }

        private void AddTriggeredCount(int actionId)
        {
            if (_actionCounterList.ContainsKey(actionId))
            {
                _actionCounterList[actionId].AddTriggeredCount();
            }
        }
    }
}
