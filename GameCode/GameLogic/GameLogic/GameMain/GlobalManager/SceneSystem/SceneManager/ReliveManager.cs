﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class ReliveManager
    {
        private CombatScene _scene;
        private int _relivedNum = 0;
        private int _reliveTotalNum = 0;
        private List<int> _reliveCdList;
        private List<int> _reliveCostList;
        private int _reliveType = 0;
        public ReliveManager(CombatScene scene)
        {
            _scene = scene;
            Init();
            AddEventListener();
        }

        public void Exit()
        {
            RemoveEventListener();
            _reliveCdList = null;
            _reliveCostList = null;
            _scene = null;
        }

        private void AddEventListener()
        {
            MogoEngine.Events.EventDispatcher.AddEventListener(SpaceActionEvents.SpaceActionEvents_PlayerDeath, OnPlayerDeath);
        }

        private void RemoveEventListener()
        {
            MogoEngine.Events.EventDispatcher.RemoveEventListener(SpaceActionEvents.SpaceActionEvents_PlayerDeath, OnPlayerDeath);
        }

        private void Init()
        {
            int instanceID = _scene.GetInstanceID();
            _reliveTotalNum = instance_helper.GetReliveNum(instanceID);
            _reliveCdList = instance_helper.GetReliveCdList(instanceID);
            _reliveCostList = instance_helper.GetReliveCostList(instanceID);
        }

        public void SetReliveType(int reliveType)
        {
            _reliveType = reliveType;
        }

        public void OnShowRelivePanel(UInt16 cd, int reliveRemainNum)
        {
            if (reliveRemainNum <= 0)
            {
                return;
            }
            int relivedNum = _reliveTotalNum - reliveRemainNum;
            if (relivedNum < 0)
            {
                LoggerHelper.Error(string.Format("复活返回报错，当前剩余复活次数为{0}, 该副本总复活次数为{1}.", reliveRemainNum, _reliveTotalNum));
                return;
            }
            int cdtime = (_reliveCdList.Count <= relivedNum) ? _reliveCdList[_reliveCdList.Count - 1] : _reliveCdList[relivedNum];
            int price = (_reliveCostList.Count <= relivedNum) ? _reliveCostList[_reliveCostList.Count - 1] : _reliveCostList[relivedNum];

            float odds = price * (1.0f / cdtime);
            ReliveData reliveData = new ReliveData();
            reliveData.cdTime = cdtime;
            reliveData.currReliveCount = reliveRemainNum;
            reliveData.price = price;
            reliveData.odds = (int)odds;
            UIManager.Instance.ShowPanel(Common.Base.PanelIdEnum.RelivePop, reliveData);
        }

        void OnPlayerDeath()
        {
            if (!_scene.IsClientScene())
            {
                return;
            }
            if (_scene is CombatScene)
            {
                (_scene as CombatScene).SetPlayerDeathCount();
            }
            if (_relivedNum >= _reliveTotalNum)
            {
                EventDispatcher.TriggerEvent(SpaceActionEvents.SpaceActionEvents_ReliveUseOut);
            }
            if (_scene.isCombatComplete)    //副本结束后不需要打开窗口
            {
                return;
            }
            OnShowRelivePanel(0, _reliveTotalNum - _relivedNum);
            _relivedNum++;
        }

        public void OnRelive()
        {
            if (_scene.IsClientScene())
            {
                if (_reliveType == 1)
                {
                    PlayerAvatar.Player.Teleport(_scene.GetEnterPoint());
                }
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, PlayerAvatar.Player.max_hp);
                PlayerAvatar.Player.stateManager.SetState(state_config.AVATAR_STATE_DEATH, false);
            }
        }
    }
}
