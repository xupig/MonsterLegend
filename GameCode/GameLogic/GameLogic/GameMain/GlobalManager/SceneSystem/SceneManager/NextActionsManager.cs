﻿using GameLoader.Utils.Timer;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class NextActionsManager
    {
        private SpaceActionManager _owner;
        List<uint> _timerIdList;
        public NextActionsManager(SpaceActionManager owner)
        {
            _timerIdList = new List<uint>();
            _owner = owner;
        }

        public void Exit()
        {
            ClearTimerList();
            _owner = null;
        }

        private void ClearTimerList()
        {
            for (int i = 0; i < _timerIdList.Count; ++i)
            {
                TimerHeap.DelTimer(_timerIdList[i]);
            }
            _timerIdList.Clear();
            _timerIdList = null;
        }

        public void CreateNextActions(string nextActions, Vector3 position, uint entityId = 0, uint delayTime = 0)
        {
            if (delayTime == 0)
            {
                _owner.CreateActions(nextActions, position, entityId);
            }
            else
            {
                uint timerId = 0;
                timerId = TimerHeap.AddTimer(delayTime, 0, () =>
                    {
                        _owner.CreateActions(nextActions, position, entityId);
                        TimerHeap.DelTimer(timerId);
                        _timerIdList.Remove(timerId);
                    });
                _timerIdList.Add(timerId);
            }
        }
    }
}
