﻿using Common.ServerConfig;
using Common.Utils;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class InspectingInfo
    {
        public int actionId;
        public string time;
    }

    public class SpaceActionInspectingManager
    {
        private List<InspectingInfo> _createCache;
        private List<InspectingInfo> _triggerCache;
        private List<InspectingInfo> _destroyCache;

        public SpaceActionInspectingManager()
        {
            _createCache = new List<InspectingInfo>();
            _triggerCache = new List<InspectingInfo>();
            _destroyCache = new List<InspectingInfo>();
        }

        public void Exit()
        {
            if (_createCache != null)
            {
                _createCache.Clear();
                _createCache = null;
            }
            if (_triggerCache != null)
            {
                _triggerCache.Clear();
                _triggerCache = null;
            }
            if (_destroyCache != null)
            {
                _destroyCache.Clear();
                _destroyCache = null;
            }
        }

        public void AddInspectingCache(int type, int actionId)
        {
            switch (type)
            {
                case public_config.SPACE_ACTION_EVENT_DEBUG_CREATE:
                    AddInspectingCache(actionId, _createCache);
                    break;
                case public_config.SPACE_ACTION_EVENT_DEBUG_DESTROY:
                    AddInspectingCache(actionId, _destroyCache);
                    break;
                case public_config.SPACE_ACTION_EVENT_DEBUG_TRIGGER:
                    AddInspectingCache(actionId, _triggerCache);
                    break;
            }
        }

        private void AddInspectingCache(int actionId, List<InspectingInfo> cache)
        {
            InspectingInfo info = new InspectingInfo();
            info.actionId = actionId;
            info.time = MogoTimeUtil.ToLocalTime((long)(Common.Global.Global.serverTimeStampSecond));
            cache.Add(info);
        }

        public List<InspectingInfo> GetInspectingCache(int type)
        {
            List<InspectingInfo> cache = null;
            switch (type)
            {
                case public_config.SPACE_ACTION_EVENT_DEBUG_CREATE:
                    cache = _createCache;
                    break;
                case public_config.SPACE_ACTION_EVENT_DEBUG_DESTROY:
                    cache = _destroyCache;
                    break;
                case public_config.SPACE_ACTION_EVENT_DEBUG_TRIGGER:
                    cache = _triggerCache;
                    break;
            }
            return cache;
        }
    }
}
