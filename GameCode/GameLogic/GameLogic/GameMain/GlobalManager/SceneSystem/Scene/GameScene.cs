﻿using Common;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using FindMovePath;
using GameData;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using SpaceSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class GameScene : BaseScene
    {
        protected SpaceActionManager _actionManager;
        protected ElevatorManager _elevatorManager;
        private BlockMap _blockMap;
        public static string ACTION_ENTITIES = "ActionEntities";
        private const string DATA_BLOCK_MAPS = "data/block_maps/s";
        private Dictionary<int, EntityCampData> _campData;
        private Dictionary<int, string> _preloadDummyInfo;
        private Dictionary<int, int> _ishasPreload;
        private List<MovePathData> _findpathPoint;
        private int _preloadDummyIndex = 0;
        protected Vector3 _autoMovePos;
        protected bool _isClientScene = false;
        protected bool _isServerMonster = false;
        private List<int> _qualitySettingList;

        public ElevatorManager elevatorManager
        {
            get { return _elevatorManager; }
        }

        public override void Enter()
        {
            _isUpdateProgress = true;
            _instanceId = map_helper.GetInstanceIDByMapID(_mapId);
            _isClientScene = map_helper.IsClientCtrl(_mapId);
            InitQualitySettingList();
            EventDispatcher.TriggerEvent<int>(SceneEvents.ENTER_SCENE, _mapId);
            base.Enter();
        }

        public override void Exit()
        {
            if (_qualitySettingList != null)
            {
                _qualitySettingList.Clear();
                _qualitySettingList = null;
            }
            StopAutoFight();
            ExitAction();
            DeleteElevator();
            UIManager.Instance.ResetAllLayer();
            EventDispatcher.TriggerEvent<int>(SceneEvents.LEAVE_SCENE, _mapId);
            base.Exit();
            ClearCampData();
            ClearPreloadDummyInfo();
            //ClearSceneDummyPosInfo();
        }

        public override string GetSpaceName()
        {
            var mapDataDic = GameData.XMLManager.map;
            if (!mapDataDic.ContainsKey(_mapId))
            {
                LoggerHelper.Error("GameData.XMLManager.map.ContainsKey - error by key :" + _mapId);
                return null;
            }
            var mapData = mapDataDic[_mapId];
            return mapData.__space_name;
        }

        public override bool IsClientScene()
        {
            return _isClientScene;
        }

        protected override void OnLoadGameSceneStart()
        {
            base.OnLoadGameSceneStart();
            _inLoading = true;
            if (_enterPoint == Vector3.zero)
            {
                _enterPoint = _spaceData.globalParams.GetEnterPoint();
            }
            ServerProxy.Instance.pauseHandleData = true;
            LoadBgMusic();
            PlayerAutoFightManager.GetInstance().OnLoadSceneStart();
        }

        protected override void OnLoadGameSceneFinish()
        {
            base.OnLoadGameSceneFinish();
            ResetCamera();
            InitPreloadInfo();
            InitFindPathInfo();
            EnterAction();
            InitCampData(); 
            InitElavator();
            SetState();
            ResetBlockMap();
            ResetBlockTransparentObjects();
            if (_isClientScene)
            {
                _isServerMonster = false;
                PlayerAvatar.Player.map_camp_id = 1;//单人客户端本默认值为1
                if (PlayerAvatar.Player.GetPet() != null) PlayerAvatar.Player.GetPet().map_camp_id = 1;
                EntityCampData data = _campData[1];
                PlayerAvatar.Player.camp_pve_type = (byte)data.camp_pve_type_i;
                PlayerAvatar.Player.camp_pvp_type = (byte)data.camp_pvp_type_i;
                PlayerAvatar.Player.canBeAttached = false;
                EntityAI.aiState = true;
            }
            else
            {
                SendToServerOnLoadGameSceneFinish();
                _isServerMonster = true;
            }
            MogoEngine.RPC.ServerProxy.Instance.pauseHandleData = false;
            EventDispatcher.TriggerEvent<int>(SceneEvents.ENTER_MAP, _mapId);
            PlayerAutoFightManager.GetInstance().OnLoadSceneFinish();
        }

        private void SendToServerOnLoadGameSceneFinish()
        {
            if (inst_type_operate_helper.IsNeedStartTime((ChapterType)map_helper.GetMapType(_mapId)))
            {
                LuaTable table = new LuaTable();
                PlayerAvatar.Player.RpcCall("mission_action_req", action_config.ACTION_PLAYER_PREPARED, table);
            }
        }

        public override void CreateActions(string actionId, Vector3 postion, uint entityId = 0)
        {
            if (_actionManager == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(actionId))
            {
                return;
            }
            string[] action = actionId.Split(',');
            for (int i = 0; i < action.Length; i++)
            {
                _actionManager.CreateAction(int.Parse(action[i]), postion, entityId);
            }
        }

        public void TerminateActions(int actionId)
        {
            if (_actionManager == null)
            {
                return;
            }
            _actionManager.TerminateActions(actionId);
        }

		public void TerminateAllActions()
		{
			if (_actionManager == null)
			{
				return;
			}
			_actionManager.TerminateAllActions();
		}

        virtual public void OnCreateAction(int id, int actionId)
        {
        }

        public void AddInspectingCache(int type, int actionId)
        {
            if (_actionManager != null)
            {
                _actionManager.AddInspectingCache(type, actionId);
            }
        }

        private void EnterAction()
        {
            _actionManager = new SpaceActionManager();
            _actionManager.Enter(this);
        }

        private void ExitAction()
        {
            if (_actionManager != null)
            {
                _actionManager.Exit();
                _actionManager = null;
            }
        }
        private void ClearCampData()
        {
            if (_campData != null)
            {
                _campData.Clear();
                _campData = null;
            }
        }

        private void ClearSceneDummyPosInfo()
        {
            GameSceneManager.GetInstance().ClearSceneDummyPosInfo();
        }

        private void ClearPreloadDummyInfo()
        {
            if (_findpathPoint != null)
            {
                _findpathPoint.Clear();
                _findpathPoint = null;
            }
            
            _preloadDummyIndex = 0;
            if (_ishasPreload != null)
            {
                _ishasPreload.Clear();
                _ishasPreload = null;
            }
            if (_preloadDummyInfo != null)
            {
                _preloadDummyInfo.Clear();
                _preloadDummyInfo = null;
            }
        }

        private void DeleteElevator()
        {
            if (_elevatorManager != null)
            {
                _elevatorManager.Exit();
                _elevatorManager = null;
            }
        }

        private void StopAutoFight()
        {
            if (PlayerAutoFightManager.GetInstance().fightType != FightType.FOLLOW_FIGHT)
            {
                PlayerAutoFightManager.GetInstance().Stop();
            }
        }

        private bool IsMultipleCombatInstance()
        { 
            if (instance_helper.GetChapterType(_instanceId) != ChapterType.MainTown)
            {
                var teamType = instance_helper.GetTeamType(_instanceId);
                if (teamType == InstanceTeamType.Team || teamType == InstanceTeamType.MultiRandom)
                {
                    return true;
                }                
            }
            return false;
        }

        private void ResetCamera()
        {
            var baseSettings = _spaceData.baseSettings;
            try
            {
                var currentCamera = CameraManager.GetInstance().mainCamera.GetComponent<Camera>();
                if (currentCamera != null && !string.IsNullOrEmpty(baseSettings.camera_near_far_l))
                {
                    var cnfl = baseSettings.camera_near_far_l.Split(',');
                    currentCamera.nearClipPlane = float.Parse(cnfl[0]);
                    currentCamera.farClipPlane = float.Parse(cnfl[1]);
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        QualitySettings.lodBias = IsMultipleCombatInstance() ? 0.4f : 0.7f;
                        currentCamera.farClipPlane = IsMultipleCombatInstance() ? 60f : float.Parse(cnfl[1]);
                    }
                }
            }
            catch
            {
                Debug.LogError("ResetCamera Error!");
            }
        }

        private void SetState()
        {
            GameStateManager.GetInstance().SetState(GameDefine.STATE_SCENE);
        }        
        
        private void ResetBlockTransparentObjects()
        {
            GameObject[] sceneObjs = SceneMgr.Instance.sceneGameObjects;
            for (int i = 0; i < sceneObjs.Length; ++i)
            {
                var sceneGameObject = sceneObjs[i];
                var transfrom = sceneGameObject.transform.FindChild("BlockTransparents");
                if (transfrom != null)
                {
                    var meshRenderers = transfrom.GetComponentsInChildren<MeshRenderer>();
                    for (int j = 0; j < meshRenderers.Length; j++)
                    {
                        var gameObject = meshRenderers[j].gameObject;
                        gameObject.AddComponent<BlockTransparent>();
                    }
                }
            }
        }

        private void ResetBlockMap()
        {
            _blockMap = LoadBlockMap();
        }

        private BlockMap LoadBlockMap()
        {
            BlockMap blockMap = null;
            string sceneName = _spaceData.globalParams.mapname;
            try
            {
                byte[] byteContent = null;
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                    case RuntimePlatform.IPhonePlayer:
                    case RuntimePlatform.WindowsWebPlayer:
                        byteContent = LoadBlockMapAtAndorid("发布模式", sceneName);
                        break;
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.WindowsPlayer:
                    case RuntimePlatform.OSXEditor:
                        if (SystemConfig.ReleaseMode)
                        {
                            byteContent = LoadBlockMapAtAndorid("开发模式下--发布模式", sceneName);
                        }
                        else
                        {
                            string path = string.Concat(SystemConfig.ResPath, DATA_BLOCK_MAPS, sceneName, ConstString.BM_SUFFIX);
                            //LoggerHelper.Info(string.Format("[开发模式][LoadBlockMap] 加载文件:{0},sceneName:{1}", path, sceneName));
                            byteContent = path.LoadByteFile();
                        }
                        break;
                }
                /*if (GameLoader.Config.SystemConfig.ReleaseMode)
                {
                    string path = "data/block_maps/s" + sceneName;
                    var stream = GameLoader.IO.ResLoader.LoadStreamFromResource(path);
                    byteContent = new byte[stream.Length];
                    stream.Read(byteContent, 0, (int)stream.Length);
                }
                else
                {
                    string path = string.Concat(GameLoader.Config.SystemConfig.ResPath, "data/block_maps/s", sceneName, ".bm");
                    byteContent = path.LoadByteFile();
                }*/
                blockMap = new BlockMap(byteContent);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
            return blockMap;
        }

        /// <summary>
        /// 发布模式下--加载BlockMap
        /// </summary>
        /// <param name="logTitle">日志标头</param>
        /// <param name="sceneName">场景名</param>
        /// <returns></returns>
        private byte[] LoadBlockMapAtAndorid(string logTitle, string sceneName)
        {
            string fileName = string.Concat(DATA_BLOCK_MAPS, sceneName, ConstString.BM_SUFFIX);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            if (!isExistFile) LoggerHelper.Info(string.Format("[{0}][LoadBlockMap] 文件:{1},{2}", logTitle, fileName, isExistFile ? "存在" : "找不到"));
            return isExistFile ? FileAccessManager.LoadBytes(fileName) : null;
        }

        private void InitCampData()
        {
            _campData = new Dictionary<int, EntityCampData>();
            var listData = _spaceData.entities.entityList;
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].type == EntityType.CAMP_ACTION)
                {
                    var campData = listData[i] as EntityCampData;
                    SetCampInfo(campData.camp_id_i, campData);
                }
            }
        }

        private void InitFindPathInfo()
        {
            _findpathPoint = new List<MovePathData>();
            
            var _scene = GameSceneManager.GetInstance().scene;
            if (_scene is MainScene) return;
            var listData = _spaceData.entities.entityList;
            for (int i = 0; i < listData.Count; i++)
            {
				var movepathData = new MovePathData();
                if (listData[i].type == EntityType.AI_MOVE_ACTION)
                {
                    float x = (float)(listData[i].pos_x_i / 100);
                    float y = (float)(listData[i].pos_y_i / 100);
                    float z = (float)(listData[i].pos_z_i / 100);
                    Vector3 pos = new Vector3(x,y,z);
                    movepathData.id = listData[i].id_i;
                    movepathData.pathPos = pos;
                    movepathData.relationId = listData[i].next_actions_l;
                    _findpathPoint.Add(movepathData);
                }
            }
        }

        public List<MovePathData> GetFindPathInfo()
        {
            return _findpathPoint;
        }

        private void InitDummyInfo()
        {
            var _scene = GameSceneManager.GetInstance().scene;
            if (_scene is MainScene) return;
            var listData = _spaceData.entities.entityList;
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].type == EntityType.SPAWN_MONSTER_ACTION)
                {
                    var spanMonsterData = listData[i] as EntitySpawnMonsterActionData;
                    GameSceneManager.GetInstance().SetSceneDummyPosInfo(spanMonsterData);
                }
            }
        }

        private void InitPreloadInfo()
        {
            _ishasPreload = new Dictionary<int, int>();
            var _scene = GameSceneManager.GetInstance().scene;
            if (_scene is MainScene) return;
            _preloadDummyInfo = new Dictionary<int, string>();
            var listData = _spaceData.entities.entityList;
            for (int i = 0; i < listData.Count; i++)
            {
                if (string.IsNullOrEmpty(listData[i].next_actions_l)) continue;
                if (listData[i].type == EntityType.SET_BEGIN_STATE_ACTION)
                {
                    string[] str = listData[i].next_actions_l.Split(',');
                    for (int j = 0; j < str.Length; j++)
                    {
                        if (System.Convert.ToInt32(str[j]) > 5000 && System.Convert.ToInt32(str[j]) < 6000)
                        {
                            _preloadDummyIndex++;
                            var dummyData = GameSceneManager.GetInstance().CurrActionEntityData(System.Convert.ToInt32(str[j])) as EntitySpawnMonsterActionData;
                            _preloadDummyInfo.Add(_preloadDummyIndex, dummyData.monster_ids_l);
                        }
                        if (_ishasPreload.ContainsKey(System.Convert.ToInt32(str[j]))) continue;
                        CheckPreloadDummyInfo(System.Convert.ToInt32(str[j]));
                    }
                    break;
                }
                /*string[] str = listData[i].next_actions_l.Split(',');
                //Debug.LogError("next_actions_l:" + listData[i].next_actions_l);
                for (int j = 0; j < str.Length; j++)
                {
                    if (System.Convert.ToInt32(str[j]) > 5000 && System.Convert.ToInt32(str[j]) < 6000)
                    {
                        var data = GameSceneManager.GetInstance().CurrActionEntityData(System.Convert.ToInt32(str[j])) as EntitySpawnMonsterActionData;
                        //Debug.LogError(listData[i].id_i + ":" + data.monster_ids_l);
                    }
                }*/
            }
        }

        private void CheckPreloadDummyInfo(int id)
        {
            _ishasPreload.Add(id, id);
            var data = GameSceneManager.GetInstance().CurrActionEntityData(id);
            if (data == null) return;
            if (string.IsNullOrEmpty(data.next_actions_l)) return;
            string[] str = data.next_actions_l.Split(',');
            for (int j = 0; j < str.Length; j++)
            {
                if (System.Convert.ToInt32(str[j]) > 5000 && System.Convert.ToInt32(str[j]) < 6000)
                {
                    _preloadDummyIndex++;
                    var dummyData = GameSceneManager.GetInstance().CurrActionEntityData(System.Convert.ToInt32(str[j])) as EntitySpawnMonsterActionData;
                    _preloadDummyInfo.Add(_preloadDummyIndex, dummyData.monster_ids_l);
                }
                if (_ishasPreload.ContainsKey(System.Convert.ToInt32(str[j]))) continue;
                CheckPreloadDummyInfo(System.Convert.ToInt32(str[j]));
            }
            
        }

        private void InitElavator()
        {
            _elevatorManager = new ElevatorManager();
            _elevatorManager.Enter();
        }

        private void SetCampInfo(int key, EntityCampData campData)
        {
            if (_campData.ContainsKey(key))
            {
                _campData.Remove(key);
            }
            _campData.Add(key, campData);
        }

        public Dictionary<int, string> GetPreloadDummyInfo()
        {
            return _preloadDummyInfo;
        }

        //A*寻路
        public List<float> GetPath(float x1, float z1, float x2, float z2, bool bFly = false)
        {
            if (_blockMap != null)
            {
                var pathList = new List<float>();
                //LoggerHelper.Error("111");
                var retrunPathList = _blockMap.findpath((int)x1, (int)z1, (int)x2, (int)z2, bFly);
                //LoggerHelper.Error("222");
                if (retrunPathList != null)
                {
                    for (int i = 0; i < retrunPathList.Count; i++)
                    {
                        pathList.Add(retrunPathList[i]);
                    }
                }
                return pathList;
            }
            return null;
        }

        public bool CheckCurrPointIsCanMove(float posx, float posz, bool bFly = false)
        {
            if (_blockMap == null) return true;
            return _blockMap.IsWay(posx, posz, bFly);
        }

        public Vector3 CheckPointAroundCanMove(float posx, float posz, bool bFly = false, bool largeRangeSearch = false)
        {
            if (_blockMap == null) return Vector3.zero;
            return _blockMap.CheckPointAroundCanMove(posx, posz, bFly, largeRangeSearch);
        }

        public byte[,] GetBlockMapData()
        {
            if (_blockMap != null)
            {
                return _blockMap.GetBlockMapData();
            }
            return null;
        }

        public EntityCampData GetCampInfo(int key)
        {
            if (_campData != null && _campData.ContainsKey(key))
            {
                return _campData[key];
            }
            return null;
        }

        public void SetCurrAutoMovePoint(string point)
        {
            if (point == null || point == "")
            {
                LoggerHelper.Error("未配置路点信息");
                return;
            }
            string[] pointStr = point.Split(',');
            float x = float.Parse(pointStr[0]);
            float y = float.Parse(pointStr[1]);
            float z = float.Parse(pointStr[2]);
            _autoMovePos = new Vector3(x, y, z);
        }

        public Vector3 GetAutoMovePoint()
        {
            return _autoMovePos;
        }

        public bool IsServerMonster()
        {
            return _isServerMonster;
        }

        private void LoadBgMusic()
        {
            if (PlayerAvatar.Player.actor != null)
            {
                TimerHeap.AddTimer(2000, 0, () =>
                {
                    BgMusicManager.Instance.PlayMapBgMusic(_mapId);
                    //LoggerHelper.Error("1_________________mapId:    " + _mapId + ",Time:    " + Time.realtimeSinceStartup);
                    SoundPool.GetInstance().Reset();
                });
            }
            else
            {
                TimerHeap.AddTimer(100, 0, () =>
                {
                    LoadBgMusic();
                });
            }
        }

        private void InitQualitySettingList()
        {
            if (_qualitySettingList == null)
            {
                _qualitySettingList = data_parse_helper.ParseListInt(map_helper.GetQualitySettings(_mapId));
                if (_qualitySettingList.Count == 0)
                {
                    //非单人本默认质量降一档
                    if (instance_helper.GetTeamTypeByMapId(_mapId) > 1)
                    {
                        _qualitySettingList.Add(0);
                        _qualitySettingList.Add(-1);
                        _qualitySettingList.Add(-1);
                    }
                }
            }
        }

        public int GetQualitySetting(int type)
        {
            InitQualitySettingList();
            if (_qualitySettingList == null || type < 0 || type >= _qualitySettingList.Count)
            {
                return 0;
            }
            return _qualitySettingList[type];
        }
    }
}
