﻿using Common.States;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using MogoEngine.Mgrs;
using SpaceSystem;
using System;
using System.Collections.Generic;
using System.Security;
using UnityEngine;
using GameLoader.LoadingBar;
using Game.Asset;
using ACTSystem;
using MogoEngine.RPC;
using GameData;
using Common.ServerConfig;
using GameLoader.Utils.Timer;
using Common.Base;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.GlobalManager
{
    public class BaseScene : IScene
    {
        private const string DATA_SPACES = "data/spaces/";

        private static int _lastMapId = 0;
        private static SpaceData _lastSpaceData = null;

        private bool _inTheSameMapName = false;
        protected int _mapId = 0;
        protected int _instanceId = 0;
        protected string _spaceName;
        protected SpaceData _spaceData;  
        private Action _loadCallback;
        protected Vector3 _enterPoint = Vector3.zero;
        protected bool _inLoading = false;
        protected bool _isUpdateProgress = true;
        protected bool _isCombinedMesh = true;
        protected int _sceneTime = SceneTimeType.NONE;

        protected static bool _firstShowLoading = true;

        virtual public void Enter()
        {
            InitSceneTime();
            LoadSpaceData();
            LoadScene();
        }

        public void Enter(Action loadCallback)
        {
            _loadCallback = loadCallback;
            Enter();
        }

        public void Enter(int mapId, Action loadCallback = null)
        {
            _mapId = mapId;
            _loadCallback = loadCallback;
            Enter();
        }

        public void Enter(int mapId, Vector3 enterPos, Action loadCallback = null)
        {
            _mapId = mapId;
            _enterPoint = enterPos;
            _loadCallback = loadCallback;
            Enter();
        }

        virtual public void Exit()
        {
            Main.Instance.curACTSystemDriver.ObjectPool.UnloadUnusedAssets();
            SceneMgr.Instance.LeaveScene();
            DelTimer();
            _lastMapId = _mapId;
            _lastSpaceData = _spaceData;
            _spaceData = null;
            _loadCallback = null;
        }

        public int GetMapID()
        {
            return _mapId;
        }

        public int GetInstanceID()
        {
            return _instanceId;
        }

        public bool IsInLoading()
        {
            return _inLoading;
        }

        virtual public bool IsClientScene()
        {
            return false;
        }

        public Vector3 GetEnterPoint()
        {
            return _enterPoint;
        }

        virtual public string GetSpaceName()
        {
            return null;
        }

        public SpaceData GetSpaceData()
        {
            return _spaceData;
        }

        public EntityData GetActionData(int actionId)
        {
            if (!_spaceData.entities.entityDic.ContainsKey(actionId))
            {
                LoggerHelper.Error(string.Format("获取行为失败，space名为{0}不包含id为{1}的行为。", _spaceData.globalParams.spacename, actionId));
                return null;
            }
            return _spaceData.entities.entityDic[actionId] as EntityData;
        }

        virtual public void CreateActions(string actionId, Vector3 postion, uint entityId = 0)
        {
        }

        /// <summary>
        /// 加载spaces文件
        /// </summary>
        /// <param name="spaceName"></param>
        /// <returns></returns>
        virtual protected void LoadSpaceData()
        {
            SecurityElement xmlDoc = null;

            GameSceneManager.GetInstance().SyncEnterPos();
            _spaceName = GetSpaceName();
            string strContent = string.Empty;
            //LoggerHelper.Info(string.Format("[LoadSpaceData] sceneId:{0},_spaceName:{1}", _mapId, _spaceName));
            xmlDoc = GameSceneManager.GetInstance().GetCacheSpaceData(_spaceName);
            if (xmlDoc == null)
            {
                //Debug.LogError("xmlDoc is null");
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                    case RuntimePlatform.IPhonePlayer:
                    case RuntimePlatform.WindowsWebPlayer:
                        strContent = LoadSpaceDataAtAndorid("发布模式", _spaceName);
                        break;
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.WindowsPlayer:
                        if (SystemConfig.ReleaseMode)
                        {
                            strContent = LoadSpaceDataAtAndorid("开发模式下--发布模式", _spaceName);
                        }
                        else
                        {
                            string path = string.Concat(SystemConfig.ResPath, DATA_SPACES, _spaceName, ConstString.XML_SUFFIX);
                            LoggerHelper.Info(string.Format("[开发模式][LoadSpaceData] 加载:{0},spaceName:{1}", path, _spaceName));
                            strContent = path.LoadFile();
                        }
                        break;
                }
                xmlDoc = XMLParser.LoadXML(strContent);
                if (xmlDoc == null) LoggerHelper.Error("load space error:" + _spaceName);
                GameSceneManager.GetInstance().CacheSpaceData(_spaceName, xmlDoc);
            }
            _spaceData = CreateEmptySpaceData();
            //LoggerHelper.Info(string.Format("[LoadSpaceData] _spaceData:{0}", _spaceData));
            _spaceData.Load(xmlDoc);
            ResetLoadingAmbientLight(_spaceData);
            if (_isUpdateProgress)
            {
                _inTheSameMapName = InTheSameMapName(_spaceData.globalParams.mapname);
                if (_inTheSameMapName)
                {
                    ShowCloudLoading();
                }
                else
                {
                    ShowProgressBar();
                }
                if (_firstShowLoading && _mapId != 0)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.CloudPanel);
                    //UIManager.Instance.ShowPanel(PanelIdEnum.SceneChange);
                }
            }
        }

        bool IsWildOrCity(int mapID)
        {
            if (mapID != 0)
            {
                int mapType = map_helper.GetMapType(mapID);
                if (mapType == public_config.MAP_TYPE_WILD || mapType == public_config.MAP_TYPE_CITY)
                {
                    return true;
                }
            }
            return false;
        }

        bool InTheSameMapName(string newMapName)
        {
            return _lastSpaceData != null && (_lastSpaceData.globalParams.mapname == newMapName);
        }

        void InitSceneTime()
        {
            _sceneTime = SceneTimeType.NONE;
            if (DateTime.Now.Hour >= 6 && DateTime.Now.Hour < 18)
            {
                _sceneTime = SceneTimeType.DAY;
            }
            else {
                _sceneTime = SceneTimeType.NIGHT;
            }
        }

        LoadSceneSetting GenerateLoadSceneSetting()
        {
            LoadSceneSetting loadSceneSetting = new LoadSceneSetting();
            loadSceneSetting.clearSceneMemory = true;
            loadSceneSetting.destroyOldSceneObject = true;
            loadSceneSetting.canCache = false;
            loadSceneSetting.updateProgress = _isUpdateProgress;
            string newMapName = _spaceData.globalParams.mapname;
            if (_inTheSameMapName)
            {
                loadSceneSetting.destroyOldSceneObject = false;
                loadSceneSetting.clearSceneMemory = false;
                loadSceneSetting.updateProgress = false;
            }
            if (IsWildOrCity(_mapId))
            {
                loadSceneSetting.canCache = true;
            }
            //上一個場景是主城或者野外，或者第一登陸遊戲場景。
            if (IsWildOrCity(_lastMapId) || (_lastMapId == 0 && _mapId != 0))
            {
                loadSceneSetting.clearSceneMemory = false;
            }
            loadSceneSetting.sceneName = _spaceData.globalParams.mapname;
            var pdata = new Dictionary<string, bool>();
            pdata.Add(_spaceData.MapPath, _isCombinedMesh && GMState.staticBatching);
            loadSceneSetting.prefabsData = pdata;
            loadSceneSetting.sceneTime = _sceneTime;
            loadSceneSetting.lightmapPaths = _spaceData.LightmapPaths;
            loadSceneSetting.lightProbesName = string.Empty;
            return loadSceneSetting;
        }

        

        virtual protected void LoadScene()
        {
            OnLoadGameSceneStart();
            LoadSceneSetting loadSceneSetting = GenerateLoadSceneSetting();
            GameObjectPoolManager.GetInstance().Clear();
            if (loadSceneSetting.clearSceneMemory) ACTVisualFXManager.GetInstance().ReleaseACTVisualFx();
            SceneMgr.Instance.LoadScene(loadSceneSetting, () =>
            {
                ActorDeathController.DestroyAllDeadBody();
                if (_isUpdateProgress) ProgressBar.Instance.UpdateProgress(0.82f, false);
                PreloadManager.GetInstance().BeforeEnterScene(() =>
                {
                    OnLoadGameSceneFinish();
                    if (_loadCallback != null) _loadCallback();
                });
            });
            GameSceneManager.GetInstance().mapPath = _spaceData.MapPath;
        }

        virtual protected void OnLoadGameSceneStart()
        {
            _inLoading = true;
            BgMusicManager.Instance.StopAllBgMusic();
            SoundRecoverManager.instance.ReleaseAll();
        }

        void ResetFog()
        {
            GameObject go = GameObject.Find("Fog_FarPlane");
            if (go != null)
            {
                FarFogPlane com = go.GetComponent<FarFogPlane>();
                Dictionary<int,Color[]> farFogColorSetting =  _spaceData.baseSettings.FarFogColorSetting;
                Dictionary<int,int[]> farFogDistanceSetting =  _spaceData.baseSettings.FarFogDistanceSetting;
                if (farFogColorSetting.ContainsKey(_sceneTime))
                {
                    com.fogColor1 = farFogColorSetting[_sceneTime][0];
                    com.fogColor2 = farFogColorSetting[_sceneTime][1];
                    com.fogStart = (float)farFogDistanceSetting[_sceneTime][0];
                    com.fogEnd = (float)farFogDistanceSetting[_sceneTime][1];
                }
                com.ResetFog();
            } 
        }

        void ResetSky()
        {
            if (SceneMgr.Instance.sceneGameObjects.Length == 0) return;
            GameObject sceneGameObject = SceneMgr.Instance.sceneGameObjects[0];
            if (sceneGameObject == null) return;
            Transform dayTrans = null;
            Transform nightTrans = null;
            if (_sceneTime != SceneTimeType.NONE) {
                dayTrans = sceneGameObject.transform.Find("Day");
                nightTrans = sceneGameObject.transform.Find("Night");
                if (dayTrans != null) dayTrans.gameObject.SetActive(false);
                if (nightTrans != null) nightTrans.gameObject.SetActive(false);
            }
            Transform timeTrans = null;
            if (_sceneTime == SceneTimeType.DAY) timeTrans = dayTrans;
            else if (_sceneTime == SceneTimeType.NIGHT) timeTrans = nightTrans;
            
            Transform skyTrans = null;
            if (timeTrans != null) {
                skyTrans = timeTrans.Find("Background");
                timeTrans.gameObject.SetActive(true);
            }
            if (skyTrans == null) skyTrans = sceneGameObject.transform.Find("Sky");
            var mainCamera = CameraManager.GetInstance().mainCamera;
            if (mainCamera != null) mainCamera.ResetSky(skyTrans);
        }

        virtual protected void OnLoadGameSceneFinish()
        {
            _inLoading = false;
            if (IsWildOrCity(_mapId))
            {
                ResetFog();
            }
            //ResetSky();
            if (_isUpdateProgress && !_inTheSameMapName)
            {
                ProgressBar.Instance.UpdateProgress(1f, false);
            }
            ResetRenderSetting(_spaceData);
            DelTimer();
            _timerId = TimerHeap.AddTimer(300, 0, OnTimeStep);
            if (_firstShowLoading)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.CloudPanel);
                //UIManager.Instance.ClosePanel(PanelIdEnum.SceneChange);
            }
            _firstShowLoading = false;
        }

        private uint _timerId;
        private void OnTimeStep()
        {
            DelTimer();
            if (_inTheSameMapName)
            {
                HideCloudLoading();
            }
            else
            {
                HideProgressBar();
            }
        }

        private void DelTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void ShowCloudLoading()
        {
            //TODO: 显示云界面
            //UIManager.Instance.ShowPanel(PanelIdEnum.SceneChange);
        }

        private void ShowProgressBar()
        {
            ProgressBar.Instance.Show();
            //ProgressBar.Instance.UpdateTip(string.Format("正在加载场景:{0}", _mapId));
            LoadingContentManager.Instance.ShowLoadingContent();
            ProgressBar.Instance.UpdateProgress(0.1f, false);
        }

        private void HideCloudLoading()
        {
            //TODO: 隐藏云界面
            //EventDispatcher.TriggerEvent(CloudEvents.CloseSceneChangePanel);
        }

        private void HideProgressBar()
        {
            if (PlayerDataManager.Instance.serverDataInited)
            {
                ProgressBar.Instance.Close();
            }
            LoadingContentManager.Instance.CloseLoadingContent();
        }

        /// <summary>
        /// 发布模式下加载SpaceData
        /// </summary>
        /// <param name="logTitle">日志标头</param>
        /// <param name="spaceName">加载SpaceData的不带后缀名的文件名</param>
        /// <returns></returns>
        private string LoadSpaceDataAtAndorid(string logTitle, string spaceName)
        {
            string fileName = string.Concat(DATA_SPACES, spaceName, ConstString.XML_SUFFIX);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            //if (!isExistFile) LoggerHelper.Info(string.Format("[{0}][LoadSpaceData] 文件:{1},{2}", logTitle, fileName, isExistFile ? "存在" : "找不到"));
            return isExistFile ? FileAccessManager.LoadText(fileName) : null;
            //string path = string.Concat(DATA_SPACES, spaceName);
            //strContent = ResLoader.LoadStringFromResource(path);
        }

        private SpaceData CreateEmptySpaceData()
        {
            return new SpaceData();
        }

        private void ResetLoadingAmbientLight(SpaceData spaceData)
        {
            /*if (spaceData == null || spaceData.baseSettings == null)
            {
                LoggerHelper.Warning("spaceData is null");
            }*/
            var baseSettings = spaceData.baseSettings;
            LoggerHelper.Debug(string.Format("baseSettings:{0}", baseSettings));
            var al = baseSettings.ambient_light_l.Split(',');
            SceneMgr.defaultAmbientLight = new Color(float.Parse(al[0]), float.Parse(al[1]), float.Parse(al[2]));
        }

        private void ResetRenderSetting(SpaceData spaceData)
        {
            var baseSettings = spaceData.baseSettings;
            RenderSettings.fog = baseSettings.is_fog_i == 1 ? true : false;
            var cl = baseSettings.fog_color_l.Split(',');
            RenderSettings.fogColor = new Color(float.Parse(cl[0]), float.Parse(cl[1]), float.Parse(cl[2]));
            RenderSettings.fogMode = (FogMode)baseSettings.fog_mode_i;
            RenderSettings.fogStartDistance = baseSettings.fog_start_distance_f;
            RenderSettings.fogEndDistance = baseSettings.fog_end_distance_f;
            var al = baseSettings.ambient_light_l.Split(',');
            RenderSettings.ambientLight = new Color(float.Parse(al[0]), float.Parse(al[1]), float.Parse(al[2]));
        }
    }
}
