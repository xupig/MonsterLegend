﻿
namespace GameMain.GlobalManager
{
    public class LoginScene : BaseScene
    {
        public override void Enter()
        {
            _isCombinedMesh = false;
            _isUpdateProgress = !Main.Instance.isUpdateProgress;
            base.Enter();
        }

        public override string GetSpaceName()
        {
            return "sLoginFlyingBoat";
        }
    }
}
