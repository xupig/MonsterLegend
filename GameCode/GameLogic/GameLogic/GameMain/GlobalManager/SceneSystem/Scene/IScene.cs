﻿using SpaceSystem;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public interface IScene
    {
        void Enter();
        void Enter(Action loadCallback);
        void Enter(int mapId, Action loadCallback = null);
        void Enter(int mapId, Vector3 enterPos, Action loadCallback = null);
        void Exit();
        int GetMapID();
        int GetInstanceID();
        bool IsInLoading();
        bool IsClientScene();
        Vector3 GetEnterPoint();
        string GetSpaceName();
        SpaceData GetSpaceData();
        EntityData GetActionData(int actionId);
        void CreateActions(string actionId, Vector3 postion, uint entityId = 0);
    }
}
