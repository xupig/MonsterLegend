﻿using MogoEngine.Mgrs;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class CharacterScene : BaseScene
    {
        public override void Enter()
        {
            _isUpdateProgress = true;
            base.Enter();
        }

        public override string GetSpaceName()
        {
            return "sCharacter";
        }

        protected override void OnLoadGameSceneFinish()
        {
            if (SceneMgr.Instance.sceneGameObjects.Length > 0)
            {
                var camera = SceneMgr.Instance.sceneGameObjects[0].GetComponentInChildren<Camera>();
                if (camera != null)
                {
                    camera.gameObject.AddComponent<Bloom>();
                }
            }
            base.OnLoadGameSceneFinish();
        }
    }
}
