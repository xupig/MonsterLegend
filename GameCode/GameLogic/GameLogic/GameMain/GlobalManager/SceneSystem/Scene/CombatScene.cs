﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Game.Asset;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class CombatScene : GameScene
    {
        private ScoreRuleManager _scoleRuleManager;     //副本结算规则
        private ReliveManager _reliveManager;           //复活规则
        private bool _isCombatComplete = false;
        private uint _teamTaskEntityId = 0;

        public bool isCombatComplete { get { return _isCombatComplete; } }

        public override void Enter()
        {
            base.Enter();
        }

        public override void Exit()
        {
            _teamTaskEntityId = 0;
            EventDispatcher.RemoveEventListener<TaskData>(TaskEvent.CREATE_TEAM_TASK_ENTERANCE, OnCreatTeamTaskEntity);
            EventDispatcher.RemoveEventListener(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE, OnDestroyTaskEntity);
            //主角恢复同步属性
            PlayerAvatar.Player.canBeAttached = true;
            //副本退出时清理主角的伤害统计数据
            PlayerAvatar.Player.OnClearStatisticsDamage();
            EntityAI.aiState = true;
            if (_scoleRuleManager != null)
            {
                _scoleRuleManager.Exit();
                _scoleRuleManager = null;
            }
            if (_reliveManager != null)
            {
                _reliveManager.Exit();
                _reliveManager = null;
            }
            base.Exit();
        }

        protected override void OnLoadGameSceneFinish()
        {
            base.OnLoadGameSceneFinish();
            OnCombatStart();
        }

        public override void OnCreateAction(int id, int actionId)
        {
            if (_scoleRuleManager != null)
            {
                _scoleRuleManager.OnCreateAction(actionId);
            }
        }

        public void OnCombatStart()
        {
            EventDispatcher.AddEventListener<TaskData>(TaskEvent.CREATE_TEAM_TASK_ENTERANCE, OnCreatTeamTaskEntity);
            EventDispatcher.AddEventListener(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE, OnDestroyTaskEntity);
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            if (taskData.task_place_Id == GameSceneManager.GetInstance().curInstanceID)
            {
                CreatTeamTaskNpc(taskData.npcId, taskData.task_place_pos);
            }
            _scoleRuleManager = new ScoreRuleManager(this);
            _reliveManager = new ReliveManager(this);
            _scoleRuleManager.OnCombatStart();
            CheckNewbie();
        }

        private void OnCreatTeamTaskEntity(TaskData taskDta)
        {
            if (GameSceneManager.GetInstance().curInstanceID == taskDta.task_place_Id)
            {
                CreatTeamTaskNpc(taskDta.npcId, taskDta.task_place_pos);
            }
        }

        private void CreatTeamTaskNpc(int npcId, Vector3 pos)
        {
            if (_teamTaskEntityId != 0) return;
            MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_NAME_NPC, (entity) =>
            {
                var npc = entity as EntityNPC;
                _teamTaskEntityId = npc.id;
                npc.npc_id = npcId;
                entity.SetPosition(pos);
            });
        }

        private void OnDestroyTaskEntity()
        {
            MogoEngine.MogoWorld.DestroyEntity(_teamTaskEntityId);
            _teamTaskEntityId = 0;
        }

        public void OnCombatComplete(bool isWin = false)
        {
            LoggerHelper.Debug(string.Format("OnCombatComplete, result = {0}, mapId = {1}.", isWin, _mapId));
			if (isWin)
			{
				ScoreRuleData instanceData = GetBattleResultData();
				MissionManager.Instance.RequsetLeaveMission(instanceData);
			}
			else
			{
				MissionManager.Instance.RequestMissionFail();
			}
			_actionManager.TerminateAllActions();
            EntityAI.aiState = false;
            PlayerAvatar.Player.canBeAttached = true;
            _isCombatComplete = true;
            EventDispatcher.TriggerEvent(SpaceActionEvents.SpaceActionEvents_Complete);
        }

        public void RequestLeaveCombat()
        {
            MissionManager.Instance.RequsetQuitMisstion();
            PlayerAvatar.Player.aiID = -1;
            PlayerAvatar.Player.ChangeAI();
            _autoMovePos = Vector3.zero;
            
        }

        public ScoreRuleData GetBattleResultData()
        {
            if (_scoleRuleManager != null)
            {
                return _scoleRuleManager.GetBattleResultData();
            }
            return null;
        }

        private void CheckNewbie()
        {
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_EXPERIENCE_COPY)
            {
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.speed, 400);
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.base_speed, 400);
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.max_hp, 10000);
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, PlayerAvatar.Player.max_hp);
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.max_ep, 20000);
                MogoWorld.SynEntityAttr(PlayerAvatar.Player, EntityPropertyDefine.cur_ep, PlayerAvatar.Player.max_ep);
            }
        }

        public void SetKillMonsterCount(int monsterNum)
        {
            if (_scoleRuleManager != null)
            {
                _scoleRuleManager.SetKillMonsterCount(monsterNum);
            }
        }

        public void SetPlayerDeathCount()
        {
            if (_scoleRuleManager != null)
            {
                _scoleRuleManager.SetPlayerDeathCount();
            }
        }

        public void OnSpawnMonster(uint id, uint monsterId)
        {
            if (_scoleRuleManager != null)
            {
                _scoleRuleManager.OnSpawnMonster(id, monsterId);
            }
        }

        public void OnShowRelivePanel(UInt16 cd, int reliveRemainNum)
        {
            if (_reliveManager != null)
            {
                _reliveManager.OnShowRelivePanel(cd, reliveRemainNum);
            }
        }

        public void SetRelive()
        {
            if (_reliveManager != null)
            {
                _reliveManager.OnRelive();
            }
        }

        public void SetReliveType(int reliveType)
        {
            if (_reliveManager != null)
            {
                _reliveManager.SetReliveType(reliveType);
            }
        }
    }
}
