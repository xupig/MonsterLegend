﻿using Common.ServerConfig;

namespace GameMain.GlobalManager
{
    public enum SceneType
    {
        LOGIN_SCENE = 0,
        CHARACTER_SCENE,
        GAME_SCENE,
        MAIN_SCENE,
        COMBAT_SCENE
    }

    public class scene_helper
    {
        public static IScene CreateScene(SceneType sceneType)
        {
            IScene scene = null;
            switch (sceneType)
            {
                case SceneType.LOGIN_SCENE:
                    scene = Create<LoginScene>();
                    break;
                case SceneType.CHARACTER_SCENE:
                    scene = Create<CharacterScene>();
                    break;
                case SceneType.GAME_SCENE:
                    scene = Create<GameScene>();
                    break;
                case SceneType.MAIN_SCENE:
                    scene = Create<MainScene>();
                    break;
                case SceneType.COMBAT_SCENE:
                    scene = Create<CombatScene>();
                    break;
                default:
                    break;
            }
            return scene;
        }

        public static SceneType GetSceneType(int mapType)
        {
            SceneType sceneType = SceneType.GAME_SCENE;
            switch (mapType)
            {
                case public_config.MAP_TYPE_CITY:
                    sceneType = SceneType.MAIN_SCENE;
                    break;
                default:
                    sceneType = SceneType.COMBAT_SCENE;
                    break;
            }
            return sceneType;
        }

        public static T Create<T>() where T : IScene, new()
        {
            return new T();
        }
    }
}
