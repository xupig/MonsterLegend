﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.Events;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// IOS9视频录制管理类
    /// </summary>
    public class RePlayKitManager
    {
        private uint _timerId = 0;
        private int _refreshTime = 1000;                //刷新时间
        private int _recordingLastTime = 0;             //录制已持续时间，单位:秒
        private bool _recordStatus = false;             //当前录制状态[true:录制中,false:已停止]
        public static RePlayKitManager Instance = new RePlayKitManager();

        /// <summary>
        /// 视频录制状态[true:录制中,false:可开始录制]
        /// </summary>
        public bool isRecording
        {
            get { return _recordStatus; }
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener<string>("OnStartRecordOK", OnStartRecordOK);
            EventDispatcher.AddEventListener<string>("OnStopRecordOK", OnStopRecordOK);
        }

        //IOS返回开始录制结果
        private void OnStartRecordOK(string result)
        {
            LoggerHelper.Info("[RePlayKitManager-OnStartRecordOK()] result:" + result);
            if (!string.IsNullOrEmpty(result) && result.Equals("1"))
            {//录制成功
                _recordingLastTime = 0;
                _recordStatus = true;
                EventDispatcher.TriggerEvent<int, bool>(MainUIEvents.REFRESH_MV_RECORD_TIME, _recordingLastTime, false);
                if (_timerId > 0) TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.AddTimer((uint)_refreshTime, _refreshTime, OnRefreshTime);
            }
        }

        //IOS返回停止录制结果
        private void OnStopRecordOK(string result)
        {
            LoggerHelper.Info("[RePlayKitManager-OnStopRecordOK()] result:" + result);
            if (!string.IsNullOrEmpty(result) && result.Equals("1"))
            {//停止成功
                if (_timerId > 0) TimerHeap.DelTimer(_timerId);
                _timerId = 0;
                _recordingLastTime = 0;
                _recordStatus = false;
                EventDispatcher.TriggerEvent<int, bool>(MainUIEvents.REFRESH_MV_RECORD_TIME, _recordingLastTime, true);
            }
        }

        /// <summary>
        /// 开始录制视频
        /// </summary>
        public void StartRecording()
        {
            if (_recordStatus)
            {
                LoggerHelper.Warning("MV Record [Doing]");
                return;
            }

            LoggerHelper.Info(string.Format("MV Record [Start]"));
            #if UNITY_IPHONE && !UNITY_EDITOR
                IOSPlugins.startRecording();
            #endif
        }

        /// <summary>
        /// 停止录制
        /// </summary>
        public void StopRecording()
        {
            if (!_recordStatus)
            {
                LoggerHelper.Warning("MV Record [HasStop]");
                return;
            }
            #if UNITY_IPHONE && !UNITY_EDITOR
                IOSPlugins.stopRecording();
            #endif
        }

        //刷新时间
        private void OnRefreshTime()
        {
            //派发刷新时间事件给FieldRePlayKitView.cs显示录制时间
            _recordingLastTime += _refreshTime / 1000;
            EventDispatcher.TriggerEvent<int, bool>(MainUIEvents.REFRESH_MV_RECORD_TIME, _recordingLastTime, false);
        }
    }
}
