﻿using ACTSystem;
using Common.Base;
using Common.Events;
using Common.Utils;
using Game.Asset;
using GameData;
using GameLoader.LoadingBar;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using GameLoader.Config;

namespace GameMain.GlobalManager
{
    public class AnimationFrameEditor : MonoBehaviour
    {
        private float _normalizedTime = 0;
        public float normalizedTime = 0;
        private Animator _animator;

        void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        void Update()
        {
            if (_normalizedTime != normalizedTime)
            {
                normalizedTime = Mathf.Clamp01(normalizedTime);
                _normalizedTime = normalizedTime;
                _animator.Play(0, 0, _normalizedTime);
            }
        }
    }

    public class LoadingModelEditor : MonoBehaviour
    {
        public int showNextModel = 0;

        void Update()
        {
            if (showNextModel != 0)
            {
                showNextModel = 0;
                LoadingContentManager.Instance.ShowLoadingContent();
            }
        }
    }

    public class LoadingContentManager
    {
        private uint _floatTipTimerId;
        private uint _modelTimerId;
        private int _floatTipIndex;
        private int _modelIndex;

        private static List<loading_content> _floatTipList;
        private static List<loading_content> _modelList;

        private const float MODE_SCALE_RATE = 0.8f;
        private static readonly Vector2 MODEL_SLOT_SIZE = new Vector2(352, 427);

        private const string IMG_BG_PATH = "Images/Progress_bg2.png";
        private const string DEFAULT_IMG_BG_PATH =  "Dialog/Image/Progress_bg";

        public static readonly string FONT_ASSET_KEY = string.Format("Font${0}$MicrosoftYaHei_unicode.ttf.u", SystemConfig.Language);
        public static readonly string FONT_ASSET_PATH = string.Format("Font/{0}/MicrosoftYaHei_unicode.ttf", SystemConfig.Language);

        private bool firstShow = true;
        private bool defaultBg = false;

        private GameObject modelSlot;
        private Text labDescription;
        private List<GameObject> preloadModelList;
        private int preloadModelIndex;

        public LoadingContentManager()
        {
            InitData();
            InitProgressBarFont();
        }

        private static LoadingContentManager s_Instance;
        public static LoadingContentManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new LoadingContentManager();
                return s_Instance;
            }
        }

        private static void InitData()
        {
            _floatTipList = new List<loading_content>();
            _modelList = new List<loading_content>();
            foreach (KeyValuePair<int,loading_content> pair in XMLManager.loading_content)
            {
                if (pair.Value.__type == 1)
                {
                    _floatTipList.Add(pair.Value);
                }
                else
                {
                    _modelList.Add(pair.Value);
                }
            }
            RandomList(_floatTipList);
            _floatTipList.Sort(SortFun);
            RandomList(_modelList);
            _modelList.Sort(SortFun);
        }

        private void InitProgressBarFont()
        {
            ObjectPool.Instance.GetObject(FONT_ASSET_PATH, (obj) =>
            {
                Font font = obj as Font;
                if (font != null)
                {
                    ProgressBar.Instance.UpdateFont(font);
                }
            });
        }

        private static int SortFun(loading_content x, loading_content y)
        {
            return x.__order - y.__order;
        }

        private static void RandomList(List<loading_content> _dataList)
        {
            for (int i = 0; i < _dataList.Count; i++)
            {
                int radom = UnityEngine.Random.Range(i, _dataList.Count);
                loading_content tmp = _dataList[i];
                _dataList[i] = _dataList[radom];
                _dataList[radom] = tmp;
            }
        }

        public void ShowLoadingContent()
        {
            if (defaultBg == true) return;
            if (firstShow == true)
            {
                LoadBackground();
                ProgressBar.Instance.SetFloatTipVisibility(false);
                firstShow = false;
            }
            ShowFloatTip();
            ShowModel();
        }

        public void CloseLoadingContent()
        {
            ResetFloatTipTimer();
            ResetModelTimer();
            defaultBg = false;
        }

        public void ResetLoadingContent()
        {
            ResetFloatTipTimer();
            ResetModelTimer();
            firstShow = true;
            ProgressBar.Instance.SetFloatTipVisibility(true);
            Texture2D bg = Resources.Load(DEFAULT_IMG_BG_PATH) as Texture2D;
            ProgressBar.Instance.UpdateBg(bg);
            defaultBg = true;
        }

        private void LoadBackground()
        {
            ObjectPool.Instance.GetObject(IMG_BG_PATH, delegate(UnityEngine.Object obj)
            {
                ProgressBar.Instance.UpdateBg(obj as Texture2D);
            });
        }

        private void ResetFloatTipTimer()
        {
            if (_floatTipTimerId != 0)
            {
                TimerHeap.DelTimer(_floatTipTimerId);
                _floatTipTimerId = 0;
            }
        }

        private void ResetModelTimer()
        {
            if (_modelTimerId != 0)
            {
                TimerHeap.DelTimer(_modelTimerId);
                _modelTimerId = 0;
            }
        }

        private void ShowFloatTip()
        {
            ResetFloatTipTimer();
            if (_floatTipList.Count > 0)
            {
                _floatTipIndex = AdjustIndex(_floatTipIndex, _floatTipList);
                if (_floatTipIndex == -1) return;
                ProgressBar.Instance.UpdateFloatText(MogoLanguageUtil.GetContent(_floatTipList[_floatTipIndex].__txt));
                _floatTipTimerId = TimerHeap.AddTimer((uint)_floatTipList[_floatTipIndex].__show_time * 1000, 0, ShowFloatTip);
                _floatTipIndex++;
            }
        }

        private int AdjustIndex(int index, List<loading_content> contentList)
        {
            if (index == -1) index = 0;
            for (int i = index; i < index + contentList.Count; i++)
            {
                int nIndex = i % contentList.Count;
                var level = data_parse_helper.ParseListInt(contentList[nIndex].__level);
                int minLevel = level[0];
                int maxLevel = level[1];
                if (PlayerAvatar.Player == null || (minLevel <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= maxLevel))
                {
                    return nIndex;
                }
            }
            return -1;
        }

        private void ShowModel()
        {
            if (preloadModelList == null || preloadModelList.Count == 0)
            {
                if (_modelList.Count > 0)
                {
                    _modelIndex = AdjustIndex(_modelIndex, _modelList);
                    if (_modelIndex == -1) return;
                    LoadModel(_modelList[_modelIndex].__model, OnModelLoaded);
                }
            }
            else
            {
                GameObject[] gos = new GameObject[preloadModelList.Count];
                preloadModelList.CopyTo(gos);
                RefreshModel(gos, preloadModelIndex);
                _modelIndex++;
                PreloadModel();
            }
        }

        private string[] GetModelPaths(string[] models)
        {
            string[] paths = new string[models.Length];
            for (int i = 0; i < models.Length; i++)
            {
                string modelPath = string.Empty;
                int modelID = 0;
                if (int.TryParse(models[i], out modelID) == false)
                {
                    modelPath = models[i];
                }
                else
                {
                    var actorData = ACTRunTimeData.GetActorData(modelID);
                    if (actorData != null)
                    {
                        modelPath = actorData.GetModelAssetPath();
                    }
                    else
                    {
                        var equipData = ACTRunTimeData.GetEquipmentData(modelID);
                        if (equipData != null)
                        {
                            modelPath = equipData.GetModelAssetPath();
                        }
                    }
                }
                paths[i] = modelPath;
            }
            return paths;
        }

        private void LoadModel(string model, Action<GameObject[]> callback)
        {
            if (string.IsNullOrEmpty(model))
            {
                callback(null);
                return;
            }
            string[] models = model.Split(',');
            Game.Asset.ObjectPool.Instance.GetGameObjects(GetModelPaths(models), (objs) =>
            {
                if (objs == null || objs.Length == 0)
                {
                    GameLoader.Utils.LoggerHelper.Error("CreateActor modelName:" + model + " is null");
                    return;
                }
                callback(objs);
            });
        }

        private void OnModelLoaded(GameObject[] gos)
        {
            ResetModelTimer();
            SetDescription(_modelIndex);
            AttachModelPrefab(gos, _modelIndex);
            _modelTimerId = TimerHeap.AddTimer((uint)_modelList[_modelIndex].__show_time * 1000, 0, ShowModel);
            _modelIndex++;
            PreloadModel();
        }

        private void PreloadModel()
        {
            if (_modelList.Count > 0)
            {
                _modelIndex = AdjustIndex(_modelIndex, _modelList);
                if (_modelIndex == -1) return;
                LoadModel(_modelList[_modelIndex].__model, OnPreloadModelLoaded);
            }
        }

        private void OnPreloadModelLoaded(GameObject[] gos)
        {
            preloadModelIndex = _modelIndex;
            if (preloadModelList == null)
            {
                preloadModelList = new List<GameObject>();
            }
            preloadModelList.Clear();
            for (int i = 0; i < gos.Length; i++)
            {
                GameObject go = gos[i];
                preloadModelList.Add(go);
                if (go == null)
                {
                    continue;
                }
                if (modelSlot == null)
                {
                    CreateModelSlot();
                }
                go.SetActive(false);
                go.transform.SetParent(modelSlot.transform.parent, false);
            }
        }

        private void RefreshModel(GameObject[] gos, int index)
        {
            ResetModelTimer();
            SetDescription(index);
            AttachModelPrefab(gos, index);
            _modelTimerId = TimerHeap.AddTimer((uint)_modelList[index].__show_time * 1000, 0, ShowModel);
        }

        private void SetDescription(int index)
        {
            if (labDescription == null)
            {
                CreateLabelDescription();
            }
            labDescription.text = MogoLanguageUtil.GetContent(_modelList[index].__txt);
        }

        private void CreateLabelDescription()
        {
            GameObject go = new GameObject("labDescription");
            labDescription = go.AddComponent<Text>();
            labDescription.font = Game.Asset.ObjectPool.Instance.GetAssemblyObject(FONT_ASSET_KEY) as Font;
            labDescription.fontSize = 29;
            labDescription.color = new Color(254, 255, 192, 255) / 255f;
            labDescription.lineSpacing = 1.5f;
            labDescription.alignment = TextAnchor.MiddleLeft;
            Outline outline = go.AddComponent<Outline>();
            outline.effectColor = new Color(52, 19, 0, 204) / 255f;
            outline.effectDistance = new Vector2(1, -1);
            Shadow shadow = go.AddComponent<Shadow>();
            shadow.effectColor = new Color(0, 0, 0, 100) / 255f;
            shadow.effectDistance = new Vector2(3.108584f, -2.517281f);
            RectTransform rect = go.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.sizeDelta = new Vector2(550, 400);
            rect.localPosition = new Vector3(550, 450, 0);
            ProgressBar.Instance.AddGameObject(go);
        }

        private void AttachModelPrefab(GameObject[] gos, int index)
        {
            ClearModelSlot();
            if (gos == null || gos.Length == 0)
            {
                Debug.LogError(string.Format("@张敬，加载提示配置表，配置项id={0}模型配置有误.", _modelList[index].__id));
            }
            for (int i = 0; i < gos.Length; i++)
            {
                GameObject go = gos[i];
                if (go != null)
                {
                    if (modelSlot == null)
                    {
                        CreateModelSlot();
                    }
                    var characterController = go.GetComponent<CharacterController>();
                    if (characterController != null)
                    {
                        characterController.enabled = false;
                    }
                    SetModelPrefabParam(index, i, go, characterController);
                    go.SetActive(true);
                    //Ari ModelComponentUtil.ChangeUIModelParam(go);
                    RefreshSortingOrder(go);
                }
                else
                {
                    Debug.LogError(string.Format("@张敬，加载提示配置表，配置项id={0}模型{1}配置有误.", _modelList[index].__id, i + 1));
                }
            }
        }

        private void SetModelPrefabParam(int index, int i, GameObject go, CharacterController characterController)
        {
            Vector3 position = new Vector3(0.5f * MODEL_SLOT_SIZE.x, -0.9f * MODEL_SLOT_SIZE.y, -500);
            Quaternion rotation = Quaternion.Euler(0, 180, 0);
            float scale = 200f;
            if (_modelList[index].__params == null)
            {
                _modelList[index].__params = string.Empty;
            }
            string[] paramList = _modelList[index].__params.Split(',');
            if (paramList.Length < (i + 1) * 7)
            {
                if (characterController != null)
                {
                    scale = MODE_SCALE_RATE * MODEL_SLOT_SIZE.y / characterController.height;
                }
            }
            else
            {
                position = new Vector3(float.Parse(paramList[7 * i + 0]), float.Parse(paramList[7 * i + 1]), float.Parse(paramList[7 * i + 2]));
                rotation = Quaternion.Euler(float.Parse(paramList[7 * i + 3]), float.Parse(paramList[7 * i + 4]), float.Parse(paramList[7 * i + 5]));
                scale = float.Parse(paramList[7 * i + 6]);
            }
            go.transform.SetParent(modelSlot.transform, false);
            go.transform.localPosition = position;
            go.transform.localRotation = rotation;
            go.transform.localScale = new Vector3(scale, scale, scale);
            Animator animator = go.GetComponent<Animator>();
            if (animator != null)
            {
                float normalizedTime = _modelList[index].__animation_time;
                animator.speed = 0;
                animator.Play(0, 0, normalizedTime);
                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    AnimationFrameEditor animationEditor = go.AddComponent<AnimationFrameEditor>();
                    animationEditor.normalizedTime = normalizedTime;
                }
            }
        }

        //修改渲染顺序，需要比进度条背景的sortingOrder高
        private void RefreshSortingOrder(GameObject go)
        {
            Renderer[] rendererList = go.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < rendererList.Length; i++)
            {
                rendererList[i].sortingOrder = 2;
            }
        }

        private void ChangeParticleScale(GameObject parent, float scale)
        {
            ParticleSystem[] particleSystemList = parent.GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < particleSystemList.Length; i++)
            {
                particleSystemList[i].startSize = scale * particleSystemList[i].startSize;
            }
        }

        private float GetUIScale()
        {
            return 1.0f / (float)Screen.height * 10f * CalculateCanvasScaleFactor();
        }

        private float CalculateCanvasScaleFactor()
        {
            float scaleWidth = (float)Screen.width / 1280f;
            float scaleHeight = (float)Screen.height / 720f;
            return scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
        }

        private void ClearModelSlot()
        {
            if (modelSlot != null)
            {
                for (int i = 0; i < modelSlot.transform.childCount; i++)
                {
                    GameObject.Destroy(modelSlot.transform.GetChild(i).gameObject);
                }
            }
        }

        private void CreateModelSlot()
        {
            modelSlot = new GameObject("modelSlot");
            RectTransform rect = modelSlot.AddComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.sizeDelta = MODEL_SLOT_SIZE;
            rect.localPosition = new Vector3(80, 480, 0);
            ProgressBar.Instance.AddGameObject(modelSlot);

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                modelSlot.AddComponent<LoadingModelEditor>();
            }
        }

        private static void ChangeLightDirection(GameObject go)
        {
            Renderer[] renderer = go.transform.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderer.Length; i++)
            {
                Material mat = renderer[i].material;
                if (mat.HasProperty("_SelfShadingLightDirX"))
                {
                    float lightDirX = mat.GetFloat("_SelfShadingLightDirX");
                    lightDirX *= -1;
                    mat.SetFloat("_SelfShadingLightDirX", lightDirX);
                }
                if (mat.HasProperty("_SelfShadingLightDirZ"))
                {
                    float lightDirZ = mat.GetFloat("_SelfShadingLightDirZ");
                    lightDirZ *= -1;
                    mat.SetFloat("_SelfShadingLightDirZ", lightDirZ);
                }
            }
        }

        private void ChangeGameObjectLayer(GameObject go)
        {
            foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = UnityEngine.LayerMask.NameToLayer("LoadBarFx");
            }
        }

    }
}
