
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Astar;
 
public class GameMap : TileBasedMap {
 
	private  int WIDTH = 0;
 
	private  int HEIGHT = 0;
 
	
 
	private byte[,] terrain = null;
	 
	private string[,] units = null;
 
	private bool[,] visited = null;

    private float tileWidth;//一格长度
    private float tileHeight;//一格宽度
    private float offsetX;//X偏移量
    private float offsetZ;//Z偏移量
    private float[,] elevation;//地图高度

    public GameMap(byte[,] terrain, float[,] elevation, int width, int height, float tileWidth, float tileHeight, float offsetX, float offsetZ) 
    {
		
		this.terrain = terrain;
        this.elevation = elevation;
		this.units = new string[width, height];
        this.visited = new bool[width, height]; 
		
		this.WIDTH = width;
		this.HEIGHT = height;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.offsetX = offsetX;
        this.offsetZ = offsetZ;
	}

 
	
 
	public void clearVisited() {
		for (int x=0;x<getWidthInTiles();x++) {
			for (int y=0;y<getHeightInTiles();y++) {
				visited[x,y] = false;
			}
		}
	}
	
 
//	public bool visited(int x, int y) {
//		return visited[x,y];
//	}
	
	public bool visited1(int x, int y) {
		return visited[x,y];
	}
	
 
	public int getTerrain(int x, int y) {
		return terrain[x,y];
	}
	
	public void setTerrain(int x, int y, byte unit) {
		terrain[x,y] = unit;
	}
	
	
	public string getUnit(int x, int y) {
		return units[x,y];
	}
	
 	public void setUnit(int x, int y, string unit) {
 		
 		units[x,y] = unit;

	}
 	
	public byte[,] getTerrain()
 	{
 		return terrain;
 	}
 
	public bool blocked(Mover mover, int x, int y) {
	 
		string type = null;
		bool moveFlag = false;
		if(mover==null)
		{
			return true;
		}
		else
		{
			type = ((UnitMover) mover).getType();
			moveFlag = ((UnitMover) mover).isMoveFlag();
		}
		
	
		
		if(x<0||x>=WIDTH||y<0||y>=HEIGHT)
		{
			return true;
		}
		
		if(terrain[x,y]==1||terrain[x,y]==9)
		{
			return true;
		}
		
		if(moveFlag)
		{
			return false;
		}
		
		string gridStr1 = getUnit(x,y);
		
		if (gridStr1==null||gridStr1.Equals(type)) 
		{
			return false;
		}
		
	 
		return true;
	}

   
	public float getCost(Mover mover, int sx, int sy, int tx, int ty) {
		if(sx == tx || sy == ty)
			return 10;
		else
			return 14;
	}

	 
	public int getHeightInTiles() {
		return HEIGHT;
	}

	 
	public int getWidthInTiles() {
		
		return WIDTH;
	}

	 
	public void pathFinderVisited(int x, int y) {
		visited[x,y] = true;
	}

	public bool hasBarrier(int startX, int startY, int endX, int endY) {
		if (startX == endX && startY == endY)
			return false;

	
		Vector2 point1 = new Vector2(startX, startY);
		Vector2 point2 = new Vector2(endX, endY);
		int distX = Mathf.Abs(endX - startX);
		int distY = Mathf.Abs(endY - startY);

		bool loopDirection = distX > distY ? true : false;

		double i;

		int loopStart;

		int loopEnd;

		List<Vector2> passedNodeList;

		if (loopDirection)
		{

			loopStart = Mathf.Min(startX, endX);
			loopEnd = Mathf.Max(startX, endX);

			for (i = loopStart; i <= loopEnd; i++) {
				if (i == loopStart){
					i += 0.5d;
				}
				double yPos = getLineFunc(point1 , point2, 0, i);
				if(yPos == -1){
					return true;
				}
				passedNodeList = getNodesUnderPoint(i, yPos, null);

				foreach (Vector2 passedNode in passedNodeList) 
				{
					if (blocked(new UnitMover(null, 0, false), (int)passedNode.x,
							(int)passedNode.y))
					{
						return true;
					}
				}

				if (i == loopStart + 0.5d)
				{
					i -= 0.5d;
				}
	
			}
		}
		else 
		{
			loopStart = Mathf.Min(startY, endY);
			loopEnd = Mathf.Max(startY, endY);

			for (i = loopStart; i <= loopEnd; i++) {
				if (i == loopStart){
					i += 0.5d;
				}
				double xPos = getLineFunc(point1, point2, 1, i);
				if(xPos == -1){
					return true;
				}
				passedNodeList = getNodesUnderPoint(xPos, i, null);

				foreach (Vector2 passedNode in passedNodeList) {
					if (blocked(new UnitMover(null, 0, false), (int)passedNode.x,
							(int)passedNode.y)){
						return true;
					}
				}

				if (i == loopStart + 0.5d){
					i -= 0.5d;
				}
			}
		}
		
		return false;
	}

	public List<Vector2> getNodesUnderPoint(double xPos, double yPos,
			List<Vector2> exception) {
		List<Vector2> result = new List<Vector2>();
		bool xIsInt = xPos % 1 == 0;
		bool yIsInt = yPos % 1 == 0;

		if (xIsInt && yIsInt) {
			result.Add(new Vector2((int)(xPos - 1), (int)(yPos - 1)));
			result.Add(new Vector2((int)xPos, (int)(yPos - 1)));
			result.Add(new Vector2((int)(xPos - 1), (int)yPos));
			result.Add(new Vector2((int)xPos, (int)yPos));
		}
		else if (xIsInt && !yIsInt) {
			result.Add(new Vector2((int)(xPos - 1), (int)yPos));
			result.Add(new Vector2((int)xPos, (int)yPos));
		}
		else if (!xIsInt && yIsInt) {
			result.Add(new Vector2((int)xPos, (int)(yPos - 1)));
			result.Add(new Vector2((int)xPos, (int)yPos));
		}
		else {
			result.Add(new Vector2((int)xPos, (int)yPos));
		}

		if (exception != null && exception.Count > 0) {
			for (int i = 0; i < result.Count; i++) {
				if (exception.Contains(result[i])) {
					result.RemoveAt(i);
					i--;
				}
			}
		}

		return result;
	}


	public double getLineFunc(Vector2 ponit1, Vector2 point2, int type, double xy) {

		if (ponit1.x == point2.x) {
			if (type == 0) {
				return -1;
			}
			else if (type == 1) {
				return ponit1.x;

			}
		}
		else if (ponit1.y == point2.y) {
			if (type == 0) {

				return ponit1.y;

			}
			else if (type == 1) {
				return -1;
			}
		}

		double a;


		a = (double)(ponit1.y- point2.y) / (double)(ponit1.x - point2.x);

		double b;

		b = ponit1.y+0.5 - a * (ponit1.x+0.5);

		if (type == 0) {

			return a * xy + b;

		}
		else if (type == 1) {
			return (xy - b) / a;
		}

		return -1;

	}


    /// <summary>
    /// 格子坐标转世界坐标
    /// </summary>
    public Vector3 GridToWorldPos(Vector2 grid)
    {
        return GridToWorldPos((int)grid.x, (int)grid.y);
    }

    /// <summary>
    /// 格子坐标转世界坐标
    /// </summary>
    public Vector3 GridToWorldPos(int x, int y)
    {
        Vector3 pos = new Vector3(offsetX, 0, offsetZ) ;
        pos.x += x * tileWidth + tileWidth / 2;
        pos.z += y * tileHeight + tileHeight / 2;

        pos.y = this.elevation[x, y];
        return pos;
    }

    /// <summary>
    /// 世界坐标转格子坐标
    /// </summary>
    public Vector2 WorldPosToGrid(Vector3 worldPos)
    {
        Vector2 pos;
        worldPos -= new Vector3(offsetX, 0, offsetZ);
        pos.x = (int)(worldPos.x / tileWidth);
        pos.y = (int)(worldPos.z / tileHeight);

        pos.x = Mathf.Clamp(pos.x, 0, WIDTH - 1);
        pos.y = Mathf.Clamp(pos.y, 0, HEIGHT - 1);

        return pos;
    }

    /// <summary>
    /// 判断世界坐标是否在格子的顶点上
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public bool isGridVertex(Vector3 v)
    {
        v -= new Vector3(offsetX, 0, offsetZ);
        if (v.x % tileWidth == 0 && v.z % tileHeight == 0)
        {
            return true;
        }
        return false;
    }

    public float getTileWidth()
    {
        return this.tileWidth;
    }

    public float getTileHeight()
    {
        return this.tileHeight;
    }
}
