
using System.Collections;
using Astar;
 
public class UnitMover : Mover {
 
	private int gridNum;
	private string type;
	private bool moveFlag;//true  不判断方阵阻挡
 
	public UnitMover(string type,int gridNum,bool moveFlag) {
		this.type = type;
		this.gridNum = gridNum;
		this.moveFlag = moveFlag;
	}
	
 
	public string getType() {
		return type;
	}
	
	public int getGridNum()
	{
		return gridNum;
	}


	public bool isMoveFlag() {
		return moveFlag;
	}

 
	
}
