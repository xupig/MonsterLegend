﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Astar;

/// <summary>
/// 寻路工具类
/// </summary>
public class PathFinderUtil 
{

    /// <summary>
    /// 获得移动路径
    /// </summary>
    /// <param name="map"></param>
    /// <param name="srcX"></param>
    /// <param name="srcY"></param>
    /// <param name="endX"></param>
    /// <param name="endY"></param>
    /// <param name="mover"></param>
    /// <param name="fightMapType"></param>
    /// <param name="isFloyd">是否弗洛伊德路径平滑处理</param>
    /// <returns></returns>
    public static Path findPath(GameMap map, Vector2 start, Vector2 end, Mover mover, bool isFloyd) {
		float time = Time.time;
		Path path = null;
		// 如果两点之间存在障碍物才用A星寻路
        if (map.hasBarrier((int)start.x, (int)start.y, (int)end.x, (int)end.y))
        {
			// true 8方向 false 4方向
			PathFinder finder = new AStarPathFinder(map, 500, true, new ManhattanHeuristic(10));
			path = finder.findPath(mover, (int)start.x, (int)start.y, (int)end.x, (int)end.y);
            if (isFloyd)
            {
                floyd(map, path, mover);
            }
		}
		else 
        {
			path = new Path();
            path.appendStep((int)start.x, (int)start.y);
			path.appendStep((int)end.x, (int)end.y);
		}
		if (path == null) {
			Debug.LogError("-------寻不到路：" + ((UnitMover) mover).getType() + ", " + start + ", "
					+ end);
			return null;
		}
        float time1 = Time.time;

		if (time1 - time > 100) {
			Debug.LogError("A* cal exception: start=" + start + ",end=" + end + ",time=" + (time1 - time) + ", phalanxId: " + ((UnitMover) mover).getType());
		}
		return path;
	}

    /// <summary>
    /// 弗洛伊德路径平滑处理
    /// </summary>
    /// <param name="gameMap"></param>
    /// <param name="path"></param>
    /// <param name="mover"></param>
    private static void floyd(GameMap gameMap, Path path, Mover mover)
    {
        if (path == null)
            return;
        List<Step> smoothPaths = path.getSteps();

        int len = smoothPaths.Count;
        if (len > 2)
        {
            Vector2 vector = new Vector2(0, 0);
            Vector2 tempVector = new Vector2(0, 0);

            // 遍历路径数组中全部路径节点，合并在同一直线上的路径节点
            // 假设有1，2，3三点，若2与1的横、纵坐标差值分别与3与2的横、纵坐标差值相等则
            // 判断此三点共线，此时可以删除中间点2
            floydVector(ref vector, smoothPaths[len - 1], smoothPaths[len - 2]);
            for (int i = len - 3; i >= 0; i--)
            {
                floydVector(ref tempVector, smoothPaths[i + 1], smoothPaths[i]);
                if (vector.x == tempVector.x && vector.y == tempVector.y)
                {
                    smoothPaths.RemoveAt(i + 1);
                }
                else
                {
                    vector.x = tempVector.x;
                    vector.y = tempVector.y;
                }
            }
        }

        // 合并共线节点后进行第二步，消除拐点操作。算法流程如下：
        // 如果一个路径由1-10十个节点组成，那么由节点10从1开始检查
        // 节点间是否存在障碍物，若它们之前不存在障碍物，则直接合并
        // 此两路径节点间所有节点
        len = smoothPaths.Count;
        for (int i = len - 1; i >= 0; i--)
        {
            for (int j = 0; j <= i - 2; j++)
            {
                Step step1 = smoothPaths[i];
                Step step2 = smoothPaths[j];
                if (!gameMap.hasBarrier((int)step1.getX(), (int)step1.getY(), (int)step2.getX(), (int)step2.getY()))
                {
                    for (int k = i - 1; k > j; k--)
                    {
                        smoothPaths.RemoveAt(k);
                    }
                    i = j;
                    len = smoothPaths.Count;
                    break;
                }
            }
        }
    }

    private static void floydVector(ref Vector2 target, Step n1, Step n2)
    {
        target.x = (int)(n1.getX() - n2.getX());
        target.y = (int)(n1.getY() - n2.getY());
    }
}
