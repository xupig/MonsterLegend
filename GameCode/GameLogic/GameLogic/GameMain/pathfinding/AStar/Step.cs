
using System.Collections;
using System;
namespace Astar
{
    /**
     * A single step within the path
     * 
     * @author Kevin Glass
     */
    public class Step
    {
        /** The x coordinate at the given step */
        private int x;
        /** The y coordinate at the given step */
        private int y;

        public Step()
        {

        }
        /**
         * Create a new step
         * 
         * @param x The x coordinate of the new step
         * @param y The y coordinate of the new step
         */
        public Step(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * Get the x coordinate of the new step
         * 
         * @return The x coodindate of the new step
         */
        public int getX()
        {
            return x;
        }

        /**
         * Get the y coordinate of the new step
         * 
         * @return The y coodindate of the new step
         */
        public int getY()
        {
            return y;
        }

        /**
         * @see Object#hashCode()
         */
        public int hashCode()
        {
            return x * y;
        }

        /**
         * @see Object#equals(Object)
         */
        public bool equals(Object other)
        {
            if (other is Step)
            {
                Step o = (Step)other;

                return (o.x == x) && (o.y == y);
            }

            return false;
        }

        public override string ToString()
        {
            return this.getX() + "," + this.getY();
        }

        // ===重写比较运算符  
        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Step p = obj as Step;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (x == p.x) && (y == p.y);
        }

        /// <summary>
        /// 定义自己的Equale方法，增强性能
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool Equals(Step p)
        {
            if ((object)p == null)
            {
                return false;
            }

            return (x == p.x) && (y == p.y);
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }

        public static bool operator ==(Step a, Step b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            if (((object)a == null) || (object)b == null)
            {
                return false;
            }

            return a.getX() == b.getX() && a.getY() == b.getY();
        }

        public static bool operator !=(Step a, Step b)
        {
            return !(a == b);
        }
    }
}