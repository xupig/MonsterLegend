
using System.Collections.Generic;
using System;
using System.Collections;
namespace Astar
{
    /**
     * A path determined by some path finding algorithm. A series of steps from
     * the starting location to the target location. This includes a step for the
     * initial location.
     * 
     * @author Kevin Glass
     */
    public class Path
    {
        /** The list of steps building up this path */
        private List<Step> steps = new List<Step>();//

        private List<int> lineNums = null;


        /**
         * Create an empty path
         */
        public Path()
        {

        }


        public List<Step> getSteps()
        {
            return steps;
        }


        public void setSteps(List<Step> steps)
        {
            this.steps = steps;
        }


        public List<int> getLineNums()
        {
            return lineNums;
        }


        public void setLineNums(List<int> lineNums)
        {
            this.lineNums = lineNums;
        }





        public void remove(int index)
        {
            steps.RemoveAt(index);
        }

        /**
         * Append a step to the path.  
         * 
         * @param x The x coordinate of the new step
         * @param y The y coordinate of the new step
         */
        public void appendStep(int x, int y)
        {
            steps.Add(new Step(x, y));
        }

        /**
         * Prepend a step to the path.  
         * 
         * @param x The x coordinate of the new step
         * @param y The y coordinate of the new step
         */
        public void prependStep(int x, int y)
        {
            steps.Insert(0, new Step(x, y));
        }


    }
}