﻿using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingAvatar : BeInspectedObject
    {
        private EntityAvatar _target;
        public InspectingAvatar(EntityAvatar target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateAvatarEquip(result);
            return result;
        }

        private void UpdateAvatarEquip(List<InspectingData> dataList)
        {
            if (_target.actor == null) return;
            InspectingData data = new InspectingData();
            data.title = "AvatarEquipInfo:";
            List<string> value = new List<string>();
            {
                value.Add("facade:" + _target.facade);
                value.Add("defaultClothID:" + _target.actor.equipController.equipCloth.defaultEquipID);
                value.Add("defaultWeaponID:" + _target.actor.equipController.equipWeapon.defaultEquipID);
                value.Add("owner_id: " + _target.owner_id);
                value.Add("team_follow_num: " + _target.team_follow_num);
                value.Add("active_ride_id: " + _target.active_ride_id);
                value.Add("ride_id: " + _target.ride_id);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        } 
    }
}
