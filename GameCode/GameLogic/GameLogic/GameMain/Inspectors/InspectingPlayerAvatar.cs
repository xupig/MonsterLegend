﻿using Common.States;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.PlayerAction;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingPlayerAvatar : BeInspectedObject
    {
        private PlayerAvatar _target;
        public InspectingPlayerAvatar(PlayerAvatar target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateServerTime(result);
            UpdatePlayerSkillInfo(result);
            UpdatePlayerActions(result);
            UpdatePlayerState(result);
            UpdateControlStickState(result);
            UpdateTaskState(result);
            UpdateActivityList(result);
            UpdateUIState(result);
            UpdateTouchMode(result);
            return result;
        }

        private void UpdateServerTime(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "ServerTime:";
            List<string> value = new List<string>();
            {
                value.Add("  " + PlayerTimerManager.GetInstance().GetNowServerDateTime() + " ms:" + PlayerTimerManager.GetInstance().GetNowServerMillisecond());
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdatePlayerSkillInfo(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "PlayerActionSkillInfo:";
            List<string> value = new List<string>();
            value.Add("inServerMode:" + _target.skillManager.inServerMode);
            value.Add("spell_proficient:" + PlayerAvatar.Player.spell_proficient);
            
            var skillList = PlayerDataManager.Instance.SpellData.OriginalLeanedSkillList;
            string learnedSkillList = string.Empty;
            for (int i = 0; i < skillList.Count; i++)
            {
                learnedSkillList += skillList[i] + ",";
            }
            value.Add("learnedSkillListServer:" + learnedSkillList);
            
            var posSkillMap = _target.skillManager.GetCurPosSkillIDMap();
            string curPosSkillStr = "";
            foreach (var pair in posSkillMap)
            {
                curPosSkillStr += pair.Key + ":" + pair.Value + " ";
            }
            value.Add("pos&skill:" + curPosSkillStr);
            string curSlotSkill = "";
            foreach (var pair2 in PlayerDataManager.Instance.SpellData.SpellSlotDict)
            {
                //spell spell = PlayerDataManager.Instance.SpellData.GetSpell(pair2.Value);
                curSlotSkill += pair2.Key + ":" + pair2.Value + " ";
            }
            value.Add("Slot:" + curSlotSkill);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdatePlayerActions(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "PlayerActionInfo:";
            var playerActionManager = PlayerActionManager.GetInstance();
            List<string> value = new List<string>();
            {
                value.Add("currentAction:" + playerActionManager.currentAction);
                value.Add("waitingCount:" + playerActionManager.waitingQueue.Count);
            }
            /*
            if (playerActionManager.currentAction is FindPosition)
            {
                var findPosition = playerActionManager.currentAction as FindPosition;
                value.Add("FindPosition.targetPosition:" + findPosition.targetPosition);
                value.Add("FindPosition.nextPoint:" + findPosition.nextPoint);
            }*/
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdatePlayerState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "PlayerState:";
            var playerActionManager = PlayerActionManager.GetInstance();
            List<string> value = new List<string>();
            {
                value.Add("canBeAttached:" + _target.canBeAttached);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateControlStickState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "ControlStickState:";
            var playerActionManager = PlayerActionManager.GetInstance();
            List<string> value = new List<string>();
            {
                value.Add("strength:" + ControlStickState.strength);
                value.Add("direction:" + ControlStickState.direction);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateTaskState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "TaskState:";
            var playerActionManager = PlayerActionManager.GetInstance();
            var taskIdList = PlayerDataManager.Instance.TaskData.mainTaskInfo;
            string curTaskStr = "";
            List<string> value = new List<string>();
            if (taskIdList!=null)
            {
                curTaskStr += taskIdList.task_id + ":" + taskIdList.status + " ";
            }
            value.Add("CurTask:" + curTaskStr);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateActivityList(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "OpenActivity:";
            var activityList = activity_helper.GetLimitActivityList();
            List<string> value = new List<string>();
            foreach (var activityData in activityList)
            {
                value.Add(" " + activityData.openData.__id);
            }
            
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateUIState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "UIState:";
            List<string> value = new List<string>();
            {
                value.Add("ControlStickState:" + ControlStickState.direction);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateTouchMode(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "TouchMode:";
            List<string> value = new List<string>();
            {
                value.Add("InTouchModeState:" + PlayerTouchBattleModeManager.GetInstance().inTouchModeState);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }
    }
}
