﻿
using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;
namespace GameMain.Inspectors
{
    public class InspectingCamera : BeInspectedObject
    {
        private MogoMainCamera _target;
        public InspectingCamera(MogoMainCamera camera)
        {
            _target = camera;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            if (_target.currentCameraMotion == null)
            {
                return null;
            }
            InspectingData data = new InspectingData();
            data.title = "CameraState:";

            List<string> value = new List<string>();

            value.Add("CurState:" + _target.currentMotionType);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            result.Add(data);

            if (_target.currentMotionType == GlobalManager.CameraMotionType.ONE_VS_ONE)
            {
                UpdateOneVsOne(result);
            }

            return result;
        }

        private void UpdateOneVsOne(List<InspectingData> dataList)
        {
            var motion = _target.currentCameraMotion as OneVsOneMotion;
            InspectingData data = new InspectingData();
            data.title = "OneVsOne:";
            List<string> value = new List<string>();
            value.Add("[X]: " + motion.selfToCenter);
            value.Add("[Distance]: " + motion.distance);
            value.Add("[RotationX]: " + motion.rotation.x);
            value.Add("[Theta]: " + motion.theta);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }
    }
}
