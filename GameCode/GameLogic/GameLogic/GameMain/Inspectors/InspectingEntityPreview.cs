﻿using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;


namespace GameMain.Inspectors
{
    public class InspectingEntityPreview : BeInspectedObject
    {
        private EntityCreature _target;

        public InspectingEntityPreview(EntityCreature target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            return result;
        }

    }
}
