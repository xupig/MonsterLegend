﻿using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    public class InspectingEntityClientDropItem : BeInspectedObject
    {
        private EntityClientDropItem _target;

        public InspectingEntityClientDropItem(EntityClientDropItem target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateItemInfo(result);
            return result;
        }

        private void UpdateItemInfo(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "Current AIBlackBoard State";
            List<string> value = new List<string>();
            {
                value.Add("belongAvatar:" + _target.belongAvatar);
                value.Add("type_id:" + _target.type_id);
                value.Add("isServerItem:" + _target.isServerItem);
                value.Add("picked:" + _target.picked);
                value.Add("pickType:" + _target.pickType);
                value.Add("type_id:" + _target.type_id);
                value.Add("entity_position:" + _target.position);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

    }
}
