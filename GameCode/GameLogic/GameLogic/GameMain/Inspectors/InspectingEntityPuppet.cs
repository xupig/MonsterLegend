﻿using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingEntityPuppet : BeInspectedObject
    {
        private EntityCreature _target;
        public InspectingEntityPuppet(EntityCreature target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateAIBlackBoardState(result);
            return result;
        }

        private void UpdateAIBlackBoardState(List<InspectingData> dataList)
        {
            if (_target.entityAI != null)
            {
                InspectingData data = new InspectingData();
                data.title = "Current AIBlackBoard State";
                var blackBoard = _target.entityAI.blackBoard;
                List<string> value = new List<string>();
                {
                    value.Add("aiState:" + blackBoard.aiState);
                    value.Add("enemyId:" + blackBoard.enemyId);
                }
                data.type = value.GetType();
                data.value = value;
                data.color = Color.cyan;
                dataList.Add(data);
            }
        }        
    }
}
