﻿using ACTSystem;
using Common.Data;
using GameMain.Entities;
using InspectorSetting;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingEntityCreature : BeInspectedObject
    {
        private EntityCreature _target;
        public InspectingEntityCreature(EntityCreature target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateCampState(result);
            UpdateAnimatorState(result);
            UpdateStateData(result);
            UpdateAIData(result);
            UpdateLearnSkillData(result);
            UpdateSkillState(result);
            UpdateSkillCDData(result);
            UpdatePlayingSkillData(result);
            UpdateBufferData(result);
  
            return result;
        }

        private void UpdateCampState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "Current Camp State";
            List<string> value = new List<string>();
            value.Add("camp_id:" + _target.map_camp_id);
            value.Add("pve:" + _target.GetCampPveType());
            value.Add("pvp:" + _target.GetCampPveType());
            data.type = value.GetType();
            data.value = value;
            data.color = Color.white;
            dataList.Add(data);
        }

        private ACTActor _actor = null;
        private void UpdateAnimatorState(List<InspectingData> dataList)
        {
            if (_target != null && _target.actor != null && _target.actor != _actor)
            {
                _actor = _target.actor;  
            }
            if (_actor == null) return;
            var animator = _actor.GetComponent<Animator>();
            AnimatorClipInfo[] animationInfo = animator.GetCurrentAnimatorClipInfo(0);
            InspectingData data = new InspectingData();
            data.title = "Current Animator State";
            List<string> value = new List<string>();
            value.Add("last:" + _actor.animationController.LastAction);
            value.Add("movement:" + _actor.actorController.movement + ":" + _actor.actorState.MovingSpeedState + ":" + _actor.actorState.MovingSpeedTransition);
            value.Add("isSinking:" + _actor.actorController.isSinking);
            value.Add("verticalSpeed:" + _actor.actorController.verticalSpeed);
            foreach (AnimatorClipInfo info in animationInfo)
            {
                value.Add(info.clip.name);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateStateData(List<InspectingData> dataList)
        {
            var actionStateMap = _target.stateManager.characterActionStateMap;
            InspectingData data = new InspectingData();
            data.title = "Current State";
            List<string> value = new List<string>();
            value.Add("state_cell:" + Convert.ToString(_target.state_cell, 2));
            value.Add("currentState:" + Convert.ToString(_target.stateManager.currentState, 2));
            value.Add("speed:" + _target.speed + "-base_speed:" + _target.base_speed + "-moveSpeed:" + _target.moveManager.curMoveSpeed
                + "-speedMlps:" + _target.moveManager.speedMlps.Count );
            value.Add("defaultAccordingMode:" + _target.moveManager.defaultAccordingMode);
            value.Add("accordingMode:" + _target.moveManager.accordingMode);
            value.Add("maxhp:" + _target.max_hp + " - hp:" + _target.cur_hp);
            value.Add("maxep:" + _target.max_ep + " - ep:" + _target.cur_ep);
            value.Add("target_id:" + _target.target_id);
            if (_target.actor != null)
            {
                value.Add("actorpos:" + _target.actor.position + " - entitypos:" + _target.position);
            }
            foreach (var kv in actionStateMap)
            {
                value.Add(kv.Key + ":" + (kv.Value == 0));
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateAIData(List<InspectingData> dataList)
        {
            var aiID = _target.entityAI!=null?_target.entityAI.aiID:0;
            InspectingData data = new InspectingData();
            data.title = "Current AI:";
            string value = aiID + string.Empty;
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateSkillState(List<InspectingData> dataList)
        {
            if (_target.skillManager.inServerMode)
            {
                UpdateSkillStateServer(dataList);
            }
            else
            {
                UpdateSkillStateClient(dataList);
            }
        }

        private void UpdateSkillStateClient(List<InspectingData> dataList)
        {
            var skillList = _target.skillManager.skillSubjectManager.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.title = "Current Skill State";
            List<string> value = new List<string>();
            for (int i = 0; i < skillList.Count; i++)
            {
                value.Add(skillList[i].skillID + ":" + skillList[i].curState);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.grey;
            dataList.Add(data);             
        }

        private void UpdateSkillStateServer(List<InspectingData> dataList)
        {
            var skillList = _target.skillManager.skillSubjectManagerServer.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.title = "Current Skill State";
            List<string> value = new List<string>();
            for (int i = 0; i < skillList.Count; i++)
            {
                value.Add(skillList[i].skillID + ":" + skillList[i].curState);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.grey;
            dataList.Add(data);
        }

        private void UpdateSkillCDData(List<InspectingData> dataList)
        {
            var skillCDs = _target.skillManager.GetSkillCDs();
            InspectingData data = new InspectingData();
            data.title = "Current Skills CD";
            List<string> value = new List<string>();
            string targetStr = "";
            var commonCDTime = _target.skillManager.GetCommonCD() - Time.realtimeSinceStartup;
            if (commonCDTime > 0) 
            {
                targetStr += "commonCD:" + String.Format("{0:F2}", commonCDTime) + " ";
            }
            foreach (var kvp in skillCDs)
            {
                var remainTime = skillCDs[kvp.Key] - Time.realtimeSinceStartup;
                if (remainTime < 0) continue;
                targetStr += kvp.Key + ":" + String.Format("{0:F2}", remainTime) + " ";
            }
            value.Add(targetStr);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.white;
            dataList.Add(data);
        }

        private void UpdateBufferData(List<InspectingData> dataList)
        {
            var buffList = _target.bufferManager.GetCurBufferIDList();
            InspectingData data = new InspectingData();
            data.title = "Current Buffers";
            List<string> value = new List<string>();
            for (int i = 0; i < buffList.Count; i++)
            {
                int bufferID = buffList[i];
                var remainTime = _target.bufferManager.GetBufferRemainTime(bufferID);
                value.Add("Buffer:" + bufferID + " r:" + String.Format("{0:F2}", remainTime) + "s");
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateLearnSkillData(List<InspectingData> dataList)
        {
            var skillList = _target.skillManager.GetLearnedSkillList();
            InspectingData data = new InspectingData();
            data.title = "Current Learned Skills";
            List<string> value = new List<string>();
            string targetStr = "";
            for (int i = 0; i < skillList.Count; i++)
            {
                targetStr += skillList[i].ToString() + " ";
            }
            value.Add(targetStr);
            var map = _target.skillManager.posToSkillIDListMap;
            
            foreach (var pair in map)
            {
                targetStr = "";
                targetStr += pair.Key + ":[";
                foreach (var subpair in pair.Value)
                {
                    targetStr += subpair.Key + ":" + subpair.Value + ",";
                }
                targetStr += "]";
                value.Add(targetStr);
            }
            var slottedSkillList = _target.skillManager.slottedSkillList;
            foreach (var skillID in slottedSkillList)
            {
                targetStr = "slotted:";
                targetStr += skillID + ",";
                value.Add(targetStr);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.white;
            dataList.Add(data);
        }

        private void UpdatePlayingSkillData(List<InspectingData> dataList)
        {
            if (_target.skillManager.inServerMode)
            {
                UpdatePlayingSkillDataServer(dataList);
            }
            else
            {
                UpdatePlayingSkillDataClient(dataList);
            }

        }

        private void UpdatePlayingSkillDataClient(List<InspectingData> dataList)
        {
            var skillSubjectsList = _target.skillManager.skillSubjectManager.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.children = new List<InspectingData>();
            data.title = "Current Playing Skills";
            List<string> value = new List<string>();
            for (int i = 0; i < skillSubjectsList.Count; i++)
            {
                var targetStr = "Skill:";
                var skillSubject = skillSubjectsList[i];
                targetStr += skillSubject.skillID.ToString();
                targetStr += GetEventsString(skillSubject.GetSkillEventList());
                value.Add(targetStr);

                var actionList = skillSubject.GetSkillActionList();
                //List<string> sbuValue = new List<string>();
                for (int j = 0; j < actionList.Count; j++)
                {
                    var action = actionList[j];
                    InspectingData subData = new InspectingData();
                    var subValue = string.Empty;

                    subData.title = "Action:";
                    subValue = action.actionID.ToString() + "-" + GetEventsString(action.GetSkillEventList());
                    subData.type = subValue.GetType();
                    subData.value = subValue;
                    subData.color = Color.blue;
                    data.children.Add(subData);
                }
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.green;
            dataList.Add(data);
        }

        private void UpdatePlayingSkillDataServer(List<InspectingData> dataList)
        {
            var skillSubjectsList = _target.skillManager.skillSubjectManagerServer.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.children = new List<InspectingData>();
            data.title = "Current Playing Skills";
            List<string> value = new List<string>();
            for (int i = 0; i < skillSubjectsList.Count; i++)
            {
                var targetStr = "Skill:";
                var skillSubject = skillSubjectsList[i];
                targetStr += skillSubject.skillID.ToString();
                targetStr += GetEventsString(skillSubject.GetSkillEventList());
                value.Add(targetStr);

                //var actionList = skillSubject.GetSkillActionList();
                ////List<string> sbuValue = new List<string>();
                //for (int j = 0; j < actionList.Count; j++)
                //{
                //    var action = actionList[j];
                //    InspectingData subData = new InspectingData();
                //    var subValue = string.Empty;

                //    subData.title = "Action:";
                //    subValue = action.actionID.ToString() + "-" + GetEventsString(action.GetSkillEventList());
                //    subData.type = subValue.GetType();
                //    subData.value = subValue;
                //    subData.color = Color.blue;
                //    data.children.Add(subData);
                //}
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.green;
            dataList.Add(data);
        }

        private string GetEventsString(List<SkillEventData> eventList)
        {
            //eventList = skillSubject.GetSkillEventList();
            string targetStr = " Event:(";
            for (int j = 0; j < eventList.Count; j++)
            {
                targetStr += " [" + eventList[j].mainID.ToString() + ","
                    + eventList[j].eventIdx.ToString() + "," + eventList[j].delay + "] ";
            }
            targetStr += ")";
            return targetStr;
        }

        
    }
}
