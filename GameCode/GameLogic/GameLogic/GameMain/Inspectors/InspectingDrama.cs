﻿using GameMain.Entities;
using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingDrama : BeInspectedObject
    {
        private PlayerAvatar _target;
        public InspectingDrama(PlayerAvatar target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateTaskState(result);
            UpdateDramaState(result);
            return result;
        }

        private void UpdateTaskState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "TaskState:";
            var taskIdList = PlayerDataManager.Instance.TaskData.mainTaskInfo;
            string curTaskStr = "";
            List<string> value = new List<string>();
            if (taskIdList!=null)
            {
                curTaskStr += taskIdList.task_id + ":" + taskIdList.status + " ";
            }
           
            value.Add("CurTask:" + curTaskStr);
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateDramaState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "DramaState:";
            var dramas = DramaManager.GetInstance().GetDramas();
            List<string> value = new List<string>();
            foreach (var pair in dramas)
            {
                var drama = pair.Value;
                value.Add("[drama]" + drama.name + "[mark]" + drama.dramaKey + " [pos]" + drama.runningPos + ":" + drama.runningActionName + " [state]" + drama.running);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }
    }
}
