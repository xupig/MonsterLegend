﻿using Common.ServerConfig;
using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingActionEntities : BeInspectedObject
    {
        private SpaceActionInspectingManager _spaceActionInspectingCache;
        public InspectingActionEntities(SpaceActionInspectingManager target)
        {
            _spaceActionInspectingCache = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateCreatStateData(result);
            UpdateTrigStateData(result);
            UpdateDestroyStateData(result);
            return result;
        }

        private void UpdateCreatStateData(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "Create:";
            List<string> value = new List<string>();
            List<InspectingInfo> cache = _spaceActionInspectingCache.GetInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_CREATE);
            if (cache == null)
            {
                return;
            }
            for (int i = 0; i < cache.Count; ++i)
            {
                value.Add(string.Concat(cache[i].time, "--", cache[i].actionId));
            }
            value.Sort();
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }


        private void UpdateTrigStateData(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "Trig:";
            List<string> value = new List<string>();
            List<InspectingInfo> cache = _spaceActionInspectingCache.GetInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_TRIGGER);
            if (cache == null)
            {
                return;
            }
            for (int i = 0; i < cache.Count; ++i)
            {
                value.Add(string.Concat(cache[i].time, "--", cache[i].actionId));
            }
            value.Sort();
            data.type = value.GetType();
            data.value = value;
            data.color = Color.magenta;
            dataList.Add(data);
        }

        private void UpdateDestroyStateData(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "Destroy:";
            List<string> value = new List<string>();
            List<InspectingInfo> cache = _spaceActionInspectingCache.GetInspectingCache(public_config.SPACE_ACTION_EVENT_DEBUG_DESTROY);
            if (cache == null)
            {
                return;
            }
            for (int i = 0; i < cache.Count; ++i)
            {
                value.Add(string.Concat(cache[i].time, "--", cache[i].actionId));
            }
            value.Sort();
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }
    }
}
