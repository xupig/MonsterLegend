﻿using Game.Asset;
using GameMain.Entities;
using InspectorSetting;
using MogoEngine.RPC;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingServerProxy : BeInspectedObject
    {
        private ServerProxy _target;
        public InspectingServerProxy(ServerProxy target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateRpcCallStatistics(result);
            UpdateRpcRespStatistics(result);
            return result;
        }

        private void UpdateRpcCallStatistics(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "RpcCallStatistics:";
            List<string> value = new List<string>();
            foreach (var pair in CommunicationStatistics.rpcCallStatistics)
            {
                string subData = pair.Key + " : " + pair.Value;
                value.Add(subData);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private void UpdateRpcRespStatistics(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "RpcRespStatistics:";
            List<string> value = new List<string>();
            foreach (var pair in CommunicationStatistics.rpcRespStatistics)
            {
                string subData = pair.Key + " : " + pair.Value;
                value.Add(subData);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.green;
            dataList.Add(data);
        }
    }
}
