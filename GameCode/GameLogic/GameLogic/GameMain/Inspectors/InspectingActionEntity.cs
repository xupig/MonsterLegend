﻿using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingActionEntity : BeInspectedObject
    {
        private ISpaceAction _target;
        public InspectingActionEntity(ISpaceAction target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateActionStateData(result);
            return result;
        }

        private void UpdateActionStateData(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "State:";
            List<string> value = new List<string>();

            value.Add("id:" + _target.GetID());
            value.Add("action_id:" + _target.GetActionID());
            value.Add("status:" + _target.GetStatus()); 

            value.Sort();
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }
    }
}
