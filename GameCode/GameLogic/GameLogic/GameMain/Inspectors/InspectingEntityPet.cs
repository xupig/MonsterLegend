﻿using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingEntityPet : BeInspectedObject
    {
        private EntityCreature _target;
        public InspectingEntityPet(EntityCreature target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateBaseInfo(result);
            UpdateAIBlackBoardState(result);
            return result;
        }

        private void UpdateBaseInfo(List<InspectingData> dataList)
        {
            var pet = _target as EntityPet;
            InspectingData data = new InspectingData();
            data.title = "BaseInfo:";
            List<string> value = new List<string>();
            {
                value.Add("pet_id:" + pet.pet_id);
                value.Add("master:" + pet.avatar_id);
                value.Add("quality:" + pet.quality);
                value.Add("star:" + pet.star);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.green;
            dataList.Add(data);
        }

        private void UpdateAIBlackBoardState(List<InspectingData> dataList)
        {
            if (_target.entityAI != null)
            {
                InspectingData data = new InspectingData();
                data.title = "Current AIBlackBoard State";
                var blackBoard = _target.entityAI.blackBoard;
                List<string> value = new List<string>();
                {
                    value.Add("aiState:" + blackBoard.aiState);
                    value.Add("enemyId:" + blackBoard.enemyId);
                }
                data.type = value.GetType();
                data.value = value;
                data.color = Color.cyan;
                dataList.Add(data);
            }
        }        
    }
}
