﻿using Game.Asset;
using GameLoader.IO;
using GameMain.Entities;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Inspectors
{
    //[System.Serializable]
    public class InspectingMogoFileSystem : BeInspectedObject
    {
        private ObjectPool _target;
        public InspectingMogoFileSystem(ObjectPool target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateFileIndexesInfo(result);
            return result;
        }

        private void UpdateFileIndexesInfo(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "FileIndexesInfo(:";
            List<string> value = new List<string>();

            var aCount = MogoFileSystem.Instance.fileIndexes;
            foreach (var pair in aCount)
            {
                //value.Add(pair.Key + " : " + pair.Value);
                string subData = pair.Key + " : " + pair.Value;
                value.Add(subData);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.white;
            dataList.Add(data);
        }
    }
}
