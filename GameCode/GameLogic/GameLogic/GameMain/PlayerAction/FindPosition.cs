using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using System.Collections.Generic;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.ClientConfig;
using Common.Utils;
using GameLoader.Utils;
using Common.Data;
using Common.Base;
using GameMain.GlobalManager.SubSystem;
namespace GameMain.PlayerAction
{
    public class FindPosition : BaseAction
    {
        protected ACTActor _actor;
        protected Vector3 _targetPosition;
        protected List<float> _path;
        protected List<float> _testPath;
        protected Vector3 _nextPoint = Vector3.zero;
        protected Vector3 _targetPos = Vector3.zero;
        protected bool _pause = false;
        protected float _arriveDistance = 0.5f;
        public FindPosition(PlayerAvatar owner, Vector3 targetPosition, uint duration)
            : base(owner, duration)
        {
            define = ActionDefine.FIND_POSITION;
            canBreak = true;
            _targetPosition = targetPosition;
            _actor = owner.actor;
        }

        override public bool CanDo()
        {
            if (ControlStickState.strength > 0)
            {
                return false;               
            }
            return true;
        }

        override public uint Does()
        {
            while(true)
            {
                if ((this._state != PlayerActionState.ACTION_STATE_DOING)) break;
                if (!_owner.CanActiveMove()
                    || _owner.bufferManager.ContainsBuffer(InstanceDefine.BUFF_ID_ELEVATOR))
                {
                    _pause = true;
                    break;
                }
                else if (_pause)
                {
                    _pause = false;
                    ResetPath();
                }

                _nextPoint.y = _actor.position.y;
                _targetPos = _targetPosition;
                _targetPos.y = _actor.position.y;
                var distance = Vector3.Distance(_actor.position, _nextPoint);

                if (PathUtils.CanLine(_actor.position, _targetPosition, _actor.modifyLayer))
                {
                    var dis = Vector3.Distance(_actor.position, _targetPos);

                    if (dis < _arriveDistance)
                    {
                        _owner.moveManager.Stop();
                        _path.Clear();
                        End();
                        break;
                    }
                    _owner.moveManager.MoveByLine(_targetPosition, _owner.moveManager.curMoveSpeed);
                }
                else
                {
                    if (distance < _arriveDistance)
                    {
                        if (!SetNextPoint())
                        {
                            _owner.moveManager.Stop();
                            End();
                            break;
                        }
                    }
                    _owner.moveManager.MoveByLine(_nextPoint, _owner.moveManager.curMoveSpeed);
                }
                
                PlayerSprintManager.Instance.TryStartSprint();
                break;
            }
            return base.Does();
        }

        override protected void OnStart()
        {
            _owner.isActiveMoving = true;
            _owner.isFindingPosition = true;
            PlayerAutoFightManager.GetInstance().Stop();
            EventDispatcher.TriggerEvent(BattleUIEvents.SHOW_FIND_PATH);
            ResetPath();
        }

        override protected void OnFail()
        {
            _owner.isActiveMoving = false;
            _owner.isFindingPosition = false;
            _owner.moveManager.Stop();
            EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_FIND_PATH);
            PlayerSprintManager.Instance.TryStopSprint();
            _owner.canAutoTask = false;
        }

        override protected void OnEnd()
        {
            _owner.isActiveMoving = false;
            _owner.isFindingPosition = false;
	        PlayerSprintManager.Instance.TryStopSprint();
            EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_FIND_PATH); 
            PanelIdEnum.SmallMap.Close();
        }

        private void ResetPath()
        {
            var _scene = GameSceneManager.GetInstance().scene;
            List<MovePathData> posInfo = new List<MovePathData>();
            var testInfo = new Dictionary<int, MovePathData>();
            var sPosition = _owner.position;
            //Debug.LogError("起点:" + sPosition + "终点：" + _targetPosition);
            posInfo = (_scene as GameScene).GetFindPathInfo();
            bool canLine = PathUtils.CanLine(sPosition, _targetPosition, _actor.modifyLayer);
            //分段寻路
            if (canLine)
            {
                GeneratePath(sPosition);
            }
            else if (posInfo != null && posInfo.Count > 0)
            {
                //把起点设置为第一个
                var beginPos = sPosition;
                testInfo = GetCurrAreaPoint(beginPos, posInfo);
                var posList = GameSceneManager.GetInstance().GetNearestPath(testInfo, 1, 2);
                //Debug.LogError("posList:" + posList.Count);
                //Debug.LogError("testInfo.Count:" + testInfo.Count);
                if (posList.Count <= 3)
                {
                    GeneratePath(sPosition);
                }
                else
                {
                    CheckNearsPath(beginPos, testInfo, posList);
                }
            }
            else
            {
                GeneratePath(sPosition);
            }
            
            if (!SetNextPoint())
            {
                Stop();
                return;
            }
            var distance = Vector3.Distance(_actor.position, _targetPosition);
            if (distance < _arriveDistance * 2)
            {
                End();
                return;
            }
        }
        
        private Dictionary<int, MovePathData> GetCurrAreaPoint(Vector3 beginPos, List<MovePathData> posInfo)
        {
            var testInfo = new Dictionary<int, MovePathData>();
            int key = 1;
            int endKey = 2;
            var movepathData = new MovePathData();
            movepathData.id = key;
            movepathData.pathPos = beginPos;
            movepathData.relationId = "";
            testInfo.Add(key, movepathData);
            //.计算两点范围内的点
            for (int i = 0; i < posInfo.Count; i++)
            {
                testInfo.Add(posInfo[i].id, posInfo[i]);
            }
            //把终点设置为最后一个
            movepathData = new MovePathData();
            movepathData.id = endKey;
            movepathData.pathPos = _targetPosition;
            movepathData.relationId = "";
            testInfo.Add(endKey, movepathData);

            return testInfo;
        }

        private void CheckNearsPath(Vector3 beginPos, Dictionary<int, MovePathData> testInfo, List<int> posList)
        {
            int id = 0;
            int startActionId = 30000;
            string pathkey = "";
            List<float> currpath = null;
            int mapId = GameSceneManager.GetInstance().curMapID;
            for (int i = 0; i < posList.Count; i++)
            {
                var dic11 = Vector3.Distance(testInfo[posList[i]].pathPos, beginPos);
                if (i == 1) continue;
                float endX = testInfo[posList[i]].pathPos.x;
                float endZ = testInfo[posList[i]].pathPos.z;
                //验证当前分段点是否可直接到达终点
                if (PathUtils.CanLine(beginPos, _targetPosition, _actor.modifyLayer))
                {
                    _testPath = GameSceneManager.GetInstance().GetPath(beginPos.x, beginPos.z,
                        _targetPosition.x, _targetPosition.z, _actor.modifyLayer);
                    CombinationPath();
                    break;
                }
                else
                {
                    currpath = null;
                    if (id > startActionId && posList[i] > startActionId)
                    {
                        pathkey = id.ToString() + "-" + posList[i].ToString();
                        currpath = GameSceneManager.GetInstance().GetMapPathInfo(mapId, pathkey);
                    }
                    if (currpath == null)
                    {
                        //Debug.LogError(id + ":" + posList[i]);
                        _testPath = GameSceneManager.GetInstance().GetPath(beginPos.x, beginPos.z, endX, endZ, _actor.modifyLayer);
                        if (id > startActionId && posList[i] > startActionId)
                        {
                            GameSceneManager.GetInstance().SetCurrMapPathInfo(mapId, pathkey, _testPath);
                        }
                    }
                    else
                    {
                        _testPath = currpath;
                    }
                }
                CombinationPath();
                id = posList[i];
                beginPos = testInfo[posList[i]].pathPos;
                if (beginPos == _targetPosition) break;
            }
            if (_path.Count == 0)
            {
                LoggerHelper.Error("寻路失败，找不到到达该点的路径:" + _targetPosition + " src:" + beginPos + " location:" + _targetPosition);
                Stop();
                return;
            }
            _path = PathUtils.FixPath(_path);
        }

        //合并分段查找路线
        private void CombinationPath()
        {
            if (_path == null || _path.Count == 0)
            {
                _path = _testPath;
            }
            else
            {
                _path.AddRange(_testPath);
            }
        }

        private void GeneratePath(Vector3 sPosition)
        {
            _path = GameSceneManager.GetInstance().GetPath(sPosition.x, sPosition.z, _targetPosition.x, _targetPosition.z, _actor.modifyLayer);
            if (_path == null || _path.Count == 0)
            {
                LoggerHelper.Error("寻路失败，找不到到达该点的路径:" + _targetPosition + " src:" + sPosition + " location:" + _targetPosition);
                Stop();
                return;
            }
            _path = PathUtils.FixPath(_path);
        }

        private bool SetNextPoint()
        {
            if (_path.Count == 0)
            {
                return false;
            }
            _nextPoint.x = _path[0];
            _nextPoint.y = _actor.position.y;
            _nextPoint.z = _path[1];
            _path.RemoveAt(0);
            _path.RemoveAt(0);
            return true;
        }
    }
}