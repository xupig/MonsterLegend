/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：BaseAction
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：行为基类
//==========================================*/

using GameMain.Entities;
using UnityEngine;

namespace GameMain.PlayerAction
{
    public class BaseAction
    {
        public ActionDefine define = ActionDefine.UNKNOW;
        public uint priority = ActionPriority.PRIORITY_UNKNOW;  //行为优先级
        public PlayerAvatar _owner; //行为所有者作用者
        public float duration = 0;  //单位秒
        public bool canBreak = false; //是否可以打断
        public bool canWait = false; //是否可以停在在等待队列中长期等待
        public float overtime = 0; //存在等待队列中的超时时间

        protected uint _state = PlayerActionState.ACTION_STATE_READY;
        protected string _featureString; //某些行为存在的特性字符串
        protected float _endTime = 0;     //行为结束绝对时间，单位毫秒

        public BaseAction(PlayerAvatar owner, uint duration)
        {
            _owner = owner;
            this.duration = duration;
        }

        virtual public bool CanDo()
        {
            return true;
        }

        virtual public bool NeedWaiting()
        {
            return false;
        }

        virtual public uint Does()
        {
            if ((this._state == PlayerActionState.ACTION_STATE_END) || (this._state == PlayerActionState.ACTION_STATE_FAIL))
            {
                return this._state;
            }

            if (this.CanDo())
            {
                if (this._state == PlayerActionState.ACTION_STATE_READY)
                {
                    ResetDuration(this.duration);
                    this.Start();     
                }
            }
            else
            {
                this.Stop();
                return this._state;
            }

            if ((this.duration) > 0 && (Time.realtimeSinceStartup >= this._endTime))
            { this.End(); }

            return this._state;
        }

        virtual public void Start()
        {
            _state = PlayerActionState.ACTION_STATE_DOING;
            OnStart();
        }

        virtual public void Stop()
        {
            if (_state == PlayerActionState.ACTION_STATE_DOING ||
                this._state == PlayerActionState.ACTION_STATE_READY)
            {
                this._state = PlayerActionState.ACTION_STATE_FAIL;
                this.OnFail();
            }
        }

        virtual public void End()
        {
            if (_state == PlayerActionState.ACTION_STATE_DOING ||
                this._state == PlayerActionState.ACTION_STATE_READY)
            {
                _state = PlayerActionState.ACTION_STATE_END;
                this.OnEnd();
            }
        }

        virtual protected void ResetDuration(float newDuration)
        {
            this.duration = newDuration;
            if (this.duration > 0)
            { this._endTime = Time.realtimeSinceStartup + this.duration; }
            else
            { this._endTime = Time.realtimeSinceStartup; }
        }

        virtual protected void OnStart(){}

        virtual protected void OnFail() {}

        virtual protected void OnEnd() {}

        public bool IsEqual(BaseAction action)
        {
            return false;
        }

        public bool IsFeature(string str)
        {
            if (str != null && this._featureString != null)
            {
                return this._featureString.IndexOf(str) >= 0;
            }
            return false;
        }
    }
}


