/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：MoveByStick
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：摇杆行为
//==========================================*/


using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
namespace GameMain.PlayerAction
{
    public class MoveByMultiMap : BaseAction
    {
        protected int _targetInsID;
        protected int _mapLine;
		protected const int MAIN_CITY = -1;

        public MoveByMultiMap(PlayerAvatar owner, int targetInsID,int mapLine, uint duration)
            : base(owner, duration)
        {
            define = ActionDefine.MOVE_BY_MULTI_MAP;
            canBreak = true;
            _targetInsID = targetInsID;
            _mapLine = mapLine;
        }

        override public bool CanDo()
        {
            return true;
        }

        override public uint Does()
        {
            var gameSceneManager = GameSceneManager.GetInstance();
            var curInsID = map_helper.GetInstanceIDByMapID(gameSceneManager.curMapID);
			if (_mapLine == MAIN_CITY && curInsID == _targetInsID && !gameSceneManager.inLoading)
            {
                this.End();
            }
			else if(_mapLine != MAIN_CITY && curInsID == _targetInsID && _mapLine == (int)PlayerAvatar.Player.curLine && !gameSceneManager.inLoading)
			{
				this.End();
			}
            return base.Does();
        }

        override protected void OnStart()
        {
			var curInsID = map_helper.GetInstanceIDByMapID( GameSceneManager.GetInstance().curMapID);
            if (_mapLine == MAIN_CITY)
            {
                var table = new GameLoader.Utils.CustomType.LuaTable();
                table.Add(1, _targetInsID);
                PlayerAvatar.Player.RpcCall("mission_action_req", Common.ServerConfig.action_config.ACTION_ENTER_MISSION, table);
            }
            else
            {
                if (instance_helper.GetChapterType(_targetInsID) != ChapterType.DuelWait)
                {
                    MissionManager.Instance.RequestSwitchLine(_targetInsID, _mapLine);
                }
                else
                {
                    DuelManager.Instance.RequestEnter();
                }
            }
        }

        override protected void OnFail()
        {

        }

        override protected void OnEnd()
        {

        }

    }

}