using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameMain.CombatSystem;
namespace GameMain.PlayerAction
{
    public class PlaySkill : BaseAction
    {
        //private ACTActor _actor;
        private int _skillPos;

        public PlaySkill(PlayerAvatar owner, int skillPos, uint duration = 0)
            : base(owner, duration)
        {
            define = ActionDefine.PLAY_SKILL;
            priority = ActionPriority.PRIORITY_TWO;
            canBreak = false;
            canWait = true;
            //_actor = owner.actor;
            _skillPos = skillPos;
        }

        override public bool CanDo()
        {
            return true;
        }

        override public bool NeedWaiting()
        {
            int skillid = _owner.skillManager.GetSkillIDByPos(_skillPos);
            return _owner.skillManager.CanDo(skillid) > 0;
        }

        override protected void OnStart()
        {
            duration = 0;
            int skillId = this._owner.skillManager.PlaySkillByPos(_skillPos);
            if (skillId == 0) 
            { 
                Stop();
                return;
            }
            if (duration == 0)
            {
                float commonCD = this._owner.skillManager.GetCommonCD() - RealTime.time;
                ResetDuration(commonCD);
            }
        }

        override protected void OnFail()
        {

        }

        override protected void OnEnd()
        {

        }
    }

}