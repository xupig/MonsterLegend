﻿#region 模块信息
/*==========================================
// 文件名：FindNpcAndEnterFb
// 命名空间: GameLogic.GameLogic.GameMain.PlayerAction
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/25 11:21:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.PlayerAction
{
    public class FindNpcAndEnterFb : FindPosition
    {
        private int _instId;
        private int _taskId;
        public FindNpcAndEnterFb(PlayerAvatar owner, Vector3 pos, uint duration,int instId,int taskId)
            : base(owner, Vector3.zero, duration)
        {
            define = ActionDefine.FIND_NPC;
            _instId = instId;
            _taskId = taskId;
            _targetPosition = pos;
            _arriveDistance = 1.5f;
        }

        override protected void OnEnd()
        {
            base.OnEnd();
            TaskManager.Instance.FinishClientTask(10002,_taskId);
            if(_instId!=0)
            {
                MissionManager.Instance.RequsetEnterMission(_instId);
            }
        }
    }
}
