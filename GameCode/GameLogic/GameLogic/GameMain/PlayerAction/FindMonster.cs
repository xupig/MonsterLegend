using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameData;
using System.Collections.Generic;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.ClientConfig;
using Common.Utils;
using GameLoader.Utils;
namespace GameMain.PlayerAction
{
    public class FindMonster : FindPosition
    {
        private int _monsterID = 0;
        public FindMonster(PlayerAvatar owner, Vector3 targetPosition, uint duration, int monsterID)
            : base(owner, targetPosition, duration)
        {
            define = ActionDefine.FIND_MONSTER;
            _monsterID = monsterID;
        }

        protected override void OnStart()
        {
            base.OnStart();
            EventDispatcher.TriggerEvent<int>(PlayerActionEvents.ON_START_FIND_MONSTER, _monsterID);
        }

        protected override void OnFail()
        {
            base.OnFail();
            EventDispatcher.TriggerEvent<int>(PlayerActionEvents.ON_FAIL_FIND_MONSTER, _monsterID);
        }

        override protected void OnEnd()
        {
            base.OnEnd();
            PlayerAutoFightManager.GetInstance().Start();
            EventDispatcher.TriggerEvent<int>(PlayerActionEvents.ON_END_FIND_MONSTER, _monsterID);
        }
    }
}