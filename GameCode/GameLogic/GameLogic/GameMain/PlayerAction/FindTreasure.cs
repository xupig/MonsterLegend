using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameData;
using System.Collections.Generic;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.ClientConfig;
using Common.Utils;
using GameLoader.Utils;
using Common.Data;
using GameMain.GlobalManager.SubSystem;
namespace GameMain.PlayerAction
{
    public class FindTreasure : FindPosition
    {
        private int treasureMapId = 0;
        public FindTreasure(PlayerAvatar owner, Vector3 targetPosition, int treasureMapId, uint duration)
            : base(owner, targetPosition, duration)
        {
            define = ActionDefine.FIND_TREASURE;
            this.treasureMapId = treasureMapId;
        }

        override protected void OnEnd()
        {
            base.OnEnd();
            if (CameraManager.GetInstance().isShow)
            {
                UIManager.Instance.ShowPanel(Common.Base.PanelIdEnum.TreasureMapUse, treasureMapId);
            }
        }

        protected override void OnFail()
        {
            base.OnFail();
            if (_path == null || _path.Count == 0)
            {
                wild_helper.RemoveTreasurePosition(treasureMapId);
                base._targetPosition = wild_helper.GetTreasurePosition(treasureMapId);
                Start();
            }
        }
    }
}