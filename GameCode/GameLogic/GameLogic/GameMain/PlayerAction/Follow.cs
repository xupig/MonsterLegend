/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：MoveByStick
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：摇杆行为
//==========================================*/


using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using Mogo.Util;
using MogoEngine;
namespace GameMain.PlayerAction
{
    public class Follow : BaseAction
    {
        protected uint _targetID;
        public Follow(PlayerAvatar owner, uint targetID, uint duration)
            : base(owner, duration)
        {
            define = ActionDefine.FOLLOW;
            _targetID = targetID;
            canBreak = true;
        }

        override public bool CanDo()
        {
            if (MogoWorld.GetEntity(_targetID) == null)
            {
                MogoUtils.FloatTips(0);
                return false;
            }
            return true;
        }

        override public uint Does()
        {

            return base.Does();
        }

        override protected void OnStart()
        {

        }

        override protected void OnFail()
        {

        }

        override protected void OnEnd()
        {

        }

    }

}