/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：BaseAction
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：行为基类
//==========================================*/

using GameMain.Entities;
using UnityEngine;

namespace GameMain.PlayerAction
{
    public class PlayerActionState
    {
        public const uint ACTION_STATE_READY = 1;
        public const uint ACTION_STATE_END = 2;
        public const uint ACTION_STATE_FAIL = 3;
        public const uint ACTION_STATE_DOING = 4;
    }

    public class ActionPriority
    {
        public const uint PRIORITY_UNKNOW = 0;
        public const uint PRIORITY_ONE = 1;
        public const uint PRIORITY_TWO = 2;
        public const uint PRIORITY_THREE = 3;
    }

    public enum ActionDefine
    {
        UNKNOW,
        FIND_POSITION,
        MOVE_BY_STICK,
        PLAY_SKILL,
        FIND_NPC,
        MOVE_BY_MULTI_MAP,
        FIND_MONSTER,
        FIND_TREASURE,
        FOLLOW,
        MOVE_BY_TOUCH_POINT
    }
}


