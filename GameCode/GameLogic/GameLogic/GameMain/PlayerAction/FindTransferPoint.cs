using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameData;
using System.Collections.Generic;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.ClientConfig;
using Common.Utils;
using GameLoader.Utils;
namespace GameMain.PlayerAction
{
    public class FindTransferPoint : FindPosition
    {
        public FindTransferPoint(PlayerAvatar owner, Vector3 targetPosition, uint duration)
            : base(owner, targetPosition, duration)
        {

        }
    }
}