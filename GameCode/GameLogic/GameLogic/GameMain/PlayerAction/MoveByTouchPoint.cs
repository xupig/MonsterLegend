﻿

using GameMain.Entities;
using GameMain.GlobalManager;
using UnityEngine;
namespace GameMain.PlayerAction
{
    public class MoveByTouchPoint : BaseAction
    {
        public MoveByTouchPoint(PlayerAvatar owner, uint duration)
            : base(owner, duration)
        {
            define = ActionDefine.MOVE_BY_TOUCH_POINT;
            canBreak = true;
        }

        override public bool CanDo()
        {
            return true;
        }

        override public uint Does()
        {
            if (_owner.controlActor.actorController.horizontalSpeed == Vector3.zero)
            {
                _owner.isActiveMoving = false;
            }
            else
            {
                _owner.isActiveMoving = true;
            }
            return base.Does();
        }

        override protected void OnStart()
        {
            _owner.isActiveMoving = true;
        }

        override protected void OnFail()
        {
            _owner.isActiveMoving = false;
            PlayerTouchBattleModeManager.GetInstance().DeleteTouchPosition();
        }

        override protected void OnEnd()
        {
            _owner.isActiveMoving = false;
            PlayerTouchBattleModeManager.GetInstance().DeleteTouchPosition();
        }

    }
}
