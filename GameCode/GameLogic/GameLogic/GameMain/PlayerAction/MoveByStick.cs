/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：MoveByStick
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：摇杆行为
//==========================================*/


using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
namespace GameMain.PlayerAction
{
    public class MoveByStick : BaseAction
    {
        //private ACTActor _actor;
        //private ACTActorController m_actorController;

        public MoveByStick(PlayerAvatar owner, uint duration):base(owner, duration)
        {
            define = ActionDefine.MOVE_BY_STICK;
            canBreak = true;
            //_actor = owner.actor;
            //m_actorController = owner.actor.actorController;
        }

        override public bool CanDo()
        {
            return true;
        }

        override public uint Does()
        {
			if (_owner.moveManager.accordingMode == CombatSystem.AccordingMode.AccordingStick)
			{
				if (_owner.controlActor.actorController.horizontalSpeed == Vector3.zero)
				{
					_owner.isActiveMoving = false;
				}
				else
				{
					_owner.isActiveMoving = true;
				}
			}
			else
			{
				_owner.isActiveMoving = true;
			}

            return base.Does();
        }

        override protected void OnStart()
        {
            _owner.isActiveMoving = true;
        }

        override protected void OnFail()
        {
            _owner.isActiveMoving = false;
        }

        override protected void OnEnd()
        {
            _owner.isActiveMoving = false;
        }

    }

}