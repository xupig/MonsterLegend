using GameMain.Entities;
using UnityEngine;
using ACTSystem;
using Common.States;
using GameData;
using System.Collections.Generic;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.ClientConfig;
using Common.Utils;
using GameLoader.Utils;
namespace GameMain.PlayerAction
{
    public class FindNPC : FindPosition
    {
        private int _npcID;
        public FindNPC(PlayerAvatar owner, int npcID, uint duration)
            : base(owner, Vector3.zero, duration)
        {
            define = ActionDefine.FIND_NPC;
            _npcID = npcID;
            var location = npc_helper.GetNpcLocation(_npcID);
            if (location == null)
            {
                LoggerHelper.Error(string.Format("NPC {0} 没有正确配置坐标信息，无法寻找！", _npcID));
                return;
            }
            _targetPosition = new Vector3(location[0] * 0.01f, _owner.position.y, location[2] * 0.01f);
            _arriveDistance = 1.5f;
        }

        override protected void OnEnd()
        {
            base.OnEnd();
            var npc = NPCManager.GetInstance().FindNPCByNPCID(_npcID);
            if (npc != null) npc.Ask();
        }
    }
}