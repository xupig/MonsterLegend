﻿//#define IPHONE_TEST
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using Common;
using GameLoader.Interfaces;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using GameMain.GlobalManager;
using GameMain.Entities;
using GameLoader.Config;
using MogoEngine.Events;
using Common.Events;
using System.Reflection;
using GameLoader.IO;
using GameData;
using Game.Asset;
using System;
using GameLoader.LoadingBar;
using Game.AssetBridge;
using GameLoader.PlatformSdk;
using Common.ExtendBuilder;
#if UNITY_WEBPLAYER
    //Ari using DockingWeb;
#endif

namespace GameMain
{
    public class Main : IClient
    {
        private int begineTime;
        private const string LIBS = "libs/";       
        private const string DATA_PBUFS = "data/pbufs/";
        private const string XML_SERIALIZED_PBUF = "XMLSerialized.pbuf";

        public bool isUpdateProgress = false;   //是否更改进度[true:更改进度,false:不更改进度]
        public ACTSystem.ACTSystemDriver curACTSystemDriver;

        private List<string> XmlDllList = new List<string> 
        {
            "XMLDefine.dll",
            "XMLSerializer.dll",
            "XMLManager.dll"
        };

        static Main _instance;
        public static Main Instance
        {
            get { return _instance; }
        }

        public Main()
        {
            _instance = this;
            ProgressBar.Instance.UpdateProgress(0.6f);  //GameLogic.dll载入完成60%进度
            Init();
            Start();
        }

        public void Init()
        {
            LoggerHelper.Debug("Main Init");
            AssetBridge.Parse = AssetAssembler.Parse;
            begineTime = Environment.TickCount;
            InitXmlData();
            LoggerHelper.Info(string.Format("InitXmlData cost:{0}ms", Environment.TickCount - begineTime));
            ModuleManager.GetInstance().Init();
            ExtendComponentBuilder.Initialize();
            AIContainer.Init();
            InitLocalSetting();
            begineTime = Environment.TickCount;
            InitACTSystem();
            InitEntityType();
            LoggerHelper.Info(string.Format("InitACTSystem cost:{0}ms", Environment.TickCount - begineTime));
        }

        public void qjphs(string json)
        {

        }

        private void InitEntityType()
        {
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_AVATAR, typeof(EntityAvatar));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_ACCOUNT, typeof(PlayerAccount));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR, typeof(PlayerAvatar));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_DUMMY, typeof(EntityDummy));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_NPC, typeof(EntityNPC));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_MONSTER, typeof(EntityMonster));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_CLIENT_DROP_ITEM, typeof(EntityClientDropItem));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_DROP_ITEM, typeof(EntityDropItem));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_PET, typeof(EntityPet));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_PUPPET, typeof(EntityPuppet));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_GROUND_VEHICLE, typeof(EntityGroundVehicle));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_AIR_VEHICLE, typeof(EntityAirVehicle));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_PREVIEW, typeof(EntityPreview));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_CHEST, typeof(EntityChest));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_DUEL_CHEST, typeof(EntityDuelChest));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_PORTAL, typeof(EntityPortal));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_COMBAT_PORTAL, typeof(EntityCombatPortal));
            EntityMgr.Instance.RegisterEntityType(EntityConfig.ENTITY_TYPE_NAME_AGENT, typeof(EntityAgent));
        }

        private void OnXMLDataInited()
        {
            XMLManager.dataLoader = new XMLDataRuntimeLoader();
            CheckReloadXML();
            EventDispatcher.TriggerEvent(LoginEvents.ON_XMLDATA_INITED);
        }

        private void InitXmlData()
        {
            //ProgressBar.Instance.UpdateTip("正在初始化XmlData");
            LoggerHelper.Info("[Main] 正在初始化XmlData");
            switch (Application.platform)
            { 
                case RuntimePlatform.Android:
                    LoggerHelper.Info("[Android发布模式][Main] 加载代码库[Start]");
                    LoadLibsFromFileSystem();
                    //SeriPbuf(XML_SERIALIZED_PBUF);
                    break;
				case RuntimePlatform.IPhonePlayer:
                    break;
                case RuntimePlatform.WindowsWebPlayer:
					break;
				case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
#if !UNITY_WEBPLAYER
                        LoggerHelper.Info("[开发模式下--发布模式][Main] 加载代码库[Start]");
                        LoadLibsFromFileSystem();
                        //SeriPbuf(XML_SERIALIZED_PBUF);
#endif
                    }
                    else
                    {
                        LoggerHelper.Info(string.Format("[{0}][Main] 加载代码库[Start]", SystemConfig.isIphoneTest ? "IphoneTest模式" : "开发模式"));
                        LoadLibsFromLocal();
                    }
                    break;
            }
            
            OnXMLDataInited();
            ProgressBar.Instance.UpdateProgress(0.65f);
        }

        //发布模式--从pkg文件载入dll
        private void LoadLibsFromFileSystem()
        {
            for (int i = 0; i < XmlDllList.Count; i++)
            { //载入.dll
                LoadDllFromFileSystem(XmlDllList[i]);
            }
        }

        /// <summary>
        /// 载入指定代码库
        /// </summary>
        /// <param name="fileName">指定文件代码库文件名,例如：MogoEngine.dll</param>
        private Assembly LoadDllFromFileSystem(string fileName)
        {
            fileName = string.Concat(LIBS, fileName);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            if (isExistFile)
            {
                byte[] enData = FileAccessManager.LoadBytes(fileName);
                byte[] deData = DESCrypto.Decrypt(enData, CryptoUtils.GetResNumber()); 
                deData = PkgFileUtils.UnpackMemory(deData);
                return Assembly.Load(deData);
            }
            else
            {
                LoggerHelper.Error(string.Format("代码库文件:{0},{1}", fileName, isExistFile ? "存在" : "找不到"));
                return null;
            }
        }

        /// <summary>
        /// 反序列化pbuf还原为数据
        /// </summary>
        /// <param name="fileName">文件名,例如：ACTData.pbuf</param>
        private byte[] SeriPbuf(string fileName)
        {
            string folder = string.Concat(DATA_PBUFS, fileName);
            bool isExistFile = FileAccessManager.IsFileExist(folder);
            if (!isExistFile) LoggerHelper.Debug(string.Format("pbuf文件:{0},{1}", folder, isExistFile ? "存在" : "找不到"));
            byte[] bytes = FileAccessManager.LoadBytes(folder);
            bytes = PkgFileUtils.UnpackMemory(bytes);
            if (fileName == XML_SERIALIZED_PBUF)
            {//反序列化XMLSerialized.pbuf
                //XMLManager.InitFromStream(new MemoryStream(bytes));
            }
            return bytes;
        }

        //开发模式--载入dll
        private void LoadLibsFromLocal()
        {
#if !UNITY_WEBPLAYER
            string baseFolder = string.Concat(SystemConfig.DllPath, "GeneratedLib/");
            for (int i = 0; i < XmlDllList.Count; i++)
            { //载入GeneratedLibs目录下的dll
                Assembly assembly = Assembly.Load(File.ReadAllBytes(string.Concat(baseFolder, XmlDllList[i])));
            }
#endif
        }


        private void CheckReloadXML() 
        {
            GlobalManager.ReloadXmlManager.GetInstance().ReloadFromLocal();
        }

        void InitLocalSetting()
        {
            LocalSetting.Init();
        }

        private void InitACTSystem()
        {
            ACTSystem.ACTSerializableData sData = new ACTSystem.ACTSerializableData();
            SerializableData.DataSerializer ds = new SerializableData.DataSerializer();
            
            if (SystemConfig.ReleaseMode)
            {//发布模式
                //ProgressBar.Instance.UpdateTip("正在初始化pbuf");
                LoggerHelper.Info("正在初始化pbuf");
                Stream strem = new MemoryStream(SeriPbuf("ACTDataActor.pbuf"));
                sData.ActorsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataActor>)) as Dictionary<int, ACTSystem.ACTDataActor>;
                strem = new MemoryStream(SeriPbuf("ACTDataSkill.pbuf"));
                sData.SkillsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataSkill>)) as Dictionary<int, ACTSystem.ACTDataSkill>;
                strem = new MemoryStream(SeriPbuf("ACTDataVisualFX.pbuf"));
                sData.VisualFXsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataVisualFX>)) as Dictionary<int, ACTSystem.ACTDataVisualFX>;
                strem = new MemoryStream(SeriPbuf("ACTDataEquipment.pbuf"));
                sData.EquipmentDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataEquipment>)) as Dictionary<int, ACTSystem.ACTDataEquipment>;
                ProgressBar.Instance.UpdateProgress(0.67f);
            } 
            else
            {
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.Config.SystemConfig.ResPath + "/data/pbufs/ACTDataActor.pbuf"))
                {
                    sData.ActorsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataActor>)) as Dictionary<int, ACTSystem.ACTDataActor>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.Config.SystemConfig.ResPath + "/data/pbufs/ACTDataSkill.pbuf"))
                {
                    sData.SkillsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataSkill>)) as Dictionary<int, ACTSystem.ACTDataSkill>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.Config.SystemConfig.ResPath + "/data/pbufs/ACTDataVisualFX.pbuf"))
                {
                    sData.VisualFXsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataVisualFX>)) as Dictionary<int, ACTSystem.ACTDataVisualFX>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.Config.SystemConfig.ResPath + "/data/pbufs/ACTDataEquipment.pbuf"))
                {
                    sData.EquipmentDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataEquipment>)) as Dictionary<int, ACTSystem.ACTDataEquipment>;
                    fs.Close();
                }
            }

            //ProgressBar.Instance.UpdateTip("正在初始化ACTSystem");
            var obj = new UnityEngine.GameObject("ACTSystem");
            UnityEngine.Object.DontDestroyOnLoad(obj);
            obj.AddComponent<ACTSystem.ACTDriver>();
            curACTSystemDriver = obj.AddComponent<ACTSystem.ACTSystemDriver>();
            curACTSystemDriver.runtimeMode = true;
            //var actSystem = ACTSystem.ACTSystemDriver.GetInstance();
            curACTSystemDriver.ObjectPool.SetAssetLoader(ACTSystemAdapter.GetRuntimeLoader());
            curACTSystemDriver.SoundPlayer = new ACTRuntimeSoundPlayer();
            curACTSystemDriver.SystemData = UnityEngine.ScriptableObject.CreateInstance<ACTSystem.ACTSystemData>();
            curACTSystemDriver.SystemData.InitBySerializableData(sData);
            ShaderUtils.ShaderRuntime.loader = new ShaderRuntimeLoader();
            ShaderUtils.ShaderRuntime.inGameRuntime = true;
            ProgressBar.Instance.UpdateProgress(0.7f);
            //obj.AddComponent<FPSCounter>();
        }

        void EnterLoginSceneCallBack()
        {  
            EventDispatcher.RemoveEventListener(LoginEvents.ON_ENTER_LOGIN_SCENE, EnterLoginSceneCallBack);
        }

        public void Start()
        {
            //EventDispatcher.AddEventListener(LoginEvents.ON_ENTER_LOGIN_SCENE, EnterLoginSceneCallBack);
            isUpdateProgress = true;
            DramaManager.GetInstance();
            NetSocketManager.GetInstance();
            GameStatisticsManager.GetInstance();
            
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                int w = Screen.width;
                int h = Screen.height;
                Screen.SetResolution(w, h, false);
               // PlatfromManager.GetInstance().InitData();
                string json = Environment.CommandLine;
                string _loginJson = "{" + json.Split('{')[1];// +"{" + json.Split('{')[2];
                LoggerHelper.Info("Main start-------------:" + "母串:" + json);
                LoggerHelper.Info("Main start-------------:" + "json:" + _loginJson);
                LoginManager.Instance.SetWindowsPlatformLoginInfo(_loginJson);
                if (LoaderDriver.Instance.gameObject.GetComponent<PlatformWindows>() == null)
                {
                    LoaderDriver.Instance.gameObject.AddComponent<PlatformWindows>();
                }
                DockingService.Initialize();
            }
            if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                WebLoginInfoManager.GetInstance();
                WebLoginInfoManager.GetInstance().Parseurl();
                if (LoaderDriver.Instance.gameObject.GetComponent<PlatformWindows>() == null)
                {
                    LoaderDriver.Instance.gameObject.AddComponent<PlatformWindows>();
                }
                #if UNITY_WEBPLAYER
                /*Ari 
                if (LoaderDriver.Instance.gameObject.GetComponent<Docking>() == null)
                {
                    LoaderDriver.Instance.gameObject.AddComponent<Docking>();
                }*/
                #endif
                DockingService.Initialize();
            }
            //CameraManager.GetInstance().InitCameraGameObject();                //初始化主摄像机prefab
            ObjectPool.Instance.SetFontAssetPath(SystemConfig.Language);
            LoginManager.InistTipDict();
            if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                //GameStateManager.GetInstance().SetState(GameDefine.STATE_CHOOSE_CHARACTER);
                LoginManager.Instance.LoadServerList();
                ProgressBar.Instance.Close();
                LoadingContentManager.Instance.CloseLoadingContent();
            }
            else
            {
                GameStateManager.GetInstance().SetState(GameDefine.STATE_LOGIN);
            }
            //GameStateManager.GetInstance().SetState(GameDefine.STATE_LOGIN);
            if (Application.platform == RuntimePlatform.WindowsEditor 
                || Application.platform == RuntimePlatform.OSXEditor 
                || Application.platform == RuntimePlatform.WindowsPlayer
                || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                KeyboardManager.GetInstance();
            }
            if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingServerProxy(null), GameObject.Find("Driver"));

            //IOS9 载入DepthSearch数据
            #if UNITY_IPHONE && !UNITY_EDITOR
                IOSPlugins.UpdateIos9SearchData();
                IOSPlugins.registerGameObjectName(PlatformSdkMgr.Instance.name);
                RePlayKitManager.Instance.AddEventListener();
                LoggerHelper.Info("[IOSPlugins] register gameObjectName:" + PlatformSdkMgr.Instance.name);
            #endif
        }
    }

}