﻿using ACTSystem;
using GameLoader.Utils;
using MogoEngine.RPC;
using System;
using UnityEngine;
using Common.ExtendTools;
using GameMain.GlobalManager;

namespace GameMain.Entities
{
    public class EntityPortal : EntityItemBase
    {
        #region def属性
        public UInt64 owner_dbid { get; set; }
        public uint mission_id { get; set; }
        #endregion

        public EntityPortal()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_NAME_PORTAL;
        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            DestroyModel();
        }

        public override void OnEnterSpace()
        {
            CreateModel();
        }

        public override void OnLeaveSpace()
        {

        }

        private GameObject portalModel;
        protected bool _modelLoaded = false;
        private void CreateModel()
        {
            if (owner_dbid != PlayerAvatar.Player.dbid) return;
            if (this._modelLoaded) return;
            this._modelLoaded = true;
            string[] resourcePaths = GetResourcePaths();
            if (resourcePaths != null)
            {
                Game.Asset.ObjectPool.Instance.GetGameObjects(resourcePaths, (objs) =>
                {
                    var model = objs[0];
                    if (model == null)
                    {
                        LoggerHelper.Error(resourcePaths[0] + " load error! ");
                        return;
                    }
                    //var actor = model.AddComponent<ActorPortalTrigger>();
                    //actor.ownerID = this.id;
                    portalModel = model;
                    OnModelLoaded();
                });
            }
            else
            {
                LoggerHelper.Error("Portal does not have a match modelPath");
            } 
        }

        private string[] GetResourcePaths()
        {
            string modelPath = GameData.drop_model_helper.GetPortalModelPath();
            if (!string.IsNullOrEmpty(modelPath))
            {
                return new string[] { modelPath };
            }
            return null;
        }

        public override Transform GetTransform()
        {
            return portalModel.transform;
        }

        protected override Vector3 GetTriggerWidth()
        {
            return Vector3.one * 3;
        }

        private void OnModelLoaded()
        {
            if (isInWorld)
            {
                portalModel.name = string.Format("{0}-{1}-{2}", entityType, id, owner_dbid);
                FixPosition();
                this.SetPosition(portalModel.transform.position);
                AddBoxCollider();
                ShowBillboard();
            }
            else
            {
                DestroyModel();
            }
        }

        private void DestroyModel()
        {
            if (portalModel != null)
            {
                HideBillboard();
                GameObject.Destroy(portalModel);
                portalModel = null;
            }
        }

        private void ShowBillboard()
        {
            CreatureDataManager.GetInstance().AddEntityID(id);
        }

        private void HideBillboard()
        {
            CreatureDataManager.GetInstance().RemoveEntityID(id);
        }
        
    }
}
