﻿using System;

namespace GameMain.Entities
{
    /// <summary>
    /// 木桩实体————用于决斗场投注的实体
    /// </summary>
    public class EntityAgent : EntityAvatar
    {
        #region def属性

        #endregion

        public override void Think()
        {
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                actor.gameObject.name = "Agent-" + this.name + "-" + this.id;
            }
        }
    }
}
