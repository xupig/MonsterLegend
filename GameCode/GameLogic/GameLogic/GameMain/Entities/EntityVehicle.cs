﻿#region 模块信息
/*==========================================
// 模块名：EntityClientVehicle
// 命名空间: GameMain.Entities
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/16/18
// 描述说明：客户端载具
// 其他：
//==========================================*/
#endregion

using MogoEngine;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMain.Entities
{
    public class EntityVehicle : EntityDummy
    {
        public static int RIDER_ACTION = 79;
        public virtual void DestroyEntityModel()
        {
            MogoWorld.DestroyEntity(id);
            //恢复骑乘者技能图标
            driver.SetSkillIcon();
        }

        public bool BelongsToPlayer()
        {
            return driver is PlayerAvatar;
        }
    }
}
