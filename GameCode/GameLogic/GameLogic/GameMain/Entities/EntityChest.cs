﻿using ACTSystem;
using Common.Events;
using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using UnityEngine;
using Common.ExtendTools;

namespace GameMain.Entities
{
    public class EntityChest : EntityChestBase
    {
        public EntityChest()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_NAME_CHEST;
        }

        protected override void CheckIsPickUp()
        {
            //Ari TreasureManager.Instance.CheckChestIsPicked(id);
        }
    }
}
