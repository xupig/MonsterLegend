﻿using GameData;
using GameMain.GlobalManager;
using UnityEngine;

namespace GameMain.Entities
{
    public class EntityPuppet : EntityAvatar
    {

        public EntityPuppet()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_PUPPET;
        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingActor;
            moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (this.actor != null && GameSceneManager.GetInstance().inCombatScene)
            {
                this.actor.gameObject.name = "Puppet-" +  this.name + "-" + this.id;
                RefreshLifeBarVisible();
            }
            if (actor != null)
            {
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityPuppet(this), this.actor.gameObject);
            }
            InitAI();
        }

        private void InitAI()
        {
            if (ai_id > 0) {
                moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingEntity;
                return;
            }
            EnterFollower();
            if (aiID == 0)
            {
                aiID = map_helper.GetOneAutoRobotAI(GameSceneManager.GetInstance().curMapID);// 1108;
            }
            if (aiID > 0)
            {
                entityAI = new EntityPuppetAI(this);
                entityAI.SetBehaviorTree((uint)aiID);
            }
        }

        public override void OnDeath()
        {
            base.OnDeath();
            entityAI = null;
        }
    }
}
