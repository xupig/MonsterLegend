﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using Common.Structs;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager;
using GameMain.CombatSystem;

using ACTSystem;
using Mogo.Game;
using Mogo.AI;
using GameData;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using Common.ClientConfig;
using MogoEngine;

namespace GameMain.Entities
{
    public partial class EntityCreature : Entity
    {

        public void OnSyncOneBa(byte attrIdbyte, Int32 value)
        {
            float v = 0;
            fight_attri_config attrId = (fight_attri_config)attrIdbyte;
            if (XMLManager.attri_config.ContainsKey(attrIdbyte) && XMLManager.attri_config[attrIdbyte].__is_float == 1)
            {
                v = (float)value * 0.0001f ;
            }
            else
            {
                v = value;
            }
            if (attributeDict.ContainsKey(attrId))
            {
                attributeDict[attrId] = v;
            }
            else
            {
                attributeDict.Add(attrId, v);
            }
            OnAttributeChange(attrId);
        }

        public void OnSyncMultiBa(byte[] byteArr)
        {
            PbAttriList list = MogoProtoUtils.ParseProto<PbAttriList>(byteArr);
            foreach (PbAttri attri in list.attriList)
            {
                OnSyncOneBa((byte)attri.atrri_id, (int)attri.attri_value);
            }
        }

        public virtual void OnAttributeChange(fight_attri_config attrId)
        {
            if (attrId == fight_attri_config.SPELL_PROFICIENT1_FACTOR)
            {
                var attr = this.GetAttribute(fight_attri_config.SPELL_PROFICIENT1_FACTOR);
                MogoWorld.SynEntityAttr(this, EntityPropertyDefine.spell_proficient_factor1, attr);
            }
            else if (attrId == fight_attri_config.SPELL_PROFICIENT2_FACTOR)
            {
                var attr = this.GetAttribute(fight_attri_config.SPELL_PROFICIENT2_FACTOR);
                MogoWorld.SynEntityAttr(this, EntityPropertyDefine.spell_proficient_factor2, attr);
            }
            else if (attrId == fight_attri_config.SPELL_PROFICIENT3_FACTOR)
            {
                var attr = this.GetAttribute(fight_attri_config.SPELL_PROFICIENT3_FACTOR);
                MogoWorld.SynEntityAttr(this, EntityPropertyDefine.spell_proficient_factor3, attr);
            }
        }
    }
}
