﻿using Common.Events;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.Entities
{
    public class EntityPreview : EntityAvatar
    {
        private GameObject _sceneSlot;
        private int _skillID;
        private Vector3 _bornLocalPosition = Vector3.zero;
        private Vector3 _bornLocalRotation = Vector3.zero;
        private bool _isInCommonCD = false;

        public EntityPreview()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_PREVIEW;
            visualFXPriority = ACTSystem.VisualFXPriority.H;
        }

        public override int GetSpaceQualitySetting()    //最高质量
        {
            return 2;
        }

        public void SetSceneSlot(GameObject target)
        {
            _sceneSlot = target;
            //GameLoader.Utils.Timer.TimerHeap.AddTimer(90, 0, () =>
            //    {
            //        SetAllChildLayer();
            //    });
            
            
        }

        private void SetAllChildLayer()
        {
            //for (int i = 0; i < target.transform.childCount; i++)
            //{
            //    var bone = target.transform.GetChild(i);
            //    bone.gameObject.layer = 22;
            //}
            if(_sceneSlot == null) return;
              var children =       _sceneSlot.transform.GetComponentsInChildren<Transform>();
              //LoggerHelper.Error("1_____________children.Length:    " + children.Length);
              foreach (var child in children)
              {
                  //LoggerHelper.Error("2_____________child.name:    " + child.name);
                  child.gameObject.layer = 22;
              }
        }

        public void SetSkillID(int skillID)
        {
            CloseAllSkillSound();
            this._skillID = skillID;
        }

        public void PlaySkill()
        {
            if (skillManager == null)
            {
                LoggerHelper.Error("[EntityPreview:PlaySkill]=>skillManager is null");
                return;
            }
            //LoggerHelper.Error("[EntityPreview:PlaySkill]=>_skillID:    " + _skillID + ",PlayerAvatar.Player.max_ep:    " + PlayerAvatar.Player.max_ep);
            ResetLocalTransform();
            cur_ep = PlayerAvatar.Player.max_ep;    //每次给最大能量

            if (actor == null)
            {
                if (actor.equipController != null)
                {
                    actor.equipController.equipWeapon.InCity(false);
                }
            } 

            skillManager.PlaySkill(_skillID);
            _isInCommonCD = true;
        }

        public override void Update()
        {
            base.Update();
            if (_isInCommonCD == true)
            {
                if (skillManager.IsInCommonCD(_skillID) == false)
                {
                     _isInCommonCD = false;
                     //LoggerHelper.Error("[EntityPreview:Update]=>1______Time:   " + Time.realtimeSinceStartup);  
                     EventDispatcher.TriggerEvent(SpellEvents.SPELL_CG_END);
                }
                
            }
        }

        private void SetActorInfo()
        {
            if (actor == null) return;

            actor.gameObject.transform.localPosition = Vector3.zero;
            actor.gameObject.transform.localEulerAngles = new Vector3(0, 270, 0);
            //备份本地坐标,放技能时候恢复之用
            _bornLocalPosition = actor.gameObject.transform.localPosition;
            _bornLocalRotation = actor.gameObject.transform.localEulerAngles;
        }

        public void ResetLocalTransform()
        {
            if (actor == null) return;
            actor.gameObject.transform.localPosition = _bornLocalPosition;
            actor.gameObject.transform.localEulerAngles = _bornLocalRotation;
        }

        #region 重载
        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            this.moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
            this.moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingActor;
            entityAI = null;
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                actor.gameObject.name = "Preview" + "_" + this.id;
                
                actor.gameObject.transform.SetParent(_sceneSlot.transform.FindChild("Slot"), false);
                actor.gameObject.AddComponent<AudioListener>();
                SetActorInfo();

                ACTSystemAdapter.SetActorCityState(actor, false, (int)this.vocation, () =>
                    {
                        EventDispatcher.AddEventListener(SpellEvents.SPELL_CG_PLAY, PlaySkill);
                        actor.equipController.equipWeapon.InCity(false);
                        DelayPlaySkill();
                    });
            }
        }

        public void DelayPlaySkill()
        {
            PlaySkill();
            //GameLoader.Utils.Timer.TimerHeap.AddTimer(300, 0, () =>
            //    {
            //        PlaySkill();
            //    });
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            EventDispatcher.RemoveEventListener(SpellEvents.SPELL_CG_PLAY, PlaySkill);
        }
        #endregion

        public void CloseAllSkillSound()
        {
            if (actor == null) return;

            var audioSources = actor.gameObject.transform.GetComponentsInChildren<AudioSource>();
            foreach (var item in audioSources)
            {
                item.clip = null;
            }
        }


    }
}
