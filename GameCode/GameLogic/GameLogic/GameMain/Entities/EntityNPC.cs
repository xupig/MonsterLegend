﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using Common.Structs;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager;


using ACTSystem;
using Mogo.Game;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.ExtendTools;
using GameLoader.Utils;


namespace GameMain.Entities
{
    public class EntityNPC : EntityCreature
    {

        public int npc_id { get; set; }

        public int activityID;

        public NPCStateManager npcStateManager;

        public EntityNPC()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_NAME_NPC;
        }

        public override void OnEnterWorld()
        {
            actorID = npc_helper.GetNpcModelID(npc_id);
            base.OnEnterWorld();
            this.moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
            this.moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingActor;
            npcStateManager = new NPCStateManager(this);
            if (!this.isDramaEntity) actorVisible = false;
            ResetPlayerEnterOnly();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (this.actor != null){
                this.actor.gameObject.name = "EntityNPC" + "-" + this.npc_id + "-" + this.id + ":" + actor.modelName;
                this.actor.actorController.isStatic = true;
                ResetScale();
                NPCManager.GetInstance().ResetNPCTaskState(this);
            }
        }

        public override bool CanTouch()
        {
            return true;
        }

        public void ResetPlayerEnterOnly()
        {
            if (IsNearbyPlayer()) _playerEnter = true;
        }

        void ResetScale()
        {
            var scale = npc_helper.GetNpcScale(npc_id);
            if (scale != 1)
            {
                AddScaleMlp(scale);
            }
            else
            {
                if (actor != null)
                {
                    actor.gameObject.transform.localScale = new Vector3(scale, scale, scale);
                }
            }
        }

        public override void AddSelectTrigger(GameObject triggerGo)
        {
            var trigger = triggerGo.AddComponentOnlyOne<ActorNPCSelectTrigger>();
            trigger.id = this.id;
        }

        bool _playerEnter = false;
        int _spaceTime = 0;
        public override void Update()
        {
            base.Update();
            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = UnityEngine.Random.Range(10,25);
            CheckPlayerEnter();
            CheckPlayerCanSee();
        }

        public bool IsNearbyPlayer()
        {
            if (PlayerAvatar.Player == null) return false;
            Vector3 playerPos = PlayerAvatar.Player.position;
            Vector3 myPos = this.position;
            playerPos.y = 0;
            myPos.y = 0;
            var dis = (playerPos - myPos).magnitude;
            return dis < 3;
        }

        public void Ask()
        {
            _playerEnter = true;
            if (CameraManager.GetInstance().isShow)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TALK_TO_NPC, this.npc_id, false);
            }
        }

        void CheckPlayerEnter()
        {
            if (IsNearbyPlayer())
            {
                if (!_playerEnter)
                {
                    _playerEnter = true;
                    if(CameraManager.GetInstance().isShow)
                    {
                        EventDispatcher.TriggerEvent(TaskEvent.TALK_TO_NPC, this.npc_id, true);
                    }
                }
            }
            else
            {
                if (_playerEnter)
                {
                    _playerEnter = false;
                }
            }
        }

        protected override void RefreshLifeBarVisible()
        {
        }

        protected override int GetRandomSpeechID(int id)
        {
            return npc_helper.GetRandomSpeechID(npc_id, id);
        }

        void CheckPlayerCanSee()
        {
            if (this.isDramaEntity) return;
            if (CanSeeByPlayer())
            {
                if (!actorVisible)
                {
                    actorVisible = true;
                    if (this.actor != null)
                    {
                        actor.gameObject.SetActive(true);
                    }
                }
            }
            else
            {
                if (actorVisible)
                {
                    actorVisible = false;
                    if (this.actor != null)
                    {
                        actor.gameObject.SetActive(false);
                    }
                }
            }
        }

        bool CanSeeByPlayer()
        {
            if (PlayerAvatar.Player == null) return false;
            Vector3 playerPos = PlayerAvatar.Player.position;
            Vector3 myPos = this.position;
            playerPos.y = 0;
            myPos.y = 0;
            var dis = (playerPos - myPos).magnitude;
            return dis < 20;
        }
    }

}
