﻿using System;
using System.Collections.Generic;

using MogoEngine.RPC;
using GameLoader.Utils.CustomType;
using GameLoader.Utils;
using MogoEngine.Events;
using GameLoader.Config;
using Common.Structs;
using Common.Events;
using GameMain.GlobalManager;

using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Base;
using GameLoader.PlatformSdk;
using Common.Data;
using Common.States;
using UnityEngine;
using GameData;
using Common;

namespace GameMain.Entities
{
    /// <summary>
    /// 登陆账号--实体类
    /// 1、一个账号(Account)有多个角色(Avatar),最多不超过4个
    /// </summary>
    public class PlayerAccount : Entity
    {
        //当前进入游戏的角色名称
        private string curRoleName;
        //当前进入游戏的角色dbid
        private ulong curAvatarDbid;

        public UInt16 game_step { get; set; }

        public SByte timezone { get; set; }
        /// <summary>
        /// 账号名称
        /// </summary>
        public string name { set; get; }
        //已废弃的变量
        public LuaTable avatars_info { get; set; }
        /// <summary>
        /// 当前选中角色ID
        /// </summary>
        public uint active_avatar_id { set; get; }
        /// <summary>
        /// 上次登录角色dbid
        /// </summary>
        public ulong last_avatar_dbid { set; get; }
        /// <summary>
        /// 防沉迷标记
        /// </summary>
        public UInt16 anti_addiction { set; get; }
        /// <summary>
        /// 今日沉迷时间
        /// </summary>
        public UInt32 addiction_time { set; get; }
        /// <summary>
        /// 账号--已创角列表
        /// avatarsInfo转换后实体数据
        /// Dictionary<职业ID,角色信息>
        /// </summary>
        public Dictionary<ulong, AvatarInfo> avatarList { get; private set; }

        public PlayerAccount()
        {
            entityType = EntityConfig.ENTITY_TYPE_ACCOUNT;
            avatarList = new Dictionary<ulong, AvatarInfo>();
        }

        /// <summary>
        /// 账号登陆成功
        /// </summary>
        public override void OnEnterWorld()
        {
            LoggerHelper.Debug("Account.OnEntityWorld");
            //RpcCall("SetPlatAccountReq", SystemConfigEx.Instance.Passport);
            RemoveEventListener();
            AddEventListener();
            LoggerHelper.Debug("timezone:" + timezone + ",isQuickLogin:" + LoginManager.Instance.isQuickLogin);
            LoginManager.Instance.nearLoginServerId = LocalSetting.settingData.SelectedServerID; //登录成功，更改最近登录服务器
            Common.Global.Global.serverTimeZone = timezone;
            SendMobileInfo();          //发送设备信息
            GetAvatarList();           //获取角色列表
            PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);
            ServerProxy.Instance.IsEnableReconnect(true);//开启网络断线检查
        }

        public override void OnLeaveWorld()
        {
            RemoveEventListener();
            LoggerHelper.Debug("Account.OnLeaveWorld");
            PlayerTimerManager.GetInstance().enableNetTimeoutCheck(false); 
        }
        

        //统一注册事件监听
        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<ulong>(LoginEvents.C_START_GAME_REQ, OnStartGameReq);
            EventDispatcher.AddEventListener<string, byte, byte>(LoginEvents.C_CREATE_ROLE, OnCreateRoleReq);
            EventDispatcher.AddEventListener<string>(RPCEvents.NetStatus, PlayerTimerManager.GetInstance().onNetStatusHandler);    //ServerProxy.cs抛出 
        }

        //统一注册事件监听
        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<ulong>(LoginEvents.C_START_GAME_REQ, OnStartGameReq);
            EventDispatcher.RemoveEventListener<string, byte, byte>(LoginEvents.C_CREATE_ROLE, OnCreateRoleReq);
            EventDispatcher.RemoveEventListener<string>(RPCEvents.NetStatus, PlayerTimerManager.GetInstance().onNetStatusHandler);    //ServerProxy.cs抛出 
        }

        //根据账号--跳转界面
        private void JumpWithAccount()
        {
            ServerListManager.StopTimer();  //停止servers.xml定时下载
            if (HasMaxCharacter())
            {
                LoggerHelper.Info("[账号首次登录游戏] 已创建角色已达4个上限");
                Jump();
            }
            else
            {
                if (HasCharacter())
                {
                    Jump();
                }
                else
                {
                    LoggerHelper.Info("[账号首次登录游戏] 到创角界面");
                    UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                    UIManager.Instance.ShowPanel(PanelIdEnum.CreateRole);
                }
            }
        }

        private void Jump()
        {
            bool hasRole = HasCharacter();
            if (LoginManager.Instance.isQuickLogin)
            {//快速进入,直接进入游戏请求
                LoggerHelper.Info("[快速进入] " + (hasRole ? "有创角记录" : "没有创角记录") + ",last_avatar_dbid:" + last_avatar_dbid);
                if (hasRole)
                {//有创角记录，取最近登录角色进入游戏
                    LocalSetting.settingData.SelectedAvatarDbid = last_avatar_dbid;
                    LocalSetting.Save();
                    OnStartGameReq(last_avatar_dbid);
                }
                else
                {//没有创角记录，跳到创角界面
                    UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                    UIManager.Instance.ShowPanel(PanelIdEnum.CreateRole);
                }
            }
            else
            {//普通登陆,请求获取账号的角色列表
                if (hasRole)
                {
                    LoggerHelper.Info("[普通登陆--有创角记录] 进入选角界面!");
                    UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
                    UIManager.Instance.ShowPanel(PanelIdEnum.SelectRole);
                }
                else
                {
                    LoggerHelper.Info("[普通登陆--没有创角记录] 进入创角界面!");
                    UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                    UIManager.Instance.ShowPanel(PanelIdEnum.CreateRole);
                }
            }
        }

        /// <summary>
        /// 返回玩家已有账号列表
        /// 1-1、true :玩家已有账号(非初次游戏)
        /// 1-1、false:玩家初次游戏
        /// </summary>
        /// <returns></returns>
        public bool HasCharacter()
        {
            return avatarList != null && avatarList.Count > 0;
        }

        /// <summary>
        /// 创建角色是否已达到上限值(不超过4个)
        /// </summary>
        /// <returns>[true:是,false:否]</returns>
        private bool HasMaxCharacter()
        {
            return avatarList != null && avatarList.Count >= 4;
        }

        #region 服务器响应监听
        private void OnMultiLogin()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.ServerLine);
            PlayerTimerManager.GetInstance().enableNetTimeoutCheck(false);
            //SystemAlert.Show(true, "顶号提示", "您的账号已在别处登录！", true);
            string txt1 = XMLManager.chinese[103050].__content;    //"顶号提示"
            string txt2 = XMLManager.chinese[103051].__content;    //"您的账号已在别处登录！"
            string txt3 = XMLManager.chinese[103022].__content;    //重启游戏
            //ari SystemAlert.Show(true, txt1, txt2, null, null, txt3, null, null, true, false);
            LoginManager.Instance.ResetLoginDefaultValue();                       //如果是在登录界面踢下线，往服务器发送的请求登陆状态：设置为可发送状态
        }

        private void HeartBeatResp()
        {
            if (GMState.showheartbeatinfo)
            {
                LoggerHelper.Info(string.Format("HeartBeatResp in PlayerAccount."));
            }
        }

        //角色列表返回
        private void OnCharaterInfoResp(Byte[] byteArr)
        {
            LoggerHelper.Debug("[账号角色信息的返回]");
            PbAvatarList list = MogoEngine.Utils.MogoProtoUtils.ParseProto(byteArr, typeof(PbAvatarList)) as PbAvatarList;
            List<PbAvatarInfo> avatars = list.avatars;
            
            avatarList.Clear();
            AvatarInfo avatarInfo = null;                              
            foreach(PbAvatarInfo item in avatars)
            {
                if (avatarList.ContainsKey(item.dbid))
                {
                    avatarInfo = avatarList[item.dbid];
                    avatarInfo.Name = MogoProtoUtils.ParseByteArrToString(item.name);
                    avatarInfo.Level = (int)item.level;
                    avatarInfo.Vocation = (int)item.vocation;
                    ModelEquipTools.ResetAvatarModelDataByString(avatarInfo.avatarModelData, item.model);  //解析已有角色的模型，武器，翅膀信息
                }
                else 
                {
                    avatarInfo = new AvatarInfo();
                    avatarInfo.DBID = item.dbid;
                    avatarInfo.Name = MogoProtoUtils.ParseByteArrToString(item.name);
                    avatarInfo.Level = (int)item.level;
                    avatarInfo.Vocation = (int)item.vocation;
                    ModelEquipTools.ResetAvatarModelDataByString(avatarInfo.avatarModelData, item.model);  //解析已有角色的模型，武器，翅膀信息
                    avatarList.Add(item.dbid, avatarInfo);
                }
            }
            JumpWithAccount();
        }

        // 创建角色返回
        private void OnCreateCharacterResp(ushort errorID, ulong avatarDbid)
        {
            LoggerHelper.Info("[创角结果] avatarDbid:" + avatarDbid + ",respCode:" + errorID);
            if (errorID == 0)
            {//创角成功
                UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
                UpdateLocalData(avatarDbid);
                OnStartGameReq(avatarDbid);
                //创建成功统计日志
                int serverId = LocalSetting.settingData.SelectedServerID;
                string serverName = ServerListManager.GetServerInfoByID(serverId).name;
                curRoleName = LocalSetting.settingData.SelectedCharacter;
                if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
                {
                    DockingService.SendCreateRoleLog(curRoleName);
                }
                PlatformSdkMgr.Instance.CreateRoleLog(avatarDbid.ToString(), curRoleName, serverId.ToString(), serverName);
                LoggerHelper.Info(string.Format("LogForCreateRole avatarDbid:{0},createRoleName:{1},serverId:{2},serverName:{3}", avatarDbid, curRoleName, serverId, serverName));
                #if UNITY_IPHONE && !UNITY_EDITOR
                    LoggerHelper.Info("[IOSPlugins] LogForCreateCharacter");
                    IOSPlugins.LogForCreateCharacter(avatarDbid.ToString(), serverId.ToString(), curRoleName);
                #endif
            }
            else
            {
                //LoggerHelper.Error("[创角失败] avatarDbid:" + avatarDbid + ",respCode:" + errorID);
                //MessageBox.Show(true, "", LoginManager.Instance.getRegisterTip(errorID));
                //ari EventDispatcher.TriggerEvent<ushort>(SelectRoleEvents.S_CREATE_ROLE_FAIL, errorID);
            }
        }

        //更新本地缓存数据
        private void UpdateLocalData(ulong dbid)
        {
            LocalSetting.settingData.SelectedAvatarDbid = dbid;
            int serverId = LocalSetting.settingData.SelectedServerID;
            List<int> serverList = LocalSetting.settingData.CreateRoleServerList;
            if (serverList == null)
            {//记录创角成功的服务器
                serverList = new List<int>();
                serverList.Add(serverId);
                LocalSetting.settingData.CreateRoleServerList = serverList;
            }
            else
            {
                if (!serverList.Contains(serverId)) serverList.Add(serverId);
            }
            LocalSetting.Save();
        }

        //角色随机名称返回
        private void OnRandomNameResp(string name)
        {
            //LoggerHelper.Info("RandomName Resp name:" + name);
            EventDispatcher.TriggerEvent<string>(LoginEvents.RANDOM_NAME_RESP, name);
        }

        private void OnCheckVersionResp(UInt16 errorID)
        { 
            
        }

        /// <summary>
        /// 账号--退出登陆结果返回
        /// </summary>
        private void ExitLoginResp(ushort respCode)
        {
            if (respCode == 0)
            { //退出登陆成功,返回登陆界面
                LoggerHelper.Info("退出登录成功");
                RemoveEventListener();
                GameStateManager.GetInstance().OnReturnLogin();
                NetSocketManager.GetInstance().isLoginNet = true;
                PlayerTimerManager.GetInstance().enableNetTimeoutCheck(false);
                ServerProxy.Instance.IsEnableReconnect(false);
                EventDispatcher.TriggerEvent(LoginEvents.RETURN_LOGIN);
                UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
                UIManager.Instance.ClosePanel(PanelIdEnum.ServerLine);  //关闭服务器排队界面
                //UIManager.Instance.ShowPanel(PanelIdEnum.Login);
            }
            else 
            {
                //ari SystemAlert.Show(true, "返回失败", "返回失败,错误码:" + respCode, true);
            }
        }

        //进入游戏返回
        private void OnEnterGameResp(UInt16 errorID)
        {
            try
            {
                LoggerHelper.Debug("请求进入游戏返回 respCode:" + errorID);
                PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);
                if (errorID == 0)
                {
                    UIManager.Instance.ClosePanel(PanelIdEnum.Login);
                    UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole);
                    UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
                    UIManager.Instance.ClosePanel(PanelIdEnum.ServerLine);

                    //统计进入游戏日志
                    //ServerProxy.Instance.IsEnableReconnect(true);
                    NetSocketManager.GetInstance().isLoginNet = false;
                    int serverId = LocalSetting.settingData.SelectedServerID;
                    string serverName = ServerListManager.GetServerInfoByID(serverId).name;
                    PlatformSdkMgr.Instance.EnterGameLog(serverId.ToString(), curRoleName, curAvatarDbid.ToString(), "1", serverName);
                    //开启SDK推送服务
                    PlatformSdkMgr.Instance.StartPushService(LoaderDriver.Instance.versionNo, LoginInfo.platformId_uid, null);
                    LoggerHelper.Info(string.Format("[SdkLoginOk] SdkPushService Start[OK] gameVersion:{0}", LoaderDriver.Instance.versionNo));
                }
                else
                {
                    system_info data = XMLManager.system_info.ContainsKey(errorID) ? XMLManager.system_info[errorID] : null;
              //ari SystemAlert.Show(true, "开始游戏失败", data != null ? data.__descript : "错误码:" + errorID, true);
                    LoginManager.Instance.ResetLoginDefaultValue();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        /// <summary>
        /// 登录排队提示返回
        /// </summary>
        /// <param name="curPos">当前排队位置</param>
        /// <param name="maxQueue">队伍长度</param>
        /// <param name="waitTime">预估排队时间，单位:秒</param>
        private void OnUpdateLoginQueue(uint curPos, uint maxQueue, uint waitTime)
        {
            LoggerHelper.Info(string.Format("登录排队返回 curPos:{0},maxQueue:{1},waitTime:{2}", curPos, maxQueue, waitTime));
            //uint[] data = new uint[] { curPos, maxQueue, waitTime };
            UIManager.Instance.ShowPanel(PanelIdEnum.ServerLine, new uint[] { curPos, maxQueue, waitTime });
            EventDispatcher.TriggerEvent<uint, uint, uint>(LoginEvents.REFRESH_SERVER_LINE_INFO, curPos, maxQueue, waitTime);
        }
        #endregion

        #region 向服务器发送请求
        /// <summary>
        /// 创建角色请求
        /// </summary>
        /// <param name="name">角色名称</param>
        /// <param name="sex">性别[1:男,2:女]</param>
        /// <param name="vocation">选择职业</param>
        private void OnCreateRoleReq(string name, byte sex, byte vocation)
        {
            LoggerHelper.Debug(string.Format("[创角请求] name:{0},sex:{1},carser:{2},connected:{3}", name, sex, vocation,ServerProxy.Instance.Connected));
            if (ServerProxy.Instance.Connected)
            {
                RpcCall("create_character_req", name, sex, vocation);
            }
            else
            {//提示服务器断开
                //EventDispatcher.TriggerEvent(RPCEvents.ReConnectFailed);  
            }
        }

        /// <summary>
        /// 进入游戏--请求
        /// 1、发送此协议表示账号已登录成功，用不同角色进入游戏请求
        /// </summary>
        /// <param name="avatarDbid">角色的dbid,传0:表示快速进入,>0:表示用已有角色进入</param>
        private void OnStartGameReq(ulong avatarDbid)
        {
            curAvatarDbid = avatarDbid;
            AvatarInfo avatarInfo=avatarList.ContainsKey(avatarDbid)?avatarList[avatarDbid]:null;
            curRoleName = avatarInfo != null ? avatarInfo.Name : "";
            LoggerHelper.Info(string.Format("开始游戏请求 avatarDbid:{0},avatarName:{1},connected:{2},avatarInfo:{3}", avatarDbid, curRoleName, ServerProxy.Instance.Connected, avatarInfo));

            if (avatarInfo != null)
            {//非创角情况
                string strLevel = avatarInfo.Level.ToString();
                PlatformSdkMgr.Instance.SelectServerLog(strLevel, curRoleName, LocalSetting.settingData.SelectedServerID.ToString());
                LoggerHelper.Info(string.Format("LogForSelectServer uid:{0},serverId:{1},roleName:{2},roleLevel:{3}", LoginInfo.platformUid, LocalSetting.settingData.SelectedServerID, curRoleName, strLevel));
                #if UNITY_IPHONE && !UNITY_EDITOR
                    IOSPlugins.LogForSelectServer(LoginInfo.platformUid, LocalSetting.settingData.SelectedServerID.ToString(), curRoleName, strLevel);
                #endif
            }

            if (ServerProxy.Instance.Connected)
            {
                RpcCall("start_game_req", avatarDbid);
            }
            else
            {//提示服务器断开
                //EventDispatcher.TriggerEvent(RPCEvents.ReConnectFailed);
            }
        }

        // 获取当前账号的角色列表
        //1、如果获取成功后端通过属性(avatars_info)同步角色列表
        private void GetAvatarList()
        {
            RpcCall("charater_info_req");
        }

        /// <summary>
        /// 随机名称请求
        /// </summary>
        /// <param name="vocation">职业</param>
        public void RandomNameReq(int vocation)
        {
            if (ServerProxy.Instance.Connected)
            {
                RpcCall("random_name_req", vocation);
            }
            else
            {//提示服务器断开
                //EventDispatcher.TriggerEvent(RPCEvents.ReConnectFailed);
            }
        }

        /// <summary>
        /// 账号--退出登陆请求
        /// </summary>
        public void ExitLoginReq()
        {
            if (ServerProxy.Instance.Connected)
            {
                RpcCall("exit_login_req");
            }
            else
            {//提示服务器断开
                //EventDispatcher.TriggerEvent(RPCEvents.ReConnectFailed);
            }
        }

       


        /// <summary>
        /// 发送用户设备信息
        /// </summary>
        private void SendMobileInfo()
        {
            //MOBILE_GAME_VERSION        =   1,--游戏版本号
            //MOBILE_DID                 =   2,--用户设备ID
            //MOBILE_OS                  =   3,--手机操作系统(android,ios)
            //MOBILE_OS_VERSION          =   4,--操作系统版本号(4.4.4)
            //MOBILE_DEVICE              =   5,--设备名称,如：三星GT-S5830
            //MOBILE_DEVICE_TYPE         =   6,--设备类型，如：xiaomi
            //MOBILE_SCREEN              =   7,--屏幕分辨率(1920*1028)
            //MOBILE_MNO                 =   8,--移动网络运营商(中国移动、中国联通)
            //MOBILE_NM                  =   9,--联网方式(2g,3g)
            string[] deviceInfo = PlatformSdkMgr.Instance.GetDeviceInfo();
            LuaTable data = new LuaTable();
            string osPlatformName = "unknown";
            if (Application.platform == RuntimePlatform.Android) osPlatformName = "android";
            else if (Application.platform == RuntimePlatform.IPhonePlayer) osPlatformName = "ios";
            else if (Application.platform == RuntimePlatform.WindowsWebPlayer) osPlatformName = "web";
            else if (Application.platform == RuntimePlatform.WindowsPlayer) osPlatformName = "pc";

            if (deviceInfo != null)
            {
                data.Add(1, LoaderDriver.Instance.versionNo);                                       //游戏版本号
                data.Add(2, deviceInfo[0]);                                                         //设备ID
                data.Add(3, osPlatformName);                                                        //操作系统(android,ios)
                data.Add(6, deviceInfo[1]);                                                         //设备类型(xiaomi,samsung)
                data.Add(5, deviceInfo[2]);                                                         //设备名称,如：三星GT-S5830,MI 2S
                data.Add(4, deviceInfo[3]);                                                         //操作系统版本号(4.4.4)
                data.Add(7, deviceInfo[4]);                                                         //屏幕分辨率(1280*720)
                data.Add(8, deviceInfo[5]);                                                         //移动网络运营商（中国移动、中国联通）
                data.Add(9, deviceInfo[6]);                                                         //联网方式(3G、WiFi)
            }
            else
            {//PC或Web
                data.Add(1, LoaderDriver.Instance.versionNo);                                       //游戏版本号
                data.Add(2, "");                                                                    //设备ID
                data.Add(3, osPlatformName);                                                        //操作系统(android,ios)
                data.Add(6, "");                                                                    //设备类型(xiaomi,samsung)
                data.Add(5, "");                                                                    //设备名称,如：三星GT-S5830,MI 2S
                data.Add(4, "");                                                                    //操作系统版本号(4.4.4)
                data.Add(7, "");                                                                    //屏幕分辨率(1280*720)
                data.Add(8, "");                                                                    //移动网络运营商（中国移动、中国联通）
                data.Add(9, "");
            }
            LoggerHelper.Info("==== Send DeviceInfo ====");
            LoggerHelper.Info("data[1]游戏版本号:" + data["1"]);
            LoggerHelper.Info("data[2]用户设备ID:" + data["2"]);
            LoggerHelper.Info("data[3]设备操作系统:" + data["3"]);
            LoggerHelper.Info("data[4]操作系统版本号:" + data["4"]);
            LoggerHelper.Info("data[5]设备名称:" + data["5"]);
            LoggerHelper.Info("data[6]设备类型:" + data["6"]);
            LoggerHelper.Info("data[7]屏幕分辨率:" + data["7"]);
            LoggerHelper.Info("data[8]移动网络运营商:" + data["8"]);
            LoggerHelper.Info("data[9]联网方式:" + data["9"]);
            RpcCall("mobile_info_req", data);
        }
        #endregion 
    }
}
