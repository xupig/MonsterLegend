﻿
using ACTSystem;
using Common.ClientConfig;
using Common.Events;
using GameData;
using GameLoader.Utils;
using MogoEngine;
using MogoEngine.Events;
using UnityEngine;

namespace GameMain.Entities
{
    public class EntityClientDropItem : EntityDropItemBase
    {
        public const int PUBLIC_DROP = -1;

        public bool picked = false;
        public int itemCount { get; set; }
        public uint belongAvatar { get; set; }
        public int pickType { get; set; }

        public bool isServerItem = false;
        float pickDelayTime = 3f;

        public EntityClientDropItem()
            : base()
        {
            entityType = EntityConfig.ENTITY_TYPE_NAME_CLIENT_DROP_ITEM;
            pickDelayTime = int.Parse(global_params_helper.GetGlobalParam(114)) * 0.001f;
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            DestroyModel();
        }

        protected override void OnModelLoaded()
        {
            base.OnModelLoaded();
            if (isInWorld)
            {
                dropItem.pickType = pickType;
                //if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityClientDropItem(this), dropItem.gameObject);
            }
        }
 
        protected override void Update()
        {
            if (picked) return;
            if (belongAvatar == 0)
            {
                CheckingDistance();
            }
            else if (belongAvatar == PlayerAvatar.Player.id)
            {
                CheckingDelayTime();
            }
        }

        int spaceTime = 0;
        void CheckingDistance()
        {
            if (spaceTime > 0) { spaceTime--; return; }
            spaceTime = 5;
            var dis = (PlayerAvatar.Player.position - this.position).magnitude;
            if (dis < 1.5f)
            {
                Pick();
            }
        }

        void CheckingDelayTime()
        {
            pickDelayTime -= Time.deltaTime;
            if (pickDelayTime <= 0)
            {
                Pick();
            }
        }

        float canPickTime = 0;
        public override void Pick()
        {
            if (isServerItem)
            {
                if (canPickTime > Time.realtimeSinceStartup) return;
                PlayerAvatar.Player.RpcCall("pick_drop_req", this.id);
                canPickTime = Time.realtimeSinceStartup + 5;
            }
            else
            {
                PickConfirm();
            }
        }

        public void PickConfirm()
        {
            picked = true;
            if (belongAvatar > 0)
            {
                ACTVisualFXManager.GetInstance().PlayACTTracer(drop_item_helper.GetClientDropItemFxID(), drop_item_helper.GetClientDropItemFxSpeed(), this.position, PlayerAvatar.Player.actor, new Vector3(0,1,0));
            }
            MogoWorld.DestroyEntity(this.id);
            EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_DROP, this.id.ToString() + ":" + this.type_id.ToString());
        }
    }

}
