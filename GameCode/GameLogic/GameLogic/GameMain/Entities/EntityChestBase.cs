﻿using ACTSystem;
using Common.Events;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using UnityEngine;
using Common.ExtendTools;

namespace GameMain.Entities
{
    public class EntityChestBase : EntityItemBase
    {
        #region def属性
        public byte chest_id { get; set; }
        #endregion

        public EntityChestBase()
        {

        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            DestroyModel();
        }

        public override void OnEnterSpace()
        {
            CheckIsPickUp();
        }

        protected virtual void CheckIsPickUp()
        {

        }

        public override void OnLeaveSpace()
        {
        }

        public GameObject chestModel;
        protected bool _modelLoaded = false;
        public void CreateModel()
        {
            if (this._modelLoaded) return;
            this._modelLoaded = true;
            string[] resourcePaths = GetResourcePaths();
            if (resourcePaths != null)
            {
                Game.Asset.ObjectPool.Instance.GetGameObjects(resourcePaths, (objs) =>
                {
                    var model = objs[0];
                    if (model == null)
                    {
                        LoggerHelper.Error(resourcePaths[0] + " load error! ");
                        return;
                    }
                    ResetModelSize(model);
                    if (objs.Length >= 2 && objs[1] != null)
                    {
                        var effect = objs[1];
                        effect.transform.SetParent(model.transform, false);
                    }
                    chestModel = model;
                    OnModelLoaded();
                });
            }
            else
            {
                LoggerHelper.Error("ChestId " + chest_id + " does not have a match modelPath");
            } 
        }

        private void ResetModelSize(GameObject model)
        {
            float size = GameData.chest_data_helper.GetModelSize((int)chest_id);
            model.transform.localScale = Vector3.one * size;
        }

        private string[] GetResourcePaths()
        {
            string modelPath = GameData.chest_data_helper.GetDropModelPath((int)chest_id);
            string effectPath = GameData.chest_data_helper.GetDropModelEffectPath((int)chest_id);
            if (!string.IsNullOrEmpty(modelPath))
            {
                if (!string.IsNullOrEmpty(effectPath))
                {
                    return new string[] { modelPath, effectPath };
                }
                else
                {
                    return new string[] { modelPath };
                }
            }
            return null;
        }

        public override Transform GetTransform()
        {
            return chestModel.transform;
        }

        private void OnModelLoaded()
        {
            if (isInWorld)
            {
                chestModel.name = string.Format("{0}-{1}-{2}", entityType, id, chest_id);
                FixPosition();
                this.SetPosition(chestModel.transform.position);
                AddBoxCollider();
                ShowBillboard();
            }
            else
            {
                DestroyModel();
            }
        }

        public void DestroyModel()
        {
            if (chestModel != null)
            {
                HideBillboard();
                GameObject.Destroy(chestModel);
                chestModel = null;
            }
        }

        private void ShowBillboard()
        {
            CreatureDataManager.GetInstance().AddEntityID(id);
        }

        private void HideBillboard()
        {
            CreatureDataManager.GetInstance().RemoveEntityID(id);
        }
    }
}
