﻿#region 模块信息
/*==========================================
// 模块名：EntityVehicle
// 命名空间: GameMain.Entities
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/16/11
// 描述说明：地面载具
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.Events;
using GameData;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.Entities
{
    public class EntityGroundVehicle : EntityVehicle
    {
        public EntityGroundVehicle()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_GROUND_VEHICLE;
        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            var accordingMode = AccordingMode.AccordingStick;
            if (PlayerTouchBattleModeManager.GetInstance().CurrentCanTouch())
            {
                accordingMode = AccordingMode.AccordingTouch;
            }
            else
            {
                if (rotateSpeed > 0)
                {
                    accordingMode = AccordingMode.AccordingSteeringWheel;
                }
            }
            moveManager.accordingMode = accordingMode;
            moveManager.defaultAccordingMode = accordingMode;
            entityAI = null;
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            
        }

        public override void OnEnterSpace()
        {
            GameSceneManager.GetInstance().Vehicle = this;
            base.OnEnterSpace();
        }

        public override void OnLeaveSpace()
        {
            GameSceneManager.GetInstance().Vehicle = null;
            base.OnLeaveSpace();
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            //临时设置坐标
            //actor.position = PlayerAvatar.Player.position;

            if (actor != null)
            {
                this.actor.gameObject.name = "Vehicle" + "-" + this.id + "-" + this.monster_id;
                SetActorLayer(driver.GetActorLayer());
                driver.DriveVehicle(this);
                driver.vehicleManager.AddVehicleEventListener(this);
                //切换主相机
                //CameraManager.GetInstance().SetFollowingTarget(actor.transform);
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityVehicle(this), this.actor.gameObject);
            }
        }

        public override void OnDeath()
        {
            base.OnDeath();
            //移除骑乘者身上的载具Buffer
            driver.vehicleManager.RequestDivorce();
        }

        public override int GetSpaceQualitySetting()
        {
            return driver.GetSpaceQualitySetting();
        }

        public override bool modelNormalQuality
        {
            get
            {
                return driver.modelNormalQuality;
            }
        }
    }
}
