﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.States;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Config;
using GameLoader.PlatformSdk;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using GameMain.RemoteLoader;

using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using MogoEngine.Utils;
using SpaceSystem;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace GameMain.Entities
{
    public partial class PlayerAvatar : EntityAvatar
    {
        private int rolePreLevel = 0;           //主角前一个等级值
        private bool isShowPlatform = false;    //平台SDK是否已初始化状态
        private uint _onlineTimeId = 0;
        #region OnEnterWorld OnLeaveWorld

        public override void OnEnterWorld()
        {
            if (_onlineTimeId != 0)
            {
                _onlineTimeId = 0;
            }
            ShortcutKeyManager.onLine = true;
            base.OnEnterWorld();
            var accordingMode = CombatSystem.AccordingMode.AccordingStick;
            if (platform_helper.InPCPlatform())
            {
                accordingMode = CombatSystem.AccordingMode.AccordingTouch;
            }
            this.moveManager.accordingMode = accordingMode;
            this.moveManager.defaultAccordingMode = accordingMode;
            //Debug.LogError("OnEnterWorld:state_cell:" + this.state_cell.Length);
            //actionManager = new PlayerActionManager();
            PerformaceManager.GetInstance().OnPlayerEnterWorld();
            PlayerActionManager.GetInstance().OnPlayerEnterWorld();
            PlayerCommandManager.GetInstance().OnPlayerEnterWorld();
            PlayerSkillManager.GetInstance().OnPlayerEnterWorld();
            PlayerAttrManager.GetInstance().OnPlayerEnterWorld();
            PlayerTimerManager.GetInstance().SetSyncTimeCallback(PlayerDataManager.Instance.RequestDataAfterSyncTime);
            PlayerTimerManager.GetInstance().OnPlayerEnterWorld();
            PlayerDataManager.Instance.RequestInitData();
            PlayerSprintManager.Instance.OnPlayerEnterWorld(this);
            PreloadManager.GetInstance().OnPlayerEnterWorld();
            NPCManager.GetInstance().OnPlayerEnterWorld();
            TreasureManager.Instance.OnPlayerEnterWorld();
            DummyManager.GetInstance().OnPlayerEnterWorld();
            PlayerCameraFeatureManager.Instance.OnPlayerEnterWorld();
            PlayerStandByManager.Instance.OnPlayerEnterWorld();
            BackgroundDownloadManager.GetInstance().OnPlayerEnterWorld();
            PlayerTouchBattleModeManager.GetInstance().OnPlayerEnterWorld();
            PlayerTaskFindMonsterManager.GetInstance().OnPlayerEnterWorld();
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                _onlineTimeId = TimerHeap.AddTimer(300 * 1000, 300 * 1000, OnLineSendLog);
            }
            GameRenderManager.GetInstance();
            DramaTrigger.Start();
            ShowPlatform();
            SendInviteCode();
            SendAntiAddictionMessage(); //发送防沉迷信息
            CheckNeedShowAntiAddiction();
            ReloadXmlManager.GetInstance().CheckHotUpdate();
        }

        private void OnLineSendLog()
        {
            DockingService.SendOnlineLog(WebLoginInfoManager.GetInstance().cuuOnlineFlag);
        }

        private const int NEED_REGISTER_INFO = 2;
        private void CheckNeedShowAntiAddiction()
        {
            if (platform_helper.InPCPlatform())
            {
                WindowsPlatformLoginData data = new WindowsPlatformLoginData();
                string cm = "2";
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    cm = LoginManager.Instance.GetWindowsPlatformLoginData().cm;
                }
                else
                {
                    cm = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData().cm;
                }
                if (int.Parse(cm) == NEED_REGISTER_INFO)
                {
                    AntiDataWrapper wrapper = new AntiDataWrapper();
                    wrapper.type = AntiPanelType.Login;
                    UIManager.Instance.ShowPanel(PanelIdEnum.AntiAddiction, wrapper);
                }
            }
        }

        /// <summary>
        /// 发送防沉迷信息
        /// </summary>
        private void SendAntiAddictionMessage()
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                WindowsPlatformLoginData _windowsPlatformLoginData = new WindowsPlatformLoginData();

                _windowsPlatformLoginData = LoginManager.Instance.GetWindowsPlatformLoginData();
                if (string.IsNullOrEmpty(_windowsPlatformLoginData.cm))
                {
                    LoggerHelper.Info("cm is null");
                    return;
                }
                LoggerHelper.Info("111:" + _windowsPlatformLoginData.cm + ":" + UInt16.Parse(_windowsPlatformLoginData.cm));
                RpcCall("anti_addiction_req", UInt16.Parse(_windowsPlatformLoginData.cm), 0);
            }
            else if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                WindowsWebPlatformLoginData _webPlatformLoginData = new WindowsWebPlatformLoginData();
                _webPlatformLoginData = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData();

                if (string.IsNullOrEmpty(_webPlatformLoginData.cm))
                {
                    LoggerHelper.Info("cm is null");
                    return;
                }
                RpcCall("anti_addiction_req", UInt16.Parse(_webPlatformLoginData.cm), 0);
            }
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            PerformaceManager.GetInstance().OnPlayerLeaveWorld();
            PlayerCommandManager.GetInstance().OnPlayerLeaveWorld();
            PlayerActionManager.GetInstance().OnPlayerLeaveWorld();
            PlayerSkillManager.GetInstance().OnPlayerLeaveWorld();
            PlayerAttrManager.GetInstance().OnPlayerLeaveWorld();
            PlayerTimerManager.GetInstance().OnPlayerLeaveWorld();
            PlayerCameraFeatureManager.Instance.OnPlayerLeaveWorld();
            PlayerDataManager.Instance.DestoryData();
            PlayerSprintManager.Instance.OnPlayerLeaveWorld(this);
            PreloadManager.GetInstance().OnPlayerLeaveWorld();
            NPCManager.GetInstance().OnPlayerLeaveWorld();
            TreasureManager.Instance.OnPlayerLeaveWorld();
            DummyManager.GetInstance().OnPlayerLeaveWorld();
            PlayerStandByManager.Instance.OnPlayerLeaveWorld();
            PlayerSoundManager.Instance.OnPlayerLeaveWorld();
            BackgroundDownloadManager.GetInstance().OnPlayerLeaveWorld();
            PlayerTouchBattleModeManager.GetInstance().OnPlayerLeaveWorld();
            PlayerTaskFindMonsterManager.GetInstance().OnPlayerLeaveWorld();
        }

        public override void OnEnterSpace()
        {
            base.OnEnterSpace();
            ResetCityState();
            PlayerStandByManager.Instance.OnPlayerEnterSpace();
            PlayerTouchBattleModeManager.GetInstance().OnPlayerEnterScene();
            _qualitySettingValue = GetSpaceQualitySetting();
            UpdateTouchEnabled();
            UpdateEntityComponents();
            //rideManager.OnEnterSpace();
            // PlayerTimerManager.GetInstance().HeartBeat();
        }

        public override void OnLeaveSpace()
        {
            PlayerStandByManager.Instance.OnPlayerLeaveSpace();
            PlayerTouchBattleModeManager.GetInstance().OnPlayerLeaveScene();
            rideManager.OnPlayerLeaveSpace();
            base.OnLeaveSpace();
        }

        #endregion

        #region OnActorLoaded

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (this.actor != null)
            {
                UnityEngine.Object.DontDestroyOnLoad(this.actor.gameObject);
                //CameraManager.GetInstance().SetFollowingTarget(this.actor.transform);
                CameraManager.GetInstance().SetFollowingTarget(this.actor.boneController.GetBoneByName("slot_camera"));
                LoginMusic.GetInstance().RemoveAudioListener();
                this.actor.actorController.alwaysCheckGround = true;
                this.actor.actorController.usePlayerController = true;
                this.actor.gameObject.AddComponent<AudioListener>();
                InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingPlayerAvatar(this), this.actor.gameObject);
                this.actor.equipController.equipCloth.isShelter = true;
                PlayerSoundManager.Instance.OnActorLoaded();
                InitFindPathFx();
            }
        }

        protected override void OnControllerLoaded()
        {
            if (this.actor != null)
            {
                this.actor.animationController.ReturnReady();
            }
            base.OnControllerLoaded();
        }

        #endregion

        #region OnEnterSpaceResp
        public void OnEnterSpaceResp(UInt32 mapId, UInt16 line, Int16 x, Int16 y, byte face)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
                face = byte.Parse(face.ToString()); 
#endif
            LoggerHelper.Info(string.Format("[OnEnterSpaceResp 场景跳转] mapId:{0} line:{1}", mapId, line));
            //切换场景
            EventDispatcher.TriggerEvent(VehicleEvents.CHANGE_SCENE);
            DramaManager.GetInstance().currentShowOtherEntities = true;
            _elevatorState = false;
            Vector3 pos = new Vector3(x / MogoEngine.RPC.RPCConfig.POSITION_SCALE, 0, y / MogoEngine.RPC.RPCConfig.POSITION_SCALE);
            bool changeLine = this.curLine != line;
            this.curLine = line;
            Action SceneLoaded = () =>
            {//场景资源加载完成的回调

                this.SetPosition(new Vector3(x / MogoEngine.RPC.RPCConfig.POSITION_SCALE, GameSceneManager.GetInstance().GetCurEnterPoint().y, y / MogoEngine.RPC.RPCConfig.POSITION_SCALE));
                if (x == 0 && y == 0)
                {
                    this.SetPosition(GameSceneManager.GetInstance().GetCurEnterPoint());
                    pos = GameSceneManager.GetInstance().GetCurEnterPoint();
                }
                if (this.actor != null)
                {
                    this.actor.position = this.position;
                    this.face = face;
                }

                PlayerDataManager.Instance.BattlePetData.EnterCombatScene();
                PlayerDataManager.Instance.TeamInstanceData.ResetHpMember();
                PlayerDataManager.Instance.InstanceAvatarData.ResetHpMember();
                SpaceLeftTimeManager.Instance.InitSpaceLeftTime();
                EventDispatcher.TriggerEvent(SceneEvents.SCENE_LOADED);

                //重置副本三星信息
                if (map_helper.GetStandardWay((int)mapId) == MapStandardWay.Star)
                {
                    PlayerDataManager.Instance.PlayingCopyStarData.ResetCopyStar();
                }

                //重置要显示的模型类型
                CreatureDataManager.GetInstance().ResetEntityTypeList();
                //由于玩家自己触发事件在此之前，所以在这里重新加下
                CreatureDataManager.GetInstance().AddEntityID(PlayerAvatar.Player.id);
                //避免频繁发送退出协议
                PlayerAvatar.Player.ClearWait((int)action_config.ACTION_QUIT_MISSION);

                NPCManager.GetInstance().OnPlayerInitPosition();
                if (GameSceneManager.GetInstance().curMapID == 1)
                {//在主城，开启后台导出
                    if (StreamingAssetsExportMgr.Instance != null) StreamingAssetsExportMgr.Instance.StartExport();
                }
            };

            //开始切换场景时暂停后台导出
            if (StreamingAssetsExportMgr.Instance != null) StreamingAssetsExportMgr.Instance.PauseExport();
            GameSceneManager.GetInstance().EnterSceneByID((int)mapId, changeLine, pos, SceneLoaded);
        }

        public void SpaceEndTimeResp(UInt32 endTime)
        {
            SpaceLeftTimeManager.Instance.SpaceEndTimeResp(endTime);
        }
        #endregion

        #region
        public void GameLevelResp(byte avatarMostLevel, byte serverLevel)
        {
            PlayerAvatar.Player.serverLevel = serverLevel;
            PlayerAvatar.Player.avatarMostLevel = avatarMostLevel;
        }
        #endregion


        #region 关卡副本系统服务器->客户端的回应
        public void OnMissionResp(UInt32 actionID, UInt32 errorID, byte[] byteArr)
        {
            MissionManager.Instance.ResponseMission(actionID, errorID, byteArr);
        }
        #endregion

        #region 平台SDK相关
        //显示平台,下一次体力满通知开启
        private void ShowPlatform()
        {
            //启动推送信息监听，选服成功统计，显示小助手,下一次体力满通知开启
            if (isShowPlatform) return;
            isShowPlatform = true;
            TimerHeap.AddTimer(1000, 60 * 1000, OnMaxEnergyCheck);
            #if UNITY_IPHONE && !UNITY_EDITOR
                IOSPlugins.LogAfterEnterGame(LoginInfo.platformUid);
                EventDispatcher.AddEventListener<string> ("OnAstarEvent",OnAstarDialogEvent);
            #endif
            PlatformSdkMgr.Instance.maxEnergyHandler = OnMaxEnergyCheck;  //体力推送回调处理
            PlatformSdkMgr.Instance.ShowFloatBar();
            if (RemoteLoaderMgr.Instance == null) RemoteLoaderMgr.Instance = new RemoteLoaderMgr();
            if (StreamingAssetsExportMgr.Instance == null) StreamingAssetsExportMgr.Instance = new StreamingAssetsExportMgr();
            WelfareActiveManager.Instance.GetInviteCodeReq();  //请求获取邀请码
        }

        private void InitNotificationData()
        {
            PlayerDataManager.Instance.SettingData.SetupNotificationData();
        }

        //发送邀请码
        private void SendInviteCode()
        {
            string code = WelfareActiveManager.Instance.inputInviteCode;
            if (!string.IsNullOrEmpty(code))
            {
                LoggerHelper.Debug("向服务器发送使用邀请码请求:" + code);
                WelfareActiveManager.Instance.UseInviteCodeReq(code);
                WelfareActiveManager.Instance.inputInviteCode = null;
            }
        }

        //体力是否满检查
        private void OnMaxEnergyCheck()
        {
            //PlatformSdkMgr.Instance.addNotification(20 * 1000); //每10秒后推送一次(做测试用)
            /*int interval = EnergyData.dataMap[1].recoverInterval;
            uint maxEnergy = MogoWorld.thePlayer.maxEnergy;
            uint energy = MogoWorld.thePlayer.energy;
            if (energy >= maxEnergy)
            {
                LoggerHelper.Info("[SDK]energy if full");
                return;
            }

            long needTime = (maxEnergy - energy) * interval * 60 * 1000;

            int tipsTime = MogoTime.Instance.GetSecond() + (int)needTime;
            DateTime dt = MogoTime.Instance.GetDateTimeBySecond(tipsTime);
            int nowTime = dt.Hour*60 + dt.Minute;
            if (nowTime >= m_ShowEnergyTipsTime[0] && nowTime <= m_ShowEnergyTipsTime[1])
            {
                PlatformSdkMgr.Instance.addNotification((int)needTime);
                LoggerHelper.Info("[SDK]energy show tips --- needTime:" + needTime);
            }
            else
            {
                LoggerHelper.Info("[SDK]out of time to show energy --- needTime:" + needTime);
            }*/
        }

        /// <summary>
        /// 记录本角色升级日志给SDK
        /// </summary>
        /// <param name="preLevel">升级前的等级值</param>
        public void RecordUpLevelLogToSdk()
        {
            if (rolePreLevel != level)
            {//主角等级有变化时才记录升级日志
                if (rolePreLevel > 0)
                {
                    PlatformSdkMgr.Instance.RoleLevelLog(level, LocalSetting.settingData.SelectedServerID);
                    #if UNITY_IPHONE && !UNITY_EDITOR
                        LoggerHelper.Info(string.Format("[IOSPlugins] LogForCharacterLevelUp level:{0},serverId:{1}", level, LocalSetting.settingData.SelectedServerID));
                        IOSPlugins.LogForCharacterLevelUp(LoginInfo.platformId_uid, LocalSetting.settingData.SelectedServerID.ToString(), level.ToString());
                    #endif
                    showAstarHaoPing(LocalName.AstarPJ_RoleLevelUp);
                }
                rolePreLevel = level;
            }
        }
        #endregion

        #region IOS下五星评价功能
        /// <summary>
        /// IOS平台下--弹出五星好评界面
        /// </summary>
        /// <param name="localName">好评玩法存储到本地ID</param>
        /// <param name="btnTexts">评价选项按钮文本组，采用,分割</param>
        private LocalName localName=LocalName.AstarPJ_RoleLevelUp;
        public void showAstarHaoPing(LocalName localName)
        {
            //五星好评开启推送等级
            int openLevel = XMLManager.global_params.ContainsKey(256) ? int.Parse(XMLManager.global_params[256].__value) : 30;
            LoggerHelper.Info(string.Format("HaoPing OpenLevel:{0}", openLevel));
            if (level <= openLevel) return; 
            #if UNITY_IPHONE && !UNITY_EDITOR
			try 
            {
				//data数据格式：日期,评价选项(1:我要吐槽,2:下次再说,3:现在就去)
                List<string> data = Common.LocalCache.Read<List<string>>(localName);
				LoggerHelper.Info (string.Format ("localName:{0},data:{1}", localName, data));
                
				string title = "给我们平分";
				string message = "喜欢奇迹破坏神就给我们好评吧！\n努力为您呈上更精彩的玩法！";
				string btnTexts = "现在就去,我要吐槽,下次再说";
                string[] arry = data != null && data.Count > 0 ? data[0].Split(',') : null;
				this.localName = localName;
				if (arry != null && arry.Length > 1) 
                {//已有评价
					string pjId = arry [1];
					if (pjId != "3") 
                    {
						TimeSpan t = DateTime.Now - DateTime.Parse (arry [0]);
						LoggerHelper.Info (string.Format ("cur day;{0},need days:5", t.TotalDays));
						if (t.TotalDays > 5) 
                        {
							LoggerHelper.Info ("Show AstarHaoPing");
							IOSPlugins.showAstarDialog (title, message, btnTexts);
						}
					}
				} 
                else 
                {//未评价
					LoggerHelper.Info ("Show AstarHaoPing");
					IOSPlugins.showAstarDialog (title, message, btnTexts);
				}
			}
            catch (Exception ex) 
            {
				LoggerHelper.Error (string.Format ("mesage:{0},reason:{1}", ex.Message, ex.StackTrace));
			}
            #endif
        }

        public void OnAstarDialogEvent(string result)
        {
            if (result.Equals("1"))
            { //1:我要吐槽
                string strQQ = Common.Utils.MogoLanguageUtil.GetContent(73013);
                string strTelephone = Common.Utils.MogoLanguageUtil.GetContent(73014);
                //ari ModuleCommonUI.MessageBox.Show(true, "联系方式", strQQ + "\n\n" + strTelephone);
            }
            List<string> list = new List<string>();
            string content = string.Concat(DateTime.Now.ToString("yyyy-MM-dd"), ",", result); 
            list.Add(content);
            Common.LocalCache.Write(localName, list, Common.Utils.CacheLevel.Permanent);
            LoggerHelper.Info(string.Format("has write local localName:{0},content:{1}", localName, content));
        }
        #endregion


        #region 登陆相关
        public void OnMultiLogin()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.ServerLine);
            PlayerTimerManager.GetInstance().enableNetTimeoutCheck(false);
            string txt1 = XMLManager.chinese[103050].__content;    //"顶号提示"
            string txt2 = XMLManager.chinese[103051].__content;    //"您的账号已在别处登录！"
            string txt3 = XMLManager.chinese[103022].__content;    //重启游戏
      //ari SystemAlert.Show(true, txt1, txt2, null, null, txt3, null, null, true, false);
            LoginManager.Instance.ResetLoginDefaultValue();   //如果是在登录界面踢下线，往服务器发送的请求登陆状态：设置为可发送状态
        }

        public void CrossResp(string ip, UInt16 port, string key)
        {
            EventDispatcher.TriggerEvent<string, int, string>(RPCEvents.BaseLogin, ip, port, key);
        }

        public void TickOut()
        {
            EventDispatcher.TriggerEvent(LoginEvents.ON_KICK_OUT);
        }

        #endregion

        #region 代币

        public void OnTokenShopResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            //PbTokenShop tokenShop = MogoProtoUtils.ParseProto<PbTokenShop>(bytes);
            TokenManager.Instance.OnTokenShopResp(actionId, errorId, bytes);
        }

        #endregion

        #region 组队 TeamManager

        public void TeamRefresh(UInt32 teamId, byte teamState, UInt64 captainId, byte[] bytes)
        {
            TeamManager.Instance.TeamRefresh(teamId, teamState, captainId, bytes);
        }

        //<Arg> UINT8 </Arg>     <!--原因:1 自己退出 2 被队长踢3掉线超过5分钟后销毁玩家时退出4队伍解散 -->
        public void TeamQuit(byte reason)
        {
            TeamManager.Instance.TeamQuit(reason);
        }

        public void TeamReceiveInvite(UInt32 teamId, UInt64 inviterId, string inviterName, byte inviterLevel, byte inviterVocation, string captainName, byte gender, UInt32 ins_id)
        {
            TeamManager.Instance.TeamReceiveInvite(teamId, inviterId, inviterName, inviterLevel, (Vocation)inviterVocation, captainName, (Gender)gender, (int)ins_id);
        }

        public void TeamLeaderReceiveInvite(UInt32 teamId, UInt64 inviterId, string inviterName, UInt64 inviteeId, string inviteeName, byte inviteeLevel, byte inviteeVocation, byte gender, UInt32 ins_id)
        {
            TeamManager.Instance.TeamLeaderReceiveInvite(teamId, inviterId, inviterName, inviteeId, inviteeName, inviteeLevel, (Vocation)inviteeVocation, (Gender)gender, (int)ins_id);
        }

        public void TeamLeaderReceiveApply(UInt32 teamId, UInt64 applicantId, string applicantName, byte applicantLevel, byte applicantVocation, byte gender)
        {
            TeamManager.Instance.TeamLeaderReceiveApply(teamId, applicantId, applicantName, applicantLevel, (Vocation)applicantVocation, (Gender)gender);
        }


        public void TeamNearPlayers(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            TeamManager.Instance.TeamNearPlayers(currentPage, totalPage, bytes);
        }

        public void TeamNearTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            TeamManager.Instance.TeamNearTeams(currentPage, totalPage, bytes);
        }

        public void TeamFriendTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            TeamManager.Instance.TeamFriendTeams(currentPage, totalPage, bytes);
        }

        public void TeamGuildTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            TeamManager.Instance.TeamGuildTeams(currentPage, totalPage, bytes);
        }

        public void TeamReceiveStart(UInt32 instId, byte type)
        {
            TeamManager.Instance.TeamReceiveStart(instId, type);
        }

        public void TeamReceiveAgree(UInt64 dbId, byte state, UInt32 instId)
        {
            TeamManager.Instance.TeamReceiveAgree(dbId, state, instId);
        }

        public void TeamReceiveCall(UInt64 dbid)
        {
            TeamManager.Instance.TeamReceiveCall(dbid);
        }

        public void TeamRefreshOnline(UInt64 id, byte online)
        {
            TeamManager.Instance.TeamRefreshOnline(id, online);
        }

        public void TeamHpMemberUpdate(UInt64 id, UInt32 hp, UInt32 maxHp)
        {
            TeamManager.Instance.TeamHpMemberUpdate(id, hp, maxHp);
        }

        public void TeamPosUpdate(UInt64 id, UInt32 posX, UInt32 posY)
        {
            TeamManager.Instance.TeamPosUpdate(id, posX, posY);
        }

        #endregion

        #region 组队秘境 TeamInstanceManager
        public void OnPriceResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            SingleInstanceManager.Instance.OnPriceResp(actionId, errorId, byteArr);
            EnergyBuyingManager.Instance.OnPriceResp(actionId, errorId, byteArr);
        }

        public void OnGroupRecordInfoResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            MissionManager.Instance.OnGroupRecordInfoResp(actionId, errorId, byteArr);
        }



        public void TeamCallResp(UInt32 copyId, byte channelId, UInt32 minLevel, UInt32 maxLevel, string msg)
        {
            TeamInstanceManager.Instance.SendTeamCallMsg(copyId, channelId, Convert.ToInt32(minLevel), Convert.ToInt32(maxLevel), msg);
        }

        public void OnTeamMissionResp(byte[] byteArr)
        {
            TeamInstanceManager.Instance.OnTeamMissionResp(byteArr);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }

        public void OnCaptainDistributionResp(UInt32 errorCode, byte[] byteArr)
        {
            TeamInstanceManager.Instance.OnCaptainDistributionResp(errorCode, byteArr);
        }

        public void HpMemberRefresh(byte[] byteArr)
        {
            TeamInstanceManager.Instance.HpMemberRefresh(byteArr);
        }

        public void HpMemberUpdate(UInt32 id, UInt32 hp, UInt32 maxHp)
        {
            TeamInstanceManager.Instance.HpMemberUpdate(id, hp, maxHp);
        }

        public void HpMemberDel(UInt32 id)
        {
            TeamInstanceManager.Instance.HpMemberDel(id);
            PlayerDataManager.Instance.InstanceAvatarData.RemoveAvatarData(id);
        }

        public void HpMemberUpdateOnline(UInt32 eid, byte online)
        {
            TeamInstanceManager.Instance.HpMemberUpdateOnline(eid, online);
        }
        #endregion

        #region PK PKManager
        public void PkReceiveInvite(UInt64 dbid, string name, byte level, byte vocation, UInt32 fight, string guildName)
        {
            PKManager.Instance.PkReceiveInvite(dbid, name, level, vocation, fight, guildName);
        }

        public void OnPkMissionResp(byte[] byteArr)
        {
            PKManager.Instance.OnPkMissionResp(byteArr);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }
        #endregion

        #region 藏宝图 TreasureManager
        public void OnChestCheck(uint entityId, byte isPick)
        {
            TreasureManager.Instance.OnChestCheck(entityId, isPick);
        }

        public void OnDuelChestCheck(uint entityId, byte isPick)
        {
            TreasureManager.Instance.OnDuelChestCheck(entityId, isPick);
        }

        public void OnSpawnTreasureBoss(uint entityId)
        {
            TreasureManager.Instance.OnSpawnTreasureBoss(entityId);
        }

        public void OnKillWorldBossResp(byte[] byteArr)
        {
            TreasureManager.Instance.OnKillWorldBossResp(byteArr);
        }
        #endregion

        #region 邮箱MailManager
        public void OnMailResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            MailManager.Instance.MailResp(action_id, errorId, byteArr);
        }

        public void PulicMailsNotification(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            MailManager.Instance.PulicMailsNotification(action_id, errorId, byteArr);
        }

        public void PrivateMailsNotification(UInt32 action_id, UInt32 errorId)
        {

        }

        #endregion

        #region
        public void OnTitleResp(UInt32 actionId, UInt32 error_code, byte[] byteArr)
        {
            TitleManager.Instance.OnTitleResp(actionId, error_code, byteArr);
        }
        #endregion

        #region 公会GuildManager
        public void OnGuildResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            GuildManager.Instance.OnGuildCreateResp(action_id, errorId, byteArr);
            GuildManager.Instance.OnGuildManageResp(action_id, errorId, byteArr);
            GuildManager.Instance.OnGuildAchievementSkillResp(action_id, errorId, byteArr);
            GuildManager.Instance.OnGuildNoticeResp(action_id, errorId, byteArr);
            GuildWarManager.Instance.OnGuildWarResp(action_id, errorId, byteArr);
            RankingListManager.Instance.OnGuildResp(action_id, errorId, byteArr);
            GuildTaskManager.Instance.OnGuildResp(action_id, errorId, byteArr);
        }

        public void OnGuildMissionResp(byte[] byteArr)
        {
            GuildWarManager.Instance.OnGuildWarResultResp(byteArr);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }

        public void EventRecordResp(UInt16 action_id, LuaTable table)
        {
            GuildManager.Instance.EventRecordResp(action_id, table);
            FriendManager.Instance.EventRecordResp(action_id, table);
        }

        public void OnGuildShopResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            GuildShopManager.Instance.OnGuildShopResp(action_id, errorId, byteArr);
        }

        public void BeginEnterGuildPkMapResp(string guildName, string name, uint fight, byte military, byte vocation, byte level, byte round)
        {
            GuildWarManager.Instance.BeginEnterGuildPkMapResp(guildName, name, fight, military, vocation, level, round);
        }
        #endregion

        #region 同步时间
        public void SyncTimeResp(UInt64 time)
        {
            PlayerTimerManager.GetInstance().SyncTimeResp(time);
        }

        public void HeartBeatResp()
        {
            if (GMState.showheartbeatinfo)
            {
                LoggerHelper.Info(string.Format("HeartBeatResp in PlayerAvatar."));
            }
        }
        #endregion

        #region 聊天 ChatManager

        public void OnChatResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            ChatManager.Instance.ResponseChat(actionId, errorId, byteArr);
        }

        public void ChatGetItemResp(string itemLink, byte[] byteArr)
        {
            ChatManager.Instance.ResponseChatItemDetail(itemLink, byteArr);
        }

        #endregion

        #region 符文 RuneManager

        public void WishInfoRuneResp(byte[] byteArr)
        {
            RuneManager.Instance.WishInfoRuneResp(byteArr);
        }


        public void UpdateRuneLock(byte bagType, byte pos, SByte lockState)
        {
            RuneManager.Instance.UpdateRuneLock(bagType, pos, lockState);
        }

        public void OnRuneResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            RuneManager.Instance.OnRuneResp(actionId, errorId, byteArr);
        }

        #endregion

        #region 玩家辅助炼金次数更新
        public void OnUpdateAssistCntResp(byte avatarType, UInt64 dbid, UInt32 count)
        {
            if (avatarType == (byte)1)
            {
                PlayerDataManager.Instance.FriendData.UpdateAssistAlchemy(dbid, count);
            }
            else if (avatarType == (byte)2)
            {
                PlayerDataManager.Instance.GuildData.UpdateAssistAlchemy(dbid, count);
            }
        }
        #endregion

        #region 好友 FriendManager

        public void OnFriendResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            FriendManager.Instance.OnFriendResp(actionId, errorId, byteArr);
        }

        #endregion

        #region 恶魔之门 DemonGateManager


        private void EvilInfoResp(byte[] byteAttr)
        {
            DemonGateManager.Instance.EvilInfoResp(byteAttr);
        }

        private void GetWorldAverageLevelResp(UInt32 worldLevel)
        {
            DemonGateManager.Instance.GetWorldAverageLevelResp(worldLevel);
        }

        private void OnEvilMissionResp(byte[] byteAttr)
        {
            DemonGateManager.Instance.ShowResultPanel(byteAttr);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }

        #endregion

        #region  宝石系统 GemManager
        public void OnGemResp(UInt32 action_id, UInt32 error_code, byte[] data)
        {
            GemManager.Instance.OnGemResp(action_id, error_code, data);
        }
        #endregion

        #region 任务系统 TaskManager
        public void TaskList(UInt32 actionId, UInt32 errorId, byte[] data)
        {
            TaskManager.Instance.OnTaskResp(actionId, errorId, data);
        }

        public void OnSpacetimeRiftResp(UInt32 actionId, UInt32 errorId, byte[] data)
        {
            TaskManager.Instance.OnSpacetimeRiftResp(actionId, errorId, data);
        }

        public void OnWildTaskResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            FieldTaskManager.Instance.OnWildTaskResp(action_id, errorId, byteArr);
        }
        #endregion

        #region 制造 MakeManager

        public void OnMfgResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            MakeManager.Instance.OnMakeResp(actionId, errorId, byteArr);
        }

        #endregion

        #region 导师 TutorManager
        public void OnTutorResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            TutorManager.Instance.OnTutorResp(action_id, errorId, byteArr);
        }
        #endregion

        #region 翅膀 WingManager
        public void OnWingResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            WingManager.Instance.OnWingResp(action_id, errorId, byteArr);
        }

        /// <summary>
        /// 获取翅膀背包
        /// </summary>
        public void WingBagResp(byte[] byteArr)
        {
            WingManager.Instance.WingBagResp(byteArr);
        }

        /// <summary>
        /// 培养翅膀
        /// </summary>
        /// <param name="wingId">翅膀的dbid</param>
        /// <param name="curExp">当前经验</param>
        /// <param name="addExp">添加经验</param>
        /// <param name="sCount">普通培养次数</param>
        /// <param name="mCount">小暴击次数</param>
        /// <param name="lCount">大暴击次数</param>
        public void UpdateWingResp(byte wingId, UInt16 curExp, UInt16 addExp, byte sCount, byte mCount, byte lCount)
        {
            WingManager.Instance.UpdateWingResp(wingId, curExp, addExp, sCount, mCount, lCount);
        }

        /// <summary>
        /// 穿上翅膀
        /// </summary>
        /// <param name="wingId">翅膀的dbid</param>
        public void PutOnWingResp(byte wingId)
        {
            WingManager.Instance.PutOnWingResp(wingId);
        }

        /// <summary>
        /// 添加翅膀
        /// </summary>
        /// <param name="wingId">翅膀的dbid</param>
        public void AddWingResp(byte wingId)
        {
            WingManager.Instance.AddWingResp(wingId);
        }
        #endregion

        #region 星魂 StarSoulManager
        public void OnStarSpiritResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            StarSoulManager.Instance.OnStarSpiritResp(action_id, errorId, byteArr);
        }
        #endregion

        #region 商城 MallManager
        public void MarketDailyResp(byte[] byteArr)
        {
            MallManager.Instance.ResponseDailyLimit(byteArr);
        }

        public void MarketBuyTotalResp(byte[] byteArr)
        {
            MallManager.Instance.ResponseTotalLimit(byteArr);
        }

        public void MarketSellTotalResp(byte[] byteArr)
        {
            MallManager.Instance.ResponseFlashSaleLimit(byteArr);
        }

        public void MarketGridDailyResp(UInt16 id, UInt32 num)
        {
            MallManager.Instance.ResponseDailyLimitById(id, num);
        }

        public void MarketGridBuyTotalResp(UInt16 id, UInt32 num)
        {
            MallManager.Instance.ResponseTotalLimitById(id, num);
        }

        public void MarketGridSellTotalResp(UInt16 id, UInt32 num)
        {
            MallManager.Instance.ResponseFlashSaleLimitById(id, num);
        }

        public void OnMarketResp(UInt32 id, UInt32 errorCode, Byte[] byteArray)
        {
            MallManager.Instance.ResponseMarketErrorCode(id, errorCode, byteArray);
        }
        #endregion

        #region 决斗 DuelManage
        public void OnDuelResp(UInt32 id, UInt32 errorCode, Byte[] byteArray)
        {
            DuelManager.Instance.OnDuelResp(id, errorCode, byteArray);
        }
        #endregion

        #region 试练秘境 WonderLandManager
        public void OnTryMissionResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            WanderLandManager.ResponseWanderLandInfo(actionId, errorCode, byteArr);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }
        #endregion

        #region 装备强化 EquipStrengthenManager

        public void StrengthenQueryListRsp(byte[] byteArr)
        {
            EquipManager.Instance.StrengthenQueryListRsp(byteArr);
        }

        public void StrengthenQueryRsp(byte[] byteArr)
        {
            EquipManager.Instance.StrengthenQueryRsp(byteArr);
        }

        public void MakeStrengthenRsp(UInt32 result, byte[] byteArr)
        {
            EquipManager.Instance.MakeStrengthenRsp(result, byteArr);
        }

        #endregion

        #region

        public void EquipUpgradeResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            EquipManager.Instance.EquipUpgradeResp(actionId, errorId, args);
        }

        #endregion

        #region 物品 BagManager

        /// <summary>
        /// 更新单个物品
        /// </summary>
        /// <param name="bagType">背包类型</param>
        /// <param name="num">增加的数目</param>
        /// <param name="table">物品内存数据结构</param>
        public void UpdateItemResp(byte bagType, int num, byte[] byteArr)
        {
            BagManager.Instance.ResponseUpdateData((BagType)bagType, num, byteArr);
        }

        public void ItemUseResp(byte[] byteArr)
        {
            ClearWait((int)action_config.ITEM_USE);
            BagManager.Instance.ItemUseResp(byteArr);
        }

        /// <summary>
        ///  更新整个背包
        /// </summary>
        public void UpdateBagResp(byte[] byteArr)
        {
            BagManager.Instance.ResponseGetBagData(byteArr);
        }

        public void RewardResp(byte[] byteArr)
        {
            BagManager.Instance.ResponseGetReward(byteArr);
        }

        #endregion

        #region 奖励系统 RewardManager

        private void OnAwardResp(UInt32 action_id, UInt32 error_code, byte[] data)
        {
            RewardManager.Instance.OnRewardResp(action_id, error_code, data);
        }

        #endregion

        #region 客户端实体

        public void CreateClientEntityResp(byte[] entityData)
        {
            ClientEntityManager.Instance.CreateClientEntityResp(entityData);
        }

        public void DestroyClientEntityResp(byte[] entityData)
        {
            ClientEntityManager.Instance.DestroyClientEntityResp(entityData);
        }

        public void PickDropResp(UInt16 errorCode, UInt32 entityId, UInt32 itemID, UInt32 ItemCount)
        {
            ClientEntityManager.Instance.PickDropResp(errorCode, entityId, itemID, ItemCount);
        }

        public void SetFaceResp(UInt32 entityId, byte face)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
                face = byte.Parse(face.ToString()); 
#endif
            ClientEntityManager.Instance.SetFaceResp(entityId, face);
        }

        #endregion

        #region 时装 FashionManager
        /// <summary>
        /// 时装信息
        /// </summary>
        /// <param name="fashionList">时装列表</param>
        public void FashionInfo(byte[] fashionList)
        {
            RoleManager.Instance.RespUpdateData(fashionList);
        }

        /// <summary>
        /// 时装通知
        /// </summary>
        /// <param name="noticeType">消息类型:1购买,2过期,3续期,4装备</param>
        /// <param name="fashionId">时装ID</param>
        /// <param name="validTime">剩余过期时间</param>
        public void FashionNotice(byte noticeType, UInt32 fashionId, UInt32 validTime)
        {
            RoleManager.Instance.Notice(noticeType, (int)fashionId, validTime);
        }
        #endregion

        #region 同步战斗属性  单条/多条通知
        public void OnSyncOneBa(UInt32 entityId, byte attrId, Int32 value)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
                attrId = byte.Parse(attrId.ToString()); 
#endif
            PlayerAttrManager.GetInstance().OnSyncOneBa(entityId, attrId, value);
        }

        public void OnSyncMultiBa(UInt32 entityId, byte[] byteArr)
        {
            PlayerAttrManager.GetInstance().OnSyncMultiBa(entityId, byteArr);
        }

        #endregion

        #region Buff
        public void BuffResp(UInt32 entityId, UInt32 buffId, byte addOrRemove, UInt32 lastTime)
        {
            PlayerBuffManager.GetInstance().BuffResp(entityId, buffId, addOrRemove, lastTime);
        }

        public void BuffClientResp(UInt32 entityId, byte[] bytes)
        {
            PlayerBuffManager.GetInstance().BuffClientResp(entityId, bytes);
        }

        public void OnBuffHpChangeResp(UInt32 buffId, Int32 hp)
        {
            PlayerBuffManager.GetInstance().OnBuffHpChangeResp(buffId, hp);
        }

        #endregion

        #region 好感度系统
        public void NpcReputationResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            FavorManager.Instance.NpcReputationResp(actionId, errorId, args);
        }
        #endregion

        public void OnAlchemyResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            AlchemyManager.Instance.OnAlchemyResp(actionId, errorId, args);
        }

        #region
        public void TimeAltarResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            TimeAltarManager.Instance.TimeAltarResp(action_id, errorId, byteArr);
        }
        #endregion

        #region 排行榜处理
        public void OnRankResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            RankingListManager.Instance.OnRankResp(actionId, errorId, args);
        }

        public void GetPlayerDataResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            RankingListManager.Instance.GetPlayerDataResp(actionId, errorId, args);
        }



        public void OnPkResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            RankingListManager.Instance.OnPkResp(actionId, errorId, args);
        }
        #endregion

        #region 技能释放相关接口

        public void SelfCastSpellResp(UInt16 errorCode, UInt16 skillID, UInt32 targetID)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
            skillID = ushort.Parse(skillID.ToString());
#endif
            //LoggerHelper.Debug("Ari:skill:SelfCastSpellResp:" + errorCode + "::" + skillID);
            PlayerSkillManager.GetInstance().SelfCastSpellResp(errorCode, skillID, targetID);
        }

        public void OtherCastSpellResp(UInt32 entityID, UInt16 skillID, Int16 x, Int16 z, byte face, UInt32 targetID)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
            face = byte.Parse(face.ToString());
            skillID = ushort.Parse(skillID.ToString());
#endif
            //LoggerHelper.Debug("Ari:skill:OtherCastSpellResp:" + entityID + "::" + skillID + "::" + x + "::" + z + "::" + face);
            PlayerSkillManager.GetInstance().OtherCastSpellResp(entityID, skillID, x, z, face, targetID);
        }

        public void SpellDamageResp(UInt32 attackerID, UInt16 skillID, UInt16 skillActionID, byte[] skillDamageByte)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
            skillID = ushort.Parse(skillID.ToString());
            skillActionID = ushort.Parse(skillID.ToString());
#endif
            //LoggerHelper.Debug("Ari:skill:SpellDamageResp:" + attackerID + "::" + skillID + "::" + skillActionID + "::" + skillDamageByte);
            PlayerSkillManager.GetInstance().SpellDamageResp(attackerID, skillID, skillActionID, skillDamageByte);
        }

        //<!-- 通知玩家自己的技能的某个行为激活 -->
        public void SpellActionResp(byte[] pbSpellActionByte)
        {
            //LoggerHelper.Debug("Ari:skill:SelfSpellActionResp:" + skillID + "::" + skillActionID + "::" + targetID);
            PlayerSkillManager.GetInstance().SpellActionResp(pbSpellActionByte);
        }

        public void OtherSpellActionResp(byte[] pbOtherSpellAction)
        {
            //LoggerHelper.Debug("Ari:skill:OtherSpellActionResp:" + attackerID + "::" + skillID + "::" + skillActionID + "::" + x + "::" + z + "::" + face);
            PlayerSkillManager.GetInstance().OtherSpellActionResp(pbOtherSpellAction);
        }

        public void SpellActionOriginResp(byte[] pbSpellActionOrigin)
        {
            PlayerSkillManager.GetInstance().SpellActionOriginResp(pbSpellActionOrigin);
        }

        public void FollowerCastSpellResp(UInt16 errorCode, UInt32 attackerID, UInt16 skillID, UInt32 targetID)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
            skillID = ushort.Parse(skillID.ToString());
#endif
            PlayerSkillManager.GetInstance().FollowerCastSpellResp(errorCode, attackerID, skillID, targetID);
        }

        public void SpellCdChangeResp(byte[] pbCdChangeSpells)
        {
            PlayerSkillManager.GetInstance().SpellCdChangeResp(pbCdChangeSpells);
        }

        //通知玩家技能特殊效果
        public void SpellSpecialEffectsResp(byte[] pbSpellSpecialEffect)
        {
            PlayerSkillManager.GetInstance().SpellSpecialEffectsResp(pbSpellSpecialEffect);
        }

        #endregion

        #region 活动
        public void ActivityResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            ActivityManager.Instance.OnActivityResp(actionId, errorId, bytes);
        }
        #endregion

        #region 技能系统

        public void OnSpellResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            SpellManager.Instance.OnSpellResp(actionId, errorId, bytes);
        }

        public void EntitySpellSlotsResp(byte[] bytes)
        {
            SpellManager.Instance.EntitySpellSlotsResp(bytes);
        }

        public void EntityLearnedSpellResp(byte[] bytes)
        {
            SpellManager.Instance.EntityLearnedSpellResp(bytes);
        }

        #endregion

        #region 成就系统
        public void AchievementResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            AchievementManager.Instance.AchievementResq(actionId, errorId, bytes);
        }
        #endregion

        public void FunctionOpenNotify(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            //Debug.Log("FunctionOpenNotify");
            FunctionManager.Instance.OnFunctionResp(actionId, errorId, bytes);
        }

        public void OnDramaResp(UInt32 actionid, UInt32 errorId, byte[] bytes)
        {
            DramaManager.GetInstance().OnDramaResp(actionid, errorId, bytes);
            PlayerDataManager.Instance.OnLastReqResped();
        }

        #region 要塞

        public void FortressResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {

        }

        public void OnTeamResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            TeamManager.Instance.OnTeamResp(actionId, errorId, bytes);
        }

        #endregion

        #region actionId统一处理入口

        /// <summary>
        ///  响应服务器返回的操作编号
        /// </summary>
        /// <param name="actionId">对应action_config</param>
        /// <param name="systemInfoId">对应error_code</param>
        public void OnActionResp(UInt16 actionId, UInt16 systemInfoId, LuaTable table)
        {
            LoggerHelper.Debug(string.Concat("OnActionResp actionId:", actionId, " systemInfoId:", systemInfoId, " table:", table));
            object[] objArr = new object[table.Count];
            table.Values.CopyTo(objArr, 0);
            SystemInfoManager.Instance.Show((action_config)actionId, (int)systemInfoId, objArr);
            TeamManager.Instance.OnTeamResp(actionId, systemInfoId, objArr);
            //WelfareActiveManager.Instance.RespErrorHandler(actionId, systemInfoId, table);
            EnergyBuyingManager.Instance.OnBuyEnergyResp(actionId, systemInfoId);
            DramaTrigger.WaitServerResp(actionId);
            LuckyTurntableManager.Instance.OnTurntableActionResp(actionId, systemInfoId);
            TaskManager.Instance.OnItemCommitResp(actionId, systemInfoId);
            RedEnvelopeManager.Instance.OnGrabRedEnvelopeActionResp(actionId, table);
        }

        #endregion

        #region errorId统一处理入口

        /// <summary>
        ///  响应服务器返回的错误码
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="table"></param>
        public void SysInfoResp(UInt32 errorCode, LuaTable table)
        {
            Debug.Log(string.Concat("SysInfoResp errorCode:", errorCode, " LuaTable:", table));
            object[] objArr = new object[table.Count];
            table.Values.CopyTo(objArr, 0);
            SystemInfoManager.Instance.Show((error_code)errorCode, objArr);
        }

        public void SysInfoTextResp(byte type, string content)
        {
            SystemInfoManager.Instance.ShowInfo(type, content);
        }

        #endregion

        #region
        public void EquipSmeltingResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            EquipManager.Instance.EquipSmeltingResp(actionId, errorCode, byteArr);
        }

        public void OnEquipResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            EquipManager.Instance.OnEquipResp(actionId, errorCode, byteArr);
        }
        #endregion

        #region 附魔响应

        /// <summary>
        ///  响应服务器返回附魔信息
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="table"></param>
        public void OnEnchantResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            EquipManager.Instance.OnEnchantResp(actionId, errorCode, byteArr);
        }

        #endregion

        #region 交易中心 TradeMarketManager

        public void AuctionResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            TradeMarketManager.Instance.AuctionResp(actionId, errorId, byteArr);
        }

        #endregion

        #region 服务器通知客户端创建副本行为
        /// <summary>
        ///  响应服务器返回需创建的行为ID
        /// </summary>
        /// <param name="mapId"></param>
        /// <param name="actionId"></param>
        /// <param name="isActivation" 0:终止该行为，1：激活该行为></param>
        public void ActiveSpaceActionResp(UInt32 mapId, UInt16 actionId, Byte isActivation, UInt32 entityId, UInt16 posx, UInt16 posy, UInt16 face)
        {
            Vector3 pos = Vector3.zero;
            pos.x = posx / 100;
            pos.z = posy / 100;
            if (isActivation == 1)
            {
                GameSceneManager.GetInstance().CreateActions(actionId.ToString(), pos, entityId);
            }
            else
            {
                GameSceneManager.GetInstance().TermiateAction(actionId);
            }
        }
        #endregion

        #region 服务器复活通知弹窗
        /// <summary>
        ///  响应服务器返回复活信息
        /// </summary>
        /// <param name="mapId"></param>
        /// <param name="actionId"></param>
        /// <param name="isActivation" 0:终止该行为，1：激活该行为></param>
        public void OnDieResp(UInt16 reliveCount, UInt16 cd, UInt32 actionId)
        {
            GameSceneManager.GetInstance().Relive(cd, reliveCount);
        }

        #endregion

        #region 复活成功
        /// <summary>
        ///  响应服务器返回复活信息
        /// </summary>
        public void ReviveSuccessResp()
        {
            GameSceneManager.GetInstance().SetRelive();
        }
        #endregion

        #region 服务器通知客户端同步副本行为
        /// <summary>
        ///  响应服务器返回需创建的行为ID
        /// </summary>
        /// <param name="mapId"></param>
        /// <param name="actionId"></param>
        /// <param name="isActivation" 0:终止该行为，1：激活该行为></param>
        public void CreateClientActionsResp(UInt32 mapId, byte[] clientActionData)
        {
            PbClientActionList clientActionList = MogoProtoUtils.ParseProto<PbClientActionList>(clientActionData);

            foreach (PbClientAction clientAction in clientActionList.clientActionList)
            {
                Vector3 pos = Vector3.zero;
                pos.x = clientAction.trigger_entity_x / 100;
                pos.z = clientAction.trigger_entity_y / 100;
                GameMain.GlobalManager.GameSceneManager.GetInstance().CreateActions(clientAction.action_id.ToString(), pos);

            }
        }
        #endregion

        #region 服务器通知客户端公告行为事件
        /// <summary> 多个
        /// </summary>
        /// <param name="mapId"></param>
        /// <param name="actionId"></param>
        public void MoreSpaceActionEventResp(UInt32 mapId, byte[] clientActionData)
        {
            SpaceNoticeActionManager.Instance.MoreSpaceActionEventResp(mapId, clientActionData);
        }

        public void SpaceActionEventResp(UInt32 mapId, byte[] clientActionData)
        {
            SpaceNoticeActionManager.Instance.SpaceActionEventResp(mapId, clientActionData);
        }

        public void SpaceActionEventDebugResp(byte[] clientActionData)
        {
            SpaceNoticeActionManager.Instance.SpaceActionEventDebugResp(clientActionData);
        }

        /// <summary>
        ///  响应服务器公告信息(带参数，例：玩家名，物品名等)
        /// </summary>
        /// <param name="actionId"></param>
        /// <param name="table"></param>
        public void SpaceNoticeActionResp(UInt32 mapId, UInt16 actionId, LuaTable table)
        {
            SpaceNoticeActionManager.Instance.SpaceNoticeActionResp(mapId, actionId, table);
        }

        /// <summary>
        /// 服务器通知客户端玩家战斗输出
        /// </summary>
        public void ShowCombatAttri(byte[] combatAttri)
        {
            SpaceNoticeActionManager.Instance.ShowCombatAttri(combatAttri);
        }

        #endregion

        #region vip系统
        private void OnVIPResp(UInt32 action_id, UInt32 error_id, byte[] byteArray)
        {
            VipManager.Instance.OnVIPResp(action_id, error_id, byteArray);
        }
        private void OnVIPLevelUpResp(UInt32 action_id, UInt32 error_id, UInt32 vipLevel)
        {

        }
        #endregion

        #region 充值系统

        public void OnChargeResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            ChargeManager.Instance.OnChargeResp(actionId, errorId, bytes);
        }

        #endregion

        #region 单人副本
        public void OnSingleMissionResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            SingleInstanceManager.Instance.OnSingleMissionResp(actionId, errorId, bytes);
            GameSceneManager.GetInstance().NoticeMissionEnd();
        }

        #endregion

        #region 幻境系统
        private void OnDreamlandResp(UInt32 actionId, UInt32 errorId, byte[] byteArray)
        {
            DreamlandManager.Instance.OnDreamlandResponse((action_config)actionId, errorId, byteArray);
        }
        #endregion

        #region 宠物系统
        private void OnPetResp(UInt32 action_id, UInt32 error_id, byte[] byteArray)
        {
            PetManager petManager = PetManager.Instance;
            switch ((action_config)action_id)
            {
                case action_config.ACTION_PET_CALL_PET:
                    PbCreatePetInfo pbCreatePetInfo = MogoProtoUtils.ParseProto<PbCreatePetInfo>(byteArray);
                    petManager.ResponseCallPet(pbCreatePetInfo);
                    break;
                case action_config.ACTION_PET_ADD_EXP:
                    PbPetInfoAddExp pbPetInfoAddExp = MogoProtoUtils.ParseProto<PbPetInfoAddExp>(byteArray);
                    petManager.ResponsePetInfoAddExp(pbPetInfoAddExp);
                    break;
                case action_config.ACTION_PET_REFRESH:
                    PbPetInfoRefresh pbPetInfoRefresh = MogoProtoUtils.ParseProto<PbPetInfoRefresh>(byteArray);
                    petManager.ResponsePetRefresh(pbPetInfoRefresh);
                    break;
                case action_config.ACTION_PET_REFRESH_CONFIRM:
                    PbPetInfoRefreshConfirm pbPetInfoRefreshConfirm = MogoProtoUtils.ParseProto<PbPetInfoRefreshConfirm>(byteArray);
                    petManager.ResponsePetRefreshConfirm(pbPetInfoRefreshConfirm);
                    break;
            }
        }

        private void PetUpdateListResp(byte[] byteArray)
        {
            PbPetInfoList petList = MogoProtoUtils.ParseProto<PbPetInfoList>(byteArray);
            PetManager.Instance.ResponsePetInfoList(petList);
        }

        private void PetUpdateResp(byte[] byteArray)
        {
            PbPetInfo petInfo = MogoProtoUtils.ParseProto<PbPetInfo>(byteArray);
            PetManager.Instance.ResponseUpdatePetInfo(petInfo);
        }

        private void PetActiveStateListResp(byte[] byteArray)
        {
            PbPetInfoActiveStateList petFightStateList = MogoProtoUtils.ParseProto<PbPetInfoActiveStateList>(byteArray);
            PetManager.Instance.ResponsePetFightState(petFightStateList);
        }

        private void PetInfoAttrisListResp(byte[] byteArray)
        {
            PbPetInfoAttrisList petInfoAttrisList = MogoProtoUtils.ParseProto<PbPetInfoAttrisList>(byteArray);
            PetManager.Instance.ResponsePetInfoAttrisList(petInfoAttrisList);
        }

        private void OnEnterSpacePetReadyResp(uint mapId)
        {
            PetManager.Instance.PetDataIsReady((int)mapId);
        }

        private void PetInfoCombatListResp(byte[] byteArray)
        {
            PbPetInfoCombatList pbPetInfoCombatList = MogoProtoUtils.ParseProto<PbPetInfoCombatList>(byteArray);
            PetManager.Instance.ResponsePetFight(pbPetInfoCombatList);
        }

        private void PetInfoIdleListResp(byte[] byteArray)
        {
            PbPetInfoIdleList pbPetInfoIdleList = MogoProtoUtils.ParseProto<PbPetInfoIdleList>(byteArray);
            PetManager.Instance.ResponsePetIdle(pbPetInfoIdleList);
        }

        #endregion

        #region 套装卷轴
        public void SuitResp(UInt32 ecode, UInt32 gridPosition, UInt32 oldSuitEffect, UInt32 newSuitEffect)
        {
            EventDispatcher.TriggerEvent<int, int, int>(EquipEvents.EQUIP_SUIT_EFFECT_CHANGED, (int)gridPosition, (int)oldSuitEffect, (int)newSuitEffect);
            EventDispatcher.TriggerEvent(EquipSuitScrollEvents.Refresh);
        }
        #endregion

        #region 世界Boss排行榜
        private void OnWorldBossHitRankResp(byte[] byteArray)
        {
            PbWorldBossRankInfoList worldBossInfo = MogoProtoUtils.ParseProto<PbWorldBossRankInfoList>(byteArray);
            WorldBossRankManager.Instance.UpdateSoloRankInfo(worldBossInfo);
        }
        #endregion

        #region 部位
        public void BodyPartsResp(UInt32 entityId, byte[] byteArray)
        {
            PbBodyPartList bodyPartList = MogoProtoUtils.ParseProto<PbBodyPartList>(byteArray);
            MonsterPartManager.Instance.ResponseBodyPartList(entityId, bodyPartList);
        }

        public void BodyPartUpdateResp(UInt32 entityId, byte[] byteArray)
        {
            PbBodyPart pbBodyPart = MogoProtoUtils.ParseProto<PbBodyPart>(byteArray);
            MonsterPartManager.Instance.ResponseBodyPartUpdate(entityId, pbBodyPart);
        }

        public void BodyPartSelectResp(UInt32 entityId, byte partId, UInt32 ecode)
        {
#if UNITY_IPHONE  //added by Ari：在加这个注释时，Unity4.6.X的IL2CPP存在类型转换的问题，这里针对IOS版本进行一次类型转换，待官方修复后再去掉.
                partId = byte.Parse(partId.ToString()); 
#endif
            MonsterPartManager.Instance.ResponseBodyPartSelect(entityId, partId, ecode);
        }
        #endregion

        #region 掉落物
        //从场景中拾取被掉落的物品
        public void PickupItemResp(UInt16 ecode, UInt32 entityId)
        {
            DropItemManager.Instance.PickupItemResp(ecode, entityId);
        }

        public void PickupItemNotify(UInt32 entityId, UInt32 pickerId)
        {
            DropItemManager.Instance.PickupItemNotify(entityId, pickerId);
        }
        #endregion

        #region 选择行为接收

        /// <summary>
        ///  选择行为接收
        /// </summary>
        /// <param name="choiceId"></param>
        /// <param name="table"></param>
        public void ChoiceActionResp(UInt32 choiceId, UInt32 choiceActionId)
        {
            EntityData entityData = GlobalManager.GameSceneManager.GetInstance().CurrActionEntityData((int)choiceActionId);

            var choiceData = entityData as EntityChoiceActionData;
            List<PortalItemData> choiceDataList = new List<PortalItemData>();
            string[] choiceActionID = choiceData.action_ids_l.Split(',');
            string[] contentIds = choiceData.action_content_l.Split(',');
            for (int i = 0; i < choiceActionID.Length; i++)
            {
                PortalItemData portalItemData = new PortalItemData();
                portalItemData.choiceId = (int)choiceId;
                portalItemData.choiceActionId = choiceActionID[i];
                portalItemData.contentId = System.Convert.ToInt32(contentIds[i]);
                portalItemData.choiceType = InstanceDefine.CHOICE_SERVER_ACTION;
                choiceDataList.Add(portalItemData);
            }

            /*object[] objArr = new object[table.Count];
            table.Values.CopyTo(objArr, 0);
            string actions = "";
            for (int i = 0; i < objArr.Length; i++)
            {
                actions += objArr[i].ToString() + ",";
            }
            actions = actions.Substring(0, actions.Length -1);*/
            EventDispatcher.TriggerEvent<List<PortalItemData>>(BattleUIEvents.SHOW_PORTAL_PANEL, choiceDataList);
        }
        #endregion

        #region 福利活动系统
        /// <summary>
        /// 福利活动系统--推送监听
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="respCode">响应码</param>
        /// <param name="data">数据</param>
        public void OnFriendInviteResp(uint msgId, uint respCode, byte[] data)
        {
            WelfareActiveManager.Instance.OnFriendInviteResp(msgId, respCode, data);
        }

        /// <summary>
        /// 手机绑定信息--推送监听
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="respCode"></param>
        /// <param name="data"></param>
        public void OnBindMobileResp(uint msgId, uint respCode, byte[] data)
        {
            WelfareActiveManager.Instance.OnBindMobileResp(msgId, respCode, data);
        }

        public void FCodeResp(uint respCode, byte[] data)
        {
            WelfareActiveManager.Instance.OnFCodeResp(respCode, data);
        }
        #endregion

        #region 体力购买

        public void OnEnergyResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            EnergyBuyingManager.Instance.OnBuyEnergyResp(actionId, errorCode);
        }
        #endregion

        #region 0点重置返回

        public void ZeroClockResp()
        {
            EnergyBuyingManager.Instance.OnZeroClockResp();
            MallManager.Instance.RequestDailyLimit();
            WanderLandManager.Instance.RequestInfo();
            DreamlandManager.Instance.RequestChapterList();
            RewardManager.Instance.OnZeroClockResp();
            TaskManager.Instance.OnZeroClockResp();
            AlchemyManager.Instance.OnZeroClockResp();
            PlayerDataManager.Instance.TokenData.OnZeroClockResp();
            PlayerDataManager.Instance.CopyData.ClearDailyTimes();
            GuildManager.Instance.OnZeroClockResp();
            RewardBuffManager.Instance.RequestRewardBuff();
            MissionManager.Instance.RequestResetTimesInfo();
            RuneManager.Instance.ZeroClockResp();
        }
        #endregion

        #region 各类卡片返回，包括月卡、季卡、年卡

        public void OnCardResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            RewardManager.Instance.OnCardResp(actionId, 0, data);
        }
        #endregion

        #region 实体说话(冒泡)
        public void EntitySay(UInt32 entID, UInt32 contentID, UInt32 speechType)
        {
            Entity entity = MogoWorld.GetEntity(entID);
            if (entity == null || !(entity is EntityCreature))
            {
                //LoggerHelper.Info(string.Format("EntitySay, id = {0} is null", entID));
                return;
            }
            bool showSpeechValue = Convert.ToBoolean(speechType & 1);
            bool playSound = Convert.ToBoolean(speechType & 2);
            (entity as EntityCreature).SpeakInSpeechValue((int)contentID, showSpeechValue, playSound);
        }
        #endregion

        #region 手动选择技能目标
        public void SpellTargetResp(UInt32 id, UInt16 errId)
        {
            if (errId > 0)
            {
                return;
            }
            if (id > 0)
            {
                PlayerAvatar.Player.skillLockManager.ManualLockEntityResp(id);
            }
            else
            {
                PlayerAvatar.Player.skillLockManager.ManualUnlockEntityResp();
            }
        }
        #endregion

        #region 装备外观特效
        public void FacadeResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            EquipEffectManager.Instance.FacadeResp(actionId, errorId, bytes);
        }
        #endregion

        #region 幸运转盘系统
        public void LuckyTurntableResp(UInt32 action_id, UInt32 error_code, byte[] data)
        {
            LuckyTurntableManager.Instance.LuckyTurntableResp(action_id, error_code, data);
        }
        #endregion

        #region 坐骑系统
        public void RideNotice(byte noticeType, UInt32 mountId, UInt32 value)
        {
            MountManager.Instance.MountNotice(noticeType, mountId, value);
        }

        public void RideInfo(byte[] byteArr)
        {
            MountManager.Instance.RespUpdateMountData(byteArr);
        }
        #endregion

        #region 小地图信息获取
        public void OnWorldBossPosResp(byte isLifeBoss, UInt32 entityId, UInt32 monsterId, Int16 posx, Int16 posz)
        {
            LoggerHelper.Info("OnWorldBossPosResp:" + isLifeBoss + "," + monsterId);
            SmallMapManager.Instance.OnWorldBossPosResp(isLifeBoss, entityId, monsterId, posx, posz);
        }

        public void OnWorldBossNextTimeResp(UInt32 time)
        {
            SmallMapManager.Instance.OnWorldBossNextTimeResp(time);
        }

        public void OnMiniMapInfoResp(byte type, UInt32 param, UInt32 entityId, byte isLife, Int16 posx, Int16 posz, UInt32 nextTime)
        {
            LoggerHelper.Debug("OnMiniMapInfoResp:" + isLife + "," + entityId + "," + nextTime);
            if (nextTime != 0)
            {
                SmallMapManager.Instance.OnGateNextTimeResp(nextTime);
            }
            if (entityId != 0)
            {
                SmallMapManager.Instance.OnMiniMapInfoResp(type, param, entityId, isLife, posx, posz);
            }
        }
        #endregion

        #region 盈福奖励
        public void ItemCompensateResp(UInt32 actionId, byte[] byteArr)
        {
            RewardBuffManager.Instance.RewardBuffResp(actionId, byteArr);
        }

        #endregion

        #region 充值活动
        public void ChargeActivityResp(UInt32 action_id, UInt32 error_id, byte[] byteArray)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_CHARGE_ACTIVITY_QUERY_INFO_LIST:
                    ChargeActivityManager.Instance.ResponseAllInfo(byteArray);
                    break;
                case action_config.ACTION_CHARGE_ACTIVITY_QUERY_INFO:
                    ChargeActivityManager.Instance.ResponseInfo(byteArray);
                    break;
                case action_config.ACTION_CHARGE_ACTIVITY_REWARD:
                    ChargeActivityManager.Instance.ResponseInfo(byteArray);
                    break;
                case action_config.ACTION_CHARGE_ACTIVITY_NOTICE:
                    ChargeActivityManager.Instance.ResponseInfo(byteArray);
                    break;
            }
        }
        #endregion

        #region 开服活动
        public void OnCreateActivityResp(UInt32 action_id, UInt32 error_id, byte[] byteArray)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_CREATE_ACTIVITY_UPDATE:
                    OpenServerManager.Instance.ResponseUpdate(byteArray);
                    break;
                case action_config.ACTION_CREATE_ACTIVITY_COMPLETE:
                    OpenServerManager.Instance.ResponseComplete(byteArray);
                    break;
                case action_config.ACTION_CREATE_ACTIVITY_GET_REWARD:
                    OpenServerManager.Instance.ResponseGetReward(byteArray);
                    break;
                case action_config.ACTION_CREATE_ACTIVITY_QUERY_LIST:
                    OpenServerManager.Instance.ResponseInfo(byteArray);
                    break;
                case action_config.ACTION_CREATE_ACTIVITY_GET_FINAL_REWARD:
                    OpenServerManager.Instance.ResponseGetFinalReward(byteArray);
                    break;
            }
        }
        #endregion

        #region 红包
        public void OnRedEnvelopeNotify(UInt32 red_envelope_config_id, byte[] byteArray)
        {
            RedEnvelopeManager.Instance.OnRedEnvelopeNotifyResp(red_envelope_config_id, byteArray);
        }

        public void OnRedEnvelopeNotifyList(byte[] byteArray)
        {
            RedEnvelopeManager.Instance.OnRedEnvelopeNotifyListResp(byteArray);
        }
        #endregion

        #region 数据热更新
        public void HotUpdateCfgResp(string fileName)
        {
            ReloadXmlManager.GetInstance().CheckHotUpdate();
        }
        #endregion

        #region 聊天答题
        public void QuestionNotify(UInt32 questionId)
        {
            ChatManager.Instance.OnChatQuestionNotifyResp((int)questionId);
        }
        #endregion

        #region 防沉迷系统
        public void OnAntiAddictionResp(UInt32 rect_code, UInt32 arg1, UInt32 arg2)
        {
            AntiAddictionManager.Instance.OnAntiAddictionResp(rect_code, arg1, arg2);
        }

        #endregion

        #region
        public void OnOldReturnResp(UInt16 actionId, UInt32 errorCode, byte[] byteArr)
        {
            PlayerReturnManager.Instance.OnOldReturnResp(actionId, errorCode, byteArr);
        }
        #endregion
    }
}
