﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using Common.Structs;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager;


using ACTSystem;
using Mogo.Game;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.ClientConfig;
using MogoEngine;
using Common.ServerConfig;
using GameMain.CombatSystem;
using Mogo.AI;

namespace GameMain.Entities
{
    public class EntityDummy : EntityMonsterBase
    {
        public bool isServerDummy = false;

        public EntityDummy()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_DUMMY;

            this.map_camp_id = 99;

            RegisterPropertySetDelegate(typeof(Action<Int32>), "cur_hp");
        }

        public override void OnEnterWorld()
        {
            actorID = monster_helper.GetMonsterModel((int)monster_id);
            base.OnEnterWorld();
            this.moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
            this.moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingActor;
            InitBattleData();
            InitAI();
            partManager = new PartManagerClient(this);
            InitBossPartsValue();
            DummyManager.GetInstance().OnDummyEnterWorld(this);
            //Debug.LogError("[EntityDummy:OnEnterWorld]=>1_________actorID:    " + actorID + ",monster_id:   " + monster_id);
        }
        
        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            entityAI = null;
            DummyManager.GetInstance().OnDummyLeaveWorld(this);
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                this.actor.gameObject.name = "Dummy" + "-" + this.id + "-" + this.monster_id + ":" + actor.modelName;
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityDummy(this), this.actor.gameObject);
            }

            if (!_isDramaEntity)
            {
                AddSelfBuff();
            }
            if (is_not_play_spawn_anim == 0)
            {
                AddBornBuff();
            }
        }

        private void InitAI()
        {
            if (aiID == 0)
            {
                var monsterData = monster_helper.GetMonster((int)monster_id);
                aiID = monsterData.__ai_id;
            }
            if (aiID > 0)
            {
                entityAI = new EntityDummyAI(this);
                entityAI.SetBehaviorTree((uint)aiID);
            }
        }

        public void ChangeAI()
        {
            if (entityAI != null)
            {
                entityAI.Destroy();
            }
            if (aiID == 0)
            {
                var monsterData = monster_helper.GetMonster((int)monster_id);
                aiID = monsterData.__ai_id;
            }
            if (aiID > 0)
            {
                entityAI = new EntityDummyAI(this);
                entityAI.SetBehaviorTree((uint)aiID);
            }
            if (aiID < 0)
            {
                entityAI = null;
            }
        }

        private void InitBattleData()
        {
            var monsterData = monster_helper.GetMonster((int)monster_id);
            var monsterValue = monster_helper.GetMonsterValue((int)monster_id);
            if (monsterValue == null)
            {
                int curMapID = GameSceneManager.GetInstance().curMapID;
                int instanceID = map_helper.GetInstanceIDByMapID(curMapID);
                int difficulty = instance_helper.GetDifficulty(XMLManager.instance[instanceID]);
                int monsterType = monsterData.__type;
                monsterValue = monster_helper.GetMonsterValue(instanceID, difficulty, monsterType);
            }
            this.level = (byte)(monsterValue.__monster_level > 0 ? monsterValue.__monster_level : 1);
            var attriIds = data_parse_helper.ParseListInt(monsterValue.__attri_ids);
            var attriValues = data_parse_helper.ParseListFloat(monsterValue.__attri_values);
            for(int i = 0;i < attriIds.Count;i++)
            {
                var attri = (Common.ServerConfig.fight_attri_config)attriIds[i];
                this.OnSyncOneBa((byte)attri, (int)attriValues[i]);
            }
            var attriAdjIds = data_parse_helper.ParseListInt(monsterData.__attri_adj_ids);
            var attriAdjValues = data_parse_helper.ParseListFloat(monsterData.__attri_adj_values);
            if (attriAdjIds != null)
            {
                for (int i = 0; i < attriAdjIds.Count; i++)
                {
                    var cattri = (Common.ServerConfig.fight_attri_config)attriAdjIds[i];
                    this.attributeDict[cattri] *= attriAdjValues[i];
                    OnAttributeChange(cattri);
                }
            }
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.cur_hp, (int)max_hp);
            var skillIdsList = data_parse_helper.ParseListInt(monsterData.__skill_ids);
            this.skillManager.SetLearnedSkillList(skillIdsList);
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.speed, monsterData.__speed);
        }

        private void InitBossPartsValue()
        {
            if (_entityPartDict == null)
            {
                return;
            }
            EntityPart part;
            foreach (var node in _entityPartDict)
            {
                part = node.Value;
                part.InitHp();
                part.InitLevel(level);
                part.InitEntityType(entityType);
            }
        }

        public override void OnAttributeChange(fight_attri_config attrId)
        {
            if (attrId == fight_attri_config.HP_LIMIT)
            {
                var max_hp = this.GetAttribute(fight_attri_config.HP_LIMIT) * (1 + this.GetAttribute(fight_attri_config.HP_LIMIT_ADD_RATE));
                MogoWorld.SynEntityAttr(this, EntityPropertyDefine.max_hp, (int)max_hp);
            }
        }

        public override void OnDeath()
        {
            if (entityAI != null)
            {
                entityAI.Think(AIEvent.Self, true);
                entityAI.Destroy();
                entityAI = null;
            }

            base.OnDeath();
            EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_KILL_MONSTER, this.id.ToString() + ":" + this.monster_id.ToString());
        }

        public override void OnHit()
        {
            base.OnHit();
            if (entityAI != null)
            {
                entityAI.blackBoard.inFightingState = true;
            }
        }

        public override string GetDefaultLayer()
        {
            return PhysicsLayerDefine.Dummy;
        }

        private void AddBornBuff()
        {
            var buffList = monster_helper.GetBornBuff((int)monster_id);
            if (buffList != null)
            {
                foreach (var kvp in buffList)
                {
                    var buffID = kvp.Key;
                    var rate = kvp.Value[0];
                    var duration = kvp.Value[1];
                    if (UnityEngine.Random.Range(0f, 1f) > rate) continue;
                    this.bufferManager.AddBuffer(buffID, duration, true);
                }
            }
        }

        private void AddSelfBuff()
        {
            var buffList = monster_helper.GetSelfBuff((int)monster_id);
            if (buffList != null)
            {
                foreach (var kvp in buffList)
                {
                    var buffID = kvp.Key;
                    var rate = kvp.Value[0];
                    var duration = kvp.Value[1];
                    if (UnityEngine.Random.Range(0f, 1f) > rate) continue;
                    this.bufferManager.AddBuffer(buffID, duration, true);
                }
            }
        }

        public void BeKilledBy(EntityCreature entityCreature)
        {
            var buffList = monster_helper.GetKillGetBuff((int)monster_id);
            if (buffList != null)
            {
                foreach (var kvp in buffList)
                {
                    var buffID = kvp.Key;
                    var rate = kvp.Value[0];
                    var duration = kvp.Value[1];
                    if (UnityEngine.Random.Range(0f, 1f) > rate) continue;
                    entityCreature.bufferManager.AddBuffer(buffID, duration);
                }
            }
        }
    }

}
