﻿using Common.Data;
using Common.ServerConfig;
using System;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/8 17:46:54 
 * function: boss部位实体，只由EntityMonsterBase创建及销毁，做属性存储计算用
 * *******************************************************/

namespace GameMain.Entities
{
    public class EntityPart : EntityCreature
    {
        new public Int32 cur_hp { get; set; }
        new public Int32 max_hp { get; set; }

        private PartData _data;
        private EntityMonsterBase _owner;

        public EntityPart()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_CREATURE;
        }

        public void SetPartData(PartData data)
        {
            _data = data;
        }

        public void SetOwner(EntityMonsterBase owner)
        {
            _owner = owner;
        }

        public void InitHp()
        {
            max_hp = GetMaxHp();
            cur_hp = max_hp;
        }

        public void UpdateMaxHp()
        {
            max_hp = GetMaxHp();
        }

        public void InitLevel(byte level)
        {
            this.level = level;
        }

        public void InitEntityType(string entityType)
        {
            this.entityType = entityType;
        }

        public void Release()
        {
            _data = null;
            _owner = null;
        }

        public override float GetAttribute(fight_attri_config attributeId)
        {
            float attributeValue = _owner.GetAttribute(attributeId);
            return AdjustAttributeValue(attributeId, attributeValue);
        }

        private float AdjustAttributeValue(fight_attri_config attributeId, float attributeValue)
        {
            int index = _data.attriAdjIds.IndexOf((int)attributeId);
            if (index > -1)
            {
                attributeValue = attributeValue * _data.attriFirstOrderAdjValues[index] + _data.attriZeroOrderAdjValues[index];
            }
            return attributeValue;
        }

        private int GetMaxHp()
        {
            float hpLimit = GetAttribute(fight_attri_config.HP_LIMIT);
            float hpLimitAddRate = GetAttribute(fight_attri_config.HP_LIMIT_ADD_RATE);
            return (int)(hpLimit * (1 + hpLimitAddRate));
        }

        public override Vector3 GetPosition()
        {
            if (_owner == null)
            {
                return Vector3.zero;
            }
            return _owner.GetPosition();
        }
    }
}
