﻿
using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.States;
using GameData;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using UnityEngine;
using System;
using Game.Asset;
using Common.ExtendTools;

namespace GameMain.Entities
{
    public partial class PlayerAvatar : EntityAvatar
    {
        private bool _elevatorState = false;
        public bool elevatorState { get { return _elevatorState; } }
        public PlayerActionManager actionManager;

        public PlayerAvatar()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_AVATAR;
            visualFXPriority = ACTSystem.VisualFXPriority.H;
            RegisterPropertySetDelegate(typeof(Action<Int32>), "cur_hp");
            RegisterPropertySetDelegate(typeof(Action<Int32>), "cur_ep");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "state_cell");
        }

        public static PlayerAvatar Player
        {
            get
            {
                return MogoWorld.Player as PlayerAvatar;
            }
        }

        public override bool modelNormalQuality
        {
            get
            {
                return true;
            }
        }

        public override void OnDeath()
        {
            base.OnDeath();
            MogoEngine.Events.EventDispatcher.TriggerEvent(SpaceActionEvents.SpaceActionEvents_PlayerDeath);
            MogoEngine.Events.EventDispatcher.TriggerEvent(PlayerEvents.ON_DEATH);
        }

        public override void OnRelive()
        {
            base.OnRelive();
            MogoEngine.Events.EventDispatcher.TriggerEvent(PlayerEvents.ON_RELIVE);
        }

        public override void OnHit()
        {
            base.OnHit();
            if (entityAI != null)
            {
                entityAI.blackBoard.inFightingState = true;
            }
        }

        public override bool isDramaEntity
        {
            get { return true; }
        }

        public override bool CanTouch()
        {
            return true;
        }

        protected override void UpdateTouchEnabled()
        {
            SetTouchEnabled(GameSceneManager.GetInstance().IsCanTouch());
        }

        public override void DriveVehicle(EntityCreature vehicle)
        {
            base.DriveVehicle(vehicle);
            GMState.showPointingArrow = false;
            PlayerSkillManager.GetInstance().OnPlayerDriveVehicle(vehicle);
        }

        public override void DivorceVehicle(EntityCreature vehicle)
        {
            base.DivorceVehicle(vehicle);
            GMState.showPointingArrow = true;
            PlayerSkillManager.GetInstance().OnPlayerDivorceVehicle(vehicle);
        }

        public EntityPet GetPet()
        {
            uint curPetEntityID = PlayerPetManager.GetInstance().curPetEntityID;
            if (curPetEntityID == 0) return null;
            return MogoWorld.GetEntity(curPetEntityID) as EntityPet;
        }

        public override int GetSpaceQualitySetting()
        {
            return GameSceneManager.GetInstance().GetQualitySetting(InstanceDefine.QUALITY_SETTING_PLAYER);
        }

        public override bool CanActiveMove()
        {
            return stateManager.InState(state_config.AVATAR_STATE_DEAD_GHOST) || base.CanActiveMove();
        }

        public override bool CanMove()
        {
            return stateManager.InState(state_config.AVATAR_STATE_DEAD_GHOST) || base.CanMove();
        }

        public override bool CanTurn()
        {
            return stateManager.InState(state_config.AVATAR_STATE_DEAD_GHOST) || base.CanTurn();
        }

        public override bool CanDisplay()
        {
            return _show;
        }

        public override void OnEnterGhost()
        {
            base.OnEnterGhost();
            entity_ghost_helper.SetGhostShader(actor.boneController.GetHumanBody());
        }

        public override void OnLeaveGhost()
        {
            base.OnLeaveGhost();
            entity_ghost_helper.ResumeGhostShader(actor.boneController.GetHumanBody());
        }

        public override void Teleport(Vector3 newPosition)
        {
            if (_elevatorState) return;
            if (GameSceneManager.GetInstance().IsInCity() && Time.realtimeSinceStartup - NetSocketManager.lastReConnectTime < 2f) return;
            base.Teleport(newPosition);
        }

        private bool _isActiveMoving = false;
        public bool isActiveMoving { get { return _isActiveMoving; } set { _isActiveMoving = value; } }

        public bool canAutoTask { get; set; }

        public bool isFindingPosition = false;

        protected override bool CheckVisibleChanged(bool newVisible, bool newShow)
        {
            return _show != newShow;
        }

        public void SetElevatorState(bool state)
        {
            _elevatorState = state;
        }

        public override void UpdateEntityComponents()
        {
            base.UpdateEntityComponents();
            UpdateFindPathFx();
        }

        protected override void SetEntityComponentActive(bool visible)
        {
            base.SetEntityComponentActive(visible);
            SetFindPathFxActive(visible);
        }

        private GameObject _findPathFx;
        private void InitFindPathFx()
        {
            if (_findPathFx != null)
            {
                return;
            }
            string path = "Fx/UI/fx_xunlu.prefab";
            ObjectPool.Instance.GetGameObject(path, (obj) =>
            {
                if (!isInWorld)
                {
                    GameObject.Destroy(obj);
                    return;
                }
                Transform tran = actor.GetTransform();
                if (tran == null)
                {
                    GameObject.Destroy(_findPathFx);
                    _findPathFx = null;
                    return;
                }
                _findPathFx = obj;
                _findPathFx.transform.ResetPositionAngles();
                UpdateFindPathFx();
            });
        }

        private void UpdateFindPathFx()
        {
            if (this._findPathFx == null) return;
            bool active = !actor.isFlyingOnVehicle && !InCity() && CanDisplay();
            _findPathFx.SetActive(active);
            _findPathFx.transform.SetParent(actor.GetTransform(), false);
            _findPathFx.transform.localEulerAngles = Vector3.zero;
            _findPathFx.transform.localPosition = Vector3.zero;
        }

        public void SetFindPathFxActive(bool active)
        {
            if (_findPathFx != null)
            {
                _findPathFx.SetActive(active);
            }
        }

        private bool InCity()
        {
            return GameSceneManager.GetInstance().IsInCity();
        }
    }
}
