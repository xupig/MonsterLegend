﻿using Common.ClientConfig;
using Common.Events;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendTools;
using ACTSystem;

namespace GameMain.Entities
{
    public class EntityMonsterBase : EntityCreature
    {


        #region def属性

        public UInt32 monster_id { get; set; }
        public int isNoDrop { get; set; }
        public int actionId { get; set; }
        public LuaTable buff_client { get; set; }
        public byte is_not_play_spawn_anim { get; set; }
        public byte event_id { get; set; }

        #endregion

        private Color GOLD_COLOR = new Color(1, 1, 0, 51f / 255f);
        private Color BLUE_COLOR = new Color(109f / 255f, 255f / 255f, 1, 51f / 255f);
        protected Dictionary<int, EntityPart> _entityPartDict;
        public PartManager partManager;

        protected ActorDeathController _actorDeathController;
        public ActorDeathController actorDeathController
        {
            get
            {
                return _actorDeathController;
            }
        }
        protected ActorACTHandler _actorACTHandler;

        public EntityMonsterBase()
        {
            if (monster_helper.ShowFxForce((int)monster_id))
            {
                visualFXPriority = VisualFXPriority.H;
            }
        }

        public override void OnEnterWorld()
        {
            InitRadius();
            base.OnEnterWorld();
            EventDispatcher.TriggerEvent<uint>(MonsterEvents.ON_ENTER_WORLD, this.monster_id);
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            if (partManager!=null) partManager.Release();
            if (_entityPartDict != null)
            {
                foreach (var node in _entityPartDict)
                {
                    node.Value.Release();
                }
                _entityPartDict.Clear();
            }
            if (GameSceneManager.GetInstance().inCombatScene)
            {
                EventDispatcher.TriggerEvent<uint>(BattleUIEvents.REMOVE_LIFE_BAR_FOR_ENTITY, id);
            }
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (this.actor != null)
            {
                this.actor.SetSkinVisibleDelay(0.1f);
                if (GameSceneManager.GetInstance().inCombatScene)
                {
                    HandleLifeBar();
                }
                this.actor.actorState.BaseSpeed = this.speed * 0.01f;
                ResetScale();
                PlayBornAnimation();
                actor.animationController.SetCullingMode(monster_helper.GetCullingMode((int)monster_id));
                _actorDeathController = actor.gameObject.AddComponentOnlyOne<ActorDeathController>();
                _actorACTHandler = actor.gameObject.AddComponentOnlyOne<ActorACTHandler>();
                _actorACTHandler.qualitySetting = qualitySettingValue;
            }
            if (_actorDeathController != null)
            {
                _actorDeathController.disappearType = monster_helper.GetDisappearType((int)monster_id);
                _actorDeathController.disappearTime = monster_helper.GetDisappearTime((int)monster_id) * 0.001f;
                _actorDeathController.effectEvents = monster_helper.GetDieEvents((int)monster_id);
            }
        }

        public override int GetSpaceQualitySetting()
        {
            return GameSceneManager.GetInstance().GetQualitySetting(InstanceDefine.QUALITY_SETTING_OTHERS);
        }

        private void HandleLifeBar()
        {
            List<int> list = monster_helper.GetMonsterHpShow((int)monster_id);
            //hp_show字段配置-1时没有血条
            if (list.Count == 1 && list[0] == -1)
            {
                return;
            }
            RefreshLifeBarVisible();
        }

        protected virtual void PlayBornAnimation()
        {
            if (is_not_play_spawn_anim == 1) return;
            var bornAni = monster_helper.GetBornAni((int)monster_id);
            this.actor.animationController.PlayAction(bornAni);
        }

        void ResetScale()
        {
            var scale = monster_helper.GetMonsterScale((int)monster_id);
            if (scale != 1)
            {
                AddScaleMlp(scale);
            }
            else
            {
                if (actor != null)
                {
                    actor.gameObject.transform.localScale = new Vector3(scale, scale, scale);
                }
            }
        }

        public override void OnDeath()
        {
            if (_actorShake != null)
            {
                _actorShake.InterruptShake();
            }
            if (_actorDeathController != null)
            {
                _actorDeathController.OnDeath();
            } 
            if (bufferManager != null)
            {
                bufferManager.RemoveBuffsOnDeath();
            }
        }

        public override void OnRelive()
        {
            if (_actorDeathController != null)
            {
                _actorDeathController.OnRelive();
            }
        }

        private void InitRadius()
        {
            var monsterData = monster_helper.GetMonster((int)monster_id);
            if (monsterData == null)
            {
                LoggerHelper.Error("(@谢思宇) 怪物数据不存在：monster_id=" + monster_id);
                return;
            }
            hitRadius = monsterData.__hit_radius * 0.01f;
            boxRadius = monsterData.__box_radius * 0.01f;
        }

        public void CreateEntityPart(PartSubject bossPartSubject)
        {
            if (_entityPartDict == null)
            {
                _entityPartDict = new Dictionary<int, EntityPart>();
            }
            EntityPart entityPart = new EntityPart();
            entityPart.SetPartData(bossPartSubject.Data);
            entityPart.SetOwner(this);
            _entityPartDict.Add(bossPartSubject.Id, entityPart);
        }

        public EntityPart GetEntityPartById(int id)
        {
            return _entityPartDict[id];
        }

        protected override int GetHitShakeID()
        {
            return monster_helper.GetHitShake((int)monster_id);
        }

        protected override void InitModelShader()
        {
            int shader = monster_helper.GetShader((int)monster_id);
            SetModelShader(shader);
        }

        public void SetModelShader(int shaderId)
        {
            if (shaderId == 0)
            {
                return;
            }
            SkinnedMeshRenderer[] renderers = actor.equipController.GetSkinnedMeshRenderers();
            if (renderers == null)
            {
                return;
            }
            TwinkleRim twinkleRim;
            GameObject rendererGo;
            for (int i = 0; i < renderers.Length; ++i)
            {
                rendererGo = renderers[i].gameObject;
                twinkleRim = rendererGo.AddComponentOnlyOne<TwinkleRim>();
                twinkleRim.enabled = true;
                twinkleRim._MinStreng = InstanceDefine.MIN_STRENG;
                twinkleRim._MaxStreng = InstanceDefine.MAX_STRENG;
                twinkleRim._Frequency = InstanceDefine.FREQUENCY;
                if (shaderId == 1)
                {
                    twinkleRim.SetRimColor(GOLD_COLOR);
                }
                else
                {
                    twinkleRim.SetRimColor(BLUE_COLOR);
                }
            }
        }

        public void ResetModelShader()
        {
            SkinnedMeshRenderer[] renderers = actor.equipController.GetSkinnedMeshRenderers();
            if (renderers == null)
            {
                return;
            }
            TwinkleRim twinkleRim;
            GameObject rendererGo;
            for (int i = 0; i < renderers.Length; ++i)
            {
                rendererGo = renderers[i].gameObject;
                twinkleRim = rendererGo.GetComponent<TwinkleRim>();
                if (twinkleRim != null)
                {
                    twinkleRim.ResetRim();
                    twinkleRim.enabled = false;
                }
            }
        }

        protected override int GetRandomSpeechID(int id)
        {
            return monster_helper.GetRandomSpeechID((int)monster_id, id);
        }

        //Ready与ReadyDamaged切换，只在部位破坏的BOSS有
        public void ChangeReadyDamagedState(bool isDamaged)
        {
            actor.animationController.ChangeReadyDamagedState(isDamaged);
        }

        public override void DestroyActor()
        {
            if (_actorShake != null)
            {
                _actorShake.InterruptShake();
                _actorShake = null;
            }
            if (actorDeathController != null)
            {
                actorDeathController.DestroyActor(CanRelease());
            }
            actor = null;
        }

        public override bool CanRelease()
        {
            //5类建筑物实体不需要回收
            return monster_helper.GetModelType((int)monster_id) != 5;
        }
    }
}
