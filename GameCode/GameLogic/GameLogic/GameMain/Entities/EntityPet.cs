﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using Mogo.AI;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.Entities
{
    
    public class EntityPet : EntityCreature, IEntityFollower
    {
        public const string AURA_NAME = "fx_chongwu_jiaodiguangquan_01";

        #region def属性

        public byte pet_id { get; set; }

        public UInt32 avatar_id { get; set; }

        public byte quality { get; set; }

        public byte star { get; set; }

        #endregion

        public bool isClientPet = false;

        private PetData petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        public EntityPet()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_NAME_PET;
        }

        public override void OnEnterWorld()
        {
            actorID = pet_quality_helper.GetModelID(pet_id, quality);
            InitRadius();
            base.OnEnterWorld();
            
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.speed, 400);

            if (MasterIsPlayer())
            {
                InitAI();
                InitSkills();
                moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingActor;
                moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
                PlayerPetManager.GetInstance().OnPetEnterWorld(this);
                if (isClientPet)
                {
                    map_camp_id = PlayerAvatar.Player.map_camp_id;
                    camp_pve_type = PlayerAvatar.Player.camp_pve_type;
                    camp_pvp_type = PlayerAvatar.Player.camp_pvp_type;
                }
            }
            else {
                moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingEntity;
                moveManager.accordingMode = CombatSystem.AccordingMode.AccordingEntity;
            }
            if (isClientPet) InitBattleData();
            AddEventListeners();
            GameRenderManager.GetInstance().OnPetEnterWorld(this);
        }

        public bool IsFollower()
        {
            return MasterIsPlayer();
        }

        public override void OnLeaveWorld()
        {
            if (this.actor != null && GameSceneManager.GetInstance().inCombatScene)
            {
                EventDispatcher.TriggerEvent<uint>(BattleUIEvents.REMOVE_LIFE_BAR_FOR_ENTITY, id);
            }
            Transform transform = this.GetTransform();
            if (transform != null)
            {
                Transform transformAura = transform.Find(AURA_NAME);
                if (transformAura != null)
                {
                    GameObject.Destroy(transformAura.gameObject);
                }
            }
            base.OnLeaveWorld();
            if (MasterIsPlayer()) PlayerPetManager.GetInstance().OnPetLeaveWorld(this);
            
            RemoveEventListeners();
            GameRenderManager.GetInstance().OnPetLeaveWorld(this);
        }

        private void AddEventListeners()
        {
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnOwnerDriveVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnOwnerDivorceVehicle);
        }

        private void RemoveEventListeners()
        {
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnOwnerDriveVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnOwnerDivorceVehicle);
        }

        private void OnOwnerDriveVehicle(ControlVehicle ctrlVehicleInfo)
        {
            if (this.avatar_id == ctrlVehicleInfo.ownerId)
            {
                bufferManager.AddBuffer(BuffDefine.BUFF_ID_PET_SUMMON_VEHICLE, 999999f, true);
            }
        }

        private void OnOwnerDivorceVehicle(ControlVehicle ctrlVehicleInfo)
        {
            if (this.avatar_id == ctrlVehicleInfo.ownerId)
            {
                bufferManager.RemoveBuffer(BuffDefine.BUFF_ID_PET_SUMMON_VEHICLE);
                bufferManager.RemoveBuffer(ctrlVehicleInfo.buffId);
            }
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                ResetScale();
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityPet(this), this.actor.gameObject);
                if (GameSceneManager.GetInstance().inCombatScene)
                {
                    Transform transform = this.GetTransform();
                    if (transform != null && transform.Find(AURA_NAME) != null) return;
                    string path = string.Format("Fx/Scene/{0}.prefab", AURA_NAME);
                    Game.Asset.ObjectPool.Instance.GetGameObject(path, (obj) =>
                    {
                        obj.name = AURA_NAME;
                        obj.transform.SetParent(this.GetTransform(), false);
                        obj.transform.localScale = Vector3.one;
                        obj.transform.localPosition = Vector3.zero;
                    });
                }
            }
        }

        public bool MasterIsPlayer()
        {
            return avatar_id == PlayerAvatar.Player.id;
        }

        private void InitAI()
        {
            if (entityAI != null)
            {
                entityAI.blackBoard.aiState = AIState.CD_STATE;
                entityAI.Destroy();
                entityAI = null;
            }
            if (aiID == 0)
            {
                aiID = pet_quality_helper.GetAIID(pet_id, quality);
            }
            if (aiID > 0)
            {
                entityAI = new EntityPetAI(this);
                entityAI.SetBehaviorTree((uint)aiID);
            }
        }

        public void InitBattleData()
        {
            var attributes = petData.GetFightAttributeDataDict(pet_id);
            foreach (var kvp in attributes)
            {
                var attriID = (Common.ServerConfig.fight_attri_config)kvp.Key;
                this.attributeDict[attriID] = kvp.Value;
            }
            this.attributeDict[Common.ServerConfig.fight_attri_config.HIT_RATE] = pet_helper.GetHitRate(pet_id);

            List<int> buffIdList = petData.GetBuffIdList(pet_id);
            for (int i = 0; i < buffIdList.Count; i++)
            {
                bufferManager.AddBuffer(buffIdList[i]);
            }
            //不用加队长技能，在主城的时候服务器已经加了

            int max_hp = (int)(this.GetAttribute(fight_attri_config.HP_LIMIT) * (1 + this.GetAttribute(fight_attri_config.HP_LIMIT_ADD_RATE)));
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.max_hp, max_hp);
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.cur_hp, max_hp);
        }

        public void InitRadius()
        {
            hitRadius = pet_quality_helper.GetHitRadius(pet_id, quality) * 0.01f;
            boxRadius = pet_quality_helper.GetBoxRadius(pet_id, quality) * 0.01f;           
        }

        public void InitSkills()
        {
            List<int> activeSkillIdList = petData.GetActiveSkillIdList(pet_id);
            if (activeSkillIdList != null)
            {
                this.skillManager.SetLearnedSkillList(activeSkillIdList);
            }
        }

        void ResetScale()
        {
            var scale = pet_quality_helper.GetScale(pet_id, quality);
            actor.gameObject.transform.localScale = new Vector3(scale, scale, scale);
        }

        public override void OnAttributeChange(fight_attri_config attrId)
        {
            if (attrId == fight_attri_config.HP_LIMIT)
            {
                var max_hp = this.GetAttribute(fight_attri_config.HP_LIMIT) * (1 + this.GetAttribute(fight_attri_config.HP_LIMIT_ADD_RATE));
                MogoWorld.SynEntityAttr(this, EntityPropertyDefine.max_hp, (int)max_hp);
            }
        }

        public override void OnAddStatisticsDamage(int damageType, float value)
        {
            base.OnAddStatisticsDamage(damageType, value);
            if (MasterIsPlayer())
            {
                petData.OnAddStatisticsDamage(pet_id, damageType, value);
            }
        }

        public override int GetCampPveType()
        {
            var master = MogoWorld.GetEntity(avatar_id) as EntityCreature;
            if (master != null)
            {
                return master.GetCampPveType();
            }
            return 0;
        }

        public override int GetCampPvpType()
        {
            var master = MogoWorld.GetEntity(avatar_id) as EntityCreature;
            if (master != null)
            {
                return master.GetCampPvpType();
            }
            return 0;
        }

        public override void OnDeath()
        {
            base.OnDeath();
            if (entityAI != null)
            {
                entityAI.blackBoard.aiState = AIState.CD_STATE;
                entityAI.Destroy();
                entityAI = null;
            }
            EventDispatcher.TriggerEvent<int>(BattleUIEvents.PET_DIE, pet_id);
        }

        public override string GetDefaultLayer()
        {
            return PhysicsLayerDefine.Pet;
        }

        protected override int GetRandomSpeechID(int id)
        {
            return pet_helper.GetRandomSpeechID(pet_id, id);
        }

        public override int GetSpaceQualitySetting()
        {
            return GameSceneManager.GetInstance().GetQualitySetting(InstanceDefine.QUALITY_SETTING_OTHERS);
        }

        protected override void RefreshActorVisible()
        {
            base.RefreshActorVisible();
            Transform transform = this.GetTransform();
            if (transform != null)
            {
                Transform transformAura = transform.Find(AURA_NAME);
                if ((transformAura != null) && (actor != null))
                {
                    transformAura.gameObject.SetActive(actor.visible);
                }
            }
            
        }
    }

}
