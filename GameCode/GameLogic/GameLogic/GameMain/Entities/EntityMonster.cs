﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using Common.Structs;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager;


using ACTSystem;
using Mogo.Game;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.ClientConfig;
using GameMain.CombatSystem;

namespace GameMain.Entities
{
    public class EntityMonster : EntityMonsterBase
    {
        static HashSet<uint> _hasBornMonsters = new HashSet<uint>();

        public EntityMonster()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_MONSTER;
            this.map_camp_id = 99;

            RegisterPropertySetDelegate(typeof(Action<Int32>), "cur_hp");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "state_cell");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "target_id");
        }

        public override void OnEnterWorld()
        {
            actorID = monster_helper.GetMonsterModel((int)monster_id, modelNormalQuality);
            base.OnEnterWorld();
            this.moveManager.accordingMode = CombatSystem.AccordingMode.AccordingEntity;
            this.moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingEntity;
            partManager = new PartManagerServer(this);
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                this.actor.gameObject.name = "Monster" + "-" + this.id + "-" + this.monster_id;
                this.face = this.face;//重置一下朝向
                this.actor.actorController.usePlayerController = false;
            }
        }

        protected override void PlayBornAnimation()
        {
            //避免AOI进入的怪物重复播放出生动作
            if (_hasBornMonsters.Contains(this.id)) return;
            if ((int)(Common.Global.Global.serverTimeStampSecond) - (int)this.born_time > 3) return;
            if (cur_hp == max_hp)
            {
                base.PlayBornAnimation();
            }
            _hasBornMonsters.Add(this.id);
        }

        public override string GetDefaultLayer()
        {
            return PhysicsLayerDefine.Monster;
        }
    }

}
