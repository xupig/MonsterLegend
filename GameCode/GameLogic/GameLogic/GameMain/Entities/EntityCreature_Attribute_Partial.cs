﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using Common.Structs;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager;
using GameMain.CombatSystem;

using ACTSystem;
using Mogo.Game;
using Mogo.AI;
using GameData;
using attri_config = Common.ServerConfig.fight_attri_config;
using Common.ClientConfig;

namespace GameMain.Entities
{
    public class FightAttriConfigComparer : IEqualityComparer<attri_config>
    {

        public bool Equals(attri_config x, attri_config y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(attri_config obj)
        {
            return (int)obj;
        }
    }

    public partial class EntityCreature : Entity
    {
        #region def属性

        //减少高频率属性的ToString操作引起的GC Alloc
        private int _curHp;
        private string _curHpString;
        private int _maxHp;
        private string _maxHpString;

        private int _cur_ep;
        private string _cur_ep_show_string;
        private int _max_ep;
        private string _max_ep_show_string;


        public Vocation vocation { get; set; }
       
        public UInt32 state_cell { get; set; }

        public Int32 cur_hp
        {
            get
            {
                return _curHp;
            }
            set
            {
                if(value != _curHp)
                {
                    _curHp = value;
                    _curHpString = _curHp.ToString();
                }
            }
        }

        public string cur_hp_string
        {
            get
            {
                return _curHpString;
            }
        }

        public Int32 max_hp 
        {
            get
            {
                return _maxHp;
            }
            set
            {
                if(value != _maxHp)
                {
                    _maxHp = value;
                    _maxHpString = _maxHp.ToString();
                }
            }
        }

        public string max_hp_string
        {
            get
            {
                return _maxHpString;
            }
        }

        public Int32 cur_ep 
        {
            get
            {
                return _cur_ep;
            }
            set
            {
                if(_cur_ep != value)
                {
                    _cur_ep = value;
                    _cur_ep_show_string = (_cur_ep / 100).ToString();
                }
            }
        }
        public Int32 max_ep
        {
            get
            {
                return _max_ep;
            }
            set
            {
                if(_max_ep != value)
                {
                    _max_ep = value;
                    _max_ep_show_string = (_max_ep / 100).ToString();
                }
            }
        }

        /// <summary>
        /// 当前ep，用于显示
        /// </summary>
        public Int32 cur_ep_show
        { 
            get 
            {
                return this.cur_ep / 100;
            } 
        }

        public string cur_ep_show_string
        {
            get
            {
                return _cur_ep_show_string;
            }
        }

        /// <summary>
        /// ep上限，用于显示
        /// </summary>
        public Int32 max_ep_show
        {
            get
            {
                return this.max_ep / 100;
            }
        }

        public string max_ep_show_string
        {
            get
            {
                return _max_ep_show_string;
            }
        }

        public float speed { get; set; }
        public float base_speed { get; set; } //用于player的移动速度计算属性
        //public byte factionFlag { get; set; }
        public byte map_camp_id { get; set; }
        public byte camp_pve_type { get; set; }
        public byte camp_pvp_type { get; set; }
        public UInt32 born_time { get; set; }

        /// <summary>
        /// 角色等级
        /// </summary>
        private byte m_level;
        public virtual byte level
        {
            get { return m_level; }
            set
            {
                m_level = value;
            }
        }

        /// <summary>
        /// 客户端用来表示选中的操作模式
        /// 1: 页游操作方式 左键移动、右键转镜头
        /// 2: LOL操作方式 
        /// </summary>
        public Byte control_setting_chosen { get; set; }
        /// <summary>
        /// 客户端用来表示LOL模式选中的控制技能方式
        /// </summary>
        public Byte control_setting_lol_move { get; set; }
        /// <summary>
        /// 客户端Web Player模式选中的控制技能方式
        /// </summary>
        public Byte control_setting_web_player { get; set; }

        /// <summary>
        /// 防沉迷标记
        /// </summary>
        public UInt32 anti_addiction { set; get; }
        /// <summary>
        /// 今日沉迷时间
        /// </summary>
        public UInt32 addiction_time { set; get; }
        /// <summary>
        /// 老玩家状态
        /// </summary>
        public Byte old_role_status { set; get; }
        /// <summary>
        /// 制造天赋，已经学习的次数
        /// </summary>
        public Byte mfg_talent_had_learn_cnt { get; set; }

        /// <summary>
        /// 制造天赋，已经重置的次数
        /// </summary>
        public Byte mfg_talent_reset_cnt { get; set; }

        public UInt32 target_id { get; set; }

        public Int16 ai_id { get; set; }

        public float spell_proficient_factor1 { get; set; }
        public float spell_proficient_factor2 { get; set; }
        public float spell_proficient_factor3 { get; set; }

        #endregion

        protected Dictionary<attri_config, float> attributeDict = new Dictionary<attri_config, float>(new FightAttriConfigComparer());//战斗属性 
        protected Dictionary<int, float> statisticsDamageDict = new Dictionary<int, float>();//伤害统计
        protected Dictionary<int, Dictionary<int, float>> spellActionExDmgDict = new Dictionary<int, Dictionary<int, float>>();//技能行为伤害附加值
        public bool useEP = false; //标记该creature是否在使用技能时计算能量值

        //转向速度
        public float rotateSpeed = 0f;
        public int aiID;
        public EntityAI entityAI;
        public int actorID;
        public ACTActor actor;
        virtual public ACTActor controlActor
        {
            get
            {
                return actor;
            }
        }

        public float hitRadius = -1f;
        public float boxRadius = -1f;
        public byte face
        {
            get 
            {
                byte result = 0;
                var transform = this.GetTransform();
                if (transform != null)
                {
                    result = (byte)(transform.eulerAngles.y * 0.5f);
                }
                return result; 
            }
            set
            {
                this.SetRotation(Quaternion.Euler(0, value * 2, 0));
            }
        }

        protected bool _collidable = true;
        public bool collidable
        {
            get { return _collidable; }
            set {
                if (_collidable != value)
                {
                    _collidable = value;
                    RefreshActorCollidable();
                }
            }
        }

        private bool _banBeControlled = false;
        public bool banBeControlled
        {
            get
            {
                return _banBeControlled;
            }
            set
            {
                _banBeControlled = value;
            }
        }

        //可视(自己看自己是可见的，别人看自己是隐藏的)
        protected bool _visible = true;
        public bool visible
        {
            set
            {
                if (_visible != value)
				{
					bool changed = CheckVisibleChanged(value, _show);
                    _visible = value;
                    if (changed)
                    {
                        OnVisibleChanged();
                    }
                }
            }
        }

        //绝对可视(自己都看不见自己)
        protected bool _show = true;
        public bool show
        {
            set
			{
                if (_show != value)
				{
					bool changed = CheckVisibleChanged(_visible, value);
                    _show = value;
                    if (changed)
                    {
                        OnVisibleChanged();
                    }
                }
            }
        }

        private bool _dramaVisible = true;
        public void UpdateDramaVisible(bool dramaVisible)
        {
            if (_dramaVisible != dramaVisible)
            {
                _dramaVisible = dramaVisible;
                OnVisibleChanged();
            }
        }

        virtual protected bool CheckVisibleChanged(bool newVisible, bool newShow)
        {
            bool oldResult = _visible && _show;
            bool newResult = newVisible && newShow;
            return oldResult != newResult;
        }

        private void OnVisibleChanged()
        {
            RefreshActorVisible();
            RefreshLifeBarVisible();
            RefreshBillboardVisible();
            FixEntityActorPosition();
        }

        protected bool _actorVisible = true;
        public bool actorVisible
        {
            get { return _actorVisible; }
            set
            {
                if (_actorVisible != value)
                {
                    _actorVisible = value;
                    RefreshActorVisible();
                    if (_actorVisible && actor == null)
                    {
                        CreateActor();
                    }
                }
            }
        }

        private bool _checkCrashWall = false;
        public bool checkCrashWall
        {
            get { return _checkCrashWall; }
            set { _checkCrashWall = value; }
        }

        public virtual float GetAttribute(attri_config attributeId)
        {
            if (attributeDict.ContainsKey(attributeId))
            {
                return attributeDict[attributeId];
            }
            return 0;
        }

        public float GetStatisticsDamage(int damageType)
        {
            if (statisticsDamageDict.ContainsKey(damageType))
            {
                return statisticsDamageDict[damageType];
            }
            return 0;
        }

        public virtual void OnAddStatisticsDamage(int damageType, float value)
        {
            if (statisticsDamageDict.ContainsKey(damageType))
            {
                statisticsDamageDict[damageType] += value;
            }
            else
            {
                statisticsDamageDict.Add(damageType, value);
            }
        }

        public void OnClearStatisticsDamage()
        {
            statisticsDamageDict.Clear();
        }

        public Dictionary<attri_config, float> CopyAttributes()
        {
            Dictionary<attri_config, float> copyAttrs = new Dictionary<attri_config, float>();
            foreach (var node in attributeDict)
            {
                copyAttrs.Add(node.Key, node.Value);
            }
            return copyAttrs;
        }

        public float GetSpellActionExDmg(int spellActionID, int damageType)
        {
            if (spellActionExDmgDict.ContainsKey(spellActionID))
            {
                if (spellActionExDmgDict[spellActionID].ContainsKey(damageType))
                {
                    return spellActionExDmgDict[spellActionID][damageType];
                }
            }
            return 0;
        }

        public void AddSpellActionExDmg(int spellActionID, int damageType, float damageValue)
        {
            if (!spellActionExDmgDict.ContainsKey(spellActionID))
            {
                spellActionExDmgDict.Add(spellActionID, new Dictionary<int, float>());
            }
            if (!spellActionExDmgDict[spellActionID].ContainsKey(damageType))
            {
                spellActionExDmgDict[spellActionID].Add(damageType, damageValue);
            }
			else
			{
				spellActionExDmgDict[spellActionID][damageType] += damageValue;
			}
        }

        /// <summary>
        /// 每一个生命体都有可能有一个骑乘者
        /// </summary>
        private EntityCreature _driver = null;
        public EntityCreature driver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        public int vehicleBufferID;
                                                                          
        public SkillManager skillManager;
        public BufferManager bufferManager;
        public StateManager stateManager;
        public MoveManager moveManager;
        public VehicleManager vehicleManager;
        public HPManager hpManager;
        public SkillLockManager skillLockManager;
    }
}
