﻿#region 模块信息
/*==========================================
// 文件名：PlayerAvatarAttribute
// 命名空间: GameLogic.GameLogic.GameMain.Entities
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/15 10:37:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace GameMain.Entities
{
    public partial class PlayerAvatar : EntityAvatar
    {

        public uint curLine; //当前分线

        public int serverLevel;

        public int avatarMostLevel;

        /// <summary>
        /// 消耗限制检查，返回值 -1:绑定元宝不足，需要消耗元宝 0：满足条件 其他返回类型ID
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="showDefaultError">是否使用默认错误提示</param>
        /// <returns></returns>
        public int CheckCostLimit(Dictionary<string,string> conditions,bool showDefaultError)
        {
            if(conditions == null)
            {
                return 0;
            }
            foreach(KeyValuePair<string,string> kvp in conditions)
            {
                int result = CheckCostLimit(int.Parse(kvp.Key), int.Parse(kvp.Value), showDefaultError);
                if(result!=0)
                {
                    return result;
                }
            }
            return 0;
        }

        /// <summary>
        /// 消耗限制检查，返回值 -1:绑定元宝不足，需要消耗元宝 0：满足条件 其他返回类型ID
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="count"></param>
        /// <param name="showDefaultError">是否使用默认错误提示</param>
        /// <returns></returns>
        public int CheckCostLimit(Dictionary<string, string> conditions, int count, bool showDefaultError)
        {
            if (conditions == null)
            {
                return 0;
            }
            foreach (KeyValuePair<string, string> kvp in conditions)
            {
                int result = CheckCostLimit(int.Parse(kvp.Key), int.Parse(kvp.Value) * count, showDefaultError);
                if (result != 0)
                {
                    return result;
                }
            }
            return 0;
        }

        /// <summary>
        /// 消耗限制检查，返回值 0:代表成功 其他值:代表不够的物品id
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public int CheckCostLimit(List<BaseItemData> baseItemDataList)
        {
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                BaseItemData baseItemData = baseItemDataList[i];
                int result = CheckCostLimit(baseItemData.Id, baseItemData.StackCount, false);
                if ((result != 0) && (result != -1))
                {
                    return result;
                }
            }
            return 0;
        }

        /// <summary>
        /// 消耗限制检查，返回值 -1:绑定元宝不足，需要消耗元宝 0：满足条件 其他返回类型ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <param name="showDefaultError">是否使用默认错误提示</param>
        /// <returns></returns>
        public int CheckCostLimit(int id, int value, bool showDefaultError)
        {
            int result = 0;
            switch (id)
            {
                case public_config.MONEY_TYPE_GOLD:
                    result = CheckValue(money_gold, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_GOLD);
                    }
                    break;
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    result = CheckValue(money_bind_diamond + money_diamond, value, id);
                    if (result == 0)
                    {
                        result = CheckValue(money_bind_diamond, value, id);
                        if (result == id)
                        {
                            result = -1;
                        }
                    }
                    else if (showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_BIND_DIAMOND);
                    }
                    break;
                case public_config.MONEY_TYPE_DIAMOND:
                    result = CheckValue(money_diamond, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_DIAMOND);
                    }
                    break;
                case public_config.MONEY_TYPE_ENERGY:
                    result = CheckValue(energy, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_ENERGY);
                    }
                    break;
                case public_config.MONEY_TYPE_HONOUR:
                    result = CheckValue(honour, value, id);
                    break;
                case public_config.MONEY_TYPE_POWER:
                    result = CheckValue(power, value, id);
                    break;
                case public_config.MONEY_TYPE_RUNE_STONE:
                    result = CheckValue(money_rune_stone, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_RUNE_STONE);
                    }
                    break;
                case public_config.MONEY_TYPE_GUILD_FUND:
                    result = CheckValue(PlayerDataManager.Instance.GuildData.MyGuildData.Money, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_GUILD_FUND_NOT_ENOUGH);
                    }
                    break;
                case public_config.MONEY_TYPE_GUILD_CONTRIBUTION:
                    result = CheckValue(PlayerDataManager.Instance.GuildData.GetMyContribution(), value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_CONTRIBUTION);
                    }
                    break;
                case public_config.MONEY_TYPE_STAR_SPIRIT:
                    result = CheckValue((int)star_spirit_num, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_RUNE_STONE);
                    }
                    break;
                case public_config.MONEY_TYPE_FORTRESS_STONE:
                    result = CheckValue((int)fortress_stone, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_FORTRESS_STONE);
                    }
                    break;
                case public_config.MONEY_TYPE_FORTRESS_WOOD:
                    result = CheckValue((int)fortress_wood, value, id);
                    if (result != 0 && showDefaultError)
                    {
                        SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_FORTRESS_WOOD);
                    }
                    break;
                case public_config.MONEY_TYPE_TUTOR_VALUE:
                    result = CheckValue((int)tutor_value, value, id);
                    if (result != 0 && showDefaultError)
                    {
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (120117).ToLanguage());
                    }
                    break;
                default:
                    BagItemType itemType = item_helper.GetItemType(id);
                    if (itemType == BagItemType.Rune)
                    {
                        result = CheckValue(PlayerDataManager.Instance.BagData.GetRuneBagData().GetItemNum(id), value, id);
                    }
                    else
                    {
                        result = CheckValue(PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(id), value, id);
                    }
                    if (result != 0 && showDefaultError)
                    {
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (56743).ToLanguage(item_helper.GetName(id)), PanelIdEnum.MainUIField);
                    }
                    break;
            }
            return result;
        }

        /// <summary>
        /// 人物相关属性限制检查，成功返回LimitEnum.NO_LIMIT 其他返回 限制类型枚举值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public LimitEnum CheckLimit<T>(Dictionary<string,T> conditions)
        {
            foreach(KeyValuePair<string,T> kvp in conditions)
            {
                LimitEnum limit = CheckLimit(int.Parse(kvp.Key),kvp.Value);
                if (limit != LimitEnum.NO_LIMIT)
                {
                    return limit;
                }
            }
            return LimitEnum.NO_LIMIT;
        }

        /// <summary>
        /// 人物相关属性限制检查，成功返回LimitEnum.NO_LIMIT 其他返回 限制类型枚举值
        /// </summary>
        /// <param name="limitId"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public LimitEnum CheckLimit(int limitId,object condition)
        {
            if (condition == null)
            {
                return LimitEnum.NO_LIMIT;
            }
            LimitEnum result = LimitEnum.NO_LIMIT;
            LimitEnum checkType = (LimitEnum)limitId;
            switch (checkType)
            {
                case LimitEnum.LIMIT_LEVEL:
                    result = CheckInRegion(condition, level, checkType);
                    break;
                case LimitEnum.LIMIT_LEVEL_LT:
                    result = CheckInRegion(condition, level, checkType, false);
                    break;
                case LimitEnum.LIMIT_VOCATION:
                    result = CheckInMatch(condition, vocation.ToInt(), checkType);
                    break;
                case LimitEnum.LIMIT_VIP_LEVEL:
                    result = CheckInRegion(condition, vip_level, checkType);
                    break;
                case LimitEnum.LIMIT_TASK:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.TaskData.IsFinishedOrComplete);
                    break;
                case LimitEnum.LIMIT_TASK_ACCEPTED:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.TaskData.IsTaskMoreThanAccepted);
                    break;
                case LimitEnum.LIMIT_TASK_REWARDED:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.TaskData.IsCompleted);
                    break;
                case LimitEnum.LIMIT_FORTRESS_FUNCTION:
                    Debug.LogError("LimitEnum.LIMIT_FORTRESS_FUNCTION已经废弃，请检查配置，limitId = " + limitId);
                    result = LimitEnum.LIMIT_FORTRESS_FUNCTION;
                    break;
                case LimitEnum.LIMIT_GENDER:
                    result = CheckInMatch(condition, (int)gender, checkType);
                    break;
                case LimitEnum.LIMIT_FIGHT_FORCE:
                    result = CheckInRegion(condition, int.Parse(fight_force.ToString()), checkType);
                    break;
                case LimitEnum.LIMIT_BUFF:
                    result = CheckInQuery(condition, checkType, bufferManager.ContainsBuffer);
                    break;
                case LimitEnum.LIMIT_EQUIP:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.BagData.GetBodyEquipBagData().HasItem);
                    break;
                case LimitEnum.LIMIT_SPELL:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.SpellData.HasSkillLearned);
                    break;
                case LimitEnum.LIMIT_GUILD_CONTRIBUTE:
                    result = CheckInRegion(condition, PlayerDataManager.Instance.GuildData.GetMyContribution(), checkType);
                    break;
                case LimitEnum.LIMIT_TRY_FB:
                    result = CheckInQuery(condition, checkType, PlayerDataManager.Instance.WanderLandData.HasCompleteInstance);
                    break;
                case LimitEnum.LIMIT_TUTOR_LEVEL:
                    result = CheckInRegion(condition, tutor_level, checkType);
                    break;
                default:
                    break;
            }
            return result;
        }

        private delegate bool QueryHandler(int id);

        private int CheckValue(int current, int need, int limitType)
        {
            return current >= need ? 0 : limitType;
        }

        private LimitEnum CheckInQuery(object condition, LimitEnum limitType, QueryHandler handler)
        {
            if(condition is int)
            {
                int id = (int)condition;
                return handler(id) ? LimitEnum.NO_LIMIT : limitType;
            }
            else if (condition is string)
            {
                int id = int.Parse(condition as string);
                return handler(id) ? LimitEnum.NO_LIMIT : limitType;
            }
            else if(condition is IList)
            {
                IList dataList = condition as IList;
                for(int i=0;i<dataList.Count;i++)
                {
                    int id = int.Parse(dataList[i].ToString());
                    if(handler(id) == false)
                    {
                        return limitType;
                    }
                }
                return LimitEnum.NO_LIMIT;
            }
            else
            {
                Debug.LogError(string.Format("Check {0} has undefined condition type:{1}", limitType, condition.GetType()));
                return LimitEnum.NO_LIMIT;
            }
        }

        private LimitEnum CheckInRegion(object condition,int value,LimitEnum limitType,bool bigger = true)
        {
            int startLevel = int.MinValue;
            int endLevel = int.MaxValue;
            if(condition is int)
            {
                if(bigger)
                {
                    startLevel = (int)condition;
                }
                else
                {
                    endLevel = (int)condition;
                }
            }
            else if(condition is string)
            {
                if (bigger)
                {
                    startLevel = int.Parse(condition as string);
                }
                else
                {
                    endLevel = int.Parse(condition as string);
                }
            }
            else if(condition is IList)
            {
                IList dataList = condition as IList;
                if(dataList.Count>0)
                {
                    startLevel = int.Parse(dataList[0].ToString());
                }
                if(dataList.Count>1)
                {
                    endLevel = int.Parse(dataList[1].ToString());
                }
            }
            else if(condition is int[])
            {
                int[] dataList = condition as int[];
                startLevel = dataList[0];
                endLevel = dataList[1];
            }
            else
            {
                Debug.LogError(string.Format("Check {0} has undefined condition type:{1}",limitType,condition.GetType()));
            }

            return value >= startLevel && value <= endLevel ? LimitEnum.NO_LIMIT : limitType;
        }

        private LimitEnum CheckInMatch(object condition, int value, LimitEnum limitType)
        {
            if(condition is int)
            {
                int cond = (int)condition;
                return (value == 0 || value == cond) ? LimitEnum.NO_LIMIT : limitType;
            }
            else if (condition is string)
            {
                int cond = int.Parse(condition as string);
                return (value == 0 || value == cond) ? LimitEnum.NO_LIMIT : limitType;
            }
            else if(condition is List<string>)
            {
                List<string> dataList = condition as List<string>;
                if(dataList.Count == 0 || dataList.Contains(value.ToString()))
                {
                    return LimitEnum.NO_LIMIT;
                }
                else
                {
                    return limitType;
                }
            }
            else if(condition is List<int>)
            {
                List<int> dataList = condition as List<int>;
                if (dataList.Count == 0 || dataList.Contains(value))
                {
                    return LimitEnum.NO_LIMIT;
                }
                else
                {
                    return limitType;
                }
            }
            else
            {
                 Debug.LogError(string.Format("Check {0} has undefined condition type:{1}",limitType,condition.GetType()));
                return LimitEnum.NO_LIMIT;
            }
        }

        public int GetItemNum(int itemId)
        {
            int num = 0;
            switch (itemId)
            {
                case public_config.MONEY_TYPE_GOLD:
                    num = money_gold;
                    break;
                case public_config.MONEY_TYPE_DIAMOND:
                    num = money_diamond;
                    break;
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    num = money_bind_diamond;
                    break;
                default:
                    num = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemId);
                    break;
            }
            return num;
        }

    }
}
