﻿using ACTSystem;
using Common.Events;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using UnityEngine;
using Common.ExtendTools;

namespace GameMain.Entities
{
    public class EntityDuelChest : EntityChestBase
    {
        public EntityDuelChest()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_NAME_DUEL_CHEST;
        }

        protected override void CheckIsPickUp()
        {
            TreasureManager.Instance.CheckDuelChestIsPicked(id);
        }
    }
}
