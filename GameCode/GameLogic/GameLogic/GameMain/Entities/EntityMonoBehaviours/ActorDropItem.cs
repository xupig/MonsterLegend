using ACTSystem;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using System.Collections.Generic;
using UnityEngine;

public class ActorDropItem : MonoBehaviour
{
    public uint ownerID;
    public string entityType;
    public uint type_id;
    float _delay = 0;   
    uint _targetEntityID = 0;
    public float approachingSpeed = 10f;
    public float rotateSpeed = 180f;
    private float _toY;

    private bool _isApproaching = false;

    public bool hasTrigger = false;
    public int pickType = 0;     //掉落物拾取类型：0代表主动飞入，否则代表拾取飞入

    Transform _myTransform;

    void Awake()
    {
        _myTransform = this.transform;
    }

    void Start()
    {
        OnTriggerDetection();
    }

    void OnDestroy()
    {
    }

    void Update()
    {
        if (entityType == EntityConfig.ENTITY_TYPE_NAME_CLIENT_DROP_ITEM)
        {
            ClientItemUpdate();
        }
        else if (entityType == EntityConfig.ENTITY_TYPE_NAME_DROP_ITEM)
        {
            DropItemUpdate();
        }
    }

    private void ClientItemUpdate()
    {
        if (pickType == 0)
        {
            ActiveTypeUpdate();
        }
        else
        {
            ColliderTypeUpdate();
        }
    }

    private void DropItemUpdate()
    {
        if (_targetEntityID <= 0)
        {
            return;
        }
        if (_isApproaching)
        {
            DropItemApproaching();
        }
    }

    //主动型掉落物更新操作
    private void ActiveTypeUpdate()
    {
        if (_isApproaching)
        {
            Approaching();
        }
        transform.Rotate(0, rotateSpeed * Time.deltaTime, 0, Space.World);
    }

    //触发型掉落物更新操作
    private void ColliderTypeUpdate()
    {
        if (_targetEntityID <= 0)
        {
            return;
        }
        if (_delay > 0)
        {
            _delay -= Time.deltaTime;
            return;
        }
        Approaching();
    }

    //触发型掉落物，飞到实体
    public void FlyToEntity(uint entityID, float delay = 0)
    {
        _targetEntityID = entityID;
        _delay = delay;
        _isApproaching = true;
        approachingSpeed = GetApproachingSpeed();
    }

    private float GetApproachingSpeed()
    {
        var target = MogoWorld.GetEntity(_targetEntityID);
        if (target == null)
        {
            return 10f;
        }
        Vector3 targetPosition = target.position;
        Vector3 myPosition = _myTransform.position;
        targetPosition.y = myPosition.y;
        float distance = Vector3.Distance(targetPosition, myPosition);
        float duration = drop_item_helper.GetDropItemDuration();
        float minSpeed = drop_item_helper.GetDropItemMinSpeed();
        return Mathf.Max(distance / duration, minSpeed);
    }

    private void Approaching()
    {
        var target = MogoWorld.GetEntity(_targetEntityID);
        if (target == null)
        {
            return;
        }
        Vector3 targetPosition = target.position;
        Vector3 myPosition = _myTransform.position;
        myPosition.y = _toY - 1.0f;
        targetPosition.y = myPosition.y;
        Vector3 direction = targetPosition - myPosition;
        float distance = direction.magnitude;
        float frameSpeed = approachingSpeed * Time.deltaTime;

        if (distance <= frameSpeed)
        {
            MogoWorld.DestroyEntity(ownerID);
        }
        else
        {
            _myTransform.position = myPosition + frameSpeed * direction.normalized;
        }
    }

    private void DropItemApproaching()
    {
        var target = MogoWorld.GetEntity(_targetEntityID);
        if (target == null)
        {
            return;
        }
        Vector3 targetPosition = target.position + new Vector3(0, 1, 0);
        Vector3 myPosition = _myTransform.position;
        if (_toY > 0)
        {
            myPosition.y = _toY;
            targetPosition.y = myPosition.y;
        }
        Vector3 direction = targetPosition - myPosition;
        float distance = direction.magnitude;
        float frameSpeed = approachingSpeed * Time.deltaTime;

        if (distance <= frameSpeed)
        {
            DestroyModel();
        }
        else
        {
            _myTransform.position = myPosition + frameSpeed * direction.normalized;
        }
    }

    private void DestroyModel()
    {
        GameObjectPoolManager.GetInstance().ReleaseDropItemGameObject((int)type_id, gameObject);
        GameObject.Destroy(this);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (hasTrigger)
        {
            return;
        }
        EntityDropItemBase owner = GetOwner();
        if (owner == null || !(owner is EntityDropItem))
        {
            return;
        }

        EntityDropItem entityDropItem = owner as EntityDropItem;
        ACTActor actor = collider.gameObject.GetComponent<ACTActor>();
        if (actor == null || actor.owner == null)
        {
            return;
        }
        uint id = actor.owner.GetActorOwnerID();
        bool result = entityDropItem.BePicked(id);
        if (result)
        {
            hasTrigger = true;
        }
    }

    void OnTriggerDetection()
    {
        if (hasTrigger)
        {
            return;
        }
        EntityDropItemBase owner = GetOwner();
        if (owner == null)
        {
            return;
        }
        BoxCollider boxCollider = transform.GetComponent<BoxCollider>();
        if (boxCollider == null || !boxCollider.isTrigger)
        {
            return;
        }
        float radius = boxCollider.size.x * 0.5f;
        var enumerator = MogoWorld.Entities.GetEnumerator();
        Vector3 position = owner.position;
        List<Collider> colliderList = new List<Collider>();
        while (enumerator.MoveNext())
        {
            var entity = enumerator.Current.Value;
            if (entity is EntityCreature && (entity as EntityCreature).actor != null)
            {
                float dis = Vector3.Distance(entity.position, position);
                if (dis > radius)
                {
                    continue;
                }
                colliderList.Add((entity as EntityCreature).actor.actorController.controller);
            }
        }
        for (int i = 0; i < colliderList.Count; ++i)
        {
            if (!hasTrigger)
            {
                OnTriggerEnter(colliderList[i]);
            }
        }
    }

    private EntityDropItemBase GetOwner()
    {
        var entity = MogoWorld.GetEntity(ownerID);
        if (entity == null || !(entity is EntityDropItemBase))
        {
            return null;
        }
        return entity as EntityDropItemBase;
    }
}