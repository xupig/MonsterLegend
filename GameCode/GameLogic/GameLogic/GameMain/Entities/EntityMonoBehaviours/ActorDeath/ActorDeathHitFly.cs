﻿using ACTSystem;
using Common.ClientConfig;
using Common.RayRefection;
using UnityEngine;

public class ActorDeathHitFly : ActorDeathBase
{
    static Ray _castRay = new Ray(Vector3.zero, Vector3.zero);
    static RaycastHit _raycastHit = new RaycastHit();
    static RayRefectionInfo _RayRefectionInfo = new RayRefectionInfo();
    static float _far = 10;
    static int _terrainLayerValue = 1 << UnityEngine.LayerMask.NameToLayer("Wall");

    private float _moveBaseSpeed = 0f;
    private float _accelerate = 0;
    private Vector3 _moveTargetPosition = Vector3.zero;
    private Vector3 oldTargetPosition;
    private bool _isCrashWall = false;
    private bool _needFix = true;

    public float flyingTime = 0;//飛行持續時間
    private bool _isFlying = false;

    public override void Start(ACTActor actor, float disappearTime, int disappearType)
    {
        actor.animationController.Reset();
        actor.animationController.PlayAction(ACTAnimationActionID.BLOW_AIR);
        base.Start(actor, disappearTime, disappearType);
    }

    public void MoveByMovement(Vector3 targetPosition, float baseSpeed, float accelerate = 0, bool needFix = true, bool checkCrashWall = true)
    {
        Reset(targetPosition, baseSpeed, accelerate, needFix, checkCrashWall);
    }

    private void Reset(Vector3 targetPosition, float baseSpeed, float accelerate, bool needFix = true, bool checkCrashWall = true)
    {
        if (baseSpeed == 0) return;
        //SetDeathState(DeathState.FLY);
        _needFix = needFix;
        oldTargetPosition = targetPosition;
        if (needFix)
        {
            //计算修正后的坐标
            targetPosition = FixTargetPosition(_actor.position, targetPosition);
            //判断是否撞墙
            _isCrashWall = false;
            if (checkCrashWall == true)
            {
                _isCrashWall = oldTargetPosition == targetPosition ? false : true;
            }
        }

        _accelerate = accelerate;
        _moveTargetPosition = targetPosition;
        _moveBaseSpeed = baseSpeed;
        _actor.SetLayer(UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Air));
        var dir = targetPosition - _actor.position;
        _actor.FaceTo(_actor.position - dir);
        //Debug.LogError(string.Format("Death Hit Fly, id = {0}, target = {1}, curPos = {2}, baseSpeed = {3}, accelerate = {4}", 
        //    _actor.ID, _moveTargetPosition, gameObject.transform.position, _moveBaseSpeed, _accelerate));
    }

    private void ResetForCrashWall()
    {
        Vector3 newTargePosition = Vector3.zero;// = PlayerAvatar.Player.position;
        newTargePosition = CalcRefectPosition();
        ResetTarget(newTargePosition);
        //设置目标模型角度
        if (_RayRefectionInfo.IsChangeAvatarModel())
        {
            _actor.forward = _RayRefectionInfo.avatarRotation;
        }

    }

    // <summary>
    /// 反射点计算
    /// </summary>
    private Vector3 CalcRefectPosition()
    {
        _RayRefectionInfo.allDistance = Vector3.Distance(oldTargetPosition, _RayRefectionInfo.castRay.origin);
        return _RayRefectionInfo.CalcReflectPosition();

    }

    private bool ResetTarget(Vector3 targetPosition)
    {
        _moveTargetPosition = targetPosition;
        _accelerate = 0;
        return true;
    }

    private Vector3 FixTargetPosition(Vector3 sourcePosition, Vector3 targetPosition)
    {
        var transform = _actor.transform;
        CharacterController controller = _actor.GetComponent<CharacterController>();
        var dir = targetPosition - sourcePosition;

        _castRay.origin = sourcePosition;
        _castRay.direction = dir.normalized;
        _raycastHit.point = Vector3.zero;
        Physics.Raycast(_castRay, out _raycastHit, _far, _terrainLayerValue);
        _RayRefectionInfo.SetInfo(_castRay, _raycastHit, oldTargetPosition);
        var dism = (_raycastHit.point - _castRay.origin).magnitude;
        //DrawMoveLine(_castRay.origin, _raycastHit.point, Color.red);

        _castRay.origin = sourcePosition + transform.right.normalized * controller.radius * 0.9f;
        _raycastHit.point = Vector3.zero;
        Physics.Raycast(_castRay, out _raycastHit, _far, _terrainLayerValue);
        var disr = (_raycastHit.point - _castRay.origin).magnitude;
        //DrawMoveLine(_castRay.origin, _raycastHit.point, Color.red);

        _castRay.origin = sourcePosition - transform.right.normalized * controller.radius * 0.9f;
        _raycastHit.point = Vector3.zero;
        Physics.Raycast(_castRay, out _raycastHit, _far, _terrainLayerValue);
        var disl = (_raycastHit.point - _castRay.origin).magnitude;
        //DrawMoveLine(_castRay.origin, _raycastHit.point, Color.red);
        var newdis = Mathf.Min(dism, disr, disl);
        if (newdis < dir.magnitude)
        {
            targetPosition = sourcePosition + dir.normalized * Mathf.Max(0, newdis - controller.radius * 2);
        }
        return targetPosition;
    }

    public override void OnUpdate()
    {
        if (_deathState == DeathState.BEGIN)
        {
            _isFlying = true;
        }
        base.OnUpdate();
        if (_isFlying)
        {
            UpdateHitFlyMovement();
        }
    }

    private void UpdateHitFlyMovement()
    {
        var moveDirection = _moveTargetPosition - _actor.gameObject.transform.position;
        moveDirection.y = 0;
        var distance = moveDirection.magnitude;
        if (_moveBaseSpeed <= 0)
        {
            _actor.actorController.Stop();
            _actor.gameObject.transform.position = _actor.position;
            _isFlying = false;
            //SetDeathState(DeathState.DISAPPEAR);
            return;
        }

        if (distance < (_moveBaseSpeed * Time.deltaTime))
        {
            _actor.actorController.Stop();
            _isFlying = false;
            //SetDeathState(DeathState.DISAPPEAR);
            _actor.position = _moveTargetPosition;
            _actor.gameObject.transform.position = _actor.position;
            //如果是撞到墙,需要重新计算目标点
            if (_isCrashWall)
            {
                _isCrashWall = false;
                ResetForCrashWall();

            }
            else
            {
                return;
            }
        }
        Vector3 movement = moveDirection.normalized * _moveBaseSpeed;
        movement.y = 0;
        _actor.actorController.Move(movement);
        _actor.gameObject.transform.position = _actor.position;
        //根据加速度计算速度
        if (_accelerate != 0 && _actor.actorState.OnGround)
        {
            _moveBaseSpeed = Mathf.Max(0, _moveBaseSpeed + _accelerate * Time.deltaTime);
        }
        //Debug.LogError(string.Format("Death Hit Fly Update, id = {0}, target = {1}, curPos = {2}, baseSpeed = {3}, accelerate = {4}",
        //    _actor.ID, _moveTargetPosition, gameObject.transform.position, _moveBaseSpeed, _accelerate));
        return;
    }

    protected override float GetSinkDuration()
    {
        return flyingTime;
    }

    protected override float GetDestroyDuration()
    {
        return flyingTime + 6f;
    }
}
