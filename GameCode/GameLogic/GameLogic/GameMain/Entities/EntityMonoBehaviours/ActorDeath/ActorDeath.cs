﻿using ACTSystem;
using Common.Data;
using System.Collections.Generic;
public class ActorDeath : ActorDeathBase
{
    public List<SkillEventData> effectEvents = null;

	public override void Start(ACTActor actor, float disappearTime, int disappearType)
	{
		base.Start(actor, disappearTime, disappearType);
		if (_actor != null)
		{
			_actor.animationController.Die();
		}
	}
    protected override void OnEnterDisappear()
    {
        base.OnEnterDisappear();
        var actorACTHandler = _actor.gameObject.GetComponent<ActorACTHandler>();
        if (actorACTHandler != null && effectEvents != null)
        {
            actorACTHandler.PlayEvents(this.effectEvents);
        }
    }
}
