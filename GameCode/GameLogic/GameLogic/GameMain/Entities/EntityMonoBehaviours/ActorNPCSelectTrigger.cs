using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using GameMain.GlobalManager;
using GameMain.Entities;
using Common.Base;

public class ActorNPCSelectTrigger : ActorSelectTrigger
{
    protected override void OnTouchEvent()
    {
        base.OnTouchEvent();
        var npc = MogoWorld.GetEntity(id) as EntityNPC;
        if (npc == null) return;
        if (CameraManager.GetInstance().isShow)
        {
            if(UIManager.GetLayerVisibility(MogoUILayer.LayerUIPanel))//UIPanel层被隐藏的时候点击Npc无效
            {
                EventDispatcher.TriggerEvent(TaskEvent.TALK_TO_NPC, npc.npc_id, false);
            }
        }
    }

}