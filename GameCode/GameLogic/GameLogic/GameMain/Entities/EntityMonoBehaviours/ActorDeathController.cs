﻿using ACTSystem;
using Common.ClientConfig;
using Common.Data;
using Common.RayRefection;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/7/1 17:32:12 
 * function: 实体死亡脚本
 * *******************************************************/

public enum DeathState
{
    NONE = 0,
    BEGIN = 1,
    WAITING_DISAPPEAR = 2,
    SINK = 3,
    DESTROY = 4
}

public class ActorDeathController : MonoBehaviour
{
    private static int _allocID = 0;
    private static Dictionary<int, ActorDeathController> _deadBodyRecord = new Dictionary<int, ActorDeathController>();
    private static Queue<int> _deadQueue = new Queue<int>();
    public static void DestroyAllDeadBody()
    {
        _deadQueue.Clear();
        foreach (var pair in _deadBodyRecord)
        {
            var gameObject = pair.Value.gameObject;
            if (gameObject.activeSelf)
            {
                GameObject.Destroy(gameObject);
            }
        }
    }

    public static void CheckDeathBodyLimit()
    {
        while(_deadBodyRecord.Count > 10)
        {
            if (_deadQueue.Count == 0)break;
            int deathID = _deadQueue.Dequeue();
            if (_deadBodyRecord.ContainsKey(deathID)) 
            {
                ActorDeathController deathController = _deadBodyRecord[deathID];
                deathController.DeathImmediately();
                _deadBodyRecord.Remove(deathID); 
            }
        }
    }

    private int _id;
    private ActorDeathBase _actorDeath;

    void Awake()
    {
        _id = ++_allocID;
        _deadBodyRecord[_id] = this;
    }

    void Update()
    {
        if (_actorDeath != null)
        {
            _actorDeath.OnUpdate();
        }
    }

    void OnDestroy()
    {
        if (_deadBodyRecord.ContainsKey(_id))
        {
            _deadBodyRecord.Remove(_id);
        }
        if (_actorDeath != null)
        {
            _actorDeath.OnDestroy();
            _actorDeath = null;
        }
    }

    private int _disappearType = -1;
    public int disappearType
    {
        get
        {
            return _disappearType;
        }
        set
        {
            _disappearType = value;
        }
    }

    private float _disappearTime = 0;
    public float disappearTime
    {
        get
        {
            return _disappearTime;
        }
        set
        {
            _disappearTime = value;
        }
    }

    private float _flyingTime = 0;//飛行持續時間
    public float flyingTime
    {
        get
        {
            return _flyingTime;
        }
        set
        {
            _flyingTime = value;
        }
    }

    public List<SkillEventData> effectEvents = null;

    //正常死亡
    public void OnDeath()
    {
        _actorDeath = new ActorDeath();
        (_actorDeath as ActorDeath).effectEvents = effectEvents;
        var actor = gameObject.GetComponent<ACTActor>();
        _actorDeath.Start(actor, disappearTime, disappearType);
        _deadQueue.Enqueue(_id);
        CheckDeathBodyLimit();
    }

    public void DeathImmediately()
    {
        if (_actorDeath != null)
        {
            _actorDeath.StopWaiting();
        }
    }

    public void OnRelive()
    {
        if (_actorDeath != null)
        {
            _actorDeath.OnRelive();
            _actorDeath.OnDestroy();
            _actorDeath = null;
        }
    }

    //死亡击飞
    public void MoveByMovement(Vector3 targetPosition, float baseSpeed, float accelerate = 0, bool needFix = true, bool checkCrashWall = true)
    {
        if (_actorDeath != null)
        {
            _actorDeath.OnDestroy();
            _actorDeath = null;
        }
        _actorDeath = new ActorDeathHitFly();
        (_actorDeath as ActorDeathHitFly).flyingTime = flyingTime;
        var actor = gameObject.GetComponent<ACTActor>();
        _actorDeath.Start(actor, disappearTime, disappearType);
        (_actorDeath as ActorDeathHitFly).MoveByMovement(targetPosition, baseSpeed, accelerate, needFix, checkCrashWall);
    }

    public void DestroyActor(bool canRelease)
    {
        if (_actorDeath == null || _actorDeath.deathState == DeathState.NONE)
        {
            if (canRelease)
            {
                var actor = gameObject.GetComponent<ACTActor>();
                GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actor.actorData.ID, actor.gameObject);
            }
            else
            {
                GameObject.Destroy(gameObject);
            }
        }
        else
        {
            _actorDeath.canRelease = canRelease;
        }
    }
}
