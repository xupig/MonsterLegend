using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using GameMain.GlobalManager;
using GameLoader.Utils;
using Common.Base;

/// <summary>
/// 实体点击事件
/// </summary>
public class ActorSelectTrigger : MonoBehaviour
{
    public uint id;
    private const float DOUBLE_CLICK_TIME_SPAN = 0.4f;
    private float _clickTimeStamp = 0;

    public void OnMouseButtonUp()
    {
        OnTouchEvent();
    }

    public Vector3 GetActorPosition()
    {
        return transform.position;
    }

    //单击事件
    protected virtual void OnTouchEvent()
    {
        EventDispatcher.TriggerEvent<uint>(TouchEvents.ON_TOUCH_ENTITY, id);
        var now = Time.realtimeSinceStartup;
        if (_clickTimeStamp > 0)
        {
            if (now - _clickTimeStamp <= DOUBLE_CLICK_TIME_SPAN)
            {
                OnDoubleTouchEvent();
                _clickTimeStamp = 0;
                return;
            }
        }
        _clickTimeStamp = now;
    }

    //双击事件
    protected virtual void OnDoubleTouchEvent()
    {
        EventDispatcher.TriggerEvent<uint>(TouchEvents.ON_DOUBLE_TOUCH_ENTITY, this.id);
    }
}