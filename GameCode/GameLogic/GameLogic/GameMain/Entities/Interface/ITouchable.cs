﻿
namespace GameMain.Entities
{
    public interface ITouchable
    {
        bool CanTouch();
        void SetTouchEnabled(bool enabled);
    }
}
