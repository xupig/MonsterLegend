﻿
namespace GameMain.Entities
{
    public interface IEntityFollower
    {
        bool IsFollower();
    }
}
