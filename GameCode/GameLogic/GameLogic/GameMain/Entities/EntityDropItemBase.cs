﻿using ACTSystem;
using Common.ClientConfig;
using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine.RPC;
using System;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/20 16:54:33 
 * function: 
 * *******************************************************/

namespace GameMain.Entities
{
    public class EntityDropItemBaseUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class EntityDropItemBase : EntityItemBase 
    {
        #region def属性
        public UInt32 type_id { get; set; }
        #endregion

        public EntityDropItemBase()
        {
        }

        public ActorDropItem dropItem;

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            MogoEngine.MogoWorld.RegisterUpdate<EntityDropItemBaseUpdateDelegate>("EntityDropItemBase.Update", Update);
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            MogoEngine.MogoWorld.UnregisterUpdate("EntityDropItemBase.Update", Update);
        }

        public override void OnEnterSpace()
        {
            CreateModel();
        }

        public override void OnLeaveSpace()
        {
        }

        protected bool _modelLoaded = false;
        private void CreateModel()
        {
            if (this._modelLoaded) return;
            this._modelLoaded = true;
            var resourcePaths = GetResourcePaths();

            if (resourcePaths != null)
            {
                int closeTypeID = (int)type_id;
                GameObjectPoolManager.GetInstance().CreateDropItemGameObject(closeTypeID, CanBePickedBySelf(), (model) =>
                {
                    if (model == null)
                    {
                        return;
                    }
                    if (isInWorld)
                    {
                        AddActorComponent(model, closeTypeID);
                        OnModelLoaded();
                    }
                    else
                    {
                        GameObjectPoolManager.GetInstance().ReleaseDropItemGameObject(closeTypeID, model);
                    }
                });
            }
            else
            {
                LoggerHelper.Error("itemId " + type_id + " does not have a match modelPath");
            } 
        }

        public override Transform GetTransform()
        {
            return dropItem.transform;
        }

        string[] GetResourcePaths()
        {
            var modelPath = GameData.item_helper.GetDropModelPath((int)type_id, CanBePickedBySelf());
            var effectPath = GameData.item_helper.GetDropEffectPath((int)type_id);
            if (!string.IsNullOrEmpty(modelPath))
            {
                if (!string.IsNullOrEmpty(effectPath))
                {
                    return new string[] { modelPath, effectPath };
                }
                else {
                    return new string[] { modelPath };
                }
            }
            return null;
        }

        protected virtual void OnModelLoaded()
        {
            dropItem.gameObject.name = string.Format("{0}-{1}-{2}", entityType, id, type_id);
            FixPosition();
            this.SetPosition(dropItem.transform.position);
        }

        protected void AddActorComponent(GameObject model, int typeID)
        {
            dropItem = model.AddComponent<ActorDropItem>();
            BoxCollider collider = model.GetComponent<BoxCollider>();
            if (collider == null)
            {
                collider = model.AddComponent<BoxCollider>();
                collider.isTrigger = true;
                float range = GetColliderRange();
                collider.size = new Vector3(range, 1, range);
                ACTSystemTools.SetLayer(model.transform, UnityEngine.LayerMask.NameToLayer("DynamicWall"), false);
            }
            dropItem.ownerID = this.id;
            dropItem.entityType = this.entityType;
            dropItem.type_id = this.type_id;
        }

        private float GetColliderRange()
        {
            float range = GameData.item_helper.GetDropRange((int)type_id) * 0.01f;
            return range == 0 ? 1 : range;
        }

        public void DestroyModel()
        {
            if (dropItem != null)
            {
                //GameObject.Destroy(dropItem.gameObject);
                GameObjectPoolManager.GetInstance().ReleaseDropItemGameObject((int)dropItem.type_id, dropItem.gameObject);
                GameObject.Destroy(dropItem);
                dropItem = null;
            }
        }

        virtual protected void Update()
        {
        }

        virtual public void Pick()
        {
        }

        virtual protected bool CanBePickedBySelf()
        {
            return true;
        }
    }
}
