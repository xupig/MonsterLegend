﻿using ACTSystem;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendTools;
using Common.ServerConfig;
using Common.States;
using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.RPC;
using ShaderUtils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Entities
{
    public class EntityCreatureUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public partial class EntityCreature : Entity, IACTActorOwner, ITouchable
    {
        private GameObject _projection;
        protected bool _actorLoaded = false;
        protected ActorShake _actorShake;
        protected int _hitShakeID = 0;
        protected bool _isDramaEntity = false;
        private Transform _bgmSlot = null;
        public bool canBeHiddenInCamera = true;
        public VisualFXPriority visualFXPriority = VisualFXPriority.L;
        protected BoxCollider _triggerBoxCollider;
        public bool isMovingByTouch = false;

        virtual public bool isDramaEntity
        {
            get { return _isDramaEntity; }
        }

        protected int _qualitySettingValue = 0;  //质量设置(特效资源/模型资源)
        public int qualitySettingValue
        {
            get { return _qualitySettingValue; }
        }

        virtual public bool modelNormalQuality
        {
            get { return map_helper.GetModelQualitySetting(GameSceneManager.GetInstance().curMapID) == 0; }
        }

        virtual public int GetSpaceQualitySetting()
        {
            return 0;
        }

        protected List<float> _scaleMlps;

        public EntityCreature()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_CREATURE;
            if (instance_helper.GetTeamType(GameSceneManager.GetInstance().curInstanceID) == InstanceTeamType.SinglePlayer)
            {
                visualFXPriority = VisualFXPriority.H;
            }
        }

        public override void OnEnterWorld()
        {
            _scaleMlps = new List<float>();
            _qualitySettingValue = GetSpaceQualitySetting();
            base.OnEnterWorld();
            MogoEngine.MogoWorld.RegisterUpdate<EntityCreatureUpdateDelegate>("EntityCreature.Update", Update);
            skillManager = new SkillManager(this);
            bufferManager = new BufferManager(this);
            stateManager = new StateManager(this);
            moveManager = new MoveManager(this);
            hpManager = new HPManager(this);
            skillLockManager = new SkillLockManager(this);
            if (CanDisplay())
            {
                ShowBillboard();
            }
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_ENTER_WORLD, id);
        }

        public override void OnLeaveWorld()
        {
            _scaleMlps.Clear();
            _scaleMlps = null;
            base.OnLeaveWorld();
            HideBillboard();
            MogoEngine.MogoWorld.UnregisterUpdate("EntityCreature.Update", Update);
            moveManager.Release();
            stateManager.Release();
            bufferManager.Release();
            skillManager.Release();
            hpManager.Release();
            skillLockManager.Release();
            DestroyActor();
            _triggerBoxCollider = null;
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_LEAVE_WORLD, id);
        }

        public void SetAudioListener(bool setActive)
        {
            if (actor == null) return;
            var ad = actor.GetComponent<AudioListener>();
            //LoggerHelper.Error("[SetAudioListener]=>1________ad:    " + ad + ",actor:   " + actor.name);
            ad.enabled = setActive;
        }

        public void SetBMGSlot(Transform target = null)
        {
            Transform slotBone = target == null ? actor.gameObject.transform : target;
            if (_bgmSlot == null) return;
            _bgmSlot.transform.SetParent(slotBone, false);
            _bgmSlot.transform.ResetPositionAngles();
        }

        public void RecordBGMSlot(Transform bgmSlot)
        {
            this._bgmSlot = bgmSlot;                              
        }

        public override void OnEnterSpace() 
        {
            if (actorVisible)CreateActor();
            this.skillManager.ClearUseSkillNum();
            EventDispatcher.TriggerEvent<uint>(EntityEvents.ON_ENTER_SPACE, this.id);
        }

        public override void OnLeaveSpace() 
        {
        }

        public override Transform GetTransform()
        {
            if (actor != null) return actor.transform;
            return base.GetTransform();
        }

        public void CreateActor()
        {
            if (this._actorLoaded) return;
            this._actorLoaded = true;
            GameObjectPoolManager.GetInstance().CreateActorGameObject(actorID, (actor) =>
            {
                if (actor == null)
                {
                    LoggerHelper.Error("CreateActor error by " + actorID);
                    return;
                }
                if (this.isInWorld == false)
                {
                    //GameObject.Destroy(actor.gameObject);
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actorID, actor.gameObject);
                    return;
                }
                this.actor = actor;
                this.actor.owner = this;
                RefreshActorCollidable();
                RefreshActorVisible();
                bufferManager.OnActorLoaded();
                OnActorLoaded();
            }, this._isDramaEntity);
        }

        private void AddBoxCollider()
        {
            if (!CanTouch())
            {
                return;
            }
            float triggerWidth = global_params_helper.GetTriggerWidth();
            GameObject triggerGo;
            Transform triggerTrans = GetTransform().FindChild("trigger");
            if (triggerTrans != null)
            {
                triggerGo = triggerTrans.gameObject;
            }
            else
            {
                triggerGo = new GameObject("trigger");
                triggerGo.transform.SetParent(GetTransform(), false);
            }
            CharacterController controller = actor.actorController.controller;
            triggerGo.layer = UnityEngine.LayerMask.NameToLayer("Trigger");
            _triggerBoxCollider = triggerGo.GetComponent<BoxCollider>();
            if (_triggerBoxCollider == null)
            {
                _triggerBoxCollider = triggerGo.AddComponent<BoxCollider>();
            }
            _triggerBoxCollider.center = controller.center;
            _triggerBoxCollider.size = new Vector3(triggerWidth, controller.height, triggerWidth);
            UpdateTouchEnabled();
            AddSelectTrigger(triggerGo);
            AddRigidBody(triggerGo);
        }

        private void AddRigidBody(GameObject go)
        {
            Rigidbody rigidBody = go.GetComponent<Rigidbody>();
            if (rigidBody == null)
            {
                rigidBody = go.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
        }

        virtual public bool CanTouch()
        {
            return TargetFilter.IsEnemy(PlayerAvatar.Player, this);
        }

        public void SetTouchEnabled(bool enabled)
        {
            if (_triggerBoxCollider == null)
            {
                return;
            }
            _triggerBoxCollider.enabled = enabled;
        }

        virtual protected void UpdateTouchEnabled()
        {
        }

        virtual public void DestroyActor()
        {
            if (_actorShake != null)
            {
                _actorShake.InterruptShake();
                _actorShake = null;
            }
            if (actor != null && actor.gameObject != null)
            {
                //GameObject.Destroy(actor.gameObject);
                if (CanRelease())
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actorID, actor.gameObject);
                }
                else
                {
                    GameObject.Destroy(actor.gameObject);
                }
            }
            //actor.owner = null;
            actor = null;
        }

        public override void SetPosition(Vector3 newPosition)
        {
            base.SetPosition(newPosition);
            //var time = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            //var ms = PlayerTimerManager.GetInstance().GetNowServerMillisecond();
            //Debug.LogError(this.id + "收到同步坐标:" + time + "::" + ms + "::" + newPosition.x + "," + newPosition.z);
        }

        public override void SetRotation(Quaternion newRotation)
        {
            base.SetRotation(newRotation);
            if (this.actor != null)
            {
                this.actor.rotation = newRotation;
            }
        }

        public override void Teleport(Vector3 newPosition)
        {
            base.Teleport(newPosition);
            if (banBeControlled) return;
            if(moveManager!=null) moveManager.Stop();
            if (this.actor != null)
            {
                newPosition.y = this.actor.position.y;
                this.actor.position = newPosition;
                this.actor.ToGround();
            }
        }

        public virtual void OnActorLoaded()
        {
            if (isInWorld)
            {
                actor.position = this.position;
                actor.rotation = this.rotation;
                var temp = this.GetType().ToString().Split('.');
                actor.gameObject.name = temp[temp.Length - 1] + "-" + this.id;
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityCreature(this), this.actor.gameObject);
                AddBoxCollider();
                if (boxRadius >= 0) { actor.actorController.SetRadius(boxRadius); }
                if (hitRadius < 0) { hitRadius = actor.actorController.GetRadius(); }
                this.actor.ToGround();
                this.position.y = actor.position.y;
                InitProjection();
                InitActorShake();
                InitModelShader();
                this.stateManager.OnStateChange();
                skillLockManager.InitSkillLockBuff();
                if (_isDramaEntity) //剧情实体设置可见
                {
                    actor.animationController.SetCullingMode(AnimatorCullingMode.AlwaysAnimate);
                }
				actor.actorController.alwaysCheckGround = this._isDramaEntity;
                if (!DramaManager.GetInstance().currentShowOtherEntities && !isDramaEntity)
                {
                    UpdateDramaVisible(false);
                }
            }
            else
            {
                DestroyActor();
            }
        }

        public virtual void AddSelectTrigger(GameObject triggerGo)
        {
            var trigger = triggerGo.AddComponentOnlyOne<ActorSelectTrigger>();
            trigger.id = this.id;
        }

        public virtual void UpdateEntityComponents()
        {
            UpdateProjection();
        }

        protected virtual void SetEntityComponentActive(bool visible)
        {
            SetProjectionActive(visible);
        }

        private void InitProjection()
        {
            Transform transform = this.GetTransform();
            if (transform != null && transform.Find("projection(Clone)") != null) return;
            string path = "Fx/UI/projection.prefab";
            Game.Asset.ObjectPool.Instance.GetGameObject(path, (obj) =>
            {
                _projection = obj;
                _projection.layer = UnityEngine.LayerMask.NameToLayer("Actor");
                if (actor == null || actor.GetTransform() == null)
                {
                    GameObject.Destroy(_projection);
                    _projection = null;
                    return;
                }
                UpdateProjection();
            });
        }

        private void UpdateProjection()
        {
            if (_projection == null) return;
            bool active = CanDisplay();
            _projection.SetActive(active);
            _projection.transform.SetParent(actor.GetTransform(), false);
            _projection.transform.localEulerAngles = new Vector3(87, 0, 0);
            _projection.transform.localPosition = Vector3.zero;
        }

        protected void SetProjectionActive(bool active)
        {
            if (_projection != null)
            {
                _projection.SetActive(active);
            }
        }

        private void InitActorShake()
        {
            _hitShakeID = GetHitShakeID();
            if (_hitShakeID > 0)
            {
                _actorShake = actor.gameObject.AddComponentOnlyOne<ActorShake>();
                _actorShake.id = _hitShakeID;
            }
        }

        protected virtual void InitModelShader()
        {
        }

        protected virtual int GetHitShakeID()
        {
            return 0;
        }

        public virtual void OnHit()
        {
            if (_actorShake != null)
            {
                _actorShake.StartShake();
            }
        }

        public virtual void Update()
        {
            Think();
        }

        public virtual void Think()
        {
            if (entityAI == null) return;
            entityAI.Think();
        }

        public void StopThinking()
        {
            if (entityAI == null) return;
            entityAI.StopThinking();
        }

        public void RecoveryThinking()
        {
            if (entityAI == null) return;
            entityAI.RecoveryThinking();
        }

        public virtual uint GetActorOwnerID()
        {
            return this.id;
        }

        public float GetHeight()
        {
            if (actor == null) return -1;
            return actor.actorController.GetHeight();
        }

        public virtual void OnDeath()
        {
            if (actor != null)
            {
                actor.animationController.Die();
            }
            if (_actorShake != null)
            {
                _actorShake.InterruptShake();
            }
            if (bufferManager != null)
            {
                bufferManager.RemoveBuffsOnDeath();
            }
        }

        public virtual void OnRelive()
        {

        }

        public void FaceToPosition(Vector3 targetPos)
        {
            if (this.actor != null)
            {
                this.actor.FaceTo(targetPos);
            }
        }

        public virtual int GetCampPveType()
        {
            return (int)camp_pve_type;
        }

        public virtual int GetCampPvpType()
        {
            return (int)camp_pvp_type;
        }

        public virtual string GetDefaultLayer()
        {
            return PhysicsLayerDefine.Actor;
        }

        public virtual Vector3 GetPosition()
        {
            return this.position;
        }

        public void SetShelter()
        {
            if (this.actor != null)
            {
                try
                {
                    var modelTrans = actor.boneController.GetHumanBody();
                    var renderer = modelTrans.GetComponent<SkinnedMeshRenderer>();
                    Material material = Application.isEditor ? renderer.sharedMaterial : renderer.material;
                    var shaderName = string.Concat(material.shader.name, "_Shelter");
                    var shader = ShaderRuntime.loader.Find(shaderName);
                    if (shader != null)
                    {
                        material.shader = shader;
                    }
                }
                catch (Exception ex){
                    Debug.LogError("SetShelter Error!" + ex.Message);
                }
            }
        }

        public void SetActorLayer(string layerName, bool includeChildren = false)
        {
            if (actor != null)
            {
                actor.SetLayer(layerName, includeChildren);
            }
        }

        public string GetActorLayer()
        {
            return actor.GetLayer();
        }

        public void RefreshActorCollidable()
        {
            if (_collidable)
            {
                SetActorLayer(GetDefaultLayer());
            }
            else
            {
                SetActorLayer(PhysicsLayerDefine.Ghost);
            }
        }

        protected virtual void RefreshActorVisible()
        {
            if (actor != null && actor.gameObject != null)
            {
                actor.visible = CanDisplay() && _actorVisible;
                SetEntityComponentActive(actor.visible);
            }
        }

        virtual protected void RefreshLifeBarVisible()
        {
            if (GameSceneManager.GetInstance().inCombatScene)
            {
                if (CanDisplay())
                {
                    ShowLifeBar();
                }
                else
                {
                    HideLifeBar();
                }
            }
        }

        protected void RefreshBillboardVisible()
        {
            if (CanDisplay())
            {
                ShowBillboard();
            }
            else
            {
                HideBillboard();
            }
        }

        protected void FixEntityActorPosition()
        {
            if (this.moveManager.defaultAccordingMode == AccordingMode.AccordingEntity && actor != null)
            {
                actor.position = this.position;
            }
        }

        private void ShowLifeBar()
        {
            if (_isDramaEntity) return; //剧情实体不显示血条
            EventDispatcher.TriggerEvent<uint>(BattleUIEvents.CREATE_LIFE_BAR_FOR_ENTITY, id);
        }

        private void HideLifeBar()
        {
            EventDispatcher.TriggerEvent<uint>(BattleUIEvents.REMOVE_LIFE_BAR_FOR_ENTITY, id);
        }

        private void ShowBillboard()
        {
            if (_isDramaEntity) return; //剧情实体不显示名字
            CreatureDataManager.GetInstance().AddEntityID(id);
        }

        private void HideBillboard()
        {
            CreatureDataManager.GetInstance().RemoveEntityID(id);
        }

        public void InitDramaEntity()
        {
            _isDramaEntity = true;
            HideLifeBar();
            aiID = -1;
        }

        public bool IsPlayer()
        {
            return this == PlayerAvatar.Player;
        }

        #region 载具
        public virtual void DriveVehicle(EntityCreature vehicle)
        {
            actor.isFlyingOnVehicle = true;
            banBeControlled = true;
            //actor.SetParent(vehicle.actor.transform);
            var riderSlot = vehicle.actor.boneController.GetBoneByName("slot_ride");
            if (riderSlot == null)
            {
                LoggerHelper.Error("名称: " + vehicle.actor.name + ",的载具没有slot_ride挂节点，@小之确认！");
                actor.transform.SetParent(vehicle.actor.transform, false);
                actor.transform.localPosition = new Vector3(0, 3.5f, 0);
            }
            else
            {
                actor.transform.SetParent(riderSlot, false);
                actor.transform.localPosition = Vector3.zero;
            }
            actor.transform.localEulerAngles = Vector3.zero;
            //
            //moveManager.accordingMode = CombatSystem.AccordingMode.AccordingActor;
            //LoggerHelper.Error("[EntityCreature:DriveVehicle]=>1______________this: " + this + "moveManager.accordingMode:  " + moveManager.accordingMode);
        }

        public virtual void DivorceVehicle(EntityCreature vehicle)
        {
            if (actor == null) return;
            if(vehicle is EntityAirVehicle)
            {
                actor.position = (vehicle as EntityAirVehicle).riderPosition;
            }
            actor.isFlyingOnVehicle = false;
            banBeControlled = false;
            actor.transform.SetParent(null, false);
            actor.position = vehicle.actor.position;
            actor.localEulerAngles = vehicle.actor.localEulerAngles;
            moveManager.accordingMode = moveManager.defaultAccordingMode;
            //设置主相机目标点
            //CameraManager.GetInstance().SetFollowingTarget(actor.transform);
            //LoggerHelper.Error("[EntityCreature:DivorceVehicle]=>2______________this: " + this + "moveManager.accordingMode:  " + moveManager.accordingMode);
        }
        #endregion

        public int GetSkillIDByIdx(int skillIdx)
        {
            return skillManager.GetSkillIDByPos(skillIdx);
        }

        public void SetSkillIcon()
        {
            for (int i = 1; i <= 4; i++)
            {
                int skillID = GetSkillIDByIdx(i);
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.SET_SKILL_ID, i, skillID);
            }
        }

        public void Speak(int id, bool showSpeechValue = true, bool playSound = false)
        {
            int speechValue = GetRandomSpeechID(id);
            if (speechValue == -1)
            {
                //LoggerHelper.Error(string.Format("怪物说话配置错误，怪物id为{0}, 说话id为{1}", monster_id, id));
                return;
            }
            SpeakInSpeechValue(speechValue, showSpeechValue, playSound);
        }

        public void SpeakInSpeechValue(int speechValue, bool showSpeechValue = true, bool playSound = false)
        {
            if (showSpeechValue)
            {
                PlayBubble(speechValue);
            }
            if (playSound)
            {
                PlayVoice(speechValue);
            }
        }

        private void PlayVoice(int soundID)
        {
            SoundInfoManager.Instance.PlaySound(soundID);
        }

        virtual protected int GetRandomSpeechID(int id)
        {
            return -1;
        }

        private void PlayBubble(int speechValue)
        {
            EventDispatcher.TriggerEvent<uint, int>(BattleUIEvents.SHOW_SPEECH, this.id, speechValue);
        }

        virtual public bool CanActiveMove()
        {
            return stateManager.CanDO(CharacterAction.ACTIVE_MOVE_ABLE);
        }

        virtual public bool CanMove()
        {
            return stateManager.CanDO(CharacterAction.MOVE_ABLE);
        }

        virtual public bool CanTurn()
        {
            return stateManager.CanDO(CharacterAction.TURN_ABLE);
        }

        virtual public bool CanDisplay()
        {
            return _show && _visible && (_isDramaEntity || DramaManager.GetInstance().currentShowOtherEntities);
        }

        virtual public void OnEnterGhost()
        {
            if (actor != null)
            {
                actor.animationController.Ghost();
            }
        }

        virtual public void OnLeaveGhost()
        {
        }

        virtual public bool CanRelease()
        {
            return true;
        }

        public uint skillLockId
        {
            get
            {
                if (skillLockManager.manualLockId == 0)
                {
                    return skillLockManager.skillLockId;
                }
                return skillLockManager.manualLockId;
            }
        }

        public uint manualLockId
        {
            get
            {
                return skillLockManager.manualLockId;
            }
        }

        public void AddScaleMlp(float value)
        {
            if (value == 1)
            {
                return;
            }
            _scaleMlps.Add(value);
            UpdateActorScale();
        }

        public void RemoveScaleMlp(float value)
        {
            if (value == 1)
            {
                return;
            }
            if (_scaleMlps.Contains(value))
            {
                _scaleMlps.Remove(value);
            }
            UpdateActorScale();
        }

        private void UpdateActorScale()
        {
            float scale = 1;
            for (int i = 0; i < _scaleMlps.Count; i++)
            {
                scale *= _scaleMlps[i];
            }
            if (actor != null)
            {
                actor.gameObject.transform.localScale = new Vector3(scale, scale, scale);
            }
        }
    }
}
