﻿using ACTSystem;
using Common.ClientConfig;
using Common.Events;
using Common.Structs;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using attri_config = Common.ServerConfig.fight_attri_config;

namespace GameMain.Entities
{
    public class EntityAvatar : EntityCreature, IEntityFollower
    {
        #region def属性

        public string name { set; get; }

        private UInt16 m_title = 0; //称号
        public UInt16 title
        { 
            set 
            {
                m_title = value;
            }
            get 
            {
                return m_title;
            }
        }

        public Gender gender { get; set; } //性别

        public Int32 exp { get; set; }  //经验

        public byte[] baseflag { get; set; }

        public Int32 money_gold{ get; set; }

        public Int32 money_diamond { get; set; }

        public Int32 money_rune_stone { get; set; }

        public Int32 money_bind_diamond { get; set; }

        public Int32 fortress_wood { get; set; }

        public Int32 fortress_stone { get; set; }

        public Int32 friend_give_count { get; set; }

        public UInt16 server_id { get; set; }

        public byte vip_level { get; set; }

        public string cross_uuid { get; set; }

        public Int32 energy { get; set; }

        public Int32 honour { get; set; }

        public Int32 power { get; set; }

        /// <summary>
        /// 提交制造专精
        /// </summary>
        public Int32 mfg_talent { get; set; }

        public byte hp_bottle_cnt { get; set; }

        public UInt32 fight_force { get; set; }
        public UInt32 fight_max { get; set; }

        public UInt32 equip_show_chest { get; set; }
        public UInt32 equip_show_arm { get; set; }
        public UInt32 equip_show_leg { get; set; }
        public UInt32 equip_show_weapon { get; set; }

        public string facade { get; set; }
        /// <summary>
        /// 装备的宝石特效
        /// </summary>
        public UInt32 equiped_stone_effect { get; set; }

        /// <summary>
        /// 当前的祝福等级
        /// </summary>
        public UInt32 spirit_level { get; set; }

        /// <summary>
        /// 当前的祝福值
        /// </summary>
        public UInt32 spirit_bless { get; set; }

        /// <summary>
        /// 装备的强化特效
        /// </summary>
        public UInt32 equiped_strength_effect { get; set; }

        /// <summary>
        /// 装备的套装特效
        /// </summary>
        public UInt32 equiped_suit_effect { get; set; }

        /// <summary>
        /// 时装特效
        /// </summary>
        public UInt32 equiped_fashion_effect { get; set; }

        public UInt32 cur_bag_count { get; set; }

        public UInt32 mail_daily_count { get; set; }

        public UInt32 daily_active_point { get; set; }

        public UInt32 active_point { get; set; }

        // 玩家活跃值
        public UInt32 achievement_point { get; set; }

        /// <summary>
        /// 玩家当天在线时长
        /// </summary>
        public UInt32 online_time_day { get; set; }

        public UInt16 auction_want_buy_times { get; set; }
        public LuaTable rune_time { get; set; }
        public UInt32 team_id { get; set; }

        public UInt32 mfg_skill_level { get; set; }
        public UInt32 mfg_skill_exp { get; set; }
        public UInt16 mission_mop_up_num { get; set; }

        public UInt16 mission_reset_times { get; set; }

        /// <summary>
        /// 恶魔之门,参加次数,globalBaseMgr同步过来,且cell和客户端需要知道
        /// </summary>
        public byte evil_mission_cnt { get; set; }

        public Int16 spell_energy { get; set; }

        public string guild_name { get; set; }

        public UInt64 guild_id { get; set; }

        public UInt16 spell_proficient { get; set; }

        /// <summary>
        /// 交易市场解锁格子数量
        /// </summary>
        public UInt32 auction_ex_grid_cnt { get; set; }

        /// <summary>
        /// 拥有星魂数量
        /// </summary>
        public UInt32 star_spirit_num { get; set; }

        /// <summary>
        /// 商城每日赠送金额,钻石,需限制 
        /// </summary>
        public UInt32 market_daily_give_price { get; set; }

        /// <summary>
        /// 商城个人消耗历史总金额 
        /// </summary>
        public UInt32 market_total_consume_price { get; set; }

        /// <summary>
        /// 玩家充值总额
        /// </summary>
        public UInt32 accumulated_charge { get; set; }

        /// <summary>
        /// 当前已出战的宠物ID
        /// </summary>
        public UInt64 active_pet_entity_id { get; set; }

        /// <summary>
        /// 登陆的天数
        /// </summary>
        public UInt32 login_day_count { get; set; }

        /// <summary>
        /// 自动同步的跟随序号
        /// </summary>
        public byte team_follow_num { get; set; }

        public uint treasure_map_count { get; set; }

        public uint chest_times { get; set; }

        public uint duelchest_times { get; set; }

        public UInt16 game_step { get; set; }

        /// <summary>
        /// 今日已炼金次数
        /// </summary>
        public byte alchemy_cnt { get; set; }

        public byte alchemy_cur_cnt { get; set; }

        /// <summary>
        /// 最后一次炼金的时间戳（秒）
        /// </summary>
        public uint alchemy_time_stamp { get; set; }

        /// <summary>
        /// 今日已协助炼金次数
        /// </summary>
        public uint assist_alchemy_cnt { get; set; }

        public Int32 lucky_coin { get; set; }

        /// <summary>
        /// 当前玩家身上的坐骑
        /// </summary>
        public byte ride_id { get; set; }

        /// <summary>
        /// 当前启用的坐骑
        /// </summary>
        public byte active_ride_id { get; set; }

        /// <summary>
        /// 强制跟随的玩家id
        /// </summary>
        public UInt32 owner_id { get; set; }

        /// <summary>
        /// 是否领取了创角活动(七天)的终极大奖
        /// </summary>
        public byte create_activity_final_flag { get; set; }

        /// <summary>
        /// 创建时间  客户端需要 创角活动(七天)
        /// </summary>
        public UInt32 create_time { get; set; }

        public byte tutor_level { get; set; }
        public Int32 tutor_exp { get; set; }
        public Int32 tutor_value { get; set; }
        public Int32 tutor_exp_day { get; set; }
        public Int32 tutor_value_day { get; set; }
        public UInt32 tutor_reward { get; set; }

        public uint duel_rank_show { get; set; }
        public uint duel_rank_true { get; set; }
        public Int16 duel_rank_level { get; set; }
        public UInt16 duel_match_count { get; set; }
        #endregion

        public AvatarEquipManager equipManager;
        public RideManager rideManager;
        public FollowerManager followerManager;
        protected SyncPosManager _syncPosManager;
        protected bool _isFollower = false;
        protected bool _beControlled = false;
        
        public EntityAvatar()
        {
            this.entityType = EntityConfig.ENTITY_TYPE_AVATAR;
        }

        public override void OnEnterWorld()
        {
            initAvatarData();
            equipManager = new AvatarEquipManager(this);
            rideManager = new RideManager(this);
            followerManager = new FollowerManager(this);
            base.OnEnterWorld();
            this.moveManager.accordingMode = CombatSystem.AccordingMode.AccordingEntity;
            this.moveManager.defaultAccordingMode = CombatSystem.AccordingMode.AccordingEntity;
            vehicleManager = new VehicleManager(this);
            GameRenderManager.GetInstance().OnAvatarEnterWorld(this);
            AvatarManager.GetInstance().OnAvatarEnterWorld(this);
            _syncPosManager = new SyncPosManager(this);
        }

        public override void OnLeaveWorld()
        {
            rideManager.Release();
            followerManager.Release();
            base.OnLeaveWorld();
            if (GameSceneManager.GetInstance().inCombatScene)
            {
                EventDispatcher.TriggerEvent<uint>(BattleUIEvents.REMOVE_LIFE_BAR_FOR_ENTITY, id);
            }
            equipManager.Release();
            vehicleManager.Release();
            GameRenderManager.GetInstance().OnAvatarLeaveWorld(this);
            AvatarManager.GetInstance().OnAvatarLeaveWorld(this);
            _syncPosManager.Release();
        }

        public override void OnEnterSpace()
        {
            base.OnEnterSpace();
            followerManager.OnEnterSpace();
        }

        public override void OnLeaveSpace()
        {
            base.OnLeaveSpace();
            vehicleManager.OnLeaveSpace();
        }

        public override int GetSpaceQualitySetting()
        {
            return GameSceneManager.GetInstance().GetQualitySetting(InstanceDefine.QUALITY_SETTING_AVATAR);
        }

        public override ACTActor controlActor
        {
            get
            {
                return (rideManager.ride == null) ? actor : rideManager.ride;
            }
        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (this.actor != null)
            {
                this.actor.gameObject.name = this.name + "-" + this.id;
                this.actor.actorController.usePlayerController = false;
				equipManager.OnActorLoaded();
                followerManager.OnActorLoaded();
                this.actor.actorState.BaseSpeed = role_data_helper.GetSpeed((int)this.vocation) * 0.01f;
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingAvatar(this), this.actor.gameObject);
				visible = false;
            }
            if (this.actor != null && GameSceneManager.GetInstance().inCombatScene)
            {
                RefreshLifeBarVisible();
            }
            ResetCityState();
        }

        public override bool CanTouch()
        {
            if (GameSceneManager.GetInstance().IsCanTouch())
            {
                return true;
            }
            return base.CanTouch();
        }

        protected void ResetCityState()
        {
            ACTSystemAdapter.SetActorCityState(actor, !GameSceneManager.GetInstance().inCombatScene, (int)this.vocation, () =>
            {
                OnControllerLoaded();
            });
        }

        virtual protected void OnControllerLoaded()
        {
            visible = true;
            rideManager.OnControllerLoaded();
        }

        public string GetAttribute2String(attri_config attributeId,bool isFloat = true)
        {
            float attribute = GetAttribute(attributeId);
            string result;
            if(isFloat)
            {
                attribute *= 100;
                result = string.Concat(attribute.ToString("0.00"), "%");
            }
            else
            {
                result = attribute.ToString();
            }
            return result;
        }

        public void initAvatarData()
        {
            actorID = role_data_helper.GetModel((int)this.vocation);
        }

        public override int GetCampPveType()
        {
            var campInfo = GameSceneManager.GetInstance().GetCampInfo(this.map_camp_id);
            return campInfo!=null?campInfo.camp_pve_type_i:0;
        }

        public override int GetCampPvpType()
        {
            var campInfo = GameSceneManager.GetInstance().GetCampInfo(this.map_camp_id);
            return campInfo != null ? campInfo.camp_pvp_type_i : 0;
        }

        public override string GetDefaultLayer()
        {
            return PhysicsLayerDefine.Avatar;
        }

        public void ChangeAI()
        {
            if (aiID >= 0)
            {
                entityAI = new EntityPlayerAI(this);
                entityAI.SetBehaviorTree((uint)aiID);
            }
            if (aiID < 0)
            {
                entityAI = null;
            }
        }

        public void SetDefaultSkill()//目前无技能系统，数据临时写死，用于测试，后面需要去掉
        {
            List<int> learnedSkills = new List<int>();
            if (this.vocation == Vocation.Warrior)
            {
                learnedSkills = new List<int>() { 21, 41, 61, 81, 101, 121, 141, 161, 181, 201, 221 };
            }
            else if (this.vocation == Vocation.Archer)
            {
                learnedSkills = new List<int>() { 6001, 6021, 6041, 6061, 6081, 6101, 6121, 6141,
                                                                    6161, 6181, 6201, 6221};//, 6241, 6261, 6281, 6301, 6321};
            }
            else if (this.vocation == Vocation.Wizard)
            {
                learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4121, 4141, 4161, 4181, 4201};
            }
            else if (this.vocation == Vocation.Assassin)
            {
                learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4241, 4261, 4281, 4301};
            }
            this.skillManager.SetLearnedSkillList(learnedSkills);
        }

        public override void SetPosition(Vector3 newPosition)
        {
            base.SetPosition(newPosition);
        }

        protected override int GetRandomSpeechID(int id)
        {
            return role_data_helper.GetRandomSpeechID((int)this.vocation, id);
        }

        public override void OnDeath()
        {
            rideManager.OnDeath();
            base.OnDeath();
        }

        public bool IsFollower()
        {
            return _isFollower;
        }

        public bool EnterFollower()
        {
            if (_isFollower)
            {
                return false;
            }
            _isFollower = true;
            _syncPosManager.OnEnterFollower();
            return true;
        }

        public bool LeaveFollower()
        {
            if (!_isFollower)
            {
                return false;
            }
            _isFollower = false;
            _syncPosManager.OnLeaveFollower();
            return true;
        }

        public bool EnterBeControlled()
        {
            if (_beControlled)
            {
                return false;
            }
            _beControlled = true;
            return true;
        }

        public bool LeaveBeControlled()
        {
            if (!_beControlled)
            {
                return false;
            }
            _beControlled = false;
            return true;
        }

        public bool BeControlled()
        {
            return _beControlled;
        }
    }
}
