﻿using ACTSystem;
using Common.ClientConfig;
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using MogoEngine;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/20 15:49:00 
 * function: 
 * *******************************************************/

namespace GameMain.Entities
{
    public class EntityDropItem : EntityDropItemBase
    {
        #region def属性
        public UInt32 owner_id { set; get; }
        public byte owner_type { set; get; }
        public byte owner_pvp_camp { set; get; }
        public byte owner_pve_camp { set; get; }
        public byte pickup_type { set; get; }
        public byte handle_type { set; get; }
        #endregion
        public bool isServerItem = true;
        public float duration = 0;
        private uint _pickerId = 0;
        private float _timeStamp = 0;
        private const int HANDLE_TYPE_PICK_UP = 1;

        public EntityDropItem()
            : base()
        {
            entityType = EntityConfig.ENTITY_TYPE_NAME_DROP_ITEM;
        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            _timeStamp = Time.realtimeSinceStartup;
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            if (_pickerId == 0)
            {
                DestroyModel();
            }
            else
            {
                FlyAnimation();
            }
        }

        protected override void OnModelLoaded()
        {
            base.OnModelLoaded();
            if (isInWorld)
            {
                dropItem.approachingSpeed = 10f;
            }
            if (handle_type == HANDLE_TYPE_PICK_UP)
            {
                AddBoxCollider();
                SetTouchEnabled(true);
            }
            else
            {
                SetTouchEnabled(false);
            }
        }

        public bool BePicked(uint pickerId)
        {
            if (!CanBePicked(pickerId))
            {
                return false;
            }
            if (isServerItem)
            {
                SendPickReq();
                return false;
            }
            else
            {
                _pickerId = pickerId;
                MogoWorld.DestroyEntity(id);
                HandleUseEffects();
                return true;
            }
        }

        public void RecievePickResp(uint pickerId)
        {
            _pickerId = pickerId;
        }

        private void HandleUseEffects()
        {
            Dictionary<int, float> useEffects = item_helper.GetUseEffect((int)type_id);
            var en = useEffects.GetEnumerator();
            while (en.MoveNext())
            {
                HandleUseEffects(en.Current.Key, en.Current.Value);
            }
        }

        private void HandleUseEffects(int effectType, float effectValue)
        {
            if (effectType.ToString().Equals(ItemUseEffect.OBTAIN_BUFF))
            {
                var target = MogoWorld.GetEntity(_pickerId) as EntityCreature;
                target.bufferManager.AddBuffer((int)effectValue);
                LoggerHelper.Info(string.Format("掉落物{0}触发buff {1}, 添加到{2}身上", type_id, effectValue, owner_id));
            }
        }

        private void SendPickReq()
        {
            LoggerHelper.Debug(string.Format("pickup_item_req, id = {0}", id));
            PlayerAvatar.Player.RpcCall("pickup_item_req", id);
        }

        private void FlyAnimation()
        {
            if (dropItem != null)
            {
                dropItem.FlyToEntity(_pickerId, 0);
            }
        }

        protected override bool CanBePickedBySelf()
        {
			return CanBePicked(PlayerAvatar.Player.id);
        }

        private bool CanBePicked(uint pickerId)
        {
            Entity entity = MogoWorld.GetEntity(pickerId);
            if (entity == null || !(entity is EntityCreature))
            {
                return false;
            }
            EntityCreature creature = entity as EntityCreature;
            if (pickup_type == public_config.PICKUP_ITEM_MODE_ENEMY)
            {
                return CheckIsEnemy(creature);
            }
            else if (pickup_type == public_config.PICKUP_ITEM_MODE_TEAMER)
            {
                return !CheckIsEnemy(creature);
            }
            else if (pickup_type == public_config.PICKUP_ITEM_MODE_ALL)
            {
                return true;
            }
            else if (pickup_type == public_config.PICKUP_ITEM_MODE_SELF)
            {
                return pickerId == owner_id;
            }
            return false;
        }

        private bool CheckIsEnemy(EntityCreature creature)
        {
            if (owner_type == public_config.ENTITY_TYPE_MONSTER || creature is EntityMonsterBase)
            {
                return owner_pve_camp != creature.GetCampPveType();
            }
            else if (owner_type == public_config.ENTITY_TYPE_AVATAR && creature is EntityAvatar)
            {
                return owner_pvp_camp != creature.GetCampPvpType();
            }
            return false;
        }

        protected override void Update()
        {
            if (isServerItem)
            {
                return;
            }
            if (_pickerId > 0)
            {
                return;
            }
            if (duration == 0 || _timeStamp == 0)
            {
                return;
            }
            if (Time.realtimeSinceStartup - _timeStamp >= duration)
            {
                MogoWorld.DestroyEntity(id);
            }
        }

    }
}
