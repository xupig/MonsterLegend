﻿using ACTSystem;
using GameLoader.Utils;
using MogoEngine.RPC;
using System;
using UnityEngine;
using Common.ExtendTools;
using GameData;

namespace GameMain.Entities
{
    public class EntityItemBase : Entity, ITouchable
    {
        protected BoxCollider _triggerBoxCollider;

        public EntityItemBase()
        {

        }

        public bool CanTouch()
        {
            return true;
        }

        public void SetTouchEnabled(bool enabled)
        {
            if (_triggerBoxCollider == null)
            {
                return;
            }
            _triggerBoxCollider.enabled = enabled;
        }

        public override Transform GetTransform()
        {
            return base.GetTransform();
        }

        protected virtual Vector3 GetTriggerWidth()
        {
            return Vector3.one;
        }

        protected virtual void AddBoxCollider()
        {
            GameObject triggerGo;
            Transform triggerTrans = GetTransform().FindChild("trigger");
            if (triggerTrans != null)
            {
                triggerGo = triggerTrans.gameObject;
            }
            else
            {
                triggerGo = new GameObject("trigger");
                triggerGo.transform.SetParent(GetTransform(), false);
            }
            triggerGo.layer = UnityEngine.LayerMask.NameToLayer("Trigger");
            BoxCollider collider = triggerGo.GetComponent<BoxCollider>();
            if (collider == null)
            {
                collider = triggerGo.AddComponent<BoxCollider>();
            }
            collider.size = GetTriggerWidth();
            collider.center = new Vector3(0, collider.size.y / 2, 0);
            _triggerBoxCollider = collider;
            AddSelectTrigger();
        }

        private void AddSelectTrigger()
        {
            var trigger = _triggerBoxCollider.gameObject.AddComponentOnlyOne<ActorSelectTrigger>();
            trigger.id = this.id;
        }

        static RaycastHit _raycastHit = new RaycastHit();
        static int _terrainLayerValue = 1 << UnityEngine.LayerMask.NameToLayer("Terrain");
        protected virtual void FixPosition()
        {
            var actorPos = this.position;
            actorPos.y = actorPos.y + 200;
            Physics.Raycast(actorPos, Vector3.down, out _raycastHit, 250f, _terrainLayerValue);
            GetTransform().position = _raycastHit.point;
            this.position = _raycastHit.point;
        }
    }
}
