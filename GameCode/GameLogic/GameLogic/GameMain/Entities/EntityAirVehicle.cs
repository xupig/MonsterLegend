﻿#region 模块信息
/*==========================================
// 模块名：EntityAirVehicle
// 命名空间: GameMain.Entities
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/16/11
// 描述说明：空中载具
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using GameData;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Common.ExtendTools;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using SpaceSystem;
using GameLoader.Utils;

namespace GameMain.Entities
{
    public class EntityAirVehicle : EntityVehicle
    {

        public Vector3 riderPosition;
        public Vector3 attackTargetPosition = Vector3.zero;

        private AirVehiclePath _airVehiclePath;
        public AirVehiclePath airVehiclePath
        {
            get
            {
                return _airVehiclePath;
            }
        }

        public EntityAirVehicle()
        {
            _airVehiclePath = new AirVehiclePath(this);
            this.entityType = EntityConfig.ENTITY_TYPE_AIRVEHICLE;
        }

        public override void OnEnterWorld()
        {
            //获取模型ID
            actorID = monster_helper.GetMonsterModel((int)monster_id);
            base.OnEnterWorld();
            EventDispatcher.AddEventListener(VehicleEvents.CHANGE_SCENE,OnChangeScene);
            this.moveManager.accordingMode = AccordingMode.AccordingAnimationClip;
            this.moveManager.defaultAccordingMode = AccordingMode.AccordingAnimationClip;
            //先关闭掉空中载具的AI
            entityAI = null;

        }

        private void OnChangeScene()
        {
            actor.gameObject.transform.SetParent(null, false);
            ResetMainCamera();
            driver.vehicleManager.RequestDivorce();

        }

        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (actor != null)
            {
                this.actor.gameObject.name = "AirVehicle" + "-" + this.id + "-" + this.monster_id;
                this.actor.isFlyingOnVehicle = true;
                actor.gameObject.AddComponent<BHAirVehicleAnimatorEvent>();
                SetActorLayer(driver.GetActorLayer());
                driver.DriveVehicle(this);
                driver.vehicleManager.AddVehicleEventListener(this, VehicleType.AIRVEHICLE);
                riderPosition = actor.position;
                _airVehiclePath.GetPathInfo(this);
                if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingEntityVehicle(this), this.actor.gameObject);
            }
        }

        public void PauseFlyAnimation()
        {
            //Debug.LogError("[EntityAirVehicle:PauseFlyAnimation]=>1_____________airVehiclePath: " + _airVehiclePath);
            if (_airVehiclePath == null) return;
            _airVehiclePath.PauseAnimation();        
  
        }

        public void ResumeFlyAnimation()
        {
            if (_airVehiclePath == null) return;

            _airVehiclePath.ResumeAnimation();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            EventDispatcher.RemoveEventListener(VehicleEvents.CHANGE_SCENE, OnChangeScene);
        }

        public override void OnDeath()
        {
            base.OnDeath();
            driver.vehicleManager.RequestDivorce();
            
        }

        public override void DestroyEntityModel()
        {                                                   
            //Debug.LogError("[EntityAirVehicle:DestroyEntityModel]=>________________time:   " + Time.realtimeSinceStartup);
            //恢复AudioListener
            //rider.SetAudioListener(true);
            //var listener  = actor.gameObject.GetComponent<AudioListener>();
            //if (listener != null) GameObject.Destroy(listener);
            
            //rider.SetBMGSlot();
            driver.UpdateEntityComponents();
            if (_airVehiclePath != null) _airVehiclePath.SetAudioListener(false);
            ResetMainCamera();
            base.DestroyEntityModel();
            _airVehiclePath.DestroyPathModel();
        }

        private void ResetMainCamera()
        {
            CameraManager.GetInstance().StopCurrentCameraAnimation();
        }

        public bool GenRayFromCamera()
        {
            if (_airVehiclePath == null) return false;
            attackTargetPosition = _airVehiclePath.GenRayFromCamera();
            return attackTargetPosition == Vector3.zero ? false : true;
        }

        private void OnAnimationOver()
        {
            if (driver.bufferManager.ContainsBuffer(driver.vehicleBufferID))
            {
                return;
            }
            riderPosition = driver.actor.position;
            driver.vehicleManager.RequestDivorce();
        }

        public void OnAnimationEvent(string action)
        {
            if (action == "over")
            {
                OnAnimationOver();
            }
        }

        public void OnRiderEvent(int cmd)
        {
            if (driver == null)
            {
                LoggerHelper.Error("[OnRiderEvent]=>骑乘者不存在!");
                return;
            }
            driver.actor.animationController.PlayAction(EntityVehicle.RIDER_ACTION);
        }

        public override bool CanTouch()
        {
            return false;
        }
    }
}
