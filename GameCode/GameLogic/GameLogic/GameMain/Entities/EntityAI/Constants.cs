﻿using Mogo.AI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace GameMain.Entities 
{
    public class AIContainer
    {
        static public Dictionary<uint, BehaviorTreeRoot> container = new Dictionary<uint, BehaviorTreeRoot>();

        static AIContainer()
        {
        }

        public static void Init()
        {
            Assembly ass = typeof(BehaviorTreeRoot).Assembly;
            var types = ass.GetTypes();
            foreach (var item in types)
            {
                if (item.Namespace == "Mogo.AI.BT")
                {
                    var type = item.BaseType;
                    if (type == typeof(BehaviorTreeRoot))
                    {
                        var id = item.Name.Replace("BT", String.Empty);
                        uint key;
                        if (UInt32.TryParse(id, out key))
                        {
                            var p = item.GetProperty("Instance");
                            var value = p.GetGetMethod().Invoke(null, null) as BehaviorTreeRoot;
                            container.Add(key, value);
                        }
                    }
                }
            }
        }
    }
}
