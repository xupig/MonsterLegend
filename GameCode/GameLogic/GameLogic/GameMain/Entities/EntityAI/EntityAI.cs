﻿using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using Mogo.AI;
using Mogo.Game;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.Entities 
{
    public abstract class EntityAI : IAIProc
    {
        static public bool aiState = true;
        public BehaviorTreeRoot _aiRoot;
        public uint aiID = 0;
        private int _lookOnTime;
        private int _lookOnModeType = 0;
        private float _curdistance;
        protected BTBlackBoard _blackBoard = new BTBlackBoard();
        public BTBlackBoard blackBoard
        {
            get { return _blackBoard; }
        }
        protected EntityCreature _entity;
        protected float _thinkInterval = 0;
        private float _lastThinkTime = 0.0f;
        private float _lastLookOnTime = 0.1f;
        static int DEFAULT_SEARCH = 30000;

        public EntityAI(EntityCreature entity)
        {
            _entity = entity;
            _blackBoard.bornPosition = entity.position;
            _thinkInterval = global_params_helper.GetAIInterval();
        }

        public void Proc()
        {
            if (_aiRoot == null) return;
            if (_entity.actor == null) return;
            _aiRoot.Proc(this);
        }

        public virtual void Think(AIEvent triggerEvent = AIEvent.Self, bool forceThink = false)
        {
            if (!Mathf.Approximately(_blackBoard.cdTimeStamp, 0) && Time.realtimeSinceStartup >= _blackBoard.cdTimeStamp)
            {
                ProcessAITimerEvent_CDEnd();
                return;
            }
            if (Mathf.Approximately(_lastThinkTime, 0))
            {
                _lastThinkTime = Time.time + _thinkInterval;
            }
            if (Time.time > _lastThinkTime || forceThink)
            {
                if (CanThink() == 0 || forceThink)
                {
                    _lastThinkTime += _thinkInterval;
                    Proc();
                }
            }
            if (_blackBoard.inLookOn && Time.time > _lastLookOnTime)
            {
                _lastLookOnTime += 0.1f;
                try
                {
                    ProcessAITimerEvent_PatrolLookOnEnd();
                }
                catch
                {
                    _blackBoard.inLookOn = false;
                }
            }
        }

        public virtual int CanThink()
        {
            if (!aiState) return 1;
            if (_entity.actor == null) return 1;
            if (_entity.entityAI == null) return 1;
            if (!_entity.stateManager.CanDO(CharacterAction.THINK_ABLE)) return 1;
            if (_blackBoard.aiState != AIState.THINK_STATE) return 1;
            if (!_blackBoard.thinkable) return 1;
            return 0;
        }

        public bool SetBehaviorTree(uint treeNum)
        {
            if (AIContainer.container.ContainsKey(treeNum))
            {
                _aiRoot = AIContainer.container.Get(treeNum);
                aiID = treeNum;
                return true;
            }
            return false;
        }

        public virtual bool ProcAOI(int searchChange, int targetType, int targetRangeType, string targetRangeArgs, int targetProirity)
        {
            if (_blackBoard.enemyId > 0)    //已经搜索过目标，需要判断是否需要重新搜索目标
            {
                Entity enemyEntity = MogoWorld.GetEntity(_blackBoard.enemyId);
                if (enemyEntity != null)
                {
                    int result = MogoMathUtils.Random(1, 101);
                    if (result > searchChange)  //该概率下不需要重新搜索目标
                    {
                        return true;
                    }
                }
            }

            _blackBoard.enemyId = 0;
            var transform = _entity.GetTransform();
            if (transform == null) return false;
            var aoiTarget = GetAOITarget(targetType, targetRangeType, targetRangeArgs, targetProirity, transform);
            if (aoiTarget != null)
            {
                _blackBoard.enemyId = aoiTarget.id;
            }
            EventDispatcher.TriggerEvent<uint, uint>(AIEvents.AIEvents_OnChangeAITarget, _entity.id, _blackBoard.enemyId);
            if (_blackBoard.enemyId == 0)
                return false;
            else
            {
                return true;
            }
        }

        private EntityCreature GetAOITarget(int targetType, int targetRangeType, string targetRangeArgs, int targetProirity, Transform transform)
        {
            TargetRangeType rangeType = (targetRangeType == 0) ? TargetRangeType.CircleRange : TargetRangeType.SectorRange;
            TargetType targetCampType = TargetType.EnemyAndFriend;
            if (targetType == 1) targetCampType = TargetType.Enemy;
            else if (targetType == 2) targetCampType = TargetType.Friend;

            List<int> targetRangeParam = data_parse_helper.ParseListInt(targetRangeArgs.Split(','));
            if (targetRangeParam[0] == 0) targetRangeParam[0] = DEFAULT_SEARCH;
            var entities = EntityFilter.GetEntitiesByRangeType(transform.localToWorldMatrix, rangeType, targetRangeParam, null);
            entities = TargetFilter.GetCreaturesByTargetType(_entity, entities, targetCampType);
            return GetCreatureByTargetProirity(entities, targetProirity, targetCampType);
        }

        virtual protected EntityCreature GetCreatureByTargetProirity(List<uint> entities, int targetProirity, TargetType targetCampType)
        {
            if (targetProirity == 1)    //优先玩家
            {
                if (entities.Contains(PlayerAvatar.Player.id))
                {
                    return PlayerAvatar.Player;
                }
            }
            else if (targetProirity == 3)   //优先宠物
            {
                uint curPetEntID = PlayerPetManager.GetInstance().curPetEntityID;
                if (curPetEntID > 0 && entities.Contains(curPetEntID))
                {
                    return MogoWorld.GetEntity(curPetEntID) as EntityCreature;
                }
            }
            else if (targetProirity == 2)   //优先怪物
            {
                if (entities.Contains(PlayerAvatar.Player.id))
                {
                    uint curPetEntID = PlayerPetManager.GetInstance().curPetEntityID;
                    if (curPetEntID > 0 && entities.Contains(curPetEntID))
                    {
                        if (entities.Count == 2)
                        {
                            entities.Remove(PlayerAvatar.Player.id);
                            entities.Remove(curPetEntID);
                        }
                    }
                    else
                    {
                        if (entities.Count == 1)
                        {
                            entities.Remove(PlayerAvatar.Player.id);
                        }
                    }
                }
            }
            var aoiTarget = TargetFilter.GetClosestCreature(this._entity, entities, targetCampType);
            return aoiTarget;
        }

        public virtual bool ProcPetAOI(int chance, int targetType = 1, int dis = 100, int iCenterX = 0, int iCenterZ = 0, int monsterTypePriority = 0)
        {
            return false;
        }

        public virtual Transform GetEnemyTransform()
        {
            if (_blackBoard.enemyId == 0) return null;
            Entity entity = MogoWorld.GetEntity(_blackBoard.enemyId);
            if (entity == null) return null;
            return entity.GetTransform();
        }

        public virtual BTBlackBoard GetBlackBoard()
        {
            return _blackBoard;
        }

        public virtual bool ProcChooseCastPoint(int skillIdx)
        {
            var castRange = GetSkillRange(skillIdx);
            var enemy = GetTargetEntity();
            if (enemy == null) return false;
            if (_entity is EntityAvatar)
            {
                return GetMovePointStraight(castRange * 0.7f);
            }
            else
            {
                return GetMovePointByBlock(castRange * 0.7f);
            }
        }

        public virtual void ProcMoveToAppoint(string posString)
        {
            Vector3 pos = new Vector3(float.Parse(posString.Split(',')[0]), float.Parse(posString.Split(',')[1]), float.Parse(posString.Split(',')[2]));
            _blackBoard.movePoint = new Vector3(pos.x, _entity.position.y, pos.z);

            float tmpDis = Vector3.Distance(_entity.position, new Vector3(_blackBoard.movePoint.x, _entity.position.y, _blackBoard.movePoint.z));
            if (tmpDis < 0.5f)
            {
                _entity.moveManager.Stop();
            }
        }

        public virtual void ProcMoveToTargetPoint()
        {
            _blackBoard.movePoint = MogoEngine.MogoWorld.GetEntity(_blackBoard.enemyId).position;
        }

        public virtual bool ProcInSkillRange(int skillIdx)
        {
            var castRange = GetSkillRange(skillIdx);
            var enemy = GetTargetEntity();
            if (enemy == null) return false;
            float entityRadius = enemy.hitRadius;
            float curdistance = Vector3.Distance(_entity.position, enemy.position) - entityRadius;
            if (curdistance > castRange) return false;
            return true;
        }

        public virtual bool ProcInSkillCoolDown(int skillIdx)
        {
            bool result = false;
            var skillId = GetSkillIDByIdx(skillIdx);
            if (skillId == 0) { result = true; }
            else
            {
                result = !_entity.skillManager.IsInCommonCD(skillId) && !_entity.skillManager.IsInCD(skillId);
            }
            return result;
        }

        public virtual void ProcMoveTo(float sec)
        {
            if (!_entity.actor.animationController.IsReady()) return ;

            if (!_entity.CanActiveMove()) return;
            float speed  = 0f;
            if(sec > 0f)
            {
                speed = (this._entity.speed * sec) * 0.01f;
            }
            else
            {
                speed = this._entity.speed * 0.01f;
            }

            _entity.moveManager.Move(_blackBoard.movePoint, speed);
            EventDispatcher.TriggerEvent<uint>(AIEvents.AIEvents_OnAIMove, _entity.id);
        }

        public virtual void ProcEnterRest(uint msec)
        {
            if (_blackBoard.timeoutId > 0)
                TimerHeap.DelTimer(_blackBoard.timeoutId);
            _blackBoard.ChangeState(AIState.REST_STATE);
            _blackBoard.timeoutId = TimerHeap.AddTimer(msec, 0, ProcessAITimerEvent_RestEnd);
        }

        public virtual void ProcEnterCD(int msec)
        {
            if (blackBoard.timeoutId > 0)
                TimerHeap.DelTimer(blackBoard.timeoutId);

            blackBoard.ChangeState(Mogo.AI.AIState.CD_STATE);

            int tmpCDTime = msec + (int)blackBoard.skillActTime;

            blackBoard.skillActTime = (uint)tmpCDTime;
            blackBoard.cdTimeStamp = Time.realtimeSinceStartup + tmpCDTime * 0.001f;
            //blackBoard.timeoutId = TimerHeap.AddTimer(blackBoard.skillActTime, 0, ProcessAITimerEvent_CDEnd);
        }

        public virtual bool ProcCastSpell(int skillIdx, int reversal)
        {
            var skillId = GetSkillIDByIdx(skillIdx);
            if (skillId == 0) return false;
            var target = GetTargetEntity();
            if (_entity.actor != null && target != null && target.actor != null)
            {
                _entity.actor.FaceTo(target.actor);
            }
            _entity.moveManager.Stop();
            bool result = _entity.skillManager.PlaySkill(skillId);
            blackBoard.skillActTime = (uint)GetTotalActionDuration(skillId);
            return result;
        }

        public virtual int GetEnemyNum()
        {
            return 0;
        }

        public virtual bool ProcIsTargetCanBeAttack()
        {
            var target = GetTargetEntity();
            if (target == null) return false;
            if (target.stateManager.InState(state_config.AVATAR_STATE_DEATH)) return false;
            return true;
        }

        public virtual bool ProTestBuff(int id)
        {
            return _entity.bufferManager.ContainsBuffer(id);
        }

        public virtual void ProcSelectAutoFightMovePoint()
        {
            var movePos = GameSceneManager.GetInstance().GetAutoMovePoint();
            movePos.y = _entity.position.y;
            if (!_entity.actor.animationController.IsReady()) return;
            if (!_entity.CanActiveMove()) return;
            float speed = _entity.speed * 0.01f;
            //Debug.LogError("ProcMoveTo::" + this.entity.id + "::" + _blackBoard.movePoint);
            _entity.moveManager.Move(movePos, speed);
        }

        public virtual void ProcEnterFight()
        {
            _blackBoard.inFightingState = true;
        }

        public virtual void ProcCancelFightState()
        {
            _blackBoard.inFightingState = false;
        }

        #region 观望
        public virtual void ProcLookOn()
        {
            if (_entity == null || _entity.GetTransform() == null)
            {
                return;
            }
            var enemy = GetTargetEntity();
            if (enemy == null)
            {
                return;
            }
            float curdistance = Vector3.Distance(_entity.actor.position, enemy.actor.position);

            _curdistance = curdistance;
            if ((curdistance - 0.1f) > blackBoard.LookOn_DistanceMin && (curdistance - 0.1f) > blackBoard.LookOn_DistanceMax)
            { //不能远离了，把远离左右移动概率填0，把接近设成100，冲锋不变，只能接近，冲锋
                blackBoard.LookOn_ModePercent[0] = 100;
                blackBoard.LookOn_ModePercent[1] = 0;
                blackBoard.LookOn_ModePercent[2] = 0;
                blackBoard.LookOn_ModePercent[3] = 0;
                blackBoard.LookOn_ModePercent[4] = 0;
            }
            if ((curdistance - 0.1f) < blackBoard.LookOn_DistanceMax && (curdistance - 0.1f) < blackBoard.LookOn_DistanceMin)
            { //不能接近了，把远离设成100，其它设成0
                blackBoard.LookOn_ModePercent[0] = 0;
                blackBoard.LookOn_ModePercent[1] = 100;
                blackBoard.LookOn_ModePercent[2] = 0;
                blackBoard.LookOn_ModePercent[3] = 0;
                blackBoard.LookOn_ModePercent[4] = 0;
                blackBoard.LookOn_ModePercent[5] = 0;
            }

            int sum = 0;
            for (int i = 0; i < blackBoard.LookOn_ModePercent.Length; i++)
            {
                sum += blackBoard.LookOn_ModePercent[i];
            }
            int randomV = Mogo.Util.RandomHelper.GetRandomInt(sum);
            sum = 0;
            //该处随机模式有问题，需重新计算
            for (int i = 0; i < blackBoard.LookOn_ModePercent.Length; i++)
            {
                sum += blackBoard.LookOn_ModePercent[i];
                if (randomV <= sum)
                {
                    blackBoard.LookOn_Mode = i;
                    break;
                }
            }
            if (_blackBoard.inLookOn) return;
            switch (blackBoard.LookOn_Mode)
            {
                case 0:
                    blackBoard.speedFactor = InstanceDefine.SPEED_FACTOR_MODE_0;
                    ChangeLookOnState(blackBoard.LookOn_Mode);
                    break;
                case 1:
                    blackBoard.speedFactor = InstanceDefine.SPEED_FACTOR_MODE_1;
                    ChangeLookOnState(blackBoard.LookOn_Mode);
                    break;
                case 2:
                    blackBoard.speedFactor = InstanceDefine.SPEED_FACTOR_MODE_2_3;
                    _entity.moveManager.MoveByDirection(enemy.id, -1.2f, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], true);
                    ChangeLookOnState(blackBoard.LookOn_Mode);
                    break;
                case 3:
                    blackBoard.speedFactor = InstanceDefine.SPEED_FACTOR_MODE_2_3;
                    _entity.moveManager.MoveByDirection(enemy.id, 1.2f, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], true);
                    ChangeLookOnState(blackBoard.LookOn_Mode);
                    break;
                case 4:
                    float timeStanderd = blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode];
                    float time = Mogo.Util.RandomHelper.GetRandomFloat(timeStanderd * 0.8f, timeStanderd * 1.2f);
                    ProcEnterRest((uint)(time * 1000.0f));
                    break;
                case 5:
                    break;
            }
        }
        #endregion

        void ChangeLookOnState(int modeType)
        {
            _blackBoard.ChangeState(Mogo.AI.AIState.PATROL_CD_STATE);
            _lookOnTime = (int)(blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode] * 1000);
            _lookOnModeType = modeType;
            _blackBoard.inLookOn = true;
        }

        public virtual bool ProcPatrol(int min, int max, int range)
        {
            if (_blackBoard.HatredCount() > 0)
                return false;

            //冷却totleUseTime时间
            if (_blackBoard.timeoutId > 0)
            {
                TimerHeap.DelTimer(_blackBoard.timeoutId);
            }
            if (!_entity.CanActiveMove()) return false;
            _blackBoard.ChangeState(Mogo.AI.AIState.PATROL_CD_STATE);
            Vector3 movePoint = new Vector3();
            var rangeMeter = range * 0.01f;
            movePoint.x = _blackBoard.bornPosition.x + UnityEngine.Random.Range(0, rangeMeter);
            movePoint.y = this._entity.position.y;
            movePoint.z = _blackBoard.bornPosition.z + UnityEngine.Random.Range(0, rangeMeter);
            _blackBoard.movePoint = movePoint;
            this._entity.FaceToPosition(_blackBoard.movePoint);
            this._entity.moveManager.MoveByLine(_blackBoard.movePoint, 2);
            int patrolTime = UnityEngine.Random.Range(min, max);
            _blackBoard.timeoutId = TimerHeap.AddTimer((uint)patrolTime, 0, ProcessAITimerEvent_PatrolCDEnd);
            return true;
        }

        public virtual float GetHpPercent()
        {
            return (float)_entity.cur_hp / (float)_entity.max_hp;
        }

        public virtual float GetAttriValue(int attriID)
        {
            return _entity.GetAttribute((fight_attri_config)attriID);
        }

        public virtual UnityEngine.Transform GetTransform()
        {
            return this._entity.GetTransform();
        }

        public virtual int Random(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public virtual bool FindTargetByMonsterID(int monsterId, int radius)
        {
            _blackBoard.enemyId = 0;
            var transform = _entity.GetTransform();
            if (transform == null) return false;
            var entities = EntityFilter.GetEntitiesInCircleRange(transform.localToWorldMatrix, radius, 0, 0, 0);

            for (int i = entities.Count-1; i>=0; i--)
            {
                var monster = MogoWorld.GetEntity(entities[i]) as EntityMonsterBase;
                if (monster == null || monster.monster_id != monsterId) { entities.RemoveAt(i); };
            }
            var creature = TargetFilter.GetClosestCreature(this._entity, entities, TargetType.NoTarget);
            if (creature != null) { _blackBoard.enemyId = creature.id; }
            if (_blackBoard.enemyId > 0) return true;
            return false;
        }

        public virtual bool FindTargetBySkillRange(int skillIndex)
        {
            _blackBoard.enemyId = 0;
            var transform = this._entity.GetTransform();
            if (transform == null) return false;
            float dis = GetSkillRange(skillIndex);
            var entities = EntityFilter.GetEntitiesInCircleRange(transform.localToWorldMatrix, dis, 0, 0, 0);
            if (entities.Contains(this._entity.id)) entities.Remove(this._entity.id);
            var targetType = GetSkillTargetType(skillIndex);
            var creature = TargetFilter.GetClosestCreature(this._entity, entities, targetType);
            if (creature != null) { _blackBoard.enemyId = creature.id; }
            if (_blackBoard.enemyId > 0) return true;
            return false;
        }


        public virtual bool ProcChaseMaster(int distance, bool isForce) 
        {
            return false;
        }

        public virtual bool ProcMonsterBackBornPoint(bool isForce, int distance)
        {
            if(isForce)
            {
                _entity.entityAI.blackBoard.movePoint = _entity.entityAI.blackBoard.bornPosition;
                _entity.moveManager.Move(_entity.entityAI.blackBoard.bornPosition, _entity.speed);
                float currdis = Vector3.Distance(_entity.position, _entity.entityAI.blackBoard.bornPosition);
                if (currdis <= (distance/100))
                {
                    _entity.moveManager.Stop();
                    return false;
                }
                return true;
            }
            return false;
        }

        public virtual float GetMasterDistance() 
        {
            return -1;
        }

        public virtual bool ProcIsInChase()
        {
            return false;
        }

        public virtual bool ProcIsForceBackState()
        {
            return true;
        }

        public virtual bool ProIsFightState()
        {
            return _blackBoard.inFightingState;
        }

        public virtual int GetSkillIDByIdx(int skillIdx)
        {
            int result = 0;
            if (this._entity is EntityAvatar || this._entity is EntityPuppet)
            {
                result = _entity.skillManager.GetSkillIDByPos(skillIdx);
            }
            else {
                result = _entity.skillManager.GetSkillIDByIndex(skillIdx);
            }
            return result;
        }

        public float GetSkillRange(int skillIdx)
        {
            float distance = 2f;
            var skillId = GetSkillIDByIdx(skillIdx);
            if (skillId == 0) return distance;
            distance = spell_helper.GetAIRange(skillId);
            return distance;
        }

        public TargetType GetSkillTargetType(int skillIdx)
        {
            var skillId = GetSkillIDByIdx(skillIdx);
            if (skillId == 0) {
                return TargetType.NoTarget; 
            }
            return (TargetType)spell_helper.GetSearchTargetType(skillId);
        }

        public float GetTotalActionDuration(int skillId)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillId);
            if (skillData != null && skillData.cd != null)
            {
                var cd = skillData.cd[1];
                var cutTime = skillData.actionCutTime;
                return Mathf.Min(cd, cutTime);
            }
            return 0;
        }

        public EntityCreature GetTargetEntity()
        {
            if (MogoWorld.GetEntity(_blackBoard.enemyId) == null)
            {
                //LoggerHelper.Error("GetTargetEntity： this entity is destroy");
                return null;
            }
            return MogoWorld.GetEntity(_blackBoard.enemyId) as EntityCreature;
        }

        public void StopThinking()
        {
            _blackBoard.aiState = AIState.REST_STATE;
        }

        public void RecoveryThinking()
        {
            _blackBoard.aiState = AIState.THINK_STATE;
        }

        public void ProcessAITimerEvent_RestEnd()
        {
            _blackBoard.LookOn_Mode = -1;
            _blackBoard.timeoutId = 0;
            _blackBoard.ChangeState(AIState.THINK_STATE);
        }

        public void ProcessAITimerEvent_CDEnd()
        {
            blackBoard.timeoutId = 0;
            blackBoard.cdTimeStamp = 0;
            blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
            Think(AIEvent.CDEnd);
        }

        public void ProcessAITimerEvent_PatrolCDEnd()
        {
            _blackBoard.timeoutId = 0;
            _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
        }

        public void ProcessAITimerEvent_PatrolLookOnEnd()
        {
            var enemy = GetTargetEntity();

            float curdistance = Vector3.Distance(this._entity.actor.position, enemy.actor.position);
            if (_lookOnModeType == 0)
            {
                if ((_curdistance - 0.1f) > blackBoard.LookOn_DistanceMax)
                {
                    if ((curdistance - 0.1f) <= blackBoard.LookOn_DistanceMax)
                    {
                        this._entity.moveManager.Stop();
                        _blackBoard.inLookOn = false;
                        _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
                        return;
                    }
                    else
                    {
                        this._entity.moveManager.MoveByDirection(enemy.id, _entity.moveManager.curMoveSpeed, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], false);
                    }
                }
                else
                {
                    if ((curdistance - 0.1f) <= blackBoard.LookOn_DistanceMin)
                    {
                        this._entity.moveManager.Stop();
                        _blackBoard.inLookOn = false;
                        _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
                        return;
                    }
                    else
                    {
                        this._entity.moveManager.MoveByDirection(enemy.id, _entity.moveManager.curMoveSpeed, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], false);
                    }
                }
            }
            if (_lookOnModeType == 1)
            {
                if ((_curdistance - 0.1f) < blackBoard.LookOn_DistanceMin)
                {
                    if ((curdistance - 0.1f) >= blackBoard.LookOn_DistanceMin)
                    {
                        this._entity.moveManager.Stop();
                        _blackBoard.inLookOn = false;
                        _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
                        return;
                    }
                    else
                    {
                        this._entity.moveManager.MoveByDirection(enemy.id, -1.2f, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], false);
                    }
                }
                else
                {
                    if ((curdistance + 0.1f) >= blackBoard.LookOn_DistanceMax)
                    {
                        this._entity.moveManager.Stop();
                        _blackBoard.inLookOn = false;
                        _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
                        return;
                    }
                    else
                    {
                        this._entity.moveManager.MoveByDirection(enemy.id, -1.2f, blackBoard.LookOn_ModeInterval[blackBoard.LookOn_Mode], false);
                    }
                }
            }
            if(_lookOnTime <= 0)
            {
                this._entity.moveManager.Stop();
                _blackBoard.inLookOn = false;
                _blackBoard.ChangeState(Mogo.AI.AIState.THINK_STATE);
            }
            _lookOnTime -= 100;
        }

        public bool GetMovePointStraight(float skillRange)
        {
            // LoggerBuilder.Error("GetMovePointStraight");
            var enemy = GetTargetEntity();
            if (enemy == null)
                return false;

            if (enemy != null)
            {
                float distance = skillRange;

                float entityRadius = enemy.hitRadius;// 0.5f;//enemy.MonsterData.scaleRadius;

                float curdistance = Vector3.Distance(_entity.position, enemy.position) - entityRadius;
                
                if (distance > curdistance)// || curdistance-distance < 0.1f
                {
                    this._entity.moveManager.Stop();
                    return false;
                }
                else
                {
                    _blackBoard.navTargetDistance = (uint)skillRange + (uint)entityRadius;
                    var direction = _entity.position - enemy.position;
                    _blackBoard.movePoint = enemy.position + direction.normalized * distance;
                    if (!GameSceneManager.GetInstance().CheckCurrPointIsCanMove(_blackBoard.movePoint.x, _blackBoard.movePoint.z, _entity.actor.modifyLayer))
                    {
                        Vector3 pos = MogoEngine.MogoWorld.GetEntity(_blackBoard.enemyId).position;
                        _blackBoard.movePoint.x = pos.x;
                        _blackBoard.movePoint.z = pos.z;
                    }
                }

                float tmpDis = Vector3.Distance(_entity.position, new Vector3(_blackBoard.movePoint.x, _entity.position.y, _blackBoard.movePoint.z));
                if (tmpDis < 0.5f)
                {
                    this._entity.moveManager.Stop();
                    return false;
                }
                return true;
            }
            else
                return false;
        }

        #region 根据格子获取移动目标点
        public bool GetMovePointByBlock(float skillRange)
        {
            var enemy = GetTargetEntity();
            if (enemy == null)
                return false;

            if (enemy != null)
            {
                if (skillRange <= 0.5f)
                {
                    LoggerHelper.Debug(string.Format("id{0}的实体寻路通过格子获取移动目标点失败，因为技能范围为{1}，小于0.5", _entity.id, skillRange));
                    return false;
                }

                int skillRangeInt = (int)skillRange;

                float entityRadius = enemy.hitRadius;
                float curdistance = Vector3.Distance(_entity.position, enemy.position) - entityRadius;

				if (skillRange >= curdistance)
                {
                    _entity.moveManager.Stop();
                    return false;
                }
                else
                {
                    Vector2 targetBlock = GetTargetBlock(skillRangeInt, enemy);
                    if (targetBlock == Vector2.zero)
                    {
                        var direction = _entity.position - enemy.position;
						_blackBoard.movePoint = enemy.position + direction.normalized * skillRangeInt;
                        LoggerHelper.Debug(string.Format("id{0}的实体寻路通过格子获取移动目标点失败，因为找不到合适的格子,改为直接找最近点.", _entity.id));
                    }
                    else
                    {
                        _blackBoard.movePoint = new Vector3(targetBlock.x, enemy.position.y, targetBlock.y);
                    }
                }

                return true;
            }
            else
                return false;
        }

        private Dictionary<int, bool> _dummyPositionList = new Dictionary<int, bool>();
        private Vector2 GetTargetBlock(int skillRangeInt, EntityCreature enemy)
        {
            Vector2 enemyBlockPosition = new Vector2((int)enemy.position.x + 0.5f, (int)enemy.position.z + 0.5f);

            AddDummyPositionList();
            
            List<Vector2> roundBlockList = new List<Vector2>();
            for (int i = -skillRangeInt; i <= skillRangeInt; ++i)
            {
                for (int j = -skillRangeInt; j <= skillRangeInt; ++j)
                {
                    if (i > -skillRangeInt && i < skillRangeInt && j > -skillRangeInt && j < skillRangeInt)
                    {
                        continue;
                    }
                    float x = enemyBlockPosition.x + i;
                    float y = enemyBlockPosition.y + j;
                    if (!CheckCanMove(x, y))
                    {
                        continue;
                    }
                    roundBlockList.Add(new Vector2(x, y));
                }
            }

            Vector2 entityBlockPosition = new Vector2((int)_entity.position.x + 0.5f, (int)_entity.position.z + 0.5f);
            float distance = 0;
            float minDistance = 999999f;
            Vector2 targetPosition = Vector2.zero;
            for (int i = 0; i < roundBlockList.Count; ++i)
            {
                distance = Vector2.Distance(roundBlockList[i], entityBlockPosition);
                if (distance < minDistance)
                {
                    targetPosition = roundBlockList[i];
					minDistance = distance;
                }
            }
            return targetPosition;
        }

        private void AddDummyPositionList()
        {
            _dummyPositionList.Clear();
            Vector3 dummyPosition;
            int dummyPositionInt;
            float boxRadius;
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityDummy dummy;
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Value is EntityDummy && enumerator.Current.Value.id != _entity.id)
                {
                    dummy = enumerator.Current.Value as EntityDummy;
                    //dummyPosition = dummy.position;
                    if (dummy.entityAI == null || dummy.entityAI.blackBoard == null)
                    {
                        dummyPosition = dummy.position;
                    }
                    else
                    {
                        dummyPosition = dummy.entityAI.blackBoard.movePoint;
                        if (dummyPosition == Vector3.zero)
                        {
                            dummyPosition = dummy.position;
                        }
                    }
                    dummyPositionInt = (int)dummyPosition.x * 1000 + (int)dummyPosition.z;
                    AddDummyPosition(dummyPositionInt);
                    if (dummy.actor == null || dummy.actor.actorController == null)
                    {
                        continue;
                    }
                    boxRadius = dummy.actor.actorController.GetRadius();
                    dummyPositionInt = (int)(dummyPosition.x - boxRadius) * 1000 + (int)dummyPosition.z;
                    AddDummyPosition(dummyPositionInt);
                    dummyPositionInt = (int)(dummyPosition.x + boxRadius) * 1000 + (int)dummyPosition.z;
                    AddDummyPosition(dummyPositionInt);
                    dummyPositionInt = (int)dummyPosition.x * 1000 + (int)(dummyPosition.z - boxRadius);
                    AddDummyPosition(dummyPositionInt);
                    dummyPositionInt = (int)dummyPosition.x * 1000 + (int)(dummyPosition.z - boxRadius);
                    AddDummyPosition(dummyPositionInt);
                }
            }
        }

        private void AddDummyPosition(int dummyPositionInt)
        {
            if (!_dummyPositionList.ContainsKey(dummyPositionInt))
            {
                _dummyPositionList.Add(dummyPositionInt, true);
            }
        }

        private bool CheckCanMove(float x, float y)
        {
            if (!GameSceneManager.GetInstance().CheckCurrPointIsCanMove(x, y, _entity.actor.modifyLayer))
            {
                return false;
            }
            int positionInt = (int)x * 1000 + (int)y;
            if (_dummyPositionList.ContainsKey(positionInt))
            {
                return false;
            }
            return true;
        }
        #endregion

        public virtual bool ProcBossPartHasBeenBroken(int partId)
        {
            return false;
        }

        public virtual bool ProcBossPartCanBeRecovered(int partId)
        {
            return false;
        }

        public virtual void ProcAddBuff(int buffId, int sec)
        {
            float time = sec;
            _entity.bufferManager.AddBuffer(buffId, time);
        }

        public virtual bool ProcHasFriendInLeftRange(int distance)
        {
            float realDistance = distance * 0.01f;
            Vector3 originalPoint = _entity.actor.position;
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            Vector3 creatureVector;
            float angle;
            while (enumerator.MoveNext())
            {
                if (!(enumerator.Current.Value is EntityCreature)) continue;
                creature = enumerator.Current.Value as EntityCreature;
                if (creature.id == _entity.id) continue;
                if (creature.actor == null) continue;
                if (TargetFilter.IsFriend(creature, this._entity))
                {
                    if (Vector3.Distance(originalPoint, creature.actor.position) > realDistance) continue;
                    creatureVector = creature.actor.position - originalPoint;
                    angle = Vector3.Angle(_entity.actor.right, creatureVector);
                    if (angle > 90f && angle <= 270f) return true;
                }
            }
            return false;
        }

        public virtual bool ProcHasFriendInRightRange(int distance)
        {
            float realDistance = distance * 0.01f;
            Vector3 originalPoint = _entity.actor.position;
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            Vector3 creatureVector;
            float angle;
            while (enumerator.MoveNext())
            {
                if (!(enumerator.Current.Value is EntityCreature)) continue;
                creature = enumerator.Current.Value as EntityCreature;
                if (creature.id == _entity.id) continue;
                if (creature.actor == null) continue;
                if (TargetFilter.IsFriend(creature, this._entity))
                {
                    if (Vector3.Distance(originalPoint, creature.actor.position) > realDistance) continue;
                    creatureVector = creature.actor.position - originalPoint;
                    angle = Vector3.Angle(_entity.actor.right, creatureVector);
                    if ((angle >= 0f && angle <= 90f) || (angle > 270f && angle <= 360f)) return true;
                }
            }
            return false;
        }

        public void Destroy()
        {
            _blackBoard.enemyId = 0;
            _blackBoard.inLookOn = false;
            if (_blackBoard.timeoutId > 0)
            {
                TimerHeap.DelTimer(_blackBoard.timeoutId);
                _blackBoard.timeoutId = 0;
            }
        }

        public virtual bool ProcIsFirstThinking()
        {
            if (_blackBoard.isFirstThinking)
            {
                _blackBoard.isFirstThinking = false;
                return true;
            }
            return false;
        }

        public virtual bool ProcIsInDeathState()
        {
            bool result = _entity.stateManager.InState(state_config.AVATAR_STATE_DEATH);
			return result;
        }

        public virtual void ProcMonsterSpeech(int id, int speechType)
        {
            bool showSpeechValue = Convert.ToBoolean(speechType & 1);
            bool playSound = Convert.ToBoolean(speechType & 2);
            _entity.Speak(id, showSpeechValue, playSound);
        }

        public virtual bool ProcCmpLeaderInTheSameScene()
        {
            return false;
        }

        public virtual bool ProcCmpTargetMapType(string id)
        {
            return false;
        }

        public virtual void ProcTeleportToLeader()
        {
        }

        public virtual bool ProcFollowLeader(int distance2, int distance3, int distance4, bool isForce)
        {
            return false;
        }

        public virtual bool ProcCmpLeaderMapType(string id)
        {
            return false;
        }

        public virtual void ProcQuitFollowing()
        {
        }

        public virtual void ProcCancelAttackTarget()
        {
            _blackBoard.enemyId = 0;
        }

        public virtual bool ProcLeaderContainsBuff(int id)
        {
            return false;
        }

        public virtual bool ProcHasTouchMovePoint()
        {
            return false;
        }

        public virtual void ProcSetTouchMovePoint()
        {
        }

        public virtual void ProcDeleteTouchMovePoint()
        {
        }

        public int ProcGetFlagTimeStamp(int id)
        {
            return _blackBoard.GetFlagTime(id);
        }

        public void ProcSetFlag(int id)
        {
            _blackBoard.SetFlagTime(id);
        }

        public bool ProcDeleteFlag(int id)
        {
            return _blackBoard.DeleteFlagTime(id);
        }
    }
}
