﻿using Common;
using Common.Utils;
using GameData;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.RPC;
using UnityEngine;

namespace GameMain.Entities
{
    class EntityPuppetAI : EntityAI
    {

        public EntityPuppetAI(EntityCreature entity) : base(entity)
        {
            _thinkInterval = global_params_helper.GetPlayerAIInterval();
        }

        public override bool ProcFollowLeader(int distance2, int distance3, int distance4, bool isForce)
        {
            float distance = GetFollowDistance(distance2, distance3, distance4);
            var targetPosition = GetMasterPosition();
            if (targetPosition == Vector3.zero)
            {
                this._entity.moveManager.Stop();
                _blackBoard.inChasingState = false;
                return false;
            }
            var curDistance = (targetPosition - _entity.position).magnitude;
            if (curDistance > distance)
            {
                var direction = _entity.position - targetPosition;
                targetPosition = targetPosition + direction.normalized * distance * 0.8f;
                //if (!_entity.actor.animationController.IsReady()) return false;
                if (!_entity.CanActiveMove()) return false;
                if (curDistance >= distance * 2.5f)
                {
                    this._entity.moveManager.Move(targetPosition, _entity.speed * 0.01f * 1.1f);
                }
                else
                {
                    this._entity.moveManager.Move(targetPosition, _entity.speed * 0.01f * 0.95f);
                }
                if (isForce) _blackBoard.inChasingState = true;
                return true;
            }
            else
            {
                this._entity.moveManager.Stop();
            }
            _blackBoard.inChasingState = false;
            return false;
        }

        private float GetFollowDistance(int distance2, int distance3, int distance4)
        {
            float distance = 0;
            byte teamFollowNum = (_entity as EntityAvatar).team_follow_num;
            switch (teamFollowNum)
            {
                case 2:
                    distance = distance3 * 0.01f;
                    break;
                case 3:
                    distance = distance4 * 0.01f;
                    break;
                default:
                    distance = distance2 * 0.01f;
                    break;
            }
            return distance;
        }

        private Vector3 GetMasterPosition()
        {
            EntityCreature leader = GetLeader();
            if (leader != null)
            {
                return leader.position;
            }
            return Vector3.zero;
        }

        private EntityCreature GetLeader()
        {
            return PlayerAvatar.Player;
        }

        public override int GetEnemyNum()
        {
            return 1;
        }

        public override bool ProcLeaderContainsBuff(int id)
        {
            EntityCreature leader = GetLeader();
            if (leader == null) return false;
            return leader.bufferManager.ContainsBuffer(id);
        }

        public override bool ProcIsInChase()
        {
            return _blackBoard.inChasingState;
        }

        public override float GetMasterDistance()
        {
            var targetPosition = GetMasterPosition();
            if (targetPosition == Vector3.zero)
            {
                return -1;
            }
            var curDistance = (targetPosition - _entity.position).magnitude;
            return curDistance;
        }

        static int DEFAULT_SEARCH = 30000;
        public override bool ProcPetAOI(int chance, int targetType = 1, int dis = 100, int iCenterX = 0, int iCenterZ = 0, int monsterTypePriority = 0)
        {
            if (_blackBoard.enemyId > 0)    //已经搜索过目标，需要判断是否需要重新搜索目标
            {
                Entity enemyEntity = MogoWorld.GetEntity(_blackBoard.enemyId);
                if (enemyEntity != null)
                {
                    int result = MogoMathUtils.Random(1, 101);
                    if (result > chance)  //该概率下不需要重新搜索目标
                    {
                        return true;
                    }
                }
            }

            _blackBoard.enemyId = 0;
            if (GameSceneManager.GetInstance().IsInCity()) return false;
            EntityCreature leader = GetLeader();
            if (leader == null) return false;
            dis = dis == 0 ? DEFAULT_SEARCH : dis;
            var entities = EntityFilter.GetEntitiesInCircleRange(leader.localToWorldMatrix, dis * 0.01f, 0, 0, 0);
            var enemy = TargetFilter.GetClosestCreature(leader, entities, TargetType.Enemy);
            if (enemy != null)
            {
                _blackBoard.enemyId = enemy.id;
            }
            if (_blackBoard.enemyId == 0)
                return false;
            else
            {
                return true;
            }
        }
    }
}
