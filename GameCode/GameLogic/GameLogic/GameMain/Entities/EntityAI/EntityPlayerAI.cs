﻿using Common;
using Common.Events;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.AI;
using Mogo.Game;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.Entities
{
    class EntityPlayerAI : EntityAI
    {
        public EntityPlayerAI(EntityCreature entity)
            : base(entity)
        {
            _thinkInterval = global_params_helper.GetPlayerAIInterval();
        }

        private float afterMoveAICanBeginTime = 0;
        public override int CanThink()
        {
            if (_entity == PlayerAvatar.Player)
            {
                if (PlayerAvatar.Player.isFindingPosition)
                {
                    return 1;
                }
                if (PlayerAvatar.Player.isMovingByTouch)
                {
                    return 1;
                }
                if (ControlStickState.strength > 0)
                {
                    afterMoveAICanBeginTime = Time.realtimeSinceStartup + 2f;
                    return 1;
                }
                else if ((afterMoveAICanBeginTime - Time.realtimeSinceStartup) > 0)
                {
                    return 1;
                }
            }
            return base.CanThink();
        }

        public override void ProcMoveTo(float sec)
        {
            //if (!_entity.actor.animationController.IsReady())	return;
            if (!_entity.CanActiveMove()) return;
            float speed = 0f;
            if (sec > 0f)
            {
                speed = (this._entity.speed * sec) * 0.01f;
            }
            else
            {
                speed = this._entity.speed * 0.01f;
            }
            this._entity.moveManager.Move(_blackBoard.movePoint, speed);
            EventDispatcher.TriggerEvent<uint>(AIEvents.AIEvents_OnAIMove, _entity.id);
        }

        public override int GetEnemyNum()
        {
            return 1;
        }

        public override bool ProcCmpLeaderInTheSameScene()
        {
            return PlayerDataManager.Instance.TeamData.IsCaptainInSameMap();
        }

        public override bool ProcCmpTargetMapType(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return false;
            }
            string[] idArray = id.Split(',');
            List<int> idList = data_parse_helper.ParseListInt(idArray);
            int mapType = GameSceneManager.GetInstance().curMapType;
            return idList.Contains(mapType);
        }

        public override bool ProcCmpLeaderMapType(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return false;
            }
            string[] idArray = id.Split(',');
            List<int> idList = data_parse_helper.ParseListInt(idArray);
            int mapId = PlayerDataManager.Instance.TeamData.GetCaptainMapId();
            if (mapId == 0)
            {
                return false;
            }
            int mapType = map_helper.GetMapType(mapId);
            return idList.Contains(mapType);
        }

        public override void ProcTeleportToLeader()
        {
            TeamManager.Instance.TeamTransmitToCaptain();
        }

        public override void ProcQuitFollowing()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                TeamManager.Instance.CancelFollowCaptain();
                PlayerAutoFightManager.GetInstance().Stop();
            }
        }

        public override bool ProcFollowLeader(int distance2, int distance3, int distance4, bool isForce)
        {
            float distance = GetFollowDistance(distance2, distance3, distance4);
            var targetPosition = GetMasterPosition();
            if (targetPosition == Vector3.zero)
            {
                this._entity.moveManager.Stop();
                _blackBoard.inChasingState = false;
                return false;
            }
			targetPosition.y = _entity.position.y;
            var curDistance = (targetPosition - _entity.position).magnitude;
            if (curDistance > distance)
            {
                var direction = _entity.position - targetPosition;
                targetPosition = targetPosition + direction.normalized * distance * 0.8f;
                //if (!_entity.actor.animationController.IsReady()) return false;
                if (!_entity.CanActiveMove()) return false;
                this._entity.moveManager.Move(targetPosition, _entity.speed * 0.01f * 0.95f);
                if (isForce) _blackBoard.inChasingState = true;
                return true;
            }
            else
            {
                this._entity.moveManager.Stop();
            }
            _blackBoard.inChasingState = false;
            return false;
        }

        private float GetFollowDistance(int distance2, int distance3, int distance4)
        {
            float distance = 0;
            byte teamFollowNum = (_entity as EntityAvatar).team_follow_num;
            switch (teamFollowNum)
            {
                case 2:
                    distance = distance3 * 0.01f;
                    break;
                case 3:
                    distance = distance4 * 0.01f;
                    break;
                default:
                    distance = distance2 * 0.01f;
                    break;
            }
            return distance;
        }

        private Vector3 GetMasterPosition()
        {
            EntityCreature leader = GetLeader();
            if (leader != null)
            {
                return leader.position;
            }
            Vector2 syncCaptainPosition = PlayerDataManager.Instance.TeamData.GetCaptainPosition();
            if (syncCaptainPosition == Vector2.zero)
            {
                return Vector3.zero;
            }
            return new Vector3(syncCaptainPosition.x, _entity.position.y, syncCaptainPosition.y);
        }

        public override bool ProcIsInChase()
        {
            return _blackBoard.inChasingState;
        }

        protected override EntityCreature GetCreatureByTargetProirity(List<uint> entities, int targetProirity, TargetType targetCampType)
        {
            int targetMonsterID = PlayerTaskFindMonsterManager.GetInstance().targetMonsterID;
            EntityCreature creature;
            if (targetMonsterID > 0)
            {
                List<uint> result = new List<uint>();
                for (int i = 0; i < entities.Count; ++i)
                {
                    creature = MogoWorld.GetEntity(entities[i]) as EntityCreature;
                    if (creature is EntityMonsterBase && (creature as EntityMonsterBase).monster_id == targetMonsterID)
                    {
                        result.Insert(0, entities[i]);
                    }
                }
                if (result.Count > 0)
                {
                    return TargetFilter.GetClosestCreature(this._entity, result, targetCampType);
                }
                else
                {
                    return TargetFilter.GetClosestCreature(this._entity, entities, targetCampType);
                }
            }
            return base.GetCreatureByTargetProirity(entities, targetProirity, targetCampType);
        }

        static int DEFAULT_SEARCH = 30000;
        public override bool ProcPetAOI(int chance, int targetType = 1, int dis = 100, int iCenterX = 0, int iCenterZ = 0, int monsterTypePriority = 0)
        {
            if (_blackBoard.enemyId > 0)    //已经搜索过目标，需要判断是否需要重新搜索目标
            {
                Entity enemyEntity = MogoWorld.GetEntity(_blackBoard.enemyId);
                if (enemyEntity != null)
                {
                    int result = MogoMathUtils.Random(1, 101);
                    if (result > chance)  //该概率下不需要重新搜索目标
                    {
                        return true;
                    }
                }
            }

            _blackBoard.enemyId = 0;
            if (GameSceneManager.GetInstance().IsInCity()) return false;
            EntityCreature leader = GetLeader();
            if (leader == null) return false;
            dis = dis == 0 ? DEFAULT_SEARCH : dis;
            var entities = EntityFilter.GetEntitiesInCircleRange(leader.localToWorldMatrix, dis * 0.01f, 0, 0, 0);
            var enemy = TargetFilter.GetClosestCreature(leader, entities, TargetType.Enemy);
            if (enemy != null)
            {
                _blackBoard.enemyId = enemy.id;
            }
            if (_blackBoard.enemyId == 0)
                return false;
            else
            {
                return true;
            }
        }

        private EntityCreature GetLeader()
        {
            uint captainId = PlayerDataManager.Instance.TeamData.GetCaptainEntityId();
            if (captainId != 0)
            {
                var entity = MogoWorld.GetEntity(captainId);
                if (entity != null && entity is EntityCreature)
                {
                    return entity as EntityCreature;
                }
            }
            return null;
        }

        public override float GetMasterDistance()
        {
            var targetPosition = GetMasterPosition();
            if (targetPosition == Vector3.zero)
            {
                return -1;
            }
			targetPosition.y = _entity.position.y;
            var curDistance = (targetPosition - _entity.position).magnitude;
            return curDistance;
        }

        public override bool ProcLeaderContainsBuff(int id)
        {
            EntityCreature leader = GetLeader();
            if (leader == null) return false;
            return leader.bufferManager.ContainsBuffer(id);
        }

        public override bool ProcHasTouchMovePoint()
        {
            return PlayerTouchBattleModeManager.GetInstance().touchPosition != Vector3.zero;
        }

        public override void ProcSetTouchMovePoint()
        {
            Vector3 touchPosition = PlayerTouchBattleModeManager.GetInstance().touchPosition;
            _blackBoard.movePoint.Set(touchPosition.x, _entity.actor.position.y, touchPosition.z);
        }

        public override void ProcDeleteTouchMovePoint()
        {
            PlayerTouchBattleModeManager.GetInstance().DeleteTouchPosition();
        }
    }
}
