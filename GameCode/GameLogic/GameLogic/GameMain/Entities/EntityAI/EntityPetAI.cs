﻿using Common;
using Common.Utils;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using Mogo.AI;
using Mogo.Game;
using MogoEngine;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.Entities
{
    class EntityPetAI : EntityAI
    {
        
        public EntityPetAI(EntityCreature entity) : base(entity) { }

        public override bool ProcChaseMaster(int distanceInCm, bool isForce)
        {
            float distance = distanceInCm * 0.01f;
            var master = GetMaster();
            var targetPosition = master.position;
            var curDistance = (targetPosition - _entity.position).magnitude;
            if (curDistance >= distance)
            {
                var direction = _entity.position - targetPosition;
                targetPosition = targetPosition + direction.normalized * distance * 0.8f;
                if (!_entity.actor.animationController.IsReady()) return false;
                if (!_entity.CanActiveMove()) return false;
                float speed = master.speed * 0.01f * 0.95f;//宠物币玩家慢一点。
                this._entity.moveManager.Move(targetPosition, speed);
                if (isForce) _blackBoard.inChasingState = true;
                return true;
            }
            else {
                this._entity.moveManager.Stop();
            }
            _blackBoard.inChasingState = false;
            return false;
        }

        public override float GetMasterDistance()
        {
            var master = GetMaster();
            var targetPosition = master.position;
            var curDistance = (targetPosition - _entity.position).magnitude;
            return curDistance;
        }

        public override bool ProcIsInChase()
        {
            return _blackBoard.inChasingState;
        }

        public override bool ProcPatrol(int min, int max, int range)
        {
            //冷却totleUseTime时间
            if (_blackBoard.timeoutId > 0)
                TimerHeap.DelTimer(_blackBoard.timeoutId);
            _blackBoard.ChangeState(Mogo.AI.AIState.PATROL_CD_STATE);
            Vector3 movePoint = new Vector3();
            var master = GetMaster();
            var rangeMeter = range * 0.01f;
            movePoint.x = master.position.x + UnityEngine.Random.Range(0, rangeMeter);
            movePoint.y = this._entity.position.y;
            movePoint.z = master.position.z + UnityEngine.Random.Range(0, rangeMeter);
            _blackBoard.movePoint = movePoint;
            this._entity.FaceToPosition(_blackBoard.movePoint);
            this._entity.moveManager.Move(_blackBoard.movePoint, 2);
            int patrolTime = UnityEngine.Random.Range(min, max);
            _blackBoard.timeoutId = TimerHeap.AddTimer((uint)patrolTime, 0, ProcessAITimerEvent_PatrolCDEnd);
            return true;
        }

        EntityCreature GetMaster()
        {
            return PlayerAvatar.Player;
        }


        static int DEFAULT_SEARCH = 30000;
        public override bool ProcPetAOI(int chance, int targetType = 1, int dis = 100, int iCenterX = 0, int iCenterZ = 0, int monsterTypePriority = 0)
        {
            if (_blackBoard.enemyId > 0)    //已经搜索过目标，需要判断是否需要重新搜索目标
            {
                Entity enemyEntity = MogoWorld.GetEntity(_blackBoard.enemyId);
                if (enemyEntity != null)
                {
                    int result = MogoMathUtils.Random(1, 101);
                    if (result > chance)  //该概率下不需要重新搜索目标
                    {
                        return true;
                    }
                }
            }

            _blackBoard.enemyId = 0;
            if (GameSceneManager.GetInstance().IsInCity()) return false;
            EntityCreature playerEntity = MogoEngine.MogoWorld.Player as EntityCreature;
            dis = dis == 0 ? DEFAULT_SEARCH : dis;
            var entities = EntityFilter.GetEntitiesInCircleRange(playerEntity.localToWorldMatrix, dis * 0.01f, 0, 0, 0);
            var enemy = TargetFilter.GetClosestCreature(playerEntity, entities, TargetType.Enemy);
            if (enemy != null)
            {
                _blackBoard.enemyId = enemy.id;
            }
            if (_blackBoard.enemyId == 0)
                return false;
            else
            {
                return true;
            }
        }
    }
}
