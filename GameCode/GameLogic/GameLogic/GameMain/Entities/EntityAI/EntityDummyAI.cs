﻿using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using Mogo.AI;
using Mogo.Game;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.Entities
{
    class EntityDummyAI : EntityAI
    {
        public EntityDummyAI(EntityCreature entity) : base(entity) { }

        public override int GetEnemyNum()
        {
            return 1;
        }

        public override bool ProcBossPartHasBeenBroken(int partId)
        {
            EntityDummy dummy = _entity as EntityDummy;
            return dummy.partManager.PartHasBeenBroken(partId);
        }

        public override bool ProcBossPartCanBeRecovered(int partId)
        {
            EntityDummy dummy = _entity as EntityDummy;
            return dummy.partManager.PartCanBeRecovered(partId);
        }
    }
}
