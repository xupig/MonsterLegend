﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Game.Asset;
using GameLoader.Config;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using UnityEngine;

namespace GameMain.RemoteLoader
{
    /// <summary>
    /// 在线资源--下载器
    /// </summary>
    public class RemoteLoaderMgr
    {
        private bool isPause;                                       //是否暂停
        private bool isRuning;                                      //加载器状态[true:加载中,false:空闲中]
        private bool isPrint;
        private bool isStartCoroutine;

        private int step;                                           
        //private int curTryNum;                                      //当前重试次数
        //private int maxTryNum;                                      //每个资源下载最大重试次数 
        private string httpUrl;                                     //资源下载地址
        private string localPath;                                   //资源保存本地路径
        private string curLoadPath;

        private List<string> curLoadList;                           //当前下载列表
        private List<string> waitLoadList;                          //等待下载队列
        public static RemoteLoaderMgr Instance;

        public RemoteLoaderMgr()
        {
            //maxTryNum = 3;
            isRuning = true;
            waitLoadList = new List<string>();

            if (Application.platform == RuntimePlatform.Android ||
                Application.platform == RuntimePlatform.IPhonePlayer)
            {//真机环境
                httpUrl = "http://192.168.1.10/";
                localPath = SystemConfig.ResourcesPath;
            }
            else
            {//本地调试环境 
                httpUrl = string.Concat(ConstString.ASSET_FILE_HEAD, SystemConfig.ResourcesPath);
                localPath = "d:/remote_test/";
            }
        }

        /// <summary>
        /// 判断指定路径--资源加载状态
        /// </summary>
        /// <param name="path">资源的逻辑路径,例如:Scenes/1005_Race/1005_Race.prefab</param>
        /// <returns>true:已加载,false:未加载|未加载完成</returns>
        public bool HasLoad(string path)
        {
            //把逻辑路径转为物理路径,例如：Scenes/1005_Race/1005_Race.prefab 转为 Scenes$1005_Race$1005_Race.prefab.u
            string physicalPath = path.Replace("/", "$") + ConstString.U_SUFFIX;
            return File.Exists(string.Concat(localPath, physicalPath));
        }

        /// <summary>
        /// 取消加载
        /// </summary>
        /// <param name="path"></param>
        public void CancelLoad(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            if (waitLoadList.Contains(path)) waitLoadList.Remove(path);
        }

        /// <summary>
        /// 开启下载
        /// </summary>
        public void StartLoad()
        {
            isPause = false;
            LoggerHelper.Debug("开始");
        }

        /// <summary>
        /// 暂停下载
        /// </summary>
        public void PauseLoad()
        {
            isPause = true;
            LoggerHelper.Debug("暂停");
        }

        /// <summary>
        /// 远程加载
        /// </summary>
        /// <param name="path">资源的逻辑路径,例如:Scenes/1005_Race/1005_Race.prefab</param>
        /// <param name="callback">加载结果回调</param>
        /// <returns>是否已加载[true:已加载完成,false:未加载完成]</returns>
        public void Load(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            if (!waitLoadList.Contains(path))
            {
                waitLoadList.Add(path);
                if (!isStartCoroutine)
                {
                    StartLoad();
                    isStartCoroutine = true;
                    LoaderDriver.Instance.StartCoroutine(Download());
                }
            }
        }

        public void Load(List<string> list)
        {
            if (list == null || list.Count < 1) return;
            foreach (string path in list)
            {
                Load(path);
            }
        }

        private IEnumerator Download()
        {
            while (isRuning)
            {
                if (!isPause)
                {
                    if (waitLoadList.Count > 0)
                    {
                        isPrint = false;
                        curLoadPath = waitLoadList[0];
                        CancelLoad(curLoadPath);
                        yield return LoaderDriver.Instance.StartCoroutine(DownloadDependAndMe(curLoadPath));
                        //LoggerHelper.Debug(string.Format("[在线下载] curLoadPath:{0} [OK]", curLoadPath));
                    }
                    else
                    {
                        if (!isPrint) { LoggerHelper.Debug("[在线下载] 下载器已空闲"); isPrint = true; }
                    }
                }
                yield return 0;
            }
        }

        //下载path依赖文件和path本身文件
        private IEnumerator DownloadDependAndMe(string path)
        {
            step = 0;
            curLoadList = ObjectPool.Instance.GetDependAssetPhysicalPathList(path);
            while (curLoadList != null && step < curLoadList.Count)
            {
                if (!isPause)
                {
                    string physicPath = curLoadList[step];
                    string localRealPath = string.Concat(localPath, physicPath);
                    if(ObjectPool.Instance.IsLoadedAsset(physicPath) ||
                        ObjectPool.Instance.GetPresistAssetData(physicPath) != null ||
                        File.Exists(localRealPath))
                    {//已加载，已在常驻内存，已在外部目录
                        step++;
                    }
                    else
                    {
                        WWW www = new WWW(string.Concat(httpUrl, physicPath));
                        yield return www;
                        if (www != null && string.IsNullOrEmpty(www.error))
                        {
                            XMLParser.SaveBytes(localRealPath, www.bytes);
                            if (www.assetBundle) www.assetBundle.Unload(true);
                            www.Dispose();
                            www = null;
                            //LoggerHelper.Debug(string.Format("[在线下载依赖] curLoadList[{0}]={1} Download [OK]", step, physicPath));
                        }
                        else
                        {
                            LoggerHelper.Info(string.Format("[在线下载依赖] curLoadList[{0}]={1} Download [Fail]", step, physicPath));
                        }
                        step++;
                    }
                }
                else yield return 0;
            }
        }
    }
}
