﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Game.Asset;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Mgrs;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using GameLoader.Version;
using LitJson;
using UnityEngine;
using GameMain.GlobalManager;

namespace GameMain.RemoteLoader
{
    /// <summary>
    /// StreamingAssets目录资源--后台导出类
    /// </summary>
    public class StreamingAssetsExportMgr
    {
        private bool isPause;
        private bool isRuning;
        private bool isStartCoroutine;

        private int curFrame;
        private int beginTime;
        private string outputPath;
        private string resoucesPath;
        private string resourcesmkPath;
        private Queue<string> physicalPathQueue;
        private const string Resources = "Resources/";

        public int frameRate = 30;
        public static StreamingAssetsExportMgr Instance;

        public StreamingAssetsExportMgr()
        {
            isPause = true;
            isRuning = false;
            isStartCoroutine = false;
            physicalPathQueue = new Queue<string>();
            InitAssetList();
            if (Application.platform == RuntimePlatform.Android)
            {//真机环境
                resourcesmkPath = VersionManager.Instance.GetStreamingAssetsmkFilePath();
                resoucesPath = IOUtils.GetStreamPath(string.Concat(Resources, ""));
                outputPath = SystemConfig.ResourcesPath; //string.Concat(VersionManager.Instance.GetOutputRuntimeResourceFolder(), Resources);
            }
            else if (Application.platform == RuntimePlatform.WindowsEditor)
            {//PC环境
                resourcesmkPath = "file:///F:/u3d_workspace/project4/client/trunk/RuntimeResource/Resources/_resources.mk";
                resoucesPath = "file://F:/u3d_workspace/project4/client/trunk/RuntimeResource/Resources/";
                outputPath = "F:/test_resources/"; 
            }
        }

        private void InitAssetList()
        {
            if (Application.platform == RuntimePlatform.WindowsWebPlayer) return;
            var assetRecord = ObjectPool.assetRecord;
            if (assetRecord == null) return;
            Dictionary<string, int> assetDict = assetRecord.assetMemoryDict;
            if (assetDict == null || assetDict.Count < 1) isRuning = false;
               
            string targetPath = null;
            foreach (string url in assetDict.Keys)
            {
                if (ObjectPool.Instance.IsExternalAsset(url)) continue;
                targetPath = string.Concat(outputPath, url);
                if (!File.Exists(targetPath)) physicalPathQueue.Enqueue(url);
            }
        }

        private IEnumerator ExecuteExport()
        {
            yield return LoaderDriver.Instance.StartCoroutine(ExportFile());
        }

        private IEnumerator ExportFile()
        {
#if !UNITY_WEBPLAYER
            while (isRuning)
            {
                if (!isPause)
                {
                    if (BackgroundDownloadManager.GetInstance().isRuning)
                    {
                        yield return new WaitForSeconds(1f);
                        continue;
                    }
                    if (physicalPathQueue.Count > 0)
                    {
                        string assetPath = physicalPathQueue.Dequeue();
                        string streamPath = string.Concat(resoucesPath, assetPath);
                        string targetPath = string.Concat(outputPath, assetPath);
                        if (!File.Exists(targetPath))
                        {
                            //LoggerHelper.Debug(assetPath + " [Start]");
                            if(ObjectPool.Instance.IsLoadedAsset(assetPath))
                            {//此文件已加载过
                                byte[] data = ObjectPool.Instance.GetPresistAssetData(assetPath);
                                if (data != null && data.Length > 0)
                                {//此资源已在持久队列中,直接写入磁盘
                                    string tempFile = targetPath + "_temp";
                                    XMLParser.SaveBytes(tempFile, data);
                                    File.Move(tempFile, targetPath);
                                    data = null;
                                    //LoggerHelper.Info("[缓存导出] " + assetPath + " [OK]");
                                    //每隔frameRate帧，导出一个文件
                                    yield return new WaitForSeconds(1f);
                                }
                            }
                            else
                            {
                                WWW www = new WWW(streamPath);
                                yield return www;
                                if (www != null && string.IsNullOrEmpty(www.error))
                                {//下载成功
                                    string tempFile = targetPath + "_temp";
                                    bool isGameLoader = assetPath.IndexOf("Bytes$GameLoader.bytes.u") != -1 ? true : false;
                                    byte[] datas = isGameLoader ? (www.assetBundle.mainAsset as TextAsset).bytes : www.bytes;
                                    if (isGameLoader) LoggerHelper.Info("BackGround Export Bytes$GameLoader.bytes.u");
                                    XMLParser.SaveBytes(tempFile, datas);  //www.bytes
                                    File.Move(tempFile, targetPath);
                                    if (www.assetBundle) www.assetBundle.Unload(true);
                                    www.Dispose();
                                    www = null;
                                    //LoggerHelper.Info("[下载导出] " + assetPath + " [OK]");

                                    //每隔frameRate帧，导出一个文件
                                    yield return new WaitForSeconds(1f);
                                }
                            }
                        }
                    }
                    else
                    {
                        LoggerHelper.Debug("导出完成:" + (Environment.TickCount - beginTime) + " ms");
                        isRuning = false;
                    }
                }
                yield return 0;
            }
#else
            yield return 0;
#endif
        }

        /// <summary>
        /// 开启后台导出StreamingAssets资源
        /// </summary>
        public void StartExport()
        {
            if (IsOpenExport)
            {
                isRuning = true;
                isPause = false;
                beginTime = Environment.TickCount;
                if (!isStartCoroutine)
                {
                    LoaderDriver.Instance.StartCoroutine(ExecuteExport());
                    isStartCoroutine = true;
                }
                LoggerHelper.Debug("后台导出StreamingAsset [Start]");
            }
        }

        /// <summary>
        /// 暂停后台导出
        /// </summary>
        public void PauseExport()
        {
            if (!isPause)
            {
                isPause = true;
                LoggerHelper.Debug("暂停，已导出时间:" + (Environment.TickCount - beginTime) + " ms");
            }
        }

        /// <summary>
        /// 后台导出开启状态[true:开启,false:关闭]
        /// </summary>
        private bool IsOpenExport 
        { 
            get
            {
                if (SystemConfig.IsExportStreamingAssets)
                {
                    if (Application.platform == RuntimePlatform.Android ||
                        Application.platform == RuntimePlatform.IPhonePlayer)
                    {//真机环境
                        return true;
                    }
                }
                return false;
            } 
        }
    }
}
