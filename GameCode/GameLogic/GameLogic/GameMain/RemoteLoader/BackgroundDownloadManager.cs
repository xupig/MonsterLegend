﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.ServerConfig;
using Common.Base;
using Game.Asset;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.Entities;
using Common.ClientConfig;
using SpaceSystem;
using GameLoader.Config;
using System.Collections;
using MogoEngine.RPC;

namespace GameMain.GlobalManager
{
    //分离资源后台下载管理器
    //在ObjectPool空闲时按优先级下载存放在服务器上的游戏资源
    public class BackgroundDownloadManager
    {
        private static List<string> QUEUE_ORDER = new List<string>() { "GUI", "Atlas", "Scenes", "Characters" };
        private static int NEED_PRIORITY_LEVEL = 10;

        private static BackgroundDownloadManager s_instance = null;
        public static BackgroundDownloadManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new BackgroundDownloadManager();
            }
            return s_instance;
        }

        private Queue<string> _physicalPathQueue;
        private bool _isRuning = false;
        public bool isRuning { get { return _isRuning; } }
        public bool isPause = false;
        private bool _isDownloading = false;
        private int _fileMax = 0;
        private int _fileProgress = 0;
        BackgroundDownloadManager()
        {
            InitPhysicalPathQueue();
        }

        public void OnPlayerEnterWorld()
        {
            if (_physicalPathQueue == null || _physicalPathQueue.Count == 0) return;
            _isRuning = true;
            LoaderDriver.Instance.StartCoroutine(DeamonDownloadFile());
        }

        public void OnPlayerLeaveWorld()
        {
            _isRuning = false;
        }

        public float GetProgerss()
        {
            if (_fileMax > 0)
            {
                return (float)_fileProgress / _fileMax;
            }
            return 1;
        }

        private float GetDownloadInterval()
        {
            if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            {//3G 4G 网络下
                return 1f;
            }
            //wifi 网络下
            return 0.3f;
        }

        private bool CanLoadInGame()
        {
            return !ObjectPool.Instance.IsBusy()
                    && !GameSceneManager.GetInstance().inLoading
                    && Application.internetReachability != NetworkReachability.NotReachable
                    && ServerProxy.Instance.Connected;
        }

        private IEnumerator DeamonDownloadFile()
        {
            Action downloadedCB = () => {
                _fileProgress++;
                _isDownloading = false; 
            };
            while (_isRuning)
            {
                if (!_isDownloading && !isPause && CanLoadInGame())
                {
                    if (_physicalPathQueue.Count == 0) break;
                    string physicalPath = _physicalPathQueue.Dequeue();
                    _isDownloading = true;
                    ObjectPool.Instance.PreloadAssetFile(new string[] { physicalPath }, downloadedCB);
                    //LoggerHelper.Info("BackgroundDownload:" + physicalPath + ":" + GetDownloadInterval());
                }
                yield return new WaitForSeconds(GetDownloadInterval());
            }
        }

        private void InitPhysicalPathQueue()
        {
            if (!ObjectPool.Instance.HasExternalAsset()) return;
            if (Application.platform == RuntimePlatform.WindowsWebPlayer) return;
            _physicalPathQueue = new Queue<string>();
            Dictionary<string, List<string>> needDownloadPaths = GetNeedDownloadPaths();
            string head;
            for (int i = 1; i <= 0xf; i++) { //先选取有优先级的内容
                head = i.ToString();
                if (!needDownloadPaths.ContainsKey(head)) break;
                List<string> list = needDownloadPaths[head];
                for (int j = 0; j < list.Count; j++)
                {
                    _physicalPathQueue.Enqueue(list[j]);
                }
                needDownloadPaths.Remove(head);
            }
            for (int i = 0; i < QUEUE_ORDER.Count; i++) //再按资源类别优先级选取
            {
                head = QUEUE_ORDER[i];
                if (needDownloadPaths.ContainsKey(head))
                {
                    List<string> list = needDownloadPaths[head];
                    for (int j = 0; j < list.Count; j++)
                    {
                        _physicalPathQueue.Enqueue(list[j]);
                    }
                    needDownloadPaths.Remove(head);
                }
            }
            foreach (var pair in needDownloadPaths) //再把剩下的资源加入队列末尾中
            {
                var tempList = pair.Value;
                for (int j = 0; j < tempList.Count; j++)
                {
                    _physicalPathQueue.Enqueue(tempList[j]);
                }
            }
            _fileMax = _physicalPathQueue.Count;
            _fileProgress = 0;
        }

        private Dictionary<string, List<string>> GetNeedDownloadPaths()
        {
            Dictionary<string, List<string>> result = null;
            var assetRecord = ObjectPool.assetRecord;
            if (assetRecord == null) return result;
            Dictionary<string, int> assetDict = assetRecord.assetMemoryDict;
            string outputPath = SystemConfig.ResourcesPath; ;
            string targetPath = null;
            result = new Dictionary<string, List<string>>();
            foreach (string physicalPath in assetDict.Keys)
            {
                if (!ObjectPool.Instance.IsExternalAsset(physicalPath)) continue;
                targetPath = string.Concat(outputPath, physicalPath);
                if (File.Exists(targetPath)) continue;
                int priority = ObjectPool.Instance.GetPriority(physicalPath);
                string head;
                if (priority > 0 && PlayerAvatar.Player.level < NEED_PRIORITY_LEVEL)
                {
                    head = priority.ToString();
                }
                else
                {
                    int index = physicalPath.IndexOf("$");
                    if (index < 0) continue;
                    head = physicalPath.Substring(0, index);
                }
                if (!result.ContainsKey(head))
                {
                    result.Add(head, new List<string>());
                }
                result[head].Add(physicalPath);
            }
            return result;
        }
    }
}
