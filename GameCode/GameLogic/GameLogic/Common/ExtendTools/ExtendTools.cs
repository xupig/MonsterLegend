﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.ExtendTools
{
    public static class ExtendTools
    {
        public static Transform FindChildByName(this Transform transform,string boneName)
        {
            Transform child = transform.FindChild(boneName);
            if (child == null)
            {
                foreach (Transform c in transform)
                {
                    child = FindChildByName(c, boneName);
                    if (child != null) return child;
                }
            }
            return child;
        }

        public static void ResetPositionAngles(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;
        }

        public static T AddComponentOnlyOne<T>(this GameObject gameObject) where T:MonoBehaviour
        {
            var component = gameObject.GetComponent<T>();
            if (component != null) GameObject.Destroy(component);
            component = gameObject.AddComponent<T>();
            return component;
        }
    }
}
