﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 17:19:52
 * 描述说明：
 * *******************************************************/

using Common.Structs;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public class GlobalParams
    {

        public static List<float> ParseListFloat(string[] source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        private static List<float> _dmgLevelFactors = null;
        public static List<float> DmgLevelFactors
        {
            get
            {
                if (_dmgLevelFactors == null)
                {
                    var value = GameData.XMLManager.global_params[7].__value;
                    _dmgLevelFactors = ParseListFloat(value.Split(','));
                }
                return _dmgLevelFactors;
            }
        }

        private static int _runeAutoSwallowLevelLimit = -1;
        public static int RuneAutoSwallowLevelLimit
        {
            get
            {
                if(_runeAutoSwallowLevelLimit == -1)
                {
                    _runeAutoSwallowLevelLimit = int.Parse(GameData.XMLManager.global_params[(int)GlobalParamsEnum.RUNE_AUTO_SWALLOW_LEVEL_LIMIT].__value);
                }
                return _runeAutoSwallowLevelLimit;
            }
        }

        private static Dictionary<int, int> _locationCdDic;//符文免费许愿CD
        private static Dictionary<int, int> _locationDailyTimesDic;//符文每日免费次数
        private static Dictionary<int, int> _locationIconDic;//符文许愿图标
        private static Dictionary<int, int> _locationNameDic;//符文许愿名字
        private static Dictionary<int, int> _locationColorDic;//符文许愿颜色定义


        public static int GetRuneLocationCd(int location)
        {
            if (_locationCdDic == null)
            {
                string cd = XMLManager.global_params[(int)GlobalParamsEnum.RUNE_LOCATION_CD].__value;
                _locationCdDic = MogoStringUtils.Convert2Dic(cd);
            }
            return _locationCdDic[location];
        }

        public static int GetRuneLocationDailyTimes(int location)
        {
            if (_locationDailyTimesDic == null)
            {
                string dailyTimes = XMLManager.global_params[(int)GlobalParamsEnum.RUNE_LOCATION_DAILY_TIME].__value;
                _locationDailyTimesDic = MogoStringUtils.Convert2Dic(dailyTimes);
            }
            return _locationDailyTimesDic[location];
        }

        public static int GetRuneLocationIcon(int location)
        {
            if (_locationIconDic == null)
            {
                string icons = XMLManager.global_params[(int)GlobalParamsEnum.RUNE_LOCATION_ICON].__value;
                _locationIconDic = MogoStringUtils.Convert2Dic(icons);
            }
            return _locationIconDic[location];
        }

        public static int GetRuneLocationColor(int location)
        {
            if (_locationColorDic == null)
            {
                string colors = XMLManager.global_params[(int)GlobalParamsEnum.RUNE_LOCATION_COLOR].__value;
                _locationColorDic = MogoStringUtils.Convert2Dic(colors);
            }
            return _locationColorDic[location];
        }

        public static int GetLocationName(int location)
        {
            if (_locationNameDic == null)
            {
                string names = XMLManager.global_params[(int)GlobalParamsEnum.RUNE_LOCATION_NAME].__value;
                _locationNameDic = MogoStringUtils.Convert2Dic(names);
            }
            return _locationNameDic[location];
        }

        public static int GetFullEnergy()
        { 
            return avatar_level_helper.GetMaxEnergy(PlayerAvatar.Player.level);
        }

        public static int GetEnergyLimit()
        {
            return avatar_level_helper.GetEnergyLimit(PlayerAvatar.Player.level);
        }

        public static int GetNewSlotOpenLevel()
        {
            string token = XMLManager.global_params[(int)GlobalParamsEnum.NEW_SLOT_LEVEL].__value;
            string[] array = token.Split(',');
            return int.Parse(array[0]);
        }

        public static int GetChatContentLengthLimit( )
        {
            return int.Parse( GameData.XMLManager.global_params[33].__value );
        }

        public static int GetTradeMarketMaxGrid()
        {
            string[] grids_cnt_array = XMLManager.global_params[60].__value.Split(',');
            return int.Parse(grids_cnt_array[0]);
        }

        public static int GetTradeMarketFreeGrid()
        {
            string[] grids_cnt_array = XMLManager.global_params[60].__value.Split(',');
            return int.Parse(grids_cnt_array[1]);
        }

        public static int GetTradeMarketExpandMoneyType()
        {
            string[] array = XMLManager.global_params[61].__value.Split(':');
            return int.Parse(array[0]);
        }

        public static int GetTradeMarketExpandPrice()
        {
            string[] array = XMLManager.global_params[61].__value.Split(':');
            return int.Parse(array[1]);
        }

        public static List<int> GetRecommentAttri(Vocation vocation)
        {
            string[] itemList = XMLManager.global_params[193].__value.Split(';');
            Dictionary<string, string[]> result = new Dictionary<string, string[]>();
            for (int i = 0; i < itemList.Length; i++)
            {
                string[] valueList = itemList[i].Split(':');
                result.Add(valueList[0], valueList[1].Split(','));
            }
            
            Dictionary<int, List<int>> recommentListDic = data_parse_helper.ParseDictionaryIntListInt(result);
            return recommentListDic[(int)vocation];
        }

        public static int GetFriendBlessingLimit()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.FRIEND_BLESSING_LIMIT].__value);
        }

        public static int GetFriendBlessedLimit()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.FRIEND_BLESSED_LIMIT].__value);
        }

        public static int GetFriendIntimateItemLimit()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.FRIEND_INTIMATE_ITEM_LIMIT].__value);
        }

        public static int GetIntimateLimit()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.INTIMATE_LIMIT].__value);
        }

        public static int GetFriendLimit()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.FRIEND_LIMIT].__value);
        }

        private static Dictionary<int, int> spellOpenLevelDict;
        public static int GetSpellSlotOpenLevel(int slotId)
        {
            if(spellOpenLevelDict == null)
            {
                spellOpenLevelDict = new Dictionary<int, int>();
                string[] levelLimit = XMLManager.global_params[GlobalParamsEnum.SPELL_SLOT_OPEN_LEVEL].__value.Split(',');
                for (int i = 0; i < levelLimit.Length;i++ )
                {
                    spellOpenLevelDict.Add(i + 1, int.Parse(levelLimit[i]));
                }
            }
            return spellOpenLevelDict[slotId];
        }


        private static Dictionary<int, int> proficientLevelDict;
        public static int GetProficientOpenLevel(int proficientId)
        {
            InitProficientDict();
            return proficientLevelDict[proficientId];
        }

        private static void InitProficientDict()
        {
            if (proficientLevelDict == null)
            {
                proficientLevelDict = new Dictionary<int, int>();
                string[] levelLimit = XMLManager.global_params[GlobalParamsEnum.PROFICIENT_LEVEL].__value.Split(',');
                for (int i = 0; i < levelLimit.Length; i++)
                {
                    proficientLevelDict.Add(i + 1, int.Parse(levelLimit[i]));
                }
            }
        }

        public static Dictionary<int, int> ProficientLevelDict
        {
            get 
            {
                InitProficientDict();
                return proficientLevelDict;
            }
        }

        public static int GetBuyOnceEnergyNum()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.ENERGY_BUY_ONCE_NUM].__value);
        }

        public static UInt64 GetInviteCd()
        {
            return UInt64.Parse(XMLManager.global_params[GlobalParamsEnum.INVITE_CD].__value);
        }

        public static int GetChatMsgMaxCharCount()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.CHAT_MSG_MAX_CHAR_COUNT].__value);
        }

        public static int GetPrivateChatPlayerMaxCount()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.PRIVATE_CHAT_PLAYER_MAX_COUNT].__value);
        }

        public static int GetChatNoticeTime()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.CHAT_NOTICE_TIME].__value);
        }

        public static int GetSystemPushTimeInterval()
        {
            string[] splits = XMLManager.global_params[GlobalParamsEnum.SYSTEM_PUSH_TIME_INTERVAL].__value.Split(',');
            return int.Parse(splits[0]);
        }

        public static int GetSystemPushLongTimeInterval()
        {
            string[] splits = XMLManager.global_params[GlobalParamsEnum.SYSTEM_PUSH_TIME_INTERVAL].__value.Split(',');
            return int.Parse(splits[1]);
        }

        public static int GetSuitReplaceCostMoneyType()
        {
            string[] array = XMLManager.global_params[174].__value.Split(',');
            return int.Parse(array[0]);
        }

        public static int GetSuitReplaceCost()
        {
            string[] array = XMLManager.global_params[174].__value.Split(',');
            return int.Parse(array[1]);
        }

        public static int GetTeamTaskDayMaxReward()
        {
            return int.Parse(XMLManager.global_params[GlobalParamsEnum.TEAM_TASK_DAY_MAX_REWARD].__value);
        }

        public static int GetDreamLandPushHintMaxLevel()
        {
            return int.Parse(XMLManager.global_params[207].__value);
        }

        public static int GetSkillOpenLevel()
        {
            return int.Parse(XMLManager.global_params[239].__value.Split(',')[0]);
        }

        public static int GetWildOpenLevel()
        {
            return int.Parse(XMLManager.global_params[243].__value);
        }
    }
}
