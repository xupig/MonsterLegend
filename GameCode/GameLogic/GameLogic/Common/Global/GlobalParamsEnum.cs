﻿#region 模块信息
/*==========================================
// 文件名：GlobalParamsEnum
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/19 15:02:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public static class GlobalParamsEnum
    {
        public static int GEM_SCORE_TO_DIAMOND_FACTOR = 18;
        public static int NEW_SLOT_LEVEL = 20;
        public static int CHAT_MSG_MAX_CHAR_COUNT = 33;
        public static int RUNE_LOCATION_CD = 50;
        public static int RUNE_LOCATION_DAILY_TIME = 51;
        public static int RUNE_LOCATION_ICON = 52;
        public static int RUNE_LOCATION_NAME = 53;
        public static int RUNE_LOCATION_COLOR = 54;
        public static int RUNE_AUTO_SWALLOW_LEVEL_LIMIT = 55;

        public static int FRIEND_BLESSING_LIMIT = 42;//赠送体力次数上限
        public static int FRIEND_BLESSED_LIMIT = 43;//接受体力赠送次数上限
        public static int FRIEND_INTIMATE_ITEM_LIMIT = 44;//赠送亲密值道具次数上限
        public static int INTIMATE_LIMIT = 67;//亲密值上限
        public static int FRIEND_LIMIT = 68;//好友数量上限
        public static int ITEM_TYPE_TO_CHINESE_NAME = 72; // 道具类型对应的中文字段

        public static int SPELL_SLOT_OPEN_LEVEL = 78;//技能槽开放等级

        public static int PROFICIENT_LEVEL = 75;

        public static int ENERGY_BUY_ONCE_NUM = 106;//每次购买的体力值

        public static int INVITE_CD = 113;//邀请CD
        public static int PRIVATE_CHAT_PLAYER_MAX_COUNT = 120;//密聊临时聊天列表保存条数

        public static int PREVIEW_ATTACK_RANGE_ARGS = 134;//密聊临时聊天列表保存条数

        public static int NOT_SHOW_EQUIP_ID = 156; // 装备不提示穿戴列表
        public static int CHAT_NOTICE_TIME = 161;  //聊天系统，链接通知，超过多长时间后显示N久之前xx秒
        public static int SYSTEM_PUSH_TIME_INTERVAL = 162;  //系统消息推送时间间隔,单位秒

        public static int TEAM_TASK_DAY_MAX_REWARD = 180;//时空裂痕每日最大收益次数

    }
}
