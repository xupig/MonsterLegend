﻿#region 模块信息
/*==========================================
// 文件名：ExtensionUtil(扩展类)
// 命名空间:
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/21 21:20:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using GameData;

using MogoEngine;
using Common.ExtendComponent;

public static class ExtensionUtil
{



    public static void Rpc(this action_config actionConfig, string serverInterface, params object[] param)
    {
        PlayerAvatar.Player.ActionRpcCall(serverInterface, (int)actionConfig, param);
    }

    public static void Register(this action_config actionConfig)
    {
        MogoWorld.Player.RegisterWait((int)actionConfig);
    }

    /// <summary>
    /// 转多语言内容
    /// </summary>
    /// <param name="langEnum"></param>
    /// <param name="param"></param>
    /// <returns></returns>
    public static string ToLanguage(this LangEnum langEnum, params object[] param)
    {
        return MogoLanguageUtil.GetString(langEnum, param);
    }

    /// <summary>
    /// 转多语言内容
    /// </summary>
    /// <param name="langId"></param>
    /// <param name="param"></param>
    /// <returns></returns>
    public static string ToLanguage(this int langId, params object[] param)
    {
        return MogoLanguageUtil.GetContent(langId, param);
    }

    /// <summary>
    /// 打开界面
    /// </summary>
    /// <param name="panelId"></param>
    /// <param name="data"></param>
    public static void Show(this PanelIdEnum panelId, object data = null)
    {
        UIManager.Instance.ShowPanel(panelId, data);
    }

    /// <summary>
    /// 关闭界面
    /// </summary>
    /// <param name="panelId"></param>
    public static void Close(this PanelIdEnum panelId)
    {
        UIManager.Instance.ClosePanel(panelId);
    }

    /// <summary>
    /// action_config转系统ID
    /// </summary>
    /// <param name="actionId"></param>
    /// <returns></returns>
    public static int ToInt32(this action_config actionId)
    {
        return (int)actionId + 60000;
    }

    public static int ToInt(this Vocation vocation)
    {
        return (int)vocation;
    }

    public static BagData GetBagData(this BagType bagType)
    {
        return PlayerDataManager.Instance.BagData.GetBagData(bagType);
    }

    /// <summary>
    /// 2047 => 00000000000000000000011111111111
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int[] ToBits(this int value)
    {
        int[] result = new int[32];
        int index = 31;
        while (index >= 0)
        {
            result[index] = value % 2;
            value = (value - result[index]) / 2;
            index--;
        }
        return result;
    }

    public static string ToIntString(this LimitEnum limitEnumId)
    {
        return ((int)limitEnumId).ToString();
    }

}
