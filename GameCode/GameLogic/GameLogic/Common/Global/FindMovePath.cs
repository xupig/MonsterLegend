﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


namespace FindMovePath
{

    //节点类
    class point
    {
        public int x;
        public int z;
        public int G;
        public int H;
        public point father;
        public point() { }

        public point(int xa, int yb)
        {
            x = xa;
            z = yb;
        }

        public point(int xa, int yb, int g, int h, point p)
        {
            x = xa;
            z = yb;
            G = g;
            H = h;
            father = p;
        }

        public static List<point> roundPointOffsets = new List<point>() 
        {
            new point(0, 1),
            new point(0, -1),
            new point(1, 0),
            new point(1, 1),
            new point(1, -1),
            new point(-1, 0),
            new point(-1, 1),
            new point(-1, -1),
        };
    }


    class BlockMap
    {
        public const byte BLOCK_TYPE_NORMAL = 0;    //可行走区域
        public const byte BLOCK_TYPE_BLOCK = 1;     //障碍区域
        public const byte BLOCK_TYPE_WALL = 2;      //墙
        public const byte BLOCK_TYPE_SIDE = 3;      //补边区域
        public const byte BLOCK_TYPE_FLY = 4;       //飞行区域
        public const byte BLOCK_TYPE_WALL_FLY = 5;  //飞行墙
        //开启列表
        private List<point> _openlist = new List<point>();
        //结束列表
        private List<point> _closelist = new List<point>();
 
        byte[,] _mapData;
        int _xMax;
        int _zMax;

        bool _bFly = false;     //是否飞行(飞行则飞行区域也算可行走区域)

        //读取地图文件
        public void Read(string mapInfoPath)
        {
            using (FileStream f = File.OpenRead(mapInfoPath))
            {

                //维数占1个字节
                //m = (int)SR.Read();
                //n = (int)SR.Read();

                byte[] b = new byte[2];
                f.Read(b, 0, 2);
                _xMax = (int)(b[0] + b[1]);
                f.Read(b, 0, 2);
                _zMax = (int)(b[0] + b[1]);

                _mapData = new byte[_xMax, _zMax];
                for (int i = 0; i < _xMax; ++i)
                for (int j = 0; j < _zMax; ++j)
                {
                    _mapData[i, j] = (byte)f.ReadByte();
                }
            }
        }

        public BlockMap(byte[] data)
        {
            Init(data);
        }

        public void Init(byte[] data)
        {
            uint cursor = 0;
            int xl = data[cursor++];
            int xh = data[cursor++];
            int zl = data[cursor++];
            int zh = data[cursor++];
            
            _xMax = xl + (xh << 8);
            _zMax = zl + (zh << 8);
            _mapData = new byte[_xMax, _zMax];
            
            for (int i = 0; i < _xMax; ++i)
            {
                for (int j = 0; j < _zMax; ++j)
                {
                    _mapData[i, j] = data[cursor++];
                }
            }
        }

        public byte[,] GetBlockMapData()
        {
            return _mapData;
        }

        public List<int> findpath(int x1, int y1, int x2, int y2, bool bFly = false)
        {
            _bFly = bFly;
            point s = new point(x1, y1);
            FixPointToCanMove(s);
            point d = new point(x2, y2);
            FixPointToCanMove(d);
            var result = findpath(s, d);
            return result;
        }

        public Vector3 CheckPointAroundCanMove(float posx, float posz, bool bFly = false, bool largeRangeSearch = false)
        {
            int searchRange = largeRangeSearch ? 30 : 10;
            _bFly = bFly;
            int xpos = Mathf.RoundToInt(posx);
            int zpos = Mathf.RoundToInt(posz);
            for (int i = 1; i <= searchRange; i++)
            {
                for (int j = 0; j < point.roundPointOffsets.Count; j++)
                {
                    var offset = point.roundPointOffsets[j];
                    var newX = xpos + offset.x * i;
                    var newZ = zpos + offset.z * i;
                    if (IsWay(newX, newZ))
                    {
                        return new Vector3(newX, 0f, newZ);
                    }
                }
            }
            return Vector3.zero;
        }

        public bool IsWay(float posx, float posz, bool bFly = false)
        {
            _bFly = bFly;
            int xpos = Mathf.RoundToInt(posx);
            int zpos = Mathf.RoundToInt(posz);
            return IsWay(xpos, zpos);
        }

        public bool OutOfRange(int x, int z)
        {
            return x < 0 || x >= _xMax || z < 0 || z >= _zMax;
        }

        //计算（更新）节点G值
        private int get_Gvalue(point p)
        {
            if (p.father == null) return 0;
            if (p.x == p.father.x || p.z == p.father.z) return p.father.G + 10;
            else return p.father.G + 14;
        }

        //计算节点H值
        private int get_Hvalue(point p, point pb)
        {
            return (Math.Abs(p.x - pb.x) + Math.Abs(p.z - pb.z))*10;
        }

        //开启列表成员判断
        private bool openlist_member(int x, int y)
        {
            for (int i = 0; i < _openlist.Count; i++)
            {
                var p = _openlist[i];
                if (p.x == x && p.z == y)
                    return true;
            }
            return false;
        }

        //关闭列表成员判断
        private bool closelist_member(int x, int y)
        {
            for (int i = 0; i < _closelist.Count; i++)
            {
                var p = _closelist[i];
                if (p.x == x && p.z == y)
                    return true;
            }
            return false;
        }

        //开启列表中F值最小的点
        private point get_targetmin()
        {
            point pmin = null;
            for (int i = 0; i < _openlist.Count; i++)
            {
                var p = _openlist[i];
                if (pmin == null || (pmin.G + pmin.H) > (p.G + p.H))
                    pmin = p;
            }
            return pmin;
        }

        //获得开启列表中的节点对象
        private point get_object(int x, int y)
        {
            for (int i = 0; i < _openlist.Count; i++)
            {
                var p = _openlist[i];
                if (p.x == x && p.z == y)
                    return p;
            }
            return null;
        }


        //九宫格判断
        private void check(point p0, point pa, ref point pb)
        {
            for (int xt = p0.x - 1; xt <= p0.x + 1; ++xt) //九宫格搜寻
            {
                for (int yt = p0.z - 1; yt <= p0.z + 1; ++yt)
                {
                    if (!(xt == p0.x && yt == p0.z))//排除本身中心点
                    {
                        if (IsWay(xt, yt) && !closelist_member(xt, yt))//排除范围外的冗余点、障碍点和关闭列表中的点
                        {
                            if (openlist_member(xt, yt))//对已经存在于开启列表中的点进行考察重新计算G值和确定父节点
                            {
                                point pt = get_object(xt, yt);
                                int gnew = 0;
                                if (p0.x == pt.x || p0.z == pt.z) //与中心点在同行或同列则G以10为单位增大
                                {
                                    gnew = p0.G + 10;
                                }
                                else { gnew = p0.G + 14; }
                                if (gnew < pt.G)//更新G值与父节点
                                {
                                    pt.father = p0;
                                    pt.G = gnew;
                                }
                            }
                            else //不在开启列表中
                            {
                                point pt = new point();
                                pt.x = xt;
                                pt.z = yt;
                                pt.father = p0;
                                pt.G = get_Gvalue(pt);
                                pt.H = get_Hvalue(pt, pb);
                                _openlist.Add(pt);
                            }
                        }
                    }
                }
            }
        }

        //寻找pa到目的pb的路线
        private List<int> findpath(point ps, point pd)
        {
            if (!IsWay(ps.x, ps.z) || !IsWay(pd.x, pd.z)) return null;
            if (IsAroundByBlock(ps.x, ps.z) || IsAroundByBlock(pd.x, pd.z)) return null;
            _openlist.Clear();
            _closelist.Clear();
            _openlist.Add(ps);
            while (!openlist_member(pd.x, pd.z) || _openlist.Count == 0)
            {
                point p = get_targetmin();
                if (p == null) {
                    //Debug.LogError("p == null " + _closelist.Count);
                    return null; 
                }
                _openlist.Remove(p);
                _closelist.Add(p);
                check(p, ps, ref pd);
            }
            List<int> result = new List<int>();
            point pp = get_object(pd.x, pd.z);
            result.Insert(0, pd.z);
            result.Insert(0, pd.x);
            while (pp.father != null)
            {
                pp = pp.father;
                result.Insert(0, pp.z);
                result.Insert(0, pp.x);
            }
            //DrawPoints(_closelist, Color.green);
            //DrawPoints(_openlist, Color.cyan);
            _openlist.Clear();
            _closelist.Clear();
            
            return result;
        }

        void DrawPoints(List<point> pointList, Color color)
        {
            //Debug.LogError("DrawPoints::" + pointList.Count);
            var h = 20;
            Vector3 start = new Vector3(0, h, 0);
            Vector3 end = new Vector3(0, h, 0);
            for (int i = 0; i < pointList.Count; i++)
            {
                var x = pointList[i].x;
                var z = pointList[i].z;
                start.x = x;
                start.z = z;
                end.x = x + 0.5f;
                end.z = z;
                Debug.DrawLine(start, end, color, 10);
                end.x = x - 0.5f;
                end.z = z;
                Debug.DrawLine(start, end, color, 10);
                end.x = x;
                end.z = z + 0.5f;
                Debug.DrawLine(start, end, color, 10);
                end.x = x;
                end.z = z - 0.5f;
                Debug.DrawLine(start, end, color, 10);
            }
        }

        void FixPointToCanMove(point fxPoint, int stack = 0)
        {
            if (stack > 3) return;
            if (!IsWay(fxPoint.x, fxPoint.z) || IsAroundByBlock(fxPoint.x, fxPoint.z))
            {
                Vector3 pos1 = CheckPointAroundCanMove(fxPoint.x, fxPoint.z, _bFly);
                fxPoint.x = (int)pos1.x;
                fxPoint.z = (int)pos1.z;
                FixPointToCanMove(fxPoint, ++stack);
            }
        }

        void CheckStartPoint(int x1, int y1, int index)
        {
            bool isFindFlag = false;
            if (!isFindFlag && _mapData[x1 + index + 1, y1] == 0)
            {
                _mapData[x1 + index, y1] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 + index + 1, y1 + index + 1] == 0)
            {
                _mapData[x1 + index, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1, y1 + index + 1] == 0)
            {
                _mapData[x1, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1] == 0)
            {
                _mapData[x1 - index, y1] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1, y1 - index - 1] == 0)
            {
                _mapData[x1, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1 - index - 1] == 0)
            {
                _mapData[x1 - index, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 + index + 1, y1 - index - 1] == 0)
            {
                _mapData[x1 + index, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1 + index + 1] == 0)
            {
                _mapData[x1 - index, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && index != 2)
            {
                CheckStartPoint(x1, y1, 2);
            }
        }

        private bool IsWay(int x, int z)
        { 
            if (OutOfRange(x, z)) return false;
            if (_bFly)
            {
                return _mapData[x, z] != BLOCK_TYPE_BLOCK && _mapData[x, z] != BLOCK_TYPE_WALL_FLY;
            }
            return _mapData[x, z] == BLOCK_TYPE_NORMAL;
        }

        private bool IsAroundByBlock(int x, int z)
        {
            var blockSum = 0;
            for (int j = 0; j < point.roundPointOffsets.Count; j++)
            {
                var offset = point.roundPointOffsets[j];
                var newX = x + offset.x;
                var newZ = z + offset.z;
                if (!IsWay(newX, newZ))
                {
                    blockSum++;
                }
            }
            var result = blockSum >= 7;
            return result;
        }
    }
}
