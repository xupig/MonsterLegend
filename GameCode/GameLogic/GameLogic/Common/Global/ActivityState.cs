﻿#region 模块信息
/*==========================================
// 文件名：ActivityState
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/8 12:34:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public enum ActivityState
    {
        NO_START = 1,
        UNDERWAY = 2,
        FINISHED = 3,
    }
}
