﻿#region 模块信息
/*==========================================
// 文件名：RTXDefine
// 命名空间: Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/2 21:31:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public class RTXDefine
    {
        public static string ANDY = "廖志辉（GZ1618）";//廖志辉
        public static string HDS = "胡东升（GZ1598）";//胡东升
        public static string HWC = "黄伟超（GZ0795）";//黄伟超
        public static string SCR = "沈翠荣（GZ1869）";//沈翠荣
        public static string PSN = "朴晟宁（GZ3393）";//朴晟宁

        public static string GJ = "巩靖（GZ0602）";//巩靖
        public static string XYM = "谢扬明（GZ3055）";//谢扬明
        public static string LXS = "李晓帅（GZ3092）";//李晓帅
        public static string CWC = "蔡文超（GZ2967）"; //蔡文超
        public static string CQ = "陈铨（GZ1604）";//陈铨

    }
}
