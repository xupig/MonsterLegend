﻿#region 模块信息
/*==========================================
// 文件名：LimitEnum
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/23 10:42:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public enum LimitEnum
    {
        NO_LIMIT = 0,
        LIMIT_LEVEL = public_config.LIMIT_LEVEL,
        LIMIT_VOCATION = public_config.LIMIT_VOCATION,
        LIMIT_VIP_LEVEL = public_config.LIMIT_VIP_LEVEL,
        LIMIT_TASK = public_config.LIMIT_TASK,
        LIMIT_GENDER = public_config.LIMIT_GENDER,
        LIMIT_FIGHT_FORCE = public_config.LIMIT_FIGHT_FORCE,
        LIMIT_BUFF = public_config.LIMIT_BUFF,
        LIMIT_SPELL = public_config.LIMIT_SPELL,
        LIMIT_EQUIP = public_config.LIMIT_EQUIP,
        LIMIT_TRY_FB = public_config.LIMIT_PASS_TRY_MISSION,
        LIMIT_GUILD_CONTRIBUTE = public_config.LIMIT_GUILD_CONTRIBUTION,
        LIMIT_TASK_ACCEPTED = public_config.LIMIT_TASK_ACCEPTED,
        LIMIT_TASK_REWARDED = public_config.LIMIT_TASK_REWARDED,
        LIMIT_FORTRESS_FUNCTION = public_config.LIMIT_FORTRESS_FUNCTION,
        LIMIT_LEVEL_LT = public_config.LIMIT_LEVEL_LT,
        LIMIT_TUTOR_LEVEL = public_config.LIMIT_TUTOR_LEVEL,
    }

}
