﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 17:19:52
 * 描述说明：
 * *******************************************************/

using Common.Data;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using System;
using UnityEngine;

namespace Common.Global
{
    public class Global
    {
        private static ulong _lastServerTimeStamp = 0;
        private static float _lastRealtimeSinceStartup = 0;

        private static ulong _localTimeStamp = 0;
        private static float _startTimeStamp = 0;
        private static bool _firstSync = true;

        public static ulong localTimeStamp
        {
            get
            {
                if (_localTimeStamp == 0)
                {
                    return _localTimeStamp;
                }
                return _localTimeStamp + (ulong)((GetRealTime() - _startTimeStamp) * 1000);
            }
        }

        public static UInt64 serverTimeStamp 
        { 
            set
            {
                if (_localTimeStamp == 0)
                {
                    if (_firstSync)
                    {
                        _firstSync = false;
                    }
                    else
                    {
                        _localTimeStamp = value;
                        _startTimeStamp = GetRealTime();
                    }
                }
                _lastServerTimeStamp = value;
                _lastRealtimeSinceStartup = GetRealTime();
            } 
            get
            {
                if (_lastServerTimeStamp == 0)
                {
                    return 0;
                }
                return ((ulong)((GetRealTime() - _lastRealtimeSinceStartup) * 1000) + _lastServerTimeStamp);
            } 
        }

        private static float GetRealTime()
        {
            return RealTime.time;
        }

        public static uint serverTimeStampSecond
        {
            get
            {
                return (uint)(serverTimeStamp * 0.001);
            }
        }


        public static int currentSecond { get; set; }

        public static bool HeartBeat { set; get; }

        public static float Scale {get;set;}

        public static int localTimeZone = 8;

        /// <summary>
        /// 用来存储与标准时间的小时差，比标准时间快的是负数，比标准时间慢的是正数
        /// </summary>
        public static int serverTimeZone = -8;

        public static void RefreshLocalTimeStamp()
        {
            _localTimeStamp = 0;
        }
    }
}
