﻿#region 模块信息
/*==========================================
// 文件名：ItemQualityDefine
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 10:15:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Global
{
    public enum ItemQualityDefine
    {
        WHITE = 1,
        GREEN = 2,
        BLUE = 3,
        PURPLE = 4,
        YELLOW = 5,
        GOLDEN = 6,
        RED = 7,
    }
}
