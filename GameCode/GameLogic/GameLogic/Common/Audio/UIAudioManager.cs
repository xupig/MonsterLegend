﻿using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Audio
{
    public class UIAudioManager : MonoBehaviour
    {
        public static string DEFAULT_CLICK_AUDIO_PATH = "Sound/UI/onclick.mp3";

        private bool _enableAudio = false;
        private List<UIButtonAudioAgent> _audioAgentList = new List<UIButtonAudioAgent>();

        protected void Awake()
        {
            KComponentAudioCollective.playAudio = PlayAudioDelegate;
        }

        private void PlayAudioDelegate(string goPath)
        {
            PlayAudio(GetAudioAssetPath(goPath));
        }

        private string GetAudioAssetPath(string goPath)
        {
            string path = ui_audio_helper.GetAudioAssetPath(goPath);
            return path != string.Empty ? path : DEFAULT_CLICK_AUDIO_PATH;
        }

        /// <param name="audioPath">例如：Sound/UI/skill_update.mp3</param>
        public void PlayAudio(string audioPath)
        {
            UIButtonAudioAgent audioAgent = GetAvailableAudio();
            audioAgent.PlayAudio(audioPath);
        }

        private UIButtonAudioAgent GetAvailableAudio()
        {
            for (int i = 0; i < _audioAgentList.Count; i++)
            {
                UIButtonAudioAgent agent = _audioAgentList[i];
                if (agent.IsUsing == false)
                {
                    return agent;
                }
            }
            UIButtonAudioAgent audioAgent = gameObject.AddComponent<UIButtonAudioAgent>();
            audioAgent.enabled = _enableAudio;
            _audioAgentList.Add(audioAgent);
            return audioAgent;
        }

        public bool EnableAudio
        {
            get
            {
                return _enableAudio;
            }
            set
            {
                _enableAudio = value;
                for (int i = 0; i < _audioAgentList.Count; i++)
                {
                    _audioAgentList[i].enabled = value;
                }
            }
        }
    }
}
