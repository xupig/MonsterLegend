﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Game.Asset;
using Game.UI.UIComponent;
using GameData;

namespace Common.Audio
{
    public class UIButtonAudioAgent : MonoBehaviour
    {
        private AudioSource _audioSource;
        private bool _isloading = false;
        private string _currentPath = string.Empty;

        protected void Awake()
        {
            ObjectPool.Instance.DontDestroy(new string[] { UIAudioManager.DEFAULT_CLICK_AUDIO_PATH });
            _audioSource = gameObject.AddComponent<AudioSource>();
        }

        public void PlayAudio(string audioPath)
        {
            ReleaseAudioClip();
            _currentPath = audioPath;
            ObjectPool.Instance.GetAudioClip(audioPath, OnAudioClipLoaded);
            _isloading = true;
        }

        private void ReleaseAudioClip()
        {
            if (_currentPath != string.Empty)
            {
                ObjectPool.Instance.Release(_currentPath);
                _currentPath = string.Empty;
            }
        }

        private void OnAudioClipLoaded(AudioClip clip)
        {
            _isloading = false;
            _audioSource.loop = false;
            _audioSource.clip = clip;
            _audioSource.playOnAwake = false;
            _audioSource.volume = KComponentAudioCollective.audioVolume;
            _audioSource.Play();
        }

        public bool IsUsing
        {
            get
            {
                return _audioSource.isPlaying || _isloading;
            }
        }
    }
}
