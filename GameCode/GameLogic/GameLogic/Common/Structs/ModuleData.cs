﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class ChooseCharacterGridData
    {
        public string name;
        public string level;
        public string defautText;
        public string headImg;
    }
}
