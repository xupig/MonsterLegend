﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/24 13:50:47
 * 描述说明：
 * *******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs.Enum
{
    //1武器 2头盔 3肩膀 4胸甲 5手套 6腰带 7裤子 8鞋子 9项链 10戒指 ,11戒指 12翅膀 13情缘
    public enum EquipType
    {
        /// <summary>
        /// 武器
        /// </summary>
        weapon = 1,
        /// <summary>
        /// 头盔
        /// </summary>
        helmet = 2,
        /// <summary>
        /// 肩膀
        /// </summary>
        scapula = 3,
        /// <summary>
        /// 胸甲
        /// </summary>
        breastPlate = 4,
        /// <summary>
        /// 手套
        /// </summary>
        glove = 5,
        /// <summary>
        /// 腰带
        /// </summary>
        belt = 6,
        /// <summary>
        /// 裤子
        /// </summary>
        trouser = 7,
        /// <summary>
        /// 鞋子
        /// </summary>
        shoe = 8,
        /// <summary>
        /// 项链
        /// </summary>
        necklace = 9,
        /// <summary>
        /// 戒指
        /// </summary>
        ring = 10,
        /// <summary>
        /// 翅膀
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        wing = 12,
        /// <summary>
        /// 情缘
        /// </summary>
        love = 13,
    }
}
