﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs.Enum
{
    public static class SpecialItemType
    {
        public static int GOLD = 1;
        public static int DIAMOND = 2;
        public static int ENERGY = 18;
        public static int EXP_PLAYER = 50;
        public static int EXP_PET = 62;
        public static int SOULSTONE = 11000;
    }
}
