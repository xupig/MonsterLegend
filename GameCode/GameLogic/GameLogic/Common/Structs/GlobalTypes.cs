﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public enum Vocation
    {
        None = 0,
        Warrior = 1,
        Assassin = 2,
        Wizard = 3,
        Archer = 4,
        Others = 5,
    }

    public enum Gender
    {
        Female = 0,
        Male = 1,
    }
}
