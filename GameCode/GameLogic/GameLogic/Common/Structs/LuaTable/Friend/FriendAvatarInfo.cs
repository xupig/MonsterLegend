﻿#region 模块信息
/*==========================================
// 文件名：FriendInfo
// 命名空间: GameLogic.GameLogic.Common.Structs.LuaTable.Friend
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/16 20:28:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class FriendAvatarInfo
    {
        public string Name
        {
            get;
            set;
        }

        public Vocation Vocation
        {
            get;
            set;
        }

        public Gender Gender
        {
            get;
            set;
        }

        public UInt64 DbId
        {
            get;
            set;
        }

        public int FightForce
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public bool BlessState
        {
            get;
            set;
        }

        public int Intimate
        {
            get;
            set;
        }

        public int MapId
        {
            get;
            set;
        }
    }
}
