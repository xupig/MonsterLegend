﻿#region 模块信息
/*==========================================
// 文件名：IntimateItemInfo
// 命名空间: GameLogic.GameLogic.Common.Structs.LuaTable.Friend
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/28 10:25:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class IntimateItemInfo
    {
        public bool IsDirect;
        public intimate_item item;
    }
}
