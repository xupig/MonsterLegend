﻿#region 模块信息
/*==========================================
// 模块名：ItemInfo
// 命名空间: Common.Structs
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/12
// 描述说明：道具数据结构-所有道具都是该数据类
// 其他：RPC: {"get_pkg_data_req"}  
// 相关配置表：｛"道具总表"-item_commom｝
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameData;
using GameMain.Entities;
using Common.Utils;
using Common.ServerConfig;

using GameLoader.Utils.CustomType;
using GameLoader.Utils;

namespace Common.Structs
{

    public class PlayerInfo : IInfo
    {


        /**
        public static int    UM_PLAYER_BASE_MB_INDEX            = 1;
        public static int    UM_PLAYER_CELL_MB_INDEX            = 2;
        public static int    UM_PLAYER_DBID_INDEX               = 3;
        public static int    UM_PLAYER_NAME_INDEX               = 4;
        public static int    UM_PLAYER_LEVEL_INDEX              = 5;
        public static int    UM_PLAYER_VOCATION_INDEX           = 6;
        public static int    UM_PLAYER_GENDER_INDEX             = 7;
        public static int    UM_PLAYER_UNION_INDEX              = 8;
        public static int    UM_PLAYER_FIGHT_INDEX              = 9; //20级以下才有值，否则为空，20级及以上战斗力放进m_lFights中排序存储,通过UM_PLAYER_ARENIC_FIGHT_RANK_INDEX去取值
        public static int    UM_PLAYER_ONLINE_INDEX             = 10;
        public static int    UM_PLAYER_FRIEND_NUM_INDEX         = 11; //该值存储于offlinemgr，todo：解耦
        public static int    UM_PLAYER_OFFLINETIME_INDEX        = 12;
        public static int    UM_PLAYER_ITEMS_INDEX              = 13; //装备信息
        public static int    UM_PLAYER_BATTLE_PROPS             = 14; //20级及以上才有值，否则为空
        public static int    UM_PLAYER_SPELL_BAG                = 15; //20级及以上才有值，否则为空
        public static int    UM_PLAYER_LOADED_ITEMS             = 16; //20级及以上才有值，否则为空
        public static int    UM_PLAYER_ARENIC_FIGHT_RANK_INDEX  = 18; //竞技场标准战斗力排名
        public static int    UM_PLAYER_ARENIC_GRADE_INDEX       = 19; //
        public static int    UM_PLAYER_IDOL_INDEX               = 20; //角色偶像
        public static int    UM_PLAYER_GM_SETTING               = 21; //gm配置的行为控制
        public static int    UM_PLAYER_ACCOUNT                  = 22; //帐号名字
        public static int    UM_PLAYER_TITLE_INDEX              = 23; //称号
        public static int    UM_PLAYER_SEAL_SPIRIT_INDEX        = 24; //宝石封灵
        public static int    UM_PLAYER_TEAMING_INDEX            = 25; //teaming 是否在组队状态
        public static int    UM_PLAYER_TEAM_LEADER_INDEX        = 26; //teamLeader 是否队长
        public static int    UM_PLAYER_MAP_ID_INDEX             = 27; //场景id.mpa_id,通过map_id可得inst_id
        **/


        public static Dictionary<object, object> s_dicProp;

        public PlayerInfo()
        {
            s_dicProp = new Dictionary<object, object>();
            s_dicProp.Add(public_config.ITEM_ATTRI_ID, "id");
            s_dicProp.Add(public_config.ITEM_ATTRI_COUNT, "count");
            s_dicProp.Add(public_config.ITEM_ATTRI_SLOTS, "slots");
            s_dicProp.Add(public_config.ITEM_ATTRI_NEW_SLOT_CNT, "slotActivateNum");
            s_dicProp.Add(public_config.ITEM_ATTRI_EQUIP_VALUE, "equipValueDic");
            s_dicProp.Add(public_config.ITEM_ATTRI_EQUIP_BUFF, "equipBuffArr");
            s_dicProp.Add(public_config.ITEM_ATTRI_CUR_EXP, "curExp");
            s_dicProp.Add(public_config.ITEM_ATTRI_RUNE_LOCK, "runeLock");
            s_dicProp.Add(public_config.ITEM_ATTRI_UUID, "uuid");
            s_dicProp.Add(public_config.ITEM_ATTRI_DEAD_TIME, "deadTime");
            s_dicProp.Add(public_config.ITEM_ATTRI_SUIT, "suitId");
            s_dicProp.Add(8, "rune");
        }


    }

}