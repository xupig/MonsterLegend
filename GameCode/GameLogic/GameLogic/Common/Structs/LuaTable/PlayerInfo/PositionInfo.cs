﻿#region 模块信息
/*==========================================
// 文件名：FriendActionInfo
// 命名空间: GameLogic.GameLogic.Common.Structs.LuaTable.Friend
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/30 17:20:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Structs
{
    public class PositionInfo
    {
        public PbFriendAvatarInfo avatarInfo;
        public Vector2 pos = new Vector2();
    }
}
