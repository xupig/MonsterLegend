﻿using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Utils;

namespace Common.Structs
{
    public class FavorInfo
    {
        private PbReputationInfo _reputationInfo;
        private npc_reputation _npcFavor;
        private npc _npc;
        private List<KeyValuePair<int, int>> _acceptGift;

        /// <summary>
        /// 根据服务器返回数据创建新的FavorInfo
        /// </summary>
        /// <param name="reputationInfo"></param>
        public FavorInfo(PbReputationInfo reputationInfo)
        {
            UpdateServerData(reputationInfo);
        }

        /// <summary>
        /// 在没有服务器返回数据时
        /// </summary>
        /// <param name="npcId"></param>
        //public FavorInfo(int npcId)
        //{
        //    _npc = npc_helper.GetNpc(npcId);
        //}

        /// <summary>
        /// 仅仅更新服务器返回数据
        /// </summary>
        /// <param name="reputationInfo"></param>
        public void UpdateServerData(PbReputationInfo reputationInfo)
        {
            _reputationInfo = reputationInfo;
            if (_reputationInfo.level == -1)
            {
                _reputationInfo = null;
                return;
            }
            _acceptGift = null;
            _npcFavor = npc_reputation_helper.GetNpcReputation(_reputationInfo.npc_id, _reputationInfo.level);
            _npc = npc_helper.GetNpc((int)_reputationInfo.npc_id);
        }


        //private void InitConfig()
        //{
        //    if (_reputationInfo.level == -1)
        //    {
        //        _reputationInfo = null;
        //        return;
        //    }
        //    _npcFavor = npc_reputation_helper.GetNpcReputation(_reputationInfo.npc_id, _reputationInfo.level);
        //}

        public int NpcId
        {
            get
            {
                return _reputationInfo.npc_id;
            }
        }

        public int UpgradeFavorPoint
        {
            get
            {
                return _npcFavor.__upgrade_count;
            }
        }

        public bool IsHaveServerData
        {
            get
            {
                return _reputationInfo != null;
            }
        }

        public int Level
        {
            get
            {
                return _reputationInfo.level;
            }
        }

        public bool IsPromote
        {
            get
            {
                return _npcFavor.__upgrade_count != 0;
            }
        }

        public int CurrentFavorPoint
        {
            get
            {
                return _reputationInfo.count;
            }
        }

        public List<KeyValuePair<int, int>> AcceptGift
        {
            get
            {
                if (_acceptGift == null)
                {
                    _acceptGift = new List<KeyValuePair<int, int>>();
                    if (_npcFavor != null)
                    {
                        Dictionary<string, string> dataDict = data_parse_helper.ParseMap(_npcFavor.__accept_gift);
                        foreach (KeyValuePair<string, string> kvp in dataDict)
                        {
                            _acceptGift.Add(new KeyValuePair<int, int>(int.Parse(kvp.Key), int.Parse(kvp.Value)));
                        }
                    }
                }
                return _acceptGift;
            }
        }

        public int Dialogue
        {
            get
            {
                return npc_helper.GetRandomDialogID(_npc.__id);
            }
        }

        public string NpcName
        {
            get
            {
                return MogoLanguageUtil.GetContent(_npc.__name);
            }
        }
    }
}
