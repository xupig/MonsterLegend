﻿using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using Common.Data;
using GameData;
using Common.Structs.Enum;

namespace Common.Structs
{
    /// <summary>
    /// 背包格子中装备物品信息
    /// </summary>
    public class EquipItemInfo : BaseItemInfo
    {
        public EquipItemInfo(PbGridItem gridItem)
            : base(gridItem)
        {
        }

        protected override BaseItemData CreateItemData()
        {
            return new EquipData((int)_gridItem.item.id);
        }

        public item_equipment BaseConfig
        {
            get
            { 
                return (_itemData as EquipData).BaseConfig; 
            }
        }

        public equip_value ValueConfig
        {
            get 
            {
                return (_itemData as EquipData).ValueConfig;
            }
        }

        public EquipType SubType
        {
            get
            {
                return (_itemData as EquipData).SubType;
            }
        }

        public int Star
        {
            get
            {
                return (_itemData as EquipData).Star;
            }
        }

        /// <summary>
        /// 基础属性Id列表
        /// </summary>
        /// <returns></returns>
        public List<int> GetBasisPropertyIdList()
        {
            return (_itemData as EquipData).GetBasisPropertyIdList();
        }
        /// <summary>
        /// 基础属性值列表，其长度值和Id列表长度一致
        /// </summary>
        /// <returns></returns>
        public List<int> GetBasisPropertyValueList()
        {
            return (_itemData as EquipData).GetBasisPropertyValueList();
        }

        /// <summary>
        /// 装备稀有属性Id列表，由服务器通过装备属性随机而来
        /// </summary>
        /// <returns></returns>
        public List<int> GetRarityPropertyIdList()
        {
            List<int> result = new List<int>();
            for(int i = 0; i < _gridItem.item.equip_attries.Count; i++)
            {
                result.Add((int)_gridItem.item.equip_attries[i].attri_id);
            }
            return result;
        }

        /// <summary>
        /// 装备稀有属性值列表，由服务器通过装备属性随机而来
        /// </summary>
        /// <returns></returns>
        public List<int> GetRarityPropertyValueList()
        {
            List<int> result = new List<int>();
            for(int i = 0; i < _gridItem.item.equip_attries.Count; i++)
            {
                result.Add((int)_gridItem.item.equip_attries[i].attri_value);
            }
            return result;
        }

        public int SuitId
        {
            get
            {
                return (int)_gridItem.item.suit_id;
            }
        }

        /// <summary>
        /// 装备上宝石槽位列表
        /// </summary>
        public List<int> SlotList
        {
            get
            {
                return (_itemData as EquipData).SlotList;
            }
        }

        /// <summary>
        /// 装备上可扩展
        /// </summary>
        public List<int> NewSlotList
        {
            get
            {
                return (_itemData as EquipData).NewSlotList;
            }
        }

        /// <summary>
        /// 重铸次数
        /// </summary>
        public int RecastCount
        {
            get
            {
                return (int)_gridItem.item.recast_count;
            }
        }

        /// <summary>
        /// 已开孔的NewSlot数量
        /// </summary>
        public int OpenedNewSlotCount
        {
            get
            {
                return (int)_gridItem.item.new_slot_cnt;
            }
        }

        public int Level
        {
            get
            {
                return (_itemData as EquipData).Level;
            }
        }
        
        /// <summary>
        /// 装备上镶嵌宝石列表,index 大于等于21的为可扩展槽（NewSlot）上的宝石
        /// </summary>
        public List<PbEquipGems> GemList
        {
            get
            {
                return _gridItem.item.gems;
            }
        }

        public List<uint> BuffList
        {
            get
            {
                return _gridItem.item.equip_buffs;
            }
        }

        /// <summary>
        /// 战斗力
        /// </summary>
        public int FightForce
        {
            get
            {
                return (int)_gridItem.item.fight_force;
            }
        }

        public int StrengthenLevel
        {
            get
            {
                return (int)_gridItem.item.strengthen_level;
            }
        }

        public List<PbEquipEnchantAttri> EnchantAtrri
        {
            get
            {
                return _gridItem.item.enchant_attri;
            }
        }

    }
}
