﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;

namespace Common.Structs
{
    /// <summary>
    /// 普通物品（不包含装备）,配置信息来自item_common.xml
    /// </summary>
    public class CommonItemInfo : BaseItemInfo
    {
        public CommonItemInfo(PbGridItem gridItem)
            : base(gridItem)
        {
        }

        protected override BaseItemData CreateItemData()
        {
            return new ItemData((int)_gridItem.item.id);
        }

        public item_commom BaseConfig
        {
            get 
            {
                return (_itemData as ItemData).BaseConfig; 
            }
        }


        public uint ExpireTime
        {
            get
            {
                return _gridItem.item.dead_time;
            }
        }
    }
}
