﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Common.ServerConfig;
using Common.Global;
using GameData;

namespace Common.Structs
{
    /// <summary>
    /// 背包格子中物品信息基础类
    /// </summary>
    public abstract class BaseItemInfo : IItem
    {
        protected PbGridItem _gridItem;
        protected BaseItemData _itemData;

        public BaseItemInfo(PbGridItem pbGridItem)
        {
            _gridItem = pbGridItem;
            _itemData = CreateItemData();
        }

        protected abstract BaseItemData CreateItemData();

        public BaseItemData ItemData
        {
            get
            {
                return _itemData;
            }
        }
        /// <summary>
        /// 物品Id
        /// </summary>
        public int Id
        {
            get 
            {
                return (int)_gridItem.item.id; 
            }
        }

        /// <summary>
        /// 物品堆叠数量
        /// </summary>
        public int StackCount
        {
            get { return (int)_gridItem.item.count; }
        }

        public int MaxStackCount
        {
            get
            {
                return _itemData.MaxStackCount;
            }
        }

        /// <summary>
        /// 背包格子编号
        /// </summary>
        public int GridPosition
        {
            get
            {
                return (int)_gridItem.grid_index; 
            }
        }

        /// <summary>
        /// 物品名称
        /// </summary>
        public string Name 
        { 
            get
            {
                return _itemData.Name;
            }
        }

        public virtual string ColorName
        {
            get
            {
                return _itemData.ColorName;
            }
        }

        /// <summary>
        /// 物品类型编号
        /// </summary>
        public BagItemType Type
        {
            get
            {
                return _itemData.Type;
            }
        }
        /// <summary>
        /// 物品图标文件名
        /// </summary>
        public int Icon
        {
            get
            {
                return _itemData.Icon;
            }
        }

        /// <summary>
        /// 物品品质
        /// </summary>
        public int Quality
        {
            get
            {
                return _itemData.Quality;
            }
        }

        public bool IsBind 
        {
            get
            {
                return _gridItem.item.is_unbind == 0;
            }
        }

        /// <summary>
        /// 物品描述，显示在物品Tips中
        /// </summary>
        public virtual string Description
        {
            get
            {
                return _itemData.Description;
            }
        }
        /// <summary>
        /// 物品背景故事，由物品Tips面板上故事按钮开启
        /// </summary>
        public string Story
        {
            get
            {
                return _itemData.Story;
            }
        }

        public int UsageLevelRequired
        {
            get
            {
                return _itemData.UsageLevelRequired;
            }
        }
        public Vocation UsageVocationRequired
        {
            get
            {
                return _itemData.UsageVocationRequired;
            }
        }
        public int UsageVipLevelRequired
        {
            get
            {
                return _itemData.UsageVipLevelRequired;
            }
        }

        public Dictionary<int,int> SellPrice
        {
            get
            {
                return _itemData.SellPrice;
            }
        }

        public uint GetTime
        {
            get
            {
                return _gridItem.item.get_time;
            }
        }

        public List<ulong> UserList
        {
            get
            {
                return _gridItem.item.user_list;
            }
        }


    }
}
