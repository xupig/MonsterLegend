﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;

using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace Common.Structs
{
    public class RuneItemInfo : CommonItemInfo
    {
        public static string RuneNameContent = "{0}{1}";

        /// <summary>
        /// 被选中，准备被吞噬
        /// </summary>
        public bool IsSelect;

        public RuneItemInfo(PbGridItem gridItem)
            : base(gridItem)
        {
            InitConfig();
        }

        private void InitConfig()
        {
            _runeConfig = XMLManager.rune[this.Id];
        }

        /// <summary>
        /// 对应配置表-符文表
        /// </summary>
        private rune _runeConfig;
        public rune RuneConfig
        {
            get { return _runeConfig; }
        }

        public int MaxLevel
        {
            get { return data_parse_helper.ParseListString(_runeConfig.__exp_need).Count + 1; }
        }

        /// <summary>
        /// 经验转换成等级
        /// </summary>
        /// <param name="curExp"></param>
        /// <returns></returns>
        public int Exp2Level(int curExp)
        {
            if(_runeConfig.__exp_need==null)
            {
                return 1;
            }
            Dictionary<string, string> dict = data_parse_helper.ParseMap(_runeConfig.__exp_need);
            int maxLevel = dict.Count;
            int i = 1;
            while (curExp >= 0 && i <= maxLevel)
            {
                curExp -= int.Parse(dict[i.ToString()]);
                i++;
            }
            if (curExp<0)
            {
                i -= 1;
            }
            return i;
        }

        public override string Description
        {
            get
            {
                return GetDescField("1");
            }
        }

        public Dictionary<string,string> UseEffect
        {
            get {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(BaseConfig.__use_effect);
                return dict;
            }
        }

        private string GetDescField(string key)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(BaseConfig.__desc);
            if (BaseConfig.__desc != null && dict.ContainsKey(key) == true)
            {
                return MogoLanguageUtil.GetContent(dict[key]);
            }
            return string.Empty;
        }

        public int Level
        {
            get
            {
                return Exp2Level((int)this._gridItem.item.cur_exp);
            }
        }

        /// <summary>
        /// 当前经验的级满经验
        /// </summary>
        /// <param name="curExp"></param>
        /// <returns></returns>
        public int LevelMaxExp(int curExp)
        {
            int i = 1;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(_runeConfig.__exp_need);
            int nextLevelExp = int.Parse(dict[i.ToString()]);
            int maxLevel = dict.Count;
            while (curExp >= nextLevelExp && i <= maxLevel)
            {
                i++;
                nextLevelExp += int.Parse(dict[i.ToString()]);
            }
            return nextLevelExp;
        }

        public int LevelMinExp(int curExp)
        {
            int i = 0;
            int currentLevelExp = 0;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(_runeConfig.__exp_need);
            int maxLevel = dict.Count;
            while (curExp >= currentLevelExp && i <= maxLevel)
            {
                i++;
                currentLevelExp += int.Parse(dict[i.ToString()]);
            }
            if(currentLevelExp >= curExp)
            {
                currentLevelExp -= int.Parse(dict[i.ToString()]);
            }
            return currentLevelExp;
        }

        public bool CanCombine(RuneItemInfo combineRune)
        {
            if (combineRune == null || combineRune.RuneConfig.__subtype == public_config.RUNE_MONEY || (combineRune.RuneConfig.__subtype == public_config.RUNE_EXP && _runeConfig.__subtype == public_config.RUNE_EXP))
            {
                return false;
            }
            return true;
        }


        public int EffectId
        {
            get 
            {
                Dictionary<string, string> effectDict = data_parse_helper.ParseMap(_runeConfig.__level_effect);
                int level = 1;
                foreach(KeyValuePair<string,string> kvp in effectDict)
                {
                    int currentLevel = int.Parse(kvp.Key);
                    if(currentLevel <= Level )
                    {
                        level = Math.Max(level,currentLevel);
                    }
                }
                return int.Parse(effectDict[level.ToString()]);
            }
        }

       

        public bool IsSelectable
        {
            get { return RuneLock == 0 && IsSelect == false; }
        }

        public bool CanAutoCombine
        {
            get { return RuneLock == 0 && IsSelect == false  && ( RuneConfig.__subtype == public_config.RUNE_EXP || Quality < GlobalParams.RuneAutoSwallowLevelLimit); }
        }
        public int RuneLock
        {
            get { return (int)_gridItem.item.rune_lock; }
            set { _gridItem.item.rune_lock = (uint)value; }
        }

        public int CurExp
        {
            get { return (int)_gridItem.item.cur_exp; }
        } 
    }
}
