﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;


namespace Common.Structs
{
    public class GemItemInfo : CommonItemInfo
    {
        private item_gem _gemConfig;

        public GemItemInfo(PbGridItem pbGridItem)
            : base(pbGridItem)
        {
            InitConfig();
        }

        private void InitConfig()
        {
            _gemConfig = XMLManager.item_gem[this.Id];
        }

        /// <summary>
        /// 宝石类型
        /// </summary>
        public int SubType
        {
            get
            {
                return _gemConfig.__subtype;
            }
        }

        public int NextId
        {
            get
            {
                return _gemConfig.__next_id;
            }
        }

        /// <summary>
        /// 宝石等级
        /// </summary>
        public int Level
        {
            get
            {
                return _gemConfig.__gem_level;
            }
        }

        /// <summary>
        /// 宝石可镶嵌装备相应槽列表
        /// </summary>
        /// <returns></returns>
        public List<int> SlotList
        {
            get
            {
                return data_parse_helper.ParseListInt(_gemConfig.__gem_slot);
            }
        }

    }
}
