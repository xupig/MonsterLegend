﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class AttachmentItemInfo : IInfo
    {

        public AttachmentItemInfo()
        {

        }
        public AttachmentItemInfo(BaseItemInfo _item)
        {
            Id = _item.Id;
            Count = _item.StackCount;
            GridIndex = _item.GridPosition;
            Name = _item.Name;
            DescPurpose = _item.Description;
        }

        public AttachmentItemInfo(AttachmentItemInfo _item)
        {
            Id = _item.Id;
            Count = _item.Count;
            GridIndex = _item.GridIndex;
            Name = _item.Name;
            DescPurpose = _item.DescPurpose;
        }

        public int GridIndex
        {
            get;
            set;
        }

        /// <summary>
        /// 物品的id
        /// </summary>
        public int Id 
        {
            get;
            set;
        }

        /// <summary>
        /// 物品数量
        /// </summary>
        public int Count
        {
            get;
            set;
        }

        /// <summary>
        /// 物品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 物品描述
        /// </summary>
        public string DescPurpose { get; set; }
    }
}
