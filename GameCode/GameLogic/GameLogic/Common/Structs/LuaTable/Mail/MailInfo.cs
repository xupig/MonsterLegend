﻿using GameLoader.Utils.CustomType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class MailInfo : IInfo
    {
        public MailInfo()
        {
        }
        public UInt64 avatar_dbid
        {
            get;
            set;
        }
        public string title
        {
            get;
            set;
        }
        public string text
        {
            get;
            set;
        }
        public string from
        {
            get;
            set;
        }
        public UInt32 time
        {
            get;
            set;
        }
        public LuaTable attachment
        {
            get;
            set;
        }
        public UInt32 mail_type
        {
            get;
            set;
        }
    }
}