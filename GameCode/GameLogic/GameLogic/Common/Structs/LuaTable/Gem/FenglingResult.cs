﻿#region 模块信息
/*==========================================
// 文件名：FenglingResult
// 命名空间: GameLogic.GameLogic.Common.Structs.LuaTable.Gem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/2/7 17:11:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class FenglingResult : IInfo
    {
        public static Dictionary<object, object> s_dicProp;

        public FenglingResult()
        {
            s_dicProp = new Dictionary<object,object>();

            s_dicProp.Add(1, "common_bless");
            s_dicProp.Add(2, "little_hit_bless");
            s_dicProp.Add(3, "big_hit_bless");
        }

        public uint common_bless
        {
            get;
            set;
        }

        public uint little_hit_bless
        {
            get;
            set;
        }

        public uint big_hit_bless
        {
            get;
            set;
        }

    }
}
