﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;

using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace Common.Structs
{
    public class DemonGateItemInfo 
    {
        public DemonGateItemInfo(DemonGateItemInfo gridItem)
        {
            this.id = gridItem.id;
            this.rank = gridItem.rank;
            this.rewards = gridItem.rewards;
        }

        public DemonGateItemInfo()
        {

        }

        public int id;

        public string rank;

        public List<RewardData> rewards;

        public int rankAverageValue;

    }
}
