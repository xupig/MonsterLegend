﻿#region 模块信息
/*==========================================
// 文件名：MakeFormulaData
// 命名空间: GameLogic.GameLogic.Common.Structs.LuaTable.Make
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/30 20:52:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class MakeFormulaData
    {
        public List<mfg_formula> formulaList;
        public int group;
    }
}
