﻿#region 模块信息
/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：LuaCharaterInfo
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2013.3.26
// 模块描述：角色信息实体。
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;

namespace Common.Structs
{
    /// <summary>
    /// 角色信息实体。对应 Account 实体的 avatarsInfo 的 Lua table 模板
    /// </summary>
    public class AvatarInfo
    {
        public ulong DBID { get; set; }
        public string Name { get; set; }
        public int Vocation { get; set; }
        public int Level { get; set; }

        /// <summary>
        /// 角色模型，武器，翅膀信息
        /// </summary>
        public AvatarModelData avatarModelData;

        /*/// <summary>
        /// 武器
        /// </summary>
        public int Weapon { get; set; }
        /// <summary>
        /// 胸甲
        /// </summary>
        public int Cuirass { get; set; }
        /// <summary>
        /// 鞋子
        /// </summary>
        public int Shoes { get; set; }
        /// <summary>
        /// 护手
        /// </summary>
        public int Armguard { get; set; }
        /// <summary>
        /// 翅膀
        /// </summary>
        public int Wing { get; set; }
        /// <summary>
        /// 宝石特效
        /// </summary>
        public int Jewel { get; set; }
        /// <summary>
        /// 装备特效
        /// </summary>
        public int Equip { get; set; }
        /// <summary>
        /// 强化特效
        /// </summary>
        public int Strge { get; set; }
        /// <summary>
        /// 排行榜排名索引(仅排行榜所用)
        /// </summary>
        public int ShowLevel { get; set; }
        /// <summary>
        /// 公会贡献值
        /// </summary>
        public int Contribute { get; set; }
        /// <summary>
        /// 时装
        /// </summary>
        public int Fashion { get; set; }
        /// <summary>
        /// 魔魂
        /// </summary>
        public int MagicSoul { get; set; }

        public static Dictionary<object, object> s_dicProp;*/

        public AvatarInfo()
        {
            avatarModelData = new AvatarModelData();
            /*s_dicProp = new Dictionary<object, object>();
            s_dicProp.Add(1, "DBID");
            s_dicProp.Add(2, "Name");
            s_dicProp.Add(3, "Vocation");
            s_dicProp.Add(4, "Level");
            s_dicProp.Add(5, "Weapon");
            s_dicProp.Add(6, "Cuirass");
            s_dicProp.Add(7, "Shoes");
            s_dicProp.Add(8, "Armguard");
            s_dicProp.Add(9, "Wing");
            s_dicProp.Add(10, "Jewel");
            s_dicProp.Add(11, "Equip");

            s_dicProp.Add(12, "Strge");
            s_dicProp.Add(13, "ShowLevel");
            s_dicProp.Add(14, "Contribute");
            s_dicProp.Add(15, "Fashion");
            s_dicProp.Add(16, "MagicSoul");*/
        }
    }
}
