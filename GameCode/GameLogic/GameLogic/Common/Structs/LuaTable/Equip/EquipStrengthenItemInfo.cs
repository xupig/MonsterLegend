﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Structs
{
    public class EquipStrengthenItemInfo
    {
        private PbStrengthenInfo _pbStrengthenInfo;
        private EquipItemInfo _equipItemInfo;

        private strengthen _beforeConfig;
        private strengthen _afterConfig;

        private List<AttributeChangedItemInfo> _changedInfoList;
        private List<BaseItemData> _needMaterial;

        public EquipStrengthenItemInfo(PbStrengthenInfo strengthenInfo)
        {
            _pbStrengthenInfo = strengthenInfo;
            _equipItemInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(strengthenInfo.id) as EquipItemInfo;
            if (_equipItemInfo == null) return;
            GetAttribute();
            FillChangedInfoList();
            FillNeedMaterial();
        }

        public EquipStrengthenItemInfo(EquipItemInfo equipItemInfo)
        {
            _equipItemInfo = equipItemInfo;
            if (_equipItemInfo == null) return;
            GetAttribute();
            FillChangedInfoList();
            FillNeedMaterial();
        }

        public PbStrengthenInfo ServerStrengthenInfo
        {
            get
            {
                return _pbStrengthenInfo;
            }
        }

        public EquipItemInfo EquipItemInfo
        {
            get
            {
                return _equipItemInfo;
            }
        }

        public List<AttributeChangedItemInfo> ChangedInfoList
        {
            get
            {
                return _changedInfoList;
            }
        }
        
        /// <summary>
        /// 强化的部位ID
        /// </summary>
        public int StrengthenId
        {
            get
            {
                return _equipItemInfo.GridPosition;
            }
        }

        /// <summary>
        /// 装备Id
        /// </summary>
        public int EquipId
        {
            get
            {
                return _equipItemInfo.Id;
            }
        }

        public int EquipIcon
        {
            get
            {
                return _equipItemInfo.Icon;
            }
        }

        /// <summary>
        /// 强化的等级
        /// </summary>
        public int StrengthenLevel
        {
            get
            {
                if (_equipItemInfo != null)
                {
                    return _equipItemInfo.StrengthenLevel;
                }
                else if (_pbStrengthenInfo != null)
                {
                    return _pbStrengthenInfo.level;
                }
                return 0;
            }
        }

        public bool IsMaxLevel
        {
            get
            {
                return strengthen_helper.GetStrengthenAttribute(_equipItemInfo.SubType, _equipItemInfo.StrengthenLevel + 1).Count == 0;
            }
        }

        /// <summary>
        /// 强化的等级上限
        /// </summary>
        public int StrengthenUpperLimit
        {
            get
            {
                //Dictionary<int, float> strengthenFactor;
                //strengthenFactor = MogoStringUtils.Convert2DicFloat(XMLManager.global_params[28].__value);
                //item_equipment equipConfig = item_helper.GetEquipConfig(_equipItemInfo.Id);
                //if (equipConfig.__strength_limit == 0)
                //{
                //    return (int)(strengthenFactor[_equipItemInfo.Quality] * _equipItemInfo.Level);
                //}
                if (_equipItemInfo != null && item_helper.GetEquipConfig(_equipItemInfo.Id).__strength_limit != 0)
                {
                    return item_helper.GetEquipConfig(_equipItemInfo.Id).__strength_limit;
                }
                int strengthen_level_times = int.Parse(global_params_helper.GetGlobalParam(107));
                return PlayerAvatar.Player.level * strengthen_level_times;
            }
        }

        /// <summary>
        /// 强化的成功率，除幸运石附件成功率之外的成功率
        /// </summary>
        public int SuccessRate
        {
            get
            {
                if (_afterConfig == null)
                {
                    return 10000;
                }
                else if (_pbStrengthenInfo == null)
                {
                    return _afterConfig.__rate;
                }
                return _afterConfig.__rate + _pbStrengthenInfo.addition_rate;
            }
        }

        /// <summary>
        /// 幸运石槽的数量
        /// </summary>
        public int SlotNum
        {
            get
            {
                if (_beforeConfig != null)
                {
                    return _beforeConfig.__slot_num;
                }
                return 0;
            }
        }

        /// <summary>
        /// 强化所需材料消耗
        /// </summary>
        public List<BaseItemData> NeedMaterial
        {
            get { return _needMaterial; }
        }

        private void FillNeedMaterial()
        {
            _needMaterial = strengthen_helper.GetStrengthenNeedMaterial((int)_equipItemInfo.SubType, _equipItemInfo.StrengthenLevel + 1);
        }

        public bool IsCanStrenghthen
        {
            get
            {
                if (_equipItemInfo == null)
                {
                    return false;
                }
                else if (_equipItemInfo.SubType == EquipType.glove)
                {
                    return StrengthenLevel < StrengthenUpperLimit && PlayerAvatar.Player.CheckCostLimit(NeedMaterial) == 0;
                }
                else
                {
                    return StrengthenLevel < StrengthenUpperLimit && PlayerAvatar.Player.CheckCostLimit(NeedMaterial) == 0 && (_equipItemInfo.Quality >= 4 || _equipItemInfo.StrengthenLevel > 0);
                }
            }
        }

        private void FillChangedInfoList()
        {

            _changedInfoList = new List<AttributeChangedItemInfo>();

            //if (StrengthenLevel >= StrengthenUpperLimit)
            //{
            //    FillUpLimitAttri();
            //}
            //else
            //{
                FillStrengthenAttri();
            //}
        }

        /// <summary>
        /// 达到强化上限时的属性
        /// </summary>
        private void FillUpLimitAttri()
        {
            List<string> attriList = data_parse_helper.ParseListString(XMLManager.attri_effect[_beforeConfig.__attribute_id].__attri_ids);
            List<string> attriValueList = data_parse_helper.ParseListString(XMLManager.attri_effect[_beforeConfig.__attribute_id].__attri_values);

            for (int i = 0; i < attriList.Count; i++)
            {
                int beforeChanged;
                beforeChanged = int.Parse(attriValueList[i]);

                _changedInfoList.Add(new AttributeChangedItemInfo(int.Parse(attriList[i]), beforeChanged, -1));
            }
        }

        private void FillStrengthenAttri()
        {
            Dictionary<int, int> attributeAfterDic = strengthen_helper.GetStrengthenAttribute(_equipItemInfo.SubType, _equipItemInfo.StrengthenLevel + 1);
            Dictionary<int, int> attributeBeforeDic = strengthen_helper.GetStrengthenAttribute(_equipItemInfo.SubType, _equipItemInfo.StrengthenLevel);

            List<int> attributeIdList = attributeAfterDic.Keys.ToList();
            if (attributeIdList.Count == 0)
            {
                attributeIdList = attributeBeforeDic.Keys.ToList();
            }
            if (attributeIdList.Count == 0)
            {
                throw new Exception(string.Format("不包含强化部位id为{0}，强化等级为{1}的属性配置", _equipItemInfo.SubType, _equipItemInfo.StrengthenLevel));
            }
            
            for (int i = 0; i < attributeIdList.Count; i++)
            {
                int afterChanged = -1;
                int beforeChanged = 0;
                if (attributeBeforeDic.Count != 0)
                {
                    beforeChanged = attributeBeforeDic[attributeIdList[i]];
                }
                if (attributeAfterDic.Count != 0)
                {
                    afterChanged = attributeAfterDic[attributeIdList[i]];
                }
                _changedInfoList.Add(new AttributeChangedItemInfo(attributeIdList[i], beforeChanged, afterChanged));
            }
        }

        private void GetAttribute()
        {
            List<strengthen> strengthenList = XMLManager.strengthen.Values.ToList();
            _beforeConfig = strengthen_helper.getStrengthenConfigByEquipTypeAndLevel((int)_equipItemInfo.SubType,StrengthenLevel);
            _afterConfig = strengthen_helper.getStrengthenConfigByEquipTypeAndLevel((int)_equipItemInfo.SubType,StrengthenLevel + 1);
        }
    }

    public class AttributeChangedItemInfo
    {
        private int _beforeChanged;
        private int _afterChanged;
        private int _attributeId;

        public AttributeChangedItemInfo(int attributeId,int beforeChanged, int afterChanged)
        {
            _beforeChanged = beforeChanged;
            _afterChanged = afterChanged;
            _attributeId = attributeId;
        }

        public int BeforeChanged
        {
            get{ return _beforeChanged; }
        }

        public int AfterChanged
        {
            get{ return _afterChanged; }
        }

        public int AttributeId
        {
            get { return _attributeId; }
        }

        public string AttributeName
        {
            get { return attri_config_helper.GetAttributeName(_attributeId); }
        }
    }
}
