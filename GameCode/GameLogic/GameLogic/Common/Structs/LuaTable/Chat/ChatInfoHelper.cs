﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;

namespace Common.Structs
{
    public class ChatInfoHelper
    {

        public static PbChatInfoResp CreateClientInfo(byte channeId, string content, ulong otherId = 0, bool isSystem = false, int chatMsgType = public_config.CHAT_MSG_TYPE_TEXT)
        {
            PbChatInfoResp info = new PbChatInfoResp();
            info.channel_id = channeId;
            info.send_dbid = isSystem ? 0 : PlayerAvatar.Player.dbid;
            info.send_name = PlayerAvatar.Player.name;
            info.send_level = PlayerAvatar.Player.level;
            info.msg = content;
            info.accept_dbid = otherId;
            info.msg_type = (uint)chatMsgType;
            return info;
        }

        public static bool IsSelf(PbChatInfoResp  chatInfo)
        {
            return chatInfo.send_dbid == PlayerAvatar.Player.dbid;
        }

        public static bool isSystem(PbChatInfoResp chatInfo)
        {
            return chatInfo.send_dbid == 0;
        }

        public static bool showTag(int showInChannel, PbChatInfoResp chatInfo)
        {
            return ( showInChannel <= public_config.CHANNEL_ID_WORLD || isSystem( chatInfo ) );
        }

        public void SetToCurPlayer(PbChatInfoResp chatInfo)
        {
            chatInfo.send_dbid = PlayerAvatar.Player.dbid;
        }

        public void SetToSystem(PbChatInfoResp chatInfo)
        {
            chatInfo.send_dbid = 0;
        }

    }
}
