﻿

using System.Collections.Generic;
using Common.Data;
using Common.Structs.ProtoBuf;
using UnityEngine;

namespace Common.Structs
{
    public static class PbItemHelper
    {
        public static Dictionary<int, int> ParseRewardToDic(List<PbItem> itemList)
        {
            Dictionary<int, int> itemDic = new Dictionary<int, int>();
            if (itemList != null)
            {
                for (int i = 0; i < itemList.Count; i++)
                {
                    int itemId = (int) itemList[i].item_id;
                    int itemCount = (int) itemList[i].item_count;
                    if (itemDic.ContainsKey(itemId))
                    {
                        itemDic[itemId] = itemDic[itemId] + itemCount;
                    }
                    else
                    {
                        itemDic.Add(itemId, itemCount);
                    }
                }
            }
            return itemDic;
        }

        public static List<ItemData> ParsePbItemListToItemDataList(List<PbItem> itemList)
        {
            List<ItemData> itemDataList = new List<ItemData>();
            Dictionary<int, int> itemDic = ParseRewardToDic(itemList);
            if (itemList != null)
            {
                foreach (int itemId in itemDic.Keys)
                {
                    ItemData itemData = new ItemData(itemId);
                    itemData.StackCount = itemDic[itemId];
                    itemDataList.Add(itemData);
                }
            }
            return itemDataList;
        }

        public static List<PbItem> MergePbItemList(List<PbItem> itemList)
        {
            List<PbItem> pbItemList = new List<PbItem>();
            Dictionary<int, int> itemDic = ParseRewardToDic(itemList);
            foreach (int itemId in itemDic.Keys)
            {
                PbItem pbItem = new PbItem();
                pbItem.item_id = (uint)itemId;
                pbItem.item_count = (uint)itemDic[itemId];
                pbItemList.Add(pbItem);
            }
            return pbItemList;
        }

        public static List<RewardData> ParsePbItemListToRewardDataList(List<PbItem> itemList)
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < itemList.Count; i++)
            {
                RewardData rewardData = RewardData.GetRewardData();
                rewardData.id = (int) itemList[i].item_id;
                rewardData.num = (int) itemList[i].item_count;
                rewardDataList.Add(rewardData);
            }
            return rewardDataList;
        }

        public static List<ItemData> ToItemGridList(List<PbItem> itemList)
        {
            List<ItemData> itemDataList = new List<ItemData>();
            for (int i = 0; i < itemList.Count; i++)
            {
                ItemData itemData = new ItemData((int)itemList[i].item_id);
                itemData.StackCount= (int) itemList[i].item_count;
                itemDataList.Add(itemData);
            }
            return itemDataList;
        }

        public static List<BaseItemData> ToBaseItemDataList(List<PbItem> itemList)
        {
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            for (int i = 0; i < itemList.Count; i++)
            {
                BaseItemData baseItemData = ItemDataCreator.Create((int)itemList[i].item_id);
                baseItemData.StackCount = (int)itemList[i].item_count;
                baseItemDataList.Add(baseItemData);
            }
            return baseItemDataList;
        }

        public static List<RewardData> ToRewardDataList(List<PbItem> itemList)
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < itemList.Count; i++)
            {
                RewardData rewardData = RewardData.GetRewardData((int) itemList[i].item_id, (int) itemList[i].item_count);
                rewardDataList.Add(rewardData);
            }
            return rewardDataList;
        }


        public static bool IsEqual(PbItem pbItem, ItemData itemData)
        {
            return ((pbItem.item_id == itemData.Id) && (pbItem.item_count == itemData.StackCount));
        }
    }
}
