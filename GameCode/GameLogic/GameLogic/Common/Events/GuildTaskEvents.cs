﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class GuildTaskEvents
    {
        public const string ON_GET_GUILD_TASK_LIST = "GuildTaskEvents_ON_GET_GUILD_TASK_LIST";

        public const string ON_GET_GUILD_TASK_SCORE = "GuildTaskEvents_ON_GET_GUILD_TASK_SCORE";

        public const string ON_GET_GUILD_TASK_RANK = "GuildTaskEvents_ON_GET_GUILD_TASK_RANK";
    }
}
