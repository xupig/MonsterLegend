﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 20:33:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class PetEvents
    {
        public const string UpdatePetInfo = "PetEvent_UpdatePetInfo";
        public const string LevelUp = "PetEvent_LevelUp";
        public const string StarUp = "PetEvent_StarUp";
        public const string QualityUp = "PetEvent_QualityUp";
        public const string OnPetGetPanelClose = "PetEvent_OnPetGetPanelClose";
        public const string IdleToFight = "PetEvent_IdleToFight";
        public const string ChangeStrengthPanelTab = "PetEvent_ChangeStrengthPanelTab";

        public const string GetPanelShowPetModel = "PetEvents_GetPanelShowPetModel";

        public const string RequestIdleToAssist = "PetEvent_RequestIdleToAssist";

        public const string AddExp = "PetEvent_AddExp";
        public const string ShowWashPossibleSkill = "PetEvent_ShowWashPossibleSkill";
        public const string ResponsePetWash = "PetEvent_ResponsePetWash";
        public const string ResponsePetWashConfirm = "PetEvent_ResponsePetWashConfirm";
        public const string RefreshLockHole = "PetEvent_RefreshLockHole";

        public const string ReceiveIdleToFight = "PetEvent_ReceiveIdleToFight";
        public const string ReceiveFightToIdle = "PetEvent_ReceiveFightToIdle";
    }
}
