﻿#region 模块信息
/*==========================================
// 文件名：TaskEvent
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 9:40:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TaskEvent
    {
        #region 服务器事件
        /// <summary>
        /// 刷新主线任务列表
        /// </summary>
        public const string TASK_MAIN_TASK_CHANGE = "TaskEvent_TASK_MAIN_TASK_CHANGE";
        public const string TASK_PROGRESS_CHANGE = "TaskEvent_TASK_PROGRESS_CHANGE";

        /// <summary>
        /// 刷新支线任务列表
        /// </summary>
        public const string TASK_BRANCH_TASK_CHANGE = "TaskEvent_TASK_BRANCH_TASK_CHANGE";

        public const string TASK_FINISH = "TaskEvent_TASK_FINISH";

        public const string TASK_ACCEPTED = "TaskEvent_TASK_ACCEPTED";

        public const string TASK_REACHED = "TaskEvent_TASK_REACHED";

        //public const string MIN_LEVEL_CHANGE = "TaskEvent_MIN_LEVEL_CHANGE";

        public const string REMAIN_TIMES_CHANGE = "TaskEvent_REMAIN_TIMES_CHANGE";

        public const string WEEK_TIMES_CHANGE = "TaskEvent_WEEK_TIMES_CHANGE";

        public const string RING_CHANGE = "TaskEvent_RING_CHANGE";

        public const string DAY_REWARD_CHANGE = "TaskEvent_DAY_REWARD_CHANGE";

        public const string WEEK_REWARD_CHANGE = "TaskEvent_WEEK_REWARD_CHANGE";

        public const string TEAM_TASK_STATE_CHANGE = "TaskEvent_TEAM_TASK_STATE_CHANGE";


        #endregion

        #region 客户端事件

        /// <summary>
        /// NPC头顶任务状态变化
        /// param:npcId(int),taskState(int)
        /// </summary>
        public const string NPC_TASK_STATE_CHANGE = "TaskEvent_NPC_TASK_STATE_CHANGE";

        public const string TALK_TO_NPC = "TaskEvent_TALK_TO_NPC";

        public const string FIND_MONSTER = "TaskEvent_FIND_MONSTER";

        #endregion

        public const string CREATE_TEAM_TASK_ENTERANCE = "TaskEvent_CREATE_TEAM_TASK_ENTERANCE";

        public const string DESTROY_TEAM_TASK_ENTERANCE = "TaskEvent_DESTROY_TEAM_TASK_ENTERANCE";
    }
}
