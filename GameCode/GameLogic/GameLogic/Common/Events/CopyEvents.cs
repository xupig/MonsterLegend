﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/13 14:58:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class CopyEvents
    {
        public const string GotStarRewardListChange = "CopyEvent_GotStarRewardChange";
        public const string Result = "CopyEvent_Result";
        public const string UpdatePreview = "CopyEvent_UpdatePreview";

        public const string SHOW_COPY = "CopyEvent_SHOW_COPY";

        public const string FieldShowAllLine = "CopyEvent_FieldShowAllLine";

        public const string ON_MATCH_MISSION_CHANGE = "CopyEvent_ON_MATCH_MISSION_CHANGE";

        public const string SHOW_CONFIRM = "CopyEvent_Show_Confirm";

        public const string GET_BOX_REWARD = "CopyEvent_GET_BOX_REWARD";

        public const string PLAY_AGAIN_AGREE = "CopyEvent_PLAY_AGAIN_AGREE";

        public const string TRY_QUIT_COPY = "CopyEvent_TRY_QUIT_COPY";
    }
}
