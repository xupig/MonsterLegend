﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class RewardBuffEvents
    {
        public const string ShowRewardBuffIcon = "RewardBuffEvents_ShowRewardBuffIcon";

        public const string HideRewardBuffIcon = "RewardBuffEvents_HideRewardBuffIcon";

        public const string ShowRewardBuffTip = "RewardBuffEvents_ShowRewardBuffTip";

        public const string RefreshRewardBuffList = "RewardBuffEvents_RefreshRewardBuffList";
    }
}
