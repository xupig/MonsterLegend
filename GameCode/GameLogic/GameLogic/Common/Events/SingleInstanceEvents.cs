﻿#region 模块信息
/*==========================================
// 文件名：SingleInstanceEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/20 16:43:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class SingleInstanceEvents
    {
        public const string BEGIN_REFRESH = "SingleInstanceEvents_BEGIN_REFRESH";

        public const string REFRESH_CHPATER_COUNTS = "SingleInstanceEvents_REFRESH_CHPATER_COUNTS";

        public const string OPEN_SWEEP_PANEL = "SingleInstanceEvents_OPEN_SWEEP_PANEL";

        public const string REFRESH_BUY_COUNTS = "SingleInstanceEvents_REFRESH_BUY_COUNTS";
    }
}