﻿#region 模块信息
/*==========================================
// 文件名：EquipUpgradeEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/27 20:51:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class EquipUpgradeTipEvents
    {
        public const string Bag_Item_Update = "EquipUpgradeEvents_Bag_Item_Update";

        public const string Show_Equip = "EquipUpgradeEvents_Show_Equip";

        public const string Bag_Item_Update_All = "EquipUpgradeEvents_Show_Equip_ALL";

        public const string Hide_Equip = "EquipUpgradeEvents_Hide_Equip";

        public const string Close_Or_Use_Item = "EquipUpgradeEvents_Close_Or_Use_Item";

        public const string Remove_Same_Item = "EquipUpgradeEvents_Remove_Same_Item";

        public const string Add_Item_To_First = "EquipUpgradeEvents_Add_Item_To_First";

        public const string Body_Item_Update = "EquipUpgradeEvents_Body_Item_Update";
    }
}