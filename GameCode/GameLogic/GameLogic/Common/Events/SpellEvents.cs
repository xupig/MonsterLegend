﻿#region 模块信息
/*==========================================
// 文件名：SpellEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/27 10:29:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class SpellEvents
    {
        public const string SPELL_CHANGE = "SpellEvents_SPELL_CHANGE";
        public const string SPELL_LIST_INIT = "SpellEvents_SPELL_LIST_INIT";
        public const string SPELL_SLOT_LIST_CHANGE = "SpellEvents_SPELL_SLOT_LIST_CHANGE";
        //public const string SPELL_PROFICIENT_CHANGE = "SpellEvents_SPELL_PROFICIENT_CHANGE";
        public const string SPELL_UPDATE_EQUIPED_SKILL = "SpellEvents_SPELL_UPDATE_EQUIPED_SKILL";


        public const string SPELL_CG_PLAY = "SpellEvents_SPELL_CG_PLAY";

        public const string SPELL_CG_END = "SpellEvents_SPELL_CG_END";

        public const string SPELL_OPERATION_CHANGE = "SpellEvents_SPELL_OPERATION_CHANGE";

        public const string SPELL_CAN_UPGRADE = "SpellEvents_SPELL_CAN_UPGRADE";

    }
}
