﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class PlayerEvents
    {
        public const string ON_DEATH = "ON_DEATH";//player死亡

        public const string ON_RELIVE = "ON_RELIVE";//player复活

        public const string ON_FIGHT_TYPE_CHANGED = "ON_CHANGE_FIGHT_TYPE";

        public const string ON_IN_TOUCH_MODE_STATE_CHANGED = "ON_IN_TOUCH_MODE_STATE_CHANGED";
    }
}
