﻿

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/11 10:00:33 
 * function: 
 * *******************************************************/

namespace Common.Events
{
    public class CGEvent
    {
        public const string HIDE_DIALOG = "CGEvent_HIDE_DIALOG";        //关闭对话
        public const string HIDE_SKIP = "CGEvent_SHOW_SKIP";            //隐藏跳过按钮
        public const string HIDE_BLACK_EDGE = "CGEvent_HIDE_BLACK_EDGE";            //隐藏黑边
        public const string HIDE_BOSS_INTRODUCE = "CGEvent_HIDE_BOSS_INTRODUCE";    //关闭boss介绍
        public const string SHOW_CG_SPEECH = "CGEvent_SHOW_CG_SPEECH";
        public const string ON_VIEWING_MODE_CHANGED = "CGEvent_ON_VIEWING_MODE_CHANGED"; //CG观影状态改变
    }
}
