﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class GuildShopEvents
    {
        public const string Show_List_View = "GuildShopEvents_Show_List_View";

        public const string Refresh_Praise_Count = "GuildShopEvents_Refresh_Praise_Count";

        public const string Refresh_Exchange = "GuildShopEvents_Refresh_Exchange";

        public const string Refresh_Praise = "GuildShopEvents_Refresh_Praise";

        public const string Refresh_Item = "GuildShopEvents_Refresh_Item";

    }
}
