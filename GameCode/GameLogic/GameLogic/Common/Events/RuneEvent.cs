﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/23 14:08:58
 * 描述说明：
 * *******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class RuneEvent
    {
        #region 服务器事件
        /// <summary>
        /// 符文背包单个物品变化
        /// </summary>
        public const string RUNE_BAG_ITEM_CHANGE = "RuneEvent_RUNE_BAG_ITEM_CHANGE";

        /// <summary>
        /// 符文身体背包单个物品变化
        /// </summary>
        //public const string RUNE_BODY_BAG_ITEM_CHANGE = "RuneEvent_RUNE_BODY_BAG_ITEM_CHANGE";

        /// <summary>
        /// 许愿信息变化
        /// </summary>
        public const string WISH_INFO_CHANGE = "RuneEvent_WISH_INFO_CHANGE";

        /// <summary>
        /// 符文背包变化
        /// </summary>
        public const string RUNE_BAG_ALL_CHANGE = "RuneEvent_RUNE_BAG_ALL_CHANGE";

        /// <summary>
        /// 符文身体背包变化
        /// </summary>
        public const string RUNE_BODY_BAG_ALL_CHANGE = "RuneEvent_RUNE_BODY_BAG_ALL_CHANGE";

        /// <summary>
        /// 更新符文升级面板
        /// </summary>
        public const string RUNE_LEVEL_UP_VIEW_UPDATE = "RuneEvent_RUNE_LEVEL_UP_VIEW_UPDATE";

        public const string RUNE_PUT_ON = "RuneEvent_RUNE_PUT_ON";

        #endregion

        #region 客户端事件

        /// <summary>
        /// 把符文放到符文升级界面上
        /// param:list(List<ItemInfo>)
        /// </summary>
        public const string RUNE_LEVEL_UP_PUT_ON = "RuneEvent_RUNE_LEVEL_UP_PUT_ON";

        public const string ADD_RUNE_ITEM = "RuneEvent_ADD_RUNE_ITEM";

        public const string UPDATE_CAN_EQUIPED_LIST = "RuneEvent_UPDATE_CAN_EQUIPED_LIST";

        #endregion

    }
}
