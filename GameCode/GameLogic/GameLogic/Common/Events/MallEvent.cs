﻿#region 模块信息
/*==========================================
// 文件名：MallEvent
// 创建者：陈铨
// 创建日期：2015/3/12 16:35:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class MallEvent
    {
        public const string RefreshMallItemGrid = "MallEvent_RefreshMallItemGrid";
        public const string SendFriend = "MallEvent_SendFriend";
        public const string SelectCoupon = "MallEvent_SelectCoupon";
        public const string BuySuccess = "MallEvent_BuySuccess";
        public const string TOKEN_ITEM_REFRESH = "MallEvent_TOKEN_ITEM_REFRESH";
    }
}
