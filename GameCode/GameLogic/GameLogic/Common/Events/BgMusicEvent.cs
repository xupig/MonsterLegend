﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class BgMusicEvent
    {
        public const string CHANGE_MUSIC_VOLUME = "changeMusicVolume";
        public const string CHANGE_SOUND_VOLUME = "changeSoundVolume";

    }
}
