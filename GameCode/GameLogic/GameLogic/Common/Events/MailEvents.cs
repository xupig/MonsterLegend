﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class MailEvents
    {
        /// <summary>
        /// 刷新邮件列表
        /// </summary>
        public const string REFRESH_MAIL_LIST = "MailEvents_Refresh_Mail_List";

        /// <summary>
        /// 显示邮件内容
        /// </summary>
        public const string DISPLAY_MAIL_CONTENT = "MailEvents_Display_Mail_Content";

        /// <summary>
        /// 提取单个邮件附件
        /// </summary>
        public const string DRAW_MAIL_ATTACHMENT = "MailEvents_Draw_Mail_Attachment";

        /// <summary>
        /// 删除邮件
        /// </summary>
        public const string DELETE_MAIL = "MailEvents_Delete_Mail";

        public const string BATCH_DELETE_MAIL = "MailEvents_Batch_Delete_Mail";
        /// <summary>
        /// 点击添加附件事件
        /// </summary>
        public const string SEND_MAIL_SUCCESS = "MailEvents_Send_Mail_Success";

        public const string MAIL_ADD_FRIEND = "MailEvents_add_friend";

        public const string TWEEN_RIGHT_BG = "MailEvents_Tween_right_bg";

        public const string TWEEN_LEFT_BG = "MailEvents_Tween_left_bg";

        public const string RESET_OUTBOX_VIEW = "MailEvents_Reset_Outbox_View";

        public const string MAIL_DATA_CHANGE = "MailEvents_MAIL_DATA_CHANGE";

        public const string MAIL_POINT_REFRESH = "MailEvents_MAIL_POINT_REFRESH";

        public const string MAIL_CLOSE_POPUP_VIEW = "MailEvents_Mail_CLOSE_POPUP_VIEW";
    }
}
