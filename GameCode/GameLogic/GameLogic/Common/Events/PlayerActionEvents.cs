﻿
namespace Common.Events
{
    public class PlayerActionEvents
    {
        public const string ON_START_FIND_MONSTER = "ON_START_FIND_MONSTER";
        public const string ON_FAIL_FIND_MONSTER = "ON_FAIL_FIND_MONSTER";
        public const string ON_END_FIND_MONSTER = "ON_END_FIND_MONSTER";
    }
}
