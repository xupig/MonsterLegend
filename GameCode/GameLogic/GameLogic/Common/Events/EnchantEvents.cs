﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class EnchantEvents
    {
        public const string ReplaceAttribute = "Enchant.ReplaceAttribute";

        public const string RefreshAttribute = "Enchant.RefreshAttribute";

        public const string Init = "Enchant.Init";

        public const string UpdateEnchantByBody = "Enchant.Update";

        public const string UpdateLuckyValue = "Enchant.UpdateLuckyValue";
    }
}
