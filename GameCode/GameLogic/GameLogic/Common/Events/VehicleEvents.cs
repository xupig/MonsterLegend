﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public static class VehicleEvents
    {
        /// <summary>
        /// 上载具
        /// </summary>
        public static string DRIVER_VEHICLE = "VehicleEvents_driverVehicle";

        /// <summary>
        /// 下载具
        /// </summary>
        public static string DIVORCE_VEHICLE = "VehicleEvents_DivorceVehicle";

        /// <summary>
        /// 载具攻击失败
        /// </summary>
        public static string VEHICLE_ATTACK_FAIL = "VehicleEvents_AttackFail";

        /// <summary>
        /// 请求下载具
        /// </summary>                                                        
        public static string ONREQUEST_DIVORCE_VEHICLE = "VehicleEvents_requestDivorce";

        /// <summary>
        /// 切换场景
        /// </summary>
        public static string CHANGE_SCENE = "ChangeScene";  

    }
}
