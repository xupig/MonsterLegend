﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FavorEvents
    {
        /// <summary>
        /// 更新单个NPC好感度信息
        /// </summary>
        public static string UpdateNpcFavor = "FavorEvents_UpdateNpcFavor";
    }
}
