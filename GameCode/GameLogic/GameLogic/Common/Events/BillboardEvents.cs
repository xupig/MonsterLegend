﻿using System;

public class BillboardEvents
{
    /// <summary>
    /// 屏幕坐标：Vector2，血量：int,类型：SplitBattleBillboardType
    /// </summary>
    public const String AddSplitBillboard = "BillboardViewManager_T_ShowBillboard";
}

public enum SplitBattleBillboardType
{
    CriticalMonster = 0,
    CriticalPlayer = 1,
    BrokenAttack = 2,
    NormalPlayer = 3,
    NormalMonster = 4,
    Miss = 5
}
