﻿#region 模块信息
/*==========================================
// 文件名：MakeEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/31 14:51:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class MakeEvents
    {
        public const string FORMULA_BAG_CHANGE = "MakeEvents_FORMULA_BAG_CHANGE";

        public const string NEW_FORMULA_CHANGE = "MakeEvents_NEW_FORMULA_CHANGE";

        public const string TRAIN_SUCCESS = "MakeEvents_TRAIN_SUCCESS";

        public const string REFREASH_MENU_STATE = "MakeEvents_REFREASH_MENU_STATE";

        public const string HIDE_MAKE_TAB_POINT = "MakeEvents_HIDE_MAKE_TAB_POINT";

        public const string HIDE_TRAIN_TAB_POINT = "MakeEvents_HIDE_TRAIN_TAB_POINT";

        public const string HIDE_CATEGORY_POINT = "MakeEvents_HIDE_CATEGORY_POINT";

        public const string FORMULA_MAKE_SUCCESS = "MakeEvents_FORMULA_MAKE_SUCCESS";

        public const string FORMULA_MENU_REFRESH = "MakeEvents_FORMULA_MENU_REFRESH";

        public const string REFRESH_CONTENT_STATE = "MakeEvents_REFRESH_CONTENT_STATE";
    }
}
