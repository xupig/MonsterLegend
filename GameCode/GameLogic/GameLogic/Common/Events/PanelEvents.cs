﻿#region 模块信息
/*==========================================
// 文件名：PanelEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/22 17:09:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class PanelEvents
    {
        public const string SHOW_PANEL = "PanelEvents_SHOW_PANEL";
        public const string CLOSE_PANEL = "PanelEvents_CLOSE_PANEL";
        public const string ON_PANEL_DATA_RESP = "PanelEvents_ON_PANEL_DATA_RESP";

        public const string PANEL_LAYER_RESET_POS = "PanelEvents_PANEL_LAYER_RESET_POS";

        public const string ENABLE_PANEL_CANVAS = "PanelEvents_ENABLE_PANEL_CANVAS";

    }
}
