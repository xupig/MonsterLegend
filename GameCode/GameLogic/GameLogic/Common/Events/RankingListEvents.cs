﻿#region 模块信息
/*==========================================
// 文件名：RankingListEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/17 10:21:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class RankingListEvents
    {
        public const string GET_RANKLIST_DATA = "RankingListEvents_GET_RANKLIST_DATA";

        public const string GET_MY_RANKING = "RankingListEvents_GET_MY_RANKING";

        public const string GET_RANK_PLAYER_GENERAL_INFO = "RankingListEvents_GET_RANK_GENERAL_INFO";

        public const string GET_RANK_PLAYER_FACADE_INFO = "RankingListEvents_GET_RANK_PLAYER_FACADE_INFO";

        public const string GET_RANK_PLAYER_GUILD_INFO = "RankingListEvents_GET_RANK_PLAYER_GUILD_INFO";

        public const string GET_RANK_PLAYER_ATTRIBUTE_INFO = "RankingListEvents_GET_RANK_PLAYER_ATTRIBUTE_INFO";

        public const string GET_RANK_PLAYER_RUNE_INFO = "RankingListEvents_GET_RANK_PLAYER_RUNE_INFO";

        public const string GET_RANK_PLAYER_GEM_INFO = "RankingListEvents_GET_RANK_PLAYER_GEM_INFO";

        public const string GET_RANK_PLAYER_EQUIP_INFO = "RankingListEvents_GET_RANK_PLAYER_EQUIP_INFO";

        public const string GET_RANK_PLAYER_FIGHT_FORCE_INFO = "RankingListEvents_GET_RANK_PLAYER_FIGHT_FORCE_INFO";

        public const string GET_RANK_WORSHIP_RESULT = "RankingListEvents_GET_RANK_WORSHIP_RESULT";

        public const string GET_GUILD_RANK = "RankingListEvents_GET_GUILD_RANK";

        public const string GET_MY_AWARD_SUCCESSFUL = "RankingListEvents_GET_MY_AWARD_SUCCESSFUL";

        public const string GET_GUILD_RANK_IN_CHAT = "RankingListEvents_GET_GUILD_RANK_IN_CHAT";

        public const string GET_DEMON_RANK_DATA = "RankingListEvents_GET_DEMON_RANK_DATA";

        public const string GET_RANK_PLAYER_WING_DATA = "RankingListEvents_GET_RANK_PLAYER_WIN_DATA";

        public const string GET_RANK_PLAYER_PK_DATA = "RankingListEvents_GET_RANK_PLAYER_PK_DATA";

        public const string GET_RANK_PLAYER_GUILD_DATA = "RankingListEvents_GET_RANK_PLAYER_GUILD_DATA";

        // 客户端事件
        public const string REFRESH_ONE_PLAYER_WORSHIP_DATA = "RankingListEvents_REFRESH_ONE_PLAYER_WORSHIP_DATA";

        public const string OPEN_RANK_VIEW = "OPEN_RANK_VIEW";

        public const string OPEN_AWARD_VIEW = "OPEN_AWARD_VIEW";
    }
}