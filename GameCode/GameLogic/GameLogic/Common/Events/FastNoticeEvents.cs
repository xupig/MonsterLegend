﻿#region 模块信息
/*==========================================
// 文件名：FastNoticeEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/9/14 9:49:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FastNoticeEvents
    {
        public const string REMOVE_FAST_NOTICE = "FastNoticeEvents_REMOVE_FAST_NOTICE";
        public const string REMOVE_FAST_NOTICE_BY_VIEWID = "FastNoticeEvents_REMOVE_FAST_NOTICE_BY_VIEWID";
        public const string HIDE = "FastNoticeEvents_HIDE";
    }
}
