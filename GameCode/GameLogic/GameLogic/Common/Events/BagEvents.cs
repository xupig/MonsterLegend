﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class BagEvents
    {
        /// <summary>
        /// 更新整个类型背包数据时触发，参数：背包类型
        /// </summary>
        public const string Init = "BagEvents_Init";

        /// <summary>
        /// 更新类型背包数据时触发，参数：背包类型，格子编号，物品信息
        /// </summary>
        public const string Update = "BagEvents_UpdateSingleItem";


        public const string AddItem = "BagEvents_AddItem";

        public const string UseItem = "BagEvents_UseItem";

        public const string SellItem = "BagEvents_SellItem";

        public const string DelItem = "BagEvents_DelItem";

        public const string UseItemSuccess = "BagEvents_UseItemSuccess";

        public const string UseItemFail = "BagEvents_UseItemFail";
    }
}
