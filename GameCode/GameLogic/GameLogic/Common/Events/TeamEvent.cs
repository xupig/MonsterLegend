﻿#region 模块信息
/*==========================================
// 文件名：TeamEvent
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/6 17:27:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TeamEvent
    {
        #region 服务器事件
        /// <summary>
        /// 刷新自己的队伍信息
        /// </summary>
        public static string TEAM_REFRESH = "TeamEvent_TEAM_REFRESH";

        /// <summary>
        /// 刷新队员HP信息
        /// </summary>
        public static string TEAM_MATE_HP_REFRESH = "TeamEvent_TEAM_MATE_HP_REFRESH";

        /// <summary>
        /// 刷新队员在线信息
        /// </summary>
        public static string TEAM_MATE_ONLINE_REFRESH = "TeamEvent_TEAM_MATE_ONLINE_REFRESH";

        /// <summary>
        /// 申请、邀请列表信息刷新
        /// </summary>
        public static string TEAM_RECEIVED_LIST_CHANGE = "TeamEvent_TEAM_RECEIVED_LIST_CHANGE";

        /// <summary>
        /// 附近玩家列表刷新
        /// </summary>
        public static string TEAM_NEAR_PLAYER_LIST_CHANGE = "TeamEvent_TEAM_NEAR_PLAYER_LIST_CHANGE";

        /// <summary>
        /// 附近队伍列表刷新
        /// </summary>
        public static string TEAM_NEAR_TEAM_LIST_CHANGE = "TeamEvent_TEAM_NEAR_TEAM_LIST_CHANGE";

        /// <summary>
        /// 邀请返回
        /// </summary>
        public static string TEAM_INVITE_RESP = "TeamEvent_TEAM_INVITE_RESP";

        /// <summary>
        /// 申请返回
        /// </summary>
        public static string TEAM_APPLY_RESP = "TeamEvent_TEAM_APPLY_RESP";

        //离开队伍
        public static string TEAM_QUIT = "TeamEvent_TEAM_QUIT";


        public static string TEAM_MEMBER_REPLY = "TeamEvent_TEAM_MEMBER_REPLY";

        public static string TEAM_APPLY_INVITE_INFO = "TeamEvent_TEAM_APPLY_INVITE_INFO";

        public static string TEAM_APPLY_NOTICE = "TeamEvent_TEAM_APPLY_NOTICE";

        public static string TEAM_INVITE_NOTICE = "TeamEvent_TEAM_INVITE_NOTICE";

        public static string TEAM_POINT_REFRESH = "TeamEvent_TEAM_POINT_REFRESH";

        public static string TEAM_ID_CHANGED = "TeamEvent_TEAM_ID_CHANGED";

        public static string TEAM_GUIDE_FLAG_CHANGED = "TeamEvent_TEAM_GUIDE_FLAG_CHANGED";

        public static string TEAM_RECEIVE_CHAT_CONTENT = "TeamEvent_TEAM_RECEIVE_CHAT_CONTENT";

        public static string TEAM_AUTO_TEAM_LIST_REFRESH = "TeamEvent_TEAM_AUTO_TEAM_LIST_REFRESH";

        public static string TEAM_AUTO_TEAM_TAKE_INFO_REFRESH = "TeamEvent_TEAM_AUTO_TEAM_TAKE_INFO_REFRESH";

        public static string TEAM_AUTO_TEAM_MATCH_INFO_REFRESH = "TeamEvent_TEAM_AUTO_TEAM_MATCH_INFO_REFRESH";

        public static string CAPTAIN_MATCH_INFO_REFRESH = "TeamEvent_CAPTAIN_MATCH_INFO_REFRESH";

        public static string TRANSFER_INFO_CHANGE = "TeamEvent_TRANSFER_INFO_CHANGE";

        public static string TRANSFER_APPLY = "TeamEvent_TRANSFER_APPLY";

        public static string REQUEST_APPLY_FAIL = "TeamEvent_REQUEST_APPLY_FAIL";

        #endregion

        #region 客户端UI事件
        //public static string TEAM_KICK_TEAMMATE = "TeamEvent_TEAM_KICK_TEAMMATE";

        //public static string TEAM_ADD_FRIEND = "TeamEvent_TEAM_ADD_FRIEND";

        //public static string TEAM_CHANGE_CAPTAIN = "TeamEvent_TEAM_CHANGE_CAPTAIN";

        //public static string TEAM_TRANSMIT = "TeamEvent_TEAM_TRANSMIT";

        //public static string TEAM_CHAT = "TeamEvent_TEAM_CHAT";

        //public static string TEAM_CALL = "TeamEvent_TEAM_CALL";
        #endregion


    }
}
