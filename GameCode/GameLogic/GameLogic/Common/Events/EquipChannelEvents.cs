﻿#region 模块信息
/*==========================================
// 文件名：EquipChannelEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/29 16:38:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class EquipChannelEvents
    {
        public const string CloseAllPreviousPanel = "EquipChannelEvents_CloseAllPreviousPanel";
    }
}