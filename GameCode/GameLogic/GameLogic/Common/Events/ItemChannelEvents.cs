﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/31 10:24:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class ItemChannelEvents
    {
        public const string Open_Item_Channel_Tips = "Item_Channel_Events_Open_Panel";
        
        public const string Close_Item_Channel_Tips = "Item_Channel_Events_Close_Panel";

        public const string Go_Item_Get_Panel = "Item_Channel_Events_Go_Panel";
    }
}