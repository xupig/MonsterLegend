﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Events
{
    public class TradeMarketEvents
    {
        public const string UpdateData = "TradeMarketEvents_UpdataData";
        
        //与显示相关的事件
        public const string OpenPutOutPanel = "TradeMarketEvents_OpenPutawayPanel";

        /// <summary>
        /// 刷新寄售列表
        /// </summary>
        public const string RefreshSellView = "TradeMarketEvents_RefreshSellView";

        /// <summary>
        /// 展示筛选条件
        /// </summary>
        public const string ShowConditionView = "TradeMarketEvents_ShowConditionView";

        /// <summary>
        /// 更新单个上架物品信息
        /// </summary>
        public const string UpdateSellGrid = "TradeMarketEvents_UpdateSellGrid";

        /// <summary>
        /// 打开重新上架编辑面板
        /// </summary>
        public const string OpenPutOutPopupView = "TradeMarketEvents_OpenPutawayPopupView";

        /// <summary>
        /// 刷新购买列表界面
        /// </summary>
        public const string RefreshBuyView = "TradeMarketEvents_RefreshBuyView";

        /// <summary>
        /// 物品下架
        /// </summary>
        public const string CancelSellItem = "TradeMarketEvents_CancelSellItem";

        /// <summary>
        /// 弹出物品购买界面
        /// </summary>
        public const string ShowBuyView = "TradeMarketEvents_ShowBuyView";

        /// <summary>
        /// 更新拍卖行全局信息
        /// </summary>
        public const string UpdateGlobalInfo = "TradeMarketEvents_UpdateGlobalInfo";

        /// <summary>
        /// 更新可购买的单个物品信息
        /// </summary>
        public const string UpdateBuyItem = "TradeMarketEvents_UpdateBuyItem";

        /// <summary>
        /// 更新全部求购信息
        /// </summary>
        public const string UpdateWantBuyList = "TradeMarketEvents_UpdateWantBuyList";

        /// <summary>
        /// 提取拍卖行收益
        /// </summary>
        public const string RefreshLeftMoney = "TradeMarketEvents_TakeOutMoneySuccess";

        /// <summary>
        /// 关闭物品上架view
        /// </summary>
        public const string ClosePutOutPanel = "TradeMarketEvents_ClosePutOutPanel";

        /// <summary>
        /// 更新单个物品的出售信息
        /// </summary>
        public const string UpSellItemInfo = "TradeMarketEvents_UpSellItemInfo";

        /// <summary>
        /// 更新单个物品的购买信息
        /// </summary>
        public const string UpdateBuyItemInfo = "TradeMarketEvents_UpBuyItemInfo";

        /// <summary>
        /// 更新解锁格子数
        /// </summary>
        public const string UpdateUnlockGrid = "TradeMarketEvents_UpdateUnlockGrid";

        /// <summary>
        /// 求购列表刷新
        /// </summary>
        public const string UpdateWantBuyItem = "TradeMarketEvents_UpdateWantBuyItem";

        /// <summary>
        /// 请求发送筛选
        /// </summary>
        public const string SendSearch = "TradeMarketEvents_SendSearch";

        /// <summary>
        /// 拍卖行通知信息改变
        /// </summary>
        public const string NoticeCountChanged = "TradeMarketEvents_NoticeCountChanged";

        /// <summary>
        /// 提现
        /// </summary>
        public const string WithdrawCash = "TradeMarketEvents_WithdrawCash";

        /// <summary>
        /// 刷新购买记录
        /// </summary>
        public const string RefreshBuyRecord = "TradeMarketEvents_RefreshBuyRecord";

        /// <summary>
        /// 刷新售出记录
        /// </summary>
        public const string RefreshSellRecord = "TradeMarketEvents_RefreshSellRecord";
    }
}

