﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class RewardEvents
    {
        public const string UPDATE_DAILY_TASK = "RewardEvents_UPDATE_DAILY_TASK";
        //参数为新增已接任务的数量
        public const string HAVE_NEW_ACCEPT_DAILY_TASK = "RewardEvents_HAVE_NEW_ACCEPT_DAILY_TASK";
        public const string GET_DAILY_TASK_REWARD = "RewardEvents_GET_DAILY_TASK_REWARD";
        public const string UPDATE_CARD_INFO = "RewardEvents_UPDATE_CARD_INFO";
        public const string UPDATE_DAILY_TASK_TIME_CHANGED = "RewardEvents_UPDATE_DAILY_TASK_TIME_CHANGED";

        public const string UPDATE_MONTH_LOGIN = "RewardEvents_UPDATE_MONTH_LOGIN";

        public const string UPDATE_ACTIVE_TASK = "RewardEvents_UPDATE_ACTIVE_TASK";
        public const string UPDATE_ACTIVE_BOX_INFO = "RewardEvents_UPDATE_ACTIVE_BOX_INFO";
        //参数为新增已接活跃任务的数量
        public const string HAVE_NEW_ACCEPT_ACTIVE_TASK = "RewardEvents_HAVE_NEW_ACCEPT_ACTIVE_TASK";
        public const string GET_ACTIVE_TASK_REWARD = "RewardEvents_GET_ACTIVE_TASK_REWARD";
        public const string UPDATE_ACTIVE_BOX_REWARD_LV_INFOS = "RewardEvents_UPDATE_ACTIVE_BOX_REWARD_LV_INFOS";
        public const string GET_ACTIVE_BOX_VIP_REWARD = "RewardEvents_GET_ACTIVE_BOX_VIP_REWARD";

        public const string UPDATE_VIP_GIFT = "RewardEvents_UPDATE_VIP_GIFT";
        //参数为领取的vip等级信息的列表
        public const string GET_VIP_GIFT = "RewardEvents_GET_VIP_GIFT";

        public const string GET_SERVER_OPENED_LOGIN_REWARD = "RewardEvents_GET_SERVER_OPENED_LOGIN_REWARD";
    }
}
