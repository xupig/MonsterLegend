﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TimeAltarEvents
    {
        public const string InitTimeAltar = "TimeAltarEvents_InitTimeAltar";

        public const string UpdateTimeAltar = "TimeAltarEvents_UpdateTimeAltar";

        public const string NoticeChanged = "TimeAltarEvents_NoticeChanged";
    }
}
