﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    class CloudEvents
    {
        public const string CloseCloudPanel = "CloudEvents_CloseCloudPanel";

        public const string CloseSceneChangePanel = "CloudEvents_CloseSceneChangePanel";
    }
}
