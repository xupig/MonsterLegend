﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class SpaceActionEvents
    {
        public const string SpaceActionEvents_Action = "SpaceActionEvents_Action";
        public const string SpaceActionEvents_MonsterDie = "SpaceActionEvents_MonsterDie";
        public const string SpaceActionEvents_Lose = "SpaceActionEvents_Lose";
        public const string SpaceActionEvents_Cond = "SpaceActionEvents_Cond";
        public const string SpaceActionEvents_ChangeTimer = "SpaceActionEvents_ChangeTimer";
        public const string SpaceActionEvents_PlayerDeath = "SpaceActionEvents_PlayerDeath";
        public const string SpaceActionEvents_Complete = "SpaceActionEvents_Complete";
        public const string SpaceActionEvents_PlayEffect = "SpaceActionEvents_PlayEffect";
        public const string SpaceActionEvents_AutoMove_Point = "SpaceActionEvents_AutoMove_Point";
        public const string SpaceActionEvents_Change_Boss_Part = "SpaceActionEvents_Change_Boss_Part";
        public const string SpaceActionEvents_Boss_Part_Hp_Changed = "SpaceActionEvents_Boss_Part_Hp_Changed";
        public const string SpaceActionEvents_Boss_Part_Selected_Changed = "SpaceActionEvents_Boss_Part_Selected_Changed";
        public const string SpaceActionEvents_Monsters_HP_Change = "SpaceActionEvents_Monsters_HP_Change";
        public const string SpaceActionEvents_Collect_Action = "SpaceActionEvents_Collect_Action";
        public const string SpaceActionEvents_ReliveUseOut = "SpaceActionEvents_ReliveUseOut";
        public const string SpaceActionEvents_MissionEnd = "SpaceActionEvents_MissionEnd";
    }
}
