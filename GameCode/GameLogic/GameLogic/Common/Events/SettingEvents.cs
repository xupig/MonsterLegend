﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class SettingEvents
    {
        public const string INIT_SETTINGS = "SettingEvents_INIT_SETTINGS";
        //更新系统设置，参数为设置类型
        public const string UPDATE_SYSTEM_SETTING = "SettingEvents_UPDATE_SYSTEM_SETTING";
        //更新推送设置，参数为推送的Id
        public const string UPDATE_PUSH_SETTING = "SettingEvents_UPDATE_PUSH_SETTING";
        public const string APPLY_PUSH_SETTING_CHANGE = "SettingEvents_APPLY_PUSH_SETTING_CHANGE";
        public const string INIT_TASK_COMPLETED = "SettingEvents_INIT_TASK_COMPLETED";
        //质量设置更新，参数为质量类型
        public const string UPDATE_RENDER_QUALITY_SETTING = "SettingEvents_UPDATE_RENDER_QUALITY_SETTING";
    }
}
