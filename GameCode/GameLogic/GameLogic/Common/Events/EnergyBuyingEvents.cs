﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class EnergyBuyingEvents
    {
        public const string UPDATE_BUY_TIMES = "EnergyBuyingEvents_ENERGY_BUY_UPDATE_BUY_TIMES";
        public const string SHOW_BUY_ENERGY_CONFIRM_VIEW = "EnergyBuyingEvents_SHOW_BUY_ENERGY_CONFIRM_VIEW";
    }
}
