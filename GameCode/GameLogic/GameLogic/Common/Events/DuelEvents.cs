﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class DuelEvents
    {
        public const string RequestData = "DuelEvents_RequestData";

        public const string RefreshFightView = "DuelEvents_RefreshFightView";

        public const string RefreshEntryView = "DuelEvents_RefreshEntryView";

        public const string Minimize = "DuelEvents_Minimize";

        public const string Maximize = "DuelEvents_Maximize";

        public const string Stake = "DuelEvents_Stake";

        public const string RefreshStakeView = "DuelEvents_RefreshStakeView";

        public const string ResponseStake = "DuelEvents_ResponseStake";

        public const string RewardChanged = "DuelEvents_RewardChanged";

        public const string ResponseRecord = "DuelEvents_ResponseRecord";
    }
}
