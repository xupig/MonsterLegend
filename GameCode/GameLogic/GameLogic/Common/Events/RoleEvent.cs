﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 17:09:40
 * 描述说明：
 * *******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class RoleEvent
    {
        /// <summary>
        /// 切换分类
        /// </summary>
        public const string CATEGORY_CHANGE = "RoleEvent_CATEGORY_CHANGE";

        /// <summary>
        /// 鼠标/触摸经过
        /// </summary>
        public const string POINTER_ENTER_EQUIP = "RoleEvent_POINTER_ENTER_EQUIP";

        /// <summary>
        /// 鼠标、触摸离开
        /// </summary>
        public const string POINTER_EXIT_EQUIP = "RoleEvent_POINTER_EXIT_EQUIP";


        public const string PLAYER_LEVEL_UP = "RoleEvent_PLAYER_LEVEL_UP";
    }
}
