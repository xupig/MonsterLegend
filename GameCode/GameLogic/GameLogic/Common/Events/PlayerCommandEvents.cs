﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class PlayerCommandEvents
    {

        public const string MOVE_BY_STICK = "MOVE_BY_STICK";

        
        public const string FIND_NPC = "FIND_NPC"; //寻找NPC
        public const string FIND_POSITION = "FIND_POSITION"; //尋找位置
        public const string FIND_TRANSFER_POINT = "FIND_TRANSFER_POINT"; //尋找野外传送点
        public const string FIND_TREASURE = "FIND_TREASURE";    //寻找藏宝图宝藏
        public const string FIND_SHARE_POSITION = "FIND_SHARE_POSITION"; //尋找野外分享点
        public const string FIND_NPC_AND_ENTER_FB = "FIND_NPC_AND_ENTER_FB";
                
    }
}
