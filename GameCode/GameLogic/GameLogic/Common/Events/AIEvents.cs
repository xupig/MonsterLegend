﻿

namespace Common.Events
{
    public class AIEvents
    {
        public const string AIEvents_OnChangeAITarget = "AIEvents_OnChangeAITarget";
        public const string AIEvents_OnAIMove = "AIEvents_OnAIMove";
    }
}
