﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class ChargeEvents
    {
        public const string RefreshChargeInfoList = "ChargeEvents_RefreshChargeInfoList";

        public const string ChangeChargeTab = "ChargeEvents_ChangeChargeTab";

        public const string CheckChargeConditionOK = "ChargeEvents_CheckChargeConditionOK";
    }
}
