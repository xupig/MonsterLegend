﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class VipEvents
    {
        //VipCategoryView选择节点改变
        public const string ON_CATEGORY_INDEX_CHANGE = "VipEvents_ON_CATEGORY_INDEX_CHANGE";
        //拖动ScrollPage改变选择节点
        public const string ON_SCROLLPAGE_DRAG_INDEX_CHANGE = "VipEvents_ON_SCROLLPAGE_DRAG_INDEX_CHANGE";
        //接收到vip record消息
        public const string ON_RECEIVE_VIP_REWARD_RECORD = "VipEvents_ON_RECEIVE_VIP_REWARD_RECORD";
    }
}
