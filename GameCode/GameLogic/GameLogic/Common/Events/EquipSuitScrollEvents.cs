﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class EquipSuitScrollEvents
    {
        public const string Refresh = "EquipSuitScrollEvents_Refresh";
        public const string ShowEquipSuitScrollPanel = "UIManagerEvents.ShowEquipSuitScrollPanel";
        public const string HideEquipSuitScrollPanel = "UIManagerEvents.HideEquipSuitScrollPanel";
    }
}
