﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class MissionEvents
    {
        public const string UI_SELECT_CHAPTER = "MissionEvents_UI_SELECT_CHAPTER";

        /// <summary>
        ///  显示剧情副本
        /// </summary>
        /// param int 副本id
        public const string UI_SHOW_STORY_COPY_CHAPTER = "MissionEvents_UI_SHOW_STORY_COPY_CHAPTER";
        /// <summary>
        /// 关闭剧情副本
        /// </summary>
        public const string UI_HIDE_STORY_COPY_CHAPTER = "MissionEvents_UI_HIDE_STORY_COPY_CHAPTER";


        public const string GET_ALL_MISSTION_RECORD = "MissionEvents_GET_ALL_MISSTION_RECORD";
        public const string GET_ALL_INSTANCE_RECORD = "MissionEvents_GET_ALL_INSTANCE_RECORD";

        public const string SHOW_CLEAN_UP_INFO_COMPLETE = "MissionEvents_SHOW_CLEAN_UP_INFO_COMPLETE";
        public const string SHOW_CLEAN_UP_REWARD_BOX_COMPLETE = "MissionEvents_SHOW_CLEAN_UP_REWARD_BOX_COMPLETE";
        public const string SHOW_CLEAN_UP_REWARD_LIST_COMPLETE = "MissionEvents_SHOW_CLEAN_UP_REWARD_LIST_COMPLETE";

        public const string SHOW_NORMAL_REPORT_FAILED_COMPLETE = "MissionEvents_SHOW_NORMAL_REPORT_FAILED_COMPLETE";    //挑战失败展示结束

        public const string SHOW_NORMAL_REPORT_REWARD_COMPLETE = "MissionEvents_SHOW_NORMAL_REPORT_REWARD_COMPLETE";    //普通战斗结束第一个界面奖励Win
        public const string SHOW_NORMAL_REPORT_DATA_COMPLETE = "MissionEvents_SHOW_NORMAL_REPORT_DATA_COMPLETE";//战斗数据统计结束
        public const string SHOW_NORMAL_REPORT_BOX_COMPLETE = "MissionEvents_SHOW_NORMAL_REPORT_BOX_COMPLETE";//boss本打开宝箱后
        public const string SHOW_NORMAL_REPORT_CARD_COMPLETE = "MissionEvents_SHOW_NORMAL_REPORT_CARD_COMPLETE";//普通本翻牌后

        public const string SHOW_REPORT_FIRST_CROSS_COMPLETE = "MissionEvents_SSHOW_REPORT_FIRST_CROSS_COMPLETE";//首次通关
        public const string MISSION_SWEEP_SUCCESSFUL = "MissionEvents_MISSION_SWEEP_SUCCESSFUL";
    }
}
