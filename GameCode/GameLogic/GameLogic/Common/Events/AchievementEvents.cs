﻿#region 模块信息
/*==========================================
// 文件名：AchievementEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/3 10:01:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class AchievementEvents
    {
        public const string REFRESH_ACHIEVEMENT_LIST = "Achievement_Refresh_Achievement_list";

        public const string REFRESH_ACHIEVEMENT_TOGGLE_STATE = "AchievementEvents_Refresh_Achievement_Toggle_State";
    }
}