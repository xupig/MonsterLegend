﻿#region 模块信息
/*==========================================
// 文件名：TokenEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 15:00:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TokenEvents
    {
        public const string INIT = "TokenEvents_INIT";
        public const string UPDATE = "TokenEvents_UPDATE";
        public const string REFRESH_LIST = "TokenEvents_REFRESH_LIST";
        public const string REFRESH_TOTAL_COUNT = "TokenEvents_REFRESH_TOTAL_COUNT";
    }
}
