﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FashionEvents
    {
        public const string UPDATE_FASHION_VIEW = "FashionEvents_UPDATE_FASHION_VIEW";

        public const string SHOW_FASHION_INFO = "FashionEvents_SHOW_FASHION_INFO";

        public const string REFRESH_FASHION_LIST = "FashionEvents_REFRESH_FASHION_LIST";

        public const string PUT_ON_FASHION = "FashionEvents_PUT_ON_FASHION";

        public const string SELECT_ONE_FASHION = "FashionEvents_SELECT_ONE_FASHION";

        public const string REFRESH_EFFECT_LIST = "FashionEvents_REFRESH_EFFECT_LIST";

        public const string CHANGE_EQUIP_EFFECT = "FashionEvents_CHANGE_EQUIP_EFFECT";

        public const string ACTIVATE_FASHION_CHANGE = "FashionEvents_ACTIVATE_FASHION_CHANGE";

        public const string WEAR_EQUIP_EFFECT = "FashionEvents_WEAR_EQUIP_EFFECT";

        public const string TAKE_OFF_EQUIP_EFFECT = "FashionEvents_TAKE_OFF_EQUIP_EFFECT";

        public const string CAN_ACTIVITE_NEW_EFFECT = "FashionEvents_CAN_ACTIVITE_NEW_EFFECT";

        public const string CAN_PUT_ON_NEW_FASHION = "FashionEvents_CAN_PUT_ON_NEW_FASHION";
    }
}