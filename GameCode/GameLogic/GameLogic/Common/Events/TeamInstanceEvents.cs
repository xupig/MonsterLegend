﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TeamInstanceEvents
    {

        public const string Set_Entry_View_Instance_Id = "TeamInstanceEvents_Set_Entry_View_Instance_Id";

        public const string On_Call_People_Succeed = "TeamInstanceEvents_On_Call_People_Succeed";

        public const string Teammate_Refresh = "TeamInstanceEvents_Teammate_Refresh";

        public const string Teammate_Hp_Refresh = "TeamInstanceEvents_Teammate_Hp_Refresh";

        public static string Teammate_Online_Refresh = "TeamInstanceEvents_Teammate_Online_Refresh";
    }
}
