﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    class EquipEvents
    {
        public const string GET_RISING_STAR_CHECK = "EquipEvents_Get_Rising_Star_Check";

        public const string RISING_STAR_SUCCESS = "EquipEvents_Rising_Star_Success";

        public const string REFRESH_EQUIP_POINT = "EquipEvents_Refresh_Equip_Point";

        public const string REPLACE_SUIT = "EquipEvents_Replace_Suit";

        public const string EQUIP_SUIT_EFFECT_CHANGED = "EquipEvents_Suit_Effect_Changed";

        public const string SMELTER_CUSTOME_ITEM_CHANGED = "EquipEvents_Smelter_Custome_Item_Changed";

        public const string EQUIP_SMELT_RESULT = "EquipEvents_Smelt_Result";

        public const string CONFIRM_RECAST_RESULT = "EquipEvents_Comfirm_Recast_Result";

        public const string RECAST_RESULT = "EquipEvents_Recast_Result";

        public const string EQUIP_TIPS_SHOW_FINISH = "EquipEvents_Equip_Tips_Show_Finish";

        public const string SELECT_RISING_STAR = "EquipEvents_Equip_Tips_ShowFinish";
    }
}
