﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 10:58:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class DreamlandEvents
    {
        public const string RequestData = "DreamlandEvent_RequestData";
        public const string ShowScene = "DreamlandEvent_ShowScene";
        public const string ReceiveStartExplore = "DreamlandEvent_ReceiveStartExplore";
        public const string ReceiveFinishExplore = "DreamlandEvent_ReceiveFinishExplore";
        public const string RefreshChapter = "DreamlandEvent_RefreshChapter";
        public const string ReceiveQualityUp = "DreamlandEvent_ReceiveQualityUp";
        public const string ReceiveMissionRecord = "DreamlandEvent_ReceiveMissionRecord";
        public const string ReceiveDreamlandBoxInfo = "DreamlandEvent_ReceiveDreamlandBoxInfo";
        public const string ClickDragon = "DreamlandEvent_ClickDragon";
        public const string ReceivePlayerInfo = "DreamlandEvent_ReceivePlayerInfo";
        public const string ShowMissionRecord = "DreamlandEvent_ShowMissionRecord";
        public const string ReceiveBeRobedByMeTimes = "DreamlandEvent_ReceiveBeRobedByMeTimes";
        public const string NewRecordChanged = "DreamlandEvent_NewRecordChanged";
        public const string ReceiveGetYestodayReward = "DreamlandEvent_ReceiveGetYestodayReward";
        public const string ReceiveDreamlandData = "DreamlandEvent_ReceiveDreamlandData";
        public const string ShowStationTips = "DreamlandEvent_ShowStationTips";
    }
}