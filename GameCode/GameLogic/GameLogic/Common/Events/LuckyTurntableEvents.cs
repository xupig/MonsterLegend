﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class LuckyTurntableEvents
    {
        public const string GET_TURNTABLE_INFO = "LuckyTurntableEvents_GET_TURNTABLE_INFO";
        public const string GET_TURNTABLE_REWARD = "LuckyTurntableEvents_GET_TURNTABLE_REWARD";
        public const string RESET_TURNTABLE = "LuckyTurntableEvents_RESET_TURNTABLE";
        public const string TURNTABLE_POINTER_RUNING = "LuckyTurntableEvents_TURNTABLE_POINTER_RUNING";
        public const string TURNTABLE_POINTER_STOP_RUN = "LuckyTurntableEvents_TURNTABLE_POINTER_STOP_RUN";
        public const string GET_LUCKY_RANK_INFO = "LuckyTurntableEvents_GET_LUCKY_RANK_INFO";
        public const string SHOW_LUCKY_RANK_VIEW = "LuckyTurntableEvents_SHOW_LUCKY_RANK_VIEW";
        public const string STOP_REQUEST_LUCKY_RANK_TIMER = "LuckyTurntableEvents_STOP_REQUEST_LUCKY_RANK_TIMER";
        public const string START_REQUEST_LUCKY_RANK_TIMER = "LuckyTurntableEvents_START_REQUEST_LUCKY_RANK_TIMER";
        public const string ON_SHOW_LUCKY_RANK_SPOT_LIGHT = "LuckyTurntableEvents_ON_SHOW_LUCKY_RANK_SPOT_LIGHT";
    }
}
