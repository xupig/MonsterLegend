﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class GemEvents
    {
        #region 客户端事件
        public const string GemShowFenglingResult = "GemShowFenglingResult";
        public const string GemShowCombineResult = "GemShowCombineResult";

        public const string GemPanelRefreshTabSpotLight = "GemEvents_GemPanelRefreshTabSpotLight";
        public const string GemPanelRefreshCategoryRedPoint = "GemEvents_GemPanelRefreshCategoryRedPoint";
        #endregion


        #region 客户端操作事件
        public const string INLAY = "GemEvents.Inlay";
        public const string SHOW_GEM_NOT_ENOUGH_TIP = "GemEvents.ShowNotEnoughTip";
        public const string SHOW_GEM_SPIRIT_TIP = "GemEvents.ShowSpiritTip";
        public const string SHOW_GEM_SPIRIT_HELP = "GemEvents.ShowSpiritHelp";
        public const string SHOW_SPIRIT_RESULT = "GemEvents.ShowSpiritResult";
        public const string SHOW_GEM_COMPOSITE_COST = "GemEvents.ShowGemCompositeCost";
        #endregion


        #region 服务器端返回触发事件
        public const string INLAY_SUCCESS = "GemEvents.InlaySuccess";
        public const string OUTLAY_SUCCESS = "GemEvents.OutlaySuccess";
        public const string UPGRADE_SUCCESS = "GemEvents.UpgradeSuccess";
        public const string PUNCH_SUCCESS = "GemEvents.PunchSuccess";
        public const string COMPOSITE_SUCCESS = "GemEvents.CompositeSuccess";
        public const string SPIRIT_SUCCESS = "GemEvents.SpiritSuccess";
        #endregion

    }
}
