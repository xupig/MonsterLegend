﻿#region 模块信息
/*==========================================
// 文件名：MountEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/8 19:36:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class MountEvents
    {
        public const string REFRESH_MOUNT_LIST = "MountEvents_REFRESH_MOUNT_LIST";

        public const string RIDE_ON_MOUNT = "MountEvents_RIDE_ON_MOUNT";

        public const string SHOW_MOUNT_INFO = "MountEvents_SHOW_MOUNT_INFO";

        public const string CAN_RIDE_NEW_MOUNT = "MountEvents_CAN_RIDE_NEW_MOUNT";
    }
}