﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class GuildWarEvents
    {
        public const string Refresh_Guild_War_Stage = "GuildWarEvents_Refresh_Guild_War_Stage";

        public const string Refresh_Guild_War_Try_Info = "GuildWarEvents_Refresh_Guild_War_Try_Info";

        public const string Refresh_Guild_War_Try_Person_Rank = "GuildWarEvents_Refresh_Guild_War_Try_Person_Rank";

        public const string Refresh_Guild_War_Try_Week_Rank = "GuildWarEvents_Refresh_Guild_War_Try_Week_Rank";

        public const string Refresh_Guild_War_Try_Log = "GuildWarEvents_Refresh_Refresh_Guild_War_Try_Log";

        public const string Refresh_Guild_War_Sign_List = "GuildWarEvents_Refresh_Guild_War_Sign_List";

        public const string Refresh_Guild_Sign_Up_Success = "GuildWarEvents_Refresh_Guild_Sign_Up_Success";

        public const string Refresh_Guild_Group_Match_Info = "GuildWarEvents_Refresh_Guild_Group_Match_Info";

        public const string Refresh_Guild_Group_Match_Log = "GuildWarEvents_Refresh_Guild_Group_Match_Log";

        public const string Refresh_Guild_Match_Count_Down_Info = "GuildWarEvents_Refresh_Guild_Match_Count_Down_Info";

        public const string On_Get_Guild_Military_Info = "GuildWarEvents_On_Get_Guild_Military_Info";

        public const string On_Get_Guild_War_Log = "GuildWarEvents_On_Get_Guild_War_Log";

        public const string On_Get_Guild_War_Rank_List = "GuildWarEvents_On_Get_Guild_War_Rank_List";

        public const string On_Get_Guild_War_History_Log = "GuildWarEvents_On_Get_Guild_War_History_Log";

        public const string Draw = "GuildWarEvents_Draw";

        public const string Show_Drop_Down_List = "GuildWarEvents_Show_Drop_Down_List";

        public const string Assigned_Position = "GuildWarEvents_Assigned_Position";

        public const string Draw_Tween_End = "GuildWarEvents_Draw_Tween_End";

        public const string Response_Draw_Info = "GuildWarEvents_Response_Draw_Info";

        public const string Response_Final_Match_Info = "GuildWarEvents_Response_Final_Match_Info";

    }
}
