﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TutorEvents
    {
        public const string REFRESH_TUTOR_LOGS = "TutorEvents_REFRESH_TUTOR_LOGS";

        public const string REFRESH_GREEN_POINT = "TutorEvents_REFRESH_GREEN_POINT";
    }
}
