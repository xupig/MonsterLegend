﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class OpenServerEvents
    {
        public const string InitData = "OpenServerEvents_InitData";
        public const string Update = "OpenServerEvents_Update";
        public const string CountdownEnd = "OpenServerEvents_CountdownEnd";
    }
}
