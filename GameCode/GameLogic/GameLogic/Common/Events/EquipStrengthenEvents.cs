﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class EquipStrengthenEvents
    {
        /// <summary>
        /// 更新全部部位强化信息时触发
        /// </summary>
        public const string Init = "EquipStrengthenEvents_Init";

        /// <summary>
        /// 更新单个部位强化信息时触发
        /// </summary>
        public const string Update = "EquipStrengthenEvents_Update";

        /// <summary>
        /// 幸运石选中
        /// </summary>
        public const string SelectedLuckyStone = "EquipStrengthenEvents_SelectedLuckyStone";

        /// <summary>
        /// 物品替换
        /// </summary>
        public const string ReplaceMaterial = "EquipStrengthenEvents_ReplaceMaterial";

        /// <summary>
        /// 强化成功
        /// </summary>
        public const string StrengthenSuccess = "EquipStrengthenEvents_StrengthenSuccess";

        /// <summary>
        /// 强化失败
        /// </summary>
        public const string StrengthenFail = "EquipStrengthenEvents_StrengthenFail";

        /// <summary>
        /// 星星动画播放完成后播放阶数动画
        /// </summary>
        public const string PlayStepAnimator = "EquipStrengthenEvents_PlayStepAnimator";

    }
}
