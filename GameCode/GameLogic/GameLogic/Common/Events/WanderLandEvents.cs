﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/30 14:01:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class WanderLandEvents
    {
        public const string RefreshWanderLandView = "WanderLandEvent_RefreshWanderLandView";
        public const string RefreshGetBoxReward = "WanderLandEvent_RefreshGetBoxReward";
        public const string PanelVisibleChange = "WanderLandEvent_PanelVisibleChange";
        public const string BoxPanelClose = "WanderLandEvent_BoxPanelClose";
        public const string DetailViewSlide = "WanderLandEvent_DetailViewSlide";
        public const string SweepSuccess = "WanderLandEvent_SweepSuccess";
        public const string Reset = "WanderLandEvent_Reset";
    }
}
