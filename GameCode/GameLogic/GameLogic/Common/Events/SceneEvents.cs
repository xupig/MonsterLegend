﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public static class SceneEvents
    {
        public static string ENTER_SCENE = "SceneEvents_ENTER_SCENE";  //进入某场景

        public static string LEAVE_SCENE = "SceneEvents_LEAVE_SCENE";  //离开某场景

        public static string ENTER_MAP = "SceneEvents_ENTER_MAP"; //进入某场景

        public static string BEFORE_LEAVE_MAP = "SceneEvents_BEFORE_LEAVE_MAP";  //离开某场景

        public static string ON_PLAYER_RELIVE = "SceneEvents_ON_PLAYER_RELIVE"; //玩家复活

        public static string SCENE_LOADED = "SceneEvents_SCENE_LOADED";//进入某个场景，并且场景资源加载完成
    }
}
