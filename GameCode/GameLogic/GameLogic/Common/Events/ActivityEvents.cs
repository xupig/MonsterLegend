﻿#region 模块信息
/*==========================================
// 文件名：ActivityEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/6 15:04:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class ActivityEvents
    {
        public const string ACTIVITY_LEFT_COUNT_INIT = "ActivityEvents_ACTIVITY_LEFT_COUNT_INIT";

        public const string ACTIVITY_TIME_UPDATE_LIST = "ActivityEvents_ACTIVITY_TIME_UPDATE_LIST";

        public const string ACTIVITY_CHANGE_STATE = "ActivityEvents_ACTIVITY_CHANGE_STATE";

        public const string SELECT_CHALLENGE = "ActivityEvents_SELECT_CHALLENGE";


        public const string SELECT_LIMIT_ACTIVITY = "ActivityEvents_SELECT_LIMIT_ACTIVITY";
    }
}
