﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class ChatEvents
    {

        #region 客户端事件
        /// <summary>
        ///  常用语编辑事件
        /// </summary>
        public const string EditPhrase = "ChatEvents_EditPhrase";

        public const string OPEN_CHAT_PANEL = "ChatEvents_OpenChatPanel";
        public const string CLOSE_CHAT_PANEL = "ChatEvents_CloseChatPanel";
        public const string OPEN_EMOJI = "ChatEvents_OpenEmoji";
        public const string SELECT_EMOJI = "ChatEvents_SelectEmoji";
        public const string CLOSE_EMOJI = "ChatEvents_CloseEmoji";
        public const string SELECT_PHRASE = "ChatEvents_SelectPhrase";
        public const string SEND_FORM_MENU = "ChatEvents_SendFromMenu";
        public const string END_EDIT_PHRASE = "ChatEvents_EndEditPhrase";
        public const string SELECT_FRIEND = "ChatEvents_SelectFriend";
        public const string HIDE_INPUT = "ChatEvents_HideInput";
        public const string SHOW_INPUT = "ChatEvents_ShowInput";
        public const string ENABLE_PRIVATE_CHAT_BACK_BTN = "ChatEvents_EnablePrivateChatBackBtn";
        public const string SELECT_PRIVATE_CHAT_RIGHT_FRIEND_ITEM = "ChatEvents_SelectPrivateChatRightFriendItem";
        public const string UPDATE_PRIVATE_CHAT_RECENT_PLAYER = "ChatEvents_UpdatePrivateChatRecentPlayer";
        public const string QUIT_TEAM = "ChatEvents_QuitTeam";
        public const string LEAVE_GUILD = "ChatEvents_LeaveGuild";
        public const string SHOW_PASSED_TIME_IN_NEW_LINE = "ChatEvents_ShowPassedTimeInNewLine";
        public const string ON_CLICK_ACHIEVEMENT_LINK = "ChatEvents_OnClickAchievementLink";
        public const string GRAB_RED_ENVELOPE = "ChatEvents_GrabRedEnvelope";
        public const string ON_CLICK_RESPONSE_PRIVATE_PLAYER = "ChatEvents_OnClickResponsePrivatePlayer";
        public const string START_CHAT_QUESTION = "ChatEvents_StartChatQuestion";
        public const string END_CHAT_QUESTION = "ChatEvents_EndChatQuestion";
        public const string CHANGE_PRIVATE_PLAYER_NAME = "ChatEvents_ChangePrivatePlayerName";
        public const string START_RECORD_VOICE = "ChatEvents_StartRecordVoice";
        public const string PLAY_VOICE = "ChatEvents_PlayVoice";
        public const string END_VOICE_TALK = "ChatEvents_EndVoiceTalk";
        public const string END_PLAY_VOICE = "ChatEvents_EndPlayVoice";
        public const string SHOW_VOICE_NOTICE_VIEW = "ChatEvents_ShowVoiceNoticeView";
        public const string SHOW_START_RECORD_VOICE_NOTICE = "ChatEvents_ShowStartRecordVoiceNotice";
        public const string SHOW_CAN_CANCEL_TALK_NOTICE = "ChatEvents_ShowCanCancelTalkNotice";
        public const string HIDE_ALL_VOICE_NOTICE = "ChatEvents_HideAllVoiceNotice";
        //语音sdk回调unity时，在接口中触发的事件
        public const string SDK_START_TALK = "ChatEvents_SdkStartTalk";
        public const string SDK_END_TALK = "ChatEvents_SdkEndTalk";
        public const string SDK_TALK_ERROR = "ChatEvents_SdkTalkError";
        public const string SDK_VOICE_MESSAGE = "ChatEvents_SdkVoiceMessage";
        public const string SDK_VOICE_VOLUME = "ChatEvents_SdkVoiceVolume";
        public const string SDK_START_PLAY_VOICE = "ChatEvents_SdkStartPlayVoice";

        #endregion

        #region 服务器事件
        public const string RECEIVE_CONTENT = "ChatEvents_ReceiveContent";

        public const string ReadContent = "ChatEvents_ReadContent";

        public const string GetRecentChatInfo = "ChatEvents_GetRecentChatInfo";

        public const string BLACK_LIST_CHANGE = "ChatEvents_BLACK_LIST_CHANGE";

        #endregion

    }
}
