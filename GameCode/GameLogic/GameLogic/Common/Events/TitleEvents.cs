﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class TitleEvents
    {
        public const string InitTitleData = "TitleEvents_InitTitleData";

        public const string UpdateTitleData = "TitleEvents_UpdateTitleData";
    }
}
