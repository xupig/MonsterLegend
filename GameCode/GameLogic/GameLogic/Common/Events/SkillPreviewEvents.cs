﻿using System;
using System.Collections.Generic;


namespace Common.Events
{
    public class SkillPreviewEvents
    {
        /// <summary>
        /// 播放技能
        /// </summary>
        public const string PLAY_SKILL = "SkillPreview_PlaySkill";
    }
}
