﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class StarSoulEvents
    {
        public const string REFRESH_STARSOUL_VIEW = "StarSoulEvents_REFRESH_STARSOUL_VIEW";

        public const string SHOW_STAR_SOUL_GUIDE = "StarSoulEvents_SHOW_STAR_SOUL_GUIDE";

        public const string SHOW_BACKGROUND = "StarSoulEvents_SHOW_BACKGROUND";

        public const string REFRESH_STAR_PAGE_POINT = "StarSoulEvents_REFRESH_STAR_PAGE_POINT";

        public const string ON_STAR_PAGE_OPEN = "ON_STAR_PAGE_OPEN";
    }
}
