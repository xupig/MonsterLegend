﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FieldTaskEvents
    {
        public const string ON_GET_FIELD_TASK_INFO = "FieldTaskEvents_ON_GET_FIELD_TASK_INFO";

        public const string ON_AUTO_FIELD_TASK_STATE_CHANGE = "FieldTaskEvents_ON_AUTO_FIELD_TASK_STATE_CHANGE";

    }
}
