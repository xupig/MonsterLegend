﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public static class LoginEvents
    {
        public static string ON_ENTER_LOGIN_SCENE = "ON_ENTER_LOGIN_SCENE"; //进入登陆场景
        public static string LOGIN_R_LOGIN = "LOGIN_R_LOGIN"; //接收登陆请求
        public static string ON_XMLDATA_INITED = "ON_XMLDATA_INITED"; //xml加载完成
        public static string RANDOM_NAME_RESP = "randomNameResp";  //随机名称返回
        public static string ON_KICK_OUT = "OnKickOut";  //强制T下线

        /// <summary>
        /// 选服返回
        /// </summary>
        public static string C_SELECT_SERVER_RETURN = "c_select_server_return";

        /// <summary>
        /// 创建角色
        /// </summary>
        public static string C_CREATE_ROLE = "c_create_role";

        /// <summary>
        /// 进入游戏请求
        /// </summary>
        public static string C_START_GAME_REQ = "c_start_game_req";

        /// <summary>
        /// 重新登录请求
        /// </summary>
        public static string C_RELOGIN_REQ = "c_relogin_req";
        /// <summary>
        /// 返回登录界面通知
        /// </summary>
        public static string RETURN_LOGIN = "returnLogin";
        /// <summary>
        /// 刷新排队信息
        /// </summary>
        public static string REFRESH_SERVER_LINE_INFO = "refreshServerLineInfo";

        /// <summary>
        /// 刷新登录界面的账号名显示
        /// </summary>
        public static string REFRESH_LOGIN_ACCOUNTNAME = "refreshLoginAccountName";
        /// <summary>
        /// 设置公告标题和时间事件
        /// </summary>
        public static string C_SET_NOTICE_TITLE_AND_DATE = "setNoticeTitleAndDate";
        /// <summary>
        /// 显示平台登录主界面
        /// </summary>
        public static string SHOW_PLATFORM_LOGIN = "showPlatformLogin";
        /// <summary>
        /// 显示平台登录bar
        /// </summary>
        public static string SHOW_PLATFORM_LOGIN_BAR = "showPlatformLoginBar";
        /// <summary>
        /// 隐藏平台登录bar
        /// </summary>
        public static string HIDE_PLATFORM_LOGIN_BAR = "hidePlatformLoginBar";
        /// <summary>
        /// 平台提示相关
        /// </summary>
        public static string SHOW_PLATFORM_LOGIN_TIPS = "showPlatformLoginTips";
        /// <summary>
        /// 平台登录账号
        /// </summary>
        public static string SHOW_PLATFORM_LOGIN_ACCOUNT = "showPlatformLoginAccount";

        /// <summary>
        /// 刷新服务器列表
        /// </summary>
        public static string C_REFRESH_SERVER_LIST = "refreshServerList";
    }
}
