﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class MainUIEvents
    {
        public const string SHOW_TEAM_ENTRY_VIEW = "MainUIEvents_SHOW_TEAM_ENTRY_VIEW";
        public const string HIDE_TEAM_ENTRY_VIEW = "MainUIEvents_HIDE_TEAM_ENTRY_VIEW";

        public const string MAINUI_T_BTN_SKILL1_ON_CLICK = "MAINUI_T_BTN_SKILL1_ON_CLICK";
        public const string MAINUI_T_BTN_SKILL2_ON_CLICK = "MAINUI_T_BTN_SKILL2_ON_CLICK";
        public const string MAINUI_T_BTN_SKILL3_ON_CLICK = "MAINUI_T_BTN_SKILL3_ON_CLICK";
        public const string MAINUI_T_BTN_SKILL4_ON_CLICK = "MAINUI_T_BTN_SKILL4_ON_CLICK";

        public const string CLOSE_PLAYERINFO = "MainUIEvents_CLOSE_PLAYERINFO";

        public const string FORTRESS_BUILDING_CLICK = "MainUIEvents_FORTRESS_BUILDING_CLICK";

        public const string RESET_CONTROL_STICK = "MainUIEvents_RESET_CONTROL_STICK";

        public const string FAST_NOTICE_LIST_CHANGE = "MainUIEvents_FAST_NOTICE_LIST_CHANGE";

        public const string SHOWORHIDE_MAINUI_PART = "MainUIEvents_SHOWORHIDE_MAINUI_PART";
        public const string SHOWORHIDE_MAIN_CHAT_PART = "MainUIEvents_SHOWORHIDE_MAIN_CHAT_PART";

        public const string INFORMATION_OPERATION_STATE_CHANGE = "MainUIEvents_INFORMATION_OPERATION_STATE_CHANGE";

        public const string ON_SHOW_MAIN_UI = "MainUIEvents_ON_SHOW_MAIN_UI";

        public const string WORLD_BOSS_CHANGE = "MainUIEvents_WORLD_BOSS_CHANGE";

        public const string MINI_MAP_GATE_CHANGE = "MainUIEvents_MINI_MAP_GATE_CHANGE";

        public const string MINI_MAP_DETAIL_CHANGE = "MainUIEvents_MINI_MAP_DETAIL_CHANGE";

        public const string FIELD_PLAY_DETAIL_CHANGE = "MainUIEvents_FIELD_PLAY_DETAIL_CHANGE";

        public const string ON_FUNCTION_ENTRY_VIEW_OPEN = "MainUIEvents_ON_FUNCTION_ENTRY_VIEW_OPEN";

        public const string NEED_SHOW_ANTIADDICTION = "MainUIEvents_NEED_SHOW_ANTIADDICTION";

        /// <summary>
        /// 刷新IOS9视频录制时间
        /// </summary>
        public const string REFRESH_MV_RECORD_TIME = "MainUIEvents_refresh_mv_record_time";

    }
}
