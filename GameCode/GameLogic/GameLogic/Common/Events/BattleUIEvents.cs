﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class BattleUIEvents
    {
        public const string CREATE_LIFE_BAR_FOR_ENTITY = "BattleUIEvents_CREATE_LIFE_BAR_FOR_ENTITY";
        public const string CREATE_DAMAGE_NUMBER_FOR_ENTITY = "BattleUIEvents_CREATE_DAMAGE_NUMBER_FOR_ENTITY";
        public const string SHOW_CREATURE_INFO = "MainUIEvents_SHOW_CREATURE_INFO";
        public const string HIDE_CREATURE_INFO = "MainUIEvents_HIDE_CREATURE_INFO";
        public const string SHOW_SPEECH = "MainUIEvents_SHOW_SPEECH";

        public const string SHOW_FIND_PATH = "MainUIEvents_SHOW_FIND_PATH";
        public const string HIDE_FIND_PATH = "MainUIEvents_HIDE_FIND_PATH";

        public const string REMOVE_LIFE_BAR_FOR_ENTITY = "BattleUIEvents_REMOVE_LIFE_BAR_FOR_ENTITY";
        public const string SKILL_COOL_DOWN = "BattleUIEvents_SKILL_COOL_DOWN";
        public const string SET_SKILL_ID = "BattleUIEvents_SET_SKILL_ID";
        public const string SET_SKILL_EP = "BattleUIEvents_SET_SKILL_EP";
        public const string SET_SKILL_ENABLE = "BattleUIEvents_SET_SKILL_ENABLE";
        public const string ADD_SKILL = "BattleUIEvents_ADD_SKILL";
        public const string SET_SKILL_BEFORE_NOTICE = "BattleUIEvents_SET_SKILL_BEFORE_NOTICE";

        public const string SHOW_INFOMATION = "BattleUIEvents_ShowInfomation";
        public const string HIDE_SYSTEM_NOTICE_DISPLAY = "BattleUIEvents_HIDE_SYSTEM_NOTICE_DISPLAY";
        public const string HIDE_RIGHT_UP_INFOMATION = "BattleUIEvents_HideRightUpInfomation";
        public const string HIDE_PROGRESSBAR_INFOMATION = "BattleUIEvents_HideProgressBarInfomation";
        public const string REFRESH_BOSS_VIEW = "BattleUIEvents_REFRESH_BOSS_VIEW";

        public const string BATTLE_PET_DATA_INITED = "BattleUIEvents_BATTLE_PET_DATA_INITED";
        public const string UPDATE_PET_FIGHT_STATE = "BattleUIEvents_UPDATE_PET_FIGHT_STATE";
        public const string BATTLE_PET_REVIVE = "BattleUIEvents_BATTLE_PET_REVIVE";
        public const string BATTLE_PET_REPLACE_CLICK = "BattleUIEvents_BATTLE_PET_REPLACE_CLICK";
        public const string PET_DIE = "BattleUIEvents_PET_DIE";
        public const string BATTLE_PET_HP_CHANGE = "BattleUIEvents_BATTLE_PET_HP_CHANGE";
        public const string BATTLE_PET_MAX_HP_CHANGE = "BattleUIEvents_BATTLE_PET_MAX_HP_CHANGE";
        public const string BATTLE_PET_SKILL_COOLDOWN = "BattleUIEvents_BATTLE_PET_SKILL_COOLDOWN";
        public const string BATTLE_PET_RECEIVE_SERVER_DATA_READY = "BattleUIEvents_BATTLE_PET_RECEIVE_SERVER_DATA_READY";
        public const string BATTLE_PET_REFRESH_HP = "BattleUIEvents_BATTLE_PET_REFRESH_HP";

        public const string BATTLE_CONTROL_STICK_DOWN = "BattleUIEvents_BATTLE_CONTROL_STICK_DOWN";
        public const string BATTLE_PLAYER_ATTACKED = "BattleUIEvents_BATTLE_PLAYER_ATTACKED";

        public const string BATTLE_STAR_INFO = "BattleUIEvents_Battle_Star_Info";

        public const string DIVORCE_VEHICLE_BUTTON_VISIBILITY = "BattleUIEvents_DIVORCE_VEHICLE_BUTTON_VISIBILITY";

        public const string SHOW_AUTO_TEXT = "BattleUIEvents_SHOW_AUTO_TEXT";
        public const string HIDE_AUTO_TEXT = "BattleUIEvents_HIDE_AUTO_TEXT";

        public const string SHOW_PORTAL_PANEL = "BattleUIEvents_SHOW_PORTAL_PANEL";
        public const string SHOW_FLOAT_TIP = "BattleUIEvents_SHOW_FLOAT_TIP";
        public const string ADD_EP = "BattleUIEvents_ADD_EP";

        public const string REFRESH_AVATAR_SCORE = "BattleUIEvents_REFRESH_AVATAR_SCORE";

    }
}
