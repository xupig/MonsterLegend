﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    /// <summary>
    /// 福利系统--事件常量定义类
    /// 1、C_开头的变量，表示在客户端使用事件
    /// 2、S_开头的变量，表示客户端往服务器发送事件
    /// </summary>
    public class WelfareActiveEvent
    {
        /// <summary>
        /// 刷新界面的邀请码
        /// </summary>
        public const string C_REFRESH_INVITE_CODE = "c_refresh_invite_code";

        /// <summary>
        /// 刷新邀请码--获得的奖励列表
        /// </summary>
        public const string C_REFRESH_REWARD_LIST = "c_refresh_reward_list";

        /// <summary>
        /// 刷新记录列表
        /// </summary>
        public const string C_REFRESH_INVITE_RECORD_LIST = "c_refresh_invite_record_list";

        /// <summary>
        /// 刷新邀请码领取次数
        /// </summary>
        public const string C_REFRESH_RECEVIE = "c_refresh_recevie_times";

        /// <summary>
        /// 使用邀请码成功
        /// </summary>
        public const string C_USE_SUCESS = "c_use_sucess";

        /// <summary>
        /// 刷新奖励Tip
        /// </summary>
        public const string C_REFRESH0_REWARD_TIP = "c_refresh0_reward_tip";

        public const string C_REFRESH1_REWARD_TIP = "c_refresh1_reward_tip";

        /// <summary>
        /// 手机验证码返回
        /// </summary>
        public const string C_GET_CHECK_CODE = "c_get_check_code";

        /// <summary>
        /// 手机绑定信息返回
        /// </summary>
        public const string C_GET_BIND_MOBILE_INFO = "c_get_bind_mobile_info";

        /// <summary>
        /// 绑定手机结果返回
        /// </summary>
        public const string C_BIND_MOBILE_RESULT = "c_bind_mobile_result";

        /// <summary>
        /// 绑定奖励返回
        /// </summary>
        public const string C_BIND_REWARD_RESULT = "c_bind_reward_result";

        /// <summary>
        /// 分享
        /// </summary>
        public const string DOCKING_SHARE = "DOCKING_SHARE";
    }
}
