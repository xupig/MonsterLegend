﻿using System;
using System.Collections.Generic;

namespace Common.Events
{
    public class InputEvents
    {
        public const string SDK_INPUT_MESSAGE = "InputEvents_SDK_INPUT_MESSAGE";
        public const string SDK_COMMIT_INPUT = "InputEvents_SDK_COMMIT_INPUT";
        public const string SDK_ON_INPUT_CLICK = "InputEvents_SDK_ON_INPUT_CLICK";
        public const string SDK_CANCEL_INPUT = "InputEvents_SDK_CANCEL_INPUT";
    }
}
