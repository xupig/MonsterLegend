﻿#region 模块信息
/*==========================================
// 文件名：AntiAddictionEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/16 16:34:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class AntiAddictionEvents
    {
        public const string ANTI_TIME_REFRESH = "AntiAddictionEvents_ANTI_TIME_REFRESH";
    }
}