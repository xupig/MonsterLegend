﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class WingEvents
    {
        public const string CHANGE_WING_MODEL = "WingEvents_CHANGE_WING_MODE";

        public const string UPDATE_ITEM_VIEW = "WingEvents_UPDATE_ITEM_VIEW";

        public const string SHOW_TRAIN_RESULT = "WingEvents_SHOW_TRAIN_RESULT";

        public const string ENSURE_TRAIN_TEN = "WingEvents_ENSURE_TRAIN_TEN";

        public const string SHOW_DETAIL_VIEW = "WingEvents_SHOW_DETAIL_VIEW";

        public const string CLOSE_DETAIL_ITEM = "WingEvents_CLOSE_DETAIL_ITEM";

        public const string REFRESH_WING_RANK = "WingEvents_REFRESH_WING_RANK";

        public const string UPDATE_PUT_ON_WING = "WingEvents_UPDATE_PUT_ON_WING";

        public const string REFRESH_SELECT_ID = "WingEvents_REFRESH_SELECT_ID";

        public const string REFRESH_TRAIN_TOOLTIP = "WingEvents_REFRESH_TRAIN_TOOLTIP";

        public const string REFRESH_NOTICE_POINT = "WingEvents_REFRESH_NOTICE_POINT";

        public const string CLOSE_WING_ATTRIBUTE_VIEW = "WingEvents_CLOSE_WING_ATTRIBUTE_VIEW";

        public const string NEW_WING_CAN_WEAR = "WingEvents_NEW_WING_CAN_WEAR";
    }
}
