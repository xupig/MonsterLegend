﻿

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/11 10:00:33 
 * function: 
 * *******************************************************/

namespace Common.Events
{
    public class StatisticsEvent
    {
        public const string ON_ACCEPT_MAIN_TASK = "StatisticsEvent_OnAcceptMainTask";        //接受任务
        public const string ON_ENTER_MISSION = "StatisticsEvent_OnEnterMission";            //进入副本
        public const string ON_TRIGGER_GUIDE_OR_CG = "StatisticsEvent_OnTriggerGuideOrCG";            //触发引导或CG
    }
}
