﻿#region 模块信息
/*==========================================
// 文件名：AlchemyEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/16 16:05:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class AlchemyEvents
    {
        //public const string UPDATE_ALCHEMY_DATA = "AlchemyEvents_UPDATE_ALCHEMY_DATA";
        public const string UPDATE_ALCHEMY_ASSIST_LIST = "AlchemyEvents_UPDATE_ALCHEMY_ASSIST_LIST";
        public const string UPDATE_ALCHEMY_ASSIST_CNT = "AlchemyEvents_UPDATE_ALCHEMY_ASSIST_CNT";
        public const string UPDATE_ALCHEMY_INVITE_PLAYER = "AlchemyEvents_UPDATE_ALCHEMY_INVITE_PLAYER";

       // public const string UPDATE_ALCHEMY_TIME_STAMP = "AlchemyEvents_UPDATE_ALCHEMY_TIME_STAMP";

        public const string UPDATE_ALCHEMY_REWARD = "AlchemyEvents_UPDATE_ALCHEMY_REWARD";

       // public const string UPDATE_ALCHEMY_STATE = "AlchemyEvents_UPDATE_ALCHEMY_STATE";

    }
}
