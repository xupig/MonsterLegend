﻿#region 模块信息
/*==========================================
// 文件名：FriendEvents
// 命名空间: Common.Events
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/13 11:40:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FriendEvents
    {
        /// <summary>
        /// 刷新好友列表数据
        /// </summary>
        public const string REFRESH_FRIEND_LIST = "FriendEvents_REFRESH_FRIEND_LIST";

        /// <summary>
        /// 刷新单个好友数据
        /// </summary>
        public const string REFRESH_FRIEND = "FriendEvents_REFRESH_FRIEND";

        /// <summary>
        /// 获取单个玩家数据
        /// </summary>
        public const string GET_PLAYERINFO = "FriendEvents_GET_PLAYERINFO";

        /// <summary>
        /// 新增好友申请
        /// </summary>
        public const string FRIEND_APPLICATION_CHANGE = "FriendEvents_FRIEND_APPLICATION_CHANGE";

        public const string ADD_FRIEND = "FriendEvents_ADD_FRIEND";

        public const string REMOVE_FRIEND = "FriendEvents_REMOVE_FRIEND";

        public const string ADD_BALCK_LIST = "FriendEvents_ADD_BALCK_LIST";

        public const string REMOVE_BLACK_LIST = "FriendEvents_REMOVE_BLACK_LIST";

        public const string REFRESH_FRIEND_SEND_LIST = "FriendEvents_REFRESH_FRIEND_SEND_LIST";

        public const string REFRESH_FRIEND_RECV_LIST = "FriendEvents_REFRESH_FRIEND_RECV_LIST";

        public const string FRIEND_BLESS_STATE_CHANGE = "FriendEvents_FRIEND_BLESS_STATE_CHANGE";

        public const string INTIMATE_EVENT_LIST_CHANGE = "FriendEvents_INTIMATE_EVENT_LIST_CHANGE";

        public const string REFRESH_FRIEND_RECEIVED_LIST = "FriendEvents_REFRESH_FRIEND_RECEIVED_LIST";

        public const string FRIEND_ADD_APPLY_SUCCESS = "FriendEvents_FRIEND_ADD_APPLY_SUCCESS";

        public const string FRIEND_SEND_LIST_CHANGE = "FriendEvents_FRIEND_SEND_LIST_CHANGE";

        public const string FRIEND_INTIMATE_CHANGE = "FriendEvents_FRIEND_INTIMATE_CHANGE";

        public const string FRIEND_UPDATE_ONLINE = "FriendEvents_FRIEND_UPDATE_ONLINE";


        public const string FRIEND_ONLINE_STATE_CHANGE = "FriendEvents_FRIEND_ONLINE_STATE_CHANGE";

        public const string FRIEND_POINT_CHANGE = "FriendEvents_FRIEND_POINT_CHANGE";

        public const string FRIEND_RECOMMEND_CHANGE = "FriendEvents_FRIEND_RECOMMEND_CHANGE";
    }
}
