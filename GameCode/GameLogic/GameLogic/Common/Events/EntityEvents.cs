﻿
namespace Common.Events
{
    //实体相关事件
    public class EntityEvents
    {
        public const string ON_ENTER_WORLD = "EntityEvents_ON_ENTER_WORLD";     //进入世界时(EntityCreature)
        public const string ON_LEAVE_WORLD = "EntityEvents_ON_LEAVE_WORLD";     //离开世界时(EntityCreature)
        public const string ON_ENTER_SPACE = "EntityEvents_ON_ENTER_SPACE";     //进入场景时(EntityCreature)
        public const string ON_DEATH = "EntityEvents_ON_DEATH";                 //死亡时(EntityCreature)
        public const string ON_RELIVE = "EntityEvents_ON_RELIVE";               //复活时(EntityCreature)
        public const string ON_MANUAL_LOCK_ID_CHANGED = "EntityEvents_ON_MANUAL_LOCKED"; //锁定实体改变时(EntityCreature)
        public const string ON_MANUAL_LOCK_ENTITY = "EntityEvents_ON_MANUAL_LOCK_ENTITY";//操作锁定实体时(EntityCreature)
        public const string ON_DUEL_STAKE = "EntityEvents_ON_DUEL_STAKE";
        public const string ON_DUEL_FIGHT = "EntityEvents_ON_DUEL_FIGHT";
    }
}
