﻿#region 模块信息
/*==========================================
// 文件名：OldPlayerReturn
// 命名空间: ACTSystem.ACTSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/22 17:00:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Events
{
    public class OldPlayerReturnEvent
    {
        public const string UpdateData = "OldPlayerReturnEvent";
    }
}
