﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class GuildEvents
    {
        //public const string Show_Donate_Item_View = "GuildEvents_Show_Donate_Item_View";

        public const string Show_My_Guild_Info = "GuildEvents_Show_My_Guild_Info";

        public const string Refresh_Manifesto = "GuildEvents_On_Set_Manifesto";

        public const string Refresh_Member_List = "GuildEvents_Show_Member_List";

        public const string Show_Apply_List = "GuildEvents_Show_Apply_List";

        public const string OPEN_GUILD = "GuildEvents_OPEN_GUILD";

        public const string Show_Guild_List = "GuildEvents_Show_Guild_List";

        public const string Show_Search_List = "GuildEvents_Show_Search_List";

        public const string Refresh_Guild_list_Item = "GuildEvents_Refresh_Guild_list_Item";

        public const string Refresh_Achievement_View = "GuildEvents_Refresh_Achievement_View";

        public const string Refresh_Skill_View = "GuildEvents_Refresh_Skill_View";

        public const string Refresh_Guild_Money = "GuildEvents_Refresh_Guild_Money";

        public const string Refresh_Guild_Contribute = "GuildEvents_Refresh_Guild_Contribute";

        public const string Refresh_Guild_Notice_List = "GuildEvents_Refresh_Guild_Notice_List";

        public const string Refresh_Donate_Item_View = "GuildEvents_Refresh_Donate_Item_View";

        public const string Refresh_Guild_Level = "GuildEvents_Refresh_Guild_Level";

        public const string Refresh_Guild_Booming = "GuildEvents_Refresh_Guild_Booming";

        public const string Refresh_Guild_Record = "GuildEvents_Refresh_Guild_Record";

        public const string On_Guild_Recuit_Succeed = "GuildEvents_On_Guild_Recuit_Succeed";

        public const string Refresh_Guild_Point = "GuildEvents_Refresh_Guild_Point";

        public const string Refresh_Guild_Simple_Info = "GuildEvents_Refresh_Guild_Simple_Info";

        public const string Refresh_Guild_Invite = "GuildEvents_Refresh_Guild_Invite";

        public const string Refresh_Guild_Member_Count = "GuildEvents_Refresh_Guild_Member_Count";
    }
}
