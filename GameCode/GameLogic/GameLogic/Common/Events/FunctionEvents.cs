﻿#region 模块信息
/*==========================================
// 文件名：FunctionEvents
// 命名空间: GameLogic.GameLogic.Common.Events
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/25 16:34:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class FunctionEvents
    {
        public const string INIT = "FunctionEvents_INIT";
        public const string OPEN = "FunctionEvents_OPEN";
        public const string NO_PREVIEW_FUNCATION_OPEN = "FunctionEvents_NO_PREVIEW_FUNCATION_OPEN";
        public const string ADD_FUNTION = "FunctionEvents_ADD_FUNTION";
        public const string SET_FUNTION_POINT = "FunctionEvents_SET_FUNTION_POINT";
        public const string UPDATE_NOTICE = "FunctionEvents_UPDATE_NOTICE";

        public const string UPDATE_FUNCTION = "FunctionEvents_UPDATE_FUNCTION";
    }
}
