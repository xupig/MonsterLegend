﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public class CommonUIEvents
    {
        public const string ON_ITEM_OBTAIN_PANEL_CLOSE = "CommonUIEvents_ON_ITEM_OBTAIN_PANEL_CLOSE";

        public const string ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE = "CommonUIEvents_ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE";
    }
}
