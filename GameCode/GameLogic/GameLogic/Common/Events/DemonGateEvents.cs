﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public static class DemonGateEvents
    {
        public static string MATCH_OPEN                      = "MATCH_OPEN"; //自动匹配 打开

        public static string MATCH_CLOSE                     = "MATCH_CLOSE"; //自动匹配 关闭

        public static string RANKVIEW_OPEN                   = "RANKVIEW_OPEN"; //自动匹配  打开奖励排名

        public static string RANKVIEW_CLOSE                  = "RANKVIEW_CLOSE"; //自动匹配  关闭奖励排名

        public static string TRANSFORM                       = "TRANSFORM"; //传送

        public static string HEADFORFIGHT                    = "HEADFORFIGHT"; //前往挑战

        public const string  ACTION_DEMONGATE_INFO_EVENTS    = "ACTION_DEMONGATE_INFO_EVENTS";  // info
                                                             
        public const string  ACTION_DEMONGATE_FIGHT_EVENTS   = "ACTION_DEMONGATE_FIGHT_EVENTS"; //fight

        // 挑战模式再来一次
        public const string SCORE_MODE_AGAIN = "DemonGateEvents.SCORE_MODE_FIGHT_AGAIN";
    }
}
