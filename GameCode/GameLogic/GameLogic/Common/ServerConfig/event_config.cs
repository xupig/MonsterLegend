using System;
using System.Collections.Generic;

namespace Common.ServerConfig {

        ///<summary>
        ///用来定义游戏中触发的事件和事件各参数编号的定义
        ///</summary>


    public static class event_config {


        ///<summary>
        ///-------------------------事件编号------------------------------
        ///</summary>
    

        ///<summary>
        ///杀怪
        ///</summary>
        public const int    EVT_ID_KILL_MST               = 1;  

        ///<summary>
        ///通关副本
        ///</summary>
        public const int    EVT_ID_COMPLETE_COPY          = 2;  

        ///<summary>
        ///升级
        ///</summary>
        public const int    EVT_ID_LEVEL_UP               = 3;  

        ///<summary>
        ///增加物品
        ///</summary>
        public const int    EVT_ID_ITEM_ADD               = 4;  

        ///<summary>
        ///消耗物品
        ///</summary>
        public const int    EVT_ID_ITEM_COST              = 5;  

        ///<summary>
        ///hp改变。如低于%几，buff相关处理等
        ///</summary>
        public const int    EVT_ID_HP_CHANGE              = 6;  

        ///<summary>
        ///宝石合成
        ///</summary>
        public const int    EVT_ID_GEM_COMBINE            = 7;  

        ///<summary>
        ///宝石镶嵌
        ///</summary>
        public const int    EVT_ID_GEM_INLAY              = 8;  

        ///<summary>
        ///物品总量变化
        ///</summary>
        public const int    EVT_ID_ITEM_TOTAL_COUNT       = 9;  

        ///<summary>
        ///装备打孔
        ///</summary>
        public const int    EVT_ID_EQUIPMENT_PUNCHING     = 10; 

        ///<summary>
        ///附魔属性改变
        ///</summary>
        public const int    EVT_ID_ENCHANT_ATTRI_CHANGE   = 12; 

        ///<summary>
        ///换装的宝石继承
        ///</summary>
        public const int    EVT_ID_CHANGE_EQUIP_INHERIT   = 13; 

        ///<summary>
        ///玩家攻击事件
        ///</summary>
        public const int    EVT_ID_AVATAR_ATTACK          = 14; 

        ///<summary>
        ///玩家受击事件
        ///</summary>
        public const int    EVT_ID_AVATAR_HIT             = 15; 

        ///<summary>
        ///捡物品的事件
        ///</summary>
        public const int    EVT_ID_PICK_DROP              = 16; 

        ///<summary>
        ///按键事件
        ///</summary>
        public const int    EVT_ID_PRESS_KEY              = 17; 

        ///<summary>
        ///任务事件
        ///</summary>
        public const int    EVT_ID_TASK                   = 18; 

        ///<summary>
        ///VIP升级
        ///</summary>
        public const int    EVT_ID_VIP_LEVEL_UP           = 19; 

        ///<summary>
        ///强化装备
        ///</summary>
        public const int    EVT_ID_STRENGTHEN             = 20; 

        ///<summary>
        ///符文许愿
        ///</summary>
        public const int    EVT_ID_RUNE_WISH              = 21; 

        ///<summary>
        ///完成活动
        ///</summary>
        public const int    EVT_FINISH_ACTIVITY           = 22; 

        ///<summary>
        ///培养制造等级
        ///</summary>
        public const int    EVT_TRAIN_MFG                 = 23; 

        ///<summary>
        ///制造道具
        ///</summary>
        public const int    EVT_MAKE_ITEM                 = 24; 

        ///<summary>
        ///符文合成
        ///</summary>
        public const int    EVT_ID_RUNE_COMBINE           = 25; 

        ///<summary>
        ///给好友赠送体力
        ///</summary>
        public const int    EVT_GIVE_FRIEND_ENERGY        = 26; 

        ///<summary>
        ///换天登陆
        ///</summary>
        public const int    EVT_ID_LOGIN_ANOTHER_DAY      = 27; 

        ///<summary>
        ///穿上装备
        ///</summary>
        public const int    EVT_ID_EQUIP_CHANGE           = 28; 


        ///<summary>
        ///玩家上线
        ///</summary>
        public const int    EVT_AVATAR_ONLINE     = 30;  

        ///<summary>
        ///玩家下线
        ///</summary>
        public const int    EVT_AVATAR_OFFLINE    = 31;  

        ///<summary>
        ///野外寻宝
        ///</summary>
        public const int    EVT_WILD_TREASURE     = 32;  
    

        ///<summary>
        ///玩家死亡
        ///</summary>
        public const int    EVT_ID_AVATAR_DIE     = 40;  

        ///<summary>
        ///玩家复活
        ///</summary>
        public const int    EVT_AVATAR_REVIVE     = 41;  

        ///<summary>
        ///拥有buff
        ///</summary>
        public const int    EVT_OWN_BUFF          = 42;  

        ///<summary>
        ///每日累计在线时长(单位分钟)
        ///</summary>
        public const int    EVT_ID_ONLINE_TIME    = 43;  

        ///<summary>
        ///排行榜名次变更
        ///</summary>
        public const int    EVT_RANK_CHANGE       = 44;  

        ///<summary>
        ///功能开启
        ///</summary>
        public const int    EVT_FUNCTION_OPEN     = 45;  

        ///<summary>
        ///特权卡事件
        ///</summary>
        public const int    EVT_ID_PRIVILEGE_CARD = 46;  

        ///<summary>
        ///移动结束
        ///</summary>
        public const int    EVT_MOVE_END          = 49;  

        ///<summary>
        ///怪物出生
        ///</summary>
        public const int    EVT_MST_BORN          = 50;  

        ///<summary>
        ///怪物死亡
        ///</summary>
        public const int    EVT_MST_DIE           = 51;  

        ///<summary>
        ///技能cd结束
        ///</summary>
        public const int    EVT_SPELL_CD_END      = 52;  

        ///<summary>
        ///休息结束
        ///</summary>
        public const int    EVT_REST_END          = 53;  

        ///<summary>
        ///AI每帧思考
        ///</summary>
        public const int    EVT_THINK_TICK        = 54;  

        ///<summary>
        ///某个出生点刷出的怪全部死亡
        ///</summary>
        public const int    EVT_SPAWN_POINT_MST_ALL_DEAD  = 55; 

        ///<summary>
        ///怪物某部位被破坏
        ///</summary>
        public const int    EVT_MST_BODY_BE_DESTROYED     = 56; 

        ///<summary>
        ///点亮星魂事件
        ///</summary>
        public const int    EVT_ID_LIGHT_STAR_SPIRIT      = 57; 

        ///<summary>
        ///恶魔之门每日积分改变事件
        ///</summary>
        public const int    EVT_ID_EVIL_DAY               = 58; 

        ///<summary>
        ///恶魔之门每周积分改变事件
        ///</summary>
        public const int    EVT_ID_EVIL_WEEK              = 59; 

        ///<summary>
        ///要塞功能改变
        ///</summary>
        public const int    EVT_ID_FORTRESS_FUNCTION      = 60; 

        ///<summary>
        ///战斗力事件
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE            = 61; 

        ///<summary>
        ///充值（历史）
        ///</summary>
        public const int    EVT_ID_CHARGE                 = 62; 

        ///<summary>
        ///幻境探险，护送
        ///</summary>
        public const int    EVT_ID_DREAMLAND              = 63; 

        ///<summary>
        ///幻境探险，抢劫
        ///</summary>
        public const int    EVT_ID_DREAMLAND_ATTACK       = 64; 

        ///<summary>
        ///装备符文
        ///</summary>
        public const int    EVT_ID_RUNE_WEAR              = 65; 

        ///<summary>
        ///好友数量变化
        ///</summary>
        public const int    EVT_ID_FRIEND_NUM             = 66; 

        ///<summary>
        ///制造等级
        ///</summary>
        public const int    EVT_ID_MAKE_LEVEL             = 67; 

        ///<summary>
        ///宠物
        ///</summary>
        public const int    EVT_ID_PET                    = 68; 

        ///<summary>
        ///翅膀
        ///</summary>
        public const int    EVT_ID_WING                   = 69; 

        ///<summary>
        ///完成成就
        ///</summary>
        public const int    EVT_ID_ACHIEVEMENT            = 70; 

        ///<summary>
        ///外观分数
        ///</summary>
        public const int    EVT_ID_FACADE_SCORE           = 71; 

        ///<summary>
        ///晚上活动
        ///</summary>
        public const int    EVT_ID_NIGHT_MISSION          = 72; 

        ///<summary>
        ///传送门副本
        ///</summary>
        public const int    EVT_ID_PORTAL_MISSION         = 73; 

        ///<summary>
        ///藏宝图
        ///</summary>
        public const int    EVT_ID_TREASURE_MAP           = 74; 

        ///<summary>
        ///异界宝箱
        ///</summary>
        public const int    EVT_ID_CHEST                  = 75; 

        ///<summary>
        ///公会贡献
        ///</summary>
        public const int    EVT_ID_GUILD_CONTRIBUTION     = 76; 

        ///<summary>
        ///炼金
        ///</summary>
        public const int    EVT_ID_ALCHEMY                = 77; 

        ///<summary>
        ///加入公会
        ///</summary>
        public const int    EVT_ID_GUILD_JOIN             = 78; 

        ///<summary>
        ///任务上交物品
        ///</summary>
        public const int    EVT_ID_TASK_HAND_IN           = 79; 



        ///<summary>
        ///------技能系统中相关的事件--------
        ///</summary>
    

        ///<summary>
        ///释放某个技能
        ///</summary>
        public const int    EVT_SPELL_CAST            = 80;   

        ///<summary>
        ///技能的某个行为激活
        ///</summary>
        public const int    EVT_SPELL_ACTIVE_ACTION   = 81;   

        ///<summary>
        ///技能的某个行为触发
        ///</summary>
        public const int    EVT_SPELL_FIRE_ACTION     = 82;   


        ///<summary>
        ///关卡中的某个行为被成功触发
        ///</summary>
        public const int    EVT_SPACE_ACTION_TRIGGER  = 101;  

        ///<summary>
        ///装备升星
        ///</summary>
        public const int    EVT_ID_EQUIP_UPGRADE      = 102;  

        ///<summary>
        ///检查身上装备是否过期失效
        ///</summary>
        public const int    EVT_ID_CHECK_EQUIP_DEAD   = 111;  

        ///<summary>
        ///0点刷新
        ///</summary>
        public const int    EVT_ID_ZERO_CLOCK         = 112;  

        ///<summary>
        ///套装事件
        ///</summary>
        public const int    EVT_ID_SUIT               = 113;  

        ///<summary>
        ///强化装备, 总数
        ///</summary>
        public const int    EVT_ID_STRENGTHEN_TOTAL   = 114;  

        ///<summary>
        ///领取副本星星奖励
        ///</summary>
        public const int    EVT_ID_MISSION_STAR_REWARD= 115;  

        ///<summary>
        ///商城购买物品
        ///</summary>
        public const int    EVT_ID_MARKET_BUY_ITEM    = 116;  

        ///<summary>
        ///装备战斗力
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE_EQUIP                = 117;  

        ///<summary>
        ///强化战斗力
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE_STRENGTHEN           = 118;  

        ///<summary>
        ///符文战斗力
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE_RUNE                 = 119;  

        ///<summary>
        ///试炼密境
        ///</summary>
        public const int    EVT_ID_TRY_MISSION                      = 120;  

        ///<summary>
        ///公会商店购买
        ///</summary>
        public const int    EVT_ID_GUILD_SHOP_BUY                   = 121;  

        ///<summary>
        ///套装战斗力
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE_SUIT                 = 122;  

        ///<summary>
        ///附魔战斗力
        ///</summary>
        public const int    EVT_ID_FIGHT_FORCE_ENCHANT              = 123;  

        ///<summary>
        ///充值（从当前时刻起， 不算以前的）
        ///</summary>
        public const int    EVT_ID_CHARGE_NEW                       = 124;  

        ///<summary>
        ///导师等级升级事件
        ///</summary>
        public const int    EVT_ID_TUTOR_LEVEL_UP                   = 125;  

        ///<summary>
        ///离开公会
        ///</summary>
        public const int    EVT_ID_GUILD_LEAVE                      = 126;  

        ///<summary>
        ///装备分解
        ///</summary>
        public const int    EVT_ID_EQUIP_DECOMPOSE                  = 127;  

        ///<summary>
        ///幸运转盘
        ///</summary>
        public const int    EVT_ID_LUCKY_TURNTABLE                  = 128;  

        ///<summary>
        ///异界宝箱-决斗场宝箱
        ///</summary>
        public const int    EVT_ID_DUELCHEST                        = 129; 

    

        ///<summary>
        ///------------关卡里事件触发---------------
        ///</summary>
    

        ///<summary>
        ///首杀
        ///</summary>
        public const int    EVT_ID_FIRST_KILL         = 150;  

        ///<summary>
        ///连杀
        ///</summary>
        public const int    EVT_ID_CONTINUE_KILL      = 151;  

        ///<summary>
        ///终止连杀
        ///</summary>
        public const int    EVT_ID_STOP_CONTINUE_KILL = 152;  

        ///<summary>
        ///多杀
        ///</summary>
        public const int    EVT_ID_MULTI_KILL         = 153;  

        ///<summary>
        ///玩家击杀了某个玩家
        ///</summary>
        public const int    EVT_ID_KILL_PLAYER        = 154;  

        ///<summary>
        ///玩家助攻杀人
        ///</summary>
        public const int    EVT_ID_ASSIST_KILL        = 155;  

        ///<summary>
        ///阵营积分
        ///</summary>
        public const int    EVT_ID_CAMP_SCORE         = 160;  

        ///<summary>
        ///pve积分
        ///</summary>
        public const int    EVT_ID_PVE_SCORE          = 161;  

        ///<summary>
        ///pvp积分
        ///</summary>
        public const int    EVT_ID_PVP_SCORE          = 162;  

        ///<summary>
        ///个人积分
        ///</summary>
        public const int    EVT_ID_SELF_SCORE         = 163;  

        ///<summary>
        ///副本内的某个功能计数
        ///</summary>
        public const int    EVT_ID_FUNCTION_SCORE     = 164;  

        ///<summary>
        ///装备熔炼
        ///</summary>
        public const int    EVT_ID_EQUIP_SMELTING     = 165;  

        ///<summary>
        ///装备重铸
        ///</summary>
        public const int    EVT_ID_EQUIP_RECAST       = 166;  

        ///<summary>
        ///聊天
        ///</summary>
        public const int    EVT_ID_CHAT               = 167;  


        ///<summary>
        ///该副本里的所有玩家加载场景完成
        ///</summary>
        public const int    EVT_ID_ALL_PLAYER_LOADED  = 180;  

        ///<summary>
        ///交易中心新物品上架
        ///</summary>
        public const int    EVT_ID_NEW_ITEM_TRADE     = 181;  


        ///<summary>
        ///公会等级
        ///</summary>
        public const int    EVT_ID_GUILD_LEVEL        = 201; 

        ///<summary>
        ///公会资金
        ///</summary>
        public const int    EVT_ID_GUILD_FUND         = 202; 

        ///<summary>
        ///公会人数
        ///</summary>
        public const int    EVT_ID_GUILD_MEMBER_NUM   = 203; 

        ///<summary>
        ///公会战排名 --参数28
        ///</summary>
        public const int    EVT_ID_GUILD_BATTLE_RANK  = 204; 

        ///<summary>
        ///公会物品
        ///</summary>
        public const int    EVT_ID_GUILD_ITEM         = 205; 

        ///<summary>
        ///答题事件
        ///</summary>
        public const int    EVT_ID_QUESTION           = 206; 

        ///<summary>
        ///答题幸运事件
        ///</summary>
        public const int    EVT_ID_QUESTION_LUCKY     = 207; 

        ///<summary>
        ///设置技能专精事件
        ///</summary>
        public const int    EVT_ID_SPELL_PROFICIENT   = 208; 
    

        ///<summary>
        ///EVT_ID_GUILD_BATTLE_RANK        = 210, --公会战排名，公会所有出战的人收到自己公会排第几名的事件。前16。参数28。和204重复。暂时注解这个
        ///</summary>
    

        ///<summary>
        ///公会战排名，骑士长收到自己公会排第几名的事件。前3.参数28
        ///</summary>
        public const int    EVT_ID_GUILD_BATTLE_RANK_MILITARY = 211; 

        ///<summary>
        ///EVT_ID_CHAT_VOICE                 = 212, --语音聊天次数。加上EVT_PARAM_BATCH_CNT这个称号那边自动存盘次数。 和167重复。暂时注解这个。
        ///</summary>
    


        ///<summary>
        ///------------------- 客户端可触发的事件 10001~20000 -----------------
        ///</summary>
    

        ///<summary>
        ///客户端最小编号事件
        ///</summary>
        public const int    EVT_ID_CLIENT_MIN             = 10001; 

        ///<summary>
        ///播放CG
        ///</summary>
        public const int    EVT_ID_CLIENT_PLAY_CG         = 10001; 

        ///<summary>
        ///站在npc旁
        ///</summary>
        public const int    EVT_ID_CLIENT_STAND_BY_NPC    = 10002; 

        ///<summary>
        ///客户端最大编号事件
        ///</summary>
        public const int    EVT_ID_CLIENT_MAX             = 20000; 


        ///<summary>
        ///cg_id
        ///</summary>
        public const int    EVT_PARAM_CG_ID               = 10001; 

        ///<summary>
        ///npc_id
        ///</summary>
        public const int    EVT_PARAM_NPC_ID              = 10002; 



        ///<summary>
        ///-------------------------事件的参数编号--------------------------------
        ///</summary>
    

        ///<summary>
        ///玩家所来自的平台名
        ///</summary>
        public const int    EVT_PARAM_AVATAR_PLAT_NAME  = 0;   

        ///<summary>
        ///怪物id
        ///</summary>
        public const int    EVT_PARAM_MST_ID            = 1;    

        ///<summary>
        ///副本id
        ///</summary>
        public const int    EVT_PARAM_COPY_ID           = 2;    

        ///<summary>
        ///物品id
        ///</summary>
        public const int    EVT_PARAM_ITEM_ID           = 3;    

        ///<summary>
        ///cur_hp/max_hp比值
        ///</summary>
        public const int    EVT_PARAM_HP_PER            = 4;    

        ///<summary>
        ///同个实体同个事件需要间隔的cd执行时间
        ///</summary>
        public const int    EVT_PARAM_CD                = 5;    

        ///<summary>
        ///表示某个enitty的mailbox的字符串
        ///</summary>
        public const int    EVT_PARAM_ENT_MB_STR        = 6;    

        ///<summary>
        ///玩家的dbid
        ///</summary>
        public const int    EVT_PARAM_PLAYER_DBID       = 7;    

        ///<summary>
        ///等级
        ///</summary>
        public const int    EVT_PARAM_LEVEL             = 8;    

        ///<summary>
        ///单次数量
        ///</summary>
        public const int    EVT_PARAM_NUM               = 9;    

        ///<summary>
        ///累计数量(比如累积充值）
        ///</summary>
        public const int    EVT_PARAM_ACCUMULATE_NUM    = 10;   

        ///<summary>
        ///当前所拥有的总量（比如背包里的某个道具总数）
        ///</summary>
        public const int    EVT_PARAM_ALL_NUM           = 11;   

        ///<summary>
        ///VIP等级
        ///</summary>
        public const int    EVT_PARAM_VIP_LEVEL         = 12;   

        ///<summary>
        ///玩家的职业
        ///</summary>
        public const int    EVT_PARAM_VOCATION          = 13;   

        ///<summary>
        ///任务ID
        ///</summary>
        public const int    EVT_PARAM_TASK_ID           = 20;   

        ///<summary>
        ///任务状态
        ///</summary>
        public const int    EVT_PARAM_TASK_STATE        = 21;   

        ///<summary>
        ///buff编号
        ///</summary>
        public const int    EVT_PARAM_BUFF_ID           = 22;   

        ///<summary>
        ///品质
        ///</summary>
        public const int    EVT_PARAM_QUALITY           = 23;   

        ///<summary>
        ///许愿类型
        ///</summary>
        public const int    EVT_PARAM_RUNE_TYPE         = 24;   

        ///<summary>
        ///副本类型
        ///</summary>
        public const int    EVT_PARAM_COPY_TYPE         = 25;   

        ///<summary>
        ///制造类型
        ///</summary>
        public const int    EVT_PARAM_MAKE_TYPE         = 26;   

        ///<summary>
        ///排行榜类型
        ///</summary>
        public const int    EVT_PARAM_RANK_TYPE         = 27;   

        ///<summary>
        ///排名名次
        ///</summary>
        public const int    EVT_PARAM_RANK              = 28;   

        ///<summary>
        ///强化等级
        ///</summary>
        public const int    EVT_PARAM_STRENGTHEN_LEVEL  = 29;   

        ///<summary>
        ///技能编号
        ///</summary>
        public const int    EVT_PARAM_SPELL_ID          = 30;   

        ///<summary>
        ///技能行为编号
        ///</summary>
        public const int    EVT_PARAM_SPELL_ACTION_ID   = 31;   

        ///<summary>
        ///当前星魂页号
        ///</summary>
        public const int    EVT_PARAM_CUR_STAR_SPIRIT_PAGE = 32; 

        ///<summary>
        ///特权卡类型
        ///</summary>
        public const int    EVT_PARAM_CARD_TYPE         = 33;    

        ///<summary>
        ///玩家的实体id
        ///</summary>
        public const int    EVT_PARAM_PLAYER_EID        = 34;    

        ///<summary>
        ///宝石合成等级
        ///</summary>
        public const int    EVT_PARAM_GEM_COMBINE_LEVEL = 35;   

        ///<summary>
        ///装备部位
        ///</summary>
        public const int    EVT_PARAM_EQUIP_POS         = 36;   

        ///<summary>
        ///星级
        ///</summary>
        public const int    EVT_PARAM_STAR              = 37;    

        ///<summary>
        ///途径
        ///</summary>
        public const int    EVT_PARAM_GET_WAY           = 38;   

        ///<summary>
        ///成就id
        ///</summary>
        public const int    EVT_PARAM_ACHIEVEMENT_ID    = 39;   

        ///<summary>
        ///管理器id
        ///</summary>
        public const int    EVT_PARAM_MGR_ID            = 40;   

        ///<summary>
        ///套装等级
        ///</summary>
        public const int    EVT_PARAM_SUIT_LEVEL        = 41;   

        ///<summary>
        ///套装等级
        ///</summary>
        public const int    EVT_PARAM_SUIT_COUNT        = 42;   

        ///<summary>
        ///套装ID
        ///</summary>
        public const int    EVT_PARAM_SUIT_ID           = 43;   


        ///<summary>
        ///玩家1名称
        ///</summary>
        public const int    EVT_PARAM_PLAYER1_NAME      = 50;  

        ///<summary>
        ///玩家2名称
        ///</summary>
        public const int    EVT_PARAM_PLAYER2_NAME      = 51;  

        ///<summary>
        ///攻击者的id
        ///</summary>
        public const int    EVT_PARAM_ATTACKER_ID       = 52;  
    

        ///<summary>
        ///关卡中的行为编号
        ///</summary>
        public const int    EVT_PARAM_SPACE_ACTION_ID   = 70;    

        ///<summary>
        ///活动编号
        ///</summary>
        public const int    EVT_PARAM_ACTIVITY_ID       = 71;    

        ///<summary>
        ///功能id
        ///</summary>
        public const int    EVT_PARAM_FUNCTION_ID       = 72;    

        ///<summary>
        ///积分
        ///</summary>
        public const int    EVT_PARAM_SCORE             = 73;    

        ///<summary>
        ///任务类型
        ///</summary>
        public const int    EVT_PARAM_TASK_TYPE         = 74;    

        ///<summary>
        ///时空裂隙状态
        ///</summary>
        public const int    EVT_PARAM_RIFT_STATE        = 75;    

        ///<summary>
        ///充值是否是代充
        ///</summary>
        public const int    EVT_PARAM_CHARGE_IS_FRIEND  = 76;    

        ///<summary>
        ///章节id
        ///</summary>
        public const int    EVT_PARAM_CHAPTER_ID        = 77;    

        ///<summary>
        ///总战斗力
        ///</summary>
        public const int    EVT_PARAM_SUM_FIGHT_FORCE   = 78;    

        ///<summary>
        ///单独战斗力
        ///</summary>
        public const int    EVT_PARAM_FIGHT_FORCE       = 79;    

        ///<summary>
        ///公会贡献id
        ///</summary>
        public const int    EVT_PARAM_GUILD_CONTRIBUTION_ID     = 80;   

        ///<summary>
        ///公会贡献的份数
        ///</summary>
        public const int    EVT_PARAM_GUILD_CONTRIBUTION_COUNT  = 81;   

        ///<summary>
        ///公会贡献获得贡献点的数量
        ///</summary>
        public const int    EVT_PARAM_GUILD_CONTRIBUTION_NUM    = 82;   

        ///<summary>
        ///导师等级
        ///</summary>
        public const int    EVT_PARAM_TUTOR_LEVEL               = 83;    


        ///<summary>
        ///唯一id，唯一id相同的事件不会重复触发成就
        ///</summary>
        public const int    EVT_PARAM_UNIQUE_ID         = 100;   


        ///<summary>
        ///攻击者类型
        ///</summary>
        public const int    EVT_PARAM_ATTACKER_TYPE           = 120;  

        ///<summary>
        ///被攻击者类型
        ///</summary>
        public const int    EVT_PARAM_HITER_TYPE              = 121;  

        ///<summary>
        ///副本id
        ///</summary>
        public const int    EVT_PARAM_ID_INSTANCE_ID          = 122;  

        ///<summary>
        ///攻击者输出的伤害量
        ///</summary>
        public const int    EVT_PARAM_BE_ATTACK_DAMAGE        = 123;  

        ///<summary>
        ///是否死亡
        ///</summary>
        public const int    EVT_PARAM_IS_DEATH                = 124;  

        ///<summary>
        ///标示归属，服务器怪还是纯客户端怪
        ///</summary>
        public const int    EVT_PARAM_IS_CLIENT_CTRL          = 125;  

        ///<summary>
        ///纯客户端怪时，客户端怪物死亡数量
        ///</summary>
        public const int    EVT_PARAM_ID_DIE_NUM              = 126;  

        ///<summary>
        ///boss部位编号
        ///</summary>
        public const int    EVT_PARAM_MST_BODY_ID             = 127;  

        ///<summary>
        ///阵营编号
        ///</summary>
        public const int    EVT_PARAM_CAMP_ID                 = 128;  

        ///<summary>
        ///pve阵营类型
        ///</summary>
        public const int    EVT_PARAM_PVE_CAMP                = 129;  

        ///<summary>
        ///pvp阵营类型
        ///</summary>
        public const int    EVT_PARAM_PVP_CAMP                = 130;  

        ///<summary>
        ///要塞功能增加
        ///</summary>
        public const int    EVT_PARAM_FORTRESS_FUNCTION_INC   = 131;  

        ///<summary>
        ///要塞功能减少
        ///</summary>
        public const int    EVT_PARAM_FORTRESS_FUNCTION_DEC   = 132;  

        ///<summary>
        ///装备等级
        ///</summary>
        public const int    EVT_PARAM_EQUIP_LEVEL             = 133;  

        ///<summary>
        ///公会id
        ///</summary>
        public const int    EVT_PARAM_GUILD_ID                = 134;  

        ///<summary>
        ///装备熔炼的等级
        ///</summary>
        public const int    EVT_PARAM_EQUIP_SMELTING_LEVEL    = 135;  

        ///<summary>
        ///聊天类型
        ///</summary>
        public const int    EVT_PARAM_CHAT_TYPE               = 136;  

        ///<summary>
        ///野外寻宝积分
        ///</summary>
        public const int    EVT_PARAM_WILD_ID                 = 137;  

        ///<summary>
        ///本次答题排名
        ///</summary>
        public const int    EVT_PARAM_QUESTION_RANK           = 138;  

        ///<summary>
        ///答题是否正确
        ///</summary>
        public const int    EVT_PARAM_QUESTION_IS_RIGHT       = 139;  

        ///<summary>
        ///幸运转盘类型
        ///</summary>
        public const int    EVT_PARAM_LUCKY_TURNTABLE_TYPE    = 140;  

        ///<summary>
        ///道具类型
        ///</summary>
        public const int    EVT_PARAM_ITEM_TYPE               = 141;  

        ///<summary>
        ///EVT_PARAM_GUILD_BATTLE_RANK       = 142,  --公会战排名 210 211 事件用。和28重复。暂时注解这个。
        ///</summary>
    



        ///<summary>
        ///以下编号只给程序用
        ///</summary>
    

        ///<summary>
        ///事件触发的批次
        ///</summary>
        public const int    EVT_PARAM_BATCH_CNT         = 500;  

        ///<summary>
        ///属性table
        ///</summary>
        public const int    EVT_PARAM_ATTRIBUTES        = 501;  

        ///<summary>
        ///是否增加
        ///</summary>
        public const int    EVT_PARAM_IS_ADD            = 502;  

        ///<summary>
        ///玩家身上装备
        ///</summary>
        public const int    EVT_PARAM_EQUIP_IN_BODY     = 503;  

        ///<summary>
        ///玩家替换的时候要穿上的装备
        ///</summary>
        public const int    EVT_PARAM_EQUIP_NEW         = 504;  

        ///<summary>
        ///玩家收取公共邮件的最大编号
        ///</summary>
        public const int    EVT_PARAM_PUB_MAIL_MAX_ID   = 505;  

        ///<summary>
        ///事件达成的次数。如果一个事件包含这个参数，则事件的次数直接等于这个字段
        ///</summary>
        public const int    EVT_PARAM_COND_CNT          = 506;  

        ///<summary>
        ///玩家角色创建时间
        ///</summary>
        public const int    EVT_PARAM_AVATAR_CREATE_TIME   = 507;  


        ///<summary>
        ///------------------------------------------------------------------------------
        ///</summary>
    

        ///<summary>
        ///藏宝图事件，独立编号
        ///</summary>
    

        ///<summary>
        ///奖励事件
        ///</summary>
        public const int    TREASURE_MAP_EVENT_REWARD = 1;           

        ///<summary>
        ///福利事件
        ///</summary>
        public const int    TREASURE_MAP_EVENT_DROP = 2;             

        ///<summary>
        ///boss事件
        ///</summary>
        public const int    TREASURE_MAP_EVENT_BOSS = 3;             

        ///<summary>
        ///传送门事件
        ///</summary>
        public const int    TREASURE_MAP_EVENT_TELEPORT_PORTAL = 4;  

        ///<summary>
        ///异界召唤石
        ///</summary>
        public const int    TREASURE_MAP_EVENT_SUMMON_STONE = 5;     


        ///<summary>
        ///藏宝图事件参数，藏宝图id
        ///</summary>
        public const int    TREASURE_MAP_PARAM_ID = 1;               

        ///<summary>
        ///藏宝图事件参数，藏宝图参数
        ///</summary>
        public const int    TREASURE_MAP_PARAM_ARG = 2;              
}


        ///<summary>
        ///------------------------------------------------------------------------------
        ///</summary>

}
