using System;
using System.Collections.Generic;

namespace Common.ServerConfig {

        ///<summary>
        ///
        ///</summary>


        ///<summary>
        /// User: Piaoshengning
        ///</summary>


        ///<summary>
        /// Func: 功能id定义文件
        ///</summary>


        ///<summary>
        ///
        ///</summary>



        ///<summary>
        ///定义功能id
        ///</summary>

    public static class function_define {
    

        ///<summary>
        ///代币商店
        ///</summary>
        public const int    FUNC_ID_TOKEN_SHOP          = 6;    

        ///<summary>
        ///聊天
        ///</summary>
        public const int    FUNC_ID_CHAT                = 8;    

        ///<summary>
        ///套装
        ///</summary>
        public const int    FUNC_ID_SUIT                = 13;   


        ///<summary>
        ///宠物
        ///</summary>
        public const int    FUNC_ID_PET                 = 1;    

        ///<summary>
        ///宠物,升级
        ///</summary>
        public const int    FUNC_ID_PET_LEVEL_UP        = 1;    

        ///<summary>
        ///宠物,升星
        ///</summary>
        public const int    FUNC_ID_PET_STAR_UP         = 1;    

        ///<summary>
        ///宠物,升品
        ///</summary>
        public const int    FUNC_ID_PET_QUALITY_UP      = 1;    

        ///<summary>
        ///铁匠，强化
        ///</summary>
        public const int    FUNC_ID_STRENGTHEN          = 2;    

        ///<summary>
        ///符文
        ///</summary>
        public const int    FUNC_ID_RUNE                = 3;    

        ///<summary>
        ///星魂
        ///</summary>
        public const int    FUNC_ID_STAR_SPIRIT         = 4;    

        ///<summary>
        ///背包
        ///</summary>
        public const int    FUNC_ID_BAG                 = 5;    

        ///<summary>
        ///商城
        ///</summary>
        public const int    FUNC_ID_MALL                = 6;    

        ///<summary>
        ///副本，剧情本
        ///</summary>
        public const int    FUNC_ID_STORY_COPY          = 7;    

        ///<summary>
        ///野外
        ///</summary>
        public const int    FUNC_ID_SUBURTAN            = 8;    

        ///<summary>
        ///挑战
        ///</summary>
        public const int    FUNC_ID_CHALLENGE           = 9;    

        ///<summary>
        ///奖励系统
        ///</summary>
        public const int    FUNC_ID_AWARD               = 11;   

        ///<summary>
        ///宝石
        ///</summary>
        public const int    FUNC_ID_GEM                 = 13;   

        ///<summary>
        ///任务，主线任务
        ///</summary>
        public const int    FUNC_ID_TASK_MAIN           = 14;   

        ///<summary>
        ///任务，支线任务
        ///</summary>
        public const int    FUNC_ID_TASK_BRANCH         = 14;   

        ///<summary>
        ///排行榜
        ///</summary>
        public const int    FUNC_ID_RANK_LIST           = 15;   

        ///<summary>
        ///设置
        ///</summary>
        public const int    FUNC_ID_SETTING             = 16;   

        ///<summary>
        ///公会
        ///</summary>
        public const int    FUNC_ID_GUILD               = 18;   

        ///<summary>
        ///要塞
        ///</summary>
        public const int    FUNC_ID_FORTRESS            = 19;   

        ///<summary>
        ///制造，培养制造技能
        ///</summary>
        public const int    FUNC_ID_MFG_SKILL           = 20;   

        ///<summary>
        ///制造，培养技能
        ///</summary>
        public const int    FUNC_ID_MFG_PRODUCT         = 20;   

        ///<summary>
        ///交易
        ///</summary>
        public const int    FUNC_ID_TRADE               = 21;   

        ///<summary>
        ///挑战，试炼秘境
        ///</summary>
        public const int    FUNC_ID_TRY_COPY            = 27;   

        ///<summary>
        ///挑战，幻境探险
        ///</summary>
        public const int    FUNC_ID_DREAMLAND           = 29;   

        ///<summary>
        ///组队秘境
        ///</summary>
        public const int    FUNC_ID_TEAM_MISSION        = 30;   

        ///<summary>
        ///VIP
        ///</summary>
        public const int    FUNC_ID_VIP                 = 34;   

        ///<summary>
        ///晚上活动
        ///</summary>
        public const int    FUNC_ID_NIGHT_ACTIVITY      = 37;   
    

        ///<summary>
        ///附魔
        ///</summary>
        public const int    FUNC_ID_ENCHANT             = 202;  


        ///<summary>
        ///每日活跃奖励
        ///</summary>
        public const int    FUNC_ID_AWARD_ACTIVITY      = 1101;     

        ///<summary>
        ///每日奖励
        ///</summary>
        public const int    FUNC_ID_AWARD_DAILY         = 1102;     

        ///<summary>
        ///签到奖励
        ///</summary>
        public const int    FUNC_ID_AWARD_SIGN_IN       = 1103;     

        ///<summary>
        ///vip礼包
        ///</summary>
        public const int    FUNC_ID_VIP_GIFT_BOX        = 1104;     

        ///<summary>
        ///时空裂隙
        ///</summary>
        public const int    FUNC_ID_SPACETIME_RIFT      = 1401;     

        ///<summary>
        ///野外任务
        ///</summary>
        public const int    FUNC_ID_WILD_TASK           = 1404;     

        ///<summary>
        ///好友
        ///</summary>
        public const int    FUNC_ID_FRIEND              = 1701; 

        ///<summary>
        ///组队
        ///</summary>
        public const int    FUNC_ID_TEAM                = 1702; 

        ///<summary>
        ///邮箱
        ///</summary>
        public const int    FUNC_ID_MAIL                = 1703; 

        ///<summary>
        ///技能
        ///</summary>
        public const int    FUNC_ID_SKILL               = 3202; 

        ///<summary>
        ///翅膀
        ///</summary>
        public const int    FUNC_ID_WING                = 3203; 
    

        ///<summary>
        ///称号
        ///</summary>
        public const int    FUNC_ID_TITLE               = 3204;  

        ///<summary>
        ///导师
        ///</summary>
        public const int    FUNC_ID_TUTOR               = 41;    

}

}
