using System;
using System.Collections.Generic;

namespace Common.ServerConfig {

    ///<summary>
    ///定义所有的操作编号,客户端端和服务器端之间以及服务器与服务器之间都会用到这些操作编号
    ///</summary>


    public enum action_config {


    ///<summary>
    ///进入某个传送点
    ///</summary>
    ENTER_TP_POINT = 2,  

    ///<summary>
    ///请求聊天
    ///</summary>
    CHAT_REQ       = 3,  


    ///<summary>
    ///关卡系统对应的操作
    ///</summary>
    

    ///<summary>
    ///进入指定难度的关卡副本
    ///</summary>
    ACTION_ENTER_MISSION                  = 10,         

    ///<summary>
    ///退出关卡副本
    ///</summary>
    ACTION_QUIT_MISSION                   = 11,         

    ///<summary>
    ///关卡开始计时
    ///</summary>
    ACTION_MISSION_START_TIME             = 14,         

    ///<summary>
    ///复活
    ///</summary>
    ACTION_REVIVE                         = 15,         

    ///<summary>
    ///回到王城
    ///</summary>
    ACTION_GO_TO_INIT_MAP                 = 16,         

    ///<summary>
    ///取消关卡的匹配
    ///</summary>
    ACTION_MISSION_CANCEL_MATCH           = 17,         

    ///<summary>
    ///获取某个场景所有分线状态
    ///</summary>
    ACTION_MISSION_GET_LINSE_STATE        = 18,         

    ///<summary>
    ///切换到某个场景的指定分线
    ///</summary>
    ACTION_MISSION_SWITCH_TO_LINE         = 19,         

    ///<summary>
    ///客户端请求重置玩家的挑战次数
    ///</summary>
    ACTION_RESET_MISSION_TIMES            = 20,        

    ///<summary>
    ///客户端通知服务器指定刷怪点开始刷怪
    ///</summary>
    ACTION_SPAWNPOINT_START               = 21,        

    ///<summary>
    ///客户端通知服务器指定刷怪点停止刷怪
    ///</summary>
    ACTION_SPAWNPOINT_STOP                = 22,        

    ///<summary>
    ///客户端设置的副本关卡状态，服务器无须理解其格式
    ///</summary>
    ACTION_CLIENT_MISSION_INFO            = 24,        

    ///<summary>
    ///扫荡指定难度的关卡副本
    ///</summary>
    ACTION_MOP_UP_MISSION                 = 25,        

    ///<summary>
    ///副本胜利加好友度
    ///</summary>
    ACTION_ADD_FRIEND_DEGREE              = 26,        

    ///<summary>
    ///客户端上传连击数
    ///</summary>
    ACTION_UPLOAD_COMBO                   = 27,        
    ACTION_PICKUP_DROP                    = 28,
    ACTION_GET_MISSION_REWARD             = 29,

    ///<summary>
    ///服务器通知客户端某刷怪点怪物全部死亡
    ///</summary>
    ACTION_MISSION_ON_ALL_MONSTER_DIE     = 30,        

    ///<summary>
    ///服务器通知客户端副本取得胜利
    ///</summary>
    ACTION_MISSION_WIN                    = 31,        

    ///<summary>
    ///服务器通知玩家正在匹配某个副本
    ///</summary>
    ACTION_MISSION_MATCHING               = 32,        

    ///<summary>
    ///客户端本死亡。用于计算复活相关。
    ///</summary>
    ACTION_DIE_CLIENT                     = 33,         


    ///<summary>
    ///客户端请求获取已挑战次数
    ///</summary>
    ACTION_GET_MISSION_TIMES              = 41,         
    ACTION_GET_MISSION_STARS              = 42,

    ///<summary>
    ///客户端请求已完成的副本关卡信息
    ///</summary>
    ACTION_GET_MISSION_FINISHED           = 43,        

    ///<summary>
    ///客户端获取已经拿到的关卡副本宝箱奖励
    ///</summary>
    ACTION_GET_MISSION_REWARDS            = 44,        

    ///<summary>
    ///客户端获取已复活次数
    ///</summary>
    ACTION_GET_REVIVE_TIMES               = 45,        
    ACTION_GET_MISSION_SW_REWARD          = 46,
    ACTION_GET_RESET_TIMES                = 47,
    ACTION_GET_MOP_UP_TIMES               = 49,
    ACTION_GET_MISSION_RECORD             = 50,
    ACTION_GET_PROP_DROPS                 = 51,
    ACTION_GET_RANDOM_REWARD              = 52,

    ///<summary>
    ///更新客户端副本信息  
    ///</summary>
    ACTION_UPDATE_CLIENT_MISSION_INFO     = 53,         

    ///<summary>
    ///更新服务器副本信息  
    ///</summary>
    ACTION_UPDATE_SERVER_MISSION_INFO     = 54,         

    ///<summary>
    ///完成客户端副本请求
    ///</summary>
    ACTION_FINISH_CLIENT_MISSION          = 55,         

    ///<summary>
    ///获取星魂奖励
    ///</summary>
    ACTION_GET_STAR_SPIRIT_REWARD         = 56,         

    ///<summary>
    ///获取章节记录
    ///</summary>
    ACTION_GET_CHAPTER_RECORD             = 57,         

    ///<summary>
    ///客户端获取副本掉落列表
    ///</summary>
    ACTION_GET_MISSION_DROPS              = 58,         

    ///<summary>
    ///客户端捡取副本掉落
    ///</summary>
    ACTION_PICK_MISSION_DROP              = 59,         

    ///<summary>
    ///获取指定章节记录
    ///</summary>
    GET_APPOINT_CHAPTER_RECORD            = 60,         

    ///<summary>
    ///根据副本类型购买次数
    ///</summary>
    ACTION_MISSION_TYPE_BUY_TIMES         = 61,         

    ///<summary>
    ///完成客户端副本失败请求
    ///</summary>
    ACTION_FINISH_CLIENT_MISSION_LOSE     = 62,         

    ///<summary>
    ///更新pvp/pve副本信息
    ///</summary>
    ACTION_UPDATE_PVP_PVE_MISSION_INFO    = 63,         

    ///<summary>
    ///获取副本重置记录
    ///</summary>
    ACTION_MISSION_GET_RESET_TIMES_RECORD = 64,         

    ///<summary>
    ///记录副本的入口
    ///</summary>
    ACTION_MARK_PORTAL                    = 65,         

    ///<summary>
    ///删除副本的入口
    ///</summary>
    ACTION_DEL_PORTAL                     = 66,         

    ///<summary>
    ///玩家准备好pvp
    ///</summary>
    ACTION_PLAYER_PREPARED                = 67,         

    ///<summary>
    ///询问能否再来一局
    ///</summary>
    ACTION_CAN_PLAY_AGAIN                 = 68,         

    ///<summary>
    ///玩家回应是否同意再来一局
    ///</summary>
    ACTION_AGREE_PLAY_AGAIN               = 69,         

    ///<summary>
    ///请求赠送队长奖励
    ///</summary>
    ACTION_CAPTAIN_TRANSFER_REWARD        = 70,         

    ///<summary>
    ///请求赠送奖励信息
    ///</summary>
    ACTION_CAPTAIN_TRANSFER_INFO_REQ      = 71,         

    ///<summary>
    ///副本统计信息
    ///</summary>
    ACTION_MISSION_STASTISTIC_INFO        = 72,         

    ///<summary>
    ///新纪录主动下发
    ///</summary>
    ACTION_CAPTAIN_TRANSFER_NEW_REQ       = 73,         


    ///<summary>
    ///请求传送
    ///</summary>
    ACTION_TELEPORT                       = 80,         

    ///<summary>
    ///争夺型传送门
    ///</summary>
    ACTION_FIGHT_TRANS_PORTAL_INFO        = 81,         

    ///<summary>
    ///客户端实体,同步到服务器的事件
    ///</summary>
    

    ///<summary>
    ///客户端实体死亡，客户端通知client mst被打死
    ///</summary>
    ACTION_CLIENT_ENTITY_DIE              = 85,  

    ///<summary>
    ///主角被dummy打扣血，客户端同步avatar血量
    ///</summary>
    ACTION_CLIENT_ENTITY_HITAVATAR        = 86,  

    ///<summary>
    ///客户端实体移动
    ///</summary>
    ACTION_CLIENT_ENTITY_MOVE             = 87,  
    
    

    ///<summary>
    ///---------------副本行为相关的操作码---------------------
    ///</summary>
    

    ///<summary>
    ///触发某个副本行为的next_actions
    ///</summary>
    ACTION_TRIGGER_NEXT_ACTIONS          = 150,  
    
    

    ///<summary>
    ///-----------------拍卖系统的action定义-----------------------
    ///</summary>
    

    ///<summary>
    ///将物品放入拍卖行
    ///</summary>
    ACTION_AUCTION_SELL  = 200,        

    ///<summary>
    ///将物品从拍卖行下架
    ///</summary>
    ACTION_AUCTION_SELL_CANCEL = 201,  

    ///<summary>
    ///从拍卖行购买物品
    ///</summary>
    ACTION_AUCTION_BUY = 202,          

    ///<summary>
    ///从拍卖行提取货币
    ///</summary>
    ACTION_AUCTION_TAKEOUT_MONEY = 203,   

    ///<summary>
    ///查询自己在拍卖行里的物品
    ///</summary>
    ACTION_AUCTION_QUERY_SELF_SELL = 204,  

    ///<summary>
    ///搜索拍卖行里的物品
    ///</summary>
    ACTION_AUCTION_SEARCH = 205,       

    ///<summary>
    ///扩展格子
    ///</summary>
    ACTION_AUCTION_EXTEND_GRID = 206,  

    ///<summary>
    ///将已过期的物品重新上架
    ///</summary>
    ACTION_AUCTION_RESELL_ITEM = 207,  

    ///<summary>
    ///查询拍卖行全局信息
    ///</summary>
    ACTION_AUCTION_QUERY_GLOBAL_INFO = 208,  

    ///<summary>
    ///查询某物品今日已出售数量
    ///</summary>
    ACTION_AUCTION_ITEM_SELLED_COUNT = 209,  

    ///<summary>
    ///查询某物品今日已购买数量
    ///</summary>
    ACTION_AUCTION_ITEM_BUYED_COUNT = 210,   

    ///<summary>
    ///未提取的货币信息
    ///</summary>
    ACTION_AUCTION_LEFT_MONEY = 211,   

    ///<summary>
    ///查询购买记录
    ///</summary>
    ACTION_AUCTION_BUY_RECORD = 212,   

    ///<summary>
    ///查询出售记录
    ///</summary>
    ACTION_AUCTION_SELL_RECORD = 213,  

    ///<summary>
    ///获取自己的完整求购物品信息
    ///</summary>
    ACTION_AUCTION_GET_WANT_BUY_INFO = 214, 

    ///<summary>
    ///修改玩家的求购信息（增加或删除）
    ///</summary>
    ACTION_AUCTION_CHANGE_WANT_BUY = 215,   

    ///<summary>
    ///获取物品的求购人数
    ///</summary>
    ACTION_AUCTION_GET_ITEMS_WANT_BUY = 216, 


    ///<summary>
    ///--------------------300~350好友系统相关操作----------------------
    ///</summary>
    

    ///<summary>
    ///好友管理器查询的id，获取某个好友信息
    ///</summary>
    FRIEND_GET_P_INFO                = 300,   

    ///<summary>
    ///好友管理器查询的id，获取好友列表
    ///</summary>
    FRIEND_GET_PS_INFO               = 301,   

    ///<summary>
    ///接受好友添加请求
    ///</summary>
    FRIEND_ACCEPT                    = 302,   

    ///<summary>
    ///请求好友添加请求
    ///</summary>
    FRIEND_ADD                       = 303,   

    ///<summary>
    ///好友删除请求
    ///</summary>
    FRIEND_DEL                       = 304,   

    ///<summary>
    ///离线好友消息
    ///</summary>
    OFFLINE_FRIEND_ADD               = 305,   

    ///<summary>
    ///玩家查找
    ///</summary>
    FRIEND_SEARCH                    = 306,   

    ///<summary>
    ///拒绝对方成为自己的好友
    ///</summary>
    FRIEND_REFUSE                    = 307,   

    ///<summary>
    ///赠送体力并邀请好友送我体力
    ///</summary>
    FRIEND_GIVE_INVITE               = 308,   

    ///<summary>
    ///接受对方邀请并赠送体力
    ///</summary>
    FRIEND_GIVE_ACCEPT               = 309,   

    ///<summary>
    ///好友互赠亲密值道具
    ///</summary>
    FRIEND_GIVE_GOODS                = 310,   

    ///<summary>
    ///拿到发送列表 ({dbid,num})
    ///</summary>
    FRIEND_GET_SEND_LIST             = 311,   

    ///<summary>
    ///拿到接收列表 ({dbid,num})
    ///</summary>
    FRIEND_GET_RECEIVE_LIST          = 312,   

    ///<summary>
    ///拿到对应玩家的详细信息
    ///</summary>
    FRIEND_GET_PLAYER_DETAILED_INFO  = 313,   

    ///<summary>
    ///领取祝福奖励
    ///</summary>
    FRIEND_GET_BLESS_REWARD          = 314,   

    ///<summary>
    ///批量领取祝福奖励
    ///</summary>
    FRIEND_BATCH_GET_BLESS_REWARD    = 315,   

    ///<summary>
    ///拿到已经领取的列表
    ///</summary>
    FRIEND_GET_BLESS_LIST            = 316,   

    ///<summary>
    ///被添加好友
    ///</summary>
    FRIEND_ADDED                     = 317,   

    ///<summary>
    ///被删除好友   
    ///</summary>
    FRIEND_DELED                     = 318,   

    ///<summary>
    ///被接受添加好友
    ///</summary>
    FRIEND_ACCEPTED                  = 319,   

    ///<summary>
    ///被拒绝好友  
    ///</summary>
    FRIEND_REFUSEED                  = 320,   

    ///<summary>
    ///亲密度事件
    ///</summary>
    FRIEND_INTIMATE_EVENT            = 321,   

    ///<summary>
    ///被赠送体力祝福
    ///</summary>
    FRIEND_GIVE_ACCEPTED             = 322,   

    ///<summary>
    ///购买并赠送
    ///</summary>
    FRIEND_BUY_AND_GIVE_GOODS        = 323,   

    ///<summary>
    ///被赠送亲密值道具
    ///</summary>
    FRIEND_GIVE_GOODS_ACCEPTED       = 324,   

    ///<summary>
    ///拿到在线玩家列表
    ///</summary>
    FRIEND_GET_ONLINE_LIST           = 325,   

    ///<summary>
    ///拿到亲密值事件列表
    ///</summary>
    FRIEND_GET_INTIMATE_EVENT_LIST   = 326,   

    ///<summary>
    ///好友上线下线通知
    ///</summary>
    FRIEND_STATE_NOTICE              = 327,   

    ///<summary>
    ///被添加好友列表（待审批）
    ///</summary>
    FRIEND_ADDED_LIST                = 328,   

    ///<summary>
    ///同步亲密值给客户端  亲密值改变
    ///</summary>
    FRIEND_GET_FRIEND_DEGREE         = 329,   

    ///<summary>
    ///获取好友推荐
    ///</summary>
    FRIEND_GET_RECOMMEND             = 330,   


    ///<summary>
    ///----------------------500~600公会系统对应操作-----------------------------
    ///</summary>
    

    ///<summary>
    ///获取公会列表
    ///</summary>
    GET_GUILD_LIST                        = 500,    

    ///<summary>
    ///获取公会成员列表
    ///</summary>
    GUILD_GET_MEMBERS_LIST                = 501,    

    ///<summary>
    ///申请加入公会
    ///</summary>
    GUILD_APPLY_JOIN                      = 502,    

    ///<summary>
    ///请求离开公会
    ///</summary>
    GUILD_LEAVE                           = 503,    

    ///<summary>
    ///创建公会
    ///</summary>
    GUILD_CREATE                          = 504,    

    ///<summary>
    ///设置公会宣言
    ///</summary>
    GUILD_SET_MANIFESTO                   = 507,    

    ///<summary>
    ///获取公会某个成员信息
    ///</summary>
    GUILD_GET_MEMBER_INFO                 = 508,    

    ///<summary>
    ///请求申请加入公会消息（审批入会）列表
    ///</summary>
    GUILD_APPLY_JOIN_LIST                 = 509,    

    ///<summary>
    ///同意加入公会
    ///</summary>
    GUILD_ACCEPT_APPLY                    = 510,    

    ///<summary>
    ///拒绝加入公会
    ///</summary>
    GUILD_REFUSE_APPLY                    = 511,    

    ///<summary>
    ///取消加入公会请求
    ///</summary>
    GUILD_CANCEL_JOIN                     = 512,    

    ///<summary>
    ///搜索某个公会
    ///</summary>
    GUILD_SEARCH                          = 513,    

    ///<summary>
    ///拿到自己公会的详细信息
    ///</summary>
    GUILD_GET_MY_GUILD_INFO               = 514,    

    ///<summary>
    ///学习新技能
    ///</summary>
    GUILD_STUDY_SKILL                     = 515,    

    ///<summary>
    ///获得技能列表
    ///</summary>
    GUILD_GET_SKILL_LIST                  = 516,    

    ///<summary>
    ///拿到我最近申请加入公会列表
    ///</summary>
    GUILD_GET_NEAR_JOIN_LIST              = 517,    

    ///<summary>
    ///获得成就列表
    ///</summary>
    GUILD_GET_ACHIEVEMENT_LIST            = 518,    

    ///<summary>
    ///成就通知
    ///</summary>
    GUILD_ACHIEVEMENT_NOTIFY              = 519,    

    ///<summary>
    ///成就领奖
    ///</summary>
    GUILD_GET_ACHIEVEMENT_REWARD          = 520,    

    ///<summary>
    ///上缴物品换取贡献值
    ///</summary>
    GUILD_CONTRIBUTION_HAND_IN            = 521,    

    ///<summary>
    ///工会升级
    ///</summary>
    GUILD_LEVEL_UP                        = 522,    

    ///<summary>
    ///任免职位
    ///</summary>
    GUILD_APPOINT_POSITION                = 523,    

    ///<summary>
    ///开除成员
    ///</summary>
    GUILD_FIRE_MEMBER                     = 524,    

    ///<summary>
    ///发布群公告
    ///</summary>
    GUILD_SEND_NOTICE                     = 525,    

    ///<summary>
    ///拿到群公告列表
    ///</summary>
    GUILD_GET_NOTICE_LIST                 = 526,    

    ///<summary>
    ///工会物品变化通知
    ///</summary>
    GUILD_ITEM_NOTIFY                     = 527,    

    ///<summary>
    ///工会贡献通知
    ///</summary>
    GUILD_PLAYER_CONTRIBUTION_NOTIFY      = 528,    

    ///<summary>
    ///工会记录列表请求
    ///</summary>
    GUILD_GET_EVENT_RECORD_LIST           = 529,    

    ///<summary>
    ///工会繁荣通知
    ///</summary>
    GUILD_BOOMING_NOTIFY                  = 530,    

    ///<summary>
    ///拿到公会名字
    ///</summary>
    GUILD_GET_GUILD_NAME                  = 531,    

    ///<summary>
    ///会长职位转让
    ///</summary>
    GUILD_LEADER_POSITION_TRANSFER        = 532,    

    ///<summary>
    ///获得公会新的通知信息
    ///</summary>
    GUILD_GET_NEW_NOTICE                  = 533,    

    ///<summary>
    ///邀请加入公会
    ///</summary>
    GUILD_INVITE_JOIN                     = 534,    

    ///<summary>
    ///同意邀请入公
    ///</summary>
    GUILD_ACCEPT_INVITE                   = 535,    

    ///<summary>
    ///公会成员上线下线通知
    ///</summary>
    GUILD_STATE_NOTICE                    = 536,    

    ///<summary>
    ///获取公会战概况(第几天，什么阶段)
    ///</summary>
    GUILD_BATTLE_INFO                     = 537,    

    ///<summary>
    ///获取玩家个人的公会战试炼概况
    ///</summary>
    GUILD_BATTLE_EXP_INFO                 = 538,    

    ///<summary>
    ///获取试炼场排行榜的数据
    ///</summary>
    GUILD_BATTLE_EXP_RANK                 = 539,    

    ///<summary>
    ///获取试炼场记录
    ///</summary>
    GUILD_BATTLE_LOG                      = 540,    

    ///<summary>
    ///玩家上线奖励
    ///</summary>
    GUILD_ONLINE_REWARD                   = 541,    
    

    ///<summary>
    ///-base部分
    ///</summary>
    

    ///<summary>
    ///获得新工会技能
    ///</summary>
    GUILD_NEW_SKILL                       = 542,    

    ///<summary>
    ///玩家上线后为玩家添加技能
    ///</summary>
    GUILD_ADD_SKILL                       = 543,    

    ///<summary>
    ///玩家上线后为玩家添加任务
    ///</summary>
    GUILD_ADD_TASK                        = 544,    

    ///<summary>
    ///玩家离开公会后删除任务
    ///</summary>
    GUILD_CLEAR_TASK                      = 545,    

    ///<summary>
    ///删除公会技能
    ///</summary>
    GUILD_REMOVE_SKILL                    = 546,    

    ///<summary>
    ///更新上线奖励时间
    ///</summary>
    GUILD_UPDATE_ONLINE_REWARD_TIME       = 547,    

    ///<summary>
    ///公会战淘汰赛结果
    ///</summary>
    GUILD_BATTLE_RESULT                   = 548,    


    ///<summary>
    ///获取公会战积分相关
    ///</summary>
    GUILD_BATTLE_WEEK_RANK                = 550,    

    ///<summary>
    ///报名参加公会战
    ///</summary>
    GUILD_BATTLE_SIGNUP                   = 551,    

    ///<summary>
    ///抽签
    ///</summary>
    GUILD_BATTLE_DRAW                     = 552,    

    ///<summary>
    ///获取抽签结果
    ///</summary>
    GUILD_BATTLE_GET_DRAW_RESULT          = 553,    

    ///<summary>
    ///获取已报名公会战的公会信息
    ///</summary>
    GUILD_BATTLE_GET_SIGNUPED_RANK        = 554,    

    ///<summary>
    ///更改军衔
    ///</summary>
    GUILD_BATTLE_CHANGE_MILITARY          = 555,    

    ///<summary>
    ///获取公会战抽签信息
    ///</summary>
    GUILD_BATTLE_GET_DRAW_INFO            = 556,    

    ///<summary>
    ///进入公会战场景
    ///</summary>
    GUILD_BATTLE_ENTER                    = 557,    

    ///<summary>
    ///获取当前公会战小组赛战绩
    ///</summary>
    GUILD_BATTLE_GROUP_MATCH_INFO         = 558,    

    ///<summary>
    ///获取淘汰赛的当前战绩
    ///</summary>
    GUILD_BATTLE_FINAL_MATCH_INFO         = 559,    

    ///<summary>
    ///获取公会战的倒数功能
    ///</summary>
    GUILD_BATTLE_MATCH_COUNTDOWN_INFO     = 560,    

    ///<summary>
    ///获取公会指定赛季的战记
    ///</summary>
    GUILD_BATTLE_GET_LOG                  = 561,    

    ///<summary>
    ///获取公会指定赛季的结果
    ///</summary>
    GUILD_BATTLE_GET_RESULT_LOG           = 562,    

    ///<summary>
    ///获取若干个赛季的结果
    ///</summary>
    GUILD_BATTLE_GET_RESULT_LOGS          = 563,    

    ///<summary>
    ///获取公会guild_battle_log_dbid中对应的pk记录。(已确定是第几届哪个类型哪一轮，里面的n场，当然是自己guild_id对应的所有记录)
    ///</summary>
    GUILD_BATTLE_GET_PK_LOG               = 564,    

    ///<summary>
    ///获取其他公会成员列表
    ///</summary>
    GUILD_GET_OTHER_MEMBERS_LIST          = 565,    

    ///<summary>
    ///拿到玩家的军衔
    ///</summary>
    GUILD_GET_GUILD_MILITARY              = 566,    

    ///<summary>
    ///获取当前届公会战记
    ///</summary>
    GUILD_BATTLE_GET_GROUP_MATCH_LOG      = 567,    

    ///<summary>
    ///公会战召集
    ///</summary>
    GUILD_BATTLE_CALL                     = 568,    

    ///<summary>
    ///进入公会boss场景
    ///</summary>
    GUILD_BOSS_GOTO                       = 569,    


    ///<summary>
    ///获取公会商店信息
    ///</summary>
    GUILD_SHOP_GET_INFO                   = 570,    

    ///<summary>
    ///公会管理员从商店购买到仓库
    ///</summary>
    GUILD_SHOP_BUY                        = 571,    

    ///<summary>
    ///公会成员从仓库兑换商品
    ///</summary>
    GUILD_SHOP_EXCHANGE                   = 572,    

    ///<summary>
    ///公会成员给商店商品点赞
    ///</summary>
    GUILD_SHOP_PRAISE                     = 573,    

    ///<summary>
    ///公会成员取消点赞
    ///</summary>
    GUILD_SHOP_DISPRAISE                  = 574,    

    ///<summary>
    ///获取其他成员dbid的个人公会战记
    ///</summary>
    GUILD_BATTLE_GET_OTHER_PK_LOG         = 575,    

    ///<summary>
    ///向会长（在线，有权限的人）请求需要补货
    ///</summary>
    GUILD_SHOP_REQUEST                    = 576,    

    ///<summary>
    ///新会员的聊天提醒的会长信息查询
    ///</summary>
    GUILD_NOOB_CHAT_NOTICE_LEADER_INFO    = 577,    

    ///<summary>
    ///接受公会任务
    ///</summary>
    GUILD_ACCEPT_TASK                     = 578,    

    ///<summary>
    ///放弃公会任务
    ///</summary>
    GUILD_ABANDON_TASK                    = 579,    

    ///<summary>
    ///查询公会任务
    ///</summary>
    GUILD_QUERY_TASK                      = 580,    

    ///<summary>
    ///查询公会任务进度
    ///</summary>
    GUILD_QUERY_TASK_REWARD               = 581,    

    ///<summary>
    ///领取公会任务进度奖励
    ///</summary>
    GUILD_GET_TASK_REWARD                 = 582,    

    ///<summary>
    ///获取公会任务积分排行榜
    ///</summary>
    GUILD_TASK_RANK_REQ                   = 583,    


    ///<summary>
    ///----------------------601~650附魔相关行为------------------------------
    ///</summary>
    

    ///<summary>
    ///附魔操作
    ///</summary>
    ENCHANT                               = 601,    

    ///<summary>
    ///附魔替换
    ///</summary>
    ENCHANT_REPLACE                       = 602,    

    ///<summary>
    ///获取附魔记录
    ///</summary>
    ENCHANT_GET_RECORD                    = 603,    

    ///<summary>
    ///----------------------700~750宝石相关行为------------------------------
    ///</summary>
    

    ///<summary>
    ///在背包中合成宝石
    ///</summary>
    GEM_COMBINE                               = 700,    

    ///<summary>
    ///在装备中合成宝石
    ///</summary>
    GEM_COMBINE_IN_EQUIP                      = 701,    

    ///<summary>
    ///在装备中合成宝石
    ///</summary>
    GEM_COMBINE_IN_EQUIP_ANYWAY               = 702,    

    ///<summary>
    ///在背包中合成宝石
    ///</summary>
    GEM_COMBINE_IN_BAG_ANYWAY                 = 703,    

    ///<summary>
    ///装备打孔请求
    ///</summary>
    GEM_EQUIP_PUNCHING                        = 704,    

    ///<summary>
    ///镶嵌请求
    ///</summary>
    GEM_INLAY                                 = 705,    

    ///<summary>
    ///镶嵌请求
    ///</summary>
    GEM_INLAY_TO_SLOT                         = 706,    

    ///<summary>
    ///取下宝石的请求
    ///</summary>
    GEM_OUTLAY                                = 707,    

    ///<summary>
    ///宝石封灵1次请求
    ///</summary>
    GEM_SPIRIT                                = 708,    

    ///<summary>
    ///宝石封灵10次请求
    ///</summary>
    GEM_SPIRIT_TEN                            = 709,    

    ///<summary>
    ///镶嵌宝石，直接用钻石买
    ///</summary>
    GEM_INLAY_BUY_GEM                         = 710,    


    ///<summary>
    ///--------------------751~800邮件相关行为-------------------------------
    ///</summary>
    

    ///<summary>
    ///请求邮件信息
    ///</summary>
    MAIL_INFO_REQ                                = 751,    

    ///<summary>
    ///请求读邮件
    ///</summary>
    MAIL_READ_REQ                                = 752,    

    ///<summary>
    ///请求删除邮件
    ///</summary>
    MAIL_DELETE_REQ                              = 753,    

    ///<summary>
    ///请求邮件附件
    ///</summary>
    MAIL_GET_ATTACHMENT_REQ                      = 754,    

    ///<summary>
    ///请求添加邮件的附件到背包
    ///</summary>
    MAIL_ADD_ATTACHMENT_BAG_REQ                  = 755,    

    ///<summary>
    ///设置avatar最大的邮件id
    ///</summary>
    MAIL_SET_MAX_MAIL_ID_REQ                     = 756,    

    ///<summary>
    ///申请获取邮件附件向avatar获取数据
    ///</summary>
    MAIL_GET_ATTACHMENT_BY_AVATAR_REQ            = 757,    

    ///<summary>
    ///    MAIL_SETN_SYS_MAIL_REQ                       = 758,    --发送系统邮件
    ///</summary>


    ///<summary>
    ///新邮件回复  
    ///</summary>
    MAIL_NEW_MAIL_RESP                           = 759,    

    ///<summary>
    ///请求发送邮件
    ///</summary>
    MAIL_SEND_MAIL_REQ                           = 760,    

    ///<summary>
    ///发送公共邮件
    ///</summary>
    MAIL_SEND_PUBLIC_MAIL_REQ                    = 761,    

    ///<summary>
    ///添加公共邮件
    ///</summary>
    MAIL_ON_ADD_NEW_PUBLIC_MAIL                  = 762,    

    ///<summary>
    ///邮件批量删除
    ///</summary>
    MAIL_BATCH_DELETE_REQ                        = 763,    

    ///<summary>
    ///公共邮件通知
    ///</summary>
    MAIL_PUBLIC_MAIL_NOTIFICATION                = 764,    

    ///<summary>
    ///批量获取附件
    ///</summary>
    MAIL_BATCH_GET_ATTACHMENT_REQ                = 765,    

    ///<summary>
    ///申请批量获取邮件附件向avatar获取数据
    ///</summary>
    MAIL_BATCH_GET_ATTACHMENT_BY_AVATAR_REQ      = 766,    
    
    

    ///<summary>
    ///--------------------801~850聊天系统相关行为-------------------------------
    ///</summary>
    

    ///<summary>
    ///请求屏蔽玩家发言
    ///</summary>
    CHAT_SHIELD_REQ                              = 801,    

    ///<summary>
    ///请求取消屏蔽玩家发言
    ///</summary>
    CHAT_CANCEL_SHIELD_REQ                       = 802,    

    ///<summary>
    ///举报玩家
    ///</summary>
    CHAT_REPORT_REQ                              = 803,    

    ///<summary>
    ///最近聊天列表
    ///</summary>
    CHAT_NEAR_LIST                               = 804,    

    ///<summary>
    ///请求我屏蔽了哪些人
    ///</summary>
    CHAT_SHIELD_LIST                             = 805,    

    ///<summary>
    ///聊天频道操作码返回
    ///</summary>
    CHAT_CHANNEL                                 = 806,    

    ///<summary>
    ///伪造私聊
    ///</summary>
    CHAT_FORGE_PRIVATE                           = 807,    



    ///<summary>
    ///--------------------851~899强化系统系统相关行为-------------------------------
    ///</summary>
    

    ///<summary>
    ///强化装备
    ///</summary>
    STRENGTHEN_REQ                              = 851,       

    ///<summary>
    ///装备信息查询
    ///</summary>
    STRENGTHEN_QUERY_REQ                        = 852,       

    ///<summary>
    ///装备信息列表查询
    ///</summary>
    STRENGTHEN_QUERY_LIST_REQ                   = 853,       

    ///<summary>
    ///一键强化
    ///</summary>
    STRENGTHEN_ONE_KEY_REQ                      = 854,       

    ///<summary>
    ///套装
    ///</summary>
    SUIT_REQ                                    = 861,       
    

    ///<summary>
    ///-------------------活动相关的行为定义-------------------------------
    ///</summary>
    

    ///<summary>
    ///获取活动剩余次数
    ///</summary>
    ACTION_ACTIVITY_GET_LEFT_COUNT           =  900,      

    ///<summary>
    ///参加活动
    ///</summary>
    ACTION_ACTIVITY_JOIN                     =  901,      



    ///<summary>
    ///----------------------1000~1100物品相关行为------------------------------
    ///</summary>
    

    ///<summary>
    ///移动物品
    ///</summary>
    ITEM_MOVE                             = 1000,   

    ///<summary>
    ///使用物品
    ///</summary>
    ITEM_USE                              = 1001,   

    ///<summary>
    ///整理背包
    ///</summary>
    SORT_PKG                              = 1002,   

    ///<summary>
    ///购买物品
    ///</summary>
    ITEM_BUY                              = 1003,   

    ///<summary>
    ///出售物品
    ///</summary>
    ITEM_SELL                             = 1004,   

    ///<summary>
    ///赎回物品
    ///</summary>
    ITEM_REDEEM                           = 1005,   

    ///<summary>
    ///获取背包数据
    ///</summary>
    ITEM_GET_PKG_DATA                     = 1006,   

    ///<summary>
    ///扩展背包   
    ///</summary>
    EXPAND_BAG                            = 1007,   


    ///<summary>
    ///合成宝石
    ///</summary>
    ITEM_COMBINE                          = 1008,    

    ///<summary>
    ///镶嵌宝石
    ///</summary>
    ITEM_INLAY                            = 1009,    

    ///<summary>
    ///取下宝石
    ///</summary>
    ITEM_OUTLAY                           = 1010,    


    ///<summary>
    ///物品补偿查询
    ///</summary>
    ITEM_COMPENSATE_QUERY_INFO            = 1011,    


    ///<summary>
    ///----------------------1101~1200任务相关行为------------------------------
    ///</summary>
    

    ///<summary>
    ///接受任务
    ///</summary>
    TASK_ACCEPT                           = 1101,   

    ///<summary>
    ///完成任务
    ///</summary>
    TASK_COMPLETE                         = 1102,   

    ///<summary>
    ///放弃任务
    ///</summary>
    TASK_DISCARD                          = 1103,   

    ///<summary>
    ///获得任务列表
    ///</summary>
    TASK_GETLIST                          = 1104,   

    ///<summary>
    ///获得已完成任务列表
    ///</summary>
    TASK_GETCOMPLETED                     = 1105,   

    ///<summary>
    ///提交道具
    ///</summary>
    TASK_HAND_IN                          = 1106,   


    ///<summary>
    ///----------------------1201~1300装备相关行为------------------------------
    ///</summary>
    

    ///<summary>
    ///穿装备
    ///</summary>
    EQUIP_WEAR                            = 1201,   

    ///<summary>
    ///换装备  
    ///</summary>
    EQUIP_CHANGE                          = 1202,   

    ///<summary>
    ///卸装备
    ///</summary>
    EQUIP_TAKE_OFF                        = 1203,   

    ///<summary>
    ///分解装备
    ///</summary>
    EQUIP_DECOMPOSE                       = 1204,   

    ///<summary>
    ///装备打孔
    ///</summary>
    EQUIP_PUNCHING                        = 1205,   

    ///<summary>
    ///分解装备多个
    ///</summary>
    EQUIP_DECOMPOSE_LIST                  = 1206,   

    ///<summary>
    ///装备重铸
    ///</summary>
    EQUIP_RECAST_REQ                      = 1207,   

    ///<summary>
    ///装备重铸确认
    ///</summary>
    EQUIP_RECAST_CONFIRM_REQ              = 1208,   

    ///<summary>
    ///装备重铸查询
    ///</summary>
    EQUIP_RECAST_GET_INFO_REQ             = 1209,   

    ///<summary>
    ///重铸到buff
    ///</summary>
    EQUIP_RECAST_BUFF_NOTICE              = 1210,   


    ///<summary>
    ///----------------------1301~1400组队相关的行为-------------------------------------
    ///</summary>
    

    ///<summary>
    ///建立组队
    ///</summary>
    ACTION_TEAM_CREATE              = 1301,  

    ///<summary>
    ///加入队伍
    ///</summary>
    ACTION_TEAM_JOIN                = 1302,  

    ///<summary>
    ///离开队伍
    ///</summary>
    ACTION_TEAM_LEAVE               = 1303,  

    ///<summary>
    ///解散组队
    ///</summary>
    ACTION_TEAM_DISMISS             = 1304,  

    ///<summary>
    ///剔除成员
    ///</summary>
    ACTION_TEAM_KICK                = 1305,  

    ///<summary>
    ///给队员提权
    ///</summary>
    ACTION_TEAM_GIVE_RIGHT          = 1306,  

    ///<summary>
    ///换队长
    ///</summary>
    ACTION_TEAM_CHANGE_CAPTAIN      = 1307,  

    ///<summary>
    ///邀请成员
    ///</summary>
    ACTION_TEAM_INVITE              = 1308,  

    ///<summary>
    ///接受邀请
    ///</summary>
    ACTION_TEAM_ACCEPT              = 1309,  

    ///<summary>
    ///队长授权接受邀请
    ///</summary>
    ACTION_TEAM_LEADER_ACCEPT       = 1310,  

    ///<summary>
    ///申请组队
    ///</summary>
    ACTION_TEAM_APPLY               = 1311,  

    ///<summary>
    ///队长授权接受申请
    ///</summary>
    ACTION_TEAM_LEADER_APPLY_ACCEPT = 1312,  

    ///<summary>
    ///开始玩法(副本)
    ///</summary>
    ACTION_TEAM_START               = 1313,  

    ///<summary>
    ///同意
    ///</summary>
    ACTION_TEAM_AGREE               = 1314,  

    ///<summary>
    ///获取附近玩家
    ///</summary>
    ACTION_TEAM_NEAR_PLAYERS        = 1315,  

    ///<summary>
    ///获取附近队伍
    ///</summary>
    ACTION_TEAM_NEAR_TEAMS          = 1316,  

    ///<summary>
    ///获取好友队伍
    ///</summary>
    ACTION_TEAM_FRIEND_TEAMS        = 1317,  

    ///<summary>
    ///获取公会队伍
    ///</summary>
    ACTION_TEAM_GUILD_TEAMS         = 1318,  

    ///<summary>
    ///召唤一队友,队长
    ///</summary>
    ACTION_TEAM_CALL                = 1319,  

    ///<summary>
    ///召唤一所有人,队长
    ///</summary>
    ACTION_TEAM_CALL_ALL            = 1320,  

    ///<summary>
    ///是否同意召唤
    ///</summary>
    ACTION_TEAM_CALL_ACCEPT         = 1321,  

    ///<summary>
    ///传送到一队友
    ///</summary>
    ACTION_TEAM_SEND                = 1322,  

    ///<summary>
    ///设置跟随状态
    ///</summary>
    ACTION_TEAM_SET_FOLLOW          = 1323,  

    ///<summary>
    ///设置不跟随状态
    ///</summary>
    ACTION_TEAM_UNSET_FOLLOW        = 1324,  

    ///<summary>
    ///获取所有开启队伍匹配。传参数:game_id = 0为全部玩法，其他game_id为具体的活动id,分页 0为全部，其他为分页。返回：玩法id，队伍id，队伍人数，队长名字，队长等级，队长职业。申请走原来协议。
    ///</summary>
    ACTION_AUTO_TEAMS                    = 1325,  

    ///<summary>
    ///玩家我要带队 。没有队伍的玩家。传参数：玩法id，最小等级和最大等级。且加入到所有队伍匹配列表
    ///</summary>
    ACTION_AUTO_TEAM_PLAYER_TAKE         = 1326,  

    ///<summary>
    ///取消玩家我要带队
    ///</summary>
    ACTION_AUTO_TEAM_PLAYER_CANCEL_TAKE  = 1327,  

    ///<summary>
    ///申请获取玩家我要带队的信息。上线的时候就需要申请了。
    ///</summary>
    ACTION_AUTO_TEAM_GET_PLAYER_TAKE     = 1328,  

    ///<summary>
    ///玩家自动匹配。没队伍的玩家。传参数:game_id.自动找对应game_id的队伍加入
    ///</summary>
    ACTION_AUTO_TEAM_PLAYER_MATCH        = 1329,  

    ///<summary>
    ///玩家取消自动匹配.
    ///</summary>
    ACTION_AUTO_TEAM_PLAYER_CANCEL_MATCH = 1330,  

    ///<summary>
    ///申请获取玩家自动匹配的信息。上线的时候就需要申请了。
    ///</summary>
    ACTION_AUTO_TEAM_GET_PLAYER_MATCH    = 1331,  

    ///<summary>
    ///队伍自动匹配。队长点击 --传参数：game_id，最小等级，最大等级，喊话内容，是否审批
    ///</summary>
    ACTION_AUTO_TEAM_LEADER_MATCH        = 1332,  

    ///<summary>
    ///取消队伍自动匹配。队长点击。-仅仅是暂停。
    ///</summary>
    ACTION_AUTO_TEAM_LEADER_CANCEL_MATCH = 1333,  

    ///<summary>
    ///喊话,参数和ACTION_AUTO_TEAM_LEADER_MATCH一致且多加一个chanel_id
    ///</summary>
    ACTION_AUTO_TEAM_CALL                = 1334,  

    ///<summary>
    ///更新auto_team的数据。PbTeamMemberList的分出PbTeamInfoAutoTeam。
    ///</summary>
    ACTION_TEAM_UPDATE_AUTO_TEAM         = 1335,  

    ///<summary>
    ///取消队伍自动匹配。不仅仅是暂停，是清空所有数据。看情况是否客户端来清。
    ///</summary>
    ACTION_AUTO_TEAM_LEADER_CLEAR_MATCH  = 1336,  

    ///<summary>
    ///通知弹框。如果玩家没有队伍，那么弹框，我要带队，玩家自动匹配，不处理。如果是队长，队伍自动匹配，不处理。
    ///</summary>
    ACTION_TEAM_WILD_TASK                = 1337,  
    


    ///<summary>
    ///----------------------1401~1450符文系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///请求符文背包
    ///</summary>
    ACTION_RUNE_GET                  = 1401,  

    ///<summary>
    ///请求身体符文背包
    ///</summary>
    ACTION_RUNE_GET_BODY             = 1402,  

    ///<summary>
    ///刷新符文 --刷新类型
    ///</summary>
    ACTION_RUNE_REFRESH              = 1403,  

    ///<summary>
    ///一键合成符文
    ///</summary>
    ACTION_RUNE_AUTO_COMBINE         = 1405,  

    ///<summary>
    ///一键排序,整理
    ///</summary>
    ACTION_RUNE_AUTO_SORT            = 1406,  

    ///<summary>
    ///使用符文
    ///</summary>
    ACTION_RUNE_USE                  = 1407,  

    ///<summary>
    ///穿上符文
    ///</summary>
    ACTION_RUNE_PUT_ON               = 1408,  

    ///<summary>
    ///脱下符文
    ///</summary>
    ACTION_RUNE_PUT_DOWN             = 1409,  

    ///<summary>
    ///'符文"背包换位置
    ///</summary>
    ACTION_RUNE_CHANGE_POS           = 1410,  

    ///<summary>
    ///"身体符文"背包换位置
    ///</summary>
    ACTION_RUNE_CHANGE_BODY_POS      = 1411,  

    ///<summary>
    ///符文锁定
    ///</summary>
    ACTION_RUNE_LOCK                 = 1412,  

    ///<summary>
    ///符文解锁
    ///</summary>
    ACTION_RUNE_UNLOCK               = 1413,  

    ///<summary>
    ///身上装备符文升级
    ///</summary>
    ACTION_RUNE_UPDATE_BODY          = 1414,  

    ///<summary>
    ///背包装备符文升级
    ///</summary>
    ACTION_RUNE_UPDATE               = 1415,  

    ///<summary>
    ///查看免费许愿剩余次数和倒计时
    ///</summary>
    ACTION_RUNE_WISH_INFO            = 1416,  

    ///<summary>
    ///脱下吞噬符文
    ///</summary>
    ACTION_RUNE_PUT_DOWN_EAT         = 1417,  
    

    ///<summary>
    ///----------------------1451~1500翅膀系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///请求翅膀背包
    ///</summary>
    ACTION_WING_GET                  = 1451,   

    ///<summary>
    ///请求培养翅膀
    ///</summary>
    ACTION_WING_TRAIN                = 1452,   

    ///<summary>
    ///穿上翅膀
    ///</summary>
    ACTION_WING_PUT_ON               = 1453,   

    ///<summary>
    ///请求购买翅膀
    ///</summary>
    ACTION_WING_ADD                  = 1454,   

    ///<summary>
    ///请求翅膀战斗力的击败多少玩家
    ///</summary>
    ACTION_WING_FIGHT_FORCE_RANK     = 1455,   

    ///<summary>
    ///合成请求
    ///</summary>
    ACTION_WING_COMPOSE              = 1456,   

    ///<summary>
    ///购买翅膀，转换成羽毛
    ///</summary>
    ACTION_WING_EXCHANGE             = 1457,   


    ///<summary>
    ///----------------------1501~1550制造系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///培养制造技能
    ///</summary>
    ACTION_MFG_ENHANCE               = 1501,   

    ///<summary>
    ///制造产品
    ///</summary>
    ACTION_MFG_PRODUCT               = 1502,   

    ///<summary>
    ///学习稀有配方
    ///</summary>
    ACTION_MFG_LEARN                 = 1503,   

    ///<summary>
    ///请求稀有配方背包
    ///</summary>
    ACTION_MFG_FORMULA_BAG           = 1504,   

    ///<summary>
    ///学习天赋 
    ///</summary>
    ACTION_MFG_TALENT                = 1505,   

    ///<summary>
    ///请求cd列表
    ///</summary>
    ACTION_MFG_CD                    = 1506,   

    ///<summary>
    ///重置
    ///</summary>
    ACTION_MFG_RESET                 = 1507,   
    

    ///<summary>
    ///----------------------1551~1600星魂系统---------------------------------------
    ///</summary>
    

    ///<summary>
    ///点亮星星
    ///</summary>
    ACTION_STAR_SPIRIT_LIGHT_STAR    = 1551,    

    ///<summary>
    ///获取星魂记录
    ///</summary>
    ACTION_STAR_SPIRIT_GET_RECORD    = 1552,    


    ///<summary>
    ///----------------------1601~1650代币系统 ---------------------------------------
    ///</summary>
    

    ///<summary>
    ///代币商店兑换
    ///</summary>
    ACTION_TOKEN_SHOP_EXCHANGE       = 1601,    

    ///<summary>
    ///获取商店物品限制
    ///</summary>
    ACTION_TOKEN_SHOP_GET_ITEM_LIMIT = 1602,    

    ///<summary>
    ///获取商店总物品限制，已购买的数量，所有物品
    ///</summary>
    ACTION_TOKEN_SHOP_GET_TOTAL_LIMIT_ALL = 1603, 

    ///<summary>
    ///获取商店总物品限制，已购买的数量，单个物品，参数id
    ///</summary>
    ACTION_TOKEN_SHOP_GET_TOTAL_LIMIT_SINGLE = 1604, 
       

    ///<summary>
    ///----------------------1651~1700幻境探索系统 ------------------------------------
    ///</summary>
    

    ///<summary>
    ///开始探索
    ///</summary>
    ACTION_DREAMLAND_EXPLORE         = 1651,   

    ///<summary>
    ///抢劫
    ///</summary>
    ACTION_DREAMLAND_LOOT            = 1652,   

    ///<summary>
    ///探索完成
    ///</summary>
    ACTION_DREAMLAND_EXPLORE_FINISH  = 1653,   

    ///<summary>
    ///复仇
    ///</summary>
    ACTION_DREAMLAND_REVENGE         = 1654,   

    ///<summary>
    ///提升飞龙品质
    ///</summary>
    ACTION_DREAMLAND_PROMOTE         = 1655,   

    ///<summary>
    ///购买最高品质飞龙
    ///</summary>
    ACTION_DREAMLAND_BUY_DRAGON      = 1656,   

    ///<summary>
    ///前一天奖励找回
    ///</summary>
    ACTION_DREAMLAND_AWARD_RETRIEVE  = 1657,   

    ///<summary>
    ///（月光宝盒）奖励找回基本信息
    ///</summary>
    ACTION_DREAMLAND_BOX_INFO        = 1658,   

    ///<summary>
    ///刷新对手
    ///</summary>
    ACTION_DREAMLAND_FRESH           = 1659,   

    ///<summary>
    ///立即护送完成
    ///</summary>
    ACTION_DREAMLAND_CONVOY_COMPLETE = 1660,   

    ///<summary>
    ///缩减完成时间5分钟
    ///</summary>
    ACTION_DREAMLAND_RATE_FIVE       = 1661,   

    ///<summary>
    ///清除袭击CD
    ///</summary>
    ACTION_DREAMLAND_CLEAR_ATK_CD    = 1662,   

    ///<summary>
    ///袭击次数购买
    ///</summary>
    ACTION_DREAMLAND_BUY_ATK_TIMES   = 1663,   

    ///<summary>
    ///拿到对应的幻境基本信息
    ///</summary>
    ACTION_DREAMLAND_GET_INFO        = 1664,   

    ///<summary>
    ///拿到所有幻境基本信息
    ///</summary>
    ACTION_DREAMLAND_GET_ALL_INFO    = 1665,   

    ///<summary>
    ///拿到事件记录
    ///</summary>
    ACTION_DREAMLAND_EVENT_RECORDER  = 1666,   

    ///<summary>
    ///拿到玩家可抢列表某个玩家的信息
    ///</summary>
    ACTION_DREAMLAND_GET_VISIBLE_LIST= 1667,   

    ///<summary>
    ///被抢事件通知
    ///</summary>
    ACTION_DREAMLAND_EVENT_NOTICE    = 1668,   

    ///<summary>
    ///拿到抢劫对方的次数
    ///</summary>
    ACTION_DREAMLAND_GET_ATK_COUNT   = 1669,   
    


    ///<summary>
    ///----------------------1701~1750要塞系统 ------------------------------------
    ///</summary>
    

    ///<summary>
    ///请求获取要塞信息
    ///</summary>
    ACTION_FORTRESS_GETINFO          = 1701,    

    ///<summary>
    ///设置默认显示建筑
    ///</summary>
    ACTION_FORTRESS_SETDEFAULT       = 1702,    

    ///<summary>
    ///请求获取建筑信息
    ///</summary>
    ACTION_FORTRESS_BUILDING_GETINFO = 1703,    

    ///<summary>
    ///请求升级建筑
    ///</summary>
    ACTION_FORTRESS_BUILDING_UPGRADE = 1704,    

    ///<summary>
    ///请求整个要塞的信息
    ///</summary>
    ACTION_FORTRESS_QUERY_DATA       = 1705,    

    ///<summary>
    ///查询要塞产出
    ///</summary>
    ACTION_FORTRESS_QUERY_INCOME     = 1706,    

    ///<summary>
    ///收取要塞产出
    ///</summary>
    ACTION_FORTRESS_REWARD_INCOME    = 1707,    

    ///<summary>
    ///要塞升级了
    ///</summary>
    ACTION_FORTRESS_LEVEL_UP         = 1711,    

    ///<summary>
    ///要塞建筑升级了
    ///</summary>
    ACTION_FORTRESS_BUILDING_LEVEL_UP= 1712,    


    ///<summary>
    ///----------------------1721~1725要塞系统-时光祭坛 ----------------------------
    ///</summary>
    

    ///<summary>
    ///查询时光祭坛信息
    ///</summary>
    ACTION_TIME_ALTAR_QUERY_INFO     = 1721,    

    ///<summary>
    ///时光祭坛领奖
    ///</summary>
    ACTION_TIME_ALTAR_GET_REWARD     = 1722,    


    ///<summary>
    ///----------------------1751~1800符文系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///商城购买
    ///</summary>
    ACTION_MARKET_BUY                       = 1751,  

    ///<summary>
    ///检查版本
    ///</summary>
    ACTION_MARKET_CHECK_VERSION             = 1752,  

    ///<summary>
    ///获取所有商城数据(和检查版本一致，可能热更新需要????
    ///</summary>
    ACTION_MARKET_DATA_REG                  = 1753,  

    ///<summary>
    ///每日限购，使用次数
    ///</summary>
    ACTION_MARKET_DAILY_REQ                 = 1754,  

    ///<summary>
    ///个人全部限购。使用次数
    ///</summary>
    ACTION_MARKET_BUY_TOTAL_REQ             = 1755,  

    ///<summary>
    ///抢购限购。使用次数
    ///</summary>
    ACTION_MARKET_SELL_TOTAL_REQ            = 1756,  

    ///<summary>
    ///每日限购，使用次数。按格子更新
    ///</summary>
    ACTION_MARKET_GRID_DAILY_REQ            = 1757,  

    ///<summary>
    ///个人全部限购。使用次数。按格子更新
    ///</summary>
    ACTION_MARKET_GRID_BUY_TOTAL_REQ        = 1758,  

    ///<summary>
    ///抢购限购。使用次数。按格子更新
    ///</summary>
    ACTION_MARKET_GRID_SELL_TOTAL_REQ       = 1759,  

    ///<summary>
    ///商城购买。赠送调用这个，用于提示不同,但服务端还是走同一个接口
    ///</summary>
    ACTION_MARKET_BUY_GIVE                  = 1760,  
    

    ///<summary>
    ///----------------------1801~1850时装系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///请求时装信息（时装列表和总属性）
    ///</summary>
    ACTION_FASHION_GETINFO           = 1801,    

    ///<summary>
    ///设置当前时装
    ///</summary>
    ACTION_FASHION_SETCURRENT        = 1802,    
    

    ///<summary>
    ///----------------------1851~1900试练秘境-------------------------------------
    ///</summary>
    

    ///<summary>
    ///试练秘境信息查询
    ///</summary>
    ACTION_TRY_INFO_QUERY_REQ            = 1851,    

    ///<summary>
    ///试练秘境副本挑战
    ///</summary>
    ACTION_TRY_CHALLENGE_REQ             = 1852,    

    ///<summary>
    ///试炼秘镜副本扫荡
    ///</summary>
    ACTION_TRY_MOP_UP_REQ                = 1853,    

    ///<summary>
    ///试练秘境副本重置
    ///</summary>
    ACTION_TRY_DATA_RESET_REQ            = 1854,    

    ///<summary>
    ///试炼秘境副本状态
    ///</summary>
    ACTION_TRY_PASS_STATE_REQ            = 1855,    

    ///<summary>
    ///试炼秘境副本宝箱
    ///</summary>
    ACTION_TRY_MISSION_BOX_REQ           = 1856,    

    ///<summary>
    ///试炼秘镜章节宝箱
    ///</summary>
    ACTION_TRY_CHAPTER_BOX_REQ           = 1857,    
    

    ///<summary>
    ///----------------------1901~1950VIP系统--------------------------------------
    ///</summary>
    

    ///<summary>
    ///获取vip奖励
    ///</summary>
    ACTION_VIP_GET_REWARDS            = 1901,      

    ///<summary>
    ///获取vip奖励领取记录
    ///</summary>
    ACTION_VIP_GET_RECORD            = 1902,       

    ///<summary>
    ///充值后等级提升
    ///</summary>
    ACTION_VIP_LEVEL_UP               = 1903,      
    

    ///<summary>
    ///----------------------1951~2000NPC好感度--------------------------------------
    ///</summary>
    

    ///<summary>
    ///查询所有NPC的好感度
    ///</summary>
    ACTION_NPC_REPUTATION_QUERY_ALL  = 1951,     

    ///<summary>
    ///查询某个NPC的好感度
    ///</summary>
    ACTION_NPC_REPUTATION_QUERY      = 1952,     

    ///<summary>
    ///上缴某道具
    ///</summary>
    ACTION_NPC_REPUTATION_HAND_IN    = 1953,     


    ///<summary>
    ///----------------------2001~2050NPC宠物系统--------------------------------------
    ///</summary>
    

    ///<summary>
    ///获取宠物列表
    ///</summary>
    ACTION_PET_GET_PET_LIST         = 2001,      

    ///<summary>
    ///召唤某个宠物,如果背包满足物品要求。这是一种宠物来源,已经有了，是无法看到召唤按钮的。其他来源(道具等),其他条件判定,这边可以直接加，如果已经有就转换成对应的道具。todo:这个转换的需要判断背包格子的问题啊。
    ///</summary>
    ACTION_PET_CALL_PET             = 2002,      

    ///<summary>
    ///加经验,宠物界面操作,给具体的宠物加,升级
    ///</summary>
    ACTION_PET_ADD_EXP              = 2003,      

    ///<summary>
    ///升星
    ///</summary>
    ACTION_PET_ADD_STAR             = 2004,      

    ///<summary>
    ///升品质
    ///</summary>
    ACTION_PET_ADD_QUALITY          = 2005,      

    ///<summary>
    ///出战(出战宠物列表hole为0默认后面加，不为0为更换操作)
    ///</summary>
    ACTION_PET_COMBAT               = 2006,      

    ///<summary>
    ///助战
    ///</summary>
    ACTION_PET_ASSIST               = 2007,      

    ///<summary>
    ///收战,对应出战的收战  (出战宠物列表进行列表操作，给后面的进行提前)
    ///</summary>
    ACTION_PET_IDLE                 = 2008,      

    ///<summary>
    ///激活出战
    ///</summary>
    ACTION_PET_ACTIVE               = 2009,      

    ///<summary>
    ///收激活出战
    ///</summary>
    ACTION_PET_UNACTIVE             = 2010,      

    ///<summary>
    ///激活 (替换，在副本中,只和当前激活的替换,当前激活的被替换后无法再出战)(cnmd后面又要可以出战)
    ///</summary>
    ACTION_PET_ACTIVE_RE            = 2011,      

    ///<summary>
    ///宠物洗练 --概率分段公式。x,y分n(n>=2)段,按照n个权重进行抽取i下标(从1开始)出来。那么范围是[x+((x+y)/n - x)*(i-1),x+((x+y)/n - x)*i]
    ///</summary>
    ACTION_PET_REFRESH              = 2012,      

    ///<summary>
    ///复活
    ///</summary>
    ACTION_PET_REVIVE               = 2013,      

    ///<summary>
    ///收战，对应休战
    ///</summary>
    ACTION_PET_IDLE_ASSIST          = 2014,      

    ///<summary>
    ///宠物洗练确认
    ///</summary>
    ACTION_PET_REFRESH_CONFIRM      = 2015,      

    ///<summary>
    ///宠物出战设置为先锋，然后后面的顺序排
    ///</summary>
    ACTION_PET_COMBAT_FIRST         = 2016,      


    ///<summary>
    ///----------------------2051~2100排行榜系统--------------------------------------
    ///</summary>
    

    ///<summary>
    ///崇拜请求
    ///</summary>
    ACTION_RANK_WORSHIP             = 2052,      

    ///<summary>
    ///拿到自己排行和战力
    ///</summary>
    ACTION_RANK_GET_RANKING         = 2053,      

    ///<summary>
    ///拿到排行榜更新的时间
    ///</summary>
    ACTION_RANK_GET_UPDATE_TIME     = 2055,      

    ///<summary>
    ///拿到排行榜基本信息
    ///</summary>
    ACTION_RANK_GET_RANK_INFO       = 2056,      

    ///<summary>
    ///拿到对应排行榜列表
    ///</summary>
    ACTION_GET_TNE_MIN_RANK_LIST    = 2057,      

    ///<summary>
    ///拿到十分钟更新一次，自己排行和战力
    ///</summary>
    ACTION_GET_TNE_MIN_RANKING      = 2058,      

    ///<summary>
    ///公会排行榜返回
    ///</summary>
    ACTION_RANK_GUILD_INFO          = 2059,      
    

    ///<summary>
    ///----------------------2101~2150技能系统---------------------------------------
    ///</summary>
    

    ///<summary>
    ///技能学习
    ///</summary>
    ACTION_SPELL_LEARN                        = 2101,       

    ///<summary>
    ///技能升级
    ///</summary>
    ACTION_SPELL_UPGRADE                      = 2102,       

    ///<summary>
    ///技能装备
    ///</summary>
    ACTION_SPELL_EQUIP                        = 2103,       

    ///<summary>
    ///技能专精
    ///</summary>
    ACTION_SPELL_PROFICIENT                   = 2104,       

    ///<summary>
    ///获取已学技能记录
    ///</summary>
    ACTION_SPELL_GET_LEARNED_SPELLS           = 2105,       

    ///<summary>
    ///获取技能槽记录
    ///</summary>
    ACTION_SPELL_GET_SPELL_SLOTS              = 2106,       

    ///<summary>
    ///获取专精记录
    ///</summary>
    ACTION_SPELL_GET_SPELL_PRIFICIENT         = 2107,       


    ///<summary>
    ///----------------------2151~2200 副本玩法:恶魔之门-------------------------------------
    ///</summary>
    

    ///<summary>
    ///恶魔之门个人信息,如果没有代表今天还没有参加
    ///</summary>
    ACTION_EVIL_INFO                  = 2151,    

    ///<summary>
    ///恶魔之门排行榜
    ///</summary>
    ACTION_EVIL_RANK                  = 2152,    

    ///<summary>
    ///恶魔之门挑战，进行匹配，进入副本
    ///</summary>
    ACTION_EVIL_CHALLENGE             = 2153,    

    ///<summary>
    ///恶魔之门传送  --跨场景传送
    ///</summary>
    ACTION_EVIL_SEND                  = 2154,    

    ///<summary>
    ///分享传送，参考恶魔之门传送，先放在这里
    ///</summary>
    ACTION_SHARE_SEND                 = 2155,    

    ///<summary>
    ///扫荡
    ///</summary>
    ACTION_EVIL_MOP_UP                = 2156,    


    ///<summary>
    ///---------------------2201~2250奖励系统------------------------------------------
    ///</summary>
    

    ///<summary>
    ///每日活跃任务领取奖励
    ///</summary>
    ACTION_AWARD_ACTIVE_TASK_GET_REWARD                   = 2201,       

    ///<summary>
    ///每日活跃任务宝箱奖励补领vip奖励
    ///</summary>
    ACTION_AWARD_ACTIVE_TASK_COMPENSATE_VIP_REWARD        = 2202,       

    ///<summary>
    ///每日任务领取奖励
    ///</summary>
    ACTION_AWARD_DAILY_TASK_GET_REWARD                    = 2203,       

    ///<summary>
    ///领取月登陆奖励
    ///</summary>
    ACTION_AWARD_MONTH_LOGIN_GET_REWARD                   = 2204,       

    ///<summary>
    ///通知单个活跃任务完成（单个事件触发的时候）
    ///</summary>
    ACTION_AWARD_NOTIFY_ACTIVE_TASK_COMPLETTED            = 2205,       

    ///<summary>
    ///通知单个每日任务完成
    ///</summary>
    ACTION_AWARD_NOTIFY_DAILY_TASK_COMPLETTED             = 2206,       

    ///<summary>
    ///通知所有当前已完成的活跃任务
    ///</summary>
    ACTION_AWARD_PUSH_ACTIVE_TASK_COMPLETTED              = 2207,       

    ///<summary>
    ///通知所有当前已完成每日任务
    ///</summary>
    ACTION_AWARD_PUSH_DAILY_TASK_COMPLETTED               = 2208,       

    ///<summary>
    ///活跃任务获取宝箱奖励
    ///</summary>
    ACTION_AWARD_GET_BOX_REWARD                           = 2209,       

    ///<summary>
    ///推送已接受的活跃任务
    ///</summary>
    ACTION_AWARD_PUSH_ACTIVE_TASK_ACCEPT                  = 2210,       

    ///<summary>
    ///推送已接受的每日任务
    ///</summary>
    ACTION_AWARD_PUSH_DAILY_TASK_ACCEPT                   = 2211,       

    ///<summary>
    ///推送月登陆记录
    ///</summary>
    ACTION_AWARD_PUSH_MONTH_LOGIN_RECORD                  = 2212,       

    ///<summary>
    ///领取月登陆VIP奖励
    ///</summary>
    ACTION_AWARD_MONTH_LOGIN_VIP_GET_REWARD               = 2213,       
    

    ///<summary>
    ///获取活跃任务记录
    ///</summary>
    ACTION_AWARD_GET_ACTIVE_TASK_RECORD                   = 2214,       

    ///<summary>
    ///获取每日任务记录
    ///</summary>
    ACTION_AWARD_GET_DAILY_TASK_RECORD                    = 2215,       

    ///<summary>
    ///获取活跃任务宝箱记录
    ///</summary>
    ACTION_AWARD_GET_ACTIVE_TASK_BOX_RECORD               = 2216,       

    ///<summary>
    ///获取月登陆记录
    ///</summary>
    ACTION_AWARD_GET_MONTH_LOGIN_RECORD                   = 2217,       
    

    ///<summary>
    ///通知活跃任务单个事件触发
    ///</summary>
    ACTION_AWARD_NOTIFY_ACTIVE_TASK_TRIGGER               = 2218,       
    

    ///<summary>
    ///任务回退状态
    ///</summary>
    ACTION_AWARD_NOTIFY_ACTIVE_RETURN_STATE               = 2219,       
    
    

    ///<summary>
    ///---------------------2251~2260 设置系统------------------------------------------
    ///</summary>
    

    ///<summary>
    ///设置系统推送
    ///</summary>
    ACTION_SETTING_PUSH                             = 2251, 
    

    ///<summary>
    ///---------------------2261~2270剧情指引相关操作-------------------------------------
    ///</summary>
    

    ///<summary>
    ///设置剧情指引
    ///</summary>
    ACTION_DRAMA_GUIDE_SET                              = 2261, 

    ///<summary>
    ///剧情指引返回信息
    ///</summary>
    ACTION_DRAMA_GUIDE_GET                              = 2262, 


    ///<summary>
    ///---------------------2271~2280 功能开启系统------------------------------------------
    ///</summary>
    

    ///<summary>
    ///查询所有开启的功能
    ///</summary>
    ACTION_FUNCTION_OPEN_QUERY                      = 2271, 

    ///<summary>
    ///某功能状态更新的通知
    ///</summary>
    ACTION_FUNCTION_OPEN_NOTIFY                     = 2272, 
    

    ///<summary>
    ///--------------------2281~2290 好友邀请系统-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///使用邀请码
    ///</summary>
    ACTION_INVITE_USE_INVITE_NUM                   = 2281,  

    ///<summary>
    ///生成邀请码
    ///</summary>
    ACTION_INVITE_GENERATE_INVITE_NUM              = 2282,  

    ///<summary>
    ///获取邀请码
    ///</summary>
    ACTION_INVITE_GET_INVITE_NUM                   = 2283,  

    ///<summary>
    ///获取奖励
    ///</summary>
    ACTION_INVITE_GET_REWARD                       = 2284,  

    ///<summary>
    ///更新任务
    ///</summary>
    ACTION_INVITE_UPDATE_PROGRESS                  = 2285,  

    ///<summary>
    ///检查有无新的奖励
    ///</summary>
    ACTION_INVITE_CHECK_REWARD                     = 2286,  

    ///<summary>
    ///通知新的奖励
    ///</summary>
    ACTION_INVITE_NOTIFY_NEW_REWARD                = 2287,  

    ///<summary>
    ///标记分享成功
    ///</summary>
    ACTION_INVITE_MARK_SHARED                      = 2288,  

    ///<summary>
    ///获取邀请记录
    ///</summary>
    ACTION_INVITE_QUERY_LIST                       = 2289,  


    ///<summary>
    ///--------------------2291~2299 价格次数定价系统-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///拿到对应id购买次数
    ///</summary>
    ACTION_GET_PRICE_BUY_COUNT                         = 2291,  

    ///<summary>
    ///拿到价格次数定价列表
    ///</summary>
    ACTION_GET_PRICE_BUY_COUNT_LIST                    = 2292,  



    ///<summary>
    ///----------------------2301~2350 副本玩法:组队副本-------------------------------------
    ///</summary>
    

    ///<summary>
    ///开始玩法
    ///</summary>
    ACTION_TEAM_MISSION_ENTER                    = 2301,  


    ///<summary>
    ///----------------------2351~2400 副本玩法:喊人组队-------------------------------------
    ///</summary>
    

    ///<summary>
    ///喊人,发送超链接到聊天框(dbid,inst_id)
    ///</summary>
    ACTION_TEAM_CALL_CALL                       = 2351,  

    ///<summary>
    ///接受，点击聊天框的超链接(dbid,inst_id,mydbid) --ACTION_TEAM_CALL_ACCEPT
    ///</summary>
    ACTION_TEAM_CALL_CALL_ACCEPT                = 2352,  
    

    ///<summary>
    ///----------------------2401~2450 体力购买-------------------------------------
    ///</summary>
    

    ///<summary>
    ///购买体力一次
    ///</summary>
    ACTION_ENERGY_BUY_ONCE                      = 2401,  

    ///<summary>
    ///体力全部购买
    ///</summary>
    ACTION_ENERGY_BUY_ALL                       = 2402,  
    

    ///<summary>
    ///------------------------2451~2500单人副本-------------------------------------
    ///</summary>
    

    ///<summary>
    ///单人副本基本信息查询
    ///</summary>
    ACTION_SINGLE_MISSION_INFO                  = 2451,  

    ///<summary>
    ///单人副本挑战
    ///</summary>
    ACTION_SINGLE_MISSION_CHALLENGE             = 2452,  

    ///<summary>
    ///单人副本扫荡
    ///</summary>
    ACTION_SINGLE_MISSION_MOP_UP                = 2453,  

    ///<summary>
    ///单人副本次数购买
    ///</summary>
    ACTION_SINGLE_MISSION_BUY_COUNT             = 2454,  

    ///<summary>
    ///单人副本对应章节副本的状态
    ///</summary>
    ACTION_SINGLE_MISSION_STATE                 = 2455,  
    

    ///<summary>
    ///------------------------2501~2550卡片管理-------------------------------------
    ///</summary>
    

    ///<summary>
    ///拿到卡片的基本信息
    ///</summary>
    ACTION_GET_CARD_INFO                        = 2501,  

    ///<summary>
    ///拿到指定卡片的信息
    ///</summary>
    ACTION_GET_APPOINT_CARD_INFO                = 2502,  
    

    ///<summary>
    ///------------------------2551~2600手机绑定管理-----------------------------------
    ///</summary>
    

    ///<summary>
    ///获取验证码
    ///</summary>
    ACTION_GET_VERIFICATION_CODE                = 2551,  

    ///<summary>
    ///输入验证码，绑定手机
    ///</summary>
    ACTION_BIND_MOBILE                          = 2552,  

    ///<summary>
    ///获取绑定信息
    ///</summary>
    ACTION_GET_BIND_MOBILE_INFO                 = 2553,  

    ///<summary>
    ///获取绑定奖励
    ///</summary>
    ACTION_GET_BIND_REWARD                      = 2554,  


    ///<summary>
    ///----------------------2601~2650 副本玩法:pk副本-------------------------------------
    ///</summary>
    

    ///<summary>
    ///邀请
    ///</summary>
    ACTION_PK_MISSION_INVITE          = 2601,    

    ///<summary>
    ///接受邀请
    ///</summary>
    ACTION_PK_MISSION_ACCEPT          = 2602,    

    ///<summary>
    ///获取pk日志
    ///</summary>
    ACTION_PK_GET_PK_LOGS             = 2603,    

    ///<summary>
    ///服务器内部访问
    ///</summary>
    ACTION_PK_GET_PK_LOGS_SERVER      = 2604,    
    

    ///<summary>
    ///----------------------2651~2700 称号系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///装备称号
    ///</summary>
    ACTION_TITLE_EQUIP_TITLE          = 2651,    

    ///<summary>
    ///获得新的称号
    ///</summary>
    ACTION_TITLE_GET_NEW_TITLE        = 2652,    

    ///<summary>
    ///获取称号背包
    ///</summary>
    ACTION_TITLE_GET_TITLE_BAG        = 2653,    

    ///<summary>
    ///卸载称号
    ///</summary>
    ACTION_TITLE_UNINSTALL            = 2654,    

    ///<summary>
    ///标记称号已查看
    ///</summary>
    ACTION_TITLE_MARK_CHECK           = 2655,    


    ///<summary>
    ///----------------------2701~2750 成就系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///更新成就进度
    ///</summary>
    ACTION_ACHIEVEMENT_UPDATE         = 2701,    

    ///<summary>
    ///成就完成
    ///</summary>
    ACTION_ACHIEVEMENT_COMPLETE       = 2702,    

    ///<summary>
    ///成就领奖
    ///</summary>
    ACTION_ACHIEVEMENT_REWARD         = 2703,    

    ///<summary>
    ///查询列表
    ///</summary>
    ACTION_ACHIEVEMENT_QUERY_LIST     = 2706,    


    ///<summary>
    ///----------------------2751~2800 炼金系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///邀请协助者
    ///</summary>
    ACTION_ALCHEMY_INVITE            = 2751,    

    ///<summary>
    ///开始炼金
    ///</summary>
    ACTION_ALCHEMY_ALCHEMY           = 2752,    

    ///<summary>
    ///获取炼金协助者，如果下线，再上线，客户端需要获取这个数据显示
    ///</summary>
    ACTION_ALCHEMY_GET_ASSISTS       = 2753,    

    ///<summary>
    ///获取今天自己已经协助的次数
    ///</summary>
    ACTION_ALCHEMY_GET_ASSIST_CNT    = 2754,    

    ///<summary>
    ///领取炼金的奖励，够时间，需要自己领取。其他协助的自动发邮件。
    ///</summary>
    ACTION_ALCHEMY_RECEIVE           = 2755,    

    ///<summary>
    ///服务器内部访问。
    ///</summary>
    ACTION_ALCHEMY_FINISH            = 2756,    

    ///<summary>
    ///服务器内部访问。
    ///</summary>
    ACTION_ALCHEMY_INVITE_ASSIST_CNT = 2757,    

    ///<summary>
    ///服务器内部访问。
    ///</summary>
    ACTION_ALCHEMY_ALCHEMY_ASSIST_CNT = 2758,    
   
 

    ///<summary>
    ///----------------------2801~2900 充值系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///充值信息请求
    ///</summary>
    ACTION_CHARGE_INFO_REQ           = 2801,     

    ///<summary>
    ///上线充值奖励检查
    ///</summary>
    ACTION_CHARGE_CHECK_REQ          = 2802,     

    ///<summary>
    ///领取充值资源
    ///</summary>
    ACTION_CHARGE_WITHDRAW_REQ       = 2803,     

    ///<summary>
    ///领取充值资源回调
    ///</summary>
    ACTION_CHARGE_WITHDRAW_CB        = 2804,     

    ///<summary>
    ///ACTION_CHARGE_NOTIFY             = 2805,     --充值数据通知
    ///</summary>
    

    ///<summary>
    ///充值条件检查
    ///</summary>
    ACTION_CHARGE_CONDITION_CHECK    = 2806,     

    ///<summary>
    ///----------------------2901~2950 外观系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///获取玩家的外观信息
    ///</summary>
    ACTION_FACADE_GETINFO            = 2901,     

    ///<summary>
    ///设置玩家的当前外观
    ///</summary>
    ACTION_FACADE_SETCURRENT         = 2902,     

    ///<summary>
    ///激活外观
    ///</summary>
    ACTION_FACADE_ACTIVITE           = 2903,     

    ///<summary>
    ///卸下外观
    ///</summary>
    ACTION_FACADE_TAKEOFF            = 2904,     

    ///<summary>
    ///通知客户端外观开启
    ///</summary>
    ACTION_FACADE_NOTICE             = 2906,     

    ///<summary>
    ///----------------------2951~2960 队长分配系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///申请
    ///</summary>
    CAPTAIN_ITEM_APPLY               = 2951,     

    ///<summary>
    ///分配
    ///</summary>
    CAPTAIN_ITEM_DISTRIBUTION        = 2952,     

    ///<summary>
    ///确定
    ///</summary>
    CAPTAIN_ITEM_FINAL               = 2953,     

    ///<summary>
    ///----------------------2961~2970 幸运转盘系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///幸运转盘抽奖
    ///</summary>
    ACTION_LUCKY_TURNTABLE_RUN       = 2961,     

    ///<summary>
    ///幸运转盘重置
    ///</summary>
    ACTION_LUCKY_TURNTABLE_RESET     = 2962,     

    ///<summary>
    ///幸运转盘请求信息
    ///</summary>
    ACTION_LUCKY_TURNTABLE_GETINFO   = 2963,     

    ///<summary>
    ///幸运转盘请求手气榜
    ///</summary>
    ACTION_LUCKY_TURNTABLE_GET_RANKLIST= 2964,     


    ///<summary>
    ///添加幸运币，好友系统调用
    ///</summary>
    ACTION_LUCKY_TURNTABLE_BASE_LUCKY_COIN= 2969,

    ///<summary>
    ///手气榜回掉
    ///</summary>
    ACTION_LUCKY_TURNTABLE_BASE_RANKLIST= 2970,  

    ///<summary>
    ///----------------------2971~2980 装备熔炼系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///装备熔炼
    ///</summary>
    ACTION_EQUIP_SMELTING_SMELT      = 2971,     


    ///<summary>
    ///----------------------2981~2990 坐骑系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///请求坐骑信息
    ///</summary>
    ACTION_RIDE_GETINFO           = 2981,    

    ///<summary>
    ///设置当前坐骑
    ///</summary>
    ACTION_RIDE_SETCURRENT        = 2982,    

    ///<summary>
    ///上坐骑
    ///</summary>
    ACTION_RIDE_MOUNT             = 2983,    

    ///<summary>
    ///下坐骑
    ///</summary>
    ACTION_RIDE_UNMOUNT           = 2984,    


    ///<summary>
    ///----------------------2991~3020 时空裂隙系统-------------------------------------
    ///</summary>
    

    ///<summary>
    ///时空裂隙申请任务请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_APPLY_REQ     = 2991, 

    ///<summary>
    ///时空裂隙任务接受请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_RECIVE_REQ    = 2992, 

    ///<summary>
    ///时空裂隙退出任务请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_QUIT_REQ      = 2993, 

    ///<summary>
    ///时空裂隙新成员加入
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_NEW_JOIN      = 2994, 

    ///<summary>
    ///时空裂隙周额外奖励领取请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_WEEK_REQ      = 2997, 

    ///<summary>
    ///时空裂隙任务队长提交请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_COMMIT_REQ    = 2998, 

    ///<summary>
    ///时空裂隙上线数据请求
    ///</summary>
    ACTION_SPACETIME_RIFT_TASK_ONLINE_REQ    = 2999, 


    ///<summary>
    ///----------------------3021~3040 充值活动-------------------------------------
    ///</summary>
    

    ///<summary>
    ///查询某活动的信息
    ///</summary>
    ACTION_CHARGE_ACTIVITY_QUERY_INFO        = 3021, 

    ///<summary>
    ///领奖某活动
    ///</summary>
    ACTION_CHARGE_ACTIVITY_REWARD            = 3022, 

    ///<summary>
    ///查询所有活动的信息
    ///</summary>
    ACTION_CHARGE_ACTIVITY_QUERY_INFO_LIST   = 3023, 


    ///<summary>
    ///通知某活动的状态变化
    ///</summary>
    ACTION_CHARGE_ACTIVITY_NOTICE            = 2040, 

    ///<summary>
    ///-------------------------------------------------------------------------------
    ///</summary>
    


    ///<summary>
    ///----------------------3050~3100 创角活动(七天 ) 类似成就系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///更新进度  --服务器通知                                                                                                                                                                                                          --pb结构 PbCreateActivityInfo
    ///</summary>
    ACTION_CREATE_ACTIVITY_UPDATE           = 3050,    

    ///<summary>
    ///完成   --服务器通知 --收到已完成，服务器已处理从processing转移到completed，客户端要自己转移自己的              --pb结构 PbCreateActivityInfo
    ///</summary>
    ACTION_CREATE_ACTIVITY_COMPLETE         = 3051,    

    ///<summary>
    ///领奖                                   --同上，收到已领奖，服务器已处理从completed转移到rewarded，客户端要自己转移自己的    --pb结构 PbCreateActivityInfo
    ///</summary>
    ACTION_CREATE_ACTIVITY_GET_REWARD       = 3052,    

    ///<summary>
    ///查询列表                                                                                                                                                                                                                                            --pb结构 PbCreateActivityList 
    ///</summary>
    ACTION_CREATE_ACTIVITY_QUERY_LIST       = 3053,    

    ///<summary>
    ///领取终极奖励     --未配置奖励，暂未定pb
    ///</summary>
    ACTION_CREATE_ACTIVITY_GET_FINAL_REWARD = 3054,    




    ///<summary>
    ///----------------------3101~3150 导师系统-------------------------------------------
    ///</summary>
    

    ///<summary>
    ///领奖
    ///</summary>
    ACTION_TUTOR_GET_REWARD                     = 3101,    

    ///<summary>
    ///日志
    ///</summary>
    ACTION_TUTOR_GET_LOGS                       = 3102,    


    ///<summary>
    ///----------------------3151~3200 装备升级-------------------------------------
    ///</summary>
	

    ///<summary>
    ///装备升级请求
    ///</summary>
    ACTION_EQUIP_UPGRADE_UPGRADE_REQ        = 3151,   

    ///<summary>
    ///装备查询请求
    ///</summary>
    ACTION_EQUIP_UPGRADE_GET_INFO_REQ       = 3152,   


    ///<summary>
    ///----------------------3200~3250 红包系统---------------------
    ///</summary>
    

    ///<summary>
    ///抢红包
    ///</summary>
    ACTION_RED_ENVELOPE_GRAB                = 3200,    

    ///<summary>
    ///    ACTION_RED_ENVELOPE_SEND_WORLD          = 3201,    --世界频道发红包
    ///</summary>


    ///<summary>
    ///    ACTION_RED_ENVELOPE_SEND_GUILD          = 3202,    --公会频道发红包
    ///</summary>


    ///<summary>
    ///获取红包链接
    ///</summary>
    ACTION_RED_ENVELOPE_GET_LIST            = 3201,    


    ///<summary>
    ///抢红包失败
    ///</summary>
    ACTION_RED_ENVELOPE_GRAB_FAIL           = 3249,    

    ///<summary>
    ///抢红包成功
    ///</summary>
    ACTION_RED_ENVELOPE_GRAB_SUCCESS        = 3250,    


    ///<summary>
    ///----------------------3251~3500 决斗系统---------------------
    ///</summary>
    


    ///<summary>
    ///进入决斗场
    ///</summary>
    ACTION_DUEL_ENTER_MAP           = 3251,     

    ///<summary>
    ///获取可邀请列表
    ///</summary>
    ACTION_DUEL_GET_INVITE_LIST     = 3252,     

    ///<summary>
    ///邀请对方决斗
    ///</summary>
    ACTION_DUEL_INVITE              = 3253,     

    ///<summary>
    ///响应对方决斗邀请（接受或拒绝）
    ///</summary>
    ACTION_DUEL_INVITE_RESP         = 3254,     

    ///<summary>
    ///决斗准备完毕
    ///</summary>
    ACTION_DUEL_READY               = 3255,     

    ///<summary>
    ///查询决斗赛玩家信息（包括自己）
    ///</summary>
    ACTION_DUEL_QUERY_PLAYER_INFO   = 3256,     

    ///<summary>
    ///向对手移动的通知
    ///</summary>
    ACTION_DUEL_MOVE                = 3257,     

    ///<summary>
    ///决斗赛结果通知
    ///</summary>
    ACTION_DUEL_MATCH_RESULT        = 3258,     

    ///<summary>
    ///查询决斗赛相关信息
    ///</summary>
    ACTION_DUEL_QUERY_INFO          = 3259,     

    ///<summary>
    ///领取本赛季达成最高段位的奖励
    ///</summary>
    ACTION_DUEL_GET_STAGE_REWARD    = 3260,     

    ///<summary>
    ///决斗赛正式开始（服务器主动发送）
    ///</summary>
    ACTION_DUEL_MATCH_START         = 3261,     

    ///<summary>
    ///获取一场决斗赛信息
    ///</summary>
    ACTION_DUEL_GET_GAMBLE_INFO     = 3262,     

    ///<summary>
    ///决斗下注，方式1（普通下注）
    ///</summary>
    ACTION_DUEL_BET1                = 3263,     

    ///<summary>
    ///决斗下注，方式2（眷顾之战下注）
    ///</summary>
    ACTION_DUEL_BET2                = 3264,     

    ///<summary>
    ///获取一场决斗赛状态
    ///</summary>
    ACTION_DUEL_GET_GAMBLE_STATE    = 3265,     

    ///<summary>
    ///决斗赛双方将会获得的奖金有变化（由服务器主动通知前端）
    ///</summary>
    ACTION_DUEL_BET_CHANGE          = 3266,     

    ///<summary>
    ///获取决斗赛记录
    ///</summary>
    ACTION_DUEL_GET_RECORD          = 3267,     


    ///<summary>
    ///----------------------3501~3550 野外任务---------------------
    ///</summary>
    

    ///<summary>
    ///接受一个野外任务
    ///</summary>
    ACTION_WILD_TASK_ACCEPT             = 3501,     

    ///<summary>
    ///询问是否开始队伍野外任务（队长）
    ///</summary>
    ACTION_WILD_TASK_ASK_ACCEPT_TEAM    = 3502,     

    ///<summary>
    ///询问是否开始队伍野外任务，队长拒绝了（队长）
    ///</summary>
    ACTION_WILD_TASK_LEADER_REFUSE_TASK = 3503,     

    ///<summary>
    ///查询野外任务信息
    ///</summary>
    ACTION_WILD_TASK_QUERY_INFO         = 3504,     

    ///<summary>
    ///放弃野外任务
    ///</summary>
    ACTION_WILD_TASK_ABANDON            = 3505,     

    ///<summary>
    ///-----------------------3551~3560 老玩家回归奖励--------------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///老玩家数据请求
    ///</summary>
    ACTION_OLD_ROLE_RETURN_INFO_REQ     = 3551,     

    ///<summary>
    ///老玩家奖励领取请求
    ///</summary>
    ACTION_OLD_ROLE_RETURN_REWARD_REQ   = 3552,     

    ///<summary>
    ///-------------------------------------------------------------------------------
    ///</summary>
    


    ///<summary>
    ///cell通知base复活成功
    ///</summary>
    ACTION_REVIVE_SUCCESS            = 5000,     

    ///<summary>
    ///cell通知base加好友好感度
    ///</summary>
    ACTION_ADD_FRIEND_DEGREE_C2B     = 5001,     

    ///<summary>
    ///cell通知base增加奖励道具
    ///</summary>
    ACTION_ADD_REWARD_ITEMS          = 5003,     

    ///<summary>
    ///cell通知base累加已通关关卡
    ///</summary>
    ACTION_ADD_FINISHED_MISSION      = 5004,     


}

}
