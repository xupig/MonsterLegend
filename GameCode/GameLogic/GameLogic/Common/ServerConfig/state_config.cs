using System;
using System.Collections.Generic;

namespace Common.ServerConfig {

        ///<summary>
        ///用来定义游戏中出现的各种状态
        ///</summary>



    public static class state_config {


        ///<summary>
        ///account实体的状态定义
        ///</summary>
    

        ///<summary>
        ///创建初始状态
        ///</summary>
        public const int    ACCOUNT_STATE_INIT            = 0;          

        ///<summary>
        ///客户端已连接到该实体
        ///</summary>
        public const int    ACCOUNT_STATE_CLIENT_ATTACHED = 1;          

        ///<summary>
        ///正在创建角色
        ///</summary>
        public const int    ACCOUNT_STATE_CREATING_CHRT   = 2;          

        ///<summary>
        ///正在删除角色
        ///</summary>
        public const int    ACCOUNT_STATE_DELETING_CHRT   = 3;          

        ///<summary>
        ///账号的角色正在进入游戏
        ///</summary>
        public const int    ACCOUNT_STATE_ENTERING_GAME   = 4;          

        ///<summary>
        ///账号的角色已进入游戏
        ///</summary>
        public const int    ACCOUNT_STATE_ENTERED_GAME    = 5;          

        ///<summary>
        ///账号处于销毁状态
        ///</summary>
        public const int    ACCOUNT_STATE_DESTROYING      = 6;          

        ///<summary>
        ///账号处于排队状态
        ///</summary>
        public const int    ACCOUNT_STATE_QUEUEING        = 7;          



        ///<summary>
        ///Avatar实体的状态定义，在cell上的状态不能大于31，在base上的状态不能超过61
        ///</summary>
    

        ///<summary>
        ///Avatar技能状态
        ///</summary>
    

        ///<summary>
        ///死亡状态
        ///</summary>
        public const int    AVATAR_STATE_DEATH            = 0;          

        ///<summary>
        ///                              1           --不可操作，客户端已占用
        ///</summary>
    

        ///<summary>
        ///时空裂隙状态
        ///</summary>
        public const int    AVATAR_STATE_SPACETIME_RIFT   = 2;          

        ///<summary>
        ///不可移动
        ///</summary>
        public const int    AVATAR_STATE_NOT_MOVE         = 11;         

        ///<summary>
        ///不可转向
        ///</summary>
        public const int    AVATAR_STATE_NOT_TURN         = 12;         

        ///<summary>
        ///无碰撞盒
        ///</summary>
        public const int    AVATAR_STATE_NOT_COLLIDE      = 13;         

        ///<summary>
        ///飞行状态
        ///</summary>
        public const int    AVATAR_STATE_FLY              = 14;         

        ///<summary>
        ///隐身状态  机关怪隐身
        ///</summary>
        public const int    AVATAR_STATE_INVISIBLE        = 15;         

        ///<summary>
        ///电梯状态
        ///</summary>
        public const int    AVATAR_STATE_IN_LIFT          = 16;         

        ///<summary>
        ///                              17          --客户端已用 --cg怪隐身
        ///</summary>
    

        ///<summary>
        ///停掉ai,播放cg的时候，所有实体加这个buff
        ///</summary>
        public const int    AVATAR_STATE_STOP_AI          = 18;         

        ///<summary>
        ///不可受击
        ///</summary>
        public const int    AVATAR_STATE_NOT_ATTACK       = 19;         

        ///<summary>
        ///不可视
        ///</summary>
        public const int    AVATAR_STATE_HIDE             = 20;         

        ///<summary>
        ///不可视,不可受击
        ///</summary>
        public const int    AVATAR_STATE_DRIVER_HIDE      = 21;         

        ///<summary>
        ///决斗场可下注效果（头顶按钮）
        ///</summary>
        public const int    AVATAR_STATE_DUEL_BET         = 22;         

        ///<summary>
        ///决斗场正在战斗效果（头顶刀）
        ///</summary>
        public const int    AVATAR_STATE_DUEL_FIGHT       = 23;         

        ///<summary>
        ///跟随状态
        ///</summary>
        public const int    AVATAR_STATE_CHASE            = 24;         

        ///<summary>
        ///战斗状态
        ///</summary>
        public const int    AVATAR_STATE_FIGHTING         = 25;         

        ///<summary>
        ///死亡灵魂状态 死亡且复活次数用完 组队本生效(关卡表team_type = 2)
        ///</summary>
        public const int    AVATAR_STATE_DEAD_GHOST       = 26;         

        ///<summary>
        ///队伍跟随状态
        ///</summary>
        public const int    AVATAR_STATE_TEAM_FOLLOW      = 29;         

        ///<summary>
        ///组队状态
        ///</summary>
        public const int    AVATAR_STATE_TEAMING          = 30;         

        ///<summary>
        ///是否队长
        ///</summary>
        public const int    AVATAR_STATE_TEAM_LEADER      = 31;         

        ///<summary>
        ///技能状态结束。注意这个放技能状态最后
        ///</summary>
        public const int    AVATAR_STATE_SPELL_MAX        = 32;         

    

        ///<summary>
        ///Avatar base上的状态定义
        ///</summary>
    

        ///<summary>
        ///传送状态
        ///</summary>
        public const int    AVATAR_STATE_IN_TELEPORT      = 32;          

        ///<summary>
        ///进入某个场景
        ///</summary>
        public const int    AVATAR_STATE_ENTERING_SPACE   = 33;          

        ///<summary>
        ///正在销毁
        ///</summary>
        public const int    AVATAR_STATE_DESTROYING       = 34;          

        ///<summary>
        ///正在创建cell
        ///</summary>
        public const int    AVATAR_STATE_CREATING_CELL    = 35;          

        ///<summary>
        ///在打单机副本
        ///</summary>
        public const int    AVATAR_STATE_CONSOLE          = 36;          

        ///<summary>
        ///处于跨服状态
        ///</summary>
        public const int    AVATAR_STATE_CROSSING         = 37;          

        ///<summary>
        ///玩家正从跨服回到本服
        ///</summary>
        public const int    AVATAR_STATE_CROSS_TO_ORIGIN  = 38;          

        ///<summary>
        ///玩家的战斗属性是否已初始化
        ///</summary>
        public const int    AVATAR_STATE_BA_INITED        = 39;          

        ///<summary>
        ///正在向跨服服务器申请副本
        ///</summary>
        public const int    AVATAR_STATE_APPLY_CROSS_INST = 40;          
    
    

        ///<summary>
        ///AI的状态定义
        ///</summary>
    
        public const int    AI_STATE_THINK               = 1;
        public const int    AI_STATE_REST                = 2;
        public const int    AI_STATE_PATROL              = 3;
        public const int    AI_STATE_ESCAPE              = 4;
        public const int    AI_STATE_SPELL_CD            = 6;
        public const int    AI_STATE_FORCE_BACK          = 8;
        public const int    AI_STATE_FORCE_FOLLOW        = 9;


        ///<summary>
        ///公会副本关卡状态（0代表未开启，-1代表正在开启，注意：正数有其他作用，状态数值只能使用负数）
        ///</summary>
    

        ///<summary>
        ///未开启
        ///</summary>
        public const int    GUILD_MISSION_UNOPEN    = 0;        

        ///<summary>
        ///开启中
        ///</summary>
        public const int    GUILD_MISSION_OPENING   = -1;       

        ///<summary>
        ///已结束（但还未关闭）
        ///</summary>
        public const int    GUILD_MISSION_COMPLETE  = -2;       
    }



        ///<summary>
        ///-----------------------------------------------------
        ///</summary>

}
