﻿using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.States
{
    public static class ControlStickState
    {
        //摇杆方向
        public static Vector2 direction = Vector2.zero;
        //摇杆力度
        public static float strength = 0;

        public static bool isDragging = false;

        public static int fingerId = -1;

        private static bool _canUse = true;
        public static bool canUse
        {
            get { return _canUse; }
            set
            {
                _canUse = value;
            }
        }


        public static Vector3 GetMoveDirectionByControlStick()
        {
            if (Camera.main == null) return Vector3.zero;
            Vector3 moveRight = Camera.main.transform.right;
            moveRight.y = 0;
            moveRight.Normalize();

            Vector3 moveForward = Camera.main.transform.forward;
            moveForward.y = 0;
            moveForward.Normalize();

            Vector3 moveDirection = moveRight * ControlStickState.direction.x +
                                    moveForward * ControlStickState.direction.y;
            moveDirection.Normalize();
            return moveDirection;
        }

        public static Vector3 GetMoveDirectionByMouse()
        {
            RaycastHit hit;
            int _terrainLayerValue = 1 << UnityEngine.LayerMask.NameToLayer("Terrain");
            
            //如果主摄像机被关闭时返回0
            if (Camera.main == null)
            {
                //Ray ray = UIManager.Instance.UICamera.camera.ScreenPointToRay(Input.mousePosition);
                return Vector3.zero;
            }

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            bool result = Physics.Raycast(ray, out hit, 1000, _terrainLayerValue);
            var moveDirection = hit.point;
            return moveDirection;
            //return Input.mousePosition;
        }
    }
}
