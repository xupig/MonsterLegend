using System;
using System.Collections.Generic;

using GameLoader.Utils;

namespace Common.States
{
    public abstract class AbsState : IState
    {
        protected IStateController _controller;
        private Action<IState> _callback;
        public abstract void Start( );
        public abstract IState NextState( );

        public void SetController(IStateController controller)
        {
            _controller = controller;
        }

        protected virtual void Complete( )
        {
            this.Destroy();
            this.AddNextState();
            if ( _callback != null )
            {
                _callback( this );
            }
        }

        public void AddCompleteCallback(Action<IState> callback)
        {
            if ( _callback == null )
            {
                _callback = callback;
            }
            else
            {
                _callback += callback;
            }
        }
        public virtual void AddNextState( )
        {
            IState _state = this.NextState();
            if ( _state != null )
            {
                _controller.AddState( _state );
            }
            else
            {
                UnityEngine.Debug.Log( "NextState: Null" );
            }
        }

        public virtual void Destroy( )
        {

        }
    }

}

