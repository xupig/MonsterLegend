﻿using System;

namespace Common.States
{

    public interface IState
    {
        void Start( );
        void SetController(IStateController _controller);
        void AddCompleteCallback(Action<IState> _callback);
        void Destroy( );
    }
}
