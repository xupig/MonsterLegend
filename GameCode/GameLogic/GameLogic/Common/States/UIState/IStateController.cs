
using System;
using System.Collections.Generic;

namespace Common.States
{

    public interface IStateController
    {
        void Start( );
        void Clear( );
        void AddState(IState _state);
    }

}
