using System;
using System.Collections.Generic;

namespace Common.States
{
    public abstract class AbsStateController : IStateController
    {
        protected IState _curState;
        private Stack<IState> _stateStack = new Stack<IState>();

        public abstract void Start( );
        public void Clear( )
        {
            if ( _curState != null )
            {
                _curState.Destroy();
                _curState = null;
            }
            _stateStack.Clear();
        }

        protected void StartState(IState _state)
        {
            _curState = _state;
            _curState.SetController( this );
            _state.AddCompleteCallback( CheckNextState );
            _state.Start();
        }

        protected void CheckNextState(IState _preState)
        {
            if ( _stateStack.Count > 0 )
            {
                IState _state = _stateStack.Pop();
                StartState( _state );
            }
            else
            {
                _curState = null;
                this.Complete();
            }
        }

        public void AddStateList(List<IState> _list)
        {
            if ( _list != null )
            {
                for ( int i = _list.Count - 1; i >= 0; i-- )
                {
                    AddState( _list[i] );
                }
            }
        }

        public virtual void AddState(IState _state)
        {
            if ( _state != null )
            {
                _stateStack.Push( _state );
            }
        }

        private Action _callback;

        public void AddNextState( )
        {
        }

        public void SetController(IStateController _controller)
        {
        }

        public void AddCompleteCallback(Action callback)
        {
            if ( _callback == null )
            {
                _callback = callback;
            }
            else
            {
                _callback += callback;
            }
        }

        protected void Complete( )
        {
            this.Destroy();
            if ( _callback != null )
            {
                _callback();
            }
        }

        public void Destroy( )
        {
            this.Clear();
        }
    }

}