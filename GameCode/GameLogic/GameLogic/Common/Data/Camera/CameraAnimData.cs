namespace Common.Data
{
    public class CameraAnimData 
	{
        public int priority { get; set; }

        public float xSwing { get; set; }
        public float xRate { get; set; }

        public float ySwing { get; set; }
        public float yRate { get; set; }

        public float zSwing { get; set; }
        public float zRate { get; set; }

        public int xDir { get; set; }
        public int yDir { get; set; }
        public int zDir { get; set; }

        public float xRateA { get; set; }
        public float yRateA { get; set; }
        public float zRateA { get; set; }

        public float attenuateOddsX { get; set; }
        public float attenuateOddsY { get; set; }
        public float attenuateOddsZ { get; set; }

        public float xSpeed;
        public float ySpeed;
        public float zSpeed;

        public void UpdateSpeed(ref float xSpeed, ref float ySpeed, ref float zSpeed, float dt)
        {
            float xSpeedTemp = (xRate + dt * xRateA) * 4 * xSwing;
            float ySpeedTemp = (yRate + dt * yRateA) * 4 * ySwing;
            float zSpeedTemp = (zRate + dt * zRateA) * 4 * zSwing;

            if (xSpeed > 0) xSpeed = xSpeedTemp;
            else xSpeed = -xSpeedTemp;

            if (ySpeed > 0) ySpeed = ySpeedTemp;
            else ySpeed = -ySpeedTemp;

            if (zSpeed > 0) zSpeed = zSpeedTemp;
            else zSpeed = -zSpeedTemp;
        }
	}
}
