﻿#region 模块信息
/*==========================================
// 文件名：FunctionData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Function
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/25 15:47:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.GlobalManager;
using ModuleMainUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class FunctionData
    {
        public HashSet<int> openedFunctionSet;
        public HashSet<int> pointFunctionSet;
        public List<int> noticeList;

        private int tweenId;
        public List<int> waitList;

        public List<function> activityList;

        public FunctionData()
        {
            activityList = new List<function>();
            noticeList = new List<int>();
            openedFunctionSet = new HashSet<int>();
            pointFunctionSet = new HashSet<int>();
            waitList = new List<int>();
        }

        public void SortFunctionList()
        {
            activityList.Sort(SortFunc);
        }

        public int AddFunctionData(List<function> functionList,function function)
        {
            if (functionList.Contains(function) == false)
            {
                int result = functionList.Count;
                for (int i = 0; i < functionList.Count; i++)
                {
                    if (function.__button_sequence < functionList[i].__button_sequence)
                    {
                        result = Math.Min(i, result);
                    }
                }
                functionList.Insert(result, function);
                return result;
            }
            else
            {
                return functionList.IndexOf(function);
            }
        }

        private int SortFunc(function func1, function func2)
        {
            return func1.__button_sequence - func2.__button_sequence;
        }

        public void InitFunction(List<uint> dataList)
        {
            activityList.Clear();
            
            for (int i = 0; i < dataList.Count; i++)
            {
                function function = function_helper.GetFunction((int)dataList[i]);
                if (openedFunctionSet.Contains((int)dataList[i]) == false)
                {
                    openedFunctionSet.Add((int)dataList[i]);
                    if (function.__effect_function != 0)
                    {
                        CheckSubFunction(function.__effect_function);
                    }
                }

                if (function != null)
                {
                    if (function.__type == 2)
                    {
                        activityList.Add(function);
                    }
                    if (function.__effect_function != 0)
                    {
                        function parentFunction = function_helper.GetFunction(function.__effect_function);
                        if (parentFunction.__type == 2)
                        {
                            activityList.Add(parentFunction);
                        }
                    }
                }
            }
            SortFunctionList();
        }

        public void RemoveFunction(int functionId)
        {
            function function = function_helper.GetFunction((int)functionId);
            if (openedFunctionSet.Contains(functionId))
            {
                openedFunctionSet.Remove(functionId);
                pointFunctionSet.Remove(functionId);
                function func = function_helper.GetFunction(functionId);
                activityList.Remove(func);
                waitList.Remove(functionId);
                if (function.__effect_function != 0)
                {
                    CheckSubFunction(function.__effect_function);
                }
                EventDispatcher.TriggerEvent(FunctionEvents.UPDATE_FUNCTION, functionId);
            }
        }

        public void AddFunction(uint functionId)
        {
            openedFunctionSet.Add((int)functionId);
            function function = function_helper.GetFunction((int)functionId);
            if (function.__effect_function != 0 && openedFunctionSet.Contains(function.__effect_function) == false)
            {
                AddFunction((uint)function.__effect_function);
            }
            open open = activity_helper.GetOpenByFunction(function.__id);
            if(open!=null)
            {
                if(open.__type == 1)
                {
                    PlayerDataManager.Instance.ActivityData.hasNewChallenge = true;
                }
                else if (open.__type == 2)
                {
                    PlayerDataManager.Instance.ActivityData.hasNewActivity = true;
                }
            }
            if (function.__preview == 0)//|| (function.__type == 3 && !CameraManager.isFortressShow) || (function.__type != 3 && GameSceneManager.GetInstance().curMapType != public_config.MAP_TYPE_CITY)
            {
                EventDispatcher.TriggerEvent(FunctionEvents.NO_PREVIEW_FUNCATION_OPEN,(int)functionId);
                DramaTrigger.OpenFunction((int)functionId);
            }
            else
            {
                if (tweenId != 0 || GameSceneManager.GetInstance().CanShowNotice() == false)
                {
                    waitList.Add((int)functionId);
                }
                else
                {
                    tweenId = (int)functionId;
                    PanelIdEnum.Function.Show( tweenId);
                }
            }
            EventDispatcher.TriggerEvent<int>(FunctionEvents.ADD_FUNTION, (int)functionId);
        }

         public void AddFunctionPoint(int functionId,bool isFunction)
        {
             if(isFunction)
             {
                 AddFunctionPoint(functionId);
             }
             else
             {
                 RemoveFunctionPoint(functionId);
             }
        }

        private void AddFunctionPoint(int functionId)
         {
             if (pointFunctionSet.Contains(functionId) == false)
             {
                 function function = function_helper.GetFunction(functionId);
                 if(function.__type == 1 || function.__type == 4  || function.__type == 5 || function.__type == 6)
                 {
                     noticeList.Insert(0,functionId);
                 }
                 pointFunctionSet.Add(functionId);
                 EventDispatcher.TriggerEvent<int, bool>(FunctionEvents.SET_FUNTION_POINT, functionId, true);
                 EventDispatcher.TriggerEvent(FunctionEvents.UPDATE_NOTICE);
             }
         }

        public void RemoveFunctionPoint(int functionId)
        {
            if (pointFunctionSet.Contains(functionId) == true)
            {
                function function = function_helper.GetFunction(functionId);
                if (function.__type == 1 || function.__type == 4 || function.__type == 5 || function.__type == 6)
                {
                    noticeList.Remove(functionId);
                }
                pointFunctionSet.Remove(functionId);
                EventDispatcher.TriggerEvent<int, bool>(FunctionEvents.SET_FUNTION_POINT, functionId, false);
                EventDispatcher.TriggerEvent(FunctionEvents.UPDATE_NOTICE);
            }
        }

        public void AddFunctionPoint(FunctionId functionId)
        {
            AddFunctionPoint((int)functionId);
        }

        public void RemoveFunctionPoint(FunctionId functionId)
        {
            RemoveFunctionPoint((int)functionId);
        }

        public bool IsFunctionPoint(FunctionId functionId)
        {
            return pointFunctionSet.Contains((int)functionId);
        }

        public bool IsFunctionOpen(FunctionId functionId)
        {
            return IsFunctionOpen((int)functionId);
        }

        public bool IsFunctionOpen(int functionId)
        {
            return openedFunctionSet.Contains((int)functionId);
        }

        public void CheckFunctionWaitList()
        {
            if (waitList.Count > 0)
            {
                tweenId = waitList[0];
                waitList.RemoveAt(0);
                PanelIdEnum.Function.Show(tweenId);
            }
        }

        public void TweenEnd(int tweenEndId)
        {
            if(tweenId == tweenEndId)
            {
                DramaTrigger.OpenFunction(tweenId);
                 if(waitList.Count>0)
                {
                    tweenId = waitList[0];
                    waitList.RemoveAt(0);
                    PanelIdEnum.Function.Show( tweenId);
                }
                 else
                 {
                     tweenId = 0;
                     //PlayerDataManager.Instance.TaskData.CheckAcceptableTask();
                 }
            }
        }

        private void CheckSubFunction(int functionId)
        {
            List<int> functionList = function_helper.GetFunctionTabList(functionId);
            for (int i = 0; i < functionList.Count; i++)
            {
                if (IsFunctionOpen(functionList[i]))
                {
                    AddFunction((uint)functionId);
                    return;
                }
            }
            RemoveFunction(functionId);
        }

    }
}
