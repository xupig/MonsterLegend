﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class VipData
    {
        public VipRewardState vipRewardState=VipRewardState.RewardAvailable;
       // public int neededDaimond=100;
    }

    public enum VipRewardState
    {
        /// <summary>
        ///尚未达到条件， 还不能领取
        /// </summary>
        RewardLocked=0,
        /// <summary>
        /// 可领取
        /// </summary>
        RewardAvailable=1,
        ///已领取
        RewardReceived=2,
    }
}
