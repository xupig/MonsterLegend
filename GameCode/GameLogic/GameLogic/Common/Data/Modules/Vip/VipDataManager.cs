﻿using Common.Structs.ProtoBuf;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class VipDataManager
    {
        //最大vip等级对应的全局表的id
        private const int MAX_VIP_LEVEL_ID = 73;
        //最大vip等级（假设不超过99）
        public static int MAX_VIP_LEVEL
        {
            get { return int.Parse(global_params_helper.GetGlobalParam(MAX_VIP_LEVEL_ID)); }
        }
        //当前vip等级
        public int CurrenVipLevel
        {
            get { return PlayerAvatar.Player.vip_level; }
        }
        //Dictionary<vipId,vipData>
        private Dictionary<int, VipData> vipDataDictionary;
        public VipDataManager()
        {
            vipDataDictionary = new Dictionary<int, VipData>();
            //初始化默认数据
            InitializeData();
        }

        private void InitializeData()
        {
            for (int index = 0; index < MAX_VIP_LEVEL; index++)
            {
                AddData(index + 1);
            }
        }

        private void AddData(int id)
        {
            vipDataDictionary.Add(id, new VipData());
        }

        public void RefleshVipData(PbVIPRewardRecord vipRewardRecord)
        {
            int vipLevel;
            PbVIPRewardInfo pbVIPRewardInfo;
            for (int index = 0; index < MAX_VIP_LEVEL; index++)
            {
                vipLevel = index + 1;
                if (!vipDataDictionary.ContainsKey(vipLevel))
                {
                    vipDataDictionary.Add(vipLevel, new VipData());
                }
                pbVIPRewardInfo = FindRewardInfoByVipLevel(index + 1, vipRewardRecord);
                if (pbVIPRewardInfo == null)//返回值为空可能是可领取或者未开启
                {
                    pbVIPRewardInfo = new PbVIPRewardInfo();
                    pbVIPRewardInfo.vip_rewards_flag = 1;
                }
                ExchangeServerFlag(vipLevel, pbVIPRewardInfo);
                vipDataDictionary[vipLevel].vipRewardState = (VipRewardState)pbVIPRewardInfo.vip_rewards_flag;
            }
        }

        private PbVIPRewardInfo FindRewardInfoByVipLevel(int vipLevel,PbVIPRewardRecord vipRewardRecord)
        {
            if (vipRewardRecord.vip_record == null)
                return null;
            for (int index = 0; index < vipRewardRecord.vip_record.Count; index++)
            {
                if (vipRewardRecord.vip_record[index].vip_level == vipLevel)
                    return vipRewardRecord.vip_record[index];
            }
            return null;
        }

        private void ExchangeServerFlag(int vipLevel , PbVIPRewardInfo pbVIPRewardInfo)
        {
          
            if (vipLevel > CurrenVipLevel)//尚未开启
            {
                pbVIPRewardInfo.vip_rewards_flag = 0;
            }
            else if (pbVIPRewardInfo.vip_rewards_flag == 0)//已领取
            {
                pbVIPRewardInfo.vip_rewards_flag = 2;
            }
            else
            {
                pbVIPRewardInfo.vip_rewards_flag = 1;
            }
        }

        public VipData GetVipDataById(int vipId)
        {
            return vipDataDictionary[vipId];
        }

    }
}
