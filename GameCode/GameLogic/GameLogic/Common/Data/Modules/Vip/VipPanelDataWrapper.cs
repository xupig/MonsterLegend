﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class VipPanelDataWrapper
    {
        public int subViewIndex;
        public int vipLevel; // 这个字段用于打开vip面板

        public VipPanelDataWrapper(int subViewIndex, int vipLevel)
        {
            this.subViewIndex = subViewIndex;
            this.vipLevel = vipLevel;
        }
    }


}
