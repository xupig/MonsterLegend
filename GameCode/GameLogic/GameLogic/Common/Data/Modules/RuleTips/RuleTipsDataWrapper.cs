﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public enum RuleTipsDirection
    {
        Top = 1,
        Right = 2,
        Bottom = 3,
        Left = 4,
        TopRight = 5,
        TopLeft = 6,
        BottomRight = 7,
        BottomLeft = 8,
    }

    public class RuleTipsDataWrapper
    {
        public String Content;
        public RectTransform Rect;
        public RuleTipsDirection Direction;

        public RuleTipsDataWrapper(string content, RectTransform rect, RuleTipsDirection direction)
        {
            this.Content = content;
            this.Rect = rect;
            this.Direction = direction;
        }
    }
}
