﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:08:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using GameData;
using UnityEngine;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils.Timer;
using Common.Utils;

namespace Common.Data
{
    public class DreamlandData
    {

        private static uint _timerId = TimerHeap.INVALID_ID;

        private Dictionary<int, DreamlandInfo> _dreamlandDict = new Dictionary<int, DreamlandInfo>();
        private DreamLandInfoLocal _dreamLandInfoLocal;
        public bool SendedHaveReward = false;

        public bool HaveNewRecord = false;

        public DreamlandData()
        {
            AddListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(DreamlandEvents.ReceiveDreamlandData, OnReceiveDreamlandData);
            EventDispatcher.AddEventListener(DreamlandEvents.ReceiveFinishExplore, OnReceiveDreamlandFinish);
        }

        private void OnReceiveDreamlandData()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer(0, 30000, CheckHaveReward);
            }
        }

        private void OnReceiveDreamlandFinish()
        {
            SendedHaveReward = false;
        }

        private void CheckHaveReward()
        {
            if (!SendedHaveReward)
            {
                DreamlandData data = PlayerDataManager.Instance.DreamlandData;
                if (data.HaveReward())
                {
                    SystemInfoManager.Instance.ShowFastNotice(66017);
                    SendedHaveReward = true;
                }
            }
        }


        private DreamlandInfo GetChaperInfo(int id)
        {
            if (_dreamlandDict.ContainsKey(id))
            {
                return _dreamlandDict[id];
            }
            DreamlandInfo chapterInfo = new DreamlandInfo(id);
            _dreamlandDict.Add(id, chapterInfo);
            return chapterInfo;
        }

        public void ClearChapterDict()
        {
            _dreamlandDict.Clear();
        }

        public void InitConfigChapterDict()
        {
            List<int> todayDreamlandIdList = dreamland_explore_date_helper.GetTodayDreamlandIdList();
            for (int i = 0; i < todayDreamlandIdList.Count; i++)
            {
                DreamlandInfo chapterInfo = new DreamlandInfo(todayDreamlandIdList[i]);
                _dreamlandDict.Add(todayDreamlandIdList[i], chapterInfo);
            }
        }

        public void FillChapterList(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo pbDreamlandInfo = chapterList.dream_land[i];
                int dreamlandId = (int)pbDreamlandInfo.land_id;
                DreamlandInfo chapterInfo = null;
                if (_dreamlandDict.ContainsKey(dreamlandId))
                {
                    chapterInfo = _dreamlandDict[dreamlandId];
                }
                else
                {
                    chapterInfo = new DreamlandInfo(dreamlandId);
                    _dreamlandDict.Add(dreamlandId, chapterInfo);
                }
                chapterInfo.Fill(pbDreamlandInfo);
            }
        }

        public List<DreamlandInfo> GetDreamlandInfoList()
        {
            List<DreamlandInfo> chapterInfoList = new List<DreamlandInfo>();
            foreach (DreamlandInfo chapterInfo in _dreamlandDict.Values)
            {
                chapterInfoList.Add(chapterInfo);
            }
            chapterInfoList.Sort(SortByIsYestodayData);
            return chapterInfoList;
        }

        private int SortByIsYestodayData(DreamlandInfo x, DreamlandInfo y)
        {
            if ((x.IsYestodayData) && (!y.IsYestodayData))
            {
                return -1;
            }
            return 1;
        }

        public void FillWhenFinishExplore(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenFinishExplore(dreamlandInfo);
            }
        }

        public void FillWhenStartExplore(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenStartExplore(dreamlandInfo);
            }
        }

        public void FillWhenMinusTime(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenMinusTime(dreamlandInfo);
            }
        }


        public void FillWhenFinishImmediate(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int)dreamlandInfo.land_id);
                chapterInfo.FillWhenFinishImmediate(dreamlandInfo);
            }
        }

        public void FillWhenDragonQualityUp(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenDragonQualityUp(dreamlandInfo);
            }
        }

        public void FillWhenRefreshPlayerList(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenRefreshPlayerList(dreamlandInfo);
            }
        }

        public void FillWhenBuyRobTimes(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenBuyRobTimes(dreamlandInfo);
            }
        }

        public void FillWhenClearRobCooldown(PbDreamlandInfoList chapterList)
        {
            for (int i = 0; i < chapterList.dream_land.Count; i++)
            {
                PbDreamlandInfo dreamlandInfo = chapterList.dream_land[i];
                DreamlandInfo chapterInfo = GetChaperInfo((int) dreamlandInfo.land_id);
                chapterInfo.FillWhenClearRobCooldown(dreamlandInfo);
            }
        }

        public void ReceiveRetrieveReward()
        {
            //因为昨天最多一个dreamlandId，所以全部清空
            foreach (DreamlandInfo dreamlandInfo in _dreamlandDict.Values)
            {
                dreamlandInfo.CanRetrieveReward = false;
            }
        }

        public void ClearQualityUp()
        {
            foreach (DreamlandInfo dreamlandInfo in _dreamlandDict.Values)
            {
                dreamlandInfo.IsQualityUp = false;
            }
        }

        public bool CanExplore()
        {
            function functionData = function_helper.GetFunction((int)FunctionId.dreamland);
            if (activity_helper.IsOpen(functionData))
            {
                foreach (DreamlandInfo dreamlandInfo in _dreamlandDict.Values)
                {
                    if (dreamlandInfo.CanExplore())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool HaveReward()
        {
            function functionData = function_helper.GetFunction((int)FunctionId.dreamland);
            if (activity_helper.IsOpen(functionData))
            {
                foreach (DreamlandInfo dreamlandInfo in _dreamlandDict.Values)
                {
                    if (dreamlandInfo.HaveReward())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public DreamLandInfoLocal GetDreamLandInfoLocal()
        {
            if (_dreamLandInfoLocal == null)
            {
                _dreamLandInfoLocal = LocalCache.Read<DreamLandInfoLocal>(LocalName.Dreamland);
            }
            if (_dreamLandInfoLocal == null)
            {
                _dreamLandInfoLocal = new DreamLandInfoLocal();
                _dreamLandInfoLocal.exploreTime = 0;
                _dreamLandInfoLocal.timeList = new List<uint>();
            }
            return _dreamLandInfoLocal;
        }

        public void SaveDreamLandInfoLocal()
        {
            LocalCache.Write(LocalName.Dreamland, _dreamLandInfoLocal, CacheLevel.Permanent);
        }
    }


    public class DreamLandInfoLocal
    {
        public int exploreTime;
        public List<uint> timeList;
    }
}
