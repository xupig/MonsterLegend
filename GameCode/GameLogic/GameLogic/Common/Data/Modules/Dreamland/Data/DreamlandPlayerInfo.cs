﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 19:12:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using UnityEngine;
using GameLoader.Utils;

namespace Common.Data
{
    public enum DreamlandPlayerRobState
    {
        cannotRob,
        canRob,
        hadRobed,
    }

    public class DreamlandPlayerInfo
    {
        public UInt64 Dbid;
        public string Name;
        public string GuildName;
        public int DragonQuality;
        public DreamlandPlayerRobState CurrentState;
        public int BeRobedTimes;
        public int RobedCanGetGold;
        public int RobedCanGetExp;
        public int FightValue;
        public int Level;
        public int BeRobedByMeTimes;

        public DreamlandPlayerInfo()
        {
        }

        public void FillBaseInfo(PbVisibleInfo info)
        {
            Dbid = info.dbid;
            Name = MogoProtoUtils.ParseByteArrToString(info.name_bytes);
            DragonQuality = info.dragon_quality == 0 ? public_config.DREAMLAND_QUALITY_GREEN : (int)info.dragon_quality;
            Level = (int) info.level;
        }

        public void FillDetailInfo(PbVisibleInfo info)
        {
            FillBaseInfo(info);
            GuildName = MogoProtoUtils.ParseByteArrToString(info.guild_name_bytes);
            if (info.cur_state > (int)DreamlandPlayerRobState.hadRobed)
            {
                CurrentState = DreamlandPlayerRobState.hadRobed;
            }
            else
            {
                CurrentState = (DreamlandPlayerRobState)info.cur_state;
            }
            BeRobedTimes = (int)info.convoy_atk_count;
            RobedCanGetGold = (int)info.convoy_atk_gold;
            RobedCanGetExp = (int)info.convoy_atk_exp;
            FightValue = (int)info.fight_force;
            BeRobedByMeTimes = Mathf.Max(0, (int)info.cur_state - 1);
            //LoggerHelper.Debug(this);
        }

        public override string ToString()
        {
            return string.Format("dbid:{0}  name:{1}    quality:{2} level:{3}", Dbid, Name, DragonQuality, Level);
        }

        public void Print()
        {
            //Debug.LogError(string.Format("dbid:{0}  name:{1} DragonQuality:{2}  Level:{3}", Dbid, Name, DragonQuality, Level));
        }
    }
}
