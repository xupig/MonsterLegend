﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 19:24:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using UnityEngine;
using GameData;
using GameMain.GlobalManager;
using Common.Base;

using Common.Utils;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

namespace Common.Data
{
    public enum DreamlandChapterState
    {
        unexplore,
        exploring,
        haveReward,
    }

    public class DreamlandInfo
    {
        public int Id = 0;
        /// <summary>
        /// 最小值为0，代表处于第一站点
        /// 最大值为10，代表完成第十个站点
        /// </summary>
        public int CurrentStationId = 0;
        public int FinishStationId = 0;
        public uint EndTime = 0;
        public int RobTimes = 0;
        public uint NextCanRobTime = 0;
        public int RevengeTimes = 0;
        private int _dragonQuality = public_config.DREAMLAND_QUALITY_GREEN;
        public int DragonQuality
        {
            get
            {
                return _dragonQuality;
            }
            set
            {
                if(value == 0)
                {
                    _dragonQuality = public_config.DREAMLAND_QUALITY_GREEN;
                }
                else
                {
                    _dragonQuality = value;
                }
            }
        }
        public List<DreamlandPlayerInfo> PlayerDataList = new List<DreamlandPlayerInfo>();
        public int BeRobedTimes = 0;
        public List<PBDreamlandItem> RewardList = new List<PBDreamlandItem>();
        public List<PBDreamlandItem> TreasureList = new List<PBDreamlandItem>();
        public bool IsYestodayData = false;
        public bool CanRetrieveReward = false;
        public bool IsQualityUp = false;
        public int TotalRobTimes;

        public DreamlandInfo(int id)
        {
            this.Id = id;
            TotalRobTimes = dreamland_explore_base_helper.GetDailyMaxRobTimes(id);
        }

        public void Fill(PbDreamlandInfo info)
        {
            CurrentStationId = (int) info.cur_station_id;
            EndTime = info.finish_stime;
            RobTimes = (int) info.atk_count;
            NextCanRobTime = info.atk_stime;
            RevengeTimes = (int) info.revenge_count;
            DragonQuality = (int) info.dragon_quality;
            BeRobedTimes = (int) info.convoy_atk_count;
            CanRetrieveReward = info.is_award_retrieve == 1;
            TotalRobTimes = (int) info.sum_atk_count;
            IsYestodayData = info.is_today_info == 0;
            AddPlayerDataList(info);
            Print();
        }

        private void AddPlayerDataList(PbDreamlandInfo info)
        {
            PlayerDataList.Clear();
            //第一个飞龙是自己
            AddMyPlayerData(info);
            for (int i = 0; i < info.visible_list.Count; i++)
            {
                DreamlandPlayerInfo playerInfo = new DreamlandPlayerInfo();
                playerInfo.FillBaseInfo(info.visible_list[i]);
                PlayerDataList.Add(playerInfo);
            }
            LoggerHelper.Debug(PlayerDataList.Count);
            for (int i = 0; i < PlayerDataList.Count; i++)
            {
                LoggerHelper.Debug(PlayerDataList[i]);
            }
        }

        private void AddMyPlayerData(PbDreamlandInfo info)
        {
            DreamlandPlayerInfo playerInfo = GetMyPlayerData();
            PlayerDataList.Add(playerInfo);
        }

        public DreamlandPlayerInfo GetMyPlayerData()
        {
            DreamlandPlayerInfo playerInfo = new DreamlandPlayerInfo();
            playerInfo.Dbid = PlayerAvatar.Player.dbid;
            playerInfo.Name = PlayerAvatar.Player.name;
            playerInfo.DragonQuality = DragonQuality;
            playerInfo.Level = PlayerAvatar.Player.level;
            //playerInfo.GuildName = PlayerDataManager.Instance.GuildData.MyGuildData.GuildName;
            //playerInfo.BeRobedTimes = BeRobedTimes;
            //playerInfo.CurrentState = DreamlandPlayerRobState.cannotRob;
            //playerInfo.FightValue = (int) PlayerAvatar.Player.fight_force;
            return playerInfo;
        }

        public void FillWhenStartExplore(PbDreamlandInfo info)
        {
            CurrentStationId = (int) info.cur_station_id;
            EndTime = info.finish_stime;
            AddPlayerDataList(info);
            Print();
        }

        public void FillWhenFinishExplore(PbDreamlandInfo info)
        {
            FinishStationId = CurrentStationId;
            CurrentStationId = (int) info.cur_station_id;
            _dragonQuality = public_config.DREAMLAND_QUALITY_GREEN;
            BeRobedTimes = (int)info.convoy_atk_count;
            EndTime = info.finish_stime;
            RewardList = info.item;
            TreasureList = info.extra_item;
            RobTimes = (int)info.atk_count;
            TotalRobTimes = (int)info.sum_atk_count;
            IsYestodayData = info.is_today_info == 0;
            PlayerDataList.Clear();
            Print();
        }

        public void FillWhenMinusTime(PbDreamlandInfo info)
        {
            EndTime = info.finish_stime;
            Print();
        }

        public void FillWhenFinishImmediate(PbDreamlandInfo dreamlandInfo)
        {
            EndTime = Global.Global.serverTimeStampSecond;
            Print();
        }

        public void FillWhenDragonQualityUp(PbDreamlandInfo info)
        {
            if (DragonQuality == (int)info.dragon_quality)
            {
                IsQualityUp = false;
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37802), PanelIdEnum.Dreamland);
            }
            else if ((int)info.dragon_quality > DragonQuality)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37803), PanelIdEnum.Dreamland);
                IsQualityUp = true;
            }
            else
            {
                IsQualityUp = false;
            }
            DragonQuality = (int) info.dragon_quality;
            Print();
        }

        public void FillWhenRefreshPlayerList(PbDreamlandInfo info)
        {
            AddPlayerDataList(info);
            Print();
        }

        public void FillWhenClearRobCooldown(PbDreamlandInfo info)
        {
            NextCanRobTime = 0;
            Print();
        }

        public void FillWhenBuyRobTimes(PbDreamlandInfo info)
        {
            RobTimes = (int) info.atk_count;
            TotalRobTimes = (int) info.sum_atk_count;
            Print();
        }

        public DreamlandChapterState GetState()
        {
            //if (IsYestodayData)
            //{
            //    return DreamlandChapterState.haveReward;
            //}
            if (EndTime == 0)
            {
                return DreamlandChapterState.unexplore;
            }
            if (Global.Global.serverTimeStampSecond >= EndTime)
            {
                return DreamlandChapterState.haveReward;
            }
            return DreamlandChapterState.exploring;
        }

        public void Print()
        {
            LoggerHelper.Debug(string.Format("Id:{0}  CurrentStationId:{1} serverTimeStampSecond:{8} EndTime:{2} count:{9} RobTimes:{3}  NextCanRobTime:{4}  RevengeTimes:{5}  DragonQuality:{6}  BeRobedTimes:{7}  CanRetrieveReward:{10}", Id, CurrentStationId, EndTime, RobTimes, NextCanRobTime, RevengeTimes, DragonQuality, BeRobedTimes, Global.Global.serverTimeStampSecond, PlayerDataList.Count, CanRetrieveReward));
            //Debug.LogError(string.Format("Id:{0}  CurrentStationId:{1} EndTime:{2} serverTimeStampSecond:{3} isYestodayData:{4}",
            //    Id, CurrentStationId, EndTime, Global.Global.serverTimeStampSecond, IsYestodayData));
            //Debug.LogError(string.Format("Id:{0}  EndTime:{1}  ", Id, EndTime));
        }

        public int GetCurrentTimesWhenUnfinish()
        {
            int maxTimes = dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(Id);
            return Mathf.Min(maxTimes, CurrentStationId + 1);
        }

        public int GetCurrentTimesWhenFinished()
        {
            int maxTimes = dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(Id);
            return Mathf.Min(maxTimes, FinishStationId + 1);
        }

        public bool CanExplore()
        {
            int maxTimes = dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(Id);
            if((CurrentStationId < maxTimes) 
                && ((GetState() == DreamlandChapterState.unexplore)))
            {
                return true;
            }
            return false;
        }

        public bool HaveReward()
        {
            return GetState() == DreamlandChapterState.haveReward;
        }

        public bool FinishedExploreToday()
        {
            if (CurrentStationId == dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(Id))
            {
                return true;
            }
            return false;
        }
    }
}
