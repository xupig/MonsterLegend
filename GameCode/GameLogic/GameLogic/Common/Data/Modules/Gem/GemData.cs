﻿#region 模块信息
/*==========================================
// 文件名：GemData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Gem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/23 18:34:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using MogoEngine.Events;
namespace Common.Data
{
    public class GemData
    {
        public void UpdateGemData()
        {
            EventDispatcher.TriggerEvent(GemEvents.GemPanelRefreshTabSpotLight);
            EventDispatcher.TriggerEvent(GemEvents.GemPanelRefreshCategoryRedPoint);
        }
    }
}