﻿#region 模块信息
/*==========================================
// 文件名：AlchemyData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Alchemy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/16 10:02:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{

    public enum AlchemyState
    {
        IDLE = 1,
        //ALCHEMY = 2,
        REWARD = 3,
    }
    public class AlchemyData
    {
        public bool hasReceiveAssistList = false;
        public Dictionary<int, PbAlchemyAssist> alchemyAssistDict;

        public Dictionary<UInt64, PbAlchemyAssist> inviteAlchemyAssistDict;

        public Dictionary<UInt64, PbAlchemyAssist> hadInviteAssistDict;

        public int currentSelectPos = 0;


        //public bool isAlchemyGetReward = false;

        public AlchemyData()
        {
            alchemyAssistDict = new Dictionary<int, PbAlchemyAssist>();
            inviteAlchemyAssistDict = new Dictionary<ulong, PbAlchemyAssist>();
            hadInviteAssistDict = new Dictionary<ulong, PbAlchemyAssist>();
            for(int i=0;i<4;i++)
            {
                alchemyAssistDict.Add(i + 1, null);
            }
        }

        public void ClearInvite()
        {
            inviteAlchemyAssistDict.Clear();
            hadInviteAssistDict.Clear();
        }

        public void ClearPlayer()
        {
            for (int i = 0; i < 4; i++)
            {
                alchemyAssistDict[i + 1] = null;
            }
        }

        public void UpdateCurrentSelectAlchemyAssit(PbAlchemyAssist assist)
        {
            if (inviteAlchemyAssistDict.ContainsKey(assist.dbid))
            {
                inviteAlchemyAssistDict[assist.dbid] = assist;
            }
            else
            {
                inviteAlchemyAssistDict.Add(assist.dbid, assist);
            }
        }

        public int GetAssistNum()
        {
            int num = 0;
            foreach(KeyValuePair<int,PbAlchemyAssist> kvp in alchemyAssistDict)
            {
                if(kvp.Value!=null)
                {
                    num++;
                }
            }
            return num;
        }
    }
}
