﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public enum RankType
    {
        // 排行榜系统中使用到的榜
        fightRank = 1,
        levelRank = 2,
        guildRank = 3,

        // 其他系统中使用到的排行榜
        demonGateDayRank = 4,
        demonGateWeekRank = 5,
        findTreasureRank = 6,
        eveningActivity = 7,
        pkRanking = 8,
    }
}
