﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using UnityEngine;
using GameData;
using Common.Utils;

namespace Common.Data
{
    public class MailData
    {
        private uint _curMailNum;
        private uint _newMailNum;
        private uint _attachmentNum;

        private Dictionary<ulong,PbMail> _mailDic;

        public MailData()
        {
            _mailDic = new Dictionary<ulong,PbMail>();
        }

        public void Fill(PbMailList mailList)
        {
            _curMailNum = mailList.mails_count;
            _newMailNum = mailList.new_mails_count;
            _attachmentNum = mailList.unget_attachment_mails_count;
            int count = mailList.mails.Count;
            for (int i = 0; i < count; i++)
            {
                _mailDic[mailList.mails[i].id] = mailList.mails[i];
                if (mailList.mails[i].get_state == public_config.MAIL_NOT_GET && IsWillExpire(mailList.mails[i]) == true)
                {
                    SystemInfoManager.Instance.ShowFastNotice(50102);
                }
            }

            if (_curMailNum >= int.Parse(global_params_helper.GetGlobalParam(130)))
            {
                SystemInfoManager.Instance.ShowFastNotice(50101);
            }
        }

        public void DeleteMail(ulong mailId)
        {

            if (_mailDic.ContainsKey(mailId) == true)
            {
                if (_mailDic[mailId].read_state == public_config.MAIL_NOT_READ)
                {
                    _newMailNum--;
                }
                _mailDic.Remove(mailId);
                _curMailNum--;
            }
        }

        public void DeleteMailList(List<PbMailDbid> mailIdList)
        {
            for (int i = 0; i < mailIdList.Count; i++)
            {
                DeleteMail(mailIdList[i].id);
            }
        }

        public PbMail GetMailById(ulong mailId)
        {
            return _mailDic[mailId];
        }

        public void AddMail(PbMail mail)
        {
            if (mail.get_state == public_config.MAIL_NOT_GET && IsWillExpire(mail) == true)
            {
                SystemInfoManager.Instance.ShowFastNotice(50102);
            }

            if (_mailDic.ContainsKey(mail.id) == true)
            {
                if (_mailDic[mail.id].read_state == public_config.MAIL_NOT_READ
                    && mail.read_state == public_config.MAIL_READ)
                {
                    _newMailNum--;
                }
                if (_mailDic[mail.id].get_state == public_config.MAIL_NOT_GET
                    && mail.get_state == public_config.MAIL_GET)
                {
                    _attachmentNum--;
                }
            }
            else
            {
                if (_curMailNum >= MailMaxCount)
                {
                    List<PbMail> mailList = _mailDic.Values.ToList();
                    mailList.Sort(CompareSendTime);
                    ulong oldestMailId = mailList[mailList.Count - 1].id;
                    DeleteMail(oldestMailId);
                }
                if (mail.read_state == public_config.MAIL_NOT_READ)
                {
                    _newMailNum++;
                }
                if (mail.get_state == public_config.MAIL_NOT_GET)
                {
                    _attachmentNum++;
                }
                _curMailNum++;
            }
            _mailDic[mail.id] = mail;
        }

        public List<PbMail> MailDataList
        {
            get
            {
                List<PbMail> mailList = _mailDic.Values.ToList();
                mailList.Sort(CompareSendTime);

                return mailList.GetRange(0, MailTotalNum);
            }
        }

        public void ReadMail(UInt64 mailId)
        {
            if (_mailDic.ContainsKey(mailId) == false)
            {
                SystemInfoManager.Instance.ShowFastNotice(253);
            }
            else if (_mailDic[mailId].read_state == public_config.MAIL_NOT_READ)
            {
                _newMailNum--;
                _mailDic[mailId].read_state = public_config.MAIL_READ;
            }
        }

        public void DrawMail(UInt64 mailId)
        {
            if (_mailDic.ContainsKey(mailId) == false)
            {
                SystemInfoManager.Instance.ShowFastNotice(253);
            }
            else
            {
                ReadMail(mailId);
                if (_mailDic[mailId].get_state == public_config.MAIL_NOT_GET)
                {
                    _attachmentNum--;
                }
                _mailDic[mailId].get_state = public_config.MAIL_GET;
            }
        }

        public int MailTotalNum
        {
            get
            {
                return Math.Min((int)_curMailNum, MailMaxCount);
            }
        }

        public uint NewMailCount
        {
            get
            {
                return (uint)Math.Min(_newMailNum, MailMaxCount);
            }
        }

        public int MailMaxCount
        {
            get
            {
                return int.Parse(GameData.XMLManager.global_params[58].__value);
            }
        }

        public uint AttachmentNum
        {
            get
            {
                return _attachmentNum;
            }
        }

        private int CompareSendTime(PbMail first, PbMail second)
        {
            if (first.time == second.time)
            {
                return 0;
            }
            else if (first.time < second.time)
            {
                return 1;
            }
            else
            {
                return -1;
            }
            
        }

        private bool IsWillExpire(PbMail mail)
        {
            DateTime _dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(mail.time);

            if (XMLManager.sys_mail.ContainsKey((int)mail.mail_type) == false)
            {
                _dateTime = _dateTime.AddSeconds(int.Parse(XMLManager.global_params[59].__value));
            }
            else
            {
                int effectiveTime = XMLManager.sys_mail[(int)mail.mail_type].__effective_time;                
                if (effectiveTime != 0)
                {
                    _dateTime = _dateTime.AddDays(effectiveTime);
                }
            }
            long expireTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(_dateTime);

            ///相等表示永久有效
            if (expireTimeStamp != mail.time)
            {
                long nowTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(PlayerTimerManager.GetInstance().GetNowServerDateTime());
                long subTimeStamp =   expireTimeStamp - nowTimeStamp;
                return (subTimeStamp / 86400) < 3;
            }
            return false;
        }

    }
}



