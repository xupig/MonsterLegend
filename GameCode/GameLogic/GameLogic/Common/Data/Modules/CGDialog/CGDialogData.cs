﻿

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/11 17:16:36 
 * function: 
 * *******************************************************/

using System;
using UnityEngine;
namespace Common.Data
{
    public class CGDialogShowSubtitles : CGData
    {
        public int content;
        public int contentEng;
        public float duration;
        public CGDialogShowSubtitles(string dramaKey, int content, int contentEng, float duration)
            : base(dramaKey)
        {
            this.content = content;
            this.contentEng = contentEng;
            this.duration = duration;
        }
    }

    public class CGDialogShowSkip : CGData
    {
        public bool isShow;
        public CGDialogShowSkip(string dramaKey, bool isShow)
            : base(dramaKey)
        {
            this.isShow = isShow;
        }
    }

    public class CGDialogShowBlackEdge : CGData
    {
        public bool isShow;
        public CGDialogShowBlackEdge(string dramaKey, bool isShow)
            : base(dramaKey)
        {
            this.isShow = isShow;
        }
    }

    public class CGDialogData : CGData
    {
        public string entityType;
        public string content;
        public float duration;
        public bool isWaitingEvent;
        public CGDialogData(string dramaKey, string entityType, string content, float duration, bool isWaitingEvent)
            : base(dramaKey)
        {
            this.entityType = entityType;
            this.content = content;
            this.duration = duration;
            this.isWaitingEvent = isWaitingEvent;
        }
    }

    public class CGDialogCallBackData : CGDialogData
    {
        public Action callback;

        public CGDialogCallBackData(string dramaKey, string entityType, string content, float duration, bool isWaitingEvent, Action callback)
            : base(dramaKey, entityType, content, duration, isWaitingEvent)
        {
            this.entityType = entityType;
            this.content = content;
            this.duration = duration;
            this.isWaitingEvent = isWaitingEvent;
            this.callback = callback;
        }
    }

    public class CGData
    {
        public string dramaKey;
        public CGData(string dramaKey)
        {
            this.dramaKey = dramaKey;
        }
    }

    public class BossIntroduceData
    {
        public int monsterId;
        public Texture texture;

        public BossIntroduceData(int monsterId, Texture texture)
        {
            this.monsterId = monsterId;
            this.texture = texture;
        }
    }
}
