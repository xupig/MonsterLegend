﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/20 10:43:56
 * 描述说明：
 * *******************************************************/

using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class RoleAttributeData : IAttribute
    {
        public int attriId;
        public string attriName;
        public string strAttriValue;
        private int attriValue;
        public int priority;//优先级

        public RoleAttributeData()
        {

        }

        public RoleAttributeData(int attriId, int attriValue)
        {
            this.attriId = attriId;
            this.attriName = attri_config_helper.GetAttributeName(attriId);
            this.priority = attri_config_helper.GetPriority(attriId);
            this.AttriValue = attriValue;
        }

        public int AttriValue
        {
            get
            {
                return attriValue;
            }
            set
            {
                attriValue = value;
                strAttriValue = value.ToString();
            }
        }

        public string GetAttributeName()
        {
            return attriName;
        }

        public string GetAttributeValue()
        {
            return strAttriValue;
        }

        public int GetPriority()
        {
            return priority;
        }

        public void Print()
        {
            Debug.LogError(string.Format("attriId:{0}  attriName:{1}  attriValue:{2}  priority:{3}", attriId, attriName, strAttriValue, priority));
        }

        public static int SortByPriority(IAttribute x, IAttribute y)
        {
            if (x.GetPriority() < y.GetPriority())
            {
                return -1;
            }
            return 1;
        }
    }
}
