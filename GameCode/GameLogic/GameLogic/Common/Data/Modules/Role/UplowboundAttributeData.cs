﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 15:15:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using UnityEngine;

namespace Common.Data
{
    public class UplowboundAttributeData : IAttribute
    {
        public RoleAttributeData upboundAttributeData;
        public RoleAttributeData lowboundAttributeData;
        public string attributeName = "";

        public UplowboundAttributeData(RoleAttributeData upboundAttributeData, RoleAttributeData lowboundAttributeData)
        {
            this.upboundAttributeData = upboundAttributeData;
            this.lowboundAttributeData = lowboundAttributeData;
            if (upboundAttributeData.attriId == (int) AttriId.damageUpbound)
            {
                attributeName = attri_effect_helper.DAMAGE_LANG_ID.ToLanguage();
            }
        }

        public string GetAttributeName()
        {
            return attributeName;
        }

        public string GetAttributeValue()
        {
            return string.Format("{0}-{1}", lowboundAttributeData.strAttriValue, upboundAttributeData.strAttriValue);
        }

        public int GetPriority()
        {
            return upboundAttributeData.priority < lowboundAttributeData.priority ? upboundAttributeData.priority : lowboundAttributeData.priority;
        }
    }
}
