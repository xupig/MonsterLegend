﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 15:25:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public interface IAttribute
    {
        string GetAttributeName();
        string GetAttributeValue();
        int GetPriority();
    }
}
