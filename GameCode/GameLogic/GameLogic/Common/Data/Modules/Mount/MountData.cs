﻿#region 模块信息
/*==========================================
// 文件名：MountData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Mount
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/3 20:03:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
namespace Common.Data
{
    public class MountData
    {
        private Dictionary<int, MountItemData> mountDict;

        private int _currentMountId;
        public int CurrentMountId
        {
            get { return _currentMountId; }
        }

        public MountData()
        {
            mountDict = new Dictionary<int, MountItemData>();
            foreach (KeyValuePair<int, mount> pair in XMLManager.mount)
            {
                if (pair.Key != 0)
                {
                    mount cfg = pair.Value;
                    mountDict.Add(cfg.__id, new MountItemData(cfg));
                }
            }
        }

        // 根据服务器的数据对坐骑状态刷新
        public void Fill(PbRideList rideList)
        {
            for (int i = 0; i < rideList.ride_info.Count; i++)
            {
                PbRideInfo rideInfo = rideList.ride_info[i];
                MountItemRenewal((uint)rideInfo.ride_id, rideInfo.valid_time);
            }
            // 设置当前的Mount ID，根据PlayerAvatar的属性确定
            if (PlayerAvatar.Player.ride_id != 0)
            {
                _currentMountId = PlayerAvatar.Player.ride_id;
                mountDict[_currentMountId].state = MountState.Riding;
            }
        }

        // 坐骑续费或者购买处理
        public void MountItemRenewal(uint mountId, uint validTime)
        {
            int id = (int)mountId;
            if (mountDict.ContainsKey(id))
            {
                mountDict[id].ValidTime = validTime;
                if (mountDict[id].state == MountState.NotHave)
                {
                    mountDict[id].state = MountState.NotRide;
                }
            }
        }

        // 坐骑失效处理
        public void MountItemExpire(uint mountId)
        {
            int id = (int)mountId;
            if (mountDict.ContainsKey(id))
            {
                mountDict[id].state = MountState.NotHave;
                mountDict[id].ValidTime = 0;
            }
        }

        // 更换坐骑处理
        public void SetCurrentMountId(uint mountId)
        {
            RideOnMount(_currentMountId, false);
            _currentMountId = (int)mountId;
            RideOnMount(_currentMountId, true);
        }

        private void RideOnMount(int mountId, bool isRide)
        {
            if (mountId != 0 && mountDict.ContainsKey(mountId))
            {
                if (isRide == true)
                {
                    mountDict[mountId].state = MountState.Riding;
                }
                else
                {
                    mountDict[mountId].state = MountState.NotRide;
                }
            }
        }

        public MountItemData GetMountItemData(int mountId)
        {
            if (mountDict.ContainsKey(mountId))
            {
                return mountDict[mountId];
            }
            return null;
        }

        public bool HasMount(int mountId)
        {
            MountItemData mountData = GetMountItemData(mountId);
            if (mountData == null)
            {
                return false;
            }
            return mountData.state != MountState.NotHave;
        }

        public List<MountItemData> GetMountInfoList()
        {
            List<MountItemData> result = new List<MountItemData>(mountDict.Values);
            result.Sort(CompareFunction);
            result.RemoveAll(MatchFunction);
            return result;
        }

        private bool MatchFunction(MountItemData mountData)
        {
            return !(mountData.vocationSet.Contains((int)PlayerAvatar.Player.vocation) ||
                mountData.vocationSet.Contains(0));
        }

        private int CompareFunction(MountItemData x, MountItemData y)
        {
            if (x.groupId != y.groupId)
            {
                return x.groupId - y.groupId;
            }
            return x.id - y.id;
        }

        private int totalFightForce;
        public Dictionary<int, int> GetTotalMountAttributes()
        {
            totalFightForce = 0;
            Dictionary<int, int> result = new Dictionary<int, int>();
            foreach (var pair in mountDict)
            {
                MountItemData data = pair.Value;
                if (data.state != MountState.NotHave)
                {
                    totalFightForce += pair.Value.FightForce;
                    int effectId = data.EffectId;
                    List<int> attriIdList = attri_effect_helper.GetAttributeIdList(effectId);
                    List<int> attriValueList = attri_effect_helper.GetAttributeValueList(effectId);
                    for (int i = 0; i < attriIdList.Count; i++)
                    {
                        if (result.ContainsKey(attriIdList[i]) == false)
                        {
                            result.Add(attriIdList[i], 0);
                        }
                        result[attriIdList[i]] += attriValueList[i];
                    }
                }
            }
            return result;
        }

        public int TotalFightForce
        {
            get
            { return totalFightForce; }
        }

        public bool IsMountPermanent(int mountId)
        {
            MountItemData itemData = GetMountItemData(mountId);
            return HasMount(mountId) && itemData.ValidTime == 0;
        }
    }
}