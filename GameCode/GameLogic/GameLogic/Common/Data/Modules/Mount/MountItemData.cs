﻿#region 模块信息
/*==========================================
// 文件名：MountItemData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Mount
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/3 20:05:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Utils;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
namespace Common.Data
{
    public enum MountState
    {
        NotHave = 0,
        NotRide = 1,
        Riding = 2,
    }

    public class MountItemData
    {
        public int id;
        public int nameId;
        public int descId;
        public int iconId;
        public HashSet<int> vocationSet;
        public int groupId;
        private LimitType type;
        public MountState state;
        private mount mountCfg;
        public int source;

        public string Name
        {
            get { return MogoLanguageUtil.GetContent(nameId); }
        }

        public bool IsNew
        {
            get;
            set;
        }

        public MountItemData(mount cfg)
        {
            mountCfg = cfg;
            id = cfg.__id;
            nameId = cfg.__name;
            descId = cfg.__desc;
            iconId = cfg.__icon;
            source = cfg.__source;
            vocationSet = new HashSet<int>();
            List<string> dataList = data_parse_helper.ParseListString(cfg.__vocation);
            if (dataList != null && dataList.Count > 0)
            {
                for (int i = 0; i < dataList.Count; i++)
                {
                    vocationSet.Add(int.Parse(dataList[i]));
                }
            }
            else
            {
                // 0 表示所有职业都适合
                vocationSet.Add(0);
            }
            groupId = cfg.__group_id;
            state = MountState.NotHave;
            validTime = 0;
        }


        public int EffectId
        {
            get
            {
                string vocationString = ((int)PlayerAvatar.Player.vocation).ToString();
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(mountCfg.__effect_id);
                if (dataDict.ContainsKey(vocationString))
                {
                    return int.Parse(dataDict[vocationString]);
                }
                return 0;
            }
        }

        public int RideEffect
        {
            get
            {
                return mountCfg.__ride_buff_id;
            }
        }

        public string Description
        {
            get { return MogoLanguageUtil.GetContent(descId); }
        }

        private uint validTime;

        public int FightForce
        {
            get
            {
                int vocationIndex = (int)PlayerAvatar.Player.vocation;
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(mountCfg.__effect_id);
                int effectId = int.Parse(dataDict[vocationIndex.ToString()]);
                return fight_force_helper.GetFightForce(effectId, PlayerAvatar.Player.vocation);
            }
        }

        public uint ValidTime
        {
            get
            {
                return validTime;
            }
            set
            {
                if (value == 0)
                {
                    type = LimitType.NoTimeLimited;
                    validTime = value;
                }
                else
                {
                    type = LimitType.TimeLimited;
                    validTime = value;
                }
            }
        }

        public string ValidDescString
        {
            get
            {
                if (state == MountState.NotHave)
                {
                    return Description;
                }
                if (type == LimitType.NoTimeLimited)
                {
                    return MogoLanguageUtil.GetContent(36508);
                }
                long ValidSeconds = (long)ValidTime;
                int currentTimeStamp = (int)Global.Global.serverTimeStampSecond;
                long leftSeconds = ValidSeconds - (long)currentTimeStamp;
                if (leftSeconds > 3600 * 24)
                {
                    string descString = MogoLanguageUtil.GetContent(36506);
                    int leftDays = (int)(leftSeconds / (3600 * 24));
                    leftDays += leftSeconds % (3600 * 24) > 0 ? 1 : 0;
                    return string.Format(descString, leftDays);
                }
                else if (leftSeconds > 3600)
                {
                    string descString2 = MogoLanguageUtil.GetContent(36507);
                    int leftHours = (int)(leftSeconds / 3600);
                    leftHours += leftSeconds % 3600 > 0 ? 1 : 0;
                    return string.Format(descString2, leftHours);
                }
                else
                {
                    string descString3 = MogoLanguageUtil.GetContent(36517);
                    int leftMinutes = (int)(leftSeconds / 60);
                    leftMinutes += (leftSeconds % 60) > 0 ? 1 : 0;
                    return string.Format(descString3, leftMinutes);
                }
            }
            set
            {

            }
        }

        public string ValidDescStringInTips
        {
            get
            {
                if (state == MountState.NotHave)
                {
                    return MogoLanguageUtil.GetContent(36512);
                }
                if (type == LimitType.NoTimeLimited)
                {
                    return MogoLanguageUtil.GetContent(36508);
                }
                int ValidSeconds = (int)ValidTime;
                int currentTimeStamp = (int)Global.Global.serverTimeStampSecond;
                int leftSeconds = ValidSeconds - currentTimeStamp;
                if (leftSeconds > 3600 * 24)
                {
                    string descString = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36509));
                    int leftDays = leftSeconds / (3600 * 24);
                    leftDays += leftSeconds % (3600 * 24) > 0 ? 1 : 0;
                    return string.Format(descString, leftDays);
                }
                else if (leftSeconds > 3600)
                {
                    string descString2 = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36510));
                    int leftHours = leftSeconds / 3600;
                    leftHours += (leftSeconds % 3600) > 0 ? 1 : 0;
                    return string.Format(descString2, leftHours);
                }
                else
                {
                    string descString3 = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36516));
                    int leftMinutes = leftSeconds / 60;
                    leftMinutes += (leftSeconds % 60) > 0 ? 1 : 0;
                    return string.Format(descString3, leftMinutes);
                }
            }
        }


        
    }
}