﻿using Common.ClientConfig;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TimeAltarData
    {
        public bool IsInitTimAltarData = false;
        public bool IsHaveNotice = false;
        private Dictionary<int, TimeAltarFunctionData> _timeAltarDataDic = new Dictionary<int,TimeAltarFunctionData>();
        public TimeAltarData()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.active_point, CheckIsHavaNotice);
        }

        public void Fill(PbTimeAltarInfo timeAltarInfo)
        {
            IsInitTimAltarData = true;
            if (_timeAltarDataDic == null)
            {
                _timeAltarDataDic = new Dictionary<int, TimeAltarFunctionData>();
            }
            for (int i = 0; i < timeAltarInfo.function_list.Count; i++)
            {
                if (_timeAltarDataDic.ContainsKey(timeAltarInfo.function_list[i].function_id) == true)
                {
                    _timeAltarDataDic[timeAltarInfo.function_list[i].function_id] = new TimeAltarFunctionData(timeAltarInfo.function_list[i]);
                }
                else
                {
                    _timeAltarDataDic.Add(timeAltarInfo.function_list[i].function_id, new TimeAltarFunctionData(timeAltarInfo.function_list[i]));
                }

                if (_timeAltarDataDic[timeAltarInfo.function_list[i].function_id].TimeAltarItemDataList.Count > 0)
                {
                    SystemInfoManager.Instance.ShowFastNotice(50301);
                }
            }
            CheckIsHavaNotice();
        }

        public void UpdateRedemptionItem(int functionId, int date, int count)
        {
            if (functionId != -1 && _timeAltarDataDic.ContainsKey(functionId) == true)
            {
                _timeAltarDataDic[functionId].RefreshCount(date, count);
                if (_timeAltarDataDic[functionId].TimeAltarItemDataList.Count == 0)
                {
                    _timeAltarDataDic.Remove(functionId);
                }
            }
            CheckIsHavaNotice();    
        }

        public Dictionary<int, TimeAltarFunctionData> TimeAltarDataDic
        {
            get
            {
                return _timeAltarDataDic;
            }
        }

        private void CheckIsHavaNotice()
        {
            bool isHaveNotice = false;
            foreach (KeyValuePair<int, TimeAltarFunctionData> kvp in _timeAltarDataDic)
            {
                if (kvp.Value.TimeAltarItemDataList.Count > 0 && PlayerAvatar.Player.active_point > kvp.Value.TimeAltarItemDataList[0].ActivePoint)
                {
                    isHaveNotice = true;
                    break;
                }
            }
            if (IsHaveNotice != isHaveNotice)
            {
                IsHaveNotice = isHaveNotice;
                EventDispatcher.TriggerEvent(TimeAltarEvents.NoticeChanged);
            }
        }
    }
}
