﻿
using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TimeAltarFunctionData
    {
        private List<TimeAltarItemData> _timeAltarItemDataList = new List<TimeAltarItemData>();

        public TimeAltarFunctionData(PbTimeAltarFunctionStruct timeAltarFunctionStruct)
        {
            for (int i = 0; i < timeAltarFunctionStruct.level_list.Count; i++)
            {
                if (time_alter_helper.GetConfig(timeAltarFunctionStruct.function_id, timeAltarFunctionStruct.level_list[i].level) != null)
                {
                    _timeAltarItemDataList.Add(new TimeAltarItemData(timeAltarFunctionStruct.level_list[i], timeAltarFunctionStruct.function_id));
                }
            }
        }

        public void RefreshCount(int date, int count)
        {
            for (int i = 0; i < _timeAltarItemDataList.Count; i++)
            {
                if (_timeAltarItemDataList[i].TimeAltarLevelStruct.date == date)
                {
                    if (count == 0)
                    {
                        _timeAltarItemDataList.RemoveAt(i);
                        break;
                    }
                    _timeAltarItemDataList[i].TimeAltarLevelStruct.count = count;
                }
            }
        }

        public List<TimeAltarItemData> TimeAltarItemDataList
        {
            get
            {
                return _timeAltarItemDataList;
            }
        }
    }
}
