﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TimeAltarItemData
    {
        private PbTimeAltarLevelStruct _timeAltarLevelStruct;
        private time_altar _config;
        private int _functionId;

        public TimeAltarItemData(PbTimeAltarLevelStruct timeAltarLevelStruct, int functionId)
        {
            _timeAltarLevelStruct = timeAltarLevelStruct;
            _config = time_alter_helper.GetConfig(functionId, _timeAltarLevelStruct.level);
            _functionId = functionId;
        }

        public int RedemptionNumUplimit
        {
            get
            {
                return _config.__num_limit > _timeAltarLevelStruct.count ? _timeAltarLevelStruct.count : _config.__num_limit;
            }
        }

        public int FunctionId
        {
            get
            {
                return _functionId;
            }
        }

        public PbTimeAltarLevelStruct TimeAltarLevelStruct
        {
            get
            {
                return _timeAltarLevelStruct;
            }
        }

        public List<RewardData> RewardList
        {
            get
            {
                return item_reward_helper.GetRewardDataList(_config.__reward, PlayerAvatar.Player.vocation);
            }
        }

        /// <summary>
        /// 赎回需要消耗的材料
        /// </summary>
        public List<BaseItemData> CostList
        {
            ///TODO
            get
            {
                List<BaseItemData> result = new List<BaseItemData>();
                Dictionary<string, string> dict = data_parse_helper.ParseMap(_config.__cost);
                List<string> keyList = dict.Keys.ToList();
                foreach (string str in keyList)
                {
                    BaseItemData item = ItemDataCreator.Create(int.Parse(str));
                    item.StackCount = int.Parse(dict[str]);
                    result.Add(item);
                }

                return result;
            }
        }

        /// <summary>
        /// 需要消耗的活跃度，0表示不消耗
        /// </summary>
        public int ActivePoint
        {
            get
            {
                return Convert.ToInt32(_config.__active_point);
            }
        }

        public int Icon
        {
            get
            {
                return _config.__icon;
            }
        }

        public int Level
        {
            get
            {
                return _timeAltarLevelStruct.level;
            }
        }

        public int Count
        {
            get
            {
                return _timeAltarLevelStruct.count;
            }
        }

        public string Name
        {
            get
            {
                return MogoLanguageUtil.GetContent(_config.__redemption_name, Level, Count);
            }
        }
    }
}
