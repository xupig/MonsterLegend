﻿using System;
using System.Collections.Generic;
using Common.Events;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using GameMain.GlobalManager;
using GameLoader.PlatformSdk;

namespace Common.Data
{
    //如果第一天登陆，在第二天的某个时间推送，但由于在第一天换了手机后，再登陆，还是第二天的时间提醒，只是时间推后
    //如果在第二天推送之前，换了手机，再次登陆，如果没有领奖，就没有第二天的提醒，变为等到第三天才会提醒
    public class LoginPushData
    {
        private const string LOGIN_TIME_FORMAT = "yyyy/MM/dd/hh:mm:ss";
        private LoginTimeInfo _loginTimeInfo;
        private Dictionary<int, bool> _loginPushHasCancelDict = new Dictionary<int, bool>();
        private List<int> _addToNotificationIdList = new List<int>();
        private List<int> _completeTaskIdList = new List<int>();

        public LoginPushData()
        {
            InitLoginTimeInfo();
        }

        private void InitLoginTimeInfo()
        {
            _loginTimeInfo = new LoginTimeInfo();
            _loginTimeInfo.loginDay = 0;
            _loginTimeInfo.lastNoticeDay = 0;
            _loginTimeInfo.noticeDay = 0;
            _loginTimeInfo.lastNotifyTimeStamp = 0;
            _loginTimeInfo.notifyTimeStamp = 0;
        }

        //需在同步了服务器的时间戳serverTimeStamp后再添加到sdk层的配置中
        public string AddLoginPushNotification()
        {
            LoadLoginTimeInfo();
            string strInfo = "";
            //login_push里的id不能与notification配置中的id重复
            int loginDay = (int)PlayerAvatar.Player.login_day_count;
            if (_loginTimeInfo.loginDay != loginDay)
            {
                _loginTimeInfo.loginDay = loginDay;
                if (_loginTimeInfo.noticeDay != loginDay)
                {
                    long loginPushTimeStamp = GetLoginPushTimeStamp();
                    _loginTimeInfo.notifyTimeStamp = loginPushTimeStamp * 1000;
                    _loginTimeInfo.noticeDay = loginDay + 1;
                }
                else
                {
                    _loginTimeInfo.lastNoticeDay = _loginTimeInfo.noticeDay;
                    _loginTimeInfo.lastNotifyTimeStamp = _loginTimeInfo.notifyTimeStamp;
                    long loginPushTimeStamp = GetLoginPushTimeStamp();
                    _loginTimeInfo.notifyTimeStamp = loginPushTimeStamp * 1000;
                    _loginTimeInfo.noticeDay += 1;
                }
                SaveLoginTimeInfo();
            }
            else
            {
                if (_loginTimeInfo.lastNoticeDay != 0 && _loginTimeInfo.lastNoticeDay == loginDay)
                {
                    DateTime curUtcTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.Global.serverTimeStamp / 1000));
                    DateTime lastUtcTime = MogoTimeUtil.ServerTimeStamp2UtcTime(_loginTimeInfo.lastNotifyTimeStamp / 1000);
                    if (lastUtcTime.Year != curUtcTime.Year || lastUtcTime.Month != curUtcTime.Month || lastUtcTime.Day != curUtcTime.Day)
                    {
                        DateTime notifyUtcTime = MogoTimeUtil.GetUtcTime(curUtcTime.Year, curUtcTime.Month, curUtcTime.Day, lastUtcTime.Hour, lastUtcTime.Minute, lastUtcTime.Second);
                        _loginTimeInfo.lastNotifyTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(notifyUtcTime) * 1000;
                        SaveLoginTimeInfo();
                    }
                }
            }

            //UnityEngine.Debug.LogError("login day = " + loginDay);
            foreach (login_push item in XMLManager.login_push.Values)
            {
                if (item.__notice_day == _loginTimeInfo.noticeDay || item.__notice_day == _loginTimeInfo.lastNoticeDay)
                {
                    long notifyTimeStamp = 0;
                    if (item.__notice_day == _loginTimeInfo.noticeDay)
                    {
                        notifyTimeStamp = _loginTimeInfo.notifyTimeStamp;
                    }
                    else if (item.__notice_day == _loginTimeInfo.lastNoticeDay)
                    {
                        notifyTimeStamp = _loginTimeInfo.lastNotifyTimeStamp;
                    }
                    if (notifyTimeStamp > 0)
                    {
                        _loginPushHasCancelDict.Add(item.__id, false);
                        //同步了服务器时间后，由于任务数据已经获取，就可以提前判断是否可以取消推送
                        //不能在上面处理是由于需要先处理每日首次登陆时间，并要保存到本地缓存中
                        ProcessCancelPushByTask(item.__id);
                        if (_loginPushHasCancelDict[item.__id] == true)
                            continue;
                        List<string> dataList = data_parse_helper.ParseListString(item.__desc);
                        string title1 = dataList.Count >= 2 ? MogoLanguageUtil.GetContent(item.__desc[0]) : "You can get reward if you login !";
                        string title2 = dataList.Count >= 2 ? MogoLanguageUtil.GetContent(item.__desc[0]) : "You can get reward if you login !";
                        string content = dataList.Count >= 2 ? string.Format(MogoLanguageUtil.GetContent(item.__desc[1]), item.__notice_day) :
                            string.Format(MogoLanguageUtil.GetContent(item.__desc[0]), item.__notice_day);
                        long notifyUtcTimeStamp = notifyTimeStamp + (Global.Global.serverTimeZone * 3600 * 1000);
                        strInfo += string.Format("{0},{1},{2},{3},{4},{5},{6},{7}_", item.__id, notifyUtcTimeStamp, "0", item.__id, item.__id, title1, title2, content);
                        GameLoader.Utils.LoggerHelper.Info("Login Push add to notification, id : " + item.__id + " ,  notify utc time stamp : " +notifyUtcTimeStamp + "  , notify date time : " + reward_system_helper.PrintDateTime(MogoTimeUtil.ServerTimeStamp2UtcTime(notifyTimeStamp / 1000)));
                        //UnityEngine.Debug.LogError("Login Push add to notification, id : " + item.__id + " ,  notify time stamp : " + notifyUtcTimeStamp + "  , notify date time : " + reward_system_helper.PrintDateTime(MogoTimeUtil.ServerTimeStamp2UtcTime(notifyTimeStamp / 1000)));
                        //UnityEngine.Debug.LogError("title 1 = " + title1 + " , title2 = " + title2 + " , content = " + content);
                        _addToNotificationIdList.Add(item.__id);
                    }
                }
            }
            AddEventListener();
            if (!string.IsNullOrEmpty(strInfo))
            {
                strInfo = strInfo.Remove(strInfo.Length - 1, 1);
            }
            _completeTaskIdList.Clear();
            return strInfo;
        }

        public void InitCompleteTaskIds(List<int> completeTaskIdList)
        {
            for (int i = 0; i < completeTaskIdList.Count; i++)
            {
                _completeTaskIdList.Add(completeTaskIdList[i]);
            }
        }

        public void ProcessCancelPushByTask(int id)
        {
            if (!_loginPushHasCancelDict.ContainsKey(id) || (_loginPushHasCancelDict[id] == true))
                return;

            int cancelCondType = login_push_helper.GetCancelPushCondType(id);
            if (cancelCondType == (int)CancelPushCondType.Task)
            {
                int taskId = login_push_helper.GetCancelPushCondValue(id);
                if (_completeTaskIdList.Contains(taskId))
                {
                    _loginPushHasCancelDict[id] = true;
                }
            }
        }

        private void PrintTest()
        {
            DateTime notifyDateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(_loginTimeInfo.notifyTimeStamp / 1000);
            string strDateTime = reward_system_helper.PrintDateTime(notifyDateTime);
            UnityEngine.Debug.LogError("notify date time info = " + strDateTime);
        }

        private void CancelPush(int id)
        {
            if (!_loginPushHasCancelDict.ContainsKey(id) || (_loginPushHasCancelDict[id] == true))
                return;
            int cancelCondType = login_push_helper.GetCancelPushCondType(id);
            if (cancelCondType == (int)CancelPushCondType.Task)
            {
                int condValue = login_push_helper.GetCancelPushCondValue(id);
                TaskData taskData = PlayerDataManager.Instance.TaskData;
                if (taskData.IsCompleted(condValue))
                {
                    _loginPushHasCancelDict[id] = true;
                    if (_addToNotificationIdList.Contains(id))
                    {
                        PlatformSdkMgr.Instance.CancelNotificationRecord(id);
                    }
                }
            }
        }

        private void AddEventListener()
        {
            List<int> condList = new List<int>();
            foreach (var item in _loginPushHasCancelDict)
            {
                int id = item.Key;
                bool hasCancel = item.Value;
                //加到sdk配置中的通知消息才会添加事件监听
                if (_addToNotificationIdList.Contains(id) && hasCancel == false)
                {
                    int cancelCondType = login_push_helper.GetCancelPushCondType(id);
                    if (cancelCondType == (int)CancelPushCondType.Task)
                    {
                        if (!condList.Contains(cancelCondType))
                        {
                            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
                            condList.Add(cancelCondType);
                        }
                    }
                }
            }
        }

        private void RemoveTaskEventListener()
        {
            EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
        }

        private void OnTaskComplete(int taskId)
        {
            int cancelNotificationId = 0;
            foreach (var item in _loginPushHasCancelDict)
            {
                if (_addToNotificationIdList.Contains(item.Key) && item.Value == false)
                {
                    int condValue = login_push_helper.GetCancelPushCondValue(item.Key);
                    if (condValue == taskId)
                    {
                        cancelNotificationId = item.Key;
                        break;
                    }
                }
            }
            if (cancelNotificationId > 0)
            {
                CancelPush(cancelNotificationId);
            }
            ProcessRemoveTaskListener();
        }

        private void ProcessRemoveTaskListener()
        {
            //当有关任务的推送条件都已达成时，就取消监听
            bool canRemoveTaskListener = true;
            foreach (var item in _loginPushHasCancelDict)
            {
                int cancelCondType = login_push_helper.GetCancelPushCondType(item.Key);
                if (cancelCondType == (int)CancelPushCondType.Task && item.Value == false)
                {
                    canRemoveTaskListener = false;
                    break;
                }
            }
            if (canRemoveTaskListener == true)
            {
                RemoveTaskEventListener();
            }
        }

        private long GetLoginPushTimeStamp()
        {
            //统一使用utc时间
            //当前的utc时间，day重置为0，再加上24小时候，就是下一天的时间，再加上配置的时间段，就是下一天的总时间
            //与登陆的时间+24小时做比较，就计算出推送的时间，如果在10s之内，就需要做一下提前或延后处理
            DateTime curUtcTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.Global.serverTimeStamp / 1000));
            DateTime curDayUtcTime = MogoTimeUtil.GetUtcTime(curUtcTime.Year, curUtcTime.Month, curUtcTime.Day, 0, 0, 0);
            long nextDayTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(curDayUtcTime);
            nextDayTimeStamp += 24 * 3600;  //下一天0点时的timeStamp
            long loginPushTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(curUtcTime);
            loginPushTimeStamp += 24 * 3600;  //下一天需要弹登陆推送消息的时间点

            int deltaSecond = 10;
            DateTime nextDayUtcTime = MogoTimeUtil.ServerTimeStamp2UtcTime(nextDayTimeStamp + 1);
            foreach (notification notificationItem in XMLManager.notification.Values)
            {
                //要判断下一天的每日推送(notification表)是否与登陆推送的时间有冲突
                //目前sdk中推送的实现是在相同时间内只能推送一条消息，如果改为能同时推送几条，就不需要处理时间相同的情况了
                if (IsNotificationPushNextDay(data_parse_helper.ParseListString(notificationItem.__date), (int)nextDayUtcTime.DayOfWeek))
                {
                    string[] dailyPushTimeSplits = notificationItem.__time.Split(new char[] { ':' });
                    if (dailyPushTimeSplits.Length < 3)
                    {
                        continue;
                    }
                    int dailyHour = 0;
                    int dailyMinute = 0;
                    int dailySecond = 0;
                    int.TryParse(dailyPushTimeSplits[0], out dailyHour);
                    int.TryParse(dailyPushTimeSplits[1], out dailyMinute);
                    int.TryParse(dailyPushTimeSplits[2], out dailySecond);
                    long dailyPushTimeStamp = nextDayTimeStamp + dailyHour * 3600 + dailyMinute * 60 + dailySecond;
                    //根据当前的每日推送配置没有配置到接近0点的，因此+/-10s后会超过到下一天或提前到前一天的情况就不存在
                    if (loginPushTimeStamp > dailyPushTimeStamp && loginPushTimeStamp - dailyPushTimeStamp < deltaSecond)
                    {
                        loginPushTimeStamp += deltaSecond - (loginPushTimeStamp - dailyPushTimeStamp);
                    }
                    else if (loginPushTimeStamp < dailyPushTimeStamp && dailyPushTimeStamp - loginPushTimeStamp < deltaSecond)
                    {
                        loginPushTimeStamp -= deltaSecond - (dailyPushTimeStamp - loginPushTimeStamp);
                    }
                    break;
                }
            }
            return loginPushTimeStamp;
        }

        private void SaveLoginTimeInfo()
        {
            LoginTimeInfoLocal info = new LoginTimeInfoLocal();
            info.loginDay = _loginTimeInfo.loginDay;
            info.lastNoticeDay = _loginTimeInfo.lastNoticeDay;
            info.noticeDay = _loginTimeInfo.noticeDay;
            info.lastNotifyTimeStamp = _loginTimeInfo.lastNotifyTimeStamp.ToString();
            info.notifyTimeStamp = _loginTimeInfo.notifyTimeStamp.ToString();
            LocalCache.Write(LocalName.LoginPush, info, CacheLevel.Permanent);
        }

        public void LoadLoginTimeInfo()
        {
            LoginTimeInfoLocal info = LocalCache.Read<LoginTimeInfoLocal>(LocalName.LoginPush);
            if (info == null)
            {
                return;
            }
            _loginTimeInfo.loginDay = info.loginDay;
            _loginTimeInfo.lastNoticeDay = info.lastNoticeDay;
            _loginTimeInfo.noticeDay = info.noticeDay;
            _loginTimeInfo.lastNotifyTimeStamp = long.Parse(info.lastNotifyTimeStamp);
            _loginTimeInfo.notifyTimeStamp = long.Parse(info.notifyTimeStamp);
            //UnityEngine.Debug.LogError("login day = " + info.loginDay + " last notice day = " + info.lastNoticeDay + "  notice day = " + info.noticeDay + "  last time stamp = " + info.lastNotifyTimeStamp + "  time stamp = " + info.notifyTimeStamp);
        }

        private bool IsNotificationPushNextDay(List<string> weekList, int nextDayOfWeek)
        {
            if (weekList.Count == 0)
                return true;
            bool result = false;
            for (int i = 0; i < weekList.Count; i++)
            {
                int weekday = int.Parse(weekList[i]);
                weekday = weekday <= 6 ? weekday : 0;
                if (weekday == nextDayOfWeek)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        //暂时没用
        private string GetStrLoginTime(DateTime dateTime)
        {
            string hour = dateTime.Hour > 9 ? dateTime.Hour.ToString() : "0" + dateTime.Hour;
            string minute = dateTime.Minute > 9 ? dateTime.Minute.ToString() : "0" + dateTime.Minute;
            string second = dateTime.Second > 9 ? dateTime.Second.ToString() : "0" + dateTime.Second;
            string time = string.Format(LOGIN_TIME_FORMAT, dateTime.Year, dateTime.Month, dateTime.Day, hour, minute, second);
            return time;
        }

        private string GetStrUtcTime()
        {
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.Global.serverTimeStamp / 1000));
            string strTime = LOGIN_TIME_FORMAT;
            strTime = strTime.Replace("yyyy", dateTime.Year.ToString());
            strTime = strTime.Replace("MM", dateTime.Month.ToString());
            strTime = strTime.Replace("dd", dateTime.Day.ToString());
            strTime = strTime.Replace("hh", dateTime.Hour.ToString());
            strTime = strTime.Replace("mm", dateTime.Minute.ToString());
            strTime = strTime.Replace("ss", dateTime.Second.ToString());
            return strTime;
        }

        public class LoginTimeInfo
        {
            public int loginDay;
            public int lastNoticeDay;
            public int noticeDay;
            public long lastNotifyTimeStamp;
            public long notifyTimeStamp;  //单位ms
        }

        public class LoginTimeInfoLocal
        {
            public int loginDay;
            public int lastNoticeDay;
            public int noticeDay;
            public string lastNotifyTimeStamp;
            public string notifyTimeStamp;  //单位ms
        }

    }
}
