﻿using System;
using System.Collections.Generic;
using Common.Utils;
using UnityEngine;
using GameData;
using MogoEngine.Events;
using Common.Events;
using System.Text;
using GameLoader.PlatformSdk;
using GameLoader.Utils;


namespace Common.Data
{
    public class SettingData
    {
        //默认设置常量
        public static readonly int DEFAULT_SOUND_VOLUME = 100;
        public static readonly int MAX_SHOW_PLAYER_COUNT = 50;
        public static readonly int DEFAULT_SHOW_PLAYER_COUNT = 10;

        //画质常量
        public static readonly int RENDER_QUALITY_LOW = 1;
        public static readonly int RENDER_QUALITY_MIDDLE = 2;
        public static readonly int RENDER_QUALITY_HIGH = 3;

        private Dictionary<SettingType, int> _settingDict;
        private Dictionary<int, bool> _pushSettingDict;  //key为id，value为是否开启

        private LoginPushData _loginPushData;

        private const string PREFEX = "_";
        private bool _isPushSettingChange = false;
        public bool IsPushSettingChange
        {
            get { return _isPushSettingChange; }
            set { _isPushSettingChange = value; }
        }

        public SettingData()
        {
            InitSettings();
            SaveSettingData();
            _loginPushData = new LoginPushData();
            EventDispatcher.AddEventListener<List<int>>(SettingEvents.INIT_TASK_COMPLETED, OnInitTaskCompleted);
        }

        private void InitSettings()
        {
            //初始化为默认设置
            SetDefaultSettings();
            //读取本地的数据
            LoadSettingData();
            EventDispatcher.TriggerEvent(SettingEvents.INIT_SETTINGS);
        }

        private void SetDefaultSettings()
        {
            if (_settingDict == null)
            {
                _settingDict = new Dictionary<SettingType, int>();
            }
            _settingDict.Clear();
            _settingDict.Add(SettingType.Music, DEFAULT_SOUND_VOLUME);
            _settingDict.Add(SettingType.Sound, DEFAULT_SOUND_VOLUME);
            _settingDict.Add(SettingType.Speech, DEFAULT_SOUND_VOLUME);
            _settingDict.Add(SettingType.PlayerCount, DEFAULT_SHOW_PLAYER_COUNT);
            _settingDict.Add(SettingType.RenderQuality, RENDER_QUALITY_MIDDLE);
            _settingDict.Add(SettingType.ChatSpeech, 0);

            if (_pushSettingDict == null)
            {
                _pushSettingDict = new Dictionary<int, bool>();
            }
            _pushSettingDict.Clear();
            GameDataTable<int, push> pushDict = XMLManager.push;
            foreach (KeyValuePair<int, push> item in pushDict)
            {
                if (!push_helper.IsAddPushSettingList(item.Key))
                    continue;
                _pushSettingDict.Add(item.Key, push_helper.IsPushDefaultOpen(item.Key));
            }
        }

        public void SaveSettingData()
        {
            LocalSettingCache settingCache = new LocalSettingCache();
            List<SystemSettingInfo> settingInfos = new List<SystemSettingInfo>();
            foreach (var item in _settingDict)
            {
                SystemSettingInfo info = new SystemSettingInfo();
                info.settingType = (int)item.Key;
                info.value = item.Value;
                settingInfos.Add(info);
            }
            List<PushSettingInfo> pushSettingInfos = new List<PushSettingInfo>();
            foreach (var item in _pushSettingDict)
            {
                PushSettingInfo info = new PushSettingInfo();
                info.pushId = item.Key;
                info.value = item.Value;
                pushSettingInfos.Add(info);
            }
            settingCache.sysSettingInfos = settingInfos;
            settingCache.pushSettingInfos = pushSettingInfos;
            LocalCacheHelper.WriteCommon(LocalName.Setting, settingCache);
        }

        private void LoadSettingData()
        {
            LocalSettingCache settingCache = LocalCacheHelper.ReadCommon<LocalSettingCache>(LocalName.Setting);
            if (settingCache == null)
            {
                return;
            }
            List<SystemSettingInfo> settingInfos = settingCache.sysSettingInfos;
            List<PushSettingInfo> pushSettingInfos = settingCache.pushSettingInfos;
            if (settingInfos == null || pushSettingInfos == null)
            {
                return;
            }
            for (int i = 0; i < settingInfos.Count; i++)
            {
                SystemSettingInfo info = settingInfos[i];
                if (_settingDict.ContainsKey((SettingType)info.settingType))
                {
                    _settingDict[(SettingType)info.settingType] = info.value;
                }
            }

            for (int i = 0; i < pushSettingInfos.Count; i++)
            {
                PushSettingInfo info = pushSettingInfos[i];
                if (_pushSettingDict.ContainsKey(info.pushId))
                {
                    _pushSettingDict[info.pushId] = info.value;
                }
            }
        }

        /// <summary>
        /// 根据设置的类型获取对应的值
        /// 推送设置的值不在此获取，通过IsPushTypeSelected获取
        /// </summary>
        /// <param name="settingType"></param>
        /// <returns></returns>
        public int GetSettingValue(SettingType settingType)
        {
            if (!_settingDict.ContainsKey(settingType))
                return 0;
            int result = _settingDict[settingType];
            return result;
        }

        public void SetSettingValue(SettingType settingType, int value)
        {
            if (_settingDict.ContainsKey(settingType))
            {
                _settingDict[settingType] = value;
                EventDispatcher.TriggerEvent<SettingType>(SettingEvents.UPDATE_SYSTEM_SETTING, settingType);
            }
        }

        public void SetChatSpeechValue(ChatSpeechSettingType speechType, bool isSelected)
        {
            int value = GetSettingValue(SettingType.ChatSpeech);
            if (isSelected)
            {
                value |= (int)speechType;
            }
            else
            {
                value &= ~(int)speechType;
            }
            SetSettingValue(SettingType.ChatSpeech, value);
        }

        /// <summary>
        /// 是否指定的聊天语音类型被选中
        /// </summary>
        /// <param name="speechType"></param>
        /// <returns></returns>
        public bool IsChatSpeechTypeSelected(ChatSpeechSettingType speechType)
        {
            bool result = false;
            int value = GetSettingValue(SettingType.ChatSpeech);
            result = (value & (int)speechType) != 0 ? true : false;
            return result;
        }

        public void SetPushValue(int pushId, bool isSelected)
        {
            if (!_pushSettingDict.ContainsKey(pushId))
                return;
            _pushSettingDict[pushId] = isSelected;
            _isPushSettingChange = true;
            EventDispatcher.TriggerEvent<int>(SettingEvents.UPDATE_PUSH_SETTING, pushId);
        }

        /// <summary>
        /// 是否指定的推送类型被选中
        /// </summary>
        /// <param name="pushId">推送的id，为push.xml中的shield_i字段</param>
        /// <returns></returns>
        public bool IsPushTypeSelected(int pushId)
        {
            bool result = false;
            if (!_pushSettingDict.ContainsKey(pushId))
                return result;
            result = _pushSettingDict[pushId];
            return result;
        }

        //设置设备推送信息[(数据来源T推送通知配置.xlxs)]
        public string SetupNotificationData()
        {
            string strNotificationInfo = "";
            StringBuilder id = new StringBuilder("");
            StringBuilder time = new StringBuilder("");
            StringBuilder type = new StringBuilder("");
            StringBuilder tag = new StringBuilder("");
            StringBuilder title = new StringBuilder("");
            StringBuilder titleBanner = new StringBuilder("");
            StringBuilder content = new StringBuilder("");
            StringBuilder date = new StringBuilder("");
            foreach (notification item in XMLManager.notification.Values)
            {
                if (item.__id < 1) continue;
                if (string.IsNullOrEmpty(item.__time)) continue;
                if (!CanAddToNotification(item.__type)) continue;

                if (id.Length > 0) id.Append(PREFEX);
                if (time.Length > 0) time.Append(PREFEX);
                if (type.Length > 0) type.Append(PREFEX);
                if (tag.Length > 0) tag.Append(PREFEX);
                if (title.Length > 0) title.Append(PREFEX);
                if (titleBanner.Length > 0) titleBanner.Append(PREFEX);
                if (content.Length > 0) content.Append(PREFEX);
                if (date.Length > 0) date.Append(PREFEX);
                id.Append(item.__id);
                time.Append(item.__time);
                type.Append(item.__type);
                tag.Append(item.__tag);
                List<string> dataList = data_parse_helper.ParseListString(item.__date);
                for (int i = 0; i < dataList.Count; i++)
                {
                    date.Append(item.__date[i]);
                    if (i < dataList.Count - 1)
                    {
                        date.Append(",");
                    }
                }

                title.Append(XMLManager.chinese.ContainsKey(item.__title) ? XMLManager.chinese[item.__title].__content : "title2");
                titleBanner.Append(XMLManager.chinese.ContainsKey(item.__titleBanner) ? XMLManager.chinese[item.__titleBanner].__content : "title1");
                content.Append(XMLManager.chinese.ContainsKey(item.__content) ? XMLManager.chinese[item.__content].__content : "Notice has been Sended !");
            }
            if (id.Length > 0)
            {
                strNotificationInfo = string.Format("{0};{1};{2};{3};{4};{5};{6};{7}", id, time, date, type, tag, title, titleBanner, content);
                //PlatformSdkMgr.Instance.SetupNotificationData(id.ToString(), time.ToString(), date.ToString(), type.ToString(), tag.ToString(), title.ToString(), titleBanner.ToString(), content.ToString());
            }
            return strNotificationInfo;
        }

        public void SetupLoginPushNotification()
        {
            int infoCount = 8;
            StringBuilder[] infos = new StringBuilder[infoCount];
            for (int i = 0; i < infoCount; i++)
            {
                StringBuilder strBuilder = new StringBuilder("");
                infos[i] = strBuilder;
            }
            bool isSetupDailyNotify = false;
            bool isSetupLoginPush = false;
            string strDailyNotificationInfo = SetupNotificationData();
            if (!string.IsNullOrEmpty(strDailyNotificationInfo))
            {
                string[] dailyInfoSplits = strDailyNotificationInfo.Split(new char[] { ';' });
                if (dailyInfoSplits.Length < infoCount)
                    return;
                isSetupDailyNotify = true;
                for (int i = 0; i < infos.Length; i++)
                {
                    if (infos[i].Length > 0)
                    {
                        infos[i].Append(PREFEX);
                    }
                    infos[i].Append(dailyInfoSplits[i]);
                }
            }

            string strNotificationInfo = _loginPushData.AddLoginPushNotification();
            if (!string.IsNullOrEmpty(strNotificationInfo))
            {
                string[] notificationSplits = strNotificationInfo.Split(new char[] { '_' });
                for (int i = 0; i < notificationSplits.Length; i++)
                {
                    string[] infoSplits = notificationSplits[i].Split(new char[] { ',' });
                    if (infoSplits.Length < infoCount)
                        return;
                    isSetupLoginPush = true;
                    for (int j = 0; j < infos.Length; j++)
                    {
                        if (infos[j].Length > 0)
                        {
                            infos[j].Append(PREFEX);
                        }
                        infos[j].Append(infoSplits[j]);
                    }
                }
            }

            if (infos[0].Length > 0)
            {
                PlatformSdkMgr.Instance.SetupNotificationData(infos[0].ToString(), infos[1].ToString(), infos[2].ToString(), infos[3].ToString(), infos[4].ToString(), infos[5].ToString(), infos[6].ToString(), infos[7].ToString(), 0);
                if (isSetupDailyNotify)
                    LoggerHelper.Info("设置设备推送信息 [已设置]");
                if (isSetupLoginPush)
                    LoggerHelper.Info("设置设备登录提醒推送消息 [已设置]");
            }
        }

        private bool CanAddToNotification(int pushId)
        {
            return IsPushTypeSelected(pushId);
        }

        private void OnInitTaskCompleted(List<int> completeTaskIdList)
        {
            _loginPushData.InitCompleteTaskIds(completeTaskIdList);
            EventDispatcher.RemoveEventListener<List<int>>(SettingEvents.INIT_TASK_COMPLETED, OnInitTaskCompleted);
        }

        public void ShowCustomServiceBox()
        {
            string strQQ = MogoLanguageUtil.GetContent(73013);
            string strTelephone = MogoLanguageUtil.GetContent(73014);
            //MessageBox.Show(true, MogoLanguageUtil.GetContent(73011), strQQ + "\n\n" + strTelephone);
        }

        public Dictionary<SettingType, int> SettingDict
        {
            get { return _settingDict; }
        }

        public Dictionary<int, bool> PushSettingDict
        {
            get { return _pushSettingDict; }
        }

        //自定义保存设置数据信息的结构，用于保存、读取本地缓存
        public class LocalSettingCache
        {
            public List<SystemSettingInfo> sysSettingInfos;
            public List<PushSettingInfo> pushSettingInfos;
        }

        public class SystemSettingInfo
        {
            public int settingType;
            public int value;
        }

        public class PushSettingInfo
        {
            public int pushId;
            public bool value;
        }

    }

    public enum SettingType
    {
        Music = 1,
        Sound = 2,
        Speech = 3,
        PlayerCount,
        RenderQuality,
        ChatSpeech,
        Push
    }

    public enum ChatSpeechSettingType
    {
        Guild = 1,
        Team = 2,
        Near = 4,
        World = 8
    }
}
