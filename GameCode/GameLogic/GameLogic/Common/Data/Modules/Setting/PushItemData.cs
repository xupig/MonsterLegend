﻿using System;
using System.Collections.Generic;
using Common.Utils;
using GameData;

namespace Common.Data
{
    public class PushItemData
    {
        private string _name = string.Empty;
        private string _desc = string.Empty;
        private push _pushInfo;
        private bool _defaultOpen = true;

        public PushItemData(int id)
        {
            this.Id = id;
            _pushInfo = push_helper.GetPushInfo(id);
            if (_pushInfo != null)
            {
                _defaultOpen = _pushInfo.__default_shield == 0 ? true : false;
            }
        }

        public int Id
        {
            protected set;
            get;
        }

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    if (_pushInfo != null)
                    {
                        _name = MogoLanguageUtil.GetContent(_pushInfo.__title);
                    }
                }
                return _name;
            }
        }

        public string Desc
        {
            get
            {
                if (string.IsNullOrEmpty(_desc))
                {
                    if (_pushInfo != null)
                    {
                        _desc = MogoLanguageUtil.GetContent(_pushInfo.__desc);
                    }
                }
                return _desc;
            }
        }

        public bool DefaultOpen
        {
            get { return _defaultOpen; }
        }
    }
}
