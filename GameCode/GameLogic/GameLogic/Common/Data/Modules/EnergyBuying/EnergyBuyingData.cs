﻿using System;
using System.Collections.Generic;
using Common.ClientConfig;
using Common.Global;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class EnergyBuyingData
    {
        private int _buyTimes = 0;
        private int _remainBuyTimes = 0;

        private bool _isBuying = false;
        private int _buyOnceStatus = 0;
        private int _buyAllStatus = 0;
        private int _curOwnMoney = 0;
        private int _curEnergy = 0;

        public const int BUY_TYPE_ONCE = 1;
        public const int BUY_TYPE_ALL = 2;

        public int BuyTimes
        {
            get { return _buyTimes; }
        }

        public int RemainBuyTimes
        {
            get { return _remainBuyTimes; }
        }

        public bool IsBuying
        {
            get { return _isBuying; }
            set { _isBuying = value; }
        }

        public int BuyOnceStatus
        {
            get { return _buyOnceStatus; }
        }

        public int BuyAllStatus
        {
            get { return _buyAllStatus; }
        }

        public EnergyBuyingData()
        {
            InitRemainBuyTimes();
            AddEventListener();
        }

        private void InitRemainBuyTimes()
        {
            UpdateRemainBuyTimes();
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, OnUpdateEnergyBuyData);
        }

        private void OnUpdateEnergyBuyData()
        {
            UpdateRemainBuyTimes();
            EventDispatcher.TriggerEvent(EnergyBuyingEvents.UPDATE_BUY_TIMES);
        }

        public void UpdateRemainBuyTimes()
        {
            int curVip = PlayerAvatar.Player.vip_level;
            _remainBuyTimes = int.Parse(vip_rights_helper.GetVipRights(VipRightType.EnergyBuyTimes, curVip)) - _buyTimes;
        }

        public void UpdateBuyTimes(int buyTimes)
        {
            _buyTimes = buyTimes;
            UpdateRemainBuyTimes();
        }

        public void ResetBuyTimes()
        {
            UpdateBuyTimes(0);
        }

        public void UpdateBuyOnce()
        {
            UpdateBuyTimes(_buyTimes + 1);
        }

        public void UpdateBuyAll()
        {
            int costNum = 0;
            int buyTimes = GetMaxBuyTimes(out costNum) + _buyTimes;
            UpdateBuyTimes(buyTimes);
        }

        public int GetBuyOnceEnergyNum()
        {
            int energyNum = GlobalParams.GetBuyOnceEnergyNum();
            _buyOnceStatus = GetBuyStatus();
            return energyNum;
        }

        public int GetBuyAllEnergyNum()
        {
            int energyNum = 0;
            int costNum = 0;
            int times = 0;
            times = GetMaxBuyTimes(out costNum);
            energyNum = GlobalParams.GetBuyOnceEnergyNum() * times;
            _buyAllStatus = GetBuyStatus();
            return energyNum;
        }

        private int GetMaxBuyTimes(out int costNum)
        {
            int times = 0;
            int curOwnMoney = GetCurOwnMoney();
            costNum = 0;
            for (int i = 0; i < _remainBuyTimes; i++)
            {
                BaseItemData itemData = GetCostItem(_buyTimes + i + 1);
                costNum += itemData.StackCount;
                //当体力到达上限的处理
                int curEnergyNum = GlobalParams.GetBuyOnceEnergyNum() * (times + 1) + PlayerAvatar.Player.energy;
                if (costNum <= curOwnMoney && curEnergyNum <= GlobalParams.GetEnergyLimit())
                {
                    times++;
                }
                else
                {
                    costNum -= itemData.StackCount;
                    break;
                }
            }
            return times;
        }

        private int GetBuyStatus()
        {
            int buyStatus = 0;
            if (_remainBuyTimes == 0)
            {
                buyStatus = (int)BuyEnergyStatus.NotHaveBuyTimes;
            }
            else if (GlobalParams.GetBuyOnceEnergyNum() + PlayerAvatar.Player.energy > GlobalParams.GetEnergyLimit())
            {
                buyStatus = (int)BuyEnergyStatus.ReachEnergyLimit;
            }
            else if (GetBuyOnceCostItem().StackCount > GetCurOwnMoney())
            {
                buyStatus = (int)BuyEnergyStatus.NotEnoughMoney;
            }
            else
            {
                buyStatus = (int)BuyEnergyStatus.CanBuy;
            }
            return buyStatus;
        }

        public BaseItemData GetCostItem(int times)
        {
            if (times == 0)
            {
                return price_list_helper.GetCost((int)PriceListId.energyBuy);
            }
            price_list priceList = price_list_helper.GetConfig((int)PriceListId.energyBuy);
            int num = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(priceList.__price_list);
            foreach (var item in dataDict)
            {
                if (int.Parse(item.Key) >= times)
                {
                    num = int.Parse(item.Key);
                    break;
                }
            }
            return price_list_helper.GetCost((int)PriceListId.energyBuy, num);
        }

        public BaseItemData GetBuyOnceCostItem()
        {
            int times = (_buyTimes + 1) <= (_remainBuyTimes + _buyTimes) ? _buyTimes + 1 : _buyTimes;
            return GetCostItem(times);
        }

        public BaseItemData GetBuyAllCostItem()
        {
            int costNum= 0;
            GetMaxBuyTimes(out costNum);
            BaseItemData itemData = ItemDataCreator.Create(GetCostItem(0).Id);
            itemData.StackCount = costNum;
            return itemData;
        }

        public int GetCurOwnMoney()
        {
            //当前角色拥有的钱的数量，根据消耗品的配置来定，如果消耗品为钻石或绑定钻石，
            //那么所拥有的钱的数量为钻石和绑定钻石之和
            int curOwnMoney = 0;
            int costItemId = GetCostItem(0).Id;
            if (costItemId == public_config.MONEY_TYPE_DIAMOND || costItemId == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                curOwnMoney = PlayerAvatar.Player.GetItemNum(public_config.MONEY_TYPE_DIAMOND);
                curOwnMoney += PlayerAvatar.Player.GetItemNum(public_config.MONEY_TYPE_BIND_DIAMOND);
            }
            else
            {
                curOwnMoney = PlayerAvatar.Player.GetItemNum(costItemId);
            }
            return curOwnMoney;
        }

        public bool IsCurOwnMoneyChanged()
        {
            int ownMoney = GetCurOwnMoney();
            if (_curOwnMoney != ownMoney)
            {
                _curOwnMoney = ownMoney;
                return true;
            }
            return false;
        }

        public bool IsCurEnergyChanged()
        {
            int energy = PlayerAvatar.Player.energy;
            if (_curEnergy != energy)
            {
                _curEnergy = energy;
                return true;
            }
            return false;
        }
        
    }

    public enum BuyEnergyStatus
    {
        CanBuy = 0,
        NotHaveBuyTimes = 1,
        ReachEnergyLimit = 2,
        NotEnoughMoney = 3
    }
}
