﻿using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class DemonGateConditionData
    {
        public int actionId;
        public float actionValue;
        public string actionValueStr;
        public int score;
        public bool isMaxScore;
        public string desc;

        public DemonGateConditionData(int mapId, PbActionInfo pbActionInfo)
        {
            actionId = (int)pbActionInfo.action_id;
            actionValue = pbActionInfo.val;
            score = (int)pbActionInfo.score;
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);
            string[] scorlRuleArray = dicScoreRule[actionId.ToString()];
            if (actionId == 3 || actionId == 59)  //如果是玩家血量比或者Boss血量比（对应） 关卡配置表中配置
            {
                actionValueStr = string.Format("{0}%", (int)(actionValue * 100));
            }
            else
            {
                actionValueStr = ((int)(actionValue)).ToString();
            }
            desc = map_helper.GetDesc(mapId, actionId);
            //string.Format(map_helper.GetDesc(mapId, actionId), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_CHAT_RUMOR, valueStr));
            isMaxScore = score == int.Parse(scorlRuleArray[map_helper.MAX_SCORE]);
            //Debug.LogError(string.Format("actionId:{0}  actionValue:{1} score:{2}", actionId, actionValue, score));
        }


        public override string ToString()
        {
            return string.Format("actionId:{0}  actionValue:{1} score:{2}   desc{3}", actionId, actionValue, score, desc);
        }
    }
}
