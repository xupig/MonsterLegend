﻿#region 模块信息
/*==========================================
// 文件名：FriendData
// 命名空间: Common.Data
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/9 15:41:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using UnityEngine;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using GameData;

namespace Common.Data
{
    public class DemonGateData
    {
        public DemonGateData()
        {

        }
        
        public PbEvilMissionInfo SelfRankInfo;
        public UInt32 WorldLevel;
        public bool IsInitData;
    }
}
