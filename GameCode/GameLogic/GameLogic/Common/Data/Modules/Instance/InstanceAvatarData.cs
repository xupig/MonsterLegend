﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class InstanceAvatarData
    {
        /// <summary>
        /// 己方玩家信息，Key为实体id
        /// </summary>
        public Dictionary<UInt64, PbHpMember> HpOurMemberDic;

        /// <summary>
        /// 敌方玩家信息，Key为实体id
        /// </summary>
        public Dictionary<UInt64, PbHpMember> HpEnemyMemberDic;

        public Dictionary<int, int> ScoreDict;

        public InstanceAvatarData()
        {
            HpOurMemberDic = new Dictionary<UInt64, PbHpMember>();
            HpEnemyMemberDic = new Dictionary<UInt64, PbHpMember>();
            ScoreDict = new Dictionary<int, int>();
        }

        private uint GetMyTeamCampId(PbHpMemberList pbList)
        {
            for (int i = 0; i < pbList.hpMemberList.Count; i++)
            {
                if (pbList.hpMemberList[i].dbid == PlayerAvatar.Player.dbid)
                {
                    return pbList.hpMemberList[i].map_camp_id;
                }
            }
            if (HpOurMemberDic.ContainsKey(PlayerAvatar.Player.id) == true)
            {
                return HpOurMemberDic[PlayerAvatar.Player.id].map_camp_id;
            }
            return 0;
        }

        public void ResetHpMember()
        {
            HpOurMemberDic.Clear();
            HpEnemyMemberDic.Clear();
        }

        public void FillHpMember(PbHpMemberList pbList)
        {
            uint myCampId = GetMyTeamCampId(pbList);
            if (myCampId == 0)
            {
                LoggerHelper.Error(string.Format("无自己阵营信息"));
                return;
            }
            for (int i = 0; i < pbList.hpMemberList.Count; i++)
            {
                PbHpMember info = pbList.hpMemberList[i];
                if (info.map_camp_id == myCampId)
                {
                    if (HpOurMemberDic.ContainsKey(info.avatar_id) == false)
                    {
                        HpOurMemberDic.Add(info.avatar_id, info);
                        EventDispatcher.TriggerEvent<uint>(EveningActivityEvents.Avatar_Enter_Instance, info.avatar_id);
                    }
                    else
                    {
                        HpOurMemberDic[info.avatar_id] = info;
                    }
                }
                else
                {
                    if (HpEnemyMemberDic.ContainsKey(info.avatar_id) == false)
                    {
                        HpEnemyMemberDic.Add(info.avatar_id, info);
                        EventDispatcher.TriggerEvent<uint>(EveningActivityEvents.Avatar_Enter_Instance, info.avatar_id);

                    }
                    else
                    {
                        HpOurMemberDic[info.avatar_id] = info;
                    }
                }
            }
        }

        public uint OurCampId
        {
            get
            {
                if (HpOurMemberDic.ContainsKey(PlayerAvatar.Player.id) == true)
                {
                    return HpOurMemberDic[PlayerAvatar.Player.id].map_camp_id;
                }
                return 0;
            }
        }

        public uint GetCampId(uint avatarId)
        {
            if (HpOurMemberDic.ContainsKey(avatarId) == true)
            {
                return HpOurMemberDic[avatarId].map_camp_id;
            }
            else if (HpEnemyMemberDic.ContainsKey(avatarId) == true)
            {
                return HpEnemyMemberDic[avatarId].map_camp_id;
            }
            else
            {
                return 0;
            }
        }

        public void RemoveAvatarData(UInt32 id)
        {
            if (HpOurMemberDic.ContainsKey(id) == true)
            {
                HpOurMemberDic.Remove(id);
            }
            if (HpEnemyMemberDic.ContainsKey(id) == true)
            {
                HpEnemyMemberDic.Remove(id);
            }

            EventDispatcher.TriggerEvent<uint>(EveningActivityEvents.Avatar_Leave_Instance, id);
        }
    }
}
