﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
using ACTSystem;
using Common.Structs;
namespace Common.Data
{
    public class AvatarEquipInfo
    {
        public int equipID;
        public int flow;
        public int particle;
    }

    public class AvatarModelData
    {
        public Vocation vocation;
        public Dictionary<ACTEquipmentType, AvatarEquipInfo> equips;

        public AvatarModelData()
        {
            equips = new Dictionary<ACTEquipmentType, AvatarEquipInfo>();
            equips[ACTEquipmentType.Cloth] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Weapon] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Wing] = new AvatarEquipInfo();
        }

        public AvatarEquipInfo GetEquipInfo(ACTEquipmentType type)
        {
            return equips[type];
        }
    }
}
