﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using Common.Utils;
using Common.ServerConfig;

namespace Common.Data
{
    public class WingFeatherCostData
    {
        private int id;
        private int level;

        public WingFeatherCostData(int wingId, int wingLevel)
        {
            id = wingId;
            level = wingLevel;
        }

        public int FeatherId
        {
            get
            {
                return wing_helper.GetWingTrainCostFeather(id, level);
            }
        }

        public int FeatherNum
        {
            get
            {
                return wing_helper.GetWingTrainCostNumber(id, level);
            }
        }

        public int FeatherDiamond
        {
            get
            {
                return wing_helper.GetWingPerFeatherDiamond(id, FeatherId);
            }
        }

        public int FeatherExp
        {
            get
            {
                return wing_helper.GetWingPerFeatherExp(id, FeatherId);
            }
        }

        public int AddExp
        {
            get
            {
                return FeatherExp * FeatherNum;
            }
        }

        public int FeatherIcon
        {
            get
            {
                if (FeatherId != 0)
                {
                    return item_helper.GetItemConfig(FeatherId).__icon;
                }
                return 0;
            }
        }

        public string FeatherName
        {
            get
            {
                string name = string.Empty;
                if (FeatherId != 0)
                {
                    List<string> dataList = data_parse_helper.ParseListString(item_helper.GetItemConfig(FeatherId).__name);
                    foreach (string nameId in dataList)
                    {
                        name = string.Concat(name,MogoLanguageUtil.GetContent(int.Parse(nameId)));
                    }
                }
                return name;
            }
        }

        public int DiamondIcon
        {
            get
            {
                return item_helper.GetItemConfig(public_config.MONEY_TYPE_DIAMOND).__icon;
            }
        }

        public string DiamondName
        {
            get
            {
                string name = string.Empty;
                List<string> dataList = data_parse_helper.ParseListString(item_helper.GetItemConfig(public_config.MONEY_TYPE_DIAMOND).__name);
                foreach (string nameId in dataList)
                {
                    name = string.Concat(name, MogoLanguageUtil.GetContent(int.Parse(nameId)));
                }
                return name;
            }
        }
    }
}
