using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Common.Data
{
    public class WingData
    {
        /// <summary>
        /// 所有翅膀数据,Key翅膀Id
        /// </summary>
        private Dictionary<int, WingItemData> _wingItemDataDict;
        /// <summary>
        /// 翅膀套装属性效果
        /// </summary>
        private List<WingSuitAttributeData> _wingSuitDataList;
        /// <summary>
        /// 已拥有翅膀的信息
        /// </summary>
        private PbWingBagList _pbWingBagList;
        /// <summary>
        /// 穿在身上的翅膀
        /// </summary>
        private WingItemData _wingPutOnData;
        /// <summary>
        /// 所拥有翅膀的所有属性总览,Key为属性id，Value为属性
        /// </summary>
        private Dictionary<int, WingAttributeData> _allArrtibuteDict;

        public List<int> newWingList;

        public List<int> canTrainList;

        public List<int> canComposeList;

        public HashSet<int> featherSet;

        public int Rank { get; set; }

        private bool _isInit = false;

        public WingData()
        {
            _wingItemDataDict = new Dictionary<int, WingItemData>();
            _allArrtibuteDict = new Dictionary<int, WingAttributeData>();
            _wingSuitDataList = new List<WingSuitAttributeData>();
            canComposeList = new List<int>();
            canTrainList = new List<int>();
            newWingList = new List<int>();
            featherSet = new HashSet<int>();
            InitFeatherSet();
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, CheckWingCanTrain);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, CheckWingCanTrain);
        }

        private void InitFeatherSet()
        {
            foreach (wing data in XMLManager.wing.Values)
            {
                Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList(data.__train_cost);
                foreach (string[] ss in dataDictList.Values)
                {
                    featherSet.Add(int.Parse(ss[0]));
                }
            }
        }

        private void CheckHasNewWing()
        {
            //player_helper.CheckInformationCanOperation();
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_NOTICE_POINT);
        }

        private void CheckWingCanTrain()
        {
            canTrainList.Clear();
            foreach (WingItemData data in _wingItemDataDict.Values)
            {
                if (CheckCanTrain(data.wingId))
                {
                    canTrainList.Add(data.wingId);
                }
            }
            //player_helper.CheckInformationCanOperation();
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_NOTICE_POINT);
        }

        private void CheckWingCanTrain(BagType bagType, uint gridPosition)
        {
            canComposeList.Clear();
            canTrainList.Clear();
            foreach (WingItemData data in _wingItemDataDict.Values)
            {
                if (CheckCanTrain(data.wingId))
                {
                    canTrainList.Add(data.wingId);
                }
                if (CheckCanCompose(data.wingId))
                {
                    canComposeList.Add(data.wingId);
                }
            }
            
            //player_helper.CheckInformationCanOperation();
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_NOTICE_POINT);
        }

        public bool HasNewWingByType(WingType type)
        {
            for (int i = 0; i < newWingList.Count; i++)
            {
                WingItemData data = _wingItemDataDict[newWingList[i]];
                if (data.type == type || type == WingType.NONE)
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveNewWingByType(WingType type)
        {
            for (int i = newWingList.Count - 1; i >= 0; i--)
            {
                WingItemData data = _wingItemDataDict[newWingList[i]];
                if (data.type == type || type == WingType.NONE)
                {
                    newWingList.RemoveAt(i);
                }
            }
            CheckHasNewWing();
        }

        public bool CanTrainByType(WingType type)
        {
            for (int i = canTrainList.Count - 1; i >= 0; i--)
            {
                WingItemData data = _wingItemDataDict[canTrainList[i]];
                if (data.type == type || type == WingType.NONE)
                {
                    return true;
                }
            }
            return false;
        }

        public bool CanTrain(int wingId)
        {
            for (int i = canTrainList.Count - 1; i >= 0; i--)
            {
                WingItemData data = _wingItemDataDict[canTrainList[i]];
                if (data.wingId == wingId)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckCanTrain(int wingId)
        {

            WingItemData data = _wingItemDataDict[wingId];
            if (data.isOwned == true && data.canTrain == true && data.level < data.maxLevel)
            {
                WingFeatherCostData costData = GetWingCostData(data.wingId, data.level);
                int myFeatherNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(costData.FeatherId);
                if (myFeatherNum >= costData.FeatherNum 
                    && wing_helper.GetWingTrainNeedVipLevel(data.wingId, data.level) <= PlayerAvatar.Player.vip_level)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckCanCompose(int wingId)
        {
            WingItemData data = _wingItemDataDict[wingId];
            if (data.isOwned == false)
            {
                int needNum = data.ComposeMaterial[0].Value;
                int currentNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(data.ComposeMaterial[0].Key);
                if (currentNum >= needNum)
                {
                    return true;
                }
            }
            return false;
        }

        public void Fill(PbWingBagList pbWingBagList)
        {
            InitConfig();
            UpdateWingData(pbWingBagList);
            UpdateSuitAttributeData();
            _pbWingBagList = pbWingBagList;
            PutOnNewWing();
            CheckWingCanTrain();
        }

        private void PutOnNewWing()
        {
            if (newWingList.Count > 0)
            {
                WingManager.Instance.PutOnWing(newWingList[newWingList.Count - 1]);
            }
        }

        private void InitConfig()
        {
            if (_isInit == true)
            {
                return;
            }
            _isInit = true;
            foreach (int key in XMLManager.wing.Keys)
            {
                WingItemData itemData = new WingItemData(key);
                _wingItemDataDict.Add(key, itemData);
            }
            InitSuitAttributeData();
        }


        private void InitSuitAttributeData()
        {
            foreach (KeyValuePair<int, wing_suit> kvp in XMLManager.wing_suit)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__effect_id);
                int effectId = int.Parse(dataList[0]);
                attri_effect effect = XMLManager.attri_effect[effectId];
                List<string> dataList2 = data_parse_helper.ParseListString(effect.__attri_ids);
                int attriId = int.Parse(dataList2[0]);
                List<string> dataList3 = data_parse_helper.ParseListString(effect.__attri_values);
                int value = int.Parse(dataList3[0]);
                int descId = kvp.Value.__descrip;
                List<string> dataList4 = data_parse_helper.ParseListString(kvp.Value.__need);
                int needCount = int.Parse(dataList4[0]);
                int needLevel = int.Parse(dataList4[1]);
                WingSuitAttributeData suitData = GetSuitData((WingType)kvp.Value.__type, attriId);
                suitData.value = value;
                suitData.needCount = needCount;
                suitData.needLevel = needLevel;
                suitData.description = MogoLanguageUtil.GetContent(descId, needCount, needLevel);
                suitData.isActive = false;

            }
        }

        public void UpdatePutOnData(int id)
        {
            if (_wingPutOnData != null)
            {
                _wingPutOnData.isPutOn = false;
            }
            if (id != 0)
            {
                _wingPutOnData = _wingItemDataDict[id];
                _wingPutOnData.isPutOn = true;
            }
            else
            {
                _wingPutOnData = null;
            }
        }

        public void UpdateWingExp(int id, UInt16 curExp)
        {
            WingItemData data = _wingItemDataDict[id];
            if (data != null)
            {
                int oldLevel = data.level;
                data.Exp = curExp;
                if (data.level != oldLevel)
                {
                    UpdateSuitAttributeData();
                    EventDispatcher.TriggerEvent<int>(WingEvents.CHANGE_WING_MODEL, data.wingId);
                    EventDispatcher.TriggerEvent(WingEvents.UPDATE_PUT_ON_WING);
                }
            }
            CheckWingCanTrain();
        }

        public WingItemData GetItemDataById(int wingId)
        {
            return _wingItemDataDict[wingId];
        }

        public Dictionary<int, WingItemData> GetAllItemData()
        {
            Dictionary<int, WingItemData> result = new Dictionary<int, WingItemData>();
            List<int> keyList = _wingItemDataDict.Keys.ToList<int>();
            foreach(int key in keyList)
            {
                result[key] = _wingItemDataDict[key];
            }
            return result;
        }

        public List<WingSuitAttributeData> GetWingSuitDataList(WingType wingType)
        {
            List<WingSuitAttributeData> result = new List<WingSuitAttributeData>();
            foreach (WingSuitAttributeData data in _wingSuitDataList)
            {
                if (data.type == wingType)
                {
                    result.Add(data);
                }
            }
            return result;
        }

        public List<WingAttributeData> GetAllArrtibuteList()
        {
            UpdateAllArrtibuteList();
            List<WingAttributeData> result = new List<WingAttributeData>();
            foreach (WingAttributeData data in _allArrtibuteDict.Values)
            {
                result.Add(data);
            }
            return result;
        }

        public WingItemData GetPutOnWingData()
        {
            return _wingPutOnData;
        }

        public WingFeatherCostData GetWingCostData(int id, int level)
        {
            WingFeatherCostData costData = new WingFeatherCostData(id, level);
            return costData;
        }

        private void UpdateAllArrtibuteList()
        {
            foreach (WingAttributeData attributeData in _allArrtibuteDict.Values)
            {
                attributeData.value = 0;
                attributeData.maxValue = 0;
            }

            foreach (WingItemData data in _wingItemDataDict.Values)
            {
                if(data.isOwned == false)
                {
                    continue;
                }
                foreach(WingAttributeData attribute in data.ArrtibuteList)
                {
                    AddAllAttributeData(attribute.attriId, attribute.value, attribute.maxValue);
                }
            }
            foreach (WingSuitAttributeData suitData in _wingSuitDataList)
            {
                if (suitData.isActive == true)
                {
                    AddAllAttributeData(suitData.attriId, suitData.value, 0);
                }
            }
        }

        private void AddAllAttributeData(int attriId, int value, int maxValue)
        {
            if (_allArrtibuteDict.ContainsKey(attriId) == false)
            {
                WingAttributeData attribute = new WingAttributeData();
                attribute.attriId = attriId;
                attribute.value = value;
                attribute.maxValue = maxValue;
                _allArrtibuteDict.Add(attribute.attriId, attribute);
            }
            else
            {
                WingAttributeData attribute = _allArrtibuteDict[attriId];
                attribute.value += value;
                attribute.maxValue += maxValue;
            }
        }

        private void UpdateSuitAttributeData()
        {
            foreach (WingSuitAttributeData data in _wingSuitDataList)
            {
                if (data.isActive == false)
                {
                    bool isSatisfy = CheckSuitAttributeIsSatisfy(data.type, data.needCount, data.needLevel);
                    if (isSatisfy == true)
                    {
                        data.isActive = true;
                        OnActivateWingSuit(data.type);
                    }
                }
            }
        }

        private void OnActivateWingSuit(WingType type)
        {
            if (_pbWingBagList == null) return;
            string content = string.Empty;
            if (type == WingType.SPIRIT)
            {
                content = MogoLanguageUtil.GetContent(35001);
            }
            else if (type == WingType.PHANTOM)
            {
                content = MogoLanguageUtil.GetContent(35002);
            }
            //Ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.MainUIField);
        }

        private void UpdateWingData(PbWingBagList pbWingBagList)
        {
            foreach (PbWingBagInfo info in pbWingBagList.wing_bag_info)
            {
                WingItemData data = _wingItemDataDict[info.wing_id];
                if (data != null)
                {
                    if (_pbWingBagList != null && data.isOwned == false)
                    {
                        newWingList.Add(data.wingId);
                        CheckHasNewWing();
                    }
                    data.Exp = info.wing_exp;
                    data.isOwned = true;
                }
            }
        }

        public void AddWing(PbWingBagInfo wingInfo)
        {
            WingItemData data = _wingItemDataDict[wingInfo.wing_id];
            if (data != null)
            {
                if (_pbWingBagList != null && data.isOwned == false)
                {
                    newWingList.Add(data.wingId);
                    CheckHasNewWing();
                }
                data.Exp = wingInfo.wing_exp;
                data.isOwned = true;
            }
            UpdateSuitAttributeData();
            PutOnNewWing();
            CheckWingCanTrain();
        }

        private bool CheckSuitAttributeIsSatisfy(WingType wingType, int needCount, int needLevel)
        {
            int count = 0;
            foreach (WingItemData data in _wingItemDataDict.Values)
            {
                if (data.isOwned == true && data.type == wingType)
                {
                    if (data.level >= needLevel)
                    {
                        count++;
                    }
                }
            }
            return count >= needCount;
        }

        private WingSuitAttributeData GetSuitData(WingType wingType, int attriId)
        {
            WingSuitAttributeData suitData = new WingSuitAttributeData();
            suitData.type = wingType;
            suitData.attriId = attriId;
            _wingSuitDataList.Add(suitData);
            return suitData;
        }

    }
}
