using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;

namespace Common.Data
{
    public enum WingTrainType
    {
        One = 0,    //培养一次
        Ten = 1,    //培养十次
    }

    public enum WingType
    {
        /// <summary>
        /// 默认为没有
        /// </summary>
        NONE = 0,
        /// <summary>
        /// 精灵之翼
        /// </summary>
        SPIRIT = 1,
        /// <summary>
        /// 幻影之翼
        /// </summary>
        PHANTOM = 2,
    }

    public class WingItemData
    {
        public int wingId ;
        public string name;
        public WingType type;
        public int level;
        public int totalExp;
        public int maxLevel;
        public string description;
        public string getMethod;
        public int mallId;
        public int icon;
        public int scale;
        public int fightForce;
        public int canBuy;
        public bool canTrain;
        public int effectId;

        /// <summary>
        /// 以下字段来自服务器
        /// </summary>
        public int currentExp;
        public bool isOwned;
        public bool isPutOn;

        public int ModelId
        {
            get
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(XMLManager.wing[wingId].__equip);
                if (level == 0)
                {
                    return int.Parse(dict["1"]);
                }
                return int.Parse(dict[level.ToString()]);
            }
        }

        public List<WingAttributeData> ArrtibuteList = new List<WingAttributeData>();
        public List<KeyValuePair<int, int>> ComposeMaterial = new List<KeyValuePair<int, int>>();

        public WingItemData(int _id)
        {
            wingId = _id;
            isOwned = false;
            isPutOn = false;
            ParseWingConfig(_id);
        }

        private void ParseWingConfig(int _id)
        {
            wing _wing = XMLManager.wing[_id];
            name = MogoLanguageUtil.GetContent(_wing.__name);
            description = MogoLanguageUtil.GetContent(_wing.__descrip);
            getMethod = MogoLanguageUtil.GetContent(_wing.__unlock_descrip);
            type = (WingType)(_wing.__type);
            icon = _wing.__icon;
            maxLevel = _wing.__max_level;
            canTrain = maxLevel > 1;
            canBuy = _wing.__buy_flag;
            mallId = _wing.__mall;
            UpdateAttributeData();
            UpdateComposeMaterial();
        }

        private void UpdateAttributeData()
        {
            wing _wing = XMLManager.wing[wingId];
            Dictionary<string, string[]> dict = data_parse_helper.ParseMapList(_wing.__exp_effect_id);
            foreach (KeyValuePair<string, string[]> kvp in dict)
            {
                int startExp = int.Parse(kvp.Key);
                int endExp = int.Parse(kvp.Value[0]);
                int division = int.Parse(kvp.Value[1]);
                int startEffectId = int.Parse(kvp.Value[2]);
                if (startExp <= _exp && _exp < endExp || (level == maxLevel && startExp <= _exp && _exp <= endExp) || canTrain == false)
                {
                    int relativeExp = _exp - startExp;
                    int relativeId = 0;
                    if (division != 0)
                    {
                        relativeId = relativeExp / division;
                    }
                    effectId = startEffectId + relativeId;
                    fightForce = fight_force_helper.GetFightForce(effectId, PlayerAvatar.Player.vocation);
                    attri_effect effect = XMLManager.attri_effect[effectId];
                    ArrtibuteList.Clear();
                    List<string> dataList1 = data_parse_helper.ParseListString(effect.__attri_ids);
                    List<string> dataList2 = data_parse_helper.ParseListString(effect.__attri_values);
                    for (int i = 0; i < dataList1.Count; i++)
                    {
                        int attriId = int.Parse(dataList1[i]);
                        int value = int.Parse(dataList2[i]);
                        if ((fight_attri_config)attriId == fight_attri_config.DMG_MIN)
                        {
                            AddDamageAttribute(value, true);
                        }
                        else if ((fight_attri_config)attriId == fight_attri_config.DMG_MAX)
                        {
                            AddDamageAttribute(value, false);
                        }
                        else
                        {
                            WingAttributeData attributeData = new WingAttributeData();
                            attributeData.attriId = attriId;
                            attributeData.value = int.Parse(dataList2[i]);

                            ArrtibuteList.Add(attributeData);
                        }
                    }
                    break;
                }
            }
        }

        private void UpdateComposeMaterial()
        {
            wing _wing = XMLManager.wing[wingId];
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(_wing.__complex);
            foreach (KeyValuePair<string, string> kvp in dataDict)
            {
                ComposeMaterial.Add(new KeyValuePair<int, int>(int.Parse(kvp.Key), int.Parse(kvp.Value)));
            }
        }

        private void AddDamageAttribute(int value, bool isMin)
        {
            WingAttributeData attributeData = null;
            for (int i = 0; i < ArrtibuteList.Count; i++)
            {
                if (ArrtibuteList[i].attriId == (int)fight_attri_config.DMG_MIN)
                {
                    attributeData = ArrtibuteList[i];
                }
            }
            if (attributeData == null)
            {
                attributeData = new WingAttributeData();
                attributeData.attriId = (int)fight_attri_config.DMG_MIN;
                ArrtibuteList.Add(attributeData);
            }
            if (isMin)
            {
                attributeData.value = value;
            }
            else
            {
                attributeData.maxValue = value;
            }
        }

        private int _exp;

        public int Exp
        {
            get { return _exp; }
            set
            { 
                _exp = value;
                wing _wing = XMLManager.wing[wingId];
                int totalNeed = 0;
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(_wing.__exp_need);
                foreach (KeyValuePair<string, string> kvp in dataDict)
                {
                    totalNeed += int.Parse(kvp.Value);
                    if (_exp < totalNeed)
                    {
                        level = int.Parse(kvp.Key);
                        int lastExp = totalNeed - int.Parse(kvp.Value);
                        currentExp = _exp - lastExp;
                        totalExp = int.Parse(kvp.Value);
                        break;
                    }
                }
                if (_exp >= totalNeed)
                {
                    level = maxLevel;
                    int lastLevel = level - 1;
                    if (lastLevel == 0)
                    {
                        currentExp = 0;
                    }
                    else
                    {
                        currentExp = int.Parse(dataDict[lastLevel.ToString()]);
                    }
                    totalExp = currentExp;
                }
                UpdateAttributeData();
            }
        }
    }

    public class WingAttributeData
    {
        public int attriId;
        public int value;
        public int maxValue;
    }

    public class WingSuitAttributeData
    {
        public WingType type;
        public int attriId;
        public int value;
        public string description;
        public int needCount;
        public int needLevel;
        public bool isActive;
    }

}
