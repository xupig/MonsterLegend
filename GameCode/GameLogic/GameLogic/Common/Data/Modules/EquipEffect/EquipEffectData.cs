﻿#region 模块信息
/*==========================================
// 文件名：EquipEffectData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.EquipEffect
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/14 11:04:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;

namespace Common.Data
{
    public class EffectData
    {
        public int flowId = 0;
        public int particleId = 0;
    }

    public class EquipEffectData
    {
        private Dictionary<int, EquipEffectItemData> _effectDict;
        /// <summary>
        /// 用于表示已经穿戴在身上的特效ID
        /// key : 装备外观配置表中的groupID
        /// Value : 该组已经穿戴在身上的外观特效
        /// </summary>
        private Dictionary<int, int> _equipedGroupEffect;
        private Dictionary<ACTEquipmentType, EffectData> _equipedEffectDict;

        public EquipEffectData()
        {
            _effectDict = new Dictionary<int, EquipEffectItemData>();
            _equipedGroupEffect = new Dictionary<int, int>();
            InitEquipedEffectDict();
            foreach (KeyValuePair<int, equip_facade> pair in XMLManager.equip_facade)
            {
                _effectDict.Add(pair.Key, new EquipEffectItemData(pair.Value));
            }
        }

        private void InitEquipedEffectDict()
        {
            _equipedEffectDict = new Dictionary<ACTEquipmentType, EffectData>();
            _equipedEffectDict.Add(ACTEquipmentType.Cloth, new EffectData());
            _equipedEffectDict.Add(ACTEquipmentType.Weapon, new EffectData());
        }

        public void Init(PbFacadeInfo pbInfo)
        {
            int facadeId = 0;
            // PbFacadeInfo 中的 facades 列表表示可以激活的特效列表
            for (int i = 0; i < pbInfo.facades.Count; i++)
            {
                facadeId = pbInfo.facades[i];
                if (_effectDict.ContainsKey(facadeId))
                {
                    _effectDict[facadeId].effectState = EffectState.Activated;
                }
            }
            // PbFacadeInfo 中的 enabled_facades 列表表示已经穿戴在身上的特效列表
            for (int i = 0; i < pbInfo.enabled_facades.Count; i++)
            {
                facadeId = pbInfo.enabled_facades[i];
                if (_effectDict.ContainsKey(facadeId))
                {
                    _effectDict[facadeId].effectState = EffectState.Equiped;
                    if (_equipedGroupEffect.ContainsKey(_effectDict[facadeId].groupId))
                    {
                        _equipedGroupEffect[_effectDict[facadeId].groupId] = facadeId;
                    }
                    else
                    {
                        _equipedGroupEffect.Add(_effectDict[facadeId].groupId, facadeId);
                    }
                    UpdateEquipedEffectDict(_effectDict[facadeId]);
                }
            }
            // 更新完穿戴在身上的特效列表之后,触发事件改变场景中的人物外形
            if (pbInfo.enabled_facades.Count > 0)
            {
                ChangeScenePlayerFacade();
            }
            for (int i = 0; i < pbInfo.not_activited.Count; i++)
            {
                facadeId = pbInfo.not_activited[i];
                if (_effectDict.ContainsKey(facadeId))
                {
                    _effectDict[facadeId].effectState = EffectState.CanActivate;
                }
            }
        }

        private void ChangeScenePlayerFacade()
        {
            EventDispatcher.TriggerEvent(FashionEvents.CHANGE_EQUIP_EFFECT);
        }

        private void UpdateEquipedEffectDict(EquipEffectItemData data, bool isTakeOff = false)
        {
            int curEquipWeaponId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipData.ID;
            int curEquipClothId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipData.ID;
            switch ((EffectGroup)data.groupId)
            {
                // 武器粒子
                case EffectGroup.WeaponParticle:
                    if (equip_effect_helper.GetParticleId(data.id, curEquipWeaponId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].particleId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].particleId = equip_effect_helper.GetParticleId(data.id, curEquipWeaponId);
                        }
                    }
                    if (equip_effect_helper.GetFlowId(data.id, curEquipWeaponId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].flowId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].flowId = equip_effect_helper.GetFlowId(data.id, curEquipWeaponId);
                        }
                    }
                    break;
                // 武器流光
                case EffectGroup.WeaponFlow:
                    if (equip_effect_helper.GetParticleId(data.id, curEquipWeaponId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].particleId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].particleId = equip_effect_helper.GetParticleId(data.id, curEquipWeaponId);
                        }
                    }
                    if (equip_effect_helper.GetFlowId(data.id, curEquipWeaponId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].flowId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Weapon].flowId = equip_effect_helper.GetFlowId(data.id, curEquipWeaponId);
                        }
                    }
                    break;
                // 护甲流光
                case EffectGroup.Armor:
                    if (equip_effect_helper.GetParticleId(data.id, curEquipClothId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Cloth].particleId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Cloth].particleId = equip_effect_helper.GetParticleId(data.id, curEquipClothId);
                        }
                    }
                    if (equip_effect_helper.GetFlowId(data.id, curEquipClothId) != 0)
                    {
                        if (isTakeOff)
                        {
                            _equipedEffectDict[ACTEquipmentType.Cloth].flowId = 0;
                        }
                        else
                        {
                            _equipedEffectDict[ACTEquipmentType.Cloth].flowId = equip_effect_helper.GetFlowId(data.id, curEquipClothId);
                        }
                    }
                    break;
            }
        }

        public EquipEffectItemData GetEquipEffectItemData(int effectId)
        {
            if (_effectDict.ContainsKey(effectId))
            {
                return _effectDict[effectId];
            }
            return null;
        }

        public void SetFacadeCanActivite(int effectId)
        {
            if (_effectDict.ContainsKey(effectId))
            {
                _effectDict[effectId].effectState = EffectState.CanActivate;
            }
        }

        public List<EquipEffectItemData> GetEquipEffectItemDataByGroup(int groupId)
        {
            List<int> equipEffectList = equip_facade_helper.GetFacadeListByGroup(groupId);
            if (equipEffectList == null || equipEffectList.Count == 0)
            {
                return null;
            }
            List<EquipEffectItemData> result = new List<EquipEffectItemData>();
            for (int i = 0; i < equipEffectList.Count; i++)
            {
                result.Add(_effectDict[equipEffectList[i]]);
            }
            UpdateEquipEffectState(result);
            return result;
        }

        private void UpdateEquipEffectState(List<EquipEffectItemData> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                EquipEffectItemData itemData = list[i];
                SingleAchievementData achievementData = PlayerDataManager.Instance.AchievementData.GetSingleAchievementData(itemData.achivementId);
                if (itemData.level == 1 && itemData.effectState == EffectState.Lock )
                {
                    itemData.effectState = EffectState.Processing;
                }
                if (achievementData.state == AchievementState.Processing
                    && IsGetEquipEffectFinished(list[i]))
                {
                    itemData.effectState = EffectState.Processing;
                }
            }
        }

        private bool IsGetEquipEffectFinished(EquipEffectItemData equipEffectItemData)
        {
            if (equipEffectItemData.level > 1)
            {
                int lastEffectId = equipEffectItemData.id - 1;
                return (int)GetEquipEffectItemData(lastEffectId).effectState >= (int)EffectState.CanActivate;
            }
            return true;
        }

        public void SetEffectActivited(int effectId)
        {
            if (_effectDict.ContainsKey(effectId))
            {
                _effectDict[effectId].effectState = EffectState.Activated;
            }
        }

        public void SetCurrentEffect(int effectId)
        {
            // 如果之前身上穿戴有特效，则将该之前特效状态清掉
            EquipEffectItemData data = _effectDict[effectId];
            if (_equipedGroupEffect.ContainsKey(data.groupId))
            {
                _effectDict[_equipedGroupEffect[data.groupId]].effectState = EffectState.Activated;
                _equipedGroupEffect[data.groupId] = effectId;
            }
            else
            {
                _equipedGroupEffect.Add(data.groupId, effectId);
            }
            _effectDict[effectId].effectState = EffectState.Equiped;
            UpdateEquipedEffectDict(_effectDict[effectId]);
            ChangeScenePlayerFacade();
        }

        public int GetCurrentEffectParticleID(ACTEquipmentType type)
        {
            if (_equipedEffectDict.ContainsKey(type) && PlayerDataManager.Instance.FashionData.GetCurrentFashionId() == 0)
            {
                return _equipedEffectDict[type].particleId;
            }
            return 0;
        }

        public int GetCurrentEffectFlowID(ACTEquipmentType type)
        {
            if (_equipedEffectDict.ContainsKey(type) && PlayerDataManager.Instance.FashionData.GetCurrentFashionId() == 0)
            {
                return _equipedEffectDict[type].flowId;
            }
            return 0;
        }

        public void TakeOffEffect(int effectId)
        {
            EquipEffectItemData effectData = _effectDict[effectId];
            effectData.effectState = EffectState.Activated;
            if (_equipedGroupEffect.ContainsKey(effectData.groupId))
            {
                _equipedGroupEffect.Remove(effectData.groupId);
            }
            UpdateEquipedEffectDict(effectData, true);
            ChangeScenePlayerFacade();
        }

        public bool HasCanActivateEffect()
        {
            foreach (var pair in _effectDict)
            {
                if (pair.Value.effectState == EffectState.CanActivate)
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasCanActivateEffectByGroup(int groupId)
        {
            foreach (var pair in _effectDict)
            {
                if (pair.Value.effectState == EffectState.CanActivate && pair.Value.groupId == groupId)
                {
                    return true;
                }
            }
            return false;
        }

        public Dictionary<int,int> GetEquipedEffectIdList()
        {
            return _equipedGroupEffect;
        }
    }
}
