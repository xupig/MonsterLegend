﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using System.Collections.Generic;

namespace Common.Data
{
    public class DuelPanelParameter : BasePanelParameter
    {
        public DuelData.View View;
        public object Data;

        public DuelPanelParameter(DuelData.View view, object data)
        {
            this.View = view;
            this.Data = data;
        }
    }

    public class DuelData
    {
        public enum View
        {
            search,
            fight,
            fightConfirm,
            stake,//与fight同一个资源
            stakeConfirm,
        }

        public enum State
        {
            requestFight,//发起决斗
            preparing,//准备阶段
            ready,//准备完毕
            stakePreparing,//下注 准备中
            stakeReady,//下注 开始中
            fighting,//下注 战斗中
        }

        public enum PlayerState
        {
            fight,//自己发起决斗
            responsingFight,//答复中
            preparing,//准备中
            prepared,//已准备
            unstake,//未下注
            staked,//已下注
            cannotStake,//不能下注
        }

        public class TimeConfig
        {
            public ulong SetStateTime;
            public int LeftSecond;
            public TimeConfig(){}

            public TimeConfig(int leftSecond)
            {
                this.LeftSecond = leftSecond;
            }
        }

        public class Player
        {
            public PlayerState State;
            public PbDuelTarget PbDuelTarget;
            public bool IsMe;
            public bool InitPlayerModel;

            public Player(PlayerState state)
            {
                this.State = state;
                this.InitPlayerModel = false;
            }

        }

        public uint InviteType; //0代表自己是发起方，1代表自己是受邀请方且不可拒绝（拒绝会扣分），2代表自己是受邀请方且可拒绝
        public State DuelState;
        public bool IsFightState
        {
            get
            {
                return DuelState == State.requestFight || DuelState == State.preparing || DuelState == State.ready;
            }
        }

        public bool IsStakeState
        {
            get
            {
                return DuelState == State.stakeReady || DuelState == State.stakePreparing || DuelState == State.fighting;
            }
        }

        public Dictionary<State, TimeConfig> _configDict = new Dictionary<State, TimeConfig>()
        {
            { State.requestFight, new TimeConfig(duel_data_helper.GetAcceptTime()) },
            { State.preparing, new TimeConfig(duel_data_helper.GetPrepareTime()) },
            { State.ready, new TimeConfig(3) },//3秒倒计时
            { State.stakePreparing, new TimeConfig() },
            { State.stakeReady, new TimeConfig() },
            { State.fighting, new TimeConfig() },
        };

        public const int INVALID_MATCH_ID = -1;
        public const int INVALID_BET_ID = 0;

        private Dictionary<int, PbDuelMatchBetChange> _matchRewardDict = new Dictionary<int, PbDuelMatchBetChange>();

        public Player LeftPlayer;
        public Player RightPlayer;
        public int CurrentMatchId;
        public bool IsLuckMode;
        public int BetId;

        public DuelData()
        {
            EventDispatcher.AddEventListener(MainUIEvents.ON_SHOW_MAIN_UI, OnShowMainUI);
        }

        private void OnShowMainUI()
        {
            //回到主界面清空数据
            _matchRewardDict.Clear();
        }

        public void FillInvite(PbDuelInviteTrigger pbDuelInviteTrigger)
        {
            InitFightData();
            InviteType = pbDuelInviteTrigger.type;
            switch (InviteType)
            {
                case 0:
                    LeftPlayer.PbDuelTarget = GetMyPbDuelTarget();
                    LeftPlayer.IsMe = true;
                    RightPlayer.PbDuelTarget = pbDuelInviteTrigger.target;
                    RightPlayer.IsMe = false;
                    break;
                case 1:
                case 2:
                    LeftPlayer.PbDuelTarget = pbDuelInviteTrigger.target;
                    LeftPlayer.IsMe = false;
                    RightPlayer.PbDuelTarget = GetMyPbDuelTarget();
                    RightPlayer.IsMe = true;
                    break;
            }
            SetDuelStateInfo(State.requestFight);
        }

        private void InitFightData()
        {
            LeftPlayer = new Player(PlayerState.fight);
            RightPlayer = new Player(PlayerState.responsingFight);
            CurrentMatchId = INVALID_MATCH_ID;
            IsLuckMode = false;
        }

        public int GetLeftSecond()
        {
            TimeConfig config = _configDict[DuelState];
            int leftSecond = config.LeftSecond - (int)((Global.Global.serverTimeStamp - config.SetStateTime) * 0.001);
            return leftSecond < 0 ? 0 : leftSecond;
        }

        public bool FillInviteResponse(PbDuelInviteResp pbDuelInviteResp)
        {
            if (pbDuelInviteResp.result == 0)
            {
                CurrentMatchId = (int)pbDuelInviteResp.match_id;
                IsLuckMode = pbDuelInviteResp.luck_mode == 1 ? true : false;
                LeftPlayer.State = PlayerState.preparing;
                RightPlayer.State = PlayerState.preparing;
                SetDuelStateInfo(State.preparing);
                return true;
            }
            return false;
        }

        public void FillReady(PbDuelReadyResp pbDuelReadyResp)
        {
            if (InviteType == 0)
            {
                if (pbDuelReadyResp.self_ready == 1)
                {
                    LeftPlayer.State = PlayerState.prepared;
                }
                if (pbDuelReadyResp.other_ready == 1)
                {
                    RightPlayer.State = PlayerState.prepared;
                }
            }
            else
            {
                if (pbDuelReadyResp.self_ready == 1)
                {
                    RightPlayer.State = PlayerState.prepared;
                }
                if (pbDuelReadyResp.other_ready == 1)
                {
                    LeftPlayer.State = PlayerState.prepared;
                }
            }

            if (LeftPlayer.State == PlayerState.prepared && RightPlayer.State == PlayerState.prepared)
            {
                SetDuelStateInfo(State.ready);
            }
        }

        public void FillStakeInfo(PbDuelMatchBetInfo pbDuelMatchBetInfo)
        {
            InitStakeData();
            LeftPlayer.PbDuelTarget = pbDuelMatchBetInfo.sponsor;
            RightPlayer.PbDuelTarget = pbDuelMatchBetInfo.invitee;
            FillStakeState(pbDuelMatchBetInfo.match_state);
        }

        private void InitStakeData()
        {
            LeftPlayer = new Player(PlayerState.unstake);
            RightPlayer = new Player(PlayerState.unstake);
            CurrentMatchId = INVALID_MATCH_ID;
            IsLuckMode = false;
            BetId = INVALID_BET_ID;
        }

        public void FillStakeState(PbDuelMatchStateInfo pbDuelMatchStateInfo)
        {
            PbDuelMatchStateInfo matchInfo = pbDuelMatchStateInfo;
            switch (matchInfo.match_progress)
            {
                case 0:
                    SetDuelStateInfo(State.stakePreparing, (int)matchInfo.remain_time);
                    break;
                case 1:
                    SetDuelStateInfo(State.stakeReady, (int)matchInfo.remain_time);
                    break;
                case 2:
                    SetDuelStateInfo(State.fighting, (int)matchInfo.remain_time);
                    break;
            }
            switch (matchInfo.bet_winner)
            {
                case 1:
                    LeftPlayer.State = PlayerState.staked;
                    RightPlayer.State = PlayerState.cannotStake;
                    break;
                case 2:
                    LeftPlayer.State = PlayerState.cannotStake;
                    RightPlayer.State = PlayerState.staked;
                    break;
            }
            CurrentMatchId = (int)pbDuelMatchStateInfo.match_id;
            IsLuckMode = pbDuelMatchStateInfo.luck_mode == 1;
            BetId = (int)pbDuelMatchStateInfo.bet_id;
        }

        public PbDuelMatchBetChange GetPbDuelMatchBetChange(int matchId)
        {
            if (_matchRewardDict.ContainsKey(matchId))
            {
                return _matchRewardDict[matchId];
            }
            return null;
        }

        public void FillDuelMatchBetChange(PbDuelMatchBetChange pbDuelMatchBetChange)
        {
            int matchId = (int)pbDuelMatchBetChange.match_id;
            if (_matchRewardDict.ContainsKey(matchId))
            {
                _matchRewardDict[matchId] = pbDuelMatchBetChange;
            }
            else
            {
                _matchRewardDict.Add(matchId, pbDuelMatchBetChange);
            }
        }

        private PbDuelTarget myPbDuelTarget;
        public PbDuelTarget GetMyPbDuelTarget()
        {
            PlayerAvatar player = PlayerAvatar.Player;
            if (myPbDuelTarget == null)
            {
                //静态不变的
                myPbDuelTarget = new PbDuelTarget();
                myPbDuelTarget.dbid = player.dbid;
                myPbDuelTarget.name = player.name;
                myPbDuelTarget.vocation = (uint)player.vocation;
            }
            myPbDuelTarget.fight_force = player.fight_force;
            myPbDuelTarget.guild_name = string.IsNullOrEmpty(player.guild_name) ? MogoLanguageUtil.GetContent(122090) : player.guild_name;
            myPbDuelTarget.level = player.level;
            myPbDuelTarget.rank_level = player.duel_rank_level;
            myPbDuelTarget.rank_show = player.duel_rank_show;
            return myPbDuelTarget;
        }

        private void SetDuelStateInfo(State duelState, int leftSecond = 0)
        {
            DuelState = duelState;
            if (IsFightState)
            {
                _configDict[duelState].SetStateTime = Global.Global.serverTimeStamp;
            }
            else if (IsStakeState)
            {
                _configDict[duelState].LeftSecond = leftSecond;
                _configDict[duelState].SetStateTime = Global.Global.serverTimeStamp;
            }
        }
    }
}
