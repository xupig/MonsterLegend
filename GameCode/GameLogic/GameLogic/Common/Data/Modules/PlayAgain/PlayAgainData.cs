﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;

using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TeamEntryPlayerItemData
    {
        public ulong Dbid;
        public uint EntityId;
        public uint Level;
        public string Name;
        public int Vocation;
        public bool IsAgree;
    }

    public class PlayAgainData
    {
        public List<TeamEntryPlayerItemData> DataList = new List<TeamEntryPlayerItemData>();
        public ulong EndTime;

        public List<TeamEntryPlayerItemData> InitDataList()
        {
            DataList.Clear();
            AddMe();
            AddTeammate();
            DataList.Sort(SortByDbid);
            return DataList;
        }

        private void AddMe()
        {
            TeamEntryPlayerItemData data = new TeamEntryPlayerItemData();
            data.Dbid = PlayerAvatar.Player.dbid;
            data.EntityId = PlayerAvatar.Player.id;
            data.Level = PlayerAvatar.Player.level;
            data.Name = PlayerAvatar.Player.name;
            data.Vocation = (int)PlayerAvatar.Player.vocation;
            data.IsAgree = false;
            DataList.Add(data);
        }

        private void AddTeammate()
        {
            Dictionary<UInt32, PbHpMember> dict = PlayerDataManager.Instance.TeamInstanceData.HpMemberDic;
            foreach (PbHpMember pbHpMember in dict.Values)
            {
                TeamEntryPlayerItemData data = new TeamEntryPlayerItemData();
                data.Dbid = pbHpMember.dbid;
                data.EntityId = pbHpMember.avatar_id;
                data.Level = pbHpMember.level;
                data.Vocation = (int)pbHpMember.vocation;
                data.Name = MogoProtoUtils.ParseByteArrToString(pbHpMember.name);
                data.IsAgree = false;
                DataList.Add(data);
            }
        }
         
        private int SortByDbid(TeamEntryPlayerItemData x, TeamEntryPlayerItemData y)
        {
            //把队长放在第一位
            UInt64 captainId = PlayerDataManager.Instance.TeamData.CaptainId;
            if (captainId == x.Dbid && captainId != y.Dbid)
            {
                return -1;
            }
            else if (captainId != x.Dbid && captainId != y.Dbid)
            {
                if (x.Dbid < y.Dbid)
                {
                    return -1;
                }
            }
            return 1; 
        }

        public void OnPlayAgainError(error_code error, uint entityId)
        {
            string content = string.Empty;
            string name = GetName(entityId);
            if (error == error_code.ERR_MISSION_PLAY_AGAIN_REJECT)
            {
                if (entityId == PlayerAvatar.Player.id)
                {
                    GameSceneManager.GetInstance().LeaveCombatScene();
                    return;
                }
                content = MogoLanguageUtil.GetContent(122112);
            }
            else if (error == error_code.ERR_MISSION_ONE_PLAYER_CNT_TO_LIMIT)
            {
                content = MogoLanguageUtil.GetContent(122113);
            }
            else
            {
                GameSceneManager.GetInstance().LeaveCombatScene();
            }
            content = string.Format(content, name);
            if (BaseModule.IsPanelShowing(PanelIdEnum.PlayAgain))
            {
                //ariMessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnLevelCombatScene, true, false);
            }
            else
            {
                //ariToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.PlayAgain);
                TimerHeap.AddTimer(1000, 0, OnLevelCombatScene);
            }
        }

        private void OnLevelCombatScene()
        {
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private string GetName(uint entityId)
        {
            List<PbHpMember> list = PlayerDataManager.Instance.TeamInstanceData.HpMemberList;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].avatar_id == entityId)
                {
                    return MogoProtoUtils.ParseByteArrToString(list[i].name);
                }
            }
            return string.Empty;
        }

        public void FillPlayAgainData(uint errorId, byte[] byteArr)
        {
            PbPlayAgain pbPlayAgain = MogoProtoUtils.ParseProto<PbPlayAgain>(byteArr);
            if ((error_code)errorId == error_code.ERR_SUCCESSFUL)
            {
                EndTime = Global.Global.serverTimeStamp + pbPlayAgain.left_time * 1000;
                InitDataList();
                UIManager.Instance.ShowPanel(PanelIdEnum.PlayAgain);
            }
            else
            {
                OnPlayAgainError((error_code)errorId, pbPlayAgain.ent_id);
            }
        }

        public void FillPlayAgainAgreeData(PbPlayAgainNotify pb)
        {
            if (pb.flag == 0)
            {
                OnPlayAgainError(error_code.ERR_MISSION_PLAY_AGAIN_REJECT, pb.ent_id);
            }
            else
            {
                for (int i = 0; i < DataList.Count; i++)
                {
                    TeamEntryPlayerItemData item = DataList[i];
                    if (item.EntityId == pb.ent_id)
                    {
                        item.IsAgree = true;
                    }
                }
                EventDispatcher.TriggerEvent(CopyEvents.PLAY_AGAIN_AGREE);
            }
        }
    }
}
