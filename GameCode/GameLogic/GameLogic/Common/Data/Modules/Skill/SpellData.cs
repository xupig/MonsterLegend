﻿#region 模块信息
/*==========================================
// 文件名：SkillData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Skill
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 11:30:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class SpellData
    {
        private Dictionary<int,Dictionary<int,int>> _learnedSpellDict;
        private Dictionary<int, int> _spellSlotDict;
        private Dictionary<int, Dictionary<int, List<int>>> _allLearnedSpellDict;
        //private Dictionary<int, int> _levelDict;
        //private HashSet<int> _proficientDict;

        public bool HasInitialData = false;

        private int _isSkillProficientUnLock = -1;
        public int isSkillProficientUnLock
        {
            get { return _isSkillProficientUnLock; }
            set 
            { 
                _isSkillProficientUnLock = value;
                //player_helper.CheckInformationCanOperation();
                EventDispatcher.TriggerEvent(SpellEvents.SPELL_OPERATION_CHANGE);
            }
        }

        public Dictionary<int, bool> hasSkillCanUpgradeDict;

        public Dictionary<int, List<int>> canUpgradeSkillDict;

        public SpellData()
        {
            hasSkillCanUpgradeDict = new Dictionary<int, bool>();
            _learnedSpellDict = new Dictionary<int, Dictionary<int, int>>();
            _allLearnedSpellDict = new Dictionary<int,Dictionary<int,List<int>>>();
            _spellSlotDict = new Dictionary<int, int>();
            canUpgradeSkillDict = new Dictionary<int, List<int>>();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnPlayerLevelChange);
           // _levelDict = new Dictionary<int, int>();
           // _proficientDict = new HashSet<int>();
        }

        private void OnPlayerLevelChange()
        {
            player_helper.CheckLevel();
        }

        public bool HasProficient(int group)
        {
            return PlayerAvatar.Player.spell_proficient==group;
        }

        public int GetCurrentProficient()
        {
            return PlayerAvatar.Player.spell_proficient;
        }

        //public void OnSpellProficientChange(List<PbProficientIdInfo> dataList)
        //{
        //    _proficientDict.Clear();
        //    for (int i = 0; i<dataList.Count;i++ )
        //    {
        //        _proficientDict.Add((int)dataList[i].proficient_id);
        //    }
        //    EventDispatcher.TriggerEvent(SpellEvents.SPELL_PROFICIENT_CHANGE);
        //}

        public Dictionary<int,Dictionary<int,int>> SpellDict
        {
            get { return _learnedSpellDict; }
        }

        public Dictionary<int,int> SpellSlotDict
        {
            get { return _spellSlotDict; }
        }

        //public int GetPositionLevel(int position)
        //{
        //    if (_levelDict.ContainsKey(position))
        //    {
        //        return _levelDict[position];
        //    }
        //    return 0;
        //}

        public bool HasSkillLearned(int sysId)
        {
            spell_sys sys = skill_helper.GetSpellSysy(sysId);
            if(sys.__pos == 1)
            {
                return true;
            }
            return HasPositionLearned(sys.__proficient_group,sys.__position);
        }

        private bool HasPositionLearned(int group,int position)
        {
            if (_learnedSpellDict.ContainsKey(group) && _learnedSpellDict[group].ContainsKey(position))
            {
                return true;
            }
            return false;
        }

        public spell GetPositionLearned(int group,int position)
        {
            if (_learnedSpellDict.ContainsKey(group) && _learnedSpellDict[group].ContainsKey(position))
            {
                return spell_helper.GetSkill(_learnedSpellDict[group][position]);
            }
            return null;
        }

        public spell GetSpell(int spellId)
        {
            spell spell = spell_helper.GetSkill(spellId);
            if(spell==null || spell.__pos == 1)
            {
                return spell;
            }
            spell_sys spellSys = skill_helper.GetSpellSysyByGroupId(spell.__group);

            if (spellSys == null)
            {
                return null;
            }
            return GetPositionSpell(spellSys);
        }

        public spell GetPositionSpell(spell_sys spellSys)
        {
            if (_learnedSpellDict.ContainsKey(spellSys.__proficient_group) && _learnedSpellDict[spellSys.__proficient_group].ContainsKey(spellSys.__position))
            {
                spell spell = spell_helper.GetSkill(_learnedSpellDict[spellSys.__proficient_group][spellSys.__position]);
                if (PlayerAvatar.Player.spell_proficient != spellSys.__proficient_group)
                 {
                     return spell;
                 }
                 else
                 {
                     return skill_helper.GetSpell(spellSys.__after_proficient, spell.__sp_level);
                 }
            }
            else
            {
                if (PlayerAvatar.Player.spell_proficient != spellSys.__proficient_group)
                {
                    return skill_helper.GetSpell(spellSys.__before_proficient, 1);
                }
                else
                {
                    return skill_helper.GetSpell(spellSys.__after_proficient, 1);
                }
            }
        }

        public List<uint> OriginalLeanedSkillList = new List<uint>();
        public void InitLeanedSkill(List<PbLearnedSpellsInfo> spellInfoList)
        {
            OriginalLeanedSkillList.Clear();
            HasInitialData = true;
            for(int i=0;i<spellInfoList.Count;i++)
            {
                PbLearnedSpellsInfo info = spellInfoList[i];
                UpdateLearnSkill(info,false);
                OriginalLeanedSkillList.Add(info.spell_id);
            }
            
            Dictionary<int,Dictionary<int, spell_sys>> dataDict = skill_helper.GetPlayerAllSpellList();
            foreach(KeyValuePair<int,Dictionary<int, spell_sys>> kvp in dataDict)
            {
                foreach(KeyValuePair<int,spell_sys> kk in kvp.Value)
                {
                    if (_learnedSpellDict.ContainsKey(kk.Value.__proficient_group) == false || _learnedSpellDict[kk.Value.__proficient_group].ContainsKey(kk.Value.__position) == false)
                    {
                        spell spell = skill_helper.GetSpell(kk.Value.__before_proficient, 1);
                        if (canUpgradeSkillDict.ContainsKey(kk.Value.__proficient_group) == false)
                        {
                            canUpgradeSkillDict.Add(kk.Value.__proficient_group,new List<int>());
                        }
                        canUpgradeSkillDict[kk.Value.__proficient_group].Add(spell.__id);
                    }
                }
            }
            UpdatePlayerSkill();
            player_helper.CheckSkill();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient, UpdatePlayerSkill);
        }

        public int UpdateLearnSkill(PbLearnedSpellsInfo spellInfo,bool isUpdate = true)
        {
            spell spell = spell_helper.GetSkill((int)spellInfo.spell_id);
            spell_sys spellSys = skill_helper.GetSpellSysyByGroupId(spell.__group);
            if(spellSys.__position ==0)
            {
                return spellSys.__position;
            }
            
            spell nextLevel = skill_helper.GetSpell(spell.__group, spell.__sp_level + 1);
            if(canUpgradeSkillDict.ContainsKey(spellSys.__proficient_group))
            {
                canUpgradeSkillDict[spellSys.__proficient_group].Remove(spell.__id);
            }
            if (nextLevel != null)
            {
                if (canUpgradeSkillDict.ContainsKey(spellSys.__proficient_group) == false)
                {
                    canUpgradeSkillDict.Add(spellSys.__proficient_group, new List<int>());
                }
                if (canUpgradeSkillDict[spellSys.__proficient_group].Contains(nextLevel.__id) == false)
                {
                    canUpgradeSkillDict[spellSys.__proficient_group].Add(nextLevel.__id);
                }
            }
            if (_learnedSpellDict.ContainsKey(spellSys.__proficient_group) == false)
            {
                _learnedSpellDict.Add(spellSys.__proficient_group, new Dictionary<int, int>());
                _allLearnedSpellDict.Add(spellSys.__proficient_group, new Dictionary<int, List<int>>());
            }
            if (_learnedSpellDict[spellSys.__proficient_group].ContainsKey(spellSys.__position) == false)
            {
                _learnedSpellDict[spellSys.__proficient_group].Add(spellSys.__position, (int)spellInfo.spell_id);
                _allLearnedSpellDict[spellSys.__proficient_group].Add(spellSys.__position, new List<int>());
            }
            else
            {
                _learnedSpellDict[spellSys.__proficient_group][spellSys.__position] = (int)spellInfo.spell_id;
            }
            _allLearnedSpellDict[spellSys.__proficient_group][spellSys.__position].Clear();
            
            if(spell.__pos == 1)
            {
                List<int> result = skill_helper.GetVocationPosSpellListByLevel(PlayerAvatar.Player.vocation.ToInt(), spell.__sp_level);
                if(result!=null)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        if (_allLearnedSpellDict.ContainsKey(i) && _allLearnedSpellDict[i].ContainsKey(spellSys.__position))
                        {
                            _allLearnedSpellDict[i][spellSys.__position].Clear();
                        }
                    }
                    for(int i=0;i<result.Count;i++)
                    {
                        spell otherNormalSpell = spell_helper.GetSkill(result[i]);
                        spell_sys otherNormalSpellSys = skill_helper.GetSpellSysyByGroupId(otherNormalSpell.__group);
                        if(otherNormalSpellSys.__position!=0)
                        {
                            spell nextOtherNormalSpell = skill_helper.GetSpell(otherNormalSpell.__group, otherNormalSpell.__sp_level + 1);
                            if (canUpgradeSkillDict.ContainsKey(otherNormalSpellSys.__proficient_group))
                            {
                                canUpgradeSkillDict[otherNormalSpellSys.__proficient_group].Remove(otherNormalSpell.__id);
                            }
                            if (nextOtherNormalSpell != null)
                            {
                                if (canUpgradeSkillDict.ContainsKey(otherNormalSpellSys.__proficient_group) == false)
                                {
                                    canUpgradeSkillDict.Add(otherNormalSpellSys.__proficient_group, new List<int>());
                                }
                                if (canUpgradeSkillDict[otherNormalSpellSys.__proficient_group].Contains(nextOtherNormalSpell.__id) == false)
                                {
                                    canUpgradeSkillDict[otherNormalSpellSys.__proficient_group].Add(nextOtherNormalSpell.__id);
                                }
                                
                            }

                            if (_learnedSpellDict.ContainsKey(otherNormalSpellSys.__proficient_group) == false)
                            {
                                _learnedSpellDict.Add(otherNormalSpellSys.__proficient_group, new Dictionary<int, int>());
                            }
                            if (_learnedSpellDict[otherNormalSpellSys.__proficient_group].ContainsKey(otherNormalSpellSys.__position) == false)
                            {
                                _learnedSpellDict[otherNormalSpellSys.__proficient_group].Add(otherNormalSpellSys.__position, otherNormalSpell.__id);
                            }
                            else
                            {
                                _learnedSpellDict[otherNormalSpellSys.__proficient_group][otherNormalSpellSys.__position] = otherNormalSpell.__id;
                            }
                        }

                        if (_allLearnedSpellDict.ContainsKey(otherNormalSpellSys.__proficient_group) == false)
                        {
                            _allLearnedSpellDict.Add(otherNormalSpellSys.__proficient_group, new Dictionary<int, List<int>>());
                        }
                        if (_allLearnedSpellDict[otherNormalSpellSys.__proficient_group].ContainsKey(spellSys.__position) == false)
                        {
                            _allLearnedSpellDict[otherNormalSpellSys.__proficient_group].Add(spellSys.__position, new List<int>());
                        }
                        _allLearnedSpellDict[otherNormalSpellSys.__proficient_group][spellSys.__position].Add(result[i]);
                       
                    }
                }
            }
            else
            {
                _allLearnedSpellDict[spellSys.__proficient_group][spellSys.__position].Add((int)spellInfo.spell_id);
            }
            

            if (_spellSlotDict.ContainsKey(spell.__pos))
            {
                spell prevSpell = spell_helper.GetSkill(_spellSlotDict[spell.__pos]);
                if (skill_helper.GetSpell(prevSpell.__group, prevSpell.__sp_level) == spell)
                {
                    _spellSlotDict[spell.__pos] = (int)spellInfo.spell_id;
                }
            }
            
            if(isUpdate)
            {
                UpdatePlayerSkill();
                player_helper.CheckSkill();
            }
            return spellSys.__position;
        }

        public void UpdateSlotSkills(List<PbSpellSlotInfo> slotList)
        {
            for(int i=0;i<slotList.Count;i++)
            {
                PbSpellSlotInfo info = slotList[i];
               
                if (_spellSlotDict.ContainsKey((int)info.pos) == false)
                {
                    _spellSlotDict.Add((int)info.pos, (int)info.spell_id);
                }
                else
                {
                    _spellSlotDict[(int)info.pos] = (int)info.spell_id;
                }
            }
            UpdatePlayerSkill();
        }

        

        public void UpdatePlayerSkill()
        {
            int spellProficient = PlayerAvatar.Player.spell_proficient;
            if (_allLearnedSpellDict.Count > 0)
            {
                List<int> learnedSkillList = new List<int>();
                foreach (KeyValuePair<int, Dictionary<int, List<int>>> kvp in _allLearnedSpellDict)
                {
                    Dictionary<int, List<int>> dict = kvp.Value;
                    foreach(KeyValuePair<int, List<int>> kk in dict)
                    {
                        List<int> list = kk.Value;
                        if (list != null)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                spell spell = GetSpell(list[i]);
                                spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
                                if (spell.__pos == 1)
                                {
                                    if (sys.__proficient_group == spellProficient || (spellProficient == 0 && sys.__proficient_group == 1))
                                    {
                                        learnedSkillList.Add(spell.__id);
                                    }
                                }
                                else
                                {
                                    if (_spellSlotDict.ContainsKey(spell.__pos) && _spellSlotDict[spell.__pos] == list[i])
                                    {
                                        learnedSkillList.Add(spell.__id);
                                        spell normalSpell = spell_helper.GetSkill(list[i]);
                                        spell seniorNormalSpell = skill_helper.GetSeniorSpell(normalSpell);
                                        if (seniorNormalSpell != null && _learnedSpellDict[sys.__proficient_group].ContainsKey(kk.Key + 1))
                                        {
                                            spell seniorSpell = skill_helper.GetSeniorSpell(spell);
                                            learnedSkillList.Add(seniorSpell.__id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                PlayerSkillManager.GetInstance().SetLearnedSkillList(learnedSkillList);     
            }
        }

        public void EquipSkill(PbSpellSlotInfo info)
        {
            if (_spellSlotDict.ContainsKey((int)info.pos) == false)
            {
                _spellSlotDict.Add((int)info.pos, (int)info.spell_id);
            }
            else
            {
                _spellSlotDict[(int)info.pos] = (int)info.spell_id;
            }
            UpdatePlayerSkill();
        }

        public spell GetSlotSpell(int slotId)
        {
            if(_spellSlotDict.ContainsKey(slotId))
            {
                return GetSpell(_spellSlotDict[slotId]);
            }
            return null;
        }

        public bool IsSlotFull(int slotId)
        {
            return _spellSlotDict.ContainsKey(slotId) && _spellSlotDict[slotId]!=0;
        }

    }
}
