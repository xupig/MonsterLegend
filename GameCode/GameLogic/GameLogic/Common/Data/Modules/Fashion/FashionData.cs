﻿using ACTSystem;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;

namespace Common.Data
{
    public class FashionData
    {
        /// <summary>
        /// 时装数据，包含所有显示需要的数据， Key为时装ID
        /// </summary>
        private Dictionary<int, FashionItemData> _fashionItemDataDic;

        /// <summary>
        /// 玩家当前穿的时装Id
        /// </summary>
        private int _currentFashionId;
        public int CurrentFashionId
        {
            get
            {
                return _currentFashionId;
            }
        }

        /// <summary>
        /// 新时装组别
        /// </summary>
        private int _newGroupId;

        public FashionData()
        {
            _fashionItemDataDic = new Dictionary<int, FashionItemData>();
        }

        public FashionItemData GetFashionItemData(int fashionId)
        {
            if (_fashionItemDataDic != null && _fashionItemDataDic.ContainsKey(fashionId))
            {
                return _fashionItemDataDic[fashionId];
            }
            return null;
        }

        public bool HaveFashion(int id)
        {
            FashionItemData itemData = GetFashionItemData(id);
            if (itemData == null)
            {
                return false;
            }
            return itemData.state != FashionState.NotHave;
        }

        public bool IsFashionPermanent(int id)
        {
            FashionItemData itemData = GetFashionItemData(id);
            return HaveFashion(id) && itemData.ValidTime == 0;
        }


        public void Fill(PbFashionList pbFashionList)
        {
            InitData();
            foreach (PbFashionInfo fashionInfo in pbFashionList.fashion_info)
            {
                FashionItemRenewal(fashionInfo.fashion_id, fashionInfo.valid_time);
            }
        }

        private bool _initConfig = false;
        private void InitData()
        {
            if (_initConfig == true)
            {
                return;
            }
            _initConfig = true;
            foreach (int id in XMLManager.fashion.Keys)
            {
                FashionItemData data = new FashionItemData(id);
                _fashionItemDataDic.Add(id, data);

                if (data.GorupId > _newGroupId)
                {
                    _newGroupId = data.GorupId;
                }
            }
            foreach (FashionItemData data in _fashionItemDataDic.Values)
            {
                if (data.GorupId == _newGroupId)
                {
                    SetNew(data.Id);
                }
                data.FightForce = fight_force_helper.GetFightForce(data.EffectId, PlayerAvatar.Player.vocation);
            }
        }

        /// <summary>
        /// 获得时装数据列表
        /// </summary>
        /// <returns></returns>
        public List<FashionItemData> GetFashionInfoList()
        {

            List<FashionItemData> result = new List<FashionItemData>();
            foreach (KeyValuePair<int, FashionItemData> pair in _fashionItemDataDic)
            {
                if (pair.Value.state == FashionState.Equiped || pair.Value.state == FashionState.NotEquiped)
                {
                    result.Add(pair.Value);
                }
                else
                {
                    if (pair.Value.HideType != 1)
                    {
                        result.Add(pair.Value);
                    }
                }
            }
            return result;
        }

        public int TotalFightForce
        {
            get { return totalFightForce; }
        }

        private int totalFightForce;
        public Dictionary<int, int> GetTotalFashionAttributes()
        {
            totalFightForce = 0;
            Dictionary<int,int> result = new Dictionary<int,int>();
            foreach (var pair in _fashionItemDataDic)
            {
                FashionItemData data = pair.Value;
                if (data.state != FashionState.NotHave)
                {
                    totalFightForce += pair.Value.FightForce;
                    int effectId = data.EffectId;
                    List<int> attriIdList = attri_effect_helper.GetAttributeIdList(effectId);
                    List<int> attriValueList = attri_effect_helper.GetAttributeValueList(effectId);
                    for (int i = 0; i < attriIdList.Count; i++)
                    {
                        if (result.ContainsKey(attriIdList[i]) == false)
                        {
                            result.Add(attriIdList[i], 0);
                        }
                        result[attriIdList[i]] += attriValueList[i];
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 设置当前穿的时装
        /// </summary>
        /// <param name="fashionId"></param>
        public void SetCurrentFashionId(int fashionId)
        {
            SetEquip(_currentFashionId, false);
            _currentFashionId = fashionId;
            SetEquip(_currentFashionId, true);
        }

        public int GetCurrentFashionId()
        {
            return _currentFashionId;
        }

        private void SetEquip(int fashionId, bool isEquip)
        {
            if (fashionId != 0 && _fashionItemDataDic.ContainsKey(fashionId))
            {
                if (isEquip == true)
                {
                    _fashionItemDataDic[fashionId].state = FashionState.Equiped;
                }
                else
                {
                    _fashionItemDataDic[fashionId].state = FashionState.NotEquiped;
                }
            }
        }

        private AvatarEquipInfo GetWeaponInfo(FashionItemData data)
        {
            AvatarEquipInfo weaponData = new AvatarEquipInfo();
            weaponData.equipID = data.WeaponId;
            return weaponData;
        }

        private AvatarEquipInfo GetClothInfo(FashionItemData data)
        {
            AvatarEquipInfo clothData = new AvatarEquipInfo();
            clothData.equipID = data.WeaponId;
            return clothData;
        }

        /// <summary>
        /// 设置最大的组别
        /// </summary>
        /// <param name="maxGroupId"></param>
        public void SetNew(int fashionId)
        {
            if (_fashionItemDataDic.ContainsKey(fashionId))
            {
                _fashionItemDataDic[fashionId].IsNew = true;
            }
        }

        /// <summary>
        /// 时装过期
        /// </summary>
        /// <param name="fashionId"></param>
        public void FashionItemExpire(int fashionId)
        {
            if (_fashionItemDataDic.ContainsKey(fashionId))
            {
                _fashionItemDataDic[fashionId].state = FashionState.NotHave;
                _fashionItemDataDic[fashionId].ValidTime = 0;
            }
        }

        /// <summary>
        /// 时装续期，购买时装也用这个接口
        /// </summary>
        public void FashionItemRenewal(int fashionId, uint validTime)
        {
            if (_fashionItemDataDic.ContainsKey(fashionId))
            {
                _fashionItemDataDic[fashionId].ValidTime = validTime;
                if (_fashionItemDataDic[fashionId].state == FashionState.NotHave)
                {
                    _fashionItemDataDic[fashionId].state = FashionState.NotEquiped;
                }
            }
        }

        public int GetEquipId(ACTEquipmentType type)
        {
            switch (type)
            {
                case ACTEquipmentType.Cloth:
                    if (_currentFashionId != 0 && _fashionItemDataDic.ContainsKey(_currentFashionId))
                    {
                        return _fashionItemDataDic[_currentFashionId].ClothId;
                    }
                    return 0;
                case ACTEquipmentType.Weapon:
                    if (_currentFashionId != 0 && _fashionItemDataDic.ContainsKey(_currentFashionId))
                    {
                        return _fashionItemDataDic[_currentFashionId].WeaponId;
                    }
                    return 0;
                default:
                    return 0;
            }
        }
    }
}
