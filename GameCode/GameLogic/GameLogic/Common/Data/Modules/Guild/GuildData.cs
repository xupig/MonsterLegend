using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace Common.Data
{
    public class GuildData
    {
        private MyGuildData _myGuildData;
        private List<ulong> _guildApplyJoinList;
        private PbGuildMemberInfoList _pbGuildMemberInfoList;
        private PbGuildApplyJoinInfoList _pbGuildApplyJoinInfoList;
        private PbGuildNoticeList _pbGuildNoticeList;
        private List<PbGuildInviteJoin> _pbGuildInviteJoin;
        private List<string> _guildRecordInfoList;

        /// <summary>
        /// 公会成就列表，key为成就id，value为成就数据
        /// </summary>
        private Dictionary<int, GuildAchievementData> _guildAchievemmentDataDict = new Dictionary<int, GuildAchievementData>();
        /// <summary>
        /// 公会技能列表，key为技能组id，value为技能数据
        /// </summary>
        private Dictionary<int, GuildSkillData> _guildSkillDataDict = new Dictionary<int, GuildSkillData>();

        private GuildSkillEffectData _effectData;
        public GuildSkillEffectData EffectData
        {
            get
            {
                return _effectData;
            }
        }

        private bool _hasNewNotice = false;
        public bool HasNewNotice
        {
            get
            {
                return _hasNewNotice;
            }
            set
            {
                _hasNewNotice = value;
                RefreshGuildPoint();
            }
        }

        public GuildData()
        {
            _myGuildData = new MyGuildData();
            _guildApplyJoinList = new List<ulong>();
            _pbGuildMemberInfoList = new PbGuildMemberInfoList();
            _pbGuildApplyJoinInfoList = new PbGuildApplyJoinInfoList();
            _pbGuildNoticeList = new PbGuildNoticeList();
            _pbGuildInviteJoin = new List<PbGuildInviteJoin>();
            _guildRecordInfoList = new List<string>();
            _effectData = new GuildSkillEffectData();
            InitPriorityMap();
        }


        public void Clear()
        {
            _myGuildData = new MyGuildData();
            _effectData = new GuildSkillEffectData();
            _guildApplyJoinList.Clear();
            _pbGuildMemberInfoList.member_info.Clear();
            _pbGuildApplyJoinInfoList.apply_join_info.Clear();
            _pbGuildNoticeList.notice_info.Clear();
            _guildRecordInfoList.Clear();
            _guildAchievemmentDataDict.Clear();
            _guildSkillDataDict.Clear();
            HasNewNotice = false;
            RefreshGuildPoint();
        }

        public MyGuildData MyGuildData
        {
            get
            {
                return _myGuildData;
            }
        }

        public void AddInviteInfo(PbGuildInviteJoin pbInviteInfo)
        {
            string content = MogoLanguageUtil.GetContent(74887);
            pbInviteInfo.name = MogoProtoUtils.ParseByteArrToString(pbInviteInfo.name_bytes);
            pbInviteInfo.guild_name = MogoProtoUtils.ParseByteArrToString(pbInviteInfo.guild_name_bytes);

            for (int i = 0; i < _pbGuildInviteJoin.Count; i++)
            {
                if (_pbGuildInviteJoin[i].dbid == pbInviteInfo.dbid)
                {
                    _pbGuildInviteJoin[i] = pbInviteInfo;
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Invite);
                    return;
                }
            }
            _pbGuildInviteJoin.Add(pbInviteInfo);

            FastNoticeData data = new FastNoticeData(content, PanelIdEnum.GuildInvite);
            PlayerDataManager.Instance.NoticeData.AddNoticeData(data);
            EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Invite);

        }

        public void RemoveInviteInfo(ulong dbid)
        {
            PbGuildInviteJoin inviteJoin = null;
            for (int i = 0; i < _pbGuildInviteJoin.Count; i++)
            {
                if (_pbGuildInviteJoin[i].dbid == dbid)
                {
                    inviteJoin = _pbGuildInviteJoin[i];
                }
            }
            if (inviteJoin != null)
            {
                _pbGuildInviteJoin.Remove(inviteJoin);
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Invite);
            }
        }

        public List<PbGuildInviteJoin> InviteJoinInfoList
        {
            get
            {
                return _pbGuildInviteJoin;
            }
        }

        public List<ulong> GetGuildApplyJoinList()
        {
            List<ulong> result = new List<ulong>();
            result.AddRange(_guildApplyJoinList);
            return result;
        }

        public void FillGuildApplyJoin(PbGuildApplyJoinList list)
        {
            _guildApplyJoinList.Clear();
            for (int i = 0; i < list.apply_join.Count; i++)
            {
                _guildApplyJoinList.Add(list.apply_join[i].guild_id);
            }
        }


        public PbGuildApplyJoinInfoList GuildApplyJoinInfoList
        {
            get
            {
                return _pbGuildApplyJoinInfoList;
            }
            set
            {
                _pbGuildApplyJoinInfoList = value;
                RefreshGuildPoint();
            }
        }

        public PbGuildMemberInfoList GuildMemberInfoList
        {
            get
            {
                return _pbGuildMemberInfoList;
            }
            set
            {
                _pbGuildMemberInfoList = value;
                for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
                {
                    _pbGuildMemberInfoList.member_info[i].name = MogoProtoUtils.ParseByteArrToString(_pbGuildMemberInfoList.member_info[i].name_bytes);
                }
            }
        }

        public bool IsGuildMember(UInt64 dbid)
        {
            for(int i=0;i<_pbGuildMemberInfoList.member_info.Count;i++)
            {
                if(_pbGuildMemberInfoList.member_info[i].dbid == dbid)
                {
                    return true;
                }
            }
            return false;
        }

        public List<PbGuildNoticeInfo> GetGuildNoticeList()
        {
            List<PbGuildNoticeInfo> result = new List<PbGuildNoticeInfo>();
            result.AddRange(_pbGuildNoticeList.notice_info);
            result.Reverse();
            return result;
        }

        public void FillGuildNoticeInfo(PbGuildNoticeInfo info)
        {
            _pbGuildNoticeList.notice_info.Add(info);
            HasNewNotice = true;
        }

        public void FillGuildNoticeList(PbGuildNoticeList list)
        {
            _pbGuildNoticeList = list;
            HasNewNotice = false;
        }

        public List<string> GetGuildRecordInfoList()
        {
            List<string> result = new List<string>();
            result.AddRange(_guildRecordInfoList);
            result.Reverse();
            return result;
        }

        public void AddGuildRecordToList(string content)
        {
            _guildRecordInfoList.Add(content);
        }

        public void UpdateGuildRecord(LuaTable luaTable)
        {
            _guildRecordInfoList.Clear();
            foreach (var item in luaTable)
            {
                if (luaTable.IsLuaTable(item.Key))
                {
                    LuaTable table = item.Value as LuaTable;
                    int systemInfoId = Convert.ToInt32(table["1"]);
                    LuaTable sysTable = table["2"] as LuaTable;
                    if (system_info_helper.HasConfig(systemInfoId) == false)
                    {
                        UnityEngine.Debug.LogError(string.Format("缺少ID为{0}的系统提示", systemInfoId));
                        return;
                    }
                    object[] objArr = new object[sysTable.Count];
                    sysTable.Values.CopyTo(objArr, 0);
                    string content = SystemInfoUtil.GenerateContent(systemInfoId, objArr);
                    AddGuildRecordToList(content);
                }
            }
        }

        public void FillGuildAchievementData(PbGuildAchievementList pbList)
        {
            InitAchievementData();
            foreach (PbGuildAchievementData info in pbList.achv_list)
            {
                UpdateGuildAchievement(info);
            }
        }

        public void UpdateGuildAchievement(PbGuildAchievementData info)
        {
            if (_guildAchievemmentDataDict.ContainsKey(info.achv_id))
            {
                GuildAchievementData data = _guildAchievemmentDataDict[info.achv_id];
                data.FillPbInfo(info);
                RefreshGuildPoint();
            }
        }

        private bool _initAchievementData = false;
        private void InitAchievementData()
        {
            if (_initAchievementData == true) return;
            _initAchievementData = true;
            foreach (guild_achievement value in XMLManager.guild_achievement.Values)
            {
                GuildAchievementData data = new GuildAchievementData(value.__id);
                _guildAchievemmentDataDict.Add(value.__id, data);
            }
        }

        public List<GuildAchievementData> GetAllAchievementList()
        {
            List<GuildAchievementData> result = new List<GuildAchievementData>();
            foreach (GuildAchievementData data in _guildAchievemmentDataDict.Values)
            {
                result.Add(data);
            }
            result.Sort(AchievementSort);
            return result;
        }

        public List<GuildAchievementData> GetNotReachAchievementList()
        {
            List<GuildAchievementData> result = new List<GuildAchievementData>();
            foreach (GuildAchievementData data in _guildAchievemmentDataDict.Values)
            {
                if (data.Status != public_config.ACHIEVEMENT_STATE_REWARDED)
                {
                    result.Add(data);
                }
            }
            result.Sort(AchievementSort);
            return result;
        }

        public List<GuildAchievementData> GetReachAchievementList()
        {
            List<GuildAchievementData> result = new List<GuildAchievementData>();
            foreach (GuildAchievementData data in _guildAchievemmentDataDict.Values)
            {
                if (data.Status == public_config.ACHIEVEMENT_STATE_REWARDED)
                {
                    result.Add(data);
                }
            }
            result.Sort(AchievementSort);
            return result;
        }

        private int AchievementSort(GuildAchievementData x, GuildAchievementData y)
        {
            if (x.Status != y.Status)
            {
                return priorityMap[x.Status] - priorityMap[y.Status];
            }
            return x.Order - y.Order;
        }

        private Dictionary<int, int> priorityMap;
        private void InitPriorityMap()
        {
            priorityMap = new Dictionary<int, int>();
            priorityMap.Add(2, 1);
            priorityMap.Add(1, 2);
            priorityMap.Add(0, 2);
            priorityMap.Add(3, 3);
        }

        public void AddApplyIdToList(ulong id)
        {
            for (int i = 0; i < _guildApplyJoinList.Count; i++)
            {
                if (id == _guildApplyJoinList[i])
                {
                    return;
                }
            }
            _guildApplyJoinList.Add(id);
        }

        public void CancelApplyIdToList(ulong id)
        {
            for (int i = _guildApplyJoinList.Count - 1; i >= 0; i--)
            {
                if (id == _guildApplyJoinList[i])
                {
                    _guildApplyJoinList.RemoveAt(i);
                }
            }
        }

        public void RemoveApplyId(ulong id)
        {
            for (int i = _pbGuildApplyJoinInfoList.apply_join_info.Count - 1; i >= 0; i--)
            {
                if (_pbGuildApplyJoinInfoList.apply_join_info[i].dbid == id)
                {
                    _pbGuildApplyJoinInfoList.apply_join_info.RemoveAt(i);
                }
            }
            RefreshGuildPoint();
        }

        public void ChangeMemberPosition(PbGuildAppointPosition pbAppoint)
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == pbAppoint.dbid)
                {
                    _pbGuildMemberInfoList.member_info[i].guild_position = pbAppoint.position;
                    if (pbAppoint.position == public_config.GUILD_POSITION_LEADER)
                    {
                        _myGuildData.LeaderName = MogoProtoUtils.ParseByteArrToString(_pbGuildMemberInfoList.member_info[i].name_bytes);
                    }
                    if (pbAppoint.dbid == PlayerAvatar.Player.dbid)
                    {
                        RefreshGuildPoint();
                    }
                }
            }
        }

        public void UpdateAssistAlchemy(ulong dbid, uint remainAssistCount)
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == dbid)
                {
                    _pbGuildMemberInfoList.member_info[i].remain_assist_cnt = remainAssistCount;
                }
            }
        }

        public void ChangeMemberMilitaryRank(PbGuildChangeMilitary pb)
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == pb.dbid)
                {
                    _pbGuildMemberInfoList.member_info[i].military = pb.military;
                    break;
                }
            }
        }

        public void ChangeMemberOnlineState(PbGuildStateNotice pbGuildStateNotice)
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == pbGuildStateNotice.dbid)
                {
                    _pbGuildMemberInfoList.member_info[i].online = pbGuildStateNotice.state;
                }
            }
        }

        public void RemoveMemberFromList(ulong id)
        {
            for (int i = _pbGuildMemberInfoList.member_info.Count - 1; i >= 0; i--)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == id)
                {
                    _pbGuildMemberInfoList.member_info.RemoveAt(i);
                }
            }
            _myGuildData.MemberCount--;
            RefreshGuildPoint();
        }

        public void AddApplyMemberIdToList(ulong id)
        {
            _myGuildData.MemberCount++;
            RefreshGuildPoint();
        }

        public void AddMemberInfoToList(PbGuildMemberInfo pbMemberInfo)
        {
            pbMemberInfo.name = MogoProtoUtils.ParseByteArrToString(pbMemberInfo.name_bytes);
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (_pbGuildMemberInfoList.member_info[i].dbid == pbMemberInfo.dbid)
                {
                    _pbGuildMemberInfoList.member_info[i] = pbMemberInfo;
                    return;
                }
            }
            _pbGuildMemberInfoList.member_info.Add(pbMemberInfo);
        }

        public List<PbGuildMemberInfo> GetMemberList()
        {
            List<PbGuildMemberInfo> result = new List<PbGuildMemberInfo>();
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                result.Add(_pbGuildMemberInfoList.member_info[i]);
            }
            return result;
        }

        private int MemberSort(PbGuildMemberInfo x, PbGuildMemberInfo y)
        {
            if (x.online == 1 && y.online != 1)
            {
                return -1;
            }
            else if (x.online != 1 && y.online == 1)
            {
                return 1;
            }
            if (x.dbid == PlayerAvatar.Player.dbid && y.dbid != PlayerAvatar.Player.dbid)
            {
                return -1;
            }
            if (x.dbid != PlayerAvatar.Player.dbid && y.dbid == PlayerAvatar.Player.dbid)
            {
                return 1;
            }
            if (x.guild_position != y.guild_position)
            {
                return (int)x.guild_position - (int)y.guild_position;
            }
            if (x.yesterday_contribution != y.yesterday_contribution)
            {
                return (int)y.yesterday_contribution - (int)x.yesterday_contribution;
            }
            return (int)y.dbid - (int)x.dbid;
        }


        public void AddApplyJoinInfoToList(PbGuildApplyJoinInfo joinInfo)
        {
            for (int i = 0; i < _pbGuildApplyJoinInfoList.apply_join_info.Count; i++)
            {
                if (_pbGuildApplyJoinInfoList.apply_join_info[i].dbid == joinInfo.dbid)
                {
                    return;
                }
            }
            _pbGuildApplyJoinInfoList.apply_join_info.Add(joinInfo);
            RefreshGuildPoint();
        }

        public void FillGuildSkillData(PbGuildSkillList pbSkillList)
        {
            _guildSkillDataDict.Clear();
            for (int i = 0; i < pbSkillList.skills.Count; i++)
            {
                GuildSkillData data = new GuildSkillData(pbSkillList.skills[i]);
                data.HasLearned = true;
                if (_guildSkillDataDict.ContainsKey(data.Group) == false)
                {
                    _guildSkillDataDict.Add(data.Group, data);
                }
            }
            foreach (guild_spell spell in XMLManager.guild_spell.Values)
            {
                if (_guildSkillDataDict.ContainsKey(spell.__group) == false && spell.__level == 1)
                {
                    GuildSkillData data = new GuildSkillData(spell.__id);
                    data.HasLearned = false;
                    _guildSkillDataDict.Add(data.Group, data);
                }
            }
            UpdateSkillEffect();
            RefreshGuildPoint();
        }

        public bool HasLearnSkill(int skillId)
        {
            int groupId = guild_spell_helper.GetGuildSpellGroup(skillId);
            int level = guild_spell_helper.GetGuildSpellLevel(skillId);
            if (_guildSkillDataDict.ContainsKey(groupId))
            {
                if (_guildSkillDataDict[groupId].HasLearned == true && _guildSkillDataDict[groupId].Level >= level)
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdateSkill(int skillId)
        {
            GuildSkillData data = new GuildSkillData(skillId);
            data.HasLearned = true;
            _guildSkillDataDict[data.Group] = data;
            UpdateSkillEffect();
            RefreshGuildPoint();
        }

        public void UpdateSkillEffect()
        {
            foreach (KeyValuePair<int, GuildSkillData> kvp in _guildSkillDataDict)
            {
                if (kvp.Value.HasLearned == true)
                {
                    List<string> effect = guild_spell_helper.GetGuildEffect(kvp.Value.Id);
                    if (effect[0] == "1")
                    {
                        _effectData.MaintainFundReduce = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "2")
                    {
                        _effectData.GoldLimit = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "3")
                    {
                        _effectData.DonateGetFund = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "4")
                    {
                        _effectData.DonateGetContribute = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "5")
                    {
                        _effectData.DiamondLimit = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "6")
                    {
                        _effectData.BindDiamondLimit = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "501")
                    {
                        _effectData.AddDreamLandGoldPercent = int.Parse(effect[1]);
                    }
                    else if (effect[0] == "502")
                    {
                        _effectData.AddDreamLandExpPercent = int.Parse(effect[1]);
                    }

                }
            }
        }

        public List<GuildSkillData> GetSkillList()
        {
            List<GuildSkillData> result = new List<GuildSkillData>();
            result.AddRange(_guildSkillDataDict.Values);
            result.Sort(SkillSort);
            return result;
        }

        private int SkillSort(GuildSkillData x, GuildSkillData y)
        {
            if (x.HasLearned == true && y.HasLearned == false)
            {
                return -1;
            }
            else if (x.HasLearned == false && y.HasLearned == true)
            {
                return 1;
            }
            return x.Order - y.Order;
        }

        public PbGuildMemberInfo GetMyMemberInfo()
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (PlayerAvatar.Player.dbid == _pbGuildMemberInfoList.member_info[i].dbid)
                {
                    return _pbGuildMemberInfoList.member_info[i];
                }
            }
            return null;
        }

        public int GetMyPosition()
        {
            PbGuildMemberInfo myInfo = GetMyMemberInfo();
            if (myInfo != null)
            {
                return (int)myInfo.guild_position;
            }
            return 0;
        }

        public int GetMyContribution()
        {
            PbGuildMemberInfo myInfo = GetMyMemberInfo();
            if (myInfo != null)
            {
                return (int)myInfo.contribution;
            }
            return 0;
        }

        public int GetMyTodayContribution()
        {
            PbGuildMemberInfo myInfo = GetMyMemberInfo();
            if (myInfo != null)
            {
                return (int)myInfo.today_contribution;
            }
            return 0;
        }

        public void UpdateMyContribution(uint num, uint todayNum, uint historyNum, uint yesterdayNum)
        {
            for (int i = 0; i < _pbGuildMemberInfoList.member_info.Count; i++)
            {
                if (PlayerAvatar.Player.dbid == _pbGuildMemberInfoList.member_info[i].dbid)
                {
                    _pbGuildMemberInfoList.member_info[i].contribution = num;
                    _pbGuildMemberInfoList.member_info[i].today_contribution = todayNum;
                    _pbGuildMemberInfoList.member_info[i].history_contribution = historyNum;
                    _pbGuildMemberInfoList.member_info[i].yesterday_contribution = yesterdayNum;
                }
            }
        }

        public int GetMyTodayDonate(int itemId)
        {
            PbGuildMemberInfo myInfo = GetMyMemberInfo();
            if (myInfo != null)
            {
                List<PbGuildMemberContCount> countList = myInfo.today_contribution_count;
                for (int j = 0; j < countList.Count; j++)
                {
                    PbGuildMemberContCount count = countList[j];
                    if ((int)count.cond_id == itemId)
                    {
                        return (int)count.count;
                    }
                }
            }
            return 0;
        }

        public void UpdateMyTodayDonate(uint itemId, uint value)
        {
            PbGuildMemberInfo myInfo = GetMyMemberInfo();
            if (myInfo != null)
            {
                bool isHave = false;
                List<PbGuildMemberContCount> contList = myInfo.today_contribution_count;
                for (int j = 0; j < contList.Count; j++)
                {
                    PbGuildMemberContCount cont = contList[j];
                    if (cont.cond_id == itemId)
                    {
                        isHave = true;
                        cont.count += value;
                    }
                }
                if (isHave == false)
                {
                    PbGuildMemberContCount cont = new PbGuildMemberContCount();
                    cont.cond_id = itemId;
                    cont.count = value;
                    contList.Add(cont);
                }
            }
        }

        public void RefreshNotice()
        {
            EventDispatcher.TriggerEvent<PanelIdEnum>(FastNoticeEvents.REMOVE_FAST_NOTICE, PanelIdEnum.GuildInformation);
            if (guild_helper.HasApplyJoinInfo())
            {
                for (int i = 0; i < PlayerDataManager.Instance.GuildData.GuildApplyJoinInfoList.apply_join_info.Count; i++)
                {
                    SystemInfoManager.Instance.ShowFastNotice(30082);
                }
            }
        }

        public void RefreshGuildPoint()
        {
            bool isPoint = false;
            if (PlayerAvatar.Player.guild_id != 0)
            {
                if (HasNewNotice || guild_helper.HasApplyJoinInfo() || guild_helper.CanUpgrade() || guild_helper.CanReward() || guild_helper.CanLearnSkill())
                {
                    isPoint = true;
                }
            }
            if (isPoint)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.guild);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.guild);
            }
            EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Point);
        }
    }
}
