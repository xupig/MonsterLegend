﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class MyGuildData
    {
        private PbGuildInfo _pbGuildInfo = new PbGuildInfo();

        public void Fill(PbGuildInfo pbGuildInfo)
        {
            _pbGuildInfo = pbGuildInfo;
            _pbGuildInfo.leader_name = MogoProtoUtils.ParseByteArrToString(pbGuildInfo.leader_name_bytes);
            _pbGuildInfo.name = MogoProtoUtils.ParseByteArrToString(pbGuildInfo.guild_name_bytes);
            _pbGuildInfo.guild_notice = MogoProtoUtils.ParseByteArrToString(pbGuildInfo.guild_notice_bytes);
            PlayerDataManager.Instance.GuildData.RefreshGuildPoint();
        }

        public void UpdateGuildItem(int itemId, int num)
        {
            if (itemId == 9)
            {
                Money = num;
            }
        }

        public void UpdateGuildBooming(uint num)
        {
            Booming = (int)num;
        }

        public ulong Id
        {
            get
            {
                return _pbGuildInfo.guild_id;
            }
        }

        public int GuildWarRank
        {
            get
            {
                return Convert.ToInt32(_pbGuildInfo.prev_battle_rank);
            }
        }

        public string LeaderName
        {
            get
            {
                return _pbGuildInfo.leader_name;
            }
            set
            {
                _pbGuildInfo.leader_name = value;
            }
        }

        public int Rank
        {
            get
            {
                return (int)_pbGuildInfo.ranking;
            }
        }

        public uint Time
        {
            get
            {
                return _pbGuildInfo.build_time;
            }
        }

        public int Money
        {
            get
            {
                return (int)_pbGuildInfo.guild_money;
            }
            set
            {
                _pbGuildInfo.guild_money = (uint)value;
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Money);
                PlayerDataManager.Instance.GuildData.RefreshGuildPoint();
            }
        }

        public int Level
        {
            get
            {
                return (int)_pbGuildInfo.level;
            }
            set
            {
                _pbGuildInfo.level = (uint)value;
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Level);
                PlayerDataManager.Instance.GuildData.RefreshGuildPoint();
            }
        }

        public int MemberCount
        {
            get
            {
                return (int)_pbGuildInfo.member_num;
            }
            set
            {
                _pbGuildInfo.member_num = (uint)value;
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Member_Count);
                PlayerDataManager.Instance.GuildData.RefreshGuildPoint();
            }
        }

        public string GuildName
        {
            get
            {
                return _pbGuildInfo.name;
            }
        }

        public int Booming
        {
            get
            {
                return (int)_pbGuildInfo.booming;
            }
            set
            {
                _pbGuildInfo.booming = (uint)value;
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Level);
                PlayerDataManager.Instance.GuildData.RefreshGuildPoint();
            }
        }

        public uint DeleteTime
        {
            get
            {
                return _pbGuildInfo.delete_time;
            }
        }

        public string Manifesto
        {
            get
            {
                return _pbGuildInfo.guild_notice;
            }
            set
            {
                _pbGuildInfo.guild_notice = value;
            }
        }

        public int BadgeIconId
        {
            get
            {
                List<int> iconList = MogoStringUtils.Convert2List(global_params_helper.GetGlobalParam(87));
                return iconList[(int)_pbGuildInfo.badge_type];
            }
        }
    }
}
