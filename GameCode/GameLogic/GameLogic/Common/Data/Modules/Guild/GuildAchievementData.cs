﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class GuildAchievementData
    {
        private guild_achievement _baseConfig;
        private PbGuildAchievementData _pbInfo;

        public GuildAchievementData(int id)
        {
            Id = id;
            _baseConfig = XMLManager.guild_achievement[id];
        }

        public void FillPbInfo(PbGuildAchievementData pbInfo)
        {
            _pbInfo = pbInfo;
        }

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return MogoLanguageUtil.GetContent(_baseConfig.__name);
            }
        }

        public int Order
        {
            get
            {
                return _baseConfig.__order;
            }
        }

        public string Description
        {
            get
            {
                return MogoLanguageUtil.GetContent(_baseConfig.__desc);
            }
        }

        public int AchievePoint
        {
            get
            {
                return _baseConfig.__achievePoint;
            }
        }

        public Dictionary<string, string> Reward
        {
            get
            {
                return data_parse_helper.ParseMap(_baseConfig.__reward);
            }
        }

        public int Status
        {
            get
            {
                if (_pbInfo == null) return 0;
                return _pbInfo.achv_status;
            }
        }

        public float Progress
        {
            get
            {
                return GetProgress();
            }
        }

        public string ProgressContent
        {
            get
            {
                return GetProgressContent();
            }
        }

        private int GetCondsNumByCondsId(int condsId)
        {
            if (_pbInfo == null) return 0;
            for (int i = 0; i < _pbInfo.conds.Count; i++)
            {
                if (condsId == _pbInfo.conds[i].cond_id)
                {
                    return _pbInfo.conds[i].cond_num;
                }
            }
            return 0;
        }

        private int GetEventNumByCondsId(int condsId)
        {
            if (_pbInfo == null) return 0;
            for (int i = 0; i < _pbInfo.conds_num.Count; i++)
            {
                if (condsId == _pbInfo.conds_num[i].cond_id)
                {
                    return _pbInfo.conds_num[i].cond_num;
                }
            }
            return 0;
        }

        private string GetProgressContent()
        {
            string content = string.Empty;
            Dictionary<int, int> even = guild_achievement_helper.GetGuildAchievementEvent(Id);
            Dictionary<string,string> cnds = guild_achievement_helper.GetGuildAchievementConds(Id);
            foreach(KeyValuePair<string,string> kvp in cnds)
            {
                if(content != string.Empty) content += " ";
                if (_pbInfo == null || _pbInfo.achv_status == public_config.ACHIEVEMENT_STATE_PROCESSING)
                {
                    content = string.Concat(content, GetCondsNumByCondsId(int.Parse(kvp.Key)), "/", kvp.Value);
                }
                else
                {
                    content = string.Concat(content, kvp.Value, "/", kvp.Value);
                }
            }
            return content;
        }

        private float GetProgress()
        {
            float progress = 0;
            Dictionary<int, int> even = guild_achievement_helper.GetGuildAchievementEvent(Id);
            Dictionary<string, string> cnds = guild_achievement_helper.GetGuildAchievementConds(Id);
            float weight = 1.0f / cnds.Count;
            foreach (KeyValuePair<string, string> kvp in cnds)
            {
                if (_pbInfo == null || _pbInfo.achv_status == public_config.ACHIEVEMENT_STATE_PROCESSING)
                {
                    progress += weight * (float)GetCondsNumByCondsId(int.Parse(kvp.Key)) / float.Parse(kvp.Value);
                }
                else
                {
                    progress += weight * float.Parse(kvp.Value) / float.Parse(kvp.Value);
                }
            }
            return progress;
        }

    }
}
