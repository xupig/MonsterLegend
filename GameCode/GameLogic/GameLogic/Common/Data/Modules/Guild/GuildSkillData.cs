﻿using Common.Utils;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class GuildSkillData
    {
        private guild_spell _baseConfig;

        public GuildSkillData(int id)
        {
            Id = id;
            _baseConfig = XMLManager.guild_spell[id];
        }

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return guild_spell_helper.GetGuildSpellName(Id);
            }
        }

        public int Group
        {
            get
            {
                return _baseConfig.__group;
            }
        }

        public int Level
        {
            get
            {
                return _baseConfig.__level;
            }
        }

        public int Icon
        {
            get
            {
                return guild_spell_helper.GetGuildSpellIcon(Id);
            }
        }

        public string Type
        {
            get
            {
                return guild_spell_helper.GetGuildSpellType(Id);
            }
        }

        public string Effect
        {
            get
            {
                return guild_spell_helper.GetGuildSpellEffect(Id);
            }
        }

        public string NextEffect
        {
            get
            {
                return guild_spell_helper.GetGuildSpellEffectByLevel(Level + 1, Group);
            }
        }

        public string ConditionContent
        {
            get
            {
                if (HasLearned == true)
                {
                    int nextId = guild_spell_helper.GetNextLevelId(Id);
                    if (nextId != 0)
                    {
                        return guild_spell_helper.GetGuildSpellCondition(nextId);
                    }
                    return string.Empty;
                }
                return guild_spell_helper.GetGuildSpellCondition(Id);
            }
        }

        public Dictionary<string, string> Condition
        {
            get
            {
                if (HasLearned == true)
                {
                    int nextId = guild_spell_helper.GetNextLevelId(Id);
                    if (nextId != 0)
                    {
                        return guild_spell_helper.GetGuildSpellConditionDict(nextId);
                    }
                    return null;
                }
                return guild_spell_helper.GetGuildSpellConditionDict(Id);
            }
        }

        public Dictionary<string, string> Cost
        {
            get
            {
                if (HasLearned == true)
                {
                    int nextId = guild_spell_helper.GetNextLevelId(Id);
                    if (nextId != 0)
                    {
                        return data_parse_helper.ParseMap(XMLManager.guild_spell[nextId].__update_cost);
                    }
                    return null;
                }
                return data_parse_helper.ParseMap(_baseConfig.__update_cost);
            }
        }

        public bool HasLearned
        {
            get;
            set;
        }


        public int Order
        {
            get
            {
                return _baseConfig.__order;
            }
        }
    }
}
