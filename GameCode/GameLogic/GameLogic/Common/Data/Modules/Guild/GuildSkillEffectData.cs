﻿using Common.Utils;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Structs
{
    public class GuildSkillEffectData
    {
        public int GoldLimit;
        public int DiamondLimit;
        public int BindDiamondLimit;
        public int MaintainFundReduce;
        public int DonateGetFund;
        public int DonateGetContribute;
        public int AddDreamLandGoldPercent;
        public int AddDreamLandExpPercent;
    }
}
