﻿using Common.Base;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class SmelterData
    {
        public int SmeltFightForceChange;
        public int RecastFightForceChange;

        public void Check()
        {
            if (SmeltFightForceChange > 0)
            {
                string content = string.Concat(31, ",","PARAM",",", 114016, ",", SmeltFightForceChange);
                CGDialogCallBackData cgDialog = new CGDialogCallBackData("aa1", "NPC", content, 5f, true, Check);
                UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, cgDialog);
                SmeltFightForceChange = 0;
            }
            else if (RecastFightForceChange > 0)
            {
                string content = string.Concat(31, ",", "PARAM", ",", 114017, ",", RecastFightForceChange);
                CGDialogCallBackData cgDialog = new CGDialogCallBackData("aa1", "NPC", content, 5f, true, Check);
                UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, cgDialog);
                RecastFightForceChange = 0;
            }
        }
    }
}
