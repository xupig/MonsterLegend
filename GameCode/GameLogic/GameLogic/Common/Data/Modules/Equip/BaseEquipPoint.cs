﻿
using Common.Data;
using Common.Events;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    abstract public class BaseEquipPoint
    {
        public BaseEquipPoint()
        {
            AddListener();
        }

        public virtual void RemovePoint(object data = null)
        {
            ;
        }

        protected BagDataManager BagData
        {
            get
            {
                return PlayerDataManager.Instance.BagData;
            }
        }

        protected virtual void AddListener()
        {
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, BagItemUpdate);
            EventDispatcher.AddEventListener<BagType>(BagEvents.Init, InitCheck);
        }

        protected virtual void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            switch (bagType)
            {
                case BagType.BodyEquipBag:
                case BagType.ItemBag:
                    Check();
                    break;
            }
        }

        abstract protected EquipSubTab GetTab();
        public virtual object Data { get; set; }

        abstract protected void Check();

        protected void RefreshPoint()
        {
            EventDispatcher.TriggerEvent(EquipEvents.REFRESH_EQUIP_POINT);
        }

        private void InitCheck(BagType bagType)
        {
            switch (bagType)
            {
                case BagType.BodyEquipBag:
                case BagType.ItemBag:
                    Check();
                    break;
            }
        }
        
    }

    public enum EquipSubTab
    {
        strengthen,
        enchant,
        decompose,
        Smelt,
        Suit,
        Recast,
        RisingStar,
    }
}
