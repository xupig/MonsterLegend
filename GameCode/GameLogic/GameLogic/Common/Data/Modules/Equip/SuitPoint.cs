﻿using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class SuitPoint : BaseEquipPoint
    {
        private HashSet<int> _data;

        public SuitPoint()
        {
            _data = new HashSet<int>();
        }

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.Suit;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        protected override void Check()
        {
            Dictionary<int, BaseItemInfo> itemInfoList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            _data.Clear();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in itemInfoList)
            {
                EquipItemInfo equipItemInfo = kvp.Value as EquipItemInfo;
                //当装备未开启套装效果，且有套装可供使用时，提示绿点
                if (equipItemInfo.SuitId == 0)
                {
                    if (HaveSuitToEquip(equipItemInfo) == true && function_helper.IsFunctionOpen(FunctionId.suit) == true)
                    {
                        _data.Add(kvp.Key);
                    }
                }
                else if (equipItemInfo.SuitId != 0)
                {
                    if (HaveBetterFacadeScoreSuit(equipItemInfo) == true && function_helper.IsFunctionOpen(FunctionId.suit) == true)
                    {
                        _data.Add(kvp.Key);
                    }
                }
            }

            RefreshPoint();
        }

        //是否有套装可装备
        private bool HaveSuitToEquip(EquipItemInfo equipItemInfo)
        {
            List<int> suitList = equip_suit_scroll_helper.GetSuitScrollByEquip(equipItemInfo);
            for (int i = 0; i < suitList.Count; i++)
            {
                int suitNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(suitList[i]);
                if (suitNum > 0)
                {
                    return true;
                }
            }
            return false;
        }

        //如果已经有套装，则判断是否有更好的外观评分的套装
        private bool HaveBetterFacadeScoreSuit(EquipItemInfo equipItemInfo)
        {
            int currentSuitEffectsScore = equip_suit_helper.GetSuitFacedeScore(equipItemInfo.SuitId);
            List<int> suitList = equip_suit_scroll_helper.GetSuitScrollByEquip(equipItemInfo);
            for (int i = 0; i < suitList.Count; i++)
            {
                int suitNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(suitList[i]);
                if (suitNum > 0)
                {
                    int suitEffectScore = equip_suit_helper.GetSuitFacedeScore(equip_suit_scroll_helper.GetSuitId(suitList[i]));
                    if (suitEffectScore > currentSuitEffectsScore)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
