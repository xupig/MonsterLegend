﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class EquipPanelOption
    {
        /// <summary>
        /// 包括熔炼、强化，附魔，套装 0-3
        /// </summary>
        public int EquipOperateType
        {
            get;
            set;
        }

        /// <summary>
        /// 身体部位，1-11
        /// </summary>
        public int BodyPosition
        {
            get;
            set;
        }

        public EquipPanelOption(int equipOperateType, int bodyPosition = 1)
        {
            EquipOperateType = equipOperateType;
            BodyPosition = bodyPosition;
        }
    }
}
