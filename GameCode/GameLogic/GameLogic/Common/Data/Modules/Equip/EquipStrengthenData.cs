﻿using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class EquipStrengthenData
    {
        /// <summary>
        /// Key为身体的部位编号
        /// Value为相应的强化信息
        /// </summary>
        private Dictionary<int, EquipStrengthenItemInfo> _equipStrengthenInfoDict;

        public EquipStrengthenData()
        {
            _equipStrengthenInfoDict = new Dictionary<int, EquipStrengthenItemInfo>();
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnRefresh);
        }

        private void OnRefresh(BagType bagType, uint position)
        {
            if (bagType == BagType.BodyEquipBag)
            {
                UpdateStrengthenInfo((int)position);
            }
        }

        public EquipStrengthenItemInfo GetEquipStrengthenInfo(int bodyPosition)
        {
            if (_equipStrengthenInfoDict.ContainsKey(bodyPosition) == true)
            {
                return _equipStrengthenInfoDict[bodyPosition];
            }
            return null;
        }

        public Dictionary<int, EquipStrengthenItemInfo> GetAllEquipStrengthenInfo()
        {
            return _equipStrengthenInfoDict;
        }

        public void Fill(PbStrengthenInfoList data)
        {
            Dictionary<int, BaseItemInfo> equipDataDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in equipDataDic)
            {
                if (_equipStrengthenInfoDict.ContainsKey(kvp.Key) == false)
                {
                    _equipStrengthenInfoDict.Add(kvp.Key, new EquipStrengthenItemInfo(kvp.Value as EquipItemInfo));
                }
            }

            for (int i = 0; i < data.info_list.Count; i++ )
            {
                if (_equipStrengthenInfoDict.ContainsKey(data.info_list[i].id) == true)
                {
                    _equipStrengthenInfoDict.Remove(data.info_list[i].id);
                }
                _equipStrengthenInfoDict.Add(data.info_list[i].id, new EquipStrengthenItemInfo(data.info_list[i]));
            }
        }

        public void UpdateStrengthenInfo(int gridPostion)
        {
            PbStrengthenInfo strengthenInfo = null;
            if (_equipStrengthenInfoDict.ContainsKey(gridPostion) == true)
            {
                strengthenInfo = _equipStrengthenInfoDict[gridPostion].ServerStrengthenInfo;
                _equipStrengthenInfoDict.Remove(gridPostion);
            }
            if (strengthenInfo == null)
            {
                EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(gridPostion) as EquipItemInfo;
                _equipStrengthenInfoDict.Add(gridPostion, new EquipStrengthenItemInfo(equipItemInfo));
            }
            else
            {
                _equipStrengthenInfoDict.Add(gridPostion, new EquipStrengthenItemInfo(strengthenInfo));
            }
            EventDispatcher.TriggerEvent<int>(EquipStrengthenEvents.Update, gridPostion);
        }
    }
}
