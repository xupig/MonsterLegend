﻿using Common.ClientConfig;
using Common.Structs;
using GameData;
using GameMain.Entities;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class RisingStarPoint:BaseEquipPoint
    {
        private HashSet<int> _data;

        public RisingStarPoint()
        {
            _data = new HashSet<int>();
        }

        protected override void AddListener()
        {
            base.AddListener();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, Check);
        }

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.RisingStar;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        protected override void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetItemBagData().GetItemInfo((int)gridPosition);
                if (itemInfo == null || itemInfo.Type == BagItemType.Material)
                {
                    CheckCanRisingStar(itemInfo);
                }
            }
            else if (bagType == BagType.BodyEquipBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetBodyEquipBagData().GetItemInfo((int)gridPosition);
                CheckEquipRisingStar(itemInfo as EquipItemInfo);
            }

            RefreshPoint();
        }

        public override void RemovePoint(object data)
        {
            base.RemovePoint();
            if(_data.Contains((int)data) == true)
            {
                _data.Remove((int)data);
                RefreshPoint();
            }
        }

        protected override void Check()
        {
            _data.Clear();
            Dictionary<int, BaseItemInfo> bodyEquipDic = base.BagData.GetBodyEquipBagData().GetAllItemInfo();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in bodyEquipDic)
            {
                CheckEquipRisingStar(kvp.Value as EquipItemInfo);
            }
            RefreshPoint();
        }

        private void CheckCanRisingStar(BaseItemInfo itemInfo)
        {
            Dictionary<int, BaseItemInfo> bodyEquipDic = base.BagData.GetBodyEquipBagData().GetAllItemInfo();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in bodyEquipDic)
            {
                List<ItemData> costList = equip_upgrade_helper.GetCost(kvp.Value as EquipItemInfo);
                for (int i = 0; i < costList.Count; i++)
                {
                    if (itemInfo == null || itemInfo.Id == costList[i].Id)
                    {
                        CheckEquipRisingStar(kvp.Value as EquipItemInfo);
                        break;                      
                    }
                }
            }
        }

        private void CheckEquipRisingStar(EquipItemInfo equipItemInfo)
        {
            List<ItemData> costList = equip_upgrade_helper.GetCost(equipItemInfo);
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            for (int i = 0; i < costList.Count; i++)
            {
                baseItemDataList.Add(costList[i]);
            }
            if (equip_upgrade_helper.GetLimitContent(equipItemInfo) == string.Empty
                && equip_upgrade_helper.CanUpgrade(equipItemInfo) == true
                && PlayerAvatar.Player.CheckCostLimit(baseItemDataList) == 0
                && function_helper.IsFunctionOpen(FunctionId.risingStar) == true)
            {
                _data.Add(equipItemInfo.GridPosition);
            }
            else
            {
                _data.Remove(equipItemInfo.GridPosition);
            }
        }
    }
}
