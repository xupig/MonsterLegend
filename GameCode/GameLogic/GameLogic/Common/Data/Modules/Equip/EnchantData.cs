﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class EnchantData
    {
        private Dictionary<int, PbEquipEnchantAttriInfo> _enchantAttriteDic;

        public EnchantData()
        {
            _enchantAttriteDic = new Dictionary<int,PbEquipEnchantAttriInfo>();
        }

        public void Fill(PbEquipEnchantAttriRec enchantRecord)
        {

            List<PbEquipEnchantAttriInfo> allEnchantInfo = enchantRecord.equip_enchant_attri_info;
            for (int i = 0; i < allEnchantInfo.Count; i++)
            {
                _enchantAttriteDic.Add((int)allEnchantInfo[i].pos, allEnchantInfo[i]);
            }
        }

        public void UpdateEnchantAttri(PbEquipEnchantAttriInfo enchantAttri)
        {
            if (_enchantAttriteDic.ContainsKey((int)enchantAttri.pos) == false)
            {
                _enchantAttriteDic.Add((int)enchantAttri.pos, enchantAttri);
            }
            else
            {
                _enchantAttriteDic[(int)enchantAttri.pos] = enchantAttri;
            }
        }

        public void UpdateLuckyValue(uint gridPosition, uint luckyValue)
        {
            if (_enchantAttriteDic.ContainsKey((int)gridPosition) == true)
            {
                _enchantAttriteDic[(int)gridPosition].luck_value = luckyValue;
            }
            else
            {
                PbEquipEnchantAttriInfo enchantAttriInfo = new PbEquipEnchantAttriInfo();
                enchantAttriInfo.luck_value = luckyValue;
                _enchantAttriteDic.Add((int)gridPosition, enchantAttriInfo);
            }
        }

        public PbEquipEnchantAttriInfo GetEnchantAttriInfoByBodyId(int gridPosition)
        {
            if (_enchantAttriteDic.ContainsKey(gridPosition) == true)
            {
                return _enchantAttriteDic[gridPosition];
            }
            return null;
        }
    }
}
