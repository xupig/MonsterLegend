﻿
using Common.Structs;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class SmeltPoint : BaseEquipPoint
    {
        private HashSet<int> _data;
        private bool _haveSmeltPoint = false;
        public bool HaveSmeltPoint
        {
            get
            {
                return _haveSmeltPoint;
            }
            set
            {
                if (_haveSmeltPoint != value)
                {
                    _haveSmeltPoint = value;
                    RefreshPoint();
                }
            }
        }

        public override void RemovePoint(object data = null)
        {
            HaveSmeltPoint = false;
        }

        public SmeltPoint()
        {
            _data = new HashSet<int>();
        }

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.Smelt;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        protected override void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            switch (bagType)
            {
                case BagType.ItemBag:
                    BaseItemInfo item = BagData.GetBagData(bagType).GetItemInfo((int)gridPosition);
                    if(item != null && equip_smelting_helper.IsSmeltStone(item.Id))
                    {
                        CheckSmeltPointByItemId(item.Id);
                    }
                    else if (item == null)
                    {
                        TryRemovePoint();
                    }
                    break;
            }
        }

        protected override void Check()
        {
            CheckSmelt();
            RefreshPoint();
        }

        private void CheckSmelt()
        {
            List<int> smeltLevelList = equip_smelting_helper.GetLevelList();
            for (int i = 0; i < smeltLevelList.Count; i++)
            {
                List<equip_smelting> smeltList = equip_smelting_helper.GetAllSmeltByLevel(smeltLevelList[i]);

                bool canSmelt = false;
                for (int j = 0; j < smeltList.Count; j++)
                {
                    List<BaseItemData> costList = equip_smelting_helper.GetCostList(smeltList[j].__id);
                    canSmelt = (PlayerAvatar.Player.CheckCostLimit(costList) == 0 && smeltList[j].__level_limit <= PlayerAvatar.Player.level);
                    if (canSmelt == true)
                    {
                        break;
                    }
                }
                if (canSmelt == true && function_helper.IsFunctionOpen(FunctionId.smelt) == true)
                {
                    _data.Add(smeltLevelList[i]);
                    if (smeltLevelList[i] >= PlayerAvatar.Player.level / 10 * 10)
                    {
                        HaveSmeltPoint = true;
                    }
                }
                else
                {
                    _data.Remove(smeltLevelList[i]);
                    bool haveCanSmelt = false;
                    foreach (int level in _data)
                    {
                        if (level >= PlayerAvatar.Player.level / 10 * 10)
                        {
                            haveCanSmelt = true;
                            break;
                        }
                    }
                    HaveSmeltPoint = (HaveSmeltPoint && haveCanSmelt);
                }
            }

        }

        private void CheckSmeltPointByItemId(int smeltItemId)
        {
            equip_smelting smeltItem = equip_smelting_helper.GetConfigByItemId(smeltItemId);
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(smeltItem.__id);
            bool canSmelt = PlayerAvatar.Player.CheckCostLimit(costList) == 0;

            if (canSmelt)
            {
                _data.Add(smeltItem.__level_limit);
                if (smeltItem.__level_limit >= PlayerAvatar.Player.level / 10 * 10 && PlayerAvatar.Player.level >= smeltItem.__level_limit)
                {
                    HaveSmeltPoint = true;
                }
            }
            else if(!canSmelt)
            {
                List<equip_smelting> smeltList = equip_smelting_helper.GetAllSmeltByLevel(smeltItem.__level_limit);
                bool haveItemCanSmelt = false;
                for (int j = 0; j < smeltList.Count; j++)
                {
                    List<BaseItemData> costItemList = equip_smelting_helper.GetCostList(smeltList[j].__id);
                    haveItemCanSmelt = PlayerAvatar.Player.CheckCostLimit(costItemList) == 0;
                    if (haveItemCanSmelt == true)
                    {
                        break;
                    }
                }
                if (haveItemCanSmelt == true && function_helper.IsFunctionOpen(FunctionId.smelt) == true)
                {
                    _data.Add(smeltItem.__level_limit);
                    if (smeltItem.__level_limit >= PlayerAvatar.Player.level / 10 * 10)
                    {
                        HaveSmeltPoint = haveItemCanSmelt && HaveSmeltPoint;
                    }
                }
                else
                {
                    _data.Remove(smeltItem.__level_limit);
                    bool haveCanSmelt = false;
                    foreach (int level in _data)
                    {
                        if (level >= PlayerAvatar.Player.level / 10 * 10)
                        {
                            haveCanSmelt = true;
                            break;
                        }
                    }
                    HaveSmeltPoint = (HaveSmeltPoint && haveCanSmelt);
                }
            }
        }

        private void TryRemovePoint()
        {
            List<int> removeList = new List<int>();
            foreach(int level in _data)
            {
                List<equip_smelting> smeltList = equip_smelting_helper.GetAllSmeltByLevel(level);

                bool canSmelt = false;
                for (int j = 0; j < smeltList.Count; j++)
                {
                    List<BaseItemData> costList = equip_smelting_helper.GetCostList(smeltList[j].__id);
                    canSmelt = PlayerAvatar.Player.CheckCostLimit(costList) == 0;
                    if (canSmelt == true)
                    {
                        break;
                    }
                }

                if(canSmelt == false)
                {
                    removeList.Add(level);
                }
            }
            for(int i = 0; i < removeList.Count; i++)
            {
                _data.Remove(removeList[i]);
                bool haveCanSmelt = false;
                foreach (int level in _data)
                {
                    if (level >= PlayerAvatar.Player.level / 10 * 10)
                    {
                        haveCanSmelt = true;
                        break;
                    }
                }
                HaveSmeltPoint = (HaveSmeltPoint && haveCanSmelt);
            }
            if (removeList.Count != 0)
            {
                RefreshPoint();
            }
        }
    }
}
