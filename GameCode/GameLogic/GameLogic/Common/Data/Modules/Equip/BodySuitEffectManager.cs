﻿using Common.Base;
using Common.Events;
using Common.Structs;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class BodySuitEffectManager
    {
        private static BodySuitEffectManager _instance;
        public static BodySuitEffectManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new BodySuitEffectManager();
                }
                return _instance;
            }
        }

        private Dictionary<int, int> _bodySuitEffectDic;

        public BodySuitEffectManager()
        {
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, CheckSuitEffectChange);
        }

        public void InitBodySuitEffectDic()
        {
            if (_bodySuitEffectDic == null)
            {
                _bodySuitEffectDic = new Dictionary<int, int>();
            }
            _bodySuitEffectDic.Clear();
            Dictionary<int, BaseItemInfo> bodyEquipDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            foreach (KeyValuePair<int, BaseItemInfo> pair in bodyEquipDic)
            {
                _bodySuitEffectDic[pair.Key] = (pair.Value as EquipItemInfo).SuitId;
            }
        }

        public void CheckSuitEffectChange(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.BodyEquipBag)
            {
                EquipItemInfo equipInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo((int)gridPosition) as EquipItemInfo;

                //只考虑套装效果有可能降低的情况
                if (_bodySuitEffectDic.ContainsKey((int)gridPosition) && _bodySuitEffectDic[(int)gridPosition] != equipInfo.SuitId)
                {
                    int oldSuitEffect = _bodySuitEffectDic[(int)gridPosition];
                    int newSuitEffect = equipInfo.SuitId;
                    _bodySuitEffectDic[(int)gridPosition] = newSuitEffect;
                    if (oldSuitEffect == 0) { return; } // 老套装效果为0,套装战力肯定不会下降

                    int equipedOldSuitEffectCount = equip_suit_helper.GetEquipedSuitPartCount(oldSuitEffect);
                    int beforeOldSuitEffectFightForce = equip_suit_helper.GetFightForce(oldSuitEffect, equipedOldSuitEffectCount + 1);
                    int afterOldSuitEffectFightForce = equip_suit_helper.GetFightForce(oldSuitEffect, equipedOldSuitEffectCount);

                    int beforeNewSuitEffectFightForce = 0;
                    int afterNewSuitEffectFightForce = 0;
                    if (newSuitEffect != 0)
                    {
                        int equipedNewSuitEffectCount = equip_suit_helper.GetEquipedSuitPartCount(newSuitEffect);
                        beforeNewSuitEffectFightForce = equip_suit_helper.GetFightForce(newSuitEffect, equipedNewSuitEffectCount - 1);
                        afterNewSuitEffectFightForce = equip_suit_helper.GetFightForce(newSuitEffect, equipedNewSuitEffectCount);
                    }
                    int fightForceChangeNum = afterOldSuitEffectFightForce + afterNewSuitEffectFightForce - (beforeNewSuitEffectFightForce + beforeOldSuitEffectFightForce);
                    if (fightForceChangeNum < 0)
                    {
                        string suitName = equip_suit_helper.GetName(oldSuitEffect);
                        string content = MogoLanguageUtil.GetContent(108018, suitName, equipedOldSuitEffectCount + 1, Math.Abs(fightForceChangeNum));
                        ///108019, 去套装界面
                        string goSuitPanelTxt = MogoLanguageUtil.GetContent(108019);
                        ///108020, 我知道了
                        string cancelTxt = MogoLanguageUtil.GetContent(108020);
                        /*
                        if (EquipPanel.ShowingTab != 2 || BaseModule.IsPanelShowing(PanelIdEnum.SuitDetail)) //在套装相关界面
                        {
                      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm, OnCancel, goSuitPanelTxt, cancelTxt, null, false, false);
                        }
                        else
                        {
                      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnCancel, null, cancelTxt, null, null, true, false);
                        }*/
                    }
                }
            }
        }

        private void OnConfirm()
        {
            EquipPanelOption equipPanelData = new EquipPanelOption(2);
            UIManager.Instance.ShowPanel(PanelIdEnum.Equip, equipPanelData);
        }

        private void OnCancel()
        {

        }
    }
}
