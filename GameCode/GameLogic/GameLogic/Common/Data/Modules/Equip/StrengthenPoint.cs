﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class StrengthenPoint:BaseEquipPoint
    {
        public int IgnoreStrengthenPointTimes = 0;
        private HashSet<int> _data;

        public StrengthenPoint()
        {
            _data = new HashSet<int>();
        }

        protected override void AddListener()
        {
            base.AddListener();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, Check);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, Check);
            EventDispatcher.AddEventListener(EquipStrengthenEvents.Init, Check);
            EventDispatcher.AddEventListener<int>(EquipStrengthenEvents.Update, CheckIsCanStrengthenByPosition);
        }

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.strengthen;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        protected override void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetItemBagData().GetItemInfo((int)gridPosition);
                if (itemInfo == null || itemInfo.Type == BagItemType.Material)
                {
                    CheckIsCanStrengthenAgain();
                }
            }
            RefreshPoint();
        }

        protected override void Check()
        {
            CheckIsCanStrengthenAgain();
            RefreshPoint();
        }

        public void CheckIsCanStrengthenAgain()
        {
            
            Dictionary<int, EquipStrengthenItemInfo> equipStrengthenDic = PlayerDataManager.Instance.EquipStrengthenData.GetAllEquipStrengthenInfo();
            foreach(KeyValuePair<int, EquipStrengthenItemInfo> kvp in equipStrengthenDic)
            {
                if (kvp.Value.IsCanStrenghthen == true && function_helper.IsFunctionOpen(FunctionId.equip) == true)
                {
                    _data.Add(kvp.Key);
                }
                else
                {
                    if(_data.Contains(kvp.Key) == true)
                    {
                        _data.Remove(kvp.Key);
                    }
                }
            }
        }

        private void CheckIsCanStrengthenByPosition(int gridPosition)
        {
            Dictionary<int, EquipStrengthenItemInfo> equipStrengthenDic = PlayerDataManager.Instance.EquipStrengthenData.GetAllEquipStrengthenInfo();
            if (equipStrengthenDic.ContainsKey(gridPosition) == true)
            {
                if (equipStrengthenDic[gridPosition].IsCanStrenghthen == true && function_helper.IsFunctionOpen(FunctionId.equip) == true)
                {
                    _data.Add(gridPosition);
                }
                else
                {
                    if (_data.Contains(gridPosition) == true)
                    {
                        _data.Remove(gridPosition);
                    }
                }
            }
        }
    }
}
