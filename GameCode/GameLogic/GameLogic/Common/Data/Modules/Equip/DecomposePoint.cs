﻿
using Common.Data;
using Common.Structs;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class DecomposePoint : BaseEquipPoint
    {
        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.decompose;
        }

        public override object Data
        {
            get
            {
                return CheckIsCanDecomposition();
            }
        }

        protected override void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            RefreshPoint();
        }

        protected override void Check()
        {
            
        }

        private bool CheckIsCanDecomposition()
        {
            List<BaseItemInfo> itemInfoList = base.BagData.GetBagData(BagType.ItemBag).GetTypeItemInfo(BagItemType.Equip);
            for (int i = 0; i < itemInfoList.Count; i++)
            {
                if (CheckIsCanDecompositionAgain(itemInfoList[i] as EquipItemInfo) == true)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckIsCanDecompositionAgain(EquipItemInfo equipitemInfo)
        {
            if (item_helper.IsEquipBetter(equipitemInfo) == false)
            {
                return true;
            }
            return false;
        }   
    }
}
