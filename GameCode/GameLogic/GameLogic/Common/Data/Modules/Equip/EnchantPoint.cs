﻿
using Common.Data;
using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class EnchantPoint : BaseEquipPoint
    {
        private HashSet<int> _data;

        public EnchantPoint()
        {
            _data = new HashSet<int>();
        }

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.enchant;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        protected override void AddListener()
        {
            base.AddListener();
            EventDispatcher.AddEventListener(EnchantEvents.Init, Check);
            EventDispatcher.AddEventListener<int>(EnchantEvents.UpdateEnchantByBody, bodyPosition => { Check(); });
        }
        
        protected override void BagItemUpdate(Common.Data.BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetItemBagData().GetItemInfo((int)gridPosition);
                if (itemInfo == null || itemInfo.Type == BagItemType.EnchantScroll)
                {
                    CheckIsCanEnchantAgain();
                }
            }
            else if (bagType == BagType.BodyEquipBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetBodyEquipBagData().GetItemInfo((int)gridPosition);
                CheckIsCanEnchantAgain(itemInfo, (int)gridPosition);
            }

            RefreshPoint();

        }

        protected override void Check()
        {
            CheckIsCanEnchantAgain();
            RefreshPoint();
        }

        private void CheckIsCanEnchantAgain()
        {
            Dictionary<int, BaseItemInfo> bodyEquipDic = base.BagData.GetBodyEquipBagData().GetAllItemInfo();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in bodyEquipDic)
            {
                CheckIsCanEnchantAgain(kvp.Value, kvp.Key);
            }
        }

        private void CheckIsCanEnchantAgain(BaseItemInfo itemInfo, int gridPosition)
        {
            EquipItemInfo equipItemInfo = itemInfo as EquipItemInfo;
            PbEquipEnchantAttriInfo enchantAttriInfo = PlayerDataManager.Instance.EnchantData.GetEnchantAttriInfoByBodyId(gridPosition);
            int enchantScrollId = enchant_scroll_helper.GetEnchantScrollId(equipItemInfo);
            if (enchantScrollId != 0)
            {
                int enchantScrollCount = base.BagData.GetItemBagData().GetItemNum(enchantScrollId);
                if (enchantScrollCount != 0 && enchantAttriInfo == null && function_helper.IsFunctionOpen(FunctionId.enchant) == true)
                {
                    _data.Add(gridPosition);
                    return;
                }
            }
            if(_data.Contains(gridPosition) == true)
            {
                _data.Remove(gridPosition);
            }
        }
    }
}
