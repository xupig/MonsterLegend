﻿
using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class RecastPoint:BaseEquipPoint
    {
        private bool _data;

        protected override EquipSubTab GetTab()
        {
            return EquipSubTab.Recast;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
        }

        public override void RemovePoint(object data)
        {
            if (_data)
            {
                _data = false;
                RefreshPoint();
            }
        }

        protected override void BagItemUpdate(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = base.BagData.GetItemBagData().GetItemInfo((int)gridPosition);
                if (itemInfo == null || itemInfo.Type == BagItemType.Material)
                {
                    CheckCanRecast(itemInfo);
                    RefreshPoint();
                }
            }
        }

        protected override void Check()
        {
            CheckRecast();
            RefreshPoint();
        }

        private void CheckCanRecast(BaseItemInfo itemInfo)
        {
            Dictionary<int, BaseItemInfo> bodyEquipDic = base.BagData.GetBodyEquipBagData().GetAllItemInfo();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in bodyEquipDic)
            {
                EquipItemInfo equipItemInfo = kvp.Value as EquipItemInfo;
                if (equip_recast_helper.CanRecast(equipItemInfo) == false)
                {
                    continue;
                }
                BaseItemData itemData = equip_recast_helper.GetRecastCost(equipItemInfo);
                if (itemInfo == null || itemInfo.Id == itemData.Id)
                {
                    if (CheckEquipCanRecast(kvp.Value as EquipItemInfo) == true)
                    {
                        _data = true;
                        return;
                    }
                }

            }
            _data = false;
        }

        private bool CheckEquipCanRecast(EquipItemInfo equipItemInfo)
        {
            BaseItemData itemData = equip_recast_helper.GetRecastCost(equipItemInfo);
            if (itemData == null)
            {
                return false;
            }
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(itemData.Id);
            if (itemNum >= itemData.StackCount && function_helper.IsFunctionOpen(FunctionId.smelter) == true)
            {
                return true;
            }
            return false;
        }

        private void CheckRecast()
        {
            Dictionary<int, BaseItemInfo> equipInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            _data = false;
            foreach (KeyValuePair<int, BaseItemInfo> kvp in equipInfoDic)
            {
                EquipItemInfo equipItemInfo = kvp.Value as EquipItemInfo;

                if (equip_recast_helper.CanRecast(equipItemInfo) == false)
                {
                    continue;
                }
                if (CheckEquipCanRecast(kvp.Value as EquipItemInfo) == true)
                {
                    _data = true;
                    return;
                }
            }
            _data = false;
        }
    }
}
