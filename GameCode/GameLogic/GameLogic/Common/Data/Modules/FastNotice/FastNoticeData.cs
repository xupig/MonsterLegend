﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using GameData;
using UnityEngine;

namespace Common.Data
{
    public class FastNoticeData
    {
        public string Content { get; set; }
        public PanelIdEnum PanelId { get; set; }
        public int ViewId { get; set; }
        public object Data { get; set; }
        public Dictionary<string, string[]> followData { get; set; }

        public FastNoticeData(string content, PanelIdEnum panelId, object data = null)
        {
            Content = content;
            PanelId = panelId;
            Data = data;
        }

        public FastNoticeData(string content, Dictionary<string, string[]> followData)
        {
            Content = content;
            PanelId = GetPanelId(followData);
            ViewId = GetViewId(followData);
            this.followData = followData;
        }

        private PanelIdEnum GetPanelId(Dictionary<string, string[]> followData)
        {
            if(followData == null || followData.Count == 0)
            {
                return PanelIdEnum.Empty;
            }
            foreach(KeyValuePair<string,string[]> kvp in followData)
            {
                int key = int.Parse(kvp.Key);
                if(key == 0)
                {
                    view _view = view_helper.GetView(int.Parse(kvp.Value[0]));
                   
                    return (PanelIdEnum)_view.__panel_id;
                }
                else
                {
                    return (PanelIdEnum)key;
                }
            }
            return PanelIdEnum.Empty;
        }

        private int GetViewId(Dictionary<string, string[]> followData)
        {
            int viewId = 0;
            foreach (KeyValuePair<string, string[]> kvp in followData)
            {
                int key = int.Parse(kvp.Key);
                if (key == 0)
                {
                    viewId = int.Parse(kvp.Value[0]);
                    break;
                }
            }
            return viewId;
        }
    }
}
