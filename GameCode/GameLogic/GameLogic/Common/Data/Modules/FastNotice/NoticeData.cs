﻿#region 模块信息
/*==========================================
// 文件名：NoticeData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.FastNotice
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/8/18 22:32:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class NoticeData
    {
        public List<FastNoticeData> dataList;

        public NoticeData()
        {
            dataList = new List<FastNoticeData>();
        }


        public void AddNoticeData(FastNoticeData data)
        {
            dataList.Add(data);
            EventDispatcher.TriggerEvent(MainUIEvents.FAST_NOTICE_LIST_CHANGE);
        }

        public void RemoveNoticeData(PanelIdEnum panelId)
        {
            for (int i = dataList.Count - 1; i >= 0; i--)
            {
                if (dataList[i].PanelId == panelId)
                {
                    dataList.RemoveAt(i);
                }
            }
        }

        public void RemoveNoticeData(int viewId)
        {
            for (int i = dataList.Count - 1; i >= 0; i--)
            {
                if (dataList[i].ViewId == viewId)
                {
                    dataList.RemoveAt(i);
                }
            }
        }
    }
}
