﻿#region 模块信息
/*==========================================
// 文件名：FastCacheData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.FastNotice
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/27 21:24:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class FastCacheData
    {
        public int panelId;
        public int panelViewIndex;
        public int count;
        public string content;
    }
}
