﻿#region 模块信息
/*==========================================
// 文件名：AchievementData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Achievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 19:37:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;
namespace Common.Data
{
    public enum AchievementState
    {
        NotStarted = 0,
        Processing = 1,
        Completed = 2,
        Rewarded = 3,
    }

    public class SingleAchievementData
    {
        public int id;
        public AchievementState state;
        public int currentNum;
        public int targetNum;
        public int group;
        public int finishOrder;
        public int property;
        public int find;
        public bool isShow; // 是否需要在成就系统中显示

        public SingleAchievementData(achievement achievementCfg)
        {
            id = achievementCfg.__id;
            state = AchievementState.NotStarted;
            currentNum = 0;
            targetNum = achievement_helper.GetTargetNums(id);
            find = achievementCfg.__kind;
            group = achievementCfg.__group;
            finishOrder = achievementCfg.__finish_order;
            property = achievementCfg.__property;
            isShow = achievementCfg.__is_show == 1;
        }

        public SingleAchievementData()
        {

        }
    }

    public class AchievementData
    {
        // 包含所有的成就的状态
        private Dictionary<int, SingleAchievementData> achievementDict = new Dictionary<int, SingleAchievementData>();
        private int hasRewardTask;
        public bool HasRewardedTask()
        {
            return hasRewardTask > 0;
        }

        // 初始化，从配置表中进行初始化
        public AchievementData()
        {
            hasRewardTask = 0;
            foreach (KeyValuePair<int, achievement> pair in XMLManager.achievement)
            {
                if (achievementDict.ContainsKey(pair.Value.__id) == false)
                {
                    achievementDict.Add(pair.Value.__id, new SingleAchievementData(pair.Value));
                }
                else
                {
                    achievementDict[pair.Value.__id] = new SingleAchievementData(pair.Value);
                }
            }
        }

        // 接收到服务器的状态数据后，对数据更新操作
        public void Init(PbAchievementQuery pbQuery)
        {
            UpdateProcessingAchievement(pbQuery.processing);
            UpdateCompletedAchievement(pbQuery.completed);
            UpdateRewardedAchievement(pbQuery.rewarded);
        }

        public SingleAchievementData GetSingleAchievementData(int achievementId)
        {
            if (achievementDict.ContainsKey(achievementId))
            {
                return achievementDict[achievementId];
            }
            UnityEngine.Debug.LogError("配置表中不包括成就ID为 " + achievementId + " 的配置");
            return null;
        }

        public AchievementState GetSingleAchievementState(int achievementId)
        {
            if (achievementDict.ContainsKey(achievementId))
            {
                return achievementDict[achievementId].state;
            }
            UnityEngine.Debug.LogError("配置表中不包括成就ID为 " + achievementId + " 的配置");
            return AchievementState.NotStarted;
        }

        public int GetAchievementCurrentNum(int achievementId)
        {
            if (achievementDict.ContainsKey(achievementId))
            {
                return achievementDict[achievementId].currentNum;
            }
            return 0;
        }

        public int GetAchievementTargetNum(int achievementId)
        {
            if (achievementDict.ContainsKey(achievementId))
            {
                return achievementDict[achievementId].targetNum;
            }
            return 0;
        }

        public int GetAchievementPercent(int achievementId)
        {
            if (achievementDict.ContainsKey(achievementId))
            {
                SingleAchievementData singleData = GetSingleAchievementData(achievementId);
                if (singleData.state == AchievementState.Processing)
                {
                    return Mathf.FloorToInt(singleData.currentNum * 100.0f / singleData.targetNum);
                }
            }
            return 0;
        }

        private void UpdateProcessingAchievement(List<PbAchievementInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                PbAchievementInfo pbAchiInfo = list[i];
                if (achievementDict.ContainsKey(pbAchiInfo.achv_id))
                {
                    achievementDict[pbAchiInfo.achv_id].state = AchievementState.Processing;
                    achievementDict[pbAchiInfo.achv_id].currentNum = pbAchiInfo.achv_num;
                }
            }
        }

        private void UpdateCompletedAchievement(List<PbAchievementInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                PbAchievementInfo pbAchiInfo = list[i];
                if (achievementDict.ContainsKey(pbAchiInfo.achv_id))
                {
                    achievementDict[pbAchiInfo.achv_id].state = AchievementState.Completed;
                }
            }
        }

        private void UpdateRewardedAchievement(List<PbAchievementInfo> list)
        {
            hasRewardTask = list.Count;
            for (int i = 0; i < list.Count; i++)
            {
                PbAchievementInfo pbAchiInfo = list[i];
                if (achievementDict.ContainsKey(pbAchiInfo.achv_id))
                {
                    achievementDict[pbAchiInfo.achv_id].state = AchievementState.Rewarded;
                }
            }
        }

        public void UpdateRewardedAchievement(PbAchievementInfo pbInfo)
        {
            hasRewardTask += 1;
            if (achievementDict.ContainsKey(pbInfo.achv_id))
            {
                achievementDict[pbInfo.achv_id].state = AchievementState.Rewarded;
            }
        }

        public void UpdateProcessingAchievement(PbAchievementInfo pbInfo)
        {
            if (achievementDict.ContainsKey(pbInfo.achv_id))
            {
                achievementDict[pbInfo.achv_id].state = AchievementState.Processing;
                achievementDict[pbInfo.achv_id].currentNum = pbInfo.achv_num;
            }
        }

        public void UpdateCompletedAchievement(PbAchievementInfo pbInfo)
        {
            if (achievementDict.ContainsKey(pbInfo.achv_id))
            {
                achievementDict[pbInfo.achv_id].state = AchievementState.Completed;
            }
        }

        public List<SingleAchievementData> GetRewardedAchievementList()
        {
            List<SingleAchievementData> result = new List<SingleAchievementData>();
            foreach (var pair in achievementDict)
            {
                if (pair.Value.isShow && pair.Value.state == AchievementState.Rewarded)
                {
                    result.Add(pair.Value);
                }
            }
            return result;
        }

        public List<SingleAchievementData> GetAchievementList(int groupId)
        {
            List<int> achievementDataList = achievement_helper.GetAchievementsByGroup(groupId);
            Dictionary<int, SingleAchievementData> dict = new Dictionary<int, SingleAchievementData>();
            for (int i = 0; i < achievementDataList.Count; i++)
            {
                SingleAchievementData singleAchievementData = GetSingleAchievementData(achievementDataList[i]);
                if (singleAchievementData.isShow == false)
                {
                    continue;
                }
                int group = singleAchievementData.group;
                if (singleAchievementData.state == AchievementState.Rewarded)
                {
                    continue;
                }
                if (dict.ContainsKey(group) == false)
                {
                    dict.Add(group, singleAchievementData);
                }
                else
                {
                    SingleAchievementData compareAchievementData = dict[group];
                    if(singleAchievementData.finishOrder < compareAchievementData.finishOrder
                        && singleAchievementData.state != AchievementState.Processing)
                    {
                        dict[group] = singleAchievementData;
                    }
                }
            }
            List<SingleAchievementData> result = new List<SingleAchievementData>(dict.Values);
            result.Sort(SortFunction);
            return result;
        }

        private int SortFunction(SingleAchievementData x, SingleAchievementData y)
        {
            if (x.state != y.state)
            {
                return (int)y.state - (int)x.state;
            }
            else
            {
                return x.group - y.group;
            }
        }

        public bool GroupContainsCompletedAchievement(int groupId)
        {
            List<SingleAchievementData> list = new List<SingleAchievementData>(achievementDict.Values);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].find == groupId && list[i].isShow == true && list[i].state == AchievementState.Completed)
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasCompletedAchievement()
        {
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.Achievement) == false)
            {
                return false;
            }
            List<SingleAchievementData> list = new List<SingleAchievementData>(achievementDict.Values);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].state == AchievementState.Completed && list[i].isShow == true)
                {
                    return true;
                }
            }
            return false;
        }
    }
}