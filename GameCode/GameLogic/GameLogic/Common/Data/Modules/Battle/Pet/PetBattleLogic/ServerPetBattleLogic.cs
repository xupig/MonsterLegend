﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class ServerPetBattleLogic : PetBattleLogic
    {
        private PetInfo _currentPetInfo;
        public ServerPetBattleLogic()
        {
        }

        public override void ReponsePetDataReady(int mapId)
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            //Debug.LogError("CanPetFight:" + CopyManager.Instance.CanPetFight((ChapterType)mapType, mapId));
            if (CopyLogicManager.Instance.CanPetFight((ChapterType)mapType, mapId))
            {
                InitPetData();
            }
        }

        public override PetInfo GetCurrentPetInfo()
        {
            if (PetData.FightingPetList.Count == 0)
            {
                _currentPetInfo = null;
            }
            else
            {
                PetInfo petInfo = PetData.FightingPet;
                //保留最后一只死亡的宠物
                if (petInfo != null)
                {
                    _currentPetInfo = petInfo;
                }
            }
            return _currentPetInfo;
        }

        public override List<PetInfo> GetPetInfoList()
        {
            return PetData.FightingPetList;
        }

        public override List<PetInfo> GetInactivePetInfoList()
        {
            List<PetInfo> inactivePetInfoList = new List<PetInfo>();
            List<PetInfo> fightingPetInfoList = PetData.FightingPetList;
            for(int i = 0;i < fightingPetInfoList.Count;i++)
            {
                if((fightingPetInfoList[i].FightState != PetFightState.fighting)
                    &&(fightingPetInfoList[i] != _currentPetInfo))
                {
                    inactivePetInfoList.Add(fightingPetInfoList[i]);
                }
            }
            inactivePetInfoList.Sort(PetSort);
            return inactivePetInfoList;
        }

        private int PetSort(PetInfo x, PetInfo y)
        {
            if (x.FightState == PetFightState.idle && y.FightState != PetFightState.idle)
            {
                return -1;
            }
            else
            {
                if (x.IndexForSort < y.IndexForSort)
                {
                    return -1;
                }
            }
            return 1;
        }

        public override void LevelCombatScene()
        {
            inited = false;
        }

        public override void ReplacePet(int petId)
        {
            PetManager.Instance.RequestReplacePetInBattle(petId);
        }

        public override void UpdatePetFightState()
        {
            EventDispatcher.TriggerEvent(BattleUIEvents.UPDATE_PET_FIGHT_STATE);
        }
    }
}
