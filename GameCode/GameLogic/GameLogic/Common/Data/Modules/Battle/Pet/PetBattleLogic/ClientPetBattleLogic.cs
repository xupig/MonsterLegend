﻿using Common.Events;
using Common.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class ClientPetBattleLogic : PetBattleLogic
    {
        private PetInfo _currentPetInfo;
        private List<PetInfo> _petInfoList = new List<PetInfo>();
        private List<PetInfo> _inactivePetInfoList = new List<PetInfo>();

        public override PetInfo GetCurrentPetInfo()
        {
            return _currentPetInfo;
        }

        public void SetCurrentPetInfo(PetInfo petInfo)
        {
            if (_currentPetInfo != null)
            {
                _inactivePetInfoList.Add(_currentPetInfo);
            }
            _currentPetInfo = petInfo;
            if (petInfo != null)
            {
                _inactivePetInfoList.Remove(petInfo);
            }
        }

        public override List<PetInfo> GetPetInfoList()
        {
            return _petInfoList;
        }

        public override List<PetInfo> GetInactivePetInfoList()
        {
            return _inactivePetInfoList;
        }

        public override void InitPetData()
        {
            _currentPetInfo = null;
            _inactivePetInfoList.Clear();
            List<PetInfo> petInfoList = PetData.FightingPetList;
            _petInfoList.Clear();
            for (int i = 0; i < petInfoList.Count; i++)
            {
                PetInfo petInfo = petInfoList[i];
                _petInfoList.Add(petInfo.Copy());
            }
            _inactivePetInfoList.AddRange(_petInfoList);
            ResetAllFightState(_petInfoList);
            SetFirstPetFighting(_petInfoList);
            AddListener();
            base.InitPetData();
        }

        public override void LevelCombatScene()
        {
            RemoveListener();
            inited = false;
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener<int>(BattleUIEvents.PET_DIE, PetDie);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener<int>(BattleUIEvents.PET_DIE, PetDie);
        }

        private void ResetAllFightState(List<PetInfo> petInfoList)
        {
            for (int i = 0; i < petInfoList.Count; i++)
            {
                PetInfo petInfo = petInfoList[i];
                petInfo.FightState = PetFightState.idle;
            }
        }

        private void SetFirstPetFighting(List<PetInfo> petInfoList)
        {
            if (petInfoList.Count > 0)
            {
                PetInfo petInfo = petInfoList[0];
                petInfo.FightState = PetFightState.fighting;
                SetCurrentPetInfo(petInfo);
                PlayerPetManager.GetInstance().ClientCopyAddPet(petInfo);
            }
        }

        public override void ReplacePet(int petId)
        {
            _currentPetInfo.FightState = PetFightState.replaced;
            PlayerPetManager.GetInstance().ClientCopyDelPet();
            PetInfo petInfo = FindPetInfo(petId);
            petInfo.FightState = PetFightState.fighting;
            SetCurrentPetInfo(petInfo);
            PlayerPetManager.GetInstance().ClientCopyAddPet(petInfo);
            EventDispatcher.TriggerEvent(BattleUIEvents.UPDATE_PET_FIGHT_STATE);
        }

        private PetInfo FindPetInfo(int petId)
        {
            PetInfo petInfo = null;
            for (int i = 0; i < _petInfoList.Count; i++)
            {
                petInfo = _petInfoList[i];
                if (petInfo.Id == petId)
                {
                    break;
                }
            }
            return petInfo;
        }

        public override void PetDie(int petId)
        {
            PetInfo petInfo = FindPetInfo(petId);
            petInfo.FightState = PetFightState.dead;
            PlayerPetManager.GetInstance().ClientCopyDelPet();

            PetInfo nextPetInfo = GetNextIdlePet();
            if (nextPetInfo != null)
            {
                nextPetInfo.FightState = PetFightState.fighting;
                SetCurrentPetInfo(nextPetInfo);
                PlayerPetManager.GetInstance().ClientCopyAddPet(nextPetInfo);
            }

            EventDispatcher.TriggerEvent(BattleUIEvents.UPDATE_PET_FIGHT_STATE);
        }

        public PetInfo GetNextIdlePet()
        {
            for (int i = 0; i < _petInfoList.Count; i++)
            {
                PetInfo petInfo = _petInfoList[i];
                if (petInfo.FightState == PetFightState.idle)
                {
                    return petInfo;
                }
            }
            return null;
        }
    }
}
