﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public abstract class PetBattleLogic
    {
        public bool inited = false;

        protected PetData PetData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        public virtual void InitPetData()
        {
            inited = true;
            EventDispatcher.TriggerEvent(BattleUIEvents.BATTLE_PET_DATA_INITED);
        }

        public abstract PetInfo GetCurrentPetInfo();

        public abstract List<PetInfo> GetPetInfoList();

        public abstract List<PetInfo> GetInactivePetInfoList();

        public abstract void ReplacePet(int petId);

        public virtual void PetDie(int petId) { }

        public virtual void LevelCombatScene() { }

        public virtual void UpdatePetFightState() { }

        public virtual void ReponsePetDataReady(int mapId) {}
    }
}
