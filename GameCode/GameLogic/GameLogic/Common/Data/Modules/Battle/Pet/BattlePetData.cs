﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class BattlePetData
    {
        private int _currentHp = 0;
        private int _currentMaxHp = 1;

        private ClientPetBattleLogic _clientLogic;
        private ServerPetBattleLogic _serverLogic;

        private PetBattleLogic _curLogic;

        public BattlePetData()
        {
            _clientLogic = new ClientPetBattleLogic();
            _serverLogic = new ServerPetBattleLogic();

            AddListener();
        }

        public void AddListener()
        {
            EventDispatcher.AddEventListener<int>(BattleUIEvents.BATTLE_PET_RECEIVE_SERVER_DATA_READY, ReponsePetDataReady);
            EventDispatcher.AddEventListener<int>(BattleUIEvents.BATTLE_PET_HP_CHANGE, BattlePetHpChange);
            EventDispatcher.AddEventListener<int>(BattleUIEvents.BATTLE_PET_MAX_HP_CHANGE, BattlePetMaxHpChange);
        }

        private void BattlePetHpChange(int hp)
        {
            _currentHp = hp;
            EventDispatcher.TriggerEvent<float>(BattleUIEvents.BATTLE_PET_REFRESH_HP, ((float)hp / (_currentMaxHp == 0 ? 1 : _currentMaxHp)));
        }

        private void BattlePetMaxHpChange(int maxHp)
        {
            _currentMaxHp = maxHp;
        }

        private void ReponsePetDataReady(int mapId)
        {
            if (_curLogic != null)
            {
                _curLogic.ReponsePetDataReady(mapId);
            }
        }

        public void EnterCombatScene()
        {
            if (GameSceneManager.GetInstance().inCombatScene)
            {
                int curMapID = GameSceneManager.GetInstance().curMapID;
                if (map_helper.IsClientCtrl(curMapID))
                {
                    _curLogic = _clientLogic;
                    _curLogic.InitPetData();
                }
                else
                {
                    _curLogic = _serverLogic;
                }
            }
            else
            {
                _curLogic = null;
            }
        }

        public void RequestReplacePetInBattle(int petId)
        {
            _curLogic.ReplacePet(petId);
        }

        public PetInfo GetCurrentPetInfo()
        {
            return _curLogic.GetCurrentPetInfo();
        }

        public List<PetInfo> GetPetInfoList()
        {
            return _curLogic.GetPetInfoList();
        }

        public List<PetInfo> GetInactivePetInfoList()
        {
            return _curLogic.GetInactivePetInfoList();
        }

        public bool GetInit()
        {
            if (_curLogic != null)
            {
                return _curLogic.inited;
            }
            return false;
        }

        public void UpdatePetFightState()
        {
            if (_curLogic != null)
            {
                _curLogic.UpdatePetFightState();
            }
        }

        public void LevelCombatScene()
        {
            if (_curLogic != null)
            {
                _curLogic.LevelCombatScene();
            }
        }
    }
}
