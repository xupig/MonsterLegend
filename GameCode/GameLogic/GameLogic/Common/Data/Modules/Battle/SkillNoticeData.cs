﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Data;
using Common.ClientConfig;
using GameMain.GlobalManager;
using Common.Base;

namespace ModuleMainUI
{
    public class SkillNoticeData
    {
        public int Index;
        public Vector3 targetPosition;
    }
}
