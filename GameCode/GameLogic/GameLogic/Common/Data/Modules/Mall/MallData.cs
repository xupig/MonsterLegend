﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/12 21:09:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using GameData;

namespace Common.Data
{
    //某一类型的所有商城数据
    public class MallData
    {
        public Dictionary<int, market_data> _mallItemDict = new Dictionary<int, market_data>();
        public List<market_data> _mallItemList = new List<market_data>();
        public List<market_data> MallItemList
        {
            get
            {
                return _mallItemList;
            }
        }

        public void Add(market_data marketData)
        {
            _mallItemDict.Add(marketData.__id, marketData);
        }

        public void InitList()
        {
            foreach (market_data mallItem in _mallItemDict.Values)
            {
                _mallItemList.Add(mallItem);
            }
        }

        public int GetCount()
        {
            return _mallItemList.Count;
        }
    }
}
