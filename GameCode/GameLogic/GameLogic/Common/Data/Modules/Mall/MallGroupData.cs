﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/14 16:05:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using UnityEngine;

namespace Common.Data
{
    //组id相同的所有商城数据
    public class MallGroupData
    {
        private List<int> _groupData = new List<int>();

        public void Add(int itemId)
        {
            _groupData.Add(itemId);
        }

        public void Sort()
        {
            _groupData.Sort(SortByEffectiveTime);
        }
        
        private int SortByEffectiveTime(int x, int y)
        {
            market_data xMarketData = mall_helper.GetMarketDataById(x);
            market_data yMarketData = mall_helper.GetMarketDataById(y);
            int xTime = item_helper.GetEffectiveTime(xMarketData.__item_id);
            int yTime = item_helper.GetEffectiveTime(yMarketData.__item_id);
            //0代表永久有效
            if ((xTime != 0) && (yTime == 0))
            {
                return -1;
            }
            else if (xTime < yTime)
            {
                return -1;
            }
            return 1;
        }

        public market_data GetMarketDataByIndex(int index)
        {
            return mall_helper.GetMarketDataById(_groupData[index]);
        }
    }
}
