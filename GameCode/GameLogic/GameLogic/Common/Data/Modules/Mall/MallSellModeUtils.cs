﻿using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public enum MallSellMode
    {
        normal,//普通
        limitTime,//限时
        limitNum,//总量限购(抢购)
        vip,//VIP专属
        discount,//折扣
    }

    public class MallSellModeUtils
    {
        private static MallDataManager _dataManager = PlayerDataManager.Instance.MallDataManager;

        public static int CanBuy(market_data marketData)
        {
            switch ((MallSellMode)marketData.__sell_mode)
            {
                case MallSellMode.normal:
                case MallSellMode.vip:
                    if (!ReachLimitVipLevel(marketData))
                    {
                        return 56721;
                    }
                    break;
                case MallSellMode.discount:
                    break;
                case MallSellMode.limitNum:
                    if (IsLimitNumSellOut(marketData))
                    {
                        return 56726;
                    }
                    break;
                case MallSellMode.limitTime:
                    if (IsExpired(marketData))
                    {
                        return 56736;
                    }
                    break;
            }
            return 0;
        }

        public static bool ReachLimitVipLevel(market_data marketData)
        {
            if (PlayerAvatar.Player.vip_level >= marketData.__vip_level_limit)
            {
                return true;
            }
            return false;
        }

        public static bool IsLimitNumSellOut(market_data marketData)
        {
            return _dataManager.GetFlashSaleNum(marketData) == 0;
        }

        public static bool IsExpired(market_data marketData)
        {
            UInt64 endMilliseconds = mall_helper.GetEndMilliseconds(marketData);
            return endMilliseconds <= Global.Global.serverTimeStamp;
        }
    }
}
