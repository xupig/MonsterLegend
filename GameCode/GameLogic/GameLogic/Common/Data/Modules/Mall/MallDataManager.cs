﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/13 10:28:07
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using GameData;
using UnityEngine;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;

namespace Common.Data
{
    public enum MallType
    {
        /// <summary>
        /// 热销
        /// </summary>
        hot = 1,
        /// <summary>
        /// 宝石
        /// </summary>
        gem = 2,
        /// <summary>
        /// 时装
        /// </summary>
        fashion = 3,
        /// <summary>
        /// 翅膀
        /// </summary>
        wing = 4,
        /// <summary>
        /// 坐骑
        /// </summary>
        mount = 5,
        /// <summary>
        /// 宠物
        /// </summary>
        pet = 6,
        /// <summary>
        /// 打孔器
        /// </summary>
        hole = 7,
        /// <summary>
        /// 其他
        /// </summary>
        other = 8,
        /// <summary>
        /// 装备
        /// </summary>
        equip = 9,
    }

    public class MallDataManager
    {
        public const int ALL_TYPE = 0;
        public const int HOT_SELL_PAGE = 1;

        /// <summary>
        /// Key为商店物品类型
        /// Value为具体类型商店物品的数据
        /// </summary>
        private Dictionary<MallType, MallData> _mallDataDict = new Dictionary<MallType, MallData>();
        public Dictionary<MallType, MallData> MallDataDict
        {
            get
            {
                return _mallDataDict;
            }
        }

        /// <summary>
        // Key为分组的组id
        // Value为商店物品列表
        /// </summary>
        private Dictionary<int, MallGroupData> _mallGroupDict = new Dictionary<int, MallGroupData>();
        public Dictionary<int, MallGroupData> MallGroupDict
        {
            get
            {
                return _mallGroupDict;
            }
        }

        /// <summary>
        /// Key为商店物品类型
        /// Value为物品Id列表
        /// </summary>
        private Dictionary<MallType, List<int>> _mallCouponDict = new Dictionary<MallType, List<int>>();
        public Dictionary<MallType, List<int>> MallCouponDict
        {
            get
            {
                return _mallCouponDict;
            }
        }

        public Dictionary<int, int> dailyLimitDict = new Dictionary<int, int>();
        public Dictionary<int, int> totalLimitDict = new Dictionary<int, int>();
        public Dictionary<int, int> flashSaleLimitDict = new Dictionary<int, int>();

        private bool once = true;
        public void Init()
        {
            if (once)
            {
                once = false;
                ///把配置数据划分为 热销数据 商城数据 和组数据
                CreateMallDataDict();
                FillMallDataDict();
                InitMallDataDict();
                SortMallGroupDataDict();
                ///把优惠券数据改为以mallType为key的字典
                InitMallCouponDict();
                FillMallCouponDict();
            }
        }

        public void OnReloadData()
        {
            once = true;
            _mallDataDict.Clear();
            _mallCouponDict.Clear();
            _mallGroupDict.Clear();
            Init();
        }

        private void CreateMallDataDict()
        {
            foreach (MallType mallType in Enum.GetValues(typeof(MallType)))
            {
                _mallDataDict.Add(mallType, new MallData());
            }
        }

        private void FillMallDataDict()
        {
            GameDataTable<int, market_data> marketDataDict = XMLManager.market_data;
            foreach (int id in marketDataDict.Keys)
            {
                if (id == 0) { continue; }
                market_data marketData = marketDataDict[id];
                if (!mall_helper.IsOn(marketData))
                {
                    continue;
                }
                ///添加商城数据必须在组数据之前
                AddHotMallData(marketData);
                AddMallData(marketData);
                AddGroupMallData(marketData);
            }
        }

        private void InitMallDataDict()
        {
            foreach (MallData mallData in _mallDataDict.Values)
            {
                mallData.InitList();
            }
        }

        private void SortMallGroupDataDict()
        {
            foreach (MallGroupData mallGroupData in _mallGroupDict.Values)
            {
                mallGroupData.Sort();
            }
        }

        private void InitMallCouponDict()
        {
            foreach (MallType mallType in Enum.GetValues(typeof(MallType)))
            {
                _mallCouponDict.Add(mallType, new List<int>());
            }
        }

        private void FillMallCouponDict()
        {
            GameDataTable<int, market_coupon> marketCouponDataDict = XMLManager.market_coupon;
            foreach (market_coupon value in marketCouponDataDict.Values)
            {
                if (value.__use_page == ALL_TYPE)
                {
                    foreach (MallType mallType in Enum.GetValues(typeof(MallType)))
                    {
                        //标签页为0 代表所有类型的物品都可用
                        _mallCouponDict[mallType].Insert(0, value.__id);
                    }
                }
                else
                {
                    List<int> list = _mallCouponDict[(MallType)value.__use_page];
                    list.Add(value.__id);
                }
            }
        }

        private void AddHotMallData(market_data marketData)
        {
            MallData hotMallData = _mallDataDict[(MallType)HOT_SELL_PAGE];
            if (mall_helper.IsHot(marketData.__id))
            {
                hotMallData.Add(marketData);
            }
        }

        private void AddGroupMallData(market_data marketData)
        {
            if (mall_helper.IsGroup(marketData))
            {
                if (!_mallGroupDict.ContainsKey(mall_helper.GetGroupId(marketData)))
                {
                    _mallGroupDict.Add(mall_helper.GetGroupId(marketData), new MallGroupData());
                }
                MallGroupData mallGroupData = _mallGroupDict[mall_helper.GetGroupId(marketData)];
                mallGroupData.Add(marketData.__id);
            }
        }

        private void AddMallData(market_data marketData)
        {
            if (!_mallDataDict.ContainsKey((MallType)marketData.__item_page))
            {
                Debug.LogError(string.Format("商品Id:{0}的_itemPage:{1}没有在MallType中定义", marketData.__id, marketData.__item_page));
                return;
            }
            MallData mallData = _mallDataDict[(MallType) marketData.__item_page];
            if (!_mallGroupDict.ContainsKey(mall_helper.GetGroupId(marketData)))
            {
                mallData.Add(marketData);
            }
        }

        public void FillDailyLimitData(PbBuyInfoList pbBuyInfoList)
        {
            AddListIntoDict(pbBuyInfoList.buyInfoList, dailyLimitDict);
        }

        public void FillTotalLimitData(PbBuyInfoList pbBuyInfoList)
        {
            AddListIntoDict(pbBuyInfoList.buyInfoList, totalLimitDict);
        }

        public void FillFlashSaleLimitData(PbBuyInfoList pbBuyInfoList)
        {
            AddListIntoDict(pbBuyInfoList.buyInfoList, flashSaleLimitDict);
        }

        private void AddListIntoDict(List<PbBuyInfo> list, Dictionary<int, int> dict)
        {
            dict.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                PbBuyInfo pbBuyInfo = list[i];
                dict.Add(pbBuyInfo.grid_id, pbBuyInfo.buy_cnt);
            }
        }

        public void FillDailyLimitDataById(ushort id, uint num)
        {
            SetDictValue(dailyLimitDict, id, num);
        }

        public void FillTotalLimitDataById(ushort id, uint num)
        {
            SetDictValue(totalLimitDict, id, num);
        }

        public void FillFlashSaleLimitDataById(ushort id, uint num)
        {
            SetDictValue(flashSaleLimitDict, id, num);
        }

        private void SetDictValue(Dictionary<int, int> dict, ushort id, uint num)
        {
            if (dict.ContainsKey(id))
            {
                dict[id] = (int)num;
            }
            else
            {
                dict.Add(id, (int)num);
            }
        }

        public int GetFlashSaleNum(market_data marketData)
        {
            int flashSaleNum = 0;
            if (flashSaleLimitDict.ContainsKey(marketData.__id) == true)
            {
                flashSaleNum = flashSaleLimitDict[marketData.__id];
            }
            return (marketData.__sell_total_cnt_limit - flashSaleNum);
        }

        public int GetDailyCanBuyNum(market_data marketData)
        {
            int canBuyNum = int.MaxValue;
            if (marketData.__buy_daily_cnt_limit != 0)
            {
                canBuyNum = marketData.__buy_daily_cnt_limit;
            }
            if (dailyLimitDict.ContainsKey(marketData.__id))
            {
                canBuyNum -= dailyLimitDict[marketData.__id];
            }
            return canBuyNum;
        }

        public int GetTotalCanBuyNum(market_data marketData)
        {
            int canBuyNum = int.MaxValue;
            if (marketData.__buy_total_cnt_limit != 0)
            {
                canBuyNum = marketData.__buy_total_cnt_limit;
            }
            if (totalLimitDict.ContainsKey(marketData.__id))
            {
                canBuyNum -= totalLimitDict[marketData.__id];
            }
            return canBuyNum;
        }

        //返回0代表可以购买
        //返回其他值代表无法购买的中文ID
        public int CanBuy(market_data marketData)
        {
            int errorCode = CheckMallType(marketData);
            if (errorCode != 0)
            {
                return errorCode;
            }

            if (IsDailySellOut(marketData))
            {
                return 56724;
            }
            if (IsTotalSellOut(marketData))
            {
                return 56738;
            }

            return MallSellModeUtils.CanBuy(marketData);
        }

        public int CheckMallType(market_data marketData)
        {
            switch ((MallType)marketData.__item_page)
            {
                case MallType.wing:
                    int wingId = item_helper.GetWingId(marketData.__item_id);
                    if (PlayerDataManager.Instance.WingData.GetItemDataById(wingId).isOwned)
                    {
                        return 56748;
                    }
                    break;
                case MallType.fashion:
                    int fashionId = item_helper.GetFashionId(marketData.__item_id);
                    if (PlayerDataManager.Instance.FashionData.IsFashionPermanent(fashionId))
                    {
                        return 36515;
                    }
                    break;
                case MallType.mount:
                    int mountId = item_helper.GetMountId(marketData.__item_id);
                    if (PlayerDataManager.Instance.mountData.IsMountPermanent(mountId))
                    {
                        return 112518;
                    }
                    break;
            }
            return 0;
        }

        public bool IsDailySellOut(market_data marketData)
        {
            int dailyCanBuyNum = GetDailyCanBuyNum(marketData);
            return dailyCanBuyNum == 0;
        }

        public bool IsTotalSellOut(market_data marketData)
        {
            int totalCanBuyNum = GetTotalCanBuyNum(marketData);
            return totalCanBuyNum == 0;
        }
    }
}
