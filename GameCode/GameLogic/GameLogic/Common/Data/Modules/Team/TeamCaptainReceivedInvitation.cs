﻿#region 模块信息
/*==========================================
// 文件名：TeamCaptainReceivedInvitation
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Team
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/6 19:34:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TeamCaptainReceivedInvitation:TeamReceivedData
    {
        public UInt32 teamId;
        public UInt64 inviterId;
        public string inviterName;
        public UInt64 inviteeId;
        public string inviteeName;
        public byte inviteeLevel;
        public Vocation inviteeVocation;
        public Gender gender;
        public int instanceId;
    }
}
