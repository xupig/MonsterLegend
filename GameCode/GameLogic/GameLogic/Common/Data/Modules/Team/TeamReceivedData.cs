﻿#region 模块信息
/*==========================================
// 文件名：TeamReceivedData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Team
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/7 9:49:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TeamReceivedData
    {
        public UInt64 receivedTime;
    }
}
