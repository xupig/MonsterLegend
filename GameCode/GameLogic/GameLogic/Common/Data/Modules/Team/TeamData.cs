﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/19 14:24:11
 * 描述说明：
 * *******************************************************/


using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

using Common.Structs;
using ModuleMainUI;
namespace Common.Data
{
    public class TeamData
    {
        public TeamData()
        {
            interval = uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.match_lasting_time));
        }

        public List<PbGroupRecordItem> groupRecordList = new List<PbGroupRecordItem>();

        public Dictionary<uint, uint> requiredDict = new Dictionary<uint, uint>();


        public List<PbAutoTeamInfo> autoTeamInfoList = new List<PbAutoTeamInfo>();

        public PbAutoTeamPlayerTake takeInfo;

        public PbAutoTeamPlayerMatch matchInfo;

        public bool hasRequestNearPlayerData;
        public bool hasNearPlayerDataResp;
        private uint interval = 0;
        private uint requestTime = 0;
        private bool isDialog = false;

        public void OnStep()
        {
            if (GameSceneManager.GetInstance().CanShowNotice() == false || isDialog)
            {
                return;
            }
            if (auto_team_game_id != 0 && auto_team_is_start == 1 && auto_team_time!=0 && _captainId == PlayerAvatar.Player.dbid)
            {
                uint currentTime = Global.Global.serverTimeStampSecond;
                if(currentTime - requestTime > 5)
                {
                    if (currentTime - auto_team_time > interval)
                    {
                        isDialog = true;
                        team_detail detail = team_helper.GetTeamDetail(Convert.ToInt32(auto_team_game_id));
                        string name = team_helper.GetDetailInstanceName(detail);
                  //ari MessageBox.Show(true, string.Empty, (6016141).ToLanguage(name), OnConfirm, OnCancel);
                    }
                }
            }
        }

        private void OnCancel()
        {
            isDialog = false;
            requestTime = Global.Global.serverTimeStampSecond;
            TeamManager.Instance.ClearAutoMatch();
        }

        private void OnConfirm()
        {
            isDialog = false;
            requestTime = Global.Global.serverTimeStampSecond;
            TeamManager.Instance.CaptainAutoMatch(Convert.ToInt32(auto_team_game_id), Convert.ToInt32(auto_team_min_level), Convert.ToInt32(auto_team_max_level), auto_team_content, Convert.ToInt32(auto_review));
        }

        public string auto_team_content { get; set; }
        public uint auto_team_game_id { get; set; }
        public uint auto_team_is_start { get; set; }
        public uint auto_team_max_level { get; set; }
        public uint auto_team_min_level { get; set; }
        public uint auto_team_time { get; set; }

        public bool auto_review { get; set; }


        //private Dictionary<int>
        private UInt64 _captainId;
        public UInt64 CaptainId
        {
            get
            {
                return _captainId;
            }
            set
            {
                _captainId = value;              
            }
        }

        public bool teamGuideFlag = false;
        private UInt32 _teamId;
        public UInt32 TeamId
        {
            get 
            {
                return _teamId;
            }
            set
            {
                if (_teamId != value)
                {
                    _teamId = value;
                    if(_teamId!=0)
                    {
                        teamGuideFlag = true;
                        //ari DramaManager.GetInstance().StoreTriggerCount(TeamTaskView.TEAM_GUIDE_ID);
                    }
                   
                    EventDispatcher.TriggerEvent(TeamEvent.TEAM_ID_CHANGED);
                }
            }
        }

        public Dictionary<UInt64, UInt64> inviteDict = new Dictionary<ulong,ulong>();
        public Dictionary<UInt64, UInt64> applyDict = new Dictionary<ulong,ulong>();


        public byte State
        {
            get;
            set;
        }


        public Dictionary<UInt64, PbTeamMember> TeammateDic = new Dictionary<ulong, PbTeamMember>();

        public Dictionary<UInt64, bool> applyDic = new Dictionary<ulong, bool>();

        public Dictionary<UInt64, bool> inviteDic = new Dictionary<ulong, bool>();

        public Dictionary<UInt64, Dictionary<UInt64, bool>> captainInviteDic = new Dictionary<ulong, Dictionary<ulong, bool>>();

        public List<TeamReceivedInvitation> InvitationList = new List<TeamReceivedInvitation>();

        public List<TeamReceivedData> CaptainList = new List<TeamReceivedData>();

       // public List<TeamReceivedData> InvitationList = new List<TeamReceivedData>();

       // public List<TeamReceivedData> CaptainInvitationList = new List<TeamReceivedData>();

       // public Dictionary<UInt64, PbNearPlayer> NearPlayerDic = new Dictionary<UInt64, PbNearPlayer>();
        public List<PbNearPlayer> NearPlayerList = new List<PbNearPlayer>();
        public int nearPlayerTotalPage = 1;
        public int nearPlayerCurrentPage = 0; //初始化为0，0表示未初始化，其他表示当前页码

        public List<PbNearTeam> NearTeamList = new List<PbNearTeam>();
        public int nearTeamTotalPage = 1;
        public int nearTeamCurrentPage = 1;

        public List<PbNearTeam> FriendTeamList = new List<PbNearTeam>();
        public int friendTeamTotalPage = 1;
        public int friendTeamCurrentPage = 1;

        public List<PbNearTeam> GuildTeamList = new List<PbNearTeam>();
        public int guildTeamTotalPage = 1;
        public int guildTeamCurrentPage = 1;

        public UInt32 GetCaptainEntityId()
        {
            if (CaptainId != 0 && TeammateDic.ContainsKey(CaptainId))
            {
                return TeammateDic[CaptainId].eid;
            }
            return 0;
        }

        public bool IsCaptain()
        {
            return PlayerAvatar.Player.dbid == CaptainId;
        }

        public bool IsCaptain(ulong dbid)
        {
            return dbid == CaptainId;
        }

        public bool IsCaptain(uint id)
        {
            return id == GetCaptainEntityId();
        }

        public bool IsInTeam()
        {
            return TeammateDic.Count > 0;
        }

        public bool IsOnline(uint eid)
        {
			foreach (PbTeamMember info in TeammateDic.Values)
			{
				if (info.eid == eid)
				{
					return info.online == 1;
				}
			}
            return false;
        }

        public bool IsCaptainInSameMap()
        {
            return IsTeammateInSameMap(CaptainId);
        }

        public Vector2 GetCaptainPosition()
        {
            if (CaptainId != 0 && TeammateDic.ContainsKey(CaptainId))
            {
                PbTeamMember member = TeammateDic[CaptainId];
                return new Vector2(member.pos_x * 0.01f, member.pos_y * 0.01f);
            }
            return Vector2.zero;
        }

        public void UpdateMemberOnlineByEntityId(uint eid, uint online)
        {
            foreach (PbTeamMember info in TeammateDic.Values)
            {
                if (info.eid == eid)
                {
                    info.online = online;
                }
            }
        }

        public void UpdateMemberOnline(ulong id, uint online)
        {
            if (TeammateDic.ContainsKey(id))
            {
                PbTeamMember info = TeammateDic[id];
                info.online = online;
            }
        }

        public void UpdateMemberHp(ulong id, uint hp, uint maxHp)
        {
            if (TeammateDic.ContainsKey(id))
            {
                PbTeamMember info = TeammateDic[id];
                info.hp = hp;
                info.max_hp = maxHp;
            }
        }

        public bool IsTeammateInSameMap(ulong dbid)
        {
            if (dbid != 0 && TeammateDic.ContainsKey(dbid))
            {
                PbTeamMember member = TeammateDic[dbid];
                return member.map_id == GameSceneManager.GetInstance().curMapID && PlayerAvatar.Player.curLine == member.map_line;
            }
            return false;
        }

        public int GetCaptainMapId()
        {
            if (CaptainId != 0 && TeammateDic.ContainsKey(CaptainId))
            {
                PbTeamMember member = TeammateDic[CaptainId];
                return (int)member.map_id;
            }
            return 0;
        }

        public void UpdateMemberPos(ulong id, uint posX, uint posY)
        {
            if (TeammateDic.ContainsKey(id))
            {
                PbTeamMember info = TeammateDic[id];
                info.pos_x = posX;
                info.pos_y = posY;
            }
        }

        public bool ContainInNearList(UInt64 dbId)
        {
            int index = -1;
            for(int i=0;i<NearPlayerList.Count;i++)
            {
                if(NearPlayerList[i].dbid == dbId)
                {
                    index = i;
                    break;
                }
            }
            if(index!=-1)
            {
                NearPlayerList.RemoveAt(index);
            }
            return index != -1;
        }

        public TeamReceivedApplicant GetApplication(UInt64 applicantId)
        {
            TeamReceivedApplicant _toRemoveData = null;
            if(applyDic.ContainsKey(applicantId))
            {
                int count = CaptainList.Count;
                for (int i =0;i<count;i++)
                {
                    TeamReceivedData item = CaptainList[i];
                    if (item is TeamReceivedApplicant)
                    {
                        TeamReceivedApplicant data = (item as TeamReceivedApplicant);
                        if(data.applicantId == applicantId)
                        {
                            _toRemoveData = data;
                            break;
                        }
                    }
                }
            }
            if (_toRemoveData!=null)
            {
                CaptainList.Remove(_toRemoveData);
            }
            else
            {
                applyDic.Add(applicantId,true);
                _toRemoveData = new TeamReceivedApplicant();
            }
            return _toRemoveData;
        }

        public TeamReceivedInvitation GetInvitation(UInt64 inviterId)
        {
            TeamReceivedInvitation _toRemoveData = null;
            if (inviteDic.ContainsKey(inviterId))
            {
                int count = InvitationList.Count;
                for (int i = 0; i < count;i++ )
                {
                    TeamReceivedInvitation data = InvitationList[i];
                    if (data.inviterId == inviterId)
                    {
                        _toRemoveData = data;
                        break;
                    }
                }
            }
            if (_toRemoveData != null)
            {
                InvitationList.Remove(_toRemoveData);
            }
            else
            {
                inviteDic.Add(inviterId,true);
                _toRemoveData = new TeamReceivedInvitation();
            }
            return _toRemoveData;
        }

        public TeamCaptainReceivedInvitation GetCapationInvitation(UInt64 inviterId,UInt64 inviteeId)
        {
            TeamCaptainReceivedInvitation _toRemoveData = null;
            if (captainInviteDic.ContainsKey(inviterId) && captainInviteDic[inviterId].ContainsKey(inviteeId))
            {
                int count = CaptainList.Count;
                for (int i = 0; i < count; i++)
                {
                    TeamReceivedData item = CaptainList[i];
                    if(item is TeamCaptainReceivedInvitation)
                    {
                        TeamCaptainReceivedInvitation data = item as TeamCaptainReceivedInvitation;
                        if (data.inviterId == inviterId && data.inviteeId == inviteeId)
                        {
                            _toRemoveData = data;
                            break;
                        }
                    } 
                }
            }
            if (_toRemoveData != null)
            {
                CaptainList.Remove(_toRemoveData);
            }
            else
            {
                if(captainInviteDic.ContainsKey(inviterId) == false)
                {
                    captainInviteDic.Add(inviterId,new Dictionary<ulong,bool>());
                }
                captainInviteDic[inviterId].Add(inviteeId, true);
                _toRemoveData = new TeamCaptainReceivedInvitation();
            }
            return _toRemoveData;
        }

        public void ChangePlayerAutoFightType()
        {
            if (PlayerAvatar.Player.dbid == CaptainId && PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                TeamManager.Instance.CancelFollowCaptain();
                int mapId = GameSceneManager.GetInstance().curMapID;
                if (map_helper.GetPlayerAI(mapId) != 0)
                {
                    PlayerAutoFightManager.GetInstance().Start();
                }
                else
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                }
            }
        }

        public int GetPlayerDutyIconByEntityId(uint entityId)
        {
            foreach (var pair in TeammateDic)
            {
                if (pair.Value.eid == entityId)
                {
                    return skill_helper.GetTeamDutyIconId((Vocation)pair.Value.vocation,(int)pair.Value.spell_proficient);
                }
            }
            return 0;
        }
    }
}
