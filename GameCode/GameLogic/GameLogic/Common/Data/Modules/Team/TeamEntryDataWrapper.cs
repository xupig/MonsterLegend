﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TeamEntryDataWrapper
    {
        public enum TeamEntryType
        {
            immediacy = 0,
            match = 1,
            playAgain = 2,
        }

        public TeamEntryType Type;
        public uint instId;
        public PbPlayAgain PbPlayAgain;

        public TeamEntryDataWrapper(byte type)
        {
            Type = (TeamEntryType)type;
        }

        public TeamEntryDataWrapper(TeamEntryType type)
        {
            Type = type;
        }
    }
}
