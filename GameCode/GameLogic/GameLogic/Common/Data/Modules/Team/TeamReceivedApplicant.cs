﻿#region 模块信息
/*==========================================
// 文件名：TeamApplyInfo
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Team
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/6 17:43:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TeamReceivedApplicant : TeamReceivedData
    {
        public UInt32 teamId ;
        public UInt64 applicantId;
        public string applicantName;
        public byte applicantLevel;
        public Vocation applicantVocation;
        public Gender gender;
    }
}
