﻿using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using GameData;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.Base;
using GameMain.Entities;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.Structs;

namespace Common.Data
{
    public class PetData
    {
        public const int SmallPetExpItemId = 29000;
        public const int MiddlePetExpItemId = 29001;
        public const int LargePetExpItemId = 29002;
        private int[] PetExpItemIdArray = new int[] { SmallPetExpItemId, MiddlePetExpItemId, LargePetExpItemId };

        public PetData()
        {
            EventDispatcher.AddEventListener(PetEvents.UpdatePetInfo, OnTriggerMainTownPetIconCheck);
            EventDispatcher.AddEventListener(PetEvents.AddExp, OnTriggerMainTownPetIconCheck);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, OnTriggerMainTownPetIconCheck);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
            OnTriggerMainTownPetIconCheck();
        }

        private void OnItemUpdate(BagType bagType, uint pos)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo((int)pos);
                if (itemInfo != null)
                {
                    if (itemInfo.Type == BagItemType.PetStone || itemInfo.Type == BagItemType.PetCream
                        || itemInfo.Type == BagItemType.PetFragment || itemInfo.Type == BagItemType.PetExperience)
                    {
                        OnTriggerMainTownPetIconCheck();
                    }
                }
            }
        }

        private void OnTriggerMainTownPetIconCheck()
        {
            if (NeedShowPetIconEffect())
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.pet);
                return;
            }
            PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.pet);
        }

        private bool NeedShowPetIconEffect()
        {
            if (HaveEmptyFightingHolder() && (IdlePetList.Count > 0))
            {
                return true;
            }
            if (HaveEmptyAssistHolder() && (IdlePetList.Count > 0))
            {
                return true;
            }
            foreach (PetInfo petInfo in _petDict.Values)
            {
                if ((PlayerAvatar.Player.level <= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.pet_remind_limit)))
                    && (petInfo.CanLevelUp()))
                {
                    return true;
                }
                if (petInfo.CanStarUp()
                    || petInfo.CanQualityUp()
                    || petInfo.CanWash())
                {
                    return true;
                }
            }
            return false;
        }

        private Dictionary<int, PetInfo> _petDict = new Dictionary<int, PetInfo>();
        public Dictionary<int, PetInfo> PetDict { get{ return _petDict; } }

        public PetInfo FightingPet
        {
            get
            {
                foreach (PetInfo petInfo in _petDict.Values)
                {
                    if ((petInfo.State == PetState.fighting)
                        && (petInfo.FightState == PetFightState.fighting))
                    {
                        return petInfo;
                    }
                }
                return null;
            }
        }

        private List<PetInfo> GetPetInfoListByPetState(PetState petState)
        {
            List<PetInfo> petInfoList = new List<PetInfo>();
            foreach (PetInfo petInfo in _petDict.Values)
            {
                if (petInfo.State == petState)
                {
                    petInfoList.Add(petInfo);
                }
            }
            return petInfoList;
        }

        private int SortByPosition(PetInfo x, PetInfo y) { return x.Position < y.Position ? -1 : 1; }

        public List<PetInfo> FightingPetList 
        { 
            get 
            {
                List<PetInfo> petInfoList = GetPetInfoListByPetState(PetState.fighting);
                petInfoList.Sort(SortByPosition);
                return petInfoList;
            }
        }

        public List<PetInfo> AssistPetList 
        { 
            get 
            {
                List<PetInfo> petInfoList = GetPetInfoListByPetState(PetState.assist);
                petInfoList.Sort(SortByPosition);
                return petInfoList;
            } 
        }
        public List<PetInfo> IdlePetList { get { return GetPetInfoListByPetState(PetState.idle); } }

        public void FillPetList(PbPetInfoList petList)
        {
            List<PbPetInfo> pbPetInfoList = petList.petInfoList;
            for (int i = 0; i < pbPetInfoList.Count; i++)
            {
                UpdatePetInfo(pbPetInfoList[i]);
            }
        }

        public void UpdatePetInfo(PbPetInfo pbPetInfo)
        {
            PetInfo petInfo = GetPetInfo(pbPetInfo.pet_id);
            petInfo.FillPetInfo(pbPetInfo);
        }

        private PetInfo GetPetInfo(int petId)
        {
            PetInfo petInfo = null;
            if (_petDict.ContainsKey(petId))
            {
                petInfo = _petDict[petId];
            }
            else
            {
                petInfo = new PetInfo(petId);
                _petDict.Add(petId, petInfo);
            }
            return petInfo;
        }

        public bool HaveCanCallPet()
        {
            return GetCanCallPetId() != -1;
        }

        public int GetCanCallPetId()
        {
            foreach (pet pet in XMLManager.pet.Values)
            {
                if (!_petDict.ContainsKey(pet.__id) && CanCall(pet.__id) && pet_helper.GetCanShow(pet.__id))
                {
                    return pet.__id;
                }
            }
            return -1;
        }

        public bool CanCall(int id)
        {
            ItemData itemData = pet_helper.GetItemCost(id);
            return PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemData.Id) >= itemData.StackCount;
        }

        public void SortPetListByFightValue(List<PetInfo> petInfoList)
        {
            for (int i = 0; i < petInfoList.Count; i++)
            {
                PetInfo petInfo = petInfoList[i];
                petInfo.CaculateFightValue();
            }
            petInfoList.Sort(PetInfo.SortByFightValue);
        }

        public List<PetInfo> GetSortedIdlePetInfoList()
        {
            List<PetInfo> idlePetInfoList = IdlePetList;
            SortPetListByFightValue(idlePetInfoList);
            return idlePetInfoList;
        }

        public bool IsFightPositionExist(int position)
        {
            foreach (PetInfo petInfo in FightingPetList)
            {
                if (petInfo.Position == position)
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdatePetFightState(PbPetInfoActiveStateList petFightStateList)
        {
            for (int i = 0; i < petFightStateList.petInfoActiveStateList.Count; i++)
            {
                PbPetInfoActiveState pbPetInfoActiveState = petFightStateList.petInfoActiveStateList[i];
                PetInfo petInfo = GetPetInfo(pbPetInfoActiveState.pet_id);
                petInfo.UpdateFightState((PetFightState)pbPetInfoActiveState.active_state);
            }
        }

        public List<int> GetActiveSkillIdList(int id)
        {
            return GetPetInfo(id).GetActiveSkillIdList();
        }

        public List<int> GetBuffIdList(int id)
        {
            return GetPetInfo(id).GetBuffIdList();
        }

        public int GetLeaderSkillId()
        {
            int id = -1;
            List<PetInfo> petInfoList = AssistPetList;
            if(petInfoList.Count > 0 && petInfoList[0].Position == 0)
            {
                id = pet_quality_helper.GetLeaderSkill(petInfoList[0].Id, petInfoList[0].Quality)[0];
            }
            return id;
        }

        public bool HavePetExpItem()
        {
            for (int i = 0; i < PetExpItemIdArray.Length;i++)
            {
                int num = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(PetExpItemIdArray[i]);
                if (num > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool HaveEmptyFightingHolder()
        {
            //Reyes
            return false;
            //return pet_hole_helper.GetCanFigtingPetNum() > FightingPetList.Count;
        }

        public bool HaveEmptyAssistHolder()
        {
            return pet_hole_helper.GetCanAssistPetNum() > AssistPetList.Count;
        }

        public bool HaveCanStrengthPet()
        {
            foreach (PetInfo petInfo in _petDict.Values)
            {
                if (petInfo.CanStrength())
                {
                    return true;
                }
            }
            return false;
        }

        public void ShowPetGetPanel(PbCreatePetInfo pbCreatePetInfo)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.PetGet, pbCreatePetInfo);
        }

        public void ResponsePetFight(PbPetInfoCombatList pbPetInfoCombatList)
        {
            List<PbPetInfoCombat> pbPetInfoList = pbPetInfoCombatList.petInfoCombatList;
            for (int i = 0; i < pbPetInfoList.Count; i++)
            {
                PetInfo petInfo = GetPetInfo(pbPetInfoList[i].pet_id);
                petInfo.FillPetInfoFromIdleToFight(pbPetInfoList[i]);
            }
            EventDispatcher.TriggerEvent(PetEvents.ReceiveIdleToFight);
        }

        public void ResponsePetIdle(PbPetInfoIdleList pbPetInfoIdleList)
        {
            List<PbPetInfoIdle> pbPetInfoList = pbPetInfoIdleList.petInfoIdleList;
            for (int i = 0; i < pbPetInfoList.Count; i++)
            {
                PetInfo petInfo = GetPetInfo(pbPetInfoList[i].pet_id);
                petInfo.FillPetInfoFromFightToIdle(pbPetInfoList[i]);
            }
            EventDispatcher.TriggerEvent(PetEvents.ReceiveFightToIdle);
        }

        public bool SecondFightingHolderCanFight()
        {
            if((IdlePetList.Count > 0)
                &&(FightingPetList.Count == 1)
                    && (pet_hole_helper.GetCanFigtingPetNum() >= 2))
            {
                return true;
            }
            return false;
        }

        public bool FirstAssistHolderCanAssist()
        {
            if ((IdlePetList.Count > 0)
                && (AssistPetList.Count == 0)
                    && (pet_hole_helper.GetCanAssistPetNum() >= 1))
            {
                return true;
            }
            return false;
        }

        public void ResponsePetInfoAddExp(PbPetInfoAddExp pbPetInfoAddExp)
        {
            PetInfo petInfo = GetPetInfo(pbPetInfoAddExp.pet_id);
            petInfo.FillAddExp(pbPetInfoAddExp);
        }

        public Dictionary<int, float> GetFightAttributeDataDict(int petId)
        {
            PetInfo petInfo = _petDict[petId];
            return GetFightAttributeDataDict(petInfo.Id, petInfo.Level, petInfo.Star, petInfo.Quality, petInfo.PetSkillIdList, petInfo.StarAttributeList, false, petInfo.State, petInfo.Position);
        }

        /// <summary>
        /// 获取宠物战斗属性
        /// </summary>
        public Dictionary<int, float> GetFightAttributeDataDict(int petId, int level, int star, int quality, List<int> petSkillIdList, List<RoleAttributeData> starAttributeList, bool isRaw, PetState petState, int position)
        {
            Dictionary<int, float> fightDict = new Dictionary<int, float>();
            List<RoleAttributeData> attributeDataList = GetAttributeDataList(petId, level, star, quality, petSkillIdList, starAttributeList, isRaw, petState, position);
            foreach (RoleAttributeData item in attributeDataList)
            {
                fightDict.Add(item.attriId, item.AttriValue);
            }
            return fight_force_helper.ChangeAttributeConvert(fightDict, Vocation.Warrior);
        }

        public List<RoleAttributeData> GetAttributeDataList(PetInfo petInfo, bool isRaw = false)
        {
            return GetAttributeDataList(petInfo.Id, petInfo.Level, petInfo.Star, petInfo.Quality, petInfo.PetSkillIdList, petInfo.StarAttributeList, isRaw, petInfo.State, petInfo.Position);
        }

        /// <summary>
        /// 获取宠物最终的属性
        /// </summary>
        //isRaw为true 不计算助战宠物属性 不计算助战属性百分比
        public List<RoleAttributeData> GetAttributeDataList(int petId, int level, int star, int quality, List<int> petSkillIdList, List<RoleAttributeData> starAttributeList, bool isRaw = true, PetState petState = PetState.invalid, int position = -1)
        {
            List<RoleAttributeData> attributeDataList = new List<RoleAttributeData>();
            pet_quality petQuality = pet_quality_helper.GetPetQualityConf(petId, quality);
            float[] paramArray = pet_star_helper.GetGlobalParamArray();
            Dictionary<int, float> skillAttriAdditionDict = pet_star_helper.GetSkillAdditionDict(level, star, quality, petSkillIdList);
            Dictionary<int, float> assistAttriAdditionDict = null;
            if (!isRaw && petState == PetState.fighting)
            {
                assistAttriAdditionDict = GetAssistAttriAdditionDict();
            }
            for (int i = 0; i < starAttributeList.Count; i++)
            {
                RoleAttributeData starAttibute = starAttributeList[i];
                int attriId = starAttibute.attriId;
                RoleAttributeData attributeData = new RoleAttributeData(attriId, GetAttributeValue(attriId, paramArray, level, starAttibute.AttriValue,
                    petQuality, skillAttriAdditionDict, assistAttriAdditionDict, isRaw, petState, position));
                attributeDataList.Add(attributeData);
            }
            return attributeDataList;
        }

        private Dictionary<int, float> GetAssistAttriAdditionDict()
        {
            Dictionary<int, float> assistAttriAdditionDict = new Dictionary<int, float>();
            List<RoleAttributeData> attributeDataList = PetUtils.GetSumAttributeDataList(AssistPetList);
            for (int i = 0; i < attributeDataList.Count; i++)
            {
                RoleAttributeData attributeData = attributeDataList[i];
                assistAttriAdditionDict.Add(attributeData.attriId, attributeData.AttriValue);
            }
            return assistAttriAdditionDict;
        }

        private int GetAttributeValue(int attriId, float[] paramArray, int level, int starAttributeValue, pet_quality petQuality,
            Dictionary<int, float> skillAttriAdditionDict, Dictionary<int, float> assistAttriAdditionDict, bool isRaw, PetState petState, int position)
        {
            float attriValue = 0;
            string strArriId = attriId.ToString();
            switch ((AttriId)attriId)
            {
                case AttriId.maxHp:
                    attriValue = paramArray[0] + starAttributeValue * paramArray[1];
                    break;
                case AttriId.damageUpbound:
                    attriValue = paramArray[2] + starAttributeValue * paramArray[3];
                    break;
                case AttriId.damageLowbound:
                    attriValue = paramArray[4] + starAttributeValue * paramArray[5];
                    break;
                case AttriId.phyDefense:
                    attriValue = paramArray[6] + starAttributeValue * paramArray[7];
                    break;
                case AttriId.magDefense:
                    attriValue = paramArray[8] + starAttributeValue * paramArray[9];
                    break;
                case AttriId.crit:
                    attriValue = paramArray[10] + starAttributeValue * paramArray[11];
                    break;
                default:
                    Debug.LogError("unknow attriId:" + attriId);
                    break;
            }
            attriValue = attriValue * level * (1 + (float)pet_quality_helper.GetQualityAttributeValue(petQuality, strArriId) / 10000);
            if (skillAttriAdditionDict != null && skillAttriAdditionDict.ContainsKey(attriId))
            {
                attriValue += skillAttriAdditionDict[attriId];
            }
            if (assistAttriAdditionDict != null && assistAttriAdditionDict.ContainsKey(attriId))
            {
                attriValue += assistAttriAdditionDict[attriId];
            }
            if (petState == PetState.assist && !isRaw)
            {
                float rate = pet_hole_helper.GetConvertRate(pet_hole_helper.PET_TYPE_ASSIST, position + 1);
                attriValue *= rate;
            }
            return Mathf.CeilToInt(attriValue);
        }

        public void ResponsePetRefresh(PbPetInfoRefresh pbPetInfoRefresh)
        {
            PetInfo petInfo = GetPetInfo(pbPetInfoRefresh.pet_id);
            petInfo.FillPetRefresh(pbPetInfoRefresh);
            EventDispatcher.TriggerEvent<PetInfo, PbPetInfoRefresh>(PetEvents.ResponsePetWash, petInfo, pbPetInfoRefresh);
        }

        public void ResponsePetRefreshConfirm(PbPetInfoRefreshConfirm pbPetInfoRefreshConfirm)
        {
            bool isConfirm = pbPetInfoRefreshConfirm.is_confirm == 1;
            PetInfo petInfo = GetPetInfo(pbPetInfoRefreshConfirm.pet_id);
            if (isConfirm)
            {
                petInfo.FillPetRefreshConfirm(pbPetInfoRefreshConfirm);
            }
            EventDispatcher.TriggerEvent<PetInfo, PbPetInfoRefreshConfirm>(PetEvents.ResponsePetWashConfirm, petInfo, pbPetInfoRefreshConfirm);
        }

        public void OnAddStatisticsDamage(int id, int damageType, float value)
        {
            GetPetInfo(id).OnAddStatisticsDamage(damageType, value);
        }
    }
}
