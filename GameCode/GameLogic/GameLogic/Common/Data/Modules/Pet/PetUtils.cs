﻿using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PetUtils
    {
        public static List<RoleAttributeData> GetSumAttributeDataList(List<PetInfo> petInfoList)
        {
            List<List<RoleAttributeData>> totalAttributeList = new List<List<RoleAttributeData>>();
            for (int i = 0; i < petInfoList.Count; i++)
            {
                List<RoleAttributeData> attributeDataList = PlayerDataManager.Instance.PetData.GetAttributeDataList(petInfoList[i], false);
                totalAttributeList.Add(attributeDataList);
            }
            return SumAttriList(totalAttributeList);
        }

        public static List<RoleAttributeData> SumAttriList(List<List<RoleAttributeData>> totalAttributeList)
        {
            List<RoleAttributeData> attributeList = new List<RoleAttributeData>();
            Dictionary<int, RoleAttributeData> attributeDict = new Dictionary<int, RoleAttributeData>();
            for (int i = 0; i < totalAttributeList.Count; i++)
            {
                List<RoleAttributeData> itemList = totalAttributeList[i];
                for (int j = 0; j < itemList.Count; j++)
                {
                    RoleAttributeData item = itemList[j];
                    if (attributeDict.ContainsKey(item.attriId))
                    {
                        attributeDict[item.attriId].AttriValue += item.AttriValue;
                    }
                    else
                    {
                        attributeDict.Add(item.attriId, item);
                        attributeList.Add(item);
                    }
                }
            }
            return attributeList;
        }
    }
}
