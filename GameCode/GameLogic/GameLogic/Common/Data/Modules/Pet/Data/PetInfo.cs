﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 19:20:34
// 描述说明：
// 其他：
//==========================================*/
#endregion


using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace Common.Data
{
    public class PetInfo
    {
        public const int InvalidLevel = -1;
        public const int InvalidStar = -1;
        public const int InvalidQuality = -1;

        public int Id { get; set; }
        public int Level = InvalidLevel;
        public int Exp = 0;
        public PetState State = PetState.invalid;
        public int Star = InvalidStar;
        public int Quality = InvalidQuality;
        public int Position { get; set; }
        public PetFightState FightState { get; set; }
        public List<PbPetSkill> PetSkillList { get; set; }
        public List<RoleAttributeData> _starAttributeList = new List<RoleAttributeData>();
        public List<RoleAttributeData> StarAttributeList
        {
            get
            {
                if (_starAttributeList.Count == 0)
                {
                    return pet_star_helper.GetPetStarAttributeDataList(Id, Star);
                }
                return _starAttributeList;
            }
        }

        public void SetStarAttributeList(List<PbPetStarAttri> pbList)
        {
            _starAttributeList.Clear();
            if (pbList == null || pbList.Count == 0)
            {
                return;
            }
            for (int i = 0; i < pbList.Count; i++)
            {
                RoleAttributeData roleAttributeData = new RoleAttributeData(pbList[i].attri_id, pbList[i].attri_value);
                _starAttributeList.Add(roleAttributeData);
            }

        }

        private Dictionary<int, float> _statisticsDamageDict = new Dictionary<int, float>();
        public void OnAddStatisticsDamage(int damageType, float value)
        {
            if (_statisticsDamageDict.ContainsKey(damageType))
            {
                _statisticsDamageDict[damageType] += value;
            }
            else
            {
                _statisticsDamageDict.Add(damageType, value);
            }
        }

        public float GetStatisticDamage(int damageType)
        {
            if (_statisticsDamageDict.ContainsKey(damageType))
            {
                return _statisticsDamageDict[damageType];
            }
            return 0;
        }

        public int WashLevel;
        public int WashExp;
        public int TempFightValue;
        public List<int> PetSkillIdList
        {
            get
            {
                List<int> skillIdList = new List<int>();
                for (int i = 0; i < PetSkillList.Count; i++)
                {
                    skillIdList.Add(PetSkillList[i].pet_skill_id);
                }
                return skillIdList;
            }
        }

        private UInt64 _reviveTime;
        public int IndexForSort;

        public PetInfo(int petId)
        {
            this.Id = petId;
        }

        public void FillPetInfo(PbPetInfo pbPetInfo)
        {
            Id = pbPetInfo.pet_id;
            bool isStarUp = false;
            bool isQualityUp = false;
            if (pbPetInfo.star > Star && Star != InvalidStar)
            {
                isStarUp = true;
            }
            if (pbPetInfo.quality > Quality && Quality != InvalidQuality)
            {
                isQualityUp = true;
            }
            Level = pbPetInfo.level;
            Exp = pbPetInfo.exp;
            FillPetInfoStateAndPosition(pbPetInfo.combat_state);
            Star = pbPetInfo.star;
            Quality = pbPetInfo.quality;
            FightState = (PetFightState)pbPetInfo.active_state;
            PetSkillList = pbPetInfo.petSkillList;
            //for (int i = 0; i < PetSkillList.Count; i++)
            //{
            //    Debug.LogError("index:" + PetSkillList[i].index + " skillId" + PetSkillList[i].pet_skill_id);
            //}
            WashLevel = pbPetInfo.refresh_level;
            WashExp = pbPetInfo.refresh_exp;
            SetStarAttributeList(pbPetInfo.petStarAttriList);
            Print();
            if (isStarUp)
            {
                EventDispatcher.TriggerEvent<PetInfo>(PetEvents.StarUp, this);
            }
            if (isQualityUp)
            {
                EventDispatcher.TriggerEvent<PetInfo>(PetEvents.QualityUp, this);
            }
        }

        private void FillPetInfoStateAndPosition(int combatState)
        {
            if (combatState == (int)PetState.idle)
            {
                State = PetState.idle;
                Position = 0;
            }
            else if (combatState <= 10)
            {
                State = PetState.fighting;
                Position = combatState - 1;
            }
            else
            {
                State = PetState.assist;
                Position = combatState - 11;
            }
        }

        public PetInfo Copy()
        {
            PetInfo copyPetInfo = new PetInfo(Id);
            copyPetInfo.Level = Level;
            copyPetInfo.Exp = Exp;
            copyPetInfo.State = State;
            copyPetInfo.Star = Star;
            copyPetInfo.Quality = Quality;
            copyPetInfo.Position = Position;
            copyPetInfo.FightState = FightState;
            //TODO
            CopyPetSkillList(copyPetInfo);
            return copyPetInfo;
        }

        private void CopyPetSkillList(PetInfo copyPetInfo)
        {
            copyPetInfo.PetSkillList = new List<PbPetSkill>();
            for (int i = 0; i < PetSkillList.Count; i++)
            {
                PbPetSkill pbPetSkill = new PbPetSkill();
                pbPetSkill.index = PetSkillList[i].index;
                pbPetSkill.pet_skill_id = PetSkillList[i].pet_skill_id;
                copyPetInfo.PetSkillList.Add(pbPetSkill);
            }
        }

        public List<int> GetActiveSkillIdList()
        {
            List<int> activeSkillIdList = new List<int>();
            if (PetSkillList != null)
            {
                for (int i = 0; i < PetSkillList.Count; i++)
                {
                    PbPetSkill petSkill = PetSkillList[i];
                    if (pet_skill_helper.IsActiveSkill(petSkill.pet_skill_id))
                    {
                        activeSkillIdList.Add(pet_skill_helper.GetActiveSkillId(petSkill.pet_skill_id));
                    }
                }
            }
            return activeSkillIdList;
        }

        public List<int> GetBuffIdList()
        {
            List<int> buffIdList = new List<int>();
            if (PetSkillList != null)
            {
                for (int i = 0; i < PetSkillList.Count; i++)
                {
                    PbPetSkill petSkill = PetSkillList[i];
                    if (pet_skill_helper.IsBuff(petSkill.pet_skill_id))
                    {
                        buffIdList.Add(pet_skill_helper.GetBuffId(petSkill.pet_skill_id));
                    }
                }
            }
            return buffIdList;
        }

        public static void PrintList(List<PetInfo> petInfoList)
        {
            for (int i = 0; i < petInfoList.Count; i++)
            {
                Debug.LogError(string.Format("id:{0}  fightState:{1}", petInfoList[i].Id, petInfoList[i].FightState));
            }
        }

        public void Print()
        {
            //Debug.LogError(string.Format("id:{0} State:{3} FightState:{6}", Id, Level, Exp, State, Position, Quality, FightState, Star));
            //Debug.LogError(string.Format("id:{0}  level:{1}  Exp:{2} Star:{7} State:{3} Position:{4}  Quality:{5}  FightState:{6}", Id, Level, Exp, State, Position, Quality, FightState, Star));
            //Debug.LogError(string.Format("id:{0}  count:{1}", Id, PetSkillList.Count));
            //for (int i = 0; i < PetSkillList.Count; i++)
            //{
            //    Debug.LogError("index:" + PetSkillList[i].index);
            //    Debug.LogError("skillId:" + PetSkillList[i].pet_skill_id);
            //}
        }

        public override string ToString()
        {
            return string.Format("id:{0} State:{3} FightState:{6} Index:{8}", Id, Level, Exp, State, Position, Quality, FightState, Star, IndexForSort);
            //return string.Format("id:{0} fightState:{1}", Id, FightState);
        }

        public List<PetSkill> GetAllSkillList()
        {
            List<PetSkill> petAllQualitySkillList = pet_quality_helper.GetPetSkillList(Id);
            for (int i = 0; i < PetSkillList.Count; i++)
            {
                PbPetSkill pbPetSkill = PetSkillList[i];
                for (int j = 0; j < petAllQualitySkillList.Count; j++)
                {
                    PetSkill petSkill = petAllQualitySkillList[j];
                    if (pbPetSkill.index == petSkill.index)
                    {
                        petSkill.skillId = pbPetSkill.pet_skill_id;
                        break;
                    }
                }
            }
            return petAllQualitySkillList;
        }

        public List<PetSkill> GetAllSkillList(PbPetInfoRefresh pbPetInfoRefresh)
        {
            List<PetSkill> petAllQualitySkillList = pet_quality_helper.GetPetSkillList(Id);
            for (int j = 0; j < pbPetInfoRefresh.petSkillList.Count; j++)
            {
                PbPetSkill pbPetSkill = pbPetInfoRefresh.petSkillList[j];
                for (int i = 0; i < petAllQualitySkillList.Count; i++)
                {
                    PetSkill petSkill = petAllQualitySkillList[i];
                    if (petSkill.index == pbPetSkill.index)
                    {
                        petSkill.skillId = pbPetSkill.pet_skill_id;
                        break;
                    }
                }
            }

            return petAllQualitySkillList;
        }

        public void CaculateFightValue()
        {
            //宠物用战士职业来计算战斗力
            PetData petData = PlayerDataManager.Instance.PetData;
            List<RoleAttributeData> list = petData.GetAttributeDataList(Id, Level, Star, Quality, PetSkillIdList, StarAttributeList, false, State, Position);
            TempFightValue = (int)fight_force_helper.GetFightForce(list, Vocation.Warrior);
        }

        public static int SortByFightValue(PetInfo x, PetInfo y)
        {
            if (x.TempFightValue > y.TempFightValue)
            {
                return -1;
            }
            return 1;
        }

        public bool CanStrength()
        {
            if (CanLevelUp())
            {
                return true;
            }
            if (CanQualityUp())
            {
                return true;
            }
            if (CanStarUp())
            {
                return true;
            }
            if (CanWash())
            {
                return true;
            }
            return false;
        }

        public bool CanLevelUp()
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            if (!petData.HavePetExpItem())
            {
                return false;
            }

            if (Level >= PlayerAvatar.Player.level)
            {
                return false;
            }

            if (PlayerAvatar.Player.level % 5 != 0)
            {
                return false;
            }

            //&&(pet_level_helper.GetNextLevelExp(Id, Level) == Exp))
            if (pet_quality_helper.CurrentLevelReachMax(Id, Quality, Level))
            {
                return false;
            }
            return true;
        }

        public bool CanQualityUp()
        {
            List<BaseItemData> baseItemDataList = pet_quality_helper.GetQualityUpCost(Id, Quality);
            bool levelReachMax = pet_quality_helper.CurrentLevelReachMax(Id, Quality, Level);
            bool qualityReachMax = pet_quality_helper.QualityReachMax(Id, Quality);
            return levelReachMax && !qualityReachMax && PlayerAvatar.Player.CheckCostLimit(baseItemDataList) == 0;
        }

        public bool CanStarUp()
        {
            ItemData itemData = pet_star_helper.GetStarUpCost(Id, Star);
            bool starLimit = pet_star_helper.ReachMaxStar(Id, Star);
            return !starLimit && PlayerAvatar.Player.CheckCostLimit(new List<BaseItemData>(){ itemData }) == 0;
        }

        public bool CanWash()
        {
            foreach (PetWashType washType in Enum.GetValues(typeof(PetWashType)))
            {
                if(IsWashTypeUnlock(washType))
                {
                    List<BaseItemData> baseItemDataList = pet_refresh_cost_helper.GetCostList(Id, WashLevel, (int)washType);
                    if (PlayerAvatar.Player.CheckCostLimit(baseItemDataList) == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool CanWash(PetWashType washType)
        {
            if(IsWashTypeUnlock(washType))
            {
                List<BaseItemData> baseItemDataList = pet_refresh_cost_helper.GetCostList(Id, WashLevel, (int)washType);
                if (PlayerAvatar.Player.CheckCostLimit(baseItemDataList) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsWashTypeUnlock(PetWashType washType, int lockNum = 0)
        {
            pet_refresh_limit refreshLimit = pet_refresh_limit_helper.GetPetRefreshLimit(washType, lockNum);
            if ((Star >= refreshLimit.__star)
                && (Quality >= refreshLimit.__quality)
                && (PlayerAvatar.Player.vip_level >= refreshLimit.__vip))
            {
                return true;
            }
            return false;
        }

        public int GetLeftReviveSecond()
        {
            UInt64 ms = _reviveTime - Global.Global.serverTimeStamp;
            int second = Mathf.CeilToInt((ms / 1000f));
            return second;
        }

        public void UpdateFightState(PetFightState newFightState)
        {
            PetFightState oldFightState = FightState;
            if ((FightState == PetFightState.fighting) && (newFightState == PetFightState.dead))
            {
                _reviveTime = Global.Global.serverTimeStamp + (uint)global_params_helper.GetPetReviveCd(Level) * 1000;
            }
            //Debug.LogError(
            //    string.Format("Id:{0}  From FightState:{1} To FightState:{2}", Id, FightState, newFightState)
            //    );
            FightState = newFightState;

            if ((oldFightState == PetFightState.dead) &&
                (newFightState == PetFightState.fighting || newFightState == PetFightState.idle))
            {
                EventDispatcher.TriggerEvent(BattleUIEvents.BATTLE_PET_REVIVE);
            }
        }

        public void FillPetInfoFromIdleToFight(PbPetInfoCombat pbPetInfoCombat)
        {
            FillPetInfoStateAndPosition(pbPetInfoCombat.combat_state);
            UpdateFightState((PetFightState)pbPetInfoCombat.active_state);
            IndexForSort = pbPetInfoCombat.active_seq;
            if (State == PetState.fighting)
            {
                EventDispatcher.TriggerEvent<PetInfo>(PetEvents.IdleToFight, this);
            }
        }

        public void FillPetInfoFromFightToIdle(PbPetInfoIdle pbPetInfoIdle)
        {
            FillPetInfoStateAndPosition(pbPetInfoIdle.combat_state);
            UpdateFightState((PetFightState)pbPetInfoIdle.active_state);
        }

        public void FillAddExp(PbPetInfoAddExp pbPetInfoAddExp)
        {
            bool isLevelUp = false;
            if (pbPetInfoAddExp.level > Level)
            {
                isLevelUp = true;
            }
            Exp = pbPetInfoAddExp.exp;
            Level = pbPetInfoAddExp.level;
            EventDispatcher.TriggerEvent(PetEvents.AddExp);
            if (isLevelUp)
            {
                EventDispatcher.TriggerEvent<PetInfo>(PetEvents.LevelUp, this);
            }
        }

        public void FillPetRefresh(PbPetInfoRefresh pbPetInfoRefresh)
        {
            WashLevel = pbPetInfoRefresh.refresh_level;
            WashExp = pbPetInfoRefresh.refresh_exp;
        }

        public void FillPetRefreshConfirm(PbPetInfoRefreshConfirm pbPetInfoRefreshConfirm)
        {
            switch ((PetWashType)(pbPetInfoRefreshConfirm.refresh_type))
            {
                case PetWashType.quality:
                    SetStarAttributeList(pbPetInfoRefreshConfirm.petStarAttriList);
                    break;
                case PetWashType.skill:
                    PetSkillList = pbPetInfoRefreshConfirm.petSkillList;
                    break;
                case PetWashType.total:
                    SetStarAttributeList(pbPetInfoRefreshConfirm.petStarAttriList);
                    //for (int i = 0; i < pbPetInfoRefreshConfirm.petStarAttriList.Count; i++)
                    //{
                    //    Debug.LogError(string.Format("{0}   {1}", pbPetInfoRefreshConfirm.petStarAttriList[i].attri_id, pbPetInfoRefreshConfirm.petStarAttriList[i].attri_value));
                    //}
                    PetSkillList = pbPetInfoRefreshConfirm.petSkillList;
                    break;
            }
        }
    }
}
