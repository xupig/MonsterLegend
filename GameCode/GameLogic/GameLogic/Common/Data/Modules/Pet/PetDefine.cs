﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 19:21:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PetDefine
    {
        public const int MAX_FIGHTING_PET_NUM = 6;
    }

    public enum PetState
    {
        invalid = -1,
        idle = 0,
        fighting = 1,
        assist = 2,
    }

    public enum PetFightState
    {
        idle = 0,
        fighting = 1,
        dead = 2,
        replaced = 3,
    }

    public enum PetPopType
    {
        gotPetInfo,
        ungetPetInfo,
        qualityUpSuccess,
        starUpSuccess,
    }

    public enum PetStrengthen
    {
        invalid = -1,
        levelUp,
        qualityUp,
        starUp,
        wash,
    }

    public enum PetContext
    {
        fighting,
        assist,
        handbook,
        selectPet,
        strengthen,
    }

    public enum PetSubTypeDefine
    {
        /// <summary>
        /// 物攻型
        /// </summary>
        physicsAttack = 1,
        /// <summary>
        /// 法功型
        /// </summary>
        magicAttack = 2,
        /// <summary>
        /// 防御型
        /// </summary>
        defence = 3,
        /// <summary>
        /// 辅助型
        /// </summary>
        assist = 4,
    }

    public enum PetWashType
    {
        quality = 1,
        skill = 2,
        total = 3,
    }
}
