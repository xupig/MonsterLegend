﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/27 9:59:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;

namespace Common.Data
{
    public class PetSkillDataWrapper
    {
        public int Quality;
        public int PetId;
        public PetSkill PetSkill;
        public int WashLevel;

        public PetSkillDataWrapper(int petId, int quality, PetSkill petSkill)
        {
            PetId = petId;
            Quality = quality;
            PetSkill = petSkill;
        }

        public PetSkillDataWrapper(int petId, int quality, PetSkill petSkill, int washLevel)
            : this(petId, quality, petSkill)
        {
            WashLevel = washLevel;
        }
    }
}
