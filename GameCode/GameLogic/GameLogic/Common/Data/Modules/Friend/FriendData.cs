﻿#region 模块信息
/*==========================================
// 文件名：FriendData
// 命名空间: Common.Data
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/9 15:41:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using UnityEngine;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class FriendData
    {
        public List<PbFriendAvatarInfo> FriendAvatarList;
        public List<PbFriendAvatarInfo> OnlineSortedFriendList;
        //public int FriendCurrentPage = 1;
       // public int FriendTotalPage = 1;

        public bool hasFriendSendDataRequest = true;


        public List<PbFriendAvatarInfo> FriendApplicationList;

        public List<PbAvatarData> RecommendFriendList;

        private HashSet<UInt64> _friendApplicationDict;
        private List<int> _removeList;

        public Dictionary<UInt64, int> BlessedDict;//
        public Dictionary<UInt64, int> BlessingDict;//
        public Dictionary<UInt64, int> ReceivedDict;

        public int BlessingNum;   //已经祝福的次数
        public int BlessedNum;    //正在被祝福的次数
        public int ReceivedNum;   //已经接受的祝福次数

        public List<PbIntimateEventInfo> IntimateEventInfoList;

        public Dictionary<int, int> SendItemDict;

        public bool hasDataResponsed = false;

        public  bool hasRequestData = false;

        public FriendData()
        {
            FriendAvatarList = new List<PbFriendAvatarInfo>();
            OnlineSortedFriendList = new List<PbFriendAvatarInfo>();
            FriendApplicationList = new List<PbFriendAvatarInfo>();
            RecommendFriendList = new List<PbAvatarData>();
            _friendApplicationDict = new HashSet<ulong>();
            BlessedDict = new Dictionary<ulong, int>();
            BlessingDict = new Dictionary<ulong, int>();
            ReceivedDict = new Dictionary<ulong, int>();
            _removeList = new List<int>();
            IntimateEventInfoList = new List<PbIntimateEventInfo>();
            SendItemDict = new Dictionary<int, int>();
        }


        public void FriendIntimateEventResp(error_code errorCode, bool dispatcherEvent, object[] objArr)
        {
            PbIntimateEventInfo eventInfo = new PbIntimateEventInfo();
            UInt64 dbId = 0;
            
            eventInfo.record_time = uint.Parse(objArr[3].ToString());
            dbId = ulong.Parse(objArr[4].ToString());
            eventInfo.degree = uint.Parse(objArr[2].ToString());
            switch(errorCode)
            {
                case error_code.SYS_ID_FREIDN_DEGREE_MISSION:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_MISSION;
                    eventInfo.recv_name = objArr[0].ToString();
                    eventInfo.send_dbid = PlayerAvatar.Player.dbid;
                    eventInfo.send_name = PlayerAvatar.Player.name;
                    eventInfo.item_id = uint.Parse(objArr[1].ToString());
                    break;
                case error_code.SYS_ID_FREIDN_DEGREE_GIVE_GOODS:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_GIVE_GOODS;
                    eventInfo.recv_name = objArr[0].ToString();
                    eventInfo.send_dbid = PlayerAvatar.Player.dbid;
                    eventInfo.send_name = PlayerAvatar.Player.name;
                    eventInfo.item_id = uint.Parse(objArr[1].ToString());
                    break;
                case error_code.SYS_ID_FREIDN_DEGREE_GIVE_GOODSED:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_GIVE_GOODS;
                    eventInfo.recv_name = PlayerAvatar.Player.name;
                    eventInfo.recv_dbid = PlayerAvatar.Player.dbid;
                    eventInfo.send_name = objArr[0].ToString();
                    eventInfo.item_id = uint.Parse(objArr[1].ToString());
                    break;
                case error_code.SYS_ID_FREIDN_DEGREE_TIME:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_TIME;
                    eventInfo.recv_name = objArr[0].ToString();
                    eventInfo.send_dbid = PlayerAvatar.Player.dbid;
                    eventInfo.send_name = PlayerAvatar.Player.name;
                    eventInfo.item_num = uint.Parse(objArr[1].ToString());
                    break;
                case error_code.SYS_ID_FREIDN_DEGREE_KILL:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_KILL;
                    eventInfo.recv_name = objArr[0].ToString();
                    eventInfo.send_dbid = PlayerAvatar.Player.dbid;
                    eventInfo.send_name = PlayerAvatar.Player.name;
                    eventInfo.item_id = uint.Parse(objArr[1].ToString());
                    break;
                case error_code.SYS_ID_FREIDN_DEGREE_ALCHEMY:
                    eventInfo.add_type = public_config.FREIDN_DEGREE_ALCHEMY;
                    eventInfo.send_name = objArr[0].ToString();
                    eventInfo.recv_name = objArr[1].ToString();
                    break;
            }

            IntimateEventInfoList.Insert(0, eventInfo);
           
            if (dispatcherEvent)
            {
                EventDispatcher.TriggerEvent(FriendEvents.FRIEND_INTIMATE_CHANGE, dbId);
                EventDispatcher.TriggerEvent(FriendEvents.INTIMATE_EVENT_LIST_CHANGE);
            }
        }

        public string GetFriendAvatarName(UInt64 dbId)
        {
            for(int i=0;i<FriendAvatarList.Count;i++)
            {
                if(FriendAvatarList[i].dbid == dbId)
                {
                    return FriendAvatarList[i].name;
                }
            }
            return string.Empty;
        }

        public bool HasFriend(UInt64 dbId)
        {
            for (int i = 0; i < FriendAvatarList.Count; i++)
            {
                if (FriendAvatarList[i].dbid == dbId)
                {
                    return true;
                }
            }
            return false;
        }

        public int GetMyFriendCount()
        {
            return FriendAvatarList.Count;
        }

        public void UpdateAssistAlchemy(ulong dbid, uint remainAssistCount)
        {
            int count = FriendAvatarList.Count;
            for (int i = 0; i < count; i++)
            {
                if (dbid == FriendAvatarList[i].dbid)
                {
                    FriendAvatarList[i].remain_assist_cnt = remainAssistCount;
                }
            }
        }

        public void UpdateRecommendFriendList(PbFriendRecommend recommendFriend)
        {
            RecommendFriendList = recommendFriend.data;
            for (int i = 0; i < RecommendFriendList.Count; i++)
            {
                RecommendFriendList[i].name = MogoProtoUtils.ParseByteArrToString(RecommendFriendList[i].name_bytes);
            }
        }

        public void UpdateFriendList(PbFriendInfo friendInfo)
        {
            List<PbFriendAvatarInfo> friendAvatarList = friendInfo.avatars_info;
            for (int i = 0; i < friendAvatarList.Count;i++ )
            {
                PbFriendAvatarInfo avatarInfo = friendAvatarList[i];
                avatarInfo.name = MogoProtoUtils.ParseByteArrToString(avatarInfo.name_bytes);
                //avatarInfo.guild_name = MogoProtoUtils.ParseByteArrToString(avatarInfo.guild_name_bytes);
            }
            FriendAvatarList = friendAvatarList;
            hasDataResponsed = true;
            //FriendCurrentPage = 1;
            //FriendTotalPage = 1;
        }
        public void UpdateFriend(PbFriendAvatarInfo avatarInfo)
        {
            avatarInfo.name = MogoProtoUtils.ParseByteArrToString(avatarInfo.name_bytes);
            //avatarInfo.guild_name = MogoProtoUtils.ParseByteArrToString(avatarInfo.guild_name_bytes);
            int count = FriendAvatarList.Count;
            for(int i=0;i<count;i++)
            {
                if (avatarInfo.dbid == FriendAvatarList[i].dbid)
                {
                    FriendAvatarList[i] = avatarInfo;
                }
            }
        }

        public void UpdateFriendOnlineState(UInt64 dbId,uint online)
        {
            for(int i=0;i<FriendAvatarList.Count;i++)
            {
                if(FriendAvatarList[i].dbid == dbId)
                {
                    FriendAvatarList[i].online = online;
                }
            }
        }

        public void UpdateFriendDegree(UInt64 dbid, uint degree)
        {
            for (int i = 0; i < FriendAvatarList.Count; i++)
            {
                if (FriendAvatarList[i].dbid == dbid)
                {
                    FriendAvatarList[i].degree = degree;
                }
            }
        }

        public void UpdateOnlineFriendList(PbFrinedOnlineList onlineList)
        {
            List<ulong> onlineDbidList = onlineList.dbid;
            for (int i = 0; i < onlineDbidList.Count; i++)
            {
                for (int j = 0; j < FriendAvatarList.Count; j++)
                {
                    if (FriendAvatarList[j].dbid == onlineDbidList[i])
                    {
                        FriendAvatarList[j].online = 1;
                        break;
                    }
                }
            }
            UpdateFriendInfosBySortOnline();
        }

        public void UpdateFriendInfosBySortOnline()
        {
            if (OnlineSortedFriendList.Count > 0)
            {
                OnlineSortedFriendList.Clear();
            }
            OnlineSortedFriendList.AddRange(FriendAvatarList.ToArray());
            OnlineSortedFriendList.Sort(SortFriendListByOnline);
        }

        public void UpdateBlessing(PbFriendList dataList)
        {
            BlessingDict.Clear();
            BlessingNum = dataList.bless_info.Count;
            for (int i = 0; i < BlessingNum; i++)
            {
                if(BlessingDict.ContainsKey(dataList.bless_info[i].dbid))
                {
                    BlessingDict[dataList.bless_info[i].dbid] += (int)dataList.bless_info[i].num;
                }
                else
                {
                    BlessingDict.Add(dataList.bless_info[i].dbid, (int)dataList.bless_info[i].num);
                }
            }
        }

        public void AddBlessing(UInt64 dbId)
        {
            if (BlessingDict.ContainsKey(dbId) == false)
            {
                BlessingDict.Add(dbId, 1);
                BlessingNum += 1;
            }
        }

        public void UpdateBlessed(PbFriendList dataList)
        {
            BlessedDict.Clear();
            BlessedNum = dataList.bless_info.Count;
            for (int i = 0; i < BlessedNum; i++)
            {
                if (BlessedDict.ContainsKey(dataList.bless_info[i].dbid))
                {
                    BlessedDict[dataList.bless_info[i].dbid] += (int)dataList.bless_info[i].num;
                }
                else
                {
                    BlessedDict.Add(dataList.bless_info[i].dbid, (int)dataList.bless_info[i].num);
                }
            }
        }

        public void UpdateReceivedList(PbFriendList dataList)
        {
            ReceivedDict.Clear();
            ReceivedNum = dataList.bless_info.Count;
            for (int i = 0; i < ReceivedNum; i++)
            {
                if (ReceivedDict.ContainsKey(dataList.bless_info[i].dbid) == false)
                 {
                     ReceivedDict.Add(dataList.bless_info[i].dbid, (int)dataList.bless_info[i].num);
                 }
            }
        }

        public void AddReceivedList(UInt64 dbId)
        {
            if (ReceivedDict.ContainsKey(dbId) == false)
            {
                ReceivedDict.Add(dbId, 1);
                ReceivedNum += 1;
            }
        }

        public void RemoveBlessedByDbid(UInt64 dbId)
        {
            if (BlessedDict.ContainsKey(dbId) == true)
            {
                BlessedDict.Remove(dbId);
                BlessedNum--;
            }
        }

        public void AddBlessed(UInt64 dbId)
        {
            if (BlessedDict.ContainsKey(dbId) == false)
            {
                BlessedDict.Add(dbId, 1);
                BlessedNum += 1;
            }
        }
        
        public void RemoveFriend(UInt64 dbId)
        {
            _removeList.Clear();
            int count = FriendAvatarList.Count;
            for (int i = 0; i < count; i++)
            {
                if (dbId == FriendAvatarList[i].dbid)
                {
                    _removeList.Add(i);
                }
            }
            for(int i=0;i<_removeList.Count;i++)
            {
                FriendAvatarList.RemoveAt(_removeList[i]);
            }
            //FriendTotalPage = Mathf.CeilToInt(FriendAvatarList.Count/20);
        }

        public void AddApplication(List<PbFriendAvatarInfo> friendInfoList)
        {
            for (int i = 0; i < friendInfoList.Count; i++)
            {
                PbFriendAvatarInfo avatarInfo = friendInfoList[i];
                avatarInfo.name = MogoProtoUtils.ParseByteArrToString(avatarInfo.name_bytes);
                //avatarInfo.guild_name = MogoProtoUtils.ParseByteArrToString(avatarInfo.guild_name_bytes);
                RemoveApplication(avatarInfo.dbid);

                _friendApplicationDict.Add(avatarInfo.dbid);
                FriendApplicationList.Add(avatarInfo);
            }
        }

        public PbFriendAvatarInfo RemoveApplication(UInt64 dbId)
        {
            PbFriendAvatarInfo _toRemoveData = null;
            if (_friendApplicationDict.Contains(dbId))
            {
                int count = FriendApplicationList.Count;
                for (int i = 0; i < count; i++)
                {
                    PbFriendAvatarInfo item = FriendApplicationList[i];
                    if (item.dbid == dbId)
                    {
                        _friendApplicationDict.Remove(dbId);
                        _toRemoveData = item;
                        break;
                    }
                }
                if (_toRemoveData!=null)
                {
                    FriendApplicationList.Remove(_toRemoveData);
                }
            }
            return _toRemoveData;
        }

        public PbFriendAvatarInfo GetFriendAvatarInfoByDbid(UInt64 dbid)
        {
            return FriendAvatarList.Find(item => item.dbid == dbid);
        }

        public List<PbFriendAvatarInfo> GetFriendInfosBySortOnline()
        {
            List<PbFriendAvatarInfo> friendList = new List<PbFriendAvatarInfo>();
            friendList.AddRange(FriendAvatarList.ToArray());
            friendList.Sort(SortFriendListByOnline);
            return friendList;
        }

        public List<PbFriendAvatarInfo> GetFriendInfoByDefault()
        {
            List<PbFriendAvatarInfo> friendList = new List<PbFriendAvatarInfo>();
            friendList.AddRange(FriendAvatarList.ToArray());
            friendList.Sort(SortFriendListByDefault);
            return friendList;
        }

        private int SortFriendListByOnline(PbFriendAvatarInfo a, PbFriendAvatarInfo b)
        {
            return (int)(b.online - a.online);
        }

        private int SortFriendListByDefault(PbFriendAvatarInfo first, PbFriendAvatarInfo second)
        {
            if (first.online != second.online)
            {
                return (int)(second.online - first.online);
            }
            else if (first.degree != second.degree)
            {
                return (int)(second.degree - first.degree);
            }
            else if (first.level != second.level)
            {
                return (int)(second.level - first.level);
            }
            else if (first.fight != second.fight)
            {
                return (int)(second.fight - first.level);
            }
            else 
            {
                return 0;
            }
        }
    }
}
