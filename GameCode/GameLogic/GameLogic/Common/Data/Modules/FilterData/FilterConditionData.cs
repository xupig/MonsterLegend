﻿#region 模块信息
/*==========================================
// 文件名：FilterConditionData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.FilterData
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/28 11:05:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System.Collections.Generic;
namespace Common.Data
{
    public class FilterConditionData
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Type
        {
            get
            {
                if (XMLManager.condition.ContainsKey(Id))
                {
                    return XMLManager.condition[Id].__type;
                }
                return 0;
            }
        }

        public bool IsShowAll
        {
            get
            {
                if (XMLManager.condition.ContainsKey(Id))
                {
                    List<int> listInt = data_parse_helper.ParseListInt(XMLManager.condition[Id].__value);
                    return listInt.Count == 1 && listInt[0] == 0;
                }
                return false;
            }
        }

        public FilterConditionData(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}