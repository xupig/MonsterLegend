﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TitleData
    {
        private Dictionary<int, PbTitleInfo> _titleDataDic = new Dictionary<int, PbTitleInfo>();
        private Dictionary<int, uint> _titleId2TimerId = new Dictionary<int, uint>();

        public void Fill(PbTitleBag titleBag)
        {
            RemoveAllTimer();
            _titleDataDic.Clear();
            int count = titleBag.title_info.Count;
            for (int i = 0; i < count; i++)
            {
                if (title_helper.GetTitleConfig((int)titleBag.title_info[i].title_id) == null)
                {
                    continue;
                }
                if (_titleDataDic.ContainsKey((int)titleBag.title_info[i].title_id) == false)
                {
                    _titleDataDic.Add((int)titleBag.title_info[i].title_id, titleBag.title_info[i]);
                }
                else
                {
                    _titleDataDic[(int)titleBag.title_info[i].title_id] = titleBag.title_info[i];
                }

                CheckAndAddTimer((int)titleBag.title_info[i].title_id);
            }
        }

        //已经获得的称号，要筛选掉已经过期的称号
        public List<int> GetGetTitleIdList()
        {
            List<int> titleIdList = new List<int>();
            foreach (int titleId in _titleDataDic.Keys)
            {
                TitleStateType titleStateType = GetTitleState(titleId);
                if (titleStateType != TitleStateType.OverdueUncheck && titleStateType != TitleStateType.OverdueChecked)
                {
                    titleIdList.Add(titleId);
                }
            }
            return titleIdList;
        }

        public TitleStateType GetTitleState(int titleId)
        {
            if (_titleDataDic.ContainsKey(titleId) == false)
            {
                return TitleStateType.NeverGet;
            }

            DateTime getTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)_titleDataDic[titleId].get_time);
            int titleTimeLimit = title_helper.GetTitleTimeLimit(titleId);
            getTime = getTime.AddSeconds(titleTimeLimit);
            DateTime nowTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            if (titleTimeLimit != 0 && getTime < nowTime && _titleDataDic[titleId].has_checked == 0)
            {
                return TitleStateType.OverdueUncheck;
            }
            else if (titleTimeLimit != 0 && getTime < nowTime && _titleDataDic[titleId].has_checked == 1)
            {
                return TitleStateType.OverdueChecked;
            }

            if (_titleDataDic[titleId].has_checked == 0)
            {
                return TitleStateType.New;
            }
            return TitleStateType.Normal;
        }

        public void AddTitle(PbTitleInfo titleInfo)
        {
            if (title_helper.GetTitleConfig((int)titleInfo.title_id) == null)
            {
                return;
            }
            if (_titleDataDic.ContainsKey((int)titleInfo.title_id) == false)
            {
                _titleDataDic.Add((int)titleInfo.title_id, titleInfo);
                return;
            }
            _titleDataDic[(int)titleInfo.title_id] = titleInfo;
            CheckAndAddTimer((int)titleInfo.title_id);
        }

        public PbTitleInfo GetTitleInfo(int titleId)
        {
            if (_titleDataDic.ContainsKey(titleId) == true)
            {
                return _titleDataDic[titleId];
            }
            return null;
        }

        //客户端定时将称号设置为过期状态，并将其标示为未查看状态
        private void SetTitleOverdue(int titleId)
        {
            PbTitleInfo title = _titleDataDic[titleId];
            title.has_checked = 0;

            TimerHeap.DelTimer(_titleId2TimerId[titleId]);
            _titleId2TimerId.Remove(titleId);
            EventDispatcher.TriggerEvent<int>(TitleEvents.UpdateTitleData, titleId);
        }

        private void RemoveAllTimer()
        {
            foreach (int titleId in _titleId2TimerId.Keys)
            {
                TimerHeap.DelTimer(_titleId2TimerId[titleId]);
            }
            _titleId2TimerId.Clear();
        }

        private void CheckAndAddTimer(int titleId)
        {
            TitleStateType titleStateType = GetTitleState(titleId);
            if ((titleStateType == TitleStateType.Normal || titleStateType == TitleStateType.New) && title_helper.GetTitleTimeLimit(titleId) != 0)
            {
                DateTime getTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)_titleDataDic[titleId].get_time);
                int titleTimeLimit = title_helper.GetTitleTimeLimit(titleId);
                getTime = getTime.AddSeconds(titleTimeLimit);
                DateTime nowTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
                uint timerId = TimerHeap.AddTimer<int>((uint)(getTime - nowTime).TotalMilliseconds, 0, SetTitleOverdue, titleId);
                _titleId2TimerId.Add(titleId, timerId);
            }
        }
    }

    public enum TitleStateType
    {
        New = 0,
        Normal = 1,
        OverdueUncheck = 2,
        OverdueChecked = 3,
        NeverGet = 4,
    }
}
