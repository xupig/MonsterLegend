﻿#region 模块信息
/*==========================================
// 文件名：RankCacheData
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.Data
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/22 14:03:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent.GeneralRankingList;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{
    public class RankDataWrapper
    {
        public RankType rankType;
        public PbRankInfoList rankInfoList;
        public PbRankGuildInfoList guildRankInfoList;
        public long lastUpdateTime;
    }

    public class RankInfo
    {
        /// <summary>
        /// 储存排行榜的临时数据
        /// </summary>
        public Dictionary<RankType, RankDataWrapper> rankingDict;

        /// <summary>
        /// 存储每个榜在服务器最新的更新时间
        /// </summary>
        public Dictionary<RankType, uint> lastUpdateTimeDict;

        // 公会排行榜的临时数据
        public PbRankGuildInfoList guildRankingList;

        // 已经得到奖励的玩家姓名列表
        public Dictionary<RankType, List<string>> hasGainAwardPlayerDict;

        // 排行榜玩家的临时数据
        public Dictionary<ulong, PbOfflineData> playerDataDict;

        /// <summary>
        /// 我的排名数据
        /// </summary>
        public Dictionary<RankType, int> myRankDict;

        /// <summary>
        /// 我的领奖数据
        /// </summary>
        public Dictionary<RankType, bool> hasGetAwardDict;

        /// <summary>
        /// 我今天已经崇拜过的玩家
        /// </summary>
        public List<ulong> hasWorshipPlayerList;
        public UInt32 hasWorshipTimes;
        public int MaxWorshipCount = 3;

        public Dictionary<RankType, bool> NeedGetNewData;

        /// <summary>
        /// 排行榜玩家的属性表
        /// </summary>
        public List<KeyValuePair<string, string>> playerDetailAttributeList;
        public List<KeyValuePair<string, string>> playerSpecialAttributeList;
        public List<KeyValuePair<string, string>> playerElementAttributeList;

        public PbBagList playerEquipBagList;
        public PbBagList playerRuneBagList;
        //ari public List<PlayerRuneItemData> playerRuneList;

        public RankInfo()
        {
            rankingDict = new Dictionary<RankType, RankDataWrapper>();
            playerDataDict = new Dictionary<ulong, PbOfflineData>();
            myRankDict = new Dictionary<RankType, int>();
            hasWorshipPlayerList = new List<ulong>();
            playerDetailAttributeList = new List<KeyValuePair<string, string>>();
            playerSpecialAttributeList = new List<KeyValuePair<string, string>>();
            playerElementAttributeList = new List<KeyValuePair<string, string>>();
            hasGainAwardPlayerDict = new Dictionary<RankType, List<string>>();
            playerEquipBagList = new PbBagList();
            playerRuneBagList = new PbBagList();
            //ari playerRuneList = new List<PlayerRuneItemData>();
            lastUpdateTimeDict = new Dictionary<RankType, uint>();
            NeedGetNewData = new Dictionary<RankType, bool>();
            InitNeedGetNewData();
        }

        private void InitNeedGetNewData()
        {
            foreach (var value in Enum.GetValues(typeof(RankType)))
            {
                NeedGetNewData.Add((RankType)value, true);
            }
        }

        public void UpdateRankData(RankType type, PbRankInfoList rankInfoList)
        {
            RankDataWrapper wrapper = new RankDataWrapper();
            wrapper.rankInfoList = rankInfoList;
            if (rankingDict.ContainsKey(type))
            {
                rankingDict[type] = wrapper;
            }
            else
            {
                rankingDict.Add(type, wrapper);
            }
        }

        public void UpdateAwardRankData(RankType type, PbRankInfoList rankInfoList)
        {
            List<string> nameList = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                if (rankInfoList.avatars_info.Count > i)
                {
                    nameList.Add(MogoProtoUtils.ParseByteArrToString(rankInfoList.avatars_info[i].name_bytes));
                }
                else
                {
                    nameList.Add(string.Empty);
                }
            }
            AddAwardDataToDict((RankType)rankInfoList.type, nameList);
        }

        public void UpdateAwardRankData(RankType rankType, PbRankGuildInfoList pbRankGuildInfoList)
        {
            List<string> nameList = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                if (pbRankGuildInfoList.rank_guild_info.Count > i)
                {
                    nameList.Add(MogoProtoUtils.ParseByteArrToString(pbRankGuildInfoList.rank_guild_info[i].guild_name_bytes));
                }
                else
                {
                    nameList.Add(string.Empty);
                }
            }
            AddAwardDataToDict(rankType, nameList);
        }

        private void AddAwardDataToDict(RankType type, List<string> nameList)
        {
            if (hasGainAwardPlayerDict.ContainsKey(type))
            {
                hasGainAwardPlayerDict[type] = nameList;
            }
            else
            {
                hasGainAwardPlayerDict.Add(type, nameList);
            }
        }


        public RankDataWrapper GetRankData(RankType type)
        {
            if (rankingDict.ContainsKey(type))
            {
                return rankingDict[type];
            }
            return null;
        }

        public bool HasRankData(RankType type)
        {
            return rankingDict.ContainsKey(type) && (!NeedGetNewData[type]);
        }

        public int GetMyRank(RankType type)
        {
            if (myRankDict.ContainsKey(type))
            {
                return myRankDict[type];
            }
            return 0;
        }

        /*//ari 
        public void UpdateRoleAttribute(List<PbAttri> pbAttriList, int vocation)
        {
            const int playerEPLimit = 240;
            playerDetailAttributeList.Clear();
            playerSpecialAttributeList.Clear();
            playerElementAttributeList.Clear();
            PbAttri pbAttri;
            pbAttriList.Sort(SortFunction);
            for (int i = 0; i < pbAttriList.Count; i++)
            {
                pbAttri = pbAttriList[i];
                attri_config attri = attri_config_helper.GetAttri((int)pbAttri.atrri_id);
                if (attri == null)
                {
                    continue;
                }
                string vocationString = vocation.ToString();
                if (attri.__show == null
                    || attri.__show.ContainsKey(vocationString) == false
                    || attri.__show[vocationString] == "0")
                {
                    continue;
                }
                if (attri.__tab == 1)
                {
                    // 能量上限属性特殊处理，显示值在实际值的基础上除以100；
                    if (pbAttri.atrri_id == playerEPLimit)
                    {
                        playerDetailAttributeList.Add(new KeyValuePair<string, string>(MogoLanguageUtil.GetContent(attri.__name_id), Mathf.RoundToInt(pbAttri.attri_value * 1.0f / 100).ToString()));
                    }
                    else
                    {
                        playerDetailAttributeList.Add(new KeyValuePair<string, string>(MogoLanguageUtil.GetContent(attri.__name_id), pbAttri.attri_value.ToString()));
                    }
                }
                else if (attri.__tab == 2)
                {
                    playerElementAttributeList.Add(new KeyValuePair<string, string>(MogoLanguageUtil.GetContent(attri.__name_id), pbAttri.attri_value.ToString()));
                }
                else if (attri.__tab == 3)
                {
                    playerSpecialAttributeList.Add(new KeyValuePair<string, string>(MogoLanguageUtil.GetContent(attri.__name_id), pbAttri.attri_value.ToString()));
                }
            }
        }*/

        private int SortFunction(PbAttri x, PbAttri y)
        {
            int xProperty = attri_config_helper.GetPriority((int)x.atrri_id);
            int yProperty = attri_config_helper.GetPriority((int)y.atrri_id);
            if (xProperty == yProperty)
            {
                return (int)x.atrri_id - (int)y.atrri_id;
            }
            else
            {
                return xProperty - yProperty;
            }
        }

        public void UpdateMyRankData(RankType rankType, PbRankInfo pbRankInfo)
        {
            if (myRankDict.ContainsKey(rankType))
            {
                myRankDict[rankType] = (int)pbRankInfo.ranking;
            }
            else
            {
                myRankDict.Add(rankType, (int)pbRankInfo.ranking);
            }
            if (rankType == RankType.demonGateDayRank || rankType == RankType.demonGateWeekRank)
            {
                return;
            }
            UpdateNeedGetNewData(rankType, pbRankInfo.last_update_time);
        }

        private void UpdateNeedGetNewData(RankType rankType, uint lastUpdateTime)
        {
            if (lastUpdateTimeDict.ContainsKey(rankType))
            {
                if (lastUpdateTimeDict[rankType] == lastUpdateTime)
                {
                    NeedGetNewData[rankType] = false;
                }
                else
                {
                    lastUpdateTimeDict[rankType] = lastUpdateTime;
                    NeedGetNewData[rankType] = true;
                    RankingListManager.Instance.RequestMyRanking((int)rankType);
                    RankingListManager.Instance.RequestTenMinutesRankListData((int)rankType);
                }
            }
            else
            {
                lastUpdateTimeDict.Add(rankType, lastUpdateTime);
                NeedGetNewData[rankType] = false;
            }
        }

        public void UpdateWorshipData(PbRankInfo pbWorshipInfo)
        {
            hasWorshipTimes = pbWorshipInfo.worship_count;
            foreach (KeyValuePair<RankType, RankDataWrapper> pair in rankingDict)
            {
                if (pair.Value.rankInfoList == null)
                {
                    continue;
                }
                for (int i = 0; i < pair.Value.rankInfoList.avatars_info.Count; i++)
                {
                    if (pair.Value.rankInfoList.avatars_info[i].dbid == pbWorshipInfo.to_dbid)
                    {
                        pair.Value.rankInfoList.avatars_info[i].rank_worship += 1;
                    }
                }
            }
            EventDispatcher.TriggerEvent<ulong>(RankingListEvents.REFRESH_ONE_PLAYER_WORSHIP_DATA, pbWorshipInfo.to_dbid);
        }

        public void UpdateWorshipData(PbRankInfoList pbHasWorshipInfo)
        {
            hasWorshipTimes = pbHasWorshipInfo.worship_count;
            hasWorshipPlayerList.Clear();
            for (int i = 0; i < pbHasWorshipInfo.avatars_info.Count; i++)
            {
                PbRankAvatarInfo pbRankAvatarInfo = pbHasWorshipInfo.avatars_info[i];
                hasWorshipPlayerList.Add(pbRankAvatarInfo.dbid);
            }
        }

        public void UpdateEquipInfo(PbOfflineData pbOfflineData)
        {/*
            playerRuneList.Clear();
            List<int> indexList = rune_helper.GetALLPageList();
            for (int i = 0; i < indexList.Count; i++)
            {
                playerRuneList.Add(new PlayerRuneItemData(indexList[i]));
            }

            playerEquipBagList = pbOfflineData.pkg_body_equip;

            playerRuneBagList = pbOfflineData.pkg_body_rune;
            if (playerRuneBagList == null || playerRuneBagList.items == null)
            {
                Debug.LogError("PlayerRune Bag List Null");
            }
            for (int i = 0; i < playerRuneBagList.items.Count; i++)
            {
                PbGridItem gridItem = playerRuneBagList.items[i];
                rune rrune = rune_helper.GetRune((int)gridItem.item.id);
                playerRuneList[rrune.__page - 1].runeItemInfoList.Add(new RuneItemInfo(gridItem));
            }*/
        }

        public void UpdateGuildRankData(PbRankGuildInfoList pbRankGuildInfoList)
        {
            RankDataWrapper wrapper = new RankDataWrapper();
            RankType rankType = (RankType)pbRankGuildInfoList.type;
            for (int i = 0; i < pbRankGuildInfoList.rank_guild_info.Count; i++)
            {
                PbRankGuildInfo guildInfo = pbRankGuildInfoList.rank_guild_info[i];
                guildInfo.guild_name = MogoProtoUtils.ParseByteArrToString(guildInfo.guild_name_bytes);
                guildInfo.leader_name = MogoProtoUtils.ParseByteArrToString(guildInfo.leader_name_bytes);
            }
            wrapper.guildRankInfoList = pbRankGuildInfoList;
            if (rankingDict.ContainsKey(rankType))
            {
                rankingDict[rankType] = wrapper;
            }
            else
            {
                rankingDict.Add(rankType, wrapper);
            }
        }

        public List<PbRankGuildInfo> GetGuildRankContent(RankType type, int start, int end)
        {
            int pbCount = rankingDict[type].guildRankInfoList.rank_guild_info.Count;
            end = end > pbCount ? pbCount : end;
            return rankingDict[type].guildRankInfoList.rank_guild_info.GetRange(start, end - start);
        }

        public List<PbRankAvatarInfo> GetRankContent(RankType type, int start, int end)
        {
            if (rankingDict.ContainsKey(type) == false)
            {
                return null;
            }
            int pbCount = rankingDict[type].rankInfoList.avatars_info.Count;
            end = end > pbCount ? pbCount : end;
            return rankingDict[type].rankInfoList.avatars_info.GetRange(start, end - start);
        }

        public PbRankAvatarInfo GetPlayerRankAvatarInfo(RankType rankType, ulong playerDbId)
        {
            List<PbRankAvatarInfo> list = rankingDict[rankType].rankInfoList.avatars_info;
            for (int i = 0; i < list.Count; i++)
            {
                if (playerDbId == list[i].dbid)
                {
                    return list[i];
                }
            }
            return null;
        }

        public List<PbSysFightForce> pbSystemFightForce;
        public List<PbSysFightForce> FightForceCompareInfo
        {
            get
            {
                return pbSystemFightForce;
            }
            set
            {
                pbSystemFightForce = value as List<PbSysFightForce>;
            }
        }
    }
}