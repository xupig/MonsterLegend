﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.GlobalManager;
using ModuleMainUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsData
    {
        private Dictionary<TownTipsId, BaseTownTips> _townTipsIdDict;

        public List<TownTipsWrapper> _list = new List<TownTipsWrapper>();

        public TownTipsData()
        {
            InitTownTipsIdDict();

            AddListener();
        }

        private void InitTownTipsIdDict()
        {
            _townTipsIdDict = new Dictionary<TownTipsId, BaseTownTips>();
            _townTipsIdDict.Add(TownTipsId.decompose, new TownTipsEquipDecompose(TownTipsId.decompose));
            _townTipsIdDict.Add(TownTipsId.strengthen, new TownTipsEquipStrengthen(TownTipsId.strengthen));
            _townTipsIdDict.Add(TownTipsId.petQualityUp, new TownTipsPetQualityUp(TownTipsId.petQualityUp));
            _townTipsIdDict.Add(TownTipsId.petStarUp, new TownTipsPetStarUp(TownTipsId.petStarUp));
            _townTipsIdDict.Add(TownTipsId.dragonExplore, new TownTipsDragonExplore(TownTipsId.dragonExplore));
            //_townTipsIdDict.Add(TownTipsId.rune, new TownTipsRune(TownTipsId.rune));//按策划要求屏蔽提示
            _townTipsIdDict.Add(TownTipsId.skillUpgrade, new TownTipsSkillUpgrade(TownTipsId.skillUpgrade));
            _townTipsIdDict.Add(TownTipsId.newFashion, new TownTipsNewFashion(TownTipsId.newFashion));
            _townTipsIdDict.Add(TownTipsId.newMount, new TownTipsNewMount(TownTipsId.newMount));
            _townTipsIdDict.Add(TownTipsId.newWing, new TownTipsNewWing(TownTipsId.newWing));
            _townTipsIdDict.Add(TownTipsId.newEquipEffect, new TownTipsNewEquipEffect(TownTipsId.newEquipEffect));
            _townTipsIdDict.Add(TownTipsId.petCall, new TownTipsPetCall(TownTipsId.petCall));
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(TownTipsEvent.Show, Show);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.SHOW_PANEL, OnShowPanel);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.ENABLE_PANEL_CANVAS, OnEnablePanelCanvas);
        }

        private void OnEnablePanelCanvas(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.MainUIField)
            {
                Show();
            }
        }

        private void OnShowPanel(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.MainUIField)
            {
                Show();
            }
        }

        private void UIShow(PanelIdEnum panelId, object data)
        {
            Show();
        }

        public BaseTownTips GetBaseTownTips(TownTipsId id)
        {
            return _townTipsIdDict[id];
        }

        public void Enqueue(TownTipsId id)
        {
            _list.Add(new TownTipsWrapper(id, 0));
        }

        public void Enqueue(TownTipsId id, int itemId)
        {
            _list.Add(new TownTipsWrapper(id, itemId));
        }

        public TownTipsWrapper Dequeue()
        {
            TownTipsWrapper id = _list[0];
            _list.RemoveAt(0);
            return id;
        }

        public TownTipsWrapper Peek()
        {
            return _list[0];
        }

        private void Show()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            if (mapType != public_config.MAP_TYPE_CITY && mapType != public_config.MAP_TYPE_WILD)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.MainUINotice);
                return;
            }

            if (_list.Count > 0)
            {
                TownTipsWrapper wrapper = Peek();
                TownTipsId id = wrapper.tipType;
                if (!_townTipsIdDict[id].CheckAgain())
                {
                    Dequeue();
                    Show();
                    return;
                }
                UIManager.Instance.ShowPanel(PanelIdEnum.MainUINotice, wrapper);
            }
            else
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.MainUINotice);
            }
        }

        public bool ContainsTownTip(TownTipsId townTipsId)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].tipType == townTipsId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
