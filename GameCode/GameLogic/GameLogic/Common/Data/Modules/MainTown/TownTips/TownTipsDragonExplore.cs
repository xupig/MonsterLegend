﻿using Common.Data;
using Common.Events;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TownTipsDragonExplore : BaseTownTips
    {
        private DreamlandData DreamlandData
        {
            get
            {
                return PlayerDataManager.Instance.DreamlandData;
            }
        }

        public TownTipsDragonExplore(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener(DreamlandEvents.ReceiveDreamlandData, OnReceiveDreamlandData);
            AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            AddEventListener<int>(FunctionEvents.ADD_FUNTION, OnAddFuntion);
        }

        private void OnAddFuntion(int functionId)
        {
            if (functionId == (int)FunctionId.dreamland)
            {
                if (DreamlandData.CanExplore())
                {
                    Enqueue();
                    EventDispatcher.TriggerEvent(TownTipsEvent.Show);
                }
            }
        }

        private void OnReceiveDreamlandData()
        {
            if (DreamlandData.CanExplore())
            {
                Enqueue();
                EventDispatcher.TriggerEvent(TownTipsEvent.Show);
            }
        }

        private void OnEnterMap(int mapId)
        {
            if (!GameSceneManager.GetInstance().inCombatScene)
            {
                if (DreamlandData.CanExplore())
                {
                    Enqueue();
                    EventDispatcher.TriggerEvent(TownTipsEvent.Show);
                }
            }
        }

        public override bool CheckAgain()
        {
            return DreamlandData.CanExplore();
        }

        public override void ShowView()
        {
            EventDispatcher.TriggerEvent(DreamlandEvents.ShowScene);
        }
    }
}
