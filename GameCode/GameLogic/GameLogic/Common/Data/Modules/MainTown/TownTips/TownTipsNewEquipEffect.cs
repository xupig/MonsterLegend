﻿#region 模块信息
/*==========================================
// 文件名：TownTipsNewEquipEffect
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.MainTown.TownTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/18 15:39:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameData;
using MogoEngine.Events;
namespace Common.Data
{
    public class TownTipsNewEquipEffect : BaseTownTips
    {
        public TownTipsNewEquipEffect(TownTipsId townTipsId) : base(townTipsId) {}

        private int currentFacadeId;

        protected override void AddListener()
        {
            AddEventListener<int>(FashionEvents.CAN_ACTIVITE_NEW_EFFECT, NewEffectCanActivite);
        }

        private void NewEffectCanActivite(int facadeId)
        {
            Enqueue(facadeId);
            currentFacadeId = facadeId;
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }

        public override void ShowView()
        {
            int groupId = equip_facade_helper.GetEffectGroup(currentFacadeId);
            switch (groupId)
            {
                // 宝石特效
                case 1:
                    view_helper.OpenView(90);
                    break;
                case 2:
                    view_helper.OpenView(91);
                    break;
                case 3:
                    view_helper.OpenView(92);
                    break;
            }

        }
    }
}