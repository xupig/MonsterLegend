﻿using Common.Data;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    abstract public class BaseTownTips
    {
        protected TownTipsId id;

        public BaseTownTips(TownTipsId id)
        {
            this.id = id;
            AddListener();
        }

        public TownTipsData TownTipsData
        {
            get
            {
                return PlayerDataManager.Instance.TownTipsData;
            }
        }

        //在addListener做触发判断
        abstract protected void AddListener();

        private bool BaseCheck()
        {
            return IsFunctionOpen() && !IdExist();
        }

        private bool IdExist()
        {
            return TownTipsData.ContainsTownTip(id);
        }

        protected void AddEventListener(string eventType, Action handler)
        {
            EventDispatcher.AddEventListener(eventType, () =>
            {
                if (BaseCheck())
                {
                    handler.Invoke();
                }
            });
        }

        protected void AddEventListener<T>(string eventType, Action<T> handler)
        {
            EventDispatcher.AddEventListener<T>(eventType, (T param) =>
            {
                if (BaseCheck())
                {
                    handler.Invoke(param);
                }
            });
        }

        protected void AddEventListener<T, U>(string eventType, Action<T, U> handler)
        {
            EventDispatcher.AddEventListener<T, U>(eventType, (T tParam, U uParam) =>
            {
                if (BaseCheck())
                {
                    handler.Invoke(tParam, uParam);
                }
            });
        }

        public void AddPropertyListener(Entity entity, string propertyName, EntityPropertyManager.VOID_VOID action)
        {
            EntityPropertyManager.Instance.AddEventListener(entity, propertyName, () =>
            {
                if (BaseCheck())
                {
                    action.Invoke();
                }
            });
        }

        protected void Enqueue(int itemId = 0)
        {
            if (IdExist())
            {
                throw new Exception("id: 已经在队列:" + id);
            }
            TownTipsData.Enqueue(id, itemId);
        }

        //在显示的时候会调用CheckAgain，做第二次检查
        public virtual bool CheckAgain()
        {
            return true;
        }

        public int GetIcon()
        {
            return town_tips_helper.GetIcon(id);
        }

        public virtual string GetTitle()
        {
            return MogoLanguageUtil.GetContent(town_tips_helper.GetTitle(id));
        }

        public virtual void ShowView()
        {
            view_helper.OpenView(town_tips_helper.GetViewId(id));
        }

        public bool IsFunctionOpen()
        {
            int functionId = town_tips_helper.GetFunctionId(id);
            if ((FunctionId)functionId == FunctionId.invalid)
            {
                return true;
            }
            return PlayerDataManager.Instance.FunctionData.IsFunctionOpen(functionId);
        }
    }
}
