﻿#region 模块信息
/*==========================================
// 文件名：TownTipsNewFashion
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.MainTown.TownTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/18 15:38:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameData;
using MogoEngine.Events;
namespace Common.Data
{
    public class TownTipsNewFashion : BaseTownTips
    {
        public TownTipsNewFashion(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<int>(FashionEvents.CAN_PUT_ON_NEW_FASHION, NewFashionCanPutOn);
        }

        private void NewFashionCanPutOn(int fashionId)
        {
            Enqueue(fashionId);
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }
    }
}