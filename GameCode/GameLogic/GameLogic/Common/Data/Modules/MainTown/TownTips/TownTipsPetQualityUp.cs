﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Structs;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsPetQualityUp : BaseTownTips
    {
        private PetData PetData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        public TownTipsPetQualityUp(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<PetInfo>(PetEvents.LevelUp, OnPetQualityUp);
            AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
        }

        private void OnPetQualityUp(PetInfo petInfo)
        {
            if (petInfo.CanQualityUp())
            {
                Enqueue();
            }
        }

        private void OnItemUpdate(BagType bagType, uint pos)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo((int)pos);
                if (itemInfo != null)
                {
                    if (itemInfo.Type == BagItemType.PetStone || itemInfo.Type == BagItemType.PetCream)
                    {
                        if (Check())
                        {
                            Enqueue();
                        }
                    }
                }
            }
        }

        public override string GetTitle()
        {
            PetInfo petInfo = GetCanQualityUpPetInfo();
            return string.Format(base.GetTitle(), pet_helper.GetName(petInfo.Id));
        }

        private PetInfo GetCanQualityUpPetInfo()
        {
            PetInfo petInfo = GetCanQualityUpPetInfo(PetData.FightingPetList);
            if (petInfo != null)
            {
                return petInfo;
            }

            petInfo = GetCanQualityUpPetInfo(PetData.AssistPetList);
            if (petInfo != null)
            {
                return petInfo;
            }

            petInfo = GetCanQualityUpPetInfo(PetData.IdlePetList);
            if (petInfo != null)
            {
                return petInfo;
            }
            throw new Exception("找不到可突破的魔灵");
        }

        private PetInfo GetCanQualityUpPetInfo(List<PetInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].CanQualityUp())
                {
                    return list[i];
                }
            }
            return null;
        }

        public bool QualityUpCheck(PetInfo petInfo)
        {
            return petInfo.CanQualityUp();
        }

        private bool Check()
        {
            foreach (PetInfo petInfo in PetData.PetDict.Values)
            {
                if (petInfo.CanQualityUp())
                {
                    return true;
                }
            }
            return false;
        }

        public override bool CheckAgain()
        {
            return Check();
        }

        public override void ShowView()
        {
            //PetPanelDataWrapper petDataWrapper = new PetPanelDataWrapper(PanelIdEnum.PetStrengthen, PetStrengthen.qualityUp, GetCanQualityUpPetInfo().Id);
            //UIManager.Instance.ShowPanel(PanelIdEnum.Pet, petDataWrapper);
        }
    }
}
