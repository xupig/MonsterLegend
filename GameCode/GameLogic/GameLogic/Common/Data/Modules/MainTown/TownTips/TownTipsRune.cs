﻿using Common.ClientConfig;
using Common.Data;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TownTipsRune : BaseTownTips
    {
        private FunctionData FunctionData
        {
            get
            {
                return PlayerDataManager.Instance.FunctionData;
            }
        }

        public TownTipsRune(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddPropertyListener(PlayerAvatar.Player, EntityPropertyDefine.money_rune_stone, OnRuneStoneChanged);
        }

        private void OnRuneStoneChanged()
        {
            rune_random random = rune_helper.GetRuneRandom(1, 3);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(random.__cost);
            if (PlayerAvatar.Player.CheckCostLimit(dataDict, false) == 0)
            {
                Enqueue();
            }
        }

        public override bool CheckAgain()
        {
            rune_random random = rune_helper.GetRuneRandom(1, 3);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(random.__cost);
            if (PlayerAvatar.Player.CheckCostLimit(dataDict, false) == 0)
            {
                return true;
            }
            return false;
        }
    }
}
