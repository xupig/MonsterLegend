﻿#region 模块信息
/*==========================================
// 文件名：TownTipsNewWing
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.MainTown.TownTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/18 15:39:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameData;
using MogoEngine.Events;
namespace Common.Data
{
    public class TownTipsNewWing : BaseTownTips
    {
        public TownTipsNewWing(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<int>(WingEvents.NEW_WING_CAN_WEAR, NewWingCanWear);
        }

        private void NewWingCanWear(int wingId)
        {
            Enqueue(wingId);
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }
    }
}