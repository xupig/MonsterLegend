﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsEquipStrengthen : BaseTownTips
    {
        private BagDataManager BagData
        {
            get
            {
                return PlayerDataManager.Instance.BagData;
            }
        }

        private EquipStrengthenData EquipStrengthenData
        {
            get
            {
                return PlayerDataManager.Instance.EquipStrengthenData;
            }
        }

        public TownTipsEquipStrengthen(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<byte, byte>(RoleEvent.PLAYER_LEVEL_UP, OnLevelUp);
        }

        private void OnLevelUp(byte nowLevel, byte lastLevel)
        {
            if (Check())
            {
                Enqueue();
            }
        }

        private bool Check()
        {
            Dictionary<int, EquipStrengthenItemInfo> dict = EquipStrengthenData.GetAllEquipStrengthenInfo();
            foreach (EquipStrengthenItemInfo itemInfo in dict.Values)
            {
                //能强化5次才提示
                if (strengthen_helper.CanStrengthenManyTimes(itemInfo.EquipItemInfo.GridPosition, 5) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public override bool CheckAgain()
        {
            return Check();
        }

        private int GetLowestLevelPosition()
        {
            Dictionary<int, EquipStrengthenItemInfo> dict = PlayerDataManager.Instance.EquipStrengthenData.GetAllEquipStrengthenInfo();
            List<int> positionList = dict.Keys.ToList();
            int position = -1;
            int minStrengthenLevel = int.MaxValue;
            for (int i = 0; i < positionList.Count; i++)
            {
                EquipStrengthenItemInfo itemInfo = dict[positionList[i]];
                //能强化5次才提示
                if (strengthen_helper.CanStrengthenManyTimes(itemInfo.EquipItemInfo.GridPosition, 5) == true)
                {
                    if (itemInfo.StrengthenLevel < minStrengthenLevel)
                    {
                        minStrengthenLevel = itemInfo.StrengthenLevel;
                        position = positionList[i];
                    }
                }
            }
            return position;
        }

        public override string GetTitle()
        {
            int position = GetLowestLevelPosition();
            return string.Format(base.GetTitle(), item_helper.GetEquipPositionDesc(position));
        }
    }
}
