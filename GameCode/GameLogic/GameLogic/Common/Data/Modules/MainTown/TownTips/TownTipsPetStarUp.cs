﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsPetStarUp : BaseTownTips
    {
        private PetData PetData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        public TownTipsPetStarUp(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
        }

        private void OnItemUpdate(BagType bagType, uint pos)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo((int)pos);
                if (itemInfo != null && itemInfo.Type == BagItemType.PetFragment)
                {
                    if (Check())
                    {
                        Enqueue();
                    }
                }
            }
        }

        private bool Check()
        {
            foreach (PetInfo petInfo in PetData.PetDict.Values)
            {
                if (petInfo.CanStarUp())
                {
                    return true;
                }
            }
            return false;
        }

        public override bool CheckAgain()
        {
            return Check();
        }

        public override void ShowView()
        {
            /*ari
            PetPanelDataWrapper petDataWrapper = new PetPanelDataWrapper(PanelIdEnum.PetStrengthen, PetStrengthen.starUp, GetCanStarUpPetInfo().Id);
            UIManager.Instance.ShowPanel(PanelIdEnum.Pet, petDataWrapper);*/
        }

        public override string GetTitle()
        {
            PetInfo petInfo = GetCanStarUpPetInfo();
            return string.Format(base.GetTitle(), pet_helper.GetName(petInfo.Id));
        }

        private PetInfo GetCanStarUpPetInfo()
        {
            PetInfo petInfo = GetCanStarUpPetInfo(PetData.FightingPetList);
            if (petInfo != null)
            {
                return petInfo;
            }

            petInfo = GetCanStarUpPetInfo(PetData.AssistPetList);
            if (petInfo != null)
            {
                return petInfo;
            }

            petInfo = GetCanStarUpPetInfo(PetData.IdlePetList);
            if (petInfo != null)
            {
                return petInfo;
            }
            throw new Exception("找不到可升星的魔灵");
        }

        private PetInfo GetCanStarUpPetInfo(List<PetInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].CanStarUp())
                {
                    return list[i];
                }
            }
            return null;
        }
    }
}
