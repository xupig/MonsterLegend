﻿using Common.Events;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TownTipsSkillUpgrade : BaseTownTips
    {
        public TownTipsSkillUpgrade(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener(SpellEvents.SPELL_CAN_UPGRADE, OnSpellCanUpgrade);
        }

        private void OnSpellCanUpgrade()
        {
            Enqueue();
        }

        public override bool CheckAgain()
        {
            return player_helper.CheckSkill();
        }
    }
}
