﻿#region 模块信息
/*==========================================
// 文件名：TownTipsNewMount
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.MainTown.TownTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/18 15:38:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using GameData;
using MogoEngine.Events;
namespace Common.Data
{
    public class TownTipsNewMount : BaseTownTips
    {
        public TownTipsNewMount(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<int>(MountEvents.CAN_RIDE_NEW_MOUNT, NewMountCanRide);
        }

        private void NewMountCanRide(int mountId)
        {
            Enqueue(mountId);
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }
    }
}