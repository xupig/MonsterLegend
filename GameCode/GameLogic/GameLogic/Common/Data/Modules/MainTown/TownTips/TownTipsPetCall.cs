﻿using Common.Events;
using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsPetCall : BaseTownTips
    {
        private PetData _petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        public TownTipsPetCall(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
            AddEventListener(FunctionEvents.INIT, OnFunctionInit);
        }

        private void OnFunctionInit()
        {
            if (Check())
            {
                Enqueue();
                EventDispatcher.TriggerEvent(TownTipsEvent.Show);
            }
        }

        //private void OnPetDataReady(int mapId)
        //{
        //    if (Check())
        //    {
        //        Enqueue();
        //        EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        //    }
        //}

        private void OnItemUpdate(BagType bagType, uint pos)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo((int)pos);
                if (itemInfo != null && itemInfo.Type == BagItemType.PetFragment)
                {
                    if (Check())
                    {
                        Enqueue();
                    }
                }
            }
        }

        private bool Check()
        {
            return _petData.HaveCanCallPet();
        }

        public override bool CheckAgain()
        {
            return Check();
        }

        public override string GetTitle()
        {
            return string.Format(base.GetTitle(), pet_helper.GetName(_petData.GetCanCallPetId()));
        }
    }
}
