﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TownTipsEquipDecompose : BaseTownTips
    {
        private BagDataManager BagData
        {
            get
            {
                return PlayerDataManager.Instance.BagData;
            }
        }

        public TownTipsEquipDecompose(TownTipsId townTipsId) : base(townTipsId) {}

        protected override void AddListener()
        {
            AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
        }

        private void OnItemUpdate(BagType bagType, uint pos)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = BagData.GetBagData(bagType).GetItemInfo((int)pos);
                if (itemInfo != null && item_helper.IsEquip(itemInfo.Id))
                {
                    EquipItemInfo equipItemInfo = itemInfo as EquipItemInfo;
                    if (item_helper.IsEquipBetter(equipItemInfo) == false)
                    {
                        if (Check())
                        {
                            Enqueue();
                        }
                    }
                }
            }
        }

        public override bool CheckAgain()
        {
            return Check();
        }

        private bool Check()
        {
            Dictionary<int, BaseItemInfo> itemInfoDict = BagData.GetBagData(BagType.ItemBag).GetAllItemInfo();
            int count = 0;
            foreach (BaseItemInfo baseItemInfo in itemInfoDict.Values)
            {
                if (item_helper.IsEquip(baseItemInfo.Id))
                {
                    EquipItemInfo equipItemInfo = baseItemInfo as EquipItemInfo;
                    if (item_helper.IsEquipBetter(equipItemInfo) == false)
                    {
                        count++;
                    }
                }
                if (count >= 7)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
