using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;

using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using GameData;
using Common.Utils;
using UnityEngine;


namespace Common.Data
{
    public class TeamInstanceData
    {
        private int _hasBuyCount;

        /// <summary>
        /// 组队副本队友信息，Key为实体id
        /// </summary>
        public Dictionary<UInt32, PbHpMember> HpMemberDic;

        public List<PbHpMember> HpMemberList;

        private uint _camp_id = 0;

        public bool IsAssigningTeamReward = false;

        public PbCaptainDistInfo PbCaptainDistInfo { get; set; }

        public TeamInstanceData()
        {
            HpMemberDic = new Dictionary<UInt32, PbHpMember>();
            HpMemberList = new List<PbHpMember>();
        }

        public void ResetHpMember()
        {
            HpMemberDic.Clear();
            _camp_id = 0;
        }

        private uint GetOurCampId(PbHpMemberList pbList)
        {
            if (_camp_id == 0)
            {
                for (int i = 0; i < pbList.hpMemberList.Count; i++)
                {
                    if (pbList.hpMemberList[i].dbid == PlayerAvatar.Player.dbid)
                    {
                        _camp_id = pbList.hpMemberList[i].map_camp_id;
                    }
                }
            }
            return _camp_id;
        }

        public void FillHpMember(PbHpMemberList pbList)
        {
            uint ourCampId = GetOurCampId(pbList);
            for (int i = 0; i < pbList.hpMemberList.Count; i++)
            {
                PbHpMember info = pbList.hpMemberList[i];
                if (info.dbid != PlayerAvatar.Player.dbid && ourCampId == info.map_camp_id)
                {
                    if (HpMemberDic.ContainsKey(info.avatar_id) == false)
                    {
                        HpMemberDic.Add(info.avatar_id, info);
                    }
                    else
                    {
                        HpMemberDic[info.avatar_id] = info;
                    }
                    AddHpMemberToList(info);
                }
            }

        }

        private void AddHpMemberToList(PbHpMember info)
        {
            for (int i = 0; i < HpMemberList.Count; i++)
            {
                if (HpMemberList[i].dbid == info.dbid)
                {
                    HpMemberList.RemoveAt(i);
                }
            }
            HpMemberList.Add(info);
        }

        public void UpdateHpMember(uint id, uint hp, uint maxHp)
        {
            if (HpMemberDic.ContainsKey(id))
            {
                PbHpMember info = HpMemberDic[id];
                info.hp = hp;
                info.max_hp = maxHp;
            }
        }

        public void UpdateMemberOnline(uint eid, uint online)
        {
            if (HpMemberDic.ContainsKey(eid))
            {
                PbHpMember info = HpMemberDic[eid];
                info.online = online;
            }
        }

        public void UpdateMemberOnlineByDbid(ulong dbid, uint online)
        {
            PbHpMember member = GetHpMemberInfo(dbid);
            if (member != null)
            {
                member.online = online;
            }
        }

        public PbHpMember GetHpMemberInfo(ulong dbid)
        {
            foreach (PbHpMember member in HpMemberDic.Values)
            {
                if (member.dbid == dbid)
                {
                    return member;
                }
            }
            return null;
        }

        public int HasEnterTimes
        {
            get
            {
                return PlayerDataManager.Instance.CopyData.GetDailyTimesByChapterType(ChapterType.TeamCopy);
            }
        }

        public int MaxEnterTimes
        {
            get
            {
                return inst_type_operate_helper.GetDailyTimes(ChapterType.TeamCopy);
            }
        }

        public int LeftBuyCount
        {
            get
            {
                int leftCount = int.Parse(vip_rights_helper.GetVipRights(VipRightType.TeamMissionTimes, PlayerAvatar.Player.vip_level)) - _hasBuyCount;
                return Math.Max(leftCount, 0);
            }
        }

        public void UpdateDailyTimes(PbPriceInfo pbInfo)
        {
            _hasBuyCount = (int)pbInfo.buy_count;
        }

    }
}
