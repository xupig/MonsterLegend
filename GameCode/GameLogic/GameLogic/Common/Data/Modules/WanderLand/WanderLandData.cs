﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/30 14:25:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Utils;

namespace Common.Data
{
    public class WanderLandData
    {
        public const int INVALID_CHAPTER_PAGE = 0;
        public const int INVALID_CHAPTER_LEVEL = 0;
        public const int INVALID_INSTANCE_ID = 0;
        public const int MAX_INSTANCE_NUM_PER_CHAPTER = 10;

        public int ChapterPage;
        public int ChapterLevel;

        public uint HaveResetTimes;
        public List<WanderLandChapter> ChapterList = new List<WanderLandChapter>();
        private Dictionary<int, WanderLandChapter> _chapterIdDict = new Dictionary<int, WanderLandChapter>();
        private Dictionary<int, WanderLandChapter> _chapterPageDict = new Dictionary<int,WanderLandChapter>();

        public WanderLandData()
        {
            try
            {
                InitChapterList();
                SortChapterList();
                InitInstanceDict();
            }
            catch (Exception ex)
            {
                Debug.LogError("试练秘境初始化报错:" + ex);
            }
        }

        private void InitChapterList()
        {
            foreach (chapters chapter in XMLManager.chapters.Values)
            {
                if ((ChapterType)chapter.__type == ChapterType.WanderLand)
                {
                    WanderLandChapter wanderLandChapter = new WanderLandChapter();
                    wanderLandChapter.ChapterId = chapter.__id;
                    wanderLandChapter.Page = chapter.__page;
                    wanderLandChapter.Chapter = chapter;
                    wanderLandChapter.StarRewardList = chapters_helper.GetStarRewardList(chapter.__id);
                    ChapterList.Add(wanderLandChapter);
                    _chapterIdDict.Add(chapter.__id, wanderLandChapter);
                    _chapterPageDict.Add(chapter.__page, wanderLandChapter);
                }
            }
        }

        private void SortChapterList()
        {
            ChapterList.Sort(SortByPage);
        }

        private int SortByPage(WanderLandChapter x, WanderLandChapter y)
        {
            chapters chapterX = chapters_helper.GetChapter(x.ChapterId);
            chapters chapterY = chapters_helper.GetChapter(y.ChapterId);
            if (chapterX.__page < chapterY.__page)
            {
                return -1;
            }
            return 1;
        }

        private void InitInstanceDict()
        {
            foreach (WanderLandChapter wanderLandChapter in ChapterList)
            {
                chapters chapter = chapters_helper.GetChapter(wanderLandChapter.ChapterId);
                List<ChapterStarReward> starRewardList = chapters_helper.GetStarRewardList(wanderLandChapter.ChapterId);
                wanderLandChapter.InstanceDict = new Dictionary<int, WanderLandInstance>();
                var ids = data_parse_helper.ParseListInt(chapter.__instance_ids);
                for (int i = 0; i < ids.Count; i++)
                {
                    WanderLandInstance wanderLandInstance = new WanderLandInstance();
                    wanderLandInstance.ChapterId = wanderLandChapter.ChapterId;
                    wanderLandInstance.Level = i + 1;
                    int instanceId = ids[i];
                    wanderLandInstance.InstanceId = instanceId;
                    wanderLandChapter.InstanceDict.Add(instanceId, wanderLandInstance);
                }
            }
        }

        public WanderLandChapter GetChapterByPage(int page)
        {
            return _chapterPageDict[page];
        }

        public WanderLandChapter GetChapterByChapterId(int chapterId)
        {
            return _chapterIdDict[chapterId];
        }

        public WanderLandChapter GetChapterByInstanceId(int instanceId)
        {
            int chapterId = chapters_helper.GetChapterId(instanceId);
            return GetChapterByChapterId(chapterId);
        }

        public bool HasCompleteInstance(int instanceId)
        {
            WanderLandChapter chapter = GetChapterByInstanceId(instanceId);
            return chapter.CompleteEver(instanceId);
        }

        public bool InstanceHaveCanGetBox(int instanceId)
        {
            WanderLandChapter chapter = GetChapterByInstanceId(instanceId);
            return chapter.InstanceDict[instanceId].InstaceBoxState == BoxState.CAN_GET;
        }

        public bool HadFighted()
        {
            return ChapterPage != INVALID_CHAPTER_PAGE;
        }

        //20试炼秘境特殊处理
        public bool FightFirstChapter()
        {
            return ChapterPage == 1;
        }

        public bool InstacneHadFight()
        {
            return ChapterLevel != INVALID_CHAPTER_LEVEL;
        }

        public bool ChapterCanFight(WanderLandChapter wanderLandChapter)
        {
            return ChapterCanFight(wanderLandChapter.Page);
        }

        public bool ChapterCanFight(int page)
        {
            if (HadFighted())
            {
                if (FightFirstChapter())
                {
                    return true;
                }
                if (ChapterPage != page)
                {
                    return false;
                }
            }
            return true;
        }

        public bool HaveInstanceTreasureBox(int chapterId)
        {
            WanderLandChapter chapter = GetChapterByChapterId(chapterId);
            for (int i = 0; i < chapter.InstanceList.Count; i++)
            {
                WanderLandInstance wanderlandInstance = chapter.InstanceList[i];
                if (wanderlandInstance.InstaceBoxState == BoxState.CAN_GET)
                {
                    return true;
                }
            }
            return false;
        }


        //协议 
        public void FillWanderLandInfo(PbTryMissionInfo pb)
        {
            if (pb.challenge_page == INVALID_CHAPTER_PAGE)
            {
                ChapterPage = INVALID_CHAPTER_PAGE;
                ChapterLevel = INVALID_CHAPTER_LEVEL;
            }
            else
            {
                ChapterPage = (int)pb.challenge_page;
                int instanceId = (int)pb.today_mission_id;
                if (instanceId == INVALID_CHAPTER_LEVEL)
                {
                    ChapterLevel = INVALID_CHAPTER_LEVEL;
                }
                else
                {
                    ChapterLevel = _chapterPageDict[ChapterPage].InstanceDict[instanceId].Level;
                }
            }
            HaveResetTimes = pb.vip_replace_count;
            //Debug.LogError("page:" + ChapterPage);
            //Debug.LogError("level:" + ChapterLevel);
            //Debug.LogError("HaveResetTimes:" + HaveResetTimes);
            //清理数据
            for (int i = 0; i < ChapterList.Count; i++)
            {
                ChapterList[i].Clear();
            }

            for (int i = 0; i < pb.page_info.Count; i++)
            {
                PbTryPageInfo pbTryPageInfo = pb.page_info[i];
                ChapterList[(int)pbTryPageInfo.page - 1].Fill(pbTryPageInfo);
            }
            //MogoDebugUtils.Print(pb);
        }

        public void FillMissionBox(PbMissionStarInfo pbMissionStarInfo)
        {
            WanderLandChapter wanderLandChapter = GetChapterByInstanceId((int)pbMissionStarInfo.mission_id);
            wanderLandChapter.Fill(pbMissionStarInfo);
        }
        //协议 end
    }
}
