﻿using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class WanderLandChapter
    {
        private WanderLandData _data
        {
            get
            {
                return PlayerDataManager.Instance.WanderLandData;
            }
        }

        public int ChapterId;
        public int Page;
        public int HighestLevel;
        public chapters Chapter;
        public List<ChapterStarReward> StarRewardList;
        public Dictionary<int, WanderLandInstance> InstanceDict;

        private List<WanderLandInstance> _instanceList;
        public List<WanderLandInstance> InstanceList
        {
            get
            {
                if (_instanceList != null)
                {
                    return _instanceList;
                }
                _instanceList = new List<WanderLandInstance>();
                foreach (WanderLandInstance instance in InstanceDict.Values)
                {
                    _instanceList.Add(instance);
                }
                _instanceList.Sort(SortByLevel);
                
                return _instanceList;
            }
        }

        private int SortByLevel(WanderLandInstance x, WanderLandInstance y)
        {
            if (x.Level < y.Level)
            {
                return -1;
            }
            return 1;
        }

        public void Clear()
        {
            HighestLevel = WanderLandData.INVALID_INSTANCE_ID;
            foreach (WanderLandInstance instance in InstanceDict.Values)
            {
                instance.Clear();
            }
        }

        public void Fill(PbTryPageInfo pb)
        {
            int highestInstanceId = (int)pb.highest_mission_id;
            if (highestInstanceId != WanderLandData.INVALID_INSTANCE_ID)
            {
                HighestLevel = InstanceDict[highestInstanceId].Level;
            }
            for (int i = 0; i < pb.reward_info.Count; i++)
            {
                PbTryMissionRewardInfo pbTryMissionRewardInfo = pb.reward_info[i];
                InstanceDict[(int)pbTryMissionRewardInfo.mission_id].Fill(pbTryMissionRewardInfo);
            }
        }

        public void Fill(PbMissionStarInfo pbMissionStarInfo)
        {
            InstanceDict[(int)pbMissionStarInfo.mission_id].Fill(pbMissionStarInfo);
        }

        public int GetFightLevel()
        {
            if ( (_data.ChapterPage == Page) && (_data.InstacneHadFight()) )
            {
                return Mathf.Min(_data.ChapterLevel + 1, WanderLandData.MAX_INSTANCE_NUM_PER_CHAPTER);
            }
            return 1;
        }

        public WanderLandInstance GetInstanceIdByLevel(int level)
        {
            foreach (WanderLandInstance wanderaLandInstance in InstanceDict.Values)
            {
                if (wanderaLandInstance.Level == level)
                {
                    return wanderaLandInstance;
                }
            }
            return null;
        }

        public bool CanSweeep(WanderLandInstance instance)
        {
            int canFightMaxLevel = GetFightLevel();
            if (HighestLevel >= instance.Level)
            {
                if (canFightMaxLevel == instance.Level)
                {
                    return true;
                }
            }
            return false;
        }

        public bool CanFight(int level)
        {
            return GetFightLevel() >= level;
        }

        public bool CompleteEver(int instanceId)
        {
            int level = InstanceDict[instanceId].Level;
            return HighestLevel >= level;
        }
    }
}
