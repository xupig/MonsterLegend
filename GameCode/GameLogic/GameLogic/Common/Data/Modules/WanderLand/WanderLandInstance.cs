﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class WanderLandInstance
    {
        public const int MAX_STAR = 3;
        public const uint HAD_GOT_REWARD_ID = 0;

        public int ChapterId;
        public int Level;
        public instance Instance;
        private int _instanceId;
        public int InstanceId
        {
            get
            {
                return _instanceId;
            }
            set
            {
                Instance = instance_helper.GetInstanceCfg(value);
                _instanceId = value;
                RewardIdList = new List<int>();
                RewardIdList.Add(instance_reward_helper.GetRewardIdListByStarNum(value, 1)[0]);
                RewardIdList.Add(instance_reward_helper.GetRewardIdListByStarNum(value, 2)[0]);
                RewardIdList.Add(instance_reward_helper.GetRewardIdListByStarNum(value, 3)[0]);
            }
        }
        public List<int> RewardIdList;
        public BoxState InstaceBoxState;
        private Dictionary<int, BoxState> _starBoxState = new Dictionary<int,BoxState>();
        public BoxState GetStarBoxState(int star)
        {
            return _starBoxState[star];
        }

        public void Fill(PbMissionStarInfo pbMissionStarInfo)
        {
            //全部领取
            if (pbMissionStarInfo.valid_star == 0)
            {
                for (int i = 1; i <= MAX_STAR; i++)
                {
                    if(_starBoxState[i] == BoxState.CAN_GET)
                    {
                        _starBoxState[i] = BoxState.HAD_GOT;
                    }
                }
            }
            else
            {
                _starBoxState[(int)pbMissionStarInfo.valid_star] = BoxState.HAD_GOT;
            }
            RefreshInstaceBoxState();
        }
    
        public void Fill(PbTryMissionRewardInfo pbTryMissionRewardInfo)
        {
            RefreshStarBoxState(pbTryMissionRewardInfo);
            RefreshInstaceBoxState();
        }

        private void RefreshStarBoxState(PbTryMissionRewardInfo pbTryMissionRewardInfo)
        {
            for (int i = 0; i < pbTryMissionRewardInfo.rewards.Count; i++)
            {
                PbTryRewardInfo pbTryRewardInfo = pbTryMissionRewardInfo.rewards[i];
                _starBoxState[(int)pbTryRewardInfo.valid_star] = pbTryRewardInfo.reward_id == HAD_GOT_REWARD_ID ? BoxState.HAD_GOT : BoxState.CAN_GET;
            }
        }

        private void RefreshInstaceBoxState()
        {
            bool isAllGot = true;
            InstaceBoxState = BoxState.CANNOT_GET;
            foreach (BoxState boxState in _starBoxState.Values)
            {
                if (boxState == BoxState.CAN_GET)
                {
                    InstaceBoxState = BoxState.CAN_GET;
                    return;
                }
                if (boxState != BoxState.HAD_GOT)
                {
                    isAllGot = false;
                }
            }
            if (isAllGot)
            {
                InstaceBoxState = BoxState.HAD_GOT;
            }
        }

        public void Clear()
        {
            _starBoxState.Clear();
            for (int i = 0; i < MAX_STAR; i++)
            {
                _starBoxState.Add(i + 1, BoxState.CANNOT_GET);
            }
            InstaceBoxState = BoxState.CANNOT_GET;
        }
    }
}
