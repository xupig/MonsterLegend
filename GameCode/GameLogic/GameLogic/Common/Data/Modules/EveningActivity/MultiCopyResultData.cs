﻿
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public abstract class MultiCopyResultData
    {
        public int RankNum;
        public uint AvatarId;
        public string AvatarName;
        public uint AvatarLevel;
        public uint FightForce;
        public int IsWin;
        public virtual CopyMultiPlayerLogic Logic { get; set; }

        public abstract List<PbActionInfo> GetActionInfo();

        public abstract List<RewardData> GetRewardList();

        public virtual List<RewardData> GetWinFailReward()
        {
            return new List<RewardData>();
        }

        public virtual string GetWinFailRewardChineseDesc()
        {
            return string.Empty;
        }

        public virtual List<RewardData> GetRankNumReward()
        {
            return new List<RewardData>();
        }

        public virtual string GetRankNumRewardChineseDesc()
        {
            return string.Empty;
        }

        public virtual List<RewardData> GetTotalReward()
        {
            return new List<RewardData>();
        }

        public virtual string GetTotalRewardChineseDesc()
        {
            return string.Empty;
        }

        public virtual List<RewardData> GetTotalNormalRewardList()
        {
            List<RewardData> rewardDataList = GetRewardList();
            return GetNormalReward(rewardDataList);
        }

        public virtual List<RewardData> GetNormalReward(List<RewardData> rewardList)
        {
            List<RewardData> result = new List<RewardData>();
            for (int i = 0; i < rewardList.Count; i++)
            {
                if (rewardList[i].id != public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP
                    && rewardList[i].id != public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD
                    && rewardList[i].id != public_config.MONEY_TYPE_GOLD
                    && rewardList[i].id != public_config.ITEM_SPECIAL_TYPE_EXP)
                {
                    result.Add(rewardList[i]);
                }
            }
            return result;
        }
    }
}
