﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class EveningActivityResultPersonData : MultiCopyResultData
    {
        private PbPVPMissionInfo _originalInfo;
        public ulong dbid;
        private List<PbActionInfo> _condition;
        public Vocation vocation;
        public uint Score;
        public bool IsRewardLimit;
        public int CampId;

        private CopyEveningActivityLogic _logic;

        public EveningActivityResultPersonData(uint avatarId, PbPVPMissionInfo missionInfo, CopyMultiPlayerLogic logic)
        {
            _originalInfo = missionInfo;
            AvatarId = avatarId;
            InitAvatarInfo();
            InitScoreInfo();
            InitConditionInfo();
            Logic = logic;
        }

        public override CopyMultiPlayerLogic Logic
        {
            get
            {
                return _logic;
            }
            set
            {
                _logic = value as CopyEveningActivityLogic;
            }
        }

        public override List<PbActionInfo> GetActionInfo()
        {
            return _condition;
        }

        public override List<RewardData> GetRewardList()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            int insId = map_helper.GetInstanceIDByMapID(mapId);
            List<RewardData> rewardDataList = null;
            int campScore = _logic.GetResultCampInfoByCampId(CampId).CampScore;

            if (IsRewardLimit == false)
            {
                rewardDataList = night_activity_reward_helper.GetTotalReward(insId, (int)AvatarLevel, IsWin == 1, vocation, (int)RankNum, campScore);
            }
            else
            {
                rewardDataList = instance_low_reward_helper.GetRewardData(map_helper.GetMapType(mapId), PlayerAvatar.Player.vocation);
            }
            return rewardDataList;
        }

        public override List<RewardData> GetWinFailReward()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            int insId = map_helper.GetInstanceIDByMapID(mapId);
            if (IsRewardLimit == true)
            {
                return instance_low_reward_helper.GetRewardData(map_helper.GetMapType(mapId), PlayerAvatar.Player.vocation);
            }
            if (map_helper.GetMap(mapId).__reward_way == (int)MapRewardWay.WinFailRank)
            {
                return night_activity_reward_helper.GetWinReward(insId, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation, IsWin == 1);;
            }
            else
            {
                return night_activity_reward_helper.GetScoreReward(insId, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation, _logic.GetResultCampInfoByCampId(CampId).CampScore);
            }
        }

        public override string GetWinFailRewardChineseDesc()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            if (IsRewardLimit == true)
            {
                return MogoLanguageUtil.GetContent(97043);
            }
            if (map_helper.GetMap(mapId).__reward_way == (int)MapRewardWay.WinFailRank)
            {
                if (IsWin == 1)
                {
                    return MogoLanguageUtil.GetContent(97041);
                }
                else
                {
                    return MogoLanguageUtil.GetContent(97042);
                }
            }
            else
            {
                return MogoLanguageUtil.GetContent(97040);
            }
        }

        public override List<RewardData> GetRankNumReward()
        {
            if (IsRewardLimit == false)
            {
                int mapId = GameSceneManager.GetInstance().curMapID;
                int insId = map_helper.GetInstanceIDByMapID(mapId);

                List<RewardData> rankReward = night_activity_reward_helper.GetRankReward(insId, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation, RankNum);

                return GetNormalReward(rankReward);
            }
            return new List<RewardData>();
        }

        public override string GetRankNumRewardChineseDesc()
        {
            return MogoLanguageUtil.GetContent(97021);
        }

        public override List<RewardData> GetTotalReward()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            int insId = map_helper.GetInstanceIDByMapID(mapId);
            if (IsRewardLimit == true)
            {
                return instance_low_reward_helper.GetRewardData(map_helper.GetMapType(mapId), PlayerAvatar.Player.vocation);
            }
            else
            {
                return night_activity_reward_helper.GetTotalReward(insId, PlayerAvatar.Player.level, IsWin == 1, PlayerAvatar.Player.vocation, RankNum, _logic.GetResultCampInfoByCampId(CampId).CampScore);
            }
        }

        public override string GetTotalRewardChineseDesc()
        {
            return MogoLanguageUtil.GetContent(97022);
        }

        private void InitAvatarInfo()
        {
            for (int j = 0; j < _originalInfo.rank_info.Count; j++)
            {
                for (int i = 0; i < _originalInfo.rank_info[j].player_info.Count; i++)
                {
                    if (_originalInfo.rank_info[j].player_info[i].avatar_id == AvatarId)
                    {
                        PbPlayerInfo playerInfo = GetPlayerInfoById(AvatarId);
                        if (playerInfo != null)
                        {
                            dbid = playerInfo.avatar_dbid;
                            RankNum = (int)_originalInfo.rank_info[j].ranking;
                            AvatarName = MogoProtoUtils.ParseByteArrToString(playerInfo.name);
                            AvatarLevel = playerInfo.level;
                            FightForce = playerInfo.fight_force;
                            vocation = (Vocation)playerInfo.vocation;
                            IsRewardLimit = (playerInfo.is_reward_limit == 1);
                            IsWin = (int)playerInfo.is_win;
                            CampId = (int)playerInfo.group_id;
                        }
                    }
                }
            }

        }

        private void InitScoreInfo()
        {
            for (int i = 0; i < _originalInfo.action_info_list.Count; i++)
            {
                if (_originalInfo.action_info_list[i].avatar_id == AvatarId)
                {
                    List<PbActionInfo> actionInfoList = _originalInfo.action_info_list[i].action_info;
                    for (int j = 0; j < actionInfoList.Count; j++)
                    {
                        if (actionInfoList[j].action_id == public_config.MISSION_ACTION_SELF_SCORE)
                        {
                            Score = (uint)actionInfoList[j].val;
                        }
                    }
                }
            }
        }

        private void InitConditionInfo()
        {
            for (int i = 0; i < _originalInfo.action_info_list.Count; i++)
            {
                if (_originalInfo.action_info_list[i].avatar_id == AvatarId)
                {
                    _condition = _originalInfo.action_info_list[i].action_info;
                }
            }
        }

        private PbPlayerInfo GetPlayerInfoById(ulong avatarId)
        {
            for (int i = 0; i < _originalInfo.players_info.Count; i++)
            {
                if (_originalInfo.players_info[i].avatar_id == avatarId)
                {
                    return _originalInfo.players_info[i];
                }
            }
            return null;
        }

        private uint GetOurGroupId()
        {
            List<PbPlayerInfo> playerInfoList = _originalInfo.players_info;
            for (int i = 0; i < playerInfoList.Count; i++)
            {
                if (playerInfoList[i].avatar_id == PlayerAvatar.Player.id)
                {
                    return playerInfoList[i].group_id;
                }
            }
            return 0;
        }
    }
}
