﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class EveningActivityResultCampData
    {
        public int CampId;
        public int CampScore;

        public PbGroupScoreInfo GroupScoreInfo;

        public EveningActivityResultCampData(PbGroupScoreInfo groupScoreInfo)
        {
            CampId = (int)groupScoreInfo.group_id;
            CampScore = groupScoreInfo.score;
            GroupScoreInfo = groupScoreInfo;
        }
    }
}
