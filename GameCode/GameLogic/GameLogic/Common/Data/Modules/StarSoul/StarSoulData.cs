using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;

using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using GameData;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using MogoEngine.Events;
using Common.Events;


namespace Common.Data
{
    public class StarSoulData
    {

        /// <summary>
        /// 所有星点数据,第一个Key为星点Page,第二个Key为星点Level
        /// </summary>
        private Dictionary<int, Dictionary<int, StarItemData>> _starItemDataDict;
        /// <summary>
        /// 按照Page,Level优先级排序后的列表
        /// </summary>
        private List<StarItemData> _sortList;
        /// <summary>
        /// 已点亮的星星数量
        /// </summary>
        private int _lightCount;
        /// <summary>
        /// 当前点亮星星Page
        /// </summary>
        private int _currentPage;

        public StarSoulData()
        {
            _starItemDataDict = new Dictionary<int, Dictionary<int, StarItemData>>();
            _sortList = new List<StarItemData>();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.star_spirit_num, RefreshStarSoulPoint);
        }

        private void RefreshStarSoulPoint()
        {
            if (CanLight())
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.starsoul);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.starsoul);
            }
            EventDispatcher.TriggerEvent(StarSoulEvents.REFRESH_STAR_PAGE_POINT);
        }

        public bool CanLight()
        {
            return _lightCount < _sortList.Count && PlayerAvatar.Player.star_spirit_num >= GetNextStarCost();
        }

        public bool CanLight(int page, int level)
        {
            int cost = 0;
            for (int i = _lightCount; i < _sortList.Count; i++)
            {
                cost += _sortList[i].Cost;
                if (_sortList[i].Page == page && _sortList[i].Level == level)
                {
                    break;
                }
            }
            return PlayerAvatar.Player.star_spirit_num >= cost;
        }

        public void Fill(int count)
        {
            _lightCount = count;
            InitConfig();
            UpdateStarState();
            RefreshStarSoulPoint();
        }

        private void InitConfig()
        {
            foreach (int id in XMLManager.star_spirit.Keys)
            {
                StarItemData itemData = new StarItemData(id);
                int page = itemData.Page;
                if (_starItemDataDict.ContainsKey(page) == false)
                {
                    _starItemDataDict.Add(page, new Dictionary<int, StarItemData>());
                }
                Dictionary<int, StarItemData> pageDict = _starItemDataDict[page];
                int level = itemData.Level;
                pageDict.Add(level, itemData);
                _sortList.Add(itemData);
            }
            _sortList.Sort(sortFunc);
        }

        public void AddLightStar(PbStarSpirit pbStar)
        {
            for (int i = _lightCount; i < _sortList.Count; i++)
            {
                _lightCount++;
                if (_sortList[i].Page == pbStar.page && _sortList[i].Level == pbStar.level)
                {
                    break;
                }
            }
            UpdateStarState();
            RefreshStarSoulPoint();
        }

        public int GetLightStarCount()
        {
            return _lightCount;
        }

        /// <summary>
        /// 获得某一页的星点信息，Key为星点Level
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public StarItemData GetItemDataByPageAndLevel(int page, int level)
        {
            return _starItemDataDict[page][level];
        }

        public int GetPageCount()
        {
            return _starItemDataDict.Count;
        }

        public int GetStarCountByPage(int page)
        {
            return _starItemDataDict[page].Count;
        }

        public int GetCurrentPage()
        {
            return _currentPage;
        }

        private static int sortFunc(StarItemData data1, StarItemData data2)
        {
            if (data1.Page != data2.Page)
            {
                return data1.Page - data2.Page;
            }
            return data1.Level - data2.Level;
        }

        private void UpdateStarState()
        {
            _currentPage = GetPageCount();
            for (int i = 0; i < _lightCount; i++)
            {
                _sortList[i].StarState = StarState.IsActivated;
            }
            if (_lightCount < _sortList.Count)
            {
                _currentPage = _sortList[_lightCount].Page;
            }
            for (int i = _lightCount; i < _sortList.Count; i++)
            {
                _sortList[i].StarState = StarState.NotActivate;
            }
        }

        public StarItemData GetCurrentStarItemData()
        {
            return _sortList[_lightCount - 1];
        }

        private int GetNextStarCost()
        {
            int result = 0;
            if (_sortList.Count > _lightCount)
            {
                result = _sortList[_lightCount].Cost;
            }
            return result;
        }

    }
}
