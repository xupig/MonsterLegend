﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using Common.Utils;
using Common.Structs.ProtoBuf;
using GameMain.Entities;

namespace Common.Structs
{
    public class StarItemData
    {
        /// <summary>
        /// 基础配置来自star_spirit.xml
        /// </summary>
        private star_spirit _baseConfig;

        public StarItemData(int id)
        {
            Id = id;
            _baseConfig = XMLManager.star_spirit[Id];
        }

        /// <summary>
        /// 星点ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 分页
        /// </summary>
        public int Page
        { 
            get
            { 
                return _baseConfig.__page; 
            }
        }

        /// <summary>
        /// 等级
        /// </summary>
        public int Level
        {
            get
            {
                return _baseConfig.__level;
            }
        }

        /// <summary>
        /// 星点名字
        /// </summary>
        public string Name
        {
            get
            {
                return MogoLanguageUtil.GetContent(_baseConfig.__name);
            }
        }

        /// <summary>
        /// 图标名称(未激活)
        /// </summary>
        public int NotActivateIcon
        {
            get
            {
                List<string> dataList = data_parse_helper.ParseListString(_baseConfig.__icon);
                return int.Parse(dataList[0]);
            }
        }

        /// <summary>
        /// 图标名称(可激活)
        /// </summary>
        public int CanActivateIcon
        {
            get
            {
                List<string> dataList = data_parse_helper.ParseListString(_baseConfig.__icon);
                return int.Parse(dataList[1]);
            }
        }

        /// <summary>
        /// 图标名称(已激活)
        /// </summary>
        public int IsActivatedIcon
        {
            get
            {
                List<string> dataList = data_parse_helper.ParseListString(_baseConfig.__icon);
                return int.Parse(dataList[2]);
            }
        }

        /// <summary>
        /// 点亮星点消耗星魂数
        /// </summary>
        public int Cost
        {
            get
            {
                return _baseConfig.__cost;
            }
        }

        /// <summary>
        /// 效果Id
        /// </summary>
        public int EffectId
        {
            get
            {
                return _baseConfig.__effect_id;
            }
        }

        public int RewardId
        {
            get
            {
                return _baseConfig.__item_reward;
            }
        }

        /// <summary>
        /// 星点当前状态
        /// </summary>
        public StarState StarState
        {
            get;
            set;
        }
    }

    public enum StarState
    {
        NotActivate = 1,
        IsActivated = 2,
    }
}
