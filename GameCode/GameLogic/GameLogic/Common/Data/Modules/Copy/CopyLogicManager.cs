﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyLogicManager
    {
        protected static CopyLogicManager _instance;

        public static CopyLogicManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CopyLogicManager();
                }
                return _instance;
            }
        }

        private Dictionary<ChapterType, CopyBaseLogic> _logicDict;

        private CopyBaseLogic _currentLogic;

        private CopyLogicManager()
        {
            _logicDict = new Dictionary<ChapterType, CopyBaseLogic>();
            //翻牌界面
            _logicDict.Add(ChapterType.NewPlayer, new CopyNewPlayerLogic());
            _logicDict.Add(ChapterType.Copy, new CopyCopyLogic());
            _logicDict.Add(ChapterType.WanderLand, new CopyWanderLandLogic());
            _logicDict.Add(ChapterType.TeamCopy, new CopyTeamCopyLogic());
            _logicDict.Add(ChapterType.GuildCopy, new CopyGuildCopyLogic());
            _logicDict.Add(ChapterType.Portal, new CopyPortalLogic());
            _logicDict.Add(ChapterType.PetDefence, new CopyPetDefenceLogic());
            //单人pk界面
            _logicDict.Add(ChapterType.Single, new CopySingleLogic());
            _logicDict.Add(ChapterType.PK, new CopyPKLogic());
            //单人积分界面
            _logicDict.Add(ChapterType.DemonGate, new CopyDemonGateLogic());
            //多人结算界面
            _logicDict.Add(ChapterType.EveningActivity, new CopyEveningActivityLogic());
            _logicDict.Add(ChapterType.CombatPortal, new CopyCombatPortalLogic());
            //公会战结算界面
            _logicDict.Add(ChapterType.GuildTry, new CopyGuildTryLogic());
            _logicDict.Add(ChapterType.GuildWarGroup, new CopyGuildWarLogic());
            _logicDict.Add(ChapterType.Branch, new CopyBranchLogic());
            _logicDict.Add(ChapterType.Boom, new CopyBoomLogic());
            _logicDict.Add(ChapterType.Duel, new CopyDuelLogic());
            _logicDict.Add(ChapterType.GuildBoss, new CopyCopyLogic());

            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_SCENE, EnterScene);
        }

        private void EnterScene(int mapId)
        {
            foreach (KeyValuePair<ChapterType, CopyBaseLogic> kvp in _logicDict)
            {
                kvp.Value.Valid = false;
            }
        }

        private CopyBaseLogic GetLogic(ChapterType chapterType)
        {
            if (_logicDict.ContainsKey(chapterType))
            {
                return _logicDict[chapterType];
            }
            return null;
        }

        public void ProcessResult(ChapterType chapterType, object pb, CopyInstanceData instanceData = null)
        {
            _currentLogic = _logicDict[chapterType];
            _currentLogic.ProcessResult(pb, instanceData);
        }

        public void HandlePlayAgainResult(UInt32 errorId, byte[] byteArr)
        {
            _currentLogic.HandlePlayAgainResult(errorId, byteArr);
        }

        public void RequestEnterCopy(ChapterType chapterType, Action callback, int instanceId = instance_helper.InvalidInstanceId)
        {
            CopyBaseLogic logic = GetLogic(chapterType);
            if (logic == null)
            {
                callback.Invoke();
                return;
            }
            if (!LevelCheck(instanceId))
            {
                return;
            }
            if (!TeamCheck(logic, chapterType, callback, instanceId))
            {
                return;
            }
           
            callback.Invoke();
        }

        private bool IsInTeamTask(ChapterType chapterType, int instanceId)
        {
            /*
            if (PlayerDataManager.Instance.TaskData.isTeammateInTeamTask())//在时空裂隙中，并且自己不是队长
            {
                //领奖，退队，进副本
                string content = string.Empty;
                if (PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH)
                {
                    content = MogoLanguageUtil.GetContent(115019);
                }
                else
                {
                    content = MogoLanguageUtil.GetContent(1008, GetLogic(chapterType).GetCopyName(instanceId));
                }
          //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, delegate()
                {
                    TaskManager.Instance.QuitTeamTask();
                });
                return true;
            }*/
            return false;
        }

        private bool LevelCheck(int instanceId)
        {
            if (instanceId != instance_helper.InvalidInstanceId)
            {
                int minLevel = instance_helper.GetMinLevel(instanceId);
                string copyName = MogoLanguageUtil.GetContent(instance_helper.GetName(instanceId));
                if (PlayerAvatar.Player.level < minLevel)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.LevelUpChannel);
                    return false;
                }
                int maxLevel = instance_helper.GetMaxLevel(instanceId);
                if (PlayerAvatar.Player.level > maxLevel)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(1003, copyName, maxLevel), PanelIdEnum.Copy);
                    return false;
                }
            }
            return true;
        }

        private bool TeamCheck(CopyBaseLogic logic, ChapterType chapterType, Action callback, int instanceId)
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (!logic.CanEnterByTeam(instanceId))//副本不能组队进入，但处于组队状态
            {
                if (teamData.IsInTeam())
                {
                    if (IsInTeamTask(chapterType, instanceId))
                    {
                        return false;
                    }
                    else
                    {
                        string content = MogoLanguageUtil.GetContent(1008, GetLogic(chapterType).GetCopyName(instanceId));
                  //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, callback);
                        return false;
                    }
                }
            }
            else
            {
                //能组队进入
                if (instanceId != instance_helper.InvalidInstanceId)
                {
                    InstanceTeamType teamType = instance_helper.GetTeamType(instanceId);
                    if (teamType == InstanceTeamType.Team)
                    {
                        string copyName = MogoLanguageUtil.GetContent(instance_helper.GetName(instanceId));
                        if (teamData.IsInTeam() && !teamData.IsCaptain())
                        {
                           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(1011, copyName), PanelIdEnum.Copy);
                            return false;
                        }
                        int minPlayerNum = instance_helper.GetMinPlayerNum(instanceId);
                        if (minPlayerNum != 1 && teamData.TeammateDic.Count < minPlayerNum)
                        {
                           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(1010, copyName, minPlayerNum), PanelIdEnum.Copy);
                            return false;
                        }

                        int maxPlayerNum = instance_helper.GetMaxPlayerNum(instanceId);
                        if (teamData.TeammateDic.Count < maxPlayerNum)
                        {
                            int num = Mathf.Max(teamData.TeammateDic.Count, 1);
                      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(1009, copyName, maxPlayerNum, num), callback, CancelAction);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public void RequestCallPeople(ChapterType chapterType, Action callback, int instanceId = instance_helper.InvalidInstanceId)
        {
            CopyBaseLogic logic = GetLogic(chapterType);
            if (logic == null)
            {
                callback.Invoke();
                return;
            }
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (!logic.CanEnterByTeam(instanceId))//副本不能组队进入
            {
                return;
            }
            int maxNum = instance_helper.GetMaxPlayerNum(instanceId);
            if (teamData.TeammateDic.Count >= maxNum)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83033), PanelIdEnum.Copy);
                return;
            }
            callback.Invoke();
        }

        private void CancelAction() {}

        public bool CanPetFight(ChapterType chapterType, int mapId)
        {
            CopyBaseLogic logic = GetLogic(chapterType);
            if (logic == null)
            {
                return inst_type_operate_helper.CanPetFight(chapterType);
            }
            return logic.CanPetFight(mapId);
        }

        public void ShowTeamLogicPanel()
        {
            CopyTeamCopyLogic teamLogic = _currentLogic as CopyTeamCopyLogic;
            if (teamLogic != null && teamLogic.Valid)
            {
                teamLogic.DoShowPanel();
            }
        }

        public void ShowGuildTryLogicPanel()
        {
            CopyGuildTryLogic guildTryLogic = _currentLogic as CopyGuildTryLogic;
            if (guildTryLogic != null)
            {
                guildTryLogic.ShowEntryPanel();
            }
        }

        public void FillPreviewData(ChapterType chapterType, object pb)
        {
            CopyGuildWarBaseLogic logic = GetLogic(chapterType) as CopyGuildWarBaseLogic;
            logic.FillPreviewData(pb);
        }

        public void ShowPreviewPanel(ChapterType chapterType)
        {
            CopyGuildWarBaseLogic logic = GetLogic(chapterType) as CopyGuildWarBaseLogic;
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarBattlePreview, logic);
        }
    }
}
