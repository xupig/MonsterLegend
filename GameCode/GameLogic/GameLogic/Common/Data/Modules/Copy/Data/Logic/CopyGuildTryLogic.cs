﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyGuildTryLogic : CopyGuildWarBaseLogic
    {
        private bool needShowEntryPanel = false;

        public CopyGuildTryLogic()
        {
            chapterType = ChapterType.GuildTry;
        }

        public override bool ShowRound()
        {
            return false;
        }

        public override bool ShowFinish()
        {
            return true;
        }

        public override string GetScoreDesc()
        {
            return MogoLanguageUtil.GetContent(111532);
        }

        public override CopyResult GetModelAction()
        {
            return CopyResult.Win;
        }

        protected override void FillData(object pb)
        {
            base.FillData(pb);
            needShowEntryPanel = true;
        }

        public void ShowEntryPanel()
        {
            if (needShowEntryPanel)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarTry, false);
                needShowEntryPanel = false;
            }
        }
    }
}
