﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/11 17:57:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using Common.Utils;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class CopyChapterBoxData
    {
        public bool IsGotReward;
    }

    public class CopyChapterData
    {
        private const uint IS_GOT_REWARD_ID = 0;

        public int id;
        public int TotalStarNum = 0;
        public bool IsCopyOpen = false;//剧情副本是否开启

        private Dictionary<int, CopyChapterBoxData> _gotRewardDict = new Dictionary<int, CopyChapterBoxData>();
        public List<PbStarSpiritReward> GotRewardList 
        { 
            set
            {
                foreach (PbStarSpiritReward isGetReward in value)
                {
                    int starNum = (int)isGetReward.star_num;
                    CopyChapterBoxData boxData = GetBoxData(starNum);
                    boxData.IsGotReward = isGetReward.reward_id == IS_GOT_REWARD_ID;
                }
            }
        }

        public CopyChapterData(int id)
        {
            this.id = id;
        }

        private CopyChapterBoxData GetBoxData(int starNum)
        {
            CopyChapterBoxData boxData;
            if (_gotRewardDict.ContainsKey(starNum))
            {
                boxData = _gotRewardDict[starNum];
            }
            else
            {
                boxData = new CopyChapterBoxData();
                _gotRewardDict.Add(starNum, boxData);
            }
            return boxData;
        }

        public BoxState GetBoxState(int starNum)
        {
            if (_gotRewardDict.ContainsKey(starNum))
            {
                if (_gotRewardDict[starNum].IsGotReward)
                {
                    return BoxState.HAD_GOT;
                }
                else
                {
                    return BoxState.CAN_GET;
                }
            }
            return BoxState.CANNOT_GET;
        }

        public void FillChapterDataByGetChapterRecord(PbChapterInfo pbChapterInfo)
        {
            IsCopyOpen = true;
            GotRewardList = pbChapterInfo.star_spirit_reward;
            TotalStarNum = (int) pbChapterInfo.total_star_spirit;
        }

        public List<CopyProgressBoxDataWrapper> FillChapterDataByGetStarReward(List<PbStarSpiritReward> pbStarSpiritRewardList)
        {
            List<CopyProgressBoxDataWrapper> copyBoxDataList = new List<CopyProgressBoxDataWrapper>();
            foreach (PbStarSpiritReward isGetReward in pbStarSpiritRewardList)
            {
                int starNum = (int)isGetReward.star_num;
                CopyChapterBoxData boxData = GetBoxData(starNum);
                bool hadGotReward = boxData.IsGotReward;
                boxData.IsGotReward = isGetReward.reward_id == IS_GOT_REWARD_ID;
                if (boxData.IsGotReward && !hadGotReward)
                {
                    copyBoxDataList.Add(new CopyProgressBoxDataWrapper(id, starNum));
                }
            }
            return copyBoxDataList;
        }
    }
}
