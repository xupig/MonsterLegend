﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyPreviewItemDataWrapper
    {
        public string Name;
        public int Vocation;
        public string GuildName;
        public uint Level;
        public uint FightValue;

        public string MilitaryRank;//段位

        public CopyPreviewItemDataWrapper(string name, int vocation, string guildName, uint level, uint fightValue, string rank)
        {
            this.Name = name;
            this.Vocation = vocation;
            this.GuildName = guildName;
            this.Level = level;
            this.FightValue = fightValue;
            this.MilitaryRank = rank;
        }
    }
}
