﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    abstract public class CopyGuildWarBaseLogic : CopyBaseLogic
    {
        public int MapId;
        public int MissionId;
        public CopyResult Result;
        public int Score;
        public int Round;
        public bool DuringActivity;

        public CopyPreviewItemDataWrapper LeftWrapper;
        public CopyPreviewItemDataWrapper RightWrapper;

        abstract public bool ShowRound();
        abstract public string GetScoreDesc();
        abstract public bool ShowFinish();
        abstract public CopyResult GetModelAction();

        protected override void FillData(object pb)
        {
            PbGuildMissionResult pbGuildMissionResult = pb as PbGuildMissionResult;
            MapId = (int)pbGuildMissionResult.map_id;
            MissionId = map_helper.GetInstanceIDByMapID(MapId);
            Result = (CopyResult)pbGuildMissionResult.is_win;
            Score = (int)pbGuildMissionResult.score;
            Round = (int)pbGuildMissionResult.round;
        }

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarResult, this);
        }

        public virtual void FillPreviewData(object pb) {}
        public virtual bool NeedShowPreviewTitle() { return false; }

        public virtual bool NeedLLeaveScene()
        {
            return true;
        }

        public virtual bool ShowRewardItem()
        {
            return false;
        }

        public virtual void CloseOtherPanel(){}
    }
}
