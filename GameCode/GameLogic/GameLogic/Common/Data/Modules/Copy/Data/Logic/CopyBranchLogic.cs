﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyBranchLogic : CopyWinLoseLogic
    {

        public CopyBranchLogic()
        {
            chapterType = ChapterType.Branch;
        }

        protected override void FillData(object pb)
        {
            if (pb is PbPVPMissionInfo)
            {
                PbPVPMissionInfo pbResult = pb as PbPVPMissionInfo;
                Result = pbResult.is_win == 1 ? CopyResult.Win : CopyResult.Lose;
            }
            else if (pb is PbMissionInfoNew)
            {
                PbMissionInfoNew pbResult = pb as PbMissionInfoNew;
                Result = pbResult.is_win == 1 ? CopyResult.Win : CopyResult.Lose;
            }
            BaseItemDataList.Clear();
        }

    }
}
