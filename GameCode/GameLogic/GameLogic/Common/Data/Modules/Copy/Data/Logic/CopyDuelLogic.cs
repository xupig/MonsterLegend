﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyDuelLogic : CopyGuildWarBaseLogic
    {
        private PbDuelStartMatch _pbDuelStartMatch;

        public CopyDuelLogic()
        {
            chapterType = ChapterType.Duel;
        }

        protected override void FillData(object pb)
        {
            PbDuelMatchResult pbDuelMatchResult = pb as PbDuelMatchResult;
            switch (pbDuelMatchResult.win_tag)
            {
                case 0:
                    Result = CopyResult.Lose;
                    break;
                case 1:
                    Result = CopyResult.Win;
                    break;
                case 2:
                    Result = CopyResult.Draw;
                    break;
            }
            Score = pbDuelMatchResult.rank_show_change;
            DuringActivity = pbDuelMatchResult.is_rank_mode == 1;

        }

        public override bool ShowRound()
        {
            return false;
        }

        public override bool ShowFinish()
        {
            return false;
        }

        public override CopyResult GetModelAction()
        {
            return Result;
        }

        public override bool ShowRewardItem()
        {
            return true;
        }

        public override string GetScoreDesc()
        {
            return MogoLanguageUtil.GetContent(122110);
        }

        public override void FillPreviewData(object pb)
        {
            PbDuelStartMatch pbDuelStartMatch = pb as PbDuelStartMatch;
            _pbDuelStartMatch = pbDuelStartMatch;
            PbDuelTarget pbDuelTarget = _pbDuelStartMatch.invitee;
            LeftWrapper = new CopyPreviewItemDataWrapper(pbDuelTarget.name, (int)pbDuelTarget.vocation, pbDuelTarget.guild_name, pbDuelTarget.level, pbDuelTarget.fight_force, duel_rank_reward_helper.GetName(pbDuelTarget.rank_show, pbDuelTarget.rank_level));
            pbDuelTarget = _pbDuelStartMatch.sponsor;
            RightWrapper = new CopyPreviewItemDataWrapper(pbDuelTarget.name, (int)pbDuelTarget.vocation, pbDuelTarget.guild_name, pbDuelTarget.level, pbDuelTarget.fight_force, duel_rank_reward_helper.GetName(pbDuelTarget.rank_show, pbDuelTarget.rank_level));
        }

        public override bool NeedLLeaveScene()
        {
            if (GameSceneManager.GetInstance().chapterType == ChapterType.Duel)
            {
                return true;
            }
            return false;
        }

        public override void CloseOtherPanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Duel);
        }
    }
}
