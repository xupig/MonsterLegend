﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyBoomLogic : CopyBaseLogic
    {
        public CopyResult Result;

        public CopyBoomLogic()
        {
            chapterType = ChapterType.Boom;
        }

        protected override void FillData(object pb)
        {
            PbMissionInfoNew pbResult = pb as PbMissionInfoNew;
            Result = pbResult.is_win == 1 ? CopyResult.Win : CopyResult.Lose;
        }

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.CopyTaskResult, this);
        }

    }
}
