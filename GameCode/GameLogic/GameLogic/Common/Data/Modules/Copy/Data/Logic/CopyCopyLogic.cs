﻿using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyCopyLogic : CopyCardLogic
    {
        public CopyCopyLogic()
        {
            chapterType = ChapterType.Copy;
        }

        protected override void FillData(object pb)
        {
            PbMissionInfoNew pbMissionInfo = pb as PbMissionInfoNew;
            MissionId = map_helper.GetInstanceIDByMapID((int)pbMissionInfo.map_id);
            MapId = (int)pbMissionInfo.map_id;
            Result = (CopyResult)pbMissionInfo.is_win;
            StarNum = (int)pbMissionInfo.grade;
            BoxReward = pbMissionInfo.box_reward;
            DropReward = pbMissionInfo.drop_reward;
            PetExpList = pbMissionInfo.pet_exps;
            ConditionResultList = pbMissionInfo.action_info;
        }

        protected override void FillInstanceData(object pb, CopyInstanceData instanceData)
        {
            PbMissionInfoNew pbMissionInfo = pb as PbMissionInfoNew;
            instanceData.DailyTimes = (int)pbMissionInfo.daily_count;
            instanceData.nowStar = (int)pbMissionInfo.grade;
        }

        public override bool CanPetFight(int mapId)
        {
            int instanceId = map_helper.GetInstanceIDByMapID(mapId);
            InstanceTeamType teamType = instance_helper.GetTeamType(instanceId);
            if (teamType == InstanceTeamType.Team)
            {
                TeamData teamData = PlayerDataManager.Instance.TeamData;
                if (teamData.IsInTeam())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return base.CanPetFight(mapId);
        }
    }
}
