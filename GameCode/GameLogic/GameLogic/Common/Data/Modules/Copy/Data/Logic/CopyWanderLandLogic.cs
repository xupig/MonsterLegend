﻿using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyWanderLandLogic : CopyCopyLogic
    {
        public CopyWanderLandLogic()
        {
            chapterType = ChapterType.WanderLand;
        }

        public override bool CanShowNext()
        {
            return false;
        }

        public override bool CanShowStatistic()
        {
            return true;
        }
    }
}
