﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyWinLoseLogic : CopyBaseLogic
    {
        public CopyResult Result;
        public List<BaseItemData> BaseItemDataList = new List<BaseItemData>();


        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandResult, this);
        }

        protected override void FillData(object pb)
        {

        }
    }
}
