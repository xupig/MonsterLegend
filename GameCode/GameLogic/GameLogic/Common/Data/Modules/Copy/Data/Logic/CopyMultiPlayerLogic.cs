﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    abstract public class CopyMultiPlayerLogic : CopyBaseLogic
    {
        abstract public Dictionary<int, string> GetConditionchinese();

        abstract public Dictionary<int, int> GetActionId2ConditionIdDic();

        abstract public string GetTeamScore();

        abstract public string GetTeamLabel();

        abstract public int GetMaxNotSpecialRewardCount();

        abstract public List<MultiCopyResultData> GetResultPersonDataList();

        abstract public int GetLeftScoreValue();

        abstract public int GetRightScoreValue();

        abstract public string GetLeftScoreName();

        abstract public string GetRightScoreName();

        abstract public bool IsShowCombatIntegral();

        abstract public bool IsWin();

        abstract public MultiCopyResultData GetCopyResultPersonData(uint avatarId);


        virtual public int SortFunc(MultiCopyResultData first, MultiCopyResultData second)
        {
            return (int)first.RankNum - (int)second.RankNum;
        }
        
    }
}
