﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyTeamCopyLogic : CopyCardLogic
    {
        public CopyTeamCopyLogic()
        {
            chapterType = ChapterType.TeamCopy;
        }

        protected override void ShowPanel()
        {
            if (PlayerDataManager.Instance.TeamInstanceData.IsAssigningTeamReward == false)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.CopyResult, this);
            }
        }

        public void DoShowPanel()
        {
            ShowPanel();
        }

        protected override void FillData(object pb)
        {
            PbTeamMissionResultNew pbMissionInfo = pb as PbTeamMissionResultNew;
            MissionId = map_helper.GetInstanceIDByMapID((int)pbMissionInfo.map_id);
            MapId = (int)pbMissionInfo.map_id;
            Result = (CopyResult)pbMissionInfo.is_win;
            StarNum = (int)pbMissionInfo.grade;
            BoxReward = pbMissionInfo.box_reward;
            DropReward = pbMissionInfo.drop_reward;
            ConditionResultList = pbMissionInfo.action_info;
            TeammateList = GetTeammateInfoList(pbMissionInfo);
        }

        private List<PbTeamMissionPlayerInfo> GetTeammateInfoList(PbTeamMissionResultNew pbMissionInfo)
        {
            List<PbTeamMissionPlayerInfo> result = new List<PbTeamMissionPlayerInfo>();
            for (int i = 0; i < pbMissionInfo.players_info.Count; i++)
            {
                if (pbMissionInfo.players_info[i].avatar_dbid != PlayerAvatar.Player.dbid)
                {
                    result.Add(pbMissionInfo.players_info[i]);
                }
            }
            return result;
        }

        protected override void FillInstanceData(object pb, CopyInstanceData instanceData)
        {
            PbTeamMissionResultNew pbMissionInfo = pb as PbTeamMissionResultNew;
            instanceData.DailyTimes = (int)pbMissionInfo.daily_count;
            instanceData.nowStar = (int)pbMissionInfo.grade;
        }

        public override bool ShowTeamExp()
        {
            return true;
        }
    }
}
