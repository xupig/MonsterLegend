﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyCardLogic : CopyBaseLogic
    {
        public int MissionId;
        public int MapId;
        public CopyResult Result;
        public int StarNum;
        public List<PbBoxReward> BoxReward;
        public List<PbItem> DropReward;
        //星级奖励是有星才发，没星不发
        public List<PbActionInfo> ConditionResultList;

        public List<PbPetExpInfo> PetExpList;
        public List<PbTeamMissionPlayerInfo> TeammateList;

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.CopyResult, this);
        }

        public virtual bool CanShowNext()
        {
            return Result == CopyResult.Win;
        }

        public virtual bool CanShowStatistic()
        {
            return false;
        }

        public virtual bool ShowTeamExp()
        {
            return false;
            //InstanceTeamType teamType = instance_helper.GetTeamType(MissionId);
            //return teamType == InstanceTeamType.Team;
        }

        protected override void FillData(object pb)
        {
        }

        public override void HandlePlayAgainResult(uint errorId, byte[] byteArr)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.CopyResult);
            PlayerDataManager.Instance.PlayAgainData.FillPlayAgainData(errorId, byteArr);
        }

        protected override void TriggerCopyResultEvent()
        {
            EventDispatcher.TriggerEvent<int, CopyResult>(CopyEvents.Result, MissionId, Result);
        }
    }
}
