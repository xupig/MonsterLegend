﻿using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;

namespace Common.Data
{
    public class CopySingleLogic : CopyWinLoseLogic
    {
        public CopySingleLogic()
        {
            chapterType = ChapterType.Single;
        }

        protected override void FillData(object pb)
        {
            PbDreamlandBattleCallBack pbDreamlandBattleCallBack = pb as PbDreamlandBattleCallBack;
            Result = pbDreamlandBattleCallBack.is_win == 1 ? CopyResult.Win : CopyResult.Lose;
            SetBaseItemDataList(pbDreamlandBattleCallBack.item);
        }

        private void SetBaseItemDataList(List<PBDreamlandItem> list)
        {
            BaseItemDataList.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                BaseItemData baseItemData = ItemDataCreator.Create((int)list[i].item_id);
                baseItemData.StackCount = (int)list[i].item_sum;
                BaseItemDataList.Add(baseItemData);
            }
        }

        public override bool CanEnterByTeam(int instanceId)
        {
            return false;
        }

        public override string GetCopyName(int instanceId)
        {
            return MogoLanguageUtil.GetContent(73008); //幻境探险;
        }
    }
}
