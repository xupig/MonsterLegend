﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyDemonGateLogic : CopyBaseLogic
    {
        public int MapId;
        public int MissionId;
        public CopyResult Result;
        public bool IsNewRecord;
        public int Score;
        public List<PbItem> DropReward;
        //星级奖励是有星才发，没星不发
        public List<PbActionInfo> ConditionResultList;

        public CopyDemonGateLogic()
        {
            chapterType = ChapterType.DemonGate;
        }

        protected override void FillData(object pb)
        {
            PbEvilMissionResult missionResult = pb as PbEvilMissionResult;
            Result = (CopyResult)missionResult.is_win;
            MapId = (int)missionResult.map_id;
            MissionId = map_helper.GetInstanceIDByMapID(MapId);
            Score = (int)missionResult.score;
            DropReward = missionResult.reward;
            ConditionResultList = missionResult.action_info;
        }

        protected override void FillInstanceData(object pb, CopyInstanceData instanceData)
        {
            PbEvilMissionResult missionResult = pb as PbEvilMissionResult;
            if (missionResult.score > instanceData.HistoryMaxScore)
            {
                IsNewRecord = true;
            }
            else
            {
                IsNewRecord = false;
            }
            instanceData.score = missionResult.score;
        }

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.DemonGateResult, this);
        }

        public override void HandlePlayAgainResult(uint errorId, byte[] byteArr)
        {
            if ((error_code)errorId == error_code.ERR_SUCCESSFUL)
            {
                EventDispatcher.TriggerEvent(DemonGateEvents.SCORE_MODE_AGAIN);
            }
            else
            {
                //ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(10120));   
            }
        }

        protected override void TriggerCopyResultEvent()
        {
            EventDispatcher.TriggerEvent<int, CopyResult>(CopyEvents.Result, MissionId, Result);
        }

        public override bool CanEnterByTeam(int instanceId)
        {
            return false;
        }

        public override string GetCopyName(int instanceId)
        {
            return MogoLanguageUtil.GetContent(84013);
        }
    }
}
