﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/11 17:58:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{
    public enum CopyResult
    {
        NoResult = -2,
        Lose = -1,
        Draw = 0,
        Win = 1,
    }

    public enum RewardLevel
    {
        B = 1,
        A = 2,
        S = 3,
        Team = 4,
    }

    public class CopyInstanceData
    {
        public int id;
        public int HistoryMaxStar;
        public int DailyTimes;
        private int _nowStar = 0;
        private uint _score = 0;
        public uint HistoryMaxScore = 0;
        public CopyResult result;
        public int NowSweepTimes;
        public List<PbMopUpRewards> sweepReward;
        public bool IsCopyOpen = false;//剧情副本是否开启

        public int nowStar
        { 
            get
            {
                return _nowStar;
            }
            set
            {
                _nowStar = value;
                if (_nowStar > HistoryMaxStar)
                {
                    HistoryMaxStar = _nowStar;
                }
            } 
        }

        public uint score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
                if (_score > HistoryMaxScore)
                {
                    HistoryMaxScore = _score;
                }
            }
        }

        public CopyInstanceData(int id)
        {
            this.id = id;
        }

        public void FillInstanceDataByGetMissionRecord(PbMissionRecordInfo pbMissionInfo)
        {
            int mapId = instance_helper.GetMapId(id);
            MapStandardWay standardWay = map_helper.GetStandardWay(mapId);
            switch (standardWay)
            {
                case MapStandardWay.Star:
                    HistoryMaxStar = (int)pbMissionInfo.grade;
                    break;
                case MapStandardWay.Score:
                    HistoryMaxScore = pbMissionInfo.grade;
                    break;
                case MapStandardWay.WinFail:
                    result = (CopyResult)pbMissionInfo.is_win;
                    break;
            }
            DailyTimes = (int) pbMissionInfo.daily_count;
            IsCopyOpen = true;
        }

        public int GetLeftDailyEnterTimes()
        {
            return instance_helper.GetDailyTimes(id) - DailyTimes;
        }

        private List<PbTeamMissionPlayerInfo> GetTeammateInfoList(PbTeamMissionResultNew pbMissionInfo)
        {
            List<PbTeamMissionPlayerInfo> result = new List<PbTeamMissionPlayerInfo>();
            for (int i = 0; i < pbMissionInfo.players_info.Count; i++)
            {
                if (pbMissionInfo.players_info[i].avatar_dbid != PlayerAvatar.Player.dbid)
                {
                    result.Add(pbMissionInfo.players_info[i]);
                }
            }
            return result;
        }

        public void FillInstanceByMopUpMission(PbMopUpInfo pbMopUpInfo)
        {
            DailyTimes = (int) pbMopUpInfo.daily_count;
            NowSweepTimes = (int) pbMopUpInfo.mop_up_num;
            sweepReward = pbMopUpInfo.mop_up_reward;
        }

        public List<BaseItemData> GetSweepBaseItemDataList()
        {
            List<BaseItemData> result = new List<BaseItemData>();
            for (int i = 0; i < sweepReward.Count; i++)
            {
                PbMopUpRewards pbMopUpRewards = sweepReward[i];
                result.AddRange(PbItemHelper.ToBaseItemDataList(pbMopUpRewards.reward));
            }
            return result;
        }
    }
}
