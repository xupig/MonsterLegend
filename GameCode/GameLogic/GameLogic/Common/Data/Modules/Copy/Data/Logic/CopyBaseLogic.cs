﻿using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public abstract class CopyBaseLogic
    {
        protected ChapterType chapterType;
        public bool Valid = false;

        public void ProcessResult(object pb, CopyInstanceData instanceData = null)
        {
            Valid = true;
            FillData(pb);
            FillInstanceData(pb, instanceData);
            ShowPanel();
            GameSceneManager.GetInstance().OnRecieveCopyResult();
            TriggerCopyResultEvent();
        }

        protected virtual void TriggerCopyResultEvent() {}

        public virtual bool CanPlayAgain()
        {
            return inst_type_operate_helper.CanPlayAgain(chapterType);
        }

        public virtual void HandlePlayAgainResult(UInt32 errorId, byte[] byteArr) {}

        protected virtual void FillInstanceData(object pb, CopyInstanceData instanceData) {}

        protected abstract void FillData(object pb);

        protected abstract void ShowPanel();


        public virtual bool CanEnterByTeam(int instanceId)
        {
            if (instanceId != instance_helper.InvalidInstanceId)
            {
                InstanceTeamType teamType = instance_helper.GetTeamType(instanceId);
                if (teamType == InstanceTeamType.SinglePlayer)
                {
                    return false;
                }
            }
            return true;
        }

        public virtual string GetCopyName(int instanceId)
        {
            return instance_helper.GetInstanceName(instanceId);
        }

        public virtual bool CanPetFight(int mapId)
        {
            return inst_type_operate_helper.CanPetFight(chapterType);
        }
    }
}
