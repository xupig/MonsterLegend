﻿using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyNewPlayerLogic : CopyCopyLogic
    {
        public CopyNewPlayerLogic()
        {
            chapterType = ChapterType.NewPlayer;
        }
    }
}
