﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyEveningActivityLogic : CopyMultiPlayerLogic
    {
        //保存各个玩家的结算信息
        private PbPVPMissionInfo _originalInfo;
        private Dictionary<uint, EveningActivityResultPersonData> _eveningActivityResultPersonDataDic;
        private Dictionary<int, EveningActivityResultCampData> _eveningActivityResultCampDataDic;

        public int SelfCampId;
        public int EnemyCampId;

        public CopyEveningActivityLogic()
        {
            chapterType = ChapterType.EveningActivity;
        }

        protected override void FillData(object pb)
        {
            _originalInfo = pb as PbPVPMissionInfo;

            ParseCampId();
            FillPersonResultData();
            FillCampResultData();
        }

        protected override void FillInstanceData(object pb, CopyInstanceData instanceData)
        {
            PbPVPMissionInfo missionInfo = pb as PbPVPMissionInfo;
            instanceData.DailyTimes = (int)missionInfo.daily_count;
        }

        public override bool CanEnterByTeam(int instanceId)
        {
            if (instanceId != instance_helper.InvalidInstanceId)
            {
                InstanceTeamType teamType = instance_helper.GetTeamType(instanceId);
                instance ins = instance_helper.GetInstanceCfg(instanceId);
                inst_match_rule matchRule = inst_match_rule_helper.GetMatchRule(ins.__auto_team_match);
                if (matchRule.__group_player_num <= 1 || teamType == InstanceTeamType.SinglePlayer)
                {
                    return false;
                }
            }
            return true;
        }

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.PVEResult, this);
        }

        public override List<MultiCopyResultData> GetResultPersonDataList()
        {
            List<EveningActivityResultPersonData> eveningActivityResultList = EveningActivityResultPersonDataDic.Values.ToList();
            List<MultiCopyResultData> result = new List<MultiCopyResultData>();
            for (int i = 0; i < eveningActivityResultList.Count; i++)
            {
                result.Add(eveningActivityResultList[i]);
            }
            result.Sort(SortFunc);
            return result;
        }

        public override string GetLeftScoreName()
        {
            return MogoLanguageUtil.GetContent(97046);
        }

        public override string GetRightScoreName()
        {
            return MogoLanguageUtil.GetContent(97047);
        }

        public override int GetLeftScoreValue()
        {
            return GetResultCampInfoByCampId(SelfCampId).CampScore;
        }

        public override int GetRightScoreValue()
        {
            return GetResultCampInfoByCampId(EnemyCampId).CampScore;
        }

        public override bool IsShowCombatIntegral()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            return map_helper.GetMap(mapId).__group == 2;
        }

        public override bool IsWin()
        {
            return EveningActivityResultPersonDataDic[PlayerAvatar.Player.id].IsWin == 1;
        }

        public override MultiCopyResultData GetCopyResultPersonData(uint avatarId)
        {
            return _eveningActivityResultPersonDataDic[avatarId];
        }

        private void ParseCampId()
        {
            SelfCampId = -1;
            EnemyCampId = -1;
            List<PbPlayerInfo> playerInfoList = _originalInfo.players_info;

            for (int i = 0; i < playerInfoList.Count; i++)
            {
                if (playerInfoList[i].avatar_id == PlayerAvatar.Player.id)
                {
                    SelfCampId = (int)playerInfoList[i].group_id;
                    break;
                }
            }

            for (int i = 0; i < _originalInfo.group_score.Count; i++)
            {
                if (SelfCampId != _originalInfo.group_score[i].group_id)
                {
                    EnemyCampId = (int)_originalInfo.group_score[i].group_id;
                }
            }
        }

        private void FillPersonResultData()
        {
            if (_eveningActivityResultPersonDataDic == null)
            {
                _eveningActivityResultPersonDataDic = new Dictionary<uint, EveningActivityResultPersonData>();
            }
            else
            {
                _eveningActivityResultPersonDataDic.Clear();
            }
            for (int i = 0; i < _originalInfo.action_info_list.Count; i++)
            {
                EveningActivityResultPersonData personResultData = new EveningActivityResultPersonData((uint)_originalInfo.action_info_list[i].avatar_id, _originalInfo, this);
                _eveningActivityResultPersonDataDic.Add((uint)_originalInfo.action_info_list[i].avatar_id, personResultData);
            }
        }

        private void FillCampResultData()
        {
            if (_eveningActivityResultCampDataDic == null)
            {
                _eveningActivityResultCampDataDic = new Dictionary<int, EveningActivityResultCampData>();
            }
            else
            {
                _eveningActivityResultCampDataDic.Clear();
            }
            for (int j = 0; j < _originalInfo.group_score.Count; j++)
            {
                PbGroupScoreInfo campScoreInfo = _originalInfo.group_score[j];

                EveningActivityResultCampData resultCampData = new EveningActivityResultCampData(campScoreInfo);
                _eveningActivityResultCampDataDic.Add((int)campScoreInfo.group_id, resultCampData);
            }
        }

        public EveningActivityResultCampData GetResultCampInfoByCampId(int campId)
        {
            if(_eveningActivityResultCampDataDic.ContainsKey(campId) == true)
            {
                return _eveningActivityResultCampDataDic[campId];
            }
            return null;
        }

        public Dictionary<uint, EveningActivityResultPersonData> EveningActivityResultPersonDataDic
        {
            get
            {
                return _eveningActivityResultPersonDataDic;
            }
        }

        public List<EveningActivityResultPersonData> OurGoupPersonResultDataList
        {
            get
            {
                List<EveningActivityResultPersonData> personResultDataList = new List<EveningActivityResultPersonData>();
                foreach (KeyValuePair<uint, EveningActivityResultPersonData> kvp in _eveningActivityResultPersonDataDic)
                {
                    EveningActivityResultPersonData personData = kvp.Value;
                    if (personData.CampId == SelfCampId)
                    {
                        personResultDataList.Add(personData);
                    }
                }
                return personResultDataList;
            }
        }

        public List<EveningActivityResultPersonData> EnemyPersonResultDataList
        {
            get
            {
                List<EveningActivityResultPersonData> personResultDataList = new List<EveningActivityResultPersonData>();
                foreach (KeyValuePair<uint, EveningActivityResultPersonData> kvp in _eveningActivityResultPersonDataDic)
                {
                    EveningActivityResultPersonData personData = kvp.Value;
                    if (personData.CampId == EnemyCampId)
                    {
                        personResultDataList.Add(personData);
                    }
                }
                return personResultDataList;
            }
        }

        public override Dictionary<int, int> GetActionId2ConditionIdDic()
        {
            Dictionary<int, int> actionId2ConditionId = new Dictionary<int, int>();
            Dictionary<int, string> conditionNameDic = GetConditionchinese();
            List<int> actionIdList = conditionNameDic.Keys.ToList();
            for (int i = 0; i < actionIdList.Count; i++)
            {
                actionId2ConditionId[actionIdList[i]] = i;
            }
            return actionId2ConditionId;
        }

        public override Dictionary<int, string> GetConditionchinese()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            int mapId = GameSceneManager.GetInstance().curMapID;

            map map = map_helper.GetMap(mapId);
            if (map.__group >= 2)
            {
                string scoreGroupRule = map.__score_rule_group;
                List<Dictionary<int, string>> scoreRuleChineseList = ParseScoreRuleChinese(scoreGroupRule);
                return scoreRuleChineseList[0];
            }
            else
            {
                Dictionary<string, string[]> scoreRuleDic = map_helper.GetScoreRule(mapId);
                return ParseScoreRule(scoreRuleDic);
            }
        }

        private List<Dictionary<int, string>> ParseScoreRuleChinese(string str)
        {
            string[] itemList = str.Split('@');
            List<Dictionary<int, string>> result = new List<Dictionary<int, string>>();
            for (int i = 0; i < itemList.Length; i++)
            {
                Dictionary<int, string> condionNameDic = new Dictionary<int, string>();
                string[] conList = itemList[i].Split(';');
                for (int j = 0; j < conList.Length; j++)
                {
                    string[] keyValue = conList[j].Split(':');
                    string[] valueList = keyValue[1].Split(',');
                    condionNameDic.Add(int.Parse(keyValue[0]), MogoLanguageUtil.GetContent(int.Parse(valueList[3])));
                }
                result.Add(condionNameDic);
            }
            return result;
        }

        private Dictionary<int, string> ParseScoreRule(Dictionary<string, string[]> scoreRuleDic)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            Dictionary<int, List<int>> speechList = data_parse_helper.ParseDictionaryIntListInt(scoreRuleDic);
            var en = speechList.GetEnumerator();
            while (en.MoveNext())
            {
                result.Add(en.Current.Key, MogoLanguageUtil.GetContent(en.Current.Value[3]));
            }
            return result;
        }

        public override int GetMaxNotSpecialRewardCount()
        {
            int maxCount = 0;
            foreach (KeyValuePair<uint, EveningActivityResultPersonData> pair in _eveningActivityResultPersonDataDic)
            {
                int count = pair.Value.GetTotalNormalRewardList().Count;
                if (maxCount < count)
                {
                    maxCount = count;
                }
            }
            return maxCount;
        }

        public override string GetTeamScore()
        {
            map cfg = map_helper.GetMap((int)_originalInfo.map_id);
            if (cfg.__group > 2)
            {
                EveningActivityResultPersonData maxScorePersonData = null;
                foreach (KeyValuePair<uint, EveningActivityResultPersonData> kvp in _eveningActivityResultPersonDataDic)
                {
                    if (maxScorePersonData == null)
                    {
                        maxScorePersonData = kvp.Value;
                    }
                    else
                    {
                        if (maxScorePersonData.Score < kvp.Value.Score)
                        {
                            maxScorePersonData = kvp.Value;
                        }
                    }
                }
                if (maxScorePersonData != null)
                {
                    return maxScorePersonData.AvatarName.ToString();
                }
                return string.Empty;
            }
            else
            {
                EveningActivityResultCampData selfResultCampData = GetResultCampInfoByCampId(SelfCampId);
                return selfResultCampData.CampScore.ToString();
            }
        }

        public override string GetTeamLabel()
        {
            map cfg = map_helper.GetMap((int)_originalInfo.map_id);
            if (cfg.__group > 2)
            {
                return MogoLanguageUtil.GetContent(102436);
            }
            else
            {
                return MogoLanguageUtil.GetContent(97048);
            }
        }
    }
}
