﻿using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PlayingCopyStarData
    {
        private Dictionary<string, string[]> _dicScoreRule;
        private Dictionary<int, int> _actionIdToStateDic;

        public void ResetCopyStar()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            _dicScoreRule = map_helper.GetScoreRule(mapId);
            _actionIdToStateDic = new Dictionary<int, int>();
            foreach (KeyValuePair<string, string[]> pair in _dicScoreRule)
            {

                _actionIdToStateDic[int.Parse(pair.Key)] = score_rule_helper.GetScoreRuleInitState(int.Parse(pair.Key));
            }
        }

        public void UpdateCopyStar(int actionId, int state)
        {
            if (_actionIdToStateDic.ContainsKey(actionId) == true)
            {
                _actionIdToStateDic[actionId] = state;
            }
        }

        public int GetCopyStarCount()
        {
            int starCount = 0;
            foreach (KeyValuePair<int, int> actionIdStatePair in _actionIdToStateDic)
            {
                if (actionIdStatePair.Value == 1)
                {
                    starCount++;
                }
            }
            return starCount;
        }

        public Dictionary<int, int> ActionIdToStateDic
        {
            get
            {
                return _actionIdToStateDic;
            }
        }
    }
}
