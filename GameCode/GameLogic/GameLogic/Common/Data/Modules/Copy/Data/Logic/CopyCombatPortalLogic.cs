﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class CopyCombatPortalLogic : CopyMultiPlayerLogic
    {
        private PbTransPortalList _originalTransPortalResultData;
        private Dictionary<uint, TransPortalResultPersonData> _transPortResultPersonDataDic;

        public CopyCombatPortalLogic()
        {
            chapterType = ChapterType.CombatPortal;
        }

        protected override void FillData(object pb)
        {
            _originalTransPortalResultData = pb as PbTransPortalList;
            FillPersonResultData();
        }

        protected override void ShowPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.PVEResult, this);
        }

        public override List<MultiCopyResultData> GetResultPersonDataList()
        {
            List<MultiCopyResultData> result = new List<MultiCopyResultData>();
            List<TransPortalResultPersonData> resultPersonDataList = _transPortResultPersonDataDic.Values.ToList();
            for (int i = 0; i < resultPersonDataList.Count; i++)
            {
                result.Add(resultPersonDataList[i]);
            }
            result.Sort(SortFunc);
            return result;   
        }

        public override int GetLeftScoreValue()
        {
            return TransPortalResultPersonDataDic[PlayerAvatar.Player.id].Score;
        }

        public override int GetRightScoreValue()
        {
            return (GetResultPersonDataList()[0] as TransPortalResultPersonData).Score;
        }

        public override string GetLeftScoreName()
        {
            return MogoLanguageUtil.GetContent(102440);
        }

        public override string GetRightScoreName()
        {
            return MogoLanguageUtil.GetContent(102441);
        }

        public override bool IsShowCombatIntegral()
        {
            return true;
        }

        public override bool IsWin()
        {
            return TransPortalResultPersonDataDic[PlayerAvatar.Player.id].IsWin == 1;
        }

        public override MultiCopyResultData GetCopyResultPersonData(uint avatarId)
        {
            return TransPortalResultPersonDataDic[avatarId];
        }

        public override bool CanEnterByTeam(int instanceId)
        {
            return false;
        }

        private void FillPersonResultData()
        {
            if (_transPortResultPersonDataDic == null)
            {
                _transPortResultPersonDataDic = new Dictionary<uint, TransPortalResultPersonData>();
            }
            else
            {
                _transPortResultPersonDataDic.Clear();
            }
            for (int i = 0; i < _originalTransPortalResultData.trans_portal_datas.Count; i++)
            {
                _transPortResultPersonDataDic.Add( _originalTransPortalResultData.trans_portal_datas[i].entity_id, new TransPortalResultPersonData(_originalTransPortalResultData.trans_portal_datas[i], this));
            }
        }

        public Dictionary<uint, TransPortalResultPersonData> TransPortalResultPersonDataDic
        {
            get
            {
                return _transPortResultPersonDataDic;
            }
        }

        public override Dictionary<int, string> GetConditionchinese()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            result.Add(10, MogoLanguageUtil.GetContent(72557));

            return result;
        }

        public override Dictionary<int, int> GetActionId2ConditionIdDic()
        {
            Dictionary<int, int> actionId2ConditionId = new Dictionary<int, int>();
            actionId2ConditionId.Add(10, 0);
            return actionId2ConditionId;
        }

        /// <summary>
        /// 获得非经验，金币奖励的最大数量
        /// </summary>
        /// <returns></returns>
        public override int GetMaxNotSpecialRewardCount()
        {
            int maxCount = 0;
            foreach (KeyValuePair<uint, TransPortalResultPersonData> pair in _transPortResultPersonDataDic)
            {
                int count = pair.Value.GetTotalNormalRewardList().Count;
                if (maxCount < count)
                {
                    maxCount = count;
                }
            }
            return maxCount; 
        }

        public override string GetTeamLabel()
        {
            return MogoLanguageUtil.GetContent(102436);
        }

        public override string GetTeamScore()
        {
            TransPortalResultPersonData maxScorePersonData = null;
            foreach (KeyValuePair<uint, TransPortalResultPersonData> kvp in _transPortResultPersonDataDic)
            {
                if (maxScorePersonData == null)
                {
                    maxScorePersonData = kvp.Value;
                }
                else
                {
                    if (maxScorePersonData.Score < kvp.Value.Score)
                    {
                        maxScorePersonData = kvp.Value;
                    }
                }
            }
            if (maxScorePersonData != null)
            {
                return maxScorePersonData.AvatarName.ToString();
            }
            return string.Empty;
        }


    }
}
