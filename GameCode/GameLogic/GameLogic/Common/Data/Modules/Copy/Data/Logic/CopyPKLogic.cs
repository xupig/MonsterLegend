﻿using Common.Base;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyPKLogic : CopyWinLoseLogic
    {
        public int InstanceId;
        public CopyPKLogic()
        {
            chapterType = ChapterType.PK;
        }

        protected override void FillData(object pb)
        {
            PbPkMissionResult pbPkMissionResult = pb as PbPkMissionResult;
            InstanceId = (int)pbPkMissionResult.map_id;
            Result = pbPkMissionResult.is_win == 1 ? CopyResult.Win : CopyResult.Lose;
            BaseItemDataList.Clear();
        }
    }
}
