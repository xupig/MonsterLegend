﻿using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyGuildWarLogic : CopyGuildWarBaseLogic
    {
        public CopyGuildWarLogic()
        {
            chapterType = ChapterType.GuildWarGroup;
        }

        public override bool ShowRound()
        {
            return true;
        }

        public override string GetScoreDesc()
        {
            return MogoLanguageUtil.GetContent(111531);
        }

        public override bool ShowFinish()
        {
            return false;
        }

        public override CopyResult GetModelAction()
        {
            return Result;
        }

        public override bool NeedShowPreviewTitle()
        {
            return true;
        }

        public override void FillPreviewData(object pb)
        {
            PlayerAvatar player = PlayerAvatar.Player;
            LeftWrapper = new CopyPreviewItemDataWrapper(player.name, (int)player.vocation, PlayerDataManager.Instance.GuildData.MyGuildData.GuildName, player.level, player.fight_force, MogoLanguageUtil.GetContent(111516 + (int)PlayerDataManager.Instance.GuildData.GetMyMemberInfo().military));
            GuildWarEnemyData data = pb as GuildWarEnemyData;
            RightWrapper = new CopyPreviewItemDataWrapper(data.name, data.vocation, data.guildName, (uint)data.level, (uint)data.fight, MogoLanguageUtil.GetContent(111516 + data.military));
        }
    }
}
