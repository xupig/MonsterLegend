﻿using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyChapterTypeData
    {
        public ChapterType ChapterType;
        public Dictionary<int, int> ResetTimesDict = new Dictionary<int, int>();

        public CopyChapterTypeData(ChapterType chapterType)
        {
            this.ChapterType = chapterType;
        }

        public override string ToString()
        {
            return string.Format("{0}", ChapterType);
        }

        public int GetResetTimes(int missionId)
        {
            if (ResetTimesDict.ContainsKey(missionId))
            {
                return ResetTimesDict[missionId];
            }
            return 0;
        }

        public void Fill(PbMissionResetTimesInfo pbMissionResetTimesInfo)
        {
            ResetTimesDict.Clear();
            for (int i = 0; i < pbMissionResetTimesInfo.mission_reset_times.Count; i++)
            {
                PbMissionResetTimes resetTimes = pbMissionResetTimesInfo.mission_reset_times[i];
                ResetTimesDict.Add((int)resetTimes.mission_id, (int)resetTimes.count);
            }
        }
    }
}
