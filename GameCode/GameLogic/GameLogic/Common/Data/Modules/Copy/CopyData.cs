﻿using System;
using System.Collections.Generic;
using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using Common.Structs;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameLoader.Utils;
using Common.ServerConfig;

namespace Common.Data
{
    public enum BoxState
    {
        NULL,
        CAN_GET,
        CANNOT_GET,
        HAD_GOT,
    }

    public class CopyData
    {
        public const int FIRST_CHAPTER_ID_IN_COPY = 10;
        public const int FIRST_INSTANCE_ID_IN_COPY = 20101;
        public const int MAX_STAR_NUM_PER_INSTANCE = 3;

        private Dictionary<int, CopyChapterData> _chapterDict = new Dictionary<int, CopyChapterData>();
        private Dictionary<int, CopyInstanceData> _instanceDict = new Dictionary<int, CopyInstanceData>();
        public Dictionary<int, CopyInstanceData> InstanceDict
        {
            get
            {
                return _instanceDict;
            }
        }
        private Dictionary<ChapterType, CopyChapterTypeData> _chapterTypeDict = new Dictionary<ChapterType, CopyChapterTypeData>();

        private uint _matchingId;
        private ulong _matchTime;

        public CopyChapterData GetChapterData(int id)
        {
            if (_chapterDict.ContainsKey(id))
            {
                return _chapterDict[id];
            }
            CopyChapterData chapterData = new CopyChapterData(id);
            _chapterDict.Add(id, chapterData);
            return chapterData;
        }

        public bool IsChapterExist(int id)
        {
            return _chapterDict.ContainsKey(id);
        }

        public void FillChapterDict(PbChapterRecord chapterInfo)
        {
            for (int i = 0; i < chapterInfo.chapter_info.Count; i++)
            {
                PbChapterInfo pbChapterInfo = chapterInfo.chapter_info[i];
                CopyChapterData chapterData = GetChapterData((int)pbChapterInfo.chapter_id);
                chapterData.FillChapterDataByGetChapterRecord(pbChapterInfo);
            }
        }

        public void FillChapterStarRewardList(PbChapterInfo pbChapterInfo)
        {
            CopyChapterData chapterData = GetChapterData((int)pbChapterInfo.chapter_id);
            List<CopyProgressBoxDataWrapper> copyBoxDataList = chapterData.FillChapterDataByGetStarReward(pbChapterInfo.star_spirit_reward);
            EventDispatcher.TriggerEvent(CopyEvents.GotStarRewardListChange);
            for (int i = 0; i < copyBoxDataList.Count; i++)
            {
                EventDispatcher.TriggerEvent<int, int>(CopyEvents.GET_BOX_REWARD, copyBoxDataList[i].ChapterId, copyBoxDataList[i].StarNum);
            }
        }

        public BoxState GetBoxState(int chapterId, int starNum)
        {
            if (IsChapterExist(chapterId))
            {
                CopyChapterData chapterData = GetChapterData(chapterId);
                return chapterData.GetBoxState(starNum);
            }
            return BoxState.CANNOT_GET;
        }

        public bool BoxStateIsCanGet(int chapterId, int starNum)
        {
            return GetBoxState(chapterId, starNum) == BoxState.CAN_GET;
        }

        public bool ChapterHaveCanGetBox(int chapterId)
        {
            Dictionary<string, string> progressReward = chapters_helper.GetProgressReward(chapterId);
            foreach (string starNum in progressReward.Keys)
            {
                if (BoxStateIsCanGet(chapterId, int.Parse(starNum)))
                {
                    return true;
                }
            }
            return false;
        }

        public int GetTotalStarNum(int chapterId)
        {
            if (IsChapterExist(chapterId))
            {
                CopyChapterData chapterData = GetChapterData(chapterId);
                return chapterData.TotalStarNum;
            }
            return 0;
        }

        public CopyInstanceData GetInstanceData(int id)
        {
            if (IsInstanceExist(id))
            {
                return _instanceDict[id];
            }
            CopyInstanceData instanceData = new CopyInstanceData(id);
            _instanceDict.Add(id, instanceData);
            return instanceData;
        }

        public bool IsInstanceExist(int id)
        {
            return _instanceDict.ContainsKey(id);
        }

        public void FillInstanceList(PbMissionRecord missionRecord)
        {
            for (int i = 0; i < missionRecord.mission_record_info.Count; i++)
            {
                PbMissionRecordInfo pbMissionInfo = missionRecord.mission_record_info[i];
                int missionId = (int)pbMissionInfo.mission_id;
                if (instance_helper.IsConfigExist(missionId))
                {
                    CopyInstanceData instanceData = GetInstanceData(missionId);
                    instanceData.FillInstanceDataByGetMissionRecord(pbMissionInfo);
                }
            }
        }

        public int GetHistoryMaxStar(int id)
        {
            if (IsInstanceExist(id))
            {
                return GetInstanceData(id).HistoryMaxStar;
            }
            return 0;
        }

        public bool IsInstancePass(int id)
        {
            return GetHistoryMaxStar(id) > 0;
        }

        public bool IsInstanceThreeStarPass(int id)
        {
            return GetHistoryMaxStar(id) == 3;
        }

        public bool IsChapterPass(int chapterId)
        {
            List<int> instanceIdList = chapters_helper.GetInstanceIdList(chapterId);
            for (int i = 0; i < instanceIdList.Count; i++)
            {
                if (!IsInstancePass(instanceIdList[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public int GetDailyTimes(int id)
        {
            if (IsInstanceExist(id))
            {
                return GetInstanceData(id).DailyTimes;
            }
            return 0;
        }

        public int GetDailyTimesByChapterType(ChapterType type)
        {
            int result = 0;
            foreach (CopyInstanceData data in _instanceDict.Values)
            {
                if (type == instance_helper.GetChapterType(data.id))
                {
                    result += data.DailyTimes;
                }
            }
            return result;
        }

        public int GetDailyTimesByChapterId(int chapterId)
        {
            int result = 0;
            foreach (CopyInstanceData data in _instanceDict.Values)
            {
                if (chapterId == chapters_helper.GetChapterId(data.id))
                {
                    result += data.DailyTimes;
                }
            }
            return result;
        }

        public int GetDailyLeftTimes(int id)
        {
            int totalTimes = instance_helper.GetInstanceCfg(id).__daily_times;
            return totalTimes - GetDailyTimes(id);
        }

        public void ClearDailyTimes()
        {
            foreach (KeyValuePair<int, CopyInstanceData> kvp in _instanceDict)
            {
                kvp.Value.DailyTimes = 0;
            }
        }

        public ulong GetMatchTime(int id)
        {
            if (_matchingId == id)
            {
                return _matchTime;
            }
            return 0;
        }

        public void FillInstance(int mapId, object pbMissionInfo)
        {
            int instanceId = map_helper.GetInstanceIDByMapID(mapId);
            //LoggerHelper.Info(string.Format("CopyData FillInstance, instanceId = {0}.", instanceId));
            CopyInstanceData instanceData = GetInstanceData(instanceId);
            CopyLogicManager.Instance.ProcessResult(instance_helper.GetChapterType(instanceId), pbMissionInfo, instanceData);
        }

        public void FillWanderlandInstance(uint instanceId, PbTryPassState pbTryPassState)
        {
            CopyInstanceData instanceData = GetInstanceData((int)instanceId);
            instanceData.DailyTimes += 1;
        }

        public void SweepFillInstance(PbMopUpInfo pbMopUpInfo)
        {
            CopyInstanceData instanceData = GetInstanceData((int)pbMopUpInfo.mission_id);
            instanceData.FillInstanceByMopUpMission(pbMopUpInfo);
            List<List<BaseItemData>> list = GetBaseItemDataList(instanceData.sweepReward);
            //ari CopySweepDataWrapper wrapper = new CopySweepDataWrapper(instanceData.NowSweepTimes, list, instanceData.HistoryMaxStar, instanceData.id);
            //ari UIManager.Instance.ShowPanel(PanelIdEnum.CopySweep, wrapper);
        }

        private List<List<BaseItemData>> GetBaseItemDataList(List<PbMopUpRewards> list)
        {
            List<List<BaseItemData>> resultList = new List<List<BaseItemData>>();
            for (int i = 0; i < list.Count; i++)
            {
                PbMopUpRewards pbMopUpRewards = list[i];
                resultList.Add(PbItemHelper.ToBaseItemDataList(pbMopUpRewards.reward));
            }
            return resultList;
        }

        //获取剧情副本章节列表
        public List<CopyChapterData> GetCopyChapterDataList()
        {
            List<CopyChapterData> chapterDataList = new List<CopyChapterData>();
            foreach (int chapterId in _chapterDict.Keys)
            {
                //策划可能删除章节数据
                if (chapters_helper.IsChapterExist(chapterId)
                    && chapters_helper.GetChapterType(chapterId) == ChapterType.Copy
                    && _chapterDict[chapterId].IsCopyOpen)
                {
                    chapterDataList.Add(_chapterDict[chapterId]);
                }
            }
            chapterDataList.Sort(SortByPage);
            return chapterDataList;
        }

        private int SortByPage(CopyChapterData x, CopyChapterData y)
        {
            if (chapters_helper.GetPage(x.id) < chapters_helper.GetPage(y.id))
            {
                return -1;
            }
            return 1;
        }

        public void CheckChapterAndInstance()
        {
            int largestInstanceId = GetLargestInstanceId();
            if (largestInstanceId == instance_helper.InvalidInstanceId)
            {
                CreateFirstData();
                return;
            }
            
            if (IsInstancePass(largestInstanceId))
            {
                int nextInstanceId = chapters_helper.GetNextInstanceId(largestInstanceId);
                if (nextInstanceId == instance_helper.InvalidInstanceId) return;
                CreateSingleCopy(largestInstanceId, nextInstanceId);
                //int level = instance_helper.GetMinLevel(nextInstanceId);
                ////如果大于等于40级，一次创建5个副本 否则一次创建一个副本
                //if (level >= 40)
                //{
                //    CreateMultiCopy(level, largestInstanceId);
                //}
                //else
                //{
                //    CreateSingleCopy(largestInstanceId, nextInstanceId);
                //}
            }
            else
            {
                int nowChapterId = chapters_helper.GetChapterId(largestInstanceId);
                //有本章的instanceData
                //没有本章的chapterData
                //largestInstanceId是该章节的第一个instance
                GetChapterData(nowChapterId).IsCopyOpen = true;
            }
        }

        private void CreateFirstData()
        {
            CopyInstanceData instanceData = GetInstanceData(FIRST_INSTANCE_ID_IN_COPY);
            instanceData.IsCopyOpen = true;
            CopyChapterData chapterData = GetChapterData(FIRST_CHAPTER_ID_IN_COPY);
            chapterData.IsCopyOpen = true;
        }

        private void CreateSingleCopy(int currentInstanceId, int nextInstanceId)
        {
            GetInstanceData(nextInstanceId).IsCopyOpen = true;
            GetChapterData(chapters_helper.GetChapterId(currentInstanceId)).IsCopyOpen = true;
            GetChapterData(chapters_helper.GetChapterId(nextInstanceId)).IsCopyOpen = true;
        }

        private void CreateMultiCopy(int level, int largestInstanceId)
        {
            int index = level % 5;
            int loopCount = 5 - index;
            int nowInstanceId = largestInstanceId;
            for (int i = 0; i < loopCount; i++)
            {
                int nextInstanceId = chapters_helper.GetNextInstanceId(nowInstanceId);
                if (nextInstanceId == instance_helper.InvalidInstanceId) break;
                CreateSingleCopy(nowInstanceId, nextInstanceId);
                nowInstanceId = nextInstanceId;
            }
        }

        public int GetLargestInstanceId()
        {
            int largestInstanceId = instance_helper.InvalidInstanceId;
            foreach (int instanceId in _instanceDict.Keys)
            {
                if (chapters_helper.IsInstanceExistInChapter(instanceId) && 
                    (instance_helper.GetChapterType(instanceId) == ChapterType.Copy) &&
                    instance_helper.GetMode(instanceId) == instance_helper.Mode.normal &&//只判断普通模式
                    _instanceDict[instanceId].IsCopyOpen)
                {
                    if (instanceId > largestInstanceId)
                    {
                        largestInstanceId = instanceId;
                    }
                }
            }
            return largestInstanceId;
        }

        private uint _matchTimerId = 0;
        public void FillMatchingInstance(PbMatchingMap pbMatching)
        {
            _matchingId = pbMatching.mission_id;
            _matchTime = Global.Global.serverTimeStamp;
            ResetMatchTimer();
            uint waitTime = uint.Parse(global_params_helper.GetConfig(104).__value);
            _matchTimerId = TimerHeap.AddTimer(waitTime * 1000, 0, CancelMatch);
        }

        public void FillCancelMatchInstance(PbMatchingMap pbMatching)
        {
            _matchingId = 0;
            _matchTime = 0;
            ResetMatchTimer();
        }

        private void CancelMatch()
        {
            ResetMatchTimer();
            MissionManager.Instance.RequsetCancelMatchMission((int)_matchingId);
        }

        private void ResetMatchTimer()
        {
            if (_matchTimerId != 0)
            {
                TimerHeap.DelTimer(_matchTimerId);
                _matchTimerId = 0;
            }
        }

        public void FillResetTimesInfo(PbMissionResetTimesRecord pbMissionResetTimesRecord)
        {
            for(int i = 0;i < pbMissionResetTimesRecord.reset_times_info.Count;i++)
            {
                PbMissionResetTimesInfo pbMissionResetTimesInfo = pbMissionResetTimesRecord.reset_times_info[i];
                ChapterType chapterType = (ChapterType)pbMissionResetTimesInfo.mission_type;
                CopyChapterTypeData chapterTypeData;
                if (!_chapterTypeDict.ContainsKey(chapterType))
                {
                    chapterTypeData = new CopyChapterTypeData(chapterType);
                    _chapterTypeDict.Add(chapterType, chapterTypeData);
                }
                else
                {
                    chapterTypeData = _chapterTypeDict[chapterType];
                }
                chapterTypeData.Fill(pbMissionResetTimesInfo);
            }
        }

        public int GetResetTimes(ChapterType chapterType, int instanceId)
        {
            if (_chapterTypeDict.ContainsKey(chapterType))
            {
                return _chapterTypeDict[chapterType].GetResetTimes(instanceId);
            }
            Debug.LogError("[GetResetTimes] chapterType not exist:" + chapterType);
            return 0;
        }

        public void ResetCopyDailtyTimes(PbMissionId pbMissionId)
        {
            if (_instanceDict.ContainsKey((int)pbMissionId.mission_id))
            {
                _instanceDict[(int)pbMissionId.mission_id].DailyTimes = 0;
            }
        }

        public int GetLeftSweepNum(ChapterType chapterType)
        {
            int vipLeftSweepNum = PlayerAvatar.Player.mission_mop_up_num;
            int sweepItemId = inst_type_operate_helper.GetSweepItemId(chapterType);
            return PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(sweepItemId) + vipLeftSweepNum;
        }

        public bool CheckLeftSweepNum(ChapterType chapterType)
        {
            int leftSweepNum = GetLeftSweepNum(chapterType);
            if (leftSweepNum < 1)
            {
                string vipTipsStr = "";
                if ((PlayerAvatar.Player.vip_level != public_config.AVATAR_MAX_VIP_LEVEL)
                    &&(PlayerAvatar.Player.vip_level != 0))
                {
                    int nextVipLevel = PlayerAvatar.Player.vip_level + 1;
                    string vipSweepNumStr = MogoLanguageUtil.GetContent(49219, nextVipLevel, int.Parse(vip_helper.GetDailyMaxSweepNum(nextVipLevel)));
                    vipTipsStr = string.Format("{0}", vipSweepNumStr);
                }

          //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE,
          //          string.Format("{0}\n{1}", MogoLanguageUtil.GetContent(49221), vipTipsStr), true);
                return false;
            }
            return true;
        }
    }
}
