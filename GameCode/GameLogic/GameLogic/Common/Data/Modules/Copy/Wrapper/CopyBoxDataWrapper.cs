﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyBoxDataWrapper
    {
        public BaseItemData BaseItemData;
        public BoxState BoxState;

        public CopyBoxDataWrapper(BaseItemData baseItemData, BoxState boxState)
        {
            this.BaseItemData = baseItemData;
            this.BoxState = boxState;
        }
    }
}
