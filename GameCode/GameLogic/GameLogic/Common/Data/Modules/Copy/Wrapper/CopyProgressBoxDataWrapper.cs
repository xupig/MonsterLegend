﻿using System;

namespace Common.Data
{
    public class CopyProgressBoxDataWrapper
    {
        public int ChapterId;
        public int StarNum;
        public int UngetIcon;
        public int GetIcon;

        public CopyProgressBoxDataWrapper(int chapterId, int starNum)
        {
            this.ChapterId = chapterId;
            this.StarNum = starNum;
        }

        public CopyProgressBoxDataWrapper(int chapterId, int starNum, string ungetIcon, string getIcon)
            : this(chapterId, starNum)
        {
            this.UngetIcon = Int32.Parse(ungetIcon);
            this.GetIcon = Int32.Parse(getIcon);
        }
    }
}
