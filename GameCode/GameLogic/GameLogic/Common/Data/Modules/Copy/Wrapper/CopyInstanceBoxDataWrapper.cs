﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyInstanceBoxDataWrapper
    {
        public WanderLandChapter WanderLandChapter;
        public WanderLandInstance WanderLandInstance;

        public CopyInstanceBoxDataWrapper(WanderLandChapter wanderLandChapter, WanderLandInstance wanderLandInstance)
        {
            this.WanderLandChapter = wanderLandChapter;
            this.WanderLandInstance = wanderLandInstance;
        }
    }
}
