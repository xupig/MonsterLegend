using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Common.Data
{
    public class PKInviteData
    {
        public ulong inviteTime = 0;
        public bool hasReject = false;
        public PbFriendAvatarInfo data;

        public PKInviteData(ulong inviteTime, PbFriendAvatarInfo data)
        {
            this.inviteTime = inviteTime;
            this.data = data;
        }
    }

    public class PKData
    {
        private List<PKInviteData> _inviteDataList = new List<PKInviteData>();
        private uint _timerId;
        private ulong _waitTime;

        public PKData()
        {
            _waitTime = ulong.Parse(global_params_helper.GetGlobalParam(GlobalParamId.pk_wait_time)) * 1000;
        }

        public List<PKInviteData> InviteDataList
        {
            get
            {
                return _inviteDataList;
            }
        }

        public void AddPKInviteData(PKInviteData inviteData)
        {
            bool isNew = true;
            for (int i = _inviteDataList.Count - 1; i >= 0; i--)
            {
                if (_inviteDataList[i].data.dbid == inviteData.data.dbid)
                {
                    _inviteDataList[i] = inviteData;
                    isNew = false;
                }
            }
            if (isNew)
            {
                _inviteDataList.Add(inviteData);
                string content = MogoLanguageUtil.GetContent(105001);
                FastNoticeData data = new FastNoticeData(content, PanelIdEnum.PK);
                PlayerDataManager.Instance.NoticeData.AddNoticeData(data);
            }
            EventDispatcher.TriggerEvent(PKEvents.REFRESH_INVITE_CONTENT);
            ResetTimer();
            _timerId = TimerHeap.AddTimer(0, 1000, CheckInviteOutdate);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void CheckInviteOutdate()
        {
            bool hasRejectAll = true;
            for (int i = 0; i < _inviteDataList.Count; i++)
            {
                if (_inviteDataList[i].hasReject == false)
                {
                    if (_waitTime <= (Global.Global.serverTimeStamp - _inviteDataList[i].inviteTime))
                    {
                        PKManager.Instance.AccpetPK(_inviteDataList[i].data.dbid, 0);
                        _inviteDataList[i].hasReject = true;
                        return;
                    }
                    else
                    {
                        hasRejectAll = false;
                    }
                }
            }
            if (hasRejectAll)
            {
                ResetTimer();
            }
        }

        public void RemovePKInviteData(ulong dbid)
        {
            for (int i = _inviteDataList.Count - 1; i >= 0; i--)
            {
                if (_inviteDataList[i].data.dbid == dbid)
                {
                    _inviteDataList.RemoveAt(i);
                }
            }
            EventDispatcher.TriggerEvent(PKEvents.REFRESH_INVITE_CONTENT);
        }

    }
}
