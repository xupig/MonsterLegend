﻿#region 模块信息
/*==========================================
// 文件名：PlayerReturn
// 命名空间: GameLogic.GameLogic.Common.Data.Modules
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/21 10:18:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;

namespace Common.Data
{
    public class PlayerReturnData
    {
        private bool hasSet = false;

        public bool StatusChanged = false;

        public PlayerReturnData()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.old_role_status, ShowStatus);
        }

        private void ShowStatus()
        {
            if (hasSet == false)
            {
                hasSet = true;
            }
            if (hasSet == true)
            {
                StatusChanged = true;
            }
        }

        private Dictionary<int, PbOldReturnItem> _cachedOldReturnDataDict = new Dictionary<int, PbOldReturnItem>();

        public void UpdatePbOldReturn(PbOldReturnList list)
        {
            for (int i = 0; i < list.reward_list.Count; i++)
            {
                if (_cachedOldReturnDataDict.ContainsKey((int)list.reward_list[i].reward_day))
                {
                    _cachedOldReturnDataDict[(int)list.reward_list[i].reward_day] = list.reward_list[i];
                }
                else
                {
                    _cachedOldReturnDataDict.Add((int)list.reward_list[i].reward_day, list.reward_list[i]);
                }
            }
            EventDispatcher.TriggerEvent(OldPlayerReturnEvent.UpdateData);
            ProcessRewardTipsPoint();
        }

        public void ProcessRewardTipsPoint()
        {
            if (HasRewardToGet() == true)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.reward);
            }
            if (HasRewardToGet() == false
                && PlayerDataManager.Instance.AchievementData.HasCompletedAchievement() == false
                && PlayerDataManager.Instance.RewardData.IsShowRewardSystemPoint() == false)
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.reward);
            }
        }

        public List<PbOldReturnItem> GetOldReturnRewardList()
        {
            List<PbOldReturnItem> result = new List<PbOldReturnItem>();
            result.AddRange(_cachedOldReturnDataDict.Values);
            result.Sort(SortFunction);
            return result;
        }

        public bool HasRewardToGet()
        {
            foreach (var pair in _cachedOldReturnDataDict)
            {
                if (pair.Value.reward_flag == public_config.OLD_RETURN_REWAED_NO)
                {
                    return true;
                }
            }
            return false;
        }

        private int SortFunction(PbOldReturnItem x, PbOldReturnItem y)
        {
            return (int)x.reward_day - (int)y.reward_day;
        }
    }
}
