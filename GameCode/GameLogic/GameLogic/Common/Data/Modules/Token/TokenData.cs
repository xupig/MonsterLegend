﻿#region 模块信息
/*==========================================
// 文件名：TokenData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 14:43:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TokenData
    {
        private Dictionary<int, int> _tokenShopItemDailyCountDict;
        private Dictionary<int, int> _tokenShopItemTotalCountDict;

        public int GetTokenShopItemDailyCount(int tokenShopItemId)
        {
            int limit = token_shop_helper.GetItemDaliyLimit(tokenShopItemId);
            if(limit == 0)
            {
                return int.MaxValue;
            }
            if (_tokenShopItemDailyCountDict == null)
            {
                return 0;
            }
            if(_tokenShopItemDailyCountDict.ContainsKey(tokenShopItemId))
            {
                return limit - _tokenShopItemDailyCountDict[tokenShopItemId];
            }
            return limit;
        }

        public int GetTokenShopItemTotalCount(int tokenShopItemId)
        {
            int limit = token_shop_helper.GetItemTotalLimit(tokenShopItemId);
            if (limit == 0)
            {
                return int.MaxValue;
            }
            if (_tokenShopItemTotalCountDict == null)
            {
                return 0;
            }
            if (_tokenShopItemTotalCountDict.ContainsKey(tokenShopItemId))
            {
                return limit - _tokenShopItemTotalCountDict[tokenShopItemId];
            }
            return limit;
        }

        public void OnZeroClockResp()
        {
            if (_tokenShopItemDailyCountDict!=null)
            {
                _tokenShopItemDailyCountDict.Clear();
            }
            if (_tokenShopItemTotalCountDict != null)
            {
                _tokenShopItemTotalCountDict.Clear();
            }
        }


        public void FillDaliyCount(PbTokenShop data)
        {
            _tokenShopItemDailyCountDict = new Dictionary<int,int>();
            for (int i = 0; i < data.item.Count; i++)
            {
                _tokenShopItemDailyCountDict.Add((int)data.item[i].item_id, (int)data.item[i].daily_count);
            }
        }

        public void FillTotalCount(PbTokenTotalInfoList data)
        {
            _tokenShopItemTotalCountDict = new Dictionary<int, int>();
            for (int i = 0; i < data.tokenTotalInfoList.Count; i++)
            {
                _tokenShopItemTotalCountDict.Add((int)data.tokenTotalInfoList[i].grid_id, (int)data.tokenTotalInfoList[i].buy_cnt);
            }
        }

        public void UpdateTokenShopItem(PbTokenShopItem tokenShopItem)
        {
            if(_tokenShopItemDailyCountDict.ContainsKey((int)tokenShopItem.item_id))
            {
                _tokenShopItemDailyCountDict[(int)tokenShopItem.item_id] = (int)tokenShopItem.daily_count;
            }
            else
            {
                _tokenShopItemDailyCountDict.Add((int)tokenShopItem.item_id, (int)tokenShopItem.daily_count);
            }
            token_shop_item item = token_shop_helper.GetTokenShopItem((int)tokenShopItem.item_id);
            if (item.__good_type == 3 && (item.__daily_limit - tokenShopItem.daily_count <= 0))
            {
                EventDispatcher.TriggerEvent<int>(TokenEvents.REFRESH_LIST, (int)tokenShopItem.item_id); 
            }
            else
            {
                EventDispatcher.TriggerEvent<int>(TokenEvents.UPDATE, (int)tokenShopItem.item_id); 
            }
        }

        public void UpdateTotalCount(PbTokenTotalInfo data)
        {
            if (_tokenShopItemTotalCountDict.ContainsKey((int)data.grid_id))
            {
                _tokenShopItemTotalCountDict[(int)data.grid_id] = (int)data.buy_cnt;
            }
            else
            {
                _tokenShopItemTotalCountDict.Add((int)data.grid_id, (int)data.buy_cnt);
            }
            token_shop_item item = token_shop_helper.GetTokenShopItem((int)data.grid_id);
        }

    }
}
