﻿using Common.Structs;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class FavorData
    {
        private Dictionary<int, FavorInfo> _favorDataDic;

        public void UpdateNpcFavor(PbReputationInfo reputationInfo)
        {
            if (_favorDataDic == null)
            {
                _favorDataDic = new Dictionary<int, FavorInfo>();
            }

            if (_favorDataDic.ContainsKey((int)reputationInfo.npc_id) == true)
            {
                _favorDataDic[(int)reputationInfo.npc_id].UpdateServerData(reputationInfo);
            }
            else
            {
                _favorDataDic[(int)reputationInfo.npc_id] = new FavorInfo(reputationInfo);
            }
        }

        public FavorInfo GetFavorData(int npcId)
        {
            if (_favorDataDic != null && _favorDataDic.ContainsKey(npcId) == true)
            {
                return _favorDataDic[npcId];
            }
            return null;
        }
    }
}
