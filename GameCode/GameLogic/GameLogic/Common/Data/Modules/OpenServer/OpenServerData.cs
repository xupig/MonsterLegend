﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class OpenServerData
    {
        public const int MAX_DAY = 7;
        public enum State
        {
            processing,
            completed,
            gotReward,
        }

        public class Info
        {
            public activities_issue Issue;
            public State State;
            public int Num;

            public Info(activities_issue issue)
            {
                this.Issue = issue;
            }

            public override string ToString()
            {
                return string.Format("State:{0} Num:{1}  Id:{2}", State, Num, Issue.__id);
            }
        }

        public class IdData
        {
            public int ChineseId;
            public Dictionary<int, Info> IdDict = new Dictionary<int, Info>();

            public IdData(int chineseId)
            {
                this.ChineseId = chineseId;
            }

            public bool HaveReward()
            {
                foreach (OpenServerData.Info info in IdDict.Values)
                {
                    if (info.State == OpenServerData.State.completed)
                    {
                        return true;
                    }
                }
                return false;
            }

            public int getGotRewardCount()
            {
                int count = 0;
                foreach (OpenServerData.Info info in IdDict.Values)
                {
                    if (info.State == OpenServerData.State.gotReward)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        public class TypeData
        {
            public Dictionary<int, IdData> TypeDict = new Dictionary<int, IdData>();
        }

        public Dictionary<int, TypeData> DayDict = new Dictionary<int, TypeData>();
        public int _totalNum;


        private static string lastBeginTime = string.Empty;
        private static string lastEndTime = string.Empty;
        private static long beginTime;
        private static long endTime;
        private static bool isAvailble = false;

        private bool CheckAvailble(activities_issue issue)
        {
            if (lastBeginTime != issue.__begin_time || lastEndTime != issue.__end_time)
            {
                beginTime = MogoTimeUtil.DateTime2ServerTimeStamp(issue.__begin_time);
                endTime = MogoTimeUtil.DateTime2ServerTimeStamp(issue.__end_time);
                lastBeginTime = issue.__begin_time;
                lastEndTime = issue.__end_time;
                if (beginTime <= PlayerAvatar.Player.create_time && PlayerAvatar.Player.create_time <= endTime)
                {
                    isAvailble = true;
                }
                else
                {
                    isAvailble = false;
                }
            }
            return isAvailble;
        }

        public OpenServerData()
        {
            _totalNum = 0;
            foreach (activities_issue issue in XMLManager.activities_issue.Values)
            {
                bool isAvailble = CheckAvailble(issue);
                if (!isAvailble)
                {
                    continue;
                }
                if(!DayDict.ContainsKey(issue.__day_sort))
                {
                    DayDict.Add(issue.__day_sort, new TypeData());
                }
                Dictionary<int, IdData> TypeDict = DayDict[issue.__day_sort].TypeDict;
                if(!TypeDict.ContainsKey(issue.__sort))
                {
                    TypeDict.Add(issue.__sort, new IdData(issue.__sort));
                }
                Dictionary<int, Info> idDict = TypeDict[issue.__sort].IdDict;
                idDict.Add(issue.__id, new Info(issue));
                _totalNum++;
            }

            EventDispatcher.AddEventListener(OpenServerEvents.Update, CheckFunctionEffect);
            EventDispatcher.AddEventListener(OpenServerEvents.InitData, CheckFunctionEffect);
        }

        private void CheckFunctionEffect()
        {
            if (Check())
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.Festival);
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.openServerActivity);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.Festival);
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.openServerActivity);
            }
        }

        private bool Check()
        {
            if (PlayerDataManager.Instance.OpenServerData.HaveReward())
            {
                return true;
            }
            return false;
        }

        public void FillData(PbCreateActivityList pb)
        {
            FillList(pb.processing, State.processing);
            FillList(pb.completed, State.completed);
            FillList(pb.rewarded, State.gotReward);
            EventDispatcher.TriggerEvent(OpenServerEvents.InitData);
        }

        private void FillList(List<PbCreateActivityInfo> list, State state)
        {
            for (int i = 0; i < list.Count; i++)
            {
                activities_issue issue = activities_issue_helper.GetConfig(list[i].achv_id);
                Info info = GetInfo(issue);
                info.State = state;
                info.Num = list[i].achv_num;
            }
        }

        public Info GetInfo(activities_issue issue)
        {
            Check(issue);
            return DayDict[issue.__day_sort].TypeDict[issue.__sort].IdDict[issue.__id];
        }

        private void Check(activities_issue issue)
        {
            //Debug.LogError(string.Format("day:{0} type:{1} id:{2}", issue.__day_sort, issue.__sort, issue.__id));
            if (!DayDict.ContainsKey(issue.__day_sort))
            {
                Debug.LogError(string.Format("error day:{0}", issue.__day_sort));
            }
            if (!DayDict[issue.__day_sort].TypeDict.ContainsKey(issue.__sort))
            {
                Debug.LogError(string.Format("error day:{0} type:{1}", issue.__day_sort, issue.__sort));
            }
            if (!DayDict[issue.__day_sort].TypeDict[issue.__sort].IdDict.ContainsKey(issue.__id))
            {
                Debug.LogError(string.Format("error day:{0} type:{1} id:{2}", issue.__day_sort, issue.__sort, issue.__id));
            }
        }

        public int GetTotalNum()
        {
            return _totalNum;
        }

        public int GetCurrentCompleteNum()
        {
            int currentNum = 0;
            foreach (OpenServerData.TypeData typeData in DayDict.Values)
            {
                foreach (OpenServerData.IdData idData in typeData.TypeDict.Values)
                {
                    foreach (OpenServerData.Info info in idData.IdDict.Values)
                    {
                        if ((info.State == State.gotReward)
                            ||(info.State == State.completed))
                        {
                            currentNum++;
                        }
                    }
                }
            }
            return currentNum;
        }

        public int GetCurrentRate()
        {
            return Mathf.RoundToInt((float)GetCurrentCompleteNum() / _totalNum * 100);
        }

        public bool HaveReward()
        {
            if (CanGetFinalReward())
            {
                return true;
            }
            if (IsAllDayHaveReward())
            {
                return true;
            }
            return false;
        }

        public bool CanGetFinalReward()
        {
            if (!HaveGotFinalReward())
            {
                int commandRate = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.oper_server_final_rewards_advance));
                if (GetCurrentRate() >= commandRate)
                {
                    return true;
                }
            }
            return false;
        }

        public bool HaveGotFinalReward()
        {
            return PlayerAvatar.Player.create_activity_final_flag == 1;
        }

        public bool IsAllDayHaveReward()
        {
            int currentDay = GetCurrentDay();
            for (int i = 0; i < MAX_DAY; i++)
            {
                if(i < currentDay)
                {
                    if (IsDayHaveReward(i + 1))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsDayHaveReward(int day)
        {
            if (!DayDict.ContainsKey(day))
            {
                LoggerHelper.Info("开服活动 初始化数据找不到day:" + day);
                return false;
            }
            foreach (IdData idData in DayDict[day].TypeDict.Values)
            {
                foreach (Info info in idData.IdDict.Values)
                {
                    if (info.State == State.completed)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void UpdateOneData(PbCreateActivityInfo pb)
        {
            activities_issue issue = activities_issue_helper.GetConfig(pb.achv_id);
            Info info = GetInfo(issue);
            info.Num = pb.achv_num;
            EventDispatcher.TriggerEvent(OpenServerEvents.Update);
        }

        public void CompleteOneData(PbCreateActivityInfo pb)
        {
            activities_issue issue = activities_issue_helper.GetConfig(pb.achv_id);
            Info info = GetInfo(issue);
            info.State = State.completed;
            EventDispatcher.TriggerEvent(OpenServerEvents.Update);
        }

        public void GotRewardOneData(PbCreateActivityInfo pb)
        {
            activities_issue issue = activities_issue_helper.GetConfig(pb.achv_id);
            Info info = GetInfo(issue);
            info.State = State.gotReward;
            EventDispatcher.TriggerEvent(OpenServerEvents.Update);
        }

        public bool IsEnd()
        {
            DateTime endDate = GetZeroClockAfterDays(activities_open_helper.GetCreateRoleDay());
            DateTime currentDate = MogoTimeUtil.ServerTimeStamp2LocalTime(Global.Global.serverTimeStampSecond);
            return endDate.CompareTo(currentDate) <= 0;
        }

        public DateTime GetZeroClockAfterDays(int day)
        {
            DateTime createDate = MogoTimeUtil.ServerTimeStamp2LocalTime(PlayerAvatar.Player.create_time);
            DateTime endDate = createDate.AddDays(day);
            return new DateTime(endDate.Year, endDate.Month, endDate.Day);
        }

        public string GetLeftTimeString()
        {
            DateTime endDate = GetZeroClockAfterDays(activities_open_helper.GetCreateRoleDay());
            DateTime currentDate = MogoTimeUtil.ServerTimeStamp2LocalTime(Global.Global.serverTimeStampSecond);
            float totalSecond = (float)(endDate - currentDate).TotalSeconds;
            string templateStr = string.Concat(MogoLanguageUtil.GetContent(114983, "dd"), "    hh:mm:ss");
            return MogoTimeUtil.ToUniversalTime(Convert.ToInt32(totalSecond), templateStr);
        }

        public int GetCurrentDay()
        {
            DateTime createDate = MogoTimeUtil.ServerTimeStamp2LocalTime(PlayerAvatar.Player.create_time);
            DateTime firstDayEndDate = GetZeroClockAfterDays(1);
            DateTime currentDate = MogoTimeUtil.ServerTimeStamp2LocalTime(Global.Global.serverTimeStampSecond);
            if (currentDate.CompareTo(firstDayEndDate) < 0)
            {
                return 1;
            }
            else
            {
                return 1 + Mathf.CeilToInt((float)(currentDate - firstDayEndDate).TotalDays);
            }
        }
    }
}
