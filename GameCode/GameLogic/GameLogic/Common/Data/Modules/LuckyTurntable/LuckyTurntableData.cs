﻿using System;
using System.Collections.Generic;
using GameData;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class LuckyTurntableData
    {
        private Dictionary<LuckyTurntableType, LuckyTurntableInfo> _turntableDict;
        private Dictionary<LuckyTurntableType, int> _pointerRewardIndexDict;
        private Dictionary<LuckyTurntableType, List<LuckyRankItemData>> _luckyRankDict;
        private List<int> _luckyDescIdList;
        private int _curPlayerLv = 0;
        public bool IsSkipPointerAnim { get; set; }
        private bool[] _isRequestRankInfoTags;
        private uint[] _curLuckyRankItemIds;
        private int[] _respLuckyRankCounts;
        public bool IsTurntableReset { get; set; }

        private const int LUCKY_RANK_LIST_MAX_COUNT = 50;
        //客户端创建手气榜item数据中的id的范围
        private const uint ORIGIN_NORMAL_RANK_ITEM_ID = 100000;
        private const uint ORIGIN_ADVANCED_RANK_ITEM_ID = 500000;
        private const uint MAX_NORMAL_RANK_ITEM_ID = 500000;
        private const uint MAX_ADVANCED_RANK_ITEM_ID = 900000;

        public LuckyTurntableData()
        {
            _turntableDict = new Dictionary<LuckyTurntableType, LuckyTurntableInfo>();
            InitTurntableDict();
            InitLuckyRankDict();
            _pointerRewardIndexDict = new Dictionary<LuckyTurntableType, int>();
            _pointerRewardIndexDict.Add(LuckyTurntableType.Normal, -1);
            _pointerRewardIndexDict.Add(LuckyTurntableType.Advanced, -1);
            _luckyDescIdList = new List<int>();
            _luckyDescIdList.Add(52964);
            _luckyDescIdList.Add(52965);
            _isRequestRankInfoTags = new bool[2];
            _isRequestRankInfoTags[(int)LuckyTurntableType.Normal - 1] = false;
            _isRequestRankInfoTags[(int)LuckyTurntableType.Advanced - 1] = false;
            _curLuckyRankItemIds = new uint[2];
            _curLuckyRankItemIds[(int)LuckyTurntableType.Normal - 1] = ORIGIN_NORMAL_RANK_ITEM_ID;
            _curLuckyRankItemIds[(int)LuckyTurntableType.Advanced - 1] = ORIGIN_ADVANCED_RANK_ITEM_ID;
            _respLuckyRankCounts = new int[2];
            _respLuckyRankCounts[(int)LuckyTurntableType.Normal - 1] = 0;
            _respLuckyRankCounts[(int)LuckyTurntableType.Advanced - 1] = 0;
            _curPlayerLv = PlayerAvatar.Player.level;
        }

        private void InitTurntableDict()
        {
            List<int> findTypeList = new List<int>();
            foreach (KeyValuePair<int, lucky_turntable> item in XMLManager.lucky_turntable)
            {
                lucky_turntable info = item.Value;
                if (!findTypeList.Contains(info.__type))
                {
                    if (lucky_turntable_helper.IsTurntableInLevelLimit(info.__id))
                    {
                        if (!_turntableDict.ContainsKey((LuckyTurntableType)info.__type))
                        {
                            _turntableDict.Add((LuckyTurntableType)info.__type, new LuckyTurntableInfo(info.__id));
                        }
                        findTypeList.Add(info.__type);
                    }
                }
            }
        }

        private void InitLuckyRankDict()
        {
            _luckyRankDict = new Dictionary<LuckyTurntableType, List<LuckyRankItemData>>();
            _luckyRankDict.Add(LuckyTurntableType.Normal, new List<LuckyRankItemData>());
            _luckyRankDict.Add(LuckyTurntableType.Advanced, new List<LuckyRankItemData>());
        }

        public void UpdateTurntableInfo(List<PbLuckyTurntable> pbTurntableList)
        {
            for (int i = 0; i < pbTurntableList.Count; i++)
            {
                PbLuckyTurntable pbTurntable = pbTurntableList[i];
                if (_turntableDict.ContainsKey((LuckyTurntableType)pbTurntable.turntable_type))
                {
                    LuckyTurntableInfo turntableInfo = _turntableDict[(LuckyTurntableType)pbTurntable.turntable_type];
                    turntableInfo.UpdateTurntableRewardList(pbTurntable.reward_id, pbTurntable.reward_info);
                    if (turntableInfo.IsAutoReset)
                    {
                        turntableInfo.UpdateResetLeftTime(pbTurntable.reset_time);
                    }
                }
            }
            EventDispatcher.TriggerEvent(LuckyTurntableEvents.GET_TURNTABLE_INFO);
        }

        public void ResetTurntableInfo(PbLuckyTurntable pbTurntable)
        {
            //判断等级，如果等级到了另一阶段，就需要重新初始化奖励列表
            UpdateTurntableInfoBeforeReset(pbTurntable);
            LuckyTurntableInfo turntableInfo = _turntableDict[(LuckyTurntableType)pbTurntable.turntable_type];
            turntableInfo.ResetTurntableReward(pbTurntable.reward_id);
            if (turntableInfo.IsAutoReset)
            {
                turntableInfo.UpdateResetLeftTime(pbTurntable.reset_time);
            }
            else
            {
                turntableInfo.RemoveTimer();
            }
            _pointerRewardIndexDict[(LuckyTurntableType)pbTurntable.turntable_type] = -1;
            EventDispatcher.TriggerEvent<int>(LuckyTurntableEvents.RESET_TURNTABLE, pbTurntable.turntable_type);
        }

        private void UpdateTurntableInfoBeforeReset(PbLuckyTurntable pbTurntable)
        {
            if (_curPlayerLv == PlayerAvatar.Player.level)
                return;
            _curPlayerLv = PlayerAvatar.Player.level;
            foreach (KeyValuePair<int, lucky_turntable> item in XMLManager.lucky_turntable)
            {
                lucky_turntable info = item.Value;
                if (info.__type == pbTurntable.turntable_type)
                {
                    if (lucky_turntable_helper.IsTurntableInLevelLimit(info.__id))
                    {
                        if (_turntableDict.ContainsKey((LuckyTurntableType)info.__type))
                        {
                            if (_turntableDict[(LuckyTurntableType)info.__type].Id != info.__id)
                            {
                                _turntableDict[(LuckyTurntableType)info.__type].Clear();
                                _turntableDict[(LuckyTurntableType)info.__type] = new LuckyTurntableInfo(info.__id);
                                break;
                            }
                        }
                        else
                        {
                            _turntableDict.Add((LuckyTurntableType)info.__type, new LuckyTurntableInfo(info.__id));
                            break;
                        }
                    }
                }
            }
        }

        public void UpdateGetTurntableReward(PbLuckyTurntableReward pbTurntableReward)
        {
            LuckyTurntableInfo turntableInfo = _turntableDict[(LuckyTurntableType)pbTurntableReward.turntable_type];
            turntableInfo.UpdateGetTurntableReward(pbTurntableReward.reward_index);
            //服务端传过来的reward_index是从1开始的
            _pointerRewardIndexDict[(LuckyTurntableType)pbTurntableReward.turntable_type] = pbTurntableReward.reward_index - 1;
            EventDispatcher.TriggerEvent<int, int>(LuckyTurntableEvents.GET_TURNTABLE_REWARD, pbTurntableReward.turntable_type, pbTurntableReward.reward_index - 1);
        }

        public void UpdateLuckyRankInfo(PbLuckyTurntableRankList pbLuckyRank)
        {
            List<LuckyRankItemData> luckyRankList = _luckyRankDict[(LuckyTurntableType)pbLuckyRank.turntable_type];
            List<LuckyRankItemData> spotLightLuckyRankList = new List<LuckyRankItemData>();
            //协议列表里最前面的为最新的内容
            for (int i = pbLuckyRank.items.Count - 1; i >= 0; i--)
            {
                LuckyRankItemData itemData = CreateRankItemData(pbLuckyRank.turntable_type, pbLuckyRank.items[i]);
                luckyRankList.Insert(0, itemData);
                spotLightLuckyRankList.Insert(0, itemData);
            }

            while (luckyRankList.Count > LUCKY_RANK_LIST_MAX_COUNT)
            {
                luckyRankList.RemoveAt(luckyRankList.Count - 1);
            }
            EventDispatcher.TriggerEvent<int, List<LuckyRankItemData>>(LuckyTurntableEvents.GET_LUCKY_RANK_INFO, pbLuckyRank.turntable_type, luckyRankList);
            int responseCount = GetRespRankCount((LuckyTurntableType)pbLuckyRank.turntable_type);
            if (responseCount > 1 && spotLightLuckyRankList.Count > 0)
            {
                EventDispatcher.TriggerEvent<int, List<LuckyRankItemData>>(LuckyTurntableEvents.ON_SHOW_LUCKY_RANK_SPOT_LIGHT, pbLuckyRank.turntable_type, spotLightLuckyRankList);
            }
        }

        private LuckyRankItemData CreateRankItemData(int turntableType, PbLuckyTurntableRankListItem pbRankItem)
        {
            uint id = 0;
            if (turntableType == (int)LuckyTurntableType.Normal)
            {
                id = _curLuckyRankItemIds[(int)LuckyTurntableType.Normal - 1];
                if (id >= MAX_NORMAL_RANK_ITEM_ID)
                {
                    id = ORIGIN_NORMAL_RANK_ITEM_ID;
                }
                id++;
                _curLuckyRankItemIds[(int)LuckyTurntableType.Normal - 1] = id;
            }
            else if (turntableType == (int)LuckyTurntableType.Advanced)
            {
                id = _curLuckyRankItemIds[(int)LuckyTurntableType.Advanced - 1];
                if (id >= MAX_ADVANCED_RANK_ITEM_ID)
                {
                    id = ORIGIN_ADVANCED_RANK_ITEM_ID;
                }
                id++;
                _curLuckyRankItemIds[(int)LuckyTurntableType.Advanced - 1] = id;
            }
            LuckyRankItemData itemData = new LuckyRankItemData(id, pbRankItem.name, pbRankItem.item_id, pbRankItem.item_num);
            itemData.descId = RandomLuckyRankDesc();
            return itemData;
        }

        public LuckyTurntableInfo GetTurntableInfo(LuckyTurntableType turntableType)
        {
            return _turntableDict[turntableType];
        }

        public int GetPointerRewardIndex(LuckyTurntableType turntableType)
        {
            return _pointerRewardIndexDict[turntableType];
        }

        public List<LuckyRankItemData> GetLuckyRankInfoList(LuckyTurntableType turntableType)
        {
            return _luckyRankDict[turntableType];
        }

        public void SetHasRequestRankTag(LuckyTurntableType turntableType)
        {
            _isRequestRankInfoTags[(int)turntableType - 1] = true;
        }

        public bool GetRequestRankTag(LuckyTurntableType turntableType)
        {
            return _isRequestRankInfoTags[(int)turntableType - 1];
        }

        public int GetRespRankCount(LuckyTurntableType turntableType)
        {
            return _respLuckyRankCounts[(int)turntableType - 1];
        }

        public void IncreaseRespRankCount(LuckyTurntableType turntableType)
        {
            _respLuckyRankCounts[(int)turntableType - 1]++;
        }

        public int RandomLuckyRankDesc()
        {
            int count = _luckyDescIdList.Count;
            int index = UnityEngine.Random.Range(0, count);
            return _luckyDescIdList[index];
        }
    }

    public enum LuckyTurntableType
    {
        Normal = 1,
        Advanced
    }

    public class LuckyTurntableInfo
    {
        private int _resetCostId = 0;
        private int _resetCostNum = 0;
        private int _turntableRewardId = 0;
        private List<TurntableRewardItemData> _rewardItemList;
        private int _resetTime = 0;
        private int _resetLeftTime = 0;  //单位s
        private bool _isAutoReset = false;
        private int _nextGetRewardCostId = 0;
        private int _nextGetRewardCostNum = 0;
        private uint _resetTimerId = 0;

        public LuckyTurntableInfo(int id)
        {
            Id = id;
            lucky_turntable configInfo = lucky_turntable_helper.GetTurntableInfo(id);
            if (configInfo != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(configInfo.__reset_cost);
                foreach (var item in dataDict)
                {
                    _resetCostId = int.Parse(item.Key);
                    _resetCostNum = int.Parse(item.Value);
                }
                if (configInfo.__reset_time > 0)
                {
                    _resetTime = configInfo.__reset_time;
                    _resetLeftTime = configInfo.__reset_time;
                    _isAutoReset = true;
                }
            }
        }

        public void UpdateTurntableRewardList(int rewardId, List<int> getRewardIndexList)
        {
            if (_turntableRewardId != rewardId)
            {
                _turntableRewardId = rewardId;
                if (_rewardItemList == null)
                {
                    _rewardItemList = new List<TurntableRewardItemData>();
                }
                _rewardItemList.Clear();
                lucky_turntable_reward turntableRewardConfig = lucky_turntable_helper.GetTurntableRewardConfig(rewardId);
                Dictionary<string, string[]> dataDict = data_parse_helper.ParseMapList(turntableRewardConfig.__reward);
                foreach (var item in dataDict)
                {
                    int index = int.Parse(item.Key);
                    TurntableRewardItemData itemData = new TurntableRewardItemData(item.Value);
                    _rewardItemList.Add(itemData);
                    if (getRewardIndexList.Contains(index))
                    {
                        itemData.isReceived = true;
                        getRewardIndexList.Remove(index);
                    }
                }
            }
            else
            {
                for (int i = 0; i < _rewardItemList.Count; i++)
                {
                    if (getRewardIndexList.Contains(i + 1))
                    {
                        _rewardItemList[i].isReceived = true;
                        getRewardIndexList.Remove(i + 1);
                    }
                }
            }
            UpdateGetRewardCostInfo();
        }

        public void UpdateGetTurntableReward(int getRewardIndex)
        {
            _rewardItemList[getRewardIndex - 1].isReceived = true;
            UpdateGetRewardCostInfo();
        }

        public void ResetTurntableReward(int rewardId)
        {
            if (_turntableRewardId != rewardId)
            {
                UpdateTurntableRewardList(rewardId, new List<int>());
            }
            else
            {
                if (_rewardItemList == null)
                    return;
                for (int i = 0; i < _rewardItemList.Count; i++)
                {
                    _rewardItemList[i].isReceived = false;
                }
                UpdateGetRewardCostInfo();
            }
        }

        public void UpdateResetLeftTime(int passedTime)
        {
            ulong resetTimeStamp = (ulong)(passedTime + _resetTime);
            RemoveTimer();
            if (_resetTimerId == 0)
            {
                OnUpdateResetLeftTime(resetTimeStamp);
                int curMilliseconds = (int)(Global.Global.serverTimeStamp % 1000);
                int delayTime = 1000 - curMilliseconds;
                _resetTimerId = TimerHeap.AddTimer<ulong>((uint)delayTime, 1000, OnUpdateResetLeftTime, resetTimeStamp);
            }
        }

        private void OnUpdateResetLeftTime(ulong resetTimeStamp)
        {
            _resetLeftTime = (int)(resetTimeStamp - Global.Global.serverTimeStamp / 1000);
            if (_resetLeftTime <= 0)
            {
                _resetLeftTime = 0;
                RemoveTimer();
            }
        }

        public void RemoveTimer()
        {
            if (_resetTimerId != 0)
            {
                TimerHeap.DelTimer(_resetTimerId);
                _resetTimerId = 0;
            }
        }

        public void UpdateGetRewardCostInfo()
        {
            int haveGetRewardCount = 0;
            for (int i = 0; i < _rewardItemList.Count; i++)
            {
                if (_rewardItemList[i].isReceived)
                {
                    haveGetRewardCount++;
                }
            }
            //判断已抽完所有奖
            if (haveGetRewardCount + 1 <= _rewardItemList.Count)
            {
                lucky_turntable_reward turntableRewardConfig = lucky_turntable_helper.GetTurntableRewardConfig(_turntableRewardId);
                Dictionary<string, string[]> dataDict = data_parse_helper.ParseMapList(turntableRewardConfig.__cost);
                string[] costInfo = dataDict[(haveGetRewardCount + 1).ToString()];
                if (costInfo.Length >= 2)
                {
                    _nextGetRewardCostId = int.Parse(costInfo[0]);
                    _nextGetRewardCostNum = int.Parse(costInfo[1]);
                }
            }
        }

        public void Clear()
        {
            RemoveTimer();
        }

        public int Id
        {
            get;
            private set;
        }

        public List<TurntableRewardItemData> RewardItemList
        {
            get 
            {
                if (_rewardItemList == null)
                {
                    _rewardItemList = new List<TurntableRewardItemData>();
                }
                return _rewardItemList;
            }
        }

        public int ResetCostId
        {
            get { return _resetCostId; }
        }

        public int ResetCostNum
        {
            get { return _resetCostNum; }
        }

        public int ResetLeftTime
        {
            get 
            {
                return _resetLeftTime; 
            }
        }

        public bool IsAutoReset
        {
            get { return _isAutoReset; }
        }

        public int NextGetRewardCostId
        {
            get { return _nextGetRewardCostId; }
        }

        public int NextGetRewardCostNum
        {
            get { return _nextGetRewardCostNum; }
        }
    }

    public class TurntableRewardItemData
    {
        public int itemId = 0;
        public int itemNum = 0;
        public bool isReceived = false;
        public bool isEnterLuckyRank = false;

        public TurntableRewardItemData(string[] strRewardInfos)
        {
            itemId = int.Parse(strRewardInfos[(int)TurntableRewardTag.ItemId]);
            itemNum = int.Parse(strRewardInfos[(int)TurntableRewardTag.ItemNum]);
            isEnterLuckyRank = int.Parse(strRewardInfos[(int)TurntableRewardTag.LuckyRank]) == 0 ? false : true;
        }
    }

    public class LuckyRankItemData
    {
        public uint Id  //客户端自己生成的id
        {
            get;
            private set;
        }
        public string playerName = "";
        public int itemId = 0;
        public int itemNum = 0;
        public int descId = 0;

        public LuckyRankItemData(uint id, string playerName, int itemId, int itemNum)
        {
            this.Id = id;
            this.playerName = playerName;
            this.itemId = itemId;
            this.itemNum = itemNum;
        }
    }
}
