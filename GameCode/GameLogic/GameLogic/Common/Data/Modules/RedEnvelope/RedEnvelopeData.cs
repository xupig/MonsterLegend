﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;

namespace Common.Data
{
    public class RedEnvelopeData
    {
        private Dictionary<int, RedEnvelopeInfo> _redEnvelopeGrabedDict = new Dictionary<int, RedEnvelopeInfo>();

        public void AddGrabRedEnvelope(int envelopeId, RedEnvelopeInfo info)
        {
            if (!_redEnvelopeGrabedDict.ContainsKey(envelopeId))
            {
                _redEnvelopeGrabedDict.Add(envelopeId, info);
            }
        }

        public RedEnvelopeInfo GetRedEnvelope(int envelopeId)
        {
            if (_redEnvelopeGrabedDict.ContainsKey(envelopeId))
            {
                return _redEnvelopeGrabedDict[envelopeId];
            }
            return null;
        }

        public bool ContainsGrabRedEnvelope(int envelopeId)
        {
            return _redEnvelopeGrabedDict.ContainsKey(envelopeId);
        }

        public bool RemoveRedEnvelope(int envelopeId)
        {
            if (_redEnvelopeGrabedDict.ContainsKey(envelopeId))
            {
                _redEnvelopeGrabedDict.Remove(envelopeId);
                return true;
            }
            return false;
        }
    }

    public class RedEnvelopeInfo
    {
        public ulong playerDbid;
        public string playerName;
        public int playerLv;
        public int playerVocation;
        public int envelopeConfigId;
        public int systemInfoId;
        public List<RewardData> rewardList;
        public RedEnvelopeType envelopeType;

        public RedEnvelopeInfo(PbRedEnvelopeLink pbEnvelopeLink, int systemInfoId, int envelopeType)
        {
            playerDbid = pbEnvelopeLink.dbid;
            playerName = pbEnvelopeLink.name;
            playerLv = (int)pbEnvelopeLink.level;
            playerVocation = (int)pbEnvelopeLink.vocation;
            envelopeConfigId = (int)pbEnvelopeLink.envelope_type_id;
            this.systemInfoId = systemInfoId;
            this.envelopeType = (RedEnvelopeType)envelopeType;
        }
        
    }

    public enum RedEnvelopeType
    {
        Level = 1,
        Guild = 2,
        Festival = 3
    }
}
