﻿#region 模块信息
/*==========================================
// 文件名：PbOtherPlayerInfo
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.PlayerInfo
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 16:05:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PbOtherPlayerInfo
    {
        public UInt64 dbId;
        public string name;
        public UInt64 serverLastUpdateTime;//服务器属性最近变更时间
        public UInt64 expireDate;//失效时间
    }
}
