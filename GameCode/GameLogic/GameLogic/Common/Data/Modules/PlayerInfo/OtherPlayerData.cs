﻿#region 模块信息
/*==========================================
// 文件名：OtherPlayerData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.PlayerInfo
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 16:04:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class OtherPlayerData
    {
        private Dictionary<string, PbOtherPlayerInfo> otherPlayerDict;
        private List<string> otherPlayerList;

        private const UInt64 expireTime = 600000;//10分钟

        private const int MAX_COUNT = 10;

        public OtherPlayerData()
        {
            otherPlayerDict = new Dictionary<string, PbOtherPlayerInfo>();
            otherPlayerList = new List<string>();
        }

        public void AddOtherPlayerInfo(PbOtherPlayerInfo otherPlayerInfo,int mode)
        {
            otherPlayerInfo.expireDate = Common.Global.Global.serverTimeStamp + expireTime;
            if(otherPlayerDict.ContainsKey(otherPlayerInfo.name) == false)
            {
                otherPlayerDict.Add(otherPlayerInfo.name, otherPlayerInfo);
                otherPlayerList.Add(otherPlayerInfo.name);
                if (otherPlayerList.Count > MAX_COUNT)
                {
                    string name = otherPlayerList[0];
                    otherPlayerList.RemoveAt(0);
                    otherPlayerDict.Remove(name);
                }
            }
            else
            {
                otherPlayerDict[otherPlayerInfo.name] = otherPlayerInfo;
                otherPlayerList.Remove(otherPlayerInfo.name);
                otherPlayerList.Add(otherPlayerInfo.name);
            }
            TriggerEvent();
        }

        public void RequestOtherPlayerInfo(string name,int mode)
        {
            if(otherPlayerDict.ContainsKey(name))
            {
                if(otherPlayerDict[name].expireDate < Common.Global.Global.serverTimeStamp)
                {
                    //request
                    return;
                }
            }
            TriggerEvent();
            //event dispatcher
        }

        private void TriggerEvent()
        {

        }

    }
}
