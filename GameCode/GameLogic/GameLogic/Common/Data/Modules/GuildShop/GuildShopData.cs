﻿using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class GuildShopData
    {
        private PbGuildShopInfo _info;
        private Dictionary<uint, PbGuildShopData> guildShopDataDict = new Dictionary<uint,PbGuildShopData>();
        private Dictionary<uint, uint> guildShopStoreCountDict = new Dictionary<uint,uint>();
        private HashSet<uint> myGuildPraiseDict = new HashSet<uint>();
        private Dictionary<uint, int> guildShopItemRankDict = new Dictionary<uint,int>();
        private List<PbGuildShopData> sortList = new List<PbGuildShopData>();

        public HashSet<uint> MyPraiseData
        {
            get
            {
                return myGuildPraiseDict;
            }
        }

        public int GetGuildShopItemRank(uint id)
        {
            if(guildShopItemRankDict.ContainsKey(id))
            {
                return guildShopItemRankDict[id];
            }
            return 999;
        }

        public void BuyShopData(PbGuildShopChangeData data)
        {
            if (guildShopDataDict.ContainsKey(data.grid_id) && guildShopDataDict[data.grid_id].remain_count >= data.item_count)
            {
                guildShopDataDict[data.grid_id].remain_count -= data.item_count;
            }
            if (guildShopStoreCountDict.ContainsKey(data.grid_id))
            {
                guildShopStoreCountDict[data.grid_id] += data.item_count;
            }
            EventDispatcher.TriggerEvent(GuildShopEvents.Refresh_Item, data.grid_id);
        }

        public void ExchangeShopData(PbGuildShopChangeData data)
        {
            if (guildShopDataDict.ContainsKey(data.grid_id))
            {
                guildShopDataDict[data.grid_id].daily_buy_count += data.item_count;
            }
            if (guildShopStoreCountDict.ContainsKey(data.grid_id) && guildShopStoreCountDict[data.grid_id] >= data.item_count)
            {
                guildShopStoreCountDict[data.grid_id] -= data.item_count;
            }
            EventDispatcher.TriggerEvent(GuildShopEvents.Refresh_Item, data.grid_id);
        }

        public void Fill(PbGuildShopInfo pbInfo)
        {
            _info = pbInfo;
            guildShopDataDict.Clear();
            sortList.Clear();
            for(int i=0;i<pbInfo.shop_data.Count;i++)
            {
                guildShopDataDict.Add(pbInfo.shop_data[i].grid_id, pbInfo.shop_data[i]);
                sortList.Add(pbInfo.shop_data[i]);
            }
            sortList.Sort(SortFunc);
            guildShopItemRankDict.Clear();
            for (int i = 0; i < sortList.Count;i++ )
            {
                guildShopItemRankDict.Add(sortList[i].grid_id, i + 1);
            }
            guildShopStoreCountDict.Clear();
            for(int i=0;i<pbInfo.store_data.Count;i++)
            {
                guildShopStoreCountDict.Add(pbInfo.store_data[i].grid_id, pbInfo.store_data[i].remain_count);
            }

            myGuildPraiseDict.Clear();
            for(int i=0;i<pbInfo.praise_data.Count;i++)
            {
                myGuildPraiseDict.Add(pbInfo.praise_data[i]);
            }
        }

        private int SortFunc(PbGuildShopData shop1,PbGuildShopData shop2)
        {
            return Convert.ToInt32(shop2.praise_count) - Convert.ToInt32(shop1.praise_count);
        }

        public int GetMyPraiseCount()
        {
            return myGuildPraiseDict.Count;
        }

        public bool HasPraised(uint id)
        {
            return myGuildPraiseDict.Contains(id);
        }

        public PbGuildShopData GetGuildShopData(uint id)
        {
            if(guildShopDataDict.ContainsKey(id))
            {
                return guildShopDataDict[id];
            }
            return null;
        }

        public uint GetGuildShopStoreCount(uint id)
        {
            if(guildShopStoreCountDict.ContainsKey(id))
            {
                return guildShopStoreCountDict[id];
            }
            return 0;
        }

        public bool IsBuyLimit(uint id)
        {
            guild_shop shop = guild_shop_helper.GetGuildShop(Convert.ToInt32(id));
            Dictionary<string, string> dict = data_parse_helper.ParseMap(shop.__cost);
            bool result = !(guild_rights_helper.CheckHasRights(GuildRightId.guild_shop) && dict.Count > 0);
            if(guildShopDataDict == null)
            {
                return result;
            }
            if(guildShopDataDict.ContainsKey(id))
            {
                
                if(shop.__player_buy_limit_all == 0)
                {
                    return false;
                }
                if (guildShopDataDict[id].total_buy_count >= shop.__player_buy_limit_all)
                {
                    return result;
                }
                return false;
            }
            return result;
        }


        public void UpdatePraise(bool isPraise, PbGuildPraiseData pbInfo)
        {
            for (int i = 0; i < _info.shop_data.Count; i++)
            {
                if (_info.shop_data[i].grid_id == pbInfo.grid_id)
                {
                    _info.shop_data[i].praise_count = pbInfo.praise_count;
                }
            }
            for (int i = _info.praise_data.Count - 1; i >= 0; i--)
            {
                if (_info.praise_data[i] == pbInfo.grid_id)
                {
                    _info.praise_data.RemoveAt(i);
                }
            }
            myGuildPraiseDict.Remove(pbInfo.grid_id);
            if (isPraise)
            {
                _info.praise_data.Add(pbInfo.grid_id);
                myGuildPraiseDict.Add(pbInfo.grid_id);
            }
            sortList.Sort(SortFunc);
            guildShopItemRankDict.Clear();
            for (int i = 0; i < sortList.Count;i++ )
            {
                guildShopItemRankDict.Add(sortList[i].grid_id, i + 1);
            }
        }
    }
}
