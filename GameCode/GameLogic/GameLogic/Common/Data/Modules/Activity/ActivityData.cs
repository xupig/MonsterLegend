﻿#region 模块信息
/*==========================================
// 文件名：ActivityData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Activity
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/21 10:22:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class ActivityData
    {
        private Dictionary<int,int> _activityDict;

        public bool isDataRequested;
        public bool isDataResponsed;

        private bool _hasNewChallenge = false;
        private bool _hasNewActivity = false;
        private bool _hasActiveActivity = false;
        public bool hasNewChallenge
        {
            get { return _hasNewChallenge; }
            set { _hasNewChallenge = value; UpdateState(); }
        }
        public bool hasNewActivity
        {
            get { return _hasNewActivity; }
            set { _hasNewActivity = value; UpdateState(); }
        }
        public bool hasActiveActivity
        {
            get { return _hasActiveActivity; }
            set { _hasActiveActivity = value; UpdateState(); }
        }

        private void UpdateState()
        {
            PlayerDataManager.Instance.FunctionData.AddFunctionPoint(9, _hasNewChallenge);
            PlayerDataManager.Instance.FunctionData.AddFunctionPoint(10, _hasNewActivity || _hasActiveActivity);
            EventDispatcher.TriggerEvent(ActivityEvents.ACTIVITY_CHANGE_STATE);
        }

        public ActivityData()
        {
            _activityDict = new Dictionary<int, int>();
        }

        public void SetActivityData(List<PbUnitLeftCount> leftCounts)
        {
            isDataResponsed = true;
            _activityDict.Clear();
            for(int i=0;i<leftCounts.Count;i++)
            {
                _activityDict.Add((int)leftCounts[i].id,(int)leftCounts[i].count);
            }
        }

        public void AddActivity(int activity)
        {
            if (_activityDict.ContainsKey(activity))
            {
                _activityDict[activity] += 1; 
            }
            else
            {
                _activityDict.Add(activity,1);
            }
            
        }

        public int GetActivityCount(int activity)
        {
            if(_activityDict.ContainsKey(activity))
            {
                return _activityDict[activity];
            }
            return 0;
        }


    }
}
