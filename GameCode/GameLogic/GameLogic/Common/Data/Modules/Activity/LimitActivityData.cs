﻿#region 模块信息
/*==========================================
// 文件名：LimitiActivityData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Activity
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 16:57:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class ActivityNoticeData
    {
        public List<int> noticeTimeList;
        public List<string> noticeActionList;
    }

    public class LimitActivityData : ActivityNoticeData
    {
        public long startDate;
        public long endDate;

        public List<int> openWeekOfDay;

        public List<int> openTimeStart;
        public List<int> openTimeEnd;
        public open openData;
        public function functionData;

    }
}
