﻿using Common.Events;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class WelfareActiveTabData
    {
        public WelfareActiveTabData()
        {
            EventDispatcher.AddEventListener(OpenServerEvents.Update, CheckFunctionEffect);
            EventDispatcher.AddEventListener(OpenServerEvents.InitData, CheckFunctionEffect);
            EventDispatcher.AddEventListener(ChargeActivityEvents.RefreshChargeContent, CheckFunctionEffect);
        }

        private void CheckFunctionEffect()
        {
            if (Check())
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.welfare);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.welfare);
            }
        }

        private bool Check()
        {
            if (PlayerDataManager.Instance.ChargeActivityData.CanGetReward())
            {
                return true;
            }
            return false;
        }
    }
}
