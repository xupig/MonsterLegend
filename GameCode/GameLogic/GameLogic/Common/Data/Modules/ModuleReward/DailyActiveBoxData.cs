﻿using System;
using System.Collections.Generic;
using Common.Utils;
using GameData;
using GameMain.Entities;

namespace Common.Data
{
    public class DailyActiveBoxData
    {
        private active_reward _activeRewardInfo;
        private string _desc = string.Empty;
        private int[] _lvRange = {0, 0};
        private List<RewardData> _rewardList;
        private int _infoIndex = 0;
        private int _rewardLv = 0;
        private int _vipNeed = 0;
        private int _rewardFlag = reward_system_helper.HAVE_REWARD_FLAG;
        private int _vipRewardFlag = reward_system_helper.HAVE_REWARD_FLAG;
        private int _status = (int)ActiveBoxStatus.CanNotGetReward;

        public DailyActiveBoxData(int id)
        {
            this.Id = id;
            active_reward activeRewardInfo = reward_system_helper.GetActiveRewardInfo(id);
            _activeRewardInfo = activeRewardInfo;
        }

        public void UpdateInfo(int rewardFlag, int vipRewardFlag)
        {
            UpdateInfo(_rewardLv, rewardFlag, vipRewardFlag);
        }

        public void UpdateInfo(int rewardLv, int rewardFlag, int vipRewardFlag)
        {
            if (_rewardLv != rewardLv || _rewardLv == 0 || rewardFlag != reward_system_helper.NOT_HAVE_REWARD_FLAG)
            {
                _rewardLv = rewardLv;
                UpdateDesc();
                UpdateRewardList();
                UpdateVipNeed();
            }
            _rewardFlag = rewardFlag;
            _vipRewardFlag = vipRewardFlag;
        }

        //更新与奖励等级相关的一些信息
        public void UpdateRewardLvInfos()
        {
            //如果已经领取过奖励，则不再更新
            if (_rewardLv != 0 && _rewardFlag == reward_system_helper.NOT_HAVE_REWARD_FLAG)
                return;
            UpdateLvRange();
            if (_rewardLv != _infoIndex + 1)
            {
                _rewardLv = _infoIndex + 1;
                UpdateDesc();
                UpdateRewardList();
                UpdateVipNeed();
            }
        }

        //更新是否满足领取常规奖励条件，在当前的activePoint有变化时更新
        public void UpdateStatus(int curActivePoint)
        {
            if (curActivePoint >= Point)
            {
                _status = (int)ActiveBoxStatus.CanGetReward;
            }
            else
            {
                _status = (int)ActiveBoxStatus.CanNotGetReward;
            }
        }

        private void UpdateDesc()
        {
            List<string> dataList = data_parse_helper.ParseListString(_activeRewardInfo.__des);
            string descId = dataList[_rewardLv - 1];
            _desc = MogoLanguageUtil.GetContent(descId);
        }

        private void UpdateRewardList()
        {
            List<int> rewardIdList = new List<int>();
            List<string> dataList = data_parse_helper.ParseListString(_activeRewardInfo.__reward);
            rewardIdList.Add(int.Parse(dataList[_rewardLv - 1]));
            _rewardList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
        }

        private void UpdateVipNeed()
        {
            List<string> dataList = data_parse_helper.ParseListString(_activeRewardInfo.__vip_need);
            _vipNeed = int.Parse(dataList[_rewardLv - 1]);
        }

        public void UpdateLvRange()
        {
            int playerLv = PlayerAvatar.Player.level;
            int index = 0;
            Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList(_activeRewardInfo.__level);
            foreach (var item in dataDictList)
            {
                string[] lvRangeInfo = item.Value;
                int minLv = int.Parse(lvRangeInfo[0]);
                int maxLv = int.Parse(lvRangeInfo[1]);
                if (playerLv >= minLv && playerLv <= maxLv)
                {
                    _lvRange[0] = minLv;
                    _lvRange[1] = maxLv;
                    _infoIndex = index;
                    break;
                }
                index++;
            }
        }

        public int Id
        {
            get;
            protected set;
        }

        public int Point
        {
            get
            {
                return _activeRewardInfo.__point;
            }
        }

        public string Desc
        {
            get 
            {
                if (string.IsNullOrEmpty(_desc))
                {
                    if (_rewardLv > 0)
                    {
                        UpdateDesc();
                    }
                }
                return _desc; 
            }
        }

        public int VipNeed
        {
            get { return _vipNeed; }
        }

        public List<RewardData> RewardList
        {
            get
            {
                UpdateRewardLvInfos();
                return _rewardList;
            }
        }

        public int RewardFlag
        {
            get { return _rewardFlag; }
        }

        public int VipRewardFlag
        {
            get { return _vipRewardFlag; }
        }

        public int Status
        {
            get { return _status; }
        }

        public int RewardLv
        {
            get { return _rewardLv; }
        }
    }

    public enum ActiveBoxStatus
    {
        CanNotGetReward = 0,
        CanGetReward
    }
}
