﻿using System;
using System.Collections.Generic;
using GameData;
using GameLoader.Utils;

namespace Common.Data
{
    public class VipGiftData
    {
        private List<VipGiftItemData> _vipGiftList = new List<VipGiftItemData>();

        public List<VipGiftItemData> VipGiftList
        {
            get { return _vipGiftList; }
        }

        public VipGiftData()
        {
            InitVipGiftList();
        }

        public void InitVipGiftList()
        {
            GameDataTable<int, vip> vipInfoDict = XMLManager.vip;
            foreach (KeyValuePair<int, vip> item in vipInfoDict)
            {
                int id = item.Key;
                VipGiftItemData itemData = new VipGiftItemData(id);
                _vipGiftList.Add(itemData);
            }
        }

        public List<VipGiftItemData> GetGiftItemListByCount(int count)
        {
            if (count > _vipGiftList.Count)
            {
                LoggerHelper.Error("GetGiftItemListByCount count is larger than vipGiftList count!!");
                return null;
            }
            List<VipGiftItemData> itemList = new List<VipGiftItemData>();
            for (int i = 0; i < count; i++)
            {
                itemList.Add(_vipGiftList[i]);
            }
            return itemList;
        }

        public uint GetReachVipNeedCost(int vipLv)
        {
            if (vipLv > VipGiftList.Count)
            {
                return 0;
            }
            VipGiftItemData itemData = VipGiftList[vipLv - 1];
            return itemData.GetReachVipNeedCost();
        }

        public bool IsShowTips()
        {
            bool result = false;
            for (int i = 0; i < _vipGiftList.Count; i++)
            {
                VipGiftItemData itemData = _vipGiftList[i];
                if (itemData.RewardFlag == reward_system_helper.HAVE_REWARD_FLAG && itemData.IsReachVipNeed())
                {
                    result = true;
                    break;
                }
            }
            return result && function_helper.IsFunctionOpen(FunctionId.VIP);
        }
    }
}
