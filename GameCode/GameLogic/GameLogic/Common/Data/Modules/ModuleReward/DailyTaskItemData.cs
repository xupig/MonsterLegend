﻿using System;
using System.Collections.Generic;
using GameData;
using Common.Utils;
using GameMain.Entities;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class DailyTaskItemData : BaseDailyTaskItemData
    {
        private daily_task _dailyTaskInfo = null;
        private string _desc = string.Empty;
        private int _status = (int)DailyTaskStatus.Accept;
        private int _timeStatus = (int)DailyTaskTimeStatus.Valid;  //默认为有效状态
        private DailyTaskTime[] _validTimeLimit = new DailyTaskTime[2];
        private int _type = (int)DailyTaskType.NormalTask;
        private int _playerLv = 0;
        private int _curSameTypeNum = 0;
        private int _maxSameTypeNum = 0;
        private uint _timerId = 0;
        private int _lastTimeStatus = (int)DailyTaskTimeStatus.Valid;
        private int _maxTriggerNum = 0;

        public DailyTaskItemData(int id)
            : base(id)
        {
            daily_task dailyTaskInfo = reward_system_helper.GetDailyTaskInfo(id);
            if (dailyTaskInfo == null)
            {
                LoggerHelper.Error("DailyTaskItemData not exsit daily task config ! task id = " + id);
                return;
            }
            _dailyTaskInfo = dailyTaskInfo;
            if (dailyTaskInfo != null)
            {
                _iconId = dailyTaskInfo.__icon;
                List<string> dataList = data_parse_helper.ParseListString(dailyTaskInfo.__desc);
                if (dataList.Count >= 2)
                {
                    _name = MogoLanguageUtil.GetContent(dailyTaskInfo.__desc[0]);
                    _desc = MogoLanguageUtil.GetContent(dailyTaskInfo.__desc[1]);
                }
                else
                {
                    LoggerHelper.Error("[DailyTaskItemData]  This desc config not have name and desc element, this config must contain this two element!!!  daily task id = " + id);
                }
                
                //从配置表中读取出来的字符串将"\n"转为了"\\n"，要将其替换为“\n”
                _desc = _desc.Replace("\\n", "\n");
                if (CheckIsHasTimeLimit(dailyTaskInfo))
                {
                    InitValidTimeLimit(dailyTaskInfo);
                    //如果有时间限制，就要在描述中显示时间                   
                    string strMinTime = ProcessStrTimeFormat(_validTimeLimit[0]);
                    string strMaxTime = ProcessStrTimeFormat(_validTimeLimit[1]);
                    if (!string.IsNullOrEmpty(_desc))
                        _desc = string.Format(_desc, strMinTime, strMaxTime);
                    UpdateTimeStatus(dailyTaskInfo);
                }

                _type = dailyTaskInfo.__type;
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(_dailyTaskInfo.__event_cnds);
                if (dataDict.Count > 0)
                {
                    foreach (var item in dataDict)
                    {
                        _eventId = int.Parse(item.Key);
                        _desc = ProcessEventDesc(_desc, _eventId);
                        _eventAtomId = GetEventAtomId();
                        _maxTriggerNum += int.Parse(item.Value);
                        break;
                    }
                }
            }
        }

        private string ProcessStrTimeFormat(DailyTaskTime taskTime)
        {
            string strTime = "";
            string strHour = "";
            string strMinute = "";
            strHour = taskTime.hour <= 9 ? ("0" + taskTime.hour.ToString()) : taskTime.hour.ToString();
            strMinute = taskTime.minute <= 9 ? ("0" + taskTime.minute.ToString()) : taskTime.minute.ToString();
            strTime = string.Format("{0}:{1}", strHour, strMinute);
            return strTime;
        }

        public bool CheckIsHasTimeLimit()
        {
            return CheckIsHasTimeLimit(_dailyTaskInfo);
        }

        private bool CheckIsHasTimeLimit(daily_task dailyTaskInfo)
        {
            return !string.IsNullOrEmpty(dailyTaskInfo.__time);
        }

        private void InitValidTimeLimit(daily_task dailyTaskInfo)
        {
            List<string> timeList = data_parse_helper.ParseListString(dailyTaskInfo.__time);
            DailyTaskTime minTime = ParseDailyTaskTime(timeList[0]);
            DailyTaskTime maxTime = ParseDailyTaskTime(timeList[1]);
            _validTimeLimit[0] = minTime;
            _validTimeLimit[1] = maxTime;
        }

        private void UpdateTimeStatus(daily_task dailyTaskInfo)
        {
            if (CheckIsHasTimeLimit(dailyTaskInfo))
            {
                int timeStatus = GetTimeStatus();
                if (timeStatus == (int)DailyTaskTimeStatus.Valid)
                {
                    if (_lastTimeStatus != (int)DailyTaskTimeStatus.Valid)
                    {
                        _lastTimeStatus = timeStatus;
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK_TIME_CHANGED);
                    }
                    StartTimeCountDown();
                }
                else if (timeStatus == (int)DailyTaskTimeStatus.NotStarted)
                {
                    if (_lastTimeStatus != (int)DailyTaskTimeStatus.NotStarted)
                    {
                        _lastTimeStatus = timeStatus;
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK_TIME_CHANGED);
                    }
                    StartTimeCountDown();
                }
                else if (timeStatus == (int)DailyTaskTimeStatus.HaveEnded)
                {
                    ResetTimer();
                    if (_lastTimeStatus != (int)DailyTaskTimeStatus.HaveEnded)
                    {
                        _lastTimeStatus = timeStatus;
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK_TIME_CHANGED);
                    }
                }
            }
        }

        private int GetTimeStatus()
        {
            int timeStatus = (int)DailyTaskTimeStatus.Valid;
            DateTime curDateTime = reward_system_helper.GetCurDateTime();
            int curSecond = CalculateSeconds(curDateTime);
            DailyTaskTime minTime = _validTimeLimit[0];
            DailyTaskTime maxTime = _validTimeLimit[1];
            DateTime validMinDateTime = MogoTimeUtil.GetUtcTime(curDateTime.Year, curDateTime.Month, curDateTime.Day, minTime.hour, minTime.minute, 0);
            DateTime validMaxDateTime = MogoTimeUtil.GetUtcTime(curDateTime.Year, curDateTime.Month, curDateTime.Day, maxTime.hour, maxTime.minute, 0);
            int minSecond = CalculateSeconds(validMinDateTime);
            int maxSecond = CalculateSeconds(validMaxDateTime);
            if (curSecond >= minSecond && curSecond <= maxSecond)
            {
                timeStatus = (int)DailyTaskTimeStatus.Valid;
            }
            else if (curSecond < minSecond)
            {
                timeStatus = (int)DailyTaskTimeStatus.NotStarted;
            }
            else if (curSecond > maxSecond)
            {
                timeStatus = (int)DailyTaskTimeStatus.HaveEnded;
            }
            return timeStatus;
        }

        public void StartTimeCountDown()
        {
            if (_timerId != 0)
                return;
            _timerId = TimerHeap.AddTimer(0, 1000, OnUpdateTimeStatus);
        }

        private void OnUpdateTimeStatus()
        {
            UpdateTimeStatus(_dailyTaskInfo);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private int CalculateSeconds(DateTime dateTime)
        {
            int seconds = dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second;
            return seconds;
        }

        private DailyTaskTime ParseDailyTaskTime(string time)
        {
            if (string.IsNullOrEmpty(time) || time.Length != 4)
            {
                LoggerHelper.Error("daily_task time_l 字段的配置格式不正确!");
                return null;
            }
            DailyTaskTime timeInfo = new DailyTaskTime();
            //比如：1200
            string strHour = time.Substring(0, 2);
            if (strHour[0] == '0')
            {
                strHour.Remove(0, 1);
            }
            string strMinute = time.Substring(2, 2);
            if (strMinute[0] == '0')
            {
                strMinute.Remove(0, 1);
            }
            int hour = int.Parse(strHour);
            int minute = int.Parse(strMinute);
            timeInfo.hour = hour;
            timeInfo.minute = minute;

            return timeInfo;
        }

        private string ProcessEventDesc(string descFormat, int eventId)
        {
            string desc = descFormat;
            Dictionary<int, float> condsInfo = event_cnds_helper.GetEventCnds(eventId);
            if (condsInfo.Count > 0)
            {
                foreach (var item in condsInfo)
                {
                    int condsId = item.Key;
                    if (condsId == (int)EventCondsParamId.OnceNum)
                    {
                        int num = (int)item.Value;
                        desc = string.Format(descFormat, num);
                        break;
                    }
                }
            }
            return desc;
        }

        public void UpdateTypeNumInfo(int curSameTypeNum, int maxSameTypeNum)
        {
            _curSameTypeNum = curSameTypeNum;
            _maxSameTypeNum = maxSameTypeNum;
            UpdateNameWithMultipleSameType();
        }

        private void UpdateNameWithMultipleSameType()
        {
            _name = string.Format(_name, _curSameTypeNum);
        }

        public bool IsShowTaskTypeNumProgress()
        {
            return (_maxSameTypeNum != 0 && _status != (int)DailyTaskStatus.Complete);
        }

        public string Desc
        {
            get { return _desc; }
        }

        public override List<RewardData> RewardList
        {
            get
            {
                if (_rewardList == null)
                {
                    _playerLv = PlayerAvatar.Player.level;
                    HandleRewardList();
                }
                else if (_playerLv != PlayerAvatar.Player.level)
                {
                    _playerLv = PlayerAvatar.Player.level;
                    if (_rewardList != null)
                    {
                        _rewardList.Clear();
                        _rewardList = null;
                    }
                    HandleRewardList();
                }
                return _rewardList;
            }
        }

        private void HandleRewardList()
        {
            List<int> rewardIdList = new List<int>();
            if (_dailyTaskInfo != null)
            {
                rewardIdList.Add(_dailyTaskInfo.__reward);
            }
            _rewardList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int TimeStatus
        {
            get 
            {
                if (_dailyTaskInfo != null)
                {
                    if (CheckIsHasTimeLimit(_dailyTaskInfo))
                    {
                        int timeStatus = GetTimeStatus();
                        _timeStatus = timeStatus;
                    }
                }
                return _timeStatus;
            }
        }

        public int TaskType
        {
            get { return _type; }
        }

        public int CurSameTypeNum
        {
            get { return _curSameTypeNum; }
        }

        public int MaxSameTypeNum
        {
            get { return _maxSameTypeNum; }
        }

        public int MaxTriggerNum
        {
            get { return _maxTriggerNum; }
        }
    }

    public class DailyTaskTime
    {
        public int hour;
        public int minute;
    }

    public enum DailyTaskStatus
    {
        Accept = 0,
        CanCommit,
        Complete,
        CanDelete  //过期可删除状态
    }

    public enum DailyTaskTimeStatus
    {
        NotStarted = 0,
        Valid,
        HaveEnded
    }

    public enum DailyTaskType
    {
        MonthCard = 1,  //月卡
        NormalTask
    }
}
