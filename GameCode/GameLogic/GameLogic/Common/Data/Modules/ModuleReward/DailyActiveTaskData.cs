﻿using System;
using System.Collections.Generic;
using Common.Events;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;

namespace Common.Data
{
    public class DailyActiveTaskData : BaseDailyTaskData
    {
        private List<DailyActiveBoxData> _dailyActiveBoxList = new List<DailyActiveBoxData>();
        private int _curActivePoint = 0;
        private int _maxActivePoint = 0;

        protected override int BaseDailyTaskType
        {
            get { return (int)DailyTaskItemType.DailyActiveTask; }
        }

        public List<BaseDailyTaskItemData> DailyActiveList
        {
            get { return _baseDailyTaskList; }
        }

        public List<DailyActiveBoxData> DailyActiveBoxList
        {
            get { return _dailyActiveBoxList; }
        }

        public int CurActivePoint
        {
            get { return _curActivePoint; }
        }

        public int MaxActivePoint
        {
            get { return _maxActivePoint; }
        }

        public DailyActiveTaskData()
        {
            InitDailyActiveBox();
            InitMaxActivePoint();
        }

        public void OnReloadData()
        {
            ResetDailyActiveList();
            ResetActiveBoxList();
        }

        private void InitDailyActiveBox()
        {
            GameDataTable<int, active_reward> activeRewardDict = XMLManager.active_reward;
            foreach (KeyValuePair<int, active_reward> item in activeRewardDict)
            {
                active_reward rewardInfo = item.Value;
                DailyActiveBoxData boxData = new DailyActiveBoxData(rewardInfo.__id);
                _dailyActiveBoxList.Add(boxData);
            }
            _dailyActiveBoxList.Sort(SortBoxList);
        }

        private void InitMaxActivePoint()
        {
            GameDataTable<int, active_reward> activeRewardDict = XMLManager.active_reward;
            int activePoint = 0;
            foreach (KeyValuePair<int, active_reward> item in activeRewardDict)
            {
                active_reward rewardInfo = item.Value;
                if (rewardInfo.__point > activePoint)
                {
                    activePoint = rewardInfo.__point;
                }
            }
            _maxActivePoint = activePoint;
        }

        public override bool IsShowTips()
        {
            bool result = false;
            if (IsHasCanCommitTask() || IsHasBoxReward())
            {
                result = true;
            }
            return result;
        }

        private bool IsHasCanCommitTask()
        {
            bool result = false;
            for (int i = 0; i < DailyActiveList.Count; i++)
            {
                DailyActiveItemData itemData = DailyActiveList[i] as DailyActiveItemData;
                if (itemData.Status == (int)DailyActiveStatus.CanCommit)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private bool IsHasBoxReward()
        {
            bool result = false;
            int curVip = PlayerAvatar.Player.vip_level;
            for (int i = 0; i < _dailyActiveBoxList.Count; i++)
            {
                DailyActiveBoxData boxData = _dailyActiveBoxList[i];
                if (boxData.Status == (int)ActiveBoxStatus.CanGetReward && boxData.RewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public void ResetDailyActiveList()
        {
            _baseDailyTaskList.Clear();
        }

        public void ResetActiveBoxList()
        {
            _dailyActiveBoxList.Clear();
            InitDailyActiveBox();
        }

        public void UpdateDailyActiveList(Dictionary<int, PbAwardTaskInfo> awardTaskDict)
        {
            foreach (var item in awardTaskDict)
            {
                int id = item.Key;
                PbAwardTaskInfo awardTaskInfo = item.Value;
                DailyActiveItemData itemData = AddDailyActiveItem(id);
                itemData.UpdateInfo(awardTaskInfo);
            }
            _baseDailyTaskList.Sort(SortDailyTaskList);
            UpdateBoxListRewardLvInfos();
        }

        public void UpdateDailyActiveListByStatus(Dictionary<int, int> taskList)
        {
            foreach (var item in taskList)
            {
                int id = item.Key;
                int status = item.Value;
                int curTriggerNum = 0;
                DailyActiveItemData itemData = AddDailyActiveItem(id);
                if (itemData.Status != (int)DailyActiveStatus.Complete)
                {
                    if (status == (int)DailyActiveStatus.Accept)
                    {
                        curTriggerNum = 0;
                    }
                    else if (status == (int)DailyActiveStatus.CanCommit || status == (int)DailyActiveStatus.Complete)
                    {
                        curTriggerNum = itemData.MaxTriggerNum;
                    }
                    itemData.UpdateInfo(status, curTriggerNum);
                }
            }
            _baseDailyTaskList.Sort(SortDailyTaskList);
        }

        public void UpdateDailyActiveListByIds(List<int> taskIdList)
        {
            List<BaseDailyTaskItemData> addTaskList = null;
            base.UpdateDailyListByIds(taskIdList, out addTaskList);
            UpdateBoxListRewardLvInfos();
        }

        public void UpdateDailyActiveProgressInfo(PbAwardActiveTaskProgressInfo progressInfo)
        {
            for (int i = 0; i < _baseDailyTaskList.Count; i++)
            {
                if (_baseDailyTaskList[i].Id == progressInfo.task_id)
                {
                    DailyActiveItemData itemData = _baseDailyTaskList[i] as DailyActiveItemData;
                    int triggerNum = 0;
                    for (int j = 0; j < progressInfo.trigger_info.Count; j++)
                    {
                        triggerNum += progressInfo.trigger_info[j].trigger_num;
                    }
                    itemData.UpdateInfo(triggerNum);
                    break;
                }
            }
        }

        public void UpdateDailyActiveReturnStateInfo(PbTaskReturnStateInfo pbReturnStateInfo)
        {
            bool isNeedResort = false;
            for (int i = 0; i < _baseDailyTaskList.Count; i++)
            {
                if (_baseDailyTaskList[i].Id == pbReturnStateInfo.task_id)
                {
                    DailyActiveItemData itemData = _baseDailyTaskList[i] as DailyActiveItemData;
                    int triggerNum = 0;
                    for (int j = 0; j < pbReturnStateInfo.trigger_info.Count; j++)
                    {
                        triggerNum += pbReturnStateInfo.trigger_info[j].trigger_num;
                    }
                    isNeedResort = itemData.UpdateReturnStateInfo(triggerNum);
                    break;
                }
            }
            if (isNeedResort)
            {
                _baseDailyTaskList.Sort(SortDailyTaskList);
            }
        }

        public void UpdateActiveBoxList(List<PbAwardActiveTaskBoxInfo> activeBoxInfoList)
        {
            //activeBoxInfoList是已经领取的box常规奖励的数据
            for (int i = 0; i < activeBoxInfoList.Count; i++)
            {
                PbAwardActiveTaskBoxInfo boxInfo = activeBoxInfoList[i];
                int rewardFlag = reward_system_helper.NOT_HAVE_REWARD_FLAG;
                _dailyActiveBoxList[i].UpdateInfo((int)boxInfo.box_reward_level, rewardFlag, (int)boxInfo.compensate_vip_reward_flag);
            }
            for (int i = activeBoxInfoList.Count; i < _dailyActiveBoxList.Count; i++)
            {
                _dailyActiveBoxList[i].UpdateRewardLvInfos();
            }
            UpdateCurActivePoint();
        }

        public void UpdateBoxListRewardLvInfos()
        {
            for (int i = 0; i < _dailyActiveBoxList.Count; i++)
            {
                _dailyActiveBoxList[i].UpdateRewardLvInfos();
            }
        }

        public void UpdateGetBoxReward(int boxId)
        {
            for (int i = 0; i < _dailyActiveBoxList.Count; i++)
            {
                if (_dailyActiveBoxList[i].Id == boxId)
                {
                    int rewardFlag = reward_system_helper.NOT_HAVE_REWARD_FLAG;
                    int curVip = PlayerAvatar.Player.vip_level;
                    //如果vip满足条件，则在领取常规奖励时也领取了vip奖励
                    int vipRewardFlag = curVip >= _dailyActiveBoxList[i].VipNeed ? reward_system_helper.NOT_HAVE_REWARD_FLAG : reward_system_helper.HAVE_REWARD_FLAG;
                    _dailyActiveBoxList[i].UpdateInfo(rewardFlag, vipRewardFlag);
                    break;
                }
            }
        }

        public void UpdateGetBoxVipReward(int boxId)
        {
            for (int i = 0; i < _dailyActiveBoxList.Count; i++)
            {
                if (_dailyActiveBoxList[i].Id == boxId)
                {
                    int rewardFlag = reward_system_helper.NOT_HAVE_REWARD_FLAG;
                    //如果vip满足条件，则在领取常规奖励时也领取了vip奖励
                    int vipRewardFlag = reward_system_helper.NOT_HAVE_REWARD_FLAG;
                    _dailyActiveBoxList[i].UpdateInfo(rewardFlag, vipRewardFlag);
                    break;
                }
            }
        }

        public void UpdateCurActivePoint()
        {
            _curActivePoint = (int)PlayerAvatar.Player.daily_active_point;
            for (int i = 0;i < _dailyActiveBoxList.Count;i++)
            {
                _dailyActiveBoxList[i].UpdateStatus(_curActivePoint);
            }
        }

        public bool IsCanGetBoxVipReward()
        {
            bool result = false;
            //判断是否可领取奖励，再看是否奖励已经领取
            for (int i = 0; i < _dailyActiveBoxList.Count; i++)
            {
                DailyActiveBoxData boxData = _dailyActiveBoxList[i];
                if (boxData.Status == (int)ActiveBoxStatus.CanGetReward)
                {
                    if (boxData.VipRewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                    {
                        int curVip = PlayerAvatar.Player.vip_level;
                        if (boxData.RewardFlag == reward_system_helper.NOT_HAVE_REWARD_FLAG && curVip >= boxData.VipNeed)
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        private DailyActiveItemData AddDailyActiveItem(int id)
        {
            return AddDailyTaskItem(id) as DailyActiveItemData;
        }

        protected override int SortDailyTaskList(BaseDailyTaskItemData a, BaseDailyTaskItemData b)
        {
            int aOrder = GetSortOrder(a as DailyActiveItemData);
            int bOrder = GetSortOrder(b as DailyActiveItemData);
            return bOrder - aOrder;
        }

        private int GetSortOrder(DailyActiveItemData itemData)
        {
            int order = 0;
            if (itemData.Status == (int)DailyActiveStatus.CanCommit)
            {
                order = 3000000;
            }
            else if (itemData.Status == (int)DailyActiveStatus.Accept)
            {
                order = 2000000;
            }
            else if (itemData.Status == (int)DailyActiveStatus.Complete)
            {
                order = 1000000;
            }

            order -= itemData.Id;
            return order;
        }

        private int SortBoxList(DailyActiveBoxData a, DailyActiveBoxData b)
        {
            return a.Point - b.Point;
        }
    }
}
