﻿using System;
using System.Collections.Generic;
using GameData;
using GameMain.Entities;

namespace Common.Data
{
    public class VipGiftItemData
    {
        private vip _vipInfo;
        private int _vipLv = 0;
        private List<RewardData> _rewardList;
        private int _rewardFlag = reward_system_helper.HAVE_REWARD_FLAG;

        public VipGiftItemData(int id)
        {
            this.Id = id;
            vip vipInfo = vip_helper.GetVipData(id);
            _vipLv = vipInfo.__vip_level;
            _vipInfo = vipInfo;
            InitRewardList(vipInfo);
        }

        private void InitRewardList(vip vipInfo)
        {
            _rewardList = new List<RewardData>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(vipInfo.__vip_rewards);
            foreach (var item in dataDict)
            {
                int id = int.Parse(item.Key);
                int num = int.Parse(item.Value);
                RewardData rewardData = RewardData.GetRewardData(id, num);
                _rewardList.Add(rewardData);
            }
        }

        public void UpdateGetReward()
        {
            _rewardFlag = reward_system_helper.NOT_HAVE_REWARD_FLAG;
        }

        public void UpdateInfo(int rewardFlag)
        {
            _rewardFlag = rewardFlag;
        }

        public bool IsReachVipNeed()
        {
            bool result = false;
            int curVip = PlayerAvatar.Player.vip_level;
            if (curVip >= _vipLv)
            {
                result = true;
            }
            return result;
        }

        public uint GetReachVipNeedCost()
        {
            uint diamond = 0;
            string strCost = vip_helper.GetVipLevelMinCost(_vipLv);
            uint cost = uint.Parse(strCost);
            diamond = cost - PlayerAvatar.Player.accumulated_charge;
            return diamond;
        }

        public int Id
        {
            get;
            protected set;
        }

        public int VipLv
        {
            get { return _vipLv; }
        }

        public List<RewardData> RewardList
        {
            get { return _rewardList; }
        }

        public int RewardFlag
        {
            get { return _rewardFlag; }
        }
    }
}
