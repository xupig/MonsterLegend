﻿using System;
using System.Collections.Generic;
using Common.Events;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;

namespace Common.Data
{
    public class OpenServerLoginRewardData
    {
        private List<OpenServerLoginItemData> _loginRewardInfoList = new List<OpenServerLoginItemData>();
        private bool _isOpenChanged = false;
        private bool _isAllNeedTaskCompleted = false;

        public void InitLoginRewardInfoList()
        {
            if (_loginRewardInfoList.Count > 0)
                return;
            foreach (KeyValuePair<int, open_server_login_reward>  item in XMLManager.open_server_login_reward)
            {
                open_server_login_reward config = item.Value;
                if (!reward_system_helper.IsAddServerOpenedLoginRewardInfo(config))
                    continue;
                OpenServerLoginItemData itemData = new OpenServerLoginItemData(config.__id);
                _loginRewardInfoList.Add(itemData);
            }

            //更新是否已领取
            UpdateLoginRewardListStatus();
        }

        public void UpdateLoginRewardListStatus()
        {
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            for (int i = 0; i < _loginRewardInfoList.Count; i++)
            {
                int taskId = _loginRewardInfoList[i].OpenServerLoginConfig.__task_id;
                if (taskData.IsCompleted(taskId))
                {
                    _loginRewardInfoList[i].UpdateGetReward();
                }
            }
        }

        public void UpdateReceiveRewardStatus(int taskId)
        {
            for (int i = 0; i < _loginRewardInfoList.Count; i++)
            {
                if (_loginRewardInfoList[i].OpenServerLoginConfig.__task_id == taskId)
                {
                    _loginRewardInfoList[i].UpdateGetReward();
                    EventDispatcher.TriggerEvent<OpenServerLoginItemData, int>(RewardEvents.GET_SERVER_OPENED_LOGIN_REWARD, _loginRewardInfoList[i], i);
                    break;
                }
            }
        }

        public bool CheckIsOpenFunction()
        {
            bool isOpen = false;
            int hasCompletedCount = 0;
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            foreach (KeyValuePair<int, open_server_login_reward> item in XMLManager.open_server_login_reward)
            {
                open_server_login_reward config = item.Value;
                int taskId = config.__task_id;
                //如果接受的列表中或完成列表中或已领取奖励的列表中存在该任务，则开启
                //如果这些任务都已领取奖励了，也不开启
                if (taskData.IsTaskAccepted(taskId))
                {
                    isOpen = true;
                    break;
                }
                else if (taskData.IsTaskFinished(taskId))
                {
                    isOpen = true;
                    break;
                }
                else if (taskData.IsCompleted(taskId))
                {
                    hasCompletedCount++;
                }
            }
            if (!isOpen && hasCompletedCount > 0 && hasCompletedCount < XMLManager.open_server_login_reward.Count)
            {
                isOpen = true;
            }
            return isOpen;
        }

        public List<OpenServerLoginItemData> LoginRewardInfoList
        {
            get { return _loginRewardInfoList; }
        }

        public bool IsOpenChanged
        {
            get { return _isOpenChanged; }
            set { _isOpenChanged = value; }
        }

        public bool IsAllNeedTaskCompleted
        {
            get { return _isAllNeedTaskCompleted; }
            set { _isAllNeedTaskCompleted = value; }
        }
    }

    public class OpenServerLoginItemData
    {
        private open_server_login_reward _config;
        private bool _isReceived = false;
        private List<RewardData> _rewardList;

        public int Id
        {
            get;
            protected set;
        }

        public OpenServerLoginItemData(int id)
        {
            Id = id;
            _config = reward_system_helper.GetOpenServerLoginRewardConfig(id);
            if (_config != null)
            {
                task_data taskConfig = task_data_helper.GetTaskData(_config.__task_id);
                _rewardList = item_reward_helper.GetRewardDataList(taskConfig.__reward, PlayerAvatar.Player.vocation);
            }
        }

        public void UpdateGetReward()
        {
            _isReceived = true;
        }

        public bool IsReceived
        {
            get { return _isReceived; }
        }

        public open_server_login_reward OpenServerLoginConfig
        {
            get { return _config; }
        }

        public List<RewardData> RewardList
        {
            get
            {
                if (_rewardList == null)
                {
                    _rewardList = new List<RewardData>();
                }
                return _rewardList;
            }
        }
    }
}
