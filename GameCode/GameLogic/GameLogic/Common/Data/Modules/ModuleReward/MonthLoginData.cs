﻿using System;
using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameLoader.Utils;

namespace Common.Data
{
    public class MonthLoginData
    {
        private int _signDayNum = 0;
        private int _signRewardTag = 0;
        private int _vipSignRewardTag = 0;
        private int _year = 0;
        private int _month = 0;

        List<MonthLoginItemData> _monthLoginList = new List<MonthLoginItemData>();

        public const int SIGN_TAG_HAVE_RECEIVED = 0;  //表示已领取过奖励，大于该值则未领取

        public const int VIP_SIGN_TAG_NOT_REACH_VIP_NEED = -1;  //有vip，没达到vip等级
        public const int VIP_SIGN_TAG_NOT_HAVE_REWARD = 0;  //没有vip奖励

        public List<MonthLoginItemData> MonthLoginList
        {
            get { return _monthLoginList; }
        }

        public int SignDayNum
        {
            get { return _signDayNum; }
        }

        public int SignRewardTag
        {
            get { return _signRewardTag; }
        }

        public int VipSignRewardTag
        {
            get { return _vipSignRewardTag; }
        }

        public int Month
        {
            get { return _month; }
        }

        public void InitMonthLoginList()
        {
            int dayNum = TotalDayNum;
            DateTime curDateTime = reward_system_helper.GetCurDateTime();
            _year = curDateTime.Year;
            _month = curDateTime.Month;
            int curPlayerLv = PlayerAvatar.Player.level;
            for (int i = 0; i < dayNum; i++)
            {
                month_login_reward monthInfo = reward_system_helper.GetMonthLoginInfo(curDateTime, i + 1, curPlayerLv);
                if (monthInfo != null)
                {
                    MonthLoginItemData itemData = new MonthLoginItemData(i + 1, monthInfo);
                    _monthLoginList.Add(itemData);
                }
            }
            _monthLoginList.Sort(SortList);
            _signDayNum = 1;
        }

        private void PrintMonthList()
        {
            for (int i = 0; i < _monthLoginList.Count; i++)
            {
                MonthLoginItemData itemData = _monthLoginList[i];
                UnityEngine.Debug.LogError(" day = " + itemData.Day + " status = " + itemData.Status + " vip need = " + itemData.VipNeed);
            }
        }

        public bool IsShowTips()
        {
            bool result = false;
            if (_monthLoginList[_signDayNum - 1].Status == (int)MonthLoginStatus.UnReceive)
            {
                result = true;
            }
            return result;
        }

        public void UpdateMonthLoginSign(List<PbAwardMonthLoginInfo> monthLoginInfos)
        {
            ResetOnDifferentMonthOrYear();
            for (int i = 0; i < monthLoginInfos.Count; i++)
            {
                MonthLoginItemData itemData = _monthLoginList[(int)monthLoginInfos[i].login_days - 1];
                if (monthLoginInfos[i].reward_flag > SIGN_TAG_HAVE_RECEIVED)
                {
                    itemData.Status = (int)MonthLoginStatus.UnReceive;
                }
                else
                {
                    itemData.Status = (int)MonthLoginStatus.HaveReceived;
                }
                ProcessSignVipRewardTag((int)monthLoginInfos[i].login_days);
            }

            //当前签到的是哪一天为记录中最大的那一天
            int curSignDay = 0;
            for (int i = 0; i < monthLoginInfos.Count; i++)
            {
                if (curSignDay < monthLoginInfos[i].login_days)
                {
                    curSignDay = (int)monthLoginInfos[i].login_days;
                }
            }
            _signDayNum = curSignDay;
        }

        private void ResetOnDifferentMonthOrYear()
        {
            DateTime curDateTime = reward_system_helper.GetCurDateTime();
            if (curDateTime.Month != _month || curDateTime.Year != _year)
            {
                _monthLoginList.Clear();
                InitMonthLoginList();
            }
        }

        public void GetSignReward(int day)
        {
            if (_signDayNum != day)
            {
                LoggerHelper.Error("client sign day num is not same as server's data!!!");
            }
            _signDayNum = day;
            for (int i = 0; i < day; i++)
            {
                MonthLoginItemData itemData = _monthLoginList[i];
                if (itemData.Status == (int)MonthLoginStatus.UnReceive)
                {
                    itemData.Status = (int)MonthLoginStatus.HaveReceived;
                    ProcessSignVipRewardTag(i + 1);
                }
            }
        }

        public void ProcessSignVipRewardTag(int day)
        {
            MonthLoginItemData itemData = GetLoginItemData(day);
            if (itemData != null)
            {
                int vipSignRewardTag = itemData.VipRewardTag;
                if (itemData.VipNeed > 0 && PlayerAvatar.Player.vip_level >= itemData.VipNeed)
                {
                    if (itemData.Status == (int)MonthLoginStatus.HaveReceived)
                    {
                        vipSignRewardTag = MonthLoginData.VIP_SIGN_TAG_NOT_HAVE_REWARD;
                    }
                    else
                    {
                        vipSignRewardTag = itemData.RewardId;
                    }
                }
                else if (itemData.VipNeed > 0 && PlayerAvatar.Player.vip_level < itemData.VipNeed)
                {
                    vipSignRewardTag = MonthLoginData.VIP_SIGN_TAG_NOT_REACH_VIP_NEED;
                }
                else
                {
                    vipSignRewardTag = MonthLoginData.VIP_SIGN_TAG_NOT_HAVE_REWARD;
                }
                itemData.VipRewardTag = vipSignRewardTag;
            }
        }

        public MonthLoginItemData GetLoginItemData(int day)
        {
            MonthLoginItemData itemData = null;
            for (int i = 0; i < _monthLoginList.Count; i++)
            {
                if (_monthLoginList[i].Day == day)
                {
                    itemData = _monthLoginList[i];
                    break;
                }
            }
            return itemData;
        }

        public int TotalDayNum
        {
            get
            {
                DateTime currentDateTime = reward_system_helper.GetCurDateTime();
                if (currentDateTime.Month == 4 || currentDateTime.Month == 6 ||
                    currentDateTime.Month == 9 || currentDateTime.Month == 11)
                {
                    return 30;
                }
                else if (currentDateTime.Month == 1 || currentDateTime.Month == 3
                        || currentDateTime.Month == 5 || currentDateTime.Month == 7 ||
                        currentDateTime.Month == 8 || currentDateTime.Month == 10 ||
                            currentDateTime.Month == 12)
                {
                    return 31;
                }
                else
                {
                    if (currentDateTime.Year % 400 == 0 || currentDateTime.Year % 4 == 0 && currentDateTime.Year % 100 != 0)
                    {
                        return 29;
                    }
                    else
                        return 28;
                }
            }
        }

        private int SortList(MonthLoginItemData a, MonthLoginItemData b)
        {
            return a.Day - b.Day;
        }
    }
}
