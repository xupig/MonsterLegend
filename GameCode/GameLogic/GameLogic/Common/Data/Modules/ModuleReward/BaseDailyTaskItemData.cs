﻿using System;
using System.Collections.Generic;
using GameData;

namespace Common.Data
{
    public class BaseDailyTaskItemData
    {
        protected int _iconId = 0;
        protected string _name = string.Empty;
        protected int _eventId = 0;
        protected int _eventAtomId = 0;
        protected List<RewardData> _rewardList;

        public BaseDailyTaskItemData(int id)
        {
            this.Id = id;
        }

        protected int GetEventAtomId()
        {
            int atomId = 0;
            event_cnds eventInfo = event_cnds_helper.GetEvent(_eventId);
            if (eventInfo == null)
                return atomId;
            atomId = eventInfo.__atom_id;
            return atomId;
        }

        protected bool CheckIsFollowEvent()
        {
            Dictionary<string, string[]> evtFollow = event_cnds_helper.GetEventFollow(_eventId);
            return evtFollow.Count > 0;
        }

        public int Id
        {
            get;
            protected set;
        }

        public string Name
        {
            get { return _name; }
        }

        public int IconId
        {
            get { return _iconId; }
        }

        public int EventId
        {
            get { return _eventId; }
        }

        public int EvtAtomId
        {
            get { return _eventAtomId; }
        }

        public virtual List<RewardData> RewardList
        {
            get { return _rewardList; }
        }
    }
}
