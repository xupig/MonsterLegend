﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class BaseDailyTaskData
    {
        protected List<BaseDailyTaskItemData> _baseDailyTaskList = new List<BaseDailyTaskItemData>();

        protected virtual int BaseDailyTaskType
        {
            get { return (int)DailyTaskItemType.DailyTask; }
        }

        public virtual bool IsShowTips()
        {
            return false;
        }

        protected void UpdateDailyListByIds(List<int> taskIdList, out List<BaseDailyTaskItemData> addTaskList)
        {
            addTaskList = new List<BaseDailyTaskItemData>();
            for (int i = 0; i < taskIdList.Count; i++)
            {
                int id = taskIdList[i];
                BaseDailyTaskItemData itemData = null;
                for (int j = 0; j < _baseDailyTaskList.Count; j++)
                {
                    if (_baseDailyTaskList[j].Id == id)
                    {
                        itemData = _baseDailyTaskList[j];
                        break;
                    }
                }
                if (itemData == null)
                {
                    itemData = CreateDailyTaskItem(id, BaseDailyTaskType);
                    addTaskList.Add(itemData);
                }
            }

            for (int i = 0; i < addTaskList.Count; i++)
            {
                _baseDailyTaskList.Add(addTaskList[i]);
            }

            _baseDailyTaskList.Sort(SortDailyTaskList);
        }

        protected virtual int SortDailyTaskList(BaseDailyTaskItemData a, BaseDailyTaskItemData b)
        {
            return 1;
        }

        private BaseDailyTaskItemData CreateDailyTaskItem(int id, int itemType)
        {
            switch (itemType)
            {
                case (int)DailyTaskItemType.DailyTask:
                    return new DailyTaskItemData(id);
                case (int)DailyTaskItemType.DailyActiveTask:
                    return new DailyActiveItemData(id);
                default:
                    return new BaseDailyTaskItemData(id);
            }
        }

        protected BaseDailyTaskItemData AddDailyTaskItem(int id)
        {
            BaseDailyTaskItemData itemData = null;
            for (int i = 0; i < _baseDailyTaskList.Count; i++)
            {
                if (_baseDailyTaskList[i].Id == id)
                {
                    itemData = _baseDailyTaskList[i];
                    break;
                }
            }
            if (itemData == null)
            {
                itemData = CreateDailyTaskItem(id, BaseDailyTaskType);
                _baseDailyTaskList.Add(itemData);
            }
            return itemData;
        }

        protected void RemoveDailyTaskItem(int id)
        {
            BaseDailyTaskItemData removeData = null;
            for (int i = 0; i < _baseDailyTaskList.Count; i++)
            {
                if (_baseDailyTaskList[i].Id == id)
                {
                    removeData = _baseDailyTaskList[i];
                    break;
                }
            }
            _baseDailyTaskList.Remove(removeData);
        }
    }

    public enum DailyTaskItemType
    {
        DailyTask = 0,
        DailyActiveTask
    }
}
