﻿using System;
using System.Collections.Generic;
using Common.ClientConfig;
using GameMain.Entities;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using GameData;
using Common.Structs.ProtoBuf;

namespace Common.Data
{
    public class RewardDataManager
    {
        private DailyTaskData _dailyTaskData;
        private MonthLoginData _monthLoginData;
        private DailyActiveTaskData _dailyActiveTaskData;
        private VipGiftData _vipGiftData;
        private OpenServerLoginRewardData _openServerLoginRewardData;

        //比如完成状态的更新，如果此时是在主城界面，在主城界面OnShow时再检查是否显示奖励的提示特效就不可行了，
        //因此要在数据更新时，就更新tips是否显示
        private Dictionary<int, bool> _rewardTypeTipsDict = new Dictionary<int, bool>();
        private Dictionary<int, bool> _rewardTypeOpen = new Dictionary<int, bool>();

        public const int ITEM_ID_ACTIVE_POINT = 13;

        public RewardDataManager()
        {
            _dailyTaskData = new DailyTaskData();
            _monthLoginData = new MonthLoginData();
            _dailyActiveTaskData = new DailyActiveTaskData();
            _vipGiftData = new VipGiftData();
            _openServerLoginRewardData = new OpenServerLoginRewardData();
            _rewardTypeTipsDict.Add((int)RewardSystemType.DailyTask, false);
            _rewardTypeTipsDict.Add((int)RewardSystemType.DailyActiveTask, false);
            _rewardTypeTipsDict.Add((int)RewardSystemType.MonthLoginReward, false);
            _rewardTypeTipsDict.Add((int)RewardSystemType.VipGift, false);
            _rewardTypeOpen.Add((int)RewardSystemType.DailyTask, false);
            _rewardTypeOpen.Add((int)RewardSystemType.DailyActiveTask, false);
            _rewardTypeOpen.Add((int)RewardSystemType.MonthLoginReward, false);
            _rewardTypeOpen.Add((int)RewardSystemType.VipGift, false);
            _rewardTypeOpen.Add((int)RewardSystemType.ServerOpendLoginReward, false);
            _rewardTypeOpen.Add((int)RewardSystemType.RewardCompensation, false);
            AddEventListener();
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, OnUpdateRewardSystemData);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnUpdateDailyActiveData);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.daily_active_point, OnUpdateDailyActiveBox);
            EventDispatcher.AddEventListener<List<PbVIPRewardInfo>>(RewardEvents.GET_VIP_GIFT, OnUpdateRewardSysVipGiftData);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_DAILY_TASK_TIME_CHANGED, OnUpdateDailyTaskTimeChanged);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_CARD_INFO, OnUpdateDailyTaskTimeChanged);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_ACCEPTED, OnTaskAccepted);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_REACHED, OnTaskFinished);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskCompleted);
        }

        public DailyTaskData GetDailyTaskData()
        {
            return _dailyTaskData;
        }

        public MonthLoginData GetMonthLoginData()
        {
            return _monthLoginData;
        }

        public DailyActiveTaskData GetDailyActiveData()
        {
            return _dailyActiveTaskData;
        }

        public VipGiftData GetVipGiftData()
        {
            return _vipGiftData;
        }

        public OpenServerLoginRewardData GetOpenServerLoginData()
        {
            return _openServerLoginRewardData;
        }

        private bool isVipGiftShow = false;
        private void OnUpdateRewardSystemData()
        {
            if (PlayerAvatar.Player.vip_level > 0)
            {
                isVipGiftShow = true;
            }
            else
            {
                isVipGiftShow = false;
            }
            MonthLoginData monthLoginData = _monthLoginData;
            int signDayNum = monthLoginData.SignDayNum;
            monthLoginData.ProcessSignVipRewardTag(signDayNum);
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_MONTH_LOGIN);
            RewardManager.Instance.ProcessRewardPoint(RewardSystemType.MonthLoginReward);
            RewardManager.Instance.ProcessRewardPoint(RewardSystemType.VipGift);
        }

        private void OnUpdateDailyActiveData()
        {
            //在等级变更时更新活跃宝箱奖励相关信息
            DailyActiveTaskData dailyActiveData = _dailyActiveTaskData;
            dailyActiveData.UpdateBoxListRewardLvInfos();
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_BOX_REWARD_LV_INFOS);
        }

        private void OnUpdateDailyActiveBox()
        {
            _dailyActiveTaskData.UpdateCurActivePoint();
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_BOX_INFO);
        }

        private void OnUpdateRewardSysVipGiftData(List<PbVIPRewardInfo> vipRewardInfoList)
        {
            VipGiftData vipGiftData = _vipGiftData;
            for (int j = 0; j < vipRewardInfoList.Count; j++)
            {
                int vipLv = (int)vipRewardInfoList[j].vip_level;
                for (int i = 0; i < vipGiftData.VipGiftList.Count; i++)
                {
                    VipGiftItemData itemData = vipGiftData.VipGiftList[i];
                    if (itemData.VipLv == vipLv)
                    {
                        itemData.UpdateGetReward();
                        break;
                    }
                }
            }
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_VIP_GIFT);
            RewardManager.Instance.ProcessRewardPoint(RewardSystemType.VipGift);
        }

        private void OnUpdateDailyTaskTimeChanged()
        {
            _dailyTaskData.SortDailyTaskTimeStatusChanged();
            ProcessRewardTipsPoint(RewardSystemType.DailyTask);
        }

        private void OnTaskAccepted(int taskId)
        {
            UpdateOpenServerLoginRewardData(taskId, false);
        }

        private void OnTaskFinished(int taskId)
        {
            UpdateOpenServerLoginRewardData(taskId, false);
        }

        private void OnTaskCompleted(int taskId)
        {
            UpdateOpenServerLoginRewardData(taskId, true);
        }

        public void UpdateOpenServerLoginRewardData(int taskId, bool isReceivedReward)
        {
            if (!reward_system_helper.IsOpenServerLoginConfigContainsTask(taskId))
                return;
            bool isOpen = _openServerLoginRewardData.CheckIsOpenFunction();
            UpdateServerOpenedLoginOpenStatus(isOpen);
            if (isOpen || _openServerLoginRewardData.IsAllNeedTaskCompleted)
            {
                _openServerLoginRewardData.InitLoginRewardInfoList();
                if (isReceivedReward)
                {
                    _openServerLoginRewardData.UpdateReceiveRewardStatus(taskId);
                }
                _openServerLoginRewardData.IsAllNeedTaskCompleted = false;
            }
        }

        public void UpdateOpenServerLoginRewardData()
        {
            bool isOpen = _openServerLoginRewardData.CheckIsOpenFunction();
            UpdateServerOpenedLoginOpenStatus(isOpen);
            if (isOpen || _openServerLoginRewardData.IsAllNeedTaskCompleted)
            {
                int count = _openServerLoginRewardData.LoginRewardInfoList.Count;
                _openServerLoginRewardData.InitLoginRewardInfoList();
                if (count > 0)
                {
                    _openServerLoginRewardData.UpdateLoginRewardListStatus();
                }
                _openServerLoginRewardData.IsAllNeedTaskCompleted = false;
            }
        }

        private void UpdateServerOpenedLoginOpenStatus(bool isOpen)
        {
            if (_rewardTypeOpen[(int)RewardSystemType.ServerOpendLoginReward] != isOpen)
            {
                _openServerLoginRewardData.IsOpenChanged = true;
                //当开服登录功能从开启变为关闭时，证明里面所包含的任务都已完成
                if (_rewardTypeOpen[(int)RewardSystemType.ServerOpendLoginReward] == true && isOpen == false)
                {
                    _openServerLoginRewardData.IsAllNeedTaskCompleted = true;
                }
                _rewardTypeOpen[(int)RewardSystemType.ServerOpendLoginReward] = isOpen;
            }
        }

        public void ProcessRewardTipsPoint(RewardSystemType type)
        {
            UpdateRewardTypeTips(type);
            if (IsShowRewardSystemPoint())
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.reward);
            }
            if (IsShowRewardSystemPoint() == false 
                && PlayerDataManager.Instance.AchievementData.HasCompletedAchievement() == false
                && PlayerDataManager.Instance.playerReturnData.HasRewardToGet() == false)
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.reward);
            }
        }

        public void UpdateRewardTypeTips(RewardSystemType type)
        {
            switch (type)
            {
                case RewardSystemType.DailyTask:
                    _rewardTypeTipsDict[(int)type] = (IsDailyTaskOpen() && _dailyTaskData.IsShowTips());
                    break;
                case RewardSystemType.DailyActiveTask:
                    _rewardTypeTipsDict[(int)type] = (IsDailyActiveOpen() && _dailyActiveTaskData.IsShowTips());
                    break;
                case RewardSystemType.MonthLoginReward:
                    _rewardTypeTipsDict[(int)type] = (IsMonthLoginRewardOpen() && _monthLoginData.IsShowTips());
                    break;
                case RewardSystemType.VipGift:
                    _rewardTypeTipsDict[(int)type] = (IsVipGiftOpen() && _vipGiftData.IsShowTips());
                    break;
                default:
                    break;
            }
        }

        public bool IsShowRewardSystemPoint()
        {
            bool isShow = false;
            foreach (var item in _rewardTypeTipsDict)
            {
                if (item.Value)
                {
                    isShow = true;
                    break;
                }
            }
            return isShow;
        }

        public bool IsRewardCompensationOpen()
        {
            bool isFunctionOpen = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.rewardcompensation);
            List<BaseItemData> itemDataList = avatar_level_helper.GetUpLevelCompensationReward(PlayerAvatar.Player.level);
            return isFunctionOpen && itemDataList.Count != 0 && (PlayerAvatar.Player.serverLevel - PlayerAvatar.Player.level >= global_params_helper.GetRewardCompensationLevelInterval());
        }

        public bool IsDailyTaskOpen()
        {
            return function_helper.IsFunctionOpen((int)FunctionId.dailytask);
        }

        public bool IsDailyActiveOpen()
        {
            return function_helper.IsFunctionOpen((int)FunctionId.dailyactive);
        }

        public bool IsMonthLoginRewardOpen()
        {
            return function_helper.IsFunctionOpen((int)FunctionId.monthloginreward);
        }

        public bool IsVipGiftOpen()
        {
            bool isOpen = function_helper.IsFunctionOpen((int)FunctionId.vipgift);
            if (!isVipGiftShow && PlayerAvatar.Player.vip_level > 0)
            {
                isVipGiftShow = true;
            }
            return (isOpen && isVipGiftShow);
        }

        public bool IsCategoryListChange()
        {
            bool isChange = false;
            bool isOpen = false;
            if (!_rewardTypeOpen[(int)RewardSystemType.DailyTask])
            {
                isOpen = IsDailyTaskOpen();
                if (_rewardTypeOpen[(int)RewardSystemType.DailyTask] != isOpen)
                {
                    _rewardTypeOpen[(int)RewardSystemType.DailyTask] = isOpen;
                    isChange = true;
                }
            }

            if (!_rewardTypeOpen[(int)RewardSystemType.DailyActiveTask])
            {
                isOpen = IsDailyActiveOpen();
                if (_rewardTypeOpen[(int)RewardSystemType.DailyActiveTask] != isOpen)
                {
                    _rewardTypeOpen[(int)RewardSystemType.DailyActiveTask] = isOpen;
                    isChange = true;
                }
            }

            if (!_rewardTypeOpen[(int)RewardSystemType.MonthLoginReward])
            {
                isOpen = IsMonthLoginRewardOpen();
                if (_rewardTypeOpen[(int)RewardSystemType.MonthLoginReward] != isOpen)
                {
                    _rewardTypeOpen[(int)RewardSystemType.MonthLoginReward] = isOpen;
                    isChange = true;
                }
            }

            isOpen = IsRewardCompensationOpen();
            if (_rewardTypeOpen[(int)RewardSystemType.RewardCompensation] != isOpen)
            {
                _rewardTypeOpen[(int)RewardSystemType.RewardCompensation] = isOpen;
                isChange = true;
            }

            if (!_rewardTypeOpen[(int)RewardSystemType.VipGift] || !isVipGiftShow)
            {
                isOpen = IsVipGiftOpen();
                if (_rewardTypeOpen[(int)RewardSystemType.VipGift] != isOpen)
                {
                    _rewardTypeOpen[(int)RewardSystemType.VipGift] = isOpen;
                    isChange = true;
                }
            }

            if (!isChange)
            {
                isChange = _openServerLoginRewardData.IsOpenChanged;
                if (_openServerLoginRewardData.IsOpenChanged)
                {
                    _openServerLoginRewardData.IsOpenChanged = false;
                }
            }
            
            return isChange;
        }

        public bool IsRewardTypeOpen(RewardSystemType type)
        {
            return _rewardTypeOpen[(int)type];
        }
    }

    public enum RewardSystemType
    {
        DailyTask = 1,
        DailyActiveTask,
        MonthLoginReward,
        VipGift,
        ServerOpendLoginReward,
        RewardCompensation,
    }
}
