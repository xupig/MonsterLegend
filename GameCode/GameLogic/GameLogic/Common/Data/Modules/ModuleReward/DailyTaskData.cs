﻿using System;
using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.Entities;
using UnityEngine;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;

namespace Common.Data
{
    public class DailyTaskData : BaseDailyTaskData
    {
        protected override int BaseDailyTaskType
        {
            get { return (int)DailyTaskItemType.DailyTask; }
        }

        private List<DailyTaskItemData> _dailyTaskCombineList = new List<DailyTaskItemData>();
        private DailyTaskCardInfo _taskCardInfo;

        private uint _timerId = 0;
        private int _curTime = 0;

        public DailyTaskData()
        {
            _taskCardInfo = new DailyTaskCardInfo();
            ResetTaskCardInfo();
        }

        public void OnReloadData()
        {
            ResetAcceptDailyTaskList();
        }

        public List<BaseDailyTaskItemData> DailyTaskList
        {
            get { return _baseDailyTaskList; }
        }

        public List<DailyTaskItemData> DailyTaskCombineList
        {
            get { return _dailyTaskCombineList; }
        }

        public DailyTaskCardInfo TaskCardInfo
        {
            get { return _taskCardInfo; }
        }

        public override bool IsShowTips()
        {
            bool result = false;
            for (int i = 0; i < _dailyTaskCombineList.Count; i++)
            {
                DailyTaskItemData itemData = _dailyTaskCombineList[i];
                if (itemData.Status == (int)DailyTaskStatus.CanCommit && itemData.TimeStatus == (int)DailyTaskTimeStatus.Valid)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public int ProcessTaskStatus(PbAwardTaskInfo awardTaskInfo, DailyTaskItemData itemData)
        {
            int status = (int)DailyTaskStatus.Accept;
            if (awardTaskInfo.is_complete == public_config.AWARD_ACTIVE_TASK_STATE_COMPLETED)
            {
                if (awardTaskInfo.reward_flag == reward_system_helper.NOT_HAVE_REWARD_FLAG)
                {
                    status = (int)DailyTaskStatus.Complete;
                }
                else
                {
                    status = (int)DailyTaskStatus.CanCommit;
                }
            }
            else
            {
                status = (int)DailyTaskStatus.Accept;
            }
            return status;
        }

        public void UpdateDailyTaskList(Dictionary<int, PbAwardTaskInfo> awardTaskDict)
        {
            foreach (var item in awardTaskDict)
            {
                int id = item.Key;
                DailyTaskItemData itemData = AddDailyTaskItem(id) as DailyTaskItemData;
                int status = ProcessTaskStatus(item.Value, itemData);
                itemData.Status = status;
            }

            _baseDailyTaskList.Sort(SortDailyTaskList);
            CombineDailyTaskList();
            UpdateTaskListTypeNumInfos();
        }

        public void UpdateDailyTaskList(Dictionary<int, int> taskDict)
        {
            foreach (var item in taskDict)
            {
                int id = item.Key;
                int status = item.Value;
                DailyTaskItemData itemData = null;
                itemData = AddDailyTaskItem(id) as DailyTaskItemData;
                if (itemData.Status != (int)DailyTaskStatus.Complete)
                {
                    itemData.Status = status;
                }

                if (itemData.CheckIsHasTimeLimit())
                {
                    itemData.StartTimeCountDown();
                }
            }

            _baseDailyTaskList.Sort(SortDailyTaskList);
            CombineDailyTaskList();
        }

        public void UpdateDailyTaskListByIds(List<int> taskIdList)
        {
            List<BaseDailyTaskItemData> addTaskList = null;
            base.UpdateDailyListByIds(taskIdList, out addTaskList);
            CombineDailyTaskList();
            UpdateTaskListTypeNumInfos();
        }

        public void SortDailyTaskTimeStatusChanged()
        {
            _baseDailyTaskList.Sort(SortDailyTaskList);
            CombineDailyTaskList();
            _dailyTaskCombineList.Sort(SortDailyTaskList);
        }

        public void CombineDailyTaskList()
        {
            //保存不同类型的任务，当类型相同时，如果任务状态为已接或可提交，则比较id，取id值最小的
            Dictionary<int, int> typeIdDict = new Dictionary<int, int>();
            _dailyTaskCombineList.Clear();
            for (int i = 0; i < DailyTaskList.Count; i++)
            {
                DailyTaskItemData itemData = DailyTaskList[i] as DailyTaskItemData;
                if (!typeIdDict.ContainsKey(itemData.TaskType))
                {
                    typeIdDict.Add(itemData.TaskType, itemData.Id);
                }
                else
                {
                    if (itemData.Status == (int)DailyTaskStatus.Accept || itemData.Status == (int)DailyTaskStatus.CanCommit)
                    {
                        int id = typeIdDict[itemData.TaskType];
                        if (itemData.Id < id)
                        {
                            typeIdDict[itemData.TaskType] = itemData.Id;
                        }
                    }
                }
            }

            for (int i = 0; i < DailyTaskList.Count; i++)
            {
                DailyTaskItemData itemData = DailyTaskList[i] as DailyTaskItemData;
                if (itemData.TaskType == (int)DailyTaskType.MonthCard && TaskCardInfo.leftSeconds <= 0)
                {
                    continue;
                }
                if (itemData.Status == (int)DailyTaskStatus.Accept || itemData.Status == (int)DailyTaskStatus.CanCommit)
                {
                    if (typeIdDict.ContainsKey(itemData.TaskType) && typeIdDict[itemData.TaskType] == itemData.Id)
                    {
                        _dailyTaskCombineList.Add(itemData);
                    }
                }
                else
                {
                    _dailyTaskCombineList.Add(itemData);
                }
            }
        }

        private void UpdateTaskListTypeNumInfos()
        {
            Dictionary<int, List<int>> typeIdsDict = new Dictionary<int, List<int>>();
            for (int i = 0; i < _baseDailyTaskList.Count; i++)
            {
                DailyTaskItemData itemData = _baseDailyTaskList[i] as DailyTaskItemData;
                List<int> idList = null;
                if (!typeIdsDict.ContainsKey(itemData.TaskType))
                {
                    idList = new List<int>();
                    typeIdsDict.Add(itemData.TaskType, idList);
                }
                else
                {
                    idList = typeIdsDict[itemData.TaskType];
                }
                idList.Add(itemData.Id);
            }

            foreach (var item in typeIdsDict)
            {
                List<int> idList = item.Value;
                if (idList.Count >= 2)
                {
                    idList.Sort((a, b) => { return a - b; });
                    for (int i = 0; i < idList.Count; i++)
                    {
                        DailyTaskItemData itemData = _baseDailyTaskList.Find((dailyItemData) => { return dailyItemData.Id == idList[i]; }) as DailyTaskItemData;
                        itemData.UpdateTypeNumInfo(i + 1, idList.Count);
                    }
                }
            }
        }

        public void UpdateCardInfo(PbCardInfo cardInfo)
        {
            if (cardInfo.card_finish_time == 0 || cardInfo.card_finish_time <= Common.Global.Global.serverTimeStamp / 1000)
            {
                ResetTaskCardInfo();
                RemoveCardTimer();
                return;
            }
            _taskCardInfo.cardType = (int)cardInfo.card_type;
            _taskCardInfo.finishTime = (ulong)cardInfo.card_finish_time;
            UpdateCardLeftTime();
            if (_timerId == 0)
            {
                _timerId = TimerHeap.AddTimer(0, 1000, UpdateCardLeftTime);
            }
        }

        private void UpdateCardLeftTime()
        {
            if (_taskCardInfo.finishTime == 0)
            {
                if (_taskCardInfo.cardType != 0)
                {
                    ResetTaskCardInfo();
                }
                RemoveCardTimer();
                EventDispatcher.TriggerEvent(RewardEvents.UPDATE_CARD_INFO);
                return;
            }
            if (_taskCardInfo.finishTime <= Common.Global.Global.serverTimeStamp / 1000)
            {
                if (_taskCardInfo.cardType != 0)
                {
                    ResetTaskCardInfo();
                }
                RemoveCardTimer();
                EventDispatcher.TriggerEvent(RewardEvents.UPDATE_CARD_INFO);
                return;
            }
            _taskCardInfo.leftSeconds = (uint)(_taskCardInfo.finishTime - Common.Global.Global.serverTimeStamp / 1000);
            //根据剩余时间判断什么时间会派发事件
            _curTime += 1;
            if (_taskCardInfo.leftSeconds >= 24 * 3600 || _taskCardInfo.leftSeconds >= 3600)
            {
                if (_curTime > 60 * 60) //一个小时
                {
                    _curTime = 0;
                    EventDispatcher.TriggerEvent(RewardEvents.UPDATE_CARD_INFO);
                }
            }
            else
            {
                if (_curTime > 60)
                {
                    _curTime = 0;
                    EventDispatcher.TriggerEvent(RewardEvents.UPDATE_CARD_INFO);
                }
            }
        }

        private void ResetTaskCardInfo()
        {
            _taskCardInfo.cardType = 0;
            _taskCardInfo.finishTime = 0;
            _taskCardInfo.leftSeconds = 0;
        }

        private void RemoveCardTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
            _curTime = 0;
        }

        public void ResetAcceptDailyTaskList()
        {
            _baseDailyTaskList.Clear();
            _dailyTaskCombineList.Clear();
        }

        public bool IsMonthCardValid()
        {
            return _taskCardInfo.finishTime != 0 && _taskCardInfo.cardType == public_config.CARD_TYPE_MONTH;
        }

        protected override int SortDailyTaskList(BaseDailyTaskItemData a, BaseDailyTaskItemData b)
        {
            int aOrder = GetSortOrder(a as DailyTaskItemData);
            int bOrder = GetSortOrder(b as DailyTaskItemData);
            return bOrder - aOrder;
        }

        private int GetSortOrder(DailyTaskItemData itemData)
        {
            int order = 0;
            int timeStatus = itemData.TimeStatus;
            if (itemData.Status != (int)DailyTaskStatus.Complete)
            {
                if (itemData.TaskType == (int)DailyTaskType.MonthCard)
                {
                    order = 5000000;
                }
                else
                {
                    if (itemData.Status == (int)DailyTaskStatus.CanDelete)
                    {
                        order = 6000000;
                    }
                    else
                    {
                        if (timeStatus == (int)DailyTaskTimeStatus.Valid)
                        {
                            order = 4000000;
                        }
                        else if (timeStatus == (int)DailyTaskTimeStatus.NotStarted)
                        {
                            order = 3000000;
                        }
                        else if (timeStatus == (int)DailyTaskTimeStatus.HaveEnded)
                        {
                            order = 2000000;
                        }
                    }
                }
            }
            else
            {
                order = 1000000;
            }
                
            if (itemData.Status == (int)DailyTaskStatus.CanCommit)
            {
                order += 400000;
            }
            else if (itemData.Status == (int)DailyTaskStatus.Accept)
            {
                order += 200000;
            }
            order -= itemData.Id;

            return order;
        }
    }

    public class DailyTaskCardInfo
    {
        public int cardType;
        public ulong finishTime;
        public uint leftSeconds;
    }
}
