﻿using System;
using System.Collections.Generic;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils;

namespace Common.Data
{
    public class DailyActiveItemData : BaseDailyTaskItemData
    {
        private active_task _activeTaskInfo;
        private int _status = (int)DailyActiveStatus.Accept;
        private int _curTriggerNum = 0;
        private int _maxTriggerNum = 1;
        private int _playerLv = 0;
        private bool _isFollowEvent = false;

        public DailyActiveItemData(int id)
            : base(id)
        {
            active_task activeTaskInfo = reward_system_helper.GetActiveTaskInfo(id);
            if (activeTaskInfo == null)
            {
                LoggerHelper.Error("DailyActiveItemData not exsit active task config ! task id = " + id);
                return;
            }
            _activeTaskInfo = activeTaskInfo;
            if (activeTaskInfo != null)
            {
                _iconId = activeTaskInfo.__icon;

                int eventId = 0;
                int maxTriggerNum = 0;
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(activeTaskInfo.__event_cnds);
                foreach (var item in dataDict)
                {
                    eventId = int.Parse(item.Key);
                    maxTriggerNum = int.Parse(item.Value);
                    break;
                }
                _eventId = eventId;
                _maxTriggerNum = maxTriggerNum;
                _eventAtomId = GetEventAtomId();
                _isFollowEvent = CheckIsFollowEvent();
                //初始化name信息
                ProcessName();
            }
        }

        private void ProcessName()
        {
            if (_eventAtomId == (int)EventAtomId.ItemTotalCount)
            {
                string content = MogoLanguageUtil.GetSourceContent(_activeTaskInfo.__desc);
                int itemId = GetItemId();
                int num = _maxTriggerNum;
                _name = SystemInfoUtil.GenerateContent(content, itemId, num);
            }
            else
            {
                string content = MogoLanguageUtil.GetSourceContent(_activeTaskInfo.__desc);
                if (SystemInfoUtil.IsContentWithParam(content))
                {
                    LoggerHelper.Error("This active task desc config is content with param, this case is not allowed param desc ! task id = " + Id);
                    _name = content;
                    return;
                }
                _name = string.Format(MogoLanguageUtil.GetContent(_activeTaskInfo.__desc), _maxTriggerNum);
            }
        }

        private int GetItemId()
        {
            int itemId = 0;
            Dictionary<int, float> condsInfo = event_cnds_helper.GetEventCnds(_eventId);
            if (condsInfo.Count > 0)
            {
                foreach (var item in condsInfo)
                {
                    int condsId = item.Key;
                    if (condsId == (int)EventCondsParamId.ItemId)
                    {
                        itemId = (int)item.Value;
                        break;
                    }
                }
            }
            return itemId;
        }

        public void UpdateInfo(PbAwardTaskInfo awardTaskInfo)
        {
            int status = ProcessStatus(awardTaskInfo);
            int curTriggerNum = 0;
            if (awardTaskInfo.trigger_info.Count > 0)
            {
                curTriggerNum = awardTaskInfo.trigger_info[0].trigger_num;
            }
            else
            {
                curTriggerNum = CurTriggerNum;
            }
            UpdateInfo(status, curTriggerNum);
        }

        public void UpdateInfo(int status, int curTriggerNum)
        {
            _status = status;
            _curTriggerNum = curTriggerNum;
            if (_curTriggerNum > _maxTriggerNum)
            {
                _curTriggerNum = _maxTriggerNum;
            }
        }

        public void UpdateInfo(int curTriggerNum)
        {
            _curTriggerNum = curTriggerNum;
            if (_curTriggerNum > _maxTriggerNum)
            {
                _curTriggerNum = _maxTriggerNum;
            }
        }

        public bool UpdateReturnStateInfo(int curTriggerNum)
        {
            bool isNeedResort = false;
            if (_status == (int)DailyActiveStatus.CanCommit || _status == (int)DailyActiveStatus.Accept)
            {
                UpdateInfo(curTriggerNum);
                if (_curTriggerNum < _maxTriggerNum)
                {
                    //如果状态从可提交变为未完成，需重新排序
                    if (_status == (int)DailyActiveStatus.CanCommit)
                    {
                        isNeedResort = true;
                    }
                    _status = (int)DailyActiveStatus.Accept;
                }
            }
            return isNeedResort;
        }

        private int ProcessStatus(PbAwardTaskInfo awardTaskInfo)
        {
            int status = 0;
            if (awardTaskInfo.is_complete == public_config.AWARD_ACTIVE_TASK_STATE_COMPLETED)
            {
                if (awardTaskInfo.reward_flag == reward_system_helper.NOT_HAVE_REWARD_FLAG)
                {
                    status = (int)DailyActiveStatus.Complete;
                }
                else
                {
                    status = (int)DailyActiveStatus.CanCommit;
                }
            }
            else
            {
                status = (int)DailyActiveStatus.Accept;
            }
            return status;
        }

        public int Status
        {
            get { return _status; }
        }

        public int CurTriggerNum
        {
            get { return _curTriggerNum; }
            set { _curTriggerNum = value; }
        }

        public int MaxTriggerNum
        {
            get { return _maxTriggerNum; }
        }

        public int ActivePoint
        {
            get
            {
                return _activeTaskInfo != null ? _activeTaskInfo.__daily_active_point : 0;
            }
        }

        public override List<RewardData> RewardList
        {
            get
            {
                if (_rewardList == null)
                {
                    _playerLv = PlayerAvatar.Player.level;
                    HandleRewardList();
                }
                else if (_playerLv != PlayerAvatar.Player.level)
                {
                    if (_status == (int)DailyActiveStatus.Complete)
                        return _rewardList;
                    _playerLv = PlayerAvatar.Player.level;
                    if (_rewardList != null)
                    {
                        _rewardList.Clear();
                        _rewardList = null;
                    }
                    HandleRewardList();
                }
                return _rewardList;
            }
        }

        private void HandleRewardList()
        {
            List<int> rewardIdList = new List<int>();
            if (_activeTaskInfo != null)
            {
                rewardIdList.Add(_activeTaskInfo.__reward);
            }
            _rewardList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
        }

        public bool IsFollowEvent
        {
            get { return _isFollowEvent; }
        }
    }

    public enum DailyActiveStatus
    {
        Accept = 0,
        CanCommit,
        Complete
    }
}
