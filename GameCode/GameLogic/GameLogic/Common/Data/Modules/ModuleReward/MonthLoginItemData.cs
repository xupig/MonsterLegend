﻿using System;
using System.Collections.Generic;
using GameData;
using GameMain.Entities;

namespace Common.Data
{
    public class MonthLoginItemData
    {
        private int _day;
        private month_login_reward _monthInfo;
        private int _status = (int)MonthLoginStatus.UnReceive;
        private int _vipNeed = 0;
        private List<RewardData> _rewardList;
        private int _vipRewardTag = reward_system_helper.NOT_HAVE_REWARD_FLAG;

        public MonthLoginItemData(int day, month_login_reward monthInfo)
        {
            this.Id = day;
            _day = day;
            _monthInfo = monthInfo;
            _vipNeed = 0;
            if (_vipNeed > 0)
            {
                _vipRewardTag = _monthInfo.__reward;
            }
        }

        //为对应第几天
        public int Id
        {
            get;
            protected set;
        }

        public int Day
        {
            get { return _day; }
        }

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int VipNeed
        {
            get { return _vipNeed; }
        }

        public int VipRewardTag
        {
            get { return _vipRewardTag; }
            set { _vipRewardTag = value; }
        }

        public int RewardId
        {
            get
            {
                return _monthInfo.__reward;
            }
        }

        public List<RewardData> RewardList
        {
            get
            {
                if (_rewardList == null)
                {
                    List<int> rewardIdList = new List<int>();
                    rewardIdList.Add(_monthInfo.__reward);
                    _rewardList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
                }
                return _rewardList;
            }
        }
    }

    public enum MonthLoginStatus
    {
        HaveReceived = 0,
        UnReceive = 1,
    }
}
