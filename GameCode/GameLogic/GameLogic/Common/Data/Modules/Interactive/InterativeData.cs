﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class InteractivePanelParameter : BasePanelParameter
    {
        public int InstanceId;

        public InteractivePanelParameter(int instanceId)
        {
            this.InstanceId = instanceId;
        }

        public InteractivePanelParameter(int thirdTab, int instanceId)
        {
            this.InstanceId = instanceId;
            this.ThirdTab = thirdTab;
        }
    }

    public class InterativeData
    {
    }
}
