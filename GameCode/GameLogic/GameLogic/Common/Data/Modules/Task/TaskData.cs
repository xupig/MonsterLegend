﻿#region 模块信息
/*==========================================
// 文件名：TaskData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Task
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 9:33:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameData;
using Common.Utils;
using Common.Global;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using GameMain.Entities;

namespace Common.Data
{
    public class CompleteTaskData
    {
        public int start;
        public int offset;
    }

    public class TaskData
    {

        public List<PbOfflineRecord> offlineRecord;
        public List<PbItemReward> offlineReward;
        public int offlineTimes;

        public PbGuildTaskScoreInfo GuildTaskInfo { get; set; }
        public PbWildTaskInfo WildTaskInfo { get; set; }
        public bool IsAutoWildTask;

        public int task_id
        {
            get;
            set;
        }

        //public int task_status
        //{
        //    get;
        //    set;
        //}
        public int task_place_Id
        {
            get;
            set;
        }

        public Vector3 task_place_pos
        {
            get;
            set;
        }

        public int npcId = 226;

        public int min_level
        {
            get;
            set;
        }

        private int _remain_times;
        public int remain_times
        {
            get
            {
                return _remain_times;
            }
            set
            {
                if(_remain_times!=value)
                {
                    _remain_times = value;
                    EventDispatcher.TriggerEvent(TaskEvent.REMAIN_TIMES_CHANGE);
                }
            }
        }

        public int ring
        {
            get;
            set;
        }


        private int _team_task_times;
        public int team_task_times
        {
            get
            {
                return _team_task_times;
            }
            set
            {
                if(_team_task_times!=value)
                {
                    _team_task_times = value;
                    EventDispatcher.TriggerEvent(TaskEvent.WEEK_TIMES_CHANGE);
                }
            }
        }

        //public bool isTeammateAllReady = false;

        public bool isTeammateInTeamTask()
        {
            if (ring != 0 && PlayerDataManager.Instance.TeamData.CaptainId != 0 && PlayerDataManager.Instance.TeamData.CaptainId != PlayerAvatar.Player.dbid)//在时空裂隙中，并且自己不是队长
             {
                 return true;
             }
             return false;
        }


        public Dictionary<int,int> day_reward_list;

        public void UpdateDayRewardList(List<PbItemReward> dayRewardList)
        {
            day_reward_list.Clear();
            for (int i = 0; i < dayRewardList.Count; i++)
            {
                day_reward_list.Add((int)dayRewardList[i].item_id, (int)dayRewardList[i].item_num);
            }
            EventDispatcher.TriggerEvent(TaskEvent.DAY_REWARD_CHANGE);
        }
        
      
        public HashSet<int> hasReceivedTeamTaskTimeReward;

        public void UpdateWeekRewardList(List<PbWeekRward> week_reward_list)
        {
            hasReceivedTeamTaskTimeReward.Clear();
            for (int i = 0; i < week_reward_list.Count; i++)
            {
                hasReceivedTeamTaskTimeReward.Add((int)week_reward_list[i].reward_id);
            }
            EventDispatcher.TriggerEvent(TaskEvent.WEEK_REWARD_CHANGE);
        }

        public void AddWeekRewardList(PbWeekRward weekReward)
        {
            if (hasReceivedTeamTaskTimeReward.Contains((int)weekReward.reward_id) == false)
            {
                hasReceivedTeamTaskTimeReward.Add((int)weekReward.reward_id);
            }
            EventDispatcher.TriggerEvent(TaskEvent.WEEK_REWARD_CHANGE);
        }

        public Dictionary<int, PbTaskInfo> branchTaskDic;

        private PbTaskInfo _teamTaskInfo;
        public PbTaskInfo teamTaskInfo
        {
            get
            {
                return _teamTaskInfo;
            }
            set
            {
                _teamTaskInfo = value;
                EventDispatcher.TriggerEvent(TaskEvent.TEAM_TASK_STATE_CHANGE);
            }
        }

        public PbTaskInfo preTaskInfo;

        private PbTaskInfo _mainTaskInfo;
        public PbTaskInfo mainTaskInfo
        {
            get
            {
                return _mainTaskInfo;
            }
            set
            {
                _mainTaskInfo = value;
                if(_mainTaskInfo!=null)
                {
                    int lastTaskId = 0;
                    task_data data = task_data_helper.GetTaskData(_mainTaskInfo.task_id);
                    List<int> listInt = data_parse_helper.ParseListInt(data.__last_task);
                    if (data != null && data.__last_task != null && listInt.Count > 0)
                    {
                        lastTaskId = listInt[0];
                    }
                    if(lastTaskId!=0)
                    {
                        if (lastFinishMainTaskId == -1)
                        {
                            lastFinishMainTaskId = lastTaskId;
                            completedMainTaskSet.Clear();
                            InitAllCompletedMainTask(_mainTaskInfo.task_id);
                        }
                        else if (lastFinishMainTaskId != lastTaskId && completedMainTaskSet.Contains(lastTaskId) == false)
                        {
                            completedMainTaskSet.Add(lastTaskId);
                        }
                        lastFinishMainTaskId = lastTaskId;
                    }
                }
                if (preTaskInfo == null && _mainTaskInfo != null)
                {
                    preTaskInfo = _mainTaskInfo;
                }
            }
        }

        public void InitMainTaskAllCompleteData(PbTaskInfo info)
        {
            if(info.status == public_config.TASK_STATE_REWARDED)
            {
                InitAllCompletedMainTask(_mainTaskInfo.task_id);
            }
            else
            {
                int lastTaskId = 0;
                task_data data = task_data_helper.GetTaskData(_mainTaskInfo.task_id);
                List<int> listInt = data_parse_helper.ParseListInt(data.__last_task);
                if (data != null && data.__last_task != null && listInt.Count > 0)
                {
                    lastTaskId = listInt[0];
                }
                if(lastTaskId!=0)
                {
                    InitAllCompletedMainTask(lastTaskId);
                }
            }
        }


        public int mainTaskId
        {
            get 
            { 
                if(_mainTaskInfo!=null)
                {
                    return _mainTaskInfo.task_id;
                }
                else
                {
                    return 0;
                }
            }
        }

       // public int branchTaskId;

        private List<int> dialogSet;

        private HashSet<int> completedMainTaskSet;
        private HashSet<int> completedBranchTaskSet;

        public TaskData()
        {
            day_reward_list = new Dictionary<int, int>();
            hasReceivedTeamTaskTimeReward = new HashSet<int>();
            branchTaskDic = new Dictionary<int, PbTaskInfo>();
           // mainTaskDic = new Dictionary<int, PbTaskInfo>();
            completedMainTaskSet = new HashSet<int>();
            completedBranchTaskSet = new HashSet<int>();
           // dialogSet = LocalCache.Read<List<int>>(LocalName.Task);
            if(dialogSet == null)
            {
                dialogSet = new List<int>();
            }
        }

        public int lastFinishMainTaskId = -1;

        //public int currentNpcDialogTaskId = 0;

        public void InitAllCompletedMainTask(int taskId)
        {
            if(taskId!=0)
            {
                task_data data = task_data_helper.GetTaskData(taskId);
                List<int> listInt = data_parse_helper.ParseListInt(data.__last_task);
                if (data != null && data.__last_task != null && listInt.Count > 0)
                {
                    int lastTaskId = listInt[0];
                    completedMainTaskSet.Add(lastTaskId);
                    InitAllCompletedMainTask(lastTaskId);
                }
            }
        }

        public void InitCompletedTask(PbTaskCompleted completed)
        {
            completedBranchTaskSet.Clear();
            for (int i = 0; i < completed.completed.Count; i++)
            {
                if (!completedBranchTaskSet.Contains(completed.completed[i].value))
                {
                    completedBranchTaskSet.Add(completed.completed[i].value);
                }
                //int[] offsetList = completed.completed[i].value.ToBits();
                //for(int j=0;j<offsetList.Length;j++)
                //{
                //    if(offsetList[j] == 1)
                //    {
                //        if (!completedBranchTaskSet.Contains(completed.completed[i].key + j))
                //        {
                //            Debug.LogError("completedBranchTaskSet:" + (completed.completed[i].key + j) + "," + completed.completed[i].key+","+j);
                //            completedBranchTaskSet.Add(completed.completed[i].key + j);
                //        }
                //    }
                //}
            }
        }

        public void AddCompletedTask(int taskId)
        {
            task_data data = task_data_helper.GetTaskData(taskId);
            if(data.__task_type == public_config.TASK_TYPE_MAIN)
            {
                if (completedMainTaskSet.Contains(taskId) == false)
                {
                    completedMainTaskSet.Add(taskId);
                }
                //UpdateFinishedTask(taskId);
            }
            else
            {
                if (completedBranchTaskSet.Contains(taskId) == false)
                {
                    //UpdateFinishedTask(taskId);
                    completedBranchTaskSet.Add(taskId);
                }
            }
           
        }

        public bool IsTaskFinished(int taskId)
        {
            PbTaskInfo taskData = GetTaskInfo(taskId);
            return taskData != null && taskData.status == public_config.TASK_STATE_ACCOMPLISH;
        }

        public bool IsCompleted(int taskId)
        {
            return completedMainTaskSet.Contains(taskId) || completedBranchTaskSet.Contains(taskId);
        }

        public bool IsFinishedOrComplete(int taskId)
        {
            return IsTaskFinished(taskId) || IsCompleted(taskId);
        }

        //private int lastFinishedTaskId = 0;
        public void AddDialog(int taskId)
        {
            if(dialogSet.Contains(taskId) == false)
            {
                dialogSet.Add(taskId);
                //LocalCache.Write(LocalName.Task, dialogSet, CacheLevel.Permanent);
            }
        }

        //public void AddAcceptableTask(int taskId)
        //{
        //    lastAcceptableTaskList.Add(taskId);
        //}

        //private List<int> lastAcceptableTaskList;
        

        public void RemoveDialog(int taskId)
        {
            if(dialogSet.Contains(taskId))
            {
                dialogSet.Remove(taskId);
                //LocalCache.Write(LocalName.Task, dialogSet, CacheLevel.Permanent);
            }
        }

        public bool ContainsDialog(int taskId)
        {
            return dialogSet.Contains(taskId);
        }

        public PbTaskInfo GetTaskInfo(int taskId)
        {
            if (mainTaskInfo != null && mainTaskInfo.task_id == taskId)
            {
                return mainTaskInfo;
            }
            if (branchTaskDic.ContainsKey(taskId))
            {
                return branchTaskDic[taskId];
            }
            return null;
        }

        public bool IsTaskMoreThanAccepted(int taskId)
        {
            return IsTaskAccepted(taskId) || IsFinishedOrComplete(taskId);
        }
        public bool IsTaskAccepted(int taskId)
        {
            task_data data = task_data_helper.GetTaskData(taskId);
            PbTaskInfo taskInfo = null;
            if (mainTaskInfo!=null && mainTaskInfo.task_id == taskId)
            {
                taskInfo = mainTaskInfo;
            }
            else if (data.__task_type == public_config.TASK_TYPE_BRANCH)
            {
                if (branchTaskDic.ContainsKey(taskId))
                {
                    taskInfo = branchTaskDic[taskId];
                }
            }
            if(taskInfo == null)
            {
                return false;
            }
            return taskInfo.status == public_config.TASK_STATE_ACCEPT;
        }

        public List<int> GetAcceptTaskIdList()
        {
            List<int> taskIdList = new List<int>();
            PbTaskInfo taskInfo = null;
            if (mainTaskId != 0)
            {
                taskInfo = mainTaskInfo;
                if (taskInfo.status == public_config.TASK_STATE_ACCEPT)
                {
                    taskIdList.Add(mainTaskId);
                }
            }
            foreach (PbTaskInfo pbTaskInfo in branchTaskDic.Values)
            {
                if (pbTaskInfo.status == public_config.TASK_STATE_ACCEPT)
                {
                    taskIdList.Add(pbTaskInfo.task_id);
                }
            }
            return taskIdList;
        }
    }
}
