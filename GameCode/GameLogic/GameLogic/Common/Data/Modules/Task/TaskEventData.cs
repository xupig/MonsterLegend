﻿#region 模块信息
/*==========================================
// 文件名：TaskTargetData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Task
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/9 13:53:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{

    public enum TaskEventEnum
    {
        Loop = -1,
        NpcId = 0,
        MonsterId = 1,
        InstId = 2,
        ItemId = 3,
        HpRate = 4,
        Level = 8,

        Activity = 71,

    }
    public class TaskEventData
    {
        public TaskEventEnum type;
        public float value;
    }
}
