﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskBoxData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Task
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/22 11:06:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TeamTaskBoxData
    {
         public BaseItemData BaseItemData;
        public bool hasGet;

        public TeamTaskBoxData(BaseItemData baseItemData, bool hasGet)
        {
            this.BaseItemData = baseItemData;
            this.hasGet = hasGet;
        }
    }
}
