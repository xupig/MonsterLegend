﻿
using Common.Structs.ProtoBuf;
using Common.Utils;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class TransPortalResultPersonData : MultiCopyResultData
    {
        private PbTransPortal _data;
        private CopyCombatPortalLogic _logic;

        public int Score
        {
            get
            {
                return (int)_data.self_score;
            }
        }

        public TransPortalResultPersonData(PbTransPortal transPortalResultData, CopyMultiPlayerLogic logic)
        {
            _data = transPortalResultData;
            AvatarName = MogoProtoUtils.ParseByteArrToString(_data.role_name);
            AvatarLevel = _data.role_level;
            FightForce = _data.fight_force;
            RankNum = (int)_data.rank_level;
            IsWin = (int)_data.win_result;
            AvatarId = _data.entity_id;
            Logic = logic;
        }

        public override CopyMultiPlayerLogic Logic
        {
            get
            {
                return _logic;
            }
            set
            {
                _logic = value as CopyCombatPortalLogic;
            }
        }

        public override List<PbActionInfo> GetActionInfo()
        {
            List<PbActionInfo> result = new List<PbActionInfo>();
            PbActionInfo actionInfo = new PbActionInfo();
            actionInfo.action_id = 10;
            actionInfo.val = Score;
            result.Add(actionInfo);
            return result;
        } 

        public override List<RewardData> GetRewardList()
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < _data.rewards.Count; i++)
            {
                PbRewardItem rewardItem = _data.rewards[i];
                rewardDataList.Add(RewardData.GetRewardData((int)rewardItem.item_id, (int)rewardItem.item_num));
            }
            return rewardDataList;
        }

        public override List<RewardData> GetRankNumReward()
        {
            return GetTotalNormalRewardList();
        }

        public override string GetRankNumRewardChineseDesc()
        {
            return MogoLanguageUtil.GetContent(97021);
        }

    }
}
