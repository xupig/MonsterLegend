﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;

namespace Common.Data
{
    public class ItemToolTipsData : ItemData
    {
        //private CommonItemInfo _itemInfo;

        public ItemToolTipsData(int id)
            : base(id)
        {

        }
        #region 服务器传送的字段
        public int GridPosition
        {
            get;
            set;
        }

        public bool IsBind
        {
            set;
            get;
        }

        public bool IsExpire
        {
            get
            {
                if (ExpireTime == 0)
                {
                    return false;
                }
                return ExpireTime < Global.Global.serverTimeStampSecond;
            }
        }

        public uint ExpireTime
        {
            get;
            set;
        }
        #endregion

        public static ItemToolTipsData CopyFromItemInfo(CommonItemInfo itemInfo)
        {
            ItemToolTipsData itemData = new ItemToolTipsData(itemInfo.Id);
            itemData.GridPosition = itemInfo.GridPosition;
            itemData.StackCount = itemInfo.StackCount;
            itemData.IsBind = itemInfo.IsBind;
            itemData.ExpireTime = itemInfo.ExpireTime;
            return itemData;
        }

    }
}
