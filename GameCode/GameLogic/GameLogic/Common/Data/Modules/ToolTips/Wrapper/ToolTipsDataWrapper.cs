﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;

namespace Common.Data
{
    public class ToolTipsDataWrapper
    {
        public System.Object data;
        public PanelIdEnum context;

        public ToolTipsDataWrapper(System.Object data, PanelIdEnum context)
        {
            this.data = data;
            this.context = context;
        }
    }
}
