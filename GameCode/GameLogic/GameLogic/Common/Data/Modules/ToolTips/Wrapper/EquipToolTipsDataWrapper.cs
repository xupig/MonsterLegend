﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs;

namespace Common.Data
{
    public class EquipToolTipsDataWrapper
    {
        public EquipToolTipsData rightEquipData;
        public EquipToolTipsData leftEquipData;
    }
}
