﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using UnityEngine;

namespace Common.Data
{
    public enum EquipState
    {
        NotHave = 0,
        InBag = 1,
        Wearing = 2,
    }

    public enum EquipDataType
    {
        FromServer = 0, // 服务器同步的数据
        ClientCalculate = 1, // 客户端自行计算相关属性
    }

    public class EquipToolTipsData : EquipData
    {
        private EquipItemInfo _equipInfo;
        private List<int> _rarityPropertyIdList = new List<int>();
        private List<int> _rarityPropertyValueList = new List<int>();
        private List<PbEquipGems> _gemList = new List<PbEquipGems>();
        private EquipState _state = EquipState.NotHave;
        private EquipDataType _equipDataType = EquipDataType.ClientCalculate;

        public EquipState EquipState
        {
            get
            {
                return _state;
            }
            set
            {
                _state = (EquipState)value;
            }
        }

        public EquipDataType EquipDataType
        {
            get
            {
                return _equipDataType;
            }
        }

        public EquipToolTipsData(int id)
            : base(id)
        {

        }

        public EquipItemInfo EquipInfo
        {
            get
            {
                return _equipInfo;
            }
        }

        #region 来自服务器的字段值
        public int GridPosition
        {
            get;
            set;
        }

        public bool IsBind
        {
            get;
            set;
        }

        public int SuitId
        {
            get;
            set;
        }

        /// <summary>
        /// 已开孔的NewSlot数量
        /// </summary>
        public int OpenedNewSlotCount
        {
            get;
            set;
        }

        /// <summary>
        /// 装备上镶嵌宝石列表,index 大于20的为可扩展槽（NewSlot）上的宝石
        /// </summary>
        public List<PbEquipGems> GemList
        {
            get
            {
                if (_equipInfo != null && _gemList.Count == 0)
                {
                    List<PbEquipGems> gemList = _equipInfo.GemList;
                    for (int i = 0; i < gemList.Count; i++)
                    {
                        PbEquipGems gem = new PbEquipGems();
                        gem.gem_id = gemList[i].gem_id;
                        gem.index = gemList[i].index;
                        _gemList.Add(gem);
                    }
                }
                return _gemList;
            }
        }

        /// <summary>
        /// 装备稀有属性Id列表，由服务器通过装备属性随机而来
        /// </summary>
        public List<int> GetRarityPropertyIdList()
        {
            if (_equipInfo != null && _rarityPropertyIdList.Count == 0)
            {
                List<int> rarityIdList = _equipInfo.GetRarityPropertyIdList();
                for (int i = 0; i < rarityIdList.Count; i++)
                {
                    _rarityPropertyIdList.Add(rarityIdList[i]);
                }
            }
            return _rarityPropertyIdList;
        }

        /// <summary>
        /// 装备稀有属性值列表，由服务器通过装备属性随机而来
        /// </summary>
        public List<int> GetRarityPropertyValueList()
        {
            if (_equipInfo != null && _rarityPropertyValueList.Count == 0)
            {
                List<int> rarityValueList = _equipInfo.GetRarityPropertyValueList();
                for (int i = 0; i < rarityValueList.Count; i++)
                {
                    _rarityPropertyValueList.Add(rarityValueList[i]);
                }
            }
            return _rarityPropertyValueList;
        }

        public int StrengthenLevel
        {
            get;
            set;
        }

        public List<PbEquipEnchantAttri> EnchantAttri
        {
            get;
            set;
        }

        public List<uint> BuffList
        {
            get;
            set;
        }

        public uint GetTime
        {
            get;
            set;
        }

        public List<ulong> UserList
        {
            get;
            set;
        }
        #endregion

        public static EquipToolTipsData CopyFromEquipInfo(EquipItemInfo equipInfo)
        {
            //复杂数据字段放在第一次使用的时候再从EquipItemInfo上复制
            EquipToolTipsData equipData = new EquipToolTipsData(equipInfo.Id);
            equipData.GridPosition = equipInfo.GridPosition;
            equipData.SuitId = equipInfo.SuitId;
            equipData.IsBind = equipInfo.IsBind;
            equipData.OpenedNewSlotCount = equipInfo.OpenedNewSlotCount;
            equipData.FightForce = equipInfo.FightForce;
            equipData.StrengthenLevel = equipInfo.StrengthenLevel;
            equipData._equipInfo = equipInfo;
            equipData.EnchantAttri = equipInfo.EnchantAtrri;
            equipData.BuffList = equipInfo.BuffList;
            equipData.GetTime = equipInfo.GetTime;
            equipData.UserList = equipInfo.UserList;
            equipData._equipDataType = EquipDataType.FromServer;
            return equipData;
        }
    }
}
