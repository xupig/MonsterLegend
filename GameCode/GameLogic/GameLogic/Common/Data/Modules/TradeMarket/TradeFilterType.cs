﻿using Common.ServerConfig;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public abstract class TradeFilterType
    {
        public abstract string Name { get;}

        public abstract int Type { get; }

        public List<TradeFilterItem> TradeFilterItemList { get; set; }
    }

    public class TradeLevelFilterType : TradeFilterType
    {
        public override string Name
        {
            //56104，等级筛选
            get { return MogoLanguageUtil.GetContent(56104); }
        }

        public override int Type
        {
            get { return public_config.SEARCH_CND_LEVEL; }
        }
    }

    public class TradeVocationFilterType : TradeFilterType
    {
        public override string Name
        {
            //56105, 职业筛选
            get { return MogoLanguageUtil.GetContent(56105); }
        }

        public override int Type
        {
            get { return public_config.SEARCH_CND_VOCATION; }
        }
    }

    public class TradePositionFilterType : TradeFilterType
    {
        public override string Name
        {
            //56106,部位筛选
            get { return MogoLanguageUtil.GetContent(56106); }
        }

        public override int Type
        {
            get { return public_config.SEARCH_CND_POS; }
        }
    }


}
