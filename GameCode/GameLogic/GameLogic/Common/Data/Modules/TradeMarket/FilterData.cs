﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class FilterData
    {
        /// <summary>
        /// 过滤条件类型，
        /// 按物品子类型搜索 public const int SEARCH_CND_POS = 3;
        /// 按等级搜索 public const int SEARCH_CND_LEVEL = 4;
        /// 按职业搜索 public const int SEARCH_CND_VOCATION = 5;   
        public int FilterType { get; set; }

        public int FilterValue { get; set; }

        public FilterData(int type, int value)
        {
            FilterType = type;
            FilterValue = value;
        }
    }
}
