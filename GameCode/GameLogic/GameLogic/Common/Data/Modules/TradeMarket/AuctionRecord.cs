﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class AuctionRecord
    {
        public PbItemRecord ItemRecord;

        /// <summary>
        ///表示购买记录还是出售记录,1表示购买，2表示出售
        /// </summary>
        public AuctionRecordType RecordType;

        public AuctionRecord(PbItemRecord itemRecord, AuctionRecordType recordType)
        {
            ItemRecord = itemRecord;
            RecordType = recordType;
        }
    }

    public enum AuctionRecordType
    {
        BuyRecord = 1,
        SellRecord  = 2,
    }
}
