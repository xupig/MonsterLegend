﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class AuctionData
    {
        private PbGlobalInfo _globalInfo;
        public DateTime DateTime; //记录当前获取到拍卖行全局数据时服务器时间
        private List<AuctionRecord> _auctionBuyRecordList;
        private List<AuctionRecord> _auctionSellRecordList;
        private HashSet<int> _wantBuySet;

        private Dictionary<int, Dictionary<int,TradeItemData>> _tradeItemDic; // key表示物品在拍卖行分类，value表示该类型的所有物品信息，其中value是字典（key表示物品id，value表示该物品详细信息)

        //记录当前页码
        public int CurPage;

        //记录服务器总共页码
        public int TotalPage;

        public AuctionData()
        {
            InitTradeItemData();
        }

        public void OnReloadData()
        {
            InitTradeItemData();
        }

        public void AddWantBuyItem(int itemId)
        {
            if (_wantBuySet == null)
            {
                _wantBuySet = new HashSet<int>();
            }
            if (_wantBuySet.Contains(itemId) == false)
            {
                _wantBuySet.Add(itemId);
                TradeItemData item = GetTradeItem(itemId);
                item.IsWantBuy = true;
                item.WantAvatarNum++;
                EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, item.BaseItemData.Id);
                EventDispatcher.TriggerEvent(TradeMarketEvents.UpdateWantBuyList);
            }
        }

        public void DeleteWantBuyItem(int itemId)
        {
            if (_wantBuySet == null) { return; }
            if (_wantBuySet.Contains(itemId) == true)
            {
                _wantBuySet.Remove(itemId);
                TradeItemData item = GetTradeItem(itemId);
                item.IsWantBuy = false;
                item.WantAvatarNum--;
                EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, item.BaseItemData.Id);
                EventDispatcher.TriggerEvent(TradeMarketEvents.UpdateWantBuyList);
            }
        }

        public void FillWantBuySet(PbFullWantBuy wantBuy)
        {
            if (_wantBuySet == null)
            {
                _wantBuySet = new HashSet<int>();
            }
            for (int i = 0; i < wantBuy.items.Count; i++)
            {
                _wantBuySet.Add((int)wantBuy.items[i].type_id);
                TradeItemData item = GetTradeItem((int)wantBuy.items[i].type_id);
                item.IsWantBuy = true;
                item.Price = (int)wantBuy.items[i].price;
                item.BaseItemData.StackCount = (int)wantBuy.items[i].left_count;
                item.WantAvatarNum = (int)wantBuy.items[i].want_buy_cnt;
                EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, item.BaseItemData.Id);
            }
        }

        public TradeItemData GetTradeItem(int ItemId)
        {
            int tradeType = trademarket_helper.GetTradeType((int)ItemId);
            return _tradeItemDic[tradeType][(int)ItemId];
        }

        public List<TradeItemData> FilterAndFillWantItemInfo()
        {
            List<TradeItemData> result = new List<TradeItemData>();
            if (_wantBuySet == null)
            {
                return result;
            }
            foreach (int i in _wantBuySet)
            {
                result.Add(GetTradeItem(i));
            }
            result.Sort(SortTradeItem);
            List<int> clientTradeItemIdList = GetClientSupplyTradeItemId(result);
            TradeMarketManager.Instance.RequestWantAvatarNum(clientTradeItemIdList);
            return result;
        }

        public List<TradeItemData> FilterAndFillTradeItemInfo(int tradeType, List<FilterData> filterDataList, int page)
        {
            List<TradeItemData> result = new List<TradeItemData>();
            if (_tradeItemDic.ContainsKey(tradeType) == false)
            {
                return result;
            }
            Dictionary<int, TradeItemData> tradeTypeItemDic = _tradeItemDic[tradeType];
            foreach (KeyValuePair<int, TradeItemData> kvp in tradeTypeItemDic)
            {
                if (IsMatchWorldLevel(kvp.Value) && IsMatchFilter(kvp.Value, filterDataList))
                {
                    result.Add(kvp.Value);
                }
            }
            result.Sort(SortTradeItem);
            int lastPageCount = result.Count % 20;
            if (lastPageCount == 0)
            {
                lastPageCount = 20;
            }
            int index = Math.Min((page - 1) * 20, result.Count - lastPageCount);
            int count = Math.Min(20, result.Count - index);
            CurPage = index / 20 + 1;
            result = result.GetRange(index, count);
            List<int> clientTradeItemIdList = GetClientSupplyTradeItemId(result);
            TradeMarketManager.Instance.RequestWantAvatarNum(clientTradeItemIdList);
            return result;
        }

        private List<int> GetClientSupplyTradeItemId(List<TradeItemData> tradeItemList)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < tradeItemList.Count; i++)
            {
                if (tradeItemList[i].IsClientData == true)
                {
                    result.Add(tradeItemList[i].BaseItemData.Id);
                }
            }
            return result;
        }

        private int SortTradeItem(TradeItemData first, TradeItemData second)
        {
            if ((first.BaseItemData.StackCount == 0 && second.BaseItemData.StackCount == 0)
                || (first.BaseItemData.StackCount != 0 && second.BaseItemData.StackCount != 0))
            
            {
                return second.BaseItemData.Id - first.BaseItemData.Id;
            }
            else
            {
                return second.BaseItemData.StackCount - first.BaseItemData.StackCount;
            }
        }

        private bool IsMatchWorldLevel(TradeItemData item)
        {
            List<int> useLevelList = item.GetUseLevel();
            //无等级限制
            if (useLevelList.Count == 0)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < useLevelList.Count; i++)
                {
                    if (_globalInfo.level_topmost >= useLevelList[i])
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private bool IsMatchFilter(TradeItemData item, List<FilterData> filterDataList)
        {
            for (int i = 0; i < filterDataList.Count; i++)
            {
                switch (filterDataList[i].FilterType)
                {
                    case public_config.SEARCH_CND_LEVEL:
                        if (item.GetUseLevel().Contains(filterDataList[i].FilterValue) == false) { return false; }
                        break;
                    case public_config.SEARCH_CND_VOCATION:
                        if (item.GetUseVocation().Contains(filterDataList[i].FilterValue) == false) { return false; }
                        break;
                    case public_config.SEARCH_CND_POS:
                        if (item.GetUsePosition().Contains(filterDataList[i].FilterValue) == false) { return false; }
                        break;
                }
            }
            return true;
        }

        public void RefreshTradeItemBuyCount(PbItemCount pbItemCount)
        {
            int tradeType = trademarket_helper.GetTradeType((int)pbItemCount.type_id);
            TradeItemData tradeItemData = _tradeItemDic[tradeType][(int)pbItemCount.type_id];
            tradeItemData.BuyCount = pbItemCount.count;
            EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, tradeItemData.BaseItemData.Id);
        }

        public void RefreshTradeItemsInfo(PbMarketItems marketItems)
        {
            TotalPage = marketItems.total_page;
            for (int i = 0; i < marketItems.items.Count; i++)
            {
                PbMarketItem item = marketItems.items[i];
                int tradeType = trademarket_helper.GetTradeType((int)item.type_id);
                TradeItemData tradeItemData = _tradeItemDic[tradeType][(int)item.type_id];
                tradeItemData.Price = (int)item.price;
                tradeItemData.BaseItemData.StackCount = (int)item.left_count;
                tradeItemData.WantAvatarNum = (int)item.want_buy_cnt;
                tradeItemData.IsClientData = false;
            }
        }

        public void BuyTradeItemCount(int itemId, int count)
        {
            int tradeType = trademarket_helper.GetTradeType(itemId);
            TradeItemData tradeItemData = _tradeItemDic[tradeType][itemId];
            tradeItemData.BaseItemData.StackCount -= count;
        }

        public void RefreshGlobalInfo(PbGlobalInfo globalInfo)
        {
            _globalInfo = globalInfo;
            DateTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
        }

        public void FillBuyRecordList(PbItemRecords itemRecords)
        {
            if (_auctionBuyRecordList == null)
            {
                _auctionBuyRecordList = new List<AuctionRecord>();
            }
            _auctionBuyRecordList.Clear();
            for (int i = 0; i < itemRecords.records.Count; i++)
            {
                _auctionBuyRecordList.Add(new AuctionRecord(itemRecords.records[i], AuctionRecordType.BuyRecord));
            }
        }

        public List<AuctionRecord> GetBuyRecordList()
        {
            _auctionBuyRecordList.Sort(SortRecordFunc);
            return _auctionBuyRecordList;
        }

        public void UpdateWantBuyInfo(PbItemsWantBuy wantBuy)
        {
            for (int i = 0; i < wantBuy.items.Count; i++)
            {
                PbItemWantBuy item = wantBuy.items[i];
                int tradeType = trademarket_helper.GetTradeType((int)item.type_id);
                TradeItemData tradeItemData = _tradeItemDic[tradeType][(int)item.type_id];
                tradeItemData.WantAvatarNum = (int)item.wan_buy_cnt;
                EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, (int)item.type_id);
            }
        }

        public void FillSellRecordList(PbItemRecords itemRecords)
        {
            if (_auctionSellRecordList == null)
            {
                _auctionSellRecordList = new List<AuctionRecord>();
            }
            _auctionSellRecordList.Clear();
            for (int i = 0; i < itemRecords.records.Count; i++)
            {
                _auctionSellRecordList.Add(new AuctionRecord(itemRecords.records[i], AuctionRecordType.SellRecord));
            }
        }

        public List<AuctionRecord> GetSellRecordList()
        {
            _auctionSellRecordList.Sort(SortRecordFunc);
            return _auctionSellRecordList;
        }

        public void RefreshExtendGrid(PbExtendGrid unlockNum)
        {
            _globalInfo.grid_cnt = unlockNum.count;
        }

        public void RefreshMoney(PbLeftMoney leftMoney)
        {
            if (_globalInfo != null)
            {
                _globalInfo.bind_diamond = leftMoney.bind_diamond;
                _globalInfo.diamond = leftMoney.diamond;
                _globalInfo.gold = leftMoney.gold;
            }
            if (PlayerDataManager.Instance.AuctionData.NoticeCount + PlayerDataManager.Instance.PutOutData.NoticeCount > 0)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.tradeMarket);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.tradeMarket);
            }
            EventDispatcher.TriggerEvent(TradeMarketEvents.NoticeCountChanged);
        }

        public int LeftRefreshTime
        {
            get
            {
                if (_globalInfo != null)
                {
                    return (int)_globalInfo.refresh_left_time;
                }
                return 0;
            }
        }

        public int LeftBindDiamond
        {
            get
            {
                if (_globalInfo != null)
                {
                    return _globalInfo.bind_diamond;
                }
                return 0; 
            }
        }


        public int UnlockGridNum
        {
            get
            {
                if (_globalInfo != null)
                {
                    return _globalInfo.grid_cnt;
                }
                return 0; 
            }
        }


        public int NoticeCount
        {
            get
            {
                return LeftBindDiamond == 0 ? 0 : 1;
            }
        }

        private int SortRecordFunc(AuctionRecord first, AuctionRecord second)
        {
            return (int)(second.ItemRecord.time - first.ItemRecord.time);
        }

        private void InitTradeItemData()
        {
            _tradeItemDic = new Dictionary<int,Dictionary<int,TradeItemData>>();
            foreach (KeyValuePair<int, trade_items> kvp in XMLManager.trade_items)
            {
                trade_items tradeItem = kvp.Value;
                if(_tradeItemDic.ContainsKey(tradeItem.__type) == false)
                {
                    _tradeItemDic[tradeItem.__type] = new Dictionary<int, TradeItemData>();
                }
                _tradeItemDic[tradeItem.__type][kvp.Key] = new TradeItemData(kvp.Key);
            }
        }

        public void ResetTradeItemData(int tradeType)
        {
            Dictionary<int, TradeItemData> tradeTypeItemDic = _tradeItemDic[tradeType];
            foreach (KeyValuePair<int, TradeItemData> kvp in tradeTypeItemDic)
            {
                kvp.Value.IsClientData = true;
                kvp.Value.BaseItemData.StackCount = 0;
            }
        }
    }
}
