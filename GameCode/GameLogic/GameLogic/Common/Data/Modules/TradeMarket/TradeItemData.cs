﻿using Common.Data;
using Common.Structs.ProtoBuf;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TradeItemData
    {
        private BaseItemData _baseItemData;
        private trade_items _tradeConfig;
        private bool _isWantBuy = false;
        private bool _isClientData = true;

        public TradeItemData(int id)
        {
            BagItemType itemType = item_helper.GetItemType(id);
            if (itemType == BagItemType.Equip)
            {
                _baseItemData = new EquipData(id);
            }
            else
            {
                _baseItemData = new ItemData(id);
            }

            _tradeConfig = XMLManager.trade_items[id];
        }

        /// <summary>
        /// 物品位置
        /// </summary>
        public int GridPosition
        {
            get;
            set;

        }

        public BaseItemData BaseItemData
        {
            get
            {
                return _baseItemData;
            }
            set
            {
                _baseItemData = value;
            }

        }

        public int SellUpLimit
        {
            get 
            {
                if (_tradeConfig != null)
                {
                    return _tradeConfig.__sell_max;
                }
                return 0;
            }
        }

        public int BuyUpLimit
        {
            get
            {
                if (_tradeConfig != null)
                {
                    return _tradeConfig.__buy_max;
                }
                return 0;
            }
        }

        public int SellCount
        {
            get;
            set;
        }

        public int BuyCount
        {
            get;
            set;
        }

        public int Price
        {
            get;
            set;
        }

        public bool IsWantBuy
        {
            get
            {
                return _isWantBuy;
            }
            set
            {
                _isWantBuy = value;
            }
        }

        public bool IsClientData
        {
            get
            {
                return _isClientData;
            }
            set
            {
                _isClientData = value;
            }
        }

        public int WantAvatarNum
        {
            get;
            set;
        }

        public int PriceUpLimit
        {
            get
            {
                if (_tradeConfig != null)
                {
                    List<string> dataList = data_parse_helper.ParseListString(_tradeConfig.__price_range);
                    return int.Parse(dataList[1]);
                }
                return 0;
            }
        }

        public int PriceDownLimit
        {
            get
            {
                if (_tradeConfig != null)
                {
                    List<string> dataList = data_parse_helper.ParseListString(_tradeConfig.__price_range);
                    return int.Parse(dataList[0]);
                }
                return 0;
            }
        }

        public int StandPrice
        {
            get
            {
                if (_tradeConfig != null)
                {
                    return _tradeConfig.__stand_price;
                }
                return 0;
            }
        }

        public List<int> GetUseLevel()
        {
            return trademarket_helper.GetUseLevel(_tradeConfig);
        }

        public List<int> GetUseVocation()
        {
            return trademarket_helper.GetUseVocation(_tradeConfig);
        }

        public List<int> GetUsePosition()
        {
            return trademarket_helper.GetUsePosition(_tradeConfig);
        }

        public int GetTradeType()
        {
            return trademarket_helper.GetTradeType(_tradeConfig);
        }

        public string GetTradeTypeName()
        {
            return trademarket_helper.GetTradeTypeName(_tradeConfig);
        }

        public int GetPerBuyLimitNum()
        {
            return trademarket_helper.GetPerBuyLimit(_tradeConfig);
        }

    }
}
