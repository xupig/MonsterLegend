﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PutOutData
    {
        private Dictionary<int, PutOutItemData> _putOutItemDic;
        private int _noticeCount = 0;

        public void Fill(PbPutawayItems items)
        {
            if (_putOutItemDic == null)
            {
                _putOutItemDic = new Dictionary<int, PutOutItemData>();
            }
            _putOutItemDic.Clear();
            int count = items.items.Count;
            for (int i = 0; i < count; i++)
            {
                _putOutItemDic[items.items[i].grid] = new PutOutItemData(items.items[i], PlayerTimerManager.GetInstance().GetNowServerDateTime());
            }
            RefreshNoticeCount();
        }

        public int NoticeCount
        {
            get
            {
                return _noticeCount;
            }
        }

        public void RemoveItem(int gridId)
        {
            if (_putOutItemDic.ContainsKey(gridId) == true)
            {
                _putOutItemDic.Remove(gridId);
            }

            RefreshNoticeCount();
        }

        public void RefreshItem(PbPutawayItem item)
        {
            if (_putOutItemDic == null)
            {
                _putOutItemDic = new Dictionary<int, PutOutItemData>();
            }
            if (_putOutItemDic.ContainsKey(item.grid) == true)
            {
                _putOutItemDic.Remove(item.grid);
            }
            _putOutItemDic[item.grid] = new PutOutItemData(item, PlayerTimerManager.GetInstance().GetNowServerDateTime());
            RefreshNoticeCount();
        }

        public Dictionary<int, PutOutItemData> GetPutOutItemDic()
        {
            return _putOutItemDic;
        }

        private void RefreshNoticeCount()
        {
            int preNoticeCount = _noticeCount;
            _noticeCount = 0;
            foreach (KeyValuePair<int, PutOutItemData> kvp in _putOutItemDic)
            {
                if (kvp.Value.PbPutawayItem.item.count == 0)
                {
                    _noticeCount++;
                }
                else if (kvp.Value.PbPutawayItem.left_time <= 0)
                {
                    _noticeCount++;
                }
            }
            if (_noticeCount != preNoticeCount)
            {
                if (PlayerDataManager.Instance.AuctionData.NoticeCount + PlayerDataManager.Instance.PutOutData.NoticeCount > 0)
                {
                    PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.tradeMarket);
                }
                else
                {
                    PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.tradeMarket);
                }
                EventDispatcher.TriggerEvent(TradeMarketEvents.NoticeCountChanged);
            }
        }
    }

    public class PutOutItemData
    {
        public PbPutawayItem PbPutawayItem;

        /// <summary>
        /// 获取该数据时服务器时间
        /// </summary>
        public DateTime DateTime;
 
        public PutOutItemData(PbPutawayItem pbPutawayItem, DateTime dateTime)
        {
            PbPutawayItem = pbPutawayItem;
            DateTime = dateTime;
        }
    }
}
