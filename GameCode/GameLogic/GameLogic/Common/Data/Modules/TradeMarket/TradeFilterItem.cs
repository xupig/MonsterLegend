﻿using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TradeFilterItem
    {
        protected int _value;

        public TradeFilterItem(int value)
        {
            _value = value;
        }

        public int Value 
        {
            get
            {
                return _value;
            }
        }

        public virtual string Content 
        {
            get
            {
                //56735,清空选择
                return MogoLanguageUtil.GetContent(56735);
            }
        }

    }

    public class TradeLevelFilterItem : TradeFilterItem
    {
        public TradeLevelFilterItem(int value)
            : base(value)
        {

        }

        public override string Content
        {
            get
            {
                return MogoLanguageUtil.GetContent(56232, _value);
            }
        }
    }

    public class TradeVocationFilterItem : TradeFilterItem
    {
        public TradeVocationFilterItem(int value)
            : base(value)
        {

        }

        public override string Content
        {
            get
            {
                return item_helper.GetItemVocationDesc(_value);
            }
        }
    }

    public class TradePositionFilterItem : TradeFilterItem
    {
        public TradePositionFilterItem(int value)
            : base(value)
        {

        }

        public override string Content
        {
            get
            {
                return item_helper.GetEquipTypeDesc(_value);
            }
        }
    }
}
