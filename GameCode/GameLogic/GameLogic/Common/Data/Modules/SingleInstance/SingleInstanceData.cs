﻿using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
#region 模块信息
/*==========================================
// 文件名：SingleInstanceData
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/20 16:58:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace Common.Data
{
    public enum SingleInstanceChapterType
    {
        PetInstanceOne = 401,
        PetInstanceTwo = 402,
        PetInstanceThree = 403,
        PetInstanceFour = 404,
    }

    public class SingleInstanceData
    {
        private const int SINGLE_INSTANCE_PRICE_ID = 109;

        private Dictionary<int, SingleChapterInfo> singleChapterDict;
        private Dictionary<int, SingleInstanceInfo> singleInstanceDict;
        private Dictionary<int, int> singleInstanceBuyCounts;

        public SingleInstanceData()
        {
            singleChapterDict = new Dictionary<int, SingleChapterInfo>();
            singleInstanceDict = new Dictionary<int, SingleInstanceInfo>();
            InitSingleInstanceBuyCount();
        }

        public bool ContainsPriceId(int priceId)
        {
            return singleInstanceBuyCounts.ContainsKey(priceId);
        }

        private void InitSingleInstanceBuyCount()
        {
            singleInstanceBuyCounts = new Dictionary<int, int>();
            foreach (int i in Enum.GetValues(typeof(SingleInstanceChapterType)))
            {
                if (singleInstanceBuyCounts.ContainsKey(GetPriceId(i)) == false)
                {
                    singleInstanceBuyCounts.Add(GetPriceId(i), 0);
                }
            }
        }

        public void UpdatePriceDict(PbPriceInfo pb)
        {
            if (singleInstanceBuyCounts.ContainsKey((int)pb.price_id))
            {
                singleInstanceBuyCounts[(int)pb.price_id] = (int)pb.buy_count;
            }
            else
            {
                singleInstanceBuyCounts.Add((int)pb.price_id, (int)pb.buy_count);
            }
            EventDispatcher.TriggerEvent(SingleInstanceEvents.REFRESH_BUY_COUNTS);
        }

        public void UpdateChapterInfo(PbSingChapterInfo pbSingChapterInfo)
        {
            int chapterId = (int)pbSingChapterInfo.chapter_id;
            if (singleChapterDict.ContainsKey(chapterId))
            {
                singleChapterDict[chapterId].hasChallengedCounts = (int)pbSingChapterInfo.daily_count;
                singleChapterDict[chapterId].sumChanllengeCounts = (int)pbSingChapterInfo.daily_sum_count;
            }
            else
            {
                singleChapterDict.Add(chapterId, new SingleChapterInfo(pbSingChapterInfo));
            }
            EventDispatcher.TriggerEvent(SingleInstanceEvents.REFRESH_CHPATER_COUNTS);
        }

        public void UpdateAllChapterInfo(PbSingleMissionInfo pb)
        {
            for (int i = 0; i < pb.single_mission_info.Count; i++)
            {
                UpdateChapterInfo(pb.single_mission_info[i]);
            }
        }

        public void UpadateSingleMission(PbSingleMissionState pb)
        {
            int instanceId = (int)pb.mission_id;
            if (singleInstanceDict.ContainsKey(instanceId))
            {
                singleInstanceDict[instanceId].chapterId = chapters_helper.GetChapterId(instanceId);
                singleInstanceDict[instanceId].state = SetSingleInstanceState(pb.daily_pass);
            }
            else
            {
                singleInstanceDict.Add(instanceId, new SingleInstanceInfo(pb));
            }
        }

        public void UpdateSingleMissionState(PbSingleMissionStateList pb)
        {
            for (int i = 0; i < pb.state_list.Count; i++)
            {
                UpadateSingleMission(pb.state_list[i]);
            }
        }

        private SingleInstanceState SetSingleInstanceState(int i)
        {
            switch (i)
            {
                case -1:
                    return SingleInstanceState.firstNotPass;
                case 0:
                    return SingleInstanceState.neverEnter;
                case 1:
                    return SingleInstanceState.firstPass;
            }
            return SingleInstanceState.noFightRights;
        }

        public SingleInstanceState GetInstanceState(int instanceId)
        {
            if (singleInstanceDict.ContainsKey(instanceId))
            {
                return singleInstanceDict[instanceId].state;
            }
            else
            {
                UnityEngine.Debug.LogError("副本ID : " + instanceId + "不存在");
                return SingleInstanceState.noFightRights;
            }
        }

        public int GetChapterEnterTimes(int chapterId)
        {
            if (singleChapterDict.ContainsKey(chapterId))
            {
                return singleChapterDict[chapterId].hasChallengedCounts;
            }
            else
            {
                return 0;
            }
        }

        public int GetChpaterTotalTimes(int chapterId)
        {
            if (singleChapterDict.ContainsKey(chapterId))
            {
                return singleChapterDict[chapterId].sumChanllengeCounts;
            }
            return 0;
        }

        public int GetPriceId(int chapterId)
        {
            string px = global_params_helper.GetGlobalParam(SINGLE_INSTANCE_PRICE_ID);
            Dictionary<int, int> dict = MogoStringUtils.Convert2Dic(px);
            if (dict.ContainsKey(chapterId))
            {
                return dict[chapterId];
            }
            else
            {
                throw new Exception("全局参数表缺少配置");
            }
        }

        public int GetBuyCounts(int priceId)
        {
            if (singleInstanceBuyCounts.ContainsKey(priceId))
            {
                return singleInstanceBuyCounts[priceId];
            }
            else
            {
                throw new Exception("缺少价格参数");
            }
        }

        public void UpdateChapterEnterCounts(PbMopUpInfo pbMopUpInfo)
        {
            int chapterId = chapters_helper.GetChapterId((int)pbMopUpInfo.mission_id);
            if (singleChapterDict.ContainsKey(chapterId))
            {
                singleChapterDict[chapterId].hasChallengedCounts = (int)pbMopUpInfo.daily_count;
            }
            EventDispatcher.TriggerEvent(SingleInstanceEvents.REFRESH_CHPATER_COUNTS);
        }
    }
}