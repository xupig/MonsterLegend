﻿#region 模块信息
/*==========================================
// 文件名：SingleChapterInfo
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.SingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/20 18:41:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using GameData;
namespace Common.Data
{
    public enum SingleInstanceState
    {
        noFightRights,
        neverEnter,
        firstNotPass,
        firstPass,
        NoCounts,
    }

    public class SingleChapterInfo
    {
        public int chapterId;
        public int hasChallengedCounts;
        public int sumChanllengeCounts;

        public SingleChapterInfo(int chapterId, int hasChallengedCounts, int sumChanllengeCounts)
        {
            this.chapterId = chapterId;
            this.hasChallengedCounts = hasChallengedCounts;
            this.sumChanllengeCounts = sumChanllengeCounts;
        }

        public SingleChapterInfo(PbSingChapterInfo pb)
        {
            this.chapterId = (int)pb.chapter_id;
            this.hasChallengedCounts = (int)pb.daily_count;
            this.sumChanllengeCounts = (int)pb.daily_sum_count;
        }
    }

    public class SingleInstanceInfo
    {
        public int chapterId;
        public int instanceId;
        public SingleInstanceState state;

        public SingleInstanceInfo(PbSingleMissionState state)
        {
            this.chapterId = chapters_helper.GetChapterId((int)state.mission_id);
            this.instanceId = (int)state.mission_id;
            this.state = SetSingleInstanceState(state.daily_pass);
        }

        private SingleInstanceState SetSingleInstanceState(int i)
        {
            SingleInstanceState state;
            switch (i)
            {
                case -1:
                    state = SingleInstanceState.firstNotPass;
                    break;
                case 0:
                    state = SingleInstanceState.neverEnter;
                    break;
                case 1:
                    state = SingleInstanceState.firstPass;
                    break;
                default:
                    state = SingleInstanceState.noFightRights;
                    break;
            }
            return state;
        }

    }

    
}