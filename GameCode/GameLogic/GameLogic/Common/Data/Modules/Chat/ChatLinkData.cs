﻿using System;
using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using System.Text.RegularExpressions;
using GameLoader.Utils;

namespace Common.Data
{
    public class ChatLinkData
    {
        private static Regex LINK_DATA_STRING_PATTERN = new Regex(@"\[((\w{1,50}\;){1,9})(\w{1,50})\]");
        public static Regex LINK_OR_EMOJI_PATTERN = new Regex(@"(\[E\d{1,2}\])|(\[.*?\,(\d{1,20}\,){0,1}\d{1,20}\])");
        public static Regex LINK_CONTENT_PATTERN = new Regex(@"\[.*?\,(\d{1,20}\,){0,1}\d{1,20}\]");
        public static Regex LINK_TYPE_PATTERN = new Regex(@"(?<=(\[.*?,))\d{1,3}((?=\,)|(?=\]))");
        private static Regex LINK_DESC_PATTERN = new Regex(@"(?<=\[).*?(?=\,)");

        private Dictionary<int, List<ChatLinkInfo>> _chatLinkInfoDict;
        private Dictionary<int, HashSet<uint>> _chatIdDict;
        private Dictionary<int, HashSet<PbChatInfoResp>> _chatInfoSetDict;  //在各频道中保存具有链接内容的PbChatInfoResp的Set

        private int[] _channelIds = new int[] {public_config.CHANNEL_ID_WORLD, public_config.CHANNEL_ID_TEAM, public_config.CHANNEL_ID_GUILD, public_config.CHANNEL_ID_PRIVATE
            , public_config.CHANNEL_ID_HELP, public_config.CHANNEL_ID_SYSTEM };

        private ChatData _chatData;

        public ChatLinkData(ChatData chatData)
        {
            _chatData = chatData;
            _chatLinkInfoDict = new Dictionary<int, List<ChatLinkInfo>>();
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_WORLD, new List<ChatLinkInfo>());
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_TEAM, new List<ChatLinkInfo>());
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_GUILD, new List<ChatLinkInfo>());
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_PRIVATE, new List<ChatLinkInfo>());
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_HELP, new List<ChatLinkInfo>());
            _chatLinkInfoDict.Add(public_config.CHANNEL_ID_SYSTEM, new List<ChatLinkInfo>());

            _chatIdDict = new Dictionary<int, HashSet<uint>>();
            _chatIdDict.Add(public_config.CHANNEL_ID_WORLD, new HashSet<uint>());
            _chatIdDict.Add(public_config.CHANNEL_ID_TEAM, new HashSet<uint>());
            _chatIdDict.Add(public_config.CHANNEL_ID_GUILD, new HashSet<uint>());
            _chatIdDict.Add(public_config.CHANNEL_ID_PRIVATE, new HashSet<uint>());
            _chatIdDict.Add(public_config.CHANNEL_ID_HELP, new HashSet<uint>());
            _chatIdDict.Add(public_config.CHANNEL_ID_SYSTEM, new HashSet<uint>());

            _chatInfoSetDict = new Dictionary<int, HashSet<PbChatInfoResp>>();
            _chatInfoSetDict.Add(public_config.CHANNEL_ID_ALL, new HashSet<PbChatInfoResp>());
            for (int i = 0; i < _channelIds.Length; i++)
            {
                _chatInfoSetDict.Add(_channelIds[i], new HashSet<PbChatInfoResp>());
            }
        }

        public void AddChatLinkInfo(PbChatInfoResp chatInfo)
        {
            if (chatInfo == null || chatInfo.seq_id == 0)
                return;
            string[] contentSplits = chatInfo.msg.Split(ChatData.CHAT_CONTENT_AND_LINK_SPLIT_TAG);
            if (contentSplits.Length <= 1)
                return;
            AddChatInfoWithLink(chatInfo);
            int channelId = GetChannelId(chatInfo);
            if (_chatIdDict.ContainsKey(channelId) && CheckChatIdExsit(channelId, chatInfo.seq_id))
            {
                return;
            }

            _chatIdDict[channelId].Add(chatInfo.seq_id);
            if (_chatLinkInfoDict.ContainsKey(channelId))
            {
                List<ChatLinkInfo> linkInfoList = _chatLinkInfoDict[channelId];
                ChatLinkInfo linkInfo = new ChatLinkInfo(chatInfo.seq_id);
                string linkDataString = contentSplits[1];
                MatchCollection matches = LINK_DATA_STRING_PATTERN.Matches(linkDataString);
                MatchCollection linkOrEmojiMatches = LINK_OR_EMOJI_PATTERN.Matches(contentSplits[0]);
                List<Match> linkContentMatchList = new List<Match>();
                for (int i = 0; i < linkOrEmojiMatches.Count; i++)
                {
                    string value = linkOrEmojiMatches[i].Value;
                    if (LINK_CONTENT_PATTERN.IsMatch(value))
                    {
                        linkContentMatchList.Add(linkOrEmojiMatches[i]);
                    }
                }

                string wrapperDataString = string.Empty;
                if (matches.Count > 0 && linkInfo.linkWrapperList == null)
                {
                    linkInfo.linkWrapperList = new List<ChatLinkBaseWrapper>();
                }
                for (int i = 0; i < matches.Count; i++)
                {
                    wrapperDataString = matches[i].Value;
                    ChatLinkBaseWrapper linkWrapper = ChatLinkWrapperManager.Instance.CreateWrapperFromDataString(wrapperDataString);
                    //在这里处理链接的描述信息，是将在创建聊天内容时对内容的解析移到数据层去处理
                    if (linkContentMatchList.Count > i)
                    {
                        string linkContent = linkContentMatchList[i].Value;
                        string strLinkType = LINK_TYPE_PATTERN.Match(linkContent).Value;
                        int linkType = (int)ChatLinkType.Item;
                        int.TryParse(strLinkType, out linkType);
                        if (linkType == (int)linkWrapper.LinkType)
                        {
                            linkWrapper.LinkDesc = LINK_DESC_PATTERN.Match(linkContent).Value;
                        }
                        else
                        {
                            LoggerHelper.Error("AddChatLinkInfo, 聊天内容中匹配到的链接内容的链接类型与链接数据中的链接类型不一致！chat msg = " + chatInfo.msg);
                        }
                    }
                    else
                    {
                        LoggerHelper.Error("AddChatLinkInfo, 在聊天内容中匹配到的链接内容的数量小于链接数据字符串的数量！chat msg = " + chatInfo.msg);
                    }
                    linkInfo.linkWrapperList.Add(linkWrapper);
                }
                linkInfoList.Add(linkInfo);
            }
        }

        private int GetChannelId(PbChatInfoResp chatInfo)
        {
            return _chatData.GetChannelId(chatInfo);
        }

        private bool CheckChatIdExsit(int channelId, uint chatId)
        {
            if (_chatIdDict.ContainsKey(channelId))
            {
                return _chatIdDict[channelId].Contains(chatId);
            }
            else
            {
                return false;
            }
        }

        public List<ChatLinkBaseWrapper> GetChatLinkWrapperList(PbChatInfoResp chatInfo)
        {
            int channelId = GetChannelId(chatInfo);
            uint chatId = chatInfo.seq_id;
            if (_chatLinkInfoDict.ContainsKey(channelId))
            {
                List<ChatLinkInfo> linkInfoList = _chatLinkInfoDict[channelId];
                for (int i = 0; i < linkInfoList.Count; i++)
                {
                    if (linkInfoList[i].ChatId == chatId)
                    {
                        return linkInfoList[i].linkWrapperList;
                    }
                }
            }
            return null;
        }

        private void AddChatInfoWithLink(PbChatInfoResp chatInfo)
        {
            HashSet<PbChatInfoResp> allChannelChatInfoSet = _chatInfoSetDict[public_config.CHANNEL_ID_ALL];
            if (!allChannelChatInfoSet.Contains(chatInfo))
            {
                allChannelChatInfoSet.Add(chatInfo);
            }

            int channelId = GetChannelId(chatInfo);
            HashSet<PbChatInfoResp> chatInfoSet = _chatInfoSetDict[channelId];
            if (!chatInfoSet.Contains(chatInfo))
            {
                chatInfoSet.Add(chatInfo);
            }
        }

        public void ProcessRemoveChatLinkInfo(int viewChannelId, PbChatInfoResp chatInfo)
        {
            //如果是非链接类消息，就不处理了
            HashSet<PbChatInfoResp> chatInfoSet = _chatInfoSetDict[viewChannelId];
            if (!chatInfoSet.Contains(chatInfo))
                return;

            chatInfoSet.Remove(chatInfo);

            bool isNeedRemove = true;
            if (viewChannelId != public_config.CHANNEL_ID_ALL)
            {
                HashSet<PbChatInfoResp> channelChatInfoSet = _chatInfoSetDict[public_config.CHANNEL_ID_ALL];
                if (channelChatInfoSet.Contains(chatInfo))
                {
                    isNeedRemove = false;
                }
                else
                {
                    for (int i = 0; i < _channelIds.Length; i++)
                    {
                        if (_channelIds[i] != viewChannelId)
                        {
                            channelChatInfoSet = _chatInfoSetDict[_channelIds[i]];
                            if (channelChatInfoSet.Contains(chatInfo))
                            {
                                isNeedRemove = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                HashSet<PbChatInfoResp> channelChatInfoSet = _chatInfoSetDict[public_config.CHANNEL_ID_ALL];
                for (int i = 0; i < _channelIds.Length; i++)
                {
                    channelChatInfoSet = _chatInfoSetDict[_channelIds[i]];
                    if (channelChatInfoSet.Contains(chatInfo))
                    {
                        isNeedRemove = false;
                        break;
                    }
                }
            }

            if (isNeedRemove)
            {
                int channelId = GetChannelId(chatInfo);
                HashSet<uint> chatIdSet = _chatIdDict[channelId];
                chatIdSet.Remove(chatInfo.seq_id);
                List<ChatLinkInfo> linkInfoList = _chatLinkInfoDict[channelId];
                for (int i = 0; i < linkInfoList.Count; i++)
                {
                    if (linkInfoList[i].ChatId == chatInfo.seq_id)
                    {
                        linkInfoList.RemoveAt(i);
                        break;
                    }
                }
            }
        }

    }

    public class ChatLinkInfo
    {
        public uint ChatId { get; private set; }

        public ChatLinkInfo(uint chatId)
        {
            ChatId = chatId;
        }
        public List<ChatLinkBaseWrapper> linkWrapperList;
    }

}
