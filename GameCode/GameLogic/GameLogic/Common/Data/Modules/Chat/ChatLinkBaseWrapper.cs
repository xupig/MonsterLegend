﻿using System;
using System.Collections.Generic;
using GameLoader.Utils;

namespace Common.Data
{
    public abstract class ChatLinkBaseWrapper
    {
        private uint _linkId = 0;
        protected string _linkDesc = string.Empty;  //该字段就是链接的描述内容，比如xxx物品，公会、组队招募的描述等
        protected ChatLinkType _linkType = ChatLinkType.Item;

        //必须在子类也实现该构造函数
        public ChatLinkBaseWrapper(ChatLinkType linkType, string[] dataStringSplits)
        {
            _linkType = linkType;
            ParseDataString(dataStringSplits);
        }

        public ChatLinkBaseWrapper(ChatLinkType linkType)
        {
            _linkType = linkType;
        }

        /// <summary>
        /// 解析链接所需数据的字符串，将其解析为各个字段
        /// </summary>
        /// <param name="dataStringSplits">根据ToDataString获取到的字符串分隔得到的字符串数组，也就是链接所需要的数据</param>
        protected abstract void ParseDataString(string[] dataStringSplits);

        /// <summary>
        /// 转成的字符串格式如下：
        /// [{0};{1};{2}] 每个数据用分号分隔，其中{0}的位置必须填LinkType转换成整型的字符串，后面可根据链接所用到的数据依次添加
        /// 注意：链接的描述信息不需要加到dataString中，不需加是由于在显示的消息中已经带了链接的内容
        /// </summary>
        /// <returns></returns>
        public abstract string ToDataString();

        public uint LinkId
        {
            get { return _linkId; }
            set { _linkId = value; }
        }

        public string LinkDesc
        {
            get 
            {
                if (string.IsNullOrEmpty(_linkDesc))
                {
                    LoggerHelper.Error("ChatLinkBaseWrapper LinkDesc is null or empty, please ensure that if you set this value !!!");
                }
                return _linkDesc; 
            }
            set { _linkDesc = value; }
        }

        public ChatLinkType LinkType
        {
            get { return _linkType; }
        }
    }
}
