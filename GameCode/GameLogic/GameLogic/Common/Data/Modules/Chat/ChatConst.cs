﻿using Common.Utils;
using System;
using System.Collections.Generic;

namespace Common.Data
{
    public class ChatConst
    {
        public static readonly string STR_DAY = MogoLanguageUtil.GetContent(32547);
        public static readonly string STR_HOUR = MogoLanguageUtil.GetContent(32548);
        public static readonly string STR_MINUTE = MogoLanguageUtil.GetContent(32549);
        public static readonly string STR_SECOND = MogoLanguageUtil.GetContent(32550);
        public static readonly string SEND_LINK_TIME_LENGTH_TEMPLATE = MogoLanguageUtil.GetContent(32551);
        public static readonly string CHAT_QUESTION_TITLE = MogoLanguageUtil.GetContent(123009);
        public static readonly string CHAT_QUESTION_END_TIPS = MogoLanguageUtil.GetContent(123011);

        public static readonly string IN_TEAM_TIPS_TEMPLATE = MogoLanguageUtil.GetContent(10131);
        public static readonly string FOLLOW_CAPTAIN = MogoLanguageUtil.GetContent(6314002);
        public static readonly string CANCEL_FOLLOW_CAPTAIN = MogoLanguageUtil.GetContent(6314003);
        public static readonly string CAPTAIN_CALL_ALL = MogoLanguageUtil.GetContent(6016019);
        public static readonly string GO_TO_AUTO_MATCH_TEAM_TIPS = MogoLanguageUtil.GetContent(10132);

        public static readonly string NOT_SEND_PRIVATE_MSG_TO_SELF = MogoLanguageUtil.GetContent(32555);

        public static readonly string ACCEPT_PLAYER_JOIN_TEAM = MogoLanguageUtil.GetContent(10135);

        public static readonly string CHAT_SYSTEM_TITLE_WORLD = MogoLanguageUtil.GetContent(32535);
        public static readonly string CHAT_SYSTEM_TITLE_VICINITY = MogoLanguageUtil.GetContent(32536);
        public static readonly string CHAT_SYSTEM_TITLE_TEAM = MogoLanguageUtil.GetContent(32538);
        public static readonly string CHAT_SYSTEM_TITLE_GUILD = MogoLanguageUtil.GetContent(32537);
        public static readonly string CHAT_SYSTEM_TITLE_PRIVATE = MogoLanguageUtil.GetContent(32539);
        public static readonly string CHAT_SYSTEM_TITLE_SYSTEM = MogoLanguageUtil.GetContent(32540);
        public static readonly string CHAT_SYSTEM_TITLE_TIPS = MogoLanguageUtil.GetContent(32541);
        public static readonly string CHAT_SYSTEM_TITLE_QUESTION = MogoLanguageUtil.GetContent(123008);
    }
}
