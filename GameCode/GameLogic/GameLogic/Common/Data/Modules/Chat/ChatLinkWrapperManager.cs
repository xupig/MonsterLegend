﻿using System;
using System.Collections.Generic;
using Common.Chat;
using UnityEngine.Events;

namespace Common.Data
{
    public class ChatLinkWrapperManager
    {
        private static ChatLinkWrapperManager _instance;
        public static ChatLinkWrapperManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ChatLinkWrapperManager();
                }
                return _instance;
            }
        }

        private Dictionary<ChatLinkType, Func<string[], ChatLinkBaseWrapper>> _linkWrapperCreatorDict;
        private List<ChatLinkType> _linkTypeList;

        private static readonly char[] DATA_STRING_TRIM_TAG = new char[] { '[', ']' };
        private static readonly char[] DATA_STRING_SPLIT_TAG = new char[] { ';' };

        protected ChatLinkWrapperManager()
        {
            _linkWrapperCreatorDict = new Dictionary<ChatLinkType, Func<string[], ChatLinkBaseWrapper>>();
            _linkTypeList = new List<ChatLinkType>();

            RegisterLinkWrapper(ChatLinkType.Item, ChatItemLinkWrapper.CreateLinkWrapper);
            RegisterLinkWrapper(ChatLinkType.Guild, ChatGuildLinkWrapper.CreateLinkWrapper);
            RegisterLinkWrapper(ChatLinkType.Team, ChatTeamLinkWrapper.CreateLinkWrapper);
            RegisterLinkWrapper(ChatLinkType.SharePosition, ChatSharePosLinkWrapper.CreateLinkWrapper);
            RegisterLinkWrapper(ChatLinkType.View, ChatViewLinkWrapper.CreateLinkWrapper);
            RegisterLinkWrapper(ChatLinkType.RedEnvelope, ChatRedEnvelopeWrapper.CreateLinkWrapper);
        }

        private void RegisterLinkWrapper(ChatLinkType linkType, Func<string[], ChatLinkBaseWrapper> createCallback)
        {
            if (!_linkWrapperCreatorDict.ContainsKey(linkType))
            {
                _linkWrapperCreatorDict.Add(linkType, createCallback);
            }
            if (!IsExsitChatLinkType(linkType))
            {
                _linkTypeList.Add(linkType);
            }
        }

        public ChatLinkBaseWrapper CreateWrapperFromDataString(string dataString)
        {
            string tmpDataString = dataString;
            tmpDataString = tmpDataString.Trim(DATA_STRING_TRIM_TAG);
            string[] dataSplits = tmpDataString.Split(DATA_STRING_SPLIT_TAG);
            if (dataSplits.Length > 0)
            {
                ChatLinkType linkType = (ChatLinkType)int.Parse(dataSplits[0]);
                return InternalCreateWrapper(linkType, dataSplits);
            }
            return null;
        }

        public bool IsExsitChatLinkType(ChatLinkType linkType)
        {
            for (int i = 0; i < _linkTypeList.Count; i++)
            {
                if (linkType == _linkTypeList[i])
                {
                    return true;
                }
            }
            return false;
        }

        private ChatLinkBaseWrapper InternalCreateWrapper(ChatLinkType linkType, string[] dataSplits)
        {
            ChatLinkBaseWrapper linkWrapper = null;
            if (_linkWrapperCreatorDict.ContainsKey(linkType))
            {
                linkWrapper = _linkWrapperCreatorDict[linkType](dataSplits);
            }
            return linkWrapper;
        }
    }
}
