﻿#region 模块信息
/*==========================================
// 文件名：ChatElementData
// 命名空间: Common.Data.Chat
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/10 14:00:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using MogoEngine.Events;

namespace Common.Data.Chat
{
    public class ChatPhraseData
    {
        private string[] _phraseArray;

        public ChatPhraseData()
        {
            ReadFromLocal();
        }

        public void ReadFromLocal()
        {
            if (_phraseArray == null)
            {
                _phraseArray = new string[6];
            }
            string[] phraseArrayCache = LocalCache.Read<string[]>(LocalName.ChatPhrase);
            if (phraseArrayCache == null || phraseArrayCache.Length == 0)
            {
                ResetPhrase();
            }
            else
            {
                for (int i = 0; i < phraseArrayCache.Length; i++)
                {
                    _phraseArray[i] = phraseArrayCache[i];
                }
            }
        }

        public void SetPhrase(int index, string phrase)
        {
            if(string.IsNullOrEmpty(phrase) ==  false && phrase != _phraseArray[index])
            {
                _phraseArray[index] = phrase;
            }
        }

        public void WriteToLocal()
        {
            string[] phraseArrayCache = new string[_phraseArray.Length];
            for (int i = 0; i < _phraseArray.Length; i++)
            {
                phraseArrayCache[i] = _phraseArray[i];
            }
            LocalCache.Write(LocalName.ChatPhrase, phraseArrayCache, CacheLevel.Permanent);
        }

        public string GetPhrase(int index)
        {
            return _phraseArray[index];
        }

        public void ResetPhrase()
        {
            _phraseArray[0] = MogoLanguageUtil.GetContent(32522);
            _phraseArray[1] = MogoLanguageUtil.GetContent(32523);
            _phraseArray[2] = MogoLanguageUtil.GetContent(32524);
            _phraseArray[3] = MogoLanguageUtil.GetContent(32525);
            _phraseArray[4] = MogoLanguageUtil.GetContent(32526);
            _phraseArray[5] = MogoLanguageUtil.GetContent(32527);
        }

    }
}
