﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class SpotlightData
    {
        public const int SHOW_TYPE_BOTTOM = 0;
        public const int SHOW_TYPE_TOP = 1;

        public string Content { get; set; }
        public int ShowTime { get; set; }
        public int ShowType { get; set; }

        public SpotlightData(string content, int showType, int showTime)
        {
            Content = content;
            ShowType = showType;
            ShowTime = showTime;
        }
    }
}
