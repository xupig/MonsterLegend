﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using MogoEngine.Events;
using System.Collections.Generic;

namespace Common.Data
{
    public enum ActivityState
    {
        HIDE = 1,   // 隐藏，不需要显示，完成前一个才能显示
        SHOW = 2,   // 显示，正在进行中
        COMPLETE = 3, //已经完成，可以领奖
        REWARDED = 4, //已经领奖
    }

    public class ChargeActivityItemData
    {
        private int activityId;
        public int ID
        {
            get { return activityId; }
        }

        private int icon;
        public int Icon
        {
            get { return icon; }
        }

        private int _showType;
        public BagItemType showModelType
        {
            get
            {
                switch (_showType)
                {
                    case 1:
                        return BagItemType.Wing;
                    case 2:
                        return BagItemType.PetCard;
                    case 3:
                        return BagItemType.Fashion;
                    case 4:
                        return BagItemType.Mount;
                }
                UnityEngine.Debug.LogError("新增了充值活动的展示道具类型");
                return BagItemType.All;
            }
        }

        private int _rewardId;
        public int RewardId
        {
            get
            {
                return _rewardId;
            }
        }

        private int _nameId;
        public string Name
        {
            get { return MogoLanguageUtil.GetContent(_nameId); }
        }

        private int _descId;
        public string Desc
        {
            get { return MogoLanguageUtil.GetContent(_descId); }
        }

        private ActivityState _state = ActivityState.HIDE;
        public ActivityState State
        {
            get { return _state; }
            set { _state = (ActivityState)value; }
        }

        public ChargeActivityItemData(charge_activity cfg)
        {
            _showType = cfg.__show_type;
            activityId = cfg.__id;
            _nameId = cfg.__name;
            _descId = cfg.__desc;
            _rewardId = cfg.__reward_id;
            _state = ActivityState.HIDE;
            icon = cfg.__icon;
        }
    }

    public class ChargeActivityData
    {
        private Dictionary<int, ChargeActivityItemData> chargeActivityItemDict;

        public ChargeActivityData()
        {
            chargeActivityItemDict = new Dictionary<int, ChargeActivityItemData>();
            foreach (KeyValuePair<int, charge_activity> pair in XMLManager.charge_activity)
            {
                chargeActivityItemDict.Add(pair.Value.__id, new ChargeActivityItemData(pair.Value));
            }
        }

        /// <summary>
        /// 从服务器端获得数据
        /// </summary>
        public void UpdateActivityData(PbChargeActivityQueryList data)
        {
            for (int i = 0; i < data.act_list.Count; i++)
            {
                PbChargeActivityTypeOne activityData = data.act_list[i];
                if (chargeActivityItemDict.ContainsKey(activityData.act_id))
                {
                    chargeActivityItemDict[activityData.act_id].State = GetStateOfActivity(activityData.status);
                }
            }
            EventDispatcher.TriggerEvent(ChargeActivityEvents.RefreshChargeContent);
        }

        public void UpdateSingleActivityData(PbChargeActivityTypeOne activityData)
        {
            if (chargeActivityItemDict.ContainsKey(activityData.act_id))
            {
                chargeActivityItemDict[activityData.act_id].State = GetStateOfActivity(activityData.status);
            }
            EventDispatcher.TriggerEvent(ChargeActivityEvents.RefreshChargeContent);
        }

        private ActivityState GetStateOfActivity(int statusId)
        {
            switch (statusId)
            {
                case public_config.TASK_STATE_ACCEPT:
                    return ActivityState.SHOW;
                case public_config.TASK_STATE_ACCOMPLISH:
                    return ActivityState.COMPLETE;
                case public_config.TASK_STATE_REWARDED:
                    return ActivityState.REWARDED;
            }
            return ActivityState.HIDE;
        }

        public List<ChargeActivityItemData> GetShowActivityNameList()
        {
            List<ChargeActivityItemData> result = new List<ChargeActivityItemData>();
            foreach (var i in chargeActivityItemDict.Keys)
            {
                if (chargeActivityItemDict[i].State != ActivityState.HIDE)
                {
                    result.Add(chargeActivityItemDict[i]);
                }
            }
            return result;
        }

        public bool CanGetReward()
        {
            foreach (var i in chargeActivityItemDict.Keys)
            {
                if (chargeActivityItemDict[i].State == ActivityState.COMPLETE)
                {
                    return true;
                }
            }
            return false;
        }

        
    }
}
