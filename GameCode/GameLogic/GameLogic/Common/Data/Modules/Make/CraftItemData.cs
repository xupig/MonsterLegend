﻿#region 模块信息
/*==========================================
// 文件名：CraftItemData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Make
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 10:01:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
namespace Common.Data
{
    public class CraftItemData
    {
        public int id;
        public int iconId;
        public int nameId;
        public int descId;
        // 所需玩家等级
        public int needPlayerLevel;
        // 制造的产品ID
        public int productId;
        // 制造出产品适合的职业
        public int vocation;
        // 所需玩家天赋
        public int needGift;
        // 配方类型
        public int craftType;
        // 配方制造的产品所用于的部位
        public int productType;
        // 配方制造的产品等级
        public List<int> productLevelList;
        // 是否绑定， 1 代表可交易 2 不可交易
        public int bindNum;
        // 配方分组
        public int groupId;
        // 制造时间间隔
        public int coolDownTime;
        // 是否为稀有配方
        public bool isRare;
        // CD 时间
        private int cdTimeStamp;
        public int CDTimeStamp
        {
            get { return cdTimeStamp; }
        }

        private int minProductLevel;
        public int MinProductLevel
        {
            get { return minProductLevel; }
        }

        private int maxProductLevel;
        public int MaxProductLevel
        {
            get { return maxProductLevel; }
        }

        public bool NeedLearn;

        public CraftItemData(mfg_formula cfg)
        {
            id = cfg.__id;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(cfg.__product);
            productId = dataDict.ContainsKey("1") ? int.Parse(dataDict["1"]) : 0;
            nameId = cfg.__name;
            iconId = cfg.__icon;
            descId = cfg.__desc;
            craftType = cfg.__product_type;
            needPlayerLevel = cfg.__player_level_need;
            productType = cfg.__product_subtype;
            needGift = cfg.__talent_condition;
            groupId = cfg.__product_group;
            productLevelList = data_parse_helper.ParseListInt(cfg.__product_level);
            productLevelList.Sort();
            minProductLevel = productLevelList.Count > 1 ? productLevelList[0] : 0;
            maxProductLevel = productLevelList.Count > 1 ? productLevelList[productLevelList.Count - 1] : 0;
            coolDownTime = cfg.__cd_time;
            vocation = cfg.__product_vocation;
            isRare = cfg.__is_rare == 1;
            bindNum = cfg.__is_unbind;
            NeedLearn = !string.IsNullOrEmpty(cfg.__learn_cost);
        }

        private List<int> ConvertStringListToIntList(List<string> list)
        {
            List<int> result = new List<int>();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    result.Add(int.Parse(list[i]));
                }
            }
            result.Sort();
            return result;
        }

        /// <summary>
        /// 制造配方名称
        /// </summary>
        public string Name
        {
            get
            {
                if (nameId != 0)
                {
                    return MogoLanguageUtil.GetContent(nameId);
                }
                else
                {
                    return item_helper.GetName(productId);
                }
            }
        }

        /// <summary>
        /// 制造配方的描述
        /// </summary>
        public string Describe
        {
            get
            {
                return string.Empty;
            }
        }

        public string DescribeInTipPanel
        {
            get
            {
                if (descId != 0)
                {
                    return MogoLanguageUtil.GetContent(descId);
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// 配方是否解锁
        /// </summary>
        public bool isLocked
        {
            get { return PlayerAvatar.Player.level < needPlayerLevel; }
        }

        public void SetTimeStamp(int cdTime)
        {
            cdTimeStamp = (int)Global.Global.serverTimeStampSecond + cdTime;
        }
    }
}