﻿#region 模块信息
/*==========================================
// 文件名：MakeGiftItemData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Make
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/28 16:29:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
namespace Common.Data
{
    public class MakeGiftItemData
    {
        private mfg_talent cfg;
        public int id;
        public string name;
        public string desc;
        public int icon;
        public string learnConditionDesc;
        public bool isAchieved
        {
            get
            {
                return GetGiftState();
            }
        }

        public MakeGiftItemData(mfg_talent cfg)
        {
            this.cfg = cfg;
            id = cfg.__id;
            name = MogoLanguageUtil.GetContent(cfg.__name);
            desc = MogoLanguageUtil.GetContent(cfg.__desc);
            icon = cfg.__icon;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(cfg.__condition);
            learnConditionDesc = player_condition_helper.GetConditionDescribe(dataDict);
        }

        public bool canLearn
        {
            get
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(cfg.__condition);
                return player_condition_helper.JudgePlayerCondition(dataDict);
            }
        }

        // 天赋是否已经拥有由PlayerAvatar上的mfg_talent决定
        // mfg_talent使用位来表示，若对应ID位上的数字为1，则表示已经拥有该天赋
        // 否则未拥有该天赋
        private bool GetGiftState()
        {
            return (((PlayerAvatar.Player.mfg_talent >> (id - 1))) & 0x1) > 0;
        }

        public int GetOwnedGiftNum()
        {
            int talent = PlayerAvatar.Player.mfg_talent;
            int num = 0;
            while (talent > 0)
            {
                if ((talent & 0x1) > 0)
                {
                    num++;
                }
                talent >>= 1;
            }
            return num;
        }
    }
}