﻿#region 模块信息
/*==========================================
// 文件名：MakeData
// 命名空间: GameLogic.GameLogic.Common.Data.Modules.Make
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/28 10:24:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;

namespace Common.Data
{
    public enum FilterType
    {
        Vocation = 0,
        Level = 1,
        Trade = 2
    }

    public class FilterCondition
    {
        private FilterType type;
        private int value;

        public FilterType Type
        {
            get { return type; }
        }

        public int Value
        {
            get { return value; }
        }

        public FilterCondition(int value, FilterType type)
        {
            this.value = value;
            this.type = type;
        }

        public string Name
        {
            get
            {
                if (type == FilterType.Vocation)
                {
                    if (value == -1)
                    {
                        return MogoLanguageUtil.GetContent(6050059);
                    }
                    return MogoLanguageUtil.GetContent(value);
                }
                else if (type == FilterType.Level)
                {
                    if (value == -1)
                    {
                        return MogoLanguageUtil.GetContent(6050059);
                    }
                    return string.Format(MogoLanguageUtil.GetContent(56232), value);
                }
                else if (type == FilterType.Trade)
                {
                    if (value == -1)
                    {
                        return MogoLanguageUtil.GetContent(6050059);
                    }
                    else if (value == 0)
                    {
                        return MogoLanguageUtil.GetContent(5080);
                    }
                    else
                    {
                        return MogoLanguageUtil.GetContent(5079);

                    }
                }
                return string.Empty;
            }
        }

        public static FilterCondition GetEmptyFilterCondition(FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.Vocation:
                    return new FilterCondition(-1, FilterType.Vocation);
                case FilterType.Level:
                    return new FilterCondition(-1, FilterType.Level);
                case FilterType.Trade:
                    return new FilterCondition(-1, FilterType.Trade);
            }
            return null;
        }
    }

    public class MakeData
    {
        public Dictionary<int, HashSet<int>> newFormulaDict;
        public HashSet<int> learnDict;
        public Dictionary<int, MakeGiftItemData> _giftDict;
        // 如果在该列表中有数据，则表示制造有CD时间，记录到期时间戳，若为0，则表示无CD时间
        public Dictionary<int, DateTime> _cdDict;

        public bool hasRequestData;
        public bool hasDataResponsed;

        public Dictionary<int, CraftItemData> craftDict;

        private Dictionary<FilterType, FilterCondition> _filterCondition = new Dictionary<FilterType, FilterCondition>();

        public void AddFilterCondition(FilterCondition filterData)
        {
            if (filterData == null)
            {
                return;
            }
            if (_filterCondition.ContainsKey(filterData.Type))
            {
                _filterCondition[filterData.Type] = filterData;
            }
            else
            {
                _filterCondition.Add(filterData.Type, filterData);
            }
        }

        public void RemoveFilterCondition(FilterType type)
        {
            if (_filterCondition.ContainsKey(type))
            {
                _filterCondition.Remove(type);
            }
        }

        public void RemoveAllFilterConditions()
        {
            _filterCondition.Clear();
        }

        public MakeData()
        {
            _cdDict = new Dictionary<int, DateTime>();
            craftDict = new Dictionary<int, CraftItemData>();
            newFormulaDict = new Dictionary<int, HashSet<int>>();
            learnDict = new HashSet<int>();
            InitGiftData();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                AddItemToCraftDict(pair.Value);
            }
        }

        private void InitGiftData()
        {
            _giftDict = new Dictionary<int, MakeGiftItemData>();
            foreach (KeyValuePair<int, mfg_talent> pair in XMLManager.mfg_talent)
            {
                _giftDict.Add(pair.Key, new MakeGiftItemData(pair.Value));
            }
        }

        private void AddItemToCraftDict(mfg_formula cfg)
        {
            if (craftDict.ContainsKey(cfg.__id) == false)
            {
                craftDict.Add(cfg.__id, new CraftItemData(cfg));
            }
            else
            {
                craftDict[cfg.__id] = new CraftItemData(cfg);
            }
        }

        /// <summary>
        /// key : 制造产品的类型
        /// value : 制造产品类型中包含的分组
        /// </summary>
        private Dictionary<int, HashSet<int>> _typeGroupDict;
        
        /// <summary>
        /// key : 制造产品中的分组
        /// value : 制造分组中对应的配方
        /// </summary>
        private Dictionary<int, List<CraftItemData>> _suitGroupDict;
        public List<int> GetSuitGroupList(int type)
        {
            if (_typeGroupDict == null)
            {
                InitGroupDict();
            }
            if (_suitGroupDict == null)
            {
                InitSuitGroupDict();
            }
            if (_filterCondition == null || _filterCondition.Count == 0)
            {
                if (_typeGroupDict.ContainsKey(type))
                {
                    return new List<int>(_typeGroupDict[type]);
                }
                return new List<int>();
            }
            else
            {
                return GetFilterGroups(type);
            }
        }

        private void InitGroupDict()
        {
            _typeGroupDict = new Dictionary<int, HashSet<int>>();
            _suitGroupDict = new Dictionary<int, List<CraftItemData>>();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                // 初始化TypeGroupDict
                int productType = pair.Value.__product_type;
                if (_typeGroupDict.ContainsKey(productType) == false)
                {
                    _typeGroupDict.Add(productType, new HashSet<int>());
                }
                _typeGroupDict[productType].Add(pair.Value.__product_group);
                
                // 初始化SuitGroupDict
                int productGroup = pair.Value.__product_group;
                if (_suitGroupDict.ContainsKey(productGroup) == false)
                {
                    _suitGroupDict.Add(productGroup, new List<CraftItemData>());
                }
                _suitGroupDict[productGroup].Add(new CraftItemData(pair.Value));
            }
        }

        public List<int> GetFilterGroups(int type)
        {
            List<int> result = new List<int>();
            if(_typeGroupDict.ContainsKey(type) == false)
            {
                UnityEngine.Debug.LogError("配置表中不包含产品类别为" + type + "的产品");
                return new List<int>();
            }
            HashSet<int> groupHashSet = _typeGroupDict[type];
            foreach (int groupId in groupHashSet)
            {
                if (_suitGroupDict.ContainsKey(groupId))
                {
                    List<CraftItemData> itemList = _suitGroupDict[groupId];
                    for (int i = itemList.Count - 1; i >= 0; i--)
                    {
                        if (CheckMeetTheFilter(itemList[i]) == true)
                        {
                            result.Add(groupId);
                            break;
                        }
                    }
                }
            }
            return result;
        }

        private bool CheckMeetTheFilter(CraftItemData craftItemData)
        {
            bool result = true;
            if (_filterCondition.Count == 0)
            {
                return true;
            }
            foreach (var pair in _filterCondition)
            {
                switch (pair.Key)
                {
                    case FilterType.Level:
                        if (craftItemData.productLevelList != null && craftItemData.productLevelList.Count > 0)
                        {
                            result = (result && craftItemData.productLevelList.Contains(pair.Value.Value));
                        }
                        break;
                    case FilterType.Vocation:
                        int vocation = pair.Value.Value;
                        if (craftItemData.vocation != 0)
                        {
                            result = (result && (craftItemData.vocation == vocation));
                        }
                        break;
                    case FilterType.Trade:
                        int tradeType = pair.Value.Value;
                        result = (result && tradeType == craftItemData.bindNum);
                        break;
                }
            }
            return result;
        }

        public List<CraftItemData> GetCraftItemByGroup(int groupId)
        {
            if (_suitGroupDict == null)
            {
                UnityEngine.Debug.LogError("MakeData 中 SuitGroupDict 为空");
                return null;
            }
            if (_suitGroupDict.ContainsKey(groupId))
            {
                return GetFilterData(_suitGroupDict[groupId]);
            }
            return null;
        }

        private List<CraftItemData> GetFilterData(List<CraftItemData> list)
        {
            List<CraftItemData> result = new List<CraftItemData>();
            for (int i = 0; i < list.Count; i++)
            {
                if (CheckMeetTheFilter(list[i]))
                {
                    result.Add(list[i]);
                }
            }
            return result;
        }

        private void InitSuitGroupDict()
        {
        }

        public bool HasOpenCraftGift(int craftId)
        {
            int giftId = mfg_formula_helper.GetProductType(craftId);
            if (giftId == 0)
            {
                return true;
            }
            if (giftId != 0 && _giftDict.ContainsKey(giftId))
            {
                return _giftDict[giftId].isAchieved;
            }
            return false;
        }

        public CraftItemData GetCraftItemData(int craftId)
        {
            if (craftDict.ContainsKey(craftId))
            {
                return craftDict[craftId];
            }
            return null;
        }

        public void AddLearnItem(PbMfgFormulaInfo info)
        {
            learnDict.Add((int)info.formula_id);
        }

        public bool IsFormulaLearned(int formulaId)
        {
            return learnDict.Contains(formulaId);
        }

        public List<MakeGiftItemData> GetAllMakeGiftItemData()
        {
            List<MakeGiftItemData> result = new List<MakeGiftItemData>();
            foreach (var pair in _giftDict)
            {
                result.Add(pair.Value);
            }
            result.Sort(ComparisonFunction);
            return result;
        }

        private int ComparisonFunction(MakeGiftItemData x, MakeGiftItemData y)
        {
            return x.id - y.id;
        }


        public List<int> GetOpenCraftGiftList()
        {
            List<int> result = new List<int>();
            int talent = PlayerAvatar.Player.mfg_talent;
            int giftBit = 1;
            while (talent > 0)
            {
                if (talent % 2 == 1)
                {
                    result.Add(giftBit);
                }
                talent /= 2;
                giftBit++;
            }
            return result;
        }

        public void InitLearnList(List<PbMfgFormulaInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                learnDict.Add((int)list[i].formula_id);
            }
        }

        public void InitCDList(List<PbMfgFormulaInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (_cdDict.ContainsKey((int)list[i].formula_id) == false)
                {
                    _cdDict.Add((int)list[i].formula_id, DateTime.Now.AddSeconds((int)list[i].cd));
                }
                else
                {
                    _cdDict[(int)list[i].formula_id] = DateTime.Now.AddSeconds((int)list[i].cd);
                }
            }
        }

        public void UpdateSingleCDTime(PbMfgFormulaInfo info)
        {
            if (_cdDict.ContainsKey((int)info.formula_id))
            {
                _cdDict[(int)info.formula_id] = DateTime.Now.AddSeconds((int)info.cd);
            }
            else
            {
                _cdDict.Add((int)info.formula_id, DateTime.Now.AddSeconds((int)info.cd));
            }
        }

        public DateTime GetFormulaCDTime(int craftId)
        {
            if (_cdDict.ContainsKey(craftId))
            {
                return _cdDict[craftId];
            }
            return DateTime.Now;
        }

        public bool hasOpenOneMakeGift()
        {
            foreach (KeyValuePair<int, MakeGiftItemData> pair in _giftDict)
            {
                if (pair.Value.isAchieved == true)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
