﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public enum BagItemType
    {
        /// <summary>
        /// 全部类型
        /// </summary>
        All = 0,

        /// <summary>
        /// 普通道具
        /// </summary>
        Common = 1,
        
        /// <summary>
        /// 装备
        /// </summary>
        Equip = 2,
        
        /// <summary>
        /// 宝石
        /// </summary>
        Gem = 3,
        
        /// <summary>
        /// 符文
        /// </summary>
        Rune = 4,
        
        /// <summary>
        /// 附魔卷轴
        /// </summary>
        EnchantScroll = 5,
        
        /// <summary>
        /// 货币
        /// </summary>
        Currency = 6,
        
        /// <summary>
        /// 材料
        /// </summary>
        Material = 11,
        
        /// <summary>
        /// 幸运石
        /// </summary>
        LuckStone = 12,

        /// <summary>
        /// Buff道具
        /// </summary>
        BuffItem = 13, 

        /// <summary>
        /// 套装卷轴
        /// </summary>
        SuitScroll = 14,

        /// <summary>
        /// 建筑图纸
        /// </summary>
        BuildingPaper = 15,

        /// <summary>
        /// 代币
        /// </summary>
        Token = 16,

        /// <summary>
        /// 亲密值道具
        /// </summary>
        Intimate = 17,

        // 优惠券
        DiscountTicket = 18,

        // 装备宝盒
        EquipBox = 19,

        // 时装
        Fashion = 20,

        // 翅膀
        Wing = 21,

        // 附魔制造配方
        EnchantPaper = 23,

        // 套装制造配方
        SuitPaper = 24,

        // 装备制造配方
        EquipRecipe = 25,

        // 魔灵整卡
        PetCard = 26, 
        
        // 魔灵突破石
        PetStone = 27,
        
        // 魔灵碎片（升星）
        PetFragment = 28,

        // 魔灵经验药水
        PetExperience = 29,

        // 魔灵类型精华
        PetCream = 30,

        // 野外BOSS掉落A宝箱
        BossDropBoxA = 31,

        // 野外BOSS掉落B宝箱
        BossDropBoxB = 32,

        // 坐骑道具类型
        Mount = 33,

        // 藏宝图
        TreasureMap = 34,
    }
}
