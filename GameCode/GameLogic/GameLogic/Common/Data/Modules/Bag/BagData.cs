﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Structs.ProtoBuf;
using Common.Structs.Enum;

namespace Common.Data
{
    /// <summary>
    /// 具体类型背包的数据结构
    /// </summary>
    public class BagData
    {
        //private BagType _bagType;

        /// <summary>
        /// Key为格子位置编号，编号从1开始
        /// Value格子上物品数据
        /// </summary>
        private Dictionary<int, BaseItemInfo> _itemInfoDict = new Dictionary<int,BaseItemInfo>();
        public Dictionary<int,HashSet<int>> bagRuneCanEquipedSet;
        public BagData(BagType type)
        {
            if(type == BagType.RuneBag)
            {
                bagRuneCanEquipedSet = new Dictionary<int, HashSet<int>>();
            }
            //_bagType = type;
        }

        public bool CanEquiped(int page,int position)
        {
            return bagRuneCanEquipedSet != null && bagRuneCanEquipedSet.ContainsKey(page) && bagRuneCanEquipedSet[page].Contains(position);
        }

        /// <summary>
        /// 填充背包全部数据
        /// </summary>
        /// <param name="data"></param>
        public void Fill(PbBagList data)
        {
            _itemInfoDict.Clear();
            for(int i = 0; i < data.items.Count; i++)
            {
                _itemInfoDict.Add((int)data.items[i].grid_index, ItemInfoCreator.CreateItemInfo(data.items[i]));
            }
        }

        public bool UpdateItem(PbGridItem pbGridItem)
        {
            BaseItemInfo itemInfo = ItemInfoCreator.CreateItemInfo(pbGridItem);
            int position = (int)pbGridItem.grid_index;
            bool isNew = true;
            if(_itemInfoDict.ContainsKey(position) == true)
            {
                isNew = false;
                _itemInfoDict.Remove(position);
            }
            if(itemInfo != null)
            {
                _itemInfoDict.Add(position, itemInfo);
            }
            return isNew;
        }

        public BaseItemInfo GetItemInfo(int position)
        {
            if(_itemInfoDict.ContainsKey(position) == true)
            {
                return _itemInfoDict[position];
            }
            return null;
        }

        public List<BaseItemInfo> GetTypeItemInfo(BagItemType itemType)
        {
            List<BaseItemInfo> itemInfoList = new List<BaseItemInfo>();
            foreach(var pair in _itemInfoDict)
            {
                if(pair.Value.Type == itemType)
                {
                    itemInfoList.Add(pair.Value);
                }
            }
            return itemInfoList;
        }

        public List<BaseItemInfo> GetItemInfoList(Func<BaseItemInfo, bool> selector)
        {
            List<BaseItemInfo> result = new List<BaseItemInfo>();
            foreach(KeyValuePair<int, BaseItemInfo> pair in _itemInfoDict)
            {
                if(selector(pair.Value) == true)
                {
                    result.Add(pair.Value);
                }
            }
            return result;
        }

        public Dictionary<int, BaseItemInfo> GetAllItemInfo()
        {
            Dictionary<int, BaseItemInfo> result = new Dictionary<int, BaseItemInfo>();
            foreach(KeyValuePair<int, BaseItemInfo> kvp in _itemInfoDict)
            {
                result.Add(kvp.Key, kvp.Value);
            }
            return result;
        }

        public bool NeedSort()
        {
            return true;
        }

        public int GetItemNum(int itemId)
        {
            int count = 0;
            foreach(KeyValuePair<int, BaseItemInfo> kvp in _itemInfoDict)
            {
                if(kvp.Value != null && kvp.Value.Id == itemId)
                {
                    count += kvp.Value.StackCount;
                }
            }
            return count;
        }

        public int GetItemPosition(int itemId)
        {
            foreach(KeyValuePair<int, BaseItemInfo> kvp in _itemInfoDict)
            {
                if(kvp.Value.Id == itemId)
                {
                    return kvp.Key;
                }
            }
            return 0;
        }

        public bool HasItem(int itemId)
        {
            return GetItemNum(itemId) > 0;
        }

        public int GetAllItemCount()
        {
            return _itemInfoDict.Keys.Count;
        }

    }
}
