﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameData;

namespace Common.Data
{
    public static class ItemInfoCreator
    {
        public static BaseItemInfo CreateItemInfo(PbGridItem gridItem)
        {
            //格子变空操作后返回协议中PbGridItem的item字段为空
            if(gridItem.item == null)
            {
                return null;
            }
            BagItemType itemType = item_helper.GetItemType((int)gridItem.item.id);
            switch(itemType)
            {
                case BagItemType.Equip:
                    return new EquipItemInfo(gridItem);
                case BagItemType.Gem:
                    return new GemItemInfo(gridItem);
                case BagItemType.Rune:
                    return new RuneItemInfo(gridItem);
                default:
                    return new CommonItemInfo(gridItem);
            }
        }

        public static BaseItemInfo CreateItemInfo(PbItemInfo pbItemInfo)
        {
            PbGridItem dummy = new PbGridItem();
            dummy.grid_index = 1;
            dummy.item = pbItemInfo;
            return CreateItemInfo(dummy);
        }

        public static BaseItemInfo CreateItemInfo(int itemId, int count)
        {
            PbItemInfo pbItemInfo = new PbItemInfo();
            pbItemInfo.id = (uint)itemId;
            pbItemInfo.count = (uint)count;
            return CreateItemInfo(pbItemInfo);
        }
    }
}
