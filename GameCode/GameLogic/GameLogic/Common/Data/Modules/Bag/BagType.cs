﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    /// <summary>
    /// 背包类型定义，值和public_config中相关字段值相同
    /// 作用是为了隔离前后端的概念
    /// </summary>
    public enum BagType
    {
        Undefined = -1,
        /// <summary>
        /// 物品背包
        /// </summary>
        ItemBag = 1,
        /// <summary>
        /// 身上物品背包
        /// </summary>
        BodyEquipBag = 2,
        /// <summary>
        /// 赎回仓库
        /// </summary>
        RedeemBag = 3,
        /// <summary>
        /// 符文背包
        /// </summary>
        RuneBag = 4,
        /// <summary>
        /// 身上符文背包
        /// </summary>
        BodyRuneBag = 5,

    }
}
