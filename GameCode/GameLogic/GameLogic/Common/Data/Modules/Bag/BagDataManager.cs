
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using Common.Structs.Enum;
using GameData;

namespace Common.Data
{
    public class BagDataManager
    {
        public static int MaxItemNum
        {
            get
            {
                return int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.bag_number_all));
            }
        }

        /// <summary>
        /// Key为背包类型
        /// Value为具体类型背包的数据
        /// </summary>
        private Dictionary<BagType, BagData> _bagDataDict;

        public BagDataManager()
        {
            _bagDataDict = new Dictionary<BagType, BagData>();
            AddBagData(BagType.ItemBag);
            AddBagData(BagType.BodyEquipBag);
            AddBagData(BagType.BodyRuneBag);
            AddBagData(BagType.RuneBag);
            AddBagData(BagType.RedeemBag);
        }

        private void AddBagData(BagType type)
        {
            _bagDataDict.Add(type, new BagData(type));
        }

        public BagData GetBagData(BagType type)
        {
            return _bagDataDict[type];
        }

        public BagData GetItemBagData()
        {
            return GetBagData(BagType.ItemBag);
        }

        public BagData GetRuneBagData( )
        {
            return GetBagData( BagType.RuneBag );
        }

        public BagData GetBodyRuneBagData( )
        {
            return GetBagData( BagType.BodyRuneBag );
        }

        public BagData GetBodyEquipBagData( )
        {
            return GetBagData( BagType.BodyEquipBag );
        }

        public BagData GetRedeemBagData( )
        {
            return GetBagData( BagType.RedeemBag );
        }

        public EquipItemInfo GetEquipedItem(EquipType type)
        {
            return GetBodyEquipBagData().GetItemInfo((int)type) as EquipItemInfo;
        }

        public EquipItemInfo GetEquipedItem(int index)
        {
            return GetBodyEquipBagData().GetItemInfo(index) as EquipItemInfo;
        }

        public int GetBodyEquipPosition(EquipItemInfo info)
        {
            BagData bodyEquipData = GetBodyEquipBagData();
            if (info.SubType == EquipType.ring)
            {
                if (bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) == null)
                {
                    return public_config.EQUIP_POS_LEFTRING;
                }
                if (bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) == null)
                {
                    return public_config.EQUIP_POS_RIGHTRING;
                }
                EquipItemInfo leftRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) as EquipItemInfo;
                EquipItemInfo rightRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
                return leftRing.FightForce < rightRing.FightForce ? public_config.EQUIP_POS_LEFTRING : public_config.EQUIP_POS_RIGHTRING;
            }
            return (int)info.SubType;
        }

        private EquipItemInfo GetLeftEquipData(EquipItemInfo info)
        {
            BagData bodyData = GetBodyEquipBagData();
            EquipItemInfo equipInfo = bodyData.GetItemInfo((int)info.SubType) as EquipItemInfo;
            if (equipInfo == null && info.SubType == EquipType.ring)
            {
                equipInfo = bodyData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
            }
            if (equipInfo == null)
            {
                return null;
            }
            return equipInfo;
        }

        public bool IsBodyEquipPositionOccupied(EquipItemInfo info)
        {
            BagData bodyEquipData = GetBodyEquipBagData();
            if (info.SubType == EquipType.ring)
            {
                return bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) != null && bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) != null;
            }
            return bodyEquipData.GetItemInfo((int)info.SubType) != null;
        }
    }
}
