﻿using System;
using System.Collections.Generic;
using System.Text;

using GameLoader.Utils;
using UnityEngine;
using GameData;

namespace Common.Data
{
    public class BufferData
    {
        public int buffID;
        public int totalTime;
        public int delayTime;
        public bool showInClient;//(不用)
        public int showPriority;//(不用)
        public int removeMode;
        public int replaceType;//(不用)
        public int group;
        public int groupLevel;
        public int special_hitfx_id;
        public List<int> excludeBuffsGroup;
        public List<int> replaceBuffsGroup;
        public List<int> excludeBuffs;
        public List<int> replaceBuffs;
        public List<int> conditionBuffs;
        public List<int> createBuffs;
        public List<int> createRemoveModes;
        public List<int> createTimeModes;
        public List<int> conditionBuffsGroup;//(不用)
        public List<int> createBuffsGroup;
        public List<int> createRemoveModesGroup;
        public List<int> createTimeModesGroup;
        public List<BufferEffectData> startEffect;
        public List<BufferEffectData> endEffect;
        public List<BufferEffectData> updateEffect;
        public int eventCondition1;//(不用)
        public List<BufferEffectData> eventEffect1;//(不用)
        public int eventCondition2;//(不用)
        public List<BufferEffectData> eventEffect2;//(不用)
        public int eventCondition3;//(不用)
        public List<BufferEffectData> eventEffect3;//(不用)
        public List<SkillEventData> skillEvents;//(不用)
        public List<int> buffVisualFXs;
        public Dictionary<int, BufferCreateData> conditionBuffsMap = new Dictionary<int,BufferCreateData>();
        public Dictionary<int, BufferCreateData> conditionBuffsGroupMap = new Dictionary<int, BufferCreateData>();//(不用)

        public BufferData(int bufferID)
        {
            if (!GameData.XMLManager.buff.ContainsKey(bufferID))
            {
                LoggerHelper.Error("BufferData Key Error:" + bufferID);
            }
            var data = GameData.XMLManager.buff[bufferID];
            buffID = bufferID;
            totalTime = data.__total_time;
            delayTime = data.__delay_time;
            showInClient = data.__show == 1;
            showPriority = data.__show_priority;
            removeMode = data.__remove_mode;
            replaceType = data.__replace_type;
            group = data.__group;
            groupLevel = data.__group_level;
            special_hitfx_id = data.__special_hitfx_id;
            excludeBuffsGroup = data_parse_helper.ParseListInt(data.__exclude_buffs_group);
            replaceBuffsGroup = data_parse_helper.ParseListInt(data.__replace_buffs_group);
            excludeBuffs = data_parse_helper.ParseListInt(data.__exclude_buffs);
            replaceBuffs = data_parse_helper.ParseListInt(data.__replace_buffs);
            
            conditionBuffs = data_parse_helper.ParseListInt(data.__condition_buffs);
            createBuffs = data_parse_helper.ParseListInt(data.__create_buffs);
            createRemoveModes = data_parse_helper.ParseListInt(data.__create_remove_modes);
            createTimeModes = data_parse_helper.ParseListInt(data.__create_time_modes);
            
            conditionBuffsGroup = data_parse_helper.ParseListInt(data.__condition_buffs_group);
            createBuffsGroup = data_parse_helper.ParseListInt(data.__create_buffs_group);
            createRemoveModesGroup = data_parse_helper.ParseListInt(data.__create_remove_modes_group);
            createTimeModesGroup = data_parse_helper.ParseListInt(data.__create_time_modes_group);

            eventCondition1 = data.__event_condition_1;
            eventCondition2 = data.__event_condition_2;
            eventCondition3 = data.__event_condition_3;

            startEffect = CombatDataTools.ParseBufferEffect(data.__start_effect);
            endEffect = CombatDataTools.ParseBufferEffect(data.__end_effect);
            updateEffect = CombatDataTools.ParseBufferEffect(data.__update_effect);
            eventEffect1 = CombatDataTools.ParseBufferEffect(data.__event_effect_1);
            eventEffect2 = CombatDataTools.ParseBufferEffect(data.__event_effect_2);
            eventEffect3 = CombatDataTools.ParseBufferEffect(data.__event_effect_3);
            skillEvents = CombatDataTools.ParseSkillEvents(data.__effect_events);
            buffVisualFXs = data_parse_helper.ParseListInt(data.__fx);

            InitConditionBuffsMap();
            InitConditionBuffsGroupMap();

        }

        void InitConditionBuffsMap()
        {
            if (createBuffs.Count == 0) return;
            var data = GameData.XMLManager.buff[buffID];
            for (int i = 0; i < conditionBuffs.Count; i++)
            {
                int conditionBufferID = conditionBuffs[i];
                var bufferCreateData = new BufferCreateData();
                bufferCreateData.bufferID = createBuffs.Count >= i + 1
                    ? createBuffs[i] : createBuffs[createBuffs.Count - 1];
                bufferCreateData.createRemoveMode = createRemoveModes.Count == 0 
                    ? 1 : (createRemoveModes.Count >= i + 1 ? createRemoveModes[i] : createRemoveModes[createRemoveModes.Count - 1]);
                bufferCreateData.createTimeMode = createTimeModes.Count == 0
                    ? 1 : (createTimeModes.Count >= i + 1 ? createTimeModes[i] : createTimeModes[createTimeModes.Count - 1]);
                conditionBuffsMap[conditionBufferID] = bufferCreateData;
            }
        }

        void InitConditionBuffsGroupMap()
        {
            if (createBuffsGroup.Count == 0) return;
            for (int i = 0; i < conditionBuffs.Count; i++)
            {
                int conditionBufferID = conditionBuffs[i];
                var bufferCreateData = new BufferCreateData();
                bufferCreateData.bufferID = createBuffsGroup.Count >= i + 1 ? createBuffsGroup[i] : createBuffsGroup[createBuffsGroup.Count - 1];
                bufferCreateData.createRemoveMode = createRemoveModesGroup.Count == 0 ? 1
                    : (createRemoveModesGroup.Count >= i + 1 ? createRemoveModesGroup[i] : createRemoveModesGroup[createRemoveModesGroup.Count - 1]);
                bufferCreateData.createTimeMode = createTimeModesGroup.Count == 0 ? 1
                    : (createTimeModesGroup.Count >= i + 1 ? createTimeModesGroup[i] : createTimeModesGroup[createTimeModesGroup.Count - 1]);
                conditionBuffsMap[conditionBufferID] = bufferCreateData;
            }
        }
    }
}
