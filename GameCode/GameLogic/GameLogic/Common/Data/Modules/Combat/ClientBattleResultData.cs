﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class ClientBattleResultData
    {
        public int winFlag { get; set; } //BattleResult.WIN;
        public float bloodRate { get; set; } 
        public float passTime { get; set; } 
        public int killMonsterNum { get; set; }
        public int skillUseNum { get; set; }
        public int star { get; set; }       //MissionRate.S

        public static ClientBattleResultData s_Instance;
        public static ClientBattleResultData Instance
        {
            get
            {
                if ( s_Instance == null )
                {
                    s_Instance = new ClientBattleResultData();
                }
                return s_Instance;
            }
        }

        public void Dispose( )
        { 
            
        }

    }
}
