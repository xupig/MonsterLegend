﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{
    public class BufferCreateData
    {
        public int bufferID;
        public int createRemoveMode;
        public int createTimeMode;
    }

    public class NewBufferData
    {
        public int bufferID;
        public int delay;
        
    }
}
