﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{

    public class SkillEventData
    {
        public int mainID;
        public int eventIdx;
        public float delay;
        public int extend;
        public float playTime; //用于部分功能指定播放時間
        public int duration;   //生命周期

    }

}
