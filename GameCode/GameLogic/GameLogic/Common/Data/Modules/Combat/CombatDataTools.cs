﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{

    public static class CombatDataTools
    {
        public static List<SkillEventData> ParseSkillEvents(string eventStr)
        {
            var skillEvents = new List<SkillEventData>();
            if (!string.IsNullOrEmpty(eventStr))
            {
                var subEventStrList = eventStr.Split(';');
                for (int i = 0; i < subEventStrList.Length; i++)
                {
                    if (string.IsNullOrEmpty(subEventStrList[i].Trim())) continue;
                    SkillEventData skillEventData = new SkillEventData();
                    var strList = subEventStrList[i].Split(',');
                    skillEventData.mainID = int.Parse(strList[0]);
                    skillEventData.eventIdx = int.Parse(strList[1]);
                    skillEventData.delay = float.Parse(strList[2]) / 1000f;
                    skillEventData.extend = strList.Length >= 4 ? int.Parse(strList[3]) : 0;
                    skillEventData.duration = strList.Length >= 5 ? int.Parse(strList[4]) : 0;
                    skillEvents.Add(skillEventData);
                }
            }
            return skillEvents;
        }

        public static List<int> ParseListInt(List<string> source)
        {
            List<int> result = null;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<int> ParseListInt(string[] source)
        {
            List<int> result = null;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<string> ParseListString(string[] source)
        {
            List<string> result = null;
            if (source != null)
            {
                result = new List<string>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(source[i]);
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(List<string> source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(string[] source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static Dictionary<int, int> ParseDictionaryIntInt(Dictionary<string, string> source)
        {
            Dictionary<int, int> result = null;
            if (source != null)
            {
                result = new Dictionary<int, int>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), int.Parse(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, float> ParseDictionaryIntFloat(Dictionary<string, string> source)
        {
            Dictionary<int, float> result = null;
            if (source != null)
            {
                result = new Dictionary<int, float>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), float.Parse(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, string> ParseDictionaryIntString(Dictionary<string, string> source)
        {
            Dictionary<int, string> result = null;
            if (source != null)
            {
                result = new Dictionary<int, string>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), en.Current.Value);
                }
            }
            return result;
        }

        public static Dictionary<int, List<int>> ParseDictionaryIntListInt(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<int>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<int>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListInt(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<float>> ParseDictionaryIntListFloat(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<float>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<float>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListFloat(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<string>> ParseDictionaryIntListString(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<string>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<string>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListString(en.Current.Value));
                }
            }
            return result;
        }

        public static List<BufferEffectData> ParseBufferEffect(string source)
        {
            List<BufferEffectData> result = null;
            if (!string.IsNullOrEmpty(source))
            {
                result = new List<BufferEffectData>();
                var subEffectStrList = source.Split(';');
                for (int i = 0; i < subEffectStrList.Length; i++)
                {       
                    var subEffectStr = subEffectStrList[i];
                    var subEffectStrSp = subEffectStr.Split('(');
                    var effectName = subEffectStrSp[0];
                    var eventArgs = subEffectStrSp[1].Replace(")", "").Split(',');
                    result.Add(new BufferEffectData(effectName, eventArgs));  
                }
            }
            return result;
        }

        public static bool ParseIntBoolean(int source)
        {
            return Convert.ToBoolean(source);
        }
    }
}
