﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{

    public class BufferEffectData
    {
        public string effectName;
        public string[] effectValue;

        public BufferEffectData(string name, string[] value)
        {
            effectName = name;
            effectValue = value;
        }
    }

}
