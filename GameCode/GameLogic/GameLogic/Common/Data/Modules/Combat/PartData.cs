﻿using GameData;
using GameLoader.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{
    public class PartData
    {
        public int id;
        public int name;
        public int desc;
        public List<int> icons;
        public int sort;
        public float recoverDuration;
        public List<int> attriAdjIds;
        public List<float> attriFirstOrderAdjValues;
        public List<float> attriZeroOrderAdjValues;
        public Dictionary<int, List<float>> unlockConditions;
        public Dictionary<int, List<float>> ownBuffs;
        public Dictionary<int, List<float>> addBuffs;
        public Dictionary<int, List<string>> models;
        public List<int> _hpList;
        public bool readyDamaged;
        public bool autoSelect;

        public PartData(int id)
        {
            this.id = id;
            if (!GameData.XMLManager.boss_part.ContainsKey(id))
            {
                LoggerHelper.Error("BossPartData Key Error:" + id);
            }
            var data = GameData.XMLManager.boss_part[id];
            name = data.__name;
            desc = data.__desc;
            icons = data_parse_helper.ParseListInt(data.__icon);
            sort = data.__sort;
            recoverDuration = data.__recover / 1000f;
            attriAdjIds = data_parse_helper.ParseListInt(data.__attri_adj_ids);
            attriFirstOrderAdjValues = data_parse_helper.ParseListFloat(data.__attri_adj_values);
            attriZeroOrderAdjValues = data_parse_helper.ParseListFloat(data.__attri_adj_values_b);
            unlockConditions = data_parse_helper.ParseDictionaryIntListFloat(data.__unlock);
            ownBuffs = data_parse_helper.ParseDictionaryIntListFloat(data.__own_buff);
            addBuffs = data_parse_helper.ParseDictionaryIntListFloat(data.__add_buff);
            models = data_parse_helper.ParseDictionaryIntListString(data.__model);
            _hpList = new List<int>();
            foreach (var node in models)
            {
                _hpList.Add(node.Key);
            }
            _hpList.Sort();
            readyDamaged = CombatDataTools.ParseIntBoolean(data.__ready_damaged);
            autoSelect = CombatDataTools.ParseIntBoolean(data.__auto_select);
        }
    }
}
