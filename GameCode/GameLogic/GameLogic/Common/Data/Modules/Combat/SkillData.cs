﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{

    public class SkillData
    {
        public static int SKILL_POS_ONE = 1;
        public static int SKILL_POS_TWO = 2;
        public static int SKILL_POS_THREE = 3;
        public static int SKILL_POS_FOUR = 4;

        public int skillID;

        public int group;
        public int level;

        public int pos;
        public int showPirority;
        public int showType;
        public List<int> adjustSPGroup;
        public List<int> adjustSpell;

        public int adjustType;
        public List<int> skillActions;
        public List<float> skillActionProbs;
        public List<float> skillActionActiveTimes;

        public int spActiveWay;
        public List<int> cd;
        public Dictionary<int, int> useCosts;

        public Dictionary<int, List<int>> addBuffsOnCast;
        public List<int> delBuffsOnBreak;

        public float attackActionSpeed;
        public float actionCutTime;
        public int actionMovSwitch;
        public int searchTargetType;
        public List<int> originAdjust;
        public int searchRegionType;
        public List<int> searchRegionArg;
        public List<int> targetFilterOrders;
        public Dictionary<int, List<int>> targetFilterArgs;
        public int accordingStick;
        public int searchTargetRepeat;
        public int faceLockMode;
        public int faceAlways;
        public bool clearBuffWhenCast;
        public List<SkillEventData> skillEvents;

        public SkillData(int skillID)
        {
            if (!GameData.XMLManager.spell.ContainsKey(skillID)) 
            {
                LoggerHelper.Error("SkillData Key Error:" + skillID);
                return;
            }
            var data = GameData.XMLManager.spell[skillID];

            this.skillID = data.__id;

            skillActionActiveTimes = data_parse_helper.ParseListFloat(data.__action_active_time);
            skillEvents = CombatDataTools.ParseSkillEvents(data.__events);
            var skillSoundEvents = CombatDataTools.ParseSkillEvents(data.__sound_events);
            skillEvents.AddRange(skillSoundEvents);
            group = data.__group;
            level = data.__sp_level;
            pos = data.__pos;
            showPirority = data.__show_priority;
            showType = data.__show_type;
            adjustSPGroup = data_parse_helper.ParseListInt(data.__adjust_sp_group);
            adjustSpell = data_parse_helper.ParseListInt(data.__adjust_spell);
            adjustType = data.__adjust_type;
            skillActions = data_parse_helper.ParseListInt(data.__actions);
            skillActionProbs = data_parse_helper.ParseListFloat(data.__action_probs);
            skillActionActiveTimes = data_parse_helper.ParseListFloat(data.__action_active_time);
            spActiveWay = data.__sp_active_way;
            cd = data_parse_helper.ParseListInt(data.__cd);
            useCosts = data_parse_helper.ParseDictionaryIntInt(data.__use_costs);
            addBuffsOnCast = data_parse_helper.ParseDictionaryIntListInt(data.__add_buffs_on_cast);
            delBuffsOnBreak = data_parse_helper.ParseListInt(data.__del_buffs_on_break);
            attackActionSpeed = data.__attack_action_speed;
            actionCutTime = data.__action_cut_time;
            actionMovSwitch = data.__action_mov_switch;
            searchTargetType = data.__search_tgt_type;
            originAdjust = data_parse_helper.ParseListInt(data.__origin_adjust);
            searchRegionType = data.__search_region_type;
            searchRegionArg = data_parse_helper.ParseListInt(data.__search_region_arg);
            targetFilterOrders = data_parse_helper.ParseListInt(data.__target_filter_orders);
            targetFilterArgs = data_parse_helper.ParseDictionaryIntListInt(data.__target_filter_args);
            accordingStick = data.__according_stick;
            searchTargetRepeat = data.__search_tgt_repeat;
            faceLockMode = data.__face_lock_mode;
            faceAlways = data.__face_always;
            clearBuffWhenCast = data.__show_type >= 0;
        }
        
    }
}
