﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace Common.Data
{
    public class SkillActionData
    {
        public int actionID;
        public List<SkillEventData> skillEvents;

        public List<float> dmgRateLvlFactors;
        public List<float> dmgAddLvlFactors;
        public List<float> dmgCritRateLvlFactors;
        public List<float> dmgStrikeRateLvlFactors;
        public List<int> dmgTypes;
        public List<float> dmgTypesRate;
        public List<float> dmgTypesAdd;
        public float dmgProficientFactor;
        public int dmgSpecialType;
        public List<float> dmgSpecialArg;
        public Dictionary<int, int> fireCosts;
        public int adjustType;
        public HashSet<int> adjustActionSet;
        public int targetType;
        public int originType;
        public List<int> originAdjust;
        public int attackRegionType;
        public List<int> attackRegionArg;
        public int earlyWarningType;
        public int regionJudgeTime;
        public float fireDelayTime;
        public List<int> targetFilterOrders;
        public Dictionary<int, List<int>> targetFilterArgs;
        public int targetMinCount;
        public int targetMaxCount;
        public int collisionId;
        public int selfMovType;
        public List<float> selfMovArg;
        public Dictionary<int, List<float>> targetMovPreBuff;
        public int targetMovType;
        public List<float> targetMovArg;
        public Dictionary<int, List<float>> addBuffSelf;
        public Dictionary<int, int> delBuffSelf;
        public Dictionary<int, List<float>> addBuffTarget;
        public Dictionary<int, int> delBuffTarget;
        public Dictionary<int, float> fireAction;
        public Dictionary<int, int> chgSpellCD;
        public Dictionary<int, List<float>> fireDropItem;

        public SkillActionData(int actID)
        {
            if (!GameData.XMLManager.spell_action.ContainsKey(actID)) 
            {
                LoggerHelper.Error("SkillActionData Key Error:" + actID);
                return;
            }
            var data = GameData.XMLManager.spell_action[actID];
            this.actionID = actID;
            skillEvents = CombatDataTools.ParseSkillEvents(data.__effect_events);
            var skillSoundEvents = CombatDataTools.ParseSkillEvents(data.__sound_events);
            skillEvents.AddRange(skillSoundEvents);
            dmgRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_rate_lvl_factors);
            dmgAddLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_add_lvl_factors);
            dmgCritRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_crit_rate_lvl_factors);
            dmgStrikeRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_strike_rate_lvl_factors);
            dmgTypes = data_parse_helper.ParseListInt(data.__dmg_types);
            dmgTypesRate = data_parse_helper.ParseListFloat(data.__dmg_types_rate);
            dmgTypesAdd = data_parse_helper.ParseListFloat(data.__dmg_types_add);
            dmgProficientFactor = data.__dmg_proficient_factor;
            dmgSpecialType = data.__dmg_special_type;
            dmgSpecialArg = data_parse_helper.ParseListFloat(data.__dmg_special_arg);

            fireCosts = data_parse_helper.ParseDictionaryIntInt(data.__fire_costs);
            adjustType = data.__adjust_type;
            List<int> adjustActionList = data_parse_helper.ParseListInt(data.__adjust_action);
            adjustActionSet = new HashSet<int>();
            for (int i = 0; i < adjustActionList.Count; ++i)
            {
                adjustActionSet.Add(adjustActionList[i]);
            }
            targetType = data.__target_type;
            originType = data.__origin_type;
            originAdjust = data_parse_helper.ParseListInt(data.__origin_adjust);
            attackRegionType = data.__attack_region_type;
            attackRegionArg = data_parse_helper.ParseListInt(data.__attack_region_arg);

            earlyWarningType = data.__early_warning_type;

            regionJudgeTime = data.__region_judge_time;
            fireDelayTime = data.__fire_delay_time;
            targetFilterOrders = data_parse_helper.ParseListInt(data.__target_filter_orders);
            targetFilterArgs = data_parse_helper.ParseDictionaryIntListInt(data.__target_filter_args);
            targetMinCount = data.__target_min_count;
            targetMaxCount = data.__target_max_count;
            collisionId = data.__collision_id;
            selfMovType = data.__self_mov_type;
            selfMovArg = data_parse_helper.ParseListFloat(data.__self_mov_arg);

            targetMovPreBuff = data_parse_helper.ParseDictionaryIntListFloat(data.__target_mov_pre_buff);
            targetMovType = data.__target_mov_type;
            targetMovArg = data_parse_helper.ParseListFloat(data.__target_mov_arg);

            addBuffSelf = data_parse_helper.ParseDictionaryIntListFloat(data.__add_buff_self);
            delBuffSelf = data_parse_helper.ParseDictionaryIntInt(data.__del_buff_self);
            addBuffTarget = data_parse_helper.ParseDictionaryIntListFloat(data.__add_buff_target);
            delBuffTarget = data_parse_helper.ParseDictionaryIntInt(data.__del_buff_target);
            fireAction = data_parse_helper.ParseDictionaryIntFloat(data.__fire_action);
            chgSpellCD = data_parse_helper.ParseDictionaryIntInt(data.__chg_spell_cd);
            fireDropItem = data_parse_helper.ParseDictionaryIntListFloat(data.__fire_dropitem);
        }
    }


}
