using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Common.Data
{
    public class GuildWarData
    {
        public const int GUILD_TRY_MISSION_ID = 90104;
        public const int GUILD_WAIT_SINGLE_MISSION_ID = 90105;
        public const int GUILD_WAIT_DOUBLE_MISSION_ID = 90106;
        public const int GUILD_WAR_PK_MISSION_ID = 90107;

        public enum MilitaryRank
        {
            knightLeader = 1,
            knight = 2,
            soldier = 3,
        }

        public GuildWarEnemyData EnemyData
        {
            get;
            set;
        }

        public PbGuildStage Stage
        {
            get;
            set;
        }

    }

    public class GuildWarEnemyData
    {
        public string guildName;
        public string name;
        public int fight;
        public int military;
        public int vocation;
        public int level;
        public int round;
    }
}
