﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class WorldBossInfoData
    {
        public byte isBossLife { get; set; }
        public UInt32 entityId { get; set; }
        public UInt32 monsterId { get; set; }
        public Vector3 targetPosition { get; set; }

        public string name { get; set; }
    }

    public class MiniMapInfoData
    {
        public byte type { get; set; }  //1.争夺型传送门
        public UInt32 param { get; set; }   //不同类型type，需要的参数 1:portalId
        public UInt32 id { get; set; }
        public byte isLife { get; set; }
        public Vector3 targetPosition { get; set; }
        public string name { get; set; }
    }
}

