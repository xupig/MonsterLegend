﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class SharePostionData
    {
        public int mapId { get; set; }
        public int mapLine { get; set; }
        public int type { get; set; }
        public Vector3 targetPosition { get; set; }
        public bool multiNeedConfirm { get; set; }
    }
}
