﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class MovePathData
    {
        public int id { get; set; }
        public Vector3 pathPos { get; set; }
        public string relationId { get; set; }
    }
}
