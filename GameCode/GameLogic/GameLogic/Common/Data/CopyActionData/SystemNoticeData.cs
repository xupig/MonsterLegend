﻿using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class SystemNoticeData
    {
        public int type { get; set; }
    }

    public class SystemNoticeDisplayData : SystemNoticeData
    {
        public int content { get; set; }
        public object[] objArr { get; set; }
        public uint time { get; set; }
        public int priority { get; set; }
    }

    public class SystemNoticeProgressData : SystemNoticeData
    {
        public int content { get; set; }
        public object[] objArr { get; set; }
        public uint time { get; set; }
        public int priority { get; set; }
        public int progressbarPlayType { get; set; }
    }

    public class SystemNoticeRightUpData : SystemNoticeData
    {
        public int content { get; set; }
        public object[] objArr { get; set; }
        public uint time { get; set; }
        public int priority { get; set; }
        public int rightUpType { get; set; }
        public int rightUpNoticeId { get; set; }
        public int rightUpTargetId { get; set; }
        public int killMonsterNum { get; set; }
        public int monsterTotalNum { get; set; }
    }

    public class SystemNoticeTopData : SystemNoticeData
    {
        public int notice_type { get; set; }
        public int timerCount { get; set; }
        public int maxCount { get; set; }
        public int rightUpTargetId { get; set; }
    }

    public class SystemCombatAttriData : SystemNoticeTopData
    {
        public List<PbCombatAttri> combat_attri { get; set; }
    }

    public class SystemNoticeCopyStarData : SystemNoticeData
    {
        public int actionId { get; set; }
        public int state { get; set; }
    }

}
