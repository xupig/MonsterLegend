﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class CopyStatisticData
    {
        /// <summary>
        /// 指承受伤害还是输出伤害
        /// </summary>
        public StatisticOutputOrBear type { get; set; }

        /// <summary>
        /// 统计的行为id
        /// </summary>
        public int actionId { get; set; }

        /// <summary>
        /// 伤害数值
        /// </summary>
        public int value { get; set; }

        /// <summary>
        /// 伤害百分比，此变量只有在UI取数据时进行填充
        /// </summary>
        public int percent { get; set; }
    }

    public class PlayerCopyStatisticData : CopyStatisticData
    {
        /// <summary>
        /// 玩家名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 玩家等级
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 玩家职业
        /// </summary>
        public int vocation { get; set; }

        /// <summary>
        /// 玩家阵营id
        /// </summary>
        public int campId { get; set; }
    }

    public class PetCopyStatisticData : CopyStatisticData
    {
        /// <summary>
        /// 实体id
        /// </summary>
        public int petId { get; set; }

        /// <summary>
        /// 宠物的信息
        /// </summary>
        public PetInfo petInfo { get;set;}
    }

    public enum StatisticOutputOrBear
    {
        Output,
        Bear
    }
}
