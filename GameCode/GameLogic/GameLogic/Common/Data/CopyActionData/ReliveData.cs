﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class ReliveData
    {
        public int type { get; set; } //
        public int cdTime { get; set; } 
        public int odds { get; set; }
        public int price { get; set; }
        public int currReliveCount { get; set; } //剩余
       
        public static ReliveData s_Instance;
        public static ReliveData Instance
        {
            get
            {
                if ( s_Instance == null )
                {
                    s_Instance = new ReliveData();
                }
                return s_Instance;
            }
        }

        public void Dispose( )
        { 
            
        }

    }
}

