﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public class WindowsWebPlatformLoginData
    {
        /*public string username { get; set; }
        public string time { get; set; }
        public string flag { get; set; }
        public string cm { get; set; }
        public string server { get; set; }
        public string platform_id { get; set; }
        public string device_type { get; set; }
        
        public string fnpid { get; set; }
        public string fnpidraw { get; set; }
        public string fngid { get; set; }
        public string client { get; set; }
        public string uid { get; set; }
        public string name { get; set; }
        public string ext { get; set; }*/
        
        public string username { get; set; }
        public string time { get; set; }
        public string server { get; set; }
        public string cm { get; set; }
        public string flag { get; set; }
        public string platform_id { get; set; }
        public string device_type { get; set; }
        public string name { get; set; }
        public string game { get; set; }
        public string cid { get; set; }
        public string oid { get; set; }
        public string aid { get; set; }
        public string fnpid { get; set; }
        public string fnppid { get; set; }
        public string fngid { get; set; }
    }
}