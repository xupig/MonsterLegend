﻿namespace Common.Data
{
    public class ConfigConst
    {
        /// <summary>
        /// 返回正确码
        /// </summary>
        public static int CorrectCode = 300;

        /// <summary>
        /// 验证码的code
        /// </summary>
        public static int NeedVerifyCodeNum = 3001;

        ///// <summary>
        ///// 游戏appid
        ///// </summary>
        public static string ClientId = "1436768614496470";

        ///// <summary>
        ///// 渠道ID
        ///// </summary>
        public static string ChannelId = "270";

        /// <summary>
        /// 标示大小包
        /// </summary>
        public static string PackageTypeValue = "1";

        /// <summary>
        /// 服务器ID
        /// </summary>
        public static string ServerId = "1";

        /// <summary>
        /// 游戏ID
        /// </summary>
        public static string GameId = "1436768614496470";

        /// <summary>
        /// 事件id，绑定事务用
        /// </summary>
        public static string EventId = "0";

     
        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public static string Uid = "oYjrmt9HJLZctMapZpYn_7rR1kvg";

        /// <summary>
        /// 用户昵称
        /// </summary>
        public static string NickName = "0";

        ///// <summary>
        ///// 平台ID
        ///// </summary>
        public static string PlatformId = "202";

        /// <summary>
        /// 分区唯一标识
        /// </summary>
        public static string AreaId = "0";

        /// <summary>
        /// 游戏角色等级
        /// </summary>
        public static string RoleLevel = "1";

        /// <summary>
        /// 角色名
        /// </summary>
        public static string RoleName = "IM";
    }
}
