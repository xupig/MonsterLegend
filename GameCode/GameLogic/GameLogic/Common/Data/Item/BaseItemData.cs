﻿using System.Collections.Generic;
using Common.Global;
using Common.Structs;

namespace Common.Data
{
    public abstract class BaseItemData : IItem
    {
        public BaseItemData(int id)
        {
            this.Id = id;
        }

        public int Id
        {
            protected set;
            get;
        }

        public abstract string Name { get; }

        public virtual string ColorName
        {
            get
            {
                return string.Format("<color={0}>{1}</color>", ColorDefine.GetColorHexToken(this.Quality), this.Name);
            }
        }

        public override string ToString()
        {
            return string.Format("id:{0}    num:{1}", Id, StackCount);
        }

        public abstract BagItemType Type { get; }

        public abstract int Icon { get; }
        public abstract int Quality { get; }
        public abstract string Description { get; }
        public abstract string Story { get; }
        public abstract int UsageLevelRequired { get; }
        public abstract Vocation UsageVocationRequired { get; }
        public abstract int UsageVipLevelRequired { get; }
        public abstract Dictionary<int,int> SellPrice { get; }
        public abstract int StackCount { get; set; }
        public abstract int MaxStackCount { get; }
    }
}
