﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/24 11:15:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class RewardData
    {
        public int id;
        public int num;

        private static List<RewardData> _rewardList = new List<RewardData>();

        public RewardData()
        {
        }

        public RewardData(int id, int num)
        {
            this.id = id;
            this.num = num;
        }

        public static RewardData GetRewardData(BaseItemData baseItemData)
        {
            return GetRewardData(baseItemData.Id, baseItemData.StackCount);
        }

        public static RewardData GetRewardData(int id, int num)
        {
            RewardData data = GetRewardData();
            data.id = id;
            data.num = num;
            return data;
        }

        public static RewardData GetRewardData()
        {
            RewardData data;
            if (_rewardList.Count > 0)
            {
                data = _rewardList[0];
                _rewardList.RemoveAt(0);
            }
            else
            {
                data = new RewardData();
            }
            return data;
        }

        public static void ReturnRewardData(RewardData data)
        {
            if (_rewardList.IndexOf(data) == -1)
            {
                _rewardList.Add(data);
            }
        }

        public static List<RewardData> FilterSameItemId(List<RewardData> rewardDataList)
        {
            HashSet<int> hashSet = new HashSet<int>();
            List<RewardData> filteredList = new List<RewardData>();
            for (int i = 0; i < rewardDataList.Count; i++)
            {
                RewardData rewardData = rewardDataList[i];
                if (!hashSet.Contains(rewardData.id))
                {
                    hashSet.Add(rewardData.id);
                    filteredList.Add(rewardData);
                }
                else
                {
                    ReturnRewardData(rewardData);
                }
            }
            return filteredList;
        }
    }
}
