﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Common.ServerConfig;
using Common.Global;
using Common.Structs;
using GameData;
using GameMain.Entities;
using UnityEngine;

namespace Common.Data
{
    public class ItemDataCreator
    {
        public static BaseItemData Create(int id)
        {
            BagItemType itemType = item_helper.GetItemType(id);
            if(itemType == BagItemType.Equip)
            {
                return new EquipData(id);
            }
            return new ItemData(id);
        }
    }
}
