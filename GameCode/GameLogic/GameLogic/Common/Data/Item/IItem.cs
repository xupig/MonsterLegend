﻿namespace Common.Data
{
    public interface IItem
    {
        int Id { get; }
    }
}
