﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Common.ServerConfig;
using Common.Global;
using Common.Structs;
using GameData;
using GameMain.Entities;

namespace Common.Data
{
    public class ItemData : BaseItemData
    {
        private item_commom _baseConfig;
        private item_commom baseConfig
        {
            get
            {
                if (_baseConfig == null)
                {
                    _baseConfig = item_helper.GetItemConfig(Id);
                }
                return _baseConfig;
            }
        }
        private int _stackCount = 0;

        public ItemData(int id)
            : base(id)
        {
            this.Id = id;
        }

        #region 读配置的数据
        public override string Name
        {
            get
            {
                List<string> nameList = data_parse_helper.ParseListString(baseConfig.__name);
                return nameList.Count > 0 ? MogoLanguageUtil.GetContent(nameList) : Id.ToString();
            }
        }

        public override string ColorName
        {
            get
            {
                return string.Format("<color={0}>{1}</color>", ColorDefine.GetColorHexToken(this.Quality), this.Name);
            }
        }

        public override BagItemType Type
        {
            get
            {
                return (BagItemType)baseConfig.__type;
            }
        }

        public override int Icon
        {
            get
            {
                if (Type == BagItemType.Fashion)
                {
                    return fashion_helper.GetIconId(int.Parse(UseEffect.Value), (int)PlayerAvatar.Player.vocation);
                }
                return baseConfig.__icon;
            }
        }


        public override int Quality
        {
            get
            {
                return baseConfig.__quality;
            }
        }

        public override int StackCount
        {
            get
            {
                return _stackCount;
            }
            set
            {
                _stackCount = value;
            }
        }

        public override int MaxStackCount
        {
            get
            {
                return baseConfig.__stack > 0 ? baseConfig.__stack : 1;
            }
        }

        public override string Description
        {
            get
            {
                return GetDescField("1");
            }
        }

        public override string Story
        {
            get
            {
                return GetDescField("2");
            }
        }

        private string GetDescField(string key)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(baseConfig.__desc);
            if (dataDict != null && dataDict.ContainsKey(key) == true)
            {
                return MogoLanguageUtil.GetContent(dataDict[key]);
            }
            return string.Empty;
        }

        public override int UsageLevelRequired
        {
            get
            {
                return GetUsageLimitField("1");
            }
        }

        public override Vocation UsageVocationRequired
        {
            get
            {
                return (Vocation)GetUsageLimitField("2");
            }
        }

        public override int UsageVipLevelRequired
        {
            get
            {
                return GetUsageLimitField("3");
            }
        }

        public int UsageBagCapacityRequired
        {
            get
            {
                return GetUsageLimitField("16");
            }
        }

        private int GetUsageLimitField(string key)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(baseConfig.__use_limit);
            if (dataDict != null && dataDict.ContainsKey(key) == true)
            {
                return Convert.ToInt32(dataDict[key]);
            }
            return 0;
        }

        public override Dictionary<int, int> SellPrice
        {
            get
            {
                Dictionary<int, int> result = new Dictionary<int, int>();
                if (baseConfig.__sell_price != null)
                {
                    Dictionary<string, string> dataDict = data_parse_helper.ParseMap(baseConfig.__sell_price);
                    foreach (var pair in dataDict)
                    {
                        result.Add(int.Parse(pair.Key), int.Parse(pair.Value));
                    }
                }
                return result;
            }
        }

        public KeyValuePair<string, string> UseEffect
        {
            get
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(baseConfig.__use_effect);
                foreach (KeyValuePair<string, string> kvp in dataDict)
                {
                    return kvp;
                }
                return new KeyValuePair<string, string>(string.Empty, string.Empty);
            }
        }

        public Dictionary<string, string> UseEffectDict
        {
            get
            {
                return data_parse_helper.ParseMap(baseConfig.__use_effect);
            }
        }

        public item_commom BaseConfig
        {
            get
            {
                return baseConfig;
            }
        }
        #endregion
    }
}
