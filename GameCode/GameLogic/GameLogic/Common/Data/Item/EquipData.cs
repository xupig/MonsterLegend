﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Common.ServerConfig;
using Common.Global;
using Common.Structs;
using GameData;
using GameMain.Entities;
using UnityEngine;
using GameLoader.Utils;

namespace Common.Data
{
    public class EquipData : BaseItemData
    {
        private item_equipment _baseConfig;
        public item_equipment BaseConfig
        {
            get
            {
                if (_baseConfig == null)
                {
                    _baseConfig = item_helper.GetEquipConfig(this.Id);
                }
                return _baseConfig;
            }
        }
        private equip_value _valueConfig;
        public equip_value ValueConfig
        {
            get
            {
                if (_valueConfig == null)
                {
                    int configID = GetValueConfigId();
                    _valueConfig = equip_value_helper.GetConfig(configID);
                }
                return _valueConfig;
            }
        }


        private int _stackCount = 1;

        public EquipData(int id)
            : base(id)
        {
            this.Id = id;
        }

        /// <summary>
        /// 基础战斗力，客户端自行计算
        /// </summary>
        public int BaseFightForce
        {
            get
            {
                return fight_force_helper.GetFightForce(GetBasisPropertyIdList(), GetBasisPropertyValueList(), PlayerAvatar.Player.vocation);
            }
        }

        /// <summary>
        /// 根据服务器数据同步
        /// </summary>
        private int fightForce;
        public int FightForce
        {
            get
            {
                if (fightForce == 0)
                {
                    return BaseFightForce;
                }
                return fightForce;
            }
            set
            {
                fightForce = value;
            }
        }

        public override string Name
        {
            get
            {
                List<string> nameList = data_parse_helper.ParseListString(BaseConfig.__name);
                return nameList.Count > 0 ? MogoLanguageUtil.GetContent(nameList) : Id.ToString();
            }
        }

        public override string ColorName
        {
            get
            {
                return string.Format("<color={0}>{1}</color>", ColorDefine.GetColorHexToken(this.Quality), this.Name);
            }
        }

        public override BagItemType Type
        {
            get
            {
                return (BagItemType)BaseConfig.__type;
            }
        }

        public override int Icon
        {
            get
            {
                return BaseConfig.__icon;
            }
        }

        public override int StackCount
        {
            get
            {
                return _stackCount;
            }
            set
            {
                _stackCount = value;
            }
        }

        public override int MaxStackCount
        {
            get
            {
                return 1;
            }
        }

        public EquipType SubType
        {
            get
            {
                return (EquipType)BaseConfig.__subtype;
            }
        }

        public override string Description
        {
            get
            {
                return GetDescField("1");
            }
        }

        public override string Story
        {
            get
            {
                return GetDescField("2");
            }
        }

        private string GetDescField(string key)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(BaseConfig.__desc);
            if (dict != null && dict.ContainsKey(key) == true)
            {
                return MogoLanguageUtil.GetContent(dict[key]);
            }
            return string.Empty;
        }

        public override int Quality
        {
            get
            {
                return BaseConfig.__quality;
            }
        }

        public int Star
        {
            get
            {
                return BaseConfig.__equ_star;
            }
        }

        public override int UsageLevelRequired
        {
            get
            {
                return BaseConfig.__level_need;
            }
        }

        public override Vocation UsageVocationRequired
        {
            get
            {
                return (Vocation)BaseConfig.__vocation;
            }
        }

        public override int UsageVipLevelRequired
        {
            get
            {
                return 0;
            }
        }

        public override Dictionary<int, int> SellPrice
        {
            get
            {
                Dictionary<int, int> result = new Dictionary<int, int>();
                if (BaseConfig.__sell_price != null)
                {
                    Dictionary<string, string> dataDict = data_parse_helper.ParseMap(BaseConfig.__sell_price);
                    foreach (var pair in dataDict)
                    {
                        result.Add(int.Parse(pair.Key), int.Parse(pair.Value));
                    }
                }
                return result;
            }
        }

        private int GetValueConfigId()
        {
            return BaseConfig.__vocation * 100000 + BaseConfig.__quality * 10000 + (BaseConfig.__subtype % 10) * 1000 + BaseConfig.__level_need * 10 + BaseConfig.__equ_star;
        }

        /// <summary>
        /// 基础属性Id列表
        /// </summary>
        /// <returns></returns>
        public List<int> GetBasisPropertyIdList()
        {
            return data_parse_helper.ParseListInt(ValueConfig.__attri_type);
        }
        /// <summary>
        /// 基础属性值列表，其长度值和Id列表长度一致
        /// </summary>
        /// <returns></returns>
        public List<int> GetBasisPropertyValueList()
        {
            return data_parse_helper.ParseListInt(ValueConfig.__attri_value);
        }

        /// <summary>
        /// 装备上宝石槽位列表
        /// </summary>
        public List<int> SlotList
        {
            get
            {
                return data_parse_helper.ParseListInt(BaseConfig.__slot);
            }
        }

        /// <summary>
        /// 装备上可扩展
        /// </summary>
        public List<int> NewSlotList
        {
            get
            {
                return data_parse_helper.ParseListInt(BaseConfig.__new_slot);
            }
        }

        public int Level
        {
            get
            {
                if (BaseConfig.__level_limit == 0)
                {
                    return BaseConfig.__level_need;
                }
                else
                {
                    return Mathf.Clamp(PlayerAvatar.Player.level, BaseConfig.__level_need, BaseConfig.__level_limit);
                }
            }
        }
    }
}
