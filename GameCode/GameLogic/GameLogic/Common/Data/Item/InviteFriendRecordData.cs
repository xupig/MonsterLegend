﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager;

namespace Common.Data
{
    /// <summary>
    /// 邀请好友记录数据
    /// </summary>
    public class InviteFriendRecordData
    {
        public bool isBeInvite;               //是否为被邀请(true:被邀请,false:邀请)
        public PbInviteRecord inviteRecord;   //邀请或被邀请者记录信息(后端返回)

        public InviteFriendRecordData(PbInviteRecord inviteRecord, bool isBeInvite)
        {
            this.inviteRecord = inviteRecord;
            this.isBeInvite = isBeInvite;
        }

        /// <summary>
        /// 被邀请者跟自己是否已为好友(true:已是好友,false:不是好友)
        /// </summary>
        public bool isFriend
        {
            get { return PlayerDataManager.Instance.FriendData.HasFriend(inviteRecord.dbid); }
        }            
    }
}
