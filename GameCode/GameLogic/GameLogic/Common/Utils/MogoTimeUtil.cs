﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/26 13:18:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Utils
{
    public static class MogoTimeUtil
    {
        private static DateTime dateTime = new DateTime();
        private const string mode = "{0}{1}";

        /// <summary>
        /// 2015-4-21 20:20:21 转成服务器时区的时间戳
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static long DateTime2ServerTimeStamp(string time)
        {
            DateTime dateTime = DateTime.Parse(time);
            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            return UtcTime2ServerTimeStamp(dateTime);
        }

        public static DateTime GetUtcTime(int year,int month,int day,int hour,int min,int sec)
        {
            return new DateTime(year, month, day, hour, min, sec, DateTimeKind.Utc);
        }

        /// <summary>
        /// utc时间转成服务器时区的时间戳
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long UtcTime2ServerTimeStamp(DateTime dateTime)
        {
            if(dateTime.Kind!=DateTimeKind.Utc)
            {
                dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
            }
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((dateTime - start).TotalSeconds + Global.Global.serverTimeZone * 3600);
        }

        /// <summary>
        /// 服务器时间戳转成utc时间
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static DateTime ServerTimeStamp2UtcTime(long timeStamp)
        {          
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            long time = timeStamp - (Global.Global.serverTimeZone * 3600);
            return start.AddSeconds(time);
        }

        /// <summary>
        /// 服务器时间戳转成本地时区时间
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static DateTime ServerTimeStamp2LocalTime(long timeStamp)
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            long time = timeStamp + (Common.Global.Global.localTimeZone * 3600);
            return start.AddSeconds(time);
        }

        /// <summary>
        /// 服务器时间戳转成本地时间戳
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static long ServerTimeStamp2LocalTimeStamp(long timeStamp)
        {
            long time = timeStamp + (Common.Global.Global.localTimeZone * 3600);
            return time;
        }

        /// <summary>
        /// 相对时间转换，譬如倒计时就用这个
        /// </summary>
        /// <param name="second"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string ToUniversalTime(int second, string formatter = "hh:mm:ss")
        {
            int day = second / 86400;
            second -= day * 86400;
            int hour = second / 3600;
            second -= hour * 3600;
            int min = second / 60;
            second -= min * 60;
            formatter = formatter.Replace("dd", day);
            formatter = formatter.Replace("hh", hour);
            formatter = formatter.Replace("mm", min);
            formatter = formatter.Replace("ss", second);
            
            return formatter;
        }

        /// <summary>
        /// 这个是本地绝对时间，假如服务器在东8区，配置表配了一条早上8点开始的活动，同一服务器的东12区玩家看到活动的开始时间是12点，而不是8点
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string ToLocalTime(long timeStamp, string formatter = "yyyy-MM-dd hh:mm:ss")
        {
           
            dateTime = ServerTimeStamp2LocalTime(timeStamp);

            formatter = formatter.Replace("yyyy", dateTime.Year.ToString());
            formatter = formatter.Replace("MM", dateTime.Month);
            formatter = formatter.Replace("dd", dateTime.Day);
            formatter = formatter.Replace("hh", dateTime.Hour);
            formatter = formatter.Replace("mm", dateTime.Minute);
            formatter = formatter.Replace("ss", dateTime.Second);
            return formatter;
        }

        /// <summary>
        /// string的扩展方法
        /// </summary>
        /// <param name="content"></param>
        /// <param name="mode"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string Replace(this string content, string formatter, int value)
        {
            if(value >= 10)
            {
                return content.Replace(formatter, value.ToString());
            }
            return content.Replace(formatter, string.Concat("0", value.ToString()));
        }

        /// <summary>
        /// 获取当前星期
        /// Monday 1 ... Sunday 7
        /// </summary>
        /// <returns></returns>
        public static int GetDayOfWeek(Int64 serverTimeStamp)
        {
            DateTime serverTime = ServerTimeStamp2LocalTime(serverTimeStamp);
            return (serverTime.DayOfWeek == DayOfWeek.Sunday) ? 7 : (int)serverTime.DayOfWeek;
        }

        public static DateTime GetEndDateOfMonth(long serverTimeStamp)
        {
            DateTime currentDate = ServerTimeStamp2LocalTime(serverTimeStamp);
            DateTime nextMonthDate = currentDate.AddMonths(1);
            return new DateTime(nextMonthDate.Year, nextMonthDate.Month, 1);
        }

    }
}
