﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Utils
{
    public class MogoRayUtils
    {

        /// <summary>
        /// 计算反射方向
        /// </summary>
        /// <param name="incidentDirection">入射方向</param>
        /// <param name="raycastHit">碰撞点</param>
        /// <returns>反射方向</returns>
        public static Vector3 CalcReflectionDirection(Vector3 incidentDirection, RaycastHit raycastHit)
        {
            Vector3 reflection = Vector3.zero;
            //反射角度计算公式
            reflection = Vector3.Dot(2 * raycastHit.normal, incidentDirection) * raycastHit.normal - incidentDirection;
            reflection = -reflection;
            return reflection.normalized;

        }

        #region 点击屏幕从主相机产生射线
        public static RaycastHit MouseRaycastFromCamera(int rayLenght, int layerMask)
        {
            RaycastHit hit;

            Ray ray = Camera.main != null? Camera.main.ScreenPointToRay(Input.mousePosition):new Ray(Vector3.zero,Vector3.zero);
            Physics.Raycast(ray, out hit, rayLenght, (int)layerMask);
            if (Application.isEditor) Debug.DrawLine(ray.origin, hit.point, Color.red, 30);
            return hit;
        }

        public static RaycastHit TouchRaycastFromCamera(Vector3 touchPosition, int rayLenght, int layerMask)
        {
            RaycastHit hit;

            Ray ray = Camera.main != null ? Camera.main.ScreenPointToRay(touchPosition) : new Ray(Vector3.zero, Vector3.zero);
            Physics.Raycast(ray, out hit, rayLenght, (int)layerMask);
            if (Application.isEditor) Debug.DrawLine(ray.origin, hit.point, Color.red, 30);
            return hit;
        }

        public static RaycastHit[] MouseRaycastAllFromCamera(int rayLenght, int layerMask)
        {
            RaycastHit[] hits = null;

            Ray ray = Camera.main != null ? Camera.main.ScreenPointToRay(Input.mousePosition) : new Ray(Vector3.zero, Vector3.zero);
            hits = Physics.RaycastAll(ray, rayLenght, layerMask);
            if (Application.isEditor && hits != null && hits.Length > 0) Debug.DrawLine(ray.origin, hits[hits.Length - 1].point, Color.green, 30);
            return hits;
        }

        public static RaycastHit[] TouchRaycastAllFromCamera(Vector3 touchPosition, int rayLenght, int layerMask)
        {
            RaycastHit[] hits = null;

            Ray ray = Camera.main != null ? Camera.main.ScreenPointToRay(touchPosition) : new Ray(Vector3.zero, Vector3.zero);
            hits = Physics.RaycastAll(ray, rayLenght, layerMask);
            if (Application.isEditor && hits != null && hits.Length > 0) Debug.DrawLine(ray.origin, hits[hits.Length - 1].point, Color.green, 30);
            return hits;
        }

        #endregion

    }
}
