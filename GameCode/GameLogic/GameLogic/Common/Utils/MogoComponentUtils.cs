﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Common.Utils
{
    /// <summary>
    /// 组件工具类
    /// </summary>
    public class MogoComponentUtils
    {
        /// <summary>
        /// 获取粒子系统时长
        /// </summary>
        /// <param name="transform"></param>
        /// <returns></returns>
        public static float GetParticleSystemLength(Transform transform)
        {
            ParticleSystem[] particleSystems = transform.GetComponentsInChildren<ParticleSystem>();
            float maxDuration = 0;
            foreach (ParticleSystem ps in particleSystems)
            {
                if (ps.enableEmission)
                {
                    if (ps.loop)
                    {
                        return -1f;
                    }
                    float dunration = 0f;
                    if (ps.emissionRate <= 0)
                    {
                        dunration = ps.startDelay + ps.startLifetime;
                    }
                    else
                    {
                        dunration = ps.startDelay + Mathf.Max(ps.duration, ps.startLifetime);
                    }
                    if (dunration > maxDuration)
                    {
                        maxDuration = dunration;
                    }
                }
            }
            return maxDuration;
        }

       
    }
}
