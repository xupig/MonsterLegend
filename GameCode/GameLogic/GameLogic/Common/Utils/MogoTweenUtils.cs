﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace MogoEngine.Utils
{
    public class MogoTweenUtilsUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum MogoTweenType
    {
        SUB = 0,
        ADD = 1
        
    }

    public class MogoTween
    {

        private MogoTweenType _tweenType = MogoTweenType.SUB;
        private GameObject _resourceObj;
        private float _resourceValue;
        private float _targetValue;
        private Dictionary<MogoTweenType, Action> _updateDict = null;
        private float _changeValue = 0f;
        private bool _isStart = false;
        private Material mat;
        //
        private float _changeValue2 = 0f;
        private float _resourceValue2;
        private Color _color;

        public bool isEnd { get; set; }
        public MogoTween(GameObject resourceValue, float targetValue, float _startTime, float time,Color color, MogoTweenType tweenType = MogoTweenType.SUB)
        {
            _isStart = false;
            isEnd = false;
            this._resourceObj = resourceValue;
            mat = _resourceObj.GetComponent<MeshRenderer>().materials[0];
            
            this._targetValue = targetValue;
            this._tweenType = tweenType;
            this._color = color;
            _updateDict = new Dictionary<MogoTweenType, Action>();
            //_updateDict.Add(MogoTweenType.ADD, AddUpdate);
           // _updateDict.Add(MogoTweenType.SUB, SubUpdate);
            //计算变化量
            _resourceValue = mat.GetColor("_BaseColor").a;
            _changeValue = Math.Abs(this._resourceValue - targetValue) / time;
            _changeValue /= 30f;
            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _changeValue2 = Math.Abs(this._resourceValue2 - targetValue) / time;
            _changeValue2 /= 30f;

            GameLoader.Utils.Timer.TimerHeap.AddTimer(Convert.ToUInt32(_startTime * 1000), 0, () =>
                {
                    _isStart = true;
                    
                });
        }

        public void Update()
        {
            if (_isStart == false) return;
            //_updateDict[_tweenType]();
            switch (_tweenType)
            {
                case MogoTweenType.SUB:
                    SubUpdate();
                    break;
                case MogoTweenType.ADD:
                    AddUpdate();
                    break;
               
            }


        }    

        private void AddUpdate()
        {
            _resourceValue = mat.GetColor("_BaseColor").a;
            _resourceValue = _resourceValue + _changeValue;
            isEnd = _resourceValue >= _targetValue ? true : false;
            if (isEnd == true) _resourceValue = _targetValue;
            mat.SetColor("_BaseColor", new Color(this._color.r, this._color.g, this._color.b, _resourceValue));
           
            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _resourceValue2 = _resourceValue2 + _changeValue2;
            if (isEnd == true) _resourceValue2 = _targetValue;
            mat.SetFloat("_InternalAlpha", _resourceValue2);

        }

        private void SubUpdate()
        {
            _resourceValue = mat.GetColor("_BaseColor").a;
            _resourceValue = _resourceValue - _changeValue; 
            isEnd = _resourceValue <= _targetValue ? true : false;
            if (isEnd == true) _resourceValue = _targetValue;
            mat.SetColor("_BaseColor", new Color(this._color.r, this._color.g, this._color.b, _resourceValue));

            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _resourceValue2 = _resourceValue2 - _changeValue2;
            if (isEnd == true) _resourceValue2 = _targetValue;
            mat.SetFloat("_InternalAlpha", _resourceValue2);
            //Debug.LogError("1_______resourceValue: " + _resourceValue + ",_resourceValue2: " + _resourceValue2);
        }

        public void Dispose()
        {
            _updateDict = null;
        }

    }

    public class MogoTweenUtils
    {
        private static MogoTweenUtils _instance;
        public static MogoTweenUtils instance
        {
            get 
            {
                if(_instance == null)
                {
                    _instance = new MogoTweenUtils();
                }
                return _instance;
            }
        }

        private List<MogoTween> _tweenList = null;


        public MogoTweenUtils()
        {
            MogoWorld.RegisterUpdate<MogoTweenUtilsUpdateDelegate>("MogoTweenUtils.Update", Update);
            _tweenList = new List<MogoTween>();
        }

        private void Update()
        {
            if (_tweenList.Count <= 0) return;
            //
            for (int i = 0; i < _tweenList.Count; i++)
            {
                _tweenList[i].Update();
                if (_tweenList[i].isEnd == true)
                {
                    _tweenList[i].Dispose();
                    _tweenList.Remove(_tweenList[i]);
                }
            }

        }

        public void AddTween(GameObject resourceValue, float targetValue, float _startTime, float time,Color useCustomColor, MogoTweenType tweenType = MogoTweenType.SUB)
        {
            _tweenList.Add(new MogoTween(resourceValue, targetValue, _startTime,time,useCustomColor, tweenType));
        }



    }
}
