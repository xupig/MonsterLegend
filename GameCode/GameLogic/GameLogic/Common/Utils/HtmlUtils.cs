﻿#region 模块信息
/*==========================================
// 文件名：HtmlUtils
// 命名空间: GameLogic.GameLogic.Common.Utils
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/10/21 10:16:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Common.Utils
{
    public class HtmlUtils
    {
       
       

        private Regex _leftTagPattern = new Regex(@"(?<color><color=#[a-f0-9]{6}>)|(?<size><size=\d+>)|(?<b><b>)|(?<i><i>)", RegexOptions.IgnoreCase);
        private Regex _rightTagPattern = new Regex(@"(?<color></color>)|(?<size></size>)|(?<b></b>)|(?<i></i>)", RegexOptions.IgnoreCase);

        private Dictionary<int, string> leftDict = new Dictionary<int, string>();
        private Dictionary<int, string> rightDict = new Dictionary<int, string>();

        public Dictionary<int, string> contentDict = new Dictionary<int, string>();
        private Dictionary<int, List<int>> leftTagDict = new Dictionary<int, List<int>>();
        private Dictionary<int, List<int>> rightTagDict = new Dictionary<int, List<int>>();

        private Stack<int> tagStack = new Stack<int>();

        private Dictionary<int, List<int>> leftTagWaitDict = new Dictionary<int, List<int>>();
        private StringBuilder sb = new StringBuilder();

        private string _currentContent = null;
        private int _currentKey = -1;
        private int _currentIndex = 0;


        private int _contentCount = 0;
        private int _currentStartIndex = 0;
        private StringBuilder _sb = new StringBuilder();

        public void Clear()
        {
            leftDict.Clear();
            rightDict.Clear();
            contentDict.Clear();
            leftTagDict.Clear();
            rightTagDict.Clear();
            tagStack.Clear();
            leftTagWaitDict.Clear();
        }

        private int parseLength;
        private int endKey = 0;
        private int endKeyIndex = 0;
        public void GetContentStack(string content)
        {
            parseLength = 0;
            MatchCollection c = _leftTagPattern.Matches(content, 0);
            MatchCollection c2 = _rightTagPattern.Matches(content, 0);
            for (int i = 0; i < c.Count; i++)
            {
                leftDict.Add(c[i].Index, c[i].Value);
                rightDict.Add(c2[i].Index, c2[i].Value);
            }
            int start = 0;
            int total = 0;
            bool isNew = false;
            if (sb.Length > 0)
            {
                sb.Remove(0, sb.Length);
            }
            while (start < content.Length)
            {
                if (leftDict.ContainsKey(start))
                {
                    tagStack.Push(start);
                    start += leftDict[start].Length;
                    isNew = true;
                }
                else if (rightDict.ContainsKey(start))
                {
                    if (leftTagDict.ContainsKey(total) == false)
                    {
                        leftTagDict.Add(total, new List<int>());
                        rightTagDict.Add(total, new List<int>());
                    }
                    int leftTag = tagStack.Pop();
                    if (leftTagWaitDict.ContainsKey(leftTag))
                    {
                        List<int> list = leftTagWaitDict[leftTag];
                        for (int i = 0; i < list.Count; i++)
                        {
                            if (leftTagDict.ContainsKey(list[i]) == false)
                            {
                                leftTagDict.Add(list[i], new List<int>());
                                rightTagDict.Add(list[i], new List<int>());
                            }
                            leftTagDict[list[i]].Insert(0, leftTag);
                            rightTagDict[list[i]].Add(start);
                        }
                        leftTagWaitDict.Remove(leftTag);
                    }
                    leftTagDict[total].Insert(0, leftTag);
                    if (tagStack.Count > 0)
                    {
                        foreach (int value in tagStack)
                        {
                            if (leftTagWaitDict.ContainsKey(value) == false)
                            {
                                leftTagWaitDict.Add(value, new List<int>());
                            }
                            leftTagWaitDict[value].Add(total);
                        }
                    }
                    rightTagDict[total].Add(start);
                    isNew = true;
                    start += rightDict[start].Length;
                }
                else
                {
                    if (isNew)
                    {
                        if (sb != null && sb.Length > 0)
                        {
                            contentDict.Add(total, sb.ToString());
                            parseLength += sb.ToString().Length;
                            endKey = total;
                            endKeyIndex = sb.ToString().Length-1;
                            total += sb.Length;
                        }
                        if (sb.Length > 0)
                        {
                            sb.Remove(0, sb.Length);
                        }
                        isNew = false;
                        if (tagStack.Count > 0)
                        {
                            foreach (int value in tagStack)
                            {
                                if (leftTagWaitDict.ContainsKey(value) == false)
                                {
                                    leftTagWaitDict.Add(value, new List<int>());
                                }
                                leftTagWaitDict[value].Add(total);
                            }
                        }
                    }
                    sb.Append(content.Substring(start, 1));
                    start++;
                }
            }
            if (sb != null && contentDict.ContainsKey(total) == false)
            {
                contentDict.Add(total, sb.ToString());
                parseLength += sb.ToString().Length;
                endKey = total;
                endKeyIndex = sb.ToString().Length - 1;
            }
        }

        public void End(StateText _txt,string content)
        {
            if(parseLength<109)
            {
                _txt.CurrentText.text = content;
            }
            else
            {
                _currentStartIndex = (parseLength / 109) * 109;
                _currentIndex = endKeyIndex+1;
                _currentKey = endKey;
                SetContent();
                _txt.CurrentText.text = _sb.ToString();
            }
        }

        public void ClearContent()
        {
            _currentContent = null;
            _currentKey = -1;
            _currentStartIndex = 0;
            _contentCount = 0;
        }

        private void GetContent()
        {
            int key = _currentKey;
            _currentContent = null;
            _currentKey = -1;

            foreach (KeyValuePair<int, string> kvp in contentDict)
            {
                if (kvp.Key > key)
                {
                    _currentContent = kvp.Value;
                    _currentKey = kvp.Key;
                    return;
                }
            }
        }

        private void SetContent()
        {
            _sb.Remove(0, _sb.Length);
            _currentLength = 0;
            foreach (KeyValuePair<int, string> kvp in contentDict)
            {
                if (leftTagDict.ContainsKey(kvp.Key))
                {
                    for (int j = 0; j < leftTagDict[kvp.Key].Count; j++)
                    {
                        _sb.Append(leftDict[leftTagDict[kvp.Key][j]]);
                    }
                }
                if (kvp.Key != _currentKey)
                {
                    if (_currentLength>=_currentStartIndex)
                    {
                        _sb.Append(contentDict[kvp.Key]);
                    }
                    _currentLength += contentDict[kvp.Key].Length;
                }
                else
                {
                    try
                    {
                        int index = _currentStartIndex - _currentLength;
                        if(index>0)
                        {
                            if ((index - 1) < _currentIndex)
                            {
                                _sb.Append(contentDict[kvp.Key].Substring(index - 1, _currentIndex-index+1));
                            }
                        }
                        else
                        {
                            _sb.Append(contentDict[kvp.Key].Substring(0, _currentIndex));
                        }
                        _currentLength += contentDict[kvp.Key].Substring(0, _currentIndex).Length;
                    }
                    catch (Exception ex)
                    {
                       
                    }
                }
                if (rightTagDict.ContainsKey(kvp.Key))
                {
                    for (int j = 0; j < rightTagDict[kvp.Key].Count; j++)
                    {
                        _sb.Append(rightDict[rightTagDict[kvp.Key][j]]);
                    }
                }
                if (kvp.Key == _currentKey)
                {
                    return;
                }
            }
        }

        public void ContinuePlay(StateText _txt)
        {
            _currentIndex++;
            _contentCount++;
            SetContent();
            _txt.CurrentText.text = _sb.ToString();
            isPause = false;
        }

        public bool isPause = false;

        private int _currentLength = 0;
        public void Next(StateText _txt,Action callback)
        {
            if(isPause)
            {
                return;
            }
            if (_currentContent == null || _currentContent.Length <= _currentIndex)
            {
                GetContent();
                _currentIndex = 0;
            }
            if (_currentKey == -1)
            {
                callback.Invoke();
            }
            else
            {
                _currentIndex++;
                _contentCount++;
                if (_contentCount % 109 == 0 && _contentCount > 0)
                {
                    
                    isPause = true;
                    _currentStartIndex = _contentCount;
                    _currentIndex--;
                    _contentCount--;
                    return;
                }
                SetContent();
                _txt.CurrentText.text = _sb.ToString();
            }
        }

        public int GetLength()
        {
            int total = 0;
            foreach(KeyValuePair<int,string> kvp in contentDict)
            {
                total += kvp.Value.Length;
            }
            return total;
        }

        public string GetString(int length)
        {
            for(int i=0;i<length;i++)
            {
                if (_currentContent == null || _currentContent.Length <= _currentIndex)
                {
                    GetContent();
                    _currentIndex = 0;
                }
                if (_currentKey != -1)
                {
                    _currentIndex++;
                    SetContent();
                }
                else
                {
                    return _sb.ToString();
                }
            }
            return _sb.ToString();
        }

    }
}
