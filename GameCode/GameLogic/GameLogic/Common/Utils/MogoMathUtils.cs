﻿

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/20 9:53:53 
 * function: 
 * *******************************************************/

using UnityEngine;
namespace Common.Utils
{
    public class MogoMathUtils
    {
        //生成非重复随机序列
        public static int[] RandomSequence(int min, int max)
        {
            int x = 0;
            int temp = 0;
            if (min > max)
            {
                temp = min;
                min = max;
                max = temp;
            }
            int[] arr = new int[max - min + 1];
            for (int i = min; i <= max; ++i)
            {
                arr[i - min] = i;
            }
            for (int i = arr.Length - 1; i > 0; --i)
            {
                x = Random(0, i + 1);
                temp = arr[i];
                arr[i] = arr[x];
                arr[x] = temp;
            }
            return arr;
        }

        //加权随机数
        public static int WeightedRandom(float[] weights)
        {
            if (weights.Length == 0)
            {
                return -1;
            }
            float totalWeight = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                totalWeight += weights[i];
            }
            float random = Random(0, totalWeight);
            float sum = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                sum += weights[i];
                if (random <= sum)
                {
                    return i;
                }
            }
            return weights.Length - 1;
        }

        //加权随机数
        public static int WeightedRandom(int[] weights)
        {
            if (weights.Length == 0)
            {
                return -1;
            }
            int totalWeight = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                totalWeight += weights[i];
            }
            int random = Random(0, totalWeight + 1);
            int sum = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                sum += weights[i];
                if (random <= sum)
                {
                    return i;
                }
            }
            return weights.Length - 1;
        }

        public static int Random(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public static float Random(float min, float max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public static uint Random(uint min, uint max)
        {
            return (uint)Random((int)min, (int)max);
        }

        //弧度转角度
        public static float Radian2Angle(float radian)
        {
            return radian * 180f / Mathf.PI;
        }

        //角度转弧度
        public static float Angle2Radian(float angle)
        {
            return angle * Mathf.PI / 180f;
        }
    }
}
