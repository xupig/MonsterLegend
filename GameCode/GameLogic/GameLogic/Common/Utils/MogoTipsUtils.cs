﻿using Common.Data;
using Common.Global;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Utils
{
    public class MogoTipsUtils
    {
        //准备弃用
        public static void ShowBindDiamondTips(int bindDianmondNum, string enoughBindDiamondContent, string notEnoughBindDiamondContent, Action SureCallback)
        {
            int playerBindDiamondNum = PlayerAvatar.Player.money_bind_diamond;
            string boxContent;
            if (playerBindDiamondNum >= bindDianmondNum)
            {
                boxContent = enoughBindDiamondContent;
            }
            else
            {
                boxContent = string.Format(notEnoughBindDiamondContent, bindDianmondNum - playerBindDiamondNum);
            }
            //Ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, boxContent, SureCallback);
        }

        public static string DiamondName = string.Empty;
        public static string BindDiamondName = string.Empty;

        public static void ShowTips(BaseItemData baseItemData, string content, Action SureCallback)
        {
            switch(baseItemData.Id)
            {
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    ShowBindDiamondTips(baseItemData, content, SureCallback);
                    break;
                default:
                    Debug.LogError("ShopTips还未实现Id为:" + baseItemData.Id);
                    break;
            }
        }


        private static void ShowBindDiamondTips(BaseItemData baseItemData, string srcContent, Action SureCallback)
        {
            InitItemName();
            int myBindDiamond = PlayerAvatar.Player.money_bind_diamond;
            int myDiamond = PlayerAvatar.Player.money_diamond;
            string content;
            if (myBindDiamond >= baseItemData.StackCount)
            {
                content = string.Format(srcContent, string.Concat(baseItemData.StackCount, BindDiamondName));
            }
            else if (myDiamond + myBindDiamond >= baseItemData.StackCount)
            {
                if(myBindDiamond > 0)
                {
                    content = string.Concat(MogoLanguageUtil.GetContent(5648), ",", string.Format(srcContent, string.Concat(myBindDiamond, BindDiamondName, ",", baseItemData.StackCount - myBindDiamond, DiamondName)));
                }
                else
                {
                    content = string.Concat(MogoLanguageUtil.GetContent(5648), ",", string.Format(srcContent, string.Concat(baseItemData.StackCount - myBindDiamond, DiamondName)));
                }
            }
            else
            {
                content = string.Format(MogoLanguageUtil.GetContent(5649),
                    baseItemData.StackCount, baseItemData.Name, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(5650)));
                //Ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content);
                return;
            }
            //Ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, SureCallback);
        }

        private static void InitItemName()
        {
            if (string.IsNullOrEmpty(DiamondName))
            {
                //Ari DiamondName = item_helper.GetName(public_config.MONEY_TYPE_DIAMOND);
                //Ari BindDiamondName = item_helper.GetName(public_config.MONEY_TYPE_BIND_DIAMOND);
            }
        }
    }
}
