﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using System.Security;
using System.Text.RegularExpressions;
using GameLoader.Utils;
using GameMain.GlobalManager;
using Common.Base;
using UnityEngine.UI;
using Game.UI.UIComponent;
using Common.ServerConfig;
using Common.ExtendComponent;
using Common.Utils;
using GameData;
using GameMain.Entities;
using GameLoader.Config;

namespace Mogo.Util
{
    public enum LayerMask
    {
        Default = 1,
        Character = 256,
        Terrain = 512,
        Monster = 2048,
        Npc = 4096,
        Trap = 131072,
        Mercenary = 262144,
    }

    /// <summary>
    /// 游戏逻辑相关通用工具类。
    /// </summary>
    public static class MogoUtils
    {
        #region 范围距离判断
        static public bool GetPointInTerrain(float x, float z, out Vector3 point)
        {
            //Mogo.Util.LoggerHelper.Debug("GetPointInTerrain");
            RaycastHit hit;
            //var flag = Physics.Raycast(new Vector3(x, 500, z), Vector3.down, out hit,1000, (int)LayerMask.Terrain);
            var flag = Physics.Linecast(new Vector3(x, 1000, z), new Vector3(x, -1000, z), out hit, (int)LayerMask.Terrain);
            if (flag)
            {
                //Mogo.Util.LoggerHelper.Debug("hit.point: " + hit.point);
                point = new Vector3(hit.point.x, hit.point.y + 0.2f, hit.point.z);
                return true;
            }
            else
            {
                point = new Vector3(x, 50, z);
                //Mogo.Util.LoggerHelper.Debug("hit nothing: " + x + " " + z);
                LoggerHelper.Warning("hit noting:" + x + "," + z);
                return false;
            }

        }

        static public bool CrossPointOld(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
        {
            //Vector3 rst = Vector3.zero;
            float d = (v2.z - v1.z) * (v4.x - v3.x) - (v4.z - v3.z) * (v2.x - v1.x);
            if (d == 0)
            {//平行线
                return false;
            }
            float x0 = ((v2.x - v1.x) * (v4.x - v3.x) * (v3.z - v1.z) + (v2.z - v1.z) * (v4.x - v3.x) * v1.x - (v4.z - v3.z) * (v2.x - v1.x) * v3.x) / d;
            float y0 = ((v2.z - v1.z) * (v4.z - v3.z) * (v3.x - v1.x) + (v2.x - v1.x) * (v4.z - v3.z) * v1.z - (v4.x - v3.x) * (v2.z - v1.z) * v3.z) / -d;
            if (((x0 - v1.x) * (x0 - v2.x) <= 0) &&
                ((x0 - v3.x) * (x0 - v4.x) <= 0) &&
                ((y0 - v1.z) * (y0 - v2.z) <= 0) &&
                ((y0 - v3.z) * (y0 - v4.z) <= 0))
            {
                return true;
            }

            return false;
        }

        static private float mulpti(Vector3 ps, Vector3 pe, Vector3 p)
        {
            float m;
            m = (pe.x - ps.x) * (p.z - ps.z) - (p.x - ps.x) * (pe.z - ps.z);
            return m;
        }


        static public bool CrossPoint(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
        {
            if (Math.Max(v1.x, v2.x) >= Math.Min(v3.x, v4.x) &&
                Math.Max(v3.x, v4.x) >= Math.Min(v1.x, v2.x) &&
                Math.Max(v1.z, v2.z) >= Math.Min(v3.z, v4.z) &&
                Math.Max(v3.z, v4.z) >= Math.Min(v1.z, v2.z) &&
                mulpti(v1, v2, v3) * mulpti(v1, v2, v4) <= 0 &&
                mulpti(v3, v4, v1) * mulpti(v3, v4, v2) <= 0)
                return true;
            else
                return false;
        }

        static public bool InRect(Vector3 p, Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
        {//检测 p点在v0, v1, v2, v3构成的四边形中
            bool rst = false;
            int cnt = 0; //交点计数
            Vector3 p1 = p + new Vector3(50, 0, 0);
            if (CrossPoint(p, p1, v0, v1))
            {
                cnt++;
            }
            if (CrossPoint(p, p1, v1, v2))
            {
                cnt++;
            }
            if (CrossPoint(p, p1, v2, v3))
            {
                cnt++;
            }
            if (CrossPoint(p, p1, v3, v0))
            {
                cnt++;
            }
            if (cnt == 1)
            {
                rst = true;
            }
            return rst;
        }

        public static void UseDiamondReplaceBindDiamond(int money, Action confirmAction, Action cancelAction)
        {
            string content = MogoLanguageUtil.GetContent(110526, money - PlayerAvatar.Player.money_bind_diamond, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND));
      //ari MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
        }

        /// <summary>
        /// 由近到远排序
        /// </summary>
        /// <param name="t"></param>
        /// <param name="gos"></param>
        /// <param name="count">返回数量</param>
        /// <returns></returns>
        static public void SortByDistance(this Transform t, List<GameObject> gos)
        {
            gos.Sort(delegate(GameObject a, GameObject b)
            {
                Vector3 aPos = a.transform.position;
                Vector3 bPos = b.transform.position;
                if (Vector3.Distance(t.position, aPos) >= Vector3.Distance(t.position, bPos)) return 1;
                else return -1;
            });

        }

        static public void SortByDistance(this Transform t, List<Transform> gos)
        {
            gos.Sort(delegate(Transform a, Transform b)
            {
                Vector3 aPos = a.position;
                Vector3 bPos = b.position;
                if (Vector3.Distance(t.position, aPos) >= Vector3.Distance(t.position, bPos)) return 1;
                else return -1;
            });

        }


        /// <summary>
        /// 支持深层孩子
        /// </summary>
        static public Transform GetChild(Transform transform, string boneName)
        {
            Transform child = transform.FindChild(boneName);
            if (child == null)
            {
                foreach (Transform c in transform)
                {
                    child = GetChild(c, boneName);
                    if (child != null) return child;
                }
            }
            return child;
        }

        /// <summary>
        /// 得到所有孩子，无层次限制
        /// </summary>
        static public List<Transform> GetAllChild(Transform transform)
        {
            List<Transform> children = new List<Transform>();
            children.AddRange(transform.GetComponentsInChildren<Transform>());
            foreach (Transform child in children)
            {
                children.AddRange(GetAllChild(child));
            }

            return children;
        }

        #endregion

        #region 获取设备参数

        /// <summary>
        /// 获取设备参数。
        /// </summary>
        /// <returns></returns>
        public static string GetDeviceInfo()
        {
            var props = typeof(SystemInfo).GetProperties();
            var needed = new Dictionary<string, string>();
            var sb = new System.Text.StringBuilder();
            foreach (var item in props)
            {
                sb.AppendFormat("{0}: {1}\n", needed[item.Name], item.GetGetMethod().Invoke(null, null));
            }

            return sb.ToString();
        }

        #endregion

        public static void AdaptScreen(StateImage img)
        {
            if(img.gameObject.GetComponent<UIImageResizer>() == null)
            {
                img.gameObject.AddComponent<UIImageResizer>();
            }
            img.SetDimensionsDirty();
            RectTransform rect = img.CurrentImage.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(-1, 1) - GetPosition2D(img.transform.parent.gameObject);
            rect.sizeDelta = new Vector2(UIManager.CANVAS_WIDTH + 2, UIManager.CANVAS_HEIGHT + 2);
        }


        public static string[] CopyGameObjectPath(Transform transform)
        {
            GameObject go = transform.gameObject;
            List<Transform> transformList = new List<Transform>();
            while (transform != null)
            {
                transformList.Add(transform);
                transform = transform.parent;
            }
            string path = "";
            for (int i = 0; i < transformList.Count - 1; i++)
            {
                if (i == 0)
                {
                    path = transformList[i].name;
                }
                else
                {
                    path = transformList[i].name + "/" + path;
                }
            }
            bool isPointerClick = false;
            MonoBehaviour[] components = go.GetComponents<MonoBehaviour>();
            bool isButton = false;
            bool isToggle = false;
            for (int i = 0; i < components.Length; i++)
            {
                if (components[i].enabled)
                {
                    Type[] types = components[i].GetType().GetInterfaces();
                    for (int j = 0; j < types.Length; j++)
                    {
                        if (types[j].Name == "IPointerClickHandler")
                        {
                            isPointerClick = true;
                        }
                    }
                    if (components[i] is Button)
                    {
                        isButton = true;
                    }
                    else if (components[i] is Toggle)
                    {
                        isToggle = true;
                    }
                }
            }
            if (isPointerClick)
            {
                if (isButton)
                {
                    return new string[] { path, "Button" };
                }
                else if (isToggle)
                {
                    return new string[] { path, "Toggle" };
                }
                else
                {
                    return new string[] { path, "Container" };
                }
            }
            else
            {
                Debug.LogError("无效路径:" + path);
                return null;
            }
        }


        public static void FloatTips(int langId,params object[] param)
        {
            //Ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, langId.ToLanguage(param), PanelIdEnum.MainUIField);
        }


        static public Vector3 ConvertWorldPos(Camera fromCamera, Camera toCamera, Vector3 pos)
        {
            return toCamera.ScreenToWorldPoint(fromCamera.WorldToScreenPoint(pos));
        }

        /// <summary>
        /// 挂载object在某父上并保持本地坐标、转向、大小不变
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public static void MountToSomeObjWithoutPosChange(Transform child, Transform parent)
        {
            Vector3 scale = child.localScale;
            Vector3 position = child.localPosition;
            Vector3 angle = child.localEulerAngles;
            child.parent = parent;
            child.localScale = scale;
            child.localEulerAngles = angle;
            child.localPosition = position;
        }

        public static void PrintLog(string log)
        {
            if (SystemConfig.printUILog)
            {
                LoggerHelper.Info(log);
            }
        }
        public static Vector3 GetPosition3D(GameObject go)
        {
            GameObject root = UIManager.Instance.UIRoot;
            Vector3 vector3 = Vector3.zero;
            while(go!=root)
            {
                RectTransform rect = go.GetComponent<RectTransform>();
                Vector3 offset = Vector3.zero;
                offset.x = -rect.sizeDelta.x * rect.pivot.x;
                offset.y = rect.sizeDelta.y * (1-rect.pivot.y);
                vector3 += rect.anchoredPosition3D + offset;
                go = go.transform.parent.gameObject;
            }
            return vector3;
        }


        public static Vector2 GetPosition2D(GameObject go)
        {
            GameObject root = UIManager.Instance.UIRoot;
            Vector2 vector3 = Vector2.zero;
            while (go.transform.parent != null && go.transform.parent.gameObject != root)
            {
                RectTransform rect = go.GetComponent<RectTransform>();
                Vector2 offset = Vector2.zero;
                offset.x = -rect.sizeDelta.x * rect.pivot.x;
                offset.y = rect.sizeDelta.y * (1 - rect.pivot.y);
                if (go.transform.parent.parent.gameObject == root)
                {
                    BasePanel panel = go.GetComponent<BasePanel>();
                    if (panel != null && panel.hasMovePos)
                    {
                        offset.y += 2000;
                    }
                }
                vector3 += rect.anchoredPosition + offset;
                go = go.transform.parent.gameObject;
            }
            return vector3;
        }
        

        #region 内存检测

        private static long m_currentAvailMemory = 200;

        public static long CurrentAvailMemory
        {
            get { return m_currentAvailMemory; }
        }

        public static void BeginUpdateAvailMemory()
        {
#if UNITY_ANDROID
            /*
            if (!Application.isEditor)
                TimerHeap.AddTimer(1000, 20000, () =>
                {
                    m_currentAvailMemory = MogoUtils.NormalizeMemory(AndroidUtils.Instance.GetAvailMemory());
                    if (m_currentAvailMemory > 200)
                        SystemConfigEx.Instance.SynCreateRequestCount = 5;
                    else if (m_currentAvailMemory > 150)
                        SystemConfigEx.Instance.SynCreateRequestCount = 3;
                    else if (m_currentAvailMemory > 100)
                        SystemConfigEx.Instance.SynCreateRequestCount = 2;
                    else
                        SystemConfigEx.Instance.SynCreateRequestCount = 1;
                    //LoggerHelper.Info(string.Format("AvailMemory: {0}, createCount: {1}", m_currentAvailMemory, SystemConfigEx.Instance.SynCreateRequestCount));
                });*/
#endif
        }

        static public long NormalizeMemory(long byteData)
        {
            return byteData / (1024 * 1024);
        }

        #endregion
    }
}