﻿#region 模块信息
/*==========================================
// 文件名：MogoPlayerUtils
// 命名空间: GameLogic.GameLogic.Common.Utils
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 16:15:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Utils
{
    public class MogoPlayerUtils
    {

        private static int avatarIcon = 610;
        private static int smallIcon = 620;
        public const int VOCATION_WARRIOR_ICON_ID = 625;
        public const int VOCATION_GUNNER_ICON_ID = 626;
        public const int VOCATION_MASTER_ICON_ID = 627;

        public static int GetAvatarIcon(int vocation)
        {
            return avatarIcon + vocation;
        }

        public static int GetSmallIcon(int vocation)
        {
            return smallIcon + vocation;
        }

        public static int GetVocationIcon(int vocation)
        {
            switch ((PlayerVocationType)vocation)
            {
                case PlayerVocationType.Warrior:
                    return VOCATION_WARRIOR_ICON_ID;
                case PlayerVocationType.Gunner:
                    return VOCATION_GUNNER_ICON_ID;
                case PlayerVocationType.Master:
                    return VOCATION_MASTER_ICON_ID;
                default:
                    return VOCATION_WARRIOR_ICON_ID;
            }
        }

    }

    public enum PlayerVocationType
    {
        Warrior = 1,
        Master = 3,
        Gunner = 4
    }
}
