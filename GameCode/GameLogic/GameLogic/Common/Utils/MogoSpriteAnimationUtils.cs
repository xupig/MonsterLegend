﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Game.Asset;
using LitJson;
using Common.Global;
using Object = UnityEngine.Object;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Game.UI;
using GameLoader.Config;

namespace Common.Utils
{
    /// <summary>
    /// 用作表情动画
    /// </summary>
    public class MogoSpriteAnimationUtils
    {
        private static string ATLAS_JSON_PATH = string.Format("Atlas/{0}/Atlas.json", SystemConfig.Language);
        private static string HOLDER_NAME = "Animation";
        private static bool _isJsonStartLoad = false;
        private static Dictionary<string, List<string>> _spriteAtlasDict; //Key 为SpriteName, Value 为绑定Atlas的prefab的逻辑路径
        private static Dictionary<GameObject, ParamsRecord> _paramsRecordDict = new Dictionary<GameObject, ParamsRecord>();

        public static void AddAnimation(GameObject parent, string[] spriteNameList, float width, float height)
        {
            if(_isJsonStartLoad == false)
            {
                LoadAtlasJson();
                _isJsonStartLoad = true;
            }
            if(string.IsNullOrEmpty(spriteNameList[0]) == false)
            {
                GameObject holder = AddHolder(parent, width, height);
                _paramsRecordDict.Add(holder, new ParamsRecord(holder, spriteNameList));
                LoadAtlasPrefab(holder);
            }
        }

        private static GameObject AddHolder(GameObject parent, float width, float height)
        {
            GameObject holder = new GameObject();
            holder.AddComponent<RectTransform>();
            holder.name = HOLDER_NAME;
            holder.transform.SetParent(parent.transform);
            RectTransform rect = holder.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, 0);
            rect.sizeDelta = new Vector2(width, height);
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.localPosition = Vector3.zero;
            rect.localScale = Vector3.one;
            return holder;
        }

        private static void LoadAtlasJson()
        {
            ObjectPool.Instance.GetTextAsset(ATLAS_JSON_PATH, OnAtlasJsonLoaded);
        }

        private static void OnAtlasJsonLoaded(TextAsset asset)
        {
            _spriteAtlasDict = JsonMapper.ToObject<Dictionary<string, List<string>>>(asset.text);
            foreach(KeyValuePair<GameObject, ParamsRecord> kvp in _paramsRecordDict)
            {
                LoadAtlasPrefab(kvp.Key);
            }
        }

        private static void LoadAtlasPrefab(GameObject holder)
        {
            if(_spriteAtlasDict != null)
            {
                string spriteName = _paramsRecordDict[holder].spriteNameList[0];
                if(_spriteAtlasDict.ContainsKey(spriteName) == true)
                {
                    string atlasPrefabPath = _spriteAtlasDict[spriteName][0];
                    ObjectPool.Instance.GetGameObject(atlasPrefabPath, delegate(GameObject go) { OnAtlasPrefabLoaded(holder, go); });
                }
                else
                {
                    Debug.LogError(string.Format("Key {0} not found in atlas JSON {1}", spriteName, ATLAS_JSON_PATH));
                }
            }
        }

        private static void OnAtlasPrefabLoaded(GameObject holder, GameObject go)
        {
            if(holder != null)
            {
                ParamsRecord record = _paramsRecordDict[holder];
                go.transform.SetParent(holder.transform);
                go.SetActive(true);
                RectTransform rect = go.GetComponent<RectTransform>();
                rect.localScale = Vector3.one;
                rect.localPosition = Vector3.zero;
                rect.sizeDelta = holder.GetComponent<RectTransform>().sizeDelta;
                SpritePlayer player = go.AddComponent<SpritePlayer>();
                player.spriteNameList = record.spriteNameList;
                player.spriteAtlasDict = _spriteAtlasDict;
                ImageWrapper image = go.GetComponent<ImageWrapper>();
                Color color = image.color;
                color.a = 0.0f;
                image.color = color;
                _paramsRecordDict.Remove(holder);
            }
        }

        private class ParamsRecord
        {
            public GameObject holder;
            public string[] spriteNameList;

            public ParamsRecord(GameObject holder, string[] spriteNameList)
            {
                this.holder = holder;
                this.spriteNameList = spriteNameList;
            }
        }

        private class SpritePlayer : UIBehaviour
        {
            public Dictionary<string, List<string>> spriteAtlasDict;
            public string[] spriteNameList;

            private ImageWrapper _image;
            private int _spriteIndex = 0;

            protected override void Awake()
            {
                _image = GetComponent<ImageWrapper>();
            }

            protected void Update()
            {
                string spriteKey = string.Format(_image.spriteKeyTemplate, spriteNameList[_spriteIndex]);
                _image.sprite = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
                _spriteIndex++;
                if(_spriteIndex == spriteNameList.Length)
                {
                    _spriteIndex = 0;
                }
            }

            private void FixPosition(string spriteName)
            {
                List<string> param = spriteAtlasDict[spriteName];
                RectTransform rect = GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(int.Parse(param[1]), -int.Parse(param[2]));
                rect.sizeDelta = new Vector2(int.Parse(param[3]), int.Parse(param[4]));
            }
        }

    }
}
