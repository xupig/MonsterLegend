﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/13 22:00:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System;
using LitJson;

namespace Common.Utils
{
    public enum MogoDebugId
    {
        move = 1,
        active = 2,
        inactive = 3,
    }

    public class MogoProfiler
    {
        public MogoDebugId Id;
        public float StartTime;
        public float SumTime;
        public float Average;
        public int Count;

        public MogoProfiler(MogoDebugId id)
        {
            this.Id = id;
        }
    }

    public class MogoDebugUtils
    {
        public static Dictionary<MogoDebugId, MogoProfiler> Dict = new Dictionary<MogoDebugId, MogoProfiler>();

        public static void Print(List<int> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(list[i]);
            }
        }

        public static void Print(Dictionary<int, int> dict)
        {
            foreach (int key in dict.Keys)
            {
                Debug.LogError(string.Format("key:{0}  value:{1}", key, dict[key]));
            }
        }

        public static void Print(List<RewardData> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(string.Format("id:{0}    num:{1}", list[i].id, list[i].num));
            }
        }

        public static void Print(List<ItemData> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(string.Format("id:{0}    num:{1}", list[i].Id, list[i].StackCount));
            }
        }

        public static void Print(List<PbItem> list)
        {
            Debug.LogError(string.Format("List<PbItem> count: {0}", list.Count));
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(string.Format("id:{0}    num:{1}", list[i].item_id, list[i].item_count));
            }
        }

        public static void Print(List<PbMopUpRewards> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(string.Format("count: {0}", i));
                Print(list[i].reward);
            }
        }

        public static void Print(PbTryMissionInfo pb)
        {
            string content = string.Format("challenge_page:{0} today_mission_id:{1} vip_replace_count:{2}\n", pb.challenge_page, pb.today_mission_id, pb.vip_replace_count);
            for (int i = 0; i < pb.page_info.Count; i++)
            {
                PbTryPageInfo pageInfo = pb.page_info[i];
                content += string.Format("  page:{0}  hightest:{1} rewardCount:{2}\n", pageInfo.page, pageInfo.highest_mission_id, pageInfo.reward_info.Count);
                for (int j = 0; j < pageInfo.reward_info.Count; j++)
                {
                    PbTryMissionRewardInfo rewardInfo = pageInfo.reward_info[j];
                    content += string.Format("      missionId:{0}   count:{1}\n", rewardInfo.mission_id, rewardInfo.rewards.Count);
                }
            }
            Debug.LogError(content);
        }

        public static void Print<T1, T2>(Dictionary<T1, T2> dict)
        {
            Debug.LogError("PrintDict: count:" + dict.Count);
            foreach (T1 key in dict.Keys)
            {
                Debug.LogError(string.Format("key:{0}  value:{1}", key, dict[key].ToString()));
            }
        }

        public static void Print<T>(List<T> list)
        {
            Debug.LogError("PrintList: length:" + list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                Debug.LogError(string.Format("index:{0}  value:{1}", i, list[i].ToString()));
            }
        }

        private static string Path = string.Concat(UnityEngine.Application.persistentDataPath, ConstString.RutimeResourceConfig, "/debug.txt");

        public static void WriteObject(object source)
        {
            FileStream fileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, source);
            byte[] bytes = stream.GetBuffer();
            fileStream.SetLength(bytes.Length);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Flush();
            fileStream.Close();
            fileStream.Dispose();
            fileStream = null;
        }

        public static T ReadObject<T>() where T:class
        {
            FileStream fileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
            byte[] data = new byte[fileStream.Length];
            fileStream.Read(data, 0, data.Length);
            fileStream.Close();
            fileStream.Dispose();
            fileStream = null;
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream) as T;
        }

        public static void Start(MogoDebugId id)
        {
            MogoProfiler profiler;
            if (!Dict.ContainsKey(id))
            {
                profiler = new MogoProfiler(id);
                profiler.Count = 0;
                profiler.SumTime = 0f;
                Dict.Add(id, profiler);
            }
            profiler = Dict[id];
            profiler.StartTime = Time.realtimeSinceStartup;
        }

        public static void End(MogoDebugId id)
        {
            MogoProfiler profiler = Dict[id];
            profiler.SumTime += (Time.realtimeSinceStartup - profiler.StartTime);
            profiler.Count++;
            profiler.Average = profiler.SumTime / profiler.Count;
        }

        public static void Print(MogoDebugId id)
        {
            MogoProfiler profiler = Dict[id];
            Debug.LogError(string.Format("id:{0}    count:{1}   sum:{2}     average:{3}", id, profiler.Count, profiler.SumTime, profiler.Average));
        }

        public static void Print(PbDuelInviteTrigger pbDuelInviteTrigger)
        {
            Debug.LogError(string.Format("matchId:{0} type:{1} is_rank_mode:{2}", pbDuelInviteTrigger.match_id, pbDuelInviteTrigger.type, pbDuelInviteTrigger.is_rank_mode));
            Print(pbDuelInviteTrigger.target);
        }

        private static void Print(PbDuelTarget pbDuelTarget)
        {
            Debug.LogError(
                string.Format("dbid:{0} name:{1} level:{2} guild_name:{3} rank_show:{4} rank_level:{5} vocation:{6} fight_force:{7}", 
                pbDuelTarget.dbid, pbDuelTarget.name, pbDuelTarget.level, pbDuelTarget.guild_name, pbDuelTarget.rank_show, pbDuelTarget.rank_level, pbDuelTarget.vocation, pbDuelTarget.fight_force));
        }

        public static void Print(PbDuelInviteResp pbDuelInviteResp)
        {
            Debug.LogError(string.Format("matchId:{0} result:{1}", pbDuelInviteResp.match_id, pbDuelInviteResp.result));
        }

        public static void Print(PbDuelReadyResp pbDuelReadyResp)
        {
            Debug.LogError(string.Format("matchId:{0} self:{1} other:{2}", pbDuelReadyResp.match_id, pbDuelReadyResp.self_ready, pbDuelReadyResp.other_ready));
        }

        public static void Print(PbDuelQueryPlayerInfo pbDuelQueryPlayerInfo)
        {
            Debug.LogError(string.Format("can_refuse_type:{0} is_rank_mode:{1}", pbDuelQueryPlayerInfo.can_refuse_type, pbDuelQueryPlayerInfo.is_rank_mode));
            Print(pbDuelQueryPlayerInfo.target);
        }

        public static void Print(PbDuelMatchStateInfo pb)
        {
            Debug.Log(string.Format("match_id:{0} match_progress:{1} remain_time:{2} bet_winner:{3} bet_sponsor_num:{4} bet_invitee_num:{5} luck_mode:{6} luck_money:{7} bet_id:{8} bet_rank_side:{9}", 
                pb.match_id, pb.match_progress, pb.remain_time, pb.bet_winner, pb.bet_sponsor_num, pb.bet_invitee_num, pb.luck_mode, pb.luck_money, pb.bet_id, pb.bet_rank_side));
        }

        public static void Print(PbDuelMatchBetChange pb)
        {
            Debug.Log(string.Format("match_id:{0} luck_mode:{1} item_id:{2} item_num:{3}",
                pb.match_id, pb.luck_mode, pb.item_id, pb.item_num));
        }

        public static void Print(PbMissionResetTimesRecord pbMissionResetTimesRecord)
        {
            Debug.LogError("first count:" + pbMissionResetTimesRecord.reset_times_info.Count);
            for (int i = 0; i < pbMissionResetTimesRecord.reset_times_info.Count; i++)
            {
                var first = pbMissionResetTimesRecord.reset_times_info[i];
                Debug.LogError(string.Format("type:{0}   missionCount:{1}", first.mission_type, first.mission_reset_times.Count));
                string content = "";
                for (int j = 0; j < first.mission_reset_times.Count; j++)
                {
                    content += string.Format("missionId:{0} count:{1}", first.mission_reset_times[j].mission_id, first.mission_reset_times[j].count);
                }
                Debug.LogError(content);
            }
        }

        public static void Print(PbGuildBattleDrawResultInfoList pb)
        {
            string result = string.Empty;
            result += "drawed_times:" + pb.drawed_times;
            AddEndOfLine(ref result);
            result += "count:" + pb.list.Count;
            AddEndOfLine(ref result);
            for (int i = 0; i < pb.list.Count; i++)
            {
                PbGuildBattleDrawResultInfo info = pb.list[i];
                result += string.Format("index:{0} group:{1}, guildName1:{2} guildName2:{3} guildName3:{4} guildName4:{5}\n", i , info.group, info.guild1_name, info.guild2_name, info.guild3_name, info.guild4_name);
            }
            Debug.LogError(result);
        }

        private static void AddEndOfLine(ref string result)
        {
            result += "\n";
        }

        public static void Print(PbGuildBattleDrawResult pb)
        {
            string result = string.Format("name:{0}, index:{1} count:{2}", pb.name, pb.index, pb.pos);
            Debug.LogError(result);
        }
    }
}
