﻿using Game.Asset;
using Game.UI;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using GameData;
using Common.ServerConfig;

namespace Common.Utils
{
    public class MogoShaderUtils
    {
        public const string IconShaderPath = "Shader$PSD2UGUI_ICON.u";
        public const string IconCutShaderPath = "Shader$PSD2UGUI_ICON_CUT.u";
        public const string PSD2UGUI_NODEPTH = "Shader$PSD2UGUI_NODEPTH.u";
        public const string PSD2UGUI_ETC = "Shader$PSD2UGUI.u";

        private static Dictionary<string, Material> _cachedMaterialDict = new Dictionary<string, Material>();
        private const string MATERIAL_DICT_KEY_FORMATE = "{0},{1}";

        public static void ChangeShader(Image image, string shaderPath, Material initMaterial = null)
        {
            string materialName = "";
            if (initMaterial != null)
            {
                materialName = initMaterial.name;
            }
            else
            {
                materialName = image.material.name;
            }

            Shader shader = ObjectPool.Instance.GetAssemblyShader(shaderPath);
            if (shader != null)
            {
                if (Application.isEditor && Shader.Find(shader.name) != null)
                {
                    shader = Shader.Find(shader.name);
                    LoggerHelper.Warning("Shader replace in editor:" + shader.name);
                }
            }

            string key = string.Format(MATERIAL_DICT_KEY_FORMATE, materialName, shader.name);
            if (!_cachedMaterialDict.ContainsKey(key))
            {
                Material material = null;
                if (initMaterial != null)
                {
                    material = Material.Instantiate(initMaterial) as Material;
                }
                else
                {
                    material = Material.Instantiate(image.material) as Material;
                }
                material.name = string.Format("modify_{0}", materialName);
                material.shader = shader;
                _cachedMaterialDict.Add(key, material);
                image.material = material;
            }
            else
            {
                image.material = _cachedMaterialDict[key];
            }
        }

        public static void ClearCachedMaterials()
        {
            if (_cachedMaterialDict.Count > 0)
            {
                string[] keys = _cachedMaterialDict.Keys.ToArray<string>();
                for (int i = 0; i < keys.Length; i++)
                {
                    Material material = _cachedMaterialDict[keys[i]];
                    _cachedMaterialDict.Remove(keys[i]);
                    UnityEngine.Object.Destroy(material);
                }
            }
        }

        public static void ClearAfterEnterBattleScene(int mapId)
        {
            int mapType = map_helper.GetMapType(mapId);
            if (mapType != public_config.MAP_TYPE_CITY)
            {
                ClearCachedMaterials();
            }
        }
    }
}
