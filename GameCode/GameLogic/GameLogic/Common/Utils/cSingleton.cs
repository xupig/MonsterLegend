﻿using System;
using System.Diagnostics;

namespace Common.Utils
{
    // 此单例在继承关系下会有问题，继承的父类子类根据他们访问的接口不同都会被实例化出不同的对象
    public class Singleton<T> where T : new()
    {
        protected static T _instance = default(T);
        private static Object _objLock = new System.Object();

        protected Singleton()
        {
            Debug.Assert(_instance == null);
        }

        public static void Depose()
        {
            _instance = default(T);
        }

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_objLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new T();
                        }
                    }
                }
                return _instance;
            }
        }
    }
}
