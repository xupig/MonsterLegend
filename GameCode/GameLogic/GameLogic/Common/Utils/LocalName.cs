﻿#region 模块信息
/*==========================================
// 文件名：LocalIdEnum
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/14 11:06:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Utils
{
    public enum LocalName
    {
        Task = 1,
        ChatPhrase = 2,
        Setting = 3,
        EquipEffectSetting = 4,
        Dreamland = 5,
        FastNotice = 6,
        PrivateChat = 7,
        LoginPush = 8,
        OpenServer = 9,
        RewardBuffTipShowTime = 10,
        TouchCameraScale = 11,
        TouchBattleMode = 12,
        AstarPJ_RoleLevelUp = 13,
        AstarPJ_RoleBattleUp = 14,
		KeyControlWebMode = 15,
        KeyControlLolMode = 16,
        KeyControlModeChosen = 17
    }
}
