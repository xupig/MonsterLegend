﻿using Game.Asset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using GameLoader.Config;

namespace Common.Utils
{
    public class MogoMaterialUtils
    {
        public static readonly string MallPanelMaterialPath = string.Format("GUI${0}$Sprite$MallPanel$MallPanel_etc_shadow.u", SystemConfig.Language);

        public static void CreateMaterialAndChangeShader(Image image, string materialPath, string shaderPath)
        {
            Material material = ObjectPool.Instance.GetAssemblyObject(materialPath) as Material;
            Material newMaterial = Material.Instantiate(material) as Material;
            MogoShaderUtils.ChangeShader(image, shaderPath, newMaterial);
        }
    }
}
