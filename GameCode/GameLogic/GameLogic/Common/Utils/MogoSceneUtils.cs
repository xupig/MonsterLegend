﻿using ACTSystem;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/7/28 21:14:48 
 * function: 
 * *******************************************************/

namespace Common.Utils
{
    public class MogoSceneUtils
    {
        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static Vector3 _hitPoint = Vector3.zero;
        static int _terrainLayerValue = 1 << UnityEngine.LayerMask.NameToLayer("Terrain");

        public static float GetGoundYPosition(Vector3 position)
        {
            var originPos = position;
            float highestY = 130f;
            originPos.y = highestY;
            _downRay.origin = originPos;
            Physics.Raycast(_downRay, out _raycastHit, highestY, _terrainLayerValue);
            _hitPoint.x = originPos.x;
            _hitPoint.y = _raycastHit.point.y;
            _hitPoint.z = originPos.z;
            var dis = position.y - _hitPoint.y;
            if (Application.isEditor)
            {
                Debug.DrawLine(originPos, _hitPoint, Color.red);
            }
            return _hitPoint.y;
        }
    }
}
