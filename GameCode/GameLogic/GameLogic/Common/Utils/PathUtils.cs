﻿#region 模块信息
/*==========================================
// 文件名：MogoHtmlUtils
// 命名空间: GameLogic.GameLogic.Common.Utils
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/7 14:58:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Common.Utils
{
    public static class PathUtils
    {
        static public List<float> FixPath(List<float> path)
        {
            List<float> result = new List<float>();
            Vector3 lastDirection = Vector3.zero;
            Vector3 lastPoint = Vector3.zero;
            Vector3 nextPoint = Vector3.zero;
            int pointCount = path.Count / 2;
            for (int i = 0; i < pointCount; i++)
            {
                nextPoint.x = path[i * 2];
                nextPoint.y = 0;
                nextPoint.z = path[i * 2 + 1];
                if (lastPoint == Vector3.zero)
                {
                    result.Add(nextPoint.x);
                    result.Add(nextPoint.z);
                }
                else
                {
                    var newDirection = nextPoint - lastPoint;
                    if (newDirection != lastDirection)
                    {
                        result.Add(nextPoint.x);
                        result.Add(nextPoint.z);
                        lastDirection = newDirection;
                    }
                    else if (pointCount - 1 == i)
                    {
                        result.Add(nextPoint.x);
                        result.Add(nextPoint.z);
                    }
                }
                lastPoint = nextPoint;
            }
            return result;
        }

        static public void DrawPath(List<float> path, float y = 0)
        {
            List<float> result = new List<float>();
            Vector3 lastDirection = Vector3.zero;
            Vector3 lastPoint = Vector3.zero;
            Vector3 nextPoint = Vector3.zero;
            //Debug.LogError("DrawPath: =====" + path.Count);
            for (int i = 0; i < path.Count / 2; i++)
            {
                nextPoint.x = path[i * 2];
                nextPoint.y = y;
                nextPoint.z = path[i * 2 + 1];
                if (lastPoint != Vector3.zero)
                {
                    Debug.DrawLine(lastPoint, nextPoint, Color.blue, 10);
                }
                //Debug.LogError(nextPoint);
                lastPoint = nextPoint;
            }
        }

        static public bool CanLine(Vector3 posA, Vector3 posB, bool bFly = false)
        {
            bool result = true;
            posA.y = 0;
            posB.y = 0;
            var vector = posB - posA;
            var dis = vector.magnitude;
            var dir = vector.normalized;
            float step = 0.3f;
            int count = (int)(dis / step);
            for (int i = 0; i < count; i++)
            {
                float newPosX = posA.x + (dir.x * i * step);
                float newPosZ = posA.z + (dir.z * i * step);
                if (!GameSceneManager.GetInstance().CheckCurrPointIsCanMove(newPosX, newPosZ, bFly))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}
