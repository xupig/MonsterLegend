﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Utils
{
    public class MogoRandomUtils
    {
        public static void RandomList(IList list, int start = -1, int end = -1)
        {
            if (start == -1)
            {
                start = 0;
            }
            if (end == -1)
            {
                end = list.Count;
            }
            for (int i = 0; i < list.Count * 2; i++)
            {
                int j = MogoMathUtils.Random(start, end);
                int k = MogoMathUtils.Random(start, end);
                if (j != k)
                {
                    Object temp = list[j];
                    list[j] = list[k];
                    list[k] = temp;
                }
            }
        }
    }
}
