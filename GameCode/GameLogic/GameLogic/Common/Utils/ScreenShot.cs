﻿using ACTSystem;
using ShaderUtils;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShot
{
	public static RenderTexture GetScreenShot(Camera camera)
	{
		RenderTexture screen0 = RenderTexture.GetTemporary((int)camera.pixelWidth, (int)camera.pixelHeight, 8);
		screen0.DiscardContents();
		RenderTexture camRT = camera.targetTexture;
		camera.targetTexture = screen0;
		camera.Render();
		camera.targetTexture = camRT;
		Shader shader = ShaderRuntime.loader.Find("MOGO2/PostProcess/ResetAlpha");
		Material mtrl = new Material(shader);
		mtrl.hideFlags = HideFlags.DontSave;
		screen0.filterMode = FilterMode.Point;
		RenderTexture screen1 = new RenderTexture(screen0.width, screen0.height, 0);
		screen1.DiscardContents();
		mtrl.SetFloat("_NewAlpha", 1.0f);
		Graphics.Blit(screen0, screen1, mtrl);
		RenderTexture.ReleaseTemporary(screen0);
		return screen1;
	}

	public static RenderTexture GetBackgroundBlurScreenShot(Camera camera, List<GameObject> foregroundObjList)
	{
		const float BLUR_RANGE = 0.025f;

		Shader ds = ShaderRuntime.loader.Find("MOGO2/PostProcess/Downsample4x");
		Shader gb = ShaderRuntime.loader.Find("MOGO2/PostProcess/GaussBlur7");
		Shader ra = ShaderRuntime.loader.Find("MOGO2/PostProcess/ResetAlpha");
		if (!SystemInfo.supportsRenderTextures || !ds.isSupported || !gb.isSupported || !ra.isSupported)
		{
			return null;
		}

		Material downsample = new Material(ds);
		downsample.hideFlags = HideFlags.DontSave;
		Material gaussBlur = new Material(gb);
		gaussBlur.hideFlags = HideFlags.DontSave;
		Material resetAlpha = new Material(ra);
		resetAlpha.hideFlags = HideFlags.DontSave;

		RenderTexture tmpA = RenderTexture.GetTemporary((int)camera.pixelWidth, (int)camera.pixelHeight, 16);
		RenderTexture tmpB = RenderTexture.GetTemporary((int)camera.pixelWidth / 4, (int)camera.pixelHeight / 4, 0);
		RenderTexture tmpC = RenderTexture.GetTemporary((int)camera.pixelWidth / 4, (int)camera.pixelHeight / 4, 0);
		tmpA.filterMode = FilterMode.Bilinear;
		tmpA.wrapMode = TextureWrapMode.Clamp;
		tmpB.filterMode = FilterMode.Bilinear;
		tmpB.wrapMode = TextureWrapMode.Clamp;
		tmpC.filterMode = FilterMode.Bilinear;
		tmpC.wrapMode = TextureWrapMode.Clamp;

		CameraClearFlags cf0 = camera.clearFlags;
		RenderTexture tr0 = camera.targetTexture;
		int cm0 = camera.cullingMask;
        int sepLayer = UnityEngine.LayerMask.NameToLayer("RenderSeparate");
		camera.cullingMask = camera.cullingMask & (~(1 << sepLayer));

		List<GameObject> gol = new List<GameObject>();
		List<int> layer0 = new List<int>();
		foreach (GameObject fo in foregroundObjList)
		{
			Renderer[] rdr = fo.transform.GetComponentsInChildren<Renderer>();
			foreach (Renderer r in rdr)
			{
				GameObject o = r.gameObject;
				gol.Add(o);
				layer0.Add(o.layer);
				o.layer = sepLayer;
			}
		}

		tmpA.DiscardContents();
		camera.targetTexture = tmpA;
		camera.Render();

		downsample.SetVector("_Offset", new Vector2(1.0f / (tmpB.width * 4.0f), 1.0f / (tmpB.height * 4.0f)));
		tmpB.DiscardContents();
		Graphics.Blit(tmpA, tmpB, downsample);

		gaussBlur.SetVector("_Offset", new Vector2(BLUR_RANGE / 7.0f / camera.pixelWidth * camera.pixelHeight, 0.0f));
		tmpC.DiscardContents();
		Graphics.Blit(tmpB, tmpC, gaussBlur);

		gaussBlur.SetVector("_Offset", new Vector2(0.0f, BLUR_RANGE / 7.0f));
		tmpB.DiscardContents();
		Graphics.Blit(tmpC, tmpB, gaussBlur);

		tmpA.DiscardContents();
		Graphics.Blit(tmpB, tmpA);

		camera.cullingMask = 1 << sepLayer;
		camera.clearFlags = CameraClearFlags.Nothing;
		camera.targetTexture = tmpA;
		camera.Render();

		tmpA.filterMode = FilterMode.Point;

		RenderTexture rt = new RenderTexture((int)camera.pixelWidth, (int)camera.pixelHeight, 0);
		rt.DiscardContents();
		resetAlpha.SetFloat("_NewAlpha", 1.0f);
		Graphics.Blit(tmpA, rt, resetAlpha);

		camera.targetTexture = tr0;
		camera.clearFlags = cf0;
		camera.cullingMask = cm0;
		for (int i = 0; i < gol.Count; ++i)
		{
			gol[i].layer = layer0[i];
		}

		RenderTexture.ReleaseTemporary(tmpA);
		RenderTexture.ReleaseTemporary(tmpB);
		RenderTexture.ReleaseTemporary(tmpC);

		return rt;
	}

	public static RenderTexture GetBackgroundBlurScreenShot(Camera camera, GameObject foregroundObj)
	{
		return GetBackgroundBlurScreenShot(camera, new List<GameObject>() { foregroundObj });
	}
}
