﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Game.Asset;
using Game.UI;
using LitJson;
using Common.Global;
using Object = UnityEngine.Object;
using UnityEngine.Events;
using GameData;
using Game.UI.UIComponent;
using GameLoader.Config;

namespace Common.Utils
{
    public class MogoAtlasUtils
    {
        private static string ATLAS_JSON_PATH = "Atlas/{0}/Atlas.json";
        private static bool _isJsonStartLoad = false;
        private static Dictionary<string, List<string>> _spriteAtlasDict; //Key 为SpriteName, Value 为绑定Atlas的prefab的逻辑路径
        private static Dictionary<GameObject, ParamsRecord> _paramsRecordDict = new Dictionary<GameObject, ParamsRecord>();

        public static string HOLDER_NAME = "holder";
        public static string BG_HOLDER_NAME = "bg_holder";

        private static void RemoveHolder(GameObject parent)
        {
            Transform holderTransform = parent.transform.FindChild(HOLDER_NAME);
            if(holderTransform != null)
            {
                //Ari增加:在holder所在父節點Active為false時，同一幀下創建并馬上刪除該holder時，
                //如果該holder已掛接剛創建的ImageWrapper對象，引擎就會崩毀，
                //以下這句代碼能暫時解決崩潰問題，實際崩潰原因後續再進行檢查。
                holderTransform.gameObject.SetActive(holderTransform.gameObject.activeInHierarchy);
                holderTransform.SetParent(null);
                _paramsRecordDict.Remove(holderTransform.gameObject);
                Object.Destroy(holderTransform.gameObject);
            }

            Transform bgHolderTransform = parent.transform.FindChild(BG_HOLDER_NAME);
            if(bgHolderTransform != null)
            {
                bgHolderTransform.gameObject.SetActive(bgHolderTransform.gameObject.activeInHierarchy);
                bgHolderTransform.SetParent(null);
                _paramsRecordDict.Remove(bgHolderTransform.gameObject);
                Object.Destroy(bgHolderTransform.gameObject);
            }
        }

        private static GameObject AddHolder(GameObject parent, int width, int height, string name, Vector3 pos)
        {
            GameObject holder = new GameObject();
            holder.AddComponent<RectTransform>();
            holder.name = name;
            holder.transform.SetParent(parent.transform);
            RectTransform rect = holder.GetComponent<RectTransform>();
            rect.anchoredPosition3D = pos;
            rect.sizeDelta = new Vector2(width, height);
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.localRotation = new Quaternion();
            rect.localScale = Vector3.one;
            return holder;
        }

        public static void RemoveSprite(GameObject parent)
        {
            RemoveHolder(parent);
        }

        public static void AddSprite(GameObject parent, string spriteName)
        {
            AddSprite(parent, spriteName, string.Empty, 1);
        }

        public static void AddSprite(GameObject parent, string spriteName, Action<GameObject> callback)
        {
            RectTransform rect = parent.GetComponent<RectTransform>();
            int width = (int)rect.sizeDelta.x;
            int height = (int)rect.sizeDelta.y;
            AddSprite(parent, spriteName, string.Empty, 1, width, height, callback);
        }

        public static void AddIcon(GameObject parent, int iconId, Action<GameObject> callback = null)
        {
            AddSprite(parent, icon_item_helper.GetIcon(iconId), icon_item_helper.GetIconBg(iconId), icon_item_helper.GetIconColor(iconId), callback);
        }

        public static void AddSprite(GameObject parent, string spriteName, string bgSpriteName, int colorId, Action<GameObject> callback = null)
        {
            RectTransform rect = parent.GetComponent<RectTransform>();
            int width = (int)rect.sizeDelta.x;
            int height = (int)rect.sizeDelta.y;
            AddSprite(parent, spriteName, bgSpriteName, colorId, width, height, callback);
        }

        private static void AddSprite(GameObject parent, string spriteName, string bgSpriteName, int colorId, int width, int height, Action<GameObject> callback = null)
        {
            if(_isJsonStartLoad == false)
            {
                LoadAtlasJson();
                _isJsonStartLoad = true;
            }
            RemoveHolder(parent);
            Vector3 pos = Vector3.zero;
            if(bgSpriteName != string.Empty)
            {
                GameObject bgHolder = AddHolder(parent, width, height, BG_HOLDER_NAME, Vector3.zero);
                _paramsRecordDict.Add(bgHolder, new ParamsRecord(bgSpriteName, 1, width, height, callback));
                LoadSprite(bgHolder);
            }
            if(spriteName != string.Empty)
            {
                GameObject holder = AddHolder(parent, width, height, HOLDER_NAME, pos);
                _paramsRecordDict.Add(holder, new ParamsRecord(spriteName, colorId, width, height, callback));
                LoadSprite(holder);
            }
        }

        private static void LoadSprite(GameObject holder)
        {
            if(_spriteAtlasDict != null)
            {
                string spriteName = _paramsRecordDict[holder].spriteName;
                if(_spriteAtlasDict.ContainsKey(spriteName) == true)
                {
                    string atlasPrefabPath = _spriteAtlasDict[spriteName][0];
                    ObjectPool.Instance.GetGameObject(atlasPrefabPath, delegate(GameObject go) { OnSpriteGoLoaded(holder, go); });
                }
                else
                {
                    Debug.LogError(string.Format("Key [{0}] not found in atlas JSON {1}", spriteName, string.Format(ATLAS_JSON_PATH,SystemConfig.Language)));
                }
            }
        }

        private static void OnSpriteGoLoaded(GameObject holder, GameObject go)
        {
            if (holder != null && _paramsRecordDict.ContainsKey(holder))
            {
                ParamsRecord record = _paramsRecordDict[holder];
                go.name = record.spriteName;
                ImageWrapper image = go.GetComponent<ImageWrapper>();
                string spriteKey = string.Format(image.spriteKeyTemplate, record.spriteName);
                image.sprite = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
                Color color = ColorDefine.GetColorById(record.colorId);
                if(record.colorId == 0 || record.colorId == 1) color.a = 0.0f;
                image.color = color;
                go.transform.SetParent(holder.transform);
                go.SetActive(true);
                image.SetDimensionsDirty();
                RectTransform rect = go.GetComponent<RectTransform>();
                List<string> param = _spriteAtlasDict[record.spriteName];
                float scaleX = record.width / float.Parse(param[5]);
                float scaleY = record.height / float.Parse(param[6]);
                rect.anchoredPosition3D = new Vector3(int.Parse(param[1]) * scaleX, -int.Parse(param[2]) * scaleY, 0);//锚点位置等于原图裁剪位置*缩放比
                rect.sizeDelta = new Vector2(int.Parse(param[3]) * scaleX, int.Parse(param[4]) * scaleY);//sizeDelta等于裁剪后的图片的高宽*缩放比
                rect.localScale = Vector3.one;
                rect.localRotation = new Quaternion();
                _paramsRecordDict.Remove(holder);
                if (record.callback != null)
                {
                    record.callback(go);
                }
            }
            else
            {
                Object.Destroy(go);
            }
        }

        private static void LoadAtlasJson()
        {
            ObjectPool.Instance.GetTextAsset(string.Format(ATLAS_JSON_PATH, SystemConfig.Language), OnAtlasJsonLoaded);
        }

        private static void OnAtlasJsonLoaded(TextAsset asset)
        {
            _spriteAtlasDict = JsonMapper.ToObject<Dictionary<string, List<string>>>(asset.text);

            foreach(KeyValuePair<GameObject, ParamsRecord> kvp in _paramsRecordDict)
            {
                LoadSprite(kvp.Key);
            }
        }

        private class ParamsRecord
        {
            public ParamsRecord(string spriteName, int colorId, int width, int height, Action<GameObject> callback = null)
            {
                this.spriteName = spriteName;
                this.colorId = colorId;
                this.width = width;
                this.height = height;
                this.callback = callback;
            }
            public string spriteName;
            public int colorId;
            public int width;
            public int height;
            public Action<GameObject> callback;
        }
    }
}
