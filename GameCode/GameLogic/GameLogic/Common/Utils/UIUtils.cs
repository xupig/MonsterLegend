﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Game.UI;
using GameLoader.Utils;
using UnityEngine;

namespace Common.Utils
{
    public class UIUtils
    {
        public static void SetStateImageUVScaleDir(StateImage stateImg, UVScaleImage.UVScaleDirection dir)
        {
            ImageWrapper[] imgs = stateImg.GetAllStateComponent<ImageWrapper>();
            for (int i = 0; i < imgs.Length; i++)
            {
                if (imgs[i] == null)
                    continue;
                if (imgs[i] is UVScaleImage)
                {
                    UVScaleImage uvImg = imgs[i] as UVScaleImage;
                    uvImg.Direction = dir;
                }
            }
        }

        public static void SetProgressBarScaleDir(KProgressBar progressBar, UVScaleImage.UVScaleDirection dir)
        {
            StateImage barImg = progressBar.GetChildComponent<StateImage>("bar");
            SetStateImageUVScaleDir(barImg, dir);
        }

        public static void SetSliderScaleDir(KSlider slider, UVScaleImage.UVScaleDirection dir)
        {
            StateImage fillImg = slider.GetChildComponent<StateImage>("fillArea/fillHolder/fill");
            if (fillImg == null)
            {
                LoggerHelper.Error("Can not find fill image in slider !! The path should be : fillArea/fill");
                return;
            }
            SetStateImageUVScaleDir(fillImg, dir);
        }

        public static void SetWidgetParentWithOriginalPos(GameObject widgetGo, GameObject parentGo, Camera uiCamera)
        {
            Transform transform = widgetGo.transform;
            RectTransform rect = widgetGo.GetComponent<RectTransform>();
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(uiCamera, transform.position);
            RectTransform parentRect = parentGo.GetComponent<RectTransform>();
            Vector2 destPos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, screenPos, uiCamera, out destPos);
            transform.SetParent(parentGo.transform);
            transform.localScale = Vector3.one;
            transform.localPosition = Vector3.zero;
            rect.anchoredPosition = destPos;
        }
    }
}
