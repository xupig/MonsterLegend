﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Utils
{
    public class MogoLayoutUtils
    {
        public static readonly Vector2 Vector2Half = new Vector2(0.5f, 0.5f);

        public static void ImageTextMixHorizontalLayout(params object[] objectArray)
        {
            float x = 0f;
            float gap = 3f;
            for (int i = 0; i < objectArray.Length; i++)
            {
                object obj = objectArray[i];
                if (obj is StateText)
                {
                    StateText text = obj as StateText;
                    Vector3 position = text.transform.localPosition;
                    text.transform.localPosition = new Vector3(x, position.y, position.z);
                    x += text.CurrentText.preferredWidth + gap;
                }
                else if(obj is StateImage)
                {
                    StateImage image = obj as StateImage;
                    Vector3 position = image.transform.localPosition;
                    image.transform.localPosition = new Vector3(x, position.y, position.z);
                    x += image.Width + gap;
                }
                else
                {
                    KContainer container = obj as KContainer;
                    Vector3 position = container.transform.localPosition;
                    container.transform.localPosition = new Vector3(x, position.y, position.z);
                    RectTransform rect = container.GetComponent<RectTransform>();
                    x += rect.sizeDelta.x + gap;
                }
            }
        }

        public static void ComponentHorizontalLayout(float gap, params object[] objectArray)
        {
            float x = 0f;
            for (int i = 0; i < objectArray.Length; i++)
            {
                object obj = objectArray[i];
                if (obj is StateText)
                {
                    StateText text = obj as StateText;
                    Vector3 position = text.transform.localPosition;
                    text.transform.localPosition = new Vector3(x, position.y, position.z);
                    x += text.CurrentText.preferredWidth + gap;
                }
                else if (obj is StateImage)
                {
                    StateImage image = obj as StateImage;
                    Vector3 position = image.transform.localPosition;
                    image.transform.localPosition = new Vector3(x, position.y, position.z);
                    x += image.Width + gap;
                }
                else if (obj is KButton)
                {
                    KButton button = obj as KButton;
                    Vector3 position = button.transform.localPosition;
                    button.transform.localPosition = new Vector3(x, position.y, position.z);
                    RectTransform rect = button.GetComponent<RectTransform>();
                    x += rect.sizeDelta.x + gap;
                    if (obj is KShrinkableButton)
                    {
                        (obj as KShrinkableButton).RefreshRectTransform();
                    }
                }
                else
                {
                    KContainer container = obj as KContainer;
                    Vector3 position = container.transform.localPosition;
                    container.transform.localPosition = new Vector3(x, position.y, position.z);
                    RectTransform rect = container.GetComponent<RectTransform>();
                    x += rect.sizeDelta.x + gap;
                }

            }
        }

        public static RectTransform SetMiddle(GameObject go)
        {
            RectTransform rect = go.GetComponent<RectTransform>();
            rect.pivot = new Vector2(1f, 0.5f);
            rect.anchorMax = new Vector2(1f, 0.5f);
            rect.anchorMin = new Vector2(1f, 0.5f);
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0f);
            return rect;
        }

        public static RectTransform SetCenter(GameObject go, int way = 1)
        {
            RectTransform rect = go.GetComponent<RectTransform>();
            switch (way)
            {
                case 1:
                    rect.pivot = new Vector2(0.5f, 1f);
                    rect.anchorMax = new Vector2(0.5f, 1f);
                    rect.anchorMin = new Vector2(0.5f, 1f);
                    rect.anchoredPosition = new Vector2(0, rect.anchoredPosition.y);
                    break;
                case 2:
                    RectTransform parentRect = go.transform.parent.GetComponent<RectTransform>();
                    rect.anchoredPosition = new Vector3((parentRect.sizeDelta.x - rect.sizeDelta.x) * 0.5f, rect.anchoredPosition.y);
                    break;
            }
            return rect;
        }

        public static RectTransform SetMiddleCenter(GameObject go, int way = 1)
        {
            RectTransform rect = go.GetComponent<RectTransform>();
            switch (way)
            {
                case 1:
                    rect.pivot = Vector2Half;
                    rect.anchorMax = Vector2Half;
                    rect.anchorMin = Vector2Half;
                    rect.anchoredPosition = Vector2.zero;
                    break;
                case 2:
                    rect.pivot = new Vector2(0.5f, 0.5f);
                    rect.localPosition += new Vector3(rect.sizeDelta.x * 0.5f, -rect.sizeDelta.y * 0.5f);
                    break;
            }
            return rect;
        }

        public static void RecalculateSize(GameObject go)
        {
            RectTransform rect = go.GetComponent<RectTransform>();
            float width = 0;
            float height = 0;
            for (int i = 0; i < go.transform.childCount; i++)
            {
                RectTransform childRect = go.transform.GetChild(i) as RectTransform;
                if (childRect.gameObject.activeSelf == false)
                {
                    continue;
                }
                float childX = childRect.anchoredPosition.x;
                float childY = childRect.anchoredPosition.y;
                float childWidth = childRect.sizeDelta.x;
                float childHeight = childRect.sizeDelta.y;
                if (width < (childX + childWidth))
                {
                    width = childX + childWidth;
                }
                if (height < (childHeight - childY))
                {
                    height = childHeight - childY;
                }
            }
            rect.sizeDelta = new Vector2(width, height);
        }

        public static void SetPositionX(GameObject go, float x)
        {
            Vector3 position = go.transform.localPosition;
            go.transform.localPosition = new Vector3(x, position.y, position.z);
        }
    }
}
