﻿
using UnityEngine;
using System.Collections;
using ShaderUtils;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class Bloom : MonoBehaviour
{
	public Color _BrightThreshhold = new Color(0.608f, 0.608f, 0.608f);
	public float _SpreadRange = 0.04f;
	public float _BloomIntensity = 2.5f;

	private Material _DownsampleMax4x;
	private Material _GetBright;
	private Material _GaussBlur;
	private Material _CombineBloom;

	// Use this for initialization
	void Start ()
	{
		_DownsampleMax4x = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/DownsampleMax4x"));
		_DownsampleMax4x.hideFlags = HideFlags.DontSave;

		_GetBright = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/GetBright"));
		_GetBright.hideFlags = HideFlags.DontSave;

		_GaussBlur = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/GaussBlur7"));
		_GaussBlur.hideFlags = HideFlags.DontSave;

		_CombineBloom = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/CombineBloom"));
		_CombineBloom.hideFlags = HideFlags.DontSave;

		if (!IsSupported())
		{
			enabled = false;
		}
	}

	public bool IsSupported()
	{
		return SystemInfo.supportsImageEffects && SystemInfo.supportsRenderTextures && _DownsampleMax4x.shader.isSupported &&
			_GetBright.shader.isSupported && _GaussBlur.shader.isSupported && _CombineBloom.shader.isSupported;
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		int w4x = src.width / 4;
		int h4x = src.height / 4;

		RenderTexture tmpA = RenderTexture.GetTemporary(w4x, h4x, 0);
		RenderTexture tmpB = RenderTexture.GetTemporary(w4x, w4x, 0);

		src.filterMode = FilterMode.Bilinear;
		src.wrapMode = TextureWrapMode.Clamp;
		_DownsampleMax4x.SetVector("_Offset", new Vector2(1.0f / (w4x * 4.0f), 1.0f / (h4x * 4.0f)));
		tmpA.DiscardContents();
		Graphics.Blit(src, tmpA, _DownsampleMax4x);

		tmpA.filterMode = FilterMode.Point;
		tmpA.wrapMode = TextureWrapMode.Clamp;
		_GetBright.SetColor("_BrightThreshhold", _BrightThreshhold);
		tmpB.DiscardContents();
		Graphics.Blit(tmpA, tmpB, _GetBright);

		tmpB.filterMode = FilterMode.Bilinear;
		tmpB.wrapMode = TextureWrapMode.Clamp;
		_GaussBlur.SetVector("_Offset", new Vector2(0.0f, _SpreadRange / 7.0f));
		tmpA.DiscardContents();
		Graphics.Blit(tmpB, tmpA, _GaussBlur);

		tmpA.filterMode = FilterMode.Bilinear;
		tmpA.wrapMode = TextureWrapMode.Clamp;
		_GaussBlur.SetVector("_Offset", new Vector2(_SpreadRange / 7.0f / src.width * src.height, 0.0f));
		tmpB.DiscardContents();
		Graphics.Blit(tmpA, tmpB, _GaussBlur);

		src.filterMode = FilterMode.Point;
		src.wrapMode = TextureWrapMode.Clamp;
		tmpB.filterMode = FilterMode.Bilinear;
		tmpB.wrapMode = TextureWrapMode.Clamp;
		_CombineBloom.SetTexture("_Bloom", tmpB);
		_CombineBloom.SetFloat("_BloomIntensity", _BloomIntensity);
		Graphics.Blit(src, dest, _CombineBloom);

		RenderTexture.ReleaseTemporary(tmpA);
		RenderTexture.ReleaseTemporary(tmpB);
	}
}
