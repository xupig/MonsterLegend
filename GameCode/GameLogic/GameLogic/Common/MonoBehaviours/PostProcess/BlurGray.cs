﻿
using UnityEngine;
using System.Collections;
using ShaderUtils;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class BlurGray : MonoBehaviour
{
	public float _SpreadRange = 0.045f;
	public float _BlurScale = 1;// [0, 1]
	public bool _GrayEnable = true;
	public float _GrayScale = 0.5f;// [0, 1]

	private Material _Downsample4x;
	private Material _GaussBlur;
	private Material _CombineBlur;
	private Material _Gray;

	// Use this for initialization
	void Start ()
	{
		_Downsample4x = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/Downsample4x"));
		_Downsample4x.hideFlags = HideFlags.DontSave;

		_GaussBlur = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/GaussBlur5"));
		_GaussBlur.hideFlags = HideFlags.DontSave;

		_CombineBlur = new Material(ShaderRuntime.loader.Find("-MOGO2/PostProcess/CombineBlur"));
		_CombineBlur.hideFlags = HideFlags.DontSave;

		_Gray = new Material(ShaderRuntime.loader.Find("MOGO2/PostProcess/Gray"));
		_Gray.hideFlags = HideFlags.DontSave;

		if (!IsSupported())
		{
			enabled = false;
		}
	}
	
	//// Update is called once per frame
	//void Update ()
	//{
	//}

	//void OnEnable()
	//{
	//}

	public bool IsSupported()
	{
		return SystemInfo.supportsImageEffects && SystemInfo.supportsRenderTextures &&
			_Downsample4x.shader.isSupported && _GaussBlur.shader.isSupported &&
			_CombineBlur.shader.isSupported && _Gray.shader.isSupported;
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		int w4x = src.width / 4;
		int h4x = src.height / 4;

		RenderTexture tmpA = RenderTexture.GetTemporary(w4x, h4x, 0);
		RenderTexture tmpB = RenderTexture.GetTemporary(w4x, w4x, 0);

		src.filterMode = FilterMode.Bilinear;
		src.wrapMode = TextureWrapMode.Clamp;
		_Downsample4x.SetVector("_Offset", new Vector2(1.0f / (w4x * 4.0f), 1.0f / (h4x * 4.0f)));
		tmpA.DiscardContents();
		Graphics.Blit(src, tmpA, _Downsample4x);

		tmpA.filterMode = FilterMode.Bilinear;
		tmpA.wrapMode = TextureWrapMode.Clamp;
		_GaussBlur.SetVector("_Offset", new Vector2(_SpreadRange / 5.0f / src.width * src.height, 0.0f));
		tmpB.DiscardContents();
		Graphics.Blit(tmpA, tmpB, _GaussBlur);

		tmpB.filterMode = FilterMode.Bilinear;
		tmpB.wrapMode = TextureWrapMode.Clamp;
		_GaussBlur.SetVector("_Offset", new Vector2(0.0f, _SpreadRange / 5.0f));
		tmpA.DiscardContents();
		Graphics.Blit(tmpB, tmpA, _GaussBlur);

		if (_GrayEnable)
		{
			RenderTexture tmpC = RenderTexture.GetTemporary(src.width, src.height, 0);

			src.filterMode = FilterMode.Point;
			src.wrapMode = TextureWrapMode.Clamp;
			tmpA.filterMode = FilterMode.Bilinear;
			tmpA.wrapMode = TextureWrapMode.Clamp;
			_CombineBlur.SetTexture("_Blur", tmpA);
			_CombineBlur.SetFloat("_BlurScale", _BlurScale);
			tmpC.DiscardContents();
			Graphics.Blit(src, tmpC, _CombineBlur);

			tmpC.filterMode = FilterMode.Point;
			tmpC.wrapMode = TextureWrapMode.Clamp;
			_Gray.SetFloat("_GrayScale", _GrayScale);
			Graphics.Blit(tmpC, dest, _Gray);

			RenderTexture.ReleaseTemporary(tmpC);
		}
		else
		{
			src.filterMode = FilterMode.Point;
			src.wrapMode = TextureWrapMode.Clamp;
			tmpA.filterMode = FilterMode.Bilinear;
			tmpA.wrapMode = TextureWrapMode.Clamp;
			_CombineBlur.SetTexture("_Blur", tmpA);
			_CombineBlur.SetFloat("_BlurScale", _BlurScale);
			Graphics.Blit(src, dest, _CombineBlur);
		}

		RenderTexture.ReleaseTemporary(tmpA);
		RenderTexture.ReleaseTemporary(tmpB);
	}
}
