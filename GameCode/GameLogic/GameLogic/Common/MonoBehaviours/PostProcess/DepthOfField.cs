﻿using ACTSystem;
using ShaderUtils;
using UnityEngine;

public class DepthOfField : MonoBehaviour
{
	public Transform _FocusObj = null;
	public float _FocusOffset = 2.0f;
	public float _VaryDistance = 20.0f;
	public float _BlurRange = 0.005f;

	private Shader _DownsampleShader;
	private Material _DownsampleMtrl;
	private Shader _BlurShader;
	private Material _BlurMtrl;
	private Shader _DofShader;
	private Material _DofMtrl;
	private Shader _DepthShader;
	private GameObject _DepthCam;
	private RenderTextureFormat _DepthFormat;

	void Start()
	{
        _DownsampleShader = ShaderRuntime.loader.Find("MOGO2/PostProcess/Downsample");
		_DownsampleMtrl = new Material(_DownsampleShader);
		_DownsampleMtrl.hideFlags = HideFlags.DontSave;

        _BlurShader = ShaderRuntime.loader.Find("MOGO2/PostProcess/Blur");
		_BlurMtrl = new Material(_BlurShader);
		_BlurMtrl.hideFlags = HideFlags.DontSave;

        _DofShader = ShaderRuntime.loader.Find("MOGO2/PostProcess/DepthOfField");
		_DofMtrl = new Material(_DofShader);
		_DofMtrl.hideFlags = HideFlags.DontSave;

        _DepthShader = ShaderRuntime.loader.Find("MOGO2/PostProcess/RenderDepth");
		_DepthCam = new GameObject("_DepthCam", typeof(Camera));
		_DepthCam.SetActive(false);
		_DepthFormat = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.R8) ? RenderTextureFormat.R8 : RenderTextureFormat.Default;
	}

    public bool IsSupported()
	{
		return (SystemInfo.supportsImageEffects && SystemInfo.supportsRenderTextures &&
			_DownsampleShader.isSupported && _BlurShader.isSupported && _DofShader.isSupported && _DepthShader.isSupported);
	}

    void OnEnable()
    {
        if (!IsSupported())
        {
            enabled = false;
        }
    }

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		Vector3 focus;
		if (_FocusObj == null)
		{
			focus = transform.position;
		}
		else
		{
			focus = _FocusObj.position;
		}
		Vector3 viewPos = GetComponent<Camera>().worldToCameraMatrix.MultiplyPoint(focus);
		Shader.SetGlobalVector("_DofParam", new Vector2(-viewPos.z + _FocusOffset, -1.0f / _VaryDistance));

		_DepthCam.GetComponent<Camera>().CopyFrom(GetComponent<Camera>());
		_DepthCam.GetComponent<Camera>().backgroundColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		_DepthCam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;

        int layer = UnityEngine.LayerMask.NameToLayer("NoDofDepth");
		_DepthCam.GetComponent<Camera>().cullingMask = _DepthCam.GetComponent<Camera>().cullingMask & (~(1 << layer));

		RenderTexture depth = RenderTexture.GetTemporary(src.width, src.height, 16, _DepthFormat);
		depth.DiscardContents();
		_DepthCam.GetComponent<Camera>().targetTexture = depth;
		_DepthCam.GetComponent<Camera>().RenderWithShader(_DepthShader, "");
		_DepthCam.GetComponent<Camera>().targetTexture = null;

		int lowW = src.width / 3;
		int lowH = src.height / 3;
		RenderTexture lowA = RenderTexture.GetTemporary(lowW, lowH, 0);
		RenderTexture lowB = RenderTexture.GetTemporary(lowW, lowH, 0);

		src.filterMode = FilterMode.Bilinear;
		src.wrapMode = TextureWrapMode.Clamp;
		lowA.filterMode = FilterMode.Bilinear;
		lowA.wrapMode = TextureWrapMode.Clamp;
		lowB.filterMode = FilterMode.Bilinear;
		lowB.wrapMode = TextureWrapMode.Clamp;

		lowA.DiscardContents();
		_DownsampleMtrl.SetVector("_Offset", new Vector2(0.75f / (float)src.width, 0.75f / (float)src.height));
		Graphics.Blit(src, lowA, _DownsampleMtrl);

		lowB.DiscardContents();
		_BlurMtrl.SetVector("_Offset", new Vector2(_BlurRange, 0.0f));
		Graphics.Blit(lowA, lowB, _BlurMtrl);

		lowA.DiscardContents();
		_BlurMtrl.SetVector("_Offset", new Vector2(0.0f, _BlurRange / (float)lowH * (float)lowW));
		Graphics.Blit(lowB, lowA, _BlurMtrl);

		src.filterMode = FilterMode.Point;
		depth.filterMode = FilterMode.Point;
		depth.wrapMode = TextureWrapMode.Clamp;

		_DofMtrl.SetTexture("_BlurTex", lowA);
		_DofMtrl.SetTexture("_DofFactor", depth);
		Graphics.Blit(src, dest, _DofMtrl);

		RenderTexture.ReleaseTemporary(lowA);
		RenderTexture.ReleaseTemporary(lowB);
		RenderTexture.ReleaseTemporary(depth);
	}
}
