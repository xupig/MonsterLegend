﻿using UnityEngine;
using System.Collections;
using ShaderUtils;

public class ScreenFade : MonoBehaviour
{
	public Color _Color = new Color(0.0f, 0.0f, 0.0f, 0.0f);

	private Shader _Shader = null;
	private Material _Material = null;
	private GameObject _Quad = null;

	// Use this for initialization
	void Start ()
	{
		_Shader = ShaderRuntime.loader.Find("MOGO2/PostProcess/ScreenFade");
		_Material = new Material(_Shader);
		_Material.hideFlags = HideFlags.DontSave;
		_Quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
		_Quad.GetComponent<Renderer>().sharedMaterial = _Material;
		_Quad.hideFlags = HideFlags.HideAndDontSave;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_Quad.transform.position = GetComponent<Camera>().cameraToWorldMatrix * new Vector4(0, 0, -20, 1);
		_Material.SetColor("_Color", _Color);
	}

	void OnDestroy()
	{
		GameObject.DestroyImmediate(_Quad);
	}
}
