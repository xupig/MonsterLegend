﻿using GameMain.GlobalManager;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 10:38:22 
 * function: 
 * *******************************************************/
public class CameraLookAt : CameraAction
{
    public Transform target;
    public float rotationDuration = 0;
    public float rotationMinSpeed = 0;
    public Vector3 targetPosition = -1 * Vector3.one;
    public float positionDuration = 0;

    protected override void Run()
    {
        CameraManager.GetInstance().ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, targetPosition, positionDuration);
    }
}
