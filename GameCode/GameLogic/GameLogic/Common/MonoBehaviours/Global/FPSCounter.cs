﻿using UnityEngine;
using System.Collections;
using GameMain.GlobalManager;
//using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using GameMain.Entities;

public class FPSCounter : MonoBehaviour {

    
    float _updateInterval = 0.5f;

    float _lastInterval = 0; // Last interval end time
    float _frames = 0; // Frames over current interval
    float _fps = 0; // Current FPS

    void Awake()
    {
        useGUILayout = false;
    }

	// Use this for initialization
	void Start () {
        _lastInterval = Time.realtimeSinceStartup;
        _frames = 0;
	}

    float _panelPosX = 0;
    float _panelPosY = 0;
    void OnGUI()
    {
        GUI.Box(new Rect(_panelPosX, _panelPosY, 700, 100), "");
        //GUILayout.BeginVertical(GUILayout.Width(_panelWidth + 5), GUILayout.Height(_panelHeight + 5));
        var style = new GUIStyle();
        style.fontSize = 40;
        style.normal.textColor = Color.white;
        GUI.Label(new Rect(_panelPosX, _panelPosY, 100, 50), "FPS: " + _fps.ToString("f2"), style);
        ShowCommandLine();
        //GUI.Box(new Rect(r.position.x, r.position.y, _panelWidth, _panelHeight), "");
        //GUI.Label(new Rect(100, 100, 100, 30), "FPS: " + m_fps.ToString("f2"));
        //GUILayout.EndVertical();
    }

    string inputString = "";
    void ShowCommandLine()
    {
        inputString = GUI.TextField(new Rect(_panelPosX + 210, _panelPosY, 200, 60), inputString);
        if (GUI.Button(new Rect(_panelPosX + 420, _panelPosY, 200, 50), "run"))
        {
            //ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, inputString, PlayerAvatar.Player.dbid);
            Debug.LogError("ShowCommandLine::" + inputString);
        }
    }

	// Update is called once per frame
	void Update () {
        ++_frames;
        var timeNow = Time.realtimeSinceStartup;
        if (timeNow > _lastInterval + _updateInterval)
        {
            _fps = _frames / (timeNow - _lastInterval);
            _frames = 0;
            _lastInterval = timeNow;
        }
        PrintFPS();
	}

    static public bool printFPS = false;
    float _printInterval = 1f;
    float _lastPrint = 0; // Last print end time
    void PrintFPS()
    {
        if (printFPS)
        {
            var timeNow = Time.realtimeSinceStartup;
            if (timeNow > _lastPrint + _printInterval)
            {
                Debug.LogWarning("<FPS:" + _fps + ">");
                _lastPrint = timeNow;
            }
        }
    }
}
