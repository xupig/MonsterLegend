﻿using Common.Data;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/19 14:35:01 
 * function: 
 * *******************************************************/
public class MogoMainCamera : MonoBehaviour
{
    //跟踪目标
    public Transform target;
    //距离目标
    public float distance = 5.5f;
    //角度（0为水平,90俯视）
    public float rotationX = 40.0f;
    public float rotationY = 135.0f;
    public float rotationZ = 0f;
    public float rotationMinSpeed = 0f;

    private CameraAnimData _shakeData;
    private float _shakeStartTime;
    private float _shakeDuration;
    public bool isShaking = false;

    private float _xSpeed;
    private float _xDelta;//跟轴同向
    private float _ySpeed;
    private float _yDelta;
    private float _zSpeed;
    private float _zDelta;
    private float dTime;
    private int _currPlayShakeId = 0;
    private List<CameraInfo> test = new List<CameraInfo>();
    private BaseCameraMotion _cameraMotion;

    public CameraAccordingMode accordingNode { get; set; }

    private HashSet<string> _withoutTestObjectTags = new HashSet<string>();
    void Start()
    {
        //在此添加需要排除检测的物体,以后可能用layer处理更好
        _withoutTestObjectTags.Add("MainCamera");
        _withoutTestObjectTags.Add("terrain");
        _withoutTestObjectTags.Add("Player");

        AddTestScripts();
        if (Application.isEditor)InspectorSetting.InspectingTool.Inspect(new GameMain.Inspectors.InspectingCamera(this), gameObject);
    }

    void AddTestScripts()
    {
        gameObject.AddComponent<RotateCamera>();
        gameObject.AddComponent<MoveCamera>();
        gameObject.AddComponent<CameraLookAt>();
    }

    void OnDestroy()
    {
        _cameraMotion = null;
    }

    void LateUpdate()
    {
        if (!target)
        {
            return;
        }

        if (_cameraMotion != null && accordingNode == CameraAccordingMode.AccordingMotion)
        {
            _cameraMotion.OnUpdate();
        }
        if (accordingNode != CameraAccordingMode.AccordingMotion && !isShaking)
        {
            UpdateCamera();
        }
        if (isShaking)
        {
            DoShaking();
        }
        if (_skyTransform != null)
        {
            _skyTransform.position = transform.position;
        }

    }

    void UpdateCamera()
    {
        gameObject.transform.position = new Vector3(0, 0, 0);
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }

    void DoShaking()
    {
        dTime = Time.realtimeSinceStartup - _shakeStartTime;
        if (dTime > _shakeDuration)
        {
            isShaking = false;
            _xDelta = 0;
            _yDelta = 0;
            _zDelta = 0;

            for (int i = 0; i < test.Count; i++)
            {
                if (test[i].key == _currPlayShakeId)
                {
                    test.RemoveAt(i);
                }
            }
            _currPlayShakeId = 0;
            if (test != null && test.Count > 0)
            {
                bool isCanShake = IsCanShake(test[test.Count - 1].key);
                if (!isCanShake) return;
                CameraManager.GetInstance().ShakeCamera(test[test.Count - 1].key, test[test.Count - 1].len, test[test.Count - 1].shakeCountOdds);
            }
            return;
        }
        _shakeData.ySwing = _shakeData.ySwing * _shakeData.attenuateOddsY;
        _shakeData.xSwing = _shakeData.xSwing * _shakeData.attenuateOddsX;
        _shakeData.zSwing = _shakeData.zSwing * _shakeData.attenuateOddsZ;

        if (_shakeData.ySwing != 0 && _shakeData.ySwing <= 0.001 || _shakeData.xSwing != 0 && _shakeData.xSwing <= 0.001 || _shakeData.zSwing != 0 && _shakeData.zSwing <= 0.001)
        {
            ResetShakeInfo();
            return;
        }
        _shakeData.UpdateSpeed(ref _xSpeed, ref _ySpeed, ref _zSpeed, dTime);

        _xDelta = _xSpeed * Time.deltaTime + _xDelta;
        _yDelta = _ySpeed * Time.deltaTime + _yDelta;
        _zDelta = _zSpeed * Time.deltaTime + _zDelta;

        while (Mathf.Abs(_xDelta) > Mathf.Abs(_shakeData.xSwing))
        {
            if (_xDelta > 0) _xDelta = _shakeData.xSwing * 2 - _xDelta;
            else _xDelta = -_shakeData.xSwing * 2 - _xDelta;
            _xSpeed = -_xSpeed;
        }
        while (Mathf.Abs(_yDelta) > _shakeData.ySwing)
        {
            if (_yDelta > 0) _yDelta = _shakeData.ySwing * 2 - _yDelta;
            else _yDelta = -_shakeData.ySwing * 2 - _yDelta;
            _ySpeed = -_ySpeed;
        }
        while (Mathf.Abs(_zDelta) > _shakeData.zSwing)
        {
            if (_zDelta > 0) _zDelta = _shakeData.zSwing * 2 - _zDelta;
            else _zDelta = -_shakeData.zSwing * 2 - _zDelta;
            _zSpeed = -_zSpeed;
        }

        transform.Translate(new Vector3(_xDelta, _yDelta, _zDelta), Space.Self);
    }

    Transform _skyTransform = null;
    public void ResetSky(Transform transform)
    {
        _skyTransform = transform;
        if (_skyTransform == null) return;
        _skyTransform.gameObject.GetComponent<Renderer>().material.renderQueue = 900;
        Camera myCamera = gameObject.GetComponent<Camera>();
        myCamera.clearFlags = CameraClearFlags.Depth;
        GameObject go = GameObject.Find("Fog_FarPlane");
        if (go)
        {
            FarFogPlane ffp = go.GetComponent<FarFogPlane>();
            ffp.ResetFog();
            go.SetActive(false);
        }
    }

    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    public void SetCurrShakeInfo(int priority, int id, float len, float odds = 1.0f)
    {
        //test = new List<CameraInfo>();
        var cameraInfo = new CameraInfo();
        cameraInfo.priority = priority;
        cameraInfo.key = id;
        cameraInfo.len = len;
        cameraInfo.endTime = Time.realtimeSinceStartup + len;
        cameraInfo.shakeCountOdds = odds;
        test.Add(cameraInfo);
        test.Sort((Comparison<CameraInfo>)delegate(CameraInfo a, CameraInfo b)
        {
            return a.priority > b.priority ? 1 : a.priority == b.priority ? 0 : -1;
        });
    }

    public bool IsCanShake(int id)
    {
        for (int i = 0; i < test.Count; i++)
        {
            if (test[i].key == id)
            {
                if (test[i].endTime <= Time.realtimeSinceStartup)
                {
                    test.RemoveAt(i);
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void ResetCurrShakeInfo()
    {
        if (test != null)
        {
            test.Clear();
        }
    }

    public void ShakeCamera(int id, float _xSwing, float _ySwing, float _zSwing, int _xRate, int _yRate, int _zRate, float _length, float _attenuateOddsX, float _attenuateOddsY, float _attenuateOddsZ, int _xDir = 1, int _yDir = 1, int _zDir = 1, float _xRateA = 0, float _yRateA = 0, float _zRateA = 0, float durative = 1.0f)
    {
        if (isShaking && _currPlayShakeId > 0)
        {
            for (int i = 0; i < test.Count; i++)
            {
                if (test[i].key == _currPlayShakeId)
                {
                    test.RemoveAt(i);
                }
            }
            _xDelta = 0;
            _yDelta = 0;
            _zDelta = 0;
            float shakeCountOdds = dTime / _shakeDuration;
            SetCurrShakeInfo(XMLManager.camera_anim[id].__priority, _currPlayShakeId, dTime, shakeCountOdds);
            isShaking = false;
        }
        if (durative >= 1.0f)
        {
            durative = 1.0f;
        }
        CameraAnimData data = new CameraAnimData()
        {
            xRate = (_xRate / 100) * durative,
            yRate = (_yRate / 100) * durative,
            zRate = (_zRate / 100) * durative,
            xSwing = _xSwing,
            ySwing = _ySwing,
            zSwing = _zSwing,
            xDir = _xDir,
            yDir = _yDir,
            zDir = _zDir,
            xRateA = _xRateA,
            yRateA = _yRateA,
            zRateA = _zRateA,
            attenuateOddsX = _attenuateOddsX,
            attenuateOddsY = _attenuateOddsY,
            attenuateOddsZ = _attenuateOddsZ
        };
        if (data == null) return;
        _shakeData = data;
        _shakeStartTime = Time.realtimeSinceStartup;
        _shakeDuration = _length;
        _xDelta = 0;
        _yDelta = 0;
        _zDelta = 0;
        _xSpeed = data.xRate * 4 * data.xSwing * data.xDir;
        _ySpeed = data.yRate * 4 * data.ySwing * data.yDir;
        _zSpeed = data.zRate * 4 * data.zSwing * data.zDir;
        _currPlayShakeId = id;
        isShaking = true;
    }

    public int CurrPlayShakeId
    {
        get
        {
            return _currPlayShakeId;
        }
    }

    public void ResetShakeInfo()
    {
        isShaking = false;
        _xDelta = 0;
        _yDelta = 0;
        _zDelta = 0;
    }

    public void SetCameraMotion(BaseCameraMotion motion)
    {
        _cameraMotion = motion;
    }

    public void StopCurrentCameraMotion()
    {
        if (_cameraMotion != null)
        {
            _cameraMotion.Stop();
        }
    }

    public bool IsScaled()
    {
        return _cameraMotion != null && _cameraMotion is RotationTargetMotion && (_cameraMotion as RotationTargetMotion).IsScaled();
    }

    public void ChangeToRotationTargetMotion(Transform target, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        RotationTargetMotion motion = _cameraMotion as RotationTargetMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToRotationPositionMotion(Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, Vector3 position, float positionDuration, bool ignoreTimeScale)
    {
        RotationPositionMotion motion = _cameraMotion as RotationPositionMotion;
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.focusPoint.Set(0, 0, 0);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        Vector3 targetPosition = SetTargetPosition(position);
        motion.data.targetPosition = targetPosition;
        motion.data.positionDuration = positionDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.Start();
    }

    public void ChangeToTargetPositionMotion(Transform target, float rotationDuration, float rotationMinSpeed, Vector3 position, float positionDuration, bool ignoreTimeScale)
    {
        TargetPositionMotion motion = _cameraMotion as TargetPositionMotion;
        SetTarget(target);
        this.rotationMinSpeed = rotationMinSpeed;
        motion.data.target = this.target;
        motion.data.focusPoint.Set(0, 0, 0);
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationMinSpeed = rotationMinSpeed;
        Vector3 targetPosition = SetTargetPosition(position);
        motion.data.targetPosition = targetPosition;
        motion.data.positionDuration = positionDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.Start();
    }

    public void ChangeToOneVsOneMotion(Transform self, Transform opponent, Vector3 startRotation)
    {
        OneVsOneMotion motion = _cameraMotion as OneVsOneMotion;
        SetTarget(self);
        if (motion.data.focusPoint == Vector3.zero)
        {
            motion.data.focusPoint = self.position;
        }
        motion.self = self;
        motion.opponent = opponent;
        motion.startFocusPoint = motion.data.focusPoint;
        motion.startRotation = startRotation;
        motion.Start();
    }

    private void SetTarget(Transform target)
    {
        if (target != null)
        {
            this.target = target;
        }
    }

    private Vector3 SetTargetRotation(Vector3 localEulerAngles)
    {
        if (localEulerAngles == -1 * Vector3.one)
        {
            return new Vector3(rotationX, rotationY, rotationZ);
        }
        rotationX = localEulerAngles.x;
        rotationY = localEulerAngles.y;
        rotationZ = localEulerAngles.z;
        return localEulerAngles;
    }

    private Vector3 SetTargetPosition(Vector3 position)
    {
        if (position == -1 * Vector3.one)
        {
            return gameObject.transform.position;
        }
        return position;
    }

    private float SetTargetDistance(float distance)
    {
        if (distance == -1)
        {
            return this.distance;
        }
        this.distance = distance;
        return distance;
    }

    public CameraMotionType currentMotionType
    {
        get
        {
            if (_cameraMotion != null)
            {
                return _cameraMotion.GetCameraType();
            }
            return CameraMotionType.ROTATION_TARGET;
        }
    }

    public BaseCameraMotion currentCameraMotion
    {
        get
        {
            return _cameraMotion;
        }
    }

    public float GetTargetDistance()
    {
        if (target == null)
        {
            return 0;
        }
        return Vector3.Distance(target.transform.position, transform.position);
    }

    public void UpdateTouchesArgs(float touchDistance2Distance, float minDistance, float minRotationX, float touchDistanceToRotation)
    {
        if (_cameraMotion != null && _cameraMotion is RotationTargetMotion)
        {
            (_cameraMotion as RotationTargetMotion).UpdateArgs(touchDistance2Distance, minDistance, minRotationX, touchDistanceToRotation);
        }
    }

    public void UpdateTouchesRange(float top, float right, float bottom, float left)
    {
        if (_cameraMotion != null && _cameraMotion is RotationTargetMotion)
        {
            (_cameraMotion as RotationTargetMotion).UpdateRange(top, right, bottom, left);
        }
    }
}

public class CameraInfo
{
    public int priority;
    public int key;
    public float len;
    public float shakeCountOdds = 1.0f;
    public float endTime = 0;
}
