﻿using UnityEngine;
using System.Collections;
using GameMain.GlobalManager;
using Common.ServerConfig;
using GameMain.Entities;
using GameLoader.IO;
using MogoEngine.RPC;
using GameLoader.Utils;

public class PlatformWindows : MonoBehaviour 
{
    private int flag = 0;
    public static bool allowQuitting = false;

    void Awake()
    {

    }

    void Update()
    {
        if(flag != 0)
        {
            return;
        }
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
        {
            DockingService.UpdateMes();
        }
    }

    void OnApplicationQuit()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (!allowQuitting)
            {
                //Ari MessageBox.Show(true, string.Empty, "确定要离开我吗？", OnConfirm, OnCancel);
            }
            
            if (allowQuitting)
                StartCoroutine("DelayedQuit");

            if (!allowQuitting)
                Application.CancelQuit();
        }
        if (allowQuitting && (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer))
        {
            flag = 1;
            DestroyGameObject();
        }
    }

    void OnConfirm()
    {
        MogoFileSystem.Instance.Close();
        ServerProxy.Instance.Disconnect();
        ServerProxy.Instance.Release();
        LoggerHelper.Release();
        allowQuitting = true;
        //OnApplicationQuit();
        Application.Quit();
    }

    void OnCancel()
    {
        allowQuitting = false;
    }

    IEnumerator DelayedQuit()
    {
        yield return new WaitForSeconds(0.2F);
        allowQuitting = true;
        Application.Quit();
    }

    void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            WebLoginInfoManager.GetInstance().cuuOnlineFlag = "2";
        }
        else
        {
            WebLoginInfoManager.GetInstance().cuuOnlineFlag = "1";
        }
    }

    void DestroyGameObject()
    {
        DockingService.DestroyDockingSocket();
        GameObject.Destroy(gameObject);
    }
}
