﻿using UnityEngine;
using System.Collections;
using GameMain.GlobalManager;
using GameMain.Entities;
using ShaderUtils;

public class BlockTransparent : MonoBehaviour
{
	private static Shader s_BlockShader = null;

	public float _CheckTime = 0.33333f;

	private float _PassTime = 0.0f;
	private bool _IsVisible = false;
	private bool _IsBlock = false;
	private Shader _NormalShader = null;

	//// Use this for initialization
	//void Start ()
	//{
	//}
	
	// Update is called once per frame
	void Update()
	{
		if (_IsVisible)
		{
			_PassTime += Time.deltaTime;
			if (_PassTime > _CheckTime)
			{
				//_PassTime -= _CheckTime;
				_PassTime = 0;
				CheckBlock();
			}
		}
	}

	void OnEnable()
	{
		if (GetComponent<Collider>() == null || GetComponent<Renderer>() == null)
		{
			enabled = false;
			return;
		}

		Camera cam = CameraManager.GetInstance().Camera;
		if (cam == null)
		{
			_IsVisible = false;
		}
		else
		{
			Plane[] frustum = GeometryUtility.CalculateFrustumPlanes(cam);
			_IsVisible = GeometryUtility.TestPlanesAABB(frustum, GetComponent<Renderer>().bounds);
		}

	}

	void OnBecameVisible()
	{
		_IsVisible = true;
		//_PassTime = _CheckTime / 2.0f;
	}

	void OnBecameInvisible()
	{
		_IsVisible = false;
	}

	private void CheckBlock()
	{
		//if (collider != null)
		//{
			Vector3 camPos = CameraManager.GetInstance().CameraTransform.position;
			Vector3 playerPos = PlayerAvatar.Player.position;
			float playerH = PlayerAvatar.Player.GetHeight();
			Vector3 pos1 = new Vector3(playerPos.x, playerPos.y + 0.75f * playerH, playerPos.z);
			Vector3 pos2 = new Vector3(playerPos.x, playerPos.y + 0.25f * playerH, playerPos.z);
			Vector3 v1 = pos1 - camPos;
			Vector3 v2 = pos2 - camPos;
			Ray ray1 = new Ray(camPos, v1);
			Ray ray2 = new Ray(camPos, v2);

			bool isBlock = false;
			RaycastHit rh;
			if (GetComponent<Collider>().Raycast(ray1, out rh, v1.magnitude))
			{
				if (GetComponent<Collider>().Raycast(ray2, out rh, v2.magnitude))
				{
					isBlock = true;
				}
			}

			ChangeMaterial(isBlock);
		//}
	}

	private void ChangeMaterial(bool isBlock)
	{
		if (isBlock && !_IsBlock)
		{
			_IsBlock = true;

			//if (renderer != null)
			//{
				if (s_BlockShader == null)
				{
					s_BlockShader = ShaderRuntime.loader.Find("MOGO2/Environment/BlockObjectTransparent");
				}

				if (s_BlockShader != null)
				{
					_NormalShader = GetComponent<Renderer>().material.shader;
					GetComponent<Renderer>().material.shader = s_BlockShader;
				}
			//}
		}
		else if (!isBlock && _IsBlock)
		{
			_IsBlock = false;

			//if (renderer != null)
			//{
				if (s_BlockShader != null)
				{
					GetComponent<Renderer>().material.shader = _NormalShader;
				}
			//}
		}
	}
}
