﻿using GameMain.GlobalManager;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 10:37:59 
 * function: 
 * *******************************************************/
public class MoveCamera : CameraAction
{
    public Vector3 rotation = Vector3.zero;
    public float rotationDuration = 0;
    public float rotationAccelerateRate = 0;
    public Vector3 position = -1 * Vector3.one;
    public float positionDuration = 0;

    protected override void Run()
    {
        CameraManager.GetInstance().ChangeToRotationPositionMotion(rotation, rotationDuration, rotationAccelerateRate, position, positionDuration);
    }
}
