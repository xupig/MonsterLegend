﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Chat
{
    public class ChatDuelLinkWrapper : ChatLinkBaseWrapper
    {
        public int MatchId;
        public string Name;

        public ChatDuelLinkWrapper(int matchId, string name)
            : base(ChatLinkType.Duel)
        {
            this.MatchId = matchId;
            this.Name = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            
        }

        public override string ToDataString()
        {
            return string.Empty;
        }
    }
}
