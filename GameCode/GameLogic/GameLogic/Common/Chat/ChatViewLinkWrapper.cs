﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Data;

namespace Common.Chat
{
    public class ChatViewLinkWrapper : ChatLinkBaseWrapper
    {
        public string desc;
        public int viewId;

        private const string VIEW_LINK_TEMPLATE = "[{0};{1}]";

        public static ChatViewLinkWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatViewLinkWrapper(dataStringSplits);
        }

        public ChatViewLinkWrapper(string[] dataStringSplits)
            : base(ChatLinkType.View, dataStringSplits)
        {

        }

        public ChatViewLinkWrapper(string desc, int viewId)
            : base(ChatLinkType.View)
        {
            this.desc = desc;
            this.viewId = viewId;
            LinkDesc = desc;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            this.viewId = int.Parse(dataStringSplits[1]);
        }

        public override string ToDataString()
        {
            return string.Format(VIEW_LINK_TEMPLATE, ((int)LinkType).ToString(), viewId.ToString());
        }
    }
}
