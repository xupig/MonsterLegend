﻿using System;
using System.Collections.Generic;
using Game.UI;
using Game.UI.TextEngine;
using UnityEngine;
using UnityEngine.UI;
using GameLoader.Utils;
using Common.Data;

namespace Common.Chat
{
    public class ChatLinkElement : TextElement
    {
        protected string _srcContent = string.Empty;

        public ChatLinkElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            _srcContent = content;
        }

        public override GameObject GetFragment(GameObject parent, float availableWidth)
        {
            return base.GetFragment(parent, availableWidth);
        }

        protected string GetLinkDesc(ChatLinkBaseWrapper linkWrapper)
        {
            if (linkWrapper != null)
            {
                string desc = linkWrapper.LinkDesc;
                return desc;
            }
            else
            {
                return string.Empty;
            }
        }

        private static float SHADOW_DISTANCE_X = -2 * Mathf.Cos(120.0f * Mathf.PI / 180.0f);
        private static float SHADOW_DISTANCE_Y = -2 * Mathf.Sin(120.0f * Mathf.PI / 180.0f);
        public static void AddTextShadowEffect(GameObject textGo)
        {
            Shadow shadow = textGo.AddComponent<Shadow>();
            shadow.effectDistance = new Vector2(SHADOW_DISTANCE_X, SHADOW_DISTANCE_Y);
            shadow.effectColor = Color.black;
        }
    }
}
