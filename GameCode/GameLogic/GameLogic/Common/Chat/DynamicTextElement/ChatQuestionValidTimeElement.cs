﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.TextEngine;
using System.Text.RegularExpressions;
using UnityEngine;
using Game.UI;
using Common.Data;

namespace Common.Chat
{
    public class ChatQuestionValidTimeElement : GraphicElement
    {
        private static Regex DESC_PATTERN = new Regex(@"(?<=\[).*?(?=\,)");
        private static Regex END_TIME_STAMP_PATTERN = new Regex(@"(?<=\,)\d{1,20}(?=\])");

        private static readonly Vector2 QUESTION_END_TIPS_SIZE = TextBlock.GetTextContentSize(ChatConst.CHAT_QUESTION_END_TIPS);
        private TextWrapper _text;
        private string _srcContent = string.Empty;

        public ChatQuestionValidTimeElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {

        }

        protected override void AddFragmentComponent(GameObject go)
        {
            TextWrapper text = ChatTextBlock.CreateChatLinkTextComponent(go);
            text.text = GetDesc();
            text.SetDimensionsDirty();
            _text = text;
            ChatLinkElement.AddTextShadowEffect(go);
            ChatQuestionTimeComponent compoent = go.AddComponent<ChatQuestionTimeComponent>();
            compoent.Init(GetEndTimeStamp());
        }

        private string GetDesc()
        {
            return DESC_PATTERN.Match(this.Content).Value;
        }

        private uint GetEndTimeStamp()
        {
            uint endTimeStamp = 0;
            uint.TryParse(END_TIME_STAMP_PATTERN.Match(this.Content).Value, out endTimeStamp);
            return endTimeStamp;
        }

        protected override Vector2 GetFragmentSize()
        {
            Vector2 curSize = TextBlock.GetTextContentSize(GetDesc());
            if (curSize.x < QUESTION_END_TIPS_SIZE.x)
            {
                curSize.x = QUESTION_END_TIPS_SIZE.x;
            }
            if (curSize.y < QUESTION_END_TIPS_SIZE.y)
            {
                curSize.y = QUESTION_END_TIPS_SIZE.y;
            }
            return curSize;
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
