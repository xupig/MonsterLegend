﻿using Common.Chat;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Chat
{
    public class ChatDuelElement : ChatLinkElement
    {
        private string _name;
        private int _matchId;

        public ChatDuelElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            char[] splitChar = new char[] { ',', ']' , '】'};
            string[] stringList = _srcContent.Split(splitChar);
            _name = stringList[1];
            int.TryParse(stringList[stringList.Length - 2], out _matchId);
            this.Content = GetContent(content);
        }

        private string GetContent(string content)
        {
            
            return content.Substring(1, content.IndexOf("】"));
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            DuelManager.Instance.RequestStakeInfo(_name, _matchId);
        }
    }
}
