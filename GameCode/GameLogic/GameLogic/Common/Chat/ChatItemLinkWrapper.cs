﻿using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;

namespace Common.Chat
{
    public class ChatItemLinkWrapper : ChatLinkBaseWrapper
    {
        public int itemLinkType { get; protected set; }
        public string name;
        public int bag;
        public int grid;
        public int quality;
        public int itemId;
        public ulong getTime;
        public int achievementId;

        private const string ITEM_NORMAL_TEMPLATE = "[{0};{1};{2};{3};{4}]";
        private const string ITEM_WING_TEMPLATE = "[{0};{1};{2}]";
        private const string ITEM_TITLE_TEMPLATE = "[{0};{1};{2};{3}]";
        private const string ITEM_ACHIEVEMENT_TEMPLATE = "[{0};{1};{2}]";
        private const string ITEM_ITEM_TEMPLATE = "[{0};{1};{2}]";

        public static ChatItemLinkWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatItemLinkWrapper(dataStringSplits);
        }

        public ChatItemLinkWrapper(string[] dataStringSplits)
            : base(ChatLinkType.Item, dataStringSplits)
        {

        }

        public ChatItemLinkWrapper(string name, int bag, int grid, int quality)
            : base(ChatLinkType.Item)
        {
            this.itemLinkType = (int)ItemLinkType.Normal;
            this.name = name;
            this.bag = bag;
            this.grid = grid;
            this.quality = quality;
            LinkDesc = name;
        }

        public ChatItemLinkWrapper(string name, int wingId)
            : base(ChatLinkType.Item)
        {
            this.itemLinkType = (int)ItemLinkType.Wing;
            this.name = name;
            this.itemId = wingId;
            LinkDesc = name;
        }

        public ChatItemLinkWrapper(string name, int itemId, ulong getTime)
            : base(ChatLinkType.Item)
        {
            this.itemLinkType = (int)ItemLinkType.Title;
            this.name = name;
            this.itemId = itemId;
            this.getTime = getTime;
            LinkDesc = name;
        }

        public ChatItemLinkWrapper(int achievementId)
            : base(ChatLinkType.Item)
        {
            this.itemLinkType = (int)ItemLinkType.Achievement;
            this.achievementId = achievementId;
            this.name = achievement_helper.GetAchievementName(achievementId);
            LinkDesc = name;
        }

        public ChatItemLinkWrapper(ItemLinkType itemLinkType, string name, int itemId)
            : base(ChatLinkType.Item)
        {
            this.itemLinkType = (int)itemLinkType;
            this.itemId = itemId;
            LinkDesc = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            itemLinkType = int.Parse(dataStringSplits[1]);
            switch ((ItemLinkType)itemLinkType)
            {
                case ItemLinkType.Normal:
                    bag = int.Parse(dataStringSplits[2]);
                    grid = int.Parse(dataStringSplits[3]);
                    quality = int.Parse(dataStringSplits[4]);
                    break;
                case ItemLinkType.Wing:
                    itemId = int.Parse(dataStringSplits[2]);
                    break;
                case ItemLinkType.Title:
                    itemId = int.Parse(dataStringSplits[2]);
                    getTime = ulong.Parse(dataStringSplits[3]);
                    break;
                case ItemLinkType.Achievement:
                    achievementId = int.Parse(dataStringSplits[2]);
                    break;
                case ItemLinkType.Item:
                    itemId = int.Parse(dataStringSplits[2]);
                    break;
                default:
                    break;
            }
        }

        public override string ToDataString()
        {
            switch ((ItemLinkType)this.itemLinkType)
            {
                case ItemLinkType.Normal:
                    return string.Format(ITEM_NORMAL_TEMPLATE, ((int)LinkType).ToString(), itemLinkType.ToString(), bag.ToString(), grid.ToString(), quality.ToString());
                case ItemLinkType.Wing:
                    return string.Format(ITEM_WING_TEMPLATE, ((int)LinkType).ToString(), itemLinkType.ToString(), itemId.ToString());
                case ItemLinkType.Title:
                    return string.Format(ITEM_TITLE_TEMPLATE, ((int)LinkType).ToString(), itemLinkType.ToString(), itemId.ToString(), getTime.ToString());
                case ItemLinkType.Achievement:
                    return string.Format(ITEM_ACHIEVEMENT_TEMPLATE, ((int)LinkType).ToString(), itemLinkType.ToString(), achievementId.ToString());
                case ItemLinkType.Item:
                    return string.Format(ITEM_ITEM_TEMPLATE, ((int)LinkType).ToString(), itemLinkType.ToString(), itemId.ToString());
                default:
                    return string.Empty;
            }
        }

        public enum ItemLinkType
        {
            Normal = 1,
            Wing = 2,
            Title = 3,
            Achievement = 4,
            Item = 5,
        }
    }
}
