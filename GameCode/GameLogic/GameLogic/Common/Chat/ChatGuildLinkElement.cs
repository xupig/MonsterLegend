﻿using System;
using System.Collections.Generic;
using Game.UI.TextEngine;
using System.Text.RegularExpressions;
using UnityEngine;
using Game.UI;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameMain.Entities;
using Common.Utils;
using GameData;


namespace Common.Chat
{
    public class ChatGuildLinkElement : ChatLinkElement
    {

        private ChatGuildLinkWrapper _linkWrapper = null;

        public ChatGuildLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            string desc = GetLinkDesc(linkWrapper);
            _linkWrapper = linkWrapper as ChatGuildLinkWrapper;
            this.Content = desc;
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);
        }

        private ulong GetGuildId()
        {
            ulong guildId = _linkWrapper.guildId;
            return guildId;
        }

        private void OnNameClick()
        {
            int needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            if (PlayerAvatar.Player.level < needLevel)
            {
                //ari//ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74612, needLevel));
                return;
            }
            ulong guildId = GetGuildId();
            if (guildId != 0)
            {
                GuildManager.Instance.ApplyJoinGuild(guildId);
            }
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
