﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Chat
{
    public class MiniChatEmojiElement : ChatEmojiElement
    {
        private static readonly float STANDARD_MINI_EMOJI_WIDTH = 26f;
        private static readonly float STANDARD_MINI_EMOJI_HEIGHT = 26f;

        public MiniChatEmojiElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            _width = Mathf.CeilToInt(STANDARD_MINI_EMOJI_WIDTH * (float)_width / ChatEmojiDefine.EMOJI_WIDTH);
            _height = Mathf.CeilToInt(STANDARD_MINI_EMOJI_HEIGHT * (float)_height / ChatEmojiDefine.EMOJI_HEIGHT);
        }

        protected override Vector2 GetFragmentSize()
        {
            return new Vector2(_width + 6, _height);
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
