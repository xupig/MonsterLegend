﻿using System;
using System.Collections.Generic;
using Game.UI.TextEngine;
using UnityEngine;
using System.Text.RegularExpressions;
using Game.UI;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using GameMain.Entities;

using Common.Base;
using Common.Utils;
using Common.Data;
using System.Text;
using GameData;
using Mogo.Util;
using GameMain.GlobalManager;
using GameLoader.Utils;

namespace Common.Chat
{
    public class ChatTeamLinkElement : ChatLinkElement
    {
        private ChatTeamLinkWrapper _linkWrapper;

        public ChatTeamLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            string desc = GetLinkDesc(linkWrapper);
            _linkWrapper = linkWrapper as ChatTeamLinkWrapper;
            this.Content = desc;
            if (_linkWrapper == null)
            {
                LoggerHelper.Error("ChatTeamLinkElement team link data is null ! This element content = " + _srcContent + ", block content = " + textBlock.Content);
            }
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);
        }

        private ulong GetPlayerDbid()
        {
            ulong playerDbid = 0;
            if (_linkWrapper == null)
            {
                return playerDbid;
            }
            playerDbid = _linkWrapper.playerDbid;
            return playerDbid;
        }

        private uint GetCopyId()
        {
            uint copyId = 0;
            if (_linkWrapper == null)
            {
                return copyId;
            }
            copyId = _linkWrapper.copyId;
            return copyId;
        }

        private uint GetMinLevel()
        {
            uint minLevel = 0;
            if (_linkWrapper == null)
            {
                return minLevel;
            }
            minLevel = (uint)_linkWrapper.minLevel;
            return minLevel;
        }

        private uint GetMaxLevel()
        {
            uint maxLevel = 0;
            if (_linkWrapper == null)
            {
                return maxLevel;
            }
            maxLevel = (uint)_linkWrapper.maxLevel;
            return maxLevel;
        }

        private ulong GetGuildId()
        {
            ulong guildDbid = 0;
            if (_linkWrapper == null)
            {
                return guildDbid;
            }
            guildDbid = _linkWrapper.guildId;
            return guildDbid;
        }

        private void OnNameClick()
        {
            ulong playerDbid = GetPlayerDbid();
            uint copyId = GetCopyId();
            ulong guildDbid = GetGuildId();
            uint minLevel = GetMinLevel();
            uint maxLevel = GetMaxLevel();
            if(PlayerAvatar.Player.level < minLevel)
            {
                MogoUtils.FloatTips(6016135);
                return;
            }
            if (PlayerAvatar.Player.level > maxLevel)
            {
                MogoUtils.FloatTips(6016136);
                return;
            }
            if (guildDbid != 0)
            {
                if (guildDbid != PlayerAvatar.Player.guild_id)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74875), PanelIdEnum.MainUIField);
                    return;
                }
            }
            if (playerDbid != 0 && copyId != 0)
            {
                if (playerDbid == PlayerAvatar.Player.dbid)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83034), PanelIdEnum.MainUIField);
                    return;
                }
                if (copyId == 10000000)
                {
                    if (function_helper.IsFunctionOpen(FunctionId.team_task))
                    {
                        TeamManager.Instance.AcceptInvite(1, 0, playerDbid);
                    }
                    else
                    {
                        MogoUtils.FloatTips(115030);
                    }
                }
                else
                {
                    TeamInstanceManager.Instance.ApplyJoinTeam(playerDbid, copyId);
                }

            }
            else if (playerDbid != 0 && copyId == 0)
            {
                if (playerDbid == PlayerAvatar.Player.dbid)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83034), PanelIdEnum.MainUIField);
                    return;
                }
                TeamManager.Instance.Apply(playerDbid);
            }
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
