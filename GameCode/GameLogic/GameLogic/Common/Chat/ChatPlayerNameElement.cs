﻿using System;
using System.Collections.Generic;
using Game.UI.TextEngine;
using System.Text.RegularExpressions;
using UnityEngine;
using Game.UI;
using UnityEngine.UI;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Global;

namespace Common.Chat
{
    public class ChatPlayerNameElement : ChatLinkElement
    {
        private static Regex NAME_PATTERN = new Regex(@"(?<=\#).*?(?=\,)");
        private static string PLAYER_NAME_TEMPLETE = "{0}：";
        private static readonly Color PLAYER_SELF_NAME_COLOR = new Color(80f / 255f, 236f / 255f, 2f / 255f);
        private static readonly Color PLAYER_OTHER_NAME_COLOR = new Color(79f / 255f, 132f / 255f, 253f / 255f);
        private static readonly string OTHER_NAME_COLOR_TEMPLATE = "<color=#4f84fd>{0}</color>";

        private GameObject _go;
        private string _playerRealName = string.Empty;
        private bool _isPrivateName = false;

        public ChatPlayerNameElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            this.Content = GetPlayerName();
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            TextWrapper text = ChatTextBlock.CreateChatLinkTextComponent(go);
            text.text = _fragmentContent;
            text.color = GetPlayerNameColor(text);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);
            _go = go;
        }

        private string GetPlayerName()
        {
            string playerName = NAME_PATTERN.Match(_srcContent).Value;
            if (playerName.IndexOf(ChatTextBlock.PRIVATE_NAME_SEPERATE_CHAR) != -1)
            {
                _isPrivateName = true;
                string[] splits = playerName.Split(new char[] { ChatTextBlock.PRIVATE_NAME_SEPERATE_CHAR });
                if (splits.Length == 2)
                {
                    _playerRealName = splits[0];
                }
                else if (splits.Length == 3)
                {
                    _playerRealName = splits[1];
                }
                string colorName = string.Format(OTHER_NAME_COLOR_TEMPLATE, _playerRealName);
                playerName = playerName.Replace(ChatTextBlock.PRIVATE_NAME_SEPERATE_CHAR.ToString(), "");
                playerName = playerName.Replace(_playerRealName, colorName);
            }
            else
            {
                _playerRealName = playerName;
            }
            playerName = string.Format(PLAYER_NAME_TEMPLETE, playerName);
            return playerName;
        }

        private Color GetPlayerNameColor(TextWrapper text)
        {
            if (!_isPrivateName)
            {
                if (text.text.IndexOf(PlayerAvatar.Player.name) != -1)
                {
                    return PLAYER_SELF_NAME_COLOR;
                }
                else
                {
                    return PLAYER_OTHER_NAME_COLOR;
                }
            }
            else
            {
                return ColorDefine.GetColorById(ColorDefine.COLOR_ID_CHAT_PRIVATE);
            }
        }

        private void OnNameClick()
        {
            ChatTextBlock chatBlock = this.TextBlock as ChatTextBlock;
            ulong sendDbid = chatBlock.SendDbid;
            if (chatBlock.ChannelId != public_config.CHANNEL_ID_PRIVATE)
            {
                if (sendDbid != PlayerAvatar.Player.dbid)
                {
                    ShowPlayerDetail(_playerRealName);
                }
            }
            else
            {
                ShowPlayerDetail(_playerRealName);
            }
        }

        private void ShowPlayerDetail(string playerName)
        {
            Vector3 targetPos = new Vector3(941f, 5000f, 5000f);
            PlayerInfoParam param = new PlayerInfoParam(targetPos, _go.GetComponent<RectTransform>().sizeDelta,
                PlayerInfoParam.AlignmentType.SpecifyPos, PlayerInfoParam.AlignmentType.ScreenCenter);
            FriendManager.Instance.GetPlayerDetailInfo(playerName, param);
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
