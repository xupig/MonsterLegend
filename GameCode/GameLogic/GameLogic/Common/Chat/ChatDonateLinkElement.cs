﻿#region 模块信息
/*==========================================
// 文件名：ChatDonateLinkElement
// 命名空间: GameLogic.GameLogic.Common
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/2 10:54:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Structs;
using GameMain.GlobalManager;

using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Chat
{
    public class ChatDonateLinkElement : ChatLinkElement
    {
        private static char[] split = new char[] {',' };
        private ulong dbId;
        private int itemId;

        public ChatDonateLinkElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            string[] result = _srcContent.Split(split);
           
            this.Content = result[0].Substring(1,result[0].Length-1);
            dbId = ulong.Parse(result[2]);
            itemId = int.Parse(result[3].Substring(0,result[3].Length-1));
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);
        }

        private void OnNameClick()
        {
            Dictionary<int,BaseItemInfo> dict = PlayerDataManager.Instance.BagData.GetBagData(Data.BagType.ItemBag).GetAllItemInfo();
            List<BaseItemInfo> itemList = null;
            foreach(KeyValuePair<int,BaseItemInfo> kvp in dict)
            {
                if(kvp.Value!=null)
                {
                    if (kvp.Value.Id == itemId && kvp.Value.UserList != null && kvp.Value.UserList.Count > 0)
                    {
                        if(itemList == null)
                        {
                            itemList = new List<BaseItemInfo>();
                        }
                        itemList.Add(kvp.Value);
                    }
                }
            }
            if(itemList == null)
            {
                MogoUtils.FloatTips(6016143);
            }
            else
            {
                PanelIdEnum.TeamDonate.Show(new object[] { dbId,itemList });
            }
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
