﻿#region 模块信息
/*==========================================
// 文件名：ChatDonateLinkWrapper
// 命名空间: GameLogic.GameLogic.Common.Chat
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/2 10:05:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Chat
{
    public class ChatDonateLinkWrapper
    {
        public string desc;
        public string name;
        public ulong playerDbid;
        public int id;

        public ChatDonateLinkWrapper(string desc, string name, ulong playerDbid, int id)
        {
            this.desc = desc;
            this.name = name;
            this.playerDbid = playerDbid;
            this.id = id;
        }
    }
}
