﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Game.Asset;
using MogoEngine.Utils;
using UnityEngine;
using UnityEngine.UI;
using Game.UI;
using Game.UI.TextEngine;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using System.Text.RegularExpressions;
using GameMain.GlobalManager;

namespace Common.Chat
{
    public class MiniChatTextBlock : ChatTextBlock
    {
        private const int MAX_TEXT_LINE_NUM = 2;
        private static readonly string DESC_WITH_NAME_FORMAT = "{0}：{1}";
        private static readonly string SYSTEM_REPLACED_TITLE_PREFIX = "$";
        private static readonly string SYSTEM_REPLACED_TITLE_SUFFIX = "$";
        private static Regex SYSTEM_REPLACED_TITLE_PATTERN = new Regex(@"\$.*?\$");

        private static Dictionary<string, string> _replacedSystemTitleDict = new Dictionary<string, string>();

        private static int _blockIndex = 0;
        private static bool hasInitialFontData = false;
        private static FontData fontData;

        public static MiniChatTextBlock CreateMiniBlock(PbChatInfoResp chatInfo, float maxWidth, Color fontColor)
        {
            if(hasInitialFontData == false)
            {
                hasInitialFontData = true;
                fontData = new FontData();
                fontData.alignment = TextAnchor.UpperLeft;
                Font font = ObjectPool.Instance.GetAssemblyObject(FONT_ASSET_KEY) as Font;
                fontData.font = font;
                fontData.lineSpacing = 1.0f;
                fontData.richText = true;
            }
            if (_chatData == null)
            {
                _chatData = PlayerDataManager.Instance.ChatData;
            }
            UpdateTextGenerationSettings(fontData, fontColor);

            string content = GetLinkContentWithoutSendTime(chatInfo);
            string[] splits = content.Split(ChatData.CHAT_CONTENT_AND_LINK_SPLIT_TAG);
            content = splits[0];
            bool isSystemLink = false;
            string systemTitle;
            if (ChatInfoHelper.isSystem(chatInfo) == false)
            {
                systemTitle = GetSystemTitle(chatInfo);
                if (ChatManager.Instance.IsSendToSystemChannel(chatInfo))
                {
                    string name = string.Format(NAME_TEMPLETE, chatInfo.send_name);
                    content = name + content;
                    if (ChatManager.Instance.IsChatMsgContainsLink(chatInfo))
                    {
                        content = GetContentWithReplacedSystemTitle(content, systemTitle);
                        isSystemLink = true;
                    }
                    else
                    {
                        content = string.Format(DESC_WITH_NAME_FORMAT, systemTitle, content);
                    }
                }
                else
                {
                    content = string.Format(DESC_WITH_NAME_FORMAT, chatInfo.send_name, content);                    
                }
            }
            else
            {
                systemTitle = GetSystemTitle(chatInfo);
                if (ChatManager.Instance.IsChatMsgContainsLink(chatInfo))
                {
                    //为了防止[世界]这种形式的title会影响链接内容的匹配，就去掉[]，后面补##
                    content = GetContentWithReplacedSystemTitle(content, systemTitle);
                    isSystemLink = true;
                }
                else
                {
                    content = string.Format(DESC_WITH_NAME_FORMAT, systemTitle, content);
                }
            }

            MiniChatTextBlock block = new MiniChatTextBlock(content, maxWidth, fontData, fontColor);
            if (chatInfo.chat_item_list.Count > 0)
            {
                for (int i = 0; i < chatInfo.chat_item_list.Count; i++)
                {
                    string link = MogoProtoUtils.ParseByteArrToString(chatInfo.chat_item_list[i].uuid_bytes);
                    block.AddItemLink(link);
                }
            }

            if (splits.Length >= 2)
            {
                block.InitLinkWrapperList(chatInfo);
            }
            else
            {
                block.IsExsitLinkDataString = false;
            }

            block.SendDbid = chatInfo.send_dbid;
            block.ChannelId = (int)chatInfo.channel_id;
            if (chatInfo.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
            {
                block.VoiceId = chatInfo.voice_id;
            }

            block.Build();

            if (isSystemLink)
            {
                block.SystemTitle = systemTitle;
                block.ProcessSystemContentTitle();
            }
            //尽量优化的在内部不添加btn
            block.CancelChatLinkBtnClick();
            return block;
        }

        private string _systemTitle;
        public string SystemTitle
        {
            get { return _systemTitle; }
            protected set { _systemTitle = value; }
        }

        public MiniChatTextBlock(string content, float maxWidth, FontData fontData, Color fontColor)
            : base(content, maxWidth, fontData, fontColor)
        {

        }

        protected override ContentElement CreateOtherElement(string elementContent)
        {
            if (EMOJI_PATTERN.IsMatch(elementContent) == true)
            {
                return new MiniChatEmojiElement(elementContent, this);
            }
            else
            {
                return base.CreateOtherElement(elementContent);
            }
        }

        protected override void ShowContent()
        {
            TextLine line = CreateTextLine(null, _maxWidth);
            while (line != null)
            {
                _lineList.Add(line);
                if (_lineList.Count >= 2)
                {
                    break;
                }
                line = CreateTextLine(line, _maxWidth);
            }
            DoLineListLayout();
        }

        private static string GetContentWithReplacedSystemTitle(string content, string systemTitle)
        {
            if (!_replacedSystemTitleDict.ContainsKey(systemTitle))
            {
                string replacedTitle = systemTitle;
                replacedTitle = replacedTitle.TrimStart(new char[] { '[' });
                replacedTitle = replacedTitle.TrimEnd(new char[] { ']' });
                replacedTitle = replacedTitle.Insert(0, SYSTEM_REPLACED_TITLE_PREFIX);
                replacedTitle = replacedTitle.Insert(replacedTitle.Length, SYSTEM_REPLACED_TITLE_SUFFIX);
                content = string.Format(DESC_WITH_NAME_FORMAT, replacedTitle, content);
                _replacedSystemTitleDict.Add(systemTitle, replacedTitle);
            }
            else
            {
                content = string.Format(DESC_WITH_NAME_FORMAT, _replacedSystemTitleDict[systemTitle], content);
            }
            return content;
        }

        private void CancelChatLinkBtnClick()
        {
            for (int i = 0; i < this.GameObject.transform.childCount; i++)
            {
                Transform child = this.GameObject.transform.GetChild(i);
                for (int j = 0; j < child.childCount; j++)
                {
                    Transform fragmentChild = child.GetChild(j);
                    Button btn = fragmentChild.GetComponent<Button>();
                    if (btn != null)
                    {
                        btn.enabled = false;
                    }
                }
            }
        }

        private void ProcessSystemContentTitle()
        {
            Transform child = this.GameObject.transform.GetChild(0);
            if (child != null)
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    Transform lineChild = child.GetChild(i);
                    if (lineChild.name.IndexOf("text") != -1)
                    {
                        TextWrapper textWrapper = lineChild.gameObject.GetComponent<TextWrapper>();
                        string srcTitle = SYSTEM_REPLACED_TITLE_PATTERN.Match(textWrapper.text).Value;
                        if (!string.IsNullOrEmpty(srcTitle))
                        {
                            string text = textWrapper.text;
                            string subMsg = text.Substring(srcTitle.Length, text.Length - srcTitle.Length);
                            textWrapper.text = this.SystemTitle + subMsg;
                        }
                        break;
                    }
                }
            }
        }

        protected override string GetGameObjectName()
        {
            _blockIndex++;
            if (_blockIndex == int.MaxValue)
            {
                _blockIndex = 0;
            }
            return string.Format("miniTextBlock_{0}", _blockIndex);
        }

        private static string GetSystemTitle(PbChatInfoResp chatInfo)
        {
            int channel = (int)chatInfo.channel_id;
            int msgType = (int)chatInfo.msg_type;
            string title = "";
            if (msgType == (int)ChatContentType.TipsMsg)
            {
                title = ChatConst.CHAT_SYSTEM_TITLE_TIPS;
            }
            else if (ChatManager.Instance.IsSendToSystemChannel(chatInfo))
            {
                title = ChatConst.CHAT_SYSTEM_TITLE_SYSTEM;
            }
            else if (msgType == (int)ChatContentType.Question)
            {
                title = ChatConst.CHAT_SYSTEM_TITLE_QUESTION;
            }
            else
            {
                switch (channel)
                {
                    case public_config.CHANNEL_ID_WORLD:
                        title = ChatConst.CHAT_SYSTEM_TITLE_WORLD;
                        break;
                    case public_config.CHANNEL_ID_VICINITY:
                        title = ChatConst.CHAT_SYSTEM_TITLE_VICINITY;
                        break;
                    case public_config.CHANNEL_ID_TEAM:
                        title = ChatConst.CHAT_SYSTEM_TITLE_TEAM;
                        break;
                    case public_config.CHANNEL_ID_GUILD:
                        title = ChatConst.CHAT_SYSTEM_TITLE_GUILD;
                        break;
                    case public_config.CHANNEL_ID_PRIVATE:
                        title = ChatConst.CHAT_SYSTEM_TITLE_PRIVATE;
                        break;
                    default:
                        break;
                }
            }
            return title;
        }

    }
}
