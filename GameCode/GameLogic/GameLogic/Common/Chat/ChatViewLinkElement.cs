﻿using Game.UI;
using Game.UI.TextEngine;
using GameMain.GlobalManager.SubSystem;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Common.Data;
using System.Collections.Generic;
using System.Text;
using GameData;

namespace Common.Chat
{
    public class ChatViewLinkElement : ChatLinkElement
    {
        private ChatViewLinkWrapper _linkWrapper;

        public ChatViewLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            _linkWrapper = linkWrapper as ChatViewLinkWrapper;
            this.Content = GetLinkDesc(linkWrapper);
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnClick);
        }

        private int GetViewId()
        {
            return _linkWrapper.viewId;
        }

        private void OnClick()
        {
            view_helper.OpenView(GetViewId());
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
