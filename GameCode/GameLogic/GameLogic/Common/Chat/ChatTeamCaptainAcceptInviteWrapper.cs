﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Chat
{
    public class ChatTeamCaptainAcceptInviteWrapper
    {
        public uint TeamId;
        public ulong ApplicationDbid;
        public string ApplicantName;
        public int ApplicantLevel;

        public ChatTeamCaptainAcceptInviteWrapper(uint teamId, ulong applicationDbid, string applicantName, int applicantLevel)
        {
            this.TeamId = teamId;
            this.ApplicationDbid = applicationDbid;
            this.ApplicantName = applicantName;
            this.ApplicantLevel = applicantLevel;
        }
    }
}
