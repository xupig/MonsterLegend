﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Game.UI.TextEngine;
using Game.UI;
using Common.Utils;
using Common.Global;


namespace Common.Chat
{
    public class ChatItemLinkElement : GraphicElement
    {
        private static string ITEM_NAME_TEMPLATE = "【{0}】";
        private int _itemLinkIndex;

        private TextWrapper _text;

        private ChatItemLinkWrapper _linkWrapper;

        public ChatItemLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            _linkWrapper = linkWrapper as ChatItemLinkWrapper;
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            TextWrapper text = ChatTextBlock.CreateChatLinkTextComponent(go);
            text.color = ColorDefine.GetColorById(GetItemLinkQuality());
            text.text = GetItemLinkLabel();
            text.SetDimensionsDirty();
            _text = text;
            ChatLinkElement.AddTextShadowEffect(go);
            int itemLinkType = GetItemLinkType();
            if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Normal)
            {
                _itemLinkIndex = (this.TextBlock as ChatTextBlock).GetItemLinkIndex();
            }
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnItemLinkClick);
        }

        private string GetItemLinkLabel()
        {
            if (_linkWrapper != null)
            {
                return string.Format(ITEM_NAME_TEMPLATE, _linkWrapper.LinkDesc);
            }
            else
            {
                return string.Empty;
            }
        }

        private int GetItemLinkQuality()
        {
            if (_linkWrapper != null)
            {
                return _linkWrapper.quality;
            }
            else
            {
                return ColorDefine.COLOR_ID_WHITE;
            }
        }

        private int GetWingId()
        {
            int wingId = 0;
            if (_linkWrapper != null)
            {
                wingId = _linkWrapper.itemId;
            }
            return wingId;
        }

        private int GetTitleId()
        {
            int titleId = 0;
            if (_linkWrapper != null)
            {
                titleId = _linkWrapper.itemId;
            }
            return titleId;
        }

        private ulong GetTitleGetTime()
        {
            ulong getTime = 0;
            if (_linkWrapper != null)
            {
                getTime = _linkWrapper.getTime;
            }
            return getTime;
        }

        private int GetAchievementId()
        {
            int achievementId = 0;
            if (_linkWrapper != null)
            {
                achievementId = _linkWrapper.achievementId;
            }
            return achievementId;
        }

        private int GetItemId()
        {
            int itemId = 0;
            if (_linkWrapper != null)
            {
                itemId = _linkWrapper.itemId;
            }
            return itemId;
        }

        private int GetItemLinkType()
        {
            int itemLinkType = 0;
            if (_linkWrapper != null)
            {
                itemLinkType = _linkWrapper.itemLinkType;
            }
            return itemLinkType;
        }

        private void OnItemLinkClick()
        {
            int itemLinkType = GetItemLinkType();
            if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Normal)
            {
                string itemLink = (this.TextBlock as ChatTextBlock).GetItemLink(_itemLinkIndex);
                if (string.IsNullOrEmpty(itemLink) == false)
                {
                    ChatManager.Instance.RequestChatItemDetail(itemLink);
                }
            }
            else if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Wing)
            {
                int wingId = GetWingId();
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.WingToolTips, wingId, PanelIdEnum.Wing);
            }
            else if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Title)
            {
                object[] obj = new object[2];
                obj[0] = GetTitleId();
                obj[1] = GetTitleGetTime();
                UIManager.Instance.ShowPanel(PanelIdEnum.TitleDetail, obj);
            }
            else if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Achievement)
            {
                int achievementId = GetAchievementId();
                ChatTextBlock textBlock = this.TextBlock as ChatTextBlock;
                EventDispatcher.TriggerEvent<GameObject, GameObject, int>(ChatEvents.ON_CLICK_ACHIEVEMENT_LINK, textBlock.GameObject, _text.gameObject, achievementId);
            }
            else if (itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Item)
            {
                int itemId = GetItemId();
               //ari ToolTipsManager.Instance.ShowItemTip(itemId, PanelIdEnum.Chat, false);
            }
        }

        protected override Vector2 GetFragmentSize()
        {
            return ChatTextBlock.GetChatLinkContentSize(GetItemLinkLabel(), _text);
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
