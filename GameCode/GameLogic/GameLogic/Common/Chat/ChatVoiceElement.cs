﻿using System;
using System.Collections.Generic;
using Game.UI.TextEngine;
using UnityEngine;
using System.Text.RegularExpressions;
using Game.UI;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using GameMain.GlobalManager.SubSystem;

namespace Common.Chat
{
    public class ChatVoiceElement : ChatLinkElement
    {
        private static Regex CONTENT_PATTERN = new Regex(@"(?<=\[).*?(?=\,)");

        public ChatVoiceElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            this.Content = GetContent();
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            TextWrapper text = ChatTextBlock.CreateChatLinkTextComponent(go);
            text.text = _fragmentContent;
            if (ChatManager.Instance.IsCancelChatVoiceFunction())
                return;
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnContentClick);
        }

        private string GetContent()
        {
            return CONTENT_PATTERN.Match(_srcContent).Value;
        }

        private void OnContentClick()
        {
            //播放语音
            string voiceId = (this.TextBlock as ChatTextBlock).VoiceId;
            ulong sendDbid = (this.TextBlock as ChatTextBlock).SendDbid;
            if (!string.IsNullOrEmpty(voiceId))
            {
                EventDispatcher.TriggerEvent<string, ulong>(ChatEvents.PLAY_VOICE, voiceId, sendDbid);
            }
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
