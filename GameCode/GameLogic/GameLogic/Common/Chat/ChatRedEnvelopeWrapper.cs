﻿using System;
using System.Collections.Generic;
using Common.Data;

namespace Common.Chat
{
    public class ChatRedEnvelopeWrapper : ChatLinkBaseWrapper
    {
        public string playerName;
        public uint redEnvelopeId;
        public int systemInfoId;

        private const string RED_ENVELOPE_TEMPLATE = "[{0};{1};{2};{3}]";

        public static ChatRedEnvelopeWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatRedEnvelopeWrapper(dataStringSplits);
        }

        public ChatRedEnvelopeWrapper(string[] dataStringSplits)
            : base(ChatLinkType.RedEnvelope, dataStringSplits)
        {
            
        }

        public ChatRedEnvelopeWrapper(uint redEnvelopeId, string name, string playerName, int systemInfoId)
            : base(ChatLinkType.RedEnvelope)
        {
            this.redEnvelopeId = redEnvelopeId;
            this.playerName = playerName;
            this.systemInfoId = systemInfoId;
            LinkDesc = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            redEnvelopeId = uint.Parse(dataStringSplits[1]);
            playerName = dataStringSplits[2];
            systemInfoId = int.Parse(dataStringSplits[3]);
        }

        public override string ToDataString()
        {
            return string.Format(RED_ENVELOPE_TEMPLATE, ((int)LinkType).ToString(), redEnvelopeId.ToString(), playerName, systemInfoId.ToString());
        }
    }
}
