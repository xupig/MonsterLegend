﻿using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Chat
{
    public class ChatTeamCaptainAcceptInviteElement : ChatLinkElement
    {
        private uint _teamId;
        private ulong _dbid;

        public ChatTeamCaptainAcceptInviteElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            char[] splitChar = new char[] { ',', ']' };
            string[] stringList = _srcContent.Split(splitChar);
            int length = stringList.Length;
            _teamId = uint.Parse(stringList[length - 3]);
            _dbid = ulong.Parse(stringList[length - 2]);
            this.Content = GetContent(content);
        }

        private string GetContent(string content)
        {
            return content.Substring(1, content.IndexOf("】"));
        }

        protected override void AddFragmentComponent(UnityEngine.GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            TeamManager.Instance.CaptainAcceptApply(1, _teamId, _dbid);
        }
    }
}
