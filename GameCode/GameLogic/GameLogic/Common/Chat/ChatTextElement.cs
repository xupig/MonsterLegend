﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Game.UI.TextEngine;
using Game.UI;

namespace Common.Chat
{
    public class ChatTextElement : TextElement
    {
        public ChatTextElement(string elementContent, TextBlock textBlock)
            : base(elementContent, textBlock)
        {

        }

        protected override Text AddTextComponent(GameObject go)
        {
            return go.AddComponent<TextWrapper>();
        }
    }
}