﻿#region 模块信息
/*==========================================
// 文件名：ChatWantDonateLinkElement
// 命名空间: GameLogic.GameLogic.Common.Chat
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/11 9:54:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Chat
{
    public class ChatWantDonateLinkElement:ChatLinkElement
    {
        private static char[] split = new char[] {',' };
        private uint time;
        private uint id;

        public ChatWantDonateLinkElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            string[] result = _srcContent.Split(split);
           
            this.Content = result[0].Substring(1,result[0].Length-1);
            time = uint.Parse(result[2]);
            id = uint.Parse(result[3].Substring(0, result[3].Length - 1));
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);
        }

        private void OnNameClick()
        {
            if (Global.Global.serverTimeStampSecond > time)
            {
                Debug.LogError("服务器时间:" + Global.Global.serverTimeStampSecond+",赠送截止时间"+time);
                MogoUtils.FloatTips(99289);
                return;
            }
            //else if(PlayerDataManager.Instance.TeamData.requiredDict.ContainsKey(id) && PlayerDataManager.Instance.TeamData.requiredDict[id] == uint.MaxValue)
            //{
            //    MogoUtils.FloatTips(6016152);
            //}
            //else
            //{
            //     MissionManager.Instance.RequestTransfer(Convert.ToInt32(id));
            //}
            if(PlayerDataManager.Instance.TeamData.TeamId == 0)
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 4 });
            }
            else if(PlayerDataManager.Instance.TeamData.CaptainId == PlayerAvatar.Player.dbid)
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 4 });
            }
            else
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 3 });
            }
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
       
    }
}
