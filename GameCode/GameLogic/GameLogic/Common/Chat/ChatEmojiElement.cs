﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Game.UI.TextEngine;
using Common.Utils;
using Common.Global;

namespace Common.Chat
{
    public class ChatEmojiElement : GraphicElement
    {
        private static Regex EMOJI_INDEX_EXTRACT_PATTERN = new Regex(@"(?<=\[E)\d{1,2}");

        private int _index;
        protected float _width;
        protected float _height;

        public ChatEmojiElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            _index = GetEmojiIndex();
            _width = ChatEmojiDefine.GetEmojiWidth(_index);
            _height = ChatEmojiDefine.GetEmojiHeight(_index);
        }

        private int GetEmojiIndex()
        {
            return Convert.ToInt32(EMOJI_INDEX_EXTRACT_PATTERN.Match(this.Content).Value);
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            MogoSpriteAnimationUtils.AddAnimation(go, ChatEmojiDefine.GetEmojiSpriteNameList(_index), _width, _height);
        }

        protected override Vector2 GetFragmentSize()
        {
            return new Vector2(_width, _height);
        }

        protected override Vector2 GetFragmentPostion()
        {
            return new Vector2(0.0f, 7.0f);
        }
    }
}
