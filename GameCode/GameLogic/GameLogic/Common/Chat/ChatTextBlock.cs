﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Game.UI.TextEngine;
using Common.Global;
using MogoEngine.Utils;
using Game.Asset;
using Game.UI;
using GameMain.Entities;

using Common.Utils;
using GameLoader.Config;

namespace Common.Chat
{
    public class ChatTextBlock : TextBlock
    {
        public static readonly string FONT_ASSET_KEY = string.Format("Font${0}$MicrosoftYaHei_unicode.ttf.u", SystemConfig.Language);
        public static Regex GRAPHIC_PATTERN = new Regex(@"(\[E\d{1,2}\])|(\[.*?\,(\d{1,20}\,){0,1}\d{1,20}\])|(\#.*?,(name)?\#)|(\#resp,.*?,\d{1,20}\#)");
        protected static Regex EMOJI_PATTERN = new Regex(@"\[E\d{1,2}\]");
        private static Regex NAME_PATTERN = new Regex(@"\#.*?,(name)?\#");
        protected static Regex LINK_PATTERN = ChatLinkData.LINK_CONTENT_PATTERN;
        protected static Regex LINK_TYPE_PATTERN = ChatLinkData.LINK_TYPE_PATTERN;
        protected static Regex PRIVATE_RESP_PATTERN = new Regex(@"\#resp,.*?,\d{1,20}\#");
        public const float MAX_WIDTH = 440.0f;
        public static readonly Color COLOR_SYSTEM_WORLD_CANNEL = new Color(255f / 255f, 241f / 255f, 0f);

        protected const string NAME_TEMPLETE = "#{0},name#";
        protected const string PRIVATE_CHAT_RESP_TEMPLATE = "#resp,{0},{1}#";
        private static readonly string YOU_SEND_PRIVATE_CHAT_NAME_TEMPLATE = MogoLanguageUtil.GetContent(32545);
        private static readonly string OTHER_SEND_PRIVATE_CHAT_NAME_TEMPLATE = MogoLanguageUtil.GetContent(32546);
        public const char PRIVATE_NAME_SEPERATE_CHAR = '&';

        private static FontData fontData;
        private static bool hasInitialFontData = false;
        protected static ChatData _chatData;

        public static ChatTextBlock CreateBlock(PbChatInfoResp chatInfo, float maxWidth, Color fontColor, bool showName = false)
        {
            if(hasInitialFontData == false)
            {
                hasInitialFontData = true;
                fontData = new FontData();
                fontData.alignment = TextAnchor.UpperLeft;
                Font font = ObjectPool.Instance.GetAssemblyObject(FONT_ASSET_KEY) as Font;
                fontData.font = font;
                fontData.lineSpacing = 1.0f;
                fontData.richText = true;
            }
            if (_chatData == null)
            {
                _chatData = PlayerDataManager.Instance.ChatData;
            }
            UpdateTextGenerationSettings(fontData, fontColor);

            string content = GetLinkContentWithoutSendTime(chatInfo);
            string[] splits = content.Split(ChatData.CHAT_CONTENT_AND_LINK_SPLIT_TAG);
            content = splits[0];
            if(showName == true)
            {
                if(ChatInfoHelper.isSystem(chatInfo) == false)
                {
                    string name = string.Empty;
                    if (chatInfo.channel_id == public_config.CHANNEL_ID_PRIVATE)
                    {
                        if (chatInfo.send_dbid != PlayerAvatar.Player.dbid)
                        {
                            name = string.Format(OTHER_SEND_PRIVATE_CHAT_NAME_TEMPLATE, chatInfo.send_name + PRIVATE_NAME_SEPERATE_CHAR.ToString());
                        }
                        else
                        {
                            name = PRIVATE_NAME_SEPERATE_CHAR.ToString() + chatInfo.accept_name + PRIVATE_NAME_SEPERATE_CHAR.ToString();
                            name = string.Format(YOU_SEND_PRIVATE_CHAT_NAME_TEMPLATE, name);
                        }
                        name = string.Format(NAME_TEMPLETE, name);
                    }
                    else
                    {
                        name = string.Format(NAME_TEMPLETE, chatInfo.send_name);
                    }
                    content = string.Format("{0}{1}", name, content);
                }
                else
                {
                    if (chatInfo.msg_type == (int)ChatContentType.Question)
                    {
                        string name = string.Format(NAME_TEMPLETE, ChatConst.CHAT_SYSTEM_TITLE_QUESTION);
                        content = name + content;
                    }
                }
            }

            if (chatInfo.channel_id == public_config.CHANNEL_ID_PRIVATE && ChatInfoHelper.isSystem(chatInfo) == false && chatInfo.send_dbid != PlayerAvatar.Player.dbid)
            {
                //ari content = content + string.Format(PRIVATE_CHAT_RESP_TEMPLATE, ChatUtil.GetFriendName(chatInfo), ChatUtil.GetFriendDbid(chatInfo).ToString());
            }

            ChatTextBlock block = new ChatTextBlock(content, maxWidth, fontData, fontColor);
            if(chatInfo.chat_item_list.Count > 0)
            {
                for(int i = 0; i < chatInfo.chat_item_list.Count; i++)
                {
                    string link = MogoProtoUtils.ParseByteArrToString(chatInfo.chat_item_list[i].uuid_bytes);
                    block.AddItemLink(link);
                }
            }

            if (splits.Length >= 2)
            {
                block.InitLinkWrapperList(chatInfo);
            }
            else
            {
                block.IsExsitLinkDataString = false;
            }

            block.SendDbid = chatInfo.send_dbid;
            block.ChannelId = (int)chatInfo.channel_id;
            if(chatInfo.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
            {
                block.VoiceId = chatInfo.voice_id;
            }
            block.Build();
            return block;
        }

        protected static string GetLinkContentWithoutSendTime(PbChatInfoResp chatInfo)
        {
            if(ChatManager.Instance.IsChatMsgContainsSendTime(chatInfo))
            {
                return ChatContentUtils.GetLinkContentWithoutSendTime(chatInfo.msg);
            }
            return chatInfo.msg;
        }

        private List<string> _itemLinkList = new List<string>();
        private int _itemLinkIndex = 0;

        private ulong _sendDbid = 0;
        public ulong SendDbid
        {
            get { return _sendDbid; }
            set { _sendDbid = value; }
        }

        private string _voiceId = string.Empty;
        public string VoiceId
        {
            get { return _voiceId; }
            set { _voiceId = value; }
        }

        private int _channelId = public_config.CHANNEL_ID_WORLD;
        public int ChannelId
        {
            get { return _channelId; }
            set { _channelId = value; }
        }

        private List<ChatLinkBaseWrapper> _linkWrapperList = null;
        public List<ChatLinkBaseWrapper> LinkWrapperList
        {
            get { return _linkWrapperList; }
            protected set { _linkWrapperList = value; }
        }

        private int _linkWrapperIndex = 0;

        private bool _isExsitLinkDataString = false;
        public bool IsExsitLinkDataString
        {
            get { return _isExsitLinkDataString; }
            protected set { _isExsitLinkDataString = value; }
        }

        public ChatTextBlock(string content, float maxWidth, FontData fontData, Color fontColor)
            : base(content, GRAPHIC_PATTERN, maxWidth, fontData, fontColor)
        {
        }

        protected void InitLinkWrapperList(PbChatInfoResp chatInfo)
        {
            LinkWrapperList = _chatData.ChatLinkData.GetChatLinkWrapperList(chatInfo);
            if (LinkWrapperList != null && LinkWrapperList.Count > 0)
            {
                IsExsitLinkDataString = true;
            }
            else
            {
                IsExsitLinkDataString = false;
            }
        }

        protected override ContentElement CreateTextElement(string elementContent)
        {
            return new ChatTextElement(elementContent, this);
        }

        protected override ContentElement CreateOtherElement(string elementContent)
        {
            if(EMOJI_PATTERN.IsMatch(elementContent) == true)
            {
                return new ChatEmojiElement(elementContent, this);
            }
            else if(NAME_PATTERN.IsMatch(elementContent) == true)
            {
                return new ChatPlayerNameElement(elementContent, this);
            }
            else if(LINK_PATTERN.IsMatch(elementContent))
            {
                string strLinkType = LINK_TYPE_PATTERN.Match(elementContent).Value;
                int linkType = (int)ChatLinkType.Item;
                int.TryParse(strLinkType, out linkType);
                int linkWrapperIndex = GetLinkWrapperIndex();
                if (!IsValidLinkContent(linkType))
                {
                    return new ChatTextElement(elementContent, this);
                }

                if(linkType == (int)ChatLinkType.Voice)
                {
                    return new ChatVoiceElement(elementContent, this);
                }
                else if(linkType == (int)ChatLinkType.Guild)
                {
                    return new ChatGuildLinkElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
                else if(linkType == (int)ChatLinkType.Team)
                {
                    return new ChatTeamLinkElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
                else if(linkType == (int)ChatLinkType.SharePosition)
                {
                    return new ChatSharePosLinkElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
                else if(linkType == (int)ChatLinkType.View)
                {
                    return new ChatViewLinkElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
                else if (linkType == (int)ChatLinkType.RedEnvelope)
                {
                    return new ChatRedEnvelopeElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
                else if (linkType == (int)ChatLinkType.TradeMarket)
                {
                    return new ChatTradeLinkElement(elementContent, this);
                }
                else if(linkType == (int)(ChatLinkType.Donate))
                {
                    return new ChatDonateLinkElement(elementContent, this);
                }
                else if(linkType == (int)(ChatLinkType.WantDonate))
                {
                    return new ChatWantDonateLinkElement(elementContent,this);
                }
                else if (linkType == (int)ChatLinkType.TeamCaptainAccept)
                {
                    return new ChatTeamCaptainAcceptInviteElement(elementContent, this);
                }
                else if (linkType == (int)ChatLinkType.QuestionValidTime)
                {
                    return new ChatQuestionValidTimeElement(elementContent, this);
                }
                else if (linkType == (int)ChatLinkType.Duel)
                {
                    return new ChatDuelElement(elementContent, this);
                }
                else
                {
                    return new ChatItemLinkElement(elementContent, this, GetLinkWrapper(linkWrapperIndex));
                }
            }
            else if (PRIVATE_RESP_PATTERN.IsMatch(elementContent))
            {
                return new ChatPrivateRespElement(elementContent, this);
            }
            return new ChatTextElement(elementContent, this);
        }

        private bool IsValidLinkContent(int linkType)
        {
            //如果链接数据继承自ChatLinkBaseWrapper，并且注册到ChatLinkWrapperManager中，就需要检测是否存在链接数据
            if (ChatLinkWrapperManager.Instance.IsExsitChatLinkType((ChatLinkType)linkType))
            {
                return _isExsitLinkDataString;
            }
            return true;
        }

        public int GetLinkWrapperIndex()
        {
            int index = _linkWrapperIndex;
            _linkWrapperIndex++;
            return index;
        }

        public ChatLinkBaseWrapper GetLinkWrapper(int index)
        {
            if (_linkWrapperList != null && _linkWrapperList.Count > index)
            {
                return _linkWrapperList[index];
            }
            return null;
        }

        protected void AddItemLink(string link)
        {
            _itemLinkList.Add(link);
        }

        public string GetItemLink(int index)
        {
            if(_itemLinkList.Count > index)
            {
                return _itemLinkList[index];
            }
            return string.Empty;
        }

        public int GetItemLinkIndex()
        {
            return _itemLinkIndex++;
        }

        public static Color GetChannelFontColor(int channel)
        {
            int colorId = ColorDefine.COLOR_ID_CHAT_ALL;
            switch(channel)
            {
                case public_config.CHANNEL_ID_ALL:
                    colorId = ColorDefine.COLOR_ID_CHAT_ALL;
                    break;
                case public_config.CHANNEL_ID_WORLD:
                    colorId = ColorDefine.COLOR_ID_CHAT_WORLD;
                    break;
                case public_config.CHANNEL_ID_VICINITY:
                    colorId = ColorDefine.COLOR_ID_CHAT_NEARBY;
                    break;
                case public_config.CHANNEL_ID_TEAM:
                    colorId = ColorDefine.COLOR_ID_CHAT_TEAM;
                    break;
                case public_config.CHANNEL_ID_GUILD:
                    colorId = ColorDefine.COLOR_ID_CHAT_GUILD;
                    break;
                case public_config.CHANNEL_ID_PRIVATE:
                    colorId = ColorDefine.COLOR_ID_CHAT_PRIVATE;
                    break;
            }
            return ColorDefine.GetColorById(colorId);
        }

        public static TextWrapper CreateChatLinkTextComponent(GameObject linkGo)
        {
            TextWrapper text = linkGo.AddComponent<TextWrapper>();
            text.alignment = TextBlock.textGenerationSettings.textAnchor;
            text.color = TextBlock.textGenerationSettings.color;
            text.font = TextBlock.textGenerationSettings.font;
            text.fontSize = TextBlock.textGenerationSettings.fontSize;
            text.fontStyle = TextBlock.textGenerationSettings.fontStyle;
            text.lineSpacing = TextBlock.textGenerationSettings.lineSpacing;
            text.supportRichText = TextBlock.textGenerationSettings.richText;
            return text;
        }

        public static Vector2 GetChatLinkContentSize(string content, Text text)
        {
            Vector2 size = TextBlock.GetTextContentSize(content);
            if(text != null)
            {
                if(size.x <= text.preferredWidth)
                {
                    if(size.x < text.preferredWidth)
                    {
                        size.x = Mathf.Ceil(text.preferredWidth);
                    }
                    else
                    {
                        size.x = Mathf.Floor(text.preferredWidth + 1);
                    }
                }
            }
            return size;
        }

    }
}
