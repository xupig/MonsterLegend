﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using GameLoader.Utils;
using ProtoBuf;
using UnityEngine;
using System.Text;

namespace GameLoader.Config
{
    public class LocalSetting
    {
        public static LocalSettingData settingData = new LocalSettingData();
        
        public static void Init()
        {
            LoadLocalSetting();
        }

        static void InitLocalSetting()
        {
            settingData.UserAccount = "";
            //settingData.SelectedCharacter = "";
            settingData.SelectedServerID = 0;
            settingData.SelectedAvatarDbid = 0;
        }

#if !UNITY_WEBPLAYER
        /**加载配置信息**/
        static void LoadConfig()
        {
            var localConfigPath = string.Concat(Application.persistentDataPath, ConstString.RutimeResource, ConstString.RutimeResourceConfig);
            if (!Directory.Exists(localConfigPath))
            {
                Directory.CreateDirectory(localConfigPath);
            }
            ConstString.LocalSettingPath = string.Concat(localConfigPath + "/LocalSettings.xml");
            ConstString.ServerListPath = string.Concat(localConfigPath + "/server.xml");
        }

        static void LoadLocalSetting()
        {
            LoadConfig();
            if (!File.Exists(ConstString.LocalSettingPath))
            {
                InitLocalSetting();
                Save(); 
            }
            else
            {
                try
                {
                    using (FileStream stream = File.OpenRead(ConstString.LocalSettingPath))
                    {
                        SerializableData.DataSerializer ds = new SerializableData.DataSerializer();
                        settingData = ds.Deserialize(stream, null, typeof(LocalSettingData)) as LocalSettingData;
                    }
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("本地文件：" + ConstString.LocalSettingPath + " 反序列化失败! reason:" + ex.StackTrace);
                    InitLocalSetting();
                    Save(); 
                }
            }
        }

        static public void Save()
        {
            if (!Directory.Exists(ConstString.LocalSettingPath.GetDirectoryName()))
            {
                Directory.CreateDirectory(ConstString.LocalSettingPath.GetDirectoryName());
            }
            if (File.Exists(ConstString.LocalSettingPath))
            {
                File.Delete(ConstString.LocalSettingPath);
            }
            using (FileStream stream = File.OpenWrite(ConstString.LocalSettingPath))
            {
                SerializableData.DataSerializer ds = new SerializableData.DataSerializer();
                ds.Serialize(stream, settingData);  
            }
        }
#else
        static string LOCAL_SETTING_KEY = "LocalSetting";
        static void LoadLocalSetting()
        {
            
            if (!PlayerPrefs.HasKey(LOCAL_SETTING_KEY))
            {
                Debug.LogError("!PlayerPrefs.HasKey(LOCAL_SETTING_KEY)::");
                InitLocalSetting();
                Save();
            }
            else
            {
                try
                {
                    string content = PlayerPrefs.GetString(LOCAL_SETTING_KEY);
                    content = WWW.UnEscapeURL(content);
                    byte[] contentBytes = Encoding.Unicode.GetBytes(content);
                    Stream stream = new MemoryStream(contentBytes);
                    SerializableData.DataSerializer ds = new SerializableData.DataSerializer();
                    settingData = ds.Deserialize(stream, null, typeof(LocalSettingData)) as LocalSettingData;
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("PlayerPrefs：LocalSetting 反序列化失败! reason:" + ex.Message);
                    InitLocalSetting();
                    Save();
                }
            }
        }

        static public void Save()
        {
            MemoryStream stream = new MemoryStream();
            SerializableData.DataSerializer ds = new SerializableData.DataSerializer();
            ds.Serialize(stream, settingData);
            byte[] contentBytes = new byte[stream.Length];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(contentBytes, 0, (int)stream.Length);
            string content = Encoding.Unicode.GetString(contentBytes);
            content = WWW.EscapeURL(content);
            PlayerPrefs.SetString(LOCAL_SETTING_KEY, content);
        }
#endif
    }
}
