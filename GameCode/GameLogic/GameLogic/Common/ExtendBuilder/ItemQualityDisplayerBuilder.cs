﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.Builder;
using UnityEngine;
using UnityEngine.UI;
using Common.ExtendComponent;

namespace Common.ExtendBuilder
{
    public class ItemQualityDisplayerBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "yanse";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
            ItemQualityDisplayer displayer = go.AddComponent<ItemQualityDisplayer>();
            string param = GetParam(go);
            if(string.IsNullOrEmpty(param) == false)
            {
                displayer.prefix = param;
            }
        }
    }
}
