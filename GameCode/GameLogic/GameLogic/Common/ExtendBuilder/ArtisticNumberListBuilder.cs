﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 16:38:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using Game.UI.Builder;
using UnityEngine;

namespace Common.ExtendBuilder
{
    public class ArtisticNumberListBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "meishuzi";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
            go.AddComponent<ArtisticNumberList>();
        }

    }
}
