﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.Builder;
using UnityEngine;
using UnityEngine.UI;
using Common.ExtendComponent;

namespace Common.ExtendBuilder
{
    public class ItemGridBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "wupin";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            go.AddComponent<ItemGrid>();
        }
    }
}
