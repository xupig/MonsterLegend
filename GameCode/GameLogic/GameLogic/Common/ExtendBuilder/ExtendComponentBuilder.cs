﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.Builder;

namespace Common.ExtendBuilder
{
    public class ExtendComponentBuilder
    {
        public static void Initialize()
        {
            ComponentBuilder.AddSpecificBuilder(new ItemQualityDisplayerBuilder());
            ComponentBuilder.AddSpecificBuilder(new ItemGridBuilder());
            ComponentBuilder.AddSpecificBuilder(new StateIconBuilder());
            ComponentBuilder.AddSpecificBuilder(new ArtisticNumberListBuilder());
            ComponentBuilder.AddSpecificBuilder(new TimerButtonBuilder());
            ComponentBuilder.AddSpecificBuilder(new CategoryListItemBuilder());
            ComponentBuilder.AddSpecificBuilder(new CategoryListBuilder());
            ComponentBuilder.AddSpecificBuilder(new ValidateInputFieldBuilder());
            ComponentBuilder.AddSpecificBuilder(new StretchFullScreenBuilder());
            ComponentBuilder.AddSpecificBuilder(new StretchHorizontalBuilder());
            ComponentBuilder.AddSpecificBuilder(new StretchVerticalBuilder());
        }
    }
}
