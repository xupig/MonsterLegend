﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/6 14:07:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using Game.UI.Builder;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendBuilder
{
    public class TimerButtonBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_BUTTON;
            }
        }

        public override string Name
        {
            get
            {
                return "TimerButton";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            go.AddComponent<KButtonStateFacade>();
            if (interactable == true)
            {
                go.AddComponent<TimerButton>();
            }
        }
    }
}
