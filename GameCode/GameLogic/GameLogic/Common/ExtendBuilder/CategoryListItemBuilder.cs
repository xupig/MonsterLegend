﻿#region 模块信息
/*==========================================
// 文件名：CategoryToggleItemBuildercs
// 命名空间: GameLogic.GameLogic.Common.ExtendBuilder
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/7 11:48:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.Builder;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendBuilder
{
    public class CategoryListItemBuilder : ContainerBuilder
    {
        public const string NAME = "categoryListItem";

        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return NAME;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
            go.AddComponent<CategoryListItem>();
        }
    }
}
