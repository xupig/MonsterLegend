﻿#region 模块信息
/*==========================================
// 文件名：CategoryToggleGroupBuilder
// 命名空间: GameLogic.GameLogic.Common.ExtendBuilder
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/7 15:28:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendBuilder
{
    public class CategoryListBuilder : ListBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_LIST;
            }
        }

        public override string Name
        {
            get
            {
                return "categoryList";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            CategoryList list = go.AddComponent<CategoryList>();
            list.itemBuilder = ComponentBuilder.typeBuilderDict[ComponentBuilder.TYPE_ITEM];
        }
    }
}
