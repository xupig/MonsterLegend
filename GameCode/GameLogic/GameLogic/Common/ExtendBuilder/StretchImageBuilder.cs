﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.Builder;
using UnityEngine;
using UnityEngine.UI;
using Common.ExtendComponent;
using GameMain.GlobalManager;

namespace Common.ExtendBuilder
{
    public abstract class StretchImageBuilder : ContainerBuilder
    {
        public override void Build(GameObject go, bool interactable)
        {
            base.Build(go, interactable);
            AddUIResizer(go);
        }

        protected abstract void AddUIResizer(GameObject go);
    }

    public class StretchFullScreenBuilder : StretchImageBuilder
    {
        public override string Name
        {
            get
            {
                return "fullScreen";
            }
        }

        protected override void AddUIResizer(GameObject go)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                UIImageResizer resizer = go.transform.GetChild(i).gameObject.AddComponent<UIImageResizer>();
                resizer.isHorizontal = true;
                resizer.isVertical = true;
            }
        }
    }

    public class StretchHorizontalBuilder : StretchImageBuilder
    {
        public override string Name
        {
            get
            {
                return "stretchHorizontal";
            }
        }

        protected override void AddUIResizer(GameObject go)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                UIImageResizer resizer = go.transform.GetChild(i).gameObject.AddComponent<UIImageResizer>();
                resizer.isHorizontal = true;
                resizer.isVertical = false;
            }
        }
    }

    public class StretchVerticalBuilder : StretchImageBuilder
    {
        public override string Name
        {
            get
            {
                return "StretchVertical";
            }
        }

        protected override void AddUIResizer(GameObject go)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                UIImageResizer resizer = go.transform.GetChild(i).gameObject.AddComponent<UIImageResizer>();
                resizer.isHorizontal = false;
                resizer.isVertical = true;
            }
        }
    }
}
