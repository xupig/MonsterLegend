﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using Game.UI.Builder;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendBuilder
{
    /// <summary>
    /// Build带Unicode字符集参数的文本输入框
    /// </summary>
    public class ValidateInputFieldBuilder : InputBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_INPUT;
            }
        }

        public override string Name
        {
            get
            {
                return "validateInput";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            base.Build(go, interactable);
            go.AddComponent<KInputFieldValidator>();
        }
    }
}
