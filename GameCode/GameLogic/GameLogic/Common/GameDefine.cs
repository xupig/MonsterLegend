﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    //用于定义游戏中的全局常量
    public class GameDefine
    {
        //游戏状态定义
        public static string STATE_LOGIN = "GAME_STATE_LOGIN";                     //登陆场景
        public static string STATE_CREATE_CHARACTER = "STATE_CREATE_CHARACTER";    //创建角色场景
        public static string STATE_CHOOSE_CHARACTER = "STATE_CHOOSE_CHARACTER";    //选择角色场景
        public static string STATE_LOADING = "STATE_LOADING";                      //loading状态
        public static string STATE_SCENE = "STATE_SCENE";                          //场景(包括主城 野外 战斗)
    }
}
