﻿using UnityEngine;
using System.Collections;
using D = GameLoader.Utils.LoggerHelper;
using LitJson;
using GameLoader.Utils;

namespace Common
{
    /// <summary>
    /// 玩家平台账号信息
    /// </summary>
    public class LoginInfo
    {
        private static ExtJsonVo extJsonVo;                              //ext串的对象封装

        public static bool isReturnEmptyAccount = false;                 //平台是否返回空账号[true:返回空账号,false:返回非空账号]
        public static string platformUid = string.Empty;                 //平台唯一id
        public static string platformAccount = string.Empty;             //平台账号
        public static string ext = string.Empty;                         //平台校验串
        public static string platformId_uid = string.Empty;              //平台ID和uid组合的串
        public static string platformId = string.Empty;                  //平台ID（数字)
        public static string platformName = string.Empty;                //平台名称
        public static string userName = string.Empty;                    //用户名(觉得这里等同于platformAccount)

        //IOS下,特有参数
        public static string sign;
        public static string token;
        public static string timestamp;
        public static string ip;
        public static string port;
        public static ExtJsonVo ExtJsonData
        {
            get { return extJsonVo; }
        }

        /// <summary>
        /// 存储平台账号信息
        /// </summary>
        /// <param name="platformUid">平台唯一id</param>
        /// <param name="userName">平台用户名</param>
        /// <param name="ext">平台校验串</param>
        /// <param name="platformName">平台名称</param>
        public static void InitWithParams(string platformUid, string userName, string ext, string platformName)
        {
            LoginInfo.platformId = "1";             //PC登录时用到，固定死
            LoginInfo.ext = ext;
            LoginInfo.platformUid = platformUid;
            LoginInfo.platformAccount = userName;
            LoginInfo.userName = userName;
            LoginInfo.platformName = platformName;
            ParserExt(ext);
        }
		
		public static void InitKoreaWithParams(string platformUid, string userName, string timestamp, string signStr)
        {
            LoginInfo.platformUid = platformUid;
            LoginInfo.platformAccount = userName;
            LoginInfo.userName = userName;
            if (LoginInfo.extJsonVo == null)
            {
                LoginInfo.extJsonVo = new ExtJsonVo();
            }
            LoginInfo.extJsonVo.timestamp = timestamp;
            LoginInfo.extJsonVo.signStr = signStr;
        }

        public static string[] GetPCStrList()
        {
            string[] strs = new string[6];
            strs[0] = userName;
            strs[1] = "timestamp";
            strs[2] = "strSign";
            strs[3] = "strPlatId";
            strs[4] = "0";
            strs[5] = "token";

            for (int i = 0; i < strs.Length; i++)
            {
                D.Debug(strs[i]);
            }
            return strs;
        }

        private static void ParserExt(string json)
        {
            if (string.IsNullOrEmpty(json)) return;
            extJsonVo = JsonMapper.ToObject<ExtJsonVo>(json);
            if (extJsonVo != null)
            {
                LoginInfo.platformId = extJsonVo.fnpid;
                platformId_uid = string.Concat(extJsonVo.fnpid, "_", platformUid);
                LoggerHelper.Info("timestamp:" + extJsonVo.timestamp);
                LoggerHelper.Info("signStr:" + extJsonVo.signStr);
                LoggerHelper.Info("username:" + extJsonVo.username);
                LoggerHelper.Info("fngid:" + extJsonVo.fngid);
                LoggerHelper.Info("fnpid:" + extJsonVo.fnpid);
                LoggerHelper.Info("suid:" + extJsonVo.suid);
                LoggerHelper.Info("verifyToken:" + extJsonVo.verifyToken);
                LoggerHelper.Info("targetServerId:" + extJsonVo.targetServerId);
                LoggerHelper.Info("platformId_uid:" + platformId_uid);
            }
        }

        /*public static string uid = string.Empty;
        public static string timestamp = string.Empty;
        public static string strSign = string.Empty;
        public static string strPlatId = string.Empty;
        public static string strPlatAccount = string.Empty;
        public static string token = string.Empty;
        public static string platName = string.Empty;
        
        public static string ip = string.Empty;
        public static string sessionId = string.Empty;
        public static string port = string.Empty;
        public static string p1 = string.Empty;
        public static string p2 = string.Empty;
        public static string p3 = string.Empty;
        public static string p4 = string.Empty;
        public static string p5 = string.Empty;
        public static string p6 = string.Empty;
        public static string p7 = string.Empty;
        public static string p8 = string.Empty;
        public static string p9 = string.Empty;
        public static string p10 = string.Empty;
        
        public static string username
        {
            get
            {
                return m_Username;
            }
            set
            {
                m_Username = value;
            }
        }
        static string m_Username = string.Empty;

        public static void InitWithParams(string strUID, 
                                          string strTimeStamp,
                                          string strSignCode,
                                          string strPlatID,
                                          string strPlatformAcount, 
                                          string strToken,
                                          string strPlatName, 
                                          string strUserName,
                                          string ipx = "", string portx = "", string sid = "",
                                          string xp1 = "", string xp2 = "", string xp3 = "", string xp4 = "", string xp5 = "",
                                          string xp6 = "", string xp7 = "", string xp8 = "", string xp9 = "", string xp10 = "")
        {
            uid = strUID;
            timestamp = strTimeStamp;
            strSign = strSignCode;
            strPlatId = strPlatID;
            strPlatAccount = strPlatformAcount;
            token = strToken;
            platName = strPlatName;
            m_Username = strUserName;
            ip = ipx;
            port = portx;
            sessionId = sid;
            p1 = xp1;
            p2 = xp2;
            p3 = xp3;
            p4 = xp4;
            p5 = xp5;
            p6 = xp6;
            p7 = xp7;
            p8 = xp8;
            p9 = xp9;
            p10 = xp10;
        }

        public static string[] GetStrList()
        {
            string[] strs = new string[6];
            strs[0] = uid;
            strs[1] = timestamp;
            strs[2] = strSign;
            strs[3] = strPlatId;
            strs[4] = strPlatAccount;
            strs[5] = token;

            for (int i = 0; i < strs.Length; i++)
            {
                D.Debug(strs[i]);
            }
            return strs;
        }

        public static string[] GetPCStrList()
        {
            string[] strs = new string[6];
            strs[0] = m_Username;
            strs[1] = "timestamp";
            strs[2] = "strSign";
            strs[3] = "strPlatId";
            strs[4] = "0";
            strs[5] = "token";

            for (int i = 0; i < strs.Length; i++)
            {
                D.Debug(strs[i]);
            }
            return strs;
        }*/
    }


    /// <summary>
    /// 平台返回的Ext串的对象封装
    /// </summary>
    public class ExtJsonVo
    {
        public ExtJsonVo() { }

        public string timestamp { get; set; }
        public string signStr { get; set; }
        public string username { get; set; }
        public string fngid { get; set; }
        public string fnpid { get; set; }

        public string suid { get; set; }
        public string verifyToken { get; set; }
        public string targetServerId { get; set; }
    }

}