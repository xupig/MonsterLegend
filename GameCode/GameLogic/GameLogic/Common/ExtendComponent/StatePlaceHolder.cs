﻿#region 模块信息
/*==========================================
// 文件名：StatePlaceHolder
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 16:10:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class StatePlaceHolder : KContainer, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private GameObject _normalGo;
        private GameObject _overGo;
        private GameObject _downGo;

        public KComponentEvent<StatePlaceHolder, PointerEventData> onPointerUp = new KComponentEvent<StatePlaceHolder, PointerEventData>();
        public KComponentEvent<StatePlaceHolder, PointerEventData> onPointerDown = new KComponentEvent<StatePlaceHolder, PointerEventData>();
        public KComponentEvent<StatePlaceHolder, PointerEventData> onPointerEnter = new KComponentEvent<StatePlaceHolder, PointerEventData>();
        public KComponentEvent<StatePlaceHolder, PointerEventData> onPointerExit = new KComponentEvent<StatePlaceHolder, PointerEventData>();
        public KComponentEvent onClick = new KComponentEvent();

        private int _currentIcon = -1;

        protected override void Awake()
        {
            _normalGo = GetChild("Container_normal");
            _overGo = GetChild("Container_over");
            _downGo = GetChild("Container_down");
            SetState(KButtonState.NORMAL);
            base.Awake();
        }

        public void SetIcon(int iconId)
        {
            if (_currentIcon!=iconId)
            {
                _currentIcon = iconId;
                if (_normalGo != null)
                {
                    MogoAtlasUtils.AddIcon(_normalGo, iconId);
                }
                if (_overGo != null)
                {
                    MogoAtlasUtils.AddIcon(_overGo, iconId);
                }
                if (_downGo != null)
                {
                    MogoAtlasUtils.AddIcon(_downGo, iconId);
                }
            }
        }

        public void OnPointerDown(PointerEventData evtData)
        {
            SetState(KButtonState.DOWN);
            onPointerDown.Invoke(this, evtData);
        }

        public void OnPointerUp(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            onPointerUp.Invoke(this, evtData);
            if (evtData.dragging==false)
            {
                onClick.Invoke();
            }
        }

        public void OnPointerEnter(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            onPointerExit.Invoke(this, evtData);
        }

        public void OnPointerExit(PointerEventData evtData)
        {
            SetState(KButtonState.NORMAL);
            onPointerExit.Invoke(this, evtData);
        }

        public virtual void SetState(string state)
        {
            switch(state)
            {
                case KButtonState.NORMAL:
                    if (_normalGo != null)
                    {
                        _normalGo.SetActive(true);
                        if (_overGo != null)
                        {
                            _overGo.SetActive(false);
                        }
                        if (_downGo != null)
                        {
                            _downGo.SetActive(false);
                        }
                    }
                    break;
                case KButtonState.OVER:
                    if (_overGo != null)
                    {
                        _overGo.SetActive(true);
                        if (_normalGo != null)
                        {
                            _normalGo.SetActive(false);
                        }
                        if (_downGo != null)
                        {
                            _downGo.SetActive(false);
                        }
                    }
                    break;
                case KButtonState.DOWN:
                    if (_downGo != null)
                    {
                        _downGo.SetActive(true);
                        if (_normalGo != null)
                        {
                            _normalGo.SetActive(false);
                        }
                        if (_overGo != null)
                        {
                            _overGo.SetActive(false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
