﻿using Common.Global;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class Countdown : KContainer
    {
        private float _endTime;
        private bool isCountdowning = false;

        private string _template = string.Format("{0}{1}",
            ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, "{0}"),
            ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_WHITE, "秒后自动退出"));

        private StateText _textLeftSecond;

        public KComponentEvent onEnd = new KComponentEvent();

        protected override void Awake()
        {
            InvokeRepeating("OnTick", 1, 0.5f);
        }

        protected override void OnDisable()
        {
            StopCountdown();
        }

        public void SetTemplate(string template)
        {
            _template = template;
        }

        public void StartCountdown(uint second, StateText text)
        {
            isCountdowning = true;
            _textLeftSecond = text;
            //+0.2f 是为了确保第一时间能显示second的值
            _endTime = Time.realtimeSinceStartup + second + 0.2f;
            OnTick();
        }

        private void OnTick()
        {
            if (isCountdowning)
            {
                int leftSecond = (int)(_endTime - Time.realtimeSinceStartup);
                if (leftSecond <= 0)
                {
                    OnEnd();
                }
                else
                {
                    _textLeftSecond.CurrentText.text = string.Format(_template, leftSecond);
                }
            }
        }

        private void OnEnd()
        {
            StopCountdown();
            onEnd.Invoke();
        }

        public void StopCountdown()
        {
            isCountdowning = false;
            if (_textLeftSecond != null)
            {
                _textLeftSecond.Clear();
            }
            _textLeftSecond = null;
        }
    }
}
