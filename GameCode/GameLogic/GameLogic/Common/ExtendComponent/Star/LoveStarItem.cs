﻿#region 模块信息
/*==========================================
// 文件名：IntimateItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/16 21:11:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent
{
    public class LoveStarItem : KContainer
    {
        private StateImage[] _imgList;

        protected override void Awake()
        {
            _imgList = new StateImage[3];
            for (int i = 0; i < 3;i++ )
            {
                _imgList[i] = GetChildComponent<StateImage>("Image_xinxin"+i);
            }
            base.Awake();
        }

        public void SetState(int state)
        {
            for (int i = 0; i < 3; i++)
            {
                _imgList[i].gameObject.SetActive(false);
            }
            _imgList[state].gameObject.SetActive(true);
            if (state == LoveStarItemList.HALF)
            {
                _imgList[LoveStarItemList.EMPTY].gameObject.SetActive(true);
            }
        }
    }
}
