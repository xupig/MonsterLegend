﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 14:30:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class StarList : KContainer
    {
        private List<StateImage> _imageList;
        public List<StateImage> ImageList
        {
            get
            {
                return _imageList;
            }
        }

        private List<Star> _starList;
        public List<Star> ContainerStarList
        {
            get
            {
                return _starList;
            }
        }
        /// <summary>
        /// 初始化星星信息
        /// psd格式为
        /// Container_**
        ///     image_(参数name)0
        ///     image_(参数name)1
        ///     image_(参数name)2
        ///     ...
        /// </summary>
        /// <param name="num"></param>
        /// <param name="name"></param>
        public void SetImageData(int totalNum, string name)
        {
            _imageList = new List<StateImage>();
            for (int i = 0; i < totalNum; i++)
            {
                _imageList.Add(GetChildComponent<StateImage>(string.Format("{0}{1}", name, i)));
            }
        }

        public void SetImageNum(int num)
        {
            for (int i = 0; i < _imageList.Count; i++)
            {
                if (i < num)
                {
                    _imageList[i].State = KButtonState.NORMAL;
                }
                else
                {
                    _imageList[i].State = KButtonState.DISABLE;
                }
            }
        }

        /// <summary>
        /// 初始化星星信息
        /// psd格式为
        /// Container_**
        ///     Container_(参数name)0
        ///         brightStarName
        ///     Container_(参数name)1
        ///         brightStarName
        ///     Container_(参数name)2
        ///         brightStarName
        ///     ...
        /// </summary>
        /// <param name="num"></param>
        /// <param name="name"></param>
        /// <param name="brightStarName"></param>
        public void SetStarData(int totalNum, string name, string brightStarName)
        {
            _starList = new List<Star>();
            for (int i = 0; i < totalNum; i++)
            {
                int index = i;
                Star star = AddChildComponent<Star>(string.Format("{0}{1}", name, index));
                star.SetBrightStarName(brightStarName);
                _starList.Add(star);
            }
        }

        public void SetStarNum(int num)
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                if (i < num)
                {
                    _starList[i].ShowBrightStar();
                }
                else
                {
                    _starList[i].HideBrightStar();
                }
            }
        }

        public Vector3 GetStarPosition(int star)
        {
            return _starList[star - 1].transform.localPosition;
        }
    }
}
