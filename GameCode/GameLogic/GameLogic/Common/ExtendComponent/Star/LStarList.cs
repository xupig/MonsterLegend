﻿
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class LStarList : KContainer
    {
        private enum StarType
        {
            lStar,//LStarList的一级目录是Container
            image,//LStarList的一级目录是Image
        }

        private StarType _type;

        private List<LStar> _lStarList;
        private List<StateImage> _imageStarList;

        protected override void Awake()
        {
            InitStarType();
            switch (_type)
            {
                case StarType.lStar:
                    InitLStarList();
                    break;
                case StarType.image:
                    InitImageStarList();
                    break;
            }
        }

        private void InitStarType()
        {
            string name = transform.GetChild(0).name;
            if (name.StartsWith("Container"))
            {
                _type = StarType.lStar;

            }
            else if (name.StartsWith("Image"))
            {
                _type = StarType.image;
            }
        }

        private void InitLStarList()
        {
            _lStarList = new List<LStar>();
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject child = GetChild(i).gameObject;
                _lStarList.Add(child.AddComponent<LStar>());
            }
        }

        private void InitImageStarList()
        {
            _imageStarList = new List<StateImage>();
            for (int i = 0; i < transform.childCount; i++)
            {
                StateImage image = GetChild(i).GetComponent<StateImage>();
                _imageStarList.Add(image);
            }
        }

        public void SetNum(int num, bool useAlpha = true)
        {
            switch(_type)
            {
                case StarType.lStar:
                    LStarSetNum(num, useAlpha);
                    break;
                case StarType.image:
                    StarImageSetNum(num, useAlpha);
                    break;
            }
        }

        private void LStarSetNum(int num, bool useAlpha)
        {
            num = Mathf.Clamp(num, 0, _lStarList.Count);
            for (int i = 0; i < _lStarList.Count; i++)
            {
                _lStarList[i].SetVisible(i < num, useAlpha);
            }
        }

        private void StarImageSetNum(int num, bool useAlpha)
        {
            for (int i = 0; i < _imageStarList.Count; i++)
            {
                StateImage image = _imageStarList[i];
                if (useAlpha)
                {
                    image.Visible = true;
                    if (image.StateCount > 1)
                    {
                        image.State = i < num ? KButtonState.NORMAL : KButtonState.DISABLE;
                    }
                    else
                    {
                        image.Alpha = i < num ? 1f : 0f;
                    }
                }
                else
                {
                    image.Visible = i < num ? true : false;
                }
            }
        }

        private class LStar : KContainer
        {
            private StateImage _imageBrightStar;

            protected override void Awake()
            {
                _imageBrightStar = GetChild(transform.childCount - 1).GetComponent<StateImage>();
            }

            public void SetVisible(bool visible, bool useAlpha)
            {
                if (useAlpha)
                {
                    _imageBrightStar.Visible = true;
                    _imageBrightStar.Alpha = visible == true ? 1 : 0;

                }
                else
                {
                    _imageBrightStar.Visible = visible;
                }
            }
        }
    }
}
