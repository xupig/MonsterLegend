﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 14:29:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class Star : KContainer
    {
        private GameObject _goBrightStar;

        public void SetBrightStarName(string brightStarName)
        {
            _goBrightStar = GetChild(brightStarName);
        }

        public void ShowBrightStar()
        {
            _goBrightStar.SetActive(true);
        }

        public void HideBrightStar()
        {
            _goBrightStar.SetActive(false);
        }
    }
}
