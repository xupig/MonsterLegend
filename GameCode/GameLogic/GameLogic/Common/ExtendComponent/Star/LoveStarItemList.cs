﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/25 15:15:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;

namespace Common.ExtendComponent
{
    public class LoveStarItemList:KContainer
    {
        private const int STAR_MAX = 5;

        public const int FULL = 2;
        public const int HALF = 1;
        public const int EMPTY = 0;

        private LoveStarItem[] _starItemList;


        protected override void Awake()
        {
            _starItemList = new LoveStarItem[STAR_MAX];
            for (int i = 0; i < _starItemList.Length; i++)
            {
                _starItemList[i] = AddChildComponent<LoveStarItem>("Container_xin" + i);
            }
        }

        public void SetLevel(float level)
        {
            for (int i = 0; i < _starItemList.Length; i++)
            {
                if (level - i >= 1)
                {
                    _starItemList[i].SetState(FULL);
                }
                else if (level - i == 0.5f)
                {
                    _starItemList[i].SetState(HALF);
                }
                else
                {
                    _starItemList[i].SetState(EMPTY);
                }
            }
        }
    }
}
