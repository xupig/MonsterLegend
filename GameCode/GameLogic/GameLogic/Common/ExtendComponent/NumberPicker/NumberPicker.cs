﻿#region 模块信息
/*==========================================
// 文件名：NumberPicker
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.NumPicker
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/2/25 15:23:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleCommonUI
{
    public class PickerItem : KList.KListItemBase
    {
        private int _value;
        private StateText _text;

        public override object Data
        {
            get
            {
                return _value;
            }
            set
            {
                if (value is int)
                {
                    _value = (int)value;
                    Refresh();
                }
                else
                {
                    _text.CurrentText.text = string.Empty;
                }
            }
        }

        private void Refresh()
        {
            _text.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, _value.ToString());
        }

        protected override void Awake()
        {
            _text = GetChildComponent<StateText>("Label_txtLevel");
        }

        public override void Dispose()
        {

        }
    }

    public class NumberPicker : KContainer
    {
        private KPageableList _numOneList;
        private KScrollView _scrollView; // 滚动
        private RectTransform listRect;
        private int _currentValue = 0;

        private int itemHeight;
        private float markPosition;
        private List<int> valueList;

        protected override void Awake()
        {
            _numOneList = GetChildComponent<KPageableList>("mask/content");
            itemHeight = Mathf.CeilToInt(GetChildComponent<RectTransform>("mask/content/item").rect.height);
            markPosition = (GetChildComponent<RectTransform>("mask").rect.height - itemHeight) * 0.5f;
            listRect = _numOneList.GetComponent<RectTransform>();
            _numOneList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _scrollView = GetComponent<KScrollView>();
        }

        private bool needAdjustValue = false;
        void Update()
        {
            if (Mathf.Abs(_scrollView.ScrollRect.velocity.y) > Mathf.Epsilon)
            {
                needAdjustValue = true;
            }
            else if (Mathf.Abs(_scrollView.ScrollRect.velocity.y) < Mathf.Epsilon && needAdjustValue == true)
            {
                _scrollView.ScrollRect.StopMovement();
                SetListPosition();
                needAdjustValue = false;
            }
        }

        private void SetListPosition()
        {
            int listMoveDistance = Mathf.RoundToInt(listRect.anchoredPosition.y);
            int moveDistance = listMoveDistance % itemHeight;
            if (moveDistance > itemHeight / 2)
            {
                moveDistance = itemHeight - moveDistance;
            }
            else
            {
                moveDistance = -moveDistance;
            }
            TweenPosition tween = TweenPosition.Begin(_numOneList.gameObject, 0.05f, new Vector2(listRect.anchoredPosition.x, listRect.anchoredPosition.y + moveDistance));
        }

        public void InitPicker(List<int> numberList)
        {
            valueList = numberList;
            _numOneList.SetDataList<PickerItem>(numberList);
            listRect.sizeDelta = new Vector2(listRect.rect.width, numberList.Count * itemHeight + 2 * markPosition);
            for (int i = 0; i < numberList.Count; i++)
            {
                _numOneList.ItemList[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -(itemHeight * i + markPosition));
            }
        }

        public void InitPicker(int minNumber, int maxNumber)
        {
            List<int> list = new List<int>();
            for (int i = Mathf.Min(minNumber,maxNumber); i <= Mathf.Max(minNumber,maxNumber); i++)
            {
                list.Add(i);
            }
            InitPicker(list);
        }

        public int CurrentValue
        {
            get
            {
                int listMoveDistance = Mathf.RoundToInt(listRect.anchoredPosition.y);
                int itemIndex = listMoveDistance / itemHeight;
                _currentValue = (int)_numOneList.ItemList[itemIndex].Data;
                return _currentValue;
            }
            set
            {
                if (valueList != null && valueList.Contains(value))
                {
                    int index = valueList.IndexOf(value);
                    _numOneList.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, index * itemHeight);
                }
            }
        }
    }
}