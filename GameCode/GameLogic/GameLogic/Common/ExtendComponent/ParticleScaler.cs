﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class ParticleScaler : MonoBehaviour
    {
        private float _scale = 1;

        private void RecalculateParticleScale(float scale)
        {
            ParticleSystem particleSystem = GetComponent<ParticleSystem>();
            if (particleSystem != null)
            {
                particleSystem.startSize = particleSystem.startSize / _scale * scale;
            }
        }

        void OnWillRenderObject()
        {
            float scale = transform.lossyScale.x;
            if (_scale != scale)
            {
                RecalculateParticleScale(scale);
                _scale = scale;
            }
        }
    }
}
