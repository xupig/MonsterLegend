﻿#region 模块信息
/*==========================================
// 文件名：KMoney
// 命名空间: Assets.Plugins.UIComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/3 10:41:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;
using UnityEngine.Events;

namespace Common.ExtendComponent
{
    public class MoneyItem : KContainer
    {
        private IconContainer _iconContainer;
        private StateText _txtGold;
        private KButton _btnAdd;
        private KDummyButton _btn;

        private BaseItemData _needItem = null;
        private int _id = -1;
        private bool _hasAddListener = false;
        public UnityAction onAddClick = null;

        protected override void Awake()
        {
            CreateChildren();
        }

        public void HideBtn()
        {
            _btnAdd.Visible = false;
            _btn.onClick.RemoveListener(OnClick);
        }

        private void CreateChildren()
        {
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _btn = _iconContainer.gameObject.AddComponent<KDummyButton>();
            _txtGold = GetChildComponent<StateText>("Label_txtGold");
            _txtGold.CurrentText.alignment = TextAnchor.UpperRight;
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (_btnAdd != null)
            {
                _btnAdd.onClick.AddListener(OnClickAdd);
            }
            _btn.onClick.AddListener(OnClick);
            RefreshText();
            if (_hasAddListener == false)
            {
                AddListener();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (_btnAdd != null)
            {
                _btnAdd.onClick.RemoveListener(OnClickAdd);
            }
            _btn.onClick.RemoveListener(OnClick);
            RemoveListener();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();        }

        public void SetMoneyType(int moneyId, BaseItemData needItem = null)
        {
            if (_id != moneyId || _needItem != needItem)
            {
                _needItem = needItem;
                RemoveListener();
                _id = moneyId;
                _iconContainer.SetIcon( item_helper.GetIcon(moneyId));
                RefreshText();
                AddListener();
            }
        }

        private void RefreshText()
        {
            if (_id <= public_config.MONEY_TYPE_MAX_ID)
            {
                RefreshMoneyNum();
            }
            else
            {
                RefreshItemNum();
            }
        }

        private void RefreshMoneyNum()
        {
            int moneyCount = item_helper.GetMyMoneyCountByItemId(_id);
            if (_needItem != null && moneyCount < _needItem.StackCount)
            {
                _txtGold.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            }
            else
            {
                _txtGold.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
            }
            _txtGold.CurrentText.text = item_helper.GetMyMoneyCountByItemId(_id).ToString();
        }

        private void RefreshItemNum()
        {
            int itemCount = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_id);
            if (_needItem != null && itemCount < _needItem.StackCount)
            {
                _txtGold.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            }
            else
            {
                _txtGold.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
            }
            _txtGold.CurrentText.text = itemCount.ToString();
        }

        private void OnBagItemChange(BagType type, uint pos)
        {
            if (type == BagType.ItemBag)
            {
                RefreshItemNum();
            }
        }

        /// <summary>
        /// 该接口需要外部调用RefreshContent来刷新数据
        /// </summary>
        /// <param name="iconId"></param>
        public void SetIcon(int iconId)
        {
            _iconContainer.SetIcon(iconId);
        }

        public void RefreshContent(int value)
        {
            _txtGold.CurrentText.text = value.ToString();
        }

        public void SetNumTxtColor(Color color)
        {
            _txtGold.CurrentText.color = color;
        }

        private void OnClickAdd()
        {
            if (onAddClick == null)
            {
                if (_id == public_config.MONEY_TYPE_DIAMOND)
                {
                    if (function_helper.IsFunctionOpen(FunctionId.charge))
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                    }
                    else
                    {
                  //ari MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(56749));
                    }
                }
                else if (_id == public_config.MONEY_TYPE_ENERGY)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.EnergyBuying);
                }
                else if (_id == public_config.MONEY_TYPE_GUILD_CONTRIBUTION)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.GuildPop, 1);
                }
                else if (_id == public_config.MONEY_TYPE_GUILD_FUND)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.GuildPop, 1);
                }
                else
                {
                    if (item_channel_helper.ContainId(_id) == false)
                    {
                  //ari MessageBox.Show(true, "", "该物品暂时没有获取渠道！");
                        return;
                    }
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, _id);
                }
            }
            else
            {
                onAddClick.Invoke();
            }
        }

        private void OnClick()
        {
            if (_id != 0)
            {
               //ari ToolTipsManager.Instance.ShowItemTip(_id, PanelIdEnum.Empty);
            }
        }

        private void AddListener()
        {
            if (_id == -1) return;
            switch (_id)
            {
                case public_config.MONEY_TYPE_GOLD:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_DIAMOND:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_bind_diamond, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_HONOUR:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.honour, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_ENERGY:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_POWER:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.power, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_RUNE_STONE:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_rune_stone, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_GUILD_CONTRIBUTION:
                    EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_GUILD_FUND:
                    EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_STAR_SPIRIT:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.star_spirit_num, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_FORTRESS_STONE:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fortress_stone, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_FORTRESS_WOOD:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fortress_wood, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_ACTIVE_POINT:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.active_point, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_TUTOR_VALUE:
                    EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_value, RefreshMoneyNum);
                    break;
                default:
                    EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagItemChange);
                    break;
            }
            _hasAddListener = true;
        }

        private void RemoveListener()
        {
            if (_id == -1) return;
            switch (_id)
            {
                case public_config.MONEY_TYPE_GOLD:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_DIAMOND:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_bind_diamond, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_HONOUR:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.honour, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_ENERGY:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_POWER:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.power, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_RUNE_STONE:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_rune_stone, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_GUILD_CONTRIBUTION:
                    EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_GUILD_FUND:
                    EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_STAR_SPIRIT:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.star_spirit_num, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_FORTRESS_STONE:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.fortress_stone, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_FORTRESS_WOOD:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.fortress_wood, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_ACTIVE_POINT:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.active_point, RefreshMoneyNum);
                    break;
                case public_config.MONEY_TYPE_TUTOR_VALUE:
                    EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_value, RefreshMoneyNum);
                    break;
                default:
                    EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnBagItemChange);
                    break;
            }
            _hasAddListener = false;
        }
    }
}
