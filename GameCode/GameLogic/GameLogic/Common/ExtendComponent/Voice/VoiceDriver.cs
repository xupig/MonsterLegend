﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

namespace Common.ExtendComponent
{
    /// <summary>
    /// 平台语音SDK回调unity时的接口类
    /// </summary>
    public class VoiceDriver : MonoBehaviour
    {
        //开始录音
        private void StartTalk()
        {
            EventDispatcher.TriggerEvent(ChatEvents.SDK_START_TALK);
        }

        //语音转文字
        private void VoiceMessage(string str)
        {
            EventDispatcher.TriggerEvent<string>(ChatEvents.SDK_VOICE_MESSAGE, str);
        }

        //录音音量变化
        private void VoiceVolume(string strVoiceVolume)
        {
            int volume = int.Parse(strVoiceVolume);
            EventDispatcher.TriggerEvent<int>(ChatEvents.SDK_VOICE_VOLUME, volume);
        }

        //录音结束
        private void EndTalk(string str)
        {
            //str = example: audioPath + [duration=audioDuration]
            string strAudioDurationTag = "[duration=";
            int subIndex = str.IndexOf(strAudioDurationTag);
            string strDuration = str.Substring(subIndex + strAudioDurationTag.Length, str.Length - (subIndex + strAudioDurationTag.Length));
            strDuration = strDuration.Substring(0, strDuration.Length - 1);
            int duration = 0;
            int.TryParse(strDuration, out duration);
            string path = str.Substring(0, subIndex);
            EventDispatcher.TriggerEvent<string, int>(ChatEvents.SDK_END_TALK, path, duration);
        }

        private void PlayVoice(string path)
        {
            EventDispatcher.TriggerEvent<string>(ChatEvents.SDK_START_PLAY_VOICE, path);
        }

        //发生错误
        private void TalkError(string errorId)
        {
            EventDispatcher.TriggerEvent<string>(ChatEvents.SDK_TALK_ERROR, errorId);
        }

    }
}
