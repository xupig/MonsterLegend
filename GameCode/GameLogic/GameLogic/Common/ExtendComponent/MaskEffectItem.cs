﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using GameLoader.Utils;

namespace Common.ExtendComponent
{
    public class MaskEffectItem : MaskObjectItemBase
    {
        public delegate bool CheckCanShowEffectCallback();

        private KParticle _effect;

        private CheckCanShowEffectCallback _canShowEffectCallback;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="attachEffectGo">设置挂载特效的父节点gameObject</param>
        /// <param name="callback">设置检查是否可显示特效的回调</param>
        /// <param name="processMaskImmediately">是否立刻处理一次遮罩特效</param>
        public void Init(GameObject attachEffectGo, CheckCanShowEffectCallback callback = null, bool processMaskImmediately = false)
        {
            SetAttachEffectGo(attachEffectGo);
            SetCanShowEffectCallback(callback);
            if (processMaskImmediately)
            {
                ControlMaskObject();
            }
        }

        /// <summary>
        /// 更新处理一次遮罩特效
        /// </summary>
        //public void UpdateMaskEffect()
        //{
        //    ControlMaskEffect();
        //}

        //设置挂载特效的父节点gameObject
        private void SetAttachEffectGo(GameObject attachEffectGo)
        {
            SetAttachObjectGo(attachEffectGo);
            InitEffect();
        }

        //设置检查是否可显示特效的回调
        private void SetCanShowEffectCallback(CheckCanShowEffectCallback callback)
        {
            _canShowEffectCallback = callback;
        }

        private void InitEffect()
        {
            //如果用_attachEffectGo.GetComponentInChildren<KParticle>()获取KParticle组件，可能在挂有该组件的gameObject为隐藏并且该组件
            //的enable为false时，会找不到，因此换另一种方法查找
            for (int i = 0; i < _attachObjectGo.transform.childCount; i++)
            {
                Transform child = _attachObjectGo.transform.GetChild(i);
                KParticle effect = child.GetComponent<KParticle>();
                if (effect != null)
                {
                    _effect = effect;
                    break;
                }
            }
            if (_effect == null)
            {
                LoggerHelper.Error("Can not find KParticle component go at attach effect game object's children !!!");
            }
        }

        //private void AddEventListener()
        //{
        //    if (_scrollView != null)
        //    {
        //        _scrollView.ScrollRect.onValueChanged.AddListener(OnScrollRectMove);
        //    }
        //}

        //private void RemoveEventListener()
        //{
        //    if (_scrollView != null)
        //    {
        //        _scrollView.ScrollRect.onValueChanged.RemoveListener(OnScrollRectMove);
        //    }
        //}

        protected override void OnDestroy()
        {
            if (_canShowEffectCallback != null)
            {
                _canShowEffectCallback = null;
            }
            base.OnDestroy();
        }

        protected override bool CheckGameObjectValid()
        {
            if (!base.CheckGameObjectValid())
            {
                return false;
            }
            if (_effect == null)
            {
                LoggerHelper.Error("Can not find KParticle component go at attach effect game object's children !!!");
                return false;
            }
            return true;
        }

        protected override void ProcessInRect()
        {
            if (_canShowEffectCallback == null)
            {
                ShowEffect();
            }
            else if (_canShowEffectCallback != null && _canShowEffectCallback() == true)
            {
                ShowEffect();
            }
            else
            {
                HideEffect();
            }
        }

        protected override void ProcessOutRect()
        {
            HideEffect();
        }

        private void ShowEffect()
        {
            if (_effect != null && _effect.Visible == false)
            {
                _effect.Visible = true;
            }
        }

        private void HideEffect()
        {
            if (_effect != null && _effect.Visible == true)
            {
                _effect.Visible = false;
            }
        }
    }
}
