﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.ServerConfig;

namespace Common.ExtendComponent
{
    public class KInputFieldValidator : MonoBehaviour
    {
        private KInputField _inputField;

        protected void Awake()
        {
            _inputField = GetComponent<KInputField>();
        }

        protected void OnGUI()
        {
            if(EventSystem.current.currentSelectedGameObject != this.gameObject)
            {
                return;
            }
            if(Event.GetEventCount() <= 0)
            {
                return;
            }
            if(Event.current == null)
            {
                return;
            }
            if(Event.current.rawType != EventType.keyDown)
            {
                return;
            }
            if(IsFilteredChar(Event.current.character) == false)
            {
                return;
            }
            if(IsValidChar(Event.current.character) == false)
            {
                //含有不能显示字符提示
                //ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74888), PanelIdEnum.MainUIField);
            }
        }


        private bool IsFilteredChar(char c)
        {
            if(char.IsLetterOrDigit(c) || char.IsPunctuation(c))
            {
                return true;
            }
            return false;
        }

        private bool IsValidChar(char c)
        {
            return _inputField.textComponent.font.HasCharacter(c);
        }
    }
}
