﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

namespace Common.ExtendComponent
{
    public class HalfScreenInputDriver : MonoBehaviour
    {
        //软键盘在输入过程中
        private void InputMessage(string str)
        {
            EventDispatcher.TriggerEvent<string>(InputEvents.SDK_INPUT_MESSAGE, str);
        }

        //软键盘输入完毕
        private void CommitInput(string str)
        {
            EventDispatcher.TriggerEvent<string>(InputEvents.SDK_COMMIT_INPUT, str);
        }
        
        //屏幕点击
        private void OnInputClick(string xy)
        {
            EventDispatcher.TriggerEvent<string>(InputEvents.SDK_ON_INPUT_CLICK, xy);
        }

        //取消输入
        private void CanelInput()
        {
            EventDispatcher.TriggerEvent(InputEvents.SDK_CANCEL_INPUT);
        }
    }
}
