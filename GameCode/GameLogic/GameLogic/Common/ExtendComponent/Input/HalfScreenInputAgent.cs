﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using GameLoader.Utils;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;

namespace Common.ExtendComponent
{
    public class HalfScreenInputAgent : MonoBehaviour
    {
        //考虑是否在KInput组件中增加一个id标识是哪个控件
        private KInputField _inputField;

        protected void Awake()
        {
            _inputField = transform.GetComponent<KInputField>();
            if (_inputField == null)
            {
                LoggerHelper.Error("Can not find KInputField Component in this game object !!!");
                return;
            }
            _inputField.IMECamera = UIManager.Instance.UICamera;
            _inputField.isEnableHalfScreenInput = true;
        }

        protected void OnEnable()
        {
            AddEventListener();
        }

        protected void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<string>(InputEvents.SDK_INPUT_MESSAGE, OnInputMessage);
            EventDispatcher.AddEventListener<string>(InputEvents.SDK_COMMIT_INPUT, OnCommitInput);
            EventDispatcher.AddEventListener<string>(InputEvents.SDK_ON_INPUT_CLICK, OnClickInput);
            EventDispatcher.AddEventListener(InputEvents.SDK_CANCEL_INPUT, OnCancelInput);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<string>(InputEvents.SDK_INPUT_MESSAGE, OnInputMessage);
            EventDispatcher.RemoveEventListener<string>(InputEvents.SDK_COMMIT_INPUT, OnCommitInput);
            EventDispatcher.RemoveEventListener<string>(InputEvents.SDK_ON_INPUT_CLICK, OnClickInput);
            EventDispatcher.RemoveEventListener(InputEvents.SDK_CANCEL_INPUT, OnCancelInput);
        }

        private void OnInputMessage(string str)
        {
            _inputField.text = str;
        }

        private void OnCommitInput(string str)
        {
            _inputField.text = str;
        }

        private void OnClickInput(string xy)
        {

        }

        private void OnCancelInput()
        {

        }

        protected void OnDestroy()
        {
            if (_inputField != null)
            {
                _inputField = null;
            }
        }
    }
}
