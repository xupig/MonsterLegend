﻿using Game.UI.UIComponent;

namespace Common.ExtendComponent
{
    public class RankBackgroundContainer : KContainer
    {
        private StateImage _bgImage1;
        private StateImage _bgImage2;

        protected override void Awake()
        {
            _bgImage1 = GetChildComponent<StateImage>("ScaleImage_sharedGridBg01");
            _bgImage2 = GetChildComponent<StateImage>("ScaleImage_sharedGridBg02");
        }

        public void RefreshBackground(int rank)
        {
            _bgImage1.gameObject.SetActive(rank % 2 == 1);
            _bgImage2.gameObject.SetActive(rank % 2 == 0);
        }
    }

}
