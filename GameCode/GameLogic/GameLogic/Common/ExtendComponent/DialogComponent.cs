﻿#region 模块信息
/*==========================================
// 文件名：DialogComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/11 14:47:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class DialogComponent:MonoBehaviour
    {
        private StateText _txt;
        private string _content;
        public int index;

        private HtmlUtils _htmlUtil = new HtmlUtils();

       

        public void SetTxtContent(StateText txt, string content)
        {
            //Stop();


            Clear();
            _htmlUtil.Clear();
            _txt = txt;
            _txt.CurrentText.text = string.Empty;
            _content = content;
            index = 1;
            _htmlUtil.GetContentStack(content);
        }


        public void Clear()
        {
            index = 0;
            _content = null;
            _txt = null;
            _htmlUtil.ClearContent();
            _htmlUtil.isPause = false;
        }

        public void Stop()
        {
            index = 0;

            _htmlUtil.End(_txt, _content);
            _content = null;
            _txt = null;
            _htmlUtil.ClearContent();
            _htmlUtil.isPause = false;
        }

        public bool IsPaused()
        {
            if(_htmlUtil!=null)
            {
                return _htmlUtil.isPause;
            }
            return false;
        }

        public void ContinuePlay()
        {
            _htmlUtil.ContinuePlay(_txt);
        }

        void Update()
        {
            if (index != 0)
            {
                _htmlUtil.Next(_txt, Callback);
            }
            if (index != 0)
            {
                _htmlUtil.Next(_txt, Callback);
            }
        }

       private void Callback()
        {
            Stop();
            index = 0;
        }

       

        
    }
}
