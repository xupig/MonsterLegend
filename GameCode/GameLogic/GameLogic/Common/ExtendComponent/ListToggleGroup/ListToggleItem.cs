﻿#region 模块信息
/*==========================================
// 文件名：ListToggleItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.ListToggleGroup
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/5 20:57:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent.ListToggleGroup
{
    public abstract class ListToggleItem:KList.KListItemBase
    {
        protected KToggle m_toggle;
        public KToggle toggle
        {
            get { return m_toggle; }
        }



    }
}
