﻿#region 模块信息
/*==========================================
// 文件名：ListToggleGroup
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.ListToggleGroup
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/5 17:43:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common.ExtendComponent.ListToggleGroup
{
    public class ListToggleGroup:KContainer
    {
            private KList m_listCategoty;
            private ToggleGroup m_toggleGroup;
            private List<KToggle> _toggleList;

            private int _selectedIndex = -1;
            public KComponentEvent<ListToggleGroup, int> onSelectedIndexChanged = new KComponentEvent<ListToggleGroup, int>();

            protected override void Awake()
            {
                m_listCategoty = this.gameObject.GetComponent<KList>();
                m_toggleGroup = this.gameObject.AddComponent<ToggleGroup>();
                _toggleList = new List<KToggle>();
                
            }

            public List<KList.KListItemBase> ItemList
            {
                get
                {
                    return m_listCategoty.ItemList;
                }
            }

            public void DoLayout()
            {
                m_listCategoty.DoLayout();
            }

            public void SetGap(int topToDownGap, int leftToRightGap)
            {
                m_listCategoty.SetGap(topToDownGap, leftToRightGap);
            }

            public void SetDirection(KList.KListDirection direction, int leftToRightCount, int topToDownCount)
            {
                m_listCategoty.SetDirection(direction, leftToRightCount, topToDownCount);
            }

            public void SetPadding(int top, int right, int bottom, int left)
            {
                m_listCategoty.SetPadding(top, right, bottom, left);
            }

            public void SetDataList<T>(IList dataList) where T : ListToggleItem
            {
                foreach(KToggle toggle in _toggleList)
                {
                    m_toggleGroup.UnregisterToggle(toggle);
                    toggle.group = null;
                    toggle.onValueChanged.RemoveListener(this.OnToggleValueChanged);
                }
                _toggleList.Clear();

                m_listCategoty.RemoveAll();
                m_listCategoty.SetDataList<T>(dataList);

                foreach(T item in m_listCategoty.ItemList)
                {
                    RegisterToggle(item);
                }

                if(_toggleList.Count > 0)
                {
                    if (_selectedIndex + 1 <= _toggleList.Count)
                    {
                        if(_selectedIndex == -1)
                        {
                            _selectedIndex = 0;
                        }
                        _toggleList[_selectedIndex].isOn = true;
                       
                    }
                    else
                    {
                        _selectedIndex = 0;
                        _toggleList[_selectedIndex].isOn = true;
                       
                    }
                }
            }


            public void RegisterToggle(ListToggleItem item)
            {
                KToggle toggle = item.toggle;
                _toggleList.Add(toggle);
                toggle.group = m_toggleGroup;
                m_toggleGroup.RegisterToggle(toggle);
                toggle.onValueChanged.AddListener(this.OnToggleValueChanged);
            }

            public int SelectIndex
            {
                get
                {
                    return _selectedIndex;
                }
                set
                {
                    if(value != _selectedIndex)
                    {
                        if (_selectedIndex + 1 <= _toggleList.Count && _toggleList.Count > 0)
                        {
                            _toggleList[_selectedIndex].isOn = false;
                            _selectedIndex = value;
                            _toggleList[_selectedIndex].isOn = true;
                        }
                    }
                }
            }

            void OnToggleValueChanged(KToggle target, bool isOn)
            {
                if (isOn == true)
                {
                    int oldSelectIndex = _selectedIndex;
                    UpdateSelectIndex();
                    if (oldSelectIndex != _selectedIndex)
                    {
                        TriggerChangeEvent();
                    }
                }
            }

            private void TriggerChangeEvent()
            {
                onSelectedIndexChanged.Invoke(this, _selectedIndex);
            }

            private void UpdateSelectIndex()
            {
                _selectedIndex = -1;
                for (int i = 0; i < _toggleList.Count; i++)
                {
                    KToggle toggle = _toggleList[i];
                    if (toggle.isOn == true)
                    {
                        _selectedIndex = i;
                        break;
                    }
                }
            }

        }
}
