﻿#region 模块信息
/*==========================================
// 文件名：DragableComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/5 16:29:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent.Dragable;
using Common.Structs;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class DragableComponent : MonoBehaviour,IDragHandler,IBeginDragHandler,IEndDragHandler
    {

        public KComponentEvent<PointerEventData> onBeginDrag = new KComponentEvent<PointerEventData>();
        public KComponentEvent<PointerEventData> onDrag = new KComponentEvent<PointerEventData>();
        public KComponentEvent<PointerEventData> onEndDrag = new KComponentEvent<PointerEventData>();

        public void OnDrag(PointerEventData eventData)
        {
            onDrag.Invoke(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            onBeginDrag.Invoke(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            onEndDrag.Invoke(eventData);
        }
    }
}
