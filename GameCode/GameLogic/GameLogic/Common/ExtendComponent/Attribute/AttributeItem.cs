﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 16:24:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class AttributeItem : KList.KListItemBase
    {
        private const string ATTRIBUTE_NAME_PREFIX = "Label_attributeName";
        private const string ATTRIBUTE_VALUE_PREFIX = "Label_txtFightValue";

        private StateText _textName;
        private StateText _textValue;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>(ATTRIBUTE_NAME_PREFIX);
            _textValue = GetChildComponent<StateText>(ATTRIBUTE_VALUE_PREFIX);
        }

        public override object Data
        {
            set
            {
                IAttribute data = value as IAttribute;
                _textName.CurrentText.text = string.Format(GetNameTemplate(), data.GetAttributeName());
                _textValue.CurrentText.text = data.GetAttributeValue();
            }
        }

        protected virtual string GetNameTemplate()
        {
            return "{0}：";
        }

        public override void Dispose()
        {
        }
    }
}
