﻿using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class LongClickButton : KContainer, IPointerDownHandler, IPointerUpHandler
    {
        private const uint LONG_CLICK_TIME = 300;
        private uint _clickTimerId;
        private bool _isLongClick;

        public KComponentEvent onClick = new KComponentEvent();
        public KComponentEvent onLongClickBegin = new KComponentEvent();
        public KComponentEvent onLongClickEnd = new KComponentEvent();

        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
            if (_clickTimerId != 0)
            {
                TimerHeap.DelTimer(_clickTimerId);
                _clickTimerId = 0;
            }
            _isLongClick = false;
            _clickTimerId = TimerHeap.AddTimer(LONG_CLICK_TIME, 0, OnLongClick);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_clickTimerId != 0)
            {
                TimerHeap.DelTimer(_clickTimerId);
                _clickTimerId = 0;
            }
            if (_isLongClick == false)
            {
                onClick.Invoke();
            }
            else
            {
                onLongClickEnd.Invoke();
            }
        }

        private void OnLongClick()
        {
            onLongClickBegin.Invoke();
            _isLongClick = true;
            if (_clickTimerId != 0)
            {
                TimerHeap.DelTimer(_clickTimerId);
                _clickTimerId = 0;
            }
        }
    }
}
