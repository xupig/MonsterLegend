﻿using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class TweenViewMove : KContainer
    {
        private RectTransform _rect;
        private Vector2 _originalPosition;
        private Vector2 _targetPosition;
        private TweenPosition _tweener;

        public float percent = 1f;

        public TweenPosition Tweener
        {
            get
            {
                return _tweener;
            }
        }

        protected override void Awake()
        {
            _rect = transform as RectTransform;
            _originalPosition = _rect.anchoredPosition;
            _tweener = gameObject.AddComponent<TweenPosition>();
            _tweener.enabled = false;
        }

        public void ResetOriginalPosition()
        {
            _originalPosition = _rect.anchoredPosition;
        }

        private void Show(MoveDirection directioin)
        {
            Visible = true;
            if (gameObject.activeInHierarchy == true)
            {
                CalculateTargetPosition(directioin);
                _rect.anchoredPosition = _targetPosition;
                _tweener = TweenPosition.Begin(gameObject, 0.2f, _originalPosition, 0, UITweener.Method.EaseOut, ShowOnFinish);
            }
        }

        private void ShowOnFinish(UITweener tween)
        {
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
        }

        private void Hide(MoveDirection directioin)
        {
            if (gameObject.activeInHierarchy == true)
            {
                CalculateTargetPosition(directioin);
                _rect.anchoredPosition = _originalPosition;
                _tweener = TweenPosition.Begin(gameObject, 0.2f, _targetPosition, 0, UITweener.Method.EaseIn);
                _tweener.onFinished = OnTweenEnd;
            }
            else
            {
                Visible = false;
            }
        }

        private void OnTweenEnd(UITweener tween)
        {
            Visible = false;
        }

        private void Begin(MoveType moveType, MoveDirection directioin)
        {
            switch (moveType)
            {
                case MoveType.Show:
                    Show(directioin);
                    break;
                case MoveType.Hide:
                    Hide(directioin);
                    break;
            }
        }

        private void CalculateTargetPosition(MoveDirection directioin)
        {
            _targetPosition = _originalPosition;
            switch (directioin)
            {
                case MoveDirection.Left:
                    _targetPosition.x = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent as RectTransform, new Vector2(0, 0)).x - _rect.sizeDelta.x;
                    _rect.localPosition = new Vector3(_targetPosition.x, _rect.localPosition.y, _rect.localPosition.z);
                    break;
                case MoveDirection.Right:
                    _targetPosition.x = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent as RectTransform, new Vector2(Screen.width, 0)).x;
                    _rect.localPosition = new Vector3(_targetPosition.x, _rect.localPosition.y, _rect.localPosition.z);
                    break;
                case MoveDirection.Up:
                    _targetPosition.y = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent as RectTransform, new Vector2(0, Screen.height)).y + _rect.sizeDelta.y;
                    _rect.localPosition = new Vector3(_rect.localPosition.x, _targetPosition.y, _rect.localPosition.z);
                    break;
                case MoveDirection.Down:
                    _targetPosition.y = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent as RectTransform, new Vector2(0, 0)).y;
                    _rect.localPosition = new Vector3(_rect.localPosition.x, _targetPosition.y, _rect.localPosition.z);
                    break;
            }
            _targetPosition = _rect.anchoredPosition;
            CalculatePercentPosition(directioin);
        }

        private void CalculatePercentPosition(MoveDirection directioin)
        {
            if (percent == 1) return;
            switch (directioin)
            {
                case MoveDirection.Left:
                    _targetPosition.x = _targetPosition.x + (_originalPosition.x - _targetPosition.x) * (1 - percent);
                    break;
                case MoveDirection.Right:
                    _targetPosition.x = _originalPosition.x + (_targetPosition.x - _originalPosition.x) * percent;
                    break;
                case MoveDirection.Up:
                    _targetPosition.y = _originalPosition.y + (_targetPosition.y - _originalPosition.y) * percent;
                    break;
                case MoveDirection.Down:
                    _targetPosition.y = _targetPosition.y + (_originalPosition.y - _targetPosition.y) * (1 - percent);
                    break;
            }
        }

        static public TweenViewMove Begin(GameObject go, MoveType moveType, MoveDirection directioin, float percent = 1)
        {
            TweenViewMove comp = go.GetComponent<TweenViewMove>();
            if (comp == null) comp = go.AddComponent<TweenViewMove>();
            comp.percent = percent;
            comp.Begin(moveType, directioin);
            return comp;
        }

        static public TweenViewMove Begin(GameObject go, MoveType moveType, MoveDirection directioin, bool resetPosition)
        {
            TweenViewMove comp = go.GetComponent<TweenViewMove>();
            if (comp == null) comp = go.AddComponent<TweenViewMove>();
            if (resetPosition)
            {
                comp.ResetOriginalPosition();
            }
            comp.Begin(moveType, directioin);
            return comp;
        }


    }

    public enum MoveType
    {
        Show = 1,   //切入
        Hide = 2,   //切出
    }

    public enum MoveDirection
    {
        Left = 1,//右往左
        Up = 2,
        Right = 3,//左往右
        Down = 4,
    }

}
