﻿#region 模块信息
/*==========================================
// 文件名：TweenProgressBar
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/6 11:27:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class TweenProgressBar : UIBehaviour
    {
        private KProgressBar _progressBar;

        private float _preNum;
        private float _preTotal;
        private float _preLevel;

        private float _nowNum;
        private float _nowTotal;
        private float _nowLevel;
        private bool _reachMax;

        private float _step;
        private uint _timeId;

        private KParticle _particle;
        private Transform _transformParticle;

        private float width = 0;


        public KComponentEvent<float, float> onProgress = new KComponentEvent<float, float>();

        protected override void Awake()
        {
            base.Awake();
            _progressBar = this.gameObject.GetComponent<KProgressBar>();
            _progressBar.Value = 0;
            width = _progressBar.GetComponent<RectTransform>().sizeDelta.x;
            _transformParticle = gameObject.transform.FindChild("fx_ui_10_3_jindutiao_01");
            if (_transformParticle != null)
            {
                _particle = _transformParticle.gameObject.AddComponent<KParticle>();
                _particle.Stop();
            }
        }

        public void Clear()
        {
            RemoveTimer();
            StopParticle();
            _preNum = 0;
            _preTotal = 0;
            _nowNum = 0;
            _nowTotal = 0;
            _step = 0;
            _preLevel = 0;
            _nowLevel = 0;
            _progressBar.Value = 0;
            _reachMax = false;
        }

        protected sealed override void OnDisable()
        {
            base.OnDisable();
            RemoveTimer();
            StopParticle();
            _preNum = _nowNum;
            _preTotal = _nowTotal;
            _preLevel = _nowLevel;
            SetProgressValue(_nowNum, _nowTotal);
        }

        public void SetProgress(float num, float total,int level)
        {
            _nowNum = num;
            _nowTotal = total;
            _nowLevel = level;
            if (_preTotal == 0)
            {
                _preNum = _nowNum;
                _preTotal = _nowTotal;
                _preLevel = _nowLevel;
                _progressBar.Value = _nowNum / _nowTotal;
                onProgress.Invoke(_preNum, _preTotal);
                return;
            }
            if (total > _preTotal)
            {
                _step = 0.02f;
            }
            else if (total < _preTotal)
            {
                _step = -0.02f;
            }
            else
            {
                _step = (level - _preLevel > 0) ? 0.02f : -0.02f;
            }
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
            }
            if (_transformParticle != null)
            {
                _transformParticle.localPosition = new Vector3(width * _progressBar.Value, 0, 0);
            }
            _timeId = TimerHeap.AddTimer(0, 50, UpdateProgress2);
            if (_particle != null)
            {
                _particle.Play(true);
            }

        }

        public void SetProgress(float num, float total, bool ReachMax = false)
        {
            _nowNum = num;
            _nowTotal = total;
            _reachMax = ReachMax;
            //第一次设置值不用缓动
            if (_preTotal == 0)
            {
                _preNum = _nowNum;
                _preTotal = _nowTotal;
                SetProgressValue(_nowNum, _nowTotal);
                return;
            }
            if (ReachMax)
            {
                _step = 0.02f;
            }
            else if (total > _preTotal)
            {
                _step = 0.02f;
            }
            else if (total < _preTotal)
            {
                _step = -0.02f;
            }
            else
            {
                _step = (_nowNum - _preNum > 0) ? 0.02f : -0.02f;
            }
            RemoveTimer();
            if (_transformParticle != null)
            {
                _transformParticle.localPosition = new Vector3(width * _progressBar.Value, 0, 0);
            }
            _timeId = TimerHeap.AddTimer(0, 50, UpdateProgress);
            if (_particle != null)
            {
                _particle.Play(true);
            }
        }

        private void UpdateProgress()
        {
            float nextProgress = _progressBar.Value + _step;
            if (nextProgress > 1 && _preTotal != _nowTotal)
            {
                _progressBar.Value = 0;
                _preTotal = _nowTotal;
                _preNum = 0;
            }
            else if (nextProgress < 0 && _preTotal != _nowTotal)
            {
                _progressBar.Value = 1;
                _preTotal = _nowTotal;
                _preNum = _nowTotal;
            }
            else
            {
                _progressBar.Value = nextProgress;
            }
            _preNum = _preTotal * _progressBar.Value;
            if (_transformParticle != null)
            {
                _transformParticle.localPosition = new Vector3(width * _progressBar.Value, 0, 0);
            }
            if (MeetEndCondition(nextProgress))
            {
                EndTween();
                _preNum = _nowNum;
                SetProgressValue(_nowNum, _nowTotal);
            }
        }

        private bool MeetEndCondition(float nextProgress)
        {
            if (_preTotal == _nowTotal)
            {
                if (((_reachMax == true) && (nextProgress > 1))
                    || ((_step > 0) && (_preNum >= _nowNum))
                    || ((_step < 0) && (_preNum <= _nowNum)))
                {
                    return true;
                }
            }
            return false;
        }

        private void UpdateProgress2()
        {
            float nextProgress = _progressBar.Value + _step;
            if (nextProgress > 1 && _preLevel != _nowLevel)
            {
                _progressBar.Value = 0;
                _preTotal = _nowTotal;
                _preLevel = _nowLevel;
                _preNum = 0;
            }
            else if (nextProgress < 0 && _preLevel != _nowLevel)
            {
                _progressBar.Value = 1;
                _preTotal = _nowTotal;
                _preNum = _nowTotal;
                _preLevel = _nowLevel;
            }
            else
            {
                _progressBar.Value = nextProgress;
            }
            _preNum = _preTotal * _progressBar.Value;
            if (_transformParticle != null)
            {
                _transformParticle.localPosition = new Vector3(width * _progressBar.Value, 0, 0);
            }
            if ((_step > 0 && _preLevel == _nowLevel && _preNum >= _nowNum) || (_step < 0 && _preLevel == _nowLevel && _preNum <= _nowNum))
            {
                EndTween();
                _preNum = _nowNum;
                SetProgressValue(_nowNum, _nowTotal);
                _preLevel = _nowLevel;
            }
            onProgress.Invoke(_preNum, _preTotal);
        }

        private void SetProgressValue(float num, float total)
        {
            if (total == 0)
            {
                _progressBar.Value = 0;
            }
            else
            {
                _progressBar.Value = num / total;
            }
        }

        private void EndTween()
        {
            RemoveTimer();
            StopParticle();
        }

        private void RemoveTimer()
        {
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
        }

        private void StopParticle()
        {
            if (_particle != null)
            {
                _particle.Stop();
            }
        }

        public void SetProgressFx(string name)
        {
            _transformParticle = gameObject.transform.FindChild(name);
            if (_transformParticle != null)
            {
                _particle = _transformParticle.gameObject.AddComponent<KParticle>();
                _particle.Stop();
            }
        }
    }
}
