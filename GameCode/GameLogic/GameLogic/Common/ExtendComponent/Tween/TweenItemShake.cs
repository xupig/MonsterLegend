﻿using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class TweenItemShake : KContainer
    {
        private const float EVERY_SHAKE_TIME = 0.5f;
        private const float RANGE_DECLINE = 1.5f;
        private float _radius;
        private bool _isDecline;
        private uint _timerId = 0;
        private Vector3 _originalPosition;

        protected override void Awake()
        {
            _originalPosition = transform.localPosition;
        }

        protected override void OnDisable()
        {
            ResetTimer();
        }

        static public TweenItemShake Begin(GameObject go)
        {
            TweenItemShake comp = go.GetComponent<TweenItemShake>();
            if (comp == null) comp = go.AddComponent<TweenItemShake>();
            comp.BeginShake();
            return comp;
        }

        private void BeginShake()
        {
            ResetTimer();
            _radius = 4f;
            _isDecline = true;
            float radian = UnityEngine.Random.Range(0, 2 * Mathf.PI);
            float x = _radius * Mathf.Cos(radian);
            float y = _radius * Mathf.Sin(radian);
            gameObject.transform.localPosition = _originalPosition + new Vector3(x, y, 0);
            _timerId = TimerHeap.AddTimer<float>(0, Mathf.RoundToInt(EVERY_SHAKE_TIME * 1000), OnStepShake, radian);
        }

        private void OnStepShake(float radian)
        {
            if (_radius <= 0)
            {
                ResetTimer();
                TweenPosition.Begin(gameObject, EVERY_SHAKE_TIME, _originalPosition, 0, UITweener.Method.EaseOut);
                return;
            }
            float nx = _radius * Mathf.Cos(radian);
            float ny = _radius * Mathf.Sin(radian);
            if (_isDecline == true)
            {
                TweenPosition.Begin(gameObject, EVERY_SHAKE_TIME, _originalPosition + new Vector3(-nx, -ny, 0), 0, UITweener.Method.EaseOut);
                _radius -= RANGE_DECLINE;
            }
            else
            {
                TweenPosition.Begin(gameObject, EVERY_SHAKE_TIME, _originalPosition + new Vector3(nx, ny, 0), 0, UITweener.Method.EaseOut);
            }
            _isDecline = !_isDecline;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }
    }
}
