﻿using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class TweenListEntry : KContainer
    {
        private KList _list;

        private const float ITEM_ENTRY_INTERVAL = 0.05f;
        private const float ITEM_ENTRY_TIME = 0.2f;

        private const float ROW_ENTRY_TIME = 0.3f;

        public TweenListEntryMoveDirection MoveDirection;

        protected override void Awake()
        {
            _list = GetComponent<KList>();
        }

        private void Show()
        {
            float delay = 0f;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                RectTransform _rect = _list.ItemList[i].GetComponent<RectTransform>();
                if (_rect.gameObject.activeInHierarchy == false) return; 
                TweenPosition tween = _rect.GetComponent<TweenPosition>();
                Vector2 originalPosition;
                if (tween != null && tween.enabled == true)
                {
                    originalPosition = tween.to;
                }
                else
                {
                    originalPosition = _rect.anchoredPosition;
                }
                if (MoveDirection == TweenListEntryMoveDirection.LeftToRight)
                {
                    _rect.anchoredPosition = new Vector2(-_rect.sizeDelta.x, _rect.anchoredPosition.y);
                }
                else if (MoveDirection == TweenListEntryMoveDirection.RightToLeft)
                {
                    _rect.anchoredPosition = new Vector2(_rect.sizeDelta.x, _rect.anchoredPosition.y);
                }
                if (i == _list.ItemList.Count - 1)
                {
                    tween = TweenPosition.Begin(_rect.gameObject, ITEM_ENTRY_TIME, originalPosition, delay, UITweener.Method.EaseOut, OnFinish);
                }
                else
                {
                    tween = TweenPosition.Begin(_rect.gameObject, ITEM_ENTRY_TIME, originalPosition, delay, UITweener.Method.EaseOut);
                }
                delay += ITEM_ENTRY_INTERVAL;
            }
        }

        private void OnFinish(UITweener tween)
        {
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
        }

        private Vector2 CalculateStartPosition(int index, RectTransform _rect, KList list)
        {
            int gap = list.LeftToRightGap;
            int leftToRightCount = list.LeftToRightCount;
            float x = 0;
            if (MoveDirection == TweenListEntryMoveDirection.LeftToRight)
            {
                x = -(index % leftToRightCount + 1) * _rect.sizeDelta.x - (index % leftToRightCount) * gap;
            }
            else if (MoveDirection == TweenListEntryMoveDirection.RightToLeft)
            {
                x = (index % leftToRightCount + 1) * _rect.sizeDelta.x + (index % leftToRightCount) * gap;
            }
            return new Vector2(x, _rect.anchoredPosition.y);
        }

        static public TweenListEntry Begin(GameObject go, TweenListEntryMoveDirection moveDirection = TweenListEntryMoveDirection.RightToLeft)
        {
            TweenListEntry comp = go.GetComponent<TweenListEntry>();
            if (comp == null) comp = go.AddComponent<TweenListEntry>();
            comp.MoveDirection = moveDirection;
            comp.Show();
            return comp;
        }
    }

    public enum TweenListEntryMoveDirection
    {
        LeftToRight = 1,
        RightToLeft = 2,
    }
}
