using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class TweenStateChangeableAlpha : UITweener
    {
        public float from = 1f;
        public float to = 1f;

        StateChangeable m_sc;

        public float Alpha
        {
            get
            {
                if (m_sc != null) return m_sc.Alpha;
                return 0f;
            }
            set
            {
                if (m_sc != null) m_sc.Alpha = value;
            }
        }

        void Awake()
        {
            m_sc = GetComponent<StateChangeable>();
        }

        override protected void OnUpdate(float factor, bool isFinished) { Alpha = Mathf.Lerp(from, to, factor); }

        static public TweenStateChangeableAlpha Begin(GameObject go, float duration, float alpha)
        {
            TweenStateChangeableAlpha comp = UITweener.Begin<TweenStateChangeableAlpha>(go, duration);
            comp.from = comp.Alpha;
            comp.to = alpha;

            if (duration <= 0f)
            {
                comp.Sample(1f, true);
                comp.enabled = false;
            }
            return comp;
        }

        static public TweenStateChangeableAlpha Begin(GameObject go, float duration, float alpha, float delay, Method method)
        {
            TweenStateChangeableAlpha comp = Begin(go, duration, alpha);
            comp.delay = delay;
            comp.method = method;

            return comp;
        }

        static public TweenStateChangeableAlpha Begin(GameObject go, float duration, float alpha, OnFinished onFinish)
        {
            TweenStateChangeableAlpha comp = Begin(go, duration, alpha);
            comp.onFinished = onFinish;

            return comp;
        }

    }
}