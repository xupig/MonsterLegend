﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class TweenTreasureBox : KContainer
    {
        private const float Duration = 0.5f;

        private Vector3 _initPosition;
        private Vector3 _initScale;

        private TweenPosition _tweenPosition;
        private TweenScale _tweenScale;

        protected override void Awake()
        {
            _initPosition = transform.localPosition;
            _initScale = transform.localScale;
        }

        public void StartTween()
        {
            Vector3 targetPosition = new Vector3(_initPosition.x, _initPosition.y + 5, _initPosition.z);
            TweenPosition tweenPosition = TweenPosition.Begin(gameObject, Duration, targetPosition);
            tweenPosition.from = _initPosition;
            tweenPosition.style = UITweener.Style.PingPong;
            _tweenPosition = tweenPosition;

            Vector3 targetScale = new Vector3(_initScale.x + 0.02f, _initScale.y + 0.02f, _initPosition.z);
            TweenScale tweenScale = TweenScale.Begin(gameObject, Duration, targetScale);
            tweenScale.from = _initScale;
            tweenScale.style = UITweener.Style.PingPong;
            _tweenScale = tweenScale;
        }

        public void StopTween()
        {
            if (_tweenPosition != null)
            {
                _tweenPosition.enabled = false;
            }
            if (_tweenScale != null)
            {
                _tweenScale.enabled = false;
            }
            transform.localPosition = _initPosition;
            transform.localScale = _initScale;
        }

        public void ResetPosition()
        {
            _initPosition = transform.localPosition;
        }

        public static TweenTreasureBox Begin(GameObject gameObject)
        {
            TweenTreasureBox tween = gameObject.GetComponent<TweenTreasureBox>();
            if (tween == null)
            {
                tween = gameObject.AddComponent<TweenTreasureBox>();
            }
            tween.StartTween();
            return tween;
        }

        public static void Stop(GameObject gameObject)
        {
            TweenTreasureBox tween = gameObject.GetComponent<TweenTreasureBox>();
            if (tween != null)
            {
                tween.StopTween();
            }
        }

        public static void ResetPosition(GameObject gameObject)
        {
            TweenTreasureBox tween = gameObject.GetComponent<TweenTreasureBox>();
            if (tween != null)
            {
                tween.ResetPosition();
            }
        }
    }
}
