﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class TweenRollNumber : KContainer
    {
        private bool _playing;
        private int _index;
        private List<int> _numberList = new List<int>();
        private int _playFrame;

        public StateText _textNumber;
        private string _template = "{0}";

        protected override void Awake()
        {
            _textNumber = gameObject.GetComponent<StateText>();
            _playing = false;
        }

        public void SetLabelTemplate(string template)
        {
            _template = template;
        }

        public void StartTween(int startNum, int endNum)
        {
            _numberList.Clear();
            if (Math.Abs(endNum - startNum) >= 20)
            {
                _playFrame = 20;
            }
            else
            {
                _playFrame = Math.Abs(endNum - startNum);
            }
            float interval = (endNum - startNum) / (float)_playFrame;
            for (int i = 0; i < _playFrame; i++)
            {
                _numberList.Add((int)(startNum + interval * i));
            }
            _numberList.Add(endNum);

            _index = 0;
            _playing = true;
        }
        void Update()
        {
            if (_playing)
            {
                if (_index < _numberList.Count)
                {
                    _textNumber.ChangeAllStateText(string.Format(_template, _numberList[_index]));
                }
                else
                {
                    _playing = false;
                }
                _index++;
            }
        }
    }
}
