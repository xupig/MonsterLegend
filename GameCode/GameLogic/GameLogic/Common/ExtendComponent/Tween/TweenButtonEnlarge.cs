﻿using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class TweenButtonEnlarge : KContainer, IPointerUpHandler, IPointerDownHandler
    {
        private RectTransform _rect;
        //private Vector3 _orginPosition;
        private const float EXTAND_RATE = 1.2f;
        private const float ORIGN_RATE = 1.0f;

        protected override void Awake()
        {
            _rect = GetComponent<RectTransform>();
            _rect.pivot = new Vector2(0.5f, 0.5f);
            Vector3 localposition = _rect.localPosition;
            localposition.x += _rect.sizeDelta.x * 0.5f;
            localposition.y -= _rect.sizeDelta.y * 0.5f;
            _rect.localPosition = localposition;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //TweenPosition.Begin(gameObject, 0.1f, _orginPosition, 0, UITweener.Method.EaseOut);
            TweenScale.Begin(gameObject, 0.1f, new Vector3(ORIGN_RATE, ORIGN_RATE, 0), UITweener.Method.EaseOut);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //_orginPosition = _rect.localPosition;
            //Vector3 movePosition = new Vector3(_rect.sizeDelta.x * (ORIGN_RATE - EXTAND_RATE) * 0.5f, -_rect.sizeDelta.y * (ORIGN_RATE - EXTAND_RATE) * 0.5f, 0);
            transform.SetAsLastSibling();
            //TweenPosition.Begin(gameObject, 0.1f, _orginPosition + movePosition, 0, UITweener.Method.EaseOut);
            TweenScale.Begin(gameObject, 0.1f, new Vector3(EXTAND_RATE, EXTAND_RATE, 0), UITweener.Method.EaseOut);
        }
    }
}
