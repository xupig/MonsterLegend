﻿#region 模块信息
/*==========================================
// 文件名：CircleToggleGroupItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.CircleToggleGroup
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 10:41:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class CircleToggleItem : KContainer,IPointerClickHandler
    {
        private StateText _txtName;
        private StatePlaceHolder _placeHolder;
        private StateImage _imgFinish;
       // private StateImage _lockState;
        private IconContainer _activityTypeContainer;
        private LimitActivityData _functionData;
        public KComponentEvent<int> onClick = new KComponentEvent<int>();
        private KToggle _toggle;
        private RectTransform _rectTransform;

        private KContainer _imgNameBg;
        protected override void Awake()
        {

            _toggle = GetComponent<KToggle>();
            _activityTypeContainer = AddChildComponent<IconContainer>("huodongleixing");
            _txtName = GetChildComponent<StateText>("label");
            _imgNameBg = GetChildComponent<KContainer>("mingchengdi");
            _imgFinish = GetChildComponent<StateImage>("jinriyiwancheng");
            _placeHolder = AddChildComponent<StatePlaceHolder>("icon");
            //_lockState = GetChildComponent<StateImage>("image3");
            //_lockState.Visible = false;
            _rectTransform = _toggle.gameObject.GetComponent<RectTransform>();
            _rectTransform.pivot = new Vector2(0.5f, 0);
            _toggle.enabled = false;
            _toggle.isOn = false;
            _placeHolder.onClick.AddListener(OnPlaceHolderClick);
            base.Awake();
        }

        public KToggle Toggle
        {
            get { return _toggle; }
            set { _toggle = value; }
        }

        public int Index
        {
            get;
            set;
        }

        public object Data
        {
            get
            {
                return _functionData;
            }
            set
            {
                _functionData = value as LimitActivityData;
                Refresh();
            }
        }

        protected override void OnDestroy()
        {
            _placeHolder.onClick.RemoveListener(OnPlaceHolderClick);
            base.OnDestroy();
        }

        private void Refresh()
        {
            if(_functionData!=null)
            {
                //_lockState.Visible = !activity_helper.IsOpen(_functionData);
                if (_functionData.openData.__name != 0)
                {
                    _txtName.ChangeAllStateText(MogoLanguageUtil.GetContent(_functionData.openData.__name));
                }
                else
                {
                    _txtName.ChangeAllStateText(MogoLanguageUtil.GetContent(_functionData.functionData.__name));
                }
                //_imgFinish.Visible = PlayerDataManager.Instance.ActivityData.Contains(_functionData.openData.__id);
                if (_functionData.openData.__type_icon != 0)
                {
                    _activityTypeContainer.Visible = true;
                    _activityTypeContainer.SetIcon(_functionData.openData.__type_icon);
                }
                else
                {
                    _activityTypeContainer.Visible = false;
                }
                if (activity_helper.IsOpen(_functionData))
                {
                    if (_functionData.openData.__icon != 0)
                    {
                        _placeHolder.SetIcon(_functionData.openData.__icon);
                    }
                    else
                    {
                        _placeHolder.SetIcon(_functionData.functionData.__icon);
                    }
                }
                else
                {
                    _placeHolder.SetIcon(15762);
                }
                
                //_placeHolder.Visible = !_lockState.Visible;
                _imgNameBg.Visible = true;
            }
            else
            {
                _activityTypeContainer.Visible = false;
                _placeHolder.SetIcon(15762);
                _txtName.CurrentText.text = string.Empty;
                _imgNameBg.Visible = false;
            }
        }

        public void SetActivityLeftCount()
        {
            _imgFinish.Visible = PlayerDataManager.Instance.ActivityData.GetActivityCount(_functionData.openData.__id) == 0;
        }

        public Vector3 localPosition
        {
            get 
            {
                if(_rectTransform!=null)
                {
                    return _rectTransform.anchoredPosition3D;
                }
                return Vector3.zero;
            }
            set
            {
                if(_rectTransform!=null)
                {
                    _rectTransform.anchoredPosition3D = value; 
                }
            }
        }

        public Vector3 localRotation
        {
            get { return _rectTransform.localRotation.eulerAngles; }
            set
            { 
                Quaternion q = _rectTransform.localRotation;
                q.eulerAngles = value;
                _rectTransform.localRotation = q;
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            onClick.Invoke(Index);
        }

        private void OnPlaceHolderClick()
        {
            onClick.Invoke(Index);
        }

    }
}
