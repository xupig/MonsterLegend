﻿#region 模块信息
/*==========================================
// 文件名：CircleToggleGroup
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 10:36:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class CircleToggleGroup : KContainer, IEndDragHandler, IDragHandler
    {
        private List<CircleToggleItem> _itemList;
        private List<CircleToggleItem> _sortList;
        //private KToggleGroup _toggleGroup;
        private GameObject _template;
        //private int _selectIndex = 0;
        private float _totalDetlaX = 0;
        private int _dragCenterIndex = 0;

        private int _clickIndex = -1;

        //private float gap = -72;
        private float _stepMove = 0;
        private int _count = 0;
        private int _totalCount = 0;
       // private uint _timerId;
        private static int TOTAL_FRAME = 5;

        public KComponentEvent<CircleToggleGroup, int> onSelectedIndexChanged = new KComponentEvent<CircleToggleGroup, int>();
        public KComponentEvent onClickSelectedItem = new KComponentEvent();
        private int angle = 14;

        private int radius = 400;

        private float _threshold;

        private Vector2 centerPos = new Vector2(122, -834);

        private bool _isTween = false;


        protected override void Awake()
        {
            _threshold = 400;
            _itemList = new List<CircleToggleItem>();
            _sortList = new List<CircleToggleItem>();
            InitToggleGroup();
            base.Awake();
        }

        private void InitToggleGroup()
        {
            //_toggleGroup = this.gameObject.AddComponent<KToggleGroup>();
            InitTemplate();
        }

        private void InitTemplate()
        {
            _template = GetChild("Toggle_item");
            if (_template != null)
            {
                _template.SetActive(false);
            }
        }

        public List<CircleToggleItem> GetItemList()
        {
            return _itemList;
        }

        public void SetDataList(IList dataList)
        {
            _clickIndex = -1;
            _totalDetlaX = 0;
            _dragCenterIndex = 0;
            _stepMove = 0;
            _count = 0;
            _totalCount = 0;
            _isTween = false;
            for (int i = 0; i < dataList.Count;i++ )
            {
                AddItem<CircleToggleItem>(dataList[i], false);
            }
            OnItemDragEnd();
        }
        private int _selectedIndex;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set 
            {
                if (_itemList.Count > _selectedIndex && _selectedIndex >= 0)
                {
                    _itemList[_selectedIndex].Toggle.isOn = false;
                    _itemList[_selectedIndex].transform.localScale = Vector3.one;
                }
                _selectedIndex = value;
                if (_itemList.Count > _selectedIndex && _selectedIndex >=0)
                {
                    _itemList[_selectedIndex].Toggle.isOn = true;
                    _itemList[_selectedIndex].transform.localScale = new Vector3(1.05f, 1.05f, 1.05f);
                }
            }
        }

        public void SetSelected(int index)
        {
            _clickIndex = index;
            TweenEnd();
        }

        private void OnItemDragEnd()
        {
            SelectedIndex = _dragCenterIndex;
            _totalDetlaX = 0;
            UpdateItemListLayout();
            onSelectedIndexChanged.Invoke(this, SelectedIndex);
        }


        private void AddItemAt(CircleToggleItem item, object data, int index, bool layoutImmediately = true)
        {
            AddItemGoToHierarchy(item.gameObject);
            item.Index = index;
            item.name = string.Format("Toggle_item{0}",index);
            item.Data = data;
            _itemList.Insert(index, item);
            _sortList.Insert(index,item);
            //_toggleGroup.RegisterToggle(item.Toggle);
            item.Toggle.isOn = false;
            AddItemEventListener(item);
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
        }

        private void AddItem<T>(object data, bool layoutImmediately = true) where T : CircleToggleItem
        {
            AddItemAt<T>(data, _itemList.Count, layoutImmediately);
        }

        private void AddItemAt<T>(object data, int index, bool layoutImmediately = true) where T : CircleToggleItem
        {
            GameObject itemGo = CreateItemGo();
            T item = itemGo.AddComponent<T>();

            AddItemAt(item, data, index, layoutImmediately);
        }

        private void AddItemEventListener(CircleToggleItem item)
        {
            item.onClick.AddListener(OnItemClicked);
        }

        private void RemoveItemEventListener(CircleToggleItem item)
        {
            item.onClick.RemoveListener(OnItemClicked);
        }

        public GameObject CreateItemGo()
        {
            return Instantiate(_template) as GameObject;
        }

        private void AddItemGoToHierarchy(GameObject itemGo)
        {
            itemGo.SetActive(true);
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
        }


        public void RemoveItemAt(int index)
        {
            CircleToggleItem item = _itemList[index];
            //_toggleGroup.UnregisterToggle(item.Toggle);
            item.gameObject.SetActive(false);
            _itemList.RemoveAt(index);
            _sortList.RemoveAt(index);
            RemoveItemEventListener(item);
            Destroy(item.gameObject);
            UpdateItemListLayout();
        }

        public void RemoveAll()
        {
            RemoveItemRange(0, _itemList.Count);
        }

        public void RemoveItemRange(int start, int count)
        {
            for (int i = start + count - 1; i >= start; i--)
            {
                CircleToggleItem item = _itemList[i];
               // _toggleGroup.UnregisterToggle(item.Toggle);
                _itemList.RemoveAt(i);
                _sortList.RemoveAt(i);
                RemoveItemEventListener(item);
                Destroy(item.gameObject);
            }
            UpdateItemListLayout();
        }

        private void OnItemClicked(int index)
        {
            if (_isTween &&  _isDraging)
            {
                return;
            }
            if (index != SelectedIndex)
            {
                _count = 0;
                _clickIndex = index;
                _totalCount = Math.Abs(SelectedIndex - index) * TOTAL_FRAME;
                float currentAngle = (SelectedIndex - index) * angle;
                float gap = Mathf.Sin(currentAngle * Mathf.PI / 180) * radius;
                _stepMove = gap / _totalCount;
                _isTween = true;
                //_timerId = TimerHeap.AddTimer(0, 20, TweenUpdate);
            }
            else
            {
                onClickSelectedItem.Invoke();
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _count = 0;
            _isTween = false;
            _dragCenterIndex = _clickIndex;
            _selectedIndex = _dragCenterIndex;
            _totalDetlaX = 0;
            
        }

        void Update()
        {
            if(_isTween)
            {
                OnItemDrag(_stepMove);
                _count++;
                if (_count >= _totalCount)
                {
                    TweenEnd();
                }
            }
        }

        private void TweenEnd()
        {
            _isTween = false;
            _count = 0;
            _totalCount = 0;
            _dragCenterIndex = _clickIndex;
            OnItemDragEnd();
        }


        private void UpdateItemListLayout()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                CircleToggleItem item = _itemList[i];
                float currentAngle = (i - SelectedIndex) * angle;
                if(currentAngle > 90)
                {
                    currentAngle = 90;
                }
                else if(currentAngle < -90)
                {
                    currentAngle = -90;
                }
                Vector2 position = CalculateItemPosition(currentAngle * Mathf.PI / 180);
                item.gameObject.SetActive(Math.Abs(position.x) <= _threshold);
                item.localRotation = new Vector3(0, 0, -currentAngle);

                item.localPosition = new Vector3(centerPos.x + position.x, centerPos.y + position.y, 0);
            }
            _sortList.Sort(SortIndex);
            for (int i = 0; i < _sortList.Count; i++)
            {
                _sortList[i].transform.SetSiblingIndex(i);
            }
            
        }

        private int SortIndex(CircleToggleItem item2, CircleToggleItem item1)
        {
            float angle1 = item1.localRotation.z>180?item1.localRotation.z-360:item1.localRotation.z;
            float angle2 = item2.localRotation.z>180?item2.localRotation.z-360:item2.localRotation.z;
            return Mathf.CeilToInt(Math.Abs(angle1) - Math.Abs(angle2));
        }

        private Vector2 CalculateItemPosition(float currentAngle)
        {
            Vector2 result = Vector2.zero;
            result.x = Mathf.Sin(currentAngle) * radius;
            result.y = Mathf.Cos(currentAngle) * radius;
            return result;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_isTween == false && _isDraging)
            {
                _isDraging = false;
                OnItemDragEnd();
            }
        }

        private bool _isDraging = false;

        public void OnDrag(PointerEventData eventData)
        {
            if (_isTween)
            {
                return;
            }
            if (eventData.delta.x != 0)
            {
                _isDraging = true;
                OnItemDrag(eventData.delta.x / Common.Global.Global.Scale);
            }
        }

        private void OnItemDrag(float delta)
        {
            int startIndex = 0;
            int endIndex = _itemList.Count - 1;
            if (delta > 0 && _itemList[startIndex].localPosition.x + delta > centerPos.x)
            {
                return;
            }
            if (delta < 0 && _itemList[endIndex].localPosition.x + delta < centerPos.x)
            {
                return;
            }
            _totalDetlaX += delta;
            DragAndUpdateItemLayout();
        }

        private void DragAndUpdateItemLayout()
        {
            float distance = int.MaxValue;
            int lastSelectedIndex = _dragCenterIndex;
            float posX = Mathf.Clamp(_totalDetlaX, -radius, radius);
            float selectedAngle = Mathf.Asin(posX / radius) * 180 / Mathf.PI;

            for (int i = 0; i < _itemList.Count; i++)
            {
                if(_itemList[i]!=null&&_itemList[i].gameObject!=null)
                {
                    CircleToggleItem item = _itemList[i];
                    float currentAngle = (i - SelectedIndex) * angle + selectedAngle;
                    if (currentAngle > 90)
                    {
                        currentAngle = 90;
                    }
                    else if (currentAngle < -90)
                    {
                        currentAngle = -90;
                    }
                    Vector2 position = CalculateItemPosition(currentAngle * Mathf.PI / 180);
                    item.gameObject.SetActive(Math.Abs(position.x) <= _threshold);
                    item.localRotation = new Vector3(0, 0, -currentAngle);
                    item.localPosition = new Vector3(centerPos.x + position.x, centerPos.y + position.y, 0);
                    float currentDistance = Math.Abs(position.x);
                    if (currentDistance < distance && currentDistance <= _threshold)
                    {
                        distance = currentDistance;
                        _dragCenterIndex = i;
                    }
                }
            }
            if (_dragCenterIndex != lastSelectedIndex)
            {
                _sortList.Sort(SortIndex);
                for (int i = 0; i < _itemList.Count; i++)
                {
                    if (_sortList[i].transform!=null)
                    {
                        _sortList[i].transform.SetSiblingIndex(i);
                    }
                }
            }
            _itemList[lastSelectedIndex].Toggle.isOn = false;
            _itemList[_dragCenterIndex].Toggle.isOn = true;
        }


    }
}
