﻿#region 模块信息
/*==========================================
// 文件名：ScrollToggleGroup
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/13 10:40:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class ScrollToggleGroup : UIBehaviour
    {
        //private static int longRadius = 247*247;
        private static int shortRadius = 120*120;

        private void UpdatePosition()
        {
            
        }

        private float GetX(float y)
        {
            return Mathf.Sqrt((1 - Mathf.Pow(y,2))*shortRadius);
        }
    }
}
