﻿using System;
using UnityEngine;
using Game.UI.UIComponent;
using Common.Structs;
using Common.Data;
using Common.Utils;
using GameData;
using ModuleCommonUI;
using Game.UI;
using Common.Base;
using Common.ServerConfig;
using GameLoader.Config;

namespace Common.ExtendComponent
{
    /// <summary>
    /// 添加到表示物品格子的Container_wupin节点上
    /// </summary>
    [DisallowMultipleComponent]
    public class ItemGrid : KContainer
    {
        private PanelIdEnum _context = PanelIdEnum.Empty;
        private BaseItemInfo data;
        private KContainer _icon;
        private ItemQualityDisplayer _displayer;
        private int _iconId;
        private int _itemId;
        private Action<GameObject> _loadCallback;
        private const int STAR_COUNT = 5;

        private GameObject[] _starGoList = new GameObject[STAR_COUNT];
        private RectTransform _iconRect;
        private KDummyButton gridBtn;
        
        public EquipState equipState = EquipState.NotHave;

        public bool IsClickable = true;
        public bool IsShowStar = true;

        public Action onClick
        {
            get;
            set;
        }

        private static readonly string STAR_ICON_SPRITE_KEY = string.Format("GUI${0}$Sprite$Shared$xingxingIcon.u", SystemConfig.Language);
        private static readonly string STAR_ICON_MATERIAL_KEY = string.Format("GUI${0}$Sprite$Shared$Shared_etc_shadow.u", SystemConfig.Language);
        private const string STAR_ICON_NAME = "xingxing{0}";

        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; }
        }

        protected override void Awake()
        {
            base.Awake();
            _icon = GetChildComponent<KContainer>("Container_icon");
            _displayer = GetChildComponent<ItemQualityDisplayer>("Container_yanse");
            _iconRect = GetChildComponent<RectTransform>("Container_icon");
            gameObject.AddComponent<KDummyButton>();
            gridBtn = GetComponent<KDummyButton>();
            gridBtn.onClick.AddListener(OnClickHandler);
        }

        public PanelIdEnum Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        private void OnClickHandler()
        {
            if (IsClickable)
            {
                if (onClick == null)
                {
                    if (data != null)
                    {
                        //ari//ari ToolTipsManager.Instance.ShowItemTip(data, _context, equipState);
                    }
                    else if (_itemId != 0)
                    {
                        //ari//ari ToolTipsManager.Instance.ShowItemTip(_itemId, _context);
                    }
                }
                else
                {
                    onClick.Invoke();
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            gridBtn.onClick.RemoveListener(OnClickHandler);
        }

        public void SetLoadCallback(Action<GameObject> loadCallback)
        {
            _loadCallback = loadCallback;
        }

        public void SetItemData(int itemId, Action<GameObject> loadCallback = null)
        {
            _itemId = itemId;
            _loadCallback = loadCallback;
            int startLevel = 0;
            if (item_helper.GetQuality(itemId) >= public_config.ITEM_QUALITY_PURPLE)
            {
                startLevel = item_helper.GetStarLevel(itemId);
            }
            SetItemData(item_helper.GetIcon(itemId), item_helper.GetQuality(itemId), startLevel);
        }

        public void SetItemData(BaseItemInfo itemInfo, Action<GameObject> loadCallback = null)
        {
            _loadCallback = loadCallback;
            if (itemInfo == null)
            {
                Clear();
            }
            else
            {
                data = itemInfo;
                _itemId = itemInfo.Id;
                SetItemData(itemInfo.ItemData);
            }
        }

        public void SetItemData(BaseItemData itemData, Action<GameObject> loadCallback = null)
        {
            _loadCallback = loadCallback;
            if (itemData == null)
            {
                Clear();
            }
            else
            {
                _itemId = itemData.Id;
                int starLevel = 0;
                EquipData equipData = itemData as EquipData;
                if (equipData != null && equipData.Quality >= 4)
                {
                    starLevel = equipData.Star;
                }
                SetItemData(itemData.Icon, itemData.Quality, starLevel);
            }
        }

        public void SetItemData(int iconId, int quality, int starLevel = 0)
        {
            if (iconId == 0 && quality == 0)
            {
                Clear();
            }
            else
            {
                RefreshIcon(iconId);
                RefreshQuality(quality);
                RefreshStarLevel(starLevel);
            }
        }

        public void SetGridBtnEnable(bool enable)
        {
            if (gridBtn != null)
            {
                gridBtn.enabled = enable;
            }
        }

        private void RefreshIcon(int iconId)
        {
            if (_iconId != iconId)
            {
                _iconId = iconId;
                MogoAtlasUtils.AddIcon(_icon.gameObject, _iconId, OnIconLoaded);
            }
        }

        private void RefreshQuality(int quality)
        {
            if (_displayer != null)
            {
                _displayer.SetLoadCallback(_loadCallback);
                _displayer.Quality = quality;
            }
        }

        private void OnIconLoaded(GameObject go)
        {
            if (_loadCallback != null)
            {
                _loadCallback(go);
            }
        }

        public void Clear()
        {
            _iconId = 0;
            _itemId = 0;
            _loadCallback = null;
            data = null;
            onClick = null;
            if (_icon != null)
            {
                MogoAtlasUtils.RemoveSprite(_icon.gameObject);
            }
            if (_displayer != null)
            {
                _displayer.Clear();
            }
            RefreshStarLevel(0);
        }

        private void RefreshStarLevel(int starLevel)
        {
            HideAllStar();
            if (IsShowStar == true)
            {
                for (int i = 0; i < starLevel; i++)
                {
                    if (_starGoList[i] == null)
                    {
                        GameObject go = CreateStarGo(i);
                        AddStarImage(go);
                        _starGoList[i] = go;
                    }
                    _starGoList[i].SetActive(true);
                }
                DoStarLayout();
            }
        }

        private void HideAllStar()
        {
            for (int i = 0; i < STAR_COUNT; i++)
            {
                if (_starGoList[i] != null)
                {
                    _starGoList[i].SetActive(false);
                }
            }
        }

        private void AddStarImage(GameObject go)
        {
            ImageWrapper wrapper = go.AddComponent<ImageWrapper>();
            wrapper.material = ImageWrapper.GetMaterial(STAR_ICON_MATERIAL_KEY) as Material;
            wrapper.sprite = ImageWrapper.GetSprite(STAR_ICON_SPRITE_KEY) as Sprite;
            wrapper.color = new Color(1, 1, 1, 0);
        }

        private GameObject CreateStarGo(int index)
        {
            GameObject go = new GameObject(string.Format(STAR_ICON_NAME, index));
            RectTransform rect = go.AddComponent<RectTransform>();
            rect.pivot = new Vector2(0, 1);
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.sizeDelta = new Vector2(_iconRect.sizeDelta.x / 5, _iconRect.sizeDelta.x / 5);
            rect.SetParent(transform, false);
            return go;
        }

        private void DoStarLayout()
        {
            for (int i = 0; i < STAR_COUNT; i++)
            {
                GameObject go = _starGoList[i];
                if (go != null)
                {
                    RectTransform rect = go.GetComponent<RectTransform>();
                    rect.anchoredPosition = new Vector2((STAR_COUNT - i - 1) * rect.sizeDelta.x + _iconRect.localPosition.x, _iconRect.localPosition.y - _iconRect.sizeDelta.y + rect.sizeDelta.y);
                }
            }
        }

    }
}
