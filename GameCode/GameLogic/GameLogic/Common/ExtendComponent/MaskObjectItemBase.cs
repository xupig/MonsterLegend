﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class MaskObjectItemBase : MonoBehaviour
    {
        protected KScrollView _scrollView;
        protected KScrollPage _scrollPage;
        protected RectTransform _contentRect;
        protected GameObject _attachObjectGo;

        protected Vector2 _lastItemPos = Vector2.zero;
        protected Vector2 _lastContentPos = Vector2.zero;

        protected virtual void Awake()
        {
            _scrollView = transform.GetComponentInParent<KScrollView>();
            _scrollPage = transform.GetComponentInParent<KScrollPage>();
            if (_scrollView == null && _scrollPage == null)
            {
                LoggerHelper.Error("MaskObjectItemBase Can not find Scroll View or Scroll page Component !!!");
                return;
            }
            if (_scrollPage != null)
            {
                _contentRect = _scrollPage.GetChildComponent<RectTransform>("mask/content");
            }
            AddEventListener();
        }

        private void OnEnable()
        {
            if (_attachObjectGo == null)
            {
                return;
            }
            ControlMaskObject();
        }

        public void SetAttachObjectGo(GameObject attachObjectGo)
        {
            _attachObjectGo = attachObjectGo;
        }

        /// <summary>
        /// 更新处理一次遮罩特效
        /// </summary>
        public void UpdateMaskEffect()
        {
            ControlMaskObject();
        }

        private void AddEventListener()
        {
            if (_scrollView != null)
            {
                _scrollView.ScrollRect.onValueChanged.AddListener(OnScrollRectMove);
            }
        }

        private void RemoveEventListener()
        {
            if (_scrollView != null)
            {
                _scrollView.ScrollRect.onValueChanged.RemoveListener(OnScrollRectMove);
            }
        }

        private void OnScrollRectMove(Vector2 position)
        {
            //当ScrollView有滑动时，需进行物体的遮罩处理
            ControlMaskObject();
        }

        protected virtual void OnDestroy()
        {
            RemoveEventListener();
        }

        private void Update()
        {
            if (_scrollView != null)
            {
                if (Mathf.Abs(transform.localPosition.x - _lastItemPos.x) < 0.001f && Mathf.Abs(transform.localPosition.y - _lastItemPos.y) < 0.001f)
                {
                    return;
                }
                //当Item的位置有改变时，需进行特效的遮罩处理
                _lastItemPos.x = transform.localPosition.x;
                _lastItemPos.y = transform.localPosition.y;
                ControlMaskObject();
            }
            else if (_scrollPage != null)
            {
                bool isContentMove = !(Mathf.Abs(_contentRect.anchoredPosition.x - _lastContentPos.x) < 0.001f && Mathf.Abs(_contentRect.anchoredPosition.y - _lastContentPos.y) < 0.001f);
                bool isItemMove = !(Mathf.Abs(transform.localPosition.x - _lastItemPos.x) < 0.001f && Mathf.Abs(transform.localPosition.y - _lastItemPos.y) < 0.001f);
                if (!isContentMove && !isItemMove)
                {
                    return;
                }
                if (isContentMove)
                {
                    //当ScrollPage的内容整体移动时，需进行特效的遮罩处理
                    _lastContentPos = _contentRect.anchoredPosition;
                }
                if (isItemMove)
                {
                    _lastItemPos.x = transform.localPosition.x;
                    _lastItemPos.y = transform.localPosition.y;
                }
                ControlMaskObject();
            }
        }

        protected void ControlMaskObject()
        {
            if (!CheckGameObjectValid())
            {
                return;
            }
            RectTransform maskRect = null;
            if (_scrollView != null)
            {
                maskRect = _scrollView.Mask.GetComponent<RectTransform>();
            }
            else if (_scrollPage != null)
            {
                maskRect = _scrollPage.GetChildComponent<RectTransform>("mask");
            }
            RectTransform attachEffectRect = _attachObjectGo.GetComponent<RectTransform>();
            Vector3[] maskWorldCorners = new Vector3[4];
            Vector3[] attachGoWorldCorners = new Vector3[4];
            maskRect.GetWorldCorners(maskWorldCorners);
            attachEffectRect.GetWorldCorners(attachGoWorldCorners);
            Vector3 maskMinPos = maskWorldCorners[0];
            Vector3 maskMaxPos = maskWorldCorners[2];
            Vector3 attachGoMinPos = attachGoWorldCorners[0];
            Vector3 attachGoMaxPos = attachGoWorldCorners[2];
            Vector2 maskMin = new Vector2(maskMinPos.x, maskMinPos.y);
            Vector2 maskMax = new Vector2(maskMaxPos.x, maskMaxPos.y);
            Vector2 attachGoMin = new Vector2(attachGoMinPos.x, attachGoMinPos.y);
            Vector2 attachGoMax = new Vector2(attachGoMaxPos.x, attachGoMaxPos.y);
            if (IsInRect(maskMin, maskMax, attachGoMin, attachGoMax))
            {
                ProcessInRect();
            }
            else
            {
                ProcessOutRect();
            }
        }

        protected virtual bool CheckGameObjectValid()
        {
            if (_scrollView == null && _scrollPage == null)
            {
                return false;
            }
            if (_attachObjectGo == null)
            {
                return false;
            }
            return true;
        }

        protected virtual void ProcessInRect()
        {

        }

        protected virtual void ProcessOutRect()
        {

        }

        private bool IsInRect(Vector2 aMin, Vector2 aMax, Vector2 bMin, Vector2 bMax)
        {
            if (aMin.x <= bMin.x && aMin.y <= bMin.y && aMax.x >= bMax.x && aMax.y >= bMax.y)
            {
                return true;
            }
            return false;
        }

        private bool IsRectIntersect(Vector2 aMin, Vector2 aMax, Vector2 bMin, Vector2 bMax)
        {
            if (aMin.x > bMax.x || aMin.y > bMax.y || bMin.x > aMax.x || bMin.y > aMax.y)
            {
                return false;
            }
            return true;
        }
    }
}
