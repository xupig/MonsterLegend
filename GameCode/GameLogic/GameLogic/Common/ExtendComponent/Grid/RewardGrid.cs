﻿#region 模块信息
/*==========================================
// 文件名：RewardGrid
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.Grid
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 16:27:51
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.ServerConfig;

namespace Common.ExtendComponent
{
    public class RewardGrid : RewardGridBase
    {
        protected override void Awake()
        {
            base.Awake();
            if (_textNum != null)
            {
                _textNum.CurrentText.alignment = TextAnchor.UpperRight;
            }
            if (_textXNum != null)
            {
                _textXNum.CurrentText.alignment = TextAnchor.UpperRight;
            }
            if (_textName != null)
            {
                _textName.CurrentText.alignment = TextAnchor.UpperCenter;
            }
        }
    }

    public class RewardGridBase : KList.KListItemBase
    {
        private int _id;

        protected StateText _textNum;
        protected StateText _textXNum;
        protected StateText _textName;
        protected ItemGrid _itemGrid;
        protected StateImage _imageRareIcon;

        protected override void Awake()
        {
            InitNum();
            InitXNum();
            InitName();
            InitIcon();
            InitRareIcon();
            _itemGrid.onClick = OnClick;
        }

        private void InitNum()
        {
            GameObject go = GetChild("Label_txtNum");
            if (go != null)
            {
                _textNum = go.GetComponent<StateText>();
            }
        }

        private void InitXNum()
        {
            GameObject go = GetChild("Label_txtXNum");
            if (go != null)
            {
                _textXNum = go.GetComponent<StateText>();
            }
        }

        private void InitName()
        {
            GameObject go = GetChild("Label_txtName");
            if (go != null)
            {
                _textName = go.GetComponent<StateText>();
            }
        }

        private void InitIcon()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        private void InitRareIcon()
        {
            GameObject go = GetChild("Image_xiyouIcon");
            if (go != null)
            {
                _imageRareIcon = go.GetComponent<StateImage>();
            }
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                RewardData data = value as RewardData;
                Refresh(data);
                if (data != null)
                {
                    _id = data.id;
                    RewardData.ReturnRewardData(data);
                }
                else
                {
                    _id = 0;
                }
            }
        }

        protected void Refresh(RewardData data)
        {
            if (data != null)
            {
                RefreshNum(data);
                RefreshXNum(data);
                RefreshName(data);
                RefreshIcon(data);
                RefreshRareIcon(data);
                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private void RefreshXNum(RewardData data)
        {
            if (_textXNum != null)
            {
                if (data == null)
                {
                    _textXNum.Clear();
                }
                else
                {
                    _textXNum.ChangeAllStateText(data.num > 1 ? string.Format("x{0}", data.num.ToString()) : "");
                }
            }
        }

        private void RefreshNum(RewardData data)
        {
            if (_textNum != null)
            {
                if (data == null)
                {
                    _textNum.Clear();
                }
                else
                {
                    _textNum.ChangeAllStateText(data.num > 1 ? data.num.ToString() : "");
                }
            }
        }

        private void RefreshName(RewardData data)
        {
            if (_textName != null)
            {
                if (data == null)
                {
                    _textName.Clear();
                }
                else
                {
                    _textName.ChangeAllStateText(item_helper.GetName(data.id));
                    _textName.CurrentText.color = item_helper.GetColor(data.id);
                }
            }
        }

        private void RefreshIcon(RewardData data)
        {
            if (data != null)
            {
                _itemGrid.SetItemData(data.id);
            }
        }

        private void RefreshRareIcon(RewardData data)
        {
            if (_imageRareIcon != null)
            {
                _imageRareIcon.gameObject.SetActive(item_helper.IsRare(data.id));
            }
        }

        private void OnClick()
        {
            /*
            if (_id != 0)
            {
                BagItemType itemType = item_helper.GetItemType(_id);
                if (itemType == BagItemType.Rune)
                {
                    rune rune = rune_helper.GetRune(_id);
                    RuneTipsData bagData = new RuneTipsData();
                    bagData.pos = _id;
                    bagData.toPos = -1;
                    bagData.bagType = BagType.Undefined;
                    if (rune.__subtype == public_config.RUNE_EXP || rune.__subtype == public_config.RUNE_MONEY)
                    {
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.ExpRuneToolTips, bagData, PanelIdEnum.Rune);
                    }
                    else
                    {
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.RuneToolTips, bagData, PanelIdEnum.Rune);
                    }
                }
                else
                {
                   //ari ToolTipsManager.Instance.ShowItemTip(_id, PanelIdEnum.Empty);
                }
            }*/
        }

    }
}
