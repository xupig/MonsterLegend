﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class RewardItemList : KContainer
    {
        private List<RewardItem> _rewardList;

        protected override void Awake()
        {
            _rewardList = new List<RewardItem>();
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject child = GetChild(i).gameObject;
                _rewardList.Add(child.AddComponent<RewardItem>());
            }
        }

        public void SetData(int rewardId)
        {
            List<BaseItemData> itemDataList = item_reward_helper.GetItemDataList(rewardId);
            SetData(itemDataList);
        }

        public void SetData(List<BaseItemData> itemDataList)
        {
            for (int i = 0; i < _rewardList.Count; i++)
            {
                RewardItem rewardItem = _rewardList[i];
                if (i < itemDataList.Count)
                {
                    rewardItem.Visible = true;
                    rewardItem.Data = itemDataList[i];
                }
                else
                {
                    rewardItem.Visible = false;
                }
            }
        }
    }
}
