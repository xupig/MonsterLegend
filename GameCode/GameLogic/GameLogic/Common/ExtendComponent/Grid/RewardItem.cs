﻿using Common.Data;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class RewardItem : KList.KListItemBase
    {
        protected BaseItemData _data;
        private bool _isDimensionDirty;
        private bool _isIconLoaded;

        private StateText _textNum;
        private StateText _textXNum;
        private StateText _textName;
        private ItemGrid _itemGrid;

        protected override void Awake()
        {
            InitNum();
            InitXNum();
            InitName();
            InitIcon();
        }

        private void InitNum()
        {
            GameObject go = GetChild("Label_txtNum");
            if (go != null)
            {
                _textNum = go.GetComponent<StateText>();
            }
        }

        private void InitXNum()
        {
            GameObject go = GetChild("Label_txtXNum");
            if (go == null)
            {
                go = GetChild("Label_txtShuliang");
            }
            if (go != null)
            {
                _textXNum = go.GetComponent<StateText>();
            }
        }

        private void InitName()
        {
            GameObject go = GetChild("Label_txtName");
            if (go != null)
            {
                _textName = go.GetComponent<StateText>();
            }
        }

        private void InitIcon()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                BaseItemData data = value as BaseItemData;
                _data = data;
                _isDimensionDirty = false;
                _isIconLoaded = false;
                RefreshNum(data);
                RefreshXNum(data);
                RefreshName(data);
                RefreshIcon(data);
            }
        }

        private void RefreshXNum(BaseItemData data)
        {
            if (_textXNum != null)
            {
                if (data == null)
                {
                    _textXNum.Clear();
                }
                else
                {
                    _textXNum.ChangeAllStateText(data.StackCount > 1 ? string.Format("x{0}", data.StackCount.ToString()) : "");
                }
            }
        }

        private void RefreshNum(BaseItemData data)
        {
            if (_textNum != null)
            {
                if (data == null)
                {
                    _textNum.Clear();
                }
                else
                {
                    _textNum.ChangeAllStateText(data.StackCount > 1 ? data.StackCount.ToString() : "");
                }
            }
        }

        private void RefreshName(BaseItemData data)
        {
            if (_textName != null)
            {
                if (data == null)
                {
                    _textName.Clear();
                }
                else
                {
                    _textName.ChangeAllStateText(item_helper.GetName(data.Id));
                    _textName.CurrentText.color = item_helper.GetColor(data.Id);
                }
            }
        }

        private void RefreshIcon(BaseItemData data)
        {
            if (data != null)
            {
                _itemGrid.SetItemData(data.Id, IconLoaded);
            }
        }

        public void SetDimensionDirty()
        {
            _isDimensionDirty = true;
            SetAllDimensionDirty();
        }

        private void IconLoaded(GameObject icon)
        {
            _isIconLoaded = true;
            SetAllDimensionDirty();
        }

        private void SetAllDimensionDirty()
        {
            if(_isDimensionDirty && _isIconLoaded)
            {
                if (_textName != null)
                {
                    TextWrapper[] array = _textName.gameObject.GetComponentsInChildren<TextWrapper>();
                    for (int i = 0; i < array.Length; i++)
                    {
                        array[i].SetContinuousDimensionDirty(true);
                    }
                }
                if (_textNum != null)
                {
                    TextWrapper[] array = _textNum.gameObject.GetComponentsInChildren<TextWrapper>();
                    for (int i = 0; i < array.Length; i++)
                    {
                        array[i].SetContinuousDimensionDirty(true);
                    }
                }
                if (_textXNum != null)
                {
                    TextWrapper[] array = _textXNum.gameObject.GetComponentsInChildren<TextWrapper>();
                    for (int i = 0; i < array.Length; i++)
                    {
                        array[i].SetContinuousDimensionDirty(true);
                    }
                }

                ImageWrapper[] imageArray = _itemGrid.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < imageArray.Length; i++)
                {
                    imageArray[i].SetContinuousDimensionDirty(true);
                }
            }
        }
    }
}
