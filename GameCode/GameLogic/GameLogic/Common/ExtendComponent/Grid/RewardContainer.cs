﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class RewardContainer : KContainer
    {
        protected StateText _textXNum;
        protected ItemGrid _itemGrid;

        protected override void Awake()
        {
            InitXNum();
            InitItemGrid();
        }

        private void InitXNum()
        {
            GameObject go = GetChild("Label_txtXNum");
            if (go != null)
            {
                _textXNum = go.GetComponent<StateText>();
            }
        }

        private void InitItemGrid()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public void SetData(RewardData data)
        {
            Refresh(data);
            if (data != null)
            {
                RewardData.ReturnRewardData(data);
            }
        }

        protected void Refresh(RewardData data)
        {
            if (data != null)
            {
                RefreshXNum(data);
                RefreshItem(data);
                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private void RefreshXNum(RewardData data)
        {
            if (_textXNum != null)
            {
                if (data == null || data.num < 2)
                {
                    _textXNum.Clear();
                }
                else
                {
                    _textXNum.ChangeAllStateText(string.Format("x{0}", data.num.ToString()));
                }
            }
        }

        private void RefreshItem(RewardData data)
        {
            if (data != null)
            {
                _itemGrid.SetItemData(data.id);
            }
        }

    }
}
