﻿#region 模块信息
/*==========================================
// 文件名：AwardItemData
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/22 17:23:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using System.Collections.Generic;
namespace Common.ExtendComponent.GeneralRankingList
{
    public class AwardItemData
    {
        public int index;

        public int rankMin;

        public int rankMax;

        public List<BaseItemData> awardDetaiItemlList;

        public int type;

        public AwardItemData(int type, int index, int rankMin, int rankMax, List<BaseItemData> awardDetailList)
        {
            this.type = type;
            this.index = index;
            this.rankMin = rankMin;
            this.rankMax = rankMax;
            this.awardDetaiItemlList = awardDetailList;
        }
    }
}