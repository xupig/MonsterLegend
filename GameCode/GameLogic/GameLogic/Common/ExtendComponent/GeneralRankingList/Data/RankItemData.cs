﻿#region 模块信息
/*==========================================
// 文件名：RankItemData
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.Data
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/29 16:02:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs.ProtoBuf;
using GameData;
using MogoEngine.Utils;
namespace Common.ExtendComponent.GeneralRankingList
{
    public class RankItemData
    {
        public RankType rankType;
        public int rank_index;
        public ulong dbid;

        public string firstColumnString;
        public string secondColumnString;
        public string thirdColumnString;
        public string fourthColumnString;
        public string fifthColumnString;

        public RankItemData(RankType type, PbRankAvatarInfo pbRankAvatarInfo)
        {
            dbid = pbRankAvatarInfo.dbid;
            rankType = type;
            switch ((RankType)rankType)
            {
                case RankType.fightRank:
                    SetFightRankData(pbRankAvatarInfo);
                    break;
                case RankType.levelRank:
                    SetLevelRankData(pbRankAvatarInfo);
                    break;
                case RankType.demonGateDayRank:
                    SetDemonRankData(pbRankAvatarInfo);
                    break;
                case RankType.demonGateWeekRank:
                    SetDemonRankData(pbRankAvatarInfo);
                    break;
                case RankType.findTreasureRank:
                    SetFindTreasureData(pbRankAvatarInfo);
                    break;
                case RankType.pkRanking:
                    SetFindTreasureData(pbRankAvatarInfo);
                    break;
            }
        }

        private void SetFindTreasureData(PbRankAvatarInfo pbRankAvatarInfo)
        {
            rank_index = (int)pbRankAvatarInfo.rank_index;
            firstColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.name_bytes);
            secondColumnString = pbRankAvatarInfo.score.ToString();
            thirdColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.guild_name_bytes);
            fourthColumnString = pbRankAvatarInfo.rank_worship.ToString();
        }

        public RankItemData(RankType type, PbRankGuildInfo pbRankGuildInfo)
        {
            rankType = type;
            SetGuildRankData(type, pbRankGuildInfo);
        }

        private void SetGuildRankData(RankType type, PbRankGuildInfo pbRankGuildInfo)
        {
            rank_index = (int)pbRankGuildInfo.rank_index;
            firstColumnString = MogoProtoUtils.ParseByteArrToString(pbRankGuildInfo.guild_name_bytes);
            secondColumnString = pbRankGuildInfo.guild_level.ToString();
            thirdColumnString = MogoProtoUtils.ParseByteArrToString(pbRankGuildInfo.leader_name_bytes);
            if (type == RankType.guildRank)
            {
                fourthColumnString = pbRankGuildInfo.guild_member_num.ToString();
            }
            else if(type == RankType.eveningActivity)
            {
                fourthColumnString = pbRankGuildInfo.score.ToString();
            }
        }

        private void SetDemonRankData(PbRankAvatarInfo pbRankAvatarInfo)
        {
            rank_index = (int)pbRankAvatarInfo.rank_index;
            firstColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.name_bytes);
            secondColumnString = pbRankAvatarInfo.evil_score.ToString();
            SetGuildTxt(pbRankAvatarInfo);
            fourthColumnString = pbRankAvatarInfo.rank_worship.ToString();
        }

        private void SetLevelRankData(PbRankAvatarInfo pbRankAvatarInfo)
        {
            rank_index = (int)pbRankAvatarInfo.rank_index;
            firstColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.name_bytes);
            secondColumnString = pbRankAvatarInfo.level.ToString();
            SetGuildTxt(pbRankAvatarInfo);
            fourthColumnString = pbRankAvatarInfo.rank_worship.ToString();
        }

        private void SetFightRankData(PbRankAvatarInfo pbRankAvatarInfo)
        {
            rank_index = (int)pbRankAvatarInfo.rank_index;
            firstColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.name_bytes);
            secondColumnString = pbRankAvatarInfo.fight.ToString();
            SetGuildTxt(pbRankAvatarInfo);
            fourthColumnString = pbRankAvatarInfo.rank_worship.ToString();
        }

        private void SetGuildTxt(PbRankAvatarInfo pbRankAvatarInfo)
        {
            if (pbRankAvatarInfo.guild_name_bytes == null || pbRankAvatarInfo.guild_name_bytes.Length == 0)
            {
                thirdColumnString = rank_helper.GetNotJoinAnyGuildString();
            }
            else
            {
                thirdColumnString = MogoProtoUtils.ParseByteArrToString(pbRankAvatarInfo.guild_name_bytes);
            }
        }
    }
}