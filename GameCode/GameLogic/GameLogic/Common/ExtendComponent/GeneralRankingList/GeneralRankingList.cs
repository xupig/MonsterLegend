﻿#region 模块信息
/*==========================================
// 文件名：RankingList
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.RankingList
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/21 16:21:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using GameLoader.Utils;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class GeneralRankingList : KContainer
    {
        private RankingView _rankingView;
        private AwardView _awardView;
        private RankType currentType;

        protected override void Awake()
        {
            base.Awake();
            _rankingView = AddChildComponent<RankingView>("Container_paihangbang");
            _awardView = AddChildComponent<AwardView>("Container_jiangli");
            AddEventListener();
        }

        private void AddEventListener()
        {
            _rankingView.showAwardClick.AddListener(OpenAwardView);
            _awardView.backClick.AddListener(OpenRankView);
        }

        private void RemoveEventListener()
        {
            _rankingView.showAwardClick.RemoveListener(OpenAwardView);
            _awardView.backClick.RemoveListener(OpenRankView);
        }

        private void OpenAwardView()
        {
            _awardView.Refresh(currentType);
            _rankingView.Hide();
        }

        private void OpenRankView()
        {
            _rankingView.Refresh(currentType);
            _awardView.Hide();
        }

        public void Refresh(RankType type)
        {
            currentType = type;
            OpenRankView();
        }

        public void Hide()
        {
            Visible = false;
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}