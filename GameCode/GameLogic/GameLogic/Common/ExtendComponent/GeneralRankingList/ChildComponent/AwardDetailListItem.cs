﻿#region 模块信息
/*==========================================
// 文件名：AwardDetailListItem
// 命名空间: ModuleRankingList
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/16 15:32:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using ModuleCommonUI;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class AwardDetailListItem : KList.KListItemBase
    {
        private StateText _numTxt;
        private ItemGrid _itemGrid;

        private BaseItemData _data;
        
        protected override void Awake()
        {
            _numTxt = GetChildComponent<StateText>("Label_txtShuliang");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            onClick.AddListener(OnItemClick);
        }

        private void OnItemClick(KList.KListItemBase target, int index)
        {
            //ari//ari ToolTipsManager.Instance.ShowItemTip(_data.Id , PanelIdEnum.RankingList, false);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as BaseItemData;
                    RefreshItemView();
                }
            }
        }

        private void RefreshItemView()
        {
            _numTxt.CurrentText.text = _data.StackCount.ToString();
            _itemGrid.Clear();
            _itemGrid.SetItemData(_data);
        }

        public override void Dispose()
        {
        
        }

    }
}
