﻿#region 模块信息
/*==========================================
// 文件名：RankItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/22 17:21:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class RankItem : KList.KListItemBase
    {
        private KContainer _rankContainer;
        private KContainer _backgroundContainer;
        private StateText _secondColumnTxt;
        private StateText _thirdColumnTxt;
        private StateText _fourthColumnTxt;
        private StateText _fifthColumnTxt;

        private RankItemData _data;

        public static RankType rankingListType;

        protected override void Awake()
        {
            _rankContainer = GetChildComponent<KContainer>("Container_mingci");
            _backgroundContainer = GetChildComponent<KContainer>("Container_bg");
            _secondColumnTxt = GetChildComponent<StateText>("Label_txtWanjia");
            _thirdColumnTxt = GetChildComponent<StateText>("Label_txtDengji");
            _fourthColumnTxt = GetChildComponent<StateText>("Label_txtZhandouli");
            _fifthColumnTxt = GetChildComponent<StateText>("Label_txtFensi");
        }

        private void OnItemClicked(KList.KListItemBase target, int index)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.RankingListDetail, _data.dbid);
        }

        private void AddEventListener()
        {
            onClick.AddListener(OnItemClicked);
        }

        private void RemoveEventListener()
        {
            onClick.RemoveListener(OnItemClicked);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as RankItemData;
                    rankingListType = _data.rankType;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            if (rank_helper.IsOperational((RankType)_data.rankType))
            {
                AddEventListener();
            }
            RefreshBackground();
            RefreshRankIcon();
            RefreshItemContent();
        }

        private void RefreshItemContent()
        {
            _secondColumnTxt.CurrentText.text = _data.firstColumnString;
            _thirdColumnTxt.CurrentText.text = _data.secondColumnString;
            _fourthColumnTxt.CurrentText.text = _data.thirdColumnString;
            _fifthColumnTxt.CurrentText.text = _data.fourthColumnString;
        }

        private void RefreshRankIcon()
        {
            StateText rankNumTxt = _rankContainer.GetChildComponent<StateText>("Label_txtPaiming");
            rankNumTxt.gameObject.SetActive(true);
            StateImage firstIcon = _rankContainer.GetChildComponent<StateImage>("Image_firstLcon");
            StateImage secondIcon = _rankContainer.GetChildComponent<StateImage>("Image_secondLcon");
            StateImage thirdIcon = _rankContainer.GetChildComponent<StateImage>("Image_thirdLcon");
            rankNumTxt.CurrentText.text = _data.rank_index > 3 ? _data.rank_index.ToString() : string.Empty;
            firstIcon.gameObject.SetActive(_data.rank_index == 1);
            secondIcon.gameObject.SetActive(_data.rank_index == 2);
            thirdIcon.gameObject.SetActive(_data.rank_index == 3);
        }

        private void RefreshBackground()
        {
            StateImage bgImage_1 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg");
            StateImage bgImage_2 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg02");

            bgImage_1.gameObject.SetActive(_data.rank_index % 2 == 1);
            bgImage_2.gameObject.SetActive(_data.rank_index % 2 == 0);
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }
    }
}