﻿#region 模块信息
/*==========================================
// 文件名：AwardItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/22 17:22:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class AwardItem : KList.KListItemBase
    {
        private KContainer _rankContainer;
        private KContainer _backgroundContainer;
        private KList _detailList;
        private StateText _hasGainedTxt;
        private StateText _NotGainedTxt;
        private StateText _rankNumTxt;

        private string _hasGainTemplate;
        
        private AwardItemData _data;

        public static RankType type;

        private string NotGainTxt
        {
            get
            {
                if (type == RankType.eveningActivity)
                {
                    return MogoLanguageUtil.GetContent(65342);
                }
                else
                {
                    return MogoLanguageUtil.GetContent(6090005);
                }
            }
        }

        protected override void Awake()
        {
            _rankContainer = GetChildComponent<KContainer>("Container_mingci");
            _backgroundContainer = GetChildComponent<KContainer>("Container_bg");
            _detailList = GetChildComponent<KList>("List_jiangli");
            _hasGainedTxt = _rankContainer.GetChildComponent<StateText>("Label_txtHuode");
            _NotGainedTxt = _rankContainer.GetChildComponent<StateText>("Label_txtWeiHuode");
            _rankNumTxt = _rankContainer.GetChildComponent<StateText>("Label_txtPaiming");
            InitAwardDetailList();
        }

        private void InitAwardDetailList()
        {
            _detailList.SetGap(0, 15);
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as AwardItemData;
                    type = (RankType)_data.type;
                    RefreshItemView();
                }
            }
        }

        private void RefreshItemView()
        {
            RefreshBackground();
            RefreshRankIcon();
            RefreshDetailList();
            RefreshGainTxt();
        }

        private void RefreshGainTxt()
        {
            _hasGainedTxt.Visible = true;
            _NotGainedTxt.Visible = true;
            _hasGainTemplate = _hasGainedTxt.CurrentText.text;
            if (_data.rankMin > 3)
            {
                _hasGainedTxt.CurrentText.text = string.Empty;
                _NotGainedTxt.CurrentText.text = string.Empty;
            }
            else
            {
                if (RankingListManager.Instance.rankInfo.hasGainAwardPlayerDict.ContainsKey(type) == false)
                {
                    _NotGainedTxt.CurrentText.text = NotGainTxt;
                    _hasGainedTxt.CurrentText.text = string.Empty;
                    return;
                }
                string playerName = RankingListManager.Instance.rankInfo.hasGainAwardPlayerDict[type][_data.rankMin - 1];
                if (playerName == string.Empty)
                {
                    _hasGainedTxt.CurrentText.text = string.Empty;
                    _NotGainedTxt.CurrentText.text = NotGainTxt;
                }
                else
                {
                    _hasGainedTxt.CurrentText.text = string.Format(_hasGainTemplate, playerName);
                    _NotGainedTxt.CurrentText.text = string.Empty;
                }
            }
        }

        private void RefreshDetailList()
        {
            _detailList.RemoveAll();
            _detailList.SetDataList<AwardDetailListItem>(_data.awardDetaiItemlList);
        }

        private void RefreshRankIcon()
        {
            StateImage _firstIcon = _rankContainer.GetChildComponent<StateImage>("Image_firstLcon");
            StateImage _secondIcon = _rankContainer.GetChildComponent<StateImage>("Image_secondLcon");
            StateImage _thirdIcon = _rankContainer.GetChildComponent<StateImage>("Image_thirdLcon");

            _rankNumTxt.gameObject.SetActive(true);

            if (_data.rankMin <= 3)
            {
                _rankNumTxt.CurrentText.text = string.Empty;
            }
            else if (_data.rankMin == _data.rankMax)
            {
                _rankNumTxt.CurrentText.text = _data.rankMin.ToString();
            }
            else
            {
                _rankNumTxt.CurrentText.text = string.Format("{0}-{1}", _data.rankMin, _data.rankMax);
            }

            _firstIcon.gameObject.SetActive(_data.rankMin == 1);
            _secondIcon.gameObject.SetActive(_data.rankMin == 2);
            _thirdIcon.gameObject.SetActive(_data.rankMin == 3);
        }

        private void RefreshBackground()
        {
            StateImage bgImage_1 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg01");
            StateImage bgImage_2 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg02");

            bgImage_1.gameObject.SetActive(_data.index % 2 == 1);
            bgImage_2.gameObject.SetActive(_data.index % 2 == 0);
        }
    }
}