﻿#region 模块信息
/*==========================================
// 文件名：RankingUpperView
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.ChildView.RankingSubView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/1 13:54:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.Utils;
using GameMain.Entities;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class RankingUpperView : KContainer
    {
        private KContainer _myRankingContainer;
        private KContainer _myAttributeContainer;
        private StateText _myRankingTxt;
        private StateText _myAttributeValueTxt;
        private string _myRankingTemplate;
        private string _myAttributeTemplate;

        public int currentRanking;

        protected override void Awake()
        {
            _myRankingContainer = GetChildComponent<KContainer>("Container_paiming");
            _myRankingTxt = _myRankingContainer.GetChildComponent<StateText>("Label_txtWodepaiming");
            _myRankingTemplate = _myRankingTxt.CurrentText.text;

            _myAttributeContainer = GetChildComponent<KContainer>("Container_wodeshuju");
            _myAttributeValueTxt = _myAttributeContainer.GetChildComponent<StateText>("Label_txtWodepaihangbang");
            _myAttributeTemplate = _myAttributeValueTxt.CurrentText.text;
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbRankInfo>(RankingListEvents.GET_MY_RANKING, RefrehMyRank);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbRankInfo>(RankingListEvents.GET_MY_RANKING, RefrehMyRank);
        }

        public void Refresh(RankType type)
        {
            RankingListManager.Instance.RequestMyRanking((int)type);
        }

        private void RefrehMyRank(PbRankInfo pbRankInfo)
        {
            currentRanking = (int)pbRankInfo.ranking;
            SetRankingTxt((RankType)pbRankInfo.type ,(int)pbRankInfo.ranking);
            SetAttributeTxt((RankType)pbRankInfo.type, (int)pbRankInfo.ranking ,(int)pbRankInfo.ranking_value);
        }

        private void SetRankingTxt(RankType rankType, int value)
        {
            if (value >= 1 && value <= 100)
            {
                _myRankingTxt.CurrentText.text = string.Format(_myRankingTemplate, MogoLanguageUtil.GetContent(65200), value.ToString());
            }
            else
            {
                _myRankingTxt.CurrentText.text = MogoLanguageUtil.GetContent(65201);
            }
        }

        private void SetAttributeTxt(RankType rankType, int ranking, int value)
        {
            if (ranking >= 1 && ranking <= 100)
            {
                _myAttributeContainer.Visible = true;
                if (rankType == RankType.guildRank)
                {
                    _myAttributeValueTxt.CurrentText.text = string.Format(_myAttributeTemplate, rank_helper.GetRankAttributeDesc((int)rankType), PlayerAvatar.Player.guild_name);
                }
                else
                {
                    _myAttributeValueTxt.CurrentText.text = string.Format(_myAttributeTemplate, rank_helper.GetRankAttributeDesc((int)rankType), value.ToString());
                }
            }
            else
            {
                _myAttributeContainer.Visible = false;
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}