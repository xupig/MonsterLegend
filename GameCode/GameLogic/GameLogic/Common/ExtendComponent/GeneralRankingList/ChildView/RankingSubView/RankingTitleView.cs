﻿#region 模块信息
/*==========================================
// 文件名：RankingTitleView
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.GeneralRankingList.ChildView.RankingSubView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/1 14:11:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine;
namespace Common.ExtendComponent.GeneralRankingList
{
    public class RankingTitleView : KContainer
    {
        private StateText[] _rowLabelArray = new StateText[5];

        protected override void Awake()
        {
            for (int i = 0; i < _rowLabelArray.Length; i++)
            {
                _rowLabelArray[i] = GetChildComponent<StateText>("Label_text0" + (i + 1));
                _rowLabelArray[i].ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            }
        }

        public void Refresh(RankType rankType)
        {
            int type = (int)rankType;
            List<string> allTitles = rank_helper.GetAllRowNames(type);
            for (int i = 0; i < _rowLabelArray.Length; i++)
            {
                _rowLabelArray[i].CurrentText.text = allTitles[i];
            }
        }
    }
}