﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class AwardView : KContainer
    {
        public KComponentEvent backClick = new KComponentEvent();

        private StateText _descTxt;
        private StateText _myRankTxt;
        private KList _myAwardList;
        private KScrollView _awardScrollView;
        private KList _awardList;
        private KButton _backBtn;

        private string myRankTemplate;
        public int awardType;
        private List<BaseItemData> awardDataList;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtGengxinshijian");
            _myRankTxt = GetChildComponent<StateText>("Container_wodeshuju/Label_txtWodepaiming");
            myRankTemplate = _myRankTxt.CurrentText.text;
            _myAwardList = GetChildComponent<KList>("Container_wodeshuju/List_wodejiangpin");
            _awardScrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _awardList = _awardScrollView.GetChildComponent<KList>("mask/content");
            _backBtn = GetChildComponent<KButton>("Button_fanhui");
            InitAwardList();
            AddEventListener();
        }

        private void InitAwardList()
        {
            _awardList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _awardList.SetGap(0, 5);
            _myAwardList.SetGap(0, 15);
        }

        public void Hide()
        {

            Visible = false;
        }

        private void AddEventListener()
        {
            _backBtn.onClick.AddListener(OnBackButtonClicked);
        }

        private void RemoveEventListener()
        {
            _backBtn.onClick.RemoveListener(OnBackButtonClicked);
        }

        private void OnBackButtonClicked()
        {
            Hide();
            backClick.Invoke();
        }

        public void Refresh(RankType rankType)
        {
            Visible = true;
            int type = (int)rankType;
            AwardItem.type = rankType;
            awardType = type;
            RefreshMyRank(rankType);
            RefreshAwardList(type);
            RefreshAwardDesc(type);
        }

        private void RefreshAwardDesc(int type)
        {
            _descTxt.CurrentText.text = rank_helper.GetAwardDesc(type);
        }

        private void RefreshMyRank(RankType type)
        {
            int ranking = RankingListManager.Instance.rankInfo.GetMyRank(type);
            _myAwardList.RemoveAll();
            awardDataList = rank_helper.GetAwardByRank((int)type, ranking);
            if (awardDataList == null || awardDataList.Count == 0)
            {
                _myRankTxt.CurrentText.text = string.Format(myRankTemplate, MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_OUT_OF_RANK));
                return;
            }
            _myRankTxt.CurrentText.text = string.Format(myRankTemplate, ranking);
            _myAwardList.RemoveAll();
            _myAwardList.SetDataList<AwardDetailListItem>(awardDataList);
        }

        private void RefreshAwardList(int type)
        {
            List<AwardItemData> allAwardList = rank_helper.GetAllAwardData(type);
            _awardList.RemoveAll();
            _awardScrollView.ResetContentPosition();
            _awardList.SetDataList<AwardItem>(allAwardList);
        }
    }
}
