﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace Common.ExtendComponent.GeneralRankingList
{
    public class RankingView : KContainer
    {
        private RankingUpperView _upperView;
        private RankingTitleView _titleView;
        private KScrollView _rankScrollView;
        private KList _rankList;
        private StateText _rankDescTxt;
        private StateText _noDataTxt;
        private string _noDataString;

        private KButton _showAwardBtn;
        public KComponentEvent showAwardClick = new KComponentEvent();

        private RankType currentRankType;

        private const int MAXCount = 100;
        private int currentItemCount = 0;
        private int itemAddStep = 10;

        protected override void Awake()
        {
            _upperView = AddChildComponent<RankingUpperView>("Container_wodepaiming");
            _titleView = AddChildComponent<RankingTitleView>("Container_txtTitle");
            _rankScrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _rankList = _rankScrollView.GetChildComponent<KList>("mask/content");
            _rankDescTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _noDataTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _noDataString = _noDataTxt.CurrentText.text;
            _showAwardBtn = _upperView.GetChildComponent<KButton>("Button_chakanjiangli");
            InitRankList();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        private void InitRankList()
        {
            _rankList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _rankList.SetGap(0, 0);
        }

        public void Hide()
        {
            Visible = false;
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _showAwardBtn.onClick.AddListener(OnShowAwardBtnClicked);
            _rankScrollView.ScrollRect.onRequestNext.AddListener(RequestMoreRankData);
            EventDispatcher.AddEventListener<RankType>(RankingListEvents.GET_GUILD_RANK, RefreshRankContent);
            EventDispatcher.AddEventListener<RankType>(RankingListEvents.GET_RANKLIST_DATA, RefreshRankContent);
            EventDispatcher.AddEventListener<RankType>(RankingListEvents.GET_DEMON_RANK_DATA, RefreshDemonContent);
            EventDispatcher.AddEventListener<ulong>(RankingListEvents.REFRESH_ONE_PLAYER_WORSHIP_DATA, RefreshOnePlayerData);
        }

        private void RemoveEventListener()
        {
            _showAwardBtn.onClick.RemoveListener(OnShowAwardBtnClicked);
            _rankScrollView.ScrollRect.onRequestNext.RemoveListener(RequestMoreRankData);
            EventDispatcher.RemoveEventListener<RankType>(RankingListEvents.GET_GUILD_RANK, RefreshRankContent);
            EventDispatcher.RemoveEventListener<RankType>(RankingListEvents.GET_RANKLIST_DATA, RefreshRankContent);
            EventDispatcher.RemoveEventListener<RankType>(RankingListEvents.GET_DEMON_RANK_DATA, RefreshDemonContent);
            EventDispatcher.RemoveEventListener<ulong>(RankingListEvents.REFRESH_ONE_PLAYER_WORSHIP_DATA, RefreshOnePlayerData);
        }

        private void RefreshDemonContent(RankType rankType)
        {
            currentRankType = rankType;
            RefreshRankContentFromCacheData();
        }

        private void RefreshOnePlayerData(ulong playerDbId)
        {
            for (int i = 0; i < _rankList.ItemList.Count; i++)
            {
                RankItem item = _rankList.ItemList[i] as RankItem;
                RankItemData itemData = item.Data as RankItemData;
                if (itemData.dbid == playerDbId)
                {
                    PbRankAvatarInfo info = RankingListManager.Instance.rankInfo.GetPlayerRankAvatarInfo(currentRankType, playerDbId);
                    if (info != null)
                    {
                        item.Data = new RankItemData(currentRankType, info);
                    }
                }
            }
        }

        private void OnShowAwardBtnClicked()
        {
            showAwardClick.Invoke();
        }

        public void Refresh(RankType type)
        {
            Visible = true;
            currentRankType = type;
            _upperView.Refresh(type);
            _titleView.Refresh(type);
            ShowAwardBtn(type);
            RefreshRankDesc(type);
            RefreshRankContent(type);
        }

        private void RequestMoreRankData(KScrollRect arg0)
        {
            List<RankItemData> list;
            if (currentItemCount >= MAXCount)
            {
                return;
            }
            else
            {
                list = GetRankItemData(currentRankType, currentItemCount, currentItemCount + itemAddStep);
                currentItemCount += list.Count;
                _rankList.SetDataList<RankItem>(list);
            }
        }

        private void ShowAwardBtn(RankType type)
        {
            _showAwardBtn.gameObject.SetActive(rank_helper.IsAwardRankType(type));
        }

        private void RefreshRankDesc(RankType type)
        {
            _rankDescTxt.CurrentText.text = rank_helper.GetRankingDesc((int)type);
        }

        private void RefreshRankContent(RankType rankType)
        {
            // 如果是恶魔日榜或者恶魔周榜，则每次都会刷新
            if (rankType == RankType.demonGateDayRank || rankType == RankType.demonGateWeekRank)
            {
                RankingListManager.Instance.RequestTenMinutesRankListData((int)rankType);
                return;
            }
            if (RankingListManager.Instance.rankInfo.HasRankData(rankType) == true)
            {
                RefreshRankContentFromCacheData();
            }
            else
            {
                RankingListManager.Instance.RequestTenMinutesRankListData((int)rankType);
            }
        }

        private void RefreshRankContentFromCacheData()
        {
            _rankScrollView.ResetContentPosition();
            _rankList.RemoveAll();
            currentItemCount = 0;
            List<RankItemData> list = GetRankItemData(currentRankType, 0, currentItemCount + itemAddStep);
            currentItemCount += list.Count;
            SetNoRankDataTxt(list.Count);
            _rankList.SetDataList<RankItem>(list);
        }

        private void SetNoRankDataTxt(int count)
        {
            if (count > 0)
            {
                _noDataTxt.CurrentText.text = string.Empty;
            }
            else
            {
                _noDataTxt.CurrentText.text = _noDataString;
            }
        }

        private List<RankItemData> GetRankItemData(RankType type, int start, int end)
        {
            List<RankItemData> result = new List<RankItemData>();
            if (type == RankType.guildRank || type == RankType.eveningActivity)
            {
                List<PbRankGuildInfo> guildList = RankingListManager.Instance.rankInfo.GetGuildRankContent(type, start, end);
                for (int i = 0; i < guildList.Count; i++)
                {
                    PbRankGuildInfo pb = guildList[i];
                    result.Add(new RankItemData(type, pb));
                }
                return result;
            }
            else
            {
                List<PbRankAvatarInfo> avatarList = RankingListManager.Instance.rankInfo.GetRankContent(type, start, end);
                if (avatarList == null)
                {
                    return result;
                }
                for (int i = 0; i < avatarList.Count; i++)
                {
                    PbRankAvatarInfo pb = avatarList[i];
                    result.Add(new RankItemData(type, pb));
                }
                return result;
            }
        }
    }
}
