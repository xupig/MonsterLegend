﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using GameMain.GlobalManager;
using Game.Asset;
using GameLoader.Utils;
using Common.Base;
using UnityEngine.UI;
using GameData;

namespace Common.ExtendComponent
{
    public class BlurColorRimComponent : MonoBehaviour
    {
        private const string BLUR_SHADER_PATH = "Shader$GaussBlur7.u";
        private const string COLOR_RIM_PATH = "Shader$ColorRim.u";

        private const string RIM_COLOR = "_RimColor";
        private const string RIM_VARY = "_RimVary";
        private const string MAIN_TEX = "_MainTex";
        private const string OFFSET = "_Offset";

        public float _BlurRange = 0.025f;

        public Color _RimColor = new Color(0.058f, 0.054f, 0.05f, 1.0f);
        public float _Shape = 6.09f;
        public float _CenterCtrl = 0.16f;
        public float _RimCtrl = 0.02f;
        public float _DarkAdd = 0.02f;

        /*
        private float _DarkAddStart = -1.0f;
        private float _DarkAddEnd = 0.05f;
        private float _DarkAddStep = 0.1f;
         * */

        private Material _BlurMaterial;

        private RawImage _image;


        protected void Awake()
        {
            _image = gameObject.AddComponent<RawImage>();
            Shader blurShader = ObjectPool.Instance.GetAssemblyShader(BLUR_SHADER_PATH);
            _BlurMaterial = new Material(blurShader);
            _BlurMaterial.hideFlags = HideFlags.DontSave;

            Shader colorRimShader = ObjectPool.Instance.GetAssemblyShader(COLOR_RIM_PATH);
            _image.material = new Material(colorRimShader);

            _image.material.SetColor(RIM_COLOR, _RimColor);
            _image.material.SetVector(RIM_VARY, new Vector4(_CenterCtrl - _RimCtrl, _RimCtrl, _Shape, _DarkAdd));
        }

        public void Execute(PanelIdEnum panelId)
        {
            //_DarkAdd = _DarkAddStart;
            Camera camera = null;
            if (panelId == PanelIdEnum.Server || panelId == PanelIdEnum.Notice)
            {//登陆场景的选择服务器界面特殊提取摄像机
                GameObject go = GameObject.Find("Main Camera");
                if (go != null)
                {
                    camera = go.GetComponent<Camera>();
                    LoggerHelper.Debug("提取登录场景摄像机:" + camera);
                }
            }
            else if (panelId == PanelIdEnum.DreamlandPop)
            {
                camera = GetDreamlandCamera();
            }
            else
            {
                camera = CameraManager.GetInstance().Camera;
            }
            if(camera==null)
            {
                return;
            }
            Texture blurScene = GenerateBlurScene(camera);
            _image.texture = blurScene;
            _image.material.SetTexture(MAIN_TEX, blurScene);
            _image.material.SetColor(RIM_COLOR, _RimColor);
            _image.material.SetVector(RIM_VARY, new Vector4(_CenterCtrl - _RimCtrl, _RimCtrl, _Shape, _DarkAdd));
        }

        private Camera GetDreamlandCamera()
        {
            /*
            string sceneName = DreamlandScene.EXPLORING_SCENE_NAME;
            GameObject go = GameObject.Find(sceneName);
            if (go != null && go.activeInHierarchy)
            {
                return go.GetComponentInChildren<Camera>();
            }
            else
            {
                //GameObject goUi = GameObject.Find("UICamera");
                //return goUi.gameObject.GetComponent<Camera>();
                return CameraManager.GetInstance().Camera;
            }
            //sceneName = DreamlandScene.PREPARE_SCENE_NAME;
            //go = GameObject.Find(sceneName);
            //return go.GetComponentInChildren<Camera>();
             * */
            return null;
        }

        protected void Update()
        {
            /*
            if(_DarkAdd < _DarkAddEnd)
            {
                if((_DarkAdd + _DarkAddStep) > _DarkAddEnd)
                {
                    _DarkAdd = _DarkAddEnd;
                }
                else
                {
                    _DarkAdd += _DarkAddStep;
                }
            }
             * */
          
        }

        private Texture GenerateBlurScene(Camera camera)
        {
            RenderTexture tmpA = RenderTexture.GetTemporary((int)camera.pixelWidth / 4, (int)camera.pixelHeight / 4, 8);
            //RenderTexture tmpA = RenderTexture.GetTemporary(Screen.width, Screen.height, 8);
            tmpA.DiscardContents();
            RenderTexture camRT = camera.targetTexture;
            camera.targetTexture = tmpA;
            camera.Render();
            camera.targetTexture = camRT;

            tmpA.filterMode = FilterMode.Bilinear;
            tmpA.wrapMode = TextureWrapMode.Clamp;
            RenderTexture tmpB = RenderTexture.GetTemporary(tmpA.width, tmpA.height, 0);
            tmpB.DiscardContents();
            _BlurMaterial.SetVector(OFFSET, new Vector2(_BlurRange / 7.0f / camera.pixelWidth * camera.pixelHeight, 0.0f));
            Graphics.Blit(tmpA, tmpB, _BlurMaterial);

            tmpB.filterMode = FilterMode.Bilinear;
            tmpB.wrapMode = TextureWrapMode.Clamp;
            RenderTexture blurScene = new RenderTexture(tmpB.width, tmpB.height, 0);
            blurScene.DiscardContents();
            _BlurMaterial.SetVector(OFFSET, new Vector2(0.0f, _BlurRange / 7.0f));
            Graphics.Blit(tmpB, blurScene, _BlurMaterial);

            RenderTexture.ReleaseTemporary(tmpA);
            RenderTexture.ReleaseTemporary(tmpB);

            if(Application.platform == RuntimePlatform.WindowsPlayer)
            {
                RenderTexture rta = RenderTexture.active;
                RenderTexture.active = blurScene;
                Texture2D tex = new Texture2D(blurScene.width, blurScene.height);
                tex.ReadPixels(new Rect(0, 0, blurScene.width, blurScene.height), 0, 0);
                tex.Apply();
                RenderTexture.active = rta;

                tex.filterMode = FilterMode.Bilinear;
                tex.wrapMode = TextureWrapMode.Clamp;

                return tex;
            }
            else
            {

                blurScene.filterMode = FilterMode.Bilinear;
                blurScene.wrapMode = TextureWrapMode.Clamp;

                return blurScene;
            }
        }

    }
}
