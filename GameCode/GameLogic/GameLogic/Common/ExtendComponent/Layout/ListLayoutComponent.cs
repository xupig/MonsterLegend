﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine;

namespace Common.ExtendComponent
{
    /// <summary>
    /// 主要针对Item的大小是可变的list进行布局
    /// </summary>
    public class ListLayoutComponent : UIBehaviour
    {
        private KList _list;

        protected KList.KListPadding _padding;
        protected int _leftToRightGap = 0;
        protected int _topToDownGap = 0;
        protected KList.KListDirection _direction;
        protected int _leftToRightCount = 0;
        protected int _topToDownCount = 0;
        protected Vector2 _itemSize = Vector2.zero;

        protected override void Awake()
        {
            _list = gameObject.GetComponent<KList>();
            if (_list == null)
            {
                LoggerHelper.Error("This component must add to exist KList component game object!!");
            }
        }

        /// <summary>
        /// 仅支持单行或单列
        /// </summary>
        /// <param name="direction"></param>
        public void InitListLayoutInfo(KList.KListDirection direction)
        {
            _padding = _list.Padding;
            _topToDownGap = _list.TopToDownGap;
            _leftToRightGap = _list.LeftToRightGap;
            _direction = direction;
            _topToDownCount = _list.TopToDownCount;
            _leftToRightCount = _list.LeftToRightCount;
            _itemSize = _list.ItemSize;
        }

        public void RecalculateItemPosition()
        {
            Vector2 totalSizeDelta = Vector2.zero;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                KList.KListItemBase item = _list.ItemList[i];
                RectTransform itemRect = item.GetComponent<RectTransform>();
                Vector2 position = CalculateItemPosition(i, itemRect, ref totalSizeDelta);
                itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                if (item.Index != i)
                {
                    item.Index = i;
                    item.name = GenerateItemName(i);
                }
            }
        }

        public Vector2 RecalculateItemSize(int index, int count)
        {
            Vector2 sizeDelta = Vector2.zero;
            int endIndex = Math.Min(index + count, _list.ItemList.Count);
            for (int i = index; i < endIndex; i++)
            {
                KList.KListItemBase item = _list.ItemList[i];
                RectTransform itemRect = item.GetComponent<RectTransform>();
                CalculateItemPosition(i, itemRect, ref sizeDelta);
            }
            return sizeDelta;
        }

        private Vector2 CalculateItemPosition(int index, RectTransform itemRect, ref Vector2 curTotalSizeDelta)
        {
            Vector2 result = Vector2.zero;
            int pageCount = _leftToRightCount * _topToDownCount;
            if (_direction == KList.KListDirection.LeftToRight)
            {
                int xIndex = (int)(index % pageCount % _leftToRightCount);
                result.y = _padding.top + (int)((index % pageCount) / _leftToRightCount) * (_itemSize.y + _topToDownGap);
                if (xIndex == 0)
                {
                    result.x = _padding.left + curTotalSizeDelta.x;
                    curTotalSizeDelta.x = itemRect.sizeDelta.x + _padding.left;
                }
                else
                {
                    result.x = curTotalSizeDelta.x + _leftToRightGap;
                    curTotalSizeDelta.x += itemRect.sizeDelta.x + _leftToRightGap;
                }
                curTotalSizeDelta.y = itemRect.sizeDelta.y + _padding.top;
            }
            else
            {
                int yIndex = (int)((index % pageCount) % _topToDownCount);
                result.x = _padding.left + (int)((index % pageCount) / _topToDownCount) * (_itemSize.x + _leftToRightGap);
                curTotalSizeDelta.x = itemRect.sizeDelta.x + _padding.left;
                if (yIndex == 0)
                {
                    result.y = _padding.top + curTotalSizeDelta.y;
                    curTotalSizeDelta.y = itemRect.sizeDelta.y + _padding.top;
                }
                else
                {
                    result.y = curTotalSizeDelta.y + _topToDownGap;
                    curTotalSizeDelta.y += itemRect.sizeDelta.y + _topToDownGap;
                }
            }
            return result;
        }

        private string GenerateItemName(int index)
        {
            return KList.ITEM_PREFIX + index;
        }
    }
}
