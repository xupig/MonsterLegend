﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameLoader.Utils;

namespace Common.ExtendComponent
{
    //水平、垂直居中对齐的组件
    public class CenterAlignComponent : MonoBehaviour
    {
        private GameObject _alignBgGo;
        private List<GameObject> _alignGoList = new List<GameObject>();
        private GameObject _alignObjectParent;
        private Camera _uiCamera;

        private int _curAlignObjectCount = 0;
        private float _leftToRightGap = 0f;
        private float _topToDownGap = 0f;
        //private Vector2 _screenCenter = Vector2.zero;
        private Vector2 _localCenter = Vector2.zero;

        public void Init(List<GameObject> alignGoList, GameObject alignObjectParent, GameObject alignBgGo, Camera uiCamera)
        {
            if (alignGoList.Count < 2)
            {
                LoggerHelper.Error("Please set align Game Object list at least 2 count !!");
                return;
            }
            for (int i = 0;i < alignGoList.Count;i++)
            {
                _alignGoList.Add(alignGoList[i]);
            }
            _alignObjectParent = alignObjectParent;
            _alignBgGo = alignBgGo;
            _uiCamera = uiCamera;
            CalculateLocalCenter();
        }

        public void SetGap(float leftToRightGap, float topToDownGap)
        {
            _leftToRightGap = leftToRightGap;
            _topToDownGap = topToDownGap;
        }

        private void CalculateLocalCenter()
        {
            RectTransform bgRect = _alignBgGo.GetComponent<RectTransform>();
            Vector3[] bgWorldCorners = new Vector3[4];
            bgRect.GetWorldCorners(bgWorldCorners);
            Vector3 center = (bgWorldCorners[2] + bgWorldCorners[0]) / 2;
            Vector2 screenCenter = RectTransformUtility.WorldToScreenPoint(_uiCamera, center);
            RectTransform alignObjParentRect = _alignObjectParent.GetComponent<RectTransform>();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(alignObjParentRect, screenCenter, _uiCamera, out _localCenter);
        }

        public void DoLayout()
        {
            if (_alignGoList.Count < 2)
                return;
            int alignObjectCount = 0;
            List<GameObject> layoutGoList = new List<GameObject>();
            for (int i = 0; i < _alignGoList.Count; i++)
            {
                if (_alignGoList[i].activeSelf)
                {
                    alignObjectCount++;
                    layoutGoList.Add(_alignGoList[i]);
                }
            }
            if (_curAlignObjectCount != alignObjectCount)
            {
                _curAlignObjectCount = alignObjectCount;
                List<RectTransform> layoutGoRectList = new List<RectTransform>();
                for (int i = 0; i < layoutGoList.Count; i++)
                {
                    RectTransform rect = layoutGoList[i].GetComponent<RectTransform>();
                    layoutGoRectList.Add(rect);
                }
                DoHorizontalLayout(layoutGoRectList);
            }
        }

        private void DoHorizontalLayout(List<RectTransform> layoutGoRectList)
        {
            List<float> xList = new List<float>();
            float x = 0f;
            if (_curAlignObjectCount % 2 == 0)
            {
                int halfCount = _curAlignObjectCount / 2;
                float halfGap = _leftToRightGap / 2.0f;
                int index = 0;
                for (int i = halfCount - 1; i >= 0; i--)
                {
                    RectTransform rect = layoutGoRectList[i];
                    x = -((index + 1) * rect.sizeDelta.x + halfGap + index * _leftToRightGap);
                    xList.Insert(0, x);
                    index++;
                }

                for (int i = 0; i < halfCount; i++)
                {
                    RectTransform rect = layoutGoRectList[i];
                    x = i * rect.sizeDelta.x + halfGap + i * _leftToRightGap;
                    xList.Add(x);
                }

                CalculateHorizontalObjectsPos(layoutGoRectList, xList);
            }
            else
            {
                int halfCount = Mathf.FloorToInt(_curAlignObjectCount / 2.0f);
                float halfGoWidth = layoutGoRectList[0].sizeDelta.x / 2.0f;
                int index = 0;
                for (int i = halfCount - 1; i >= 0; i--)
                {
                    RectTransform rect = layoutGoRectList[i];
                    x = -((index + 1) * (rect.sizeDelta.x + _leftToRightGap) + halfGoWidth);
                    xList.Insert(0, x);
                    index++;
                }

                xList.Add(-halfGoWidth);

                for (int i = 0; i < halfCount; i++)
                {
                    RectTransform rect = layoutGoRectList[i];
                    x = (i + 1) * _leftToRightGap + i * rect.sizeDelta.x + halfGoWidth;
                    xList.Add(x);
                }

                CalculateHorizontalObjectsPos(layoutGoRectList, xList);
            }
        }

        private void CalculateHorizontalObjectsPos(List<RectTransform> layoutGoRectList, List<float> offsetXList)
        {
            RectTransform alignParentRect = _alignObjectParent.GetComponent<RectTransform>();
            for (int i = 0; i < layoutGoRectList.Count; i++)
            {
                CalculateObjectPosition(layoutGoRectList[i], alignParentRect, offsetXList[i], 0);
            }
        }

        private void CalculateObjectPosition(RectTransform layoutGoRect, RectTransform alignParentRect, float offsetX, float offsetY)
        {
            Vector2 anchorPos = new Vector2(_localCenter.x + offsetX, _localCenter.y + offsetY);
            anchorPos.y = layoutGoRect.anchoredPosition.y;
            layoutGoRect.anchoredPosition = anchorPos;
        }
    }
}
