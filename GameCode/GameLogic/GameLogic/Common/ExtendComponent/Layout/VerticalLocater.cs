﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/6 20:27:58
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class VerticalLocater : UIBehaviour
    {
        private bool _needRecalculateSize = true;
        public bool NeedRecalculateSize
        {
            set
            {
                _needRecalculateSize = value;
            }
        }

        private int _gap = int.MaxValue;
        public int Gap
        {
            get
            {
                return _gap;
            }
            set
            {
                _gap = value;
            }
        }

        private float _startY = int.MaxValue;
        public float StartY
        {
            set
            {
                _startY = value;
            }
        }

        private Locater _locater;
        private SizeRecalculator _sizeRecalculator;
        private List<VerticalLocater> _locaterList = new List<VerticalLocater>();
        public int Priority;//从上到下，越大越优先

        protected override void Awake()
        {
            _gap = int.MaxValue;
            _locater = gameObject.AddComponent<Locater>();
        }

        public void AddLocater(VerticalLocater locater)
        {
            _locaterList.Add(locater);
        }

        public void Remove(VerticalLocater locater)
        {
            _locaterList.Remove(locater);
        }

        public void RemoveAll()
        {
            for (int i = 0; i < _locaterList.Count; i++)
            {
                _locaterList[i].RemoveAll();
            }
            _locaterList.Clear();
        }

        public float Locate(float y = 0, List<int> gapList = null, int depth = 0)
        {
            LocateChildren(gapList, depth);
            LocateSelf(y);
            return _locater.Height;
        }

        private void LocateSelf(float y)
        {
            if (_needRecalculateSize)
            {
                if (_sizeRecalculator == null)
                {
                    _sizeRecalculator = gameObject.AddComponent<SizeRecalculator>();
                }
                _sizeRecalculator.Recalculate();
            }
            if (_startY != int.MaxValue)
            {
                y = _startY;
            }
            _locater.Y = y;
        }

        private void LocateChildren(List<int> gapList, int depth)
        {
            float y = 0;
            if (_startY != int.MaxValue)
            {
                y = _startY;
            }
            for (int i = 0; i < _locaterList.Count; i++)
            {
                VerticalLocater verticalLocater = _locaterList[i];
                float height = verticalLocater.Locate(y, gapList, depth + 1);
                int gap = GetGap(verticalLocater, depth, gapList);
                y -= (height + gap);
            }
        }

        private int GetGap(VerticalLocater verticalLocater, int depth, List<int> gapList)
        {
            if (verticalLocater.Gap != int.MaxValue)
            {
                return verticalLocater.Gap;
            }
            if ((gapList != null) && (gapList.Count > depth))
            {
                return gapList[depth];
            }
            return 0;
        }

        public void Sort()
        {
            _locaterList.Sort(SortByPriority);
        }

        private int SortByPriority(VerticalLocater x, VerticalLocater y)
        {
            if (x.Priority > y.Priority)
            {
                return -1;
            }
            return 1;
        }
    }
}
