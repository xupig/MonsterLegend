﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/6 20:42:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class SizeRecalculator : UIBehaviour
    {
        private StateText _text;
        private KContainer _container;
        private KList _list;

        protected override void Awake()
        {
            bool initSuccess = TryInitStateText();
            if (!initSuccess)
            {
                TryInitList();
            }
            if (!initSuccess)
            {
                TryInitContainer();
            }
        }

        /// <summary>
        /// 重新计算组件的宽高
        /// 目前的需求是高度，所以text控件只重新计算高度
        /// </summary>
        public void Recalculate()
        {
            if (_text != null)
            {
                _text.RecalculateAllStateHeight();
            }
            else if (_list != null)
            {
                ListRecalculate();
            }
            else if (_container != null)
            {
                _container.RecalculateSize();
            }
        }

        private void ListRecalculate()
        {
            float listRight = 0;
            float listBottom = 0;
            List<KList.KListItemBase> itemList = _list.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                KList.KListItemBase item = itemList[i];
                RectTransform itemRect = item.GetComponent<RectTransform>();
                Vector3 position = itemRect.localPosition;
                float itemRight = position.x + itemRect.sizeDelta.x + _list.Padding.right;
                listRight = itemRight > listRight ? itemRight : listRight;
                float itemBottom = Mathf.Abs(position.y) + itemRect.sizeDelta.y + _list.Padding.bottom;
                listBottom = itemBottom > listBottom ? itemBottom : listBottom;
            }
            RectTransform listRect = _list.GetComponent<RectTransform>();
            listRect.sizeDelta = new Vector2(listRight, listBottom);
        }

        private bool TryInitStateText()
        {
            StateText text = gameObject.GetComponent<StateText>();
            if (text != null)
            {
                _text = text;
                return true;
            }
            return false;
        }

        private void TryInitContainer()
        {
            KContainer container = gameObject.GetComponent<KContainer>();
            if (container != null)
            {
                _container = container;
            }
        }

        private void TryInitList()
        {
            KList list = gameObject.GetComponent<KList>();
            if (list != null)
            {
                _list = list;
            }
        }
    }
}
