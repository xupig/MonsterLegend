﻿#region 模块信息
/*==========================================
// 文件名：KDragable
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.DragableComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/16 16:18:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent.Dragable
{
    public class KDragable : KContainer, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public KComponentEvent<KDragable, PointerEventData> onBegineDrag = new KComponentEvent<KDragable, PointerEventData>();
        public KComponentEvent<KDragable, PointerEventData> onDrag = new KComponentEvent<KDragable, PointerEventData>();
        public KComponentEvent<KDragable, PointerEventData> onEndDrag = new KComponentEvent<KDragable, PointerEventData>();

        public void OnBeginDrag(PointerEventData _data)
        {
            onBegineDrag.Invoke(this,_data);
        }

        public void OnDrag(PointerEventData _data)
        {
            onDrag.Invoke(this,_data);
        }

        public void OnEndDrag(PointerEventData _data)
        {
            onEndDrag.Invoke(this,_data);
        }
    }
}
