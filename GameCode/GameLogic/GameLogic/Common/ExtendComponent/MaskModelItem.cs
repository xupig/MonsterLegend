﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.Utils;

namespace Common.ExtendComponent
{
    public class MaskModelItem : MaskObjectItemBase
    {
        private GameObject _modelGo;

        public void Init(GameObject attachModelGo, GameObject modelGo, bool processMaskImmediately = false)
        {
            SetAttachObjectGo(attachModelGo);
            _modelGo = modelGo;
            if (processMaskImmediately)
            {
                ControlMaskObject();
            }
        }

        protected override bool CheckGameObjectValid()
        {
            if (!base.CheckGameObjectValid())
            {
                return false;
            }
            if (_modelGo == null)
            {
                return false;
            }
            return true;
        }

        protected override void ProcessInRect()
        {
            if (!_modelGo.activeSelf)
            {
                _modelGo.SetActive(true);
            }
        }

        protected override void ProcessOutRect()
        {
            if (_modelGo.activeSelf)
            {
                _modelGo.SetActive(false);
            }
        }
    }
}
