﻿using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class ArtNumberList : KContainer
    {
        private string _cacheContent = string.Empty;
        private Dictionary<char, GameObject> _numberDict;
        private List<GameObject> _showNumbers;
        private Transform _tran;

        protected override void Awake()
        {
            InitNumberDict();
            _showNumbers = new List<GameObject>();
            _tran = transform;
        }

        private void InitNumberDict()
        {
            _numberDict = new Dictionary<char, GameObject>();
            Transform _templateTran = GetChild("item").transform;
            _templateTran.gameObject.SetActive(true);
            for (int i = 0; i < _templateTran.childCount; i++)
            {
                GameObject childGo = _templateTran.GetChild(i).gameObject;
                string childName = childGo.name;
                char mark = childName[childName.Length - 1];
                _numberDict.Add(mark, childGo);
                childGo.SetActive(false);
            }
        }

        public void SetNumber(int value)
        {
            SetNumber(value.ToString());
        }

        public void SetNumber(string content)
        {
            for (int i = 0; i < content.Length; i++)
            {
                if (_cacheContent.Length > i)
                {
                    if (_cacheContent[i].Equals(content[i]))
                    {
                        continue;
                    }
                    else
                    {
                        UpdateNumber(i, content[i]);
                    }
                }
                else
                {
                    UpdateNumber(i, content[i]);
                }
            }
            _cacheContent = content;
            for (int i = _showNumbers.Count - 1; i >= content.Length; i--)
            {
                Destroy(_showNumbers[i]);
                _showNumbers.RemoveAt(i);
            }
            AdjustNumberPostion();
        }

        private void AdjustNumberPostion()
        {
            Vector2 startVector = Vector2.zero;
            RectTransform rectTran = GetComponent<RectTransform>();
            for (int i = 0; i < _showNumbers.Count; i++)
            {
                _showNumbers[i].SetActive(true);
                Locater locater = _showNumbers[i].GetComponent<Locater>();
                locater.Position = new Vector2(startVector.x, -(rectTran.rect.height - locater.Height) * 0.5f);
                startVector += new Vector2(locater.Width, 0);
            }
            rectTran.sizeDelta = new Vector2(startVector.x, rectTran.sizeDelta.y);
        }

        private void UpdateNumber(int index, char c)
        {
            if (_showNumbers.Count > index)
            {
                GameObject.Destroy(_showNumbers[index]);
                GameObject numberGo = GameObject.Instantiate(_numberDict[c]) as GameObject;
                numberGo.transform.SetParent(_tran);
                numberGo.transform.localPosition = Vector3.zero;
                numberGo.transform.localScale = Vector3.one;
                numberGo.AddComponent<Locater>();
                _showNumbers[index] = numberGo;
            }
            else
            {
                GameObject numberGo = GameObject.Instantiate(_numberDict[c]) as GameObject;
                numberGo.transform.SetParent(_tran);
                numberGo.transform.localScale = Vector3.one;
                numberGo.transform.localPosition = Vector3.zero;
                numberGo.AddComponent<Locater>();
                _showNumbers.Add(numberGo);
            }
        }

    }
}
