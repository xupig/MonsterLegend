﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/6 13:43:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class TimerButton : KButton
    {
        private bool hasTick = false;
        private uint _timerId = TimerHeap.INVALID_ID;

        public override void OnPointerDown(PointerEventData evtData)
        {
            base.OnPointerDown(evtData);
            hasTick = false;
            AddTimer();
        }

        public override void OnPointerUp(PointerEventData evtData)
        {
            base.OnPointerUp(evtData);
            //如果已经处于长按状态，放开按钮时不再触发onClick事件
            if (hasTick)
            {
                evtData.eligibleForClick = false;
            }
            Stop();
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer(500, 500, OnTick);
            }
        }

        public void Stop()
        {
            RemoveTimer();
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnTick()
        {
            hasTick = true;
            onClick.Invoke();
        }
    }
}
