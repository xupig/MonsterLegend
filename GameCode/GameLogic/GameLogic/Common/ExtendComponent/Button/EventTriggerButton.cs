﻿using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class EventTriggerButton : KButton
    {
        public KComponentEvent<int> ResponseEvent = new KComponentEvent<int>();

        private string _eventName = string.Empty;
        private bool _hasReceiveEvent;
        private int _nowNum;
        private Func<int> _getLeftNumFunc;

        protected override void Awake()
        {
            gameObject.AddComponent<KButtonStateFacade>();
            base.Awake();
        }

        public void Init(string eventName, Func<int> GetLeftNumFunc)
        {
            _eventName = eventName;
            _getLeftNumFunc = GetLeftNumFunc;
        }

        private uint _timerId = TimerHeap.INVALID_ID;
        public override void OnPointerDown(PointerEventData evtData)
        {
            _hasReceiveEvent = false;
            _nowNum = 1;
            AddListener();
            base.OnPointerDown(evtData);
            _timerId = TimerHeap.AddTimer(200, 0, Delay);
        }

        private void Delay()
        {
            OnReceiveEvent();
        }

        private void DelTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        public override void OnPointerUp(PointerEventData evtData)
        {
            DelTimer();
            RemoveListener();
            if (_hasReceiveEvent)
            {
                evtData.eligibleForClick = false;
            }
            base.OnPointerUp(evtData);
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(_eventName, OnReceiveEvent);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener(_eventName, OnReceiveEvent);
        }

        public void OnReceiveEvent()
        {
            _hasReceiveEvent = true;
            int leftNum = _getLeftNumFunc();
            _nowNum = GetNowNum(_nowNum);
            if (_nowNum > leftNum)
            {
                _nowNum = leftNum;
            }
            ResponseEvent.Invoke(_nowNum);
            if (_nowNum == leftNum)
            {
                RemoveListener();
            }
        }

        private int GetNowNum(int nowNum)
        {
            if (nowNum > 4)
            {
                nowNum += 1;
            }
            else
            {
                nowNum *= 2;
            }
            return nowNum;
        }
    }
}
