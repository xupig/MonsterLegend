﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameMain.GlobalManager;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;

namespace Common.ExtendComponent
{
    public class SortOrderedRenderAgent : MonoBehaviour
    {
        private static int _order = 0;
        private static bool _refreshAllFlag = false;
        private static ChildrenComponentsPool<List<Renderer>> _rendererListPool = new ChildrenComponentsPool<List<Renderer>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<SortOrderedRenderAgent>> _agentListPool = new ChildrenComponentsPool<List<SortOrderedRenderAgent>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<Canvas>> _canvasListPool = new ChildrenComponentsPool<List<Canvas>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<GraphicRaycaster>> _raycasterListPool = new ChildrenComponentsPool<List<GraphicRaycaster>>(null, l => l.Clear());

        public int order = 0;
        private Canvas _canvas;
        private GraphicRaycaster _raycaster;
       

        protected void Awake()
        {
             List<Canvas> canvasList = _canvasListPool.Get();
             GetComponentsInChildren(true, canvasList);
            if(canvasList.Count>0)
            {
                _canvas = canvasList[0];
                List<GraphicRaycaster> raycasterList = _raycasterListPool.Get();
                GetComponentsInChildren(true, raycasterList);
                _raycaster = raycasterList[0];
            }
        }

        protected void OnDestroy()
        {
            _canvas = null;
            _raycaster = null;
        }

        protected void LateUpdate()
        {
            if(SortOrderedRenderAgent._refreshAllFlag == true)
            {
                SortOrderedRenderAgent._refreshAllFlag = false;
                SortOrderedRenderAgent.DoRebuildAll();
            }
        }

        private void Refresh()
        {
            order = _order++;
            if (_canvas!=null)
            {
                if (_canvas.overrideSorting == false)
                {
                    _canvas.overrideSorting = true;
                }
                if (_canvas.sortingOrder != this.order)
                {
                    _canvas.sortingOrder = this.order;
                }
                //Ari _raycaster.priority = this.order;
            }
            else
            {
                List<Renderer> rendererList = _rendererListPool.Get();
                GetComponentsInChildren(false, rendererList);
                for(int i = 0; i < rendererList.Count; i++)
                {
                    rendererList[i].sortingOrder = this.order;
                }
                _rendererListPool.Release(rendererList);
            }
        }

        public static void RebuildAll()
        {
            _refreshAllFlag = true;
        }

        private static void DoRebuildAll()
        {
            _order = 0;
            List<SortOrderedRenderAgent> agentList = _agentListPool.Get();
            UIManager.Instance.UIRoot.GetComponentsInChildren(false, agentList);
            for(int i = 0; i < agentList.Count; i++)
            {
                agentList[i].Refresh();
            }
            _agentListPool.Release(agentList);
        }
    }

    
}
