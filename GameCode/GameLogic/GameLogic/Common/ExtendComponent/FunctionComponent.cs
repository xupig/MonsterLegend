﻿#region 模块信息
/*==========================================
// 文件名：FunctionComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/30 21:59:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{

    public class EntryItem : KContainer
    {
    }
    public class FunctionComponent : UIBehaviour
    {
        private Dictionary<int, EntryItem> entryItemDict;

        private Dictionary<int, EntryItem> _itemPoolDict;

        //Ari private TownActivityEntryView m_activityView;
        private RectTransform _activityRect;
        private Vector2 _originalPos;
        private GameObject _template;

        public static GameObject pointGo;

        protected override void Awake()
        {
            entryItemDict = new Dictionary<int, EntryItem>();
            _itemPoolDict = new Dictionary<int, EntryItem>();


            _template = transform.FindChild("Container_itemTemplate/Button_itemTemplate").gameObject;
            //Ari m_activityView = transform.FindChild("Container_topRight/Container_UIEntrytTopRight").gameObject.AddComponent<TownActivityEntryView>();
            //_activityRect = m_activityView.GetComponent<RectTransform>();
            _originalPos = _activityRect.anchoredPosition;
            EventDispatcher.AddEventListener(FunctionEvents.INIT, OnFunctionRefresh);
            EventDispatcher.AddEventListener<int, GameObject, Vector3>(FunctionEvents.OPEN, OnFunctionOpen);
            EventDispatcher.AddEventListener<int, bool>(FunctionEvents.SET_FUNTION_POINT, OnSetFunctionPoint);
            EventDispatcher.AddEventListener<int>(FunctionEvents.NO_PREVIEW_FUNCATION_OPEN, OnNoPreviewFunctionOpen);
            EventDispatcher.AddEventListener<int>(FunctionEvents.UPDATE_FUNCTION, OnUpdateFunction);
            InitEffectGo();
            base.Awake();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        private void InitEffectGo()
        {
            Transform child = transform.FindChild("Container_itemTemplate/fx_ui_23_zhiyin_01");
            if (child != null)
            {
                pointGo = child.gameObject;
                pointGo.SetActive(false);
            }
        }

        private void OnSetFunctionPoint(int funId, bool isPoint)
        {

            if (entryItemDict.ContainsKey(funId) == true)
            {
                //ari entryItemDict[funId].SetPoint(isPoint);
            }
        }

        private void OnUpdateFunction(int functionId)
        {
            if(entryItemDict.ContainsKey(functionId) && gameObject.activeInHierarchy)
            {
                OnFunctionRefresh();
            }
        }

        public void ShowActivityView()
        {
            //ari m_activityView.Visible = true;
        }

        public void HideActivityView()
        {
            //ari m_activityView.Visible = false;
        }

        private void RefreshFieldEntryVisible()
        {
            int curMapID = GameSceneManager.GetInstance().curMapID;
            if (curMapID != 0 &&
                (map_helper.GetMapType(curMapID) == public_config.MAP_TYPE_CITY) ||
                (map_helper.GetMapType(curMapID) == public_config.MAP_TYPE_DUEL))
            {
                _activityRect.anchoredPosition = _originalPos + new Vector2(60,0);
                SetFieldEntryVisible(true);
            }
            else
            {
                _activityRect.anchoredPosition = _originalPos;
                SetFieldEntryVisible(false);
            }
        }

        private void SetFieldEntryVisible(bool visible)
        {
            foreach (KeyValuePair<int, EntryItem> kvp in entryItemDict)
            {
                if (kvp.Key == (int)FunctionId.WorldMap)
                {
                    kvp.Value.Visible = visible;
                }
            }
        }

        private void ClearAllItem()
        {
            foreach (KeyValuePair<int, EntryItem> kvp in entryItemDict)
            {
                kvp.Value.Visible = false;
                _itemPoolDict.Add(kvp.Key, kvp.Value);
            }
            entryItemDict.Clear();
        }

        public void OnFunctionRefresh()
        {
            ClearAllItem();
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            for (int i = 0; i < functionData.activityList.Count; i++)
            {
                //ari CreateItem(m_activityView.gameObject, functionData.activityList[i], i);
            }
            RefreshFieldEntryVisible();
            TweenActivityEntry();
        }

        private EntryItem CreateItem(GameObject parent, function function, int count)
        {
            EntryItem item;
            if (_itemPoolDict.ContainsKey(function.__id))
            {
                item = _itemPoolDict[function.__id];
                _itemPoolDict.Remove(function.__id);

            }
            else
            {
                GameObject gameObject = GameObject.Instantiate(_template) as GameObject;
                item = gameObject.AddComponent<EntryItem>();
                item.transform.SetParent(parent.transform);

            }
            item.Visible = true;
            //ari item.SetInfo(function, count);
            if (entryItemDict.ContainsKey(function.__id) == false)
            {
                entryItemDict.Add(function.__id, item);
            }
            else
            {
                entryItemDict[function.__id] = item;
            }
            return item;
        }

        private void TweenActivityEntry()
        {
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            for (int i = 0; i < functionData.activityList.Count; i++)
            {
                EntryItem item = entryItemDict[functionData.activityList[i].__id];
                //ari item.TweenHide(0.0f, functionData.activityList.Count - 1 - i, OnHideActivityComplete);
            }
        }

        private void OnHideActivityComplete(UITweener tween)
        {
            ShowActivityEntry();
        }

        private void ShowActivityEntry()
        {
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            for (int i = 0; i < functionData.activityList.Count; i++)
            {
                EntryItem item = entryItemDict[functionData.activityList[i].__id];
                TweenShow(item, 350);
            }
        }

        private void TweenShow(EntryItem entryItem, uint duration)
        {
            //ari entryItem.TweenShow(duration / 1000.0f, TweenEnd);
        }

        private void TweenEnd(UITweener tweener)
        {

        }

        private void OnFunctionOpen(int openId, GameObject go, Vector3 offset)
        {
            if (entryItemDict.ContainsKey(openId))
            {
                PlayerDataManager.Instance.FunctionData.TweenEnd(openId);
                PanelIdEnum.Function.Close();
                return;
            }
            function function = function_helper.GetFunction(openId);
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            if (function.__type == 2)
            {
                //ari AddFunction(functionData.activityList, function, m_activityView.gameObject, m_activityView.Visible, go, offset);
            }

        }


        private void AddNoPrevFunction(List<function> functionList, function function, GameObject parent, bool needTween)
        {
            int result = PlayerDataManager.Instance.FunctionData.AddFunctionData(functionList, function);
            if (result == -1)
            {
                return;
            }
            EntryItem item = CreateItem(parent, function, result);
            //item.SetDefaultPlace();
            //item.MoveTo(result, needTween);
            for (int i = result + 1; i < functionList.Count; i++)
            {
                if (entryItemDict.ContainsKey(functionList[i].__id) == true)
                {
                    //ari  entryItemDict[functionList[i].__id].MoveTo(i, needTween, 0.15f);
                }
            }
        }

        private void OnNoPreviewFunctionOpen(int openId)
        {
            if (entryItemDict.ContainsKey(openId))
            {
                return;
            }
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            function function = function_helper.GetFunction(openId);
            if (function.__type == 2)
            {
                //ari AddNoPrevFunction(functionData.activityList, function, m_activityView.gameObject, m_activityView.Visible);
            }
        }

        /// <summary>
        /// 刚开启的item列表
        /// </summary>
        private static List<EntryItem> itemList;

        private void AddFunction(List<function> functionList, function function, GameObject parent, bool needTween, GameObject go, Vector3 offset)
        {
            int result = PlayerDataManager.Instance.FunctionData.AddFunctionData(functionList, function);
            if (result == -1)
            {
                return;
            }
            if (itemList == null)
            {
                itemList = new List<EntryItem>();
            }
            EntryItem item = CreateItem(parent, function, result);
            item.Visible = false;
            itemList.Add(item);
            for (int i = result + 1; i < functionList.Count; i++)
            {
                if (entryItemDict.ContainsKey(functionList[i].__id) == true)
                {
                    //ari entryItemDict[functionList[i].__id].MoveTo(i, needTween, 1f);
                }
            }
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.MainUIField);
            TweenAbsolutePosition.Begin(go, 1f, item.transform.position, delegate(UITweener tween)
            {
                if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false)
                {
                    UIManager.Instance.DisablePanelCanvas(PanelIdEnum.MainUIField);
                }
                PanelIdEnum.Function.Close();
                for (int i = 0; i < itemList.Count; i++)
                {
                    itemList[i].Visible = true;
                }
                itemList.Clear();
                // item.MoveTo(result, needTween);
                PlayerDataManager.Instance.FunctionData.TweenEnd(function.__id);
            });

        }
    }
}
