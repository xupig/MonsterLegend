﻿#region 模块信息
/*==========================================
// 文件名：PageableScrollView
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.PageableComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/12 15:02:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class PageableScrollView:KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private LoadingComponent _loading;
        private int _currentPage;
        private int _totalPage;
       
        private int _capacity = 10;
        private float _height;
        private float _width;
        private RectTransform _rectTransform;

        public KComponentEvent<PageableScrollView, int> onRequestPage = new KComponentEvent<PageableScrollView, int>();
        public KComponentEvent<PageableScrollView, int> onSelectedIndexChanged = new KComponentEvent<PageableScrollView, int>();

        public bool IsPageable
        {
            get;
            set;
        }

        public int CurrentPage
        {
            get { return _currentPage; }
        }

        protected override void Awake()
        {
            _scrollView = GetComponent<KScrollView>();
            _rectTransform = _scrollView.Content.GetComponent<RectTransform>();
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _scrollView.ScrollRect.vertical = true;
            _scrollView.ScrollRect.movementType = UnityEngine.UI.ScrollRect.MovementType.Elastic;
            IsPageable = true;
            //_loading = KComponentUtil.AddChildComponent<LoadingComponent>(this.gameObject, "");
            base.Awake();
            AddEventListener();
        }

        public KList List
        {
            get
            {
                return _list;
            }
        }

        private void AddEventListener()
        {
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNextPage);
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveEventListener()
        {
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNextPage);
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnReRequest(KScrollRect scrollRect)
        {
            if(IsPageable)
            {
                //scrollRect.Draggable = false;
                onRequestPage.Invoke(this, 1);
            }
        }

        private void OnRequestNextPage(KScrollRect scrollRect)
        {
            if(IsPageable)
            {
               // scrollRect.Draggable = false;
                onRequestPage.Invoke(this, Math.Min(_currentPage + 1, _totalPage));
            }
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            onSelectedIndexChanged.Invoke(this, index);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public void SetDirection(int leftToRightCount, int topToDownCount)
        {
            _leftToRightCount = leftToRightCount;
            //_topToDownCount = topToDownCount;
        }

        public void SetGap(int topToDownGap, int leftToRightGap)
        {
            _topToDownGap = topToDownGap;
            _leftToRightGap = leftToRightGap;
        }

        public void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public void SetCapacity(int capacity)
        {
            _capacity = capacity;
        }

        private void Init<T>() where T:KList.KListItemBase
        {
            if (_list.ItemList.Count == 0)
            {
                List<object> dataList = new List<object>();
                for (int i = 0; i < _capacity; i++)
                {
                    dataList.Add(null);
                }
                _list.SetDataList<T>(dataList);
                Rect rect = _list[0].GetComponent<RectTransform>().rect;
                _width = rect.width;
                _height = rect.height;
            }
           
        }

        public void ClearSelected()
        {
            _list.SelectedIndex = -1;
        }

        private IList _dataList;

        public void SetDataList<T>(IList dataList,int currentPage,int totalPage,bool isReset = false) where T : KList.KListItemBase
        {
            _dataList = dataList;
            Init<T>();
            if (isReset || (IsPageable && currentPage == 1))
            {
                _index = 0;
                _rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x, 0);
            }
            else
            {
                if(dataList.Count-_capacity-1 < _index)
                {
                    _index = Math.Max(0, dataList.Count - _capacity - 1);
                }
            }
            if(isReset)
            {
                _list.SelectedIndex = -1;
            }
            int count = Math.Min(dataList.Count,_capacity);
            for (int i = 0; i < count;i++ )
            {
                _list[i].Data = dataList[i+_index];
                _list[i].gameObject.SetActive(true);
            }
            for(int j = count;j<_capacity;j++)
            {
                _list[j].gameObject.SetActive(false);
            }
            
            RectTransform listRect = _list.GetComponent<RectTransform>();
            listRect.sizeDelta = new Vector2(_width+_padding.left+_padding.right, (_height + _topToDownGap) * Mathf.CeilToInt((float)dataList.Count / _leftToRightCount) + _padding.top + _padding.bottom);
            UpdateItemListLayout();
            _currentPage = currentPage;
            _totalPage = totalPage;
            //_scrollView.ScrollRect.Draggable = true;
            
        }

        private int _index = 0;

        private void MoveNext()
        {
            if (_index < _dataList.Count - _capacity)
            {
                KList.KListItemBase item = _list.ItemList[0];
                _list.ItemList.Remove(item);
                _list.ItemList.Add(item);
                _index++;
                item.Data = _dataList[_index + _capacity -1];
            }
        }


        private void MovePrev()
        {
            if(_index > 0)
            {
                KList.KListItemBase item = _list.ItemList[Math.Min(_capacity, _dataList.Count) - 1];
                _list.ItemList.Remove(item);
                _list.ItemList.Insert(0, item);
                _index--;
                item.Data = _dataList[_index];
            }
        }

        protected void Update()
        {
            if (_dataList == null || _list == null || _list.ItemList.Count == 0)
            {
                return;
            }
            int num = (Mathf.FloorToInt(Math.Abs(_rectTransform.anchoredPosition.y) / (_height + _topToDownGap)) - 1) * _leftToRightCount;
            if (num + 1 > _dataList.Count || num < 0)
            {
                return;
            }
            if (_index < num)
            {
                for (int i = _index; i < num ; i++)
                {
                    MoveNext();
                }
                UpdateItemListLayout();
            }
            else if (_index > num )
            {
                for (int i = num; i < _index; i++)
                {
                    MovePrev();
                }
                UpdateItemListLayout();
            }
        }


        private int _topToDownGap = 3;	//item列间距
        private int _leftToRightGap = 3;		//item行间距
        //private int _topToDownCount = int.MaxValue;			//item列数量
        private int _leftToRightCount = 1;	//item行
        private Game.UI.UIComponent.KList.KListPadding _padding = new KList.KListPadding();

        protected virtual void UpdateItemListLayout()
        {
            int index = _index;
            List<KList.KListItemBase> _itemList = _list.ItemList;
            for (int i = 0; i < _itemList.Count; i++)
            {
                KList.KListItemBase item = _itemList[i];
                if (item.gameObject.activeSelf)
                {
                    RectTransform itemRect = item.GetComponent<RectTransform>();
                    Vector2 position = CalculateItemPosition(index, itemRect);

                    itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                    itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                    item.Index = i;
                    item.name = KList.ITEM_PREFIX + index;
                    index++;
                }  
            }
            
        }

        private Vector2 CalculateItemPosition(int index, RectTransform itemRect)
        {
            Vector2 result = Vector2.zero;
            result.x = _padding.left + (int)(index % _leftToRightCount) * (itemRect.sizeDelta.x + _leftToRightGap);
            result.y = _padding.top +  (int)(index / _leftToRightCount)  * (_height + _topToDownGap);
            return result;
        }

    }
}
