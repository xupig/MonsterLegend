﻿#region 模块信息
/*==========================================
// 文件名：PageableListToggleGroup
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.PageableComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/13 10:23:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent.PageableComponent
{
    public class PageableListToggleGroup:KContainer
    {
        private KScrollPage _scrollPage;
        private Common.ExtendComponent.ListToggleGroup.ListToggleGroup _list;

        public KComponentEvent<PageableListToggleGroup,int> onRequestPage = new KComponentEvent<PageableListToggleGroup,int>();
        public KComponentEvent<PageableListToggleGroup, int> onCurrentPageChanged = new KComponentEvent<PageableListToggleGroup, int>();

        private int _capacity = 20;
        private int _pageCapacity = 4;

        private int _currentPage;
        private int _totalPage;

        public int CurrentPage
        {
            get { return _currentPage; }
        }
        protected override void Awake()
        {
            _scrollPage = GetComponent<KScrollPage>();

            _list = _scrollPage.AddChildComponent<Common.ExtendComponent.ListToggleGroup.ListToggleGroup>("mask/content");
            
            _scrollPage.onRequestPrePage.AddListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.AddListener(OnRequestNextPage);
            _scrollPage.onCurrentPageChanged.AddListener(OnCurrentPageChanged);
            base.Awake();
        }

        protected override void OnDestroy()
        {
            _scrollPage.onRequestPrePage.RemoveListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.RemoveListener(OnRequestNextPage);
            _scrollPage.onCurrentPageChanged.RemoveListener(OnCurrentPageChanged);
            base.OnDestroy();
        }

        public void SetPageCapacity(int pageCapacity)
        {
            _pageCapacity = pageCapacity;
        }

        public void SetCapacity<T>(int capacity) where T : ListToggleItem
        {
            _capacity = capacity;
            List<object> dataList = new List<object>();
            for (int i = 0; i < _capacity; i++)
            {
                dataList.Add(null);
            }
            _list.SetDataList<T>(dataList);
        }

        public void SetDirection(KList.KListDirection direction, int leftToRightCount, int topToDownCount)
        {
            _list.SetDirection(direction, leftToRightCount, topToDownCount);
        }

        public void SetGap(int topToDownGap, int leftToRightGap)
        {
            _list.SetGap(topToDownGap, leftToRightGap);
        }

        public void SetPadding(int top, int right, int bottom, int left)
        {
            _list.SetPadding(top, right, bottom, left);
        }

        public void SetDataList<T>(IList dataList,int currentPage,int totalPage) where T : ListToggleItem
        {
            _currentPage = currentPage;
            _totalPage = totalPage;
            if (_list.ItemList.Count == 0)
            {
                SetCapacity<ListToggleItem>(_capacity);
            }
            int i = 0;
            for (i = 0; i < dataList.Count; i++)
            {
                _list.ItemList[i].Data = dataList[i];
            }
            for (int j = i; j < _capacity; j++)
            {
                _list.ItemList[j].Data = null;
            }
            _list.DoLayout();
            _scrollPage.TotalPage = Mathf.CeilToInt((float)dataList.Count / _pageCapacity);
            _scrollPage.CurrentPage = 1;
            _scrollPage.Dragable = true;
        }

        private void OnCurrentPageChanged(KScrollPage scrollPage, int index)
        {
            onCurrentPageChanged.Invoke(this, index);
            _scrollPage.UpdateArrow((_currentPage - 1) * (_capacity / _pageCapacity) + index, (_totalPage - 1) * (_capacity / _pageCapacity) + _scrollPage.TotalPage);
        }

        private void OnRequestNextPage(KScrollPage scrollPage)
        {
            _scrollPage.Dragable = false;
            onRequestPage.Invoke(this, Math.Min(_currentPage + 1, _totalPage));
        }

        private void OnRequestPrePage(KScrollPage scrollPage)
        {
            _scrollPage.Dragable = false;
            onRequestPage.Invoke(this, Math.Max(1, _currentPage - 1));
        }
    }
}
