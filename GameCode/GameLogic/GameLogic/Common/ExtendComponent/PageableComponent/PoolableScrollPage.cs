﻿#region 模块信息
/*==========================================
// 文件名：PageableComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.PageableComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/12 9:59:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class PoolableScrollPage : KContainer
    {
        private KList _list;
        private KScrollPage _scrollPage;

        public KComponentEvent<PoolableScrollPage, int> onRequestPage = new KComponentEvent<PoolableScrollPage, int>();
        public KComponentEvent<PoolableScrollPage, int> onCurrentPageChanged = new KComponentEvent<PoolableScrollPage, int>();

        private int _pageCapacity = 4;
        private int _preparePage = 3;
        private IList _dataList;

        private float _height;
        private float _width;

        public KList List
        {
            get { return _list; }
        }

        public KScrollPage ScrollPage
        {
            get { return _scrollPage; }
        }

        protected override void Awake()
        {
            _scrollPage = GetComponent<KScrollPage>();
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            base.Awake();
            _scrollPage.onCurrentPageChanged.AddListener(OnCurrentPageChanged);
        }


        protected override void OnDestroy()
        {
            _scrollPage.onCurrentPageChanged.RemoveListener(OnCurrentPageChanged);
            base.OnDestroy();
        }

        public void SetPageCapacity(int pageCapacity)
        {
            _pageCapacity = pageCapacity;
        }

        private void Init<T>() where T : KList.KListItemBase
        {
            List<object> dataList = new List<object>();
            for (int i = 0; i < _pageCapacity * _preparePage; i++)
            {
                dataList.Add(null);
            }
            _list.SetDataList<T>(dataList);
            Rect rect = _list[0].GetComponent<RectTransform>().rect;
            _width = rect.width;
            _height = rect.height;
        }

        public void SetDataList<T>(IList dataList) where T : KList.KListItemBase
        {
            if (_list.ItemList.Count == 0)
            {
                Init<T>();
            }
            _dataList = dataList;
            int totalPage = Math.Max(1, Mathf.CeilToInt((float)dataList.Count / _pageCapacity));
            _preCurrentPage = 1;
            UpdateContent(1, totalPage);
            RectTransform listRect = _list.GetComponent<RectTransform>();
            listRect.sizeDelta = new Vector2((_width + _list.LeftToRightGap) * _list.LeftToRightCount + _list.Padding.left + _list.Padding.right, (_height + _list.TopToDownGap) * _list.TopToDownCount + _list.Padding.top + _list.Padding.bottom);
            _scrollPage.TotalPage = totalPage;
            _scrollPage.CurrentPage = 0;
        }

        private int _preCurrentPage;
        private void UpdateContent(int currentPage, int totalPage)
        {
            if (_preCurrentPage > currentPage)
            {
                MovePrev(currentPage);
            }
            else if (_preCurrentPage < currentPage)
            {
                MoveNext(currentPage, totalPage);
            }
            else
            {
                Refresh(currentPage, totalPage);
            }
            _preCurrentPage = currentPage;
            UpdateItemListLayout(currentPage, totalPage);
        }

        private void MovePrev(int currentPage)
        {
            if (currentPage > 1)
            {
                int index = 0;
                int capacity = _list.ItemList.Count;
                while (index < _pageCapacity)
                {
                    KList.KListItemBase item = _list.ItemList[capacity - 1];
                    item.Visible = true;
                    item.Data = _dataList[(currentPage - 1) * _pageCapacity - index - 1];
                    _list.ItemList.Remove(item);
                    _list.ItemList.Insert(0, item);
                    index++;
                }
            }
        }

        private void MoveNext(int currentPage, int totalPage)
        {
            if (currentPage < totalPage - 1)
            {
                int index = 0;
                while (index < _pageCapacity)
                {
                    KList.KListItemBase item = _list.ItemList[0];
                    item.Visible = true;
                    item.Data = _dataList[(currentPage) * _pageCapacity + index];
                    _list.ItemList.Remove(item);
                    _list.ItemList.Add(item);
                    index++;
                }
            }
        }

        private void Refresh(int currentPage, int totalPage)
        {
            int capacity = _pageCapacity * _preparePage;
            int beforeIndex = Math.Max(0, currentPage - 1) * _pageCapacity;
            int nextIndex = Math.Min(totalPage, currentPage + 1) * _pageCapacity;
            int endIndex = Math.Min(nextIndex, _dataList.Count);
            for (int i = beforeIndex; i < endIndex; i++)
            {
                _list.ItemList[i - beforeIndex].Data = _dataList[i];
                _list.ItemList[i - beforeIndex].Visible = true;
            }
            for (int j = endIndex; j < beforeIndex + capacity; j++)
            {
                _list.ItemList[j - beforeIndex].Visible = false;
            }
        }

        protected virtual void UpdateItemListLayout(int currentPage, int totalPage)
        {
            int index = Math.Max(0, currentPage - 1) * _pageCapacity;
            List<KList.KListItemBase> _itemList = _list.ItemList;
            for (int i = 0; i < _itemList.Count; i++)
            {
                KList.KListItemBase item = _itemList[i];
                if (item.gameObject.activeSelf)
                {
                    RectTransform itemRect = item.GetComponent<RectTransform>();
                    Vector2 position = CalculateItemPosition(index, itemRect, totalPage);

                    itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                    itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                    item.Index = i;
                    item.name = KList.ITEM_PREFIX + index;
                    index++;
                }
            }
        }

        private Vector2 CalculateItemPosition(int index, RectTransform itemRect, int totalPage)
        {
            Vector2 result = Vector2.zero;
            result.x = _list.Padding.left + ((int)(index / _pageCapacity) * _list.LeftToRightCount + index % _list.LeftToRightCount) * (itemRect.sizeDelta.x + _list.LeftToRightGap);
            result.y = _list.Padding.top + (int)((index % _pageCapacity) / _list.LeftToRightCount) * (itemRect.sizeDelta.y + _list.TopToDownGap);
            return result;
        }

        private void OnCurrentPageChanged(KScrollPage scrollPage, int index)
        {
            UpdateContent(_scrollPage.CurrentPage+1, _scrollPage.TotalPage);
            onCurrentPageChanged.Invoke(this, index);
        }

    }
}
