﻿#region 模块信息
/*==========================================
// 文件名：PageableComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.PageableComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/12 9:59:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class PageableScrollPage : KContainer
    {
        private KScrollPage _scrollPage;
        private KList _list;

        public KComponentEvent<PageableScrollPage, int> onRequestPage = new KComponentEvent<PageableScrollPage, int>();
        public KComponentEvent<PageableScrollPage, int> onCurrentPageChanged = new KComponentEvent<PageableScrollPage, int>();

        private int _capacity = 20;
        private int _pageCapacity = 4;

        private int _currentPage;
        private int _totalPage;

        public KList List
        {
            get { return _list; }
        }

        //public void SetDirection(KScrollPage.KScrollPageDirection direction)
        //{
        //    _scrollPage.SetDirection(direction);
        //}

        protected override void Awake()
        {
            _scrollPage = GetComponent<KScrollPage>();
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            base.Awake();
            _scrollPage.onRequestPrePage.AddListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.AddListener(OnRequestNextPage);
            _scrollPage.onCurrentPageChanged.AddListener(OnCurrentPageChanged);
        }

        protected override void OnDestroy()
        {
            _scrollPage.onRequestPrePage.RemoveListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.AddListener(OnRequestNextPage);
            _scrollPage.onCurrentPageChanged.RemoveListener(OnCurrentPageChanged);
            base.OnDestroy();
        }

        public void SetPageCapacity(int pageCapacity)
        {
            _pageCapacity = pageCapacity;
        }

        public void SetCapacity(int capacity)
        {
            _capacity = capacity;
        }

        private void Init<T>() where T : KList.KListItemBase
        {
            List<object> dataList = new List<object>();
            for (int i = 0; i < _capacity; i++)
            {
                dataList.Add(null);
            }
            _list.SetDataList<T>(dataList);
        }

        public void SetDataList<T>(IList dataList,int currentPage,int totalPage) where T : KList.KListItemBase
        {
            _currentPage = currentPage;
            _totalPage = totalPage;
            if (_list.ItemList.Count == 0)
            {
                Init<T>();
            }
            int count = Math.Min(_capacity,dataList.Count);
            for(int i=0;i<count;i++)
            {
                _list.ItemList[i].Data = dataList[i];
                _list.ItemList[i].gameObject.SetActive(true);
            }
            for (int j = count; j < _capacity; j++)
            {
                _list.ItemList[j].gameObject.SetActive(false);
            }
            _list.DoLayout();
            _scrollPage.TotalPage = Math.Max(1,Mathf.CeilToInt((float)dataList.Count / _pageCapacity));
            _scrollPage.CurrentPage = 1;
            _scrollPage.Dragable = true;
        }

        private void OnCurrentPageChanged(KScrollPage scrollPage,int index)
        {
            onCurrentPageChanged.Invoke(this,index);
            _scrollPage.UpdateArrow((_currentPage - 1) * (_capacity / _pageCapacity) + index, (_totalPage - 1) * (_capacity / _pageCapacity) + _scrollPage.TotalPage);
        }

        private void OnRequestNextPage(KScrollPage scrollPage)
        {
            _scrollPage.Dragable = false;
            onRequestPage.Invoke(this, Math.Min(_currentPage + 1, _totalPage));
        }

        private void OnRequestPrePage(KScrollPage scrollPage)
        {
            _scrollPage.Dragable = false;
            onRequestPage.Invoke(this, Math.Max(1, _currentPage - 1));
        }
    }
}
