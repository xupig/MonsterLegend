﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Game.UI.UIComponent;
using Common.Global;
using Common.Structs;
using Common.Data;
using Common.Utils;
using GameData;
using UnityEngine.Events;
using Game.UI;

namespace Common.ExtendComponent
{
    public class IconContainer : KContainer
    {
        private int _currentIcon = -1;
        private string _currentSpriteName;

        public void SetIcon(int iconId)
        {
            if (_currentIcon != iconId)
            {
                _currentIcon = iconId;
                MogoAtlasUtils.AddIcon(this.gameObject, iconId);
            }
        }

        public void SetIcon(int iconId, Action<GameObject> callback)
        {
            _currentIcon = iconId;
            MogoAtlasUtils.AddIcon(this.gameObject, iconId, callback);
        }

        public void SetMoneyIcon(int moneyId)
        {
            int iconId = item_helper.GetIcon(moneyId);
            if(_currentIcon!=iconId)
            {
                _currentIcon = iconId;
                MogoAtlasUtils.AddIcon(this.gameObject, iconId);
            }
        }

        public void AddSprite(string spriteName)
        {
            if (_currentSpriteName!=spriteName)
            {
                _currentSpriteName = spriteName;
                MogoAtlasUtils.AddSprite(gameObject, spriteName);
            }
        }

        public void RemoveSprite()
        {
            MogoAtlasUtils.RemoveSprite(gameObject);
            _currentIcon = -1;
            _currentSpriteName = string.Empty;
        }

        //public void AddSprite(string spriteName, Action<GameObject> callback)
        //{
        //    if (_currentSpriteName != spriteName)
        //    {
        //        _currentSpriteName = spriteName;
        //        MogoAtlasUtils.AddSprite(gameObject, spriteName, callback);
        //    }
        //}

        //public void AddSprite(string spriteName, string bgSpriteName, int colorId, Action<GameObject> callback = null)
        //{
        //    MogoAtlasUtils.AddSprite(gameObject, spriteName, bgSpriteName, colorId, callback);
        //}
    }
}
