﻿#region 模块信息
/*==========================================
// 文件名：PointerDownComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/19 15:07:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class PointerDownComponent : KContainer, IPointerDownHandler
    {
        public KComponentEvent<PointerEventData> onClick = new KComponentEvent<PointerEventData>();
        public void OnPointerDown(PointerEventData eventData)
        {
            onClick.Invoke(eventData);
        }
    }
}
