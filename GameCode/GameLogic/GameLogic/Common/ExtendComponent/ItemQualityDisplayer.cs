﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Game.UI.UIComponent;
using Common.Global;
using Common.Utils;
using Common.ServerConfig;

namespace Common.ExtendComponent
{
    
    public class ItemQualityDisplayer : KContainer
    {
        public static string[] IMAGE_NAME_LIST = new string[] { "Bai", "Lv", "Lan", "Zi", "Jin", "Huang", "Hong" };
        public static string OVERLAY = "Over";
        public static string LARGE_PREFIX = "l";

        public string prefix = "s";

        private int _quality = 0;
        private Action<GameObject> _loadCallback;
        private GameObject _overlay;


        protected override void Awake()
        {
            base.Awake();
        }

        public int Quality
        {
            get
            {
                return _quality;
            }
            set
            {
                value = Mathf.Clamp(value, (int)ItemQualityDefine.WHITE, (int)ItemQualityDefine.RED);
                if (_quality != value)
                {
                    MogoAtlasUtils.AddSprite(gameObject, prefix + IMAGE_NAME_LIST[value - 1], OnSpriteLoaded);
                    RefreshOverlay(value);
                    _quality = value;
                }
            }
        }

        private void RefreshOverlay(int quality)
        {
            if(prefix == LARGE_PREFIX && _quality != quality)
            {
                if(_overlay != null)
                {
                    MogoAtlasUtils.RemoveSprite(_overlay);
                }
                if(prefix == LARGE_PREFIX && (quality == public_config.ITEM_QUALITY_GOLDEN || quality == public_config.ITEM_QUALITY_RED))
                {
                    if(_overlay == null)
                    {
                        _overlay = new GameObject("yanseOver");
                        RectTransform rect = _overlay.AddComponent<RectTransform>();
                        rect.pivot = new Vector2(0, 1);
                        rect.anchorMin = new Vector2(0, 1);
                        rect.anchorMax = new Vector2(0, 1);
                        rect.sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
                        _overlay.transform.SetParent(gameObject.transform.parent);
                        MogoGameObjectHelper.SetLocalScale(_overlay, gameObject.GetComponent<RectTransform>().localScale);
                        MogoGameObjectHelper.SetPosition(_overlay, gameObject.GetComponent<RectTransform>().localPosition);
                    }
                    MogoAtlasUtils.AddSprite(_overlay, prefix + IMAGE_NAME_LIST[quality - 1] + OVERLAY);
                }
            }
        }

        public void SetLoadCallback(Action<GameObject> callback)
        {
            _loadCallback = callback;
        }

        public void Clear()
        {
            _quality = 0;
            _loadCallback = null;
            MogoAtlasUtils.RemoveSprite(gameObject);
            if (_overlay != null)
            {
                MogoAtlasUtils.RemoveSprite(_overlay);
            }
        }

        private void OnSpriteLoaded(GameObject go)
        {
            if (_loadCallback != null)
            {
                _loadCallback(go);
            }
        }
    }
}
