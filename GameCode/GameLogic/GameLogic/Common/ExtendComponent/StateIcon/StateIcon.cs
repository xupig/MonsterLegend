﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 21:02:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class StateIcon : StateChangeable
    {
        private GameObject _normalGo;
        private GameObject _overGo;
        private GameObject _downGo;
        private GameObject _disableGo;

        private Dictionary<string, GameObject> _goDict = new Dictionary<string,GameObject>();
        private Action<GameObject> _callback;

        private int _currentIcon = -1;
        private int _loadedIconNum;

        private bool _hasInitial = false;

        protected override void Awake()
        {
            Initial();
            base.Awake();
        }

        public void Initial()
        {
            _hasInitial = true;
            SetGameObject(KButtonState.NORMAL, ref _normalGo);
            SetGameObject(KButtonState.OVER, ref _overGo);
            SetGameObject(KButtonState.DOWN, ref _downGo);
            SetGameObject(KButtonState.DISABLE, ref _disableGo);
            ShowDefaultState();
        }

        private void ShowDefaultState()
        {
            if (transform.childCount == 0)
            {
                return;
            }
            GameObject gameObjectShow;
            if (_normalGo != null)
            {
                gameObjectShow = _normalGo;
            }
            else
            {
                gameObjectShow = transform.GetChild(0).gameObject;
            }
            foreach (KeyValuePair<string, GameObject> pair in _goDict)
            {
                if (pair.Value != gameObjectShow)
                {
                    pair.Value.SetActive(false);
                }
            }
        }

        private void SetGameObject(string name, ref GameObject go)
        {
            Transform goTransform= transform.FindChild(string.Format("Container_{0}",name));
            if(goTransform != null)
            {
                go = goTransform.gameObject;
                go.name = name;
                _goDict.Add(name, go);
            }
        }


        public void SetIcon(int iconId, Action<GameObject> callback = null)
        {
            if(_hasInitial == false)
            {
                Initial();
            }
            if (_currentIcon == iconId)
            {
                if (callback != null)
                {
                    callback.Invoke(gameObject);
                }
                return;
            }
            _loadedIconNum = 0;
            _callback = callback;
            _currentIcon = iconId;
            AddIconIfGameObjectExist(_normalGo, iconId, callback);
            AddIconIfGameObjectExist(_overGo, iconId, callback);
            AddIconIfGameObjectExist(_downGo, iconId, callback);
            AddIconIfGameObjectExist(_disableGo, iconId, callback);
        }

        private void AddIconIfGameObjectExist(GameObject go, int iconId, Action<GameObject> callback)
        {
            if (go != null)
            {
                if (callback != null)
                {
                    MogoAtlasUtils.AddIcon(go, iconId, LoadIconCallback);
                }
                else
                {
                    MogoAtlasUtils.AddIcon(go, iconId);
                }
            }
        }

        private void LoadIconCallback(GameObject obj)
        {
            _loadedIconNum++;
            if (_loadedIconNum == _goDict.Count)
            {
                _callback.Invoke(gameObject);
            }
        }
    }
}
