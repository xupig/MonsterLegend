﻿#region 模块信息
/*==========================================
// 文件名：LongClickComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/8/31 20:14:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class LongClickComponent:MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {

        public KComponentEvent<PointerEventData> onPointerDown = new KComponentEvent<PointerEventData>();
        public KComponentEvent<PointerEventData> onPointerUp = new KComponentEvent<PointerEventData>();

         public void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown.Invoke(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            onPointerUp.Invoke(eventData);
        }
    }
}
