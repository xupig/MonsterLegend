﻿#region 模块信息
/*==========================================
// 文件名：ClickableComponent
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/20 10:04:11
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class ClickableComponent : KContainer, IPointerClickHandler
    {
        public KComponentEvent<PointerEventData> onClick = new KComponentEvent<PointerEventData>();
        public override void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke(eventData);
        }
    }
}
