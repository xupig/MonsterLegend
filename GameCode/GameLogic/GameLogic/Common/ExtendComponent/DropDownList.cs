﻿#region 模块信息
/*==========================================
// 文件名：DropDownList
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/2 14:27:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class DropDownList : KContainer
    {
        private StateImage _img;
        private RectTransform _rectTransform;
        private KList _list;
        public KComponentEvent<DropDownList, DropDownItem> onClick = new KComponentEvent<DropDownList, DropDownItem>();

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _img = GetChildComponent<StateImage>("Image_content");
            _rectTransform = _img.CurrentImage.GetComponent<RectTransform>();
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            base.Awake();
        }


        protected override void OnDestroy()
        {
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            base.OnDestroy();
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            onClick.Invoke(this, _list.ItemList[index] as DropDownItem);
        }

        public void SetGap(int topToDownGap, int leftToRightGap)
        {
            _list.SetGap(topToDownGap, leftToRightGap);
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _list.SetPadding(top, right, bottom, left);
        }

        public void SetDataList<T>(IList dataList) where T : DropDownItem
        {
            _list.RemoveAll();
            _list.SetDataList<T>(dataList);
            if (_list.ItemList.Count > 0)
            {
                (_list.ItemList[_list.ItemList.Count - 1] as DropDownItem).HideSplitLine();
            }
            RectTransform listRect = _list.GetComponent<RectTransform>();
            _img.SetDimensionsDirty();
            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, listRect.sizeDelta.y);
        }
    }

    public class DropDownItem : KList.KListItemBase
    {

        private StateImage _imgSplitLine;

        protected override void Awake()
        {
            if (GetChild("Image_split") != null)
            {
                _imgSplitLine = GetChildComponent<StateImage>("Image_split");
            }
        }

        public void HideSplitLine()
        {
            if (_imgSplitLine != null)
            {
                _imgSplitLine.gameObject.SetActive(false);
            }
        }

        public override void Dispose()
        {

        }

    }

}
