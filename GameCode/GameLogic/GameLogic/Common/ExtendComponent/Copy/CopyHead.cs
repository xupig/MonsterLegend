﻿using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyHead : KContainer
    {
        private IconContainer _containerIcon;
        private LStarList _starList;
        private StateText _textTitle;

        protected override void Awake()
        {
            _containerIcon = AddChildComponent<IconContainer>("Container_fuben");
            _starList = AddChildComponent<LStarList>("Container_xingxing");
            _textTitle = GetChildComponent<StateText>("Container_biaoti/Label_txtFuben");
            _textTitle.CurrentText.alignment = TextAnchor.UpperCenter;
        }

        public void Refresh(int instanceId, int historyMaxStar)
        {
            _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(instance_helper.GetName(instanceId));
            _starList.SetNum(historyMaxStar);
            _containerIcon.SetIcon(instance_helper.GetIcon(instanceId));
        }
    }
}
