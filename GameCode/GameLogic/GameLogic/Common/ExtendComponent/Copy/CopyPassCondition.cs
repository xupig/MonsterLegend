﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent
{
    public class CopyPassCondition : KContainer
    {
        private StateText _textDescribe;

        protected override void Awake()
        {
            _textDescribe = GetChildComponent<StateText>("Label_txtNr");
        }

        public void SetDescribe(string describe)
        {
            _textDescribe.CurrentText.text = describe;
        }
    }
}
