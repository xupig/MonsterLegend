﻿using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent
{
    public class CopyPassConditionList : KContainer
    {
        private const int MAX_CONDITION_NUM = 3;

        private List<CopyPassCondition> _conditionList;

        protected override void Awake()
        {
            _conditionList = new List<CopyPassCondition>();
            for (int i = 0; i < MAX_CONDITION_NUM; i++)
            {
                CopyPassCondition condition = AddChildComponent<CopyPassCondition>(string.Format("Container_NR0{0}", i));
                _conditionList.Add(condition);
            }
        }

        public void Refresh(int instanceId)
        {
            instance instance = instance_helper.GetInstanceCfg(instanceId);
            int mapId = instance_helper.GetMapId(instance);
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);
            int count = 0;
            foreach (KeyValuePair<string, string[]> pair in dicScoreRule)
            {
                string desc = map_helper.GetDesc(mapId, int.Parse(pair.Key));
                _conditionList[count].SetDescribe(
                    string.Format(desc, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_CHAT_RUMOR, map_helper.ParseScoreRuleParam(pair.Value[0])))
                    );
                count++;
            }
            for (int i = 0; i < _conditionList.Count; i++)
            {
                _conditionList[i].Visible = i < dicScoreRule.Count;
            }
        }
    }
}
