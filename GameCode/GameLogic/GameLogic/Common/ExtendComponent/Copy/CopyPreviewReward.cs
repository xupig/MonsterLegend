﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent
{
    public class CopyPreviewReward : KContainer
    {
        private KList _rewardList;

        protected override void Awake()
        {
            _rewardList = GetChildComponent<KList>("List_content");
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _rewardList.SetGap(0, 28);
        }

        public void Refresh(int instanceId)
        {
            _rewardList.RemoveAll();
            List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(instanceId, RewardLevel.S);
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _rewardList.SetDataList<RewardGrid>(rewardDataList);
        }
    }
}
