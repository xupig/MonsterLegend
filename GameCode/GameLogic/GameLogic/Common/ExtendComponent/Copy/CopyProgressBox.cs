﻿using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;

namespace Common.ExtendComponent
{
    public class CopyProgressBox : KList.KListItemBase
    {
        private chapters_helper.StarConfig _starConfig;
        private BoxState _boxState;

        private KButton _btnOpen;
        private StateText _textOpenStarNum;
        private KButton _btnClose;
        private StateText _textCloseStarNum;
        private StateIcon _stateIconOpen;
        private StateIcon _stateIconClose;
        private KParticle _particleHaveReward;

        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        protected override void Awake()
        {
            _stateIconOpen = GetChildComponent<StateIcon>("Button_xiangziopen/stateIcon");
            _stateIconClose = GetChildComponent<StateIcon>("Button_xiangziclose/stateIcon");

            _btnOpen = GetChildComponent<KButton>("Button_xiangziopen");
            _textOpenStarNum = GetChildComponent<StateText>("Button_xiangziopen/label");

            _btnClose = GetChildComponent<KButton>("Button_xiangziclose");
            _textCloseStarNum = GetChildComponent<StateText>("Button_xiangziclose/label");
            _particleHaveReward = AddChildComponent<KParticle>("Button_xiangziclose/fx_ui_24_2_baoxianglingqu");

            AddListener();
        }

        public override void Dispose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnOpen.onClick.AddListener(ShowCopyBox);
            _btnClose.onClick.AddListener(ShowCopyBox);
        }

        private void RemoveListener()
        {
            _btnOpen.onClick.RemoveListener(ShowCopyBox);
            _btnClose.onClick.RemoveListener(ShowCopyBox);
        }

        protected virtual void ShowCopyBox()
        {
            if (chapters_helper.GetChapterType(_starConfig.ChapterId) == ChapterType.WanderLand)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.WanderLandBox, new CopyProgressBoxDataWrapper(_starConfig.ChapterId, _starConfig.StarNum));
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.CopyBox, new CopyProgressBoxDataWrapper(_starConfig.ChapterId, _starConfig.StarNum));
            }
        }

        public override object Data
        {
            get
            {
                return _starConfig;
            }
            set
            {
                _starConfig = value as chapters_helper.StarConfig;
                BoxState boxState = _copyData.GetBoxState(_starConfig.ChapterId, _starConfig.StarNum);
                _boxState = boxState;
                Refresh(boxState);
            }
        }

        private void Refresh(BoxState boxState)
        {
            RefreshBox(boxState);
            RefreshParticle(boxState);
            RefreshStarNum();
            TweenTreasureBox.Stop(gameObject);
        }

        private void RefreshBox(BoxState boxState)
        {
            if (boxState == BoxState.HAD_GOT)
            {
                _btnOpen.Visible = true;
                _stateIconOpen.SetIcon(_starConfig.GotIconId);
                _btnClose.Visible = false;
            }
            else
            {
                _btnClose.Visible = true;
                _stateIconClose.SetIcon(_starConfig.UngetIconId);
                _btnOpen.Visible = false;
            }
        }

        private void RefreshParticle(BoxState boxState)
        {
            switch (boxState)
            {
                case BoxState.HAD_GOT:
                case BoxState.CANNOT_GET:
                    _particleHaveReward.Visible = false;
                    break;
                case BoxState.CAN_GET:
                    _particleHaveReward.Visible = true;
                    break;
            }
        }

        private void RefreshStarNum()
        {
            _textCloseStarNum.ChangeAllStateText(_starConfig.StarNum.ToString());
            _textOpenStarNum.ChangeAllStateText(_starConfig.StarNum.ToString());
        }

        public void DoTween()
        {
            TweenTreasureBox.ResetPosition(gameObject);
            if (_boxState == BoxState.CAN_GET)
            {
                TweenTreasureBox.Begin(gameObject);
            }
            else
            {
                TweenTreasureBox.Stop(gameObject);
            }
        }
    }
}
