﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common.ExtendComponent
{
    public class CopyTeamView : KContainer
    {
        private int _instanceId;

        private KContainer _teamContainer;
        private StateText _noTeamText;
        private List<CopyTeamItem> _itemList;
        private KButton _btnInvite;

        protected override void Awake()
        {
            _teamContainer = GetChildComponent<KContainer>("Container_duiwu");
            _noTeamText = GetChildComponent<StateText>("Label_txtDangqianweiyou");
            _itemList = new List<CopyTeamItem>();
            for (int i = 0; i < 4; i++)
            {
                _itemList.Add(AddChildComponent<CopyTeamItem>(string.Format("Container_duiwu/Button_duiyuan{0}", i + 1)));
            }
            if (GetChild("Button_yaoqing") != null)
            {
                _btnInvite = GetChildComponent<KButton>("Button_yaoqing");
            }
        }

        public void RefreshContent(int instanceId)
        {
            _instanceId = instanceId;
            RefreshContent();
        }

        private void RefreshContent()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            _noTeamText.Visible = true;
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].Visible = false;
            }
            if (teamData.TeammateDic.Count == 0)
            {
                return;
            }
            _noTeamText.Visible = false;
            RefreshTeammateButton(teamData);
            RefreshInviteButton();
        }

        private void RefreshTeammateButton(TeamData data)
        {
            List<PbTeamMember> memberList = GetMemberList();
            for (int i = 0; i < memberList.Count; i++)
            {
                _itemList[i].Visible = true;
                _itemList[i].RefreshContent(memberList[i].dbid, _instanceId);
            }
        }

        private List<PbTeamMember> GetMemberList()
        {
            List<PbTeamMember> result = new List<PbTeamMember>();
            foreach (PbTeamMember memberInfo in PlayerDataManager.Instance.TeamData.TeammateDic.Values)
            {
                if (memberInfo.dbid == PlayerDataManager.Instance.TeamData.CaptainId)
                {
                    result.Insert(0, memberInfo);
                }
                else
                {
                    result.Add(memberInfo);
                }
            }
            return result;
        }

        private void RefreshInviteButton()
        {
            if (_btnInvite != null)
            {
                InstanceTeamType teamType = instance_helper.GetTeamType(_instanceId);
                if (teamType == InstanceTeamType.Team)
                {
                    _btnInvite.Visible = true;
                }
                else
                {
                    _btnInvite.Visible = false;
                }
            }
        }

        public void AddEventListener()
        {
            if(_btnInvite != null) _btnInvite.onClick.AddListener(OnInvite);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshContent);
        }

        public void RemoveEventListener()
        {
            if (_btnInvite != null) _btnInvite.onClick.RemoveListener(OnInvite);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshContent);
        }

        private void OnInvite()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83031), PanelIdEnum.MainUIField);
                return;
            }
            view_helper.OpenView(112, new InteractivePanelParameter(_instanceId));
        }

        protected override void OnEnable()
        {
            RefreshContent();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
    }

    public class CopyTeamItem : KContainer
    {
        private ulong _dbid;
        private int _instanceId;

        private IconContainer _icon;
        private KButton _btn;
        private Text _levelTxt;

        protected override void Awake()
        {
            base.Awake();
            _btn = gameObject.GetComponent<KButton>();
            _btn.onClick.AddListener(OnTeamButtonClick);
            _icon = AddChildComponent<IconContainer>("icon");
            _levelTxt = GetChildComponent<Text>("txtDengji");
        }

        private void OnTeamButtonClick()
        {
            view_helper.OpenView(112, new InteractivePanelParameter(_instanceId));
        }

        private void SetIcon(int iconId, Action<GameObject> callback)
        {
            _icon.SetIcon(iconId, callback);
        }

        private void SetLevel(int level)
        {
            if (_levelTxt != null)
            {
                _levelTxt.text = level.ToString();
            }
        }

        public void RefreshContent(ulong dbid, int instanceId)
        {
            _dbid = dbid;
            _instanceId = instanceId;
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_dbid))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[_dbid];
                SetIcon(MogoPlayerUtils.GetSmallIcon((int)memberInfo.vocation), OnIconLoaded);
                SetLevel((int)memberInfo.level);
            }
        }

        private void OnIconLoaded(GameObject go)
        {
            RefreshImageState();
        }

        private void RefreshOnlineContent(ulong dbid, int online)
        {
            RefreshImageState();
        }

        private void RefreshImageState()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_dbid))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[_dbid];
                ImageWrapper[] wrappers = _icon.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < wrappers.Length; i++)
                {
                    wrappers[i].SetGray(memberInfo.online);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

    }
}
