﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyProgress : KContainer
    {
        private float BOX_WIDTH;

        private KProgressBar _progressBar;
        private StateText _textProgress;
        private RectTransform _rectProgressBar;
        private KList _listBox;

        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        protected override void Awake()
        {
            _listBox = GetChildComponent<KList>("List_xiangzi");
            _listBox.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _progressBar = GetChildComponent<KProgressBar>("Container_jindu/ProgressBar_jindu");
            _textProgress = GetChildComponent<StateText>("Container_jindu/Label_txtJd");
            _rectProgressBar = _progressBar.GetComponent<RectTransform>();
            BOX_WIDTH = GetChildComponent<RectTransform>("List_xiangzi/item").sizeDelta.x;

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _listBox.onAllItemCreated.AddListener(OnAllItemCreated);
        }

        private void RemoveListener()
        {
            _listBox.onAllItemCreated.RemoveListener(OnAllItemCreated);
        }

        public void Refresh(int chapterId)
        {
            int totalStar = chapters_helper.GetTotalStar(chapterId);
            int nowStar = _copyData.GetTotalStarNum(chapterId);
            _progressBar.Value = nowStar / (float)totalStar;
            _textProgress.CurrentText.text = string.Format("{0}/{1}", nowStar, totalStar);

            List<chapters_helper.StarConfig> starConfigList = chapters_helper.GetStarConfigList(chapterId);
            _listBox.SetDataList<CopyProgressBox>(starConfigList, 1);
        }

        private void OnAllItemCreated()
        {
            RefreshLayout();
            DoTween();
        }

        private void RefreshLayout()
        {
            List<KList.KListItemBase> itemList = _listBox.ItemList;
            int maxStarNum = ((itemList[itemList.Count - 1] as CopyProgressBox).Data as chapters_helper.StarConfig).StarNum;
            for (int i = 0; i < itemList.Count; i++)
            {
                CopyProgressBox progressBox = itemList[i] as CopyProgressBox;
                chapters_helper.StarConfig starConfig = progressBox.Data as chapters_helper.StarConfig;
                RectTransform rect = progressBox.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(((float)starConfig.StarNum / maxStarNum) * _rectProgressBar.sizeDelta.x - BOX_WIDTH / 2, rect.anchoredPosition.y);
            }
        }

        private void DoTween()
        {
            List<KList.KListItemBase> itemList = _listBox.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                CopyProgressBox progressBox = itemList[i] as CopyProgressBox;
                progressBox.DoTween();
            }
        }
    }
}
