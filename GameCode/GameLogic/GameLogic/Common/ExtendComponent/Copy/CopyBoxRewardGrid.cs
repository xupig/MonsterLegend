﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyBoxRewardGrid : RewardItem
    {
        private StateImage _imageHaveGot;

        protected override void Awake()
        {
            base.Awake();
            _imageHaveGot = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _imageHaveGot.SetContinuousDimensionsDirty(true);
            _imageHaveGot.gameObject.AddComponent<RaycastComponent>();
            MogoLayoutUtils.SetMiddleCenter(_imageHaveGot.gameObject);
        }

        public override object Data
        {
            set
            {
                CopyBoxDataWrapper wrapper = value as CopyBoxDataWrapper;
                base.Data = wrapper.BaseItemData;
                RefreshBoxState(wrapper.BoxState);
            }
        }

        private void RefreshBoxState(BoxState boxState)
        {
            _imageHaveGot.Visible = boxState == BoxState.HAD_GOT;
        }

        public void DoTween()
        {
            TweenScale tween = TweenScale.Begin(_imageHaveGot.gameObject, 0.2f, Vector3.one);
            tween.from = new Vector3(5, 5, 1);
        }
    }
}
