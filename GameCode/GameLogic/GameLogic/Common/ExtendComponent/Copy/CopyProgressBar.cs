﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/18 14:03:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyProgressBar : KContainer
    {
        private const int StepNum = 50;
        private const int CostTime = 1000;

        private uint _timerId = TimerHeap.INVALID_ID;
        private float _step;
        private float _currentRate;
        private float _nowRate;
        private int _initLevel;

        private KProgressBar _progressBar;

        public KComponentEvent<int> Over100PercentOnce = new KComponentEvent<int>();

        protected override void Awake()
        {
            base.Awake();
            _progressBar = gameObject.GetComponent<KProgressBar>();
        }

        protected override void OnDisable()
        {
            DelTimer();
        }

        public void SetProgress(float rate)
        {
            _progressBar.Value = rate;
        }

        public void SetProgress(float lastRate, float nowRate)
        {
            lastRate = Mathf.Clamp(lastRate, 0f, 1f);
            if (nowRate < lastRate)
            {
                LoggerHelper.Debug(string.Format("error: nowRate:{0} is large than lastRate:{1}", nowRate, lastRate));
                return;
            }
            _nowRate = nowRate;
            _currentRate = lastRate;
            _progressBar.Value = _currentRate;
            _step = (nowRate - lastRate) / StepNum;
            _timerId = TimerHeap.AddTimer(CostTime / StepNum, CostTime / StepNum, OnRefresh);
        }

        private void OnRefresh()
        {
            if ((int)_currentRate != (int)(_currentRate + _step))
            {
                Over100PercentOnce.Invoke(_initLevel + (int)(_currentRate + _step));
            }
            _currentRate += _step;
            _currentRate = Mathf.Min(_currentRate, _nowRate);
            _progressBar.Value = (_currentRate - (int)_currentRate);
            if (_currentRate >= _nowRate)
            {
                DelTimer();
            }
        }

        private void DelTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        public void SetInitLevel(int initLevel)
        {
            _initLevel = initLevel;
        }
    }
}
