﻿#region 模块信息
/*==========================================
// 文件名：CategoryToggleGroup
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.CategoryToggleGroup
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/9 16:53:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.Utils.Timer;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using LitJson;
using Game.UI.Builder;
using System.Collections;
using GameMain.GlobalManager;
using GameLoader.Utils;

namespace Common.ExtendComponent
{
    public class CategoryList : KList,  IEndDragHandler, IDragHandler
    {
        private const float SemimajorAxisLength = 247f;
        private const float SemiminorAxisLength = 120f;
        private static float _centerPositionX = -81;

        private int _selectIndex = -1;
        private float _stepMove;
        private int _clickIndex = -1;

        private float gap = -72;
        private float _centerPositionY;

        private float _totalDetlaY = 0;
        private int _dragCenterIndex;
        private int _count = 0;
        private int _totalCount = 0;
        private uint _timerId;
        private static int TOTAL_FRAME = 5;
        private bool _isDraging;
        private int _lasetSelectedToggleItemIndex = -1;

        private List<int> _hideList;

        public new KComponentEvent<CategoryList, int> onSelectedIndexChanged = new KComponentEvent<CategoryList, int>();


        protected override void Awake()
        {
            base.Awake();
            _hideList = new List<int>();
            InitCategoryItemList();
            this.gameObject.AddComponent<KButtonHitArea>();
            RectTransform rect = gameObject.GetComponent<RectTransform>();
            _centerPositionY = (UIManager.PANEL_HEIGHT - rect.sizeDelta.y) / 2 - 319;
        }

        private void InitCategoryItemList()
        {
            CategoryListItem[] itemList = gameObject.GetComponentsInChildren<CategoryListItem>();
            if(itemList.Length > 0)
            {
                CategoryListItem preItem = null;
                for (int i = 0; i < itemList.Length; i++)
                {
                    itemList[i].Index = i;
                    _itemList.Insert(i, itemList[i]);
                    itemList[i].onClick.AddListener(OnItemClicked);
                    if (preItem != null)
                    {
                        gap = itemList[i].localPosition.y - preItem.localPosition.y;
                    }
                    preItem = itemList[i];
                }
                OnItemDragEnd(false);
            }
        }

        public void SetHiddenList(List<int> indexList)
        {
            _hideList = indexList;
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].gameObject.SetActive(true);
            }
            for(int i=0;i<indexList.Count;i++)
            {
                _itemList[indexList[i]].gameObject.SetActive(false);
            }
            if (indexList.Contains(SelectedIndex))
            {
                for (int i = 0; i < _itemList.Count; i++)
                {
                    if (indexList.Contains(i) == false)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }
            }
            UpdateItemListLayout();
        }

        public override void SetDataList<T>(IList dataList, int coroutineCreateCount = 0)
        {
            base.SetDataList<T>(dataList, coroutineCreateCount);
            Clear();
            OnItemDragEnd(false);
        }

        public void SetDataList(List<string> labelList, List<int> iconIdList, int coroutineCreateCount = 0)
        {
            List<CategoryItemData> list = new List<CategoryItemData>();
            for(int i = 0; i < labelList.Count; i++)
            {
                CategoryItemData data = new CategoryItemData();
                data.name = labelList[i];
                data.iconId = iconIdList[i];
                list.Add(data);
            }
            SetDataList<CategoryListItem>(list, coroutineCreateCount);
        }

        protected override void OnItemClicked(KList.KListItemBase item, int index)
        {

            //缓动
            if(_timerId!=0)
            {
                return;
            }
            if (index != _selectIndex)
            {
                _count = 0;
                _clickIndex = index;
                int hideCount = GetHideCount(index, _selectIndex);
                if (_selectIndex > index)
                {
                    _totalCount = (_selectIndex - index - hideCount) * TOTAL_FRAME;
                    _stepMove = gap / TOTAL_FRAME;
                }
                else
                {
                    _totalCount = (index - _selectIndex - hideCount) * TOTAL_FRAME;
                    _stepMove = -gap / TOTAL_FRAME;
                }
                _timerId = TimerHeap.AddTimer(0, 20, TweenUpdate);
            }
        }

        private void TweenUpdate()
        {
            try
            {
                OnItemDrag(_stepMove);
            }
            catch
            {
                TweenEnd();
                return;
            }
            _count++;
            if (_count >= _totalCount)
            {
                TweenEnd();
            }
        }
        private void TweenEnd()
        {
            if (_timerId!=0)
            {
                TimerHeap.DelTimer(_timerId);
            }
            _timerId = 0;
            _count = 0;
            _totalCount = 0;
            _dragCenterIndex = _clickIndex;
            OnItemDragEnd();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
                _count = 0;
                _totalCount = 0;
                _dragCenterIndex = _clickIndex;
                _selectIndex = _dragCenterIndex;
                _totalDetlaY = 0;
            }
        }

        private void Clear()
        {
            _timerId = 0;
            _count = 0;
            _totalCount = 0;
            _dragCenterIndex = 0;
        }

        private void OnItemDragEnd(bool dispatchEvent = true)
        {
            int oldSelectedIndex = _selectIndex;
            _selectIndex = _dragCenterIndex;
            _totalDetlaY = 0;
            UpdateItemListLayout();
            SelectedItem(_selectIndex);
            if(dispatchEvent && oldSelectedIndex  != _selectIndex)
            {
                onSelectedIndexChanged.Invoke(this, _selectIndex);
            }
        }

        private void SelectedItem(int index)
        {
            if (_lasetSelectedToggleItemIndex >= 0 && _lasetSelectedToggleItemIndex < _itemList.Count)
            {
                _itemList[_lasetSelectedToggleItemIndex].IsSelected = false;
            }
            _lasetSelectedToggleItemIndex = index;
            if (_lasetSelectedToggleItemIndex >= 0 && _lasetSelectedToggleItemIndex < _itemList.Count)
            {
                _itemList[_lasetSelectedToggleItemIndex].IsSelected = true;
            }
        }

        public new int SelectedIndex
        {
            get { return _selectIndex; }
            set
            {
                if (_selectIndex != -1)
                {
                    _itemList[_selectIndex].IsSelected = false;
                }
                if(value >= 0 && value <= _itemList.Count - 1)
                {
                    _dragCenterIndex = value;
                    _selectIndex = value;
                    _clickIndex = value;
                    UpdateItemListLayout();
                    SelectedItem(_selectIndex);
                }
                else
                {
                    _selectIndex = -1;
                }
            }

        }

        
        public void OnDrag(PointerEventData _data)
        {
            if (_timerId != 0)
            {
                return;
            }
            if (_data.delta.y != 0)
            {
                _isDraging = true;
                OnItemDrag(_data.delta.y / Common.Global.Global.Scale);
            }
        }

        public void OnEndDrag(PointerEventData _data)
        {
            if (_timerId == 0 && _isDraging)
            {
                _isDraging = false;
                OnItemDragEnd();
            }
        } 

        private void OnItemDrag(float delta)
        {
            int startIndex = 0;
            int endIndex = _itemList.Count - 1;
            for (int i = 0; i < _itemList.Count; i++)
            {
                if (_hideList.Contains(i) == false)
                {
                    startIndex = i;
                    break;
                }
            }
            for (int i = _itemList.Count - 1; i >= 0; i--)
            {
                if (_hideList.Contains(i) == false)
                {
                    endIndex = i;
                    break;
                }
            }
            CategoryListItem item = _itemList[endIndex] as CategoryListItem;
            if (delta > 0 && item.localPosition.y + delta > _centerPositionY)
            {
                return;
            }
            item = _itemList[startIndex] as CategoryListItem;
            if (delta < 0 && item.localPosition.y + delta < _centerPositionY)
            {
                return;
            }
            _totalDetlaY += delta;
            DragAndUpdateItemLayout();
        }

        protected override void UpdateItemListLayout()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                CategoryListItem item = _itemList[i] as CategoryListItem;
                if (CheckItemIsHide(item) == false)
                {
                    Vector2 position = CalculateItemPosition(i);
                    item.Visible = !(position.x == int.MaxValue && (position.y == int.MaxValue || position.y == int.MinValue));
                    item.localPosition = new Vector3(position.x, position.y, 0);
                }
            }
        }

        private bool CheckItemIsHide(CategoryListItem item)
        {
            return _hideList.Contains(item.Index);
        }

        private void DragAndUpdateItemLayout()
        {
            float distance = int.MaxValue;
            for (int i = 0; i < _itemList.Count; i++)
            {
                CategoryListItem item = _itemList[i] as CategoryListItem;
                if (CheckItemIsHide(item) == false)
                {
                    Vector2 position = CalculateItemPosition(i);
                    if (position.x == int.MaxValue && (position.y == int.MaxValue || position.y == int.MinValue))
                    {
                        item.gameObject.SetActive(false);
                    }
                    else
                    {
                        item.gameObject.SetActive(true);
                        float currentDistance = Math.Abs(position.y - _centerPositionY);
                        if (currentDistance < distance)
                        {
                            distance = currentDistance;
                            _dragCenterIndex = i;
                        }
                    }
                    item.localPosition = new Vector3(position.x, position.y, 0);
                } 
            }
            SelectedItem(_dragCenterIndex);
        }

        private Vector2 CalculateItemPosition(int index)
        {
            Vector2 result = Vector2.zero;
            int hideCount = GetHideCount(index, _selectIndex);
            float ellipseY = 0;
            if (index > _selectIndex)
            {
                ellipseY = (index - _selectIndex - hideCount) * gap + _totalDetlaY;
            }
            else
            {
                ellipseY = -(_selectIndex - index - hideCount) * gap + _totalDetlaY;
            }
            if (ellipseY > (gap - _centerPositionY))
            {
                result.y = int.MaxValue;
                result.x = int.MaxValue;
            }
            else if (ellipseY < (_centerPositionY - gap))
            {
                result.y = int.MinValue;
                result.x = int.MaxValue;
            }
            else
            {
                result.y = ellipseY + _centerPositionY;
                result.x = (float)CalculateEllipseX(ellipseY) + _centerPositionX;
            }
            return result;
        }

        private int GetHideCount(int start, int end)
        {
            if (end < start)
            {
                int temp = start;
                start = end;
                end = temp;
            }
            int count =0;
            for (int i = start; i < end; i++)
            {
                if (_hideList.Contains(i))
                {
                    count++;
                }
            }
            return count;
        }

        private double CalculateEllipseX(float ellipseY)
        {
            return Mathf.Sqrt(Mathf.Clamp(1 - ellipseY * ellipseY / (SemimajorAxisLength * SemimajorAxisLength), 0, 1)) * SemiminorAxisLength;
        }
    }
}
