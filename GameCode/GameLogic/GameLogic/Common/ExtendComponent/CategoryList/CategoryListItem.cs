﻿#region 模块信息
/*==========================================
// 文件名：ToggleItem
// 命名空间: 
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/9 16:58:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CategoryListItem : KList.KListItemBase
    {
        private StateText _txtSelectedName;
        private StateText _txtNotSelectedName;
        private IconContainer _selectedIcon;

        private RectTransform _rectTransform;
        private KButton _btnSelected;
        private KButton _btnNotSelected;

        private StateImage _greenPoint;
        private StateImage _greenPointSelected;

        private CategoryItemData _itemData;
        private bool _isSelected = false;

        protected override void Awake()
        {
            _btnSelected = GetChildComponent<KButton>("Button_checkmark");
            _btnSelected.Visible = false;
            _btnNotSelected = GetChildComponent<KButton>("Button_back");
            _rectTransform = gameObject.GetComponent<RectTransform>();

            _btnSelected.onClick.AddListener(OnClick);
            _btnNotSelected.onClick.AddListener(OnClick);

            InitNameText();
            InitGreenPoint();
        }

        public override bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                _btnSelected.Visible = _isSelected;
                if (_isSelected == true)
                {
                    RefreshSelectedName();
                }
                _btnNotSelected.Visible = !_isSelected;
            }
        }

        private void RefreshSelectedName()
        {
            if (_txtSelectedName != null && _itemData != null && string.IsNullOrEmpty(_itemData.name) == false)
            {
                _txtSelectedName.ChangeAllStateText(_itemData.name);
            }
        }

        private void OnClick()
        {
            onClick.Invoke(this, this.Index);
        }

        public override object Data
        {
            get
            {
                return _itemData;
            }
            set
            {
                _itemData = value as CategoryItemData;
                Refresh();
            }
        }

        private void InitNameText()
        {
            _txtSelectedName = _btnSelected.Label;
            _txtNotSelectedName = _btnNotSelected.Label;
            _selectedIcon = AddChildComponent<IconContainer>("Button_checkmark/icon");
        }

        private void InitGreenPoint()
        {
            _greenPoint = GetChildComponent<StateImage>("Button_back/xiaodian");
            _greenPointSelected = GetChildComponent<StateImage>("Button_checkmark/xiaodian");
            if (_greenPoint != null)
            {
                _greenPoint.Visible = false;
            }
            if (_greenPointSelected != null)
            {
                _greenPointSelected.Visible = false;
            }
        }

        private void Refresh()
        {
            if (_itemData != null)
            {
                if (_txtSelectedName != null)
                {
                    _txtSelectedName.ChangeAllStateText(_itemData.name);
                }
                if (_txtNotSelectedName != null)
                {
                    _txtNotSelectedName.ChangeAllStateText(_itemData.name);
                }
                if (_selectedIcon != null)
                {
                    _selectedIcon.SetIcon(_itemData.iconId);
                }
            }
        }

        public Vector3 localPosition
        {
            get
            { 
                if(_rectTransform!=null)
                {
                    return _rectTransform.anchoredPosition3D;
                }
                return _rectTransform.anchoredPosition3D;
            }
            set 
            {
                if(_rectTransform!=null)
                {
                    _rectTransform.anchoredPosition3D = value; 
                }
            }
        }


        public void SetPoint(bool isShow)
        {
            if (_greenPoint != null)
            {
                _greenPoint.Visible = isShow;
            }

            if (_greenPointSelected != null)
            {
                _greenPointSelected.Visible = isShow;
            }
        }

        public override void Dispose()
        {
            _btnSelected.onClick.RemoveListener(OnClick);
            _btnNotSelected.onClick.RemoveListener(OnClick);
        }

    }

    public class CategoryItemData
    {
        public string name;
        public int iconId;

        public static CategoryItemData GetCategoryItemData(string name, int iconId = 0)
        {
            CategoryItemData data = new CategoryItemData();
            data.name = name;
            data.iconId = iconId;
            return data;
        }

    }

}
