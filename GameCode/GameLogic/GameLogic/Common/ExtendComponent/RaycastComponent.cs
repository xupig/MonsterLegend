﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/10 15:48:56
 * 描述说明：点击事件穿透组件
 * *******************************************************/

using UnityEngine;
using Game.UI.UIComponent;

namespace Common.ExtendComponent
{
    /// <summary>
    /// 通过射线交互组件（默认不可交互）
    /// </summary>
    public class RaycastComponent : KContainer, ICanvasRaycastFilter
    {
        private bool _rayCast = false;

        public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            return _rayCast;
        }

        public bool RayCast
        {
            get
            {
                return _rayCast;
            }
            set
            {
                _rayCast = value;
            }
        }
    }
}
