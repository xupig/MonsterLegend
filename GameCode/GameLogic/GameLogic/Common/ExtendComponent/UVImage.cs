﻿using UnityEngine;
using UnityEngine.Sprites;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Game.UI;

namespace Common.ExtendComponent
{
    public class UVImage : ImageWrapper
    {
        protected override void OnFillVBO(List<UIVertex> vbo)
        {
            if (overrideSprite == null)
            {
                //Ari base.OnFillVBO(vbo);
                return;
            }
            if (type != Type.Simple)
            {
                if (Application.isEditor == true)
                {
                    Debug.LogWarning("UVImage only support the [Image.Type.Simple] option");
					//Ari base.OnFillVBO(vbo);
                    return;
                }
                else
                {
                    type = Type.Simple;
                }
            }
            GenerateSprite(vbo, preserveAspect);
            ApplyGreyAndAlpha(vbo);
        }

        private Vector2 _pos = Vector2.zero;
        public Vector2 Pos
        {
            get
            {
                return _pos;
            }
            set
            {
                _pos = value;
            }
        }

        private Vector2 _sizeDelta = Vector2.zero;
        public Vector2 SizeDelta
        {
            get
            {
                return _sizeDelta;
            }
            set
            {
                _sizeDelta = value;
            }
        }

        private Vector4 CalculateUV()
        {
            Vector4 uv = (overrideSprite != null) ? DataUtility.GetOuterUV(overrideSprite) : Vector4.zero;
            uv = new Vector4(uv.x + _pos.x, uv.y + _pos.y, uv.x + _pos.x + _sizeDelta.x, uv.y + _pos.y + _sizeDelta.y);
            return uv;
        }

        private void GenerateSprite(List<UIVertex> vbo, bool preserveAspect)
        {
            var vert = UIVertex.simpleVert;
            vert.color = color;

			Vector4 v = Vector4.zero;//Ari GetDrawingDimensions(preserveAspect);
            Vector4 uv = CalculateUV();

            vert.position = new Vector3(v.x, v.y);
            vert.uv0 = new Vector2(uv.x, uv.y);
            vbo.Add(vert);

            vert.position = new Vector3(v.x, v.w);
            vert.uv0 = new Vector2(uv.x, uv.w);
            vbo.Add(vert);

            vert.position = new Vector3(v.z, v.w);
            vert.uv0 = new Vector2(uv.z, uv.w);
            vbo.Add(vert);

            vert.position = new Vector3(v.z, v.y);
            vert.uv0 = new Vector2(uv.z, uv.y);
            vbo.Add(vert);
        }
    }
}
