﻿using Game.UI.UIComponent;

namespace Common.ExtendComponent
{
    public class RankContainer : KContainer
    {
        private StateImage _firstImg;
        private StateImage _secondImg;
        private StateImage _thridImg;
        private StateText _rankingTxt;

        protected override void Awake()
        {
            _firstImg = GetChildComponent<StateImage>("Image_firstLcon");
            _secondImg = GetChildComponent<StateImage>("Image_secondLcon");
            _thridImg = GetChildComponent<StateImage>("Image_thirdLcon");
            _rankingTxt = GetChildComponent<StateText>("Label_txtPaiming");
        }


        public void SetRank(int rank)
        {
            _firstImg.Visible = rank == 1;
            _secondImg.Visible = rank == 2;
            _thridImg.Visible = rank == 3;
            _rankingTxt.Visible = rank >= 4;
            _rankingTxt.CurrentText.text = rank.ToString();
        }
    }

}
