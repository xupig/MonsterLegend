﻿#region 模块信息
/*==========================================
// 文件名：KEffect
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/13 20:51:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    [RequireComponent(typeof(SortOrderedRenderAgent))]
    public class KParticle : UIBehaviour
    {
        public KComponentEvent onComplete = new KComponentEvent();
        private uint _timeId;
        private ParticleSystemWrapper _wrapper;
        private bool _isPlaying = false;
        private List<ParticleSystem> particleSystemList;

        public bool IsSortWhenEnable = true;
        private bool isFirst = true;

        protected override void Awake()
        {
            //enabled = false;
            ChangeGameObjectLayer(gameObject);
            transform.localPosition = Vector3.zero;
            transform.localRotation = new Quaternion();
            transform.localScale = Vector3.one;
            _wrapper = GetComponent<ParticleSystemWrapper>();

            particleSystemList = new List<ParticleSystem>();
            GetComponentsInChildren<ParticleSystem>(true, particleSystemList);
            RecalculateParticleScale(1);

        }
        
        public Vector3 Position
        {
            get { return transform.localPosition; }
            set { transform.localPosition = value; }
        }

        protected override void OnEnable()
        {
            if (IsSortWhenEnable || isFirst)
            {
                SortOrderedRenderAgent.RebuildAll();
            }
            isFirst = false;
            base.OnEnable();
        }

        protected override void OnDestroy()
        {
            Stop();
            particleSystemList.Clear();
            particleSystemList = null;
        }

        private float _scale = 1;
        public float Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                if (_scale != value)
                {
                    RecalculateParticleScale(value);
                    _scale = value;
                }
            }
        }

        private void RecalculateParticleScale(float scale)
        {
            float rate = UIManager.PANEL_HEIGHT / UIManager.CANVAS_HEIGHT;
            for (int i = 0; i < particleSystemList.Count; i++)
            {
                particleSystemList[i].startSize = rate * particleSystemList[i].startSize / _scale * scale;
            }
        }

        private void ChangeGameObjectLayer(GameObject parent)
        {
            parent.layer = UIManager.UI_LAYER;
            Transform[] trans = parent.GetComponentsInChildren<Transform>();
            for (int i = 0; i < trans.Length;i++ )
            {
                trans[i].gameObject.layer = UIManager.UI_LAYER;
            }
        }

        public void Play(bool isLoop = false, bool reset = true)
        {
            if (reset == true || _isPlaying == false)
            {
                Stop();
                gameObject.SetActive(true);
                _isPlaying = true;
                if (isLoop == false)
                {
                    _timeId = TimerHeap.AddTimer((uint)(_wrapper.lifeTime * 1000), 1000, OnPlayEnd);
                }
            }
        }

        public void Stop()
        {
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
            gameObject.SetActive(false);
            _isPlaying = false;
        }

        private void OnPlayEnd()
        {
            Stop();
            onComplete.Invoke();
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}
