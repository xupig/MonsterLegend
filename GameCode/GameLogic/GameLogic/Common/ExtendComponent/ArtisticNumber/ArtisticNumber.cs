﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 16:40:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using Game.UI;
using Common.Utils;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class ArtisticNumber : KContainer
    {
        private const int MAX_NUMBER_COUNT = 10;

        private List<ImageWrapper> _imageList;

        public void Init(string imageName)
        {
            _imageList = new List<ImageWrapper>();
            for (int i = 0; i < MAX_NUMBER_COUNT; i++)
            {
                ImageWrapper imageWrapper = GetChildComponent<ImageWrapper>(string.Format("{0}{1}", imageName, i));
                imageWrapper.SetDimensionsDirty();
                MogoLayoutUtils.SetMiddleCenter(imageWrapper.gameObject, 2);
                _imageList.Add(imageWrapper);
            }
        }

        public int Data
        {
            set
            {
                Refresh(value);
            }
        }

        private void Refresh(int value)
        {
            for (int i = 0; i < MAX_NUMBER_COUNT; i++)
            {
                ImageWrapper imageWrapper = _imageList[i];
                if (i == value)
                {
                    imageWrapper.gameObject.SetActive(true);
                }
                else
                {
                    imageWrapper.gameObject.SetActive(false);
                }
            }
        }
    }
}
