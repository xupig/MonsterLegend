﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 16:42:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using GameData;

namespace Common.ExtendComponent
{
    public class ArtisticNumberList : KContainer
    {
        private const int MAX_PLACES = 10;

        private List<ArtisticNumber> artisticNumberList;

        public void Init(string itemName, string imageName)
        {
            artisticNumberList = new List<ArtisticNumber>();
            for (int i = 0; i < MAX_PLACES; i++)
            {
                ArtisticNumber number = AddChildComponent<ArtisticNumber>(string.Format("{0}{1}", itemName, i));
                if (number == null)
                {
                    break;
                }
                number.Init(imageName);
                artisticNumberList.Add(number);
            }
        }

        public void Refresh(int value, bool padZero = false)
        {
            List<int> placeValueList;
            if(padZero)
            {
                placeValueList = global_helper.ConvertIntToDigitalList(value, (uint)artisticNumberList.Count);
            }
            else
            {
                placeValueList = global_helper.ConvertIntToDigitalList(value);
            }
            for (int i = 0; i < artisticNumberList.Count; i++)
            {
                if (i < placeValueList.Count)
                {
                    artisticNumberList[i].Visible = true;
                    artisticNumberList[i].Data = placeValueList[i];
                }
                else
                {
                    artisticNumberList[i].Visible = false;
                }
            }
        }
    }
}
