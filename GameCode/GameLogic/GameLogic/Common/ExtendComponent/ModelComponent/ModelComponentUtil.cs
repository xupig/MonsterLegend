﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using System.Collections.Generic;

namespace Common.ExtendComponent
{
    public class ModelComponentUtil
    {
        private static ChildrenComponentsPool<List<Renderer>> _rendererListPool = new ChildrenComponentsPool<List<Renderer>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<Transform>> _transformListPool = new ChildrenComponentsPool<List<Transform>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<ParticleSystem>> _particleSystemListPool = new ChildrenComponentsPool<List<ParticleSystem>>(null, l => l.Clear());

        public static void ChangeUIModelParam(GameObject go)
        {
            ChangeLightDirection(go);
            ChangeGameObjectLayer(go);
            AddParticleScale(go);
            SortOrderedRenderAgent.RebuildAll();
        }

        public static void ChangeLightDirection(GameObject go)
        {
            List<Renderer> rendererList = _rendererListPool.Get();
            go.transform.GetComponentsInChildren<Renderer>(true, rendererList);
            for (int i = 0; i < rendererList.Count; i++)
            {
                Material mat = rendererList[i].material;
                if (mat.HasProperty("_SelfShadingLightDirX") &&
                    mat.HasProperty("_SelfShadingLightDirY") &&
                    mat.HasProperty("_SelfShadingLightDirZ"))
                {
                    Vector3 lightDir = new Vector3();
                    lightDir.x = mat.GetFloat("_SelfShadingLightDirX");
                    lightDir.y = mat.GetFloat("_SelfShadingLightDirY");
                    lightDir.z = mat.GetFloat("_SelfShadingLightDirZ");
                    Vector3 view = UIManager.Instance.UICamera.transform.forward;

                    if (Vector3.Dot(lightDir, view) > 0)
                    {
                        mat.SetFloat("_SelfShadingLightDirX", -lightDir.x);
                        mat.SetFloat("_SelfShadingLightDirZ", -lightDir.z);
                    }
                }
            }
            _rendererListPool.Release(rendererList);
        }

        public static void ChangeGameObjectLayer(GameObject parent)
        {
            List<Transform> transformList = _transformListPool.Get();
            parent.GetComponentsInChildren<Transform>(true, transformList);
            for (int i = 0; i < transformList.Count; i++)
            {
                transformList[i].gameObject.layer = UnityEngine.LayerMask.NameToLayer("UI");
            }
            _transformListPool.Release(transformList);
        }

        public static void AddParticleScale(GameObject parent)
        {
            List<ParticleSystem> particleSystemList = _particleSystemListPool.Get();
            parent.GetComponentsInChildren<ParticleSystem>(true, particleSystemList);
            for (int i = 0; i < particleSystemList.Count; i++)
            {
                ParticleScaler scaler = particleSystemList[i].GetComponent<ParticleScaler>();
                if (scaler == null)
                {
                    scaler = particleSystemList[i].gameObject.AddComponent<ParticleScaler>();
                }
            }
            _particleSystemListPool.Release(particleSystemList);
        }
    }
}
