﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;

namespace Common.ExtendComponent
{
    public class ModelDragComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private RectTransform _rectTransform;
        private Vector2 _currentPosition;
        private GameObject _modelGo;

        private const float ROTE_SPEED = 1.0f;

        /// <summary>
        /// 是否可拖动
        /// </summary>
        public bool Dragable { get; set; }

        protected void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            this.gameObject.AddComponent<KButtonHitArea>();
            Dragable = true;
        }

        public void SetModel(GameObject modelGo)
        {
            _modelGo = modelGo;
        }
        
        public void Clear()
        {
            _modelGo = null;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
            if (_modelGo == null)
            {
                return;
            }
            Vector2 localPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out localPosition))
            {
                float length = (localPosition - _currentPosition).x;
                _currentPosition = localPosition;
                if (_modelGo != null)
                {
                    _modelGo.transform.Rotate(new Vector3(0, -length * ROTE_SPEED, 0));
                }

            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out _currentPosition);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
        }
       
    }
}
