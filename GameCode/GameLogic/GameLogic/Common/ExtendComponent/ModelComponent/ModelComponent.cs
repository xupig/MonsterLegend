﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;

namespace Common.ExtendComponent
{
    public class ModelComponent : KContainer, IPointerClickHandler
    {
        private RectTransform _rectTransform;
        private GameObject _modelGo;
        private GameObject _mountModelGo;
        private ACTActor _playerActor;
        private int _actorID = 0;

        private const float MODE_SCALE_RATE = 0.8f;
        private const float MOUNT_SCALE_RATE = 0.6f;
        private Vector3 _startPosition = new Vector3(0, 0, -500);
        private float _rotationY = 180f;

        protected ModelDragComponent _dragComponent;

        public ModelDragComponent DragComponent
        {
            get
            {
                return _dragComponent;
            }
        }

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _dragComponent = this.gameObject.AddComponent<ModelDragComponent>();
        }

        public void LoadAvatarModel(Vocation vocation, string equipInfo = "", Action<ACTActor> callback = null, bool inCity = true)
        {
            if (vocation == Vocation.None)
            {
                GameLoader.Utils.LoggerHelper.Error("LoadAvatarModel Error vocation == Vocation.None");
                return;
            }
            int actorID = role_data_helper.GetModel((int)vocation);
            LoadModel(actorID, (actor) =>
            {
                actor.equipController.equipCloth.isMaterialHQ = true;
                actor.equipController.equipCloth.defaultEquipID = role_data_helper.GetDefaultCloth((int)vocation);
                actor.equipController.equipWeapon.defaultEquipID = role_data_helper.GetDefaultWeapon((int)vocation);
                actor.animationController.SetCullingMode(AnimatorCullingMode.AlwaysAnimate);
                var avatarModelData = ModelEquipTools.CreateAvatarModelData(vocation, equipInfo);
                var weaponInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                // 加载完成之后，需要改动特效或者流光的效果
                actor.equipController.equipWeapon.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadEquipFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadEquipFinished = OnLoadFinished;
                actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                actor.equipController.equipWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID, weaponInfo.particle, weaponInfo.flow);
                actor.equipController.equipWing.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID);
                ACTSystemAdapter.SetUIActorController(actor, inCity, (int)vocation, () =>
                {
                    OnModelPrefabLoaded(actor);
                    if (callback != null)
                    {
                        callback(actor);
                    }
                });
            }, false);
        }

        private void OnLoadFinished()
        {
            if (_modelGo != null)
            {
                ModelComponentUtil.ChangeUIModelParam(_modelGo);
            }
        }

        public void LoadModel(int actorID, Action<ACTActor> callback = null, bool layoutImmediately = true)
        {
            if (_actorID != actorID)
            {
                ClearModelGameObject();
                _actorID = actorID;
                GameMain.GlobalManager.ACTSystemAdapter.CreateActor(actorID, (actor) =>
                {
                    if (actor == null)
                    {
                        GameLoader.Utils.LoggerHelper.Error("CreateActor error in ModelComponent " + actorID);
                        return;
                    }
                    if (actorID != _actorID)
                    {
                        GameObject.Destroy(actor.gameObject);
                        return;
                    }
                    if (layoutImmediately == true)
                    {
                        OnModelPrefabLoaded(actor);
                    }
                    if (callback != null)
                    {
                        callback(actor);
                    }
                }, true);
            }
            else
            {
                if (callback != null && _modelGo != null)
                {
                    callback(_modelGo.GetComponent<ACTActor>());
                }
            }
        }

        /// <summary>
        /// 加载坐骑ID
        /// </summary>
        /// <param name="mountId"></param>
        /// <param name="callback"></param>
        public void LoadMountModel(int mountId, Action<ACTActor> callback = null)
        {
            int mountModelId = mount_helper.GetModelID((byte)mountId);
            GameObjectPoolManager.GetInstance().CreateActorGameObject(mountModelId, (actor) =>
            {
                if (actor == null)
                {
                    LoggerHelper.Error(string.Format("创建坐骑模型出错，坐骑模型id = {0}找不到对应的模型。", mountModelId));
                    return;
                }
                if (mountModelId != actor.actorData.ID)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(mountModelId, actor.gameObject);
                }
                OnMountModelPrefabLoaded(actor);
                if (callback != null)
                {
                    callback.Invoke(actor);
                }
            });
        }

        public void ClearModelGameObject()
        {
            if (_modelGo != null)
            {
                Destroy(_modelGo);
                _dragComponent.Clear();
                _modelGo = null;
                _actorID = 0;
            }
            if (_mountModelGo != null)
            {
                Destroy(_mountModelGo);
                _mountModelGo = null;
            }
        }

        private void OnMountModelPrefabLoaded(ACTActor mountActor)
        {
            GameObject mountGo = mountActor.gameObject;
            if (mountGo != null && mountGo != _mountModelGo)
            {
                if (_mountModelGo != null)
                {
                    Destroy(_mountModelGo);
                }
                _mountModelGo = mountGo;
                _dragComponent.SetModel(_mountModelGo);
                mountActor.actorController.isStatic = true;
                var characterController = mountGo.GetComponent<CharacterController>();
                characterController.enabled = false;
                float Scale = MOUNT_SCALE_RATE * _rectTransform.sizeDelta.y / characterController.height;
                mountGo.transform.localScale = new Vector3(Scale, Scale, Scale);
                mountGo.transform.localRotation = Quaternion.Euler(0, _rotationY, 0);
                mountGo.transform.localPosition = new Vector3(0.5f * _rectTransform.sizeDelta.x, -0.92f * _rectTransform.sizeDelta.y, 0) + _startPosition;
                mountGo.transform.SetParent(transform, false);
                mountGo.AddComponentOnlyOne<SortOrderedRenderAgent>();
                SortOrderedRenderAgent.RebuildAll();
                ModelComponentUtil.ChangeUIModelParam(mountGo);
            }
        }

        private void OnModelPrefabLoaded(ACTActor actor)
        {
            _playerActor = actor;
            GameObject go = actor.gameObject;
            if (go != null && go != _modelGo)
            {
                ClearModelGameObject();
                _modelGo = go;
                _dragComponent.SetModel(_modelGo);
                actor.actorController.isStatic = true;
                var characterController = go.GetComponent<CharacterController>();
                characterController.enabled = false;
                float Scale = MODE_SCALE_RATE * _rectTransform.sizeDelta.y / characterController.height;
                go.transform.localScale = new Vector3(Scale, Scale, Scale);
                go.transform.localRotation = Quaternion.Euler(0, _rotationY, 0);
                go.transform.localPosition = new Vector3(0.5f * _rectTransform.sizeDelta.x, -0.92f * _rectTransform.sizeDelta.y, 0) + _startPosition;
                go.transform.SetParent(transform, false);
                go.AddComponentOnlyOne<SortOrderedRenderAgent>();
                SortOrderedRenderAgent.RebuildAll();
                ModelComponentUtil.ChangeUIModelParam(go);
            }
        }


        public void SetStartPosition(Vector3 position)
        {
            _startPosition = position;
        }

        public void SetStartRotationY(float y)
        {
            _rotationY = y;
        }

        private uint timeId = 0;
        public bool IsOnMount = false;
        public void PlayRandomIdleAction()
        {
            if (IsOnMount == true)
            {
                return;
            }
            TimerHeap.DelTimer(timeId);
            if (_playerActor != null)
            {
                timeId = TimerHeap.AddTimer(0, 10000, PlayRandomAction);
            }
        }

        private bool canPlay = true;
        private void PlayRandomAction()
        {
            if (_playerActor == null)
            {
                return;
            }
            if (canPlay == false)
            {
                return;
            }
            if (IsOnMount == true)
            {
                TimerHeap.DelTimer(timeId);
                return;
            }
            List<int> randomActionList = global_params_helper.GetPlayerStandByList();
            int actionId = randomActionList[MogoMathUtils.Random(0, randomActionList.Count)];
            _playerActor.animationController.PlayAction(actionId);
            canPlay = false;
            TimerHeap.AddTimer(2000, 0, ResetCanPlay);
        }

        private void ResetCanPlay()
        {
            canPlay = true;
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (IsOnMount == true)
            {
                return;
            }
            PlayRandomAction();
        }

        public void RemoveAnimationTimer()
        {
            TimerHeap.DelTimer(timeId);
        }

        private int _wingId;
        public void LoadModelWithWing(int wingId)
        {
            _wingId = wingId;
            this.LoadPlayerModel(OnPlayerModelLoaded);
        }

        private void OnPlayerModelLoaded(ACTActor _actor)
        {
            int wingModelId = PlayerDataManager.Instance.WingData.GetItemDataById(_wingId).ModelId;
            _actor.equipController.equipWing.PutOn(wingModelId);
        }

        private int _fashionId;
        public void LoadModelWithFashion(int fashionId)
        {
            _fashionId = fashionId;
            this.LoadPlayerModel(OnFashionModelLoaded);
        }

        private void OnFashionModelLoaded(ACTActor actor)
        {
            fashion cfg = fashion_helper.GetFashionCfg(_fashionId);
            int clothId = fashion_helper.GetClothId(cfg, PlayerAvatar.Player.vocation);
            actor.equipController.equipCloth.PutOn(clothId);
            int weaponId = fashion_helper.GetWeaponId(cfg, PlayerAvatar.Player.vocation);
            actor.equipController.equipWeapon.PutOn(weaponId);
        }
       
    }
}
