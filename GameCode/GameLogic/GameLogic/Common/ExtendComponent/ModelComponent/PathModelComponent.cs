﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;

namespace Common.ExtendComponent
{
    public class PathModelComponent : KContainer, IPointerClickHandler
    {
        private RectTransform _rectTransform;
        private GameObject _modelGo;

        private const float MODE_SCALE_RATE = 0.8f;

        private ModelDragComponent _dragComponent;

        public ModelDragComponent DragComponent
        {
            get
            {
                return _dragComponent;
            }
        }

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _dragComponent = this.gameObject.AddComponent<ModelDragComponent>();
        }

        public void LoadModel(string modelPath, string paramStr = "", Action<GameObject> callback = null)
        {
            if (string.IsNullOrEmpty(modelPath))
            {
                if (callback != null)
                {
                    callback(null);
                }
                return;
            }
            Game.Asset.ObjectPool.Instance.GetGameObject(modelPath, (obj) =>
            {
                if (obj == null)
                {
                    GameLoader.Utils.LoggerHelper.Error("CreateActor modelName:" + modelPath + " is null");
                    return;
                }
                OnModelLoaded(obj, paramStr);
                if (callback != null)
                {
                    callback(obj);
                }
            });
        }

        private void OnModelLoaded(GameObject go, string paramStr)
        {
            if (go != null)
            {
                ClearModelGameObject();
                _modelGo = go;
                _dragComponent.SetModel(_modelGo);
                var actor = go.GetComponent<ACTActor>();
                if (actor != null)
                {
                    actor.actorController.isStatic = true;
                }
                var characterController = go.GetComponent<CharacterController>();
                if (characterController != null)
                {
                    characterController.enabled = false;
                }
                Vector3 position = new Vector3(0.5f * _rectTransform.sizeDelta.x, -0.9f * _rectTransform.sizeDelta.y, -500);
                Quaternion rotation = Quaternion.Euler(0, 180, 0);
                float scale = 100f;
                if (paramStr == null)
                {
                    paramStr = "";
                }
                string[] paramList = paramStr.Split(',');
                if (paramList.Length < 7)
                {
                    if (characterController != null)
                    {
                        scale = 0.8f * _rectTransform.sizeDelta.y / characterController.height;
                    }
                }
                else
                {
                    position = new Vector3(float.Parse(paramList[0]), float.Parse(paramList[1]), float.Parse(paramList[2]));
                    rotation = Quaternion.Euler(float.Parse(paramList[3]), float.Parse(paramList[4]), float.Parse(paramList[5]));
                    scale = float.Parse(paramList[6]);
                }
                go.transform.SetParent(_rectTransform);
                go.transform.localPosition = position;
                go.transform.localRotation = rotation;
                go.transform.localScale = new Vector3(scale, scale, scale);
                go.AddComponentOnlyOne<SortOrderedRenderAgent>();
                SortOrderedRenderAgent.RebuildAll();
                ModelComponentUtil.ChangeUIModelParam(go);
            }
        }
        
        public void ClearModelGameObject()
        {
            if (_modelGo != null)
            {
                Destroy(_modelGo);
                _dragComponent.Clear();
                _modelGo = null;
            }
        }
       
    }
}
