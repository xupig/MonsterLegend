﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;

namespace Common.ExtendComponent
{
    public static class ModelComponentExtend
    {

        public static void LoadPlayerModel(this ModelComponent component, Action<ACTActor> callback = null, bool inCity = true)
        {
            Vocation vocation = PlayerAvatar.Player.vocation;
            string equipInfo = PlayerAvatar.Player.equipManager.GetEquipInfoStr();
            component.LoadAvatarModel(vocation, equipInfo, callback, inCity);
        }

        public static void LoadMonsterModel(this ModelComponent component, int monsterId, Action<ACTActor> callback = null)
        {
            int actorID = monster_helper.GetMonsterModel(monsterId);
            component.LoadModel(actorID, callback);
        }

        public static void LoadNPCModel(this ModelComponent component, int npcId, Action<ACTActor> callback = null)
        {
            int actorID = npc_helper.GetNpcModelUIID(npcId);
            if (actorID == 0) actorID = npc_helper.GetNpcModelID(npcId);
            component.LoadModel(actorID, callback);
        }

        public static void LoadPetModel(this ModelComponent component, int pet_quality_id, Action<ACTActor> callback = null)
        {
            pet_quality pet = pet_quality_helper.GetPetQualityConf(pet_quality_id);
            if (pet != null)
            {
                component.LoadPetModel(pet.__pet_id, pet.__quality, callback);
            }
        }

        public static void LoadPetModel(this ModelComponent component, int petId, int quality, Action<ACTActor> callback = null)
        {
            int actorID = pet_quality_helper.GetModelID(petId, quality);
            component.LoadModel(actorID, callback);
        }
       
    }
}
