﻿#region 模块信息
/*==========================================
// 文件名：ScrollListItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.ScrollList
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/24 14:13:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ExtendComponent
{
    public abstract class ScrollListItem : KList.KListItemBase
    {
    }
}
