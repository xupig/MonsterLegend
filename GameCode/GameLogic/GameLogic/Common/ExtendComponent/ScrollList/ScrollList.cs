﻿#region 模块信息
/*==========================================
// 文件名：ScrollList
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.ScrollList
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/24 14:11:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.Builder;
using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.ExtendComponent
{
    public class ScrollList : KContainer, IEndDragHandler, IDragHandler
    {
        public const string ITEM_PREFIX = "item_";
        private GameObject _template;
        private List<ScrollListItem> _itemList;
        public bool isItemAddRenderingAgent = false;
        private Vector2 _itemSize = Vector2.zero;

        private IList _dataList;

        public const int DEFAULT_GAP = 0;
        private Game.UI.UIComponent.KList.KListPadding _padding = new Game.UI.UIComponent.KList.KListPadding();

        private int _topToDownGap = DEFAULT_GAP;        //item列间距
        private bool _isMax = true;
        private int _currentSelectedIndex = 0;
        private ScrollListItem _currentSelectedItem;
        public int poolCount = 15;
        private Vector2 _centerPos;


        public Game.UI.UIComponent.KList.KListPadding Padding
        {
            get { return _padding; }
        }

        public int TopToDownGap
        {
            get { return _topToDownGap; }
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public virtual void SetGap(int topToDownGap)
        {
            _topToDownGap = topToDownGap;
        }

        public virtual void SetType(bool isMax)
        {
            _isMax = isMax;
        }

        protected override void Awake()
        {
            base.Awake();
            _itemList = new List<ScrollListItem>();
            _template = GetChild("Container_template");
            _template.SetActive(false);
            _centerPos = (_template.transform as RectTransform).anchoredPosition;
        }

        protected void InitItemSize()
        {
            RectTransform rect = _template.GetComponent<RectTransform>();
            _itemSize.x = rect.sizeDelta.x;
            _itemSize.y = rect.sizeDelta.y;
        }

        public List<ScrollListItem> ItemList
        {
            get
            {
                return _itemList;
            }
        }

        public void OnDrag(PointerEventData _data)
        {

        }

        public void OnEndDrag(PointerEventData _data)
        {

        } 

        /// <summary>
        /// 用于对性能比较敏感的场合，只做一次布局操作
        /// coroutineCreateCount，当通过协程的方式创建Item时每帧创建Item的数量设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataList"></param>
        public virtual void SetDataList<T>(IList dataList) where T : ScrollListItem
        {
            SetDataList(typeof(T), dataList);
        }

        public virtual void SetDataList(Type itemType, IList dataList)
        {
            if(_itemList == null)
            {
                for(int i=0;i<poolCount;i++)
                {
                    AddItemAt(itemType, _itemList.Count);
                }
            }
            _dataList = dataList;
            if(_isMax)
            {
                _currentSelectedIndex = _dataList.Count;
                int count = 0;
                for(int i=_dataList.Count-1;i>=0;i--)
                {
                    if(_itemList.Count - count - 1 >= 0)
                    {
                        _itemList[_itemList.Count - count - 1].Visible = true;
                        _itemList[_itemList.Count - count - 1].Data = _dataList[i];
                        count++;
                    }
                }
                for(int i=0;i<_itemList.Count-count;i++)
                {
                    _itemList[i].Visible = false;
                }
            }
            else
            {
                _currentSelectedIndex = 1;
                int i = 0;
                for(i=0;i<_dataList.Count;i++)
                {
                    if(_itemList.Count>i)
                    {
                        _itemList[i].Visible = true;
                        _itemList[i].Data = _dataList[i];
                    }
                }
                for(;i<_itemList.Count;i++)
                {
                    _itemList[i].Visible = false;
                }
            }
            UpdateItemListLayout();
        }


        public virtual void AddItemAt(Type itemType, int index)
        {
            GameObject itemGo = CreateItemGo();
            AddToHierarchy(itemGo);
            BuildItem(itemGo);
            ScrollListItem item = itemGo.AddComponent(itemType) as ScrollListItem;
            item.Index = index;
            item.name = GenerateItemName(index);
           // item.Data = data;
            _itemList.Insert(index, item);
            AddItemEventListener(item);
            if (isItemAddRenderingAgent == true)
            {
                RenderingAgent agent = itemGo.AddComponent<RenderingAgent>();
                agent.SetBoundary(this.transform.parent.GetComponent<RectTransform>());
                agent.Item = item;
            }
        }

        private void AddItemEventListener(ScrollListItem item)
        {
            item.onClick.AddListener(OnItemClicked);
        }

        private void RemoveItemEventListener(ScrollListItem item)
        {
            item.onClick.RemoveListener(OnItemClicked);
        }

        private void OnItemClicked(KList.KListItemBase item,int index)
        {

        }

        private string GenerateItemName(int index)
        {
            return ITEM_PREFIX + index;
        }

        protected virtual void BuildItem(GameObject itemGo)
        {
            ComponentBuilder.typeBuilderDict[ComponentBuilder.TYPE_ITEM].Build(itemGo, true);
        }

        private void AddToHierarchy(GameObject itemGo)
        {
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
            itemGo.SetActive(true);
        }

        protected GameObject CreateItemGo()
        {
            return Instantiate(_template) as GameObject;
        }

        protected virtual void UpdateItemListLayout()
        {
            int beforeCount = 0;
            int afterCount = 0;
            if(_currentSelectedIndex - 8 <= 0)
            {
                beforeCount = _currentSelectedIndex - 1;
                afterCount = poolCount - beforeCount;
            }
            else
            {
                afterCount = _dataList.Count - _currentSelectedIndex;
                beforeCount = poolCount - afterCount;
            }

            for (int i = 0; i < beforeCount;i++ )
            {
                ScrollListItem item = _itemList[i];
                if(item.Visible)
                {
                    item.Data = _dataList[_currentSelectedIndex - beforeCount + i];
                }
            }
            for (int i = beforeCount; i < afterCount;i++ )
            {
                ScrollListItem item = _itemList[i];
                if(item.Visible)
                {
                    item.Data = _dataList[_currentSelectedIndex - beforeCount + i];
                }
            }
        }

        protected virtual Vector2 CalculateItemPosition(int index, RectTransform itemRect)
        {
            Vector2 result = Vector2.zero;
            result.x = _padding.left;
            result.y = _padding.top + index * (_itemSize.y + _topToDownGap);
            return result;
        }
    }
}
