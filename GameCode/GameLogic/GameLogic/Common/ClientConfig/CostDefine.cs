﻿#region 模块信息
/*==========================================
// 文件名：CostDefine
// 命名空间: Common.ClientConfig
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/14 23:36:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ClientConfig
{
    public class CostDefine
    {
        public const int MONEY_TYPE_GOLD = 1;  //金币
        public const int MONEY_TYPE_DIAMOND = 2;  //充值货币类型,钻石
        public const int MONEY_TYPE_BIND_DIAMOND = 3;  //绑定钻石
        public const int MONEY_TYPE_HONOUR = 4;  //荣誉
        public const int MONEY_TYPE_ENERGY = 5;  //体力
        public const int MONEY_TYPE_POWER = 6;  //精力
        public const int MONEY_TYPE_MAX_ID = 6;  //货币类型最大id。每加一种货币类型此值要+1
    }
}
