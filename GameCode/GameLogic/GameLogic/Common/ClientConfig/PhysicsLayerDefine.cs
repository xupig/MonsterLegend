﻿#region 模块信息
/*==========================================
// 文件名：EntityPropertyDefine
// 命名空间: Common.ClientConfig
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/14 23:36:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ClientConfig
{
    public class PhysicsLayerDefine
    {
        public static string Actor = "Actor";
        public static string Avatar = "Avatar";
        public static string Monster = "Monster";
        public static string Dummy = "Dummy";
        public static string Ghost = "Ghost";
        public static string Pet = "Pet";
        public static string Air = "Air";
        public static string DynamicWall = "DynamicWall";
    }
}
