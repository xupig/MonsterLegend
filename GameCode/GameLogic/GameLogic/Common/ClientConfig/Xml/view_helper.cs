﻿#region 模块信息
/*==========================================
// 文件名：view_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/22 14:27:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class view_helper
    {
        public static view GetView(int viewId)
        {
            if (!XMLManager.view.ContainsKey(viewId))
            {
                throw new Exception("配置表view找不到id:" + viewId);
            }
            return XMLManager.view[viewId];
        }

        public static void OpenView(int viewId, List<string> fid_arg)
        {
            if (XMLManager.view.ContainsKey(viewId))
            {
                view view = XMLManager.view[viewId];
                ((PanelIdEnum)view.__panel_id).Show(fid_arg);
            }
        }

        public static void OpenView(int viewId, int[] fid_arg)
        {
            view view = view_helper.GetView(viewId);
            List<int> paramList = new List<int>();
            paramList.Add(view.__tab);
            paramList.Add(view.__second_tab);
            for (int i = 0; i < fid_arg.Length; i++)
            {
                paramList.Add(fid_arg[i]);
            }
            if (XMLManager.view.ContainsKey(viewId))
            {
                ((PanelIdEnum)view.__panel_id).Show(paramList.ToArray<int>());
            }
        }

        public static void OpenView(int viewId, BasePanelParameter basePanelParameter)
        {
            view view = view_helper.GetView(viewId);
            basePanelParameter.FirstTab = view.__tab;
            basePanelParameter.SecondTab = view.__second_tab;
            ((PanelIdEnum)view.__panel_id).Show(basePanelParameter);
        }
        
        public static void OpenView(int viewId)
        {
            if (XMLManager.view.ContainsKey(viewId))
            {
                view view = XMLManager.view[viewId];
                if (view.__second_tab == 0)
                {
                    if (view.__tab != 0)
                    {
                        ((PanelIdEnum)view.__panel_id).Show(view.__tab);
                    }
                    else
                    {
                        ((PanelIdEnum)view.__panel_id).Show();
                    }
                }
                else
                {
                    ((PanelIdEnum)view.__panel_id).Show(new int[] { view.__tab, view.__second_tab });
                }
            }
        }


        public static PanelIdEnum GetViewPanelId(int viewId)
        {
            if (XMLManager.view.ContainsKey(viewId))
            {
                view view = XMLManager.view[viewId];
                return (PanelIdEnum)view.__panel_id;
            }
            return PanelIdEnum.Empty;
        }

        public static void OpenMapEntry(int viewId, int mapId)
        {
            if (XMLManager.view.ContainsKey(viewId))
            {
                view view = XMLManager.view[viewId];
                ((PanelIdEnum)view.__panel_id).Show( mapId);
            }
        }
    }
}
