﻿using Common.Data;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class pet_refresh_limit_helper
    {
        private static Dictionary<PetWashType, Dictionary<int, pet_refresh_limit>> _dict = new Dictionary<PetWashType, Dictionary<int,pet_refresh_limit>>();

        static pet_refresh_limit_helper()
        {
            foreach(pet_refresh_limit refreshLimit in XMLManager.pet_refresh_limit.Values)
            {
                PetWashType washType = (PetWashType)refreshLimit.__refresh_type;
                if(!_dict.ContainsKey(washType))
                {
                    _dict.Add(washType, new Dictionary<int, pet_refresh_limit>());
                }
                _dict[washType].Add(refreshLimit.__lock_num, refreshLimit);
            }
        }

        public static bool IsPetWashUnlock(PetInfo petInfo)
        {
            foreach(Dictionary<int, pet_refresh_limit> value in _dict.Values)
            {
                pet_refresh_limit refreshLimit = value[0];
                if ((petInfo.Star >= refreshLimit.__star)
                    && (petInfo.Quality >= refreshLimit.__quality)
                    && (PlayerAvatar.Player.vip_level >= refreshLimit.__vip))
                {
                    return true;
                }
            }
            return false;
            
        }

        public static int GetMinStar()
        {
            //假设技能0孔需要的星数最低
            return _dict[PetWashType.skill][0].__star;
        }

        public static pet_refresh_limit GetPetRefreshLimit(PetWashType washType, int lockNum = 0)
        {
            if(!_dict.ContainsKey(washType))
            {
                throw new Exception("pet_refresh_limit 不存在Id:" + washType);
            }
            if(!_dict[washType].ContainsKey(lockNum))
            {
                throw new Exception("pet_refresh_limit 不存在Id:" + washType + "     lockNum:" + lockNum);
            }
            return _dict[washType][lockNum];
        }

        public static bool IsPetRefreshLimitExist(PetWashType washType, int lockNum = 0)
        {
            if (!_dict.ContainsKey(washType))
            {
                return false;
            }
            return _dict[washType].ContainsKey(lockNum);
        }

        public static void Init()
        {
            int count = XMLManager.pet_refresh_limit.Count;
        }
    }
}
