﻿using Common.States;
using UnityEngine;

namespace GameData
{
    public enum PlatformMode
    {
        PC = 0,
        MOBILE,
        EDITOR
    }
    public class platform_helper
    {
        private static PlatformMode _platformMode = PlatformMode.PC;
        private static bool _initialized = false;

        public static PlatformMode GetPlatformMode()
        {
            Initialize();
            return _platformMode;
        }

        public static bool InPlatformMode(PlatformMode platformMode)
        {
            Initialize();
            return _platformMode == platformMode;
        }

        public static void SetPlatformMode(PlatformMode platformMode)
        {
            _initialized = true;
            _platformMode = platformMode;
        }

        public static bool InPCPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.PC;
        }

        public static bool InTouchPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.MOBILE;
        }

        public static bool InEditorPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.EDITOR;
        }

        private static void Initialize()
        {
            if (_initialized)
            {
                return;
            }
            _initialized = true;
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsWebPlayer:
                    _platformMode = PlatformMode.PC;
                    break;
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    _platformMode = PlatformMode.MOBILE;
                    break;
                case RuntimePlatform.WindowsEditor:
                    _platformMode = PlatformMode.EDITOR;
                    break;
                default:
                    _platformMode = PlatformMode.MOBILE;
                    break;
            }
        }
    }
}
