﻿using Common.Data;
using Common.Events;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class skill_play_helper
    {
        public static int GetCurrentSkillId(int position)
        {
            int skillId = 0;
            Dictionary<int, int> posSkillIdMap;
            if (PlayerAvatar.Player.vehicleManager.vehicle != null)
            {
                posSkillIdMap = PlayerAvatar.Player.vehicleManager.vehicle.skillManager.GetCurPosSkillIDMap();
            }
            else
            {
                posSkillIdMap = PlayerAvatar.Player.skillManager.GetCurPosSkillIDMap();
            }
            if (posSkillIdMap.ContainsKey(position))
            {
                skillId = posSkillIdMap[position];
            }
            return skillId;
        }

        public static void PlaySkill(int position)
        {
            if (skill_guide_helper.IsHide(position) == true)
            {
                return;
            }
            if (position == SkillData.SKILL_POS_ONE)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.MAINUI_T_BTN_SKILL1_ON_CLICK);
            }
            else if (position == SkillData.SKILL_POS_TWO)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.MAINUI_T_BTN_SKILL2_ON_CLICK);
            }
            else if (position == SkillData.SKILL_POS_THREE)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.MAINUI_T_BTN_SKILL3_ON_CLICK);
            }
            else if (position == SkillData.SKILL_POS_FOUR)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.MAINUI_T_BTN_SKILL4_ON_CLICK);
            }
        }

    }
}
