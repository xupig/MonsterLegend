﻿using System;
using System.Collections.Generic;

namespace GameData
{
    public enum CancelPushCondType
    {
        Task = 1
    }

    public class login_push_helper
    {
        public static login_push GetLoginPushInfo(int id)
        {
            if (XMLManager.login_push.ContainsKey(id))
            {
                return XMLManager.login_push[id];
            }
            return null;
        }

        public static int GetCancelPushCondType(int id)
        {
            login_push loginPush = GetLoginPushInfo(id);
            if (loginPush == null)
                return 0;
            int type = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(loginPush.__cancel_push_cond);
            foreach (var item in dataDict)
            {
                type = int.Parse(item.Key);
                break;
            }
            return type;
        }

        public static int GetCancelPushCondValue(int id)
        {
            login_push loginPush = GetLoginPushInfo(id);
            if (loginPush == null)
                return 0;
            int value = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(loginPush.__cancel_push_cond);
            foreach (var item in dataDict)
            {
                value = int.Parse(item.Value);
                break;
            }
            return value;
        }
    }
}
