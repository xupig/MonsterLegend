﻿#region 模块信息
/*==========================================
// 文件名：activity_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 16:00:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public enum ActivityType
    {
        NONE = 0,
        Challenge = 1,
        LimitActivity = 2,
        GuildActivity = 3,
    }

    public class activity_helper
    {
        public const int DemonGateActivity = 1;
        private const int ExploreInstance = 2;
        private const int TrailInstance = 3;
        private const int TeamInstance = 4;

        private static Dictionary<int, open> functionToActivityDict;

        private static List<LimitActivityData> _challengeList;//挑战
        private static List<LimitActivityData> _openList;//活动

        private static DateTime dateTime = new DateTime();

        private static List<LimitActivityData> _currentOpenList;
        private static List<LimitActivityData> _tomorrowOpenList;

        private static Dictionary<int, int[]> _currentOpenDict;
        private static Dictionary<int, int[]> _tomorrowOpenDict;

        static activity_helper()
        {
            InitFunctionToActivityDict();
            InitData();
        }

        public static void OnReloadData()
        {
            functionToActivityDict = null;
            _challengeList = null;
            InitFunctionToActivityDict();
            InitData();
        }

        public static open GetOpenByFunction(int functionId)
        {
            if (functionToActivityDict.ContainsKey(functionId))
            {
                return functionToActivityDict[functionId];
            }
            return null;
        }

        public static open GetOpenData(int openId)
        {
            if (XMLManager.open.ContainsKey(openId))
            {
                return XMLManager.open[openId];
            }
            else
            {
                throw new Exception(string.Format("活动开放表缺少ID为{0}的配置", openId));
            }
        }

        public static function GetFunction(int functionId)
        {
            if (XMLManager.function.ContainsKey(functionId))
            {
                return XMLManager.function[functionId];
            }
            else
            {
                throw new Exception(string.Format("活动表缺少ID为{0}的配置", functionId));
            }
        }

        private static void InitFunctionToActivityDict()
        {
            if (functionToActivityDict == null)
            {
                functionToActivityDict = new Dictionary<int, open>();
                foreach (KeyValuePair<int, open> kvp in XMLManager.open)
                {
                    if (kvp.Value.__type != 0)
                    {
                        if (functionToActivityDict.ContainsKey(kvp.Value.__fid) == false)
                        {
                            functionToActivityDict.Add(kvp.Value.__fid, kvp.Value);
                        }
                    }
                }
            }
        }

        private static void InitData()
        {
            if (_challengeList == null)
            {
                _challengeList = new List<LimitActivityData>();
                _openList = new List<LimitActivityData>();
                _currentOpenList = new List<LimitActivityData>();
                _tomorrowOpenList = new List<LimitActivityData>();
                _currentOpenDict = new Dictionary<int, int[]>();
                _tomorrowOpenDict = new Dictionary<int, int[]>();
                foreach (KeyValuePair<int, open> kvp in XMLManager.open)
                {
                    if (kvp.Value.__type != 0)
                    {
                        LimitActivityData data = new LimitActivityData();
                        SetOpenDate(data, data_parse_helper.ParseListString(kvp.Value.__open_date));
                        SetOpenWeekOfDay(data, data_parse_helper.ParseListString(kvp.Value.__open_day));
                        SetOpenTime(data, data_parse_helper.ParseListString(kvp.Value.__open_time), data_parse_helper.ParseListString(kvp.Value.__time));

                        SetNoticeTime(data, data_parse_helper.ParseListString(kvp.Value.__actiontime));
                        data.openData = kvp.Value;
                        if (kvp.Value.__fid < 10000)
                        {
                            data.functionData = GetFunction(kvp.Value.__fid);
                        }
                        if (kvp.Value.__type == (int)ActivityType.Challenge)
                        {
                            _challengeList.Add(data);
                        }
                        else
                        {
                            _openList.Add(data);
                        }
                    }
                }
            }
        }

        private static void SetOpenDate(LimitActivityData data, List<string> openDate)
        {
            data.startDate = long.MinValue;
            data.endDate = long.MaxValue;
            if (openDate != null)
            {
                if (openDate.Count > 0)
                {
                    data.startDate = ConvertToTimeStamp(int.Parse(openDate[0]));
                }
                if (openDate.Count > 1)
                {
                    data.endDate = ConvertToTimeStamp(int.Parse(openDate[1]));
                }
            }
        }

        private static long ConvertToTimeStamp(int time)
        {
            int year = time / 10000;
            time = time % 10000;
            int month = time / 100;
            int day = time % 100;
            DateTime dateTime = MogoTimeUtil.GetUtcTime(year, Math.Max(1, month), Math.Max(1, day), 0, 0, 0);
            return MogoTimeUtil.UtcTime2ServerTimeStamp(dateTime);
        }

        private static void SetOpenWeekOfDay(LimitActivityData data, List<string> openOfWeek)
        {
            data.openWeekOfDay = new List<int>();
            if (openOfWeek == null || openOfWeek.Count == 0 || openOfWeek[0] == "0")
            {
                for (int i = 0; i < 7; i++)
                {
                    data.openWeekOfDay.Add(i);
                }
            }
            else
            {
                for (int i = 0; i < openOfWeek.Count; i++)
                {
                    int day = int.Parse(openOfWeek[i]);
                    if (day == 7)
                    {
                        day = 0;
                    }
                    data.openWeekOfDay.Add(day);
                }
            }
        }

        public static void SetNoticeTime(ActivityNoticeData data, List<string> noticeTimeList)
        {
            data.noticeTimeList = new List<int>();
            for (int i = 0; i < noticeTimeList.Count; i++)
            {
                int time = ConvertTime(int.Parse(noticeTimeList[i]));
                data.noticeTimeList.Add(time);
            }
        }

        private static void SetOpenTime(LimitActivityData data, List<string> openTime, List<string> lastTime)
        {
            data.openTimeStart = new List<int>();
            data.openTimeEnd = new List<int>();
            if (openTime != null)
            {
                for (int i = 0; i < openTime.Count; i++)
                {
                    int time = int.Parse(openTime[i]);
                    int hour = time / 100;
                    int min = time % 100;
                    data.openTimeStart.Add(hour * 60 * 60 + min * 60);
                    data.openTimeEnd.Add(hour * 60 * 60 + min * 60 + int.Parse(lastTime[i]));
                }
            }
        }


        public static List<LimitActivityData> GetNormalFunctionList()
        {
            return _challengeList;
        }

        public static bool IsOpen(function functionData)
        {
            bool result = true;
            LimitEnum limit;
            if (functionData.__belong_system != 0)
            {
                function belongFunction = GetFunction(functionData.__belong_system);
                result &= IsOpen(belongFunction);
            }
            if (functionData.__condition != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(functionData.__condition);
                limit = PlayerAvatar.Player.CheckLimit<string>(dataDict);
                result &= limit == LimitEnum.NO_LIMIT;
            }
            
            return result;
        }

        public static int[] GetCurrentOpenData(int id)
        {
            return _currentOpenDict[id];
        }

        public static int[] GetTommorrowOpenData(int id)
        {
            return _tomorrowOpenDict[id];
        }

        public static List<LimitActivityData> GetLimitActivityList()
        {
            IntiLimitData();
            return _currentOpenList;
        }

        public static LimitActivityData GetLimitActivity(int activityId)
        {
            IntiLimitData();
            for (int i = 0; i < _currentOpenList.Count; i++)
            {
                if (_currentOpenList[i].openData.__id == activityId)
                {
                    return _currentOpenList[i];
                }
            }
            return null;
        }

        public static LimitActivityData GetActivity(int activityId)
        {
            IntiLimitData();
            for (int i = 0; i < _openList.Count; i++)
            {
                if (_openList[i].openData.__id == activityId)
                {
                    return _openList[i];
                }
            }
            for (int i = 0; i < _challengeList.Count; i++)
            {
                if (_challengeList[i].openData.__id == activityId)
                {
                    return _challengeList[i];
                }
            }
            return null;
        }
        /*
        public static List<LimitActivityData> GetActivityByType(ActivityType type)
        {
            List<LimitActivityData> result = new List<LimitActivityData>();
            for (int i = 0; i < _currentOpenList.Count; i++)
            {
                if (_currentOpenList[i].openData.__type == (int)type)
                {
                    result.Add(_currentOpenList[i]);
                }
            }
            return result;
        }*/

        private static void IntiLimitData()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp / 1000);
            dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp + 24 * 60 * 60);
            SetLimitDataList(timeStamp, _tomorrowOpenList, _tomorrowOpenDict);
            _tomorrowOpenList.Sort(TomorrowSortFunc);

            dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
            SetLimitDataList(timeStamp, _currentOpenList, _currentOpenDict);
            _currentOpenList.Sort(SortFunc);
        }

        private static void SetLimitDataList(long timeStamp, List<LimitActivityData> dataList, Dictionary<int, int[]> dict)
        {
            int dayOfWeek = (int)dateTime.DayOfWeek;
            dataList.Clear();
            dict.Clear();
            for (int i = 0; i < _openList.Count; i++)
            {
                if (IsTodayOpen(_openList[i], dateTime, timeStamp, dayOfWeek, dict))
                {
                    dataList.Add(_openList[i]);
                }
            }
            currentSecond = dateTime.Second + dateTime.Minute * 60 + dateTime.Hour * 3600;

        }

        public static List<LimitActivityData> GetTomorrowLimitActivityList()
        {
            IntiLimitData();
            return _tomorrowOpenList;
        }

        private static bool IsTodayOpen(LimitActivityData data, DateTime dateTime, long timeStamp, int dayOfWeek, Dictionary<int, int[]> dict)
        {
            dict.Add(data.openData.__id, new int[] { 0, 24 * 3600 });
            if (data.startDate > timeStamp || data.endDate < timeStamp)//日期判断
            {
                return false;
            }
            if (data.openWeekOfDay.Count > 0 && data.openWeekOfDay.Contains(dayOfWeek) == false)//星期判断
            {
                return false;
            }
            if (data.openTimeStart.Count > 0)
            {
                dict[data.openData.__id][0] = int.MaxValue;
                dict[data.openData.__id][1] = int.MaxValue;
                for (int i = 0; i < data.openTimeStart.Count; i++)
                {
                    dict[data.openData.__id][0] = Math.Min(dict[data.openData.__id][0], data.openTimeStart[i]);
                    dict[data.openData.__id][1] = Math.Min(dict[data.openData.__id][1], data.openTimeEnd[i]);
                }
            }
            return true;
        }

        private static int ConvertTime(int time)
        {
            int hour = time / 10000;
            time = time % 10000;
            int min = time / 100;
            int sec = time % 100;
            return hour * 3600 + min * 60 + sec;
        }

        public static bool IsOpen(int activityId)
        {
            if (_openList == null) return false;
            for (int i = 0; i < _openList.Count; i++)
            {
                if (_openList[i].openData.__id == activityId)
                {
                    return IsOpen(_openList[i]);
                }
            }
            return false;
        }

        public static bool IsOpenByFunctionId(int functionId)
        {
            if (_openList == null) return false;
            for (int i = 0; i < _openList.Count; i++)
            {
                if (_openList[i].openData.__fid == functionId)
                {
                    if (IsOpen(_openList[i]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsOpen(LimitActivityData data)
        {
            if (data.functionData != null)
            {
                if (IsOpen(data.functionData) == false)
                {
                    return false;
                }
            }
            if (data.startDate > (long)(Global.serverTimeStamp / 1000) || data.endDate < (long)(Global.serverTimeStamp / 1000))//日期判断
            {
                return false;
            }
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.serverTimeStamp / 1000));
            if (data.openWeekOfDay.Count > 0 && data.openWeekOfDay.Contains((int)dateTime.DayOfWeek) == false)//星期判断
            {
                return false;
            }

            int currentSecond = PlayerTimerManager.GetInstance().GetCurrentSecond();
            if (data.openTimeStart.Count > 0)
            {
                for (int i = 0; i < data.openTimeStart.Count; i++)
                {
                    if (currentSecond >= data.openTimeStart[i] && currentSecond <= data.openTimeEnd[i])//时间判断
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public static void SendNotice(LimitActivityData data, int currentSecond)
        {
            if (data.noticeTimeList.Count > 0)
            {
                for (int i = 0; i < data.noticeTimeList.Count; i++)
                {
                    int offset = currentSecond - data.noticeTimeList[i];
                    if (offset >= 0 && offset < 15)//时间判断
                    {
                        var action = data_parse_helper.ParseListInt(data.openData.__action);
                        if (action.Count <= i)
                        {
                            Debug.LogError("SendNotice openData:" + data.openData.__id);
                        }
                        if (data.openTimeStart == null || data.openTimeStart.Count==0)
                        {
                            Debug.LogError("SendNotice openTimeStart:" + data.openData.__id);
                        }
                        SystemInfoManager.Instance.Show(action[i], data.openData.__id, MogoTimeUtil.ToUniversalTime(data.openTimeStart[0], "hh:mm"));
                        return;
                    }
                }
            }
        }



        private static int currentSecond;

        private static int SortFunc(LimitActivityData data1, LimitActivityData data2)
        {
            int[] dataList1 = GetCurrentOpenData(data1.openData.__id);
            int[] dataList2 = GetCurrentOpenData(data2.openData.__id);
            if (dataList1[0] < currentSecond)
            {
                if (dataList2[0] < currentSecond)
                {
                    return dataList2[0] - dataList1[0];
                }
                else if (dataList2[1] < currentSecond)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (dataList1[1] < currentSecond)
            {
                if (dataList2[1] < currentSecond)
                {
                    return dataList1[0] - dataList2[0];
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (dataList2[0] < currentSecond || dataList2[1] < currentSecond)
                {
                    return 1;
                }
                else
                {
                    return dataList1[0] - dataList2[0];
                }
            }
        }


        private static int TomorrowSortFunc(LimitActivityData data1, LimitActivityData data2)
        {
            int[] dataList1 = GetTommorrowOpenData(data1.openData.__id);
            int[] dataList2 = GetTommorrowOpenData(data2.openData.__id);
            if (dataList1[0] < currentSecond)
            {
                if (dataList2[0] < currentSecond)
                {
                    return dataList2[0] - dataList1[0];
                }
                else if (dataList2[1] < currentSecond)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else if (dataList1[1] < currentSecond)
            {
                if (dataList2[1] < currentSecond)
                {
                    return dataList1[0] - dataList2[0];
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (dataList2[0] < currentSecond || dataList2[1] < currentSecond)
                {
                    return 1;
                }
                else
                {
                    return dataList1[0] - dataList2[0];
                }
            }
        }

        /// <summary>
        /// 根据open_day_l 获取当天开启的玩法id  //  具体规则，参见恶魔之门玩法文档
        /// </summary>
        public static string GetPlayerMethodSubID(open openData)
        {
            string playerMethodSubID = string.Empty;
            int currentWeek = (int)System.DateTime.Now.DayOfWeek;
            if (System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                currentWeek = 7;   //0表示通俗周天。  0但是配置表规定， 0表示 所有天都开启， 7表示礼拜天。  所有有如此设置。
            }

            int openDayIndex = -1;
            if (openData.__open_day.Contains("0"))
            {
                openDayIndex = currentWeek;
            }
            else
            {
                openDayIndex = openData.__open_day.IndexOf(currentWeek.ToString());
            }

            if (openDayIndex != -1)
            {
                var fidArg = data_parse_helper.ParseListString(openData.__fid_arg);
                playerMethodSubID = fidArg[openDayIndex];
            }

            return playerMethodSubID;
        }

        public static int NpcContainsActivity(int npcId)
        {
            foreach (KeyValuePair<int, open> item in XMLManager.open)
            {
                open openItem = item.Value;
                if (IsOpen(openItem.__id) == false)
                {
                    continue;
                }
                switch (openItem.__id)
                {
                    case DemonGateActivity:
                        if (evil_mission_helper.IsNPCShow(npcId) == true)
                        {
                            return DemonGateActivity;
                        }
                        break;
                    default:
                        return 0;
                }
            }
            return 0;
        }
    }
}
