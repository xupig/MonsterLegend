﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/15 15:05:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class LevelConfig
    {
        public int Level;
        public int Exp;
        public LevelConfig(int level, int exp)
        {
            this.Level = level;
            this.Exp = exp;
        }
    }

    public class avatar_level_helper
    {
        public static avatar_level GetAvatarLevel(int level)
        {
            if (!XMLManager.avatar_level.ContainsKey(level))
            {
                throw new Exception("avatar_level表找不到id为:" + level);
            }
            return XMLManager.avatar_level[level];
        }

        public static int GetNextLevelExp(int level)
        {
            avatar_level _avatarLevel = GetAvatarLevel(level);
            return _avatarLevel.__next_level_exp;
        }

        public static float GetExpPercent(int exp, int level)
        {
            float nextLevelExp = GetNextLevelExp(level);
            if (nextLevelExp == 0)
            {
                return 1;
            }
            return exp / nextLevelExp;
        }

        public static int GetMaxEnergy(int level)
        {
            avatar_level _avatarLevel = GetAvatarLevel(level);
            return _avatarLevel.__max_energy;
        }

        public static int GetEnergyLimit(int level)
        {
            avatar_level avatarLv = GetAvatarLevel(level);
            return avatarLv.__energy_limit;
        }

        public static LevelConfig GetLastLevelConfig(int nowLevel, int nowExp, int addExp)
        {
            LevelConfig avatarLevel = new LevelConfig(nowLevel, nowExp);
            if (nowLevel == 1 && nowExp == 0)
            {
                return avatarLevel;
            }
            if (nowExp >= addExp)
            {
                avatarLevel.Exp -= addExp;
            }
            else
            {
                int leftAddExp = addExp;
                leftAddExp -= nowExp;
                avatarLevel.Level -= 1;
                while (true)
                {
                    int nextExp = GetNextLevelExp(avatarLevel.Level);
                    if (leftAddExp > nextExp)
                    {
                        avatarLevel.Level -= 1;
                    }
                    else
                    {
                        avatarLevel.Exp = nextExp - leftAddExp;
                    }
                    leftAddExp -= nextExp;

                    if (leftAddExp <= 0)
                    {
                        break;
                    }
                }
            }
            return avatarLevel;
        }

        public static int GetExpStandard(int level)
        {
            if (XMLManager.avatar_level.ContainsKey(level))
            {
                return XMLManager.avatar_level[level].__exp_standard;
            }
            return 0;
        }

        public static int GetGoldStandard(int level)
        {
            if (XMLManager.avatar_level.ContainsKey(level))
            {
                return XMLManager.avatar_level[level].__gold_standard;
            }
            return 0;
        }

        public static List<BaseItemData> GetUpLevelCompensationReward(int playerLevel)
        {
            avatar_level config = GetAvatarLevel(playerLevel);
            if(config != null && config.__level_up_compensate_reward != 0)
            {
                return item_reward_helper.GetItemDataList(config.__level_up_compensate_reward);
            }
            return new List<BaseItemData>();
        }
    }
}
