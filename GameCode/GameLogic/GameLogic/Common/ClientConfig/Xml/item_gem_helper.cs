﻿#region 模块信息
/*==========================================
// 文件名：gem_item_helper
// 命名空间: GameData
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/2/9 17:08:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using Common.Global;
using Common.Utils;
using Common.Data;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;


namespace GameData
{
    public class item_gem_helper
    {
        private const int GOLD_ID = 1;  // 金币ID : 1
        private const int SOUL_STONE_ID = 11000;    // 灵魂石ID:11000
        public const int MAX_LEVEL = 10;
        public const int GEM_START_ID = 3001;
        public const int GEM_END_ID = 3090;
        public const int GEM_COMPOSITE_COST = 3;
        public const int MAX_SPIRIT_LEVEL = 100;
        public const int NEW_SLOT_OFFSET = 20;
        private static Dictionary<int, int> _slotTypeDesc;//Key为宝石槽类型，Value为语言配置Id
        private static Dictionary<int, int> _typeStartGemIdDict; //Key为宝石子类型， Value为子类型1级宝石开始Id
        private static Dictionary<int, int> _typeUnderlayDict; //Key为宝石槽类型， Value为宝石Icon衬底Icon id

        static item_gem_helper()
        {
            _slotTypeDesc = new Dictionary<int, int>();
            _slotTypeDesc.Add(1, 3101);//生命
            _slotTypeDesc.Add(2, 3102);//暴击
            _slotTypeDesc.Add(3, 3103);//破击
            _slotTypeDesc.Add(4, 3104);//伤害
            _slotTypeDesc.Add(5, 3105);//物防
            _slotTypeDesc.Add(6, 3106);//法防
            _slotTypeDesc.Add(7, 3107);//穿透
            _slotTypeDesc.Add(8, 3108);//攻强
            _slotTypeDesc.Add(9, 3109);//格挡
            _slotTypeDesc.Add(99, 3110);//通用

            _typeUnderlayDict = new Dictionary<int, int>();
            _typeUnderlayDict.Add(0, 3137);
            _typeUnderlayDict.Add(1, 3127);
            _typeUnderlayDict.Add(2, 3135);
            _typeUnderlayDict.Add(3, 3131);
            _typeUnderlayDict.Add(4, 3130);
            _typeUnderlayDict.Add(5, 3132);
            _typeUnderlayDict.Add(6, 3133);
            _typeUnderlayDict.Add(7, 3134);
            _typeUnderlayDict.Add(8, 3128);
            _typeUnderlayDict.Add(9, 3129);
            _typeUnderlayDict.Add(99, 3136);

            _typeStartGemIdDict = new Dictionary<int, int>();
            _typeStartGemIdDict.Add(1, 3001);
            _typeStartGemIdDict.Add(2, 3011);
            _typeStartGemIdDict.Add(3, 3021);
            _typeStartGemIdDict.Add(4, 3031);
            _typeStartGemIdDict.Add(5, 3041);
            _typeStartGemIdDict.Add(6, 3051);
            _typeStartGemIdDict.Add(7, 3061);
            _typeStartGemIdDict.Add(8, 3071);
            _typeStartGemIdDict.Add(9, 3081);
        }

        public static Dictionary<int, List<int>> gemTypeDict;
        public static List<int> GetGemTypeRankRange(int subType)
        {
            if (gemTypeDict == null)
            {
                gemTypeDict = new Dictionary<int, List<int>>();
                foreach (KeyValuePair<int, item_gem> pair in XMLManager.item_gem)
                {
                    int type = pair.Value.__subtype;
                    if (gemTypeDict.ContainsKey(type) == false)
                    {
                        gemTypeDict.Add(type, new List<int>());
                    }
                    gemTypeDict[type].Add(pair.Key);
                }
            }
            return gemTypeDict[subType];
        }

        public static int GetUnderlayBySlotType(int slotType)
        {
            return _typeUnderlayDict[slotType];
        }

        public static int GetUnderlayByGemType(int gemType)
        {
            return _typeUnderlayDict[gemType];
        }

        public static int GetGemIdBySubTypeAndLevel(int subType, int level)
        {
            return _typeStartGemIdDict[subType] + (level - 1);
        }

        public static string GetGemName(int gemId)
        {
            List<string> name = data_parse_helper.ParseListString(item_helper.GetItemConfig(gemId).__name);
            return name.Count == 0 ? gemId.ToString() : MogoLanguageUtil.GetContent(item_helper.GetItemConfig(gemId).__name);
        }

        public static int GetGemLevel(int gemId)
        {
            return XMLManager.item_gem[gemId].__gem_level;
        }

        public static item_gem GetItemGem(int gemId)
        {
            if (XMLManager.item_gem.ContainsKey(gemId) == false)
            {
                throw new Exception("配置表 item_gem.xml 未包含项 " + gemId.ToString());
            }
            return XMLManager.item_gem[gemId];
        }

        public static bool HasGem(int gemId)
        {
            return XMLManager.item_gem.ContainsKey(gemId);
        }

        public static bool HasGemOfType(int subType)
        {
            bool result = false;
            List<int> gemIdList = GetGemTypeRankRange(subType);
            for (int i = 0; i < gemIdList.Count; i++)
            {
                result |= PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(gemIdList[i]) > 0;
                if (result == true)
                {
                    return result;
                }
            }
            return result;
        }
        public static int GetNextLevelGemID(int gemId)
        {
            return XMLManager.item_gem[gemId].__next_id;
        }

        public static int GetGemSubtype(int gemId)
        {
            return XMLManager.item_gem[gemId].__subtype;
        }

        public static int ConvertGemScoreToDiamond(int score)
        {
            int factor = Convert.ToInt32(XMLManager.global_params[GlobalParamsEnum.GEM_SCORE_TO_DIAMOND_FACTOR].__value);
            return score * factor;
        }

        public static int GetGemDimondCost(int gemId)
        {
            int score = GetGemScore(gemId);
            return ConvertGemScoreToDiamond(score);
        }

        public static int GetGemScore(int gemId)
        {
            return XMLManager.item_gem[gemId].__gem_score;
        }

        public static bool IsCanUpgradeByLevel(int gemId)
        {
            int gemLevel = GetGemLevel(gemId);
            return gemLevel < MAX_LEVEL;
        }

        public static bool IsCanUpgradeByVipLevel(int gemId)
        {
            int gemLevel = GetGemLevel(gemId);
            return gemLevel < vip_rights_helper.GetComposableGemMaxLevel(PlayerAvatar.Player.vip_level);
        }

        public static bool IsCanUpgradeByTheSameGem(int gemId)
        {
            if (GetGemLevel(gemId) >= MAX_LEVEL)
            {
                return false;
            }
            return PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(gemId) >= GemManager.UPGRADE_NEED_COUNT;
        }

        public static Dictionary<int, int> CalculateUpgradeMaterialNeed(int gemId)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            int needNum = 1;
            int level = GetGemLevel(gemId);
            int type = GetGemSubtype(gemId);
            int minSameTypeGemId = GetGemIdBySubTypeAndLevel(type, 1);
            for (int id = gemId - 1; id >= minSameTypeGemId; id--)
            {
                needNum *= GemManager.COMPOSITE_NEEN_COUNT;
                int num = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(id);
                if (num >= needNum)
                {
                    result.Add(id, needNum);
                    return result;
                }
                if (num > 0)
                {
                    result.Add(id, num);
                }
                needNum -= num;
            }
            if (needNum > 0)
            {
                result.Add(public_config.MONEY_TYPE_DIAMOND, GetGemScore(minSameTypeGemId) * needNum * int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.gemValue)));
            }
            return result;
        }

        /// <summary>
        /// 计算升级gemId高一级宝石需要的分数，当为0时表示当前材料够
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static int CalculateUpgradeScroeNeed(int gemId, int count)
        {
            BagData itemBagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            int result = 0;
            if (GetGemLevel(gemId) == MAX_LEVEL)//已达最高等级
            {
                return result;
            }
            int countInBag = itemBagData.GetItemNum(gemId);
            if (countInBag >= count)
            {
                return result;
            }
            if (item_gem_helper.GetGemLevel(gemId) == 1)
            {
                result += item_gem_helper.GetGemScore(gemId) * (count - countInBag);
            }
            else
            {
                result += CalculateUpgradeScroeNeed(gemId - 1, GemManager.COMPOSITE_NEEN_COUNT * (count - countInBag));
            }
            return result;
        }

        public static string GetGemSubTypeDescById(int gemId)
        {
            int subtype = GetGemSubtype(gemId);
            return MogoLanguageUtil.GetContent(_slotTypeDesc[subtype]);
        }

        public static string GetGemSlotDesc(int slotType)
        {
            return MogoLanguageUtil.GetContent(_slotTypeDesc[slotType]);
        }

        public static int GetLevelNeeded(int gemId)
        {
            return XMLManager.item_gem[gemId].__level_need;
        }

        public static List<string> GetSlots(int gemId)
        {
            return data_parse_helper.ParseListString(XMLManager.item_gem[gemId].__gem_slot);
        }

        /// <summary>
        /// index：0普通祝福，1小爆击祝福，2大暴击祝福
        /// </summary>
        /// <param name="spiritLevel"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static int GetSpiritBlessImprove(int spiritLevel, int index)
        {
            List<string> list = data_parse_helper.ParseListString(XMLManager.gem_spirit[spiritLevel].__improve);
            return int.Parse(list[index]);
        }

        public static int GetSpiritGoldCost(int spiritLevel)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(XMLManager.gem_spirit[spiritLevel].__cost);
            return int.Parse(dict[GOLD_ID.ToString()]);
        }

        public static int GetSpiritSoulStoneCost(int spiritLevel)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(XMLManager.gem_spirit[spiritLevel].__cost);
            return int.Parse(dict[SOUL_STONE_ID.ToString()]);
        }

        public static int GetAttriEffectId(int gemId)
        {
            item_gem itemGem = GetItemGem(gemId);
            return itemGem.__attri_effect_id;
        }

        public static int GemComparison(IItem x, IItem y)
        {
            int xLevel = item_gem_helper.GetGemLevel(x.Id);
            int yLevel = item_gem_helper.GetGemLevel(y.Id);
            if (xLevel > yLevel)
            {
                return -1;
            }
            if (xLevel == yLevel)
            {
                int xSubType = item_gem_helper.GetGemSubtype(x.Id);
                int ySubType = item_gem_helper.GetGemSubtype(y.Id);
                if (xSubType < ySubType)
                {
                    return -1;
                }
                if (xSubType == ySubType)
                {
                    return 0;
                }
                return 1;
            }
            return 1;
        }

        public static int GetSlotUnlockedLevel(int slotIndex)
        {
            string slotSetting = global_params_helper.GetGlobalParam(20);
            string[] levelArray = slotSetting.Split(',');
            return int.Parse(levelArray[slotIndex]);
        }

        private const int PURPLE = 4;
        private const int ORANGE = 5;
        private const int GOLDEN = 6;
        public static int GetPunchItemId(int quality, int index)
        {
            index += 1;
            InitSlotDict();
            switch (quality)
            {
                case PURPLE:
                    return purpleSlotCostDict[index];
                case ORANGE:
                    return orangeSlotCostDict[index];
                case GOLDEN:
                    return goldenSlotCostDict[index];
                default:
                    return 0;
            }
        }


        private static Dictionary<int, int> purpleSlotCostDict;
        private static Dictionary<int, int> orangeSlotCostDict;
        private static Dictionary<int, int> goldenSlotCostDict;
        private static void InitSlotDict()
        {
            if (purpleSlotCostDict == null)
            {
                purpleSlotCostDict = MogoStringUtils.Convert2Dic(global_params_helper.GetGlobalParam(GlobalParamId.gemPurpleSlot));
            }
            if (orangeSlotCostDict == null)
            {
                orangeSlotCostDict = MogoStringUtils.Convert2Dic(global_params_helper.GetGlobalParam(GlobalParamId.gemOrangeSlot));
            }
            if (goldenSlotCostDict == null)
            {
                goldenSlotCostDict = MogoStringUtils.Convert2Dic(global_params_helper.GetGlobalParam(GlobalParamId.gemGoldenSlot));
            }
        }

        public static bool HasGemToComposite()
        {
            for (int itemId = GEM_START_ID; itemId <= GEM_END_ID; itemId++)
            {
                if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(itemId) >= GEM_COMPOSITE_COST
                    && GetGemLevel(itemId) < MAX_LEVEL
                    && IsCanUpgradeByVipLevel(itemId))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool HasGemToInlay()
        {
            bool result = false;
            //根据装备来选择宝石
            Dictionary<int, BaseItemInfo> equipDict = PlayerDataManager.Instance.BagData.GetBagData(BagType.BodyEquipBag).GetAllItemInfo();
            foreach (var pair in equipDict)
            {
                EquipItemInfo equipInfo = pair.Value as EquipItemInfo;
                result |= CheckEquipCanInlayGemOrCanUpgrade(equipInfo);
            }
            return result;
        }

        private static bool CheckEquipCanInlayGemOrCanUpgrade(EquipItemInfo equipInfo)
        {
            bool result = false;
            List<int> slotList = equipInfo.SlotList;
            List<int> newSlotList = equipInfo.NewSlotList;

            Dictionary<int, int> hasEquipedGemDict = GetEquipedGemDict(equipInfo.GemList);

            for (int i = 0; i < slotList.Count; i++)
            {
                if (hasEquipedGemDict.ContainsKey(i + 1) == false)
                {
                    result |= GetGemTypeCount(slotList[i]) > 0;
                }
                else // 如果需要提示有更好的宝石,在此处添加判断
                {

                }
            }

            for (int i = 0; i < newSlotList.Count && i < equipInfo.OpenedNewSlotCount; i++)
            {
                if (hasEquipedGemDict.ContainsKey(NEW_SLOT_OFFSET + i + 1) == false)
                {
                    result |= GetGemTypeCount(newSlotList[i]) > 0;
                }
            }
            return result;
        }

        private static Dictionary<int, int> GetEquipedGemDict(List<PbEquipGems> list)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].gem_id != 0)
                {
                    result.Add((int)list[i].index, (int)list[i].gem_id);
                }
            }
            return result;
        }

        private static int GetGemTypeCount(int gemType)
        {
            if (gemType == 99)
            {
                return 1;
            }
            List<int> gemList = GetGemTypeRankRange(gemType);
            int count = 0;
            for (int i = 0; i < gemList.Count; i++)
            {
                count += PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(gemList[i]);
            }
            return count;
        }

        private static List<int> StringListToIntList(List<string> list)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(int.Parse(list[i]));
            }
            return result;
        }

        public static bool CanSpirit()
        {
            if ((int)PlayerAvatar.Player.spirit_level < MAX_SPIRIT_LEVEL
                && CheckMaterialEnough((int)PlayerAvatar.Player.spirit_level))
            {
                return true;
            }
            return false;
        }

        private static bool CheckMaterialEnough(int spiritLevel)
        {
            if (XMLManager.gem_spirit.ContainsKey(spiritLevel))
            {
                gem_spirit gemSpiritCfg = XMLManager.gem_spirit[spiritLevel];
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(gemSpiritCfg.__cost);
                foreach (var pair in dataDict)
                {
                    int itemId = int.Parse(pair.Key);
                    int number = int.Parse(pair.Value);
                    if (itemId == 1 && PlayerAvatar.Player.money_gold < number)
                    {
                        return false;
                    }
                    else if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(itemId) < number)
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                throw new Exception("@章振宇 在宝石封灵配置表中未找到封灵等级为" + spiritLevel + "的配置选项");
            }
        }

    }
}
