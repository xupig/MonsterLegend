﻿using Common.ServerConfig;
using Common.Structs;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_level_helper
    {
        public static guild_level GetGuildLevel(int level)
        {
            if (XMLManager.guild_level.ContainsKey(level))
            {
                return XMLManager.guild_level[level];
            }
            return null;
        }

        public static int GetMaxLevel()
        {
            return XMLManager.guild_level.Count;
        }

        public static int GetUpgradeCostCurrency(int level)
        {
            guild_level config = GetGuildLevel(level);
            if(config != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__update_cost);
                foreach (string currency in dataDict.Keys)
                {
                    return int.Parse(currency);
                }
            }
            return 0;
        }

        public static int GetUpgradeCostValue(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__update_cost);
                foreach (string value in dataDict.Values)
                {
                    return int.Parse(value);
                }
            }
            return 0;
        }

        public static int GetDayCostCurrency(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__day_cost);
                foreach (string currency in dataDict.Keys)
                {
                    return int.Parse(currency);
                }
            }
            return 0;
        }

        public static int GetDayCostValue(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__day_cost);
                foreach (string value in dataDict.Values)
                {
                    return int.Parse(value);
                }
            }
            return 0;
        }

        public static int GetFundLimit(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__fund_limit;
            }
            return 0;
        }

        public static int GetContributionLimit(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__contribution_limit;
            }
            return 0;
        }

        public static int GetMemberCount(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__allmembers_count;
            }
            return 0;
        }

        public static int GetMemberCountByPosition(int level, int position)
        {
            if (position == public_config.GUILD_POSITION_LEADER)
            {
                return GetChairmanCount(level);
            }
            if (position == public_config.GUILD_POSITION_VICE_LEADER)
            {
                return GetViceChairmanCount(level);
            }
            if (position == public_config.GUILD_POSITION_DEACON)
            {
                return GetDeconCount(level);
            }
            if (position == public_config.GUILD_POSITION_ELITE)
            {
                return GetEliteCount(level);
            }
            return GetMemberCount(level);
        }

        public static int GetChairmanCount(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__chairman_count;
            }
            return 0;
        }

        public static int GetViceChairmanCount(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__vicechairman_count;
            }
            return 0;
        }

        public static int GetDeconCount(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__deacon_count;
            }
            return 0;
        }

        public static int GetEliteCount(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return config.__elite_count;
            }
            return 0;
        }

        public static Dictionary<string, string> GetGuildUpgradeCondition(int level)
        {
            guild_level config = GetGuildLevel(level);
            if (config != null)
            {
                return data_parse_helper.ParseMap(config.__update_condition);
            }
            return null;
        }

        public static string GetGuildUpgradeConditionContent(int level)
        {
            var dict = GetGuildUpgradeCondition(level);
            if (dict != null)
            {
                return guild_spell_helper.GetConditionContent(dict);
            }
            return string.Empty;
        }

        public static bool IsItemEnough(int itemId, int value)
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            if (itemId == 9 && value > data.Money)
            {
                return false;
            }
            return true;
        }
    }
}
