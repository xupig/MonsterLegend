﻿#region 模块信息
/*==========================================
// 文件名：achievement_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 15:54:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
namespace GameData
{
    public class achievement_helper
    {
        // 特殊组，表示已经领取过成就的组
        public const int HASREWARDED = 9999;
        private static Dictionary<int, int> _groupWeight;
        private static Dictionary<int, List<int>> _achievementDict;
        public static bool hasRewardAchievement;
        public static void InitAchievementDict()
        {
            if (_achievementDict != null)
            {
                return;
            }
            _groupWeight = new Dictionary<int, int>();
            _achievementDict = new Dictionary<int, List<int>>();
            foreach (KeyValuePair<int, achievement> pair in XMLManager.achievement)
            {
                if (_achievementDict.ContainsKey(pair.Value.__kind) == false)
                {
                    _achievementDict.Add(pair.Value.__kind, new List<int>());
                    _groupWeight.Add(pair.Value.__kind, pair.Value.__property);
                }
                _achievementDict[pair.Value.__kind].Add(pair.Value.__id);
            }
        }

        public static int GetTargetNums(int id)
        {
            if (XMLManager.achievement.ContainsKey(id))
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(XMLManager.achievement[id].__conds);
                foreach (var pair in dataDict)
                {
                    return int.Parse(pair.Value);
                }
            }
            return 0;
        }

        public static List<int> GetAchievementsByGroup(int groupId)
        {
            InitAchievementDict();
            return _achievementDict[groupId];
        }

        public static List<int> GetCategoryGroups()
        {
            InitAchievementDict();
            List<int> list = new List<int>(_achievementDict.Keys);
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (CheckNeedShow(list[i]) == false)
                {
                    list.RemoveAt(i);
                }
            }
            list.Sort(SortFunction);
            if (PlayerDataManager.Instance.AchievementData.HasRewardedTask() == true)
            {
                hasRewardAchievement = true;
                list.Add(HASREWARDED);
            }
            return list;
        }

        private static bool CheckNeedShow(int groupId)
        {
            List<int> list = _achievementDict[groupId];
            for (int i = 0; i < list.Count; i++)
            {
                if (GetAchievementCfg(list[i]).__is_show == 1)
                {
                    return true;
                }
            }
            return false;
        }

        private static achievement GetAchievementCfg(int achievementId)
        {
            return XMLManager.achievement[achievementId];
        }

        private static int SortFunction(int x, int y)
        {
            return _groupWeight[x] - _groupWeight[y];
        }

        public static string GetAchievementName(int achieveId)
        {
            if (XMLManager.achievement.ContainsKey(achieveId))
            {
                return MogoLanguageUtil.GetContent(XMLManager.achievement[achieveId].__name);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetAchievementDesc(int achieveId)
        {
            if (XMLManager.achievement.ContainsKey(achieveId))
            {
                return MogoLanguageUtil.GetContent(XMLManager.achievement[achieveId].__describe);
            }
            else
            {
                return string.Empty;
            }
        }

        public static List<RewardData> GetAchievementReward(int achieveId)
        {
            if (XMLManager.achievement.ContainsKey(achieveId))
            {
                int rewardId = XMLManager.achievement[achieveId].__reward_id;
                return item_reward_helper.GetRewardDataList(rewardId, (Vocation)PlayerAvatar.Player.vocation);
            }
            return null;
        }

        public static bool NeedShowGetTip(int achieveId)
        {
            if (XMLManager.achievement.ContainsKey(achieveId))
            {
                return XMLManager.achievement[achieveId].__is_show == 1;
            }
            return false;
        }
    }
}