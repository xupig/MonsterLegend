﻿#region 模块信息
/*==========================================
// 文件名：event_cnds_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/9 17:55:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Common.ServerConfig;

namespace GameData
{
    public enum EventCondsParamId
    {
        MonsterId = 1,
        CopyId = 2,
        ItemId = 3,
        OnceNum = 9  //单次数量
    }

    public enum EventAtomId
    {
        ItemTotalCount = 9,
        DayLogin = 27,  //每日登陆
        DayOnlineTime = 43,
        RightsCard = 46,  //特权卡，月卡、季卡等
        DemonGate = 58,
        LimitTimeTotalCharge = 124  //每日累计充值
    }

    public class event_cnds_helper
    {
        public enum FollowId
        {
            showView = 1,
            showCopy,
            enterCopy,
            trigger,
            goToNpc,
        }

        private static char[] split = new char[] { ';'};
        private static char[] equal = new char[] { '='};
        private static char[] less = new char[] { '{'};
        private static char[] bigger = new char[] { '}'};
        private static Dictionary<int,Dictionary<int,float>> eventDic;

        public static void Init()
        {
            int count = XMLManager.event_cnds.Count;
        }

        public static event_cnds GetEvent(int eventId)
        {
            if(XMLManager.event_cnds.ContainsKey(eventId))
            {
                return XMLManager.event_cnds[eventId];
            }
            return null;
        }

        public static Dictionary<string,string[]> GetEventFollow(int eventId)
        {
            event_cnds cnds = GetEvent(eventId);
            if(cnds != null)
            {
                return data_parse_helper.ParseMapList(cnds.__follow);
            }
            return null;
        }

        public static bool ContainFollowId(int eventId, FollowId followId)
        {
            return GetFollowParam(eventId, followId) != null;
        }

        public static string[] GetFollowParam(int eventId, FollowId id)
        {
            Dictionary<string,string[]> followDict =  GetEventFollow(eventId);
            if (followDict != null && followDict.ContainsKey(((int)id).ToString()))
            {
                return followDict[((int)id).ToString()];
            }
            return null;
        }

        public static Dictionary<int, float> GetEventCnds(int eventId)
        {
            if(eventDic == null)
            {
                eventDic = new Dictionary<int, Dictionary<int, float>>();
            }
            if(eventDic.ContainsKey(eventId) == false)
            {
                eventDic.Add(eventId,new Dictionary<int,float>());
                event_cnds cnds = GetEvent(eventId);
                if (cnds != null && (cnds.__conds!=null || cnds.__conds_or!=null))
                {
                    string[] list = null;
                  
                    if(cnds.__conds!=null && cnds.__conds.Length>0)
                    {
                        list = cnds.__conds.Split(split);
                    }
                    else
                    {
                        list = cnds.__conds_or.Split(split);
                    }
                    string[] data;
                    foreach(string str in list)
                    {
                       
                        if(str.IndexOf('}') != -1)
                        {
                           data  = str.Split(bigger);
                        }
                        else if(str.IndexOf('=')!=-1)
                        {
                            data = str.Split(equal);
                        }
                        else if(str.IndexOf('{')!=-1)
                        {
                            data = str.Split(less);
                        }
                        else
                        {
                            data = null;
                        }
                        if (data != null && eventDic[eventId].ContainsKey(int.Parse(data[0])) == false)
                        {
                            eventDic[eventId].Add(int.Parse(data[0]), float.Parse(data[1]));
                        }
                    }
                }
            }
            return eventDic[eventId];
        }


        public static bool IsContainConditionType(int eventId, int conditionType)
        {
            Dictionary<int, float> conditionDict = GetEventCnds(eventId);
            return conditionDict.ContainsKey(conditionType);
        }

        public static int GetInstanceId(int eventId)
        {
            Dictionary<int, float> conditionDict = GetEventCnds(eventId);
            return (int)conditionDict[event_config.EVT_ID_COMPLETE_COPY];
        }

        public static void Follow(int eventId)
        {
            string[] paramArray = GetFollowParam(eventId, FollowId.showView);
            if(paramArray == null)
            {
                Debug.LogError("没有参数 eventId:" + eventId);
                return;
            }
            view view = view_helper.GetView(int.Parse(paramArray[0]));
            if (paramArray.Length > 1)
            {
                List<int> paramList = new List<int>();
                for (int i = 1; i < paramArray.Length; i++)
                {
                    paramList.Add(int.Parse(paramArray[i]));
                }
                view_helper.OpenView(view.__id, paramList.ToArray<int>());
            }
            else
            {
                view_helper.OpenView(view.__id);
            }
        }
    }
}
