﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Global;
using UnityEngine;
using Common.Data;
using Common.Utils;
using GameLoader.Utils;

namespace GameData
{
    public enum GlobalParamId
    {
        gemValue = 18,
        gemPurpleSlot = 21,
        gemOrangeSlot = 22,
        gemGoldenSlot = 23,
        minFriendDegreeLevelToGiveFriend = 69,
        dailyMaxWorthToGiveFriend = 70,
        minAccumulateCostToGiveFriend = 71,
        petAttri = 77,
        rankingListUpdateTime = 81,
        guild_apply_join_cd_i = 93,
        demonGateTransportCostItem = 105,
        invite_cd = 113,
        treasureTransportCostItem = 124,
        treasureMapDayUseTimes = 131,
        friend_recommend = 141,
        scene_change_mask = 144,
        petReviveCost = 145,
        petReviveCd = 146,
        guild_activity_open = 148,
        boss_part_select_show = 155,
        not_show_equip_list = 156,
        pk_wait_time = 158,
        bag_number_all = 160,
        petGetWay = 167,
        walking_sound = 168,
        triggerWidth = 170,
        transfer_item_to_player_time = 171,
        captain_auto_distribution_time = 172,
        guild_match_showtime = 176,
        auto_mount_duration = 179,
        one_vs_one_params = 181,
        copySweepTimes = 182,
        guild_shop_reset = 183,
        one_vs_one_back_params = 186,
        team_task_ai = 188,
        guild_shop_admire_limit = 198,
        fight_tips_cd_time = 206,
        touches_camera_params = 202,
        touches_camera_screen_region = 203,
        oper_server_final_rewards_advance = 208,
        mouse_camera_params = 210,
        first_charge_reward_id = 211,
        touches_camera_default_rate = 213,
        touches_camera_back_duration = 214,
        open_server_final_rewards = 215,
        bind_phone_open = 217,
        pet_remind_limit = 218,
        riding_sound = 220,
        task_guide_level = 224,
        press_mode_ai = 223,
        open_notice_level = 225,
        level_control = 228,
        rotate_camera_params = 229,
        team_addition = 241,
        match_lasting_time = 242,
        team_guide = 253,
    }

    public class global_params_helper
    {
        public static float[] _highLightSetting;
        private static Dictionary<int, int> _instancePetDict;
        private static Dictionary<int, BaseItemData> _petReviveCostDict;
        private static Dictionary<int, int> _petReviveCdDict;
        private static float _triggerWidth = 0;
        private static int _spaceTimeRiftCityAI = 0;
        private static List<int> _standByActionList;
        private static int _fightTipsCdTime = -1;
        private static bool _hasInitAiIntervalParams = false;

        public static void OnReloadData()
        {
            _highLightSetting = null;
            _instancePetDict = null;
            _petReviveCostDict = null;
            _petReviveCdDict = null;
            _triggerWidth = 0;
            _spaceTimeRiftCityAI = 0;
            _standByActionList = null;
            _fightTipsCdTime = -1;
            _hasInitAiIntervalParams = false;
        }

        public static string GetGlobalParam(int id)
        {
            global_params config = GetConfig(id);
            return config.__value;
        }

        public static string GetGlobalParam(GlobalParamId id)
        {
            global_params config = GetConfig((int)id);
            return config.__value;
        }

        public static T[] StringToArray<T>(string strParam, Func<string, T> transform)
        {
            string[] strParamArray = strParam.Split(',');
            T[] paramArray = new T[strParamArray.Length];
            for (int i = 0; i < strParamArray.Length; i++)
            {
                paramArray[i] = transform(strParamArray[i]);
            }
            return paramArray;
        }

        public static global_params GetConfig(int id)
        {
            if(XMLManager.global_params.ContainsKey(id) == false)
            {
                throw new Exception("配置表global_params.xml 未找到配置项： " + id);
            }
            return XMLManager.global_params[id];
        }

        public static float[] GetHighLightSetting()
        {
            if (_highLightSetting == null)
            {
                var data = GetConfig(98);
                var temp = data.__value.Split(',');
                _highLightSetting = new float[temp.Length];
                for(int i = 0; i < temp.Length; i++)
                {
                    _highLightSetting[i] = float.Parse(temp[i]);
                }
            }
            return _highLightSetting;
        }

        public static Dictionary<int, int> GetInstancePetDict()
        {
            if (_instancePetDict == null)
            {
                _instancePetDict = new Dictionary<int, int>();
                var data = GetConfig(140);
                var temp = data.__value.Split(',');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _instancePetDict.Add(key, value);
                }
            }
            return _instancePetDict;
        }

        public static BaseItemData GetPetReviveCost(int petLevel)
        {
            if (_petReviveCostDict == null)
            {
                string source = GetGlobalParam(GlobalParamId.petReviveCost);
                Dictionary<int, List<int>> dict = MogoStringUtils.Convert2OneToMany(source);
                _petReviveCostDict = new Dictionary<int, BaseItemData>();
                foreach (int level in dict.Keys)
                {
                    List<int> valueList = dict[level];
                    BaseItemData baseItemData = ItemDataCreator.Create(valueList[0]);
                    baseItemData.StackCount = valueList[1];
                    _petReviveCostDict.Add(level, baseItemData);
                }
            }
            return _petReviveCostDict[petLevel];
        }

        public static int GetPetReviveCd(int petLevel)
        {
            if (_petReviveCdDict == null)
            {
                string source = GetGlobalParam(GlobalParamId.petReviveCd);
                _petReviveCdDict = MogoStringUtils.Convert2Dic(source);
            }
            return _petReviveCdDict[petLevel];
        }

        //获取实体点击碰撞盒宽度
        public static float GetTriggerWidth()
        {
            if (_triggerWidth == 0)
            {
                _triggerWidth = float.Parse(GetGlobalParam(GlobalParamId.triggerWidth));
            }
            return _triggerWidth;
        }

        public static int GetCopySweepMaxTimes()
        {
            return int.Parse(GetGlobalParam(GlobalParamId.copySweepTimes));
        }

        //获取时空裂隙主城/野外中的AI
        public static int GetSpaceTimeRiftCityAI()
        {
            if (_spaceTimeRiftCityAI == 0)
            {
                _spaceTimeRiftCityAI = int.Parse(GetGlobalParam(GlobalParamId.team_task_ai));
            }
            return _spaceTimeRiftCityAI;
        }

        public static int GetFightTipsCdTime()
        {
            if (_fightTipsCdTime == -1)
            {
                _fightTipsCdTime = int.Parse(GetGlobalParam(GlobalParamId.fight_tips_cd_time));
            }
            return _fightTipsCdTime;
        }

        public static List<int> GetPlayerStandByList()
        {
            if (_standByActionList != null)
            {
                return _standByActionList;
            }
            string[] args = global_params_helper.GetGlobalParam(154).Split(',');
            _standByActionList = new List<int>();
            for (int i = 2; i < args.Length; ++i)
            {
                _standByActionList.Add(int.Parse(args[i]));
            }
            return _standByActionList;
        }

        private static float _aiInterval;
        private static float _playerAIInterval;
        public static float GetAIInterval()
        {
            if (_hasInitAiIntervalParams)
            {
                return _aiInterval;
            }
            string[] args = global_params_helper.GetGlobalParam(227).Split(',');
            _aiInterval = float.Parse(args[1]);
            _playerAIInterval = float.Parse(args[0]);
            _hasInitAiIntervalParams = true;
            return _aiInterval;
        }

        public static float GetPlayerAIInterval()
        {
            if (_hasInitAiIntervalParams)
            {
                return _playerAIInterval;
            }
            string[] args = global_params_helper.GetGlobalParam(227).Split(',');
            _aiInterval = float.Parse(args[1]);
            _playerAIInterval = float.Parse(args[0]);
            _hasInitAiIntervalParams = true;
            return _playerAIInterval;
        }

        public static int GetRewardCompensationLevelInterval()
        {
            return int.Parse(global_params_helper.GetGlobalParam(222));
        }

        private static float _flyOperation = 0f;
        private static int _flyChineseID = 0;
        public static float GetFlyOperation()
        {
            if (Mathf.Approximately(_flyOperation, 0))
            {
                InitRideFlyParams();
            }
            return _flyOperation;
        }

        public static int GetFlyChineseID()
        {
            if (_flyChineseID == 0)
            {
                InitRideFlyParams();
            }
            return _flyChineseID;
        }

        private static void InitRideFlyParams()
        {
            string[] args = global_params_helper.GetGlobalParam(254).Split(',');
            bool resultOperation = float.TryParse(args[0], out _flyOperation);
            bool resultChineseID = int.TryParse(args[1], out _flyChineseID);
            if (!resultOperation || !resultChineseID)
            {
                LoggerHelper.Error("全局表254配置格式错误。");
            }
        }
    }
}
