﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class guild_task_helper
    {
        public static guild_task GetGuildTask(int id)
        {
            if (XMLManager.guild_task.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_task.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_task[id];
        }

        public static int GetTaskDifficulty(int id)
        {
            return GetGuildTask(id).__difficulty;
        }

        public static int GetTaskId(int id)
        {
            return GetGuildTask(id).__task_id;
        }

        public static int GetRewardScore(int id)
        {
            return GetGuildTask(id).__score;
        }

        public static int GetRewardContribution(int id)
        {
            return GetGuildTask(id).__guild_contribution_reward;
        }

        public static int GetRewardFund(int id)
        {
            return GetGuildTask(id).__guild_fund_reward;
        }

        public static bool CanAccept(PbGuildTaskInfo data)
        {
            if (data.task_num > 0)
            {
                return true;
            }
            return GetGuildTask(data.guild_task_id).__task_num == 0;
        }

        public static bool IsEnd(PbGuildTaskInfo data)
        {
            if (CanAccept(data))
            {
                return false;
            }
            for (int i = 0; i < data.player_info_list.Count; i++)
            {
                if (data.player_info_list[i].task_status == public_config.GTS_ACCEPT)
                {
                    return false;
                }
            }
            return true;
        }

        public static int GetTaskGuildId(int taskId)
        {
            foreach (KeyValuePair<int, guild_task> kvp in XMLManager.guild_task)
            {
                if (kvp.Value.__task_id == taskId)
                {
                    return kvp.Value.__id;
                }
            }
            return 0;
        }

        public static int GetCurrentTaskId()
        {
            Dictionary<int, PbTaskInfo> branchTaskDic = PlayerDataManager.Instance.TaskData.branchTaskDic;
            foreach (KeyValuePair<int, PbTaskInfo> kvp in branchTaskDic)
            {
                if (task_data_helper.GetTaskType(kvp.Value.task_id) == public_config.TASK_TYPE_GUILD)
                {
                    return kvp.Value.task_id;
                }
            }
            return 0;
        }

        public static bool HasGuildTask()
        {
            return GetCurrentTaskId() > 0;
        }

        public static int GetMyFinshTimes(PbGuildTaskInfo data)
        {
            for (int i = 0; i < data.player_info_list.Count; i++)
            {
                if ((ulong)data.player_info_list[i].avatar_dbid == PlayerAvatar.Player.dbid)
                {
                    return (int)data.player_info_list[i].accomplish_times;
                }
            }
            return 0;
        }

        public static bool HasTimesLimit(int guildTaskId)
        {
            return GetGuildTask(guildTaskId).__task_num > 0;
        }
    }
}
