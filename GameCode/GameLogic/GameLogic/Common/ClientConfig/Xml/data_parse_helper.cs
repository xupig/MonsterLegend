﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameData
{

    public static class data_parse_helper
    {

        public static List<string> ParseListString(string source)
        {
            List<string> result = null;
            if (source != null)
            {
                result = new List<string>();
                string[] sourceArray = source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(sourceArray[i]);
                }
            }
            return result;
        }

        public static List<string> ParseListString(string[] source)
        {
            List<string> result = null;
            if (source != null)
            {
                result = new List<string>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(source[i]);
                }
            }
            return result;
        }

        public static List<int> ParseListInt(string source)
        {
            List<int> result = null;
            if (source != null)
            {
                result = new List<int>();
                string[] sourceArray =  source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(int.Parse(sourceArray[i]));
                }
            }
            return result;
        }

        public static List<int> ParseListInt(List<string> source)
        {
            List<int> result = null;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<int> ParseListInt(string[] source)
        {
            List<int> result = null;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(string source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                string[] sourceArray = source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(float.Parse(sourceArray[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(List<string> source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(string[] source)
        {
            List<float> result = null;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static Dictionary<int, int> ParseDictionaryIntInt(string source)
        {
            return ParseDictionaryIntInt(ParseMap(source));
        }

        public static Dictionary<int, int> ParseDictionaryIntInt(Dictionary<string, string> source)
        {
            Dictionary<int, int> result = null;
            if (source != null)
            {
                result = new Dictionary<int, int>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), int.Parse(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, float> ParseDictionaryIntFloat(string source)
        {
            return ParseDictionaryIntFloat(ParseMap(source));
        }

        public static Dictionary<int, float> ParseDictionaryIntFloat(Dictionary<string, string> source)
        {
            Dictionary<int, float> result = null;
            if (source != null)
            {
                result = new Dictionary<int, float>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), float.Parse(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<int>> ParseDictionaryIntListInt(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<int>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<int>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListInt(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<int>> ParseDictionaryIntListInt(string source)
        {
            return ParseDictionaryIntListInt(ParseMapList(source));
        }

        public static Dictionary<int, List<float>> ParseDictionaryIntListFloat(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<float>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<float>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListFloat(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<float>> ParseDictionaryIntListFloat(string source)
        {
            return ParseDictionaryIntListFloat(ParseMapList(source));
        }

        public static Dictionary<int, List<string>> ParseDictionaryIntListString(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<string>> result = null;
            if (source != null)
            {
                result = new Dictionary<int, List<string>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListString(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<string>> ParseDictionaryIntListString(string source)
        {
            return ParseDictionaryIntListString(ParseMapList(source));
        }

        public static Dictionary<string, string> ParseMap(string strMap)
        {
            Dictionary<String, String> result = new Dictionary<String, String>();
            if (string.IsNullOrEmpty(strMap))
            {
                return result;
            }

            string[] map = strMap.Split(',');//根据字典项分隔符分割字符串，获取键值对字符串
            for (int i = 0; i < map.Length; i++)
            {
                if (String.IsNullOrEmpty(map[i]))
                {
                    continue;
                }

                string[] keyValuePair = map[i].Split(':');//根据键值分隔符分割键值对字符串
                if (keyValuePair.Length == 2)
                {
                    if (!result.ContainsKey(keyValuePair[0]))
                        result.Add(keyValuePair[0], keyValuePair[1]);
                    else
                        LoggerHelper.Error(String.Format(" Key {0} already exist index {1} of {2}", keyValuePair[0], i, strMap));
                }
                else
                {
                    LoggerHelper.Error(String.Format(" KeyValuePair are not match: {0} index {1} of {2}", map[i], i, strMap));
                }
            }
            return result;
        }

        public static Dictionary<string, string[]> ParseMapList(string strMap)
        {
            Dictionary<String, string[]> result = new Dictionary<String, string[]>();
            if (string.IsNullOrEmpty(strMap))
            {
                return result;
            }

            string[] map = strMap.Split(';');//根据字典项分隔符分割字符串，获取键值对字符串
            for (int i = 0; i < map.Length; i++)
            {
                if (String.IsNullOrEmpty(map[i]))
                {
                    continue;
                }

                string[] keyValuePair = map[i].Split(':');//根据键值分隔符分割键值对字符串
                if (keyValuePair.Length == 2)
                {
                    if (!result.ContainsKey(keyValuePair[0]))
                        result.Add(keyValuePair[0], keyValuePair[1].Split(','));
                    else
                        LoggerHelper.Error(String.Format(" Key {0} already exist, index {1} of {2}.", keyValuePair[0], i, strMap));
                }
                else
                {
                    LoggerHelper.Error(String.Format(" KeyValuePair are not match: {0}, index {1} of {2}.", map[i], i, strMap));
                }
            }
            return result;
        }
    }
}
