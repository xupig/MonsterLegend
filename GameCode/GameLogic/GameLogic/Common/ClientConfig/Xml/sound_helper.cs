﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class sound_helper
    {
        
        public static sound GetSoundData(int id)
        {
            if (!XMLManager.sound.ContainsKey(id))
            {
                throw new Exception("[音效语音配置表]未包含项: " + id.ToString());
            }
            return XMLManager.sound[id];
        }

        public static int Priority(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
             {
                 return snd.__priority;
             }
            LoggerHelper.Error("[sound_helper:Priority]=>ID: " + id + " 找不到相应音效数据,@方晓之把[音效语音配置表]中数据配全!");
            return 0;
        }

        public static string Path(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__path;
            }
            LoggerHelper.Error("[sound_helper:Path]=>ID: " + id + " 的音效路径是空,@方晓之把[音效语音配置表]中数据配全!");
            return string.Empty;
        }

        public static int Loop(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__loop;
            }
            LoggerHelper.Error("[sound_helper:Path]=>ID: " + id + " 循环信息没有配置,@方晓之把[音效语音配置表]中数据配全!");
            return 0;
        }


    }
}
