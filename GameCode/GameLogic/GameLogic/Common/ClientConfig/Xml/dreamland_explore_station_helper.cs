﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 11:35:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameData;
using UnityEngine;

namespace GameData
{
    public class DreamlandExploreChapter
    {
        public List<dreamland_explore_station> StationList = new List<dreamland_explore_station>();

        public void AddStation(dreamland_explore_station station)
        {
            StationList.Add(station);
        }
    }

    public class dreamland_explore_station_helper
    {
        private static Dictionary<int, DreamlandExploreChapter> _idToChapterDict = new Dictionary<int, DreamlandExploreChapter>();

        static dreamland_explore_station_helper()
        {
            foreach (int id in XMLManager.dreamland_explore_station.Keys)
            {
                dreamland_explore_station station = GetConfig(id);
                DreamlandExploreChapter chapter = GetDreamlandExploreChapter(station.__land_id);
                chapter.AddStation(XMLManager.dreamland_explore_station[id]);
            }
        }

        private static DreamlandExploreChapter GetDreamlandExploreChapter(int id)
        {
            if (_idToChapterDict.ContainsKey(id))
            {
                return _idToChapterDict[id];
            }
            DreamlandExploreChapter chapter = new DreamlandExploreChapter();
            _idToChapterDict.Add(id, chapter);
            return chapter;
        }

        private static dreamland_explore_station GetConfig(int id)
        {
            return XMLManager.dreamland_explore_station[id];
        }

        public static int GetDreamlandChapterMaxStationNum(int id)
        {
            return _idToChapterDict[id].StationList.Count;
        }

        public static string GetTitle(int id, int station)
        {
            return MogoLanguageUtil.GetContent(_idToChapterDict[id].StationList[station].__name);
        }

        public static string GetDesc(int id, int station)
        {
            return MogoLanguageUtil.GetContent(_idToChapterDict[id].StationList[station].__descpt);
        }

        public static float GetAdditionRate(int id, int station)
        {
            return _idToChapterDict[id].StationList[station].__add_factor / 10000.0f;
        }
    }
}
