﻿using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class tutor_event_helper
    {
        public static tutor_event GetConfig(int id)
        {
            if (XMLManager.tutor_event.ContainsKey(id) == false)
            {
                throw new Exception("tutor_event.xml 中未找到配置项: " + id);
            }
            return XMLManager.tutor_event[id];
        }

        public static string GetEventDescription(int id)
        {
            return MogoLanguageUtil.GetContent(GetConfig(id).__describe);
        }

    }
}
