﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 11:33:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Utils;
using UnityEngine;

namespace GameData
{
    public class dreamland_explore_base_helper
    {
        public static dreamland_explore_base GetConfig(int id)
        {
            if (!XMLManager.dreamland_explore_base.ContainsKey(id))
            {
                throw new Exception("dreamland_explore_base找不到id:" + id);
            }
            return XMLManager.dreamland_explore_base[id];
        }

        public static int GetDailyExlporeTimes(int id)
        {
            return GetConfig(id).__daily_convoy_times;
        }

        public static BaseItemData GetQualityUpCost(int id)
        {
            return price_list_helper.GetCost(GetConfig(id).__upgrade_quality_cost);
        }

        public static BaseItemData GetRefreshCost(int id)
        {
            return price_list_helper.GetCost(GetConfig(id).__fresh_adversary_price);
        }

        public static BaseItemData GetMinusTimeCost(int id)
        {
            return price_list_helper.GetCost(GetConfig(id).__clear_cd_five_min_price);
        }

        public static BaseItemData GetFinishImmediateCost(int id)
        {
            return price_list_helper.GetCost(GetConfig(id).__immediate_complete_price);
        }

        public static BaseItemData GetClearRobCooldownCost(int id)
        {
            return price_list_helper.GetCost(GetConfig(id).__clear_attack_cd_price);
        }

        public static BaseItemData GetBuyRobTimesCost(int id, int num)
        {
            return price_list_helper.GetCost(GetConfig(id).__attack_times_price, num);
        }

        public static BaseItemData GetAttackSameTargetCost(int id, int num)
        {
            return price_list_helper.GetCost(GetConfig(id).__attack_again_price, num);
        }

        public static int GetDailyMaxRobTimes(int id)
        {
            return GetConfig(id).__daily_attack_times;
        }

        public static int GetOnceBeRobedMaxTimes(int id)
        {
            return GetConfig(id).__convoy_attacked_times;
        }

        public static int GetMaxRevengeTimes(int id)
        {
            return GetConfig(id).__revenge_times;
        }

        public static ItemData GetQualityUpItemCost(int id)
        {
            dreamland_explore_base config = GetConfig(id);
            ItemData itemData = null;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__up_qualityitem_cost);
            foreach (string itemId in dataDict.Keys)
            {
                itemData = new ItemData(int.Parse(itemId));
                itemData.StackCount = int.Parse(dataDict[itemId]);
                break;
            }
            return itemData;
        }

        public static string GetName(int id)
        {
            return MogoLanguageUtil.GetContent(GetConfig(id).__name);
        }

        public static string GetShortName(int id)
        {
            return MogoLanguageUtil.GetContent(GetConfig(id).__animal_name);
        }
    }
}
