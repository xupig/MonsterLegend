﻿
using ShaderUtils;
using UnityEngine;
namespace GameData
{
    public class entity_ghost_helper
    {
        private static string _ghostShaderName = "MOGO2/Character/Player_Ghost";
        private static string _shaderName;

        public static int GetGhostBuffID()
        {
            return int.Parse(global_params_helper.GetGlobalParam(147));
        }

        public static void SetGhostShader(Transform modelTrans)
        {
            _shaderName = GetShaderName(modelTrans);
            SetShader(modelTrans, _ghostShaderName);
        }

        public static void ResumeGhostShader(Transform modelTrans)
        {
            if (string.IsNullOrEmpty(_shaderName))
            {
                return;
            }
            SetShader(modelTrans, _shaderName);
        }

        private static void SetShader(Transform modelTrans, string shaderName)
        {
            var renderer = modelTrans.GetComponent<SkinnedMeshRenderer>();
            //Material material = Application.isEditor ? renderer.material : renderer.sharedMaterial;
            Material material = renderer.material;
            var shader = ShaderRuntime.loader.Find(shaderName);
            if (shader != null)
            {
                material.shader = shader;
            }
        }

        private static string GetShaderName(Transform modelTrans)
        {
            var renderer = modelTrans.GetComponent<SkinnedMeshRenderer>();
            //Material material = Application.isEditor ? renderer.material : renderer.sharedMaterial;
            Material material = renderer.material;
            return material.shader.name;
        }
    }
}
