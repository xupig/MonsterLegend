﻿using Common.Data;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    class equip_smelting_helper
    {
        private static Dictionary<int, List<equip_smelting>> _levelToSmeltingIdDic;

        static equip_smelting_helper()
        {
            _levelToSmeltingIdDic = new Dictionary<int, List<equip_smelting>>();
            foreach (KeyValuePair<int, equip_smelting> pair in XMLManager.equip_smelting)
            {
                if (_levelToSmeltingIdDic.ContainsKey(pair.Value.__level) == false)
                {
                    List<equip_smelting> idList = new List<equip_smelting>();
                    _levelToSmeltingIdDic.Add(pair.Value.__level, idList);
                }
                _levelToSmeltingIdDic[pair.Value.__level].Add(pair.Value);
            }
        }

        public static equip_smelting GetConfig(int id)
        {
            if (!XMLManager.equip_smelting.ContainsKey(id))
            {
                throw new Exception("配置表 equip_smelting 找不到id:" + id);
            }
            return XMLManager.equip_smelting[id];
        }

        public static bool IsSmeltStone(int id)
        {
            foreach (KeyValuePair<int, equip_smelting> pair in XMLManager.equip_smelting)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(pair.Value.__cost);
                if (dataDict.ContainsKey(id.ToString()) == true && item_helper.isMoneyType(id) == false)
                {
                    return true;
                }
            }
            return false;
        }

        public static List<int> GetLevelList()
        {
            List<int> result = _levelToSmeltingIdDic.Keys.ToList();
            result.Sort();
            return result;
        }

        public static string GetLevelName(int level)
        {
            return MogoLanguageUtil.GetContent(_levelToSmeltingIdDic[level][0].__level_name);
        }

        public static List<equip_smelting> GetAllSmeltByLevel(int level)
        {
            if (_levelToSmeltingIdDic.ContainsKey(level) == true)
            {
                return _levelToSmeltingIdDic[level];
            }
            return new List<equip_smelting>();
        }

        public static List<BaseItemData> GetCostList(int smeltingId)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            Dictionary<string, string> cost = data_parse_helper.ParseMap(GetConfig(smeltingId).__cost);
            foreach(KeyValuePair<string, string> kvp in cost)
            {
                BaseItemData itemData = ItemDataCreator.Create(int.Parse(kvp.Key));
                itemData.StackCount = int.Parse(kvp.Value);
                result.Add(itemData);
            }

            return result;
        }

        public static Dictionary<int, int> GetBodyToBodyIconDic()
        {
            if (PlayerAvatar.Player.vocation == Vocation.Warrior)
            {
                return data_parse_helper.ParseDictionaryIntInt(GetConfig(1).__zhanshi_icon);
            }
            else if (PlayerAvatar.Player.vocation == Vocation.Wizard)
            {
                return data_parse_helper.ParseDictionaryIntInt(GetConfig(1).__fashi_icon);
            }
            else
            {
                return data_parse_helper.ParseDictionaryIntInt(GetConfig(1).__qiangshou_icon);
            }
        }

        public static List<int> GetProductEquipBody(int smeltingId)
        {
            List<BaseItemData> itemDataList = item_reward_helper.GetItemDataList(GetConfig(smeltingId).__reward);
            List<int> result = new List<int>();
            for (int i = 0; i < itemDataList.Count; i++)
            {
                BaseItemData baseItemData = itemDataList[i] as BaseItemData;
                if (item_helper.IsEquip(baseItemData.Id))
                {
                    EquipData equipData = baseItemData as EquipData;
                    if (result.Contains((int)equipData.SubType) == false)
                    {
                        result.Add((int)equipData.SubType);
                    }
                }
            }
            return result;
        }

        internal static equip_smelting GetConfigByItemId(int smeltItemId)
        {
            foreach (KeyValuePair<int, equip_smelting> pair in XMLManager.equip_smelting)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(pair.Value.__cost);
                if (dataDict.ContainsKey(smeltItemId.ToString()) == true && item_helper.isMoneyType(smeltItemId) == false)
                {
                    return pair.Value;
                }
            }
            return null;
        }
    }
}
