﻿using System;
using System.Collections.Generic;
using GameMain.Entities;

namespace GameData
{
    public enum TurntableRewardTag
    {
        ItemId = 0,
        ItemNum = 1,
        Weight = 2,
        LuckyRank = 3,
        BindTag = 4
    }

    public class lucky_turntable_helper
    {
        public static lucky_turntable GetTurntableInfo(int turntableId)
        {
            if (XMLManager.lucky_turntable.ContainsKey(turntableId))
            {
                return XMLManager.lucky_turntable[turntableId];
            }
            return null;
        }

        public static lucky_turntable_reward GetTurntableRewardConfig(int id)
        {
            if (XMLManager.lucky_turntable_reward.ContainsKey(id))
            {
                return XMLManager.lucky_turntable_reward[id];
            }
            return null;
        }

        public static bool IsTurntableInLevelLimit(int turntableId)
        {
            lucky_turntable info = GetTurntableInfo(turntableId);
            List<string> dataList = data_parse_helper.ParseListString(info.__level);
            int minLv = int.Parse(dataList[0]);
            int maxLv = int.Parse(dataList[1]);
            int curLv = PlayerAvatar.Player.level;
            if (curLv >= minLv && curLv <= maxLv)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public static int GetTurntableRewardDetailByTag(Dictionary<string, string[]> rewardDict, int rewardIndex)
        //{

        //}
    }
}
