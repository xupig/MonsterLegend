﻿using Common.Structs;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class fashion_helper
    {
        /// <summary>
        /// 获取时装配置
        /// </summary>
        /// <param name="fashionId"></param>
        /// <returns></returns>
        public static fashion GetFashionCfg(int fashionId)
        {
            if (XMLManager.fashion.ContainsKey(fashionId))
            {
                return XMLManager.fashion[fashionId];
            }
            return null;
        }

        public static int getId(fashion data)
        {
            return data.__id;
        }

        public static string GetName(fashion data)
        {
            return MogoLanguageUtil.GetContent(data.__name);
        }

        public static string GetDiscription(fashion data)
        {
            return MogoLanguageUtil.GetContent(data.__dsc);
        }

        public static int GetIconId(int fashionId, int vocation)
        {
            fashion fashion = GetFashionCfg(fashionId);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(fashion.__icon);
            return int.Parse(dict[vocation.ToString()]);
        }

        public static int GetSource(fashion data)
        {
            return data.__source;
        }

        //public static int GetTexture(fashion data, Vocation vocation)
        //{
        //    int vocationId = (int)vocation;
        //    string index = vocationId.ToString();
        //    return int.Parse(data.__texture[index]);
        //}

        public static int GetDisplayEffect(fashion data)
        {
            return data.__display_effect;
        }

        public static int GetEffectId(fashion data, Vocation vocation)
        {
            int vocationIndex = (int)vocation;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(data.__effect_id);
            if (dict.ContainsKey(vocationIndex.ToString()))
            {
                return int.Parse(dict[vocationIndex.ToString()]);
            }
            return 0;
        }

        public static int GetClothId(fashion data, Vocation vocation)
        {
            int vocationIndex = (int)vocation;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(data.__cloth);
            if (dict.ContainsKey(vocationIndex.ToString()))
            {
                return int.Parse(dict[vocationIndex.ToString()]);
            }
            return 0;
        }

        public static int GetWeaponId(fashion data, Vocation vocation)
        {
            int vocationIndex = (int)vocation;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(data.__weapon);
            if (dict.ContainsKey(vocationIndex.ToString()))
            {
                return int.Parse(dict[vocationIndex.ToString()]);
            }
            return 0;
        }
    }
}
