﻿using Common.Data;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
namespace GameData
{
    public class ui_audio_helper
    {
        public static string GetAudioAssetPath(string goPath)
        {
            GameDataTable<int, ui_audio> audioDict = XMLManager.ui_audio;
            foreach(int id in audioDict.Keys)
            {
                ui_audio audio = audioDict[id];
                if(audio.__path == goPath)
                {
                    return audio.__audio;
                }
            }
            return string.Empty;
        }
    }
}
