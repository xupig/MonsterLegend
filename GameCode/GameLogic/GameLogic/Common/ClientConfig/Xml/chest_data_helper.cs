﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using UnityEngine;

namespace GameData
{
    public enum TreasureBoxType
    {
        NONE = 0,
        Chest = 1,
        DuelChest = 2,
    }

    public class chest_data_helper
    {
        public static chest_data GetConfig(int id)
        {
            if (!XMLManager.chest_data.ContainsKey(id))
            {
                throw new Exception("配置表 chest_data.xml 未包含项 " + id.ToString());
            }
            return XMLManager.chest_data[id];
        }

        public static List<int> GetChestRewardIdList(TreasureBoxType type)
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, chest_data> kvp in XMLManager.chest_data)
            {
                if (GetTreasureTypeByChestId(kvp.Value.__id) == type)
                {
                    List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__show_level);
                    if (int.Parse(dataList[0]) <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= int.Parse(dataList[1]))
                    {
                        result.Add(kvp.Key);
                    }
                }
            }
            result.Sort(SortFun);
            return result;
        }

        public static TreasureBoxType GetTreasureTypeByChestId(int chestId)
        {
            chest_data data = GetConfig(chestId);
            if (1 <= data.__chest_type && data.__chest_type <= 3)
            {
                return TreasureBoxType.Chest;
            }
            else if (4 <= data.__chest_type && data.__chest_type <= 6)
            {
                return TreasureBoxType.DuelChest;
            }
            return TreasureBoxType.NONE;
        }

        public static TreasureBoxType GetTreasureTypeByActivityId(int activityId)
        {
            if (activityId == 18)
            {
                return TreasureBoxType.Chest;
            }
            else if (activityId == 25)
            {
                return  TreasureBoxType.DuelChest;
            }
            return TreasureBoxType.NONE;
        }

        private static int SortFun(int x, int y)
        {
            return GetChestType(y) - GetChestType(x);
        }

        public static int GetModelID(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                List<int> listInt = data_parse_helper.ParseListInt(cfg.__chest_model);
                return listInt[0];
            }
            return 0;
        }

        public static float GetModelSize(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                List<int> listInt = data_parse_helper.ParseListInt(cfg.__chest_model);
                if (listInt.Count >= 3)
                {
                    return listInt[2];
                }
            }
            return 1;
        }

        public static string GetDropModelPath(int chestId)
        {
            int modelID = GetModelID(chestId);
            if (XMLManager.drop_model.ContainsKey(modelID))
            {
                return XMLManager.drop_model[modelID].__prefab;
            }
            return string.Empty;
        }

        public static int GetModelEffectID(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                List<int> listInt = data_parse_helper.ParseListInt(cfg.__chest_model);
                if (listInt.Count >= 2)
                {
                    return listInt[1];
                }
            }
            return 0;
        }

        public static string GetChestName(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            return MogoLanguageUtil.GetContent(cfg.__name);
        }

        public static string GetDropModelEffectPath(int chestId)
        {
            int modelID = GetModelEffectID(chestId);
            if (XMLManager.drop_model_effect.ContainsKey(modelID))
            {
                return XMLManager.drop_model_effect[modelID].__prefab;
            }
            return string.Empty;
        }

        public static int GetChestRewardId(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                return cfg.__chest_reward;
            }
            return 0;
        }

        public static int GetChestType(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                return cfg.__chest_type;
            }
            return 0;
        }

        public static int GetChestIcon(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                return cfg.__icon;
            }
            return 0;
        }

        public static KeyValuePair<string, string> GetOpenCost(int chestId)
        {
            chest_data cfg = GetConfig(chestId);
            if (cfg != null)
            {
                var cost = data_parse_helper.ParseMap(cfg.__cost);
                foreach (KeyValuePair<string, string> kvp in cost)
                {
                    return kvp;
                }
            }
            return new KeyValuePair<string, string>("0", "0");
        }
    }
}
