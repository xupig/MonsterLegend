﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class guild_score_rank_reward_helper
    {
        public static List<guild_score_rank_reward> GetScoreRankRewardList()
        {
            List<guild_score_rank_reward> rewardList = new List<guild_score_rank_reward>();
            rewardList.AddRange(XMLManager.guild_score_rank_reward.Values);
            return rewardList;
        }

        public static int GetRewardIdByRank(int rank)
        {
            foreach(KeyValuePair<int,guild_score_rank_reward> kvp in XMLManager.guild_score_rank_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__rank);
                if (int.Parse(dataList[0]) <= rank && rank <= int.Parse(dataList[1]))
                {
                    return kvp.Value.__reward;
                }
            }
            return 0;
        }
    }
}
