﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public enum GuildMatchId
    {
        testwar_open = 1,
        draw_date = 2,
        draw_time = 3,
        group_match_data = 4,
        group_match_time = 5,
        final_match_date = 6,
        final_match_time = 7,
        testwar_player_score = 8,
        testwar_guild_score = 9,

        guild_match_player_rank_name = 11,
        guild_match_player_rank_num = 12,
    }

    public class guild_match_helper
    {
        public static guild_match GetConfig(int id)
        {
            if (XMLManager.guild_match.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_match.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_match[id];
        }

        public static guild_match GetConfig(GuildMatchId id)
        {
            if (XMLManager.guild_match.ContainsKey((int)id) == false)
            {
                throw new Exception("配置表guild_match.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_match[(int)id];
        }

        public static string GetGuildMatchParam(GuildMatchId id)
        {
            guild_match config = GetConfig((int)id);
            return config.__value;
        }

        public static int GetBattleStageByIndex(int index)
        {
            if (index == 0 || index == 1)
            {
                return public_config.GUILD_BATTLE_STATE_TESTING;
            }
            else if (index == 2)
            {
                return public_config.GUILD_BATTLE_STATE_DRAWING;
            }
            else if (index == 3 || index == 5)
            {
                return public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            }
            else if (index == 4 || index == 6)
            {
                return public_config.GUILD_BATTLE_STATE_FINAL_MATCHING;
            }
            return public_config.GUILD_BATTLE_STATE_NONE;
        }

        public static List<int> GetOpenDay(int stage)
        {
            List<int> openList = null;
            if (stage == public_config.GUILD_BATTLE_STATE_TESTING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.testwar_open).__value);
            }
            else if (stage == public_config.GUILD_BATTLE_STATE_DRAWING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.draw_date).__value);
            }
            else if (stage == public_config.GUILD_BATTLE_STATE_GROUP_MATCHING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.group_match_data).__value);
            }
            else if (stage == public_config.GUILD_BATTLE_STATE_FINAL_MATCHING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.final_match_date).__value);
            }
            if (openList == null || openList.Count == 0)
            {
                throw new Exception("配置表guild_match.xml 配置开启日期有误 " + stage);
            }
            return openList;
        }

        public static List<int> GetOpenTime(int stage)
        {
            List<int> openList = null;
            if (stage == public_config.GUILD_BATTLE_STATE_DRAWING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.draw_time).__value);
            }
            else if (stage == public_config.GUILD_BATTLE_STATE_GROUP_MATCHING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.group_match_time).__value);
            }
            else if (stage == public_config.GUILD_BATTLE_STATE_FINAL_MATCHING)
            {
                openList = MogoStringUtils.Convert2List(GetConfig((int)GuildMatchId.final_match_time).__value);
            }
            if (openList == null || openList.Count != 2)
            {
                throw new Exception("配置表guild_match.xml 配置开启时间有误 " + stage);
            }
            return openList;
        }

        public static string GetOpenContent(int stage)
        {
            List<int> openDayList = guild_match_helper.GetOpenDay(stage);
            if (openDayList.Count > 1)
            {
                string start = MogoLanguageUtil.GetContent(102514 + openDayList[0]);
                string end = MogoLanguageUtil.GetContent(102514 + openDayList[openDayList.Count - 1]);
                return MogoLanguageUtil.GetContent(111529, start, end);
            }
            else
            {
                string day = MogoLanguageUtil.GetContent(102514 + openDayList[0]);
                List<int> openTimeList = guild_match_helper.GetOpenTime(stage);
                int start = openTimeList[0];
                int end = openTimeList[1];
                return MogoLanguageUtil.GetContent(111530, day, string.Format("{0:00}", start / 100), string.Format("{0:00}", start % 100), string.Format("{0:00}", end / 100), string.Format("{0:00}", end % 100));
            }
        }

        private static Dictionary<int, int> _militaryRankNameDict;
        public static string GetMilitaryRankName(int militaryRank)
        {
            if(_militaryRankNameDict == null)
            {
                _militaryRankNameDict = MogoStringUtils.Convert2Dic(GetConfig(GuildMatchId.guild_match_player_rank_name).__value);
            }
            return MogoLanguageUtil.GetContent(_militaryRankNameDict[militaryRank]);
        }

        public static bool HaveMilitaryRank(int militaryRank)
        {
            if (_militaryRankNameDict == null)
            {
                _militaryRankNameDict = MogoStringUtils.Convert2Dic(GetConfig(GuildMatchId.guild_match_player_rank_name).__value);
            }
            return _militaryRankNameDict.ContainsKey(militaryRank);
        }

        private static Dictionary<int, int> _militaryRankLimitNumDict;
        public static int GetMilitaryRankLimitNum(int militaryRank)
        {
            if (_militaryRankLimitNumDict == null)
            {
                _militaryRankLimitNumDict = MogoStringUtils.Convert2Dic(GetConfig(GuildMatchId.guild_match_player_rank_num).__value);
            }
            return _militaryRankLimitNumDict[militaryRank];
        }

        public static string GetDrawWeekday()
        {
            return MogoLanguageUtil.GetWeekday(int.Parse(GetConfig(GuildMatchId.draw_date).__value));
        }

        private static string ParseOpenTimeList(List<int> openTimeList)
        {
            int start = openTimeList[0];
            string startStr = string.Concat(string.Format("{0:00}:", start / 100), string.Format("{0:00}", start % 100));
            int end = openTimeList[1];
            string endStr = string.Concat(string.Format("{0:00}:", end / 100), string.Format("{0:00}", end % 100));
            return string.Format("{0}-{1}", startStr, endStr);
        }

        public static string GetDrawDuringTime()
        {
            return ParseOpenTimeList(MogoStringUtils.Convert2List(GetConfig(GuildMatchId.draw_time).__value));
        }

        public static string GetFinalMatchWeekday()
        {
            return MogoLanguageUtil.GetWeekday(int.Parse(GetConfig(GuildMatchId.final_match_date).__value));
        }

        public static string GetFinalMatchDuringTime()
        {
            return ParseOpenTimeList(MogoStringUtils.Convert2List(GetConfig(GuildMatchId.final_match_time).__value));
        }

        public static int GetEnterMatchGuildCount(int format)
        {
            if (format == public_config.GUILD_BATTLE_FORMAT_1)
            {
                return 16;
            }
            else
            {
                return 8;
            }
        }
    }
}
