﻿using Common.Data;
using Common.ServerConfig;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameData
{
    public class trademarket_helper
    {
        private static trade_center _tradeCenter;

        static trademarket_helper()
        {
            _tradeCenter = XMLManager.trade_center[1];
        }

        public static void OnReloadData()
        {
            _tradeCenter = XMLManager.trade_center[1];
        }

        public static trade_items GetTradeItemConfig(int itemId)
        {
            if (XMLManager.trade_items.ContainsKey(itemId) == false)
            {
                throw new Exception("配置表 trade_items.xml 未包含项 " + itemId.ToString());
            }
            return XMLManager.trade_items[itemId];
        }

        public static int GetTradeType(int itemId)
        {
            return GetTradeType(GetTradeItemConfig(itemId));
        }

        public static int GetTradeType(trade_items tradeItemConfig)
        {
            return tradeItemConfig.__type;
        }

        public static int GetWantBuyUpLimit()
        {
            List<int> listInt = data_parse_helper.ParseListInt(_tradeCenter.__want_buy_max_cnt);
            return listInt[0];
        }

        public static string GetTradeTypeName(trade_items tradeItemConfig)
        {
            return GetTradeTypeDesc(tradeItemConfig.__type);
        }

        public static List<int> GetUseLevel(trade_items tradeItemConfig)
        {
            return data_parse_helper.ParseListInt(tradeItemConfig.__level);
        }

        public static List<int> GetUseVocation(trade_items tradeItemConfig)
        {
            return data_parse_helper.ParseListInt(tradeItemConfig.__vocation);
        }

        public static List<int> GetUsePosition(trade_items tradeItemConfig)
        {
            return data_parse_helper.ParseListInt(tradeItemConfig.__pos);
        }

        public static string GetTradeTypeDesc(int tradeType)
        {
            int tradeTypeIndex = _tradeCenter.__item_type_order.IndexOf(tradeType.ToString());
            if (tradeTypeIndex != -1)
            {
                return MogoLanguageUtil.GetContent(_tradeCenter.__item_type_desc[tradeTypeIndex]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        /// <summary>
        /// 获取拍卖行指定拍卖物品类型的筛选条件
        /// </summary>
        /// <param name="itemType">拍卖物品的类型</param>
        /// <returns>物品类型的筛选条件</returns>
        public static List<TradeFilterType> GetTradeFilterList(int itemType)
        {
            List<TradeFilterType> result = new List<TradeFilterType>();
            TradeFilterType filterType = TryGetFilterType(itemType, public_config.SEARCH_CND_LEVEL);
            if (filterType != null)
            {
                result.Add(filterType);
            }

            filterType = TryGetFilterType(itemType, public_config.SEARCH_CND_POS);
            if (filterType != null)
            {
                result.Add(filterType);
            }

            filterType = TryGetFilterType(itemType, public_config.SEARCH_CND_VOCATION);
            if (filterType != null)
            {
                result.Add(filterType);
            }

            return result;
        }

        public static List<TradeFilterItem> GetTradeFilterItemList(int itemType, int filterType)
        {
            Dictionary<int, TradeFilterItem> tradeFilterItemDic = new Dictionary<int, TradeFilterItem>();
            if (IsHavaFilterItem(itemType, filterType))
            {
                //特殊增加 “清空选择”
                tradeFilterItemDic[0] = new TradeFilterItem(0);

                foreach (KeyValuePair<int, trade_items> kvp in XMLManager.trade_items)
                {
                    if (kvp.Value.__type == itemType)
                    {
                        switch (filterType)
                        {
                            case public_config.SEARCH_CND_LEVEL:
                                List<int> listInt = data_parse_helper.ParseListInt(kvp.Value.__level);
                                if (listInt.Count > 0)
                                {
                                    for (int i = 0; i < listInt.Count; i++)
                                    {
                                        int value = listInt[i];
                                        if(tradeFilterItemDic.ContainsKey(value) == false)
                                        {
                                            tradeFilterItemDic[value] = new TradeLevelFilterItem(value);
                                        }
                                    }
                                }
                                break;
                            case public_config.SEARCH_CND_VOCATION:
                                List<int> listInt2 = data_parse_helper.ParseListInt(kvp.Value.__vocation);
                                if (listInt2.Count > 0)
                                {
                                    for (int i = 0; i < listInt2.Count; i++)
                                    {
                                        int value = listInt2[i];
                                        if (tradeFilterItemDic.ContainsKey(value) == false)
                                        {
                                            tradeFilterItemDic[value] = new TradeVocationFilterItem(value);
                                        }
                                    }
                                }
                                break;
                            case public_config.SEARCH_CND_POS:
                                List<int> listInt3 = data_parse_helper.ParseListInt(kvp.Value.__pos);
                                if (listInt3.Count > 0)
                                {
                                    for (int i = 0; i < listInt3.Count; i++)
                                    {
                                        int value = listInt3[i];
                                        if (tradeFilterItemDic.ContainsKey(value) == false)
                                        {
                                            tradeFilterItemDic[value] = new TradePositionFilterItem(value);
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
                return tradeFilterItemDic.Values.ToList();
            }

            return null;
        }

        private static TradeFilterType TryGetFilterType(int itemType, int filterType)
        {
            TradeFilterType result = null;
            List<TradeFilterItem> tradeFilterItemList = GetTradeFilterItemList(itemType, filterType);
            if (tradeFilterItemList != null)
            {
                switch (filterType)
                {
                    case public_config.SEARCH_CND_LEVEL:
                        result = new TradeLevelFilterType();
                        break;
                    case public_config.SEARCH_CND_VOCATION:
                        result = new TradeVocationFilterType();
                        break;
                    case public_config.SEARCH_CND_POS:
                        result = new TradePositionFilterType();
                        break;
                }
                result.TradeFilterItemList = tradeFilterItemList;
            }
            return result;
        }

        public static bool IsHavaFilterItem(int itemType, int filterType)
        {
            foreach (KeyValuePair<int, trade_items> kvp in XMLManager.trade_items)
            {
                if (kvp.Value.__type == itemType)
                {
                    switch (filterType)
                    {
                        case public_config.SEARCH_CND_LEVEL:
                            if (!string.IsNullOrEmpty(kvp.Value.__level)) { return true; }
                            break;
                        case public_config.SEARCH_CND_VOCATION:
                            if (!string.IsNullOrEmpty(kvp.Value.__vocation)) { return true; }
                            break;
                        case public_config.SEARCH_CND_POS:
                            if (!string.IsNullOrEmpty(kvp.Value.__pos)) { return true; }
                            break;
                    }
                }
            }

            return false;
        }

        public static int GetPerBuyLimit(trade_items tradeItemConfig)
        {
            if (tradeItemConfig.__per_buy_limit != 0)
            {
               return tradeItemConfig.__per_buy_limit;
            }

            return int.MaxValue;
        }

        public static int PerBuyUpLimit
        {
            get
            {
                return _tradeCenter.__buy_max_cnt_once;
            }
        }

        public static int RefreshInteral
        {
            get
            {
                return _tradeCenter.__refresh_interval;
            }
        }

        public static int StorageFeeRate
        {
            get
            {
                return _tradeCenter.__storage_fee;
            }
        }

        public static List<string> TradeItemTypeList
        {
            get
            {
                return data_parse_helper.ParseListString(_tradeCenter.__item_type_order);
            }
        }

        public static int payMoneyType
        {
            get
            {
                return _tradeCenter.__cost_money_type;
            }
        }

        public static int getMoneyType
        {
            get
            {
                return _tradeCenter.__get_money_type;
            }
        }

        public static int TradeTax
        {
            get
            {
                return _tradeCenter.__trade_tax;
            }
        }

        public static bool ContainsItem(int itemId)
        {
            return XMLManager.trade_items.ContainsKey(itemId);
        }

        public static int GetTradeMoneyIcon(int itemId)
        {
            int marketId = XMLManager.trade_items.ContainsKey(itemId) ? XMLManager.trade_items[itemId].__market_id : 0;
            int moneyType = XMLManager.trade_center.ContainsKey(marketId) ? XMLManager.trade_center[marketId].__cost_money_type : 0;
            return item_helper.GetIcon(moneyType);
        }

        public static int GetTradeMinPrice(int itemId)
        {
            int price = 0;
            List<int> listInt = data_parse_helper.ParseListInt(XMLManager.trade_items[itemId].__price_range);
            if (XMLManager.trade_items.ContainsKey(itemId) && listInt.Count >= 2)
            {
                price = listInt[0];
            }
            return price;
        }

        public static int GetTradeMaxPrice(int itemId)
        {
            int price = 0;
            List<int> listInt = data_parse_helper.ParseListInt(XMLManager.trade_items[itemId].__price_range);
            if (XMLManager.trade_items.ContainsKey(itemId) && listInt.Count >= 2)
            {
                price = listInt[1];
            }
            return price;
        }
    }
}
