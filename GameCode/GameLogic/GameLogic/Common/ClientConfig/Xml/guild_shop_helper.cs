﻿#region 模块信息
/*==========================================
// 文件名：guild_shop_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/26 10:37:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class guild_shop_helper
    {
        private static Dictionary<int, List<guild_shop>> guildShopDict;
        private static Dictionary<int, List<guild_shop>> myGuildShopDict;

        public static guild_shop GetGuildShop(int id)
        {
            if(XMLManager.guild_shop.ContainsKey(id) == true)
            {
                return XMLManager.guild_shop[id];
            }
            return null;
        }

        public static void UpdateMyGuildShop()
        {
           
            InitialGuildShopData();
            if(myGuildShopDict == null)
            {
                myGuildShopDict = new Dictionary<int,List<guild_shop>>();
            }
            else
            {
                foreach(KeyValuePair<int,List<guild_shop>> kvp in myGuildShopDict)
                {
                    if(kvp.Value!=null)
                    {
                        kvp.Value.Clear();
                    }
                }
            }
            List<guild_shop> pageShop = null;
            GuildData guildData = PlayerDataManager.Instance.GuildData;
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            MyGuildData myGuildData = guildData.MyGuildData;
            foreach(KeyValuePair<int,List<guild_shop>> kvp in guildShopDict)
            {
                pageShop = guildShopDict[kvp.Key];
                if (myGuildShopDict.ContainsKey(kvp.Key) == false)
                {
                    myGuildShopDict.Add(kvp.Key, new List<guild_shop>());
                }
                for(int i=0;i<pageShop.Count;i++)
                {
                    if (pageShop[i].__id != 0 && pageShop[i].__is_off == 0 && shopData.IsBuyLimit(Convert.ToUInt32(pageShop[i].__id)) == false)
                    {
                        myGuildShopDict[kvp.Key].Add(pageShop[i]);
                    }
                }
            }
            foreach(KeyValuePair<int,List<guild_shop>> kvp in myGuildShopDict)
            {
                kvp.Value.Sort(SortFunc);
            }
        }

        public static List<guild_shop> GetPraisedShopList()
        {
            HashSet<uint> data = PlayerDataManager.Instance.GuildShopData.MyPraiseData;
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            List<guild_shop> result = new List<guild_shop>();
            foreach(uint id in data)
            {
                guild_shop shop = guild_shop_helper.GetGuildShop(Convert.ToInt32(id));
                if (shop.__id != 0 && shop.__is_off == 0 && shopData.IsBuyLimit(Convert.ToUInt32(shop.__id)) == false)
                {
                    result.Add(shop);
                }
            }
            result.Sort(SortFunc);
            return result;
        }

        public static List<guild_shop> GetRareShopList()
        {
            List<guild_shop> result = new List<guild_shop>();
            /*ari
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            foreach (KeyValuePair<int,guild_shop> kvp in XMLManager.guild_shop)
            {
                guild_shop shop = kvp.Value;
                if (shop.__id != 0 && shop.__is_off == 0 && shop.__cost.Count>0 && shopData.IsBuyLimit(Convert.ToUInt32(shop.__id)) == false)
                {
                    result.Add(shop);
                }
            }
            result.Sort(SortFunc);*/
            return result;
        }

        public static List<guild_shop> GetGuildShopList(int page)
        {
            if (myGuildShopDict!=null && myGuildShopDict.ContainsKey(page))
            {
                return myGuildShopDict[page];
            }
            return new List<guild_shop>();
        }

        private static int SortFunc(guild_shop shop1,guild_shop shop2)
        {
            if(shop1.__best_level != shop2.__best_level)
            {
                if(shop1.__best_level<shop2.__best_level)
                {
                    if(shop2.__best_level<=PlayerAvatar.Player.level)
                    {
                        if(shop1.__best_level == 0)
                        {
                            return -1;
                        }
                        return 1;
                    }
                    else if(shop1.__best_level>PlayerAvatar.Player.level)
                    {
                        return -1;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    if (shop1.__best_level <= PlayerAvatar.Player.level)
                    {
                        if (shop2.__best_level == 0)
                        {
                            return 1;
                        }
                        return -1;
                    }
                    else if (shop2.__best_level > PlayerAvatar.Player.level)
                    {
                        return 1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            else
            {
                if(shop1.__sort_id != shop2.__sort_id)
                {
                    return shop1.__sort_id - shop2.__sort_id;
                }
                else
                {
                    return -1;
                }
            }
        }

        private static void InitialGuildShopData()
        {
            if(guildShopDict == null)
            {
                guildShopDict = new Dictionary<int, List<guild_shop>>();
                foreach(KeyValuePair<int,guild_shop> kvp in XMLManager.guild_shop)
                {
                    if(guildShopDict.ContainsKey(kvp.Value.__page) == false)
                    {
                        guildShopDict.Add(kvp.Value.__page, new List<guild_shop>());
                    }
                    guildShopDict[kvp.Value.__page].Add(kvp.Value);
                }
            }
        }
    }
}
