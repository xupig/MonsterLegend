﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using GameMain.GlobalManager;
using Common.Structs;
using GameMain.Entities;

namespace GameData
{
    /// <summary>
    /// 套装卷轴道具
    /// </summary>
    public class equip_suit_scroll_helper
    {
        private static Dictionary<int, List<int>> _equipToSuitScroll;

        /// <summary>
        /// 通过装备去获取所有可以用于此装备的套装卷轴
        /// </summary>
        /// <param name="equipItemInfo">装备信息</param>
        /// <returns></returns>
        public static List<int> GetSuitScrollByEquip(EquipItemInfo equipItemInfo)
        {
            int VOCATION = 2;
            if (_equipToSuitScroll == null)
            {
                _equipToSuitScroll = new Dictionary<int, List<int>>();
            }
            if (_equipToSuitScroll.ContainsKey(equipItemInfo.Id) == false)
            {
                List<int> suilScrollList = new List<int>();
                _equipToSuitScroll.Add(equipItemInfo.Id, suilScrollList);
            }
            else
            {
                return _equipToSuitScroll[equipItemInfo.Id];
            }
            foreach (KeyValuePair<int, equip_suit_scroll> kvp in XMLManager.equip_suit_scroll)
            {
                equip_suit_scroll suit_scroll = kvp.Value;
                List<string> dataList = data_parse_helper.ParseListString(suit_scroll.__level_range);
                if (suit_scroll.__position.Contains(((int)equipItemInfo.SubType).ToString()) == false)
                {
                    continue;
                }
                else if (int.Parse(dataList[0]) > equipItemInfo.Level || (int.Parse(dataList[1])) < equipItemInfo.Level)
                {
                    continue;
                }
                else if(suit_scroll.__quality.Contains(equipItemInfo.Quality.ToString()) == false)
                {
                    continue;
                }

                ///根据职业进行筛选
                Dictionary<int, int> useLimitDict = item_helper.GetItemUseLimit(kvp.Key);
                if (useLimitDict.ContainsKey(VOCATION) == true && useLimitDict[VOCATION] != (int)PlayerAvatar.Player.vocation)
                {
                    continue;
                }
                _equipToSuitScroll[equipItemInfo.Id].Add(kvp.Key);
            }

            return _equipToSuitScroll[equipItemInfo.Id];
        }

        public static List<int> GetEquipPositionList(int suitScrollId)
        {
            equip_suit_scroll config = GetConfig(suitScrollId);
            return data_parse_helper.ParseListInt(config.__position);
        }

        public static int GetMinEquipLevel(int suitScrollId)
        {
            equip_suit_scroll config = GetConfig(suitScrollId);
            return Convert.ToInt32(config.__level_range[0]);
        }

        public static int GetMaxEquipLevel(int suitScrollId)
        {
            equip_suit_scroll config = GetConfig(suitScrollId);
            return Convert.ToInt32(config.__level_range[1]);
        }

        public static int GetMinEquipQuality(int suitScrollId)
        {
            equip_suit_scroll config = GetConfig(suitScrollId);
            return Convert.ToInt32(config.__quality[0]);
        }

        public static int GetSuitId(int suitScrollId)
        {
            //item_common.xml use_effect字段
            item_commom item = item_helper.GetItemConfig(suitScrollId);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(item.__use_effect);
            return Convert.ToInt32(dict["5"]);
        }

        public static equip_suit_scroll GetConfig(int suitScrollId)
        {
            if(XMLManager.equip_suit_scroll.ContainsKey(suitScrollId) == false)
            {
                throw new Exception("equip_suit_scroll.xml 未找到配置项 " + suitScrollId);
            }
            return XMLManager.equip_suit_scroll[suitScrollId];
        }
    }
}
