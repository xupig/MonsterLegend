﻿#region 模块信息
/*==========================================
// 文件名：npc_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:17:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class npc_helper
    {

        public static npc GetNpc(int npcId)
        {
            if(XMLManager.npc.ContainsKey(npcId))
            {
                return XMLManager.npc[npcId];
            }
            return null;
        }

        public static string GetNpcName(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if(_npc!=null)
            {
                return MogoLanguageUtil.GetContent(_npc.__name);
            }
            return string.Empty;
        }

        public static int GetNpcIcon(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if(_npc!=null)
            {
                return _npc.__icon;
            }
            return 0;
        }

        public static int GetNpcHeadIcon(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null)
            {
                return _npc.__head_icon;
            }
            return 0;
        }

        public static int GetNpcModelID(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null)
            {
                return XMLManager.npc[npcId].__model;
            }
            return 0;
        }

        public static int GetNpcModelUIID(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null)
            {
                return XMLManager.npc[npcId].__model_ui;
            }
            return 0;
        }

        public static List<int> GetNpcLocation(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null && XMLManager.npc[npcId].__location!=null)
            {
                return data_parse_helper.ParseListInt(XMLManager.npc[npcId].__location);
            }
            return new List<int>() { 0,0,0};
        }

        public static int GetNpcMap(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null)
            {
                return XMLManager.npc[npcId].__map;
            }
            return 0;
        }

        public static List<int> GetNpcActivityID(int npcId)
        {
            npc _npc = GetNpc(npcId);
            if (_npc != null)
            {
                return data_parse_helper.ParseListInt(XMLManager.npc[npcId].__activity);
            }
            return null;
        }

        public static bool IsActivityNpc(int npcId)
        { 
            var activities = GetNpcActivityID(npcId);
            return activities != null && activities.Count > 0;
        }

        public static float GetNpcScale(int npcId)
        {
            float scale = 1f;
            if (XMLManager.npc.ContainsKey(npcId))
            {
                scale = XMLManager.npc[npcId].__scale;
                scale = scale == 0 ? 1 : scale;
            }
            return scale;
        }

        public static int GetRandomSpeechID(int npcId, int id)
        {
            if (XMLManager.npc.ContainsKey(npcId))
            {
                Dictionary<int, List<int>> speechList = data_parse_helper.ParseDictionaryIntListInt(XMLManager.npc[npcId].__speak);
                if (speechList == null || !speechList.ContainsKey(id) || speechList[id].Count == 0)
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, speechList[id].Count);
                return speechList[id][random];
            }
            return -1;
        }

        public static int GetRandomNPCVoice(int npcId)
        {
            if (XMLManager.npc.ContainsKey(npcId))
            {
                List<int> voiceList = data_parse_helper.ParseListInt(XMLManager.npc[npcId].__dialog_voice);
                if (voiceList == null || voiceList.Count == 0)
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, voiceList.Count);
                return voiceList[random];
            }
            return -1;
        }

        public static int GetRandomDialogID(int npcId)
        {
            if (XMLManager.npc.ContainsKey(npcId))
            {
                List<int> dialogList = data_parse_helper.ParseListInt(XMLManager.npc[npcId].__dialog_txt);
                if (dialogList == null || dialogList.Count == 0 || (dialogList.Count == 1 && dialogList[0] == 0))
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, dialogList.Count);
                return dialogList[random];
            }
            return -1;
        }

        public static string GetNpcFunctionHonor(int npcId)
        {
            if (XMLManager.npc.ContainsKey(npcId))
            {
                npc npc = XMLManager.npc[npcId];
                if (npc.__function_honor != 0)
                {
                    return MogoLanguageUtil.GetContent(npc.__function_honor);
                }
            }
            return string.Empty;
        }
    }
}
