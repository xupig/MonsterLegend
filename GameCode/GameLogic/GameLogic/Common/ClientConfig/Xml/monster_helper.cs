﻿#region 模块信息
/*==========================================
// 文件名：npc_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:17:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class monster_helper
    {
        private static Dictionary<int, Dictionary<int, Dictionary<int, monster_value>>> _monsterValues 
            = new Dictionary<int, Dictionary<int, Dictionary<int, monster_value>>>();
        public static void InitMonsterValue()
        {
            if (_monsterValues.Count > 0) return;
            foreach (KeyValuePair<int, monster_value> pk in XMLManager.monster_value)
            { 
                var data = pk.Value;
                if (!_monsterValues.ContainsKey(data.__inst_id))
                {
                    _monsterValues.Add(data.__inst_id, new Dictionary<int,Dictionary<int,monster_value>>());
                }
                var forInst = _monsterValues[data.__inst_id];
                if (!forInst.ContainsKey(data.__difficulty))
                {
                    forInst.Add(data.__difficulty, new Dictionary<int, monster_value>());
                }
                var forDiff = forInst[data.__difficulty];
                if (!forDiff.ContainsKey(data.__monster_type))
                {
                    forDiff.Add(data.__monster_type, data);
                }
            }
        }

        public static void OnReloadData()
        {
            _monsterValues.Clear();
            InitMonsterValue();
        }

        public static string GetMonsterName(int monsterId)
        {
            monster mon = GetMonster(monsterId);
            if(mon!=null)
            {
                return MogoLanguageUtil.GetContent(mon.__name_id);
            }
            return string.Empty;
        }

        public static string GetMonsterDesc(int monsterId)
        {
            monster mon = GetMonster(monsterId);
            List<int> listInt = data_parse_helper.ParseListInt(mon.__desc);
            if (mon != null && listInt.Count > 0)
            {
                return MogoLanguageUtil.GetContent(listInt[0]);
            }
            return string.Empty;
        }


        public static string GetMonsterEnglishDesc(int monsterId)
        {
            monster mon = GetMonster(monsterId);
            List<int> listInt = data_parse_helper.ParseListInt(mon.__desc);
            if (mon != null && listInt.Count > 1)
            {
                return MogoLanguageUtil.GetContent(listInt[1]);
            }
            return string.Empty;
        }

        public static monster GetMonster(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId];
            }
            return null;
        }

        public static int GetMonsterModel(int monsterId, bool isNormalQuality = true)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return isNormalQuality ? XMLManager.monster[monsterId].__model : XMLManager.monster[monsterId].__model_low;
            }
            return 0;
        }

        public static int GetModelType(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__model_type;
            }
            return 0;
        }

        public static float GetMonsterScale(int monsterId)
        {
            float scale = 1f;
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                scale = XMLManager.monster[monsterId].__scale;
                scale = scale == 0 ? 1 : scale;
            }
            return scale;
        }

        public static int GetMonsterType(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__type;
            }
            return 0;
        }

        public static List<int> GetMonsterSkills(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return data_parse_helper.ParseListInt(XMLManager.monster[monsterId].__skill_ids);
            }
            return null;
        }

        public static List<int> GetMonsterHpShow(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                List<string> list = data_parse_helper.ParseListString(XMLManager.monster[monsterId].__hp_show);
                return data_parse_helper.ParseListInt(list);
            }
            return null;
        }

        public static int GetBornAni(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__born_ani;
            }
            return 0;
        }

        public static int GetDisappearType(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__disappear_type;
            }
            return 0;
        }

        public static AnimatorCullingMode GetCullingMode(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__culling_mode == 1
                    ? AnimatorCullingMode.CullUpdateTransforms : AnimatorCullingMode.AlwaysAnimate;
            }
            return AnimatorCullingMode.AlwaysAnimate;
        }

        public static int GetDisappearTime(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__disappear_time;
            }
            return 1000;
        }

        public static Dictionary<int, List<float>> GetBornBuff(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return data_parse_helper.ParseDictionaryIntListFloat(XMLManager.monster[monsterId].__born_buff);
            }
            return null;
        }

        public static Dictionary<int, List<float>> GetSelfBuff(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return data_parse_helper.ParseDictionaryIntListFloat(XMLManager.monster[monsterId].__self_buff);
            }
            return null;
        }

        public static Dictionary<int, List<float>> GetKillGetBuff(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return data_parse_helper.ParseDictionaryIntListFloat(XMLManager.monster[monsterId].__kill_get_buff);
            }
            return null;
        }

        public static List<int> GetBossPart(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return data_parse_helper.ParseListInt(XMLManager.monster[monsterId].__boss_part);
            }
            return null;
        }

        public static int GetHitShake(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__hit_shake;
            }
            return 0;
        }

        public static int GetShader(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__shader;
            }
            return 0;
        }

        public static int GetRandomSpeechID(int monsterId, int id)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                Dictionary<int, List<int>> speechList = data_parse_helper.ParseDictionaryIntListInt(XMLManager.monster[monsterId].__speak);
                if (speechList == null || !speechList.ContainsKey(id) || speechList[id].Count == 0)
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, speechList[id].Count);
                return speechList[id][random];
            }
            return -1;
        }

        public static int GetMonsterValueID(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                //return XMLManager.monster[monsterId].__monster_value_id;
                return 0;
            }
            return 0;
        }

        public static monster_value GetMonsterValue(int monsterId)
        {
            int valueID = GetMonsterValueID(monsterId);
            if (valueID > 0 && XMLManager.monster_value.ContainsKey(valueID))
            {
                return XMLManager.monster_value[valueID];
            }
            return null;
        }

        public static monster_value GetMonsterValue(int instID, int difficult, int monsterType)
        {
            InitMonsterValue();
            monster_value result = null;
            try
            {
                result = _monsterValues[instID][difficult][monsterType];
            }
            catch 
            {
                result = _monsterValues[0][0][0];
            }
            return result;
        }

        public static List<SkillEventData> GetDieEvents(int monsterId)
        {
            List<SkillEventData> skillEvents = null;
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                skillEvents = CombatDataTools.ParseSkillEvents(XMLManager.monster[monsterId].__die_events);
            }
            return skillEvents;
        }

        public static bool ShowFxForce(int monsterId)
        {
            if (XMLManager.monster.ContainsKey(monsterId))
            {
                return XMLManager.monster[monsterId].__fx_show == 1;
            }
            return false;
        }

    }
}
