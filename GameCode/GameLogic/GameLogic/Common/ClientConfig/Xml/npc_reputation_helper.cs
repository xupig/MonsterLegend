﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class npc_reputation_helper
    {
        private static Dictionary<int, Dictionary<int, npc_reputation>> _npcReputationDic;

        public static npc_reputation GetNpcReputation(int npcId, int npcLevel)
        {
            InitReputationDic();
            if (_npcReputationDic.ContainsKey(npcId) == true
                && _npcReputationDic[npcId].ContainsKey(npcLevel) == true)
            {
                return _npcReputationDic[npcId][npcLevel];
            }
            return null;
        }


        public static Dictionary<int, npc_reputation> GetNpcFavorDic(int npcId)
        {
            InitReputationDic();
            if (_npcReputationDic.ContainsKey(npcId) == true)
            {
                return _npcReputationDic[npcId];
            }
            return null;
        }

        private static void InitReputationDic()
        {
            if (_npcReputationDic == null)
            {
                _npcReputationDic = new Dictionary<int, Dictionary<int, npc_reputation>>();


                GameDataTable<int, npc_reputation> npcReputationDic = XMLManager.npc_reputation;
                foreach (KeyValuePair<int, npc_reputation> kvp in npcReputationDic)
                {
                    if (_npcReputationDic.ContainsKey(kvp.Value.__npc_id) == false)
                    {
                        _npcReputationDic.Add(kvp.Value.__npc_id, new Dictionary<int, npc_reputation>());
                    }
                    _npcReputationDic[kvp.Value.__npc_id][kvp.Value.__level] = kvp.Value;
                }
            }
        }
    }
}
