﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class guild_score_reward_helper
    {
        public static guild_score_reward GetGuildTask(int id)
        {
            if (XMLManager.guild_score_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_score_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_score_reward[id];
        }

        public static List<guild_score_reward> GetGuildScoreRewardList()
        {
            List<guild_score_reward> result = new List<guild_score_reward>();
            foreach (KeyValuePair<int, guild_score_reward> kvp in XMLManager.guild_score_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__player_level_range);
                if (int.Parse(dataList[0]) <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= int.Parse(dataList[1]))
                {
                    result.Add(kvp.Value);
                }
            }
            return result;
        }

        public static bool CanReward()
        {
            foreach (KeyValuePair<int, guild_score_reward> kvp in XMLManager.guild_score_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__player_level_range);
                if (int.Parse(dataList[0]) <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= int.Parse(dataList[1]))
                {
                    if (CanReward(kvp.Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool CanReward(guild_score_reward data)
        {
            if (IsSatisfy(data) && IsRewarded(data) == false)
            {
                return true;
            }
            return false;
        }

        public static bool  IsSatisfy(guild_score_reward data)
        {
            PbGuildTaskScoreInfo scoreInfo = PlayerDataManager.Instance.TaskData.GuildTaskInfo;
            if (scoreInfo == null)
            {
                return false;
            }
            if (scoreInfo.task_num < data.__need_task_num)
            {
                return false;
            }
            if (scoreInfo.task_score < data.__guild_score)
            {
                return false;
            }
            return true;
        }

        public static bool IsRewarded(guild_score_reward data)
        {
            PbGuildTaskScoreInfo scoreInfo = PlayerDataManager.Instance.TaskData.GuildTaskInfo;
            if (scoreInfo == null)
            {
                return false;
            }
            return scoreInfo.reward_data.Contains(data.__group);
        }

    }
}
