﻿using Common.Global;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class tutor_helper
    {
        public static tutor GetConfig(int id)
        {
            if (XMLManager.tutor.ContainsKey(id) == false)
            {
                throw new Exception("tutor.xml 中未找到配置项: " + id);
            }
            return XMLManager.tutor[id];
        }

        public static int GetNeedExpByLevel(int level)
        {
            foreach (KeyValuePair<int, tutor> kvp in XMLManager.tutor)
            {
                if (kvp.Value.__tutor_level == level)
                {
                    return kvp.Value.__tutor_exp;
                }
            }
            return 0;
        }


        public static int GetTotalExpByLevel(int level)
        {
            int total = 0;
            foreach (KeyValuePair<int, tutor> kvp in XMLManager.tutor)
            {
                if (kvp.Value.__tutor_level < level)
                {
                    total += kvp.Value.__tutor_exp;
                }
            }
            return total;
        }

        public static List<int> GetRewardIdList()
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, tutor> kvp in XMLManager.tutor)
            {
                if (kvp.Value.__tutor_level != 0)
                {
                    result.Add( kvp.Value.__tutor_reward_id);
                }
            }
            return result;
        }

        public static bool HasGetReward(int level)
        {
            if (((1 << level) & PlayerAvatar.Player.tutor_reward) != 0)
            {
                return true;
            }
            return false;
        }

        public static bool CanGetReward()
        {
            for (int i = 1; i <= PlayerAvatar.Player.tutor_level; i++)
            {
                if (HasGetReward(i) == false)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool CanBuyTutorShopItem()
        {
            /*ari
            List<token_shop> shopList = token_shop_helper.GetTokenShopData(TokenType.TUTOR);
            for (int i = 0; i < shopList.Count; i++)
            {
                List<token_shop_item> shopItemList = token_shop_helper.GetShopItemData(shopList[i].__id, PlayerAvatar.Player.vocation);
                for (int j = 0; j < shopItemList.Count; j++)
                {
                    TokenItemData shopItemData =new TokenItemData(shopItemList[j]);
                    if (CheckHasActive(shopItemData) && CheckCostLimit(shopItemData))
                    {
                        return true;
                    }
                }
            }*/
            return false;
        }

        /*
        private static bool CheckHasLeftCount(TokenItemData shopItemData)
        {
            return shopItemData.DailyLeftCount > 0 && shopItemData.TotalLeftCount > 0;
        }

        private static bool CheckHasActive(TokenItemData shopItemData)
        {
            return PlayerAvatar.Player.CheckLimit<string>(shopItemData.Active) == LimitEnum.NO_LIMIT;
        }

        private static bool CheckCostLimit(TokenItemData shopItemData)
        {
            return PlayerAvatar.Player.CheckCostLimit(shopItemData.Cost, false) == 0;
        }*/
        
    }
}
