﻿using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_spell_helper
    {
        public static guild_spell GetGuildSpell(int id)
        {
            if (XMLManager.guild_spell.ContainsKey(id))
            {
                return XMLManager.guild_spell[id];
            }
            return null;
        }

        public static int GetName(int skillId)
        {
            guild_spell skillData = GetGuildSpell(skillId);
            if (skillData != null)
            {
                return skillData.__name;
            }
            return 0;
        }

        public static int GetGuildSpellGroup(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return config.__group;
        }

        public static int GetGuildSpellLevel(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return config.__level;
        }

        public static string GetGuildSpellName(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return MogoLanguageUtil.GetContent(config.__name);
        }

        public static int GetGuildSpellIcon(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return config.__icon;
        }

        public static string GetGuildSpellEffect(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return MogoLanguageUtil.GetContent(config.__effects);
        }

        public static string GetGuildSpellEffectByLevel(int level, int group)
        {
            foreach (guild_spell config in XMLManager.guild_spell.Values)
            {
                if (config.__group == group && config.__level == level)
                {
                    return GetGuildSpellEffect(config.__id);
                }
            }
            return string.Empty;
        }

        public static string GetGuildSpellType(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return MogoLanguageUtil.GetContent(config.__type);
        }

        public static Dictionary<string, string> GetGuildSpellConditionDict(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return data_parse_helper.ParseMap(config.__study);
        }

        public static string GetGuildSpellCondition(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return GetConditionContent(data_parse_helper.ParseMap(config.__study));
        }

        public static List<string> GetGuildEffect(int id)
        {
            guild_spell config = GetGuildSpell(id);
            return data_parse_helper.ParseListString(config.__guild_effect);
        }

        public static string GetConditionContent(Dictionary<string,string> dict)
        {
            string result = string.Empty;
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                if (kvp.Key == "1") //公会等级
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Level < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74862), value, " ");
                }
                if (kvp.Key == "2") //公会人数
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.MemberCount < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74863), value, " ");
                }
                if (kvp.Key == "3") //公会繁荣度
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Booming < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74864), value, " ");
                }
                if (kvp.Key == "4") //公会资金
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Money < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74865), value, " ");
                }
                if (kvp.Key == "101") //公会等级
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Level < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74945, value), " ");
                }
                if (kvp.Key == "102") //公会人数
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.MemberCount < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74946, value), " ");
                }
                if (kvp.Key == "103") //公会繁荣度
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Booming < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74947, value), " ");
                }
                if (kvp.Key == "104") //公会个人贡献
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.GetMyContribution() < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74948, value), " ");
                }
                if (kvp.Key == "105") //公会资金
                {
                    string value = kvp.Value;
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Money < int.Parse(value))
                    {
                        value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                    }
                    result += string.Concat(result, MogoLanguageUtil.GetContent(74949, value), " ");
                }
            }
            return result == string.Empty ? MogoLanguageUtil.GetContent(0) : result;
        }

        public static bool IsMeetCondition(Dictionary<string, string> dict)
        {
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                if (kvp.Key == "1") //公会等级
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Level < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "2") //公会人数
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.MemberCount < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "3") //公会繁荣度
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Booming < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "4") //公会资金
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Money < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "101") //公会等级
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Level < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "102") //公会人数
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.MemberCount < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "103") //公会繁荣度
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Booming < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "104") //公会个人贡献
                {
                    if (PlayerDataManager.Instance.GuildData.GetMyContribution() < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
                if (kvp.Key == "105") //公会资金
                {
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Money < int.Parse(kvp.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static int GetNextLevelId(int id)
        {
            guild_spell config = GetGuildSpell(id);
            foreach (guild_spell spell in XMLManager.guild_spell.Values)
            {
                if (spell.__group == config.__group && spell.__level == config.__level + 1)
                {
                    return spell.__id;
                }
            }
            return 0;
        }

    }
}
