﻿#region 模块信息
/*==========================================
// 文件名：instance_reward_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using GameLoader.Utils;

namespace GameData
{
    public class  instance_reward_helper
    {
        public static bool needInit = true;
        public static Dictionary<int, List<int>> instanceIdDict;

        static instance_reward_helper()
        {
            instanceIdDict = new Dictionary<int, List<int>>();
            foreach (instance_reward instanceReward in XMLManager.instance_reward.Values)
            {
                if (instanceIdDict.ContainsKey(instanceReward.__inst_id))
                {
                    instanceIdDict[instanceReward.__inst_id].Add(instanceReward.__id);
                }
                else
                {
                    instanceIdDict.Add(instanceReward.__inst_id, new List<int>() { instanceReward.__id });
                }
            }
        }

        /// <summary>
        /// 获取一条奖励信息的配置item_reward
        /// </summary>
        /// <param name="instanceId"></param>
        /// <param name="difficulty"></param>
        /// <param name="level">s a b</param>
        /// <returns></returns>
        public static List<int> 
            GetItemRewardKey(instance_reward instanceRewardCfg, RewardLevel rewardLevel)
        {
            switch (rewardLevel)
            {
                case RewardLevel.S:
                    return GetRewardList(data_parse_helper.ParseListString(instanceRewardCfg.__reward_s));
                case RewardLevel.A:
                    return GetRewardList(data_parse_helper.ParseListString(instanceRewardCfg.__reward_a));
                case RewardLevel.B:
                    return GetRewardList(data_parse_helper.ParseListString(instanceRewardCfg.__reward_b));
                case RewardLevel.Team:
                    return GetRewardList(data_parse_helper.ParseListString(instanceRewardCfg.__reward_cap_show));
                default:
                    return null;
            }
        }

        private static List<int> GetRewardList(List<string> rewardList)
        {
            List<int> reward = new List<int>();
            for(int i=0;i<rewardList.Count;i++)
            {
                reward.Add(int.Parse(rewardList[i]));
            }
            return reward;
        }

        public static instance_reward GetInstanceReward(int instanceRewardId)
        {
            return XMLManager.instance_reward[instanceRewardId];
        }

        public static List<int> GetAllRewardIdList(int instanceId)
        {
            List<int> rewardIdList = new List<int>();
            int instanceRewardId = GetInstanceRewardId(instanceId);
            if (instanceRewardId != 0)
            {
                instance_reward instanceReward = GetInstanceReward(instanceRewardId);
                rewardIdList.AddRange(GetItemRewardKey(instanceReward, RewardLevel.S));
                rewardIdList.AddRange(GetItemRewardKey(instanceReward, RewardLevel.A));
                rewardIdList.AddRange(GetItemRewardKey(instanceReward, RewardLevel.B));
            }
            return rewardIdList;
        }

        public static List<int> GetRewardIdListByStarNum(int instanceId, int starNum)
        {
            if (starNum == 3)
            {
                return GetRewardIdListByRewardLevel(instanceId, RewardLevel.S);
            }
            else if(starNum == 2)
            {
                return GetRewardIdListByRewardLevel(instanceId, RewardLevel.A);
            }
            return GetRewardIdListByRewardLevel(instanceId, RewardLevel.B);
        }


        public static List<int> GetRewardIdListByRewardLevel(int instanceId, RewardLevel rewardLevel)
        {
            List<int> rewardIdList = new List<int>();
            int instanceRewardId = GetInstanceRewardId(instanceId);
            if (instanceRewardId == 0)
            {
                return rewardIdList;
            }
            instance_reward instanceReward = GetInstanceReward(instanceRewardId);
            rewardIdList.AddRange(GetItemRewardKey(instanceReward, rewardLevel));
            return rewardIdList;
        }

        public static List<int> GetInstanceRewardId(int instanceId, int difficulty)
        {
            List<int> result = new List<int>();
            if (instanceIdDict.ContainsKey(instanceId))
            {
                List<int> instanceRewardIdList = instanceIdDict[instanceId];
                for (int i = 0; i < instanceRewardIdList.Count; i++)
                {
                    instance_reward instanceReward = GetInstanceReward(instanceRewardIdList[i]);
                    if (difficulty == instanceReward.__difficulty)
                    {
                        int index = instanceRewardIdList[i];
                        return GetAllRewardIdListByIndex(index);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 根据关卡奖励ID获取该ID下的所有的奖励列表
        /// </summary>
        /// <param name="index">关卡奖励ID</param>
        /// <returns>对应奖励表中的ID列表</returns>
        private static List<int> GetAllRewardIdListByIndex(int index)
        {
            instance_reward instanceReward = GetInstanceReward(index);
            List<int> result = new List<int>();
            List<string> rewardStringList = new List<string>();
            rewardStringList.AddRange(data_parse_helper.ParseListString(instanceReward.__reward_s));
            rewardStringList.AddRange(data_parse_helper.ParseListString(instanceReward.__reward_a));
            rewardStringList.AddRange(data_parse_helper.ParseListString(instanceReward.__reward_b));
            for (int i = 0; i < rewardStringList.Count; i++)
            {
                result.Add(int.Parse(rewardStringList[i]));
            }
            return result;
        }

        private static int GetInstanceRewardId(int instanceId)
        {
            int difficulty = instance_helper.GetDifficulty(instanceId);
            if (instanceIdDict.ContainsKey(instanceId))
            {
                List<int> instanceRewardIdList = instanceIdDict[instanceId];
                for (int i = 0; i < instanceRewardIdList.Count; i++)
                {
                    instance_reward instanceReward = GetInstanceReward(instanceRewardIdList[i]);
                    if (difficulty == instanceReward.__difficulty)
                    {
                        return instanceRewardIdList[i];
                    }
                }
            }
            else
            {
                LoggerHelper.Error("未找到关卡id为：" + instanceId + "， 难度为:" + difficulty + "的奖励配置");
            }
            return 0;
        }

    }
}
