﻿using Common.Base;
using Common.ServerConfig;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{

    public enum GuildRightId
    {
        examine = 1,
        appoint = 2,
        notice = 3,
        fire = 4,
        recruit = 5,
        activity = 6,
        welfare = 7,
        upgrade = 8,
        spell = 9,
        manifesto = 10,
        guildwarSignUp = 11,
        guildwarDraw = 12,
        guildwarAssignedPosition = 13,
        guild_shop = 14,
    }

    public class guild_rights_helper
    {

        public static guild_rights GetGuildRight(int id)
        {
            if (XMLManager.guild_rights.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_rights.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_rights[id];
        }

        public static int GetGuildRightsPositionById(GuildRightId id)
        {
            guild_rights rights = GetGuildRight((int)id);
            if(rights.__members != "0")
            {
                return public_config.GUILD_POSITION_MEMBER;
            }
            if (rights.__elite != "0")
            {
                return public_config.GUILD_POSITION_ELITE;
            }
            if (rights.__deacon != "0")
            {
                return public_config.GUILD_POSITION_DEACON;
            }
            if (rights.__viceleader != "0")
            {
                return public_config.GUILD_POSITION_VICE_LEADER;
            }
            if (rights.__leader != "0")
            {
                return public_config.GUILD_POSITION_LEADER;
            }
            return public_config.GUILD_POSITION_MEMBER;
        }

        public static bool CheckHasRights(GuildRightId rightId)
        {
            if (PlayerDataManager.Instance.GuildData.GetMyPosition() > guild_rights_helper.GetGuildRightsPositionById(rightId))
            {
                return false;
            }
            return true;
        }

        public static bool CheckHasRights(GuildRightId rightId, int operateId)
        {
            if (PlayerDataManager.Instance.GuildData.GetMyPosition() > guild_rights_helper.GetGuildRightsPositionById(rightId))
            {
                string tip = string.Format(MogoLanguageUtil.GetContent(74620), MogoLanguageUtil.GetContent(guild_rights_helper.GetGuildRightsPositionById(rightId) + 74625), MogoLanguageUtil.GetContent(operateId));
                //ari//ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, tip, PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

    }
}
