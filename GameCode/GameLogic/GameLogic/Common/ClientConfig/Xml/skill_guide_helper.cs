﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class skill_guide_helper
    {
        private static List<int> _beforeGuideList;

        public static bool IsHide(int position)
        {
            if (_beforeGuideList != null)
            {
                return _beforeGuideList.Contains(position);
            }
            return false;
        }

        public static void AddBeforeGuide(int position)
        {
            if (_beforeGuideList == null)
            {
                _beforeGuideList = new List<int>();
            }
            if (_beforeGuideList.Contains(position) == false)
            {
                _beforeGuideList.Add(position);
            }
        }

        public static void RemoveBeforeGuide(int position)
        {
            if (_beforeGuideList != null)
            {
                _beforeGuideList.Remove(position);
            }
        }

        public static void OnEnterMap()
        {
            if (_beforeGuideList != null)
            {
                _beforeGuideList.Clear();
            }
        }
    }
}
