﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 11:34:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using GameLoader.Utils;

namespace GameData
{
    public class DreamlandExploreQuality
    {
        public Dictionary<int, dreamland_explore_quality> QualityDict = new Dictionary<int, dreamland_explore_quality>();

        public void Add(int quality, dreamland_explore_quality qualityConfig)
        {
            QualityDict.Add(quality, qualityConfig);
        }
    }

    public class dreamland_explore_quality_helper
    {
        private static Dictionary<int, DreamlandExploreQuality> _landIdDict = new Dictionary<int, DreamlandExploreQuality>();

        static dreamland_explore_quality_helper()
        {
            foreach (dreamland_explore_quality config in XMLManager.dreamland_explore_quality.Values)
            {
                DreamlandExploreQuality dreamlandExploreQuality = GetDreamlandExploreQuality(config.__land_id);
                dreamlandExploreQuality.Add(config.__quality, config);
            }
        }

        private static DreamlandExploreQuality GetDreamlandExploreQuality(int landId)
        {
            if(_landIdDict.ContainsKey(landId))
            {
                return _landIdDict[landId];
            }
            DreamlandExploreQuality quality = new DreamlandExploreQuality();
            _landIdDict.Add(landId, quality);
            return quality;
        }

        public static dreamland_explore_quality GetConfig(int landId, int quality)
        {
            return _landIdDict[landId].QualityDict[quality];
        }

        public static List<dreamland_explore_quality> GetConfigList(int landId)
        {
            List<dreamland_explore_quality> configList = new List<dreamland_explore_quality>();
            foreach (dreamland_explore_quality config in _landIdDict[landId].QualityDict.Values)
            {
                configList.Add(config);
            }
            configList.Sort(SortByQuality);
            return configList;
        }

        private static int SortByQuality(dreamland_explore_quality x, dreamland_explore_quality y)
        {
            if (x.__quality < y.__quality)
            {
                return -1;
            }
            return 1;
        }

        public static ItemData GetDragonSoulCost(int landId, int quality)
        {
            return GetDragonSoulCost(GetConfig(landId, quality));
        }

        public static ItemData GetDragonSoulCost(dreamland_explore_quality qualityConfig)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(qualityConfig.__buy_items_cost);
            foreach (string key in dataDict.Keys)
            {
                int itemId = int.Parse(key);
                ItemData itemData = new ItemData(itemId);
                itemData.StackCount = int.Parse(dataDict[key]);
                return itemData;
            }
            LoggerHelper.Debug("NO DragonSoulCost Id is:" + qualityConfig.__id);
            return null;
        }

        public static float GetRewardAdditionRate(int landId, int quality)
        {
            return GetConfig(landId, quality).__reward_addition / 10000.0f;
        }
    }
}
