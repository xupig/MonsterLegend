﻿using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public enum PetSkillType
    {
        skill = 1,
        buff = 2,
        fixAttribute = 3,
        relativeLevel = 4,
        relativeQuality = 5,
        relativeStar = 6,
    }

    public class PetSkillValue
    {
        public int Key;
        public List<int> ValueList;

        public PetSkillValue(KeyValuePair<string, string[]> kvp)
        {
            Key = int.Parse(kvp.Key);
            ValueList = new List<int>();
            for (int i = 0; i < kvp.Value.Length; i++)
            {
                ValueList.Add(int.Parse(kvp.Value[i]));
            }
        }
    }

    public class pet_skill_helper
    {
        public static pet_skill GetConfig(int id)
        {
            if(!XMLManager.pet_skill.ContainsKey(id))
            {
                throw new Exception(string.Format("pet_skill 找不到 id:{0}", id));
            }
            return XMLManager.pet_skill[id];
        }

        public static int GetName(int id)
        {
            if (IsActiveSkill(id))
            {
                int skillId = GetActiveSkillId(id);
                return spell_helper.GetName(skillId);
            }
            else if (IsBuff(id))
            {
                int buffId = GetBuffId(id);
                return buff_helper.GetNameId(buffId);
            }
            return GetConfig(id).__name;
        }

        private static int GetDesc(int id)
        {
            return GetConfig(id).__desc;
        }

        public static string GetDescStr(int id)
        {
            if (IsActiveSkill(id))
            {
                int skillId = GetActiveSkillId(id);
                return MogoLanguageUtil.GetContent(spell_helper.GetDesc(skillId));
            }
            List<PetSkillValue> valueList = GetPetSkillValueList(GetConfig(id));
            PetSkillValue firstValue = valueList[0];
            PetSkillValue secondValue = valueList.Count > 1 ? valueList[1] : null;
            if (IsBuff(id))
            {
                int buffId = GetBuffId(id);
                KeyValuePair<int, float> kvp = buff_helper.GetAttriKvp(buffId);
                return MogoLanguageUtil.GetContent(buff_helper.GetDescId(buffId), kvp.Key, kvp.Value);
            }
            int descId = GetDesc(id);
            string content = MogoLanguageUtil.GetContent(descId);
            string descStr = "";
            switch (descId)
            {
                case 76002:
                    descStr = string.Format(content, attri_config_helper.GetAttributeName(firstValue.Key), firstValue.ValueList[0]);
                    break;
                case 76003:
                    descStr = string.Format(content, firstValue.ValueList[0], secondValue.ValueList[0]);
                    break;
                case 76004:
                case 76006:
                case 76008:
                    descStr = string.Format(content, attri_config_helper.GetAttributeName(firstValue.Key), firstValue.ValueList[0], firstValue.ValueList[1]);
                    break;
                case 76005:
                case 76007:
                case 76009:
                    descStr = string.Format(content, firstValue.ValueList[0], secondValue.ValueList[0], 
                        ((float)(firstValue.ValueList[1] + secondValue.ValueList[1]) / 2));
                    break;
            }
            return descStr;
        }

        public static int GetIconId(int id)
        {
            if (IsActiveSkill(id))
            {
                int skillId = GetActiveSkillId(id);
                return spell_helper.GetIconId(skillId);
            }

            else if (IsBuff(id))
            {
                int buffId = GetBuffId(id);
                return buff_helper.GetIcon(buffId);
            }
            return GetConfig(id).__icon;
        }

        public static int GetLevelIconId(int id)
        {
            return GetConfig(id).__level_icon;
        }

        public static int GetQualityIconId(int id)
        {
            return GetConfig(id).__quality_icon;
        }

        public static bool IsActiveSkill(int id)
        {
            pet_skill petSkill = GetConfig(id);
            return (PetSkillType)petSkill.__type == PetSkillType.skill;
        }

        public static bool IsBuff(int id)
        {
            pet_skill petSkill = GetConfig(id);
            return (PetSkillType)petSkill.__type == PetSkillType.buff;
        }

        public static int GetActiveSkillId(int id)
        {
            pet_skill petSkill = GetConfig(id);
            int activeSkillId = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petSkill.__value);
            foreach (string strSkillId in dataDict.Keys)
            {
                activeSkillId = int.Parse(strSkillId);
                break;
            }
            return activeSkillId;
        }

        public static int GetBuffId(int id)
        {
            pet_skill petSkill = GetConfig(id);
            int buffId = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petSkill.__value);
            foreach (string strBuffId in dataDict.Keys)
            {
                buffId = int.Parse(strBuffId);
                break;
            }
            return buffId;
        }

        public static bool IsEffectAttriSkill(int id)
        {
            pet_skill petSkill = GetConfig(id);
            return (((PetSkillType)petSkill.__type == PetSkillType.fixAttribute)||
                ((PetSkillType)petSkill.__type == PetSkillType.relativeLevel)||
                ((PetSkillType)petSkill.__type == PetSkillType.relativeQuality)||
                ((PetSkillType)petSkill.__type == PetSkillType.relativeStar));
        }

        public static List<PetSkillValue> GetPetSkillValueList(pet_skill petSkill)
        {
            List<PetSkillValue> valueList = new List<PetSkillValue>();
            Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList(petSkill.__value);

            foreach (KeyValuePair<string, string[]> kvp in dataDictList)
            {
                valueList.Add(new PetSkillValue(kvp));
            }
            return valueList;
        }

        public static Dictionary<int, float> GetAttriAdditionDict(int id, int level, int star, int quality)
        {
            Dictionary<int, float> dict = new Dictionary<int, float>();
            pet_skill petSkill = GetConfig(id);
            List<PetSkillValue> valueList = GetPetSkillValueList(petSkill);
            for (int i = 0; i < valueList.Count;i++)
            {
                PetSkillValue value = valueList[i];
                switch ((PetSkillType)petSkill.__type)
                {
                    case PetSkillType.fixAttribute:
                        dict.Add(value.Key, (float)value.ValueList[0]);
                        break;
                    case PetSkillType.relativeLevel:
                        dict.Add(value.Key, (float)(value.ValueList[0] * level + value.ValueList[1]));
                        break;
                    case PetSkillType.relativeStar:
                        dict.Add(value.Key, (float)(value.ValueList[0] * star + value.ValueList[1]));
                        break;
                    case PetSkillType.relativeQuality:
                        dict.Add(value.Key, (float)(value.ValueList[0] * quality + value.ValueList[1]));
                        break;
                }
            }
            return dict;
        }

        public static void Init()
        {
            int count = XMLManager.pet_skill.Count;
        }
    }
}
