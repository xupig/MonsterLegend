﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.Structs;
using Common.Utils;

namespace GameData
{
    public class reward_system_helper
    {
        public static readonly int NOT_HAVE_REWARD_FLAG = 0;
        public static readonly int HAVE_REWARD_FLAG = 1;

        public static daily_task GetDailyTaskInfo(int id)
        {
            if (XMLManager.daily_task.ContainsKey(id))
            {
                return XMLManager.daily_task[id];
            }
            return null;
        }

        public static active_task GetActiveTaskInfo(int id)
        {
            if (XMLManager.active_task.ContainsKey(id))
            {
                return XMLManager.active_task[id];
            }
            return null;
        }

        public static active_reward GetActiveRewardInfo(int id)
        {
            if (XMLManager.active_reward.ContainsKey(id))
            {
                return XMLManager.active_reward[id];
            }
            return null;
        }

        public static month_login_reward GetMonthLoginInfo(DateTime curDateTime, int day, int curPlayerLv)
        {
            month_login_reward loginInfo = null;
            GameDataTable<int, month_login_reward> monthLoginDict = XMLManager.month_login_reward;
            //根据当前的年月判断是否符合
            foreach (KeyValuePair<int, month_login_reward> item in monthLoginDict)
            {
                month_login_reward info = item.Value;
                if (info.__day != day)
                {
                    continue;
                }
                bool isFindMonth = false;
                List<string> dataList = data_parse_helper.ParseListString(info.__month);
                for (int i = 0; i < dataList.Count; i++)
                {
                    int month = int.Parse(dataList[i]);
                    if (month == curDateTime.Month)
                    {
                        isFindMonth = true;
                        break;
                    }
                }
                if (!isFindMonth)
                {
                    continue;
                }
                List<string> dataList2 = data_parse_helper.ParseListString(info.__level);
                int minLv = int.Parse(dataList2[0]);
                int maxLv = int.Parse(dataList2[1]);
                if (curPlayerLv >= minLv && curPlayerLv <= maxLv)
                {
                    loginInfo = info;
                    break;
                }
            }
            return loginInfo;
        }

        public static DateTime GetCurDateTime()
        {
            //先强制设置时区
            if (Common.Global.Global.serverTimeZone != -8)
            {
                Common.Global.Global.serverTimeZone = -8;
            }
            long timeStamp = (long)Common.Global.Global.serverTimeStamp / 1000;  //服务器记录的是ms，因此要除以1000
            DateTime curDateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
            return curDateTime;
        }

        public static DateTime GetDateTime(long timeStamp)
        {
            if (Common.Global.Global.serverTimeZone != -8)
            {
                Common.Global.Global.serverTimeZone = -8;
            }
            long time = timeStamp / 1000;
            return MogoTimeUtil.ServerTimeStamp2UtcTime(time);
        }

        public static bool IsOnlineTimeEvent(int eventId)
        {
            bool result = false;
            event_cnds eventCndsInfo = event_cnds_helper.GetEvent(eventId);
            if (eventCndsInfo != null)
            {
                if (eventCndsInfo.__atom_id == (int)EventAtomId.DayOnlineTime)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool IsDailyAccumulationEvent(int eventAtomId)
        {
            if (eventAtomId == (int)EventAtomId.LimitTimeTotalCharge || eventAtomId == (int)EventAtomId.DayLogin)
            {
                return true;
            }
            return false;
        }

        public static string PrintDateTime(DateTime dateTime)
        {
            string str = string.Format(" date time = year = {0} : mon = {1} : day = {2} : hour = {3} : min = {4} : s = {5}", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute,
                dateTime.Second);
            return str;
        }

        public static open_server_login_reward GetOpenServerLoginRewardConfig(int id)
        {
            if (XMLManager.open_server_login_reward.ContainsKey(id))
            {
                return XMLManager.open_server_login_reward[id];
            }
            return null;
        }

        public static bool IsOpenServerLoginConfigContainsTask(int taskId)
        {
            foreach (KeyValuePair<int, open_server_login_reward> item in XMLManager.open_server_login_reward)
            {
                open_server_login_reward config = item.Value;
                if (config.__task_id == taskId)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsAddServerOpenedLoginRewardInfo(open_server_login_reward config)
        {
            if (config.__model_id == 0)
            {
                return false;
            }
            return true;
        }
    }
}
