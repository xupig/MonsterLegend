﻿#region 模块信息
/*==========================================
// 文件名：mfg_skill_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/25 19:37:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class mfg_skill_helper
    {
        private static Dictionary<int, mfg_skill> skillDict;
        private static Dictionary<int,int[]> levelRegionDict;
        private static List<int> stageList;
        private static int maxLevel;

        private static void InitData()
        {
            if (skillDict == null)
            {
                skillDict = new Dictionary<int, mfg_skill>();
                levelRegionDict = new Dictionary<int, int[]>();
                stageList = new List<int>();
                foreach (KeyValuePair<int, mfg_skill> kvp in XMLManager.mfg_skill)
                {
                    if (skillDict.ContainsKey(kvp.Value.__skill_level) == false)
                    {
                        skillDict.Add(kvp.Value.__skill_level, kvp.Value);
                    }
                    if (levelRegionDict.ContainsKey(kvp.Value.__stage) == false)
                    {
                        levelRegionDict.Add(kvp.Value.__stage, new int[] { 1000000, 0 });
                        stageList.Add(kvp.Value.__stage);
                    }
                    maxLevel = Math.Max(kvp.Value.__skill_level,maxLevel);
                    if (levelRegionDict[kvp.Value.__stage][0] > kvp.Value.__skill_level)
                    {
                        levelRegionDict[kvp.Value.__stage][0] = kvp.Value.__skill_level;
                    }
                    if (levelRegionDict[kvp.Value.__stage][1] < kvp.Value.__skill_level)
                    {
                        levelRegionDict[kvp.Value.__stage][1] = kvp.Value.__skill_level;
                    }
                }

            }
        }

        public static mfg_skill GetSkill(int skillLevel)
        {
            InitData();
            if(skillDict.ContainsKey(skillLevel) == false)
            {
                return null;
            }
            return skillDict[skillLevel];
        }

        public static int GetMaxLevel()
        {
            InitData();
            return maxLevel;
        }

        public static List<int> GetStageList()
        {
            InitData();
            return stageList;
        }

        public static int[] GetStageLevelRegion(int stage)
        {
            InitData();
            return levelRegionDict[stage];
        }

    }
}
