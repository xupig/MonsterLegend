﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;


namespace GameData
{
    public enum AttriId
    {
        crit = 12,
        phyDefense = 30,
        magDefense = 31,
        damageUpbound = 160,
        damageLowbound = 161,
        maxHp = 200,
    }

    public class attri_config_helper
    {
        
        public static string GetAttributeName(int attributeId)
        {
            attri_config config = GetAttri(attributeId);
            return MogoLanguageUtil.GetContent(config.__name_id);
        }

        public static string GetAttributeDesc(int attributeId)
        {
            attri_config config = GetAttri(attributeId);
            return MogoLanguageUtil.GetContent(config.__desc_id);
        }

        public static attri_config GetAttri(int attributeId)
        {
            if(XMLManager.attri_config.ContainsKey(attributeId) == false)
            {
                UnityEngine.Debug.LogError("配置表 attri_config.xml 未找到配置项: " + attributeId.ToString());
                return null;
            }
            return XMLManager.attri_config[attributeId];
        }

		public static int GetPriority(int attributeId)
        {
            attri_config config = GetAttri(attributeId);
            return config.__priority;
        }

        public static bool IsFloat(int attributeId)
        {
            attri_config config = GetAttri(attributeId);
            return config.__is_float == 1;
        }

        public static List<string> GetDetailAttributesList()
        {
            List<string> result = new List<string>();
            Dictionary<int, string> nameDict = new Dictionary<int,string>();
            GameDataTable<int, attri_config> attrDict = XMLManager.attri_config;
            foreach(KeyValuePair<int,attri_config> kvp in attrDict)
            {
                if (kvp.Value.__tab == 1)
                {
                     nameDict.Add(kvp.Value.__priority, MogoLanguageUtil.GetContent(kvp.Value.__name_id));
                }
            }
            foreach (var item in nameDict.OrderBy(dict => dict.Key))
            {
                result.Add(item.Value);
            }
            return result;
        }

        public static int GetRecommendStar(int attributeId, Vocation vocation)
        {
            int result = 2;
            attri_config cfg = GetAttri(attributeId);
            List<int> listInt = data_parse_helper.ParseListInt(cfg.__recom_config);
            if (cfg != null && cfg.__recom_config != null && listInt.Count > 0)
            {
                int vocationIndex = listInt.Count >= 3 ? listInt[2] : 0;
                if (vocationIndex == 0 || (int)vocation == vocationIndex)
                {
                    return listInt.Count >= 2 ? listInt[1] : 2;
                }
            }
            return result;
        }

        public static string GetRecommendTxt(int attributeId)
        {
            attri_config cfg = GetAttri(attributeId);
            List<int> listInt = data_parse_helper.ParseListInt(cfg.__recom_config);
            if (cfg != null && cfg.__recom_config != null && listInt.Count > 0)
            {
                return MogoLanguageUtil.GetContent(cfg.__recom_config[0]);
            }
            return string.Empty;
        }
    }
}
