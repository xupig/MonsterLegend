﻿using System;
using System.Collections.Generic;

namespace GameData
{
    public class chat_show_helper
    {
        public static chat_show GetConfig(int id)
        {
            if (XMLManager.chat_show.ContainsKey(id))
            {
                return XMLManager.chat_show[id];
            }
            return null;
        }

        public static bool IsShowFunctionOpen(int id)
        {
            chat_show showInfo = GetConfig(id);
            if (showInfo != null)
            {
                return function_helper.IsFunctionOpen(showInfo.__function_id);
            }
            return false;
        }
    }
}
