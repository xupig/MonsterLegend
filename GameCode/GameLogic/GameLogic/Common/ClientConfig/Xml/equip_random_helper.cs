﻿using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class equip_random_helper
    {
        public static void Init()
        {
            int count = XMLManager.equip_random.Count;
        }

        public static bool ContainsId(int id)
        {
            return XMLManager.equip_random.ContainsKey(id);
        }

        public static int GetExtraMinFightForce(int equipId, bool buffIncluded = true, bool rarityPropertyIncluded = false)
        {
            int extraMinFightForce = 0;
            equip_random cfg = XMLManager.equip_random[equipId];
            if (rarityPropertyIncluded)
            {
                extraMinFightForce += GetExtraMinFightForceByRandomAttribute(cfg);
            }
            if (buffIncluded)
            {
                extraMinFightForce += GetExtraMinFightForceByBuff(cfg);
            }
            return extraMinFightForce;
        }

        public static int GetExtraMaxFightForce(int equipId, bool buffIncluded = true)
        {
            int extraFightForce = 0;
            equip_random cfg = XMLManager.equip_random[equipId];
            extraFightForce += GetExtraMaxFightForceByRandomAttribute(cfg);
            if (buffIncluded)
            {
                extraFightForce += GetExtraMaxFightForceByBuff(cfg);
            }
            return extraFightForce;
        }

        private static int GetExtraMinFightForceByBuff(equip_random cfg)
        {
            int minBuffFightForce = 0;
            if (cfg.__buff_prob >= 10000)
            {
                List<int> buffFightList = GetBuffFightList(cfg);
                minBuffFightForce = buffFightList.Min();
            }
            return minBuffFightForce;
        }

        private static int GetExtraMaxFightForceByBuff(equip_random cfg)
        {
            int maxBuffFightForce = 0;
            if (cfg.__buff_prob > 0)
            {
                List<int> buffFightList = GetBuffFightList(cfg);
                maxBuffFightForce = buffFightList.Max();
            }
            return maxBuffFightForce;
        }

        private static List<int> GetBuffFightList(equip_random cfg)
        {
            List<int> buffFightList = new List<int>();
            if (cfg != null)
            {
                List<string> dataList = data_parse_helper.ParseListString(cfg.__buff_id);
                foreach (string buffId in dataList)
                {
                    buffFightList.Add(buff_helper.GetFightForce(int.Parse(buffId)));
                }
            }
            return buffFightList;

        }

        private static int GetExtraMinFightForceByRandomAttribute(equip_random cfg)
        {
            int result = 0;
            int minRandomAttribute = GetMinRandomAttributeCount(cfg);
            if (minRandomAttribute > 0)
            {
                List<int> attributeValueList = new List<int>();
                int index = 1;
                Dictionary<int, float> dict = new Dictionary<int, float>();
                Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList(cfg.__attri_value);
                List<string> dataList = data_parse_helper.ParseListString(cfg.__attri_type);
                foreach (string i in dataList)
                {
                    dict.Clear();
                    dict.Add(int.Parse(i), float.Parse(dataDictList[index.ToString()][0]));
                    int fightForce = fight_force_helper.GetFightForce(dict, PlayerAvatar.Player.vocation);
                    attributeValueList.Add(fightForce);
                    index++;
                }
                attributeValueList.Sort();
                for (int i = 0; i < minRandomAttribute; i++)
                {
                    result += attributeValueList[i];
                }
            }
            return result;
        }

        private static int GetExtraMaxFightForceByRandomAttribute(equip_random cfg)
        {
            int result = 0;
            int maxRandomAttributeCount = GetMaxRandomAttributeCount(cfg);
            if (maxRandomAttributeCount > 0)
            {
                List<int> attributeValueList = new List<int>();
                int index = 1;
                Dictionary<int, float> dict = new Dictionary<int, float>();
                List<string> list = data_parse_helper.ParseListString(cfg.__attri_type);
                Dictionary<string, string[]> attriValueDict = data_parse_helper.ParseMapList(cfg.__attri_value);
                foreach (string i in list)
                {
                    dict.Clear();
                    dict.Add(int.Parse(i), float.Parse(attriValueDict[index.ToString()][1]));
                    int fightForce = fight_force_helper.GetFightForce(dict,PlayerAvatar.Player.vocation);
                    attributeValueList.Add(fightForce);
                    index++;
                }
                attributeValueList.Sort();
                attributeValueList.Reverse();
                for (int i = 0; i < maxRandomAttributeCount; i++)
                {
                    result += attributeValueList[i];
                }
            }
            return result;
        }

        private static int GetMinRandomAttributeCount(equip_random cfg)
        {
            int minCount = int.MaxValue;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__attri_num);
            foreach (var pair in dict)
            {
                if (int.Parse(pair.Value) > 0 && int.Parse(pair.Key) < minCount)
                {
                    minCount = int.Parse(pair.Key);
                }
            }
            // 防止策划配置了一组数据，0,1,2出现的概率均为0，还是按照0计算
            return minCount != int.MaxValue ? minCount : 0;
        }

        private static int GetMaxRandomAttributeCount(equip_random cfg)
        {
            int maxCount = 0;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__attri_num);
            foreach (var pair in dict)
            {
                if (int.Parse(pair.Value) > 0 && int.Parse(pair.Key) > maxCount)
                {
                    maxCount = int.Parse(pair.Key);
                }
            }
            return maxCount;
        }
    }
}
