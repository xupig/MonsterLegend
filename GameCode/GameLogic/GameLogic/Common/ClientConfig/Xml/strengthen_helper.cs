﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class strengthen_helper
    {
        private static int _maxStrengthenLevel = 0;
        private static Dictionary<int, Dictionary<int, strengthen> > _materialDic; // 装备的类型，要强化的等级，所需材料

        public static strengthen getStrengthenConfigByEquipTypeAndLevel(int equipType, int level)
        {
            if (_materialDic == null)
            {
                InitData();
            }
            if (_materialDic.ContainsKey(equipType) == true && _materialDic[equipType].ContainsKey(level) == true)
            {
                return _materialDic[equipType][level];
            }
            return null;
        }

        public static int MaxStrengthenLevel
        {
            get
            {
                if (_materialDic == null)
                {
                    InitData();
                }
                return _maxStrengthenLevel;
            }
        }

        /// <summary>
        /// 根据强化部位和强化等级获得强化属性
        /// </summary>
        /// <param name="equipType"></param>
        /// <param name="level"></param>
        /// <returns>key表示属性id，value表示该属性的值</returns>
        public static Dictionary<int, int> GetStrengthenAttribute(EquipType equipType, int level)
        {
            if (_materialDic == null)
            {
                InitData();
            }
            Dictionary<int, int> result = new Dictionary<int, int>();
            if (level == 0)
            {
                return result;
            }
            if (_materialDic.ContainsKey((int)equipType) == true && _materialDic[(int)equipType].ContainsKey(level) == true)
            {
                attri_effect effect = attri_effect_helper.GetAttriEffect(_materialDic[(int)equipType][level].__attribute_id);
                List<int> attriIdList = data_parse_helper.ParseListInt(effect.__attri_ids);
                List<int> attriValueList = data_parse_helper.ParseListInt(effect.__attri_values);
                if (attriIdList.Count != attriValueList.Count)
                {
                    throw new Exception(string.Format("特效 {0} 配置项的id和value数量不一致", _materialDic[(int)equipType][level].__attribute_id));
                }
                for (int i = 0; i < attriIdList.Count; i++)
                {
                    result.Add(attriIdList[i], attriValueList[i]);
                }
            }
            return result;
        }

        public static List<BaseItemData> GetStrengthenNeedMaterial(int equipType, int level)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            strengthen strengthen = getStrengthenConfigByEquipTypeAndLevel(equipType, level);
            if (strengthen != null)
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(strengthen.__material);
                List<string> keyList = dict.Keys.ToList();
                foreach (string str in keyList)
                {
                    KeyValuePair<int, int> kvp = new KeyValuePair<int, int>(int.Parse(str), int.Parse(dict[str]));
                    BaseItemData itemData = ItemDataCreator.Create(int.Parse(str));
                    itemData.StackCount = int.Parse(dict[str]);
                    result.Add(itemData);
                }
            }
            return result;
        }

        private static void InitData()
        {
            _materialDic = new Dictionary<int, Dictionary<int, strengthen>>();
            foreach (KeyValuePair<int, strengthen> kvp in XMLManager.strengthen)
            {
                if (_materialDic.ContainsKey(kvp.Value.__position) == false)
                {
                    _materialDic.Add(kvp.Value.__position, new Dictionary<int, strengthen>());
                }
                _materialDic[kvp.Value.__position][kvp.Value.__strengthen_level] = kvp.Value;
                if(kvp.Value.__strengthen_level > _maxStrengthenLevel)
                {
                    _maxStrengthenLevel = kvp.Value.__strengthen_level;
                }
            }
        }

        public static bool CanStrengthenManyTimes(int equipPosition, int strengthenTimes)
        {
            Dictionary<int, EquipStrengthenItemInfo> equipStrengthenDic = PlayerDataManager.Instance.EquipStrengthenData.GetAllEquipStrengthenInfo();
            EquipStrengthenItemInfo strengthenItemInfo = equipStrengthenDic[equipPosition];

            if (strengthenItemInfo.IsCanStrenghthen == false)
            {
                return false;
            }

            if (strengthenItemInfo.StrengthenLevel + strengthenTimes > strengthenItemInfo.StrengthenUpperLimit)
            {
                return false;
            }

            List<BaseItemData> needTotalMaterial = new List<BaseItemData>();

            for (int i = 0; i < strengthenTimes; i++)
            {
                List<BaseItemData> needMaterial = strengthen_helper.GetStrengthenNeedMaterial((int)strengthenItemInfo.EquipItemInfo.SubType, strengthenItemInfo.StrengthenLevel + 1 + i);
                needTotalMaterial = MergedNeedMaterial(needTotalMaterial, needMaterial);
            }

            return PlayerAvatar.Player.CheckCostLimit(needTotalMaterial) == 0;
        }

        private static List<BaseItemData> MergedNeedMaterial(List<BaseItemData> first, List<BaseItemData> second)
        {
            Dictionary<int, BaseItemData> result = new Dictionary<int, BaseItemData>();
            for (int i = 0; i < first.Count; i++)
            {
                result.Add(first[i].Id, first[i]);
            }
            for (int j = 0; j < second.Count; j++)
            {
                if (result.ContainsKey(second[j].Id) == true)
                {
                    result[second[j].Id].StackCount += second[j].StackCount;
                }
                else
                {
                    result.Add(second[j].Id, second[j]);
                }
            }

            return result.Values.ToList();
        }

    }
}
