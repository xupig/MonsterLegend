﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Utils;
using GameMain.Entities;

namespace GameData
{
    public class system_chat_msg_helper
    {
        public static system_chat_msg GetConfig(int systemMsgId)
        {
            if (XMLManager.system_chat_msg.ContainsKey(systemMsgId))
            {
                return XMLManager.system_chat_msg[systemMsgId];
            }
            return null;
        }

        public static bool CanShowSystemMsgCurLv(int systemMsgId)
        {
            system_chat_msg info = GetConfig(systemMsgId);
            List<int> listInt = data_parse_helper.ParseListInt(info.__lvl_limit);
            if (listInt.Count == 0)
                return true;
            int minLv = listInt[0];
            int maxLv = listInt.Count > 1 ? listInt[1] : 0;
            int curLv = PlayerAvatar.Player.level;
            if (maxLv == 0)
            {
                return curLv >= minLv;
            }
            else
            {
                return (curLv >= minLv && curLv <= maxLv);
            }
        }

        public static bool CanShowSystemMsgCurDay(int systemMsgId)
        {
            system_chat_msg info = GetConfig(systemMsgId);
            Dictionary<string,string> dict = data_parse_helper.ParseMap(info.__date);
            if (dict.Count == 0)
                return true;
            int curDayOfWeek = MogoTimeUtil.GetDayOfWeek((long)(Global.serverTimeStamp / 1000));
            return info.__date.Contains(curDayOfWeek.ToString());
        }
    }
}
