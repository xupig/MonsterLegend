﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class activities_open_helper
    {
        public enum Id
        {
            createRole = 1,
        }

        public static activities_open GetConfig(Id id)
        {
            if (!XMLManager.activities_open.ContainsKey((int)id))
            {
                throw new Exception("活动开启配置表 activities_open 找不到Id:" + id);
            }
            return XMLManager.activities_open[(int)id];
        }

        public static int GetCreateRoleDay()
        {
            return GetConfig(Id.createRole).__time;
        }
    }
}
