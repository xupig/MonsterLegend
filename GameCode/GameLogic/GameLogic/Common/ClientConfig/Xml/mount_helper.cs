﻿using System;

namespace GameData
{
    public enum MountType
    {
        NORMAL = 0,
        FLY
    }

    public class mount_helper
    {
        private static float _autoMountDuration = 0;

        //获取自动上马的时长
        public static float GetAutoMountDuration()
        {
            if (_autoMountDuration == 0)
            {
                _autoMountDuration = float.Parse(global_params_helper.GetGlobalParam(GlobalParamId.auto_mount_duration));
            }
            return _autoMountDuration;
        }

        public static mount GetMount(int id)
        {
            if(!XMLManager.mount.ContainsKey(id))
            {
                UnityEngine.Debug.LogError("mount 不存在id:" + id);
                return null;
            }
            return XMLManager.mount[id];
        }

        public static int GetMountIcon(int id)
        {
            mount cfg = GetMount(id);
            if (cfg != null)
            {
                return cfg.__icon;
            }
            return 0;
        }

        public static int GetModelID(byte rideId)
        {
            mount mount = GetMount((int)rideId);
            return mount.__model;
        }

        public static MountType GetMountType(byte rideId)
        {
            mount mount = GetMount((int)rideId);
            return mount == null ? MountType.NORMAL : (MountType)mount.__type;
        }
    }
}
