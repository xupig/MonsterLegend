﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{

    public class guild_match_group_match_helper
    {
        public static guild_battle_group_match_reward GetConfig(int id)
        {
            if (XMLManager.guild_battle_group_match_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_battle_group_match_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_battle_group_match_reward[id];
        }

        public static List<List<guild_battle_group_match_reward>> GetRewardList(int format, int level)
        {
            Dictionary<int, List<guild_battle_group_match_reward>> rewardDict = new Dictionary<int, List<guild_battle_group_match_reward>>();
            foreach (KeyValuePair<int, guild_battle_group_match_reward> kvp in XMLManager.guild_battle_group_match_reward)
            {
                if (kvp.Value.__format != format) { continue; }
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if (int.Parse(dataList[0]) <= level && level <= int.Parse(dataList[1]))
                {
                    if (rewardDict.ContainsKey(kvp.Value.__guild_group_rank) == false)
                    {
                        rewardDict.Add(kvp.Value.__guild_group_rank, new List<guild_battle_group_match_reward>());
                    }
                    rewardDict[kvp.Value.__guild_group_rank].Add(kvp.Value);
                }
            }
            List<List<guild_battle_group_match_reward>> rewardList = rewardDict.Values.ToList<List<guild_battle_group_match_reward>>();
            rewardList.Sort(SortFun);
            return rewardList;
        }

        private static int SortFun(List<guild_battle_group_match_reward> x, List<guild_battle_group_match_reward> y)
        {
            return x[0].__guild_group_rank - y[0].__guild_group_rank;
        }
    }

    public class guild_match_final_match_helper
    {
        public static guild_battle_final_match_reward GetConfig(int id)
        {
            if (XMLManager.guild_battle_final_match_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_battle_final_match_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_battle_final_match_reward[id];
        }

        public static List<List<guild_battle_final_match_reward>> GetRewardList(int format, int level)
        {
            Dictionary<int, List<guild_battle_final_match_reward>> rewardDict = new Dictionary<int, List<guild_battle_final_match_reward>>();
            foreach (KeyValuePair<int, guild_battle_final_match_reward> kvp in XMLManager.guild_battle_final_match_reward)
            {
                if (kvp.Value.__format != format) { continue; }
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if (int.Parse(dataList[0]) <= level && level <= int.Parse(dataList[1]))
                {
                    if (rewardDict.ContainsKey(kvp.Value.__guild_final_rank) == false)
                    {
                        rewardDict.Add(kvp.Value.__guild_final_rank, new List<guild_battle_final_match_reward>());
                    }
                    rewardDict[kvp.Value.__guild_final_rank].Add(kvp.Value);
                }
            }
            List<List<guild_battle_final_match_reward>> rewardList = rewardDict.Values.ToList<List<guild_battle_final_match_reward>>();
            rewardList.Sort(SortFun);
            return rewardList;
        }

        private static int SortFun(List<guild_battle_final_match_reward> x, List<guild_battle_final_match_reward> y)
        {
            return x[0].__guild_final_rank - y[0].__guild_final_rank;
        }
    }

    public class guild_match_friendly_match_helper
    {
        public static guild_battle_friendly_match_reward GetConfig(int id)
        {
            if (XMLManager.guild_battle_friendly_match_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_battle_friendly_match_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_battle_friendly_match_reward[id];
        }

        public static List<List<guild_battle_friendly_match_reward>> GetRewardList(int format, int level)
        {
            Dictionary<int, List<guild_battle_friendly_match_reward>> rewardDict = new Dictionary<int, List<guild_battle_friendly_match_reward>>();
            foreach (KeyValuePair<int, guild_battle_friendly_match_reward> kvp in XMLManager.guild_battle_friendly_match_reward)
            {
                if (kvp.Value.__format != format) { continue; }
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if (int.Parse(dataList[0]) <= level && level <= int.Parse(dataList[1]))
                {
                    if (rewardDict.ContainsKey(kvp.Value.__friend_match_point) == false)
                    {
                        rewardDict.Add(kvp.Value.__friend_match_point, new List<guild_battle_friendly_match_reward>());
                    }
                    rewardDict[kvp.Value.__friend_match_point].Add(kvp.Value);
                }
            }
            List<List<guild_battle_friendly_match_reward>> rewardList = rewardDict.Values.ToList<List<guild_battle_friendly_match_reward>>();
            rewardList.Sort(SortFun);
            return rewardList;
        }

        private static int SortFun(List<guild_battle_friendly_match_reward> x, List<guild_battle_friendly_match_reward> y)
        {
            return y[0].__friend_match_point - x[0].__friend_match_point;
        }
    }
}
