﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using System.Text.RegularExpressions;


namespace GameData
{
    /// <summary>
    /// 对应配置表buff.xml
    /// </summary>
    public class buff_helper
    {
        public static bool CheckBuffId(int buffId)
        {
            if (XMLManager.buff.ContainsKey(buffId) == false)
            {
                UnityEngine.Debug.LogError("@策划  配置表buff.xml未找到配置项: " + buffId.ToString());
            }
            return XMLManager.buff.ContainsKey(buffId);
        }

        public static string GetName(int buffId)
        {
            return MogoLanguageUtil.GetContent(GetConfig(buffId).__name);
        }

        public static int GetNameId(int buffId)
        {
            return GetConfig(buffId).__name;
        }

        public static string GetDesc(int buffId)
        {
            return MogoLanguageUtil.GetContent(GetConfig(buffId).__desc);
        }

        public static int GetDescId(int buffId)
        {
            return GetConfig(buffId).__desc;
        }

        public static int GetIcon(int buffId)
        {
            return GetConfig(buffId).__icon;
        }

        public static buff GetConfig(int buffId)
        {
            if(XMLManager.buff.ContainsKey(buffId) == false)
            {
                throw new Exception("配置表 buff.xml 未找到配置项: " + buffId.ToString() + "使用该方法前先根据buff.helper.CheckBuffId()判断buff.xml是否存在该配置，做好容错处理");
            }
            return XMLManager.buff[buffId];
        }

        public static int GetType(int buffId)
        {
            return GetConfig(buffId).__type;
        }

        public static string GetStartEffect(int buffId)
        {
            return GetConfig(buffId).__start_effect;
        }

        public static KeyValuePair<int, float> GetAttriKvp(int buffId)
        {
            string startEffect = GetStartEffect(buffId);
            MatchCollection matchCollection = Regex.Matches(startEffect, @"[0-9.]+");
            if (matchCollection.Count >= 2)
            {
                return new KeyValuePair<int, float>(int.Parse(matchCollection[0].ToString()), float.Parse(matchCollection[1].ToString()));
            }
            return new KeyValuePair<int,float>();
        }

        public static bool NeedShowInMainUI(int buffId)
        {
            return GetConfig(buffId).__need_show >= 1 && GetConfig(buffId).__need_show <= 3;
        }

        public static int GetFightForce(int buffId)
        {
            if (XMLManager.buff.ContainsKey(buffId))
            {
                return GetConfig(buffId).__fight_force;
            }
            return 0;
        }
    }
}
