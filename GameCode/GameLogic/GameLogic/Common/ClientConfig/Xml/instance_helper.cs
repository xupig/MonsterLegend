﻿#region 模块信息
/*==========================================
// 文件名：instance_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils;
using UnityEngine;
using GameMain.Entities;
using GameMain.GlobalManager;

namespace GameData
{
    public enum InstanceTeamType
    {
        SinglePlayer = 1,
        Team = 2,
        MultiRandom = 3,//多人随机
    }

    public class  instance_helper
    {
        public enum Mode
        {
            normal = 1,
            difficult,
        }

        public const int DreamlandInstanceId = 100002;
        public const int MainTownInstanceId = 1;
        public const int InvalidInstanceId = -1;

        public const int MIN = 0;
        public const int MAX = 1;

        public static Dictionary<int, List<instance>> _instanceGroupDict;

        static instance_helper()
        {
            _instanceGroupDict = new Dictionary<int, List<instance>>();
            foreach (instance instance in XMLManager.instance.Values)
            {
                if ((ChapterType)instance.__type == ChapterType.Copy)
                {
                    int modeGroup = instance.__mode_group;
                    if (modeGroup != 0)
                    {
                        if (!_instanceGroupDict.ContainsKey(modeGroup))
                        {
                            _instanceGroupDict.Add(modeGroup, new List<instance>());
                        }
                        _instanceGroupDict[modeGroup].Add(instance);
                    }
                }
            }
            SortInstanceGroupDict();
        }

        private static void SortInstanceGroupDict()
        {
            foreach (List<instance> list in _instanceGroupDict.Values)
            {
                list.Sort(SortByModeIndex);
            }
        }

        private static int SortByModeIndex(instance x, instance y)
        {
            if (x.__mode_index < y.__mode_index)
            {
                return -1;
            }
            return 1;
        }

        public static Mode GetMode(int id)
        {
            return (Mode)GetInstanceCfg(id).__mode_index;
        }

        public static List<instance> GetGroupInstanceList(int id)
        {
            instance cfg = GetInstanceCfg(id);
            return _instanceGroupDict[cfg.__mode_group];
        }

        public static int GetDifficulty(int id)
        {
            instance cfg = GetInstanceCfg(id);
            return GetDifficulty(cfg);
        }

        public static int GetStatisticType(instance cfg)
        {
            if (cfg != null)
            {
                return cfg.__statistic_type;
            }
            return 0;
        }

        public static int GetDifficulty(instance cfg)
        {
            //当难度修正字段为1时，副本难度为玩家等级
            if (cfg.__reward_difficulty == 1)
            {
                int minLevel = GetMinLevel(cfg);
                return PlayerAvatar.Player.level >= minLevel ? PlayerAvatar.Player.level : minLevel;
            }
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__difficulty);
            if (dict == null)
            {
                return 1;
            }
            else if (dict.ContainsKey("1"))
            {
                return int.Parse(dict["1"]);
            }
            else
            {
                return 1;
            }
        }

        public static ChapterType GetChapterType(int id)
        {
            instance cfg = GetInstanceCfg(id);
            return (ChapterType) cfg.__type;
        }

        /// <summary>
        /// 获得的是下标，不是章节id
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public static int GetChapterIndex(int instanceId)
        {
            int index = ((int)(instanceId / 100f) % 100) - 1;
            return index > 0 ? index : 0 ;
        }

        public static string GetInstanceName(int instanceId)
        {
            instance _instance = GetInstanceCfg(instanceId);
            if (_instance == null) return string.Empty;
            return MogoLanguageUtil.GetContent(_instance.__inst_name);
        }

        public static string GetTeamInstanceName(int instanceId)
        {
            instance _instance = GetInstanceCfg(instanceId);
            if (_instance == null) return string.Empty;
            if (_instance.__type == (int)ChapterType.TeamCopy)
            {
                int index = chapters_helper.GetInstanceIndex(ChapterType.TeamCopy, instanceId);
                string difficulty = MogoLanguageUtil.GetContent(83035 + index);
                return string.Concat(MogoLanguageUtil.GetContent(_instance.__inst_name), "(", difficulty, ")");
            }
            return MogoLanguageUtil.GetContent(_instance.__inst_name);
        }

        public static string GetInstanceCategoryName(int instanceId)
        {
            instance _instance = GetInstanceCfg(instanceId);
            return _instance != null ? MogoLanguageUtil.GetContent(_instance.__inst_name).Substring(0,5) : string.Empty;
        }

        public static instance GetInstanceCfg(int id)
        {
            if (!XMLManager.instance.ContainsKey(id))
            {
                Debug.LogError(string.Format("instance.xml 找不到id:{0}", id));
                return null;
            }
            return XMLManager.instance[id];
        }

        public static bool IsConfigExist(int id)
        {
            return XMLManager.instance.ContainsKey(id);
        }

        public static int GetDailyTimes(int instanceId)
        {
            ChapterType chapterType = chapters_helper.GetChapterType(chapters_helper.GetChapterId(instanceId));
            switch (chapterType)
            {
                case ChapterType.TeamCopy:
                    return GetTeamCopyTotalTimes(instanceId);
            }
            return _getDailyTimes(instanceId);
        }

        private static int _getDailyTimes(int instanceId)
        {
            return GetInstanceCfg(instanceId).__daily_times;
        }
        
        public static int GetDailyLeftTimes(int instanceId)
        {
            ChapterType chapterType = chapters_helper.GetChapterType(chapters_helper.GetChapterId(instanceId));
            switch (chapterType)
            {
                case ChapterType.TeamCopy:
                    return GetTeamCopyLeftTimes(instanceId);
            }
            return _getDailyTimes(instanceId) - PlayerDataManager.Instance.CopyData.GetDailyTimes(instanceId);
        }
        
        public static bool IsClient(instance cfg)
        {
            return cfg.__is_client_ctrl == 1;
        }

        public static int GetMapId(int id)
        {
            return GetMapId(GetInstanceCfg(id));
        }

        public static int GetMapId( instance cfg)
        {
            //随机生成0（包含）~__map_ids.Count（不包含）的随机数
            List<int> listInt = data_parse_helper.ParseListInt(cfg.__map_ids);
            int random = UnityEngine.Random.Range(0, listInt.Count);
            return listInt[random];
        }

        //副本结算行为id获得对应描述
        public static LangEnum GetActionLevelLangEnum(string action)
        {
            LangEnum lang = LangEnum.COPY_SCORE_RULE_TIME;
            switch ( action )
            { 
            case "1":
                lang = LangEnum.COPY_SCORE_RULE_TIME;
                break;
            case "2":
                lang = LangEnum.COPY_SCORE_RULE_KILL_COUNT;
                break;
            case "3":
                lang = LangEnum.COPY_SCORE_RULE_LIFE;
                break;
            case "4":
                lang = LangEnum.COPY_SCORE_RULE_DEAD_COUNT;
                break;
            case "5":
                lang = LangEnum.COPY_SCORE_RULE_NO_PET;
                break;
            }
            return lang;
        }

        public static bool IsLevelOk(instance instance, int level)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__level_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return true;
            }
            if ((level >= GetMinLevel(instance)) && (level <= GetMaxLevel(instance)))
            {
                return true;
            }
            return false;
        }

        public static int GetMinLevel(int id)
        {
            return GetMinLevel(GetInstanceCfg(id));
        }

        public static int GetMinLevel(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__level_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MinValue;
            }
            int level = listInt[MIN];
            return level;
        }

        public static int GetMaxLevel(int id)
        {
            return GetMaxLevel(GetInstanceCfg(id));
        }

        public static int GetMaxLevel(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__level_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MaxValue;
            }
            int level = 0;
            if (listInt.Count > MAX)
            {
                level = listInt[MAX];
            }
            return level == 0 ? int.MaxValue : level;
        }

        public static int GetMinAutoLevel(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__level_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MinValue;
            }
            int level = listInt[MIN];
            return level;
        }

        public static int GetMaxAutoLevel(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__level_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MaxValue;
            }
            int level = 0;
            if (listInt.Count > MAX)
            {
                level = listInt[MAX];
            }
            return level == 0 ? int.MaxValue : level;
        }

        public static int GetMinPlayerNum(int instanceId)
        {
            return GetMinPlayerNum(GetInstanceCfg(instanceId));
        }

        public static int GetMinPlayerNum(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__player_num_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MinValue;
            }
            int num = listInt[MIN];
            return num;
        }

        public static int GetMaxPlayerNum(int instanceId)
        {
            return GetMaxPlayerNum(GetInstanceCfg(instanceId));
        }

        public static int GetMaxPlayerNum(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__player_num_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return int.MaxValue;
            }
            int num = 0;
            if (listInt.Count > MAX)
            {
                num = listInt[MAX];
            }
            return num == 0 ? int.MaxValue : num;
        }

        public static bool IsFightForceOk(instance instance, uint fightValue)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__fa_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return true;
            }
            if ((fightValue >= GetMinFightForce(instance)) && (fightValue <= GetMaxFightForce(instance)))
            {
                return true;
            }
            return false;
        }

        public static int GetRecommendFightForce(instance instance)
        {
            return instance.__recommend_ability;
        }

        public static uint GetMinFightForce(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__fa_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return uint.MinValue;
            }
            if (listInt.Count != 2)
            {
                return uint.MinValue;
            }
            uint level = (uint)listInt[MIN];
            return level;
        }

        public static uint GetMaxFightForce(instance instance)
        {
            List<int> listInt = data_parse_helper.ParseListInt(instance.__fa_limit);
            if (listInt == null || listInt.Count == 0)
            {
                return uint.MaxValue;
            }
            if (listInt.Count != 2)
            {
                return uint.MaxValue;
            }
            uint level = (uint)listInt[MAX];
            return level == 0 ? uint.MaxValue : level;
        }

        public static int GetCostEnergy(int id)
        {
            return GetCostEnergy(GetInstanceCfg(id));
        }

        public static int GetCostEnergy(instance instance)
        {
            string energyStr = public_config.MONEY_TYPE_ENERGY.ToString();
            Dictionary<string, string> dict = data_parse_helper.ParseMap(instance.__cost);
            if (dict.ContainsKey(energyStr))
            {
                return Int32.Parse(dict[energyStr]);
            }
            return 0;
        }

        public static int GetCostGold(instance instance)
        {
            string goldStr = public_config.MONEY_TYPE_GOLD.ToString();
            Dictionary<string, string> dict = data_parse_helper.ParseMap(instance.__cost);
            if (dict.ContainsKey(goldStr))
            {
                return Int32.Parse(dict[goldStr]);
            }
            return 0;
        }

        public static int GetName(int id)
        {
            return GetInstanceCfg(id).__inst_name;
        }

        public static int GetIcon(int id)
        {
            return GetInstanceCfg(id).__icon;
        }

        public static int GetAutoTeamMatchId(int id)
        {
            return GetInstanceCfg(id).__auto_team_match;
        }

        public static InstanceTeamType GetTeamType(int id)
        {
            return (InstanceTeamType)GetTeamType(GetInstanceCfg(id));
        }

        public static int GetTeamType(instance ins)
        {
            return ins.__team_type;
        }

        public static int GetTeamTypeByMapId(int mapId)
        {
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            instance ins = instance_helper.GetInstanceCfg(instId);
            int teamType = instance_helper.GetTeamType(ins);
            return teamType;
        }

        public static int GetTimeLeft(int id)
        {
            instance inst = GetInstanceCfg(id);
            if (inst != null)
            {
                if (inst.__time_win == 0)
                {
                    return inst.__time_lose;
                }
                return inst.__time_win;
            }
            return 0;
        }

        public static int GetReliveNum(int id)
        {
            instance inst = GetInstanceCfg(id);
            if (inst != null)
            {
                return inst.__relive_num;
            }
            return 0;
        }

        public static List<int> GetReliveCdList(int id)
        {
            instance inst = GetInstanceCfg(id);
            if (inst != null)
            {
                return data_parse_helper.ParseListInt(inst.__relive_cd);
            }
            return null;
        }

        public static List<int> GetReliveCostList(int id)
        {
            instance inst = GetInstanceCfg(id);
            if (inst != null)
            {
                return data_parse_helper.ParseListInt(inst.__relive_cost);
            }
            return null;
        }

        public static Dictionary<string, string> GetUnlockConditions(int id)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            Dictionary<string, string[]> dict = data_parse_helper.ParseMapList(XMLManager.instance[id].__unlock_cnds);
            foreach (KeyValuePair<string, string[]> pair in dict)
            {
                result.Add(pair.Key, pair.Value[0]);
            }
            return result;
        }

        public static int GetTeamCopyTotalTimes(int instanceId)
        {
            if (HasInstanceDailyCountLimit(instanceId))
            {
                return _getDailyTimes(instanceId);
            }
            else if (HasChapterDailyCountLimit(instanceId))
            {
                return chapters_helper.GetDailyCount(chapters_helper.GetChapterId(instanceId));
            }
            else
            {
                return inst_type_operate_helper.GetDailyTimes(instance_helper.GetChapterType(instanceId));
            }
        }

        public static int GetTeamCopyLeftTimes(int instanceId)
        {
            int maxCount = 0;
            int remain = int.MaxValue;
            if (HasInstanceDailyCountLimit(instanceId))
            {
                maxCount = _getDailyTimes(instanceId);
                remain = Math.Min(remain, maxCount - PlayerDataManager.Instance.CopyData.GetDailyTimes(instanceId));
            }
            if (HasChapterDailyCountLimit(instanceId))
            {
                maxCount = chapters_helper.GetDailyCount(chapters_helper.GetChapterId(instanceId));
                remain = Math.Min(remain, maxCount - PlayerDataManager.Instance.CopyData.GetDailyTimesByChapterId(chapters_helper.GetChapterId(instanceId)));
            }
            if (HasChapterTypeDailyCountLimit(instanceId))
            {
                maxCount = inst_type_operate_helper.GetDailyTimes(instance_helper.GetChapterType(instanceId));
                remain = Math.Min(remain, maxCount - PlayerDataManager.Instance.CopyData.GetDailyTimesByChapterType(instance_helper.GetChapterType(instanceId)));
            }
            return remain;
        }

        private static bool HasChapterTypeDailyCountLimit(int instanceId)
        {
            return inst_type_operate_helper.GetDailyTimes(instance_helper.GetChapterType(instanceId)) > 0;
        }

        private static bool HasChapterDailyCountLimit(int instanceId)
        {
            return chapters_helper.GetDailyCount(chapters_helper.GetChapterId(instanceId)) > 0;
        }

        private static bool HasInstanceDailyCountLimit(int instanceId)
        {
            return _getDailyTimes(instanceId) > 0;
        }

        public static bool IsInstanceIdInvalid(int instanceId)
        {
            return instanceId <= 0;
        }
    }
}