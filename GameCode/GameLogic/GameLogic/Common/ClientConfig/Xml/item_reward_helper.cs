﻿#region 模块信息
/*==========================================
// 文件名：item_reward_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using GameLoader.Utils;
using GameMain.Entities;
using UnityEngine;
using System.Collections;
using System;

namespace GameData
{
    public class  item_reward_helper
    {
        private static Dictionary<int, Dictionary<int, List<BaseItemData>>> rewardDic = new Dictionary<int, Dictionary<int, List<BaseItemData>>>();

        public static item_reward GetItemReward(int rewardId)
        {
            if(!XMLManager.item_reward.ContainsKey(rewardId))
            {
                throw new Exception("表item_reward 找不到Id:" + rewardId);
            }
            return XMLManager.item_reward[rewardId];
        }

        public static void OnReloadData()
        {
            rewardDic.Clear();
        }

        public static List<RewardData> GetRewardDataList(List<int> rewardIdList, Vocation vocation)
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < rewardIdList.Count; i++)
            {
                int rewardId = rewardIdList[i];
                item_reward itemReward = GetItemReward(rewardId);
                if (itemReward != null)
                {
                    rewardDataList.AddRange(GetRewardDataList(itemReward, vocation));
                }
                else
                {
                    LoggerHelper.Error("Not exist this reward id !!!  id = " + rewardId);
                }
            }
            return rewardDataList;
        }

        public static List<RewardData> GetRewardDataList(int rewardId, Vocation vocation)
        {
            item_reward cfg = GetItemReward(rewardId);
            return GetRewardDataList(cfg, vocation);
        }

        public static List<RewardData> GetRewardDataList(item_reward cfg, Vocation vocation)
        {
            List<BaseItemData> baseItemDataList = GetItemDataList(cfg, vocation);
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                RewardData rewardData = RewardData.GetRewardData(baseItemDataList[i]);
                rewardDataList.Add(rewardData);
            }
            return rewardDataList;
        }

        public static List<BaseItemData> GetItemDataList(int rewardId)
        {
            return GetItemDataList(GetItemReward(rewardId), PlayerAvatar.Player.vocation);
        }

        public static List<BaseItemData> GetItemDataList(item_reward cfg, Vocation vocation)
        {
            int _vocation = (int)vocation;
            if(rewardDic.ContainsKey(cfg.__id) == false)
            {
                rewardDic.Add(cfg.__id, new Dictionary<int, List<BaseItemData>>());
            }
            if(rewardDic[cfg.__id].ContainsKey(_vocation) == false)
            {
                List<string> dataList = data_parse_helper.ParseListString(cfg.__item1);
                rewardDic[cfg.__id].Add(_vocation, new List<BaseItemData>(dataList.Count));
            }
            else
            {
                return InternalGetItemDataList(rewardDic[cfg.__id][_vocation]);
            }
            AppendReward(cfg,_vocation);
            return InternalGetItemDataList(rewardDic[cfg.__id][_vocation]);
        }

        //处理如果存在按等级自适应的特殊道具时的数量
        private static List<BaseItemData> InternalGetItemDataList(List<BaseItemData> srcItemDataList)
        {
            List<BaseItemData> destItemList = new List<BaseItemData>();
            for (int i = 0; i < srcItemDataList.Count; i++)
            {
                BaseItemData itemData = srcItemDataList[i];
                if (IsLvAdaptExpReward(itemData) || IsLvAdaptGoldReward(itemData))
                {
                    BaseItemData data = CreateLvAdaptReward(itemData);
                    if (data != null)
                    {
                        destItemList.Add(data);
                    }
                }
                else
                {
                    destItemList.Add(itemData);
                }
            }
            return destItemList;
        }

        public static Dictionary<int, int> ParseRewardListToDic(List<RewardData> rewardList)
        {
            Dictionary<int, int> itemDic = new Dictionary<int, int>();
            if (rewardList != null)
            {
                for (int i = 0; i < rewardList.Count; i++)
                {
                    int itemId = (int)rewardList[i].id;
                    int itemCount = (int)rewardList[i].num;
                    if (itemDic.ContainsKey(itemId))
                    {
                        itemDic[itemId] = itemDic[itemId] + itemCount;
                    }
                    else
                    {
                        itemDic.Add(itemId, itemCount);
                    }
                }
            }
            return itemDic;
        }

        public static bool IsLvAdaptExpReward(BaseItemData rewardItemData)
        {
            return IsLvAdaptExpReward(rewardItemData.Id);
        }

        public static bool IsLvAdaptGoldReward(BaseItemData rewardItemData)
        {
            return IsLvAdaptGoldReward(rewardItemData.Id);
        }

        public static bool IsLvAdaptExpReward(int itemId)
        {
            return itemId == 60;
        }

        public static bool IsLvAdaptGoldReward(int itemId)
        {
            return itemId == 61;
        }

        public static BaseItemData CreateLvAdaptReward(BaseItemData srcItemData)
        {
            BaseItemData baseItemData = ItemDataCreator.Create(srcItemData.Id);
            int curLv = PlayerAvatar.Player.level;
            avatar_level avatarLvInfo = avatar_level_helper.GetAvatarLevel(curLv);
            if (avatarLvInfo == null)
            {
                return null;
            }
            if (IsLvAdaptGoldReward(baseItemData))  //金币
            {
                baseItemData.StackCount = Mathf.CeilToInt((float)avatarLvInfo.__gold_standard * srcItemData.StackCount / 10000.0f);
            }
            else if (IsLvAdaptExpReward(baseItemData))  //经验
            {
                baseItemData.StackCount = Mathf.CeilToInt((float)avatarLvInfo.__exp_standard * srcItemData.StackCount / 10000.0f);
            }
            return baseItemData;
        }

        public static int ProcessLvAdaptItemId(int itemId)
        {
            int resultId = itemId;
            if (IsLvAdaptExpReward(itemId))
            {
                resultId = 50;
            }
            else if (IsLvAdaptGoldReward(itemId))
            {
                resultId = 1;
            }
            return resultId;
        }

        private static void AppendReward(item_reward cfg,int vocation)
        {
            List<string> itemList = null;
            if (public_config.VOC_WARRIOR == vocation)
            {
                itemList = data_parse_helper.ParseListString(cfg.__item1);
            }
            else if (public_config.VOC_ASSASSIN == vocation)
            {
                itemList = data_parse_helper.ParseListString(cfg.__item2);
            }
            else if (public_config.VOC_WIZARD == vocation)
            {
                itemList = data_parse_helper.ParseListString(cfg.__item3);
            }
            else if (public_config.VOC_ARCHER == vocation)
            {
                itemList = data_parse_helper.ParseListString(cfg.__item4);
            }

            for (int i=0;i<itemList.Count;i++)
            {
                BaseItemData baseItemData = ItemDataCreator.Create(int.Parse(itemList[i]));
                List<string> dataList = data_parse_helper.ParseListString(cfg.__item_value);
                baseItemData.StackCount = int.Parse(dataList[i]);
                rewardDic[cfg.__id][vocation].Add(baseItemData);
            }
        }
    }
}
