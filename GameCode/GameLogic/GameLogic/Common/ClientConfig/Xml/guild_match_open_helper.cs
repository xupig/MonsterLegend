﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_match_open_ui_helper
    {
        public static guild_match_open_ui GetConfig(int id)
        {
            if (XMLManager.guild_match_open_ui.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_match_open_ui.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_match_open_ui[id];
        }

        public static int GetIcon(int id)
        {
            return GetConfig(id).__icon;
        }

        public static string GetDesc(int id)
        {
            return MogoLanguageUtil.GetContent(GetConfig(id).__describe);
        }

        public static string GetButtonText(int id)
        {
            return MogoLanguageUtil.GetContent(GetConfig(id).__button);
        }
    }

    public class guild_match_open_helper
    {
        public static guild_match_open GetConfig(int id)
        {
            if (XMLManager.guild_match_open.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_match_open.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_match_open[id];
        }

        public static List<string> GetOpenDate(int id)
        {
            List<string> openList = data_parse_helper.ParseListString(GetConfig(id).__date);
            if (openList.Count != 2)
            {
                throw new Exception("配置表guild_match_open.xml 配置开启日期有误： " + id);
            }
            return openList;
        }

        public static string GetOpenDateContent(int id)
        {
            if (id == 0) id = 1;
            List<string> openList = guild_match_open_helper.GetOpenDate(id);
            string start = openList[0];
            string end = openList[1];
            return MogoLanguageUtil.GetContent(111501, start.Substring(0, 4), start.Substring(4, 2), start.Substring(6, 2), end.Substring(0, 4), end.Substring(4, 2), end.Substring(6, 2));
        }
    }
}
