﻿#region 模块信息
/*==========================================
// 模块名：wing_helper
// 命名空间: GameData
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/2/10 22:21:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class wing_helper
    {
        public static bool IsIgnoreVip = false;

        public static wing GetWing(int wingId)
        {
            if (XMLManager.wing.ContainsKey(wingId))
            {
                return XMLManager.wing[wingId];
            }
            return null;
        }

        public static int GetWingIcon(int wingId)
        {
            wing cfg = GetWing(wingId);
            if (cfg != null)
            {
                return cfg.__icon;
            }
            return 0;
        }

        public static string GetWingName(int wingId)
        {
            wing cfg = GetWing(wingId);
            if (cfg != null)
            {
                return MogoLanguageUtil.GetContent(cfg.__name);
            }
            return string.Empty;
        }

        public static int GetWingTrainCostFeather(int id, int level)
        {
            wing cfg = GetWing(id);
            if (cfg != null )
            {
                Dictionary<string, string[]> dict = data_parse_helper.ParseMapList(cfg.__train_cost);
                if (dict.ContainsKey(level.ToString()))
                {
                    return int.Parse(dict[level.ToString()][0]);
                }
            }
            return 0;
        }

        public static int GetWingTrainCostNumber(int id, int level)
        {
            wing cfg = GetWing(id);
            if (cfg != null)
            {
                Dictionary<string, string[]> dict = data_parse_helper.ParseMapList(cfg.__train_cost);
                if (dict.ContainsKey(level.ToString()))
                {
                    return int.Parse(dict[level.ToString()][1]);
                }
            }
            return 0;
        }

        public static List<string> GetAttriEffectDesc(int id, int exp)
        {
            wing cfg = GetWing(id);
            if (cfg != null)
            {
                Dictionary<int, List<int>> expEffectDic = data_parse_helper.ParseDictionaryIntListInt(cfg.__exp_effect_id);
                List<int> expEffectList = expEffectDic.Keys.ToList();
                expEffectList.Sort();
                for (int i = 0; i < expEffectList.Count; i++)
                {
                    if (expEffectDic[expEffectList[i]][0] > exp)
                    {
                        int expGap = expEffectDic[expEffectList[i]][1];
                        int startEffectId = expEffectDic[expEffectList[i]][2];
                        int effectId = (expEffectDic[expEffectList[i]][0] - expEffectList[i]) / expGap + startEffectId;
                        return attri_effect_helper.GetAttributeDescList(effectId);
                    }
                }
            }
            return new List<string>();
        }
        

        public static KeyValuePair<int, int> GetWingComposeCostNumber(int id)
        {
            wing cfg = GetWing(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__complex);
            if (cfg != null && dict.Count > 0)
            {
                KeyValuePair<string, string> composeCost = dict.ElementAt(0);
                return new KeyValuePair<int, int>(int.Parse(composeCost.Key), int.Parse(composeCost.Value));
            }
            return new KeyValuePair<int, int>();
        }

        public static KeyValuePair<int, int> GetWingConvert(int id)
        {
            wing cfg = GetWing(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__complex);
            if (cfg != null && dict.Count > 0)
            {
                KeyValuePair<string, string> exchange = dict.ElementAt(0);
                return new KeyValuePair<int, int>(int.Parse(exchange.Key), int.Parse(exchange.Value));
            }
            return new KeyValuePair<int, int>();
        }

        public static int GetWingPerFeatherExp(int id, int featherId)
        {
            wing cfg = GetWing(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__per_exp);
            if (cfg != null && dict.ContainsKey(featherId.ToString()))
            {
                return int.Parse(dict[featherId.ToString()]);
            }
            return 0;
        }

        public static int GetWingPerFeatherDiamond(int id, int featherId)
        {
            wing cfg = GetWing(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__per_diamond);
            if (cfg != null && dict.ContainsKey(featherId.ToString()))
            {
                return int.Parse(dict[featherId.ToString()]);
            }
            return 0;
        }

        public static bool CanUseDiamondReplaceFeather(int id, int featherId)
        {
            wing cfg = GetWing(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__per_diamond);
            return cfg != null && dict.ContainsKey(featherId.ToString());
        }

        public static int GetWingTrainNeedVipLevel(int wingId, int wingLevel)
        {
            wing cfg = GetWing(wingId);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__vip_need);
            return int.Parse(dict[wingLevel.ToString()]);
        }

        public static int GetWingTrainAddValue(int id, int index)
        {
            wing cfg = GetWing(id);
            if (cfg != null)
            {
                List<int> listInt = data_parse_helper.ParseListInt(cfg.__train_value);
                return listInt[index];
            }
            return 0;
        }
    }
}
