﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 14:48:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Utils;
using UnityEngine;

namespace GameData
{
    public class dreamland_explore_date_helper
    {
        private const int SunDay = 7;

        public static dreamland_explore_date GetConfig(int id)
        {
            return XMLManager.dreamland_explore_date[id];
        }

        public static List<int> GetTodayDreamlandIdList()
        {
            List<int> dreamlandIdList = new List<int>();
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(Global.serverTimeStampSecond);
            dreamland_explore_date exploreDate;
            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                exploreDate = GetConfig(SunDay);
            }
            else
            {
                exploreDate = GetConfig(((int) dateTime.DayOfWeek));
            }
            List<string> list = data_parse_helper.ParseListString(exploreDate.__lock_land_id);
            for (int i = 0; i < list.Count; i++)
            {
                dreamlandIdList.Add(int.Parse(list[i]));
            }
            return dreamlandIdList;
        }
    }
}
