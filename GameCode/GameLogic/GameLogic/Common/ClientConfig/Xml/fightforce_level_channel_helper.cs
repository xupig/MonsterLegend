﻿#region 模块信息
/*==========================================
// 文件名：fightforce_level_channel_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 16:06:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
namespace GameData
{
    public class fightforce_level_channel_helper
    {
        public static List<int> GetFunctionIdList(int type)
        {
            List<int> result = new List<int>();
            if (XMLManager.combat_level.ContainsKey(type))
            {
                List<string> stringList = data_parse_helper.ParseListString(XMLManager.combat_level[type].__channel);
                for (int i = 0; i < stringList.Count; i++)
                {
                    result.Add(int.Parse((stringList[i])));
                }
            }
            return result;
        }

        public static List<int> GetDescribeIdList(int type)
        {
            List<int> result = new List<int>();
            if (XMLManager.combat_level.ContainsKey(type))
            {
                List<string> stringList = data_parse_helper.ParseListString(XMLManager.combat_level[type].__describe);
                for (int i = 0; i < stringList.Count; i++)
                {
                    result.Add(int.Parse((stringList[i])));
                }
            }
            return result;
        }

        public static List<int> GetChannelName(int type)
        {
            List<int> result = new List<int>();
            if (XMLManager.combat_level.ContainsKey(type))
            {
                List<string> stringList = data_parse_helper.ParseListString(XMLManager.combat_level[type].__channel_name);
                for (int i = 0; i < stringList.Count; i++)
                {
                    result.Add(int.Parse((stringList[i])));
                }
            }
            return result;
        }

        public static List<int> GetViewIdList(int type)
        {
            List<int> result = new List<int>();
            if (XMLManager.combat_level.ContainsKey(type))
            {
                List<string> stringList = data_parse_helper.ParseListString(XMLManager.combat_level[type].__sys);
                for (int i = 0; i < stringList.Count; i++)
                {
                    result.Add(int.Parse((stringList[i])));
                }
            }
            return result;
        }
    }
}