﻿
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class enchant_helper
    {
        private static Dictionary<int, List<float>> _luckyAdjustInfoDic;
        private static Dictionary<int, List<float>> _levelAdjustInfoDic;
        private static Dictionary<int, List<float>> _qualityAdjustInfoDic;

        public static KeyValuePair<int, List<float>> GetEnchantLuckyAdjustInfo(int luckyValue)
        {
            if (_luckyAdjustInfoDic == null)
            {
                _luckyAdjustInfoDic = MogoStringUtils.Convert2OneToManyFloat(global_params_helper.GetGlobalParam(137));
            }
            foreach (KeyValuePair<int, List<float>> kvp in _luckyAdjustInfoDic)
            {
                if (kvp.Value[0] <= luckyValue && kvp.Value[1] >= luckyValue)
                {
                    return kvp;
                }
            }
            return new KeyValuePair<int,List<float>>(0, new List<float>());
        }



        public static List<float> GetLevelAdjustInfo(int equipLevel)
        {
            if(_levelAdjustInfoDic == null)
            {
                _levelAdjustInfoDic = MogoStringUtils.Convert2OneToManyFloat(global_params_helper.GetGlobalParam(138));
            }

            foreach (KeyValuePair<int, List<float>> kvp in _levelAdjustInfoDic)
            {
                if (kvp.Value[0] <= equipLevel && kvp.Value[1] >= equipLevel)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public static List<float> GetQualityAdjustInfo(int quality)
        {
            if (_qualityAdjustInfoDic == null)
            {
                _qualityAdjustInfoDic = MogoStringUtils.Convert2OneToManyFloat(global_params_helper.GetGlobalParam(139));
            }

            if (_qualityAdjustInfoDic.ContainsKey(quality) == true)
            {
                return _qualityAdjustInfoDic[quality];
            }
            return null;
        }


        public static int ComputeEnchantFightForce(PbEquipEnchantAttriInfo attriInfo)
        {
            int result = 0;

            if (attriInfo != null)
            {
                List<PbEquipEnchantAttri> currentEnchantAttriList = attriInfo.enchant_attri;
                Dictionary<int, float> attributeDic = new Dictionary<int, float>();
                for (int i = 0; i < currentEnchantAttriList.Count; i++)
                {
                    if (attributeDic.ContainsKey((int)currentEnchantAttriList[i].enchant_attri_id) == true)
                    {
                        attributeDic[(int)currentEnchantAttriList[i].enchant_attri_id] += currentEnchantAttriList[i].enchant_attri_value;
                    }
                    else
                    {
                        attributeDic.Add((int)currentEnchantAttriList[i].enchant_attri_id, currentEnchantAttriList[i].enchant_attri_value);
                    }
                }
                result = fight_force_helper.GetFightForce(attributeDic, PlayerAvatar.Player.vocation);
            }

            return result;
        }
    }
}
