﻿#region 模块信息
/*==========================================
// 文件名：map_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Utils;
using GameLoader.Utils;
using Common.ServerConfig;

namespace GameData
{
    public enum MapRewardWay
    {
        No = 0,//没有奖励
        Card = 1,//翻牌奖励
        Box = 2,//宝箱奖励
        WinFail = 3,//胜负奖励
        WinFailRank = 4, //胜负排名奖励
        ScoreRank = 5, //分数段排名奖励
    }

    public enum MapStandardWay
    {
        Star = 1,//星级标准
        Score= 2,//分数标准
        WinFail = 3,//胜负标准
    }

    public class map_helper
    {
        public const int MAX_SCORE = 1;
        public const int DESC = 2;


        private static Dictionary<int, int> _mapID2InstanceID = null;
        private static Dictionary<int, List<int>> _mapNPCList;
        private static Dictionary<int, List<npc>> _mapNpcDict;
        public map_helper( )
        {
        }

        public static Dictionary<string, string[]> GetScoreRule(int mapId)
        {
            map cfg = GetMap(mapId);
            if (cfg != null)
            {
                return data_parse_helper.ParseMapList(cfg.__score_rule);
            }
            return null;
        }

        public static map GetMap(int mapId)
        {
            if(!XMLManager.map.ContainsKey(mapId))
            {
                throw new Exception("map 不存在id:" + mapId);
            }
            return XMLManager.map[mapId];
        }

        public static MapStandardWay GetStandardWay(int mapId)
        {
            map map = GetMap(mapId);
            return (MapStandardWay)map.__standard_way;
        }

        public static int GetPlayerAI(int mapId)
        {
            map map = GetMap(mapId);
            return map.__play_ai;
        }

        public static int GetOneAutoRobotAI(int mapId)
        {
            map map = GetMap(mapId);
            var list = data_parse_helper.ParseListInt(map.__auto_robot_ai);
            if (list.Count == 0)return 0;
            return list[UnityEngine.Random.Range(0, list.Count-1)];
        }

        private static void InitMapID2InstanceID()
        {
            if (_mapID2InstanceID != null)
            {
                return;
            }
            _mapID2InstanceID = new Dictionary<int,int>();
            var en = XMLManager.instance.GetEnumerator() as IEnumerator<KeyValuePair<int, instance>>;
            while (en.MoveNext())
            {
                var instanceID = en.Current.Key;
                var mapIDs = data_parse_helper.ParseListString(en.Current.Value.__map_ids);
                for (int i = 0; i < mapIDs.Count; i++)
                {
                    var mapID = int.Parse(mapIDs[i]);
                    _mapID2InstanceID.Add(mapID, instanceID);
                }
            }
        }

        private static void InitMapNPCList()
        {
            if (_mapNPCList != null)
            {
                return;
            }
            _mapNPCList = new Dictionary<int,List<int>>();
            _mapNpcDict = new Dictionary<int, List<npc>>();
            var en = XMLManager.npc.GetEnumerator() as IEnumerator<KeyValuePair<int, npc>>;
            while (en.MoveNext())
            {
                var npcID = en.Current.Key;
                var mapID = en.Current.Value.__map;
                if (!_mapNPCList.ContainsKey(mapID))
                {
                    _mapNpcDict.Add(mapID, new List<npc>());
                    _mapNPCList.Add(mapID, new List<int>());
                }
                _mapNPCList[mapID].Add(npcID);
                _mapNpcDict[mapID].Add(en.Current.Value);
            }
        }

        public static bool IsWild(int instId)
        {
            return GetMapType(instId) == public_config.MAP_TYPE_WILD;
        }

        public static int GetMapType(int mapID)
        {
            InitMapID2InstanceID();
            var instanceID = _mapID2InstanceID[mapID];
            return XMLManager.instance[instanceID].__type;
        }

        public static int GetInstanceIDByMapID(int mapID)
        {
            InitMapID2InstanceID();
            if (!_mapID2InstanceID.ContainsKey(mapID))
            {
                LoggerHelper.Error(string.Format("GetInstanceIDByMapID失败，mapId = {0}找不到对应的instanceId。", mapID));
                return 0;
            }
            return _mapID2InstanceID[mapID];
        }

        public static string GetSpaceName(int mapID)
        {
            return GetMap(mapID).__space_name;
        }

        public static int GetSynPosSpaceTime(int mapID)
        {
            int synPosSpace = GetMap(mapID).__syn_pos_space;
            if (synPosSpace == 0) synPosSpace = 15;
            return Math.Max(synPosSpace, 3);
        }

        public static bool IsClientCtrl(int mapID)
        {
            InitMapID2InstanceID();
            var instanceID = _mapID2InstanceID[mapID];
            return XMLManager.instance[instanceID].__is_client_ctrl > 0;
        }

        public static List<int> GetNPCListByMapID(int mapID)
        {
            InitMapNPCList();
            if (_mapNPCList.ContainsKey(mapID))
            {
                return _mapNPCList[mapID];
            }
            return null;
        }

        public static List<npc> GetNPCList(int mapID)
        {
            InitMapNPCList();
            if (_mapNpcDict.ContainsKey(mapID))
            {
                return _mapNpcDict[mapID];
            }
            return null;
        }

        public static string GetDesc(int mapId, int actionId)
        {
            map map = GetMap(mapId);
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);
            string[] scorlRuleArray = dicScoreRule[actionId.ToString()];
            string desc = "";
            if((MapStandardWay)map.__standard_way == MapStandardWay.Star)
            {
                desc = MogoLanguageUtil.GetContent(scorlRuleArray[2]);
            }
            else if((MapStandardWay)map.__standard_way == MapStandardWay.Score)
            {
                desc = MogoLanguageUtil.GetContent(scorlRuleArray[3]);
            }
            return desc;
        }

        public static string ParseScoreRuleParam(string value)
        {
            float param = float.Parse(value);
            string strParam = "";
            if (param < 0)
            {
                param = param * -100;
                strParam = param.ToString() + "%";
            }
            else
            {
                strParam = param.ToString();
            }
            return strParam;
        }

        public static List<string> GetQualitySettings(int mapId)
        {
            map map = GetMap(mapId);
            return data_parse_helper.ParseListString(map.__quality_setting);
        }

        public static int GetModelQualitySetting(int mapId)
        {
            map map = GetMap(mapId);
            return map.__model_quality_setting;
        }

        public static int GetUseLockEntityCameraMotion(int mapId)
        {
            map map = GetMap(mapId);
            return map.__use_lock_entity_camera_motion;
        }

        public static bool ShowFullBlood(int mapId)
        {
            map map = GetMap(mapId);
            return map.__is_show_full_blood == 1;
        }


        public static bool CanFly(int mapId)
        {
            map map = GetMap(mapId);
            return map.__is_can_fly == 1;
        }
    }
}
