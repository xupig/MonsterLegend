﻿#region 模块信息
/*==========================================
// 文件名：token_shop_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 14:47:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class token_shop_helper
    {
        /*ari
        private static Dictionary<TokenType,List<token_shop>> _tokenShopDict;
        private static Dictionary<TokenType,token_shop> _mallTokenShopDict;
         */
        private static List<token_shop> _mallTokenShopList;
        private static Dictionary<int, Dictionary<Vocation,List<token_shop_item>>> _shopItemDict;
        private static Dictionary<int, HashSet<Vocation>> _sortSet;
        private static Dictionary<int, int[]> _itemShopIdDict;

        public static List<token_shop> GetMallTokenShopList()
        {
            //ari InitTokenShopData();
            return _mallTokenShopList;
        }

        public static token_shop GetTokenShop(int shopId)
        {
            if(XMLManager.token_shop.ContainsKey(shopId))
            {
                return XMLManager.token_shop[shopId];
            }
            return null;
        }
        
        /*ari
        public static List<token_shop> GetTokenShopData(TokenType tokenType)
        {
            InitTokenShopData();
            if(_tokenShopDict.ContainsKey(tokenType) == false)
            {
                Debug.LogError(string.Format("代币商店缺少{0}的配置",tokenType));
            }
            return _tokenShopDict[tokenType];
        }

        public static bool IsTokenCanExchange(TokenType tokenType)
        {
            List<token_shop> shopList = GetTokenShopData(tokenType);
            Vocation vocation = PlayerAvatar.Player.vocation;
            TokenData tokenData = PlayerDataManager.Instance.TokenData;
            for(int i=0;i<shopList.Count;i++)
            {
                List<token_shop_item> shopItemList = GetShopItemData(shopList[i].__id, vocation);
                for(int j=0;j<shopItemList.Count;j++)
                {
                    token_shop_item item = shopItemList[j];
                    if(PlayerAvatar.Player.CheckCostLimit(item.__cost,false) == 0 &&  tokenData.GetTokenShopItemDailyCount(item.__id) > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }*/

        public static token_shop_item GetTokenShopItem(int itemId)
        {
            if(XMLManager.token_shop_item.ContainsKey(itemId))
            {
                return XMLManager.token_shop_item[itemId];
            }
            return null;
        }

        public static int GetItemDaliyLimit(int itemId)
        {
            token_shop_item item = GetTokenShopItem(itemId);
            if(item!=null)
            {
                return item.__daily_limit;
            }
            return 0;
        }

        public static int GetItemTotalLimit(int itemId)
        {
            token_shop_item item = GetTokenShopItem(itemId);
            if (item != null)
            {
                return item.__total_limit;
            }
            return 0;
        }

        public static void OnReloadData()
        {
            //_tokenShopDict = null;
            _shopItemDict = null;
        }
         /*ari
        private static void InitTokenShopData()
        {
            if(_tokenShopDict == null)
            {
                _tokenShopDict = new Dictionary<TokenType, List<token_shop>>();
                _itemShopIdDict = new Dictionary<int, int[]>();
                _mallTokenShopDict = new Dictionary<TokenType, token_shop>();
                _mallTokenShopList = new List<token_shop>();
                foreach(KeyValuePair<int,token_shop> kvp in XMLManager.token_shop)
                {
                    TokenType tokenType = (TokenType)kvp.Value.__type;
                    if (_tokenShopDict.ContainsKey(tokenType) == false)
                    {
                        _tokenShopDict.Add(tokenType, new List<token_shop>());
                    }
                    if (_itemShopIdDict.ContainsKey(int.Parse(kvp.Value.__token)) == false)
                    {
                        _itemShopIdDict.Add(int.Parse(kvp.Value.__token), new int[] { kvp.Value.__type, kvp.Value.__sub_type });
                    }
                    if(kvp.Value.__show == 1)
                    {
                        if (_mallTokenShopDict.ContainsKey(tokenType) == false)
                        {
                            _mallTokenShopDict.Add(tokenType, kvp.Value);
                            _mallTokenShopList.Add(kvp.Value);
                        }
                       
                    }
                    _tokenShopDict[tokenType].Add(kvp.Value);
                }
                _mallTokenShopList.Sort(SortFunction);
            }
        }*/

        private static int SortFunction(token_shop shop1,token_shop shop2)
        {
            return shop2.__order - shop1.__order;
        }

        public static int[] GetTokenTypeByItemId(int itemId)
        {
            //ari InitTokenShopData();
            if(_itemShopIdDict.ContainsKey(itemId))
            {
                return _itemShopIdDict[itemId];
            }
            return null;
        }

        public static List<token_shop_item> GetShopItemData(int shopId,Vocation vocation)
        {
            InitShopItemData();
            if (_shopItemDict.ContainsKey(shopId) == false)
            {
                _shopItemDict.Add(shopId,new Dictionary<Vocation,List<token_shop_item>>());
            }
            if(_shopItemDict[shopId].ContainsKey(vocation) == false)
            {
                _shopItemDict[shopId].Add(vocation,new List<token_shop_item>());
            }
            if(_shopItemDict[shopId].ContainsKey(vocation) == false)
            {
                _shopItemDict[shopId].Add(vocation, new List<token_shop_item>());
            }
            if (_sortSet.ContainsKey(shopId) == false)
            {
                _sortSet.Add(shopId, new HashSet<Vocation>());
            }
            if(_sortSet[shopId].Contains(vocation) == false)
            {
                _sortSet[shopId].Add(vocation);
                _shopItemDict[shopId][vocation].Sort(SortFunc);
            }
            return _shopItemDict[shopId][vocation];
        }

        private static int SortFunc(token_shop_item item1, token_shop_item item2)
        {
            return item1.__sort_id - item2.__sort_id;
        }

        private static void InitShopItemData()
        {
            if(_shopItemDict == null)
            {
                _shopItemDict = new Dictionary<int, Dictionary<Vocation, List<token_shop_item>>>();
                _sortSet = new Dictionary<int, HashSet<Vocation>>();
                foreach(KeyValuePair<int,token_shop_item> kvp in XMLManager.token_shop_item)
                {
                    if(_shopItemDict.ContainsKey(kvp.Value.__shop_id) == false)
                    {
                        _shopItemDict.Add(kvp.Value.__shop_id,new Dictionary<Vocation,List<token_shop_item>>());
                    }
                    var vocations = data_parse_helper.ParseListInt(kvp.Value.__vocation);
                    for (int i = 0; i < vocations.Count; i++)
                    {
                        Vocation vocation = (Vocation)(vocations[i]);
                        if (_shopItemDict[kvp.Value.__shop_id].ContainsKey(vocation) == false)
                        {
                            _shopItemDict[kvp.Value.__shop_id].Add(vocation,new List<token_shop_item>());
                        }
                        _shopItemDict[kvp.Value.__shop_id][vocation].Add(kvp.Value);
                    }
                }
            }
        }
    }
}
