﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class wild_task_helper
    {
        public static wild_task GetWildTask(int id)
        {
            if (XMLManager.wild_task.ContainsKey(id) == false)
            {
                throw new Exception("配置表wild_task.xml 未找到配置项： " + id);
            }
            return XMLManager.wild_task[id];
        }

        public static wild_task GetWildTask()
        {
            foreach (KeyValuePair<int, wild_task> kvp in XMLManager.wild_task)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__player_level);
                if (int.Parse(dataList[0]) <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= int.Parse(dataList[1]))
                {
                    return kvp.Value;
                }
            }
            throw new Exception("配置表wild_task.xml 未找到对应等级的配置项： " + PlayerAvatar.Player.level);
        }

        public static int GetCurrentWildTaskId()
        {
            Dictionary<int, PbTaskInfo> branchTaskDic = PlayerDataManager.Instance.TaskData.branchTaskDic;
            foreach (KeyValuePair<int, PbTaskInfo> kvp in branchTaskDic)
            {
                if (task_data_helper.GetTaskType(kvp.Value.task_id) == public_config.TASK_TYPE_WILD)
                {
                    return kvp.Value.task_id;
                }
            }
            return 0;
        }

        public static bool HasWildTask()
        {
            return GetCurrentWildTaskId() > 0;
        }

        public static int GetWildTaskIdByTaskId(int taskId)
        {
            foreach (KeyValuePair<int, wild_task> kvp in XMLManager.wild_task)
            {
                if (kvp.Value.__task_id.Contains(taskId.ToString()))
                {
                    return kvp.Value.__id;
                }
            }
            return 0;
        }
    }
}
