﻿#region 模块信息
/*==========================================
// 文件名：gem_spirit_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/28 19:57:30
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
namespace GameData
{
    public class gem_spirit_helper
    {
        private static int MIN_LEVEL = 0;
        private static int MAX_LEVEL = 99;


        public static int GetNormalBlessScore(int spiritLevel)
        {
            if (CheckLevel(spiritLevel) == false)
            {
                return 0;
            }
            else
            {
                List<string> dataList = data_parse_helper.ParseListString(XMLManager.gem_spirit[spiritLevel].__improve);
                return int.Parse(dataList[0]);
            }
        }

        public static int GetLittleHitBlessScore(int spiritLevel)
        {
            if (CheckLevel(spiritLevel) == false)
            {
                return 0;
            }
            else
            {
                List<string> dataList = data_parse_helper.ParseListString(XMLManager.gem_spirit[spiritLevel].__improve);
                return int.Parse(dataList[1]);
            }
        }

        public static int GetBigHitBlessScore(int spiritLevel)
        {
            if (CheckLevel(spiritLevel) == false)
            {
                return 0;
            }
            else
            {
                List<string> dataList = data_parse_helper.ParseListString(XMLManager.gem_spirit[spiritLevel].__improve);
                return int.Parse(dataList[2]);
            }
        }

        private static bool CheckLevel(int spiritLevel)
        {
            if (spiritLevel >= MIN_LEVEL && spiritLevel <= MAX_LEVEL)
            {
                return true;
            }
            return false;
        }

        

    }
}