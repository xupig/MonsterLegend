﻿#region 模块信息
/*==========================================
// 文件名：item_commom_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/28 10:42:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using GameMain.Entities;
using Common.Structs;
using GameMain.GlobalManager;
using Common.Structs.Enum;
using GameLoader.Utils;


namespace GameData
{
    public class ItemUseEffect
    {
        public const string OBTAIN_ITEM = "1";
        public const string OBTAIN_BUFF = "2";
        public const string LEARN_CRAFT = "3";
        public const string OPEN_FUNCTION_PANEL = "4";
        public const string SUIT_SCROLL = "5";
        /*用在要塞建筑，已经被弃用*/
        public const string OBTAIN_BUILDING = "6";
        public const string TOKEN_EXCHANGE = "7";
        public const string OBTAIN_INTIMATE = "8";
        public const string OBTAIN_PET = "9";
        public const string OBTAIN_PET_EXP = "10";
        public const string WING_ID = "11";
        public const string FASION_ID = "12";
        public const string MONTH_CARD = "13";
        public const string SEASON_CARD = "14";
        public const string YEAR_CARD = "15";
        public const string COMSPOSITE_GET_VALUE = "18";
        public const string EFFECTIVE_TIME = "19";
        public const string MOUNT_ID = "20";
        /// <summary>
        /// 用来表示功能ID，用于判断VIEW_ID对应的界面是否开启
        /// 21 与 22 成对出现
        /// </summary>
        public const string FUNCTION_ID = "21";
        public const string VIEW_ID = "22";
    }

    public class ItemUseLimit
    {
        public const string LIMIT_LEVEL = "1";
        public const string LIMIT_VOCATION = "2";
        public const string LIMIT_VIP = "3";
        public const string LIMIT_MANUFACTURE = "4";
        public const string LIMIT_SEX = "5";
        public const string LIMIT_MAP = "14";
        public const string LIMIT_ITEM_COUNT = "16";
    }

    public class ItemUseLimit2
    {
        public const string LIMIT_BUFF = "1";
    }

    public class item_helper
    {
        private static Dictionary<int, int> _typeDescDict;
        private static Dictionary<int, int> _qualityDescDict;
        private static Dictionary<int, int> _vocationDescDict;
        /// <summary>
        /// Key为装备类型，Value为装备类型描述
        /// </summary>
        private static Dictionary<int, int> _equipTypeDescDict;
        private static Dictionary<int, int> _equipPositionDescDict;
        private static Dictionary<int, int> _equipPositionIconDict;

        private const int RARE_QUALITY = 4;

        static item_helper()
        {
            _qualityDescDict = new Dictionary<int, int>();
            _qualityDescDict.Add(public_config.ITEM_QUALITY_WHITE, (int)LangEnum.ITEM_QUALITY_WHITE);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_GREEN, (int)LangEnum.ITEM_QUALITY_GREEN);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_BLUE, (int)LangEnum.ITEM_QUALITY_BLUE);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_PURPLE, (int)LangEnum.ITEM_QUALITY_PURPLE);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_ORANGE, (int)LangEnum.ITEM_QUALITY_ORANGE);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_GOLDEN, (int)LangEnum.ITEM_QUALITY_GOLDEN);
            _qualityDescDict.Add(public_config.ITEM_QUALITY_RED, (int)LangEnum.ITEM_QUALITY_RED);

            _vocationDescDict = new Dictionary<int, int>();
            _vocationDescDict.Add(public_config.VOC_WARRIOR, (int)LangEnum.VOC_WARRIOR);
            _vocationDescDict.Add(public_config.VOC_ASSASSIN, (int)LangEnum.VOC_ASSASSIN);
            _vocationDescDict.Add(public_config.VOC_WIZARD, (int)LangEnum.VOC_WIZARD);
            _vocationDescDict.Add(public_config.VOC_ARCHER, (int)LangEnum.VOC_ARCHER);

            _equipTypeDescDict = new Dictionary<int, int>();
            _equipTypeDescDict.Add(1, 501);  //武器类型
            _equipTypeDescDict.Add(2, 502);  //头盔
            _equipTypeDescDict.Add(3, 503);  //肩甲
            _equipTypeDescDict.Add(4, 504);  //胸甲
            _equipTypeDescDict.Add(5, 505);  //手套
            _equipTypeDescDict.Add(6, 506);  //腰带
            _equipTypeDescDict.Add(7, 507);  //裤子
            _equipTypeDescDict.Add(8, 508);  //鞋子
            _equipTypeDescDict.Add(9, 509);  //项链
            _equipTypeDescDict.Add(10, 56121); //戒指

            _equipPositionDescDict = new Dictionary<int, int>();
            _equipPositionDescDict.Add(1, 501);  //武器
            _equipPositionDescDict.Add(2, 502);  //头盔
            _equipPositionDescDict.Add(3, 503);  //肩甲
            _equipPositionDescDict.Add(4, 504);  //胸甲
            _equipPositionDescDict.Add(5, 505);  //手套
            _equipPositionDescDict.Add(6, 506);  //腰带
            _equipPositionDescDict.Add(7, 507);  //裤子
            _equipPositionDescDict.Add(8, 508);  //鞋子
            _equipPositionDescDict.Add(9, 509);  //项链
            _equipPositionDescDict.Add(10, 510); //左戒指
            _equipPositionDescDict.Add(11, 511); //右戒指

            _equipPositionIconDict = new Dictionary<int, int>();
            _equipPositionIconDict.Add(1, 11427);
            _equipPositionIconDict.Add(2, 11421);
            _equipPositionIconDict.Add(3, 11432);
            _equipPositionIconDict.Add(4, 11423);
            _equipPositionIconDict.Add(5, 11424);
            _equipPositionIconDict.Add(6, 11425);
            _equipPositionIconDict.Add(7, 11426);
            _equipPositionIconDict.Add(8, 11430);
            _equipPositionIconDict.Add(9, 11422);
            _equipPositionIconDict.Add(10, 11429);
            _equipPositionIconDict.Add(11, 11429);
        }

        public static string GetEquipTypeDesc(int equipType)
        {
            if(_equipTypeDescDict.ContainsKey(equipType) == true)
            {
                return MogoLanguageUtil.GetContent(_equipTypeDescDict[equipType]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static string GetEquipPositionDesc(int equipPosition)
        {
            if (_equipPositionDescDict.ContainsKey(equipPosition) == true)
            {
                return MogoLanguageUtil.GetContent(_equipPositionDescDict[equipPosition]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static int GetEquipPositionIcon(int equipPosition)
        {
            if(_equipPositionIconDict.ContainsKey(equipPosition) == true)
            {
                return _equipPositionIconDict[equipPosition];
            }
            return 0;
        }

        public static string GetItemTypeDesc(int type)
        {
            if (_typeDescDict == null)
            {
                _typeDescDict = MogoStringUtils.Convert2Dic(XMLManager.global_params[72].__value);
            }
            if(_typeDescDict.ContainsKey(type) == true)
            {
                return MogoLanguageUtil.GetContent(_typeDescDict[type]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static string GetItemQualityDesc(int quality)
        {
            if(_qualityDescDict.ContainsKey(quality) == true)
            {
                return MogoLanguageUtil.GetContent(_qualityDescDict[quality]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static string GetItemVocationDesc(int vocation)
        {
            if(_vocationDescDict.ContainsKey(vocation) == true)
            {
                return MogoLanguageUtil.GetContent(_vocationDescDict[vocation]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static item_commom GetItemConfig(int id)
        {
            if(!XMLManager.item_commom.ContainsKey(id))
            {
                throw new Exception("配置表 item_common.xml 未包含项 " + id.ToString());
            }
            return XMLManager.item_commom[id];
        }

        public static item_equipment GetEquipConfig(int id)
        {
            if(!XMLManager.item_equipment.ContainsKey(id))
            {
                throw new Exception("配置表 item_equipment.xml 未包含项 " + id.ToString());
            }
            return XMLManager.item_equipment[id];
        }

        public static int GetEquipOpenableNewSlotCount(int equipId)
        {
            item_equipment config = GetEquipConfig(equipId);
            List<string> dataList = data_parse_helper.ParseListString(config.__new_slot);
            int count = dataList.Count;
            if(GlobalParams.GetNewSlotOpenLevel() > PlayerAvatar.Player.level)
            {
                return 0;
            }
            return count;
        }

        public static BagItemType GetItemType(int id)
        {
            if(IsEquip(id) == true)
            {
                return BagItemType.Equip;
            }
            else
            {
                return (BagItemType)(GetItemConfig(id).__type);
            }
        }

        public static bool IsEquip(int id)
        {
            return id > 100000 && id < 999999;
        }

        public static int BindType(int id)
        {
            if (IsEquip(id))
            {
                return GetEquipConfig(id).__bind_type;

            }
            else
            {
                return GetItemConfig(id).__bind_type;
            }
        }

        public static string GetName(int id)
        {
            List<string> name;
            if(IsEquip(id))
            {
                name = data_parse_helper.ParseListString(GetEquipConfig(id).__name);

            }
            else
            {
                name = data_parse_helper.ParseListString(GetItemConfig(id).__name);
            }
            return (name.Count > 0) ? (MogoLanguageUtil.GetContent(name)) : id.ToString();
        }

        public static int GetQuality(int id)
        {
            if(IsEquip(id))
            {
                return GetEquipConfig(id).__quality;
            }
            else
            {
                return GetItemConfig(id).__quality;
            }
        }

        public static int GetStarLevel(int id)
        {
            if (IsEquip(id))
            {
                return GetEquipConfig(id).__equ_star;
            }
            return 0;
        }

        public static int GetDropModelID(int id, bool canBePicked)
        {
            List<string> modelIdList = data_parse_helper.ParseListString(IsEquip(id) ? GetEquipConfig(id).__look : GetItemConfig(id).__look);
            if (modelIdList.Count == 1 || canBePicked)
            {
                return int.Parse(modelIdList[0]);
            }
            else if (modelIdList.Count >= 2)
            {
                return int.Parse(modelIdList[1]);
            }
            else
            {
                LoggerHelper.Error(string.Format("获取掉落物的模型id, id = {0}， canBePicked = {1}的look字段配置出错。", id, canBePicked));
                return 0;
            }
        }

        public static string GetDropModelPath(int id, bool canBePicked)
        {
            var modelID = GetDropModelID(id, canBePicked);
            if (XMLManager.drop_model.ContainsKey(modelID))
            {
                return XMLManager.drop_model[modelID].__prefab;
            }
            return string.Empty;
        }

        public static int GetDropRange(int id)
        {
            if (IsEquip(id))
            {
                return GetEquipConfig(id).__drop_range;
            }
            else
            {
                return GetItemConfig(id).__drop_range;
            }
        }

        public static string GetDropEffectPath(int id)
        {
            var qualityID = GetQuality(id);
            if (XMLManager.drop_model_effect.ContainsKey(qualityID))
            {
                return XMLManager.drop_model_effect[qualityID].__prefab;
            }
            return string.Empty;
        }

        public static int GetEquipModelID(int id)
        {
            if (IsEquip(id))
            {
                return GetEquipConfig(id).__equip_model;
            }
            return 0;
        }

        public static int GetIcon(int id)
        {
            int icon = 0;
            if(IsEquip(id))
            {
                icon = GetEquipConfig(id).__icon;
            }
            else
            {
                icon = GetItemConfig(id).__icon;
            }
            return icon;
        }

        public static Color GetColor(int id)
        {
            if(IsEquip(id))
            {
                return ColorDefine.GetColorById(GetEquipConfig(id).__quality);
            }
            else
            {
                return ColorDefine.GetColorById(GetItemConfig(id).__quality);
            }
        }

        public static string GetColorHexToken(int id)
        {
            if (IsEquip(id))
            {
                return ColorDefine.GetColorHexToken(GetEquipConfig(id).__quality);
            }
            else
            {
                return ColorDefine.GetColorHexToken(GetItemConfig(id).__quality);
            }
        }

        public static string GetDescPurpose(int id)
        {
            return GetDescription(id, 1);
        }

        private static string GetDescription(int id,int index)
        {
            item_commom itemCommon = GetItemConfig(id);
            if(itemCommon!=null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemCommon.__desc);
                return MogoLanguageUtil.GetContent(int.Parse(dataDict[index.ToString()]));
            }
            else
            {
                item_equipment itemEquipment = GetEquipConfig(id);
                Dictionary<string, string> dataDict2 = data_parse_helper.ParseMap(itemEquipment.__desc);
                return MogoLanguageUtil.GetContent(int.Parse(dataDict2[index.ToString()]));
            }
        }

        public static bool IsRare(int id)
        {
            int quality = GetQuality(id);
            return quality >= 4;
        }

        public static int GetPetAddExp(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dataDict[ItemUseEffect.OBTAIN_PET_EXP]);
        }

        public static int GetWingId(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dataDict[ItemUseEffect.WING_ID]);
        }

        public static int GetFashionId(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dataDict[ItemUseEffect.FASION_ID]);
        }

        public static int GetEffectiveTime(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dataDict[ItemUseEffect.EFFECTIVE_TIME]);
        }

        public static Dictionary<int, float> GetUseEffect(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return data_parse_helper.ParseDictionaryIntFloat(dataDict);
        }

        public static KeyValuePair<int, float> GetUseEffectKvp(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<int, float> dic = GetUseEffect(id);
            foreach (KeyValuePair<int, float> kvp in dic)
            {
                return kvp;
            }
            return new KeyValuePair<int,float>(0,0);
        }

        /// <summary>
        /// 垃圾装备定义
        /// 
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static bool IsEquipBetter(EquipItemInfo equipItemInfo)
        {
            ///转交期内
            uint canAssignTime = 60 * 60 * uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.transfer_item_to_player_time));
            uint spanTime = Global.serverTimeStampSecond > equipItemInfo.GetTime ? Global.serverTimeStampSecond - equipItemInfo.GetTime : 0;
            if (equipItemInfo.UserList != null && equipItemInfo.UserList.Count > 0 && spanTime < canAssignTime)
            {
                return true;
            }

            //非本职装备
            if (equipItemInfo.UsageVocationRequired != PlayerAvatar.Player.vocation)
            {
                return false;
            }
            List<BaseItemInfo> bodyEquipList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo().Values.ToList();
            int equipNum = bodyEquipNumBySubType(equipItemInfo.SubType);

            if (equipNum < 1 || (equipItemInfo.SubType == EquipType.ring && equipNum < 2))
            {
                return true;
            }

            for (int j = 0; j < bodyEquipList.Count; j++)
            {
                EquipItemInfo bodyEquipInfo = bodyEquipList[j] as EquipItemInfo;

                if(bodyEquipInfo.SubType != equipItemInfo.SubType)
                {
                    continue;
                }
                if (bodyEquipInfo.FightForce < equipItemInfo.FightForce)
                {
                    return true;
                }
                else
                {
                    if (equipItemInfo.Level > bodyEquipInfo.Level)
                    {
                        if (equipItemInfo.Quality >= 4)
                        {
                            return true;
                        }
                    }
                    else if (equipItemInfo.Level == bodyEquipInfo.Level)
                    {
                        if (equipItemInfo.Quality > bodyEquipInfo.Quality)
                        {
                            return true;
                        }
                        else if (equipItemInfo.Quality == bodyEquipInfo.Quality)
                        {
                            if (equipItemInfo.Star > bodyEquipInfo.Star)
                            {
                                return true;
                            }
                            else if (equipItemInfo.Quality >= 5)
                            {
                                return true;
                            }
                        }
                        else if (equipItemInfo.Quality >= 5)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (equipItemInfo.Quality >= 5)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private static int bodyEquipNumBySubType(EquipType equipType)
        {
            int count = 0;
            List<BaseItemInfo> bodyEquipList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo().Values.ToList();
            for (int i = 0; i < bodyEquipList.Count; i++)
            {
                EquipItemInfo bodyEquipInfo = bodyEquipList[i] as EquipItemInfo;
                if (bodyEquipInfo.SubType == equipType)
                {
                    count++;
                }
            }
            return count;
        }

        public static int GetMyMoneyCountByItemId(int itemId)
        {
            switch(itemId)
            {
                case public_config.MONEY_TYPE_GOLD:
                    return PlayerAvatar.Player.money_gold;
                case public_config.MONEY_TYPE_DIAMOND:
                    return PlayerAvatar.Player.money_diamond;
                case public_config.MONEY_TYPE_BIND_DIAMOND:
                    return PlayerAvatar.Player.money_bind_diamond;
                case public_config.MONEY_TYPE_HONOUR:
                    return PlayerAvatar.Player.honour;
                case public_config.MONEY_TYPE_ENERGY:
                    return PlayerAvatar.Player.energy;
                case public_config.MONEY_TYPE_POWER:
                    return PlayerAvatar.Player.power;
                case public_config.MONEY_TYPE_RUNE_STONE:
                    return PlayerAvatar.Player.money_rune_stone;
                case public_config.MONEY_TYPE_GUILD_CONTRIBUTION:
                    return PlayerDataManager.Instance.GuildData.GetMyContribution();
                case public_config.MONEY_TYPE_GUILD_FUND:
                    return PlayerDataManager.Instance.GuildData.MyGuildData.Money;
                case public_config.MONEY_TYPE_STAR_SPIRIT:
                    return (int)PlayerAvatar.Player.star_spirit_num;
                case public_config.MONEY_TYPE_FORTRESS_STONE:
                    return PlayerAvatar.Player.fortress_stone;
                case public_config.MONEY_TYPE_FORTRESS_WOOD:
                    return PlayerAvatar.Player.fortress_wood;
                case public_config.MONEY_TYPE_ACTIVE_POINT:
                    return (int)PlayerAvatar.Player.active_point;
                case public_config.MONEY_TYPE_TUTOR_VALUE:
                    return PlayerAvatar.Player.tutor_value;
                default:
                    return PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(itemId);
            }
        }

        public static bool isMoneyType(int itemId)
        {
            return itemId <= public_config.MONEY_TYPE_MAX_ID;
        }

        public static int GetSuitEffect(int product)
        {
            item_commom item = GetItemConfig(product);
            if (item != null) return -1;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(item.__use_effect);
            if( dict != null
                && dict.ContainsKey("5"))
            {
                return int.Parse(dict["5"]);
            }
            return -1;
        }

        public static int GetMountId(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dict[ItemUseEffect.MOUNT_ID]);
        }

        public static int GetPetId(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(itemConfig.__use_effect);
            return int.Parse(dict[ItemUseEffect.OBTAIN_PET]);
        }

        /// <summary>
        /// 返回使用该物品时，需要扣除的其他物品ID列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns>
        /// 返回一个字典：
        /// Key : 消耗的道具ID
        /// Value : 消耗的道具数据
        /// </returns>
        public static Dictionary<int,int> GetItemUseCost(int id)
        {
            item_commom itemConfig = GetItemConfig(id);
            Dictionary<string, string> cost = data_parse_helper.ParseMap(itemConfig.__use_cost);
            if (cost == null || cost.Count <= 0)
            {
                return null;
            }
            else
            {
                Dictionary<int, int> dict = new Dictionary<int, int>();
                foreach (var pair in cost)
                {
                    int itemId = int.Parse(pair.Key);
                    int itemCount = int.Parse(pair.Value);
                    dict.Add(itemId, itemCount);
                }
                return dict;
            }
        }

        public static Dictionary<int, int> GetItemUseLimit(int id)
        {
            if (XMLManager.item_commom.ContainsKey(id) == false)
            {
                throw new Exception("item_common.xml 未找到配置项 " + id);
            }
            return StringDictToIntDict(data_parse_helper.ParseMap(XMLManager.item_commom[id].__use_limit));
        }

        public static Dictionary<int, int> StringDictToIntDict(Dictionary<string, string> dict)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            foreach (KeyValuePair<string, string> pair in dict)
            {
                result.Add(int.Parse(pair.Key), int.Parse(pair.Value));
            }
            return result;
        }

        public static Dictionary<int, List<int>> GetItemUseLimit2(int id)
        {
            if (XMLManager.item_commom.ContainsKey(id) == false)
            {
                throw new Exception("item_common.xml 未找到配置项 " + id);
            }
            return data_parse_helper.ParseDictionaryIntListInt(XMLManager.item_commom[id].__item_use_2);
        }

        public static int GetCompositeItem(int itemId)
        {
            item_commom cfg = GetItemConfig(itemId);
            Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__use_effect);
            if (cfg != null && dict != null && dict.ContainsKey("18"))
            {
                return int.Parse(dict["18"]);
            }
            return 0;
        }
    }
}
