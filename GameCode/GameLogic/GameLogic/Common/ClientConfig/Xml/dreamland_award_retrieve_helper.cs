﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/20 11:31:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class DreamlandAwardRetrieve
    {
        public Dictionary<int, List<dreamland_award_retrieve>> LevelDict = new Dictionary<int,List<dreamland_award_retrieve>>();

        public List<dreamland_award_retrieve> GetRetrieveRewardList(int level)
        {
            if (LevelDict.ContainsKey(level))
            {
                return LevelDict[level];
            }
            List<dreamland_award_retrieve> retrieveRewardList = new List<dreamland_award_retrieve>();
            LevelDict.Add(level, retrieveRewardList);
            return retrieveRewardList;
        }

        public void Add(dreamland_award_retrieve config)
        {
            List<string> dataList = data_parse_helper.ParseListString(config.__level_range);
            int levelMin = int.Parse(dataList[0]);
            int levelMax = int.Parse(dataList[1]);
            for (int level = levelMin; level <= levelMax; level++)
            {
                List<dreamland_award_retrieve> retrieveRewardList = GetRetrieveRewardList(level);
                retrieveRewardList.Add(config);
            }
        }

        public void Sort()
        {
            foreach (List<dreamland_award_retrieve> list in LevelDict.Values)
            {
                list.Sort(SortByGetBackType);
            }
        }

        private int SortByGetBackType(dreamland_award_retrieve x, dreamland_award_retrieve y)
        {
            if (x.__get_back < y.__get_back)
            {
                return -1;
            }
            return 1;
        }
    }

    public class dreamland_award_retrieve_helper
    {
        public const int GetBackTypeFree = 1;
        public const int GetBackTypeCost1 = 2;
        public const int GetBackTypeCost2 = 3;

        private static Dictionary<int, DreamlandAwardRetrieve> _stationNumDict = new Dictionary<int, DreamlandAwardRetrieve>();

        static dreamland_award_retrieve_helper()
        {
            foreach (int id in XMLManager.dreamland_award_retrieve.Keys)
            {
                dreamland_award_retrieve config = GetConfig(id);
                DreamlandAwardRetrieve awardRetrieve = GetDreamlandAwardRetrieve(config.__yesterday_station);
                awardRetrieve.Add(config);
            }

            Sort();
        }

        private static void Sort()
        {
            foreach (DreamlandAwardRetrieve awardRetrieve in _stationNumDict.Values)
            {
                awardRetrieve.Sort();
            }
        }

        private static DreamlandAwardRetrieve GetDreamlandAwardRetrieve(int stationNum)
        {
            if (_stationNumDict.ContainsKey(stationNum))
            {
                return _stationNumDict[stationNum];
            }
            DreamlandAwardRetrieve awardRetrieve = new DreamlandAwardRetrieve();
            _stationNumDict.Add(stationNum, awardRetrieve);
            return awardRetrieve;
        }

        private static dreamland_award_retrieve GetConfig(int id)
        {
            return XMLManager.dreamland_award_retrieve[id];
        }

        public static List<dreamland_award_retrieve> GetRetrieveRewardList(int stationNum, int level)
        {
            return _stationNumDict[stationNum].LevelDict[level];
        }
    }
}
