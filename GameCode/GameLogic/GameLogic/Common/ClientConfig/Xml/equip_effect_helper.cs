﻿#region 模块信息
/*==========================================
// 文件名：equip_effect_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/16 18:44:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using GameMain.Entities;
namespace GameData
{
    public enum EffectGroup
    {
        WeaponParticle = 1, // 武器粒子特效
        WeaponFlow = 2,     // 武器流光特效
        Armor = 3,          // 护甲流光
    }

    public class equip_effect_helper
    {
        private const int MIN_WEAPON = 1;
        private const int MAX_WEAPON = 40;
        private const int MIN_ARMOR = 41;

        public static int GetFlowId(int effectId, int equipId)
        {
            int id = equipId * 1000 + effectId;
            if (XMLManager.equip_effect.ContainsKey(id))
            {
                return XMLManager.equip_effect[id].__material_id;
            }
            //UnityEngine.Debug.LogError("@黄兴椿 装备外观配置表中缺少ID为 " + id + " 的配置选项");
            return 0;
        }

        public static int GetParticleId(int effectId, int equipId)
        {
            int id = equipId * 1000 + effectId;
            if (XMLManager.equip_effect.ContainsKey(id))
            {
                return XMLManager.equip_effect[id].__particle_id;
            }
            //UnityEngine.Debug.LogError("@黄兴椿 装备外观配置表中缺少ID为 " + id + " 的配置选项");
            return 0;
        }
    }
}