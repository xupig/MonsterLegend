﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class wild_helper
    {
        private static Dictionary<string, List<wild>> _wildSpaceDic;

        public static void InitSpaceDic()
        {
            _wildSpaceDic = new Dictionary<string,List<wild>>();
            foreach(KeyValuePair<int,wild> kvp in XMLManager.wild)
            {
                if (_wildSpaceDic.ContainsKey(kvp.Value.__space_name) == false)
                {
                    _wildSpaceDic[kvp.Value.__space_name] = new List<wild>();
                }
                _wildSpaceDic[kvp.Value.__space_name].Add(kvp.Value);
            }
        }

        private static Dictionary<string, Dictionary<Vector3, string>> spaceDummyPosDict;
        public static Dictionary<Vector3,string> GetCurrSceneDummyPosInfo(string spaceName)
        {
            if (_wildSpaceDic == null)
            {
                InitSpaceDic();
            }
            if (_wildSpaceDic.ContainsKey(spaceName) == false)
            {
                return null;
            }
            if(spaceDummyPosDict == null)
            {
                spaceDummyPosDict = new Dictionary<string, Dictionary<Vector3, string>>();
            }
            if(spaceDummyPosDict.ContainsKey(spaceName))
            {
                return spaceDummyPosDict[spaceName];
            }
            List<wild> wildList = _wildSpaceDic[spaceName];
            int count = wildList.Count;
            Dictionary<Vector3, string> result = new Dictionary<Vector3, string>();
            for (int i = 0; i < count; i++)
            {
                if (wildList[i].__type == 5)
                {
                    List<string> position = data_parse_helper.ParseListString(wildList[i].__position);
                    Vector3 vector = new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
                    if (result.ContainsKey(vector) == false)
                    {
                        result.Add(vector, wildList[i].__name.ToLanguage());
                    }
                }
            }
            spaceDummyPosDict.Add(spaceName, result);
            return result;
        }

        private static Dictionary<string, Dictionary<Vector3, string>> spaceTransformPointDict;
        public static Dictionary<Vector3,string> GetTransformPointDict(string spaceName)
        {
            if (_wildSpaceDic == null)
            {
                InitSpaceDic();
            }
            if (_wildSpaceDic.ContainsKey(spaceName) == false)
            {
                return null;
            }
            if(spaceTransformPointDict == null)
            {
                spaceTransformPointDict = new Dictionary<string, Dictionary<Vector3, string>>();
            }
            if(spaceTransformPointDict.ContainsKey(spaceName))
            {
                return spaceTransformPointDict[spaceName];
            }
            List<wild> wildList = _wildSpaceDic[spaceName];
            int count = wildList.Count;
            Dictionary<Vector3, string> result = new Dictionary<Vector3, string>();
            for (int i = 0; i < count; i++)
            {
                if (wildList[i].__type == 1)
                {
                    List<string> position = data_parse_helper.ParseListString(wildList[i].__position);
                    Vector3 vector = new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
                    if(result.ContainsKey(vector) == false)
                    {
                        result.Add(vector, wildList[i].__name.ToLanguage());
                    }
                }
            }
            spaceTransformPointDict.Add(spaceName, result);
            return result;
        }

        public static List<Vector3> GetTransferPoint(string spaceName)
        {
            if (_wildSpaceDic == null)
            {
                InitSpaceDic();
            }
            if(_wildSpaceDic.ContainsKey(spaceName) == false)
            {
                return null;
            }
            List<wild> wildList = _wildSpaceDic[spaceName];
            int count = wildList.Count;
            List<Vector3> result = new List<Vector3>();
            for (int i = 0; i < count; i++)
            {
                if (wildList[i].__type == 1)
                {
                    List<string> position = data_parse_helper.ParseListString(wildList[i].__position);
                    result.Add(new Vector3(float.Parse(position[0]),float.Parse(position[1]),float.Parse(position[2])));
                }
            }
            return result;
        }

        public static Vector3 GetMonsterPosition(string spaceName, int monsterId)
        {
            if (_wildSpaceDic == null)
            {
                InitSpaceDic();
            }
            if (_wildSpaceDic.ContainsKey(spaceName) == false)
            {
                return Vector3.zero;
            }
            List<wild> wildList = _wildSpaceDic[spaceName];
            int count = wildList.Count;
            for (int i = 0; i < count; i++)
            {
                if (wildList[i].__type == 2 && wildList[i].__type_arg == monsterId)
                {
                    List<string> position = data_parse_helper.ParseListString(wildList[i].__position);
                    return new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
                }
            }
            return Vector3.zero;
        }

        public static Vector3 GetMonsterPosition(int insID, int monsterId)
        {
            var mapID = instance_helper.GetMapId(insID);
            var spaceName = map_helper.GetSpaceName(mapID);
            return GetMonsterPosition(spaceName, monsterId);
        }

        private static Dictionary<int, Vector3> _treasurePoint;
        public static Vector3 GetTreasurePosition(int treasureMapId)
        {
            if (_treasurePoint == null) _treasurePoint = new Dictionary<int, Vector3>();
            if (_treasurePoint.ContainsKey(treasureMapId))
            {
                return _treasurePoint[treasureMapId];
            }
            KeyValuePair<int, float> kvp = item_helper.GetUseEffectKvp(treasureMapId);
            int insID = (int)kvp.Value;
            var mapID = instance_helper.GetMapId(insID);
            var spaceName = map_helper.GetSpaceName(mapID);
            Vector3 position = GetTreasurePosition(spaceName);
            _treasurePoint.Add(treasureMapId, position);
            return position;
        }

        public static void RemoveTreasurePosition(int treasureMapId)
        {
            if (_treasurePoint.ContainsKey(treasureMapId))
            {
                _treasurePoint.Remove(treasureMapId);
            }
        }

        private static Vector3 GetTreasurePosition(string spaceName)
        {
            List<wild> wildList = GetWildListByType(spaceName, 4);
            if (wildList.Count == 0)
            {
                throw new Exception("野外配置表宝图坐标未配置： 地图id" + spaceName);
            }
            //随机生成0（包含）~__map_ids.Count（不包含）的随机数
            int region = UnityEngine.Random.Range(0, wildList.Count);
            List<string> positionList = data_parse_helper.ParseListString(wildList[region].__position);
            if (positionList.Count % 3 != 0)
            {
                throw new Exception("野外配置表宝图坐标配置错误： id" + wildList[region].__id);
            }
            int positionCount = positionList.Count / 3;
            int index = UnityEngine.Random.Range(0, positionCount);
            return new Vector3(float.Parse(positionList[3 * index]), float.Parse(positionList[3 * index + 1]), float.Parse(positionList[3 * index + 2]));
        }

        private static List<wild> GetWildListByType(string spaceName, int type)
        {
            if (_wildSpaceDic == null)
            {
                InitSpaceDic();
            }
            if (_wildSpaceDic.ContainsKey(spaceName) == false)
            {
                return new List<wild>();
            }
            List<wild> wildList = _wildSpaceDic[spaceName];
            List<wild> result = new List<wild>();
            for (int i = 0; i < wildList.Count; i++)
            {
                if (wildList[i].__type == type)
                {
                    result.Add(wildList[i]);
                }
            }
            return result;
        }

        public static wild GetWild(int wildId)
        {
            if(XMLManager.wild.ContainsKey(wildId) == false)
            {
                Debug.LogError(string.Format("缺少ID为{0}的野外配置",wildId));
                return null;
            }
            return XMLManager.wild[wildId];
        }

    }
}
