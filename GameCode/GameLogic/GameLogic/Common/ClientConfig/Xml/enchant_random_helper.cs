﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class enchant_random_helper
    {
        private static Dictionary<int, Dictionary<int, List<int>>> _enchantRandomDic;

        public static List<int> GetAttributeRange(int enchantScrollId, int attributeId, int equipLevel, int equipQuality, int luckyValue)
        {
            if (enchantScrollId == 0)
            {
                return null;
            }
            List<float> rangeFactor = GetAdjustFactor(equipLevel, equipQuality, luckyValue);
            List<int> rawRange = GetRawAttributeRange(enchantScrollId, attributeId);

            rawRange[0] = Mathf.CeilToInt(rawRange[0] * (rangeFactor[0] + 1));
            rawRange[1] = Mathf.CeilToInt(rawRange[1] * (rangeFactor[1] + 1));

            return rawRange;

        }

        private static List<int> GetRawAttributeRange(int enchantScrollId, int attributeId)
        {
            if (_enchantRandomDic == null)
            {
                InitData();
            }

            List<int> result = new List<int>();
            List<int> randomRangeIdList = enchant_scroll_helper.GetRandomRangeIdList(enchantScrollId);
            int minValue = int.MaxValue;
            int maxValue = 0;
            for (int i = 0; i < randomRangeIdList.Count; i++)
            {
                List<int> range = _enchantRandomDic[attributeId][randomRangeIdList[i]];
                int start = range[0];
                int end = range[1];
                if (minValue > start)
                {
                    minValue = start;
                }
                if (maxValue < end)
                {
                    maxValue = end;
                }
            }

            result.Add(minValue);
            result.Add(maxValue);
            return result;
        }

        private static List<float> GetAdjustFactor(int equipLevel, int equipQuality, int luckyValue)
        {
            KeyValuePair<int, List<float>> luckyAdjustInfo = enchant_helper.GetEnchantLuckyAdjustInfo(luckyValue);
            List<float> levelAdjustInfo = enchant_helper.GetLevelAdjustInfo(equipLevel);
            List<float> qualityAdjustInfo = enchant_helper.GetQualityAdjustInfo(equipQuality);

            float upperLimit = 0;
            float lowerLimit = 0;
            if (luckyAdjustInfo.Key != 0)
            {
                lowerLimit += luckyAdjustInfo.Value[2];
                upperLimit += luckyAdjustInfo.Value[3];
            }

            if (levelAdjustInfo != null)
            {
                lowerLimit += levelAdjustInfo[2];
                upperLimit += levelAdjustInfo[3];
            }

            if (qualityAdjustInfo != null)
            {
                lowerLimit += qualityAdjustInfo[0];
                upperLimit += qualityAdjustInfo[1];
            }

            List<float> range = new List<float>();
            range.Add(lowerLimit);
            range.Add(upperLimit);

            return range;
        }

        private static void InitData()
        {
            _enchantRandomDic = new Dictionary<int, Dictionary<int, List<int>>>();
            foreach (KeyValuePair<int, enchant_random> kvp in XMLManager.enchant_random)
            {
                if (_enchantRandomDic.ContainsKey(kvp.Value.__attri_id) == false)
                {
                    _enchantRandomDic.Add(kvp.Value.__attri_id, new Dictionary<int, List<int>>());
                }

                Dictionary<int, List<int>> attributeRangeDic = _enchantRandomDic[kvp.Value.__attri_id];

                if (attributeRangeDic.ContainsKey(kvp.Value.__value_range_id) == false)
                {
                    List<int> result = new List<int>();
                    List<string> range = data_parse_helper.ParseListString(kvp.Value.__range);
                    int start = int.Parse(range[0]);
                    int end = int.Parse(range[1]);

                    result.Add(start);
                    result.Add(end);
                    attributeRangeDic.Add(kvp.Value.__value_range_id, result);
                }
            }
        }
    }
}
