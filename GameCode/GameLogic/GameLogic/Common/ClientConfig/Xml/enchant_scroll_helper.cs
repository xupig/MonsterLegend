﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using GameMain.GlobalManager;
using Common.Structs;
using Common.Structs.Enum;
using System.Linq;

namespace GameData
{
    /// <summary>
    /// 套装卷轴道具
    /// </summary>
    public class enchant_scroll_helper
    {

        public static List<int> GetEquipPositionList(int enchantScrollId)
        {
            enchant_scroll config = GetConfig(enchantScrollId);
            return data_parse_helper.ParseListInt(config.__equip_type);
        }

        public static int GetMinEquipLevel(int enchantScrollId)
        {
            enchant_scroll config = GetConfig(enchantScrollId);
            return Convert.ToInt32(config.__equip_level[0]);
        }

        public static int GetMaxEquipLevel(int enchantScrollId)
        {
            enchant_scroll config = GetConfig(enchantScrollId);
            return Convert.ToInt32(config.__equip_level[1]);
        }

        public static int GetMinEquipQuality(int enchantScrollId)
        {
            enchant_scroll config = GetConfig(enchantScrollId);
            return Convert.ToInt32(config.__equip_quality[0]);
        }
       
        public static enchant_scroll GetConfig(int enchantScrollId)
        {
            if (XMLManager.enchant_scroll.ContainsKey(enchantScrollId) == false)
            {
                throw new Exception("enchant_scroll.xml 未找到配置项 " + enchantScrollId);
            }
            return XMLManager.enchant_scroll[enchantScrollId];
        }

        public static int GetEnchantScrollId(EquipItemInfo equipItemInfo)
        {
            foreach (KeyValuePair<int, enchant_scroll> kvp in XMLManager.enchant_scroll)
            {
                enchant_scroll config = kvp.Value;
                bool isCanUse = IsEquipCanUseEnchantScroll(config, equipItemInfo);

                if (isCanUse == true)
                {
                    return kvp.Key;
                }
            }
            return 0;
        }

        public static List<int> GetRandomRangeIdList(int enchantScrollId)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(GetConfig(enchantScrollId).__value_range_weight);
            List<string> randomRangeIdList = dataDict.Keys.ToList();
            return data_parse_helper.ParseListInt(randomRangeIdList);
        }

        public static List<int> GetEnchantAttributeIdList(int enchantScrollId)
        {
            Dictionary<string,string> attriIdWeightDic = data_parse_helper.ParseMap(GetConfig(enchantScrollId).__attri_id_weight);
            Dictionary<int, int> attriIdWeightIntDic = data_parse_helper.ParseDictionaryIntInt(attriIdWeightDic);

            return attriIdWeightIntDic.Keys.ToList();
        }

        public static BaseItemInfo GetCurrEnchantScrollFlag(int itemId)
        {
            enchant_scroll enchantScrollInfo = GetConfig(itemId);
            BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            Dictionary<int, BaseItemInfo> equipData = bodyEquipData.GetAllItemInfo();

            BaseItemInfo result = null;
            foreach (KeyValuePair<int, BaseItemInfo> kvp in equipData)
            {
                EquipItemInfo equipItemInfo = kvp.Value as EquipItemInfo;

                bool isCanUse = IsEquipCanUseEnchantScroll(enchantScrollInfo, equipItemInfo);

                if (isCanUse == true)
                {
                    ///当身上存在满足该附魔卷轴使用条件的装备时：
                    ///1、若存在未附魔装备，选择其中一个未附魔的装备，
                    ///2、若均附魔，选择第一个可附魔装备
                    if (equipItemInfo.EnchantAtrri.Count == 0)
                    {
                        result = equipItemInfo;
                        return result;
                    }
                    else if (result == null)
                    {
                        result = equipItemInfo;
                    }
                }
            }
            return result;
        }

        private static bool IsEquipCanUseEnchantScroll(enchant_scroll config, EquipItemInfo equipItemInfo)
        {
            List<string> dataList = data_parse_helper.ParseListString(config.__equip_level);
            if (config.__equip_type.Contains(((int)equipItemInfo.SubType).ToString()) == false)
            {
                return false;
            }
            else if (config.__equip_quality.Contains(equipItemInfo.Quality.ToString()) == false)
            {
                return false;
            }
            else if (equipItemInfo.Level < int.Parse(dataList[0])
                || equipItemInfo.Level > int.Parse(dataList[1]))
            {
                return false;
            }
            return true;   
        }
    }
}
