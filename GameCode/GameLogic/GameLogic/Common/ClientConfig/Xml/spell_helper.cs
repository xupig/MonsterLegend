﻿#region 模块信息
/*==========================================
// 文件名：npc_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:17:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class spell_helper
    {
        public static spell GetSkill(int skillid)
        {
            if (XMLManager.spell.ContainsKey(skillid))
            {
                return XMLManager.spell[skillid];
            }
            return null;
        }

        public static float GetDistance(int skillid)
        {
            spell skillData = GetSkill(skillid);
            if (skillData != null)
            {
                var regionArg = data_parse_helper.ParseListInt(skillData.__search_region_arg);
                if (regionArg != null && regionArg.Count > 0)
                {
                    return regionArg[0] * 0.01f;
                }
            }
            return 1f;
        }

        public static float GetAIRange(int skillid)
        {
            spell skillData = GetSkill(skillid);
            if (skillData != null)
            {
                return skillData.__ai_range * 0.01f;
            }
            return 1f;
        }

        public static int GetSearchTargetType(int skillid)
        {
            spell skillData = GetSkill(skillid);
            if (skillData != null)
            {
                return skillData.__search_tgt_type == 0 ? 1 : skillData.__search_tgt_type;
            }
            return 1;
        }

        public static int GetIconId(int skillid)
        {
            spell skillData = GetSkill(skillid);
            if (skillData != null)
            {
                return skillData.__icon;
            }
            return 0;
        }

        public static int GetName(int skillId)
        {
            spell skillData = GetSkill(skillId);
            if (skillData != null)
            {
                return skillData.__name;
            }
            return 0;
        }

        public static int GetDesc(int skillId)
        {
            spell skillData = GetSkill(skillId);
            if (skillData != null)
            {
                return skillData.__desc;
            }
            return 0;
        }

        public static int GetPos(int skillId)
        {
            spell skillData = GetSkill(skillId);
            if (skillData != null)
            {
                return skillData.__pos;
            }
            return 0;
        }

        public static int GetShowType(int skillId)
        {
            spell skillData = GetSkill(skillId);
            if (skillData != null)
            {
                return skillData.__show_type;
            }
            return 0;
        }
    }
}
