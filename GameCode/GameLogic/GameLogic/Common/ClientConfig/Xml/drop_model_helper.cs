﻿#region 模块信息
/*==========================================
// 文件名：map_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;

namespace GameData
{
    public class  drop_model_helper
    {
        private const int PORTAL_ID = 33;

        public static drop_model GetModelData(int modelID)
        {
            if (XMLManager.drop_model.ContainsKey(modelID))
            {
                return XMLManager.drop_model[modelID];
            }
            return null;
        }

        public static string GetModelPath(int modelID)
        {
            return XMLManager.drop_model[modelID].__prefab;
        }

        public static string GetPortalModelPath()
        {
            drop_model cfg = GetModelData(PORTAL_ID);
            if (cfg != null)
            {
                return cfg.__prefab;
            }
            return string.Empty;
        }
    }
}
