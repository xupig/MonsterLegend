﻿#region 模块信息
/*==========================================
// 文件名：font_style_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 9:44:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class font_style_helper
    {
        public static font_style GetFontStyle(int id)
        {
            if(XMLManager.font_style.ContainsKey(id))
            {
                return XMLManager.font_style[id];
            }
            return null;
        }
    }
}
