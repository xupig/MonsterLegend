﻿#region 模块信息
/*==========================================
// 文件名：equip_channel_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/25 14:55:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using UnityEngine;
namespace GameData
{
    public class EquipDataWrapper
    {
        public int id;
        public int quality;
        public int star;
        public int descId;
    }

    public static class equip_channel_helper
    {
        private static List<EquipDataWrapper> _allEquipTypeCfg;
        public static List<EquipDataWrapper> GetAllEquipTypeCfg()
        {
            if (_allEquipTypeCfg == null || _allEquipTypeCfg.Count == 0)
            {
                _allEquipTypeCfg = new List<EquipDataWrapper>();
                foreach (KeyValuePair<int, equip_quality_info> cfg in XMLManager.equip_quality_info)
                {
                    EquipDataWrapper wrapper = new EquipDataWrapper();
                    wrapper.id = cfg.Value.__equip_quality_id;
                    wrapper.quality = cfg.Value.__equip_quality;
                    wrapper.star = cfg.Value.__equip_star;
                    wrapper.descId = cfg.Value.__quality_info;
                    _allEquipTypeCfg.Add(wrapper);
                }
            }
            return _allEquipTypeCfg;
        }



        public static int GetEquipLevel(int playerLevel)
        {
            foreach (KeyValuePair<int, equip_level> pair in XMLManager.equip_level)
            {
                List<string> dataList = data_parse_helper.ParseListString(pair.Value.__player_level);
                int minLevel = int.Parse(dataList[0]);
                int maxLevel = int.Parse(dataList[1]);
                if (playerLevel >= minLevel && playerLevel <= maxLevel)
                {
                    return pair.Value.__equip_level;
                }
            }
            Debug.LogError("@策划 装备来源表equip_level缺少玩家等级为  " + playerLevel + "  的配置");
            return 20;
        }

        //public static List<EquipItemData> GetRecommendEquipList(int equipLevel, int quality, int star)
        //{
        //    for(var pair in )
        //}
    }
}
