﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;


namespace GameData
{
    /// <summary>
    /// 对应配置表buff.xml
    /// </summary>
    public class role_data_helper
    {
        public static int GetModel(int vocation)
        {
            if (XMLManager.role_data.ContainsKey(vocation))
            {
                return XMLManager.role_data[vocation].__model;
            }
            return 0;
        }

        public static int GetSpeed(int vocation)
        {
            if (XMLManager.role_data.ContainsKey(vocation))
            {
                return XMLManager.role_data[vocation].__speed;
            }
            return 0;
        }

        public static int GetDefaultCloth(int vocation)
        {
            if (XMLManager.role_data.ContainsKey(vocation))
            {
                return XMLManager.role_data[vocation].__default_cloth;
            }
            return 0;
        }

        public static int GetDefaultWeapon(int vocation)
        {
            if (XMLManager.role_data.ContainsKey(vocation))
            {
                return XMLManager.role_data[vocation].__default_weapon;
            }
            return 0;
        }

        public static string GetCityControllerPath(int vocation)
        {
            return XMLManager.role_data[vocation].__city_controller;
        }

        public static string GetCombatControllerPath(int vocation)
        {
            return XMLManager.role_data[vocation].__combat_controller;
        }

        public static string GetUIControllerPath(int vocation)
        {
            return XMLManager.role_data[vocation].__ui_controller;
        }

        public static int GetRandomSpeechID(int vocation, int id)
        {
            if (XMLManager.role_data.ContainsKey(vocation))
            {
                Dictionary<int, List<int>> speechList = data_parse_helper.ParseDictionaryIntListInt(XMLManager.role_data[vocation].__speak);
                if (speechList == null || !speechList.ContainsKey(id) || speechList[id].Count == 0)
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, speechList[id].Count);
                return speechList[id][random];
            }
            return -1;
        }

        public static List<int> GetDefaultSpellList(int vocation)
        {
            return data_parse_helper.ParseListInt(XMLManager.role_data[vocation].__spell);
        }
    }
}
