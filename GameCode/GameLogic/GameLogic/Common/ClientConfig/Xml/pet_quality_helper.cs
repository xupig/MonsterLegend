﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/24 14:54:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using UnityEngine;

namespace GameData
{
    public class PetQualityDict
    {
        Dictionary<int, pet_quality> _qualityDict = new Dictionary<int, pet_quality>();

        public pet_quality GetPetStarConf(int quality)
        {
            return _qualityDict[quality];
        }

        public void Add(int quality, pet_quality petQuality)
        {
            _qualityDict.Add(quality, petQuality);
        }

        public bool IsExist(int quality)
        {
            return _qualityDict.ContainsKey(quality);
        }
    }

    public class PetSkill
    {
        public int skillId;
        public int quality;
        public int index;

        public PetSkill(int quality, int skillId, int index)
        {
            this.quality = quality;
            this.skillId = skillId;
            this.index = index;
        }

        public PetSkill Copy()
        {
            return new PetSkill(quality, skillId, index);
        }

        public void Print()
        {
            Debug.LogError(string.Format("quality:{0}   skillId:{1} index:{2}", quality, skillId, index));
        }
    }

    public class PetSkillList
    {
        public List<PetSkill> petSkillList = new List<PetSkill>();
        private List<pet_quality> petQualityList = new List<pet_quality>();

        public void Add(pet_quality petQuality)
        {
            petQualityList.Add(petQuality);
        }

        public void Sort()
        {
            petQualityList.Sort(SortByQuality);
        }

        private int SortByQuality(pet_quality x, pet_quality y)
        {
            if (x.__quality < y.__quality)
            {
                return -1;
            }
            return 1;
        }
        
        public void FillPetSkillList()
        {
            for(int i = 0;i < petQualityList.Count;i++)
            {
                pet_quality petQuality = petQualityList[i];
                petSkillList.Add(new PetSkill(petQuality.__quality, petQuality.__pet_skill, i + 1));
            }
        }

        public List<PetSkill> CopyPetSkillList()
        {
            List<PetSkill> copyPetSkillList = new List<PetSkill>();
            for (int i = 0; i < petSkillList.Count; i++)
            {
                copyPetSkillList.Add(petSkillList[i].Copy());
            }
            return copyPetSkillList;
        }

        public List<int> GetSkillIdListByQuality(int quality)
        {
            List<int> skillIdList = new List<int>();
            for (int i = 0; i < petSkillList.Count; i++)
            {
                PetSkill petSkill = petSkillList[i];
                if (petSkill.quality == quality)
                {
                    skillIdList.Add(petSkill.skillId);
                }
            }
            return skillIdList;
        }
    }

    public class pet_quality_helper
    {
        private static Dictionary<int, PetQualityDict> _petIdDict = new Dictionary<int, PetQualityDict>();
        private static Dictionary<int, PetSkillList> _petSkillDict = new Dictionary<int, PetSkillList>();
        private static Dictionary<int, LangEnum> _qualityDict;

        static pet_quality_helper()
        {
            _qualityDict = new Dictionary<int, LangEnum>();
            _qualityDict.Add(public_config.ITEM_QUALITY_GREEN, LangEnum.PET_QUALITY_GREEN);
            _qualityDict.Add(public_config.ITEM_QUALITY_BLUE, LangEnum.PET_QUALITY_BLUE);
            _qualityDict.Add(public_config.ITEM_QUALITY_PURPLE, LangEnum.PET_QUALITY_PURPLE);
            _qualityDict.Add(public_config.ITEM_QUALITY_ORANGE, LangEnum.PET_QUALITY_ORANGE);
            _qualityDict.Add(public_config.ITEM_QUALITY_GOLDEN, LangEnum.PET_QUALITY_GOLDEN);

            foreach (int key in XMLManager.pet_quality.Keys)
            {
                pet_quality petQuality = XMLManager.pet_quality[key];
                AddPetIdDict(petQuality);
                AddPetSkllDict(petQuality);
            }

            foreach (PetSkillList skillList in _petSkillDict.Values)
            {
                skillList.Sort();
                skillList.FillPetSkillList();
            }
        }

        private static void AddPetIdDict(pet_quality petQuality)
        {
            PetQualityDict petQualityDict;
            if (_petIdDict.ContainsKey(petQuality.__pet_id))
            {
                petQualityDict = _petIdDict[petQuality.__pet_id];
            }
            else
            {
                petQualityDict = new PetQualityDict();
                _petIdDict.Add(petQuality.__pet_id, petQualityDict);
            }
            petQualityDict.Add(petQuality.__quality, petQuality);
        }

        private static void AddPetSkllDict(pet_quality petQuality)
        {
            PetSkillList petSkillList;
            if (_petSkillDict.ContainsKey(petQuality.__pet_id))
            {
                petSkillList = _petSkillDict[petQuality.__pet_id];
            }
            else
            {
                petSkillList = new PetSkillList();
                _petSkillDict.Add(petQuality.__pet_id, petSkillList);
            }
            petSkillList.Add(petQuality);
        }

        public static pet_quality GetPetQualityConf(int pet_quality_id)
        {
            if (XMLManager.pet_quality.ContainsKey(pet_quality_id))
            {
                return XMLManager.pet_quality[pet_quality_id];
            }
            return null;
        }

        public static pet_quality GetPetQualityConf(int petId, int quality)
        {
            return _petIdDict[petId].GetPetStarConf(quality);
        }

        public static bool QualityReachMax(int petId, int quality)
        {
            return !_petIdDict[petId].IsExist(quality + 1);
        }

        public static bool CurrentLevelReachMax(int petId, int quality, int level)
        {
            pet_quality petQuality = pet_quality_helper.GetPetQualityConf(petId, quality);
            return level >= petQuality.__level_limit;
        }

        public static int GetQualityAttributeValue(pet_quality petQuality, string strArriId)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petQuality.__attri_addition);
            return int.Parse(dataDict[strArriId]);
        }

        public static LangEnum GetQuality(int quality)
        {
            return _qualityDict[quality];
        }

        public static List<PetSkill> GetPetSkillList(int petId)
        {
            return _petSkillDict[petId].CopyPetSkillList();
        }

        public static int GetPetSkillListCount(int petId)
        {
            return _petSkillDict[petId].petSkillList.Count;
        }

        public static List<int> GetPetSkillIdListByQuality(int petId, int quality)
        {
            return _petSkillDict[petId].GetSkillIdListByQuality(quality);
        }

        public static int GetNameColorId(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__name_color;
        }

        public static string GetPostfixName(pet_quality petQuality)
        {
            string postfixName = "";
            if (petQuality.__postfix_name != 0)
            {
                postfixName = MogoLanguageUtil.GetContent(petQuality.__postfix_name);
            }
            return postfixName;
        }

        private static string GetPostfixName(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return GetPostfixName(petQuality);
        }

        public static int GetLevelLimit(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__level_limit;
        }

        public static int GetModelID(PetInfo petInfo)
        {
            return GetModelID(petInfo.Id, petInfo.Quality);
        }

        public static int GetModelID(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__model;
        }

        public static int GetAIID(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__ai;
        }

        public static int GetBoxRadius(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__box_radius;
        }

        public static int GetHitRadius(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__hit_radius;
        }

        public static List<int> GetLeaderSkill(int petId, int quality)
        {
            List<int> leaderSkillList = new List<int>();
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            List<string> dataList = data_parse_helper.ParseListString(petQuality.__assist_buffs);
            foreach (string value in dataList)
            {
                leaderSkillList.Add(int.Parse(value));
            }
            return leaderSkillList;
        }

        public static float GetScale(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__scale>0f?petQuality.__scale:1f;
        }

        public static int GetPetNameColor(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            return petQuality.__name_color;
        }

        //public static string ChangeNameColor(string nameStr, int petId, int quality)
        //{
        //    pet_quality petQuality = GetPetQualityConf(petId, quality);
        //    int color = petQuality.__name_color;
        //    if (color == 0)
        //    {
        //        color = ColorDefine.COLOR_ID_WHITE;
        //    }
        //    return ColorDefine.GetColorHtmlString(color, nameStr);
        //}

        public static string GetPetQualityName(int petId, int quality)
        {
            string name = pet_helper.GetName(petId);
            string postfix = GetPostfixName(petId, quality);
            return string.Format("{0} {1}", name, postfix);
        }

        public static string GetPetQualityName(PetInfo petInfo)
        {
            return GetPetQualityName(petInfo.Id, petInfo.Quality);
        }

		public static string GetPetQualityString(PetInfo petInfo)
        {
            return GetPetQualityString(petInfo.Id, petInfo.Quality);
        }

        public static string GetPetQualityString(int petId, int quality)
        {
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            string qualityName = MogoLanguageUtil.GetContent((int) GetQuality(petQuality.__name_color));
            string postfixName = "";
            if (petQuality.__postfix_name != 0)
            {
                postfixName = GetPostfixName(petQuality);
            }
            int color = petQuality.__name_color;
            if(color == 0)
            {
                color = ColorDefine.COLOR_ID_WHITE;
            }
            return ColorDefine.GetColorHtmlString(color, string.Format("{0}{1}", qualityName, postfixName));
        }

        public static List<PetSkillDataWrapper> ToPetSkillDataWrapperList(int petId, List<PetSkill> petSkillList, int quality)
        {
            List<PetSkillDataWrapper> wrapperList = new List<PetSkillDataWrapper>();
            for (int i = 0; i < petSkillList.Count; i++)
            {
                PetSkillDataWrapper wrapper = new PetSkillDataWrapper(petId, quality, petSkillList[i]);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        public static List<PetSkillDataWrapper> ToPetSkillDataWrapperList(int petId, List<PetSkill> petSkillList, int quality, int washLevel)
        {
            List<PetSkillDataWrapper> wrapperList = new List<PetSkillDataWrapper>();
            for (int i = 0; i < petSkillList.Count; i++)
            {
                PetSkillDataWrapper wrapper = new PetSkillDataWrapper(petId, quality, petSkillList[i], washLevel);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        public static List<BaseItemData> GetQualityUpCost(int petId, int quality)
        {
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            pet_quality petQuality = GetPetQualityConf(petId, quality);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petQuality.__next_quality_item_cost);
            foreach (string strItemId in dataDict.Keys)
            {
                int itemId = int.Parse(strItemId);
                BaseItemData baseItemData = ItemDataCreator.Create(itemId);
                baseItemData.StackCount = int.Parse(dataDict[strItemId]);
                baseItemDataList.Add(baseItemData);
            }
            return baseItemDataList;
        }

        public static void Init()
        {
            int count = XMLManager.pet_quality.Count;
        }
    }
}
