﻿using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class wild_task_reward_helper
    {
        public static wild_task_reward GetWildTaskReward(int id)
        {
            if (XMLManager.wild_task_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表wild_task_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.wild_task_reward[id];
        }

        public static wild_task_reward GetWildTaskReward()
        {
            foreach (KeyValuePair<int, wild_task_reward> kvp in XMLManager.wild_task_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__player_level);
                if (int.Parse(dataList[0]) <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= int.Parse(dataList[1]))
                {
                    return kvp.Value;
                }
            }
            throw new Exception("配置表wild_task_reward.xml 未找到对应等级的配置项： " + PlayerAvatar.Player.level);
        }
    }
}
