﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class pet_refresh_cost_helper
    {
        public static Dictionary<int, Dictionary<int, Dictionary<int, pet_refresh_cost>>> _dict = 
            new Dictionary<int, Dictionary<int, Dictionary<int, pet_refresh_cost>>>();

        static pet_refresh_cost_helper()
        {
            foreach (pet_refresh_cost petRefreshCost in XMLManager.pet_refresh_cost.Values)
            {
                if (!_dict.ContainsKey(petRefreshCost.__pet_id))
                {
                    _dict.Add(petRefreshCost.__pet_id, new Dictionary<int, Dictionary<int, pet_refresh_cost>>());
                }
                if (!_dict[petRefreshCost.__pet_id].ContainsKey(petRefreshCost.__refresh_level))
                {
                    _dict[petRefreshCost.__pet_id].Add(petRefreshCost.__refresh_level, new Dictionary<int, pet_refresh_cost>());
                }
                _dict[petRefreshCost.__pet_id][petRefreshCost.__refresh_level].Add(petRefreshCost.__refresh_type, petRefreshCost);
            }
        }

        public static pet_refresh_cost GetConfig(int petId, int refreshLevel, int refreshType)
        {
            if (!_dict.ContainsKey(petId))
            {
                Debug.LogError("洗练消耗表不存在id:" + petId);
            }
            if (!_dict[petId].ContainsKey(refreshLevel))
            {
                Debug.LogError("洗练消耗表不存在id:" + petId + "    洗练等级:" + refreshLevel);
            }
            if (!_dict[petId][refreshLevel].ContainsKey(refreshType))
            {
                Debug.LogError("洗练消耗表不存在id:" + petId + "    洗练等级:" + refreshLevel + "    洗练类型:" + refreshType);
            }
            return _dict[petId][refreshLevel][refreshType];
        }

        public static List<BaseItemData> GetCostList(int petId, int refreshLevel, int refreshType)
        {
            pet_refresh_cost petRefreshCost = GetConfig(petId, refreshLevel, refreshType);
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petRefreshCost.__refresh_cost);
            foreach (string itemIdStr in dataDict.Keys)
            {
                BaseItemData baseItemData = ItemDataCreator.Create(int.Parse(itemIdStr));
                baseItemData.StackCount = int.Parse(dataDict[itemIdStr]);
                baseItemDataList.Add(baseItemData);
            }
            return baseItemDataList;
        }

        public static void Init()
        {
            int count = XMLManager.pet_refresh_cost.Count;
        }
    }
}
