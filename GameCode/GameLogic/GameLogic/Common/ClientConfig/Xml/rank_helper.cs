﻿#region 模块信息
/*==========================================
// 文件名：rank_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/15 16:40:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Common.ExtendComponent.GeneralRankingList;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
namespace GameData
{
    public class rank_helper
    {

        private const int HAS_ALREADY_GET_AWARD = 65305;
        private const int NOT_JOIN_ANY_GUILD = 65311;

        public static bool IsAwardRankType(RankType type)
        {
            return (XMLManager.rank_list[(int)type].__has_award == 1);
        }
        
        public static string GetRankAttributeDesc(int rankType)
        {
            int chineseID = XMLManager.rank_list[rankType].__attribute_describe;
            return MogoLanguageUtil.GetContent(chineseID);
        }

        public static string GetRankingListName(int rankType)
        {
            int chineseID = XMLManager.rank_list[rankType].__name;
            return MogoLanguageUtil.GetContent(chineseID);
        }

        public static int GetRankingListLevel(int rankType)
        {
            return XMLManager.rank_list[rankType].__level;
        }

        public static List<int> GetRankCategory()
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            foreach(KeyValuePair<int, rank_list> pair in XMLManager.rank_list)
            {
                dict.Add(pair.Value.__priority, pair.Value.__type);
            }
            SortedDictionary<int, int> sortedDict = new SortedDictionary<int, int>(dict);
            List<int> rankCategoryList = new List<int>();
            foreach(KeyValuePair<int, int> pair in sortedDict)
            {
                rankCategoryList.Add(pair.Value);
            }
            return rankCategoryList;
        }

        public static List<int> GetAwardCategory()
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            foreach (KeyValuePair<int, rank_list> pair in XMLManager.rank_list)
            {
                if (pair.Value.__has_award == 1)
                {
                    dict.Add(pair.Value.__priority, (int)pair.Value.__type);
                }
            }
            SortedDictionary<int, int> sortedDict = new SortedDictionary<int, int>(dict);
            List<int> awardCategoryList = new List<int>();
            foreach(KeyValuePair<int, int> pair in sortedDict)
            {
                awardCategoryList.Add(pair.Value);
            }
            return awardCategoryList;
        }

        public static List<string> GetAllRowNames(int type)
        {
            List<string> allRowNames = new List<string>();
            List<string> dataList = data_parse_helper.ParseListString(XMLManager.rank_list[type].__title);
            foreach (string s in dataList)
            {
                allRowNames.Add(MogoLanguageUtil.GetContent(int.Parse(s)));
            }
            return allRowNames;
        }

        public static string GetRankingDesc(int type)
        {
            int describe_id = XMLManager.rank_list[type].__rank_describe;
            return MogoLanguageUtil.GetContent(describe_id);
        }

        public static List<AwardItemData> GetAllAwardData(int type)
        {
            List<AwardItemData> awardDataList = new List<AwardItemData>();
            foreach (KeyValuePair<int, rank_rewards> p in XMLManager.rank_rewards)
            {
                int playerLevel = PlayerAvatar.Player.level;
                List<string> dataList = data_parse_helper.ParseListString(p.Value.__world_level);
                if (p.Value.__type == type && playerLevel >= int.Parse(dataList[0]) && playerLevel <= int.Parse(dataList[1]))
                {
                    item_reward itemReward = item_reward_helper.GetItemReward(p.Value.__rewards);
                    List<BaseItemData> rewardList = item_reward_helper.GetItemDataList(itemReward, PlayerAvatar.Player.vocation);
                    List<string> dataList2 = data_parse_helper.ParseListString(p.Value.__rank);
                    awardDataList.Add(new AwardItemData(type, p.Key, int.Parse(dataList2[0]), int.Parse(dataList2[1]), rewardList));
                }
            }
            return awardDataList;
        }

        private static List<ItemData> ConvertKeyValuePairToItemDataList(List<KeyValuePair<int, int>> rewardList)
        {
            List<ItemData> rewardItemDataList = new List<ItemData>();
            for (int i = 0; i < rewardList.Count; i++)
            {
                KeyValuePair<int, int> pair = rewardList[i];
                ItemData item = new ItemData(pair.Key);
                item.StackCount = pair.Value;
                rewardItemDataList.Add(item);
            }
            return rewardItemDataList;
        }

        private static List<KeyValuePair<int, int>> ConvertStringDictToIntDict(Dictionary<string, string> dict)
        {
            List<KeyValuePair<int, int>> result = new List<KeyValuePair<int, int>>();
            foreach(KeyValuePair<string,string> p in dict)
            {
                result.Add(new KeyValuePair<int,int>(int.Parse(p.Key), int.Parse(p.Value)));
            }
            return result;
        }

        public static List<CategoryItemData> GetRankingDetailCategoryNames()
        {
            List<CategoryItemData> categoryNames = new List<CategoryItemData>();
            categoryNames.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_BASIC_INFO)));
            categoryNames.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_FIGHT_FORCE_COMPARE)));
            categoryNames.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_GEM)));
            categoryNames.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_RUNE)));
            categoryNames.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_FIGHT_INFO)));
            return categoryNames;
        }
        
        public static List<BaseItemData> GetAwardByRank(int rankType, int rank)
        {
            int playerLevel = PlayerAvatar.Player.level;
            foreach(KeyValuePair<int, rank_rewards> pair in XMLManager.rank_rewards)
            {
                List<string> list = data_parse_helper.ParseListString(pair.Value.__rank);
                List<string> list2 = data_parse_helper.ParseListString(pair.Value.__world_level);
                if (pair.Value.__type == rankType
                    && rank >= int.Parse(list[0])
                    && rank <= int.Parse(list[1])
                    && playerLevel >= int.Parse(list2[0])
                    && playerLevel <= int.Parse(list2[1]))
                {
                    item_reward itemReward = item_reward_helper.GetItemReward(pair.Value.__rewards);
                    return item_reward_helper.GetItemDataList(itemReward, PlayerAvatar.Player.vocation);
                }
            }
            return null;
        }

        private static List<ItemData> GetAwardItemList(Dictionary<string, string> dict)
        {
            List<ItemData> result = new List<ItemData>();
            foreach (KeyValuePair<string, string> p in dict)
            {
                ItemData itemData = new ItemData(int.Parse(p.Key));
                itemData.StackCount = int.Parse(p.Value);
                result.Add(itemData);
            }
            return result;
        }

        public static string GetHasAlreadyGetAward()
        {
            return MogoLanguageUtil.GetContent(HAS_ALREADY_GET_AWARD);
        }

        public static string GetNotJoinAnyGuildString()
        {
            return MogoLanguageUtil.GetContent(NOT_JOIN_ANY_GUILD);
        }

        public static bool IsOperational(RankType type)
        {
            return XMLManager.rank_list[(int)type].__operation == 1;
        }

        public static string GetAwardUpdateType(int type)
        {
            if(XMLManager.rank_list.ContainsKey(type) == false)
            {
                UnityEngine.Debug.LogError("排行榜配置有误，不包含编号为" + type + "的选项");
                return string.Empty;
            }
            rank_list currentRankCfg = XMLManager.rank_list[type];
            int todayDayOfWeek = MogoTimeUtil.GetDayOfWeek((long)Global.serverTimeStampSecond);
            List<string> list = data_parse_helper.ParseListString(currentRankCfg.__update_day);
            List<string> list2 = data_parse_helper.ParseListString(currentRankCfg.__update_hour);
            if (currentRankCfg.__update_day.Contains(todayDayOfWeek.ToString()))
            {
                int index = list.IndexOf(todayDayOfWeek.ToString());
                return list2[index] + ":00";
            }
            else if (list[0] == "7")
            {
                return MogoLanguageUtil.GetContent(65307) + currentRankCfg.__update_hour[0] + ":00";
            }
            return string.Empty;
        }

        public static string GetAwardDesc(int type)
        {
            int chineseId = XMLManager.rank_list[type].__award_describe;
            string template = MogoLanguageUtil.GetContent(chineseId);
            return string.Format(template, GetRankingListName(type), GetAwardUpdateType(type));
        }

        private const int EquipmentFightForce = 1;
        private const int EnchantFightForce = 2;
        private const int StrengthenFightForce = 3;
        private const int EquipSuitFightForce = 4;
        public static string GetEquipDesc(int fightForceType)
        {
            switch (fightForceType)
            {
                case EquipmentFightForce:
                    return MogoLanguageUtil.GetContent(124200);
                case EnchantFightForce:
                    return MogoLanguageUtil.GetContent(124201);
                case StrengthenFightForce:
                    return MogoLanguageUtil.GetContent(124202);
                case EquipSuitFightForce:
                    return MogoLanguageUtil.GetContent(124203);
            }
            return string.Empty;
        }

        public static string GetGetFightForceDesc(int fightForceType)
        {
            switch (fightForceType)
            {
                case EquipmentFightForce:
                    return MogoLanguageUtil.GetContent(124207);
                case EnchantFightForce:
                    return MogoLanguageUtil.GetContent(124208);
                case StrengthenFightForce:
                    return MogoLanguageUtil.GetContent(124209);
                case EquipSuitFightForce:
                    return MogoLanguageUtil.GetContent(124210);
            }
            return string.Empty;
        }
    }
}