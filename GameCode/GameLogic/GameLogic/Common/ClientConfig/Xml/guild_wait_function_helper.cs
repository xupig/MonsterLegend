﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{

    public class guild_wait_function_helper
    {
        private static List<function> activityList;
        private static List<function> baseFunctionList;
        private static List<function> baseFunctionRightList;
        private static List<function> extendFunctionList;
        private static List<function> extendFunctionRightList;

        private static bool _isInit = false;

        public static guild_wait_function GetConfig(int id)
        {
            if (XMLManager.guild_wait_function.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_wait_function.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_wait_function[id];
        }

        public static List<function> GetBaseFunctionList()
        {
            if (_isInit == false)
            {
                InitData();
            }
            return baseFunctionList;
        }

        public static List<function> GetBaseFunctionRightList()
        {
            if (_isInit == false)
            {
                InitData();
            }
            return baseFunctionRightList;
        }

        public static List<function> GetActivityList()
        {
            if (_isInit == false)
            {
                InitData();
            }
            return activityList;
        }

        public static List<function> GetExtendFunctionList()
        {
            if (_isInit == false)
            {
                InitData();
            }
            return extendFunctionList;
        }

        public static List<function> GetExtendFunctionRightList()
        {
            if (_isInit == false)
            {
                InitData();
            }
            return extendFunctionRightList;
        }

        private static void InitData()
        {
            _isInit = true;
            activityList = new List<function>();
            baseFunctionList = new List<function>();
            baseFunctionRightList = new List<function>();
            activityList = new List<function>();
            extendFunctionList = new List<function>();
            extendFunctionRightList = new List<function>();
            foreach (guild_wait_function data in XMLManager.guild_wait_function.Values)
            {
                if (data.__type == 1)
                {
                    baseFunctionList.Add(function_helper.GetFunction(data.__function));
                }
                else if (data.__type == 5)
                {
                    baseFunctionRightList.Add(function_helper.GetFunction(data.__function));
                }
                else if (data.__type == 2)
                {
                    activityList.Add(function_helper.GetFunction(data.__function));
                }
                else if (data.__type == 4)
                {
                    extendFunctionList.Add(function_helper.GetFunction(data.__function));
                }
                else if (data.__type == 6)
                {
                    extendFunctionRightList.Add(function_helper.GetFunction(data.__function));
                }
            }
        }
    }
}
