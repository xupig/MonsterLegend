﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    class equip_recast_helper
    {
        public static bool CanRecast(int equipId)
        {
            if(item_helper.IsEquip(equipId))
            {
                EquipData equipData = ItemDataCreator.Create(equipId) as EquipData;
                foreach (KeyValuePair<int, equip_recast> kvp in XMLManager.equip_recast)
                {
                    if (kvp.Value.__quality == equipData.Quality
                        && kvp.Value.__star == equipData.Star
                        && equipData.UsageVocationRequired == PlayerAvatar.Player.vocation
                        && equipData.Level == kvp.Value.__equip_level)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        /// <summary>
        /// 判断指定装备可不可以重铸
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static bool CanRecast(EquipItemInfo equipItemInfo)
        {
            foreach (KeyValuePair<int, equip_recast> kvp in XMLManager.equip_recast)
            {
                if (kvp.Value.__quality == equipItemInfo.Quality 
                    && kvp.Value.__star == equipItemInfo.Star
                    && equipItemInfo.UsageVocationRequired == PlayerAvatar.Player.vocation
                    && equipItemInfo.Level == kvp.Value.__equip_level)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 根据装备信息(等级，品质，星级)获得配置
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static equip_recast GetRecastConfig(EquipItemInfo equipItemInfo)
        {
            foreach (KeyValuePair<int, equip_recast> kvp in XMLManager.equip_recast)
            {
                if (kvp.Value.__quality == equipItemInfo.Quality && kvp.Value.__star == equipItemInfo.Star && kvp.Value.__equip_level == equipItemInfo.Level)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// 重铸的最基本消耗
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static BaseItemData GetRecastCost(EquipItemInfo equipItemInfo)
        {
            equip_recast config = GetRecastConfig(equipItemInfo);
            if (config != null)
            {
                List<string> list = data_parse_helper.ParseListString(config.__cost);
                int costIndex = equipItemInfo.RecastCount > list.Count - 1 ? list.Count - 1 : equipItemInfo.RecastCount;

                BaseItemData itemData = ItemDataCreator.Create(int.Parse(list[costIndex]));
                List<string> list2 = data_parse_helper.ParseListString(config.__cost_num);
                itemData.StackCount = int.Parse(list2[costIndex]);
                return itemData;
            }
            return null;
        }

        /// <summary>
        /// 重铸材料不足时需要的钻石消耗
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static BaseItemData GetRecastReplaceCost(EquipItemInfo equipItemInfo)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            BaseItemData originalItemData = GetRecastCost(equipItemInfo);
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(originalItemData.Id);
            BaseItemData perItemCost = GetRecastPerItemCost(equipItemInfo);
            BaseItemData diamondData = ItemDataCreator.Create(perItemCost.Id);

            if (itemNum >= originalItemData.StackCount)
            {
                diamondData.StackCount = 0;
            }
            else
            {
                diamondData.StackCount = perItemCost.StackCount * (originalItemData.StackCount - itemNum);
            }
            return diamondData;        
        }

        /// <summary>
        /// 重铸所需消耗材料(材料不足时，包含替换材料)
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static List<BaseItemData> GetRecastCostList(EquipItemInfo equipItemInfo)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            BaseItemData originalItemData = GetRecastCost(equipItemInfo);
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(originalItemData.Id);

            BaseItemData itemData = ItemDataCreator.Create(originalItemData.Id);
            if (itemNum >= originalItemData.StackCount)
            {
                itemData.StackCount = originalItemData.StackCount;
                result.Add(itemData);
                return result;
            }
            if (itemNum != 0)
            {
                itemData.StackCount = itemNum;
                result.Add(itemData);
            }

            BaseItemData diamondData = GetRecastReplaceCost(equipItemInfo);
            result.Add(diamondData);
            return result;
        }

        /// <summary>
        /// 每个重铸材料替换时所需钻石数量
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        private static BaseItemData GetRecastPerItemCost(EquipItemInfo equipItemInfo)
        {
            BaseItemData perItemCost = ItemDataCreator.Create(public_config.MONEY_TYPE_DIAMOND);

            equip_recast config = GetRecastConfig(equipItemInfo);
            if (config != null)
            {
                List<string> list = data_parse_helper.ParseListString(config.__diamond);
                int costIndex = (equipItemInfo.RecastCount > (list.Count - 1)) ? (list.Count - 1) : equipItemInfo.RecastCount;
                perItemCost.StackCount = int.Parse(list[costIndex]);
            }
            return perItemCost;
        }

        /// <summary>
        /// 重铸取消时所需最基本消耗
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static BaseItemData GetRecastCancelItemCost(EquipItemInfo equipItemInfo)
        {
            equip_recast config = GetRecastConfig(equipItemInfo);
            if (config != null)
            {
                List<string> list = data_parse_helper.ParseListString(config.__cancel_cost);
                int costIndex = equipItemInfo.RecastCount > list.Count - 1 ? list.Count - 1 : equipItemInfo.RecastCount;

                BaseItemData itemData = ItemDataCreator.Create(int.Parse(list[costIndex]));
                List<string> list2 = data_parse_helper.ParseListString(config.__cancel_cost_num);
                itemData.StackCount = int.Parse(list2[costIndex]);
                return itemData;
            }
            return null;
        }

        /// <summary>
        /// 重铸取消所需消耗列表(材料不足时，包含替换材料)
        /// </summary>
        /// <param name="equipItemInfo"></param>
        /// <returns></returns>
        public static List<BaseItemData> GetRecastCancelCost(EquipItemInfo equipItemInfo)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            BaseItemData originalItemData = GetRecastCancelItemCost(equipItemInfo);
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(originalItemData.Id);

            BaseItemData itemData = ItemDataCreator.Create(originalItemData.Id);
            if (itemNum >= originalItemData.StackCount)
            {
                itemData.StackCount = originalItemData.StackCount;
                result.Add(itemData);
                return result;
            }
            if (itemNum != 0)
            {
                itemData.StackCount = itemNum;
                result.Add(itemData);
            }
            int perItemCost = 0;

            equip_recast config = GetRecastConfig(equipItemInfo);
            if (config != null)
            {
                List<string> list = data_parse_helper.ParseListString(config.__cancel_diamond);
                int costIndex = equipItemInfo.RecastCount > list.Count - 1 ? list.Count - 1 : equipItemInfo.RecastCount;
                perItemCost = int.Parse(list[costIndex]);
            }

            BaseItemData diamondData = ItemDataCreator.Create(public_config.MONEY_TYPE_BIND_DIAMOND);
            diamondData.StackCount = perItemCost * (originalItemData.StackCount - itemNum);
            result.Insert(0, diamondData);
            return result;
        }
    }
}
