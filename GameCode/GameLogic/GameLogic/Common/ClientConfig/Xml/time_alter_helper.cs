﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class time_alter_helper
    {
        public static time_altar GetConfig(int functionId, int level)
        {
            foreach (KeyValuePair<int, time_altar> kvp in XMLManager.time_altar)
            {
                List<int> dialogList = data_parse_helper.ParseListInt(kvp.Value.__level_range);
                if (kvp.Value.__function_id == functionId)
                {
                    if (dialogList.Count == 1 && dialogList[0] == level)
                    {
                        return kvp.Value;
                    }
                    else if (dialogList.Count == 2 && dialogList[0] <= level && dialogList[1] >= level)
                    {
                        return kvp.Value;
                    }
                }
            }

            LoggerHelper.Error(string.Format("未能在time_altar表中查找到function_id为{0}且等级范围包含{1}的配置", functionId, level));
            return null;
        }

        public static List<int> GetFunctionIdList()
        {
            HashSet<int> result = new HashSet<int>();
            foreach (KeyValuePair<int, time_altar> kvp in XMLManager.time_altar)
            {
                if (result.Contains(kvp.Value.__function_id) == false && kvp.Value.__function_id != 0)
                {
                    result.Add(kvp.Value.__function_id);
                }
            }

            return result.ToList();
        }
    }
}
