﻿#region 模块信息
/*==========================================
// 文件名：player_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/9/12 15:05:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class player_helper
    {

         private static List<int> taskList = new List<int>();

         public static void ShowAllTaskNotice()
         {
             if(taskList.Count>0)
             {
                 CheckSkill();
             }
             taskList.Clear();
         }

         public static void CheckTask(int taskId)
         {
             if (GameSceneManager.GetInstance().CanShowNotice())
             {
                 taskList.Add(taskId);
             }
             else
             {
                 CheckSkill();
             }
         }

        public static void CheckMoney()
        {
            CheckSkill();
        }

        public static void CheckLevel()
        {
            CheckSkill();
        }

        //public static void CheckInformationCanOperation()
        //{
        //    EventDispatcher.TriggerEvent(MainUIEvents.INFORMATION_OPERATION_STATE_CHANGE);
        //}

        //public static bool CanInformationOperation()
        //{
        //    return PlayerDataManager.Instance.SpellData.isSkillProficientUnLock == 1 || PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.Count > 0 || PlayerDataManager.Instance.WingData.newWingList.Count > 0 || PlayerDataManager.Instance.WingData.canTrainList.Count > 0;
        //}

        public static bool CheckSkill()
        {
            Dictionary<int, List<int>> data = PlayerDataManager.Instance.SpellData.canUpgradeSkillDict;
            PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.Clear();

            bool haveSkillUpgrade = false;
            foreach (KeyValuePair<int, List<int>> kvp in data)
            {
                for (int i = 0; i < kvp.Value.Count; i++)
                {
                    spell spell = spell_helper.GetSkill(kvp.Value[i]);
                    spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
                    spell nextSpell = skill_helper.GetSpell(spell.__group, spell.__sp_level + 1);
                    Dictionary<string, string> dataDict = data_parse_helper.ParseMap(spell.__study_cnd);
                    Dictionary<string, string> dataDict2 = data_parse_helper.ParseMap(spell.__study_costs);
                    if (nextSpell != null && PlayerAvatar.Player.CheckLimit<string>(dataDict) == LimitEnum.NO_LIMIT && PlayerAvatar.Player.CheckCostLimit(dataDict2, false) == 0 && GlobalParams.GetProficientOpenLevel(sys.__proficient_group) <= PlayerAvatar.Player.level)
                    {
                        haveSkillUpgrade = true;
                        if (PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.ContainsKey(kvp.Key) == false)
                        {
                            PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.Add(kvp.Key, true);
                        }
                        else
                        {
                            PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict[kvp.Key] = true;
                        }
                    }
                }
            }
            if (haveSkillUpgrade)
            {
                EventDispatcher.TriggerEvent(SpellEvents.SPELL_CAN_UPGRADE);
            }
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_OPERATION_CHANGE);
            return haveSkillUpgrade;
        }
    }
}
