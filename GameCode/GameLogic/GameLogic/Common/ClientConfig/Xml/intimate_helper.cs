﻿#region 模块信息
/*==========================================
// 文件名：intimate_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/23 20:59:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class intimate_helper
    {
        private static Dictionary<int, int[]> degreeDict;

        static intimate_helper()
        {
            degreeDict = new Dictionary<int, int[]>();
            foreach (KeyValuePair<int, intimate> kvp in XMLManager.intimate)
            {
                int[] region = new int[] { 0, 0 };
                List<string> list = data_parse_helper.ParseListString(kvp.Value.__closevalue);
                for (int i = 0; i < list.Count; i++)
                {
                    region[i] = int.Parse(list[i]);
                }
                degreeDict.Add(kvp.Value.__degree, region);
            }
        }


        public static float GetIntimateLevel(int degree)
        {
            if(degree == 0)
            {
                return 0;
            }
            int level = 0;
            foreach(KeyValuePair<int,int[]> kvp in degreeDict)
            {
                if(kvp.Value[0]<=degree && kvp.Value[1]>degree)
                {
                    return kvp.Key-0.5f;
                }
                if(kvp.Value[1] == degree)
                {
                    return kvp.Key;
                }
                level = kvp.Key;
            }
            return level;
        }

        public static int GetIntimateLevelMax(int degree)
        {
             foreach(KeyValuePair<int,int[]> kvp in degreeDict)
            {
                if (kvp.Value[1] >= degree && kvp.Value[1] != 0)
                {
                    return kvp.Value[1];
                }
            }
             return degree;
        }

        public static intimate_item GetIntimateItem(int id)
        {
            if(XMLManager.intimate_item.ContainsKey(id))
            {
                return XMLManager.intimate_item[id];
            }
            return null;
        }

        public static int GetMaxDegreeByDegreeLevel(int degreeLevel)
        {
            return degreeDict[degreeLevel][1];
        }

        public static bool IsReachUpLimit(int degree)
        {
            foreach (KeyValuePair<int, int[]> kvp in degreeDict)
            {
                if (kvp.Value[1] > degree)
                {
                    return false;
                }
            }
            return true;
        }
        
    }
}
