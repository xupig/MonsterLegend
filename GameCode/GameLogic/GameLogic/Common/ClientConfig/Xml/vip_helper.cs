﻿using Common.Data;
using Common.Utils;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class vip_helper
    {
        public static vip GetVipData(int vipId)
        {
            return XMLManager.vip[vipId];
        }

        //返回特权id列表
        public static List<string> GetRightsIdListByVipId(int vipId)
        {
            Dictionary<string, string> rightsBuffer = data_parse_helper.ParseMap(GetVipData(vipId).__vip_rights);
            List<string> rightsIdList = new List<string>();
            foreach (var pair in rightsBuffer)
            {
                string awardTemplate = MogoLanguageUtil.GetContent(pair.Value);
                if (int.Parse(pair.Key) == (int)VipRightType.AccumaltedCharged)
                {
                    rightsIdList.Add(string.Format(awardTemplate, GetVipLevelMinCost(vipId)));
                }
                else
                {
                    rightsIdList.Add(string.Format(awardTemplate, vip_rights_helper.GetVipRights((VipRightType)int.Parse(pair.Key), vipId)));
                }
            }
            List<string> dataList = data_parse_helper.ParseListString(XMLManager.vip[vipId].__vip_privilege);
            foreach (string id in dataList)
            {
                rightsIdList.Add(MogoLanguageUtil.GetContent(id));
            }
            return rightsIdList;
        }

        private static object GetGemCombineMaxLevel(int vipId)
        {
            vip_rights vipRight = XMLManager.vip_rights[2];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipId.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);
        }

        private static object GetMaxReplace(int vipId)
        {
            vip_rights vipRight = XMLManager.vip_rights[3];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipId.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);

        }

        public static List<BaseItemData> GetVipReward(int vipLevel)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            Dictionary<string, string> dict = GetRewardsDictionaryByVipId(vipLevel);
            foreach (var pair in dict)
            {
                BaseItemData data = ItemDataCreator.Create(int.Parse(pair.Key));
                data.StackCount = int.Parse(pair.Value);
                result.Add(data);
            }
            return result;
        }

        public static Dictionary<string, string> GetRewardsDictionaryByVipId(int vipId)
        {
            return data_parse_helper.ParseMap(GetVipData(vipId).__vip_rewards);
        }

        public static string GetVipLevelMinCost(int vipLevel)
        {
            return GetVipLevelCost(vipLevel).Split(',')[0];
        }

        //获取达到指定等级需要的最小充值数
        public static string GetVipLevelCost(int vipLevel)
        {
            vip_rights vipRight = XMLManager.vip_rights[1];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipLevel.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);
        }

        public static string GetWanderlandMaxResetTimes(int vipId)
        {
            vip_rights vipRight = XMLManager.vip_rights[3];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipId.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);
        }

        public static string GetDailyMaxSweepNum(int vipId)
        {
            vip_rights vipRight = XMLManager.vip_rights[4];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipId.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);
        }

        public static string GetCopyMaxResetTimes(int vipId)
        {
            vip_rights vipRight = XMLManager.vip_rights[7];
            Type rightType = typeof(vip_rights);
            string filedName = string.Format("__vip_{0}", vipId.ToString());
            return (string)rightType.GetField(filedName).GetValue(vipRight);
        }
    }
}
