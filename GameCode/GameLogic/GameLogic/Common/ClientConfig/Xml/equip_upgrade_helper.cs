﻿using Common.Data;
using Common.Global;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class equip_upgrade_helper
    {
        static equip_upgrade_helper()
        {

        }

        public static equip_upgrade GetConfig(int subType, int level, int quality, int star)
        {
            foreach (KeyValuePair<int, equip_upgrade> pair in XMLManager.equip_upgrade)
            {
                if (pair.Value.__equip_level == level
                    && pair.Value.__equip_quality == quality
                    && pair.Value.__equip_star == star
                    && pair.Value.__equip_pos == subType)
                {
                    return pair.Value;
                }
            }
            return null;
        }

        public static equip_upgrade GetNextConfig(int subType, int level, int quality, int star)
        {
            foreach (KeyValuePair<int, equip_upgrade> pair in XMLManager.equip_upgrade)
            {
                if (pair.Value.__next_equip_level == level
                    && pair.Value.__next_equip_quality == quality
                    && pair.Value.__next_equip_star == star
                    && pair.Value.__next_equip_pos == subType)
                {
                    return pair.Value;
                }
            }
            return null;
        }

        public static List<ItemData> GetCost(EquipItemInfo equipItemInfo)
        {
            List<ItemData> costList = new List<ItemData>();
            equip_upgrade config = GetConfig((int)equipItemInfo.SubType, equipItemInfo.Level, equipItemInfo.Quality, equipItemInfo.Star);
            if (config != null)
            {
                Dictionary<int, int> costDic = data_parse_helper.ParseDictionaryIntInt(config.__cost);
                foreach (KeyValuePair<int, int> pair in costDic)
                {
                    ItemData item = new ItemData(pair.Key);
                    item.StackCount = pair.Value;
                    costList.Add(item);
                }
            }
            return costList;
        }

        public static string GetLimitContent(EquipItemInfo equipItemInfo)
        {
            equip_upgrade config = GetConfig((int)equipItemInfo.SubType, equipItemInfo.Level, equipItemInfo.Quality, equipItemInfo.Star);
            if (config != null)
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(config.__limit);
                LimitEnum limit = PlayerAvatar.Player.CheckLimit<string>(dict);
                if (limit == LimitEnum.LIMIT_LEVEL)
                {
                    return MogoLanguageUtil.GetContent(114051, dict[((int)limit).ToString()]);
                }
            }
            return string.Empty;
        }

        public static int GetUpgradeEquipId(EquipItemInfo equipItemInfo)
        {
            equip_upgrade config = GetConfig((int)equipItemInfo.SubType, equipItemInfo.Level, equipItemInfo.Quality, equipItemInfo.Star);
            return (int)equipItemInfo.UsageVocationRequired * 100000 
                + (int)equipItemInfo.SubType * 10000 
                + config.__next_equip_quality * 1000 
                + config.__next_equip_level * 10 
                + config.__next_equip_star;
        }

        public static bool CanUpgrade(EquipItemInfo equipItemInfo)
        {
            equip_upgrade config = GetConfig((int)equipItemInfo.SubType, equipItemInfo.Level, equipItemInfo.Quality, equipItemInfo.Star);
            if (config != null)
            {
                return config.__can_upgrade == 1;
            }
            return false;
        }

    }
}
