﻿#region 模块信息
/*==========================================
// 文件名：intimate_item_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/12 21:05:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace GameData
{
    public class intimate_item_helper
    {
        public static int GetIntimate(int itemId)
        {
            if(XMLManager.intimate_item.ContainsKey(itemId))
            {
                return XMLManager.intimate_item[itemId].__value;
            }
            return 0;
        }

    }
}