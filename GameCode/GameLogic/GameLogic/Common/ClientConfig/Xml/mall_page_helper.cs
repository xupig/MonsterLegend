﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/20 17:42:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;

namespace GameData
{
    public class mall_page_helper
    {
        private static bool _initialized = false;

        public static void Initialize()
        {
            if(_initialized == false)
            {
                _initialized = true;
                int count = XMLManager.market_page.Count;
            }
        }

        public static string GetName(int id)
        {
            return MogoLanguageUtil.GetContent(GetMallPage(id).__name);
        }

        public static int GetIcon(int id)
        {
            return GetMallPage(id).__icon;
        }

        public static market_page GetMallPage(int id)
        {
            if(!XMLManager.market_page.ContainsKey(id))
            {
                throw new Exception ("market_page表不存在id：" + id);
            }
            return XMLManager.market_page[id];
        }
    }
}
