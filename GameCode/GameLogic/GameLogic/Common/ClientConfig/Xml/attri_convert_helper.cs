﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;


namespace GameData
{
    public class attri_convert_1_helper
    {

        public static attri_convert_1 GetAttributeConvert(int attributeId)
        {
            if (XMLManager.attri_convert_1.ContainsKey(attributeId))
            {
                return XMLManager.attri_convert_1[attributeId];
            }
            return null;
        }

        public static List<float> GetAttributeRatiosList(attri_convert_1 convert, Vocation vocation)
        {
            List<float> result;
            int vocationId = (int)vocation;
            switch (vocationId)
            {
                case 1:
                    result = data_parse_helper.ParseListFloat(convert.__convert_ratios1);
                    break;
                case 2:
                    result = data_parse_helper.ParseListFloat(convert.__convert_ratios2);
                    break;
                case 3:
                    result = data_parse_helper.ParseListFloat(convert.__convert_ratios3);
                    break;
                case 4:
                    result = data_parse_helper.ParseListFloat(convert.__convert_ratios4);
                    break;
                default:
                    result = new List<float>();
                    break;
            }
            return result;
        }

        public static List<int> GetAttributeIdList(attri_convert_1 convert, Vocation vocation)
        {
            List<int> result;
            int vocationId = (int)vocation;
            switch (vocationId)
            {
                case 1:
                    result = data_parse_helper.ParseListInt(convert.__convert_ids1);
                    break;
                case 2:
                    result = data_parse_helper.ParseListInt(convert.__convert_ids2);
                    break;
                case 3:
                    result = data_parse_helper.ParseListInt(convert.__convert_ids3);
                    break;
                case 4:
                    result = data_parse_helper.ParseListInt(convert.__convert_ids4);
                    break;
                default:
                    result = new List<int>();
                    break;
            }
            return result;
        }

    }

    public class attri_convert_2_helper
    {

        public static attri_convert_2 GetAttributeConvert(int attributeId)
        {
            if (XMLManager.attri_convert_2.ContainsKey(attributeId))
            {
                return XMLManager.attri_convert_2[attributeId];
            }
            return null;
        }

        public static int GetConvertAttributeId(attri_convert_2 convert)
        {
            return convert.__convert_id;
        }

        public static float GetConvertAttributeValue(attri_convert_2 convert, float value)
        {
            float result = 0;
            float x = value;
            float arg2 = convert.__arg2;
            float arg3 = convert.__arg3;
            float arg4 = convert.__arg4;
            float arg5 = convert.__arg5;
            float arg6 = convert.__arg6;
            float arg7 = convert.__arg7;
            if (value < convert.__arg1)
            {
                result = x * arg2 / arg3;
            }
            else
            {
                result = x / (arg4 * x / arg5 + arg6 / arg7);
            }
            return result;
        }
    }
}
