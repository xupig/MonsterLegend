﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/15 16:56:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using UnityEngine;
using GameMain.Entities;
using Common.ServerConfig;
using GameMain.GlobalManager;

namespace GameData
{
    public class DreamlandLevelReward
    {
        public Dictionary<int, dreamland_explore_rewards> LevelDict = new Dictionary<int,dreamland_explore_rewards>();

        public void AddLevel(int levelMin, int levelMax, dreamland_explore_rewards reward)
        {
            for (int i = levelMin; i <= levelMax; i++)
            {
                LevelDict.Add(i, reward);
            }
        }
    }

    public class dreamland_explore_reward_helper
    {
        public static Dictionary<int, DreamlandLevelReward> _dreamlandIdDict = new Dictionary<int, DreamlandLevelReward>();

        static dreamland_explore_reward_helper()
        {
            foreach (dreamland_explore_rewards reward in XMLManager.dreamland_explore_rewards.Values)
            {
                if (reward.__id == 0) continue;
                DreamlandLevelReward levelReward = GetDreamlandLevelReward(reward.__land_id);
                List<string> dataList = data_parse_helper.ParseListString(reward.__level_range);
                levelReward.AddLevel(int.Parse(dataList[0]), int.Parse(dataList[1]), reward);
            }
        }

        public static DreamlandLevelReward GetDreamlandLevelReward(int dreamlandId)
        {
            if (_dreamlandIdDict.ContainsKey(dreamlandId))
            {
                return _dreamlandIdDict[dreamlandId];
            }
            DreamlandLevelReward reward = new DreamlandLevelReward();
            _dreamlandIdDict.Add(dreamlandId, reward);
            return _dreamlandIdDict[dreamlandId];
        }

        private static dreamland_explore_rewards GetConfig(int id)
        {
            return XMLManager.dreamland_explore_rewards[id];
        }

        private static dreamland_explore_rewards GetLevelReward(int dreamlandId, int level)
        {
            return _dreamlandIdDict[dreamlandId].LevelDict[level];
        }

        public static List<BaseItemData> GetBaseItemDataList(int dreamlandId, int level, int quality, int station)
        {
            dreamland_explore_rewards reward = GetLevelReward(dreamlandId, level);
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            float qualityAddtionRate = dreamland_explore_quality_helper.GetRewardAdditionRate(dreamlandId, quality);
            float stationAddtionRate = dreamland_explore_station_helper.GetAdditionRate(dreamlandId, station);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(reward.__rewards);
            foreach (string itemId in dataDict.Keys)
            {
                BaseItemData baseItemData = ItemDataCreator.Create(int.Parse(itemId));
                int num = int.Parse(dataDict[itemId]);

                baseItemData.StackCount = (int)Mathf.Floor(num * (1 + qualityAddtionRate) * (1 + stationAddtionRate));
                baseItemDataList.Add(baseItemData);
            }
            return baseItemDataList;
        }

        public static List<BaseItemData> AttachGuildAddition(List<BaseItemData> baseItemDataList)
        {
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                if (PlayerAvatar.Player.guild_id != 0)
                {
                    if (baseItemDataList[i].Id == public_config.MONEY_TYPE_GOLD)
                    {
                        baseItemDataList[i].StackCount = (int)Mathf.Floor((PlayerDataManager.Instance.GuildData.EffectData.AddDreamLandGoldPercent / 10000.0f + 1) * baseItemDataList[i].StackCount);
                    }
                    else if (baseItemDataList[i].Id == public_config.ITEM_SPECIAL_TYPE_EXP)
                    {
                        baseItemDataList[i].StackCount = ((int)Mathf.Floor((PlayerDataManager.Instance.GuildData.EffectData.AddDreamLandExpPercent / 10000.0f + 1) * baseItemDataList[i].StackCount));
                    }
                }

            }
            return baseItemDataList;
        }

        public static BaseItemData GetTreasureBoxBaseItemData(int dreamlandId, int level)
        {
            
            dreamland_explore_rewards reward = GetLevelReward(dreamlandId, level);
            if (reward.__item_reward != 0)
            {
                List<BaseItemData> itemBaseList =
                    item_reward_helper.GetItemDataList(item_reward_helper.GetItemReward(reward.__item_reward), PlayerAvatar.Player.vocation);
                if (itemBaseList != null)
                {
                    return itemBaseList[0];
                }
            }
            return null;
        }
    }
}
