﻿#region 模块信息
/*==========================================
// 文件名：evil_mission_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/7 16:23:07
// 描述说明：
// 其他：
//==========================================*/
#endregion


using Common.Global;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class evil_mission_helper
    {
        private static int worldLevel;
        private const int DEMONGATE_ACTIVITY = 1;
        private static HashSet<int> instanceIdNormalSet;
        private static HashSet<int> instanceIdScoreSet;
        private static bool init = false;

        private static HashSet<int> InstanceIdNormolSet
        {
            get
            {
                if (instanceIdNormalSet == null)
                {
                    InitInstanceIdSet();
                }
                return instanceIdNormalSet;
            }
        }

        private static HashSet<int> InstanceIdScoreSet
        {
            get
            {
                if (instanceIdScoreSet == null)
                {
                    InitInstanceIdSet();
                }
                return instanceIdScoreSet;
            }
        }

        private static void InitInstanceIdSet()
        {
            if (init == true)
            {
                return;
            }
            instanceIdNormalSet = new HashSet<int>();
            instanceIdScoreSet = new HashSet<int>();
            foreach (KeyValuePair<int, evil_mission> pair in XMLManager.evil_mission)
            {
                List<string> dataList = data_parse_helper.ParseListString(pair.Value.__inst_id);
                instanceIdScoreSet.Add(int.Parse(dataList[0]));
                instanceIdNormalSet.Add(int.Parse(dataList[1]));   
            }
            init = true;
        }

        public static bool InstanceIsDemonGateInstance(int instanceId)
        {
            return InstanceIdNormolSet.Contains(instanceId) || InstanceIdScoreSet.Contains(instanceId);
        }

        public static bool IsDemonGateNormalType(int instanceId)
        {
            return InstanceIdNormolSet.Contains(instanceId);
        }

        public static bool IsDemonGateScoreType(int instanceId)
        {
            return InstanceIdScoreSet.Contains(instanceId);
        }

        public static bool IsNPCShow(int npcId)
        {
            if (npcId == DemonGateManager.Instance.GetNPCId())
            {
                return true;
            }
            return false;
        }


        public static int GetNormalInstanceId(int playerLevel)
        {
            int type = MogoTimeUtil.GetDayOfWeek((long)Global.serverTimeStampSecond);
            foreach (KeyValuePair<int, evil_mission> pair in XMLManager.evil_mission)
            {
                List<string> dataList = data_parse_helper.ParseListString(pair.Value.__world_level);
                if (pair.Value.__type == type && PlayerInRange(playerLevel, int.Parse(dataList[0]), int.Parse(dataList[1])))
                {
                    List<string> dataList2 = data_parse_helper.ParseListString(pair.Value.__inst_id);
                    return int.Parse(dataList2[1]);
                }
            }
            return 0;
        }

        private static bool PlayerInRange(int num, int min, int max)
        {
            if (num >= min && num <= max)
            {
                return true;
            }
            return false;
        }

        public static int GetScoreInstanceId(int playerLevel)
        {
            int type = MogoTimeUtil.GetDayOfWeek((long)Global.serverTimeStampSecond);
            foreach (KeyValuePair<int, evil_mission> pair in XMLManager.evil_mission)
            {
                List<string> dataList = data_parse_helper.ParseListString(pair.Value.__world_level);
                if (pair.Value.__type == type && PlayerInRange(playerLevel, int.Parse(dataList[0]), int.Parse(dataList[1])))
                {
                    List<string> dataList2 = data_parse_helper.ParseListString(pair.Value.__inst_id);
                    return int.Parse(dataList2[0]);
                }
            }
            return 0;
        }
    }
}