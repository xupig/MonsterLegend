﻿using Common.Data;
using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public enum VipRightType
    {
        AccumaltedCharged = 1,
        GemCombineMaxLevel = 2,
        ReplaceCount = 3,
        MissionSweep = 4,
        EnergyBuyTimes = 6,
        ResetMissionTimes = 7,
        TeamMissionTimes = 8,
        FreeGiveFriendTims = 9,
        SingleMissionBuyLimits = 10,
        ChestTimes = 11,
        Vipmakegold_times_limit = 12,
        assistant_makegold_times = 13,
        DuelChestTimes = 14,
    }

    public class vip_rights_helper
    {
        public static int GetComposableGemMaxLevel(int vipLevel)
        {
            string levelToken = "3";
            switch(vipLevel)
            {
                case 0:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 1:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 2:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 3:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 4:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 5:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 6:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 7:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 8:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 9:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 10:
                    levelToken = XMLManager.vip_rights[2].__vip_10;
                    break;
                case 11:
                    levelToken = XMLManager.vip_rights[2].__vip_11;
                    break;
                case 12:
                    levelToken = XMLManager.vip_rights[2].__vip_12;
                    break;
                case 13:
                    levelToken = XMLManager.vip_rights[2].__vip_13;
                    break;
                case 14:
                    levelToken = XMLManager.vip_rights[2].__vip_14;
                    break;
                case 15:
                    levelToken = XMLManager.vip_rights[2].__vip_15;
                    break;
            }
            return (int)Convert.ToInt32(levelToken);
        }

        public static int GetVIPMaxBuyCounts(int vipLevel)
        {
            const int SingleMissionBuyCountIndex = 10;
            return int.Parse(GetVipRights(SingleMissionBuyCountIndex, vipLevel));
        }

        public static string GetVipRights(VipRightType rightId, int vipLevel)
        {
            return GetVipRights((int)rightId, vipLevel);
        }

        public static string GetVipRights(int rightId, int vipLevel)
        {
            if (XMLManager.vip_rights.ContainsKey(rightId))
            {
                vip_rights rights = XMLManager.vip_rights[rightId];
                switch (vipLevel)
                {
                    case 0:
                        return rights.__vip_10;
                    case 1:
                        return rights.__vip_10;
                    case 2:
                        return rights.__vip_10;
                    case 3:
                        return rights.__vip_10;
                    case 4:
                        return rights.__vip_10;
                    case 5:
                        return rights.__vip_10;
                    case 6:
                        return rights.__vip_10;
                    case 7:
                        return rights.__vip_10;
                    case 8:
                        return rights.__vip_10;
                    case 9:
                        return rights.__vip_10;
                    case 10:
                        return rights.__vip_10;
                    case 11:
                        return rights.__vip_11;
                    case 12:
                        return rights.__vip_12;
                    case 13:
                        return rights.__vip_13;
                    case 14:
                        return rights.__vip_14;
                    case 15:
                        return rights.__vip_15;
                }
            }
            return string.Empty;
        }
    }
}
