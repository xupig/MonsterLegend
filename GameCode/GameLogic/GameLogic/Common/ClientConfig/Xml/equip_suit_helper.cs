﻿using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using GameMain.GlobalManager;
using Common.Structs;
using GameMain.Entities;

namespace GameData
{
    public class equip_suit_helper
    {
        /// <summary>
        /// attri_effect.xml标准配置id值为1000的时候，去buff.xml表中取value对应的buff设置
        /// </summary>
        public const int BUFF_SWITCH_FLAG = 1000;
        public static string GetName(int suitId)
        {
            return MogoLanguageUtil.GetContent(XMLManager.equip_suit[suitId].__name);
        }

        /// <summary>
        /// 获取套装部件总数
        /// </summary>
        /// <param name="suitId"></param>
        /// <returns></returns>
        public static int GetSuitPartCount(int suitId)
        {
            equip_suit suit = GetSuit(suitId);
            //TODO 
            string keyToken = string.Empty;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(suit.__attrs);
            foreach (string key in dataDict.Keys)
            {
                keyToken = key;
            }
            return Convert.ToInt32(keyToken);
        }

        /// <summary>
        /// 获取装备的套装部件数量
        /// </summary>
        /// <param name="suitId"></param>
        /// <returns></returns>
        public static int GetEquipedSuitPartCount(int suitId)
        {
            int result = 0;
            Dictionary<int, BaseItemInfo> dict = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            foreach(BaseItemInfo itemInfo in dict.Values)
            {
                EquipItemInfo equipInfo = itemInfo as EquipItemInfo;
                if(equipInfo.SuitId == suitId)
                {
                    result++;
                }
            }
            return result;
        }

        public static List<int> GetEquipedSuitPartList(int suitEffectId)
        {
            List<int> result = new List<int>();
            Dictionary<int, BaseItemInfo> dict = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            foreach (BaseItemInfo itemInfo in dict.Values)
            {
                EquipItemInfo equipInfo = itemInfo as EquipItemInfo;
                if (equipInfo.SuitId == suitEffectId)
                {
                    result.Add(equipInfo.GridPosition);
                }
            }
            return result;
        }

        public static Dictionary<int, int> GetSuitEffectDict(int suitId)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(GetSuit(suitId).__attrs);
            foreach (var pair in dataDict)
            {
                result.Add(int.Parse(pair.Key), int.Parse(pair.Value));
            }
            return result;
        }

        /// <summary>
        /// 当没有满足的效果id时返回-1，有时返回效果id
        /// </summary>
        /// <param name="suitEffectId"></param>
        /// <returns></returns>
        public static int GetEffectId(int suitEffectId, int equipedBodyCount)
        {
            Dictionary<int, int> effectDic = GetSuitEffectDict(suitEffectId);
            List<int> effectList = effectDic.Keys.ToList();
            effectList.Sort();
            for (int i = 0; i < effectList.Count; i++)
            {
                if (effectList[i] > equipedBodyCount && i > 0)
                {
                    return effectDic[effectList[i - 1]];
                }
                else if (effectList[i] > equipedBodyCount && i == 0)
                {
                    return -1;
                }
            }
            return effectDic[effectList[effectList.Count - 1]];
        }

        public static int GetFightForce(int suitEffectId, int equipedBodyCount)
        {
            int fightForce = 0;
            Dictionary<int, int> effectDic = GetSuitEffectDict(suitEffectId);
            List<int> effectList = effectDic.Keys.ToList();
            effectList.Sort();
            for (int i = 0; i < effectList.Count; i++)
            {
                if (effectList[i] <= equipedBodyCount)
                {
                    fightForce += fight_force_helper.GetFightForce(effectDic[effectList[i]], PlayerAvatar.Player.vocation);
                }
            }
            return fightForce;
        }

        public static List<int> GetSuitPartCountList(int suitId)
        {
            List<int> result = new List<int>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(GetSuit(suitId).__attrs);
            foreach (string key in dataDict.Keys)
            {
                result.Add(Convert.ToInt32(key));
            }
            return result;
        }

        public static List<int> GetSuitEffectIdList(int suitId)
        {
            List<int> result = new List<int>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(GetSuit(suitId).__attrs);
            foreach (string value in dataDict.Values)
            {
                result.Add(Convert.ToInt32(value));
            }
            return result;
        }

        public static equip_suit GetSuit(int suitId)
        {
            if (XMLManager.equip_suit.ContainsKey(suitId) == false)
            {
                throw new Exception("equip_suit.xml 未找到配置项 " + suitId);
            }
            return XMLManager.equip_suit[suitId];
        }

        public static string GetSuitName(int suitId)
        {
            return MogoLanguageUtil.GetContent(GetSuit(suitId).__name);
        }

        public static int GetSuitFacedeScore(int suitEffectId)
        {
            return GetSuit(suitEffectId).__facade_score;
        }
    }
}
