﻿#region 模块信息
/*==========================================
// 文件名：rune_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/31 15:29:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class rune_helper
    {
        private static Dictionary<int, Dictionary<int, int>> holeIndex2IdDic;
        public static rune GetRune(int runeId)
        {
            if(XMLManager.rune.ContainsKey(runeId))
            {
                return XMLManager.rune[runeId];
            }
            return null;
        }

        private static List<CategoryItemData> bagList;
        //private static List<CategoryItemData> equipList;
        public static List<CategoryItemData> GetBagList()
        {
            //GetEquipList();
            if (bagList == null)
            {
                bagList = new List<CategoryItemData>();
               
                bagList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(56122), 11400));
                bagList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4337" }), 11407));
                bagList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4338" }), 11404));
                bagList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4339"}), 11408));
                bagList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4340"}), 11405));
            }
            return bagList;
        }

        public static string GetRuneTypeName(int type)
        {
            return (4337+type-1).ToLanguage();
        }

        private static Dictionary<int, CategoryItemData> categoryItemDataDict;

        public static List<CategoryItemData> GetEquipList()
        {
            if(categoryItemDataDict == null)
            {
                categoryItemDataDict = new Dictionary<int, CategoryItemData>();
                categoryItemDataDict.Add(1, CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4337" }), 11407));
                categoryItemDataDict.Add(2, CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4338" }), 11404));
                categoryItemDataDict.Add(3, CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4339" }), 11408));
                categoryItemDataDict.Add(4, CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(new List<string>() { "4340" }), 11405));
            }
            HashSet<int> holeSet = new HashSet<int>();
            foreach(KeyValuePair<int,rune_hole> kvp in XMLManager.rune_hole)
            {
                if (kvp.Value.__unlock_level<=PlayerAvatar.Player.level && holeSet.Contains(kvp.Value.__page) == false)
                {
                    //Debug.LogError("holeSet:"+kvp.Value.__page);
                    holeSet.Add(kvp.Value.__page);
                }
            }
            List<CategoryItemData> equipList = new List<CategoryItemData>();
            int minLevel = 0;
            for (int i = 0; i < 4;i++ )
            {
                if (holeSet.Contains(i+1))
                {
                    equipList.Add(categoryItemDataDict[i+1]);
                    minLevel = i + 2;
                }
            }
            if(minLevel<=4)
            {
                equipList.Add(categoryItemDataDict[minLevel]);
            }
            return equipList;
        }

        public static bool CheckSubType(List<string> _subTypes, int subType)
        {
            if (_subTypes == null || _subTypes.Count == 0)
            {
                return true;
            }
            foreach (string type in _subTypes)
            {
                if (int.Parse(type) == subType)
                {
                    return true;
                }
            }
            return false;
        }

        public static rune_hole GetRuneHole(int id)
        {
            if(XMLManager.rune_hole.ContainsKey(id))
            {
                return XMLManager.rune_hole[id];
            }
            return null;
        }

        private static void InitHole()
        {
            if (holeIndex2IdDic == null)
            {
                holeIndex2IdDic = new Dictionary<int, Dictionary<int, int>>();
                foreach (KeyValuePair<int, rune_hole> kvp in XMLManager.rune_hole)
                {
                    if (holeIndex2IdDic.ContainsKey(kvp.Value.__page) == false)
                    {
                        holeIndex2IdDic.Add(kvp.Value.__page, new Dictionary<int, int>());
                    }
                    if (holeIndex2IdDic[kvp.Value.__page].ContainsKey(kvp.Value.__pos) == false)
                    {
                        holeIndex2IdDic[kvp.Value.__page].Add(kvp.Value.__pos, kvp.Value.__id);
                    }
                }
            }
        }

        public static Dictionary<int, Dictionary<int, int>> GetHoleIndex2IdDic()
        {
            InitHole();
            return holeIndex2IdDic;
        }

        public static Dictionary<int, int> GetPageHoleIndex(int page)
        {
            InitHole();
            return holeIndex2IdDic[page];
        }

        private static List<int> allPageList;
        public static List<int> GetALLPageList()
        {
            if (allPageList == null)
            {
                allPageList = new List<int>();
                foreach (KeyValuePair<int, rune> pair in XMLManager.rune)
                {
                    if (!allPageList.Contains(pair.Value.__page) && pair.Value.__page != 0)
                    {
                        allPageList.Add(pair.Value.__page);
                    }
                }
            }
            return allPageList;
        }

        public static int GetMaxUnlockHole()
        {
            InitHole();
            int maxLevel = 0;
            foreach(KeyValuePair<int, Dictionary<int, int>> kvp in holeIndex2IdDic)
            {
               if(kvp.Key!=0)
               {
                   rune_hole hole = XMLManager.rune_hole[kvp.Value[1]];
                   if (hole.__unlock_level <= PlayerAvatar.Player.level)
                   {
                       maxLevel = Math.Max(maxLevel, hole.__page);
                   }
               }
                
            }
            return maxLevel;
        }

        public static Dictionary<int, Dictionary<int, rune_random>> GetWishData()
        {
            Dictionary<int, Dictionary<int, rune_random>> _wishData = new Dictionary<int, Dictionary<int, rune_random>>();
            foreach (KeyValuePair<int, rune_random> kvp in XMLManager.rune_random)
            {
                if (kvp.Value.__avatar_level_area != null)
                {
                    List<string> list = data_parse_helper.ParseListString(kvp.Value.__avatar_level_area);
                    int start = int.Parse(list[0]);
                    int end = int.Parse(list[1]);
                    if (PlayerAvatar.Player.level >= start && PlayerAvatar.Player.level <= end)
                    {
                        if (_wishData.ContainsKey(kvp.Value.__location) == false)
                        {
                            _wishData.Add(kvp.Value.__location, new Dictionary<int, rune_random>());
                        }
                        if (_wishData[kvp.Value.__location].ContainsKey(kvp.Value.__wish_type) == false)
                        {
                            _wishData[kvp.Value.__location].Add(kvp.Value.__wish_type, kvp.Value);
                        }
                    }
                }
            }
            return _wishData;
        }

        public static rune_random GetRuneRandom(int costType,int timeType)
        {
            Dictionary<int, Dictionary<int, rune_random>> _wishData = GetWishData();
            if (_wishData.ContainsKey(costType) && _wishData[costType].ContainsKey(timeType))
            {
                return _wishData[costType][timeType];
            }
            return null;
        }

        public static rune_random GetRuneRandomById(int id)
        {
            if(XMLManager.rune_random.ContainsKey(id))
            {
                return XMLManager.rune_random[id];
            }
            return null;
        }

        public static int GetMaxLevel(rune _runeConfig)
        {
            Dictionary<int, List<int[]>> dict = GetRuneAttributeData(_runeConfig);
            int maxLevel = 1;
            foreach(KeyValuePair<int,List<int[]>> kvp in dict)
            {
                maxLevel = Math.Max(kvp.Key,maxLevel);
            }
            return maxLevel;
        }

        private static Dictionary<int, Dictionary<int, List<int[]>>> _runeAttributeDic = new Dictionary<int, Dictionary<int, List<int[]>>>();
        public static Dictionary<int, List<int[]>> GetRuneAttributeData(rune _runeConfig)
        {
            if(_runeAttributeDic.ContainsKey(_runeConfig.__id) == false)
            {
                Dictionary<int, List<int[]>> _attributeLevelValueDic = new Dictionary<int, List<int[]>>();
                if (_runeConfig.__level_effect != null)
                {
                    foreach (KeyValuePair<string, string> kvp in data_parse_helper.ParseMap(_runeConfig.__level_effect))
                    {
                        int level = int.Parse(kvp.Key);
                        if (_attributeLevelValueDic.ContainsKey(level) == false)
                        {
                            _attributeLevelValueDic.Add(level, new List<int[]>());
                        }
                        int effectId = int.Parse(kvp.Value);
                        attri_effect _effect = XMLManager.attri_effect[effectId];
                        List<string> list = data_parse_helper.ParseListString(_effect.__attri_ids);
                        List<string> list2 = data_parse_helper.ParseListString(_effect.__attri_values);
                        for (int i = 0; i < list.Count; i++)
                        {
                            int id = int.Parse(list[i]);
                            int value = int.Parse(list2[i]);
                            int[] item = new int[] { id, value  };
                            _attributeLevelValueDic[level].Add(item);
                        }
                    }
                }
                _runeAttributeDic.Add(_runeConfig.__id, _attributeLevelValueDic);
            }
            return _runeAttributeDic[_runeConfig.__id];
        }

        public static int GetLevelAttribute(rune rune,int level)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(rune.__level_effect);
            int effectId = int.Parse(dict[level.ToString()]);
            return effectId;
        }

        public static string AppendAttribute(List<int[]> itemList, int curLevel, int level, string _attributeContent)
        {
            if (itemList.Count > 1)
            {
                return AppendTwoAttribute(curLevel, level, itemList[0], itemList[1], _attributeContent);
            }
            else
            {
                return AppendOneAttribute(curLevel, level, itemList[0], _attributeContent);
            }
        }

        private static string AppendOneAttribute(int curLevel, int needLevel, int[] item, string _attributeContent)
        {
            GameData.attri_config _attri = XMLManager.attri_config[item[0]];
            string _attribute = string.Concat(item[1]);
            if (needLevel > curLevel)
            {
                return string.Concat(_attributeContent, "<color=#FFFFFF>", MogoLanguageUtil.GetContent(_attri.__name_id), ": ", _attribute, " ", MogoLanguageUtil.GetString(LangEnum.RUNE_LEVEL_ACTIVE, needLevel), "</color>");
            }
            else
            {
                return string.Concat(_attributeContent, "<color=#00FF00>", MogoLanguageUtil.GetContent(_attri.__name_id), ": ", _attribute, "</color>");
            }
        }
        private static string AppendTwoAttribute(int curLevel, int needLevel, int[] item1, int[] item2, string _attributeContent)
        {
            GameData.attri_config _attri = XMLManager.attri_config[item1[0]];
            string _attribute = string.Concat(item2[1], "~", item1[1]);
            if (needLevel > curLevel)
            {
                return string.Concat(_attributeContent, "<color=#FFFFFF>", MogoLanguageUtil.GetContent(_attri.__name_id).Substring(0,2), ": ", _attribute, " ", MogoLanguageUtil.GetString(LangEnum.RUNE_LEVEL_ACTIVE, needLevel), "</color>");
            }
            else
            {
                return string.Concat(_attributeContent, "<color=#00FF00>", MogoLanguageUtil.GetContent(_attri.__name_id).Substring(0, 2), ": ", _attribute, "</color>");
            }
        }

    }
}
