﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/24 14:53:52
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs;
using Common.Utils;
using UnityEngine;
using GameMain.GlobalManager;

namespace GameData
{
    public class pet_star_helper
    {
        public class PetStarDict
        {
            Dictionary<int, pet_star> _starDict = new Dictionary<int, pet_star>();

            public pet_star GetPetStarConf(int star)
            {
                return _starDict[star];
            }

            public void Add(int star, pet_star petStar)
            {
                _starDict.Add(star, petStar);
            }

            public bool IsExist(int star)
            {
                return _starDict.ContainsKey(star);
            }
        }

        private static Dictionary<int, PetStarDict> _petIdDict = new Dictionary<int, PetStarDict>();

        static pet_star_helper()
        {
            foreach (int key in XMLManager.pet_star.Keys)
            {
                pet_star petStar = GetPetStarConf(key);
                PetStarDict petStarDict;
                if (_petIdDict.ContainsKey(petStar.__pet_id))
                {
                    petStarDict = _petIdDict[petStar.__pet_id];
                }
                else
                {
                    petStarDict = new PetStarDict();
                    _petIdDict.Add(petStar.__pet_id, petStarDict);
                }
                petStarDict.Add(petStar.__star, petStar);
            }
        }

        public static void Init()
        {
            int count = XMLManager.pet_star.Count;
        }

        private static pet_star GetPetStarConf(int id)
        {
            return XMLManager.pet_star[id];
        }

        public static pet_star GetPetStarConf(int petId, int star)
        {
            return _petIdDict[petId].GetPetStarConf(star);
        }

        public static bool ReachMaxStar(int petId, int star)
        {
            return !_petIdDict[petId].IsExist(star + 1);
        }

        /// <summary>
        /// 获取宠物星级配置表的属性
        /// </summary>
        public static List<RoleAttributeData> GetPetStarAttributeDataList(int petId, int star)
        {
            List<RoleAttributeData> attributeDataList = new List<RoleAttributeData>();
            pet_star petStar = GetPetStarConf(petId, star);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petStar.__attri_addition);
            foreach (string strAttriId in dataDict.Keys)
            {
                int attriId = int.Parse(strAttriId);
                RoleAttributeData attributeData = new RoleAttributeData(attriId, int.Parse(dataDict[strAttriId]));
                attributeDataList.Add(attributeData);
            }
            return attributeDataList;
        }

        public static float[] GetGlobalParamArray()
        {
            string strParam = global_params_helper.GetGlobalParam((int)GlobalParamId.petAttri);
            int[] paramArray = global_params_helper.StringToArray<int>(strParam, int.Parse);
            float[] transformedParamArray = new float[paramArray.Length];
            for (int i = 0; i < paramArray.Length; i++)
            {
                transformedParamArray[i] = (float)paramArray[i] / 10000;
            }
            return transformedParamArray;
        }

        public static Dictionary<int, float> GetSkillAdditionDict(int level, int star, int quality, List<int> petSkillIdList)
        {
            Dictionary<int, float> skillAttriAdditionDict = new Dictionary<int, float>();
            for (int i = 0; i < petSkillIdList.Count; i++)
            {
                int petSkillId = petSkillIdList[i];
                if (pet_skill_helper.IsEffectAttriSkill(petSkillId))
                {
                    Dictionary<int, float> configAttrDict = pet_skill_helper.GetAttriAdditionDict(petSkillId, level, star, quality);
                    foreach(int key in configAttrDict.Keys)
                    {
                        if (!skillAttriAdditionDict.ContainsKey(key))
                        {
                            skillAttriAdditionDict.Add(key, 0);
                        }
                        skillAttriAdditionDict[key] += configAttrDict[key];
                    }
                }
            }
            return skillAttriAdditionDict;
        }


        public static int SortByPriority(RoleAttributeData x, RoleAttributeData y)
        {
            if (x.priority < y.priority)
            {
                return -1;
            }
            return 1;
        }

        public static List<IAttribute> ParseUpLowbound(List<RoleAttributeData> list)
        {
            List<IAttribute> resultList = new List<IAttribute>();
            int upIndex = -1;
            int lowIndex = -1;
            for (int i = 0; i < list.Count; i++)
            {
                RoleAttributeData data = list[i];
                resultList.Add(data as IAttribute);
                if (data.attriId == (int) AttriId.damageUpbound)
                {
                    upIndex = i;
                    continue;
                }
                else if (data.attriId == (int) AttriId.damageLowbound)
                {
                    lowIndex = i;
                    continue;
                }
            }

            if ((upIndex != -1) && (lowIndex != -1))
            {
                int remainIndex;
                int removeIndex;
                if (upIndex > lowIndex)
                {
                    removeIndex = upIndex;
                    remainIndex = lowIndex;
                }
                else
                {
                    removeIndex = lowIndex;
                    remainIndex = upIndex;
                }
                resultList.RemoveAt(removeIndex);
                resultList[remainIndex] = new UplowboundAttributeData(list[upIndex], list[lowIndex]) as IAttribute;
            }

            resultList.Sort(RoleAttributeData.SortByPriority);
            return resultList;
        }

        public static ItemData GetStarUpCost(int petId, int star)
        {
            pet_star petStar = GetPetStarConf(petId, star);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(petStar.__next_star_item_cost);
            foreach (string strItemId in dataDict.Keys)
            {
                int itemId = int.Parse(strItemId);
                ItemData itemData = new ItemData(itemId);
                itemData.StackCount = int.Parse(dataDict[strItemId]);
                return itemData;
            }
            return null;
        }
    }
}
