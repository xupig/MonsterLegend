﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;


namespace GameData
{
    public class global_helper
    {
        public static List<int> ConvertToIntList(List<string> list)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(int.Parse(list[i]));
            }
            return result;
        }

        public static List<float> ConvertToFloatList(List<string> list)
        {
            List<float> result = new List<float>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(float.Parse(list[i]));
            }
            return result;
        }

        /// <summary>
        /// 将数字转换为特定长度的按位值数组
        /// </summary>
        /// <param name="value"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static List<int> ConvertIntToDigitalList(int value, uint length = 0)
        {
            List<int> result = new List<int>();
            do
            {
                result.Add(value % 10);
                value /= 10;
            } while (value != 0);
            while (result.Count < length)
            {
                result.Add(0);
            }
            result.Reverse();
            return result;
        }
    }
}
