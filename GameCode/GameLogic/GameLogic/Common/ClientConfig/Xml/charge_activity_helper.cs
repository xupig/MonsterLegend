﻿#region 模块信息
/*==========================================
// 文件名：charge_activity_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/19 9:35:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace GameData
{
    public static class charge_activity_helper
    {
        public static int GetRewardId(int id)
        {
            switch (id)
            {
                case 1:
                    return 44;
                case 2:
                    return 44;
                case 3:
                    return 44;
                case 4:
                    return 44;
            }
            return 0;
        }
    }
}
