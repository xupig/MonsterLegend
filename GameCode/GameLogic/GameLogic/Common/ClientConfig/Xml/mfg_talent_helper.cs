﻿using Common.Utils;
using System.Collections.Generic;
#region 模块信息
/*==========================================
// 文件名：mfg_talent_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/27 15:19:30
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace GameData
{
    public class mfg_talent_helper
    {
        public static string GetTalentName(int talentId)
        {
            if (XMLManager.mfg_talent.ContainsKey(talentId))
            {
                return MogoLanguageUtil.GetContent(XMLManager.mfg_talent[talentId].__name);
            }
            return string.Empty;
        }

        private static Dictionary<int, int> GiftTipTitleDict;
        public static int GetTalentTipTitleTxt(int giftId)
        {
            if (GiftTipTitleDict == null)
            {
                GiftTipTitleDict = new Dictionary<int, int>();
                GiftTipTitleDict.Add(1, 32054);
                GiftTipTitleDict.Add(2, 32055);
                GiftTipTitleDict.Add(3, 32056);
            }
            if (GiftTipTitleDict.ContainsKey(giftId))
            {
                return GiftTipTitleDict[giftId];
            }
            return 0;
        }

        public static string GetTalentDesc(int talentId)
        {
            if (XMLManager.mfg_talent.ContainsKey(talentId))
            {
                return MogoLanguageUtil.GetContent(XMLManager.mfg_talent[talentId].__desc);
            }
            return string.Empty;
        }
    }
}