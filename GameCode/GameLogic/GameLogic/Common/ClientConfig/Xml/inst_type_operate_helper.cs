﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{

    public class inst_type_operate_helper
    {
        public static inst_type_operate GetConfig(ChapterType chapterType)
        {
            if (!XMLManager.inst_type_operate.ContainsKey((int)chapterType))
            {
                throw new Exception("inst_type_operate找不到id:" + (int)chapterType);
            }
            return XMLManager.inst_type_operate[(int)chapterType];
        }

        public static int GetChangeProficientType(ChapterType chapterType)
        {
            return GetConfig(chapterType).__can_change_proficient;
        }

        public static int GetSweepItemId(ChapterType chapterType)
        {
            return GetConfig(chapterType).__mop_up_ticket_id;
        }

        public static int GetTimePriceId(ChapterType chapterType)
        {
            return GetConfig(chapterType).__buy_times_price_id;
        }

        public static int GetDailyTimes(ChapterType chapterType)
        {
            return GetConfig(chapterType).__daily_times;
        }

        public static bool TeamCanCall(ChapterType chapterType)
        {
            return GetConfig(chapterType).__team_can_call == 1;
        }

        public static List<string> GetShowEntityTypeList(ChapterType chapterType)
        {
            inst_type_operate cfg = GetConfig(chapterType);
            List<string> entityTypeName = new List<string>();

            if (cfg != null)
            {
                List<int> entityTypeIdList = data_parse_helper.ParseListInt(cfg.__is_show_title);
                for (int i = 0; i < entityTypeIdList.Count; i++)
                {
                    entityTypeName.Add(XMLManager.monster_type[entityTypeIdList[i]].__type);
                }
            }
            return entityTypeName;
        }

        public static bool IsShowTitle(ChapterType chapterType)
        {
            inst_type_operate cfg = GetConfig(chapterType);
            if (cfg != null)
            {
                ////暂时写成这样
                ////return cfg.__is_show_title == 1;
                return true;
            }
            return false;
        }

        public static bool IsShowWing(ChapterType chapterType)
        {
            return GetConfig(chapterType).__is_show_wing == 1;
        }

        public static bool CanPetFight(ChapterType chapterType)
        {
            return GetConfig(chapterType).__can_pet_fight == 1;
        }

        public static bool CanPk(ChapterType chapterType)
        {
            return GetConfig(chapterType).__can_pk == 1;
        }

		public static bool IsNeedStartTime(ChapterType chapterType)
		{
			return GetConfig(chapterType).__is_need_start_time == 1;
		}

        public static bool CanPlayAgain(ChapterType chapterType)
        {
            return GetConfig(chapterType).__play_again_time != 0;
        }

        public static bool CanRide(ChapterType chapterType)
        {
            return GetConfig(chapterType).__can_ride == 1;
        }

        public static bool CanTouchCamera(ChapterType chapterType)
        {
            return GetConfig(chapterType).__can_touch_camera == 1;
        }
    }

}
