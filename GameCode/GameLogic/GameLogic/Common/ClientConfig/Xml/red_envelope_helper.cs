﻿using Common.Utils;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class red_envelope_helper
    {
        public static red_envelope GetConfig(int id)
        {
            if (XMLManager.red_envelope.ContainsKey(id))
            {
                return XMLManager.red_envelope[id];
            }
            return null;
        }

        public static string GetPrivateChatNotice(int id, int grabNum)
        {
            string noticeDesc = string.Empty;
            if (grabNum == 0)
                return noticeDesc;
            red_envelope config = GetConfig(id);
            List<string> dataList = data_parse_helper.ParseListString(config.__Notice_private);
            if (grabNum <= dataList.Count)
            {
                int languageId = int.Parse(dataList[grabNum - 1]);
                noticeDesc = MogoLanguageUtil.GetContent(languageId);
            }
            return noticeDesc;
        }
    }
}
