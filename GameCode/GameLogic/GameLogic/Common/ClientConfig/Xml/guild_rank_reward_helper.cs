﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_rank_reward_helper
    {

        public static guild_rank_reward GetConfig(int id)
        {
            if (XMLManager.guild_rank_reward.ContainsKey(id) == false)
            {
                throw new Exception("配置表guild_rank_reward.xml 未找到配置项： " + id);
            }
            return XMLManager.guild_rank_reward[id];
        }

        public static int GetRewardId(int rank)
        {
            foreach (guild_rank_reward reward in XMLManager.guild_rank_reward.Values)
            {
                List<int> listInt = data_parse_helper.ParseListInt(reward.__rank);
                if (listInt.Count == 1 && rank == listInt[0])
                {
                    return reward.__reward;
                }
                else if (listInt.Count == 2 && listInt[0] <= rank && rank <= listInt[1])
                {
                    return reward.__reward;
                }
            }
            return 0;
        }

    }
}
