﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.Structs;
using GameMain.Entities;


namespace GameData
{
    public class attri_effect_helper
    {
        /// <summary>
        /// attri_effect.xml标准配置id值为1000的时候，去buff.xml表中取value对应的buff设置
        /// </summary>
        public const int BUFF_SWITCH_FLAG = 1000;
        public static int DAMAGE_LANG_ID = 4318;

        public static List<int> GetAttributeIdList(int effectId)
        {
            attri_effect effect = GetAttriEffect(effectId);
            return data_parse_helper.ParseListInt(effect.__attri_ids);
        }

        public static List<int> GetAttributeValueList(int effectId)
        {
            attri_effect effect = GetAttriEffect(effectId);
            return data_parse_helper.ParseListInt(effect.__attri_values);
        }

        /// <summary>
        /// 特效表中生成特效描述列表
        /// 包括属性Id为160,161连续出现的时候 生成伤害描述的处理
        /// 包括属性Id为1000时，取Buff表中描述的处理
        /// </summary>
        /// <param name="effectId"></param>
        /// <returns></returns>
        public static List<string> GetAttributeDescList(int effectId)
        {
            List<string> result = new List<string>();
            List<int> attriIdList = GetAttributeIdList(effectId);
            List<int> attriValueList = GetAttributeValueList(effectId);
            if (attriIdList.Count != attriValueList.Count)
            {
                throw new Exception(string.Format("特效 {0} 配置项的id和value数量不一致", effectId));
            }
            int index = 0;
            string content = string.Empty;
            while (index < attriIdList.Count)
            {
                int attriId = attriIdList[index];
                if (attriId == (int)AttriId.damageUpbound && attriIdList.Count > (index + 1) && attriIdList[index + 1] == (int)AttriId.damageLowbound)
                {
                    content = string.Format("{0} +{1}~{2}", MogoLanguageUtil.GetContent(DAMAGE_LANG_ID), attriValueList[index + 1].ToString(), attriValueList[index].ToString());
                    index++;
                }
                else if (attriId == (int)AttriId.damageLowbound && attriIdList.Count > (index + 1) && attriIdList[index + 1] == (int)AttriId.damageUpbound)
                {
                    content = string.Format("{0} +{1}~{2}", MogoLanguageUtil.GetContent(DAMAGE_LANG_ID), attriValueList[index].ToString(), attriValueList[index + 1].ToString());
                    index++;
                }
                else if (attriId == BUFF_SWITCH_FLAG)
                {
                    content = buff_helper.GetName(attriValueList[index]);
                }
                else
                {
                    content = string.Format("{0} +{1}", attri_config_helper.GetAttributeName(attriId), attriValueList[index].ToString());
                }
                result.Add(content);
                index++;
            }
            return result;
        }

        public static attri_effect GetAttriEffect(int effectId)
        {
            if(XMLManager.attri_effect.ContainsKey(effectId) == false)
            {
                throw new Exception("配置表 attri_effect.xml 未找到配置项: " + effectId.ToString());
            }
            return XMLManager.attri_effect[effectId];
        }

    }
}
