﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class duel_gamble_helper
    {
        public static duel_gamble GetConfig(int id)
        {
            if (!XMLManager.duel_gamble.ContainsKey(id))
            {
                throw new Exception("duel_gamble找不到id:" + id);
            }
            return XMLManager.duel_gamble[id];
        }

        public static int GetCostItemId(int id)
        {
            return GetConfig(id).__gamble_item;
        }

        public static int[] GetLowHideDuelScoreRate(int id)
        {
            List<string> strList = data_parse_helper.ParseListString(GetConfig(id).__gamble_value_low);
            int[] array = new int[2];
            array[0] = int.Parse(strList[0]);
            array[1] = int.Parse(strList[1]);
            return array;
        }

        public static int[] GetHighHideDuelScoreRate(int id)
        {
            List<string> strList = data_parse_helper.ParseListString(GetConfig(id).__gamble_value_high);
            int[] array = new int[2];
            array[0] = int.Parse(strList[0]);
            array[1] = int.Parse(strList[1]);
            return array;
        }

        public static int[] GetOdds(bool hideScoreIsHigher, int id)
        {
            int[] array;
            if (hideScoreIsHigher)
            {
                array = duel_gamble_helper.GetHighHideDuelScoreRate(id);
            }
            else
            {
                array = duel_gamble_helper.GetLowHideDuelScoreRate(id);
            }
            return array;
        }
    }
}
