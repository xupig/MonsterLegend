﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/24 14:54:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Utils;
using UnityEngine;
using Common.ExtendComponent;
using Common.Global;

namespace GameData
{
    public class pet_helper
    {
        public static pet GetConfig(int id)
        {
            if (!XMLManager.pet.ContainsKey(id))
            {
                throw new Exception("配置表 pet.xml 未包含项 " + id.ToString());
            }
            return XMLManager.pet[id];
        }

        public static string GetName(int id)
        {
            pet pet = GetConfig(id);
            return GetName(pet);
        }

        public static List<CategoryItemData> GetSubTypeNameList()
        {
            List<CategoryItemData> dataList = new List<CategoryItemData>();
            dataList.Add(CategoryItemData.GetCategoryItemData(LangEnum.PET_ALL.ToLanguage()));
            dataList.Add(CategoryItemData.GetCategoryItemData(LangEnum.PET_PHYSICS_ATTACK.ToLanguage()));
            dataList.Add(CategoryItemData.GetCategoryItemData(LangEnum.PET_MAGIC_ATTACK.ToLanguage()));
            dataList.Add(CategoryItemData.GetCategoryItemData(LangEnum.PET_DEFENCE.ToLanguage()));
            //dataList.Add(CategoryItemData.GetCategoryItemData(LangEnum.PET_ASSIST.ToLanguage()));
            return dataList;
        }

        public static string GetName(pet pet)
        {
            return MogoLanguageUtil.GetContent(pet.__name);
        }

        public static PetSubTypeDefine GetSubType(int id)
        {
            pet pet = GetConfig(id);
            return GetSubType(pet);
        }

        public static PetSubTypeDefine GetSubType(pet pet)
        {
            return (PetSubTypeDefine)pet.__sub_type;
        }

        public static ItemData GetItemCost(int id)
        {
            return GetItemCost(GetConfig(id));
        }

        public static ItemData GetItemCost(pet pet)
        {
            ItemData itemData = null;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(pet.__item_cost);
            foreach (string key in dataDict.Keys)
            {
                itemData = new ItemData(int.Parse(key));
                itemData.StackCount = int.Parse(dataDict[key]);
                break;
            }
            return itemData;
        }

        public static ItemData GetExchangeItemData(pet pet)
        {
            ItemData itemData = null;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(pet.__item_exchange);
            foreach (string key in dataDict.Keys)
            {
                itemData = new ItemData(int.Parse(key));
                itemData.StackCount = int.Parse(dataDict[key]);
                break;
            }
            return itemData;
        }

        public static int GetIcon(int id)
        {
            return GetIcon(GetConfig(id));
        }

        public static int GetHitRate(int id)
        {
            return GetConfig(id).__hit_rate;
        }

        public static int GetIcon(pet _pet)
        {
            return _pet.__icon;
        }

        public static int GetPetInitQuality(int id)
        {
            return GetConfig(id).__init_quality;
        }

        public static bool GetCanShow(int id)
        {
            return GetConfig(id).__is_show != 1;
        }

        public static int GetSpeakVoiceId(int id)
        {
            List<string> speakList = data_parse_helper.ParseListString(GetConfig(id).__speak_voice);
            int speackVoiceId = int.Parse(speakList[MogoMathUtils.Random(0, speakList.Count)]);
            return speackVoiceId;
        }

        public static int GetRandomSpeechID(int petId, int id)
        {
            if (XMLManager.pet.ContainsKey(petId))
            {
                Dictionary<int, List<int>> speechList = data_parse_helper.ParseDictionaryIntListInt(XMLManager.pet[petId].__speak);
                if (speechList == null || !speechList.ContainsKey(id) || speechList[id].Count == 0)
                {
                    return -1;
                }
                int random = MogoMathUtils.Random(0, speechList[id].Count);
                return speechList[id][random];
            }
            return -1;
        }

        public static Dictionary<int, float> GetFightPetConvertRate(int id)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(GetConfig(id).__convert_player_rate);
            Dictionary<int, float> result = new Dictionary<int, float>();
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                result.Add(int.Parse(kvp.Key), float.Parse(kvp.Value) / 10000);
            }
            return result;
        }
    }
}
