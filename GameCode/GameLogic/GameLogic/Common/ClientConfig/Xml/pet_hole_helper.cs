﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/27 11:01:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameMain.Entities;
using UnityEngine;

namespace GameData
{
    public class pet_hole_helper
    {
        public const int PET_TYPE_ASSIST = 1;
        public const int PET_TYPE_FIGHT = 2;

        private static Dictionary<int, pet_hole> _assistPetHoleDict = new Dictionary<int, pet_hole>();
        private static Dictionary<int, pet_hole> _fightPetHoleDict = new Dictionary<int, pet_hole>();

        static pet_hole_helper()
        {
            foreach (pet_hole petHole in XMLManager.pet_hole.Values)
            {
                if (petHole.__type == PET_TYPE_ASSIST)
                {
                    _assistPetHoleDict.Add(petHole.__index, petHole);
                }
                else if (petHole.__type == PET_TYPE_FIGHT)
                {
                    _fightPetHoleDict.Add(petHole.__index, petHole);
                }
            }
        }

        public static void Init()
        {
            int count = XMLManager.pet_hole.Count;
        }

        public static List<pet_hole> GetFightingPetHoldList()
        {
            List<pet_hole> petHoleList = new List<pet_hole>();
            foreach (pet_hole petHole in _fightPetHoleDict.Values)
            {
                petHoleList.Add(petHole);
            }
            petHoleList.Sort(SortByIndex);
            return petHoleList;
        }

        public static List<pet_hole> GetAssistPetHoldList()
        {
            List<pet_hole> petHoleList = new List<pet_hole>();
            foreach (pet_hole petHole in _assistPetHoleDict.Values)
            {
                petHoleList.Add(petHole);
            }
            petHoleList.Sort(SortByIndex);
            return petHoleList;
        }

        private static int SortByIndex(pet_hole x, pet_hole y)
        {
            if (x.__index < y.__index)
            {
                return -1;
            }
            return 1;
        }

        public static pet_hole GetPetHole(int id)
        {
            return XMLManager.pet_hole[id];
        }

        public static pet_hole GetPetHole(int petType, int position)
        {
            foreach (pet_hole petHole in XMLManager.pet_hole.Values)
            {
                if (petHole.__type == petType && petHole.__index == position)
                {
                    return petHole;
                }
            }
            return null;
        }

        public static string GetDesc(int id)
        {
            pet_hole petHole = GetPetHole(id);
            return MogoLanguageUtil.GetContent(petHole.__desc);
        }

        public static string GetDesc(pet_hole petHole)
        {
            return MogoLanguageUtil.GetContent(petHole.__desc);
        }

        public static bool IsFightHoleLock(int index)
        {
            pet_hole petHole = _fightPetHoleDict[index];
            return IsLock(petHole);
        }

        public static int GetFightLockLevel(int index)
        {
            pet_hole petHole = _fightPetHoleDict[index];
            return petHole.__avatar_level;
        }

        public static bool IsLock(pet_hole petHole)
        {
            if (petHole.__avatar_level > PlayerAvatar.Player.level)
            {
                return true;
            }
            return false;
        }

        public static int GetCanFigtingPetNum()
        {
            int count = 0;
            foreach (pet_hole petHole in _fightPetHoleDict.Values)
            {
                if (PlayerAvatar.Player.level >= petHole.__avatar_level)
                {
                    count++;
                }
            }
            return count;
        }

        public static int GetCanAssistPetNum()
        {
            int count = 0;
            foreach (pet_hole petHole in _assistPetHoleDict.Values)
            {
                if (PlayerAvatar.Player.level >= petHole.__avatar_level)
                {
                    count++;
                }
            }
            return count;
        }

        public static float GetConvertRate(int petType, int position)
        {
            pet_hole petHole = GetPetHole(petType, position);
            return ((float)petHole.__convert_rate) / 10000;
        }
    }
}
