﻿#region 模块信息
/*==========================================
// 文件名：minimap_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/19 15:47:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class minimap_helper
    {
        private static Dictionary<string, minimap> spaceMinimapDict;
        public static minimap GetMinimapByMapId(string spaceName)
        {
            if (spaceMinimapDict == null)
            {
                spaceMinimapDict = new Dictionary<string, minimap>();
                foreach (KeyValuePair<int, minimap> kvp in XMLManager.minimap)
                {
                    spaceMinimapDict.Add(kvp.Value.__spacename, kvp.Value);
                }
            }
            if (spaceMinimapDict.ContainsKey(spaceName) == false)
            {
                return null;
            }
            return spaceMinimapDict[spaceName];
        }
    }

    public class minimap_menu_helper
    {
        private static Dictionary<int, List<minimap_menu>> minimapDict;
        public static List<minimap_menu> GetMinimapMenu(int mapId)
        {
            if (minimapDict == null)
            {
                minimapDict = new Dictionary<int, List<minimap_menu>>();
                foreach (KeyValuePair<int, minimap_menu> kvp in XMLManager.minimap_menu)
                {
                    if (minimapDict.ContainsKey(kvp.Value.__map) == false)
                    {
                        minimapDict.Add(kvp.Value.__map, new List<minimap_menu>());
                    }
                    minimapDict[kvp.Value.__map].Add(kvp.Value);
                }
                foreach (KeyValuePair<int, List<minimap_menu>> kvp in minimapDict)
                {
                    kvp.Value.Sort(SortFun);
                }
            }
            if (minimapDict.ContainsKey(mapId) == false)
            {
                throw new Exception("表minimap_menu 找不到地图Id:" + mapId+" 的配置项");
            }
            return minimapDict[mapId];
        }

        private static int SortFun(minimap_menu x, minimap_menu y)
        {
            return x.__sort - y.__sort;
        }

        /*
        public static minimap_menu GetMinimapMenuData(int mapId, SmallMapItemType type)
        {
            List<minimap_menu> menuDataList = minimap_menu_helper.GetMinimapMenu(mapId);
            foreach (minimap_menu menuData in menuDataList)
            {
                if ((SmallMapItemType)menuData.__type == type)
                {
                    return menuData;
                }
            }
            return null;
        }*/
    }

    public class field_play_introduce_helper
    {
        private static Dictionary<int, List<field_play_introduce>> _playIntroduceDict;
        public static List<field_play_introduce> GetPlayIntroduceMenu(int mapId)
        {
            if (_playIntroduceDict == null)
            {
                _playIntroduceDict = new Dictionary<int, List<field_play_introduce>>();
                foreach (KeyValuePair<int, field_play_introduce> kvp in XMLManager.field_play_introduce)
                {
                    if (_playIntroduceDict.ContainsKey(kvp.Value.__map) == false)
                    {
                        _playIntroduceDict.Add(kvp.Value.__map, new List<field_play_introduce>());
                    }
                    _playIntroduceDict[kvp.Value.__map].Add(kvp.Value);
                }
            }
            if (_playIntroduceDict.ContainsKey(mapId) == false)
            {
                throw new Exception("表field_play_introduce 找不到地图Id:" + mapId + " 的配置项");
            }
            return _playIntroduceDict[mapId];
        }

    }
}
