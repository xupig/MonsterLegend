﻿using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_helper
    {
        public static string GetGuildPositionContent(int position)
        {
            return MogoLanguageUtil.GetContent(74626 + (int)position - 1);
        }

        public static bool HasApplyJoinInfo()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.examine) == false) return false;
            return PlayerDataManager.Instance.GuildData.GuildApplyJoinInfoList.apply_join_info.Count > 0;
        }


        public static bool CanUpgrade()
        {
            MyGuildData myGuildData = PlayerDataManager.Instance.GuildData.MyGuildData;
            if (myGuildData.Level >= guild_level_helper.GetMaxLevel())
            {
                return false;
            }
            if (PlayerDataManager.Instance.GuildData.GetMyPosition() > guild_rights_helper.GetGuildRightsPositionById(GuildRightId.upgrade))
            {
                return false;
            }
            int goldId = guild_level_helper.GetUpgradeCostCurrency(myGuildData.Level + 1);
            int value = guild_level_helper.GetUpgradeCostValue(myGuildData.Level + 1);
            if (item_helper.GetMyMoneyCountByItemId(goldId) < value)
            {
                return false;
            }
            if (guild_spell_helper.IsMeetCondition(guild_level_helper.GetGuildUpgradeCondition(myGuildData.Level + 1)) == false)
            {
                return false;
            }
            return true;
        }

        public static bool CanReward()
        {
            if (function_helper.IsFunctionOpen(1802) == false)
            {
                return false;
            }
            foreach (GuildAchievementData data in PlayerDataManager.Instance.GuildData.GetAllAchievementList())
            {
                if (data.Status == public_config.ACHIEVEMENT_STATE_ACCOMPLISH)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool CanLearnSkill()
        {
            if (function_helper.IsFunctionOpen(1802) == false)
            {
                return false;
            }
            if (PlayerDataManager.Instance.GuildData.GetMyPosition() > guild_rights_helper.GetGuildRightsPositionById(GuildRightId.spell))
            {
                return false;
            }
            foreach (GuildSkillData data in PlayerDataManager.Instance.GuildData.GetSkillList())
            {
                if (CanLearnSkill(data))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool CanLearnSkill(GuildSkillData data)
        {
            if (data.Cost == null) return false;
            foreach (KeyValuePair<string, string> kvp in data.Cost)
            {
                if (item_helper.GetMyMoneyCountByItemId(int.Parse(kvp.Key)) < int.Parse(kvp.Value))
                {
                    return false;
                }
            }
            if (data.Condition != null && guild_spell_helper.IsMeetCondition(data.Condition) == false)
            {
                return false;
            }
            return true;
        }
    }
}
