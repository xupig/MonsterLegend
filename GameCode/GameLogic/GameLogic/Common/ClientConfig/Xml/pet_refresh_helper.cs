﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class pet_refresh_helper
    {
        private static Dictionary<int, Dictionary<int, pet_refresh>> _dict = new Dictionary<int, Dictionary<int, pet_refresh>>();

        static pet_refresh_helper()
        {
            try
            {
                foreach (pet_refresh petRefresh in XMLManager.pet_refresh.Values)
                {
                    if (!_dict.ContainsKey(petRefresh.__pet_id))
                    {
                        _dict.Add(petRefresh.__pet_id, new Dictionary<int, pet_refresh>());
                    }
                    Dictionary<int, pet_refresh> petIdDict = _dict[petRefresh.__pet_id];
                    petIdDict.Add(petRefresh.__refresh_level, petRefresh);
                }
            }
            catch(Exception ex)
            {
                Debug.LogError("宠物洗练表报错:" + ex);
            }
        }

        public static pet_refresh GetConfig(int petId, int washLevel)
        {
            if (!_dict.ContainsKey(petId))
            {
                Debug.LogError("不存在宠物id:" + petId);
            }
            if (!_dict[petId].ContainsKey(washLevel))
            {
                Debug.LogError("不存在宠物id:" + petId);
            }
            return _dict[petId][washLevel];
        }

        public static List<IAttribute> GetAttributeList(int petId, int washLevel)
        {
            List<IAttribute> attributeList = new List<IAttribute>();
            pet_refresh petRefresh = GetConfig(petId, washLevel);
            Dictionary<string, string[]> dataDict = data_parse_helper.ParseMapList(petRefresh.__attris_range);
            foreach (string attributeIdStr in dataDict.Keys)
            {
                int attriId = int.Parse(attributeIdStr);
                RoleAttributeData lowAttributeData = new RoleAttributeData(attriId, int.Parse(dataDict[attributeIdStr][0]));
                RoleAttributeData upAttributeData = new RoleAttributeData(attriId, int.Parse(dataDict[attributeIdStr][1]));
                UplowboundAttributeData uplowboundAttributeData = new UplowboundAttributeData(upAttributeData, lowAttributeData);
                attributeList.Add(uplowboundAttributeData);
            }

            attributeList.Sort(RoleAttributeData.SortByPriority);

            return attributeList;
        }

        public static int GetNextExp(int petId, int washLevel)
        {
            return GetConfig(petId, washLevel).__next_refresh_level_exp;
        }

        public static Dictionary<string, string[]> GetAttributeValueRange(int petId, int washLevel)
        {
            return data_parse_helper.ParseMapList(GetConfig(petId, washLevel).__attris_range);
        }

        public static List<int> GetSkillIdList(int petId, int washLevel, int hole)
        {
            Dictionary<string, string[]> dict = data_parse_helper.ParseMapList(GetConfig(petId, washLevel).__skills_range);
            string[] skillIdStrArray = dict[hole.ToString()];
            List<int> skillIdList = new List<int>();
            for(int i = 0;i < skillIdStrArray.Length;i++)
            {
                skillIdList.Add(int.Parse(skillIdStrArray[i]));
            }
            return skillIdList;
        }

        public static void Init()
        {
            int count = XMLManager.pet_refresh.Count;
        }
    }
}
