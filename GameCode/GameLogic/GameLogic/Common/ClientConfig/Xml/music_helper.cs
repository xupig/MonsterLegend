﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class music_helper
    {
        private static music_helper _instance = null;
        public static music_helper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new music_helper();
            }
            return _instance;
        }

        public music GetBgMusicData(int mapID)
        {
            if (string.IsNullOrEmpty(XMLManager.map[mapID].__bgMusic)) return null;
            var bgMusic = data_parse_helper.ParseListString(XMLManager.map[mapID].__bgMusic);
            int musicID = GetRandomMusic(bgMusic);
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        public music GetBgMusicByMusicID(int musicID)
        {
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        /// <summary>
        /// 根据场景ID,获取BOSS音乐信息
        /// </summary>
        /// <param name="mapID"></param>
        /// <returns></returns>
        public music GetBossMusicData(int mapID)
        {
            if (string.IsNullOrEmpty(XMLManager.map[mapID].__bossMusic)) return null;
            var bossMusic = data_parse_helper.ParseListString(XMLManager.map[mapID].__bossMusic);
            int musicID = GetRandomMusic(bossMusic);
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        public music GetMonsterMusicData(int mapID)
        {
            if (string.IsNullOrEmpty(XMLManager.map[mapID].__monsterMusic)) return null;
            var monsterMusic = data_parse_helper.ParseListString(XMLManager.map[mapID].__monsterMusic);
            int musicID = GetRandomMusic(monsterMusic);
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        public music GetMusicData(int musicID)
        {
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        private int GetRandomMusic(List<string> musicList)
        {
            if (musicList.Count > 1)
            {
                int rndNum = RandomHelper.GetRandomInt(1000) % musicList.Count;
                return int.Parse(musicList[rndNum]);
            }
            //目前就是返回第一首音乐
            return int.Parse(musicList[0]);

        }


    }
}
