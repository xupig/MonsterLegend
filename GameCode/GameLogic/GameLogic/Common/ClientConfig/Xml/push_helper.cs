﻿using System;
using System.Collections.Generic;
using Common.ServerConfig;

namespace GameData
{
    public enum ServerPushId
    {
        DreamLandReward = 10,  //幻境探索奖励
        TradeMarket = 11  //交易所
    }

    public class push_helper
    {
        public static push GetPushInfo(int pushId)
        {
            if (XMLManager.push.ContainsKey(pushId))
            {
                return XMLManager.push[pushId];
            }
            return null;
        }

        public static bool IsAddPushSettingList(int pushId)
        {
            bool result = false;
            push pushInfo = GetPushInfo(pushId);
            if (pushInfo != null)
            {
                result = pushInfo.__desc != 0 ? true : false;
            }
            return result;
        }

        public static bool IsPushDefaultOpen(int pushId)
        {
            bool isOpen = true;
            push pushInfo = GetPushInfo(pushId);
            isOpen = pushInfo.__default_shield == 0 ? true : false;
            return isOpen;
        }

        public static bool IsServerPush(int pushId)
        {
            push pushInfo = GetPushInfo(pushId);
            return pushInfo.__server_push == 1 ? true : false;
        }

        public static int GetServerPushType(int pushId)
        {
            switch (pushId)
            {
                case (int)ServerPushId.DreamLandReward:
                    return public_config.SETTING_TYPE_PUSH_DREAM_LAND;
                case (int)ServerPushId.TradeMarket:
                    return public_config.SETTING_TYPE_PUSH_AUCTION;
                default:
                    return public_config.SETTING_TYPE_PUSH_AUCTION;
            }
        }
    }
}
