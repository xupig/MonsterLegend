﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    class equip_value_helper
    {
        public static void Init()
        {
            int count = XMLManager.equip_value.Count;
        }

        public static equip_value GetConfig(int id, int equipId = -1)
        {
            if (!XMLManager.equip_value.ContainsKey(id))
            {
                string errorContent = "配置表equip_value获取数据失败，错误id为" + id;
                if (equipId != -1)
                {
                    errorContent += " 对应装备id为:" + equipId;
                }
                throw new Exception(errorContent);
            }
            return XMLManager.equip_value[id];
        }
    }
}
