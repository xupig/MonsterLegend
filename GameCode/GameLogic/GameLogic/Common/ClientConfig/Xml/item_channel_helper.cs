﻿#region 模块信息
/*==========================================
// 文件名：item_channel_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/17 19:51:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
namespace GameData
{
    public static class item_channel_helper
    {
        public static List<int> GetItemChannelViewList(int itemId)
        {
            List<int> result = new List<int>();
            if (XMLManager.item_channel.ContainsKey(itemId))
            {
                item_channel data = XMLManager.item_channel[itemId];
                List<string> dataList = data_parse_helper.ParseListString(data.__channel_id);
                for (int i = 0; i < dataList.Count; i++)
                {
                    int channelId = int.Parse(dataList[i]);
                    if (channelId > 0)
                    {
                        result.Add(channelId);
                    }
                }
            }
            return result;
        }

        public static List<int> GetItemChannelIconList(int itemId)
        {
            List<int> result = new List<int>();
            if (XMLManager.item_channel.ContainsKey(itemId))
            {
                item_channel data = XMLManager.item_channel[itemId];
                List<string> dataList = data_parse_helper.ParseListString(data.__channel_icon);
                for (int i = 0; i < dataList.Count; i++)
                {
                    int channelIconId = int.Parse(dataList[i]);
                    if (channelIconId > 0)
                    {
                        result.Add(channelIconId);
                    }
                }
            }
            return result;
        }

        public static List<int> GetItemChannelDescList(int itemId)
        {
            List<int> result = new List<int>();
            if (XMLManager.item_channel.ContainsKey(itemId))
            {
                item_channel data = XMLManager.item_channel[itemId];
                List<string> dataList = data_parse_helper.ParseListString(data.__channel_describe);
                for (int i = 0; i < dataList.Count; i++)
                {
                    int channelDescId = int.Parse(dataList[i]);
                    if (channelDescId > 0)
                    {
                        result.Add(channelDescId);
                    }
                }
            }
            return result;
        }

        public static List<int> GetItemChannelFunctionList(int itemId)
        {
            List<int> result = new List<int>();
            if (XMLManager.item_channel.ContainsKey(itemId))
            {
                item_channel data = XMLManager.item_channel[itemId];
                List<string> dataList = data_parse_helper.ParseListString(data.__channel_function);
                for (int i = 0; i < dataList.Count; i++)
                {
                    int channelDescId = int.Parse(dataList[i]);
                    if (channelDescId > 0)
                    {
                        result.Add(channelDescId);
                    }
                }
            }
            return result;
        }

        public static bool ContainId(int id)
        {
            return XMLManager.item_channel.ContainsKey(id);
        }

        public static int GetNextLevelEquipIndex(int equipLevel, int quality, int position, int star)
        {
            int defaultIndex = 0;
            foreach (KeyValuePair<int, equip_quality_info> pair in XMLManager.equip_quality_info)
            {
                if (quality == pair.Value.__equip_quality && star == pair.Value.__equip_star 
                    && XMLManager.equip_quality_info.ContainsKey(pair.Key + 1))
                {
                    defaultIndex = pair.Key + 1;
                }
            }
            // 特殊处理，当满级之后，没有可推荐装备时
            if (defaultIndex == 0)
            {
                return 0;
            }
            int nextLevelQuality = XMLManager.equip_quality_info[defaultIndex].__equip_quality;
            int nextLevelStar = XMLManager.equip_quality_info[defaultIndex].__equip_star;
            return GetIndex(equipLevel, nextLevelQuality, position, nextLevelStar);
        }

        public static int GetDefaultLevelEquipIndex(int equipLevel, int position)
        {
            int defaultIndex = 1;
            int quility = XMLManager.equip_quality_info[defaultIndex].__equip_quality;
            int star = XMLManager.equip_quality_info[defaultIndex].__equip_star;
            return GetIndex(equipLevel, quility, position, star);
        }

        public static int GetIndex(int equipLevel, int quality, int postion, int star)
        {
            foreach (KeyValuePair<int, equip_sources> pair in XMLManager.equip_sources)
            {
                if (equipLevel == pair.Value.__equip_level
                    && quality == pair.Value.__equip_quality_id
                    && star == pair.Value.__equip_star
                    && postion == pair.Value.__equip_position)
                {
                    return pair.Key;
                }
            }
            string errorMessage = "equipLevel : " + equipLevel + "  quality : " + quality + "  "
                + "position : " + postion + "  " + "star : " + star;
            throw new Exception("@策划 在装备来源 equip_source 配置表中缺少 " + errorMessage + "的配置信息");
        }
    }
}