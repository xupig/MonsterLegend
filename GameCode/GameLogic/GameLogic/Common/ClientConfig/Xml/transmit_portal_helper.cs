﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;

namespace GameData
{
    public class transmit_portal_helper
    {
        private static List<ActivityNoticeData> _dataList;

        public static List<ActivityNoticeData> GetNoticeDataList()
        {
            if (_dataList == null)
            {
                _dataList = new List<ActivityNoticeData>();
                InitPortalNotice();
                InitWorldBossNotice();
            }
            return _dataList;
        }

        private static void InitWorldBossNotice()
        {
            foreach (spawn_world_boss cfg in XMLManager.spawn_world_boss.Values)
            {
                ActivityNoticeData data = new ActivityNoticeData();
                List<string> timeList = new List<string>();
                if (cfg.__is_notice == 0) continue;
                List<string> list = data_parse_helper.ParseListString(cfg.__open_time);
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        timeList.Add(string.Format("{0}00", int.Parse(list[i]) + j));
                    }
                }
                data.noticeActionList = new List<string>();
                for (int i = 0; i < timeList.Count; i++)
                {
                    data.noticeActionList.Add(cfg.__notice.ToString());
                }
                activity_helper.SetNoticeTime(data, timeList);
                _dataList.Add(data);
            }
        }

        private static void InitPortalNotice()
        {
            foreach (transmit_portal cfg in XMLManager.transmit_portal.Values)
            {
                ActivityNoticeData data = new ActivityNoticeData();
                data.noticeActionList = data_parse_helper.ParseListString(cfg.__before_notice);
                activity_helper.SetNoticeTime(data, data_parse_helper.ParseListString(cfg.__before_time));
                _dataList.Add(data);
            }
        }

        public static transmit_portal GetConfig(int id)
        {
            if (!XMLManager.transmit_portal.ContainsKey(id))
            {
                throw new Exception("配置表 transmit_portal.xml 未包含项 " + id.ToString());
            }
            return XMLManager.transmit_portal[id];
        }

        public static int GetModelID(int portalId)
        {
            transmit_portal cfg = GetConfig(portalId);
            if (cfg != null)
            {
                return cfg.__model;
            }
            return 0;
        }

        public static string GetDropModelPath(int portalId)
        {
            int modelID = GetModelID(portalId);
            if (XMLManager.drop_model.ContainsKey(modelID))
            {
                return XMLManager.drop_model[modelID].__prefab;
            }
            return string.Empty;
        }

        public static string GetPortalName(int portalId)
        {
            transmit_portal cfg = GetConfig(portalId);
            if (cfg != null)
            {
                return MogoLanguageUtil.GetContent(cfg.__portal_name);
            }
            return string.Empty;
        }

        public static string GetPortalDesc(int portalId)
        {
            transmit_portal cfg = GetConfig(portalId);
            if (cfg != null)
            {
                return MogoLanguageUtil.GetContent(cfg.__desc);
            }
            return string.Empty;
        }

        public static void SendNotice(ActivityNoticeData data, int currentSecond)
        {
            if (data.noticeTimeList.Count > 0)
            {
                for (int i = 0; i < data.noticeTimeList.Count; i++)
                {
                    int offset = currentSecond - data.noticeTimeList[i];
                    if (offset >= 0 && offset < 15)//时间判断
                    {
                        SystemInfoManager.Instance.Show(int.Parse(data.noticeActionList[i]));
                        return;
                    }
                }
            }
        }

    }
}
