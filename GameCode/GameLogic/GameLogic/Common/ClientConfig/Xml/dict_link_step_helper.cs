﻿#region 模块信息
/*==========================================
// 文件名：map_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;

namespace GameData
{
    public class dict_link_step_helper
    {
        private static Dictionary<int, Dictionary<string, dict_link_step>> _eventsDataMap = null;
        public static Dictionary<int, Dictionary<string, dict_link_step>> eventsDataMap
        {
            get {
                if (_eventsDataMap == null)
                {
                    _eventsDataMap = new Dictionary<int, Dictionary<string, dict_link_step>>();
                    foreach (KeyValuePair<int, dict_link_step> pair in XMLManager.dict_link_step)
                    {
                        var stepID = pair.Key;
                        var data = pair.Value;
                        if (data.__step_type == 0) continue;
                        if (!_eventsDataMap.ContainsKey(data.__step_type))
                        {
                            _eventsDataMap[data.__step_type] = new Dictionary<string, dict_link_step>();
                        }
                        if (string.IsNullOrEmpty(data.__step_value)) continue;
                        _eventsDataMap[data.__step_type][data.__step_value] = data;
                    }
                }
                return _eventsDataMap; 
            }
        }

        public static dict_link_step GetDictLinkStepData(int dataID)
        {
            if (XMLManager.dict_link_step.ContainsKey(dataID))
            {
                return XMLManager.dict_link_step[dataID];
            }
            return null;
        }

        public static int GetStep(int stepType, string value)
        {
            var data = eventsDataMap;
            if (data.ContainsKey(stepType) && data[stepType].ContainsKey(value))
            {
                return data[stepType][value].__id;
            }
            return 0;
        }
    }
}
