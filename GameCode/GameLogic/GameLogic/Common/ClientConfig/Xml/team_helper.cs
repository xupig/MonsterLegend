﻿#region 模块信息
/*==========================================
// 文件名：team_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/22 16:39:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{

    public enum team_detail_type
    {
       
    }

    public class TeamLimitData
    {
        public int type;
        public int minLevel;
        public int maxLevel;
        public long startDate;
        public long endDate;

        public List<int> openWeekOfDay;
        public List<int> openTimeStart;
        public List<int> openTimeEnd;
    }

    public class team_helper
    {
        public const int zu_dui_mi_jing = 1;//组队秘境
        public const int ye_wai_boss = 2;//野外boss
        public const int ye_wai_guaji = 3;//野外挂机
        public const int wan_shang_huo_dong = 4;//晚上活动
        public const int ye_wai_ren_wu = 5;//野外任务
        private static Dictionary<int, List<team_detail>> teamDict;
        private static List<team_menu> teamMenuList;
        private static List<team_menu> teamMiniMenuList;
        private static Dictionary<int, List<TeamLimitData>> effectDict;
        private static Dictionary<int, TeamLimitData> detailEffectDict;
        private static Dictionary<int, Dictionary<int,int>> instanceId2GameIdDict;

        public static string GetDetailInstanceName(team_detail detail)
        {
            if(detail.__task!=0)
            {
                return detail.__task.ToLanguage();
            }
            else
            {
                return instance_helper.GetInstanceName(detail.__instance_id);
            }
        }

        public static TeamLimitData GetDetailEffectData(int detailId)
        {
            InitialData();
            if(detailEffectDict.ContainsKey(detailId))
            {
                return detailEffectDict[detailId];
            }
            return null;
        }

        public static List<team_menu> GetTeamMenuList()
        {
            if(teamMenuList == null)
            {
                teamMenuList = new List<team_menu>();
                foreach(KeyValuePair<int,team_menu> kvp in XMLManager.team_menu)
                {
                    if(kvp.Value.__id != -10)
                    {
                        teamMenuList.Add(kvp.Value);
                    }
                }
            }
            return teamMenuList;
        }

        public static List<team_menu> GetMiniTeamMenuList()
        {
            if (teamMiniMenuList == null)
            {
                teamMiniMenuList = new List<team_menu>();
                foreach (KeyValuePair<int, team_menu> kvp in XMLManager.team_menu)
                {
                    if (kvp.Value.__id != 0 && kvp.Value.__id != -10)
                    {
                        teamMiniMenuList.Add(kvp.Value);
                    }
                }
            }
            return teamMiniMenuList;
        }

        /// <summary>
        /// type=2、3、5类型的一定要传值
        /// </summary>
        /// <param name="instId"></param>
        /// <param name="type">2、3、5类型的一定要传值</param>
        /// <returns></returns>
        public static int GetGameIdByInstanceId(int instId,int type = 0)
        {
            InitialData();
            if (instanceId2GameIdDict.ContainsKey(instId))
            {
                if(instanceId2GameIdDict[instId].ContainsKey(type))
                {
                    return instanceId2GameIdDict[instId][type];
                }
                else
                {
                    return instanceId2GameIdDict[instId][0];
                }
            }
            return -2;
        }

        private static void InitialData()
        {
            if (teamDict == null)
            {
                teamDict = new Dictionary<int, List<team_detail>>();
                effectDict = new Dictionary<int, List<TeamLimitData>>();
                detailEffectDict = new Dictionary<int, TeamLimitData>();
                instanceId2GameIdDict = new Dictionary<int, Dictionary<int, int>>();
                foreach (KeyValuePair<int, team_detail> kvp in XMLManager.team_detail)
                {
                    if (instanceId2GameIdDict.ContainsKey(kvp.Value.__instance_id) == false)
                    {
                        instanceId2GameIdDict.Add(kvp.Value.__instance_id,new Dictionary<int,int>());
                    }
                    if(instanceId2GameIdDict[kvp.Value.__instance_id].ContainsKey(kvp.Value.__type) == false)
                    {
                        instanceId2GameIdDict[kvp.Value.__instance_id].Add(kvp.Value.__type, kvp.Value.__id);
                        if (kvp.Value.__type != 0 && kvp.Value.__type != 2 && kvp.Value.__type != 3 && kvp.Value.__type != 5)
                        {
                            instanceId2GameIdDict[kvp.Value.__instance_id].Add(0, kvp.Value.__id);
                        }
                    }
                    if (teamDict.ContainsKey(kvp.Value.__type) == false)
                    {
                        teamDict.Add(kvp.Value.__type, new List<team_detail>());
                        effectDict.Add(kvp.Value.__type, new List<TeamLimitData>());
                    }
                    if (kvp.Value.__effect_open == 1)
                    {
                        TeamLimitData data = new TeamLimitData();
                        data.type = kvp.Value.__type;
                        data.minLevel = GetDetailMinLevel(kvp.Value);
                        data.maxLevel = GetDetailMaxLevel(kvp.Value);
                        SetOpenDate(data, data_parse_helper.ParseListString(kvp.Value.__open_date));
                        SetOpenWeekOfDay(data, data_parse_helper.ParseListString(kvp.Value.__open_day));
                        SetOpenTime(data, data_parse_helper.ParseListString(kvp.Value.__open_time), data_parse_helper.ParseListString(kvp.Value.__time));
                        effectDict[kvp.Value.__type].Add(data);
                        detailEffectDict.Add(kvp.Value.__id, data);
                    }
                    if(kvp.Value.__id!=-1)
                    {
                        teamDict[kvp.Value.__type].Add(kvp.Value);
                    }
                }
            }
        }


        private static void SetOpenTime(TeamLimitData data, List<string> openTime, List<string> lastTime)
        {
            data.openTimeStart = new List<int>();
            data.openTimeEnd = new List<int>();
            if (openTime != null)
            {
                for (int i = 0; i < openTime.Count; i++)
                {
                    int time = int.Parse(openTime[i]);
                    int hour = time / 100;
                    int min = time % 100;
                    data.openTimeStart.Add(hour * 60 * 60 + min * 60);
                    data.openTimeEnd.Add(hour * 60 * 60 + min * 60 + int.Parse(lastTime[i]));
                }
            }
        }

        private static void SetOpenWeekOfDay(TeamLimitData data, List<string> openOfWeek)
        {
            data.openWeekOfDay = new List<int>();
            if (openOfWeek == null || openOfWeek.Count == 0 || openOfWeek[0] == "0")
            {
                for (int i = 0; i < 7; i++)
                {
                    data.openWeekOfDay.Add(i);
                }
            }
            else
            {
                for (int i = 0; i < openOfWeek.Count; i++)
                {
                    int day = int.Parse(openOfWeek[i]);
                    if (day == 7)
                    {
                        day = 0;
                    }
                    data.openWeekOfDay.Add(day);
                }
            }
        }

        private static void SetOpenDate(TeamLimitData data, List<string> openDate)
        {
            data.startDate = long.MinValue;
            data.endDate = long.MaxValue;
            if (openDate != null)
            {
                if (openDate.Count > 0)
                {
                    data.startDate = ConvertToTimeStamp(int.Parse(openDate[0]));
                }
                if (openDate.Count > 1)
                {
                    data.endDate = ConvertToTimeStamp(int.Parse(openDate[1]));
                }
            }
        }

        private static long ConvertToTimeStamp(int time)
        {
            int year = time / 10000;
            time = time % 10000;
            int month = time / 100;
            int day = time % 100;
            DateTime dateTime = MogoTimeUtil.GetUtcTime(year, Math.Max(1, month), Math.Max(1, day), 0, 0, 0);
            return MogoTimeUtil.UtcTime2ServerTimeStamp(dateTime);
        }

        public static List<TeamLimitData> GetEffectList(int type)
        {
            InitialData();
            if(type == 0)
            {
                return null;
            }
            return effectDict[type];
        }

        public static List<team_detail> GetTeamDetailList(int type)
        {
            InitialData();
            if(teamDict.ContainsKey(type))
            {
                return teamDict[type];
            }
            return null;
        }

        public static team_detail GetTeamDetail(int gameId)
        {
            if(XMLManager.team_detail.ContainsKey(gameId))
            {
                return XMLManager.team_detail[gameId];
            }
            return null;
        }

        public static int GetDetailMinLevel(team_detail detail)
        {
            int minLevel = 1;
            if (detail != null)
            {
                if (!string.IsNullOrEmpty(detail.__level_rank))
                {
                    List<int> listInt = data_parse_helper.ParseListInt(detail.__level_rank);
                    minLevel = listInt[0];
                }
                else if (detail.__instance_id != 0)
                {
                    instance inst = instance_helper.GetInstanceCfg(detail.__instance_id);
                    if (inst != null && !string.IsNullOrEmpty(detail.__level_rank))
                    {
                        List<int> listInt2 = data_parse_helper.ParseListInt(detail.__level_rank);
                        minLevel = listInt2[0];
                    }
                }
            }
            return minLevel;
        }
        private static int maxLevel = -1;

        public static int GetAvatarMaxLevel()
        {
            InitAvatarMaxLevel();
            return maxLevel;
        }

        private static void InitAvatarMaxLevel()
        {
            if (maxLevel == -1)
            {
                if (maxLevel == -1)
                {
                    foreach (KeyValuePair<int, avatar_level> kvp in XMLManager.avatar_level)
                    {
                        maxLevel = Math.Max(maxLevel, kvp.Key);
                    }
                }
            }
        }

        public static int GetDetailMaxLevel(team_detail detail)
        {
            InitAvatarMaxLevel();
            int _maxLevel = maxLevel;
            List<int> listInt = data_parse_helper.ParseListInt(detail.__level_rank);
            if (listInt != null && listInt.Count > 1)
            {
                maxLevel = listInt[1];
                if (maxLevel == 0)
                {
                    maxLevel = _maxLevel;
                }
            }
            else if (detail.__instance_id != 0)
            {
                instance inst = instance_helper.GetInstanceCfg(detail.__instance_id);
                List<int> listInt2 = data_parse_helper.ParseListInt(inst.__level_limit);
                if (inst != null && listInt2 != null && listInt2.Count > 1)
                {
                    maxLevel = listInt2[1];
                    if (maxLevel == 0)
                    {
                        maxLevel = _maxLevel;
                    }
                }
            }
            return maxLevel;
        }

       
    }
}
