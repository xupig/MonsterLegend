﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class mission_battle_statistics_helper
    {
        public static mission_battle_statistics GetBattleStatistics(int actionId)
        {
            foreach (KeyValuePair<int, mission_battle_statistics> kvp in XMLManager.mission_battle_statistics)
            {
                if(kvp.Value.__action == actionId)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public static List<int> GetStatisticActionIdList()
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, mission_battle_statistics> kvp in XMLManager.mission_battle_statistics)
            {
                if (result.Contains(kvp.Value.__action) == false && (kvp.Value.__bear_show == 1 || kvp.Value.__output_show == 1))
                {
                    result.Add(kvp.Value.__action);
                }

            }
            return result;
        }

        public static bool IsCanShowStatistic(int actionId, int showType)
        {
            foreach (KeyValuePair<int, mission_battle_statistics> kvp in XMLManager.mission_battle_statistics)
            {
                if (kvp.Value.__action == actionId)
                {
                    switch (showType)
                    {
                        case 1:
                            return kvp.Value.__single_show == 1;
                        case 2:
                            return kvp.Value.__multi_show == 1;
                        case 3:
                            return kvp.Value.__pvp_show == 1;
                    }
                }

            }
            return false;
        }

        public static StatisticOutputOrBear OutPutOrBear(int actionId)
        {
            foreach (KeyValuePair<int, mission_battle_statistics> kvp in XMLManager.mission_battle_statistics)
            {
                if (kvp.Value.__action == actionId)
                {
                    if (kvp.Value.__bear_show == 1)
                    {
                        return StatisticOutputOrBear.Bear;
                    }
                    else
                    {
                        return StatisticOutputOrBear.Output;
                    }
                }

            }
            return StatisticOutputOrBear.Output;
        }
    }
}
