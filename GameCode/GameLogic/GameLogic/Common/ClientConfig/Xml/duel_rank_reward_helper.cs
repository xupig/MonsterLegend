﻿using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class duel_rank_reward_helper
    {
        public static List<duel_rank_reward> ConfigList;

        static duel_rank_reward_helper()
        {
            ConfigList = XMLManager.duel_rank_reward.Values.ToList<duel_rank_reward>();
            ConfigList.Sort(SortByLevel);
        }

        private static int SortByLevel(duel_rank_reward x, duel_rank_reward y)
        {
            if (x.__level < y.__level)
            {
                return -1;
            }
            return 1;
        }

        public static duel_rank_reward GetConfig(int id)
        {
            if (!XMLManager.duel_rank_reward.ContainsKey(id))
            {
                throw new Exception("duel_rank_reward 找不到id:" + id);
            }
            return XMLManager.duel_rank_reward[id];
        }

        public static duel_rank_reward GetConfig(uint score, short rank)
        {
            for(int i = 0;i < ConfigList.Count;i++)
            {
                duel_rank_reward duelRankReward = ConfigList[i];
                List<int> listInt = data_parse_helper.ParseListInt(duelRankReward.__score);
                if((listInt[0] <= score)
                    && (score <= listInt[1]))
                {
                    List<int> rankListInt = data_parse_helper.ParseListInt(duelRankReward.__rank);
                    if (rankListInt.Count == 0)
                    {
                        return duelRankReward;
                    }
                    else
                    {
                        int startRank = rankListInt[0];
                        int endRank = rankListInt.Count > 1 ? rankListInt[1] : startRank;
                        if (startRank <= rank && rank <= endRank)
                        {
                            return duelRankReward;
                        }
                    }
                }
            }
            throw new Exception(string.Format("score:{0}  rank:{1} 在duel_rank_reward找不到配置", score, rank));
        }

        public static int GetIconId(duel_rank_reward duelRankReward)
        {
            return duelRankReward.__icon;
        }

        public static duel_rank_reward GetNext(duel_rank_reward duelRankReward)
        {
            int index = ConfigList.IndexOf(duelRankReward);
            if (index != 0)
            {
                return ConfigList[index - 1];
            }
            return null;
        }

        public static string GetMinScoreRecommand(duel_rank_reward duelRankReward)
        {
            List<string> listString = data_parse_helper.ParseListString(duelRankReward.__score);
            return listString[0];
        }

        public static string GetMinRank(duel_rank_reward duelRankReward)
        {
            List<string> listString = data_parse_helper.ParseListString(duelRankReward.__rank);
            return listString[0];
        }

        public static string GetName(uint score, int rank)
        {
            return GetName(GetConfig(score, (short)rank), rank);
        }

        public static string GetName(duel_rank_reward duelRankReward, int rank)
        {
            if (rank < 0)
            {
                return MogoLanguageUtil.GetContent(122109);
            }
            string name = MogoLanguageUtil.GetContent(duelRankReward.__name);
            List<string> listString = data_parse_helper.ParseListString(duelRankReward.__rank);
            if (listString.Count != 0)
            {
                string rankStr = string.Format("({0})", rank);
                name += rankStr;
            }
            return name;
        }

        public static string GetNameInReward(int id)
        {
            return GetNameInReward(GetConfig(id));
        }

        public static string GetNameInReward(duel_rank_reward duelRankReward)
        {
            string name = MogoLanguageUtil.GetContent(duelRankReward.__name);
            List<int> listInt = data_parse_helper.ParseListInt(duelRankReward.__rank);
            if (listInt.Count != 0)
            {
                string rankStr;
                if (listInt.Count > 1)
                {
                    rankStr = string.Format("({0},{1})", listInt[0], listInt[1]);
                }
                else
                {
                    rankStr = string.Format("({0})", listInt[0]);
                }
                name += rankStr;
            }
            return name;
        }
    }
}
