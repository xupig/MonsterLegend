﻿#region 模块信息
/*==========================================
// 文件名：mfg_formula_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/30 20:30:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;

namespace GameData
{
    public enum FormulaType
    {
        AttachMagic = 1,
        Suit = 2,
        Equip = 3,
    }

    public class mfg_formula_helper
    {
        /// <summary>
        /// key : make_product_category中的category字段
        /// value : 对应的配置项
        /// </summary>
        private static Dictionary<int, make_product_category> makeCategoryDict;
        private static List<int> makeCategoryList;
        public static List<int> GetCategoryList()
        {
            InitMakeCategoryDict();
            return makeCategoryList;
        }

        public static string GetCategoryName(int category)
        {
            return MogoLanguageUtil.GetContent(makeCategoryDict[category].__name);
        }

        private static int SortFunction(int x, int y)
        {
            return makeCategoryDict[x].__priority - makeCategoryDict[y].__priority;
        }

        private static void InitMakeCategoryDict()
        {
            if (makeCategoryDict == null)
            {
                makeCategoryDict = new Dictionary<int, make_product_category>();
                foreach (KeyValuePair<int, make_product_category> pair in XMLManager.make_product_category)
                {
                    makeCategoryDict.Add(pair.Value.__category, pair.Value);
                }
                makeCategoryList = new List<int>(makeCategoryDict.Keys);
                makeCategoryList.Sort(SortFunction);
            }
        }

        public static mfg_formula GetFormulaCfg(int formulaId)
        {
            if (XMLManager.mfg_formula.ContainsKey(formulaId))
            {
                return XMLManager.mfg_formula[formulaId];
            }
            return null;
        }

        public static Dictionary<int, int> GetFormulaLearnCost(int formulaId)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            if (XMLManager.mfg_formula.ContainsKey(formulaId))
            {
                Dictionary<string, string> cost = data_parse_helper.ParseMap(XMLManager.mfg_formula[formulaId].__learn_cost);
                if (cost != null)
                {
                    foreach (var pair in cost)
                    {
                        result.Add(int.Parse(pair.Key), int.Parse(pair.Value));
                    }
                }
                return result;
            }
            return result;
        }

        public static Dictionary<int, int> GetFormulaMakeCost(int formulaId, bool includeSpirit = true)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            if (XMLManager.mfg_formula.ContainsKey(formulaId))
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(XMLManager.mfg_formula[formulaId].__forge_cost);
                foreach (var item in dataDict)
                {
                    if (item.Key == "6" && includeSpirit == false)
                    {
                    }
                    else
                    {
                        result.Add(int.Parse(item.Key), int.Parse(item.Value));
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 获取开启专精之后的制造消耗
        /// </summary>
        /// <param name="formulaId"></param>
        /// <returns>返回消耗列表</returns>
        public static Dictionary<int, int> GetExpertFormulaMakeCost(int formulaId, bool includeSpirit = true)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            if (XMLManager.mfg_formula.ContainsKey(formulaId))
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(XMLManager.mfg_formula[formulaId].__expert_forge_cost);
                foreach (var item in dataDict)
                {
                    if (item.Key == "6" && includeSpirit == false)
                    {
                    }
                    else
                    {
                        result.Add(int.Parse(item.Key), int.Parse(item.Value));
                    }
                }
            }
            return result;
        }

        public static int GetMakeGiftByCraftId(int craftId)
        {
            if (XMLManager.mfg_formula.ContainsKey(craftId))
            {
                return XMLManager.mfg_formula[craftId].__talent_condition;
            }
            return 0;
        }

        public static int GetProductType(int craftId)
        {
            if (XMLManager.mfg_formula.ContainsKey(craftId))
            {
                return XMLManager.mfg_formula[craftId].__product_type;
            }
            return 0;
        }

        public static int GetExpertFormulaMakeCostOfSpirit(int craftId)
        {
            if (XMLManager.mfg_formula.ContainsKey(craftId))
            {
                mfg_formula cfg = XMLManager.mfg_formula[craftId];
                Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__expert_forge_cost);
                if (dict != null && dict.ContainsKey("6"))
                {
                    return int.Parse(dict["6"]);
                }
            }
            return 0;
        }

        public static int GetFormulaMakeCostOfSpirit(int craftId)
        {
            if (XMLManager.mfg_formula.ContainsKey(craftId))
            {
                mfg_formula cfg = XMLManager.mfg_formula[craftId];
                Dictionary<string, string> dict = data_parse_helper.ParseMap(cfg.__forge_cost);
                if (dict != null && dict.ContainsKey("6"))
                {
                    return int.Parse(dict["6"]);
                }
            }
            return 0;
        }

        private static Dictionary<int, List<int>> _giftLockFormulaDict;
        public static List<int> GetGiftLockFormulaIdList(int giftId)
        {
            if (_giftLockFormulaDict == null)
            {
                InitGiftLockFormulaDict();
            }
            if (_giftLockFormulaDict.ContainsKey(giftId))
            {
                return _giftLockFormulaDict[giftId];
            }
            return null;
        }

        private static void InitGiftLockFormulaDict()
        {
            _giftLockFormulaDict = new Dictionary<int, List<int>>();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                if (pair.Value.__talent_condition == 0)
                {
                    continue;
                }
                if (_giftLockFormulaDict.ContainsKey(pair.Value.__talent_condition) == false)
                {
                    _giftLockFormulaDict.Add(pair.Value.__talent_condition, new List<int>());
                }
                _giftLockFormulaDict[pair.Value.__talent_condition].Add(pair.Value.__id);
            }
        }

        public static List<BaseItemData> GetItemId(int craftId)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            if (XMLManager.mfg_formula.ContainsKey(craftId))
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(XMLManager.mfg_formula[craftId].__product);
                if (dict.ContainsKey("1"))
                {
                    int itemId = int.Parse(dict["1"]);
                    if (item_helper.IsEquip(itemId))
                    {
                        result.Add(new EquipData(itemId));
                    }
                    else
                    {
                        result.Add(new ItemData(itemId));
                    }
                }
                else if (dict.ContainsKey("2"))
                {
                    int rewardId = int.Parse(dict["2"]);
                    foreach (RewardData data in item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation))
                    {
                        ItemData itemData = new ItemData(data.id);
                        itemData.StackCount = data.num;
                        result.Add(itemData);
                    }
                }
            }
            return result;
        }

        public static int CheckEnoughMaterialLearn(int currentCraftId)
        {
            Dictionary<int, int> learnCost = GetFormulaLearnCost(currentCraftId);
            foreach (var pair in learnCost)
            {
                int itemId = pair.Key;
                int num = pair.Value;
                int bagNum = item_helper.GetMyMoneyCountByItemId(itemId);
                if (bagNum < num)
                {
                    return itemId;
                }
            }
            return 0;
        }

        public static int CheckEnoughMaterialMake(int currentCraftId)
        {
            bool isCraftGiftLearned = MakeManager.Instance.makeData.HasOpenCraftGift(currentCraftId);
            Dictionary<int, int> makeCost;
            if (isCraftGiftLearned)
            {
                makeCost = mfg_formula_helper.GetExpertFormulaMakeCost(currentCraftId);
            }
            else
            {
                makeCost = mfg_formula_helper.GetFormulaMakeCost(currentCraftId);
            }
            foreach (var pair in makeCost)
            {
                int itemId = pair.Key;
                int num = pair.Value;
                int bagNum = item_helper.GetMyMoneyCountByItemId(itemId);
                if (bagNum < num)
                {
                    return itemId;
                }
            }
            return 0;
        }

        public static int GetViewIdByProductType(int type)
        {
            int viewId = 0;
            if (XMLManager.make_product_category.ContainsKey(type))
            {
                viewId = XMLManager.make_product_category[type].__view_id;
            }
            return viewId;
        }

        public static bool CheckHasGiftCanLearn()
        {
            foreach (KeyValuePair<int, mfg_talent> pair in XMLManager.mfg_talent)
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(pair.Value.__condition);
                return player_condition_helper.JudgePlayerCondition(dict);
            }
            return false;
        }

        public static bool CheckGiftCanLearn(int giftId)
        {
            if (XMLManager.mfg_talent.ContainsKey(giftId))
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(XMLManager.mfg_talent[giftId].__condition);
                return player_condition_helper.JudgePlayerCondition(dict);
            }
            return false;
        }

        public static List<int> GetVocationConditionList(int type)
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                if (pair.Key != 0)
                {
                    if (pair.Value.__product_type == type)
                    {
                        if (pair.Value.__product_vocation != 0 && 
                            result.Contains(pair.Value.__product_vocation) == false)
                        {
                            result.Add(pair.Value.__product_vocation);
                        }
                    }
                }
            }
            return result;
        }

        public static List<int> GetLevelConditionList(int type)
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                if (pair.Key != 0)
                {
                    if (pair.Value.__product_type == type)
                    {
                        List<string> list = data_parse_helper.ParseListString(pair.Value.__product_level);
                        for (int i = 0; i < list.Count; i++)
                        {
                            int productLevel = int.Parse(list[i]);
                            if (productLevel != 0 &&
                                result.Contains(productLevel) == false)
                            {
                                result.Add(productLevel);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static List<int> GetTradeConditionList(int type)
        {
            List<int> result = new List<int>();
            foreach (KeyValuePair<int, mfg_formula> pair in XMLManager.mfg_formula)
            {
                if (pair.Key != 0)
                {
                    if (pair.Value.__product_type == type)
                    {
                        if (result.Contains(pair.Value.__is_unbind) == false)
                        {
                            result.Add(pair.Value.__is_unbind);
                        }
                    }
                }
            }
            return result;
        }
    }
}
