﻿using Common.ServerConfig;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class guild_achievement_helper
    {
        public static guild_achievement GetGuildAchievement(int id)
        {
            if (XMLManager.guild_achievement.ContainsKey(id))
            {
                return XMLManager.guild_achievement[id];
            }
            return null;
        }

        public static string GetName(int id)
        {
            guild_achievement config = GetGuildAchievement(id);
            return MogoLanguageUtil.GetContent(config.__name);
        }

        public static Dictionary<string, string> GetGuildAchievementConds(int id)
        {
            guild_achievement config = GetGuildAchievement(id);
            return data_parse_helper.ParseMap(config.__conds);
        }

        public static string GetGuildAchievementGetContent(int id)
        {
            string result = string.Empty;
            guild_achievement config = GetGuildAchievement(id);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__reward);
            foreach (KeyValuePair<string, string> kvp in dataDict)
            {
                result += MogoLanguageUtil.GetContent(74860, kvp.Value, item_helper.GetName(int.Parse(kvp.Key)));
            }
            return result;
        }

        /// <summary>
        /// 获得成就对应事件的分母，key为事件id，value为事件的分母
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, int> GetGuildAchievementEvent(int id)
        {
            guild_achievement config = GetGuildAchievement(id);
            Dictionary<int, int> result = new Dictionary<int, int>();
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(config.__conds);
            foreach (KeyValuePair<string, string> kvp in dataDict)
            {
                int condsId = int.Parse(kvp.Key);
                if (kvp.Value == "1")
                {
                    event_cnds cnds = event_cnds_helper.GetEvent(condsId);
                    string[] data = cnds.__conds.Split('=');
                    int value = int.Parse(data[1]);
                    result.Add(condsId, value);
                }
            }
            return result;
        }

    }
}
