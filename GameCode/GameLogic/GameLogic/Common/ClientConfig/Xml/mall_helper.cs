﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/13 11:29:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;

namespace GameData
{
    public enum MallSellLabel
    {
        nothing,//无标签
        newThing,//新品
        hot,//热销
        flashSale,//抢购
        discount,//折扣
        limitTime,//限时
        vip,//vip
        limitNum,//限购
    }

    public class mall_helper
    {
        private static bool _initialized = false;

        public static void Initialize()
        {
            if(_initialized == false)
            {
                _initialized = true;
                int count = XMLManager.market_data.Count;
            }
        }

        public static market_data GetConfig(int id)
        {
            if (!XMLManager.market_data.ContainsKey(id))
            {
                throw new Exception("配置表 market_data.xml 未包含项 " + id.ToString());
            }
            return XMLManager.market_data[id];
        }

        public static bool IsHot(int id)
        {
            market_data marketData = XMLManager.market_data[id];
            return marketData.__is_hot == 1;
        }

        public static bool IsOn(market_data data)
        {
            return data.__is_off == 0;
        }

        public static bool CanSend(market_data data)
        {
            return data.__buy_mode == 2;
        }

        public static bool CanDisplay(market_data data)
        {
            return data.__big_icon != 0;
        }

        public static bool IsGroup(market_data data)
        {
            return data.__show_item_id != 0;
        }

        public static int GetGroupId(market_data data)
        {
            return data.__show_item_id;
        }

        public static int GetGroupId(int id)
        {
            market_data marketData = GetMarketDataById(id);
            return GetGroupId(marketData);
        }

        public static string GetSellLabel(market_data data)
        {
            string sellLabel = "";
            switch(data.__sell_label)
            {
            case (int) MallSellLabel.newThing:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_NEW_THING);
                break;
            case (int) MallSellLabel.hot:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_HOT_SELL);
                break;
            case (int) MallSellLabel.flashSale:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_FLASH_SALE);
                break;
            case (int) MallSellLabel.discount:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_DISCOUNT);
                break;
            case (int) MallSellLabel.limitTime:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_LIMIT_TIME);
                break;
            case (int) MallSellLabel.vip:
                sellLabel = MogoLanguageUtil.GetString(LangEnum.MALL_VIP);
                break;
            }
            return sellLabel;
        }

        public static int GetSellPrice(market_data data)
        {
            if (PlayerAvatar.Player.vip_level > 0 && data.__price_vip > 0)
            {
                return data.__price_vip;
            }
            if (data.__price_now > 0)
            {
                return data.__price_now;
            }
            return data.__price_org;
        }

        public static market_data GetMarketDataById(int id)
        {
            return XMLManager.market_data[id];
        }

        public static UInt64 GetEndMilliseconds(market_data data)
        {
            UInt64 startMilliseconds = (UInt64)MogoTimeUtil.DateTime2ServerTimeStamp(data.__start_time);
            return startMilliseconds + (UInt64) (data.__duration * 1000);
        }
    }
}
