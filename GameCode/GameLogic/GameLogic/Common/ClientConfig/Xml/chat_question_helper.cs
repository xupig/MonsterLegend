﻿using System;
using System.Collections.Generic;

namespace GameData
{
    public class chat_question_helper
    {
        public static question_answer GetQuestionAnswerConfig(int id)
        {
            if (XMLManager.question_answer.ContainsKey(id))
            {
                return XMLManager.question_answer[id];
            }
            return null;
        }

        public static question GetQuestionConfig(int id)
        {
            if (XMLManager.question.ContainsKey(id))
            {
                return XMLManager.question[id];
            }
            return null;
        }
    }
}
