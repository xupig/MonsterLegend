﻿#region 模块信息
/*==========================================
// 文件名：skill_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 11:23:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class skill_helper
    {
        private static Dictionary<int,Dictionary<int,spell>> spellDict;
        private static Dictionary<int, List<int>> spellList;
        private static Dictionary<int, Dictionary<int, List<int>>> vocationPosSpellListDict;

        private static Dictionary<int, int> maxLevelDict;

        private static Dictionary<int, int> posNameDict;

        private static Dictionary<int, Dictionary<int, Dictionary<int, spell_sys>>> vocationSpellPositionDict;
        private static Dictionary<int, int> vocationNormalSkillDict;
        private static Dictionary<int, int> spell2PositionDict;
        private static Dictionary<int, int> spell2PositionSerniorDict;
        private static Dictionary<int, Dictionary<int, List<int>>> levelDict;
        private static Dictionary<int, Dictionary<int, List<int>>> taskDict;
        //private static Dictionary<int, Dictionary<int, int>> vocationGroupNameDict;

        public static int GetPosName(int pos)
        {
            if(posNameDict == null)
            {
                posNameDict = new Dictionary<int, int>();
                for (int i = 1; i <= 10;i++ )
                {
                    posNameDict.Add(i, 71299+i);
                }
            }
            return posNameDict[pos];
        }

        public static spell GetSeniorSpell(spell spell)
        {
            spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
            spell seniorSpell = null;
            if (sys.__position % 2 == 1)
            {
                spell_sys sysNext = skill_helper.GetSpellSysByPosition(PlayerAvatar.Player.vocation.ToInt(), sys.__proficient_group, sys.__position+1);
                if(sysNext!=null)
                {
                    seniorSpell = PlayerDataManager.Instance.SpellData.GetPositionSpell(sysNext);
                }
            }
            return seniorSpell;
        }

        public static int[] GetEffectAttri(spell spell)
        {
            int type = -1;
            int rate = 0;
            int addRate = 0;
            int level = spell.__sp_level;
            List<string> listString = data_parse_helper.ParseListString(spell.__actions_for_desc);
            for (int i = 0; i < listString.Count; i++)
            {
                spell_action action = XMLManager.spell_action[int.Parse(listString[i])];
                List<string> listString2 = data_parse_helper.ParseListString(action.__dmg_rate_lvl_factors);
                if (level >= 0 && level <= listString2.Count)
                {
                    float value = float.Parse(listString2[level - 1]);
                    float addValue = float.Parse(listString2[level - 1]);
                    rate += Mathf.FloorToInt(value * 100);
                    addRate += Mathf.FloorToInt(addValue);
                }
                List<string> listString3 = data_parse_helper.ParseListString(action.__dmg_types);
                for (int j = 0; j < listString3.Count; j++)
                {
                    int typeValue = int.Parse(listString3[j]);
                    if (type == -1)
                    {
                        type = typeValue;
                    }
                    else if (type != typeValue)
                    {
                        type = 0;
                    }
                }
            }
            return new int[] { rate, GetDamageType (type),addRate};
        }

        public static int GetDamageType(int type)
        {
            if(type>0)
            {
                return type + 71333;
            }
            return 71340;
        }


        public static spell_sys GetSpellSysByPosition(int vocation, int groupId, int position)
        {
            InitSpellSysData();
            if (vocationSpellPositionDict.ContainsKey(vocation))
            {
                if (vocationSpellPositionDict[vocation].ContainsKey(groupId))
                {
                    if (vocationSpellPositionDict[vocation][groupId].ContainsKey(position))
                    {
                        return vocationSpellPositionDict[vocation][groupId][position];
                    }
                }
            }
            return null;
        }

        //private static Dictionary<int, Dictionary<int, spell_sys>> playerSpellGroupDict;
        public static Dictionary<int, spell_sys> GetPlayerSpellListByGroup(int groupId)
        {
            InitSpellSysData();
            return vocationSpellPositionDict[PlayerAvatar.Player.vocation.ToInt()][groupId];
        }

        public static Dictionary<int,Dictionary<int, spell_sys>> GetPlayerAllSpellList()
        {
            InitSpellSysData();
            return vocationSpellPositionDict[PlayerAvatar.Player.vocation.ToInt()];
        }

        public static float GetCd(spell spell)
        {
            float cd = 0;
            List<int> listInt = data_parse_helper.ParseListInt(spell.__cd);
            if (listInt.Count > 0)
            {
                int data = listInt[0] / 100;
                cd = data / 10.0f;
            }
            return cd;
        }

        public static int GetOutput(spell spell)
        {
            if(spell.__use_costs!=null)
            {
                Dictionary<string, string> dict = data_parse_helper.ParseMap(spell.__use_costs);
                foreach (KeyValuePair<string, string> kvp in dict)
                {
                    return int.Parse(kvp.Value) / int.Parse(kvp.Key);
                }
            }
            return 0;
        }

        public static spell_sys GetSpellSysyByGroupId(int groupId)
        {
            InitSpellSysData();
            if (spell2PositionDict.ContainsKey(groupId))
            {
                return GetSpellSysy(spell2PositionDict[groupId]);
            }
            if (spell2PositionSerniorDict.ContainsKey(groupId))
            {
                return GetSpellSysy(spell2PositionSerniorDict[groupId]);
            }
            return null;
        }

        private static Dictionary<int, Dictionary<int, spell_proficient>> vocationSpellProficientDict;

        public static int GetVocationGroupName(int vocation,int group)
        {
            InitSpellProficient();
            return vocationSpellProficientDict[vocation][group].__name;
        }

        private static void InitSpellProficient()
        {
            if(vocationSpellProficientDict == null)
            {
                vocationSpellProficientDict = new Dictionary<int, Dictionary<int, spell_proficient>>();
                foreach(KeyValuePair<int,spell_proficient> kvp in XMLManager.spell_proficient)
                {
                    if(vocationSpellProficientDict.ContainsKey(kvp.Value.__vocation) == false)
                    {
                        vocationSpellProficientDict.Add(kvp.Value.__vocation, new Dictionary<int, spell_proficient>());
                    }
                    vocationSpellProficientDict[kvp.Value.__vocation].Add(kvp.Value.__group,kvp.Value);
                }
            }
        }

        public static spell_proficient GetSpellProficient(int vocation,int group)
        {
            InitSpellProficient();
            if (!vocationSpellProficientDict.ContainsKey(vocation))
            {
                return null;
            }
            if (vocationSpellProficientDict[vocation].ContainsKey(group))
            {
                return vocationSpellProficientDict[vocation][group];
            }
            return null;
        }

        public static Dictionary<int,spell_proficient> GetVocationSpellPrificientDict(int vocation)
        {
            InitSpellProficient();
            if (!vocationSpellProficientDict.ContainsKey(vocation))
            {
                return null;
            }
            return vocationSpellProficientDict[vocation];
        }

        private static void InitSpellSysData()
        {
            if (vocationSpellPositionDict == null)
            {
                vocationSpellPositionDict = new Dictionary<int, Dictionary<int, Dictionary<int, spell_sys>>>();
                vocationNormalSkillDict = new Dictionary<int, int>();
                spell2PositionDict = new Dictionary<int, int>();
                spell2PositionSerniorDict = new Dictionary<int, int>();
                //vocationGroupNameDict = new Dictionary<int, Dictionary<int, int>>();
                foreach (KeyValuePair<int, spell_sys> kvp in XMLManager.spell_sys)
                {
                    if(kvp.Value.__pos == 1)
                    {
                        if(vocationNormalSkillDict.ContainsKey(kvp.Value.__vocation) == false)
                        {
                            vocationNormalSkillDict.Add(kvp.Value.__vocation, kvp.Value.__id);
                        }
                    }
                    if (vocationSpellPositionDict.ContainsKey(kvp.Value.__vocation) == false)
                    {
                        vocationSpellPositionDict.Add(kvp.Value.__vocation, new Dictionary<int, Dictionary<int, spell_sys>>());
                        //vocationGroupNameDict.Add(kvp.Value.__vocation, new Dictionary<int, int>());
                    }
                    if (vocationSpellPositionDict[kvp.Value.__vocation].ContainsKey(kvp.Value.__proficient_group) == false)
                    {
                        vocationSpellPositionDict[kvp.Value.__vocation].Add(kvp.Value.__proficient_group, new Dictionary<int, spell_sys>());
                    }
                    if (spell2PositionDict.ContainsKey(kvp.Value.__before_proficient) == false)
                    {
                        spell2PositionDict.Add(kvp.Value.__before_proficient, kvp.Value.__id);
                    }
                    if (spell2PositionSerniorDict.ContainsKey(kvp.Value.__after_proficient) == false)
                    {
                        spell2PositionSerniorDict.Add(kvp.Value.__after_proficient, kvp.Value.__id);
                    }
                    if (kvp.Value.__position>0)
                    {
                        vocationSpellPositionDict[kvp.Value.__vocation][kvp.Value.__proficient_group].Add(kvp.Value.__position, kvp.Value);
                    }
                    
                }
            }
        }

        public static spell_sys GetSpellSysy(int spellSysId)
        {
            return XMLManager.spell_sys[spellSysId];
        }

        public static spell_sys GetVocationNormalSpell(int vocation)
        {
            InitSpellSysData();
            int spellId = vocationNormalSkillDict[vocation];
            return XMLManager.spell_sys[spellId];
        }

        public static spell GetSpell(int groupId,int level)
        {
            InitData();
            if (spellDict.ContainsKey(groupId) == false || spellDict[groupId].ContainsKey(level) == false)
            {
                return null;
            }
            return spellDict[groupId][level];
        }

        public static List<int> GetSpellGroupList(int groupId)
        {
            InitData();
            if (!spellList.ContainsKey(groupId))
            {
                return null;
            }
            return spellList[groupId];
        }

        public static string GetProfocoentEffect(spell spell)
        {
            List<string> dataList = data_parse_helper.ParseListString(spell.__actions_for_desc);
            float total = 0;
            for(int i=0;i<dataList.Count;i++)
            {
                int actionId = int.Parse(dataList[i]);
                spell_action action = XMLManager.spell_action[actionId];
                float factor = action.__dmg_proficient_factor;
                float attribute = PlayerAvatar.Player.GetAttribute((fight_attri_config)(190+PlayerAvatar.Player.spell_proficient));
                total += factor * attribute;
               // Debug.Log("GetProfocoentEffect:" + factor + "," + attribute);
            }
            //Debug.Log("GetProfocoentEffect:" + spell.__id + "," + spell.__name.ToLanguage() + "," + total + "," + PlayerAvatar.Player.spell_proficient);
            if(total == 0)
            {
                return string.Empty;
            }
            return string.Format("+{0}",Mathf.CeilToInt(total));
        }

        public static int GetSpellMaxLevel(int groupId)
        {
            InitData();
            return maxLevelDict[groupId];
        }

        public static List<int> GetVocationPosSpellListByLevel(int vocation,int level)
        {
            InitData();
            if (vocationPosSpellListDict.ContainsKey(vocation))
            {
                if (vocationPosSpellListDict[vocation].ContainsKey(level))
                {
                    return vocationPosSpellListDict[vocation][level];
                }
            }
            return null;
        }

        public static int GetTeamDutyIconId(Vocation vocation, int proficientGroupId)
        {
            foreach (KeyValuePair<int, spell_proficient> pair in XMLManager.spell_proficient)
            {
                if (vocation == (Vocation)pair.Value.__vocation && proficientGroupId == pair.Value.__group)
                {
                    return pair.Value.__team_duty_icon;
                }
            }
            return 0;
        }

        private static void InitData()
        {
            if (spellDict == null)
            {
                spellDict = new Dictionary<int, Dictionary<int, spell>>();
                spellList = new Dictionary<int, List<int>>();
                maxLevelDict = new Dictionary<int, int>();
                levelDict = new Dictionary<int, Dictionary<int, List<int>>>();
                taskDict = new Dictionary<int, Dictionary<int, List<int>>>();
                vocationPosSpellListDict = new Dictionary<int,Dictionary<int,List<int>>>();
                foreach(KeyValuePair<int,spell> kvp in XMLManager.spell)
                {
                    if (kvp.Value.__pos == 1 && kvp.Value.__group!=0)
                    {
                        spell_sys sys = GetSpellSysyByGroupId(kvp.Value.__group);
                        if (sys == null)
                        {
                            //Debug.LogError("技能" + kvp.Value.__id+"的专精组" + kvp.Value.__group + "不存在");
                        }
                        else
                        {
                            if (vocationPosSpellListDict.ContainsKey(kvp.Value.__vocation) == false)
                            {
                                vocationPosSpellListDict.Add(kvp.Value.__vocation, new Dictionary<int, List<int>>());
                            }
                            
                            if (vocationPosSpellListDict[kvp.Value.__vocation].ContainsKey(kvp.Value.__sp_level) == false)
                            {
                                vocationPosSpellListDict[kvp.Value.__vocation].Add(kvp.Value.__sp_level, new List<int>());
                            }
                            vocationPosSpellListDict[kvp.Value.__vocation][kvp.Value.__sp_level].Add(kvp.Value.__id);
                        }
                    }
                    
                    if (spellDict.ContainsKey(kvp.Value.__group) == false)
                    {
                        spellDict.Add(kvp.Value.__group, new Dictionary<int, spell>());
                        spellList.Add(kvp.Value.__group, new List<int>());
                        maxLevelDict.Add(kvp.Value.__group,kvp.Value.__sp_level);
                    }
                    if (spellDict[kvp.Value.__group].ContainsKey(kvp.Value.__sp_level) == false)
                    {
                        spellDict[kvp.Value.__group].Add(kvp.Value.__sp_level, kvp.Value);
                    }
                    spellList[kvp.Value.__group].Add(kvp.Value.__id);
                    maxLevelDict[kvp.Value.__group] = Math.Max(kvp.Value.__sp_level, maxLevelDict[kvp.Value.__group]);
                    if (levelDict.ContainsKey(kvp.Value.__vocation) == false)
                    {
                        levelDict.Add(kvp.Value.__vocation,new Dictionary<int,List<int>>());
                        taskDict.Add(kvp.Value.__vocation,new Dictionary<int,List<int>>());
                    }
                    if( kvp.Value.__study_cnd!=null)
                    {
                        Dictionary<string, string> dict = data_parse_helper.ParseMap(kvp.Value.__study_cnd);
                        if(dict.ContainsKey(LimitEnum.LIMIT_LEVEL.ToIntString()))
                        {
                            int level = int.Parse(dict[LimitEnum.LIMIT_LEVEL.ToIntString()]);
                            if (levelDict[kvp.Value.__vocation].ContainsKey(level) == false)
                            {
                                levelDict[kvp.Value.__vocation].Add(level, new List<int>());
                            }
                            levelDict[kvp.Value.__vocation][level].Add(kvp.Value.__id);
                        }
                        if (dict.ContainsKey(LimitEnum.LIMIT_TASK.ToIntString()))
                        {
                            int taskId = int.Parse(dict[LimitEnum.LIMIT_TASK.ToIntString()]);
                            if (taskDict[kvp.Value.__vocation].ContainsKey(taskId) == false)
                            {
                                taskDict[kvp.Value.__vocation].Add(taskId, new List<int>());
                            }
                            taskDict[kvp.Value.__vocation][taskId].Add(kvp.Value.__id);
                        }
                    }
                   
                }
            }
        }

    }
}
