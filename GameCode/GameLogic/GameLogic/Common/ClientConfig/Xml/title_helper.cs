﻿using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class title_helper
    {
        public static List<int> GetTitleIdList()
        {
            //List<int>
            List<int> list = XMLManager.title.Keys.ToList();
            list.Remove(0); // 去除模板行数据
            return list;
        }

        public static title GetTitleConfig(int titleId)
        {
            if (XMLManager.title.ContainsKey(titleId) != false)
            {
                return XMLManager.title[titleId];
            }
            else
            {
                LoggerHelper.Error(string.Format("title表中不存在id为{0}的配置", titleId));
                return null;
            }
        }

        public static string GetTitleName(int titleId)
        {
            title config = GetTitleConfig(titleId);
            if (config != null)
            {
                return MogoLanguageUtil.GetContent(config.__title_name);
            }
            return string.Empty;
        }

        public static string GetTitleDescribe(int titleId)
        {
            title config = GetTitleConfig(titleId);
            if (config != null)
            {
                return MogoLanguageUtil.GetContent(config.__title_describe);
            }
            return string.Empty;
        }

        public static Color GetTitleColor(int titleId)
        {
            title config = GetTitleConfig(titleId);
            if (config != null)
            {
                return ColorDefine.GetColorById(config.__quality);
            }
            return ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
        }

        public static Dictionary<int, float> GetTitleAttriEffect(int titleId)
        {
            title config = GetTitleConfig(titleId);
            if (config != null && config.__effect != 0)
            {
                return fight_force_helper.ChangeAttributeConvert(config.__effect, PlayerAvatar.Player.vocation);
            }
            return new Dictionary<int,float>();
        }

        public static int GetTitleIcon(int titleId)
        {
            title config = GetTitleConfig(titleId);
            return config.__icon;
        }

        public static int GetTitleTimeLimit(int titleId)
        {
            title config = GetTitleConfig(titleId);
            return config.__time_limit;
        }
    }
}
