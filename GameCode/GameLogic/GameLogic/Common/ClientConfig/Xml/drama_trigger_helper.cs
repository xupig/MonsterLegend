﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using GameMain.GlobalManager;


namespace GameData
{
    /// <summary>
    /// 对应配置表 drama_trigger_cg.xml 和 drama_trigger_guide.xml
    /// </summary>
    public class drama_trigger_helper
    {
        private static Dictionary<string, Dictionary<string, List<DramaTriggerData>>> _triggerDramaMap = null; 

        public static drama_trigger GetConfig(int dataID)
        {
            if (XMLManager.buff.ContainsKey(dataID) == false)
            {
                throw new Exception("配置表 drama_trigger.xml 未找到配置项: " + dataID.ToString());
            }
            return XMLManager.drama_trigger[dataID];
        }

        public static void OnReloadData()
        {
            _triggerDramaMap = null;
            InitTriggerDramaMap();
        }

        private static void InitTriggerDramaMap()
        {
            if (_triggerDramaMap != null) return;
            _triggerDramaMap = new Dictionary<string, Dictionary<string, List<DramaTriggerData>>>();
            AddDramaTriggerCGData(XMLManager.drama_trigger_cg);
            AddDramaTriggerGuideData(XMLManager.drama_trigger_guide);
        }

        private static void AddDramaTriggerCGData(GameDataTable<int,drama_trigger_cg> cgDict)
        {
            foreach(KeyValuePair<int,drama_trigger_cg> kvp in cgDict)
            {
                if (!_triggerDramaMap.ContainsKey(kvp.Value.__eventname))
                {
                    _triggerDramaMap.Add(kvp.Value.__eventname, new Dictionary<string, List<DramaTriggerData>>());
                }
                if (!_triggerDramaMap[kvp.Value.__eventname].ContainsKey(kvp.Value.__eventvalue))
                {
                    _triggerDramaMap[kvp.Value.__eventname].Add(kvp.Value.__eventvalue, new List<DramaTriggerData>());
                }
                DramaTriggerData data = new DramaTriggerData();
                data.id = kvp.Value.__id;
                data.mark = kvp.Value.__mark;
                data.eventname = kvp.Value.__eventname;
                data.eventvalue = kvp.Value.__eventvalue;
                data.drama = kvp.Value.__drama;
                data.dramaType = DramaType.CG;
                _triggerDramaMap[kvp.Value.__eventname][kvp.Value.__eventvalue].Add(data);
            }
        }


        private static void AddDramaTriggerGuideData(GameDataTable<int, drama_trigger_guide> cgDict)
        {
            foreach (KeyValuePair<int, drama_trigger_guide> kvp in cgDict)
            {
                if(kvp.Value.__eventname != null )
                {
                    if (!_triggerDramaMap.ContainsKey(kvp.Value.__eventname))
                    {
                        _triggerDramaMap.Add(kvp.Value.__eventname, new Dictionary<string, List<DramaTriggerData>>());
                    }
                    if (!_triggerDramaMap[kvp.Value.__eventname].ContainsKey(kvp.Value.__eventvalue))
                    {
                        _triggerDramaMap[kvp.Value.__eventname].Add(kvp.Value.__eventvalue, new List<DramaTriggerData>());
                    }
                    DramaTriggerData data = new DramaTriggerData();
                    data.id = kvp.Value.__id;
                    data.mark = kvp.Value.__mark;
                    data.eventname = kvp.Value.__eventname;
                    data.eventvalue = kvp.Value.__eventvalue;
                    data.drama = kvp.Value.__drama;
                    data.dramaType = DramaType.GUIDE;
                    _triggerDramaMap[kvp.Value.__eventname][kvp.Value.__eventvalue].Add(data);
                }
            }
        }

        public static List<DramaTriggerData> GetDramaTriggers(string eventname, string eventvalue)
        {
            InitTriggerDramaMap();
            if (_triggerDramaMap.ContainsKey(eventname) && _triggerDramaMap[eventname].ContainsKey(eventvalue))
            {
                return _triggerDramaMap[eventname][eventvalue];
            }
            return null;
        }
    }
}
