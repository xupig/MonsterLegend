﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/23 14:25:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ServerConfig;
using UnityEngine;
using GameMain.Entities;
using Common.Utils;

namespace GameData
{
    public enum PriceListId
    {
        copyReset = 10,
        energyBuy = 11,
        alchemy = 16,
        guildWarDraw = 18,
        wanderlandReset = 17,
        makeGift = 19,

    }

    public class price_list_helper
    {
        public static price_list GetConfig(int id)
        {
            if (!XMLManager.price_list.ContainsKey(id))
            {
                throw new Exception("配置表 price_list.xml 未包含项 " + id.ToString());
            }
            return XMLManager.price_list[id];
        }

        public static BaseItemData GetCost(PriceListId id, int num = 1)
        {
            return GetCost((int)id, num);
        }

        public static BaseItemData GetCost(int id, int num = 1)
        {
            price_list priceList = GetConfig(id);
            BaseItemData baseItemData = ItemDataCreator.Create(priceList.__currency_type);
            int value = 0;
            int max = -1;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(priceList.__price_list);

            foreach (KeyValuePair<string, string> kvp in dataDict)
            {
                int currentCount = int.Parse(kvp.Key);
                if (max < currentCount &&  currentCount <= num)
                {
                    max = currentCount;
                    value = int.Parse(kvp.Value);
                }
            }
            baseItemData.StackCount = value;
            return baseItemData;
        }

        public static BaseItemData GetCostByRange(int id, int min, int max)
        {
            price_list priceList = GetConfig(id);
            BaseItemData baseItemData = ItemDataCreator.Create(priceList.__currency_type);
            for (int i = min; i <= max; i++)
            {
                baseItemData.StackCount += GetCost(id, i).StackCount;
            }
            return baseItemData;
        }

        public static int GetPrice(int id, int hasBuyCount)
        {
            if (XMLManager.price_list.ContainsKey(id) == false)
            {
                throw new Exception("次数定价表中不包含ID为:" + id + "的项");
            }
            else
            {
                price_list value = XMLManager.price_list[id];
                Dictionary<string, string> dict = data_parse_helper.ParseMap(value.__price_list);
                List<int> list = ConvertStringListToIntList(dict.Keys.ToList());
                int index = FindIndex(list, hasBuyCount + 1);
                return int.Parse(dict[index.ToString()]);
            }
        }

        private static int FindIndex(List<int> list, int hasBuyCount)
        {
            int result = 0;
            list.Sort();
            for (int i = 0; i < list.Count; i++)
            {
                if (hasBuyCount >= list[i])
                {
                    result = list[i];
                }
            }
            return result;
        }

        private static List<int> ConvertStringListToIntList(List<string> list)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(int.Parse(list[i]));
            }
            return result;
        }

    }
}
