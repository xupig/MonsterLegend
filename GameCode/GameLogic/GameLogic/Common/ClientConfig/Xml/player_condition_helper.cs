﻿#region 模块信息
/*==========================================
// 文件名：player_condition_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/22 10:03:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;
namespace GameData
{
    //private static Dict
    public enum PlayerCondtion
    {
        Level = 1,
        Vocation = 2,
        VipLevel = 3,
        TaskFinishOrReward = 4,
        Gender = 5,
        FightForce = 6,
        Skill = 7,
        Fortress = 8, 
        TaskGetOrFinishOrReward = 9,
        PassChapter = 10,
        TaskGetReward = 13,
        FortressBuilding = 15,
        Buff = 40,
        Equip = 50,
    }

    public class player_condition_helper
    {
        private static Dictionary<int, int> fortressBuildingDict;

        private static void InitFortressBuildingDict()
        {
            fortressBuildingDict = new Dictionary<int,int>();
            fortressBuildingDict.Add(20, 74015);
            fortressBuildingDict.Add(21, 74019);
            fortressBuildingDict.Add(25, 74027);
            fortressBuildingDict.Add(26, 74011);
        }

        private static string GetFortressName(int fortressFunctionId)
        {
            if (fortressBuildingDict == null)
            {
                InitFortressBuildingDict();
            }
            if (fortressBuildingDict.ContainsKey(fortressFunctionId))
            {
                return MogoLanguageUtil.GetContent(fortressBuildingDict[fortressFunctionId]);
            }
            UnityEngine.Debug.LogError("@蔡文超 要塞配置表中缺少OpenFunctionID " + fortressFunctionId);
            return string.Empty;
        }

        /// <summary>
        /// 判断玩家是否满足条件
        /// </summary>
        /// <param name="conditionDict"> 条件列表 </param>
        /// Key : 条件ID  Value : 条件值
        /// <returns></returns>
        public static bool JudgePlayerCondition(Dictionary<string, string> conditionDict)
        {
            foreach (var pair in conditionDict)
            {
                if (JudgeSingleCondition(int.Parse(pair.Key), int.Parse(pair.Value)) == false)
                {
                    return false;
                }
            }
            return true;
        }

        public static string GetConditionDescribe(Dictionary<string, string> condition)
        {
            string valueString = string.Empty;
            foreach(var pair in condition)
            {
                if (XMLManager.player_condition.ContainsKey(int.Parse(pair.Key)))
                {
                    int chineseId = XMLManager.player_condition[int.Parse(pair.Key)].__name;
                    if (int.Parse(pair.Key) == (int)PlayerCondtion.PassChapter)
                    {
                        valueString = chapters_helper.GetName(int.Parse(pair.Value));
                    }
                    else if (int.Parse(pair.Key) == (int)PlayerCondtion.FortressBuilding)
                    {
                        string descString = MogoLanguageUtil.GetContent(98174);
                        valueString = string.Format(descString, GetFortressName(int.Parse(pair.Value)));
                        return valueString;
                    }
                    else if (int.Parse(pair.Key) == (int)PlayerCondtion.TaskFinishOrReward 
                        || int.Parse(pair.Key) == (int)PlayerCondtion.TaskGetOrFinishOrReward
                        || int.Parse(pair.Key) == (int)PlayerCondtion.TaskGetReward)
                    {
                        valueString = task_data_helper.GetTaskName(task_data_helper.GetTaskData(int.Parse(pair.Value)));
                    }
                    else
                    {
                        valueString = pair.Value;
                    }
                    return string.Format(MogoLanguageUtil.GetContent(chineseId), valueString);
                }
                else
                {
                    Debug.LogError("@策划 玩家条件限制表中不包含该条件");
                }
                
            }
            return string.Empty;
        }

        public static bool JudgeSingleCondition(int conditionId, int value)
        {
            switch ((PlayerCondtion)conditionId)
            {
                case PlayerCondtion.Level:
                    return JudgeLevel(value);
                case PlayerCondtion.Vocation:
                    return JudgeVocation(value);
                case PlayerCondtion.VipLevel:
                    return JudgeVipLevel(value);
                case PlayerCondtion.TaskFinishOrReward:
                    return JudgeTaskFinishOrReward(value);
                case PlayerCondtion.Gender:
                    return JudgeGender(value);
                case PlayerCondtion.FightForce:
                    return JudgeFightForce(value);
                case PlayerCondtion.Skill:
                    return JudgeSkill(value);
                case PlayerCondtion.Fortress:
                    return JudgeFortress(value);
                case PlayerCondtion.TaskGetOrFinishOrReward:
                    return JudgeTaskFinish(value);
                case PlayerCondtion.PassChapter:
                    return JudgePassChapter(value);
                case PlayerCondtion.FortressBuilding:
                    return JudgeFortressBuilding(value);
                case PlayerCondtion.Buff:
                    return JudgeBuff(value);
                case PlayerCondtion.Equip:
                    return JudgeEquip(value);
                default:
                    return false;
            }
        }

        private static bool JudgeFortressBuilding(int value)
        {
            return PlayerDataManager.Instance.FunctionData.IsFunctionOpen(value);
        }

        private static bool JudgeEquip(int value)
        {
            return PlayerDataManager.Instance.BagData.GetBagData(BagType.BodyEquipBag).HasItem(value);
        }

        private static bool JudgeBuff(int value)
        {
            return PlayerAvatar.Player.bufferManager.ContainsBuffer(value);
        }

        private static bool JudgePassChapter(int value)
        {
            return PlayerDataManager.Instance.CopyData.IsChapterPass(value);
        }

        private static bool JudgeTaskFinish(int value)
        {
            return PlayerDataManager.Instance.TaskData.IsTaskAccepted(value);
        }

        private static bool JudgeFortress(int value)
        {
            Debug.LogError("JudgeFortress已经弃用，building = " + value);
            return false;
        }

        private static bool JudgeSkill(int value)
        {
            return PlayerDataManager.Instance.SpellData.HasSkillLearned(value);
        }

        private static bool JudgeFightForce(int value)
        {
            if (PlayerAvatar.Player.fight_force >= value)
            {
                return true;
            }
            return false;
        }

        private static bool JudgeTaskFinishOrReward(int value)
        {
            if (PlayerDataManager.Instance.TaskData.IsCompleted(value) == true)
            {
                return true;
            }
            return false;
        }

        private static bool JudgeGender(int value)
        {
            if (PlayerAvatar.Player.gender == (Gender)value)
            {
                return true;
            }
            return false;
        }

        private static bool JudgeVipLevel(int value)
        {
            if (PlayerAvatar.Player.vip_level >= value)
            {
                return true;
            }
            return false;
        }

        private static bool JudgeVocation(int value)
        {
            if (PlayerAvatar.Player.vocation == (Vocation)value)
            {
                return true;
            }
            return false;
        }

        private static bool JudgeLevel(int value)
        {
            if (PlayerAvatar.Player.level >= value)
            {
                return true;
            }
            return false;
        }
    }

}