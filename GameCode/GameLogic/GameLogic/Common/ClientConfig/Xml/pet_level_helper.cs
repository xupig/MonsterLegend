﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 21:51:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class PetLevelDict
    {
        Dictionary<int, int> _levelDict = new Dictionary<int,int>();

        public int GetNextExp(int level)
        {
            return _levelDict[level];
        }

        public void AddExp(int level, int nextExp)
        {
            _levelDict.Add(level, nextExp);
        }
    }

    public class pet_level_helper
    {
        private static Dictionary<int, PetLevelDict> _petIdDict = new Dictionary<int, PetLevelDict>();
        static pet_level_helper()
        {
            foreach (int key in XMLManager.pet_level.Keys)
            {
                pet_level petLevel = GetPetLevelConf(key);
                PetLevelDict petLevelDict;
                if (_petIdDict.ContainsKey(petLevel.__pet_id))
                {
                    petLevelDict = _petIdDict[petLevel.__pet_id];
                }
                else
                {
                    petLevelDict = new PetLevelDict();
                    _petIdDict.Add(petLevel.__pet_id, petLevelDict);
                }
                petLevelDict.AddExp(petLevel.__level, petLevel.__next_level_exp);
            }
        }

        public static void Init()
        {
            int count = XMLManager.pet_level.Count;
        }

        private static pet_level GetPetLevelConf(int id)
        {
            return XMLManager.pet_level[id];
        }

        public static float GetExpPercent(int petId, int exp, int level)
        {
            float nextLevelExp = GetNextLevelExp(petId, level);
            if (nextLevelExp == 0)
            {
                return 0;
            }
            return exp / nextLevelExp;
        }

        public static int GetNextLevelExp(int petId, int level)
        {
            return _petIdDict[petId].GetNextExp(level);
        }

        public static bool IsMaxLevel(int petId, int level)
        {
            return GetNextLevelExp(petId, level) == 0;
        }

        public static LevelConfig GetLastLevelConfig(int petId, int nowLevel, int nowExp, int addExp)
        {
            LevelConfig avatarLevel = new LevelConfig(nowLevel, nowExp);
            if ((nowLevel == 1) && (nowExp < addExp))
            {
                Debug.LogError("服务器没有加宠物经验！！！");
                return avatarLevel;
            }
            if (nowExp >= addExp)
            {
                avatarLevel.Exp -= addExp;
            }
            else
            {
                int leftAddExp = addExp;
                leftAddExp -= nowExp;
                avatarLevel.Level -= 1;
                while (true)
                {
                    int nextExp = GetNextLevelExp(petId, avatarLevel.Level);
                    if (leftAddExp > nextExp)
                    {
                        avatarLevel.Level -= 1;
                        if (avatarLevel.Level == 0)
                        {
                            avatarLevel.Level = 1;
                            avatarLevel.Exp = 0;
                            break;
                        }
                    }
                    else
                    {
                        avatarLevel.Exp = nextExp - leftAddExp;
                    }
                    leftAddExp -= nextExp;
                    if (leftAddExp <= 0)
                    {
                        break;
                    }
                }
            }
            return avatarLevel;
        }
    }
}
