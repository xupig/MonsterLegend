﻿#region 模块信息
/*==========================================
// 文件名：function_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/21 20:18:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public enum FunctionId
    {
        invalid = 0,
        pet = 1,
        equip = 2,
        rune = 3,
        starsoul = 4,
        mall = 6,
        WorldMap = 8,
        reward = 11,
        welfare = 12,
        gem = 13,
        task = 14,
        interactive = 17,
        guild = 18,
        make = 20,
        tradeMarket = 21,
        demongate = 28,
        dreamland = 29,
        Charge = 34,
        fightForceIncrease = 36,
        alchemy = 39,
        favor = 40,
        tutor = 41,
        Festival = 43,
        enchant = 202,
        smelter = 203,
        suit = 204,
        challenge = 901,
        rewardcompensation = 1100,
        dailyactive = 1101,
        dailytask = 1102,
        monthloginreward = 1103,
        vipgift = 1104,
        Achievement = 1105,
        TimeAltar = 1106,
        openServerActivity = 1202,
        chargeActivity = 1203,
        gemInlay = 1301,
        gemComposite = 1302,
        gemSpirit = 1303,
        team_task = 1401,
        myGuild = 1801,
        friend = 1701,
        title = 3204,
        charge = 3401,
        VIP = 3403,
        smelt = 20201,
        EquipRecast = 20301,
        risingStar = 20302,
    }


    public class function_helper
    {
        private const string OPEN_VIEW = "1";
        private const string OPEN_MAP = "2";
        private const string TALK_TO_NPC = "3";
        private const string TRIGGER_EVENT = "4";

        private static Vector3 startPos1 = new Vector3(28, -34, 0);
        private static Vector3 startPos2 = new Vector3(28, -150, 0);
        private static Vector3 startPos3 = new Vector3(28, -395, 0);
        private static Vector3 startPos4 = new Vector3(28, -515, 0);

        private static Vector3 start2 = new Vector3(620 - 1113, -14 + 98, 0);
        private static Vector3 start3 = new Vector3(1131, -9, 0);

        private const int ICON_WIDTH = 105;
        private const int ICON_HEIGHT = 120;
        private const int OFFSETX = -20;
        private const int OFFSETY = 20;
        /// <summary>
        /// "0无按钮
        //1，右下角默认显示横排；
        //5，右下角默认显示竖排；
        //2，右上一排默认显示区域；
        //3，要塞区域；
        //4，右下角点击展开后区域横排；
        //6，右下角展开展开区域竖排"

        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Vector3 GetStartPosition(int type)
        {
            if (type == 1)
            {
                return startPos1;
            }
            else if (type == 5)
            {
                return startPos2;
            }
            else if (type == 4)
            {
                return startPos3;
            }
            else if (type == 6)
            {
                return startPos4;
            }
            else if (type == 2)
            {
                return start2;
            }
            else
            {
                return start3;
            }
        }

        public static Vector3 GetStartPosition(RectTransform rect, int type, int num)
        {
            Vector3 position = rect.localPosition;
            if (type == 2)
            {
                position.x = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(rect.parent as RectTransform, new Vector2(Screen.width, 0)).x + (num + 1) * ICON_WIDTH;
                return position;
            }
            else
            {
                return Vector3.zero;
            }
        }

        public static Vector3 GetPosition(int type, int num)
        {
            Vector3 offset = Vector3.zero;
            Vector3 startPos = GetStartPosition(type);
            if (type == 1 || type == 4 || type == 6)
            {
                offset.x = startPos.x + (num - 1) * ICON_WIDTH;
                offset.y = startPos.y;
            }
            else if (type == 5)
            {
                offset.x = startPos.x + ((num - 1) % 4) * ICON_WIDTH;
                offset.y = startPos.y - ((num - 1) / 4) * ICON_HEIGHT;
            }
            else if (type == 2)
            {
                offset.x = -227 + (5 - num % 6) * (ICON_WIDTH - 5);
                offset.y = 98 + 94 * (num / 6);
            }
            else if (type == 3)
            {
                offset.x = num == 0 ? 0 : 108;
                offset.y = num == 0 ? 10 : 17 + num * -110;
            }
            return offset;
        }

        private static Dictionary<int, List<int>> dataDict;

        public static List<int> GetFunctionTabList(int functionId)
        {
            if (dataDict == null)
            {
                dataDict = new Dictionary<int, List<int>>();
                foreach (KeyValuePair<int, function> kvp in XMLManager.function)
                {
                    if (kvp.Value.__belong_system != 0)
                    {
                        if (dataDict.ContainsKey(kvp.Value.__belong_system) == false)
                        {
                            dataDict.Add(kvp.Value.__belong_system, new List<int>());
                        }
                        dataDict[kvp.Value.__belong_system].Add(kvp.Key);
                    }
                }
                foreach (KeyValuePair<int, List<int>> kvp in dataDict)
                {
                    kvp.Value.Sort(SortFunc);
                }
            }
            if (dataDict.ContainsKey(functionId))
            {
                return dataDict[functionId];
            }
            else
            {
                return null;
            }
        }

        private static int SortFunc(int id1, int id2)
        {
            function func1 = GetFunction(id1);
            function func2 = GetFunction(id2);
            return func1.__tab - func2.__tab;
        }

        public static bool IsFunctionOpen(FunctionId functionId)
        {
            return IsFunctionOpen((int)functionId);
        }

        public static bool IsFunctionOpen(int functionId)
        {
            function func = GetFunction(functionId);
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(func.__condition);
            LimitEnum result = PlayerAvatar.Player.CheckLimit<string>(dataDict);
            return result == LimitEnum.NO_LIMIT;
        }

        public static List<int> GetHideTabList(int functionId)
        {
            List<int> tabList = GetFunctionTabList(functionId);
            return GetHideTabList(tabList);
        }

        public static List<int> GetHideTabList(List<int> tabList)
        {
            List<int> hideList = new List<int>();
            for (int i = 0; i < tabList.Count; i++)
            {
                if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(tabList[i]) == false)
                {
                    hideList.Add(function_helper.GetFunction(tabList[i]).__tab);
                }
            }
            return hideList;
        }

        public static function GetFunction(int functionId)
        {
            if (XMLManager.function.ContainsKey(functionId))
            {
                return XMLManager.function[functionId];
            }
            UnityEngine.Debug.LogError("@张敬 功能配置表中缺少ID为 : " + functionId + " 的配置选项");
            return null;
        }

        public static void Follow(int functionId, List<string> fid_arg)
        {
            function function = GetFunction(functionId);
            Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList( function.__follow);
            foreach (KeyValuePair<string, string[]> kvp in dataDictList)
            {
                Follow(kvp.Key, kvp.Value, fid_arg);
            }
        }

        private static Dictionary<int, List<function>> _functionListDict;
        public static List<function> GetFunctionList(int type)
        {
            if (_functionListDict == null)
            {
                _functionListDict = new Dictionary<int, List<function>>();
                foreach (KeyValuePair<int, function> kvp in XMLManager.function)
                {
                    if (_functionListDict.ContainsKey(kvp.Value.__type) == false)
                    {
                        _functionListDict.Add(kvp.Value.__type, new List<function>());
                    }
                    _functionListDict[kvp.Value.__type].Add(kvp.Value);
                }
                foreach (KeyValuePair<int, List<function>> kvp in _functionListDict)
                {
                    kvp.Value.Sort(SortFunc);
                }
            }
            if (_functionListDict.ContainsKey(type))
            {
                return _functionListDict[type];
            }
            return null;
        }

        private static int SortFunc(function func1, function func2)
        {
            return func1.__button_sequence - func2.__button_sequence;
        }

        public static void ActivityFollow(int open, int activityId, List<string> fid_arg)
        {
            if (open >= 10000)
            {
                EventDispatcher.TriggerEvent(CopyEvents.SHOW_COPY, open, activityId);
            }
            else
            {
                Follow(open, fid_arg);
            }
        }

        public static string GetConditionDesc(FunctionId functionId)
        {
            return GetConditionDesc(functionId);
        }
        public static string GetConditionDesc(int functionId)
        {
            if (XMLManager.function.ContainsKey(functionId))
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(XMLManager.function[functionId].__condition);
                return player_condition_helper.GetConditionDescribe(dataDict);
            }
            return string.Empty;
        }

        public static void Follow(string key, string[] value, List<string> fid_arg)
        {
            switch (key)
            {
                case OPEN_VIEW:
                    if (fid_arg != null && fid_arg.Count > 0)
                    {
                        view_helper.OpenView(int.Parse(value[0]), fid_arg);
                    }
                    else
                    {
                        view_helper.OpenView(int.Parse(value[0]));

                    }
                    break;
                case OPEN_MAP:
                    int mapId = value.Length >= 2 ? int.Parse(value[1]) : 0;
                    EventDispatcher.TriggerEvent(CopyEvents.SHOW_COPY, mapId, 0);
                    //view_helper.OpenMapEntry(int.Parse(value[0]), mapId);
                    break;
                case TALK_TO_NPC:
                    EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, int.Parse(value[0]), true);
                    break;
                case TRIGGER_EVENT:
                    EventDispatcher.TriggerEvent(value[0]);
                    break;
                default:
                    break;
            }
        }

        public static function GetConfig(FunctionId id)
        {
            if (!XMLManager.function.ContainsKey((int)id))
            {
                throw new Exception("function 配置表不存在id:" + id);
            }
            return XMLManager.function[(int)id];
        }

        public static int GetIconId(FunctionId id)
        {
            return GetConfig(id).__icon;
        }

        public static int GetIconId(int functionID)
        {
            if (XMLManager.function.ContainsKey(functionID))
            {
                return XMLManager.function[functionID].__icon;
            }
            return 0;
        }

        public static string GetName(int functionId)
        {
            return MogoLanguageUtil.GetContent(GetConfig((FunctionId)functionId).__name);
        }

        public static bool NeedPushNoticeToMainUI(int functionId)
        {
            if (XMLManager.function.ContainsKey(functionId))
            {
                return XMLManager.function[functionId].__need_notice == 1;
            }
            return false;
        }
    }
}
