﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/11 14:24:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class inst_match_rule_helper
    {
        public static inst_match_rule GetMatchRule(int id)
        {
            if (XMLManager.inst_match_rule.ContainsKey(id))
            {
                return XMLManager.inst_match_rule[id];
            }
            return null;
        }

        public static int GetWaitMaxTime(int id)
        {
            return GetMatchRule(id).__wait_max_time;
        }

    }
}
