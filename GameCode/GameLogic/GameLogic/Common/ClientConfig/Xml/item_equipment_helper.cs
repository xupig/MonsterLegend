﻿using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class item_equipment_helper
    {
        public static void Init()
        {
            int count = XMLManager.item_equipment.Count;
        }

        public static bool CheckEquipFightForceBetter(EquipItemInfo equipItem)
        {
            if (CheckPlayerCondition(equipItem) == false)
            {
                return false;
            }
            if (equipItem.SubType == EquipType.ring)
            {
                return CheckRingCanAdd(equipItem);

            }
            else
            {
                return CheckEquipCanWear(equipItem);
            }
        }

        private static bool CheckPlayerCondition(EquipItemInfo equipItem)
        {
            if (equipItem.UsageLevelRequired > PlayerAvatar.Player.level)
            {
                return false;
            }
            else if (equipItem.UsageVipLevelRequired > PlayerAvatar.Player.vip_level)
            {
                return false;
            }
            else if (equipItem.UsageVocationRequired != PlayerAvatar.Player.vocation)
            {
                return false;
            }
            return true;
        }

        private static bool CheckEquipCanWear(EquipItemInfo equipItem)
        {
            Dictionary<int, EquipItemInfo> _equipDict = GetBodyEquipDict();
            if (_equipDict.ContainsKey((int)equipItem.SubType) == false)
            {
                return true;
            }
            else if (_equipDict[(int)equipItem.SubType].FightForce < equipItem.FightForce)
            {
                return true;
            }
            return false;
        }

        private static Dictionary<int, EquipItemInfo> GetBodyEquipDict()
        {
            Dictionary<int, EquipItemInfo> result = new Dictionary<int, EquipItemInfo>();
            int start = public_config.EQUIP_POS_WEAPON;
            int end = public_config.EQUIP_POS_RIGHTRING;
            for (int i = start; i <= end; i++)
            {
                EquipItemInfo bodyEquip = PlayerDataManager.Instance.BagData.GetEquipedItem(i);
                if (bodyEquip != null)
                {
                    result.Add(i, bodyEquip);
                }
            }
            return result;
        }

        private static bool CheckRingCanAdd(EquipItemInfo equipItem)
        {
            Dictionary<int, EquipItemInfo> _equipDict = GetBodyEquipDict();
            if (_equipDict.ContainsKey(public_config.EQUIP_POS_LEFTRING) == false)
            {
                return true;
            }
            else if (_equipDict.ContainsKey(public_config.EQUIP_POS_RIGHTRING) == false)
            {
                return true;
            }
            else
            {
                EquipItemInfo leftRing = _equipDict[public_config.EQUIP_POS_LEFTRING];
                EquipItemInfo rightRing = _equipDict[public_config.EQUIP_POS_RIGHTRING];
                if (leftRing.FightForce > rightRing.FightForce)
                {
                    if (equipItem.FightForce > rightRing.FightForce)
                    {
                        _equipDict[public_config.EQUIP_POS_RIGHTRING] = equipItem;
                        return true;
                    }
                }
                else
                {
                    if (equipItem.FightForce > leftRing.FightForce)
                    {
                        _equipDict[public_config.EQUIP_POS_LEFTRING] = equipItem;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
