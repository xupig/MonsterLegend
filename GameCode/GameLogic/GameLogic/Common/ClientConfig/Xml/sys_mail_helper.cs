﻿using Common.Utils;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class sys_mail_helper
    {
        public static string GetSysMailFrom(int sysMailId)
        {
            if (XMLManager.sys_mail.ContainsKey(sysMailId) == true)
            {
                return XMLManager.sys_mail[sysMailId].__from;
            }
            else
            {
                LoggerHelper.Error(string.Format("系统邮件配置错误，缺少邮件类型id为{0}", sysMailId));
                ///37748未知来源
                return MogoLanguageUtil.GetContent(37748);
            }
        }

        public static string GetSysMailTitle(int sysMailId)
        {
            if (XMLManager.sys_mail.ContainsKey(sysMailId) == true)
            {
                return XMLManager.sys_mail[sysMailId].__title;
            }
            else
            {
                LoggerHelper.Error(string.Format("系统邮件配置错误，缺少邮件类型id为{0}", sysMailId));
                ///37748未知来源
                return MogoLanguageUtil.GetContent(37748);
            }
        }

        public static string GetSysMailContent(int sysMailId, params object[] param)
        {
            if (XMLManager.sys_mail.ContainsKey(sysMailId) == true)
            {
                return SystemInfoUtil.GenerateContent(XMLManager.sys_mail[sysMailId].__txt, param);                  
            }
            else
            {
                LoggerHelper.Error(string.Format("系统邮件配置错误，缺少邮件类型id为{0}", sysMailId));
                ///37748未知来源
                return MogoLanguageUtil.GetContent(37748);
            }
        }
    }
}
