﻿
namespace GameData
{
    public class drop_item_helper
    {
        private static int _clientDropItemFxID = 0;
        private static float _clientDropItemFxSpeed = 0;

        public static int GetClientDropItemFxID()
        {
            InitClientDropItemInfo();
            return _clientDropItemFxID;
        }

        public static float GetClientDropItemFxSpeed()
        {
            InitClientDropItemInfo();
            return _clientDropItemFxSpeed;
        }

        private static void InitClientDropItemInfo()
        {
            if (_clientDropItemFxID == 0 || _clientDropItemFxSpeed == 0)
            {
                string[] dropItemInfoArray = global_params_helper.GetGlobalParam(159).Split(',');
                _clientDropItemFxID = int.Parse(dropItemInfoArray[0]);
                _clientDropItemFxSpeed = float.Parse(dropItemInfoArray[1]);
            }
        }

        private static float _dropItemDuration = 0;
        private static float _dropItemMinSpeed = 0;

        public static float GetDropItemDuration()
        {
            InitDropItemInfo();
            return _dropItemDuration;
        }

        public static float GetDropItemMinSpeed()
        {
            InitDropItemInfo();
            return _dropItemMinSpeed;
        }

        private static void InitDropItemInfo()
        {
            if (_dropItemDuration == 0 || _dropItemMinSpeed == 0)
            {
                string[] dropItemInfoArray = global_params_helper.GetGlobalParam(187).Split(',');
                _dropItemDuration = float.Parse(dropItemInfoArray[0]);
                _dropItemMinSpeed = float.Parse(dropItemInfoArray[1]);
            }
        }
    }
}
