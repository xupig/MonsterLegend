﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class replace_helper
    {
        private static Dictionary<int, List<int>> _itemReplaceDict;


        public static List<int> GetReplaceItem(int itemId)
        {
            if (_itemReplaceDict == null)
            {
                _itemReplaceDict = new Dictionary<int, List<int>>();
                GameDataTable<int, item_replace> replaceDic = XMLManager.item_replace;
                foreach (KeyValuePair<int, item_replace> kvp in replaceDic)
                {
                    var items = data_parse_helper.ParseListInt(kvp.Value.__items);
                    int count = data_parse_helper.ParseListInt(kvp.Value.__items).Count;
                    for (int i = count - 1; i >= 0; i--)
                    {
                        List<int> replaceList = new List<int>();
                        for (int j = i; j >= 0; j--)
                        {
                            replaceList.Add(items[j]);
                        }
                        _itemReplaceDict[items[i]] = replaceList;
                    }
                }
            }

            if (_itemReplaceDict.ContainsKey(itemId) == true)
            {
                return _itemReplaceDict[itemId];
            }
            List<int> result = new List<int>();
            result.Add(itemId);
            return result;
        }
    }
}
