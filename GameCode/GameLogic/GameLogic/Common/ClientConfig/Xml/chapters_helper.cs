﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Utils;
using Common.ExtendComponent;

namespace GameData
{
    public enum ChapterType
    {
        MainTown = 0,//主城
        Field = 1,//野外
        Copy = 2,
        WanderLand = 3,
        PetDefence = 4,//单人本 （魔灵本等）
        DemonGate = 5,//恶魔之门
        TeamCopy = 7,
        EveningActivity = 8,
        GuildCopy = 9,
        Single = 10,//幻境探索打劫
        NewPlayer = 11,//体验关
        Portal = 12,//传送门
        PK = 13,//PK
        CombatPortal = 14,  //争夺型传送门
        GuildTry = 15,  //公会战试炼
        GuildWaitSingle = 16,//公会竞技场单独候场
        GuildWaitDouble = 17,//公会竞技场双反候场
        GuildWarGroup = 18,  //公会战
        TeamTask = 19,  //时空裂隙
        Branch = 20,
        Boom = 21,
        DuelWait = 22, //决斗候场
        Duel = 23, //决斗
        GuildBoss = 24, //公会BOSS
    }

    public class ChapterStarReward
    {
        public int StarNum {get;set;}
        public int RewardId {get;set;}
    }

    public class  chapters_helper
    {
        public class StarConfig
        {
            public int ChapterId;
            public int StarNum;
            public int RewardId;
            public int UngetIconId;
            public int GotIconId;

            public StarConfig(int chapterId, int starNum, int rewardId)
            {
                this.ChapterId = chapterId;
                this.StarNum = starNum;
                this.RewardId = rewardId;
            }
        }

        private static Dictionary<int, int> _instanceIdToChapterIdMap = new Dictionary<int, int>();
        private static Dictionary<ChapterType, List<chapters>> _copyListDict = new Dictionary<ChapterType, List<chapters>>();
        private static Dictionary<ChapterType, List<int>> _instanceIdQueue = new Dictionary<ChapterType, List<int>>();
        private static Dictionary<int, Dictionary<int, StarConfig>> _chapterStarDict = new Dictionary<int, Dictionary<int, StarConfig>>();

        static chapters_helper()
        {
            InitInstanceIdToChapterIdMap();
            InitChapterStarDict();
        }

        private static void InitInstanceIdToChapterIdMap()
        {
            foreach (chapters chapter in XMLManager.chapters.Values)
            {
                var list = data_parse_helper.ParseListInt(chapter.__instance_ids);
                for (int i = 0; i < list.Count; i++)
                {
                    _instanceIdToChapterIdMap[list[i]] = chapter.__id;
                }
            }
        }

        private static void InitChapterStarDict()
        {
            List<StarConfig> _starConfigList = new List<StarConfig>();
            foreach (chapters chapter in XMLManager.chapters.Values)
            {
                ChapterType chapterType = (ChapterType)chapter.__type;
                if (chapterType == ChapterType.Copy || chapterType == ChapterType.WanderLand)
                {
                    _starConfigList.Clear();
                    Dictionary<int, StarConfig> starDict = new Dictionary<int, StarConfig>();
                    var progressReward = data_parse_helper.ParseMap(chapter.__instance_ids);
                    foreach (string starNumStr in progressReward.Keys)
                    {
                        int starNum = int.Parse(starNumStr);
                        StarConfig starConfig = new StarConfig(chapter.__id, starNum, int.Parse(progressReward[starNumStr]));
                        starDict.Add(starNum, starConfig);
                        _starConfigList.Add(starConfig);
                    }
                    _starConfigList.Sort(SortByStarNum);
                    var iconUnget = data_parse_helper.ParseListInt(chapter.__icon_unget);
                    var iconGet = data_parse_helper.ParseListInt(chapter.__icon_get);
                    for (int i = 0; i < _starConfigList.Count; i++)
                    {
                        StarConfig starConfig = _starConfigList[i];

                        starConfig.UngetIconId = iconUnget[i];
                        starConfig.GotIconId = iconGet[i];
                    }
                    _chapterStarDict.Add(chapter.__id, starDict);
                }
            }
        }

        private static int SortByStarNum(StarConfig x, StarConfig y)
        {
            if (x.StarNum < y.StarNum)
            {
                return -1;
            }
            return 1;
        }

        public static chapters GetConfig(int id)
        {
            if (!XMLManager.chapters.ContainsKey(id))
            {
                throw new Exception("chapters表里面不存在id：" + id);
            }
            return XMLManager.chapters[id];
        }

        public static bool IsInstanceExistInChapter(int instanceId)
        {
            return _instanceIdToChapterIdMap.ContainsKey(instanceId);
        }

        public static int GetChapterId(int instanceId)
        {
            if (_instanceIdToChapterIdMap.ContainsKey(instanceId))
            {
                return _instanceIdToChapterIdMap[instanceId];
            }
            else
            {
                return 0;
            }
        }

        public static int GetDailyCount(int chapterId)
        {
            chapters chapter = GetChapter(chapterId);
            return chapter.__daily_time;
        }

        public static List<chapters> GetChapterList(ChapterType chapterType)
        {
            if (_copyListDict.ContainsKey(chapterType))
            {
                return _copyListDict[chapterType];
            }
            else
            {
                List<chapters> copyList = new List<chapters>();
                foreach (KeyValuePair<int, chapters> pair in XMLManager.chapters)
                {
                    if (pair.Value.__type == (int)chapterType)
                    {
                        copyList.Add(pair.Value);
                    }
                }
                _copyListDict.Add(chapterType, copyList);
                return _copyListDict[chapterType];
            }
        }

        public static List<chapters> GetChapterListSortByPage(ChapterType chapterType)
        {
            List<chapters> copyList = GetChapterList(chapterType);
            copyList.Sort(SortByPage);
            return copyList;
        }

        public static int GetNextInstanceId(int instanceId)
        {
            int chapterId = GetChapterId(instanceId);
            ChapterType chapterType = GetChapterType(chapterId);
            if (!_instanceIdQueue.ContainsKey(chapterType))
            {
                InitInstanceIdQueue(chapterId, chapterType);
            }
            int index = _instanceIdQueue[chapterType].FindIndex((id) => { return id == instanceId; });
            if ((index < 0) || (index >= _instanceIdQueue[chapterType].Count - 1))
            {
                return instance_helper.InvalidInstanceId;
            }
            return _instanceIdQueue[chapterType][index + 1];
        }

        private static void InitInstanceIdQueue(int chapterId, ChapterType chapterType)
        {
            List<chapters> chapterList = GetChapterListSortByPage(chapterType);
            List<int> instanceIdList = new List<int>();
            for (int i = 0; i < chapterList.Count; i++)
            {
                instanceIdList.AddRange(GetInstanceIdList(chapterList[i].__id));
            }
            _instanceIdQueue.Add(chapterType, instanceIdList);
        }

        private static int SortByPage(chapters x, chapters y)
        {
            if (x.__page < y.__page)
            {
                return -1;
            }
            return 1;
        }

        public static int GetPage(int id)
        {
            return GetConfig(id).__page;
        }

        public static bool IsChapterExist(int id)
        {
            return XMLManager.chapters.ContainsKey(id);
        }

        public static ChapterType GetChapterType(int id)
        {
            chapters chapter = GetConfig(id);
            return (ChapterType)chapter.__type;
        }

        public static string GetName(int id)
        {
            return GetName(GetConfig(id));
        }

        public static string GetName(chapters chapter )
        {
            return MogoLanguageUtil.GetContent(chapter.__name);
        }

        public static int GetTotalStar(int id)
        {
            return GetTotalStar(GetConfig(id));
        }

        public static int GetTotalStar(chapters chapter)
        {
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(chapter.__progress_reward);
            List<string> starNumList = dataDict.Keys.ToList();
            return Int32.Parse(starNumList[starNumList.Count - 1]);
        }

        public static chapters GetChapter(int chapterId)
        {
            if (!XMLManager.chapters.ContainsKey(chapterId))
            {
                throw new Exception("chapters表里面不存在id：" + chapterId);
            }
            return XMLManager.chapters[chapterId];
        }

        public static List<string> GetInstanceIdStrList(int id)
        {
            return data_parse_helper.ParseListString( GetConfig(id).__instance_ids);
        }

        public static int GetInstanceListCount(chapters chapter)
        {
            return data_parse_helper.ParseListString( chapter.__instance_ids).Count;
        }

        public static int GetInstanceListCount(int id)
        {
            return GetInstanceListCount(GetConfig(id));
        }

        public static List<instance> GetInstanceList(int chapterId)
        {
            List<instance> instanceList = new List<instance>();
            chapters chapter = GetChapter(chapterId);
            var list = data_parse_helper.ParseListInt(chapter.__instance_ids);
            for (int i = 0; i < list.Count; i++)
            {
                instanceList.Add(instance_helper.GetInstanceCfg(list[i]));
            }
            return instanceList;
        }

        public static List<int> GetInstanceIdList(int chapterId)
        {
            List<instance> instanceList = GetInstanceList(chapterId);
            List<int> instanceIdList = new List<int>();
            for (int i = 0; i < instanceList.Count; i++)
            {
                instanceIdList.Add(instanceList[i].__id);
            }
            return instanceIdList;
        }

        public static List<StarConfig> GetStarConfigList(int id)
        {
            List<StarConfig> list = _chapterStarDict[id].Values.ToList();
            list.Sort(SortByStarNum);
            return list;
        }

        public static StarConfig GetStarConfig(int id, int starNum)
        {
            return _chapterStarDict[id][starNum];
        }

        public static List<ChapterStarReward> GetStarRewardList(int chapterId)
        {
            List<ChapterStarReward> rewardList = new List<ChapterStarReward>();
            chapters chapter = GetChapter(chapterId);
            var progressReward = data_parse_helper.ParseMap(chapter.__progress_reward);
            foreach (string starNumStr in progressReward.Keys)
            {
                ChapterStarReward starReward = new ChapterStarReward();
                starReward.StarNum = int.Parse(starNumStr);
                starReward.RewardId = int.Parse(progressReward[starNumStr]);
                rewardList.Add(starReward);
            }
            rewardList.Sort(SortByStarNum);
            return rewardList;
        }

        private static int SortByStarNum(ChapterStarReward x, ChapterStarReward y)
        {
            if (x.StarNum < y.StarNum)
            {
                return -1;
            }
            return 1;
        }

        public static int GetWanderLandInstanceId(int chapterId, int starNum)
        {
            chapters chapter = GetChapter(chapterId);
            List<int> starNumList = new List<int>();
            var progressReward = data_parse_helper.ParseMap(chapter.__progress_reward);
            foreach (string starNumStr in progressReward.Keys)
            {
                starNumList.Add(int.Parse(starNumStr));
            }
            starNumList.Sort(SortByIncrease);
            for (int i = 0; i < starNumList.Count; i++)
            {
                if (starNumList[i] == starNum)
                {
                    return GetInstanceIdList(chapterId)[i];
                }
            }
            return -1;
        }

        private static int SortByIncrease(int x, int y)
        {
            if (x < y)
            {
                return -1;
            }
            return 1;
        }

        public static Dictionary<string, string> GetProgressReward(int chapterId)
        {
            return data_parse_helper.ParseMap(GetChapter(chapterId).__progress_reward);
        }

        public static int GetStarRewardId(int chapterId, int starNum)
        {
            List<ChapterStarReward> starRewardList = GetStarRewardList(chapterId);
            foreach (ChapterStarReward starReward in starRewardList)
            {
                if (starReward.StarNum == starNum)
                {
                    return starReward.RewardId;
                }
            }
            return 0;
        }

        /* Ari
        public static List<CategoryItemData> GetNameList(ChapterType type)
        {
            List<CategoryItemData> nameList = new List<CategoryItemData>();
            foreach (KeyValuePair<int, chapters> pair in XMLManager.chapters)
            {
                if (pair.Value.__type == (int) type)
                {
                    nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(pair.Value.__name)));
                }
            }
            return nameList;
        }*/

        public static int GetMinLevel(int chapterId)
        {
            List<string> instanceIdList = GetInstanceIdStrList(chapterId);
            return instance_helper.GetMinLevel(int.Parse(instanceIdList[0]));
        }

        public static int GetInstanceIndex(ChapterType type, int instanceId)
        {
            List<chapters> chapters = GetChapterListSortByPage(type);
            for (int i = 0; i < chapters.Count; i++)
            {
                var list = data_parse_helper.ParseListString(chapters[i].__instance_ids);
                List<int> instanceIds = data_parse_helper.ParseListInt(list);
                for (int j = 0; j < instanceIds.Count; j++)
                {
                    if (instanceIds[j] == instanceId)
                    {
                        return j;
                    }
                }
            }
            return 0;
        }

        public static int GetInstancePage(ChapterType type, int instanceId)
        {
            List<chapters> chapters = chapters_helper.GetChapterListSortByPage(type);
            for (int i = 0; i < chapters.Count; i++)
            {
                var list = data_parse_helper.ParseListString(chapters[i].__instance_ids);
                List<int> instanceIds = data_parse_helper.ParseListInt(list);
                if (instanceIds.Contains(instanceId))
                {
                    return chapters[i].__page;
                }
            }
            return 1;
        }
    }
}
