﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class activities_issue_helper
    {
        public static string COMPLETE_TYPE = "1";

        public static activities_issue GetConfig(int id)
        {
            if (!XMLManager.activities_issue.ContainsKey(id))
            {
                throw new Exception("开服活动配置表activities_issue找不到id:" + id);
            }
            return XMLManager.activities_issue[id];
        }

        public static int GetEventId(activities_issue issue)
        {
            int eventId = -1;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(issue.__completed_type);
            if (dataDict.ContainsKey(COMPLETE_TYPE))
            {
                Dictionary<string, string> dataDict2 = data_parse_helper.ParseMap(issue.__conds);
                foreach (string eventIdStr in dataDict2.Keys)
                {
                    eventId = int.Parse(eventIdStr);
                    break;
                }
            }
            return eventId;
        }

        public static int GetEventCount(activities_issue issue)
        {
            int count = 0;
            Dictionary<string, string> dataDict = data_parse_helper.ParseMap(issue.__completed_type);
            if (dataDict.ContainsKey(COMPLETE_TYPE))
            {
                Dictionary<string, string> dataDict2 = data_parse_helper.ParseMap(issue.__conds);
                Dictionary<string, string>.Enumerator enumerator = dataDict2.GetEnumerator();
                if(enumerator.MoveNext())
                {
                    count = int.Parse(enumerator.Current.Value);
                }
            }
            return count;
        }
    }
}
