﻿
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class TownTipsWrapper
    {
        public TownTipsId tipType;
        public int itemId; // 需要显示的道具ID

        public TownTipsWrapper(TownTipsId id, int itemId)
        {
            tipType = id;
            this.itemId = itemId;
        }
    }

    public enum TownTipsId
    {
        invalid = 0,
        decompose = 1,
        strengthen = 2,
        petQualityUp = 3,
        petStarUp = 4,
        dragonExplore = 5,
        rune = 6,
        skillUpgrade = 7,
        newMount = 8,
        newFashion = 9,
        newWing = 10,
        newEquipEffect = 11,
        petCall = 12,
    }

    public class town_tips_helper
    {
        public static town_tips GetConfig(TownTipsId id)
        {
            if (!XMLManager.town_tips.ContainsKey((int)id))
            {
                throw new Exception("表town_tips找不到id:" + (int)id);
            }
            return XMLManager.town_tips[(int)id];
        }

        public static int GetIcon(TownTipsId id)
        {
            return GetConfig(id).__icon;
        }

        public static int GetTitle(TownTipsId id)
        {
            return GetConfig(id).__title;
        }

        public static int GetViewId(TownTipsId id)
        {
            return GetConfig(id).__view_id;
        }

        public static int GetFunctionId(TownTipsId id)
        {
            return GetConfig(id).__function_id;
        }
    }
}
