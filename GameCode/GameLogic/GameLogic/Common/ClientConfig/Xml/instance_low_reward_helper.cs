﻿using Common.Data;
using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class instance_low_reward_helper
    {
        public static List<RewardData> GetRewardData(int instanceType, Vocation vocation)
        {
            if (XMLManager.instance_low_reward.ContainsKey(instanceType) == true)
            {
                var list = data_parse_helper.ParseListInt(XMLManager.instance_low_reward[instanceType].__reward_id);
                return item_reward_helper.GetRewardDataList(list[0], vocation);
            }
            else
            {
                //LogHelper
                return null;
            }
        }

        public static int GetRewardId(int instanceId, int starLevel)
        {
            int instanceType = (int)instance_helper.GetChapterType(instanceId);
            if (XMLManager.instance_low_reward.ContainsKey(instanceType) == true)
            {
                int index = starLevel - 1;
                var list = data_parse_helper.ParseListInt(XMLManager.instance_low_reward[instanceType].__reward_id);
                if (index >= list.Count)
                {
                    index = list.Count - 1;
                }
                return list[index];
            }
            return 0;
        }
    }
}
