﻿#region 模块信息
/*==========================================
// 文件名：alchemy_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/12 15:04:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameLoader.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class alchemy_helper
    {
        public static alchemy GetAlchemyById(int id)
        {
            if(XMLManager.alchemy.ContainsKey(id) == false)
            {
                LoggerHelper.Error("缺少id为" + id + "的炼金配置,"+PlayerAvatar.Player.alchemy_cur_cnt+","+PlayerAvatar.Player.alchemy_time_stamp);
                return null;
            }
            return XMLManager.alchemy[id];
        }

        public static int GetAlchemyBasicReward(alchemy alchemy)
        {
            avatar_level level = avatar_level_helper.GetAvatarLevel(PlayerAvatar.Player.level);
            return level.__gold_standard * alchemy.__basic_reward / 10000;
        }

        public static int GetAlchemyAssistantReward(alchemy alchemy)
        {
            avatar_level level = avatar_level_helper.GetAvatarLevel(PlayerAvatar.Player.level);
            return level.__gold_standard * alchemy.__assistant_reward / 10000;
        }

        public static int GetAlchemyAdditionReward(alchemy alchemy,int num)
        {
            if(num == 0)
            {
                return 0;
            }
            Dictionary<string, string> dict = data_parse_helper.ParseMap(alchemy.__addition);
            if (dict.ContainsKey(num.ToString()) == false)
            {
                LoggerHelper.Error("炼金配置Id:"+alchemy.__id+"缺少数量为" + num + "的配置");
                return 0;
            }
            int addition = int.Parse(dict[num.ToString()]);
            avatar_level level = avatar_level_helper.GetAvatarLevel(PlayerAvatar.Player.level);
            return level.__gold_standard * addition / 10000;
        }

    }
}
