﻿#region 模块信息
/*==========================================
// 文件名：icon_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:20:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Structs;
using UnityEngine;

namespace GameData
{
    public class icon_item_helper
    {
        private const string DEFAULT_VALUE = "0";
        private const string ICON_404 = "item404";

        public static icon_item GetIconItem(int iconId)
        {
            if(XMLManager.icon_item.ContainsKey(iconId))
            {
                return XMLManager.icon_item[iconId];
            }
            Debug.LogError("配置表icon_item.xml 未找到配置项： " + iconId);
            return null;
        }

        public static string GetIcon(int iconId)
        {
            icon_item _icon = GetIconItem(iconId);
            if(_icon == null || string.IsNullOrEmpty(_icon.__icon) == true || _icon.__icon == DEFAULT_VALUE)
            {
                return ICON_404;
            }
            return _icon.__icon;
        }

        public static string GetIconBg(int iconId)
        {
            icon_item _icon = GetIconItem(iconId);
            if(_icon == null || _icon.__bg == null || _icon.__bg == DEFAULT_VALUE)
            {
                return string.Empty;
            }
            return _icon.__bg;
        }

        public static int GetIconColor(int iconId)
        {
            icon_item _icon = GetIconItem(iconId);
            if(_icon == null || _icon.__color == 0)
            {
                return 1;
            }
            return _icon.__color;
        }

        public static int GetVocationIconId(Vocation vocation)
        {
            if (vocation == Vocation.Warrior) return 625;
            if (vocation == Vocation.Archer) return 626;
            if (vocation == Vocation.Wizard) return 627;
            return -1;
        }

    }
}
