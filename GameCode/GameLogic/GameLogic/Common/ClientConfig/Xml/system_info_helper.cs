﻿#region 模块信息
/*==========================================
// 文件名：system_info_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 18:08:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Global;
using Common.Base;
using Common.Utils;
using GameMain.Entities;

namespace GameData
{
    public class system_info_helper
    {
        public static system_info GetConfig(int systemInfoId)
        {
            if(XMLManager.system_info.ContainsKey(systemInfoId) == false)
            {
                throw new Exception("System_info.xml 中未找到配置项: " + systemInfoId);
            }
            return XMLManager.system_info[systemInfoId];
        }

        public static bool HasConfig(int systemInfoId)
        {
            return XMLManager.system_info.ContainsKey(systemInfoId);
        }

        public static string GetContent(int systemInfoId)
        {
            system_info info = GetConfig(systemInfoId);
            return info.__descript;
        }

        public static List<int> GetTypeList(int systemInfoId)
        {
            system_info info = GetConfig(systemInfoId);
            return data_parse_helper.ParseListInt(info.__type);
        }

        public static Dictionary<string,string[]> GetFollowData(int systemInfoId)
        {
            system_info info = GetConfig(systemInfoId);
            return data_parse_helper.ParseMapList(info.__follow);
        }

        public static bool CanShowSystemMsgCurDay(int systemInfoId)
        {
            system_info info = GetConfig(systemInfoId);
            List<string> listInt = data_parse_helper.ParseListString(info.__date);
            if (listInt.Count == 0)
                return true;
            int curDayOfWeek = MogoTimeUtil.GetDayOfWeek((long)(Global.serverTimeStamp / 1000));
            return listInt.Contains(curDayOfWeek.ToString());
        }

        public static bool CanShowSystemMsgCurLv(int systemInfoId)
        {
            system_info info = GetConfig(systemInfoId);
            List<int> listInt = data_parse_helper.ParseListInt(info.__lvl_limit);
            if (listInt.Count == 0)
                return true;
            int minLv = listInt[0];
            int maxLv = listInt.Count > 1 ? info.__lvl_limit[1] : 0;
            int curLv = PlayerAvatar.Player.level;
            if (maxLv == 0)
            {
                return curLv >= minLv;
            }
            else
            {
                return (curLv >= minLv && curLv <= maxLv);
            }
        }

    }
}
