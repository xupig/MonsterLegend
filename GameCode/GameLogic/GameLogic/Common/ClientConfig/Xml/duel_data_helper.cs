﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class duel_data_helper
    {
        public static duel_data GetConfig()
        {
            return XMLManager.duel_data[1];
        }

        public static int GetAcceptTime()
        {
            return GetConfig().__accept_time;
        }

        public static int GetPrepareTime()
        {
            return GetConfig().__prepare_time;
        }

        public static int GetMatchRecordCountLimit()
        {
            return GetConfig().__match_record_count_limit;
        }

        public static BaseItemData GetLuckModeCostItem()
        {
            List<string> specialDuelGamble = data_parse_helper.ParseListString(GetConfig().__special_duel_gamble);
            int itemId = int.Parse(specialDuelGamble[0]);
            BaseItemData baseItemData = ItemDataCreator.Create(itemId);
            baseItemData.StackCount = int.Parse(specialDuelGamble[2]);
            return baseItemData;
        }

        public static BaseItemData GetLuckModeRewardItem()
        {
            List<string> specialDuelGamble = data_parse_helper.ParseListString(GetConfig().__special_duel_gamble);
            int itemId = int.Parse(specialDuelGamble[0]);
            BaseItemData baseItemData = ItemDataCreator.Create(itemId);
            baseItemData.StackCount = int.Parse(specialDuelGamble[1]);
            return baseItemData;
        }
    }
}
