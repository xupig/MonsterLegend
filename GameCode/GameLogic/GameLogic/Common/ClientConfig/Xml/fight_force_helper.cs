﻿using Common.Data;
using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using S = Common.ServerConfig;

namespace GameData
{
    public class fight_force_helper
    {
        public static int GetFightForce(int effectId, Vocation vocation)
        {
            List<int> idList = attri_effect_helper.GetAttributeIdList(effectId);
            List<int> valueList = attri_effect_helper.GetAttributeValueList(effectId);
            if (idList.Count != valueList.Count)
            {
                throw new Exception("配置表 attri_effect.xml 配置项: " + effectId.ToString() + "配置有误");
            }
            Dictionary<int, float> attriDic = new Dictionary<int, float>();
            for (int i = 0; i < idList.Count; i++)
            {
                int attriId = idList[i];
                float value = valueList[i];
                AddAttribute(attriDic, attriId, value);
            }
            return GetFightForce(attriDic, vocation);
        }

        public static int GetFightForce(List<int> attriTypeList, List<int> attriValueList, Vocation vocation)
        {
            Dictionary<int, float> baseAttriDic = new Dictionary<int, float>();
            for (int i = 0; i < Math.Min(attriTypeList.Count, attriValueList.Count); i++)
            {
                baseAttriDic.Add(attriTypeList[i], (float)attriValueList[i]);
            }
            return GetFightForce(baseAttriDic, vocation);
        }

        /// <summary>
        /// 获取战斗力，传入未转换前的属性字典
        /// </summary>
        /// <param name="baseAttriDic"></param>
        /// <param name="vocation"></param>
        /// <returns></returns>
        public static int GetFightForce(Dictionary<int, float> baseAttriDic, Vocation vocation)
        {
            Dictionary<int, float> resultAttriDic = ChangeAttributeConvert(baseAttriDic, vocation);
            float result = 0;
            foreach (KeyValuePair<int, float> kvp in resultAttriDic)
            {
                result += GetFightForce(kvp.Key, kvp.Value, vocation);
            }
            return Mathf.FloorToInt(result);
        }

        public static float GetFightForce(List<RoleAttributeData> attributeDataList, Vocation vocation)
        {
            Dictionary<int, float> attriDic = new Dictionary<int, float>();
            for (int i = 0; i < attributeDataList.Count; i++)
            {
                RoleAttributeData data = attributeDataList[i];
                AddAttribute(attriDic, data.attriId, data.AttriValue);
            }
            return GetFightForce(attriDic, vocation);
        }

        /// <summary>
        /// 返回属性等级
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public static int GetAttributeLevel(int attributeId)
        {
            if (attributeId >= (int)S.fight_attri_config.CLASS_1_MIN && attributeId <= (int)S.fight_attri_config.CLASS_1_MAX)
            {
                return 1;
            }
            else if (attributeId >= (int)S.fight_attri_config.CLASS_2_MIN && attributeId <= (int)S.fight_attri_config.CLASS_2_MAX)
            {
                return 2;
            }
            else if (attributeId >= (int)S.fight_attri_config.CLASS_3_MIN && attributeId <= (int)S.fight_attri_config.CLASS_3_MAX)
            {
                return 3;
            }
            else if (attributeId >= (int)S.fight_attri_config.CLASS_4_MIN && attributeId <= (int)S.fight_attri_config.CLASS_4_MAX)
            {
                return 4;
            }
            else if (attributeId >= (int)S.fight_attri_config.CLASS_5_MIN && attributeId <= (int)S.fight_attri_config.CLASS_5_MAX)
            {
                return 5;
            }
            else
            {
                return 6;
            }
        }

        public static Dictionary<int, float> ChangeAttributeConvert(int effectId, Vocation vocation)
        {
            List<int> idList = attri_effect_helper.GetAttributeIdList(effectId);
            List<int> valueList = attri_effect_helper.GetAttributeValueList(effectId);
            if (idList.Count != valueList.Count)
            {
                throw new Exception("配置表 attri_effect.xml 配置项: " + effectId.ToString() + "配置有误");
            }
            Dictionary<int, float> attriDic = new Dictionary<int, float>();
            for (int i = 0; i < idList.Count; i++)
            {
                int attriId = idList[i];
                float value = valueList[i];
                AddAttribute(attriDic, attriId, value);
            }
            return ChangeAttributeConvert(attriDic, vocation);
        }

        /// <summary>
        /// 属性转换，参数为原始属性字典，返回为经过多次转换得到的最终属性字典，key为属性id，value为属性数值
        /// </summary>
        /// <param name="baseAttriDic">原始属性字典，key为属性id，value为属性数值</param>
        /// <param name="vocation">玩家职业</param>
        /// <returns>最终属性字典，key为属性id，value为属性数值</returns>
        public static Dictionary<int, float> ChangeAttributeConvert(Dictionary<int, float> baseAttriDic, Vocation vocation)
        {
            Dictionary<int, float> tempAttriDic = ChangeAttributeConvert1(baseAttriDic, vocation);
            Dictionary<int, float> resultAttriDic = ChangeAttributeConvert2(tempAttriDic, vocation);
            return resultAttriDic;
        }

        /// <summary>
        /// 计算转化后每个属性的战斗力
        /// </summary>
        /// <param name="attributeId"></param>
        /// <param name="value"></param>
        /// <param name="vocation"></param>
        /// <returns></returns>
        private static float GetFightForce(int attributeId, float value, Vocation vocation)
        {
            attri_config attri = attri_config_helper.GetAttri(attributeId);
            int vocationId = (int)vocation;
            Dictionary<string, string> dict = data_parse_helper.ParseMap(attri.__fight_ratios);
            if (dict.ContainsKey(vocationId.ToString()))
            {
                float ratios = float.Parse(dict[vocationId.ToString()]);
                return value * ratios;
            }
            return 0;
        }

        //一级属性向上传递
        private static Dictionary<int, float> ChangeAttributeConvert1(Dictionary<int, float> attriDic, Vocation vocation)
        {
            Dictionary<int, float> result = new Dictionary<int, float>();
            foreach (KeyValuePair<int, float> kvp in attriDic)
            {
                attri_convert_1 convert = attri_convert_1_helper.GetAttributeConvert(kvp.Key);
                if (convert != null)
                {
                    List<int> idList = attri_convert_1_helper.GetAttributeIdList(convert, vocation);
                    List<float> ratiosList = attri_convert_1_helper.GetAttributeRatiosList(convert, vocation);
                    if (idList.Count != ratiosList.Count)
                    {
                        throw new Exception("配置表 attri_convert_1.xml 配置项: " + kvp.Key.ToString() + "配置有误");
                    }
                    for (int i = 0; i < idList.Count; i++)
                    {
                        int attributeId = idList[i];
                        float value = kvp.Value * ratiosList[i];
                        AddAttribute(result, attributeId, value);
                    }
                }
                AddAttribute(result, kvp.Key, kvp.Value);
            }
            return result;
        }

        //二级属性向上传递
        private static Dictionary<int, float> ChangeAttributeConvert2(Dictionary<int, float> attriDic, Vocation vocation)
        {
            Dictionary<int, float> result = new Dictionary<int, float>();
            foreach (KeyValuePair<int, float> kvp in attriDic)
            {
                attri_convert_2 convert = attri_convert_2_helper.GetAttributeConvert(kvp.Key);
                if (convert != null)
                {
                    int attributeId = attri_convert_2_helper.GetConvertAttributeId(convert);
                    float value = attri_convert_2_helper.GetConvertAttributeValue(convert, kvp.Value);
                    AddAttribute(result, attributeId, value);
                }
                AddAttribute(result, kvp.Key, kvp.Value);
            }
            return result;
        }

        private static void AddAttribute(Dictionary<int, float> result, int attributeId, float value)
        {
            if (result.ContainsKey(attributeId))
            {
                result[attributeId] += value;
            }
            else
            {
                result.Add(attributeId, value);
            }
        }
    }
}
