﻿using Common.Data;
using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class night_activity_reward_helper
    {
        /// <summary>
        /// 获得晚上活动排名奖励
        /// </summary>
        /// <param name="instanceId">关卡id</param>
        /// <param name="level">玩家等级</param>
        /// <param name="vocation">玩家职业</param>
        /// <param name="rankNum">玩家排名</param>
        /// <returns></returns>
        public static List<RewardData> GetRankReward(int instanceId, int level, Vocation vocation, int rankNum)
        {
            foreach (KeyValuePair<int, pvp_night_rank_reward> kvp in XMLManager.pvp_night_rank_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if(kvp.Value.__instance_id == instanceId &&
                    int.Parse(dataList[0]) <= level &&
                    int.Parse(dataList[1]) >= level && 
                    kvp.Value.__ranking == rankNum)
                {
                    return item_reward_helper.GetRewardDataList(kvp.Value.__reward, vocation);
                }
            }
            return new List<RewardData>();
        }

        /// <summary>
        /// 获得晚上活动胜负奖励
        /// </summary>
        /// <param name="instanceId">关卡id</param>
        /// <param name="level">玩家等级</param>
        /// <param name="vocation">玩家职业</param>
        /// <param name="isWin">胜利还是失败 isWin = true表示胜利</param>
        /// <returns></returns>
        public static List<RewardData> GetWinReward(int instanceId, int level, Vocation vocation, bool isWin)
        {
            foreach (KeyValuePair<int, pvp_night_victory_reward> kvp in XMLManager.pvp_night_victory_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if (kvp.Value.__instance_id == instanceId &&
                    int.Parse(dataList[0]) <= level &&
                    int.Parse(dataList[1]) >= level)
                {
                    if (isWin == true)
                    {
                        return item_reward_helper.GetRewardDataList(kvp.Value.__victory_reward, vocation);
                    }
                    else
                    {
                        return item_reward_helper.GetRewardDataList(kvp.Value.__defeat_reward, vocation);
                    }
                }
            }
            return new List<RewardData>();
        }

        /// <summary>
        /// 获得晚上活动积分奖励
        /// </summary>
        /// <param name="instanceId">关卡id</param>
        /// <param name="level">玩家等级</param>
        /// <param name="vocation">玩家职业</param>
        /// <param name="score">获得的积分</param>
        /// <returns>奖励列表</returns>
        public static List<RewardData> GetScoreReward(int instanceId, int level, Vocation vocation, int score)
        {
            foreach (KeyValuePair<int, pvp_night_score_segment> kvp in XMLManager.pvp_night_score_segment)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                List<string> dataList2 = data_parse_helper.ParseListString(kvp.Value.__score);
                if (kvp.Value.__instance_id == instanceId &&
                    int.Parse(dataList[0]) <= level &&
                    int.Parse(dataList[1]) >= level &&
                    int.Parse(dataList2[0]) <= score &&
                    int.Parse(dataList2[1]) >= score)
                {
                    return item_reward_helper.GetRewardDataList(kvp.Value.__reward, vocation);
                }
            }
            return new List<RewardData>();
        }

        public static List<RewardData> GetTotalReward(int instanceId, int level, bool isWin, Vocation vocation, int rankNum, int score = 0)
        {
            List<RewardData> rankRewardList = GetRankReward(instanceId, level, vocation, rankNum);
            List<RewardData> winRewardList = GetWinReward(instanceId,level,  vocation, isWin);
            List<RewardData> scoreRewardList = GetScoreReward(instanceId, level, vocation, score);
            return MergeReward(MergeReward(rankRewardList, winRewardList), scoreRewardList);
        }


        /// <summary>
        /// 活动入口用于显示可能获得的物品（积分奖励部分）
        /// </summary>
        /// <param name="instanceId">关卡id</param>
        /// <param name="level">玩家等级</param>
        /// <param name="vocation">玩家职业</param>
        /// <returns></returns>
        public static List<RewardData> GetAllScoreReward(int instanceId, int level, Vocation vocation)
        {
            List<RewardData> rewardData = new List<RewardData>();
            foreach (KeyValuePair<int, pvp_night_score_segment> kvp in XMLManager.pvp_night_score_segment)
            {
                List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__level);
                if (kvp.Value.__instance_id == instanceId &&
                    int.Parse(dataList[0]) <= level &&
                    int.Parse(dataList[1]) >= level)
                {
                    rewardData.AddRange(item_reward_helper.GetRewardDataList(kvp.Value.__reward, vocation));
                }
            }
            return rewardData;
        }

        /// <summary>
        /// 活动入口用于显示所有可能获得的物品
        /// </summary>
        /// <param name="instanceId">关卡id</param>
        /// <param name="level">玩家等级</param>
        /// <param name="vocation">玩家职业</param>
        /// <returns></returns>
        public static List<RewardData> GetRewardList(int instanceId, int level, Vocation vocation)
        {
            List<RewardData> rewardData = new List<RewardData>();
            for (int i = 0; i < 8; i++)
            {
                rewardData.AddRange(GetRankReward(instanceId, level, vocation, i+1));
            }
            rewardData.AddRange(GetWinReward( instanceId, level,vocation, true));
            rewardData.AddRange(GetWinReward(instanceId, level, vocation, false));
            rewardData.AddRange(GetAllScoreReward(instanceId, level, vocation));
            return rewardData;
            
        }

        private static List<RewardData> MergeReward(List<RewardData> first, List<RewardData> second)
        {
            for (int i = 0; i < first.Count; i++)
            {
                for (int j = 0; j < second.Count; j++)
                {
                    if (first[i].id == second[j].id)
                    {
                        first[i].num += second[j].num;
                    }
                }
            }

            return first;
        }
    }
}
