﻿using Common.Data;
using Common.Utils;
using System.Collections.Generic;

namespace GameData
{
    public class condition_helper
    {
        private static Dictionary<int, int> _conditionDescDict;

        private static Dictionary<int, List<FilterConditionData>> _conditionDict;

        static condition_helper()
        {
            _conditionDescDict = new Dictionary<int, int>();
            _conditionDescDict.Add((int)ConditionType.Vocation, 56105);
            _conditionDescDict.Add((int)ConditionType.Level, 56104);
            _conditionDescDict.Add((int)ConditionType.Body, 56106);
        }

        public static string GetConditionDesc(int condition)
        {
            if (_conditionDescDict.ContainsKey(condition) == true)
            {
                return MogoLanguageUtil.GetContent(_conditionDescDict[condition]);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        public static condition GetCondition(int id)
        {
            return XMLManager.condition[id];
        }

        public static List<FilterConditionData> GetFilterList(int type)
        {
            if (_conditionDict == null)
            {
                _conditionDict = new Dictionary<int, List<FilterConditionData>>();
                foreach (KeyValuePair<int, condition> kvp in XMLManager.condition)
                {
                    if (_conditionDict.ContainsKey(kvp.Value.__type) == false)
                    {
                        _conditionDict.Add(kvp.Value.__type, new List<FilterConditionData>());
                    }
                    string name = string.Empty;
                    if ((ConditionType)kvp.Value.__type == ConditionType.Level)
                    {
                        List<string> dataList = data_parse_helper.ParseListString(kvp.Value.__value);
                        if (dataList.Count > 1)
                        {
                            name = string.Format(MogoLanguageUtil.GetContent(kvp.Value.__name), dataList[0], dataList[1]);
                        }
                        else
                        {
                            name = MogoLanguageUtil.GetContent(kvp.Value.__name);
                        }
                    }
                    else
                    {
                        name = MogoLanguageUtil.GetContent(kvp.Value.__name);
                    }
                    FilterConditionData data = new FilterConditionData(kvp.Value.__id, name);
                    _conditionDict[kvp.Value.__type].Add(data);
                }
            }
            _conditionDict[type].Sort(SortFunc);
            return _conditionDict[type];
        }

        private static int SortFunc(FilterConditionData value1, FilterConditionData value2)
        {
            condition cond1 = GetCondition(value1.Id);
            condition cond2 = GetCondition(value2.Id);
            List<string> dataList = data_parse_helper.ParseListString(cond1.__value);
            List<string> dataList2 = data_parse_helper.ParseListString(cond2.__value);
            return int.Parse(dataList[0]) - int.Parse(dataList2[0]);
        }
    }

    public enum ConditionType
    {
        Body = 3,
        Level = 4,
        Vocation = 5,
        Trade = 6,
    }
}
