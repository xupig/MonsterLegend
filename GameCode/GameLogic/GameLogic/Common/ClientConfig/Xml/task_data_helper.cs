﻿#region 模块信息
/*==========================================
// 文件名：task_data_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/26 16:56:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using Common.Utils;
using GameMain.Entities;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ServerConfig;
using UnityEngine;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;

namespace GameData
{
    public class task_data_helper
    {

        private static Dictionary<int, Dictionary<TaskEventEnum, TaskEventData>> taskEventDic;

        private static Dictionary<int, Dictionary<int, List<spacetime_rift_task_reward>>> teamTaskDict;

        public static spacetime_rift_task_reward GetTeamTaskReward(int level,int ring,int rewardType)
        {
            if(teamTaskDict == null)
            {
                teamTaskDict = new Dictionary<int, Dictionary<int, List<spacetime_rift_task_reward>>>();
                foreach(spacetime_rift_task_reward reward in XMLManager.spacetime_rift_task_reward.Values)
                {
                    if(teamTaskDict.ContainsKey(reward.__reward_type) == false)
                    {
                        teamTaskDict.Add(reward.__reward_type, new Dictionary<int, List<spacetime_rift_task_reward>>());
                    }
                    if(teamTaskDict[reward.__reward_type].ContainsKey(reward.__player_ring) == false)
                    {
                        teamTaskDict[reward.__reward_type].Add(reward.__player_ring, new List<spacetime_rift_task_reward>());
                    }
                    teamTaskDict[reward.__reward_type][reward.__player_ring].Add(reward);
                }
            }
            if(teamTaskDict.ContainsKey(rewardType) && teamTaskDict[rewardType].ContainsKey(ring))
            {
                List<spacetime_rift_task_reward> rewardList = teamTaskDict[rewardType][ring];
                for(int i=0;i<rewardList.Count;i++)
                {
                    var data = data_parse_helper.ParseListInt(rewardList[i].__player_level);
                    int start = data[0];
                    int end = data[1];
                    if(level>=start && level<=end)
                    {
                        return rewardList[i];
                    }
                }
            }
            return null;
        }


        private static List<spacetime_rift_times_reward> rewardList;
        public static List<spacetime_rift_times_reward> GetTeamTaskTimeRewardList()
        {
            if(rewardList == null)
            {
                rewardList = new List<spacetime_rift_times_reward>();
            }
            else
            {
                return rewardList;
            }
            foreach(spacetime_rift_times_reward reward in XMLManager.spacetime_rift_times_reward.Values)
            {
                rewardList.Add(reward);
            }
            rewardList.Sort(SortFunc);
            return rewardList;
        }

        private static int SortFunc(spacetime_rift_times_reward reward1, spacetime_rift_times_reward reward2)
        {
            return reward1.__task_times - reward2.__task_times;
        }

        public static task_data GetTaskData(int taskId)
        {
            if(XMLManager.task_data.ContainsKey(taskId))
            {
                return XMLManager.task_data[taskId];
            }
            return null;
        }

        public static int GetTaskType(int taskId)
        {
            task_data data = GetTaskData(taskId);
            if(data!=null)
            {
                return data.__task_type;
            }
            return 0;
        }

        public static string GetTaskName(task_data data)
        {
            return MogoLanguageUtil.GetContent(data.__task_name);
        }

        public static string GetTaskDesc(task_data data)
        {
            return MogoLanguageUtil.GetContent(data.__task_desc);
        }

        public static string GetTaskSummary(task_data data)
        {
            return "";//TaskParser.ParseAllNumMatch(MogoLanguageUtil.GetContent(data.__task_summary), data.__id);
        }

        public static int getTaskTargetNum(task_data data)
        {
            var target = data_parse_helper.ParseMap(data.__target);
            foreach (KeyValuePair<string, string> kvp in target)
            {
                return int.Parse(kvp.Value);
            }
            return 0;
        }

        public static List<BaseItemData> GetReward(task_data data, Vocation vocation)
        {
            if (data.__reward == 0) { return new List<BaseItemData>(); }
            item_reward _reward = item_reward_helper.GetItemReward(data.__reward);
            if(_reward!=null)
            {
                return item_reward_helper.GetItemDataList(_reward, vocation);
            }
            Debug.LogError(string.Format("职业{0}的任务{1}没有配奖励数据", vocation, data.__id));
            return null;
        }

        public static string GetTaskIcon(task_data data)
        {
            return "taskIcon1";
            //return icon_item_helper.GetIcon(data.__task_icon);
        }

        public static int GetTaskIconColor(task_data data)
        {
            return icon_item_helper.GetIconColor(data.__task_icon);
        }

        public static bool IsMissionTask(int taskId)
        {
            return true;
        }

        public static event_cnds GetTaskEvent(int taskId)
        {
            task_data data = GetTaskData(taskId);
            if(data!=null)
            {
                var target = data_parse_helper.ParseMap(data.__target);
                foreach (KeyValuePair<string, string> kvp in target)
                {
                    return event_cnds_helper.GetEvent(int.Parse(kvp.Key));
                }
            }
            return null;
        }

        public static int GetTaskInstanceId(int taskId)
        {
            task_data data = GetTaskData(taskId);

            if (data != null)
            {
                var target = data_parse_helper.ParseMap(data.__target);
                foreach (KeyValuePair<string, string> kvp in target)
                {
                    int eventId = int.Parse(kvp.Key);
                    if (event_cnds_helper.IsContainConditionType(eventId, event_config.EVT_ID_COMPLETE_COPY))
                    {
                        return event_cnds_helper.GetInstanceId(eventId);
                    }
                }
            }
            return 0;
        }

        public static task_data GetMainTaskIdByInstanceId(int instanceId)
        {
            foreach (task_data taskData in XMLManager.task_data.Values)
            {
                if (taskData.__task_type == public_config.TASK_TYPE_MAIN)
                {
                    if (GetTaskInstanceId(taskData.__id) == instanceId)
                    {
                        return taskData;
                    }
                }
            }
            return null;
        }

        public static Dictionary<string, string[]> GetTaskFollow(int taskId)
        {
            event_cnds cnds = GetTaskEvent(taskId);
            if(cnds == null)
            {
                return null;
            }
            return event_cnds_helper.GetEventFollow(cnds.__id);
        }

        public static Dictionary<TaskEventEnum,TaskEventData> GetTaskEventData(int taskId)
        {
            if(taskEventDic == null)
            {
                taskEventDic = new Dictionary<int, Dictionary<TaskEventEnum, TaskEventData>>();
            }
            if(taskEventDic.ContainsKey(taskId) == false)
            {
                InitTaskEventData(taskId);
            }
            return taskEventDic[taskId];
        }

        private static void InitTaskEventData(int taskId)
        {
            taskEventDic.Add(taskId, new Dictionary<TaskEventEnum, TaskEventData>());
            task_data data = GetTaskData(taskId);

            TaskEventData eventData = CreateTaskEventData(TaskEventEnum.NpcId, data.__npc_id_report);
            taskEventDic[taskId].Add(eventData.type, eventData);
            if(data.__target != null)
            {
                var target = data_parse_helper.ParseMap(data.__target);
                foreach (KeyValuePair<string, string> kvp in target)
                {
                    eventData = CreateTaskEventData(TaskEventEnum.Loop, int.Parse(kvp.Value));
                    taskEventDic[taskId].Add(eventData.type, eventData);

                    AddEventCnds(taskId, int.Parse(kvp.Key));
                    return;
                } 
            }
        }

        private static void AddEventCnds(int taskId,int eventId)
        {
            Dictionary<int,float> cnds = event_cnds_helper.GetEventCnds(eventId);
            foreach(KeyValuePair<int,float> kvp in cnds)
            {
                TaskEventData eventData = CreateTaskEventData((TaskEventEnum)kvp.Key, kvp.Value);
                taskEventDic[taskId].Add(eventData.type, eventData);
            }
        }

        private static TaskEventData CreateTaskEventData(TaskEventEnum type, float value)
        {
            TaskEventData data = new TaskEventData();
            data.type = type;
            data.value = value;

            return data;
        }

        public static string GetTargetProgressConent(int taskId, string template = "{0}/{1}")
        {
            PbTaskInfo taskInfo = PlayerDataManager.Instance.TaskData.GetTaskInfo(taskId);
            if (taskInfo == null)
            {
                return string.Empty;
            }
            string taskTargetProgress = string.Empty;
            int total = task_data_helper.getTaskTargetNum(GetTaskData(taskInfo.task_id));
            if (total == 0)
            {
                return taskTargetProgress;
            }
            else
            {
                if (taskInfo.target != null && taskInfo.target.Count > 0)
                {
                    taskTargetProgress = string.Format(template, taskInfo.target[0].value, total);
                }
                else
                {
                    taskTargetProgress = string.Format(template, total, total);
                }
            }
            return taskTargetProgress;
        }
    }
}
