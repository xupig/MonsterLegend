﻿#region 模块信息
/*==========================================
// 文件名：chapters_helper
// 命名空间: Common.ClientConfig.Xml
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/26 16:23:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.Utils;
using UnityEngine;
using Common.ExtendComponent;

namespace GameData
{
    public class  channel_helper
    {
        public static channel GetConfig(int id)
        {
            if (XMLManager.channel.ContainsKey(id))
            {
                return XMLManager.channel[id];
            }
            return null;
        }

        public static bool HasConfig(int getway)
        {
            return XMLManager.channel.ContainsKey(getway);
        }

        public static int GetChannelType(int getway)
        {
            channel cfg = GetConfig(getway);
            if (cfg != null)
            {
                return cfg.__show_type;
            }
            return 0;
        }

        public static string GetChannelDesc(int getway)
        {
            channel cfg = GetConfig(getway);
            if (cfg != null)
            {
                if (cfg.__desc == 0)
                {
                    return string.Empty;
                }
                return MogoLanguageUtil.GetContent(cfg.__desc);
            }
            return string.Empty;
        }

        public static List<string> GetChannelSystemDesc(int getway)
        {
            channel cfg = GetConfig(getway);
            if (cfg != null)
            {
                List<string> listInt = data_parse_helper.ParseListString(cfg.__system_desc);
                if (listInt.Count == 0 || listInt[0] == "1")
                {
                    return null;
                }
                return listInt;
            }
            return null;
        }
    }
}
