﻿#region 模块信息
/*==========================================
// 文件名：equip_facade_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/12 20:14:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Utils;
namespace GameData
{
    public class equip_facade_helper
    {
        public static Dictionary<int, string> groupNameDict;
        public static Dictionary<int, List<int>> equipFacadeDict;

        public static void InitData()
        {
            if (equipFacadeDict != null)
            {
                return;
            }
            equipFacadeDict = new Dictionary<int, List<int>>();
            groupNameDict = new Dictionary<int, string>();
            foreach (KeyValuePair<int, equip_facade> pair in XMLManager.equip_facade)
            {
                int group = pair.Value.__group;
                if (equipFacadeDict.ContainsKey(group) == false)
                {
                    equipFacadeDict.Add(group, new List<int>());
                }
                equipFacadeDict[group].Add(pair.Value.__id);
                if (groupNameDict.ContainsKey(group) == false)
                {
                    groupNameDict.Add(group, MogoLanguageUtil.GetContent(pair.Value.__group_name));
                }
            }
        }

        public static Dictionary<int, string> GetFacadeGroupList()
        {
            InitData();
            const int Facade = 9999;
            const int MOUNT = 10000;
            string FacadeString = MogoLanguageUtil.GetContent(5120);
            string MountString = MogoLanguageUtil.GetContent(112500);
            groupNameDict.Add(Facade, FacadeString);
            groupNameDict.Add(MOUNT, MountString);
            return groupNameDict;
        }


        public static List<int> GetFacadeListByGroup(int groupId)
        {
            if (equipFacadeDict.ContainsKey(groupId))
            {
                return equipFacadeDict[groupId];
            }
            return null;
        }

        public static int GetEffectIcon(int effectId)
        {
            if (XMLManager.equip_facade.ContainsKey(effectId))
            {
                return XMLManager.equip_facade[effectId].__icon;
            }
            return 0;
        }

        public static int GetEffectGroup(int effectId)
        {
            if (XMLManager.equip_facade.ContainsKey(effectId))
            {
                return XMLManager.equip_facade[effectId].__group;
            }
            return 0;
        }
    }
}