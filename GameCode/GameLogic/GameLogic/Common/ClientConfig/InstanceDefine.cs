﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ClientConfig
{
    public class InstanceDefine
    {
        public const int COND_TYPE_KILL_MONSTER = 1;   //击杀怪物激活行为
        public const int COND_TYPE_HP_CHANG = 6;      //验证当前怪物血量百分比
        public const int COND_TYPE_SKILL_ACTION = 81;  //技能行为激活
        public const int COND_TYPE_DROP = 16;  //道具
        public const int COND_TYPE_ACTION = 101;  //行为
        public const int COND_TYPE_PVE_SCORE = 161; //PVE积分
        public const int COND_TYPE_SCORE = 163; //个人积分
        public const int COND_TYPE_CAMP_SCORE = 164; //阵营积分
        public const int COND_TYPE_BOSS_PART_DESTROY = 56; //boss部位破坏
        public const int COND_TYPE_DRAMA_TRIGGER_COUNT = 997;   //剧情触发次数
        public const int COND_TYPE_TASK_ACCEPTED = 998;   //接受任务
        public const int COND_TYPE_TASK_FINISH = 999;   //完成任务

        public const int DESTROY_SPAWN_DIE_MOSTER_TYPE = 1;  //怪物正常死亡标识
        public const int DESTROY_SPAWN_COLLECT_MOSTER_TYPE = 2;  //怪物回收标识

        public const int SYSTEM_NOTICE_ACTION_UP_TYPE = 0; //居中靠上提示
        public const int SYSTEM_NOTICE_ACTION_MIDDLE_TYPE = 1; //居中提示（可能需特效）
        public const int SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE = 2; //类型为进度条
        public const int SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE = 3; //右上角显示

        public const int COUNTER_NOTICE_ACTION = 4; //计数器系统提示
        public const int COUNTER_NOTICE_ACTION_INIT = 5;


        public const int SYSTEM_NOTICE_ACITON_UP_PLAYER_SCORE = 0; //个人总积分
        public const int SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE = 0; //带有进度条
        public const int SYSTEM_NOTICE_ACTION_RIGHTUP_CONTENT_TYPE = 1; //文字
        public const int SYSTEM_NOTICE_ACTION_RIGHTUP_TIME_TYPE = 2; //只显示CD时间
        public const int COPY_STAR_CHAGED = 6; //副本三星显示


        public const int SYSTEM_NOTICE_ACTION_PROGRESSBAR_LEFT_TYPE = 1; //类型为从左往右进度条
        public const int SYSTEM_NOTICE_ACTION_PROGRESSBAR_RIGHT_TYPE = 2; //类型为从右往左进度条

        public readonly static float CLOSE_MODE_FLOAT_FACTOR = 0.2f; //接近模式距离浮动系数(random(技能castRange*(1-CLOSE_MODE_FLOAT_FACTOR), 技能castRange*(1+CLOSE_MODE_FLOAT_FACTOR)))
        public readonly static float REFER = 1.6f;              //接近远离界定距离系数（技能castRange*REFER）
        public readonly static float PER_ANGLE = 40.0f;         //绕圈模式每次绕的角度 
        public readonly static float SPEED_FACTOR_MODE_0 = 0.5f;
        public readonly static float SPEED_FACTOR_MODE_1 = 0.4f;
        public readonly static float SPEED_FACTOR_MODE_2_3 = 0.4f;
        public readonly static float SPEED_FACTOR_MODE_5 = 1.6f;

        public const int MESSAGEBOX_SURE_COUNTDOWN_END_CLICK = 2;
        public const int MESSAGEBOX_AUTO_COUNTDOWN_CLICK = 0;

        public const float MIN_STRENG = 0.2f;
        public const float MAX_STRENG = 2.5f;
        public const float FREQUENCY = 3f;

        public const int BUFF_ID_ELEVATOR = 4903;       //上电梯时加上的buff，有该buff时意味正在电梯上
        public const int BUFF_ID_VIEWING_MODE = 4907;             //播CG时隐藏界面产生的buff，有该buff时意味着玩家正处于观影模式
        public const int BUFF_ID_SKILL_LOCK = 4943;     //被手动锁定时实体加上的buff，有该buff时意味着实体被自己锁定了，身上出现了锁定的标识

        public const int LEVEL_UP_VISUAL_FX_ID = 10000;
        public const int BORN_VISUAL_FX_ID = 10001;

        public const int AUTO_FIGHT_AI_ID = 3001;

        public const int CHOICE_SERVER_ACTION = 0;
        public const int CHOICE_CLIENT_ACTION = 1;

        public const int NOTICE_CAMP_SCORE = 1; //阵营积分UI
        public const int NOTICE_MONSTER_WAVE = 2; //怪物波数UI
        public const int NOTICE_SUPPLY = 3;       //剩余物资UI
        public const int NOTICE_COPY_PROGRESS = 4; //副本进度上UI
        public const int NOTICE_PLAYER_SCORE = 5; //玩家个人积分UI
        public const int NOTICE_COPY_PROGRESS_DOWN = 6; //副本进度下UI
        public const int NOTICE_TEAM_SCORE = 7; //PVE队伍积分
        public const int NOTICE_COMBAT_ATTRI = 8; //战斗输出属性
        public const int NOTICE_MAX_SCORE = 9; //pvp记分牌

        public const int QUALITY_SETTING_PLAYER = 0;
        public const int QUALITY_SETTING_AVATAR = 1;
        public const int QUALITY_SETTING_OTHERS = 2;

        public const int TIMER_TYPE_NORMAL = 0;
        public const int TIMER_TYPE_SPECIAL = 1;
        public const int TIMER_TYPE_RANDOM_RANGE = 2;
        public const int TIMER_TYPE_RANDOM_COUNT = 3;
    }
}
