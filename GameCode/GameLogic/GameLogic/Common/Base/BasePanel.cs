﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Utils;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.ExtendComponent;
using Mogo.Util;


namespace Common.Base
{
    public class BasePanelParameter
    {
        public const int InvalidTab = 0;

        public int FirstTab;
        public int SecondTab;
        public int ThirdTab;

        public BasePanelParameter(int firstTab = InvalidTab, int secondTab = InvalidTab, int thirdTab = InvalidTab)
        {
            this.FirstTab = firstTab;
            this.SecondTab = secondTab;
            this.ThirdTab = thirdTab;
        }
    }

    /**View视图--基础类**/
    public abstract class BasePanel : KContainer
    {

        public bool hasMovePos = false;
        private KButton _closeBtn;

        public bool isResizeByMyself = false;

        public static HashSet<PanelIdEnum> initialedPanelSet = new HashSet<PanelIdEnum>();

        private StateImage _modalMask;
        private bool _hasInitailCanvas = false;

        private Canvas _canvas;
        public Canvas Canvas
        {
            get
            {
                if (_hasInitailCanvas == false)
                {
                    _hasInitailCanvas = true;
                    _canvas = GetComponent<Canvas>();
                }
                return _canvas;
            }
        }

        public virtual void EnableCanvas()
        {
            Canvas.enabled = true;
        }

        public virtual void DisableCanvas()
        {
            Canvas.enabled = false;
        }


        public virtual void OnResize()
        {
            throw new Exception(string.Format("{0}.OnResize方法没有实现", ID));
        }

        protected sealed override void OnEnable()
        {
            base.OnEnable();
            if (Application.isEditor == true)
            {
                if (gameObject.GetComponent<PanelIdInspector>() == null)
                {
                    PanelIdInspector inspector = gameObject.AddComponent<PanelIdInspector>();
                    inspector.PanelId = (int)Id;
                }
            }
        }

        protected sealed override void OnDisable()
        {
            base.OnDisable();
        }

        public PanelIdEnum ID
        {
            get { return Id; }
        }

        protected abstract PanelIdEnum Id { get; }

        /// <summary>
        /// 此处初始化子元素
        /// </summary>
        protected override void Awake()
        {

        }

        /// <summary>
        /// 此处添加面板显示时的相关处理，比如添加事件处理
        /// </summary>
        /// <param name="data"></param>
        public abstract void OnShow(System.Object data);

        public virtual void SetData(System.Object data)
        {

        }

        public virtual void OnLeaveMap(int mapType)
        {
            Id.Close();
        }

        public virtual void OnEnterMap(int mapType)
        {

        }

        protected void SetTabList(int functionId, KToggleGroup toggleGroup)
        {
            _tabList = function_helper.GetFunctionTabList(functionId);
            SetTabList(_tabList, toggleGroup);
        }

        protected void SetTabList(List<int> functionIdList, KToggleGroup toggleGroup)
        {
            _tabList = functionIdList;
            _toggleList = toggleGroup.GetToggleList();
            _rectTransform = new List<RectTransform>();
            for (int i = 0; i < _toggleList.Count; i++)
            {
                _rectTransform.Add(_toggleList[i].GetComponent<RectTransform>());
            }
        }


        private List<int> _tabList;
        private List<KToggle> _toggleList;
        private List<RectTransform> _rectTransform;

        public void CheckTabList()
        {
            if (_tabList == null)
            {
                return;
            }
            List<int> hideList = function_helper.GetHideTabList(_tabList);
            List<RectTransform> _rectList = new List<RectTransform>();
            for (int i = 0; i < _toggleList.Count; i++)
            {
                if (hideList.Contains(i + 1))
                {
                    _toggleList[i].Visible = false;
                }
                else
                {
                    _toggleList[i].Visible = true;
                    _rectList.Add(_rectTransform[i]);
                }

            }
            if (_rectList.Count == 0)
            {
                return;
            }
            DoTabLayout(_rectList);
        }

        private static void DoTabLayout(List<RectTransform> _rectList)
        {
            int middle = _rectList.Count / 2;
            float startX;
            const float tabOverlapDistance = 20;    // PSD中两个Tab之间的重叠区域
            float gap = _rectList[middle].sizeDelta.x - tabOverlapDistance; // 修正之后使得Tab的标签页和PSD中保持一致
            float toggleGroupWidth = _rectList[middle].parent.GetComponent<RectTransform>().sizeDelta.x;
            if (_rectList.Count % 2 == 1)
            {
                startX = (toggleGroupWidth - _rectList[middle].sizeDelta.x) / 2;
            }
            else
            {
                startX = toggleGroupWidth / 2;
            }
            for (int i = 0; i < _rectList.Count; i++)
            {
                _rectList[i].anchoredPosition3D = new Vector3(startX - (middle - i) * (gap - 47), _rectList[i].anchoredPosition3D.y, _rectList[i].anchoredPosition3D.z);
            }
        }

        public void DoTabLayout(KToggleGroup group)
        {
            if(group == null || group.Visible == false)
            {
                return;
            }
            float toggleGroupWidth = group.GetComponent<RectTransform>().sizeDelta.x;
            int needShowCount = GetShowCount(group);
            List<KToggle> toggleList = group.GetToggleList();
            float toggleWidth = toggleList.Count > 0 ? toggleList[0].GetComponent<RectTransform>().sizeDelta.x : 0;
            float startX = (toggleGroupWidth - toggleWidth * needShowCount) * 0.5f;
            for (int i = 0; i < toggleList.Count; i++)
            {
                if (toggleList[i].Visible == true)
                {
                    Vector2 position = toggleList[i].GetComponent<RectTransform>().anchoredPosition;
                    toggleList[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(startX, position.y);
                    startX += toggleWidth;
                }
            }
        }

        private int GetShowCount(KToggleGroup group)
        {
            int count = 0;
            List<KToggle> toggleList = group.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                if (toggleList[i].Visible == true)
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// 此处添加面板关闭时的相关处理，比如移除事件处理
        /// </summary>
        public abstract void OnClose();


        public void OnDataResp()
        {
            if (initialedPanelSet.Contains(Id) == false)
            {
                initialedPanelSet.Add(Id);
                EventDispatcher.TriggerEvent(PanelEvents.ON_PANEL_DATA_RESP, Id);
            }
        }

        public static void OnDataResp(PanelIdEnum id)
        {
            if (initialedPanelSet.Contains(id) == false)
            {
                initialedPanelSet.Add(id);
                EventDispatcher.TriggerEvent(PanelEvents.ON_PANEL_DATA_RESP, id);
            }
        }


        protected KButton CloseBtn
        {
            get
            {
                return _closeBtn;
            }
            set
            {
                _closeBtn = value;
                _closeBtn.onClick.AddListener(OnCloseBtnClick);
            }
        }

        /// <summary>
        /// 模态遮罩
        /// </summary>
        protected StateImage ModalMask
        {
            get
            {
                return _modalMask;
            }
            set
            {
                _modalMask = value;
                MogoUtils.AdaptScreen(value);
            }
        }

        protected virtual void OnCloseBtnClick()
        {
            ClosePanel();
        }

        protected virtual void ClosePanel()
        {
            UIManager.Instance.ClosePanel(this.Id);
        }

        protected override void OnDestroy()
        {
            if (_closeBtn != null)
            {
                _closeBtn.onClick.RemoveListener(OnCloseBtnClick);
            }
            if (initialedPanelSet.Contains(Id) == true)
            {
                initialedPanelSet.Remove(Id);
            }
        }


    }

}
