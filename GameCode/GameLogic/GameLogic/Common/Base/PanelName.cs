﻿#region 模块信息
/*==========================================
// 模块名：PanelName
// 命名空间: Common.Base
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/04
// 描述说明：UIManager.Instance.ShowView参数列表
// 其他：新加Module中的View在此注册ID和名字
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using UnityEngine;


namespace Common.Base
{
    public class PanelIdEnumComparer : IEqualityComparer<PanelIdEnum>
    {
        public bool Equals(PanelIdEnum x, PanelIdEnum y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(PanelIdEnum obj)
        {
            return (int)obj;
        }
    }

    public enum PanelIdEnum
    {
        Empty = 0,
        Login = 1,//登陆
        Server = 2,//选服
        MainUIBattle = 3,    //战斗主UI
        MainBattleControl = 4,      //战斗控制UI
        CreateRole = 5,      //创号界面
        SelectRole = 6,        //选角界面
        Billboard = 7,//
        Bag = 8,//背包
        Rune = 9,//符文
        RuneLevelUp = 10,//符文升级
        RuneAdd = 11,//符文添加
        Information = 12,//玩家信息
        Skill = 13,     //技能
        Chat = 14,//聊天
        //FastNotice = 15,//通知
        Team = 16,//组队界面
        Gem = 17,//宝石
        Task = 18,//任务
        Wing = 19, //翅膀
        EquipToolTips = 20,
        RuneToolTips = 21,
        ExpRuneToolTips = 22,

        SendBox = 24,

  //ari MessageBox = 27,
        SellItemToolTips = 30,
        ItemToolTips = 31,
        FloatTips = 32,
        Spotlight = 33,
        NpcDailog = 34,
        Copy = 35,
        FightTips = 36,
        UILoadingBar = 37,
        //Wing = 38,

        Mail = 39, //邮件

        StarSoul = 41,
        QuickBuy = 42,//快速购买

        Interactive = 43,//好友

        TradeMarket = 50, //交易中心
        Mall = 51,

        Equip = 52, //装备系统
        EquipSmelter = 53, //装备熔炼
        EquipEnchantTip = 54, //装备附魔

        MallPop = 55,

        GemItemToolTips = 60,
        /// <summary>
        /// 代币ToolTips
        /// </summary>
        TokenItemToolTips = 61,
        SuitScrollToolTips = 62,

        ItemStory = 63,

        PlayerInfo = 64,
        WanderLand = 65,
        Make = 66,
        FriendSend = 67,
        Vip = 68,
        MakeMessage = 69,
        WanderLandDetail = 70,
        WanderLandBox = 71,

        /// <summary>
        /// 好感度
        /// </summary>
        Favor = 73,
        CopyResult = 74,
        CopyPreview = 76,
        CopyBox = 77,
        CopySweep = 78,
        SuitEquip = 79,

        ChatEmoji = 81,
        //ChatGift = 82,
        ChatMenu = 83,
        ChatPhraseEdit = 84,
        ChatReport = 85,

        Activity = 86,
        TaskParticle = 87,
        Token = 88,
        ActivityNotice = 89,

        RankingList = 90,
        RankingListDetail = 91,

        SkillToolTips = 101,
        //SkillShow = 101,
        // SkillDetail = 102,

        TomorrowActivity = 103,
        UseItemToolTips = 104,
        EnchantScrollToolTips = 105,

        Dreamland = 106,//幻境
        DreamlandPop = 107,//幻境弹窗

        Pet = 120,
        //SkillReplace = 121,
        PetPop = 122,
        PetStrengthen = 124,


        Fortress = 126,
        FortressUI = 125,

        DemonGate = 127,
        DemonGatePop = 128,

        Reward = 129,

        Setting = 130,
        ItemObtain = 131,

        GuildInformation = 132,
        GemPop = 133,
        GuildCreatePanel = 134,
        TeamEntry = 135,
        SkillNotice = 136,
        Function = 137,
        SkillCg = 138,
        GuideArrow = 139,
        CGDialog = 140,
        BossIntroduce = 141,

        // BackBattle = 142,
        FieldWorldMap = 143,
        FieldChangeLine = 144,
        DemonGateResult = 145,
        TeamInstance = 146,

        WelfareActive = 147,     //福利活动系统
        DreamlandResult = 148, //幻境结算

        PVEPreview = 150,
        PVEResult = 151,
        PVETeammate = 152,

        SingleInstanceIntroduction = 159,
        SingleInstance = 160,   // 单人副本

        RelivePop = 161,
        EnergyBuying = 162,  //体力购买

        WingToolTips = 163,

        PortalPanel = 164,

        MainUIField = 165,

        BattleElement = 166,
        PetGet = 167,
        MainUINotice = 170,

        GuildPop = 171,

        WingPop = 172,

        CommonTips = 173,

        // 道具产出渠道
        ItemChannel = 174,

        //规则介绍Tips
        RuleTips = 175,

        ChangeScene = 176,

        TreasureEntry = 177,

        TreasureReward = 178,
        SpecialGuide = 179,

        TreasureMapUse = 180,

        PortalInstance = 181,
        FieldBossIntroduce = 182,
  //ari SystemAlert = 183,
        Notice = 184,
        FightForceChannel = 185,

        RecommendFriend = 186,
        EquipChannel = 187,

        EquipUpgradeTips = 188,

        GuildInstance = 189,

        KickTeammate = 190,

        TimeAltar = 191,

        BossPartSelect = 192,

        BossPartPop = 193,

        PlayerInfoDetail = 200,

        CommonRule = 201,

        PK = 202,

        MaskPanel = 203,

        WorldBossResult = 204,

        Title = 205,
        TitleDetail = 206,

        Achievement = 207,
        AchievementTips = 208,
        AchievementObtainTips = 209,
        PlayAgain = 210,

        CombatPortalInstance = 211,

        PortalFight = 212,

        RuneEffect = 213,

        StarSoulTips = 214,

        TeamRewardAssign = 215,
        EquipAssignTips = 216,

        SmallMap = 217,

        Alchemy = 218,

        EquipFacade = 219,

        GuildInvite = 220,

        AlchemyInvite = 221,

        PetSkillTips = 223,


        GuildWar = 224,

        ChargeSelf = 225,

        Charge = 226, //充值，代充，vip统称

        GuildWarRankingTips = 227,

        GuildWarTry = 228,

        GuildWarTryTips = 229,

        GuildWarFlow = 230,

        GuildWarResult = 231,//公会战结算 

        WingGet = 232,

        GuildWarSignUp = 233,

        GuildWarReward = 234,

        GuildWarAssignedPosition = 235,

        GuildWarResultRecord = 236,

        Reconnect = 237,

        GuildWarDraw = 238,

        EquipDecomposition = 239,

        GuildWarMainUI = 240,

        GuildWarGroupMatch = 241,

        GuildWarFinalMatch = 242,

        SuitDetail = 243,

        GuildWarBattlePreview = 244,

        LuckyTurntable = 245,

        ChatEntry = 246,

        TeamRewardChatEntry = 247,

        //GuildWelfare = 248,

        TeamTaskOfflineReward = 249,

        TeamTaskTip = 250,

        DreamlandStationTips = 251,

        WingAttribute = 252,

        EquipFacadeTip = 253,

        ChargeOther = 254,

        RecastSkill = 255,

        BranchPreview = 256,

        RecastCompare = 257,

        //PlatformLogin = 258,

        FingerGuide = 259,

        PCFingerGuide = 260,

        GuildShopPop = 261,

        TeamMatch = 270,

        TeamDonate = 271,

        TeamInvitePop = 272,
        
        TeamTaskBox = 300,

        ServerLine = 301,

        CopyTaskResult = 302,

        EveningActivityStatistic = 303,

        ItemCommit = 304,

        CGParticle = 305,

        CloudPanel = 306,

        RewardBuffDetail = 307,

        RewardBuffTip = 308,

        OpenServerActivity = 309,

        ChargeActivity = 310,

        WelfareActiveTab = 311,

        SceneChange = 312,

        TreasureRewardPreview = 313,
        
        TeammateInfo = 314,

		LevelUpChannel = 315,

        TouchBattleMode = 316,

        OpenServerLoginReward = 317,

        Tutor = 318,

        EquipRisingStarPop = 319,

        RedEnvelope = 320,

        UIOperationShield = 330,

        DuelEntry = 331,

        Duel = 332,

        WorldBossRanking = 333,

        DuelScene = 334,

        GuildTask = 335,

        GuildTaskTips = 336,

        TradeWantBuy = 337,

        AntiAddiction = 338,

        BattleDataStatistic = 339,

 		SelectList = 340,
        
        BuffDetail = 341,
        
        InviteFriendRecord =342,
        
        RewardBox = 343,

        TradeBuy = 344,

        ChangeProficient = 350,

        ProficientSkill = 351,

        Festival = 352, //节日活动统称

        BagTips = 353,

        GuildBoss = 354,

        RewardCompensation = 355,

        BuffTips = 356,

        PetAttribute = 357,

        ItemCompose = 358,

        PCChooseMouseMode = 359,

        EquipFightForceTips = 360,

        GM = 999,
    }

}
