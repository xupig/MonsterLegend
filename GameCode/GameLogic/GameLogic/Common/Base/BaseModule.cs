﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Common.IManager;
using MogoEngine.Events;
using UnityEngine;
using GameMain.GlobalManager;
using Game.Asset;
using Game.UI.Builder;
using LitJson;
using Common.Utils;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using Game.UI.UIComponent;
using Common.Events;
using Common.ServerConfig;
using GameLoader;
using GameData;
using UnityEngine.UI;
using Common.ExtendComponent;
using Game.UI;
using Mogo.Util;
using GameLoader.Config;
using GameMain.GlobalManager.SubSystem;

namespace Common.Base
{

    public enum BlurUnderlay
    {
        Have = 1,
        Empty = 2,
    }

    public enum HideMainUI
    {
        Hide = 1,
        Not_Hide = 2,
    }

    /// <summary>
    /// 模块管理其下有一个或多个面板（Panel），包括面板加载，卸载，显示，关闭等行为及面板的生命周期
    /// 模块本身由ModuleManager管理
    /// </summary>
    public abstract class BaseModule
    {
        public static string UI_FOLDER = string.Format("GUI/{0}", SystemConfig.Language);
        public static Dictionary<PanelIdEnum, string> PanelPrefabPathDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        //用于将多个面板组合成主面板及其Tab子面板的效果
        public static Dictionary<PanelIdEnum, HashSet<PanelIdEnum>> panelPopBunchDict = new Dictionary<PanelIdEnum, HashSet<PanelIdEnum>>(new PanelIdEnumComparer());
        public static Dictionary<PanelIdEnum, PanelIdEnum> panelBunchDict = new Dictionary<PanelIdEnum, PanelIdEnum>(new PanelIdEnumComparer());
        private static List<PanelIdEnum> showPanelList = new List<PanelIdEnum>();
        private static Dictionary<PanelIdEnum, BasePanel> _showingPanelDict = new Dictionary<PanelIdEnum, BasePanel>(new PanelIdEnumComparer());
        private static HashSet<PanelIdEnum> _loadingPanelSet = new HashSet<PanelIdEnum>();
        private static Dictionary<PanelIdEnum, string> _panelDestroyEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelCloseEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelShowEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelClassNameDict = new Dictionary<PanelIdEnum, string>();
        private static Dictionary<PanelIdEnum, string> _panelEnterMapEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelLeaveMapEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelCanvasEnableEventDict = new Dictionary<PanelIdEnum, string>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, string> _panelCanvasDisableEventDict = new Dictionary<PanelIdEnum, string>();
        private static Dictionary<PanelIdEnum, BlurUnderlay> _panelBlurShowSettingDict = new Dictionary<PanelIdEnum, BlurUnderlay>(new PanelIdEnumComparer());           //Key为面板ID，Value为面板开启时是否显示模糊背景的设置
        private static Dictionary<PanelIdEnum, HideMainUI> _panelHideMainUISettingDict = new Dictionary<PanelIdEnum, HideMainUI>(new PanelIdEnumComparer());
        private static Dictionary<PanelIdEnum, MogoUILayer> _panelLayerDict = new Dictionary<PanelIdEnum, MogoUILayer>(new PanelIdEnumComparer());   //Key为面板ID，Value为面板应该放置的图层   
        public static Dictionary<MogoUILayer, HashSet<PanelIdEnum>> layerOpenedPanelSet = new Dictionary<MogoUILayer, HashSet<PanelIdEnum>>(new MogoUILayerComparer());
        public IModuleManager moduleManager;

        private Dictionary<PanelIdEnum, string> _panelPrefabDict;       //Key为面板ID，Value为资源名称
        private Dictionary<PanelIdEnum, BasePanel> _panelDict;          //Key为面板ID，Value为面板实例

        private static Dictionary<PanelIdEnum, bool> _panelCenterSettingDict = new Dictionary<PanelIdEnum, bool>();       //面板自适应居中
        private int _loadTimeoutSetting;



        private static bool _hasAddedMapEvent = false;

        public static PanelIdEnum currentGuidePanel = PanelIdEnum.Empty;

        public BaseModule()
        {
            _panelPrefabDict = new Dictionary<PanelIdEnum, string>();
            _panelDict = new Dictionary<PanelIdEnum, BasePanel>();
            _loadTimeoutSetting = XMLManager.global_params.ContainsKey(115) ? int.Parse(XMLManager.global_params[115].__value) : 30 * 1000;

            if (_hasAddedMapEvent == false)
            {
                _hasAddedMapEvent = true;
                EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, OnLeaveMap);
                EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            }
        }

        public static bool CanShowMainUILayer()
        {
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                if (IsPanelMutexWithMainUILayer(kvp.Key) && _showingPanelDict[kvp.Key].Canvas.enabled)//全屏界面并且是显示状态
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 关闭互斥面板
        /// </summary>
        /// <param name="panelId"></param>
        public static void CloseMutexPanel(PanelIdEnum panelId)
        {
            if (IsMutexLayer(panelId) == false)//不是UIPanel
            {
                return;
            }
            PanelIdEnum modulePanelId = PanelIdEnum.Empty;
            if (panelBunchDict.ContainsKey(panelId))
            {
                modulePanelId = panelBunchDict[panelId];
            }
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                PanelIdEnum id = kvp.Key;
                if (id != panelId)
                {
                    if (_panelLayerDict[id] == MogoUILayer.LayerUIPopPanel)
                    {
                        if (panelPopBunchDict[id].Contains(modulePanelId) == false)
                        {
                            UIManager.Instance.DisablePanelCanvas(id);
                        }
                        else if (_panelLayerDict[id] == _panelLayerDict[panelId])//层次相同
                        {
                            UIManager.Instance.DisablePanelCanvas(id);
                        }
                    }
                    else
                    {
                        if (panelBunchDict.ContainsKey(id))//不同模块
                        {
                            if (modulePanelId != panelBunchDict[id])
                            {
                                UIManager.Instance.DisablePanelCanvas(id);
                            }
                        }
                        else if (_panelLayerDict[id] == _panelLayerDict[panelId] || (modulePanelId != PanelIdEnum.Empty && (_panelLayerDict[id] == MogoUILayer.LayerUIPanel || _panelLayerDict[id] == MogoUILayer.LayerSpecialUIPanel)))//层次相同
                        {
                            UIManager.Instance.DisablePanelCanvas(id);
                        }
                    }

                }
            }
            if (panelId != PanelIdEnum.Chat && _showingPanelDict.ContainsKey(PanelIdEnum.Chat))
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.Chat, true);
            }
        }

        public static void CloseLayerLoadingPanel(MogoUILayer layer)
        {
            List<PanelIdEnum> removeList = new List<PanelIdEnum>();
            foreach (PanelIdEnum panelId in _loadingPanelSet)
            {
                if (_panelLayerDict[panelId] == layer && IsMutexLayer(panelId))
                {
                    removeList.Add(panelId);
                }
            }
            for (int i = 0; i < removeList.Count; i++)
            {
                _loadingPanelSet.Remove(removeList[i]);
            }
        }

        public static void PrintLog()
        {
            string content = string.Empty;
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                content += "\n" + kvp.Key + "," + kvp.Value.Canvas.enabled;
            }
            //ari LoggerHelper.Info("panel info:" + content);
            //ari LoggerHelper.Info("guide info:" + GuidePanel.isShow + "," + GuidePanel.inspector.url + "," + BaseModule.currentGuidePanel + "," + GuidePanel.inspector.pos);
        }

        private static List<PanelIdEnum> removeList = new List<PanelIdEnum>();

        /// <summary>
        /// 关闭正在加载中的互斥界面
        /// </summary>
        /// <param name="panelId"></param>
        public static void CloseAllLoadingMutexPanel(PanelIdEnum panelId)
        {
            if (IsMutexLayer(panelId) == false)//不是UIPanel
            {
                return;
            }
            removeList.Clear();
            foreach (PanelIdEnum id in _loadingPanelSet)
            {
                if (id != panelId && IsMutexLayer(id))
                {
                    PanelIdEnum modulePanelId = PanelIdEnum.Empty;
                    if (panelBunchDict.ContainsKey(panelId))
                    {
                        modulePanelId = panelBunchDict[panelId];
                    }

                    if (_panelLayerDict[id] == MogoUILayer.LayerUIPopPanel)
                    {
                        if (panelPopBunchDict[id].Contains(modulePanelId) == false)
                        {
                            removeList.Add(id);
                            // UIManager.Instance.DisablePanelCanvas(id);
                        }
                        else if (_panelLayerDict[id] == _panelLayerDict[panelId])//层次相同
                        {
                            removeList.Add(id);
                            //UIManager.Instance.DisablePanelCanvas(id);
                        }
                    }
                    else
                    {
                        if (panelBunchDict.ContainsKey(id))//不同模块
                        {
                            if (modulePanelId != panelBunchDict[id])
                            {
                                removeList.Add(id);
                                //UIManager.Instance.DisablePanelCanvas(id);
                            }
                        }
                        else if (_panelLayerDict[id] == _panelLayerDict[panelId])//层次相同
                        {
                            removeList.Add(id);
                            //UIManager.Instance.DisablePanelCanvas(id);
                        }
                    }


                }
            }
            for (int i = 0; i < removeList.Count; i++)
            {
                _loadingPanelSet.Remove(removeList[i]);
            }
        }

        /// <summary>
        /// 互斥的层
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        private static bool IsMutexLayer(PanelIdEnum panelId)
        {
            return _panelLayerDict[panelId] == MogoUILayer.LayerUIPanel || _panelLayerDict[panelId] == MogoUILayer.LayerSpecialUIPanel || _panelLayerDict[panelId] == MogoUILayer.LayerUIPopPanel;
        }

        private static List<PanelIdEnum> changeMapPanelList = new List<PanelIdEnum>();
        private static void OnLeaveMap(int mapId)
        {
            //Debug.LogError("OnLeaveMap:"+mapId);
            changeMapPanelList.Clear();
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                changeMapPanelList.Add(kvp.Key);
            }
            for (int i = 0; i < changeMapPanelList.Count; i++)
            {
                EventDispatcher.TriggerEvent(GetLeaveMapEventName(changeMapPanelList[i]), changeMapPanelList[i], mapId);
            }
            UIManager.change_map_flag = false;
        }

        private static void OnEnterMap(int mapType)
        {
            changeMapPanelList.Clear();
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                changeMapPanelList.Add(kvp.Key);
            }
            for (int i = 0; i < changeMapPanelList.Count; i++)
            {
                EventDispatcher.TriggerEvent(GetEnterMapEventName(changeMapPanelList[i]), changeMapPanelList[i], mapType);
            }
        }

        abstract public ModuleType ModuleType { get; }
        /// <summary>
        /// 此处添加模块下属面板的开启和关闭事件监听
        /// </summary>
        abstract public void Init();

        public void RegisterPanelModule(PanelIdEnum panelId, PanelIdEnum modulePanelId)
        {
            if (panelBunchDict.ContainsKey(panelId) == false)
            {
                panelBunchDict.Add(panelId, modulePanelId);
            }
        }

        /// <summary>
        /// 注册PopPanel的绑定UIPanel面板，对应的绑定面板打开的时候不会关闭PopPanel
        /// </summary>
        /// <param name="panelId"></param>
        /// <param name="modulePanelId"></param>
        public void RegisterPopPanelBunchPanel(PanelIdEnum panelId, PanelIdEnum modulePanelId)
        {
            if (panelPopBunchDict.ContainsKey(panelId) == false)
            {
                panelPopBunchDict.Add(panelId, new HashSet<PanelIdEnum>());
            }
            if (panelPopBunchDict[panelId].Contains(modulePanelId) == false)
            {
                panelPopBunchDict[panelId].Add(modulePanelId);
            }
        }

        protected void AddPanel(PanelIdEnum panelId, string prefab, MogoUILayer layer, string panelClassName, BlurUnderlay panelType, HideMainUI hideMainUI, bool isCenterAdjust = true)
        {
            if (panelType == BlurUnderlay.Have)
            {
                if (layer != MogoUILayer.LayerSpecialUIPanel && layer != MogoUILayer.LayerUIPanel)
                {
                    Debug.LogError("有模糊底的面板的层次一定要是LayerUIPanel或LayerSpecialUIPanel:" + panelId);
                }
                if (hideMainUI != HideMainUI.Hide)
                {
                    Debug.LogError("有模糊底的面板一定要关闭MainUI:" + panelId);
                }
            }
            if (hideMainUI == HideMainUI.Hide)
            {
                if (layer == MogoUILayer.LayerUIPopPanel)
                {
                    Debug.LogError("UI弹出层的面板一定不隐藏MainUI的:" + panelId);
                }
            }
            if (layer == MogoUILayer.LayerUIPopPanel)
            {
                if (panelPopBunchDict.ContainsKey(panelId) == false)
                {
                    Debug.LogError("在Add弹出层的面板前要先注册弹出层面板的主面板:" + panelId);
                }
            }
            if (_panelClassNameDict.ContainsKey(panelId) == false)
            {
                _panelClassNameDict.Add(panelId, panelClassName);
                _panelPrefabDict.Add(panelId, prefab);
                _panelLayerDict.Add(panelId, layer);
                _panelBlurShowSettingDict.Add(panelId, panelType);
                _panelHideMainUISettingDict.Add(panelId, hideMainUI);
                _panelCenterSettingDict.Add(panelId, isCenterAdjust);
                PanelPrefabPathDict.Add(panelId, GetPanelPrefabPath(panelId));
                string showEventType = string.Concat("Panel_", (int)panelId, "_show");
                string closeEventType = string.Concat("Panel_", (int)panelId, "_close");
                string enterMapEventType = string.Concat("Panel_", (int)panelId, "_enterMap");
                string leaveMapEventType = string.Concat("Panel_", (int)panelId, "_leaveMap");
                string enablePanelCanvasEventType = string.Concat("Panel_", (int)panelId, "_CanvasEnable");
                string disablePanelCanvasEventType = string.Concat("Panel_", (int)panelId, "_CanvasDisable");
                string destroyPanelCanvasEventType = string.Concat("Panel_", (int)panelId, "_CanvasDestroy");
                _panelShowEventDict.Add(panelId, showEventType);
                _panelCloseEventDict.Add(panelId, closeEventType);
                _panelEnterMapEventDict.Add(panelId, enterMapEventType);
                _panelLeaveMapEventDict.Add(panelId, leaveMapEventType);
                _panelCanvasEnableEventDict.Add(panelId, enablePanelCanvasEventType);
                _panelCanvasDisableEventDict.Add(panelId, disablePanelCanvasEventType);
                _panelDestroyEventDict.Add(panelId, destroyPanelCanvasEventType);
                AddPanelEventListener(panelId);
            }
            else
            {
                Debug.LogError(panelId + "  " + panelClassName);
            }
        }

        private void AddPanelEventListener(PanelIdEnum panelId)
        {
            string showEventType = GetShowEventName(panelId);
            string closeEventType = GetCloseEventName(panelId);
            string enterMapEventType = GetEnterMapEventName(panelId);
            string leaveMapEventType = GetLeaveMapEventName(panelId);
            string enablePanelCanvasEventType = GetEnablePanelCanvasEventName(panelId);
            string disablePanelCanvasEventType = GetDisablePanelCanvasEventName(panelId);
            string destroyPanelCanvasEventType = GetDestroyPanelCanvasEventName(panelId);
            EventDispatcher.AddEventListener<PanelIdEnum, System.Object>(showEventType, ShowPanel);
            EventDispatcher.AddEventListener<PanelIdEnum, bool>(closeEventType, ClosePanel);
            EventDispatcher.AddEventListener<PanelIdEnum, int>(enterMapEventType, EnterMap);
            EventDispatcher.AddEventListener<PanelIdEnum, int>(leaveMapEventType, LeaveMap);
            EventDispatcher.AddEventListener<PanelIdEnum>(enablePanelCanvasEventType, EnablePanelCanvas);
            EventDispatcher.AddEventListener<PanelIdEnum>(disablePanelCanvasEventType, DisablePanelCanvas);
            EventDispatcher.AddEventListener<PanelIdEnum>(destroyPanelCanvasEventType, DestroyPanel);
        }


        private void UpdateUIMainLayer(PanelIdEnum panelId)
        {
            if (IsPanelMutexWithMainUILayer(panelId))
            {
                UIManager.Instance.HideLayer(MogoUILayer.LayerUIMain);
                UIManager.Instance.HideLayer(MogoUILayer.LayerUnderPanel);
                CameraManager.GetInstance().HideMainCamera();
            }
            else if (IsUILayerPanel(panelId) && _panelBlurShowSettingDict[panelId] == BlurUnderlay.Empty)//
            {
                if (panelId != PanelIdEnum.CGDialog && panelId != PanelIdEnum.NpcDailog)
                {
                    UIManager.Instance.ShowLayer(MogoUILayer.LayerUIMain);
                    UIManager.Instance.ShowLayer(MogoUILayer.LayerUnderPanel);
                }
                CameraManager.GetInstance().ShowMainCamera();
            }
        }

        private void UpdateBlurUnderlay(PanelIdEnum panelId)
        {
            if (_panelBlurShowSettingDict[panelId] == BlurUnderlay.Have && _showingPanelDict.ContainsKey(panelId))
            {
                UIManager.Instance.ShowBlurUnderlay(_showingPanelDict[panelId]);
            }
            else if (IsUILayerPanel(panelId) && _panelBlurShowSettingDict[panelId] == BlurUnderlay.Empty)
            {
                UIManager.Instance.HideBlurUnderlay();
            }
        }

        private void EnablePanelCanvas(PanelIdEnum panelId)
        {
            //Debug.LogError("EnablePanelCanvas:" + panelId);
            if (panelId != PanelIdEnum.FloatTips && panelId != PanelIdEnum.Spotlight && _panelLayerDict[panelId] != MogoUILayer.LayerUIPopPanel)//这两个在野外台频繁过滤掉
            {
                UpdateUIMainLayer(panelId);
                UpdateBlurUnderlay(panelId);
            }
            if (_panelDict.ContainsKey(panelId) && _panelDict[panelId].Canvas != null && _panelDict[panelId].Canvas.enabled == false)
            {
                _panelDict[panelId].EnableCanvas();
                if (_panelDict[panelId].hasMovePos)
                {
                    _panelDict[panelId].transform.localPosition += new Vector3(0, 2000, 0);
                    _panelDict[panelId].hasMovePos = false;
                }
                EventDispatcher.TriggerEvent(PanelEvents.ENABLE_PANEL_CANVAS, panelId);
                if (panelId == currentGuidePanel)
                {
                    EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
                }
            }
        }

        private void DisablePanelCanvas(PanelIdEnum panelId)
        {
            //Debug.LogError("DisablePanelCanvas:" + panelId);
            if (_panelDict.ContainsKey(panelId) && _panelDict[panelId].Canvas != null && _panelDict[panelId].Canvas.enabled)
            {
                _panelDict[panelId].DisableCanvas();
                if (_panelDict[panelId].hasMovePos == false)
                {
                    _panelDict[panelId].transform.localPosition -= new Vector3(0, 2000, 0);
                }
                _panelDict[panelId].hasMovePos = true;
                if (panelId == currentGuidePanel)
                {
                    EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
                }
            }
        }

        private void EnterMap(PanelIdEnum id, int mapType)
        {
            if (_panelDict.ContainsKey(id))
            {
                _panelDict[id].OnEnterMap(mapType);
            }
        }

        private void LeaveMap(PanelIdEnum id, int mapId)
        {
            if (_panelDict.ContainsKey(id))
            {
                _panelDict[id].OnLeaveMap(mapId);
            }
        }

        public static bool HasFullScreenPanel()
        {
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                if (kvp.Value.Canvas.enabled == true)
                {
                    MogoUILayer layer = _panelLayerDict[kvp.Key];
                    if (layer == MogoUILayer.LayerUIPanel || layer == MogoUILayer.LayerSpecialUIPanel || layer == MogoUILayer.LayerHideOther)
                    {
                        return true;
                    }
                    if (_panelHideMainUISettingDict[kvp.Key] == HideMainUI.Hide)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsFullScreenPanel(PanelIdEnum panelId)
        {
            MogoUILayer layer = _panelLayerDict[panelId];
            if (layer == MogoUILayer.LayerUIPanel || layer == MogoUILayer.LayerSpecialUIPanel || layer == MogoUILayer.LayerHideOther)
            {
                return true;
            }
            if (_panelHideMainUISettingDict[panelId] == HideMainUI.Hide)
            {
                return true;
            }
            return false;
        }

        public static string GetShowEventName(PanelIdEnum panelId)
        {
            if (_panelShowEventDict.ContainsKey(panelId))
            {
                return _panelShowEventDict[panelId];
            }
            return string.Empty;
        }

        public static string GetCloseEventName(PanelIdEnum panelId)
        {
            if (_panelCloseEventDict.ContainsKey(panelId))
            {
                return _panelCloseEventDict[panelId];
            }
            return string.Empty;
        }

        private static string GetEnterMapEventName(PanelIdEnum id)
        {
            if (_panelEnterMapEventDict.ContainsKey(id))
            {
                return _panelEnterMapEventDict[id];
            }
            return string.Empty;
        }

        private static string GetLeaveMapEventName(PanelIdEnum id)
        {
            if (_panelLeaveMapEventDict.ContainsKey(id))
            {
                return _panelLeaveMapEventDict[id];
            }
            return string.Empty;
        }

        public static string GetEnablePanelCanvasEventName(PanelIdEnum id)
        {
            if (_panelCanvasEnableEventDict.ContainsKey(id))
            {
                return _panelCanvasEnableEventDict[id];
            }
            return string.Empty;
        }

        public static string GetDestroyPanelCanvasEventName(PanelIdEnum id)
        {
            if (_panelDestroyEventDict.ContainsKey(id))
            {
                return _panelDestroyEventDict[id];
            }
            return string.Empty;
        }

        public static string GetDisablePanelCanvasEventName(PanelIdEnum id)
        {
            if (_panelCanvasDisableEventDict.ContainsKey(id))
            {
                return _panelCanvasDisableEventDict[id];
            }
            return string.Empty;
        }

        private void RemovePanelEventListener(PanelIdEnum panelId)
        {
            string showEventType = GetShowEventName(panelId);
            string closeEventType = GetCloseEventName(panelId);
            string enterMapEventType = GetEnterMapEventName(panelId);
            string leaveMapEventType = GetLeaveMapEventName(panelId);
            string enablePanelCanvasEventType = GetEnablePanelCanvasEventName(panelId);
            string disablePanelCanvasEventType = GetDisablePanelCanvasEventName(panelId);
            string destroyPanelCanvasEventType = GetDestroyPanelCanvasEventName(panelId);
            EventDispatcher.RemoveEventListener<PanelIdEnum, System.Object>(showEventType, ShowPanel);
            EventDispatcher.RemoveEventListener<PanelIdEnum, bool>(closeEventType, ClosePanel);
            EventDispatcher.RemoveEventListener<PanelIdEnum, int>(enterMapEventType, EnterMap);
            EventDispatcher.RemoveEventListener<PanelIdEnum, int>(leaveMapEventType, LeaveMap);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(enablePanelCanvasEventType, EnablePanelCanvas);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(disablePanelCanvasEventType, DisablePanelCanvas);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(destroyPanelCanvasEventType, DestroyPanel);
        }

        private void SetPanelEnable(PanelIdEnum panelId)
        {
            if (UIManager.GetLayerVisibility(_panelLayerDict[panelId]))
            {
                EnablePanelCanvas(panelId);
            }
            else
            {
                DisablePanelCanvas(panelId);
            }
        }

        public static void OnResize()
        {
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                PanelIdEnum panelId = kvp.Key;
                float offset = kvp.Value.hasMovePos ? -2000 : 0;
                if (kvp.Value.isResizeByMyself)
                {
                    kvp.Value.OnResize();
                }
                else
                {
                    if (_panelCenterSettingDict.ContainsKey(panelId) && _panelCenterSettingDict[panelId])
                    {
                        MogoGameObjectHelper.SetPosition(kvp.Value.gameObject, Vector3.zero);
                    }
                    else
                    {
                        MogoGameObjectHelper.SetPosition(kvp.Value.gameObject, Vector3.zero);
                    }
                }
            }
        }

        #region 下属面板生命周期管理
        private void ShowPanel(PanelIdEnum panelId, System.Object data)
        {
            if (_panelDict.ContainsKey(panelId) == true)
            {
                if (IsPanelShowing(panelId) == false)
                {
                    RefreshBlurUnderlay(panelId);
                    ShowPanelGameObject(panelId);
                    ExcutePanelOnShow(panelId, data);
                    _panelDict[panelId].CheckTabList();
                    SortOrderedRenderAgent.RebuildAll();
                }
                else
                {
                    SetPanelEnable(panelId);
                    _panelDict[panelId].SetData(data);
                    _panelDict[panelId].CheckTabList();
                    if (panelId != PanelIdEnum.FloatTips && panelId != PanelIdEnum.Spotlight)
                    {
                        SortOrderedRenderAgent.RebuildAll();
                    }
                }
                EventDispatcher.TriggerEvent(PanelEvents.SHOW_PANEL, panelId);
            }
            else
            {
                RefreshBlurUnderlay(panelId);
                OnBeforeShowPanel(panelId);
                LoadPanel(panelId, data);
            }
        }

        protected virtual void OnBeforeShowPanel(PanelIdEnum panelId)
        {
        }

        private void LoadPanel(PanelIdEnum panelId, System.Object data)
        {
            if (_loadingPanelSet.Contains(panelId) == false)
            {
                _loadingPanelSet.Add(panelId);
                string path = GetPanelPrefabPath(panelId);
                string[] paths = new string[] { path };
                int beginTime = Environment.TickCount;
                uint timerId = 0;
                bool isFullScreenUI = IsPanelMutexWithMainUILayer(panelId);
                if (isFullScreenUI)
                {//加载全屏界面，才显示进度条
                    if (panelId != PanelIdEnum.UILoadingBar)
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.UILoadingBar, 0);
                    }
                    timerId = TimerHeap.AddTimer(50, 50, () =>
                    {
                        if (Environment.TickCount - beginTime >= _loadTimeoutSetting)
                        {
                            TimerHeap.DelTimer(timerId);
                           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, "UI加载失败", PanelIdEnum.MainUIField);
                        }
                    });
                }

                ObjectPool.Instance.GetUIGameObject(path, delegate(GameObject go)
                {
                    TimerHeap.DelTimer(timerId);
                    try
                    {
                        OnPrefabLoaded(go, panelId, data);
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Error(string.Format("UI:{0} 错误信息:{2} 打开失败:{1} ", path, ex.StackTrace, ex.Message));
                    }
                    if (panelId != PanelIdEnum.UILoadingBar)
                    {
                        UIManager.Instance.ClosePanel(PanelIdEnum.UILoadingBar);
                    }
                });
            }
        }

        private void OnPrefabLoaded(GameObject go, PanelIdEnum panelId, System.Object data)
        {
            if (_loadingPanelSet.Contains(panelId) == true)
            {
                _loadingPanelSet.Remove(panelId);
                string panelClassName = _panelClassNameDict[panelId];
                //此处调用Panel上的Awake方法，初始化面板子元素
                Type type = LoaderDriver.gameModulesAssembly.GetType(panelClassName);
                if (type != null)
                {
                    AddToUILayer(go, panelId);
                    go.SetActive(true);
                    //Build(go, panelId);
                    Component component = go.AddComponent(type);
                    BasePanel panel = component as BasePanel;
                    _panelDict.Add(panelId, panel);
                    ShowPanelGameObject(panelId);
                    ExcutePanelOnShow(panelId, data);
                    panel.CheckTabList();
                    EventDispatcher.TriggerEvent(PanelEvents.SHOW_PANEL, panelId);
                    SortOrderedRenderAgent.RebuildAll();
                }
                else
                {
                    LoggerHelper.Error("AddPanel的时候" + panelId + "的panelClassName错了");
                }
            }
            else
            {
                //面板资源加载完毕，但是面板已经被关闭
                DoCleanWork();
            }
        }

        private void ExcutePanelOnShow(PanelIdEnum panelId, System.Object data)
        {
            _panelDict[panelId].OnShow(data);
        }

        public static void Resize()
        {
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _showingPanelDict)
            {
                PanelIdEnum panelId = kvp.Key;
                float offset = kvp.Value.hasMovePos ? -2000 : 0;
                if (_panelCenterSettingDict.ContainsKey(panelId) && _panelCenterSettingDict[panelId])
                {
                    MogoGameObjectHelper.SetPosition(kvp.Value.gameObject, Vector3.zero);
                }
                else
                {
                    MogoGameObjectHelper.SetPosition(kvp.Value.gameObject, Vector3.zero);
                }
            }
        }

        private void AddToUILayer(GameObject go, PanelIdEnum panelId)
        {
            Transform parentTransform = UILayerManager.GetUILayerTransform(GetUILayer(panelId));
            if (parentTransform == null)
            {
                Debug.LogError("未找到面板的目标放置UILayer: " + (int)panelId);
                return;
            }
            go.transform.SetParent(parentTransform);
            go.name = go.name.Replace("(Clone)", string.Empty);
            RectTransform rectTrans = go.GetComponent<RectTransform>();
            rectTrans.pivot = new Vector2(0.0f, 0.0f);
            MogoGameObjectHelper.SetLocalScale(go, new Vector3(1.0f, 1.0f, 1.0f));
            MogoGameObjectHelper.SetPosition(go, new Vector3(0.0f, 0.0f, 0.0f));
            if (_panelCenterSettingDict.ContainsKey(panelId) && _panelCenterSettingDict[panelId])
            {
                MogoGameObjectHelper.SetPosition(go, Vector3.zero);
            }
            else
            {
                MogoGameObjectHelper.SetPosition(go, Vector3.zero);
            }
            go.layer = UIManager.UI_LAYER;
            Canvas canvas = go.GetComponent<Canvas>();
            if (canvas == null)
            {
                canvas = go.AddComponent<Canvas>();
                go.AddComponent<GraphicRaycaster>();
            }
            SortOrderedRenderAgent agent = go.AddComponent<SortOrderedRenderAgent>();
        }

        private static void HideBlurUnderlay(PanelIdEnum panelId)
        {
            if (_panelBlurShowSettingDict[panelId] == BlurUnderlay.Have)
            {
                UIManager.Instance.HideBlurUnderlay();
            }
        }


        private void RefreshBlurUnderlay(PanelIdEnum panelId)
        {
            if (_panelBlurShowSettingDict[panelId] == BlurUnderlay.Have)
            {
                UIManager.Instance.RefreshBlurUnderlay(panelId);
            }
        }

        public static void CGEndAndShow()
        {
            RecoverTopPanel(PanelIdEnum.CGDialog);
        }

        private static void RecoverTopPanel(PanelIdEnum panelId)
        {
            if (IsPanelNeedRecovery(panelId) == false)//需要恢复最顶的面板
            {
                return;
            }
            if (_loadingPanelSet.Count > 0)//如果有正在加载的面板
            {
                foreach (PanelIdEnum id in _loadingPanelSet)
                {
                    if (id != panelId && IsPanelNeedRecovery(id))//正在加载额度面板是有模糊底的
                    {
                        HideBlurUnderlay(panelId);
                        ShowMainUILayer(panelId);
                        return;
                    }
                }
            }
            BasePanel topPanel = null;//寻找需要显示的非UIPop面板
            BasePanel popPanel = null;//寻找需要显示的UIPop面板
            for (int i = showPanelList.Count - 1; i >= 0; i--)
            {
                if (showPanelList[i] != panelId && IsPanelNeedRecoveryBesidePopPanel(showPanelList[i]))
                {
                    topPanel = _showingPanelDict[showPanelList[i]];
                    break;
                }
            }
            for (int i = showPanelList.Count - 1; i >= 0; i--)
            {
                if (showPanelList[i] != panelId && _panelLayerDict[showPanelList[i]] == MogoUILayer.LayerUIPopPanel && (topPanel == null || panelPopBunchDict[showPanelList[i]].Contains(topPanel.ID)))
                {
                    popPanel = _showingPanelDict[showPanelList[i]];
                    break;
                }
            }
            if (popPanel != null)
            {
                UIManager.Instance.EnablePanelCanvas(popPanel.ID);
            }
            if (topPanel != null)//如果需要还原面板
            {
                if (panelBunchDict.ContainsKey(topPanel.ID))
                {
                    PanelIdEnum modulePanel = panelBunchDict[topPanel.ID];
                    if (modulePanel != topPanel.ID)
                    {
                        UIManager.Instance.EnablePanelCanvas(modulePanel);
                    }
                }
                UIManager.Instance.EnablePanelCanvas(topPanel.ID);
            }
            else//否则关闭模糊底、还原MainUI
            {
                HideBlurUnderlay(panelId);
                ShowMainUILayer(panelId);
            }
        }

        /// <summary>
        /// 与MainUI不共存
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        public static bool IsPanelMutexWithMainUILayer(PanelIdEnum panelId)
        {
            return _panelHideMainUISettingDict[panelId] == HideMainUI.Hide;
        }

        public static bool IsBlurUnderlay(PanelIdEnum panelId)
        {
            return _panelBlurShowSettingDict[panelId] == BlurUnderlay.Have;
        }

        private static bool IsPanelNeedRecovery(PanelIdEnum panelId)
        {
            return IsMutexLayer(panelId) || _panelLayerDict[panelId] == MogoUILayer.LayerHideOther;
        }

        private static bool IsPanelNeedRecoveryBesidePopPanel(PanelIdEnum panelId)
        {
            if (_panelLayerDict[panelId] == MogoUILayer.LayerUIPopPanel)
            {
                return false;
            }
            return IsMutexLayer(panelId) || _panelLayerDict[panelId] == MogoUILayer.LayerHideOther;
        }

        public static bool IsUILayerPanel(PanelIdEnum panelId)
        {
            return _panelLayerDict[panelId] == MogoUILayer.LayerUIPanel;
        }

        private static void ShowMainUILayer(PanelIdEnum panelId)
        {
            if (IsPanelMutexWithMainUILayer(panelId))
            {
                UIManager.Instance.ShowLayer(MogoUILayer.LayerUIMain);
                UIManager.Instance.ShowLayer(MogoUILayer.LayerUnderPanel);
                CameraManager.GetInstance().ShowMainCamera();
            }
        }

        private void ShowPanelGameObject(PanelIdEnum panelId)
        {
            _panelDict[panelId].gameObject.SetActive(true);
            AddToShowingPanelSet(panelId);
            SetPanelEnable(panelId);
        }

        private void AddToOpenLayerList(PanelIdEnum panelId)
        {
            if (_panelLayerDict.ContainsKey(panelId))
            {
                MogoUILayer layer = _panelLayerDict[panelId];
                if (layerOpenedPanelSet.ContainsKey(layer) == false)
                {
                    layerOpenedPanelSet.Add(layer, new HashSet<PanelIdEnum>());
                }
                if (layerOpenedPanelSet[layer].Contains(panelId) == false)
                {
                    layerOpenedPanelSet[layer].Add(panelId);
                }
            }
        }

        private void RemoveFromOpenLayerList(PanelIdEnum panelId)
        {
            if (_panelLayerDict.ContainsKey(panelId))
            {
                MogoUILayer layer = _panelLayerDict[panelId];
                if (layerOpenedPanelSet.ContainsKey(layer) && layerOpenedPanelSet[layer].Contains(panelId))
                {
                    layerOpenedPanelSet[layer].Remove(panelId);
                }
            }
        }

        private void HidePanelGameObject(PanelIdEnum panelId, bool isExclusive)
        {
            if (_panelDict.ContainsKey(panelId) == true)
            {
                _panelDict[panelId].gameObject.SetActive(false);
                RemoveFromShowingPanelSet(panelId, isExclusive);
            }
        }

        private void AddToShowingPanelSet(PanelIdEnum panelId)
        {
            if (_showingPanelDict.ContainsKey(panelId) == false)
            {
                AddToOpenLayerList(panelId);
                _showingPanelDict.Add(panelId, _panelDict[panelId]);
                showPanelList.Add(panelId);
            }
        }

        private void RemoveFromShowingPanelSet(PanelIdEnum panelId, bool isExclusive)
        {
            if (_showingPanelDict.ContainsKey(panelId) == true)
            {
                RemoveFromOpenLayerList(panelId);
                _showingPanelDict.Remove(panelId);
                showPanelList.Remove(panelId);
            }
        }



        private void ShowMainCamera(PanelIdEnum panelId)
        {
            if (_panelBlurShowSettingDict[panelId] == BlurUnderlay.Have && CanShowMainUILayer())
            {
                CameraManager.GetInstance().ShowMainCamera();
            }
        }


        private MogoUILayer GetUILayer(PanelIdEnum panelId)
        {
            return _panelLayerDict[panelId];
        }

        private void Build(GameObject go, PanelIdEnum panelId)
        {
            ComponentBuilder.ClearChildBuilder();
            ComponentBuilder builder = ComponentBuilder.typeBuilderDict[ComponentBuilder.TYPE_PANEL];
            builder.Build(go, true);
        }

        private string GetPanelPrefabPath(PanelIdEnum panelId)
        {
            return string.Format("{0}/{1}/{2}{3}", UI_FOLDER, _panelPrefabDict[panelId], _panelPrefabDict[panelId], ".prefab");
        }

        public static bool IsPanelShowing(PanelIdEnum panelId)
        {
            return _showingPanelDict.ContainsKey(panelId) == true;
        }

        public static bool IsPanelCanvasEnable(PanelIdEnum panelId)
        {
            if (IsPanelShowing(panelId) == false)
            {
                return false;
            }
            return _showingPanelDict[panelId].Canvas.enabled;
        }

        private void ClosePanel(PanelIdEnum panelId, bool isExclusive)
        {
            if (_panelDict.ContainsKey(panelId) == true && IsPanelShowing(panelId))
            {
                HidePanelGameObject(panelId, isExclusive);
                _panelDict[panelId].OnClose();
                DisablePanelCanvas(panelId);
                if (isExclusive == false && panelId != PanelIdEnum.CGDialog)
                {
                    RecoverTopPanel(panelId);
                }
            }
            if (_loadingPanelSet.Contains(panelId) == true)
            {
                _loadingPanelSet.Remove(panelId);
            }
            DoCleanWork();
            SortOrderedRenderAgent.RebuildAll();
        }

        #endregion

        private void DoCleanWork()
        {
            if (ModuleType == ModuleType.OneOff)
            {
                DestroyPanelDict();
            }
        }

        private bool IsAllPanelNotLoading()
        {
            foreach(KeyValuePair<PanelIdEnum,string> kvp in _panelPrefabDict)
            {
                if(_loadingPanelSet.Contains(kvp.Key))
                {
                    return false;
                }
            }
            return true;
        }

        private void DestroyPanelDict()
        {
            if (IsAllPanelClosed() == true && IsAllPanelNotLoading())
            {
                foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _panelDict)
                {
                    PanelIdEnum id = kvp.Key;
                    _showingPanelDict.Remove(id);
                    showPanelList.Remove(id);
                    _loadingPanelSet.Remove(id);
                    GameObject.Destroy(_panelDict[id].gameObject);
                }
                _panelDict.Clear();
            }
        }

        private void DestroyPanel(PanelIdEnum panelId)
        {
            if (_panelDict.ContainsKey(panelId))
            {
                _showingPanelDict.Remove(panelId);
                showPanelList.Remove(panelId);
                _loadingPanelSet.Remove(panelId);
                GameObject.Destroy(_panelDict[panelId].gameObject);
                _panelDict.Remove(panelId);
            }
        }

        private bool IsAllPanelClosed()
        {
            foreach (KeyValuePair<PanelIdEnum, BasePanel> kvp in _panelDict)
            {
                if (_showingPanelDict.ContainsKey(kvp.Key) == true)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public enum ModuleType
    {
        /// <summary>
        /// 一次性模块，关闭模块时即销毁模块，如登陆模块。
        /// </summary>
        OneOff,
        /// <summary>
        /// 永久性模块，关闭模块时只隐藏模块，如背包模块
        /// </summary>
        Permanent,
    }
}
