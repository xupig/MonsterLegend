﻿#region 模块信息
/*==========================================
// 文件名：PanelIdInspector
// 命名空间: GameLogic.GameLogic.Common.Base
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/30 17:32:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Base
{
    public class PanelIdInspector:MonoBehaviour
    {
        public int PanelId;
    }

    
}
