﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.IManager
{
    public interface IGameStateManager
    {
        void SetState(string state);
    }
}
