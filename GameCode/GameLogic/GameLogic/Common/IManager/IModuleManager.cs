﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Base;

namespace Common.IManager
{
    public interface IModuleManager
    {
        /**移除模块
         * @param T 接口类
         * **/
        void RemoveModule(string moduleName);

        /**获取模块
         * @param T 接口类
         * **/
        BaseModule GetModule(string moduleName);

        /**根据模块实例--注册模块
         * @param module 模块实例
         * **/
        BaseModule RegisterModule(BaseModule module);

        /**根据模块接口类型--注册模块
         * 1、约定:
         * @param key 模块的接口类
         * **/
        BaseModule RegisterModule(string moduleName);

        /**根据模块实例--注册模块
         * @param key    module对应接口类
         * @param module 模块实例
         * **/
        BaseModule RegisterModule(string moduleName, BaseModule module);
    }
}
