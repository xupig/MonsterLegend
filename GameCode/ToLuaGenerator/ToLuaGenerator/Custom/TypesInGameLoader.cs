﻿using GameLoader;
using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;

using GameLoader.IO;
using GameLoader.LoadingBar;
using GameLoader.Config;
using GameLoader.PlatformSdk;

public class TypesInGameLoader : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            return new List<BindType>(){
     
                _GT(typeof(FileAccessManager)),    
                _GT(typeof(ProgressBar)),  
                _GT(typeof(SystemConfig)),
                _GT(typeof(PlatformSdkMgr)),
            };
        }
    }
 
}
