﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;
using UnityEngine.UI;

public class TypesInUnityEngineUI : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            return new List<BindType>(){
     
                _GT(typeof(RectTransform)),
                _GT(typeof(Text)),
                _GT(typeof(GraphicRaycaster)),
                _GT(typeof(CanvasScaler)),
                _GT(typeof(CanvasScaler.ScaleMode)),
                _GT(typeof(Image)),
                _GT(typeof(Image.Type)),
                _GT(typeof(Image.FillMethod)),
                _GT(typeof(Image.OriginHorizontal)),
            };
        }
    }
 
}
