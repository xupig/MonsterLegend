﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;

using ACTSystem;

public class TypesInACTSystem : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            return new List<BindType>(){
     
                _GT(typeof(ACTActor)),                     
            };
        }
    }
 
}
