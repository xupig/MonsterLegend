﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;



public class TypesInSystem : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            return new List<BindType>(){
     
                _GT(typeof(Dictionary<string, bool>)).SetLibName("StringBoolMap"),
                _GT(typeof(KeyValuePair<string, bool>)),  
                _GT(typeof(Dictionary<int, string[]>)).SetLibName("IntStringArrayMap"),
                _GT(typeof(KeyValuePair<int, string[]>)), 
                _GT(typeof(Dictionary<string, string>)).SetLibName("StringStringMap"),
                _GT(typeof(KeyValuePair<string, string>)), 
            };
        }
    }
 
}
