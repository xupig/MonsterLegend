﻿using System.Collections.Generic;

using BindType = ToLuaMenu.BindType;

using MogoEngine;
using MogoEngine.Mgrs;

public class TypesInMogoEngine : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList
    {
        get
        {
            return new List<BindType>(){

                _GT(typeof(MogoWorld)),
                _GT(typeof(SceneMgr)),
                _GT(typeof(LoadSceneSetting)),
            };
        }
    }

}
