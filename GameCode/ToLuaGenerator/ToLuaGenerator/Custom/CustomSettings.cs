﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;

public static class CustomSettings
{
    public static string saveDir =  "../ToLuaWrap/ToLuaWrap/Generate/";
    public static string luaDir = "/Lua/";
    public static string toluaBaseType = "/ToLua/BaseType/";
    public static string toluaLuaDir = "/ToLua/Lua";

    //导出时强制做为静态类的类型(注意customTypeList 还要添加这个类型才能导出)
    //unity 有些类作为sealed class, 其实完全等价于静态类
    public static List<Type> staticClassTypes
    {        
        get{
            return new List<Type>(){
                typeof(UnityEngine.Application),
                typeof(UnityEngine.Time),
                typeof(UnityEngine.Screen),
                typeof(UnityEngine.SleepTimeout),
                typeof(UnityEngine.Input),
                typeof(UnityEngine.Resources),
                typeof(UnityEngine.Physics),
                typeof(UnityEngine.RenderSettings),
                typeof(UnityEngine.QualitySettings),
                typeof(UnityEngine.GL),
                typeof(GameMain.LuaGameState),
                typeof(GameMain.LuaPreloadPathsBuilder),
            };
        }
    }

    //附加导出委托类型(在导出委托时, customTypeList 中牵扯的委托类型都会导出， 无需写在这里)
    public static DelegateType[] customDelegateList 
    {        
        get{
            return new DelegateType[]{
                _DT(typeof(Action)),        
                _DT(typeof(UnityEngine.Events.UnityAction)), 
            };
        }       
    }

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            List<BindType> result = new List<BindType>();
            result.AddRange(TypesInUnityEngine.customTypeList);
            result.AddRange(TypesInUnityEngineUI.customTypeList);
            result.AddRange(TypesInSystem.customTypeList);
            result.AddRange(TypesInUIExtension.customTypeList);
            result.AddRange(TypesInMogoEngine.customTypeList);
            result.AddRange(TypesInGameLoader.customTypeList);
            result.AddRange(TypesInGameMain.customTypeList);
            result.AddRange(TypesInFirstPass.customTypeList);
            return result;
        }
    }

    public static List<Type> dynamicList
    {        
        get{
            return new List<Type>(){
                /*typeof(MeshRenderer),
                typeof(ParticleEmitter),
                typeof(ParticleRenderer),
                typeof(ParticleAnimator),

                typeof(BoxCollider),
                typeof(MeshCollider),
                typeof(SphereCollider),
                typeof(CharacterController),
                typeof(CapsuleCollider),

                typeof(Animation),
                typeof(AnimationClip),
                typeof(AnimationState),        

                typeof(BlendWeights),
                typeof(RenderTexture),
                typeof(Rigidbody),*/
            };
        }
    }

    //重载函数，相同参数个数，相同位置out参数匹配出问题时, 需要强制匹配解决
    //使用方法参见例子14
    public static List<Type> outList 
    {
        get { return new List<Type>(); }
    }

    static BindType _GT(Type t)
    {
        return new BindType(t);
    }

    static DelegateType _DT(Type t)
    {
        return new DelegateType(t);
    }    
}
