﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;
using UIExtension;
using TMPro;


public class TypesInFirstPass : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList
    {
        get
        {
            return new List<BindType>(){
                _GT(typeof(ButtonWrapper)),
                _GT(typeof(DropdownWrapper)),
                _GT(typeof(ImageWrapper)),
                _GT(typeof(InputFieldWrapper)),
                _GT(typeof(NotRenderingImageWrapper)),
                _GT(typeof(RawImageWrapper)),
                _GT(typeof(ScrollbarWrapper)),
                _GT(typeof(ScrollRectWrapper)),
                _GT(typeof(SliderWrapper)),
                _GT(typeof(TextWrapper)),
                _GT(typeof(ToggleWrapper)),

                _GT(typeof(TextMeshWrapper)),
                _GT(typeof(TextMeshProUGUI)),
                _GT(typeof(InputFieldMeshWrapper)),
            };
        }
    }

}
