﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;
using BindType = ToLuaMenu.BindType;
using System.Reflection;

using GameMain;

public class TypesInGameMain : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList
    {
        get
        {
            return new List<BindType>(){

                _GT(typeof(LuaFacade)),
                _GT(typeof(LuaGameState)),
                _GT(typeof(LuaPreloadPathsBuilder)),
                _GT(typeof(LuaBehaviour)),

                _GT(typeof(LuaUIComponent)),
                _GT(typeof(LuaUIDragable)),
                _GT(typeof(LuaUIPointable)),
                _GT(typeof(LuaUIPointableDragable)),
                _GT(typeof(LuaUIPanel)),
                _GT(typeof(LuaUIList)),
                _GT(typeof(LuaUIComplexList)),
                _GT(typeof(LuaUIListItem)),
                _GT(typeof(LuaUIListDirection)),
                _GT(typeof(LuaUIPageableList)),
                _GT(typeof(LuaUIScrollPage)),
                _GT(typeof(LuaUIScrollView)),
                _GT(typeof(LuaUIToggleGroup)),
                _GT(typeof(LuaUIToggle)),
                _GT(typeof(LuaUIProgressBar)),
                _GT(typeof(LuaUIScaleProgressBar)),
				_GT(typeof(LuaUINavigationMenu)),
                _GT(typeof(LuaUINavigationMenuItem)),
                _GT(typeof(LuaUIParticle)),
                _GT(typeof(LuaUIInputField)),
                _GT(typeof(LuaUIInputFieldMesh)),
                _GT(typeof(LuaUILinkTextMesh)),
                _GT(typeof(LuaUIChatEmojiItem)),
                _GT(typeof(LuaUIChatTextBlock)),
                _GT(typeof(LuaUIVoiceDriver)),
                _GT(typeof(LuaUIMultipleProgressBar)),
                _GT(typeof(LuaUISlider)),
                _GT(typeof(LuaUIRoller)),
                _GT(typeof(LuaUIRollerItem)),
                _GT(typeof(LuaUIRotateModel)),

                _GT(typeof(SortOrderedRenderAgent)),

                _GT(typeof(Locater)),
                _GT(typeof(Resizer)),
                _GT(typeof(Rotator)),
                _GT(typeof(XArtNumber)),
                _GT(typeof(XButtonHitArea)),
                _GT(typeof(XInvisibleButton)),
                _GT(typeof(XImageUpgradeFilling)),
                _GT(typeof(XImageFilling)),
                _GT(typeof(XImageScaleFilling)),
                _GT(typeof(XImageScaling)),
                _GT(typeof(XImageFloat)),
                _GT(typeof(XImageFollow)),
                _GT(typeof(XImageFlowLight)),
                _GT(typeof(XImageTweenAlpha)),
                _GT(typeof(XImageTweenColor)),
                _GT(typeof(XGameObjectTweenPosition)),
                _GT(typeof(XGameObjectTweenScale)),
                _GT(typeof(XGameObjectTweenRotation)),
                _GT(typeof(XGameObjectComplexIcon)),
                _GT(typeof(XGameObjectTweenSizeDelta)),
                _GT(typeof(XContainerRotator)),
                _GT(typeof(XScreenShot)),
                _GT(typeof(XIntroduceBoss)),
                _GT(typeof(XIntroduceNormal)),
                _GT(typeof(XUILine)),
                _GT(typeof(XIconAnim)),
                _GT(typeof(XIconAnimOnce)),
                _GT(typeof(ActorModelComponent)),
                _GT(typeof(EquipModelComponent)),
                _GT(typeof(PathModelComponent)),
                _GT(typeof(PlayerModelComponent)),
                _GT(typeof(XSkillDragContainer)),
                _GT(typeof(XImageChanger)),
                _GT(typeof(XLogoImage)),
                _GT(typeof(XHPBar)),

                _GT(typeof(EntityLuaBase)),
                _GT(typeof(EntityCreature)),
                _GT(typeof(EntityPlayer)),
                _GT(typeof(EntityBillboard)),
                _GT(typeof(EntityStatic)),
                _GT(typeof(EntityDropItems)),
                _GT(typeof(PosPointingArrow)),
                _GT(typeof(EntityPointingArrow)),
                _GT(typeof(PosBubbleDialog)),
                _GT(typeof(EntityBubbleDialog)),
                _GT(typeof(GameObjectPoolManager)),
                _GT(typeof(ServerProxyFacade)),
                _GT(typeof(XResourceDownloadProgress)),
                _GT(typeof(XMapPathPoint)),
                _GT(typeof(XLowHPContainer)),

                _GT(typeof(LuaTween)),
                _GT(typeof(LuaUIRawImage)),
                _GT(typeof(LuaUIDial)),
                _GT(typeof(LuaUIGridLayoutGroup)),
                _GT(typeof(LuaUIGridLayoutGroupItem)),
                _GT(typeof(LuaUILayoutElement)),
                _GT(typeof(XCutoutAdapter)),
                _GT(typeof(XDamageHandler)),
                _GT(typeof(XAutoCombat)),
            };
        }
    }

}
