﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using LuaInterface;

using BindType = ToLuaMenu.BindType;
using System.Reflection;

public class TypesInUnityEngine : TypesInSettingBase
{

    //在这里添加你要导出注册到lua的类型列表
    public static List<BindType> customTypeList 
    {                
        get{
            return new List<BindType>(){
     
                _GT(typeof(Debugger)),                      
                 
                _GT(typeof(Component)),
                _GT(typeof(Transform)),
                _GT(typeof(Material)),
                _GT(typeof(Light)),
                _GT(typeof(Rigidbody)),
                _GT(typeof(Camera)),
                _GT(typeof(AudioSource)),     
                        
                _GT(typeof(Behaviour)),
                _GT(typeof(MonoBehaviour)),        
                _GT(typeof(GameObject)),
                _GT(typeof(TrackedReference)),
                _GT(typeof(Application)),
                _GT(typeof(Physics)),
                _GT(typeof(Collider)),
                _GT(typeof(Time)),        
                _GT(typeof(Texture)),
                _GT(typeof(Texture2D)),
                _GT(typeof(Shader)),
                _GT(typeof(Renderer)),
                _GT(typeof(WWW)),
                _GT(typeof(Screen)),
                _GT(typeof(CameraClearFlags)),
                _GT(typeof(AudioClip)),
                _GT(typeof(AssetBundle)),
                _GT(typeof(ParticleSystem)),
                _GT(typeof(AsyncOperation)).SetBaseType(typeof(System.Object)),
                _GT(typeof(LightType)),
                //_GT(typeof(LightmapBakeType)),
                _GT(typeof(SleepTimeout)),
                _GT(typeof(Animator)),
                _GT(typeof(Input)),
                _GT(typeof(KeyCode)),
                _GT(typeof(SkinnedMeshRenderer)),
                _GT(typeof(Space)),        
                                           
                _GT(typeof(MeshRenderer)),            
                _GT(typeof(ParticleEmitter)),
                _GT(typeof(ParticleRenderer)),
                _GT(typeof(ParticleAnimator)), 
                              
                _GT(typeof(BoxCollider)),
                _GT(typeof(MeshCollider)),
                _GT(typeof(SphereCollider)),        
                _GT(typeof(CharacterController)),
                _GT(typeof(CapsuleCollider)),
        
                _GT(typeof(Animation)),        
                _GT(typeof(AnimationClip)).SetBaseType(typeof(UnityEngine.Object)),        
                _GT(typeof(AnimationState)),
                _GT(typeof(AnimationBlendMode)),
                _GT(typeof(QueueMode)),  
                _GT(typeof(PlayMode)),
                _GT(typeof(WrapMode)),

                _GT(typeof(QualitySettings)),
                _GT(typeof(RenderSettings)),                                                   
                _GT(typeof(BlendWeights)),           
                _GT(typeof(RenderTexture)),
                _GT(typeof(RenderTextureFormat)),

                _GT(typeof(EventSystem)),
                _GT(typeof(StandaloneInputModule)),
                _GT(typeof(Canvas)),
                _GT(typeof(RenderMode)),
                _GT(typeof(FogMode)),
                _GT(typeof(PointerEventData)),
                _GT(typeof(PointerInputModule)),
                _GT(typeof(RectTransformUtility)),
                _GT(typeof(Rect)),
                _GT(typeof(RenderingPath)),
            };
        }
    }
 
}
