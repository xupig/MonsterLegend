﻿using UnityEngine;
using UnityEngine.UI;

namespace GameResource
{
    public class SelectableAssembler : AssetAssembler
    {
        public override void Assemble(GameObject go, string[] tokens)
        {
            string type = tokens[0];
            Selectable selectable = go.GetComponent(type) as Selectable;
            if (selectable != null && selectable.transition == Selectable.Transition.SpriteSwap)
            {
                SpriteState spriteState = new SpriteState();
                string key = tokens[1];
                if (!string.IsNullOrEmpty(key))
                {
                    spriteState.highlightedSprite = ObjectPool.Instance.GetAssemblyObject(key) as Sprite;
                }
                key = tokens[2];
                if (!string.IsNullOrEmpty(key))
                {
                    spriteState.pressedSprite = ObjectPool.Instance.GetAssemblyObject(key) as Sprite;
                }
                key = tokens[3];
                if (!string.IsNullOrEmpty(key))
                {
                    spriteState.disabledSprite = ObjectPool.Instance.GetAssemblyObject(key) as Sprite;
                }
                selectable.spriteState = spriteState;
            }
            else if(selectable == null)
            {
                Debug.LogError("selectable == null in " + go.name );
            }
        }

    }
}
