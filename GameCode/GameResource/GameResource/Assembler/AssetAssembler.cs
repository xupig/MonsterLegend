﻿using UnityEngine;
using System.Collections.Generic;
using Game.AssetBridge;

namespace GameResource
{
    public abstract class AssetAssembler
    {
        public const string ANIMATOR_CONTROLLER = "AnimatorController";
        public const string ANIMATION = "Animation";
        public const string MESH_FILTER = "MeshFilter";
        public const string MESH_COLLIDER = "MeshCollider";
        public const string RENDERER = "Renderer";
        public const string SKINNED_MESH_RENDERER = "SkinnedMeshRenderer";
        public const string PARTICLE_SYSTEM_RENDERER = "ParticleSystemRenderer";
        public const string TERRAIN = "Terrain";
        public const string SELECTABLE = "Selectable";
        public const string ASSET = "asset";
        public const string LIGHTMAPUV = "LightmapUV";


        public static char[] COMMA_SPLIT = new char[] { ',' };

        private static Dictionary<string, AssetAssembler> _assemblerDict = new Dictionary<string, AssetAssembler>();

        static AssetAssembler()
        {
            _assemblerDict.Add(ANIMATOR_CONTROLLER, new AnimatorControllerAssembler());
            _assemblerDict.Add(ANIMATION, new AnimationAssembler());
            _assemblerDict.Add(MESH_FILTER, new MeshFilterAssembler());
            _assemblerDict.Add(MESH_COLLIDER, new MeshColliderAssembler());
            _assemblerDict.Add(RENDERER, new RendererAssembler());
            _assemblerDict.Add(SKINNED_MESH_RENDERER, new SkinnedMeshRendererAssembler());
            _assemblerDict.Add(PARTICLE_SYSTEM_RENDERER, new ParticleSystemRendererAssembler());
            _assemblerDict.Add(TERRAIN, new TerrainAssembler());
            _assemblerDict.Add(SELECTABLE, new SelectableAssembler());
            _assemblerDict.Add(LIGHTMAPUV, new LightmapUVAssembler());
        }

        public static void Parse(GameObject go, AssetEntry[] entries)
        {
            for(int i = 0; i < entries.Length; i++)
            {
                AssetEntry entry = entries[i];
                string asset = entry.asset;
                _assemblerDict[asset].Assemble(go, entry.tokens);
            }
        }

        public static void Parse(GameObject go, AssetEntryEx[] entries)
        {
            for (int i = 0; i < entries.Length; i++)
            {
                AssetEntryEx entry = entries[i];
                string asset = entry.asset;
                _assemblerDict[asset].Assemble(go, entry.tokens);
            }
        }

        public static void Parse(GameObject go, string asset, string[] tokens)
        {
           _assemblerDict[asset].Assemble(go, tokens);
        }

        public abstract void Assemble(GameObject go, string[] tokens);

        public static Vector2 GetVector2(string token)
        {
            string[] list = token.Split(COMMA_SPLIT);
            return new Vector2(float.Parse(list[0]), float.Parse(list[1]));
        }
    }
}

