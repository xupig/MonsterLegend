﻿using UnityEngine;

namespace GameResource
{
    public class AnimationAssembler : AssetAssembler
    {
        public override void Assemble(GameObject go, string[] tokens)
        {
            var ani = go.GetComponent<Animation>();
            var aniName = tokens[0];
            int type = 0;
            if (int.TryParse(tokens[1], out type))
            {

            }
            if(ani != null && !string.IsNullOrEmpty(aniName))
            {
                var clip = ObjectPool.Instance.GetAssemblyObject(aniName) as AnimationClip;
                ani.AddClip(clip, clip.name);
                ani.clip = clip;
                ani.cullingType = (AnimationCullingType)type;
                if (!ani.isPlaying)
                    ani.Play();
            }
        }

    }
}
