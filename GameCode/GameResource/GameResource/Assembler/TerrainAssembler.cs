﻿using UnityEngine;

namespace GameResource
{
    public class TerrainAssembler : AssetAssembler
    {
        //Key example: Scenes$Compond$Outside_new$tex$156afa40.png.u;4,4;0,0;
        public static char[] SEMICOLON_SPLIT = new char[] { ';' };

        public override void Assemble(GameObject go, string[] tokens)
        {
            if (go.CompareTag(DelayedConfig.TAG_DELAYED))//
            {
                DelayedTerrainAssembler com = go.AddComponent<DelayedTerrainAssembler>();
                com.tokens = tokens;
            }
            else
            {
                string[] splatKeys = tokens;
                Terrain terrain = go.GetComponent<Terrain>();
                if (terrain != null)
                {
                    SplatPrototype[] splatPrototypes = new SplatPrototype[splatKeys.Length];
                    for (int i = 0; i < splatKeys.Length; i++)
                    {
                        string token = splatKeys[i];
                        string[] paramList = token.Split(SEMICOLON_SPLIT);
                        SplatPrototype splat = new SplatPrototype();
                        splat.normalMap = null;
                        splat.texture = ObjectPool.Instance.GetAssemblyObject(paramList[0]) as Texture2D;
                        splat.tileSize = GetVector2(paramList[1]);
                        splat.tileOffset = GetVector2(paramList[2]);
                        var p3 = GetVector2(paramList[3]);
                        splat.metallic = p3.x;
                        splat.smoothness = p3.y;
                        if (paramList.Length == 5 && paramList[4] != "")
                        {
                            splat.normalMap = ObjectPool.Instance.GetAssemblyObject(paramList[4]) as Texture2D;
                        }
                        splatPrototypes[i] = splat;
                    }
                    terrain.terrainData.splatPrototypes = splatPrototypes;
                }
            }
        }

    }
}
