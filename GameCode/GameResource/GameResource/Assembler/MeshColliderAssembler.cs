﻿using UnityEngine;

namespace GameResource
{
    public class MeshColliderAssembler : AssetAssembler
    {
        public override void Assemble(GameObject go, string[] tokens)
        {
            string key = tokens[0];
            string meshHostName = tokens[1];
            MeshCollider collider = go.GetComponent<MeshCollider>();
            if (collider != null)
            {
                Object obj = ObjectPool.Instance.GetAssemblyObject(key) as Object;
                if (obj is Mesh)
                {
                    collider.sharedMesh = obj as Mesh;
                    return;
                }
                GameObject model = obj as GameObject;
                if (model == null) return;
                Transform hostTransform = model.transform.Find(meshHostName);
                if (hostTransform == null)
                {
                    hostTransform = model.transform;
                }
                if (hostTransform != null)
                {
                    MeshCollider meshCollider = hostTransform.GetComponent<MeshCollider>();
                    if (meshCollider != null && meshCollider.sharedMesh != null)
                    {
                        collider.sharedMesh = meshCollider.sharedMesh;
                        return;
                    }
                }
                var newMesh = GetMesh(model.transform, meshHostName);
                if (newMesh != null)
                {
                    collider.sharedMesh = newMesh;
                }
                else
                {
                    Debug.LogError(string.Format("Mesh {0} not found in {1}!", meshHostName, key));
                }
            }
        }

        private Mesh GetMesh(Transform modelTransform, string meshHostName)
        {
            if (modelTransform.name == meshHostName)
            {
                MeshFilter meshFilter = modelTransform.GetComponent<MeshFilter>();
                if (meshFilter != null && meshFilter.sharedMesh != null)
                {
                    return meshFilter.sharedMesh;
                }
            }
            for (int i = 0; i < modelTransform.childCount; i++)
            {
                Transform childTransform = modelTransform.GetChild(i);
                Mesh mesh = GetMesh(childTransform, meshHostName);
                if (mesh != null) return mesh;
            }
            return null;
        }
    }
}
