﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

namespace GameResource
{
    public class PrefabBridge : MonoBehaviour
    {
        public string physicalPath;
        void Awake()
        {
            if (string.IsNullOrEmpty(physicalPath))
            {
                Debug.LogError("PrefabBridge:Awake:" + this.gameObject.name);
                return;
            }
            JsonData jsonData = ObjectPool.Instance.GetShadowJsonData(physicalPath);
            var settings = (IDictionary)jsonData["settings"];
            int layer = 0;
            PrefabBridge.RecoverGameObject(this.gameObject, ref layer, settings);
        }

        public static void RecoverGameObject(GameObject go, ref int layer, IDictionary jsonData)
        {
            int childCount = go.transform.childCount;
            var key = layer.ToString();
            if (jsonData.Contains(key))
            {
                var settting = (IList)jsonData[key];
                for (int x = 0; x < settting.Count; x++)
                {
                    JsonData entrySet = (JsonData)settting[x];
                    string asset = (string)entrySet[0];
                    string[] tokens = new string[entrySet.Count - 1];
                    for (int j = 1; j < entrySet.Count; j++)
                    {
                        tokens[j - 1] = (string)entrySet[j];
                    }
                    AssetAssembler.Parse(go, asset, tokens);
                }
            }
            for (int i = 0; i < childCount; i++)
            {
                GameObject child = go.transform.GetChild(i).gameObject;
                layer = layer + 1;
                RecoverGameObject(child, ref layer, jsonData);
            }
        }
    }

    public class AssetCreator
    {
        public const string SHADER = "shader";
        public const string PROPERTY_LIST = "propertyList";
        public const string SHADER_KEYWORDS = "shaderKeywords";
        public const string FLOAT = "Float";
        public const string COLOR = "Color";
        public const string TEX_ENV = "TexEnv";
        public const string VECTOR = "Vector";
        public const string RANGE = "Range";
        public const string KEYWORDS = "Keywords";
        public const string RENDERQUEUE = "RQ";
        public const string CONTROLLER = "controller";
        public const string PREFAB = "prefab";
        public const string SETTINGS = "settings";
        public const string ASSEMBLE_ON_INST = "assembleOnInst";
        public const string CLIP_KEYS = "clipKeys";
        public const string DEFAULT_DIFFUSE = "Default-Diffuse";
        public const string SHADER_DEFAULT_DIFFUSE_KEY = "Shader$Diffuse.u";
        public const string DEFAULT_PARTICLE = "Default-Particle";
        public const string DEFAULT_PARTICLE_TEXTURE = "Default-Particle-Texture";
        public const string SHADER_DEFAULT_PARTICLE_KEY = "Shader$XGame/MOGO2/Particles/MOGO2_ParticlesBlend43.u";

        private static Material _defaultDiffuseMaterial;
        private static Material _defaultParticleMaterial;
        private static Dictionary<string, Material> _prepareMaterials = new Dictionary<string, Material>();

        public static Material GetDefaultDiffuseMaterial()
        {
            if (_defaultDiffuseMaterial == null)
            {
                _defaultDiffuseMaterial = new Material(GetShader(SHADER_DEFAULT_DIFFUSE_KEY));
                _defaultDiffuseMaterial.name = DEFAULT_DIFFUSE;
            }
            return _defaultDiffuseMaterial;
        }

        public static Material GetDefaultParticleMaterial()
        {
            if (_defaultParticleMaterial == null)
            {
                _defaultParticleMaterial = new Material(GetShader(SHADER_DEFAULT_PARTICLE_KEY));
                _defaultParticleMaterial.SetTexture("_MainTex", ObjectPool.Instance.GetDefaultParticleTexture());
                _defaultParticleMaterial.name = DEFAULT_PARTICLE;
            }
            return _defaultParticleMaterial;
        }

        public static void PrepareMaterials(string[] prewarmupShaders)
        {
            for (int i = 0; i < prewarmupShaders.Length; i++)
            {
                string shaderKey = prewarmupShaders[i];
                var shader = GetShader(shaderKey);
                _prepareMaterials[shaderKey] = new Material(shader);
            }
        }

        public static Material CreateMaterial(string name, string shaderKey)
        {
            Material result = null;
            if (_prepareMaterials.ContainsKey(shaderKey))
            {
                result = new Material(_prepareMaterials[shaderKey]);
                result.name = name;
                return result;
            }
            var shader = GetShader(shaderKey);
            if (shader == null)
            {
                shader = GetShader(SHADER_DEFAULT_DIFFUSE_KEY);
            }
            Material mat = new Material(shader);
            _prepareMaterials[shaderKey] = mat;
            result = new Material(mat);
            result.name = name;
            return result;
        }

        public static Material CreateMaterial(JsonData jsonData, string name)
        {
            string shaderKey = (string)jsonData[SHADER];
            var shader = GetShader(shaderKey);
            Material material = CreateMaterial(name, shaderKey); ;
            material.name = name;
            JsonData propertyList = jsonData[PROPERTY_LIST];
            for (int i = 0; i < propertyList.Count; i++)
            {
                JsonData property = propertyList[i];
                ParseMaterialProperty(material, property);
            }
            IOSVestSetting.OnCreateMaterial(material);
            return material;
        }

        static Dictionary<string, DelayedMaterial> _delayedMaterials = new Dictionary<string, DelayedMaterial>();
        public static DelayedMaterial CreateDelayedMaterial(string physicalPath)
        {
            if (!_delayedMaterials.ContainsKey(physicalPath))
            {
                //if (physicalPath.Contains("1001_sky_shadow"))
                //{
                //    Debug.LogError("CreateDelayedMaterial new 1001_sky_shadow");
                //}
                _delayedMaterials[physicalPath] = new DelayedMaterial(physicalPath);
            }
            return _delayedMaterials[physicalPath];
        }

        public static Shader GetShader(string shaderKey)
        {
            Shader shader = ObjectPool.Instance.GetAssemblyShader(shaderKey);
            if (shader == null)
            {
                Debug.LogError("can't not find shader:" + shaderKey);
            }
            if (UnityPropUtils.IsEditor == true && Shader.Find(shader.name) != null)
            {
                shader = Shader.Find(shader.name);
            }
            return shader;
        }

        public static void ParseMaterialProperty(Material material, JsonData property)
        {
            string type = (string)property[0];
            string name = (string)property[1];
            switch (type)
            {
                case RANGE:
                case FLOAT:
                    material.SetFloat(name, float.Parse((string)property[2]));
                    break;
                case COLOR:
                    material.SetColor(name, new Color(float.Parse((string)property[2]), float.Parse((string)property[3]), float.Parse((string)property[4]), float.Parse((string)property[5])));
                    break;
                case TEX_ENV:
                    //if (((string)property[2]).Contains("SKY_chuangjue"))
                    //    Debug.LogError("ParseMaterialProperty: " + name + " " + ((string)property[2]));
                    material.SetTexture(name, ObjectPool.Instance.GetAssemblyObject((string)property[2]) as Texture);
                    material.SetTextureOffset(name, new Vector2(float.Parse((string)property[3]), float.Parse((string)property[4])));
                    material.SetTextureScale(name, new Vector2(float.Parse((string)property[5]), float.Parse((string)property[6])));
                    break;
                case VECTOR:
                    material.SetVector(name, new Vector4(float.Parse((string)property[2]), float.Parse((string)property[3]), float.Parse((string)property[4]), float.Parse((string)property[5])));
                    break;
                case KEYWORDS:

                    string[] newShaderKeywords = new string[property.Count - 2];
                    for (int i = 0; i < newShaderKeywords.Length; i++)
                    {
                        newShaderKeywords[i] = (string)property[i + 2];
                    }
                    material.shaderKeywords = newShaderKeywords;
                    break;
                case RENDERQUEUE:
                    material.renderQueue = int.Parse(name);
                    break;
            }
        }

        public static AnimatorOverrideController CreateAnimatorController(JsonData jsonData)
        {
            string name = (string)jsonData[CONTROLLER];
            AnimatorOverrideController controller = new AnimatorOverrideController();
            controller.runtimeAnimatorController = ObjectPool.Instance.GetAssemblyObject((string)jsonData[CONTROLLER]) as RuntimeAnimatorController;
            JsonData clipKeys = jsonData[CLIP_KEYS];
            for (int i = 0; i < clipKeys.Count; i++)
            {
                JsonData clipData = clipKeys[i];
                string clipName = (string)clipData[0];
                string clipKey = (string)clipData[1];
                controller[clipName] = ObjectPool.Instance.GetAssemblyObject(clipKey) as AnimationClip;
            }
            return controller;
        }

        static Dictionary<string, DelayedAnimatorController> _delayAnimatorControllers = new Dictionary<string, DelayedAnimatorController>();
        public static DelayedAnimatorController CreateDelayAnimatorController(string physicalPath)
        {
            if (!_delayAnimatorControllers.ContainsKey(physicalPath))
            {
                _delayAnimatorControllers[physicalPath] = new DelayedAnimatorController(physicalPath);
            }
            return _delayAnimatorControllers[physicalPath];
        }



        public static GameObject CreateGameObject(JsonData jsonData, string physicalPath, bool delay = true)
        {
            string prefabName = (string)jsonData[PREFAB];
            var go = ObjectPool.Instance.GetAssemblyObject(prefabName) as GameObject;
            if (go == null)
            {
                Debug.LogError("CreateGameObject ERROR :" + prefabName);
                return null;
            }
            if (delay)
            {
                DelayedConfig.SetDelayTag(go.transform);
            }
            int layer = 0;
            try
            {
                if ((jsonData.Keys.Contains(ASSEMBLE_ON_INST) && (int)jsonData[ASSEMBLE_ON_INST] == 1))
                {
                    PrefabBridge prefabBridge = go.AddComponent<PrefabBridge>();
                    prefabBridge.physicalPath = physicalPath;
                }
                else
                {
                    var settings = (IDictionary)jsonData[SETTINGS];
                    PrefabBridge.RecoverGameObject(go, ref layer, settings);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("CreateGameObject Error:\n" + go.name + "\n" + ex.Message + "\n \n traceback:" + ex.StackTrace);
            }
            return go;
        }

        static List<string> _releasePhysicalPathList = new List<string>();
        static void ClearUnuseMaterials()
        {
            _releasePhysicalPathList.Clear();
            foreach (KeyValuePair<string, DelayedMaterial> kvp in _delayedMaterials)
            {
                string physicalPath = kvp.Key;
                //if (physicalPath.Contains("1001_sky_shadow"))
                //{
                //    Debug.LogError("ClearUnuseMaterials 1001_sky_shadow: " + ObjectPool.Instance.IsActiveObject(physicalPath));
                //}
                if (!ObjectPool.Instance.IsActiveObject(physicalPath))
                {
                    _releasePhysicalPathList.Add(physicalPath);
                }
            }
            for (int i = 0; i < _releasePhysicalPathList.Count; i++)
            {
                string physicalPath = _releasePhysicalPathList[i];
                _delayedMaterials[physicalPath].Release();
                _delayedMaterials.Remove(physicalPath);
                //if (physicalPath.Contains("1001_sky_shadow"))
                //{
                //    Debug.LogError("_delayedMaterials Remove 1001_sky_shadow");
                //}
            }
        }

        static void ClearUnuseControllers()
        {
            DelayedAnimatorController.LoadAllWhenInit = false;
            _releasePhysicalPathList.Clear();
            foreach (KeyValuePair<string, DelayedAnimatorController> kvp in _delayAnimatorControllers)
            {
                string physicalPath = kvp.Key;
                if (!ObjectPool.Instance.IsActiveObject(physicalPath))
                {
                    _releasePhysicalPathList.Add(physicalPath);
                }
            }
            for (int i = 0; i < _releasePhysicalPathList.Count; i++)
            {
                string physicalPath = _releasePhysicalPathList[i];
                _delayAnimatorControllers[physicalPath].Release();
                _delayAnimatorControllers.Remove(physicalPath);
            }
        }

        public static void OnDeleteZeroReferrencedAsset()
        {
            ClearUnuseMaterials();
            ClearUnuseControllers();
        }

        public static void FullAllDelayControllers()
        {
            DelayedAnimatorController.LoadAllWhenInit = true;
            if (_delayAnimatorControllers != null && _delayAnimatorControllers.Count > 0)
            {
                foreach (var pair in _delayAnimatorControllers)
                {
                    pair.Value.LoadAllClips();
                }
            }
        }
    }
}
