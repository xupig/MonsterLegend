﻿using LitJson;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace GameResource
{

    public class DelayedConfig
    {
        public readonly static string TAG_DELAYED = "Delayed";

        public readonly static string CAN_DELAY_PATH_SCENES_PREFAB = "^Scenes.*?(\\.prefab)$";
        public readonly static string CAN_DELAY_PATH_CHARACTERS_PREFAB = "^Characters.*?(\\.prefab)$";

        public readonly static string CAN_DELAY_PHYSICAL_PATH = ".*(\\.tga\\.u|\\.png\\.u|\\.cubemap\\.u)$";//\\.anim\\.u|

        private static bool _inited = false;
        public static bool CanDelayLoading = false;


        public static void InitConfig()
        {
            if (_inited) return;
            _inited = true;
            CanDelayLoading = true;
            if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer
               || ObjectPool.Instance.HasExternalAsset())
            {
                CanDelayLoading = true;
            }
        }

        public static bool IsPlatformCanDelay()
        {
            return CanDelayLoading;
        }

        public static bool IsPathCanDelay(string path)
        {
            bool result = false;
            if (Regex.IsMatch(path, CAN_DELAY_PATH_SCENES_PREFAB)) return true;  //场景资源
            //if (Regex.IsMatch(path, CAN_DELAY_PATH_CHARACTERS_PREFAB)) return true; //角色资源
            return result;
        }

        public static bool IsPhysicalPathCanDelay(string physicalPath)
        {
            return Regex.IsMatch(physicalPath, CAN_DELAY_PHYSICAL_PATH);
        }

        static void SetTag(Transform trans, string tag, bool includeChildren = false)
        {
            trans.gameObject.tag = tag;
            if (includeChildren)
            {
                for (int i = 0; i < trans.childCount; i++)
                {
                    SetTag(trans.GetChild(i), tag, includeChildren);
                }
            }
        }

        public static void SetDelayTag(Transform trans)
        {
            //Debug.LogError("SetDelayTag: "+ trans);
            if (trans.gameObject.CompareTag(TAG_DELAYED)) return;
            SetTag(trans, TAG_DELAYED, true);
        }
    }

}
