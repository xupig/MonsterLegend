﻿using LitJson;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class DelayedRendererAssembler : MonoBehaviour
    {
        public string[] tokens;

        bool _showed = false;
        bool _loaded = false;

        static Material[] _defaultMaterials;
        static Material[] CreateDefaultMaterials()
        {
            if (_defaultMaterials == null)
            {
                Material material = new Material(Shader.Find("XGame/Legacy Shaders/Transparent/Diffuse"));
                material.SetColor("_Color", DelayedMaterial.DEFAULT_COLOR);
                _defaultMaterials = new Material[] { material };
            }
            return _defaultMaterials;
        }

        public static Action UpdateCheck;
        DelayedMaterial[] _delayedMaterials;

        void Awake()
        {
            SetDefaultMaterials();
            UpdateCheck += UpdateCheckLoaded;
        }

        void OnDestroy()
        {
            UpdateCheck -= UpdateCheckLoaded;
        }

        void OnBecameVisible()
        {
            Show();
        }

        void Show()
        {
            if (_showed) return;
            _showed = true;
            CreateDelayedMaterials();
        }

        int spaceTime = 0;
        void UpdateCheckLoaded()
        {
            spaceTime--;
            if (spaceTime > 0) return;
            spaceTime = 10;
            if (!_showed) return;
            if (_loaded) return;
            CheckLoaded();
        }

        void CheckLoaded()
        {
            if (_delayedMaterials == null) return;
            bool allLoaded = true;
            for (int i = 0; i < _delayedMaterials.Length; i++)
            {
                if (_delayedMaterials[i] != null && !_delayedMaterials[i].loaded)
                {
                    allLoaded = false;
                }
            }
            if (allLoaded)
            {
                _loaded = true;
                string type = tokens[0];
                Renderer render = gameObject.GetComponent(type) as Renderer;
                if (render != null)
                {
                    int count = _delayedMaterials.Length;
                    Material[] materials = new Material[count];
                    bool canReplace = false;
                    for (int j = 0; j < count; j++)
                    {
                        if (_delayedMaterials[j] != null)
                        {
                            materials[j] = _delayedMaterials[j].material;
                            canReplace = true;
                        }
                    }
                    if (canReplace)render.materials = materials;
                }
                UpdateCheck -= UpdateCheckLoaded;
            }
        }

        void SetDefaultMaterials()
        {
            Renderer render = gameObject.GetComponent<Renderer>();
            if (render != null)
            {
                render.materials = CreateDefaultMaterials();
            } 
        }

        void CreateDelayedMaterials()
        {
            string type = tokens[0];
            int materialCount = tokens.Length - 1;
            Renderer render = gameObject.GetComponent(type) as Renderer;
            if (render != null)
            {
                _delayedMaterials = new DelayedMaterial[materialCount];
                for (int i = 0; i < materialCount; i++)
                {
                    string physicalPath = tokens[i + 1];
                    if (physicalPath != AssetCreator.DEFAULT_DIFFUSE)
                    {
                        _delayedMaterials[i] = AssetCreator.CreateDelayedMaterial(physicalPath);
                    }
                }
            }
        }
    }
}
