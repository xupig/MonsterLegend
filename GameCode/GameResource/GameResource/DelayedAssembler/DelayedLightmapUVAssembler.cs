﻿using LitJson;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class DelayedLightmapUVAssembler : MonoBehaviour
    {
        public string[] tokens;
        public static bool LoadLightmapFinished;
        public static Action LoadLightmapFinishedEvent;
        private static string TypeRender = "render";
        private static string TypeTerrain = "terrain";
        bool _showed = false;
        bool _destroyed = false;
        bool _hasAddEvent = false;
        string _type;
        int _index;
        Vector4 _scaleOffset;

        void Start()
        {
            if (tokens.Length != 6)
            {
                Debug.LogError("LightmapUV tokens.Length not equals 6, value: " + tokens.Length);
                _showed = true;
                return;
            }
            _type = tokens[0];
            _index = 0;
            for (int index = 0; index < 10; index++)
            {
                if (tokens[1].Contains(index.ToString()))
                {
                    _index = index;
                    break;
                }
            }
            _scaleOffset = new Vector4(float.Parse(tokens[2]), float.Parse(tokens[3]), float.Parse(tokens[4]), float.Parse(tokens[5]));

            if (LoadLightmapFinished)
                Show();
            else
            {
                if (_type == TypeTerrain)
                {
                    LoadLightmapFinishedEvent += Show;
                }
            }
        }

        //int spaceTime = 0;
        //void Update()
        //{
        //    if (_showed) return;
        //    spaceTime--;
        //    if (spaceTime > 0) return;
        //    spaceTime = 10;
        //    if (LoadLightmapFinished)
        //    {
        //        Show();
        //    }
        //}

        void OnDestory()
        {
            _destroyed = true;
            if (_hasAddEvent)
                LoadLightmapFinishedEvent -= Show;
        }

        void OnBecameVisible()
        {
            if (_showed) return;
            //_isVisible = true;
            if (LoadLightmapFinished)
            {
                Show();
            }
            else
            {
                LoadLightmapFinishedEvent += Show;
                _hasAddEvent = true;
            }
        }

        void Show()
        {
            //if (!_isVisible) return;
            if (_showed) return;
            //_showed = true;
            LoadLightmapUV();
        }

        void LoadLightmapUV()
        {
            if (_destroyed)
                return;
            if (_type == TypeRender)
            {
                var render = gameObject.GetComponent<Renderer>();
                if (render != null)
                {
                    render.lightmapIndex = _index;
                    render.lightmapScaleOffset = _scaleOffset;
                }
                _showed = true;
                enabled = false;
            }
            if (_type == TypeTerrain)
            {
                var terr = gameObject.GetComponent<Terrain>();
                if (terr != null)
                {
                    terr.lightmapIndex = _index;
                    terr.lightmapScaleOffset = _scaleOffset;
                }
                _showed = true;
                enabled = false;
            }
        }
    }
}
