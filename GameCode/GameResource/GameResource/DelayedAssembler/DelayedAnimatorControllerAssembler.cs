﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class DelayedAnimatorControllerAssembler : MonoBehaviour
    {
        public string[] tokens;

        private Animator _animator;
        private DelayedAnimatorController _delayedAnimatorController;

        void Start()
        {
            //Debug.LogError("1");
            _animator = GetComponent<Animator>();
            //Debug.LogError("2");
            if (_animator != null)
            {
                //Debug.LogError("2 1");
                string controllerKey = tokens[0];
                //Debug.LogError("2 2");
                _delayedAnimatorController = AssetCreator.CreateDelayAnimatorController(controllerKey);
                //Debug.LogError("2 3");
                _animator.runtimeAnimatorController = _delayedAnimatorController.controller;
                //Debug.LogError("2 4");
            }
            //Debug.LogError("3");
            if (gameObject.name.Equals("face"))
            {
                //Debug.LogError("3 1");
                _delayedAnimatorController.LoadAllClips();
                //Debug.LogError("3 2");
            }
            //Debug.LogError("4");
        }

        int spaceTime = 0;
        void Update()
        {
            spaceTime--;
            if (spaceTime > 0) return;
            spaceTime = 10;
            for (int i = 0; i < _animator.layerCount; i++)
            {
                AnimatorClipInfo[] infos = _animator.GetCurrentAnimatorClipInfo(i);
                foreach (AnimatorClipInfo info in infos)
                {
                    string currentClipName = info.clip.name;
                    _delayedAnimatorController.PlayClip(currentClipName);
                }
            }
        }
    }
}
