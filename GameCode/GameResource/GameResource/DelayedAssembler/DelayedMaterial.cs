﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{

    public class DelayedMaterial
    {
        Material _material;
        public Material material
        {
            get { return _material; }
        }

        List<string> _textures;

        bool _isReleased = false;
        bool _loaded = false;
        public bool loaded
        {
            get { return _loaded; }
        }

        public static Color DEFAULT_COLOR = new Color(0.7f, 0.7f, 0.0f, 0f);
        public string name;
        public DelayedMaterial(string physicalPath)
        {
            if (string.IsNullOrEmpty(physicalPath) == true)
            {
                Debug.LogError("ActiveOjbect physicalPath can not by null or empty!!");
                return;
            }
            if (physicalPath == AssetCreator.DEFAULT_DIFFUSE)
            {
                _material = AssetCreator.GetDefaultDiffuseMaterial();
            }
            else if (physicalPath == AssetCreator.DEFAULT_PARTICLE)
            {
                _material = AssetCreator.GetDefaultParticleMaterial();
            }
            else
            {
                JsonData jsonData = ObjectPool.Instance.GetShadowJsonData(physicalPath);
                _material = new Material(Shader.Find("XGame/Legacy Shaders/Transparent/Diffuse"));
                _material.SetColor("_Color", DEFAULT_COLOR);
                name = physicalPath.Substring(physicalPath.LastIndexOf("$") + 1);
                name = name.Substring(0, name.LastIndexOf("_shadow"));
                _material.name = name;
                _textures = GetTextures(physicalPath);
                ObjectPool.Instance.LoadAssetByPhysicalPath(_textures.ToArray(), delegate () { ResetMaterial(jsonData); });
            }
        }

        void ResetMaterial(JsonData jsonData)
        {
            if (_isReleased) return;
            _loaded = true;
            Shader shader = AssetCreator.GetShader((string)jsonData[AssetCreator.SHADER]);
            _material.shader = shader;
            JsonData propertyList = jsonData[AssetCreator.PROPERTY_LIST];
            for (int i = 0; i < propertyList.Count; i++)
            {
                JsonData property = propertyList[i];
                AssetCreator.ParseMaterialProperty(material, property);
            }
            IOSVestSetting.OnCreateMaterial(material);
        }

        public void Release()
        {
            _isReleased = true;
            _loaded = false;
            Object.DestroyImmediate(_material);
            _material = null;
            _textures.Clear(); ;
        }

        public static List<string> GetTextures(string physicalPath)
        {
            List<string> result = new List<string>();
            var jsonData = ObjectPool.Instance.GetShadowJsonData(physicalPath);
            JsonData propertyList = jsonData[AssetCreator.PROPERTY_LIST];
            for (int i = 0; i < propertyList.Count; i++)
            {
                JsonData property = propertyList[i];
                string type = (string)property[0];
                if (type == AssetCreator.TEX_ENV)
                {
                    result.Add((string)property[2]);
                }
            }
            return result;
        }
    }

}
