﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class DelayedTerrainAssembler : MonoBehaviour
    {
        public string[] tokens;

        bool loaded = false;
        bool hasDestroy = false;

        List<string> _textures;

        void Start()
        {
            if (loaded) return;
            loaded = true;
            Load();
        }

        void Load()
        {
            _textures = GetTextures();
            ObjectPool.Instance.LoadAssetByPhysicalPath(_textures.ToArray(), CreateTerrain);
        }

        List<string> GetTextures()
        {
            List<string> result = new List<string>();
            SplatPrototype[] splatPrototypes = new SplatPrototype[tokens.Length];
            for (int i = 0; i < tokens.Length; i++)
            {
                string token = tokens[i];
                string[] paramList = token.Split(TerrainAssembler.SEMICOLON_SPLIT);
                result.Add(paramList[0]);
                if (paramList.Length == 5 && paramList[4] != "")
                {
                    result.Add(paramList[4]);
                }
            }
            return result;
        }

        void CreateTerrain()
        {
            if (hasDestroy) return;
            if (gameObject == null) return;
            Terrain terrain = gameObject.GetComponent<Terrain>();
            if (terrain != null)
            {
                SplatPrototype[] splatPrototypes = new SplatPrototype[tokens.Length];
                for (int i = 0; i < tokens.Length; i++)
                {
                    string token = tokens[i];
                    string[] paramList = token.Split(TerrainAssembler.SEMICOLON_SPLIT);
                    SplatPrototype splat = new SplatPrototype();
                    splat.normalMap = null;
                    splat.texture = ObjectPool.Instance.GetAssemblyObject(paramList[0]) as Texture2D;
                    splat.tileSize = AssetAssembler.GetVector2(paramList[1]);
                    splat.tileOffset = AssetAssembler.GetVector2(paramList[2]);
                    var p3 = AssetAssembler.GetVector2(paramList[3]);
                    splat.metallic = p3.x;
                    splat.smoothness = p3.y;
                    if (paramList.Length == 5 && paramList[4] != "")
                    {
                        splat.normalMap = ObjectPool.Instance.GetAssemblyObject(paramList[4]) as Texture2D;
                    }
                    splatPrototypes[i] = splat;
                }
                terrain.terrainData.splatPrototypes = splatPrototypes;
                IOSVestSetting.OnCreateTerrain(terrain);
            }
            else
            {
                Debug.LogError("terrain != null");
            }
        }

        void OnDestroy()
        {
            hasDestroy = true;
        }
    }
}
