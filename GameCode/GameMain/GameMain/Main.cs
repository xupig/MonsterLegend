﻿//#define IPHONE_TEST
using System.Collections.Generic;
using System.IO;
//using Artngame.INfiniDy;
using UnityEngine;
using GameLoader.Interfaces;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using GameLoader;
using MogoEngine.Events;
using System.Reflection;
using GameLoader.IO;
using GameResource;
using System;
using GameLoader.LoadingBar;
using GameLoader.PlatformSdk;
using Game.AssetBridge;
using LuaInterface;
using GameData;
using UIExtension;
using GameMain.GlobalManager;
using Common.Utils;
using Common.ClientConfig;
using MogoEngine.RPC;

namespace GameMain
{
    public class Main : IClient
    {
        private int begineTime;
        private const string LIBS = "libs/";
        private const string DATA_PBUFS = "data/pbufs/";
        private const string XML_SERIALIZED_PBUF = "XMLSerialized.pbuf";

        public bool isUpdateProgress = false;   //是否更改进度[true:更改进度,false:不更改进度]

        private List<string> XmlDllList = new List<string>
        {
            //"XMLDefine.dll",
            //"XMLSerializer.dll",
            //"XMLManager.dll"
        };

        private List<string> ToLuaDllList = new List<string>
        {
            "ToLua.dll",
            "ToLuaWrap.dll",
        };
        public static Dictionary<string, Assembly> assemblies = new Dictionary<string, Assembly>();
        static Main _instance;
        public static Main Instance
        {
            get { return _instance; }
        }

        public Main()
        {
            _instance = this;
            //ProgressBar.Instance.UpdateProgress(0.6f);  //GameLogic.dll载入完成60%进度
            Init();
            Start();
        }

        public void Init()
        {
            LoggerHelper.Debug("Main Init");
            AssetBridge.Parse = AssetAssembler.Parse;
            AssetBridgeEx.Parse = AssetAssembler.Parse;
            begineTime = Environment.TickCount;
            InitXmlData();
            LoggerHelper.Info(string.Format("InitXmlData cost:{0}ms", Environment.TickCount - begineTime));
            InitLocalSetting();
            begineTime = Environment.TickCount;
            InitACTSystem();
            InitUISystem();
            InitRTSystem();
            InitEntityType();
            //InitGrassSystem();
            InitAudioSystem();
            VestManager.GetInstance();
            ResourceMappingManager.Instance.Init();
            LocalCacheHelper.InitCommonLocalCache();
            ObjectPool.Instance.GetResMapping = ResourceMappingManager.Instance.GetResMapping;
            ObjectPool.Instance.GetResMappings = ResourceMappingManager.Instance.GetResMappings;
            InitConfig();
            LoggerHelper.Info(string.Format("InitACTSystem cost:{0}ms", Environment.TickCount - begineTime));
            //ObjectPool.Instance.PreloadAsset(new string[] { "Shader/UGUI/UGUI_DEFAULT.shader" }, null);
        }

        private void InitConfig()
        {
            RPCMsgLogManager.IsRecord = SystemConfig.IsRecordRPCMsg;
            if (SystemConfig.GetValueInCfg("UseFadeFloor") == "1")
            {
                DynamicScenes.UseFadeFloor = true;
                DynamicScenes.AddTweenColorScript = AddTweenColorScript;
                DynamicScenes.GetFadeFloorMat();
            }
            DelayedConfig.InitConfig();
            if (SystemConfig.GetValueInCfg("CanDelayLoading") != "")
            {
                DelayedConfig.CanDelayLoading = SystemConfig.GetValueInCfg("CanDelayLoading") != "0";
            }
            SortOrderedRenderAgent.refreshByPanels = SystemConfig.GetValueInCfg("RefreshByPanels") != "0";
            var loadingDelay = SystemConfig.GetValueInCfg("HideLoadingDelay");
            float delay = 0;
            if (float.TryParse(loadingDelay, out delay))
            {
                ProgressBar.Instance.HideDelay = delay;
            }
            var mhdt = SystemConfig.GetValueInCfg("MaxHandleDataTime");
            float time = 0;
            if (float.TryParse(mhdt, out time))
            {
                LoggerHelper.Info("MaxHandleDataTime: " + time);
                ServerProxy.Instance.SetHandlePerPackageSpaceTime(time);
            }
            InitUploadLogWhiteList();
            if (SystemConfig.GetValueInCfg("FixProgressIds") != "")
            {
                var fixIds = SystemConfig.GetValueInCfg("FixProgressIds").ParseMapIntString(mapSpriter:';');
                ProgressBar.Instance.SetFixProgressIds(fixIds);
                //DelayedConfig.CanDelayLoading = SystemConfig.GetValueInCfg("FixProgressIds") != "0";
            }
            if (SystemConfig.GetValueInCfg("OutterProgressIds") != "")
            {
                var fixIds = SystemConfig.GetValueInCfg("OutterProgressIds").ParseMapIntString(mapSpriter: ';');
                ProgressBar.Instance.SetOutterProgressIds(fixIds);
                //DelayedConfig.CanDelayLoading = SystemConfig.GetValueInCfg("FixProgressIds") != "0";
            }

        }

        private Color FadeInBeginValue = new Color(90f / 255, 50f / 255, 0, 1);
        private Color FadeInEndValue = new Color(90f / 255, 50f / 255, 0, 0.7f);
        private Color FadeOutBeginValue = new Color(90f / 255, 50f / 255, 0, 0.3f);
        private Color FadeOutEndValue = new Color(90f / 255, 50f / 255, 0, 0);
        private string TargetProp = "_TintColor";

        private void AddTweenColorScript(GameObject prefab)
        {
            var tmc = prefab.AddComponent<TweenMaterialColor>();
            tmc.TargetProp = TargetProp;
            tmc.Duration = 5;
            tmc.FadeInBeginValue = FadeInBeginValue;
            tmc.FadeInEndValue = FadeInEndValue;
            tmc.FadeOutBeginValue = FadeOutBeginValue;
            tmc.FadeOutEndValue = FadeOutEndValue;
        }

        private void InitUploadLogWhiteList()
        {
            try
            {
                var whiteList = SystemConfig.GetValueInCfg("UploadLogWhiteList");
                if (!string.IsNullOrEmpty(whiteList))
                {
                    LogUploader.GetInstance().WhiteList = new List<string>(whiteList.Split('|'));
                    //LoggerHelper.Info("UploadLogWhiteList: " + LogUploader.GetInstance().WhiteList.PackList());
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        private void InitEntityType()
        {
            EntityMgr.Instance.RegisterDefaultEntityType(typeof(GameMain.EntityLuaBase));
        }

        private void OnXMLDataInited()
        {
            XMLManager.dataLoader = new XMLDataRuntimeLoader();
            CheckReloadXML();
            spell_state_helper.GetSpellStateData(0);
            var buff = XMLManager.buff;
            var spell = XMLManager.spell;
            var spell_action = XMLManager.spell_action;
            //EventDispatcher.TriggerEvent(LoginEvents.ON_XMLDATA_INITED);
        }

        private void InitXmlData()
        {
            //ProgressBar.Instance.UpdateTip("正在初始化XmlData");
            LoggerHelper.Info("[Main] 正在初始化XmlData");
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                    LoggerHelper.Info("[Android发布模式][Main] 加载代码库[Start]");
                    LoadLibsFromFileSystem();
                    //SeriPbuf(XML_SERIALIZED_PBUF);
                    break;
                case RuntimePlatform.IPhonePlayer:
                    break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
#if !UNITY_WEBPLAYER && !UNITY_IPHONE
                        LoggerHelper.Info("[开发模式下--发布模式][Main] 加载代码库[Start]");
                        LoadLibsFromFileSystem();
                        //SeriPbuf(XML_SERIALIZED_PBUF);
#endif
                    }
                    else
                    {
                        LoggerHelper.Info(string.Format("[{0}][Main] 加载代码库[Start]", SystemConfig.isIphoneTest ? "IphoneTest模式" : "开发模式"));
                        LoadLibsFromLocal();
                    }
                    break;
            }

            OnXMLDataInited();
            //ProgressBar.Instance.UpdateProgress(0.65f);
        }

        //发布模式--从pkg文件载入dll
        private void LoadLibsFromFileSystem()
        {
            ////LoggerHelper.Error("LoadLibsFromFileSystem 11111111111");
            //for (int i = 0; i < XmlDllList.Count; i++)
            //{ //载入.dll
            //    //LoggerHelper.Error("LoadLibsFromFileSystem 22222:" + XmlDllList[i]);
            //    assemblies[XmlDllList[i]] = LoadDllFromFileSystem(XmlDllList[i]);
            //    //LoggerHelper.Error("LoadLibsFromFileSystem 3333333");
            //}
            for (int i = 0; i < ToLuaDllList.Count; i++)
            { //载入.dll
                assemblies[ToLuaDllList[i]] = LoadDllFromFileSystem(ToLuaDllList[i]);
            }
        }

        /// <summary>
        /// 载入指定代码库
        /// </summary>
        /// <param name="fileName">指定文件代码库文件名,例如：MogoEngine.dll</param>
        private Assembly LoadDllFromFileSystem(string fileName)
        {
            fileName = string.Concat(LIBS, fileName);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            if (isExistFile)
            {
                //LoggerHelper.Error("LoadDllFromFileSystem 111111");
                byte[] enData = FileAccessManager.LoadBytes(fileName);
                //LoggerHelper.Error("LoadDllFromFileSystem 222222");
                byte[] deData = DESCrypto.Decrypt(enData, KeyUtils.GetResNumber());
                //LoggerHelper.Error("LoadDllFromFileSystem 333333");
                deData = PkgFileUtils.UnpackMemory(deData);
                //LoggerHelper.Error("LoadDllFromFileSystem 1111111:" + fileName+" length:" + deData.Length);
                Assembly ass = Assembly.Load(deData);
                //LoggerHelper.Error("LoadDllFromFileSystem 22222:"+fileName+"is null=="+(ass==null).ToString());
                return ass;
            }
            else
            {
                LoggerHelper.Error(string.Format("代码库文件:{0},{1}", fileName, isExistFile ? "存在" : "找不到"));
                return null;
            }
        }

        /// <summary>
        /// 反序列化pbuf还原为数据
        /// </summary>
        /// <param name="fileName">文件名,例如：ACTData.pbuf</param>
        private byte[] SeriPbuf(string fileName)
        {
            string folder = string.Concat(DATA_PBUFS, fileName);
            bool isExistFile = FileAccessManager.IsFileExist(folder);
            if (!isExistFile) LoggerHelper.Debug(string.Format("pbuf文件:{0},{1}", folder, isExistFile ? "存在" : "找不到"));
            byte[] bytes = FileAccessManager.LoadBytes(folder);
            bytes = PkgFileUtils.UnpackMemory(bytes);
            if (fileName == XML_SERIALIZED_PBUF)
            {//反序列化XMLSerialized.pbuf
                //XMLManager.InitFromStream(new MemoryStream(bytes));
            }
            return bytes;
        }

        //开发模式--载入dll
        private void LoadLibsFromLocal()
        {
#if !UNITY_WEBPLAYER
            string baseFolder = string.Concat(SystemConfig.DllPath, "GeneratedLib/");
            //for (int i = 0; i < XmlDllList.Count; i++)
            //{ //载入GeneratedLibs目录下的dll
            //    assemblies[XmlDllList[i]] = Assembly.Load(File.ReadAllBytes(string.Concat(baseFolder, XmlDllList[i])));
            //}
            for (int i = 0; i < ToLuaDllList.Count; i++)
            { //载入GeneratedLibs目录下的dll
                assemblies[ToLuaDllList[i]] = Assembly.Load(FileUtils.LoadByteFile(string.Concat(SystemConfig.DllPath, ToLuaDllList[i])));
            }
#endif
        }


        private void CheckReloadXML()
        {
            ReloadXmlManager.GetInstance().ReloadFromLocal();
        }

        void InitLocalSetting()
        {
            //LocalSetting.Init();
            int terrain = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Terrain); 
            int Pet = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Pet);
            int Avatar = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Avatar);
            int Dummy = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Dummy);
            int Monster = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Monster);
            int Actor = ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Actor);
            Physics.IgnoreLayerCollision(terrain, Pet);
            Physics.IgnoreLayerCollision(terrain, Avatar);
            Physics.IgnoreLayerCollision(terrain, Dummy);
            Physics.IgnoreLayerCollision(terrain, Pet);
            Physics.IgnoreLayerCollision(terrain, Monster);
            Physics.IgnoreLayerCollision(Actor, terrain);
        }

        void InitUISystem()
        {
            ImageWrapper.GetMaterial = ObjectPool.Instance.GetAssemblyObject;
            ImageWrapper.GetSprite = ObjectPool.Instance.GetAssemblyObject;
            TextWrapper.GetFont = ObjectPool.Instance.GetAssemblyObject;
            TextWrapper.GetLanguage = Common.Utils.GameLanguageUtil.GetContent;
            TextMeshWrapper.GetFont = ObjectPool.Instance.GetAssemblyObject;
            TextMeshWrapper.GetLanguage = Common.Utils.GameLanguageUtil.GetContent;
            ButtonWrapper.ClickSound = (id) =>
            {
                //clickid填0不播放
                if (id > 0)
                {
                    SoundManager.GetInstance().PlaySound(id);
                }
            };
            ToggleWrapper.ClickSound = (id) =>
            {
                //clickid填0不播放
                if (id > 0)
                {
                    SoundManager.GetInstance().PlaySound(id);
                }
            };
        }

        void InitAudioSystem()
        {
            AudioManager.GetInstance().SetRootGameObject(LoaderDriver.Instance.gameObject);
        }

        private void InitACTSystem()
        {
            ACTSystemAdapter.InitACTSystem();
            //ProgressBar.Instance.UpdateProgress(0.7f);
        }

        void InitRTSystem()
        {
            CameraRTManager.GetInstance().Init();
            //GameObject go = new GameObject("DL");
            //GameObject.DontDestroyOnLoad(go);
            //Light light = go.AddComponent<Light>();
            //light.type = LightType.Directional;
            //light.color = new Color(138f / 255f, 147f / 255f, 165f / 255f);
            //light.transform.Rotate(new Vector3(1, 0, 0), 45);
        }

        void EnterLoginSceneCallBack()
        {
            //EventDispatcher.RemoveEventListener(LoginEvents.ON_ENTER_LOGIN_SCENE, EnterLoginSceneCallBack);
        }

        public void Start()
        {
            PreloadNeededResources(() =>
            {
                VideoManager.Instance.Init(() =>
                {
                    GameAtlasUtils.Init();
                    LoaderDriver.Instance.gameObject.AddComponent<LuaDriver>();
                    LoaderDriver.Instance.gameObject.AddComponent<ScreenUtil>();
                    ProjectionManager.GetInstance();
                    ResourceMappingManager.Instance.LoadUI();
                    ShaderRuntimeLoader.Warmup();
                    ProgressBar.Instance.UpdateTip(DefaultLanguage.GetString("load_login"));
                });
                //GameAtlasUtils.Init();
                //LoaderDriver.Instance.gameObject.AddComponent<LuaDriver>();
                //LoaderDriver.Instance.gameObject.AddComponent<ScreenUtil>();
            });

            OnGetLogo();
        }

        public void OnGetLogo()
        {
            string logoPath = String.Concat(Application.persistentDataPath, ConstString.LogoPath);
            string url = SystemConfig.GetValueInCfg("LogoUrl");
            if (string.IsNullOrEmpty(url))
            {
                File.Delete(logoPath);
                return;
            }
            if (File.Exists(logoPath))
            {
                string localLogoMD5 = MD5Utils.BuildFileMd5(logoPath);
                string logoMD5 = SystemConfig.GetValueInCfg("LogoMd5");
                if (localLogoMD5 != logoMD5)
                {
                    HttpWrappr.instance.LoadWwwFile(url, logoPath, null, OnLogoLoaded, null);
                }
            }
            else
            {
                HttpWrappr.instance.LoadWwwFile(url, logoPath, null, OnLogoLoaded, null);
            }
        }

        private void OnLogoLoaded(string url)
        {
            EventDispatcher.TriggerEvent("OnLogoLoaded");
        }

        public void PreloadNeededResources(Action callback)
        {
            List<string> paths = new List<string>();
            paths.Add("Shader/UGUI/UGUI_IMAGEGREY.shader");
            paths.Add(CameraManager.MAIN_CAMERA_PREFAB);
            paths.Add(GameAtlasUtils.ATLAS_JSON_PATH);
            paths.Add(ProjectionManager.PROJECTION_PATH);
            paths.Add(WaitingLoadResourceUIManager.WaitingLoadResourceUIPath);
            paths.Add(Utils.PathUtils.UIFlowLightPath);
            ObjectPool.Instance.DontDestroy(paths.ToArray());
            //no ResourceMappingManager
            ObjectPool.Instance.PreloadAsset(paths.ToArray(), callback);
        }

        public void TestLuaCall()
        {

            float t = UnityPropUtils.realtimeSinceStartup;
            Action a = LuaFacade.TestLuaCall;
            for (int i = 0; i < 100000; i++)
            {
                a();
            }
            //Debug.LogError("cs call cs:" + (UnityPropUtils.realtimeSinceStartup - t));
        }

        public void CheckResources()
        {
            var tt = Resources.Load<Texture2D>("test/Lightmap-0_comp_light");



            var lightmaps = new LightmapData();
            lightmaps.lightmapFar = tt;
            lightmaps.lightmapNear = tt;
            var lightmapArray = new LightmapData[1];
            lightmapArray[0] = lightmaps;

            LightmapSettings.lightmaps = lightmapArray;

            return;/*
            ObjectPool.Instance.GetObjects(new string[] { "Scenes/Demo02/Demo02/Lightmap-0_comp_light.exr" }, 
                (objects) => {
                    if (objects[0] != null)
                    {
                        var t = objects[0] as Texture2D;
                        t.filterMode = FilterMode.Bilinear;
                        
                        //byte[] byt = t.();
                        //File.WriteAllBytes("E:/ttt.exr", byt);
                        var lightmaps = new LightmapData();
                        lightmaps.lightmapFar = t;
                        lightmaps.lightmapNear = t;
                        var lightmapArray = new LightmapData[1];
                        lightmapArray[0] = lightmaps;
                        LightmapSettings.lightmaps = lightmapArray;
                    }
                    
            });*/
        }

    }

}