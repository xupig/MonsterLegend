﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using LuaInterface;
using UnityEngine;
using UnityEngine.UI;
namespace GameMain
{
    public class LuaTween
    {
        public static LuaTween Instance = new LuaTween();
        private static Tweener tweener;
        #region ExtensionBase
        #region Event

        public static LuaTween OnComplete(LuaFunction luafunc)
        {
            tweener.OnComplete(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnKill(LuaFunction luafunc)
        {
            tweener.OnKill(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnPlay(LuaFunction luafunc)
        {
            tweener.OnPlay(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnPause(LuaFunction luafunc)
        {
            tweener.OnPause(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnRewind(LuaFunction luafunc)
        {
            tweener.OnRewind(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnStart(LuaFunction luafunc)
        {
            tweener.OnStart(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween OnStepComplete(LuaFunction luafunc)
        {
            tweener.OnStepComplete(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public LuaTween OnUpdate(LuaFunction luafunc)
        {
            tweener.OnUpdate(() =>
            {
                LuaFacade.CallLuaFunction(luafunc);
            });
            return Instance;
        }

        public static LuaTween SetLoops(int loops)
        {
            tweener.SetLoops(loops, LoopType.Yoyo);
            return Instance;
        }

        #endregion
        #region Audio

        public static LuaTween AudioSource_DoFade(GameObject go, float endValue, float duration)
        {
            AudioSource audioSource = go.GetComponent<AudioSource>();
            tweener = audioSource.DOFade(endValue, duration);
            return Instance;
        }

        public static LuaTween AudioSource_DoPitch(GameObject go, float endValue, float duration)
        {
            AudioSource audioSource = go.GetComponent<AudioSource>();
            audioSource.DOPitch(endValue, duration);
            return Instance;
        }
        #endregion
        #region Transform

        public static LuaTween Transform_DoMove(GameObject go, Vector3 endVector3, float duration, bool snapping = false)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOMove(endVector3, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoMoveX(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOMoveX(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoMoveY(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformy = go.GetComponent<Transform>();
            tweener = transformy.DOMoveY(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoMoveZ(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformz = go.GetComponent<Transform>();
            tweener = transformz.DOMoveZ(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoLocalMove(GameObject go, Vector3 endValue, float duration)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOLocalMove(endValue, duration);
            return Instance;
        }
        //Ease不导出了，用数字代替 
        public static LuaTween Transform_DoLocalMoveMode(GameObject go, Vector3 endValue, float duration, int mode = 0)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOLocalMove(endValue, duration);
            tweener.SetEase((Ease)mode);
            return Instance;
        }
        public static LuaTween Transform_DoLocalMoveX(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOLocalMoveX(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoLocalMoveY(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformy = go.GetComponent<Transform>();
            tweener = transformy.DOLocalMoveY(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoLocalMoveZ(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Transform transformz = go.GetComponent<Transform>();
            tweener = transformz.DOLocalMoveZ(endValue, duration, snapping);
            return Instance;
        }
        //RotateMode不导出了，用数字代替 
        public static LuaTween Transform_DoRotate(GameObject go, Vector3 endValue, float duration, int mode = 0)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DORotate(endValue, duration, (RotateMode)mode);
            return Instance;
        }

        public static LuaTween Transform_DoRotateQuaternion(GameObject go, Quaternion endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DORotateQuaternion(endValue, duration);
            return Instance;
        }

        public static LuaTween Transform_DoLocalRotate(GameObject go, Vector3 endValue, float duration, int mode = 0)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOLocalRotate(endValue, duration, (RotateMode)mode);
            return Instance;
        }

        public static LuaTween Transform_DoLocalRotateQuaternion(GameObject go, Quaternion endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOLocalRotateQuaternion(endValue, duration);
            return Instance;
        }

        public static LuaTween Transform_DoScale(GameObject go, Vector3 endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOScale(endValue, duration);
            return Instance;
        }

        public static LuaTween Transform_DoScale(GameObject go, float endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOScale(endValue, duration);
            return Instance;
        }
        public static LuaTween Transform_DoScaleX(GameObject go, float endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOScaleX(endValue, duration);
            return Instance;
        }
        public static LuaTween Transform_DoScaleY(GameObject go, float endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOScaleY(endValue, duration);
            return Instance;
        }
        public static LuaTween Transform_DoScaleZ(GameObject go, float endValue, float duration)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOScaleZ(endValue, duration);
            return Instance;
        }

        public static LuaTween Transform_DoLookAt(GameObject go, Vector3 towards, float duration,
            int axisConstraint = 0, Vector3? up = null)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOLookAt(towards, duration, (AxisConstraint)axisConstraint, up);
            return Instance;
        }

        public static LuaTween Transform_DoPunchPostion(GameObject go, Vector3 punch, float duration, int vibrato = 10, float elasticity = 1,
            bool snapping = false)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOPunchPosition(punch, duration, vibrato, elasticity, snapping);
            return Instance;
        }

        public static LuaTween Transform_DoPunchScale(GameObject go, Vector3 punch, float duration, int vibrato = 10, float elasticity = 1)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOPunchScale(punch, duration, vibrato, elasticity);
            return Instance;
        }

        public static LuaTween Transform_DoPunchRotation(GameObject go, Vector3 punch, float duration, int vibrato = 10, float elasticity = 1)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOPunchRotation(punch, duration, vibrato, elasticity);
            return Instance;
        }

        public static LuaTween Transform_DoShakePosition(GameObject go, float duration, float strength, int vibrato = 10,
            float randomness = 90, bool snapping = false, bool fadeout = true)
        {
            Transform transformR = go.GetComponent<Transform>();
            tweener = transformR.DOShakePosition(duration, strength, vibrato, randomness, snapping, fadeout);
            return Instance;
        }

        public static LuaTween Transform_DoShakePosition(GameObject go, float duration, Vector3 strength, int vibrato = 10,
            float randomness = 90, bool snapping = false, bool fadeOut = true)
        {   
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOShakePosition(duration, strength, vibrato, randomness, snapping, fadeOut);
            return Instance;
        }

        public static LuaTween Transform_DoShakeRotation(GameObject go, float duration, float strength = 90,
            int vibrato = 10, float randomness = 90, bool fadeout = true)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOShakeRotation(duration, strength, vibrato, randomness, fadeout);
            return Instance;
        }
        public static LuaTween Transform_DoShakeRotation(GameObject go, float duration, Vector3 strength,
            int vibrato = 10, float randomness = 90, bool fadeout = true)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOShakeRotation(duration, strength, vibrato, randomness, fadeout);
            return Instance;
        }

        public static LuaTween Transform_DoShakeScale(GameObject go, float duration, float strength = 1,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOShakeScale(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }
        public static LuaTween Transform_DoShakeScale(GameObject go, float duration, Vector3 strength ,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOShakeScale(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }

        public static LuaTween Transform_DoJump(GameObject go, Vector3 endValue, float jumpPower, int numJumps,
            float duration, bool snapping = false)
        {
            Transform transformx = go.GetComponent<Transform>();
            transformx.DOJump(endValue, jumpPower, numJumps, duration, snapping);
            return Instance;
        }
        public static LuaTween Transform_DoLocalJump(GameObject go, Vector3 endValue, float jumpPower, int numJumps,
            float duration, bool snapping = false)
        {
            Transform transformx = go.GetComponent<Transform>();
            transformx.DOLocalJump(endValue, jumpPower, numJumps, duration, snapping);
            return Instance;
        }

        public static LuaTween Transform_DoPath(GameObject go, Vector3[] path, float duration, int pathType, int pathMode,
            int resolution = 10, Color? gizmoColor = null)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOPath(path, duration, (PathType)pathType, (PathMode)pathMode, resolution, gizmoColor);
            return Instance;
        }
        public static LuaTween Transform_DoLocalPath(GameObject go, Vector3[] path, float duration, int pathType, int pathMode,
            int resolution = 10, Color? gizmoColor = null)
        {
            Transform transformx = go.GetComponent<Transform>();
            tweener = transformx.DOLocalPath(path, duration, (PathType)pathType, (PathMode)pathMode, resolution, gizmoColor);
            return Instance;
        }
        #endregion
        #region Camera

        public static LuaTween Camera_DoAspect(GameObject go, float endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOAspect(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoColor(GameObject go, Color endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOColor(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoFarClipPlane(GameObject go, float endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOFarClipPlane(endValue, duration);
            return Instance;
        }
        public static LuaTween Camera_DoFieldOfView(GameObject go, float endvalue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOFieldOfView(endvalue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoOrthoSize(GameObject go, float endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOOrthoSize(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoNearClipPlane(GameObject go, float endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DONearClipPlane(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoPixelRect(GameObject go, Rect endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOPixelRect(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoRect(GameObject go, Rect endValue, float duration)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DORect(endValue, duration);
            return Instance;
        }

        public static LuaTween Camera_DoShakePosition(GameObject go, float duration, float strength = 3,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOShakePosition(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }
        public static LuaTween Camera_DoShakePosition(GameObject go, float duration, Vector3 strength ,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOShakePosition(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }
        public static LuaTween Camera_DoShakeRotation(GameObject go, float duration, float strength = 90,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOShakeRotation(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }
        public static LuaTween Camera_DoShakeRotation(GameObject go, float duration, Vector3 strength,
            int vibrato = 10, float randomness = 90, bool fadeOut = true)
        {
            Camera ca = go.GetComponent<Camera>();
            ca.DOShakeRotation(duration, strength, vibrato, randomness, fadeOut);
            return Instance;
        }
#endregion
        #region Light 

        public static LuaTween Light_DoColor(GameObject go,Color endValue, float duration)
        {
            Light l = go.GetComponent<Light>();
            l.DOColor(endValue, duration);
            return Instance;
        }

        public static LuaTween Light_DoIntensity(GameObject go, float endValue, float duration)
        {
            Light l = go.GetComponent<Light>();
            l.DOIntensity(endValue, duration);
            return Instance;
        }

        public static LuaTween Light_DoShadowStrenght(GameObject go, float endValue, float duration)
        {
            Light l = go.GetComponent<Light>();
            l.DOShadowStrength(endValue, duration);
            return Instance;
        }
        #endregion
        #region LineRenderer

        //public static LuaTween LineRenderer_DoColor(GameObject go, Color2 startValue, Color2 endValue, float duration)
        //{
        //    LineRenderer lr = go.GetComponent<LineRenderer>();
        //    lr.DOColor(startValue, endValue, duration);
        //    return Instance;
        //}
        #endregion
        #region Material

        public static LuaTween Material_DoColor(GameObject go, Color endValue, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOColor(endValue, duration);
            return Instance;
        }

        public static LuaTween Material_DoColor(GameObject go, Color endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOColor(endValue, properties, duration);
            return Instance;
        }

        public static LuaTween Material_DoFade(GameObject go, float endValue, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOFade(endValue, duration);
            return Instance;
        }

        public static LuaTween Material_DoFade(GameObject go, float endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOFade(endValue, properties, duration);
            return Instance;
        }

        public static LuaTween Material_DoFloat(GameObject go, float endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOFloat(endValue, properties, duration);
            return Instance;
        }

        public static LuaTween Material_DoOffset(GameObject go, Vector2 endValue, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOOffset(endValue, duration);
            return Instance;
        }

        public static LuaTween Material_DoOffset(GameObject go, Vector2 endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOOffset(endValue, properties, duration);
            return Instance;
        }

        public static LuaTween Material_DOTiling(GameObject go, Vector2 endValue, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOTiling(endValue, duration);
            return Instance;
        }

        public static LuaTween Material_DoTiling(GameObject go, Vector2 endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOTiling(endValue, properties, duration);
            return Instance;
        }

        public static LuaTween Material_DoVector(GameObject go, Vector4 endValue, string properties, float duration)
        {
            Material m = go.GetComponent<Material>();
            m.DOVector(endValue, properties, duration);
            return Instance;
        }
        #endregion
        #region Rigibody

        public static LuaTween Rigidbody_DoMove(GameObject go, Vector3 endValue, float duration, bool snapping = false)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOMove(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Rigidbody_DoMoveX(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOMoveX(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Rigidbody_DoMoveY(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOMoveY(endValue, duration, snapping);
            return Instance;
        }
        public static LuaTween Rigidbody_DoMoveZ(GameObject go, float endValue, float duration, bool snapping = false)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOMoveZ(endValue, duration, snapping);
            return Instance;
        }

        public static LuaTween Rigidbody_DoRotate(GameObject go, Vector3 endValue, float duration)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DORotate(endValue, duration);
            return Instance;
        }

        public static LuaTween Rigidbody_DoLookAt(GameObject go, Vector3 towards, float duration,int axisConstraint=0,Vector3?up=null)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOLookAt(towards, duration, (AxisConstraint)axisConstraint, up);
            return Instance;
        }

        public static LuaTween Rigidbody_DoJump(GameObject go, Vector3 endValue, float jumpPower, int numJumps,
            float duration, bool snapping)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.DOJump(endValue, jumpPower, numJumps, duration, snapping);
            return Instance;
        }
        #endregion
        #region TrailRenderer

        public static LuaTween TrailRenderer_DoResize(GameObject go, float toStartWidth, float toEndWidth,
            float duration)
        {
            TrailRenderer tr = go.GetComponent<TrailRenderer>();
            tr.DOResize(toStartWidth, toEndWidth, duration);
            return Instance;
        }

        public static LuaTween TrailRenderer_DoTime(GameObject go, float endValue, float duration)
        {
            TrailRenderer tr = go.GetComponent<TrailRenderer>();
            tr.DOTime(endValue, duration);
            return Instance;
        }
        #endregion
        #region Blendables

        #endregion
        #endregion
        #region Extension46

        #region UI
        #region CanvasGroup
        public static LuaTween CavasGroup_DoFade(GameObject go, float endValue, float duration)
        {
            CanvasGroup canvasGroup = go.GetComponent<CanvasGroup>();
            canvasGroup.DOFade(endValue, duration);
            return Instance;
        }
        #endregion
        #region Graphic
        public static LuaTween Graphic_DoColor(GameObject go, Color endValue, float duration)
        {
            Graphic graphics = go.GetComponent<Graphic>();
            graphics.DOColor(endValue, duration);
            return Instance;
        }

        public static LuaTween Graphic_DoFade(GameObject go, float endValue, float duration)
        {
            Graphic graphics = go.GetComponent<Graphic>();
            graphics.DOFade(endValue, duration);
            return Instance;
        }
        #endregion
        #region Image

        public static LuaTween Image_DoColor(GameObject go, Color endValue, float duration)
        {
            Image image = go.GetComponent<Image>();
            image.DOColor(endValue, duration);
            return Instance;
        }

        public static LuaTween Image_DoFade(GameObject go, float endValue, float duration)
        {
            Image image = go.GetComponent<Image>();
            image.DOFade(endValue, duration);
            return Instance;
        }
        #endregion
        #endregion
        #endregion

    }

}
