﻿using GameMain.GlobalManager;
using UnityEngine;

namespace GameMain
{
    public class PosBubbleDialog : XContainer
    {
        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1200, 0);
        private Vector3 _pos;
        private bool _isVisible;
        protected float _addtiveHeight;

        private RectTransform _rectTransform;
        //private RectTransform _imgRect;
        //private TextMeshWrapper _textDistance;
        /// <summary>
        /// 高度偏移常量
        /// </summary>
        //private const float BOTTOM_PADDING = 0;
        /// <summary>
        /// 记录屏幕四个顶点坐标位置
        /// </summary>
        //private Vector2[] _rectPoint = new Vector2[5];
        //private Vector2 _centerPosition = new Vector2(ScreenUtil.ScreenWidth / 2, ScreenUtil.ScreenHeight / 2);
        /// <summary>
        /// 实际屏幕宽度减去图标宽度后的宽度
        /// </summary>
        private float _screenWidth;
        /// <summary>
        /// 实际屏幕高度（没有减去图标高度）
        /// </summary>
        private float _screenHeight;
        /// <summary>
        /// WorldToScreenPoint后的坐标
        /// </summary>
        private Vector3 _screenPosition;
        /// <summary>
        /// WorldToViewportPoint后的坐标
        /// </summary>
        private Vector3 _viewPosition;
        /// <summary>
        /// 主摄像机缓存
        /// </summary>
        private Camera _mainCamera;
        /// <summary>
        /// 图标宽度中心点（宽度的一半）
        /// </summary>
        //private float _imgRectCenterX;
        //private float _taskTrackingRadius = 30;
        //private float m_updateCounter = 100;
        //private bool _hiddenByDistance;

        protected override void Awake()
        {
            base.Awake();
            var container = this.gameObject.GetComponent<XContainer>();
            _rectTransform = container.GetComponent<RectTransform>();
            //_imgRect = container.GetChildComponent<RectTransform>("Image_Pointing");
            //_imgRect.pivot = new Vector2(0.5f, 0f);
            //_textDistance = container.GetChildComponent<TextMeshWrapper>("Container_Content/Text_Content");

            //图标大小需要处理屏幕缩放比例
            //_imgRectCenterX = (float)(_imgRect.sizeDelta.x * 0.5 * ScreenUtil.screenScale);
            _screenWidth = ScreenUtil.ScreenWidth;
            _screenHeight = ScreenUtil.ScreenHeight;

            //_rectPoint[0] = new Vector2(_imgRectCenterX, BOTTOM_PADDING);//左上
            //_rectPoint[1] = new Vector2(_screenWidth + _imgRectCenterX, BOTTOM_PADDING);//右上
            //_rectPoint[2] = new Vector2(_screenWidth + _imgRectCenterX, _screenHeight);//右下
            //_rectPoint[3] = new Vector2(_imgRectCenterX, _screenHeight);//左下
            //_rectPoint[4] = new Vector2(_imgRectCenterX, BOTTOM_PADDING);//左上
            _mainCamera = CameraManager.GetInstance().Camera;
            //var ttr = global_params_helper.GetGlobalParam(33);
            //float.TryParse(ttr, out _taskTrackingRadius);
        }

        public virtual void Show(Vector3 pos)
        {
            _pos = pos;
            _isVisible = true;
        }

        public virtual void Hide()
        {
            _isVisible = false;
            _pos = HIDE_POSITION;
            _rectTransform.localPosition = HIDE_POSITION;
        }

        void Update()
        {
            if (!_isVisible) return;
            if (_mainCamera == null) return;
            UpdatePos(_pos);
        }

        //private void CheckDistance(Vector3 pos)
        //{
        //    m_updateCounter += UnityPropUtils.deltaTime;
        //    if (m_updateCounter > 1)//1秒检查一次，提高性能
        //    {
        //        m_updateCounter = 0;
        //        if (MogoWorld.Player != null)
        //        {
        //            var distance = Vector3.Distance(MogoWorld.Player.position, pos);
        //            if (distance > _taskTrackingRadius)
        //            {
        //                _hiddenByDistance = true;
        //                if (_rectTransform.localPosition != HIDE_POSITION)
        //                    _rectTransform.localPosition = HIDE_POSITION;
        //            }
        //            else
        //            {
        //                _textDistance.text = string.Format("{0:0}M", distance - _addtiveHeight);//这里算距离不是很准确，但是如果其他人没有发现有问题，就这样简单处理吧
        //                _hiddenByDistance = false;
        //            }
        //        }
        //    }
        //}


        protected void UpdatePos(Vector3 position)
        {
            Vector2 screenPosition = _mainCamera.WorldToScreenPoint(position);
            Vector3 viewPosition = _mainCamera.WorldToViewportPoint(position);
            if (Common.Utils.GameObjectHelper.CheckPositionIsInScreen(screenPosition) && viewPosition.z > 0 && viewPosition.z < 30)
            {
                CalculatePosition(screenPosition);
            }
            else
            {
                _rectTransform.localPosition = HIDE_POSITION;
            }
        }

        private void CalculatePosition(Vector3 screenPosition)
        {
            Vector3 localPosition = screenPosition / ScreenUtil.screenScale;
            _rectTransform.localPosition = localPosition;
        }

    }
}