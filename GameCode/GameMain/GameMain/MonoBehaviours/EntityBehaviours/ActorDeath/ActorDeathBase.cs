﻿using ACTSystem;
using Common.ClientConfig;
using GameMain;
using GameMain.GlobalManager;
using GameMain.ClientConfig;
using UnityEngine;
using GameData;

namespace GameMain
{

    public abstract class ActorDeathBase
    {
        public const int DISAPPEAR_TYPE_NONE = -1;
        public const int DISAPPEAR_TYPE_SINK = 0;
        public const int DISAPPEAR_TYPE_DIRECTLY = 1;

        protected ACTActor _actor;
        protected DeathState _deathState;

        protected float _waitingSinkDuration = 0;
        protected float _waitingDestroyDuration = 0;

        private bool _isWaitingSink = false;
        private bool _isWaitingDestroy = false;
        private float _startWaitingSinkTimeStamp = 0;
        private float _startWaitingDestroyTimeStamp = 0;

        protected int _disappearType = DISAPPEAR_TYPE_NONE;
        protected float _disappearTime = 0;
        public float _sinkTime = 0f;
        protected float _destroyTime = 3f;
        public bool canRelease = true;

        virtual public void Start(ACTActor actor, float disappearTime, int disappearType)
        {
            _actor = actor;
            _disappearTime = disappearTime;
            _disappearType = disappearType;
            _deathState = DeathState.BEGIN;
        }

        virtual public void OnDestroy()
        {
            _actor = null;
        }

        virtual public void OnUpdate()
        {
            if (_deathState == DeathState.NONE)
            {
                return;
            }
            else if (_deathState == DeathState.BEGIN)
            {
                SetDeathState(DeathState.WAITING_DISAPPEAR);
            }
            else
            {
                if (_isWaitingDestroy)
                {
                    UpdateWaitingDestroy();
                }
                if (_deathState == DeathState.WAITING_DISAPPEAR)
                {
                    if (_isWaitingSink)
                    {
                        UpdateWaitingSink();
                    }
                }
            }
        }

        virtual public void OnRelive()
        {
            _deathState = DeathState.NONE;
        }

        public void StopWaiting()
        {
            _startWaitingDestroyTimeStamp = 0;
            _startWaitingSinkTimeStamp = 0;
        }

        protected void SetDeathState(DeathState state)
        {
            if (this._deathState == state) return;
            this._deathState = state;
            if (this._deathState == DeathState.WAITING_DISAPPEAR)
            {
                OnEnterDisappear();
            }
        }

        virtual protected void OnEnterDisappear()
        {            
            if (_disappearType == DISAPPEAR_TYPE_SINK)
            {
                _waitingSinkDuration = GetSinkDuration();
                _waitingDestroyDuration = GetDestroyDuration();
                StartWaitingSink();
            }
            else if (_disappearType == DISAPPEAR_TYPE_DIRECTLY)
            {
                _waitingDestroyDuration = GetSinkDuration();
            }
            StartWaitingDestroy();
            _actor.SetLayer(PhysicsLayerDefine.Ghost);
        }

        virtual protected float GetSinkDuration()
        {
            return _disappearTime;
        }

        virtual protected float GetDestroyDuration()
        {
            return _disappearTime;
        }

        private void StartWaitingSink()
        {
            _startWaitingSinkTimeStamp = RealTime.time;
            _isWaitingSink = true;
        }

        private void StartWaitingDestroy()
        {
            _startWaitingDestroyTimeStamp = RealTime.time;
            _isWaitingDestroy = true;
        }

        private void UpdateWaitingSink()
        {
            if (RealTime.time - _startWaitingSinkTimeStamp >= _waitingSinkDuration)
            {
                _deathState = DeathState.SINK;
                _actor.actorController.Sink();
                _isWaitingSink = false;
            }
        }

        private void UpdateWaitingDestroy()
        {
            if (RealTime.time - _startWaitingDestroyTimeStamp >= _waitingDestroyDuration)
            {
                _deathState = DeathState.DESTROY;
                if (canRelease)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(_actor.actorData.ID, _actor.gameObject);
                }
                else
                {
                    GameObject.Destroy(_actor.gameObject);
                }
            }
        }

        public DeathState deathState
        {
            get { return _deathState; }
        }

        public void SetEntityInfo(EntityCreature entity)
        {
            if (entity == null) return;
            _sinkTime = 0f;
            _destroyTime = 2f;
            if (entity.entityType != EntityConfig.ENTITY_TYPE_NAME_MONSTER || !monster_helper.GetShowDeathFly(entity.monsterId))
            {
                _sinkTime = 11f;
                _destroyTime = 5f;
            }
        }
    }

}