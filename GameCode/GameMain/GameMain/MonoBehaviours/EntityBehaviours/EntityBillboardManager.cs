﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MogoEngine;
using GameMain;
using MogoEngine.RPC;
using Common.Utils;
using GameMain.GlobalManager;

namespace GameMain
{

    public class EntityBillboardManagerDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }
    public class EntityBillboardManager
    {
        public static readonly EntityBillboardManager instance = new EntityBillboardManager();

        List<EntityBillboard> _billboardList = new List<EntityBillboard>();
        List<EntityBillboard> _updateList = new List<EntityBillboard>();

        private Transform _cameraTransform;
        private Camera _camera;
        private Vector3 _lastCameraPos = Vector3.zero;

        public EntityBillboardManager()
        {
            MogoEngine.MogoWorld.RegisterUpdate<EntityBillboardManagerDelegate>("EntityBillboardManager.Update", Update);
        }

        public void AddBillboard(EntityBillboard billboard)
        {
            _billboardList.Add(billboard);
        }

        public void RemoveBillboard(EntityBillboard billboard)
        {
            _billboardList.Remove(billboard);
        }

        void Update()
        {
            if (_cameraTransform == null)
            {
                _camera = CameraManager.GetInstance().Camera;
                if (_camera != null)
                {
                    _cameraTransform = _camera.transform;
                }
                else
                {
                    return;
                }
            }
            if (_cameraTransform == null) return;
            Vector3 camPos = _cameraTransform.position;
            bool isCamPosChange = _lastCameraPos != camPos;
            if (isCamPosChange) _lastCameraPos = camPos;

            for (var i = 0; i < _billboardList.Count; i++)
            {
                var billboard = _billboardList[i];
                if (billboard.IsLooping())
                {
                    Vector3 pos = billboard.GetEntityPos();
                    if (isCamPosChange || billboard.IsEntityPosChange(pos))
                    {
                        if (CheckUpdatePosition(billboard, pos))
                        {
                            billboard.Show();
                        }
                        else
                        {
                            billboard.Hide();
                        }
                    }
                }
                else
                {
                    billboard.Hide();
                }
            }
        }

        // 检查屏幕内，是则更新位置
        private bool CheckUpdatePosition(EntityBillboard billboard, Vector3 pos)
        {
            Vector2 screenPosition = _camera.WorldToScreenPoint(pos);
            Vector3 viewPosition = _camera.WorldToViewportPoint(pos);
            if (!(Common.Utils.GameObjectHelper.CheckPositionIsInScreen(screenPosition) && viewPosition.z > 0 && viewPosition.z < 30))
            {
                return false;
            }

            billboard.CalculatePosition(screenPosition); // 更新位置
            return true;
        }


    }
}