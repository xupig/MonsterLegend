﻿using GameData;
using GameLoader.Utils;
using System;
using UnityEngine;
using MogoEngine.Mgrs;

namespace GameMain
{
    public class StaticModel : MonoBehaviour
    {
        public int modelID = 0;
        private bool _flying;
        private float _flyingSpeed = 0.1f;
        private uint _ownerEntityId;
        private bool _loaded;//是否加载完毕

        public bool Loaded
        {
            get { return _loaded; }
            set { _loaded = value; }
        }

        public Vector3 scale
        {
            set
            {
                GetTransform().localScale = value;
            }
            get
            {
                return GetTransform().localScale;
            }
        }

        public Vector3 position
        {
            set
            {
                GetTransform().position = value;
            }
            get
            {
                return GetTransform().position;
            }
        }

        public Transform GetTransform()
        {
            return transform;
        }

        public void FlyToEntity(uint ownerEntityId)
        {
            _ownerEntityId = ownerEntityId;
            _flying = true;
        }

        void Start()
        {

        }

        void Update()
        {
            if (_flying)
            {
                this.position += Vector3.Normalize(GameMain.EntityPlayer.Player.position - this.position)*0.8f;
                if (Vector3.Distance(this.position, GameMain.EntityPlayer.Player.position) < 0.5f)
                {
                    _flying = false;
                    EntityMgr.Instance.DestroyEntity(_ownerEntityId);
                }
            }
        }

    }
}