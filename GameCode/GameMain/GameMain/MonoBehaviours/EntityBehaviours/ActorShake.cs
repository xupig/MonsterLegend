﻿using GameData;
using GameLoader.Utils;
using System;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/5 11:20:21 
 * function: 实体震动脚本
 * *******************************************************/
namespace GameMain
{

    public class ActorShake : MonoBehaviour
    {
        public int id;
        public float xDirectionAmplitude;
        public float yDirectionAmplitude;
        public float zDirectionAmplitude;
        public float period;
        public float duration;

        private CharacterController _controller;
        private bool _isShaking = false;
        private float _xDirectionOffset = 0;
        private float _yDirectionOffset = 0;
        private float _zDirectionOffset = 0;
        private float _startShakeTime = 0;
        private Vector3 _lastPosition;
        private double _omega;

        void Start()
        {
            _controller = gameObject.GetComponent<CharacterController>();
            if (!XMLManager.entity_shake.ContainsKey(id))
            {
                LoggerHelper.Error(string.Format("初始化实体震动脚本错误，因为entity_shake表没有配置id = {0}的数据。", id));
                return;
            }
            var data = XMLManager.entity_shake[id];

            xDirectionAmplitude = data.__x_amplitude;
            yDirectionAmplitude = data.__y_amplitude;
            zDirectionAmplitude = data.__z_amplitude;
            period = data.__period;
            duration = data.__duration;
        }

        void LateUpdate()
        {
            if (!_isShaking)
            {
                return;
            }
            CalcShakeOffset();
            UpdatePosition();
        }

        public void StartShake()
        {
            if (_isShaking)
            {
                InterruptShake();
            }
            _controller.enabled = false;
            _omega = 2 * Math.PI / ACTSystem.ACTMathUtils.Abs(period);
            _lastPosition = Vector3.zero;
            _isShaking = true;
            _startShakeTime = UnityPropUtils.time;
        }

        public void InterruptShake()
        {
            if (!_isShaking)
            {
                return;
            }
            _controller.enabled = true;
            Reset();
            _isShaking = false;
        }

        private void Reset()
        {
            _xDirectionOffset = _yDirectionOffset = _zDirectionOffset = 0;
            gameObject.transform.position -= _lastPosition;
            _lastPosition = Vector3.zero;
        }

        private void CalcShakeOffset()
        {
            float nowTime = UnityPropUtils.time;
            float x = nowTime - _startShakeTime;
            if (x >= duration)
            {
                EndShake();
                return;
            }
            float fx = (float)Math.Sin(_omega * x);
            _xDirectionOffset = xDirectionAmplitude * fx;
            _yDirectionOffset = yDirectionAmplitude * fx;
            _zDirectionOffset = zDirectionAmplitude * fx;
        }

        private void EndShake()
        {
            _controller.enabled = true;
            _xDirectionOffset = _yDirectionOffset = _zDirectionOffset = 0;
            _isShaking = false;
        }

        private void UpdatePosition()
        {
            Vector3 offset = gameObject.transform.TransformDirection(_xDirectionOffset, _yDirectionOffset, _zDirectionOffset);
            Vector3 currentPosition = gameObject.transform.position;
            gameObject.transform.position = currentPosition - _lastPosition + offset;
            _lastPosition = offset;
        }
    }
}