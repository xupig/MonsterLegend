﻿using System;
using UnityEngine;

namespace GameMain
{
    public class XCutoutAdapter : MonoBehaviour
    {
        void OnEnable()
        {
            float cutout = 60;

            if (RuntimePlatform.Android == Application.platform)
            {
                cutout = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetCutoutScreen() * 1.0f;
            } 

            var rect = this.GetComponent<RectTransform>();
            var aMaxX = rect.anchorMax.x;
            var aMaxY = rect.anchorMax.y;
            var aMinX = rect.anchorMin.x;
            var aMinY = rect.anchorMin.y;
            var pivotX = rect.pivot.x;
            var posX = rect.anchoredPosition.x;
            var posY = rect.anchoredPosition.y;
            if (posX == 0 && pivotX == aMaxX && pivotX == aMinX)
            {
                float offset = 0;
                if (pivotX == 0) offset = cutout;
                if (pivotX == 1) offset = cutout * -1.0f;
                rect.anchoredPosition = new Vector3(offset, posY);
            }
        }
    }
}
