﻿using Common.ClientConfig;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.IO;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    public class XLowHPContainer : XContainer
    {
        private Transform _imageContainer;
        private List<ImageWrapper> _warningImageList;
        private uint _timerId;
        private bool _isShow = false;
        private float _currentTransparency;
        private float delta;
        private Color _setColor = new Color(1, 1, 1, 1);

        private float fadeOut;
        private int lastTime;

        private const int INTERVAL = 20;
        private const float FADE_OUT = 100;
        private const int LAST_TIME = 500;

        protected override void Awake()
        {
            _warningImageList = new List<ImageWrapper>();
            _imageContainer = this.transform;
            for (int i = 0; i < _imageContainer.transform.childCount; i++)
            {
                _warningImageList.Add(GetChildComponent<ImageWrapper>("Static_dishenming" + i));
            }
            Init();
        }

        private void ShowWarningImage()
        {
            int cur_hp = EntityPlayer.Player.cur_hp;
            int max_hp = EntityPlayer.Player.max_hp;
            float percent = (float)cur_hp / max_hp;
            if (percent <= 0.3)
            {
                if (_timerId == 0)
                {
                    ToggleWarningImage(true);
                    _currentTransparency = fadeOut;
                    SetAlpha(_currentTransparency);
                    _isShow = true;
                    _timerId = TimerHeap.AddTimer(0, INTERVAL, Twinkle);
                }
            }
            else
            {
                if (_timerId != 0)
                {
                    TimerHeap.DelTimer(_timerId);
                    ToggleWarningImage(false);
                    _timerId = 0;
                }
            }
        }

        private void ToggleWarningImage(bool visible)
        {
            _imageContainer.gameObject.SetActive(visible);
        }

        private void Twinkle()
        {
            if (_isShow == true)
            {
                _currentTransparency += delta;
                if (_currentTransparency >= 255)
                {
                    _currentTransparency = 255;
                    _isShow = false;
                }
            }
            else
            {
                _currentTransparency -= delta;
                if (_currentTransparency <= fadeOut)
                {
                    _currentTransparency = fadeOut;
                    _isShow = true;
                }
            }
            SetAlpha(_currentTransparency);
        }

        private void SetAlpha(float value)
        {
            for (int i = 0; i < _warningImageList.Count; i++)
            {
                _setColor.a = value / 255.0f;
                _warningImageList[i].color = _setColor;
            }
        }

        private void Init()
        {
            fadeOut = FADE_OUT;
            lastTime = LAST_TIME;
            ToggleWarningImage(false);
            delta = (255 - fadeOut) * INTERVAL / lastTime;
            EntityPropertyManager.Instance.AddEventListener(EntityPlayer.Player, EntityPropertyDefine.cur_hp, ShowWarningImage);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }
    }
}