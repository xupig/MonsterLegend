using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using Common.ClientConfig;

namespace GameMain
{
    public class XUICamera : MonoBehaviour
    {
        private static Camera _camera;
        public static Camera Camera
        {
            get { return _camera; }
        }

        protected void Awake()
        {
            _camera = GetComponent<Camera>();
            var layer = 1 << PhysicsLayerDefine.LAYER_TRANSPARENTFX;
            _camera.cullingMask ^= layer;
        }
    }
}
