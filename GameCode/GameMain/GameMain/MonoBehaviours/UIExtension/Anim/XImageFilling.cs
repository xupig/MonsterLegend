using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageFilling : MonoBehaviour
    {
        Image _currentImage = null;
        List<Image> _currentImages = new List<Image>();

        float _amount = 1f;
        float _fillingAmount = 1f;

        public float FillingAmount
        {
            get { return _fillingAmount; }
        }
        bool _isFilling = false;

        public bool IsFilling
        {
            get { return _isFilling; }
        }

        float _fillingDirection = 0;
        float _damping = 0.5f;

        public float Damping
        {
            set {_damping = value; }
        }

        public bool isRight
        {
            get
            {
                return _currentImage.fillOrigin == 1; 
            }
        }

        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                LoggerHelper.Error("XImageFilling can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            _currentImages.Add(_currentImage);
            for (int i = 0; i < transform.childCount; i++)
            {
                Image image = transform.GetChild(i).GetComponent<Image>();
                if (image == null)
                {
                    LoggerHelper.Error("XImageFilling can not find a Image in " + transform.GetChild(i).name);
                    this.enabled = false;
                    return;
                }
                _currentImages.Add(image);
            }
            _amount = _currentImage.fillAmount;
            _fillingAmount = _currentImage.fillAmount;
        }

        private bool _isResetToZero = false;
        public void SetIsResetToZero(bool value)
        {
            _isResetToZero = value;
        }

        //直接设定_fillingAmount，用于不进行动画播放的时候同步设定进度百分比
        public void SetValueDirectly(float value)
        {
            _fillingAmount = value;
            _amount = value;
            _isFilling = false;
        }

        private float _changeValue = 0;
        public void SetValue(float value)
        {
            _fillingAmount = _amount;
            _amount = value;
            _changeValue = _amount - _fillingAmount;
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
            }
        }

        void Update()
        {
            if (!_isFilling) return;
            //_fillingAmount = Mathf.SmoothDamp(_fillingAmount, _amount, ref _ref, _damping);
            _fillingAmount += (_changeValue / _damping) * UnityPropUtils.deltaTime;
            if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
            {
                _isFilling = false;
                float residualValue = _amount % 1;
                if ((_amount >= 1 && !_isResetToZero && residualValue == 0))
                {
                    _amount = 1;
                }
                else
                {
                    _amount = residualValue;
                }
                GetShowImage(_fillingAmount).fillAmount = _amount;
                //_fillingAmount = _amount;
            }
            else
            {
                GetShowImage(_fillingAmount).fillAmount = _fillingAmount % 1;
            }
        }

        Image GetShowImage(float value)
        {
            Image result = _currentImage;
            if (_currentImages.Count == 1)
            {
                return result;
            }
            int index = 0;
            if (value == 1)
            {
                index = 0;
            }
            else
            {
                index = (int)Math.Max(Mathf.Floor(value), 0);
            }
            index = Math.Min(index, _currentImages.Count - 1);
            result = _currentImages[index];
            for (int i = 0; i < _currentImages.Count; i++)
            {
                if(i > index)
                {
                    _currentImages[i].fillAmount = 0;
                }
                else if (i < index)
                {
                    _currentImages[i].fillAmount = 1;
                }
            }
            return result;
        }
    }
}
