﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    //旋转（拖动）组件
    public class XContainerRotator : XContainer, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
         //Transform _containerTransform;//为排除警告，注释掉

        private bool _isDragging = false;
        private bool _isMoving = false;

        private float _pointerStartX = 0;
        private float _pointerStartY = 0;
        private float _pointerEndX = 0;
        private float _pointerEndY = 0;
        private float _centerX = 0;
        private float _centerY = 0;
        private float _angleStart = 0;
        private float _angleEnd = 0;
        private float _zStart = 0;
        
        //可以设置的值
        private bool _drageable = true;     //是否可拖动
        private bool _isClockwise = true;    //起始转动方向是否为顺时针
        private bool _isChildrenRotate = true;//子节点是否跟随旋转
        private float _maxRotation = 0.05f; //最大旋转值

        public bool IsClockwise
        {
            get { return _isClockwise; }
            set { _isClockwise = value; }
        }

        public float MaxRotation
        {
            get { return _maxRotation; }
            set { _maxRotation = value; }
        }

        public bool IsChildrenRotate
        {
            get { return _isChildrenRotate; }
            set { _isChildrenRotate = value; }
        }

        public bool Dragable
        {
            get { return _drageable; }
            set { _drageable = value; }
        }

        public bool IsDragging
        {
            get { return _isDragging; }
        }

         new void Awake()//为排除警告，加回new
        {
        }

        public void OnDrag(PointerEventData eventData)
        {
            //CallFunction("OnDrag", eventData);
            if (Dragable == false)
            {
                return;
            }
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
            if (IsActive() == false)
            {
                return;
            }
            _isDragging = true;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            //CallFunction("OnBeginDrag", eventData);
            if (Dragable == false)
            {
                return;
            }
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
            if (IsActive() == false)
            {
                return;
            }
            Vector2 pointerPosition = Vector2.zero;
            _isDragging = true;
            if(this.gameObject)
            {
                _pointerStartX = eventData.position.x;
                _pointerStartY = eventData.position.y;
                _centerX = gameObject.transform.localPosition.x;
                _centerY = gameObject.transform.localPosition.y;
                //LoggerHelper.Error("_pointerStartX" + _pointerStartX);
                //LoggerHelper.Error("_pointerStartY" + _pointerStartY);
                //LoggerHelper.Error("_centerX" + _centerX);
                //LoggerHelper.Error("_centerY" + _centerY);
                _angleStart = (float)(Math.Atan2((_pointerStartY - _centerY), (_pointerStartX - _centerX)) / Math.PI * 180);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //CallFunction("OnEndDrag", eventData);
            _isDragging = false;
            if (this.gameObject)
            {
                _pointerEndX = eventData.position.x;
                _pointerEndY = eventData.position.y;
                _angleEnd = (float)(Math.Atan2((_pointerEndY - _centerY), (_pointerEndX - _centerX)) / Math.PI * 180);
                _isDragging = true;
            }
            StartMove();
        }

        private float _dAngle;       //变化角度
        private float _rotatorAmount;//旋转角度插值计数
        private float _damping = 0.3f;
        private float _ref = 0f;
        private float _lastZChange = 0.0f;//上一次update的z轴旋转变化值
        private float _offset = 0.5f;
        private void StartMove()
        {
            _zStart = this.gameObject.transform.eulerAngles.z;
            _dAngle = _angleEnd - _angleStart;
            //LoggerHelper.Error("_dAngle0:" + _dAngle);
            //LoggerHelper.Error("_zStart0:" + _zStart);
            //主顺时针转动
            if (_isClockwise)
            {
                //本来已经在边缘的情况不再转动
                if ((ACTSystem.ACTMathUtils.Abs((360 - _zStart) % 360) < _offset && _dAngle > 0) || (ACTSystem.ACTMathUtils.Abs((360 + _maxRotation - _zStart) % 360) < _offset && _dAngle < 0))
                {
                    _isMoving = false;
                    //LoggerHelper.Error("edge");
                }
                else
                {
                    _isMoving = true;
                }

                if (_zStart + _dAngle >= 360)
                {
                    _dAngle = 360 -_zStart;
                }
                //已经移动出初始位置，并接近终点
                if (ACTSystem.ACTMathUtils.Abs(_zStart % 360) > 1 && _zStart + _dAngle < _maxRotation + 360)
                {
                    _dAngle = _maxRotation - _zStart + 360;
                }
            }
            //主逆时针转动
            else
            {
                //本来已经在边缘的情况不再转动
                if ((ACTSystem.ACTMathUtils.Abs((360 - _zStart) % 360) < _offset && _dAngle < 0) || (ACTSystem.ACTMathUtils.Abs((360 + _maxRotation - _zStart) % 360) < _offset && _dAngle > 0))
                {
                    _isMoving = false;
                    //LoggerHelper.Error("edge");
                }
                else
                {
                    _isMoving = true;
                }

                if (_zStart + _dAngle <= 0)
                {
                    _dAngle = -_zStart;
                }
                if ( _zStart + _dAngle > _maxRotation)
                {
                    _dAngle = _maxRotation - _zStart;
                }
            }
            //LoggerHelper.Error("_dAngle:" + _dAngle);
            //LoggerHelper.Error("_zStart:" + _zStart);
            _rotatorAmount = 0.0f;
        }

        void Update()
        {
            if (this.gameObject)
            {
                if (this._isMoving)
                {
                    _rotatorAmount = Mathf.SmoothDamp(_rotatorAmount,_dAngle, ref _ref, _damping);
                    float curRotation = _zStart + _rotatorAmount >= 360 ? _zStart + _rotatorAmount - 360 : _zStart + _rotatorAmount;
                    curRotation = _zStart + _rotatorAmount;
                    this.gameObject.transform.rotation = Quaternion.Euler(0, 0, _zStart + _rotatorAmount);

                    if (!this._isChildrenRotate)
                    {
                        //子节点会向逆方向旋转
                        foreach (Transform child in this.gameObject.transform)
                        {
                            child.transform.rotation = Quaternion.Euler(0, 0, 0);
                        }
                    }

                    float zRotation = this.gameObject.transform.eulerAngles.z;
                    //判断旋转结束
                    //z旋转变化量
                    float zChange = zRotation - _zStart;

                    if (ACTSystem.ACTMathUtils.Abs(zChange - _lastZChange) < 0.1)
                    {
                        
                        RotateComplete();
                        return;
                    }
                    _lastZChange = zChange;
                }
            }
            else
            {
                LoggerHelper.Error("Container doesn't exist!!!");
            }
            
        }

        //旋转完成
        private void RotateComplete()
        {
            //LoggerHelper.Error("RotateComplete" + this.gameObject.transform.rotation.z);
            _isMoving = false;
        }
    }
}
