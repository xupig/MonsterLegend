﻿using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UIExtension;
using UnityEngine;
using UnityEngine.UI;

namespace GameMain
{
    public class XIntroduceBoss : XContainer
    {
        private XScreenShot _screenShot;
        private TextMeshWrapper _descriptionText;
        private TextMeshWrapper _firstNameText;
        private TextMeshWrapper _lastNameText;
        private GameObject _nameContainer;

        private const float MOVE_TIME = 0.3f;
        private const float DELAY_TIME = 0.0f;
        

        private string Key_left = "leftLine";
        private string Key_right = "rightLine";

        Dictionary<string, Transform> childTransform = new Dictionary<string, Transform>();
        Dictionary<string, Vector3> SrcPos = new Dictionary<string, Vector3>();
        Dictionary<string, Vector3> TargetPos = new Dictionary<string, Vector3>();

        private uint _closeTimerId;

        protected override void Awake()
        {
            _screenShot = AddChildComponent<XScreenShot>("Container_jietu");
            _firstNameText = GetChildComponent<TextMeshWrapper>("Container_introduce/Text_firstName");
            _lastNameText = GetChildComponent<TextMeshWrapper>("Container_introduce/Text_lastName");
            _descriptionText = GetChildComponent<TextMeshWrapper>("Container_introduce/Text_txtjieshao");
            //_firstNameText.rectTransform.localScale = new Vector3(0.6f,0.6f,0.6f);
            childTransform.Add(Key_left, transform.Find("Container_introduce/Container_leftLine"));
            childTransform.Add(Key_right, transform.Find("Container_introduce/Container_rightLine"));

            TargetPos.Add(Key_left, childTransform[Key_left].localPosition);
            TargetPos.Add(Key_right, childTransform[Key_right].localPosition);

            SrcPos.Add(Key_left, new Vector3(TargetPos[Key_left].x, TargetPos[Key_left].y+300, TargetPos[Key_left].z));
            SrcPos.Add(Key_right, new Vector3(TargetPos[Key_right].x, TargetPos[Key_right].y-300, TargetPos[Key_right].z));

            //var zzTop = AddChildComponent<DynamicAnchor>("Container_heibian/Zhezhao_top");
            //zzTop.Pivot = new Vector2(0, 1);
            //zzTop.ScreenAnchoredPosition = new Vector2(0f,1f);
            //var zzBottom = AddChildComponent<DynamicAnchor>("Container_heibian/Zhezhao_bottom");
            //zzBottom.Pivot = new Vector2(1, 0);
            //zzBottom.ScreenAnchoredPosition = new Vector2(1f, 0f);
            var jieTu = AddChildComponent<DynamicAnchor>("Container_jietu");
            jieTu.Pivot = new Vector2(0, 0);
            jieTu.ScreenAnchoredPosition = new Vector2(0f, 0f);
        }

        public void Play(string fname, string lname, string desc)
        {
            try
            {
                this.gameObject.SetActive(true);
                ResetCloseTimer();
                _closeTimerId = TimerHeap.AddTimer(10000, 0, ExceptionalClose);

                var rect = _screenShot.gameObject.GetComponent<RectTransform>();
                var canvasRect = _firstNameText.canvas.rootCanvas.GetComponent<RectTransform>();
                rect.sizeDelta = new Vector2(canvasRect.sizeDelta.x, canvasRect.sizeDelta.y);
                _screenShot.Play();
                RefreshContent(fname, lname, desc);
            }catch(Exception ex)
            {
                LoggerHelper.Error("Play:" + ex.Message + ":\n" + ex.StackTrace);
            }
        }

        private void ResetCloseTimer()
        {
            if (_closeTimerId != 0)
            {
                TimerHeap.DelTimer(_closeTimerId);
                _closeTimerId = 0;
            }
        }

        private void ExceptionalClose()
        {
            Stop();
        }

        public void Stop()
        {
            ResetCloseTimer();
            _screenShot.Stop();
            this.gameObject.SetActive(false);
        }

        private void RefreshContent(string fname, string lname, string desc)
        {
            _descriptionText.enabled = false;
            _firstNameText.enabled = false;
            _lastNameText.enabled = false;
            _descriptionText.text = desc;
            _firstNameText.text = fname;
            _lastNameText.text = lname;
            PlayAnimation();
        }

        private void PlayAnimation()
        {
            childTransform[Key_left].gameObject.SetActive(false);
            childTransform[Key_right].gameObject.SetActive(false);
            childTransform[Key_left].localPosition = SrcPos[Key_left];
            childTransform[Key_right].localPosition = SrcPos[Key_right];

            PlayLeftBgAnimation();
            PlayRightBgAnimation();
        }

        private void PlayLeftBgAnimation()
        {
            childTransform[Key_left].gameObject.SetActive(true);
            TweenPosition.Begin(childTransform[Key_left].gameObject, MOVE_TIME, TargetPos[Key_left], DELAY_TIME, UITweener.Method.EaseOut);
        }

        private void PlayRightBgAnimation()
        {
            childTransform[Key_right].gameObject.SetActive(true);
            TweenPosition.Begin(childTransform[Key_right].gameObject, MOVE_TIME, TargetPos[Key_right], DELAY_TIME, UITweener.Method.EaseOut, OnTweenFinshed);
        }

        private void OnTweenFinshed(UITweener tween)
        {
            _descriptionText.enabled = true;
            _firstNameText.enabled = true;
            _lastNameText.enabled = true;
        }

        public void FadeOut()
        {
            if (!this.gameObject.activeSelf) return;
            //TweenPosition tween = TweenPosition.Begin(_nameContainer.gameObject, MOVE_TIME, _nameFrom, 0, UITweener.Method.EaseIn);
            //tween.onFinished = OnCloseTweenEnd;
            Stop();
        }
    }
}
