﻿using UnityEngine;
using UIExtension;

namespace GameMain
{
    public class XGameObjectTweenSizeDelta : MonoBehaviour
    {
        RectTransform _trans = null;
        Vector2 _fromSize;              //起始值
        Vector2 _curSize;              //当前值
        Vector2 _toSize;                //目标值    
        float _duration = 0f;
        Vector2 _add;
        Vector2 _tweenDirection;
        ImageWrapper _image;
        WrapMode _wrapMode;

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
            set { _isTweening = value; }
        }

        void Awake()
        {
            _trans = GetComponent<RectTransform>();
            _image = GetComponent<ImageWrapper>();
            
        }

        public void SetFromToSizeOnce(Vector2 formSize, Vector2 toSize, float duration)
        {
            SetFromToSize(formSize, toSize, duration, (int)WrapMode.Once);
        }

        public void SetToSizeOnce(Vector2 toSize, float duration)
        {
            SetFromToSize(_trans.sizeDelta, toSize, duration, (int)WrapMode.Once);
        }

        public void SetToSize(Vector2 toSize, float duration, int wrapMode)
        {
            SetFromToSize(_trans.sizeDelta, toSize, duration, wrapMode);
        }

        public void SetFromToSize(Vector2 formSize, Vector2 toSize, float duration, int wrapMode)
        {
            _curSize = formSize;
            _fromSize = formSize;
            _toSize = toSize;
            _duration = duration;
            _wrapMode = (WrapMode)wrapMode;

            _add = new Vector2((_toSize.x - _fromSize.x) / _duration, (_toSize.y - _fromSize.y) / _duration);
            _tweenDirection = (Vector2)Vector3.Normalize(_toSize - _curSize);
            SetSizeDelta();
            _isTweening = true;
        }


        void Update()
        {
            if (!_isTweening) return;

            _curSize += _add * UnityPropUtils.deltaTime;


            if (!_isTweening) return;

            _curSize += _add * UnityPropUtils.deltaTime;
            if ((Vector2)Vector3.Normalize((_toSize - _curSize)) != _tweenDirection)
            {
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curSize = _toSize;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _curSize = _toSize;
                    _toSize = _fromSize;
                    _fromSize = _curSize;
                    _tweenDirection = -_tweenDirection;
                    _add = -_add;
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curSize = _fromSize;
                }
            }
            
            
            SetSizeDelta();
            _image.SetAllDirty();

        }


        private void SetSizeDelta()
        {
            _trans.sizeDelta = _curSize;
        }
    }
}
