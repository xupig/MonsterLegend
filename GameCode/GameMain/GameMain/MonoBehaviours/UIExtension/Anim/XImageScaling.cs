using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageScaling : MonoBehaviour
    {
        Image _currentImage = null;
        bool _isScaling = false;
        float _velocity = 0;
        float _fillingDirection = 0;
        float _endAmount = 0;
        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                LoggerHelper.Error("XImageScaling can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
        }

        public void SetValue(float startAmount, float endAmount, float time)
        {
            _currentImage.fillAmount = startAmount;
            _isScaling = true;
            _endAmount = endAmount;
            _velocity = (_endAmount - startAmount) / time;
            _fillingDirection = Mathf.Sign(endAmount - startAmount);
        }

        void Update()
        {
            if (!_isScaling) return;
            float curAmount = _currentImage.fillAmount;
            float temp = _endAmount - curAmount;
            if (ACTSystem.ACTMathUtils.Abs(temp) < 0.001f
                || Mathf.Sign(temp) != _fillingDirection)
            {
                _isScaling = false;
                _currentImage.fillAmount = _endAmount;
            }
            else {
                _currentImage.fillAmount = curAmount + UnityPropUtils.deltaTime * _velocity;
            }
        }
    }
}
