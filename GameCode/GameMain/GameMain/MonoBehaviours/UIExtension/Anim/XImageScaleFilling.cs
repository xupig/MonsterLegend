﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageScaleFilling : MonoBehaviour
    {
        Image _currentImage = null;
        float _amount = 1f;
        float _fillingAmount = 1f;
        Vector3 _scale = Vector3.zero;
        bool _isHorizontal = true;

        public float FillingAmount
        {
            get { return _fillingAmount; }
        }
        bool _isFilling = false;

        public bool IsFilling
        {
            get { return _isFilling; }
        }

        float _fillingDirection = 0;
        float _damping = 0.5f;

        public float Damping
        {
            set { _damping = value; }
        }

        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                LoggerHelper.Error("XImageFilling can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            _amount = 0;
            _fillingAmount = 0;
        }

        public void SetHorizontal(bool isHorizontal)
        {
            _isHorizontal = isHorizontal;
        }

        private bool _isResetToZero = false;
        public void SetIsResetToZero(bool value)
        {
            _isResetToZero = value;
        }

        //直接设定_fillingAmount，用于不进行动画播放的时候同步设定进度百分比
        public void SetValueDirectly(float value)
        {
            _fillingAmount = value;
            _amount = value;
            _isFilling = false;
        }

        private float _changeValue = 0;
        public void SetValue(float value)
        {
            _fillingAmount = _amount;
            _amount = value;
            _changeValue = _amount - _fillingAmount;
            //LoggerHelper.Error("SetValue" + _changeValue + "/" + _amount +"/"+_fillingAmount);
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
                //LoggerHelper.Error("_fillingDirection" + _fillingDirection);
            }
        }

        void Update()
        {
            if (!_isFilling) return;
            //_fillingAmount = Mathf.SmoothDamp(_fillingAmount, _amount, ref _ref, _damping);
            _fillingAmount = Math.Max(_fillingAmount + (_changeValue / _damping) * UnityPropUtils.deltaTime, 0);
            //_fillingAmount = Math.Min(_fillingAmount, 1);
            if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
            {
                _isFilling = false;
                float residualValue = _amount % 1;
                if (_isHorizontal)
                {
                    _scale = Vector3.one;
                    if ((_amount > 1 && !_isResetToZero && residualValue == 0) || (_amount == 1 && residualValue == 0))
                    {
                        _scale.x = 1;
                    }
                    else
                    {
                        _scale.x = residualValue;
                    }
                    _currentImage.transform.localScale = _scale;
                }
                else
                {
                    _scale = Vector3.one;
                    if ((_amount > 1 && !_isResetToZero && residualValue == 0) || (_amount == 1 && residualValue == 0))
                    {
                        _scale.y = 1;
                    }
                    else
                    {
                        _scale.y = residualValue;
                    }
                    _currentImage.transform.localScale = _scale;
                }
                //_fillingAmount = _amount;
                _amount = residualValue;
            }
            else
            {
                if (_isHorizontal)
                {
                    _scale = Vector3.one;
                    _scale.x = _fillingAmount % 1;
                    _currentImage.transform.localScale = _scale;
                }
                else
                {
                    _scale = Vector3.one;
                    _scale.y = _fillingAmount % 1;
                    _currentImage.transform.localScale = _scale;
                }
            }
        }
    }
}
