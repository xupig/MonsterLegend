﻿using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain
{
    public class XGameObjectComplexIcon : MonoBehaviour
    {
        private List<GameObject> _itemList;
        private ComplexIcons _complexIcons;

        void Awake()
        {
            _itemList = new List<GameObject>();
            gameObject.transform.Find("item").GetComponent<RectTransform>().sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
        }

        private GameObject CopyUIGameObject(GameObject hostGo, string srcPath, string parentPath)
        {
            return LuaFacade.CopyUIGameObject(hostGo, srcPath, parentPath);
        }

        public void AddComplexIcon(int complexIconId)
        {
            Dictionary<int, List<int>> types = icon_complex_helper.GetTypes(complexIconId);
            _complexIcons = new ComplexIcons();
            List<ComplexIcon> list = new List<ComplexIcon>();
            CheckItem(types.Count);
            int num = 0;
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].SetActive(i + 1 <= types.Count);                
            }

            foreach (var item in types)
            {
                var value = item.Value;
                ComplexIcon icon = new ComplexIcon();
                List<ComplexParams> complexParams = new List<ComplexParams>();
                for (int i = 0; i < value.Count; i += 2)
                {
                    int type = value[i];
                    int id = value[i + 1];
                    List<float> paramList = icon_complex_helper.GetParams(complexIconId, type, id);
                    if (paramList == null)
                        continue;
                    ComplexParams c = ComplexParams.CreateParamsByType(_itemList[num], type, paramList);
                    if(c != null)
                        complexParams.Add(c);
                }
                icon.ParamsList = complexParams;
                list.Add(icon);
                num += 1;
            }
            _complexIcons.IconList = list;


            //TODO:获取complexIcon配置信息，现在写死。
            //_complexIcons = new ComplexIcons();
            //List<ComplexIcon> list = new List<ComplexIcon>();
            //var listLength = types.Count;

            //if (_itemList.Count < listLength)
            //{
            //    int addNum = listLength - _itemList.Count;
            //    for (int i = 0; i < addNum; i++)
            //    {
            //        _itemList.Add(CopyUIGameObject(gameObject, "item", ""));
            //    }
            //}

            ////序列帧
            //ComplexIcon icon1 = new ComplexIcon();
            //List<ComplexParams> complexParams1 = new List<ComplexParams>();
            //ComplexParams complexIcon11 = new AnimParams(_itemList[0], 2, 102, 5);
            //complexParams1.Add(complexIcon11);
            //icon1.ParamsList = complexParams1;
            //list.Add(icon1);


            ////旋转、缩放
            //ComplexIcon icon2 = new ComplexIcon();
            //List<ComplexParams> complexParams2 = new List<ComplexParams>();
            //XGameObjectTweenRotation rotationComponent =  InitRotationComponent(_itemList[1]);
            //ComplexParams complexIcon21 = new RotationParams(new Vector3(0, 0, 0), new Vector3(0, 0, 360), 5, 2, rotationComponent, 6028, _itemList[1]);
            //complexParams2.Add(complexIcon21);

            //XGameObjectTweenScale scaleComponent = InitScaleComponent(_itemList[1]);
            //ComplexParams complexIcon22 = new ScaleParams(0.5f, 1, 5, 4, scaleComponent, 6028, _itemList[1]);
            //complexParams2.Add(complexIcon22);

            //icon2.ParamsList = complexParams2;
            //list.Add(icon2);

            //////alpha
            ////ComplexIcon icon3 = new ComplexIcon();
            ////List<ComplexParams> complexParams3 = new List<ComplexParams>();
            ////XImageTweenAlpha alphaComponent = InitAlphaComponent(_itemList[2]);
            ////Debug.LogError("alpha 1");
            ////ComplexParams complexIcon31 = new AlphaParams(0.3f, 1f, 5f, 4, alphaComponent, 6028, _itemList[2]);
            ////Debug.LogError("alpha 2");
            ////complexParams3.Add(complexIcon31);

            ////icon3.ParamsList = complexParams3;
            ////list.Add(icon3);

            //_complexIcons.IconList = list;
        }

        private void CheckItem(int num)
        {
            if (_itemList.Count < num)
            {
                int addNum = num - _itemList.Count;
                for (int i = 0; i < addNum; i++)
                {
                    var go = CopyUIGameObject(gameObject, "item", "");
                    InitScaleComponent(go);
                    InitRotationComponent(go);
                    //InitAlphaComponent(go);
                    _itemList.Add(go);
                }
            }
        }

        public void StartPlay()
        {
            foreach (var item in _complexIcons.IconList)
            {
                foreach (var paramsItem in item.ParamsList)
                {
                    paramsItem.StartTween();
                }
            }
        }

        private XGameObjectTweenScale InitScaleComponent(GameObject go)
        {
            var tweenScale = go.GetComponent<XGameObjectTweenScale>();
            if (tweenScale == null)
            {
                tweenScale = go.AddComponent<XGameObjectTweenScale>();
            }
            return tweenScale;
        }

        private XGameObjectTweenRotation InitRotationComponent(GameObject go)
        {
            var tweenRotation = go.GetComponent<XGameObjectTweenRotation>();
            if (tweenRotation == null)
            {
                tweenRotation = go.AddComponent<XGameObjectTweenRotation>();
            }
            return tweenRotation;
        }

        private XImageTweenAlpha InitAlphaComponent(GameObject go)
        {
            var alpha = go.GetComponent<XImageTweenAlpha>();
            if (alpha == null)
            {
                alpha = go.AddComponent<XImageTweenAlpha>();
            }
            return alpha;
        }

        private class ComplexIcons
        {
            private int _id;
            //private XGameObjectTweenScale _tweenScale;
            //private XImageTweenAlpha _tweenAlpha;
            //private XGameObjectTweenRotation _tweenRotation;
            private List<ComplexIcon> _iconList;

            public List<ComplexIcon> IconList
            {
                get { return _iconList; }
                set { _iconList = value; }
            }
        }

        private class ComplexIcon
        {
            private List<ComplexParams> _paramsList;

            public List<ComplexParams> ParamsList
            {
                get { return _paramsList; }
                set { _paramsList = value; }
            }
        }

        private class ComplexParams
        {
            public virtual void StartTween() { }
            public virtual void StopTween() { }

            public static ComplexParams CreateParamsByType(GameObject parent, int type, List<float> paramList)
            {
                if (type == 1)
                {
                    return new AnimParams(parent, (int)paramList[0], (int)paramList[1], (int)paramList[2]);
                }
                else if (type == 2)
                {

                    return new RotationParams(new Vector3(0, 0, paramList[0]), new Vector3(0, 0, paramList[1]), (int)paramList[2], (int)paramList[3], (int)paramList[4], (int)paramList[5], parent);
                }
                else if (type == 3)
                {
                    return new ScaleParams(paramList[0], paramList[1], paramList[2], (int)paramList[3], (int)paramList[4], parent);
                }
                return null;
            }
        }

        private class AnimParams : ComplexParams
        {
            private GameObject _parent;
            private int _iconAnimId;
            private int _colorId;
            private int _speed = 1;


            public AnimParams(GameObject parent, int iconAnimId, int colorId, int speed, bool raycastTarget = false)
            {
                _parent = parent;
                _iconAnimId = iconAnimId;
                _colorId = colorId;
                _speed = speed;
                GameSpriteAnimationUtils.AddAnimation(parent, iconAnimId, colorId, raycastTarget);
                GameSpriteAnimationUtils.SetAnimationSpeed(parent, speed);
            }


            public override void StartTween()
            {
                GameSpriteAnimationUtils.PlayAnimation(_parent, true);
            }

            public override void StopTween()
            {
                GameSpriteAnimationUtils.PlayAnimation(_parent, false);
            }
        }

        private class ScaleParams : ComplexParams
        {
            private GameObject _parent;
            private float _fromScale;
            private float _toScale;
            private float _duration;
            private int _wrapMode;
            private XGameObjectTweenScale _tweenScale;

            public ScaleParams(float fromScale, float toScale, float duration, int wrapMode, int iconId, GameObject parent)
            {
                _fromScale = fromScale;
                _toScale = toScale;
                _duration = duration;
                _wrapMode = wrapMode;
                _tweenScale = parent.GetComponent<XGameObjectTweenScale>();
                _tweenScale.IsTweening = false;
                _parent = parent;
                GameAtlasUtils.AddIcon(parent, iconId, 0, false, 1, false, false, 1);
            }

            public override void StartTween()
            {
                _tweenScale.SetFromToScale(_fromScale, _toScale, _duration, _wrapMode);
            }

            public override void StopTween()
            {
                _tweenScale.IsTweening = false;
            }
        }

        private class AlphaParams : ComplexParams
        {
            private GameObject _parent;
            private float _fromAlpha;
            private float _toAlpha;
            private float _duration;
            private int _wrapMode;
            private XImageTweenAlpha _tweenAlpha;


            public AlphaParams(float fromAlpha, float toAlpha, float duration, int wraMode, XImageTweenAlpha tweenAlpha, int iconId, GameObject parent)
            {
                _fromAlpha = fromAlpha;
                _toAlpha = toAlpha;
                _duration = duration;
                _wrapMode = wraMode;
                _tweenAlpha = tweenAlpha;
                _tweenAlpha.IsTweening = false;
                _parent = parent;
                GameAtlasUtils.AddIcon(parent, iconId, 0, false, 1, false, false, 1);
            }

            public override void StartTween()
            {
                _tweenAlpha.SetFromToAlpha(_fromAlpha, _toAlpha, _duration, _wrapMode);
            }

            public override void StopTween()
            {
                _tweenAlpha.IsTweening = false;
            }
        }

        private class RotationParams : ComplexParams
        {
            private GameObject _parent;
            private Vector3 _fromRotation;
            private Vector3 _toRotation;
            private float _duration;
            private int _wrapMode;
            private int _colorId;
            private XGameObjectTweenRotation _tweenRotation;

            public RotationParams(Vector3 fromRotation, Vector3 toRotation, float duration, int wrapMode, int iconId, int colorId, GameObject parent)
            {
                _fromRotation = fromRotation;
                _toRotation = toRotation;
                _duration = duration;
                _wrapMode = wrapMode;
                _colorId = colorId;
                _tweenRotation = parent.GetComponent<XGameObjectTweenRotation>();
                _tweenRotation.IsTweening = false;
                _parent = parent;
                GameAtlasUtils.AddIcon(parent, iconId, _colorId, false, 1, false, false, 1);
            }

            public override void StartTween()
            {
                _tweenRotation.SetFromToRotation(_fromRotation, _toRotation, _duration, _wrapMode, null);
            }

            public override void StopTween()
            {
                _tweenRotation.IsTweening = false;
            }
        }
    }
}
