﻿using UnityEngine;
using System.Collections.Generic;

namespace GameMain
{
    public class XAutoCombat : MonoBehaviour
    {
        public Dictionary<string, GameObject> _textDict;
        Transform _curTrans;
        GameObject _curObj = null;

        float _add;
        float _fromPosY = 0; //起始值
        float _curPosY = 0; //当前值
        int _direction = 1;  //1：向上，-1：向下
        int _childCount = 0;
        int _index = 1; //播放动画的次数

        int _distance = 10; //跳跃距离
        float _duration = 0.15f; //经过时间
        float _originPosY = 0;
        

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
            set
            {
                _isTweening = value;
            }
        }

        void Awake()
        {
            InitTextDict();
        }

        void InitTextDict()
        {
            _childCount = transform.childCount;
            _textDict = new Dictionary<string, GameObject>();
            for (int i = 0; i < _childCount; i++)
            {
                GameObject childGo = transform.GetChild(i).gameObject;
                string childName = childGo.name;
                string mark = childName[childName.Length - 1]+"";
                _textDict.Add(mark, childGo);
            }

            _originPosY = _textDict["1"].transform.localPosition.y;
        }

        public void StartAnimation()
        {
            StopAnimation();
            _add = _distance / _duration;
            SetObject(1);
            SetPosY();
            _isTweening = true;  
        }

        //设置跳跃距离和经过时间
        public void SetDD(int distance, float duration)
        {
            _distance = distance;
            _duration = duration;
        }

        void SetObject(int index)
        {
            _curObj = _textDict[index.ToString()];
            _curTrans = _curObj.transform;
        }

        void SetPosY()
        {
            _curPosY = _curTrans.localPosition.y;
            _fromPosY = _curPosY;
        }

        void Update()
        {
            if (!_isTweening)
            {
                return;
            } 

            _curPosY += _add * Time.deltaTime * _direction;

            //到达终点
            if (CheckDistance())
            {
                SetPosition();
                //方向向下，可以开始下一个数
                if (_direction == -1)
                {
                    this.SetObject(GetNextIndex());
                }

                _direction = -_direction;
                this.SetPosY();
            }
            else
            {
                SetPosition();
            }
                
        }

        private int GetNextIndex()
        {
            _index += 1;
            if (_index > _childCount)
            {
                _index = 1;
            }
            return _index;
        }

        private bool CheckDistance()
        {
            if (Mathf.Abs(_curPosY - _fromPosY)> _distance)
            {
                _curPosY = _fromPosY + _distance*_direction;
                return true;
            }
            else
            {
                return false;
            }    
        }

        private void SetPosition()
        {
            _curTrans.localPosition = new Vector3(_curTrans.localPosition.x, _curPosY, 0);
        }

        void StopAnimation()
        {
            for (int i = 0; i < _childCount; i++)
            {
                Transform trans = _textDict[(i+1).ToString()].transform;
                trans.localPosition = new Vector3(trans.localPosition.x, _originPosY, 0);
            }
            _direction = 1;
        }

    }
}
