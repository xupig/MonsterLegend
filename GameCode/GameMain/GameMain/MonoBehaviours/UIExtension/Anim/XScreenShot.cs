﻿using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UIExtension;
using UnityEngine;
using UnityEngine.UI;

namespace GameMain
{
    public class XScreenShot : XContainer
    {
        private RawImage _rawImage;
        protected override void Awake()
        {
            _rawImage = gameObject.AddComponent<RawImage>();
           //MogoGameObjectHelper.SetFullScreenSize(_rawImage.gameObject);
        }

        private RenderTexture GetEntityScreenShot()
        {
            List<GameObject> goList = new List<GameObject>();
            var enumerator = MogoWorld.Entities.GetEnumerator();
            EntityCreature creature;
            while (enumerator.MoveNext())
            {
                var entity = enumerator.Current.Value;
                if (entity is EntityCreature)
                {
                    creature = entity as EntityCreature;
                    if (creature.CanDisplay() && creature.actor != null && creature.actor.gameObject != null)
                    {
                        goList.Add(creature.actor.gameObject);
                    }
                }
            }
            RenderTexture texture = ScreenShot.GetBackgroundBlurScreenShot(CameraManager.GetInstance().Camera, goList);
            return texture;
        }

        public void Play()
        {
            var camera = CameraManager.GetInstance().Camera;
            bool enabled = PostEffectHandlerBase.Instance.GetPostEffectEnabled(camera);
            PostEffectHandlerBase.Instance.SetPostEffectEnabled(camera, false);
            RefreshImage(GetEntityScreenShot());
            PostEffectHandlerBase.Instance.SetPostEffectEnabled(camera, enabled);
        }

        private void RefreshImage(Texture texture)
        {
            if (texture != null)
            {
                _rawImage.texture = texture;
            }
        }

        public void Stop()
        {
            _rawImage.texture = null;
        }

    }
}
