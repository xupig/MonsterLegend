﻿using LuaInterface;
using UnityEngine;
using System;
using ACTSystem;

namespace GameMain
{
    public enum DirectionMode
    {
        All = 0,
        Only_X = 1,
        Only_Y = 2,
        Only_Z = 3
    }

    public class XGameObjectTweenScale : MonoBehaviour
    {
        Transform _trans = null;
        float _beginScale;              //起始值
        float _endScale;                //目标值
        float _curScale;                //当前值
        float _change = 0f;             //变化值
        float _duration = 0f;
        float _timestamp = 0f;
        WrapMode _wrapMode;
        TweenMode _tweenMode = TweenMode.Linear;
        DirectionMode _directionMode = DirectionMode.All;
        LuaFunction _luafunc;
        
        private bool _isTweening = false;
        public bool IsTweening
        {
            get { return _isTweening; }
            set { _isTweening = value; }
        }

        void Awake()
        {
            _trans = transform;
        }

        public void OnceOutQuad(float toScale, float duration, LuaFunction luafunc)
        {
            _tweenMode = TweenMode.OutQuad;
            SetFromToScale(_trans.localScale.x, toScale, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToScaleOnce(float toScale, float duration, LuaFunction luafunc)
        {
            SetFromToScale(_trans.localScale.x, toScale, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetFromToScaleOnce(float formScale, float toScale, float duration, LuaFunction luafunc)
        {
            SetFromToScale(formScale, toScale, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToScale(float toScale, float duration, int wrapMode, LuaFunction luafunc)
        {
            SetFromToScale(_trans.localScale.x, toScale, duration, wrapMode, luafunc);
        }

        public void SetScaleTween(float formScale, float toScale, float duration, int wrapMode, int tweenMode, LuaFunction luafunc = null)
        {
            _tweenMode = (TweenMode)tweenMode;
            SetFromToScale(formScale, toScale, duration, wrapMode, luafunc);
        }

        public void SetSacleDirection(float formScale, float toScale, float duration, int wrapMode, int tweenMode, int directionMode, LuaFunction luafunc = null)
        {
            _tweenMode = (TweenMode)tweenMode;
            _directionMode = (DirectionMode)directionMode;
            SetFromToScale(formScale, toScale, duration, wrapMode, luafunc);
        }

        public void SetFromToScale(float formScale, float toScale, float duration, int wrapMode, LuaFunction luafunc = null)
        {
            _timestamp = 0;
            _beginScale = formScale;
            _curScale = formScale;
            _endScale = toScale;
            _change = _endScale - _beginScale;
            _duration = duration;
            _wrapMode = (WrapMode)wrapMode;
            _luafunc = luafunc;
            SetScale();
            _isTweening = true;
        }

        void Update()
        {
            if (!_isTweening)
            {
                _timestamp = 0;
                return;
            }
            _timestamp += UnityPropUtils.deltaTime;
            _curScale = TweenModeManager.GetProgress(_tweenMode, _timestamp, _beginScale, _change, _duration);

            if (_timestamp > _duration)
            {
                _timestamp = 0;
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curScale = _endScale;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _curScale = _endScale;
                    _endScale = _beginScale;
                    _beginScale = _curScale;
                    _change = _endScale - _beginScale;
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curScale = _beginScale;
                }
                
                if (_luafunc != null)
                {
                    SetScale();
                    LuaFacade.CallLuaFunction(_luafunc);
                    return;
                }
            }           
            SetScale();
        }

        private void SetScale()
        {
            switch (_directionMode)
            {
                case DirectionMode.All:
                    _trans.localScale = new Vector3(_curScale, _curScale, _curScale);
                    return;
                case DirectionMode.Only_X:
                    _trans.localScale = new Vector3(_curScale, _trans.localScale.y, _trans.localScale.z);
                    return;
                case DirectionMode.Only_Y:
                    _trans.localScale = new Vector3(_trans.localScale.x, _curScale, _trans.localScale.z);
                    return;
                case DirectionMode.Only_Z:
                    _trans.localScale = new Vector3(_trans.localScale.x, _trans.localScale.y, _curScale);
                    return;
                default:
                    _trans.localScale = new Vector3(_curScale, _curScale, _curScale);
                    return;
            }
        }

    }
}