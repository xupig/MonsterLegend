﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;
using GameResource;

namespace GameMain
{
    public class PathModelComponent : XContainer, IPointerClickHandler
    {
        protected RectTransform _rectTransform;
        private GameObject _modelGo;

        private float _scale = 1f;
        private Vector3 _startPosition = new Vector3(0, 0, 0);
        private Vector3 _rotation = new Vector3(0, 180, 0);

        private ModelDragComponent _dragComponent;

        SortOrderedRenderAgent sortOrderedRenderAgent;

        public ModelDragComponent DragComponent
        {
            get
            {
                return _dragComponent;
            }
        }

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _dragComponent = this.gameObject.AddComponent<ModelDragComponent>();
        }

        public void SetDragable(bool state)
        {
            if (_dragComponent)
                _dragComponent.dragable = state;
        }

        public void LoadModel(string modelPath, Action<GameObject> callback = null)
        {
            DoLoadModel(modelPath, callback);
        }

        private void DoLoadModel(string modelPath, Action<GameObject> callback = null)
        {
            if (string.IsNullOrEmpty(modelPath))
            {
                if (callback != null)
                {
                    callback(null);
                }
                return;
            }
            modelPath = ObjectPool.Instance.GetResMapping(modelPath);
            ObjectPool.Instance.GetGameObject(modelPath, (obj) =>
            {
                if (obj == null)
                {
                    GameLoader.Utils.LoggerHelper.Error("CreateActor modelName:" + modelPath + " is null");
                    return;
                }
                OnModelLoaded(obj);
                if (callback != null)
                {
                    callback(obj);
                }
            });
        }

        private void OnModelLoaded(GameObject go)
        {
            if (go != null)
            {
                ClearModelGameObject();
                _modelGo = go;
                if (_dragComponent)
                    _dragComponent.SetModel(_modelGo);
                var actor = go.GetComponent<ACTActor>();
                if (actor != null)
                {
                    actor.actorController.isStatic = true;
                }
                var characterController = go.GetComponent<CharacterController>();
                if (characterController != null)
                {
                    characterController.enabled = false;
                }
                go.transform.SetParent(_rectTransform);
                go.transform.localPosition = _startPosition;
                go.transform.localRotation = Quaternion.Euler(_rotation);
                go.transform.localScale = new Vector3(_scale, _scale, _scale);
                sortOrderedRenderAgent = go.AddComponentOnlyOne<SortOrderedRenderAgent>();
                SortOrderedRenderAgent.ReorderAll();
                ModelComponentUtil.ChangeUIModelParam(go);
                //强制刷新一下，使得模型上的特效能正常显示
                go.SetActive(false);
                go.SetActive(true);
            }
        }

        public void ClearModelGameObject()
        {
            if (_modelGo != null)
            {
                Destroy(_modelGo);
                if (_dragComponent)
                    _dragComponent.Clear();
                _modelGo = null;
            }
        }

        public void SetStartSetting(Vector3 position, Vector3 euler, int scale)
        {
            _startPosition = position;
            _rotation = euler;
            _scale = scale;
        }

        public void PlayFx(string path, Vector3 relativePos, float duration)
        {
            if (_modelGo != null)
            {
                Vector3 position = _modelGo.transform.position + relativePos;
                ACTVisualFXManager.GetInstance().PlayACTVisualFX(path, position, Vector3.zero, duration, 0, VisualFXPriority.H, sortOrderedRenderAgent.order + 1);
            }
        }
    }
}