﻿using ACTSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using System.Collections.Generic;

namespace GameMain
{
    public class ActModelComponent : XContainer, IPointerClickHandler
    {
        protected RectTransform _rectTransform;
        protected GameObject _modelGo;
        protected GameObject _mountModelGo;
        protected ACTActor _playerActor;
        protected int _id = 0;
        protected Vector3 _initRectPosition = new Vector3(0, 0, 0);

        SortOrderedRenderAgent sortOrderedRenderAgent;

        protected float _scale = 1f;
        protected Vector3 _startPosition = new Vector3(0, 0, 0);
        protected Vector3 _rotation = new Vector3(0, 180, 0);

        protected ModelDragComponent _dragComponent;

        public ModelDragComponent dragComponent
        {
            get
            {
                return _dragComponent;
            }
        }

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _dragComponent = this.gameObject.AddComponent<ModelDragComponent>();
            if (_rectTransform != null)
            {
                _initRectPosition = _rectTransform.localPosition;
            }
        }

        public void SetDragable(bool state)
        {
            this._dragComponent.dragable = state;
            this._dragComponent.raycastTarget = state;
        }

        protected void OnLoadFinished()
        {
            if (_modelGo != null)
            {
                ModelComponentUtil.ChangeUIModelParam(_modelGo);
            }
        }

        public void ClearModelGameObject()
        {
            if (_modelGo != null)
            {
                Destroy(_modelGo);
                _dragComponent.Clear();
                _modelGo = null;
                _id = 0;
            }
            if (_mountModelGo != null)
            {
                Destroy(_mountModelGo);
                _mountModelGo = null;
            }
        }

        protected void OnModelPrefabLoaded(ACTActor actor)
        {
            _playerActor = actor;
            GameObject go = actor.gameObject;
            if (go != null && go != _modelGo)
            {
                ClearModelGameObject();
                _modelGo = go;
                _dragComponent.SetModel(_modelGo);
                actor.actorController.isStatic = true;
                SetModelSetting();
                go.transform.SetParent(transform, false);
                sortOrderedRenderAgent = go.AddComponentOnlyOne<SortOrderedRenderAgent>();
                SortOrderedRenderAgent.ReorderAll();
                ModelComponentUtil.ChangeUIModelParam(go);
            }
        }

        private void SetModelSetting()
        {
            var characterController = _modelGo.GetComponent<CharacterController>();
            characterController.enabled = false;
            float Scale = 1;
            if (_rectTransform != null)
            {
                Scale = _scale * _rectTransform.sizeDelta.y / characterController.height;
                _modelGo.transform.localPosition = new Vector3(0.5f * _rectTransform.sizeDelta.x, 0, 0) + _startPosition;
            }
            else
            {
                _modelGo.transform.localPosition = _startPosition;
            }
            //float Scale = _scale * _rectTransform.sizeDelta.y / characterController.height;
            _modelGo.transform.localScale = new Vector3(Scale, Scale, Scale);
            _modelGo.transform.localRotation = Quaternion.Euler(_rotation);
            //go.transform.localPosition = new Vector3(0.5f * _rectTransform.sizeDelta.x, 0, 0) + _startPosition;
        }

        public void SetStartSetting(Vector3 position, Vector3 euler, float scale)
        {
            _startPosition = position;
            _rotation = euler;
            _scale = scale;
            if(_modelGo)
            {
                SetModelSetting();
            }
        }

        public void PlayFx(string path, Vector3 relativePos, float duration)
        {
            if (_modelGo != null)
            {
                Vector3 position = _modelGo.transform.position + relativePos;
                ACTVisualFXManager.GetInstance().PlayACTVisualFX(path, position, Vector3.zero, duration, 0, VisualFXPriority.H, sortOrderedRenderAgent.order + 1);
            }
        }
    }


}
