﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using Common.ClientConfig;
using GameLoader.LoadingBar;
using ShaderUtils;
using GameLoader.Utils;

namespace GameMain
{
    public class ImagesProviderProxy : MonoBehaviour
    {
        private ImageWrapper[] _imageWrappers;
        public ImageWrapper[] imageWrappers
        {
            get
            {
                if (_imageWrappers == null)
                {
                    var trans = transform;
                    _imageWrappers = new ImageWrapper[trans.childCount];
                    for (int i = 0; i < trans.childCount; i++)
                    {
                        GameObject childGo = trans.GetChild(i).gameObject;
                        var image = childGo.GetComponent<ImageWrapper>();
                        _imageWrappers[i] = image;
                    }
                }
                return _imageWrappers;
            }
        }

        private Dictionary<string, ImageWrapper> _imageWrappersDict;
        public Dictionary<string, ImageWrapper> imageWrappersDict
        {
            get
            {
                if (_imageWrappersDict == null)
                {
                    var trans = transform;
                    _imageWrappersDict = new Dictionary<string, ImageWrapper>(trans.childCount);
                    for (int i = 0; i < trans.childCount; i++)
                    {
                        GameObject childGo = trans.GetChild(i).gameObject;
                        var image = childGo.GetComponent<ImageWrapper>();
                        _imageWrappersDict[childGo.name] = image;
                    }
                }
                return _imageWrappersDict;
            }
        }

        void Awake()
        {
            transform.localScale = Vector3.zero;
        }

        public static ImagesProviderProxy GetProxy(ImagesProvider imagesProvider)
        {
            ImagesProviderProxy result = null;
            if (imagesProvider == null) return result;
            var itemRoot = imagesProvider.itemsRoot;
            if (itemRoot != null)
            {
                result = itemRoot.GetComponent<ImagesProviderProxy>();
                if (result == null)
                {
                    result = itemRoot.AddComponent<ImagesProviderProxy>();
                }
            }
            return result;
        }
    }
}
