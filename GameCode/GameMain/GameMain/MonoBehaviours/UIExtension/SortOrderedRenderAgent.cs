﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameMain.GlobalManager;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using GameLoader.Utils;

namespace GameMain
{
    public class SortOrderedRenderAgent : MonoBehaviour
    {
        public static bool refreshByPanels = true;
        private static GameObject _uiRoot = null;
        private static int _order = 0;
        private static bool _refreshAllFlag = false;
        private static bool _forceSortAll = false;
        private static ChildrenComponentsPool<List<Renderer>> _rendererListPool = new ChildrenComponentsPool<List<Renderer>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<SortOrderedRenderAgent>> _agentListPool = new ChildrenComponentsPool<List<SortOrderedRenderAgent>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<Canvas>> _canvasListPool = new ChildrenComponentsPool<List<Canvas>>(null, l => l.Clear());

        public int order = 0;
        private Canvas _canvas;
        
        protected void Awake()
        {
            if (_uiRoot == null) _uiRoot = this.gameObject;
            List<Canvas> canvasList = _canvasListPool.Get();
            GetComponentsInChildren(true, canvasList);
            if(canvasList.Count>0)
            {
                _canvas = canvasList[0];
            }
        }

        protected void OnDestroy()
        {
            _canvas = null;
        }

        protected void LateUpdate()
        {
            if(SortOrderedRenderAgent._refreshAllFlag == true)
            {
                SortOrderedRenderAgent._refreshAllFlag = false;
                SortOrderedRenderAgent.DoReorderAll();
                SortOrderedRenderAgent._forceSortAll = false;
            }
        }

        private void Refresh()
        {
            order = _order++;
            if (_canvas!=null)
            {
                if (_canvas.overrideSorting == false)
                {
                    _canvas.overrideSorting = true;
                }
                if (_canvas.sortingOrder != this.order)
                {
                    _canvas.sortingOrder = this.order;
                }
            }
            else
            {
                List<Renderer> rendererList = _rendererListPool.Get();
                GetComponentsInChildren(_forceSortAll, rendererList);
                for(int i = 0; i < rendererList.Count; i++)
                {
                    rendererList[i].sortingOrder = this.order;
                }
                _rendererListPool.Release(rendererList);
            }
        }

        public static void ReorderAll()
        {
            _refreshAllFlag = true;
        }

        public static void ForceReorderAll()
        {
            _forceSortAll = true;
        }

        private static void DoReorderAll()
        {
            if (refreshByPanels)
            {
                DoReorderAllByPanel();
            }
            else {
                DoReorderAllByRoot();
            }
        }

        private static void DoReorderAllByRoot()
        {
            _order = 0;
            List<SortOrderedRenderAgent> agentList = _agentListPool.Get();
            _uiRoot.GetComponentsInChildren(false, agentList);
            for (int i = 0; i < agentList.Count; i++)
            {
                agentList[i].Refresh();
            }
            _agentListPool.Release(agentList);
        }

        static List<Transform> _layers = null;
        private static List<Transform> GetLayers()
        {
            if (_layers == null)
            {
                _layers = new List<Transform>();
                Transform trans = _uiRoot.transform;
                for (int i = 0; i < trans.childCount; i++)
                {
                    _layers.Add(trans.GetChild(i));
                }
            }
            return _layers;
        }

        static List<SortOrderedRenderAgent> _sorList = new List<SortOrderedRenderAgent>();
        private static void DoReorderAllByPanel()
        {
            _order = 0;
            List<SortOrderedRenderAgent> agentList = _agentListPool.Get();
            var layers = GetLayers();
            for (int i = 0; i < layers.Count; i++)
            {
                var layer = layers[i];
                for (int j = 0; j < layer.childCount; j++)
                {
                    var panel = layer.GetChild(j);
                    if (panel.localScale.x == 0 || !panel.gameObject.activeSelf) continue;
                    agentList.Clear();
                    panel.GetComponentsInChildren(false, agentList);
                    _sorList.AddRange(agentList);
                }
            }
            _agentListPool.Release(agentList);
            for (int i = 0; i < _sorList.Count; i++)
            {
                _sorList[i].Refresh();
            }
            _sorList.Clear();
        }
    }

    
}
