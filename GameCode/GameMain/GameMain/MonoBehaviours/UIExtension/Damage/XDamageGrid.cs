using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;

namespace GameMain
{
    public class XDamageGrid : MonoBehaviour
    {
        List<XDamageItem> _damageItemList;
        Vector3 _size;
        Vector3 _floatPosition;
        int _height;
        bool _isUse;
        int _randOffX;
        Transform _gridTransform;
        int _gridIndex;
        bool _isPlayer;

        void Awake()
        {
            this._damageItemList = new List<XDamageItem>();
            this._size = Vector3.zero;
            this._floatPosition = Vector3.zero;
            this._height = 1;  
            this._isUse = false;
            this._randOffX = 0;
        }

        public void AddNumberItem(XDamageItem damageItem)
        {
            if (damageItem == null ) 
            {
                return;
            }
            _damageItemList.Add(damageItem);
            this._size.x = this._size.x + damageItem.rectTransform.sizeDelta.x;
            this._size.y = Mathf.Max(damageItem.rectTransform.sizeDelta.y, this._size.y);
        }
        
        public void Show(Vector3 position, int type)
        {
            this._floatPosition = position;
            this._isUse = true;
            this.InitValue();
            for (int i = 0;i<this._damageItemList.Count;i++)
            {
                var item = this._damageItemList[i];
                item.Show(item.GetPosition(), type, this.OnEnd);
            }
        }
        
        public void SetColor(Color color)
        {
            for (int i = 0;i<this._damageItemList.Count;i++)
            {
                var item = this._damageItemList[i];
                item.SetColor(color);
            }

        }

        void InitValue()
        {
            var damagePosition = this._floatPosition + new Vector3(0, this._height, 0);
            var screenPosition = Camera.main.WorldToScreenPoint(damagePosition);
            var localPosition = screenPosition;
            float scaleWidth = (float)ScreenUtils.ScreenWidth/ ScreenUtil.PANEL_WIDTH;
            float scaleHeight = (float)ScreenUtils.ScreenHeight / ScreenUtil.PANEL_HEIGHT;
            localPosition.x = screenPosition.x / scaleWidth;
            localPosition.y = screenPosition.y / scaleHeight;
            this.SetStartPosition(localPosition);
            this.SetStartScale();
            this.SetStartAlpha();
        }


        void OnEnd()
        {
            if (this._isUse){
                this._isUse = false;
            }
            else{
                return;
            }
            XDamageHandler.instance.RemoveItemFormList(this._damageItemList);
            this.RemoveAllItem();
            XDamageHandler.instance.RemoveDamageGrid(this);
        }
    

        void SetStartScale()
        {
            var sizeX = this._size.x;
            for (int i = 0; i < this._damageItemList.Count; i++)
            {
                var item = this._damageItemList[i];
                item.SetScale(Vector3.one);
                item.SetPivot(new Vector2(sizeX / item.GetSize().x * 0.5f, 0.5f));
                sizeX = sizeX - item.GetSize().x * 2;
                var pos = item.GetPosition() + new Vector3(item.GetSize().x * item.GetPivot().x, 0, 0);
                item.SetPosition(pos);
            }
        }

        void SetStartPosition(Vector3 localPosition)
        {
            this._randOffX = UnityEngine.Random.Range(-35,35);
            localPosition.x = localPosition.x + this._randOffX;
            localPosition.z = 0;
            var startX = localPosition.x - this._size.x / 2;
            var pos = Vector3.zero;
            var povit = new Vector2(0f, 0.5f);
            for (int i = 0; i < _damageItemList.Count; i++)
            {
                var item = this._damageItemList[i];
                item.SetPivot(povit);
                pos.x = startX;
                pos.y = localPosition.y;
                item.SetPosition(pos);
                startX = startX + item.GetSize().x;
            }
        }

        void SetStartAlpha()
        {
            for (int i = 0; i < this._damageItemList.Count; i++) 
            {
                var item = this._damageItemList[i];
                item.SetAlpha(1);
            }
        }

        public void Hide()
        {
            this._floatPosition = Vector3.zero;
        }

        void RemoveAllItem()
        {
            this._damageItemList.Clear();
            this._size = Vector3.zero;
        }

        public void SetTransform(Transform trans)
        {
            this._gridTransform = trans;
        }

        public Transform GetTransform()
        {
            return this._gridTransform;
        }

        public void SetGridIndex(int index)
        {
            this._gridIndex = index;
        }

        public int GetGridInde()
        {
            return this._gridIndex;
        }

        public bool GetIsPlayer()
        {
            return this._isPlayer;
        }

        public void SetIsPlayer(bool isPlayer)
        {
            this._isPlayer = isPlayer;
        }
    }
}
