using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using GameLoader.Utils.Timer;
using GameMain;

namespace GameMain
{
    public class XDamageItem : MonoBehaviour
    {
        public string Name;
        RectTransform _rectTransform;
        Transform _itemTransform;
        XImageFloat _imageFloat;
        GameObject _go;


        XGameObjectTweenPosition _tweenPosition;
        XGameObjectTweenScale _tweenScale;
        XImageTweenAlpha _tweenAlpha;
        ImageWrapper _imageNumber;
        Vector3 _position;
        Action _callback;
        float _moveDistance;

        float _moveTimer = 0;
        float _hideTimer = 0;
        float _littleTimer = 0;
        float _endCallbackTimer = 0;

        public RectTransform rectTransform
        {
            get { return _rectTransform; }
        }

        void Awake()
        {
            _go = this.gameObject;
            this._imageFloat = _go.AddComponent<XImageFloat>();
            this._imageNumber = this.GetComponent<ImageWrapper>(); //this.GetChildComponent('', "ImageWrapper");
            this._rectTransform = this.GetComponent<RectTransform>();

            
            this._itemTransform = this.transform;
            this.Name = "";
            this._tweenPosition = _go.AddComponent<XGameObjectTweenPosition>();
            this._tweenScale = _go.AddComponent<XGameObjectTweenScale>();
            this._tweenAlpha = _go.AddComponent<XImageTweenAlpha>();
            this._tweenPosition.enabled = false;
            this._tweenScale.enabled = false;
            this._tweenAlpha.enabled = false;
            this.InitVars();
        }

        void InitVars()
        {
           
        }

        public void Show(Vector3 position, int type, Action callBack)
        {
            this._imageFloat.StartFloat(position, type, callBack);
        }

        public void Hide()
        {
            this._rectTransform.localPosition = XDamageHandler.HIDE_POSITION;
        }

        public void SetScale(Vector3 scale)
        {
            this._rectTransform.localScale = scale;
        }

        public Vector3 GetScale()
        {
            return this._rectTransform.localScale;
        }

        public void SetPivot(Vector2 pivot)
        {
            this._rectTransform.pivot = pivot;
        }

        public Vector2 GetPivot()
        {
            return this._rectTransform.pivot;
        }

        public void SetPosition(Vector3 position)
        {
            this._rectTransform.localPosition = position;
        }

        public Vector3 GetPosition()
        {
            return this._rectTransform.localPosition;
        }

        public Vector2 GetSize()
        {
            return this._rectTransform.sizeDelta;
        }


        public void SetSprite(Sprite sprite)
        {
            this._imageNumber.sprite = sprite;
            this._imageNumber.SetNativeSize();
        }

        public void SetAlpha(float alpha)
        { 
            this._imageFloat.SetAlpha(alpha);
        }


        public void SetColor(Color color)
        {
            if (this._imageNumber != null)
            {
                this._imageNumber.color = color;
                this._imageNumber.SetAllDirty();
            }
        }

        public Transform GetTransform()
        {
            return this._itemTransform;
        }
    }
}
