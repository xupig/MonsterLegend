﻿
using UIExtension;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using MogoEngine.RPC;

namespace GameMain
{
    public class XHPBar : XContainer
    {
        private ImageWrapper _imageSlow = null;
        private ImageWrapper _imageFast = null;
        private XImageFilling _imageSlowFilling = null;

        private static readonly string path_slow = "Container_Bar/Image_Slow";
        private static readonly string path_fast = "Container_Bar/Image_Fast";


        uint _entityId = 0;
        EntityCreature _entity = null;
        protected override void Awake()
        {
            _imageSlow = this.GetChildComponent<ImageWrapper>(path_slow);
            _imageFast = this.GetChildComponent<ImageWrapper>(path_fast);
            _imageSlowFilling = this.AddChildComponent<XImageFilling>(path_slow);
        }


        public void AddHPListener(uint entityId)
        {
            if (entityId == 0) return;
            _entityId = entityId;
            Entity entity = EntityMgr.Instance.GetEntity(entityId);
            if (entity != null)
            {
                _entity = entity as EntityCreature;
                EntityPropertyManager.Instance.AddEventListener(entity, EntityPropertyDefine.max_hp, OnHPChange);
                EntityPropertyManager.Instance.AddEventListener(entity, EntityPropertyDefine.cur_hp, OnHPChange);
                OnHPChange();
            }
        }

        public void RemoveHPListener()
        {
            if (_entityId == 0) return;
            Entity entity = EntityMgr.Instance.GetEntity(_entityId);
            if (entity != null)
            {
                EntityPropertyManager.Instance.RemoveEventListener(entity, EntityPropertyDefine.max_hp, OnHPChange);
                EntityPropertyManager.Instance.RemoveEventListener(entity, EntityPropertyDefine.cur_hp, OnHPChange);
                _entityId = 0;
                _entity = null;
            }
        }

        public void OnHPChange()
        {
            if (_entity != null && _entity.id !=0)
            {
                float max_hp = _entity.max_hp;
                if(max_hp != 0)
                {
                    float cur_hp = _entity.cur_hp;
                    SetHPValue(cur_hp/max_hp);
                }
            }
        }
        
        private void SetHPValue(float value)    
        {
            _imageFast.fillAmount = value;
            _imageSlowFilling.SetValue(value);
        }

    }
}
