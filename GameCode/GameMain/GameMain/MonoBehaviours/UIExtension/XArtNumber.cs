﻿using GameData;
using GameLoader.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    public class XArtNumber : XContainer
    {
        private string _cacheContent = string.Empty;
        private Dictionary<char, Transform> _numberDict;
        private float _xGap = 0f;
        private GameObject _head;
        private GameObject _end;
        private List<Transform> _showNumbers;
        private Transform _tran;
        private bool _useObjectPool = true;
        private Transform _poolTransform;
        private Dictionary<char, Stack<Transform>> _objectPool;

        protected override void Awake()
        {
            InitHeadEnd();
            InitNumberDict();
            InitObjectPool();
            _showNumbers = new List<Transform>();
            _tran = transform;
        }

        private void InitHeadEnd()
        {
            _head = GetChild("head");
            if (_head != null) _head.AddComponent<Locater>();
            _end = GetChild("end");
            if (_end != null) _end.AddComponent<Locater>();
        }

        private void InitNumberDict()
        {
            _numberDict = new Dictionary<char, Transform>();
            Transform _templateTran = GetChild("item").transform;
            _templateTran.gameObject.SetActive(true);
            for (int i = 0; i < _templateTran.childCount; i++)
            {
                GameObject childGo = _templateTran.GetChild(i).gameObject;
                string childName = childGo.name;
                char mark = childName[childName.Length - 1];
                _numberDict.Add(mark, childGo.transform);
                _templateTran.localScale = Vector3.zero;
                //childGo.SetActive(false);
            }
        }

        private void InitObjectPool()
        {
            if (_poolTransform == null)
            {
                var go = new GameObject("pool");
                var rect = go.AddComponent<RectTransform>();
                rect.anchoredPosition = Vector2.zero;
                //go.SetActive(false);
                _poolTransform = go.transform;
                _poolTransform.SetParent(this.transform, false);
                _poolTransform.localScale = Vector3.zero;
                _objectPool = new Dictionary<char, Stack<Transform>>();
            }
        }

        public void UseObjectPool(bool value)
        {
            this._useObjectPool = value;
            if (this._useObjectPool)
            {
                InitObjectPool();
            }
        }

        public void SetXGap(float value)
        {
            _xGap = value;
        }

        public void SetNumberDontChangeY(int value)
        {
            SetString(value.ToString(), false);
        }

        public void SetNumber(int value)
        {
            SetString(value.ToString());
        }

        public void SetString(string content)
        {
            SetString(content, true);
        }

        private void SetString(string content, bool changeY)
        {
            try
            {
                for (int i = 0; i < content.Length; i++)
                {
                    if (_cacheContent.Length > i)
                    {
                        if (_cacheContent[i].Equals(content[i]))
                        {
                            continue;
                        }
                        else
                        {
                            UpdateNumber(i, content[i]);
                        }
                    }
                    else
                    {
                        UpdateNumber(i, content[i]);
                    }
                }
                _cacheContent = content;
                for (int i = _showNumbers.Count - 1; i >= content.Length; i--)
                {
                    ReleaseObject(_showNumbers[i]);
                    _showNumbers.RemoveAt(i);
                }
                AdjustNumberPostion(changeY);
            }
            catch (System.Exception ex)
            {
                LoggerHelper.Except(ex, "content: " + content);
            }
        }

        private void AdjustNumberPostion(bool changeY = true)
        {
            Vector2 startVector = Vector2.zero;
            RectTransform rectTran = GetComponent<RectTransform>();
            Locater locater;
            if (_head != null)
            {
                locater = _head.GetComponent<Locater>();
                float y = 0;
                if (changeY)
                {
                    y = -(rectTran.rect.height - locater.Height) * 0.5f;
                }
                locater.Position = new Vector2(startVector.x, y);
                startVector += new Vector2(locater.Width + _xGap, 0);
            }
            for (int i = 0; i < _showNumbers.Count; i++)
            {
                //_showNumbers[i].SetActive(true);
                locater = _showNumbers[i].GetComponent<Locater>();
                float y = 0;
                if (changeY)
                {
                    y = -(rectTran.rect.height - locater.Height) * 0.5f;
                }
                locater.Position = new Vector2(startVector.x, y);
                startVector += new Vector2(locater.Width + _xGap, 0);
            }
            if (_end != null)
            {
                locater = _end.GetComponent<Locater>();
                float y = 0;
                if (changeY)
                {
                    y = -(rectTran.rect.height - locater.Height) * 0.5f;
                }
                locater.Position = new Vector2(startVector.x, y);
                startVector += new Vector2(locater.Width + _xGap, 0);
            }
            rectTran.sizeDelta = new Vector2(startVector.x, rectTran.sizeDelta.y);
        }

        private void UpdateNumber(int index, char mark)
        {
            if (_showNumbers.Count > index)
            {
                ReleaseObject(_showNumbers[index]);
                Transform numberTrans = CreateObject(mark);
                numberTrans.SetParent(_tran);
                numberTrans.localScale = Vector3.one;
                _showNumbers[index] = numberTrans;
            }
            else
            {
                Transform numberTrans = CreateObject(mark);
                numberTrans.SetParent(_tran);
                numberTrans.localScale = Vector3.one;
                _showNumbers.Add(numberTrans);
            }
        }

        private Transform CreateObject(char mark)
        {
            Transform result = null;
            if (this._useObjectPool)
            {
                if (!_objectPool.ContainsKey(mark)) _objectPool[mark] = new Stack<Transform>();
                var stack = _objectPool[mark];
                if (stack.Count > 0)
                {
                    result = stack.Pop();
                }
            }
            if (result == null)
            {
                result = GameObject.Instantiate<Transform>(_numberDict[mark]);
                result.name = _numberDict[mark].name;
                result.SetParent(_tran);
                result.localPosition = Vector3.zero;
                result.localScale = Vector3.one;
                result.gameObject.AddComponent<Locater>();
            }
            return result;
        }

        private void ReleaseObject(Transform trans)
        {
            if (this._useObjectPool)
            {
                string goName = trans.name;
                char mark = goName[goName.Length - 1];
                if (!_objectPool.ContainsKey(mark)) _objectPool[mark] = new Stack<Transform>();
                var stack = _objectPool[mark];

                trans.SetParent(_poolTransform, false);
                stack.Push(trans);
            }
            else
            {
                GameObject.Destroy(trans.gameObject);
            }
        }
    }
}
