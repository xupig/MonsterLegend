﻿using Common.Utils;
using GameResource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    public class XIconAnimOnceProxy : XIconAnimProxyBase
    {
        public float second;  //代表完成动画需要多少秒
        public float changeOneTime = 0;
        public float nowSecond = 0;
        public bool ignoreTimeScale = false; //是否忽略缓动动画

        private int _spriteIndex = 0;

        protected void Update()
        {
            if ((sprites == null || sprites.Length <= _spriteIndex))
            {
                return;
            }

            if (changeOneTime != 0)
            {
                nowSecond += ignoreTimeScale ? RealTime.deltaTime : UnityPropUtils.deltaTime;
                if (changeOneTime * (_spriteIndex + 1) >= nowSecond)
                {
                    image.sprite = sprites[_spriteIndex];
                    _spriteIndex++;
                }
            }
        }
    }

    public class XIconAnimOnce : XIcon
    {
        private float _second;  //代表完成动画需要多少秒
        private float _changeOneTime = 0;
        private float _nowSecond = 0;
        private string[] _spriteNameList;
        private XIconAnimOnceProxy _spritesProxy;
        private bool _ignoreTimeScale = false; //是否忽略缓动动画
        /// <summary>
        /// 是否忽略缓动动画
        /// </summary>
        /// <param name="value"></param>
        public void SetIgnoreTimeScale(bool value)
        {
            _ignoreTimeScale = value;
        }

        public void SetSprites(string[] spriteNameList, float second)
        {
            _second = second;
            _spriteNameList = spriteNameList;
            if (spriteNameList.Length > 0)
            {
                _nowSecond = 0;
                SetSprite(_spriteNameList[0], Color.white);
                _changeOneTime = second / spriteNameList.Length;
                this.gameObject.SetActive(true);
            }
        }

        protected override void DoResetSprite()
        {
            base.DoResetSprite();
            if (_atlasPrefab != null)
            {
                _spritesProxy = GameSpriteAnimationUtils.GetSpritesProxy<XIconAnimOnceProxy>(_atlasPrefab, _spriteNameList);
                _spritesProxy.second = _second;
                _spritesProxy.changeOneTime = _changeOneTime;
                _spritesProxy.nowSecond = _nowSecond;
                _spritesProxy.ignoreTimeScale = _ignoreTimeScale;
            }
        }
    }
}
