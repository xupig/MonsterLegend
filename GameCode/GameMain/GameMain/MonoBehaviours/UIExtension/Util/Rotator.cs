﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class Rotator : UIBehaviour
    {
        private RectTransform _rect;
        private float _angleZ;
        public float foo;

        protected override void Awake()
        {
            base.Awake();
            _rect = GetComponent<RectTransform>();
            Vector2 position = _rect.anchoredPosition;
            _rect.anchoredPosition = new Vector2(position.x + _rect.sizeDelta.x * 0.5f, position.y - _rect.sizeDelta.y * 0.5f);
            _rect.pivot = new Vector2(0.5f, 0.5f);
        }

        public float angleZ
        {
            get
            {
                return _angleZ;
            }
            set
            {
                _angleZ = value % 360.0f;
                RotateZ(_angleZ);
            }
        }

        private void RotateZ(float angle)
        {
            _rect.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
}
