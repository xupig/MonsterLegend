﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLoader.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace GameMain
{
    public class SortOrder : SortOrderBase
    {
        private static int _order = 0;
        private static bool _refreshAllFlag = false;
        private static ChildrenComponentsPool<List<Renderer>> _rendererListPool = new ChildrenComponentsPool<List<Renderer>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<SortOrder>> _agentListPool = new ChildrenComponentsPool<List<SortOrder>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<Canvas>> _canvasListPool = new ChildrenComponentsPool<List<Canvas>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<GraphicRaycaster>> _raycasterListPool = new ChildrenComponentsPool<List<GraphicRaycaster>>(null, l => l.Clear());


        private Canvas _canvas;
        private GraphicRaycaster _raycaster;


        protected new void Awake()////为排除警告，加回new
        {
            List<Canvas> canvasList = _canvasListPool.Get();
            GetComponentsInChildren(true, canvasList);
            if (canvasList.Count > 0)
            {
                _canvas = canvasList[0];
                List<GraphicRaycaster> raycasterList = _raycasterListPool.Get();
                GetComponentsInChildren(true, raycasterList);
                _raycaster = raycasterList[0];
            }
        }

        protected void OnDestroy()
        {
            _canvas = null;
            _raycaster = null;
        }

        protected  new void LateUpdate()//为排除警告，加回new
        {
            if(_refreshAllFlag == true)
            {
                _refreshAllFlag = false;
                DoRebuildAll();
            }
        }

        private void Refresh()
        {
            order = _order++;
            if (_canvas!=null)
            {
                if (_canvas.overrideSorting == false)
                {
                    _canvas.overrideSorting = true;
                }
                if (_canvas.sortingOrder != this.order)
                {
                    _canvas.sortingOrder = this.order;
                }
            }
            else
            {
                List<Renderer> rendererList = _rendererListPool.Get();
                GetComponentsInChildren(false, rendererList);
                for(int i = 0; i < rendererList.Count; i++)
                {
                    rendererList[i].sortingOrder = this.order;
                }
                _rendererListPool.Release(rendererList);
            }
        }

        public static void RebuildAll()
        {
            _refreshAllFlag = true;
        }

        private static void DoRebuildAll()
        {
            _order = 0;
            List<SortOrder> agentList = _agentListPool.Get();
            GameObject uiRoot = GameObject.Find("UIRoot");
            uiRoot.GetComponentsInChildren(false, agentList);
            for(int i = 0; i < agentList.Count; i++)
            {
                agentList[i].Refresh();
            }
            _agentListPool.Release(agentList);
        }

        public static void ShowOrder()
        {
            GameObject uiRoot = GameObject.Find("UIRoot");
            Canvas[] canvases = uiRoot.GetComponentsInChildren<Canvas>();
            if (canvases != null && canvases.Length != 0)
            {
                foreach (Canvas canvas in canvases)
                {
                    LoggerHelper.Error(canvas.gameObject.name+"---canvas sortOrder---"+canvas.sortingOrder);
                }
            }
            else
            {
                Renderer[] renderers = uiRoot.GetComponentsInChildren<Renderer>();
                foreach (Renderer renderer1 in renderers)
                {
                    LoggerHelper.Error(renderer1.gameObject.name+"---renderer sortOrder---"+renderer1.sortingOrder);
                }
            }
            
        }
    }
}
