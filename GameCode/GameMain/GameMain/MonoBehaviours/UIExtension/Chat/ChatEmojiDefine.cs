﻿using System.Collections.Generic;
using GameData;

namespace GameMain
{
    public class ChatEmojiDefine
    {
        public const float EMOJI_WIDTH = 28.0f;
        public const float EMOJI_HEIGHT = 28.0f;

        //private static List<string> _emojiFirstSpriteNameList;
        //private static Dictionary<string, int[]> _emojiSpriteIndexListDict;
        private static Dictionary<string, string[]> _emojiSpriteNameListDict = new Dictionary<string, string[]>();

        static ChatEmojiDefine()
        { 
          /*  _emojiSpriteIndexListDict = new Dictionary<string, int[]>();
            _emojiSpriteIndexListDict.Add("aixiu", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8 });
            _emojiSpriteIndexListDict.Add("beifen", new int[] { 1, 1, 1, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("bishi", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("daxiao", new int[] { 1, 1, 1, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("ding", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("emo", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5 });
            _emojiSpriteIndexListDict.Add("jiong", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5 });
            _emojiSpriteIndexListDict.Add("koubikong", new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7 });
            _emojiSpriteIndexListDict.Add("ku", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8 });
            _emojiSpriteIndexListDict.Add("leiben", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 });
            _emojiSpriteIndexListDict.Add("leng", new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3 });
            _emojiSpriteIndexListDict.Add("maren", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 });
            _emojiSpriteIndexListDict.Add("meigui", new int[] { 1, 1, 1, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("nansheng", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("nvsheng", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("songhua", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 });
            _emojiSpriteIndexListDict.Add("touxiao", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("touxiang", new int[] { 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2 });
            _emojiSpriteIndexListDict.Add("enai", new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 });

            _emojiFirstSpriteNameList = new List<string>();
            foreach (string key in _emojiSpriteIndexListDict.Keys)
            {
                _emojiFirstSpriteNameList.Add(key);
            }*/
        }

        public static List<string> GetEmojiFirstSpriteNameList()
        {
            return chatemoji_helper.GetEmojiFirstSpriteNameList();//  _emojiFirstSpriteNameList;
        }

        public static string[] GetEmojiSpriteNameList(int index)
        {
            List<string> names = chatemoji_helper.GetEmojiFirstSpriteNameList();
            if (index >= names.Count)
            {
                index = 1;
            }
            string firstName = names[index];
            return GetEmojiSpriteNameList(firstName);
        }

        public static string[] GetEmojiSpriteNameList(string name)
        {
            if (_emojiSpriteNameListDict.ContainsKey(name) == true)
            {
                return _emojiSpriteNameListDict[name];
            }
            List<int> indexList = chatemoji_helper.GetSpriteIndexListDict()[name];
            string[] result = new string[indexList.Count];
            for (int i = 0; i < indexList.Count; i++)
            {
                result[i] = string.Format("{0}00{1}", name, indexList[i].ToString());
            }
            _emojiSpriteNameListDict.Add(name, result);
            return result;
        } 

        public static float GetEmojiWidth(int index)
        {
            if (index == chatemoji_helper.GetEmojiFirstSpriteNameList().Count - 1)
            {
                return EMOJI_WIDTH * 2;
            }
            return EMOJI_WIDTH;
        }

        public static float GetEmojiHeight(int index)
        {
            return EMOJI_HEIGHT;
        }

        public static int GetEmojiIndex(string name)
        {
            int index = 0;
            for (int i = 0; i < chatemoji_helper.GetEmojiFirstSpriteNameList().Count; i++)
            {
                if (name == chatemoji_helper.GetEmojiFirstSpriteNameList()[i])
                {
                    index = i;
                    break;
                }
            }
            return index;
        }
    }
}
