﻿using UnityEngine;
//using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class HtmlHelper
    {
        private Regex _leftTagPattern = new Regex(@"(?<color><color=#[a-f0-9]{6}>)|(?<size><size=\d+>)|(?<b><b>)|(?<i><i>)", RegexOptions.IgnoreCase);
        private Regex _rightTagPattern = new Regex(@"(?<color></color>)|(?<size></size>)|(?<b></b>)|(?<i></i>)", RegexOptions.IgnoreCase);

        private string[] _matchGroupNames;
        private Stack<Match> _blockTagStack;

        public HtmlHelper()
        {
            _matchGroupNames = _leftTagPattern.GetGroupNames();
            _blockTagStack = new Stack<Match>();
        }

        public bool Validate(string content)
        {
            List<Match> matchList = GetTagMatchList(content);
            return CheckTagMatchList(matchList);
        }

        private List<Match> GetTagMatchList(string content)
        {
            MatchCollection leftMc = _leftTagPattern.Matches(content);
            MatchCollection rightMc = _rightTagPattern.Matches(content);
            List<Match> matchList = new List<Match>();
            int i = 0, j = 0;
            while (i < leftMc.Count && j < rightMc.Count)
            {
                if (leftMc[i].Index < rightMc[j].Index)
                {
                    matchList.Add(leftMc[i]);
                    i++;
                }
                else
                {
                    matchList.Add(rightMc[j]);
                    j++;
                }
            }
            while (i < leftMc.Count)
            {
                matchList.Add(leftMc[i]);
                i++;
            }
            while (j < rightMc.Count)
            {
                matchList.Add(rightMc[j]);
                j++;
            }
            return matchList;
        }

        private bool CheckTagMatchList(List<Match> matchList)
        {
            bool result = true;
            Stack<Match> tagStack = new Stack<Match>();
            foreach (Match m in matchList)
            {
                if (_rightTagPattern.IsMatch(m.Value) == true)
                {
                    if (tagStack.Peek() != null && GetMatchGroupIndex(tagStack.Peek()) == GetMatchGroupIndex(m))
                    {
                        tagStack.Pop();
                    }
                    else
                    {
                        Debug.LogError(string.Format("Error:  Unmatched right tag[index {0}, value {1}] found!!", m.Index, m.Value));
                        return false;
                    }
                }
                else
                {
                    tagStack.Push(m);
                }
            }
            while (tagStack.Count > 0)
            {
                Match m = tagStack.Pop();
                Debug.LogError(string.Format("Error:  Unmatched left tag[index {0}, value {1}] found!!", m.Index, m.Value));
                result = false;
            }
            return result;
        }

        private int GetMatchGroupIndex(Match match)
        {
            for (int i = 1; i < _matchGroupNames.Length; i++)
            {
                if (string.IsNullOrEmpty(match.Groups[_matchGroupNames[i]].Value) == false)
                {
                    return i;
                }
            }
            return -1;//never reached
        }

        private string GetMatchGroupName(Match match)
        {
            return _matchGroupNames[GetMatchGroupIndex(match)];
        }

        public string GetLeftTag()
        {
            if (_blockTagStack.Count == 0)
            {
                return string.Empty;
            }
            Match[] matchList = new Match[_blockTagStack.Count];
            _blockTagStack.CopyTo(matchList, 0);
            StringBuilder sb = new StringBuilder();
            for (int i = matchList.Length - 1; i >= 0; i--)
            {
                sb.Append(matchList[i].Value);

            }
            return sb.ToString();
        }

        public string GetRightTag()
        {
            if (_blockTagStack.Count == 0)
            {
                return string.Empty;
            }
            Match[] matchList = new Match[_blockTagStack.Count];
            _blockTagStack.CopyTo(matchList, 0);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matchList.Length; i++)
            {
                sb.Append(string.Format("</{0}>", GetMatchGroupName(matchList[i])));
            }
            return sb.ToString();
        }

        public void UpdateTagMatchStack(string elementFragmentContent)
        {
            List<Match> matchList = GetTagMatchList(elementFragmentContent);
            foreach (Match m in matchList)
            {
                if (_rightTagPattern.IsMatch(m.Value) == true)
                {
                    try
                    {
                        if (_blockTagStack.Peek() != null && GetMatchGroupIndex(_blockTagStack.Peek()) == GetMatchGroupIndex(m))
                        {
                            _blockTagStack.Pop();
                        }
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        Debug.LogError("UpdateTagMatchStack elment fragment content = " + elementFragmentContent);
                        break;
                    }
                }
                else
                {
                    _blockTagStack.Push(m);
                }
            }
        }

        public void Reset()
        {
            _blockTagStack.Clear();
        }
    }

}
