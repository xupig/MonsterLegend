﻿using Common.Utils;
using UnityEngine;

namespace GameMain
{
    public class ChatEmojiItem : XListItemBase
    {
        private string _data;
        private GameObject _holder;

        protected override void Awake()
        {

        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = (string)value;
                Refresh();
            }
        }

        private void Refresh()
        {
            int index = ChatEmojiDefine.GetEmojiIndex(_data);
            float width = ChatEmojiDefine.GetEmojiWidth(index);
            float height = ChatEmojiDefine.GetEmojiHeight(index);
            //GameLoader.Utils.LoggerHelper.Error("data" + _data + " " + index);
            _holder = GameSpriteAnimationUtils.AddAnimation(this.gameObject, ChatEmojiDefine.GetEmojiSpriteNameList(_data), width, height);
        }

        public void SetAnimation(bool value)
        {
            if (_holder != null)
            {
                GameSpriteAnimationUtils.SetSpritePlayerAnimation(_holder, value);
                _holder.SetActive(value);
            }
        }

        public override void Dispose()
        {

        }
    }
}
