﻿using UnityEngine;
using Common.Data;

namespace GameMain
{
    public class ChatSharePosLinkWrapper : ChatLinkBaseWrapper
    {
        public string desc;
        public int mapId;
        public int mapLine;
        public Vector3 position;
        public int type;

        private const string SHARE_POS_LINK_TEMPLATE = "[{0};{1};{2};{3};{4};{5};{6}]";

        public static ChatSharePosLinkWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatSharePosLinkWrapper(dataStringSplits);
        }

        public ChatSharePosLinkWrapper(string[] dataStringSplits)
            : base(ChatLinkType.SharePosition, dataStringSplits)
        {

        }

        public ChatSharePosLinkWrapper(string desc, int mapId, int mapline, Vector3 position, int type = 0)
            : base(ChatLinkType.SharePosition)
        {
            this.desc = desc;
            this.mapId = mapId;
            this.mapLine = mapline;
            this.position = position;
            this.type = type;
            LinkDesc = desc;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            this.mapId = int.Parse(dataStringSplits[1]);
            this.mapLine = int.Parse(dataStringSplits[2]);
            float x = float.Parse(dataStringSplits[3]);
            float y = float.Parse(dataStringSplits[4]);
            float z = float.Parse(dataStringSplits[5]);
            this.position = new Vector3(x, y, z);
            this.type = int.Parse(dataStringSplits[6]);
        }

        public override string ToDataString()
        {
            int x = Mathf.RoundToInt(position.x);
            int y = Mathf.RoundToInt(position.y);
            int z = Mathf.RoundToInt(position.z);
            return string.Format(SHARE_POS_LINK_TEMPLATE, ((int)LinkType).ToString(), mapId.ToString(), mapLine.ToString(), x.ToString(), y.ToString(), z.ToString(), type.ToString());
        }
    }
}
