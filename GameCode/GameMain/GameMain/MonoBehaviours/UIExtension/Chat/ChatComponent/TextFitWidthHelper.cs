﻿using System.Collections.Generic;
using UnityEngine;
using GameMain;

namespace GameMain
{
    public class TextFitWidthHelper
    {
        private TextGenerator _textGenerator;
        private TextGenerationSettings _textGenerationSettings;
        private HtmlHelper _htmlHelper = new HtmlHelper();

        private int _lastCharIndex = 0;
        private float _fitSumWidth = 0f;   //文本适应时累计的宽度值
        private bool _hasInvalidChar = false;   //如果存在不可用字符，表示可能存在富文本标签

        public TextFitWidthHelper(TextGenerator textGenerator, TextGenerationSettings textGenerationSettings)
        {
            _textGenerator = textGenerator;
            _textGenerationSettings = textGenerationSettings;
        }

        public string GetFitContent(string content, float maxWidth)
        {
            Vector2 size = GetTextContentSize(content);
            if (size.x < maxWidth)
            {
                return content;
            }
            Reset();
            string destContent = DigestContent(content, maxWidth);
            if (_textGenerationSettings.richText == true && _hasInvalidChar == true)
            {
                _htmlHelper.Reset();
                destContent = ProcessRichTextContent(destContent);
                _htmlHelper.Reset();
            }
            return destContent;
        }

        public List<string> GetFitSplitContentList(string content, float maxWidth)
        {
            Reset();
            return SplitContentWithWidth(content, maxWidth);
        }

        private string DigestContent(string srcContent, float availableWidth)
        {
            string destContent = string.Empty;
            float width = 0;
            int index = _lastCharIndex;
            while (index < srcContent.Length && width <= availableWidth)
            {
                if (IsValidateChar(srcContent, index) == false)
                {
                    index++;
                    _hasInvalidChar = true;
                    continue;
                }
                float charWidth = _textGenerator.characters[index].charWidth;
                if ((width + charWidth) > availableWidth)
                {
                    break;
                }
                width += charWidth;
                index++;
            }
            _fitSumWidth += width;
            if (index != _lastCharIndex)
            {
                //考虑是否有换行符\n的情况
                destContent = srcContent.Substring(_lastCharIndex, index - _lastCharIndex);
                _lastCharIndex = index;
            }
            return destContent;
        }

        private bool IsValidateChar(string content, int index)
        {
            if (IsBlank(content, index) == true)
            {
                return true;
            }
            UIVertex vertex0 = _textGenerator.verts[index * 4];
            UIVertex vertex1 = _textGenerator.verts[index * 4 + 1];
            if ((vertex1.position.x - vertex0.position.x) == 0)
            {
                return false;
            }
            return true;
        }

        private bool IsBlank(string content, int index)
        {
            if (content[index] == ' ')
            {
                return true;
            }
            return false;
        }

        private List<string> SplitContentWithWidth(string srcContent, float maxWidth)
        {
            List<string> contentList = new List<string>();
            string text = srcContent;
            Vector2 size = GetTextContentSize(srcContent);
            _fitSumWidth = 0;
            _lastCharIndex = 0;
            while ((size.x - _fitSumWidth) > maxWidth)
            {
                text = DigestContent(srcContent, maxWidth);
                if (_textGenerationSettings.richText == true && _hasInvalidChar == true)
                {
                    text = ProcessRichTextContent(text);
                }
                contentList.Add(text);
            }
            if (_lastCharIndex != 0)
            {
                text = srcContent.Substring(_lastCharIndex, srcContent.Length - _lastCharIndex);
            }
            if (size.x > maxWidth)
            {
                if (_textGenerationSettings.richText == true && _hasInvalidChar == true)
                {
                    text = ProcessRichTextContent(text);
                }
            }
            contentList.Add(text);
            return contentList;
        }

        private Vector2 GetTextContentSize(string content)
        {
            return new Vector2(_textGenerator.GetPreferredWidth(content, _textGenerationSettings), _textGenerator.GetPreferredHeight(content, _textGenerationSettings));
        }

        private string ProcessRichTextContent(string content)
        {
            string text = content;
            string leftTag = _htmlHelper.GetLeftTag();
            _htmlHelper.UpdateTagMatchStack(text);
            string rightTag = _htmlHelper.GetRightTag();
            text = string.Format("{0}{1}{2}", leftTag, text, rightTag);
            return text;
        }

        private void Reset()
        {
            _lastCharIndex = 0;
            _fitSumWidth = 0;
            _hasInvalidChar = false;
        }
    }
}
