﻿
using UnityEngine;
using UnityEngine.UI;
using UIExtension;


namespace GameMain
{
    public class ChatTextElement : TextElement
    {
        public ChatTextElement(string elementContent, TextBlock textBlock)
            : base(elementContent, textBlock)
        {

        }

        protected override Text AddTextComponent(GameObject go)
        {
            return go.AddComponent<TextWrapper>();
        }
    }
}