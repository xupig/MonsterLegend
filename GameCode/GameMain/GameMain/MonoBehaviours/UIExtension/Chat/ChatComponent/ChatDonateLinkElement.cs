﻿using UnityEngine;
using UIExtension;

namespace GameMain
{
    public class ChatDonateLinkElement : ChatLinkElement
    {
        private static char[] split = new char[] { ',' };
        private ulong dbId;
        private int itemId;

        public ChatDonateLinkElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {
            string[] result = _srcContent.Split(split);

            this.Content = result[0].Substring(1, result[0].Length - 1);
            dbId = ulong.Parse(result[2]);
            itemId = int.Parse(result[3].Substring(0, result[3].Length - 1));
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            ButtonWrapper btn = go.AddComponent<ButtonWrapper>();
            btn.onClick.AddListener(OnNameClick);
        }

        private void OnNameClick()
        {
            GameLoader.Utils.LoggerHelper.Debug("ChatDonateLinkElement OnNameClick");
            /*Dictionary<int, BaseItemInfo> dict = PlayerDataManager.Instance.BagData.GetBagData(Data.BagType.ItemBag).GetAllItemInfo();
            List<BaseItemInfo> itemList = null;
            foreach (KeyValuePair<int, BaseItemInfo> kvp in dict)
            {
                if (kvp.Value != null)
                {
                    if (kvp.Value.Id == itemId && kvp.Value.UserList != null && kvp.Value.UserList.Count > 0)
                    {
                        if (itemList == null)
                        {
                            itemList = new List<BaseItemInfo>();
                        }
                        itemList.Add(kvp.Value);
                    }
                }
            }
            if (itemList == null)
            {
                MogoUtils.FloatTips(6016143);
            }
            else
            {
                PanelIdEnum.TeamDonate.Show(new object[] { dbId, itemList });
            }*/
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
