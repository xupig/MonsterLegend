﻿using UnityEngine;
using UnityEngine.UI;
using Common.Data;
using GameLoader.Utils;

namespace GameMain
{
    public class ChatSharePosLinkElement : ChatLinkElement
    {
        //content格式：[哇哈哈我在10级野外晨星山谷(坐标为可选(215,20,50))找到了宝箱,5,12345675,12,215,20,50]
        private string _desc = string.Empty;
        private string _strData = string.Empty;
        private ChatSharePosLinkWrapper _treasurePosData;

        public ChatSharePosLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            _treasurePosData = linkWrapper as ChatSharePosLinkWrapper;
            this.Content = GetLinkDesc(linkWrapper);
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnDescClick);
        }

        private void OnDescClick()
        {
            if (_treasurePosData == null)
            {
                LoggerHelper.Error("treasure share Pos Data is null ! This element content = " + _srcContent + ", block content = " + this.TextBlock.Content);
                return;
            }
            LoggerHelper.Debug("ChatSharePosLinkElement OnDescClick");
            // lhs TreasureManager.Instance.TransferToSharePosition(_treasurePosData.mapId, _treasurePosData.mapLine, _treasurePosData.position, _treasurePosData.type);
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
