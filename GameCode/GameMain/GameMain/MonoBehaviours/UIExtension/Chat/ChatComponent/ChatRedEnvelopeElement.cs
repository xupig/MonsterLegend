﻿using UnityEngine;
using Common.Data;
using GameLoader.Utils;

namespace GameMain
{
    public class ChatRedEnvelopeElement : GraphicElement
    {
        private GameObject _redEnvelopBtnGo;

        private ChatRedEnvelopeWrapper _redEnvelopeWrapper;

        public ChatRedEnvelopeElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            _redEnvelopeWrapper = linkWrapper as ChatRedEnvelopeWrapper;
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            /*
            _redEnvelopBtnGo = ChatWidgetPool.Instance.CreateWidgetGo(ChatWidgetPool.ChatWidgetTemplateId.RedEnvelope);
            _redEnvelopBtnGo.transform.SetParent(go.transform);
            _redEnvelopBtnGo.transform.localScale = Vector3.one;
            _redEnvelopBtnGo.transform.localPosition = Vector3.zero;
            if (_redEnvelopBtnGo != null)
            {
                KButton btn = _redEnvelopBtnGo.GetComponent<KButton>();
                btn.onClick.AddListener(OnClickRedEnvelopeBtn);
                RefreshRedEnvelope(btn);
            }*/
        }

      /*  private void RefreshRedEnvelope(KButton redEnvelopeBtn)
        {
            StateText playerNameText = redEnvelopeBtn.GetChildComponent<StateText>("name");
            StateText nameText = redEnvelopeBtn.GetChildComponent<StateText>("title");
            nameText.ChangeAllStateText(GetName());
            if (!string.IsNullOrEmpty(_redEnvelopeWrapper.playerName))
            {
                playerNameText.ChangeAllStateText(_redEnvelopeWrapper.playerName);
            }
            else
            {
                playerNameText.ChangeAllStateText("测试");// MogoLanguageUtil.GetContent(121119));
            }
        }*/

        private string GetName()
        {
            return _redEnvelopeWrapper.LinkDesc;
        }

        private int GetEnvelopeId()
        {
            return (int)_redEnvelopeWrapper.redEnvelopeId;
        }

        private int GetSystemInfoId()
        {
            return _redEnvelopeWrapper.systemInfoId;
        }

        private void OnClickRedEnvelopeBtn()
        {
            int envelopeId = GetEnvelopeId();
            //RedEnvelopeManager.Instance.RequestGrabRedEnvelope(envelopeId);
            LoggerHelper.Debug("ChatRedEnvelopeElement OnClickRedEnvelopeBtn");
        }

        protected override Vector2 GetFragmentSize()
        {
            if (_redEnvelopBtnGo == null)
                return Vector2.zero;
            RectTransform btnRect = _redEnvelopBtnGo.GetComponent<RectTransform>();
            return btnRect.sizeDelta;
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
