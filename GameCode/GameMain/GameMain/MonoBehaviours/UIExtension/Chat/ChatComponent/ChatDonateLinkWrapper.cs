﻿using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain
{
    public class ChatDonateLinkWrapper
    {
        public string desc;
        public string name;
        public ulong playerDbid;
        public int id;

        public ChatDonateLinkWrapper(string desc, string name, ulong playerDbid, int id)
        {
            this.desc = desc;
            this.name = name;
            this.playerDbid = playerDbid;
            this.id = id;
        }
    }
}
