﻿using UnityEngine;
using GameLoader.Utils.Timer;
using UnityEngine.UI;

namespace GameMain
{
    public class ChatQuestionTimeComponent : MonoBehaviour
    {
        private uint _endTimeStamp = 0;
        private uint _validTimerId = 0;
        private string _desc = string.Empty;
        private bool _isTextChanged = false;
        private Text _text;

        protected void Awake()
        {
            _text = this.gameObject.GetComponent<Text>();
            _desc = _text.text;
        }

        public void Init(uint endTimeStamp)
        {
            _endTimeStamp = endTimeStamp;
            if (_validTimerId == 0)
            {
                _validTimerId = TimerHeap.AddTimer(0, 1000, OnUpdateValidTime);
            }
        }

        private void OnUpdateValidTime()
        {
            /*if (_endTimeStamp <= (uint)(Global.Global.serverTimeStamp / 1000))
            {
                ResetTimer();
                _desc = "ChatConst.CHAT_QUESTION_END_TIPS"; //ChatConst.CHAT_QUESTION_END_TIPS;
                Refresh();
            }*/
        }

        private void ResetTimer()
        {
            if (_validTimerId != 0)
            {
                TimerHeap.DelTimer(_validTimerId);
                _validTimerId = 0;
            }
        }

        protected void OnEnable()
        {
            Refresh();
        }

        private void Refresh()
        {
            if (gameObject.activeInHierarchy)
            {
                if (!_isTextChanged && _text.text != _desc)
                {
                    _text.text = _desc;
                    _isTextChanged = true;
                }
            }
        }

        protected void OnDestroy()
        {
            ResetTimer();
        }
    }
}
