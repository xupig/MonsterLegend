﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUISetting 
    {
        static Dictionary<string, Type> _componentTypeMap;
        public static Type GetLuaUIComponent(string componentType)
        {
            if (_componentTypeMap == null)
            {
                _componentTypeMap = new Dictionary<string, Type>();
                _componentTypeMap["Component"] = typeof(LuaUIComponent);
                _componentTypeMap["PointableComponent"] = typeof(LuaUIPointable);
                _componentTypeMap["DragableComponent"] = typeof(LuaUIDragable);
                _componentTypeMap["PointableDragableComponent"] = typeof(LuaUIPointableDragable);
                _componentTypeMap["Panel"] = typeof(LuaUIPanel);
                _componentTypeMap["List"] = typeof(LuaUIList);
                _componentTypeMap["ComplexList"] = typeof(LuaUIComplexList);
                _componentTypeMap["PageableList"] = typeof(LuaUIPageableList);
                _componentTypeMap["ScrollView"] = typeof(LuaUIScrollView);
                _componentTypeMap["ScrollPage"] = typeof(LuaUIScrollPage);
                _componentTypeMap["Toggle"] = typeof(LuaUIToggle);
                _componentTypeMap["ToggleGroup"] = typeof(LuaUIToggleGroup);
                _componentTypeMap["InputField"] = typeof(LuaUIInputField);
                _componentTypeMap["LinkTextMesh"] = typeof(LuaUILinkTextMesh);
                _componentTypeMap["ProgressBar"] = typeof(LuaUIProgressBar);
				_componentTypeMap["Particle"] = typeof(LuaUIParticle);
				_componentTypeMap["ChatEmojiItem"] = typeof(LuaUIChatEmojiItem);
                _componentTypeMap["ChatTextBlock"] = typeof(LuaUIChatTextBlock);
                _componentTypeMap["NavigationMenu"] = typeof(LuaUINavigationMenu);
                
				_componentTypeMap["VoiceDriver"] = typeof(LuaUIVoiceDriver);
                _componentTypeMap["InputFieldMesh"] = typeof(LuaUIInputFieldMesh);
                _componentTypeMap["ScaleProgressBar"] = typeof(LuaUIScaleProgressBar);
                _componentTypeMap["Dial"] = typeof(LuaUIDial);
                _componentTypeMap["MultipleProgressBar"] = typeof(LuaUIMultipleProgressBar);
                _componentTypeMap["LuaUIGridLayoutGroup"] = typeof(LuaUIGridLayoutGroup);
                _componentTypeMap["LuaUILayoutElement"] = typeof(LuaUILayoutElement);
                _componentTypeMap["LuaUISlider"] = typeof(LuaUISlider);
                _componentTypeMap["Roller"] = typeof(LuaUIRoller);
                _componentTypeMap["RollerItem"] = typeof(LuaUIRollerItem);
            }
            if (!_componentTypeMap.ContainsKey(componentType))
            {
                LoggerHelper.Error("GetLuaUIComponent Error has no type ===" + componentType);
            }
            return _componentTypeMap[componentType];
        }
    }
}