﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class XScrollView : XContainer
    {
        private GameObject _maskGo;
        private XScrollRect _scrollRect;
        private GameObject _contentGo;
        private ScrollViewArrow _arrow;
        private Scrollbar _scrollbar;
        private Text _curPage;
        private Text _allPage;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
            _scrollRect.horizontal = false;
            _scrollRect.vertical = true;
            MogoEngine.Events.EventDispatcher.AddEventListener("UpdateScrollViewBar", UpdateContent);
        }

        protected override void OnDestroy()
        {
            MogoEngine.Events.EventDispatcher.RemoveEventListener("UpdateScrollViewBar", UpdateContent);
        }

        protected override void OnEnable()
        {
            _scrollRect.onValueChanged.AddListener(OnScrollRectValueChanged);
        }

        protected override void OnDisable()
        {
            _scrollRect.onValueChanged.RemoveListener(OnScrollRectValueChanged);            
        }

        private void CreateChildren()
        {
            _maskGo = GetChild("Mask");
            _scrollRect = _maskGo.GetComponent<XScrollRect>();
            if (_scrollRect == null) _scrollRect = _maskGo.AddComponent<XScrollRect>();
            _contentGo = GetChild("Mask/Content");
            _scrollRect.content = _contentGo.GetComponent<RectTransform>();
            GameObject arrowGo = GetChild("Arrow");
            if(arrowGo != null)
            {
                _arrow = arrowGo.GetComponent<ScrollViewArrow>();
                if (_arrow == null) _arrow = arrowGo.AddComponent<ScrollViewArrow>();
                UpdateArrow();
            }

            GameObject pageContainer = GetChild("Container_Page/Text_curPage");
            GameObject allPageContainer = GetChild("Container_Page/Text_allPage");
            if (pageContainer != null)
            { 
                _curPage = pageContainer.GetComponent<Text>();
                _allPage = allPageContainer.GetComponent<Text>();
                UpdatePage();
            }

            GameObject barGo = GetChild("Scrollbar");
            if (barGo != null)
            {
                _scrollbar = barGo.GetComponent<Scrollbar>();
                UpdateBar();
            }
        }

        public Mask Mask
        {
            get
            {
                return _maskGo.GetComponent<Mask>();
            }
        }

        public GameObject Content
        {
            get
            {
                return _contentGo;
            }
        }

        public XScrollRect ScrollRect
        {
            get
            {
                return _scrollRect;
            }
        }

        public ScrollViewArrow Arrow
        {
            get
            {
                return _arrow;
            }
        }

        private void OnScrollRectValueChanged(Vector2 position)
        {
            UpdateArrow();
            UpdateBar();
            UpdatePage();
        }

        public void SetScrollRectState(bool active)
        {
            _scrollRect.enabled = active;
        }

        public void UpdateArrow()
        {
            if(_arrow == null)
            {
                return;
            }
            _arrow.UpdateArrow(_maskGo.GetComponent<RectTransform>(), _contentGo.GetComponent<RectTransform>(), _scrollRect.vertical);
        }

        public void ResetMaskSize(float x, float y)
        {
            RectTransform rect = _maskGo.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(x, y);
            Image image = _maskGo.GetComponent<Image>();
            image.SetAllDirty();
        }

        public void RefreshContent()
        {
            _scrollRect.content = _contentGo.GetComponent<RectTransform>();
        }

        public void ResetContentPosition()
        {
            _contentGo.GetComponent<RectTransform>().localPosition = Vector2.zero;
            UpdateArrow();
            UpdateBar();
        }

        private void UpdateContent()
        {
            UpdateBar();
            UpdatePage();
        }

        private void UpdatePage()
        {
            if (_curPage == null)
            {
                return;
            }

            RectTransform maskRect = _maskGo.GetComponent<RectTransform>();
            RectTransform contentRect = _contentGo.GetComponent<RectTransform>();
            bool isVertical = _scrollRect.vertical;

            int allPage = 0;
            int nowPage = 0;
            if (isVertical == true)
            {
                float maskHeight = maskRect.rect.yMax - maskRect.rect.yMin;
                float contentHeight = contentRect.rect.yMax - contentRect.rect.yMin;
                allPage = (int)Math.Ceiling(contentHeight / maskHeight);
                nowPage = (int)Math.Ceiling((contentRect.localPosition.y - maskRect.localPosition.y) / maskHeight);
            }
            else
            {

                float maskWidth = maskRect.rect.xMax - maskRect.rect.xMin;
                float contentWidth = contentRect.rect.xMax - contentRect.rect.xMin;
                allPage = (int)Math.Ceiling(maskWidth / contentWidth);
                nowPage = (int)Math.Ceiling((maskRect.localPosition.x - contentRect.localPosition.x) / maskWidth);
            }

            _curPage.text = string.Concat(nowPage);
            _allPage.text = string.Concat("/", allPage);
        }

        private void UpdateBar()
        {
            if (_scrollbar == null)
            {
                return;
            }
            RectTransform _maskRect = _maskGo.GetComponent<RectTransform>();
            RectTransform _contentRect = _contentGo.GetComponent<RectTransform>();
            bool _isVertical = _scrollRect.vertical;
            if (_isVertical == true)
            {
                float maskHeight = _maskRect.rect.yMax - _maskRect.rect.yMin;
                float contentHeight = _contentRect.rect.yMax - _contentRect.rect.yMin;
                float _Heightper = (_contentRect.localPosition.y - maskHeight - _maskRect.localPosition.y) / (contentHeight- maskHeight);
                if (contentHeight < maskHeight)
                {
                    _scrollbar.gameObject.SetActive(false);
                }
                else
                {
                    _scrollbar.gameObject.SetActive(true);
                }
                _scrollbar.value = 1 - _Heightper;
                /*Debug.LogError(_Heightper + "========_contentRect.localPosition.y=====" + _contentRect.localPosition.y
                    + "========_maskRect.localPosition.y=====" + _maskRect.localPosition.y
                    + "========contentHeight=====" + contentHeight
                     + "========maskHeight=====" + maskHeight);*/
            }
            else
            {

                float maskWidth = _maskRect.rect.xMax - _maskRect.rect.xMin;
                float contentWidth = _contentRect.rect.xMax - _contentRect.rect.xMin;
                float _Widthper = (_contentRect.localPosition.x - _maskRect.localPosition.x ) / (contentWidth - maskWidth);
                if (contentWidth < maskWidth)
                {
                    _scrollbar.gameObject.SetActive(false);
                }
                else
                {
                    _scrollbar.gameObject.SetActive(true);
                }
                /*Debug.LogError(_Widthper + "========_contentRect.localPosition.x=====" + _contentRect.localPosition.x
                    + "========_maskRect.localPosition.x=====" + _maskRect.localPosition.x
                    + "========contentWidth=====" + contentWidth
                     + "========maskWidth=====" + maskWidth);*/
                _scrollbar.value = -_Widthper;
            }
        }

        /// <summary>
        /// KScrollView的辅助类
        /// 当Content高度或宽度小于Mask相应值时，关闭Mask和ScrollRect功能
        /// </summary>
        public class KScrollViewContent : UIBehaviour
        {
            public GameObject maskGo;
            public GameObject arrowGo;

            protected override void OnRectTransformDimensionsChange()
            {
                base.OnRectTransformDimensionsChange();
                UpdateUsability();
            }

            private void UpdateUsability()
            {
                if(maskGo == null)
                {
                    return;
                }
                RectTransform rect = GetComponent<RectTransform>();
                Mask mask = maskGo.GetComponent<Mask>();
                RectTransform maskRect = maskGo.GetComponent<RectTransform>();
                XScrollRect scrollRect = maskGo.GetComponent<XScrollRect>();
                if(scrollRect.vertical == true)
                {
                    if(rect.sizeDelta.y <= maskRect.sizeDelta.y)
                    {
                        mask.enabled = false;
                        scrollRect.verticalNormalizedPosition = 1;
                        scrollRect.enabled = false;
                        HideArrow();
                    }
                    else
                    {
                        mask.enabled = true;
                        scrollRect.enabled = true;
                        ShowArrow();
                    }
                }
                else
                {
                    if(rect.sizeDelta.x <= maskRect.sizeDelta.x)
                    {
                        mask.enabled = false;
                        scrollRect.horizontalNormalizedPosition = 0;
                        scrollRect.enabled = false;
                        HideArrow();
                    }
                    else
                    {
                        mask.enabled = true;
                        scrollRect.enabled = true;
                        ShowArrow();
                    }
                }
            }

            private void HideArrow()
            {
                if (arrowGo != null)
                {
                    arrowGo.SetActive(false);
                }
            }

            private void ShowArrow()
            {
                if (arrowGo != null)
                {
                    arrowGo.SetActive(true);
                }
            }
        }
    }

}
