﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class ScrollPageDisplay : UIBehaviour
    {
        private int _currentPage = 0;
        private XList _list;
        private int _gap = 10;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
        }

        private void CreateChildren()
        {
            _list = GetComponent<XList>();
            _list.SetDirection(XListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, _gap);
        }

        public void SetTotalPage(int total, RectTransform anchoredRect, bool createByCoroutine)
        {
            _list.RemoveAll();
            System.Object[] fakeDatas = new System.Object[total];
            int coroutineCreateCount = createByCoroutine == true ? 1 : 0;
            _list.SetDataList<ScrollPageDisplayListItem>(fakeDatas, coroutineCreateCount);
            _list[_currentPage].IsSelected = true;
            RectTransform rect = GetComponent<RectTransform>();
            float width;
            if(createByCoroutine == true)
            {
                //使用协程创建Item时，此时只有一个item已经创建好了
                width = rect.sizeDelta.x * total + _gap * (total - 1);
            }
            else
            {
                width = rect.sizeDelta.x;
            }
            float x = anchoredRect.anchoredPosition.x + (anchoredRect.sizeDelta.x - width) * 0.5f;
            rect.anchoredPosition = new Vector2(x, rect.anchoredPosition.y);
        }

        public void SetCurrentPage(int current)
        {
            if(current != _currentPage)
            {
                _list[_currentPage].IsSelected = false;
                _list[current].IsSelected = true;
                _currentPage = current;
            }
        }
    }

    class ScrollPageDisplayListItem : XListItemBase, ICanvasRaycastFilter
    {
        private Image _spotlightImage;

        protected override void Awake()
        {
            CreateChildren();
        }

        public override void Dispose()
        {
            
        }

        private void CreateChildren()
        {
            _spotlightImage = GetChildComponent<Image>("Image_spotlight");
            ToggleSpotlight(false);
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                ToggleSpotlight(value);
            }
        }

        private void ToggleSpotlight(bool isOn)
        {
            if(isOn == true)
            {
                //_spotlightImage.Visible = true;
            }
            else
            {
                //_spotlightImage.Visible = false;
            }
        }

        public bool IsRaycastLocationValid(Vector2 screenPosition, Camera eventCamera)
        {
            return false;
        }
    }
}
