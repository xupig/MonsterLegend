﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class XScrollPage : XContainer, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField]
        private float _bounce = 0.5f;
        public float bounce { get { return _bounce; } set { _bounce = value; } }

        private bool _isDragging = false;
        private bool _isMoving = false;

        private RectTransform _viewTransform;
        private RectTransform _contentTransform;

        private float _pointerStartX = 0;
        private float _pointerEndX = 0;
        private float _contentStartX = 0;
        private float _contentEndX = 0;

        private int _totalPage = 1;
        private int _currentPage = 0;
        public XComponentEvent<XScrollPage, int> onCurrentPageChanged = new XComponentEvent<XScrollPage, int>();
        public XComponentEvent<XScrollPage> onRequestPrePage = new XComponentEvent<XScrollPage>();
        public XComponentEvent<XScrollPage> onRequestNextPage = new XComponentEvent<XScrollPage>();

        private ScrollPageArrow _pageArrow;
        private ScrollPageDisplay _pageDisplay;

        private KScrollPageDirection _direction = KScrollPageDirection.LeftToRight;

        private bool _isRequestPrePage = false;
        private bool _isRequestNextPage = false;

        private bool _drageable = true;
        public bool Dragable
        {
            get { return _drageable; }
            set { _drageable = value; }
        }

        public bool IsMoving
        {
            get { return _isMoving; }
        }

        public bool IsDragging
        {
            get { return _isDragging; }
        }

        private ScrollRect.MovementType _moveType = ScrollRect.MovementType.Elastic;
        public ScrollRect.MovementType MoveType
        {
            get { return _moveType; }
            set { _moveType = value; }
        }

        public bool CreatePagerByCoroutine
        {
            get;
            set;
        }

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
        }

        public void SetDirection(KScrollPageDirection direction)
        {
            this._direction = direction;
        }

        private void CreateChildren()
        {
            GameObject arrowGo = GetChild("arrow");
            if(arrowGo != null)
            {
                _pageArrow = arrowGo.AddComponent<ScrollPageArrow>();
            }
            GameObject pagerGo = GetChild("pager");
            if(pagerGo != null)
            {
                _pageDisplay = pagerGo.AddComponent<ScrollPageDisplay>();
            }
            _viewTransform = GetChildComponent<RectTransform>("Mask");
            _contentTransform = GetChildComponent<RectTransform>("Mask/Content");
        }

        public int TotalPage
        {
            get
            {
                return _totalPage;
            }
            set
            {
                if(value != _totalPage)
                {
                    _totalPage = value;
                    if(_pageDisplay != null)
                    {
                        _pageDisplay.SetTotalPage(_totalPage, _viewTransform, this.CreatePagerByCoroutine);
                    }
                }
                if(_pageArrow != null)
                {
                    _pageArrow.UpdateArrow(_currentPage, _totalPage);
                }
                
            }
        }

        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                int page = Mathf.Clamp(value, 0, _totalPage - 1);
                float x = (float)Math.Round(page * GetViewWidth());
                SetContentX(-x);
                UpdateCurrentPage(page);
            }
        }

        public void OnBeginDrag(PointerEventData evtData)
        {
            if (Dragable == false)
            {
                return;
            }
            if(evtData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
            if(IsActive() == false)
            {
                return;
            }
            Vector2 pointerPosition = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_viewTransform, evtData.position, evtData.pressEventCamera, out pointerPosition);
            _pointerStartX = pointerPosition.x;
            _contentStartX = _contentTransform.anchoredPosition.x;
            _isDragging = true;
        }

        public void OnDrag(PointerEventData evtData)
        {
            if (Dragable == false)
            {
                return;
            }
            if(evtData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
            if(IsActive() == false)
            {
                return;
            }
            Vector2 pointerPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_viewTransform, evtData.position, evtData.pressEventCamera, out pointerPosition);
            float deltaX = pointerPosition.x - _pointerStartX;
            if (_moveType == ScrollRect.MovementType.Clamped)
            {
                if ((_currentPage == 0 && deltaX > 0)
                || (_currentPage == (_totalPage - 1) && deltaX < 0))
                {
                    return;
                }
            }
            float contentX = _contentStartX + deltaX;
            SetContentX(contentX);
            
        }

        public void OnEndDrag(PointerEventData evtData)
        {
            if (Dragable == false)
            {
                return;
            }
            if ( _isDragging == false )
            {
                return;
            }
            if(evtData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
            _isDragging = false;
            Vector2 pointerPosition = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_viewTransform, evtData.position, evtData.pressEventCamera, out pointerPosition);
            _pointerEndX = pointerPosition.x;
            StartMove();
        }

        private void StartMove()
        {
            _isMoving = true;
            if (_currentPage == _totalPage && _pointerEndX > _pointerStartX)
            {
                _isRequestNextPage = true;
            }
            else if (_currentPage == 1 && _pointerEndX < _pointerStartX)
            {
                _isRequestPrePage = true;
            }
            float x = (float)Math.Round(_currentPage * GetViewWidth());
            _contentEndX = -x + ((_pointerEndX < _pointerStartX) ? -GetViewWidth() : GetViewWidth());
            _contentEndX = Mathf.Clamp(_contentEndX, -(_totalPage - 1) * GetViewWidth(), 0);
        }

        private void Move()
        {
            if(_isMoving == true)
            {
                float delta = (_contentEndX - GetContentX()) * _bounce;
                
                if(ACTSystem.ACTMathUtils.Abs(delta) < 1.0f)
                {
                    SetContentX(_contentEndX);
                    StopMove();
                }
                else
                {
                    SetContentX(GetContentX() + delta);
                }
            }
        }

        private void StopMove()
        {
            _isMoving = false;
            OnPageChanged();
            if (_isRequestNextPage)
            {
                onRequestNextPage.Invoke(this);
            }
            else if(_isRequestPrePage)
            {
                onRequestPrePage.Invoke(this);
            }
        }

        protected void Update()
        {
            Move();
        }

        private void SetContentX(float value)
        {
            if(value != _contentTransform.anchoredPosition.x)
            {
                _contentTransform.anchoredPosition = new Vector2(value, _contentTransform.anchoredPosition.y);
            }
        }

        private float GetContentX()
        {
            return _contentTransform.anchoredPosition.x;
        }

        private float GetViewWidth()
        {
            return _viewTransform.rect.width;
        }


        private void OnPageChanged()
        {
            int page = -(int)Math.Round((GetContentX()) / GetViewWidth());
            if(page != _currentPage)
            {
                UpdateCurrentPage(page);
                onCurrentPageChanged.Invoke(this, page);
            }
        }

        private void UpdateCurrentPage(int page)
        {
            _currentPage = page;
            if(_pageArrow != null)
            {
                _pageArrow.UpdateArrow(page, _totalPage);
            }
            if(_pageDisplay != null)
            {
                _pageDisplay.SetCurrentPage(page);
            }
            UpdatePageableList();
        }

        private void UpdatePageableList()
        {
            LuaUIPageableList pageableList = _contentTransform.gameObject.GetComponent<LuaUIPageableList>();
            if(pageableList != null)
            {
                pageableList.CurrentPage = _currentPage;
            }
        }

        public void UpdateArrow(int current, int total)
        {
            if (_pageArrow != null)
            {
                _pageArrow.UpdateArrow(current, total);
            }
        }

        public enum KScrollPageDirection
        {
            //
            /// <summary>
            /// List Item布局时，纵向布局优先（TopToDown）横向布局优先（LeftToRight）
            /// 如单列列表应该是（LeftToRight = 1, TopToDown = int.Max）;
            /// 如单行列表应该是（LeftToRight = int.Max, TopToDown = 1）;
            /// </summary>
            TopToDown = 0,
            LeftToRight = 1,
        }

    }
}