﻿using UnityEngine;
using System.Collections.Generic;

namespace GameMain
{
    /// <summary>
    /// 
    /// </summary>
    public interface IXContainer
    {
        GameObject GetChild(string childPath);
        T GetChildComponent<T>(string childPath) where T : Component;
        T AddChildComponent<T>(string childPath) where T : Component;
        bool Visible { get; set; }
    }

}
