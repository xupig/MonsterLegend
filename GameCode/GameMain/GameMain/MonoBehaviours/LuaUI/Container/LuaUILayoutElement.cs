﻿using UnityEngine.UI;

namespace GameMain
{
    public class LuaUILayoutElement : LuaUIComponent
    {
        private LayoutElement _layoutElement;
        protected override void Awake()
        {
            base.Awake();
            _layoutElement = this.GetComponent<LayoutElement>();
            if (_layoutElement == null) _layoutElement = this.gameObject.AddComponent<LayoutElement>();
        }

        public void SetIgnoreLayout(bool state)
        {
            _layoutElement.ignoreLayout = state;
        }

        protected override void Start()
        {
        }
            
        protected override void OnDestroy()
        {
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }
    }
}