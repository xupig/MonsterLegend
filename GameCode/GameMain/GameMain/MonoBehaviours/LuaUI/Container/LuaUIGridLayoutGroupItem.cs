﻿using LuaInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMain
{
    [DisallowMultipleComponent]
    public class LuaUIGridLayoutGroupItem : LuaUIComponent, IPointerClickHandler, IPointerDownHandler
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }

        protected XComponentEvent<LuaUIGridLayoutGroupItem, int> _onClick;

        public XComponentEvent<LuaUIGridLayoutGroupItem, int> onClick
        {
            get
            {
                if (_onClick == null)
                {
                    _onClick = new XComponentEvent<LuaUIGridLayoutGroupItem, int>();
                }
                return _onClick;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
        }

        public void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            if (_onClick != null)
            {
                _onClick.Invoke(this, this.Index);
            }
        }

        public void DoRefreshData()
        {
            CallFunction("__refresh__");
        }

        public void SetBelongList(LuaTable belongList)
        {
            CallFunction("__belonglist__", belongList);
        }

        public virtual void Show() { }
        public virtual void Hide() { }
        public virtual void Dispose() { }
    }
}
