﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;

namespace GameMain
{
    public class LuaUIChatEmojiItem : LuaUIComponent
    {
        ChatEmojiItem _chatEmojiItem;
        protected override void Awake()
        {
            base.Awake();
            _chatEmojiItem = this.gameObject.AddComponent<ChatEmojiItem>();
        }

        protected override void OnDestroy()
        {
        }

        public void SetChatEmojiItem(string value)
        {
            _chatEmojiItem.Data = value;
        }

        public void SetAnimation(bool value)
        {
            //LoggerHelper.Error("value =============== " + value);
            if (_chatEmojiItem != null)
            {
                _chatEmojiItem.SetAnimation(value);
            }
        }
    }
}