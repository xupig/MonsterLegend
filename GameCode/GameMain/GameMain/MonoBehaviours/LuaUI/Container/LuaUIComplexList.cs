﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;

namespace GameMain
{
    /// <summary>
    /// List可以作为单行单列或多行多列布局容器
    /// List中必须含有名为item的子元素
    /// </summary>
    public class LuaUIComplexList : LuaUIList
    {

        protected override void Awake()
        {
            base.Awake();
        }

        public new void RemoveItemAt(int index)
        {
            if (index >= 0 && index < _itemList.Count)
            {
                LuaUIListItem item = _itemList[index];
                RemoveItemEventListener(item);
                _itemList.RemoveAt(index);
                item.Dispose();
                ReleaseItemEx(item);
            }
        }

        public new void RemoveItemRange(int start, int count)
        {
            for (int i = start + count - 1; i >= start; i--)
            {
                if (i >= 0 && i < _itemList.Count)
                {
                    LuaUIListItem item = _itemList[i];
                    RemoveItemEventListener(item);
                    _itemList.RemoveAt(i);
                    item.Dispose();
                    ReleaseItemEx(item);
                }
            }
        }

        public void AddItems(int count, bool layoutImmediately = false)
        {
            for (int i = 0; i < count; i++)
            {
                AddItem(false);
            }
            if (layoutImmediately)
            {
                UpdateItemListLayout();
            }
        }

        public void SetItemDirtyAt(int index)
        {
            _itemList[index].DoRefreshData();
        }

        public override void SetAllItemDirty()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].DoRefreshData();
            }
        }

        public void RefreshAllDirtyItem()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                var item = _itemList[i];
                if (item.IsDataDirty)
                {
                    item.IsDataDirty = false;
                    item.DoRefreshData();
                }
            }
        }

        protected override void UpdateItemListLayout()
        {
            RectTransform listRect = GetComponent<RectTransform>();
            int index = OffsetItemCount;
            Vector2 totalSizeDelta = Vector2.zero;
            for (int i = 0; i < _itemList.Count; i++)
            {
                LuaUIListItem item = _itemList[i];
                RectTransform itemRect = item.GetComponent<RectTransform>();
                Vector2 position = CalculateItemPosition(i, itemRect, ref totalSizeDelta);
                itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                if (item.Index != i)
                {
                    item.Index = i;
                    item.name = GenerateItemName(i);
                }
            }
            listRect.sizeDelta = totalSizeDelta;
            MogoEngine.Events.EventDispatcher.TriggerEvent("UpdateScrollViewBar");
        }

        private Vector2 CalculateItemPosition(int index, RectTransform itemRect, ref Vector2 curTotalSizeDelta)
        {
            Vector2 result = Vector2.zero;
            int pageCount = _leftToRightCount * _topToDownCount;
            if (_direction == LuaUIListDirection.LeftToRight)
            {
                int xIndex = (int)(index % pageCount % _leftToRightCount);
                result.y = _padding.top + (int)((index % pageCount) / _leftToRightCount) * (_itemSize.y + _topToDownGap);
                if (xIndex == 0)
                {
                    result.x = _padding.left + curTotalSizeDelta.x;
                    curTotalSizeDelta.x = itemRect.sizeDelta.x + _padding.left;
                }
                else
                {
                    result.x = curTotalSizeDelta.x + _leftToRightGap;
                    curTotalSizeDelta.x += itemRect.sizeDelta.x + _leftToRightGap;
                }
                curTotalSizeDelta.y = itemRect.sizeDelta.y + _padding.top;
            }
            else
            {
                int yIndex = (int)((index % pageCount) % _topToDownCount);
                result.x = _padding.left + (int)((index % pageCount) / _topToDownCount) * (_itemSize.x + _leftToRightGap);
                curTotalSizeDelta.x = itemRect.sizeDelta.x + _padding.left;
                if (yIndex == 0)
                {
                    result.y = _padding.top + curTotalSizeDelta.y;
                    curTotalSizeDelta.y = itemRect.sizeDelta.y + _padding.top;
                }
                else
                {
                    result.y = curTotalSizeDelta.y + _topToDownGap;
                    curTotalSizeDelta.y += itemRect.sizeDelta.y + _topToDownGap;
                }
            }
            return result;
        }
    }

}
