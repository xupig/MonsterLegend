﻿using LuaInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace GameMain
{
    public class LuaUIGridLayoutGroup : LuaUIComponent
    {
        public const string ITEM_PREFIX = "item_";
        public string luaItemType = string.Empty;
        private List<LuaUIListItem> _itemList = new List<LuaUIListItem>();
        private XComponentEvent<LuaUIGridLayoutGroup, LuaUIListItem> _onItemClicked;
        protected int length = 0;
        private bool _useObjectPool = false;
        private Queue<GameObject> _objectPool;
        protected GameObject _template;
        private Transform _poolTransform;
        private LuaUIListItem _selectedItem;
        private GridLayoutGroup _gridLayoutGroup;

        private XComponentEvent<LuaUIGridLayoutGroup, int> _onSelectedIndexChanged;

        public int SelectedIndex
        {
            get
            {
                if (_selectedItem == null)
                {
                    return -1;
                }
                return _selectedItem.Index;
            }
            set
            {
                if (_selectedItem != null)
                {
                    _selectedItem.IsSelected = false;
                    _selectedItem = null;
                }
                if (value >= 0 && value <= _itemList.Count - 1)
                {
                    _selectedItem = _itemList[value];
                    _selectedItem.IsSelected = true;
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            if (this.transform.Find("item") != null)
            {
                InitItemTemplate();
                InitObjectPool();
            }
            _gridLayoutGroup = this.GetComponent<GridLayoutGroup>();
        }

        public void SetChildAlignment(int childAlignment)
        {
            _gridLayoutGroup.childAlignment = (TextAnchor)(childAlignment);
        }

        protected void InitItemTemplate()
        {
            _template = this.transform.Find("item").gameObject;
            var rect = _template.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            _template.SetActive(false);
        }

        private void InitObjectPool()
        {
            if (_poolTransform == null)
            {
                var go = new GameObject("pool");
                var rect = go.AddComponent<RectTransform>();
                rect.anchoredPosition = Vector2.zero;
                go.SetActive(false);
                _poolTransform = go.transform;
                _poolTransform.SetParent(this.transform, false);
                _objectPool = new Queue<GameObject>();
            }
        }

        public void UseObjectPool(bool value)
        {
            this._useObjectPool = value;
            if (this._useObjectPool)
            {
                InitObjectPool();
            }
        }

        public void SetLength(int len)
        {
            this.length = len;
            int curListCount = _itemList.Count;
            if (len == curListCount)
            {
            }
            else if (len > curListCount)
            {
                for (int i = curListCount; i < len; i++)
                {
                    AddItem();
                }
            }
            else if (len < curListCount)
            {
                RemoveItemRange(len, curListCount - len);
                //SetAllItemDirty();
            }
            UpdateData();
        }

        public void UpdateData()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].DoRefreshData();
            }
        }

        public void AddItem()
        {
            AddItemAt(_itemList.Count);
        }

        public virtual void AddItemAt(int index)
        {
            LuaUIListItem item = CreateItemEx();
            GameObject itemGo = item.gameObject;
            AddToHierarchy(itemGo);
            item.Index = index;
            item.name = GenerateItemName(index);
            item.SetBelongList(this.luaTable);
            _itemList.Insert(index, item);
            AddItemEventListener(item);
        }

        protected string GenerateItemName(int index)
        {
            return ITEM_PREFIX + index;
        }

        protected LuaUIListItem CreateItemEx()
        {
            GameObject itemGo = null;
            LuaUIListItem item = null;
            if (this._useObjectPool)
            {
                if (_objectPool.Count > 0)
                {
                    itemGo = _objectPool.Dequeue();
                    item = itemGo.GetComponent<LuaUIListItem>();
                }
            }
            if (itemGo == null)
            {
                itemGo = Instantiate(_template) as GameObject;
                item = LuaBehaviour.AddLuaBehaviour(itemGo, typeof(LuaUIListItem), this.luaItemType) as LuaUIListItem;
            }
            return item;
        }

        private void AddToHierarchy(GameObject itemGo)
        {
            itemGo.transform.SetParent(this.transform, true);
            itemGo.transform.localScale = Vector3.one;
            itemGo.transform.localPosition = Vector3.zero;
            itemGo.SetActive(true);
        }

        protected void AddItemEventListener(LuaUIListItem item)
        {
            item.onClick.AddListener(OnItemClicked);
        }

        protected virtual void OnItemClicked(LuaUIListItem item, int index)
        {
            int oldSelectedIndex = this.SelectedIndex;
            if (_onItemClicked != null)
            {
                _onItemClicked.Invoke(this, item);
            }
            CallFunction("OnItemClicked", item.luaTable);
            if (item == _selectedItem && _selectedItem != null && _selectedItem.Index == index)
            {
                return;
            }
            bool isSelectedIndexChanged = (this.SelectedIndex != oldSelectedIndex);
            if (isSelectedIndexChanged == true)//若在_onItemClicked事件中修改了KList.SelectedIndex，则不再调用后续事件
            {
                return;
            }
            if (_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }
            _selectedItem = item;
            _selectedItem.IsSelected = true;
            if (_onSelectedIndexChanged != null)
            {
                _onSelectedIndexChanged.Invoke(this, index);
            }
        }

        public virtual void RemoveItemRange(int start, int count)
        {
            for (int i = start + count - 1; i >= start; i--)
            {
                LuaUIListItem item = _itemList[i];
                RemoveItemEventListener(item);
                _itemList.RemoveAt(i);
                item.Dispose();
                ReleaseItemEx(item);
            }
        }

        protected void RemoveItemEventListener(LuaUIListItem item)
        {
            item.onClick.RemoveAllListeners();
        }

        protected void ReleaseItemEx(LuaUIListItem item)
        {
            if (this._useObjectPool)
            {
                _objectPool.Enqueue(item.gameObject);
                item.transform.SetParent(_poolTransform, false);
            }
            else
            {
                GameObject.Destroy(item.gameObject);
            }
        }

        public virtual void RemoveAll()
        {
            RemoveItemRange(0, _itemList.Count);
        }

        public virtual LuaTable GetItem(int index)
        {
            return _itemList[index].luaTable;
        }
    }
}
