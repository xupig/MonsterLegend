﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;
using Common.Voice;
using MogoEngine.Events;
using Common.Events;

namespace GameMain
{
    public class LuaUIVoiceDriver : LuaUIComponent
    {
        private VoiceDriver _voiceDriver;
        private string _curMsg = "";
        private uint _Time = 0;
        protected override void Awake()
        {
            base.Awake();
            _voiceDriver = this.gameObject.AddComponent<VoiceDriver>();
            AddEventListener();
            VoiceManager.Instance.ClearFile();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(ChatEvents.SDK_START_TALK, OnStartVoiceTalk);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_VOICE_MESSAGE, OnConvertVoiceMessage);
            EventDispatcher.AddEventListener<int>(ChatEvents.SDK_VOICE_VOLUME, OnVoiceVolumeChange);
            EventDispatcher.AddEventListener<string, int>(ChatEvents.SDK_END_TALK, OnEndVoiceTalk);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_TALK_ERROR, OnShowVoiceTalkError);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoiceCallback);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_PLAY_VOICE_ERROR, OnPlayVoiceError);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(ChatEvents.SDK_START_TALK, OnStartVoiceTalk);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_VOICE_MESSAGE, OnConvertVoiceMessage);
            EventDispatcher.RemoveEventListener<int>(ChatEvents.SDK_VOICE_VOLUME, OnVoiceVolumeChange);
            EventDispatcher.RemoveEventListener<string, int>(ChatEvents.SDK_END_TALK, OnEndVoiceTalk);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_TALK_ERROR, OnShowVoiceTalkError);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoiceCallback);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_PLAY_VOICE_ERROR, OnPlayVoiceError);
        }

        private void OnStartVoiceTalk()
        {
            CallFunction("OnStartVoiceTalk");
        }

        private void OnConvertVoiceMessage(string msg)
        {
            _curMsg += msg;
            //CallFunction("OnConvertVoiceMessage", msg);
        }

        private void OnVoiceVolumeChange(int volume)
        {
            CallFunction("OnVoiceVolumeChange", volume);
        }

        private void OnShowVoiceTalkError(string error)
        {
            //CallFunction("OnShowVoiceTalkError", error);
        }

        private void OnEndVoiceTalk(string path, int duration)
        {
            LoggerHelper.Info("[OnEndVoiceTalk] path:" + path + " duration:" + duration);
            //CallFunction("OnVoiceVolumeChange", path,duration);
            VoiceManager.Instance.OnEndVoiceTalk(_curMsg, path, duration, OnEndVoiceTalkCallBack);
        }

        private void OnEndVoiceTalkCallBack(string msg)
        {
            CallFunction("OnConvertVoiceMessage", msg);
        }

        private void OnStartPlayVoiceCallback(string path)
        {
            //CallFunction("OnStartPlayVoiceCallback", path);
        }

        /// <summary>
        /// 调用 讯飞 SDK 开始语音
        /// </summary>
        public void OnStartTalk()
        {
            LoggerHelper.Info("[OnStartTalk]:GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GotyeStartTalkTo");
            _curMsg = "";
#if !UNITY_ANDROID
            //EventDispatcher.TriggerEvent<string>(ChatEvents.SDK_PLAY_VOICE_ERROR, "1");
            VoiceManager.Instance.OnEndVoiceTalk("别试了，语音只有在手机上才有用", "thumb1.png",17, OnEndVoiceTalkCallBack);

            return;
#endif


            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GotyeStartTalkTo("GotyeStartTalkTo");
        }

        /// <summary>
        /// 调用 SDK 停止 录音
        /// </summary>
        public void OnSDKStopTalk()
        {
            LoggerHelper.Info("[OnSDKStopTalk]:GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GotyeStopTalk");

            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GotyeStopTalk();
        }

        /// <summary>
        /// 播放语音
        /// </summary>
        /// <param name="path"></param>
        public void OnSDKPlayVoice(string path,string time)
        {
            LoggerHelper.Info("[OnSDKPlayVoice]:GameLoader.PlatformSdk.PlatformSdkMgr.Instance.OnSDKPlayVoice" + path, true);
            VoiceManager.Instance.OnDownloadAndPlayVoice(path);

            CloseMusic(time);
        }

        private void CloseMusic(string time)
        {
            uint playTime = uint.Parse(time);
            if(_Time > 0 )
            {
                GameLoader.Utils.Timer.TimerHeap.DelTimer(_Time);
            }
            _Time = GameLoader.Utils.Timer.TimerHeap.AddTimer(playTime, 0,OpenMusic);
            AudioManager.GetInstance().TempCloseVolume();
        }

        private void OpenMusic()
        {
            _Time = 0;
            AudioManager.GetInstance().ResetTempVolume();
        }

        private void OnPlayVoiceError(string errorid)
        {
            CallFunction("OnPlayVoiceError", errorid);
        }
    }
}