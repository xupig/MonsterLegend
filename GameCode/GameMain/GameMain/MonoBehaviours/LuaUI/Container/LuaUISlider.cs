﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;

namespace GameMain
{
    public class LuaUISlider : LuaUIComponent
    {
        SliderWrapper _slider;
        protected override void Awake()
        {
            base.Awake();
            _slider = GetComponent<SliderWrapper>();
            if (_slider == null) _slider = this.gameObject.AddComponent<SliderWrapper>();
            _slider.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void OnDestroy()
        {
            _slider.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void OnValueChanged(float value)
        {
            CallFunction("OnValueChanged", value);
        }

        public void SetValue(float value)
        {
            _slider.value = value;
        }

        public float GetValue()
        {
            return _slider.value;
        }

        public void SetMinValue(float minValue)
        {
            _slider.minValue = minValue;
        }

        public void SetMaxValue(float maxValue)
        {
            _slider.maxValue = maxValue;
        }

        public void SetEnabled(bool flag)
        {
            _slider.enabled = flag;
        }
    }
}