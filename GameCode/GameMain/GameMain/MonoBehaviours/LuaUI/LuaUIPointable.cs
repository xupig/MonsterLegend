﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUIPointable : LuaUIComponent, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            CallFunction("OnPointerDown", eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            CallFunction("OnPointerUp", eventData);
        }
    }
}