﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;

namespace GameMain
{
    //=========================================================
    [DisallowMultipleComponent]
    public class LuaUIRollerItem : LuaUIComponent
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }
        internal bool IsDataDirty { get; set; }

        public Vector3 _startPos = Vector3.zero;
        public Vector3 _curPos = Vector3.zero;

        public virtual int ID
        {
            get;
            set;
        }

        public void SetDataDirty()
        {
            IsDataDirty = true;
        }

        public void DoRefreshData() 
        {
            CallFunction("__refresh__");
        }

        public void SetBelongList(LuaTable belongList)
        {
            CallFunction("__belonglist__", belongList);
        }

        public bool onEnd = false;
        public bool use = false;
        public void Reset()
        {
            onEnd = false;
            use = false;
        }

        public void Stop()
        {
            onEnd = true;
            use = false;
        }


        public virtual void Show() { }
        public virtual void Hide() { }
        public virtual void Dispose() { }

        protected new void OnEnable()//为排除警告，加回new
        {
            Show();
        }

        protected new void OnDisable()
        {
            Hide();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Dispose();
        }

        protected override void Awake()
        {
            base.Awake();
        }
    }
}
