﻿#region 模块信息
/*==========================================
// 文件名：KEffect
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/13 20:51:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMain
{
    [RequireComponent(typeof(SortOrderedRenderAgent))]
    public class LuaUIParticle : LuaBehaviour
    {
        private bool _isPlaying = false;
        private List<ParticleSystem> particleSystemList;
        private float _lifeTime;
        private bool _isSortWhenEnable = true;
        private bool isFirst = true;
        private bool _timeScaleEnable = true;
        private float _endTime;

        protected override void Awake()
        {
            base.Awake();
            //enabled = false;
            ChangeGameObjectLayer(gameObject);
            transform.localPosition = Vector3.zero;
            transform.localRotation = new Quaternion();
            transform.localScale = Vector3.one;

            particleSystemList = new List<ParticleSystem>();
            GetComponentsInChildren<ParticleSystem>(true, particleSystemList);
            RecalculateParticleScale(1);

        }
        
        public Vector3 Position
        {
            get { return transform.localPosition; }
            set { transform.localPosition = value; }
        }

        protected void Update()
        {
            if (!_timeScaleEnable)
            {
                for (int i = 0; i < particleSystemList.Count; i++)
                {
                    particleSystemList[i].Simulate(Time.unscaledDeltaTime, true, false);
                }
            }
            if (_isPlaying)
            {
                if (_endTime > 0 && _endTime < UnityPropUtils.realtimeSinceStartup)
                {
                    OnPlayEnd();
                }
            }
        }

        protected override void OnEnable()
        {
            if (_isSortWhenEnable || isFirst)
            {
                SortOrderedRenderAgent.ReorderAll();
            }
            isFirst = false;
            base.OnEnable();
        }

        protected override void OnDestroy()
        {
            Stop();
            particleSystemList.Clear();
            particleSystemList = null;
        }

        private float _scale = 1;
        public float Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                if (_scale != value)
                {
                    RecalculateParticleScale(value);
                    _scale = value;
                }
            }
        }

        public void SetTimeScaleEnable(bool state)
        {
            this._timeScaleEnable = state;
        }

        public void SetLifeTime(float value)
        {
            this._lifeTime = value;
        }

        public void IsSortWhenEnable(bool value)
        {
            this._isSortWhenEnable = value;
        }

        public void ForceReorderAll()
        {
            SortOrderedRenderAgent.ForceReorderAll();
        }

        private void RecalculateParticleScale(float scale)
        {
            for (int i = 0; i < particleSystemList.Count; i++)
            {
                particleSystemList[i].startSize = particleSystemList[i].startSize / _scale * scale;
            }
        }

        private void ChangeGameObjectLayer(GameObject parent)
        {
            parent.layer = PhysicsLayerDefine.LAYER_UI;
            Transform[] trans = parent.GetComponentsInChildren<Transform>();
            for (int i = 0; i < trans.Length;i++ )
            {
                trans[i].gameObject.layer = PhysicsLayerDefine.LAYER_UI;
            }
        }

        public void Play(bool isLoop = false, bool reset = true)
        {
            //Debug.LogError("Play:" + isLoop + ":" + reset);
            if (reset == true || _isPlaying == false)
            {
                Stop();
                gameObject.SetActive(true);
                _isPlaying = true;
                _endTime = isLoop ? -1 : UnityPropUtils.realtimeSinceStartup + _lifeTime;
            }
        }

        public void Stop()
        {
            gameObject.SetActive(false);
            _isPlaying = false;
        }

        private void OnPlayEnd()
        {
            Stop();
            CallFunction("__onplayend__");
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}
