﻿using GameMain.GlobalManager;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 15:02:33 
 * function: 
 * *******************************************************/
public class CameraAction : MonoBehaviour
{
    public bool run = false;
    private bool _isRunning = false;

    public bool reset = false;
    private bool _isReset = false;

    void Update()
    {
        if (!run)
        {
            _isRunning = false;
        }
        else
        {
            if (!_isRunning)
            {
                Run();
                _isRunning = true;
            }
            else
            {
            }
        }

        if (!reset)
        {
            _isReset = false;
        }
        else
        {
            if (!_isReset)
            {
                CameraManager.GetInstance().ResetCameraInfo();
                _isReset = false;
                reset = false;
            }
        }
    }

    protected virtual void Run()
    {
    }
}
