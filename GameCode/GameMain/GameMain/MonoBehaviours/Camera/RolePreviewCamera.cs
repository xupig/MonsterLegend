﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RolePreviewCamera:MonoBehaviour
{
    public float targetDistance = 15;
    public float rotationX = 15;
    public float height = 1f;
    public float right = 0f;
    Vector3 lockPos = Vector3.zero;
    Vector3 foucsPoint = Vector3.zero;
    Vector3 targetPos = Vector3.zero;
    Vector3 localEulerAngles = Vector3.zero;
    Camera _camera;

    void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    void Update()
    {
        if (lockPos == Vector3.zero)
        {
            return;
        }
        foucsPoint = lockPos;
        foucsPoint.y += height;
        foucsPoint.x += right;
        targetPos = foucsPoint - Vector3.forward * targetDistance * -1; //相机指向-z方向
        this.transform.position = Vector3.Lerp(this.transform.position, targetPos, 0.12f);
        this.transform.RotateAround(foucsPoint, new Vector3(1, 0, 0), rotationX);
        this.transform.LookAt(foucsPoint);
    }

    public void SetLockPos(Vector3 pos)
    {
        lockPos = pos;
    }

    public void SetArgs(float distance, float rotationX, float height = 1f, float right = 0f)
    {
        this.targetDistance = distance;
        this.rotationX = rotationX;
        this.height = height;
        this.right = right;
    }
}

