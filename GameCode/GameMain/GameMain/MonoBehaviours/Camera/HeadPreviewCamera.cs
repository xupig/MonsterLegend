﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HeadPreviewCamera : MonoBehaviour
{
    public Transform target;
    public float targetDistance = 15;
    public float rotationX = 15;
    public float rotationY = 0;
    public float height = 1f;
    public float right = 0f;
    Vector3 foucsPoint = Vector3.zero;
    Vector3 targetPos = Vector3.zero;
    Vector3 localEulerAngles = Vector3.zero;
    Camera _camera;
    string Layer = "Terrain";
    Rect viewRect = new Rect(0, 0, 1, 1);

    void Awake()
    {
        _camera = GetComponent<Camera>();
        int layerInt = ACTSystem.ACTSystemTools.NameToLayer(Layer);
        _camera.cullingMask &= ~(1 << layerInt);
        _camera.clearFlags = CameraClearFlags.SolidColor;
    }

    void Update()
    {
        if (target == null)
        {
            return;
        }
        foucsPoint = target.position;
        foucsPoint.y += height;
        foucsPoint.x += right;
        targetPos = foucsPoint + target.forward * targetDistance;
        this.transform.position = Vector3.Lerp(this.transform.position, targetPos, 0.12f);
        this.transform.position = targetPos;
        this.transform.RotateAround(foucsPoint, new Vector3(1, 0, 0), rotationX);
        this.transform.RotateAround(foucsPoint, new Vector3(0, 1, 0), rotationY);
        this.transform.LookAt(foucsPoint);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void SetArgs(float distance, float rotationX, float rotationY, float rectX, float height = 1f, float right = 0f)
    {
        this.targetDistance = distance;
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.height = height;
        this.right = right;
        target.localEulerAngles = localEulerAngles;
        viewRect.x = rectX;
        _camera.rect = viewRect;
    }
}

