﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using ShaderUtils;
using GameResource;
using GameLoader.Utils;
using LuaInterface;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using Common.States;

namespace GameMain
{
    public static class LuaPreloadPathsBuilder
    {
        static List<string> _paths = new List<string>();

        public static List<string> GetPaths()
        {
            return _paths;
        }

        public static void Clear()
        {
            _paths.Clear();
        }

        public static void AddPath(string path)
        {
            _paths.Add(path);
        }

        public static void AddActorID(int actorID)
        {
            _paths.Add(ACTSystemAdapter.GetActorModelPath(actorID));
        }

        public static void AddVisualFXID(int vfxId)
        {
            _paths.Add(ACTSystemAdapter.GetVisualFXPath(vfxId));
        }

        public static void AddSkillID(int vfxId)
        {
            
        }
    }
}
