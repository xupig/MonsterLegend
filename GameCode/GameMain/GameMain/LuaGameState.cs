﻿
using UnityEngine;
using Common.States;

namespace GameMain
{
    public static class LuaGameState
    {

        //摇杆方向c
        public static Vector2 controlStickDirection { set{ ControlStickState.direction = value;}  }
        //摇杆力度
        public static float controlStickStrength { set { ControlStickState.strength = value; } }

        public static bool controlStickIsDragging { set { ControlStickState.isDragging = value; } }

        public static int controlStickFingerId { set { ControlStickState.fingerId = value; } }

        //技能摇杆数据
        public static Vector2 skillStickDirection { set { SkillStickState.direction = value; } }
        public static float skillStickStrength { set { SkillStickState.strength = value; } }
        public static bool skillStickIsDragging { set { SkillStickState.isDragging = value; } }
        public static int skillStickFingerId { set { SkillStickState.fingerId = value; } }

        //锁定切换摇杆数据
        public static Vector2 lockStickDirection { set { LockStickState.direction = value; } }
        public static float lockStickStrength { set { LockStickState.strength = value; } }
        public static bool lockStickIsDragging { set { LockStickState.isDragging = value; } }
        public static int lockStickFingerId { set { LockStickState.fingerId = value; } }

        public static float bgMusicVolume { set { AudioState.bgMusicVolume = value; } }

        public static float uiVolume { set { AudioState.uiVolume = value; } }

        public static float speechVolume { set { AudioState.speechVolume = value; } }

        public static float battleVolume { set { AudioState.battleVolume = value; } }

        public static bool PlatfromIsAndroid()
        {
            return UnityPropUtils.Platform == RuntimePlatform.Android;
        }

        public static bool ScreenHasCutout()
        {
            if (Screen.width * 1.0 / Screen.height > 2.0)
            {
                return true;
            }
            return false;
        }

        //技能滑动状态 1：在滑动中 0：未滑动
        public static int skillDragState = 0;
    }
}
