﻿using UnityEngine;
using Common.Data;

namespace GameMain.Test
{
    public class TestChat:MonoBehaviour
    {

        void Awake()
        {
            //AddTextBlock(3);
        }

        protected void AddTextBlock(uint channel=3,ulong dbid = 2,string msg = "")
        {
            ChatTextBlock block = null;
            float maxWidth = 465.0f;
            PbChatInfoResp _data = new PbChatInfoResp();
            _data.SetTestData();
            _data.channel_id = channel;
            _data.send_dbid = dbid;
            _data.msg = msg;

            TestManager.GetInstance().ChatData.ChatLinkData.AddChatLinkInfo(_data);
            block = ChatTextBlock.CreateBlock(_data, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL, true);
            block.GameObject.transform.SetParent(gameObject.transform);
            RectTransform rect = block.GameObject.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
            rect.anchoredPosition3D = Vector3.zero;
            rect.anchoredPosition = new Vector2(42f, -6.0f);
            
        }

        public void TestAddText()
        {
            //TestChannel();

            //私聊 名字有问题
            //TestPrivateChannelName();

            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "交易中心{0,6081}等物品更新啦，快去瞧瞧吧！ [E1]  [E2]");
            //AddTextBlock(1, 3, "$(1,{0,PlayerName})求购{1,ItemId}，还有$(23,{2})个玩家也在求购这个物品 [去上架]");
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, " $(1,{ 0,PlayerName})求购[神油,7]|[7;3;测试;3]，还有$(23,3)个玩家也在求购这个物品[去上架] [E1]  [E2]");


            //================================测试数据============================================
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "公会[公会,3]|[3;22]");  //公会数据格式
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "物品[平常,1]|[1;1;1;4;3]");  //物品 平常 数据格式
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "物品[翅膀,1]|[1;2;1]");  //物品 翅膀 数据格式
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "物品[title,1]|[1;3;1;132424]  [E1]");  //物品 title 数据格式
            //AddTextBlock(1, (ulong)EntityPlayer.Player.id, "语音 [语音,2]|[2;3;1;132424] [E1]");  //物品 title 数据格式
            AddTextBlock(1, (ulong)EntityPlayer.Player.id, "表情测试[E1]");  //物品 title 数据格式
        }

        private void TestPrivateChannelName()
        {
            AddTextBlock(5,(ulong)EntityPlayer.Player.id);
            AddTextBlock(5,3);
        }

        private void TestChannel()
        {
            for (int i = 0; i < 10; i++)
            {
                ChangeDataChannel((uint)i);
            }
            ChangeDataChannel(22);
            ChangeDataChannel(23);
        }

        private void ChangeDataChannel(uint channel)
        {
            AddTextBlock(channel);
        }


        //====================================================================================

        public static bool IsSelf(PbChatInfoResp chatInfo)
        {
            return chatInfo.send_dbid == EntityPlayer.Player.id;
        }

        public static bool isSystem(PbChatInfoResp chatInfo)
        {
            return chatInfo.send_dbid == 0;
        }

        public static bool showTag(int showInChannel, PbChatInfoResp chatInfo)
        {
            return (showInChannel <= Common.ServerConfig.public_config.CHANNEL_ID_WORLD || isSystem(chatInfo));
        }

        public void SetToCurPlayer(PbChatInfoResp chatInfo)
        {
            chatInfo.send_dbid = EntityPlayer.Player.id;
        }

        public void SetToSystem(PbChatInfoResp chatInfo)
        {
            chatInfo.send_dbid = 0;
        }
    }
    
}
