﻿using UnityEngine;
using Common.Data;

namespace GameMain.Test
{
    public class TestManager
    {
        private static TestManager s_instance = null;
        public static TestManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new TestManager();
            }
            return s_instance;
        }

        private ChatData _chatData;
        public ChatData ChatData
        {
            get
            {
                if (_chatData == null)
                {
                    _chatData = new ChatData();
                }
                return _chatData;
            }
        }

        public void Init(GameObject go,int type)
        {
            GameLoader.Utils.LoggerHelper.Debug("TestManager Init");
            switch(type)
            {
                case 1:
                    if (go.GetComponent<TestChat>() == null)
                    {
                        go.AddComponent<TestChat>();
                    }
                    else
                    {
                        go.GetComponent<TestChat>().TestAddText();
                    }
                    break;
            }
        }
    }
}
