﻿#region 模块信息
/*==========================================
// 文件名：PlayerAvatarState
// 命名空间: Common.States
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/13 13:42:58
// 描述说明：不用做主城UI和战斗UI显示与否控制
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.States
{
    public static class GMState
    {
        public static bool showBlood = true;
        public static bool showLifeBar = true;
        public static bool showPointingArrow = true;
        public static bool showName = true;
        public static bool showSpeech = true;
        public static bool showheartbeatinfo = false;
        public static bool staticBatching = true;
        public static bool openMainChatAnim = true;
        public static bool keyboardCtrl = true;
        public static bool enableSortOrderedRender = true;
        public static bool enableVoiceTest = false;
        public static bool includeWindowsEditor = true;
    }
}
