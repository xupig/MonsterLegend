﻿using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.States
{
    public static class SkillStickState
    {
        //摇杆方向
        public static Vector2 direction = Vector2.zero;
        //摇杆力度
        public static float strength = 0;
        public static bool isDragging = false;
        public static int fingerId = -1;

        public static Vector3 GetMoveDirectionBySkillStick()
        {
            if (CameraManager.GetInstance().CameraTransform == null) return Vector3.zero;
            Vector3 moveRight = CameraManager.GetInstance().CameraTransform.right;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveRight.y = 0;
            }
            moveRight.Normalize();

            Vector3 moveForward = CameraManager.GetInstance().CameraTransform.forward;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveForward.y = 0;
            }
            moveForward.Normalize();

            Vector3 moveDirection = moveRight * SkillStickState.direction.x +
                                    moveForward * SkillStickState.direction.y;
            moveDirection.Normalize();
            return moveDirection;
        }
    }
}
