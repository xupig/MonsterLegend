﻿using ACTSystem;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.States
{
    public static class ControlStickState
    {
        //摇杆方向
        public static Vector2 direction = Vector2.zero;
        //摇杆力度
        public static float strength = 0;

        public static bool isDragging = false;

        public static int fingerId = -1;

        private static bool _canUse = true;
        public static bool canUse
        {
            get { return _canUse; }
            set
            {
                _canUse = value;
            }
        }


        public static Vector3 GetMoveDirectionByControlStick()
        {
            if (CameraManager.GetInstance().CameraTransform == null) return Vector3.zero;
            Vector3 moveRight = CameraManager.GetInstance().CameraTransform.right;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveRight.y = 0;
            }
            moveRight.Normalize();

            Vector3 moveForward = CameraManager.GetInstance().CameraTransform.forward;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveForward.y = 0;
            }
            moveForward.Normalize();

            Vector3 moveDirection = moveRight * ControlStickState.direction.x +
                                    moveForward * ControlStickState.direction.y;
            moveDirection.Normalize();
            return moveDirection;
        }

        public static Vector3 GetMoveDirectionByMouse()
        {
            RaycastHit hit;
            int _terrainLayerValue = 1 << ACTSystemTools.NameToLayer("Terrain");
            
            //如果主摄像机被关闭时返回0
            if (CameraManager.GetInstance().Camera == null)
            {
                //Ray ray = UIManager.Instance.UICamera.camera.ScreenPointToRay(Input.mousePosition);
                return Vector3.zero;
            }

            Ray ray = CameraManager.GetInstance().Camera.ScreenPointToRay(Input.mousePosition);
            bool result = Physics.Raycast(ray, out hit, 1000, _terrainLayerValue);
            var moveDirection = hit.point;
            return moveDirection;
            //return Input.mousePosition;
        }

        public static int GetDirectionZone()
        {
            if (ControlStickState.strength == 0) return 0;
            float x = ControlStickState.direction.x;
            float y = ControlStickState.direction.y;
            float angle = Mathf.Atan(x / y);
            angle = angle * Mathf.Rad2Deg;
            if (y < 0)angle = angle + 180;
            else if (x < 0) angle = angle + 360;
            if (angle <= 45 || angle >= 315) return 1;
            else if (angle <= 135 && angle >= 45) return 2;
            else if (angle <= 225 && angle >= 135) return 3;
            return 4;
        }

        static Vector3 dirV3 = Vector3.zero;
        static Vector3 stickV3 = Vector3.zero;
        static Vector3 v3 = Vector3.zero;
        static int setAngle = 0;
        public static int GetSkillDirectionZone()
        {
            if (ControlStickState.strength == 0) return 0;
            if (GameMain.EntityPlayer.Player.target_id == 0) return 0;
            if (!MogoEngine.MogoWorld.Entities.ContainsKey(GameMain.EntityPlayer.Player.target_id))
            {
                return 0;
            }
            GameMain.EntityCreature creature = MogoEngine.MogoWorld.Entities[GameMain.EntityPlayer.Player.target_id] as GameMain.EntityCreature;
            if (GameMain.EntityPlayer.Player.actor == null || creature == null) return 0;
            Vector3 _from = CameraManager.GetInstance().Camera.WorldToScreenPoint(GameMain.EntityPlayer.Player.position);
            Vector3 _to = CameraManager.GetInstance().Camera.WorldToScreenPoint(creature.position);
            dirV3 = _to - _from;        
            dirV3.z = dirV3.y;
            dirV3.y = 0;

            stickV3.x = ControlStickState.direction.x;
            stickV3.y = 0;
            stickV3.z = ControlStickState.direction.y;

            dirV3.Normalize();
            stickV3.Normalize();
            v3 = Vector3.Cross(dirV3, stickV3);
            float angle = Vector3.Angle(dirV3, stickV3);
            if (v3.y < 0)
                angle = 360 - angle;
            setAngle = int.Parse(GameData.global_params_helper.GetGlobalParam(306));

            if (angle <= setAngle || angle >= 360 - setAngle) return 1;
            else if (angle <= 180 - setAngle && angle >= setAngle) return 2;
            else if (angle <= 180 + setAngle && angle >= 180 - setAngle) return 3;
            return 4;
        }
    }
}
