﻿#region 模块信息
/*==========================================
// 文件名：PlayerAvatarState
// 命名空间: Common.States
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/13 13:42:58
// 描述说明：不用做主城UI和战斗UI显示与否控制
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.States
{

    public enum AvatarState
    {
        Normal,
        Battle,
    }
    
    public static class PlayerAvatarState
    {
        public static AvatarState state = AvatarState.Battle;
        public static bool isPowerfully = false;
        public static bool isWeakly = false;
    }
}
