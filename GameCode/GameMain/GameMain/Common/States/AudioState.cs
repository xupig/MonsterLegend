﻿namespace Common.States
{
    public static class AudioState
    {
        public static float bgMusicVolume = 1;

        public static float uiVolume = 1;

        public static float speechVolume = 1;

        public static float battleVolume = 1;
    }
}
