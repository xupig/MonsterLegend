﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class color_helper
    {
        static Dictionary<int, UnityEngine.Color> colorDict = new Dictionary<int, UnityEngine.Color>();

        public static color GetColorData(int colorId)
        {
            if (XMLManager.color.ContainsKey(colorId))
            {
                return XMLManager.color[colorId];
            }
            return null;
        }

        public static UnityEngine.Color GetColor(int colorId)
        {
            if (colorDict.ContainsKey(colorId))
            {
                return colorDict[colorId];
            }
            else
            {
                color data = GetColorData(colorId);
                UnityEngine.Color color = new UnityEngine.Color((data.__r / 255.0f), (data.__g / 255.0f), (data.__b / 255.0f));
                colorDict[colorId] = color;
                return color;
            }
        }

        public static string GetColorString(int colorId)
        {
            if (colorId == 0) return "";
            UnityEngine.Color resultColor = GetColor(colorId);
            return UnityEngine.ColorUtility.ToHtmlStringRGB(resultColor);
        }
    }
}
