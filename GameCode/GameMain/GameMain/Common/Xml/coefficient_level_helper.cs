﻿#region 模块信息
/*==========================================
// 文件名：style_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/10 19:39:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class coefficient_level_helper
    {
        public static coefficient_level GetData(int level)
        {
            if (XMLManager.coefficient_level.ContainsKey(level))
            {
                return XMLManager.coefficient_level[level];
            }
            else {
                LoggerHelper.Error("XMLManager.coefficient_level id error by " + level);
            }
            return null;
        }

        public static float GetLevelPram1(int level)
        {
            var data = GetData(level);
            return data.__levelpram1;
        }

        public static float GetLevelPram2(int level)
        {
            var data = GetData(level);
            return data.__levelpram2;
        }

        public static float GetLevelPram3(int level)
        {
            var data = GetData(level);
            return data.__levelpram3;
        }

        public static float GetLevelPram4(int level)
        {
            var data = GetData(level);
            return data.__levelpram4;
        }
    }
}
