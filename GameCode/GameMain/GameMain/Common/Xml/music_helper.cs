﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class music_helper
    {
        private static music_helper _instance = null;
        public static music_helper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new music_helper();
            }
            return _instance;
        }

        public music GetBgMusicByMusicID(int musicID)
        {
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        public music GetMusicData(int musicID)
        {
            if (!XMLManager.music.ContainsKey(musicID)) return null;
            return XMLManager.music[musicID];
        }

        public float GetMusicVolumn(int musicID)
        {
            if (!XMLManager.music.ContainsKey(musicID)) return 0.0f;
            return XMLManager.music[musicID].__volume;
        }

        private int GetRandomMusic(List<string> musicList)
        {
            if (musicList.Count > 1)
            {
                int rndNum = RandomUtils.GetRandomInt(1000) % musicList.Count;
                return int.Parse(musicList[rndNum]);
            }
            //目前就是返回第一首音乐
            return int.Parse(musicList[0]);

        }


    }
}
