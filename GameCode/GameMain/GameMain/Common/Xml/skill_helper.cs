﻿#region 模块信息
/*==========================================
// 文件名：skill_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 11:23:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class skill_helper
    {
        private static Dictionary<int, Dictionary<int, spell>> spellDict;
        private static Dictionary<int, List<int>> spellList;

        public static List<int> GetSpellGroupList(int groupId)
        {
            InitData();
            if (!spellList.ContainsKey(groupId))
            {
                return null;
            }
            return spellList[groupId];
        }

        private static void InitData()
        {
            if (spellDict == null)
            {
                spellDict = new Dictionary<int, Dictionary<int, spell>>();
                spellList = new Dictionary<int, List<int>>();
            }
        }

    }
}
