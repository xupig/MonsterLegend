﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class video_helper
    {
        public static video GetVideoData(int id)
        {
            if (!XMLManager.video.ContainsKey(id))
            {
                throw new Exception("[视频配置表]未包含项: " + id.ToString());
            }
            return XMLManager.video[id];
        }

        public static string GetPath(int id)
        {
            video vi = GetVideoData(id);
            if (vi != null)
            {
                return vi.__path;
            }
            LoggerHelper.Error("[sound_helper:Path]=>ID: " + id + " 的视频路径是空,把[视频配置表]中数据配全!");
            return string.Empty;
        }

        public static List<string> GetAllPath()
        {
            List<string> result = new List<string>();
            var keys = XMLManager.video.Keys;
            for (int i = 0; i < keys.Count; i++)
            {
                result.Add(XMLManager.video[keys[i]].__path);
            }
            return result;
        }

        public static string TransformPathToPhysicalPath(string path)
        {
            string physicalPath = path.Replace("\\", "/");
            physicalPath = path.Replace("/", "$");
            physicalPath = physicalPath.ToLower();
            physicalPath = physicalPath + ".u";
            return physicalPath;
        }
    }
}
