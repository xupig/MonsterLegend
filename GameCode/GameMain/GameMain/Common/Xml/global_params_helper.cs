﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Global;
using UnityEngine;
using Common.Utils;
using GameLoader.Utils;

namespace GameData
{
    public enum GlobalParamId
    {
        lockTargetDis = 21,
        player_running_camera_setting = 320,
        one_vs_one_params = 181,
        one_vs_one_back_params = 186,
        touches_camera_params = 202,
        touches_camera_screen_region = 203,
        mouse_camera_params = 210,
        touches_camera_default_rate = 213,
        touches_camera_back_duration = 214,
        rotate_camera_params = 229,
        lock_two_rotate_rate = 300,
        camera_target = 301,
        camera_h_rotate_params = 302,
        default_camera_h_rotate_params = 303,
        camera_lock_target_time = 305,
        powerplus_lock_camera_params = 322,
        distance_limit = 889,
    }

    public class global_params_helper
    {
        public static float[] _highLightSetting;
        private static Dictionary<int, int> _instancePetDict;
        private static List<int> _standByActionList;
        //private static bool _hasInitAiIntervalParams = false;//为排除警告，注释掉

        public static void OnReloadData()
        {
            
        }

        public static string GetGlobalParam(int id)
        {
            global_params config = GetConfig(id);
            return config.__value;
        }

        public static string GetGlobalParam(GlobalParamId id)
        {
            global_params config = GetConfig((int)id);
            return config.__value;
        }

        public static List<float> GetListFloat(GlobalParamId id)
        {
            return data_parse_helper.ParseListFloat(GetGlobalParam(id));            
        }

        public static List<int> GetListInt(GlobalParamId id)
        {
            return data_parse_helper.ParseListInt(GetGlobalParam(id));
        }

        public static List<string> GetListString(GlobalParamId id)
        {
            return data_parse_helper.ParseListString(GetGlobalParam(id));
        }

        public static T[] StringToArray<T>(string strParam, Func<string, T> transform)
        {
            string[] strParamArray = strParam.Split(',');
            T[] paramArray = new T[strParamArray.Length];
            for (int i = 0; i < strParamArray.Length; i++)
            {
                paramArray[i] = transform(strParamArray[i]);
            }
            return paramArray;
        }

        public static global_params GetConfig(int id)
        {
            if(XMLManager.global_params.ContainsKey(id) == false)
            {
                throw new Exception("配置表global_params.xml 未找到配置项： " + id);
            }
            return XMLManager.global_params[id];
        }

        public static float[] GetHighLightSetting()
        {
            if (_highLightSetting == null)
            {
                var data = GetConfig(98);
                var temp = data.__value.Split(',');
                _highLightSetting = new float[temp.Length];
                for(int i = 0; i < temp.Length; i++)
                {
                    _highLightSetting[i] = float.Parse(temp[i]);
                }
            }
            return _highLightSetting;
        }

        public static Dictionary<int, int> GetInstancePetDict()
        {
            if (_instancePetDict == null)
            {
                _instancePetDict = new Dictionary<int, int>();
                var data = GetConfig(140);
                var temp = data.__value.Split(',');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _instancePetDict.Add(key, value);
                }
            }
            return _instancePetDict;
        }

        public static List<int> GetPlayerStandByList()
        {
            if (_standByActionList != null)
            {
                return _standByActionList;
            }
            string[] args = global_params_helper.GetGlobalParam(154).Split(',');
            _standByActionList = new List<int>();
            for (int i = 2; i < args.Length; ++i)
            {
                _standByActionList.Add(int.Parse(args[i]));
            }
            return _standByActionList;
        }

        private static List<float> _dmgLevelFactors = null;
        public static List<float> DmgLevelFactors
        {
            get
            {
                if (_dmgLevelFactors == null)
                {
                    var value = GameData.XMLManager.global_params[7].__value;
                    _dmgLevelFactors = data_parse_helper.ParseListFloat(value);
                }
                return _dmgLevelFactors;
            }
        }

        private static List<float> _cameraInitial = null;
        public static List<float> CameraInitial
        {
            get
            {
                if (_cameraInitial == null)
                {
                    _cameraInitial = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(76).Split(','));
                }
                return _cameraInitial;
            }
        }

        private static float _defaultGravity = 0;
        public static float defaultGravity
        {
            get
            {
                if (_defaultGravity == 0)
                {
                    var value = GameData.XMLManager.global_params[5].__value;
                    _defaultGravity = float.Parse(value);
                }
                return _defaultGravity;
            }
        }

        private static float _maxFlyH = -1;
        public static float maxFlyH
        {
            get
            {
                if (_maxFlyH == -1)
                {
                    var value = GameData.XMLManager.global_params[310].__value;
                    _maxFlyH = float.Parse(value);
                }
                return _maxFlyH;
            }
        }

        private static float _minFlyH = -1;
        public static float minFlyH
        {
            get
            {
                if (_minFlyH == -1)
                {
                    var value = GameData.XMLManager.global_params[309].__value;
                    _minFlyH = float.Parse(value);
                }
                return _minFlyH;
            }
        }

        private static int _glideSpeedH = -1;
        public static int glideSpeedH
        {
            get
            {
                if (_glideSpeedH == -1)
                {
                    var value = GameData.XMLManager.global_params[316].__value;
                    _glideSpeedH = int.Parse(value);
                }
                return _glideSpeedH;
            }
        }

        private static int _glideSpeedV = -1;
        public static int glideSpeedV
        {
            get
            {
                if (_glideSpeedV == -1)
                {
                    var value = GameData.XMLManager.global_params[317].__value;
                    _glideSpeedV = int.Parse(value);
                }
                return _glideSpeedV;
            }
        }

        private static int _glideEndDelBuff = -1;
        public static int glideEndDelBuff
        {
            get
            {
                if (_glideEndDelBuff == -1)
                {
                    var value = GameData.XMLManager.global_params[315].__value;
                    _glideEndDelBuff = int.Parse(value);
                }
                return _glideEndDelBuff;
            }
        }

        private static int _glideAngleSpeed = 0;
        public static int glideAngleSpeed
        {
            get
            {
                if (_glideAngleSpeed == 0)
                {
                    var value = GameData.XMLManager.global_params[318].__value;
                    _glideAngleSpeed = int.Parse(value);
                }
                return _glideAngleSpeed;
            }
        }

        private static int _glideEndSkill = 0;
        public static int glideEndSkill
        {
            get
            {
                if (_glideEndSkill == 0)
                {
                    var value = GameData.XMLManager.global_params[318].__value;
                    _glideEndSkill = int.Parse(value);
                }
                return _glideEndSkill;
            }
        }

        private static Dictionary<int, int> _glideLandingSkillId = null;
        public static Dictionary<int, int> GetGlideLandingSkillId()
        {
            if (_glideLandingSkillId == null)
            {
                _glideLandingSkillId = new Dictionary<int, int>();
                var data = GetConfig(314);
                var temp = data.__value.Split(';');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _glideLandingSkillId.Add(key, value);
                }
            }
            return _glideLandingSkillId;
        }

        private static Dictionary<int, int> _glideStartSkillId = null;
        public static Dictionary<int, int> GetGlideStartSkillId()
        {
            if (_glideStartSkillId == null)
            {
                _glideStartSkillId = new Dictionary<int, int>();
                var data = GetConfig(313);
                var temp = data.__value.Split(';');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _glideStartSkillId.Add(key, value);
                }
            }
            return _glideStartSkillId;
        }

        private static Dictionary<int, int> _flyStartSkillId = null;
        public static Dictionary<int, int> GetFlyStartSkillId()
        {
            if (_flyStartSkillId == null)
            {
                _flyStartSkillId = new Dictionary<int, int>();
                var data = GetConfig(311);
                var temp = data.__value.Split(';');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _flyStartSkillId.Add(key, value);
                }
            }
            return _flyStartSkillId;
        }

        private static Dictionary<int, int> _flyLandingSkillId = null;
        public static Dictionary<int, int> GetFlyLandingSkillId()
        {
            if (_flyLandingSkillId == null)
            {
                _flyLandingSkillId = new Dictionary<int, int>();
                var data = GetConfig(312);
                var temp = data.__value.Split(';');
                for (int i = 0; i < temp.Length; i++)
                {
                    int key = int.Parse(temp[i].Split(':')[0]);
                    int value = int.Parse(temp[i].Split(':')[1]);
                    _flyLandingSkillId.Add(key, value);
                }
            }
            return _flyLandingSkillId;
        }

        private static int _lockCameraType = -1;
        public static int lockCameraType
        {
            get
            {
                if (_lockCameraType == -1)
                {
                    var value = GameData.XMLManager.global_params[321].__value;
                    _lockCameraType = int.Parse(value);
                }
                return _lockCameraType;
            }
        }

        private static List<float> _weaponSwitchParams = null;
        public static List<float> weaponSwitchParams
        {
            get
            {
                if (_weaponSwitchParams == null)
                {
                    var value = GameData.XMLManager.global_params[324].__value;
                    _weaponSwitchParams = data_parse_helper.ParseListFloat(value);
                }
                return _weaponSwitchParams;
            }
        }

        private static float _map_mount_speed = 0;
        public static float map_mount_speed
        {
            get
            {
                if (_map_mount_speed == 0)
                {
                    _map_mount_speed = float.Parse(GetGlobalParam(376)) * 0.01f;
                }
                return _map_mount_speed;
            }
        }

        private static float _no_fight_state_time = 0;
        public static float no_fight_state_time
        {
            get
            {
                if (_no_fight_state_time == 0)
                {
                    _no_fight_state_time = float.Parse(GetGlobalParam(482));
                }
                return _no_fight_state_time;
            }
        }

        private static float _no_move_state_time = 0;
        public static float no_move_state_time
        {
            get
            {
                if (_no_move_state_time == 0)
                {
                    _no_move_state_time = float.Parse(GetGlobalParam(498));
                }
                return _no_move_state_time;
            }
        }

        private static float _player_ai_search_range = 0;
        public static float player_ai_search_range
        {
            get
            {
                if (_player_ai_search_range == 0)
                {
                    _player_ai_search_range = float.Parse(GetGlobalParam(543)) * 0.01f;
                }
                return _player_ai_search_range;
            }
        }

        private static float _early_warning_adjust_y = 0;
        public static float early_warning_adjust_y
        {
            get
            {
                if (_early_warning_adjust_y == 0)
                {
                    _early_warning_adjust_y = float.Parse(GetGlobalParam(713)) * 0.01f;
                }
                return _early_warning_adjust_y;
            }
        }

        private static float _death_fly_time = 0;
        public static float death_fly_time
        {
            get
            {
                if (_death_fly_time == 0)
                {
                    _death_fly_time = float.Parse(GetGlobalParam(925)) * 0.001f;
                }
                return _death_fly_time;
            }
        }

        private static List<float> _death_fly_move_args = null;
        public static List<float> death_fly_move_args
        {
            get
            {
                if (_death_fly_move_args == null)
                {
                    _death_fly_move_args = data_parse_helper.ParseListFloat(GetGlobalParam(926));
                }
                return _death_fly_move_args;
            }
        }

        private static float _hit_state_exit_map_time = 0;
        public static float hit_state_exit_map_time
        {
            get
            {
                if (_hit_state_exit_map_time == 0)
                {
                    _hit_state_exit_map_time = int.Parse(GetGlobalParam(975)) * 0.001f;
                }
                return _hit_state_exit_map_time;
            }
        }

        # region 超过距离不处理战斗协议,包括buff、spell、spell_action等
        private static float _distance_limit = 0f;
        public static float distance_limit
        {
            get
            {
                if (_distance_limit == 0f)
                {
                    _distance_limit = float.Parse(GameData.XMLManager.global_params[(int)GlobalParamId.distance_limit].__value);
                }
                return _distance_limit;
            }
        }
        # endregion
    }
}
