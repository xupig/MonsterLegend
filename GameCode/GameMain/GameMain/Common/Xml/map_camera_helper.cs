﻿using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class map_camera_helper
    {
        public static map_camera GetMapCamera(int id)
        {
            if (XMLManager.map_camera.ContainsKey(id))
            {
                return XMLManager.map_camera[id];
            }
            LoggerHelper.Error("配置表map_camera.xml 未找到配置项： " + id);
            return null;
        }

        public static List<float> GetCameraInitial(int id)
        {
            map_camera item = GetMapCamera(id);
            if (item != null)
                return data_parse_helper.ParseListFloat(item.__camera_initial);
            else
                return null;
        }
    }
}


