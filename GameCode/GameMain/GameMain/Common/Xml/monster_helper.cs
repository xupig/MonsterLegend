﻿using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class monster_helper
    {
        public static monster GetMonsterData(int id)
        {
            if (XMLManager.monster.ContainsKey(id))
            {
                return XMLManager.monster[id];
            }
            LoggerHelper.Error("配置表monster.xml 未找到配置项： " + id);
            return null;
        }

        public static int GetMonsterType(int id)
        {
            monster item = GetMonsterData(id);
            return item.__monster_type;
        }

        public static float GetBeHitRadius(int id)
        {
            monster item = GetMonsterData(id);
            return item.__be_hit_radius * 0.01f;
        }

        public static float GetBeHitHeight(int id)
        {
            monster item = GetMonsterData(id);
            return item.__be_hit_height * 0.01f;
        }

        public static bool GetShowDamage(int id)
        {
            monster item = GetMonsterData(id);
            return item.__show_damage == 1;
        }

        public static bool GetShowDeathFly(int id)
        {
            monster item = GetMonsterData(id);
            return item.__die_fly == 1;
        }
    }
}


