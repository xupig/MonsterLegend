﻿#region 模块信息
/*==========================================
// 文件名：icon_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:20:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    public class IconAnimSetting
    {
        public string[] iconNames;
    }

    public class icon_anim_helper
    {
        public static icon_anim GetIconAnim(int iconId)
        {
            if (XMLManager.icon_anim.ContainsKey(iconId))
            {
                return XMLManager.icon_anim[iconId];
            }
            if (UnityPropUtils.IsEditor) LoggerHelper.Error("配置表icon_anim.xml 未找到配置项： " + iconId);
            return null;
        }

        private static Dictionary<int, IconAnimSetting> _iconAnimSettingCache = new Dictionary<int, IconAnimSetting>();
        public static IconAnimSetting GetIconAnimSetting(int iconId)
        {
            if (!_iconAnimSettingCache.ContainsKey(iconId))
            {
                icon_anim _icon = GetIconAnim(iconId);
                if (_icon != null)
                {
                    var setting = new IconAnimSetting();
                    var list = data_parse_helper.ParseListString(_icon.__icon);
                    setting.iconNames = list.ToArray();
                    _iconAnimSettingCache[iconId] = setting;
                }
            }
            return _iconAnimSettingCache[iconId];
        }

        public static string[] GetIcons(int iconId)
        {
            var setting = GetIconAnimSetting(iconId);
            if (setting != null)
            {
                return setting.iconNames;
            }
            return null;
        }
    }
}
