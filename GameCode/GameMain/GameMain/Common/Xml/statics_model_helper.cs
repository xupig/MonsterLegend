﻿#region 模块信息
/*==========================================
// 文件名：style_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/10 19:39:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class statics_model_helper
    {
        public static statics_model GetStyle(int modelId)
        {
            if (XMLManager.statics_model.ContainsKey(modelId))
            {
                return XMLManager.statics_model[modelId];
            }
            else {
                LoggerHelper.Error("XMLManager.statics_model id error by " + modelId);
            }
            return null;
        }

        public static string GetModelPath(int modelId)
        {
            var data = GetStyle(modelId);
            return data.__prefab;
        }
    }
}
