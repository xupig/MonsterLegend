﻿using UnityEngine;

namespace Common.Global
{
    public enum PlatformMode
    {
        PC = 0,
        MOBILE,
        EDITOR
    }
    public class PlatformHelper
    {
        private static PlatformMode _platformMode = PlatformMode.PC;
        private static bool _initialized = false;

        public static PlatformMode GetPlatformMode()
        {
            Initialize();
            return _platformMode;
        }

        public static bool InPlatformMode(PlatformMode platformMode)
        {
            Initialize();
            return _platformMode == platformMode;
        }

        public static void SetPlatformMode(PlatformMode platformMode)
        {
            _initialized = true;
            _platformMode = platformMode;
        }

        public static bool InPCPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.PC;
        }

        public static bool InTouchPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.MOBILE;
        }

        public static bool InEditorPlatform()
        {
            Initialize();
            return _platformMode == PlatformMode.EDITOR;
        }

        private static void Initialize()
        {
            if (_initialized)
            {
                return;
            }
            _initialized = true;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.WindowsPlayer:
                    _platformMode = PlatformMode.PC;
                    break;
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    _platformMode = PlatformMode.MOBILE;
                    break;
                case RuntimePlatform.WindowsEditor:
                    _platformMode = PlatformMode.EDITOR;
                    break;
                default:
                    _platformMode = PlatformMode.MOBILE;
                    break;
            }
        }
    }
}
