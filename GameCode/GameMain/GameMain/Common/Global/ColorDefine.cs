﻿#region 模块信息
/*==========================================
// 文件名：ColorDefine
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/5 13:43:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Utils;
using GameLoader.Utils;
using Common.ClientConfig;

namespace Common.Global
{
    public static class ColorDefine
    {
        public static Dictionary<int, string> _colorNameDict = new Dictionary<int, string>();
        public static Dictionary<int, int> _newQualityColorDict = new Dictionary<int, int>();

        /// <summary>
        /// 白色ID
        /// </summary>
        public const int COLOR_ID_WHITE = 1;
        /// <summary>
        /// 绿色ID
        /// </summary>
        public const int COLOR_ID_GREEN = 2;
        /// <summary>
        /// 蓝色ID
        /// </summary>
        public const int COLOR_ID_BLUE = 3;
        /// <summary>
        /// 紫色ID
        /// </summary>
        public const int COLOR_ID_PURPLE = 4;
        /// <summary>
        /// 橙色ID
        /// </summary>
        public const int COLOR_ID_ORANGE = 5;
        /// <summary>
        /// 暗金ID
        /// </summary>
        public const int COLOR_ID_DARK_GOLDEN = 6;
        /// <summary>
        /// 红色ID
        /// </summary>
        public const int COLOR_ID_RED = 7;
        /// <summary>
        /// 金色ID
        /// </summary>
        public const int COLOR_ID_GOLDEN = 8;
        /// <summary>
        /// 深蓝ID
        /// </summary>
        public const int COLOR_ID_DARK_BLUE = 9;
        /// <summary>
        /// 天蓝ID
        /// </summary>
        public const int COLOR_ID_SKY_BLUE = 10;
        /// <summary>
        /// 粉色ID
        /// </summary>
        public const int COLOR_ID_PINK = 11;
        /// <summary>
        /// 灰色ID
        /// </summary>
        public const int COLOR_ID_GRAY = 12;

        /// <summary>
        /// 综合频道
        /// </summary>
        public const int COLOR_ID_CHAT_ALL = 13;
        /// <summary>
        /// 世界频道
        /// </summary>
        public const int COLOR_ID_CHAT_WORLD = 14;
        /// <summary>
        /// 附近频道
        /// </summary>
        public const int COLOR_ID_CHAT_NEARBY = 15;
        /// <summary>
        /// 队伍频道
        /// </summary>
        public const int COLOR_ID_CHAT_TEAM = 16;
        /// <summary>
        /// 公会频道
        /// </summary>
        public const int COLOR_ID_CHAT_GUILD = 17;
        /// <summary>
        /// 密聊频道
        /// </summary>
        public const int COLOR_ID_CHAT_PRIVATE = 18;
        /// <summary>
        /// 帮助频道
        /// </summary>
        public const int COLOR_ID_CHAT_HELP = 19;
        /// <summary>
        /// 传闻频道
        /// </summary>
        public const int COLOR_ID_CHAT_RUMOR = 20;

        public const int COLOR_NUMBER_GREEN = 22;

        public const int COLOR_NUMBER_BLUE = 23;

        public const int PLAYER_AVATAR_INFO_COLOR = 30;
        public const int ENTITY_AVATAR_INFO_COLOR = 31;
        public const int ENTITY_NPC_INFO_COLOR = 32;

        /// <summary>
        /// 新绿色ID
        /// </summary>
        public const int COLOR_ID_NEW_GREEN = 33;
        /// <summary>
        /// 新蓝色ID
        /// </summary>
        public const int COLOR_ID_NEW_BLUE = 34;
        /// <summary>
        /// 新紫色ID
        /// </summary>
        public const int COLOR_ID_NEW_PURPLE = 35;
        /// <summary>
        /// 新橙色ID
        /// </summary>
        public const int COLOR_ID_NEW_ORANGE = 36;
        /// <summary>
        /// 新暗金ID
        /// </summary>
        public const int COLOR_ID_NEW_DARK_GOLDEN = 37;

        /// <summary>
        /// 角色头顶公会名称颜色id
        /// </summary>
        public const int COLOR_ID_GUILD_NAME = 48;

        /// <summary>
        /// 套装件数满足时颜色
        /// </summary>
        public const int COLOR_ID_SUIT_MEET = 49;

        /// <summary>
        /// 套装件数不满足时颜色
        /// </summary>
        public const int COLOR_ID_SUIT_NOT_MEET = 50;

        /// <summary>
        /// 文字描述金色ID
        /// </summary>
        public const int COLOR_ID_TEXT_GOLDEN = 57;

        private static Dictionary<int, Color> _colorDic = new Dictionary<int, Color>();
        private static Dictionary<int, string> _colorHexTokenDict = new Dictionary<int, string>();

        public static Color GetColorById(int colorId)
        {
            if(_colorDic.ContainsKey(colorId))
            {
                return _colorDic[colorId];
            }
            if(XMLManager.color.ContainsKey(colorId) == true)
            {
                color color = XMLManager.color[colorId];
                Color c = new Color((float)color.__r / 255, (float)color.__g / 255, (float)color.__b / 255);
                _colorDic.Add(colorId, c);
                return c;
            }
            return Color.white;
        }

        public static string GetColorHexToken(int colorId)
        {
            if(_colorHexTokenDict.ContainsKey(colorId) == true)
            {
                return _colorHexTokenDict[colorId];
            }
            if(XMLManager.color.ContainsKey(colorId) == true)
            {
                color color = XMLManager.color[colorId];
                string token = string.Format("#{0:X2}{1:X2}{2:X2}", color.__r, color.__g, color.__b);
                _colorHexTokenDict.Add(colorId, token);
                return token;
            }
            return "#FFFFFF";
        }

        public static string GetColorHtmlString(int colorId, string content)
        {
            return string.Format("<color={0}>{1}</color>", GetColorHexToken(colorId), content);
        }

        public static string GetQualityWithColorHtmlString(int qualityId, string content)
        {
            int colorId = GetNewQualityColorId(qualityId);
            return GetColorHtmlString(colorId, content);
        }

        static ColorDefine()
        {
            _colorNameDict.Add(public_config.ITEM_QUALITY_GREEN, GameLanguageUtil.GetString(LangEnum.ITEM_QUALITY_GREEN));
            _colorNameDict.Add(public_config.ITEM_QUALITY_BLUE, GameLanguageUtil.GetString(LangEnum.ITEM_QUALITY_BLUE));
            _colorNameDict.Add(public_config.ITEM_QUALITY_PURPLE, GameLanguageUtil.GetString(LangEnum.ITEM_QUALITY_PURPLE));
            _colorNameDict.Add(public_config.ITEM_QUALITY_ORANGE, GameLanguageUtil.GetString(LangEnum.ITEM_QUALITY_ORANGE));
            _colorNameDict.Add(public_config.ITEM_QUALITY_GOLDEN, GameLanguageUtil.GetString(LangEnum.ITEM_QUALITY_GOLDEN));

            _newQualityColorDict.Add(public_config.ITEM_QUALITY_WHITE, ColorDefine.COLOR_ID_WHITE);
            _newQualityColorDict.Add(public_config.ITEM_QUALITY_GREEN, ColorDefine.COLOR_ID_NEW_GREEN);
            _newQualityColorDict.Add(public_config.ITEM_QUALITY_BLUE, ColorDefine.COLOR_ID_NEW_BLUE);
            _newQualityColorDict.Add(public_config.ITEM_QUALITY_PURPLE, ColorDefine.COLOR_ID_NEW_PURPLE);
            _newQualityColorDict.Add(public_config.ITEM_QUALITY_ORANGE, ColorDefine.COLOR_ID_NEW_ORANGE);
            _newQualityColorDict.Add(public_config.ITEM_QUALITY_GOLDEN, ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
        }

        public static string GetColorName(int colorId)
        {
            if (!_colorNameDict.ContainsKey(colorId))
            {
                LoggerHelper.Debug("Config not exist! colorId is:" + colorId);
            }
            return _colorNameDict[colorId];
        }

        public static Color GetNewQualityColor(int quality)
        {
            if (!_newQualityColorDict.ContainsKey(quality))
            {
                LoggerHelper.Error("_newQualityColorDict 不存在品质:" + quality);
            }
            return GetColorById(_newQualityColorDict[quality]);
        }

        public static int GetNewQualityColorId(int quality)
        {
            if (!_newQualityColorDict.ContainsKey(quality))
            {
                LoggerHelper.Error("_newQualityColorDict  不存在品质:" + quality);
                return 0;
            }
            return _newQualityColorDict[quality];
        }
    }
}
