﻿#region 模块信息
/*==========================================
// 文件名：EntityPropertyDefine
// 命名空间: Common.ClientConfig
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/14 23:36:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ClientConfig
{
    public class EntityPropertyDefine
    {
        public static string fight_force = "fight_force";
        public static string money_gold = "money_gold";
        public static string money_diamond = "money_diamond";
        public static string money_bind_diamond = "money_bind_diamond";
        public static string honour = "honour";
        public static string money_rune_stone = "money_rune_stone";
        public static string fortress_wood = "fortress_wood";
        public static string fortress_stone = "fortress_stone";
        public static string level = "level";
        public static string energy = "energy";
        public static string exp = "exp";
        public static string name = "name";
        public static string spirit_level = "spirit_level";
        public static string spirit_bless = "spirit_bless";
        public static string cur_bag_count = "cur_bag_count";
        public static string state_cell = "state_cell";
        public static string max_hp = "max_hp";
        public static string cur_hp = "cur_hp";
        public static string max_mp = "max_mp";
        public static string cur_mp = "cur_mp";
        public static string max_ep = "max_ep";
        public static string cur_ep = "cur_ep";
        public static string speed = "speed";
        public static string base_speed = "base_speed";
        public static string star_spirit_num = "star_spirit_num";
        public static string power = "power";
        public static string mfg_skill_level = "mfg_skill_level";
        public static string mfg_skill_exp = "mfg_skill_exp";
        public static string mfg_talent = "mfg_talent";
        public static string auction_ex_grid_cnt = "auction_ex_grid_cnt";
        public static string accumulated_charge = "accumulated_charge";
        public static string vip_level = "vip_level";
        public static string facade = "facade";
        public static string online_time_day = "online_time_day";
        public static string guild_id = "guild_id";
        public static string friend_give_count = "friend_give_count";
        public static string spell_proficient = "spell_proficient";
		public static string team_follow_num = "team_follow_num";
        public static string spell_proficient_factor1 = "spell_proficient_factor1";
        public static string spell_proficient_factor2 = "spell_proficient_factor2";
        public static string spell_proficient_factor3 = "spell_proficient_factor3";
        public static string active_point = "active_point";
        public static string daily_active_point = "daily_active_point";
        public static string title = "title";
        public static string achievement_point = "achievement_point";
        public static string enter_count = "enter_count";
        public static string lucky_coin = "lucky_coin";
        public static string ride_id = "ride_id";
        public static string active_ride_id = "active_ride_id";
        public static string owner_id = "owner_id";
        public static string tutor_value = "tutor_value";
        public static string tutor_reward = "tutor_reward";
        public static string tutor_level = "tutor_level";
        public static string duel_rank_show = "duel_rank_show";
        public static string duel_rank_true = "duel_rank_true";
        public static string duel_rank_level = "duel_rank_level";
        public static string duel_match_count = "duel_match_count";
        public static string auction_want_buy_times = "auction_want_buy_times";
        public static string anti_addiction = "anti_addiction";
        public static string addiction_time = "addiction_time";
        public static string old_role_status = "old_role_status";
        public static string mfg_talent_reset_cnt = "mfg_talent_reset_cnt";
        public static string mfg_talent_had_learn_cnt = "mfg_talent_had_learn_cnt";
        public static string control_setting_lol_move = "control_setting_lol_move";
        public static string control_setting_web_player = "control_setting_web_player";
    }
}
