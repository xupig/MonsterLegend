﻿#region 模块信息
/*==========================================
// 文件名：EntityPropertyDefine
// 命名空间: Common.ClientConfig
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/14 23:36:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ClientConfig
{
    public class PhysicsLayerDefine
    {
        public static string Actor = "Actor";
        public static string Avatar = "Avatar";
        public static string Monster = "Monster";
        public static string Dummy = "Dummy";
        public static string Ghost = "Ghost";
        public static string Pet = "Pet";
        public static string Air = "Air";
        public static string DynamicWall = "DynamicWall";
        public static string Wall = "Wall";
        public const string CameraWall = "CameraWall";
        public const string UI = "UI";
        public const string Terrain = "Terrain";
        public const string Bloom = "Bloom";
        public const string TransparentFX = "TransparentFX";
        public const string Trigger = "Trigger";

        public static int LAYER_ACTOR = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Actor);
        public static int LAYER_UI = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.UI);
        public static int LAYER_CAMERA_WALL = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.CameraWall);
        public static int LAYER_PET = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Pet);
        public static int LAYER_TERRAIN = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Terrain);
        public static int LAYER_BLOOM = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Bloom);
        public static int LAYER_TRANSPARENTFX = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.TransparentFX);
        public static int LAYER_TRIGGER = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Trigger);
        public static int LAYER_GHOST = UnityEngine.LayerMask.NameToLayer(PhysicsLayerDefine.Ghost);
    }
}
