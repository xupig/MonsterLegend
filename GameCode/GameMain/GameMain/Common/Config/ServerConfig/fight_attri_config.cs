using System;
using System.Collections.Generic;

namespace Common.ServerConfig {
    public enum fight_attri_config {


    ///<summary>
    ///---------------------属性id范围-------------------------
    ///</summary>
    
    CLASS_1_MIN = 1,
    CLASS_1_MAX = 9,

    CLASS_2_MIN = 10,
    CLASS_2_MAX = 99,

    CLASS_3_MIN = 110,
    CLASS_3_MAX = 199,

    CLASS_4_MIN = 200,
    CLASS_4_MAX = 209,

    CLASS_5_MIN = 210,
    CLASS_5_MAX = 219,
    
    CLASS_6_MIN = 240,
    CLASS_6_MAX = 249,


    ///<summary>
    ///---------------------一级属性-------------------------
    ///</summary>
    

    ///<summary>
    ///一级属性 1-9 --改变，转换成二级属性
    ///</summary>
    


    ///<summary>
    ///耐力
    ///</summary>
      STAMINA                 = 1, 

    ///<summary>
    ///力量
    ///</summary>
      STRENGTH                = 2, 

    ///<summary>
    ///敏捷
    ///</summary>
      DEXTERITY               = 3, 

    ///<summary>
    ///智力
    ///</summary>
      INTELLIGENC             = 4, 

    ///<summary>
    ///穿透
    ///</summary>
      PENETRATION             = 5, 

    ///<summary>
    ///专注 
    ///</summary>
      CONCENTRATE             = 6, 
      


    ///<summary>
    ///---------------------二级属性-------------------------
    ///</summary>
    

    ///<summary>
    ///二级属性10-99 --改变，转换成三级属性
    ///</summary>
    


    ///<summary>
    ///分类1 概率  10-19
    ///</summary>
      
      HIT                     = 10,
      MISS                    = 11,
      CRIT                    = 12,
      ANTI_CRIT               = 13,

    ///<summary>
    ///破击
    ///</summary>
      TRUE_STRIKE             = 14,

    ///<summary>
    ///抗破击
    ///</summary>
      ANTI_TRUE_STRIKE        = 15,


    ///<summary>
    ///分类2 伤害DmgPower 20-29
    ///</summary>
      
      PHY_DMG_POWER           = 20,
      MAG_DMG_POWER           = 21,
      FIRE_DMG_POWER          = 22,
      ICE_DMG_POWER           = 23,
      LIGHNING_DMG_POWER      = 24,
      POISON_DMG_POWER        = 25,


    ///<summary>
    ///分类3 防御Defense 30-39
    ///</summary>
      
      PHY_DEFENSE             = 30,
      MAG_DEFENSE             = 31,
      FIRE_DEFENSE            = 32,
      ICE_DEFENSE             = 33,
      LIGHNING_DEFENSE        = 34,
      POISON_DEFENSE          = 35,


    ///<summary>
    ///分类4 穿透Penetration 40-49
    ///</summary>
      
      PHY_PENETRATION         = 40,
      MAG_PENETRATION         = 41,
      FIRE_PENETRATION        = 42,
      ICE_PENETRATION         = 43,
      LIGHNING_PENETRATION    = 44,
      POISON_PENETRATION      = 45,


    ///<summary>
    ///分类5 50-59
    ///</summary>
      
      ATTACK_POWER            = 50,
      BLOCK                   = 51,
      PVP_POWER               = 52,
      PVP_DEFENSE             = 53,
      CRIT_POWER              = 54,
      TRUE_STRIKE_POWER       = 55,



    ///<summary>
    ///---------------------三级属性-------------------------
    ///</summary>
    

    ///<summary>
    ///三级属性100-199  --改变，没转换
    ///</summary>
    


    ///<summary>
    ///分类1 概率  110-119
    ///</summary>
      

    ///<summary>
    ///对应HIT
    ///</summary>
      HIT_RATE                = 110, 
      MISS_RATE               = 111,
      CRIT_RATE               = 112,
      ANTI_CRIT_RATE          = 113,
      TRUE_STRIKE_RATE        = 114,
      ANTI_TRUE_STRIKE_RATE   = 115,


    ///<summary>
    ///分类2 伤害DmgPower 120-129
    ///</summary>
      

    ///<summary>
    ///对应PHY_DMG_POWER
    ///</summary>
      PHY_DMG_ADD_RATE        = 120, 
      MAG_DMG_ADD_RATE        = 121,
      FIRE_DMG_ADD_RATE       = 122,
      ICE_DMG_ADD_RATE        = 123,
      LIGHNING_DMG_ADD_RATE   = 124,
      POISON_DMG_ADD_RATE     = 125,


    ///<summary>
    ///分类3 防御Defense 130-139
    ///</summary>
      

    ///<summary>
    ///对应PHY_DEFENSE
    ///</summary>
      PHY_DMG_REDUCE_RATE      = 130, 
      MAG_DMG_REDUCE_RATE      = 131,
      FIRE_DMG_REDUCE_RATE     = 132,
      ICE_DMG_REDUCE_RATE      = 133,
      LIGHNING_DMG_REDUCE_RATE = 134,
      POISON_DMG_REDUCE_RATE   = 135,


    ///<summary>
    ///分类4 穿透Penetration 140-149
    ///</summary>
      

    ///<summary>
    ///对应PHY_PENETRATION
    ///</summary>
      PHY_PENETRATION_RATE      = 140, 
      MAG_PENETRATION_RATE      = 141,
      FIRE_PENETRATION_RATE     = 142,
      ICE_PENETRATION_RATE      = 143,
      LIGHNING_PENETRATION_RATE = 144,
      POISON_PENETRATION_RATE   = 145,


    ///<summary>
    ///分类5  150-159
    ///</summary>
      

    ///<summary>
    ///对应ATTACK_POWER
    ///</summary>
      ALL_DMG_ADD_RATE           = 150,

    ///<summary>
    ///对应BLOCK
    ///</summary>
      ALL_DMG_REDUCE_RATE        = 151,

    ///<summary>
    ///对应PVP_POWER
    ///</summary>
      PVP_DMG_ADD_RATE           = 152,

    ///<summary>
    ///对应PVP_DEFENSE
    ///</summary>
      PVP_DMG_REDUCE_RATE        = 153,

    ///<summary>
    ///对应CRIT_POWER
    ///</summary>
      CRIT_DMG_ADD_RATE          = 154,

    ///<summary>
    ///对应RUESTRIKEPOWER
    ///</summary>
      TURE_STRIKE_DMG_ADD_RATE   = 155,

    ///<summary>
    ///天赋伤害加成比
    ///</summary>
      TALENT_DMG_ADD_RATE        = 156,

    ///<summary>
    ///天赋伤害加成比
    ///</summary>
      TALENT_DMG_REDUCE_RATE     = 157,


    ///<summary>
    ///分类6 160-162
    ///</summary>
      

    ///<summary>
    ///伤害上限
    ///</summary>
      DMG_MAX                     = 160, 

    ///<summary>
    ///伤害下限
    ///</summary>
      DMG_MIN                     = 161, 

    ///<summary>
    ///幸运值
    ///</summary>
      LUCK_RATE                   = 162, 


    ///<summary>
    ///分类7 连着分类6 163-
    ///</summary>
      

    ///<summary>
    ///对战士伤害加成比
    ///</summary>
      TO_WARRIOR_DMG_ADD_RATE    = 163, 

    ///<summary>
    ///对刺客伤害加成比
    ///</summary>
      TO_ASSASSIN_DMG_ADD_RATE   = 164, 

    ///<summary>
    ///对法师伤害加成比
    ///</summary>
      TO_WIZARD_DMG_ADD_RATE     = 165, 

    ///<summary>
    ///对弓手伤害加成比
    ///</summary>
      TO_ARCHER_DMG_ADD_RATE     = 166, 


    ///<summary>
    ///对小怪伤害加成比
    ///</summary>
      TO_VEHICON_DMG_ADD_RATE    = 167, 

    ///<summary>
    ///对精英怪伤害加成比
    ///</summary>
      TO_ELITE_DMG_ADD_RATE      = 168, 

    ///<summary>
    ///对首领boss怪伤害加成比
    ///</summary>
      TO_BOSS_DMG_ADD_RATE       = 169, 

    ///<summary>
    ///对人形怪伤害加成比
    ///</summary>
      TO_HUMAN_DMG_ADD_RATE      = 170, 

    ///<summary>
    ///对亡灵怪伤害加成比
    ///</summary>
      TO_DEATH_DMG_ADD_RATE      = 171, 

    ///<summary>
    ///对野兽怪伤害加成比
    ///</summary>
      TO_BEAST_DMG_ADD_RATE      = 172, 

    ///<summary>
    ///对魔鬼怪伤害加成比
    ///</summary>
      TO_DEAMON_DMG_ADD_RATE     = 173, 

    ///<summary>
    ///对天使怪伤害加成比
    ///</summary>
      TO_ANGEL_DMG_ADD_RATE      = 174, 


    ///<summary>
    ///受战士伤害减免比
    ///</summary>
      FROM_WARRIOR_DMG_REDUCE_RATE = 175, 

    ///<summary>
    ///受刺客伤害减免比
    ///</summary>
      FROM_ASSASSIN_DMG_REDUCE_RATE= 176, 

    ///<summary>
    ///受法师伤害减免比
    ///</summary>
      FROM_WIZARD_DMG_REDUCE_RATE  = 177, 

    ///<summary>
    ///受弓手伤害减免比
    ///</summary>
      FROM_ARCHER_DMG_REDUCE_RATE  = 178, 


    ///<summary>
    ///受小怪伤害减免比
    ///</summary>
      FROM_VEHICON_DMG_REDUCE_RATE = 179, 

    ///<summary>
    ///受精英伤害减免比
    ///</summary>
      FROM_ELITE_DMG_REDUCE_RATE   = 180, 

    ///<summary>
    ///受首领伤害减免比
    ///</summary>
      FROM_BOSS_DMG_REDUCE_RATE    = 181, 


    ///<summary>
    ///受人形怪伤害减免比
    ///</summary>
      FROM_HUMAN_DMG_REDUCE_RATE   = 182, 

    ///<summary>
    ///受亡灵怪伤害减免比
    ///</summary>
      FROM_DEATH_DMG_REDUCE_RATE   = 183, 

    ///<summary>
    ///受野兽怪伤害减免比
    ///</summary>
      FROM_BEAST_DMG_REDUCE_RATE   = 184, 

    ///<summary>
    ///受魔鬼怪伤害减免比
    ///</summary>
      FROM_DEAMON_DMG_REDUCE_RATE  = 185, 

    ///<summary>
    ///受天使怪伤害减免比
    ///</summary>
      FROM_ANGEL_DMG_REDUCE_RATE   = 186, 
      

    ///<summary>
    ///技能行为激活时间加速
    ///</summary>
      SPELL_ACTION_ATTACK_SPEED = 190,  

    ///<summary>
    ///技能专精1加成系数
    ///</summary>
      SPELL_PROFICIENT1_FACTOR  = 191, 

    ///<summary>
    ///技能专精2加成系数
    ///</summary>
      SPELL_PROFICIENT2_FACTOR  = 192, 

    ///<summary>
    ///技能专精3加成系数
    ///</summary>
      SPELL_PROFICIENT3_FACTOR  = 193, 



    ///<summary>
    ///---------------------生命属性-------------------------
    ///</summary>
      

    ///<summary>
    ///生命属性200-209
    ///</summary>
      

    ///<summary>
    ///注意,改变，转换成hpMax
    ///</summary>
      HP_LIMIT                  = 200, 

    ///<summary>
    ///注意,改变，转换成hpMax
    ///</summary>
      HP_LIMIT_ADD_RATE         = 201, 

    ///<summary>
    ///改变，没转换
    ///</summary>
      HP_RECOVER_SPEED          = 202, 

    ///<summary>
    ///生命回复比
    ///</summary>
      HP_RECOVER_RATE           = 203, 

    ///<summary>
    ///回血降低比
    ///</summary>
      HP_RECOVER_REDUCE_RATE    = 204, 

    ///<summary>
    ///最大生命
    ///</summary>
      HP_MAX                    = 205, 
    


    ///<summary>
    ///---------------------其他属性-------------------------
    ///</summary>
      

    ///<summary>
    ///其他属性210-
    ///</summary>
      

    ///<summary>
    ///表示buff
    ///</summary>
      BUFF       = 210,    
     

    ///<summary>
    ///---------------------技能能量属性-------------------------
    ///</summary>
     

    ///<summary>
    ///注意,改变，转换成EP_MAX
    ///</summary>
      EP_LIMIT                  = 240, 

    ///<summary>
    ///注意,改变，转换成EP_MAX
    ///</summary>
      EP_LIMIT_ADD_RATE         = 241, 

    ///<summary>
    ///改变，没转换
    ///</summary>
      EP_RECOVER_SPEED          = 242, 

    ///<summary>
    ///技能能量回复比
    ///</summary>
      EP_RECOVER_RATE           = 243, 

    ///<summary>
    ///技能能量降低比
    ///</summary>
      EP_RECOVER_REDUCE_RATE    = 244, 

    ///<summary>
    ///最大技能能量
    ///</summary>
      EP_MAX                    = 245, 

    }


    ///<summary>
    ///------------------------------------------------------------
    ///</summary>

}


