using System;
using System.Collections.Generic;

namespace Common.ServerConfig {
    public static class public_config {

        ///<summary>
        ///玩家
        ///</summary>
        public const int    ENTITY_TYPE_AVATAR          = 1;  
        public const int    ENTITY_TYPE_SPAWNPOINT      = 2;
        public const int    ENTITY_TYPE_MONSTER         = 3;
        public const int    ENTITY_TYPE_NPC             = 4;
        public const int    ENTITY_TYPE_CLICKITEM       = 5;
        public const int    ENTITY_TYPE_TELEPORTSRC     = 6;
        public const int    ENTITY_TYPE_TELEPORTDES     = 7;

        ///<summary>
        ///假人
        ///</summary>
        public const int    ENTITY_TYPE_PUPPET          = 8;  
        public const int    ENTITY_TYPE_SPACELOADER     = 9;

        ///<summary>
        ///区域触发器
        ///</summary>
        public const int    ENTITY_TYPE_REGIONTRIGGER   = 10;  

        ///<summary>
        ///宠物
        ///</summary>
        public const int    ENTITY_TYPE_PET             = 11;  

        ///<summary>
        ///掉落物品
        ///</summary>
        public const int    ENTITY_TYPE_DROPITEM        = 12;  

        ///<summary>
        ///异界宝箱
        ///</summary>
        public const int    ENTITY_TYPE_CHEST           = 13;  

        ///<summary>
        ///战斗型传送门
        ///</summary>
        public const int    ENTITY_TYPE_COMBAT_PORTAL   = 14;  

        ///<summary>
        ///传送门
        ///</summary>
        public const int    ENTITY_TYPE_PORTAL          = 15;  

        ///<summary>
        ///代理
        ///</summary>
        public const int    ENTITY_TYPE_AGENT           = 16;  

        ///<summary>
        ///异界宝箱-决斗场宝箱
        ///</summary>
        public const int    ENTITY_TYPE_DUELCHEST       = 17;  


        ///<summary>
        ///客户端实体类型，服务器仅仅是一个数据
        ///</summary>
    
        public const int    CLIENT_ENTITY_TYPE_DUMMY      = 1;
        public const int    CLIENT_ENTITY_TYPE_DROP       = 2;
        public const int    CLIENT_ENTITY_TYPE_SPAWNPOINT = 3;


        ///<summary>
        ///spawnPoint分类类型
        ///</summary>
    

        ///<summary>
        ///相对副本开始时间
        ///</summary>
        public const int    SPAWN_POINT_TYPE_BEGIN            = 1; 

        ///<summary>
        ///客户端触发
        ///</summary>
        public const int    SPAWN_POINT_TYPE_CLIENT_TRIGGER   = 2; 

        ///<summary>
        ///循环刷怪，spawn point 实体，需要aoi
        ///</summary>
        public const int    SPAWN_POINT_TYPE_CYCLE            = 3; 

        ///<summary>
        ///0或者没填写，默认成为1和2类型的next_spawn_points
        ///</summary>
        public const int    SPAWN_POINT_TYPE_OTHER            = 9; 

        ///<summary>
        ///服务器内部，延迟的next_spawn_points,{spawn_point:time},time>0的，spawn后remove
        ///</summary>
        public const int    SPAWN_POINT_TYPE_NEXT             = 10; 


        ///<summary>
        ///monster中is_client的含义:制作类型make_type
        ///</summary>
    

        ///<summary>
        ///服务器端怪物且ai服务器
        ///</summary>
        public const int    MONSTER_IS_CLIENT_MONSTER = 0;  

        ///<summary>
        ///客户端怪物
        ///</summary>
        public const int    MONSTER_IS_CLIENT_DUMMY   = 1;  

        ///<summary>
        ///服务器端且ai客户端  混合模式-- MONSTER_IS_CLIENT_HYBRID
        ///</summary>
        public const int    MONSTER_IS_CLIENT_MERCENARY  = 2;  


        ///<summary>
        ///小怪
        ///</summary>
        public const int    MONSTER_TYPE_VEHICON      = 1;  

        ///<summary>
        ///精英怪
        ///</summary>
        public const int    MONSTER_TYPE_ELITE        = 2;  

        ///<summary>
        ///boss怪
        ///</summary>
        public const int    MONSTER_TYPE_BOSS         = 3;  

        ///<summary>
        ///野外世界boss
        ///</summary>
        public const int    MONSTER_TYPE_WORLD_BOSS   = 4;  

        ///<summary>
        ///载具 客户端用
        ///</summary>
        public const int    MONSTER_TYPE_ZAIJU        = 5;  

        ///<summary>
        ///特殊掉落怪
        ///</summary>
        public const int    MONSTER_TYPE_SP_DROP      = 9;  


        ///<summary>
        ///人形怪
        ///</summary>
        public const int    MONSTER_MODEL_TYPE_HUMAN  = 0; 

        ///<summary>
        ///亡灵怪
        ///</summary>
        public const int    MONSTER_MODEL_TYPE_DEATH  = 1; 

        ///<summary>
        ///野兽怪
        ///</summary>
        public const int    MONSTER_MODEL_TYPE_BEAST  = 2; 

        ///<summary>
        ///魔鬼怪
        ///</summary>
        public const int    MONSTER_MODEL_TYPE_DEAMON = 3; 

        ///<summary>
        ///天使怪
        ///</summary>
        public const int    MONSTER_MODEL_TYPE_ANGEL  = 4; 


        ///<summary>
        ///默认速度
        ///</summary>
    

        ///<summary>
        ///每秒多少厘米
        ///</summary>
        public const int    BASE_SPEED_PER_SECOND = 400;  


        ///<summary>
        ///关卡类型定义
        ///</summary>
    

        ///<summary>
        ///主城
        ///</summary>
        public const int    INSTANCE_TYPE_CITY   = 0;        

        ///<summary>
        ///野外
        ///</summary>
        public const int    INSTANCE_TYPE_WILD   = 1;        

        ///<summary>
        ///副本
        ///</summary>
        public const int    INSTANCE_TYPE_COPY   = 2;        


        ///<summary>
        ///关卡玩法分类
        ///</summary>
    

        ///<summary>
        ///主城
        ///</summary>
        public const int    MAP_TYPE_CITY                   = 0;    

        ///<summary>
        ///野外
        ///</summary>
        public const int    MAP_TYPE_WILD                   = 1;    

        ///<summary>
        ///剧情副本
        ///</summary>
        public const int    MAP_TYPE_STORY_COPY             = 2;    

        ///<summary>
        ///试炼秘境
        ///</summary>
        public const int    MAP_TYPE_TRY_COPY               = 3;    

        ///<summary>
        ///单人副本
        ///</summary>
        public const int    MAP_TYPE_SINGLE_COPY            = 4;    

        ///<summary>
        ///恶魔之门
        ///</summary>
        public const int    MAP_TYPE_EVIL_COPY              = 5;    

        ///<summary>
        ///组队副本
        ///</summary>
        public const int    MAP_TYPE_TEAM_COPY              = 7;    

        ///<summary>
        ///晚上活动
        ///</summary>
        public const int    MAP_TYPE_NIGHT_ACTIVITY         = 8;    

        ///<summary>
        ///公会副本
        ///</summary>
        public const int    MAP_TYPE_GUILD_COPY             = 9;    

        ///<summary>
        ///幻境探索
        ///</summary>
        public const int    MAP_TYPE_DREAMLAND_COPY         = 10;   

        ///<summary>
        ///体验关卡
        ///</summary>
        public const int    MAP_TYPE_EXPERIENCE_COPY        = 11;  

        ///<summary>
        ///通过使用宝图刷出的传送门进入的地图
        ///</summary>
        public const int    MAP_TYPE_TREASUREMAP_TP         = 12;  

        ///<summary>
        ///两人PK切磋
        ///</summary>
        public const int    MAP_TYPE_PK_COPY                = 13;  

        ///<summary>
        ///争夺型传送门
        ///</summary>
        public const int    MAP_TYPE_COMBAT_PORTAL          = 14;  

        ///<summary>
        ///公会副本试炼
        ///</summary>
        public const int    MAP_TYPE_GUILD_TRY_COPY         = 15;  

        ///<summary>
        ///公会竞技场单独候场
        ///</summary>
        public const int    MAP_TYPE_GUILD_WAIT_SINGLE_COPY = 16;  

        ///<summary>
        ///公会竞技场双反候场
        ///</summary>
        public const int    MAP_TYPE_GUILD_WAIT_DOUBLE_COPY = 17;  

        ///<summary>
        ///公会小组赛pk
        ///</summary>
        public const int    MAP_TYPE_GUILD_PK_COPY          = 18;  

        ///<summary>
        ///时空裂隙
        ///</summary>
        public const int    MAP_TYPE_SPACETIME_RIFT         = 19;  

        ///<summary>
        ///组队支线剧情本
        ///</summary>
        public const int    MAP_TYPE_TEAM_BRANCH_STORY      = 20;  

        ///<summary>
        ///无奖励客户端副本
        ///</summary>
        public const int    MAP_TYPE_NO_REWARD_CLIENT_COPY  = 21;  

        ///<summary>
        ///决斗候备场
        ///</summary>
        public const int    MAP_TYPE_DUEL                   = 22;  

        ///<summary>
        ///决斗比赛场
        ///</summary>
        public const int    MAP_TYPE_DUEL_MATCH             = 23;  

        ///<summary>
        ///公会boss
        ///</summary>
        public const int    MAP_GUILD_BOSS                  = 24;  


        ///<summary>
        ///关卡组队分类
        ///</summary>
    

        ///<summary>
        ///单人副本
        ///</summary>
        public const int    MAP_TEAM_TYPE_SINGLE  = 1;      

        ///<summary>
        ///组队副本
        ///</summary>
        public const int    MAP_TEAM_TYPE_TEAM    = 2;      

        ///<summary>
        ///有人数上限的随机进入副本
        ///</summary>
        public const int    MAP_TEAM_TYPE_RANDOM  = 3;      


        ///<summary>
        ///性别
        ///</summary>
    

        ///<summary>
        ///性别:女
        ///</summary>
        public const int    GENDER_FEMALE = 0;                 

        ///<summary>
        ///性别:男
        ///</summary>
        public const int    GENDER_MALE   = 1;                 


        ///<summary>
        /// 职业
        ///</summary>
    

        ///<summary>
        /// 战士
        ///</summary>
        public const int    VOC_WARRIOR   = 1;         

        ///<summary>
        /// 刺客
        ///</summary>
        public const int    VOC_ASSASSIN  = 2;         

        ///<summary>
        /// 法师
        ///</summary>
        public const int    VOC_WIZARD    = 3;         

        ///<summary>
        /// 枪手
        ///</summary>
        public const int    VOC_ARCHER    = 4;         

        ///<summary>
        /// 最小职业编号
        ///</summary>
        public const int    VOC_MIN       = 1;         

        ///<summary>
        /// 最大职业编号
        ///</summary>
        public const int    VOC_MAX       = 4;         


        ///<summary>
        ///玩家退出游戏的各种方式
        ///</summary>
    

        ///<summary>
        ///还在游戏中
        ///</summary>
        public const int    NOT_QUIT_GAME  = 0;  

        ///<summary>
        ///普通退出（走完整的退出流程）
        ///</summary>
        public const int    QUIT_GAME_BY_NORMAL = 1;    

        ///<summary>
        ///只销毁base和cell上的角色相关数据
        ///</summary>
        public const int    QUIT_GAME_BY_DESTROY = 2;   

        ///<summary>
        ///顶号
        ///</summary>
        public const int    QUIT_GAME_BY_MULTI_LOGIN = 3;   

        ///<summary>
        ///玩家下线
        ///</summary>
        public const int    QUIT_GAME_BY_OFFLINE = 4;       

        ///<summary>
        ///玩家在原服服务器上登录
        ///</summary>
        public const int    QUIT_GAME_BY_ORIGIN_LOGIN = 5;  

        ///<summary>
        ///gm将玩家提下线
        ///</summary>
        public const int    QUIT_GAME_BY_GM_KICK = 6;       


        ///<summary>
        ///>角色属性编号
        ///</summary>
    

        ///<summary>
        ///int
        ///</summary>
        public const int    CHARACTER_KEY_DBID       = 1;       

        ///<summary>
        ///string
        ///</summary>
        public const int    CHARACTER_KEY_NAME       = 2;       

        ///<summary>
        ///int
        ///</summary>
        public const int    CHARACTER_KEY_VOCATION   = 3;       

        ///<summary>
        ///int
        ///</summary>
        public const int    CHARACTER_KEY_LEVEL      = 4;       

        ///<summary>
        ///string
        ///</summary>
        public const int    CHARACTER_KEY_MODEL      = 5;       


        ///<summary>
        ///avatar base上的标记位
        ///</summary>
    

        ///<summary>
        ///新建角色
        ///</summary>
        public const int    AVATAR_BASE_STATE_NEWBIE   = 0;    


        ///<summary>
        ///avatar 最大vip等级
        ///</summary>
    

        ///<summary>
        ///Avatar最大vip等级
        ///</summary>
        public const int    AVATAR_MAX_VIP_LEVEL       = 15;   


        ///<summary>
        ///技能目标类型的值，与TARGET_TYPE相匹配
        ///</summary>
    

        ///<summary>
        ///敌方
        ///</summary>
        public const int    SPELL_TARGET_ENEMY   = 1;    

        ///<summary>
        ///友方
        ///</summary>
        public const int    SPELL_TARGET_TEAMER  = 2;    

        ///<summary>
        ///自己
        ///</summary>
        public const int    SPELL_TARGET_SELF    = 4;    

        ///<summary>
        ///中立
        ///</summary>
        public const int    SPELL_TARGET_NEUTRAL = 8;    


        ///<summary>
        ///定义技能攻击伤害类型
        ///</summary>
    

        ///<summary>
        ///物理攻击
        ///</summary>
        public const int    SPELL_DAMAGE_PHY      = 1;          

        ///<summary>
        ///魔法攻击
        ///</summary>
        public const int    SPELL_DAMAGE_MAG      = 2;          

        ///<summary>
        ///火元素攻击
        ///</summary>
        public const int    SPELL_DAMAGE_FIRE     = 3;          

        ///<summary>
        ///冰元素攻击
        ///</summary>
        public const int    SPELL_DAMAGE_ICE      = 4;          

        ///<summary>
        ///电元素攻击
        ///</summary>
        public const int    SPELL_DAMAGE_LIGHNING = 5;          

        ///<summary>
        ///毒元素攻击
        ///</summary>
        public const int    SPELL_DAMAGE_POISON   = 6;          


        ///<summary>
        ///技能攻击中所发生的事件定义,按位定义,可以多个位进行组合
        ///</summary>


        ///<summary>
        ///命中
        ///</summary>
        public const int HIT_RET_HIT = 0;

        ///<summary>
        ///暴击
        ///</summary>
        public const int HIT_RET_CRI = 1;

        ///<summary>
        ///破击
        ///</summary>
        public const int HIT_RET_BREAK = 2;

        ///<summary>
        ///死亡
        ///</summary>
        public const int HIT_RET_DEATH = 3;

        ///<summary>
        ///治疗
        ///</summary>
        public const int HIT_RET_TREAT = 4;

        ///<summary>
        ///格挡
        ///</summary>
        public const int HIT_RET_PARRY = 5;

        ///<summary>
        ///无敌
        ///</summary>
        public const int HIT_RET_WUDI = 6;

        ///<summary>
        ///不屈
        ///</summary>
        public const int HIT_RET_BUQU = 7;

        ///<summary>
        ///技能槽的位置定义
        ///</summary>
    

        ///<summary>
        ///普通攻击
        ///</summary>
        public const int    SPELL_SLOT_COMMON_ATTACK     = 1;     

        ///<summary>
        ///位移攻击
        ///</summary>
        public const int    SPELL_SLOT_MOVE_ATTACK       = 2;     

        ///<summary>
        ///输出攻击
        ///</summary>
        public const int    SPELL_SLOT_OUTPUT_ATTACK     = 3;     

        ///<summary>
        ///控制攻击
        ///</summary>
        public const int    SPELL_SLOT_CONTROL_ATTACK    = 4;     

        ///<summary>
        ///技能的最大装备位置编号
        ///</summary>
        public const int    SPELL_SLOT_MAX_POS           = 4;     
    

        ///<summary>
        ///定义技能的特殊效果
        ///</summary>
    

        ///<summary>
        ///技能治疗效果编号
        ///</summary>
        public const int    SPELL_SPECIAL_ID_CURE        = 1;     


        ///<summary>
        ///玩家各子系统管理器的编号定义
        ///</summary>
    

        ///<summary>
        ///玩家的场景管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_SPACE             = 1;  

        ///<summary>
        ///玩家物品管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_ITEM              = 2;  

        ///<summary>
        ///玩家GM管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_GM                = 3;  

        ///<summary>
        ///玩家聊天管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_CHAT              = 4;  

        ///<summary>
        ///玩家关卡管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_MISSION           = 5;  

        ///<summary>
        ///玩家操作与状态匹配管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_ACTION            = 6;  

        ///<summary>
        ///玩家任务管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_TASK              = 7;  

        ///<summary>
        ///玩家技能管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_SPELL             = 8;  

        ///<summary>
        ///玩家装备管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_EQUIP             = 9;  

        ///<summary>
        ///玩家拍卖管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_AUCTION           = 10; 

        ///<summary>
        ///玩家组队管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM              = 11; 

        ///<summary>
        ///宝石管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_GEM               = 12; 

        ///<summary>
        ///符文系统
        ///</summary>
        public const int    MGR_ID_AVATAR_RUNE              = 13; 

        ///<summary>
        ///附魔管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_ENCHANT           = 14; 

        ///<summary>
        ///体力管理器编号
        ///</summary>
        public const int    MGR_ID_AVATAR_ENERGY            = 15; 

        ///<summary>
        ///uuid
        ///</summary>
        public const int    MGR_ID_AVATAR_UUID              = 16; 

        ///<summary>
        ///玩家buff的子系统
        ///</summary>
        public const int    MGR_ID_AVATAR_BUFF              = 17; 

        ///<summary>
        ///玩家邮件系统
        ///</summary>
        public const int    MGR_ID_AVATAR_MAIL              = 18; 

        ///<summary>
        ///强化系统
        ///</summary>
        public const int    MGR_ID_AVATAR_STRENGTHEN        = 19; 

        ///<summary>
        ///翅膀系统
        ///</summary>
        public const int    MGR_ID_AVATAR_WING              = 20; 

        ///<summary>
        ///制造系统
        ///</summary>
        public const int    MGR_ID_AVATAR_MFG               = 21; 

        ///<summary>
        ///好友管理器
        ///</summary>
        public const int    MGR_ID_AVATAR_FRIEND            = 22; 

        ///<summary>
        ///套装系统
        ///</summary>
        public const int    MGR_ID_AVATAR_SUIT              = 23; 

        ///<summary>
        ///时装系统
        ///</summary>
        public const int    MGR_ID_AVATAR_FASHION           = 24; 

        ///<summary>
        ///要塞系统
        ///</summary>
        public const int    MGR_ID_AVATAR_FORTRESS          = 25; 

        ///<summary>
        ///星魂系统
        ///</summary>
        public const int    MGR_ID_AVATAR_STAR_SPIRIT       = 26; 

        ///<summary>
        ///代币商店
        ///</summary>
        public const int    MGR_ID_AVATAR_TOKEN_SHOP        = 27; 

        ///<summary>
        ///幻境探索系统
        ///</summary>
        public const int    MGR_ID_AVATAR_DREAMLAND         = 28; 

        ///<summary>
        ///商城
        ///</summary>
        public const int    MGR_ID_AVATAR_MARKET            = 29; 

        ///<summary>
        ///试炼秘境
        ///</summary>
        public const int    MGR_ID_AVATAR_TRY_MISSION       = 30; 

        ///<summary>
        ///精力管理器,类似体力
        ///</summary>
        public const int    MGR_ID_AVATAR_POWER             = 31; 

        ///<summary>
        ///vip系统
        ///</summary>
        public const int    MGR_ID_AVATAR_VIP               = 32; 

        ///<summary>
        ///NPC好感度
        ///</summary>
        public const int    MGR_ID_AVATAR_NPC_REPUTATION    = 33; 

        ///<summary>
        ///宠物系统
        ///</summary>
        public const int    MGR_ID_AVATAR_PET               = 34; 

        ///<summary>
        ///排行榜系统
        ///</summary>
        public const int    MGR_ID_AVATAR_RANK_LIST         = 35; 

        ///<summary>
        ///发放奖励的系统
        ///</summary>
        public const int    MGR_ID_AVATAR_REWARD            = 36; 

        ///<summary>
        ///奖励系统
        ///</summary>
        public const int    MGR_ID_AVATAR_AWARD             = 37; 

        ///<summary>
        ///活动系统
        ///</summary>
        public const int    MGR_ID_AVATAR_ACTIVITY          = 38; 

        ///<summary>
        ///副本活动:恶魔之门
        ///</summary>
        public const int    MGR_ID_AVATAR_EVIL_MISSION      = 39; 

        ///<summary>
        ///公会系统
        ///</summary>
        public const int    MGR_ID_AVATAR_GUILD             = 40; 

        ///<summary>
        ///功能开启系统
        ///</summary>
        public const int    MGR_ID_AVATAR_FUNCTION          = 41; 

        ///<summary>
        ///设置系统
        ///</summary>
        public const int    MGR_ID_AVATAR_SETTING           = 42; 

        ///<summary>
        ///怪物boss部位
        ///</summary>
        public const int    MGR_ID_MONSTER_BODY_PART        = 43; 

        ///<summary>
        ///好友邀请
        ///</summary>
        public const int    MGR_ID_AVATAR_FRIEND_INVITE     = 44; 

        ///<summary>
        ///副本活动:组队副本
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_MISSION      = 45; 

        ///<summary>
        ///副本活动:喊人组队
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_CALL         = 46; 

        ///<summary>
        ///次数价格表
        ///</summary>
        public const int    MGR_ID_AVATAR_PRICE             = 47; 

        ///<summary>
        ///组队好友加好友度
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_FSHIP        = 48; 

        ///<summary>
        ///单人副本
        ///</summary>
        public const int    MGR_ID_AVATAR_SINGLE_MISSIONG   = 49; 

        ///<summary>
        ///卡片管理月卡，季卡，年卡
        ///</summary>
        public const int    MGR_ID_AVATAR_CARD              = 50; 

        ///<summary>
        ///ai托管
        ///</summary>
        public const int    MGR_ID_AVATAR_AI                = 51; 

        ///<summary>
        ///组队或者分组的血量同步
        ///</summary>
        public const int    MGR_ID_AVATAR_HP_SYNC           = 52; 

        ///<summary>
        ///日志收集
        ///</summary>
        public const int    MGR_ID_AVATAR_COLLECTOR_LOG     = 53; 

        ///<summary>
        ///组队，队长pos同步
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_POS_SYNC     = 54; 

        ///<summary>
        ///藏宝图
        ///</summary>
        public const int    MGR_ID_AVATAR_TREASURE_MAP      = 55; 

        ///<summary>
        ///宝箱
        ///</summary>
        public const int    MGR_ID_AVATAR_CHEST             = 56; 

        ///<summary>
        ///时光祭坛
        ///</summary>
        public const int    MGR_ID_AVATAR_TIME_ALTAR        = 57; 

        ///<summary>
        ///晚上活动
        ///</summary>
        public const int    MGR_ID_AVATAR_NIGHT_ACTIVITY    = 58; 

        ///<summary>
        ///手机绑定
        ///</summary>
        public const int    MGR_ID_AVATAR_BIND_MOBILE       = 59; 

        ///<summary>
        ///副本活动:pk副本
        ///</summary>
        public const int    MGR_ID_AVATAR_PK_MISSION        = 60; 

        ///<summary>
        ///称号
        ///</summary>
        public const int    MGR_ID_AVATAR_TITLE             = 61; 

        ///<summary>
        ///成就
        ///</summary>
        public const int    MGR_ID_AVATAR_ACHIEVEMENT       = 62; 

        ///<summary>
        ///自动组队
        ///</summary>
        public const int    MGR_ID_AVATAR_AUTO_TEAM         = 63; 

        ///<summary>
        ///组队buff
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_BUFF         = 64; 

        ///<summary>
        ///炼金系统
        ///</summary>
        public const int    MGR_ID_AVATAR_ALCHEMY           = 65; 

        ///<summary>
        ///副本活动:公会试炼
        ///</summary>
        public const int    MGR_ID_AVATAR_GUILD_TRY_MISSION = 66; 

        ///<summary>
        ///外观系统
        ///</summary>
        public const int    MGR_ID_AVATAR_FACADE            = 67; 

        ///<summary>
        ///充值系统
        ///</summary>
        public const int    MGR_ID_AVATAR_CHARGE            = 68; 

        ///<summary>
        ///幸运转盘
        ///</summary>
        public const int    MGR_ID_AVATAR_LUCKY_TURNTABLE   = 69; 

        ///<summary>
        ///副本活动:pk副本
        ///</summary>
        public const int    MGR_ID_AVATAR_GUILD_PK_MISSION  = 70; 

        ///<summary>
        ///装备熔炼
        ///</summary>
        public const int    MGR_ID_AVATAR_EQUIP_SMELTING    = 71; 

        ///<summary>
        ///时空裂隙
        ///</summary>
        public const int    MGR_ID_AVATAR_SPACETIME_RIFT    = 72; 

        ///<summary>
        ///坐骑系统
        ///</summary>
        public const int    MGR_ID_AVATAR_RIDE              = 73; 

        ///<summary>
        ///公会商城系统
        ///</summary>
        public const int    MGR_ID_AVATAR_GUILD_SHOP        = 74; 

        ///<summary>
        ///浮盈奖励
        ///</summary>
        public const int    MGR_ID_AVATAR_ITEM_COMPENSATE   = 75; 

        ///<summary>
        ///创角活动(七天) 开服活动中的一个活动     MGR_ID_AVATAR_CREATE_SEVEN_DAY
        ///</summary>
        public const int    MGR_ID_AVATAR_CREATE_ACTIVITY   = 76; 

        ///<summary>
        ///充值活动
        ///</summary>
        public const int    MGR_ID_AVATAR_CHARGE_ACTIVITY   = 77; 

        ///<summary>
        ///红包活动
        ///</summary>
        public const int    MGR_ID_AVATAR_RED_ENVELOPE      = 78; 

        ///<summary>
        ///导师系统
        ///</summary>
        public const int    MGR_ID_AVATAR_TUTOR             = 79; 

        ///<summary>
        ///装备升级
        ///</summary>
        public const int    MGR_ID_AVATAR_EQUIP_UPGRADE     = 80; 

        ///<summary>
        ///导师系统 cell 野外
        ///</summary>
        public const int    MGR_ID_AVATAR_TEAM_TUTOR        = 81; 

        ///<summary>
        ///决斗系统
        ///</summary>
        public const int    MGR_ID_AVATAR_DUEL              = 82; 

        ///<summary>
        ///野外任务系统
        ///</summary>
        public const int    MGR_ID_AVATAR_WILD_TASK         = 83; 

        ///<summary>
        ///老玩家回归系统
        ///</summary>
        public const int    MGR_ID_AVATAR_OLD_RETURN        = 84; 

        ///<summary>
        ///宝箱 决斗场宝箱
        ///</summary>
        public const int    MGR_ID_AVATAR_DUELCHEST         = 85; 
    

        ///<summary>
        ///UserMgr返回的数据里面LUA_TABLE(DbidToPlayers)的index
        ///</summary>
    
        public const int    UM_PLAYER_BASE_MB_INDEX            = 1;
        public const int    UM_PLAYER_CELL_MB_INDEX            = 2;
        public const int    UM_PLAYER_DBID_INDEX               = 3;
        public const int    UM_PLAYER_NAME_INDEX               = 4;
        public const int    UM_PLAYER_LEVEL_INDEX              = 5;
        public const int    UM_PLAYER_VOCATION_INDEX           = 6;
        public const int    UM_PLAYER_GENDER_INDEX             = 7;
        public const int    UM_PLAYER_UNION_INDEX              = 8;

        ///<summary>
        ///战斗力
        ///</summary>
        public const int    UM_PLAYER_FIGHT_INDEX              = 9;  
        public const int    UM_PLAYER_ONLINE_INDEX             = 10;

        ///<summary>
        ///该值存储于offlinemgr
        ///</summary>
        public const int    UM_PLAYER_FRIEND_NUM_INDEX         = 11; 
        public const int    UM_PLAYER_OFFLINETIME_INDEX        = 12;

        ///<summary>
        ///装备信息
        ///</summary>
        public const int    UM_PLAYER_ITEMS_INDEX              = 13; 

        ///<summary>
        ///20级及以上才有值，否则为空
        ///</summary>
        public const int    UM_PLAYER_FIGHT_ATTRIES            = 14; 

        ///<summary>
        ///20级及以上才有值，否则为空
        ///</summary>
        public const int    UM_PLAYER_FIGHT_ATTRIES_TMP        = 15; 

        ///<summary>
        ///已装备的技能
        ///</summary>
        public const int    UM_PLAYER_SPELL_SLOTS              = 16; 

        ///<summary>
        ///玩家身上所带的buff
        ///</summary>
        public const int    UM_PLAYER_BUFFS                    = 17; 

        ///<summary>
        ///已学会的技能
        ///</summary>
        public const int    UM_PLAYER_LEARNED_SPELLS           = 18; 

        ///<summary>
        ///技能启用专精
        ///</summary>
        public const int    UM_PLAYER_SPELL_PROFICIENT         = 19; 

        ///<summary>
        ///玩家的外形
        ///</summary>
        public const int    UM_PLAYER_FACADE                   = 20; 

        ///<summary>
        ///帐号名字
        ///</summary>
        public const int    UM_PLAYER_ACCOUNT                  = 22; 

        ///<summary>
        ///称号
        ///</summary>
        public const int    UM_PLAYER_TITLE_INDEX              = 23; 

        ///<summary>
        ///宝石封灵
        ///</summary>
        public const int    UM_PLAYER_SEAL_SPIRIT_INDEX        = 24; 

        ///<summary>
        ///teaming 是否在组队状态
        ///</summary>
        public const int    UM_PLAYER_TEAMING_INDEX            = 25; 

        ///<summary>
        ///teamLeader 是否队长
        ///</summary>
        public const int    UM_PLAYER_TEAM_LEADER_INDEX        = 26; 

        ///<summary>
        ///场景id.mpa_id,通过map_id可得inst_id
        ///</summary>
        public const int    UM_PLAYER_MAP_ID_INDEX             = 27; 

        ///<summary>
        ///玩家vip等级
        ///</summary>
        public const int    UM_PLAYER_VIP_LEVEL_INDEX          = 28; 

        ///<summary>
        ///是否在单人副本状态
        ///</summary>
        public const int    UM_PLAYER_CONSOLE_INDEX            = 29; 

        ///<summary>
        ///公会id
        ///</summary>
        public const int    UM_PLAYER_GUILD_ID_INDEX           = 30; 

        ///<summary>
        ///公会名字
        ///</summary>
        public const int    UM_PLAYER_GUILD_NAME_INDEX         = 31; 

        ///<summary>
        ///强化信息
        ///</summary>
        public const int    UM_PLAYER_STRENGTHEN_DATA          = 32; 

        ///<summary>
        ///翅膀总战斗力
        ///</summary>
        public const int    UM_PLAYER_WING_FIGHT_FORCE         = 33; 

        ///<summary>
        ///符文
        ///</summary>
        public const int    UM_PLAYER_BODY_RUNE                = 34; 

        ///<summary>
        ///邀请码
        ///</summary>
        public const int    UM_PLAYER_INVITE_CODE              = 35; 

        ///<summary>
        ///还需要map_line分线的记录
        ///</summary>
        public const int    UM_PLAYER_MAP_LINE_INDEX           = 36; 

        ///<summary>
        ///玩家实体id
        ///</summary>
        public const int    UM_PLAYER_EID_INDEX                = 37; 

        ///<summary>
        ///玩家平台名字
        ///</summary>
        public const int    UM_PLAYER_PLAT_NAME                = 38; 

        ///<summary>
        ///角色当前装备的翅膀
        ///</summary>
        public const int    UM_PLAYER_WING_BODY                = 39; 

        ///<summary>
        ///炼金已经协助次数
        ///</summary>
        public const int    UM_PLAYER_ALCHEMY_ASSIST_CNT       = 40; 

        ///<summary>
        ///pk战记 --100条上限
        ///</summary>
        public const int    UM_PLAYER_PK_LOGS                  = 41; 

        ///<summary>
        ///推送系统设置
        ///</summary>
        public const int    UM_PLAYER_PUSH_CFG                 = 42; 

        ///<summary>
        ///登陆天数
        ///</summary>
        public const int    UM_PLAYER_LOGIN_DAYS               = 43; 

        ///<summary>
        ///附魔信息
        ///</summary>
        public const int    UM_PLAYER_ENCHANT_INDEX            = 44; 

        ///<summary>
        ///玩家系统战力数据
        ///</summary>
        public const int    UM_PLAYER_SYS_FIGHT                = 45; 



        ///<summary>
        ///申请某个场景时，扩展参数所带的key值定义
        ///</summary>
    

        ///<summary>
        ///玩家的dbid
        ///</summary>
        public const int    SPACE_APPLY_KEY_DBID                        = 1;  

        ///<summary>
        ///是否创建cell
        ///</summary>
        public const int    SPACE_APPLY_KEY_CREATE_CELL                 = 2;  

        ///<summary>
        ///进入位置的x坐标
        ///</summary>
        public const int    SPACE_APPLY_KEY_MAPX                        = 3;  

        ///<summary>
        ///进入位置的y坐标
        ///</summary>
        public const int    SPACE_APPLY_KEY_MAPY                        = 4;  

        ///<summary>
        ///玩家在跨服服务器上的唯一编号
        ///</summary>
        public const int    SPACE_APPLY_KEY_CROSS_UUID                  = 5;  

        ///<summary>
        ///玩家的等级
        ///</summary>
        public const int    SPACE_APPLY_KEY_LEVEL                       = 6;  

        ///<summary>
        ///玩家的战斗力
        ///</summary>
        public const int    SPACE_APPLY_KEY_FF                          = 7;  

        ///<summary>
        ///队伍编号
        ///</summary>
        public const int    SPACE_APPLY_KEY_TEAM_ID                     = 9;  

        ///<summary>
        ///0：表示直接进入；1：表示随机匹配
        ///</summary>
        public const int    SPACE_APPLY_KEY_MATCH                       = 10;  

        ///<summary>
        ///1：表示个人；2：表示队伍
        ///</summary>
        public const int    SPACE_APPLY_KEY_ENT_TYPE                    = 11;  

        ///<summary>
        ///申请本服关卡时所有队友的等级信息
        ///</summary>
        public const int    SPACE_APPLY_KEY_TEAMERS                     = 12;  

        ///<summary>
        ///申请跨服关卡时所有队友的信息
        ///</summary>
        public const int    SPACE_APPLY_KEY_CROSS_TEAMERS               = 13; 

        ///<summary>
        ///关卡的难度系数
        ///</summary>
        public const int    SPACE_APPLY_KEY_DIFFICULTY                  = 14;  

        ///<summary>
        ///玩家在该副本中的阵营编号
        ///</summary>
        public const int    SPACE_APPLY_KEY_CAMP_ID                     = 15;  

        ///<summary>
        ///申请本服关卡时已匹配的组员信息
        ///</summary>
        public const int    SPACE_APPLY_KEY_MATCH_TEAMERS               = 16; 

        ///<summary>
        ///申请者的mailbox的字符串
        ///</summary>
        public const int    SPACE_APPLY_KEY_MAILBOX                     = 17; 

        ///<summary>
        ///是否是队伍新增队员来加入匹配
        ///</summary>
        public const int    SPACE_APPLY_KEY_TEAM_ADD                    = 18; 

        ///<summary>
        ///队伍匹配成功的玩家的dbid
        ///</summary>
        public const int    SPACE_APPLY_KEY_TEAM_DBIDS                  = 19; 

        ///<summary>
        ///队伍需要的机器人数量
        ///</summary>
        public const int    SPACE_APPLY_KEY_ROBOT_COUNT                 = 20; 

        ///<summary>
        ///队伍的平均等级
        ///</summary>
        public const int    SPACE_APPLY_KEY_AVERAGE_LEVEL               = 21; 

        ///<summary>
        ///一起进入该副本的队伍人数
        ///</summary>
        public const int    SPACE_APPLY_KEY_PLAYER_COUNT                = 22; 

        ///<summary>
        ///一起进入该副本的战斗力最高玩家dbid
        ///</summary>
        public const int    SPACE_APPLY_KEY_MAX_FF_DBID                 = 23; 

        ///<summary>
        ///一起进入该副本的某个玩家的mailbox的字符串
        ///</summary>
        public const int    SPACE_APPLY_KEY_PARTNER_MB                  = 24; 

        ///<summary>
        ///一起再来一局该副本的所有玩家的mailbox的字符串数组
        ///</summary>
        public const int    SPACE_APPLY_KEY_PLAY_AGAIN_MBS              = 25; 

        ///<summary>
        ///一起再来一局该副本的所有玩家的等级数组
        ///</summary>
        public const int    SPACE_APPLY_KEY_PLAY_AGAIN_LEVELS           = 26; 

        ///<summary>
        ///战斗型传送门信息({[1] = 传送门的Mbstr})
        ///</summary>
        public const int    SPACE_APPLY_KEY_PORTAL_INFO                 = 27; 

        ///<summary>
        ///当指定分线号不存在时则自动创建分线号（此流程未测试过）
        ///</summary>
        public const int    SPACE_APPLY_KEY_AUTO_CREATE_LINE            = 28; 

        ///<summary>
        ///公会赛场景类型（{[1] = 类型（见public_config.GUILD_MATCH_MISSION定义），[2] = 公会ID或候选赛ID（根据类型决定），[3] = 公会管理器Mbstr，[4] = 候选赛轮数，[5] = 候选赛分组}）
        ///</summary>
        public const int    SPACE_APPLY_KEY_GUILD_MATCH_MISSION_INFO    = 29; 

        ///<summary>
        ///公会小组赛淘汰赛中匹配规则的key，用于计算分数
        ///</summary>
        public const int    SPACE_APPLY_KEY_MATCH_RULE_K                = 30; 

        ///<summary>
        ///公会小组赛淘汰赛中场次，用于计算分数给客户端显示
        ///</summary>
        public const int    SPACE_APPLY_KEY_MATCH_ROUND_COUNT           = 31; 

        ///<summary>
        ///公会小组赛淘汰赛中，类型，区分小组赛，淘汰赛王者组，淘汰赛精英组。
        ///</summary>
        public const int    SPACE_APPLY_KEY_MATCH_PK_TYPE               = 32; 

        ///<summary>
        ///传送方式。默认nil或者0。    1就是队伍召唤传送
        ///</summary>
        public const int    SPACE_APPLY_KEY_TP_WAY                      = 33; 

        ///<summary>
        ///决斗比赛场信息({[1] = match_id})
        ///</summary>
        public const int    SPACE_APPLY_KEY_DUEL_MATCH_INFO             = 34; 

        ///<summary>
        ///公会pk的机器人dbid。对战公会长的dbid
        ///</summary>
        public const int    SPACE_APPLY_KEY_GUILD_LEADER_DBID           = 35; 

        ///<summary>
        ///公会pk机器人需要
        ///</summary>
        public const int    SPACE_APPLY_KEY_GUILD_DBID                  = 36; 

        ///<summary>
        ///开启这个副本的申请者id
        ///</summary>
        public const int    SPACE_APPLY_KEY_APPLYER_ID                  = 37; 
    

        ///<summary>
        ///mailbox中key的定义
        ///</summary>
    

        ///<summary>
        ///server_id
        ///</summary>
        public const int    MAILBOX_KEY_SERVER_ID    = 1;      

        ///<summary>
        ///entity ent_type
        ///</summary>
        public const int    MAILBOX_KEY_CLASS_TYPE   = 2;      

        ///<summary>
        ///entity id
        ///</summary>
        public const int    MAILBOX_KEY_ENTITY_ID    = 3;      


        ///<summary>
        ///随机类型
        ///</summary>
    

        ///<summary>
        ///多次随机时，已随机结果不被剔除随机池
        ///</summary>
        public const int    RANDOM_TYPE_1               = 1;    

        ///<summary>
        ///多次随机时，已随机结果剔除随机池
        ///</summary>
        public const int    RANDOM_TYPE_2               = 2;    

        ///<summary>
        ///每个随机概率独立计算
        ///</summary>
        public const int    RANDOM_TYPE_3               = 3;    


        ///<summary>
        ///排行榜各个子系统管理器的编号定义
        ///</summary>
    


        ///<summary>
        ///战力排行榜
        ///</summary>
        public const int    MGR_ID_RANK_FIGHT_FORCE             = 1;  

        ///<summary>
        ///等级排行榜
        ///</summary>
        public const int    MGR_ID_RANK_LEVEL                   = 2;  

        ///<summary>
        ///公会排行榜
        ///</summary>
        public const int    MGR_ID_RANK_GUILD                   = 3;  

        ///<summary>
        ///恶魔之门日排行榜
        ///</summary>
        public const int    MGR_ID_RANK_EVIL_DAY                = 4;  

        ///<summary>
        ///恶魔之门周排行榜
        ///</summary>
        public const int    MGR_ID_RANK_EVIL_WEEK               = 5;  

        ///<summary>
        ///野外寻宝周排行榜
        ///</summary>
        public const int    MGR_ID_RANK_WILD_TREASURE           = 6;  

        ///<summary>
        ///晚上活动按公会积分的排行榜
        ///</summary>
        public const int    MGR_ID_RANK_ACTIVITY                = 7;  

        ///<summary>
        ///决斗排行榜
        ///</summary>
        public const int    MGR_ID_RANK_DUEL                    = 8;  


        ///<summary>
        ///公会成员子系统管理
        ///</summary>
    

        ///<summary>
        ///公会成员子系统
        ///</summary>
        public const int    MGR_ID_GUILD_MEMBER                 = 1;  
        public const int    MGR_ID_GUILD_ACHIEVEMENT            = 2;
        public const int    MGR_ID_GUILD_SKILL                  = 3;

        ///<summary>
        ///公会战子系统
        ///</summary>
        public const int    MGR_ID_GUILD_BATTLE                 = 4;  

        ///<summary>
        ///公会商城系统
        ///</summary>
        public const int    MGR_ID_GUILD_SHOP                   = 5;  

        ///<summary>
        ///公会任务系统
        ///</summary>
        public const int    MGR_ID_GUILD_TASK                   = 6;  



        ///<summary>
        ///---------------------------物品相关---------------------
        ///</summary>
    

        ///<summary>
        ///物品内存、存盘数据结构。若要增加字段只能往后面增加，不能修改和删除已有的字段。
        ///</summary>
    

        ///<summary>
        ///物品id
        ///</summary>
        public const int    ITEM_ATTRI_ID                       = 1;    

        ///<summary>
        ///物品数量
        ///</summary>
        public const int    ITEM_ATTRI_COUNT                    = 2;    

        ///<summary>
        ///插槽（宝石）
        ///</summary>
        public const int    ITEM_ATTRI_SLOTS                    = 3;    

        ///<summary>
        ///可扩展槽激活数量（宝石）
        ///</summary>
        public const int    ITEM_ATTRI_NEW_SLOT_CNT             = 4;    

        ///<summary>
        ///装备固定属性{[属性类型]=属性值, ...}
        ///</summary>
        public const int    ITEM_ATTRI_EQUIP_VALUE              = 5;    

        ///<summary>
        ///装备buff效果（buff id数组）
        ///</summary>
        public const int    ITEM_ATTRI_EQUIP_BUFF               = 6;    

        ///<summary>
        ///当前经验,符文系统中的符文道具
        ///</summary>
        public const int    ITEM_ATTRI_CUR_EXP                  = 7;    

        ///<summary>
        ///该物品是否非绑定，0：绑定；1：非绑定
        ///</summary>
        public const int    ITEM_ATTRI_IS_UNBIND                = 8;    

        ///<summary>
        ///每个道具都加一个uuid，用于跟踪道具
        ///</summary>
        public const int    ITEM_ATTRI_UUID                     = 11;   

        ///<summary>
        ///记录物品失效时间戳
        ///</summary>
        public const int    ITEM_ATTRI_DEAD_TIME                = 12;   

        ///<summary>
        ///符文是否解锁
        ///</summary>
        public const int    ITEM_ATTRI_RUNE_LOCK                = 13;   

        ///<summary>
        ///记录装备的套装id
        ///</summary>
        public const int    ITEM_ATTRI_SUIT                     = 14;   

        ///<summary>
        ///附魔次数
        ///</summary>
        public const int    ITEM_ATTRI_ENCHANT_NUM              = 16;   

        ///<summary>
        ///强化
        ///</summary>
        public const int    ITEM_ATTRI_STRENGTHEN               = 17;   

        ///<summary>
        ///战斗力
        ///</summary>
        public const int    ITEM_ATTRI_FIGHT_FORCE              = 18;   

        ///<summary>
        ///物品获取时间
        ///</summary>
        public const int    ITEM_ATTRI_GET_TIME                 = 19;   

        ///<summary>
        ///能够获取这个物品的玩家列表
        ///</summary>
        public const int    ITEM_ATTRI_USER_LIST                = 20;   

        ///<summary>
        ///重铸次数
        ///</summary>
        public const int    ITEM_ATTRI_RECAST_COUNT             = 21;   

        ///<summary>
        ///获得的副本
        ///</summary>
        public const int    ITEM_ATTRI_INSTANCE                 = 22;   


        ///<summary>
        ///未绑定道具
        ///</summary>
        public const int    ITEM_BIND_ID_NO                     = 1;    

        ///<summary>
        ///绑定道具
        ///</summary>
        public const int    ITEM_BIND_ID_OK                     = 0;    


        ///<summary>
        ///回调函数
        ///</summary>
        public const int    ITEM_TRANSFER_FUNC                  = 1;    

        ///<summary>
        ///道具信息
        ///</summary>
        public const int    ITEM_TRANSFER_ITEM                  = 2;    

        ///<summary>
        ///角色DBID
        ///</summary>
        public const int    ITEM_TRANSFER_ROLE                  = 3;    

        ///<summary>
        ///符文相关
        ///</summary>
    

        ///<summary>
        ///"身体符文'属性
        ///</summary>
        public const int    BODY_RUNE_ATTRI_LOCK                = 1; 

        ///<summary>
        ///'身体符文'属性
        ///</summary>
        public const int    BODY_RUNE_ATTRI_RUNE                = 2; 


        ///<summary>
        ///许愿最后的时间列表
        ///</summary>
        public const int    RUNE_WISH_LAST_TIME                 = 1; 

        ///<summary>
        ///许愿剩余的免费次数列表
        ///</summary>
        public const int    RUNE_WISH_COUNT                     = 2; 


        ///<summary>
        ///免费许愿类型
        ///</summary>
        public const int    RUNE_WISH_FREE                        = 1;

        ///<summary>
        ///许愿一次
        ///</summary>
        public const int    RUNE_WISH_ONE                         = 2;

        ///<summary>
        ///许愿十次
        ///</summary>
        public const int    RUNE_WISH_TEN                         = 3;


        ///<summary>
        ///金币符文
        ///</summary>
        public const int    RUNE_MONEY                          = 1;

        ///<summary>
        ///经验符文
        ///</summary>
        public const int    RUNE_EXP                            = 2;

        ///<summary>
        ///生命符文
        ///</summary>
        public const int    RUNE_HP                             = 3;

        ///<summary>
        ///暴击符文
        ///</summary>
        public const int    RUNE_HIT                            = 4;


        ///<summary>
        ///物品类型
        ///</summary>
    

        ///<summary>
        ///所有道具，前端用来枚举，后端没有这个
        ///</summary>
        public const int    ITEM_TYPE_ALL           = 0;    

        ///<summary>
        ///普通道具
        ///</summary>
        public const int    ITEM_TYPE_COMMOM        = 1;    

        ///<summary>
        ///装备
        ///</summary>
        public const int    ITEM_TYPE_EQUIP         = 2;    

        ///<summary>
        ///宝石
        ///</summary>
        public const int    ITEM_TYPE_GEM           = 3;    

        ///<summary>
        ///符文
        ///</summary>
        public const int    ITEM_TYPE_RUNE          = 4;    

        ///<summary>
        ///附魔卷轴
        ///</summary>
        public const int    ITEM_TYPE_ENCHANT_SCROLL= 5;    

        ///<summary>
        ///货币
        ///</summary>
        public const int    ITEM_TYPE_CURRENCY      = 6;    

        ///<summary>
        ///工会贡献
        ///</summary>
        public const int    ITEM_TYPE_GUILD_CONTRIBUTION = 8;

        ///<summary>
        ///工会资金
        ///</summary>
        public const int    ITEM_TYPE_GUILD_FUND    = 9;    

        ///<summary>
        ///材料
        ///</summary>
        public const int    ITEM_TYPE_MATERIAL      = 11;   

        ///<summary>
        ///幸运石
        ///</summary>
        public const int    ITEM_TYPE_LUCKY_STONE   = 12;   

        ///<summary>
        ///buff法球
        ///</summary>
        public const int    ITEM_TYPE_BUFF_ITEM     = 13;   

        ///<summary>
        ///套装卷轴
        ///</summary>
        public const int    ITEM_TYPE_SUIT_SCROLL   = 14;   

        ///<summary>
        ///建筑图纸
        ///</summary>
        public const int    ITEM_TYPE_BUILD_DRAWING = 15;   

        ///<summary>
        ///代币
        ///</summary>
        public const int    ITEM_TYPE_TOKEN         = 16;   

        ///<summary>
        ///好友亲密值道具
        ///</summary>
        public const int    ITEM_TYPE_FRIEND        = 17;   

        ///<summary>
        ///商城优惠劵
        ///</summary>
        public const int    ITEM_TYPE_COUPON        = 18;   

        ///<summary>
        ///装备宝盒
        ///</summary>
        public const int    ITEM_TYPE_EQUIP_BOX     = 19;   

        ///<summary>
        ///时装
        ///</summary>
        public const int    ITEM_TYPE_FASHION       = 20;   

        ///<summary>
        ///翅膀
        ///</summary>
        public const int    ITEM_TYPE_WING          = 21;   

        ///<summary>
        ///宠物
        ///</summary>
        public const int    ITEM_TYPE_PET           = 22;   

        ///<summary>
        ///魔灵精华,宠物进阶材料
        ///</summary>
        public const int    ITEM_TYPE_PET_MAT       = 30;   

        ///<summary>
        ///野外bossA
        ///</summary>
        public const int    ITEM_TYPE_WILD_BOSS_A   = 31;   

        ///<summary>
        ///野外bossB
        ///</summary>
        public const int    ITEM_TYPE_WILD_BOSS_B   = 32;   

        ///<summary>
        ///特权卡（包括月卡，季卡，年卡）
        ///</summary>
        public const int    ITEM_TYPE_CARD          = 33;   

        ///<summary>
        ///藏宝图
        ///</summary>
        public const int    ITEM_TYPE_TREASURE_MAP  = 34;   


        ///<summary>
        ///白色
        ///</summary>
        public const int    ITEM_QUALITY_WHITE     = 1;    

        ///<summary>
        ///绿色
        ///</summary>
        public const int    ITEM_QUALITY_GREEN     = 2;    

        ///<summary>
        ///蓝色
        ///</summary>
        public const int    ITEM_QUALITY_BLUE      = 3;    

        ///<summary>
        ///紫色
        ///</summary>
        public const int    ITEM_QUALITY_PURPLE    = 4;    

        ///<summary>
        ///橙色
        ///</summary>
        public const int    ITEM_QUALITY_ORANGE    = 5;    

        ///<summary>
        ///金色
        ///</summary>
        public const int    ITEM_QUALITY_GOLDEN    = 6;    

        ///<summary>
        ///红色
        ///</summary>
        public const int    ITEM_QUALITY_RED       = 7;    


        ///<summary>
        ///背包类型
        ///</summary>
    

        ///<summary>
        ///道具背包
        ///</summary>
        public const int    PKG_TYPE_BASE                       = 1; 

        ///<summary>
        ///玩家身上装备栏
        ///</summary>
        public const int    PKG_TYPE_BODY_EQUIP                 = 2; 

        ///<summary>
        ///物品赎回仓库
        ///</summary>
        public const int    PKG_TYPE_REDEEM                     = 3; 

        ///<summary>
        ///符文背包
        ///</summary>
        public const int    PKG_TYPE_RUNE                       = 4; 

        ///<summary>
        ///身体符文背包
        ///</summary>
        public const int    PKG_TYPE_BODY_RUNE                  = 5; 


        ///<summary>
        ///背包全部栏总格子数
        ///</summary>
        public const int    MAX_COUNT_PKG_ITEMS_ALL       = 84; 

        ///<summary>
        ///仓库总格子数
        ///</summary>
        public const int    MAX_COUNT_PKG_REDEEM          = 5; 

        ///<summary>
        ///身上装备栏总格子数
        ///</summary>
        public const int    MAX_COUNT_PKG_BODY_EQUIP      = 11;  

        ///<summary>
        ///符文背包总格子数
        ///</summary>
        public const int    MAX_COUNT_PKG_RUNE            = 60; 

        ///<summary>
        ///身体符文背包总格子数
        ///</summary>
        public const int    MAX_COUNT_PKG_BODY_RUNE       = 72; 


        ///<summary>
        ///货币类型
        ///</summary>
    

        ///<summary>
        ///货币类型最小值（包含）
        ///</summary>
        public const int    MONEY_TYPE_MIN_ID                   = 1;  

        ///<summary>
        ///金币
        ///</summary>
        public const int    MONEY_TYPE_GOLD                     = 1;  

        ///<summary>
        ///钻石(直接对应人民币)
        ///</summary>
        public const int    MONEY_TYPE_DIAMOND                  = 2;  

        ///<summary>
        ///绑定钻石
        ///</summary>
        public const int    MONEY_TYPE_BIND_DIAMOND             = 3;  

        ///<summary>
        ///荣誉
        ///</summary>
        public const int    MONEY_TYPE_HONOUR                   = 4;  

        ///<summary>
        ///体力
        ///</summary>
        public const int    MONEY_TYPE_ENERGY                   = 5;  

        ///<summary>
        ///精力
        ///</summary>
        public const int    MONEY_TYPE_POWER                    = 6;  

        ///<summary>
        ///符文石
        ///</summary>
        public const int    MONEY_TYPE_RUNE_STONE               = 7;  

        ///<summary>
        ///工会贡献(存到公会成员上)
        ///</summary>
        public const int    MONEY_TYPE_GUILD_CONTRIBUTION       = 8;  

        ///<summary>
        ///工会资金(存到工会上)
        ///</summary>
        public const int    MONEY_TYPE_GUILD_FUND               = 9;  

        ///<summary>
        ///星魂
        ///</summary>
        public const int    MONEY_TYPE_STAR_SPIRIT              = 10; 

        ///<summary>
        ///石头
        ///</summary>
        public const int    MONEY_TYPE_FORTRESS_STONE           = 11; 

        ///<summary>
        ///木头
        ///</summary>
        public const int    MONEY_TYPE_FORTRESS_WOOD            = 12; 

        ///<summary>
        ///活跃值，不扣除
        ///</summary>
        public const int    MONEY_TYPE_DAILY_ACTIVE_POINT       = 13; 

        ///<summary>
        ///活跃值，会扣除
        ///</summary>
        public const int    MONEY_TYPE_ACTIVE_POINT             = 14; 

        ///<summary>
        ///成就点
        ///</summary>
        public const int    MONEY_TYPE_ACHIEVEMENT_POINT        = 15; 

        ///<summary>
        ///钻石(直接对应人民币)  且触发充值事件
        ///</summary>
        public const int    MONEY_TYPE_DIAMOND_CHARGE           = 16; 

        ///<summary>
        ///导师值 货币
        ///</summary>
        public const int    MONEY_TYPE_TUTOR_VALUE              = 17; 

        ///<summary>
        ///导师经验 
        ///</summary>
        public const int    MONEY_TYPE_TUTOR_EXP                = 18; 

        ///<summary>
        ///货币类型最大值（包含）
        ///</summary>
        public const int    MONEY_TYPE_MAX_ID                   = 18; 


        ///<summary>
        ///消费类型的类别
        ///</summary>
    

        ///<summary>
        ///未定义归类（无效属性）
        ///</summary>
        public const int    COST_CLASS_UNDEFINE                 = 0;  

        ///<summary>
        ///个人身上的属性和普通背包道具（本地属性，可即时操作）
        ///</summary>
        public const int    COST_CLASS_AVATAR                   = 1;  

        ///<summary>
        ///公会或公会成员相关的属性（远程属性，需要异步跨进程操作）
        ///</summary>
        public const int    COST_CLASS_GUILD                    = 2;  


        ///<summary>
        ///特殊物品类型
        ///</summary>
    

        ///<summary>
        ///经验
        ///</summary>
        public const int    ITEM_SPECIAL_TYPE_EXP               = 50; 

        ///<summary>
        ///buff
        ///</summary>
        public const int    ITEM_SPECIAL_TYPE_BUFF              = 51; 

        ///<summary>
        ///按等级自适应加经验
        ///</summary>
        public const int    ITEM_SPECIAL_TYPE_LEVEL_EXP         = 60; 

        ///<summary>
        ///按等级自适应加金币
        ///</summary>
        public const int    ITEM_SPECIAL_TYPE_LEVEL_GOLD        = 61; 

        ///<summary>
        ///宠物经验
        ///</summary>
        public const int    ITEM_SPECIAL_TYPE_PET_EXP           = 62; 


        ///<summary>
        ///技能系统中的能量
        ///</summary>
        public const int    ITEM_SPECIAL_SPELL_ENERGY           = 100;

        ///<summary>
        ///技能系统中的MP能量
        ///</summary>
        public const int ITEM_SPECIAL_SPELL_MP                  = 1; 


        ///<summary>
        ///道具类最小id（包括）
        ///</summary>
        public const int    ITEM_ID_MIN                         = 1000; 

        ///<summary>
        ///宝石最大等级
        ///</summary>
        public const int    GEM_MAX_LEVEL                       = 10;


        ///<summary>
        ///1w --int2float转换精度 --属性系统用到
        ///</summary>
        public const int    FLOAT_PRECISION                     = 10000; 


        ///<summary>
        ///宝石类型
        ///</summary>
    
        public const int    GEM_TYPE_LIFE                       = 1;
        public const int    GEM_TYPE_CRIT                       = 2;
        public const int    GEM_TYPE_STRIKE                     = 3;
        public const int    GEM_TYPE_DAMAGE                     = 4;
        public const int    GEM_TYPE_PHYSICS_DEFENSE            = 5;
        public const int    GEM_TYPE_MAGIC_DAMAGE               = 6;
        public const int    GEM_TYPE_PENETRATE                  = 7;
        public const int    GEM_TYPE_STRENGTH                   = 8;
        public const int    GEM_TYPE_WARD                       = 9;


        ///<summary>
        ///增加物品操作类型
        ///</summary>
        public const int    OPT_TYPE_ADD    = 1;     

        ///<summary>
        ///更新物品操作类型
        ///</summary>
        public const int    OPT_TYPE_UPDATE = 2;     

        ///<summary>
        ///删除物品操作类型
        ///</summary>
        public const int    OPT_TYPE_DEL    = 3;     


        ///<summary>
        ///物品使用效果指向的配置表
        ///</summary>
    

        ///<summary>
        ///空效果
        ///</summary>
        public const int    USE_EFFECT_CFG_EMPTY                = 0;    

        ///<summary>
        ///道具奖励表
        ///</summary>
        public const int    USE_EFFECT_CFG_ITEM_REWARD          = 1;    

        ///<summary>
        ///buff表
        ///</summary>
        public const int    USE_EFFECT_CFG_BUFF                 = 2;    

        ///<summary>
        ///配方表
        ///</summary>
        public const int    USE_EFFECT_CFG_CRAFT                = 3;    

        ///<summary>
        ///value为界面id
        ///</summary>
        public const int    USE_EFFECT_CFG_OPEN_UI              = 4;    

        ///<summary>
        ///套装卷轴
        ///</summary>
        public const int    USE_EFFECT_CFG_SUIT_SCROLL          = 5;    

        ///<summary>
        ///value为建筑id
        ///</summary>
        public const int    USE_EFFECT_CFG_BUILDING             = 6;    

        ///<summary>
        ///value为代币对应的商店id
        ///</summary>
        public const int    USE_EFFECT_CFG_EXCHANGE             = 7;    

        ///<summary>
        ///好友亲密值道具,这里不指向，只取对应的值
        ///</summary>
        public const int    USE_EFFECT_CFG_FRIEND               = 8;    

        ///<summary>
        ///宠物
        ///</summary>
        public const int    USE_EFFECT_CFG_PET                  = 9;    

        ///<summary>
        ///宠物经验
        ///</summary>
        public const int    USE_EFFECT_CFG_PET_EXP              = 10;   

        ///<summary>
        ///指向翅膀id
        ///</summary>
        public const int    USE_EFFECT_CFG_WING                 = 11;   

        ///<summary>
        ///指向时装id
        ///</summary>
        public const int    USE_EFFECT_CFG_FASHION              = 12;   

        ///<summary>
        ///月卡
        ///</summary>
        public const int    USE_EFFECT_CFG_VIP_CARD_MONTH       = 13;   

        ///<summary>
        ///季卡
        ///</summary>
        public const int    USE_EFFECT_CFG_VIP_CARD_SEASON      = 14;   

        ///<summary>
        ///年卡
        ///</summary>
        public const int    USE_EFFECT_CFG_VIP_CARD_YEAR        = 15;   

        ///<summary>
        ///藏宝图
        ///</summary>
        public const int    USE_EFFECT_CFG_TREASURE_MAP         = 16;   

        ///<summary>
        ///VIP卡
        ///</summary>
        public const int    USE_EFFECT_CFG_VIP_CARD             = 17;   

        ///<summary>
        ///合成
        ///</summary>
        public const int    USE_EFFECT_CFG_COMPOSE              = 18;   

        ///<summary>
        ///数量，需要和其他效果组合使用
        ///</summary>
        public const int    USE_EFFECT_CFG_NUM                  = 19;   

        ///<summary>
        ///指向坐骑id
        ///</summary>
        public const int    USE_EFFECT_CFG_RIDE                 = 20;   


        ///<summary>
        ///物品失效时间类型
        ///</summary>
    

        ///<summary>
        ///道具失效相对时间
        ///</summary>
        public const int    ITEM_DEAD_TIME_TYPE_RELATIVE        = 1;    

        ///<summary>
        ///道具失效绝对时间
        ///</summary>
        public const int    ITEM_DEAD_TIME_TYPE_ABSOLUTE        = 2;    


        ///<summary>
        ///物品数据结构类型
        ///</summary>
    

        ///<summary>
        ///key value结构
        ///</summary>
        public const int    ITEMS_STRUCT_TYPE_KV                = 1;    

        ///<summary>
        ///物品存盘数据结构
        ///</summary>
        public const int    ITEMS_STRUCT_TYPE_SAVE              = 2;    



        ///<summary>
        ///---------------------------装备相关---------------------
        ///</summary>
    


        ///<summary>
        ///武器
        ///</summary>
        public const int    EQUIP_POS_WEAPON                   = 1; 

        ///<summary>
        ///头盔
        ///</summary>
        public const int    EQUIP_POS_HEAD                     = 2; 

        ///<summary>
        ///肩膀
        ///</summary>
        public const int    EQUIP_POS_SHOULDER                 = 3; 

        ///<summary>
        ///胸甲
        ///</summary>
        public const int    EQUIP_POS_CHEST                    = 4; 

        ///<summary>
        ///手
        ///</summary>
        public const int    EQUIP_POS_ARM                      = 5; 

        ///<summary>
        ///腰带
        ///</summary>
        public const int    EQUIP_POS_BELT                     = 6; 

        ///<summary>
        ///裤子
        ///</summary>
        public const int    EQUIP_POS_TROUSERS                 = 7; 

        ///<summary>
        ///鞋子
        ///</summary>
        public const int    EQUIP_POS_SHOES                    = 8; 

        ///<summary>
        ///项链
        ///</summary>
        public const int    EQUIP_POS_NECKLANCE                = 9; 

        ///<summary>
        ///左戒指
        ///</summary>
        public const int    EQUIP_POS_LEFTRING                 = 10;

        ///<summary>
        ///右戒指
        ///</summary>
        public const int    EQUIP_POS_RIGHTRING                = 11;



        ///<summary>
        ///装备扩展槽偏移
        ///</summary>
        public const int    EQUIP_NEW_SLOTS_OFFSET             = 20;   

        ///<summary>
        ///通用插槽类型
        ///</summary>
        public const int    EQUIP_COMMON_SLOT_TYPE             = 99;   


        ///<summary>
        ///---------------------------聊天相关---------------------
        ///</summary>
    

        ///<summary>
        ///综合聊天频道
        ///</summary>
        public const int    CHANNEL_ID_ALL          = 0;    

        ///<summary>
        ///世界聊天频道id
        ///</summary>
        public const int    CHANNEL_ID_WORLD        = 1;    

        ///<summary>
        ///附近聊天频道id
        ///</summary>
        public const int    CHANNEL_ID_VICINITY     = 2;    

        ///<summary>
        ///组队聊天频道id
        ///</summary>
        public const int    CHANNEL_ID_TEAM         = 3;    

        ///<summary>
        ///公会聊天频道id
        ///</summary>
        public const int    CHANNEL_ID_GUILD        = 4;    

        ///<summary>
        ///私聊聊天频道id
        ///</summary>
        public const int    CHANNEL_ID_PRIVATE      = 5;    

        ///<summary>
        ///帮助消息
        ///</summary>
        public const int    CHANNEL_ID_HELP         = 6;    

        ///<summary>
        ///传闻消息
        ///</summary>
        public const int    CHANNEL_ID_RUMOR        = 7;    

        ///<summary>
        ///系统消息
        ///</summary>
        public const int    CHANNEL_ID_SYSTEM       = 8;    

        ///<summary>
        ///好友频道（只支持在线好友链接转发）
        ///</summary>
        public const int    CHANNEL_ID_FRIEND       = 9;    

        ///<summary>
        ///弹窗           --对话框
        ///</summary>
        public const int    CHANNEL_ID_DLG          = 22;   

        ///<summary>
        ///顶部滚动 --跑马灯
        ///</summary>
        public const int    CHANNEL_ID_ROLLING      = 23;   




        ///<summary>
        ///举报违法违规
        ///</summary>
        public const int    REPORT_REASON_ILLEGAL             = 1;    

        ///<summary>
        ///线下交易
        ///</summary>
        public const int    REPORT_REASON_LINE_TRADE          = 2;    

        ///<summary>
        ///发布广告
        ///</summary>
        public const int    REPORT_REASON_PUBLISH_AD          = 3;    

        ///<summary>
        ///侮辱他人
        ///</summary>
        public const int    REPORT_REASON_INSULT_OTHERS       = 4;    


        ///<summary>
        ///文本类型
        ///</summary>
        public const int    CHAT_MSG_TYPE_TEXT                = 1;    

        ///<summary>
        ///物品展示
        ///</summary>
        public const int    CHAT_MSG_TYPE_ITEM_SHOW           = 2;    

        ///<summary>
        ///语音聊天
        ///</summary>
        public const int    CHAT_MSG_TYPE_VOICE               = 3;    

        ///<summary>
        ///公会邀请
        ///</summary>
        public const int    CHAT_MSG_TYPE_GUILD_CALL          = 4;    

        ///<summary>
        ///组队喊话邀请
        ///</summary>
        public const int    CHAT_MSG_TYPE_TEAM_CALL           = 5;    

        ///<summary>
        ///分享野外宝箱，boss
        ///</summary>
        public const int    CHAT_MSG_TYPE_TREASURE_POS        = 6;    

        ///<summary>
        ///提示消息（物品掉落）
        ///</summary>
        public const int    CHAT_MSG_TYPE_TIPS                = 7;    

        ///<summary>
        ///界面消息（界面公告）
        ///</summary>
        public const int    CHAT_MSG_TYPE_VIEW                = 8;    

        ///<summary>
        ///系统消息（boss分享等等）
        ///</summary>
        public const int    CHAT_MSG_TYPE_SYSTEM              = 9;    

        ///<summary>
        ///红包消息
        ///</summary>
        public const int    CHAT_MSG_TYPE_RED_ENVELOPE        = 10;   



        ///<summary>
        ///最大展示数据
        ///</summary>
        public const int    MAX_CHAT_ITEM_CNT                 = 10000;  


        ///<summary>
        ///-------------------------客户端show_text显示频道---------------------
        ///</summary>
    

        ///<summary>
        ///这个可在中文表加个text_id配置，而不需要服务器指定
        ///</summary>
    

        ///<summary>
        ///对话框
        ///</summary>
        public const int    SHOW_TEXT_DLG           = 1;  

        ///<summary>
        ///提示频道
        ///</summary>
        public const int    SHOW_TEXT_TIPS          = 2;  

        ///<summary>
        ///调试频道
        ///</summary>
        public const int    SHOW_TEXT_DBG           = 3;  

        ///<summary>
        ///世界悬浮提示(屏幕上方滑动)
        ///</summary>
        public const int    SHOW_TEXT_WORLD         = 4;  

        ///<summary>
        ///世界悬浮提示和对话框同时显示
        ///</summary>
        public const int    SHOW_TEXT_DLG_WORLD     = 5;  

        ///<summary>
        ///聊天框
        ///</summary>
        public const int    SHOW_TEXT_CHATDLG       = 6;  


        ///<summary>
        ///-----------------副本中玩家信息的属性编号定义-------------------------
        ///</summary>
    

        ///<summary>
        ///PlayerInfo字段的index，entityId
        ///</summary>
        public const int    PLAYER_CM_INDEX_EID       = 1;   

        ///<summary>
        ///PlayerInfo字段的index，PVP是否准备好
        ///</summary>
        public const int    PLAYER_CM_INDEX_PREPARED  = 2;   

        ///<summary>
        ///PlayerInfo字段的index，每日已结算的挑战次数
        ///</summary>
        public const int    PLAYER_CM_DAILY_COUNT     = 3;   


        ///<summary>
        ///---------------------------邮件相关---------------------
        ///</summary>
    

        ///<summary>
        ///邮件未读
        ///</summary>
        public const int    MAIL_NOT_READ     = 1;    

        ///<summary>
        ///邮件已读
        ///</summary>
        public const int    MAIL_READ         = 2;    


        ///<summary>
        ///邮件无附件
        ///</summary>
        public const int    MAIL_NONE         = 3;    

        ///<summary>
        ///邮件附件未领取
        ///</summary>
        public const int    MAIL_NOT_GET      = 4;    

        ///<summary>
        ///邮件附件已领取
        ///</summary>
        public const int    MAIL_GET          = 5;    


        ///<summary>
        ///-----------邮件类型分类-----------------
        ///</summary>
    

        ///<summary>
        ///玩家发送的邮件
        ///</summary>
        public const int    MAIL_TYPE_PLAYER  = 0;   

        ///<summary>
        ///系统发送的邮件
        ///</summary>
        public const int    MAIL_TYPE_SYS     = 1;   


        ///<summary>
        ///两个月的秒数
        ///</summary>
        public const int    MAIL_TIMEOUT        = 5184000;   

        ///<summary>
        ///邮件列表1页的邮件个数
        ///</summary>
        public const int    MAIL_CNT_EVERY_PAGE = 5;    


        ///<summary>
        ///---------------------------任务相关---------------------
        ///</summary>
    


        ///<summary>
        ///任务状态                             服务端             客户端显示
        ///</summary>
    

        ///<summary>
        ///任务可接          进行中，寻找npc
        ///</summary>
        public const int    TASK_STATE_NEW              = 1;    

        ///<summary>
        ///任务已接受        进行中
        ///</summary>
        public const int    TASK_STATE_ACCEPT           = 2;    

        ///<summary>
        ///任务完成          领奖
        ///</summary>
        public const int    TASK_STATE_ACCOMPLISH       = 3;    

        ///<summary>
        ///任务已领奖        已完成
        ///</summary>
        public const int    TASK_STATE_REWARDED         = 4;    

        ///<summary>
        ///任务删除          清掉这个任务
        ///</summary>
        public const int    TASK_STATE_REMOVE           = 5;    


        ///<summary>
        ///主线任务
        ///</summary>
        public const int    TASK_TYPE_MAIN              = 1;    

        ///<summary>
        ///支线任务
        ///</summary>
        public const int    TASK_TYPE_BRANCH            = 2;    

        ///<summary>
        ///公会任务
        ///</summary>
        public const int    TASK_TYPE_GUILD             = 3;    

        ///<summary>
        ///组队任务
        ///</summary>
        public const int    TASK_TYPE_TEAM              = 4;    

        ///<summary>
        ///野外任务
        ///</summary>
        public const int    TASK_TYPE_WILD              = 5;    


        ///<summary>
        ///---------------------------工会相关---------------------
        ///</summary>
    

        ///<summary>
        ///未完成
        ///</summary>
        public const int    ACHIEVEMENT_STATE_PROCESSING = 1;   

        ///<summary>
        ///已完成，未领奖
        ///</summary>
        public const int    ACHIEVEMENT_STATE_ACCOMPLISH = 2;   

        ///<summary>
        ///已领奖
        ///</summary>
        public const int    ACHIEVEMENT_STATE_REWARDED   = 3;   


        ///<summary>
        ///要塞
        ///</summary>
    

        ///<summary>
        ///要塞升级
        ///</summary>
        public const int    FORTRESS_UPGRADE            = 1;    

        ///<summary>
        ///要塞建筑升级
        ///</summary>
        public const int    FORTRESS_BUILDING_UPGRADE   = 2;    


        ///<summary>
        ///时装
        ///</summary>
    

        ///<summary>
        ///新时装
        ///</summary>
        public const int    FASHION_NEW                 = 1;    

        ///<summary>
        ///时装过期
        ///</summary>
        public const int    FASHION_EXPIRE              = 2;    

        ///<summary>
        ///时装续期
        ///</summary>
        public const int    FASHION_RENEWAL             = 3;    

        ///<summary>
        ///更换时装
        ///</summary>
        public const int    FASHION_CHANGE              = 4;    


        ///<summary>
        ///坐骑
        ///</summary>
    

        ///<summary>
        ///新坐骑
        ///</summary>
        public const int    RIDE_NEW                 = 1;    

        ///<summary>
        ///坐骑过期
        ///</summary>
        public const int    RIDE_EXPIRE              = 2;    

        ///<summary>
        ///坐骑续期
        ///</summary>
        public const int    RIDE_RENEWAL             = 3;    

        ///<summary>
        ///更换坐骑
        ///</summary>
        public const int    RIDE_CHANGE              = 4;    

        ///<summary>
        ///---------------------------副本相关----------------------------
        ///</summary>
    


        ///<summary>
        ///标识副本评价 C
        ///</summary>
        public const int    MISSION_VALUATION_C           = 0;       

        ///<summary>
        ///标识副本评价 B
        ///</summary>
        public const int    MISSION_VALUATION_B           = 1;       

        ///<summary>
        ///标识副本评价 A
        ///</summary>
        public const int    MISSION_VALUATION_A           = 2;       

        ///<summary>
        ///标识副本评价 S
        ///</summary>
        public const int    MISSION_VALUATION_S           = 3;       


        ///<summary>
        ///------------------------avatar.mission_record的标志字段---------------------
        ///</summary>
    

        ///<summary>
        ///副本id
        ///</summary>
        public const int    MISSION_ID                    = 1;        

        ///<summary>
        ///副本胜利标示
        ///</summary>
        public const int    MISSION_WIN_FLAG              = 2;        

        ///<summary>
        ///每个副本的最大星数/分数
        ///</summary>
        public const int    MISSION_BEST_GRADE            = 3;        

        ///<summary>
        ///每日已经挑战次数
        ///</summary>
        public const int    MISSION_DAILY_COUNT           = 4;        

        ///<summary>
        ///首次奖励是否已经领取
        ///</summary>
        public const int    MISSION_HAS_GET_FIRST_REWARD  = 5;        


        ///<summary>
        ///普通行为id
        ///</summary>
        public const int    MISSION_COMMON_ACTION_ID        = 1;        

        ///<summary>
        ///特殊行为id
        ///</summary>
        public const int    MISSION_SPECIAL_ACTION_ID       = 2;        


        ///<summary>
        ///副本某行为是否加星
        ///</summary>
        public const int    MISSION_IS_ADD_STAR           = 9999;    


        ///<summary>
        ///评星关卡
        ///</summary>
        public const int    MISSION_STAR_INSTANCE           = 1;        

        ///<summary>
        ///评分关卡
        ///</summary>
        public const int    MISSION_POINT_INSTANCE          = 2;        

        ///<summary>
        ///胜负关卡
        ///</summary>
        public const int    MISSION_VICTORY_DEFEAT_INSTANCE = 3;        

        ///<summary>
        ///行为积分关卡
        ///</summary>
        public const int    MISSION_ACTION_SCORE_INSTANCE   = 4;        



        ///<summary>
        ///没有奖励
        ///</summary>
        public const int    MISSION_NO_REWARD_TYPE                 = 0;        

        ///<summary>
        ///翻牌奖励（已取消）
        ///</summary>
        public const int    MISSION_TURN_POKER_REWARD_TYPE         = 1;        

        ///<summary>
        ///宝箱奖励
        ///</summary>
        public const int    MISSION_TREASURE_BOX_REWARD_TYPE       = 2;        

        ///<summary>
        ///排名奖励(已取消)
        ///</summary>
        public const int    MISSION_RANK_REWARD_TYPE               = 3;        

        ///<summary>
        ///队伍胜负+个人排名奖励
        ///</summary>
        public const int    MISSION_VICTORY_RANK_REWARD_TYPE       = 4;        

        ///<summary>
        ///队伍分数段+个人排名奖励
        ///</summary>
        public const int    MISSION_SCORE_SEGMENT_RANK_REWARD_TYPE = 5;        


        ///<summary>
        ///副本s奖励
        ///</summary>
        public const int    MISSION_EVERY_REWARD_S        = 1;        

        ///<summary>
        ///副本a奖励
        ///</summary>
        public const int    MISSION_EVERY_REWARD_A        = 2;        

        ///<summary>
        ///副本b奖励
        ///</summary>
        public const int    MISSION_EVERY_REWARD_B        = 3;        

        ///<summary>
        ///首次s奖励
        ///</summary>
        public const int    MISSION_FIRST_S_REWARD        = 4;        

        ///<summary>
        ///首次奖励
        ///</summary>
        public const int    MISSION_FIRST_REWARD          = 5;        

        ///<summary>
        ///掉落奖励
        ///</summary>
        public const int    MISSION_DROP_ITEMS_REWARD     = 6;        



        ///<summary>
        ///章节id
        ///</summary>
        public const int    CHAPTER_ID                    = 1;        

        ///<summary>
        ///章节星魂数量
        ///</summary>
        public const int    CHAPTER_STAR_SPIRIT           = 2;        

        ///<summary>
        ///章节星魂奖励
        ///</summary>
        public const int    CHAPTER_STAR_SPIRIT_REWARD    = 3;        

        ///<summary>
        ///章节重置时间
        ///</summary>
        public const int    CHAPTER_STAR_RESET_TIME       = 4;        

        ///<summary>
        ///--------------------副本行为记录-----------------------------------
        ///</summary>
    

        ///<summary>
        ///玩家私人数据
        ///</summary>
    

        ///<summary>
        ///副本通过时间
        ///</summary>
        public const int    MISSION_ACTION_PASS_TIME                 = 1;          

        ///<summary>
        ///副本杀怪数量
        ///</summary>
        public const int    MISSION_ACTION_KILL_MON_NUM              = 2;          

        ///<summary>
        ///副本玩家血量比
        ///</summary>
        public const int    MISSION_ACTION_BLOOD_RATE                = 3;          

        ///<summary>
        ///副本杀人数量
        ///</summary>
        public const int    MISSION_ACTION_KILL_PLAYER_NUM           = 4;          

        ///<summary>
        ///副本死亡次数
        ///</summary>
        public const int    MISSION_ACTION_DIE_NUM                   = 5;          

        ///<summary>
        ///副本释放技能次数
        ///</summary>
        public const int    MISSION_ACTION_CAST_SPELL_NUM            = 6;          

        ///<summary>
        ///副本掉落奖励
        ///</summary>
        public const int    MISSION_ACTION_DROP_ITEMS_REWARD         = 7;          

        ///<summary>
        ///宠物经验
        ///</summary>
        public const int    MISSION_ACTION_PET_EXP                   = 8;          

        ///<summary>
        ///{0=10,1=11,2=12,3=13}
        ///</summary>
    

        ///<summary>
        ///副本个人总积分
        ///</summary>
        public const int    MISSION_ACTION_SELF_SCORE                = 10;         

        ///<summary>
        ///副本个人积分类型1,如个人奖励积分
        ///</summary>
        public const int    MISSION_ACTION_SELF_SCORE1               = 11;         

        ///<summary>
        ///副本个人积分类型2,如个人奖励积分
        ///</summary>
        public const int    MISSION_ACTION_SELF_SCORE2               = 12;         

        ///<summary>
        ///副本个人积分类型3,如个人奖励积分
        ///</summary>
        public const int    MISSION_ACTION_SELF_SCORE3               = 13;         


        ///<summary>
        ///副本输出总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_DAMAGE             = 20;        

        ///<summary>
        ///副本受到总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_DAMAGE                = 21;        


        ///<summary>
        ///副本输出物理总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_PHYSICS_DAMAGE     = 22;        

        ///<summary>
        ///副本输出魔法总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_MAGIC_DAMAGE       = 23;        

        ///<summary>
        ///副本输出火元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_FIRE_DAMAGE        = 24;        

        ///<summary>
        ///副本输出冰元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_ICE_DAMAGE         = 25;        

        ///<summary>
        ///副本输出电元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_ELEC_DAMAGE        = 26;        

        ///<summary>
        ///副本输出毒元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_POISON_DAMAGE      = 27;        


        ///<summary>
        ///副本受到物理总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_PHYSICS_DAMAGE        = 28;        

        ///<summary>
        ///副本受到魔法总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_MAGIC_DAMAGE          = 29;        

        ///<summary>
        ///副本受到火元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_FIRE_DAMAGE           = 30;        

        ///<summary>
        ///副本受到冰元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_ICE_DAMAGE            = 31;        

        ///<summary>
        ///副本受到电元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_ELEC_DAMAGE           = 32;        

        ///<summary>
        ///副本受到毒元素总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_POISON_DAMAGE         = 33;        


        ///<summary>
        ///副本暴击造成的总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_CRIT_DAMAGE        = 34;        

        ///<summary>
        ///副本破击造成的总伤害
        ///</summary>
        public const int    MISSION_ACTION_OUTPUT_STRIKE_DAMAGE      = 35;        

        ///<summary>
        ///副本受到的暴击总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_CRIT_DAMAGE           = 36;        

        ///<summary>
        ///副本受到的破击总伤害
        ///</summary>
        public const int    MISSION_ACTION_GET_STRIKE_DAMAGE         = 37;        


        ///<summary>
        ///副本受到的治疗总量
        ///</summary>
        public const int    MISSION_ACTION_GET_CURE_ALL              = 38;        
    

        ///<summary>
        ///统计技能输出伤害偏移量
        ///</summary>
        public const int    SPELL_DAMAGE_OUTPUT_OFFSET               = 21;        

        ///<summary>
        ///统计技能输出伤害偏移量
        ///</summary>
        public const int    SPELL_DAMAGE_GET_OFFSET                  = 27;        


        ///<summary>
        ///玩家dbid
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_DBID_ID            = 50;        

        ///<summary>
        ///玩家名字
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_NAME               = 51;        

        ///<summary>
        ///玩家等级
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_LEVEL              = 52;        

        ///<summary>
        ///玩家职业
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_VOCATION           = 53;        

        ///<summary>
        ///玩家战力
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_FIGHT_FORCE        = 54;        

        ///<summary>
        ///玩家人物经验
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_EXP                = 55;        

        ///<summary>
        ///阵营id
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_GROUP_ID           = 56;        

        ///<summary>
        ///本次玩家获得的经验
        ///</summary>
        public const int    MISSION_ACTION_AVATAR_GAIN_EXP           = 57;        


        ///<summary>
        ///副本中玩家所属阵营总的杀怪数量
        ///</summary>
        public const int    MISSION_ACTION_CAMP_KILL_MON_NUM         = 58;        

        ///<summary>
        ///怪物类型3(BOSS)的剩余血量百分比
        ///</summary>
        public const int    MISSION_ACTION_BOSS_BLOOD_RATE           = 59;        


        ///<summary>
        ///-----------在pvp场景中记录每个玩家的战斗信息-----------------
        ///</summary>
    

        ///<summary>
        ///攻击过自己的玩家
        ///</summary>
        public const int    MISSION_ACTION_ATTACKERS                 = 60;        

        ///<summary>
        ///玩家助攻次数
        ///</summary>
        public const int    MISSION_ACTION_ASSIST_CNT                = 61;        

        ///<summary>
        ///连杀数量
        ///</summary>
        public const int    MISSION_ACTION_CONTINUE_KILL_CNT         = 62;        

        ///<summary>
        ///上次击杀时间
        ///</summary>
        public const int    MISSION_ACTION_LAST_KILL_TIME            = 63;        

        ///<summary>
        ///多杀数量
        ///</summary>
        public const int    MISSION_ACTION_MULTI_KILL_CNT            = 64;        


        ///<summary>
        ///下面是公共的{}
        ///</summary>
    

        ///<summary>
        ///副本阵营积分{1=100,2=100,...}
        ///</summary>
        public const int    MISSION_ACTION_CAMP_SCORE                = 100;        

        ///<summary>
        ///副本pve积分{1=100,2=100,...}
        ///</summary>
        public const int    MISSION_ACTION_PVE_SCORE                 = 101;        

        ///<summary>
        ///副本pvp积分{1=100,2=100,...}
        ///</summary>
        public const int    MISSION_ACTION_PVP_SCORE                 = 102;        

        ///<summary>
        ///副本内某个功能计数{1=100,2=100,...}
        ///</summary>
        public const int    MISSION_ACTION_FUNC_SCORE                = 103;        



        ///<summary>
        ///记录首杀玩家的id
        ///</summary>
        public const int    MISSION_ACTION_FIRST_KILL                = 150;        

        ///<summary>
        ///所有玩家个人信息{action_id=value}
        ///</summary>
        public const int    MISSION_ACTION_PLAYERS_INFO              = 200;        

        ///<summary>
        ///所有宠物的统计信息{action_id=value}
        ///</summary>
        public const int    MISSION_ACTION_PETS_INFO                 = 300;        

        ///<summary>
        ///所有宠物的主人映射{[pet_eid] = }
        ///</summary>
        public const int    MISSION_ACTION_OWNER_PETS                = 301;        

        ///<summary>
        ///副本的特殊行为条件最小值
        ///</summary>
        public const int    MISSION_ACTION_CONDITION_MIN             = 1000;       

        ///<summary>
        ///副本行为多个条件偏移量
        ///</summary>
        public const int    MISSION_ACTION_LINE_OFFSET               = 3000;       



        ///<summary>
        ///---------------------------好友相关----------------------------
        ///</summary>
    


        ///<summary>
        ///好友列表1页的数据个数
        ///</summary>
        public const int    FRIEND_CNT_EVERY_PAGE        = 20;   

        ///<summary>
        ///FRIEND_MAX_NUM               = 100,  --好友总个数
        ///</summary>
    

        ///<summary>
        ///通过名字查找玩家
        ///</summary>
        public const int    FRIEND_SEARCH_BY_NAME        = 1;   

        ///<summary>
        ///通过dbid查找玩家
        ///</summary>
        public const int    FRIEND_SEARCH_BY_DBID        = 2;   

        ///<summary>
        ///可接受体力祝福个数
        ///</summary>
        public const int    FRIEND_MAX_RECEIVE           = 30;  


        ///<summary>
        ///好友一起打副本获得好友度   --副本结算
        ///</summary>
        public const int    FREIDN_DEGREE_MISSION        = 1;   

        ///<summary>
        ///赠送亲密值道具获得好友度
        ///</summary>
        public const int    FREIDN_DEGREE_GIVE_GOODS     = 2;   

        ///<summary>
        ///持续时间获得(组队，且好友,且野外)
        ///</summary>
        public const int    FREIDN_DEGREE_TIME           = 3;   

        ///<summary>
        ///合作击杀boss(组队，且好友,且野外)
        ///</summary>
        public const int    FREIDN_DEGREE_KILL           = 4;   

        ///<summary>
        ///好友代充值
        ///</summary>
        public const int    FREIDN_DEGREE_CHARGE         = 5;   

        ///<summary>
        ///炼金增加
        ///</summary>
        public const int    FREIDN_DEGREE_ALCHEMY        = 6;   



        ///<summary>
        ///---------------------------离线消息----------------------------
        ///</summary>
    

        ///<summary>
        ///请求加好友信息
        ///</summary>
        public const int    OFFLINE_FRIEND_ADD           = 1;   

        ///<summary>
        ///公会系统提示消息
        ///</summary>
        public const int    OFFLINE_GUILD_SYS_INFO       = 2;   

        ///<summary>
        ///拍卖行系统先关的离线消息
        ///</summary>
        public const int    OFFLINE_AUCITON_SYS_INFO     = 3;   



        ///<summary>
        ///---------------------------公会相关----------------------------
        ///</summary>
    

        ///<summary>
        ///公会职位
        ///</summary>
    

        ///<summary>
        ///会长
        ///</summary>
        public const int    GUILD_POSITION_LEADER             = 1;   

        ///<summary>
        ///副会长
        ///</summary>
        public const int    GUILD_POSITION_VICE_LEADER        = 2;   

        ///<summary>
        ///执事
        ///</summary>
        public const int    GUILD_POSITION_DEACON             = 3;   

        ///<summary>
        ///精英
        ///</summary>
        public const int    GUILD_POSITION_ELITE              = 4;   

        ///<summary>
        ///普通成员
        ///</summary>
        public const int    GUILD_POSITION_MEMBER             = 5;   


        ///<summary>
        ///公会总人数
        ///</summary>
        public const int    GUILD_MAX_MEMBER                  = 30;  

        ///<summary>
        ///公会列表1页的数据个数
        ///</summary>
        public const int    GUILD_CNT_EVERY_PAGE              = 20;   

        ///<summary>
        ///公会成员列表1页的数据个数
        ///</summary>
        public const int    GUILD_MEMBER_CNT_EVERY_PAGE       = 4;   


        ///<summary>
        ///正常状态
        ///</summary>
        public const int    GUILD_STATE_NORMAL                = 0;   

        ///<summary>
        ///将要删除
        ///</summary>
        public const int    GUILD_STATE_DELETEING             = 1;   

        ///<summary>
        ///已经被删除
        ///</summary>
        public const int    GUILD_STATE_DELETE                = 2;   


        ///<summary>
        ///公会技能，公会效果最小ID
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_MIN                        = 1;           

        ///<summary>
        ///减少维护所需资金
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_MAINTAIN                   = 1;           

        ///<summary>
        ///提高公会成员每日捐献的金币上限
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_HANDIN_GOLD_LIMIT          = 2;           

        ///<summary>
        ///提高公会成员每日捐献获得的公会资金数量
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_HANDIN_FUND_NUM            = 3;           

        ///<summary>
        ///提高公会成员每日捐献获得的贡献数量
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_HANDIN_CONTRIBUTION_NUM    = 4;           

        ///<summary>
        ///提高公会成员每日捐献的钻石上限
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_HANDIN_DIAMOND_LIMIT       = 5;           

        ///<summary>
        ///提高公会成员每日捐献的绑钻上限
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_HANDIN_BIND_DIAMOND_LIMIT  = 6;           

        ///<summary>
        ///公会技能，公会效果最大ID
        ///</summary>
        public const int    GUILD_SKILL_ID_GUILD_MAX                        = 500;         


        ///<summary>
        ///公会技能，玩家效果最小ID
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_MIN                       = 501;         

        ///<summary>
        ///增加公会成员幻境探索金币收
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_DREAMLAND_GOLD            = 501;         

        ///<summary>
        ///增加公会成员幻境探索经验收益
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_DREAMLAND_EXP             = 502;         

        ///<summary>
        ///增加公会成员试炼秘境产出试炼币
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_TRYCOPY                   = 503;         

        ///<summary>
        ///增加公会成员晚间活动产出暗夜碎片
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_NIGHTCOPY                 = 504;         

        ///<summary>
        ///增加公会成员组队秘境产出秘境碎片
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_TEAMCOPY                  = 505;         

        ///<summary>
        ///指向BUFF表
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_BUFF                      = 1000;        

        ///<summary>
        ///公会技能，玩家效果最大ID
        ///</summary>
        public const int    GUILD_SKILL_ID_AVATAR_MAX                       = 1000;        


        ///<summary>
        ///公会成员列表排序规则名字
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_TYPE_NAME                  = 1;           

        ///<summary>
        ///公会成员列表排序规则等级
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_TYPE_LEVEL                 = 2;           

        ///<summary>
        ///公会成员列表排序规则战斗力
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_TYPE_FIGHT                 = 3;           

        ///<summary>
        ///公会成员列表排序规则职位
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_TYPE_POSITION              = 4;           

        ///<summary>
        ///公会成员列表排序规则昨日贡献
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_TYPE_YESTERDAY_CONTRIBUTION= 5;           


        ///<summary>
        ///公会成员排序升序
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_ASC                        = 1;           

        ///<summary>
        ///公会成员排序降序
        ///</summary>
        public const int    GUILD_MEMBER_LIST_ORDER_DESC                       = 2;           



        ///<summary>
        ///公会定时器ID
        ///</summary>
    

        ///<summary>
        ///删除公会
        ///</summary>
        public const int    TIMER_ID_GUILD_DEL                = 1;   


        ///<summary>
        ///删除公会延迟秒数（3天）
        ///</summary>
        public const int    GUILD_DEL_DELAY_TIME              = 259200;   

        ///<summary>
        ///---------------------------附魔相关----------------------------
        ///</summary>
    


        ///<summary>
        ///附魔存盘数据结构
        ///</summary>
    

        ///<summary>
        ///属性id
        ///</summary>
        public const int    ENCHANT_INFO_ATTRI_ID       = 1;    

        ///<summary>
        ///属性值
        ///</summary>
        public const int    ENCHANT_INFO_ATTRI_VALUE    = 2;    


        ///<summary>
        ///---------------------------组队相关-------------------------------
        ///</summary>
    

        ///<summary>
        ///组队最小等级 --通用组队，无论哪个玩法都要满足。玩法(可能map_id)等可额外判断，如果无法满足，要求队长重新组队。
        ///</summary>
        public const int    TEAM_MIN_LEVEL       = 1;  

        ///<summary>
        ///组队最小人数  --同理上面
        ///</summary>
        public const int    TEAM_MIN_COUNT       = 2;  

        ///<summary>
        ///组队最大人数  --同理上面
        ///</summary>
        public const int    TEAM_MAX_COUNT       = 4;  

        ///<summary>
        ///组队列表分页数
        ///</summary>
        public const int    TEAM_PAGE_CNT        = 20; 



        ///<summary>
        ///-------------------- 场景重置方式-----------------------
        ///</summary>
    

        ///<summary>
        ///场景中没人
        ///</summary>
        public const int    RESET_SPACE_NO_PLAYER    = 1;    

        ///<summary>
        ///活动结束时间已到
        ///</summary>
        public const int    RESET_SPACE_TIME_END     = 2;    



        ///<summary>
        ///---------------------玩家进入副本个人限制条件-----------------------
        ///</summary>
    

        ///<summary>
        ///个人限制条件下限（包含）
        ///</summary>
        public const int    LIMIT_AVATAR_CONDITION_MIN      = 1;    

        ///<summary>
        ///等级限制（默认等级大于）
        ///</summary>
        public const int    LIMIT_LEVEL                     = 1;    

        ///<summary>
        ///职业限制
        ///</summary>
        public const int    LIMIT_VOCATION                  = 2;    

        ///<summary>
        ///VIP等级限制
        ///</summary>
        public const int    LIMIT_VIP_LEVEL                 = 3;    

        ///<summary>
        ///任务已完成/已领奖限制
        ///</summary>
        public const int    LIMIT_TASK                      = 4;    

        ///<summary>
        ///性别限制（后端未实现）
        ///</summary>
        public const int    LIMIT_GENDER                    = 5;    

        ///<summary>
        ///战力限制（后端未实现）
        ///</summary>
        public const int    LIMIT_FIGHT_FORCE               = 6;    

        ///<summary>
        ///技能限制（后端未实现）
        ///</summary>
        public const int    LIMIT_SPELL                     = 7;    

        ///<summary>
        ///要塞建筑开启
        ///</summary>
        public const int    LIMIT_FORTRESS                  = 8;    

        ///<summary>
        ///任务已接受/已完成/已领奖
        ///</summary>
        public const int    LIMIT_TASK_ACCEPTED             = 9;    

        ///<summary>
        ///通关某个章节
        ///</summary>
        public const int    LIMIT_PASS_CHAPTER              = 10;   

        ///<summary>
        ///通关某个试炼秘境副本
        ///</summary>
        public const int    LIMIT_PASS_TRY_MISSION          = 11;   

        ///<summary>
        ///公会贡献值（后端未实现）
        ///</summary>
        public const int    LIMIT_GUILD_CONTRIBUTION        = 12;   

        ///<summary>
        ///任务已领奖
        ///</summary>
        public const int    LIMIT_TASK_REWARDED             = 13;   

        ///<summary>
        ///地图限制
        ///</summary>
        public const int    LIMIT_MAP                       = 14;   

        ///<summary>
        ///要塞功能限制
        ///</summary>
        public const int    LIMIT_FORTRESS_FUNCTION         = 15;   

        ///<summary>
        ///背包剩余格子限制
        ///</summary>
        public const int    LIMIT_BAG_REMAIN_GRID           = 16;   

        ///<summary>
        ///等级小于满足条件
        ///</summary>
        public const int    LIMIT_LEVEL_LT                  = 17;   

        ///<summary>
        ///历史充值金额
        ///</summary>
        public const int    LIMIT_CHARGE_HISTORY_NUM        = 18;   

        ///<summary>
        ///导师等级限制（默认等级大于）
        ///</summary>
        public const int    LIMIT_TUTOR_LEVEL               = 19;    

        ///<summary>
        ///个人限制条件上限（不包含）
        ///</summary>
        public const int    LIMIT_AVATAR_CONDITION_MAX      = 100;  



        ///<summary>
        ///buff限制
        ///</summary>
        public const int    LIMIT_BUFF                      = 40;   

        ///<summary>
        ///装备限制（后端未实现）
        ///</summary>
        public const int    LIMIT_EQUIP                     = 50;   


        ///<summary>
        ///---------------------公会相关的限制条件-----------------------
        ///</summary>
    

        ///<summary>
        ///公会限制条件上限（包含）
        ///</summary>
        public const int    LIMIT_GUILD_CONDITION_MIN       = 101;  

        ///<summary>
        ///公会等级限制大于等于条件值
        ///</summary>
        public const int    LIMIT_GUILD_LEVEL_ME            = 101;  

        ///<summary>
        ///公会成员数限制大于等于条件值
        ///</summary>
        public const int    LIMIT_GUILD_MEMBER_COUNT_ME     = 102;  

        ///<summary>
        ///公会繁荣度大于等于条件值
        ///</summary>
        public const int    LIMIT_GUILD_BOOM_ME             = 103;  

        ///<summary>
        ///公会成员贡献值大于等于条件值
        ///</summary>
        public const int    LIMIT_GUILD_CONTRIBUTION_ME     = 104;  

        ///<summary>
        ///公会资金大于等于条件值
        ///</summary>
        public const int    LIMIT_GUILD_FUND_ME             = 105;  

        ///<summary>
        ///公会限制条件上限（不包含）
        ///</summary>
        public const int    LIMIT_GUILD_CONDITION_MAX       = 200;  


        ///<summary>
        ///---------------------玩家进入副本组队限制条件-----------------------
        ///</summary>
    

        ///<summary>
        ///队伍成员全部来自同一个公会 1:1
        ///</summary>
        public const int    LIMIT_TEAM_SAME_GUILD           = 1;  

        ///<summary>
        ///队伍成员全部为女性 2:1
        ///</summary>
        public const int    LIMIT_TEAM_FEMALE               = 2;  

        ///<summary>
        ///队伍成员全部为男性 3:1
        ///</summary>
        public const int    LIMIT_TEAM_MALE                 = 3;  

        ///<summary>
        ///队伍成员为1男1女（情人节副本用） 4:1
        ///</summary>
        public const int    LIMIT_TEAM_VALENTINE            = 4;  

        ///<summary>
        ///队伍成员等级差不超过n级   5:10
        ///</summary>
        public const int    LIMIT_TEAM_LEVEL_DIFF           = 5;  




        ///<summary>
        ///--------------------物品替换表 系统ID-----------------
        ///</summary>
    

        ///<summary>
        ///强化系统
        ///</summary>
        public const int    ITEM_REPLACE_SYS_STRENGTHEN     = 1; 


        ///<summary>
        ///--------------------翅膀相关-----------------------
        ///</summary>
    

        ///<summary>
        ///翅膀数据
        ///</summary>
        public const int    WING_DATA_INDEX            = 2;    

        ///<summary>
        ///记录翅膀套装已经添加的效果列表
        ///</summary>
        public const int    WING_SUIT_INDEX            = 3;    


        ///<summary>
        ///翅膀当前总的经验
        ///</summary>
        public const int    WING_CUR_SUM_EXP           = 1;    


        ///<summary>
        ///培养一次
        ///</summary>
        public const int    WING_TRAIN_ONE             = 1;    

        ///<summary>
        ///培养十次
        ///</summary>
        public const int    WING_TRAIN_TEN             = 2;    


        ///<summary>
        ///-------------------代币商店-----------------------------
        ///</summary>
    

        ///<summary>
        ///普通道具
        ///</summary>
        public const int    TOKEN_SHOP_GOOD_TYPE_COMMON       = 1;    

        ///<summary>
        ///赌博道具
        ///</summary>
        public const int    TOKEN_SHOP_GOOD_TYPE_GAMBLE       = 2;    

        ///<summary>
        ///限量道具
        ///</summary>
        public const int    TOKEN_SHOP_GOOD_TYPE_LIMIT        = 3;    



        ///<summary>
        ///-------------------幻境探索-----------------------------
        ///</summary>
    

        ///<summary>
        ///飞龙绿色品质
        ///</summary>
        public const int    DREAMLAND_QUALITY_GREEN       =  2;   

        ///<summary>
        ///飞龙蓝色品质
        ///</summary>
        public const int    DREAMLAND_QUALITY_BLUE        =  3;   

        ///<summary>
        ///飞龙粉色品质
        ///</summary>
        public const int    DREAMLAND_QUALITY_PURPLE      =  4;   

        ///<summary>
        ///飞龙橙色品质
        ///</summary>
        public const int    DREAMLAND_QUALITY_ORANGE      =  5;   

        ///<summary>
        ///飞龙暗金品质
        ///</summary>
        public const int    DREAMLAND_QUALITY_GOLD        =  6;   



        ///<summary>
        ///-------------------buff系统相关-----------------------------
        ///</summary>
    

        ///<summary>
        ///队伍人数奖励加成buff
        ///</summary>
        public const int    TEAM_CNT_2_BUFF         = 2001;         
        public const int    TEAM_CNT_3_BUFF         = 2002;
        public const int    TEAM_CNT_4_BUFF         = 2003;


        ///<summary>
        ///野外收益上限显示buff
        ///</summary>
        public const int    WILD_LIMIT_BUFF         = 50;          

        ///<summary>
        ///cg的时候加一个buff --但注意时间动态。
        ///</summary>
        public const int    CG_BUFF                 = 4907;        

        ///<summary>
        ///设置为死亡灵魂状态
        ///</summary>
        public const int    BUFF_DEAD_GHOST         = 201;         



        ///<summary>
        ///------------------商城系统------------------------------------
        ///</summary>
    


        ///<summary>
        ///销售模式
        ///</summary>
    

        ///<summary>
        ///普通模式
        ///</summary>
        public const int    MARKET_MODE_NORMAL    =   0;  

        ///<summary>
        ///限时
        ///</summary>
        public const int    MARKET_MODE_TIME      =   1;  

        ///<summary>
        ///总量限购
        ///</summary>
        public const int    MARKET_MODE_TOTAL     =   2;  

        ///<summary>
        ///vip专属
        ///</summary>
        public const int    MARKET_MODE_VIP       =   3;  


        ///<summary>
        ///购买模式
        ///</summary>
    

        ///<summary>
        ///仅购买
        ///</summary>
        public const int    MARKET_BUY_MODE_BUY           =   0;  

        ///<summary>
        ///仅赠送
        ///</summary>
        public const int    MARKET_BUY_MODE_GIVE          =   1;  

        ///<summary>
        ///购买加赠送
        ///</summary>
        public const int    MARKET_BUY_MODE_BUY_AND_GIVE  =   2;  


        ///<summary>
        ///赠送亲密度限制
        ///</summary>
        public const int    MARKET_FRIEND_DEGREE          =   3;    

        ///<summary>
        ///每日赠送钻石限制
        ///</summary>
        public const int    MARKET_DAILY_GIVE_PRICE       =   1000; 

        ///<summary>
        ///赠送者累计钻石限制
        ///</summary>
        public const int    MARKET_TOTAL_CONSUME_PRICE    =   5000; 


        ///<summary>
        ///------------------排行榜系统----------------------------------
        ///</summary>
    

        ///<summary>
        ///每页20个
        ///</summary>
        public const int    RANK_CNT_EVERY_PAGE = 20; 


        ///<summary>
        ///-----------------拍卖行物品搜索条件----------------------
        ///</summary>
    

        ///<summary>
        ///搜索条件定义
        ///</summary>
    

        ///<summary>
        ///按货币类型
        ///</summary>
        public const int    SEARCH_CND_MONEY_TYPE = 1; 

        ///<summary>
        ///按物品类型搜索
        ///</summary>
        public const int    SEARCH_CND_TYPE = 2;       

        ///<summary>
        ///按物品子类型搜索
        ///</summary>
        public const int    SEARCH_CND_POS = 3;    

        ///<summary>
        ///按等级搜索
        ///</summary>
        public const int    SEARCH_CND_LEVEL = 4;      

        ///<summary>
        ///按职业搜索
        ///</summary>
        public const int    SEARCH_CND_VOCATION = 5;   



        ///<summary>
        ///--------------------副本行为事件id,通知客户端--------------------------------
        ///</summary>
    

        ///<summary>
        ///定时器行为触发剩余时间，事件扩展参数为：剩余时间（单位为毫秒）
        ///</summary>
        public const int    SPACE_ACTION_EVENT_TIMER_LEFT_TIME  = 1;  

        ///<summary>
        ///区域触发器行为触发剩余时间，事件扩展参数为：剩余时间（单位为毫秒）
        ///</summary>
        public const int    SPACE_ACTION_EVENT_REGION_LEFT_TIME = 2; 

        ///<summary>
        ///某个行为停止计时，事件扩展参数：无
        ///</summary>
        public const int    SPACE_ACTION_EVENT_STOP_TIMER = 3;       

        ///<summary>
        ///条件行为的每个目标进度，事件扩展参数为：目标编号，目标进度
        ///</summary>
        public const int    SPACE_ACTION_EVENT_CND_PROGRESS = 4;     

        ///<summary>
        ///通知玩家的下个路点，事件扩展参数：无
        ///</summary>
        public const int    SPACE_ACTION_EVENT_MOVE_POINT = 5;       

        ///<summary>
        ///通知某个玩家的积分，事件扩展参数：玩家编号，分数
        ///</summary>
        public const int    SPACE_ACTION_EVENT_PLAYER_SCORE = 6;     

        ///<summary>
        ///通知某个阵营的积分，事件扩展参数：阵营编号，分数
        ///</summary>
        public const int    SPACE_ACTION_EVENT_CAMP_SCORE = 7;       

        ///<summary>
        ///通知某个计数器当前的计数，事件扩展参数：当前计数
        ///</summary>
        public const int    SPACE_ACTION_EVENT_UPDATE_COUNT = 8;     

        ///<summary>
        ///通知副本行为任务是否达成
        ///</summary>
        public const int    SPACE_ACTION_EVENT_ACTION_TASK = 9;      

        ///<summary>
        ///-------------以下行为事件用于调试服务器本中的行为，以方便策划确定问题-------------------
        ///</summary>
    

        ///<summary>
        ///创建某个服务器行为，事件扩展参数：无
        ///</summary>
        public const int    SPACE_ACTION_EVENT_DEBUG_CREATE  = 30;   

        ///<summary>
        ///销毁某个服务器行为，事件扩展参数：无
        ///</summary>
        public const int    SPACE_ACTION_EVENT_DEBUG_DESTROY = 31;   

        ///<summary>
        ///触发某个服务器行为，事件扩展参数：无
        ///</summary>
        public const int    SPACE_ACTION_EVENT_DEBUG_TRIGGER = 32;   


        ///<summary>
        ///------------------宠物系统----------------------------------
        ///</summary>
    

        ///<summary>
        ///助战偏移10
        ///</summary>
        public const int    COMBAT_STATE_ASSIST_OFFSET = 10; 

        ///<summary>
        ///出战宠物最大数 不能大于COMBAT_STATE_ASSIST_OFFSET
        ///</summary>
        public const int    MAX_COMBAT_PET             = 6;  

        ///<summary>
        ///1 --不知道为什么默认最小为2呢,策划要求
        ///</summary>
        public const int    PET_INIT_QUALITY           = 2;  

        ///<summary>
        ///最大品质
        ///</summary>
        public const int    PET_MAX_QUALITY            = 8;  


        ///<summary>
        ///------------------奖励系统----------------------------------
        ///</summary>
    

        ///<summary>
        ///每日活跃任务状态
        ///</summary>
    

        ///<summary>
        ///任务已接受
        ///</summary>
        public const int    AWARD_ACTIVE_TASK_STATE_ACCEPTED           = 0;      

        ///<summary>
        ///任务已完成（跟下面的AWARD_TASK_IS_COMPLETE对应）
        ///</summary>
        public const int    AWARD_ACTIVE_TASK_STATE_COMPLETED          = -1;     


        ///<summary>
        ///奖励系统任务记录
        ///</summary>
    

        ///<summary>
        ///任务id
        ///</summary>
        public const int    AWARD_TASK_ID                   = 1;    

        ///<summary>
        ///活跃任务触发次数
        ///</summary>
        public const int    AWARD_TASK_TRIGGER_NUM          = 2;    

        ///<summary>
        ///是否已达成(可领取奖励的完成次数)
        ///</summary>
        public const int    AWARD_TASK_IS_COMPLETE          = 3;    

        ///<summary>
        ///活跃任务奖励标示,是否已领取(0：没有奖励了)
        ///</summary>
        public const int    AWARD_TASK_REWARD_FLAG          = 4;    


        ///<summary>
        ///活跃宝箱奖励
        ///</summary>
    

        ///<summary>
        ///vip补领奖励
        ///</summary>
        public const int    AWARD_ACTIVE_TASK_COMPENSATE_VIP_REWARD  = 1;    

        ///<summary>
        ///宝箱奖励等级
        ///</summary>
        public const int    AWARD_ACTIVE_TASK_BOX_REWARD_LEVEL       = 2;    

        ///<summary>
        ///每日签到奖励
        ///</summary>
    

        ///<summary>
        ///日期
        ///</summary>
        public const int    AWARD_DAILY_SIGN_IN_TIME                 = 1;     

        ///<summary>
        ///每日签到奖励
        ///</summary>
        public const int    AWARD_DAILY_SIGN_IN_REWARD               = 2;     

        ///<summary>
        ///每日签到vip奖励标示
        ///</summary>
        public const int    AWARD_DAILY_SIGN_IN_VIP_REWARD           = 3;     

        ///<summary>
        ///月签到id
        ///</summary>
        public const int    AWARD_DAILY_SIGN_ID                      = 4;     


        ///<summary>
        ///------------------副本玩法:恶魔之门-----------------------
        ///</summary>
    

        ///<summary>
        ///世界等级要求,后续改到global_setting全局配置中
        ///</summary>
        public const int    EVIL_MIN_WORLD_LEVEL                    = 20; 


        ///<summary>
        ///日清除
        ///</summary>
        public const int    EVIL_INFO_KEY_CUR_SCORE                 = 2; 

        ///<summary>
        ///日清除
        ///</summary>
        public const int    EVIL_INFO_KEY_MAX_SCORE                 = 3; 

        ///<summary>
        ///周清除
        ///</summary>
        public const int    EVIL_INFO_KEY_WEEK_SCORE                = 4; 

        ///<summary>
        ///周记录最后的时间
        ///</summary>
        public const int    EVIL_INFO_KEY_WEEK_TIME                 = 5; 



        ///<summary>
        ///-------------------活动编号定义-----------------------
        ///</summary>
    

        ///<summary>
        ///恶魔之门
        ///</summary>
        public const int    ACTIVITY_ID_EVIL_MISSION  = 1;    

        ///<summary>
        ///宝箱活动
        ///</summary>
        public const int    ACTIVITY_ID_CHEST         = 18;   

        ///<summary>
        ///异界宝箱-决斗场宝箱
        ///</summary>
        public const int    ACTIVITY_ID_DUELCHEST     = 25;   


        ///<summary>
        ///-------------------活动功能编号定义-----------------------
        ///</summary>
    

        ///<summary>
        ///宝箱活动
        ///</summary>
        public const int    ACTIVITY_FUNC_CHEST         = 35;   

        ///<summary>
        ///异界宝箱-决斗场宝箱
        ///</summary>
        public const int    ACTIVITY_FUNC_DUELCHEST     = 44;   

        ///<summary>
        ///开服七天活动
        ///</summary>
        public const int    ACTIVITY_FUNC_CREATE        = 1202;   


        ///<summary>
        ///-------------------宝箱类活动子分类-参考异界宝箱.xls-----------------------
        ///</summary>
     

        ///<summary>
        ///异界宝箱
        ///</summary>
        public const int    ACTIVITY_TYPE_CHEST = 1; 

        ///<summary>
        ///异界宝箱-决斗场宝箱
        ///</summary>
        public const int    ACTIVITY_TYPE_DUELCHEST = 2; 


        ///<summary>
        ///-------------------好友邀请--------------------------
        ///</summary>
    

        ///<summary>
        ///创建账号时，用的邀请码
        ///</summary>
        public const int    FRIEND_INVITE_USED_INVITE_CODE         = 1;  

        ///<summary>
        ///自身邀请码被使用的的信息以及当前的任务完成程度
        ///</summary>
        public const int    FRIEND_INVITE_INVITE_INFO              = 2;  


        ///<summary>
        ///触发次数
        ///</summary>
        public const int    FRIEND_INVITE_TRIGGER_NUM              = 1;  

        ///<summary>
        ///是否完成
        ///</summary>
        public const int    FRIEND_INVITE_IS_COMPLETE              = 2;  


        ///<summary>
        ///-------------------body_part部位 怪物部位-----------------------
        ///</summary>
    

        ///<summary>
        ///锁定状态
        ///</summary>
        public const int    BODY_PART_STATE_LOCK    = 0;       

        ///<summary>
        ///正常状态
        ///</summary>
        public const int    BODY_PART_STATE_NORMAL  = 1;       

        ///<summary>
        ///已经破坏状态。服务端给到客户端，客户端自己判断是否可恢复，并且读表，做相关显示。
        ///</summary>
        public const int    BODY_PART_STATE_DESTROY = 2;       

        ///<summary>
        ///恢复的另外一回事。暂时客户端用。服务端不用
        ///</summary>
    

        ///<summary>
        ///正在恢复状态。
        ///</summary>
        public const int    BODY_PART_STATE_RECOVER = 3;       

        public const int    BODY_PART_KEY_ID            = 1;
        public const int    BODY_PART_KEY_STATE         = 2;
        public const int    BODY_PART_KEY_HP            = 3;
        public const int    BODY_PART_KEY_MAX_HP        = 4;
        public const int    BODY_PART_KEY_DESTROY_TIME  = 5;


        ///<summary>
        ///部位解锁条件。怪物血量变化。
        ///</summary>
        public const int    BODY_PART_CONDITION_HP      = 1;  


        ///<summary>
        ///------------------掉落物品拾取模式--------------------------------
        ///</summary>
    

        ///<summary>
        ///代表仅施法者敌方可拾取
        ///</summary>
        public const int    PICKUP_ITEM_MODE_ENEMY = 1;  

        ///<summary>
        ///代表仅施法者友方可拾取
        ///</summary>
        public const int    PICKUP_ITEM_MODE_TEAMER = 2; 

        ///<summary>
        ///代表所有人可拾取
        ///</summary>
        public const int    PICKUP_ITEM_MODE_ALL = 3;    

        ///<summary>
        ///代表仅施法者可拾取
        ///</summary>
        public const int    PICKUP_ITEM_MODE_SELF = 4;   



        ///<summary>
        ///------------------次数定价表--------------------------------
        ///</summary>
    

        ///<summary>
        ///副本重置次数定价
        ///</summary>
        public const int    PRICE_ID_STORY_COPY_RESET   = 10;       

        ///<summary>
        ///复活次数
        ///</summary>
        public const int    PRICE_ID_REVIVE             = 15;       

        ///<summary>
        ///试炼秘镜价格表
        ///</summary>
        public const int    PRICE_ID_TRY_STORY          = 17;       

        ///<summary>
        ///公会战抽签
        ///</summary>
        public const int    PRICE_ID_GUILD_BATTLE_DRAW  = 18;       




        ///<summary>
        ///-------------------卡片管理器--------------------
        ///</summary>
    

        ///<summary>
        ///月卡
        ///</summary>
        public const int    CARD_TYPE_MONTH            = 1;        
        public const int    CARD_TYPE_SEASON           = 2;
        public const int    CARD_TYPE_YEAR             = 3;


        ///<summary>
        ///-------------------后台日志数据采集----------------
        ///</summary>
    

        ///<summary>
        ///代币商店
        ///</summary>
        public const int    SHOP_TYPE_TOKEN_SHOP       = 1;        

        ///<summary>
        ///钻石商店
        ///</summary>
        public const int    SHOP_TYPE_MARKET           = 2;        
    

        ///<summary>
        ///代币商店中的shop_id，和其他shop_id不同，这个要有全局限制
        ///</summary>
        public const int    SHOP_TYPE_TUTOR_SHOP       = 35;        



        ///<summary>
        ///-------------------禁言类型-------------------------
        ///</summary>
    
        public const int    NO_SPEAK_FOREVER           = -1;

        ///<summary>
        ///能够自由发言
        ///</summary>
        public const int    SPEAK_FREE                 = 0;  



        ///<summary>
        ///---------------------手机设备信息----------------------------
        ///</summary>
    

        ///<summary>
        ///游戏版本号
        ///</summary>
        public const int    MOBILE_GAME_VERSION        =   1;

        ///<summary>
        ///用户设备ID
        ///</summary>
        public const int    MOBILE_DID                 =   2;

        ///<summary>
        ///手机操作系统
        ///</summary>
        public const int    MOBILE_OS                  =   3;

        ///<summary>
        ///操作系统版本号
        ///</summary>
        public const int    MOBILE_OS_VERSION          =   4;

        ///<summary>
        ///设备名称,如：三星GT-S5830
        ///</summary>
        public const int    MOBILE_DEVICE              =   5;

        ///<summary>
        ///设备类型，如：xiaomi
        ///</summary>
        public const int    MOBILE_DEVICE_TYPE         =   6;

        ///<summary>
        ///屏幕分辨率
        ///</summary>
        public const int    MOBILE_SCREEN              =   7;

        ///<summary>
        ///移动网络运营商
        ///</summary>
        public const int    MOBILE_MNO                 =   8;

        ///<summary>
        ///联网方式
        ///</summary>
        public const int    MOBILE_NM                  =   9;


        ///<summary>
        ///平台请求超时
        ///</summary>
        public const int    AIYOU_API_TIME_OUT         = 120;   


        ///<summary>
        ///----------------------手机绑定--------------------------------
        ///</summary>
    

        ///<summary>
        ///绑定手机每日验证码发送次数
        ///</summary>
        public const int    BIND_MOBILE_DAILY_CODE_TIMES       = 10;    

        ///<summary>
        ///生成验证码时间间隔,秒
        ///</summary>
        public const int    BIND_MOBILE_CODE_INTERVAL          = 30;    

        ///<summary>
        ///两次绑定手机之间的间隔天数
        ///</summary>
        public const int    BIND_MOBILE_DAY_INTERVAL           = 30;    



        ///<summary>
        ///----------------------称号系统--------------------------------
        ///</summary>
    

        ///<summary>
        ///任务触发次数
        ///</summary>
        public const int    TITLE_TASK_TRIGGER_NUM          = 1;    

        ///<summary>
        ///是否已达成(可领取称号的完成次数,-1表示已完成,其他表示未完成)
        ///</summary>
        public const int    TITLE_TASK_IS_COMPLETE          = 2;    


        ///<summary>
        ///任务已完成（跟TITLE_TASK_IS_COMPLETE对应）
        ///</summary>
        public const int    TITLE_TASK_STATE_COMPLETED      = -1;    


        ///<summary>
        ///----------------------充值系统--------------------------------
        ///</summary>
    

        ///<summary>
        ///统计可加载的数据条数
        ///</summary>
        public const int    CHARGE_LOADING_COUNT            = 1;     

        ///<summary>
        ///加载有效数据条数
        ///</summary>
        public const int    CHARGE_LOADING_DATA             = 2;     

        ///<summary>
        ///初始回调ID
        ///</summary>
        public const int    INIT_CHARGE_CALLBACK_ID         = 3;     

        ///<summary>
        ///每次加载数据条数
        ///</summary>
        public const int    LOADING_COUNT_PER_TIME          = 1000;  

        ///<summary>
        ///订单保存超时处理
        ///</summary>
        public const int    ORDER_SAVING_TIMEOUT            = 120;   


        ///<summary>
        ///初始未领取订单
        ///</summary>
        public const int    CHARGE_ORDER_UNCLAIMED          = 0;     

        ///<summary>
        ///订单已领取
        ///</summary>
        public const int    CHARGE_ORDER_HAVE_CALIMED       = 1;     


        ///<summary>
        ///充值fd
        ///</summary>
        public const int    CHARGE_ORDER_SOCKET_FD          = 1;     

        ///<summary>
        ///订单信息
        ///</summary>
        public const int    CHARGE_ORDER_SAVE_INFO          = 2;     

        ///<summary>
        ///timerId
        ///</summary>
        public const int    CHARGE_ORDER_TIMER_ID           = 3;     


        ///<summary>
        ///DB操作类型
        ///</summary>
        public const int    CHARGE_OPTION_TYPE              = 1;     

        ///<summary>
        ///DB擦做timer
        ///</summary>
        public const int    CHARGE_OPTION_TIMER             = 2;     

        ///<summary>
        ///Avatar mb
        ///</summary>
        public const int    CHARGE_OPTION_MAILBOX           = 3;     

        ///<summary>
        ///操作回调
        ///</summary>
        public const int    CHARGE_OPTION_CALLBACK          = 4;     

        ///<summary>
        ///订单信息
        ///</summary>
        public const int    CHARGE_OPTION_INFO              = 5;     

        ///<summary>
        ///订单的dbid
        ///</summary>
        public const int    CHARGE_OPTION_ORDER_DBID        = 6;     

        ///<summary>
        ///请求action id
        ///</summary>
        public const int    CHARGE_OPTION_ACTION            = 7;     


        ///<summary>
        ///记录更新操作
        ///</summary>
        public const int    CHARGE_UPDATE_ORDER             = 1;     

        ///<summary>
        ///记录删除操作
        ///</summary>
        public const int    CHARGE_DELETE_ORDER             = 2;     


        ///<summary>
        ///TIMER保存订单超时类型
        ///</summary>
        public const int    ORDER_SAVING_TIME_OUT_USERDATA  = 1;     

        ///<summary>
        ///TIMER移除订单超时类型
        ///</summary>
        public const int    ORDER_DELETE_TIME_OUT_USERDATA  = 2;     

        ///<summary>
        ///TIMER更新订单超时类型
        ///</summary>
        public const int    ORDER_UPDATE_TIME_OUT_USERDATA  = 3;     


        ///<summary>
        ///购买次数
        ///</summary>
        public const int    CHARGE_RECORD_TIMES             = 1;     

        ///<summary>
        ///过期时间
        ///</summary>
        public const int    CHARGE_RECORD_TIME              = 2;     


        ///<summary>
        ///普通好友
        ///</summary>
        public const int    CHARGE_PRESENT_FRIEND           = 1;     

        ///<summary>
        ///公会好友
        ///</summary>
        public const int    CHARGE_PRESENT_GUILD            = 2;     


        ///<summary>
        ///    GUILD_BATTLE_STAGE_NOT          = 0,     --不在公会战阶段
        ///</summary>


        ///<summary>
        ///    GUILD_BATTLE_STAGE_EXP          = 1,     --公会战试炼阶段
        ///</summary>


        ///<summary>
        ///    GUILD_BATTLE_STAGE_DRAW         = 2,     --公会战抽签阶段
        ///</summary>


        ///<summary>
        ///    GUILD_BATTLE_STAGE_GROUP_MATCH  = 3,     --公会战小组赛阶段
        ///</summary>


        ///<summary>
        ///    GUILD_BATTLE_STAGE_FINAL_MATCH  = 4,     --公会战淘汰赛阶段
        ///</summary>


        ///<summary>
        ///    GUILD_BATTLE_STAGE_OTHER        = 5,     --公会战其他阶段
        ///</summary>



        ///<summary>
        ///无公会战状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_NONE           = 0;     

        ///<summary>
        ///正在试炼赛状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_TESTING        = 1;     

        ///<summary>
        ///未开始抽签的状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_TO_DRAW        = 2;     

        ///<summary>
        ///正在抽签的状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_DRAWING        = 3;     

        ///<summary>
        ///未开始小组赛状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_TO_GROUP_MATCH = 4;     

        ///<summary>
        ///正在小组赛状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_GROUP_MATCHING = 5;     

        ///<summary>
        ///未开始淘汰赛状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_TO_FINAL_MATCH = 6;     

        ///<summary>
        ///正在淘汰赛状态
        ///</summary>
        public const int    GUILD_BATTLE_STATE_FINAL_MATCHING = 7;     


        ///<summary>
        ///公会小组赛A组
        ///</summary>
        public const int    GUILD_BATTLE_MATCH_A_GROUP        = 1;     

        ///<summary>
        ///公会小组赛B组
        ///</summary>
        public const int    GUILD_BATTLE_MATCH_B_GROUP        = 2;     

        ///<summary>
        ///公会小组赛C组
        ///</summary>
        public const int    GUILD_BATTLE_MATCH_C_GROUP        = 3;     

        ///<summary>
        ///公会小组赛D组
        ///</summary>
        public const int    GUILD_BATTLE_MATCH_D_GROUP        = 4;     


        ///<summary>
        ///获取公会战王者组的战况
        ///</summary>
        public const int    GUILD_BATTLE_FINAL_MATCH          = 1;     

        ///<summary>
        ///获取公会战精英组的战况
        ///</summary>
        public const int    GUILD_BATTLE_FINAL_CREAM_MATCH    = 2;     


        ///<summary>
        ///公会战开始到第一轮公会匹配的阶段
        ///</summary>
        public const int    GUILD_BATTLE_COUNTDOWN_STATE1     = 1;     

        ///<summary>
        ///公会匹配后到第一轮成员匹配的阶段
        ///</summary>
        public const int    GUILD_BATTLE_COUNTDOWN_STATE2     = 2;     

        ///<summary>
        ///成员匹配中
        ///</summary>
        public const int    GUILD_BATTLE_COUNTDOWN_STATE3     = 3;     

        ///<summary>
        ///成员匹配后到下一轮公会匹配
        ///</summary>
        public const int    GUILD_BATTLE_COUNTDOWN_STATE4     = 4;     


        ///<summary>
        ///赛制1
        ///</summary>
        public const int    GUILD_BATTLE_FORMAT_1             = 1;     

        ///<summary>
        ///赛制2
        ///</summary>
        public const int    GUILD_BATTLE_FORMAT_2             = 2;     

        ///<summary>
        ///赛制3
        ///</summary>
        public const int    GUILD_BATTLE_FORMAT_3             = 3;     


        ///<summary>
        ///队长分配奖励timer
        ///</summary>
        public const int    CAPTAIN_DISTRIBUTION_TIMER        = 1;     

        ///<summary>
        ///CAPTAIN_ITEM_APPLY                = 1,     --道具申请
        ///</summary>
    

        ///<summary>
        ///CAPTAIN_ITEM_DISTRIBUTION         = 2,     --道具分配
        ///</summary>
    


        ///<summary>
        ///想要
        ///</summary>
        public const int    CAPTAIN_ITEM_APPLY_WANT           = 1;     

        ///<summary>
        ///放弃
        ///</summary>
        public const int    CAPTAIN_ITEM_APPLY_GIVEUP         = 2;     


        ///<summary>
        ///队长控制分配
        ///</summary>
        public const int    CAPTAIN_MODE_MASTER               = 1;     

        ///<summary>
        ///骰子随机模式
        ///</summary>
        public const int    CPATAIN_MODE_DICE                 = 2;     


        ///<summary>
        ///等待分配
        ///</summary>
        public const int    CAPTAIN_GAIN_WAIT                 = 1;     

        ///<summary>
        ///想要获取
        ///</summary>
        public const int    CAPTAIN_GAIN_WANT                 = 2;     

        ///<summary>
        ///主动放弃
        ///</summary>
        public const int    CAPTAIN_GAIN_GIVEUP               = 3;     

        ///<summary>
        ///获取失败
        ///</summary>
        public const int    CAPTAIN_GAIN_FAILURE              = 4;     

        ///<summary>
        ///成功获得
        ///</summary>
        public const int    CAPTAIN_GAIN_SUCCESS              = 5;     


        ///<summary>
        ///dbid
        ///</summary>
        public const int    CAPTAIN_DIST_ROLE_DBID            = 1;     

        ///<summary>
        ///获取状态
        ///</summary>
        public const int    CAPTAIN_DIST_GAIN_STATUS          = 2;     

        ///<summary>
        ///等候时间
        ///</summary>
        public const int    CAPTAIN_DIST_WAIT_TIME            = 3;     

        ///<summary>
        ///轮次ID
        ///</summary>
        public const int    CAPTAIN_DIST_TURN_ID              = 4;     

        ///<summary>
        ///道具ID
        ///</summary>
        public const int    CAPTAIN_DIST_ITEM_ID              = 5;     

        ///<summary>
        ///道具数量
        ///</summary>
        public const int    CAPTAIN_DIST_ITEM_NUM             = 6;     

        ///<summary>
        ///分配模式
        ///</summary>
        public const int    CAPTAIN_DIST_MODE_ID              = 7;     

        ///<summary>
        ///DICE随机值
        ///</summary>
        public const int    CAPTAIN_DIST_RAND_ID              = 8;     

        ///<summary>
        ///道具随机属性
        ///</summary>
        public const int    CAPTAIN_DIST_ITEM_INFO            = 9;     

        ///<summary>
        ///角色等级
        ///</summary>
        public const int    CAPTAIN_DIST_ROLE_LEVEL           = 10;    

        ///<summary>
        ///角色职业
        ///</summary>
        public const int    CAPTAIN_DIST_ROLE_VOC             = 11;    

        ///<summary>
        ///装备战斗力
        ///</summary>
        public const int    CAPTAIN_DIST_FIGHT_FORCE          = 12;    


        ///<summary>
        ///骑士长
        ///</summary>
        public const int    GUILD_MILITARY_KNIGHT_LEADER     = 1;    

        ///<summary>
        ///骑士
        ///</summary>
        public const int    GUILD_MILITARY_KNIGHT            = 2;    

        ///<summary>
        ///士兵
        ///</summary>
        public const int    GUILD_MILITARY_SOLDIER           = 3;    

        ///<summary>
        ///没打过的玩家的军衔，用于取奖励的时候使用
        ///</summary>
        public const int    GUILD_MILITARY_NOBODY            = 4;    


        ///<summary>
        ///公会战场景标识
        ///</summary>
    

        ///<summary>
        ///公会场景
        ///</summary>
        public const int    GUILD_MATCH_MISSION_LAND        = 1;    

        ///<summary>
        ///公会候选场景
        ///</summary>
        public const int    GUILD_MATCH_MISSION_CANDIDATE   = 2;    

        ///<summary>
        ///公会boss场景
        ///</summary>
        public const int    GUILD_BOSS_MISSION              = 3;    


        ///<summary>
        ///公会战类型
        ///</summary>
    

        ///<summary>
        ///公会战小组赛
        ///</summary>
        public const int    GUILD_MATCH                     = 1;    

        ///<summary>
        ///公会战淘汰赛王者组
        ///</summary>
        public const int    GUILD_MATCH_FINAL_MATCH         = 2;    

        ///<summary>
        ///公会战淘汰赛精英组
        ///</summary>
        public const int    GUILD_MATCH_FINAL_CREAM_MATCH   = 3;    

        ///<summary>
        ///公会战友谊赛
        ///</summary>
        public const int    GUILD_MATCH_FRIENDLY_MATCH      = 4;    



        ///<summary>
        ///争夺传送门个人信息
        ///</summary>
    

        ///<summary>
        ///角色名称
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_NAME      = 1;  

        ///<summary>
        ///角色等级
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_LEVEL     = 2;  

        ///<summary>
        ///角色战力
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_FIGTH     = 3;  

        ///<summary>
        ///角色积分
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_SCORE     = 4;  

        ///<summary>
        ///角色胜利标识
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_WIN       = 5;  

        ///<summary>
        ///角色战斗排名
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_RANK      = 6;  

        ///<summary>
        ///角色奖励ID
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_REWARD    = 7;  

        ///<summary>
        ///角色DBID
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_DBID      = 8;  

        ///<summary>
        ///角色实体ID
        ///</summary>
        public const int    FIGHT_TRANS_PORT_ROLE_EID       = 9;  

        ///<summary>
        ///------------------------------------------------------------
        ///</summary>
    


        ///<summary>
        ///世界Boss出生
        ///</summary>
        public const int    WORLD_BOSS_APPEAR               = 1;    

        ///<summary>
        ///世界Boss死亡
        ///</summary>
        public const int    WORLD_BOSS_DISAPPEAR            = 2;    


        ///<summary>
        ///制造系统最大天赋
        ///</summary>
        public const int    MFG_MAX_TALENT                  = 3;    



        ///<summary>
        ///公会小组赛pk场景
        ///</summary>
        public const int    MAP_ID_GUILD_PK                 = 90107;  

        ///<summary>
        ///公会淘汰赛pk场景
        ///</summary>
        public const int    MAP_ID_GUILD_FINAL              = 90108;  

        ///<summary>
        ///公会友谊赛pk场景
        ///</summary>
        public const int    MAP_ID_GUILD_FRIEND             = 90109;  


        ///<summary>
        ///---------------------------時空裂隙---------------------------------
        ///</summary>
    

        ///<summary>
        ///最大次數内的收益類型
        ///</summary>
        public const int    SPACETIME_RIFT_REWARD_COMMON    = 1;      

        ///<summary>
        ///超出範圍的收益類型
        ///</summary>
        public const int    SPACETIME_RIFT_REWARD_CEIL      = 2;      


        ///<summary>
        ///当前任务的ID
        ///</summary>
        public const int    SPACETIME_RIFT_TASK_ID          = 1;      

        ///<summary>
        ///当前任务的状态
        ///</summary>
        public const int    SPACETIME_RIFT_TASK_STATUS      = 2;      

        ///<summary>
        ///当前任务关卡ID
        ///</summary>
        public const int    SPACETIME_RIFT_MISSION_ID       = 3;      

        ///<summary>
        ///队长的DBID
        ///</summary>
        public const int    SPACETIME_RIFT_CAPTAIN_DBID     = 4;      

        ///<summary>
        ///任务随机地点坐标
        ///</summary>
        public const int    SPACETIME_RIFT_RANDOM_POSI      = 5;      

        ///<summary>
        ///队伍最低等级
        ///</summary>
        public const int    SPACETIME_RIFT_LOWEST_LEVEL     = 6;      

        ///<summary>
        ///当前任务环数
        ///</summary>
        public const int    SPACETIME_RIFT_CURRENT_RING     = 7;      


        ///<summary>
        ///当前任务信息{任务id,任务状态}
        ///</summary>
        public const int    SPACETIME_RIFT_TASK_INFO        = 1;      

        ///<summary>
        ///先前任务记录进度
        ///</summary>
        public const int    SPACETIME_RIFT_PRE_DAY_RECORD   = 2;      

        ///<summary>
        ///周完成任务次数
        ///</summary>
        public const int    SPACETIME_RIFT_WEEK_TIMES       = 3;      

        ///<summary>
        ///离线记录{dbid=num}
        ///</summary>
        public const int    SPACETIME_RIFT_OFFLINE_RECORD   = 4;      

        ///<summary>
        ///今日任务总收益{id=num}
        ///</summary>
        public const int    SPACETIME_RIFT_DAY_REWARDS      = 5;      

        ///<summary>
        ///更新时间
        ///</summary>
        public const int    SPACETIME_RIFT_UPDATE_TIME      = 6;      

        ///<summary>
        ///周奖励领取记录{id,id,id,...}
        ///</summary>
        public const int    SPACETIME_RIFT_WEEK_REWARDS     = 7;      

        ///<summary>
        ///天任务次数
        ///</summary>
        public const int    SPACETIME_RIFT_DAY_TIMES        = 8;      

        ///<summary>
        ///队长随机任务临时记录
        ///</summary>
        public const int    SPACETIME_RIFT_RANDOM_TASK      = 9;      

        ///<summary>
        ///离线奖励记录
        ///</summary>
        public const int    SPACETIME_RIFT_OFFLINE_REWARDS  = 10;     

        ///<summary>
        ///队长环数记录
        ///</summary>
        public const int    SPACETIME_RIFT_CAPTAIN_RING     = 11;     


        ///<summary>
        ///队长随机任务ID
        ///</summary>
        public const int    SPACETIME_RIFT_TMP_TASK_ID      = 1;      

        ///<summary>
        ///队长随机任务关卡ID
        ///</summary>
        public const int    SPACETIME_RIFT_TMP_MISSION_ID   = 2;      


        ///<summary>
        ///角色离线未在时空裂隙中
        ///</summary>
        public const int    SPACETIME_OFFLINE_NO_RIFT       = 0;      

        ///<summary>
        ///角色离线在时空裂隙中
        ///</summary>
        public const int    SPACETIME_OFFLINE_IN_RIFT       = 1;      


        ///<summary>
        ///------------------------------------------------------------
        ///</summary>
    


        ///<summary>
        ///pk日志最大数
        ///</summary>
        public const int    MAX_PK_LOG      = 100;  

        ///<summary>
        ///导师系统最大记录日志
        ///</summary>
        public const int    TUTOR_MAX_LOG   = 40;   


        ///<summary>
        ///交易系统的推送
        ///</summary>
        public const int    SETTING_TYPE_PUSH_AUCTION    = 0;    

        ///<summary>
        ///幻境探险系统的推送
        ///</summary>
        public const int    SETTING_TYPE_PUSH_DREAM_LAND = 1;    
    

        ///<summary>
        ///开服活动  各活动id宏 
        ///</summary>
    

        ///<summary>
        ///创角活动(七天)
        ///</summary>
        public const int    ACTIVITY_OPEN_CREATE_ACTIVITY         = 1;   


        ///<summary>
        ///充值活动
        ///</summary>
        public const int    ACTIVITY_CHARGE_TYPE   = 1;      

        ///<summary>
        ///活动下架
        ///</summary>
        public const int    ACTIVITY_CHARGE_CANCEL = 1;      


        ///<summary>
        ///红包活动，类型：1是等级红包，2是公会红包，3是节日红包
        ///</summary>
    
        public const int    RED_ENVELOPE_LEVEL              = 1;
        public const int    RED_ENVELOPE_GUILD              = 2;
        public const int    RED_ENVELOPE_FESTIVAL           = 3;
    

        ///<summary>
        ///导师系统
        ///</summary>
    

        ///<summary>
        ///组队秘境
        ///</summary>
        public const int    TUTOR_EVENT_TEAM_MISSION         = 1;   

        ///<summary>
        ///组队打野外世界boss
        ///</summary>
        public const int    TUTOR_EVENT_KILL_WORLD_BOSS      = 2;   

        ///<summary>
        ///组队在野外共同时间
        ///</summary>
        public const int    TUTOR_EVENT_WILD_TIME            = 3;   

        ///<summary>
        ///组队野外共同杀怪
        ///</summary>
        public const int    TUTOR_EVENT_WILD_KILL_MONSTER    = 4;   

        ///<summary>
        ///组队支线副本
        ///</summary>
        public const int    TUTOR_EVENT_TEAM_BRANCHE_MISSION = 5;   

        ///<summary>
        ///组队野外任务
        ///</summary>
        public const int    TUTOR_EVENT_TEAM_WILD_MISSION    = 6;   



        ///<summary>
        ///野外寻宝排行榜
        ///</summary>
    

        ///<summary>
        ///世界boss
        ///</summary>
        public const int    WILD_TREASURE_WORLD_BOSS        = 1;  

        ///<summary>
        ///宝图boss
        ///</summary>
        public const int    WILD_TREASURE_PICTURE_BOSS      = 2;  

        ///<summary>
        ///传送门
        ///</summary>
        public const int    WILD_TREASURE_TRANSPORT         = 3;  

        ///<summary>
        ///藏宝图
        ///</summary>
        public const int    WILD_TREASURE_MAP               = 4;  

        ///<summary>
        ///异界宝箱
        ///</summary>
        public const int    WILD_TREASURE_OUTLAND_BOX       = 5;  


        ///<summary>
        ///野外寻宝积分
        ///</summary>
        public const int    WILD_TREASURE_SCORE_ID          = 1;  

        ///<summary>
        ///野外寻宝重置时间
        ///</summary>
        public const int    WILD_TREASURE_WEEK_TIME         = 2;  


        ///<summary>
        ///普通模式
        ///</summary>
        public const int    MISSION_MODE_COMMON             = 1;  

        ///<summary>
        ///困难模式
        ///</summary>
        public const int    MISSION_MODE_DIFFICULT          = 2;  

        ///<summary>
        ///炼狱模式
        ///</summary>
        public const int    MISSION_MODE_PURGATORY          = 3;  

        ///<summary>
        ///恶魔模式
        ///</summary>
        public const int    MISSION_MODE_EVIL               = 4;  


        ///<summary>
        ///统计输出
        ///</summary>
        public const int    BATTLE_STATISTIC_OUTPUT         = 1;  

        ///<summary>
        ///统计承受
        ///</summary>
        public const int    BATTLE_STATISTIC_BEAR           = 2;  


        ///<summary>
        ///已登记未满18周岁
        ///</summary>
        public const int    ANTI_ADDICTION_NOT_AGREE        = 1;  

        ///<summary>
        ///已登记已满18周岁
        ///</summary>
        public const int    ANTI_ADDICTION_HAS_AGREE        = 2;  

        ///<summary>
        ///未登记信息
        ///</summary>
        public const int    ANTI_ADDICTION_NOT_INFO         = 3;  


        ///<summary>
        ///公会任务进度
        ///</summary>
    

        ///<summary>
        ///没接
        ///</summary>
        public const int    GTS_NONE_ACCEPT                 = 1;  

        ///<summary>
        ///进行中
        ///</summary>
        public const int    GTS_ACCEPT                      = 2;  

        ///<summary>
        ///已完成
        ///</summary>
        public const int    GTS_ACCOMPLISH                  = 3;  


        ///<summary>
        ///    GUILD_TASK_REFRESH_TIME         = 12, --公会任务刷新时间
        ///</summary>

    

        ///<summary>
        ///玩家最大等级，注意修改
        ///</summary>
        public const int    AVATAR_MAX_LEVEL                = 70; 

        ///<summary>
        ///自动匹配。野外任务的game_id
        ///</summary>
        public const int    AUTO_TEAM_GAME_ID_WILD_TASK     = 200; 


        ///<summary>
        ///第一档最小天数
        ///</summary>
        public const int    OLD_RETURN_FIRST_MIN            = 7;   

        ///<summary>
        ///第一档最大天数
        ///</summary>
        public const int    OLD_RETURN_FIRST_MAX            = 13;  

        ///<summary>
        ///第二档最小天数
        ///</summary>
        public const int    OLD_RETURN_SECOND_MIN           = 14;  

        ///<summary>
        ///第二档最大天数
        ///</summary>
        public const int    OLD_RETURN_SECOND_MAX           = 30;  

        ///<summary>
        ///第三档最小天数
        ///</summary>
        public const int    OLD_RETURN_THIRD_MIN            = 30;  


        ///<summary>
        ///第一档
        ///</summary>
        public const int    OLD_RETURN_FIRST_IDX            = 1;   

        ///<summary>
        ///第二档
        ///</summary>
        public const int    OLD_RETURN_SECOND_IDX           = 2;   

        ///<summary>
        ///第三档
        ///</summary>
        public const int    OLD_RETURN_THIRD_IDX            = 3;   


        ///<summary>
        ///回归开始时间
        ///</summary>
        public const int    OLD_RETURN_START_TIME           = 1;   

        ///<summary>
        ///回归奖励下次领取天数
        ///</summary>
        public const int    OLD_RETURN_REWARD_DAY           = 2;   

        ///<summary>
        ///回归奖励重置时等级
        ///</summary>
        public const int    OLD_RETURN_RESET_LEVEL          = 3;   

        ///<summary>
        ///回归奖励离线类型
        ///</summary>
        public const int    OLD_RETURN_VALID_TYPE           = 4;   

        ///<summary>
        ///回归奖励今日领取状态
        ///</summary>
        public const int    OLD_RETURN_TODAY_FLAG           = 5;   


        ///<summary>
        ///已经领取奖励
        ///</summary>
        public const int    OLD_RETURN_REWAED_OK            = 1;   

        ///<summary>
        ///没有领取奖励
        ///</summary>
        public const int    OLD_RETURN_REWAED_NO            = 0;   

        ///<summary>
        ///不能领取奖励
        ///</summary>
        public const int    OLD_RETURN_REWAED_CANNOT        = 2;   


        ///<summary>
        ///激活老玩家回归状态
        ///</summary>
        public const int    OLD_RETURN_STATUS_YES           = 1;   

        ///<summary>
        ///清除老玩家回归状态
        ///</summary>
        public const int    OLD_RETURN_STATUS_NONE          = 0;   


        ///<summary>
        ///玩家装备战力
        ///</summary>
        public const int    FF_PLAYER_EQUIP_FIGHT           = 1;   

        ///<summary>
        ///玩家附魔战力
        ///</summary>
        public const int    FF_PLAYER_ENCHANT_FIGHT         = 2;   

        ///<summary>
        ///玩家强化战力
        ///</summary>
        public const int    FF_PLAYER_STRENGTHEN_FIGHT      = 3;   

        ///<summary>
        ///玩家套装战力
        ///</summary>
        public const int    FF_PLAYER_SUIT_FIGHT            = 4;   
}



        ///<summary>
        ///--------------------------------------------------------------------------------------------------
        ///</summary>


}


