﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
using ACTSystem;
using Common.Structs;
namespace Common.Data
{
    public class AvatarEquipInfo
    {
        public int equipID;
        public int flow;
        public int particle;
        //id方便用来实现同步
        private int _colorRID = 0;
        private int _colorGID = 0;
        private int _colorBID = 0;
        //string方便用来实现本地颜色设置
        private string _colorR = "";
        private string _colorG = "";
        private string _colorB = "";

        public int colorRID
        {
            get { return _colorRID; }
            set 
            {
                _colorRID = value;
                _colorR = color_helper.GetColorString(value);
            }
        }

        public int colorGID
        {
            get { return _colorGID; }
            set
            {
                _colorGID = value;
                _colorG = color_helper.GetColorString(value);
            }
        }

        public int colorBID
        {
            get { return _colorBID; }
            set
            {
                _colorBID = value;
                _colorB = color_helper.GetColorString(value);
            }
        }

        public string colorR
        {
            get { return _colorR; }
        }

        public string colorG
        {
            get { return _colorG; }
        }

        public string colorB
        {
            get { return _colorB; }
        }

        public void ClearData()
        {
            equipID = 0;
            flow = 0;
            particle = 0;
            _colorRID = 0;
            _colorGID = 0;
            _colorBID = 0;
            _colorR = "";
            _colorG = "";
            _colorB = "";
        }

    }

    public class AvatarModelData
    {
        public int vocation;
        public Dictionary<ACTEquipmentType, AvatarEquipInfo> equips;

        public AvatarModelData()
        {
            equips = new Dictionary<ACTEquipmentType, AvatarEquipInfo>();
            equips[ACTEquipmentType.Cloth] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Weapon] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Wing] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Head] = new AvatarEquipInfo();
            equips[ACTEquipmentType.Effect] = new AvatarEquipInfo();
        }

        public AvatarEquipInfo GetEquipInfo(ACTEquipmentType type)
        {
            return equips[type];
        }

        public void ClearData() 
        {
            equips[ACTEquipmentType.Cloth].ClearData();
            equips[ACTEquipmentType.Weapon].ClearData();
            equips[ACTEquipmentType.Wing].ClearData();
            equips[ACTEquipmentType.Head].ClearData();
            equips[ACTEquipmentType.Effect].ClearData();
        }
    }
}
