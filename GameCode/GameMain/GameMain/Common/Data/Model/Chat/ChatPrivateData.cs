﻿#region 模块信息
/*==========================================
// 文件名：ChatPrivateData
// 命名空间: Common.Data.Chat
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/10 14:00:20
// 修改时间：2015/8/18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
//using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Global;
using MogoEngine.Utils;
using GameMain;

namespace Common.Data
{
    public class ChatPrivateData
    {
        private const int MAX_CHAT_INFO_COUNT = 30;

        //保存最近密聊信息
        private List<PbChatInfoResp> _recentChatInfoList = new List<PbChatInfoResp>();

        private ChatData _chatData;

        private bool _canSavePrivateChat = false;
        public bool CanSavePrivateChat
        {
            get { return _canSavePrivateChat; }
            set { _canSavePrivateChat = value; }
        }

        public List<PbChatInfoResp> RecentChatInfoList
        {
            get { return _recentChatInfoList; }
        }

        public ChatPrivateData(ChatData chatData)
        {
            _chatData = chatData;
            InitPrivateChatPlayerList();
            AddPrivateChatInfoToChatData();
        }

        private void InitPrivateChatPlayerList()
        {
            LoadPrivateChatInfo(_recentChatInfoList);
        }

        private void AddPrivateChatInfoToChatData()
        {
            for (int i = 0; i < _recentChatInfoList.Count; i++)
            {
                _chatData.AddChatInfo(_recentChatInfoList[i], false);
            }
        }

        public void SavePrivateChatInfo()
        {
            if (_canSavePrivateChat)
            {
                if (_recentChatInfoList.Count > 0)
                {
                    SavePrivateChatInfo(_recentChatInfoList);
                }
                _canSavePrivateChat = false;
            }
        }

        public void AddToPrivateChatData(PbChatInfoResp chatInfo)
        {
            _recentChatInfoList.Add(chatInfo);
            while (_recentChatInfoList.Count > MAX_CHAT_INFO_COUNT)
            {
                _recentChatInfoList.RemoveAt(0);
            }
            _canSavePrivateChat = true;
        }

        #region 保存、读取最近密聊数据

        //保存最近密聊的信息
        private void SavePrivateChatInfo(List<PbChatInfoResp> recentChatInfoList)
        {
            LoggerHelper.Info("[ChatPrivateData]  SavePrivateChatInfo");
            PrivateChatCache chatCache = new PrivateChatCache();
            chatCache.chatInfoList = new List<PrivateChatInfo>();
            for (int i = 0; i < recentChatInfoList.Count; i++)
            {
                PrivateChatInfo chatInfo = CreatePrivateChatInfoCache(recentChatInfoList[i]);
                chatCache.chatInfoList.Add(chatInfo);
            }
            if (chatCache.chatInfoList.Count > 0)
            {
                LocalCache.Write(LocalName.PrivateChat.ToString(), chatCache, CacheLevel.Permanent);
            }
        }

        //读取最近密聊的信息
        private void LoadPrivateChatInfo(List<PbChatInfoResp> recentChatInfoList)
        {
            PrivateChatCache chatCache = LocalCache.Read<PrivateChatCache>(LocalName.PrivateChat.ToString());
            if (chatCache == null || chatCache.chatInfoList.Count == 0)
            {
                return;
            }
            recentChatInfoList.Clear();
            for (int i = 0; i < chatCache.chatInfoList.Count; i++)
            {
                PrivateChatInfo chatInfo = chatCache.chatInfoList[i];
                recentChatInfoList.Add(CreatePbChatInfo(chatInfo));
            }
        }

        private PrivateChatInfo CreatePrivateChatInfoCache(PbChatInfoResp srcChatInfo)
        {
            PrivateChatInfo chatInfo = new PrivateChatInfo();
            chatInfo.acceptDbid = srcChatInfo.accept_dbid.ToString();
            chatInfo.acceptLevel = srcChatInfo.accept_level;
            chatInfo.acceptName = srcChatInfo.accept_name;
            chatInfo.acceptVocation = srcChatInfo.accept_vocation;
            chatInfo.channelId = srcChatInfo.channel_id;
            chatInfo.msg = srcChatInfo.msg;
            chatInfo.msgType = srcChatInfo.msg_type;
            chatInfo.sendDbid = srcChatInfo.send_dbid.ToString();
            chatInfo.sendLevel = srcChatInfo.send_level;
            chatInfo.sendName = srcChatInfo.send_name;
            chatInfo.sendVocation = srcChatInfo.send_vocation;
            chatInfo.voiceId = srcChatInfo.voice_id;
            //chatInfo.linkId = srcChatInfo.link_ids;
            if (srcChatInfo.chat_item_list.Count > 0)
            {
                for (int i = 0; i < srcChatInfo.chat_item_list.Count; i++)
                {
                    PbChatItem chatItem = new PbChatItem();
                    PbChatItem srcChatItem = srcChatInfo.chat_item_list[i];
                    //要用bytes转为string，原因是可能uuid在之前并没有从bytes进行转换
                    //chatItem.uuid = MogoProtoUtils.ParseByteArrToString(srcChatItem.uuid_bytes);
                    chatInfo.chatItemList.Add(chatItem);
                }
            }
            return chatInfo;
        }

        private PbChatInfoResp CreatePbChatInfo(PrivateChatInfo srcChatInfo)
        {
            PbChatInfoResp chatInfo = new PbChatInfoResp();
            chatInfo.accept_dbid = ulong.Parse(srcChatInfo.acceptDbid);
            chatInfo.accept_level = srcChatInfo.acceptLevel;
            chatInfo.accept_name = srcChatInfo.acceptName;
            chatInfo.accept_vocation = srcChatInfo.acceptVocation;
            chatInfo.channel_id = srcChatInfo.channelId;
            chatInfo.msg = srcChatInfo.msg;
            chatInfo.msg_type = srcChatInfo.msgType;
            chatInfo.send_dbid = ulong.Parse(srcChatInfo.sendDbid);
            chatInfo.send_level = srcChatInfo.sendLevel;
            chatInfo.send_name = srcChatInfo.sendName;
            chatInfo.send_vocation = srcChatInfo.sendVocation;
            chatInfo.voice_id = srcChatInfo.voiceId;
            //chatInfo.link_id = srcChatInfo.linkId;
            if (srcChatInfo.chatItemList.Count > 0)
            {
                for (int i = 0; i < srcChatInfo.chatItemList.Count; i++)
                {
                    PbChatItem chatItem = new PbChatItem();
                    PbChatItem srcChatItem = srcChatInfo.chatItemList[i];
                    //chatItem.uuid_bytes = MogoProtoUtils.ParseStringToByteArr(srcChatItem.uuid);
                    chatInfo.chat_item_list.Add(chatItem);
                }
            }
            return chatInfo;
        }
        #endregion
    }

    public class PrivateChatCache
    {
        public List<PrivateChatInfo> chatInfoList;
    }

    public class PrivateChatInfo
    {
        public string acceptDbid;
        public uint acceptLevel;
        public string acceptName;
        public uint acceptVocation;
        public uint channelId;
        public string msg;
        public uint msgType;
        public string sendDbid;
        public uint sendLevel;
        public string sendName;
        public uint sendVocation;
        public string voiceId;
        public uint linkId;
        public List<PbChatItem> chatItemList = new List<PbChatItem>();
    }
}