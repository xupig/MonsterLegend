﻿
using System;
using System.Collections.Generic;

using Common.ServerConfig;
using GameLoader.Utils;

using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils.Timer;
using System.Text.RegularExpressions;
using GameMain;

namespace Common.Data
{
    public class ChatData
    {
        public List<PbChatAvatarInfo> BlackList { get; private set; }

        private HashSet<ulong> _blackDict;
        public ChatPrivateData ChatPrivateData { get; private set; }        //密聊相关数据
        public ChatPhraseData ChatPhraseData { get; private set; }        //聊天元素相关数据，常用语等
        public ChatShowData ChatShowData { get; private set; }

        private FilterWordsData _filterWordsData;
        public FilterWordsData FilterWordsInfo
        {
            get { return _filterWordsData; }
        }

        public TeamEntryChatData TeamEntryChatData { get; private set; }
        public ChatQuestionData ChatQuestionData { get; private set; }
        public ChatLinkData ChatLinkData { get; private set; }

        private Dictionary<int, List<NewMsgFlagInfo>> _hasNewMsgDict;
        private Dictionary<string, int> _chatVoiceDurationDict;
        private Dictionary<string, bool> _hasPlayedVoiceDict;
        private Queue<PbChatInfoResp> _autoPlayVoiceQueue;
        private string _curVoiceBtnName = "";
        public string CurVoiceBtnName
        {
            get { return _curVoiceBtnName; }
            set { _curVoiceBtnName = value; }
        }
        private bool _isPlayingVoice = false;
        public bool IsPlayingVoice
        {
            get { return _isPlayingVoice; }
            set { _isPlayingVoice = value; }
        }

        private bool _isCanReqRankGuildList = false;
        private uint _getGuildListTimerId = 0;
        private const int START_TIME_RESET_GET_GUILD_LIST_TAG = 600000;
        public const int MAX_SYSTEM_MSG_COUNT = 3;

        public static Regex VOICE_DURATION_PATTERN = new Regex(@"(?<=\,)\d{1,5}(?=\])");
        public const string VOICE_TEMPLETE = "[{0},{1},{2}]";  //0:语音内容 1:链接类型  2:语音的时长
        private const string GUILD_LINK_TEMPLATE = "[{0},{1},{2}]";
        private const string TEAM_LINK_TEMPLATE = "[{0},{1},{2},{3},{4},{5},{6}]";
        private const string SHARE_POS_LINK_TEMPLATE = "[{0},{1},{2},{3},{4},{5},{6},{7}]";
        private const string VIEW_LINK_TEMPLATE = "[{0},{1},{2}]";
        private const string RED_ENVELOPE_LINK_TEMPLATE = "[{0},{1},{2},{3}]";
        private const string DONATE_LINK_TEMPLATE = "[{0},{1},{2},{3}]";
        private const string WANT_DONATE_LINK_TEMPLATE = "[{0},{1},{2},{3}]";
        private const string TEAM_CAPTAIN_ACCEPT_INVITE_TEMPLATE = "[{0},{1},{2},{3}]";
        private static Regex VIEW_LINK_PATTERN = new Regex(@"((\[[^0-9]+\])|(【[^0-9]+】))(^(,\d+))");
        private const string DUEL_LINK_TEMPLATE = "[{0},{1},{2},{3}]";

        private const string CONTENT_LINK_TEMPLATE = "[{0},{1}]";
        public const string CHAT_CONTENT_WITH_LINK_DATA_TEMPLATE = "{0}|{1}";
        public static readonly char[] CHAT_CONTENT_AND_LINK_SPLIT_TAG = new char[] { '|' };

        /// <summary>
        /// 各个频道聊天信息
        /// Key为聊天频道Id
        /// Value为服务器返回聊天信息
        /// </summary>
        private Dictionary<int, List<PbChatInfoResp>> _chatInfoDict;

        private static uint _curChatId = 0;

        public ChatData()
        {
            _chatInfoDict = new Dictionary<int, List<PbChatInfoResp>>();
            _chatInfoDict.Add(public_config.CHANNEL_ID_ALL, new List<PbChatInfoResp>());
            _chatInfoDict.Add(public_config.CHANNEL_ID_GUILD, new List<PbChatInfoResp>());
            _chatInfoDict.Add(public_config.CHANNEL_ID_PRIVATE, new List<PbChatInfoResp>());
            _chatInfoDict.Add(public_config.CHANNEL_ID_TEAM, new List<PbChatInfoResp>());
            _chatInfoDict.Add(public_config.CHANNEL_ID_VICINITY, new List<PbChatInfoResp>());//附近频道
            _chatInfoDict.Add(public_config.CHANNEL_ID_WORLD, new List<PbChatInfoResp>());
            //提示频道消息，根据pb里的msgType==CHAT_MSG_TYPE_TIPS来确定
            _chatInfoDict.Add(public_config.CHANNEL_ID_HELP, new List<PbChatInfoResp>());
            //系统频道消息，根据IsSystemChannelMsg来确定
            _chatInfoDict.Add(public_config.CHANNEL_ID_SYSTEM, new List<PbChatInfoResp>());
            BlackList = new List<PbChatAvatarInfo>();
            ChatPrivateData = new ChatPrivateData(this);
            ChatPhraseData = new ChatPhraseData();
            ChatShowData = new ChatShowData();
            _blackDict = new HashSet<ulong>();
            _filterWordsData = FilterWordsData.Instance;
            TeamEntryChatData = new TeamEntryChatData();
            ChatQuestionData = new ChatQuestionData();
            ChatLinkData = new ChatLinkData(this);
            _hasNewMsgDict = new Dictionary<int, List<NewMsgFlagInfo>>();
            _chatVoiceDurationDict = new Dictionary<string, int>();
            _hasPlayedVoiceDict = new Dictionary<string, bool>();
            _autoPlayVoiceQueue = new Queue<PbChatInfoResp>();
            InitHasNewMsgDict();
            InitVoiceInfosFromPrivateData();
        }

        private void InitHasNewMsgDict()
        {
            _hasNewMsgDict.Add(public_config.CHANNEL_ID_TEAM, new List<NewMsgFlagInfo>());
            _hasNewMsgDict.Add(public_config.CHANNEL_ID_GUILD, new List<NewMsgFlagInfo>());
            _hasNewMsgDict.Add(public_config.CHANNEL_ID_PRIVATE, new List<NewMsgFlagInfo>());
            NewMsgFlagInfo info = new NewMsgFlagInfo();
            info.playerDbid = 0;
            info.newFlag = false;
            _hasNewMsgDict[public_config.CHANNEL_ID_TEAM].Add(info);
            info = new NewMsgFlagInfo();
            info.playerDbid = 0;
            info.newFlag = false;
            _hasNewMsgDict[public_config.CHANNEL_ID_GUILD].Add(info);
            info = new NewMsgFlagInfo();
            info.playerDbid = 0;
            info.newFlag = false;
            _hasNewMsgDict[public_config.CHANNEL_ID_PRIVATE].Add(info);
        }

        private void InitVoiceInfosFromPrivateData()
        {
            for (int i = 0; i < ChatPrivateData.RecentChatInfoList.Count; i++)
            {
                PbChatInfoResp chatInfo = ChatPrivateData.RecentChatInfoList[i];
                if (chatInfo.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
                {
                    AddChatVoiceDurationInfo(chatInfo);
                    AddPlayedVoiceInfo(chatInfo);
                }
            }
        }

        public void AddBlackList(List<PbChatAvatarInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                BlackList.Add(list[i]);
                if (_blackDict.Contains(list[i].dbid) == false)
                {
                    _blackDict.Add(list[i].dbid);
                }
            }
        }

        public void RemoveBlackList(List<PbChatAvatarInfo> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                RemoveItem(list[i].dbid);
                _blackDict.Remove(list[i].dbid);
            }
        }

        private void RemoveItem(UInt64 dbId)
        {
            for (int i = 0; i < BlackList.Count; i++)
            {
                if (BlackList[i].dbid == dbId)
                {
                    BlackList.RemoveAt(i);
                    return;
                }
            }
        }

        public bool isShield(UInt64 dbId)
        {
            return _blackDict.Contains(dbId);
        }

        public void AddChatInfo(PbChatInfoResp chatInfo, bool isAddToChannelAll)
        {
            int channel = GetChannelId(chatInfo);
            DoBeforeAddRedEnvelopeMsg(chatInfo, isAddToChannelAll);
            if (isAddToChannelAll)
            {
                DoBeforeAddSystemMsg(chatInfo, public_config.CHANNEL_ID_ALL);
                _chatInfoDict[(int)public_config.CHANNEL_ID_ALL].Add(chatInfo);
            }

            DoBeforeAddSystemMsg(chatInfo, channel);
            _chatInfoDict[channel].Add(chatInfo);

            List<PbChatInfoResp> allChannelList = _chatInfoDict[public_config.CHANNEL_ID_ALL];
            if (allChannelList.Count > ChatConst.MAX_CHAT_ITEM_COUNT)
            {
                PbChatInfoResp oldChatInfo = allChannelList[0];
                allChannelList.RemoveAt(0);
                ChatLinkData.ProcessRemoveChatLinkInfo(public_config.CHANNEL_ID_ALL, oldChatInfo);
            }
            if (channel != public_config.CHANNEL_ID_PRIVATE)
            {
                List<PbChatInfoResp> channelList = null;
                int channelId = channel;
                if (chatInfo.msg_type == (int)ChatContentType.TipsMsg)
                {
                    channelList = _chatInfoDict[public_config.CHANNEL_ID_HELP];
                    channelId = public_config.CHANNEL_ID_HELP;
                }
                else if (IsSystemChannelMsg(chatInfo))
                {
                    channelList = _chatInfoDict[public_config.CHANNEL_ID_SYSTEM];
                    channelId = public_config.CHANNEL_ID_SYSTEM;
                }
                else
                {
                    channelList = _chatInfoDict[channel];
                }
                if (channelList.Count > ChatConst.MAX_CHAT_ITEM_COUNT)
                {
                    PbChatInfoResp oldChatInfo = channelList[0];
                    channelList.RemoveAt(0);
                    ChatLinkData.ProcessRemoveChatLinkInfo(channelId, oldChatInfo);
                }
            }
        }

        private void DoBeforeAddSystemMsg(PbChatInfoResp addChatInfo, int addChannel)
        {
            if (!IsLimitCountTypeMsg(addChatInfo, addChannel))
                return;
            //处理当综合频道或公会频道聊天消息中已经存在3条以上的系统消息的情况
            List<PbChatInfoResp> channelList = _chatInfoDict[addChannel];
            List<PbChatInfoResp> systemMsgList = new List<PbChatInfoResp>();
            for (int i = 0; i < channelList.Count; i++)
            {
                if (IsLimitCountTypeMsg(channelList[i], addChannel))
                {
                    systemMsgList.Add(channelList[i]);
                }
            }
            if (systemMsgList.Count >= MAX_SYSTEM_MSG_COUNT)
            {
                while (systemMsgList.Count > MAX_SYSTEM_MSG_COUNT - 1)
                {
                    channelList.Remove(systemMsgList[0]);
                    systemMsgList.RemoveAt(0);
                }
            }
        }

        private void DoBeforeAddRedEnvelopeMsg(PbChatInfoResp addChatInfo, bool isAddToChannelAll)
        {
            if (addChatInfo.msg_type != (uint)ChatContentType.RedEnvelope)
                return;
            //查找综合和要加到列表里的addChatInfo中对应的频道数据中是否存在红包，如果存在，就删除旧的红包
            if (isAddToChannelAll)
            {
                List<PbChatInfoResp> allChannelList = _chatInfoDict[public_config.CHANNEL_ID_ALL];
                for (int i = 0; i < allChannelList.Count; i++)
                {
                    if (allChannelList[i].msg_type == (uint)ChatContentType.RedEnvelope)
                    {
                        allChannelList.RemoveAt(i);
                        break;
                    }
                }
            }

            int channel = GetChannelId(addChatInfo);
            List<PbChatInfoResp> channelList = _chatInfoDict[channel];
            for (int i = 0; i < channelList.Count; i++)
            {
                if (channelList[i].msg_type == (uint)ChatContentType.RedEnvelope)
                {
                    channelList.RemoveAt(i);
                    break;
                }
            }
        }

        public int GetChannelId(PbChatInfoResp chatInfo)
        {
            int channelId = (int)chatInfo.channel_id;
            if (chatInfo.msg_type == (int)ChatContentType.TipsMsg)
            {
                channelId = public_config.CHANNEL_ID_HELP;
            }
            else if (IsSystemChannelMsg(chatInfo))
            {
                channelId = public_config.CHANNEL_ID_SYSTEM;
            }
            return channelId;
        }

        //是否是限制了显示数量的消息，比如在综合频道只能显示最多3条该类消息
        public bool IsLimitCountTypeMsg(PbChatInfoResp chatInfo, int addChannel)
        {
            bool isSystemMsg = false;
            if (addChannel == public_config.CHANNEL_ID_ALL)
            {
                isSystemMsg = IsSystemChannelMsg(chatInfo);
            }
            else if (addChannel == public_config.CHANNEL_ID_GUILD)
            {
                isSystemMsg = IsGuildSystemMsg(chatInfo);
            }
            return isSystemMsg;
        }

        public uint CreateChatId()
        {
            //从服务器返回的PbChatInfo中，seq_id为ushort类型
            if (_curChatId >= ushort.MaxValue - 1)
            {
                _curChatId = 0;
            }
            _curChatId++;
            return _curChatId;
        }

        public void AddChatVoiceDurationInfo(PbChatInfoResp chatInfo)
        {
            if (string.IsNullOrEmpty(chatInfo.voice_id))
                return;
            int duration = 0;
            int.TryParse(VOICE_DURATION_PATTERN.Match(chatInfo.msg).Value, out duration);
            if (!_chatVoiceDurationDict.ContainsKey(chatInfo.voice_id))
            {
                _chatVoiceDurationDict.Add(chatInfo.voice_id, duration);
            }
        }

        public void AddPlayedVoiceInfo(PbChatInfoResp chatInfo)
        {
            if (string.IsNullOrEmpty(chatInfo.voice_id) || chatInfo.send_dbid == GameMain.EntityPlayer.Player.id)
                return;
            if (!_hasPlayedVoiceDict.ContainsKey(chatInfo.voice_id))
            {
                _hasPlayedVoiceDict.Add(chatInfo.voice_id, false);
            }
        }

        public void SetVoiceHasPlayed(string voiceId)
        {
            if (_hasPlayedVoiceDict.ContainsKey(voiceId))
            {
                _hasPlayedVoiceDict[voiceId] = true;
            }
        }

        public void AddAutoPlayVoice(PbChatInfoResp chatInfo)
        {
            StartAutoPlayVoice();
            if (chatInfo.send_dbid == GameMain.EntityPlayer.Player.id)
                return;
            /*SettingData settingData = PlayerDataManager.Instance.SettingData;
            ChatSpeechSettingType speechType = GetSpeechType((int)chatInfo.channel_id);
            if (settingData.IsChatSpeechTypeSelected(speechType))
            {
                if (!IsVoiceHasPlayed(chatInfo.voice_id))
                {
                    _autoPlayVoiceQueue.Enqueue(chatInfo);
                    StartAutoPlayVoice();
                }
            }*/
        }

        /// <summary>
        /// BY LHS  语音类型
        /// </summary>
        public enum ChatSpeechSettingType
        {
            Guild = 1,
            Team = 2,
            Near = 4,
            World = 8
        }

        private ChatSpeechSettingType GetSpeechType(int channelId)
        {
            switch (channelId)
            {
                case public_config.CHANNEL_ID_WORLD:
                    return ChatSpeechSettingType.World;
                case public_config.CHANNEL_ID_VICINITY:
                    return ChatSpeechSettingType.Near;
                case public_config.CHANNEL_ID_TEAM:
                    return ChatSpeechSettingType.Team;
                case public_config.CHANNEL_ID_GUILD:
                    return ChatSpeechSettingType.Guild;
                default:
                    return ChatSpeechSettingType.World;
            }
        }

        public void StartAutoPlayVoice()
        {
            if (!_isPlayingVoice && _autoPlayVoiceQueue.Count > 0)
            {
                PbChatInfoResp chatInfo = _autoPlayVoiceQueue.Dequeue();
                EventDispatcher.TriggerEvent<string, ulong>(ChatEvents.PLAY_VOICE, chatInfo.voice_id, chatInfo.send_dbid);
            }
        }

        public void ProcessAddNewMsg(int channelId, ulong playerDbid)
        {
            if (channelId == public_config.CHANNEL_ID_TEAM || channelId == public_config.CHANNEL_ID_GUILD || channelId == public_config.CHANNEL_ID_PRIVATE)
            {
                _hasNewMsgDict[channelId][0].newFlag = true;
            }
        }

        public void ProcessReadNewMsg(int channelId, ulong playerDbid)
        {
            if (channelId == public_config.CHANNEL_ID_TEAM || channelId == public_config.CHANNEL_ID_GUILD || channelId == public_config.CHANNEL_ID_PRIVATE)
            {
                _hasNewMsgDict[channelId][0].newFlag = false;
                EventDispatcher.TriggerEvent<int>(ChatEvents.ReadContent, channelId);
            }
        }

        public bool CheckChannelHasNewMsg(int channelId)
        {
            bool result = false;
            if (channelId == public_config.CHANNEL_ID_TEAM || channelId == public_config.CHANNEL_ID_GUILD || channelId == public_config.CHANNEL_ID_PRIVATE)
            {
                result = _hasNewMsgDict[channelId][0].newFlag;
            }
            return result;
        }

        public bool CheckPrivateChatPlayerHasNewMsg(ulong playerDbid)
        {
            bool result = false;
            List<NewMsgFlagInfo> newMsgList = _hasNewMsgDict[public_config.CHANNEL_ID_PRIVATE];
            for (int i = 0; i < newMsgList.Count; i++)
            {
                if (newMsgList[i].playerDbid == playerDbid)
                {
                    result = newMsgList[i].newFlag;
                    break;
                }
            }
            return result;
        }

        //public List<PbChatInfoResp> GetChannelChatInfoList(int channel)
        //{
        //    return _chatInfoDict[channel];
        //}

        /// <summary>
        /// 获得最新消息
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public List<PbChatInfoResp> GetNewChatInfoList(int channel)
        {
            return _chatInfoDict[channel];
        }

        public List<PbChatInfoResp> GetAndClearRecentChatInfoList(int channel)
        {
            List<PbChatInfoResp> channelChatInfoList = _chatInfoDict[channel];
            int startIndex = 0;
            int count = channelChatInfoList.Count;
            if (channelChatInfoList.Count > ChatConst.MAX_CHAT_ITEM_COUNT)
            {
                startIndex = channelChatInfoList.Count - ChatConst.MAX_CHAT_ITEM_COUNT;
                count = ChatConst.MAX_CHAT_ITEM_COUNT;
            }
            List<PbChatInfoResp> recentChatInfoList = channelChatInfoList.GetRange(startIndex, count);
            channelChatInfoList.Clear();
            return recentChatInfoList;
        }

        public List<PbChatInfoResp> PopNewChatInfoList(int channel, int num)
        {
            List<PbChatInfoResp> channelChatInfoList = _chatInfoDict[channel];
            int count = Math.Min(num, channelChatInfoList.Count);
            List<PbChatInfoResp> popChatInfoList = channelChatInfoList.GetRange(0, count);
            channelChatInfoList.RemoveRange(0, count);
            return popChatInfoList;
        }

        public int GetChatVoiceDuration(string voiceId)
        {
            int duration = 0;
            if (_chatVoiceDurationDict.ContainsKey(voiceId))
            {
                duration = _chatVoiceDurationDict[voiceId];
            }
            return duration;
        }

        public bool IsVoiceHasPlayed(string voiceId)
        {
            if (!_hasPlayedVoiceDict.ContainsKey(voiceId))
            {
                LoggerHelper.Error("IsVoiceHasPlayed not exist this voice id = " + voiceId);
                return false;
            }
            return _hasPlayedVoiceDict[voiceId];
        }

        public bool CanShowTeamChatContent()
        {
            return IsPlayerInTeam();
        }

        public bool IsPlayerInTeam()
        {
            GameLoader.Utils.LoggerHelper.Error("判断是否在队伍中。。。 现在都是返回false");
            //判断是否在队伍中
            //if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0 || PlayerDataManager.Instance.TeamInstanceData.HpMemberDic.Count > 0)
            //{
            //    return true;
            //}
            return false;
        }

        public void ProcessNewMsgFlagNotInTeam()
        {
            if (IsPlayerInTeam() == false && CheckChannelHasNewMsg(public_config.CHANNEL_ID_TEAM) == true)
            {
                ProcessReadNewMsg(public_config.CHANNEL_ID_TEAM, 0);
            }
        }

        public void ProcessNewMsgFlagNotInGuild()
        {
            GameLoader.Utils.LoggerHelper.Error("判断公会ID。。。 现在都是返回false");
            //if (PlayerAvatar.Player.guild_id == 0 && CheckChannelHasNewMsg(public_config.CHANNEL_ID_GUILD) == true)
            //{
                ProcessReadNewMsg(public_config.CHANNEL_ID_GUILD, 0);
            //}
        }

        public bool IsSystemChannelMsg(PbChatInfoResp chatInfo)
        {
            if (chatInfo.msg_type == (uint)ChatContentType.Guild || chatInfo.msg_type == (uint)ChatContentType.Team || chatInfo.msg_type == (uint)ChatContentType.SharePosition ||
                chatInfo.msg_type == (uint)ChatContentType.SystemMsg)
            {
                return true;
            }
            return false;
        }

        public bool IsGuildSystemMsg(PbChatInfoResp chatInfo)
        {
            return chatInfo.channel_id == public_config.CHANNEL_ID_GUILD && GameMain.Test.TestChat.isSystem(chatInfo);
        }

        public string CreateLinkContent(ChatLinkBaseWrapper link)
        {
            string content = string.Format(CONTENT_LINK_TEMPLATE, link.LinkDesc, ((int)link.LinkType).ToString());
            return content;
        }

        public string CreateLinkMsg(ChatLinkBaseWrapper link)
        {
            string content = CreateLinkContent(link);
            content = CreateLinkMsg(content, link, true);
            return content;
        }

        public string CreateLinkMsg(string srcMsg, ChatLinkBaseWrapper link, bool withDataString)
        {
            return CreateLinkMsg(srcMsg, link, false, withDataString);
        }

        public string CreateLinkMsg(string srcMsg, ChatLinkBaseWrapper link, bool withSendTime, bool withDataString)
        {
            string content = string.Empty;
            if (!string.IsNullOrEmpty(srcMsg))
            {
                content = srcMsg;
            }
            if (withSendTime)
            {
                //加时间戳记录发送出去之后，过了多长时间
                content += string.Format(ChatContentUtils.LINK_SEND_TIME_FORMAT, Global.Global.serverTimeStamp / 1000);
            }
            if (link != null && withDataString)
            {
                content = string.Format(CHAT_CONTENT_WITH_LINK_DATA_TEMPLATE, content, link.ToDataString());
            }
            return content;
        }

        public string CreateLinkMsg(string srcMsg, ChatLinkBaseWrapper[] links, bool withDataString)
        {
            if (links == null || links.Length == 0)
                return srcMsg;
            string content = srcMsg;
            if (withDataString)
            {
                string linksDataString = string.Empty;
                for (int i = 0; i < links.Length; i++)
                {
                    ChatLinkBaseWrapper link = links[i];
                    linksDataString += link.ToDataString();
                }
                content = string.Format(CHAT_CONTENT_WITH_LINK_DATA_TEMPLATE, content, linksDataString);
            }
            return content;
        }

        public string CreateGuildLinkMsg(ChatGuildLinkWrapper link)
        {
            string msg = CreateLinkContent(link);
            return CreateLinkMsg(msg, link, true, true);
        }

        public string CreateTeamLinkMsg(ChatTeamLinkWrapper link)
        {
            string msg = CreateLinkContent(link);
            return CreateLinkMsg(msg, link, true, true);
        }

        public string CreateSharePosLinkMsg(ChatSharePosLinkWrapper link)
        {
            string msg = CreateLinkContent(link);
            return CreateLinkMsg(msg, link, true, true);
        }

        public string CreateDonateLinkMsg(ChatDonateLinkWrapper link)
        {
            return string.Format(DONATE_LINK_TEMPLATE, link.desc, (int)ChatLinkType.Donate, link.playerDbid, link.id);
        }

        public string CreateWantDonateLinkMsg(ChatWantDonateLinkWrapper link)
        {
            return string.Format(WANT_DONATE_LINK_TEMPLATE, link.desc, (int)ChatLinkType.WantDonate, link.time, link.id);
        }

        public string CreateViewLinkMsg(ChatViewLinkWrapper link)
        {
            string msg = CreateLinkContent(link);
            return CreateLinkMsg(msg, link, true, true);
        }

        public string CreateTeamCaptainAcceptInviteTemplate(ChatTeamCaptainAcceptInviteWrapper link)
        {
            string desc = string.Format(ChatConst.ACCEPT_PLAYER_JOIN_TEAM, link.ApplicantName, link.ApplicantLevel);
            return string.Format(TEAM_CAPTAIN_ACCEPT_INVITE_TEMPLATE, desc, (int)ChatLinkType.TeamCaptainAccept, link.TeamId, link.ApplicationDbid);
        }

        public string CreateDuelTemplate(string content, ChatDuelLinkWrapper link)
        {
            return string.Format(DUEL_LINK_TEMPLATE, content + link.Name, (int)ChatLinkType.Duel, link.Name.Length, link.MatchId);
        }

        public void DoGetRankGuildList()
        {
            /*if (_isCanReqRankGuildList)
                return;
            _isCanReqRankGuildList = true;
            RankingListManager.Instance.RequestTenMinutesRankListData((int)RankType.guildRank);
            if (_getGuildListTimerId == 0)
            {
                _getGuildListTimerId = TimerHeap.AddTimer(START_TIME_RESET_GET_GUILD_LIST_TAG, 0, ResetGetGuildListTag);
            }*/
        }

        private void ResetGetGuildListTag()
        {
            if (_isCanReqRankGuildList)
            {
                _isCanReqRankGuildList = false;
            }
            if (_getGuildListTimerId != 0)
            {
                TimerHeap.DelTimer(_getGuildListTimerId);
                _getGuildListTimerId = 0;
            }
        }
    }

    public class NewMsgFlagInfo
    {
        public ulong playerDbid;
        public bool newFlag;
    }

    public enum ChatContentType
    {
        Text = 1,
        Item,
        Voice,
        Guild,
        Team,
        SharePosition,
        TipsMsg,
        View,
        SystemMsg,
        RedEnvelope,
        Question,
        Donate,
        TeamCaptainAccept,
        WantDonate,
        Duel,
    }

    public enum ChatLinkType
    {
        Item = 1,
        Voice,
        Guild,
        Team,
        SharePosition,
        View,
        RedEnvelope,
        TradeMarket,
        Donate,
        TeamCaptainAccept,
        WantDonate,
        Duel,

        //100以后的为动态文本，本身没有点击功能，根据时间等条件刷新文本内容
        QuestionValidTime = 100,
    }
}