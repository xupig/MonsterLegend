﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class PbChatInfoResp
    {
        public ulong accept_dbid;
        public uint accept_level;
        public string accept_name;
        public uint accept_vocation;
        public uint channel_id;
        public string msg;
        public uint msg_type;
        public ulong send_dbid;
        public uint send_level;
        public string send_name;
        public uint send_vocation;
        public string voice_id;
        public uint linkId;
        public List<PbChatItem> chat_item_list = new List<PbChatItem>();

        public uint seq_id;

        public void SetTestData()
        {
            accept_dbid = 2323;
            accept_level = 3;
            accept_name = "接收者名字";
            accept_vocation = 4;
            channel_id = 3;
            msg = "你好，这是测试数据这是测试数据"+ 
                "这是测试数据这是测试数据这是测试" + 
                "数据这是测试数据这是测试数据" + 
                "这是测试数据这是测试数据这是测试数据";
            msg_type = 3;
            send_dbid = 329;
            send_level = 332;
            send_name = "发送者名字";
            send_vocation = 3;
            voice_id = "32938kd";
            linkId = 3;
            seq_id = 3;
    }
    }
}
