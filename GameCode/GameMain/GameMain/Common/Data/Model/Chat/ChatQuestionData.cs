﻿using System;
using System.Collections.Generic;
using GameData;
using Common.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;

namespace Common.Data
{
    public class ChatQuestionData
    {
        private ChatQuestionInfo _curQuestionInfo;
        private uint _questionValidTimerId = 0;

        public void UpdateQuestionInfo(int questionId)
        {
            ResetTimer();
            _curQuestionInfo = new ChatQuestionInfo(questionId);
            if (_questionValidTimerId == 0)
            {
                _questionValidTimerId = TimerHeap.AddTimer(1000, 1000, OnUpdateQuestionValid);
            }
            EventDispatcher.TriggerEvent<ChatQuestionInfo>(ChatEvents.START_CHAT_QUESTION, _curQuestionInfo);
        }

        private void OnUpdateQuestionValid()
        {
            if (_curQuestionInfo.EndTimeStamp <= Global.Global.serverTimeStamp / 1000)
            {
                ResetTimer();
                _curQuestionInfo = null;
                EventDispatcher.TriggerEvent(ChatEvents.END_CHAT_QUESTION);
            }
        }

        private void ResetTimer()
        {
            if (_questionValidTimerId != 0)
            {
                TimerHeap.DelTimer(_questionValidTimerId);
                _questionValidTimerId = 0;
            }
        }

        public ChatQuestionInfo CurQuestionInfo
        {
            get { return _curQuestionInfo; }
        }
    }

    public class ChatQuestionInfo
    {
        public int Id { get; private set; }
        private string _desc = string.Empty;
        private string _chatDesc = string.Empty;
        private int _time = 0;
        private uint _endTimeStamp = 0;
        //private static readonly string TIME_TEMPLATE = "（本次答题时间为{0}）";//为排除警告，注释掉。
        //private static readonly string CHAT_TIME_TEMPLATE = "[{0},{1},{2}]";

        public ChatQuestionInfo(int id)
        {
            Id = id;
            GameLoader.Utils.LoggerHelper.Error("答题");
            /*question_answer questionAnswerConfig = chat_question_helper.GetQuestionAnswerConfig(id);
            if (questionAnswerConfig != null)
            {
                _time = questionAnswerConfig.__time;
                _endTimeStamp = (uint)(Global.Global.serverTimeStamp / 1000) + (uint)_time;
            }
            question questionConfig = chat_question_helper.GetQuestionConfig(id);
            if (questionConfig != null)
            {
                _desc = questionConfig.__question;
                _desc = _desc.Replace("\\n", "\n");
                _chatDesc = _desc;
                string strTime = string.Empty;
                if (_time >= 60)
                {
                    string minute = (_time / 60).ToString() + ChatConst.STR_MINUTE;
                    strTime = string.Format(TIME_TEMPLATE, minute);
                }
                else
                {
                    string second = _time.ToString() + ChatConst.STR_SECOND;
                    strTime = string.Format(TIME_TEMPLATE, second);
                }
                _desc += strTime;

                _chatDesc += string.Format(CHAT_TIME_TEMPLATE, strTime, ((int)ChatLinkType.QuestionValidTime).ToString(), _endTimeStamp.ToString());
            }*/
        }

        public string Desc
        {
            get { return _desc; }
        }

        public string ChatDesc
        {
            get { return _chatDesc; }
        }

        public int Time
        {
            get { return _time; }
        }

        public uint EndTimeStamp
        {
            get { return _endTimeStamp; }
        }
    }
}
