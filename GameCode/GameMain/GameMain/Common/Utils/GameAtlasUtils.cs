﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LitJson;
using Common.Global;
using Object = UnityEngine.Object;
using UnityEngine.Events;
using GameData;
using GameLoader.Config;
using GameResource;
using UIExtension;
using GameLoader.Utils;
using GameMain;

namespace Common.Utils
{
    public class GameAtlasUtils
    {
        public static string ATLAS_JSON_PATH = "Atlas/Atlas.json";
        private static bool _isJsonStartLoad = false;
        private static Dictionary<string, List<string>> _spriteAtlasDict; //Key 为SpriteName, Value 为绑定Atlas的prefab的逻辑路径

        public static void Init()
        {
            if (_isJsonStartLoad == false)
            {
                LoadAtlasJson();
                _isJsonStartLoad = true;
            }
        }

        public static List<string> GetSpriteParams(string spriteName)
        {
            List<string> result = null;
            if (_spriteAtlasDict != null)
            {
                if (_spriteAtlasDict.ContainsKey(spriteName) == true)
                {
                    result = _spriteAtlasDict[spriteName];
                }
                else
                {
                    LoggerHelper.Error(string.Format("Key [{0}] not found in atlas JSON {1}", spriteName, ATLAS_JSON_PATH));
                }
            }
            return result;
        }

        public static void AddIcon(GameObject parent, int iconId, int colorId, bool raycastTarget, float gray = 1, bool resetContainer = false, bool autoSize = false, float scale = 1.0f)
        {
            AddSprite(parent, icon_item_helper.GetIcon(iconId), colorId, raycastTarget, gray, resetContainer, autoSize, scale);
        }

        private static void AddSprite(GameObject parent, string spriteName, int colorId, bool raycastTarget = false, float gray = 1, bool resetContainer = false, bool autoSize = false, float scale = 1.0f)
        {
            try
            {
                XIcon xIcon = parent.GetComponent<XIcon>();
                if (xIcon == null)
                {
                    xIcon = parent.AddComponent<XIcon>();
                }
                if (resetContainer) xIcon.Reset();
                xIcon.AutoSize(autoSize);
                Color color = Color.white;
                if (colorId > 0)
                {
                    color = ColorDefine.GetColorById(colorId);
                }
                xIcon.SetSprite(spriteName, color, raycastTarget, gray, scale);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("::" + ex.Message + ":" + ex.StackTrace);
            }
        }

        private static void LoadAtlasJson()
        {
            ObjectPool.Instance.GetTextAsset(ATLAS_JSON_PATH, OnAtlasJsonLoaded);
        }

        private static void OnAtlasJsonLoaded(TextAsset asset)
        {
            _spriteAtlasDict = JsonMapper.ToObject<Dictionary<string, List<string>>>(asset.text);
        }

        static Dictionary<string, string> _spriteNameKeyMap = new Dictionary<string, string>();
        static public string GetSpriteKeyByName(string spriteName, string spriteKeyTemplate)
        {
            if (_spriteNameKeyMap.ContainsKey(spriteName))
            {
                return _spriteNameKeyMap[spriteName];
            }
            string spriteKey = string.Format(spriteKeyTemplate, spriteName);
            _spriteNameKeyMap[spriteName] = spriteKey;
            return spriteKey;
        }
    }
}
