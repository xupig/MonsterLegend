﻿#region 模块信息
/*==========================================
// 文件名：MogoGameObjectHelper
// 命名空间: Common.Utils
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/30 19:24:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using GameMain.GlobalManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Common.Utils
{
    public class GameObjectHelper
    {
        public static T AddByTemplate<T>(GameObject template, Transform parent) where T : UIBehaviour
        {
            GameObject go = GameObject.Instantiate(template) as GameObject;
            T component = go.AddComponent<T>();
            SetGameObjectParam(go, parent);
            return component;
        }

        public static T AddByTemplate<T>(GameObject template) where T : UIBehaviour
        {
            GameObject go = GameObject.Instantiate(template) as GameObject;
            T component = go.AddComponent<T>();
            SetGameObjectParam(go, template.transform.parent);
            return component;
        }

        public static GameObject CloneGameObject(Transform transformTemplate, string path, Transform transformParent)
        {
            GameObject go = GameObject.Instantiate(transformTemplate.Find(path).gameObject) as GameObject;
            go.transform.SetParent(transformParent, false);
            go.transform.localScale = Vector3.one;
            return go;
        }

        public static void SetRectPosition(GameObject go, Vector2 position)
        {
            RectTransform trans = go.GetComponent<RectTransform>();
            trans.anchoredPosition = position;
        }

        public static Vector2 GetRectPosition(GameObject go)
        {
            RectTransform trans = go.GetComponent<RectTransform>();
            return trans.anchoredPosition;
        }

        public static void ChangeGameObjectLayer(GameObject go, string layer = "UI")
        {
            foreach (Transform trans in go.GetComponentsInChildren<Transform>())
            {
                trans.gameObject.layer = ACTSystemTools.NameToLayer(layer);
            }
        }

        public static void ChangeGameObjectAllLayer(GameObject go, int layer)
        {
            int childCount = go.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                ChangeGameObjectAllLayer(go.transform.GetChild(i).gameObject, layer);
            }
            go.layer = layer;
        }

        private static void SetGameObjectParam(GameObject go, Transform parent)
        {
            RectTransform trans = go.GetComponent<RectTransform>();
            if (trans == null)
            {
                trans = go.AddComponent<RectTransform>();
            }
            trans.SetParent(parent, false);
        }

        public static void SetLocalScale(GameObject go, Vector3 scale)
        {
            RectTransform rectTransform = go.GetComponent<RectTransform>();
            rectTransform.localScale = scale;
        }

        public static void SetPosition(GameObject go, Vector3 position)
        {
            RectTransform rectTransform = go.GetComponent<RectTransform>();
            rectTransform.localPosition = position;
        }

        public static void TransformToNodePosition(GameObject go, GameObject node, Vector3 position)
        {
            Vector3 worldPosition = node.transform.TransformPoint(position);
            Vector3 localPosition = go.transform.InverseTransformPoint(worldPosition);
            go.transform.localPosition = localPosition;
        }

        /*
        public static Vector3 ScreenPointToLocalPointInRectangle(RectTransform parent, Vector2 screenPosition)
        {
            Vector2 position = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parent, screenPosition, UIManager.Instance.UICamera, out position);
            return position;
        }*/

        public static bool CheckPositionIsInScreen(Vector2 screenPosition)
        {
            return CheckPositionIsInScreen(screenPosition, 0, 0, ScreenUtil.ScreenWidth, ScreenUtil.ScreenHeight);
        }

        public static bool CheckPositionIsInScreen(Vector2 screenPosition, float startPointX, float startPointY, float screenWidth, float screenHeight)
        {
            Rect screenRect = new Rect(startPointX, startPointY, screenWidth, screenHeight);
            return screenRect.Contains(screenPosition);
        }

        public static void SetZ(Transform transform, float z)
        {
            Vector3 position = transform.localPosition;
            transform.localPosition = new Vector3(position.x, position.y, z);
        }
    }
}
