﻿#region 模块信息
/*==========================================
// 文件名：MogoLanguageUtil
// 命名空间: Common.Utils
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/15 15:50:50
// 描述说明：通过配置得到对应语言文字
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.Global;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Utils
{
    public enum LanguageType
    { 
        Chinese,
    }

    public static class GameLanguageUtil
    {
        private static readonly LanguageType languageType = LanguageType.Chinese;

        public static string GetContent(List<string> strIdArr)
        {
            string result = "";
            for (int i = 0; i < strIdArr.Count; i++)
            {
                result = String.Concat(result, GameLanguageUtil.GetContent(strIdArr[i]));
            }
            return result;
        }

        public static string GetContent(string strId)
        {
            return GameLanguageUtil.GetContent(Convert.ToInt32(strId));
        }

        public static string GetContent(int id)
        {
            switch (languageType)
            {
                case LanguageType.Chinese:
                default:
                    if (GameData.XMLManager.chinese.ContainsKey(id))
                    {
                        if (!string.IsNullOrEmpty(GameData.XMLManager.chinese[id].__content))
                        {
                            return GameStringUtils.ConvertStyle(GameData.XMLManager.chinese[id].__content.Replace("\\n", "\n"));
                        }
                        return string.Empty;
                    }
                    else
                    {
                        return GameStringUtils.ConvertStyle(string.Format(GameData.XMLManager.chinese[-1].__content,id));
                    }
            }
        }

        public static string GetChineseNum(int num)
        {
            if (num < 1 || num > 10)
            {
                LoggerHelper.Error("中文配置表不存在数字:" + num);
            }
            return GetContent(num + 43);
        }

        public static string GetWeekday(int num)
        {
            if (num < 1 || num > 7)
            {
                LoggerHelper.Error("中文配置表不存在Weekday:" + num);
            }
            if (num == 7)
            {
                return "周日";
            }
            return string.Format("周{0}", GetContent(num + 43));
        }

        public static string GetContent(int id,params object[] param)
        {
            string result = GetContent(id);
            if (param!=null&&param.Length > 0)
            {
                result = string.Format(result, param);
            }
            return result;
        }

        public static string GetContent(string strId, params object[] paramss)
        {
            string result = GetContent(Convert.ToInt32(strId));
            result = string.Format(result, paramss);
            return result;
        }

        public static string GetSourceContent(int id)
        {
            switch (languageType)
            {
                case LanguageType.Chinese:
                default:
                    if (GameData.XMLManager.chinese.ContainsKey(id))
                    {
                        return GameData.XMLManager.chinese[id].__content.Replace("\\n", "\n");
                    }
                    else
                    {
                        return string.Format(GameData.XMLManager.chinese[-1].__content,id);
                    }
            }
        }

        /// <summary>
        /// 以中文字符为键值，返回相应语言的文字
        /// </summary>
        /// <param name="chineseContent"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GetString(LangEnum lang,params object[] param)
        {
            if(param.Length > 0)
            {
                return string.Format(GetContent((int)lang), param);
            }
            else
            {
                return GetContent((int)lang);
            }
        }

        public static string GetString(string convert, params object[] param)
        {
            return string.Format(GameStringUtils.ConvertStyle(convert), param);
        }

    }
}
