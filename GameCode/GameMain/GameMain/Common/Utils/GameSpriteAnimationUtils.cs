﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
using Object = UnityEngine.Object;
using UnityEngine.EventSystems;
using GameResource;
using UIExtension;
using GameLoader.Utils;
using GameData;
using GameMain;
using System;
using Common.Global;

namespace Common.Utils
{
    /// <summary>
    /// 用作表情动画
    /// </summary>
    public class GameSpriteAnimationUtils
    {
        private static string ATLAS_JSON_PATH = "Atlas/Atlas.json";
        private static string HOLDER_NAME = "Animation";
        private static bool _isJsonStartLoad = false;
        private static Dictionary<string, List<string>> _spriteAtlasDict; //Key 为SpriteName, Value 为绑定Atlas的prefab的逻辑路径
        private static Dictionary<GameObject, ParamsRecord> _paramsRecordDict = new Dictionary<GameObject, ParamsRecord>();
        private static Dictionary<GameObject, SpritePlayer> _spritePlasyerDict = new Dictionary<GameObject, SpritePlayer>();

        public static GameObject AddAnimation(GameObject parent, string[] spriteNameList, float width, float height)
        {
            if (_isJsonStartLoad == false)
            {
                LoadAtlasJson();
                _isJsonStartLoad = true;
            }
            if (string.IsNullOrEmpty(spriteNameList[0]) == false)
            {
                GameObject holder = AddHolder(parent, width, height);
                _paramsRecordDict.Add(holder, new ParamsRecord(holder, spriteNameList));
                LoadAtlasPrefab(holder);
                return holder;
            }
            return null;
        }

        public static void SetSpritePlayerAnimation(GameObject go, bool animation)
        {
            if (_spritePlasyerDict.ContainsKey(go))
            {
                _spritePlasyerDict[go]._isAnimation = animation;
            }
        }


        private static GameObject AddHolder(GameObject parent, float width, float height)
        {
            GameObject holder = new GameObject();
            holder.AddComponent<RectTransform>();
            holder.name = HOLDER_NAME;
            holder.transform.SetParent(parent.transform);
            RectTransform rect = holder.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, 0);
            rect.sizeDelta = new Vector2(width, height);
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.localPosition = Vector3.zero;
            rect.localScale = Vector3.one;
            return holder;
        }

        private static void LoadAtlasJson()
        {
            ObjectPool.Instance.GetTextAsset(ATLAS_JSON_PATH, OnAtlasJsonLoaded);
        }

        private static void OnAtlasJsonLoaded(TextAsset asset)
        {
            _spriteAtlasDict = JsonMapper.ToObject<Dictionary<string, List<string>>>(asset.text);
            foreach (KeyValuePair<GameObject, ParamsRecord> kvp in _paramsRecordDict)
            {
                LoadAtlasPrefab(kvp.Key);
            }
        }

        private static void LoadAtlasPrefab(GameObject holder)
        {
            if (_spriteAtlasDict != null)
            {
                string spriteName = _paramsRecordDict[holder].spriteNameList[0];
                if (_spriteAtlasDict.ContainsKey(spriteName) == true)
                {
                    string atlasPrefabPath = _spriteAtlasDict[spriteName][0];
                    atlasPrefabPath = ObjectPool.Instance.GetResMapping(atlasPrefabPath);
                    ObjectPool.Instance.GetGameObject(atlasPrefabPath, delegate(GameObject go) { OnAtlasPrefabLoaded(holder, go); });
                }
                else
                {
                    Debug.LogError(string.Format("Key {0} not found in atlas JSON {1}", spriteName, ATLAS_JSON_PATH));
                }
            }
        }

        private static void OnAtlasPrefabLoaded(GameObject holder, GameObject go)
        {
            if (holder != null)
            {
                ParamsRecord record = _paramsRecordDict[holder];
                go.transform.SetParent(holder.transform);
                go.SetActive(true);
                RectTransform rect = go.GetComponent<RectTransform>();
                rect.localScale = Vector3.one;
                rect.localPosition = Vector3.zero;
                rect.sizeDelta = holder.GetComponent<RectTransform>().sizeDelta;
                SpritePlayer player = go.AddComponent<SpritePlayer>();
                player.spriteNameList = record.spriteNameList;
                player.spriteAtlasDict = _spriteAtlasDict;
                ImageWrapper image = go.GetComponent<ImageWrapper>();
                Color color = image.color;
                color.a = 1.0f;
                image.color = color;
                _paramsRecordDict.Remove(holder);
                _spritePlasyerDict.Add(holder, player);
            }
        }



        public static XIconAnimOnce AddIconAnimOnce(GameObject parent, int iconAnimId, float seconds)
        {

            var setting = icon_anim_helper.GetIconAnimSetting(iconAnimId);
            return AddAnimationOnce(parent, setting.iconNames, seconds);
        }

        public static XIconAnimOnce AddAnimationOnce(GameObject parent, string[] spriteNameList, float seconds)
        {
            GameAtlasUtils.Init();
            XIconAnimOnce xIconAnim = null;
            try
            {
                xIconAnim = parent.GetComponent<XIconAnimOnce>();
                if (xIconAnim == null)
                {
                    xIconAnim = parent.AddComponent<XIconAnimOnce>();
                }
                xIconAnim.SetSprites(spriteNameList, seconds);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("::" + ex.Message + ":" + ex.StackTrace);
            }
            return xIconAnim;
        }

        public static XIconAnim AddAnimation(GameObject parent, int iconAnimId, int colorId, bool raycastTarget)
        {
            var setting = icon_anim_helper.GetIconAnimSetting(iconAnimId);
            return AddAnimation(parent, setting.iconNames, colorId, raycastTarget);
        }

        public static XIconAnim AddAnimation(GameObject parent, string[] spriteNameList, int colorId, bool raycastTarget = false)
        {
            GameAtlasUtils.Init();
            XIconAnim xIconAnim = null;
            try
            {
                xIconAnim = parent.GetComponent<XIconAnim>();
                if (xIconAnim == null)
                {
                    xIconAnim = parent.AddComponent<XIconAnim>();
                }
                Color color = Color.white;
                if (colorId > 0)
                {
                    color = ColorDefine.GetColorById(colorId);
                }
                xIconAnim.SetSprites(spriteNameList, color, raycastTarget);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("::" + ex.Message + ":" + ex.StackTrace);
            }
            return xIconAnim;
        }

        public static void PlayAnimation(GameObject go, bool animation)
        {
            var xIconAnim = go.GetComponent<XIconAnim>();
            if (xIconAnim != null)
            {
                xIconAnim.playAnimation = animation;
            }
        }

        public static void SetAnimationSpeed(GameObject go, int speed)
        {
            var xIconAnim = go.GetComponent<XIconAnim>();
            if (xIconAnim != null)
            {
                xIconAnim.speed = speed;
            }
        }

        static Dictionary<string, GameObject> _spritesMap = new Dictionary<string, GameObject>();
        public static T GetSpritesProxy<T>(GameObject atlasPrefab, string[] spriteNameList) where T : XIconAnimProxyBase
        {
            if (atlasPrefab == null) return null;
            string firstSpriteName = spriteNameList[0];
            Sprite[] sprites;
            T spritesProxy = atlasPrefab.GetComponent<T>();
            if (spritesProxy == null) spritesProxy = atlasPrefab.AddComponent<T>();
            var image = atlasPrefab.GetComponent<ImageWrapper>();
            spritesProxy.image = image;
            if (_spritesMap.ContainsKey(firstSpriteName))
            {
                var oldGO = _spritesMap[firstSpriteName];
                if (oldGO != null)
                {
                    var oldSpritesProxy = oldGO.GetComponent<XIconAnimProxyBase>();
                    if (oldSpritesProxy.sprites.Length > 0 && !firstSpriteName.Equals(oldSpritesProxy.sprites[0].name))
                    {
                        _spritesMap[firstSpriteName] = null;
                    }
                    else
                    {
                        spritesProxy.sprites = oldSpritesProxy.sprites;
                        return spritesProxy;
                    }
                }
                else
                {
                    _spritesMap[firstSpriteName] = null;
                }
            }

            sprites = new Sprite[spriteNameList.Length];
            for (int i = 0; i < spriteNameList.Length; i++)
            {
                string spriteKey = GameAtlasUtils.GetSpriteKeyByName(spriteNameList[i], image.spriteKeyTemplate);
                sprites[i] = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
            }
            spritesProxy.sprites = sprites;
            _spritesMap[firstSpriteName] = atlasPrefab;

            return spritesProxy;
        }

        private class ParamsRecord
        {
            public GameObject holder;
            public string[] spriteNameList;

            public ParamsRecord(GameObject holder, string[] spriteNameList)
            {
                this.holder = holder;
                this.spriteNameList = spriteNameList;
            }
        }

        private class SpritePlayer : UIBehaviour
        {
            public Dictionary<string, List<string>> spriteAtlasDict;
            public string[] _spriteNameList;
            public bool _isAnimation = true;

            private ImageWrapper _image;
            private int _spriteIndex = 0;

            protected override void Awake()
            {
                _image = GetComponent<ImageWrapper>();
                //_image.color = Color.white;
                _image.raycastTarget = false;
            }

            public string[] spriteNameList
            {
                set
                {
                    _spriteNameList = InitListSpriteKey(value);
                }
                get
                {
                    return _spriteNameList;
                }
            }

            private string[] InitListSpriteKey(string[] listName)
            {
                if (listName == null)
                {
                    return null;
                }
                _spriteNameList = new string[listName.Length];
                for (int i = 0; i < listName.Length; i++)
                {
                    _spriteNameList[i] = string.Format(_image.spriteKeyTemplate, listName[i]);
                }
                return _spriteNameList;
            }

            protected void Update()
            {
                if (!_isAnimation)
                {
                    return;
                }
                string spriteKey = _spriteNameList[_spriteIndex];// string.Format(_image.spriteKeyTemplate, spriteNameList[_spriteIndex]);
                //GameLoader.Utils.LoggerHelper.Error(spriteKey + "====================================");
                //_image.SetDimensionsDirty();
                _image.sprite = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
                //_image.color = Color.white;
                //_image.SetDimensionsDirty();
                _spriteIndex++;
                if (_spriteIndex == spriteNameList.Length)
                {
                    _spriteIndex = 0;
                }
            }

            private void FixPosition(string spriteName)
            {
                List<string> param = spriteAtlasDict[spriteName];
                RectTransform rect = GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(int.Parse(param[1]), -int.Parse(param[2]));
                rect.sizeDelta = new Vector2(int.Parse(param[3]), int.Parse(param[4]));
            }
        }
    }
}
