﻿using ACTSystem;
using GameMain.GlobalManager;
using UnityEngine;

namespace Common.Utils
{
    public class GeometryUtil
    {
        public static float CalculateRotation(Vector3 targetVector)
        {
            Vector2 vec1 = new Vector2(0, 1);
            Vector2 vec2 = targetVector;
            float radian = CalculateRadianBetweenTwoVector(vec1, vec2);
            float angle = radian * Mathf.Rad2Deg;
            if (vec2.x < 0)
            {
                angle = 360 - angle;
            }
            return angle;
        }

        public static bool TryGetIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, out Vector2 intersection)
        {
            intersection = Vector2.zero;
            if (IsIntersect(p1, p2, p3, p4) == false)
            {
                return false;
            }
            float area1 = Det(p1, p2, p1, p3);
            float area2 = Det(p1, p2, p1, p4);
            intersection.x = (area2 * p3.x - area1 * p4.x) / (area2 - area1);
            intersection.y = (area2 * p3.y - area1 * p4.y) / (area2 - area1);
            return true;
        }

        public static float Det(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
        {
            return (b.x - a.x) * (d.y - c.y) - (d.x - c.x) * (b.y - a.y);
        }

        public static bool IsPointOnSegment(Vector2 start, Vector2 end, Vector2 point)
        {
            float distance1 = Vector2.Distance(start, point) + Vector2.Distance(point, end);
            float distance2 = Vector2.Distance(start, end);
            return ACTMathUtils.Approximately(distance1, distance2);
        }

        public static float Cross(Vector2 a, Vector2 b, Vector2 c)
        {
            return (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y);
        }

        public static bool IsIntersect(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
        {
            return Mathf.Max(a.x, b.x) >= Mathf.Min(c.x, d.x)
                && Mathf.Max(a.y, b.y) >= Mathf.Min(c.y, d.y)
                && Mathf.Max(c.x, d.x) >= Mathf.Min(a.x, b.x)
                && Mathf.Max(c.y, d.y) >= Mathf.Min(a.y, b.y)
                && Cross(c, b, a) * Cross(b, d, a) >= 0
                && Cross(a, d, c) * Cross(d, b, c) >= 0;
        }

        public static bool CheckPositionIsInRect(Vector2 position, float left, float bottom, float right, float top)
        {
            Rect screenRect = new Rect(left, bottom, right, top);
            return screenRect.Contains(position);
        }

        public static float CalculateRadianBetweenTwoVector(Vector2 vec1, Vector2 vec2)
        {
            float dot = Vector2.Dot(vec1, vec2);
            float cosx = dot / (vec1.magnitude * vec2.magnitude);
            return Mathf.Acos(cosx);
        }

        public static Vector2 GetTargetVector(Vector3 position)
        {
            if (CameraManager.GetInstance().CameraTransform == null)
            {
                return Vector2.zero;
            }
            Vector3 cameraPosition = CameraManager.GetInstance().CameraTransform.position;
            Vector3 vectorCameraToEntity = position - cameraPosition;
            Vector3 cameraDirection = CameraManager.GetInstance().CameraTransform.forward;
            float radian = CalculateRadianBetweenTwoVector(cameraDirection, vectorCameraToEntity);
            float distance = vectorCameraToEntity.magnitude * Mathf.Cos(radian);
            Vector3 cameraVector = cameraDirection * distance;
            Vector3 targetVector = -cameraVector + vectorCameraToEntity;
            Vector3 vector = CameraManager.GetInstance().CameraTransform.InverseTransformVector(targetVector).normalized;
            return vector;
        }

    }
}
