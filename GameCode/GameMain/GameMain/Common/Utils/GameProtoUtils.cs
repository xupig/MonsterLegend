﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using GameLoader.Utils;

namespace Common.Utils
{
    public class GameProtoUtils
    {
        private static System.IO.Stream _stream;
        private static SerializableData.DataSerializer _dataSerializer;
        private static byte[] _byteArrNullProto;
        private static int s_maxStreamLength = 1000;

        public static T ParseProto<T>(Byte[] byteArr) where T: global::ProtoBuf.IExtensible
        {
           return (T)(ParseProto(byteArr,typeof(T)));
        }

        private static bool IsEqualNullByteArr(Byte[] byteArr1, Byte[] byteArr2)
        {
            if(_byteArrNullProto == null)
            {
                _byteArrNullProto = System.Text.Encoding.Default.GetBytes( "{}" );
            }
            if ( byteArr1 == null || byteArr2 == null || byteArr1.Length != byteArr2.Length )
            {
                return false;
            }
            for ( int i = 0; i < byteArr1.Length; i++ )
            {
                if ( byteArr1[i] != byteArr2[i] )
                {
                    return false;
                }
            }
            return true;
        }

        public static object ParseProto(Byte[] byteArr, Type type)
        {
            if ( IsEqualNullByteArr( byteArr, _byteArrNullProto ) == true )
            {
                var byteToString = System.Text.Encoding.UTF8.GetString(byteArr);
                LoggerHelper.Error("ParseProto Wrong! " + byteToString);
                return null;
            }
            if (_stream == null)
            {
                _stream = new System.IO.MemoryStream( s_maxStreamLength );
            }
            if ( _dataSerializer == null )
            {
                _dataSerializer = new SerializableData.DataSerializer();
            }
            _stream.SetLength( byteArr.Length );
            _stream.Write( byteArr, 0, byteArr.Length );
            _stream.Position = 0;
            object obj = _dataSerializer.Deserialize( _stream, null, type );
            _stream.SetLength( 0 );
            _stream.Position = 0;
            return obj;
        }

        public static void CloseStream()
        {
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }

        public static string ParseByteArrToString( byte[] byteArr)
        {
            if (byteArr == null)
            {
                return string.Empty;
            }
            return System.Text.ASCIIEncoding.UTF8.GetString( byteArr );
        }

        public static byte[] ParseStringToByteArr(string content)
        {
            return System.Text.ASCIIEncoding.UTF8.GetBytes( content );
        }

        public static LuaInterface.LuaTable ProtoTableToLuaTable(GameLoader.Utils.CustomType.LuaTable proto)
        {
            object[] result = GameMain.LuaDriver.instance.CallFunction("CSCALL_CREATE_TABLE", null, null);
            LuaInterface.LuaTable table = result[0] as LuaInterface.LuaTable;
            foreach (var item in proto)
            {
                if (item.Value.GetType() == typeof(GameLoader.Utils.CustomType.LuaTable))
                {
                    table[item.Key] = ProtoTableToLuaTable((GameLoader.Utils.CustomType.LuaTable)item.Value);
                }
                else
                {
                    table[item.Key] = item.Value;
                }
            }
            return table;
        }

    }
}
