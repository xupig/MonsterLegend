﻿#region 模块信息
/*==========================================
// 文件名：MogoGameObjectHelper
// 命名空间: Common.Utils
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/30 19:24:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using GameMain;
using GameMain.GlobalManager;
using UIExtension;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Common.Utils
{
    public class RaycastTargetHelper
    {
        const string HIT_AREA_NAME = "____HitArea";
        public static void AddHitArea(GameObject go, float width, float high, float offsetX, float offsetY)
        {
            var rect = go.GetComponent<RectTransform>();
            if (rect == null) return;
            var hitArea = go.transform.FindChild(HIT_AREA_NAME);
            GameObject hitAreaGo = null;
            if (hitArea == null)
            {
                hitAreaGo = new GameObject(HIT_AREA_NAME);
                hitArea = hitAreaGo.transform;
                hitArea.SetParent(go.transform, false);
                hitAreaGo.AddComponent<RectTransform>();
            }
            var hitAreaRect = hitAreaGo.GetComponent<RectTransform>();
            hitAreaRect.anchoredPosition = new Vector2(offsetX, offsetY);
            hitAreaRect.anchorMax = rect.anchorMax;
            hitAreaRect.anchorMin = rect.anchorMin;
            hitAreaRect.sizeDelta = new Vector2(width, high);
            hitAreaGo.AddComponent<XButtonHitArea>();
        }
    }
}
