﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GameLoader;
using UnityEngine;
using ShaderUtils;
using GameResource;
using GameLoader.Utils;
using LuaInterface;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using Common.States;
using GameData;
using Common.Utils;
using System.Net;
using GameMain.GlobalManager;
using MogoEngine;
using GameLoader.Utils.Timer;
using Common.ClientConfig;
using ACTSystem;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using ArtTech;
using Common.Global;

namespace GameMain
{
    public class LuaFacade
    {
        public static void CallLuaFunction(LuaFunction luafunc)
        {
            if (luafunc != null)
            {
                luafunc.BeginPCall();
                luafunc.PCall();
                luafunc.EndPCall();
                luafunc.Dispose();
            }
        }

        public static void CallLuaFunction(LuaFunction luafunc, object obj)
        {
            if (luafunc != null)
            {
                luafunc.BeginPCall();
                luafunc.Push(obj);
                luafunc.PCall();
                luafunc.EndPCall();
                luafunc.Dispose();
            }
        }

        public static void GetGameObject(string path, LuaFunction luafunc)
        {
            ObjectPool.Instance.GetGameObject(path, delegate (GameObject go)
            {
                CallLuaFunction(luafunc, go);
            });
        }

        public static void GetGameObjects(string[] paths, LuaFunction luafunc)
        {
            ObjectPool.Instance.GetGameObjects(paths, delegate (GameObject[] gos)
            {
                CallLuaFunction(luafunc, gos);
            });
        }

        public static void Destroy(UnityEngine.Object obj)
        {
            GameObject.Destroy(obj);
        }

        public static void PreloadAsset(string[] paths, LuaFunction luafunc)
        {
            ObjectPool.Instance.PreloadAsset(paths, delegate ()
            {
                CallLuaFunction(luafunc);
            });
        }
        public static void PreloadAssetFromBuilder(LuaFunction luafunc)
        {
            var paths = LuaPreloadPathsBuilder.GetPaths().ToArray();
            ObjectPool.Instance.PreloadAsset(paths, delegate ()
            {
                CallLuaFunction(luafunc);
            });
        }

        public static void PreloadAssetInBackgroundFromBuilder(uint priority)
        {
            var paths = LuaPreloadPathsBuilder.GetPaths().ToArray();
            PreloadManager.GetInstance().AddBackgroundPreloadPaths(priority, paths);
        }

        public static void PreloadAssetInBackground(uint priority, string[] paths)
        {
            PreloadManager.GetInstance().AddBackgroundPreloadPaths(priority, paths);
        }

        public static void SetLoadAsync(bool state)
        {
            ObjectPool.Instance.SetIsAsync(state);
        }

        public static void ClearPreloadInBackground()
        {
            PreloadManager.GetInstance().ClearPreloadInBackground();
        }

        public static LuaTable AddLuaBehaviour(GameObject go, string luaClassType)
        {
            LuaBehaviour luaBehaviour = LuaBehaviour.AddLuaBehaviour(go, typeof(LuaBehaviour), luaClassType);
            return luaBehaviour.luaTable;
        }

        public static LuaTable AddLuaUIComponent(GameObject go, string componentType, string luaClassType)
        {
            LuaBehaviour luaBehaviour = LuaBehaviour.AddLuaBehaviour(go, LuaUISetting.GetLuaUIComponent(componentType), luaClassType);
            return luaBehaviour.luaTable;
        }

        public static void LoadScene(LoadSceneSetting loadSceneSetting, LuaFunction luafunc)
        {
            ClearCacheBeforeLoading();
            SceneMgr.Instance.LoadScene(loadSceneSetting, delegate ()
            {
                CallLuaFunction(luafunc);
            });
        }

        static Dictionary<string, Type> _entityTypeMap;
        public static void RegisterEntityType(string entityTypeName, string typeName)
        {
            if (_entityTypeMap == null)
            {
                _entityTypeMap = new Dictionary<string, Type>();
                _entityTypeMap["EntityStatic"] = typeof(GameMain.EntityStatic);
                _entityTypeMap["EntityCreature"] = typeof(GameMain.EntityCreature);
                _entityTypeMap["EntityPlayer"] = typeof(GameMain.EntityPlayer);
                _entityTypeMap["EntityDropItems"] = typeof(GameMain.EntityDropItems);
                _entityTypeMap["EntityAccount"] = typeof(GameMain.EntityAccount);
            }
            EntityMgr.Instance.RegisterEntityType(entityTypeName, _entityTypeMap[typeName]);
        }

        //创建客户端实体
        public static LuaTable CreateEntity(string entityTypeName, LuaFunction luafunc = null)
        {
            EntityLuaBase entity = EntityMgr.Instance.CreateClientEntity(entityTypeName, delegate (Entity ent)
            {
                CallLuaFunction(luafunc, (ent as EntityLuaBase).luaEntity);
            }) as EntityLuaBase;
            return entity.luaEntity;
        }

        //创建客户端实体
        public static LuaTable CreateEntity(uint entityId, string entityTypeName, LuaFunction luafunc = null)
        {
            EntityLuaBase entity = EntityMgr.Instance.CreateEntity(entityId, entityTypeName, delegate (Entity ent)
            {
                CallLuaFunction(luafunc, (ent as EntityLuaBase).luaEntity);
            }) as EntityLuaBase;
            return entity.luaEntity;
        }

        //删除实体，主要用于删除客户端创建的实体
        public static void DestroyEntity(uint entityId)
        {
            EntityMgr.Instance.DestroyEntity(entityId);
        }

        public static void AddEntityTypePool(string typeName, string tagName)
        {
            EntityMgr.Instance.AddEntityTypePool(typeName, tagName);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrByte(int entityID, string attrName, byte value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrInt(int entityID, string attrName, int value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrInt16(int entityID, string attrName, Int16 value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrUInt(int entityID, string attrName, uint value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrFloat(int entityID, string attrName, float value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttrString(int entityID, string attrName, string value)
        {
            SynEntityAttr(entityID, attrName, (object)value);
        }

        private static void SynEntityAttr(int entityID, string attrName, object value)
        {

            Entity entity = EntityMgr.Instance.GetEntity((uint)entityID);
            if (entity != null)
            {
                EntityMgr.Instance.SetAttr(entity, attrName, value);
            }
            //EntityMgr.Instance.SetAttr(entity, attrName, value);
        }

        public static Type INT16 = typeof(Int16);
        public static Type UINT16 = typeof(UInt16);
        public static Type INT32 = typeof(Int32);
        public static Type UINT32 = typeof(UInt32);
        public static Type BYTE = typeof(byte);
        public static Type FLOAT = typeof(float);
        public static Type STRING = typeof(string);
        public static Type BOOL = typeof(bool);
        public static void SynEntityAttrByDrama(uint entityID, string attrName, string attrValue)
        {
            Entity creature = EntityMgr.Instance.GetEntity((uint)entityID);
            if (creature != null)
            {
                var propertyInfo = creature.GetType().GetProperty(attrName);
                if (propertyInfo == null) return;
                var type = propertyInfo.PropertyType;
                if (type == INT16) { EntityMgr.Instance.SetAttr(creature, attrName, Int16.Parse(attrValue)); }
                else if (type == UINT16) { EntityMgr.Instance.SetAttr(creature, attrName, UInt16.Parse(attrValue)); }
                else if (type == INT32) { EntityMgr.Instance.SetAttr(creature, attrName, Int32.Parse(attrValue)); }
                else if (type == UINT32) { EntityMgr.Instance.SetAttr(creature, attrName, UInt32.Parse(attrValue)); }
                else if (type == BYTE) { EntityMgr.Instance.SetAttr(creature, attrName, byte.Parse(attrValue)); }
                else if (type == FLOAT) { EntityMgr.Instance.SetAttr(creature, attrName, float.Parse(attrValue)); }
                else if (type == STRING) { EntityMgr.Instance.SetAttr(creature, attrName, attrValue); }
                else if (type == BOOL) {
                    bool attrVal = true;
                    if (Int16.Parse(attrValue) == 0) {
                        attrVal = false;
                    }
                    EntityMgr.Instance.SetAttr(creature, attrName, attrVal); 
                }
            }
        }

        public static Dictionary<string, string> GetReloadXmlMap()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (!UnityPropUtils.IsEditor || GameLoader.SystemConfig.ReleaseMode) return result;
            string dataPath = GameLoader.SystemConfig.ResPath + "/data/xml";
            //LoggerHelper.Error("dataPath:"+dataPath);
            var xmlFiles = Directory.GetFiles(dataPath, "*.xml", SearchOption.TopDirectoryOnly);
            if (xmlFiles.Length != 0)
            {
                for (int i = 0; i < xmlFiles.Length; i++)
                {
                    string filePath = xmlFiles[i];
                    string xmlName = Path.GetFileNameWithoutExtension(filePath);
                    result.Add(xmlName, FileUtils.LoadFile(filePath));
                }
            }
            return result;
        }

        public static int[] BytesDesToIntArray(byte[] bytes, int start, int len)
        {
            return PBUtils.BytesDesToIntArray(bytes, start, len);
        }

        public static void ReloadXml()
        {
            ReloadXmlManager.GetInstance().ReloadFromLocal();
        }

        public static LuaTable CreateSpaceBox(string luaClassType, string name, Vector3 position, Vector3 size, Vector3 rotation)
        {
            GameObject obj = new GameObject(name);
            obj.layer = ACTSystemTools.NameToLayer("SpaceAction");
            obj.transform.position = position;
            LuaSpaceBox box = LuaBehaviour.AddLuaBehaviour(obj, typeof(LuaSpaceBox), luaClassType) as LuaSpaceBox;
            box.CreateBoxCollider(size, rotation);
            return box.luaTable;
        }
        public static void AddIcon(GameObject parent, int iconId, int colorId, bool raycastTarget, float gray, bool resetContainer, bool autoSize = false, float scale = 1.0f)
        {
            GameAtlasUtils.AddIcon(parent, iconId, colorId, raycastTarget, gray, resetContainer, autoSize, scale);
        }

        public static void AddHitArea(GameObject parent, float width, float high, float offsetX, float offsetY)
        {
            RaycastTargetHelper.AddHitArea(parent, width, high, offsetX, offsetY);
        }

        public static void GetHttpUrl(string url, LuaFunction onDoneLuaF, LuaFunction onFailLuaF)
        {
            Action<string> onDoneCB = (result) =>
            {
                CallLuaFunction(onDoneLuaF, result);
            };
            Action<HttpStatusCode> onFailCB = (code) =>
            {
                CallLuaFunction(onFailLuaF, (int)code);
            };
            GameLoader.Utils.HttpUtils.Get(url, onDoneCB, onFailCB);
        }

        public static void DownloadString(string url, LuaFunction onDoneLuaF, LuaFunction onFailLuaF)
        {
            Action<string> onDoneCB = (result) =>
            {
                CallLuaFunction(onDoneLuaF, result);
            };
            Action onFailCB = () =>
            {
                CallLuaFunction(onFailLuaF);
            };
            GameLoader.Mgrs.TextDownloadMgr.AsyncDownloadString(url, onDoneCB, onFailCB);
        }

        public static void DownloadStringWithRandomUrl(string url, LuaFunction onDoneLuaF, LuaFunction onFailLuaF)
        {
            url = GameLoader.Mgrs.TextDownloadMgr.GetRandomParasUrl(url);
            DownloadString(url, onDoneLuaF, onFailLuaF);
        }

        public static string LoadFile(string fileName)
        {
            return FileUtils.LoadFile(fileName);
        }

        public static void DeleteFile(string fileName)
        {
            File.Delete(fileName);
        }

        public static void SaveText(string fileName, string text)
        {
            FileUtils.SaveText(fileName, text);
        }

        public static void ClearClientAllFile()
        {
            GameLoader.Mgrs.VersionManager.Instance.ClearClientAllFile();
        }

        public static void SaveCache(string localId, string[] data, bool forCurPlayer)
        {
            if (forCurPlayer)
            {
                LocalCache.Write(localId, data, CacheLevel.Permanent);
            }
            else
            {
                LocalCacheHelper.WriteCommon(localId, data);
            }
        }

        public static string[] ReadCache(string localId, bool forCurPlayer)
        {
            if (forCurPlayer)
            {
                return LocalCache.Read<string[]>(localId);
            }
            else
            {
                return LocalCacheHelper.ReadCommon<string[]>(localId);
            }
        }

        public static void ShakeCamera(int id, float length)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ShakeCamera(id, length);
        }

        public static void ChangeCameraFov(int cameraFov = 0)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeCameraFov(cameraFov);
        }

        public static void ChangeToRotationTargetMotion(Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        public static uint PlaySound(int soundId)
        {
            return SoundManager.GetInstance().PlaySound(soundId);
        }

        public static bool StopSound(uint id)
        {
            return SoundManager.GetInstance().StopSound(id);
        }

        public static void ResetAllSound()
        {
            SoundManager.GetInstance().ResetAllSound();
        }

        public static void PlayBgMusic(int musicId, bool fadeOut, bool fadeIn)
        {
            try
            {
                BgMusicManager.GetInstance().PlayBgMusic(musicId, fadeOut, fadeIn);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PlayBgMusic:" + ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        public static void StopAllBgMusic()
        {
            BgMusicManager.GetInstance().StopAllBgMusic();
        }

        public static void ChangeToRotationPositionMotion(Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToRotationPositionMotion(localEulerAngle, rotationDuration, rotationAccelerateRate, position, positionDuration, ignoreTimeScale);
        }

        public static void ChangeToTargetPositionMotion(Transform target, float rotationDuration, float rotationMinSpeed, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration, ignoreTimeScale);
        }

        public static void ChangeToRotationTargetMotionForLocal(Transform root, Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate,
                    float distance, float distanceDuration)
        {
            var rootEuler = root.localEulerAngles;
            localEulerAngle = rootEuler + localEulerAngle;
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToCloseUpMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, true, false, false);
        }

        public static void StopCurrentCameraAnimation()
        {
            GameMain.GlobalManager.CameraManager.GetInstance().StopCurrentCameraAnimation();
        }

        public static void StopCurrentCameraMotion()
        {
            GameMain.GlobalManager.CameraManager.GetInstance().StopCurrentCameraMotion();
        }

        public static void ChangeCamera(int cameraType, float rotationX, float rotationY, float rotateTime, float distance, Vector3 targetPostion, float moveTime, float speed, float odds, string concuss, int cameraFov = 0)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeCamera(cameraType, rotationX, rotationY, rotateTime, distance, targetPostion, moveTime, speed, odds, concuss, cameraFov);
        }

        public static object[] GetCameraInfoBackup()
        {
            object[] result = new object[2];
            result[0] = CameraManager.GetInstance().GetCameraInfoBackup().localEulerAngles;
            result[1] = CameraManager.GetInstance().GetCameraInfoBackup().distance;
            return result;
        }

        public static void ChangeCameraFarClipPlane(float farClipPlane, float duration)
        {
            CameraManager.GetInstance().ChangeCameraFarClipPlane(farClipPlane, duration);
        }

        public static void ChangeCameraNearClipPlane(float nearClipPlane, float duration)
        {
            CameraManager.GetInstance().ChangeCameraNearClipPlane(nearClipPlane, duration);
        }

        public static void ChangeFogStartDistance(float fogStartDistance, float duration)
        {
            CameraManager.GetInstance().ChangeFogStartDistance(fogStartDistance, duration);
        }

        public static void ChangeFogEndDistance(float fogEndDistance, float duration)
        {
            CameraManager.GetInstance().ChangeFogEndDistance(fogEndDistance, duration);
        }

        public static void ChangeCameraColor(float r, float g, float b)
        {
            var camera = CameraManager.GetInstance().Camera;
            if(camera)
            {
                camera.backgroundColor = new Color(r, g, b);
            }
        }

        public static void ResetCameraInfo(bool keepCurrentState = false)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ResetCameraInfo(keepCurrentState);
        }

        public static void ResetToNormal(bool keepCurrentState = false)
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ResetToLastRotationTargetMotion(keepCurrentState);
        }

        public static void FaceToNPC(uint id, float distance, float height = 1f)
        {
            EntityCreature lockTarget = MogoWorld.Entities[id] as EntityCreature;
            if (lockTarget == null || lockTarget.actor == null)
                return;
            Transform trans = lockTarget.actor.boneController.GetBoneByName("slot_camera");
            //var player = MogoWorld.Player as EntityCreature;
            //if (player != null)
            //{
            //    lockTarget.actor.FaceTo(player.actor);
            //    player.actor.FaceTo(lockTarget.actor);
            //}
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToFaceTargetMotion(trans, Vector3.zero, distance, height);
        }

        public static void SwitchCameraLockTarget(uint id)
        {
            EntityCreature lockTarget = MogoWorld.Entities[id] as EntityCreature;
            if (lockTarget == null)
            {
                return;
            }
            Transform trans = lockTarget.actor.boneController.GetBoneByName("slot_camera");
            if (trans != null)
            {
                GameMain.GlobalManager.CameraManager.GetInstance().SwitchLockTarget(trans);
            }
        }

        public static void BlackCamera(float alpha, float duration)
        {

        }

        public static void FixedAngleCamera()
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToFixedAngleMotion(EntityPlayer.Player.GetTransform(), 45, 12);
        }

        //public static void FlyToScene(string targetScene)
        //{
        //    var path = new List<Vector3>();
        //    path.AddRange(SceneMgr.Instance.GetFlyPath(targetScene));
        //    //LoggerHelper.Error(targetScene + " " + path.PackList());
        //    EntityPlayer.Player.moveManager.MoveByPathFly(speed: 10, path: path);
        //}

        static GameLoader.Utils.CustomType.LuaTable _luaTableCache = new GameLoader.Utils.CustomType.LuaTable();
        static LuaTable _resultLuaTable = null;
        public static LuaTable CSLuaStringToLuaTable(string inputString, string src)
        {
            if (_resultLuaTable != null) _resultLuaTable.Dispose();
            _resultLuaTable = null;
            try
            {
                if (inputString == null)
                {
                    LoggerHelper.Error("CSLuaStringToLuaTable inputString is null. src: " + src);
                    inputString = "";
                }
                _luaTableCache.sourceLuaString = string.Empty;
                _luaTableCache.SetLuaString("{}");
                bool flag = CSLuaTableUtils.ParseLuaTable(inputString, out _luaTableCache);
                if (!flag)
                {
                    if (_luaTableCache == null)
                    {
                        LoggerHelper.Error("CSLuaStringToLuaTable Error, _luaTableCache is null: inputString: " + inputString + " src: " + src);
                        _luaTableCache = new GameLoader.Utils.CustomType.LuaTable();
                    }
                    _luaTableCache.sourceLuaString = string.Empty;
                    _luaTableCache.SetLuaString("{}");
                }
                //Debug.LogError("inputString:" + _luaTableCache.sourceLuaString + ":" + _luaTableCache.GetLuaString());
                string luaString = "return " + _luaTableCache.GetLuaString();
                var objs = LuaDriver.instance.DoString(luaString);
                if (objs.Length > 0)
                {
                    _resultLuaTable = objs[0] as LuaTable;
                }
            }
            catch (Exception ex)
            {
                CSLuaTableUtils.ParseLuaTable("", out _luaTableCache);
                string luaString = "return " + _luaTableCache.GetLuaString();
                var objs = LuaDriver.instance.DoString(luaString);
                if (objs.Length > 0)
                {
                    _resultLuaTable = objs[0] as LuaTable;
                }
                LoggerHelper.Error("CSLuaStringToLuaTable Error: inputString: " + inputString + " src: " + src + "\n" + ex.Message);
            }
            return _resultLuaTable;
        }

        public static string GetPropertyNameById(int id)
        {
            if (EntityPlayer.Player == null)
            {
                return "";
            }
            EntityDefProperties prop;
            EntityPlayer.Player.entityDef.Properties.TryGetValue((ushort)id, out prop);
            if (prop == null)
            {
                return "";
            }
            return prop.Name;
        }

        public static void LoadBlockMap(string name)
        {
            FindPathManager.GetInstance().LoadBlockMap(name);
            SafeAreaManager.GetInstance().LoadSafeAreaMap(name);
        }

        public static void PauseHandleData(bool pause)
        {
            ServerProxy.Instance.pauseHandleData = pause;
        }

        public static void SetSyncPosSpaceFrame(int frame)
        {
            PlayerSyncPosManager.GetInstance().SetSyncPosSpaceFrame(frame);
        }

        public static void SetPlayerSyncPosState(bool state)
        {
            PlayerSyncPosManager.GetInstance().SetSyncPosState(state);
        }

        public static void PauseAllSynPos(bool state)
        {
            EntityPlayer.pauseSyncPos = state;
        }

        public static void SetBattleServerMode(bool state)
        {
            EntityPlayer.battleServerMode = state;
        }

        public static void SetSceneLibra(bool state)
        {
            EntityPlayer.isLibraMode = state;
        }

        public static string GetLanguageContent(int id)
        {
            return GameLanguageUtil.GetContent(id);
        }

        public static GameObject CopyUIGameObject(GameObject hostGo, string srcPath, string parentPath)
        {
            Transform srcTran = hostGo.transform.Find(srcPath);
            Transform parentTran = null;
            if (string.IsNullOrEmpty(parentPath))
                parentTran = hostGo.transform;
            else
                parentTran = hostGo.transform.Find(parentPath);
            if (srcTran != null && parentTran != null)
            {
                Transform destTran = GameObject.Instantiate(srcTran);
                destTran.SetParent(parentTran, false);
                return destTran.gameObject;
            }
            return null;
        }

        public static bool State = false;
        public static void TestLuaCall()
        {
            if (State)
            {
                EntityPlayer.Player.actor.animationController.IdleToReady();
                TimerHeap.AddTimer(400, 0, () =>
                {
                    EntityPlayer.Player.actor.equipController.PackUpWeapon(false);
                });

            }
            else
            {
                EntityPlayer.Player.actor.animationController.ReadyToIdle();
                TimerHeap.AddTimer(400, 0, () =>
                {
                    EntityPlayer.Player.actor.equipController.PackUpWeapon(true);
                });
            }
            State = !State;
        }

        public static void TestUIComponent(GameObject go, int type)
        {
            GameMain.Test.TestManager.GetInstance().Init(go, type);
        }

        public static string ConvertStyle(string content)
        {
            return GameStringUtils.ConvertStyle(content);
        }

        public static string FilterWords(string text)
        {
            return UIChatManager.Instance.FilterWords(text);
        }

        public static void SetGlobalRim(int flag)
        {
            //ShaderGlobal.SetGlobalRim(flag);
        }

        private static ChildrenComponentsPool<List<Canvas>> _canvasListPool = new ChildrenComponentsPool<List<Canvas>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<LuaUIParticle>> _particlesListPool = new ChildrenComponentsPool<List<LuaUIParticle>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<ActModelComponent>> _actorModelListPool = new ChildrenComponentsPool<List<ActModelComponent>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<PathModelComponent>> _pathModelListPool = new ChildrenComponentsPool<List<PathModelComponent>>(null, l => l.Clear());
        private static ChildrenComponentsPool<List<ACTActor>> _actActorListPool = new ChildrenComponentsPool<List<ACTActor>>(null, l => l.Clear());

        public static void SetCanvasesState(GameObject go, bool state)
        {
            List<Canvas> canvasList = _canvasListPool.Get();
            go.GetComponentsInChildren(true, canvasList);
            for (int i = 0; i < canvasList.Count; i++)
            {
                //canvasList[i].enabled = state;
                var canvas = canvasList[i];
                canvas.gameObject.layer = state ? PhysicsLayerDefine.LAYER_UI : PhysicsLayerDefine.LAYER_GHOST;
                //var cGo = canvasList[i];
                //if (deepHide && state != cGo.enabled && cGo.name.StartsWith("Panel_"))
                //{
                //    if (state)
                //    {
                //        cGo.transform.localPosition = cGo.transform.localPosition + new Vector3(0, 5000, 0);
                //    }
                //    else
                //    {
                //        cGo.transform.localPosition = cGo.transform.localPosition + new Vector3(0, -5000, 0);
                //    }

                //}
                //cGo.enabled = state;
            }
            _canvasListPool.Release(canvasList);
            HandleChangeLayer(go, _pathModelListPool, state);
            HandleChangeLayer(go, _actorModelListPool, state);
            HandleChangeLayer(go, _particlesListPool, state);
            HandleChangeLayer(go, _actActorListPool, state);
        }

        static private void HandleChangeLayer<T>(GameObject go, ChildrenComponentsPool<List<T>> listPool, bool state) where T : MonoBehaviour
        {
            List<T> list = listPool.Get();
            go.GetComponentsInChildren(true, list);
            for (int i = 0; i < list.Count; i++)
            {
                SetLayer(list[i].transform,
                    state ? PhysicsLayerDefine.LAYER_UI : PhysicsLayerDefine.LAYER_TRANSPARENTFX, true);
            }
            listPool.Release(list);
        }

        static public void SetLayer(Transform trans, int layer, bool includeChildren = false)
        {
            trans.gameObject.layer = layer;
            if (includeChildren)
            {
                for (int i = 0; i < trans.childCount; i++)
                {
                    SetLayer(trans.GetChild(i), layer, includeChildren);
                }
            }
        }

        public static uint PlayFX(string path, Vector3 position, Vector3 eulers, float duration)
        {
            return ACTVisualFXManager.GetInstance().PlayACTVisualFX(path, position, eulers, duration);
        }

        public static void StopFX(uint uid)
        {
            ACTVisualFXManager.GetInstance().StopVisualFX(uid);
        }

        public static void PlayVideo(int id, bool loop)
        {
            Action endCallback = () =>
            {
                LuaDriver.instance.CallFunction("CSCALL_ON_VIDEO_END", id);
            };
            VideoManager.Instance.Play(id, loop, endCallback);
        }

        public static void KeepTimeScale(float scale, float time)
        {
            ACTSystem.ACTTimeScaleManager.GetInstance().KeepTimeScale(scale, time);
        }

        public static void SetRadialBlurColorFul(bool state, float darkness)
        {
            CameraManager.GetInstance().mainCamera.SetRadialBlurColorFul(state, darkness);
        }

        public static void PlayScreenWhite(float start, float end, float duration)
        {
            CameraManager.GetInstance().PlayScreenWhite(start, end, duration);
        }

        public static void ShowMainCamera()
        {
            CameraManager.GetInstance().ShowMainCamera();
        }

        public static void HideMainCamera()
        {
            CameraManager.GetInstance().HideMainCamera();
        }

        private static RolePreviewCamera _rolePreviewCamera = null;
        public static void SetRolePreviewCamera(Transform tran, Vector3 lockPos)
        {
            if (_rolePreviewCamera == null)
            {
                _rolePreviewCamera = tran.gameObject.AddComponent<RolePreviewCamera>();
                _rolePreviewCamera.SetLockPos(lockPos);
            }
        }

        public static void SetRolePreviewCameraArgs(float distance, float rotationX, float height = 1f, float right = 0f)
        {
            if (_rolePreviewCamera == null) return;
            _rolePreviewCamera.SetArgs(distance, rotationX, height, right);
        }

        public static void ResetCameraFaceTo(int rotationY)
        {
            CameraManager.GetInstance().ResetCameraFaceTo(rotationY);
        }

        public static void SetMapCameraArgs(int id)
        {
            try
            {
                CameraManager.GetInstance().SetMapCameraArgs(id);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("SetMapCameraArgs Error: " + id + " " + ex.Message);
            }
        }

        public static void SwitchLockTarget()
        {
            EntityPlayer.Player.lockTargetManager.FindTargetBySwitchButton();
        }

        public static void ShowWaitingResLoading()
        {
            ResourceMappingManager.Instance.ShowLoading();
        }

        public static void HideWaitingResLoading()
        {
            ResourceMappingManager.Instance.HideLoading();
        }

        public static bool IsLevelReady(int level)
        {
            //LoggerHelper.Warning("IsLevelReady: " + ResourceMappingManager.Instance.IsLevelReady(level));
            return ResourceMappingManager.Instance.IsLevelReady(level);
        }

        public static float GetNecessaryResources(int level)
        {
            return ResourceMappingManager.Instance.GetNecessaryResources(level);
        }

        public static void DownloadNecessaryResources()
        {
            ResourceMappingManager.Instance.DownloadNecessaryResources();
        }

        public static void SyncTime()
        {
            MogoWorld.Player.RpcCall("sync_time_req", Global.serverTimeStamp);
        }

        /// <summary>
        /// 设置屏幕亮度
        /// </summary>
        /// <param name="light">亮度，0-255,255为最亮</param>
        public static void SetScreenLight(int light)
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.SetScreenLight(light);
        }

        #region 游戏设置相关

        public static void SetGameQuality(int value)
        {
            PlayerSettingManager.GetInstance().SetGameQuality(value);
        }

        public static void SetGameQualityDelta(int value)
        {
            PlayerSettingManager.GetInstance().SetGameQualityDelta(value);
        }

        public static void SetMusicVolume(float value)
        {
            PlayerSettingManager.GetInstance().SetMusicVolume(value);
        }

        public static void SetSoundVolume(float value)
        {
            PlayerSettingManager.GetInstance().SetSoundVolume(value);
        }

        public static void TempCloseMusicAndSoundVolume()
        {
            PlayerSettingManager.GetInstance().TempCloseMusicAndSoundVolume();
        }

        public static void ResetMusicAndSoundVolume()
        {
            PlayerSettingManager.GetInstance().ResetMusicAndSoundVolume();
        }

        public static int GetAutoQualityLevel()
        {
            //LoggerHelper.Info("GraphicsQuality.TestFPS:" + GraphicsQuality.TestFPS);
            if (GraphicsQuality.AutoQualityLevel == GraphicsQuality.Quality.Lowest)
                return (int)GraphicsQuality.Quality.Low;
            return (int)GraphicsQuality.AutoQualityLevel;
        }

        public static void SetMaxScreenPlayerCount(int count)
        {
            PlayerSettingManager.GetInstance().SetMaxScreenPlayerCount(count);
        }
        
        public static void SetMaxScreenMonsterCount(int count)
        {
            PlayerSettingManager.GetInstance().SetMaxScreenMonsterCount(count);
        }
        public static int GetMaxScreenPlayerCount()
        {
            return PlayerSettingManager.GetInstance().GetMaxScreenPlayerCount();
        }

        public static void SetRealTimeShadow(int mode)
        {
            PlayerSettingManager.GetInstance().SetRealTimeShadow(mode);
        }

        public static void SetHideMonster(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideMonster(mode);
        }

        public static void SetHideNpc(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideNpc(mode);
        }
        
        public static void SetHidePlayer(int mode)
        {
            PlayerSettingManager.GetInstance().SetHidePlayer(mode);
        }

        public static void SetHidePet(int mode)
        {
            PlayerSettingManager.GetInstance().SetHidePet(mode);
        }

        public static void SetHideOtherPlayer(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherPlayer(mode);
        }

        public static void SetHideOtherWing(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherWing(mode);
        }

        public static void SetHideOtherPet(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherPet(mode);
        }

        public static void SetHideOtherFabao(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherFabao(mode);
        }

        public static void SetHideOtherSkillFx(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherSkillFx(mode);
        }

        public static void SetHideOtherFlow(int mode)
        {
            PlayerSettingManager.GetInstance().SetHideOtherFlow(mode);
        }

        #endregion 游戏设置相关

        #region 断线重连相关
        public static void TryReConnect()
        {
            NetSocketManager.OnTryLink();
        }

        public static void SetSilentReConnect(bool state)
        {
            NetSocketManager.silentReConnect = state;
        }

        public static void OnReturnLogin()
        {
            NetSocketManager.OnReturnLogin();
        }
        #endregion

        public static void FindTargetByAI()
        {
            EntityPlayer.Player.lockTargetManager.FindTargetByAI();
        }

        public static GameObject GetMainCameraGo()
        {
            return CameraManager.GetInstance().mainCamera.gameObject;
        }

        public static Transform GetMainCameraTargetTrans()
        {
            return CameraManager.GetInstance().mainCamera.target;
        }

        public static GameObject GetSceneRootObject(string name)
        {
            //Debug.LogError(Driver.Instance.gameObject.scene.IsValid());
            //GameObject[] gos = Driver.Instance.gameObject.scene.GetRootGameObjects();
            //foreach (var go in gos)
            //{
            //    if (go.name.Contains(name))
            //    {
            //        return go;
            //    }
            //}
            //var allScenes = SceneManager.GetAllScenes();
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var item = SceneManager.GetSceneAt(i);
                GameObject[] gos = item.GetRootGameObjects();
                foreach (var go in gos)
                {
                    if (go.name.Contains(name))
                    {
                        return go;
                    }
                }
            }
            return null;
        }

        public static void SetServerId(string serverId)
        {
            LogUploader.GetInstance().ServerId = serverId;
        }

        public static void SetAccount(string account)
        {
            LogUploader.GetInstance().Account = account;
        }

        public static void SetAvatarName(string avatarName)
        {
            LogUploader.GetInstance().AvatarName = avatarName;
        }

        public static void SetSceneId(string sceneId)
        {
            LogUploader.GetInstance().SceneId = sceneId;
        }

        public static void SetParas(string paras)
        {
            LogUploader.GetInstance().Paras = paras;
        }

        public static string GetParas()
        {
            return LogUploader.GetInstance().Paras;
        }

        public static XIconAnim AddIconAnim(GameObject parent, int iconAnimId, int colorId, bool raycastTarget)
        {
            return GameSpriteAnimationUtils.AddAnimation(parent, iconAnimId, colorId, raycastTarget);
        }

        public static XIconAnimOnce AddIconAnimOnce(GameObject parent, int iconAnimId, float seconds)
        {
            return GameSpriteAnimationUtils.AddIconAnimOnce(parent, iconAnimId, seconds);
        }

        public static void PlayIconAnim(GameObject parent, bool state)
        {
            GameSpriteAnimationUtils.PlayAnimation(parent, state);
        }

        public static void SetIconAnimSpeed(GameObject parent, int speed)
        {
            GameSpriteAnimationUtils.SetAnimationSpeed(parent, speed);
        }

        public static void RecoverCamera(Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            Transform target = EntityPlayer.Player.GetBoneByName(ACTBoneController.SLOT_CAMERA_NAME);
            if (EntityPlayer.Player.actor.actorState.OnRiding)
            {
                target = EntityPlayer.Player.rideManager.ride.boneController.GetBoneByName(ACTBoneController.SLOT_CAMERA_NAME);
            }
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        public static void SetMainCameraToPlayer()
        {
            GameMain.GlobalManager.CameraManager.GetInstance().SetMainCameraToPlayer();
        }

        public static void ResetMainCameraFromPlayer()
        {
            GameMain.GlobalManager.CameraManager.GetInstance().ResetMainCameraFromPlayer();
        }

        public static void SetMainCameraMode(int mode)
        {
            MogoMainCamera mainCamera = GameMain.GlobalManager.CameraManager.GetInstance().mainCamera;
            if (mainCamera == null) return;
            if (mode == 0)
            {
                mainCamera.cameraCom.cullingMask = mainCamera.defaultCullingMask;
            }
            else if (mode == 1)
            {
                mainCamera.cameraCom.cullingMask = mainCamera.defaultCullingMask - (1 << PhysicsLayerDefine.LAYER_ACTOR)
                    - (1 << PhysicsLayerDefine.LAYER_TRIGGER) - (1 << PhysicsLayerDefine.LAYER_BLOOM);
            }
            else if (mode == 2)
            {
                mainCamera.cameraCom.cullingMask = 0;
            }
        }

        public static string CreateMD5(string str)
        {
            return MD5Utils.CreateMD5(str);
        }

        public static void LoadCG1v1()
        {
            PlayerSkillPreviewManager.GetInstance().LoadCG1v1();
        }

        //=========================SDK 登录==================================================
        public static void LoginSDK(LuaFunction loginSuccessFunctionxFunction, LuaFunction loginResultCB)
        {
            Action<List<string>> loginCB = (dataList) =>
            {
                if (loginSuccessFunctionxFunction != null)
                {
                    loginSuccessFunctionxFunction.BeginPCall();
                    loginSuccessFunctionxFunction.Push(dataList[0]);
                    loginSuccessFunctionxFunction.Push(dataList[1]);
                    loginSuccessFunctionxFunction.Push(dataList[2]);
                    loginSuccessFunctionxFunction.Push(dataList[3]);
                    loginSuccessFunctionxFunction.Push(dataList[4]);
                    loginSuccessFunctionxFunction.Push(dataList[5]);
                    loginSuccessFunctionxFunction.PCall();
                    loginSuccessFunctionxFunction.EndPCall();
                    loginSuccessFunctionxFunction.Dispose();
                }
            };
            Action<int> loginResult = (code) =>
            {
                if (loginResultCB != null)
                {
                    loginResultCB.BeginPCall();
                    loginResultCB.Push(code);
                    loginResultCB.PCall();
                    loginResultCB.EndPCall();
                    loginResultCB.Dispose();
                }
            };

            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.Login(loginCB, loginResult);
        }

        public static string StrToMd5String(string str)
        {

            MD5 md = new MD5CryptoServiceProvider();

            byte[] b = md.ComputeHash(System.Text.Encoding.Default.GetBytes(str));

            var md5str = BitConverter.ToString(b).Replace("-", "");

            return md5str.ToLower();

        }

        public static void OnLoginRspSDK(string jsonData, LuaFunction loginResultCB)
        {
            LoggerHelper.Info(string.Format("[AndroidSdk] Login OnLoginRspSDK :{0}", jsonData));
            Action<string> loginResult = (code) =>
            {
                if (loginResultCB != null)
                {
                    loginResultCB.BeginPCall();
                    loginResultCB.Push(code);
                    loginResultCB.PCall();
                    loginResultCB.EndPCall();
                    loginResultCB.Dispose();
                }
            };
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.SetLoginDataToSDK(jsonData, loginResult);
        }

        public static void SetSwitchAccountCB(LuaFunction switchAccountCB)
        {
            LoggerHelper.Info("[AndroidSdk] Login SetSwitchAccountCB ");
            Action switchAccountResult = () =>
            {
                if (switchAccountCB != null)
                {
                    switchAccountCB.BeginPCall();
                    switchAccountCB.PCall();
                    switchAccountCB.EndPCall();
                    switchAccountCB.Dispose();
                }
            };
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.SetSwitchAccountCB(switchAccountResult);
        }

        public static void ShowSDKLog(string functionName, string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime)
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.ShowSDKLog(functionName, roleId, roleName, roleLevel, vipLevel, serverId, serverName, diamond, guildName, createTime);
        }


        public static void onBuyItemLog(string functionName, string roleId, string roleName,
            string roleLevel, string vipLevel, string serverId, string serverName,
            string diamond, string guildName, string createTime, string itemPrice, string itemAction, string itemCount,
            string itemName, string itemDes, bool isPayFromCharge, bool isGain)
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.onBuyItemLog(functionName, roleId, roleName, roleLevel,
                vipLevel, serverId, serverName, diamond, guildName, createTime, itemPrice, itemAction, itemCount,
                itemName, itemDes, isPayFromCharge, isGain);
        }

        public static void BuyMarkt(string orderId, string roleId, string roleName, string roleLevel, string vipLevel,
            string serverId, string serverName, string shopMoney, string shopCount, string shopName,
            string shopType, string shopId, string shopDes, string shopRate, string shopCallbackUrl)
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.BuyMarkt(orderId, roleId, roleName, roleLevel, vipLevel,
             serverId, serverName, shopMoney, shopCount, shopName,
             shopType, shopId, shopDes, shopRate, shopCallbackUrl);
        }

        public static int GetSDKInitState()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetSDKInitState();
        }

        public static void LoginOutSDK()
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.LoginOutSDK();
        }

        public static String GetChannelId()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
        }

        public static String GetGameId()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetGameId();
        }

        public static String GetAppId()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetAppId();
        }

        public static String GetUserId()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetUserId();
        }

        public static String GetIPAddress()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetIPAddress();
        }

        public static void OnServerCloseLogin(string jsonData)
        {
            GameLoader.PlatformSdk.PlatformSdkMgr.Instance.ParsingJsonData(jsonData);
        }

        //======================================日志接口==========================================================================
        public static void UploadInfo()
        {
            UploadLogManager.GetInstance().UploadInfo();
        }

        public static void SetUploadLogHead(string head)
        {
            GameLoader.Mgrs.UploadLogMgr.GetInstance().ContentHead = head;
        }

        public static void SetUploadLogServer(string server)
        {
            GameLoader.Mgrs.UploadLogMgr.GetInstance().UpdateUrl(server);
        }

        public static void UploadLog(string[] data)
        {
            UploadLogManager.GetInstance().UploadLog(data);
        }

        public static void CSGM(string cmd)
        {
            GMManager.GetInstance().ProcessCommand(cmd);
        }

        private static string[] m_tempLogTexts;
        private static int PAGE_SIZE = 50;
        private static int m_currPage;
        private static int m_totalPage;

        public static void GetTodayLogText()
        {
            if (File.Exists(LoggerHelper.LogWriter.LogFilePath))
            {
                var fileInfo = new FileInfo(LoggerHelper.LogWriter.LogFilePath);
                long topSize = 1024 * 1024 * 100;
                if (fileInfo.Length > topSize)
                {
                    m_tempLogTexts = new[] { "Log file over size: " + LoggerHelper.LogWriter.LogFilePath + " \nsize: " + fileInfo.Length / (1024 * 1024) + "M" };
                }
                else
                {
                    var tempPath = LoggerHelper.LogWriter.LogFilePath + ".temp";
                    File.Copy(LoggerHelper.LogWriter.LogFilePath, tempPath, true);
                    m_tempLogTexts = File.ReadAllLines(tempPath);
                    File.Delete(tempPath);
                }
                m_totalPage = m_tempLogTexts.Length / PAGE_SIZE;
                m_currPage = m_totalPage;
            }
        }

        public static int GetCurrPage()
        {
            return m_currPage;
        }

        public static int GetTotalPage()
        {
            return m_totalPage;
        }

        public static void LastPage()
        {
            m_currPage--;
            if (m_currPage < 0)
                m_currPage = 0;
        }

        public static void NextPage()
        {
            m_currPage++;
            if (m_currPage > m_totalPage)
                m_currPage = m_totalPage;
        }

        public static string GetCurrPageContent()
        {
            var index = m_currPage * PAGE_SIZE;
            var end = index + PAGE_SIZE;
            end = end > m_tempLogTexts.Length ? m_tempLogTexts.Length : end;
            var sb = new StringBuilder();
            for (int i = index; i < end; i++)
            {
                sb.AppendLine(m_tempLogTexts[i]);
            }
            return sb.ToString();
        }

        public static void ClearLog()
        {
            m_tempLogTexts = null;
        }

        public static void OpenTodayLogFile()
        {
            if (File.Exists(LoggerHelper.LogWriter.LogFilePath))
                Application.OpenURL(LoggerHelper.LogWriter.LogFilePath);
        }

        public static void WriteInfoLog(string msg)
        {
            LoggerHelper.Info(msg);
        }
        
        public static void SetPauseFps(int key, bool isPause)
        {
            FPSUtil.SetPauseState(key, isPause);
        }

        public static void InitPauseFps(int[] keys, bool isPause)
        {
            FPSUtil.InitPauseState(keys, isPause);
        }

        public static float GetFps()
        {
            return FPSUtil.fps;
        }

        public static float GetAverageFps()
        {
            return FPSUtil.averageFps;
        }

        public static bool GetLowFps()
        {
            return FPSUtil.lowFps;
        }

        public static void SetLowFps(bool flag)
        {
            FPSUtil.lowFps = flag;
        }

        public static int GetCutout()
        {
            return GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetCutoutScreen();
        }
        
        public static int GetTargetFrameRate()
        {
            return Application.targetFrameRate;
        }
        
        public static void SetTargetFrameRate(int frameRate)
        {
            Application.targetFrameRate = frameRate;
        }
        
        public static void SetDamageHandlerRunning(bool isRunning)
        {
            PerformaceDamageHandler.isRunning = isRunning;
        }

        public static void ClearCacheBeforeLoading()
        {
            GameObjectPoolManager.GetInstance().Clear();
            ACTVisualFXManager.GetInstance().ReleaseACTVisualFx();
            ACTSystemAdapter.curACTSystemDriver.ObjectPool.ClearPool();
            GameObjectSoundManager.GetInstance().ClearPool();
        }
    }
}
