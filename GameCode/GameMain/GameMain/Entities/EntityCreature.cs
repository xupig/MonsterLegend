﻿using ACTSystem;
using GameData;
using GameLoader.Utils;
using MogoEngine.Events;
using MogoEngine.RPC;
using ShaderUtils;
using System;
using System.Collections.Generic;
using UnityEngine;
using GameMain.CombatSystem;
using Common.ServerConfig;
using Common.ExtendTools;
using GameMain.ClientConfig;
using LuaInterface;
using GameMain.GlobalManager;
using MogoEngine;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using ArtTech;
using Common.Global;

namespace GameMain
{
    public class FightAttriConfigComparer : IEqualityComparer<fight_attri_config>
    {

        public bool Equals(fight_attri_config x, fight_attri_config y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(fight_attri_config obj)
        {
            return (int)obj;
        }
    }

    public enum CreatureType
    {
        PLAYER = 1,
        MONSTER = 2,
    }

    public class EntityCreature : EntityLuaBase, IACTActorOwner
    {
        #region def属性

        public float speed { get; set; }
        public float base_speed { get; set; } //用于player的移动速度计算属性
        public UInt32 state_cell { get; set; }

        //public UInt32 target_id { get; set; }
        public UInt16 vocation { get; set; }
        public string facade { get; set; }
        public string name { get; set; }

        /// <summary>
        /// 所有人都能看到的buff
        /// 格式是 1001:10;1002:20
        /// </summary>
        private string _buff_allclients;
        public string buff_allclients 
        {
            get { return _buff_allclients; }
            set
            {
                _buff_allclients = value;
            } 
        }

        /// <summary>
        /// 自己能看到的buff
        /// 格式是 1001:10;1002:20
        /// </summary>
        private string _buff_client;
        public string buff_client
        {
            get { return _buff_client; }
            set 
            {   
                _buff_client = value;
            }
        }

        public Dictionary<uint, float> showEntityDict = new Dictionary<uint, float>();
        public List<uint> showEntityList = new List<uint>();
        private UInt32 m_target_id = 0;
        public UInt32 target_id
        {
            get { return m_target_id; }
            set
            {
                if (value != m_target_id)
                {
                    if (value == 0)
                    {
                        if (showEntityDict.ContainsKey(m_target_id))
                        {
                            showEntityDict.Remove(m_target_id);
                            showEntityList.Remove(m_target_id);
                        }
                    }
                    else
                    {
                        if (showEntityDict.ContainsKey(m_target_id))
                        {
                            showEntityDict.Remove(m_target_id);
                            showEntityList.Remove(m_target_id);
                        }
                        if (showEntityDict.ContainsKey(value))
                        {
                            showEntityDict[value] = -1;
                        }
                        else
                        {
                            showEntityDict.Add(value, -1);
                            showEntityList.Add(value);
                        }
                    }
                    m_target_id = value;
                    if (this.IsPlayer())
                    {
                        EntityPlayer.Player.lockTargetManager.SetTarget(m_target_id);
                        CallLuaSelfFunction("RefreshTargetId", m_target_id);
                    }
                }
            }
        }

        /// <summary>
        /// 角色等级
        /// </summary>
        protected UInt16 m_level;

        public virtual UInt16 level
        {
            get { return m_level; }
            set
            {
                m_level = value;
            }
        }

        private int _curHp;

        public Int32 cur_hp
        {
            get
            {
                return _curHp;
            }
            set
            {
                if (value != _curHp)
                {
                    _curHp = value;
                    if (stateManager == null)
                    {
                        return;
                    }
                    if (_curHp <= 0 && !this.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                    {
                        this.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
                    }
                    if (_curHp > 0 && this.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                    {
                        this.stateManager.SetState(state_config.AVATAR_STATE_DEATH, false);
                    }
                    if (this is EntityPlayer)
                    {
                        OnHPChange();
                    }

                    //else if (_curHp > 0 && this.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                    //{
                    //    this.stateManager.SetState(state_config.AVATAR_STATE_DEATH, false);
                    //}
                    //if (_curHp <= 0 && this is EntityPlayer)
                    //{
                    //    stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
                    //}
                }
            }
        }

        private int _maxHp;

        public Int32 max_hp
        {
            get
            {
                return _maxHp;
            }
            set
            {
                if (value != _maxHp)
                {
                    _maxHp = value;
                }
            }
        }

        public byte watch_war_status { get; set; }

        public byte tire_flag { get; set; }

        private Int16 _cur_mp = 0;
        public Int16 cur_mp 
        { 
            get {return _cur_mp; }
            set
            {
                if (_cur_mp != value)
                {
                    _cur_mp = value;
                }
            }
        }
        public Int16 max_mp { get; set; }

        public byte pk_mode { get; set; }

        private int _map_id = 0;
        public int map_id 
        {
            get { return _map_id; }
            set 
            {
                if (this.IsPlayer())
                {
                    ActorDeathController.DestroyAllDeadBody();
                }
                if (_map_id != value)
                {
                    _map_id = value;
                    if (this.IsPlayer())
                    {
                        EntityPlayer.Player.lockTargetManager.OnMapIdChange();
                    }
                }
            }
        }

        public UInt16 pk_value { get; set; }

        public UInt32 team_id { get; set; }

        public string guild_name { get; set; }

        private byte _fight_idle_state = 0;
        public byte fight_idle_state 
        {
            get { return _fight_idle_state; }
            set
            {
                if (value != _fight_idle_state)
                {
                    _fight_idle_state = value;
                    if (this.IsPlayer())
                    {
                        return;
                    }
                    if (_fight_idle_state == 0)
                    {
                        this.SetActorMode(0);
                    }
                    else if (_fight_idle_state == 1)
                    {
                        this.SetActorMode(1);
                    }
                    if (isActorLoaded)
                    {
                        this.actor.inSafeArea = _fight_idle_state == 0;
                    }
                }
            }
        }

        #endregion

        private bool isActorLoaded = false;
        private ACTActor _actor;
        public ACTActor actor {
            get { return _actor; }
            set
            {
                isActorLoaded = (value != null);
                _actor = value;
            }
        }
        public int actorID;
        public string defaultLayer = "Default";
        public float hitRadius = 0.5f;
        public float hitHeight = 2f;
        public float boxRadius = 0.5f;
        public float boxHeight = 2f;
        public float rotateSpeed = 0f;
        public bool isMovingByTouch = false;
        public CreatureType creatureType = CreatureType.PLAYER;
        public bool hasActorDeathController = false;
        private Transform _bgmSlot = null;
        protected AudioListener _audioListener = null;
        public bool aiControlling = true;
        public bool showProjection = true;
        public bool canRelease = false;
        public bool isStatic{ get; set; }
        
        public VisualFXPriority visualFXPriority = VisualFXPriority.L;

        protected Dictionary<fight_attri_config, float> attributeDict =
            new Dictionary<fight_attri_config, float>(new FightAttriConfigComparer()); //战斗属性 
        protected Dictionary<int, float> _battleAttributes = new Dictionary<int, float>();

        public SkillManager skillManager;
        public BufferManager bufferManager;
        public StateManager stateManager;
        public MoveManager moveManager;
        public EquipManager equipManager;
        public ConditionEventManager conditionEventManager;
        public TrapManager trapManager;
        public ThrowObjectManager throwObjectManager;
        public SyncPosManager syncPosManager;
        public RideManager rideManager;

        protected ActorDeathController _actorDeathController;

        public ActorDeathController actorDeathController
        {
            get
            {
                return _actorDeathController;
            }
        }

        public EntityCreature()
        {
            RegisterPropertySetDelegate(typeof (Action<float>), "speed");
            RegisterPropertySetDelegate(typeof (Action<UInt32>), "state_cell");
            RegisterPropertySetDelegate(typeof (Action<UInt16>), "level");
            RegisterPropertySetDelegate(typeof (Action<Int32>), "cur_hp");
            RegisterPropertySetDelegate(typeof (Action<Int32>), "max_hp");
            RegisterPropertySetDelegate(typeof(Action<UInt16>), "vocation");
            RegisterPropertySetDelegate(typeof(Action<string>), "facade");
            RegisterPropertySetDelegate(typeof(Action<string>), "name");
            RegisterPropertySetDelegate(typeof(Action<byte>), "faction_id");
            RegisterPropertySetDelegate(typeof(Action<string>), "buff_client");
            RegisterPropertySetDelegate(typeof(Action<string>), "buff_allclients");
            RegisterPropertySetDelegate(typeof(Action<byte>), "watch_war_status");
            RegisterPropertySetDelegate(typeof(Action<Int16>), "cur_mp");
            RegisterPropertySetDelegate(typeof(Action<Int16>), "max_mp");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "owner_id");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "fabao_eid");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "pet_eid");
            RegisterPropertySetDelegate(typeof(Action<byte>), "tire_flag");
            RegisterPropertySetDelegate(typeof(Action<byte>), "pk_mode");
            RegisterPropertySetDelegate(typeof(Action<UInt16>), "pk_value");
            RegisterPropertySetDelegate(typeof(Action<byte>), "fight_idle_state");
            RegisterPropertySetDelegate(typeof(Action<UInt32>), "team_id");
            RegisterPropertySetDelegate(typeof(Action<string>), "guild_name");
            RegisterPropertySetDelegate(typeof(Action<bool>), "isStatic");
        }

        public Vector3 face
        {
            get
            {
                Vector3 result = Vector3.zero;
                var transform = this.GetTransform();
                if (transform != null)
                {
                    result = (transform.eulerAngles*0.5f);
                }
                return result;
            }
            set
            {
                this.SetRotation(Quaternion.Euler(value.x*2, value.y*2, value.z*2));
            }
        }

        protected bool _collidable = true;

        public bool collidable
        {
            get { return _collidable; }
            set
            {
                if (_collidable != value)
                {
                    _collidable = value;
                    RefreshActorCollidable();
                }
            }
        }

        public void SetActorLayer(string layerName, bool includeChildren = false)
        {
            if (actor != null)
            {
                actor.SetLayer(layerName, includeChildren);
            }
        }

        public string GetActorLayer()
        {
            return actor.GetLayer();
        }

        public void RefreshActorCollidable()
        {
            if (_collidable)
            {
                if (entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO)
                {
                    SetActorLayer(defaultLayer, true);
                }
                else
                {
                    SetActorLayer(defaultLayer);
                }
            }
            else
            {
                SetActorLayer(Common.ClientConfig.PhysicsLayerDefine.Ghost);
            }
        }

        protected int _qualitySettingValue = 0; //质量设置(特效资源/模型资源)

        public int qualitySettingValue
        {
            get { return _qualitySettingValue; }
        }

        private bool _checkCrashWall = false;

        public bool checkCrashWall
        {
            get { return _checkCrashWall; }
            set { _checkCrashWall = value; }
        }
        //可视(自己看自己是可见的，别人看自己是隐藏的)
        protected bool _visible = true;
        public bool visible
        {
            set
            {
                if (_visible != value)
                {
                    bool changed = CheckVisibleChanged(value, _show);
                    _visible = value;
                    if (changed)
                    {
                        OnVisibleChanged();
                    }
                }
            }
        }

        //绝对可视(自己都看不见自己)
        protected bool _show = true;
        public bool show
        {
            set
            {
                if (_show != value)
                {
                    bool changed = CheckVisibleChanged(_visible, value);
                    _show = value;
                    if (changed)
                    {
                        OnVisibleChanged();
                    }
                }
            }
        }

        protected bool _actorVisible = true;
        public bool actorVisible
        {
            get { return _actorVisible; }
            set
            {
                if (_actorVisible != value)
                {
                    _actorVisible = value;
                    RefreshActorVisible();
                    if (_actorVisible && actor == null)
                    {
                        CreateActor();
                    }
                }
            }
        }

        protected double _curHPRate = 1;
        public double curHPRate
        {
            get { return Math.Round(cur_hp/(double) max_hp, 2); }
        }

        public byte faction_id { get; set; }
        public byte clientCamp = 0;
        public byte realCamp
        {
            get 
            {
                if (!this.skillManager.inServerMode)
                {
                    return clientCamp;
                }
                else
                {
                    return faction_id;
                }
            }
        }

        protected bool _isAlive = true;
        public bool isAlive
        {
            get { return _isAlive; }
        }
        //锁定模式目标死亡切换镜头处理
        protected float _clearTargetTime = 0;
        public bool clearTarget
        {
            get
            {
                if (!_isAlive && _clearTargetTime != 0 && RealTime.time > _clearTargetTime)
                {
                    return true;
                }
                return false;
            }
        }

        protected Vector3 _pickUpSkillPosition = Vector3.zero;
        public Vector3 pickUpSkillPosition
        {
            get { return _pickUpSkillPosition; }
            set { _pickUpSkillPosition = value; }
        }

        private void OnVisibleChanged()
        {
            RefreshActorVisible();
        }

        virtual protected bool CheckVisibleChanged(bool newVisible, bool newShow)
        {
            bool oldResult = _visible && _show;
            bool newResult = newVisible && newShow;
            return oldResult != newResult;
        }

        protected virtual void RefreshActorVisible()
        {
            if (actor != null && actor.gameObject != null)
            {
                actor.visible = CanDisplay() && _actorVisible;
            }
        }
        
        [NoToLua]
        public bool IsRidingValid()
        {
            if (rideManager != null && rideManager.ride != null)
            {
                return true;
            }
            return false;
        }

        public bool IsRiding()
        {
            if (rideManager != null)
            {
                return rideManager.isRiding;
            }
            return false;
        }

        private bool _inAttackState = false;
        public bool inAttackState
        {
            get { return _inAttackState; }
            set
            {
                if (value != _inAttackState)
                {
                    _inAttackState = value;
                    SetAttr("inAttackState", _inAttackState);
                }
            }
        }

        //是否开启待机定时启动自动战斗
        public bool isOpenAutoFightCheck = false;

        public override void OnEnterWorld()
        {
            RefreshNeedHideMonsterInClient();
            RefreshNeedHideInClient();

            this.conditionEventManager = CreatureManagerObjectPool.CreateConditionEventManager(this);
            this.moveManager = CreatureManagerObjectPool.CreateMoveManager(this);
            this.stateManager = CreatureManagerObjectPool.CreateStateManager(this);
            this.skillManager = CreatureManagerObjectPool.CreateSkillManager(this);
            this.bufferManager = CreatureManagerObjectPool.CreateBufferManager(this);
            if (this.entityType.IndexOf("Avatar") >= 0 || this.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
            {
                equipManager = CreatureManagerObjectPool.CreateEquipManager(this);
            }
            base.OnEnterWorld();
            var accordingMode = CombatSystem.AccordingMode.AccordingEntity;
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY || entityType == EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR
                || entityType == EntityConfig.ENTITY_TYPE_NAME_PET || entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO || entityType == EntityConfig.ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR)
            {
                accordingMode = CombatSystem.AccordingMode.AccordingActor;
            }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP || entityType == EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT)
            {
                accordingMode = CombatSystem.AccordingMode.AccordingActor;
            }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY || entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
            {
                monsterType = monster_helper.GetMonsterType(this.monsterId);
                hitRadius = monster_helper.GetBeHitRadius(this.monsterId);
                hitHeight = monster_helper.GetBeHitHeight(this.monsterId);
            }
            this.moveManager.accordingMode = accordingMode;
            this.moveManager.defaultAccordingMode = accordingMode;
            //_qualitySettingValue = (int) VisualFXQuality.L;
            _lowerRate = float.Parse(global_params_helper.GetGlobalParam(52));
            ProjectionManager.GetInstance().OnCreatureEnterWorld(this);
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            ProjectionManager.GetInstance().OnCreatureLeaveWorld(this);
            CreatureManagerObjectPool.ReleaseMoveManager(this.moveManager);
            CreatureManagerObjectPool.ReleaseStateManager(this.stateManager);
            CreatureManagerObjectPool.ReleaseSkillManager(this.skillManager);
            CreatureManagerObjectPool.ReleaseBufferManager(this.bufferManager);
            if (equipManager != null) { CreatureManagerObjectPool.ReleaseEquipManager(this.equipManager); }
            if (rideManager != null) { CreatureManagerObjectPool.ReleaseRideManager(rideManager); }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_NPC){ PerformaceManager.GetInstance().GetNPCHandler().OnNPCLeaveWorld(this);}
            DestroyActor();
            if (npcIcon != null)
            {
                GameObject.Destroy(npcIcon);
            }
            if (this.trapManager != null) { CreatureManagerObjectPool.ReleaseTrapManager(this.trapManager); }
            if (this.throwObjectManager != null) { CreatureManagerObjectPool.ReleaseThrowObjectManager(this.throwObjectManager);}
            if (this.conditionEventManager != null) { CreatureManagerObjectPool.ReleaseConditionEventManager(this.conditionEventManager); }
            if (syncPosManager != null) { syncPosManager.Release(); }
            if (this.peri_eid != 0)
            {
                MogoWorld.DestroyEntity(this.peri_eid);
            }
            if (trigger != null)
            {
                EventDispatcher.RemoveEventListener<uint>(Common.Events.TouchEvents.ON_TOUCH_ENTITY, OnTouchEntity);
                trigger = null;
                selectTriggerExist = false;
            }
        }

        public override void OnReuse(uint id)
        {
            base.OnReuse(id);
            if (!_loadingActor && this.actor == null)
            {
                CreateActor();
            }
            OnRelive();
        }

        public override void OnRelease()
        {
            base.OnRelease();
            DestroyActor();
            ClearData();
        }

        private void ClearData()
        {
            _actorVisible = true;
            m_target_id = 0;
            _fight_idle_state = 0;
            this.id = 0;
        }

        bool isCellAttached = false;
        public override void OnCellAttached()
        {
            isCellAttached = true;
            isInit = false;
            CheckCellAttachedAndActorLoaded();
        }

        bool isInit = false;
        public void CheckCellAttachedAndActorLoaded()
        {
            if (this.IsPlayer())
            {
                if (!isInit && isCellAttached && actor != null)
                {
                    bufferManager.ClearAllServerBuffer();
                    AddBuffOnEnterWorld();
                    isInit = true;
                }
            }
            else 
            {
                if (!isInit && actor != null)
                {
                    AddBuffOnEnterWorld();
                    isInit = true;
                }
            }
        }

        public override void SetPosition(Vector3 newPosition)
        {
            base.SetPosition(newPosition);
        }

        public override void SetRotation(Quaternion newRotation)
        {
            base.SetRotation(newRotation);
            if (isActorLoaded)
            {
                this.actor.rotation = newRotation;
            }
        }

        public override Transform GetTransform()
        {
            if (isActorLoaded)
            {
                return actor.transform;
            }
            else
            {
                return base.GetTransform();
            }
        }

        public GameObject GetGameObject()
        {
            if (isActorLoaded)
            {
                return this.actor.gameObject;
            }
            else
            {
                return null;
            }
        }

        public override void Teleport(Vector3 newPosition)
        {
            //LoggerHelper.Error("Teleport: " + name + " " + newPosition + " " + this.entityType);
            base.Teleport(newPosition);
            if (isActorLoaded)
            {
                //newPosition.y = this.actor.position.y;
                this.actor.position = newPosition;
                this.actor.ToGround();
                //BoxCollider bc = this.actor.gameObject.AddComponent<BoxCollider>();
                if (this.peri_eid != 0)
                {
                    //女神直接同步位移
                    EntityCreature peri = MogoWorld.GetEntity(this.peri_eid) as EntityCreature;
                    if (peri != null)
                    {
                        peri.Teleport(newPosition);
                    }
                }
            }
        }

        //单纯用来模型移动位置
        public void TeleportClient(Vector3 newPosition)
        {
            //LoggerHelper.Error("Teleport: " + name + " " + newPosition + " " + this.entityType);
            base.Teleport(newPosition);
            if (isActorLoaded)
            {
                this.actor.position = newPosition;
            }
        }

        public virtual float PlaySkillByPos(int pos)
        {
            try
            {
                //LoggerHelper.Error("PlaySkillByPos :" + UnityPropUtils.realtimeSinceStartup);
                int skillId = this.skillManager.PlaySkillByPos(pos);
                if (skillId == 0) return 0;
                float commonCD = this.skillManager.GetCommonCD() - RealTime.time;
                return commonCD;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PlaySkillByPos:" + ex.Message + "\n" + ex.StackTrace);
                return -1;
            }
        }

        public virtual void PlaySkill(int skillId)
        {
            this.skillManager.PlaySkill(skillId);
        }

        public void PlaySkillByCreateRoleAvatar(int skillId)
        {
            this.skillManager.PlaySkillBySkillPlayer(skillId);
        }

        public void PlaySkillBySkillPlayer(int skillId)
        {
            //先复位位置
            EntityPlayer.ShowPlayer.actor.position = PlayerSkillPreviewManager.GetInstance().initPos;
            this.skillManager.PlaySkillBySkillPlayer(skillId);
        }

        public void SetDramaSkill(int mode)
        {
            this.skillManager.SetDramaSkill(mode == 1);
        }

        public float GetActionCutTimeByPos(int pos)
        {
            int skillId = this.skillManager.GetSkillIDByPos(pos);
            if (skillId == 0) return 0;
            return this.skillManager.GetActionCutTime(skillId);
        }

        public void ReleaseSkillByPos(int pos)
        {
            this.skillManager.ReleaseSkillByPos(pos);
        }

        public bool SkillCanPlayByPos(int pos)
        {
            int skillId = this.skillManager.GetSkillIDByPos(pos);
            if (skillId == 0) return false;
            return this.skillManager.CanDo(skillId) == 0;
        }

        public int GetSkillAIRangeByPos(int pos)
        {
            int skillId = this.skillManager.GetSkillIDByPos(pos);
            if (skillId == 0) return -1;
            return this.skillManager.GetSkillAIRange(skillId);
        }

        public bool IsStickSkill(int pos)
        {
            int skillId = this.skillManager.GetSkillIDByPos(pos);
            if (skillId == 0) return false;
            return this.skillManager.IsStickSkill(skillId);
        }

        public void Move(Vector3 destination, float speed, LuaFunction luafunc, float endDistance)
        {
            this.moveManager.Move(destination, speed, delegate(EntityCreature creature)
            {
                LuaFacade.CallLuaFunction(luafunc, creature != null ? creature.luaEntity : null);
            }, endDistance);
        }

        public void MoveByLine(Vector3 destination, float speed, LuaFunction luafunc, float endDistance, bool returnReady)
        {
            if (returnReady) ReturnReady();
            this.moveManager.MoveByLine(destination, speed, delegate(EntityCreature creature)
            {
                LuaFacade.CallLuaFunction(luafunc, creature != null ? creature.luaEntity : null);
            }, endDistance);
        }

        public void ReturnReady()
        {
            if (this.actor)
            {
                this.actor.animationController.ReturnReady();
            }
        }

        public void MoveByDirection(uint targetID, float speed, float moveTime = 1, bool right = false)
        {
            this.moveManager.MoveByDirection(targetID, speed, moveTime, right);
        }

        public void MoveToDirection(Vector3 targetPosition, float speed, float moveTime, bool returnReady)
        {
            if (returnReady) ReturnReady();
            this.moveManager.MoveToDirection(targetPosition, speed, moveTime);
        }

        public void MoveByPathFly(float speed, LuaFunction luafunc, float endDistance)
        {
            this.moveManager.MoveByPathFly(speed, null, delegate(EntityCreature creature)
            {
                LuaFacade.CallLuaFunction(luafunc, creature != null ? creature.luaEntity : null);
            }, endDistance);
        }

        public void PauseFly(bool state)
        {
            this.moveManager.PauseFly(state);
        }

        #region 隐藏显示相关
        public bool isHideEntity = false; //处理客户端不显示的实体
        private void RefreshNeedHideInClient()
        {
            int avatarCount = 0;
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !MogoWorld.IsClientEntity(entity) && entity.id != this.id)
                {
                    if (!(entity as EntityCreature).isHideEntity)
                    {
                        avatarCount++;
                    }
                }
            }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
            {
                //超过同屏人数标记实体
                if (avatarCount >= PlayerSettingManager.maxScreenPlayerCount)
                {
                    isHideEntity = true;
                }
                else
                {
                    isHideEntity = false;
                }
            }
        }
        #endregion

        #region 同屏怪物限制显示相关
        public bool isHideMonster = false; //处理客户端不显示的实体
        private void RefreshNeedHideMonsterInClient()
        {
            int monsterCount = 0;
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                {
                    if (!(entity as EntityCreature).isHideMonster)
                    {
                        monsterCount++;
                    }
                }
            }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
            {
                //超过同屏怪物标记实体
                if (monsterCount >= PlayerSettingManager.maxScreenMonsterCount)
                {
                    isHideMonster = true;
                }
                else
                {
                    isHideMonster = false;
                }
            }
        }
        #endregion

        #region 传送吟唱
        public bool isTeleportSing = false;
        public void SetTeleportSingState(bool isSing)
        {
            isTeleportSing = isSing;
        }
        #endregion

        #region 采集吟唱
        public bool isCollectSing = false;
        #endregion

        #region Monster属性
        public int monsterId = 0;
        public int monsterType = 0;
        public bool isClientControlServerEntity = false;
        private UInt32 _delegateEid = 0;
        public UInt32 delegateEid
        {
            get { return _delegateEid; }
            set 
            {
                if (_delegateEid != value)
                {
                    if (value == EntityPlayer.Player.id)
                    {
                        isClientControlServerEntity = true;
                        if (syncPosManager == null)
                        {
                            syncPosManager = new SyncPosManager(this);
                        }
                        syncPosManager.OnEnterFollower();
                    }
                    else
                    {
                        isClientControlServerEntity = false;
                        if (syncPosManager != null)
                        {
                            syncPosManager.OnLeaveFollower();
                        }
                    }
                    _delegateEid = value;
                }                
            }
        }
        #endregion

        #region 技能生成实体
        public uint castEntityId = 0;
        public int spawnObjectId = 0;
        public Vector3 spawnEntityFace;
        public uint assignId = 0;
        #endregion

        #region 守护灵
        public int eudemonSkillId = 0;
        #endregion

        #region 女神
        private uint _peri_eid = 0;
        public uint peri_eid
        {
            get { return _peri_eid; }
            set { _peri_eid = value; }
        }
        #endregion

        #region 法宝
        public UInt32 fabao_eid { get; set; }
        public bool setFabao = false;
        public bool initFabao = false;
        #endregion

        #region 霸体
        public int superArmorLevel = 0;
        #endregion

        #region 战斗属性

        public virtual float GetAttribute(fight_attri_config attributeId)
        {
            if (attributeDict.ContainsKey(attributeId))
            {
                return attributeDict[attributeId];
            }
            return 0;
        }

        public float GetBattleAttribute(int attributeId)
        {
            if (_battleAttributes.ContainsKey(attributeId))
            {
                return _battleAttributes[attributeId];
            }
            return 0;
        }

        public Dictionary<int, float> GetBattleAttributes()
        {
            return this._battleAttributes;
        }

        public void SetBattleAttributes(int[] keys, float[] values)
        {
            if (keys.Length == 0) return;
            if (keys.Length != values.Length)
            {
                LoggerHelper.Error("SetBattleAttributes keys.Length != values.Length");
                return;
            }
            for (int i = 0; i < keys.Length; i++)
            {
                SetBattleAttribute(keys[i], values[i]);
            }
            ResetHPMPAndRage();
        }

        public void SetBattleAttribute(int key, float value)
        {
            this._battleAttributes[key] = value;
            OnAttrChanged(key, value);
        }

        protected void OnAttrChanged(int key, float value)
        {
            if (key == battle_attri_config.ATTRI_ID_MOVE_SPEED_RATE || key == battle_attri_config.ATTRI_ID_MOVE_SPEED_REDUCE_RATE)
            {
                this.moveManager.UpdateSpeedRate();
                this.moveManager.UpdateActorSpeedRate();
            }
        }

        private void ResetHPMPAndRage()
        {
            int maxHP = 0;
            int curHP = 0;
            //玩家自己用服务器属性数据
            if (this._battleAttributes.ContainsKey(battle_attri_config.ATTRI_ID_MAX_HP))
            {
                maxHP = (int)this._battleAttributes[battle_attri_config.ATTRI_ID_MAX_HP];
                curHP = (int)this._battleAttributes[battle_attri_config.ATTRI_ID_CUR_HP];
            }
            else
            {
                maxHP = this.max_hp;
                curHP = this.cur_hp;
            }
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.max_hp, maxHP);
            MogoWorld.SynEntityAttr(this, EntityPropertyDefine.cur_hp, curHP);
        }
        #endregion

        public void StopMove()
        {
            this.moveManager.Stop();
        }

        public void StopMoveWithoutCallback()
        {
            this.moveManager.Stop(false);
        }

        public void PlayAction(int actionID)
        {
            if (this.actor == null) return;
            this.actor.animationController.PlayAction(actionID);
        }

        public void SetApplyRootMotion(bool state)
        {
            this.actor.animationController.SetApplyRootMotion(state);
        }

        public void SetActorSortingOrder(int sortOrder = -1)
        {
            if (isActorLoaded)
            {
                this.actor.fxSortingOrder = sortOrder;
            }
        }

        public void PlaySfx(int fxID, float duration)
        {
            ACTVisualFXManager.GetInstance().PlayACTVisualFX(fxID, actor, duration, 0);
        }

        public Transform GetBoneByName(string slotName)
        {
            return this.actor.boneController.GetBoneByName(slotName);
        }

        public bool ContainsBuffer(int buffId)
        {
            return this.bufferManager.ContainsBuffer(buffId);
        }

        public bool AddBuffer(int buffId, float duration = 0)
        {
            return this.bufferManager.AddBuffer(buffId, duration);
        }

        public virtual void RemoveBuffer(int bufferID)
        {
            bufferManager.RemoveBuffer(bufferID);
        }

        public virtual void ClearAllClientBuffers()
        {
            bufferManager.ClearAllClientBuffers();
        }

        public virtual uint GetActorOwnerID()
        {
            return this.id;
        }

        static readonly int default_model_id = 2410;

        public void SetActor(int id)
        {
            if (this.actorID == id) return;
            this.actorID = id;
            if (isHideMonster) this.actorID = default_model_id; //不显示的模型
            DestroyActor();
            CreateActor();
        }

        [NoToLua]
        public bool IsDefaultModel()
        {
            return (this.actorID == default_model_id);
        }

        bool _loadingActor = false;
        public void CreateActor()
        {
            int loadActorID = actorID;
            _loadingActor = true;
            isActorLoaded = false;
            GameObjectPoolManager.GetInstance().CreateActorGameObject(loadActorID, (actor) =>
            {
                _loadingActor = false;
                if (actor == null)
                {
                    LoggerHelper.Error("CreateActor error by " + actorID);
                    return;
                }
                if (this.isInWorld == false || loadActorID != this.actorID)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actorID, actor.gameObject);
                    return;
                }
                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                {
                    SkinnedMeshRenderer smr = actor.GetComponentInChildren<SkinnedMeshRenderer>();
                    if (smr)
                    {
                        smr.gameObject.SetActive(false);
                        TimerHeap.AddTimer(200, 0, () => 
                        {
                            if (smr != null)
                            {
                                smr.gameObject.SetActive(true);
                            }
                        });
                    }
                }
                this.actor = actor;
                this.actor.owner = this;
                RefreshActorCollidable();
                OnActorLoaded();
            }, false);
        }

        public virtual void DestroyActor()
        {
            if (actor == null || actor.gameObject == null)
            {
                actor = null;
                return;
            }
            animationEvent = null;

            if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO)
            {
                if (MogoWorld.Entities.ContainsKey(owner_id))
                {
                    EntityCreature owner = MogoWorld.Entities[owner_id] as EntityCreature;
                    if (owner != null && owner.actor != null && owner.IsPlayer())
                    {
                        owner.initFabao = false;
                    }
                }
            }

            if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR)
            {
                if (MogoWorld.Entities.ContainsKey(fabao_eid))
                {
                    EntityCreature fabao = MogoWorld.Entities[fabao_eid] as EntityCreature;
                    if (fabao != null && fabao.actor != null)
                    {
                        fabao.actor.transform.SetParent(null);
                    }
                }
                this.initFabao = false;
            }
            
            this.actor.actorState.OnAirChange = null;
            this.actor.actorState.OnGroundChange = null;
            if (actorDeathController != null)
            {
                actorDeathController.DestroyActor(canRelease);
            }
            else
            {
                if (canRelease)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actorID, actor.gameObject);
                }
                else
                {
                    GameObject.Destroy(actor.gameObject);
                }
            }
            actor = null;

            if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR)
            {
                AudioManager.GetInstance().SetAudioListener(true);
            }
        }

        private void OnGroundChange(bool onGround)
        {
            if (onGround)
            {
                this.conditionEventManager.Trigger(ConditionEventType.OnGround);
                if (this is EntityPlayer)
                {
                    EntityPlayer.Player.RpcCall("buff_event", (byte)ConditionEventType.OnGround);
                    if (this.InState(state_config.AVATAR_STATE_GLIDE))
                    {
                        this.skillManager.PlaySkill(global_params_helper.GetGlideLandingSkillId()[vocation]);
                    }
                }
            }
            if (onGround)
            {

                int action = actor.animationController.LastAction;
                //Debug.LogError("onGround action:"+action);
            }
        }

        private int lastAction = -1;
        private void OnAirChange(bool onAir)
        {

        }

        public virtual void OnActorLoaded()
        {
            if (isInWorld)
            {
                Vector3 pos = this.position;
                actor.position = this.position;
                actor.rotation = this.rotation;
                if (this.equipManager != null) equipManager.OnActorLoaded();
                actor.gameObject.name = this.entityType + " - " + this.id;
                this.actor.ToGround();
                _audioListener = this.actor.gameObject.GetComponent<AudioListener>();
                if (_audioListener != null)
                {
                    _audioListener.enabled = false;
                }
                //this.actor.actorState.IsReady = true;
                this.boxHeight = this.actor.actorController.GetHeight();
                this.actor.actorController.isStatic = isStatic;
                this.SetGravity(global_params_helper.defaultGravity);
                this.position.y = actor.position.y;
                ResetCityState();
                this.actor.actorState.OnAirChange = OnAirChange;
                this.actor.actorState.OnGroundChange = OnGroundChange;
                InspectorSetting.InspectingTool.Inspect(new InspectingCreature(this), this.actor.gameObject);
                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR)
                {
                    BecomeShadowTarget(5, false);
                    this.actor.actorState.IsReady = false;
                    //创角实体加上audioListener
                    if (_audioListener == null)
                    {
                        _audioListener = this.actor.gameObject.AddComponent<AudioListener>();
                        _audioListener.enabled = true;
                        AudioManager.GetInstance().SetAudioListener(false);
                    }
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_UISHOWENTITY)
                {
                    GameObject.DontDestroyOnLoad(this.actor.gameObject);
                    actor.position = pos;
                    this.SetGravity(0);
                    this.moveManager.accordingMode = AccordingMode.AccordingActor;
                    SetActorLayer("UI", true);
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
                {
                    if (EntityPlayer.Player != null && owner_id == EntityPlayer.Player.id)
                    {
                        //玩家自己的宠物宝宝不删除
                        GameObject.DontDestroyOnLoad(this.actor.gameObject);
                    }
                    else
                    {
                        EntityCreature owner = null;
                        if (MogoWorld.Entities.ContainsKey(owner_id))
                        {
                            owner = MogoWorld.Entities[owner_id] as EntityCreature;
                        }
                        actorVisible = !PlayerSettingManager.hideOtherPet;
                        actor.isHideFX = PlayerSettingManager.hideOtherSkillFx;
                        bool hide = false;
                        if (owner != null)
                        {
                            hide = owner.isHideEntity;
                        }
                        if (PlayerSettingManager.hideOtherPlayer || hide)
                        {
                            actorVisible = false;
                        }
                    }
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO)
                {
                    actor.actorController.needFixHeight = true;
                    this.actor.ToGround();
                    if (MogoWorld.Entities.ContainsKey(owner_id))
                    {
                        EntityCreature owner = MogoWorld.Entities[owner_id] as EntityCreature;
                        if (owner != null && owner.actor != null && !owner.initFabao)
                        {
                            actor.transform.SetParent(owner.actor.transform, true);
                            actor.transform.localPosition = Vector3.zero;
                            actor.transform.localEulerAngles = Vector3.zero;
                            actor.actorController.controller.enabled = false;
                            owner.initFabao = true;
                        }
                    }

                    if (owner_id != EntityPlayer.Player.id)
                    {
                        actorVisible = !PlayerSettingManager.hideOtherFabao;
                        actor.isHideFX = PlayerSettingManager.hideOtherSkillFx;

                        EntityCreature owner = null;
                        if (MogoWorld.Entities.ContainsKey(owner_id))
                        {
                            owner = MogoWorld.Entities[owner_id] as EntityCreature;
                        }
                        bool hide = false;
                        if (owner != null)
                        {
                            hide = owner.isHideEntity;
                        }
                        if (PlayerSettingManager.hideOtherPlayer || hide)
                        {
                            actorVisible = false;
                        }
                    }
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR)
                {
                    if (MogoWorld.Entities.ContainsKey(fabao_eid))
                    {
                        EntityCreature fabao = MogoWorld.Entities[fabao_eid] as EntityCreature;
                        if (fabao != null && fabao.actor != null && !this.initFabao)
                        {
                            fabao.actor.transform.SetParent(actor.transform, true);
                            fabao.actor.transform.localPosition = Vector3.zero;
                            fabao.actor.transform.localEulerAngles = Vector3.zero;
                            fabao.actor.actorController.controller.enabled = false;
                            this.initFabao = true;
                        }
                    }
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_PERI && EntityPlayer.Player != null && owner_id == EntityPlayer.Player.id)
                {
                    //玩家自己的女神不删除
                    GameObject.DontDestroyOnLoad(this.actor.gameObject);
                    this.moveManager.accordingMode = AccordingMode.AccordingActor;
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP)
                {
                    SetGravity(0);
                }
                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT)
                {
                    actor.animationController.animator.enabled = false;
                    SetGravity(0);
                    if (!this.skillManager.inServerMode)
                    {
                        throwObjectManager = CreatureManagerObjectPool.CreateThrowObjectManager(this);
                    }
                }
                this.CallLuaSelfFunction("OnActorLoaded");
                //出生音效
                actor.soundController.PlayBornSound();
                if (this.entityType == "Monster" && actor.actorData.FootSoundList.Count != 0)
                {
                    actor.IsUseFootSound = true;
                    actor.IsUseFootSmoke = false;
                    actor.FootStepSpeed = -1.0f;
                    actor.FootMinDistance = 0.4f;
                    //Debug.LogError("boss foot step!!!!");
                }
                else if (this.entityType == "PlayerAvatar")
                {
                    actor.IsUseFootSound = true;
                    actor.IsUseFootSmoke = true;
                    actor.FootMinDistance = 0.1f;
                }
                if (hasActorDeathController)
                {
                    _actorDeathController = actor.gameObject.AddComponentOnlyOne<ActorDeathController>();
                    _actorDeathController.SetEntityInfo(this);
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR)
                {
                    GameObject.DontDestroyOnLoad(this.actor.gameObject);
                    this.actor.position = PlayerSkillPreviewManager.GetInstance().initPos;
                    this.actor.localEulerAngles = new Vector3(0, 180, 0);
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
                {
                    this.SetActorMode(fight_idle_state);
                    SetCanWingShow(!PlayerSettingManager.hideOtherWing);
                    actor.isHideFX = PlayerSettingManager.hideOtherSkillFx;
                    RefreshEquipFlowShow(!PlayerSettingManager.hideOtherFlow);
                    actorVisible = !(PlayerSettingManager.hideOtherPlayer || isHideEntity);

                    if (MogoWorld.Entities.ContainsKey(pet_eid))
                    {
                        EntityCreature pet = MogoWorld.Entities[pet_eid] as EntityCreature;
                        if (pet != null && pet.actor != null)
                        {
                            pet.actorVisible = actorVisible;
                        }
                    }
                }else if(this.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR)
                 {
                    bool isHide = PlayerSettingManager.hidePlayer;
                    this.SetActorMode(fight_idle_state);
                    SetCanWingShow(!isHide);
                    actor.isHideFX = isHide;
                    RefreshEquipFlowShow(!isHide);
                    SetHideSkin(isHide);

                    if (MogoWorld.Entities.ContainsKey(pet_eid))
                    {
                        EntityCreature pet = MogoWorld.Entities[pet_eid] as EntityCreature;
                        if (pet != null && pet.actor != null)
                        {
                            pet.actorVisible = actorVisible;
                        }
                    }
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                {
                    actorVisible = !PlayerSettingManager.hideMonster;
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PERI)
                {
                    if (!this.IsDefaultModel())
                    { actorVisible = !PlayerSettingManager.hideNpc; }
                }
                else if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
                {
                    actorVisible = !PlayerSettingManager.hidePet;
                }

                if (this.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR
                        || this.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER || this.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY
                        || this.entityType == EntityConfig.ENTITY_TYPE_NAME_PET || this.entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO
                        || this.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                {
                    RefreshActorFxShow(!PlayerSettingManager.disableFx);
                }

                if (entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                {
                    PerformaceManager.GetInstance().GetNPCHandler().OnNPCActorLoaded(this);
                }

                CheckCellAttachedAndActorLoaded();
            }
            else
            {
                DestroyActor();
            }           
        }

        public virtual void BecomeShadowTarget(float size, bool ignoreDefaultLayer)
        {
            ShadowManager.GetInstance().SetFollowingTarget(this.actor.boneController.GetBoneByName("slot_camera"));
            ShadowManager.GetInstance().SetOrthographicSize(size);
            ShadowManager.GetInstance().OpenShadow();
            ShadowManager.GetInstance().IgnoreDefaultLayer(ignoreDefaultLayer);
        }

        private bool _banBeControlled = false;

        public bool banBeControlled
        {
            get
            {
                return _banBeControlled;
            }
            set
            {
                _banBeControlled = value;
            }
        }

        public virtual void OnHit()
        {

        }

        public virtual bool CanSearch()
        {
            return stateManager.CanDO(CharacterAction.SEARCH_ABLE);
        }

        public virtual bool CanActiveMove()
        {
            return stateManager.CanDO(CharacterAction.ACTIVE_MOVE_ABLE);
        }

        public virtual bool CanMove()
        {
            return stateManager.CanDO(CharacterAction.MOVE_ABLE);
        }

        public virtual bool CanTurn()
        {
            return stateManager.CanDO(CharacterAction.TURN_ABLE);
        }

        public virtual bool CanAttack()
        {
            return stateManager.CanDO(CharacterAction.ATTACK_ABLE);
        }

        public virtual bool CanDisplay()
        {
            return _visible;
        }

        public virtual bool InState(int state)
        {
            return stateManager.InState(state);
        }

        public void AddState(int state)
        {
            this.stateManager.AccumulateState(state, 1);
        }

        public void RemoveState(int state)
        {
            this.stateManager.AccumulateState(state, -1);
        }

        private GameObject npcIcon = null;

        public void ShowNPCIcon(string path, Vector3 height)
        {
            path = GameResource.ObjectPool.Instance.GetResMapping(path);
            GameResource.ObjectPool.Instance.GetGameObject(path, delegate(GameObject go)
            {
                go.transform.position = this.position + height;
                npcIcon = go;
            });
        }

        public void HideNPCIcon()
        {
            if (npcIcon != null)
            {
                GameObject.Destroy(npcIcon);
            }
        }

        public float freeFlyHspeed = 0;
        public float freeFlyG = 0;
        public float freeFlyMinH = 0;
        public float freeHdeceleration = 0;
        public float deltaHv = 0;
        public float freeFlyRotaRate = 1;
        public bool freeFlyEnd = false;
        public float freeFlyVspeed = 0;
        public float freeFlycx = 0;
        public float freeFlycy = 0;
        public float freeFlycz = 0;

        public bool limitCameraRotation = false;
        public virtual void FlyControl(bool start, bool end, float rate, float hspeed, float hdeceleration, float vspeed,
            float gravity, float minHeight, float cx, float cy, float cz)
        {
            limitCameraRotation = !limitCameraRotation;
            freeFlyRotaRate = rate;
            if (rate == 0)
            {
                freeFlyRotaRate = 30;
            }
            freeFlyHspeed = hspeed;
            freeHdeceleration = hdeceleration;
            freeFlyG = gravity;
            freeFlyMinH = minHeight;
            deltaHv = 0;
            freeFlyEnd = end;
            freeFlyVspeed = vspeed;
            freeFlycx = cx;
            freeFlycy = cy;
            freeFlycz = cz;
            actor.actorController.UpdateFreeFlyArgs(freeFlyRotaRate, freeFlyHspeed, freeHdeceleration, freeFlyVspeed,
                freeFlyG, freeFlyMinH);
        }

        public virtual void OnDeath()
        {
            this._isAlive = false;
            this._clearTargetTime = RealTime.time + global_params_helper.CameraInitial[4] * 0.001f;
            if (actor != null)
            {
                actor.animationController.Die();
                actor.soundController.PlayDeadSound();
            }
            if (actorDeathController != null)
            {
                actorDeathController.OnDeath();
            }
            this.CallLuaSelfFunction("OnDeath");
            if (this.IsPlayer())
            {
                PlayerEventManager.GetInstance().OnDeath();
            }
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
            {
                //宝宝死亡隐身
                TimerHeap.AddTimer(1000, 0, () =>
                {
                    if (!_isAlive)
                    { 
                        actorVisible = false; 
                    }
                });
            }
        }

        public virtual void OnRelive()
        {
            this._isAlive = true;
            this._clearTargetTime = 0;
            this.CallLuaSelfFunction("OnRelive");
            if (entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
            {
                actorVisible = true;
            }
        }

        public virtual void OnEnterTransform()
        {
            
        }

        public virtual void OnLeaveTransform()
        {

        }

        [NoToLua]
        public virtual void OnEnterDistance()
        {
            this.CallLuaSelfFunction("__OnEnterDistance");
        }

        public void OnChangeHp(uint casterId, int oldHp, int newHp, int attackType)
        {
            this.CallLuaSelfFunction("OnChangeHp", casterId, oldHp, newHp, attackType);
        }

        public virtual void OnEnterGhost()
        {
            if (actor != null)
            {
                actor.animationController.Ghost();
            }
        }

        public virtual void OnLeaveGhost()
        {
        }

        virtual public Transform GetCameraTransform()
        {
            if (isActorLoaded)
            {
                return this.actor.boneController.GetBoneByName("slot_camera");
            }
            return null;
        }

        public ACTActor controlActor
        {
            get
            {
                return (rideManager != null && rideManager.ride != null) ? rideManager.ride : actor;
            }
        }

        protected bool inCity = true;

        public void SetCityState(bool state)
        {
            inCity = state;
            ResetCityState();
        }

        //
        public void SetActorControllerEnabled(bool state)
        {
            actor.actorController.SetControllerEnabled(state);
        }

        //createRoleType 0:创角 1:选角
        private byte _createRoleType = 0;
        public byte createRoleType
        {
            get { return _createRoleType; }
            set { 
                    _createRoleType = value;
                }
        }

        protected void OnFootEvent(int param)
        {
            if (actor == null) return;
            actor.footStep.OnFootEvent(actor, param);
        }

        protected void OnEvent(string param)
        {
            if (actor == null) return;
            this.CallLuaSelfFunction("OnAnimationEvent", param);
        }

        [NoToLua]
        public float GetGroundHigh()
        {
            if (isActorLoaded)
            {
                return this.actor.actorController.groundHeight;
            }
            return 0;
        }

        AnimationEvent animationEvent;
        protected void ResetCityState()
        {
            if (actor == null) return;

            if (entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR || entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
            {
                ACTSystemAdapter.SetActorCityState(actor, false, this.vocation);
                actor.equipController.PackUpWeapon(false);
                if (animationEvent == null)
                {
                    animationEvent = actor.gameObject.AddComponent<AnimationEvent>();
                    animationEvent.onFootEvent = OnFootEvent;
                    animationEvent.onEvent = OnEvent;
                    if (this.IsPlayer())
                    {
                        actor.footStep.canUse = true;
                    }
                }
            }

            if (entityType == EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR )
            {
                actor.equipController.PackUpWeapon(false);
                if (animationEvent == null)
                {
                    animationEvent = actor.gameObject.AddComponent<AnimationEvent>();
                    animationEvent.onFootEvent = OnFootEvent;
                    animationEvent.onEvent = OnEvent;
                }
            }

            if (entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
            {
                if (this.vocation > 0)
                {
                    if (inCity)
                    {
                        ACTSystemAdapter.SetUIActorController(actor, false, this.vocation, () =>
                        {
                            this.CallLuaSelfFunction("OnControllerLoaded");
                        });
                    }
                    else 
                    {
                        ACTSystemAdapter.SetActorCityState(actor, true, this.vocation, () =>
                        {
                            this.CallLuaSelfFunction("OnControllerLoaded");
                        });
                        SetActorMode(fight_idle_state);
                    }
                    if (animationEvent == null)
                    {
                        animationEvent = actor.gameObject.AddComponent<AnimationEvent>();
                        animationEvent.onFootEvent = OnFootEvent;
                        animationEvent.onEvent = OnEvent;
                    }
                }
            }

            if (entityType == EntityConfig.ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR)
            {
                actor.equipController.PackUpWeapon(false);
                if (createRoleType == 0)
                {
                    ACTSystemAdapter.SetCreateActorController(actor, false, (int)this.vocation, () =>
                    {
                        this.CallLuaSelfFunction("OnControllerLoaded");
                    });
                }
                else 
                {
                    ACTSystemAdapter.SetUIActorController(actor, false, (int)this.vocation, () =>
                    {

                    });
                }

                if (animationEvent == null)
                {
                    animationEvent = actor.gameObject.AddComponent<AnimationEvent>();
                    animationEvent.onEvent = OnEvent;
                }
            }
            return;
        }

        //模型初始scale
        protected float _initModelScale = 1;

        //设置模型初始化scale
        public void InitActorScale(float scale)
        {
            _initModelScale = scale;
            if (actor != null && actor.gameObject != null)
            {
                Vector3 scaleVector = scale * VestManager.GetInstance().GetScale(this.entityType);
                actor.gameObject.transform.localScale = scaleVector;
            }
        }

        protected List<float> _scaleMlps = new List<float>();

        public void AddScaleMlp(float value)
        {
            if (value == _initModelScale)
            {
                return;
            }
            _scaleMlps.Add(value);
            UpdateActorScale();
        }

        public void RemoveScaleMlp(float value)
        {
            if (value == _initModelScale)
            {
                return;
            }
            if (_scaleMlps.Contains(value))
            {
                _scaleMlps.Remove(value);
            }
            UpdateActorScale();
        }

        private void UpdateActorScale()
        {
            float scale = _initModelScale;
            for (int i = 0; i < _scaleMlps.Count; i++)
            {
                scale *= _scaleMlps[i];
            }
            if (actor != null && actor.gameObject != null)
            {
                actor.gameObject.transform.localScale = new Vector3(scale, scale, scale);
            }
        }

        public void SetGravity(float gravity)
        {
            if (actor != null)
            {
                actor.actorController.gravity = gravity;
            }
        }

        public bool IsOnAir()
        {
            if (actor != null)
            {
                return actor.actorState.OnAir;
            }
            return false;
        }

        public bool IsMoving()
        {
            if (actor != null)
            {
                return actor.actorController.IsMoving();
            }
            return false;
        }

        public void SetLearnedSkillDict(int[] learnedDict)
        {
            if (learnedDict != null)
            {
                Dictionary<int, int> _learnedDict = new Dictionary<int, int>();
                for (int i = 0; i < learnedDict.Length; i += 2)
                {
                    _learnedDict.Add(learnedDict[i], learnedDict[i + 1]);
                }
                this.skillManager.SetLearnedSkillDict(_learnedDict);
            }
        }

        public void AddEquip(int equipID, int particleID = 0, int flowID = 0, int colorR = 0, int colorG = 0, int colorB = 0)
        {
            if (actor != null)
            {
                //actor.equipController.equipWeapon.InCity(false);
                equipManager.AddEquip(equipID, particleID, flowID, colorR, colorG, colorB);
            }
        }

        public void ResetDefaultEquip(int type)
        {
            if (actor != null)
            {
                equipManager.ResetDefaultEquip((ACTEquipmentType)type);
            }
        }

        public void SetCurCloth(int clothID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            equipManager.SetCurCloth(clothID, particleID, flowID, r, g, b);
        }

        //public void SetCurHair(int hairID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        //{
        //    equipManager.SetCurHair(hairID, particleID, flowID, r, g, b);
        //}

        public void SetCurWeapon(int weaponID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            equipManager.SetCurWeapon(weaponID, particleID, flowID, r, g, b);
        }

        public void SetCurHead(int headID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            equipManager.SetCurHead(headID, particleID, flowID, r, g, b);
        }

        public void SetCurWing(int wingID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            equipManager.SetCurWing(wingID, particleID, flowID, r, g, b);
        }

        public void SetCurEffect(int effectID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            equipManager.SetCurEffect(effectID, particleID, flowID, r, g, b);
        }

        public bool isPreviewState = false;
        public void SyncShowPlayerFacade()
        {
            equipManager.SyncShowPlayerFacade();
        }

        public void SetReady(bool isReady)
        {
            this.actor.actorState.IsReady = isReady;
        }

        public void FaceToPosition(Vector3 position)
        {
            if (isActorLoaded)
            {
                this.actor.FaceTo(position);
            }
        }

        public void IdleToReady()
        {
            actor.animationController.IdleToReady();
        }

        public void ReadyToIdle()
        {
            actor.animationController.ReadyToIdle();
        }

        public Transform GetCameraTargetProxy()
        {
            return CameraTargetProxy.instance.transform;
        }

        public void SetBMGSlot(Transform target = null)
        {
            Transform slotBone = target == null ? actor.gameObject.transform : target;
            if (_bgmSlot == null) return;
            _bgmSlot.transform.SetParent(slotBone, false);
            _bgmSlot.transform.ResetPositionAngles();
        }

        public void RecordBGMSlot(Transform bgmSlot)
        {
            this._bgmSlot = bgmSlot;
        }

        public void SetCreatureType(int type)
        {
            creatureType = (CreatureType) type;
        }

        bool selectTriggerExist = false; //
        ActorSelectTrigger trigger;
        public void SetSelectTrigger()
        {
            trigger = this.actor.gameObject.AddComponent<ActorSelectTrigger>();
            trigger.id = id;
            if (!selectTriggerExist)
            {
                EventDispatcher.AddEventListener<uint>(Common.Events.TouchEvents.ON_TOUCH_ENTITY, OnTouchEntity);
                selectTriggerExist = true;
            }
        }

        private void OnTouchEntity(uint id)
        {
            if (this.id != id || this.actor == null)
            {
                return;
            }
            this.CallLuaSelfFunction("OnTouchEntity");
            EntityPlayer.Player.lockTargetManager.OnTouchEntity(id);
        }

        private bool _lowerHPState = false;
        private bool _preLowerHPState = false;
        private float _lowerRate = 0;
        private bool _isShowLowerHP = false;
        private void OnHPChange()
        {
            _lowerHPState = this._curHp <= _lowerRate * this._maxHp;
            if (!_preLowerHPState && _lowerHPState)
            {
                CameraManager.GetInstance().mainCamera.ShowScreenDying(true);
                _isShowLowerHP = true;
            }
            if (_preLowerHPState && !_lowerHPState)
            {
                CameraManager.GetInstance().mainCamera.ShowScreenDying(false);
                _isShowLowerHP = false;
            }
            if (this.stateManager.InState(state_config.AVATAR_STATE_DEATH) && _isShowLowerHP)
            {
                CameraManager.GetInstance().mainCamera.ShowScreenDying(false);
                _isShowLowerHP = false;
            }
            _preLowerHPState = _lowerHPState;
        }

        Dictionary<int, UInt64> addBuffs;
        private void AddBuffOnEnterWorld()
        {
            if(!string.IsNullOrEmpty(buff_allclients))
            {
                UInt64 serverTime = Global.serverTimeStamp;
                addBuffs = data_parse_helper.ParseDictionaryIntUInt64(buff_allclients);
                var en = addBuffs.GetEnumerator();
                while (en.MoveNext())
                {
                    var buffid = en.Current.Key;
                    var endtime = en.Current.Value;
                    if (endtime == 0)
                    {
                        this.bufferManager.AddBuffer(buffid, -1, false);
                    }
                    else
                    {
                        endtime = endtime - serverTime;
                        if (endtime > 0) this.bufferManager.AddBuffer(buffid, endtime, false);
                    }
                } 
            }
            if (!string.IsNullOrEmpty(buff_client))
            {
                UInt64 serverTime = Global.serverTimeStamp;
                addBuffs = data_parse_helper.ParseDictionaryIntUInt64(buff_client);
                var en = addBuffs.GetEnumerator();
                while (en.MoveNext())
                {
                    var buffid = en.Current.Key;
                    var endtime = en.Current.Value;
                    if (endtime == 0)
                    {
                        this.bufferManager.AddBuffer(buffid, -1, false);
                    }
                    else
                    {
                        endtime = endtime - serverTime;
                        if (endtime > 0) this.bufferManager.AddBuffer(buffid, endtime, false);
                    }
                }
            }
        }

        private SkillPreviewCamera _skillPreviewCamera = null;
        public void SetSkillPreviewCamera(Transform tran)
        {
            _skillPreviewCamera = tran.gameObject.AddComponent<SkillPreviewCamera>();
            _skillPreviewCamera.SetTarget(EntityPlayer.ShowPlayer.GetTransform());
        }

        public void SetSkillPreviewCameraArgs(float distance, float rotationX, float rotationY,float rectX)
        {
            if (_skillPreviewCamera == null) return;
            _skillPreviewCamera.SetArgs(distance, rotationX, rotationY, rectX);
        }

        private HeadPreviewCamera _headPreviewCamera = null;
        public void SetHeadPreviewCamera(Transform tran)
        {
            if (_headPreviewCamera == null)
            {
                _headPreviewCamera = tran.gameObject.AddComponent<HeadPreviewCamera>();
            }
            _headPreviewCamera.SetTarget(EntityPlayer.ShowPlayer.GetTransform());
        }

        public void SetHeadPreviewCameraArgs(float distance, float rotationX, float rotationY, float rectX, float height = 1f, float right = 0f)
        {
            if (_headPreviewCamera == null) return;
            _headPreviewCamera.SetArgs(distance, rotationX, rotationY, rectX, height, right);
        }

        public void PauseTo(int frame)
        {
            actor.animationController.PauseTo(frame);
        }

        public void SetRide(int actorID, float modelScale = -1)
        {
            if (rideManager == null) this.rideManager = CreatureManagerObjectPool.CreateRideManager(this);
            rideManager.SetMount(actorID, modelScale);
        }

        public void SetActorMode(int mode)
        {
            if (this.actor == null) return;
            if (mode == 0)
            {
                //普通待机
                this.ReadyToIdle();
                actor.equipController.PackUpWeapon(true);
                if (entityType == EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR)
                {
                    ACTSystemAdapter.SetUIActorController(actor, false, this.vocation);
                }
            }
            else
            {
                //战斗待机
                this.IdleToReady();
                actor.equipController.PackUpWeapon(false);
                if (entityType == EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR)
                {
                    ACTSystemAdapter.SetActorCityState(actor, false, this.vocation);
                }
            }
        }

        public void PackUpWeapon(int state)
        {
            if (this.actor == null) return;
            actor.equipController.PackUpWeapon(state == 1);
        }

        public int GetSkillIdByPos(int pos)
        {
            return this.skillManager.GetSkillIDByPos(pos);
        }

        public void ResetSkillShowAvater()
        {
            if (EntityPlayer.ShowPlayer != null && EntityPlayer.ShowPlayer.actor != null)
            {
                PlayerSkillPreviewManager.GetInstance().SetTransform();
                EntityPlayer.ShowPlayer.actor.position = PlayerSkillPreviewManager.GetInstance().initPos;
                EntityPlayer.ShowPlayer.actor.localScale = Vector3.one;
                EntityPlayer.ShowPlayer.actor.localEulerAngles = new Vector3(0, 180, 0);
            }
        }

        public Texture2D GetHeadPicture(RenderTexture renderTexture)
        {
            int width = 320;
            int height = 180;
            Texture2D tex2d = new Texture2D(width, height, TextureFormat.ARGB32, false);
            RenderTexture.active = renderTexture;
            tex2d.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex2d.Apply();
            return tex2d;
        }

        public void SetUpdateFaceEnabled(bool enable)
        {
            if (actor == null) return;
            this.actor.actorController.SetUpdateFaceEnabled(enable);
        }

        public string GetCurBufferIDList()
        {
            var buffList = this.bufferManager.GetCurBufferIDList();
            string buffs = buffList.PackList();
            return buffs;
        }

        public float GetBufferRemainTime(int bufferID)
        {
            var remainTime = this.bufferManager.GetBufferRemainTime(bufferID);
            return remainTime;
        }

        public void SetCanWingShow(bool isShow)
        {
            if (actor == null) return;
            actor.equipController.SetWingShow(isShow);
        }

        public void RefreshActorFxShow(bool isShow)
        {
            if (actor == null) return;
            actor.equipController.canEquipFxShow = isShow;
            if (rideManager != null && rideManager.ride != null)
            {
                ACTSystemTools.SetFxShow(rideManager.ride.transform, isShow);
            }
            else
            {
                ACTSystemTools.SetFxShow(actor.transform, isShow);
            }
        }

        public void RefreshEquipFlowShow(bool isShow)
        {
            if (actor == null) return;
            actor.equipController.SetEquipFlowShow(isShow);
        }

        public bool NeedHitFly() { 
            if (this.IsAvatar()) return true;
            return (entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO || entityType == EntityConfig.ENTITY_TYPE_NAME_PET || entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP);
        }


        public void SetHideSkin(bool isHide)
        {
            if (this.actor == null) return;
            SkinnedMeshRenderer[] smrList = this.actor.equipController.GetSkinnedMeshRenderers();
            if (smrList == null) return;
            for (int i = 0; i < smrList.Length; ++i)
            {
                smrList[i].enabled = !isHide;
            }
            this.actor.boneController.EnableMeshRender(isHide);
            //this.actor.equipController.EnableMeshRender(isHide);
        }

        public float hitOrBeHitTime = 0;
        public int CanExitMap()
        {
            if (UnityPropUtils.realtimeSinceStartup - hitOrBeHitTime > global_params_helper.hit_state_exit_map_time)
            {
                return 1; 
            }
            return 0; 
        }
    }
}
