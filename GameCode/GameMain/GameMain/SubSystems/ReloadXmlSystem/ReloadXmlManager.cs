﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using GameData;
using GameLoader.IO;
using System.Collections;
using GameLoader;

namespace GameMain
{
    public class ReloadXmlManager
    {
       
        private static ReloadXmlManager s_instance = null;
        public static ReloadXmlManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new ReloadXmlManager();
            }
            return s_instance;
        }

        public void ReloadFromLocal()
        { 
#if !UNITY_WEBPLAYER            
            string dataPath = GameLoader.SystemConfig.ResPath + "/data/xml";
            if (GameData.XMLManager.LoadXMLDataFromFolder(dataPath))
            {
                if (UnityPropUtils.IsEditor)
                {
                    var xmlFiles = Directory.GetFiles(dataPath, "*.xml", SearchOption.TopDirectoryOnly);
                    if (xmlFiles.Length != 0)
                    {
                        string temp = "The following xmldata has been reloaded:\n";
                        for (int i = 0; i < xmlFiles.Length; i++)
                        {
                            temp += "\t" + xmlFiles[i] + "\n";
                        }
                        //Debug.LogError(temp);
                    }
                }
                OnReloadFromLocal();
            }
#endif
        }

        public void OnReloadFromLocal()
        {
            CombatSystem.CombatLogicObjectPool.ClearAllData();
            
        }

        const string HOT_UPDATE_NAME = "update_files.txt";
        string _reloadFolderPath = string.Empty;
        byte[] _updateFilesSettingBytes;
        Dictionary<string, string> _curHotInfo = new Dictionary<string, string>();
        Dictionary<string, string> _newHotInfo = new Dictionary<string, string>();
        bool _lock = false;

        public void CheckHotUpdate()
        {
#if UNITY_WEBPLAYER
            CheckHotUpdateForWeb();
            return;
#endif
            if (_lock) return;
            _lock = true;
            CheckFolder();
            LoadCurHotInfo();
            DownloadHotUpdateInfoFromRemote(() => {
                DeleteOldFiles();
                LoaderDriver.Instance.StartCoroutine(DownloadNewFiles(() => {
                    ReloadDataFromDownload();
                    SaveUpdateFilesSetting();
                    _lock = false;
                }));
            });
        }

        void CheckFolder()
        {
            _reloadFolderPath = string.Concat(UnityEngine.Application.persistentDataPath, ConstString.RutimeResourceConfig, "/reload");
            if (!Directory.Exists(_reloadFolderPath))
            {
                Directory.CreateDirectory(_reloadFolderPath);
            }
        }

        void LoadCurHotInfo()
        {
#if !UNITY_WEBPLAYER
            string hotUpdatePath = Path.Combine(_reloadFolderPath, HOT_UPDATE_NAME);
            if (File.Exists(hotUpdatePath))
            {
                string strContent = FileUtils.LoadFile(hotUpdatePath);
                _curHotInfo = ReadHotUpdateInfo(strContent);
            }
#endif
        }

        Dictionary<string, string> ReadHotUpdateInfo(string strContent)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            string[] tempList = strContent.Split('\n');
            for (int i = 0; i < tempList.Length; i++)
            {
                string subContent = tempList[i].Trim();
                if (!string.IsNullOrEmpty(subContent))
                {
                    string[] data = subContent.Split(',');
                    if (data.Length < 2) continue;
                    string fileName = data[0];
                    string MD5 = data[1];
                    result[fileName] = MD5;
                }
            }
            return result;
        }

        public void DownloadHotUpdateInfoFromRemote(Action callback)
        {
            if (string.IsNullOrEmpty(SystemConfig.RemoteHotUpdateUrl))
            {
                callback();
                return;
            }
            ResLoader.LoadWwwBytes(Path.Combine(SystemConfig.RemoteHotUpdateUrl, HOT_UPDATE_NAME).Replace('\\', '/'), (url, bytes) =>
            {
                if (bytes != null)
                {
                    string strContent = Encoding.ASCII.GetString(bytes);
                    _newHotInfo = ReadHotUpdateInfo(strContent);
                    _updateFilesSettingBytes = bytes;
                }
                else {
                    LoggerHelper.Error("DownloadHotUpdateInfoFromRemote Error:" + url + " 不存在！");
                }
                callback();
            });
        }

        public void DeleteOldFiles()
        {
            List<string> deleteFiles = new List<string>();
            foreach (var pair in _curHotInfo)
            {
                if (!_newHotInfo.ContainsKey(pair.Key))
                {
                    deleteFiles.Add(pair.Key);
                }
                else if (_newHotInfo[pair.Key] != _curHotInfo[pair.Key])
                {
                    deleteFiles.Add(pair.Key);
                }
            }
            for (int i = 0; i < deleteFiles.Count; i++)
            {
                string filePath = Path.Combine(_reloadFolderPath, deleteFiles[i]);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);                    
                }
                _curHotInfo.Remove(deleteFiles[i]);
            }
        }

        private IEnumerator DownloadNewFiles(Action callback)
        {
            Queue<string> downloadFiles = new Queue<string>();
            foreach (var pair in _newHotInfo)
            {
                if (!_curHotInfo.ContainsKey(pair.Key))
                {
                    downloadFiles.Enqueue(pair.Key);
                }
            }
            while (downloadFiles.Count > 0)
            {
                string downloadFile = downloadFiles.Dequeue();
                string url = Path.Combine(SystemConfig.RemoteHotUpdateUrl, downloadFile);
                WWW www = new WWW(url);
                yield return www;
                string localFilePath = Path.Combine(_reloadFolderPath, downloadFile); ;
                SaveBytes(localFilePath, www.bytes);
            }
            callback();
        }

        private void ReloadDataFromDownload()
        {
#if !UNITY_WEBPLAYER
            foreach (var pair in _newHotInfo)
            {
                string fileName = pair.Key;
                string MD5 = pair.Value;
                if (!_curHotInfo.ContainsKey(fileName))
                {
                    _curHotInfo.Add(fileName, MD5);
                    try
                    {
                        string filePath = Path.Combine(_reloadFolderPath, fileName);
                        if (File.Exists(filePath))
                        {
                            string strContent = FileUtils.LoadFile(filePath);
                            XMLManager.OverrideData(Path.GetFileNameWithoutExtension(fileName), strContent);
                            LoggerHelper.Info("OverrideData by " + filePath);
                        }
                        else {
                            LoggerHelper.Error("Reload Error:" + filePath + " 不存在！");
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Error("Reload Error:" + ex.Message);
                    }
                }
            }
            if (_newHotInfo.Count > 0)
            {
                OnReloadFromLocal();
                ReloadXmlCallbacks.OnReload(_curHotInfo);
            }
#endif
        }

        private void SaveUpdateFilesSetting()
        {
            if (_updateFilesSettingBytes != null)
            {
                string localFilePath = Path.Combine(_reloadFolderPath, HOT_UPDATE_NAME);
                SaveBytes(localFilePath, _updateFilesSettingBytes);
            }
        }

        static void SaveBytes(String fileName, byte[] buffer)
        {
            if (!Directory.Exists(fileName.GetDirectoryName()))
            {
                Directory.CreateDirectory(fileName.GetDirectoryName());
            }
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (BinaryWriter sw = new BinaryWriter(fs))
                {
                    sw.Write(buffer);
                    sw.Flush();
                    sw.Close();
                }
                fs.Close();
            }
        }

        private void CheckHotUpdateForWeb()
        {
            if (_lock) return;
            _lock = true;
            DownloadHotUpdateInfoFromRemote(() =>
            {
                LoaderDriver.Instance.StartCoroutine(DownloadNewFilesForWeb(() =>
                {
                    _lock = false;
                }));
            });
        }

        private IEnumerator DownloadNewFilesForWeb(Action callback)
        {
            Queue<string> downloadFiles = new Queue<string>();
            foreach (var pair in _newHotInfo)
            {
                if (!_curHotInfo.ContainsKey(pair.Key))
                {
                    downloadFiles.Enqueue(pair.Key);
                }
            }
            while (downloadFiles.Count > 0)
            {
                string downloadFile = downloadFiles.Dequeue();
                string url = Path.Combine(SystemConfig.RemoteHotUpdateUrl, downloadFile);
                WWW www = new WWW(url);
                yield return www;
                string localFilePath = Path.Combine(_reloadFolderPath, downloadFile); ;
                XMLManager.OverrideData(Path.GetFileNameWithoutExtension(downloadFile), www.text);
            }
            callback();
        }
    }
}
