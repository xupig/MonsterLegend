﻿using System;
using System.Collections.Generic;


using GameData;

namespace GameMain
{
    public class ReloadXmlCallbacks
    {
        static Dictionary<string, Action> _curXMLNameCallbackMap;
        static Dictionary<string, Action> curXMLNameCallbackMap
        {
            get {
                if (_curXMLNameCallbackMap == null)
                {
                    _curXMLNameCallbackMap = new Dictionary<string, Action>() 
                    {
                        {"global_params.xml", OnReloadGlobalParams},

                    };
                }
                return _curXMLNameCallbackMap; }
        }

        public static void OnReload(Dictionary<string, string> hotInfo)
        {
            HashSet<Action> actions = new HashSet<Action>();
            foreach (var pair in hotInfo)
            {
                string fileName = pair.Key;
                if (curXMLNameCallbackMap.ContainsKey(fileName) && !actions.Contains(curXMLNameCallbackMap[fileName]))
                {
                    actions.Add(curXMLNameCallbackMap[fileName]);
                }
            }
            foreach (var action in actions)
            {
                action();
            }
        }

        static void OnReloadGlobalParams()
        {
            global_params_helper.OnReloadData();
        }
    }
}
