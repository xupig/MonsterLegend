﻿#region 模块信息
/*==========================================
// 文件名：LocalIdEnum
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/14 11:06:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain
{
    public enum LocalName
    {
        TouchCameraScale = 1,
        TouchBattleMode = 2,
        ChatPhrase = 3,
        PrivateChat = 4,
    }
}
