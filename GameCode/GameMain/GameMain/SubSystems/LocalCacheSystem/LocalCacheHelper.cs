﻿#region 模块信息
/*==========================================
// 文件名：SharedObject
// 命名空间: GameLoader.GameLoader.MogoEngine.Utils
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/31 16:35:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameLoader.Utils;
using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace GameMain
{

    public enum CacheLevel
    {
        /// <summary>
        /// 永久
        /// </summary>
        Permanent = 0,
        Year = 1,
        Month = 2,
        Week = 3,
        Day = 4,
    }

    /// <summary>
    /// 本地缓存
    /// </summary>
    public class LocalCacheHelper
    {

        private static LocalCacheWriter _writer;
        private static CommonLocalCacheWriter _commonWriter;
        private static string _dbId;
        private static Dictionary<string, string> _cacheDict;
        private static Dictionary<string, LocalData> _dataDict;
        private static char[] split = new char[] { '|'};
        private static Dictionary<string, string> _commonCacheDict;

        public static void Init(string dbId)
        {
            if(_dbId != dbId)
            {
                _dbId = dbId;
                _writer = new LocalCacheWriter(dbId);
                _cacheDict = _writer.ReadLocalCache();
                _dataDict = new Dictionary<string, LocalData>();
            }
            
        }

        public static void InitCommonLocalCache()
        {
            _commonWriter = new CommonLocalCacheWriter();
            _commonCacheDict = _commonWriter.ReadLocalCache();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localId">缓存ID</param>
        /// <param name="data">数据</param>
        /// <param name="level">缓存级别</param>
        /// <param name="expireData">失效时间</param>
        public static void Write(string localId, object data, CacheLevel level, int serverTime)
        {
            if (_cacheDict != null)
            {
                int expireData = GetExpireData(level) + serverTime;
                if (_dataDict.ContainsKey(localId))
                {
                    _dataDict[localId].data = data;
                    _dataDict[localId].expireData = expireData;
                }
                else
                {
                    _dataDict.Add(localId, new LocalData(data, expireData));
                }
                string localIdString = localId.ToString();
                string content = string.Format("{0}|{1}",expireData,JsonMapper.ToJson(data));
                if (_cacheDict.ContainsKey(localIdString) == false)
                {
                    _cacheDict.Add(localIdString, content);
                }
                else
                {
                    _cacheDict[localIdString] = content;
                }
                _writer.WriteLocalCache(JsonMapper.ToJson(_cacheDict));
            }
            
        }

        public static T Read<T>(string localId)
        {
            if (_cacheDict == null)
            {
                return default(T);
            }
            if(_dataDict.ContainsKey(localId) == false)
            {
                if (_cacheDict.ContainsKey(localId.ToString()))
                {
                    string content = _cacheDict[localId.ToString()];
                    string[] dataList = content.Split(split);
                    try
                    {
                        T obj = JsonMapper.ToObject<T>(dataList[1]);
                        _dataDict.Add(localId, new LocalData(obj, int.Parse(dataList[0])));
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Info(string.Format("LocalCacheHelper Read, {0} localId:{1}", ex.Message, localId));
                        _writer.CleanLocalCache();
                        return default(T);
                    }
                }
            }
            if(_dataDict.ContainsKey(localId))
            {
                return (T)_dataDict[localId].data;
            }
            return default(T);
        }

        /// <summary>
        /// 写到公共本地缓存中
        /// </summary>
        /// <param name="localName"></param>
        /// <param name="data"></param>
        public static void WriteCommon(string localId, object data)
        {
            if (_commonCacheDict != null)
            {
                string strLocalId = localId.ToString();
                string content = JsonMapper.ToJson(data);
                if (!_commonCacheDict.ContainsKey(strLocalId))
                {
                    _commonCacheDict.Add(strLocalId, content);
                }
                else
                {
                    _commonCacheDict[strLocalId] = content;
                }
                _commonWriter.WriteLocalCache(JsonMapper.ToJson(_commonCacheDict));
            }
        }

        /// <summary>
        /// 读取公共本地缓存中的数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localName"></param>
        /// <returns></returns>
        public static T ReadCommon<T>(string localId)
        {
            if (_commonCacheDict == null)
            {
                return default(T);
            }
            if (_commonCacheDict.ContainsKey(localId.ToString()))
            {
                string content = _commonCacheDict[localId.ToString()];
                try
                {
                    T obj = JsonMapper.ToObject<T>(content);
                    return obj;
                }
                catch (Exception ex)
                {
                    LoggerHelper.Info(string.Format("LocalCacheHelper ReadCommon, {0}", ex.Message));
                    _commonWriter.CleanLocalCache();
                    return default(T);
                }
            }
            return default(T);
        }

        private static int GetExpireData(CacheLevel level)
        {
            switch(level)
            {
                case CacheLevel.Permanent:
                    return -1;
                case CacheLevel.Year:
                    return 60 * 60 * 24 * 365;
                case CacheLevel.Month:
                    return 60 * 60 * 24 * 30;
                case CacheLevel.Week:
                    return 60 * 60 * 24 * 7;
                case CacheLevel.Day:
                    return 60 * 60 * 24;
                default:
                    return 0;
            }
        }
    }

    public class LocalData
    {
        public object data;
        public int expireData;
        public LocalData(object data,int expireData)
        {
            this.data = data;
            this.expireData = expireData;
        }
    }
    

    public class LocalCacheWriter
    {

#if !UNITY_WEBPLAYER
        private string m_logFileName = ".so";
        private FileStream m_fs;
        //private Action<string> m_logWriter;
        private readonly object m_locker = new object();
        private string m_logFilePath;
        private Dictionary<string, string> emptyDict;

        private const Int32 DEFAULT_PLUTO_BUFF_SIZE = 1024;
        /// <summary>
        /// 默认构造函数。
        /// </summary>
        public LocalCacheWriter(string dbId)
        {
            emptyDict = new Dictionary<string, string>();
            string m_logPath = string.Concat(UnityEngine.Application.persistentDataPath, ConstString.RutimeResourceConfig, "/cache");
            if (!Directory.Exists(m_logPath))
            {
                Directory.CreateDirectory(m_logPath);
            }
               

            m_logFilePath = String.Concat(m_logPath, "/", dbId, m_logFileName);
            //m_logWriter = WriteLocalCache;
        }

        /// <summary>
        /// 释放资源。
        /// </summary>
        public void Release()
        {
            lock (m_locker)
            {
                if (m_fs != null)
                {
                    m_fs.Close();
                    m_fs.Dispose();
                }
            }
        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="msg">日志内容</param>
        public void WriteLocalCache(string content)
        {
            try
            {
                m_fs = new FileStream(m_logFilePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
                byte[] bytes = ASCIIEncoding.UTF8.GetBytes(content);
                m_fs.SetLength(bytes.Length);
                m_fs.Write(bytes, 0, bytes.Length);
                m_fs.Flush();
                m_fs.Close();
                m_fs.Dispose();
                m_fs = null;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
        }

        public void CleanLocalCache()
        {
            WriteLocalCache(JsonMapper.ToJson(emptyDict));
        }

        public Dictionary<string,string> ReadLocalCache()
        {
            try
            {
                m_fs = new FileStream(m_logFilePath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
                if(m_fs.Length == 0)
                {
                    m_fs.Close();
                    m_fs.Dispose();
                    return new Dictionary<string,string>();
                }
                byte[] data = new byte[m_fs.Length];
                m_fs.Read(data, 0, data.Length);
                m_fs.Close();
                m_fs.Dispose();
                m_fs = null;
                string content = ASCIIEncoding.UTF8.GetString(data);
                return JsonMapper.ToObject<Dictionary<string, string>>(content);
            }
            catch (Exception ex)
            {
                CleanLocalCache();
                LoggerHelper.Error(ex.Message);
            }
            return new Dictionary<string,string>();
        }
#else
        const string LOCAL_CACHE_BASE_NAME = "LocalCache";

        private readonly object m_locker = new object();
        private string m_logCacheKey;
        private Dictionary<string, string> emptyDict;

        private const Int32 DEFAULT_PLUTO_BUFF_SIZE = 1024;
        /// <summary>
        /// 默认构造函数。
        /// </summary>
        public LocalCacheWriter(UInt64 dbId)
        {
            emptyDict = new Dictionary<string, string>();
            m_logCacheKey = String.Concat(dbId, LOCAL_CACHE_BASE_NAME);
        }

        /// <summary>
        /// 释放资源。
        /// </summary>
        public void Release()
        {

        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="msg">日志内容</param>
        public void WriteLocalCache(string content)
        {
            try
            {
                content = WWW.EscapeURL(content);
                PlayerPrefs.SetString(m_logCacheKey, content);
                //Debug.LogError("WriteLocalCache Save:" + content.Length);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
        }

        public void CleanLocalCache()
        {
            WriteLocalCache(JsonMapper.ToJson(emptyDict));
        }

        public Dictionary<string, string> ReadLocalCache()
        {
            try
            {
                string content = PlayerPrefs.GetString(m_logCacheKey);
                content = WWW.UnEscapeURL(content);
                return JsonMapper.ToObject<Dictionary<string, string>>(content);
            }
            catch (Exception ex)
            {
                CleanLocalCache();
                LoggerHelper.Error(ex.Message);
            }
            return emptyDict;
        }
#endif
    }

}
