﻿using GameResource;
using GameData;
using GameLoader.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Global;
using Common.Events;
using Common.ExtendTools;
using GameMain.GlobalManager;
using ACTSystem;


namespace GameMain
{
    public class CameraRTManager
	{
        
        private static CameraRTManager s_instance = null;
        public static CameraRTManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new CameraRTManager();
            }
            return s_instance;
        }

        public void Init()
        {
            RenderTextureMaterialHelper.curRTLoader = new RenderTextureRuntimeLoader();
        }

        private ACTOneShotRTCamera _oneShotRTCamera;
        public RenderTexture GetMainCameraOneShotRT()
        {
            if (_oneShotRTCamera == null)
            {
                int cullingMask = UnityEngine.LayerMask.GetMask("Terrain");
                _oneShotRTCamera = ACTOneShotRTCamera.CreateOneShotRTCamera(CameraManager.GetInstance().Camera, cullingMask);
            }
            return _oneShotRTCamera.OneShot();            
        }


    }
}
