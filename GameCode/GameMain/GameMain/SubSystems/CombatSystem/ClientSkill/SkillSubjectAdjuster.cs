﻿using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    static public class SkillSubjectAdjuster
    {
        static public List<int> GetCanUseTime(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.useTimes;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.useTimes, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetCD(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.cd;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.cd, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetEnergyCostType(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.energyCostType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.energyCostType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetEnergyCost(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.energyCost;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.energyCost, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<int>> GetAddBuffsOnCast(this SkillSubject skillSubject)
        {
            Dictionary<int, List<int>> result = skillSubject.skillData.addBuffsOnCast;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.addBuffsOnCast, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetDelBuffsOnBreak(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.delBuffsOnBreak;
            if (result != null)
            {
                for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
                {
                    var adjustData = skillSubject.adjustSkillDataList[i];
                    result = AdjustTools.Adjust(result, adjustData.delBuffsOnBreak, (AdjustType)adjustData.adjustType);
                }
            }
            return result;
        }

        static public List<int> GetSpecialSearchArg(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.specialSearchArg;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.specialSearchArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetOriginAdjust(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.originAdjust;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.originAdjust, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchRegionType(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.searchRegionType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchRegionType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetSearchRegionArg(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.searchRegionArg;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchRegionArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetSearchTargetTypes(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.searchTargetType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchTargetType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchType(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.searchType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetTargetFilterOrders(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.targetFilterOrders;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterOrders, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<int>> GetTargetFilterArgs(this SkillSubject skillSubject)
        {
            Dictionary<int, List<int>> result = skillSubject.skillData.targetFilterArgs;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterArgs, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetAccordingStick(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.accordingStick;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.accordingStick, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchTargetRepeat(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.searchTargetRepeat;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchTargetRepeat, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceLockMode(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.faceLockMode;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceLockMode, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceAlways(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.faceAlways;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceAlways, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceTgt(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.faceTgt;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceTgt, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
        
        static public List<int> GetSkillActions(this SkillSubject skillSubject)
        {
            List<int> result = skillSubject.skillData.skillActions;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.skillActions, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetSkillActionActiveTimes(this SkillSubject skillSubject)
        {
            List<float> result = skillSubject.skillData.skillActionActiveTimes;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.skillActionActiveTimes, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetSkillActionProbs(this SkillSubject skillSubject)
        {
            List<float> result = skillSubject.skillData.skillActionProbs;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.skillActionProbs, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<SkillEventData> GetSkillEvents(this SkillSubject skillSubject)
        {
            List<SkillEventData> result = skillSubject.skillData.skillEvents;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                if (adjustData.skillEvents != null && adjustData.skillEvents.Count!=0)
                {
                    result = adjustData.skillEvents;
                }
            }
            return result;
        }

        static public float GetActionCutTime(this SkillSubject skillSubject)
        {
            float result = skillSubject.skillData.actionCutTime;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.actionCutTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetIsSingingRequire(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.isSingingRequire;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.isSingingRequire, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSingingTime(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.singingTime;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.singingTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetIsStorageRequire(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.isStorageRequire;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.isStorageRequire, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFollowingSpell(this SkillSubject skillSubject)
        {
            int result = skillSubject.skillData.followingSpell;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.followingSpell, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
    }
}
