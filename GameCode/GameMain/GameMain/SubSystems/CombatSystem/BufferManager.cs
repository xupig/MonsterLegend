﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.RPC;
using GameMain;

using Common.Structs.ProtoBuf;
using UnityEngine;

using Common.ServerConfig;

namespace GameMain.CombatSystem
{
    public class BufferManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    enum CreateRemoveMode
    { 
       removeThisAndCondition = 1,
       keepThisAndremoveCondition = 2,
       removeThisAndkeepCondition = 3,
       keepThisAndCondition = 4,
    }

    enum CreateTimeMode
    {
        defaultTime = 1,
        ConditionRemainTime = 2,
        defaultTimeAddConditionRemainTime = 3,
        buffTime = 4,
        buffTimeAndConditionRemainTime = 5,
    }

    public class BufferManager
    {
        private EntityCreature _owner;

        private Dictionary<int, BufferSubject> _bufferSubjects = new Dictionary<int, BufferSubject>();
        private List<int> _bufferSubjectsList = new List<int>();
        private bool _isClearingBuffList = false;
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public BufferManager() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<BufferManagerUpdateDelegate>("BufferManager.Update", Update);
            }
            isLooping = true;
        }

        public void OnActorLoaded()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.PlayBuffFX();
                }
            }
        }

        public void OnActorBeDestroyed()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.StopBuffFX();
                }
            }
        }

        public void PlaySpecialFX()
        {
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                var bufferSubject = en.Current.Value;
                if (!bufferSubject.IsOver())
                {
                    bufferSubject.PlaySpecialFX();
                }
            }
        }

        public void RemoveBuffsOnDeath()
        {
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var bufferID = _bufferSubjectsList[i];
                var bufferSubject = _bufferSubjects[bufferID];
                if (!bufferSubject.isClientBuff) continue;
                if (bufferSubject.bufferData.removeMode == (int)BuffRemoveMode.DELETE_ON_DEATH)
                {
                    RemoveBuffer(bufferID);
                }
            }
        }

        public void Release()
        {
            OnActorBeDestroyed(); // 在ClearData前
            //MogoEngine.MogoWorld.UnregisterUpdate("BufferManager.Update", Update);
            ClearData();
        }

        public void ClearData()
        {
            isLooping = false;
            _bufferSubjects.Clear();
            _bufferSubjectsList.Clear();
            _isClearingBuffList = false;
            _addCreatedBufferDictPool.Clear();
            _removeList.Clear();

        }

        public void ResetServerBuffList(PbBuffList buffList)
        {
            ClearBufferSubjects();
            var bList = buffList.buffList;
            for (int i = 0; i < bList.Count; i++)
            {
                var buffData = bList[i];
                AddBuffer((int)buffData.buff_id, (float)(buffData.remain_tick), false, false);
            }
        }

        List<int> _removeList = new List<int>();
        public void ClearBufferSubjects()
        {
            _isClearingBuffList = true;
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                _removeList.Add(en.Current.Key);
            }
            for (int i = 0; i < _removeList.Count; i++)
            {
                RemoveBuffer(_removeList[i]);
            }
            _isClearingBuffList = false;
        }

        public void ClearAllClientBuffers()
        {
            _isClearingBuffList = true;
            var en = _bufferSubjects.GetEnumerator();
            while (en.MoveNext())
            {
                if (!en.Current.Value.isClientBuff) continue;
                _removeList.Add(en.Current.Key);
            }
            for (int i = 0; i < _removeList.Count; i++)
            {
                RemoveBuffer(_removeList[i]);
            }
            _isClearingBuffList = false;
        }

        public void ClearAllServerBuffer()
        {
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var bufferID = _bufferSubjectsList[i];
                var bufferSubject = _bufferSubjects[bufferID];
                if (bufferSubject.isClientBuff) continue;
                RemoveBuffer(bufferID);
            }
        }

        private void Update()
        {
            if (!isLooping) return;
            if (_bufferSubjects.Count == 0) return;
            _removeList.Clear();
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var bufferID = _bufferSubjectsList[i];
                var bufferSubject = _bufferSubjects[bufferID];
                var result = bufferSubject.Update();
                if (!result)
                {
                    _removeList.Add(bufferID);
                }
            }
            for (int i = 0; i < _removeList.Count; i++)
            {
                RemoveBuffer(_removeList[i]);
            }
        }

        public bool CanAddBuffer(int bufferID)
        {
            if (_isClearingBuffList)    //移除全部buff时不可加buff操作
            {
                return false;
            }
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            var excludeBuffs = bufferData.excludeBuffs;
            for (int i = 0; i < excludeBuffs.Count; i++)
            {
                if (_bufferSubjects.ContainsKey(excludeBuffs[i])) return false;
            }

            var excludeBuffsGroup = bufferData.excludeBuffsGroup;
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                var curBufferData = CombatLogicObjectPool.GetBufferData(_bufferSubjectsList[i]);
                if (excludeBuffsGroup.Contains(curBufferData.group))
                {
                    return false;
                }
                if (curBufferData.group != 0 && curBufferData.group == bufferData.group)
                {
                    if (curBufferData.groupLevel <= bufferData.groupLevel)
                    {
                        RemoveBuffer(curBufferID);
                    }
                    else  //curBufferData.groupLevel <= bufferData.groupLevel
                    {
                        return false;
                        //break;    
                    }
                }
            }   
            return true;
        }

        private void CheckReplaceBuffer(int bufferID)
        {
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            var replaceBuffsGroup = bufferData.replaceBuffsGroup;
            var replaceBuffs = bufferData.replaceBuffs;
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                var curBufferData = CombatLogicObjectPool.GetBufferData(_bufferSubjectsList[i]);
                if (replaceBuffs.Contains(curBufferID) || replaceBuffsGroup.Contains(curBufferData.group))
                {
                    RemoveBuffer(curBufferID);
                }
            }
        }

        Queue<Dictionary<int, float>> _addCreatedBufferDictPool = new Queue<Dictionary<int, float>>();
        Dictionary<int, float> CreateAddCreateBufferDict()
        {
            if (_addCreatedBufferDictPool.Count == 0)
            {
                _addCreatedBufferDictPool.Enqueue(new Dictionary<int, float>());
            }
            return _addCreatedBufferDictPool.Dequeue();
        }

        void ReleaseAddCreateBufferDict(Dictionary<int, float> dict)
        {
            dict.Clear();
            _addCreatedBufferDictPool.Enqueue(dict);
        }

        private bool CreateBuffersByCondition(int bufferID, float duration)
        {
            var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
            bool keepOnCurrentBuffer = true;
            List<int> _removeConditionBufferList = new List<int>();
            var addCreatedBufferDict = CreateAddCreateBufferDict();
            for (int i = _bufferSubjectsList.Count - 1; i >= 0; i--)
            {
                var curBufferID = _bufferSubjectsList[i];
                if (bufferData.conditionBuffsMap.ContainsKey(curBufferID))
                {
                    var createData = bufferData.conditionBuffsMap[curBufferID];
                    var curBufferData = CombatLogicObjectPool.GetBufferData(curBufferID);
                    int delay = curBufferData.delayTime;
                    switch ((CreateTimeMode)createData.createTimeMode)
                    {
                        case CreateTimeMode.ConditionRemainTime:
                            delay = (int)_bufferSubjects[curBufferID].remainTime * 1000;
                            break;
                        case CreateTimeMode.defaultTimeAddConditionRemainTime:
                            delay += (int)_bufferSubjects[curBufferID].remainTime * 1000;
                            break;
                        case CreateTimeMode.buffTime:
                            delay = (int)duration;
                            break;
                        case CreateTimeMode.buffTimeAndConditionRemainTime:
                            delay = (int)(duration + _bufferSubjects[curBufferID].remainTime * 1000);
                            break;
                        default:
                            break;
                    }
                    switch((CreateRemoveMode)createData.createRemoveMode)
                    {
                        case CreateRemoveMode.removeThisAndCondition:
                            _removeConditionBufferList.Add(curBufferID);
                            keepOnCurrentBuffer = false;
                            break;
                        case CreateRemoveMode.keepThisAndremoveCondition:
                            _removeConditionBufferList.Add(curBufferID);
                            break;
                        case CreateRemoveMode.removeThisAndkeepCondition:
                            keepOnCurrentBuffer = false;
                            break;
                        case CreateRemoveMode.keepThisAndCondition:
                            break;
                    }
                    if (!addCreatedBufferDict.ContainsKey(createData.bufferID))
                    {
                        addCreatedBufferDict.Add(createData.bufferID, delay);
                    }
                    else
                    {
                        addCreatedBufferDict[createData.bufferID] = delay;
                    }
                }
            }
            for (int i = 0; i < _removeConditionBufferList.Count; ++i)
            {
                RemoveBuffer(_removeConditionBufferList[i]);
            }
            foreach (KeyValuePair<int, float> createBuffer in addCreatedBufferDict)
            {
                AddBuffer(createBuffer.Key, createBuffer.Value, true, false);
            }
            ReleaseAddCreateBufferDict(addCreatedBufferDict);
            return keepOnCurrentBuffer;
        }

        public bool AddBuffer(int bufferID, float duration = -1f, bool isClient = true, bool conditionCreate = true, bool canReplaceByServer = true)
        {
            if (duration == -2f)    //配置时间为-2即为删除该buff
            {
                RemoveBuffer(bufferID);
                return false;
            }
            if (!CanAddBuffer(bufferID)) { return false; }
            CheckReplaceBuffer(bufferID);
            if (conditionCreate && !CreateBuffersByCondition(bufferID, duration)) { return false; }
            if (!TryRemoveOldBuffer(bufferID, isClient)) return false;
            var buffer = CombatLogicObjectPool.CreateBufferSubject(bufferID);
            buffer.SetOwner(_owner);
            buffer.isClientBuff = isClient;
            buffer.canReplaceByServer = canReplaceByServer;
            _bufferSubjects.Add(bufferID, buffer);
            _bufferSubjectsList.Add(bufferID);
            buffer.Start(duration);
            this._owner.skillManager.OnBufferChange(bufferID, true);//设置按钮位置
            return true;
        }

        //部分buff会被标记不容许服务器同ID的buff所顶替，这个会尝试做个顶替的操作
        public bool TryRemoveOldBuffer(int newBufferID, bool isClient = false)
        {
            if (_bufferSubjects.ContainsKey(newBufferID))
            {
                var bufferSubject = _bufferSubjects[newBufferID];
                if (!bufferSubject.canReplaceByServer && !isClient) return false;
                RemoveBuffer(newBufferID);
            }
            return true;
        }

        public void RemoveBuffer(int bufferID) //暂时假设同一个buffID只有有一个实例
        {
            if (_bufferSubjects.ContainsKey(bufferID)) 
            {
                var buff = _bufferSubjects[bufferID];
                _bufferSubjects.Remove(bufferID);
                _bufferSubjectsList.Remove(bufferID);
                if (!buff.IsOver())
                {
                    buff.Stop();
                }
                else
                {
                    buff.End();
                }
                CombatLogicObjectPool.ReleaseBufferSubject(buff);
                this._owner.skillManager.OnBufferChange(bufferID, false);
            }
        }

        public List<int> GetCurBufferIDList()
        {
            return _bufferSubjectsList;
        }

        public bool ContainsBuffer(int bufferID)
        {
            return _bufferSubjects.ContainsKey(bufferID);
        }

        public float GetBufferRemainTime(int bufferID)
        {
            return _bufferSubjects.ContainsKey(bufferID)?_bufferSubjects[bufferID].remainTime:0f;
        }

        public bool IsClientBuff(int bufferID)
        {
            return _bufferSubjects.ContainsKey(bufferID) ? _bufferSubjects[bufferID].isClientBuff : false;
        }

        public void OnBuffHpChange(int hp)
        {
            /*
            if (_owner.IsPlayer())
            {
                PerformaceManager.GetInstance().CreateDamageNumber(_owner.actor.position, hp, AttackType.ATTACK_HIT, true);
            }
            if (hp > 0)
            {
                _owner.OnAddStatisticsDamage(public_config.MISSION_ACTION_GET_CURE_ALL, hp);
            }*/
        }
    }
}
