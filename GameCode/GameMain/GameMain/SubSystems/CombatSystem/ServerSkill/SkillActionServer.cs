﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain;
using GameMain.ClientConfig;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    public class SkillActionServer
    {
        static public float DEFAULT_DURATION = 5;
        public int skillID;
        public int actionID;

        protected uint _mainTargetID;
        public uint mainTargetID
        {
            set {
                _mainTargetID = value;
                if (_actHandler != null) _actHandler.mainTargetID = value;
            }
        }

        protected EntityCreature _owner;
        protected ACTHandler _actHandler;

        public PbSpellAction pbSpellAction;

        protected SkillActionStage _curStage = SkillActionStage.STANDBY;
        public SkillActionStage curState
        {
            get { return this._curStage; }
        }

        protected SkillActionData _skillActionData;
        public SkillActionData skillActionData { get { return _skillActionData; } }
        protected List<SkillActionData> _adjustSkillActionDataList = new List<SkillActionData>();
        public List<SkillActionData> adjustSkillActionDataList { get { return _adjustSkillActionDataList; } }

        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();

        protected List<SkillActionMovement> _skillActionMovements = new List<SkillActionMovement>();

        protected RegionJudgeTime _regionJudgeTime;

        protected float _duration = 0;
        protected float _pastTime = 0;
        private float _startTimeStamp = 0;
        protected float _fireTime = 0;
        public float fireTime
        {
            get { return this._fireTime; }
        }

        public SkillActionServer()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillActionData data)
        {
            _duration = DEFAULT_DURATION;
            _pastTime = 0;
            actionID = data.actionID;
            _mainTargetID = 0;
            _curStage = SkillActionStage.STANDBY;
            _skillActionData = data;
            _adjustSkillActionDataList.Clear();
        }


        private void ShowEarlyWarning(PbSpellActionOrigin spellActionOrigin)
        {
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self
                && this._owner != MogoWorld.Player)
            {
                return;
            }
            var transform = this._owner.GetTransform();
            if (transform == null)return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = spellActionOrigin.origin_x * 0.01f;
            curOwnerPosition.z = spellActionOrigin.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(spellActionOrigin.origin_facex * 2, spellActionOrigin.origin_facey * 2, spellActionOrigin.origin_facez * 2), Vector3.one);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, duration);
        }

        private void ShowEarlyWarning(PbSpellAction spellAction)
        {
            
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if (spellAction.origin_facex == 361) //代表服务端找不到判定原点
            {
                return;
            }
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self
                && this._owner != MogoWorld.Player)
            {
                return;
            }
            var transform = this._owner.GetTransform();
            if (transform == null) return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = curOwnerPosition.x + spellAction.origin_x * 0.01f;
            curOwnerPosition.z = curOwnerPosition.z + spellAction.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(spellAction.origin_facex * 2, spellAction.origin_facey * 2, spellAction.origin_facez * 2), Vector3.one);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, duration);
        }

        public void OnFixTime(PbSpellActionOrigin spellActionOrigin)
        {
            var transform = this._owner.GetTransform();
            if (transform == null) return;
            var curOwnerPosition = _owner.position;
            curOwnerPosition.x = spellActionOrigin.origin_x * 0.01f;
            curOwnerPosition.z = spellActionOrigin.origin_y * 0.01f;
            Matrix4x4 judgeMatrix = Matrix4x4.TRS(curOwnerPosition, Quaternion.Euler(spellActionOrigin.origin_facex * 2, spellActionOrigin.origin_facey * 2, spellActionOrigin.origin_facez * 2), Vector3.one);
            _actHandler.actionActiveOrigin = judgeMatrix.position();
            _actHandler.OnFixTime(this._owner.actor);
        }

        public void AddAdjustSkillActionData(SkillActionData skillActionData)
        {
            if (_adjustSkillActionDataList.Contains(skillActionData)) return;
            _adjustSkillActionDataList.Add(skillActionData);
        }

        public void SetActionOriginForDelay(PbSpellActionOrigin spellActionOrigin)
        {
            ShowEarlyWarning(spellActionOrigin);
            OnFixTime(spellActionOrigin);
        }

        private void SetACTHandlerActiveOrigin()
        { 
            if(pbSpellAction != null){
                var curOwnerPosition = _owner.position;
                curOwnerPosition.x = curOwnerPosition.x + pbSpellAction.origin_x * 0.01f;
                curOwnerPosition.y = curOwnerPosition.y + pbSpellAction.origin_y * 0.01f;
                curOwnerPosition.z = curOwnerPosition.z + pbSpellAction.origin_z * 0.01f;
                _actHandler.actionActiveOrigin = curOwnerPosition;
            }
        }

        private void CalculateDuration()
        {
            for (int i = 0; i < _skillActionMovements.Count; i++)
            {
                _duration = Mathf.Max(_skillActionMovements[i].startTime - RealTime.time, _duration);
            }
            for (int i = 0; i < _skillEventList.Count; i++)
            {
                _duration = Mathf.Max(_skillEventList[i].delay, _duration);
            }
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        private void ResetSkillEventList()
        {
            _skillEventList.Clear();
            for (int i = 0; i < _skillActionData.skillEvents.Count; i++)
            {
                var eventData = _skillActionData.skillEvents[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                _skillEventList.Add(_skillActionData.skillEvents[i]);
            }
        }

        private void ResetRegionJudgeTime()
        {
            var data = this.GetRegionJudgeTime();
            if (data < 0)
            {
                _regionJudgeTime = RegionJudgeTime.OnFixTime;
            }
            else
            {
                _regionJudgeTime = (RegionJudgeTime)data;
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.Clear();
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public void Clear()
        {
            _owner = null;
            _skillActionData = null;
            pbSpellAction = null;
            _skillActionMovements.Clear();
        }

        bool IsAllEnd()
        {
            if (_skillActionMovements.Count > 0) return false;
            if (_skillEventList.Count > 0) return false;
            if (_fireTime > 0) return false;
            return true;
        }

        public SkillActionStage Update()
        {
            if (_curStage == SkillActionStage.STANDBY)
            {
                Begin();
                DoingEvents();
            }
            else if (_curStage == SkillActionStage.ACTIVE)
            {
                _pastTime = RealTime.time - _startTimeStamp;
                if (_pastTime >= _duration && IsAllEnd())
                {
                    End();
                }
                else
                {
                    DoingEvents();
                    Moving();
                }
            }
            return _curStage;
        }

        void Begin()
        {
            _curStage = SkillActionStage.ACTIVE;
            OnBegin();
        }

        protected void OnBegin()
        {
            _startTimeStamp = RealTime.time;
            ResetFireTime();
            ResetRegionJudgeTime();
            ResetSkillEventList();
            SetSelfMovement();
            CalculateDuration();
            SetACTHandlerActiveOrigin();
            if (_regionJudgeTime == RegionJudgeTime.OnSkillActive
                || _regionJudgeTime == RegionJudgeTime.OnActionActive)
                ShowEarlyWarning(this.pbSpellAction);
        }

        public void End()
        {
            _curStage = SkillActionStage.ENDED;
        }

        private void ResetFireTime()
        {
            _fireTime = this.pbSpellAction.fire_delay_time * 0.001f + RealTime.time;
        }

        public void Judge(PbSpellDamageInfo skillDamageInfo)
        {
            _hitFlyTargetDict.Clear();
            var damages = skillDamageInfo.damages;
            for (int i = 0; i < damages.Count; i++)
            {
                PbSpellDamageUnit damageInfo = damages[i];
                var events = damageInfo.events;
                EntityCreature target = MogoWorld.GetEntity(damageInfo.target_id) as EntityCreature;
                if (target != null && target.actor != null)
                {
                    _actHandler.OnTargetBeHit(_owner.actor, target, delegate()
                    {
                        if (damageInfo.damage > 0)
                        {
                            TargetBeHit(target);
                            if (_owner.NeedHitFly())
                            {
                                if (_owner.IsPlayer())
                                {
                                    PlayerEventManager.GetInstance().OnHitAndDamageOther(target.id);
                                }
                                if (damageInfo.damage > 0 && target.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER && target.cur_hp - damageInfo.damage <= 0)
                                {
                                    HandleTargetBeHitFlyOnDeath(target);
                                    SetTargetMovBeHitFly(target);
                                }
                            }
                        }
                    });

                    //AttackType attackType = CombatSystemTools.DamageMode2AttackType(events);
                    if (CombatSystemTools.CanShowDamage(this._owner, target))
                    {
                        bool isPlayer = target.IsPlayer();
                        int userData = damageInfo.userdata; //会心伤害
                        int userData2 = damageInfo.userdata2; //伤害吸收
                        AttackType attackType = AttackType.ATTACK_MISS;

                        if (isPlayer)
                        {
                            attackType = CombatSystemTools.DamageMode2AttackType(events);
                            if (attackType != AttackType.TREAT)
                            {
                                PlayerEventManager.GetInstance().BeHit(_owner.id);
                            }
                            int result = (int)damageInfo.damage + userData - userData2;
                            PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, result, (int)attackType, isPlayer);
                        }
                        else
                        {
                            if (userData > 0)
                            {
                                attackType = AttackType.DEADLY;
                                PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, userData, (int)attackType, isPlayer);
                            }

                            if (userData2 > 0)
                            {
                                attackType = AttackType.ABSORB;
                                PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, userData2, (int)attackType, isPlayer);
                            }

                            attackType = CombatSystemTools.DamageMode2AttackType(events);
                            if (this._owner.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_PET && this._owner.owner_id == EntityPlayer.Player.id)
                            {
                                //玩家宠物
                                attackType = AttackType.PET_HIT;
                            }

                            if (this._owner.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_FABAO && this._owner.owner_id == EntityPlayer.Player.id)
                            {
                                //玩家法宝
                                attackType = AttackType.TREASURE_HIT;
                            }
                            if (this._owner.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                            {
                                //可飘血显示的友方怪物
                                attackType = AttackType.ATTACK_HIT;
                            }
                            if (this._owner.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_TRAP)
                            {
                                //自己的技能召唤实体
                                attackType = AttackType.ATTACK_HIT;
                            }
                            //target.OnChangeHp(_owner.id, (int)damageInfo.damage, 0, (int)attackType);
                            PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, (int)damageInfo.damage - userData2, (int)attackType, isPlayer);
                        }
                    }

                    if (damageInfo.can_move == 1)   //水平位移
                    {
                        SetTargetMovement(target, skillDamageInfo, damageInfo);
                        target.actor.FaceTo(_owner.position);
                    }
                    else if (damageInfo.can_move == 2) //没有水平位移
                    {
                        SetTargetUpSpeedMovement(target);
                        target.actor.FaceTo(_owner.position);
                    }
                }
            }
            var originPosX = (skillDamageInfo.ent_x + skillDamageInfo.origin_x) * 0.01f;
            var originPosY = (skillDamageInfo.ent_y + skillDamageInfo.origin_y) * 0.01f;
            var originPosZ = (skillDamageInfo.ent_z + skillDamageInfo.origin_z) * 0.01f;
            _actHandler.actionJudgeOrigin = new Vector3(originPosX, originPosY, originPosZ);
            _actHandler.OnJudge(this._owner.actor);
            if (_duration == DEFAULT_DURATION) _duration = 0;
            CalculateAddEp();
        }

        protected Dictionary<uint, int> _hitFlyTargetDict = new Dictionary<uint, int>();
        private void HandleTargetBeHitFlyOnDeath(EntityCreature target)
        {
            bool needToBeHitFly = CheckTargetNeedToBeHitFly(target);
            if (needToBeHitFly && !_hitFlyTargetDict.ContainsKey(target.id))
            {
                target.actorDeathController.flyingTime = global_params_helper.death_fly_time;
                _hitFlyTargetDict.Add(target.id, 1);
            }
        }

        private bool CheckTargetNeedToBeHitFly(EntityCreature target)
        {
            if (!_owner.NeedHitFly()) return false;
            if (target.entityType != EntityConfig.ENTITY_TYPE_NAME_MONSTER) return false;
            return monster_helper.GetShowDeathFly(target.monsterId);
        }

        void SetTargetMovBeHitFly(EntityCreature target)
        {
            if (target != null && target.actor != null)
            {
                if (_hitFlyTargetDict.ContainsKey(target.id))
                {
                    target.actor.FaceTo(_owner.position);
                    SetTargetMovement(target);
                }
            }
        }

        int GetTargetMovType(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return 2;
            }
            return this.GetTargetMovType();
        }

        List<float> GetTargetMovArg(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return global_params_helper.death_fly_move_args;
            }
            return this.GetTargetMovArg();
        }

        List<float> FixMoveArgs(MovementType movementType, List<float> srcMoveArgs, bool forTargetMove = false)
        {
            int argNum = 8;
            List<float> curMoveArgs = srcMoveArgs;
            if (MovementType.AccordingAuto == movementType)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target == null && srcMoveArgs.Count >= argNum * 2)
                {
                    curMoveArgs = new List<float>();
                    for (int i = argNum; i < argNum * 2; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            else if (forTargetMove)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target != null && target.IsOnAir() && srcMoveArgs.Count >= argNum * 2)
                {
                    curMoveArgs = new List<float>();
                    for (int i = argNum; i < argNum * 2; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            return curMoveArgs;
        }

        void SetTargetMovement(EntityCreature moveEntity)
        {
            if (moveEntity == null || moveEntity.actor == null || moveEntity.actorDeathController == null) return;
            MovementType moveType = (MovementType)GetTargetMovType(moveEntity.id);
            if (moveType == MovementType.None) return;
            var moveArgs = GetTargetMovArg(moveEntity.id);
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs, true);
            Matrix4x4 moveTarget = Matrix4x4.identity;
            Matrix4x4 moveSource = _owner.GetTransform().localToWorldMatrix;
            switch (moveType)
            {
                case MovementType.None:
                    return;
                case MovementType.AccordingSelf:
                    moveTarget = moveSource;
                    break;
                case MovementType.AccordingTarget:
                    moveTarget = moveEntity.GetTransform().localToWorldMatrix;
                    moveEntity.checkCrashWall = true;
                    break;
                default:
                    return;
            }
            if (moveTarget == Matrix4x4.identity) return;
            var movement = GetActionMovement(moveEntity, moveSource, moveTarget, moveType, curMoveArgs);
            var srcPosition = moveEntity.position;
            var dir = movement.targetPosition - moveEntity.position;
            moveEntity.actor.FaceTo(moveEntity.position - dir);
            Vector3 targetPosition = movement.targetPosition;
            var dis = Mathf.Min(dir.magnitude, movement.maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.actorDeathController.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate, false, false);
        }

        private void CalculateAddEp()
        {
            /*
            var usecosts = this.GetFireCosts();
            if (_owner == EntityPlayerLuaBase.Player && usecosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
            {
                int oldEp = _owner.cur_ep;
                Int16 cur_ep = (Int16)(_owner.cur_ep - usecosts[public_config.ITEM_SPECIAL_SPELL_ENERGY]);
                cur_ep = (Int16)Mathf.Min(_owner.max_ep, Mathf.Max(0, cur_ep));
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.ADD_EP, oldEp, cur_ep);
            }*/
        }

        void TargetBeHit(EntityCreature target)
        {
            if (target.actor != null)
            {
                if (CombatSystemTools.CanShowDamage(this._owner, target))
                {
                    var hlSetting = global_params_helper.GetHighLightSetting();
                    target.actor.equipController.HighLight(hlSetting[0], hlSetting[1]);                   
                }
                target.OnHit();
            }
        }

        #region 持续效果

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void Moving()
        {
            for (int i = _skillActionMovements.Count - 1; i >= 0; i--)
            {
                var movement = _skillActionMovements[i];
                if (movement.startTime < RealTime.time)
                {
                    _skillActionMovements.RemoveAt(i);
                    _owner.checkCrashWall = false;
                    ExecuteMovement(movement);
                }
            }
        }

        void ExecuteMovement(SkillActionMovement movement)
        {
            var moveEntity = CombatSystemTools.GetCreatureByID(movement.moveEntityID);
            if (moveEntity == null) return;
            var srcPosition = moveEntity.position;

            Vector3 targetPosition = movement.targetPosition;
            var dir = targetPosition - srcPosition;
            var dis = Mathf.Min(dir.magnitude, movement.maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
            }
        }

        #endregion 持续效果

        #region 瞬间效果

        List<float> FixMoveArgs(MovementType movementType, List<float> srcMoveArgs)
        {
            int argNum = 8;
            List<float> curMoveArgs = srcMoveArgs;
            if (MovementType.AccordingAuto == movementType)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target == null && srcMoveArgs.Count >= argNum * 2)
                {
                    curMoveArgs = new List<float>();
                    for (int i = argNum; i < argNum * 2; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            return srcMoveArgs;
        }

        SkillActionMovement GetSelfActionMovement(List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            int idx = 0;
            movement.offsetX = curMoveArgs[idx++] * 0.01f;
            movement.offsetY = curMoveArgs[idx++] * 0.01f;
            movement.offsetH = curMoveArgs[idx++] * 0.01f;
            var delay = curMoveArgs[idx++] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.maxMoveDistance = curMoveArgs[idx++] * 0.01f;
            movement.baseSpeed = curMoveArgs[idx++] * 0.01f;
            movement.accelerate = curMoveArgs[idx++] * 0.01f;
            return movement;
        }

        SkillActionMovement GetSelfActionMovement(PbSpellAction pbSpellAction)
        {
            SkillActionMovement movement = new SkillActionMovement();
            movement.offsetX = pbSpellAction.dest_x;
            movement.offsetY = pbSpellAction.dest_z;
            movement.offsetH = pbSpellAction.dest_y;
            movement.startTime = RealTime.time + pbSpellAction.delay_time * 0.001f;
            movement.maxMoveDistance = pbSpellAction.max_distance * 0.01f;
            movement.baseSpeed = pbSpellAction.speed * 0.01f;
            if (ACTSystem.ACTMathUtils.Abs(pbSpellAction.acceleration) < 100000)
            {
                movement.accelerate = pbSpellAction.acceleration * 0.01f;
                movement.percent = false;
            }
            else
            {
                movement.accelerate = pbSpellAction.acceleration % 100000 * 0.01f;
                movement.percent = true;
            }
            movement.targetPosition = _owner.position;
            movement.targetPosition.x = movement.targetPosition.x + pbSpellAction.dest_x * 0.01f;
            movement.targetPosition.y = movement.targetPosition.y + pbSpellAction.dest_y * 0.01f;
            movement.targetPosition.z = movement.targetPosition.z + pbSpellAction.dest_z * 0.01f;
            movement.moveEntityID = _owner.id;
            return movement;
        }

        void SetSelfMovement()
        {
            if (pbSpellAction.can_move == 0) return;
            MovementType moveType = (MovementType)this.GetSelfMovType();
            if (moveType == MovementType.None) return;
            var movement = GetSelfActionMovement(pbSpellAction);
            _skillActionMovements.Add(movement);
        }

        SkillActionMovement GetActionMovement(EntityCreature moveEntity, Matrix4x4 moveSource, Matrix4x4 moveTarget, MovementType moveType, List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            int idx = 0;
            int axisType = (int)curMoveArgs[idx++];
            movement.offsetX = curMoveArgs[idx++] * 0.01f;
            movement.offsetY = curMoveArgs[idx++] * 0.01f;
            movement.offsetH = curMoveArgs[idx++] * 0.01f;
            var delay = curMoveArgs[idx++] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.maxMoveDistance = curMoveArgs[idx++] * 0.01f;
            movement.baseSpeed = curMoveArgs[idx++] * 0.01f;
            float accelerate = curMoveArgs[idx++];
            if (ACTSystem.ACTMathUtils.Abs(accelerate) < 100000)
            {
                movement.accelerate = accelerate * 0.01f;
                movement.percent = false;
            }
            else
            {
                movement.accelerate = accelerate % 100000 * 0.01f;
                movement.percent = true;
            }
            movement.movementType = moveType;
            movement.moveEntityID = moveEntity.id;
            Vector3 targetPos = moveTarget.position();
            targetPos = FixTargetPosition(axisType, targetPos);
            Vector3 sourcePos = moveSource.position();
            if (moveSource == moveTarget)
            {
                Matrix4x4 m1 = Matrix4x4.identity;
                m1.SetColumn(3, new Vector4(movement.offsetY, movement.offsetH, movement.offsetX, 1));
                moveSource = moveSource * m1;
                movement.targetPosition = moveSource.position();
            }
            else
            {
                var direction = (targetPos - sourcePos).normalized;
                if (axisType != AxisType.RelativeAxis)
                {
                    Vector3 tempTargetPos = targetPos;
                    tempTargetPos.y = sourcePos.y;
                    direction = (tempTargetPos - sourcePos).normalized;
                }
                Matrix4x4 mm = Matrix4x4.identity;
                mm.SetTRS(targetPos, Quaternion.LookRotation(direction), Vector3.one);
                var fontDriection = mm.forward().normalized * movement.offsetX;
                var rightDirection = mm.right().normalized * movement.offsetY;
                var upDirection = mm.upwards().normalized * movement.offsetH;
                var offset = fontDriection + rightDirection + upDirection;
                movement.targetPosition = new Vector3(targetPos.x + offset.x, targetPos.y + offset.y, targetPos.z + offset.z);
            }
            return movement;
        }

        Vector3 FixTargetPosition(int axisType, Vector3 oldPosition)
        {
            Vector3 result = oldPosition;
            switch (axisType)
            {
                case AxisType.StandardAxis:
                    result = oldPosition;
                    break;
                case AxisType.StandardAxisAtSameHeight:
                    result.y = _owner.position.y;
                    break;
                case AxisType.StandardAxisAtProjection:
                    result.y = CombatSystemTools.GetGroundPosition(_owner.position).y;
                    break;
                case AxisType.RelativeAxis:
                    result = oldPosition;
                    break;
            }
            return result;
        }

        SkillActionMovement GetTargetActionMovement(List<float> curMoveArgs)
        {
            SkillActionMovement movement = new SkillActionMovement();
            movement.offsetX = curMoveArgs[0] * 0.01f;
            movement.offsetY = curMoveArgs[1] * 0.01f;
            movement.maxMoveDistance = curMoveArgs[2] * 0.01f;
            var delay = curMoveArgs[3] * 0.001f;
            movement.startTime = RealTime.time + delay;
            movement.baseSpeed = curMoveArgs[4] * 0.01f;
            movement.accelerate = curMoveArgs[5] * 0.01f;
            if (curMoveArgs.Count >= 7) movement.upSpeed = curMoveArgs[6] * 0.01f;
            if (curMoveArgs.Count >= 8) movement.upAccelerate = curMoveArgs[7] * 0.01f;
            return movement;
        }

        SkillActionMovement GetTargetActionMovement(PbSpellDamageUnit pbSpellDamageUnit)
        {
            SkillActionMovement movement = new SkillActionMovement();
            if (ACTSystem.ACTMathUtils.Abs(pbSpellDamageUnit.acceleration) < 100000)
            {
                movement.accelerate = pbSpellDamageUnit.acceleration * 0.01f;
                movement.percent = false;
            }
            else
            {
                movement.accelerate = pbSpellDamageUnit.acceleration % 100000 * 0.01f;
                movement.percent = true;
            }
            movement.maxMoveDistance = pbSpellDamageUnit.max_distance * 0.01f;
            movement.startTime = RealTime.time + pbSpellDamageUnit.delay_time * 0.001f;
            movement.baseSpeed = pbSpellDamageUnit.speed * 0.01f;
            movement.offsetX = pbSpellDamageUnit.dest_x * 0.01f;
            movement.offsetY = pbSpellDamageUnit.dest_z * 0.01f;
            movement.offsetH = pbSpellDamageUnit.dest_y * 0.01f;

            return movement;
        }

        void SetTargetMovement(EntityCreature moveEntity, PbSpellDamageInfo skillDamageInfo, PbSpellDamageUnit pbSpellDamageUnit)
        {
            MovementType moveType = (MovementType)this.GetTargetMovType();
            if (moveType == MovementType.None) return;
            Vector3 moveSource = Vector3.zero;
            Vector3 moveTarget = Vector3.zero;
            Vector3 casterFace = new Vector3(skillDamageInfo.face_x * 2f, skillDamageInfo.face_y * 2f, skillDamageInfo.face_z * 2f);
            var casterPosX = skillDamageInfo.ent_x * 0.01f;
            var casterPosY = skillDamageInfo.ent_y * 0.01f;
            var casterPosZ = skillDamageInfo.ent_z * 0.01f;
            var casterPos = new Vector3(casterPosX, casterPosY, casterPosZ);
            var targetPosX = casterPosX + pbSpellDamageUnit.offset_x * 0.01f;
            var targetPosY = casterPosY + pbSpellDamageUnit.offset_y * 0.01f;
            var targetPosZ = casterPosZ + pbSpellDamageUnit.offset_z * 0.01f;
            //服务器传来的的受击者坐标，目前不做处理，以客户端模型坐标为准
            var targetPos = new Vector3(targetPosX, targetPosY, targetPosZ);
            var movement = GetTargetActionMovement(pbSpellDamageUnit);
            Vector3 targetPosition;
            switch (moveType)
            {
                case MovementType.AccordingSelf:
                case MovementType.AccordingJudgePoint:
                    moveSource = casterPos;
                    targetPosition = new Vector3(moveSource.x + pbSpellDamageUnit.dest_x * 0.01f, moveSource.y + pbSpellDamageUnit.dest_y * 0.01f, moveSource.z + pbSpellDamageUnit.dest_z * 0.01f);
                    break; 
                case MovementType.AccordingTarget:
                case MovementType.AccordingJudgeTarget:
                    moveSource = casterPos;
                    targetPosition = new Vector3(moveSource.x + pbSpellDamageUnit.dest_x * 0.01f, moveSource.y + pbSpellDamageUnit.dest_y * 0.01f, moveSource.z + pbSpellDamageUnit.dest_z * 0.01f);
                    break;
                default:
                    return;
            }
            //受击者坐标，目前不做处理，以客户端模型坐标为准
            var srcPosition = moveEntity.position;
            var dir = targetPosition - srcPosition;
            var dis = Mathf.Min(dir.magnitude, movement.maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.checkCrashWall = true;
            moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate, movement.percent);
        }

        void SetTargetUpSpeedMovement(EntityCreature moveEntity)
        {
            MovementType moveType = (MovementType)this.GetTargetMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetTargetMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            var movement = GetTargetActionMovement(curMoveArgs);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
            }
        }
        #endregion
    }
}
