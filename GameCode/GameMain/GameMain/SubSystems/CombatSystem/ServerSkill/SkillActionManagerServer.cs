﻿using Common.Structs.ProtoBuf;
using GameMain;
using System;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public class SkillActionManagerServerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillActionManagerServer
    {
        private EntityCreature _owner;

        private List<SkillActionServer> _skillActions = new List<SkillActionServer>();
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public SkillActionManagerServer() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<SkillActionManagerServerUpdateDelegate>("SkillActionManagerServer.Update", Update);
            }
            isLooping = true;
        }

        public void Release()
        {
            isLooping = false;
            //MogoEngine.MogoWorld.UnregisterUpdate("SkillActionManagerServer.Update", Update);
            _skillActions.Clear();
        }

        public void AddSkillAction(SkillActionServer skillAction)
        {
            RemoveSkillAction(skillAction.actionID);
            AdjustTools.AdjustSkillAction(skillAction, _owner.skillManager.GetCurAdjustSkillActionIDList()); 
            _skillActions.Add(skillAction);
            skillAction.Update();
        }

        private void RemoveSkillAction(int actionID)
        {
            for (int i = _skillActions.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActions[i];
                if (skillAction.actionID == actionID)
                {
                    _skillActions.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillActionServer(skillAction);
                }
            }
        }

        private void Update()
        {
            if (!isLooping) return;
            UpdateSkillActions();
        }

        void UpdateSkillActions()
        {
            for (int i = _skillActions.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActions[i];
                var result = skillAction.Update();
                if (result == SkillActionStage.ENDED)
                {
                    _skillActions.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillActionServer(skillAction);
                }
            }
        }

        public SkillActionServer GetCurSkillActionByID(int actionID)
        {
            for (int i = 0; i < _skillActions.Count; i++)
            {
                if (_skillActions[i].actionID == actionID)
                {
                    return _skillActions[i];
                }
            }
            return null;
        }

        public void JudgeSkillActionByServer(UInt16 skillID, UInt16 skillActionID, PbSpellDamageInfo skillDamageInfo)
        {
            var skillAction = GetCurSkillActionByID(skillActionID);
            if (skillAction == null)
            {
                return;
            }
            skillAction.Judge(skillDamageInfo);
        }

        public void OnSetActionOriginByServer(PbSpellActionOrigin spellActionOrigin)
        {
            var skillAction = GetCurSkillActionByID((int)spellActionOrigin.action_id);
            if (skillAction == null)
            {
                return;
            }
            skillAction.SetActionOriginForDelay(spellActionOrigin);
        }
    }
}
