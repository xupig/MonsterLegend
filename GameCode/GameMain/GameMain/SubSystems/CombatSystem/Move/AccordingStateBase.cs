﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;

namespace GameMain.CombatSystem
{
    public enum AccordingMode
    {
        AccordingActor,
        AccordingEntity,
        AccordingStick,
        AccordingStickInSkill,
        AccordingMovement,
        AccordingPath,
        AccordingLine,
        AccordingDirection,
        AccordingAnimationClip,
        AccordingElevator,
        AccordingSteeringWheel,
        AccordingTouch,
        AccordingTouchInSkill,
        AccordingFly,
        AccordingGlide,
        AccordingSkill,
        AccordingPathFly,
        AccordingToDirection
    }

    public struct AccordingModeComparer : IEqualityComparer<AccordingMode>
    {
        public bool Equals(AccordingMode x, AccordingMode y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(AccordingMode obj)
        {
            return (int)obj;
        }
    }


    public class AccordingStateBase
    {
        protected EntityCreature _owner;

        public AccordingMode modeType;

        public AccordingStateBase(EntityCreature owner)
        {
            _owner = owner;
        }

        public virtual void OnEnter()
        {
            
        }

        public virtual bool Update()
        {
            return true;
        }

        public virtual void OnLeave()
        {

        }

        public virtual void Stop()
        {
            var actor = movingActor;
            if (actor == null) return;
            actor.actorController.Stop();
        }

        public virtual void StopVertical()
        {
            var actor = movingActor;
            if (actor == null) return;
            actor.actorController.StopVertical();
        }

        public virtual List<float> GetPath()
        {
            return new List<float>();
        }

        public ACTActor movingActor
        {
            get
            {
                return _owner.controlActor;
            }
        }
    }
}
