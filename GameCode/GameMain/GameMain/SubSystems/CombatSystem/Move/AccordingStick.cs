﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using Common.States;
using MogoEngine.Events;

using GameData;

using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    //player专用，常规状态下根据摇杆状态移动
    public class AccordingStick : AccordingStateBase
    {
        public AccordingStick(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingStick;
            InitAdjustCameraParams();
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            //Debug.LogError("Update:" + ControlStickState.strength);

            CheckingStick();
            bool adjustCamera = false;
            Vector3 moveDirection = Vector3.zero;
            bool sprint = false;
            while (true)
            {
                if (ControlStickState.strength == 0)
                {
                    actor.actorController.Stop();
                    if (actor.isFly)
                    {
                        actor.actorController.StopVertical();
                    }
                    sprint = false;
                    adjustCamera = false;
                    break;
                }
                moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                if (_owner.CanTurn())
                {
                    _owner.actor.forward = moveDirection;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed;
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    if (actor.isFly)
                    {
                        actor.actorController.verticalSpeed = movement.y;
                    }
                    actor.actorController.Move(movement);
                    sprint = true;
                    adjustCamera = true;
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                    sprint = false;
                    adjustCamera = false;
                }
                break;
            }
            TrySprint(sprint);
            TryAdjustCamera(adjustCamera, moveDirection);
            return true;
        }

        public override void OnLeave()
        {
            TryAdjustCamera(false, Vector3.zero);
            TrySprint(false);
        }

        public override void Stop()
        {
            base.Stop();
        }

        private void TrySprint(bool state)
        {
            if (state)
            {
                //ari PlayerSprintManager.Instance.TryStartSprint();
            }
            else
            {
                //ari PlayerSprintManager.Instance.TryStopSprint();
            }
        }

        float _nextClearTime = 0;
        private void TryClearPlayerAction()
        {
            if (_nextClearTime < UnityPropUtils.realtimeSinceStartup)
            {
                //ari PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = UnityPropUtils.realtimeSinceStartup + 0.0f;
            }
        }

        private float _lastStickStrength = 0;
        private void CheckingStick()
        {
            var actor = movingActor;
            if (actor == null) return;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return;

            if (_lastStickStrength == 0 && _lastStickStrength != ControlStickState.strength)
            {
                //ari EventDispatcher.TriggerEvent(MainUIEvents.CLOSE_PLAYERINFO);
            }
            _lastStickStrength = ControlStickState.strength;
        }

        bool _canAdjust = false;
        float _canAdjustTime = 0;
        float _moveBeginTime = 0;
        float _targetAngle = 10;
        float _angleSpeed = 45;
        float _coefficient = 1;
        private void TryAdjustCamera(bool state, Vector3 moveDirection)
        {
            if (!_canAdjust) return;
            if (state)
            {
                if (_moveBeginTime == 0)
                {
                    _moveBeginTime = UnityPropUtils.realtimeSinceStartup;
                }
                else if (UnityPropUtils.realtimeSinceStartup - _moveBeginTime > _canAdjustTime)
                {
                    //GameMain.GlobalManager.CameraManager.GetInstance().RecoverHorizontally(moveDirection, _targetAngle, _angleSpeed, _coefficient);
                }
            }
            else
            {
                _moveBeginTime = 0;
            }
        }

        private void InitAdjustCameraParams()
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.player_running_camera_setting).Split(',');
            _canAdjust = int.Parse(args[0]) == 1;
            _canAdjustTime = float.Parse(args[1]);
            _targetAngle = float.Parse(args[2]);
            _angleSpeed = float.Parse(args[3]);
            _coefficient = float.Parse(args[4]);
        }
    }
}
