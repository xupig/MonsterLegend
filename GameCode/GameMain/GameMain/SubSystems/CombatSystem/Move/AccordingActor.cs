﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;

namespace GameMain.CombatSystem
{
    public class AccordingActor : AccordingStateBase
    {
        public AccordingActor(EntityCreature owner)
            :base(owner)
        {
            modeType = AccordingMode.AccordingActor;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            if (movingActor != null)
            {
                _owner.position = movingActor.position;
            }
            return true;
        }

        public override void OnLeave()
        {

        }
    }
}
