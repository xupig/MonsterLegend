﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;

using Common.Utils;

namespace GameMain.CombatSystem
{
    public class AccordingPath : AccordingStateBase
    {
        private Vector3 _targetPosition = Vector3.zero;
        private float _moveSpeed = 0;
        private List<float> _path;
        private Vector3 _startPoint = Vector3.zero;     //每段寻路的起始点
        private Vector3 _nextPoint = Vector3.zero;      //每段寻路的终点
        private float _currentPartDistance = 0f;        //每段寻路的距离
        private Vector3 moveByPathTargetPosition = Vector3.zero;
        private float _moveTime = 0;
        private float _distanceCache = 0;
        private float _startLockTimeStamp = 0;
        private float _endDistance = 0;
        private Queue<float> _pathQueue = new Queue<float>();
        private bool _refresh = false; //是否用玩家实时速度值

        public AccordingPath(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingPath;
        }

        public bool Reset(Vector3 targetPosition, float speed, float endDistance = 0, bool refresh = false)
        {
            var actor = movingActor;
            if (actor == null) return false;
            _endDistance = endDistance;
            _moveSpeed = speed;
            _refresh = refresh;
            _targetPosition = targetPosition;
            if ((moveByPathTargetPosition - _targetPosition).magnitude < 0.5f) return false;
            moveByPathTargetPosition = _targetPosition;

            var sPosition = actor.position;
            _path = FindPathManager.GetInstance().GetPath(sPosition.x, sPosition.z, _targetPosition.x, _targetPosition.z, _owner.actor.modifyLayer);
            _path = FindPathManager.GetInstance().FixPath(_path);

            _pathQueue.Clear();
            for (int i = 0; i < _path.Count; i++)
            {
                _pathQueue.Enqueue(_path[i]);
            }

            _currentPartDistance = 0;
            if (_pathQueue.Count > 2)
            {
                SetNextPoint();//路径中的第一个点为当前玩家所在位置，可以不要
            }
            if (sPosition.x == _targetPosition.x && sPosition.z == _targetPosition.z)
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            if (!SetNextPoint())
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            _distanceCache = 0;
            _startLockTimeStamp = 0;
            return true;
        }

        public override void OnEnter()
        {

        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            bool result = true;
            if (_refresh)
            {
                _moveSpeed = _owner.moveManager.curMoveSpeed;
            }
            while (true)
            {
                if (!_owner.CanActiveMove())
                {
                    actor.actorController.Stop();
                    return false;
                }
                _nextPoint.y = actor.position.y;
                var moveDirection = _nextPoint - actor.position;
                var distance = moveDirection.magnitude;

                bool setNextPointResult = true;
                Vector3 vec = _startPoint - actor.position;
                vec.y = 0;
                var distanceToStartPoint = vec.magnitude;

                var toEndDistance = (actor.position - _targetPosition).magnitude;
                if (toEndDistance < this._endDistance)
                {
                    actor.actorController.Stop();
                    _owner.position = actor.position;
                    return false;
                }
                if (distance < _moveSpeed * UnityPropUtils.deltaTime && !HasNextPoint())
                {
                    actor.position = _nextPoint;
                    _owner.position = actor.position;
                    return false;
                }

                if (ACTSystem.ACTMathUtils.Abs(_currentPartDistance - distanceToStartPoint) <= 0.5f && HasNextPoint())
                {
                    setNextPointResult = SetNextPoint();
                }
                if (CheckLockedInBlock(distance))//卡障碍的问题 3秒后直接跳点
                {
                    actor.position = _nextPoint;
                    setNextPointResult = SetNextPoint();
                }
                if (!setNextPointResult)
                {
                    result = false;
                    break;
                }
                _moveTime = _moveTime - UnityPropUtils.deltaTime;
                Vector3 movement = moveDirection.normalized * _moveSpeed;
                movement.y = 0;
                actor.actorController.Move(movement);
                actor.actorController.SetSlowRate(_moveSpeed);
                _owner.position = actor.position;
                actor.FaceTo(_nextPoint);
                break;
            }
            return result;
        }

        private bool CheckLockedInBlock(float distance)
        {
            bool result = false;
            if (distance == 0)
            {
                _distanceCache = distance;
                return result;
            }
            if (ACTSystem.ACTMathUtils.Abs(distance - _distanceCache) < 0.01f)
            {
                if (_startLockTimeStamp == 0)
                {
                    _startLockTimeStamp = UnityPropUtils.realtimeSinceStartup;
                    return result;
                }
                if (UnityPropUtils.realtimeSinceStartup - _startLockTimeStamp > 3f)
                {
                    result = true;
                    _startLockTimeStamp = 0;
                    _distanceCache = 0;
                    return result;
                }
                _distanceCache = distance;
                return result;
            }
            _distanceCache = distance;
            _startLockTimeStamp = 0;
            return result;
        }

        public override void OnLeave()
        {
            if (_path != null) _path.Clear();
            moveByPathTargetPosition = Vector3.zero;
        }

        private bool HasNextPoint()
        {
            return _pathQueue.Count > 0;
        }

        private bool SetNextPoint()
        {
            if (_pathQueue.Count == 0)
            {
                return false;
            }
            if (_nextPoint != Vector3.zero)
            {
                _startPoint = _nextPoint;
            }

            _nextPoint.x = _pathQueue.Dequeue();
            _nextPoint.z = _pathQueue.Dequeue();
            _nextPoint.y = movingActor.position.y;
            
            if (_startPoint != Vector3.zero)
            {
                Vector3 vec = _startPoint - _nextPoint;
                vec.y = 0;
                _currentPartDistance = vec.magnitude;
            }
            return true;
        }

        public override List<float> GetPath()
        {
            if (movingActor)
            {
                var result = new List<float>();
                result.Add(movingActor.position.x);
                result.Add(movingActor.position.z);
                result.AddRange(_path);
                return result;
            }
            else
            {
                return _path;
            }
        }
    }
}
