﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace GameMain.CombatSystem
{
    //判断区域类型
    public enum TargetRangeType
    {
        SectorRange = 1,  //扇形块
        SphereRange = 2,  //圆球范围
        ForwardRectangleRange = 3, //前方长方体范围
        CylinderRange = 4,  //圆柱范围
    }

    //判断目标类型
    public enum TargetType
    {
        None = 0,   //None和Enemy都表示Enemy
        Enemy = 1,
        Friend = 2,
        MySelf = 3,
        Assign = 4,
        Caller = 5,
    }

    //判断目标类型
    public enum TargetFilterType
    {
        MinAngle = 0,
        Closest = 1,
        HaveBuffer = 2,
        RangeAndAngle = 3,
        Random = 4,
        Farthest = 5,
        EntityType = 6
    }

    //判断原点类型
    public enum OriginType
    { 
        Self = 0,
        Target = 1,
        Auto = 2,
        Custom = 3,
    }

    //判定攻击区域的时机
    public enum RegionJudgeTime
    {
        OnSkillActive = 1,
        OnActionActive = 2,
        OnActionJudge = 3,
        OnFixTime = 4,
    }

    //位移定位类型
    public enum MovementType
    { 
        None = 0,
        AccordingSelf = 1,
        AccordingTarget = 2,
        AccordingAuto = 3,
        AccordingJudgePoint = 4,
        AccordingJudgeTarget = 5,
        AccordingTargetProjection = 6,
        AccordingBackInTime = 7,
    }

    public static class AxisType
    {
        public const int StandardAxis = 1;
        public const int StandardAxisAtSameHeight = 2;
        public const int StandardAxisAtProjection = 3;
        public const int RelativeAxis = 4;
    }

    //调整定位类型
    public enum AdjustType
    {
        None = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
        SIX = 6,
        SEVEN = 7,
        EIGHT = 8,
        NINE = 9,
    }

    //目标锁定
    public enum FaceLockMode
    {
        None = 0,
        LockOnSkillActive = 1,
        LockDuringSkill = 2,
    }

    public enum SearchTargetRepeatType
    { 
        Default = 0,
        ForEveryAction = 1,
        ForEveryActionUnique = 2,
    }

    public enum SearchMainTargetType
    {
        CurLockTarget = 0,//当前锁定目标
        GlobalSearch = 1,  //全局搜索
    }

    public enum AttackType
    {
        ATTACK_HIT = 1,
        ATTACK_CRITICAL = 2,
        ATTACK_STRIKE = 3,
        ATTACK_MISS = 4,
        TREAT = 5,
        PARRY = 6,   //格挡
        DEADLY = 9,  //会心
        ABSORB = 10 , //伤害吸收
        INVINCIBLE = 11,  //无敌
        GET_HIT = 12,  //受击
        PET_HIT = 13,  //宠物攻击
        TREASURE_HIT = 14,  //法宝攻击
        UNYIELDING = 15,  //不屈
    }

    public enum EarlyWarningType
    { 
        None = 0,
        All = 1,
        Self = 2
    }

    //部位解锁条件
    public enum PartUnlockType
    {
        HP = 1
    }

    public enum SkillDropItemOriginalPointType
    {
        ATTACKER = 0,       //施法者
        RANDOM_TARGET,      //随机判定目标
        RANDOM_TARGET_EDGE  //随机判定目标边缘，向施法者方向偏移hitRadius长度
    }

    public enum BuffRemoveMode
    {
        NONE = 0,
        DELETE_ON_DEATH
    }

    public enum SkillCastZoneType
    {
        Ground = 0,
        Air = 1,
        All = 2,
        Sky = 3,
        Recover = 4,
        Transform = 5, //变身
    }

    //生成实体类型
    public enum SpawnType
    {
        None = 0,
        ThrowObject = 1,
        Monster = 2,
        Trap = 3, 
    }

    public enum TrapEndType
    {
        TimeEnd = 0,
        EnterEnd = 1,
    }

    public enum TriggerTrapType
    {
        None = 0,
        Enemy = 1,
        Friend = 2,
        Caller = 3,
    }
}
