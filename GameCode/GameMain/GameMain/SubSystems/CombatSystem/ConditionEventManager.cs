﻿using MogoEngine.RPC;
using GameMain;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.States;
using MogoEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Events;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public enum ConditionEventType
    {
        OnGround = 1,
    }

    public class ConditionEventManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ConditionEventManager
    {

        private Dictionary<ConditionEventType, bool> eventStateDict = new Dictionary<ConditionEventType, bool>();
        private EntityCreature _owner;

        public ConditionEventManager() { }
        public void SetOwner(EntityCreature ower)
        {
            _owner = ower;
        }

        public void Release() 
        {
            eventStateDict.Clear();
        }

        public void Trigger(ConditionEventType type)
        {
            if (!eventStateDict.ContainsKey(type)) return;
            if (!eventStateDict[type]) return;
            switch (type)
            {
                case ConditionEventType.OnGround:
                    OnGroundEvent();
                    break;
                default:
                    break;
            }
        }

        public void SetState(string[] args, bool state = false)
        {
            ConditionEventType type = (ConditionEventType)int.Parse(args[0]);
            eventStateDict[type] = state;
            if (!state)
            {
                return;
            }
            switch (type)
            {
                case ConditionEventType.OnGround:
                    addBuffId = int.Parse(args[1]);
                    break;
                default:
                    break;
            }
        }

        #region OnGround-落地事件
        private int addBuffId = 0;

        private void OnGroundEvent()
        {
            _owner.AddBuffer(addBuffId, 0);
        }
        #endregion
    }
}
