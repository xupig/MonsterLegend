﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{

    public class BufferEffectData
    {
        public string effectName;
        public string[] effectValue;

        public BufferEffectData(string name, string[] value)
        {
            effectName = name;
            effectValue = value;
        }
    }

    public class BufferTimeEffectData : BufferEffectData
    { 
        public float exeTime;
        public BufferTimeEffectData(float time, string name, string[] value)
            : base(name, value)
        {
            exeTime = time;
        }
    }

}
