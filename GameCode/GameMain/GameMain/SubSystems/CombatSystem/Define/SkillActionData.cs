﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{
    public class SkillActionData
    {
        public int actionID;
        public List<SkillEventData> skillEvents;

        public List<float> dmgRateLvlFactors;
        public List<float> dmgAddLvlFactors;
        public Dictionary<int, int> dmgRateTargetTypeFactors;  
       // public List<float> dmgCritRateLvlFactors;
        //public List<float> dmgStrikeRateLvlFactors;
        //public List<int> dmgTypes;
        public List<float> dmgTypesRate;
        public List<float> dmgTypesAdd;
        public float dmgProficientFactor;
        public int dmgSpecialType;
        public List<float> dmgSpecialArg;
        //public Dictionary<int, int> fireCosts;
        public int dmg_type;
        public int dmg_property;
        public int adjustType;
        public int adjustPriority;  //新增
        public int faceAdjust;//新增
        public HashSet<int> adjustActionSet;
        public List<int> targetType;
        public int originType;
        public List<int> originAdjust;
        public int attackRegionType;
        public List<int> attackRegionArg;
        public int earlyWarningType;
        public int regionJudgeTime;
        public int fireDelayTime;
        public List<int> targetFilterOrders;
        public Dictionary<int, List<int>> targetFilterArgs;
        //public int targetMinCount;
        public int targetMaxCount;
        //public int collisionId;
        public int selfMovType;
        public List<float> selfMovArg;
        public Dictionary<int, List<float>> targetMovPreBuff;
        public int targetMovType;
        public List<float> targetMovArg;
        public Dictionary<int, List<float>> addBuffSelf;
        public Dictionary<int, int> delBuffSelf;
        public Dictionary<int, List<float>> addBuffTarget;
        public Dictionary<int, int> delBuffTarget;
        public Dictionary<int, float> fireAction;
        public Dictionary<int, int> spawnType;//新增
        //public Dictionary<int, int> chgSpellCD;
        //public Dictionary<int, List<float>> fireDropItem;
        public List<int> endureDmgLvlFactors;
        public Dictionary<int, List<float>> targetBehitBuff;
        public int superArmorDestroyLevel;
        public Dictionary<int, List<float>> targetSuperArmorBehitBuff;
        public int noJudgeSpellId;
        public bool needRecord;

        public SkillActionData(int actID)
        {
            if (!GameData.XMLManager.spell_action.ContainsKey(actID)) 
            {
                LoggerHelper.Error("SkillActionData Key Error:" + actID);
                return;
            }
            var data = GameData.XMLManager.spell_action[actID];
            this.actionID = actID;
            skillEvents = CombatDataTools.ParseSkillEvents(data.__effect_events);
            var skillSoundEvents = CombatDataTools.ParseSkillEvents(data.__sound_events);
            skillEvents.AddRange(skillSoundEvents);
            dmgRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_rate_lvl_factors);
            dmgAddLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_add_lvl_factors);
            dmgRateTargetTypeFactors = data_parse_helper.ParseDictionaryIntInt(data.__dmg_rate_target_type);
            //dmgCritRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_crit_rate_lvl_factors);
            //dmgStrikeRateLvlFactors = data_parse_helper.ParseListFloat(data.__dmg_strike_rate_lvl_factors);
            //dmgTypes = data_parse_helper.ParseListInt(data.__dmg_types);
            //dmgTypesRate = data_parse_helper.ParseListFloat(data.__dmg_types_rate);
            //dmgTypesAdd = data_parse_helper.ParseListFloat(data.__dmg_types_add);
            //dmgProficientFactor = data.__dmg_proficient_factor;
            dmg_type = data.__dmg_type;
            dmg_property = data.__dmg_property;
            dmgSpecialType = data.__dmg_special_type;
            dmgSpecialArg = data_parse_helper.ParseListFloat(data.__dmg_special_arg);
            //fireCosts = data_parse_helper.ParseDictionaryIntInt(data.__fire_costs);
            adjustType = data.__adjust_type;
            faceAdjust = data.__face_adjust;
            List<int> adjustActionList = data_parse_helper.ParseListInt(data.__adjust_action);
            adjustActionSet = new HashSet<int>();
            for (int i = 0; i < adjustActionList.Count; ++i)
            {
                adjustActionSet.Add(adjustActionList[i]);
            }
            targetType = data_parse_helper.ParseListInt(data.__target_type2);
            originType = data.__origin_type;
            originAdjust = data_parse_helper.ParseListInt(data.__origin_adjust);
            attackRegionType = data.__attack_region_type;
            attackRegionArg = data_parse_helper.ParseListInt(data.__attack_region_arg);
            earlyWarningType = data.__early_warning_type;

            regionJudgeTime = data.__region_judge_time;
            fireDelayTime = (int)data.__fire_delay_time;
            targetFilterOrders = data_parse_helper.ParseListInt(data.__target_filter_orders);
            targetFilterArgs = data_parse_helper.ParseDictionaryIntListInt(data.__target_filter_args);
            //targetMinCount = data.__target_min_count;
            targetMaxCount = data.__target_max_count;
            //collisionId = data.__collision_id;
            selfMovType = data.__self_mov_type;
            selfMovArg = data_parse_helper.ParseListFloat(data.__self_mov_arg);
            targetMovPreBuff = data_parse_helper.ParseDictionaryIntListFloat(data.__target_mov_pre_buff);
            targetMovType = data.__target_mov_type;
            targetMovArg = data_parse_helper.ParseListFloat(data.__target_mov_arg);

            addBuffSelf = data_parse_helper.ParseDictionaryIntListFloat(data.__add_buff_self);
            delBuffSelf = data_parse_helper.ParseDictionaryIntInt(data.__del_buff_self);
            addBuffTarget = data_parse_helper.ParseDictionaryIntListFloat(data.__add_buff_target);
            delBuffTarget = data_parse_helper.ParseDictionaryIntInt(data.__del_buff_target);
            fireAction = data_parse_helper.ParseDictionaryIntFloat(data.__fire_action);
            spawnType = data_parse_helper.ParseDictionaryIntInt(data.__spawn_type);
            //chgSpellCD = data_parse_helper.ParseDictionaryIntInt(data.__chg_spell_cd);
            //fireDropItem = data_parse_helper.ParseDictionaryIntListFloat(data.__fire_dropitem);
            endureDmgLvlFactors = data_parse_helper.ParseListInt(data.__endure_dmg_lvl_factors);
            targetBehitBuff = data_parse_helper.ParseDictionaryIntListFloat(string.Concat(data.__target_behit_buff));
            superArmorDestroyLevel = data.__super_armor_destroy_level;
            targetSuperArmorBehitBuff = data_parse_helper.ParseDictionaryIntListFloat(data.__target_super_armor_behit_buff);
            noJudgeSpellId = data.__no_judge_spell_id;
            needRecord = data.__need_record == 1;
        }
    }


}
