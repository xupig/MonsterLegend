﻿using GameData;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    /// <summary>
    /// 反射信息
    /// </summary>
    public class RayRefectionInfo
    {
        public Ray castRay;
        public RaycastHit raycastHit;
        /// <summary>
        /// 射线总长度
        /// </summary>
        public float allDistance;

        /// <summary>
        /// 原来目标位置
        /// </summary>
        public Vector3 oldTargetPosition;

        /// <summary>
        /// 反弹系数
        /// </summary>
        private float _reboundRatio = 0.5f;

        /// <summary>
        /// 临界距离,用来判断目标模型改变
        /// </summary>
        private float _criticalDistance = 0.1f;

        /// <summary>
        /// 反弹距离
        /// </summary>
        private float _boundDistance;

        private Vector3 _avatarRotation;
        public Vector3 avatarRotation
        {
            get
            {
                return _avatarRotation;
            }
        }

        public RayRefectionInfo()
        {
            castRay = new Ray(Vector3.zero, Vector3.zero);
            raycastHit = new RaycastHit();
        }

        public void SetInfo(Ray castRay, RaycastHit raycastHit, Vector3 oldTargetPosition)
        {
            this.castRay = castRay;
            this.raycastHit = raycastHit;
            this.oldTargetPosition = oldTargetPosition;

            _reboundRatio = (float)(int.Parse(global_params_helper.GetGlobalParam(89))) / 100;
            _criticalDistance = (float)(int.Parse(global_params_helper.GetGlobalParam(90))) / 100;
            //LoggerHelper.Error("[RayRefectionInfo:SetInfo]=>_reboundRatio:  " + _reboundRatio + ",_criticalDistance:    " + _criticalDistance);
        }

        private Vector3 GetRayDirection()
        {
            return castRay.direction.normalized;
        }

        private Vector3 GetReflectionDirection()
        {
            Vector3 dir1 = GetRayDirection();
            return Common.Utils.GameRayUtils.CalcReflectionDirection(dir1, raycastHit);

        }

        public Vector3 CalcReflectPosition()
        {
            Vector3 p3 = Vector3.zero;
            Vector3 p4 = Vector3.zero;
            //法线长度
            float normalHalfLenght = 0;
            float refectionDistance = allDistance - raycastHit.distance;
            p3 = raycastHit.point + GetReflectionDirection() * refectionDistance;

            //计算半法线长度
            normalHalfLenght = Vector3.Distance(p3, oldTargetPosition) / 2;
            p4 = p3 + (-raycastHit.normal) * normalHalfLenght * (1 - (_reboundRatio * _reboundRatio));

            //计算反弹距离
            _boundDistance = normalHalfLenght - Vector3.Distance(p4, p3);
            _avatarRotation = (raycastHit.point - p4).normalized;
            //LoggerHelper.Error("[AccordingMovement:CalcRefectPosition]=>m_avatarRotation:    " + _avatarRotation + ",raycastHit.point: " + raycastHit.point + ",p4:    " + p4);
            return p4;
        }

        public bool IsChangeAvatarModel()
        {
            return _boundDistance > _criticalDistance ? true : false;
        }

    }
}
