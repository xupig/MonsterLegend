﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using MogoEngine;
using MogoEngine.RPC;
using Common.ServerConfig;
using Common.Utils;
using GameMain.ClientConfig;
using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    public class TargetFilter
    {
        static float CalculateAngle(EntityCreature attacker, EntityCreature defender)
        {
            if (attacker.GetTransform() != null && defender.GetTransform() != null)
            {
                Vector3 dir = attacker.GetTransform().position - defender.GetTransform().position;
                Vector3 forward = defender.GetTransform().forward;
                return Vector3.Angle(forward, dir);
            }
            return 0;
        }

        static public void GetCanSearchCreaturesByTargetType(List<uint> result, EntityCreature self, List<uint> entities, List<int> targetTypes, uint assignId = 0)
        {
            result.Clear();
            var allEntities = MogoWorld.Entities;
            for (int i = 0; i < entities.Count; i++)
            {
                uint entID = entities[i];
                if (!allEntities.ContainsKey(entID)) continue;
                var creature = allEntities[entID] as EntityCreature;
                if (creature == null) continue;
                if (creature.CanSearch() == false) continue;
                if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP || creature.entityType == EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT) 
                    continue;
                if (!creature.stateManager.CanDO(CharacterAction.HIT_ABLE)) continue;
                if (IsTarget(self, creature, targetTypes, assignId))
                {
                    result.Add(entID);
                }
            }
        }

        
        static public void GetCreaturesByTargetType(List<uint> result, EntityCreature self, List<uint> entities, List<int> targetTypes, uint assignId = 0)
        {
            result.Clear();
            var allEntities = MogoWorld.Entities;
            for (int i = 0; i < entities.Count; i++)
            {
                uint entID = entities[i];
                if (!allEntities.ContainsKey(entID)) continue;
                var creature = allEntities[entID] as EntityCreature;
                if (creature == null) continue;
                if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP || creature.entityType == EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT) 
                    continue;
                if (!creature.stateManager.CanDO(CharacterAction.HIT_ABLE)) continue;
                if (IsTarget(self, creature, targetTypes, assignId))
                {
                    result.Add(entID);
                }
            }
        }

        static private bool IsTarget(EntityCreature self, EntityCreature target, List<int> targetTypes, uint assignId = 0)
        {
            if (targetTypes.Contains((int)TargetType.None)) return false;
            if (targetTypes.Contains((int)TargetType.MySelf) && self.id == target.id) return true;
            if (targetTypes.Contains((int)TargetType.Enemy) && IsEnemy(self, target)) return true;
            if (targetTypes.Contains((int)TargetType.Friend) && IsFriend(self, target)) return true;
            if (targetTypes.Contains((int)TargetType.Assign) && IsAssign(target, assignId)) return true;
            if (targetTypes.Contains((int)TargetType.Caller) && IsCaller(self, target)) return true;
            return false;
        }

        static private Dictionary<uint, float> _entitiesWeightMap = new Dictionary<uint, float>();
        static public void FilterTarget(List<uint> result, EntityCreature self, List<uint> entities, List<int> targetFilterOrders, Dictionary<int, List<int>> targetFilterArgs, int targetMinCount, int targetMaxCount)
        {
            result.Clear();
            result.AddRange(entities);
            var allEntities = MogoWorld.Entities;
            for (int i = result.Count - 1; i >= 0; --i)
            {
                if (!allEntities.ContainsKey(result[i]))
                {
                    result.RemoveAt(i);
                }
            }
			if (targetFilterOrders.Count > 0)
			{
	            _entitiesWeightMap.Clear();
				int targetFilterOrder = 0;
				List<int> targetFilterArg = null;
	            for (int i = 0; i < targetFilterOrders.Count; ++i)
				{
					targetFilterOrder = targetFilterOrders[i];
					targetFilterArg = null;
					if (targetFilterArgs.ContainsKey(targetFilterOrder))
					{
						targetFilterArg = targetFilterArgs[targetFilterOrder];
					}
					UpdateWeightMap(self, result, targetFilterOrder, targetFilterArg, i);
	            }
	            result.Sort((a, b) =>
	            {
	                return (int)(_entitiesWeightMap[a] - _entitiesWeightMap[b]);
	            });
			}
            NotifyOrderBySkillLockID(self, result);
            while (result.Count > targetMaxCount)
            {
                result.RemoveAt(result.Count - 1);
            }
        }

        static public void FilterTargetWithShieldAgainst(List<uint> result, EntityCreature self)
        {
            result.Clear();
            var allEntities = MogoWorld.Entities;
            for (int i = result.Count - 1; i >= 0; --i)
            {
                if (!allEntities.ContainsKey(result[i]))
                {
                    result.RemoveAt(i);
                    continue;
                }
                var creature = allEntities[result[i]] as EntityCreature;
                ShieldAgainstData shieldAgainstData = creature.skillManager.shieldAgainstData;
                if (shieldAgainstData.isShieldAgainst && CalculateAngle(self, creature) < shieldAgainstData.angle)
                {
                    result.RemoveAt(i);
                    shieldAgainstData.curHitCount++;
                    creature.AddBuffer(shieldAgainstData.buffIdList[0]);
                    creature.AddBuffer(shieldAgainstData.buffIdList[1]);
                    if (shieldAgainstData.curHitCount >= shieldAgainstData.maxHitCount)
                    {
                        creature.AddBuffer(shieldAgainstData.buffIdList[2]);
                    }
                }
            }
        }

        private static void UpdateWeightMap(EntityCreature self, List<uint> result, int targetFilterType, List<int> targetFilterArgs, int order)
        {
            switch ((TargetFilterType)targetFilterType)
            {
                case TargetFilterType.MinAngle:
                    UpdateWeightMapByMinAngle(self, result, order);
                    break;
                case TargetFilterType.Closest:
                    UpdateWeightMapByClosest(self, result, order);
                    break;
                case TargetFilterType.HaveBuffer:
                    UpdateWeightMapByBuff(self, result, targetFilterArgs[0], order);
                    break;
                case TargetFilterType.RangeAndAngle:
                    UpdateWeightMapByRangeAndAngle(self, result, targetFilterArgs[0], targetFilterArgs[1], order);
                    break;
                case TargetFilterType.Random:
                    UpdateWeightMapByRandom(self, result, order);
                    break;
                case TargetFilterType.Farthest:
                    UpdateWeightMapByFarthest(self, result, order);
                    break;
                case TargetFilterType.EntityType:
                    UpdateWeightMapByEntityType(self, result, targetFilterArgs, order);
                    break;
            }
        }

        private static void UpdateWeightMapByMinAngle(EntityCreature self, List<uint> result, int order)
        {
            var selfTransform = self.GetTransform();
            if (selfTransform == null) return;
            float weight = 0;
            for (int i = 0; i < result.Count; i++)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                var transform = creature.GetTransform();
                if (transform == null) continue;
                var dir = transform.position - selfTransform.position;
                weight = (int)(Vector3.Angle(selfTransform.forward, dir) * 100) / 100.0f;
                weight *= Mathf.Pow(10000, order);
                _entitiesWeightMap[entID] += weight;
            }
        }

        private static void UpdateWeightMapByClosest(EntityCreature self, List<uint> result, int order)
        {
            float weight = 0;
            for (int i = 0; i < result.Count; i++)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                var newDis = (int)((creature.position - self.position).magnitude * 100) / 100.0f;
                weight = newDis >= 100 ? 100 : newDis;
                weight *= Mathf.Pow(10000, order);
                _entitiesWeightMap[entID] += weight;
            }
        }

        private static void UpdateWeightMapByBuff(EntityCreature self, List<uint> result, int buffId, int order)
        {
            float weight = 0;
            for (int i = result.Count - 1; i >= 0; i--)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                if (!creature.bufferManager.ContainsBuffer(buffId))
                {
                    result.RemoveAt(i);
                }
                else
                {
                    weight = Mathf.Pow(10000, order);
                    _entitiesWeightMap[entID] += weight;
                }
            }
        }

        private static void UpdateWeightMapByRangeAndAngle(EntityCreature self, List<uint> result, int range, int angle, int order)
        {
            float weight = 0;
            var selfTransform = self.GetTransform();
            if (selfTransform == null) return;
            for (int i = 0; i < result.Count; i++)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                var transform = creature.GetTransform();
                if (transform == null) continue;
                var dir = transform.position - selfTransform.position;
                float entAngle = Vector3.Angle(selfTransform.forward, dir);
                float entDis = (int)((creature.position - self.position).magnitude * 100) / 100.0f;
                float entResult = entDis * 100 * entAngle;
                weight = entDis >= 100 ? 100 : entDis;
                if (entResult <= range && entAngle <= angle)
                {
                }
                else
                {
                    weight *= 100;
                }
                weight *= Mathf.Pow(10000, order);
                _entitiesWeightMap[entID] += weight;
            }
        }

        private static void UpdateWeightMapByRandom(EntityCreature self, List<uint> result, int order)
        {
            float weight = 0;
            int[] randomList = RandomUtils.RandomSequence(0, result.Count - 1);
            for (int i = 0; i < result.Count; i++)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                weight = randomList[i];
                weight *= Mathf.Pow(10000, order);
                _entitiesWeightMap[entID] += weight;
            }
        }

        private static void UpdateWeightMapByFarthest(EntityCreature self, List<uint> result, int order)
        {
            float weight = 0;
            for (int i = 0; i < result.Count; i++)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                var newDis = 100 - (int)((creature.position - self.position).magnitude * 100) / 100.0f;
                weight = newDis < 0 ? 0 : newDis;
                weight *= Mathf.Pow(10000, order);
                _entitiesWeightMap[entID] += weight;
            }
        }

        private static void UpdateWeightMapByEntityType(EntityCreature self, List<uint> result, List<int> entityTypeList, int order)
        {
            float weight = 0;
            for (int i = result.Count - 1; i >= 0; i--)
            {
                uint entID = result[i];
                if (!_entitiesWeightMap.ContainsKey(entID))
                {
                    _entitiesWeightMap.Add(entID, 0);
                }
                var creature = MogoWorld.GetEntity(entID) as EntityCreature;
                bool checkEntityTypeResult = false;
                for (int j = 0; j < entityTypeList.Count; ++j)
                {
                    if (CheckEntityType(creature, entityTypeList[j]))
                    {
                        checkEntityTypeResult = true;
                        break;
                    }
                }
                if (!checkEntityTypeResult)
                {
                    result.RemoveAt(i);
                }
                else
                {
                    weight = Mathf.Pow(10000, order);
                    _entitiesWeightMap[entID] += weight;
                }
            }
        }

        private static bool CheckEntityType(EntityCreature creature, int entityType)
        {
            return true;
        }

        static public bool IsEnemy(EntityCreature creature1, EntityCreature creature2)
        {
            if (creature1.id == creature2.id)
            {
                return false;
            }
            if (creature1 == null || creature2 == null)
            {
                return false;
            }
            if (creature1.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER && creature2.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
            {
                return false;
            }
            if (creature1.realCamp == creature2.realCamp)
            {
                return false;
            }
            /*
            if (creature1 is EntityMonsterBase || creature2 is EntityMonsterBase)
            {
                return creature1.GetCampPveType() != creature2.GetCampPveType();
            }
            else if (creature1 is EntityAvatar && creature2 is EntityAvatar)
            {
                return creature1.GetCampPvpType() != creature2.GetCampPvpType();
            }*/
            return true;
        }

        static public bool IsFriend(EntityCreature creature1, EntityCreature creature2)
        {
            if (creature1.id == creature2.id)
            {
                return false;
            }
            return !IsEnemy(creature1, creature2);
        }

        public static bool IsAssign(EntityCreature target, uint assignId = 0)
        {
            if (assignId == target.id)
            {
                return true;
            }
            return false;
        }

        public static bool IsCaller(EntityCreature creature1, EntityCreature creature2)
        {
            if (creature1.castEntityId == creature2.id)
            {
                return true;
            }
            return false;
        }

        private static void NotifyOrderBySkillLockID(EntityCreature self, List<uint> entities)
        {
            /*
            if (self.skillLockId == 0 || entities.Count <= 0)
            {
                return;
            }
            uint skillLockId = self.skillLockId;
            if (entities.Contains(skillLockId))
            {
                entities.Remove(skillLockId);
                entities.Insert(0, skillLockId);
            }*/
        }

    }
}
