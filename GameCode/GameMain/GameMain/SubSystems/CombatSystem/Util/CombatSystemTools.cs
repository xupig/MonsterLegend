﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using MogoEngine;
using MogoEngine.RPC;
using Common.ServerConfig;
using GameData;


namespace GameMain.CombatSystem
{
    public static class CombatSystemTools
    {
        public static EntityCreature GetCreatureByID(uint entityID)
        {
            var entity = MogoWorld.GetEntity(entityID);
            if (entity == null) return null;
            return entity as EntityCreature;
        }

        public static AttackType DamageMode2AttackType(UInt32 mode)
        {
            if (mode == 0)
            {
                return AttackType.ATTACK_MISS;
            }
            if ((mode & 1 << public_config.HIT_RET_CRI) > 0)
            {
                return AttackType.ATTACK_CRITICAL;
            }
            if ((mode & 1 << public_config.HIT_RET_BREAK) > 0)
            {
                return AttackType.ATTACK_STRIKE;
            }
            if ((mode & 1 << public_config.HIT_RET_HIT) > 0)
            {
                return AttackType.ATTACK_HIT;
            }
            if ((mode & 1 << public_config.HIT_RET_TREAT) > 0)
            {
                return AttackType.TREAT;
            }
            if ((mode & 1 << public_config.HIT_RET_PARRY) > 0)
            {
                return AttackType.PARRY;
            }
            if ((mode & 1 << public_config.HIT_RET_WUDI) > 0)
            {
                return AttackType.INVINCIBLE;
            }
            if ((mode & 1 << public_config.HIT_RET_BUQU) > 0)
            {
                return AttackType.UNYIELDING;
            }
            return AttackType.ATTACK_MISS;
        }

        public static float CalculateDistanceBetween(EntityCreature creature1, EntityCreature creature2)
        {
            var pos1 = creature1.position;
            pos1.y = 0;
            var pos2 = creature2.position;
            pos2.y = 0;
            return ACTSystem.ACTMathUtils.Abs((pos1 - pos2).magnitude);
        }

        //是否为显示伤害飘字的友方怪
        public static bool IsShowDamageMonster(EntityCreature attacker)
        {
            if (attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_MONSTER && attacker.faction_id == EntityPlayer.Player.faction_id
                && monster_helper.GetShowDamage(attacker.monsterId))
            {
                return true;
            }
            return false;
        }

        private static void CheckShowBloodBoard(EntityCreature attacker, EntityCreature target)
        {
            if (attacker.id == EntityPlayer.Player.id || IsShowDamageMonster(attacker))
            {
                int lifeTime = EntityPlayer.Player.billboardManager.bloodMissTime;
                if (EntityPlayer.Player.showEntityDict.ContainsKey(target.id))
                {
                    if (EntityPlayer.Player.showEntityDict[target.id] > 0)
                    {
                        EntityPlayer.Player.showEntityDict[target.id] = RealTime.time + lifeTime;
                    }
                }
                else
                {
                    EntityPlayer.Player.showEntityDict.Add(target.id, RealTime.time + lifeTime);
                    EntityPlayer.Player.showEntityList.Add(target.id);
                }
            }
        }

        public static bool CanShowDamage(EntityCreature attacker, EntityCreature target)
        {
            CheckShowBloodBoard(attacker, target);
            uint playerID = EntityPlayer.Player.id;
            if (target.id == playerID || attacker.id == playerID)
            {
                return true;
            }
            if ((attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT || attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_TRAP) 
                && attacker.castEntityId == playerID)
            {
                return true;
            }
            if (attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_PET && attacker.owner_id == playerID)
            {
                return true;
            }
            if (attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_FABAO && attacker.owner_id == playerID)
            {
                return true;
            }
            if (attacker.entityType == GameMain.ClientConfig.EntityConfig.ENTITY_TYPE_NAME_TRAP && attacker.owner_id == playerID)
            {
                return true;
            }
            if (IsShowDamageMonster(attacker))
            {
                return true;
            }

            return false;           
        }

        static int _terrainLayerValue = 1 << ACTSystem.ACTSystemTools.NameToLayer("Terrain");
        static RaycastHit _raycastHit = new RaycastHit();
        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        public static Vector3 GetGroundPosition(Vector3 position)
        {
            Vector3 result = position;
            _downRay.origin = position;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_downRay, out _raycastHit, 2000f, _terrainLayerValue);
            return result;
        }
    }
}
