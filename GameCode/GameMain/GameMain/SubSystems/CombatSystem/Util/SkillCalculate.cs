﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

using MogoEngine.RPC;
using GameMain;

using Common.ServerConfig;
using Common.States;
using GameData;
using GameMain.ClientConfig;
using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    public struct SkillDamage
    {
        public int value;
        public UInt32 mode;
    }

    public struct SkillTypeDamage
    {
        public float curDmg;
        public byte damageFlag;
        public SkillTypeDamage(float curDmg, byte damageFlag)
        {
            this.curDmg = curDmg;
            this.damageFlag = damageFlag;
        }
    }

    public class SkillCalculate
    {
        //将x的取值范围纠正到[lower,upper]之间
        static float CheckAndFixRange(float x, float lower_value, float upper_value)
        {
            if (x < lower_value)
            {
                return lower_value;
            }
            if (x > upper_value)
            {
                return upper_value;
            }
            return x;
        }

        //属性攻击
        static bool GetPropertyParam(EntityCreature caster, EntityCreature target, int property, ref int p1, ref int p2, ref int p3, ref int p4)
        {
            switch(property)
            {
                case 11:
                    //挥砍
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_SLASHING_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_SLASHING_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY_DEFENSE);
                    return true;
                case 12:
                    //穿刺
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PIERCING_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_PIERCING_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY_DEFENSE);
                    return true;
                case 13:
                    //打击
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_BLOW_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_BLOW_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_PHY_PROPERTY_DEFENSE);
                    return true;
                case 21:
                    //火
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_FIRE_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_FIRE_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY_DEFENSE);
                    return true;
                case 22:
                    //水
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ICE_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ICE_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY_DEFENSE);
                    return true;
                case 23:
                    //光
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_LIGHT_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_LIGHT_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY_DEFENSE);
                    return true;
                case 24:
                    //暗
                    p1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_DARK_PROPERTY);
                    p2 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_DARK_PROPERTY_DEFENSE);
                    p3 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY);
                    p4 = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ALL_MAG_PROPERTY_DEFENSE);
                    return true;
            }
            return false;
        }

        //暂时占位处理
        static bool BreakSpells(EntityCreature target, int spellId) { return false; }

        //天平处理
        static void GetLevelParamPair(int casterLevel, int tgtLevel, out coefficient_level a, out coefficient_level b)
        {
            //非天平场景和天平场景目前只有一个区别,就是几个公式里被除数不一样
            //天平场景是攻击者自己的属性除以自己的等级
            //非天平场景是攻击者的属性除以目标的等级,这样会有等级压制的效果
            var casterLevelParams = coefficient_level_helper.GetData(casterLevel);
            var targetLevelParams = coefficient_level_helper.GetData(tgtLevel);
            if (EntityPlayer.isLibraMode)
            {
                a = targetLevelParams;
                b = casterLevelParams;
            }
            else
            {
                b = targetLevelParams;
                a = casterLevelParams;
            }
        }

        public static SkillDamage CalculateDamage(EntityCreature caster, EntityCreature target, int skillID, SkillAction skillAction, int targetCnt)
        {            
            var result = new SkillDamage();
            SkillData skillData = CombatLogicObjectPool.GetSkillData(skillID);

            //命中
            var hitRate = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_HIT_RATE) - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MISS_RATE);
            //Debug.LogError("caster ATTRI_ID_HIT_RATE:" + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_HIT_RATE) + " ATTRI_ID_MISS_RATE:" + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MISS_RATE) + " hitRate:" + hitRate);
            if ((hitRate) <= 0)
            {
                //不用随机了,miss了
                return result;
            }
            const float HIT_DIV = 10000.0f;
            if (RandomUtils.GetRandomFloat() > ((float)hitRate) / HIT_DIV)
            {
                //miss了
                //log_i("calculate_damage.miss", "eid=%d;hit-rate=%d", caster.getId(), hit_rate);
                return result;
            }

            //命中了
            result.mode |= (UInt32)1 << public_config.HIT_RET_HIT;

            //破招
            float fixCoBreak = 1.0f;     //破招伤害修正
            if (BreakSpells(target, 0))
            {
                fixCoBreak += caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_BROKEN_ATK_RATE) / 100.0f;
                CheckAndFixRange(fixCoBreak, 1.0f, 4.0f);
                result.mode |= (1 << public_config.HIT_RET_BREAK);
            }

            //天平处理
            coefficient_level levelParamsA;
            coefficient_level levelParamsB;
            int casterLevel = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_LEVEL);
            int tgtLevel = (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_LEVEL);
            GetLevelParamPair(casterLevel, tgtLevel, out levelParamsA, out levelParamsB);

            //暴击
            float fixCoCri = 1f; //暴击伤害修正
            {
                float c1 = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_CRIT) / levelParamsA.__levelpram2
                        - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ANTI_CRIT) / levelParamsB.__levelpram2;
                float c2 = ACTSystem.ACTMathUtils.Abs(c1) + 1.0f;
                float c3 = c1 / c2 + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_CRIT_RATE) / 10000.0f
                        - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_ANTI_CRIT_RATE) / 10000.0f;
                if (RandomUtils.GetRandomFloat() <= c3)
                {
                    //暴击了
                    result.mode |= (UInt32)1 << public_config.HIT_RET_CRI;
                    fixCoCri = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_CRIT_ATK_RATE) / 100.0f
                            + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_CRIT_EXTRA_ATK_RATE) / 10000.0f
                            - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_ANTI_CRIT_EXTRA_ATK_RATE) / 10000.0f;
                    fixCoCri = CheckAndFixRange(fixCoCri, 1.0f, 2.5f);
                }
            }

            //防御减伤率
            float defDmgDecRate = 0;
            float casterAtt = 0;
            int dmgType = skillAction.skillActionData.dmg_type;
            if (dmgType == 1)
            {
                //物理攻击=【（物攻基础值-盾牌防御）×（1+物攻加成比/10000）+物攻附加值】×（1+额外物攻加成比/10000）+额外物攻附加值
                casterAtt = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_ATK_BASE) - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_SHIELD_DEFENSE) *
                    (1 + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_ATK_RATE) / 10000.0f) + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_ATK_ADDED) *
                    (1 + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_ATK_RATE) / 10000.0f) + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_ATK_ADDED);
                //物理防御=【物防基础值×（1+物防加成比/10000）+物防附加值】×（1+额外物防加成比/10000）+额外物防附加值
                float tgtDef = (target.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_DEF_BASE) * (1 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_DEF_RATE) / 10000.0f)
                    + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_DEF_ADDED)) * (1 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_DEF_RATE) / 10000.0f)
                    + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_DEF_ADDED);

                float c1 = tgtDef / levelParamsA.__levelpram3 - caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PHY_DEF_PENETRATION) / levelParamsB.__levelpram3;
                float c2 = ACTSystem.ACTMathUtils.Abs(c1) + 1.0f;
                defDmgDecRate = c1 / c2 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_DEF_REDUCE_RATE) / 10000.0f
                    - caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PHY_DEF_PENETRATION_RATE) / 10000.0f;
            }
            else 
            {
                //魔法攻击=【（魔攻基础值-魔法抵抗）×（1+魔攻加成比/10000）+魔攻附加值】×（1+额外魔攻加成比/10000）+额外魔攻附加值
                //(物攻基础值-盾牌防御）和(魔攻基础值-魔法抵抗）不小于物攻基础值或魔攻基础值的10%
                casterAtt = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_ATK_BASE) - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_RESISTANCE) *
                    (1 + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_ATK_BASE) / 10000.0f) + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_ATK_ADDED) *
                    (caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_ATK_RATE) / 10000.0f) + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_ATK_ADDED);
                //魔法防御=【魔防基础值×（1+魔防加成比/10000）+魔防附加值】×（1+额外魔防加成比/10000）+额外魔防附加值
                float tgtDef = (target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_DEF_BASE) * (1 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_DEF_RATE) / 10000.0f)
                    + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_DEF_ADDED)) * (1 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_DEF_RATE) / 10000.0f)
                    + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_DEF_ADDED);

                float c1 = tgtDef / levelParamsA.__levelpram3 - caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_MAG_DEF_PENETRATION) / levelParamsB.__levelpram3;
                float c2 = ACTSystem.ACTMathUtils.Abs(c1) + 1.0f;
                defDmgDecRate = c1 / c2 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_DEF_REDUCE_RATE) / 10000.0f
                    - caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_MAG_DEF_PENETRATION_RATE) / 10000.0f;
            }
            defDmgDecRate = CheckAndFixRange(defDmgDecRate, 0.0f, 1.0f);

            //PVP修正
            float fixCoPvp = 1.0f;
            if (IsAvatar(caster) && IsAvatar(target))
            {
                float c1 = caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_PVP_ADDITION) / levelParamsA.__levelpram4
                    - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_PVP_ANTI) / levelParamsB.__levelpram4;
                float c2 = ACTSystem.ACTMathUtils.Abs(c1) + 1;
                fixCoPvp = 1 + (c1 / c2) + caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PVP_ADDITION_RATE) / 10000.0f
                        - target.GetBattleAttribute(battle_attri_config.ATTRI_ID_EXTRA_PVP_ANTI_RATE) / 10000.0f;
                fixCoPvp = CheckAndFixRange(fixCoPvp, 0.5f, 2.0f);
            }

            //攻击属性修正
            float fixCoProperty = 1.0f;
            int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
            int dmgProperty = skillAction.skillActionData.dmg_property;
            if (GetPropertyParam(caster, target, dmgProperty, ref p1, ref p2, ref p3, ref p4))
            {
                fixCoProperty = 1.0f + ((float)(p1 - p2 + p3 - p4)) / 200.0f;
                CheckAndFixRange(fixCoProperty, 0.1f, 3.0f);
            }

            //攻击浮动修正
            float fixCoFloat;
            {
                int c1 = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_FLOAT_ATK);
                int r = RandomUtils.GetRandomInt(10000 - c1, 10000 + c1);
                fixCoFloat = r / 10000.0f;
                fixCoFloat = CheckAndFixRange(fixCoFloat, 0.0f, 2.0f);
            }

            //等级压制修正,只适用于玩家打怪物
            float fixCoLevel = 1.0f;
            if (IsMonster(target) && IsAvatar(caster))
            {
                int levelDelta = (int)caster.GetBattleAttribute(battle_attri_config.ATTRI_ID_LEVEL) - (int)target.GetBattleAttribute(battle_attri_config.ATTRI_ID_LEVEL);
                if (levelDelta <= -20)
                {
                    fixCoLevel = 0.01f;
                }
                else if (-19 <= levelDelta && levelDelta <= -6)
                {
                    fixCoLevel = 1.0f - ACTSystem.ACTMathUtils.Abs(levelDelta) * 0.05f;
                }
            }

            //目标类型修正,某些技能有对不同类型怪物（人类、机械、。。。）的伤害修正
            float fixCoTgtType = 1.0f;
		    //todo

            ////生命最大值=【生命基础值×（1+生命加成比/10000）+生命附加值】×（1+额外生命加成比/10000）+额外生命附加值

            //技能加成
            float spAddRate = 1.0f;
            float spAdd = 0.0f;
            {
                int spellLevel = skillData.level;	//查表获取技能等级
                List<float> spDmgRateData = skillAction.GetDmgRateLvlFactors();
                if ((1 <= spellLevel) && (spellLevel <= spDmgRateData.Count))
                {
                    spAddRate = ((float)spDmgRateData[spellLevel - 1]) * 0.0001f;
                }
                List<float> spDmgAddData = skillAction.GetDmgAddLvlFactors();
                if ((1 <= spellLevel) && (spellLevel <= spDmgAddData.Count))
                {
                    spAdd = (float)spDmgAddData[spellLevel - 1];
                }
            }

            //最终伤害=（物理攻击×技能比例加成/10000+技能固定加成）×破招伤害修正×暴击伤害修正×(1-防御减伤率)/(1+〖伤害减免率〗_被攻击方/10000)
            //              ×PVP修正×元素属性修正×攻击浮动修正×等级压制修正×目标类型修正
            float lastDmg = (casterAtt * spAddRate + spAdd) * fixCoBreak * fixCoCri * (1 - defDmgDecRate) / (1 + target.GetBattleAttribute(battle_attri_config.ATTRI_ID_DAMAGE_REDUCE) / 10000.0f)
                * fixCoPvp * fixCoProperty * fixCoFloat * fixCoLevel * fixCoTgtType;
            result.value = (int)Math.Ceiling(lastDmg);
            return result;
        }

        static bool IsAvatar(EntityCreature creature)
        {
            return creature.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || creature.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR;
        }

        static bool IsMonster(EntityCreature creature)
        {
            return creature.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY;
        }

        //-----------------------
        //统计物，魔，火，冰，电，毒类型，暴击，破击伤害
        //@param attacker: 攻击者
        //@param defender: 受击者
        //-----------------------
        public static void StatisticsDamage(EntityCreature attacker, EntityCreature defender, int damageType, byte damageFlag, float totalDamage)
        {
            SetDamage(attacker, public_config.MISSION_ACTION_OUTPUT_DAMAGE, totalDamage);
            SetDamage(defender, public_config.MISSION_ACTION_GET_DAMAGE, totalDamage);
            //--统计六种技能攻击类型伤害
            int index = 1;
            if (damageType >= public_config.SPELL_DAMAGE_PHY && damageType <= public_config.SPELL_DAMAGE_POISON)
            {
                //--六种元素伤害
                index =  public_config.SPELL_DAMAGE_OUTPUT_OFFSET + damageType;
                SetDamage(attacker, index, totalDamage);
                index = public_config.SPELL_DAMAGE_GET_OFFSET + damageType;
                SetDamage(defender, index, totalDamage);
                //--暴击，破击伤害统计
                if (damageFlag == 1)
                {
                    index = public_config.MISSION_ACTION_OUTPUT_CRIT_DAMAGE;
                    SetDamage(attacker, index, totalDamage);
                    index = public_config.MISSION_ACTION_GET_CRIT_DAMAGE;
                    SetDamage(defender, index, totalDamage);
                }
                else if (damageFlag == 2)
                {
                    index = public_config.MISSION_ACTION_OUTPUT_STRIKE_DAMAGE;
                    SetDamage(attacker, index, totalDamage);
                    index = public_config.MISSION_ACTION_GET_STRIKE_DAMAGE;
                    SetDamage(defender, index, totalDamage);
                }
            }
        }

        //设置统计的伤害值
        public static void SetDamage(EntityCreature player, int damage_type, float totalDamage)
        {
            //player.OnAddStatisticsDamage(damage_type, totalDamage);
        }

        //----------------------------
        //--获取技能行为对所有类型的伤害加成系数
        //----------------------------
        public static List<float> CalcDmgTypesRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> typesDmgRateCfg = skillAction.GetDmgTypesRate();
            List<float> dmgRateBases = skillAction.GetDmgRateLvlFactors();
            float dmgRateBase = 0;
            if (dmgRateBases.Count > 0)
            {
                dmgRateBase = dmgRateBases.Count >= skillLevel ? dmgRateBases[skillLevel - 1] : dmgRateBases[dmgRateBases.Count - 1];
            }
            dmgRateBase = dmgRateBase * 0.0001f;
            List<float> typesDmgRate = new List<float>();
            for (int i = 0; i < typesDmgRateCfg.Count; i++)
            {
                typesDmgRate.Add(typesDmgRateCfg[i] * dmgRateBase);
            }
            return typesDmgRate;
        }

        //----------------------------
        //--获取技能行为对所有类型的伤害追加系数
        //----------------------------
        public static List<float> CalcDmgTypesAdd(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> typesDmgAddCfg = skillAction.GetDmgTypesAdd();
            List<float> dmgAddBases = skillAction.GetDmgAddLvlFactors();
            float dmgAddBase = 0 ;
            if (dmgAddBases.Count > 0)
            {
                dmgAddBase = dmgAddBases.Count >= skillLevel ? dmgAddBases[skillLevel - 1] : dmgAddBases[dmgAddBases.Count - 1];
            }
            List<float> typesDmgAdd = new List<float>();
            for (int i = 0; i < typesDmgAddCfg.Count; i++)
            {
                typesDmgAdd.Add(typesDmgAddCfg[i] * dmgAddBase);
            }
            return typesDmgAdd;
        }

        //----------------------------
        //--获取技能行为的暴击追加系数
        //----------------------------
        public static float CalcDmgCritRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> dmgCritRateBases = skillAction.GetDmgCritRateLvlFactors();
            float critRate = 0;
            if (dmgCritRateBases.Count > 0)
            {
                critRate = dmgCritRateBases.Count >= skillLevel ? dmgCritRateBases[skillLevel - 1] : dmgCritRateBases[dmgCritRateBases.Count - 1];
            }
            return critRate;
        }

        //----------------------------
        //--获取技能行为的破击追加系数
        //----------------------------
        public static float CalcDmgStrikeRate(EntityCreature entity, SkillData skillData, SkillAction skillAction)
        {
            int skillLevel = skillData.level;
            List<float> dmgStrikeRateBases = skillAction.GetDmgStrikeRateLvlFactors();
            float strikeRate = 0;
            if (dmgStrikeRateBases.Count > 0)
            {
                strikeRate = dmgStrikeRateBases.Count >= skillLevel ? dmgStrikeRateBases[skillLevel - 1] : dmgStrikeRateBases[dmgStrikeRateBases.Count - 1];
            }
            return strikeRate;
        }

        /*
        -----------------------
        --计算各种类型伤害
        --@param attacker: 攻击者
        --@param defender: 受击者
        --@param spell_per: 伤害加成比例
        --@param spell_add: 伤害增加数值
        --@param damage_type: 伤害类型
        --@param damage_base: 伤害基础值
        --@param crit_factor: 暴击加成系数
        --@param stirke_factor: 破击加成系数
        -----------------------*/
        public static SkillTypeDamage CalculateTypeDamage(EntityCreature attacker, EntityCreature defender, SkillAction skillAction, int damageType, 
                                                float spellPer, float spellAdd, float damageBase, float critFactor, float stirkeFactor)
        {
            float damageAddRate = 0;
            float damageReduceRate = 0;
            float damagePernRate = 0;
            //--获取各类伤害对应的加成值
            if (damageType == public_config.SPELL_DAMAGE_PHY)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.PHY_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.PHY_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.PHY_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_MAG)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.MAG_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.MAG_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.MAG_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_FIRE)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.FIRE_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.FIRE_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.FIRE_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_ICE)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.ICE_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.ICE_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.ICE_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_LIGHNING)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.LIGHNING_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.LIGHNING_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.LIGHNING_PENETRATION_RATE);
            }
            else if (damageType == public_config.SPELL_DAMAGE_POISON)
            {
                damageAddRate = attacker.GetAttribute(fight_attri_config.POISON_DMG_ADD_RATE);
                damageReduceRate = defender.GetAttribute(fight_attri_config.POISON_DMG_REDUCE_RATE);
                damagePernRate = attacker.GetAttribute(fight_attri_config.POISON_PENETRATION_RATE);
            }

            float damage = damageBase * spellPer + spellAdd;
            //判断玩家的专精伤害加成
            if (attacker.IsPlayer())
            {
                float proficient_factor = skillAction.GetDmgProficientFactor();
                if (proficient_factor > 0)
                {
                    List<int> typesDmgCfg = skillAction.GetDmgTypes();
                    float damageTypeFactor = 0;
                    if (typesDmgCfg!=null)
                        for (int i = 0; i < typesDmgCfg.Count; ++i)
                        {
                            if (typesDmgCfg[i] == damageType)
                            {
                                List<float> dmgTypesRate = skillAction.GetDmgTypesRate();
                                damageTypeFactor = dmgTypesRate[i];
                            }
                        }
                    /*ari
                    int proficient_id = (attacker as PlayerAvatar).spell_proficient;
                    fight_attri_config proficient_attri_id = fight_attri_config.SPELL_PROFICIENT1_FACTOR + proficient_id - 1;
                    damage += proficient_factor * attacker.GetAttribute(proficient_attri_id) * damageTypeFactor;*/
                }
            }
            //判断buff对该行为的加成
            //damage = damage + attacker.GetSpellActionExDmg(skillAction.actionID, damageType);

            damage = damage * (1 + damageAddRate);
            float damageFactor = Mathf.Min(1 - damageReduceRate + damagePernRate, 1);
            damage = damage * damageFactor;
            float dmgAddFactor = critFactor;
            //damage_flag:0：没有发生额外的伤害。1：发生了暴击。2：发生了破击
            byte damageFlag = 0;
            if (critFactor > 1)
            {
                damageFlag = 1 ;
            }
            if (stirkeFactor > 1)  //发生破击
            {
                dmgAddFactor = Mathf.Max(dmgAddFactor, stirkeFactor / damageFactor);
                if (dmgAddFactor > stirkeFactor)
                {
                    damageFlag = 2;
                }
            }
            damage = damage * dmgAddFactor;
            return new SkillTypeDamage(damage, damageFlag);
        }

        //--获取伤害加成修正系数
        public static float GetDmgARCorrect(EntityCreature attacker, EntityCreature defender)
        {
            if (defender.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
            {
                var vocation = (int)(defender).vocation;
                if (vocation == public_config.VOC_WARRIOR)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_WARRIOR_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_WIZARD)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_WIZARD_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_ARCHER)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_ARCHER_DMG_ADD_RATE);
                }
                else if (vocation == public_config.VOC_ASSASSIN)
                {
                    return attacker.GetAttribute(fight_attri_config.TO_ASSASSIN_DMG_ADD_RATE);
                }
            }/*ari
            else if (defender is EntityDummy)
            {
                var monsterData = GameData.monster_helper.GetMonster((int)(defender as EntityDummy).monster_id);
                var mstType = monsterData.__type;
                var mstModelType = monsterData.__model_type;
                var addRate = attacker.GetAttribute(fight_attri_config.TO_VEHICON_DMG_ADD_RATE + mstType - 1);
                addRate = addRate + attacker.GetAttribute(fight_attri_config.TO_HUMAN_DMG_ADD_RATE + mstModelType);
                return addRate;
            }*/
            return 0;
        }

        //--获取伤害减免修正系数
        public static float GetDmgRRCorrect(EntityCreature attacker, EntityCreature defender)
        {
            if (defender.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
            {
                var vocation = (int)defender.vocation;
                if (vocation == public_config.VOC_WARRIOR)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_WARRIOR_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_WIZARD)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_WIZARD_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_ARCHER)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_ARCHER_DMG_REDUCE_RATE);
                }
                else if (vocation == public_config.VOC_ASSASSIN)
                {
                    return attacker.GetAttribute(fight_attri_config.FROM_ASSASSIN_DMG_REDUCE_RATE);
                }
            }/*ari
            else if (defender is EntityDummy)
            {
                var monsterData = GameData.monster_helper.GetMonster((int)(defender as EntityDummy).monster_id);
                var mstType = monsterData.__type;
                var mstModelType = monsterData.__model_type;
                var reduceRate = attacker.GetAttribute(fight_attri_config.FROM_VEHICON_DMG_REDUCE_RATE + mstType - 1);
                reduceRate = reduceRate + attacker.GetAttribute(fight_attri_config.FROM_VEHICON_DMG_REDUCE_RATE + mstModelType);
                return reduceRate;
            }*/
            return 0;
        }

        //--获取PVP修正值
        public static float GetPvpCorrect(EntityCreature attacker, EntityCreature defender)
        {
            var attkEntType = attacker.entityType;
            var dfdEntType = defender.entityType;
            if ((attkEntType == dfdEntType) && (attkEntType == EntityConfig.ENTITY_TYPE_NAME_AVATAR))
            {
                var pvpAddRate = attacker.GetAttribute(fight_attri_config.PVP_DMG_ADD_RATE);
                var pvpReduceRate = attacker.GetAttribute(fight_attri_config.PVP_DMG_REDUCE_RATE);
                return (1 + pvpAddRate) * (1 - pvpReduceRate);
            }
            return 1;
        }

        //--获取等级修正值
        public static float GetLevelCorrect(EntityCreature attacker, EntityCreature defender)
        {
            var dfdEntType = defender.entityType;
            if (dfdEntType == EntityConfig.ENTITY_TYPE_NAME_DUMMY)
            {
                var levelFactors = global_params_helper.DmgLevelFactors;
                var lvlDiff = defender.level - attacker.level - levelFactors[0];
                if (lvlDiff <= 0)
                {
                    return 1;
                }
                else
                {
                    return Mathf.Max(1 - levelFactors[1] * lvlDiff, levelFactors[2]);
                }
            }
            return 1;
        }

        // 计算技能行为的特殊
        public static float GetActionSpecialFactor(EntityCreature attacker, EntityCreature defender, int skillID, SkillAction skillAction, int targetCnt)
        {
            int dmgSpecialType = skillAction.GetDmgSpecialType();
            List<float> dmgSpecialArg = skillAction.GetDmgSpecialArg();
            float factor;
            int partionCnt;
            switch (dmgSpecialType)
            {
                case 1:  //按距离计算伤害系数
                    float distance = CombatSystemTools.CalculateDistanceBetween(attacker, defender) * 100; //换算为以cm为单位
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = partionCnt - 1; i >= 0; --i)
                    {
                        if (distance >= dmgSpecialArg[i])
                        {
                            return dmgSpecialArg[i + partionCnt];
                        }
                    }
                    break;
                case 2:  //按目标buff加成
                    factor = 1;
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = 0; i < partionCnt; ++i)
                    {
                        if (defender.bufferManager.ContainsBuffer((int)dmgSpecialArg[i]))
                        {
                            factor = factor * dmgSpecialArg[partionCnt + i];
                        }
                    }
                    return factor;
                case 3:     //按搜集目标数量进行调整
                    factor = 1f / targetCnt;
                    partionCnt = dmgSpecialArg.Count / 2;
                    for (int i = 0; i < partionCnt; ++i)
                    {
                        if ((int)dmgSpecialArg[i] == targetCnt)
                        {
                            factor = dmgSpecialArg[partionCnt + i];
                            break;
                        }
                    }
                    return factor;
                default:
                    break;
            }
            return 1;
        }
    }
}
