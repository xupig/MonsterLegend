﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.States;

namespace GameMain.CombatSystem
{
    public class CirclePickUpRange : BasePickUpRange
    {
        float rangeRadius;
        float skillRadius;
        Vector3 direction;
        Vector3 originPos;

        override protected void OnPlay()
        {
        }

        override protected void Update()
        {
            direction = SkillStickState.GetMoveDirectionBySkillStick();
            direction.y = 0;
            _skillTrans.position = originPos + direction * SkillStickState.strength * (rangeRadius - skillRadius);
        }

        override protected void OnStop()
        {
            this.transform.SetParent(GetRangePoolObj(), false);
            _rangePool.Enqueue(this);
            PickUpSkillControl.SetPickUpPosData(_skillTrans.position);
        }

        protected void ResetScale(float rangeRadius, float skillRadius)
        {
            _trans.localScale = new Vector3(rangeRadius, rangeRadius, rangeRadius);
            _skillTrans.localScale = new Vector3(skillRadius / rangeRadius, skillRadius / rangeRadius, skillRadius / rangeRadius);
        }

        protected void ResetSkillPos(Vector3 origin,float offsetX, float offsetY)
        {
            origin.x += offsetX;
            origin.z += offsetY;
            _skillTrans.position = origin;
        }

        protected void ResetParam(Vector3 origin, float rangeRadius, float skillRadius)
        {
            originPos = origin;
            this.rangeRadius = rangeRadius;
            this.skillRadius = skillRadius;
        }

        static void Draw(CirclePickUpRange range, Vector3 origin, float rangeRadius, float skillRadius, float offsetX, float offsetY)
        {
            range.ResetParam(origin, rangeRadius, skillRadius);
            range.ResetPosition(origin);
            range.ResetSkillPos(origin, offsetX, offsetY);
            range.ResetScale(rangeRadius * 2, skillRadius * 2);
            range.Play();
        }

        static Queue<CirclePickUpRange> _rangePool = new Queue<CirclePickUpRange>();
        static public void Draw(Vector3 origin, float rangeRadius, float skillRadius, float offsetX, float offsetY)
        {
            if (_rangePool.Count == 0)
            {
                LoadRange<CirclePickUpRange>(Utils.PathUtils.PickupCirclePath, (newRange) =>
                {                   
                    GameObject.DontDestroyOnLoad(newRange);
                    newRange.transform.SetParent(null);
                    Draw(newRange, origin, rangeRadius, skillRadius, offsetX, offsetY);
                    PickUpSkillControl.curPickUpRange = newRange;
                });
            }
            else
            {
                var range = _rangePool.Dequeue();
                range.transform.SetParent(null);
                Draw(range, origin, rangeRadius, skillRadius, offsetX, offsetY);
                PickUpSkillControl.curPickUpRange = range;
            }
        }
    }
}
