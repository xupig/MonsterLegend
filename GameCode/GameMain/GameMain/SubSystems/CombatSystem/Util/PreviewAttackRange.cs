﻿using GameLoader.Utils.Timer;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    public class PreviewAttackRange
    {
        static public void DrawEarlyWarningByRangeType(Matrix4x4 srcWorldMatrix, List<int> targetRangeParam, List<int> originAdjust, float duration,
            Color useCustomColor, float dynamicTime = 0.1f)
        {
            float offsetX = 0;
            float offsetY = 0;
            //float angleOffset = 0;
            if (originAdjust != null && originAdjust.Count > 0)
            {
                offsetX = originAdjust[0] * 0.01f;
                offsetY = originAdjust[1] * 0.01f;
                //if (originAdjust.Count >= 3)
                //{
                //    angleOffset = originAdjust[2];
                //}
            }
            if (targetRangeParam.Count >= 1)
            {
                float radius = targetRangeParam[0] * 0.01f;
                DrawEarlyWarningInCircleRange(srcWorldMatrix, radius,useCustomColor, offsetX, offsetY, 0, duration, dynamicTime);
            }
        }

        static public void DrawEarlyWarningInCircleRange(Matrix4x4 srcWorldMatrix, float radius, Color useCustomColor, float offsetX = 0, float offsetY = 0, float angleOffset = 0, float duration = 1,
        float dynamicTime = 0.1f, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, 0, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            DrawCircleStatic(posi, radius, duration, useCustomColor, dynamicTime);
            DrawCircleDynamic(posi, radius, duration, useCustomColor, dynamicTime);
        }


        static void DrawCircleStatic(Vector3 position, float radius, float duration, Color useCustomColor, float dynamicTime = 0.1f)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningCirclePath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    SetMaterialProp(alertObject, duration, useCustomColor, dynamicTime);
                    
                    //alertObject.transform.rotation = Quaternion.Euler(270, 0, 0);
                    float scale = radius * 2;
                    alertObject.transform.localScale = new Vector3(scale, scale, scale);
                    CircleGradientViaScale gradientViaScaleScript = alertObject.GetComponent<CircleGradientViaScale>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = false;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        static private void SetMaterialProp(GameObject alertObject, float duration, Color useCustomColor, float dynamicTime)
        {
            Material mat = alertObject.GetComponent<MeshRenderer>().materials[0];
            float alpha = mat.GetColor("_BaseColor").a;
            mat.SetColor("_BaseColor", new Color(useCustomColor.r, useCustomColor.g, useCustomColor.b, alpha * useCustomColor.a));
            float internalAlpha = mat.GetFloat("_InternalAlpha");
            internalAlpha = internalAlpha * useCustomColor.a;
            mat.SetFloat("_InternalAlpha", internalAlpha);

            GameTweenUtils.instance.AddTween(alertObject, 0f, duration - dynamicTime, dynamicTime, useCustomColor);
        }

        static void DrawCircleDynamic(Vector3 position, float radius, float duration, Color useCustomColor, float dynamicTime = 0.1f)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningCirclePath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    alertObject.transform.rotation = Quaternion.Euler(270, 0, 0);
                    alertObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

                    SetMaterialProp(alertObject, duration, useCustomColor, dynamicTime);
                    //Color color = alertObject.GetComponent<MeshRenderer>().materials[0].GetColor("_BaseColor");
                    //object alphaObj = color.a;


                    CircleGradientViaScale gradientViaScaleScript = alertObject.GetComponent<CircleGradientViaScale>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = true;
                        gradientViaScaleScript.animCurv.postWrapMode = WrapMode.Default;
                        gradientViaScaleScript.duration = duration - dynamicTime;
                        gradientViaScaleScript.duration = gradientViaScaleScript.duration < 0 ? 0 : gradientViaScaleScript.duration;
                        gradientViaScaleScript.maxScale = radius * 2;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

    }
}
