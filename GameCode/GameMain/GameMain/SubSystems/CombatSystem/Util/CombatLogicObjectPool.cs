﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using MogoEngine.RPC;
using GameMain;



namespace GameMain.CombatSystem
{
    public class CombatLogicObjectPool
    {
        #region skillsubject
        //skilldata缓冲池
        private static Dictionary<int, SkillData> s_skillDataDict = new Dictionary<int, SkillData>();
        public static SkillData GetSkillData(int skillID)
        {
            SkillData skillData = null;
            if (s_skillDataDict.ContainsKey(skillID))
            {
                skillData = s_skillDataDict[skillID];
            }else
            {
                skillData = new SkillData(skillID);
                s_skillDataDict.Add(skillID, skillData);
            }
            return skillData;
        }

        //skillsubject对象池
        private static Queue<SkillSubject> s_skillSubjectPool = new Queue<SkillSubject>();
        public static SkillSubject CreateSkillSubject(int skillID, int slot = 0)
        {
            SkillSubject result;
            var skillData = GetSkillData(skillID);
            if (s_skillSubjectPool.Count > 0)
            {
                result = s_skillSubjectPool.Dequeue();
            }
            else
            {
                result = new SkillSubject();
            }
            result.slot = slot;
            result.Reset(skillData);
            return result;
        }

        public static void ReleaseSkillSubject(SkillSubject skillSubject)
        {
            s_skillSubjectPool.Enqueue(skillSubject);
        }

        //SkillSubjectServer对象池
        private static Queue<SkillSubjectServer> s_skillSubjectServerPool = new Queue<SkillSubjectServer>();
        public static SkillSubjectServer CreateSkillSubjectServer(int skillID, int slot = 0)
        {
            SkillSubjectServer result;
            var skillData = GetSkillData(skillID);
            if (s_skillSubjectServerPool.Count > 0)
            {
                result = s_skillSubjectServerPool.Dequeue();
            }
            else
            {
                result = new SkillSubjectServer();
            }
            result.slot = slot;
            result.Reset(skillData);
            return result;
        }

        public static void ReleaseSkillSubjectServer(SkillSubjectServer skillSubject)
        {
            s_skillSubjectServerPool.Enqueue(skillSubject);
        }

        #endregion

        #region skillaction
        private static Dictionary<int, SkillActionData> s_skillActionDataDict = new Dictionary<int, SkillActionData>();
        public static SkillActionData GetSkillActionData(int actionID)
        {
            SkillActionData skillActionData = null;
            if (s_skillActionDataDict.ContainsKey(actionID))
            {
                skillActionData = s_skillActionDataDict[actionID];
            }
            else
            {
                try
                {
                    skillActionData = new SkillActionData(actionID);
                    s_skillActionDataDict.Add(actionID, skillActionData);
                }
                catch (Exception ex)
                {
                    GameLoader.Utils.LoggerHelper.Error("SkillActionData init error:" + actionID);
                    GameLoader.Utils.LoggerHelper.Error(ex);
                }
            }
            return skillActionData;
        }

        //skillsubject对象池
        private static Queue<SkillAction> s_skillActionPool = new Queue<SkillAction>();
        public static SkillAction CreateSkillAction(int actionID)
        {
            SkillAction result;
            var actionData = GetSkillActionData(actionID);
            if (s_skillActionPool.Count > 0)
            {
                result = s_skillActionPool.Dequeue();
            }
            else
            {
                result = new SkillAction();
            }
            result.Reset(actionData);
            return result;
        }

        public static void ReleaseSkillAction(SkillAction skillAction)
        {
            skillAction.Clear();
            s_skillActionPool.Enqueue(skillAction);
        }

        //skillActionServer对象池
        private static Queue<SkillActionServer> s_skillActionServerPool = new Queue<SkillActionServer>();
        public static SkillActionServer CreateSkillActionServer(int actionID)
        {
            SkillActionServer result;
            var actionData = GetSkillActionData(actionID);
            if (s_skillActionServerPool.Count > 0)
            {
                result = s_skillActionServerPool.Dequeue();
            }
            else
            {
                result = new SkillActionServer();
            }
            result.Reset(actionData);
            return result;
        }

        public static void ReleaseSkillActionServer(SkillActionServer skillAction)
        {
            skillAction.Clear();
            s_skillActionServerPool.Enqueue(skillAction);
        }
        #endregion

        #region throwobject
        private static Dictionary<int, ThrowObjectData> s_throwObjectDataDict = new Dictionary<int, ThrowObjectData>();
        public static ThrowObjectData GetThrowObjectData(int id)
        {
            ThrowObjectData throwObjectData = null;
            if (s_throwObjectDataDict.ContainsKey(id))
            {
                throwObjectData = s_throwObjectDataDict[id];
            }
            else
            {
                try
                {
                    throwObjectData = new ThrowObjectData(id);
                    s_throwObjectDataDict.Add(id, throwObjectData);
                }
                catch (Exception ex)
                {
                    GameLoader.Utils.LoggerHelper.Error("ThrowObjectData init error:" + id);
                    GameLoader.Utils.LoggerHelper.Error(ex);
                }
            }
            return throwObjectData;
        }
        #endregion

        #region trapobject
        private static Dictionary<int, TrapData> s_trapObjectDataDict = new Dictionary<int, TrapData>();
        public static TrapData GetTrapObjectData(int id)
        {
            TrapData trapObjectData = null;
            if (s_trapObjectDataDict.ContainsKey(id))
            {
                trapObjectData = s_trapObjectDataDict[id];
            }
            else
            {
                try
                {
                    trapObjectData = new TrapData(id);
                    s_trapObjectDataDict.Add(id, trapObjectData);
                }
                catch (Exception ex)
                {
                    GameLoader.Utils.LoggerHelper.Error("TrapData init error:" + id);
                    GameLoader.Utils.LoggerHelper.Error(ex);
                }
            }
            return trapObjectData;
        }
        #endregion

        #region buffer
        //BufferData缓冲池
        private static Dictionary<int, BufferData> s_bufferDataDict = new Dictionary<int, BufferData>();
        public static BufferData GetBufferData(int bufferID)
        {
            BufferData data;
            if (s_bufferDataDict.ContainsKey(bufferID))
            {
                data = s_bufferDataDict[bufferID];
            }
            else
            {
                data = new BufferData(bufferID);
                s_bufferDataDict.Add(bufferID, data);
            }
            return data;
        }

        //skillsubject对象池
        private static Queue<BufferSubject> s_bufferSubjectPool = new Queue<BufferSubject>();
        public static BufferSubject CreateBufferSubject(int skillID)
        {
            BufferSubject result;
            var bufferData = GetBufferData(skillID);
            if (s_bufferSubjectPool.Count > 0)
            {
                result = s_bufferSubjectPool.Dequeue();
            }
            else
            {
                result = new BufferSubject();
            }
            result.Reset(bufferData);
            return result;
        }

        public static void ReleaseBufferSubject(BufferSubject bufferSubject)
        {
            s_bufferSubjectPool.Enqueue(bufferSubject);
        }
        #endregion


        public static void ClearAllData()
        {
            s_skillDataDict.Clear();
            s_skillActionDataDict.Clear();
            s_bufferDataDict.Clear();
            s_throwObjectDataDict.Clear();
        }
    }
}
