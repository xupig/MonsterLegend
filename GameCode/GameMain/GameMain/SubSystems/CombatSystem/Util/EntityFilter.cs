﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;
using GameData;
using MogoEngine;
using MogoEngine.RPC;

namespace GameMain.CombatSystem
{
    public enum EntityFilterType
    {
        SKILL_SUBJECT_FILTER = 1,
        SKILL_ACTION_FILTER = 2,
    } 

    public class EntityFilter
    {

        static public void GetEntitiesByRangeType(List<uint> result, Matrix4x4 srcWorldMatrix, TargetRangeType targetRangeType, List<int> targetRangeParam, List<int> originAdjust, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            float offsetX = 0;
            float offsetY = 0;
            float offsetH = 0;
            float angleOffset = 0;
            if (originAdjust != null && originAdjust.Count>0)
            {
                offsetX = originAdjust[0] * 0.01f;
                offsetY = originAdjust[1] * 0.01f;
                offsetH = originAdjust[2] * 0.01f;
                if (originAdjust.Count>=4) 
                { 
                    angleOffset = originAdjust[3]; 
                }
            }
            result.Clear();
            switch (targetRangeType)
            {
                case TargetRangeType.SphereRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        EntityFilter.GetEntitiesInSphereRange(result, srcWorldMatrix, radius, offsetX, offsetY, offsetH, 0, filterType);
                    }
                    break;
                case TargetRangeType.SectorRange:
                    if (targetRangeParam.Count >= 3)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        float angle = targetRangeParam[1];
                        float height = targetRangeParam[2] * 0.01f;
                        EntityFilter.GetEntitiesInSectorRange(result, srcWorldMatrix, radius, angle, height, offsetX, offsetY, offsetH, angleOffset, filterType);
                    }
                    break;
                case TargetRangeType.ForwardRectangleRange:
                    if (targetRangeParam.Count >= 3)
                    {
                        float length = targetRangeParam[0] * 0.01f;
                        float width = targetRangeParam[1] * 0.01f;
                        float height = targetRangeParam[2] * 0.01f;
                        EntityFilter.GetEntitiesInRectangleRange(result, srcWorldMatrix, length, width, height, offsetX, offsetY, offsetH, angleOffset, filterType);
                    }
                    break;
                case TargetRangeType.CylinderRange:
                    if (targetRangeParam.Count >= 2)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        float height = targetRangeParam[1] * 0.01f;
                        EntityFilter.GetEntitiesInCylinderRange(result, srcWorldMatrix, radius, height, offsetX, offsetY, offsetH, 0, filterType);
                    }
                    break;
            }
        }

        static public void GetEntitiesInSphereRange(List<uint> list, Matrix4x4 srcWorldMatrix, float radius, float offsetX = 0, float offsetY = 0, float offsetH = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            //Debug.LogError("GetEntitiesInCircleRange:");
            //Matrix4x4 ltwM = transform.localToWorldMatrix;
            list.Clear();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetH, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature entity = entities[entityIDs[i]] as EntityCreature;
                if (entity == null) continue;
                var eTransform = entity.GetTransform();
                if (eTransform == null) continue;
                Vector3 entityPos = eTransform.position;
                if (entityPos.y > posi.y + radius || entityPos.y + entity.hitHeight < posi.y - radius) continue;
                if ((posi - entityPos).magnitude > (radius + entity.hitRadius)) continue;
                list.Add(entity.id);
            }
            DrawRangeTools.DrawCircleRange(posi, radius, filterType);
        }

        static Vector2 dirV2 = Vector2.zero;
        static public void GetEntitiesInSectorRange(List<uint> list, Matrix4x4 srcWorldMatrix, float radius, float angle = 180f, float height = 2, float offsetX = 0, float offsetY = 0, float offsetH = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            list.Clear();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetH, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            float fixAngle = angle * 0.5f;
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            m = m * matrixAngleOffset;
            Vector3 forward = m.forward().normalized;
            Vector3 right = m.right().normalized;
            Vector3 upwards = m.upwards().normalized;
            Vector3 normal = Vector3.Cross(forward, right).normalized;

            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature entity = entities[entityIDs[i]] as EntityCreature;
                if (entity == null) continue;
                var eTransform = entity.GetTransform();
                if (entity.GetTransform() == null)
                {
                    continue;
                }
                Vector3 entityPos = eTransform.position;
                float entityRadius = entity.hitRadius;
                Vector3 dir = entityPos - posi;
                dirV2.x=dir.x;
                dirV2.y=dir.z;
                if (dirV2.magnitude > radius) continue;
                if (dirV2.magnitude < -entityRadius || dirV2.magnitude > radius + entityRadius
                    || dir.y < -entityRadius || dir.y > height + entityRadius)
                    continue;
                //得到切线与（目标物体到人物线）的夹角a
                float a = Mathf.Asin(entityRadius / dir.magnitude) * Mathf.Rad2Deg;
                //得到目标点与人物正前方的夹角b
                float b = Vector3.Angle(dir, forward);
                //判断b - a 是否在 angle/2内
                if ((b - a) > fixAngle) continue;
                list.Add(entity.id);
            }
            DrawRangeTools.DrawSectorRange(posi, forward, radius, angle, height, filterType);
        }

        static public void GetEntitiesInRectangleRange(List<uint> list, Matrix4x4 srcWorldMatrix, float length, float width, float height, float offsetX = 0, float offsetY = 0, float offsetH = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            list.Clear();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetH, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            m = m * matrixAngleOffset;
            Vector3 forward = m.forward().normalized;
            Vector3 right = m.right().normalized;
            Vector3 upwards = m.upwards().normalized;
            var rightBottomPoint = posi + right * (width * 0.5f);
            rightBottomPoint.y = posi.y;
            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature entity = entities[entityIDs[i]] as EntityCreature;
                if (entity == null) continue;
                float entityRadius = entity.hitRadius;
                var entityPos = entity.position;
                var dir = entityPos - rightBottomPoint;
                var dotL = Vector3.Dot(dir, forward);
                var dotW = Vector3.Dot(dir, -right);
                float dotU = Vector3.Dot(dir, upwards);
                if (dotL < -entityRadius || dotL > length + entityRadius
                    || dotW < -entityRadius || dotW > width + entityRadius
                    || dotU < -entityRadius || dotU > height + entityRadius)
                {
                    continue;
                }
                list.Add(entity.id);                
            }
            DrawRangeTools.DrawRectangleRange(posi, forward, right, length, width, height, filterType);
        }

        static public void GetEntitiesInCylinderRange(List<uint> list, Matrix4x4 srcWorldMatrix, float radius, float height, float offsetX = 0, float offsetY = 0, float offsetH = 0, float angleOffset = 0, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            //Debug.LogError("GetEntitiesInCircleRange:");
            //Matrix4x4 ltwM = transform.localToWorldMatrix;
            //List<uint> list = new List<uint>();
            list.Clear();
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetH, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            Vector3 posXZ = posi;
            posXZ.y = 0;
            Vector3 entityPosXZ = Vector3.zero;
            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature entity = entities[entityIDs[i]] as EntityCreature;
                if (entity == null) continue;
                var entityPos = entity.position;
                if (entityPos.y > posi.y + height || entityPos.y + entity.hitHeight < posi.y) continue;
                entityPosXZ = entityPos;
                entityPosXZ.y = 0;
                if ((posXZ - entityPosXZ).magnitude > (radius + entity.hitRadius)) continue;
                list.Add(entity.id);
            }
            DrawRangeTools.DrawCylinderRange(posi, radius, height, filterType);
        }
    }
}
