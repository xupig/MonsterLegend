﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SphereRange : BaseRange
    {

        override protected void OnPlay()
        {

        }

        override protected void OnStop()
        {
            this.transform.SetParent(GetRangePoolObj(), false);
            _rangePool.Enqueue(this);
        }

        protected void ResetRadius(float radius)
        {
            _trans.localScale = new Vector3(radius * 2, radius * 2, radius * 2);
        }

        static void Draw(SphereRange range, Vector3 origin, float radius, Color color, float time)
        {
            range.ResetPosition(origin);
            range.ResetDuration(time);
            range.ResetColor(color);
            range.ResetRadius(radius);
            range.Play();
        }

        static Queue<SphereRange> _rangePool = new Queue<SphereRange>();
        static public void Draw(Vector3 origin, float radius, Color color, float time)
        {
            if (_rangePool.Count == 0)
            {
                LoadRange<SphereRange>(Utils.PathUtils.SphereRangePath, (newRange) =>
                {
                    GameObject.DontDestroyOnLoad(newRange);
                    newRange.transform.SetParent(null);
                    Draw(newRange, origin, radius, color, time);
                });
            }
            else
            {
                var range = _rangePool.Dequeue();
                range.transform.SetParent(null);
                Draw(range, origin, radius, color, time);
            }

        }


    }
}
