﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class RectangleRange : BaseRange
    {

        override protected void OnPlay()
        {

        }

        override protected void OnStop()
        {
            this.transform.SetParent(GetRangePoolObj(), false);
            _rangePool.Enqueue(this);
        }

        protected void ResetScale(float length, float width, float high)
        {
            _trans.localScale = new Vector3(width, high, length);
        }

        protected void ResetForward(Vector3 forward)
        {
            this.transform.forward = forward;
        }

        static void Draw(RectangleRange range, Vector3 origin, Vector3 forward, Vector3 right, float length, float width, float high, Color color, float time)
        {
            range.ResetPosition(origin);
            range.ResetDuration(time);
            range.ResetColor(color);
            range.ResetForward(forward);
            range.ResetScale(length, width, high);
            range.Play();
        }

        static Queue<RectangleRange> _rangePool = new Queue<RectangleRange>();
        static public void Draw(Vector3 origin, Vector3 forward, Vector3 right, float length, float width, float high, Color color, float time)
        {
            if (_rangePool.Count == 0)
            {
                LoadRange<RectangleRange>(Utils.PathUtils.RectangleRangePath, (newRange) =>
                {
                    GameObject.DontDestroyOnLoad(newRange);
                    newRange.transform.SetParent(null);
                    Draw(newRange, origin, forward, right, length, width, high, color, time);
                });
            }
            else
            {
                var range = _rangePool.Dequeue();
                range.transform.SetParent(null);
                Draw(range, origin, forward, right, length, width, high, color, time);
            }

        }


    }
}
