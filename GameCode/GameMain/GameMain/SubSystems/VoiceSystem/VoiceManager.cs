﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Common.Utils;
using GameLoader.Utils;
using System.Net;
using UnityEngine;
using GameLoader.Utils.Timer;
using GameLoader;

namespace GameMain
{
    public class VoiceManager
    {
        public delegate void EndVoiceTalkCallback(string voiceText);
        public static readonly string AUDIO_extension = "amr";
        private static VoiceManager _instance;

        private string _voiceUrl = "http://192.168.11.25/";
        private bool _bStartTalk = false;  //标志位，决定是否发送

        public static VoiceManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new VoiceManager();
                    //ClearFile();
                }
                return _instance;
            }
        }

        public bool IsStartTalk
        {
            get { return _bStartTalk; }
            set { _bStartTalk = value; }
        }

        public VoiceManager()
        {
            _voiceUrl = SystemConfig.GetValueInCfg("voiceUrl");
        }

        public byte[] PostData(string url, byte[] postData)
        {
            byte[] responseData = null;
            try
            {
                WebClient webClient = new WebClient();

                responseData = webClient.UploadData(url, postData);
            }
            catch (System.Exception ex)
            {
                LoggerHelper.Except(ex);
            }
            return responseData;
        }

        public void DownloadVoice(string fileName, string key)
        {
#if !UNITY_WEBPLAYER
            LoggerHelper.Info("[Chat]fileName = " + fileName + "  key = " + key);
            if (File.Exists(fileName))
                return;

            byte[] sendData = new byte[5];
            string md5data = string.Concat("get98I*&%^%/rsfsRx>L", key);
            var md5 = System.Security.Cryptography.MD5.Create();
            md5data = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(md5data))).Replace("-", "").ToLower();
            byte[] result = PostData(_voiceUrl + "action=get&md5=" + md5data + "&chat_id=" + key, sendData);
            if (result == null)
                return;
            if (result.Length > 0)
            {
                //LoggerHelper.Info(Convert.ToInt32(result[1]) + "[Chat] get 错误码 = " + Convert.ToInt32(result[0]));
                if (result[0] == '0' && result[1] == ',')
                {
                    int i = 0;
                    for (i = 0; i < result.Length; i++)
                    {
                        if (result[i] == ',')
                            break;
                    }
                    int curIndex = i + 1;
                    //LoggerHelper.Info(curIndex + " : " + result.Length);
                    if (curIndex < result.Length)
                    {
                        //string fileName = Application.persistentDataPath + "/UnityVoice/" + path+".png";
                        byte[] data = new byte[result.Length - curIndex];

                        Array.ConstrainedCopy(result, curIndex, data, 0, result.Length - curIndex);
                        string base64String = Encoding.Default.GetString(data);
                        //Debug.Log(base64String);
                        File.WriteAllBytes(fileName, Convert.FromBase64String(base64String));
                    }
                }
            }
#endif
        }

        public void OnDownloadAndPlayVoice(string path)
        {
            LoggerHelper.Info("[Chat]path = " + path);
            string fileName = "";
            if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer)
            {
                fileName = Application.persistentDataPath + "/UnityVoice/" + path + "." + "wav";
            }
            else
            {
                fileName = Application.persistentDataPath + "/UnityVoice/" + path + "." + AUDIO_extension;
            }
            if (!Directory.Exists(Application.persistentDataPath + "/UnityVoice/"))
                Directory.CreateDirectory(Application.persistentDataPath + "/UnityVoice/");
            Action action = () =>
            {
                DownloadVoice(fileName, path);

                Invoke(() =>
                {
#if UNITY_ANDROID
                    GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GotyePlayVoiceMessage("/" + path + "." + AUDIO_extension,"");
                    

#endif

#if UNITY_IPHONE && !UNITY_EDITOR
                    //IOSPlugins.PlaySpeech("/" + path + "." + AUDIO_extension);
#endif
                    LoggerHelper.Info("[OnDownloadAndPlayVoice]: Path = " + "/" + path + "." + AUDIO_extension);
                    if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer)
                    {
                        //AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                        //AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                        //mainActivity.Call("playMusic", "/" + path + "." + AUDIO_extension);
                        string p = path + "." + "wav";
                        //WindowsVoice.GetInstance().Play(fileName);
                    }

                });
            };
            action.BeginInvoke(null, null);
        }

        public void OnEndVoiceTalk(string text, string path,int duration, EndVoiceTalkCallback endVoiceCallback)
        {
            string destPath = Application.persistentDataPath + "/UnityVoice/" + path;
            LoggerHelper.Info("[Chat]desPath = " + destPath);
            LoggerHelper.Info("[Chat]voiceUrl = " + _voiceUrl);
            if (!File.Exists(destPath))
            {
                LoggerHelper.Info("[Chat]EndVoiceCallback 1");
                endVoiceCallback(text);
                return;
            }
#if !UNITY_WEBPLAYER
            Action action = () =>
            {
                byte[] voiceData = File.ReadAllBytes(destPath);
                string postString = Convert.ToBase64String(voiceData);
                string md5data = String.Concat("put$%8*&%^%/rsfsRx00",postString);
                var md5 = System.Security.Cryptography.MD5.Create();
                md5data = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(md5data))).Replace("-", "").ToLower();

                byte[] result = PostData(_voiceUrl + "action=put&md5=" + md5data, Encoding.Default.GetBytes(postString));
                if (result == null)
                {
                    Invoke(() => { LoggerHelper.Info("[Chat]endVoiceCallback 2"); endVoiceCallback(text); });
                    return;
                }
                string[] s = Encoding.UTF8.GetString(result).Split(new char[] { ',' });
                if (s.Length == 2)
                {
                    if (Convert.ToInt32(s[0]) == 0)
                    {
                        Invoke(() => { LoggerHelper.Info("[Chat]endVoiceCallback 3"); endVoiceCallback(text + "<vmsg=" + s[1] + ">" + "<vtime=" + duration + ">"); });
                    }
                    else
                    {
                        Invoke(() => { LoggerHelper.Info("[Chat]endVoiceCallback 4 错误码：" + Convert.ToInt32(s[0])); endVoiceCallback(text); });
                    }
                }
                else
                {
                    Invoke(() => { LoggerHelper.Info("[Chat]endVoiceCallback 5 错误码：" + Convert.ToInt32(s[0])); endVoiceCallback(text); });
                }
            };
            action.BeginInvoke(null, null);
#endif
        }

        public void CallVoiceSdk()
        {
#if UNITY_ANDROID
            LoggerHelper.Info("[Chat]platform = " + UnityPropUtils.Platform);
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                try
                {
                    AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                    mainActivity.Call("startTalk");
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("CallVoiceSdk  exception stack trace = " + ex.StackTrace);
                }
            }
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
            //IOSPlugins.BeginSpeech();
#endif
        }

        public void OnStopTalk()
        {
#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                mainActivity.Call("stopTalk");
            }
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
           // IOSPlugins.EndSpeech();
#endif
        }

        public void OnCancelTalk()
        {
#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                mainActivity.Call("cancelTalk");
            }
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
           // IOSPlugins.CancelSpeech();
#endif
        }

        private void Invoke(Action action)
        {
            TimerHeap.AddTimer(0, 0, action);
        }

        public void ClearFile()
        {
            string path = Application.persistentDataPath + "/UnityVoice/";
            if (Directory.Exists(path))
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                FileInfo[] files = dir.GetFiles();
                try
                {
                    foreach (var item in files)
                    {
                        File.Delete(item.FullName);
                    }
                }
                catch (Exception)
                {
                   
                }
            }
        }
    }
}
