using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using Common.ServerConfig;

namespace GameMain
{
    public class ProjectionManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ProjectionManager
    {
        public static string PROJECTION_PATH = "Fx/UI/projection.prefab";

        private static ProjectionManager s_instance = null;
        public static ProjectionManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new ProjectionManager();
            }
            return s_instance;
        }

        List<EntityCreature> _creatureList = new List<EntityCreature>();
        List<Transform> _tranformList = new List<Transform>();
        bool _running = false;
        ProjectionManager()
        {
            InitTipsGameObject();
        }

        public void OnPlayerEnterWorld()
        {
            MogoEngine.MogoWorld.RegisterUpdate<ProjectionManagerUpdateDelegate>("ProjectionManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("ProjectionManager.Update", Update);
        }

        public void Run()
        {
            if (_running) return;
            _running = true;
            RemoveAllCreature();
            AddAllCreature();
        }

        public void Stop()
        {
            if (!_running) return;
            _running = false;
            RemoveAllCreature();
        }

        int _spaceFrame = 0;
        private void Update()
        {
            if (!_running) return;
            UpdateCreatures();
            _spaceFrame--;
            if (_spaceFrame > 0) return;
            _spaceFrame = 30;
        }

        float _scaleF = 5f;
        private void UpdateCreatures()
        {
            for (int i = _creatureList.Count-1; i >=0; i--)
            {
                var creature = _creatureList[i];
                var tranform = _tranformList[i];
                if (creature.InState(state_config.AVATAR_STATE_DEATH) || !InScreen(creature) 
                    || !creature.showProjection || creature.actor == null || creature.actor.visible == false)
                {
                    tranform.position = _defaultPos;
                } 
                else {
                    Vector3 targetPosition = creature.actor.position;
                    float gh = creature.GetGroundHigh();
                    float deltaH = ACTSystem.ACTMathUtils.Abs(targetPosition.y - gh);
                    if (deltaH >= _scaleF)
                    {
                        tranform.position = _defaultPos;
                    }
                    else
                    {
                        targetPosition.y = creature.GetGroundHigh() + 0.03f;
                        tranform.position = targetPosition;
                        Vector3 targetScale = Vector3.one * ((_scaleF - deltaH) / _scaleF);
                        targetScale.z = 1;
                        tranform.localScale = targetScale;
                    }
                }
            }
        }

        bool InScreen(EntityCreature target)
        {
            Vector3 targetSP = CameraManager.GetInstance().Camera.WorldToScreenPoint(target.position);
            return !(targetSP.x < 0 || targetSP.x > Screen.width || targetSP.y < 0 || targetSP.z < 0);
        }

        Vector3 _defaultPos = new Vector3(-1000, -1000, -1000);
        GameObject _root;
        GameObject _template;
        public void InitTipsGameObject()
        {
            _root = new GameObject("ProjectionsRoot");
            UnityEngine.Object.DontDestroyOnLoad(_root);
            var paths = new string[] { PROJECTION_PATH };
            GameResource.ObjectPool.Instance.DontDestroy(paths);
            GameResource.ObjectPool.Instance.GetGameObject(PROJECTION_PATH, (go) =>
            {
                _template = go;
                _template.name = "Projection";
                _template.transform.position = _defaultPos;
                _template.transform.SetParent(_root.transform, false);
            });
        }

        Queue<Transform> _projectionObjectPool = new Queue<Transform>();
        public Transform CreateProjectionObject()
        {
            if (_projectionObjectPool.Count == 0)
            {
                var trans = GameObject.Instantiate(_template).transform;
                trans.SetParent(_root.transform, false);
                _projectionObjectPool.Enqueue(trans);
            }
            return _projectionObjectPool.Dequeue();
        }

        public void ReleaseProjectionObject(Transform trans)
        {
            trans.position = _defaultPos;
            _projectionObjectPool.Enqueue(trans);
        }

        void AddCreature(EntityCreature creature)
        {
            for (int i = 0; i < _creatureList.Count; i++)
            {
                if (_creatureList[i].id == creature.id)
                {
                    return;
                }
            }
            _creatureList.Add(creature);
            _tranformList.Add(CreateProjectionObject());
        }

        void RemoveCreature(EntityCreature creature)
        {
            for (int i = 0; i < _creatureList.Count; i++)
            {
                if (_creatureList[i].id == creature.id)
                {
                    ReleaseProjectionObject(_tranformList[i]);
                    _creatureList.RemoveAt(i);
                    _tranformList.RemoveAt(i);
                    break;
                }
            }
        }

        void AddAllCreature()
        {
            foreach (var item in MogoWorld.Entities)
            {
                if (!(item.Value is EntityCreature)) continue;
                OnCreatureEnterWorld((item.Value as EntityCreature));
            }
        }

        void RemoveAllCreature()
        {
            for (int i = _creatureList.Count - 1; i >= 0; i--)
            {
                ReleaseProjectionObject(_tranformList[i]);
                _creatureList.RemoveAt(i);
                _tranformList.RemoveAt(i);
            }
        }

        public void OnCreatureEnterWorld(EntityCreature creature)
        {
            if (!_running) return;
            if (!creature.showProjection) return;
            AddCreature(creature);
        }

        public void OnCreatureLeaveWorld(EntityCreature creature)
        {
            if (!_running) return;
            RemoveCreature(creature);
        }
    }
}