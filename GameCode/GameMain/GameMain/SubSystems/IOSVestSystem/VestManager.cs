﻿
using GameLoader.Utils;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;
using ArtTech;
using MogoEngine.Mgrs;
using GameLoader;
using LitJson;
using GameResource;
using MogoEngine.Events;
using Common.Events;
using ShaderUtils;
namespace GameMain
{
    public class VestPostEffect : MonoBehaviour
    {
        public Material _dcMaterial = null;
        void Awake()
        {
            Shader dcShader = ShaderRuntime.loader.Find("CameraFilterPack/Drawing_Comics_ZHJ");
            if (dcShader.isSupported)
            {
                _dcMaterial = new Material(dcShader);
            }
        }

        public void ResetMaterialProperties(VestPostEffectSetting setting)
        {
            if (_dcMaterial == null) return;
            _dcMaterial.SetFloat("_TimeX", setting.time);
            _dcMaterial.SetFloat("_DotSize", setting.dotSize);
            _dcMaterial.SetColor("_Color", setting.color);
            _dcMaterial.SetInt("_Hue", setting.hue);
            _dcMaterial.SetFloat("_Saturation", setting.saturation);
            _dcMaterial.SetFloat("_Value", setting.value);
        }

        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (_dcMaterial == null)
            {
                Graphics.Blit(source, destination);
            }
            else {
                Graphics.Blit(source, destination, _dcMaterial);
            }
        }
    }

    public class VestPostEffectSetting
    {
        public float time;
        public float dotSize;
        public Color color;
        public int hue;
        public float saturation;
        public float value;

        public VestPostEffectSetting(){ }
    }

    public class VestManager
    {
        private const string DEFAULT_JSON_SETTING =
        @"{
            'COLOR_MAIN':'1,1.3,1,1',
            'COLOR_SHADER':
                  {
                        'Monster':'0.6,0.6,0.6,0',
                        'Boss':'1,1,1.5,1',
                        'UGUI_ICON':'1,1,1,1',
                        'UGUI_PANEL':'1,1,1,1',
                        'Lightmap_Wind_Cut':'0.1,0.4,0.1,1,0',
                        'Lightmap_Wind':'0.1,0.4,0.4,1,0',
                        'WindCut':'0.1,0.4,0.4,1,0',
                        'Texture':'0.5,0.5,0.5,1,0',
                        'Terrain-Vest-Diffuse':'0.5,0.5,0.5,1,0',
                        'VirtualGloss_PerVertex_Additive_Lightmap':'0.5,0.5,0.5,1,0'
                  },
            'ACTOR_SCALE':
                {
                    'Monster':'1,0.5,1',  
                    'NPC':'1,1,1' 
                },
            'POST_EFFECT':
                {
                    'switch':'1',  
                    'dotSize':'0.06', 
                    'color':'0.3,0.2,0.2,1',  
                    'hue':'200', 
                    'saturation':'0.86',  
                    'value':'3'
                }
        }";
        
        public const string COLOR_MAIN = "COLOR_MAIN";
        public const string COLOR_SHADER = "COLOR_SHADER";
        public const string ACTOR_SCALE = "ACTOR_SCALE";
        public const string POST_EFFECT = "POST_EFFECT";

        private static VestManager s_instance = null;
        public static VestManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new VestManager();
            }
            return s_instance;
        }

        public bool ForIOSVest = false;
        Dictionary<string, Vector3> _actorScaleSetting = null;
        VestPostEffectSetting _vestPostEffectSetting = null;

        private VestManager()
        {
            InitSetting();
        }

        private void InitSetting()
        {
            EventDispatcher.AddEventListener(CameraEvents.ON_MAIN_CAMERA_LOADED, OnRefreshMainCamera);
            string jsonStr = SystemConfig.VestPackageSetting;
            //jsonStr = DEFAULT_JSON_SETTING;
            if (!string.IsNullOrEmpty(jsonStr))
            {
                JsonData jsonData = JsonMapper.ToObject(jsonStr);
                InitColor(jsonData);
                InitScale(jsonData);
                InitPostEffect(jsonData);
                ForIOSVest = true;
                IOSVestSetting.ForIOSVest = ForIOSVest;
            }
        }

        private void InitColor(JsonData jsonData)
        {
            if (jsonData.Keys.Contains(COLOR_MAIN))
            {
                IOSVestSetting.VestColorDefault = GetColorFromStr((string)jsonData[COLOR_MAIN]);
            }

            if (jsonData.Keys.Contains(COLOR_SHADER))
            {
                var sub = jsonData[COLOR_SHADER];
                foreach (var key in sub.Keys)
                {
                    IOSVestSetting.VestColorMap[key] = GetVestShaderData((string)sub[key]);
                }
            }
        }

        private void InitScale(JsonData jsonData)
        {
            if (jsonData.Keys.Contains(ACTOR_SCALE))
            {
                _actorScaleSetting = new Dictionary<string, Vector3>();
                var sub = jsonData[ACTOR_SCALE];
                foreach (var key in sub.Keys)
                {
                    _actorScaleSetting[key] = GetScaleFromStr((string)sub[key]);
                }
            }
        }

        private void InitPostEffect(JsonData jsonData)
        {
            if (jsonData.Keys.Contains(POST_EFFECT))
            {
                var sub = jsonData[POST_EFFECT];
                _vestPostEffectSetting = new VestPostEffectSetting();
                _vestPostEffectSetting.time = float.Parse((string)sub["switch"]);
                _vestPostEffectSetting.dotSize = float.Parse((string)sub["dotSize"]);
                _vestPostEffectSetting.color = GetColorFromStr((string)sub["color"]);
                _vestPostEffectSetting.hue = int.Parse((string)sub["hue"]);
                _vestPostEffectSetting.saturation = float.Parse((string)sub["saturation"]);
                _vestPostEffectSetting.value = float.Parse((string)sub["value"]);
                OnRefreshMainCamera();
            }
        }

        private void OnRefreshMainCamera()
        {
            if (!ForIOSVest || _vestPostEffectSetting == null) return;
            var mainCamera = CameraManager.GetInstance().mainCamera;
            if (mainCamera != null)
            {
                GameObject go = mainCamera.gameObject;
                if (go.GetComponent<VestPostEffect>() == null)
                {
                    var vpe = go.AddComponent<VestPostEffect>();
                    vpe.ResetMaterialProperties(_vestPostEffectSetting);
                }
            }
        }

        private Color GetColorFromStr(string content)
        {
            string[] tempArray = content.Split(',');
            return new Color(float.Parse(tempArray[0]),
                        float.Parse(tempArray[1]), float.Parse(tempArray[2]), float.Parse(tempArray[3]));
        }

        private IOSVestShaderData GetVestShaderData(string content)
        {
            string[] tempArray = content.Split(',');
            Color color = new Color(float.Parse(tempArray[0]),
                        float.Parse(tempArray[1]), float.Parse(tempArray[2]), float.Parse(tempArray[3]));
            bool poly = tempArray.Length > 4 && float.Parse(tempArray[4]) == 1;
            return new IOSVestShaderData(color, poly);
        }

        private Vector3 GetScaleFromStr(string content)
        {
            string[] tempArray = content.Split(',');
            return new Vector3(float.Parse(tempArray[0]),
                        float.Parse(tempArray[1]), float.Parse(tempArray[2]));
        }

        public Vector3 GetScale(string entityType)
        {
            Vector3 scale = Vector3.one;
            if (ForIOSVest && _actorScaleSetting != null)
            {
                if (!_actorScaleSetting.TryGetValue(entityType, out scale))
                {
                    scale = Vector3.one;
                }
            }
            return scale;
        }
    }
}
