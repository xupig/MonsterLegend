﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


namespace GameMain
{
    public class MinPointHeap
    {
        private List<point> _minHeap = new List<point>();

        public int Count
        {
            get { return _minHeap.Count; }
        }

        public void Clear()
        {
            _minHeap.Clear();
        }
        /// <summary>
        /// 加入新元素
        /// </summary>
        /// <param name="p"></param>
        public void HeapInsert(point p)
        {
            _minHeap.Add(p);
            ChangeHeap();
        }
        /// <summary>
        /// 取出堆顶元素
        /// </summary>
        /// <returns></returns>
        public point ExtractMin()
        {
            if (Count <= 0)
            {
                return null;
            }
            ChangeHeap();
            point min = _minHeap[0];
            int theTail = Count - 1;
            _minHeap[0] = _minHeap[theTail];
            _minHeap.Remove(_minHeap[theTail]);
            ChangeHeap();
            return min;
        }
        /// <summary>
        /// 重新整理最小堆
        /// </summary>
        /// <param name="top"></param>
        private void ChangeHeap()
        {
            int theLeast = 0;
            int curIndex;
            int HeapSize = Count;
            int theL;
            int theR;
            do
            {
                curIndex = theLeast;
                theL = HeapL(curIndex);
                theR = HeapR(curIndex);
                if (theL < HeapSize && _minHeap[theL].F < _minHeap[theLeast].F)
                {
                    theLeast = theL;
                }
                if (theR < HeapSize && _minHeap[theR].F < _minHeap[theLeast].F)
                {
                    theLeast = theR;
                }

                if (theLeast != curIndex)
                {
                    SwapElement(curIndex, theLeast);
                }

            } while (theLeast != curIndex);
        }
        /// <summary>
        /// 队列元素交换位置
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        private void SwapElement(int i, int j)
        {
            point theTmp = _minHeap[i];
            _minHeap[i] = _minHeap[j];
            _minHeap[j] = theTmp;
        }
        /// <summary>
        /// 取节点的左孩子节点
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private int HeapL(int i)
        {
            return i * 2 + 1;
        }
        /// <summary>
        /// 取节点的右孩子节点
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private int HeapR(int i)
        {
            return i * 2 + 2;
        }
        /// <summary>
        /// 取节点的父节点
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private int HeapP(int i)
        {
            return (i + 1) / 2 - 1;
        }
    }

    //节点类
    public class point
    {
        public int x;
        public int z;
        public int G;
        public int H;
        public point father;
        public point() { }

        public point(int xa, int yb)
        {
            x = xa;
            z = yb;
        }

        public point(int xa, int yb, int g, int h, point p)
        {
            x = xa;
            z = yb;
            G = g;
            H = h;
            father = p;
        }

        public int F
        {
            get { return G + H; }
        }

        public static List<point> roundPointOffsets = new List<point>() 
        {
            new point(0, 1),
            new point(0, -1),
            new point(1, 0),
            new point(1, 1),
            new point(1, -1),
            new point(-1, 0),
            new point(-1, 1),
            new point(-1, -1),
        };
    }

    public class PointPool
    {
        private static List<point> pool = new List<point>();
        private static int curNum = 0;
        private static int addNum = 100;
        private static int MaxNum = 100;

        public static void Init()
        {
            for (int i = 0; i < MaxNum; i++)
            {
                pool.Add(new point());
            }
        }

        public static point GetPoint()
        {
            if (pool.Count == 0)
            {
                PointPool.Init();
            }
            if (curNum == MaxNum - 1)
            {
                for (int i = 0; i < addNum; i++)
                {
                    pool.Add(new point());
                }
                MaxNum += addNum;
            }
            return pool[curNum++];
        }

        public static void Clear()
        {
            curNum = 0;
        }
    }

    public class BlockMap
    {
        public const byte BLOCK_TYPE_NORMAL = 0;    //可行走区域
        public const byte BLOCK_TYPE_BLOCK = 1;     //障碍区域
        public const byte BLOCK_TYPE_WALL = 2;      //墙
        public const byte BLOCK_TYPE_SIDE = 3;      //补边区域
        public const byte BLOCK_TYPE_FLY = 4;       //飞行区域
        public const byte BLOCK_TYPE_WALL_FLY = 5;  //飞行墙
        //开启列表
        private List<point> _openlist = new List<point>();
        //结束列表
        private bool[,] _closeArray;
        //用来快速判断是否在开启列表中和快速通过下标取值
        private point[,] _openArrayValue;
        //获取最小F值point
        //private MinPointHeap _openHeap = new MinPointHeap();


        byte[,] _mapData;
        int _xMax;
        int _zMax;
        int _origin_x;
        int _origin_z;

        bool _bFly = false;     //是否飞行(飞行则飞行区域也算可行走区域)

        //读取地图文件
        public void Read(string mapInfoPath)
        {
            using (FileStream f = File.OpenRead(mapInfoPath))
            {

                //维数占1个字节
                //m = (int)SR.Read();
                //n = (int)SR.Read();

                byte[] b = new byte[2];
                f.Read(b, 0, 2);
                _xMax = (int)(b[0] + b[1]);
                f.Read(b, 0, 2);
                _zMax = (int)(b[0] + b[1]);

                _mapData = new byte[_xMax, _zMax];
                for (int i = 0; i < _xMax; ++i)
                for (int j = 0; j < _zMax; ++j)
                {
                    _mapData[i, j] = (byte)f.ReadByte();
                }
            }
        }

        public BlockMap(byte[] data)
        {
            Init(data);
        }

        public void Init(byte[] data)
        {
            uint cursor = 0;
            int xl = data[cursor++];
            int xh = data[cursor++];
            int zl = data[cursor++];
            int zh = data[cursor++];
            
            _xMax = xl + (xh << 8);
            _zMax = zl + (zh << 8);
            _mapData = new byte[_xMax, _zMax];

            xl = data[cursor++];
            xh = data[cursor++];
            zl = data[cursor++];
            zh = data[cursor++];
            _origin_x = xl + (xh << 8);
            _origin_z = zl + (zh << 8);

            InitCloseArray(_xMax, _zMax);
            InitOpenArray(_xMax, _zMax);

            for (int i = 0; i < _xMax; ++i)
            {
                for (int j = 0; j < _zMax; ++j)
                {
                    _mapData[i, j] = data[cursor++];
                }
            }
        }

        public byte[,] GetBlockMapData()
        {
            return _mapData;
        }

        private point s = new point();
        private point d = new point();
        public List<int> FindPath(int x1, int y1, int x2, int y2, bool bFly = false)
        {
            //转换相对坐标
            x1 -= _origin_x;
            y1 -= _origin_z;
            x2 -= _origin_x;
            y2 -= _origin_z;

            _bFly = bFly;
            s.x = x1;
            s.z = y1;
            FixPointToCanMove(s);
            d.x = x2;
            d.z = y2;
            FixPointToCanMove(d);
            var result = FindPath(s, d);
            return result;
        }

        public Vector3 CheckPointAroundCanMove(float posx, float posz, bool bFly = false, bool largeRangeSearch = false)
        {
            int searchRange = largeRangeSearch ? 30 : 10;
            _bFly = bFly;
            int xpos = Mathf.RoundToInt(posx);
            int zpos = Mathf.RoundToInt(posz);
            for (int i = 1; i <= searchRange; i++)
            {
                for (int j = 0; j < point.roundPointOffsets.Count; j++)
                {
                    var offset = point.roundPointOffsets[j];
                    var newX = xpos + offset.x * i;
                    var newZ = zpos + offset.z * i;
                    if (IsWay(newX, newZ))
                    {
                        return new Vector3(newX, 0f, newZ);
                    }
                }
            }
            return Vector3.zero;
        }

        public bool IsWay(float posx, float posz, bool bFly = false)
        {
            _bFly = bFly;
            int xpos = Mathf.RoundToInt(posx);
            int zpos = Mathf.RoundToInt(posz);
            //转换相对坐标
            xpos -= _origin_x;
            zpos -= _origin_z;
            return IsWay(xpos, zpos);
        }

        public bool OutOfRange(int x, int z)
        {
            return x < 0 || x >= _xMax || z < 0 || z >= _zMax;
        }

        //计算（更新）节点G值
        private int GetGValue(point p)
        {
            if (p.father == null) return 0;
            if (p.x == p.father.x || p.z == p.father.z) return p.father.G + 10;
            else return p.father.G + 14;
        }

        //计算节点H值
        private int GetHValue(point p, point pb)
        {
            return (ACTSystem.ACTMathUtils.Abs(p.x - pb.x) + ACTSystem.ACTMathUtils.Abs(p.z - pb.z))*10;
        }       

        private void InitOpenArray(int _xmax, int _zmax)
        {
            _openArrayValue = new point[_xmax, _zmax];
            for (int i = 0; i < _xmax; i++)
            {
                for (int j = 0; j < _zmax; j++)
                {
                    _openArrayValue[i,j] = null;
                }
            }
        }

        private void ClearOpenArray()
        {
            for (int i = 0; i < _xMax; i++)
            {
                for (int j = 0; j < _zMax; j++)
                {
                    _openArrayValue[i, j] = null;
                }
            }
        }

        private void InitCloseArray(int _xmax,int _zmax)
        {
            _closeArray = new bool[_xmax, _zmax];
            for (int i = 0; i < _xmax; i++)
            {
                for (int j = 0; j < _zmax; j++)
                {
                    _closeArray[i, j] = false;
                }
            }
        }

        private void ClearCloseArray()
        {
            for (int i = 0; i < _xMax; i++)
            {
                for (int j = 0; j < _zMax; j++)
                {
                    _closeArray[i, j] = false;
                }
            }
        }

        //开启列表成员判断
        private bool IsOpenListMember(int x, int y)
        {
            if (_openArrayValue[x, y] != null)
            {
                return true;
            }
            return false;
        }

        //关闭列表成员判断
        private bool IsCloseListMember(int x, int y)
        {
            return _closeArray[x, y];
        }

        private void ClearAll()
        {
            _openlist.Clear();
            PointPool.Clear();
            ClearCloseArray();
            ClearOpenArray();
            //_openHeap.Clear();
        }

        private void AddOpenList(point p)
        {
            _openlist.Add(p);
            //_openHeap.HeapInsert(p);
            _openArrayValue[p.x, p.z] = p;
        }
        //开启列表中F值最小的点
        private point GetMinTarget()
        {
            point pmin = null;
            for (int i = 0; i < _openlist.Count; i++)
            {
                var p = _openlist[i];
                if (pmin == null || (pmin.F) > (p.F))
                    pmin = p;
            }
            //pmin = _openHeap.ExtractMin();         
            if (pmin != null)
            {
                _openlist.Remove(pmin);
                _openArrayValue[pmin.x, pmin.z] = null;
                _closeArray[pmin.x, pmin.z] = true;
            }
            return pmin;
        }

        //获得开启列表中的节点对象
        private point GetPointInOpen(int x, int y)
        {
            return _openArrayValue[x, y];
        }

        //九宫格判断
        private void Check(point p0, point pa, ref point pb)
        {
            for (int xt = p0.x - 1; xt <= p0.x + 1; ++xt) //九宫格搜寻
            {
                for (int yt = p0.z - 1; yt <= p0.z + 1; ++yt)
                {
                    if (!(xt == p0.x && yt == p0.z))//排除本身中心点
                    {
                        if (IsWay(xt, yt) && !IsCloseListMember(xt, yt))//排除范围外的冗余点、障碍点和关闭列表中的点
                        {
                            if (IsOpenListMember(xt, yt))//对已经存在于开启列表中的点进行考察重新计算G值和确定父节点
                            {
                                point pt = GetPointInOpen(xt, yt);
                                int gnew = 0;
                                if (p0.x == pt.x || p0.z == pt.z) //与中心点在同行或同列则G以10为单位增大
                                {
                                    gnew = p0.G + 10;
                                }
                                else { gnew = p0.G + 14; }
                                if (gnew < pt.G)//更新G值与父节点
                                {
                                    pt.father = p0;
                                    pt.G = gnew;
                                }
                            }
                            else //不在开启列表中
                            {
                                point pt = PointPool.GetPoint();
                                pt.x = xt;
                                pt.z = yt;
                                pt.father = p0;
                                pt.G = GetGValue(pt);
                                pt.H = GetHValue(pt, pb);
                                AddOpenList(pt);
                            }
                        }
                    }
                }
            }
        }

        private List<int> result = new List<int>();
        //寻找pa到目的pb的路线
        private List<int> FindPath(point ps, point pd)
        {
            if (!IsWay(ps.x, ps.z) || !IsWay(pd.x, pd.z)) return null;
            if (IsAroundByBlock(ps.x, ps.z) || IsAroundByBlock(pd.x, pd.z)) return null;
            ClearAll();
            AddOpenList(ps);

            while (!IsOpenListMember(pd.x, pd.z) || _openlist.Count == 0)
            {
                point p = GetMinTarget();
                if (p == null) 
                {
                    return null; 
                }
                Check(p, ps, ref pd);
            }
            result.Clear();
            point pp = GetPointInOpen(pd.x, pd.z);
            result.Insert(0, pd.z);
            result.Insert(0, pd.x);
            while (pp.father != null)
            {
                pp = pp.father;
                result.Insert(0, pp.z);
                result.Insert(0, pp.x);
            }
            //转换相对坐标
            for (int i = 0; i < result.Count; i++)
            {
                if (i % 2 == 0)
                {
                    result[i] += _origin_x;
                }
                else
                {
                    result[i] += _origin_z;
                }
            }

            return result;
        }

        void DrawPoints(List<point> pointList, Color color)
        {
            //Debug.LogError("DrawPoints::" + pointList.Count);
            var h = 20;
            Vector3 start = new Vector3(0, h, 0);
            Vector3 end = new Vector3(0, h, 0);
            for (int i = 0; i < pointList.Count; i++)
            {
                var x = pointList[i].x;
                var z = pointList[i].z;
                start.x = x;
                start.z = z;
                end.x = x + 0.5f;
                end.z = z;
                Debug.DrawLine(start, end, color, 10);
                end.x = x - 0.5f;
                end.z = z;
                Debug.DrawLine(start, end, color, 10);
                end.x = x;
                end.z = z + 0.5f;
                Debug.DrawLine(start, end, color, 10);
                end.x = x;
                end.z = z - 0.5f;
                Debug.DrawLine(start, end, color, 10);
            }
        }

        void FixPointToCanMove(point fxPoint, int stack = 0)
        {
            if (stack > 3) return;
            if (!IsWay(fxPoint.x, fxPoint.z) || IsAroundByBlock(fxPoint.x, fxPoint.z))
            {
                Vector3 pos1 = CheckPointAroundCanMove(fxPoint.x, fxPoint.z, _bFly);
                fxPoint.x = (int)pos1.x;
                fxPoint.z = (int)pos1.z;
                FixPointToCanMove(fxPoint, ++stack);
            }
        }

        void CheckStartPoint(int x1, int y1, int index)
        {
            bool isFindFlag = false;
            if (!isFindFlag && _mapData[x1 + index + 1, y1] == 0)
            {
                _mapData[x1 + index, y1] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 + index + 1, y1 + index + 1] == 0)
            {
                _mapData[x1 + index, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1, y1 + index + 1] == 0)
            {
                _mapData[x1, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1] == 0)
            {
                _mapData[x1 - index, y1] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1, y1 - index - 1] == 0)
            {
                _mapData[x1, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1 - index - 1] == 0)
            {
                _mapData[x1 - index, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 + index + 1, y1 - index - 1] == 0)
            {
                _mapData[x1 + index, y1 - index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && _mapData[x1 - index - 1, y1 + index + 1] == 0)
            {
                _mapData[x1 - index, y1 + index] = 0;
                isFindFlag = true;
            }
            if (!isFindFlag && index != 2)
            {
                CheckStartPoint(x1, y1, 2);
            }
        }

        private bool IsWay(int x, int z)
        { 
            if (OutOfRange(x, z)) return false;
            if (_bFly)
            {
                return _mapData[x, z] != BLOCK_TYPE_BLOCK && _mapData[x, z] != BLOCK_TYPE_WALL_FLY;
            }
            return _mapData[x, z] == BLOCK_TYPE_NORMAL;
        }

        private bool IsAroundByBlock(int x, int z)
        {
            var blockSum = 0;
            for (int j = 0; j < point.roundPointOffsets.Count; j++)
            {
                var offset = point.roundPointOffsets[j];
                var newX = x + offset.x;
                var newZ = z + offset.z;
                if (!IsWay(newX, newZ))
                {
                    blockSum++;
                }
            }
            var result = blockSum >= 7;
            return result;
        }
    }
}
