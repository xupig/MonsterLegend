using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;


namespace GameMain
{
    public class PlayerSyncPosManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerSyncPosManager
    {

        private static PlayerSyncPosManager s_instance = null;
        public static PlayerSyncPosManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerSyncPosManager();
            }
            return s_instance;
        }

        //private uint _syncPosTimerID;
        private int _syncPosSpaceFrame = 6; //属性同步间隔时间（单位：帧）。
        private float _syncPosNextTime = 0;

        private uint _recoverAttrTimerID;
        //private int _recoverAttrSpaceTime = 100; //属性恢复间隔时间（单位：毫秒）。//为排除警告，注释掉
        private Vector3 _prePos = new Vector3();
        private uint _lastLevel = 0;
        private bool _syncPosState = false;

        private PlayerSyncPosManager()
        {

        }

        public void OnPlayerEnterWorld()
        {
            _lastLevel = EntityPlayer.Player.level;
            MogoWorld.RegisterUpdate<PlayerSyncPosManagerUpdateDelegate>("PlayerSyncPosManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PlayerSyncPosManager.Update", Update);
            TimerHeap.DelTimer(_recoverAttrTimerID);
        }

        public void SetSyncPosSpaceFrame(int frame)
        {
            _syncPosSpaceFrame = frame;
        }

        public void SetSyncPosState(bool state)
        {
            _syncPosState = state;
        }

        public void Update()
        {
            _syncPosNextTime--;
            if (_syncPosNextTime <= 0)
            {
                _syncPosNextTime = _syncPosSpaceFrame;
                SyncPos();
            }
        }

        public void SyncPos()
        {
            if (!_syncPosState)//用于停个人坐标同步
                return;
            if (EntityPlayer.pauseSyncPos)//用于停所有坐标同步
                return;
            EntityPlayer player = EntityPlayer.Player;
            if (player == null || player.actor == null)
            {
                return;
            }
            if (NeedPauseSync())//因移动状态停止坐标同步
            {
                return;
            }
            Vector3 position = player.actor.position;
            if (ACTSystem.ACTMathUtils.Abs(position.x - _prePos.x) < 0.2f && ACTSystem.ACTMathUtils.Abs(position.z - _prePos.z) < 0.2f)
            {
                return;
            }
            _prePos.x = position.x;
            _prePos.y = position.y;
            _prePos.z = position.z;
            System.Int32 x = (System.Int32)(position.x * RPCConfig.POSITION_SCALE);
            System.Int32 y = (System.Int32)(position.y * RPCConfig.POSITION_SCALE);
            System.Int32 z = (System.Int32)(position.z * RPCConfig.POSITION_SCALE);
            Vector3 localEulerAngles = player.actor.localEulerAngles;
            byte eulerAnglesX = (byte)(localEulerAngles.x * 0.5f + 0.5f);
            byte eulerAnglesY = (byte)(localEulerAngles.y * 0.5f + 0.5f);
            byte eulerAnglesZ = (byte)(localEulerAngles.z * 0.5f + 0.5f);

            MogoEngine.RPC.ServerProxy.Instance.Move(eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z);
        }

        private bool NeedPauseSync()
        {
            if (EntityPlayer.Player.moveManager._accordingMode == CombatSystem.AccordingMode.AccordingPathFly)
            {
                return true;
            }
            return false;
        }
    }
}