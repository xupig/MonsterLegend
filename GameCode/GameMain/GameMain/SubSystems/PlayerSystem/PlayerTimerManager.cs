﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 17:50:33
 * 描述说明：
 * *******************************************************/

using Common.Data;
using Common.Events;
using Common.Global;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Network;
using GameLoader.Utils.Timer;

using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class PlayerTimerManager
    {
        private static PlayerTimerManager s_instance = null;
        public static PlayerTimerManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerTimerManager();
            }
            return s_instance;
        }

        private PlayerTimerManager()
        {

        }

        private uint _timeId;
        private uint _syncServerTimeReqTimerId;  //心跳定时器ID
        private Action _syncServerTimeCallback;
        private bool _checkLocalRate = false;
        private ulong _maxLocalTimeOffset = 0;
        private DateTime _dateTime;

        public void OnPlayerEnterWorld()
        {
            StartSyncServerTime();                                                                     //请求进入游戏成功，开启心跳
            enableNetTimeoutCheck(true);                                                          //开启网络断线检查
            Global.localTimeZone = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            EventDispatcher.AddEventListener<string>(RPCEvents.NetStatus, onNetStatusHandler);    //ServerProxy.cs抛出   
        }

        public void onNetStatusHandler(string status)
        {
            /*
            //PlayerTimerManager.GetInstance().OnPlayerLeaveWorld();
            LoggerHelper.Info("[PlayerTimerManager] 网络状态:" + status);
            if (status == TCPClientWorker.LINK_FAIL)
            {
          //ari SystemAlert.Show(true, "网络失败", "无法连接服务器！", true);
            }
            else if (status == TCPClientWorker.LINKING)
            {
          //ari SystemAlert.Show(true, "网络重连", "服务器重连中...", true);
            }
            else SystemAlert.CloseMessageBox();
             * */
        }

        public void OnPlayerLeaveWorld()
        {
            StopSyncServerTime();
            StopSimulateServerTime();
            EventDispatcher.RemoveEventListener<string>(RPCEvents.NetStatus, onNetStatusHandler);
        }

        /// <summary>
        /// 开启网络断线检查
        /// </summary>
        /// <param name="isEnable"></param>
        public void enableNetTimeoutCheck(bool isEnable)
        {
            int timeout = int.Parse(XMLManager.global_params[80].__value);
            TCPClientWorker.CLOSE_TIMEOUT = timeout;                          //网络断线超时（毫秒）60 * 1000;
            ServerProxy.Instance.enableTimeout(isEnable);                         //开启断线检查
            LoggerHelper.Info(string.Format("[网络超时设置] closeTimeout:{0}ms,检查开启状态:{1}", timeout, isEnable));
        }

        /// <summary>
        /// 开启心跳
        /// </summary>
        public void StartSyncServerTime()
        {
            //LoggerHelper.Error("StartSyncServerTime: " + _syncServerTimeReqTimerId);
            if (_syncServerTimeReqTimerId < 1)
            {
                _syncServerTimeReqTimerId = TimerHeap.AddTimer(60000, 60000, SyncServerTimeReq);
            }
        }

        /// <summary>
        /// 关闭心跳
        /// </summary>
        private void StopSyncServerTime()
        {
            if (_syncServerTimeReqTimerId != 0)
            {
                TimerHeap.DelTimer(_syncServerTimeReqTimerId);
                _syncServerTimeReqTimerId = 0;
            }
        }

        private float _lastTimeSyncReq = 0;
        //发送心跳包
        public void SyncServerTimeReq()
        {
            //LoggerHelper.Error("SyncServerTimeReq: " + (UnityPropUtils.realtimeSinceStartup - _lastTimeSyncReq) + " Connected: " + ServerProxy.Instance.Connected);
            if ((UnityPropUtils.realtimeSinceStartup - _lastTimeSyncReq) < 2) return;
            _lastTimeSyncReq = UnityPropUtils.realtimeSinceStartup;
            if (ServerProxy.Instance.Connected)
            {
                if (GMState.showheartbeatinfo)
                {
                    LoggerHelper.Info(string.Format("sync_time_req, timeStamp = {0}.", Global.serverTimeStamp));
                }
                MogoWorld.Player.RpcCall("sync_time_req", Global.serverTimeStamp);
                //LoggerHelper.Error("sync_time_req: " + Global.serverTimeStamp);
            }
        }

        /// <summary>
        /// 启动模拟服务端时间戳
        /// </summary>
        private void StartSimulateServerTime()
        {
            if (_timeId == 0)
            {
                _timeId = TimerHeap.AddTimer(0, 1000, SimulateServerTimeHandler);
            }
        }

        private void StopSimulateServerTime()
        {
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
        }

        public int GetCurrentSecond()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            _dateTime = GameTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
            return _dateTime.Hour * 3600 + _dateTime.Minute * 60 + _dateTime.Second;
        }

        private void SimulateServerTimeHandler()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            if (timeStamp % 15 == 0)
            {
                _dateTime = GameTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
                Global.currentSecond = _dateTime.Hour * 3600 + _dateTime.Minute * 60 + _dateTime.Second;
            }
        }


        public DateTime GetNowServerDateTime()
        {
            long timeStamp = (long)(Common.Global.Global.serverTimeStamp * 0.001);
            return GameTimeUtil.ServerTimeStamp2UtcTime(timeStamp);
        }

        public int GetNowServerMillisecond()
        {
            return (int)(Common.Global.Global.serverTimeStamp % 1000);
        }

        //private bool _isCheckTime = true;

        public void SyncTimeResp(UInt64 time)
        {
            if (CanCheckLocalRate())
            {
                CheckLocalRate(time);
            }
            Global.serverTimeStamp = time;
            StartSimulateServerTime();
            if (_syncServerTimeCallback != null)
            {
                _syncServerTimeCallback();
                _syncServerTimeCallback = null;
            }
            //if(_isCheckTime)
            //{
            //    _isCheckTime = false;
            //    SyncServerTimeReq();
            //}
        }

        private ulong _startServerTimeStamp = 0;
        private float _startClientTimeStamp = 0;
        private bool _hasShowedAlert = false;
        private bool _firstSync = true;
        private bool CheckLocalRate(ulong serverTime)
        {
            if (_startServerTimeStamp == 0)
            {
                if (_firstSync)
                {
                    _firstSync = false;
                }
                else
                {
                    _startServerTimeStamp = serverTime;
                    _startClientTimeStamp = UnityPropUtils.realtimeSinceStartup;
                }
                return true;
            }
            ulong clientDuration = (ulong)((UnityPropUtils.realtimeSinceStartup - _startClientTimeStamp) * 1000);
            ulong serverDuration = serverTime - _startServerTimeStamp;
            if (clientDuration > serverDuration)
            {
                ulong offset = clientDuration - serverDuration;
                if (offset >= GetMaxLocalTimeOffset())
                {
                    if (_hasShowedAlert == false)
                    {
                        _hasShowedAlert = true;
                    }
                    LoggerHelper.Error(string.Format(string.Format("CheckLocalRate, clientDuration = {0}, serverDuration = {1}, offset = {2}.", clientDuration, serverDuration, offset)));
                    return false;
                }
                return false;
            }
            return true;
        }

        public ulong GetLocalTimeStamp()
        {
            if (_startServerTimeStamp == 0) return 0;
            return _startServerTimeStamp + (ulong)((UnityPropUtils.realtimeSinceStartup - _startClientTimeStamp) * 1000);
        }

        private bool CanCheckLocalRate()
        {
            if (_maxLocalTimeOffset == 0)
            {
                InitLocalTimeParams();
            }
            return _checkLocalRate;
        }

        private ulong GetMaxLocalTimeOffset()
        {
            if (_maxLocalTimeOffset == 0)
            {
                InitLocalTimeParams();
            }
            return _maxLocalTimeOffset;
        }

        private void InitLocalTimeParams()
        {
            string[] localTimeParams = global_params_helper.GetGlobalParam(164).Split(',');
            _checkLocalRate = Convert.ToBoolean(int.Parse(localTimeParams[0]));
            _maxLocalTimeOffset = ulong.Parse(localTimeParams[1]);
        }

        //刷新客户端时间戳
        public void RefreshLocalTimeStamp()
        {
            Global.RefreshLocalTimeStamp();
            SyncServerTimeReq();
        }

        public void SetSyncTimeCallback(Action syncTimeCallback)
        {
            _syncServerTimeCallback = syncTimeCallback;
        }
    }
}
