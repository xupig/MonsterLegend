using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using GameMain.GlobalManager;
using ArtTech;
using ACTSystem;
using GameMain.ClientConfig;


namespace GameMain
{
    public static class CameraModeSettingType
    {
        public static int CAMERA_MODE_3D = 3; 		// 3D模式视角
        public static int CAMERA_MODE_2D = 2; 		// 2.5D模式视角
    }

    public static class ClientQualityLevel
    {
        public const int LOW = 0;
        public const int MEDIUM = 1;
        public const int HIGH = 2;
    }

    public class PlayerSettingManager
    {
        private static PlayerSettingManager s_instance = null;
        public static PlayerSettingManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerSettingManager();
            }
            return s_instance;
        }

        static int _globalQualityLevel = 0;
        public static int globalQualityLevel
        {
            get { return _globalQualityLevel; }
        }

        static int _qualityDelta = 0;
        public static int qualityDelta
        {
            get { return _qualityDelta; }
        }

        static int _curQualityLevel = 0;
        public static int curQualityLevel
        {
            get { return _curQualityLevel; }
        }

        /// <summary>
        /// 最大同屏人数
        /// </summary>
        private static int _maxScreenPlayerCount = 20;
        public static int maxScreenPlayerCount
        {
            get { return _maxScreenPlayerCount; }
        }

        /// <summary>
        /// 最大同屏怪物
        /// </summary>
        private static int _maxScreenMonsterCount = 30;
        public static int maxScreenMonsterCount
        {
            get { return _maxScreenMonsterCount; }
        }

        private static bool _hideMonster = false;
        public static bool hideMonster
        {
            get { return _hideMonster; }
        }

        private static bool _hideNpc = false;
        public static bool hideNpc
        {
            get { return _hideNpc; }
        }

        private static bool _hidePlayer = false;
        public static bool hidePlayer
        {
            get { return _hidePlayer; }
        }

        private static bool _hidePet = false;
        public static bool hidePet
        {
            get { return _hidePet; }
        }

        private static bool _hideOtherPlayer = false;
        public static bool hideOtherPlayer
        {
            get { return _hideOtherPlayer; }
        }

        private static bool _hideOtherWing = false;
        public static bool hideOtherWing
        {
            get { return _hideOtherWing; }
        }

        private static bool _hideOtherPet = false;
        public static bool hideOtherPet
        {
            get { return _hideOtherPet; }
        }

        private static bool _hideOtherFabao = false;
        public static bool hideOtherFabao
        {
            get { return _hideOtherFabao; }
        }

        private static bool _hideOtherSkillFx = false;
        public static bool hideOtherSkillFx
        {
            get { return _hideOtherSkillFx; }
        }

        private static bool _disableFx = false;
        public static bool disableFx
        {
            get { return _disableFx; }
        }

        private static bool _hideOtherFlow = false;
        public static bool hideOtherFlow
        {
            get { return _hideOtherFlow; }
        }

        public void SetGameQuality(int value)
        {
            SetHideActorFx(value);
            switch (value)
            {
                case 0: //精细画质
                    _globalQualityLevel = ClientQualityLevel.HIGH;
                    break;
                case 1: //中等画质
                    _globalQualityLevel = ClientQualityLevel.MEDIUM;
                    break;
                case 2: //流畅画质
                default:
                    _globalQualityLevel = ClientQualityLevel.LOW;
                    break;
            }
            ResetCurQualityLevel();
            CameraManager.GetInstance().UseQualitySetting(true);
        }

        public void SetMusicVolume(float value)
        {
            AudioState.bgMusicVolume = value;
            MogoEngine.Events.EventDispatcher.TriggerEvent<float>(Common.Events.BgMusicEvent.CHANGE_MUSIC_VOLUME, value);
        }

        public void SetSoundVolume(float value)
        {
            AudioState.uiVolume = value;
            AudioState.battleVolume = value;
            ACTSystem.ACTSystemDriver.GetInstance().soundVolume = value;
        }

        private float tempMusicVolume;
        private float tempSoundVolume;

        public void TempCloseMusicAndSoundVolume()
        {
            tempMusicVolume = AudioState.bgMusicVolume;
            tempSoundVolume = AudioState.battleVolume;
            SetMusicVolume(0);
            SetSoundVolume(0);
        }

        public void ResetMusicAndSoundVolume()
        {
            SetMusicVolume(tempMusicVolume);
            SetSoundVolume(tempSoundVolume);
        }

        public void SetGameQualityDelta(int value)
        {
            _qualityDelta = value;
            ResetCurQualityLevel();
        }

        private void ResetCurQualityLevel()
        {
            _curQualityLevel = Mathf.Min(Mathf.Max(ClientQualityLevel.LOW, _globalQualityLevel + _qualityDelta), ClientQualityLevel.HIGH);
            switch (_curQualityLevel)
            {
                case ClientQualityLevel.HIGH: //精细画质
                    if (Application.isEditor || /*(GraphicsQuality.TestFPS > 40) &&*/ (SystemInfo.systemMemorySize > 1500))
                    {
                        GraphicsQuality.QualityLevel = GraphicsQuality.Quality.High;
                        PostEffectQuality.QualityLevel = PostEffectQuality.Quality.High;
                        WaterQuality.QualityLevel = WaterQuality.Quality.High;
                        ShaderQuality.QualityLevel = ShaderQuality.Quality.High;
                        ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.H;
                        SceneMgr.Instance.SetHideAddStaticPrefabs(false);
                    }
                    else
                    {
                        GraphicsQuality.QualityLevel = GraphicsQuality.Quality.Medium;
                        PostEffectQuality.QualityLevel = PostEffectQuality.Quality.Medium;
                        WaterQuality.QualityLevel = WaterQuality.Quality.Low;
                        ShaderQuality.QualityLevel = ShaderQuality.Quality.Medium;
                        ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.H;
                    }
                    GraphicsQuality.SetScreenResolution(1080);
                    Application.targetFrameRate = 30;
                    SceneMgr.Instance.SetHideAddStaticPrefabs(false);
                    CameraManager.GetInstance().SetRotationXScale(7f / 9);
                    break;
                case ClientQualityLevel.MEDIUM: //中等画质
                                                //SetShadowState(false);
                    GraphicsQuality.QualityLevel = GraphicsQuality.Quality.Medium;
                    PostEffectQuality.QualityLevel = PostEffectQuality.Quality.Medium;
                    WaterQuality.QualityLevel = WaterQuality.Quality.Low;
                    ShaderQuality.QualityLevel = ShaderQuality.Quality.Medium;
                    ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.H;
                    GraphicsQuality.SetScreenResolution(810);
                    Application.targetFrameRate = 30;
                    SceneMgr.Instance.SetHideAddStaticPrefabs(false);
                    CameraManager.GetInstance().SetRotationXScale(8f / 9);
                    break;
                case ClientQualityLevel.LOW: //流畅画质
                default:
                    GraphicsQuality.QualityLevel = GraphicsQuality.Quality.Low;
                    PostEffectQuality.QualityLevel = PostEffectQuality.Quality.Low;
                    WaterQuality.QualityLevel = WaterQuality.Quality.Low;
                    ShaderQuality.QualityLevel = ShaderQuality.Quality.Low;
                    ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.L;
                    GraphicsQuality.SetScreenResolution(720);
                    Application.targetFrameRate = 24;
                    SceneMgr.Instance.SetHideAddStaticPrefabs(true);
                    CameraManager.GetInstance().SetRotationXScale(1f);
                    break;
            }
        }

        void SetProjectionState(bool state)
        {
            if (state)
            {
                ProjectionManager.GetInstance().Run();
            }
            else
            {
                ProjectionManager.GetInstance().Stop();
            }
        }

        void SetShadowState(bool state)
        {
            if (state)
            {
                ShadowManager.GetInstance().OpenShadow();
            }
            else
            {
                ShadowManager.GetInstance().HideShadow();
            }
        }

        void RefreshOtherPlayerShow()
        {
            int count = 0;
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
                {
                    count++;
                    EntityCreature avatar = (entity as EntityCreature);
                    if (count > _maxScreenPlayerCount)
                    {
                        avatar.isHideEntity = true;
                    }
                    else
                    {
                        avatar.isHideEntity = false;
                    }
                }
            }

            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
                {
                    EntityCreature avatar = (entity as EntityCreature);
                    avatar.actorVisible = !avatar.isHideEntity;

                    Entity pet = MogoWorld.GetEntity((entity as EntityCreature).pet_eid);
                    if (pet != null)
                    {
                        (pet as EntityCreature).actorVisible = !(_hideOtherPlayer || _hideOtherPet || avatar.isHideEntity);

                    }

                    Entity fabao = MogoWorld.GetEntity((entity as EntityCreature).fabao_eid);
                    if (fabao != null)
                    {
                        (fabao as EntityCreature).actorVisible = !(_hideOtherPlayer || _hideOtherFabao || avatar.isHideEntity);
                    }
                }
            }
        }

        public void SetMaxScreenPlayerCount(int count)
        {
            _maxScreenPlayerCount = count;
            RefreshOtherPlayerShow();
        }

        public int GetMaxScreenPlayerCount()
        {
            return _maxScreenPlayerCount;
        }

        public void SetMaxScreenMonsterCount(int count)
        {
            _maxScreenMonsterCount = count;
        }

        public void SetRealTimeShadow(int mode)
        {
            if (mode == 1)
            {
                SetProjectionState(false);
                ShadowManager.QualityLevel = ShadowManager.Quality.High;
            }
            else
            {
                SetProjectionState(true);
                ShadowManager.QualityLevel = ShadowManager.Quality.Low;
            }
        }

        public void SetHideOtherPlayer(int mode)
        {
            if ((mode == 1) == _hideOtherPlayer)
            {
                return;
            }
            if (mode == 0)
            {
                _hideOtherPlayer = false;
            }
            else if (mode == 1)
            {
                _hideOtherPlayer = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !(entity as EntityCreature).isHideEntity)
                {
                    EntityCreature avatar = (entity as EntityCreature);
                    avatar.actorVisible = !_hideOtherPlayer;
                    if (avatar.rideManager != null) avatar.rideManager.RefreshRide();

                    Entity pet = MogoWorld.GetEntity((entity as EntityCreature).pet_eid);
                    if (pet != null)
                    {
                        (pet as EntityCreature).actorVisible = !(_hideOtherPlayer || _hideOtherPet);

                    }

                    Entity fabao = MogoWorld.GetEntity((entity as EntityCreature).fabao_eid);
                    if (fabao != null)
                    {
                        (fabao as EntityCreature).actorVisible = !(_hideOtherPlayer || _hideOtherFabao);
                    }
                }
            }
        }

        public void SetHideMonster(int mode)
        {
            if (mode == 0)
            {
                _hideMonster = false;
            }
            else if (mode == 1)
            {
                _hideMonster = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                {
                    (entity as EntityCreature).actorVisible = !_hideMonster;
                }
            }
        }

        public void SetHideOtherWing(int mode)
        {
            if (mode == 0)
            {
                _hideOtherWing = false;
            }
            else if (mode == 1)
            {
                _hideOtherWing = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !MogoWorld.IsClientEntity(entity))
                {
                    (entity as EntityCreature).SetCanWingShow(!_hideOtherWing);
                }
            }
        }

        public void SetHideOtherPet(int mode)
        {
            if (mode == 0)
            {
                _hideOtherPet = false;
            }
            else if (mode == 1)
            {
                _hideOtherPet = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !MogoWorld.IsClientEntity(entity))
                {
                    Entity pet = MogoWorld.GetEntity((entity as EntityCreature).pet_eid);
                    if (pet == null) continue;
                    (pet as EntityCreature).actorVisible = !_hideOtherPet;
                }
            }
        }

        public void SetHideOtherFabao(int mode)
        {
            if (mode == 0)
            {
                _hideOtherFabao = false;
            }
            else if (mode == 1)
            {
                _hideOtherFabao = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !MogoWorld.IsClientEntity(entity))
                {
                    Entity fabao = MogoWorld.GetEntity((entity as EntityCreature).fabao_eid);
                    if (fabao == null) continue;
                    (fabao as EntityCreature).actorVisible = !_hideOtherFabao;
                }
            }
        }

        public void SetHideOtherSkillFx(int mode)
        {
            if (mode == 0)
            {
                _hideOtherSkillFx = false;
            }
            else if (mode == 1)
            {
                _hideOtherSkillFx = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                EntityCreature entityEC;
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !MogoWorld.IsClientEntity(entity))
                {
                    entityEC = (entity as EntityCreature);
                    if (entityEC.actor != null)
                    {
                        entityEC.actor.isHideFX = _hideOtherSkillFx;
                    }
                }
                else
                {
                    continue;
                }

                Entity fabao = MogoWorld.GetEntity(entityEC.fabao_eid);
                if (fabao != null)
                {
                    EntityCreature fabaoEC = (fabao as EntityCreature);
                    if (fabaoEC.actor != null)
                    {
                        fabaoEC.actor.isHideFX = _hideOtherSkillFx;
                    }
                }

                Entity pet = MogoWorld.GetEntity(entityEC.pet_eid);
                if (pet != null)
                {
                    EntityCreature petEC = (pet as EntityCreature);
                    if (petEC.actor != null)
                    {
                        petEC.actor.isHideFX = _hideOtherSkillFx;
                    }
                }
            }
        }

        public void SetHideActorFx(int mode)
        {
            if (mode == 2)
            {
                _disableFx = true;
            }
            else
            {
                _disableFx = false;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                EntityCreature entityEC;
                if (entity != null)
                {
                    if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR
                        || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY
                        || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_PET || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO
                        || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                    {
                        entityEC = (entity as EntityCreature);
                        if (entityEC.actor != null)
                        {
                            entityEC.RefreshActorFxShow(!_disableFx);
                        }
                    }
                }
            }
        }

        public void SetHideOtherFlow(int mode)
        {
            if (mode == 0)
            {
                _hideOtherFlow = false;
            }
            else if (mode == 1)
            {
                _hideOtherFlow = true;
            }
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                EntityCreature entityEC;
                if (entity != null)
                {
                    if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
                    {
                        entityEC = (entity as EntityCreature);
                        if (entityEC.actor != null)
                        {
                            entityEC.RefreshEquipFlowShow(!_hideOtherFlow);
                        }
                    }
                }
            }
        }

        public void SetHideNpc(int mode)
        {
            _hideNpc = (mode == 1);
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                EntityCreature entityEC;
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_PERI)
                {
                    entityEC = (entity as EntityCreature);
                    if (entityEC.actor != null && !entityEC.IsDefaultModel())
                    {
                        entityEC.actorVisible = !_hideNpc;
                    }
                }
            }
        }


        public void SetHidePet(int mode)
        {
            _hidePet = (mode == 1);
            for (int i = 0; i < MogoWorld.EntityIDs.Count; i++)
            {
                uint id = MogoWorld.EntityIDs[i];
                Entity entity = MogoWorld.GetEntity(id);
                EntityCreature entityEC;
                if (entity != null && entity.entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
                {
                    entityEC = (entity as EntityCreature);
                    if (entityEC.actor != null)
                    {
                        entityEC.actorVisible = !_hidePet;
                    }
                }
            }
        }

        public void SetHidePlayer(int mode)
        {
            _hidePlayer = (mode == 1);
            EntityPlayer entity = EntityPlayer.Player;
            entity.SetHideSkin(_hidePlayer);
            entity.SetCanWingShow(!_hidePlayer);
            if (entity.rideManager != null) 
            {
                entity.rideManager.RefreshRide();
            }
            EntityCreature pet = MogoWorld.GetEntity(entity.pet_eid) as EntityCreature;
            if (pet != null && pet.actor != null)
            {
                (pet as EntityCreature).actorVisible = !_hidePlayer;
            }
            EntityCreature fabao = MogoWorld.GetEntity(entity.fabao_eid) as EntityCreature;
            if (fabao != null && fabao.actor != null)
            {
                (fabao as EntityCreature).actorVisible = !_hidePlayer;
            }
        }


        private float _battleDistanceLimit = 0f;
        public float battleDistanceLimit
        {
            get
            {
                if (_battleDistanceLimit == 0f)
                {
                    RefreshBattleDistanceLimit();
                }
                return _battleDistanceLimit;
            }
        }

        public void RefreshBattleDistanceLimit()
        {
            _battleDistanceLimit = global_params_helper.distance_limit;
        }
    }
}