﻿using System;
using System.Collections.Generic;
using GameResource;
using GameData;
using GameLoader.Utils;
using MogoEngine.Events;
using UnityEngine;
using Common.Global;
using Common.Events;
using Common.ExtendTools;
using MogoEngine;
using GameMain.ClientConfig;
using MogoEngine.RPC;
using GameMain;
using MogoEngine.Mgrs;


public class PlayerSkillPreviewManager
{
    public static string SKILL_PREVIEW_PREFAB = "Global/SkillPreview.prefab";
    private bool _isInited = false;
    public Vector3 initPos = new Vector3(-1000, 0, -1000);
    public Vector3 initCGPos = new Vector3(-2000, 0, -2000);
    private GameObject _objPool;

    private static PlayerSkillPreviewManager s_instance = null;
    public static PlayerSkillPreviewManager GetInstance()
    {
        if (s_instance == null)
        {
            s_instance = new PlayerSkillPreviewManager();
        }
        return s_instance;
    }

    private PlayerSkillPreviewManager()
    {

    }

    public void InitGameObject()
    {
        if (_isInited) return;
        _isInited = true;
        _objPool = new GameObject("SkillPreviewPool");
        UnityEngine.Object.DontDestroyOnLoad(_objPool);
        if (GameObject.Find("SkillPreview") == null)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(SKILL_PREVIEW_PREFAB, (obj) =>
            {
                if (obj == null)
                {
                    LoggerHelper.Error("CreateActor modelName:" + SKILL_PREVIEW_PREFAB + " is null");
                    return;
                }
                UnityEngine.Object.DontDestroyOnLoad(obj);
                obj.transform.SetParent(_objPool.transform);
                obj.transform.localScale = new Vector3(10, 1, 10);
                obj.transform.position = initPos;
            });
        }
    }

    public void OnPlayerEnterWorld()
    {
        InitGameObject();
        EntityPlayer.ShowPlayer = EntityMgr.Instance.CreateClientEntity(EntityConfig.ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR, delegate (Entity ent)
        {
            (ent as EntityCreature).SetAttr("vocation", EntityPlayer.Player.vocation);
            (ent as EntityCreature).vocation = EntityPlayer.Player.vocation;
        }) as EntityCreature;
    }

    public void OnPlayerLeaveWorld()
    {
    }

    public void LoadCG1v1()
    {
        // 1v1CG
        //if (GameObject.Find("CG1v1Go") == null)
        //{
        //no ResourceMappingManager
        GameResource.ObjectPool.Instance.GetGameObject(global_params_helper.GetGlobalParam(720), (obj) =>
        {
            if (obj == null)
            {
                return;
            }
            UnityEngine.Object.DontDestroyOnLoad(obj);
            obj.transform.SetParent(_objPool.transform);
                //obj.transform.position = initCGPos;
            });
        //}
    }

    public void SetTransform()
    {
        if (EntityPlayer.ShowPlayer != null && EntityPlayer.ShowPlayer.actor != null && _objPool != null)
        {
            EntityPlayer.ShowPlayer.actor.transform.SetParent(_objPool.transform, false);
        }
    }
}
