﻿using System;
using MogoEngine.Events;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using GameMain.CombatSystem;
using UnityEngine;
using MogoEngine.RPC;

namespace GameMain.GlobalManager
{
    public class PlayerBuffManager
    {
        private static PlayerBuffManager s_instance = null;
        public static PlayerBuffManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerBuffManager();
            }
            return s_instance;
        }

        private PlayerBuffManager()
        {

        }

        private bool CanDiscard(EntityCreature creature)
        {
            if (creature == null || creature.actor == null || creature.isHideEntity || creature.isHideMonster)
            {
                return true;
            }
            return false;
        }

        public void AddBuffResp(byte[] data)
        {
            PbAddBuff pbCdChangeSpells = GameProtoUtils.ParseProto<PbAddBuff>(data);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(pbCdChangeSpells.target_id);

            //删除buff不判断actor存在
            if (pbCdChangeSpells.is_add == 0)
            {
                if (creature == null || creature.isHideEntity || creature.isHideMonster)
                {
                    return;
                }
            }
            else 
            {
                if (CanDiscard(creature)) return;
            }

            int buffId = (int)pbCdChangeSpells.buff_id;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit &&
                !CombatLogicObjectPool.GetBufferData(buffId).needRecord) {
                    return;
            } //距离远的情况下不处理对应的包
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "AddBuffResp buffid=" + buffId + ",isAdd=" + pbCdChangeSpells.is_add);
            }
            if (pbCdChangeSpells.is_add == 1)
            {
                creature.bufferManager.AddBuffer((int)pbCdChangeSpells.buff_id, (float)(pbCdChangeSpells.remain_tick == 0 ? -1 : pbCdChangeSpells.remain_tick), false, false);
            }
            else
            {
                creature.bufferManager.RemoveBuffer((int)pbCdChangeSpells.buff_id);
            }
        }

        public void BuffHpChangeResp(byte[] data)
        {
            PbBuffHpChange pbBuffHpChange = GameProtoUtils.ParseProto<PbBuffHpChange>(data);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(pbBuffHpChange.target_id);
            if (CanDiscard(creature)) return;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit) return; //距离远的情况下不处理对应的包
            bool isPlayer = creature.IsPlayer();
            if (pbBuffHpChange.hp_change > 0)
            {
                AttackType attackType = AttackType.TREAT;
                PerformaceManager.GetInstance().CreateDamageNumber(creature.actor.position, pbBuffHpChange.hp_change, (int)attackType, isPlayer);
            }
            else
            {
                AttackType attackType = AttackType.ATTACK_HIT;
                PerformaceManager.GetInstance().CreateDamageNumber(creature.actor.position, -pbBuffHpChange.hp_change, (int)attackType, isPlayer);
            }
            if (pbBuffHpChange.userdata == 1)
            {
                //伤害吸收处理
                AttackType attackType = AttackType.ABSORB;
                PerformaceManager.GetInstance().CreateDamageNumber(creature.actor.position, pbBuffHpChange.userdata, (int)attackType, isPlayer);
            }
            if (pbBuffHpChange.userdata == 2)
            {
                //不屈处理
                AttackType attackType = AttackType.UNYIELDING;
                PerformaceManager.GetInstance().CreateDamageNumber(creature.actor.position, pbBuffHpChange.userdata, (int)attackType, isPlayer);
            }
        }

    }
}
