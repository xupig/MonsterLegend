﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain;
using Common.ClientConfig;
using GameResource;
using System.IO;
using GameLoader;
using GameLoader.Utils;
using MogoEngine;
using GameData;
using System.Collections;
using GameLoader.IO;
//using UnityEngine.Video;

namespace GameMain
{
    public class VideoManager
    {
        private const string FILE_HEAD = "file://";
        public const string VIDEO_PREFAB = "Global/VideoPlayer/Prefab/Video.prefab";
        private static VideoManager instance = null;

        public static VideoManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new VideoManager();
                return instance;
            }
        }

        private GameObject _videoMainGo;
        private GameObject _videoManagerGo;
        private GameObject _videoUIGo;
        //private VideoPlayer _mediaPlayerCtrl;
        private bool _isPlaying = false;
        private bool _isLoading = false;
        private bool _isLoop = false;
        private Action _endCallback = null;
        VideoManager()
        {
        }

        public void Init(Action callback)
        {
            //GameResource.ObjectPool.Instance.GetGameObject(VIDEO_PREFAB, (go) =>
            //{
            //    InitGameObjects(go);
            //    if (UnityPropUtils.Platform == RuntimePlatform.Android || UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
            //    {//真机环境
            //        string resoucesPath = IOUtils.GetStreamPath("Resources/");
            //        string outputPath = SystemConfig.ResourcesPath;
            //        LoaderDriver.Instance.StartCoroutine(ExportFile(resoucesPath, outputPath, callback));
            //    }
            //    else {
                    callback();
            //    }
            //});
        }

        private void InitGameObjects(GameObject go)
        {
            _videoMainGo = go;
            GameObject.DontDestroyOnLoad(_videoMainGo);

            _videoUIGo = _videoMainGo.transform.Find("Canvas").gameObject;
            var buttonGo = _videoUIGo.transform.Find("Button").gameObject;
            var button = buttonGo.GetComponent<UIExtension.ButtonWrapper>();
            button.onClick.AddListener(OnPass);

            _videoManagerGo = _videoMainGo.transform.Find("VideoManager").gameObject;
            var audioSource = _videoManagerGo.GetComponent<AudioSource>();
            //_mediaPlayerCtrl = _videoManagerGo.GetComponent<VideoPlayer>();
            //_mediaPlayerCtrl.waitForFirstFrame = false;
            //_mediaPlayerCtrl.controlledAudioTrackCount = 1;
            //_mediaPlayerCtrl.SetTargetAudioSource(0, audioSource);
            //_mediaPlayerCtrl.source = VideoSource.Url;
            //_mediaPlayerCtrl.playOnAwake = false;
            //_mediaPlayerCtrl.isLooping = false;
            //_mediaPlayerCtrl.waitForFirstFrame = true;
            //_mediaPlayerCtrl.prepareCompleted += OnReady;
            //_mediaPlayerCtrl.errorReceived += OnError;
            //_mediaPlayerCtrl.started += OnStarted;
            //_mediaPlayerCtrl.loopPointReached += OnLoopPointReached;
            //_mediaPlayerCtrl.seekCompleted += OnSeekCompleted;
            //_mediaPlayerCtrl.frameDropped += OnFrameDropped;
            //_mediaPlayerCtrl.frameReady += OnFrameReady;
            _videoMainGo.SetActive(_isPlaying);
        }

        private IEnumerator ExportFile(string resoucesPath, string outputPath, Action callback)
        {
            var allPath = video_helper.GetAllPath();
            while (allPath.Count > 0)
            {
                string physicalPath = video_helper.TransformPathToPhysicalPath(allPath[0].ToLower());
                allPath.RemoveAt(0);
                string streamPath = string.Concat(resoucesPath, physicalPath);
                string targetPath = string.Concat(outputPath, physicalPath);
                if (!File.Exists(targetPath))
                {
                    WWW www = new WWW(streamPath);
                    yield return www;
                    if (www != null && string.IsNullOrEmpty(www.error))
                    {//下载成功
                        string tempFile = targetPath + "_temp";
                        File.WriteAllBytes(tempFile, www.bytes);
                        File.Move(tempFile, targetPath);
                    }
                    else {
                        Debug.LogError(www.error);
                    }
                    if (www.assetBundle) www.assetBundle.Unload(true);
                    www.Dispose();
                    www = null;
                }
            }
            callback();
        }

        public void Play(int id, bool loop = false, Action endCallback = null)
        {
            string path = video_helper.GetPath(id);
            Play(path, loop, endCallback);
        }

        public void Play(string path, bool loop = false, Action endCallback = null)
        {
            if (_isPlaying) Stop();
            _isLoop = loop;
            _endCallback = endCallback;
            SetPlaying(true);
            //_mediaPlayerCtrl.isLooping = _isLoop;
            Load(GetAssetUrl(path));
        }

        void Load(string path)
        {
            _isLoading = true;
            //_mediaPlayerCtrl.url = path;
            //_mediaPlayerCtrl.Prepare();
            return;
        }

        public void Stop()
        {
            //Debug.LogError("Stop");
            //_mediaPlayerCtrl.Stop();
            SetPlaying(false);
        }

        void SetPlaying(bool state)
        {
            if (state == _isPlaying) return;
            _isPlaying = state;
            _videoMainGo.SetActive(_isPlaying);
            if (!_isPlaying)
            {
                EndCallback();
                //AudioManager.GetInstance().ResetTempVolume();
            }
            else {
                //AudioManager.GetInstance().TempCloseVolume();
            }
        }

        //void OnError(VideoPlayer source, string message)
        //{
        //    Debug.LogError("[VideoPlayer OnError] " + message);
        //}

        //void OnStarted(VideoPlayer source)
        //{
        //    //Debug.LogError("OnStarted");
        //}

        //void OnLoopPointReached(VideoPlayer source)
        //{
        //    //Debug.LogError("OnLoopPointReached");
        //    if (!_isLoop)
        //    {
        //        OnEnd();
        //    }
        //}

        //void OnSeekCompleted(VideoPlayer source)
        //{
        //    //Debug.LogError("OnSeekCompleted");
        //}

        //void OnFrameDropped(VideoPlayer source)
        //{
        //    //Debug.LogError("OnFrameDropped");
        //}

        //void OnFrameReady(VideoPlayer source, long frameIdx)
        //{
        //    //Debug.LogError("OnFrameReady " + frameIdx);
        //}

        void OnPass()
        {
            Stop();
        }

        //void OnReady(VideoPlayer source)
        //{
        //    //Debug.LogError("OnReady");
        //    _isLoading = false;
        //    if (_isPlaying)
        //    {
        //        _mediaPlayerCtrl.Play();
        //    }
        //}

        void OnEnd()
        {
            //当前版本（653p2）不能在OnLoopPointReached同一帧处理关闭，否则闪退，只能延后处理
            GameLoader.Utils.Timer.TimerHeap.AddTimer(100, 0, () =>
            {
                //_mediaPlayerCtrl.Stop();
                SetPlaying(false);
            });
        }

        void EndCallback()
        {
            if (_endCallback != null)
            {
                _endCallback();
                _endCallback = null;
            }
        }

        private string GetAssetUrl(string path)
        {
            string physicalPath = path.Replace("\\", "/");
            physicalPath = path.Replace("/", "$");
            physicalPath = physicalPath.ToLower();
            physicalPath = SystemConfig.ResourcesPath + physicalPath;
            string mp4Path = physicalPath;
            physicalPath = physicalPath + ".u";

            if (File.Exists(physicalPath))
            {
                if (File.Exists(mp4Path)) File.Delete(mp4Path);
                File.Move(physicalPath, mp4Path);
            }
            physicalPath = mp4Path;

            return string.Concat(FILE_HEAD, physicalPath);//直接读外部资源
        }
    }

}