using System;
namespace GameMain.GlobalManager
{
    public class CameraAnimData 
	{
        public int priority { get; set; }

        public float xSwing { get; set; }
        public float ySwing { get; set; }
        public float zSwing { get; set; }

        public int xDir { get; set; }
        public int yDir { get; set; }
        public int zDir { get; set; }

        public float attenuateOddsX { get; set; }
        public float attenuateOddsY { get; set; }
        public float attenuateOddsZ { get; set; }

        public float xSpeed;
        public float ySpeed;
        public float zSpeed;

        public void UpdateSpeed(ref float xSpeed, ref float ySpeed, ref float zSpeed, float dt)
        {
            xSpeed = -Math.Sign(xSpeed) * xSwing;
            ySpeed = -Math.Sign(ySpeed) * ySwing;
            zSpeed = -Math.Sign(zSpeed) * zSwing;
        }
	}
}
