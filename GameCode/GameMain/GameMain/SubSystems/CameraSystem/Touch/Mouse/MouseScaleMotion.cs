﻿using UnityEngine;

namespace GameMain.GlobalManager
{
    public class MouseScaleMotion : TouchScaleMotion
    {
        private float _axis = 0;
        private float _lastAxis = 0;
        private float _distanceSpeed = 0;
        private float _scrollWheelAxis2Speed = 0;
        private float _scrollDuration = 0;
        private float _endTime = 0;
        public MouseScaleMotion(float distanceDefaultScale, float scrollWheelAxis2Speed, float scrollDuration, float minDistance, float minRotationX,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
            : base(distanceDefaultScale, minDistance, minRotationX,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange)
        {
            _scrollWheelAxis2Speed = scrollWheelAxis2Speed;
            _scrollDuration = scrollDuration;
        }

        protected override void OnStart()
        {
            _endTime = 0;
        }

        public override void OnUpdate()
        {
            _axis = Input.GetAxis("Mouse ScrollWheel");
            if (_axis != 0)
            {
                if (_status == TouchStatus.IDLE)
                {
                    if (CheckFullUI() && CheckMouseInRange())
                    {
                        Begin();
                    }
                }
                else
                {
                    Touching();
                }
            }
            else
            {
				if (_status == TouchStatus.TOUCHING)
                {
					if (RealTime.time >= _endTime)
					{
						End();
					}
					else
					{
						Touching();
					}
                }
            }
        }

        private bool CheckMouseInRange()
        {
            Vector3 position = Input.mousePosition;
            return position.x >= 0 && position.x <= ScreenUtil.ScreenWidth &&
                   position.y >= 0 && position.y <= ScreenUtil.ScreenHeight;
        }

        private void Begin()
        {
            _distance = _curDistance;
            _status = TouchStatus.TOUCHING;
            Touching();
        }

        private void Touching()
        {
            if (_axis != 0 && (Mathf.Sign(_axis) != Mathf.Sign(_lastAxis) || ACTSystem.ACTMathUtils.Abs(_axis) > ACTSystem.ACTMathUtils.Abs(_lastAxis)))
            {
                _lastAxis = _axis;
				_distanceSpeed = _axis * _scrollWheelAxis2Speed;
                _endTime = RealTime.time + _scrollDuration;
            }
            _distance -= _distanceSpeed;
            _distance = (_distance > _maxDistance) ? _maxDistance : _distance;
            _distance = (_distance < _minDistance) ? _minDistance : _distance;
            _distanceScale = (_distance - _minDistance) / (_maxDistance - _minDistance);
            _rotationX = GetRotationXByDistance(_distance);
        }

        private void End()
        {
            _status = TouchStatus.IDLE;
            _lastAxis = 0;
            _endTime = 0;
            _curDistance = _distance;
            WriteLocalCache();
        }

        public override void UpdateArgs(float touchDistance2Distance, float scrollDuration, float minDistance, float minRotationX)
        {
            _scrollWheelAxis2Speed = touchDistance2Distance;
            _scrollDuration = scrollDuration;
            base.UpdateArgs(touchDistance2Distance, scrollDuration, minDistance, minRotationX);
        }
    }
}
