﻿using Common.States;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TouchRotateMotion
    {
        protected TouchStatus _status = TouchStatus.IDLE;
        public TouchStatus status { get { return _status; } }
        protected float _touchDistanceToRotation;
        private float _touchLeftRange = 0f;
        private float _touchRightRange = 0f;
        private float _touchTopRange = 0f;
        private float _touchBottomRange = 0f;
        protected float _originalRotationY = 0f;
        protected float _originalRotationX = 0f;
        protected float _rotationYOffset = 0f;
        protected float _rotationY = 0f;
        protected float _rotationX = 0f;
        protected float _rotationXOffset = 0f;
        public float rotationY { get { return _rotationY; } }
        public float rotationX { get { return _rotationX; } }
        protected float _minRotationX = 0f;
        protected float _maxRotationX = 0f;

        public TouchRotateMotion(float touchDistanceToRotation,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            _touchDistanceToRotation = touchDistanceToRotation;
            _touchLeftRange = touchLeftRange;
            _touchRightRange = touchRightRange;
            _touchTopRange = touchTopRange;
            _touchBottomRange = touchBottomRange;
        }

        public void Start(float targetRotationX, float targetRotationY, float minRotationX = -25, float maxRotationX = 60)
        {
            if (targetRotationX > 180)
            {
                targetRotationX = 360 - targetRotationX;
            }
            _originalRotationX = -targetRotationX;
            _originalRotationY = targetRotationY;
            _rotationYOffset = 0f;
            _rotationXOffset = 0f;
            _rotationY = 0;
            _status = TouchStatus.IDLE;
            _minRotationX = minRotationX;
            _maxRotationX = maxRotationX;
            OnStart();
        }

        virtual protected void OnStart()
        {
        }

        virtual public void OnUpdate()
        {
        }

        public void UpdateArgs(float touchDistanceToRotation)
        {
            _touchDistanceToRotation = touchDistanceToRotation;
        }

        virtual public void UpdateArgs(float touchDistanceToRotation, float startRotateMinDistance)
        {
            _touchDistanceToRotation = touchDistanceToRotation;
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchLeftRange = ScreenUtil.ScreenWidth * left;
            _touchRightRange = ScreenUtil.ScreenWidth * right;
            _touchTopRange = ScreenUtil.ScreenHeight * top;
            _touchBottomRange = ScreenUtil.ScreenHeight * bottom;
        }

        protected bool CheckRange(float x, float y)
        {
            return x >= _touchLeftRange && x <= _touchRightRange &&
                   y >= _touchBottomRange && y <= _touchTopRange;
        }

        protected int GetStickFingerId()
        {
            return ControlStickState.fingerId;
        }

        protected bool CheckFullUI()
        {
            //return !UIManager.HasFullScreenPanel();
            return true;
        }

        protected bool InControlStick()
        {
            return ControlStickState.strength > 0 || SkillStickState.strength > 0;
        }
    }
}
