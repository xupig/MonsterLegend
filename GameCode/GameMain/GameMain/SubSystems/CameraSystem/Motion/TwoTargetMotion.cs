﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TwoTargetMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private float _horizontalSideRate;
        private float _inchingVerticalRate;
        private float _minX;
        private float _maxX;
        private float _horizontalRoundRate;
        private float _recoverSpeedRate;

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.TWO_TARGET;
        }

        public TwoTargetMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            _originalDistance = data.targetDistance;
            _originalRotation = data.targetRotation;

            _updateImmediately = false;
            _isAdjustingDistance = false;
            _isAdjustingRotation = false;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            //_distance = Vector3.Distance(data.target.position, camera.position);
            _distance = data.targetDistance;
            if (data.rotationDuration > 0)
            {
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            UpdateStretching();
            UpdateFocusPoint();
            UpdateLockPoint();
            UpdateCamera();
            UpdateCameraByWall();
        }

        private float _distanceOffset = 0;
        private float _distanceOffsetSpeed;
        private float _targetDistanceOffset;
        private bool _isStretching;
        private int _distanceOffsetDir = 1;
        private float _distanceOffsetEndTime = 0;
        public override void Stretch(float offset, float speed, float duration)
        {
            _isStretching = true;
            _targetDistanceOffset = offset;
            _distanceOffsetSpeed = speed;
            _distanceOffsetDir = Math.Sign(offset);
            _distanceOffsetEndTime = UnityPropUtils.realtimeSinceStartup + duration;
        }

        protected void UpdateStretching()
        {
            if (!_isStretching) return;
            if (IsAdjusting()) return;
            if (ACTSystem.ACTMathUtils.Abs(_targetDistanceOffset) > ACTSystem.ACTMathUtils.Abs(_distanceOffset) || _distanceOffsetDir != Math.Sign(_distanceOffset))
            {
                _distanceOffset += _distanceOffsetDir * _distanceOffsetSpeed * deltaTime;
            }
            if (UnityPropUtils.realtimeSinceStartup > _distanceOffsetEndTime)
            {
                _isStretching = false;
                _distanceOffset = 0;
                return;
            }
        }

        protected override void UpdateCamera()
        {
            float distance = _distance + _distanceOffset;
            Vector3 targetPos = data.focusPoint - Vector3.forward * distance;
            //策划要求暂时关闭镜头缓动
            bool isUse = false;
            if (!_isStretching && isUse)
            {
                Vector3 cameraPos = camera.position;
                Vector3 dir = cameraPos - data.focusPoint;
                float dis = dir.magnitude;
                float d = dis - distance;
                if (ACTSystem.ACTMathUtils.Abs(d) > 0.1f)
                {
                    float speed = d * _recoverSpeedRate;
                    float moveDis = UnityPropUtils.deltaTime * speed;
                    if (ACTSystem.ACTMathUtils.Abs(moveDis) <= ACTSystem.ACTMathUtils.Abs(d))
                    {
                        cameraPos = data.focusPoint - (Vector3.forward * (dis - moveDis));
                    }
                    else
                    {
                        cameraPos = targetPos;
                    }
                    targetPos = cameraPos;
                }
            }
            camera.position = targetPos;
            Vector3 focusPoint = data.focusPoint;
            camera.RotateAround(focusPoint, Vector3.right, _rotation.x);
            camera.RotateAround(focusPoint, Vector3.up, _rotation.y);
            camera.LookAt(focusPoint);
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static int cameraWallLayerValue = (1 << PhysicsLayerDefine.LAYER_CAMERA_WALL);
        protected void UpdateCameraByWall()
        {
            Vector3 focusPoint = data.focusPoint;
            Vector3 dir = camera.position - focusPoint;
            _downRay.direction = dir.normalized;
            _downRay.origin = focusPoint;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_downRay, out _raycastHit, 50f, cameraWallLayerValue);
            float newDis = (_raycastHit.point - focusPoint).magnitude;
            if (newDis < dir.magnitude)
            {
                camera.position = _raycastHit.point;
            }
        }

        private Vector3 _lastFocusPoint = Vector3.zero;
        private Vector3 _lastPlayerToFocus = Vector3.zero;
        private Camera _curCamera;
        private CameraTargetProxy _cameraTargetProxy;
        public Vector3 lockPointSP;
        public Vector3 playerViewSP;
        private Vector3 _lastPlayerToFocusH = Vector3.zero;
        private float deltaRotationY = 0;
        private void UpdateLockPoint()
        {
            if (data.lockTarget == null)return;
            if (_curCamera == null) _curCamera = CameraManager.GetInstance().Camera;
            if (_cameraTargetProxy == null) _cameraTargetProxy = CameraTargetProxy.instance;
            Vector3 focusPoint = data.focusPoint;
            Transform focusTransform = _cameraTargetProxy.GetTransform();
            Vector3 targetLockPoint = data.lockTarget.position;
            Vector3 playerPosition = _cameraTargetProxy.GetTargetTransform().position;//EntityPlayer.Player.position;
            lockPointSP = _curCamera.WorldToScreenPoint(targetLockPoint);
            playerViewSP = _curCamera.WorldToScreenPoint(playerPosition);
            var dis = data.lockTarget.transform.position - playerPosition;
            float xMin = ScreenUtil.ScreenWidth / 6.0f; //六分一屏幕宽度//(ScreenUtil.ScreenWidth * 0.5f - ACTSystem.ACTMathUtils.Abs(ScreenUtil.ScreenWidth * 0.5f - focusPointSP.x)); // 
            float yMin = ScreenUtil.ScreenHeight / 4.0f; //四分一屏幕高度
            float xMax = ScreenUtil.ScreenWidth - xMin;
            float yMax = ScreenUtil.ScreenHeight - ScreenUtil.ScreenHeight / 9.0f;
            Vector3 playerToFocus = targetLockPoint - playerPosition;
            Vector3 playerToFocusH = playerToFocus;
            Vector3 cameraForwardH = camera.transform.forward;
            playerToFocusH.y = cameraForwardH.y = 0;
            deltaRotationY = 0;

            if (lockPointSP.x > 0 && lockPointSP.y > 0 
                && lockPointSP.x < ScreenUtil.ScreenWidth && lockPointSP.y < ScreenUtil.ScreenHeight)
            {
                if (lockPointSP.x > xMax)
                {
                    deltaRotationY += (lockPointSP.x - xMax) * UnityPropUtils.deltaTime * _horizontalSideRate;
                }
                else if (lockPointSP.x < xMin)
                {
                    deltaRotationY += -(xMin - lockPointSP.x) * UnityPropUtils.deltaTime * _horizontalSideRate;
                }
                else if (lockPointSP.z < playerViewSP.z)
                {
                    Vector3 cameraRight = focusTransform.right;
                    Vector3 cameraForwardToTarget = (playerToFocusH.normalized - cameraForwardH);
                    int sign = Math.Sign(Vector3.Dot(cameraForwardToTarget, cameraRight));
                    sign = GetThresholdSign(sign, ref _recordDeep, ref _lastRecordDeep, ref _statusDeep);
                    var angle = Vector3.Angle(cameraForwardH, playerToFocusH);
                    deltaRotationY += sign * UnityPropUtils.deltaTime * angle * _lineAngleSpeedRate;
                }
                else
                {
                    if (_lastPlayerToFocus != Vector3.zero && _lastPlayerToFocus != playerToFocus && playerToFocusH.magnitude > 2)
                    {
                        
                        var dir = playerToFocus - _lastPlayerToFocus;
                        var sign = Math.Sign(Vector3.Dot(dir, camera.transform.right));
                        sign = GetThresholdSign(sign, ref _recordNormal, ref _lastRecordNormal, ref _statusNormal);
                        float angle = Vector3.Angle(cameraForwardH, playerToFocusH);
                        float angleChange = Vector3.Angle(playerToFocusH, _lastPlayerToFocusH);
                        float rate = 1;
                        if (angle > _maxHorizontalRoundAngle)
                        {
                            rate = _maxHorizontalRoundSpeedRate;
                        }
                        else if (angle < _minHorizontalRoundAngle)
                        {
                            rate = -1 * (_minHorizontalRoundAngle - angle) / (_minHorizontalRoundAngle) *_maxReverseHorizontalRoundSpeedRate;
                        }
                        else
                        {
                            rate = (angle - _minHorizontalRoundAngle) / (_maxHorizontalRoundAngle - _minHorizontalRoundAngle) * _maxHorizontalRoundSpeedRate;
                        }
                        deltaRotationY += sign * rate * angleChange * UnityPropUtils.deltaTime;
                    }
                }
            }
            else
            {             
                Vector3 cameraRight = focusTransform.right;
                Vector3 cameraForwardToTarget = (playerToFocusH.normalized - cameraForwardH);
                int sign = Math.Sign(Vector3.Dot(cameraForwardToTarget, cameraRight));
                var angle = Vector3.Angle(cameraForwardH, playerToFocusH);
                sign = GetThresholdSign(sign, ref _recordOutScreen,ref _lastRecordOutScreen,ref _statusOutScreen);
                deltaRotationY += sign * UnityPropUtils.deltaTime * angle * _horizontalRoundRate;
            }

            if (_lastPlayerToFocus != Vector3.zero && _lastPlayerToFocus.y != playerToFocus.y)
            {
                var cos = Vector3.Dot(playerToFocus.normalized, playerToFocusH.normalized);
                float tx = data.targetRotation.x;
                var dir = playerToFocus - playerToFocusH;
                var sign = -Math.Sign(Vector3.Dot(dir, camera.transform.up));
                float del = _rotation.x;
                float disRate = ACTSystem.ACTMathUtils.Abs(dis.y);
                float attenuation = 2f;
                disRate = disRate < attenuation ? disRate / attenuation : 1;
                _rotation.x = tx * (1 + sign * (1 - cos) * _inchingVerticalRate * disRate);
            }
            _rotation.y += deltaRotationY;
            _rotation.x = Math.Max(_minX, Math.Min(_maxX, _rotation.x));
            _lastFocusPoint = focusPoint;
            _lastPlayerToFocus = playerToFocus;
            _lastPlayerToFocusH = playerToFocusH;
        }

        private int _upThreshold = 0;
        private int _downThreshold = -2;
        //0为向上，1为向下
        private int _statusDeep = 0;
        private int _recordDeep = 0;
        private int _lastRecordDeep = 0;
        private int _statusOutScreen = 0;
        private int _recordOutScreen = 0;
        private int _lastRecordOutScreen = 0;
        private int _statusNormal = 0;
        private int _recordNormal = 0;
        private int _lastRecordNormal = 0;
        private int GetThresholdSign(int sign, ref int record, ref int lastRecord, ref int status)
        {
            record += sign;
            if(record >= lastRecord)
            {
                status = 0;
            }
            else
            {
                status = 1;
            }
            lastRecord = record;
            if (status == 0)
            {
                if (record > _upThreshold)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (record <= _downThreshold)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }

        private void UpdateFocusPoint()
        {
            data.focusPoint = data.target.position;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        private float _lineAngleSpeedRate;
        float _maxHorizontalRoundSpeedRate = 1;
        float _maxHorizontalRoundAngle = 45;
        float _minHorizontalRoundAngle = 20;
        float _maxReverseHorizontalRoundSpeedRate = 1;
        private void InitParams()
        {
            List<float> rotateList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.lock_two_rotate_rate).Split(','));
            _horizontalSideRate = rotateList[0];
            _inchingVerticalRate = rotateList[1];
            _minX = rotateList[2];
            _maxX = rotateList[3];
            _horizontalRoundRate = rotateList[4];
            _recoverSpeedRate = rotateList[5];
            _lineAngleSpeedRate = rotateList[6];
            _maxHorizontalRoundAngle = rotateList[7];
            _maxHorizontalRoundSpeedRate = rotateList[8];
            _minHorizontalRoundAngle = rotateList[9];
            _maxReverseHorizontalRoundSpeedRate = rotateList[10];
        }
    }
}
