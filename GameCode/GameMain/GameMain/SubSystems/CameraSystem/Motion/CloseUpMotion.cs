﻿
using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  leishen

 * history:  created by leishen 2015/6/18 21:12:08 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager
{
    public class CloseUpMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private float _focusPointAdjustTime;
        private float _focusPointMinSpeed;
        private float _focusPointOffset;

        private BaseTouchesMotion _touchesMotion;

        public bool IsScaled()
        {
            return _touchesMotion != null && _touchesMotion.distanceScale < 1;
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.CLOSE_UP;
        }

        public CloseUpMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            _originalDistance = data.targetDistance;
            _originalRotation = data.targetRotation;
            _updateImmediately = false;
            _isAdjustingDistance = true;
            _isAdjustingRotation = true;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            _distance = Vector3.Distance(data.target.position, camera.position);
            if (data.rotationDuration > 0)
            {
                if ((data.targetRotation.y - _rotation.y) > 180)
                {
                    data.targetRotation.y -= 360;
                }
                else if ((data.targetRotation.y - _rotation.y) < -180)
                {
                    _rotation.y -= 360;
                }
                if ((data.targetRotation.x - _rotation.x) > 180)
                {
                    data.targetRotation.x -= 360;
                }
                else if ((data.targetRotation.x - _rotation.x) < -180)
                {
                    _rotation.x -= 360;
                }
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
                OnUpdate();
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            if (IsAdjusting())
            {
                _duration += deltaTime;
                if (_isAdjustingRotation)
                {
                    AdjustRotation();
                }
                if (_isAdjustingDistance)
                {
                    AdjustDistance();
                }
            }
            UpdateFocusPoint();
            UpdateCamera();
            UpdateCameraByWall();
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static int cameraWallLayerValue = (1 << PhysicsLayerDefine.LAYER_CAMERA_WALL);
        protected void UpdateCameraByWall()
        {
            Vector3 focusPoint = data.focusPoint;
            Vector3 dir = camera.position - focusPoint;
            _downRay.direction = dir.normalized;
            _downRay.origin = focusPoint;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_downRay, out _raycastHit, 50f, cameraWallLayerValue);
            float newDis = Mathf.Max((_raycastHit.point - focusPoint).magnitude, 1);
            if (newDis < dir.magnitude)
            {
                camera.position = focusPoint + dir.normalized * newDis;//_raycastHit.point;
            }
        }

        protected override void UpdateCamera()
        {
            Vector3 focusPoint = data.focusPoint;
            float rx = _rotation.x;
            Matrix4x4 ltow = new Matrix4x4();
            ltow.SetTRS(focusPoint, Quaternion.Euler(new Vector3(rx, 0, 0)), Vector3.one);
            Vector3 p1 = ltow.MultiplyPoint(new Vector3(0, 0, -_distance));
            float dy = p1.y - focusPoint.y;
            float realdistance = 0;
            realdistance = _distance;
            camera.position = focusPoint - Vector3.forward * realdistance;
            camera.RotateAround(focusPoint, new Vector3(1, 0, 0), rx);
            camera.RotateAround(focusPoint, new Vector3(0, 1, 0), _rotation.y);
            camera.LookAt(focusPoint);
        }

        private void UpdateFocusPoint()
        {
            Vector3 targetFocusPoint = data.target.position;
            if ((targetFocusPoint - data.focusPoint).magnitude >= _focusPointOffset)
            {
                data.focusPoint = targetFocusPoint;
                return;
            }
            Vector3 lerp = Vector3.Lerp(data.focusPoint, targetFocusPoint, deltaTime / _focusPointAdjustTime);

            float minOffset = _focusPointMinSpeed * deltaTime;
            Vector3 lerpOffset = lerp - data.focusPoint;
            if ((targetFocusPoint - data.focusPoint).magnitude < minOffset)
            {
                data.focusPoint = targetFocusPoint;
            }
            else
            {
                if (lerpOffset.magnitude < minOffset)
                {
                    data.focusPoint = data.focusPoint + lerpOffset.normalized * minOffset;
                }
                else
                {
                    data.focusPoint = lerp;
                }
            }
        }

        private void AdjustRotation()
        {
            if (data.rotationDuration == 0 || _duration >= data.rotationDuration || _rotation == data.targetRotation)
            {
                _rotation = data.targetRotation;
                _isAdjustingRotation = false;
                return;
            }
            UpdateRotation();
        }

        private void UpdateRotation()
        {
            if (_rotationAccelerateDuration == 0)
            {
                _rotation += _rotationSpeed * deltaTime;
                return;
            }

            if (data.rotationDuration - _duration <= _rotationAccelerateDuration)
            {
                float t = (data.rotationDuration - _duration);
                Vector3 offset = 0.5f * _rotationAcceleration * t * t;
                _rotation = data.targetRotation - offset;
            }
            else if (_duration <= _rotationAccelerateDuration)
            {
                Vector3 offset = 0.5f * _rotationAcceleration * _duration * _duration;
                _rotation = _startRotation + offset;
            }
            else
            {
                _rotation += _rotationSpeed * deltaTime;
            }
        }

        private void AdjustDistance()
        {
            if (data.distanceDuration == 0 || _duration >= data.distanceDuration || _distance == data.targetDistance)
            {
                _distance = data.targetDistance;
                _isAdjustingDistance = false;
                return;
            }
            _distance += _distanceSpeed * deltaTime;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        private void InitParams()
        {
            List<float> argList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.rotate_camera_params).Split(','));
            _focusPointAdjustTime = argList[0];
            _focusPointMinSpeed = argList[1];
            _focusPointOffset = argList[2];
        }

        public override void Stretch(float offset, float speed, float duration)
        { }
    }
}
