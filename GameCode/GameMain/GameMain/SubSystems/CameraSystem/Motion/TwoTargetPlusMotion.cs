﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class TwoTargetPlusMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private CameraTargetProxy _cameraTargetProxy;

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.TWO_TARGET_PLUS;
        }

        public TwoTargetPlusMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            if (_cameraTargetProxy == null) _cameraTargetProxy = CameraTargetProxy.instance;
            _originalDistance = data.targetDistance;
            _originalRotation = data.targetRotation;

            _updateImmediately = false;
            _isAdjustingDistance = false;
            _isAdjustingRotation = false;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            _distance = data.targetDistance;
            if (data.rotationDuration > 0)
            {
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            UpdateFocusPoint();
            UpdateCamera();
        }

        private float _distanceOffsetSpeed;
        private float _targetDistanceOffset;
        private int _distanceOffsetDir = 1;
        private float _distanceOffsetEndTime = 0;
        public override void Stretch(float offset, float speed, float duration)
        {
            _targetDistanceOffset = offset;
            _distanceOffsetSpeed = speed;
            _distanceOffsetDir = Math.Sign(offset);
            _distanceOffsetEndTime = UnityPropUtils.realtimeSinceStartup + duration;
        }

        Vector3 playerPosition = Vector3.zero;
        Vector3 playerToLockTarget = Vector3.zero;
        Vector3 playerPosition2D = Vector3.zero;
        Vector3 playerToLockTarget2D = Vector3.zero;
        Vector3 lockTargetPosition = Vector3.zero;
        Vector3 focusPoint = Vector3.zero;
        Vector3 targetPos = Vector3.zero;
        Vector3 lastCameraPos = Vector3.zero;
        float deltaRound = 0.1f;
        protected override void UpdateCamera()
        {
            lockTargetPosition = _cameraTargetProxy.GetLockTargetProxyPos();
            playerPosition = _cameraTargetProxy.GetTransform().position;
            lastCameraPos = camera.position;
            lastCameraPos.y = playerPosition.y;

            playerPosition2D = playerPosition;
            playerPosition2D.y = 0;
            playerToLockTarget2D = lockTargetPosition;
            playerToLockTarget2D.y = 0;
            float distance = Vector3.Distance(playerPosition2D, playerToLockTarget2D);
            if (distance < checkDist)
            {
                deltaRound = minDeltaRound + (maxDeltaRound - minDeltaRound) / (checkDist * checkDist) * (distance * distance);
            }
            else
            {
                deltaRound = maxDeltaRound;
            }

            playerToLockTarget = lockTargetPosition - playerPosition;
            playerToLockTarget.y = 0;
            targetPos = playerPosition - playerToLockTarget.normalized * distanceToPlayer;

            camera.position = targetPos;
            camera.LookAt(playerPosition);
            _rotation.y = angleToPlayer;
            camera.RotateAround(playerPosition, Vector3.up, _rotation.y);
            targetPos = camera.position;
            camera.position = lastCameraPos;
            
            Vector3 v1 = camera.position - playerPosition;
            Vector3 v2 = targetPos - playerPosition;
            Vector3 lerp = Vector3.Slerp(v1, v2, deltaRound);
            targetPos = lerp + playerPosition;

            targetPos.y += deltaY;
            camera.position = targetPos;

            _rotation.x = 0;
            camera.LookAt(playerPosition);
            camera.RotateAround(playerPosition, Vector3.right, _rotation.x);
            focusPoint = lockTargetPosition;
            if (lockTargetPosition.y - playerPosition.y > maxDeltaY)
            {
                focusPoint.y = playerPosition.y + maxDeltaY;
            }
            camera.LookAt(focusPoint);
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static int cameraWallLayerValue = (1 << PhysicsLayerDefine.LAYER_CAMERA_WALL);
        protected void UpdateCameraByWall()
        {
            Vector3 focusPoint = data.focusPoint;
            Vector3 dir = camera.position - focusPoint;
            _downRay.direction = dir.normalized;
            _downRay.origin = focusPoint;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_downRay, out _raycastHit, 50f, cameraWallLayerValue);
            float newDis = (_raycastHit.point - focusPoint).magnitude;
            if (newDis < dir.magnitude)
            {
                camera.position = _raycastHit.point;
            }
        }       

        private void UpdateFocusPoint()
        {
            data.focusPoint = data.target.position;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        private float deltaY = 0;
        private float maxDeltaY = 0;
        private float distanceToPlayer = 5;
        private float angleToPlayer = 5;
        float maxDeltaRound = 0.2f;
        float minDeltaRound = 0.02f;
        float checkDist = 5f;
        private void InitParams()
        {
            List<float> rotateList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.powerplus_lock_camera_params).Split(','));
            deltaY = rotateList[0];
            maxDeltaY = rotateList[1];        
            angleToPlayer = rotateList[2];
            distanceToPlayer = rotateList[3];
            minDeltaRound = rotateList[4];
            maxDeltaRound = rotateList[5];
            checkDist = rotateList[6];
        }

        public void RefreshParams(List<float> args)
        {
            deltaY = args[0];
            maxDeltaY = args[1];
            angleToPlayer = args[2];
            distanceToPlayer = args[3];
            minDeltaRound = args[4];
            maxDeltaRound = args[5];
            checkDist = args[6];
        }
    }
}
