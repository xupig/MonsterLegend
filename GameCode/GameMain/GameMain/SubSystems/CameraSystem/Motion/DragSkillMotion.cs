﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class DragSkillMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private CameraTargetProxy _cameraTargetProxy;

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.DRAG_SKILL;
        }

        public DragSkillMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            if (_cameraTargetProxy == null) _cameraTargetProxy = CameraTargetProxy.instance;
            _isAdjustingDistance = false;
            _isAdjustingRotation = false;
        }

        List<float> rotateList;
        private float distanceToPlayer = 10;
        private float angleToPlayer = 45;
        private void InitParams()
        {
            rotateList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(323).Split(','));
            distanceToPlayer = rotateList[0];
            angleToPlayer = rotateList[1];
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override void OnUpdate()
        {
            UpdateCamera();
        }

        Vector3 playerPosition = Vector3.zero;
        Vector3 focusPoint = Vector3.zero;
        Vector3 targetPos = Vector3.zero;
        protected override void UpdateCamera()
        {
            playerPosition = _cameraTargetProxy.GetTransform().position;
            focusPoint = playerPosition;
            targetPos = playerPosition - Vector3.forward * distanceToPlayer;
            camera.position = targetPos;
            camera.RotateAround(focusPoint, Vector3.right, angleToPlayer);
            camera.RotateAround(focusPoint, Vector3.up, 0);
            camera.LookAt(focusPoint);
        }        

        public override void Stop()
        {
            _isAdjustingDistance = false;
            _isAdjustingRotation = false;
        }

        public override void Stretch(float offset, float speed, float duration)
        {

        }

    }
}
