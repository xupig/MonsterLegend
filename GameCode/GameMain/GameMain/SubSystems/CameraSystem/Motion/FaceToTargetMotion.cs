﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class FaceToTargetMotion : BaseCameraMotion
    {
        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;

        public FaceToTargetMotion(CameraData data)
            : base(data)
        {
            //InitParams();
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.FACE_TARGRT;
        }

        public override void Start()
        {
            CameraManager.GetInstance().mainCamera.cameraCom.cullingMask = CameraManager.GetInstance().mainCamera.defaultCullingMask
                - (1 << ACTSystem.ACTSystemTools.NameToLayer("Actor"));
        }

        public override void OnUpdate()
        {
            UpdateCamera();
        }

        Vector3 targetPos = Vector3.zero;
        Vector3 lockPoint = Vector3.zero;
        protected override void UpdateCamera()
        {
            targetPos = data.target.forward * data.targetDistance + data.target.position;
            targetPos.y += data.height;
            camera.position = Vector3.Lerp(camera.position, targetPos, 1f);
            camera.LookAt(data.target);
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
            CameraManager.GetInstance().mainCamera.cameraCom.cullingMask = CameraManager.GetInstance().mainCamera.defaultCullingMask;
        }

        public override void Stretch(float offset, float speed, float duration)
        { 
        }
    }
}
