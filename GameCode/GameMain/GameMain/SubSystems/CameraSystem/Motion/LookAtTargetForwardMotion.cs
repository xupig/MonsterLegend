﻿using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class LookAtTargetForwardMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        // private Vector3 _rotationSpeed;//为排除警告，注释掉
        // private Vector3 _rotationAcceleration;//为排除警告，注释掉
        //private float _rotationAccelerateDuration;//为排除警告，注释掉
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private float _focusPointAdjustTime;
        private float _focusPointMinSpeed;
        private float _focusPointOffset;

        private BaseTouchesMotion _touchesMotion;

        public bool IsScaled()
        {
            return _touchesMotion != null && _touchesMotion.distanceScale < 1;
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.ROTATION_TARGET;
        }

        public LookAtTargetForwardMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            _updateImmediately = true;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            _distance = Vector3.Distance(data.target.position, camera.position);
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            UpdateCamera();
        }

        protected override void UpdateCamera()
        {
            //UpdateCameraSlowFollow();
            UpdateCameraLockFly();
        }

        private Transform flycamera = null;
        private void UpdateCameraLockFly()
        {
            if (flycamera == null)
            {
                flycamera = GameObject.Find("fly(Clone)").transform.Find("Camera");
            }
            if (flycamera == null)
            {
                return;
            }
            camera.position = flycamera.position;
            camera.rotation = flycamera.rotation;
        }

        private float preTime = 0;
        private bool following = false;
        private void UpdateCameraSlowFollow()
        {
            if (!following)
            {
                Vector3 cp_ = data.target.worldToLocalMatrix.MultiplyPoint(camera.position); //摄像机在目标坐标系的位置
                Vector2 camera2d = new Vector2(cp_.x, cp_.z);
                double angle = Math.Asin(ACTSystem.ACTMathUtils.Abs(camera2d.x) / camera2d.magnitude);
                float currTime = UnityPropUtils.time;
                bool timeover = true;
                bool angleover = false;
                if (currTime - preTime > 1.0f)
                {//观察1秒后跟随
                    timeover = true;
                    preTime = currTime;
                }
                if (angle > (30.0f / 180.0f) * Math.PI) //0.25是45度/180度
                {//转角30度后跟随
                    angleover = true;
                }
                if (!timeover && !angleover)
                {
                    return;
                }
            }
            following = true;
            Vector3 t = new Vector3(0, 0, -6);
            Vector4 t_ = data.target.localToWorldMatrix.MultiplyPoint(t);
            Vector3 p = new Vector3(t_.x, t_.y + 2, t_.z);
            if (Vector3.Distance(camera.position, p) > 1000)
            {//当镜头太远时直接回位，处理切换场景时镜头位置差太远引起的越界问题
                camera.position = p;
                camera.LookAt(data.target.position);
                return;
            }
            Vector3 pt = CalculateSlowFollow(camera.position, p);
            if (Vector3.Distance(p, pt) < 0.1f)
            {
                following = false;
            }
            camera.position = pt;
            camera.LookAt(data.target.position);
        }

        private void UpdateCameraLockForever()
        {
            Vector3 t = new Vector3(0, 0, -6);
            Vector4 t_ = data.target.localToWorldMatrix.MultiplyPoint(t);
            Vector3 p = new Vector3(t_.x, t_.y + 2, t_.z);
            if (Vector3.Distance(camera.position, p) > 1000)
            {//当镜头太远时直接回位，处理切换场景时镜头位置差太远引起的越界问题
                camera.position = p;
                camera.LookAt(data.target.position);
                return;
            }
            Vector3 pt = Calculate(camera.position, p);
            camera.position = pt;
            camera.LookAt(data.target.position);
        }

        private Vector3 Calculate(Vector3 origin, Vector3 target)
        {
            Vector3 rst;
            float x = origin.x - target.x;
            float y = origin.y - target.y;
            float z = origin.z - target.z;
            float rate = 1.0f;
            x = origin.x - rate * x;
            y = origin.y - rate * y;
            z = origin.z - rate * z;
            rst = new Vector3(x, y, z);
            return rst;
        }

        private Vector3 CalculateSlowFollow(Vector3 origin, Vector3 target)
        {
            Vector3 rst;
            float x = origin.x - target.x;
            float y = origin.y - target.y;
            float z = origin.z - target.z;
            float rate = 0.3f;
            x = origin.x - rate * x;
            y = origin.y - rate * y;
            z = origin.z - rate * z;
            rst = new Vector3(x, y, z);
            return rst;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchesMotion.UpdateRange(top, right, bottom, left);
        }

        private bool CanTouch()
        {
            return true;
        }

        public override void Stretch(float offset, float speed, float duration)
        {
        }

        private void InitParams()
        {
            List<float> argList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.rotate_camera_params).Split(','));
            _focusPointAdjustTime = argList[0];
            _focusPointMinSpeed = argList[1];
            _focusPointOffset = argList[2];
        }
    }
}
