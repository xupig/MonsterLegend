﻿using GameResource;
using GameData;
using GameLoader.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Global;
using Common.Events;
using Common.ExtendTools;
using MogoEngine;

namespace GameMain.GlobalManager
{
    public class CameraManager
    {
        public static string MAIN_CAMERA_PREFAB = "Global/MainCamera.prefab";
        private bool _isInited = false;
        private Camera _cameraComponent;
        private Transform _cameraTransform;
        private Transform _curTarget;
        private bool _isShow = true;
        private float _initRotationX;
        private float _initRotationY;
        private float _initDistance;
        private int _initFov;
        private float _initHeight;
        private float _nearClipPlane = 0.3f;
        private float _farClipPlane = 4000;
        private float _rotationXScale = 1f;

        private Dictionary<CameraMotionType, BaseCameraMotion> _cameraMotionDict;
        private CameraAccordingMode _accordingMode = CameraAccordingMode.AccordingMotion;
        private float _oneVsOneDistanceDuration = 0f;
        private float _oneVsOnePositionDuration = 0f;
        private float _oneVsOneRotationDuration = 0f;
        private float _doubleTouchDuration = 0f;

        public MogoMainCamera mainCamera;
        public bool isOpenLock = true;
        public int lockCameraType = 0;

        public CameraMotionType curCameraMotionType
        {
            get { return mainCamera.cameraMotion.GetCameraType(); }
        }

        private static CameraManager s_instance = null;
        public static CameraManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new CameraManager();
            }
            return s_instance;
        }

        private CameraManager()
        {
            
        }

        public void SetRotationXScale(float scale)
        {
            _rotationXScale = scale;
            if (_curTarget != null)
            {
                SetFollowingTarget(_curTarget);
            }
        }

        private float GetActuralRotationX()
        {
            return _initRotationX * _rotationXScale;
        }

        private void InitCameraGameObject()
        {
            if (_isInited) return;
            _isInited = true;
            if (GameObject.Find("MainCamera") == null)
            {
                new global_params_helper();
                List<float> globalParams = global_params_helper.CameraInitial;
                _initDistance = globalParams[0];
                _initRotationX = globalParams[1];
                _initRotationY = globalParams[2];
                _initFov = (int)globalParams[3];
                _initHeight = globalParams[4];

                //no ResourceMappingManager
                GameResource.ObjectPool.Instance.GetGameObject(MAIN_CAMERA_PREFAB, (obj) =>
                {
                    if (obj == null)
                    {
                        LoggerHelper.Error("CreateActor modelName:" + MAIN_CAMERA_PREFAB + " is null");
                        return;
                    }
                    mainCamera = obj.AddComponent<MogoMainCamera>();
                    AddInteractionCompoment(obj);
                    //obj.AddComponent<KeyboardCamera>();
                    Camera cam = obj.GetComponent<Camera>();
                    if (cam != null)
                    {
                        cam.nearClipPlane = _nearClipPlane;
                        cam.farClipPlane = _farClipPlane;
                    } //以后这个值每个场景单独应该有一个
                    UnityEngine.Object.DontDestroyOnLoad(obj);
                    OnMainCameraLoaded();
                    mainCamera.gameObject.SetActive(_isShow);
                    InspectorSetting.InspectingTool.Inspect(new InspectingCamera(this), obj);
                    //AudioManager.GetInstance().SetRootGameObject(obj);
                });
            }
        }

        private void AddInteractionCompoment(GameObject cameraGameObject)
        {
            if (PlatformHelper.InTouchPlatform())
            {
                cameraGameObject.AddComponent<FingerResponse>();
            }
            else
            {
                cameraGameObject.AddComponent<MouseResponse>();
            }
        }

        private void OnMainCameraLoaded()
        {
            InitCameraMotion();
            InitOneVsOneParams();
            InitDoubleTouchParams();
            if (_curTarget != null)
            {
                SetFollowingTarget(_curTarget);
            }
            EventDispatcher.TriggerEvent(CameraEvents.ON_MAIN_CAMERA_LOADED);
        }

        private void InitCameraMotion()
        {
            CameraData data = new CameraData();
            _cameraMotionDict = new Dictionary<CameraMotionType, BaseCameraMotion>();
            _cameraMotionDict.Add(CameraMotionType.ROTATION_TARGET, new RotationTargetMotion(data));
            _cameraMotionDict.Add(CameraMotionType.ROTATION_POSITION, new RotationPositionMotion(data));
            _cameraMotionDict.Add(CameraMotionType.TARGET_POSITION, new TargetPositionMotion(data));
            _cameraMotionDict.Add(CameraMotionType.TWO_TARGET, new TwoTargetMotion(data));
            _cameraMotionDict.Add(CameraMotionType.LOOK_AT_FORWARD, new LookAtTargetForwardMotion(data));
            _cameraMotionDict.Add(CameraMotionType.ROTATION_PK_TARGET, new RotationPKTargetMotion(data));
            _cameraMotionDict.Add(CameraMotionType.TWO_TARGET_PLUS, new TwoTargetPlusMotion(data));
            _cameraMotionDict.Add(CameraMotionType.DRAG_SKILL, new DragSkillMotion(data));
            _cameraMotionDict.Add(CameraMotionType.FACE_TARGRT, new FaceToTargetMotion(data));
            _cameraMotionDict.Add(CameraMotionType.FIXED_ANGLE, new FixedAngleMotion(data));
            _cameraMotionDict.Add(CameraMotionType.CLOSE_UP, new CloseUpMotion(data));
            var enumerator = _cameraMotionDict.GetEnumerator();
            while (enumerator.MoveNext())
            {
                enumerator.Current.Value.camera = mainCamera.transform;
            }
        }

        public void SetMapCameraArgs(int id)
        {
            List<float> cameraInitial = map_camera_helper.GetCameraInitial(id);
            if (cameraInitial != null)
            {
                _initDistance = cameraInitial[0];
                _initRotationX = cameraInitial[1];
                _initRotationY = cameraInitial[2];
                _initFov = (int)cameraInitial[3];
                _initHeight = cameraInitial[4];

                if (curCameraMotionType == CameraMotionType.ROTATION_TARGET)
                {
                    if(mainCamera)
                    {
                        mainCamera.distance = _initDistance;
                        mainCamera.rotationX = GetActuralRotationX();
                        mainCamera.rotationY = _initRotationY;
                        mainCamera.height = _initHeight;
                        ChangeCameraFov(_initFov);
                        if(mainCamera.cameraMotion != null)
                        {
                            mainCamera.cameraMotion.data.targetRotation.x = GetActuralRotationX();
                            mainCamera.cameraMotion.data.targetRotation.y = _initRotationY;
                            mainCamera.cameraMotion.data.targetDistance = _initDistance;
                            mainCamera.cameraMotion.data.height = _initHeight;
                            mainCamera.cameraMotion.Start();
                        }
                    }
                }
            }
            else
            {
                LoggerHelper.Error("empty cameraInitial: " + id);
            }
        }

        private void InitOneVsOneParams()
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.one_vs_one_back_params).Split(',');
            _oneVsOnePositionDuration = float.Parse(args[0]);
            _oneVsOneRotationDuration = float.Parse(args[1]);
            _oneVsOneDistanceDuration = float.Parse(args[2]);
        }

        private void InitDoubleTouchParams()
        {
            _doubleTouchDuration = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.touches_camera_back_duration));
        }

        public void OnDoubleTouchBlankSpace()
        {
            /*
            if (PlatformHelper.InTouchPlatform() && PlayerTouchBattleModeManager.GetInstance().inTouchModeState)
            {
                return;
            }*/
            if (EntityPlayer.Player.InState(Common.ServerConfig.state_config.AVATAR_STATE_GLIDE))
            {
                return;
            }
            OnRecoverTouchesCamera();
        }

        public void OnRecoverTouchesCamera()
        {
            if (mainCamera.accordingMode == CameraAccordingMode.AccordingMotion && mainCamera.currentMotionType == CameraMotionType.ROTATION_TARGET)
            {
                RotationTargetMotion motion = _cameraMotionDict[mainCamera.currentMotionType] as RotationTargetMotion;
                if (!motion.IsAdjusting())
                {
                    ChangeToRotationTargetMotion(null, motion.originalRotation, _doubleTouchDuration, 0,
                                                 motion.originalDistance, _doubleTouchDuration);
                }
            }
        }

        //主相机是否显示
        public bool isShow { get { return _isShow; } }

        //获取当前主相机
        public Camera Camera
        {
            get
            {
                if (_cameraComponent == null && mainCamera != null)
                {
                    _cameraComponent = mainCamera.gameObject.GetComponent<Camera>();
                }
                return _cameraComponent;
            }
        }

        //获取当前主相机的显示对象
        public Transform CameraTransform
        {
            get
            {
                if (_cameraTransform == null && mainCamera != null)
                {
                    _cameraTransform = mainCamera.transform;
                }
                return _cameraTransform;
            }
        }

        public void ResetCameraFaceTo(int rotationY)
        {
            if (mainCamera != null && EntityPlayer.Player.GetTransform() != null)
            {
                _initRotationY = rotationY;
                ChangeToRotationTargetMotion(_curTarget, new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
            }
        }

        public void SetFollowingTarget(Transform target)
        {
            InitCameraGameObject();
            this._curTarget = target;
            if (mainCamera != null)
            {
                mainCamera.target = target;
                ChangeCameraFov(_initFov);
                ChangeToRotationTargetMotion(_curTarget, new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
            }
        }

        public void SetLockingTarget(Transform target, bool keepCurrentState = true)
        {
            if (this._curTarget == null) return;
            if (mainCamera != null)
            {
                if (target == null || !isOpenLock)
                {
                    ResetCameraInfo(keepCurrentState);
                }
                else
                {
                    switch (lockCameraType) //global_params_helper.lockCameraType
                    {
                        case 0:
                            ChangeToRotationPKTargetMotion(_curTarget, new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
                            break;
                        case 1:
                            ChangeToRotationPKTargetMotion(target, new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
                            break;
                        case 2:
                            ChangeToTwoTargetMotion(_curTarget, target, new Vector3(GetActuralRotationX(), _initRotationY, 0), 2f, 0, _initDistance, 2f, true, true);
                            break;
                        case 3:
                            ChangeToTwoTargetPlusMotion(_curTarget, target, new Vector3(GetActuralRotationX(), _initRotationY, 0), 2f, 0, _initDistance, 2f, true, true);
                            break;
                        case 4:
                            ChangeToFixedAngleMotion(_curTarget, 45, 12);
                            break;
                        default:
                            ChangeToTwoTargetMotion(_curTarget, target, new Vector3(GetActuralRotationX(), _initRotationY, 0), 2f, 0, _initDistance, 2f, true, true);
                            break;
                    }
                }
            }
            CameraTargetProxy.instance.SetLockTargetTranform(target);
        }

        //寻路时追背角度
        public void AutoMovingCamera(float targetAngleX = 20, float targetAngleY = 10)
        {
            if (mainCamera.currentMotionType != CameraMotionType.ROTATION_TARGET)
            {
                return;
            }
            ChangeToRotationTargetMotion(_curTarget, new Vector3(targetAngleX, targetAngleY, 0), 0.8f, 0, _initDistance, 0.1f, true, true);
        }

        public void RecoverHorizontally(Vector3 moveDirection, float targetAngle = 10, float angleSpeed = 45f, float coefficient = 1f)
        {
            //关闭
            return;
            if (mainCamera.currentMotionType != CameraMotionType.ROTATION_TARGET)
            {
                return;
            }
            float curAngleY = this.CameraTransform.rotation.eulerAngles.y;
            float curAngleX = this.CameraTransform.rotation.eulerAngles.x;
            float deltaAngle = ACTSystem.ACTMathUtils.Abs(curAngleX - targetAngle);
            if (deltaAngle > 180) deltaAngle = 360 - deltaAngle;
            if (deltaAngle > 1)
            {
                float time = deltaAngle / angleSpeed;
                ChangeToRotationTargetMotion(_curTarget, new Vector3(targetAngle, curAngleY, 0), time, 0, _initDistance, time, true, true);
            }
            else
            {
                var dot = Vector3.Dot(this.CameraTransform.right, moveDirection);
                curAngleY = curAngleY + dot * coefficient;
                ChangeToRotationTargetMotion(_curTarget, new Vector3(targetAngle, curAngleY, 0), 0.1f, 0, _initDistance, 0.1f, true, true);
            }
        }

        public void UseQualitySetting(bool state)
        {
            //int level = PlayerSettingManager.curQualityLevel;
            //if (!state)
            //{
            //    level = ClientQualityLevel.HIGH;
            //}
            //switch (level)
            //{
            //    case ClientQualityLevel.LOW:
            //        Camera.clearFlags = CameraClearFlags.SolidColor;
            //        Camera.farClipPlane = 50;
            //        Camera.backgroundColor = RenderSettings.fogColor;
            //        RenderSettings.fog = true;
            //        RenderSettings.fogStartDistance = 20;
            //        RenderSettings.fogEndDistance = 50;
            //        SceneMgr.Instance.SetEnvironment(false);
            //        break;
            //    default:
            //        Camera.clearFlags = CameraClearFlags.Skybox;
            //        Camera.farClipPlane = this._farClipPlane;
            //        RenderSettings.fog = true;
            //        RenderSettings.fogStartDistance = 30;
            //        RenderSettings.fogEndDistance = 120;
            //        SceneMgr.Instance.SetEnvironment(true);
            //        break;
            //}
        }

        public void ShowMainCameraWithOutAI()
        {
            if (mainCamera != null)
            {
                Camera.enabled = true;
            }
            _isShow = true;
        }

        public void ShowMainCamera()
        {
            if (mainCamera != null)
            {
                Camera.enabled = true;
            }
            _isShow = true;
            EventDispatcher.TriggerEvent(CameraEvents.SHOW_MAIN_CAMERA);
        }

        public void HideMainCamera()
        {
            if (mainCamera != null)
            {
                Camera.enabled = false;
            }
            _isShow = false;
            EventDispatcher.TriggerEvent(CameraEvents.HIDE_MAIN_CAMERA);
        }

        public void ResetCameraInfo(bool keepCurrentState = false)
        {
            if (keepCurrentState)
            {
                //ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), MogoMainCamera.CURRENT_ROTATION, 1f, 0, MogoMainCamera.CURRENT_DISTANCE, 1f, true, true);
                ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), mainCamera.CURRENT_ROTATION_NEW, 1f, 0, mainCamera.CURRENT_DISTANCE_NEW, 1f, true, true);
            }
            else
            {
                ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
                mainCamera.GetComponent<Camera>().fieldOfView = _initFov;
            }
            mainCamera.ResetCurrShakeInfo();
        }

        public void ResetToLastRotationTargetMotion(bool keepCurrentState = false)
        {
            if (keepCurrentState)
            {
                ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), MogoMainCamera.LAST_ROTATION_TARGET_ROTATION, 0f, 0, _initDistance, 0f, true, true);
            }
            else
            {
                ChangeToRotationTargetMotion(CameraUtil.GetDefaultCameraTarget(), new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
                mainCamera.GetComponent<Camera>().fieldOfView = _initFov;
            }
            mainCamera.ResetCurrShakeInfo();
        }

        public void SwitchLockTarget(Transform lockTarget)
        {
            ChangeToRotationTargetMotion(lockTarget, new Vector3(GetActuralRotationX(), _initRotationY, 0), 0, 0, _initDistance, 0, true, true);
            mainCamera.GetComponent<Camera>().fieldOfView = _initFov;
            mainCamera.ResetCurrShakeInfo();
        }

        public void ChangeToLastMotion()
        {
            EntityCreature newTarget = MogoWorld.GetEntity(EntityPlayer.Player.target_id) as EntityCreature;
            if (newTarget != null)
            {
                SetLockingTarget(newTarget.actor.boneController.GetBoneByName("slot_camera"));
            }
            else
            {
                SetLockingTarget(null, false);
            }
        }

        public bool CurrentIsScaled()
        {
            return mainCamera.IsScaled();
        }

        public CameraInfoBackup GetCameraInfoBackup()
        {
            var backup = new CameraInfoBackup();
            backup.accordingMode = _accordingMode;
            if (_accordingMode == CameraAccordingMode.AccordingMotion)
            {
                backup.motionType = mainCamera.currentMotionType;
            }
            BaseCameraMotion currentMotion = _cameraMotionDict[mainCamera.currentMotionType];
            backup.position = currentMotion.data.targetPosition;
            backup.localEulerAngles = currentMotion.data.targetRotation;
            backup.distance = currentMotion.data.targetDistance;
            if (backup.motionType == CameraMotionType.ROTATION_TARGET)
            {
                backup.localEulerAngles.x = (currentMotion as RotationTargetMotion).originalRotation.x;
                backup.distance = (currentMotion as RotationTargetMotion).originalDistance;
            }
            backup.target = currentMotion.data.target;
            backup.rotationMinSpeed = currentMotion.data.rotationMinSpeed;
            return backup;
        }

        //相机震动
        public void ShakeCamera(int id, float length, int count = -1)
        {
            var value = XMLManager.camera_anim[id];
            int currUsePriority = 0;
            if (mainCamera.CurrPlayShakeId > 0)
            {
                currUsePriority = XMLManager.camera_anim[mainCamera.CurrPlayShakeId].__priority;
            }

            mainCamera.SetCurrShakeInfo(value.__priority, id, length);
            if (value.__priority <= currUsePriority) return;

            mainCamera.ShakeCamera(id, value.__xSwing, value.__ySwing, value.__zSwing,
                        length, count, value.__attenuateOddsX, value.__attenuateOddsY, value.__attenuateOddsZ, value.__xDir, value.__yDir, value.__zDir);
        }

        public void StretchCamera(float offset, float speed, float duration)
        {
            if (mainCamera == null) return;
            mainCamera.Stretch(offset, speed, duration);
        }

        //改变相机视角
        public void ChangeCameraFov(int cameraFov = 0)
        {
            if (cameraFov == 0)
            {
                cameraFov = _initFov;
            }
            mainCamera.GetComponent<Camera>().fieldOfView = cameraFov;
        }

        public void ChangeCameraFarClipPlane(float farClipPlane = 0, float duration = 0)
        {
            this._farClipPlane = farClipPlane;
            if (mainCamera != null) mainCamera.ChangeCameraFarClipPlane(farClipPlane, duration);
        }

        public void ChangeCameraNearClipPlane(float nearClipPlane = 0, float duration = 0)
        {
            this._nearClipPlane = nearClipPlane;
            if (mainCamera != null) mainCamera.ChangeCameraNearClipPlane(nearClipPlane, duration);
        }

        public void ChangeFogStartDistance(float fogStartDistance, float duration)
        {
            if (mainCamera != null) mainCamera.ChangeFogStartDistance(fogStartDistance, duration);
        }

        public void ChangeFogEndDistance(float fogEndDistance, float duration)
        {
            if (mainCamera != null) mainCamera.ChangeFogEndDistance(fogEndDistance, duration);
        }

        public void ChangeToFly(Transform target)
        {
            float rotationDuration = 0;
            float rotationMinSpeed = 0;
            //ChangeToRotationTargetMotion(target, new Vector3(30, 0, 0), rotationDuration, rotationMinSpeed, 8, 0);
            ChangeToRotationTargetMotion(target, mainCamera.CURRENT_ROTATION_NEW, rotationDuration, rotationMinSpeed, 8, 0);
        }

        public void ChangeToFixedAngleMotion(Transform target, float angles, float distance)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.FIXED_ANGLE]);
            mainCamera.ChangeToFixedAngleMotion(target, angles, distance);
        }

        public void ChangeToFaceTargetMotion(Transform target, Vector3 localEuleraAngles, float distance, float height)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.FACE_TARGRT]);
            mainCamera.ChangeToFaceTargetMotion(target, localEuleraAngles, distance, height, true, false);
        }
        /// <summary>
        /// 拖动技能镜头模式,固定距离和朝向
        /// </summary>
        /// <param name="target"></param>
        /// <param name="localEuleraAngles"></param>
        /// <param name="distance"></param>
        /// <param name="distanceDuration"></param>
        public void ChangeToDragSkillMotion(Transform target, Vector3 localEuleraAngles, float distance)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.DRAG_SKILL]);
            mainCamera.ChangeToDragSkillMotion(target, localEuleraAngles, distance, true, false);
        }

        public void ChangeToLookAtTargetForwardMotion(Transform target, Vector3 localEuleraAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.LOOK_AT_FORWARD]);
            mainCamera.ChangeToLookAtTargetForwardMotion(target, localEuleraAngles, rotationDuration, rotationAccelerateRate, distance, distanceDuration, true, false);
        }

        /// <summary>
        /// 切换为战斗锁定角度，目标，距离模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
        /// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
        public void ChangeToRotationPKTargetMotion(Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ROTATION_PK_TARGET]);
            mainCamera.ChangeToRotationPKTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        /// <summary>
        /// 切换为锁定角度，目标，距离模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
        /// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
        public void ChangeToRotationTargetMotion(Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ROTATION_TARGET]);
            mainCamera.ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        /// <summary>
        /// 切换为锁定角度，目标，距离模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
        /// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
        public void ChangeToTwoTargetPlusMotion(Transform mainTarget, Transform lockTarget, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.TWO_TARGET_PLUS]);
            mainCamera.ChangeToTwoTargetPlusMotion(mainTarget, lockTarget, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        /// <summary>
        /// 切换为锁定角度，目标，距离模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
        /// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
        public void ChangeToTwoTargetMotion(Transform mainTarget, Transform lockTarget, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.TWO_TARGET]);
            mainCamera.ChangeToTwoTargetMotion(mainTarget, lockTarget, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale);
        }

        /// <summary>
        /// 切换为锁定角度，位置模式
        /// </summary>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="position">目标位置，-Vector3.one表示保持当前位置不变</param>
        /// <param name="positionDuration">位置缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        public void ChangeToRotationPositionMotion(Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.ROTATION_POSITION]);
            mainCamera.ChangeToRotationPositionMotion(localEulerAngle, rotationDuration, rotationAccelerateRate, position, positionDuration, ignoreTimeScale);
        }

        /// <summary>
        /// 切换为锁定目标，位置模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationMinSpeed">锁定最小角度</param>
        /// <param name="position">目标位置，-Vector3.one表示保持当前位置不变</param>
        /// <param name="positionDuration">位置缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        public void ChangeToTargetPositionMotion(Transform target, float rotationDuration, float rotationMinSpeed, Vector3 position, float positionDuration, bool ignoreTimeScale = true)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.TARGET_POSITION]);
            mainCamera.ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration, ignoreTimeScale);
        }

        /// <summary>
        /// 切换为锁定角度，目标，距离模式
        /// </summary>
        /// <param name="target">目标，传null表示保持当前目标不变</param>
        /// <param name="localEulerAngle">目标角度，-Vector3.one表示保持当前角度不变</param>
        /// <param name="rotationDuration">角度缓动时间, 0表示瞬间完成</param>
        /// <param name="rotationAccelerateRate">角度加速度, 配置为0-0.5，先加速后减速，0表示没有加速阶段，0.5表示没有匀速阶段</param>
        /// <param name="distance">目标距离，-1表示保持当前的距离不变</param>
        /// <param name="distanceDuration">距离缓动时间, 0表示瞬间完成</param>
        /// <param name="ignoreTimeScale">是否忽略TimeScale</param>
        /// <param name="keepTouchesScale">是否保持触控拉伸Scale</param>
        public void ChangeToCloseUpMotion(Transform target, Vector3 localEulerAngle, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale = true, bool keepTouchesScale = false, bool increment = false)
        {
            StopCurrentCameraMotion();
            if (mainCamera == null) return;
            mainCamera.SetCameraMotion(_cameraMotionDict[CameraMotionType.CLOSE_UP]);
            mainCamera.ChangeToCloseUpMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, ignoreTimeScale, keepTouchesScale, increment);
        }

        /// <summary>
        /// 切换为动画模式
        /// </summary>
        /// <param name="path">动画资源路径</param>
        /// <param name="createdCallback">创建后的回调</param>
        public void ChangeToCameraAnimation(string path, Action<GameObject> createdCallback = null)
        {
            StopCurrentCameraMotion();
            //no ResourceMappingManager
            ObjectPool.Instance.GetGameObject(path, (obj) =>
            {
                if (obj == null)
                {
                    LoggerHelper.Error(string.Format("切换为相机动画模式失败，因为路径为{0}找不到资源", path));
                    return;
                }

                _accordingMode = CameraAccordingMode.AccordingAnimation;
                if (createdCallback != null)
                {
                    createdCallback(obj);
                }
                Transform cameraSlot = obj.transform.FindChildByName("camera_slot");
                mainCamera.transform.SetParent(cameraSlot, false);
                mainCamera.transform.localPosition = Vector3.zero;
                mainCamera.transform.localEulerAngles = Vector3.zero;
                mainCamera.accordingMode = _accordingMode;
            });
        }

        /// <summary>
        /// 停止当前的动画模式
        /// </summary>
        public void StopCurrentCameraAnimation()
        {
            _accordingMode = CameraAccordingMode.AccordingMotion;
            mainCamera.transform.SetParent(null, false);
            mainCamera.accordingMode = _accordingMode;
        }

        /// <summary>
        /// 停止当前的相机效果
        /// </summary>
        public void StopCurrentCameraMotion()
        {
            switch (_accordingMode)
            {
                case CameraAccordingMode.AccordingMotion:
                    mainCamera.StopCurrentCameraMotion();
                    break;
                case CameraAccordingMode.AccordingAnimation:
                    StopCurrentCameraAnimation();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 相机缓动方法(兼容旧接口)
        /// </summary>
        /// <param name="cameraType">缓动类型</param>
        /// <param name="rotationX">目标角度X(用于类型0,1)</param>
        /// <param name="rotationY">目标角度Y(用于类型0,1)</param>
        /// <param name="rotateTime">缓动时间(没用到)</param>
        /// <param name="distance">缓动与目标距离(用于类型0,1)</param>
        /// <param name="targetPostion">目标位置(用于类型4)</param>
        /// <param name="moveTime">缓动时间(用于类型4)</param>
        /// <param name="speed">缓动频率(用于类型0)</param>
        /// <param name="odds">缓动步长(用于类型0)</param>
        /// <param name="concuss">震动参数(用于类型2,3)</param>
        /// <param name="cameraFov">视野改变(用于类型0,1)</param>
        public void ChangeCamera(int cameraType, float rotationX, float rotationY, float rotateTime, float distance, Vector3 targetPostion, float moveTime, float speed, float odds, string concuss, int cameraFov = 0)
        {
            ChangeCameraFov(cameraFov);
            switch (cameraType)
            {
                case CameraConfig.CAMERA_TYPE_ROTATE:
                    ChangeToRotationTargetMotion(null, new Vector3(rotationX, rotationY, 0), 2f, 0, distance, 2f, true, true);
                    break;
                case CameraConfig.CAMERA_TYPE_ROTATE_DIRECTLY:
                    ChangeToRotationTargetMotion(null, new Vector3(rotationX, rotationY, 0), 0f, 0, distance, 0f, true, true);
                    break;
                case CameraConfig.CAMERA_TYPE_SHAKE_1:
                case CameraConfig.CAMERA_TYPE_SHAKE_2:
                    string[] str = concuss.Split(',');
                    int id = System.Convert.ToInt32(str[0]);
                    float length = float.Parse(str[1]);
                    ShakeCamera(id, length);
                    break;
                case CameraConfig.CAMERA_TYPE_MOVE:
                    ChangeToRotationPositionMotion(-1 * Vector3.one, moveTime, 0, targetPostion, moveTime);
                    break;
                default:
                    break;
            }
        }

        public void SetScreenCamera(int cameraType, Vector3 targetPostion, float moveTime)
        {
            ChangeCamera(cameraType, 0, 0, 0, 0, targetPostion, moveTime, 0, 0, "");
        }

        public void SetPlayerCamera(int cameraType, float rotationX, float rotationY, float distance, float speed, float odds)
        {
            ChangeCamera(cameraType, rotationX, rotationY, 0, distance, -1 * Vector3.one, 0, speed, odds, "");
        }

        public void FlashCamera(float alpha)
        {
            mainCamera.FlashCamera(alpha);
        }

        public void BlackCamera(float alpha, float duration)
        {
            mainCamera.BlackCamera(alpha, duration);
        }

        public void PlayScreenWhite(float start, float end, float duration)
        {
            mainCamera.PlayScreenWhite(start, end, duration);
        }

        public void SetMainCameraToPlayer()
        {
            Transform cameraTran = EntityPlayer.Player.GetBoneByName("bone_camera");
            //EntityPlayer.Player.SetActorControllerEnabled(false);
            CameraTransform.SetParent(cameraTran);
            CameraTransform.localPosition = Vector3.zero;
            CameraTransform.localEulerAngles = new Vector3(270, 180, 0);
            mainCamera.enabled = false;
        }

        public void ResetMainCameraFromPlayer()
        {
            //LoggerHelper.Error("ResetMainCameraFromPlayer");
            //EntityPlayer.Player.SetActorControllerEnabled(true);
            CameraTransform.SetParent(null, true);
            mainCamera.cameraMotion.data.distanceDuration = 1;
            mainCamera.cameraMotion.data.rotationDuration = 1;
            mainCamera.cameraMotion.data.positionDuration = 1;
            mainCamera.enabled = true;
            mainCamera.cameraMotion.Start();
            GameLoader.Utils.Timer.TimerHeap.AddTimer(1000, 0, () =>
            {
                mainCamera.cameraMotion.data.distanceDuration = 0;
                mainCamera.cameraMotion.data.rotationDuration = 0;
                mainCamera.cameraMotion.data.positionDuration = 0;
                mainCamera.cameraMotion.Start();
            });
            //(mainCamera.cameraMotion as RotationTargetMotion)
        }
    }
}
