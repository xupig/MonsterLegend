﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain;
using Common.Data;

public class UIChatManager
{
    private static UIChatManager instance = null;

    public static UIChatManager Instance
    {
        get
        {
            if (instance == null)
                instance = new UIChatManager();
            return instance;
        }
    }

    private ChatData _chatData;
    public ChatData ChatData
    {
        get
        {
            if (_chatData == null)
            {
                _chatData = new ChatData();
            }
            return _chatData;
        }
    }

    private uint _chatId = 0;
    public uint ChatId
    {
        get
        {
            _chatId++;
            return _chatId;
        }
    }

    public string FilterWords(string content)
    {
        string str = UIChatManager.Instance.ChatData.FilterWordsInfo.FilterContent(content);
        return str;
    }

    private List<TextElement> _textElementList = new List<TextElement>();
    public TextElement GetTextElementFromPool()
    {
        if(_textElementList.Count > 0)
        {

        }
        return _textElementList[0];
    }

    public void SetTextElementToPllo()
    {

    }

    private Font font = null;
    public bool FontHasCharacter(string text)
    {
        if (font == null)
        {
            font = GameResource.ObjectPool.Instance.GetAssemblyObject(ChatTextBlock.FONT_ASSET_KEY) as Font;
        }
        char[] cc = text.ToCharArray();
        bool isHas = true;
        for (int i=0;i<cc.Length;i++)
        {
            isHas = font.HasCharacter(cc[i]);
            if (!isHas)
            {
                return isHas;
            }
        }
        return isHas;
    }

    public string ChangeCharacterNotInFont(string text)
    {
        if (font == null)
        {
            font = GameResource.ObjectPool.Instance.GetAssemblyObject(ChatTextBlock.FONT_ASSET_KEY) as Font;
        }
        List<Char> list = text.ToList<Char>();
        bool isHas = true;
        for (int i = 0; i < list.Count; i++)
        {
            isHas = font.HasCharacter(list[i]);
            if (!isHas)
            {
                list[i] = '*';
                i--;
            }
        }
        string lastText = "";
        if (list.Count > 0)
        {
            lastText = new string(list.ToArray());
        }
        //Debug.LogError("inputString:" + lastText);
        return lastText;
    }

}

