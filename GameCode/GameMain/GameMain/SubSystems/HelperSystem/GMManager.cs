﻿
using GameLoader.Utils;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;
using ArtTech;
namespace GameMain
{
    public class GMManager
    {
        private static GMManager s_instance = null;
        public static GMManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GMManager();
            }
            return s_instance;
        }

        private GMManager()
        {

        }

        public void ProcessCommand(string cmd)
        {
            LoggerHelper.Info("ProcessCommand:" + cmd);
            if (cmd.EndsWith("|"))
            {
                cmd = cmd.Substring(0, cmd.Length - 1);
            }
            string[] cmds = cmd.Split(' ');
            var cmdMethod = this.GetType().GetMethod(cmds[0].Replace("@", ""));
            this.ExcuteMethod(cmds, cmdMethod, cmd);
        }

        private void ExcuteMethod(string[] cmds, System.Reflection.MethodInfo cmdMethod, string content)
        {
            if (cmdMethod != null)
            {
                cmdMethod.Invoke(this, cmds);
            }
        }

        #region GM命令对应函数

        public void clayer(string cmd, string param1, string param2)
        {
            var layer = 1 << ACTSystem.ACTSystemTools.NameToLayer(param1);
            if (param2 != "0")
            {
                CameraManager.GetInstance().Camera.cullingMask |= layer;
            }
            else
            {
                CameraManager.GetInstance().Camera.cullingMask ^= layer;
            }
        }

        public void cshadow(string cmd, string param1)
        {
            var layer = 1 << ACTSystem.ACTSystemTools.NameToLayer(param1);
            if (param1 != "0")
            {
                ShadowManager.GetInstance().OpenShadow();
            }
            else
            {
                ShadowManager.GetInstance().HideShadow();
            }
        }

        GameObject _uiRoot = null;
        public void cuiroot(string cmd, string param1)
        {
            if (_uiRoot == null)
            {
                _uiRoot = GameObject.Find("UIRoot");
            }
            if (_uiRoot == null) return;
            if (param1 != "0")
            {
                _uiRoot.SetActive(true);
            }
            else
            {
                _uiRoot.SetActive(false);
            }
        }

        public void cuicamera(string cmd, string param1)
        {
            var go = GameObject.Find("UICamera");
            if (go == null) return;
            if (param1 != "0")
            {
                go.GetComponent<Camera>().enabled = true;
            }
            else
            {
                go.GetComponent<Camera>().enabled = false;
            }
        }

        Dictionary<string, GameObject> _gameObjectCache = new Dictionary<string, GameObject>();
        public void sobj(string cmd, string param1, string param2)
        {
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                gobj.SetActive(param2 != "0");
            }
        }

        public void fps(string cmd)
        {
            //var go = LoaderDriver.Instance.gameObject;
            //var com = go.GetComponent<FPSCounter>();
            //if (com == null)
            //{
            //    go.AddComponent<FPSCounter>();
            //}
            //else
            //{
            //    GameObject.Destroy(com);
            //}
        }
        public void sbobj(string cmd, string param1)
        {
            //@sbobj Mesh
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                SetIsStatic(gobj, true);
                StaticBatchingUtility.Combine(gobj.gameObject);
            }
        }

        public void sbobj2(string cmd, string param1)
        {
            //@sbobj2 Mesh
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                Dictionary<string, List<CombineInstance>> ciMap = new Dictionary<string, List<CombineInstance>>();
                Dictionary<string, Material> mMap = new Dictionary<string, Material>();
                MeshRenderer[] meshRenderers = gobj.GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < meshRenderers.Length; i++)
                {
                    var subMR = meshRenderers[i];
                    var mName = subMR.sharedMaterial.name;
                    if (!mMap.ContainsKey(mName))
                    {
                        mMap[mName] = subMR.sharedMaterial;
                        ciMap[mName] = new List<CombineInstance>();
                    }
                    var mf = subMR.gameObject.GetComponent<MeshFilter>();
                    CombineInstance ci = new CombineInstance();
                    ci.mesh = mf.sharedMesh;
                    ci.transform = mf.transform.localToWorldMatrix;
                    ciMap[mName].Add(ci);
                    mf.gameObject.SetActive(false);
                }

                foreach (var pair in mMap)
                {
                    string subRootName = pair.Key;
                    Material subMaterial = pair.Value;
                    GameObject subRoot = new GameObject("combined_" + subRootName);
                    subRoot.transform.SetParent(gobj.transform, false);
                    var mf = subRoot.AddComponent<MeshFilter>();
                    var mr = subRoot.AddComponent<MeshRenderer>();
                    mf.mesh = new Mesh();
                    mr.sharedMaterial = subMaterial;
                    mf.mesh.CombineMeshes(ciMap[subRootName].ToArray(), true);
                }
            }
        }

        public void clearcollider(string cmd, string param1)
        {
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                TerrainCollider[] tcs = gobj.GetComponentsInChildren<TerrainCollider>();
                for (int i = 0; i < tcs.Length; i++)
                {
                    var terrainCollider = tcs[i];
                    terrainCollider.enabled = false;
                }
                MeshCollider[] mcs = gobj.GetComponentsInChildren<MeshCollider>();
                for (int i = 0; i < mcs.Length; i++)
                {
                    var meshCollider = mcs[i];
                    meshCollider.enabled = false;
                }
            }
        }

        public void cmaterials(string cmd, string param1)
        {
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                MeshRenderer[] meshRenderers = gobj.GetComponentsInChildren<MeshRenderer>();
                for (int i = 0; i < meshRenderers.Length; i++)
                {
                    var subMR = meshRenderers[i];
                    subMR.sharedMaterial.shader = ShaderUtils.ShaderRuntime.loader.Find("Diffuse");
                }
            }
        }

        public void cluab(string cmd)
        {
            var go = LoaderDriver.Instance.gameObject;
            var ll = go.GetComponent<LuaLooper>();
            ll.enabled = !ll.enabled;
            LoggerHelper.Info("LuaLooper:" + ll.enabled);
        }

        public void cmono(string cmd, string param1, string param2)
        {
            GameObject gobj;
            if (_gameObjectCache.ContainsKey(param1) && _gameObjectCache[param1] != null)
            {
                gobj = _gameObjectCache[param1];
            }
            else
            {
                gobj = GameObject.Find(param1);
                _gameObjectCache[param1] = gobj;
            }
            if (gobj)
            {
                var comp = gobj.GetComponent(param2) as MonoBehaviour;
                if (comp != null)
                {
                    comp.enabled = !comp.enabled;
                    LoggerHelper.Info(param2 + ":" + comp.enabled);
                }
            }
        }

        public void udp(string cmd)
        {
            //UDPManager.GetInstance().Start();
        }

        public void send(string cmd, string param1, string param2, string param3)
        {
            string msg = param1.Replace(',', ' ');
            string ip = param2;
            int port = int.Parse(param3);
            //UDPManager.GetInstance().Send(msg, ip, port);
        }

        public void timescale(string cmd, string param1, string param2)
        {
            ACTSystem.ACTTimeScaleManager.GetInstance().KeepTimeScale(float.Parse(param1), float.Parse(param2));
        }

        public void screendark(string cmd, string param1, string param2, string param3)
        {
            float start = float.Parse(param1);
            float end = float.Parse(param2);
            float duration = float.Parse(param3);
            //CameraManager.GetInstance().mainCamera.PlayScreenDark(start, end, duration);
        }

        public void tpath(string cmd, string param1, string param2, string param3, string param4)
        {
            float x1 = float.Parse(param1);
            float y1 = float.Parse(param2);
            float x2 = float.Parse(param3);
            float y2 = float.Parse(param4);
            FindPathManager.GetInstance().GetPath(x1, y1, x2, y2);
        }

        public void blockmap(string cmd)
        {
            var h = EntityPlayer.Player.position.y + 10;
            Vector3 start = new Vector3(0, h, 0);
            Vector3 end = new Vector3(0, h, 0);
            var data = FindPathManager.GetInstance().GetBlockMapData();
            for (int x = data.GetLowerBound(0); x <= data.GetUpperBound(0); x++)
            {
                for (int z = data.GetLowerBound(1); z <= data.GetUpperBound(1); z++)
                {
                    var subData = data[x, z];
                    Color co = Color.grey;
                    if (subData == 0)
                    {
                        co = Color.white;
                    }
                    else if (subData == 1)
                    {
                        co = Color.blue;
                    }
                    else if (subData == 2)
                    {
                        co = Color.red;
                    }
                    else if (subData == 32)
                    {
                        co = Color.green;
                    }
                    start.x = x;
                    start.z = z;
                    end.x = x + 0.5f;
                    end.z = z;
                    Debug.DrawLine(start, end, co, 10);
                    end.x = x - 0.5f;
                    end.z = z;
                    Debug.DrawLine(start, end, co, 10);
                    end.x = x;
                    end.z = z + 0.5f;
                    Debug.DrawLine(start, end, co, 10);
                    end.x = x;
                    end.z = z - 0.5f;
                    Debug.DrawLine(start, end, co, 10);
                }
            }
        }

        public void block(string cmd, string param1, string param2)
        {
            int x = int.Parse(param1);
            int z = int.Parse(param2);
            //LoggerHelper.Error("block:" + param1 + "," + param2 + ":" + FindPathManager.GetInstance().GetPointValue(x, z));
        }

        public void radialblur(string cmd, string param1, string param2, string param3)
        {
            float strength = float.Parse(param1);
            float sharpness = float.Parse(param2);
            float darkness = float.Parse(param3);
            bool state = strength >= 0;
            if (state)
            {
                CameraManager.GetInstance().mainCamera.SetRadialBlurColorFul(state, darkness, strength, sharpness);
            }
            else
            {
                CameraManager.GetInstance().mainCamera.SetRadialBlurColorFul(state);
            }
        }

        public void warmup(string cmd)
        {
            var t = UnityPropUtils.realtimeSinceStartup;
            Shader.WarmupAllShaders();
            LoggerHelper.Error("WarmupAllShaders:" + (UnityPropUtils.realtimeSinceStartup - t));
        }

        public void unload(string cmd)
        {
            Resources.UnloadUnusedAssets();
        }

        public void recordanims(string cmd)
        {
            //ACTSystem.ACTAnimationController.recordUsedAnims = !ACTSystem.ACTAnimationController.recordUsedAnims;
        }

        public void testba(string cmd)
        {
            EntityPlayer.Player.PlayAction(116);
            //CameraManager.GetInstance().ChangeToBoneAnimationMotion(EntityPlayer.Player.id);
        }

        public void console(string cmd)
        {
            LuaDriver.instance.SwitchConsole();
        }

        public void devlog(string cmd)
        {
            //Common.Global.Global.devlog = !Common.Global.Global.devlog;
        }

        public void ctestt(string cmd, string param1)
        {
            if (param1 == "1")
            {
                CameraManager.GetInstance().ChangeToRotationPositionMotion(-1 * Vector3.one, 0, 0, -1 * Vector3.one, 0);
            }
            else
            {
                //CameraManager.GetInstance().ResetCameraInfo(true, float.Parse(param1));
            }
        }
        #endregion

        void SetIsStatic(GameObject go, bool state)
        {
            go.isStatic = state;
            Transform trans = go.transform;
            int count = trans.childCount;
            for (int i = 0; i < count; i++)
            {
                Transform child = trans.GetChild(i);
                SetIsStatic(child.gameObject, state);
            }
        }

    }
}
