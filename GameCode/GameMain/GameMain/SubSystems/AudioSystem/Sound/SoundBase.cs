﻿using Common.Data;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameResource;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    public class SoundBaseUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum SoundBaseStatus
    {
        IDLE = 0,
        LOADING,
        USING
    }
    public class SoundBase
    {
        private static uint _idCounter = 0;
        private static uint GetID()
        {
            return ++_idCounter;
        }

        private uint _id = 0;
        public uint id
        {
            get { return _id; }
        }

        private AudioSource _audioSource = null;
        public AudioSource audioSource
        {
            get { return _audioSource; }
            set { _audioSource = value; }
        }

        private int _priority = 0;
        public int priority
        {
            get
            {
                return _priority;
            }
        }

        private bool _isLoading = false;
        private string _clipPath = string.Empty;
        private SoundBaseStatus _status;
        public SoundBaseStatus status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }

        private float _volumeBak;

        public SoundBase(GameObject soundSlot)
        {
            _id = GetID();
            _audioSource = soundSlot.AddComponent<AudioSource>();
            status = SoundBaseStatus.IDLE;
            _isLoading = false;
        }

        private int curSoundId;
        public Action BeginPlaySoundEvent;
        public Action StopPlaySoundEvent;

        internal void SetAudioSource(AudioClip clip, bool loop, float volume)
        {
            if (_audioSource == null) return;
            _audioSource.clip = clip;
            _audioSource.loop = loop;
            _audioSource.volume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume * volume;
        }

        private bool CheckIsPlaying()
        {
            if (_audioSource == null) return false;
            return _audioSource.isPlaying;
        }

        private void ReleaseClip()
        {
            if (!string.IsNullOrEmpty(_clipPath))
            {
                ObjectPool.Instance.Release(_clipPath);
                _clipPath = string.Empty;
            }
        }

        public void Play(int soundID, bool loop)
        {
            if (_audioSource == null) return;
            status = SoundBaseStatus.LOADING;
            _isLoading = true;
            ReleaseClip();
            _clipPath = sound_helper.Path(soundID);
            _clipPath = ObjectPool.Instance.GetResMapping(_clipPath);
            float pitch = sound_helper.Pitch(soundID);
            if (pitch == 0)
            {
                pitch = 1;
            }
            else
            {
                pitch = pitch / 100;
            }
            float volume = sound_helper.Volume(soundID);

            var closeClipPath = _clipPath;
            if (string.IsNullOrEmpty(closeClipPath)) return;
            ObjectPool.Instance.GetAudioClip(closeClipPath, (clip) =>
            {
                _isLoading = false;
                SoundRecoverManager.instance.GetSoundRecover(SoundRecoverManager.VOICE).AddToRecordDict(clip.GetInstanceID(), sound_helper.Path(soundID));
                if (status == SoundBaseStatus.IDLE)
                {
                    return;
                }
                status = SoundBaseStatus.USING;
                SetAudioSource(clip, loop, volume);
                this._priority = priority;

                _audioSource.pitch = pitch;
                _audioSource.Play();
                if (soundID > 20000)
                {
                    curSoundId = soundID;
                    BeginPlaySoundEvent.SafeInvoke();
                    //Debug.LogError(clip.name + " " + clip.length);
                    TimerHeap.AddTimer(((uint)clip.length * 1000), 0, StopPlaySoundEvent);
                }
            });
        }

        private void StopAudioSource()
        {
            if (_audioSource != null)
            {
                _audioSource.Stop();
                _audioSource.clip = null;
            }
            ReleaseClip();
        }

        public void Stop()
        {
            if (curSoundId > 20000)
                StopPlaySoundEvent.SafeInvoke();
            StopAudioSource();
            status = SoundBaseStatus.IDLE;
        }

        public void End()
        {
            status = SoundBaseStatus.IDLE;
            StopAudioSource();
        }

        public void Reset()
        {
            StopAudioSource();
            status = SoundBaseStatus.IDLE;
            _isLoading = false;
        }

        private void TryEnd()
        {
            if (!_isLoading && CheckIsPlaying() == false)
            {
                End();
            }
        }

        public bool CanUse()
        {
            TryEnd();
            return status == SoundBaseStatus.IDLE && !_isLoading;
        }

        public bool JudgePriority(int targetPriority)
        {
            if (audioSource == null) return false;
            if (audioSource.isPlaying == false) return true;

            return this._priority < targetPriority ? true : false;
        }

        #region 临时音量控制
        public void TempCloseVolume()
        {
            if (audioSource == null) return;
            _volumeBak = audioSource.volume;
            audioSource.volume = 0f;
        }

        public void ResetTempVolume()
        {
            if (audioSource == null) return;
            audioSource.volume = _volumeBak;
        }
        #endregion

    }
}
