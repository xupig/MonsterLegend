﻿#region 模块信息
/*==========================================
// 模块名：BgMusicManager
// 命名空间: GameMain.GlobalManager
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/30
// 描述说明：背景音乐管理器
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameResource;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    /// <summary>
    /// 背景音乐管理器
    /// </summary>
    public class BgMusicManager
    {
        private static BgMusicManager s_instance = null;
        public static BgMusicManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new BgMusicManager();
            }
            return s_instance;
        }

        /// <summary>
        /// 切换关卡音乐淡出时间
        /// </summary>
        public float changScenefadeTime = 1.5f;

        private static float _fadeTime = 3f;     //淡出时间
        private float _tempMuiscVolume;

        public BgMusicManager()
        {
            SoundPool.GetInstance().BeginPlaySoundEvent += OnBeginPlaySound;
            SoundPool.GetInstance().StopPlaySoundEvent += OnStopPlaySound;
        }

        private void OnBeginPlaySound()
        {
            _tempMuiscVolume = AudioState.bgMusicVolume;
            SetMusicVolume(AudioState.bgMusicVolume * 0.5f);
        }

        private void OnStopPlaySound()
        {
            SetMusicVolume(_tempMuiscVolume);
        }

        private void SetMusicVolume(float value)
        {
            AudioState.bgMusicVolume = value;
            MogoEngine.Events.EventDispatcher.TriggerEvent<float>(Common.Events.BgMusicEvent.CHANGE_MUSIC_VOLUME, value);
        }

        public void SetRootGameObject(GameObject root)
        {
            BgMusicAudioSource.GetInstance().SetAudioSourceObject(root);
        }

        public BgMusicInfo GetSpareAudioSource(music musicData, bool isLightMusic)
        {
            return BgMusicAudioSource.GetInstance().GetAudioSource(musicData, isLightMusic);
        }

        public void PlayBgMusic(music musicData, Action<BgMusicInfo> callBack = null, bool isLightMusic = false)
        {
            BgMusicInfo music = GetSpareAudioSource(musicData, isLightMusic);
            if (callBack != null) callBack(music);
            music.status = MusicStatus.READY;
            var path = ObjectPool.Instance.GetResMapping(musicData.__path);
            ObjectPool.Instance.GetAudioClip(path, (clip) =>
            {
                music.audioSource.clip = clip;
                music.isRecover = true;
                var recover = SoundRecoverManager.instance.GetSoundRecover(SoundRecoverManager.BGMUSIC);
                recover.AddToRecordDict(music.audioSource.clip.GetInstanceID(), path);
                music.volume = AudioState.bgMusicVolume;
                music.audioSource.loop = true;
                music.status = MusicStatus.IDLE;
                music.Play();
            });
        }

        public void PlayBgMusicOld(int musicID)
        {
            music musicData = music_helper.GetInstance().GetBgMusicByMusicID(musicID);
            if (musicData == null) return;
            PlayBgMusic(musicData, (musicInfo) =>
            {
            }, true);
        }

        public void PlayBgMusic(int musicID, bool fadeOut = true, bool fadeIn = true)
        {
            music newMusicData = music_helper.GetInstance().GetBgMusicByMusicID(musicID);
            BgMusicInfo music = GetSpareAudioSource(newMusicData, true);
            music.volume = AudioState.bgMusicVolume;
            music.audioSource.loop = true;

            if (music == null)
            {
                LoggerHelper.Error("[MainCityMusic:PlayAreaTriggerMusic]=>music == null");
                return;
            }

            if (musicID == music.ID && music.IsPlaying()) return;    //重复的背景音乐不播放

            Action<Action> playOut = (callback) =>
            {
                //LoggerHelper.Error("PlayBgMusic playOut:" + fadeOut);
                if (fadeOut)
                {
                    //在播放就把原来的淡出
                    music.FadeOut(_fadeTime, callback);
                }
                else
                {
                    if (callback != null) callback();
                }
            };

            Action playIn = () =>
            {
                //LoggerHelper.Error("PlayBgMusic playIn:" + fadeIn);
                if (fadeIn)
                {
                    music.SetClipAndFadeIn(newMusicData, _fadeTime);
                }
                else
                {
                    PlayBgMusic(newMusicData, null, true);
                }
            };

            if (musicID == 0)
            {
                playOut(null);
            }
            else if (music.IsPlaying())
            {
                playOut(playIn);
            }
            else
            {
                playIn();
            }
        }

        public void StopAllBgMusic()
        {
            BgMusicAudioSource.GetInstance().StopAllBgMusic();
        }
    }
}
