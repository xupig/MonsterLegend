﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain;

namespace GameMain
{
    public class UIAudioManager
    {
        private static UIAudioManager instance = null;

        public static UIAudioManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new UIAudioManager();
                return instance;
            }
        }
        private Dictionary<string, string> uiAudioDictionary = new Dictionary<string, string>();

        public void AddUiAudio(string uiName, string clipName)
        {
            uiAudioDictionary.Add(uiName, clipName);
        }

        public void PlayButtonClick(GameObject go)
        {

        }


        public void PlayUIAudio(GameObject go, string path)
        {
            if (go.GetComponent<AudioSource>() == null)
            {
                go.AddComponent<AudioSource>();
            }

            if (go.GetComponent<SoundHander>() == null)
            {
                go.AddComponent<SoundHander>();
            }
            //SoundManager.GetInstance().Play(go, path, 0.5f, true);
        }
    }
}
