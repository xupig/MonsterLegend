﻿using GameLoader.Utils;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain
{
    public class MusicTweenManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class MusicTweenManager
    {
        private static MusicTweenManager s_instance = null;
        public static MusicTweenManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new MusicTweenManager();
            }
            return s_instance;
        }

        private List<MusicTween> _mtList = new List<MusicTween>();

        private MusicTweenManager()
        {
            MogoWorld.RegisterUpdate<MusicTweenManagerUpdateDelegate>("MusicTweenManager.Update", Update);
        }

        public void AddMusicTween(MusicTween mt)
        {
            for (int i = 0; i < _mtList.Count; i++)
            {
                if (mt == _mtList[i]) return;
            }
            _mtList.Add(mt);
        }

        int _checkBlockSpaceTime = 0;
        void Update()
        {
            _checkBlockSpaceTime--;
            if (_checkBlockSpaceTime > 0) { return; }
            _checkBlockSpaceTime = 0;
            if (_mtList.Count == 0) return;
            for (int i = _mtList.Count - 1; i >= 0; i--)
            {
                var mt = _mtList[i];
                if (!mt.Update())
                {
                    _mtList.RemoveAt(i);
                }
            }
        }
    }

    public class MusicTween
    {
        private float _startValue = 0;
        private float _endValue = 0;
        private float _fadeTime = 0;
        private float _changeSpeed = 0;
        Action<float> _finishedFunc = null;
        Action<float> _intervalFunc = null;
        public void Fade(float startValue, float endValue, float fadeTime, int interval = 20, Action<float> finishedFunc = null, Action<float> intervalFunc = null)
        {
            _startValue = startValue;
            _endValue = endValue;
            _fadeTime = fadeTime;
            _changeSpeed = (_endValue - _startValue) / fadeTime;
            _finishedFunc = finishedFunc;
            _intervalFunc = intervalFunc;
            MusicTweenManager.GetInstance().AddMusicTween(this);
        }

        public bool Update()
        {
            var deltaTime = UnityPropUtils.deltaTime;
            _fadeTime = _fadeTime - deltaTime;
            _startValue += _changeSpeed * deltaTime;
            if (_intervalFunc != null) _intervalFunc(_startValue);
            if (_fadeTime < 0)
            {
                _startValue = _endValue;
                if (_finishedFunc != null) _finishedFunc(_startValue);
                return false;
            }
            return true;    
        }
    }
}
