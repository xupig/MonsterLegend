﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using GameData;
using GameLoader.IO;
using System.Collections;
using GameLoader;
using MogoEngine.RPC;
using LuaInterface;

namespace GameMain
{
    public class ServerProxyFacade
    {
        public static void BaseLogin(String token) 
        {
            ServerProxy.Instance.BaseLogin(token);
        }

        public static void Login(String loginStr)
        {
            ServerProxy.Instance.Login(loginStr);
        }

        public static void IsEnableReconnect(bool state)
        {
            //ServerProxy.Instance.IsEnableReconnect(state);
            NetSocketManager.IsEnableReconnect(state);
        }

        public static void Connect(string ip, int port, LuaFunction connectedCBLuaF)
        {
            Action<bool> connectedCB = (result) =>
            {
                LuaFacade.CallLuaFunction(connectedCBLuaF, result);
            };
            ServerProxy.Instance.Connect(ip, port, connectedCB);
        }

        public static void CheckDefMD5()
        {
            ServerProxy.Instance.CheckDefMD5(MogoEngine.RPC.DefParser.Instance.m_defContentMD5);
        }

        public static void EnableTimeout(bool state)
        {
            ServerProxy.Instance.enableTimeout(state);
        }

        public static void SetBaseServer(string ip, int port)
        {
            RemoteProxy.baseIP = ip;
            RemoteProxy.basePort = port;
        }

        public static void Disconnect()
        {
            ServerProxy.Instance.Disconnect();
        }
    }
}
