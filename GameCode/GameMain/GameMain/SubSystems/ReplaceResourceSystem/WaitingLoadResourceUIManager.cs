﻿using GameLoader.Utils;
using GameResource;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    internal class WaitingLoadResourceUIManager
    {
        public static readonly string WaitingLoadResourceUIPath = "Global/Global_WaitingLoadResource.prefab";

        private GameObject m_uiGo;
        private XResourceDownloadProgress m_xRDP;

        public void LoadUI()
        {
            //LoggerHelper.Error("LoadUI: " + WaitingLoadResourceUIPath);
            ObjectPool.Instance.GetGameObject(WaitingLoadResourceUIPath, (go) =>
            {
                m_uiGo = go;
                GameObject.DontDestroyOnLoad(go);
                var rdpTrans = m_uiGo.transform.FindChild("Canvas/Pivot/Panel_WaitingLoadResource/Container_ResourceDownloadProgress");
                m_xRDP = rdpTrans.gameObject.AddComponent<XResourceDownloadProgress>();
                HideUI();
            });
        }

        public void ShowUI()
        {
            if (m_uiGo == null)
            {
                LoggerHelper.Error("WaitingLoadResourceUI not loaded!!!!!");
                return;
            }

            m_uiGo.SetActive(true);
        }

        public void HideUI()
        {
            m_uiGo.SetActive(false);
        }
    }
}