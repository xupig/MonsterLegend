using UnityEngine;

using MogoEngine;
namespace GameMain
{
    public class PerformaceManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }
    //作為性能優化的管理器，當某些功能需要分幀處理時，可以擴展此類。
    public class PerformaceManager
    {
        private static PerformaceManager s_instance = null;
        public static PerformaceManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PerformaceManager();
            }
            return s_instance;
        }

        PerformaceDamageHandler _damageHandler;
        PerformaceNPCHandler _npcHandler;

        public PerformaceManager()
        {
            LoaderDriver.Instance.AddComponentToDriver<FPSUtil>();
            _damageHandler = new PerformaceDamageHandler();
            _npcHandler = new PerformaceNPCHandler();
        }

        public void OnPlayerEnterWorld()
        {
            MogoWorld.RegisterUpdate<PerformaceManagerUpdateDelegate>("PerformaceManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PerformaceManager.Update", Update);
        }

        void Update()
        {
            _damageHandler.Update();
            _npcHandler.Update();
        }

        public void CreateDamageNumber(Vector3 position, int damage, int type, bool isPlayer)
        {
            _damageHandler.CreateDamageNumber(position, damage, type, isPlayer);
        }

        public PerformaceNPCHandler GetNPCHandler()
        {
            return _npcHandler;
        }
    }
}