using System.Collections.Generic;
using UnityEngine;

using GameMain.CombatSystem;
namespace GameMain
{
    public class PerformaceDamageHandler : PerformaceBaseHandler
    {
        Queue<PerformaceDamageData> _damageDataQueue;
        //Dictionary<int, int> _qualityQueueMax;
        int _handleNumPerFrame = 3;
        int _handled = 0;
        public static bool isRunning = true;

        //int _hitCount = 0;

        public PerformaceDamageHandler()
        {
            _damageDataQueue = new Queue<PerformaceDamageData>();
            //_qualityQueueMax = new Dictionary<int, int>();
            //_qualityQueueMax[ClientQualityLevel.LOW] = 2;
            //_qualityQueueMax[ClientQualityLevel.MEDIUM] = 4;
            //_qualityQueueMax[ClientQualityLevel.HIGH] = 7;
        }

        override public void Update()
        {
            if (!isRunning) return;
            _handled = 0;
            while (_damageDataQueue.Count > 0)
            {
                var data = _damageDataQueue.Dequeue();
                if (XDamageHandler.instance)
                {
                    XDamageHandler.instance.CreateDamageNumber(data.position, data.damage, data.type, data.isPlayer);
                }
                //else {
                //LuaDriver.instance.CallFunction("CSCALL_CREATE_DAMAGE", data.position, data.damage, data.type, data.isPlayer);
                //}
                _handled++;
                if (_handled >= _handleNumPerFrame) break;
            }
            //if (_hitCount > 0)
            //{
            //    if (XDoubleHit.instance)
            //    {
            //        XDoubleHit.instance.OnPlayerHit(_hitCount);
            //    }
            //    _hitCount = 0;
            //}
        }

        public void CreateDamageNumber(Vector3 position, int damage, int type, bool isPlayer)
        {
            if (!isRunning) return;
            //if (!isPlayer)
            //{
                //LuaDriver.instance.CallFunction("CSCALL_PLAYER_HIT", damage);
                //_hitCount++;
            //}
            var data = new PerformaceDamageData();
            data.position = position;
            data.damage = damage;
            data.type = type;
            data.isPlayer = isPlayer;
            _damageDataQueue.Enqueue(data);
            //int max = _qualityQueueMax[PlayerSettingManager.curQualityLevel];
            //if (_damageDataQueue.Count > max)
            //{
            //    _damageDataQueue.Dequeue();
            //}
        }
    }
}