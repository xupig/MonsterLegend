﻿using ACTSystem;
using Common.Data;
using GameLoader.Utils;
using GameMain;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using EventType = ACTSystem.EventType;
namespace GameMain
{
    public class EventTypeComparer : IEqualityComparer<ACTSystem.EventType>
    {
        public bool Equals(EventType x, EventType y)
        {
            return x == y;
        }

        public int GetHashCode(EventType obj)
        {
            return (int)obj;
        }
    }

    public class ACTResource
    {
        static Dictionary<ACTSystem.EventType, Action<ACTEventBase, List<string>>> _eventTypeFuncMap = null;
        static void InitEnvetTypeFuncMap()
        {
            if (_eventTypeFuncMap != null) return;
            _eventTypeFuncMap = new Dictionary<ACTSystem.EventType, Action<ACTEventBase, List<string>>>(new EventTypeComparer());
            _eventTypeFuncMap.Add(EventType.VisualFX, GetVisualFXResources);
            _eventTypeFuncMap.Add(EventType.SoundFX, GetSoundResources);
            _eventTypeFuncMap.Add(EventType.BeHitVisualFX, GetBeHitVisualFXResources);
            _eventTypeFuncMap.Add(EventType.BeHitSoundFX, GetBeHitSoundFXResources);
            _eventTypeFuncMap.Add(EventType.TracerVisualFX, GetTracerVisualFXResources);
            _eventTypeFuncMap.Add(EventType.BulletVisualFX, GetBulletVisualFXResources);
            _eventTypeFuncMap.Add(EventType.LaserVisualFX, GetLaserVisualFXResources);
        }

        static public List<string> GetSkillResourcePaths(int skillID)
        {
            var result = new List<string>();
            InitEnvetTypeFuncMap();
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            var eventDataList = skillData.skillEvents;
            for (int i = 0; i < eventDataList.Count; i++)
            {
                try
                {
                    GetEventResources(eventDataList[i], result);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error(string.Format("GetSkillResource Error in Skill:{0}  mainID={1}  eventIdx={2}  Message={3} ", skillID, eventDataList[i].mainID, eventDataList[i].eventIdx, ex.Message));
                }
            }
            return result;
        }

        static public List<string> GetVisualFXResourcePaths(int visualFXID)
        {
            var result = new List<string>();
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), result);
            return result;
        }

        static void GetEventResources(SkillEventData eventData, List<string> pathList)
        {
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(eventData.mainID, eventData.eventIdx);
            if (subFX == null) return;
            if (_eventTypeFuncMap.ContainsKey(subFX.EventFXType))
            {
                _eventTypeFuncMap[subFX.EventFXType](subFX, pathList);
            }
        }

        static void AddACTPathToPathList(string path, List<string> pathList)
        {
            if (string.IsNullOrEmpty(path)) return;
            pathList.Add(ACTSystemAdapter.GetRuntimeLoader().transformPath(path));
        }

        static void GetVisualFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventVisualFX;
            AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(setting), pathList);
        }

        static void GetSoundResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventSoundFX;
            for(int i = 0; i < setting.HitSoundList.Count; i++)
            {
                AddACTPathToPathList(setting.HitSoundList[i].ClipPath, pathList);
            }
        }

        static void GetBeHitVisualFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventBeHitVisualFX;
            AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(setting), pathList);
        }

        static void GetBeHitSoundFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventBeHitSoundFX;
            for (int i = 0; i < setting.BeHitSoundList.Count; i++)
            {
                AddACTPathToPathList(setting.BeHitSoundList[i].ClipPath, pathList);
            }
        }

        static void GetTracerVisualFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventTracerVisualFX;
            for (int i = 0; i < setting.TracerList.Count; i++)
            {
                var data = ACTRunTimeData.GetVisualFXData(setting.TracerList[i].VisualFXID);
                AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), pathList);
            }
            
        }

        static void GetBulletVisualFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventBulletVisualFX;
            for (int i = 0; i < setting.BulletList.Count; i++)
            {
                var data = ACTRunTimeData.GetVisualFXData(setting.BulletList[i].VisualFXID);
                AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), pathList);
            }
        }

        static void GetLaserVisualFXResources(ACTEventBase baseSetting, List<string> pathList)
        {
            var setting = baseSetting as ACTEventLaserVisualFX;
            for (int i = 0; i < setting.LaserList.Count; i++)
            {
                var data = ACTRunTimeData.GetVisualFXData(setting.LaserList[i].VisualFXID);
                AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), pathList);
                data = ACTRunTimeData.GetVisualFXData(setting.LaserList[i].startFXID);
                AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), pathList);
                data = ACTRunTimeData.GetVisualFXData(setting.LaserList[i].endFXID);
                AddACTPathToPathList(ACTVisualFXManager.GetInstance().TakePerfabName(data), pathList);
            }
        }
    }
}
