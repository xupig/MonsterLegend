﻿using GameResource;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    //[System.Serializable]
    public class InspectingObjectPool : BeInspectedObject
    {
        private ObjectPool _target;
        public InspectingObjectPool(ObjectPool target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateAssetRecordInfo(result);
            UpdateReferenceInfo(result);
            return result;
        }

        private List<string> _fastList = new List<string>();
        private void UpdateAssetRecordInfo(List<InspectingData> dataList)
        {
            _fastList.Clear();
            InspectingData data = new InspectingData();
            data.title = "AssetRecordInfo:";
            List<string> value = new List<string>();
            {
                value.Add("LoadedCount:" + ObjectPool.Instance.GetLoadAssetCount());
            }
            var aCount = ObjectPool.Instance.GetAssetLoadedCountSet();
            foreach (var pair in aCount)
            {
                //value.Add(pair.Key + " : " + pair.Value);
                string subData = pair.Key + ":" + pair.Value;
                _fastList.Add(subData);
            }
            _fastList.Sort();
            foreach (var v in _fastList)
            {
                value.Add(v);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.white;
            dataList.Add(data);
        }

        private Dictionary<string, List<string>> _fastDic = new Dictionary<string, List<string>>();
        private void UpdateReferenceInfo(List<InspectingData> dataList)
        {
            _fastDic.Clear();
            InspectingData data = new InspectingData();
            data.title = "ReferenceInfo:";
            
            foreach (var pair in ObjectPool.assetReferenceCountDict)
            {
                //value.Add(pair.Key + " : " + pair.Value);
                string subData = pair.Key + " : " + pair.Value;
                string head = subData.Substring(0, subData.IndexOf("$"));
                if (!_fastDic.ContainsKey(head))
                {
                    _fastDic[head] = new List<string>();
                }
                _fastDic[head].Add(subData);
            }
            List<string> value = new List<string>();
            foreach (var pair in _fastDic)
            {
                value.Add(pair.Key + ":");
                var list = pair.Value;
                list.Sort();
                for (int i = 0; i < list.Count; i++)
                {
                    value.Add(list[i]);
                }
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.green;
            dataList.Add(data);
        }
    }
}
