﻿using ACTSystem;
using Common.States;
using GameData;
using GameMain.ClientConfig;
using GameMain.GlobalManager;
using InspectorSetting;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    //[System.Serializable]
    public class InspectingCreature : BeInspectedObject
    {
        private EntityCreature _target;
        public InspectingCreature(EntityCreature target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateStateData(result);
            UpdateSkillInfo(result);
            UpdateSkillState(result);
            UpdateBufferData(result);
            UpdateAnimatorState(result);
            UpdateRideState(result);
            return result;
        }

        private void UpdateStateData(List<InspectingData> dataList)
        {
            var actionStateMap = _target.stateManager.characterActionStateMap;
            InspectingData data = new InspectingData();
            data.title = "Current State";
            List<string> value = new List<string>();
            value.Add("state_cell:" + Convert.ToString(_target.state_cell, 2));
            value.Add("currentState:" + Convert.ToString(_target.stateManager.currentState, 2));
            value.Add("speed:" + _target.speed + "-base_speed:" + _target.base_speed + "-moveSpeed:" + _target.moveManager.curMoveSpeed
                + "-speedMlps:" + _target.moveManager.speedMlps.Count);
            value.Add("defaultAccordingMode:" + _target.moveManager.defaultAccordingMode);
            value.Add("accordingMode:" + _target.moveManager.accordingMode);
            value.Add("maxhp:" + _target.max_hp + " - hp:" + _target.cur_hp);
            value.Add("target_id:" + _target.target_id);
            value.Add("faction_id:" + _target.faction_id);
            value.Add("facade:" + _target.facade);
            value.Add("pet_eid:" + _target.pet_eid);
            value.Add("fabao_eid:" + _target.fabao_eid);
            value.Add("isHideEntity:" + _target.isHideEntity);
            value.Add("isHideMonster:" + _target.isHideMonster);
            value.Add("fight_idle_state:" + _target.fight_idle_state);
            if (_target.actor != null)
            {
                value.Add("actorpos:" + _target.actor.position + " - entitypos:" + _target.position);
            }
            if (_target.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
            {
                value.Add("delegateEid:" + _target.delegateEid);
            }
            if (_target.entityType == EntityConfig.ENTITY_TYPE_NAME_PET)
            {
                value.Add("owner_id:" + _target.owner_id);
            }
            for (int i = 0; i < actionStateMap.Length; i++)
            {
                value.Add(i + ":" + (actionStateMap[i] == 0));
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateSkillInfo(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "PlayerActionSkillInfo:";
            List<string> value = new List<string>();
            value.Add("inServerMode:" + _target.skillManager.inServerMode);

            var posSkillMap = _target.skillManager.GetCurPosSkillIDMap();
            string curPosSkillStr = "";
            foreach (var pair in posSkillMap)
            {
                curPosSkillStr += pair.Key + ":" + pair.Value + " ";
            }
            value.Add("pos&skill:" + curPosSkillStr);

            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateBufferData(List<InspectingData> dataList)
        {
            var buffList = _target.bufferManager.GetCurBufferIDList();
            InspectingData data = new InspectingData();
            data.title = "Current Buffers";
            List<string> value = new List<string>();
            for (int i = 0; i < buffList.Count; i++)
            {
                int bufferID = buffList[i];
                var remainTime = _target.bufferManager.GetBufferRemainTime(bufferID);
                bool isClientBuff = _target.bufferManager.IsClientBuff(bufferID);
                value.Add("Buffer:" + bufferID + " r:" + String.Format("{0:F2}", remainTime) + "s" + " : " + isClientBuff);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

        private ACTActor _actor = null;
        private void UpdateAnimatorState(List<InspectingData> dataList)
        {
            if (_target != null && _target.actor != null && _target.actor != _actor)
            {
                _actor = _target.actor;
            }
            if (_actor == null) return;
            var animator = _actor.GetComponent<Animator>();
            AnimatorClipInfo[] animationInfo = animator.GetCurrentAnimatorClipInfo(0);
            InspectingData data = new InspectingData();
            data.title = "Current Animator State";
            List<string> value = new List<string>();
            value.Add("last:" + _actor.animationController.LastAction);
            value.Add("gravity:" + _actor.actorController.gravity);
            value.Add("isStatic:" + _actor.actorController.isStatic);
            value.Add("movement:" + _actor.actorController.movement + ":" + _actor.actorState.forwardMovingSpeedState + ":" + _actor.actorState.forwardMovingSpeedTransition);
            value.Add("animation speed:" + _actor.animationController.GetSpeed());
            value.Add("isSinking:" + _actor.actorController.isSinking);
            value.Add("sinkSpeed" + _actor.actorController.sinkSpeed);
            value.Add("usePlayerController:" + _actor.actorController.usePlayerController);
            value.Add("verticalSpeed:" + _actor.actorController.verticalSpeed);
            value.Add("onground:" + _actor.actorState.OnGround);
            value.Add("groundHeight:" + _actor.actorController.groundHeight);
            value.Add("ready:" + _actor.animationController.animator.GetBool("Ready"));
            foreach (AnimatorClipInfo info in animationInfo)
            {
                value.Add(info.clip.name);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateSkillState(List<InspectingData> dataList)
        {
            if (_target.skillManager.inServerMode)
            {
                UpdateSkillStateServer(dataList);
            }
            else
            {
                UpdateSkillStateClient(dataList);
            }
        }

        private void UpdateSkillStateClient(List<InspectingData> dataList)
        {
            var skillList = _target.skillManager.skillSubjectManager.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.title = "Current Skill State";
            List<string> value = new List<string>();
            for (int i = 0; i < skillList.Count; i++)
            {
                value.Add(skillList[i].skillID + ":" + skillList[i].curState);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.grey;
            dataList.Add(data);
        }

        private void UpdateSkillStateServer(List<InspectingData> dataList)
        {
            var skillList = _target.skillManager.skillSubjectManagerServer.GetCurSkillSubjects();
            InspectingData data = new InspectingData();
            data.title = "Current Skill State";
            List<string> value = new List<string>();
            for (int i = 0; i < skillList.Count; i++)
            {
                value.Add(skillList[i].skillID + ":" + skillList[i].curState);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.grey;
            dataList.Add(data);
        }

        private void UpdateRideState(List<InspectingData> dataList)
        {
            if (_target.rideManager == null) return;
            var battleAttris = _target.GetBattleAttributes();
            InspectingData data = new InspectingData();
            data.title = "RideState:";
            List<string> value = new List<string>();
            var ride = _target.rideManager.ride;
            value.Add("ride ID" + ":" + (ride != null ? ride.ID : 0));
            value.Add("ride actorID" + ":" + _target.rideManager.actorID);
            if (_target.rideManager != null)
            {
                value.Add("isRiding " + ":" + _target.rideManager.isRiding);
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.yellow;
            dataList.Add(data);
        }

    }
}
