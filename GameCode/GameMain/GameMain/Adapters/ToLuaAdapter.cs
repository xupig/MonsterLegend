﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GameLoader.IO;
using LuaInterface;
using UnityEngine;
using GameLoader;
using GameLoader.Utils;

namespace GameMain
{
    public class GameLuaFileLoader : ILuaFileLoader
    {
        //const string LUA_DIR = "Lua/";
        protected List<string> _searchPaths;
        protected Dictionary<string, string> _filePaths;

        protected bool _checkExtendLuaFile = false;
        public GameLuaFileLoader()
        {
            _searchPaths = new List<string>() {
                "Lua/BaseLib/",
                "Lua/GameLogic/",
                "Lua/Generate/",
            };
            _filePaths = new Dictionary<string, string>();
            if (UseFileSystem())
            {
                string extendFolder = string.Concat(SystemConfig.RuntimeResourcePath, "Lua");
                if (!UnityPropUtils.IsEditor && Directory.Exists(extendFolder))
                {
                    LoggerHelper.Info(extendFolder + " is exists");
                    _checkExtendLuaFile = true;
                }
            }
        }

        bool UseFileSystem()
        {
            bool result = false;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    result = true;
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;
            }
            return result;
        }

        public byte[] ReadAllBytes(string path)
        {
            byte[] result = null;
            if (UseFileSystem())
            {
                if (_checkExtendLuaFile)
                {
                    result = LoadBytesAtExtend(path);
                    if (result != null) return result;
                }
                result = LoadBytesAtFileSystem(path);
            }
            else
            {
                result = LoadBytesAtLocal(path);
            }
            return result;
        }

        public bool Exists(string path)
        {
            bool result = false;
            if (UseFileSystem())
            {
                result = !string.IsNullOrEmpty(FindFileAtFileSystem(path));
            }
            else
            {
                result = !string.IsNullOrEmpty(FindFile(path));
            }
            return result;
        }

        private string FindFile(string path)
        {
            if (!_filePaths.ContainsKey(path))
            {
                string newPath = string.Empty;
                for (int i = 0; i < _searchPaths.Count; i++)
                {
                    string fullPath = string.Concat(SystemConfig.ResPath, _searchPaths[i], path);
                    if (File.Exists(fullPath))
                    {
                        newPath = fullPath;
                        break;
                    }
                }
                _filePaths[path] = newPath;
            }
            return _filePaths[path];
        }

        private string FindFileAtFileSystem(string path)
        {
            //Debug.LogError("FindFileAtFileSystem:" + path);
            if (!_filePaths.ContainsKey(path))
            {
                string newPath = string.Empty;
                for (int i = 0; i < _searchPaths.Count; i++)
                {
                    string fullPath = string.Concat(_searchPaths[i], path);
                    //Debug.LogError("::" + fullPath + ":" + FileAccessManager.IsFileExist(fullPath));
                    if (FileAccessManager.IsFileExist(fullPath))
                    {
                        newPath = fullPath;
                        break;
                    }
                }
                _filePaths[path] = newPath;
            }
            //Debug.LogError("FindFileAtFileSystem2:" + _filePaths[path]);
            return _filePaths[path];
        }

        private string FindFileAtExtend(string path)
        {
            path = Path.GetFileName(path);
            string newPath = string.Empty;
            string fullPath = string.Concat(SystemConfig.RuntimeResourcePath, "Lua/", path);
            if (File.Exists(fullPath))
            {
                newPath = fullPath;
            }
            return newPath;
        }

        private byte[] LoadBytesAtFileSystem(string path)
        {
            //Debug.LogError("LoadBytesAtFileSystem:" + path);
            byte[] result = null;
            string fullPath = FindFileAtFileSystem(path);
            result = FileAccessManager.LoadBytes(fullPath);
            return result;
        }

        private byte[] LoadBytesAtLocal(string path)
        {
            byte[] result = null;
            string fullPath = FindFile(path);
            result = FileUtils.LoadByteFile(fullPath);
            return result;
        }

        private byte[] LoadBytesAtExtend(string path)
        {
            byte[] result = null;
            string fullPath = FindFileAtExtend(path);
            result = FileUtils.LoadByteFile(fullPath);
            return result;
        }
    }
}
