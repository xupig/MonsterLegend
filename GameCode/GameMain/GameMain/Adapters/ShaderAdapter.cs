﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using ShaderUtils;
using GameResource;
using GameLoader.Utils;

namespace GameMain
{
    public class ShaderRuntimeLoader : IShaderLoader
    {
        public Shader Find(string name)
        {
            Shader shader = null;
            name = name.Replace("\\", "/");
            name = "Shader$" + Path.GetFileNameWithoutExtension(name) + ".u";
            shader = ObjectPool.Instance.GetShader(name);
            if (shader == null)
            {
                LoggerHelper.Error("Shader Find Error:" + name);
            }
            if (UnityPropUtils.IsEditor && Shader.Find(shader.name) != null)
            {
                shader = Shader.Find(shader.name);
                LoggerHelper.Warning("Shader replace in editor:" + shader.name);
            }
            return shader;   
        }

        private static string[] _prewarmupShaders = new string[] {
            "Shader$FakeShadow.u",
            "Shader$UGUI_PANEL.u",
            "Shader$sf_ui_RGB_add.u",
            "Shader$sf_uv_3_chanel_add.u",
            "Shader$sf_aion_RGBA_add.u",
            "Shader$Boss.u",
            "Shader$Skybox-Procedural.u",
            "Shader$UGUI_ICON.u",
            "Shader$Player_Advanced.u",
            "Shader$Player.u",
            "Shader$Monster.u",
            "Shader$Scroll1Layers-AlphaAdditive.u",
            "Shader$Monster-cutout.u",
            "Shader$sf_toon_RGB_add.u",
            "Shader$MOGO2_ParticlesAdd43.u",
            "Shader$MogoParticlesAdd43.u",
            "Shader$MogoParticlesBlend43.u",
            "Shader$sf_RGBA_tint_add.u",
            "Shader$sf_aion_refraction.u",
            "Shader$sf_ui_RGB_blend.u",
            "Shader$sf_character_p4_dissolve.u",
            "Shader$sf_toon_RGB_blend.u",
            "Shader$sf_aion_RGBA_blend.u",
            "Shader$sf_aion_RGBA_add_dissolve.u",
            "Shader$sf_aion_RGBA_blend_dissolve.u",
            "Shader$sf_aion_RGBA_add_UV.u",
            "Shader$sf_aion_twistwind_blend_dissolve.u",
            "Shader$sf_aion_earthquake_depthbuff.u",
            "Shader$sf_uv_3_chanel_blend.u",
            "Shader$sf_aion_earthquake.u",
            "Shader$Unlit-Normal.u",
            "Shader$VirtualGloss_PerVertex_Additive_Lightmap.u",
        };

        public static void Warmup()
        {
            ObjectPool.Instance.PrepareMaterials(_prewarmupShaders);
        }
    }
}
