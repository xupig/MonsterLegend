﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;
using GameData;
using ACTSystem;
using MogoEngine;
using GameLoader.IO;
using GameLoader;

namespace GameMain
{
    public class XMLDataRuntimeLoader : IXMLDataLoader
    {
        private const string DATA_PBUFS = "data/pbufs/";

        bool _inited = false;
        bool _useFileSystem = false;
        bool UseFileSystem()
        {
            if (_inited)return _useFileSystem;
            _inited = true;
            _useFileSystem = false;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                //case RuntimePlatform.WindowsWebPlayer://为排除警告
                //    _useFileSystem = true;
                //    break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
                        _useFileSystem = true;
                    }
                    break;
            }
            return _useFileSystem;
        }

        public Stream LoadStream(string name)
        {
            Stream result = null;
            if (UseFileSystem())
            {
                result = LoadFromFileSystem(name);
            }
            else
            {
                result = LoadFromLocalSystem(name);
            }
            return result;
        }

        static Dictionary<string, MemoryStream>  _localStreamMap = new Dictionary<string, MemoryStream>();
        Dictionary<string, byte[]> _localBytesMap = new Dictionary<string, byte[]>();
        private MemoryStream LoadFromLocalSystem(string name)
        {
            MemoryStream result = null;
            if (!_localStreamMap.ContainsKey(name))
            {
                MemoryStream stream = null;
                string path = GameLoader.SystemConfig.ResPath + "data/pbufs/" + name + ".pbuf";
                byte[] bytes = GameLoader.IO.ResLoader.LoadByteFromSystem(path);
                stream = new MemoryStream(bytes);
                _localStreamMap[name] = stream;
            }
            result = _localStreamMap[name];

            return result;
        }

        private Stream LoadFromFileSystem(string name)
        {
            Stream result = null;
            string fileName = string.Concat(DATA_PBUFS, name + ".pbuf");
            byte[] bytes = FileAccessManager.LoadBytes(fileName);
            //bytes = PkgFileUtils.UnpackMemory(bytes);
            result = new MemoryStream(bytes);
            return result;
        }

        private static XMLSerializer serializer;
        public TValue GetValue<TKey, TValue>(TKey key, Dictionary<TKey, int[]> indexDict, string mapName, long offset)
		{
			int num = indexDict[key][0];
			int num2 = indexDict[key][1];
			Stream source;
            if (XMLDataRuntimeLoader.serializer == null)
            {
                XMLDataRuntimeLoader.serializer = new XMLSerializer();
            }
            if (UseFileSystem())
			{
                source = LoadFileBlock(mapName, Convert.ToUInt32(num + offset), num2);
			}
			else
			{
                MemoryStream stream = LoadFromLocalSystem(mapName);
				byte[] array = new byte[num2];
                stream.Position = (long)num + offset;
                stream.Read(array, 0, array.Length);
                stream.Position = 0;
                //stream.Seek(0, SeekOrigin.Begin);
                source = new MemoryStream(array);
			}
			return (TValue)((object)XMLDataRuntimeLoader.serializer.Deserialize(source, null, typeof(TValue)));
		}

        public bool IsReleaseMode()
        {
            bool result = false;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                //case RuntimePlatform.WindowsWebPlayer://为排除警告
                //result = true;
                //break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
                        result = true;
                    }
                    break;
            }
            return result;
        }

        public Stream LoadFileBlock(string name, uint offset, int length)
        {
            string fileName = "data/pbufs/" + (name + ".pbuf");
            byte[] buffer = FileAccessManager.LoadFileBlock(fileName, offset, length);
            return new MemoryStream(buffer);
        }
    }


}
