﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine;
using GameData;
using GameResource;
using GameLoader;
using GameLoader.IO;

namespace GameMain
{
    public class ACTActorLoaderUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ACTActorData
    {
        public int actorID;
        public Action<ACTActor> callback;
        public ACTActorData(int id, Action<ACTActor> cb)
        {
            actorID = id;
            callback = cb;
        }
    }

    public class ACTActorLoader
    {
        Queue<ACTActorData> _actorIDList = new Queue<ACTActorData>();
        public ACTActorLoader()
        {
            MogoWorld.RegisterUpdate<ACTActorLoaderUpdateDelegate>("ACTActorData.Update", Update);
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("ACTActorData.Update", Update);
        }

        public void LoadActorNotImmediately(int actorID, Action<ACTActor> cb)
        {
            _actorIDList.Enqueue(new ACTActorData(actorID, cb));
        }

        int _loadingMax = 1;
        int _loadingCount = 0;
        public void LoadActor(int actorID, Action<ACTActor> cb)
        {
            var actorData = ACTRunTimeData.GetActorData(actorID);
            if (actorData == null)
            {
                LoggerHelper.Error("LoadActor数据配置错误 actorID = " + actorID);
                if (cb != null) cb(null);
                return;
            }
            string modelPath = actorData.GetModelAssetPath();
            modelPath = ResourceMappingManager.Instance.GetResMapping(modelPath);
            _loadingCount++;
            GameResource.ObjectPool.Instance.GetGameObjects(new string[] { modelPath }, (objs) =>
            {
                _loadingCount--;
                if (objs[0] == null)
                {
                    LoggerHelper.Error("CreateActor modelName:" + modelPath + " is null");
                    return;
                }
                var actor = ACTSystemDriver.GetInstance().CreateActor(objs[0], actorID);
                if (cb != null) cb(actor);
            });
        }

        void Update()
        {
            if (_loadingCount >= _loadingMax) return;
            if (_actorIDList.Count > 0)
            {
                var data = _actorIDList.Dequeue();
                LoadActor(data.actorID, data.callback);
            }
        }
    }

    public class ACTSystemAdapter
    {
        private const string DATA_PBUFS = "data/pbufs/";

        private static ACTActorLoader _actorLoader = null;
        private static ACTActorLoader actorLoader
        {
            get
            {
                if (_actorLoader == null)
                {
                    _actorLoader = new ACTActorLoader();
                }
                return _actorLoader;
            }
        }

        public static void CreateActor(int id, Action<ACTActor> cb, bool immediately = false)
        {
            if (immediately)
            {
                actorLoader.LoadActor(id, cb);
            }
            else
            {
                actorLoader.LoadActorNotImmediately(id, cb);
            }
        }

        public static string GetActorModelPath(int id)
        {
            var actorData = ACTRunTimeData.GetActorData(id);
            if (actorData == null)
            {
                return "";
            }
            return actorData.GetModelAssetPath();
        }

        public static string GetVisualFXPath(int id)
        {
            var visualFXData = ACTRunTimeData.GetVisualFXData(id);
            if (visualFXData == null || string.IsNullOrEmpty(visualFXData.Perfabs_H_Name))
            {
                return "";
            }
            return visualFXData.Perfabs_H_Name.Substring("Assets/Resources/".Length);
        }

        public static EntityCreature GetOwner(ACTActor actor)
        {
            if (actor == null || actor.owner == null) return null;
            uint entityID = actor.owner.GetActorOwnerID();
            EntityCreature entity = MogoWorld.GetEntity(entityID) as EntityCreature;
            return entity;
        }

        static ACTRuntimeLoader _loader = null;
        public static ACTRuntimeLoader GetRuntimeLoader()
        {
            if (_loader == null)
            {
                _loader = new ACTRuntimeLoader();
            }
            return _loader;
        }

        public static void SetCreateActorController(ACTActor actor, bool packUpWeapon, int vocation, Action callBack = null)
        {
            if (actor == null) return;
            string controllerPath = string.Empty;
            controllerPath = role_data_helper.GetCreateControllerPath(vocation);
            actor.equipController.PackUpWeapon(packUpWeapon);
            var animatorProxy = actor.gameObject.GetComponent<AnimatorProxy>();
            if (animatorProxy != null)
                animatorProxy.SetController(controllerPath, () =>
                {
                    if (callBack != null) callBack();
                });
        }

        public static void SetActorCityState(ACTActor actor, bool packUpWeapon, int vocation, Action callBack = null)
        {
            if (actor == null) return;
            string controllerPath = string.Empty;
            controllerPath = role_data_helper.GetCombatControllerPath(vocation);
            actor.equipController.PackUpWeapon(packUpWeapon);
            var animatorProxy = actor.gameObject.GetComponent<AnimatorProxy>();
            if (animatorProxy != null)
                animatorProxy.SetController(controllerPath, () =>
                {
                    actor.animationController.PlayAction(42);
                    if (callBack != null) callBack();
                });
        }

        public static void SetUIActorController(ACTActor actor, bool packUpWeapon, int vocation, Action callBack = null)
        {
            if (actor == null) return;
            string controllerPath = string.Empty;
            controllerPath = role_data_helper.GetUIControllerPath(vocation);
            actor.equipController.PackUpWeapon(packUpWeapon);
            var animatorProxy = actor.gameObject.GetComponent<AnimatorProxy>();
            animatorProxy.SetController(controllerPath, () =>
            {
                if (callBack != null) callBack();
            });
        }

        /// <summary>
        /// 反序列化pbuf还原为数据
        /// </summary>
        /// <param name="fileName">文件名,例如：ACTData.pbuf</param>
        private static byte[] SeriPbuf(string fileName)
        {
            string folder = string.Concat(DATA_PBUFS, fileName);
            bool isExistFile = FileAccessManager.IsFileExist(folder);
            if (!isExistFile) LoggerHelper.Debug(string.Format("pbuf文件:{0},{1}", folder, isExistFile ? "存在" : "找不到"));
            byte[] bytes = FileAccessManager.LoadBytes(folder);
            //bytes = PkgFileUtils.UnpackMemory(bytes);
            return bytes;
        }

        public static ACTSystem.ACTSystemDriver curACTSystemDriver;
        public static void InitACTSystem()
        {
            ACTSystem.ACTSerializableData sData = new ACTSystem.ACTSerializableData();
            SerializableData.DataSerializer ds = new SerializableData.DataSerializer();

            if (SystemConfig.ReleaseMode)
            {//发布模式
                //ProgressBar.Instance.UpdateTip("正在初始化pbuf");
                LoggerHelper.Info("正在初始化pbuf");
                Stream strem = new MemoryStream(SeriPbuf("ACTDataActor.pbuf"));
                sData.ActorsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataActor>)) as Dictionary<int, ACTSystem.ACTDataActor>;
                strem = new MemoryStream(SeriPbuf("ACTDataSkill.pbuf"));
                sData.SkillsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataSkill>)) as Dictionary<int, ACTSystem.ACTDataSkill>;
                strem = new MemoryStream(SeriPbuf("ACTDataVisualFX.pbuf"));
                sData.VisualFXsDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataVisualFX>)) as Dictionary<int, ACTSystem.ACTDataVisualFX>;
                strem = new MemoryStream(SeriPbuf("ACTDataEquipment.pbuf"));
                sData.EquipmentDict = ds.Deserialize(strem, null, typeof(Dictionary<int, ACTSystem.ACTDataEquipment>)) as Dictionary<int, ACTSystem.ACTDataEquipment>;
            }
            else
            {
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.SystemConfig.ResPath + "/data/pbufs/ACTDataActor.pbuf"))
                {
                    sData.ActorsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataActor>)) as Dictionary<int, ACTSystem.ACTDataActor>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.SystemConfig.ResPath + "/data/pbufs/ACTDataSkill.pbuf"))
                {
                    sData.SkillsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataSkill>)) as Dictionary<int, ACTSystem.ACTDataSkill>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.SystemConfig.ResPath + "/data/pbufs/ACTDataVisualFX.pbuf"))
                {
                    sData.VisualFXsDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataVisualFX>)) as Dictionary<int, ACTSystem.ACTDataVisualFX>;
                    fs.Close();
                }
                using (System.IO.FileStream fs = System.IO.File.OpenRead(GameLoader.SystemConfig.ResPath + "/data/pbufs/ACTDataEquipment.pbuf"))
                {
                    sData.EquipmentDict = ds.Deserialize(fs, null, typeof(Dictionary<int, ACTSystem.ACTDataEquipment>)) as Dictionary<int, ACTSystem.ACTDataEquipment>;
                    fs.Close();
                }
            }
            var obj = new UnityEngine.GameObject("ACTSystem");
            UnityEngine.Object.DontDestroyOnLoad(obj);
            obj.AddComponent<ACTSystem.ACTDriver>();
            curACTSystemDriver = obj.AddComponent<ACTSystem.ACTSystemDriver>();
            curACTSystemDriver.runtimeMode = true;
            curACTSystemDriver.ObjectPool.SetAssetLoader(ACTSystemAdapter.GetRuntimeLoader());
            curACTSystemDriver.SoundPlayer = new ACTRuntimeSoundPlayer();
            curACTSystemDriver.Logger = new ACTRunTimeLogger();

            curACTSystemDriver.SystemData = UnityEngine.ScriptableObject.CreateInstance<ACTSystem.ACTSystemData>();
            curACTSystemDriver.SystemData.InitBySerializableData(sData);
            ShaderUtils.ShaderRuntime.loader = new ShaderRuntimeLoader();
            ShaderUtils.ShaderRuntime.inGameRuntime = true;
            List<float> defaultRotateArgs = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.default_camera_h_rotate_params).Split(','));
            ACTBoneController.CameraSlotHight = defaultRotateArgs[3];

            PostEffectHandlerBase.Instance = ArtTech.PostEffectHandler.Instance;
        }
    }

    public class ACTRuntimeLoader : IACTAssetLoader
    {
        private const string HEAD_PATH = "Assets/Resources/";

        public void GetObject(string strPath, ACT_VOID_OBJ callback)
        {
            strPath = transformPath(strPath);
            GameResource.ObjectPool.Instance.GetObject(strPath, (obj) =>
            {
                if (callback != null) callback(obj);
            });
        }

        public void GetObjects(string[] strNames, ACT_VOID_OBJS callback)
        {
            transformPaths(strNames);
            GameResource.ObjectPool.Instance.GetObjects(strNames, (objs) =>
            {
                if (callback != null) callback(objs);
            });
        }

        public void GetGameObject(string strPath, ACT_VOID_OBJ callback)
        {
            strPath = transformPath(strPath);
            GameResource.ObjectPool.Instance.GetGameObject(strPath, (obj) =>
            {
                if (callback != null) callback(obj);
            });
        }

        public void GetGameObjects(string[] strPaths, ACT_VOID_OBJS callback)
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                strPaths[i] = transformPath(strPaths[i]);
            }
            GameResource.ObjectPool.Instance.GetGameObjects(strPaths, (objs) =>
            {
                if (callback != null) callback(objs);
            });
        }

        public void PreloadAsset(string[] strPaths, ACT_VOID callback)
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                strPaths[i] = transformPath(strPaths[i]);
            }
            GameResource.ObjectPool.Instance.PreloadAsset(strPaths, () =>
            {
                if (callback != null) callback();
            });
        }

        public void ReleaseAsset(string strPath)
        {
            if (string.IsNullOrEmpty(strPath)) return;
            strPath = transformPath(strPath);
            ObjectPool.Instance.Release(strPath);
        }

        public void ReleaseAssets(string[] strPaths)
        {
            for (int i = 0; i < strPaths.Length; i++)
            {
                ReleaseAsset(strPaths[i]);
            }
        }

        public string GetResMapping(string resourceName)
        {
            resourceName = transformPath(resourceName);
            return ResourceMappingManager.Instance.GetResMapping(resourceName);
        }

        public string[] GetResMappings(string[] resourcesName)
        {
            transformPaths(resourcesName);
            return ResourceMappingManager.Instance.GetResMappings(resourcesName);
        }

        public string transformPath(string srcPath)
        {
            if (srcPath.Contains(HEAD_PATH))
            {
                return srcPath.Substring(HEAD_PATH.Length);
            }
            return srcPath;
        }

        void transformPaths(string[] srcPaths)
        {
            for (int i = 0; i < srcPaths.Length; i++)
            {
                srcPaths[i] = transformPath(srcPaths[i]);
            }
        }
    }

    public class ACTRuntimeSoundPlayer : IACTSoundPlayer
    {
        private static readonly string AssetPath = "Assets/Resources/";
        public void Play(AudioSource source, string strPath, float volume, bool loop = false)
        {
            strPath = strPath.Substring(AssetPath.Length);
            GameObjectSoundManager.GetInstance().Play(source, source.gameObject, strPath, volume, loop);
        }
    }

    public class ACTRunTimeLogger : IACTLogger
    {

        public void Error(string msg)
        {
            LoggerHelper.Error(msg);
        }

        public void Info(string msg)
        {
            LoggerHelper.Info(msg);
        }

        public void Warning(string msg)
        {
            LoggerHelper.Warning(msg);
        }
    }




}
