﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using ShaderUtils;
using GameResource;
using GameLoader.Utils;

namespace GameMain
{
    public class RenderTextureRuntimeLoader : IRenderTextureLoader
    {
        public RenderTexture LoadRenderTexture()
        {
            return CameraRTManager.GetInstance().GetMainCameraOneShotRT();
        }
    }


}
