﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class GameMain_PosPointingArrowWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(GameMain.PosPointingArrow), typeof(GameMain.XContainer), null);
		L.RegFunction("Show", Show);
		L.RegFunction("Hide", Hide);
		L.RegFunction("__eq", op_Equality);
		L.RegFunction("__tostring", Lua_ToString);
		L.RegVar("HIDE_POSITION", get_HIDE_POSITION, null);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Show(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			GameMain.PosPointingArrow obj = (GameMain.PosPointingArrow)ToLua.CheckObject(L, 1, typeof(GameMain.PosPointingArrow));
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj.Show(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Hide(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.PosPointingArrow obj = (GameMain.PosPointingArrow)ToLua.CheckObject(L, 1, typeof(GameMain.PosPointingArrow));
			obj.Hide();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int op_Equality(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.ToObject(L, 2);
			bool o = arg0 == arg1;
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Lua_ToString(IntPtr L)
	{
		object obj = ToLua.ToObject(L, 1);

		if (obj != null)
		{
			LuaDLL.lua_pushstring(L, obj.ToString());
		}
		else
		{
			LuaDLL.lua_pushnil(L);
		}

		return 1;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_HIDE_POSITION(IntPtr L)
	{
		try
		{
			ToLua.Push(L, GameMain.PosPointingArrow.HIDE_POSITION);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}
}

