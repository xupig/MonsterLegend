﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class MogoEngine_Mgrs_SceneMgrWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(MogoEngine.Mgrs.SceneMgr), typeof(System.Object), null);
		L.RegFunction("SetHideAddStaticPrefabs", SetHideAddStaticPrefabs);
		L.RegFunction("LoadScene", LoadScene);
		L.RegFunction("LeaveScene", LeaveScene);
		L.RegFunction("CreateSceneResources", CreateSceneResources);
		L.RegFunction("__tostring", Lua_ToString);
		L.RegVar("defaultAmbientLight", get_defaultAmbientLight, set_defaultAmbientLight);
		L.RegVar("EMPTH_SCENE", get_EMPTH_SCENE, null);
		L.RegVar("CONTAINER_SCENE", get_CONTAINER_SCENE, null);
		L.RegVar("Instance", get_Instance, null);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetHideAddStaticPrefabs(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.Mgrs.SceneMgr obj = (MogoEngine.Mgrs.SceneMgr)ToLua.CheckObject(L, 1, typeof(MogoEngine.Mgrs.SceneMgr));
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.SetHideAddStaticPrefabs(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LoadScene(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			MogoEngine.Mgrs.SceneMgr obj = (MogoEngine.Mgrs.SceneMgr)ToLua.CheckObject(L, 1, typeof(MogoEngine.Mgrs.SceneMgr));
			MogoEngine.Mgrs.LoadSceneSetting arg0 = (MogoEngine.Mgrs.LoadSceneSetting)ToLua.CheckObject(L, 2, typeof(MogoEngine.Mgrs.LoadSceneSetting));
			System.Action arg1 = null;
			LuaTypes funcType3 = LuaDLL.lua_type(L, 3);

			if (funcType3 != LuaTypes.LUA_TFUNCTION)
			{
				 arg1 = (System.Action)ToLua.CheckObject(L, 3, typeof(System.Action));
			}
			else
			{
				LuaFunction func = ToLua.ToLuaFunction(L, 3);
				arg1 = DelegateFactory.CreateDelegate(typeof(System.Action), func) as System.Action;
			}

			obj.LoadScene(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LeaveScene(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.Mgrs.SceneMgr obj = (MogoEngine.Mgrs.SceneMgr)ToLua.CheckObject(L, 1, typeof(MogoEngine.Mgrs.SceneMgr));
			obj.LeaveScene();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CreateSceneResources(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			MogoEngine.Mgrs.SceneMgr obj = (MogoEngine.Mgrs.SceneMgr)ToLua.CheckObject(L, 1, typeof(MogoEngine.Mgrs.SceneMgr));
			MogoEngine.Mgrs.LoadSceneSetting arg0 = (MogoEngine.Mgrs.LoadSceneSetting)ToLua.CheckObject(L, 2, typeof(MogoEngine.Mgrs.LoadSceneSetting));
			System.Action arg1 = null;
			LuaTypes funcType3 = LuaDLL.lua_type(L, 3);

			if (funcType3 != LuaTypes.LUA_TFUNCTION)
			{
				 arg1 = (System.Action)ToLua.CheckObject(L, 3, typeof(System.Action));
			}
			else
			{
				LuaFunction func = ToLua.ToLuaFunction(L, 3);
				arg1 = DelegateFactory.CreateDelegate(typeof(System.Action), func) as System.Action;
			}

			obj.CreateSceneResources(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Lua_ToString(IntPtr L)
	{
		object obj = ToLua.ToObject(L, 1);

		if (obj != null)
		{
			LuaDLL.lua_pushstring(L, obj.ToString());
		}
		else
		{
			LuaDLL.lua_pushnil(L);
		}

		return 1;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_defaultAmbientLight(IntPtr L)
	{
		try
		{
			ToLua.Push(L, MogoEngine.Mgrs.SceneMgr.defaultAmbientLight);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_EMPTH_SCENE(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushstring(L, MogoEngine.Mgrs.SceneMgr.EMPTH_SCENE);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_CONTAINER_SCENE(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushstring(L, MogoEngine.Mgrs.SceneMgr.CONTAINER_SCENE);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_Instance(IntPtr L)
	{
		try
		{
			ToLua.PushObject(L, MogoEngine.Mgrs.SceneMgr.Instance);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_defaultAmbientLight(IntPtr L)
	{
		try
		{
			UnityEngine.Color arg0 = ToLua.ToColor(L, 2);
			MogoEngine.Mgrs.SceneMgr.defaultAmbientLight = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}
}

