﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACTSystem;
using UnityEngine;

namespace ACTSystem
{
    public class ACTFootStep
    {
        private ACTActor _actor;
        private RaycastHit footStepHit;
        public float footLength = 50;
        public bool canUse = false;

        public ACTFootStep(ACTActor actor)
        {
            _actor = actor;
        }

        public void OnFootEvent(ACTActor actor, int param)
        {
            if (!canUse)
            {
                return;
            }

            string tag = "Terrain";
            if (Physics.Raycast(actor.position, Vector3.down, out footStepHit, footLength))
            {
                tag = footStepHit.collider.tag;
            }
            switch (tag)
            {
                case "Terrain":
                    if (actor.actorData.FootFxList.Count > 0 && actor.actorData.FootFxList[0] > 0)
                    {
                        PlaySfx(actor.actorData.FootFxList[0], actor);
                    }
                    if (actor.actorData.FootSoundList.Count > 0)
                    {
                        PlayFootSound(ACTFootSoundType.Default, actor);
                    }
                    break;
                case "Water":
                    if (actor.actorData.FootFxList.Count > 1 && actor.actorData.FootFxList[1] > 0)
                    {
                        PlaySfx(actor.actorData.FootFxList[1], actor);
                    }
                    if (actor.actorData.FootSoundList.Count > 1)
                    {
                        PlayFootSound(ACTFootSoundType.Water, actor);
                    }
                    break;
                default:
                    if (actor.actorData.FootFxList.Count > 0 && actor.actorData.FootFxList[0] > 0)
                    {
                        PlaySfx(actor.actorData.FootFxList[0], actor);
                    }
                    if (actor.actorData.FootSoundList.Count > 0)
                    {
                        PlayFootSound(ACTFootSoundType.Default, actor);
                    }
                    break;
            }
        }

        public void PlaySfx(int fxID, ACTActor actor)
        {
            if (actor.IsUseFootSmoke)
            {
                ACTVisualFXManager.GetInstance().PlayACTVisualFX(fxID, actor);                                                
            }
        }

        public void PlayFootSound(ACTFootSoundType footSoundType, ACTActor actor)
        {
            if (actor.IsUseFootSound)
            {
                actor.soundController.PlayFootSound(footSoundType);
            }
        }
    }

}