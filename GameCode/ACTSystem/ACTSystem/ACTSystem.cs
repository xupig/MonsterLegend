﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(ACTEventListener))]
    public class ACTSystemDriver : MonoBehaviour
    {
        static ACTSystemDriver s_instance;
        static public ACTSystemDriver GetInstance()
        {
            return s_instance;
        }

        public bool runtimeMode = false;

        public float soundVolume = 1f;

        public ACTSystemData SystemData ;//= new ACTSystemData();

        public VisualFXQuality CurVisualQuality = VisualFXQuality.H;

        public ACTObjectPool ObjectPool;

        public IACTSoundPlayer SoundPlayer = new ACTSimpleSoundPlayer();

        public IACTLogger Logger = new ACTSimpleLogger();

        private static ACTSystemActorManager m_actorManager;
        public static ACTSystemActorManager actorManager
        {
            get{ return m_actorManager;}
        }

        void Awake()
        {
            //Debug.LogError("-----------------ACTSystemDriver awake----------------");
            s_instance = this;
            ObjectPool = new ACTObjectPool(this);
            m_actorManager = new ACTSystemActorManager();
        }

        void Start()
        {
            if (SystemData != null) SystemData.ReInit();
        }

        public void RegisterUpdate(ACTEventListener.VoidDelegate func)
        {
            ACTEventListener.Get(this.gameObject).onUpdate += func;
        }

        public void UnregisterUpdate(ACTEventListener.VoidDelegate func)
        {
            ACTEventListener.Get(this.gameObject).onUpdate -= func;
        }

        public ACTActor CreateActor(int id)
        {
            return m_actorManager.CreateActor(id);
        }

        public ACTActor CreateActor(GameObject actorModel, int id)
        {
            return m_actorManager.CreateActor(actorModel, id);
        }

        public void DestroyActor(int id)
        {
            m_actorManager.DestroyActor(id);
        }

        public void OnActorDestroy(int id)
        {
            m_actorManager.OnActorDestroy(id);
        }

        public ACTActor GetActor(int id)
        {
            return m_actorManager.GetActor(id);
        }

        public ACTDoSkill PlayAction(ACTActor actor, int actionID)
        {
            var actData = ACTRunTimeData.GetSkillData(actionID);
            return new ACTDoSkill(actor, actData);
        }


    }
}
