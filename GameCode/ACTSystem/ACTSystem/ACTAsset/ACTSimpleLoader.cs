﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public class ACTSimpleLoader : IACTAssetLoader
    {
        string GetResourcePath(string strPath)
        {
            strPath = strPath.Replace("Assets/Resources/", "");
            strPath = Path.Combine(Path.GetDirectoryName(strPath), Path.GetFileNameWithoutExtension(strPath));
            return strPath;
        }

        public void GetObject(string strPath, ACT_VOID_OBJ callback)
        {
            var obj = Resources.Load(GetResourcePath(strPath), typeof(UnityEngine.Object));
            if (callback != null) callback(obj);
        }

        public void GetObjects(string[] strNames, ACT_VOID_OBJS callback)
        {
            UnityEngine.Object[] objs = new UnityEngine.Object[strNames.Length];
            for (int i = 0; i < objs.Length; i++)
            {
                objs[i] = Resources.Load(GetResourcePath(strNames[i]), typeof(UnityEngine.Object));
            }
            if (callback != null) callback(objs);
        }

        public void GetGameObject(string strPath, ACT_VOID_OBJ callback)
        {
            var prefab = Resources.Load(GetResourcePath(strPath), typeof(GameObject));
            if (prefab == null) ACTLogger.Error(strPath + " -> is null !!!!");
            var gameObject = UnityEngine.Object.Instantiate(prefab) as GameObject;
            if (callback!=null) callback(gameObject);
        }

        public void GetGameObjects(string[] strNames, ACT_VOID_OBJS callback) 
        {
            GameObject[] gameObjects = new GameObject[strNames.Length];
            for (int i = 0; i < strNames.Length; i++)
            {
                string strPath = GetResourcePath(strNames[i]);
                var prefab = Resources.Load(strPath, typeof(GameObject));
                if (prefab == null) ACTLogger.Error(strPath + " -> is null !!!!");
                gameObjects[i] = UnityEngine.Object.Instantiate(prefab) as GameObject;
            }
            if (callback != null) callback(gameObjects);
        }

        public void PreloadAsset(string[] strPaths, ACT_VOID callback)
        {
            if (callback != null) callback();
        }

        public void ReleaseAsset(string strPath) { }

        public void ReleaseAssets(string[] strPaths) { }

        public string GetResMapping(string resourceName)
        {
            return resourceName;
        }

        public string[] GetResMappings(string[] resourcesName)
        {
            return resourcesName;
        }
    }
}
