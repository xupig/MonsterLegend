﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public class ACTSimpleSoundPlayer:IACTSoundPlayer
    {
        public void Play(AudioSource source, string strPath, float volume, bool loop = false)
        {
            strPath = strPath.Replace("Assets/Resources/", "");
            strPath = Path.Combine(Path.GetDirectoryName(strPath), Path.GetFileNameWithoutExtension(strPath));
            AudioClip audioClip = Resources.Load(strPath, typeof(AudioClip)) as AudioClip;
            if (audioClip == null) {
                ACTLogger.Error("AudioClip not found:" + strPath);
                return;
            }
            source.clip = audioClip;
            source.volume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume;
            source.loop = loop;
            source.Play();
        }
    }
}
