﻿using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public class ACTDataEquipmentFXList : ScriptableObject
    {
        public List<ACTDataEquipmentFX> m_dataList = new List<ACTDataEquipmentFX>();
        private Dictionary<int, ACTDataEquipmentFX> m_dataDict = new Dictionary<int, ACTDataEquipmentFX>();
        public ACTDataEquipmentFXList()
        {

        }

        public void ReInitDict()
        {
            m_dataDict.Clear();
            for (int i = 0; i < m_dataList.Count; i++)
            {
                var data = m_dataList[i];
                //data.SetDirty();
                m_dataDict.Add(data.ID, data);
            }
        }

        public void CreateACTDataEquipmentFX(int id)
        {
            if (m_dataDict.ContainsKey(id)) return;
            var data = new ACTDataEquipmentFX();
            data.ID = id;
            m_dataDict.Add(data.ID, data);
            m_dataList.Add(data);
        }

        public void RemoveACTDataEquipmentFX(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return;
            var data = m_dataDict[id];
            m_dataDict.Remove(data.ID);
            m_dataList.Remove(data);
        }

        public ACTDataEquipmentFX LoadOrCreate(int id)
        {
            if (!m_dataDict.ContainsKey(id))
            {
                CreateACTDataEquipmentFX(id);
            }
            return m_dataDict[id];
        }

        public ACTDataEquipmentFX GetEquipmentFXData(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return null;
            return m_dataDict[id];
        }

        public Dictionary<int, ACTDataEquipmentFX> GetEquipmentFXDict()
        {
            return m_dataDict;
        }

        public void SetEquipmentFXDict(Dictionary<int, ACTDataEquipmentFX> dict)
        {
            m_dataDict = dict;
        }

        public bool IsEmpty()
        {
            return m_dataList.Count == 0;
        }
    }



}
