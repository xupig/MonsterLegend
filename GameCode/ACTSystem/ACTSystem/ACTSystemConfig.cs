﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;


namespace ACTSystem
{
    public delegate void ACT_VOID();
    public delegate void ACT_VOID_OBJ(UnityEngine.Object obj);
    public delegate void ACT_VOID_OBJS(UnityEngine.Object[] objs);

    [System.Serializable]
    public enum VisualFXQuality
    {
        L,
        M,
        H
    }

    public enum VisualFXPriority
    {
        L,
        H
    }
}
