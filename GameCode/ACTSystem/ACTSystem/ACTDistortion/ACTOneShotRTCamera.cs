﻿using UnityEngine;
using System.Collections;
using ShaderUtils;

namespace ACTSystem
{
    public class ACTOneShotRTCamera : MonoBehaviour
    {

        public static ACTOneShotRTCamera CreateOneShotRTCamera(Camera targetCamera, int cullingMask = 0, int width = 1024, int height = 1024, int depth = 16, int antiAliasing = 1)
        {
            GameObject go = new GameObject("OneShotRTCamera");
            go.transform.SetParent(targetCamera.transform, false);
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            ACTOneShotRTCamera osrtc = go.AddComponent<ACTOneShotRTCamera>();
            osrtc.CreateCamera(targetCamera, cullingMask);
            return osrtc;
        }

        Camera _targetCam;
        Camera _curCam;
        
        void Awake()
        {
        }

        void OnDestroy()
        {
            Clear();
        }

        void Clear()
        {
            if (_curCam.targetTexture)
            {
                Destroy(_curCam.targetTexture);
                _curCam.targetTexture = null;
            }
        }

        public RenderTexture OneShot()
        {
            _curCam.fieldOfView = _targetCam.fieldOfView;
            _curCam.farClipPlane = _targetCam.farClipPlane;
            _curCam.nearClipPlane = _targetCam.nearClipPlane;
            _curCam.Render();
            return _curCam.targetTexture;
        }

        public void CreateCamera(Camera targetCamera, int cullingMask = 0, int width = 1024, int height = 1024, int depth = 16, int antiAliasing = 1)
        {
            if (_curCam == null)
            {
                _curCam = this.gameObject.AddComponent<Camera>();
            }
            _targetCam = targetCamera;
            _curCam.cullingMask = cullingMask > 0 ? cullingMask : targetCamera.cullingMask;
            _curCam.enabled = false;
            _curCam.targetTexture = CreateRenderTexture(width, height, depth, antiAliasing);
            _curCam.Render();
        }

        RenderTexture CreateRenderTexture(int width = 1024, int height = 1024, int depth = 16, int antiAliasing = 1)
        {
            Clear();
            var renderTexture = new RenderTexture(width, height, depth, RenderTextureFormat.Default);
            renderTexture.antiAliasing = antiAliasing;
            bool result = renderTexture.Create();
            return renderTexture;
        }
    }
}
