﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public class ACTSimpleRTLoader : IRenderTextureLoader
    {
        private ACTOneShotRTCamera _oneShotRTCamera;
        public RenderTexture LoadRenderTexture()
        {
            if (_oneShotRTCamera == null)
            {
                int cullingMask = 0;
                _oneShotRTCamera = ACTOneShotRTCamera.CreateOneShotRTCamera(Camera.current, cullingMask);
            }
            return _oneShotRTCamera.OneShot();
        }
    }
}
