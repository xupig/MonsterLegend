﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    static public class ACTAnimationActionID
    {

        static public int GET_UP = 43;

        static public int DIE_HIT_AIR = 46;
        static public int DIE_BLOW_AIR = 47;
        static public int CRASH_WALL = 48;
        static public int DISSOLVE = 70;
        static public int SHOW_LOSE = 74;
        static public int SHOW_WIN = 78;
        static public int BLOW_AIR = 89;
        static public int RIDE_UP = 103;
        static public int RIDE_IDLE = 90;
        static public int RIDE_DOWN = 91;
        static public int DIE_FRONT = 92;
        static public int RETURN_READY = 101;
        static public int RETURN_READY_UPPER = 102;
        static public int RETURN_IDLE_COMMON = 41;
        static public int RETURN_READY_COMMON = 42;
        static public int FLY_UP = 84;
        static public int FLY_DOWN = 87;
    }

    [RequireComponent(typeof(ACTActor))]
    [RequireComponent(typeof(Animator))]
    public class ACTAnimationController
    {
        public static readonly string readyName = "ready";
        public static readonly string idleName = "idle";
        public static readonly string fly_idleName = "fly_idle";
        public static readonly string rideidleName = "rideidle";
        public static readonly string ready_damagedName = "ready_damaged";
        public static readonly string CrashWallName = "CrashWall";
        public static readonly string SpeedName = "Speed";
        public static readonly string SpeedXName = "SpeedX";
        public static readonly string Hit_airName = "Hit_air";
        public static readonly string OnGroundName = "OnGround";
        public static readonly string ReadyName = "Ready";
        public static readonly string InAirName = "InAir";
        public static readonly string ride_typeName = "ride_type";
        public static readonly string ActionName = "Action";
        public static readonly string DamagedName = "Damaged";

        private Dictionary<int, string> m_actionIDNameMap = new Dictionary<int, string>();

        private bool _enable = true;
        public bool enable
        {
            get { return _enable; }
            set
            {
                _enable = value;
            }
        }

        public float KnockOutTime = 0;
        public float KnockOutKeepTime = 2f;
        public int LastAction = 0;

        bool _haveRideType = false;

        private float m_lastSpeed;
        private float m_lastSpeedX;
        private bool m_lastOnGround;
        private bool m_lastReady;
        private bool m_lastInAir;
        private int m_lastRideType;

        private Animator _animator;
        public Animator animator
        {
            get { return _animator; }
        }

        private HashSet<string> paramSet = new HashSet<string>();

        private ACTActor _actor;
        private int _pauseFrame;

        public ACTAnimationController(ACTActor actor)
        {
            _actor = actor;

            _animator = _actor.gameObject.GetComponent<Animator>();
            _animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
        }

        public void Reset()
        {
            enable = true;
            ReturnIdle();
        }

        bool AnimatorHasInitialized()
        {
            return _animator.enabled && _animator.runtimeAnimatorController != null;
        }

        private Queue<int> m_actionQueue = new Queue<int>();
        private Queue<Action> m_actionCallbackQueue = new Queue<Action>();
        public void PlayAction(int action, Action callback = null)
        {
            if (!enable) return;
            LastAction = action;
            if (m_actionIDNameMap.ContainsKey(action))
            {
                PlayActionByName(m_actionIDNameMap[action]);
            }
            else
            {
                m_actionQueue.Enqueue(action);
                m_actionCallbackQueue.Enqueue(callback);
            }
        }

        void ClearActionQueue()
        {
            m_actionQueue.Clear();
            m_actionCallbackQueue.Clear();
        }

        public void SetApplyRootMotion(bool state)
        {
            this._animator.applyRootMotion = state;
        }

        public void PlayActionByName(string name, int layer = -1, float normalizedTime = 0f)
        {
            _animator.Play(name, layer, normalizedTime);
        }

        public void PlayAnimationFX(ACTEventAnimationFX data)
        {
            PlayAction(data.ActionID);
        }

        public void GetUp()
        {
            PlayAction(ACTAnimationActionID.GET_UP);
        }

        public void CrashWall()
        {
            _animator.SetTrigger(CrashWallName);
        }

        //复位
        public void ReturnReady()
        {
            if (_actor.actorState.OnRiding) return;
            if (_actor.inSafeArea)
            {
                _actor.actorState.IsReady = false;
            }
            else
            {
                _actor.actorState.IsReady = true;
            }
            PlayAction(ACTAnimationActionID.RETURN_READY);
        }

        public void ReturnIdle()
        {
            if (_actor.actorState.OnRiding) return;
            _actor.actorState.IsReady = false;
            PlayAction(ACTAnimationActionID.RETURN_IDLE_COMMON);
        }

        public void IdleToReady()
        {
            if (_actor.actorState.OnRiding) return;
            _actor.actorState.IsReady = true;
            PlayAction(ACTAnimationActionID.RETURN_READY_COMMON);
        }

        public void ReadyToIdle()
        {
            if (_actor.actorState.OnRiding) return;
            _actor.actorState.IsReady = false;
            PlayAction(ACTAnimationActionID.RETURN_IDLE_COMMON);
        }
        //upper_body层
        public void IdleToReadyUpper()
        {
            PlayAction(ACTAnimationActionID.RETURN_READY_UPPER);
        }

        public void Die()
        {
            if (!enable) return;
            ClearActionQueue();
            PlayAction(ACTAnimationActionID.DIE_FRONT);
            enable = false;
        }

        public void Relive()
        {
            enable = true;
            ReturnReady();
        }

        public void Ghost()
        {
            enable = true;
            ReturnReady();
        }

        public void Dissolve()
        {
            PlayAction(ACTAnimationActionID.DISSOLVE);
        }

        public void ShowWin()
        {
            PlayAction(ACTAnimationActionID.SHOW_WIN);
        }

        public void ShowLose()
        {
            PlayAction(ACTAnimationActionID.SHOW_LOSE);
        }

        public bool IsReady()
        {
            AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            return stateInfo.IsName(readyName) ||
                   stateInfo.IsName(idleName) ||
                   stateInfo.IsName(ready_damagedName);
        }

        public bool IsInState(string name)
        {
            AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            return stateInfo.IsName(name);
        }

        public AnimatorStateInfo GetCurrentState()
        {
            return _animator.GetCurrentAnimatorStateInfo(0);
        }

        public void SetCullingMode(AnimatorCullingMode cullingMode)
        {
            if (_animator != null)
            {
                _animator.cullingMode = cullingMode;
            }
        }

        public float GetSpeed()
        {
            return m_lastSpeed;
        }

        public float GetSpeedX()
        {
            return m_lastSpeedX;
        }

        private void UpdateAnimatorState()
        {
            if (ACTMathUtils.Approximately(m_lastSpeed, _actor.actorState.forwardMovingSpeedTransition))
            {
                _animator.SetFloat(SpeedName, _actor.actorState.forwardMovingSpeedTransition);
                m_lastSpeed = _actor.actorState.forwardMovingSpeedTransition;
            }
            if (ACTMathUtils.Approximately(m_lastSpeedX, _actor.actorState.rightMovingSpeedTransition))
            {
                _animator.SetFloat(SpeedXName, _actor.actorState.rightMovingSpeedTransition);
                m_lastSpeedX = _actor.actorState.rightMovingSpeedTransition;
            }
            AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            _actor.actorState.CurAnimationState = stateInfo;
            if (!_actor.actorState.OnGround)
            {
                //_animator.SetFloat("Blow_air", _actor.actorState.OnHitAir); //m_actor.transform.position.y - m_lastY > 0 ? 0f : 1f);
                _animator.SetFloat(Hit_airName, _actor.actorState.OnHitAir);
            }
            if (m_lastOnGround != _actor.actorState.OnGround)
            {
                _animator.SetBool(OnGroundName, _actor.actorState.OnGround);
                m_lastOnGround = _actor.actorState.OnGround;
            }
            if (m_lastReady != _actor.actorState.IsReady)
            {
                _animator.SetBool(ReadyName, _actor.actorState.IsReady);
                m_lastReady = _actor.actorState.IsReady;
            }
            if (m_lastInAir != _actor.actorState.InAir)
            {
                _animator.SetBool(InAirName, _actor.actorState.InAir);
                m_lastInAir = _actor.actorState.InAir;
            }

            //if (_animator.GetFloat(SpeedName) > _actor.FootStepSpeed && _actor.IsUseFootSound)
            //FootStep.Update(_actor);

            if (_haveRideType)
            {
                if (m_lastRideType != _actor.actorState.RideType)
                {
                    _animator.SetInteger(ride_typeName, _actor.actorState.RideType);
                    m_lastRideType = _actor.actorState.RideType;
                }
            }
        }

        private void UpdateAnimatorAction()
        {
            if (_animator.IsInTransition(0))
            {
                return;
            }
            if (m_actionQueue.Count > 0)
            {
                var action = m_actionQueue.Dequeue();
                _animator.SetInteger(ActionName, action);
                Action callback = m_actionCallbackQueue.Dequeue();
                if (callback != null) callback();
            }
            else
            {
                _animator.SetInteger(ActionName, 0);
            }
        }

        private void OnActionChanged(int action)
        {

            if (action == ACTAnimationActionID.RIDE_UP)
            {
                _actor.animationController.SetRidingState(true);
            }
        }

        private void UpdatePause()
        {
            if (_pauseFrame > 0)
            {
                _pauseFrame--;
                if (_pauseFrame <= 0)
                {
                    Resume();
                }
            }
        }

        private bool IsParamExist(string name)
        {
            return paramSet.Contains(name);
        }

        bool init = false;
        public void Update()
        {
            if (!AnimatorHasInitialized()) return;
            if (!init)
            {
                init = true;
                paramSet.Clear();
                for (int i = 0; i < _animator.parameters.Length; i++)
                {
                    paramSet.Add(_animator.parameters[i].name);
                }
                m_lastSpeed = _animator.GetFloat(SpeedName);
                m_lastSpeedX = _animator.GetFloat(SpeedXName);
                m_lastOnGround = _animator.GetBool(OnGroundName);
                m_lastReady = _animator.GetBool(ReadyName);
                m_lastInAir = _animator.GetBool(InAirName);
                if (_haveRideType)
                    m_lastRideType = _animator.GetInteger(ride_typeName);
            }
            UpdateAnimatorState();
            UpdateAnimatorAction();
            UpdatePause();
        }

        public void ChangeReadyDamagedState(bool isDamaged)
        {
            if (!AnimatorHasInitialized()) return;
            if (_animator.GetBool(DamagedName))
            {
                _animator.SetBool(DamagedName, isDamaged);
            }
            _actor.actorState.Damaged = isDamaged;
        }

        public void SetOnFlying(bool state)
        {
            /*//暂时没有飞行动作的需求，先屏蔽掉
            if (!AnimatorHasInitialized()) return;
            if (_animator.GetBool("OnFlying"))
            {
                _animator.SetBool("OnFlying", state);
            }
            _actor.actorState.OnFlying = state;*/
        }

        public void PlayRidingAction(Action callback = null)
        {
            if (!AnimatorHasInitialized()) return;
            PlayAction(ACTAnimationActionID.RIDE_UP, callback);
        }

        public void SetRidingState(bool state)
        {
            _actor.actorState.OnRiding = state;
            //_animator.SetBool("OnRiding", state);
            if (state)
            {
                _actor.actorState.IsReady = false;
                PlayAction(ACTAnimationActionID.RIDE_UP);
            }
            else
            {
                ReturnIdle();
            }
        }

        public void SetRidingType(int rideType)
        {
            _actor.actorState.RideType = rideType;
            _animator.SetInteger(ride_typeName, rideType);
            m_lastRideType = rideType;
            _haveRideType = true;
            if (rideType > 0)
            {

                //PlayActionByName("ride_ready");
            }
        }

        public void PauseTo(int frame)
        {
            if (frame > 0)
            {
                _pauseFrame = frame;
                Pause();
            }
            else
            {
                Resume();
            }
        }

        public void Pause()
        {
            if (_animator != null)
            {
                _animator.speed = 0;
            }
        }

        public void Resume()
        {
            if (_animator != null)
            {
                _animator.speed = 1;
            }
        }

        public void SetReadySpeed(float s)
        {
            if (_animator != null)
            {
                //_animator.SetFloat("PlaySpeed", s);
            }
        }
    }
}
