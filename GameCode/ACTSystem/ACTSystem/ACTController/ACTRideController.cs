﻿using UnityEngine;

namespace ACTSystem
{
    public class ACTRideController
    {
        private bool _switch = false;
        private ACTActor _actor;

        public ACTRideController(ACTActor actor)
        {
            _actor = actor;
        }

        private float _slotCameraOffset = 0;
        public void Mount(ACTActor ride, bool playAction = true, bool inCity = true)
        {
            ride.SetLayer(ACTLayerUtils.Actor);
            ride.actorController.usePlayerController = true;
            Vector3 position = _actor.position;
            Quaternion rotation = _actor.rotation;

            Transform rideSlot = ride.boneController.GetBoneByName("bip_ride");
            if (rideSlot == null)
            {
                ACTLogger.Error(string.Format("坐骑模型出错，坐骑模型id = {0}没有ride_slot挂载点。", ride.actorData.ID));
                return;
            }
            _actor.transform.SetParent(rideSlot, true);
            _actor.transform.localPosition = Vector3.zero;
            _actor.transform.localEulerAngles = new Vector3(90, 0, 0);

            _actor.equipController.PackUpWeapon(true);
            _actor.actorController.Stop();
            _actor.animationController.SetRidingState(true);
            _actor.animationController.SetRidingType(ride.actorData.RideType);
            _actor.actorController.Stop();
            _actor.rideId = ride.ID;
            ride.rideOwnerId = _actor.ID;
            ride.position = position;
            ride.rotation = rotation;
            var slotCamera = _actor.boneController.GetBoneByName(ACTBoneController.SLOT_CAMERA_NAME);
            Vector3 slotCameraPosition = slotCamera.localPosition;
            if (_slotCameraOffset > 0)
            {
                slotCameraPosition.y += _slotCameraOffset;
            }
            _slotCameraOffset = rideSlot.position.y - ride.position.y;
            slotCameraPosition.y -= _slotCameraOffset;
            slotCamera.localPosition = slotCameraPosition;
            //var slotBillboard = _actor.boneController.GetBoneByName(ACTBoneController.SLOT_BILLBOARD_NAME);
            //ride.boneController.ResetBillboard(_slotCameraOffset + slotBillboard.localPosition.y * 0.8f);
            _switch = true;
        }

        public void Dismount(bool inCity = true)
        {
            _switch = false;
            _actor.transform.SetParent(null, true);
            Vector3 position = _actor.ride.position;
            Quaternion rotation = _actor.ride.rotation;
            _actor.actorController.SetControllerEnabled(true);
            _actor.ride.rideOwnerId = 0;
            _actor.rideId = 0;
            _actor.animationController.SetRidingState(false);
            _actor.animationController.SetRidingType(0);
            _actor.animationController.ReturnReady();
            if (_actor.inSafeArea)
            {
                _actor.equipController.PackUpWeapon(true);

            }
            else
            {
                _actor.equipController.PackUpWeapon(false);
            }
            _actor.position = position;
            _actor.rotation = rotation;
            Vector3 slotCameraPosition = _actor.boneController.GetBoneByName("slot_camera").localPosition;
            slotCameraPosition.y += _slotCameraOffset;
            _slotCameraOffset = 0;
            _actor.boneController.GetBoneByName("slot_camera").localPosition = slotCameraPosition;
        }

        public void Update()
        {
            if (!_switch)
            {
                return;
            }
            if (_actor.animationController.IsInState(ACTAnimationController.rideidleName))
            {
                if (_actor.ride != null)
                {
                    _actor.animationController.PlayActionByName(ACTAnimationController.rideidleName);
                    string rideActionName = _actor.isFlyingOnRide ? ACTAnimationController.fly_idleName : ACTAnimationController.readyName;
                    _actor.ride.animationController.PlayActionByName(rideActionName);
                }
                _switch = false;
            }
        }

        public void Fly(bool modifyLayer)
        {
            _actor.SetIsFlyingOnRide(true, modifyLayer);
        }

        public void Land()
        {
            _actor.SetIsFlyingOnRide(false);
        }
    }
}
