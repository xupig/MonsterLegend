﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTMaterialAnimationController
    {

        private ACTActor _actor;
        private int _pauseFrame;
        private List<ACTMaterialAnim> _anims = null; 

        public ACTMaterialAnimationController(ACTActor actor)
        {
            _actor = actor;           
        }

        public void Reset()
        {
            if (_anims == null)return;
            for (int i = 0; i < _anims.Count; i++)
            {
                _anims[i].Reset();
            }
        }

        public void PlayMaterialAnimation(ACTActorMaterials actorMaterials, ACTEventChangeMaterial changeMaterial)
        {
            if (_anims == null) _anims = new List<ACTMaterialAnim>();
            var materialList = actorMaterials.MaterialList;
            for (int i = 0; i < materialList.Count; i++)
            {
                var materialData = materialList[i];
                var slotTrans = this._actor.boneController.GetBoneByName(materialData.meshSlot);
                if (slotTrans != null)
                {
                    //主角模型兼容换装，以meshSlot和mesh索引MaterialPath
                    if (!string.IsNullOrEmpty(materialData.meshPath))
                    {
                        SkinnedMeshRenderer smr = slotTrans.GetComponent<SkinnedMeshRenderer>();
                        if (smr != null)
                        {
                            string[] names = materialData.meshPath.Split('/');
                            string meshName = names[names.Length - 1].Split('.')[0];
                            if (smr.sharedMesh.name == meshName)
                            {
                                GameObject go = slotTrans.gameObject;
                                var ani = go.GetComponent<ACTMaterialAnim>();
                                if (ani == null)
                                {
                                    ani = go.AddComponent<ACTMaterialAnim>();
                                    _anims.Add(ani);
                                }
                                ani.Play(materialData.MaterialPath, changeMaterial);
                            }
                        }
                        MeshFilter mr = slotTrans.GetComponent<MeshFilter>();
                        if (mr == null)
                        {
                            mr = slotTrans.GetComponentInChildren<MeshFilter>();
                            if (mr != null)
                            {         
                                string[] names = materialData.meshPath.Split('/');
                                string meshName = names[names.Length - 1].Split('.')[0];
                                if (mr.sharedMesh.name == meshName)
                                {
                                    GameObject go = slotTrans.gameObject;
                                    var ani = go.GetComponent<ACTMaterialAnim>();
                                    if (ani == null)
                                    {
                                        ani = go.AddComponent<ACTMaterialAnim>();
                                        _anims.Add(ani);
                                    }
                                    ani.Play(materialData.MaterialPath, changeMaterial);
                                }
                            }
                        }
                    }
                    else
                    {
                        //普通模型不变装不要配置meshPath
                        GameObject go = slotTrans.gameObject;
                        var ani = go.GetComponent<ACTMaterialAnim>();
                        if (ani == null)
                        {
                            ani = go.AddComponent<ACTMaterialAnim>();
                            _anims.Add(ani);
                        }
                        ani.Play(materialData.MaterialPath, changeMaterial);
                    }
                }
            }
        }

        public void PlayMaterialAnimation(ACTEventChangeMaterial changeMaterial)
        {
            if (changeMaterial.MaterialID == 0)
            {
                this.Reset();
            }
            else
            {
                var materialsList = this._actor.actorData.MaterialsList;
                for (int i = 0; i < materialsList.Count; i++)
                {
                    var materials = materialsList[i];
                    if (materials.ID == changeMaterial.MaterialID)
                    {
                        PlayMaterialAnimation(materials, changeMaterial);
                        break;
                    }
                }
            }
        }
    }
}
