﻿#if UNUSED
using System;
using System.Collections.Generic;
using System.Text;
using RootMotion.FinalIK;
using UnityEngine;

namespace ACTSystem
{
    public enum ACTIKPositionType
    { 
        SHOULDER_LEFT,
        SHOULDER_RIGHT,
        THIGH_LEFT,
        THIGH_RIGHT
    }

    class ACTIKController:MonoBehaviour
    {
        private FullBodyBipedIK m_targetIK = null;

        private IKEffector m_leftShoulderEffector;
        private IKEffector m_rightShoulderEffector;
        private IKEffector m_leftThighEffector;
        private IKEffector m_rightThighEffector;

        private Dictionary<ACTIKPositionType, IKEffector> m_ikEffectors;
        private ACTIKPositionType[] m_fastList;
        private float m_positionWeightReturnSpeed = 0.03f;

        private bool m_isRefreshing = false;

        void Awake()
        { 
            
        }

        void Start()
        {
            if (m_targetIK == null)
            {
                Init();
            }
        }

        void Init()
        { 
            m_targetIK = gameObject.GetComponent<FullBodyBipedIK>();
            if (m_targetIK)
            {
                m_leftShoulderEffector = m_targetIK.solver.leftShoulderEffector;
                m_rightShoulderEffector = m_targetIK.solver.rightShoulderEffector;
                m_leftThighEffector = m_targetIK.solver.leftThighEffector;
                m_rightThighEffector = m_targetIK.solver.rightThighEffector;
                m_ikEffectors = new Dictionary<ACTIKPositionType, IKEffector>();
                m_ikEffectors.Add(ACTIKPositionType.SHOULDER_LEFT, m_leftShoulderEffector);
                m_ikEffectors.Add(ACTIKPositionType.SHOULDER_RIGHT, m_rightShoulderEffector);
                m_ikEffectors.Add(ACTIKPositionType.THIGH_LEFT, m_leftThighEffector);
                m_ikEffectors.Add(ACTIKPositionType.THIGH_RIGHT, m_rightThighEffector);
                //m_ikEffectorList = new IKEffector[4] { m_leftShoulderEffector, 
                //                                        m_rightShoulderEffector, 
                //                                        m_leftThighEffector, 
                //                                        m_rightThighEffector };
                m_fastList = new ACTIKPositionType[4] { ACTIKPositionType.SHOULDER_LEFT, ACTIKPositionType.SHOULDER_RIGHT,
                                                     ACTIKPositionType.THIGH_LEFT, ACTIKPositionType.THIGH_RIGHT};
            }
        }

        private float m_endTime = 0;

        void Update()
        {
            if (!m_isRefreshing) return;
            if (m_targetIK == null) return;

            bool flag = false;
            for (int i = 0; i < m_fastList.Length; i++)
            {
                var effector = m_ikEffectors[m_fastList[i]];
                if (effector.positionWeight != 0)
                {
                    effector.positionWeight = Mathf.Clamp(effector.positionWeight - m_positionWeightReturnSpeed, 0f, 1f);
                    if (effector.positionWeight == 0)
                    {
                        effector.position = effector.bone.position;
                    }
                    flag = true;
                }
            }
            if (!flag)
            {
                m_isRefreshing = false;
                m_targetIK.solver.IKPositionWeight = 0;
                m_targetIK.enabled = false;
            }
        }

        public void Push(ACTIKPositionType positionType, Vector3 force)
        {
            if (m_targetIK == null) return;
            m_targetIK.enabled = true;
            m_targetIK.solver.IKPositionWeight = 1f;
            m_ikEffectors[positionType].positionWeight = 1f;
            m_ikEffectors[positionType].position = m_ikEffectors[positionType].bone.position;
            m_ikEffectors[positionType].position = m_ikEffectors[positionType].position + force;
            m_isRefreshing = true;
        }

        

    }
}
#endif