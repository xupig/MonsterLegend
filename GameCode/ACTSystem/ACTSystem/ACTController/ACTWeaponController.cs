﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ACTSystem
{
    public class ACTWeaponController
    {
        public ACTActor _actor;

        private Dictionary<int, ACTWeapon> _weapons;
        private Dictionary<int, MeleeWeaponTrail> _meleeWeaponTrails;

        public ACTWeaponController(ACTActor actor)
        {
            _actor = actor;
        }

        private bool _initedWeapons = false;
        private void InitWeapons()
        {
            if (_initedWeapons) return;
            _initedWeapons = true;
            var weaponTags = _actor.GetComponentsInChildren<ACTTagWeapon>();
            if (weaponTags.Length != 0) { _weapons = new Dictionary<int, ACTWeapon>(); }
            for (int i = 0; i < weaponTags.Length; i++)
            {
                var weaponObj = weaponTags[i].gameObject;
                if (weaponObj.GetComponent<ACTWeapon>() != null) continue;
                var weapon = weaponObj.AddComponent<ACTWeapon>();
                weapon.WeaponID = weaponTags[i].WeaponID;
                _weapons[weapon.WeaponID] = weapon;
                var trail = weaponObj.GetComponent<MeleeWeaponTrail>();
                if (trail)
                {
                    trail = weaponObj.GetComponentInChildren<MeleeWeaponTrail>();
                }        
                if (trail) {
                    if (_meleeWeaponTrails == null) { _meleeWeaponTrails = new Dictionary<int, MeleeWeaponTrail>(); }
                    _meleeWeaponTrails[weapon.WeaponID] = trail; 
                }
            }
        }

        public ACTWeapon GetWeapon(int weaponID)
        {
            InitWeapons();
            if (_weapons != null && _weapons.ContainsKey(weaponID)) 
            { 
                return _weapons[weaponID]; 
            }
            return null;
        }

        public Dictionary<int, ACTWeapon> GetWeapons()
        {
            InitWeapons();
            return _weapons;
        }

        public void SetWeaponTrailEmit(int weaponID, bool state)
        {
            InitWeapons();
            if (_meleeWeaponTrails == null) return;
            if (_meleeWeaponTrails.ContainsKey(weaponID))
            {
                _meleeWeaponTrails[weaponID].Emit = state;
            }
        }

        public void ClearAllWeaponTrailEmit()
        {
            if (_meleeWeaponTrails == null) return;
            foreach (var weaponTrails in _meleeWeaponTrails)
            {
                weaponTrails.Value.Emit = false;
            } 
        }
    }
}
