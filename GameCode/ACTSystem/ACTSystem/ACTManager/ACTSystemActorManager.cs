﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    public class ACTSystemActorManager 
    {

        static int s_curMaxID = 0;
        static int CreateNewID()
        {
            return ++s_curMaxID;
        }

        private Dictionary<int, ACTActor> m_actors = new Dictionary<int, ACTActor>();
        public Dictionary<int, ACTActor> actors
        {
            get { return m_actors; }
        }

        public ACTActor CreateActor(int id)
        {
            var actorData = ACTRunTimeData.GetActorData(id);
            var actorModel = UnityEngine.Object.Instantiate(actorData.Model) as GameObject;
            actorModel.layer = ACTLayerUtils.Actor;
            var actor = actorModel.AddComponent<ACTActor>();
            actor.ID = CreateNewID();
            actor.actorData = actorData;
            m_actors.Add(actor.ID, actor);
            return actor;
        }

        public void DestroyActor(int id)
        {
            if (m_actors.ContainsKey(id))
            {
                var actor = m_actors[id];
                UnityEngine.Object.Destroy(actor.gameObject);
            }
        }

        public void OnActorDestroy(int id)
        {
            m_actors.Remove(id);
        }

        public ACTActor GetActor(int id)
        {
            if (m_actors.ContainsKey(id))
            {
                return m_actors[id];
            }
            return null;
        }

        public ACTActor CreateActor(GameObject actorModel, int id)
        {
            var actorData = ACTRunTimeData.GetActorData(id);
            actorModel.layer = ACTLayerUtils.Actor;
            var actor = actorModel.AddComponent<ACTActor>();
            actor.ID = CreateNewID();
            actor.actorData = actorData;
            m_actors.Add(actor.ID, actor);
            return actor;
        }
    }
}
