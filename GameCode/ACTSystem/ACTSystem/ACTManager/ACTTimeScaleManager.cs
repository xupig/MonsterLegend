﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTTimeScaleManager
    {
        static float NORMAL_TIME_SCALE = 1f;

        static ACTTimeScaleManager s_instance;
        static public ACTTimeScaleManager GetInstance()
        { 
            if (s_instance == null)
            {
                s_instance = new ACTTimeScaleManager();
            }
            return s_instance;
        }

        //private float fastTime = 0;
        //private float fastTime2 = 0;
        private float endTime;
        private float lastTimeScale = NORMAL_TIME_SCALE;

        public void KeepTimeScale(float scale, float time)
        {
            Time.timeScale = scale;
            lastTimeScale = scale;
            endTime = UnityPropUtils.realtimeSinceStartup + time;
            //fastTime = 0;
            //fastTime2 = 0;
        }

        public void CancelTimeScale()
        {
            Time.timeScale = NORMAL_TIME_SCALE;
        }

        public void OnUpdate()
        {
            //fastTime += Time.fixedDeltaTime;
            //fastTime2 += UnityPropUtils.deltaTime;
            if (lastTimeScale != NORMAL_TIME_SCALE)
            {
                if (UnityPropUtils.realtimeSinceStartup > endTime)
                {
                    Time.timeScale = NORMAL_TIME_SCALE;
                    lastTimeScale = NORMAL_TIME_SCALE;
                }
            }
        }
    }
}
