﻿using System.Collections.Generic;
using UnityEngine;

namespace ACTSystem
{
    public class ACTLayerUtils
    {
        public static int TransparentFX = UnityEngine.LayerMask.NameToLayer("TransparentFX");
        public static int Actor = UnityEngine.LayerMask.NameToLayer("Actor");
        public static int ActorFly = UnityEngine.LayerMask.NameToLayer("ActorFly");
        public static int Terrain = UnityEngine.LayerMask.NameToLayer("Terrain");
        public static int Trigger = UnityEngine.LayerMask.NameToLayer("Trigger");
        public static int Wall = UnityEngine.LayerMask.NameToLayer("Wall");
        public static int Default = UnityEngine.LayerMask.NameToLayer("Default");
        public static int Bloom = UnityEngine.LayerMask.NameToLayer("Bloom");
        public static int UI = UnityEngine.LayerMask.NameToLayer("UI");
    }
}
