﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public enum ACTContinueAttackStatus
    {
        STANDBY,

        ATTACKING

    }

    public class ACTContinuousAttack
    {

        private ACTDataSettingContiAttack _contiAttack;
        private  ACTDataSetting _customSetting;

        private List<ACTDataSettingAction> ActionList = new List<ACTDataSettingAction>();
        /// <summary>
        /// 上次技能时间
        /// </summary>
        private float lastAttackTime = 0.0f;

        private int _attackIndex = 0;
        public int attackIndex
        {
            get
            {
                return _attackIndex;
            }
            set
            {
                _attackIndex = value;
            }
        }
        private ACTContinueAttackStatus _status = ACTContinueAttackStatus.STANDBY;
        public ACTContinueAttackStatus status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                attackIndex = 0;
            }
        }

        public ACTContinuousAttack(ACTDataSettingContiAttack contiAttack, ACTDataSetting customSetting)
        {
            this._contiAttack = contiAttack;
            this._customSetting = customSetting;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.J))
            {
                PlayContinueAttack();
                
            }
            //
            UpdateStatus();


        }

        public void PlayContinueAttack()
        {
            if (status == ACTContinueAttackStatus.STANDBY)
            {
                status = ACTContinueAttackStatus.ATTACKING;
            }
        }

        private void DoAttack()
        {
            if (GetContinueAttackAction() == false) return;
            if(CanCutTime() == false) return;

            PlayAction(ActionList[attackIndex].ActionID);
            //记录上次攻击时间
            lastAttackTime = UnityPropUtils.realtimeSinceStartup;
            attackIndex++;
            if (attackIndex >= ActionList.Count)
            {
                status = ACTContinueAttackStatus.STANDBY;
            }

        }

        private bool CanCutTime()
        {
            if (attackIndex <= 0) return true;

            int skillInterval = (int)((UnityPropUtils.realtimeSinceStartup - lastAttackTime) * 1000);
            //ACTSystemLog.LogError("[ACTContinueAttackStatus:CanCutTime]=>contiAttack.ActionList[" + attackIndex + "].CutTime: " + ActionList[attackIndex].CutTime);
            if (skillInterval < ActionList[attackIndex-1].CutTime)
            {
                return false;
            }
            return true;
        }


        private void UpdateStatus()
        {
            switch (status)
            {
                case ACTContinueAttackStatus.STANDBY:
                    break;
                case ACTContinueAttackStatus.ATTACKING:
                    DoAttack();
                    break;
                default:
                    break;
            }
        }

        public bool GetContinueAttackAction()
        {
            FilterActionList();
            if (_attackIndex >= ActionList.Count)
            {
                _attackIndex = 0;
            }
            return ActionList.Count > 0 ? true : false;
        }

        private void FilterActionList()
        {
            ActionList.Clear();
            for (int i = 0; i < _contiAttack.ActionArr.Length; i++)
            {
                if (_contiAttack.ActionArr[i].Enable == true)
                {
                    ActionList.Add(_contiAttack.ActionArr[i]);
                }
                
            }
        }

        public void PlayAction(int actionID)
        {
            var actData = ACTRunTimeData.GetSkillData(actionID);
            if (_customSetting.Actor != null && actData != null)
            {
                new ACTDoSkill(_customSetting.Actor, actData);
            }
        }

    }
}
