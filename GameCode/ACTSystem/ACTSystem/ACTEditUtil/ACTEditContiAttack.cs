﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    [RequireComponent(typeof(ACTEditDriver))]
    public class ACTEditContiAttack : MonoBehaviour
    {
        public ACTDataSettingContiAttack CustomSettingContiAttack;
        private ACTDataSetting CustomSetting;

        private ACTContinuousAttack _continueAttack;


        void Awake()
        {
            CustomSetting = gameObject.GetComponent<ACTEditDriver>().CustomSetting;
            _continueAttack = new ACTContinuousAttack(CustomSettingContiAttack, CustomSetting);
        }

        void Update()
        {
            _continueAttack.Update();

            
        }

        public void PlayAction()
        {
            _continueAttack.PlayContinueAttack();
        }


        //void OnGUI()
        //{


        //    if (GUI.Button(new Rect(10,10,60,60),"Play"))
        //    {
        //        _continueAttack.PlayContinueAttack();
        //    }
        //}


    }
}
