﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace ACTSystem
{
    [Serializable]
    public class ACTEditRoleInfo
    {
        public Transform role = null;
        private Vector3 positionBak;
        private Quaternion rotationBak;
        public void SaveTransformInfo(string name)
        {
            if (role == null)
            {
                ACTLogger.Error(string.Format("<color=#FF0000>[SaveTransformInfo]=>" + name + "没有指定角色,所以保存不了！请把角色拖上去,不要这么粗心-_-</color>"));
                return;
            }
            positionBak = role.position;
            rotationBak = role.rotation;
            
        }

        public void ReadTransformInfo(string name)
        {
            if (role == null)
            {
                ACTLogger.Error(string.Format("[SaveTransformInfo]=>" + name + "没有指定角色,所以读取不了！请把角色拖上去,不要这么粗心-_-</color>"));
                return;
            }
            role.position = positionBak;
            role.rotation = rotationBak;
            
        }
    }

    public class ACTEditCtrlRoleTransform : MonoBehaviour
    {
        /// <summary>
        /// 攻击者
        /// </summary>
        public ACTEditRoleInfo theAttacker = null;

        /// <summary>
        /// 受害者
        /// </summary>
        public ACTEditRoleInfo theVictime = null;

 

        public void SaveTransformInfo()
        {
            theAttacker.SaveTransformInfo("攻击者");
            theVictime.SaveTransformInfo("受击者");
           
        }

        public void ReadTransformInfo()
        {
            theAttacker.ReadTransformInfo("攻击者");
            theVictime.ReadTransformInfo("受击者");
           
        }

        private void CtrlTransform(Action attackerCallBack, Action victimCallBack)
        {
            attackerCallBack();
            victimCallBack();
            /*
            if (theAttacker == null)
            {
                Debug.LogError("[CtrlTransform]=>攻击者没有制定角色,请把角色拖上去!不要这么粗心-_-");
            }
            else
            {
                attackerCallBack();
            }
            if (theVictime == null)
            {
                Debug.LogError("[CtrlTransform]=>受击者没有制定角色,请把角色拖上去!不要这么粗心-_-");
            }
            else
            {
                victimCallBack();
            }
             */ 
        }


    }
}
