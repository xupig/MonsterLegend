﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using ShaderUtils;

namespace ACTSystem
{
    public class ACTEquipWing : ACTEquipComponent
    {
        private Animator _wingAnimator = null;

        public ACTEquipWing(ACTActor actor)
            : base(actor)
        {
        }

        protected override void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR, string colorG, string colorB)
        {
            if (data.EquipType != ACTEquipmentType.Wing) { ACTLogger.Error("equipID is not a Wing:" + data.ID); }
            _isNormalQuality = isNormalQuality;
            if (curEquipData != null) TakeOff();
            _wingAnimator = null;
            if (string.IsNullOrEmpty(data.PrefabPath))
            {
                ACTLogger.Error("ACTDataEquipment Data error:" + data.ID);
                return;
            }
            curEquipData = data;
            curEquipParticleID = particleID;
            curEquipFlowID = flowID;
            if (_onEquipLoading)
            {
                return;
            }
            _onEquipLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            string prefabPath = (_isNormalQuality || string.IsNullOrEmpty(data.PrefabPath_L)) ? data.PrefabPath : data.PrefabPath_L;
            prefabPath = assetLoader.GetResMapping(prefabPath);
            assetLoader.GetGameObject(prefabPath, delegate(UnityEngine.Object obj)
            {
                _onEquipLoading = false;
                if (_actorIsDestory || curEquipData != data)
                {
                    if (obj) GameObject.Destroy(obj);
                    if (curEquipData != data || _isNormalQuality != isNormalQuality)
                    {
                        if (curEquipData != null)
                        {
                            OnPutOn(curEquipData, curEquipParticleID, curEquipFlowID, _isNormalQuality, colorR, colorG, colorB);
                        }
                    }
                    return;
                }
                var _wingGameObject = obj as GameObject;
                _wingAnimator = _wingGameObject.GetComponent<Animator>();
                OnPutOn(curEquipData.Slot, _wingGameObject);   
                PutOnParticle(curEquipParticleID);
                PutOnFlow(curEquipFlowID);
                if (onLoadEquipFinished != null)
                {
                    onLoadEquipFinished();
                }
            });
        }

        private GameObject _wingGameObject;
        private Transform _slot;
        void OnPutOn(string slotName, GameObject wingGameObject)
        {
            if (string.IsNullOrEmpty(slotName) || wingGameObject != null)
            {
                _slot = _actor.boneController.GetBoneByName(slotName);
                if (_slot == null) return;
                wingGameObject.transform.SetParent(_slot, false);
                wingGameObject.transform.localEulerAngles = new Vector3(90, 0, 0);
                _wingGameObject = wingGameObject;
                _actor.animationController.SetOnFlying(true);
                ACTSystemTools.SetFxTag(_wingGameObject.transform);
                ACTSystemTools.SetLayer(_wingGameObject.transform, _slot.gameObject.layer, true);
                ACTSystemTools.SetFxShow(_wingGameObject.transform, _actor.equipController.canEquipFxShow);
                UpdateSettingType();
            }
        }

        protected override void OnTakeOff()
        {
            if (_wingGameObject != null)
            {
                GameObject.Destroy(_wingGameObject);
            }
            _wingGameObject = null;
            curEquipData = null;
            _wingAnimator = null;
            _actor.animationController.SetOnFlying(false);
        }

        protected override void OnPutOnParticle(ACTDataEquipParticle data)
        {
            if (curEquipParticleData != null) TakeOffParticle();
            curEquipParticleID = data.ID;
            curEquipParticleData = data;
            //LoggerHelper.Info(string.Format("PutOnWingParticle, equipID = {0}, particleID = {1}", _curWingData.ID, data.ID));
            if (curEquipParticleData.PrefabList.Count < 1)
            {
                return;
            }
            if (_onEquipLoading || _onParticleLoading)
            {
                return;
            }
            _onParticleLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            var prefabPath = curEquipParticleData.PrefabList[0].PrefabPath;
            prefabPath = assetLoader.GetResMapping(prefabPath);
            assetLoader.GetGameObject(prefabPath, delegate(UnityEngine.Object obj)
            {
                _onParticleLoading = false;
                if (_actorIsDestory)
                {
                    GameObject.Destroy(obj);
                    return;
                }
                if (curEquipParticleData != data)
                {
                    GameObject.Destroy(obj);
                    if (curEquipParticleData != null)
                    {
                        OnPutOnParticle(curEquipParticleData);
                    }
                    return;
                }
                if (_actorIsDestory) return;
                if (curEquipParticleData != data) return;
                var particleGameObject = obj as GameObject;
                OnPutOnParticle(particleGameObject);
                if (onLoadParticleFinished != null)
                {
                    onLoadParticleFinished();
                }
            });
        }

        GameObject _wingParticleGameObject;
        void OnPutOnParticle(GameObject particleGameObject)
        {
            if (particleGameObject != null && _wingGameObject)
            {
                particleGameObject.transform.SetParent(_wingGameObject.transform, false);
                _wingParticleGameObject = particleGameObject;
            }
        }

        protected override void OnTakeOffParticle()
        {
            if (_wingParticleGameObject != null)
            {
                //LoggerHelper.Info(string.Format("TakeOffWingParticle, equipID = {0}, particleID = {1}", _curWingData.ID, _curWingParticleData.ID));
                GameObject.Destroy(_wingParticleGameObject);
            }
            _wingParticleGameObject = null;
            curEquipParticleData = null;
        }

        protected override void OnPutOnFlow(Material material)
        {
            if (_wingParticleGameObject == null)
            {
                return;
            }
            var ownerSMRenderer = _wingParticleGameObject.transform.GetComponent<MeshRenderer>();
            if (ownerSMRenderer != null)
            {
                ownerSMRenderer.sharedMaterial = null;
                ownerSMRenderer.sharedMaterial = material;
            }
        }

        protected override void OnTakeOffFlow()
        {
            if (_wingParticleGameObject == null)
            {
                return;
            }
            var ownerSMRenderer = _wingParticleGameObject.transform.GetComponent<MeshRenderer>();
            if (ownerSMRenderer != null && ownerSMRenderer.sharedMaterial != null)
            {
                FlowEnableKeyword(ownerSMRenderer.sharedMaterial, false);
            }
        }

        public void UpdateSettingType()
        {
            int quality = (int)ACTSystemDriver.GetInstance().CurVisualQuality;
            if (_wingGameObject == null) return;
            ACTWingQuality.UpdateWingQuality(_wingGameObject);
            ShowWingGameObject(_actor.equipController.canWingShow);
        }

        public void ShowWingGameObject(bool isShow)
        {
            if (_wingGameObject == null) return;
            if (isShow)
            {
                ACTSystemTools.SetLayer(_wingGameObject.transform, _slot.gameObject.layer, true);
            }
            else
            {
                ACTSystemTools.SetLayer(_wingGameObject.transform, ACTLayerUtils.TransparentFX, true);
            }
        }
    }
}
