﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACTSystem
{
    public class WingQualityConfig
    {
        public const string LEVEL_H_NAME = "level_H";
        public const string LEVEL_M_NAME = "level_M";
        public const string LEVEL_L_NAME = "level_L";

        public const string NAME_FX_WING = "fx_wing";
    }

    public class WingQualityData
    {
        private string _wingName;
        private Dictionary<int, List<string>> _wingQualityPathDict;
        public WingQualityData(GameObject gameObject)
        {
            _wingName = gameObject.name;
            _wingQualityPathDict = new Dictionary<int, List<string>>();
            CachePath(gameObject);
        }

        private void CachePath(GameObject gameObject)
        {
            if (gameObject.name.Equals(WingQualityConfig.NAME_FX_WING))
            {
                int quality = 0;
                string name;
                GameObject childGameObject;
                List<string> pathList = new List<string>();
                for (int i = 0; i < gameObject.transform.childCount; ++i)
                {
                    pathList.Clear();
                    childGameObject = gameObject.transform.GetChild(i).gameObject;
                    name = childGameObject.name;
                    if (name.Equals(WingQualityConfig.LEVEL_H_NAME))
                    {
                        quality = (int)VisualFXQuality.H;
                    }
                    else if (name.Equals(WingQualityConfig.LEVEL_M_NAME))
                    {
                        quality = (int)VisualFXQuality.M;
                    }
                    else if (name.Equals(WingQualityConfig.LEVEL_L_NAME))
                    {
                        quality = (int)VisualFXQuality.L;
                    }

                    pathList.Add(name);
                    while (!childGameObject.transform.parent.name.Equals(_wingName))
                    {
                        pathList.Add(childGameObject.transform.parent.name);
                        childGameObject = childGameObject.transform.parent.gameObject;
                    }
                    childGameObject = null;
                    string pathName = string.Empty;
                    for (int j = pathList.Count - 1; j >= 0; --j)
                    {
                        pathName = string.Concat(pathName, pathList[j]);
                        if (j > 0) pathName = string.Concat(pathName, "/");
                    }

                    if (!_wingQualityPathDict.ContainsKey(quality))
                    {
                        _wingQualityPathDict.Add(quality, new List<string>());
                    }
                    _wingQualityPathDict[quality].Add(pathName);
                    name = null;
                }
                pathList.Clear();
                pathList = null;
            }
            else
            {
                for (int i = 0; i < gameObject.transform.childCount; ++i)
                {
                    CachePath(gameObject.transform.GetChild(i).gameObject);
                }
            }
        }

        ~WingQualityData()
        {
            _wingQualityPathDict.Clear();
            _wingQualityPathDict = null;
        }

        public Dictionary<int, List<string>> wingQualityPathDict
        {
            get
            {
                return _wingQualityPathDict;
            }
        }
    }

    public class ACTWingQuality
    {
        private static Dictionary<string, WingQualityData> _wingQualityDataDict = new Dictionary<string, WingQualityData>();
        private static WingQualityData GetWingQualityData(GameObject gameObject = null)
        {
            string wingName = gameObject.name;
            if (!_wingQualityDataDict.ContainsKey(wingName) && gameObject != null)
            {
                _wingQualityDataDict.Add(wingName, new WingQualityData(gameObject));
            }

            if (_wingQualityDataDict.ContainsKey(wingName))
            {
                return _wingQualityDataDict[wingName];
            }
            return null;
        }

        public static void UpdateWingQuality(GameObject gameObject)
        {
            WingQualityData wingQualityData = GetWingQualityData(gameObject);
            int curQuality = (int)ACTSystemDriver.GetInstance().CurVisualQuality;
            Dictionary<int, List<string>> wingQualityPathDict = wingQualityData.wingQualityPathDict;
            int quality;
            List<string> wingQualityPath;
            foreach (var node in wingQualityPathDict)
            {
                quality = node.Key;
                wingQualityPath = node.Value;
                SetWingPathVisible(gameObject, wingQualityPath, curQuality >= quality);
            }
        }

        private static void SetWingPathVisible(GameObject gameObject, List<string> wingQualityPath, bool visible)
        {
            Transform transform;
            for (int i = 0; i < wingQualityPath.Count; ++i)
            {
                transform = gameObject.transform.Find(wingQualityPath[i]);
                if (transform != null) transform.gameObject.SetActive(visible);
            }
            transform = null;
        }
    }
}
