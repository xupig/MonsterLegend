﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ACTSystem
{
    public class ACTEquipWeapon : ACTEquipComponent
    {
        public ACTEquipWeapon(ACTActor actor)
            : base(actor)
        {
        }

        GameObject _weaponGameObject1;
        GameObject _weaponGameObject2;

        protected override void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR, string colorG, string colorB)
        {
            if (data.EquipType != ACTEquipmentType.Weapon && data.EquipType != ACTEquipmentType.DeputyWeapon) 
            { 
                ACTLogger.Warning("equipID is not a Weapon:" + data.ID); 
            }
            _isNormalQuality = isNormalQuality;
            if (curEquipData != null) TakeOff();
            curEquipData = data;
            curEquipParticleID = particleID;
            curEquipFlowID = flowID;
            if (_onEquipLoading)
            {
                return;
            }
            _onEquipLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            List<string> weaponPathList = new List<string>();
            if (!string.IsNullOrEmpty(data.Slot) && !string.IsNullOrEmpty(data.PrefabPath))
            {
                string prefabPath = (_isNormalQuality || string.IsNullOrEmpty(data.PrefabPath_L)) ? data.PrefabPath : data.PrefabPath_L;
                weaponPathList.Add(prefabPath);
            }
            if (!string.IsNullOrEmpty(data.Slot2) && !string.IsNullOrEmpty(data.PrefabPath2))
            {
                string prefabPath2 = (_isNormalQuality || string.IsNullOrEmpty(data.PrefabPath2_L)) ? data.PrefabPath2 : data.PrefabPath2_L;
                weaponPathList.Add(prefabPath2);
            }
            if (weaponPathList.Count == 0)
            {
                ACTLogger.Error("WeaponData Error in " + curEquipData.ID);
            }
            var weaponPathArray = weaponPathList.ToArray();
            weaponPathArray = assetLoader.GetResMappings(weaponPathArray);
            assetLoader.GetGameObjects(weaponPathArray, delegate(UnityEngine.Object[] objs)
            {
                _onEquipLoading = false;
                if (_actorIsDestory)
                {
                    if (objs.Length >= 1 && objs[0]) GameObject.Destroy(objs[0]);
                    if (objs.Length == 2 && objs[1]) GameObject.Destroy(objs[1]);
                    return;
                }
                if (curEquipData != data || _isNormalQuality != isNormalQuality)
                {
                    if (objs.Length >= 1 && objs[0]) GameObject.Destroy(objs[0]);
                    if (objs.Length == 2 && objs[1]) GameObject.Destroy(objs[1]);
                    if (curEquipData != null)
                    {
                        OnPutOn(curEquipData, curEquipParticleID, curEquipFlowID, _isNormalQuality, colorR, colorG, colorB);
                    }
                    return;
                }
                if (objs.Length >= 1)
                {
                    _weaponGameObject1 = objs[0] as GameObject;
                    if (flowFlashEffect == null)
                    {
                        flowFlashEffect = _weaponGameObject1.AddComponent<ArtTech.FlowFlashEffect>();
                        flowFlashEffect.EnableEffect = false;
                    }
                    if (ownerMRenderer == null)
                    {
                        ownerMRenderer = _weaponGameObject1.transform.GetComponent<MeshRenderer>();
                        if (ownerMRenderer == null && _weaponGameObject1.transform.childCount > 0)
                        {
                            ownerMRenderer = _weaponGameObject1.transform.GetChild(0).GetComponent<MeshRenderer>();
                        }
                    }
                }
                if (objs.Length == 2)
                {
                    _weaponGameObject2 = objs[1] as GameObject;
                }
                ResetWeaponPosition();
                PutOnParticle(curEquipParticleID);
                PutOnFlow(curEquipFlowID);
                if (onLoadEquipFinished != null)
                {
                    onLoadEquipFinished();
                }
            });
        }

        void SetWeaponPosition(string slotName1, GameObject weaponGameObject1, string slotName2, GameObject weaponGameObject2)
        {
            if (!string.IsNullOrEmpty(slotName1) && weaponGameObject1 != null)
            {
                var slot1 = _actor.boneController.GetBoneByName(slotName1);
                if (slot1 == null)
                {
                    ACTLogger.Error("slot " + slotName1 + " is not found!!");
                    return;
                }
                weaponGameObject1.transform.SetParent(slot1, false);
                weaponGameObject1.transform.localPosition = Vector3.zero;
                weaponGameObject1.transform.localEulerAngles = Vector3.zero;
                ACTSystemTools.SetFxTag(weaponGameObject1.transform);
                ACTSystemTools.SetLayer(weaponGameObject1.transform, slot1.gameObject.layer, true);
                ACTSystemTools.SetFxShow(weaponGameObject1.transform, _actor.equipController.canEquipFxShow);

            }
            if (!string.IsNullOrEmpty(slotName2) && weaponGameObject2 != null)
            {
                var slot2 = _actor.boneController.GetBoneByName(slotName2);
                if (slot2 == null)
                {
                    ACTLogger.Error("slot " + slotName2 + " is not found!!");
                    return;
                }
                weaponGameObject2.transform.SetParent(slot2, false);
                weaponGameObject2.transform.localPosition = Vector3.zero;
                weaponGameObject2.transform.localEulerAngles = Vector3.zero;
                ACTSystemTools.SetFxTag(weaponGameObject2.transform);
                ACTSystemTools.SetLayer(weaponGameObject2.transform, slot2.gameObject.layer, true);
                ACTSystemTools.SetFxShow(weaponGameObject1.transform, _actor.equipController.canEquipFxShow);
            }
        }

        void ResetWeaponPosition()
        {
            if (curEquipData == null) return;
            if (_inCity)
            {
                SetWeaponPosition(curEquipData.CitySlot, _weaponGameObject1, curEquipData.CitySlot2, _weaponGameObject2);
            }
            else
            {
                SetWeaponPosition(curEquipData.Slot, _weaponGameObject1, curEquipData.Slot2, _weaponGameObject2);
            }
            //SetWeaponPosition(curEquipData.Slot, _weaponGameObject1, curEquipData.Slot2, _weaponGameObject2);
        }

        protected override void OnTakeOff()
        {
            if (_weaponGameObject1 != null) GameObject.Destroy(_weaponGameObject1);
            if (_weaponGameObject2 != null) GameObject.Destroy(_weaponGameObject2);
            _weaponGameObject1 = null;
            _weaponGameObject2 = null;
            curEquipData = null;
            ownerMRenderer = null;
            flowFlashEffect = null;
        }

        GameObject _weaponParticleGameObject1;
        GameObject _weaponParticleGameObject2;
        protected override void OnPutOnParticle(ACTDataEquipParticle data)
        {
            if (curEquipParticleData != null) TakeOffParticle();
            curEquipParticleID = data.ID;
            curEquipParticleData = data;
            //LoggerHelper.Info(string.Format("PutOnWeaponParticle, equipID = {0}, particleID = {1}", _curWeaponData.ID, data.ID));
            if (_onEquipLoading || _onParticleLoading)
            {
                return;
            }
            _onParticleLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            if (curEquipParticleData.PrefabList.Count < 1)
            {
                return;
            }
            List<string> prefabPaths = new List<string>();
            if (_weaponGameObject1 != null)
            {
                prefabPaths.Add(curEquipParticleData.PrefabList[0].PrefabPath);
            }
            if (_weaponGameObject2 != null)
            {
                prefabPaths.Add(curEquipParticleData.PrefabList[0].PrefabPath);
            }
            var prefabPathArray = prefabPaths.ToArray();
            prefabPathArray = assetLoader.GetResMappings(prefabPathArray);
            assetLoader.GetGameObjects(prefabPathArray, delegate(UnityEngine.Object[] objs)
            {
                _onParticleLoading = false;
                if (_actorIsDestory || (curEquipParticleData != data))
                {
                    if (objs.Length >= 1 && objs[0]) GameObject.Destroy(objs[0]);
                    if (objs.Length == 2 && objs[1]) GameObject.Destroy(objs[1]);
                    if (curEquipParticleData != null && curEquipParticleData != data)
                    {
                        OnPutOnParticle(curEquipParticleData);
                    }
                    return;
                }
                if (objs.Length >= 1)
                {
                    _weaponParticleGameObject1 = objs[0] as GameObject;
                }
                if (objs.Length == 2)
                {
                    _weaponParticleGameObject2 = objs[1] as GameObject;
                }
                OnPutOnParticle();
                if (onLoadParticleFinished != null)
                {
                    onLoadParticleFinished();
                }
            });
        }

        public void ChangeEquipWeaponShader(Shader shader)
        {
            if (shader == null)
            {
                return;
            }
            if (_weaponGameObject1 != null)
            {
                MeshRenderer render1 = _weaponGameObject1.GetComponent<MeshRenderer>();
                render1.material.shader = shader; 
            }
            if (_weaponGameObject2 != null)
            {
                MeshRenderer render2 = _weaponGameObject2.GetComponent<MeshRenderer>();
                render2.material.shader = shader;
            }
        }

        void OnPutOnParticle()
        {
            if (_weaponGameObject1 != null && _weaponParticleGameObject1 != null)
            {
                _weaponParticleGameObject1.transform.SetParent(_weaponGameObject1.transform, false);
            }
            if (_weaponGameObject2 != null && _weaponParticleGameObject2 != null)
            {
                _weaponParticleGameObject2.transform.SetParent(_weaponGameObject2.transform, false);
            }
            ChangeGameObjectLayer(_weaponParticleGameObject1);
            ChangeGameObjectLayer(_weaponParticleGameObject2);
        }

        protected override void OnTakeOffParticle()
        {
            if (_weaponParticleGameObject1 != null)
            {
                //LoggerHelper.Info(string.Format("TakeOffWeaponParticle1, equipID = {0}, particleID = {1}", _curWeaponData.ID, _curWeaponParticleData.ID));
                GameObject.Destroy(_weaponParticleGameObject1);
            }
            if (_weaponParticleGameObject2 != null)
            {
                //LoggerHelper.Info(string.Format("TakeOffWeaponParticle2, equipID = {0}, particleID = {1}", _curWeaponData.ID, _curWeaponParticleData.ID));
                GameObject.Destroy(_weaponParticleGameObject2);
            }
            _weaponParticleGameObject1 = null;
            _weaponParticleGameObject2 = null;
            curEquipParticleData = null;
        }

        protected override void OnPutOnFlow(Material material)
        {
            OnPutOnFlow(_weaponGameObject1, material);
            OnPutOnFlow(_weaponGameObject2, material);
        }

        private void OnPutOnFlow(GameObject weaponGameObject, Material material)
        {
            if (weaponGameObject == null)
            {
                return;
            }
            if (ownerMRenderer != null)
            {
                ownerMRenderer.sharedMaterial = null;
                ownerMRenderer.sharedMaterial = material;
            }
        }

        protected override void OnTakeOffFlow()
        {
            OnTakeOffFlow(_weaponGameObject1);
            OnTakeOffFlow(_weaponGameObject2);
        }

        private void OnTakeOffFlow(GameObject weaponGameObject)
        {
            if (weaponGameObject == null)
            {
                return;
            }
            //var ownerMRenderer = weaponGameObject.transform.GetComponent<MeshRenderer>();
            if (ownerMRenderer != null && ownerMRenderer.sharedMaterial != null)
            {
                //FlowEnableKeyword(ownerMRenderer.sharedMaterial, false);
                FlowEnable(flowFlashEffect, new[] { ownerMRenderer.sharedMaterial }, false);
            }
        }

        bool _inCity = true;
        public void InCity(bool state)
        {
            _inCity = state;
            ResetWeaponPosition();
        }
    }
}
