﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ACTSystem
{
    public class ACTWeapon : MonoBehaviour
    {
        public int WeaponID = 0;
        /*
        public delegate void ColliderDelegate(GameObject go, ACTActor value, Vector3 hitPoint);
        public ColliderDelegate onTriggerEnter;
        public ColliderDelegate onTriggerExit;
        
        private bool m_isTrigger = false;
        public bool IsTrigger
        {
            get { return m_isTrigger; }
            set {
                if (m_isTrigger == value) return;
                m_isTrigger = value;
                ResetColliderPoint();
            }
        }

        protected Transform[] m_colliderPointTransforms;
        protected Vector3[] m_colliderPointLastPositions;
        protected Dictionary<int, ACTActor> m_touchMap = new Dictionary<int, ACTActor>();
        protected Dictionary<int, ACTActor> m_tempMap = new Dictionary<int, ACTActor>();
        protected Dictionary<int, Vector3> m_tempHitPointMap = new Dictionary<int, Vector3>();
        protected List<int> m_tempList = new List<int>();
        void Awake()
        {
            var colliderPointList = gameObject.GetComponentsInChildren<ACTWeaponColliderPoint>();
            m_colliderPointTransforms = new Transform[colliderPointList.Length];
            m_colliderPointLastPositions = new Vector3[colliderPointList.Length];
            for (int i = 0; i < m_colliderPointTransforms.Length; i++)
            {
                m_colliderPointTransforms[i] = colliderPointList[i].transform;
                m_colliderPointLastPositions[i] = Vector3.zero;
            }
            
        }
        
        void LateUpdate()
        {
            if (!m_isTrigger) return;
            
            for (int i = 0; i < m_colliderPointTransforms.Length; i++)
            {
                var transform = m_colliderPointTransforms[i];
                var lastPosition = m_colliderPointLastPositions[i];
                if (lastPosition != Vector3.zero)
                {
                    var sub = transform.position - lastPosition;
                    var ray = new Ray(lastPosition, sub.normalized);

                    Debug.DrawRay(lastPosition, sub, Color.blue, 6);
                    //Debug.DrawLine(m_weapon.transform.FindChild("force1").position, m_lastPosition1, Color.blue, 3);
                    var objectList = Physics.RaycastAll(ray, sub.magnitude, 1 << LayerMask.NameToLayer("Actor"));
                    //Debug.Log("RaycastAll:" + sub.magnitude + ":" + objectList.Length);
                    for (int k = 0; k < objectList.Length; k++)
                    {
                        var obj = objectList[k].collider.gameObject;
                        var act = obj.GetComponent<ACTActor>();
                        if (act == null)continue;
                        var actID = act.ID;
                        if (m_tempMap.ContainsKey(actID)) continue;
                        m_tempMap.Add(actID, act);
                        m_tempHitPointMap.Add(actID, objectList[k].point);
                    }
                }
                m_colliderPointLastPositions[i] = transform.position;
             
            }

            var enumt = m_touchMap.GetEnumerator();
            while (enumt.MoveNext())
            {
                if (!m_tempMap.ContainsKey(enumt.Current.Key))
                {
                    //Debug.Log("enumt.Current:onTriggerExit" + enumt.Current);
                    if (onTriggerExit != null) onTriggerExit(this.gameObject, enumt.Current.Value, Vector3.down);
                    m_tempList.Add(enumt.Current.Key);
                }
            }

            for (var i = 0; i < m_tempList.Count; i++)
            {
                m_touchMap.Remove(m_tempList[i]);
            }

            enumt = m_tempMap.GetEnumerator();
            while (enumt.MoveNext())
            {
                if (!m_touchMap.ContainsKey(enumt.Current.Key))
                {
                    //Debug.Log("enumt.Current:onTriggerEnter" + enumt.Current);
                    if (onTriggerEnter != null) onTriggerEnter(this.gameObject, enumt.Current.Value, m_tempHitPointMap[enumt.Current.Key]);
                    m_touchMap.Add(enumt.Current.Key, enumt.Current.Value);
                }
            }
            m_tempMap.Clear();
            m_tempList.Clear();
            m_tempHitPointMap.Clear();
        }
        
        void ResetColliderPoint()
        {
            for (int i = 0; i < m_colliderPointLastPositions.Length; i++)
            {
                m_colliderPointLastPositions[i] = Vector3.zero;
            }
            m_touchMap.Clear();
            m_tempMap.Clear();
            m_tempList.Clear();
        }
        */
    }
}
