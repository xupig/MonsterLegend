﻿using System.Reflection;
using ShaderUtils;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace ACTSystem
{

    public class ACTEquipCloth : ACTEquip
    {
        private string _meshPath = string.Empty;
        public string meshPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_meshPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_meshPath);
                }
                _meshPath = value;
            }
        }

        private string _physicalClothPath = string.Empty;
        public string PhysicalClothPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_physicalClothPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_physicalClothPath);
                }
                _physicalClothPath = value;
            }
        }
        private string _materialPath = string.Empty;
        public string materialPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_materialPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_materialPath);
                }
                _materialPath = value;
            }
        }

        private bool _isShelter;
        public bool isShelter
        {
            set
            {
                _isShelter = value;
                ResetShelter();
            }
        }

        //是否用高品质材质
        private bool _isMaterialHQ = false;
        public bool isMaterialHQ
        {
            set
            {
                _isMaterialHQ = value;
            }
        }

        private SkinnedMeshRenderer ownerSMRenderer;

        public ACTEquipCloth(ACTActor actor)
            : base(actor)
        {
        }

        public override void OnActorDestroy()
        {
            base.OnActorDestroy();
            meshPath = string.Empty;
            materialPath = string.Empty;
        }

        public void Nake()
        {
            TakeOff();
        }

        protected override void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR = "", string colorG = "", string colorB = "")
        {
            if (data.EquipType != ACTEquipmentType.Cloth) { ACTLogger.Error("equipID is not a Cloth:" + data.ID); }
            _isNormalQuality = isNormalQuality;
            if (curEquipData != null) TakeOff();
            string newMeshPath = (_isNormalQuality || string.IsNullOrEmpty(data.MeshPath_L)) ? data.MeshPath : data.MeshPath_L;
            string newMaterialPath = (_isNormalQuality || string.IsNullOrEmpty(data.MaterialPath_L)) ? data.MaterialPath : data.MaterialPath_L;
            string newPhysicalClothPath = ""; //data.PhysicalClothPath 废弃
            if (_isMaterialHQ && !string.IsNullOrEmpty(data.MaterialHQPath))
            {
                newMaterialPath = data.MaterialHQPath;
            }
            var flowData = data.GetEquipFlowData(flowID);
            if (flowID > 0 && flowData != null)
            {
                newMaterialPath = flowData.MaterialPath;
            }
            if (string.IsNullOrEmpty(newMeshPath) || string.IsNullOrEmpty(newMaterialPath))
            {
                ACTLogger.Error("ACTDataEquipment Data error:" + data.ID);
                return;
            }

            curEquipData = data;
            if (flowID > 0 && flowData != null)
            {
                curEquipFlowID = flowData.ID;
                curEquipFlowData = flowData;
            }
            curEquipParticleID = particleID;
            if (_onEquipLoading)
            {
                return;
            }
            _onEquipLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            string[] paths = new string[] { newMeshPath, newMaterialPath };
            if (!string.IsNullOrEmpty(newPhysicalClothPath))
            {
                paths = new string[] { newMeshPath, newMaterialPath, newPhysicalClothPath };
            }
            paths = assetLoader.GetResMappings(paths);
            assetLoader.GetObjects(paths, delegate(UnityEngine.Object[] objs)
            {
                _onEquipLoading = false;
                if (_actorIsDestory)
                {
                    assetLoader.ReleaseAssets(paths);
                    return;
                }
                if (curEquipData != data || curEquipFlowData != flowData || _isNormalQuality != isNormalQuality)
                {
                    assetLoader.ReleaseAssets(paths);
                    if (curEquipData != null)
                    {
                        OnPutOn(curEquipData, curEquipParticleID, curEquipFlowID, _isNormalQuality, colorR, colorG, colorB);
                    }
                    return;
                }
                if (objs[0] == null)
                {
                    ACTLogger.Error(string.Format("装备加载出错 {0} MeshPath:{1}", curEquipData.ID, paths[0]));
                    return;
                }
                var fbx = objs[0] as GameObject;
                this.meshPath = paths[0];
                var meshObjName = Path.GetFileNameWithoutExtension(paths[0]);
                if (meshObjName == ACTSystemTools.EmptyPrefab)
                    return;
                Transform mashObj = fbx.transform.Find(meshObjName);
                if (mashObj == null)
                {
                    ACTLogger.Error(string.Format(" {0} 中没有找到:{1} 联系美术修复！", paths[0], meshObjName));
                    return;
                }
                var skinnedMeshRenderer = mashObj.GetComponent<SkinnedMeshRenderer>();

                if (objs[1] == null)
                {
                    ACTLogger.Error(string.Format("装备加载出错 {0} MaterialPath:{1}", curEquipData.ID, paths[1]));
                    return;
                }

                Material material = objs[1] as Material;
                this.materialPath = paths[1];

                GameObject physicalClothGo = null;
                if (!string.IsNullOrEmpty(newPhysicalClothPath))
                {
                    physicalClothGo = objs[2] as GameObject;
                    this._physicalClothPath = paths[2];
                }
                OnPutOn(skinnedMeshRenderer, material, physicalClothGo, colorR, colorG, colorB);
                PutOnParticle(curEquipParticleID);
                if (flowID == 0)
                {
                    TakeOffFlow();
                }
                else
                {
                    //FlowEnable(flowFlashEffect, new[] { ownerSMRenderer.sharedMaterial }, true, flowData);
                    FlowEnable(flowFlashEffect, new[] { ownerSMRenderer.material }, true, flowData);
                    if (onLoadFlowFinished != null)
                    {
                        onLoadFlowFinished();
                    }
                }
                if (onLoadEquipFinished != null)
                {
                    onLoadEquipFinished();
                }
            });
        }

        public static T GetCopyOf<T>(Component comp, T other) where T : Component
        {
            Type type = comp.GetType();
            if (type != other.GetType()) return null;
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
            PropertyInfo[] pinfos = type.GetProperties(flags);
            foreach (var pinfo in pinfos)
            {
                if (pinfo.CanWrite)
                {
                    try
                    {
                        object oValue = pinfo.GetValue(other, null);
                        object oValue2 = pinfo.GetValue(comp, null);
                        if (pinfo.Name == "coefficients")
                        {
                            ClothSkinningCoefficient[] coefficients = oValue as ClothSkinningCoefficient[];
                            ClothSkinningCoefficient[] coefficients2 = oValue2 as ClothSkinningCoefficient[];
                            if (coefficients.Length != coefficients2.Length)
                                Debug.LogError("coefficients:" + coefficients.Length + "--" + coefficients2.Length);
                        }

                        pinfo.SetValue(comp, oValue, null);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("GetCopyOf cathc:" + e.Message);
                    }
                }
            }
            FieldInfo[] finfos = type.GetFields(flags);
            foreach (var finfo in finfos)
            {
                try
                {
                    finfo.SetValue(comp, finfo.GetValue(other));
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
            return comp as T;

        }

        void OnPutOn(SkinnedMeshRenderer newSMRenderer, Material material, GameObject physicalClothGameObject = null, string colorR = "", string colorG = "", string colorB = "")
        {
            var clothSlot = _actor.boneController.GetHumanBody();
            if (clothSlot == null)
            {
                return;
            }
            if (flowFlashEffect == null)
            {
                flowFlashEffect = clothSlot.gameObject.AddComponent<ArtTech.FlowFlashEffect>();
                flowFlashEffect.EnableEffect = false;
            }

            if (ownerSMRenderer == null) ownerSMRenderer = clothSlot.GetComponent<SkinnedMeshRenderer>();
            //var ownerSMRenderer = clothSlot.GetComponent<SkinnedMeshRenderer>();
            ownerSMRenderer.sharedMesh = newSMRenderer.sharedMesh;
            List<Transform> bones = new List<Transform>();
            for (int i = 0; i < newSMRenderer.bones.Length; i++)
            {
                var bone = this._actor.boneController.GetBoneByName(newSMRenderer.bones[i].name);
                if (bone == null)
                {
                    Debug.LogError("bone is none " + newSMRenderer.bones[i].name);
                    continue;
                }
                bones.Add(bone);
            }
            ownerSMRenderer.bones = bones.ToArray();
            //涉及染色，每个模型单独一份材质球
            ownerSMRenderer.sharedMaterial = null;
            ownerSMRenderer.sharedMaterial = SetColor(new Material(material), this.colorR, this.colorG, this.colorB);

            ownerSMRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            ownerSMRenderer.receiveShadows = false;
            ResetShelter();
        }

        protected override void OnPutOnOthers(int particleID, int flowID, string colorR = "", string colorG = "", string colorB = "")
        {
            if (curEquipParticleID != particleID)
            {
                curEquipParticleID = particleID;
                PutOnParticle(particleID);
            }
            this.colorR = colorR;
            this.colorG = colorG;
            this.colorB = colorB;
            OnSetColor(colorR, colorG, colorB);
        }

        protected override void OnTakeOff()
        {
            var clothSlot = _actor.boneController.GetHumanBody();
            if (clothSlot == null)
            {
                return;
            }
            //var ownerSMRenderer = clothSlot.transform.GetComponent<SkinnedMeshRenderer>();
            if(ownerSMRenderer != null)
            {
                ownerSMRenderer.sharedMesh = null;
                ownerSMRenderer.sharedMaterial = null;
            }
            curEquipData = null;
        }

        protected override void OnPutOnParticle(ACTDataEquipParticle data)
        {
            if (curEquipParticleData != null) TakeOffParticle();
            curEquipParticleID = data.ID;
            curEquipParticleData = data;
            if (_onEquipLoading || _onParticleLoading)
            {
                return;
            }
            _onParticleLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            List<string> prefabPathList = new List<string>();
            for (int i = 0; i < curEquipParticleData.PrefabList.Count; ++i)
            {
                prefabPathList.Add(curEquipParticleData.PrefabList[i].PrefabPath);
            }
            var parfabPathArray = prefabPathList.ToArray();
            parfabPathArray = assetLoader.GetResMappings(parfabPathArray);
            assetLoader.GetGameObjects(parfabPathArray, delegate(UnityEngine.Object[] objs)
            {
                _onParticleLoading = false;
                if (_actorIsDestory || curEquipParticleData != data)
                {
                    if (objs != null)
                    {
                        for (int j = 0; j < objs.Length; ++j)
                        {
                            if (objs[j]) GameObject.Destroy(objs[j]);
                        }
                    }
                    if (curEquipParticleData != null && curEquipParticleData != data)
                    {
                        OnPutOnParticle(curEquipParticleData);
                    }
                    return;
                }
                for (int k = 0; k < objs.Length; ++k)
                {
                    var particleGameObject = objs[k] as GameObject;
                    OnPutOnParticle(curEquipParticleData.PrefabList[k].Slot, particleGameObject);
                    ChangeGameObjectLayer(particleGameObject);
                }
                if (onLoadParticleFinished != null)
                {
                    onLoadParticleFinished();
                }
            });
        }

        List<GameObject> _clothParticleGameObjectList = new List<GameObject>();
        void OnPutOnParticle(string slotName, GameObject particleGameObject)
        {
            if (string.IsNullOrEmpty(slotName) || particleGameObject != null)
            {
                var slot = _actor.boneController.GetBoneByName(slotName);
                particleGameObject.transform.SetParent(slot, false);
                _clothParticleGameObjectList.Add(particleGameObject);
            }
        }

        protected override void OnTakeOffParticle()
        {
            if (_clothParticleGameObjectList.Count > 0)
            {
                //LoggerHelper.Info(string.Format("TakeOffClothParticle, particleID = {0}", _curClothParticleData.ID));
                for (int i = 0; i < _clothParticleGameObjectList.Count; ++i)
                {
                    GameObject.Destroy(_clothParticleGameObjectList[i]);
                }
            }
            _clothParticleGameObjectList.Clear();
            curEquipParticleData = null;
        }

        protected override void OnPutOnFlow(ACTDataEquipFlow data)
        {
            curEquipFlowID = data.ID;
            curEquipFlowData = data;
            OnPutOn(curEquipData, curEquipParticleID, data.ID, _isNormalQuality);
        }

        protected override void OnTakeOffFlow()
        {
            var clothSlot = _actor.boneController.GetHumanBody();
            if (clothSlot == null)
            {
                return;
            }
            //var ownerSMRenderer = clothSlot.GetComponent<SkinnedMeshRenderer>();
            if (ownerSMRenderer != null && ownerSMRenderer.sharedMaterial == null)
            {
                return;
            }
            //FlowEnableKeyword(ownerSMRenderer.sharedMaterial, false);
            FlowEnable(flowFlashEffect, new Material[] { }, false);
        }

        void ResetShelter()
        {
            if (!_isShelter) return;
            try
            {
                var modelTrans = _actor.boneController.GetHumanBody();
                if (modelTrans == null) return;
                var renderer = modelTrans.GetComponent<SkinnedMeshRenderer>();
                Material material = UnityPropUtils.IsEditor ? renderer.sharedMaterial : renderer.material;
                if (material == null) return;//裸模资源的material为null
                if (material.shader.name.IndexOf("_Shelter") >= 0) return;
                var shaderName = string.Concat(material.shader.name, "_Shelter");
                if (shaderName.LastIndexOf("Diffuse") >= 0) return;
                var shader = ShaderRuntime.loader.Find(shaderName);
                if (shader != null)
                {
                    material.shader = shader;
                }
            }
            catch (Exception ex)
            {
                ACTLogger.Error("SetShelter Error!" + ex.Message);
            }
        }
    }
}
