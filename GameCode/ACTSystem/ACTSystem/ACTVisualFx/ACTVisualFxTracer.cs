﻿using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine;

namespace ACTSystem
{
    public class ACTVisualFxTracer : ACTVisualFx
    {
        public Transform target;

        public Vector3 targetOffset = Vector3.zero;

        public float speed;

        public Action callback;

        public bool arrived = false;

        public ACTVisualFxTracer()
        {
            fxType = ACTVisualFXType.TYPE_TRACER;
        }

        override protected void OnPlay()
        {
        }

        override protected void OnReset()
        {
            arrived = false;
        }

        override protected void OnUpdate()
        {
            Approaching();
        }

        Vector3 GetCurTargetPosition()
        {
            Vector3 targetPosition = target.position + targetOffset;
            return targetPosition;
        }

        void Approaching()
        {
            if (target == null)
            {
                arrived = true;
                return;
            }
            if (arrived) return;
            Vector3 targetPosition = GetCurTargetPosition();
            Vector3 direction = targetPosition - localPosition;
            float distance = direction.magnitude;
            float frameSpeed = speed * UnityPropUtils.deltaTime;

            if (distance <= frameSpeed)
            {
                localPosition = targetPosition;
                arrived = true;
                if (callback != null) 
                {
                    callback();
                    callback = null;
                }
                Stop();
            }
            else
            {
                localPosition = localPosition + frameSpeed * direction.normalized;
            }
            if (_transform != null)
            {
                _transform.LookAt(targetPosition);
            }
        }

        override protected void OnStop()
        {

        }
    }
}
