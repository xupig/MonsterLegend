﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{

    public class ACTVisualFx
    {
        public uint guid;

        public ACTVisualFXType fxType;

        public VisualFXPriority priority;

        public float duration;

        public string perfabName;
        public float disappearDelay = 0;
        public int sortOrder = -1;

        protected ACTAssetObject _assetObject;

        protected Transform _transform;
        protected ACTVisualFXPlayState _curState = ACTVisualFXPlayState.READY;

        protected float _pastTime = 0;

        private ACTEventListener.VoidDelegate _listenerDestory;

        private bool _hasParent = false;
        private Transform _parent = null;
        public Transform parent
        {
            set
            {
                _parent = value;
                _hasParent = _parent != null;
            }
        }

        private Transform _skinSlot;

        public Transform SkinSlot
        {
            set
            {
                _skinSlot = value;
            }
        }

        private ACTActor _actor;

        public ACTActor actor
        {
            set { _actor = value; }
        }

        private Vector3 _localPosition;
        public Vector3 localPosition
        {
            get
            {
                return _localPosition;
            }
            set
            {
                _localPosition = value;
                if (_transform != null) _transform.localPosition = _localPosition;
            }
        }

        private Quaternion _localRotation;
        public Quaternion localRotation
        {
            get
            {
                return _localRotation;
            }
            set
            {
                _localRotation = value;
                if (_transform != null) _transform.localRotation = _localRotation;
            }
        }

        private Vector3 _forward = Vector3.forward;
        public Vector3 forward
        {
            get
            {
                return _forward;
            }
            set
            {
                _forward = value;
                if (_transform != null) _transform.forward = _forward;
            }
        }

        private Transform _followTransform;
        public Transform followTransform
        {
            get
            {
                return _followTransform;
            }
            set
            {
                _followTransform = value;
            }
        }

        public bool isFollow = false;

        public ACTVisualFx()
        {
            fxType = ACTVisualFXType.TYPE_NORMAL;
        }

        public void Reset()
        {
            _curState = ACTVisualFXPlayState.READY;
            _assetObject = null;
            _transform = null;
            duration = 0;
            _pastTime = 0;
            perfabName = string.Empty;
            parent = null;
            SkinSlot = null;
            localPosition = Vector3.zero;
            localRotation = Quaternion.identity;
            forward = Vector3.forward;
            _skinSlot = null;
            disappearDelay = 0;
            OnReset();
        }

        public ACTVisualFXPlayState Update()
        {
            if (_curState == ACTVisualFXPlayState.READY)
            {
                Play();
            }
            else if (_curState == ACTVisualFXPlayState.PLAYING)
            {
                _pastTime += UnityPropUtils.deltaTime;
                LoadGameObject();
                if (duration < 0)
                {
                }
                else if (_pastTime > duration)
                {
                    Stop();
                }
                OnUpdate();
                UpdatePosition();
            }
            else if (_curState == ACTVisualFXPlayState.DISAPPEARING)
            { 
                CheckDisappear();
            }
            return _curState;
        }

        //实时更新特效位置，用于需要跟随实体，但不转向的特效
        private void UpdatePosition()
        {
            if (isFollow && followTransform != null && _assetObject != null && _assetObject.gameObject != null)
            {
                localPosition = followTransform.position;
            }
        }

        public bool IsReady()
        {
            return _curState == ACTVisualFXPlayState.READY;
        }

        public void Play()
        {
            if (_curState == ACTVisualFXPlayState.PLAYING) return;
            _curState = ACTVisualFXPlayState.PLAYING;
            _pastTime = 0;
            LoadGameObject();
            OnPlay();
        }

        public void Stop(bool force = false)
        {
            if (_curState == ACTVisualFXPlayState.ENDED) return;
            if (disappearDelay > 0 && !force)
            {
                _curState = ACTVisualFXPlayState.DISAPPEARING;
                StopParticles();
            }
            else
            {
                _curState = ACTVisualFXPlayState.ENDED;
                ReleaseGameObject();
                OnStop();
            }
        }

        private void LoadGameObject()
        {
            if (_assetObject != null) return;
            var assetObject = ACTSystemDriver.GetInstance().ObjectPool.CreateGameObject(perfabName);
            SetVisualFXObject(assetObject);
        }

        private void ReleaseGameObject()
        {
            if (_assetObject != null)
            {
                if (_skinSlot != null && _assetObject.gameObject != null)
                {
                    EmptySkinSlot(_assetObject.gameObject.transform);
                }
                ACTSystemDriver.GetInstance().ObjectPool.ReleaseGameObject(_assetObject);
                _assetObject = null;
                _transform = null;
            }
        }

        private void EmptySkinSlot(Transform sfxTransform)
        {
            var particles = sfxTransform.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem particle in particles)
            {
                if (particle.shape.skinnedMeshRenderer != null)
                {
                    var shape = particle.shape;
                    shape.skinnedMeshRenderer = null;

                    //particle.shape.skinnedMeshRenderer.bones = null;
                    //particle.shape.skinnedMeshRenderer.sharedMaterial = null;
                }
            }
        }

        private void SetSkinSlot(Transform sfxTransform)
        {
            SkinnedMeshRenderer smr = _skinSlot.GetComponent<SkinnedMeshRenderer>();
            if (smr == null)
                return;
            var particles = sfxTransform.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem particle in particles)
            {
                var shape = particle.shape;
                if (shape.shapeType == ParticleSystemShapeType.SkinnedMeshRenderer)
                {
                    shape.skinnedMeshRenderer = smr;
                }                  
            }
        }

        private void SetVisualFXObject(ACTAssetObject obj)
        {
            if (obj == null) return;
            ReleaseGameObject();
            _assetObject = obj;
            _transform = _assetObject.gameObject.transform;
            if (_hasParent && _parent == null)
            { 
                Stop();
                return;
            }
            _transform.localRotation = localRotation;
            if (_hasParent)
            {
                _transform.SetParent(_parent, false);
            }
            if (forward != Vector3.forward)
            {
                _transform.forward = forward;
            }
            _transform.localPosition = localPosition;
            if(_skinSlot!=null)
                SetSkinSlot(_transform);
            if (CheckInCamera(_transform))
            {
                _assetObject.gameObject.SetActive(true);
                PlayParticles();
            }
            if (ACTSystemDriver.GetInstance().runtimeMode)
            {
                var listener = ACTEventListener.Get(_assetObject.gameObject);
                if(_listenerDestory == null)
                {
                    _listenerDestory = OnDestroy;
                }
                listener.onDestroy = _listenerDestory;
            }
            if (sortOrder != -1)
            {
                List<Renderer> rendererList = new List<Renderer>();
                _assetObject.gameObject.GetComponentsInChildren(false, rendererList);
                for (int i = 0; i < rendererList.Count; i++)
                {
                    rendererList[i].sortingOrder = sortOrder;
                }
            }
        }

        protected bool CheckInCamera(Transform transform)
        {
            if (UnityPropUtils.Platform == RuntimePlatform.WindowsEditor
                || UnityPropUtils.Platform == RuntimePlatform.OSXEditor)
            {
                return true;
            }
            var position = transform.position;
            if (Camera.current != null)
            {
                var viewportPoint = Camera.current.WorldToViewportPoint(position);
                Vector4 v4 = new Vector4(viewportPoint.x, viewportPoint.y, viewportPoint.z, 1);
                var pv = Camera.current.projectionMatrix * v4;
                bool xr = (-pv.w < pv.x) | (pv.x < pv.w);
                bool yr = (-pv.w < pv.y) | (pv.y < pv.w);
                bool zr = (0 < pv.z) | (pv.z < pv.w);
                return xr && yr && zr;
            }  
            return false;
        }

        protected void PlayParticles()
        {
            if (_assetObject == null) return;
            var particles = _assetObject.gameObject.GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }
        }

        protected void StopParticles()
        {
            if (_assetObject == null) return;
            var particles = _assetObject.gameObject.GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Stop();
            }
        }

        void CheckDisappear()
        {
            disappearDelay -= UnityPropUtils.deltaTime;
            if (disappearDelay <= 0) Stop();
        }

        private void OnDestroy(GameObject obj)
        {
            Stop();   
        }

        virtual protected void OnReset()
        {
            
        }

        virtual protected void OnPlay()
        {
            
        }

        virtual protected void OnUpdate()
        {

        }

        virtual protected void OnStop()
        {
            
        }

    }
}
