﻿

namespace MogoEngine.Events
{
    static public class RPCEvents
    {
        public readonly static string EntityAttached = "RPCEvents.EntityAttached";
        public readonly static string EntityCellAttached = "RPCEvents.EntityCellAttached";
        public readonly static string AOINewEntity = "RPCEvents.AOINewEntity"; //entity进入事件
        public readonly static string AOIDelEntity = "RPCEvents.AOIDelEvtity"; //entity退出事件
        public readonly static string BaseLogin = "RPCEvents.BaseLogin";
        public readonly static string RPCResp = "RPCEvents.RPCResp";
        public readonly static string RPCWait = "RPCEvents.RPCWait";

        public readonly static string AvatarAttriSync = "RPCEvents.AvatarAttriSync";
        public readonly static string EntityPosPull = "RPCEvents.EntityPosPull";
        public readonly static string EntityPosSync = "RPCEvents.EntityPosSync";
        public readonly static string EntityPosTeleport = "RPCEvents.EntityPosTeleport";
        public readonly static string OtherAttriSync = "RPCEvents.OtherAttriSync";
        public readonly static string OtherEntityPosPull = "RPCEvents.OtherEntityPosPull";
        public readonly static string OtherEntityPosSync = "RPCEvents.OtherEntityPosSync";
        public readonly static string OtherEntityPosTeleport = "RPCEvents.OtherEntityPosTeleport";

        public readonly static string Login = "RPCEvents.Login";
        public readonly static string ReConnectKey = "RPCEvents.RECONNECT_KEY";
        public readonly static string ReConnectResp = "RPCEvents.RECONNECT_RESP";
        public readonly static string ReConnectRejected = "RPCEvents.RECONNECT_REJECTED";
        public readonly static string ReConnectFailed = "RPCEvents.RECONNECT_FAILED";
        public readonly static string ReConnectSucceeded= "RPCEvents.RECONNECT_SUCCEEDED";
        public readonly static string ReConnectRefuse = "RPCEvents.RECONNECT_REFUSE";
        public readonly static string DefuseLogin = "RPCEvents.DEFUSE_LOGIN";

        public readonly static string CheckDef = "RPCEvents.CheckDef";

        public readonly static string NetStatusEventClose = "RPCEvents.NetStatusEvent_close";
        public readonly static string NetStatusEventBegin = "RPCEvents.NetStatusEvent_reconnectBegin";
        public readonly static string NetStatusEventEnd = "RPCEvents.NetStatusEvent_reconnectEnd";
        public readonly static string NetStatus = "RPCEvents.NetStatusEvent_netStatus";

        public readonly static string AddMD5Content = "RPCEvents.AddMD5Content";
        public readonly static string GetContentMD5 = "RPCEvents.GetContentMD5";

    }
}
