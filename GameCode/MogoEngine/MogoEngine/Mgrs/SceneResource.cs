﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using GameResource;
using System.Linq;

using GameLoader.Utils;
using GameLoader.LoadingBar;
using GameLoader.Utils.XML;
using GameLoader.IO;
using UnityEngine.SceneManagement;
using MogoEngine.Events;

namespace MogoEngine.Mgrs
{
    public struct LoadSceneSetting
    {
        static public int SCENE_TIME_DEFAULT = 0;

        public string sceneName;
        public int sceneTime;
        public bool updateProgress;  //是否刷新进度条
        public bool destroyOldSceneObject;  //是否删除原来的场景资源
        public bool clearSceneMemory; //是否清理场景内存
        public bool canCache;        //是否可以cache
        public string[] addtiveSceneNames; //附加场景资源
        public bool resetAllScenes;//是否Single模式打开副本
        public bool useUnity;//是否需要加载unity
        public string skybox;
        public string[] staticPrefabs;

        public Dictionary<string, bool> prefabsData;
        public Dictionary<int, string[]> lightmapPaths;
        public string lightProbesName;

        public LoadSceneSetting(string name)
        {
            sceneName = name;
            sceneTime = SCENE_TIME_DEFAULT;
            updateProgress = true;
            destroyOldSceneObject = true;
            clearSceneMemory = true;
            canCache = false;
            addtiveSceneNames = null;
            prefabsData = null;
            lightmapPaths = null;
            lightProbesName = null;
            resetAllScenes = true;
            useUnity = false;
            skybox = null;
            staticPrefabs = null;
        }

        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("sceneName: {0}\n", sceneName);
            sb.AppendFormat("sceneTime: {0}\n", sceneTime);
            sb.AppendFormat("updateProgress: {0}\n", updateProgress);
            sb.AppendFormat("destroyOldSceneObject: {0}\n", destroyOldSceneObject);
            sb.AppendFormat("clearSceneMemory: {0}\n", clearSceneMemory);
            sb.AppendFormat("canCache: {0}\n", canCache);
            if (addtiveSceneNames != null)
                sb.AppendFormat("addtiveSceneNames: {0}\n", addtiveSceneNames.PackArray());
            if (prefabsData != null)
                sb.AppendFormat("prefabsData: {0}\n", prefabsData.Keys.ToArray().PackArray());
            sb.AppendFormat("lightProbesName: {0}\n", lightProbesName);
            return sb.ToString();
        }
    }

    public class SceneResource
    {
        public string sceneName;
        public int sceneTime = 0;

        public bool needCache = false;
        private bool m_isLowModel;

        private Dictionary<string, bool> _prefabsData;

        private string[] _addtiveSceneNames;

        private string _lightProbesName;
        private Dictionary<int, string[]> _lightmapPaths;
        private string[] _lightmapNames;
        //private LightmapData[] _lightmapArray = null;
        private bool _useUnity;

        private DynamicScenes _dynamicScenes = new DynamicScenes();

        public bool IsLowModel
        {
            get
            {
                return m_isLowModel;
            }

            set
            {
                m_isLowModel = value;
                _dynamicScenes.IsLowModel = value;
            }
        }

        public SceneResource(LoadSceneSetting loadSceneSetting)
        {
            sceneName = loadSceneSetting.sceneName;
            needCache = loadSceneSetting.canCache;
            sceneTime = loadSceneSetting.sceneTime;
            _prefabsData = loadSceneSetting.prefabsData;
            _lightmapPaths = loadSceneSetting.lightmapPaths;
            _lightProbesName = loadSceneSetting.lightProbesName;
            _addtiveSceneNames = loadSceneSetting.addtiveSceneNames;
            _useUnity = loadSceneSetting.useUnity;
        }

        void ClearLightmap()
        {
            if (!_useUnity && _lightmapNames != null)
            {
                for (int i = 0; i < _lightmapNames.Length; i++)
                {
                    ObjectPool.Instance.Release(_lightmapNames[i]);
                }
            }
            _lightmapNames = null;
            LightmapSettings.lightmaps = new LightmapData[0];
            //_lightmapArray = null;
        }

        bool CompareLightmapNames(string[] one, string[] two)
        {
            bool result = true;
            if (one == two) return result;
            if (one != null && two != null)
            {
                if (one.Length != two.Length)
                {
                    result = false;
                }
                else
                {
                    for (int i = 0; i < one.Length; i++)
                    {
                        if (!one[i].Equals(two[i]))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            else { result = false; }
            return result;
        }

        void CreateSceneLightmaps(LoadSceneSetting loadSceneSetting, Action finish = null)
        {
            if (_lightmapPaths != null && _lightmapPaths.ContainsKey(0) && _lightmapPaths[0] != null)
            {
                //Debug.LogError("CreateSceneLightmaps: " + _lightmapPaths[0].PackArray());
                _lightmapNames = _lightmapPaths[0];
            }
            if ((_useUnity && !IsLowModel) || _lightmapNames == null || _lightmapNames.Length == 0)
            {
                if (finish != null) finish();
                return;
            }
            //设置lightmap
            //no ResourceMappingManager
            if (_useUnity && IsLowModel)
            {
                var lms = new List<string>();
                for (int i = 0; i < _lightmapNames.Length; i++)
                {
                    if (_lightmapNames[i].IndexOf("dir") > 0)
                        continue;
                    var lm = _lightmapNames[i].Replace(loadSceneSetting.sceneName, loadSceneSetting.sceneName + "_L");
                    lms.Add(lm);
                    //Debug.LogError("_lightmapNames: " + lm);
                }
                _lightmapNames = lms.ToArray();
            }

            GameResource.ObjectPool.Instance.GetObjects(_lightmapNames, (objs) =>
            {
                if (_lightmapNames == null)
                {
                    return;
                }
                Dictionary<int, LightmapData> lightmaps = new Dictionary<int, LightmapData>();
                for (int i = 0; i < objs.Length; i++)
                {
                    var obj = objs[i];
                    //Debug.LogError("CreateSceneLightmaps GetObjects: " + obj);
                    if (obj == null) continue;
                    var exrFile = _lightmapNames[i];
                    var exrFileName = Path.GetFileNameWithoutExtension(exrFile);
                    int idx = 0;// int.Parse(exrFileName.Split('-')[1]);
                    for (int index = 0; index < 10; index++)
                    {
                        if (exrFileName.Contains(index.ToString()))
                        {
                            idx = index;
                            break;
                        }
                    }
                    var lightmap = obj as Texture2D;
                    if (!lightmaps.ContainsKey(idx)) { lightmaps.Add(idx, new LightmapData()); }
                    if (exrFileName.IndexOf("light") > 0)
                    {
                        //Debug.LogError("lightmapFar: " + idx + " " + lightmap.name);
                        lightmaps[idx].lightmapFar = lightmap;
                    }
                    else if (exrFileName.IndexOf("dir") > 0)
                    {
                        //Debug.LogError("lightmapNear: " + idx + " " + lightmap.name);
                        lightmaps[idx].lightmapNear = lightmap;
                    }
                    //else if (exrFileName.IndexOf("Color") > 0)
                    //{
                    //    lightmaps[idx].lightmapFar = lightmap;
                    //}
                    //else if (exrFileName.IndexOf("Scale") > 0)
                    //{
                    //    lightmaps[idx].lightmapNear = lightmap;
                    //}
                }
                //Debug.LogError("lightmaps.Count: " + lightmaps.Count);
                var lightmapArray = new LightmapData[lightmaps.Count];
                int lidx = 0;
                foreach (var sub in lightmaps)
                {
                    lightmapArray[lidx] = sub.Value;
                    lidx++;
                }
                //Debug.LogError("lightmapArray: " + lightmapArray.PackArray());
                LightmapSettings.lightmaps = lightmapArray;
                if (finish != null) finish();
            });
        }

        public void SetHideAddStaticPrefabs(bool isHide)
        {
            _dynamicScenes.SetHideAddStaticPrefabs(isHide);
        }

        public void LoadDynamicScenes(string sceneName, bool useUnity, Action callback)
        {
            _dynamicScenes.LoadScene(sceneName, useUnity, callback);
        }

        public void LoadEnvData(LoadSceneSetting envData, Action callback)
        {
            _dynamicScenes.LoadEnvData(envData, callback);
            CreateSceneLightmaps(envData, LoadLightmapFinished);
        }

        private void LoadLightmapFinished()
        {
            DelayedLightmapUVAssembler.LoadLightmapFinished = true;
            if (DelayedLightmapUVAssembler.LoadLightmapFinishedEvent != null)
                DelayedLightmapUVAssembler.LoadLightmapFinishedEvent();
        }

        public void UnloadDynamicScenes()
        {
            DelayedLightmapUVAssembler.LoadLightmapFinished = false;
            ClearLightmap();
            _dynamicScenes.UnloadScene();
        }
    }

    public class AddtiveScenesMgr
    {
        private string[] loadedSceneNames = new string[0];

        private string[] loadedUnityNames;
        private GameObject[] loadedGos;
        private List<Transform> dynamicViews = new List<Transform>();
        private Dictionary<string, List<Vector3>> flyPaths = new Dictionary<string, List<Vector3>>();

        public Dictionary<string, List<Vector3>> FlyPaths
        {
            get
            {
                return flyPaths;
            }
        }

        public AddtiveScenesMgr()
        {
            EventDispatcher.AddEventListener<string>("TriggerDynamicViewController", OnTriggerDynamicViewController);
        }

        public bool IsTheSameAddtiveScenes(string[] sceneNames)
        {
            if (sceneNames == null || sceneNames.Length == 0)
            {
                return false;
            }

            if (sceneNames != null && sceneNames.Length == loadedSceneNames.Length)
            {
                //LoggerHelper.Error("LoadAddtiveScenes: " + sceneNames.Length);
                bool isSame = true;
                for (int i = 0; i < sceneNames.Length; i++)
                {
                    if (sceneNames[i] != loadedSceneNames[i])
                    {
                        isSame = false;
                        //LoggerHelper.Error("LoadAddtiveScenes is not the same: " + sceneNames[i] + " " + loadedSceneNames[i]);
                        break;
                    }
                }
                //LoggerHelper.Error("LoadAddtiveScenes isSame: " + isSame);
                return isSame;
            }
            return false;
        }

        public void LoadAddtiveScenes(string[] sceneNames, string targetScene, Action callback)
        {
            if (!IsTheSameAddtiveScenes(sceneNames))
            {
                //if (loadedSceneNames != null && sceneNames != null)
                //    LoggerHelper.Error("UnloadScenes: loadedSceneNames: " + loadedSceneNames.PackArray() + " sceneNames: " + sceneNames.PackArray() + " targetScene: " + targetScene);
                UnloadScenes();
            }
            else
            {
                //SetDynamicView(targetScene);
                callback.SafeInvoke();
                return;
            }
            if (sceneNames == null || sceneNames.Length == 0)
            {
                callback.SafeInvoke();
                return;
            }
            loadedUnityNames = new string[sceneNames.Length];
            var loadedGoNames = new string[sceneNames.Length];
            for (int i = 0; i < sceneNames.Length; i++)
            {
                var sceneName = sceneNames[i];
                loadedUnityNames[i] = string.Concat("Scenes/", sceneName, "/", sceneName, "_p/", sceneName, ".unity");
                loadedGoNames[i] = string.Concat("Scenes/", sceneName, "/", sceneName, "_p/", sceneName, ".prefab");
            }
            //no ResourceMappingManager
            ObjectPool.Instance.GetScenes(loadedUnityNames, (scenes) =>
            {
                //no ResourceMappingManager
                ObjectPool.Instance.GetGameObjects(loadedGoNames, (objs) =>
                {
                    for (int i = 0; i < objs.Length; i++)
                    {
                        objs[i].name = sceneNames[i];
                        var tranWaitingArea = objs[i].transform.Find("WaitingArea");
                        if (tranWaitingArea)
                        {
                            foreach (Transform item in tranWaitingArea)
                            {
                                item.gameObject.AddComponent<SceneWaitingArea>();
                            }
                        }
                        //else
                        //{
                        //    LoggerHelper.Error("addtive scene not contain waiting area: " + sceneNames[i]);
                        //}
                        var dynamicView = objs[i].transform.Find("DynamicView");
                        if (dynamicView)
                        {
                            dynamicViews.Add(dynamicView);
                        }
                        var dynamicViewController = objs[i].transform.Find("DynamicViewController");
                        if (dynamicViewController)
                        {
                            foreach (Transform item in dynamicViewController)
                            {
                                item.gameObject.AddComponent<DynamicViewController>();
                            }
                        }
                        var flyPath = objs[i].transform.Find("FlyPath");
                        if (flyPath)
                        {
                            foreach (Transform item in flyPath)
                            {
                                var pathPoint = new List<Vector3>();
                                foreach (Transform point in item)
                                {
                                    pathPoint.Add(point.position);
                                }
                                flyPaths[item.name] = pathPoint;
                                //LoggerHelper.Error("flyPaths: " + item.name + " " + pathPoint.PackList());
                            }
                        }

                        SceneManager.MoveGameObjectToScene(objs[i], scenes[i]);
                    }
                    SetDynamicView(targetScene);
                    loadedGos = objs;
                    loadedSceneNames = sceneNames;
                    callback.SafeInvoke();
                });
            });
        }

        public void UnloadScenes()
        {
            dynamicViews.Clear();
            flyPaths.Clear();
            for (int i = 0; i < loadedSceneNames.Length; i++)
            {
                UnloadScene(loadedGos[i], loadedUnityNames[i]);
                loadedGos[i] = null;
            }
            loadedSceneNames = new string[0];
        }

        private void OnTriggerDynamicViewController(string targetScene)
        {
            SetDynamicView(targetScene);
        }

        private void SetDynamicView(string targetScene)
        {
            for (int i = 0; i < dynamicViews.Count; i++)
            {
                foreach (Transform item in dynamicViews[i])
                {
                    item.gameObject.SetActive(false);
                }
                var targetChild = dynamicViews[i].Find(targetScene);
                if (targetChild)
                    targetChild.gameObject.SetActive(true);
            }
        }

        private void UnloadScene(GameObject go, string unityName)
        {
            //LoggerHelper.Error("UnloadScenes: " + unityName);
            GameObject.Destroy(go);
            var scene = SceneManager.GetSceneByName(unityName);
            if (scene.isLoaded)
                SceneManager.UnloadScene(scene);
            //else
            //    LoggerHelper.Error("scene not loaded: " + unityName);
        }
    }

    public class DynamicScenes
    {
        private SceneData m_sceneData;
        public Transform m_avatar;
        public bool IsLowModel;

        private float m_zoneWidth, m_zoneHeight;
        private int m_mapSectionXNum, m_mapSectionYNum;

        private List<ZoneObjData> m_loadedZones = new List<ZoneObjData>();
        private List<ZoneObjData> m_waitForRemove = new List<ZoneObjData>();

        private List<string> m_loadedObjects = new List<string>();
        private string m_loadedSkybox;
        private List<GameObject> m_staticPrefabs = new List<GameObject>();

        private float m_updateCounter;
        private int m_loadRange = 1;
        private int m_centerZoneX, m_centerZoneY;
        private int m_startZoneX, m_startZoneY, m_endZoneX, m_endZoneY;
        private float currentStartX, currentStartY, currentEndX, currentEndY = -10000;
        private Action LoadSceneFinished;
        private bool m_useUnity;
        private Scene m_currentScene;
        private static bool m_hideAddStaticPrefabs;//因为只需要全局唯一值，先用静态处理

        /// <summary>
        /// 主角所处的切片X值
        /// </summary>
        public int CenterZoneX
        {
            get { return m_centerZoneX; }
            set { m_centerZoneX = value; }
        }
        /// <summary>
        /// 主角所处的切片Y值
        /// </summary>
        public int CenterZoneY
        {
            get { return m_centerZoneY; }
            set { m_centerZoneY = value; }
        }

        public List<GameObject> StaticPrefabs
        {
            get
            {
                return m_staticPrefabs;
            }
        }

        public void SetHideAddStaticPrefabs(bool isHide)
        {
            m_hideAddStaticPrefabs = isHide;
            for (int i = 1; i < m_staticPrefabs.Count; i++)
            {
                m_staticPrefabs[i].SetActive(!isHide);
            }
        }

        public void LoadScene(string sceneName, bool useUnity, Action callback)
        {
            m_useUnity = useUnity;
            //string zoneFileName = string.Concat(ConstString.CONFIG_SUB_FOLDER, ConstString.DYNAMIC_MAPS_PATH, sceneName, ConstString.XML_SUFFIX);
            //var content = FileAccessManager.LoadText(zoneFileName);
            List<ZoneData> zoneDatas;
            //if (string.IsNullOrEmpty(content))
            //{
            //return false;
            zoneDatas = new List<ZoneData>();
            zoneDatas.Add(new ZoneData() { X = 1, Y = 1, Width = 10000, Height = 10000 });
            zoneDatas.Add(new ZoneData() { PrefabName = string.Concat(sceneName, ".prefab") });
            //}
            //else
            //{
            //    zoneDatas = XMLUtils.LoadXMLText<ZoneData>(content);
            //}

            m_mapSectionYNum = zoneDatas[0].Y;
            m_mapSectionXNum = zoneDatas[0].X;
            m_sceneData = new SceneData() { Id = 1, Name = sceneName };
            m_sceneData.Cells = new ZoneObjData[m_mapSectionYNum, m_mapSectionXNum];

            m_zoneWidth = zoneDatas[0].Width;
            m_zoneHeight = zoneDatas[0].Height;

            //Debug.Log("m_lightmaps.Count: " + m_lightmaps.Count);
            //Debug.Log("zoneDatas.Count: " + zoneDatas.Count);
            for (int i = 1; i < zoneDatas.Count; i++)
            {
                var zoneData = zoneDatas[i];
                var cell = new ZoneObjData();
                cell.X = zoneData.X;
                cell.Y = zoneData.Y;
                cell.PrefabName = zoneData.PrefabName;
                cell.SceneName = cell.PrefabName.Replace(".prefab", ".unity");
                //var fileName = string.Concat(ConstString.CONFIG_SUB_FOLDER, ConstString.DYNAMIC_MAPS_PATH, "lightmapdata_", Path.GetFileNameWithoutExtension(cell.PrefabName), ConstString.XML_SUFFIX);
                //content = FileAccessManager.LoadText(fileName);
                //cell.LightmapAssetDatas = XMLUtils.LoadXMLText<LightmapAssetData>(content);
                m_sceneData.Cells[zoneData.Y, zoneData.X] = cell;
            }
            MogoWorld.RegisterLateUpdate("dynamic_scenes", Update);

            //PreloadResources(() =>
            //{
            UpdateAvatarZone();
            if (LoadSceneFinished == null)
                LoadSceneFinished = callback;
            //if (callback != null)
            //    callback();
            //});
        }

        public void UnloadScene()
        {
            //LoggerHelper.Error("DynamicScenes UnloadScene");
            MogoWorld.UnregisterLateUpdate("dynamic_scenes", Update);

            //if (_tranReflectionProbe)
            //    GameObject.Destroy(_tranReflectionProbe.gameObject);

            for (int i = 0; i < m_loadedZones.Count; i++)
            {
                UnloadScene(m_loadedZones[i]);
            }

            foreach (var item in m_loadedObjects)
            {
                ObjectPool.Instance.Release(item);
            }
            m_loadedObjects.Clear();
            ClearStaticPrefabs();
        }

        private void ClearStaticPrefabs()
        {
            foreach (var item in m_staticPrefabs)
            {
                GameObject.Destroy(item);
            }
            m_staticPrefabs.Clear();
        }

        private void PreloadResources(Action callback)
        {
            callback.SafeInvoke();

            //List<string> preloadScenes = new List<string>();
            //List<string> preloadResources = new List<string>();
            //for (int y = 0; y < m_mapSectionYNum; y++)
            //{
            //    for (int x = 0; x < m_mapSectionYNum; x++)
            //    {
            //        var cell = m_sceneData.Cells[y, x];
            //        preloadScenes.Add(string.Concat("Scenes/", m_sceneData.Name, "/", m_sceneData.Name, "_p/", cell.SceneName));
            //        preloadResources.Add(string.Concat("Scenes/", m_sceneData.Name, "/", m_sceneData.Name, "_p/", cell.PrefabName));
            //    }
            //}

            ////LoggerHelper.Error("PreloadResources: " + m_preloadResources.PackList());
            //ObjectPool.Instance.PreloadAsset(preloadScenes.ToArray(), () =>
            //{
            //    ObjectPool.Instance.PreloadAsset(preloadResources.ToArray(), () =>
            //    {
            //        callback.SafeInvoke();
            //    });
            //});
        }

        public void LoadEnvData(LoadSceneSetting envData, Action callBack)
        {
            if (envData.staticPrefabs != null && envData.staticPrefabs.Length != 0)
            {
                //no ResourceMappingManager
                var sps = new List<string>();
                if (m_useUnity && IsLowModel)
                {
                    for (int i = 0; i < envData.staticPrefabs.Length; i++)
                    {
                        var sp = envData.staticPrefabs[i].Replace(m_sceneData.Name, m_sceneData.Name + "_L");
                        //Debug.LogError("envData.staticPrefabs: " + sp);
                        sps.Add(sp);
                    }
                }
                else
                {
                    sps.AddRange(envData.staticPrefabs);
                }
                ObjectPool.Instance.GetGameObjects(sps.ToArray(), (objs) =>
                {
                    //StaticBatching(objs);
                    m_staticPrefabs.AddRange(objs);
                    SetHideAddStaticPrefabs(m_hideAddStaticPrefabs);
                    try
                    {
                        callBack.SafeInvoke();
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Except(ex);
                    }
                    ObjectPool.Instance.AfterLoadScene();
                });
            }
            else
            {
                try
                {
                    callBack.SafeInvoke();
                }
                catch (Exception ex)
                {
                    LoggerHelper.Except(ex);
                }
                ObjectPool.Instance.AfterLoadScene();
            }

            if (string.IsNullOrEmpty(envData.skybox))
            {
                if (!string.IsNullOrEmpty(m_loadedSkybox))
                {
                    ObjectPool.Instance.Release(m_loadedSkybox);
                }
                RenderSettings.skybox = null;
            }
            else if (!string.IsNullOrEmpty(envData.skybox) && m_loadedSkybox != envData.skybox)
            {
                //no ResourceMappingManager
                ObjectPool.Instance.GetObject(envData.skybox, (obj) =>
                {
                    if (obj)
                    {
                        RenderSettings.skybox = obj as Material;
                    }
                    if (!string.IsNullOrEmpty(m_loadedSkybox))
                    {
                        ObjectPool.Instance.Release(m_loadedSkybox);
                    }
                    m_loadedSkybox = envData.skybox;
                });
            }
        }

        private static Material fakeFloorMat;
        public static Action<GameObject> AddTweenColorScript;
        public static bool UseFadeFloor;
        private static Mesh fakeFloorMesh;

        public static void GetFadeFloorMat()
        {
            ObjectPool.Instance.GetObject("Scenes/Skybox/Collider_fade.mat", (obj) => { fakeFloorMat = obj as Material; });
            var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            var mf = go.GetComponent<MeshFilter>();
            fakeFloorMesh = mf.sharedMesh;
            GameObject.Destroy(go);
            //Debug.LogError(fakeFloorMesh);
        }

        public void FadeFloor(GameObject prefab)
        {
            var terr = prefab.transform.FindChild("Collider/Terr");
            if (terr)
            {
                var childCount = terr.childCount;
                for (int i = 0; i < childCount; i++)
                {
                    var child = terr.GetChild(i).gameObject;
                    var mr = child.GetComponent<MeshRenderer>();
                    if (!mr)
                        mr = child.AddComponent<MeshRenderer>();
                    var mf = child.GetComponent<MeshFilter>();
                    if (!mf)
                        mf = child.AddComponent<MeshFilter>();
                    mf.sharedMesh = fakeFloorMesh;
                    mr.enabled = true;
                    mr.sharedMaterial = fakeFloorMat;
                    AddTweenColorScript(child.gameObject);
                }
            }
            else
            {
                Debug.LogError("no terr: " + prefab);
            }
        }

        void Update()
        {
            m_updateCounter += UnityPropUtils.deltaTime;
            if (m_updateCounter > 1)
            {
                m_updateCounter = 0;
                UpdateAvatarZone();
            }
        }

        private Vector3 m_playerPos = Vector3.zero;//取巧兼容创角场景，后续需做返回选角应该要修改这里
        private void UpdateAvatarZone()
        {
            if (MogoWorld.Player != null)
                m_playerPos = MogoWorld.Player.position;
            if (currentStartX <= m_playerPos.x && m_playerPos.x <= currentEndX && currentStartY <= m_playerPos.z && m_playerPos.z <= currentEndY)
            {

            }
            else
            {
                CenterZoneX = (int)Math.Floor(m_playerPos.x / m_zoneWidth);
                CenterZoneY = (int)Math.Floor(m_playerPos.z / m_zoneHeight);
                currentStartX = CenterZoneX * m_zoneWidth;
                currentStartY = CenterZoneY * m_zoneHeight;
                currentEndX = currentStartX + m_zoneWidth;
                currentEndY = currentStartY + m_zoneHeight;

                ChangeMapZones();

                //Debug.Log(CenterZoneX + " " + CenterZoneY);
            }
        }

        private void ChangeMapZones()
        {
            m_startZoneX = Mathf.Max(CenterZoneX - m_loadRange, 0);
            m_endZoneX = Mathf.Min(CenterZoneX + m_loadRange, m_mapSectionXNum - 1);
            m_startZoneY = Mathf.Max(CenterZoneY - m_loadRange, 0);
            m_endZoneY = Mathf.Min(CenterZoneY + m_loadRange, m_mapSectionYNum - 1);

            //var sb = new System.Text.StringBuilder();
            m_waitForRemove.Clear();
            m_waitForRemove.AddRange(m_loadedZones);
            m_loadedZones.Clear();
            for (int y = m_startZoneY; y <= m_endZoneY; y++)
            {
                for (int x = m_startZoneX; x <= m_endZoneX; x++)
                {
                    //sb.AppendFormat("{0},{1}; ", y, x);
                    var cell = m_sceneData.Cells[y, x];
                    m_waitForRemove.Remove(cell);
                    m_loadedZones.Add(cell);
                }
            }
            UpdateSceneModel();
            //if (_tranReflectionProbe)
            //    _tranReflectionProbe.position = new Vector3(currentStartX + m_zoneWidth * 0.5f, 0, currentStartY + m_zoneHeight * 0.5f);
            //Debug.Log(sb.ToString());
            //Debug.Log("waitForRemove: " + m_waitForRemove.Count);
        }

        private void UpdateSceneModel()
        {
            for (int i = 0; i < m_waitForRemove.Count; i++)
            {
                UnloadScene(m_waitForRemove[i]);
            }

            var loadedUnityNames = new List<string>(m_loadedZones.Count);
            var loadedZoneNames = new List<string>(m_loadedZones.Count);
            var tempZones = new List<ZoneObjData>(m_loadedZones.Count);
            for (int i = 0; i < m_loadedZones.Count; i++)
            {
                if (!m_loadedZones[i].Loading && !m_loadedZones[i].Loaded)
                {
                    loadedUnityNames.Add(string.Concat("Scenes/", m_sceneData.Name, "/", m_sceneData.Name, "_p/", m_loadedZones[i].SceneName));
                    var prefabName = string.Concat("Scenes/", m_sceneData.Name, "/", m_sceneData.Name, "_p/", m_loadedZones[i].PrefabName);
                    //Debug.LogError("UpdateSceneModel: " + m_useUnity + " " + IsLowModel);
                    if (m_useUnity && IsLowModel)
                    {
                        prefabName.Replace(m_sceneData.Name, m_sceneData.Name + "_L");
                        //Debug.LogError("prefabName: " + prefabName);
                    }
                    loadedZoneNames.Add(prefabName);
                    tempZones.Add(m_loadedZones[i]);
                    m_loadedZones[i].Loading = true;
                }
            }
            if (m_useUnity && !IsLowModel)
            {
                //no ResourceMappingManager
                ObjectPool.Instance.GetScenes(loadedUnityNames.ToArray(), (scenes) =>
                {
                    SceneManager.SetActiveScene(scenes[0]);
                    m_currentScene = scenes[0];
                    LoadScenePrefab(loadedZoneNames, tempZones);
                });
            }
            else
            {
                LoadScenePrefab(loadedZoneNames, tempZones);
            }
        }

        private void LoadScenePrefab(List<string> loadedZoneNames, List<ZoneObjData> tempZones)
        {
            var temp = DelayedConfig.CanDelayLoading;
            //Debug.LogError("LoadScenePrefab: " + m_useUnity);
            if (m_useUnity && !IsLowModel)
                DelayedConfig.CanDelayLoading = false;
            //no ResourceMappingManager
            ObjectPool.Instance.GetGameObjects(loadedZoneNames.ToArray(), (objs) =>
            {
                for (int i = 0; i < objs.Length; i++)
                {
                    var zone = tempZones[i];
                    zone.Prefab = objs[i];
                    //zone.Scene = scenes[i];
                    //SceneManager.MoveGameObjectToScene(zone.Prefab, zone.Scene);
                    zone.Prefab.name = Path.GetFileNameWithoutExtension(zone.PrefabName);
                    var x = zone.X + 1;
                    var y = zone.Y + 1;
                    var terr = zone.Prefab.transform.Find("Terrain");
                    if (terr)
                        zone.Terrain = terr.GetComponent<Terrain>();
                    if (UseFadeFloor)
                        FadeFloor(zone.Prefab);
                    //else
                    //    LoggerHelper.Error("Can not find Terrain in " + zone.Prefab);
                    //LoadLightmapAsset(zone);
                    zone.Loaded = true;
                    zone.Loading = false;
                }

                //foreach (var item in LightmapSettings.lightmaps)
                //{
                //    if (item.lightmapFar)
                //        Debug.LogError("item.lightmapFar: " + item.lightmapFar.name);
                //    if (item.lightmapNear)
                //        Debug.LogError("item.lightmapNear: " + item.lightmapNear.name);
                //}

                if (m_sceneData.Cells.Length > 1)
                {
                    for (int i = 0; i < m_loadedZones.Count; i++)
                    {
                        var loadedZone = m_loadedZones[i];
                        var x = loadedZone.X;
                        var y = loadedZone.Y;

                        Terrain left = GetLoadedTerrain(y, x - 1);
                        Terrain right = GetLoadedTerrain(y, x + 1);
                        Terrain top = GetLoadedTerrain(y + 1, x);
                        Terrain bottom = GetLoadedTerrain(y - 1, x);

                        loadedZone.Terrain.SetNeighbors(left, top, right, bottom);
                    }
                }
                if (LoadSceneFinished != null)
                {
                    LoadSceneFinished();
                    LoadSceneFinished = null;
                }
                //if (m_useUnity)
                //    StaticBatching(objs);
            });
            if (m_useUnity && !IsLowModel)
                DelayedConfig.CanDelayLoading = temp;
        }

        public void StaticBatching(GameObject[] sceneGameObjects)
        {
            //Debug.LogError("StaticBatching: " + sceneGameObjects[0].name);
            for (int i = 0; i < sceneGameObjects.Length; i++)
            {
                var sceneGameObject = sceneGameObjects[i];
                sceneGameObject.isStatic = true;
                var Mesh = sceneGameObject.transform.Find("Mesh");
                if (Mesh == null) continue;
                var StaticBatching = Mesh.Find("StaticBatching");
                if (StaticBatching != null)
                {
                    StaticBatchingUtility.Combine(StaticBatching.gameObject);
                }
                else
                {
                    StaticBatchingUtility.Combine(Mesh.gameObject);
                }
            }
        }

        private void UnloadScene(ZoneObjData scene)
        {
            if (scene.Loaded)
            {
                GameObject.Destroy(scene.Prefab);
                GameObject.DontDestroyOnLoad(ProgressBar.Instance.prefab);
                if (m_useUnity && !IsLowModel)
                {
                    SceneManager.UnloadScene(m_currentScene);
                }
                scene.Loaded = false;
            }
        }

        private Terrain GetLoadedTerrain(int y, int x)
        {
            if ((x >= 0 && x < m_mapSectionXNum) && (y >= 0 && y < m_mapSectionYNum))
            {
                var zone = m_sceneData.Cells[y, x];
                if (zone.Loaded)
                    return zone.Terrain;
            }

            return null;
        }
    }
}