
using System.Collections.Generic;
using UnityEngine;

namespace MogoEngine
{
    public class UpdateRegister
    {
        private static UpdateRegister s_instance = null;
        public static UpdateRegister GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new UpdateRegister();
            }
            return s_instance;
        }

        private Dictionary<string, UpdateDelegateBase> _updateDelegates;
        private Dictionary<string, LateUpdateDelegate> _updateLateDelegates;
        private GameObject _delegateObj;
        public UpdateRegister()
        {
            var driverObj = EngineDriver.Instance.gameObject;
            _delegateObj = new GameObject("UpdateDelegate");
            _delegateObj.transform.SetParent(driverObj.transform, false);
            _updateDelegates = new Dictionary<string, UpdateDelegateBase>();
            _updateLateDelegates = new Dictionary<string, LateUpdateDelegate>();
        }

        //注册update方法，被注册方法将每次Update被调用
        public void RegisterUpdate<T>(string name, UpdateDelegateBase.VoidDelegate func) where T:UpdateDelegateBase
        {
            if (!_updateDelegates.ContainsKey(name))
            {
                var delegateCom = _delegateObj.AddComponent<T>();
                delegateCom.updateName = name;
                _updateDelegates[name] = delegateCom;
            }
            _updateDelegates[name].onUpdate += func;          
        }

        //注销update方法，注销后方法将不再被每帧调用
        public void UnregisterUpdate(string name, UpdateDelegateBase.VoidDelegate func)
        {
            if (!_updateDelegates.ContainsKey(name)) return;
            _updateDelegates[name].onUpdate -= func;
        }

        //注册update方法，被注册方法将每次LateUpdate被调用
        public void RegisterLateUpdate(string name, LateUpdateDelegate.VoidDelegate func)
        {
            if (!_updateLateDelegates.ContainsKey(name))
            {
                var delegateCom = _delegateObj.AddComponent<LateUpdateDelegate>();
                delegateCom.name = name;
                _updateLateDelegates[name] = delegateCom;
            }
            _updateLateDelegates[name].onLateUpdate += func;
        }

        //注销update方法，注销后方法将不再被每帧调用
        public void UnregisterLateUpdate(string name, LateUpdateDelegate.VoidDelegate func)
        {
            if (!_updateLateDelegates.ContainsKey(name)) return;
            _updateLateDelegates[name].onLateUpdate -= func;
        }
    }

}