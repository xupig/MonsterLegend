﻿using System;
using System.Text;
using System.IO;

using UnityEngine;
using GameLoader.Utils;
using MogoEngine.RPC;
using MogoEngine;
using GameLoader.Config;
using GameLoader.IO;
using GameResource;
using GameLoader;
using GameLoader.Utils.Timer;
using GameLoader.LoadingBar;
using MogoEngine.Events;

/// <summary>
/// 初始化
/// </summary>
public class EngineDriver : MonoBehaviour
{
    private int beginTime;
    private const string MK = "Resources/_resources.mk";
    private const string EXTERNALS_MK = "Resources/_externals.mk";
    private const string NICKNAME_MK = "Resources/_nickname.mk";

    public static EngineDriver Instance { get; private set; }

    void Start()
    {
        Instance = this;
        InitEngine();
    }

    public delegate void VoidDelegate();
    public VoidDelegate onUpdate;
    void Update()
    {
        ServerProxy.Instance.Process();
        ServerProxy.Instance.Update();
        if (onUpdate != null) onUpdate();
        DelayedRendererAssembler.UpdateCheck.SafeInvoke();
    }

    public VoidDelegate onLateUpdate;
    void LateUpdate()
    {
        if (onLateUpdate != null) onLateUpdate();
    }

    void InitEngine()
    {
        QualitySettings.vSyncCount = 0;
        QualitySettings.lodBias = 4;
        beginTime = Environment.TickCount;
        MogoWorld.Init();
        ProgressBar.Instance.UpdateProgress(0.6f);
        LoggerHelper.Info(string.Format("[EngineDriver] MogoWorld.Init cost:{0}ms", Environment.TickCount - beginTime));
        beginTime = Environment.TickCount;
        InitObjectPool(OnInitObjectPool);
    }

    //InitObjectPool初始化结束回调
    private void OnInitObjectPool()
    {
        LoggerHelper.Info(string.Format("[EngineDriver] OnInitObjectPool cost:{0}ms", Environment.TickCount - beginTime));
        ProgressBar.Instance.UpdateProgress(0.9f);
        switch (UnityPropUtils.Platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                Invoke("OnSyncLoadClient", 0.01f);
                break;
            case RuntimePlatform.IPhonePlayer:
                var loaderDriver = gameObject.GetComponent<LoaderDriver>();
                loaderDriver.LoadClient();
                break;
        }

    }

    //异步调用，解决发生阻塞时进度条无及时刷新问题
    private void OnSyncLoadClient()
    {
        Component com = gameObject.GetComponent("LoaderDriver");
        var method = com.GetType().GetMethod("LoadClient");
        method.Invoke(com, null);
    }

    private string jsonContent = string.Empty;
    private void InitObjectPool(Action callback)
    {
        ObjectPool.Instance.SetIsWWW(SystemConfig.GetValueInCfg("IsWWWLoad") == "1");
        ObjectPool.Instance.SetIsAsync(SystemConfig.GetValueInCfg("IsAsyncLoad") != "0");
        switch (UnityPropUtils.Platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
                LoadResourcesMkAtAndorid("[发布模式]", callback);
                break;
            //case RuntimePlatform.WindowsWebPlayer://为排除警告
            //LoadResourcesMkAtWeb("[开发模式下--发布模式]", callback);
            //break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                if (SystemConfig.ReleaseMode)
                {
#if UNITY_WEBPLAYER
                    LoadResourcesMkAtWeb("[开发模式下--发布模式]", callback);
#else
                    LoadResourcesMkAtAndorid("[开发模式下--发布模式]", callback);
#endif
                }
                else
                {
                    string filePath = string.Concat(SystemConfig.RuntimeResourcePath, MK); ;
                    if (!File.Exists(filePath))
                    {
                        LoggerHelper.Error(string.Format("[开发模式] 文件:{0}未找到 InitObjectPool[Fail]", filePath));
                        return;
                    }
                    LoggerHelper.Info(string.Format("[开发模式] 加载:{0} InitObjectPool[Start]", filePath));
                    jsonContent = FileUtils.LoadFile(filePath);
                    ObjectPool.Instance.Initialize(SystemConfig.RuntimeResourcePath, jsonContent);
                    LoggerHelper.Info(string.Format("[开发模式] assetRootPath:{0}", ObjectPool.ASSET_ROOT_PATH));
                    if (callback != null) callback();
                }
                break;
        }
    }

    //发布模式下--加载_resources.mk
    private void LoadResourcesMkAtAndorid(string logTitle, Action callback)
    {
        //ProgressBar.Instance.UpdateTip("正在载入_resouces.mk");
        string outputPath = string.Concat(SystemConfig.RuntimeResourcePath, MK);
        outputPath = File.Exists(outputPath) ? ConstString.ASSET_FILE_HEAD + outputPath : IOUtils.GetStreamPath(MK);
        LoggerHelper.Info(string.Format("[{0}][InitObjectPool] 加载:{1}[Start]", logTitle, outputPath));

        ResLoader.LoadWwwBytes(outputPath, (string url, byte[] bytes) =>
        {
            LoggerHelper.Info(string.Format("[{0}] 加载:{1} [OK] bytes:{2}", logTitle, outputPath, bytes));
            //ProgressBar.Instance.UpdateProgress(0.8f);
            jsonContent = Encoding.GetEncoding("utf-8").GetString(bytes);
            ObjectPool.Instance.Initialize(SystemConfig.RuntimeResourcePath, jsonContent, 60, SystemConfig.ReleaseMode);
            ObjectPool.RemoteURL = SystemConfig.RemoteResourcesUrl;
            ObjectPool.GetJson = GetJsonByObjectPool;
            LoggerHelper.Info(string.Format("[{0}] assetRootPath:{1}", logTitle, ObjectPool.ASSET_ROOT_PATH));
            string externalsFile = IOUtils.GetStreamPath(EXTERNALS_MK);
            ResLoader.LoadWwwBytes(externalsFile, (externalsFileUrl, contentBytes) =>
            {
                if (contentBytes != null)
                {
                    string externalsJsonContent = Encoding.GetEncoding("utf-8").GetString(contentBytes);
                    ObjectPool.Instance.SetExternalInfo(externalsJsonContent);
                }
                if (callback != null) callback();  //初始化完成回调
            });
        });

        SockpuppetLoader.Sockpuppet = SystemConfig.GetValueInCfg("Sockpuppet") == "1";
        if (SockpuppetLoader.Sockpuppet)
        {
            LoadNicknameMk();
        }
    }

    private void LoadNicknameMk()
    {
        string outputPath = string.Concat(SystemConfig.RuntimeResourcePath, NICKNAME_MK);
        outputPath = File.Exists(outputPath) ? ConstString.ASSET_FILE_HEAD + outputPath : IOUtils.GetStreamPath(NICKNAME_MK);
        ResLoader.LoadWwwBytes(outputPath, (string url, byte[] bytes) =>
        {
            string content = Encoding.GetEncoding("utf-8").GetString(bytes);
            SockpuppetLoader.Initialize(content);
        });
    }

    //发布模式下--加载_resources.mk
    private void LoadResourcesMkAtWeb(string logTitle, Action callback)
    {
        //ProgressBar.Instance.UpdateTip("正在载入_resouces.mk");
        string resourcesMKUrl = Path.Combine(SystemConfig.RemoteResourcesUrl, "Bytes$_resources.mk.bytes.u");
        resourcesMKUrl = string.Format("{0}?date={1}", resourcesMKUrl, DateTime.Now.ToString("yyyyMMddHHmmssffffff"));
        LoggerHelper.Info(string.Format("[{0}][InitObjectPool] 加载:{1}[Start]", logTitle, resourcesMKUrl));
        string versionMKUrl = Path.Combine(SystemConfig.RemoteResourcesUrl, "Bytes$_versions.mk.bytes.u");
        versionMKUrl = string.Format("{0}?date={1}", versionMKUrl, DateTime.Now.ToString("yyyyMMddHHmmssffffff"));

        ResLoader.LoadWwwBytes(resourcesMKUrl, (string url, byte[] bytes) =>
        {
            LoggerHelper.Info(string.Format("[{0}] 加载:{1} [OK] bytes:{2}", logTitle, resourcesMKUrl, bytes));
            ResLoader.LoadWwwBytes(versionMKUrl, (string url2, byte[] bytes2) =>
            {
                LoggerHelper.Info(string.Format("[{0}] 加载:{1} [OK] bytes:{2}", logTitle, versionMKUrl, bytes2));
                //ProgressBar.Instance.UpdateProgress(0.8f);
                jsonContent = Encoding.GetEncoding("utf-8").GetString(bytes);
                ObjectPool.Instance.Initialize(SystemConfig.RuntimeResourcePath, jsonContent, 60, SystemConfig.ReleaseMode);
                string jsonContent2 = Encoding.GetEncoding("utf-8").GetString(bytes2);
                ObjectPool.Instance.SetVersionInfo(jsonContent2);
                ObjectPool.RemoteURL = SystemConfig.RemoteResourcesUrl;
                LoggerHelper.Info(string.Format("[{0}] assetRootPath:{1}", logTitle, ObjectPool.ASSET_ROOT_PATH));

                if (callback != null) callback();  //初始化完成回调
            });
        });
    }

    string GetJsonByObjectPool(string fileName)
    {
        string result = string.Empty;
        if (FileAccessManager.IsFileExist(fileName))
        {
            result = FileAccessManager.LoadText(fileName);
        }
        return result;
    }

    void OnApplicationQuit()
    {
        if (UnityPropUtils.Platform != RuntimePlatform.WindowsPlayer)
        {
            MogoFileSystem.Instance.Close();
            ServerProxy.Instance.Disconnect();
            ServerProxy.Instance.Release();
            LoggerHelper.Release();
        }
    }

    void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            ServerProxy.Instance.enableTimeout(false);
            LoggerHelper.Info("[game pause] pauseState:" + paused);
        }
        else
        {
            ServerProxy.Instance.enableTimeout(true);
            LoggerHelper.Info("]game start] pauseState:" + paused);
        }
    }

    /// <summary>
    /// 开始录制视频结果
    /// </summary>
    /// <param name="result">[1:成功,其他:失败]</param>
    virtual public void OnStartRecordOK(string result)
    {
        //派发到RePlayKitMgr.cs中
        EventDispatcher.TriggerEvent<string>("OnStartRecordOK", result);
    }

    /// <summary>
    /// 停止录制视频结果
    /// </summary>
    /// <param name="result">[1:成功,其他:失败]</param>
    virtual public void OnStopRecordOK(string result)
    {
        //派发到RePlayKitMgr.cs中
        EventDispatcher.TriggerEvent<string>("OnStopRecordOK", result);
    }

    /// <summary>
    /// IOS--评价回调
    /// </summary>
    /// <param name="result">评价结果</param>
    public void OnAstarEvent(string result)
    {
        LoggerHelper.Info(string.Format("OnAstarEvent result:{0}", result));
        EventDispatcher.TriggerEvent<string>("OnAstarEvent", result);
    }

    //================= iFlying  =============//
    public void VoiceVolume(string volume)
    {

    }

    public void StartTalk(string result)
    {

    }

    public void TalkError(string errorCode)
    {

    }

    public void VoiceMessage(string result)
    {

    }

    public void EndTalk(string result)
    {

    }

    /*private float prePay = 0;
    void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            TimerHeap.AddTimer(1000, 0, () => { prePay = 0; });
        }
    }*/
}