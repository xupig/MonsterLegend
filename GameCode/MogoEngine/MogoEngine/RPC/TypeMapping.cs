﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：TypeMapping
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：类型映射处理类。
//==========================================*/
#endregion
using System;

using GameLoader.Utils;


namespace MogoEngine.RPC
{
    /// <summary>
    /// 类型映射处理类。
    /// </summary>
    public class TypeMapping
    {
        private TypeMapping()
        {
        }

        /// <summary>
        /// 根据类型字符串返回对应的类型实例。
        /// </summary>
        /// <param name="type">类型字符串</param>
        /// <returns>类型实例</returns>
        public static VObject GetVObject(String type)
        {
            switch (type)
            {
                case ("STRING"):
                    return VString.Instance;
                case ("INT8"):
                    return VInt8.Instance;
                case ("UINT8"):
                    return VUInt8.Instance;
                case ("INT16"):
                    return VInt16.Instance;
                case ("UINT16"):
                    return VUInt16.Instance;
                case ("INT32"):
                    return VInt32.Instance;
                case ("UINT32"):
                    return VUInt32.Instance;
                case ("INT64"):
                    return VInt64.Instance;
                case ("UINT64"):
                    return VUInt64.Instance;
                case ("FLOAT"):
                    return VFloat.Instance;
                case ("FLOAT64"):
                    return VDouble.Instance;
                case ("BOOL"):
                    return VBoolean.Instance;
                case("PB_BLOB"):
                case ("BLOB"):
                    return VBLOB.Instance;
                case ("LUA_TABLE"):
                    return VLuaTable.Instance;
                default:
                    {
                        LoggerHelper.Warning(String.Format("Can not find type: {0}.", type));
                        return VEmpty.Instance;
                    }
            }
        }

        public static byte[] VObjectEncode(VObject vo, object value)
        {
            switch (vo.VType)
            {
                case VType.V_STR:
                    return VString.Instance.Encode((string)value);
                case VType.V_INT8:
                    return VInt8.Instance.Encode((byte)Convert.ToSByte(value));
                case VType.V_UINT8:
                    return VUInt8.Instance.Encode(Convert.ToByte(value));
                case VType.V_INT16:
                    return VInt16.Instance.Encode(Convert.ToInt16(value));
                case VType.V_UINT16:
                    return VUInt16.Instance.Encode(Convert.ToUInt16(value));
                case VType.V_INT32:
                    return VInt32.Instance.Encode(Convert.ToInt32(value));
                case VType.V_UINT32:
                    return VUInt32.Instance.Encode(Convert.ToUInt32(value));;
                case VType.V_INT64:
                    return VInt64.Instance.Encode(value);
                case VType.V_UINT64:
                    return VUInt64.Instance.Encode(value);
                case VType.V_FLOAT32:
                    return VFloat.Instance.Encode(value);
                case VType.V_FLOAT64:
                    return VDouble.Instance.Encode(value);
                case VType.V_BOOLEAN:
                    return VBoolean.Instance.Encode(Convert.ToBoolean(value));
                case VType.V_BLOB:
                    return VBLOB.Instance.Encode(value);
                case VType.V_LUATABLE:
                    return VLuaTable.Instance.Encode(value);
                default:
                    {
                        LoggerHelper.Warning(String.Format("Can not find VType: {0}.", vo.VType));
                        return VEmpty.Instance.Encode(null);
                    }
            }
        }
    }
}