﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MogoEngine.RPC
{
    class PlutoDataObjectPool
    {
        public static PlutoDataObjectPool instance = new PlutoDataObjectPool();


        // 对象池
        Queue<AttachedInfo> _AttachedInfoPool = new Queue<AttachedInfo>();
        Queue<CellAttachedInfo> _CellAttachedInfoPool = new Queue<CellAttachedInfo>();
        Queue<BaseAttachedInfo> _BaseAttachedInfoPool = new Queue<BaseAttachedInfo>();
        Queue<EntityPropertyValue> _EntityPropertyValuePool = new Queue<EntityPropertyValue>();
        
        #region AttachedInfo
        public AttachedInfo GetAttachedInfo()
        {
            AttachedInfo result;
            if (_AttachedInfoPool.Count > 0)
            {
                result = _AttachedInfoPool.Dequeue();
            }
            else
            {
                {
                    result = new AttachedInfo();
                }
            }
            return result;
        }

        public void ReleaseAttachedInfo(AttachedInfo obj)
        {
            obj.ClearData();
            _AttachedInfoPool.Enqueue(obj);
        }

        public void ReleaseAttachedInfoList(List<AttachedInfo> list)
        {
            if (list.Count == 0) return; 
            foreach (var obj in list)
            {
                obj.ClearData();
                _AttachedInfoPool.Enqueue(obj);
            }
            list.Clear();
        }
        #endregion AttachedInfo


        #region CellAttachedInfo
        public CellAttachedInfo GetCellAttachedInfo()
        {
            CellAttachedInfo result;
            if (_CellAttachedInfoPool.Count > 0)
            {
                result = _CellAttachedInfoPool.Dequeue();
            }
            else
            {
                result = new CellAttachedInfo();
            }
            return result;
        }

        public void ReleaseCellAttachedInfo(CellAttachedInfo obj)
        {
            obj.ClearData();
            _CellAttachedInfoPool.Enqueue(obj);
        }

        public void ReleaseCellAttachedInfoList(List<CellAttachedInfo> list)
        {
            if (list.Count == 0) return; 
            foreach (var obj in list)
            {
                obj.ClearData();
                _CellAttachedInfoPool.Enqueue(obj);
            }
            list.Clear();
        }
        #endregion CellAttachedInfo


        #region BaseAttachedInfo
        public BaseAttachedInfo GetBaseAttachedInfo()
        {
            BaseAttachedInfo result;
            if (_BaseAttachedInfoPool.Count > 0)
            {
                result = _BaseAttachedInfoPool.Dequeue();
            }
            else
            {
                result = new BaseAttachedInfo();
            }
            return result;
        }

        public void ReleaseBaseAttachedInfo(BaseAttachedInfo obj)
        {
            obj.ClearData();
            _BaseAttachedInfoPool.Enqueue(obj);
        }

        public void ReleaseBaseAttachedInfoList(List<BaseAttachedInfo> list)
        {
            if (list.Count == 0) return; 
            foreach (var obj in list)
            {
                obj.ClearData();
                _BaseAttachedInfoPool.Enqueue(obj);
            }
            list.Clear();
        }
        #endregion BaseAttachedInfo


        #region EntityPropertyValue
        static readonly int Max_Num = 40; //池最大数量
        int cacheNum = 0;
        public EntityPropertyValue GetEntityPropertyValue(EntityDefProperties property, object value)
        {
            EntityPropertyValue result;
            if (_EntityPropertyValuePool.Count > 0)
            {
                result = _EntityPropertyValuePool.Dequeue();
                cacheNum--;
            }
            else
            {
                result = new EntityPropertyValue();
            }
            result.SetData(property, value);
            return result;
        }

        //public void ReleaseEntityPropertyValue(EntityPropertyValue obj)
        //{
        //    if (cacheNum >= Max_Num) //数量过多不回收
        //    {
        //        obj = null;
        //        return;
        //    }
        //    obj.ClearData();
        //    _EntityPropertyValuePool.Enqueue(obj);
        //    cacheNum++;
        //}

        public void ReleaseEntityPropertyValueList(List<EntityPropertyValue> list)
        {
            if (list.Count == 0) return; 
            if (cacheNum >= Max_Num) //数量过多不回收
            {
                list.Clear();
                return;
            }
            foreach (var obj in list)
            {
                if (cacheNum >= Max_Num) break;
                obj.ClearData();
                _EntityPropertyValuePool.Enqueue(obj);
                cacheNum++;
            }
            list.Clear();
        }
        #endregion EntityPropertyValue



    }
}
