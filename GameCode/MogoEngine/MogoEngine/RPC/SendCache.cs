using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MogoEngine.RPC
{
    public class SendMark
    {
        public uint idx;
        public int len;
        public List<int> pages = new List<int>();
        public byte[] data;
        public SendMark(){}

        static Queue<SendMark> _pool = new Queue<SendMark>();
        public static SendMark Create()
        { 
            if (_pool.Count == 0)
            {
                _pool.Enqueue(new SendMark());
            }
            return _pool.Dequeue();
        }

        public static void Release(SendMark sendMark)
        {
            sendMark.pages.Clear();
            sendMark.data = null;
            _pool.Enqueue(sendMark);
        }
    }

    public class SendCache
    {
        private const int CACHE_PACKAGE_COUNT_MAX = 500;
        private const int CACHE_PAGE_LEN = 100;
        private static int CACHE_BUFF_LEN = CACHE_PAGE_LEN * CACHE_PACKAGE_COUNT_MAX;

        private uint _cacheSendDataIndex = 0;                               //缓存发送包队列第一个数据的发送序号
        private byte[] _cacheSendBytes = new byte[CACHE_BUFF_LEN];                       //缓存发送包队列 
        private Queue<int> _pagesPool = new Queue<int>();
        private Queue<SendMark> _cacheSendMarks = new Queue<SendMark>();

        public SendCache()
        {
            for (int i = 0; i < CACHE_PACKAGE_COUNT_MAX; i++)
            {
                _pagesPool.Enqueue(i);
            }
        }

        void ReleaseSendMark(SendMark sendMark)
        {
            for (int i = 0; i < sendMark.pages.Count; i++)
            {
                _pagesPool.Enqueue(sendMark.pages[i]);
            }
            SendMark.Release(sendMark);
        }

        void TakePages(int pageCount, SendMark sendMark)
        {
            while (pageCount > _pagesPool.Count)
            {
                ReleaseSendMark(_cacheSendMarks.Dequeue());
            }
            for (int i = 0; i < pageCount; i++)
            {
                sendMark.pages.Add(_pagesPool.Dequeue());
            }
        }

        public void CacheSendData(byte[] data)
        {
            var sendMark = SendMark.Create();
            sendMark.idx = ++_cacheSendDataIndex;
            sendMark.len = data.Length;
            int pageCount = (int)Mathf.Ceil(data.Length / (float)CACHE_PAGE_LEN);
            TakePages(pageCount, sendMark);
            for (int i = 0; i < sendMark.pages.Count; i++)
            {
                var page = sendMark.pages[i];
                int len = i == sendMark.pages.Count - 1 ? data.Length - i * CACHE_PAGE_LEN : CACHE_PAGE_LEN;
                Array.Copy(data, i * CACHE_PAGE_LEN, _cacheSendBytes, page * CACHE_PAGE_LEN, len); 
            }
            _cacheSendMarks.Enqueue(sendMark);
        }

        void CreateData(SendMark sendMark)
        {
            if (sendMark.data == null)
            {
                var data = new byte[sendMark.len];
                for (int i = 0; i < sendMark.pages.Count; i++)
                {
                    var page = sendMark.pages[i];
                    int len = i == sendMark.pages.Count - 1 ? data.Length - i * CACHE_PAGE_LEN : CACHE_PAGE_LEN;
                    Array.Copy(_cacheSendBytes, page * CACHE_PAGE_LEN, data, i * CACHE_PAGE_LEN, len); 
                }
                sendMark.data = data;
            }
        }

        Queue<SendMark> _tempCacheData = new Queue<SendMark>();
        public void GetSendDataFrom(uint idx, Queue<byte[]> output)
        {
            _tempCacheData.Clear();
            while (_cacheSendMarks.Count > 0)
            {
                var sendMark = _cacheSendMarks.Dequeue();
                _tempCacheData.Enqueue(sendMark);
                CreateData(sendMark);
                output.Enqueue(sendMark.data);
            }
            while (_tempCacheData.Count > 0)
            {
                _cacheSendMarks.Enqueue(_tempCacheData.Dequeue());
            }
        }

        public bool HasIndex(uint idx)
        {
            if (_cacheSendMarks.Count > 0)
            {
                var sm = _cacheSendMarks.Peek();
                if (sm.idx <= idx) return true;
            }
            return false;
        }

        public void ClearBefore(uint idx)
        {
            while (_cacheSendMarks.Count>0)
            {
                var sm = _cacheSendMarks.Peek();
                if (idx <= sm.idx) break;
                ReleaseSendMark(_cacheSendMarks.Dequeue());
            }
        }

        public void ClearAllCache()
        {
            _cacheSendDataIndex = 0;
            while (_cacheSendMarks.Count > 0)
            {
                ReleaseSendMark(_cacheSendMarks.Dequeue());
            }
        }

        public void Print()
        {
            uint max = 0;
            if (_cacheSendMarks.Count > 0)
            {
                var sm = _cacheSendMarks.Peek();
                max = sm.idx;
            }
            LoggerHelper.Info("_cacheSendDataIndex: " + _cacheSendDataIndex + " : " + _pagesPool.Count + " : " + _cacheSendMarks.Count + " : " + max);    
        }
    }
}