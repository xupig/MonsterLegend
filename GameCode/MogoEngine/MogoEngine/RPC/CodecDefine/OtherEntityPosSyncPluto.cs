﻿using System;
using System.Collections.Generic;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
    class OtherEntityPosSyncPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.x = VInt32.Instance.Decode(data, ref unLen, true);
            info.y = VInt32.Instance.Decode(data, ref unLen, true);
            info.z = VInt32.Instance.Decode(data, ref unLen, true);
            //UInt32 checkFlag = VUInt32.Instance.Decode(data, ref unLen, true);
            //info.checkFlag = checkFlag;
            
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_OTHER_ENTITY_POS_SYNC, info);
            }
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.OtherEntityPosSync, info);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static OtherEntityPosSyncPluto pluto = new OtherEntityPosSyncPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}