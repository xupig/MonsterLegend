﻿using MogoEngine.Events;
using System;
using GameLoader.Utils;

namespace MogoEngine.RPC
{
	public class ReConnectPluto : Pluto
	{
        Byte[] _encodeBuff = null;
        public Byte[] Encode(uint msgCnt, string key, Byte[] defMD5)
        {
            Push(VUInt16.Instance.Encode((UInt16)MSGIDType.MSGID_BASEAPP_CLIENT_RELOGIN));
            Push(VUInt32.Instance.Encode(msgCnt));
            Push(VString.Instance.Encode(key));
            var str = MD5Utils.FormatMD5(defMD5);
            Push(VString.Instance.Encode(str));

            if (_encodeBuff == null)
            {
                _encodeBuff = new Byte[m_unLen];
            }
            Buffer.BlockCopy(m_szBuff, 0, _encodeBuff, 0, m_unLen);
            EndPluto(_encodeBuff);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Send(MSGIDType.MSGID_BASEAPP_CLIENT_RELOGIN, key);
            }
            return _encodeBuff;
        }

        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            //UnityEngine.Debug.LogError("ReConnectPluto:DoDecode:" + (int)data[unLen + 0] + ":" + (int)data[unLen + 1] + ":" + (int)data[unLen + 2] + ":" + (int)data[unLen + 3]);
            //UInt16 msgID = (UInt16)VUInt16.Instance.Decode(data, ref unLen);
            var errID = VUInt8.Instance.Decode(data, ref unLen);
            var msgCnt = VUInt32.Instance.Decode(data, ref unLen, true);

            Arguments = new Object[2];
            Arguments[0] = errID;
            Arguments[1] = msgCnt;
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_BASEAPP_CLIENT_RELOGIN, Arguments);
            }
        }

        public override void HandleData()
        {
            int errID = (int)(byte)Arguments[0];
            uint msgCnt = (uint)Arguments[1];
            EventDispatcher.TriggerEvent<int, uint>(RPCEvents.ReConnectResp, errID, msgCnt);
        }

        static ReConnectPluto pluto = new ReConnectPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
	}
}
