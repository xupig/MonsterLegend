﻿using System;
using System.Collections.Generic;


namespace MogoEngine.RPC
{
    class EntityPosPullPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            //info.id = (uint)VUInt32.Instance.Decode(data, ref unLen); // eid
            //info.face = (byte)VUInt8.Instance.Decode(data, ref unLen);// rotation
            //info.x = (short)VInt16.Instance.Decode(data, ref unLen); //x
            //info.y = (short)VInt16.Instance.Decode(data, ref unLen); //y
            UInt16 x = VUInt16.Instance.Decode(data, ref unLen, true);
            UInt16 y = VUInt16.Instance.Decode(data, ref unLen, true);
            info.x = (short)x;
            info.y = (short)y;

            Arguments = new Object[1];
            Arguments[0] = info;
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.CLIENT_ENTITY_POS_PULL, Arguments);
            }
        }

        public override void HandleData()
        {
            //MogoWorld.thePlayer.SetEntityCellInfo(Arguments[0] as CellAttachedInfo);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static EntityPosPullPluto pluto = new EntityPosPullPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
