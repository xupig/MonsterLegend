﻿using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;

namespace MogoEngine.RPC
{
    public class AvatarAttriSyncPluto : Pluto
    {
        AttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetAttachedInfo();
            var entity = DefParser.Instance.GetEntityByName(CurrentEntity.Name);
            if (entity != null)
            {
                while (unLen < unEnd)
                {//还有数据就解析
                    var index = VUInt16.Instance.Decode(data, ref unLen, true);
                    EntityDefProperties prop;
                    var flag = entity.Properties.TryGetValue((ushort)index, out prop);
                    if (flag)
                    {
                        info.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                    }
                    else 
                    {
                        LoggerHelper.Error(index + "==" + flag);
                    }
                }
            }
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_AVATAR_ATTRI_SYNC, info.ToString());
            }
        }

        public override void HandleData()
        {
            // RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "AvatarAttriSync typeId=" + info.typeId + ",target_id=" + info.id+ ",info=" +info.ToString());
            EventDispatcher.TriggerEvent<AttachedInfo>(RPCEvents.AvatarAttriSync, info);
            PlutoDataObjectPool.instance.ReleaseAttachedInfo(info);
        }

        static AvatarAttriSyncPluto pluto = new AvatarAttriSyncPluto();
        /// <summary>
        /// 创建新AvatarAttriSyncPluto实例。
        /// </summary>
        /// <returns>AvatarAttriSyncPluto实例</returns>
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
