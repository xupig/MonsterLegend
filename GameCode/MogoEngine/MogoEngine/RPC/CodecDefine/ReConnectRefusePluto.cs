﻿using System;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
	class ReConnectRefusePluto : Pluto
	{
        public Byte[] Encode()
        {
            return null;
        }

        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent(RPCEvents.ReConnectRefuse);
        }

        static ReConnectRefusePluto pluto = new ReConnectRefusePluto();
        internal static Pluto Create()
        {
            return pluto;
        }
	}
}
