﻿using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;

namespace MogoEngine.RPC
{
    class OtherAttriSyncPluto : Pluto
    {
        AttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetAttachedInfo();
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); //entity unique id
            if (GetEntity != null)
            {
                var entity = GetEntity(info.id);
                if (entity != null)
                    while (unLen < unEnd)
                    {//还有数据就解析
                        var index = VUInt16.Instance.Decode(data, ref unLen, true);
                        EntityDefProperties prop;
                        var flag = entity.Properties.TryGetValue((ushort)index, out prop);
                        if(flag)
                        {
                            info.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                        }
                    }
            }

            Arguments = new object[1] { info };
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_OTHER_ENTITY_ATTRI_SYNC, Arguments);
            }
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent<AttachedInfo>(RPCEvents.OtherAttriSync, info);
            PlutoDataObjectPool.instance.ReleaseAttachedInfo(info);
        }

        static OtherAttriSyncPluto pluto = new OtherAttriSyncPluto();
        /// <summary>
        /// 创建新EntityAttachedPluto实例。
        /// </summary>
        /// <returns>EntityAttachedPluto实例</returns>
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
