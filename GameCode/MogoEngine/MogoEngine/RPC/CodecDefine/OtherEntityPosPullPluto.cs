﻿
using System;
using System.Collections.Generic;

namespace MogoEngine.RPC
{
    class OtherEntityPosPullPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            //info.face = (byte)VUInt8.Instance.Decode(data, ref unLen);// rotation
            UInt16 x = VUInt16.Instance.Decode(data, ref unLen, true);
            UInt16 y = VUInt16.Instance.Decode(data, ref unLen, true);
            info.x = (short)x;
            info.y = (short)y;

            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.CLIENT_OTHER_ENTITY_POS_PULL, info);
            }
        }

        public override void HandleData()
        {
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static OtherEntityPosPullPluto pluto = new OtherEntityPosPullPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
