#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：ServerProxy
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.2.16
// 模块描述：远程服务代理类
//==========================================*/
#endregion
using System;
using System.Collections.Generic;

using GameLoader.Utils;
using GameLoader.Utils.Network;
using MogoEngine.Events;
using System.Threading;
using MogoEngine.Mgrs;
using Time = UnityEngine.Time;
using System.Collections;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 远程服务控制类。
    /// </summary>
    public abstract class ServerProxy
    {
        public static bool SomeToLocal = false;//标记是否有些请求发给虚拟服
        private static ServerProxy m_instance;
        private static RemoteProxy m_remoteProxy;
        protected static LocalProxy m_localProxy;
        protected static TCPClientWorker m_tcpWorker;
        public static TCPClientWorker tcpWorker
        {
            get { return m_tcpWorker; }
        }

        public Action<LoginResult> LoginResp;
        public Action BackToChooseServer;
        protected bool _enableConnect = false;
        public bool pauseHandleData = false;
        public string reConnectKey;
        /// <summary>
        /// 控制类实例。
        /// </summary>
        public static ServerProxy Instance
        {
            get { return m_instance; }
        }

        static ServerProxy()
        {
            m_remoteProxy = new RemoteProxy();
            m_localProxy = new LocalProxy();
            SwitchToRemote();
        }

        public static void SwitchToLocal()
        {
            m_instance = m_localProxy;
        }

        public static void SwitchToRemote()
        {
            m_instance = m_remoteProxy;
        }

        public void Init(Action callback)
        {
            Action action = () =>
            {
                DefParser.Instance.InitEntityData();
                if (callback != null)
                    callback();
            };

            action();
        }

        /// <summary>
        /// 连接远程服务。
        /// </summary>
        /// <param name="ip">服务器IP</param>
        /// <param name="port">服务端口</param>
        public abstract Boolean Connect(string ip, int port, Action<bool> connectedCB);

        public abstract void Disconnect();

        public bool Connected
        {
            get { return m_tcpWorker == null ? false : m_tcpWorker.Connected(); }
        }

        /// <summary>
        /// 调用远程方法。
        /// </summary>
        /// <param name="entityName">实体名称</param>
        /// <param name="funcName">方法名称</param>
        /// <param name="args">参数列表</param>
        public abstract void RpcCall(string funcName, params Object[] args);

        public abstract void Login(params string[] args);

        public abstract void Login(string _passport, string _password, string _loginArgs);

        public abstract void BaseLogin(String token);

        public abstract void Move(byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z);

        public abstract void OthersMove(uint entityID, byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z);

        public abstract void BossPartMove(uint entityID, byte partID, int x, int y, int z);

        public abstract void SendReConnectKey(string key, uint msgCnt);

        public abstract void CheckDefMD5(Byte[] bytes);

        public abstract void Process();

        public abstract void ReConnect();

        public abstract void SetHandlePerPackageSpaceTime(float value);

        /// <summary>
        /// 监听远程回调。
        /// </summary>
        public abstract void Update();

        public abstract void Release();

        /// <summary>
        /// 设置网络断线超时检查状态
        /// </summary>
        /// <param name="state">[true:开启断线检查,false:关闭断线检查]</param>
        public abstract void enableTimeout(bool state);

        /// <summary>
        /// 重连开关设置[true:开启,false:关闭]
        /// </summary>
        /// <param name="enable"></param>
        public abstract void IsEnableReconnect(bool enable);

        /// <summary>
        /// 设置每帧
        /// </summary>
        /// <param name="enable"></param>
        public abstract void MaxHandleDataTimePerFrame(float spaceTime);

        public abstract void PrintCacheData();
        
    }
    
    public class RemoteProxy : ServerProxy
    {
        private float _maxHandleDataTimePerFrame = 0.01f;
        private Dictionary<string, string> localHandles;

        static public string baseIP = "";
        static public int basePort = 0;

        private uint _receiveCount = 0; //断线重连相关的,记录已接受的服务器包数量
        private SendCache _sendCache;

        internal RemoteProxy()
        {
            m_tcpWorker = new TCPClientWorker();
            m_tcpWorker.OnNetworkDisconnected = CloseHandler;
            _sendCache = new SendCache();
            InitNeedSeriNumHead();
            InitNeedIgnoreHead();
            m_tcpWorker.statusListener(OnNetStatusListener);
            localHandles = new Dictionary<string, string>();
            localHandles.Add("MissionReq", "MissionReq");
            localHandles.Add("CliEntityActionReq", "CliEntityActionReq");
            localHandles.Add("CliEntitySkillReq", "CliEntitySkillReq");
            localHandles.Add("UseSkillReq", "UseSkillReq");
            localHandles.Add("UseHpBottleReq", "UseHpBottleReq");

            EventDispatcher.AddEventListener<LoginResult>(RPCEvents.Login, OnLoginResp);
            EventDispatcher.AddEventListener<string, int, string>(RPCEvents.BaseLogin, OnBaseLogin);

            EventDispatcher.AddEventListener<string>(RPCEvents.ReConnectKey, SetReConnectKey);
            EventDispatcher.AddEventListener<int, uint>(RPCEvents.ReConnectResp, ReConnectResp);
        }

        private void InitNeedSeriNumHead()
        {
            m_tcpWorker.AddNeedSeriNumHead((int)MSGIDType.BASEAPP_CLIENT_RPCALL);
            m_tcpWorker.AddNeedSeriNumHead((int)MSGIDType.BASEAPP_CLIENT_RPC2CELL_VIA_BASE);
        }

        private HashSet<int> _ignoreHeadSet = null;
        private HashSet<int> GetIgnoreHeadSet()
        {
            if (_ignoreHeadSet == null)
            {
                _ignoreHeadSet = new HashSet<int>();
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_ENTITY_ATTACHED);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_AVATAR_ATTRI_SYNC);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_ENTITY_CELL_ATTACHED);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_OTHER_ENTITY_ATTRI_SYNC);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_OTHER_ENTITY_POS_SYNC);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_AOI_NEW_ENTITY);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_AOI_DEL_ENTITY);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_AOI_ENTITIES);
                _ignoreHeadSet.Add((int)MSGIDType.MSGID_CLIENT_RELOGIN_RESP);
            }
            return _ignoreHeadSet;
        }

        private bool IsIgnore(int msgID)
        {
            var ignoreHeadList = GetIgnoreHeadSet();
            return ignoreHeadList.Contains(msgID);
        }

        /*
        private bool CheckCanBeCache(byte[] data)
        {
            bool result = false;
            Int32 unLen = 0;
            var msgId = (MSGIDType)VUInt16.Instance.Decode(data, ref unLen, true);
            result = !IsIgnore((int)msgId);
            //LoggerHelper.Info("CheckCanBeCache IsIgnore:  " + msgId + " : " + result);
            return result;
        }*/

        private void InitNeedIgnoreHead()
        {
            HashSet<int> ignoreHeadSet = GetIgnoreHeadSet();
            foreach (int head in ignoreHeadSet)
            {
                m_tcpWorker.AddNeedIgnoreHead(head);
            }
        }

        private void OnNetStatusListener(string status)
        {
            LoggerHelper.Info("网络状态:" + status);
            EventDispatcher.TriggerEvent(RPCEvents.NetStatus, status); //事件将被PlayerTimerManager.cs监听
        }

        /// <summary>
        /// 设置网络断线超时检查状态
        /// </summary>
        /// <param name="state">[true:开启断线检查,false:关闭断线检查]</param>
        public override void enableTimeout(bool state)
        {
            if (m_tcpWorker != null) m_tcpWorker.enableTimeout(state);
        }

        /// <summary>
        /// 重连开关设置
        /// </summary>
        /// <param name="enable"></param>
        public override void IsEnableReconnect(bool enable)
        {
            if (_enableConnect == enable) return;
            _enableConnect = enable;
            if (_enableConnect)
            {
                //EventDispatcher.AddEventListener<int, uint>(RPCEvents.ReConnectResp, ReConnectResp);
            }
            else
            {
                //EventDispatcher.RemoveEventListener<int, uint>(RPCEvents.ReConnectResp, ReConnectResp);
            }
        }

        public override void MaxHandleDataTimePerFrame(float spaceTime)
        {
            _maxHandleDataTimePerFrame = spaceTime;
        }

        //private int reConnectCnt = 0;
        //private float closeTimeStamp = 0;
        private int connectCnt = 0; //防止一连上又断开引起的问题
        //private float preCloseTime = 0;
        private void CloseHandler()
        {
            LoggerHelper.Info(string.Format("Server Disconnect:baseIP {0} basePort {1} _enableConnect {2}", baseIP, basePort, _enableConnect));
            EntityMgr.Instance.DestroyAllOtherServerEntities();
            EventDispatcher.TriggerEvent(RPCEvents.NetStatusEventClose);
        }

        private void confirmCallback(bool result)
        {
            //result=true:退出游戏,false:继续游戏
            //MogoGlobleUIManager.Instance.ConfirmHide();
            LoggerHelper.Debug("result:" + result);
            if (result)
            {//退出游戏
                if (m_tcpWorker != null) m_tcpWorker.Close();
                //PlatformSdkManager.Instance.RestartGame();
            }
        }

        public override void ReConnect()
        {
            //LoggerHelper.Info(string.Format("-ReConnect1() [{0}:{1}]times", baseIP, basePort));
            connectCnt = 0;
            EventDispatcher.TriggerEvent(RPCEvents.NetStatusEventBegin);

            //LoggerHelper.Info(string.Format("-reconnect() [{0}:{1}] {2}times", baseIP, basePort, connectCnt));
            Action<bool> cb = (connected) =>
            {
                if (connected)
                {
                    LoggerHelper.Info("ReConnect:::" + this.reConnectKey + " in " + connectCnt + " times" + " receiveCount:" + _receiveCount);
                    SendReConnectKey(this.reConnectKey, _receiveCount);
                }
                else
                {
                    LoggerHelper.Info(string.Format("-reconnect() [{0}:{1}] {2}times fail", baseIP, basePort, connectCnt));
                }
                if (!m_tcpWorker.Connected())
                {
                    //LoggerHelper.Error("RPCEvents.ReConnectFailed");
                    EventDispatcher.TriggerEvent(RPCEvents.ReConnectFailed);  //重连失败
                }
                EventDispatcher.TriggerEvent(RPCEvents.NetStatusEventEnd);
            };
            ServerProxy.Instance.Connect(baseIP, basePort, cb);
        }

        /// <summary>
        /// 连接远程服务。
        /// </summary>
        /// <param name="ip">服务器IP</param>
        /// <param name="port">服务端口</param>
        public override Boolean Connect(string ip, int port, Action<bool> connectedCB)
        {
            try
            {
                LoggerHelper.Info("connect : " + ip + " p: " + port);
                m_tcpWorker.Connect(ip, port, connectedCB);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("connect error: " + ex.ToString());
                LoggerHelper.Except(ex);
            }
            return false;
        }

        public override void Disconnect()
        {
            if (m_tcpWorker.Connected())
            {
                m_tcpWorker.Close();
            }
            LoggerHelper.Debug("connect disconnect");
        }

        private bool ToLocal(string funName, params Object[] args)
        {
            if (!localHandles.ContainsKey(funName))
            {
                return false;
            }
            if (funName != "MissionReq")
            {
                return true;
            }
            switch ((byte)args[0])
            {
                case 2:
                case 20:
                case 71:
                case 222:
                    return false;

                default:
                    return true;
            }
        }

        /// <summary>
        /// 调用远程方法。
        /// </summary>
        /// <param name="entityName">实体名称</param>
        /// <param name="funcName">方法名称</param>
        /// <param name="args">参数列表</param>
        public override void RpcCall(string funcName, params Object[] args)
        {
            if (SomeToLocal && ToLocal(funcName, args))
            {//转发给虚拟服
                ServerProxy.m_localProxy.RpcCall(funcName, args);
                return; 
            }
            try
            {
                //if (funcName != "GetServerTimeReq")
                //LoggerHelper.Debug("RPC: " + funcName);
                if (Pluto.CurrentEntity != null)
                {
                    bool toCell = false;
                    var func = Pluto.CurrentEntity.TryGetBaseMethod(funcName);
                    if (func == null)
                    {
                        toCell = true;
                        func = Pluto.CurrentEntity.TryGetCellMethod(funcName);
                    }
                    if (func != null)
                    {
                        var rpcCall = new RpcCallPluto();
                        rpcCall.svrMethod = func;
                        rpcCall.toCell = toCell;
                        var result = rpcCall.Encode(args);
                        this.Send(result);
                        CommunicationStatistics.RecordRpcCallStatistics(funcName, result);
                    }
                    else
                        LoggerHelper.Warning("RPCCall: Can not find function: " + funcName);
                }
                else
                    LoggerHelper.Warning("Pluto.CurrentEntity is not set.");
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("Rpc call error: " + funcName + " " + ex.Message);
            }
        }

        public override void Login(params string[] args)
        {
            try
            {
                if (!m_tcpWorker.Connected())
                    LoggerHelper.Error("Not connected!!");
                var u = new LoginPluto();
                var result = u.Encode(args);
                this.Send(result);
                LoggerHelper.Error("send loginPluto end");
            }
            catch (Exception ex)
            {
                LoggerHelper.Debug("login rst excp: " + ex.ToString());
                LoggerHelper.Except(ex);
            }
        }

        public override void Login(string _passport, string _password, string _loginArgs)
        {
            try
            {
                if (!m_tcpWorker.Connected())
                    LoggerHelper.Error("Not connected!!");
                var u = new LoginPluto();
                var result = u.Encode(_passport, _password, _loginArgs);
                this.Send(result);
                LoggerHelper.Debug("send loginPluto end");
            }
            catch (Exception ex)
            {
                LoggerHelper.Debug("login rst excp: " + ex.ToString());
                LoggerHelper.Except(ex);
            }
        }

        public override void BaseLogin(String token)
        {
            ClearAllCache();
            var defMd5 = DefParser.Instance.m_defContentMD5;
            var u = new BaseLoginPluto();
            var result = u.Encode(token, defMd5);
            this.Send(result);
            LoggerHelper.Debug("BaseLogin " + token);
        }

        static MovePluto _movePluto = new MovePluto();
        public override void Move(byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z)
        {
            try
            {
                var result = _movePluto.Encode(eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z);
                this.Send(result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        static OthersMovePluto _othersMovePluto = new OthersMovePluto();
        public override void OthersMove(uint entityID, byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z)
        {
            try
            {
                var result = _othersMovePluto.Encode(entityID, eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z);
                this.Send(result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        static BossPartMovePluto _bossPartMovePluto = new BossPartMovePluto();
        public override void BossPartMove(uint entityID, byte partID, int x, int y, int z)
        {
            try
            {
                var result = _bossPartMovePluto.Encode(entityID, partID, x, y, z);
                this.Send(result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public override void SendReConnectKey(string key, uint msgCnt)
        {
            try
            {
                var u = new ReConnectPluto();
                var defMd5 = DefParser.Instance.m_defContentMD5;
                var result = u.Encode(msgCnt, key, defMd5);
                //LoggerHelper.Error("SendReConnectKey:" + msgCnt + " :: " + key + " :: " + defMd5);
                this.Send(result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        void Send(byte[] result)
        {
            CacheSendData(result);
            m_tcpWorker.Send(result);
        }

        public override void CheckDefMD5(Byte[] bytes)
        {
            try
            {
                var u = new CheckDefMD5Pluto();
                var result = u.Encode(bytes);
                this.Send(result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public override void Process()
        {
            m_tcpWorker.Process();
        }

        void SetReceiveCount(byte[] data, int idx, int len)
        { 
            if (data != null)
            {
                Int32 unLen = idx;
                var msgId = (MSGIDType)VUInt16.Instance.Decode(data, ref unLen, true);
                if (IsIgnore((int)msgId)) return;
                _receiveCount++;
                //LoggerHelper.Info("<----ReceiveData:" + _receiveCount + "::" + GetBytesString(data, idx, len));
            }
        }

        float _handlePerPackageSpaceTime = 0; //测试用：处理包的强制时间间隔
        float _handlePackageLastTime = 0;     //测试用：处理包的最后一次时间点
        public override void SetHandlePerPackageSpaceTime(float value)
        {
            _handlePerPackageSpaceTime = value;
        }

        /// <summary>
        /// 监听远程回调。
        /// </summary>
        byte[] _handleBuff = new byte[0];
        Queue<ReceiveMark> _handleQueue = new Queue<ReceiveMark>();
        public override void Update()
        {
#if !UNITY_ANDROID && !UNITY_IPHONE
            if (_handlePerPackageSpaceTime > 0)
            {
                if ((Time.realtimeSinceStartup - _handlePackageLastTime) < _handlePerPackageSpaceTime)
                {
                    return;
                }
            }
#endif
            if (pauseHandleData) return;
			if (!m_canRevieve) return;
            if (m_tcpWorker.Connected())
            {
                var beginTime = Time.realtimeSinceStartup;
                if (_handleQueue.Count == 0)
                {
                    m_tcpWorker.Recv(ref _handleBuff, _handleQueue);
                }
                while (_handleQueue.Count > 0)
                {
                    var mark = _handleQueue.Dequeue();
                    SetReceiveCount(_handleBuff, mark.idx, mark.len);
                    DataHandler(_handleBuff, mark.idx, mark.len);
                    if ((Time.realtimeSinceStartup - beginTime) > _maxHandleDataTimePerFrame) break;
                    if (pauseHandleData) break;
#if !UNITY_ANDROID && !UNITY_IPHONE
                    if (_handlePerPackageSpaceTime > 0)
                    {
                        _handlePackageLastTime = Time.realtimeSinceStartup;
                        break;
                    }
#endif
                }
            }
            else {
                if (_handleQueue.Count > 0)
                {
                    _handleQueue.Clear();
                }
            }
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.DoRecord();
            }
        }

        private bool DataHandler(byte[] data, int idx, int len)
        {
            try
            {
                var pluto = Pluto.Decode(data, idx, len);
                pluto.HandleData();
            }
            catch(Exception ex)
            {
                LoggerHelper.Error("DataReceiver:Exception:\n\n" + ex.Message + "\n\n" + ex.StackTrace);
            }
            return true;
        }

        private bool m_canRevieve = true;
        public void SetRecieveState(bool canRecieve)
        {
            m_canRevieve = canRecieve;
        }

        public void RevieveOne()
        {
            if (_handleQueue.Count == 0)
            {
                m_tcpWorker.Recv(ref _handleBuff, _handleQueue);
            }
            if (_handleQueue.Count == 0)
            {
                return;
            }
            else
            {
                //LoggerHelper.Debug("msg packet len: " + data.Length.ToString());
                //count++;
                var mark = _handleQueue.Dequeue();
                SetReceiveCount(_handleBuff, mark.idx, mark.len);
                DataHandler(_handleBuff, mark.idx, mark.len);
                RPCMsgLogManager.DoRecord();
            }
        }

        public int GetRecieveQueueCount()
        {
            return m_tcpWorker.GetRecieveQueueCount();
        }

        public override void Release()
        {
            m_tcpWorker.Release();
        }

        // 帐号登录回调函数
        public void OnLoginResp(LoginResult result)
        {
            if (result != LoginResult.SUCCESS)
            {
                LoggerHelper.Error("result != LoginResult.SUCCESS");
            }
            else
            {

            }
        }

        public string GetBytesString(byte[] bytes)
        {
            string temp = "";
            for(int i = 0;i<bytes.Length;i++){
                temp += bytes[i] + ",";
            }
            return temp;
        }

        public string GetBytesString(byte[] bytes, int idx, int len)
        {
            string temp = "";
            for (int i = idx; i < idx + len; i++)
            {
                temp += Convert.ToString(bytes[i], 16) + ",";
            }
            return temp;
        }

        private string GetRPCBytesString(byte[] data)
        {
            Int32 unLen = 0;
            var msgId = (MSGIDType)VUInt16.Instance.Decode(data, ref unLen, true);
            if (msgId == MSGIDType.BASEAPP_CLIENT_RPC2CELL_VIA_BASE || msgId == MSGIDType.BASEAPP_CLIENT_RPCALL)
            {
                var funcID = VUInt16.Instance.Decode(data, ref unLen, true);
                EntityDefMethod method = null;
                if (msgId == MSGIDType.BASEAPP_CLIENT_RPC2CELL_VIA_BASE)
                {
                    method = Pluto.CurrentEntity.TryGetCellMethod(funcID);
                }
                else {
                    method = Pluto.CurrentEntity.TryGetBaseMethod(funcID);
                }
                return GetBytesString(data) + "\n" + msgId + " : " + method.FuncName;
            }
            else {
                return GetBytesString(data) + "\n" + msgId ;
            }
        }

        // 协议处理函数： 登录 baseapp
        public void OnBaseLogin(string ip, int port, string token)
        {
            baseIP = string.Empty;
            basePort = 0;
            ServerProxy.Instance.Disconnect();
            LoggerHelper.Info("OnBaseLogin: " + ip + " port: " + port);
            baseIP = ip;
            basePort = port;
            ServerProxy.Instance.Connect(ip, port, (connected) =>
            {
                if (connected)
                {
                    ServerProxy.Instance.BaseLogin(token);
                }
            });
        }



        //*******************************************断线重连相关******************************************************

        private void CacheSendData(byte[] data)
        {
            /*
            if (!CheckCanBeCache(data))
            {
                UnityEngine.Debug.LogError("CheckCanBeCache false:" + GetBytesString(data));
                return;
            }*/
            try
            {
                _sendCache.CacheSendData(data);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("CacheSendData:ERROR:" + " \n\n " + ex.Message + " \n\n " + ex.StackTrace);
            }
        }

        public void ClearAllCache()
        {
            _receiveCount = 0;
            _sendCache.ClearAllCache();
        }

        Queue<byte[]> tempCacheData = new Queue<byte[]>();
        /**发送滞留包**/
        public void SendWaitPack(uint sendDataIndex)
        {
            _sendCache.ClearBefore(sendDataIndex);
            tempCacheData.Clear();
            _sendCache.GetSendDataFrom(sendDataIndex, tempCacheData);
            while (tempCacheData.Count > 0)
            {
                var data = tempCacheData.Dequeue();
                //LoggerHelper.Error("resend:" + GetRPCBytesString(data));
                m_tcpWorker.Send(data);
            }
        }

        public override void PrintCacheData()
        {
            tempCacheData.Clear();
            _sendCache.GetSendDataFrom(0, tempCacheData);
            _sendCache.Print();
            while (tempCacheData.Count > 0)
            {
                var data = tempCacheData.Dequeue();
                LoggerHelper.Info(GetBytesString(data));
            }
        }

        public void SetReConnectKey(string key)
        {
            this.reConnectKey = key;
        }

        /*
        --断线重连错误码
            1 -- 连接不成功
            2 -- 密码不正确
            3 -- 重登陆时间已过期
            4 -- 实体不存在
            5 -- 连接未断开
            6 -- 消息缓存已超上限 
        */
        public void ReConnectResp(int errID, uint msgCnt)
        {
            if (errID > 0 || !_sendCache.HasIndex(msgCnt))
            {
                LoggerHelper.Error("ReConnectResp::Error:" + errID + "  msgCnt:" + msgCnt);
                EventDispatcher.TriggerEvent(RPCEvents.ReConnectRejected);  //重连失败
            }
            else {
                LoggerHelper.Info("ReConnectSucceeded:" + errID + ":" + msgCnt);
                SendWaitPack(msgCnt);
                EventDispatcher.TriggerEvent(RPCEvents.ReConnectSucceeded);  //重连成功
            }
        }
    }

    public class LocalProxy : ServerProxy
    {
        /// <summary>
        /// 连接远程服务。
        /// </summary>
        /// <param name="ip">服务器IP</param>
        /// <param name="port">服务端口</param>
        public override Boolean Connect(string ip, int port, Action<bool> connectedCB) { return false; }

        public override void Disconnect() { }

        /// <summary>
        /// 调用远程方法。
        /// </summary>
        /// <param name="entityName">实体名称</param>
        /// <param name="funcName">方法名称</param>
        /// <param name="args">参数列表</param>
        public override void RpcCall(string funcName, params Object[] args) { }

        public override void Login(params string[] args) { }

        public override void Login(string _passport, string _password, string _loginArgs) { }

        public override void BaseLogin(String token) { }

        public override void Move(byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z) { }

        public override void BossPartMove(uint entityID, byte partID, int x, int y, int z) { }

        public override void OthersMove(uint entityID, byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z) { }

        public override void SendReConnectKey(string key, uint msgCnt) { }

        public override void CheckDefMD5(Byte[] bytes) { }

        public override void Process() { }

        public override void ReConnect(){ }

        public override void SetHandlePerPackageSpaceTime(float value) { }
        /// <summary>
        /// 监听远程回调。
        /// </summary>
        public override void Update() { }

        public override void Release() { }

        public override void enableTimeout(bool state) { }

        public override void IsEnableReconnect(bool enable) { }

        public override void MaxHandleDataTimePerFrame(float spaceTime) { }

        public override void PrintCacheData()
        { }
    }
}