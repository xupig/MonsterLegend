﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MogoEngine.RPC
{
    public static class CommunicationStatistics
    {
        static public Dictionary<string, int> rpcCallStatistics = new Dictionary<string, int>();
        static public void RecordRpcCallStatistics(string func, byte[] content)
        {
            if (!UnityPropUtils.IsEditor) return;
            if (!rpcCallStatistics.ContainsKey(func))
            {
                rpcCallStatistics[func] = 0;
            }
            rpcCallStatistics[func] += content.Length;
        }

        static public Dictionary<string, int> rpcRespStatistics = new Dictionary<string, int>();
        static public void RecordRpcRespStatistics(string func, byte[] content)
        {
            if (!UnityPropUtils.IsEditor) return;
            if (!rpcRespStatistics.ContainsKey(func))
            {
                rpcRespStatistics[func] = 0;
            }
            rpcRespStatistics[func] += content.Length;
        }
    }
}
