﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VUInt8
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：8位无符号整数（byte）。
//==========================================*/
#endregion
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 8位无符号整数（byte）。
    /// </summary>
    public class VUInt8 : VObject
    {
        private static VUInt8 m_instance = new VUInt8();
        public static VUInt8 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VUInt8()
            : base(typeof(byte), VType.V_UINT8, 1)//Marshal.SizeOf(typeof(byte)))
        {
        }
        public VUInt8(Object vValue)
            : base(typeof(byte), VType.V_UINT8, vValue)
        {
        }

        static byte[][] _cacheBytes = new byte[byte.MaxValue][];
        public byte[] Encode(byte vValue)
        {
            if (_cacheBytes[vValue] == null)
            {
                _cacheBytes[vValue] = new byte[1] { vValue };
            }
            return _cacheBytes[vValue];
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public Byte Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            return data[index++];
        }

    }
}