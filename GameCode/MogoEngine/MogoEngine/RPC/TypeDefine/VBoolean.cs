﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VBoolean
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.17
// 模块描述：布尔数（Boolean）。
//==========================================*/
#endregion
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 布尔数（Boolean）。
    /// </summary>
    public class VBoolean : VObject
    {
        private static VBoolean m_instance = new VBoolean();
        public static VBoolean Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VBoolean()
            : base(typeof(Boolean), VType.V_BLOB, 1)
        {
        }
        public VBoolean(Object vValue)
            : base(typeof(Boolean), VType.V_BLOB, vValue)
        {
        }

        public byte[] Encode(bool vValue)
        {
            var result = BitConverter.GetBytes(vValue);
            //Array.Reverse(result);
            return result;
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public bool Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            bool result = BitConverter.ToBoolean(data, index);
            index += VTypeLength;
            return result;
        }
    }
}