﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VUInt32
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：32位无符号整数（UInt32）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 32位无符号整数（UInt32）。
    /// </summary>
    public class VUInt32 : VObject
    {
        private static VUInt32 m_instance = new VUInt32();
        public static VUInt32 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VUInt32()
            : base(typeof(UInt32), VType.V_UINT32, 4)//Marshal.SizeOf(typeof(UInt32)))
        {
        }
        public VUInt32(Object vValue)
            : base(typeof(UInt32), VType.V_UINT32, vValue)
        {
        }

        static byte[][] _cacheBytes = new byte[byte.MaxValue][];
        public byte[] Encode(UInt32 vValue)
        {
            if (vValue >= 0 && vValue < byte.MaxValue)
            {
                if (_cacheBytes[vValue] == null)
                {
                    _cacheBytes[vValue] = BitConverterExtend.GetBytes(vValue);
                }
                return _cacheBytes[vValue];
            }
            return BitConverterExtend.GetBytes(vValue);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public UInt32 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            UInt32 result = BitConverterExtend.ToUInt32(data, index);
            index += VTypeLength;
            return result;
        }
    }
}