﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VUInt16
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：16位无符号整数（UInt16）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 16位无符号整数（UInt16）。
    /// </summary>
    public class VUInt16 : VObject
    {
        private static VUInt16 m_instance = new VUInt16();

        public static VUInt16 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VUInt16()
            : base(typeof(UInt16), VType.V_UINT16, 2)//Marshal.SizeOf(typeof(UInt16)))
        {
        }
        public VUInt16(Object vValue)
            : base(typeof(UInt16), VType.V_UINT16, vValue)
        {
        }

        static byte[][] _cacheBytes = new byte[byte.MaxValue][];
        public byte[] Encode(UInt16 vValue)
        {
            if (vValue >= 0 && vValue < byte.MaxValue)
            {
                if (_cacheBytes[vValue] == null)
                {
                    _cacheBytes[vValue] = BitConverterExtend.GetBytes(vValue);
                }
                return _cacheBytes[vValue];
            }
            return BitConverterExtend.GetBytes(vValue);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        /// <summary>
        /// 返回强类型，
        /// fakeFlag用于区分重载
        /// </summary>
        /// <param name="data"></param>
        /// <param name="index"></param>
        /// <param name="fakeFlag"></param>
        /// <returns></returns>
        public UInt16 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            UInt16 result = BitConverterExtend.ToUInt16(data, index);
            index += VTypeLength;
            return result;
        }
    }
}