﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VFloat
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.17
// 模块描述：字符串（String）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 32位浮点数（float）。
    /// </summary>
    public class VFloat : VObject
    {
        private static VFloat m_instance = new VFloat();
        public static VFloat Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VFloat()
            : base(typeof(float), VType.V_FLOAT32, 4)//Marshal.SizeOf(typeof(float)))
        {
        }
        public VFloat(Object vValue)
            : base(typeof(float), VType.V_FLOAT32, vValue)
        {
        }

        public byte[] Encode(object vValue)
        {
            var result = BitConverterExtend.GetBytes(Convert.ToSingle(vValue));
            ////Array.Reverse(result);
            return result;
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public float Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            float result = BitConverterExtend.ToSingle(data, index);
            index += VTypeLength;
            return result;
        }

    }
}