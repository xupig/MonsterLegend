﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VInt32
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：32位整数（Int32）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 32位整数（Int32）。
    /// </summary>
    public class VInt32 : VObject
    {
        private static VInt32 m_instance = new VInt32();
        public static VInt32 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VInt32()
            : base(typeof(Int32), VType.V_INT32, 4)//Marshal.SizeOf(typeof(Int32)))
        {
        }
        public VInt32(Object vValue)
            : base(typeof(Int32), VType.V_INT32, vValue)
        {
        }

        static int CACHE_SIZE = 15000;//目前先定15000 对应15000厘米，主要针对坐标同步做优化cache,目前场景普遍大小150*150平方米
        static byte[][] _cacheBytes = new byte[CACHE_SIZE][];
        public byte[] Encode(Int32 vValue)
        {
            if (vValue >= 0 && vValue < CACHE_SIZE)
            {
                if (_cacheBytes[vValue] == null)
                {
                    _cacheBytes[vValue] = BitConverterExtend.GetBytes(vValue);
                }
                return _cacheBytes[vValue];
            }
            return BitConverterExtend.GetBytes(vValue);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public Int32 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            Int32 result = BitConverterExtend.ToInt32(data, index);
            index += VTypeLength;
            return result;
        }
    }
}