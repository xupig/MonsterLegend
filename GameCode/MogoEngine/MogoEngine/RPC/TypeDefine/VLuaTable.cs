﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VLuaTable
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.3.29
// 模块描述：Lua Table（LuaTable）。
//==========================================*/
#endregion
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;

using System.Linq;

using GameLoader.Utils.CustomType;
using GameLoader.Utils;

namespace MogoEngine.RPC
{
    /// <summary>
    /// Lua Table（LuaTable）。
    /// </summary>
    public class VLuaTable : VObject
    {
        private static VLuaTable m_instance = new VLuaTable();
        public static VLuaTable Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VLuaTable()
            : base(typeof(LuaTable), VType.V_LUATABLE, 0)
        {
        }
        public VLuaTable(Object vValue)
            : base(typeof(LuaTable), VType.V_LUATABLE, vValue)
        {
        }

        public byte[] Encode(Object vValue)
        {
            LuaTable value;
            CommonUtils.PackLuaTable(vValue, out value);
            var s = CommonUtils.PackLuaTable(value);
            return VString.Instance.Encode(s);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            byte[] strData = CutLengthHead(data, ref index);
            LuaTable luaTable;
            return CSLuaTableUtils.ParseLuaTable(strData, out luaTable) ? luaTable : null;

        }
    }
}
