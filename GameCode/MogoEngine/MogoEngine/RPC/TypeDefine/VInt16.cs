﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VInt16
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：16位整数（Int16）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 16位整数（Int16）。
    /// </summary>
    public class VInt16 : VObject
    {
        private static VInt16 m_instance= new VInt16();
        public static VInt16 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VInt16()
            : base(typeof(Int16), VType.V_INT16, 2)//Marshal.SizeOf(typeof(Int16)))
        {
        }
        public VInt16(Object vValue)
            : base(typeof(Int16), VType.V_INT16, vValue)
        {
        }

        static byte[][] _cacheBytes = new byte[byte.MaxValue][];
        public byte[] Encode(Int16 vValue)
        {
            if (vValue >= 0 && vValue < byte.MaxValue)
            {
                if (_cacheBytes[vValue] == null)
                {
                    _cacheBytes[vValue] = BitConverterExtend.GetBytes(vValue);
                }
                return _cacheBytes[vValue];
            }
            return BitConverterExtend.GetBytes(vValue);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public Int16 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            Int16 result = BitConverterExtend.ToInt16(data, index);
            index += VTypeLength;
            return result;
        }
    }
}