﻿using System;
using System.Collections.Generic;
using System.Text;
using GameLoader.Utils;
using MogoEngine.RPC;
using MogoEngine.Mgrs;

namespace MogoEngine
{
    public static class MogoWorld
    {
        static private EntityMgr s_entityMgr;

        static public void Init()
        {
            DefParser.Instance.InitEntityData();
            s_entityMgr = EntityMgr.Instance;
        }

        //获取当前玩家(Player)实体
        public static Entity Player
        {
            get { return EntityMgr.Instance.CurrentEntity; }
        }

        //获取当前实体字典
        public static Dictionary<uint, Entity> Entities
        {
            get { return EntityMgr.Instance.GetEntitys(); }
        }

        //获取当前实体字典
        public static List<uint> EntityIDs
        {
            get { return EntityMgr.Instance.GetEntityIDs(); }
        }

        //获取指定ID实体
        public static Entity GetEntity(uint id)
        {
            return EntityMgr.Instance.GetEntity(id);
        }

        //判断是否是客户端创建的实体
        public static bool IsClientEntity(Entity entity)
        {
            return EntityMgr.Instance.IsClientEntity(entity);
        }

        //创建客户端实体
        public static Entity CreateEntity(string entityTypeName, Action<Entity> attrSet = null)
        {
            LoggerHelper.Error("CreateEntity:"+entityTypeName);
            return EntityMgr.Instance.CreateClientEntity(entityTypeName, attrSet);
        }

        //创建客户端实体
        public static Entity CreateEntity(uint entityId, string entityTypeName, Action<Entity> attrSet = null)
        {
            return EntityMgr.Instance.CreateEntity(entityId, entityTypeName, attrSet);
        }

        //删除实体，主要用于删除客户端创建的实体
        public static void DestroyEntity(uint entityId)
        {
            EntityMgr.Instance.DestroyEntity(entityId);
        }

        //修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
        public static void SynEntityAttr(Entity entity, string attrName, object value)
        {
            EntityMgr.Instance.SetAttr(entity, attrName, value);
        }

        //远程调用服务器接口
        static object[] _emptyArgs = new object[0];
        public static void RpcCall(string funcname, params object[] args)
        {
            if (args == null) args = _emptyArgs;
            EntityMgr.Instance.CurrentEntity.RpcCall(funcname, args);
        }

        //注册update方法，被注册方法将每次Update被调用
        public static void RegisterUpdate<T>(string name, UpdateDelegateBase.VoidDelegate func) where T : UpdateDelegateBase
        {
            UpdateRegister.GetInstance().RegisterUpdate<T>(name, func);
        }

        //注销update方法，注销后方法将不再被每帧调用
        public static void UnregisterUpdate(string name, UpdateDelegateBase.VoidDelegate func)
        {
            UpdateRegister.GetInstance().UnregisterUpdate(name, func);
        }

        //注册update方法，被注册方法将每次LateUpdate被调用
        public static void RegisterLateUpdate(string name, LateUpdateDelegate.VoidDelegate func)
        {
            UpdateRegister.GetInstance().RegisterLateUpdate(name, func);
        }

        //注销update方法，注销后方法将不再被每帧调用
        public static void UnregisterLateUpdate(string name, LateUpdateDelegate.VoidDelegate func)
        {
            UpdateRegister.GetInstance().UnregisterLateUpdate(name, func);
        }

    }
}
