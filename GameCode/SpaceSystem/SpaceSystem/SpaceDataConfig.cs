﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
//using Utils.XML;
namespace SpaceSystem
{

    public static class SpaceDataConfig
    {
        public static int POS_SCALE = 100;

        public static int RealToScale(float real)
        {
            return (int)(real * POS_SCALE);
        }

        public static float ScaleToReal(int scale)
        {
            return ((float)scale) / POS_SCALE; 
        }
    }

    public static class SpaceDataTag
    {
        public static string TAG_BASE_SETTINGS = "base_settings";
        public static string TAG_GLOBAL_PARAMS = "global_params";
        public static string TAG_ENTITIES = "entities";
        public static string TAG_ENTITY = "entity";
        public static string Tar_GEARS = "gears";
        public static string Tar_GEAR = "gear";
    }

    public static class SpaceDataAttrTypes
    {
        public static Type INT = typeof(int);
        public static Type FLOAT = typeof(float);
        public static Type STRING = typeof(string);
        //public static Type STRING_ARRAY = typeof(string[]);
    }

    public static class SceneTimeType
    {
        public static int NONE = 0;
        public static int DAY = 1;
        public static int NIGHT = 2;
    }

}
