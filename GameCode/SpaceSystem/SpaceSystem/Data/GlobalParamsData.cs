﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    public class GlobalParamsData : ISpaceData
    {
        public string spacename;
        public string mapname;
        public string trapname;

        public int enter_pos_x_i;
        public int enter_pos_y_i;
        public int enter_pos_z_i;
        public string start_actions_l;

        //编辑器专用
        public UnityEngine.Object mapPrefab;
        public UnityEngine.GameObject enterPosObj;

        public void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public virtual void Save(StreamWriter stream)
        {
            stream.Write(SpaceDataTag.TAG_GLOBAL_PARAMS + "={");
            this.ResetPos();
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");

        }

        public void ResetPos()
        {
            if (enterPosObj != null)
            {
                var pos = enterPosObj.transform.position;
                enter_pos_x_i = SpaceDataConfig.RealToScale(pos.x);
                enter_pos_y_i = SpaceDataConfig.RealToScale(pos.y);
                enter_pos_z_i = SpaceDataConfig.RealToScale(pos.z);
            }
        }

        public Vector3 GetEnterPoint()
        {
            return new Vector3(SpaceDataConfig.ScaleToReal(enter_pos_x_i),
                               SpaceDataConfig.ScaleToReal(enter_pos_y_i),
                               SpaceDataConfig.ScaleToReal(enter_pos_z_i));
        }

    }

}
