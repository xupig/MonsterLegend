﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    [System.Serializable]
    public class EntityTeleportData : EntityData
    {
        public float target_map_id_i;
        public float dest_pos_x_f;
        public float dest_pos_y_f;
        public float dest_pos_z_f;

        public override int base_id
        {
            get
            {
                return 11000;
            }
        }

        public override void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public override void Save(StreamWriter stream)
        {
            stream.Write(this.id_i + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }
    }
   
}
