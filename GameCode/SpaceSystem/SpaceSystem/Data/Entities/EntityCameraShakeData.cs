﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Security;

namespace SpaceSystem
{
    [Serializable]
    public class EntityCameraShakeData : EntityData
    {
        public override int base_id
        {
            get
            {
                return 23000;
            }
        }
        public int camera_anim_id_i;
        public float length_f;
        public float shakeCountodds_f = 1.0f;

        public override void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public override void Save(StreamWriter stream)
        {
            stream.Write(this.id_i + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }
    }
}
