﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    public class EntitiesData : ISpaceData
    {
        public List<EntityData> entityList = new List<EntityData>();
        public Dictionary<int, EntityData> entityDic = new Dictionary<int, EntityData>();

        public void Load(Dictionary<object, object> table)
        {
            if (table == null || table.Count == 0)
            {
                return;
            }
            foreach (var item in table)
            {
                string _t = (string)((Dictionary<object, object>)(item.Value))["type"];
                _t = _t.Replace('"', ' ');
                _t = _t.Trim();
                var t = GetType().Assembly.GetType("SpaceSystem." + _t);
                var entityData = t.GetConstructor(Type.EmptyTypes).Invoke(null) as EntityData;
                entityData.Load((Dictionary<object, object>)item.Value);
                entityList.Add(entityData);
                entityDic.Add(entityData.id_i, entityData);
            }
        }

        public void Save(StreamWriter stream)
        {
            stream.Write(SpaceDataTag.TAG_ENTITIES + "={");

            foreach (var entityData in entityList)
            {
                entityData.ResetPos();
                entityData.Save(stream);
            }

            stream.Write("},");
        }

    }

}
