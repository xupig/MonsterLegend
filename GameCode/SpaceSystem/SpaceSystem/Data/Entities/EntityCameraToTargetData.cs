﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Security;

namespace SpaceSystem
{
    [Serializable]
    public class EntityCameraToTargetData:EntityData
    {
        public override int base_id
        {
            get
            {
                return 24000;
            }
        }
        public float rotationDuration_f;
        public float rotationMinSpeed_f;
        public Vector3 position = -Vector3.one;
        public float positionDuration_f;
        public bool ignoreTimeScale = true;

        public override void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public override void Save(StreamWriter stream)
        {
            stream.Write(this.id_i + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }
    }
}
