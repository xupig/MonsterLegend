﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    [System.Serializable]
    public class EntityData : ISpaceData
    {
        public int id_i;
        public string type;
        public int pos_x_i;
        public int pos_y_i;
        public int pos_z_i;
        public int action_type_i;       //0-服务器控制行为 1-客户端控制行为
        public string rotation_pos_l;
        public string boxSize;
        public int is_not_notice_all_i;     //是否同步给所有玩家  0 - 同步所有人 1- 同步给触发者

        public string success_actions_l;
        public string fail_actions_l;
        public string begin_effect_l;
        public string end_effect_l;
        public string begin_sound_l;
        public string end_sound_l;

        //编辑器专用
        public UnityEngine.GameObject editorObj;

        public virtual int base_id
        {
            get
            {
                throw new Exception("overwrite by subtype");
            }
        }

        public virtual void LoadSubElements(SecurityElement element) { }
        public virtual void SaveSubElements(StreamWriter stream) { }

        public virtual void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public virtual void Save(StreamWriter stream)
        {
            stream.Write(this.id_i + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }

        public EntityData Clone()
        {
            return this.MemberwiseClone() as EntityData;
        }

        public void ResetPos()
        {
            if (editorObj != null)
            {
                var pos = editorObj.transform.position;
                pos_x_i = SpaceDataConfig.RealToScale(pos.x);
                pos_y_i = SpaceDataConfig.RealToScale(pos.y);
                pos_z_i = SpaceDataConfig.RealToScale(pos.z);
            }
        }
    }

}
