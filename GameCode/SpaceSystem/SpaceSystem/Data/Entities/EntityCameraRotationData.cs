﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Security;

namespace SpaceSystem
{
    [System.Serializable]
    public class EntityCameraRotationData:EntityData
    {
        public override int base_id
        {
            get
            {
                return 22000;
            }
        }
        public Vector3 localEulerAngle = -Vector3.one;
        public float rotationDuration_f;
        public float rotationAccelerateRate_f;
        public float distance_f = -1;
        public float distanceDuration_f;
        public bool ignoreTimeScale = true;
        public bool keepTouchesScale = false;

        public override void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public override void Save(StreamWriter stream)
        {
            stream.Write(this.id_i + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }
    }
}
