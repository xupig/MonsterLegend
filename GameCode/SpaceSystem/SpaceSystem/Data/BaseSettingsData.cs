﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    public class BaseSettingsData : ISpaceData
    {
        public int is_fog_i;
        public string fog_color_l;
        public int fog_mode_i;
        public float fog_start_distance_f;
        public float fog_end_distance_f;
        public string ambient_light_l;
        public string camera_near_far_l;
        public string lightmaps_l;
        public string day_lightmaps_l;
        public string day_farfog_l;
        public string night_lightmaps_l;
        public string night_farfog_l;
        public string light_probes;

        public void Load(Dictionary<object, object> table)
        {
            SpaceData.SetValues(this, this.GetType(), table);
        }

        public void Save(StreamWriter stream)
        {
            stream.Write(SpaceDataTag.TAG_BASE_SETTINGS + "={");
            SpaceData.SaveDataToStream(this, this.GetType(), stream);
            stream.Write("},");
        }

        Dictionary<int, Color[]> _farFogColorSetting = null;
        public Dictionary<int, Color[]> FarFogColorSetting
        {
            get
            {
                InitFarFogSetting();
                return _farFogColorSetting;
            }
        }

        Dictionary<int, int[]> _farFogDistanceSetting = null;
        public Dictionary<int, int[]> FarFogDistanceSetting
        {
            get
            {
                InitFarFogSetting();
                return _farFogDistanceSetting;
            }
        }

        bool inited = false;
        private void InitFarFogSetting()
        {
            if (inited) return;
            inited = true;
            _farFogColorSetting = new Dictionary<int, Color[]>();
            _farFogDistanceSetting = new Dictionary<int, int[]>();
            if (!string.IsNullOrEmpty(day_farfog_l))
            {
                string[] dayFarFog = day_farfog_l.Split(',');
                _farFogColorSetting[SceneTimeType.DAY] = new Color[]{
                        new Color(int.Parse(dayFarFog[0])/255f, int.Parse(dayFarFog[1])/255f, int.Parse(dayFarFog[2])/255f),
                        new Color(int.Parse(dayFarFog[3])/255f, int.Parse(dayFarFog[4])/255f, int.Parse(dayFarFog[5])/255f)
                    };
                _farFogDistanceSetting[SceneTimeType.DAY] = new int[] { int.Parse(dayFarFog[6]), int.Parse(dayFarFog[7]) };
            }
            if (!string.IsNullOrEmpty(night_farfog_l))
            {
                string[] nightFarFog = day_farfog_l.Split(',');
                _farFogColorSetting[SceneTimeType.NIGHT] = new Color[]{
                        new Color(int.Parse(nightFarFog[0])/255f, int.Parse(nightFarFog[1])/255f, int.Parse(nightFarFog[2])/255f),
                        new Color(int.Parse(nightFarFog[3])/255f, int.Parse(nightFarFog[4])/255f, int.Parse(nightFarFog[5])/255f)
                    };
                _farFogDistanceSetting[SceneTimeType.NIGHT] = new int[] { int.Parse(nightFarFog[6]), int.Parse(nightFarFog[7]) };
            }
        }
    }

}
