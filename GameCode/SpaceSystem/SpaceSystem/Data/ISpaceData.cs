﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    public interface ISpaceData
    {
        void Load(Dictionary<object, object> table);
        void Save(StreamWriter stream); 
    }

}
