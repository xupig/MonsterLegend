﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;

using UnityEngine;
//using Utils.XML;
namespace SpaceSystem
{

    public class SpaceData : ISpaceData
    {

        public BaseSettingsData baseSettings = new BaseSettingsData();

        public GlobalParamsData globalParams = new GlobalParamsData();
        
        public EntitiesData entities = new EntitiesData();

        public void Load(Dictionary<object, object> table)
        {
            baseSettings.Load((Dictionary<object, object>)table["base_settings"]);
            globalParams.Load((Dictionary<object, object>)table["global_params"]);
            entities.Load((Dictionary<object, object>)table["entities"]);
        }

        public void Save(StreamWriter stream)
        {
            stream.Write("{");
            globalParams.Save(stream);
            baseSettings.Save(stream);
            entities.Save(stream);
            stream.Write("}");
        }
        
        private static Dictionary<string, string> s_fastDic = new Dictionary<string, string>();

        public static void SetValues(ISpaceData data, Type spaceData, Dictionary<object, object> table)
        {
            if (table == null) return;

            if (table == null || table.Count == 0)
            {
                return;
            }

            var fields = spaceData.GetFields();//获取实体属性
            foreach (var field in fields)
            {
                if (!table.ContainsKey(field.Name))
                {
                    continue;
                }
                var valueStr = table[field.Name];
                var ft = field.FieldType;
                if (ft == SpaceDataAttrTypes.INT)
                {
                    field.SetValue(data, int.Parse((string)valueStr));
                }
                else if (ft == SpaceDataAttrTypes.FLOAT)
                {
                    field.SetValue(data, float.Parse((string)valueStr));
                }
                else if (ft == SpaceDataAttrTypes.STRING)
                {
                    string s = (string)valueStr;
                    s = s.Replace('\"', ' ');
                    s = s.Trim();
                    if (s != "nil")
                    {
                        field.SetValue(data, s);
                    }
                }
                if (ft == typeof(Vector2))
                {
                    field.SetValue(data, ToVector2((Dictionary<object, object>)valueStr));
                }
                if (ft == typeof(Vector3))
                {
                    field.SetValue(data, ToVector3((Dictionary<object, object>)valueStr));
                }
                if (ft == typeof(Vector4))
                {
                    field.SetValue(data, ToVector4((Dictionary<object, object>)valueStr));
                }
                if (ft == typeof(bool))
                {
                    field.SetValue(data, ((string)valueStr) != "0");
                }
            }
        }

        private static Vector2 ToVector2(Dictionary<object, object> v)
        {
            return new Vector2(float.Parse((string)v["x"]), float.Parse((string)v["y"]));
        }

        private static Vector3 ToVector3(Dictionary<object, object> v)
        {
            return new Vector3(float.Parse((string)v["x"]), float.Parse((string)v["y"]), float.Parse((string)v["z"]));
        }

        private static Vector4 ToVector4(Dictionary<object, object> v)
        {
            return new Vector4(float.Parse((string)v["x"]), float.Parse((string)v["y"]), float.Parse((string)v["z"]), float.Parse((string)v["w"]));
        }

        private static List<string> excludeList = new List<string>() 
                                    { "editorObj", "mapPrefab", "enterPosObj" };

        public static void SaveDataToStream(ISpaceData spaceData, Type type, StreamWriter stream)
        {
            var fields = type.GetFields();//获取实体属性
            foreach (var field in fields)
            {

                if (field.FieldType == typeof(UnityEngine.Object) 
                    || field.FieldType == typeof(UnityEngine.GameObject)) 
                { continue; }
                string valueStr = "";
                object o = field.GetValue(spaceData);
                if (field.FieldType == typeof(Vector3))
                {
                    valueStr = "{x=" + ((Vector3)o).x + ",y=" + ((Vector3)o).y + ",z=" + ((Vector3)o).z + "}";
                }
                else if (field.FieldType == typeof(Vector2))
                {
                    valueStr = "{x=" + ((Vector2)o).x + ",y=" + ((Vector2)o).y + "}";
                }
                else if (field.FieldType == typeof(Vector4))
                {
                    valueStr = "{x=" + ((Vector4)o).x + ",y=" + ((Vector4)o).y + ",z=" + ((Vector4)o).z + ",w=" + ((Vector4)o).w + "}";
                }
                else if (field.FieldType == typeof(bool))
                {
                    valueStr = (bool)o ? "1" : "0";
                }
                else
                {
                    if (o == null)
                    {
                        valueStr = "nil";
                    }
                    else
                    {
                        if (o.GetType() == typeof(string))
                        {
                            valueStr = "\"" + o.ToString() + "\"";
                        }
                        else
                        {
                            valueStr = o.ToString();
                        }
                    }
                }
                string tagName = field.Name;
                stream.Write(tagName + "=" + valueStr + ",");

            }

        }
        
        public string MapPath
        {
            get
            {
                return string.Format("Scenes/{0}/{1}.prefab", globalParams.mapname, globalParams.mapname);
            }
        }

        public string TrapPath
        {
            get
            {
                return string.Format("Scenes/{0}/{1}.prefab", globalParams.mapname, globalParams.trapname);
            }
        }

        Dictionary<int, string[]> _lightmapPaths = null;
        public Dictionary<int, string[]> LightmapPaths
        {
            get
            {
                if (_lightmapPaths != null) return _lightmapPaths;
                _lightmapPaths = new Dictionary<int, string[]>();
                _lightmapPaths[SceneTimeType.NONE] = TransformPath(baseSettings.lightmaps_l);
                _lightmapPaths[SceneTimeType.DAY] = TransformPath(baseSettings.day_lightmaps_l);
                _lightmapPaths[SceneTimeType.NIGHT] = TransformPath(baseSettings.night_lightmaps_l);
                return _lightmapPaths;
            }
        }

        public string LightProbesPath
        {
            get
            {
                if (string.IsNullOrEmpty(baseSettings.light_probes)) return null;
                string path = string.Format("Scenes/{0}/{1}", globalParams.mapname, baseSettings.light_probes);
                return path;
            }
        }

        public string[] TransformPath(string source)
        {
            string[] paths = null;
            if (!string.IsNullOrEmpty(source))
            {
                paths = source.Split(',');
                for (int i = 0; i < paths.Length; i++)
                {
                    paths[i] = string.Format("Scenes/{0}/{1}", globalParams.mapname, paths[i]);
                }
            }
            return paths;
        }
    }
}
