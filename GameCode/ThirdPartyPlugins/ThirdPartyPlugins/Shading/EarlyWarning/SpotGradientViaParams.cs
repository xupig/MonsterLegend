﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SpotGradientViaParams : MonoBehaviour
{

    public float MaxEdgeWidth = 40.0f;

    public float maxAngle = 45.0f;
    public float minAngle = 0.0f;

    public bool hasAnimation = true;
    public AnimationCurve animCurv;

    public float duration = 1.0f;

    private float animT = 0.0f;

    private float angle = 0.0f;

    private float endWidth = 1.0f;

    // Use this for initialization
    void Start()
    {
        animT = 0.0f;
    }
    void Awake()
    {
        if (animCurv == null)
            animCurv = new AnimationCurve();

        if (animCurv != null && animCurv.length == 0)
        {
            animCurv.AddKey(0.0f, 0.0f);
            animCurv.AddKey(1.0f, 1.0f);
            animCurv.postWrapMode = WrapMode.PingPong;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0) return;
        angle = maxAngle;

        if (Application.isPlaying && hasAnimation && animCurv != null && animCurv.length > 0.0f && duration > 0.0f)
        {
            angle = animCurv.Evaluate(animT) * (maxAngle - minAngle) + minAngle;
            animT += Time.deltaTime / duration;
        }
        else
        {
            animT = 0.0f;
        }

        float endWidth = Mathf.Max(0.0001f, gameObject.transform.localScale.z * Mathf.Tan(Mathf.Deg2Rad * angle * 0.5f));
        float threshold = Mathf.Min(0.9999f, 1.0f - Mathf.Min(1.0f / endWidth, MaxEdgeWidth) / Mathf.Max(0.0001f, MaxEdgeWidth));
        gameObject.GetComponent<Renderer>().material.SetVector("_threshold", new Vector4(threshold, 1.0f / (1.0f - threshold), 0.0f, endWidth));
    }
}
