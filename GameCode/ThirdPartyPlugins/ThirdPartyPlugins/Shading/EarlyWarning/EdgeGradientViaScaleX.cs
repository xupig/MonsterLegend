﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EdgeGradientViaScaleX : MonoBehaviour
{

    //public float maxWidth = 30.0f;

    public float maxScaleX = 1.0f;
    public float minScaleX = 0.0f;
    public bool hasAnimation = true;
    public AnimationCurve animCurv;
    public float duration = 1.0f;

    private float width = 1.0f;
    private float animT = 0.0f;
    public float thickness = 0.02f;

    private Vector3 orginalScale = Vector3.zero;

    public Color baseColor = new Color(1, 0.1f, 0.1f, 1);
    public float internalAlpha = 0.5f;


    // Use this for initialization
    void Start()
    {
        orginalScale = Vector3.zero;
        animT = 0.0f;
        gameObject.GetComponent<Renderer>().material.SetFloat("_thickness", thickness);
    }

    void Awake()
    {
        if (animCurv == null)
            animCurv = new AnimationCurve();

        if (animCurv != null && animCurv.length == 0)
        {
            animCurv.AddKey(0.0f, 0.0f);
            animCurv.AddKey(1.0f, 1.0f);
            animCurv.postWrapMode = WrapMode.PingPong;
        }
    }
    // Update is called once per frame
    void Update()
    {
        //width = maxWidth;
        if (Time.timeScale == 0) return;
        if (orginalScale.Equals(Vector3.zero))
            orginalScale = gameObject.transform.localScale;

        if (Application.isPlaying && hasAnimation && animCurv != null && animCurv.length > 0.0f && duration > 0.0f)
        {
            float scalex = animCurv.Evaluate(animT) * (maxScaleX - minScaleX) + minScaleX;
            gameObject.transform.localScale = new Vector3(scalex, orginalScale.y, orginalScale.z);
            animT += Time.deltaTime / duration;
        }
        else
        {
            if (false == orginalScale.Equals(Vector3.zero))
            {
                gameObject.transform.localScale = orginalScale;
                orginalScale = Vector3.zero;
            }
            animT = 0.0f;
        }

        //float threshold = 1.0f - Mathf.Min(1.0f / gameObject.transform.localScale.x, maxWidth) / Mathf.Max(0.0001f, maxWidth);
        //gameObject.GetComponent<Renderer>().material.SetFloat("_threshold", threshold);
        gameObject.GetComponent<Renderer>().material.SetFloat("_InternalAlpha", internalAlpha);
        gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", baseColor);
    }
}
