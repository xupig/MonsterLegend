﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SpotGradientViaRadius : MonoBehaviour {

    public float maxAngle = 90.0f;
    public float minAngle = 0.0f;
    public float maxRadius = 1.0f;
    public float minRadius = 0.0f;

    public float internalAlpha = 0.5f;
    public float thickness = 0.008f;
    public Color baseColor = new Color(1, 0.1f, 0.1f, 1);
    private float angle = 0.0f;
    private float radius = 0.0f;

    public bool hasAnimation = true;
    public AnimationCurve animCurv;
    public float duration = 1.0f;
    private float animT = 0.0f;
    private Vector3 orginalScale = Vector3.zero;

	// Use this for initialization
	void Start () {
        animT = 0.0f;
        gameObject.GetComponent<Renderer>().material.SetFloat("_radius", 1.0f);
        gameObject.GetComponent<Renderer>().material.SetFloat("_thickness", thickness);
        gameObject.GetComponent<Renderer>().material.SetFloat("_angle", Mathf.Deg2Rad * maxAngle / 2);
	}

    void Awake()
    {
        if (animCurv == null)
            animCurv = new AnimationCurve();

        if( animCurv != null && animCurv.length == 0 )
        {
            animCurv.AddKey(0.0f, 0.0f);
            animCurv.AddKey(1.0f, 1.0f);
            animCurv.postWrapMode = WrapMode.PingPong;
        }
    }

	// Update is called once per frame
	void Update () 
    {
        if (Time.timeScale == 0) return;
        radius = maxRadius;
        angle = maxAngle;

        if (orginalScale.Equals(Vector3.zero))
            orginalScale = gameObject.transform.localScale;

        if (Application.isPlaying && hasAnimation && animCurv != null && animCurv.length > 0.0f && duration > 0.0f)
        {
            radius = animCurv.Evaluate(animT) * (maxRadius - minRadius) + minRadius;
            gameObject.transform.localScale = new Vector3(radius, 1, radius);
            animT += Time.deltaTime / duration;
        }
        else
        {
            if (!orginalScale.Equals(Vector3.zero))
            {
                gameObject.transform.localScale = orginalScale;
                orginalScale = Vector3.zero;
            }
            animT = 0.0f;
        }

        gameObject.GetComponent<Renderer>().material.SetFloat("_InternalAlpha", internalAlpha);
        gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", baseColor);
	}
}
