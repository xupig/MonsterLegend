﻿using UnityEngine;
using System.Collections;

public class TwinkleRim : MonoBehaviour
{
    public float _MinStreng = 0.5f;
    public float _MaxStreng = 2.0f;
    public float _Frequency = 3.0f;

    private float _Angle = 0;
	private float _DefaultRimStreng = 0;
	private Color _DefaultRimColor = Color.white;

	void Start()
	{
		
	}

    bool materialInited = false;
    bool CheckingMaterialInited()
    {
        if (materialInited) return materialInited;
		Renderer renderer = this.GetComponent<Renderer> ();
		if (!renderer.material.shader.name.Equals("Transparent/Diffuse"))
        {
            _DefaultRimStreng = renderer.material.GetFloat("_RimStrength");
            _DefaultRimColor = renderer.material.GetColor("_RimColor");
            materialInited = true;
            if (!renderer.material.HasProperty("_RimStrength"))
            {
                enabled = false;
            }
        }
        return materialInited;
    }


    // Update is called once per frame
    void Update()
    {
        if (!CheckingMaterialInited()) return;
        _Angle += _Frequency * Time.deltaTime;
        if (_Angle > 1000.0f * Mathf.PI)
        {
            _Angle -= 1000.0f * Mathf.PI;
        }
        float strength = 0.5f * (Mathf.Sin(_Angle) + 1) * (_MaxStreng - _MinStreng) + _MinStreng;
		Renderer renderer = this.GetComponent<Renderer> ();
        renderer.material.SetFloat("_RimStrength", strength);
    }

    void OnDestroy()
    {
		Renderer renderer = this.GetComponent<Renderer> ();
        Destroy(renderer.material);
    }

    void OnEnable()
    {
        if (!CheckingMaterialInited()) return;
    }

    public void SetRimColor(Color color)
    {
		Renderer renderer = this.GetComponent<Renderer> ();
        renderer.material.SetColor("_RimColor", color);
    }

	public void ResetRim()
	{
		Renderer renderer = this.GetComponent<Renderer> ();
		renderer.material.SetFloat("_RimStrength", _DefaultRimStreng);
		renderer.material.SetColor("_RimColor", _DefaultRimColor);
	}
}
