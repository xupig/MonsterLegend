﻿using UnityEngine;
using System.Collections;

namespace ShaderUtils
{
    public interface IShaderLoader 
    {
        Shader Find(string name);
    }

    public class DefaultShaderLoader : IShaderLoader
    {
        public virtual Shader Find(string name)
        {
            return Shader.Find(name);   
        }
    }

    public class ShaderRuntime
    {
        static public IShaderLoader loader = new DefaultShaderLoader();
        static public bool inGameRuntime = false;
    }
}
