﻿using UnityEngine;
using System.Collections;
using System;

public interface IRenderTextureLoader
{
    RenderTexture LoadRenderTexture();
}


public class RenderTextureMaterialHelper : MonoBehaviour
{
    public static IRenderTextureLoader curRTLoader;

    void OnDestroy()
    {
        curRTLoader = null;
    }

    void OnEnable()
    {
        if (curRTLoader != null)
        {
            RenderTexture rt = curRTLoader.LoadRenderTexture();
            if (rt != null)
            {
                Material mat = GetComponent<Renderer>().material;
                if (mat.HasProperty("_RenderTexture"))
                {
                    mat.SetTexture("_RenderTexture", rt);
                }
            }
            else {
                Debug.LogWarning("RenderTexture is null!");
            }
        }
        else {
            Debug.LogWarning("curRTLoader is null!");
        }
    }

}
