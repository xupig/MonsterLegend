﻿#region 模块信息
/*----------------------------------------------------------------
// Copyright (C) 2013 广州，爱游
//
// 模块名：Utils
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2013.2.1
// 模块描述：通用工具类。
//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using UnityEngine;
using System.Security.Cryptography;
using System.Net;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Mogo.Util
{
    public enum LayerMask
    {
        Default = 1,
        Character = 1 << 8,
        Monster = 1 << 11,
        Npc = 1 << 12,
        Terrain = 1 << 9,
        Trap = 1 << 17,
        Mercenary = 1 << 18
    }

    /// <summary>
    /// 通用工具类。
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// 挂载object在某父上并保持本地坐标、转向、大小不变
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public static void MountToSomeObjWithoutPosChange(Transform child, Transform parent)
        {
            Vector3 scale = child.localScale;
            Vector3 position = child.localPosition;
            Vector3 angle = child.localEulerAngles;
            child.parent = parent;
            child.localScale = scale;
            child.localEulerAngles = angle;
            child.localPosition = position;
        }

        /// <summary>
        /// 给目标对象--添加UI事件
        /// </summary>
        /// <param name="target"></param>
        /// <param name="type"></param>
        /// <param name="listener"></param>
        public static void AddUIEvent(GameObject target, EventTriggerType type, UnityAction<BaseEventData> listener)
        {
            if (target == null) return;
            EventTrigger.Entry enty = new EventTrigger.Entry();
            enty.eventID = type;
            enty.callback = new EventTrigger.TriggerEvent();
            enty.callback.AddListener(listener);
            EventTrigger trigger = target.GetComponent<EventTrigger>();
            if (trigger == null) trigger = target.AddComponent<EventTrigger>();
            if (trigger.delegates == null) trigger.delegates = new List<EventTrigger.Entry>();
            trigger.delegates.Add(enty);
        }

    }
}