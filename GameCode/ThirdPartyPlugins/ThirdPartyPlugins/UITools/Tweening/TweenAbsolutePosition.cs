﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Tween the object's position.
/// </summary>

[AddComponentMenu("UGUI/Tween/Position")]
public class TweenAbsolutePosition : UITweener
{
    public Vector3 from;
    public Vector3 to;

    Transform mTrans;

    public Transform cachedTransform { get { if (mTrans == null) mTrans = GetComponent<Transform>(); return mTrans; } }
    public Vector3 position { get { return cachedTransform.position; } set { cachedTransform.position = value; } }

    override protected void OnUpdate(float factor, bool isFinished) { cachedTransform.position = from * (1f - factor) + to * factor; }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>

    static public TweenAbsolutePosition Begin(GameObject go, float duration, Vector3 pos)
    {
        TweenAbsolutePosition comp = UITweener.Begin<TweenAbsolutePosition>(go, duration);
        comp.from = comp.position;
        comp.to = pos;
        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }

    static public TweenAbsolutePosition Begin(GameObject go, float duration, Vector3 pos, OnFinished onFinish)
    {
        TweenAbsolutePosition comp = Begin(go, duration, pos);
        comp.onFinished = onFinish;
        return comp;
    }

}