﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Tween the object's position.
/// </summary>

[AddComponentMenu("UGUI/Tween/Position")]
public class TweenPosition : UITweener
{
	public Vector2 from;
	public Vector2 to;

	RectTransform mTrans;

    public RectTransform cachedTransform { get { if (mTrans == null) mTrans = GetComponent<RectTransform>(); return mTrans; } }
    public Vector2 position { get { return cachedTransform.anchoredPosition; } set { cachedTransform.anchoredPosition = value; } }

	override protected void OnUpdate (float factor, bool isFinished) { cachedTransform.anchoredPosition = from * (1f - factor) + to * factor; }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

    static public TweenPosition Begin(GameObject go, float duration, Vector2 pos)
    {
        TweenPosition comp = UITweener.Begin<TweenPosition>(go, duration);
        comp.from = comp.position;
        comp.to = pos;
        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }

    static public TweenPosition Begin(GameObject go, float duration, Vector3 pos, Method method)
	{
		TweenPosition comp = UITweener.Begin<TweenPosition>(go, duration);
		comp.from = comp.position;
		comp.to = pos;
        comp.method = method;
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

    static public TweenPosition Begin(GameObject go, float duration, Vector3 pos, OnFinished onFinish)
    {
        TweenPosition comp = Begin(go, duration, pos);
        comp.onFinished = onFinish;
        return comp;
    }

    static public TweenPosition Begin(GameObject go, float duration, Vector3 pos, float delay, Method method)
    {
        TweenPosition comp = Begin(go, duration, pos);
        comp.delay = delay;
        comp.method = method;
        return comp;
    }

    static public TweenPosition Begin(GameObject go, float duration, Vector3 pos, float delay, Method method, OnFinished onFinish)
    {
        TweenPosition comp = Begin(go, duration, pos);
        comp.delay = delay;
        comp.method = method;
        comp.onFinished = onFinish;
        return comp;
    }

}