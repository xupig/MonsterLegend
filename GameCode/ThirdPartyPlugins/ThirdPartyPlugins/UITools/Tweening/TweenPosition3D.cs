﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Tween the object's position.
/// </summary>

[AddComponentMenu("UGUI/Tween/Position")]
public class TweenPosition3D : UITweener
{
    public Vector3 from;
    public Vector3 to;

    RectTransform mTrans;

    public RectTransform cachedTransform { get { if (mTrans == null) mTrans = GetComponent<RectTransform>(); return mTrans; } }
    public Vector3 position { get { return cachedTransform.anchoredPosition3D; } set { cachedTransform.anchoredPosition3D = value; } }

    override protected void OnUpdate(float factor, bool isFinished) { cachedTransform.anchoredPosition3D = from * (1f - factor) + to * factor; }

    /// <summary>
    /// Start the tweening operation.
    /// </summary>

    static public TweenPosition3D Begin(GameObject go, float duration, Vector3 pos)
    {
        TweenPosition3D comp = UITweener.Begin<TweenPosition3D>(go, duration);
        comp.from = comp.position;
        comp.to = pos;
        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }

    static public TweenPosition3D Begin(GameObject go, float duration, Vector3 pos, OnFinished onFinish)
    {
        TweenPosition3D comp = Begin(go, duration, pos);
        comp.onFinished = onFinish;
        return comp;
    }

}