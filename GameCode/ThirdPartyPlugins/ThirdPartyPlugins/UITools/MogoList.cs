﻿
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MogoList : MonoBehaviour
{
    public RectTransform contentRoot;
    public RectTransform moveRoot;

    public bool isHorizon = true;
    public int gap;
    public int numPerPage;
    //public string prefabName;
    public GameObject prefabGrid;
    public bool isToggleGroup = false;
    public bool isMovePage = true;
    public MogoDraggable dragHelper;

    public int TestLoadGridNum = 0;
    public bool isGrid = false;

    private Vector2 posBegin;
    private int m_currentGridNum = 0;
    private int m_currentPage = 0;
    private int m_curentPageNum = 0;
    private List<GameObject> m_goList = new List<GameObject>();
    private bool isInitGap = false;

    // Use this for initialization
    void Start()
    {
        if (moveRoot != null)
        {
            posBegin = new Vector2(moveRoot.anchoredPosition.x, moveRoot.anchoredPosition.y);
        }
        if (isMovePage)
        {
            dragHelper = gameObject.AddComponent<MogoDraggable>();
            dragHelper.dragRoot = moveRoot;
            dragHelper.isHorizon = isHorizon;
            dragHelper.OnDragDone = OnDragDone;
            dragHelper.OnDragExit = OnDragDone;
            GetComponent<ScrollRect>().vertical = false;
            GetComponent<ScrollRect>().horizontal = false;
        }

        if (TestLoadGridNum > 0)
        {
            AddList(TestLoadGridNum, null);
        }
    }

    private void OnDragDone(Vector2 d)
    {
        if (isHorizon)
            m_currentPage += (d.x < 0 ? 1 : -1);
        else
            m_currentPage += (d.y > 0 ? 1 : -1);
        if (m_currentPage < 0) m_currentPage = 0;
        if (m_currentPage >= m_curentPageNum) m_currentPage = m_curentPageNum - 1;
        MovePage(m_currentPage);
    }


    #region 列表控制
    public void MovePage(int page)
    {
        if (page < 0) page = 0;
        if (isHorizon)
        {
            dragHelper.MoveTo(new Vector2(posBegin.x - page * numPerPage * gap, posBegin.y));
        }
        else
        {
            dragHelper.MoveTo(new Vector2(posBegin.x, posBegin.y + page * numPerPage * gap));
        }
    }

    //旧版
    //public void AddList(int num, Action<GameObject, int> onLoaded)
    //{
    //    if (num == 0)
    //    {
    //        if (onLoaded != null) onLoaded(null, 0);
    //        return;
    //    }
    //    m_curentPageNum = num / numPerPage + 1;
    //    if (num % numPerPage == 0) m_curentPageNum--;
    //    foreach (GameObject go in m_goList)
    //    {
    //        GameObject.Destroy(go);
    //    }
    //    m_currentGridNum = num;
    //    for (int i = 0; i < num; i++)
    //    {
    //        //UnityEngine.Object obj = Resources.Load("UI/UIPrefab/" + prefabName);
    //        GameObject go = GameObject.Instantiate(prefabGrid) as GameObject;
    //        RectTransform rectTransform = go.transform.GetComponent<RectTransform>();
    //        go.transform.parent = listGrop.transform;

    //        if (isHorizon)
    //            rectTransform.anchoredPosition = new Vector2(i * gap, 0);
    //        else
    //            rectTransform.anchoredPosition = new Vector2(0, -i * gap);

    //        //go.transform.position = new Vector3(go.transform.position.x + i * gap, go.transform.position.y, go.transform.position.z);
    //        if (isToggleGroup)
    //        {
    //            go.GetComponent<Toggle>().group = listGrop.GetComponent<ToggleGroup>();
    //        }
    //        if (onLoaded != null)
    //            onLoaded(go, i);
    //    }
    //}

    public void AddList(int num, Action<GameObject, int> onLoaded)
    {
        if (num == 0)
        {
            if (onLoaded != null) onLoaded(null, 0);
            return;
        }
        if (isMovePage)
        {
            m_curentPageNum = num / numPerPage + 1;
            if (num % numPerPage == 0) m_curentPageNum--;
        }

        foreach (GameObject go in m_goList)
        {
            GameObject.Destroy(go);
        }
        m_goList.Clear();
        m_currentGridNum = num;
        for (int i = 0; i < num; i++)
        {
            //UnityEngine.Object obj = Resources.Load("UI/UIPrefab/" + prefabName);
            GameObject go = GameObject.Instantiate(prefabGrid) as GameObject;
            RectTransform rectTransform = go.transform.GetComponent<RectTransform>();
            go.transform.SetParent(contentRoot.transform, false);

            if (isToggleGroup)
            {
                Toggle toggle = go.GetComponent<Toggle>();
                ToggleGroup tg = contentRoot.GetComponent<ToggleGroup>();
                toggle.group = tg;
            }
            m_goList.Add(go);
            if (onLoaded != null)
                onLoaded(go, i);

            InitGap(go);
        }
    }

    private void InitGap(GameObject go)
    {
        float space = 0;
        if (isMovePage && !isInitGap)
        {
            HorizontalLayoutGroup hlg = moveRoot.GetComponent<HorizontalLayoutGroup>();
            if (isGrid)
            {
                space = moveRoot.GetComponent<VerticalLayoutGroup>().spacing;
                gap = (int)(space + go.GetComponent<LayoutElement>().preferredHeight);
            }
            else if (hlg != null)
            {
                space = hlg.spacing;
                gap = (int)(space + go.GetComponent<LayoutElement>().preferredWidth);
            }
            else if (moveRoot.GetComponent<VerticalLayoutGroup>() != null)
            {
                space = moveRoot.GetComponent<VerticalLayoutGroup>().spacing;
                gap = (int)(space + go.GetComponent<LayoutElement>().preferredHeight);
            }
            else
            {
                Debug.LogError("f!");
            }
            isInitGap = true;
        }
    }
    #endregion


}
