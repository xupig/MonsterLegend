﻿
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MogoDraggable : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    public RectTransform dragRoot;
    public bool isHorizon = true;

    public bool isNeedCorrect = false;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    public Action<Vector2> OnDragDone;
    public Action<Vector2> OnDragExit;

    TweenPosition m_tp;

    // Use this for initialization
    void Start()
    {
        if (dragRoot == null)
        {
            Debug.LogError("dragRoot == null");
            return;
        }

        m_tp = dragRoot.GetComponent<TweenPosition>();
        if (m_tp == null) m_tp = dragRoot.gameObject.AddComponent<TweenPosition>();
    }

    public void MoveTo(Vector2 v)
    {
        m_tp.from = dragRoot.anchoredPosition;
        m_tp.to = v;
        m_tp.Reset();
        m_tp.Play(true);
    }
   
    private void CorrectPosition()
    {
        m_tp.from = dragRoot.anchoredPosition;

        if (isHorizon)
        {
            if (dragRoot.anchoredPosition.x < minX)
            {
                m_tp.to = new Vector2(minX, dragRoot.anchoredPosition.y);
                m_tp.Reset();
                m_tp.Play(true);
            }
            else if (dragRoot.anchoredPosition.x > maxX)
            {
                m_tp.to = new Vector2(maxX, dragRoot.anchoredPosition.y);
                m_tp.Reset();
                m_tp.Play(true);
            }
        }
        else
        {
            if (dragRoot.anchoredPosition.y < minY)
            {
                m_tp.to = new Vector2(dragRoot.anchoredPosition.x, minY);
                m_tp.Reset();
                m_tp.Play(true);
            }
            else if (dragRoot.anchoredPosition.y > maxY)
            {
                m_tp.to = new Vector2(dragRoot.anchoredPosition.x, maxY);
                m_tp.Reset();
                m_tp.Play(true);
            }
        }
    }

    #region ui事件
    private Vector2 m_currentDragDetla = Vector2.zero;

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        PointerEventData data = eventData as PointerEventData;
        
        Vector2 delta = data.delta;
        m_currentDragDetla = delta;
        if (isHorizon)
            dragRoot.anchoredPosition = new Vector2(dragRoot.anchoredPosition.x + delta.x/UIResize.scale, dragRoot.anchoredPosition.y);
        else
            dragRoot.anchoredPosition = new Vector2(dragRoot.anchoredPosition.x, dragRoot.anchoredPosition.y + delta.y/UIResize.scale);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        PointerEventData data = eventData as PointerEventData;

        if (OnDragDone != null) OnDragDone(m_currentDragDetla);
        if (isNeedCorrect)
            CorrectPosition();
    }
    #endregion
}
