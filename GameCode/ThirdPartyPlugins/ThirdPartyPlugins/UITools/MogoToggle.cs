﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MogoToggle : MonoBehaviour
{
    public Text text;
    public Color txtColorOn;
    public Color txtColorOff;
    public int txtSizeOn;
    public int txtSizeOff;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnToggle(Toggle toggle)
    {
        text.color = toggle.isOn ? txtColorOn : txtColorOff;
        text.fontSize = toggle.isOn ? txtSizeOn : txtSizeOff;
    }
}
