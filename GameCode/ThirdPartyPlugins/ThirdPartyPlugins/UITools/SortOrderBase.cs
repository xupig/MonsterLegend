﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SortOrderBase : MonoBehaviour
{
    public int order = 0;

    private int _order = -1;
    private Canvas canvas;
    private Renderer[] renders;
    protected void Awake()
    {
        canvas = GetComponent<Canvas>();
        renders = GetComponentsInChildren<Renderer>();
    }

    private void SetOrderInEditor()
    {
        if(canvas!=null)
        {
            canvas.overrideSorting = true;
            canvas.sortingOrder = _order;
        }
        else
        {
            foreach (Renderer render in renders)
            {
                render.sortingOrder = _order;                
            }
        }
    }
    protected void LateUpdate()
    {
        if (_order != order)
        {
            _order = order;
            SetOrderInEditor();
        }
    }
}

