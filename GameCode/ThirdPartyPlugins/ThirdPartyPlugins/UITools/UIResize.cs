﻿
using UnityEngine;
using System.Collections;

/// <summary>
/// 将屏幕viewport固定宽高比为RATE
/// </summary>
public class UIResize : MonoBehaviour
{

    const float RATE = 16 / 9f;
    public static float scale = 1;
    public RectTransform uiRoot;

    //暂时写在update方便开发，正式发布改写在Start
    void Update()
    {
        if ((float)ScreenUtils.ScreenWidth / (float)ScreenUtils.ScreenHeight > RATE)
        {
            GetComponent<Camera>().orthographicSize = 360;
            scale = (float)ScreenUtils.ScreenHeight / 720f;
            uiRoot.sizeDelta = new Vector2(ScreenUtils.ScreenWidth / scale, 720);
        }
        else
        {
            //float yRate = (float)ScreenUtils.ScreenWidth / (RATE * ScreenUtils.ScreenHeight);
            //camera.rect = new Rect(0f, (1 - yRate) / 2, 1, yRate);
            GetComponent<Camera>().orthographicSize = ((float)ScreenUtils.ScreenHeight / ScreenUtils.ScreenWidth) * 640;
            scale = (float)ScreenUtils.ScreenWidth / 1280f;
            uiRoot.sizeDelta = new Vector2(1280, GetComponent<Camera>().orthographicSize * 2);
        }
    }
}
