﻿using UnityEngine;
using System;
using System.Collections;

public class AssetJsonBridge : MonoBehaviour
{
    public static Action<GameObject, string> ParseJson;

    public string content = string.Empty;

    protected void Awake()
    {
        if(ParseJson != null)
        {
            ParseJson(gameObject, content);
        }
    }
}
