﻿using UnityEngine;
using System;
using System.Collections;
using Object = UnityEngine.Object;

public class AssetMonoBridge : MonoBehaviour
{
    public static Func<string, Object> GetAssemblyObject;
}
