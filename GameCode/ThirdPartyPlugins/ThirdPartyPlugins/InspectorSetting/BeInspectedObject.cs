﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InspectorSetting
{
    //[System.Serializable]
    public class BeInspectedObject : UnityEngine.Object
    {  
        protected List<InspectingData> _inspectingDataList;
        protected List<InspectingData> GetInspectingDataList()
        {
            if (_inspectingDataList == null) {
                _inspectingDataList = new List<InspectingData>();
            }
            return _inspectingDataList;
        }

        public virtual List<InspectingData> GetInspectingData()
        { 
            var result = GetInspectingDataList();
            result.Clear();
            return result; 
        }
    }
}
