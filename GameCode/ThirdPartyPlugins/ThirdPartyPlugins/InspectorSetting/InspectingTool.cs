﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
namespace InspectorSetting
{
    public static class InspectingTool
    {
        public static void Inspect(BeInspectedObject iObject, GameObject gameObject)
        { 
            if (UnityPropUtils.Platform == RuntimePlatform.WindowsEditor 
                || UnityPropUtils.Platform == RuntimePlatform.OSXEditor)
            {
                gameObject.AddComponent<BeInspectedBehaviour>().beInspectedObject = iObject;
            }
        }
    }
}
