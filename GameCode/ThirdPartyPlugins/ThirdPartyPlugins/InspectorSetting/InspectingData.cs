﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace InspectorSetting
{
    [System.Serializable]
    public class InspectingData
    {
        public string title;
        public Type type;
        public object value;
        public Color color;
        public List<InspectingData> children;
    }
}
