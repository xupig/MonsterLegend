﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace InspectorSetting
{
    public class BeInspectedBehaviour : MonoBehaviour
    {
        [System.NonSerialized]
        public BeInspectedObject beInspectedObject;
    }
}
