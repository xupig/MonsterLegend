﻿using GameLoader.Utils;
using GameLoader.Utils.XML;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace GameLoader
{
    public class DefaultLanguage
    {
        public static Dictionary<string, string> LanguageNodeData = new Dictionary<string, string>();
        private static Action initCallback;
        public static readonly string RutimeResource = "/RuntimeResource";
        public static readonly string RutimeResourceConfig = "/Config";
        private static string dlOutputPath = string.Concat(Application.persistentDataPath, RutimeResource, RutimeResourceConfig, "/DefaultLanguage.xml");

        public static void CheckUpdate(string url, string md5, Action callback)
        {
            initCallback = callback;
            var content = LoadLocalByteData();
            var curMd5 = MD5Utils.FormatMD5(MD5Utils.CreateMD5(content));
            if (curMd5 != md5)
            {
                DriverLogger.Info("DefaultLanguage md5 not match, need update: " + curMd5 + " vs " + md5);
                HttpWrappr.instance.LoadWwwFile(url, dlOutputPath, null, LoadWwwTextCallback, LoadWwwTextErrorCallback);
            }
            else
            {
                if (initCallback != null)
                    initCallback();
            }
        }

        public static void LoadLocal()
        {
            SetLanguageData(LoadLocalData());
        }

        private static byte[] LoadLocalByteData()
        {
            bool isExists = File.Exists(dlOutputPath);
            if (!isExists)
            {
                var temp = LoadStringFromResource("DefaultLanguage");
                FileUtils.WriterFile(dlOutputPath, temp);
            }
            return FileUtils.LoadByteFile(dlOutputPath);
        }

        private static string LoadLocalData()
        {
            bool isExists = File.Exists(dlOutputPath);
            var content = "";
            if (isExists)
            {
                content = FileUtils.LoadFile(dlOutputPath);
            }
            else
            {
                content = LoadStringFromResource("DefaultLanguage");
                FileUtils.WriterFile(dlOutputPath, content);
            }
            return content;
        }

        private static void LoadWwwTextCallback(string url)
        {
            var content = FileUtils.LoadFile(dlOutputPath);
            SetLanguageData(content);
            if (initCallback != null)
                initCallback();
        }

        private static void LoadWwwTextErrorCallback(string url, string error)
        {
            if (initCallback != null)
                initCallback();
        }

        public static void SetLanguageData(string text)
        {
            LanguageNodeData = LoadDicFormXml(text);
        }

        public static string GetString(string key)
        {
            if (LanguageNodeData.ContainsKey(key))
            {
                return LanguageNodeData[key];
            }
            return "";
        }

        public static String LoadStringFromResource(String fileName)
        {
            var text = Resources.Load(fileName);
            if (text != null)
            {
                var result = text.ToString();
                Resources.UnloadAsset(text);
                return result;
            }
            else
            {
                DriverLogger.Error(fileName + " open error");
                return String.Empty;
            }
        }


        private static Dictionary<string, string> LoadDicFormXml(string text)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var children = XMLParser.LoadXML(text);
            if (children != null && children.Children != null && children.Children.Count != 0)
            {
                foreach (System.Security.SecurityElement item in children.Children)
                {
                    result[item.Tag] = item.Text;
                }
            }
            return result;
        }
    }
}