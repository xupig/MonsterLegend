﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArtTech {
    [DisallowMultipleComponent]
    public class FlowFlashEffect : MonoBehaviour {
        public bool EnableEffect {
            get { return _enableEffect; }
            set {
                _enableEffect = value;
                UpdateAllKeywords();
            }
        }
        private bool _enableEffect = true;

        public void SetMaterials(Material[] materials) {
            _materials.Clear();
            _materials.AddRange(materials);
        }
        private List<Material> _materials = new List<Material>();


        public Texture2D FlowTex { get { return _flowTex; } set { _flowTex = value; UpdateAllTextureParam("_FlowTex", _flowTex); } }
        public Color FlowColor { get { return _flowColor; } set { _flowColor = value; UpdateAllColorParam("_FlowColor", _flowColor); } }
        public Vector2 FlowSpeed { get { return _flowSpeed; } set { _flowSpeed = value; UpdateAllFloatParam("_FlowSpeedX", _flowSpeed.x); UpdateAllFloatParam("_FlowSpeedY", _flowSpeed.y); } }
        public Cubemap FlashCubeTex { get { return _flashCubeTex; } set { _flashCubeTex = value; UpdateAllTextureParam("_FlashCubeTex", _flashCubeTex); } }
        public Color FlashColor { get { return _flashColor; } set { _flashColor = value; UpdateAllColorParam("_FlashColor", _flashColor); } }
        public float FlashStrength { get { return _flashStrength; } set { _flashStrength = value; UpdateAllFloatParam("_FlashStrength", _flashStrength); } }
        public float RoundTime { get { return _roundTime; } set { _roundTime = value; } }
        public bool UseLayer2 { get { return _useLayer2; } set { _useLayer2 = value; UpdateAllKeywords(); } }
        public Color FlashColor2 { get { return _flashColor2; } set { _flashColor2 = value; UpdateAllColorParam("_FlashColor2", _flashColor2); } }
        public float FlashStrength2 { get { return _flashStrength2; } set { _flashStrength2 = value; UpdateAllFloatParam("_FlashStrength2", _flashStrength2); } }
        public float CycleTime { get { return _cycleTime; } set { _cycleTime = value; } }
        public float StartTime2 { get { return _startTime2; } set { _startTime2 = value; } }
        public float RoundTime2 { get { return _roundTime2; } set { _roundTime2 = value; } }


        [Space]
        [SerializeField]
        private Texture2D _flowTex;
        [SerializeField]
        private Color _flowColor = Color.white;
        [SerializeField]
        private Vector2 _flowSpeed = new Vector2(0.5f, 0.5f);

        [Space]
        [SerializeField]
        private Cubemap _flashCubeTex;
        [SerializeField]
        private Color _flashColor = Color.yellow;
        [SerializeField]
        private float _flashStrength = 1;
        private static Vector3 _flashRotAxis = Vector3.up;
        //[SerializeField]
        //private float _offsetAngle = 180;
        [SerializeField]
        private float _roundTime = 3;          //第一层旋转1周的时间

        [Space]
        [SerializeField]
        private bool _useLayer2 = false;
        [SerializeField]
        private Color _flashColor2 = Color.white;
        [SerializeField]
        private float _flashStrength2 = 1;

        [SerializeField]
        private float _cycleTime = 12;    //一个旋转周期的总时间
        //[SerializeField]
        //private float _offsetAngle2 = 180;
        [SerializeField]
        private float _startTime2 = 9;       //第二层的旋转偏移时间
        [SerializeField]
        private float _roundTime2 = 1.5f;         //第二层正反旋转2周的时间


        

        private void Awake() {
            if (Application.platform == RuntimePlatform.WindowsEditor) {
                if (_useLayer2) {
                    if (_cycleTime % _roundTime != 0) {
                        Debug.LogError("Cycle Time必须为Round Time的整数倍！");
                        enabled = false;
                        return;
                    }

                    if (_startTime2 + _roundTime2 > _cycleTime) {
                        Debug.LogError("Start Time 2 + Round Time 2不能大于Cycle Time！");
                        enabled = false;
                        return;
                    }
                }

                Renderer renderer = GetComponent<Renderer>();
                if (renderer != null)
                {
                    Material[] materials = renderer.materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        if (materials[i].HasProperty("_FlashCubeTex"))
                        {
                            UpdateKeywords(materials[i]);
                            UpdateParam(materials[i]);

                            _materials.Add(materials[i]);
                        }
                    }
                }
            }       
        }

        private void UpdateAllDatas() {
            for (int i = 0; i < _materials.Count; i++) {
                UpdateKeywords(_materials[i]);
                UpdateParam(_materials[i]);
            }
        }

        private void OnValidate() {
            UpdateAllDatas();
        }

        private void UpdateAllKeywords() {
            for (int i = 0; i < _materials.Count; i++) {
                UpdateKeywords(_materials[i]);
            }
        }

        private void UpdateKeywords(Material material) {
            if (_enableEffect) {
                if (_useLayer2) {
                    material.DisableKeyword("_FLOW_FLASH_ON");
                    material.EnableKeyword("_FLOW_FLASH2_ON");
                } else {
                    material.EnableKeyword("_FLOW_FLASH_ON");
                    material.DisableKeyword("_FLOW_FLASH2_ON");
                }
            } else {
                material.DisableKeyword("_FLOW_FLASH_ON");
                material.DisableKeyword("_FLOW_FLASH2_ON");
            }
        }

        private void UpdateParam(Material material) {
            if (_enableEffect) {
                material.SetTexture("_FlowTex", _flowTex);
                material.SetColor("_FlowColor", _flowColor);
                material.SetFloat("_FlowSpeedX", _flowSpeed.x);
                material.SetFloat("_FlowSpeedY", _flowSpeed.y);

                material.SetTexture("_FlashCubeTex", _flashCubeTex);
                material.SetColor("_FlashColor", _flashColor);
                material.SetFloat("_FlashStrength", _flashStrength);
                material.SetColor("_FlashColor2", _flashColor2);
                material.SetFloat("_FlashStrength2", _flashStrength2);
            }
        }

        private void UpdateAllTextureParam(string name, Texture tex) {
            for (int i = 0; i < _materials.Count; i++) {
                _materials[i].SetTexture(name, tex);
            }
        }

        private void UpdateAllColorParam(string name, Color col) {
            for (int i = 0; i < _materials.Count; i++) {
                _materials[i].SetColor(name, col);
            }
        }

        private void UpdateAllFloatParam(string name, float f) {
            for (int i = 0; i < _materials.Count; i++) {
                _materials[i].SetFloat(name, f);
            }
        }

        // Update is called once per frame
        void Update() {
            if (!_enableEffect || _materials.Count == 0)
                return;

            float angle = 0, angle2 = 0;
            float strength = 1, strength2 = 0;
            float time = Time.time;


            if (_flashStrength > 0) {
                angle = time * 360 / _roundTime;
            }

            if (_useLayer2 && _flashStrength2 > 0) {
                float timeInCycle = time % _cycleTime;
                if (timeInCycle < _startTime2) {
                    strength2 = 0;
                    angle2 = 0;
                } else {
                    float timeInRound = timeInCycle - _startTime2;
                    if (timeInRound < _roundTime2) {
                        strength2 = 1;
                        if (timeInRound < _roundTime2 / 2)
                            angle2 = 720f * (timeInRound / _roundTime2);
                        else
                            angle2 = -720f * ((timeInRound - _roundTime2 / 2) / _roundTime2);
                    } else {
                        strength2 = 0;
                        angle2 = 0;
                    }
                }
            }

            Matrix4x4 mat = Matrix4x4.identity;
            if (_flashStrength > 0) {
                Quaternion rot = Quaternion.AngleAxis(angle/* + _offsetAngle*/, _flashRotAxis);
                mat = Matrix4x4.TRS(Vector3.zero, rot, Vector3.one);
            }
            Matrix4x4 mat2 = Matrix4x4.identity;
            if (_useLayer2 && _flashStrength2 > 0) {
                float offsetAngle2 = 180;
                Camera currentCamera = Camera.current;
                if (currentCamera != null) {
                    Vector3 currentCamDir = currentCamera.transform.forward;
                    float dirDot = Vector3.Dot(currentCamDir, Vector3.forward);
                    dirDot = Mathf.Clamp(dirDot, -1.0f, 1.0f);
                    offsetAngle2 = -Mathf.Acos(dirDot) * Mathf.Rad2Deg;
                    if (currentCamDir.x < 0)
                        offsetAngle2 = -offsetAngle2;
                }

                Quaternion rot2 = Quaternion.AngleAxis(angle2 + offsetAngle2, _flashRotAxis);
                mat2 = Matrix4x4.TRS(Vector3.zero, rot2, Vector3.one);
            }
            for (int i = 0; i < _materials.Count; i++) {
                _materials[i].SetFloat("_FlashStrength", _flashStrength * strength);
                _materials[i].SetFloat("_FlashStrength2", _flashStrength2 * strength2);

                _materials[i].SetMatrix("_FlashNormalRotM", mat);
                _materials[i].SetMatrix("_FlashNormalRotM2", mat2);
            }
        }
    }
}