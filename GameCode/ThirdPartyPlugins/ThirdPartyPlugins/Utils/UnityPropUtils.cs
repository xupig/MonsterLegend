﻿using UnityEngine;

public class UnityPropUtils
{
    public static RuntimePlatform Platform;
    public static bool IsEditor;

    public static string DeviceModel;
    public static string OperatingSystem;
    public static int SystemMemorySize;
    public static string DeviceUniqueIdentifier;
    public static float deltaTime;
    public static float realtimeSinceStartup;
    public static float time;

    static UnityPropUtils()
    {
        Platform = Application.platform;
        IsEditor = Application.isEditor;
        DeviceModel = SystemInfo.deviceModel;
        OperatingSystem = SystemInfo.operatingSystem;
        SystemMemorySize = SystemInfo.systemMemorySize;
        DeviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
    }
}