﻿using System;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
#if !UNITY_WEBPLAYER
using System.IO;
#endif

namespace GameLoader.Utils
{
    /// <summary>
    /// 文件操作工具类
    /// </summary>
    public class FileUtils
    {
        /// <summary>
        /// 写入文本到文件
        /// </summary>
        /// <param name="filePath">写入文件的绝对路径</param>
        /// <param name="content">写入内容</param>
        public static void WriterFile(string filePath, string content)
        {
#if !UNITY_WEBPLAYER
            string path = filePath.Replace("\\", "/");
            string dicPath = path.Substring(0, path.LastIndexOf('/'));
            if (!Directory.Exists(dicPath))
            {
                Directory.CreateDirectory(dicPath);
            }
            using (FileStream fs = File.OpenWrite(path))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(content);
                sw.Flush();
            }
#endif
        }

        /// <summary>
        /// 写入二进制流到文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="data"></param>
        public static void WriterByteFile(string filePath, byte[] data)
        {
#if !UNITY_WEBPLAYER
            string path = filePath.Replace("\\", "/");
            string dicPath = path.Substring(0, path.LastIndexOf('/'));
            if (!Directory.Exists(dicPath))
            {
                Directory.CreateDirectory(dicPath);
            }
            using (FileStream fs = File.OpenWrite(path))
            {
                fs.Write(data, 0, data.Length);
                fs.Flush();
            }
#endif
        }

        /// <summary>
        /// 根据文件绝对路径加载文件
        /// </summary>
        /// <param name="filePath">绝对路径</param>
        /// <returns>文件内容</returns>
        public static string LoadFile(string filePath)
        {
#if !UNITY_WEBPLAYER
            return File.Exists(filePath) ? File.ReadAllText(filePath) : null;
#else
            return null;
#endif
        }

        /// <summary>
        /// 读取文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static byte[] LoadByteFile(string filePath)
        {
#if !UNITY_WEBPLAYER
            return File.Exists(filePath) ? File.ReadAllBytes(filePath) : null;
#else
            return null;
#endif
        }

        /// <summary>
        /// 保持 XML 文档。
        /// </summary>
        /// <param name="fileName">文档名称</param>
        /// <param name="xml">XML内容</param>
        public static void SaveBytes(String fileName, byte[] buffer)
        {
            if (!Directory.Exists(GetDirectoryName(fileName)))
            {
                Directory.CreateDirectory(GetDirectoryName(fileName));
            }
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (BinaryWriter sw = new BinaryWriter(fs))
                {
                    //开始写入
                    sw.Write(buffer);
                    //清空缓冲区
                    sw.Flush();
                    //关闭流
                    sw.Close();
                }
                fs.Close();
            }
        }

        /// <summary>
        /// 保存 Text 文档。
        /// </summary>
        /// <param name="fileName">文档名称</param>
        /// <param name="text">XML内容</param>
        public static void SaveText(String fileName, String text)
        {
            if (!Directory.Exists(GetDirectoryName(fileName)))
            {
                Directory.CreateDirectory(GetDirectoryName(fileName));
            }
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    //开始写入
                    sw.Write(text);
                    //清空缓冲区
                    sw.Flush();
                    //关闭流
                    sw.Close();
                }
                fs.Close();
            }
        }

        public static string GetDirectoryName(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                return fileName;
            return fileName.Substring(0, fileName.LastIndexOf('/'));

        }
    }
}
