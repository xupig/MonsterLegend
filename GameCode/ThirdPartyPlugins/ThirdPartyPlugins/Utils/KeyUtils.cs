﻿using System.IO;
using System.Security.Cryptography;

namespace GameLoader.Utils
{
    public class KeyUtils
    {
        public static byte[] downloadKey = new byte[] { 20, 127, 81, 79, 231, 20, 185, 13 };

        //PC上：key.dll,必须放在Plugins/目录下
        //Android：libkey.so,必须放在Plugins/Android/目录下
        //#if UNITY_ANDROID
        //[DllImport("key", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int GetIndexKey(int i);
        //[DllImport("key", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int GetResKey(int i);
        //#endif
        private static readonly byte[] resBytes = new byte[] { 43, 230, 23, 180, 240, 127, 80, 34 };    //{ 231, 20, 185, 13, 20, 127, 81, 79 };
        private static readonly byte[] indexBytes = new byte[] { 78, 244, 33, 209, 167, 6, 176, 87 };   //{ 123, 52, 53, 9, 12, 6, 23, 26 };   

        /// <summary>
        /// 取得pkg资源的加解密密钥
        /// </summary>
        /// <returns></returns>
        public static byte[] GetResNumber()
        {
            return resBytes;
            /*#if UNITY_EDITOR || UNITY_IPHONE
                return new byte[] { 231, 20, 185, 13, 20, 127, 81, 79 };      
            #else
                List<byte> result = new List<byte>();
                for (int i = 0; i < 8; i++)
                {
                    result.Add((byte)GetResKey(i));
                }
                return result.ToArray();
            #endif*/
        }

        /// <summary>
        /// 取得Des加解密的密钥
        /// </summary>
        /// <returns></returns>
        public static byte[] GetIndexNumber()
        {
            return indexBytes;
            /*#if UNITY_EDITOR || UNITY_IPHONE
                   return new byte[] { 123, 52, 53, 9, 12, 6, 23, 26 };         
            #else
                List<byte> result = new List<byte>();
                for (int i = 0; i < 8; i++)
                {
                    result.Add((byte)GetIndexKey(i));
                }
                return result.ToArray();
            #endif*/
        }

        public static byte[] Decrypt(byte[] ToDecrypt, byte[] Key)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider() { Key = Key, IV = Key };
            byte[] decrypted;
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(ToDecrypt, 0, ToDecrypt.Length);
                    cs.FlushFinalBlock();
                    decrypted = ms.ToArray();
                }
            }
            return decrypted;
        }
    }
}
