﻿
public class ScreenUtils
{
    private static int _screenWidth = 1280;
    private static int _screenHeight = 720;

    public static int ScreenWidth
    {
        get
        {
            return _screenWidth;
        }

        set
        {
            _screenWidth = value;
        }
    }

    public static int ScreenHeight
    {
        get
        {
            return _screenHeight;
        }

        set
        {
            _screenHeight = value;
        }
    }
}