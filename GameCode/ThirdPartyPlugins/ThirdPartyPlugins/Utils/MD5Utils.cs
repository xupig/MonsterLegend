﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GameLoader.Utils
{
    public class MD5Utils
    {
        /// <summary>
        /// 根据内容生成Md5值
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Byte[] CreateMD5(Byte[] data)
        {
            using (var md5 = MD5.Create())
            {
                return md5.ComputeHash(data);
            }
        }

        /// <summary>
        /// 根据内容生成Md5值
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CreateMD5(string str)
        {
            byte[] inbytes = Encoding.UTF8.GetBytes(str);
            return FormatMD5(inbytes);
        }

        /// <summary>
        /// 生成文件的md5值
        /// </summary>
        /// <param name="filePath">文件绝对路径</param>
        /// <returns></returns>
        public static string BuildFileMd5(string filePath)
        {
            string filemd5 = string.Empty;
            try
            {
                using (var fileStream = File.OpenRead(filePath))
                {
                    var md5 = MD5.Create();
                    var fileMD5Bytes = md5.ComputeHash(fileStream); //计算指定Stream 对象的哈希值                            
                    //fileStream.Close();                           //流数据比较大，手动卸载 
                    //fileStream.Dispose();
                    //由以连字符分隔的十六进制对构成的String，其中每一对表示value 中对应的元素；例如“F-2C-4A”               
                    filemd5 = FormatMD5(fileMD5Bytes);
                }
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("文件:{0} 生成Md5值[Fail],reason:{1}", filePath, ex.StackTrace));
            }
            return filemd5;
        }

        /// <summary>
        /// 格式化MD5值，去掉“-”
        /// </summary>
        /// <param name="data">MD5数据</param>
        /// <returns>格式化后的数据</returns>
        public static string FormatMD5(Byte[] data)
        {
            return BitConverter.ToString(data).Replace("-", "").ToLower();
        }
    }
}
