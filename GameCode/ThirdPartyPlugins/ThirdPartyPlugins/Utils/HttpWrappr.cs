﻿using System;
using UnityEngine;
using GameLoader;
using System.Collections;
using GameLoader.Utils;

namespace GameLoader.Utils
{
    /// <summary>
    /// 远程下载包装类
    /// </summary>
    public class HttpWrappr
    {
        private MonoBehaviour driver;
        public static HttpWrappr instance;

        public HttpWrappr(MonoBehaviour driver)
        {
            this.driver = driver;
        }

        /// <summary>
        /// 下载数据
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwData(string url, Action<string, byte[], byte[]> callback, Action<string, string> errcallback, bool isBytes)
        {
            driver.StartCoroutine(LoadBytes(url, null, callback, errcallback, isBytes));
        }

        public void LoadWwwFile(string url, string filePath, Action<float, long, long> progress, Action<string> callback, Action<string, string> errcallback)
        {
            driver.StartCoroutine(LoadBytes(url, (www) =>
            {
                if (www != null && progress != null)
                    driver.StartCoroutine(UpdateWwwProgress(www, progress));
            }, (cbUrl, data, assetData) =>
            {
                FileUtils.SaveBytes(filePath, data);
                if (callback != null) callback(cbUrl);
            }, errcallback, true));
        }

        private IEnumerator UpdateWwwProgress(WWW www, Action<float, long, long> progress)
        {
            while (true)
            {
                progress(www.progress, 0, 0);
                if (!www.isDone)
                    yield return null;
                else
                    yield break;
            }
        }

        /// <summary>
        /// 下载文本(自带url加随机参数)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwText(string url, Action<string, string> callback)
        {
            url = GetRandomParasUrl(url);
            driver.StartCoroutine(LoadText(url, callback));
        }

        /// <summary>
        /// 下载文本(自带url加随机参数)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwText(string url, Action<string> sucallback, Action errcallback)
        {
            url = GetRandomParasUrl(url);
            driver.StartCoroutine(LoadText(url, sucallback, errcallback));
        }

        /// <summary>
        /// 下载贴图
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwTexture2DInLocal(string url, Action<string, Texture2D> callback)
        {
            driver.StartCoroutine(LoadTexture2DInLocal(url, callback));
        }

        public static String GetRandomParasUrl(String url)
        {
            var r = CreateRandom();
            return String.Format("{0}?type={1}&ver={2}&sign={3}", url.Trim(), r.Next(100), r.Next(100), Guid.NewGuid().ToString().Substring(0, 8));
        }

        public static System.Random CreateRandom()
        {
            long tick = DateTime.Now.Ticks;
            return new System.Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
        }

        private IEnumerator LoadBytes(string url, Action<WWW> begincallback, Action<string, byte[], byte[]> callback, Action<string, string> errcallback, bool isBytes)
        {
            WWW www = new WWW(url);
            if (begincallback != null) begincallback(www);
            while (true)
            {
                yield return new WaitForSeconds(1);
                if (www.isDone)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(www.error))
                        {
                            DriverLogger.Error(string.Format("文件:{0} LoadBytes [Fail],reason:{1}", url, www.error));
                            if (errcallback != null) errcallback(url, www.error);
                            www = null;
                            yield break;
                        }
                        if (isBytes == false)
                        {
                            if (www.assetBundle)
                            {
                                var assets = www.assetBundle.LoadAllAssets();
                                if (callback != null) callback(url, (assets[0] as TextAsset).bytes, www.bytes);
                                www.assetBundle.Unload(false);
                                www = null;
                            }
                            else
                            {
                                if (callback != null) callback(url, www.bytes, www.bytes);
                                www = null;
                            }
                        }
                        else
                        {
                            if (callback != null) callback(url, www.bytes, www.bytes);
                            www = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        DriverLogger.Error(string.Format("文件:{0} LoadBytes [Fail],reason:{1}", url, ex.Message));
                        if (errcallback != null) errcallback(url, ex.Message);
                    }
                    yield break;
                }
            }
        }

        private IEnumerator LoadText(string url, Action<string, string> callback)
        {
            WWW www = new WWW(url);
            yield return www;
            try
            {
                if (www.assetBundle)
                {
                    if (callback != null) callback(url, (www.assetBundle.mainAsset as TextAsset).text);
                    www.assetBundle.Unload(false);
                    www = null;
                }
                else
                {
                    if (callback != null) callback(url, www.text);
                    www = null;
                }
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("文件:{0} LoadText [Fail],reason:{1}", url, ex.Message));
                if (callback != null) callback(url, null);
            }
        }

        private IEnumerator LoadText(string url, Action<string> sucallback, Action errcallback)
        {
            WWW www = new WWW(url);
            yield return www;
            try
            {
                if (www.assetBundle)
                {
                    if (sucallback != null) sucallback((www.assetBundle.mainAsset as TextAsset).text);
                    www.assetBundle.Unload(false);
                    www = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(www.error))
                    {
                        if (sucallback != null) sucallback(www.text);
                    }
                    else
                    {
                        if (errcallback != null) errcallback();
                    }
                    www = null;
                }
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("文件:{0} LoadText [Fail],reason:{1}", url, ex.Message));
                if (errcallback != null) errcallback();
            }
        }

        private IEnumerator LoadTexture2DInLocal(string url, Action<string, Texture2D> callback)
        {
            byte[] assetBytes = FileUtils.LoadByteFile(url);
            if (assetBytes != null)
            {
                AssetBundleCreateRequest abCreateRequest = AssetBundle.LoadFromMemoryAsync(assetBytes);
                yield return abCreateRequest;
                if (abCreateRequest.isDone == true)
                {
                    if (callback != null) callback(url, abCreateRequest.assetBundle.mainAsset as Texture2D);
                }
                else
                {
                    if (callback != null) callback(url, null);
                }
            }
            else
            {
                if (!url.Contains(Application.streamingAssetsPath) || UnityPropUtils.Platform != RuntimePlatform.Android)
                {
                    url = string.Concat(UnityPropUtils.IsEditor ? "file:///" : "file://", url);
                }
                WWW www = new WWW(url);
                yield return www;
                try
                {
                    if (www.assetBundle)
                    {
                        //Texture2D image = (Texture2D)www.assetBundle.mainAsset;
                        if (callback != null) callback(url, (Texture2D)www.assetBundle.mainAsset);
                        www.assetBundle.Unload(false);
                        www = null;
                    }
                    else
                    {
                        if (callback != null) callback(url, null);
                        www = null;
                    }
                }
                catch (Exception ex)
                {
                    DriverLogger.Error(string.Format("文件:{0} LoadTexture2D [Fail],reason:{1}", url, ex.Message));
                    if (callback != null) callback(url, null);
                }
            }
        }


        /// <summary>
        /// 下载图片
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwImage(string url, Action<string, Texture2D> callback)
        {
            driver.StartCoroutine(LoadImage(url, callback));
        }

        private IEnumerator LoadImage(string url, Action<string, Texture2D> callback)
        {
            if (!url.Contains(Application.streamingAssetsPath) || UnityPropUtils.Platform != RuntimePlatform.Android)
            {
                url = string.Concat(UnityPropUtils.IsEditor ? "file:///" : "file://", url);
            }
            WWW www = new WWW(url);
            yield return www;
            try
            {
                if (www.assetBundle)
                {
                    if (callback != null) callback(url, (www.assetBundle.mainAsset as Texture2D));
                    www.assetBundle.Unload(false);
                    www = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(www.error))
                    {
                        if (callback != null) callback(url, www.texture);
                    }
                    else
                    {
                        if (callback != null) callback(url, null);
                    }
                    www = null;
                }
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("文件:{0} LoadImage [Fail],reason:{1}", url, ex.Message));
                if (callback != null) callback(url, null);
            }
        }

        public void LoadWwwLocalText(string url, Action<string> sucallback, Action errcallback)
        {
            driver.StartCoroutine(LoadLocalText(url, sucallback, errcallback));
        }

        private IEnumerator LoadLocalText(string url, Action<string> sucallback, Action errcallback)
        {
            if (!url.Contains(Application.streamingAssetsPath) || UnityPropUtils.Platform != RuntimePlatform.Android)
            {
                url = string.Concat(UnityPropUtils.IsEditor ? "file:///" : "file://", url);
            }
            WWW www = new WWW(url);
            yield return www;
            try
            {
                if (www.assetBundle)
                {
                    if (sucallback != null) sucallback((www.assetBundle.mainAsset as TextAsset).text);
                    www.assetBundle.Unload(false);
                    www = null;
                }
                else
                {
                    if (string.IsNullOrEmpty(www.error))
                    {
                        if (sucallback != null) sucallback(www.text);
                    }
                    else
                    {
                        if (errcallback != null) errcallback();
                    }
                    www = null;
                }
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("文件:{0} LoadText [Fail],reason:{1}", url, ex.Message));
                if (errcallback != null) errcallback();
            }
        }

    }
}
