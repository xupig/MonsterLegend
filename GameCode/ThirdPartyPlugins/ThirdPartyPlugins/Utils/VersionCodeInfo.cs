﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameLoader.Defines
{
    /// <summary>
    /// 版本号。
    /// </summary>
    public class VersionCodeInfo
    {
        private List<int> m_tags = new List<int>();

        /// <summary>
        /// 构造函数。
        /// </summary>
        /// <param name="version">版本号字符串。</param>
        public VersionCodeInfo(String version)
        {
            if (string.IsNullOrEmpty(version))
                return;
            var versions = version.Split('.');
            for (int i = 0; i < versions.Length; i++)
            {
                int v;
                if (int.TryParse(versions[i], out v))
                    m_tags.Add(v);
                else
                    m_tags.Add(v);
            }
        }

        /// <summary>
        /// 获取比目前版本高一个版本的字符串。
        /// </summary>
        /// <returns></returns>
        public string GetUpperVersion()
        {
            var lastTag = m_tags[m_tags.Count - 1] + 1;
            var sb = new StringBuilder();
            for (int i = 0; i < m_tags.Count - 1; i++)
            {
                sb.AppendFormat("{0}.", m_tags[i]);
            }
            sb.Append(lastTag);
            return sb.ToString();
        }

        /// <summary>
        /// 获取比目前版本低一个版本的字符串。
        /// </summary>
        /// <returns></returns>
        public string GetLowerVersion()
        {
            var lastTag = m_tags[m_tags.Count - 1] - 1;
            var sb = new StringBuilder();
            for (int i = 0; i < m_tags.Count - 1; i++)
            {
                sb.AppendFormat("{0}.", m_tags[i]);
            }
            sb.Append(lastTag);
            return sb.ToString();
        }

        /// <summary>
        /// 返回无小数点版本号。
        /// </summary>
        /// <returns></returns>
        public string ToShortString()
        {
            var sb = new StringBuilder();
            foreach (var item in m_tags)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }

        public int ToInt()
        {
            int result = 0;
            for (int i = 0; i < m_tags.Count; i++)
            {
                result += m_tags[i] * (int)Math.Pow(10, (3 - i) * 2);
            }
            return result;
        }

        /// <summary>
        /// 比较版本号，自己比参数大，返回1，比参数小，返回-1，相等返回0。
        /// </summary>
        /// <param name="info">比较版本号。</param>
        /// <returns>自己比参数大，返回1，比参数小，返回-1，相等返回0。</returns>
        public int Compare(VersionCodeInfo info)
        {
            var count = this.m_tags.Count < info.m_tags.Count ? this.m_tags.Count : info.m_tags.Count;
            for (int i = 0; i < count; i++)
            {
                if (this.m_tags[i] == info.m_tags[i])
                    continue;
                else
                    return this.m_tags[i] > info.m_tags[i] ? 1 : -1;
            }
            return 0;
        }

        /// <summary>
        /// 获取版本号字符串。
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var item in m_tags)
            {
                sb.AppendFormat("{0}.", item);
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }
    }

}
