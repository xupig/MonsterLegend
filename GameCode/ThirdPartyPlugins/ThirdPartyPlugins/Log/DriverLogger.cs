﻿#region 模块信息
/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：LoggerHelper
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2013.1.17
// 模块描述：日志输出类。
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace GameLoader
{
    /// <summary>
    /// 日志等级声明。
    /// </summary>
    [Flags]
    enum LoggerLevel
    {
        /// <summary>
        /// 缺省
        /// </summary>
        NONE = 0,
        /// <summary>
        /// 调试
        /// </summary>
        DEBUG = 1,
        /// <summary>
        /// 信息
        /// </summary>
        INFO = 2,
        /// <summary>
        /// 警告
        /// </summary>
        WARNING = 4,
        /// <summary>
        /// 错误
        /// </summary>
        ERROR = 8,
        /// <summary>
        /// 异常
        /// </summary>
        EXCEPT = 16,
        /// <summary>
        /// 关键错误
        /// </summary>
        CRITICAL = 32,
    }

    /// <summary>
    /// 日志控制类。
    /// </summary>
    /// 
    public class DriverLogger
    {
        /// <summary>
        /// 当前日志记录等级。
        /// </summary>
        static LoggerLevel CurrentLoggerLevels = LoggerLevel.DEBUG | LoggerLevel.INFO | LoggerLevel.WARNING | LoggerLevel.ERROR | LoggerLevel.CRITICAL | LoggerLevel.EXCEPT;

        /// <summary>
        /// 记录是否对日志进行处理，主要系考虑日志对效率造成的影响，打印日志都需要对这个先进行判断。
        /// </summary>
        public static bool LISTEN_LOG = true;
        private const Boolean SHOW_STACK = true;
        private static LogWriter m_logWriter;
        public static string DebugFilterStr = string.Empty;
        public static List<string> lastLogs = new List<string>();

        public static void SetDebugFilterStr(string value)
        {
            DebugFilterStr = value;
        }

        public static LogWriter LogWriter
        {
            get { return m_logWriter; }
        }

        static DriverLogger()
        {
            m_logWriter = new LogWriter();
            //Application.RegisterLogCallbackThreaded(new Application.LogCallback(ProcessExceptionReport));
            //Application.logMessageReceivedThreaded += ProcessExceptionReport;
        }

        public static void Release()
        {
            m_logWriter.Release();
        }

        public static void UploadLogFile()
        {
            m_logWriter.UploadTodayLog();
        }

        static ulong index = 0;


        /// <summary>
        /// 调试日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        /// <param name="isShowStack">是否显示调用栈信息</param>
        /*
        public static void Debug(object message,  string user )
        {
            if (DebugFilterStr != "") return;

            if (LoggerLevel.DEBUG == (CurrentLoggerLevels & LoggerLevel.DEBUG))
                Log(string.Concat(user + " [DEBUG]: ", SHOW_STACK ? GetStackInfo() : "", message, " Index = ", index++), LoggerLevel.DEBUG);
            
        }*/

        /// <summary>
        /// 调试日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        /// <param name="isShowStack">是否显示调用栈信息</param>
        public static void Debug(object message, Boolean isShowStack = SHOW_STACK, int user = 0)
        {
            if (DebugFilterStr != "") return;

            if (LoggerLevel.DEBUG == (CurrentLoggerLevels & LoggerLevel.DEBUG))
                Log(string.Concat(" [DEBUG]: ", isShowStack ? GetStackInfo() : "", message, " Index = ", index++), LoggerLevel.DEBUG);
        }

        /// <summary>
        /// 扩展debug
        /// </summary>
        /// <param name="message"></param>
        /// <param name="filter">只输出与在DebugMsg->filter里面设置的filter一样的debug</param>
        public static void Debug(string filter, object message, Boolean isShowStack = SHOW_STACK)
        {
            if (DebugFilterStr != "" && DebugFilterStr != filter) return;
            if (LoggerLevel.DEBUG == (CurrentLoggerLevels & LoggerLevel.DEBUG))
            {
                Log(string.Concat(" [DEBUG]: ", isShowStack ? GetStackInfo() : "", message), LoggerLevel.DEBUG);
            }

        }

        /// <summary>
        /// 信息日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Info(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LoggerLevel.INFO == (CurrentLoggerLevels & LoggerLevel.INFO))
                Log(string.Concat(" [INFO]: ", isShowStack ? GetStackInfo() : "", message), LoggerLevel.INFO);
        }

        /// <summary>
        /// 警告日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Warning(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LoggerLevel.WARNING == (CurrentLoggerLevels & LoggerLevel.WARNING))
                Log(string.Concat(" [WARNING]: ", isShowStack ? GetStackInfo() : "", message), LoggerLevel.WARNING);
        }

        /// <summary>
        /// 异常日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Error(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LoggerLevel.ERROR == (CurrentLoggerLevels & LoggerLevel.ERROR))
                Log(string.Concat(" [ERROR]: ", message, '\n', isShowStack ? GetStacksInfo() : ""), LoggerLevel.ERROR);
        }

        /// <summary>
        /// 关键日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Critical(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LoggerLevel.CRITICAL == (CurrentLoggerLevels & LoggerLevel.CRITICAL))
                Log(string.Concat(" [CRITICAL]: ", message, '\n', isShowStack ? GetStacksInfo() : ""), LoggerLevel.CRITICAL);
        }

        /// <summary>
        /// 异常日志。
        /// </summary>
        /// <param name="ex">异常实例。</param>
        public static void Except(Exception ex, object message = null)
        {
            if (LoggerLevel.EXCEPT == (CurrentLoggerLevels & LoggerLevel.EXCEPT))
            {
                Exception innerException = ex;
                while (innerException.InnerException != null)
                {
                    innerException = innerException.InnerException;
                }
                Log(string.Concat(" [EXCEPT]: ", message == null ? "" : message + "\n", ex.Message, innerException.StackTrace), LoggerLevel.CRITICAL);
            }
        }

        /// <summary>
        /// 获取堆栈信息。
        /// </summary>
        /// <returns></returns>
        private static String GetStacksInfo()
        {
            StringBuilder sb = new StringBuilder();
            StackTrace st = new StackTrace();
            var sf = st.GetFrames();
            for (int i = 2; i < sf.Length; i++)
            {
                sb.AppendLine(sf[i].ToString());
            }

            return sb.ToString();
        }

        private static String MsgRegEx = "";
        public static void SetRegEx(string newMsgRegEx)
        {
            MsgRegEx = newMsgRegEx;
        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        private static void Log(string message, LoggerLevel level, bool writeEditorLog = true)
        {
            if (MsgRegEx != null && MsgRegEx != "" && level <= LoggerLevel.WARNING)
            {
                //m_logWriter.WriteLog("t" + Regex.IsMatch(message, MsgRegEx), LoggerLevel.INFO, writeEditorLog);
                if (Regex.IsMatch(message, MsgRegEx) == false)
                {
                    //m_logWriter.WriteLog("f" + Regex.IsMatch(message, MsgRegEx), LoggerLevel.CRITICAL, writeEditorLog);
                    return;
                }
            }
#if UNITY_WEBPLAYER
            lastLogs.Add(message);
            if (lastLogs.Count > 10) lastLogs.RemoveAt(0);
#endif
            var msg = string.Concat(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff"), message);
            m_logWriter.WriteLog(msg, level, writeEditorLog);
            //Debugger.Log(0, "TestRPC", message);
        }

        /// <summary>
        /// 获取调用栈信息。
        /// </summary>
        /// <returns>调用栈信息</returns>
        private static String GetStackInfo()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(2);//[0]为本身的方法 [1]为调用方法
            if (sf == null) return string.Empty; //针对IOS进行优化
            var method = sf.GetMethod();
            return String.Format("{0}.{1}(): ", method.ReflectedType.Name, method.Name);
        }

        private static void ProcessExceptionReport(string message, string stackTrace, LogType type)
        {
            var logLevel = LoggerLevel.DEBUG;
            switch (type)
            {
                case LogType.Assert:
                    logLevel = LoggerLevel.DEBUG;
                    break;
                case LogType.Error:
                    logLevel = LoggerLevel.ERROR;
                    break;
                case LogType.Exception:
                    logLevel = LoggerLevel.EXCEPT;
                    break;
                case LogType.Log:
                    logLevel = LoggerLevel.DEBUG;
                    break;
                case LogType.Warning:
                    logLevel = LoggerLevel.WARNING;
                    break;
                default:
                    break;
            }

            if (logLevel == (CurrentLoggerLevels & logLevel))
                Log(string.Concat(" [SYS_", logLevel, "]: ", message, '\n', stackTrace), logLevel, false);
        }
    }

    /// <summary>
    /// 日志写入文件管理类。
    /// </summary>
    public class LogWriter
    {
        private string m_logPath;
        private string m_logFileName = "driver_{0}.txt";
        private string m_logFilePath;
        private FileStream m_fs;
        private StreamWriter m_sw;
        private Action<String, LoggerLevel, bool> m_logWriter;
        private readonly static object m_locker = new object();

        public string LogPath
        {
            get { return m_logPath; }
        }

        /// <summary>
        /// 默认构造函数。
        /// </summary>
        public LogWriter()
        {
            m_logPath = string.Concat(UnityEngine.Application.persistentDataPath, "/RuntimeResource/log");
            if (!Directory.Exists(m_logPath))
                Directory.CreateDirectory(m_logPath);
            m_logFilePath = String.Concat(m_logPath, "/", String.Format(m_logFileName, DateTime.Today.ToString("yyyyMMdd")));
            try
            {
                m_logWriter = Write;
                m_fs = new FileStream(m_logFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                m_sw = new StreamWriter(m_fs);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
            }
        }

        /// <summary>
        /// 释放资源。
        /// </summary>
        public void Release()
        {
            lock (m_locker)
            {
                if (m_sw != null)
                {
                    m_sw.Close();
                    m_sw.Dispose();
                }
                if (m_fs != null)
                {
                    m_fs.Close();
                    m_fs.Dispose();
                }
            }
        }

        public void UploadTodayLog()
        {
            //lock (m_locker)
            //{
            //    using (var fs = new FileStream(m_logFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            //    {
            //        using (StreamReader sr = new StreamReader(fs))
            //        {
            //            var content = sr.ReadToEnd();
            //            var fn = Utils.GetFileName(m_logFilePath);//.Replace('/', '\\')
            //            if (MogoWorld.theAccount != null)
            //            {
            //                fn = string.Concat(MogoWorld.theAccount.name, "_", fn);
            //            }
            //            DownloadMgr.Instance.UploadLogFile(fn, content);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="msg">日志内容</param>
        internal void WriteLog(string msg, LoggerLevel level, bool writeEditorLog)
        {
            if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
                m_logWriter(msg, level, writeEditorLog);
            else
                m_logWriter.BeginInvoke(msg, level, writeEditorLog, null, null);
            UploadBug(msg, level);
        }

        private void Write(string msg, LoggerLevel level, bool writeEditorLog)
        {
            lock (m_locker)
                try
                {
                    if (writeEditorLog)
                    {
                        switch (level)
                        {
                            case LoggerLevel.DEBUG:
                            case LoggerLevel.INFO:
                                UnityEngine.Debug.Log(msg);
                                break;
                            case LoggerLevel.WARNING:
                                UnityEngine.Debug.LogWarning(msg);
                                break;
                            case LoggerLevel.ERROR:
                            case LoggerLevel.EXCEPT:
                            case LoggerLevel.CRITICAL:
                                UnityEngine.Debug.LogError(msg);
                                break;
                            default:
                                break;
                        }
                    }
                    if (m_sw != null)
                    {
                        m_sw.WriteLine(msg);
                        m_sw.Flush();
                    }
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.LogError(ex.Message);
                }
        }

        private void UploadBug(string message, LoggerLevel level)
        {
            if (level == LoggerLevel.ERROR || level == LoggerLevel.EXCEPT || level == LoggerLevel.CRITICAL)
            {
                string bugMsg = GetBugMsg(message);
                LogUploader.GetInstance().UploadBug(bugMsg);
            }
        }

        public static string GetBugMsg(string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("------------------------------------------------------\n");
            sb.AppendFormat("logtime：[{0}]\n", getCurrentTime());
            sb.AppendFormat("platform：{0} ", UnityPropUtils.Platform);
            sb.AppendFormat("deviceinfo：device={0}", UnityPropUtils.DeviceModel);
            sb.Append(message).Append("\n");
            return sb.ToString();
        }

        private static string getCurrentTime()
        {
            System.DateTime now = System.DateTime.Now;
            return now.ToString("yyyy-MM-dd HH:mm:ss fff", DateTimeFormatInfo.InvariantInfo);
        }
    }
}
