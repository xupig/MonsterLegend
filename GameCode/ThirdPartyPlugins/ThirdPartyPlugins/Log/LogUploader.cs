﻿using GameLoader.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameLoader
{
    /// <summary>
    /// 日志上传类。
    /// </summary>
    /// 
    public class LogUploader
    {
        /// <summary>
        /// 数组分隔符： ','
        /// </summary>
        private const Char LIST_SPRITER = ',';

        public string UploadLogUrl = null;
        public bool NeedUploadLog = true;
        public List<String> WhiteList = new List<string>();
        public int ErrorCounter = 0;

        public MonoBehaviour DriverInstance;
        public string BundleName = Application.bundleIdentifier;
        public string ServerId = "0";
        public string Account = "";
        public string AvatarName = "";
        public string SceneId = "";
        public string Paras = "";

        private static LogUploader s_instance = null;
        public static LogUploader GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new LogUploader();
            }
            return s_instance;
        }

        private LogUploader()
        {
            UploadLogUrl = @"http://67.216.197.109/common.php";
        }

        public void UploadBug(string message)
        {
            if (!NeedUploadLog) return;
            if (!string.IsNullOrEmpty(UploadLogUrl))
            {

                if (DriverInstance != null)
                {
                    for (int i = 0; i < WhiteList.Count; i++)
                    {
                        if (message.Contains(WhiteList[i]))
                            return;
                    }
                    DriverInstance.StartCoroutine(DoUploadBug(message, true));
                }
            }
        }

        /// <summary>
        /// 参数：bug,force,time,serverId
        /// 密钥：md5(serverId + bug + force + time +密钥KEY);
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        private IEnumerator DoUploadBug(string bug, bool isMustUpload)
        {
            yield return new WaitForEndOfFrame();
            WWWForm form = new WWWForm();
            string force = isMustUpload ? "1" : "0";
            string time = GetTimestamp().ToString();
            string key = PackArray(GetUploadNumber());
            string md5 = MD5Utils.CreateMD5(StringConcat(BundleName, force, time, key));
            form.AddField("BundleName", BundleName);
            form.AddField("PlayerInfo", String.Format("ServerId: {0}, Account: {1}, AvatarName: {2}, SceneId: {3}, Paras: {4}", ServerId, Account, AvatarName, SceneId, Paras));
            form.AddField("bug", bug);
            form.AddField("force", force);
            form.AddField("time", time);
            form.AddField("verify", md5);
            WWW w = new WWW(UploadLogUrl, form);
            yield return w;
            if (w.error != null)
            {
                UnityEngine.Debug.LogError("upload log error:" + w.error);
                ErrorCounter++;
                if(ErrorCounter >= 5)//出错次数过多，停止上传
                {
                    NeedUploadLog = false;
                }
            }
            else
            {
                UnityEngine.Debug.Log("upload log:" + w.text);
            }
        }

        public byte[] GetUploadNumber()
        {
#if UNITY_EDITOR
            return new byte[] { 231, 20, 185, 13, 20, 127, 81, 79 };
#elif UNITY_IPHONE
            return new byte[] { 20, 127, 81, 79, 231, 20, 185, 13 };
#else
            return KeyUtils.downloadKey;
#endif
        }

        public static ulong GetTimestamp()
        {
            System.DateTime now = System.DateTime.Now;
            System.DateTime start = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (ulong)(now - start).TotalSeconds;
        }

        public static String PackArray(byte[] array, Char listSpriter = LIST_SPRITER)
        {
            var list = new List<byte>();
            list.AddRange(array);
            if (list.Count == 0)
                return "";
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.Count; i++)
                {
                    sb.AppendFormat("{0}{1}", list[i], listSpriter);
                }
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
        }

        public static string StringConcat(params object[] paras)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < paras.Length; i++)
            {
                sb.Append(paras[i]);
            }
            return sb.ToString();
        }
    }
}
