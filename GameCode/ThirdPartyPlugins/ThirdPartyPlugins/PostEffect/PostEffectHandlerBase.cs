﻿using System;
using UnityEngine;



public class DofParameter {
    [Range(0.0f, 1.0f)]
    public float FocalDistance = 0.1f;
    [Range(0.0f, 1.0f)]
    public float OffsetDistance = 0.1f;
    [Range(0, 10)]
    public int DepthLevel = 5;
}

public class GodRayParameter {
    public Color SunColor = Color.white;
    [Range(0.0f, 1.0f)]
    public float Density = 0.167f;
    [Range(0.0f, 1.0f)]
    public float Decay = 0.982f;
    [Range(0.0f, 10.0f)]
    public float Exposure = 1.01f;
    [Range(0.0f, 1.0f)]
    public float Alpha = 0.5f;
}

public class HdrBloomParameter {
    [Range(0.0f, 1.0f)]
    public float Drak = 0.2f;
    [Range(0.0f, 1.0f)]
    public float Alpha = 0.5f;
    [Range(0.0f, 1.0f)]
    public float Brightness = 0.5f;
    [Range(0.0f, 20.0f)]
    public float Gloss = 10.0f;
    public Color BlurColor = new Color(1, 1, 1, 1);
}

public class RadiaBlurParameter {
    [Range(0.0f, 0.50f)]
    public float SamplerDist = 0.17f;
    [Range(1.0f, 5.0f)]
    public float SamplerStrength = 2.09f;
}

public class ScreenWhiteParameter {
    public Color Color = Color.white;
    [Range(0.0f, 1.0f)]
    public float Alpha = 0;
}

public class ScreenFogParameters {
    public Color FogColor = Color.grey;
    public float FogStart = 1;
    public float FogEnd = 20;
}


public abstract class PostEffectHandlerBase {
    public static PostEffectHandlerBase Instance { get; set; }

    public abstract void InitAll(Camera camera);

    public abstract bool GetPostEffectEnabled(Camera camera);
    public abstract void SetPostEffectEnabled(Camera camera, bool enabled);

    public abstract void SetBloomEnabled(Camera camera, bool enabled);
    public abstract void SetBloomThreshold(Camera camera, float threshold);
    public abstract void SetBloomMaxBrightness(Camera camera, float maxBrightness);
    public abstract void SetBloomSoftKnee(Camera camera, float softKnee);
    public abstract void SetBloomRadius(Camera camera, float radius);
    public abstract void SetBloomIntensity(Camera camera, float intensity);

    public abstract void SetScreenDarkLayers(Camera camera, int layers);

    public abstract void SetScreenWhiteEnabled(Camera camera, bool enabled);
    public abstract void SetScreenWhiteColor(Camera camera, Color color);
    public abstract void SetScreenWhiteAlpha(Camera camera, float alpha);

    public abstract void SetRadialBlurEnabled(Camera camera, bool enabled);
    public abstract bool GetRadialBlurEnabled(Camera camera);
    public abstract void SetRadialBlurDarkness(Camera camera, float darkness);
    public abstract void SetRadialBlurStrength(Camera camera, float strength);
    public abstract void SetRadialBlurSharpness(Camera camera, float sharpness);
    public abstract void SetRadialBlurVignetteSize(Camera camera, float vignetteSize);


    public abstract void PlayDOF(Camera camera, DofParameter StartParameter, DofParameter endParameter, float duration);
    public abstract void PlayGodRay(Camera camera, GodRayParameter startParameter, GodRayParameter endParameter, float duration);
    public abstract void PlayHdrBloom(Camera camera, HdrBloomParameter startParameter, HdrBloomParameter endParameter, float duration);
    public abstract void PlayRadialBlur(Camera camera, RadiaBlurParameter startParameter, RadiaBlurParameter endParameter, float duration);
    public abstract void PlayScreenWhite(Camera camera, ScreenWhiteParameter startParameter, ScreenWhiteParameter endParameter, float duration, Action finish = null, bool disableOnPlayFinish = false);

    public abstract void PlayRadialBlur(Camera camera, float startDarkness, float startStrength, float startSharpness, float startVignetteSize,
                   float endDarkness, float endStrength, float endSharpness, float endVignetteSize,
                   float duration, Action finish, bool disableOnPlayFinish);
    public abstract void PlayScreenDark(Camera camera, float start, float end, float duration, Action finish, bool disableOnPlayFinish);
    public abstract void PlayScreenWhite(Camera camera, Color startColor, float startAlpha, Color endColor, float endAlpha, float duration, Action finish, bool disableOnPlayFinish);
    public abstract void PlayScreenFog(Camera camera, Color fogColorStart, float fogStartStart, float fogEndStart,
                Color fogColorEnd, float fogStartEnd, float fogEndEnd,
                float duration, Action finish, bool disableOnPlayFinish);
    public abstract void PlayScreenWhite(Camera camera, float start, float end, float duration, Action finish, bool disableOnPlayFinish);
}

