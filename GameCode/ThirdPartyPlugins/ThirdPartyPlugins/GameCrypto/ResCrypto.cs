using System;

namespace GameCrypto.Utils
{
    /// <summary>
    /// 资源加|解密工具类
    /// </summary>
    public static class ResCrypto
    {
        /// <summary>
        /// 加密长度
        /// </summary>
        public static int cryptoLength = 32;
        /// <summary>
        /// 加密密码
        /// </summary>
        public static string cryptoPassword = "12345678";

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="src">明文</param>
        public static void Encrypt(byte[] src)
        {
            byteXor(src, cryptoLength, cryptoPassword, true);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="src">密文</param>
        public static void Decrypt(byte[] src)
        {
            byteXor(src, cryptoLength, cryptoPassword, false);
        }

        /// <summary>
        /// 加、解密接口
        /// </summary>
        /// <param name="src">源数据</param>
        /// <param name="encryptLen">src要加密的数据长度</param>
        /// <param name="password">加、解密密码</param>
        /// <returns></returns>
        private static void byteXor(byte[] src, int encryptLen, string password, bool isEncrypt)
        {
            //如果加密长度超过数据长度，则设置为数据长度
            if (src.Length < encryptLen) encryptLen = src.Length;

            byte[] srcData = new byte[encryptLen];
            Array.Copy(src, 0, srcData, 0, encryptLen);
            Console.WriteLine(string.Format("    {0}:{1}", isEncrypt ? "明文" : "密文", ResCrypto.HexString(srcData)));
            
            int strCount = 0;
            for (int i = 0; i < encryptLen; i++)
            {
                if (strCount >= password.Length) strCount = 0;
                src[i] ^= (byte)password[strCount++];
                srcData[i] = src[i];
            }
            Console.WriteLine(string.Format("    {0}:{1}", isEncrypt ? "密文" : "明文", ResCrypto.HexString(srcData)));
        }

        public static string HexString(byte[] data)
        {
            return BitConverter.ToString(data).Replace("-", "");
        }
    }
}