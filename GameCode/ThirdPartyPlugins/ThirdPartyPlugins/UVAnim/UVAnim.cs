﻿using UnityEngine;
using System.Collections;

public class UVAnim : MonoBehaviour {

    Renderer rd;
    Vector2 dir;
    public Vector2 Direction;
    public float speed;

    void Awake()
    {
		Renderer renderer = this.GetComponent<Renderer> ();
        rd = renderer;
    }
    void Update()
    {
        rd.material.SetTextureOffset("_Layer1Tex", dir += new Vector2(UnityPropUtils.deltaTime * speed * Direction.x, UnityPropUtils.deltaTime * speed * Direction.y));

        if (dir.x > 64)
        {
            dir = Vector2.zero;
        }
    }
}
