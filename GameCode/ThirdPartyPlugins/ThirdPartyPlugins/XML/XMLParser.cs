﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：XMLParser
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.2.1
// 模块描述：XML解析器。
//==========================================*/
#endregion

using System;
using System.IO;
using System.Security;
using System.Collections.Generic;

//using GameLoader.IO;

namespace GameLoader.Utils.XML
{
    /// <summary>
    /// XML解析器。
    /// </summary>
    public class XMLParser
    {
        /// <summary>
        /// 从指定的 XML 文档加载 map 数据。
        /// </summary>
        /// <param name="xml">XML 文档</param>
        /// <returns>map 数据</returns>
        public static Dictionary<Int32, Dictionary<String, String>> LoadIntMap(SecurityElement xml, string source)
        {
            var result = new Dictionary<Int32, Dictionary<String, String>>();

            var index = 0;
            if (xml.Children == null)
                return result;
            foreach (SecurityElement subMap in xml.Children)
            {
                index++;
                if (subMap.Children == null || subMap.Children.Count == 0)
                {
                    DriverLogger.Warning("empty row in row NO." + index + " of " + source);
                    continue;
                }
                var se = subMap.Children[0] as SecurityElement;
                if (!se.Tag.StartsWith("id"))
                {
                    DriverLogger.Error(String.Format("First element '{0}' not name as 'id_i', in {1}.", se.Tag, source));
                }
                Int32 key = Int32.Parse(se.Text);
                if (result.ContainsKey(key))
                {
                    DriverLogger.Warning(String.Format("Key {0} already exist, in {1}.", key, source));
                    continue;
                }

                var children = new Dictionary<String, String>();
                result.Add(key, children);
                for (int i = 1; i < subMap.Children.Count; i++)
                {
                    var node = subMap.Children[i] as SecurityElement;
                    //对属性名称部分后缀进行裁剪
                    string tag;
                    if (node.Tag.Length < 3)
                    {
                        tag = node.Tag;
                    }
                    else
                    {
                        var tagTail = node.Tag.Substring(node.Tag.Length - 2, 2);
                        if (tagTail == "_i" || tagTail == "_s" || tagTail == "_f" || tagTail == "_l" || tagTail == "_k" || tagTail == "_m")
                            tag = node.Tag.Substring(0, node.Tag.Length - 2);
                        else
                            tag = node.Tag;
                    }

                    if (node != null && !children.ContainsKey(tag))
                    {
                        if (String.IsNullOrEmpty(node.Text))
                            children.Add(tag, "");
                        else
                            children.Add(tag, node.Text.Trim());
                    }
                    else
                        DriverLogger.Warning(String.Format("Key {0} already exist, index {1} of {2}.", node.Tag, i, node.ToString()));
                }
            }
            return result;
        }

        /// <summary>
        /// 从指定的字符串加载 XML 文档。
        /// </summary>
        /// <param name="xml">包含要加载的 XML 文档的字符串。</param>
        /// <returns>编码安全对象的 XML 对象模型。</returns>
        public static SecurityElement LoadXML(String xml)
        {
            try
            {
                SecurityParser securityParser = new SecurityParser();
                securityParser.LoadXml(xml);
                return securityParser.ToXml();
            }
            catch (Exception ex)
            {
                DriverLogger.Except(ex);
                return null;
            }
        }
    }
}