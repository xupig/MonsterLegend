--DramaUtil.lua
DramaUtil = {}

local DramaConfig = GameConfig.DramaConfig
local Vector3 = Vector3

function DramaUtil:StringToActions(actionStr)
    local actions = {}
    local splits = string.split(actionStr,";")
    if string.IsNullOrEmpty(splits[1]) then
        return actions
    end
    local str = nil    
    for i = 1, #splits do
        str = splits[i]
        if str ~= "null" then
            table.insert(actions,self:StringToAction(str))
            if str == "End" then
                break
            end
        end
    end
    return actions
end

function DramaUtil:StringToAction(actionStr)
    local action = {}
    local splits = string.split(actionStr,":")
    action[DramaConfig.TAG_ACTION] = splits[1]
    if #splits > 1 then
        local args = string.split(splits[2],"|")
        for i = 1,#args do
            action[DramaConfig.TAG_ARG + i] = args[i]
        end
    end
    return action
end

function DramaUtil:StringToVector3(str)
    local splits = string.split(str,",")
    return Vector3.New(splits[1],splits[2],splits[3])
end

function DramaUtil:GetCreatureByEntityDramaID(id)
    return GameManager.GameSceneManager:GetDramaEntityByID(id)
end

function DramaUtil:GetDefaultCameraTarget()
    return GameWorld.Player():GetBoneByName("slot_camera")
end

function DramaUtil:IsDramaEntityByEID(eid)
    return GameManager.GameSceneManager:IsDramaEntityByEID(eid)
end

return DramaUtil