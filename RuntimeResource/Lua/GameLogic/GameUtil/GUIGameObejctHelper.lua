
GUIGameObejctHelper = {}

local RectTransform = UnityEngine.RectTransform
function GUIGameObejctHelper.AddBottomLeftRectTransform(go, position, size)
    local transf = go:AddComponent(typeof(RectTransform))
    
    transf.anchorMin = Vector2(0, 0)
    transf.anchorMax = Vector2(0, 0)
    transf.pivot = Vector2(0.0, 0.0)
    transf.sizeDelta = size;
    transf.anchoredPosition = position;
    return transf
end

return GUIGameObejctHelper