BaseUtil = {}

local math = math
local math_random = math.random
local string = string
local StringStyleUtil = GameUtil.StringStyleUtil
local RoleDataHelper = GameDataHelper.RoleDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ColorConfig = GameConfig.ColorConfig
local public_config = GameWorld.public_config
local ItemConfig = GameConfig.ItemConfig

function BaseUtil.KeepTwoDecimalPlaces(decimal)
    decimal = math.floor((decimal * 100) + 0.5) * 0.01       
    return  decimal 
end

function BaseUtil.GetColorString(content, color)
    if content ~= nil then
        return string.format("<color=%s>%s</color>", color or "#ffffffff", content)
    end
    return ""
end

-- isLeave:true表示显示剩余类型即ownCount==0才显示红色
function BaseUtil.GetColorStringByCount(ownCount, needCount, isLeave)
    if isLeave then
        if ownCount == 0 then
            return string.format("<color=%s>%s/%s</color>", ColorConfig.H, ownCount, needCount)
        else
            return string.format("<color=%s>%s/%s</color>", ColorConfig.J, ownCount, needCount)
        end
    else
        if ownCount < needCount then
            ownCount = StringStyleUtil.GetLongNumberString(ownCount)
            return string.format("<color=%s>%s/%s</color>", ColorConfig.H, ownCount, needCount)
        else
            ownCount = StringStyleUtil.GetLongNumberString(ownCount)
            return string.format("<color=%s>%s/%s</color>", ColorConfig.J, ownCount, needCount)
        end
    end
end

function BaseUtil.GetColorStringByVocation(content, vocation)
    if content ~= nil then
        local colorId = RoleDataHelper.GetColorIdByVocation(vocation)
        local text = StringStyleUtil.GetColorStringWithColorId(content,colorId)
        return text
    end
    return ""
end

--从格式"道具id1:权重;道具id2:权重2;..."中随机出一个道具id
function BaseUtil.ChooseRandomItem2(drop)
    if drop then
        local ram = math.random()
        local prop = 0
        local total = 0
        for k, v in pairs(drop) do
            total = total + v
        end
        for k, v in pairs(drop) do
            local p = v/total
            prop = prop + p
            if ram < prop then
                return k
            end
        end
    end
end

--属性折算战斗力
--t所需计算属性列表{属性key=属性value} {{attri=1,value=1}}
--voc职业
function BaseUtil.CalculateFightPower(t,voc)
    local value_sum = 0     --按基础值计算的属性总和
    local per_sum = 0       --按百分比计算的属性总和

	for i=1,#t do
    	local attri_id = t[i].attri
    	local attri_value = t[i].value
        local tmp =  AttriDataHelper.GetAttri(attri_id)
        if tmp then
            local param_type = tmp.spirit_sea_param_type
            local param = tmp.spirit_sea_param
            local param_co
            if param then
                param_co = param[0]
                --职业特有
                if param[voc] then
                    param_co = param[voc]
                end
            end
            if param_type and param_co then
                if param_type == 1 then
                    value_sum = value_sum + attri_value*param_co

            	elseif param_type == 2 then
                    per_sum = per_sum + attri_value*param_co
                end
            end
        end
    end

    local combat_value = math.ceil(value_sum * (1 + per_sum/10000))
    --LoggerHelper.Error("CalculateFightPower"..combat_value)
    return combat_value
end

--计算属性很多用到，抽出来
function BaseUtil.AddAttri(t, k, v2)
    local v1 = t[k]
    if v1 then
        t[k] = v1 + v2
    else
        t[k] = v2
    end
end

function BaseUtil.UnionTable(t1, t2)
    if t1 and t2 then
        for k,v in pairs(t2) do
            BaseUtil.AddAttri(t1, k, v)
        end
    end
end

--深度拷贝
--@depth 深度
--@src 源
--@dest 目标
--@return 同dest
function BaseUtil.DeepCopy(depth, src, dest)
    if type(src) ~= "table" then return src end
    if depth < 1 then return end
    if not dest then dest = {} end
    for k,v in pairs(src) do
        if type(v) == "table" then
            if depth <= 1 then
                dest[k] = v
            else
                dest[k] = BaseUtil.DeepCopy(depth-1, v)
            end
        else
            dest[k] = v
        end
    end
    return dest
end

--随机生成len位整数(首位可为0)
function BaseUtil.RandomNumber(len)
    local result = ""
    for i=1,len do
        result = result..math.random(0,9)
    end
    return result
end

--判定货币是否满足所需数量
function BaseUtil.IsEnoughMoney(needCount, moneyType)
    moneyType = moneyType or public_config.MONEY_TYPE_COUPONS
    needCount = needCount or 0

    return GameWorld.Player()[ItemConfig.MoneyTypeMap[moneyType]] >= needCount
end

function BaseUtil.GetDBIdFromCrossUUId(cross_uuid)
    local tmp = string.split(cross_uuid, '_')
    if tmp[2] then
        return tonumber(tmp[2])
    end
end

local zero = Vector2.zero
local one = Vector2.one
--将gameobject用scale设定是否隐藏
--scale,显示时的scale
function BaseUtil.SetObjectActive(object,isActive,scale)
    BaseUtil.SetTransformActive(object.transform,isActive,scale)
end

--将gameobject.transform用scale设定是否隐藏
--scale,显示时的scale
function BaseUtil.SetTransformActive(transform,isActive,scale)
    if isActive then
        local vecScale = one
        if scale then
            vecScale = Vector2.New(scale,scale)
        end
        transform.localScale = vecScale
    else
        transform.localScale = zero
    end
end

function BaseUtil.DistanceByXZ(v1,v2)
    return math.sqrt((v1.x - v2.x)*(v1.x-v2.x) + (v1.z - v2.z)*(v1.z-v2.z))
end

return BaseUtil