
local StringStyleUtil = {}
local FontStyleHelper = GameDataHelper.FontStyleHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

StringStyleUtil.white = "#ffffff"
StringStyleUtil.red = "#ff0000"
StringStyleUtil.green = "#00ff00"
StringStyleUtil.blue = "#0000ff"
StringStyleUtil.orange = "#aa3c00"
StringStyleUtil.yellow1 = "#ffcc33"

StringStyleUtil.cnNumber = {902,903,904,905,906,907,927,928,929,930}--931,932,933,934,935,936

function StringStyleUtil.GetColorString(content, color)
	return string.format("<color=%s>%s</color>", color or StringStyleUtil.white, content)
end

function StringStyleUtil.GetFontSizeString(content, size)
	return string.format("<size=%s>%s</size>", size or 0, content)
end

function StringStyleUtil.GetColorStringWithTextMesh(content, color)
	return string.format("<%s>%s</color>", color or StringStyleUtil.white, content)
end

function StringStyleUtil.GetColorStringWithColorId(content, colorId)
	local color = FontStyleHelper.GetFontStyleData(colorId).color
	return string.format("<%s>%s</color>", color or StringStyleUtil.white, content)
end

--长数字处理，12000 = 1万 15000 = 2万
function StringStyleUtil.GetLongNumberStringEx(number)
	local str
	if number >= 1000000000000 then
		str = tostring(math.floor(number/1000000000000))..LanguageDataHelper.GetContent(143)..LanguageDataHelper.GetContent(515)
	elseif number >= 100000000 then
		str = tostring(math.floor(number/100000000))..LanguageDataHelper.GetContent(515)
	elseif number >= 10000 then
		str = tostring(math.floor(number/10000))..LanguageDataHelper.GetContent(143)
	else
		str = tostring(number)
	end
	return str
end

--长数字处理，12000 = 1.2万
function StringStyleUtil.GetLongNumberString(number)
	local str
	if number >= 1000000000000 then
		str = tostring(math.floor(number/10000000000)/100)..LanguageDataHelper.GetContent(143)..LanguageDataHelper.GetContent(515)
	elseif number >= 100000000 then
		str = tostring(math.floor(number/1000000)/100)..LanguageDataHelper.GetContent(515)
	elseif number >= 10000 then
		str = tostring(math.floor(number/100)/100)..LanguageDataHelper.GetContent(143)
	else
		str = tostring(number)
	end
	return str
end

--获取100以下中文
function StringStyleUtil.GetChineseNumber(number)
	local str
	if number <= 10 then
		str = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[number])
	else
		local a = math.floor(number/10)
		local b = number%10
		local str1
		local str2 = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[10])
		local str3
		if a == 1 then
			str1 = ""
		else
			str1 = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[a])
		end

		if b == 0 then
			str3 = ""
		else
			str3 = LanguageDataHelper.CreateContent(StringStyleUtil.cnNumber[b])
		end

		str = str1..str2..str3
	end
	return str
end

--获取等级显示中文
--level
--level2 有值的时候如：50-60，380-400,返回Lv.50-60，巅峰10-30
--notShowLv 是否显示Lv.字样
function StringStyleUtil.GetLevelString(level,level2,notShowLv)
	local limit = GlobalParamsHelper.GetParamValue(659)
	if level2 then
		local t = string.split(level2,"-")
		if t[1] > limit then
			return LanguageDataHelper.CreateContent(969)..t[1].."-"..t[2]
		else
			if notShowLv then
				return t[1].."-"..t[2]
			else
				return "Lv."..t[1].."-"..t[2]
			end
		end
	end
	
	if level > limit then
		return LanguageDataHelper.CreateContent(969)..(level - limit)
	else
		if notShowLv then
			return tostring(level)
		else
			return "Lv."..level
		end
		return 
	end
end

return StringStyleUtil