local StringUtil = {}
local StringCache = {} -- 缓存
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

--返回当前字符实际占用的字符数
local function SubStringGetByteCount(str, index)
    local curByte = string.byte(str, index)
    local byteCount = 1;
    if curByte == nil then
        byteCount = 0
    elseif curByte > 0 and curByte <= 127 then
        byteCount = 1
    elseif curByte>=192 and curByte<=223 then
        byteCount = 2
    elseif curByte>=224 and curByte<=239 then
        byteCount = 3
    elseif curByte>=240 and curByte<=247 then
        byteCount = 4
    end
    return byteCount;
end


--获取中英混合UTF8字符串的真实字符数量
local function SubStringGetTotalIndex(str)
    local curIndex = 0;
    local i = 1;
    local lastCount = 1;
    repeat 
        lastCount = SubStringGetByteCount(str, i)
        i = i + lastCount;
        curIndex = curIndex + 1;
    until(lastCount == 0);
    return curIndex - 1;
end

local function SubStringGetTrueIndex(str, index)
    local curIndex = 0;
    local i = 1;
    local lastCount = 1;
    repeat 
        lastCount = SubStringGetByteCount(str, i)
        i = i + lastCount;
        curIndex = curIndex + 1;
    until(curIndex >= index);
    return i - lastCount;
end



--截取中英混合的UTF8字符串，endIndex可缺省
function StringUtil.SubStringUTF8(str, startIndex, endIndex)
    local key = str .. startIndex
    if endIndex then
        key = key .. "," .. endIndex
    end
    if StringCache[key] then
        return StringCache[key]
    end

    if startIndex < 0 then
        startIndex = SubStringGetTotalIndex(str) + startIndex + 1;
    end

    if endIndex ~= nil and endIndex < 0 then
        endIndex = SubStringGetTotalIndex(str) + endIndex + 1;
    end

    local ret = ""
    if endIndex == nil then 
        ret = string.sub(str, SubStringGetTrueIndex(str, startIndex));
    else
        ret = string.sub(str, SubStringGetTrueIndex(str, startIndex), SubStringGetTrueIndex(str, endIndex + 1) - 1);
    end
    StringCache[key] = ret 
    return ret
end

return StringUtil