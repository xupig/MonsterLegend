TypeUtil = {}
function TypeUtil.ToVector3(v)
    -- body
    local bs = string.split(v, ",")
    if bs == nil then return nil end
    local x = tonumber(bs[1])
    local y = tonumber(bs[2])
    local z = tonumber(bs[3])
    return Vector3(x, y, z)
end

function TypeUtil.ToVector2(v)
    -- body
    local bs = string.split(v, ",")
    if bs == nil then return nil end
    local x = tonumber(bs[1])
    local y = tonumber(bs[2])
    return Vector2(x, y)
end

function TypeUtil.ToVector4(v)
    local bs = string.split(v, ",")
    if bs == nil then return nil end
    local x = tonumber(bs[1])
    local y = tonumber(bs[2])
    local z = tonumber(bs[3])
    local w = tonumber(bs[4])
    return Vector4(x, y, z, w)
end

function TypeUtil.ToNumList(v)
    local bs = string.split(v, ",")
    if bs == nil then return nil end
    local nums = {}
    for k, v in ipairs(bs) do
        table.insert(nums, tonumber(v))
    end
    return nums
end

function TypeUtil.GetTable(content)
    -- body
    local content = "local rst=" .. content .. " return rst"
    local f = loadstring(content)
    if f == nil then
        LoggerHelper.Error("TypeUtil.GetTable empty f: " .. content)
        return nil
    else
        local r = f()
        return r
    end
end

return TypeUtil
