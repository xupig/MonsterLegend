-- GameUtil.lua
-- 这里提供统一接口，为逻辑层提供大部分manager的获取
GameUtil = {}
GameUtil._utils = {}

local mt={		   
			  __index = function(t, k)
                    local util = t._utils[k]
                    if (util == nil) then
                        util = require("GameUtil."..k)
                        t._utils[k] = util
                    end 
					return util
			   end
			  }
setmetatable(GameUtil, mt)

