local ReloadUtil = {}

local io = io
local string = string
local table = table
local GUIManager = GameManager.GUIManager
local modulePath = table.concat({"..", "RuntimeResource", "Lua", "GameLogic", "Modules"}, "\\")

function ReloadUtil.GetRuntimePath()
	local obj = io.popen("cd")
    local path = obj:read("*all"):sub(1,-2)
    obj:close()
    return path
end


function ReloadUtil.GetModulePath(moduleName)
    return table.concat({modulePath, "Module" .. moduleName, ""}, "\\")
end


function ReloadUtil.GetFiles(modulePath, seperator)
    local files = {}
    local cmd = "cd " .. modulePath .. " & dir /w /s"
    local getinfo = io.popen(cmd)
    local all = getinfo:read('*all')
    -- LoggerHelper.Log(#all .. all)
    io.close(getinfo)

    local dirStrList = string.split(all, seperator) or {}
    table.remove(dirStrList, 1)
    local subPath
    local fullPath
    for i, dirStr in ipairs(dirStrList) do
        -- if not string.find(dirStr, ".svn") then
        subPath = string.match(dirStr, "(.-) ")
        fullPath = seperator .. subPath
        for filename in string.gmatch(dirStr, "[%s]([%a][%a%d]-).lua") do
            table.insert(files, {
                name = filename, 
                path = fullPath .. "\\" .. filename .. ".lua", 
                classPath = ""
            })
        end
    end

    return files
end


function ReloadUtil.GetClassPaths(fileList)
    local pattern = ".classPath[%s]*=[%s]*\"(.-)\""
    for i, v in ipairs(fileList) do
        local file, err = io.open(v.path, "rb")
        if file then
            local content = file:read("*a")
            local classPath = string.match(content, pattern)
            if classPath then
                -- LoggerHelper.Log(classPath)
                fileList[i].classPath = classPath
            end
            file:close()
        end
    end
    return fileList
end



function ReloadUtil.ReloadPanel(panelName)
    local setting = GUIManager._panelSettings[panelName]
    if setting and setting.panel ~= nil then
        local panel = setting.panel
        panel.gameObject.Destroy(panel.gameObject)
        GUIManager._panelSettings[panelName].panel = nil
        ClassTypes[panelName .. "Panel"] = nil
        package.loaded[setting.panelClassName] = nil
        -- LoggerHelper.Log("== Reload Panel : " .. setting.panelClassName)
    end
end

function ReloadUtil.ReloadComponent(classPath, moduleName)
    if package.loaded[classPath] then
        package.loaded[classPath] = nil
        if moduleName and ClassTypes[moduleName] then
            ClassTypes[moduleName] = nil
        end
        -- LoggerHelper.Log("=== Reloaded Module = " .. classPath)
    end
    return require (classPath)
end

----- 重新加载Modules目录下的模块
function ReloadUtil.ReloadModule(moduleName)
    local runtimePath = ReloadUtil.GetRuntimePath()
    local modulePath =  ReloadUtil.GetModulePath(moduleName)
    -- todo 判断是否存在该模块
    -- LoggerHelper.Log("runtimePath : " .. runtimePath)
    LoggerHelper.Log("=== Reload Module : " .. modulePath)

    local fileList = ReloadUtil.GetFiles(modulePath, string.sub(runtimePath, 0, string.LastIndexOf(runtimePath, "\\")))
    local fileList = ReloadUtil.GetClassPaths(fileList)
    -- LoggerHelper.Log(fileList)

    for i, v in ipairs(fileList) do
        if v.classPath ~= "" then
            ReloadUtil.ReloadComponent(v.classPath, v.name)
        else
            local s, e = string.find(v.name, "Panel")
            if s then
                ReloadUtil.ReloadPanel(string.sub(v.name, 0, s-1))
            end
        end
    end
end




return ReloadUtil