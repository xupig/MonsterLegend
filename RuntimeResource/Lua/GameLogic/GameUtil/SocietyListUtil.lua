--SocietyListUtil.lua 好友系统里列表排序Util
local SocietyListUtil = {}
local table = table

local maxTime = 999999999999

function SocietyListUtil:FrinedListSort(frinedList)
    if frinedList == nil or table.isEmpty(frinedList) then
        return
    end
    table.sort(frinedList, self.CompareFrined)

    -- GameWorld.LoggerHelper.Log("好友列表排序[耗性能]")
end

function SocietyListUtil:RecentListSort(recentList)
    if recentList == nil or table.isEmpty(recentList)  then
        return
    end
    table.sort(recentList, self.CompareRecent)

    -- GameWorld.LoggerHelper.Log("最近列表排序[耗性能]")
end

--好友列表排序:是否有未读消息 -> 是否在线 -> 在线玩家第3顺序：上一次互动距今时间  / 离线玩家第3顺序：离线时间长短
function SocietyListUtil.CompareFrined(a,b)
    local atime = a:GetOfflineTime()
    local btime = b:GetOfflineTime()

    if atime == 1 then
        atime = maxTime
    end

    if btime == 1 then
        btime = maxTime
    end

    if a:GetNewChatCnt() == b:GetNewChatCnt() then

        if atime == btime then
            --做互动时间排序[存在隐患,因为有的好友互动时间是一样的]
            return a:GetContactTime()>b:GetContactTime()

        else
            return atime > btime
        end

    else
        return a:GetNewChatCnt() > b:GetNewChatCnt()
    end

end

--最近联系人列表排序:最近一次聊天顺序
function SocietyListUtil.CompareRecent(a,b)
    if a:GetContactTime() == b:GetContactTime() then
        return false
    else
        return a:GetContactTime()>b:GetContactTime()
    end
end

return SocietyListUtil