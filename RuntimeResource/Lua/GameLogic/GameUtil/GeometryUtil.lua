GeometryUtil = {}

local string = string
local Mathf = Mathf

function GeometryUtil.CheckPositionIsInScreen(position, startPointX, startPointY, screenWidth, screenHeight)
    return position.x >= startPointX and position.x < startPointX + screenWidth and position.y >= startPointY and position.y < startPointY + screenHeight
end

function GeometryUtil.Cross(a, b, c)
    return (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y);
end

function GeometryUtil.IsIntersect(a, b, c, d)
    return Mathf.Max(a.x, b.x) >= Mathf.Min(c.x, d.x)
        and Mathf.Max(a.y, b.y) >= Mathf.Min(c.y, d.y)
        and Mathf.Max(c.x, d.x) >= Mathf.Min(a.x, b.x)
        and Mathf.Max(c.y, d.y) >= Mathf.Min(a.y, b.y)
        and GeometryUtil.Cross(c, b, a) * GeometryUtil.Cross(b, d, a) >= 0
        and GeometryUtil.Cross(a, d, c) * GeometryUtil.Cross(d, b, c) >= 0
end

function GeometryUtil.TryGetIntersection(p1, p2, p3, p4)
    if GeometryUtil.IsIntersect(p1, p2, p3, p4) == false then
        return nil
    end
    local area1 = GeometryUtil.Det(p1, p2, p1, p3)
    local area2 = GeometryUtil.Det(p1, p2, p1, p4)
    return Vector2((area2 * p3.x - area1 * p4.x) / (area2 - area1), (area2 * p3.y - area1 * p4.y) / (area2 - area1))
end

function GeometryUtil.Det(a, b, c, d)
    return (b.x - a.x) * (d.y - c.y) - (d.x - c.x) * (b.y - a.y)
end

return GeometryUtil
