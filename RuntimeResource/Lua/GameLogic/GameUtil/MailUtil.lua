-- 第一顺序：未读、已读
-- 未读邮件第二顺序：发送时间从新到旧
-- 已读邮件第二顺序：有附件、没附件或附件已领3 2 4
-- 已读邮件第三顺序：发送时间从新到旧
local  MailUtil = {1}
local public_config = GameWorld.public_config
local sortHelp = {[2]=2, [3]=3,[4]=1}

function MailUtil:SortMail(mailData)
    table.sort(mailData, self.MailComps)
end

function MailUtil.MailComps(a, b)
    if a:GetInfoState() <= 1 and b:GetInfoState() <=1 then
        if a:GetInfoTime() > b:GetInfoTime() then
            return true
        elseif a:GetInfoTime() < b:GetInfoTime() then
            return false
        else
            return false
        end 
    elseif a:GetInfoState() <=1 and b:GetInfoState() >=2 then
        return true
    elseif a:GetInfoState() >=2 and b:GetInfoState() <=1 then
        return false
    elseif a:GetInfoState()==2 and b:GetInfoState()==4 then
        return false
    elseif a:GetInfoState()==4 and b:GetInfoState()==2 then
        return true
    elseif a:GetInfoState() == 2 or a:GetInfoState() == 3  and b:GetInfoState() == 2 or b:GetInfoState() == 3 then
        if sortHelp[a:GetInfoState()] > sortHelp[b:GetInfoState()] then
            return true
        elseif sortHelp[a:GetInfoState()] < sortHelp[b:GetInfoState()] then
            return false
        else
            if a:GetInfoTime() > b:GetInfoTime() then
                return true
            elseif a:GetInfoTime() < b:GetInfoTime() then
                return false
            else
                return false
            end
        end
    elseif a:GetInfoState() <= 3 and b:GetInfoState()==4 or a:GetInfoState()==4 and b:GetInfoState()<=3 then
        if sortHelp[a:GetInfoState()] > sortHelp[b:GetInfoState()] then
            return true
        elseif sortHelp[a:GetInfoState()] < sortHelp[b:GetInfoState()] then
            return false
        else
            if a:GetInfoTime() > b:GetInfoTime() then
                return true
            elseif a:GetInfoTime() < b:GetInfoTime() then
                return false
            else
                return false
            end
        end
    elseif a:GetInfoState() ==4 and b:GetInfoState() ==4 then
        if a:GetInfoTime() > b:GetInfoTime() then
            return true
        elseif a:GetInfoTime() < b:GetInfoTime() then
            return false
        else
            return false
        end 
    else
        return false
    end
end

return MailUtil