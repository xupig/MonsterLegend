local UIComponentUtil = {}

-- 搜索指定子路径下对应的GameObject,简称GO。
-- hostGO：需要被搜索的GameObject类型对象
-- childPath: 对应的子路径字符串
-- 返回类型：GameObject
function UIComponentUtil.FindChild(hostGO, childPath)
	local childTransform = hostGO.transform:Find(childPath)
	if childTransform == nil then
		LoggerHelper.Error("can not find child " .. childPath)
		return nil
	end
	return childTransform.gameObject
end

-- 搜索指定子路径下对应的子结点GameObject，并获取上面指定名称的Component实例
-- hostGO：需要被搜索的GameObject类型对象
-- childPath: 对应的子路径字符串
-- compName: 对应Component类型名称
-- 返回类型：Component的子类实例
function UIComponentUtil.GetChildComponent(hostGO, childPath, compName)
	local child = UIComponentUtil.FindChild(hostGO, childPath)
	if child == nil then
		return nil
	end
	local component = child.transform:GetComponent(compName);
	if component == nil then
		LoggerHelper.Error("can not find component " .. compName)
		return nil
	end
	return component
end

-- 搜索指定子路径下对应的子结点GameObject，并在其上面增加指定类型的Component实例
-- hostGO：需要被搜索的GameObject类型对象
-- childPath: 对应的子路径字符串
-- class: 对应Component类型
-- 返回类型：Component的子类实例
function UIComponentUtil.AddChildComponent(hostGO, childPath, class)
	local child = UIComponentUtil.FindChild(hostGO, childPath)
	if child == nil then
		return nil
	end
	local component = child:AddComponent(typeof(class))
	if component == nil then
		LoggerHelper.Error("can not Add component in " .. childPath)
		return nil
	end
	return component
end

-- 在指定的GameObject上面增加指定类型的Component实例
-- hostGO：在指定的GameObject
-- class: 对应Component类型
-- 返回类型：Component的子类实例
function UIComponentUtil.AddLuaUIComponent(hostGO, class) 
	local component = GameWorld.AddLuaUIComponent(hostGO, class.interface, class.classPath)
	if component == nil then
		LoggerHelper.Error("can not Add luacomponent " .. class.classPath)
		return nil
	end
	return component
end


-- 搜索指定子路径下对应的子结点GameObject，并在其上面增加指定类型的LuaUIComponent实例或其子类实例
-- hostGO：需要被搜索的GameObject类型对象
-- childPath: 对应的子路径字符串
-- class: 对应LuaUIComponent类型
-- 返回类型：LuaUIComponent的子类实例
function UIComponentUtil.AddChildLuaUIComponent(hostGO, childPath, class) 
	local child = UIComponentUtil.FindChild(hostGO, childPath)
	if child == nil then
		return nil
	end
	return UIComponentUtil.AddLuaUIComponent(child, class) 
end

-- 在指定的GameObject上面找某GameObject并将其复制到某路径下
-- hostGO：在指定的GameObject
-- srcPath: 被复制到GameObject所在路径
-- parentPath: 复制目标父路径
-- 返回类型：复制后的GameObject
function UIComponentUtil.CopyUIGameObject(hostGo, srcPath, parentPath) 
	local go = GameWorld.CopyUIGameObject(hostGo, srcPath, parentPath)
	if go == nil then
		LoggerHelper.Error("can not Copy UI GameObject, srcPath: " .. srcPath .." parentPath: " .. parentPath)
		return nil
	end
	return go
end

-- 搜索指定子路径下对应的GameObject,简称GO 是否存在。
-- hostGO：需要被搜索的GameObject类型对象
-- childPath: 对应的子路径字符串
-- 返回类型：bool
function UIComponentUtil.ExistChild(hostGO, childPath)
	local childTransform = hostGO.transform:Find(childPath)
	if childTransform == nil then
		return false
	end
	return true
end

return UIComponentUtil