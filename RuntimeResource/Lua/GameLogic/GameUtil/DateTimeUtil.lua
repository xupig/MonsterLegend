local _osTime = os.time
local _osDate = os.date
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

DateTimeUtil = {}

DateTimeUtil.openServerTimeStamp = 0
DateTimeUtil.serverTimeStamp = 0

DateTimeUtil.OneWeekTime = 604800
DateTimeUtil.OneDayTime = 86400
DateTimeUtil.OneHourTime = 3600
DateTimeUtil.OneMinTime = 60
DateTimeUtil.OneSecTime = 1

DateTimeUtil.Not_Open = 1 --未开启
DateTimeUtil.Openning = 2 --开启中
DateTimeUtil.Over = 3 --结束

local Transform_times = {
    day = 60 * 60 * 24,
    hour = 60 * 60,
    min = 60,
    sec = 1
}

--初始化时间工具
function DateTimeUtil.Init()
    DateTimeUtil.timeCallbackWaitList = {}
    if DateTimeUtil.allTimerId then
        for i, v in ipairs(DateTimeUtil.allTimerId) do
            TimerHeap:DelTimer(v)
        end
    end
    DateTimeUtil.allTimerId = {}
    DateTimeUtil.DayString = LanguageDataHelper.CreateContent(147)
    DateTimeUtil.HourString = LanguageDataHelper.CreateContent(148)
    DateTimeUtil.MinString = LanguageDataHelper.CreateContent(149)
    DateTimeUtil.SecString = LanguageDataHelper.CreateContent(150)
end

function DateTimeUtil.Clear()
    DateTimeUtil.timeCallbackWaitList = nil
    for i, v in ipairs(DateTimeUtil.allTimerId) do
        TimerHeap:DelTimer(v)
    end
    DateTimeUtil.allTimerId = {}
end

function DateTimeUtil.SetTimeCallbackWaitList()
    local list = DateTimeUtil.timeCallbackWaitList
    for i, v in ipairs(list) do
        DateTimeUtil.AddTimeCallback(v.hour, v.min, v.sec, v.isloopNextDay, v.func)
    end
end

function DateTimeUtil.AddTimeCallback(hour, min, sec, isloopNextDay, func)
    if DateTimeUtil.hasSetTime then
        local nowDate = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
        local nowTime = nowDate.hour * DateTimeUtil.OneHourTime + nowDate.min * DateTimeUtil.OneMinTime + nowDate.sec * DateTimeUtil.OneSecTime
        local targetTime = hour * DateTimeUtil.OneHourTime + min * DateTimeUtil.OneMinTime + sec * DateTimeUtil.OneSecTime
        if nowTime >= targetTime then
            targetTime = targetTime + DateTimeUtil.OneDayTime
        end
        local interval = isloopNextDay and DateTimeUtil.OneDayTime or 0
        -- LoggerHelper.Log("Ash: SetTimeCallback: " .. (targetTime - nowTime) .. " " .. interval)
        local timerId = TimerHeap:AddSecTimer(targetTime - nowTime, interval, 0, func)
        table.insert(DateTimeUtil.allTimerId, timerId)
        return timerId
    else
        local data = {hour = hour, min = min, sec = sec, isloopNextDay = isloopNextDay, func = func}
        table.insert(DateTimeUtil.timeCallbackWaitList, data)
        return 0
    end
end

function DateTimeUtil.DelTimeCallback(timerId)
    TimerHeap:DelTimer(timerId)
end

--获取开服时间
function DateTimeUtil.GetOpenServerTime()
    return DateTimeUtil.openServerTimeStamp
end

--设置服务器开服时间
function DateTimeUtil.ResetOpenServerTime(serverTime)--单位（秒）
    DateTimeUtil.openServerTimeStamp = serverTime
end

function DateTimeUtil.GetIsInitTime()
    return DateTimeUtil._initTime
end

--设置服务器时间
function DateTimeUtil.ResetServerTime(serverTime)--单位（秒）
    DateTimeUtil.serverTimeStamp = serverTime - _osTime()
    if DateTimeUtil.hasSetTime ~= true then
        DateTimeUtil.hasSetTime = true
        DateTimeUtil.SetTimeCallbackWaitList()
    end
    if DateTimeUtil._initTime == nil then
        EventDispatcher:TriggerEvent(GameEvents.INIT_SERVER_TIME)
        DateTimeUtil._initTime = true
    end
end

--获取服务器时间
function DateTimeUtil.GetServerTime()
    return _osTime() + DateTimeUtil.serverTimeStamp
end

--获取当前是开服第几天
function DateTimeUtil.GetServerOpenDays()
    local openTime = DateTimeUtil.GetOpenServerTime()--开服时间
    local nowTime = DateTimeUtil.GetServerTime()
    
    local diff_time = (nowTime - openTime)
    local diff_day = math.floor((diff_time / 86400) + 1)
    return diff_day
end

--获取当前时间日期的table
--返回格式：{year=2016, month=11, day=6, hour=14, min=12, sec=40}
function DateTimeUtil.Now(addtiveSeconds)
    return _osDate("*t", DateTimeUtil.GetServerTime() + (addtiveSeconds or 0))
end

--获取今天某时间点的时间戳
function DateTimeUtil.TodayTimeStamp(hour, min)
    local nowDate = DateTimeUtil.Now()
    local nowTime = DateTimeUtil.GetServerTime()
    return nowTime + (hour - nowDate.hour) * DateTimeUtil.OneHourTime + (min - nowDate.min) * DateTimeUtil.OneMinTime
end

--今天是周几 1代表周1
function DateTimeUtil.WhatDay(wday)
    if wday - 1 == 0 then
        return 7
    else
        return wday - 1
    end
end
--获取指定时间日期的table
--返回格式：{year=2016, month=11, day=6, hour=14, min=12, sec=40}
function DateTimeUtil.SomeDay(newTime)
    return _osDate("*t", newTime)
end

function DateTimeUtil.SameDay(time)
    local targetTime = DateTimeUtil.SomeDay(time)
    local nowTime = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
    if targetTime.year == nowTime.year and targetTime.month == nowTime.month and targetTime.day == nowTime.day then
        return true
    end
    return false
end

--获取两个时间戳的差值
--返回格式s
function DateTimeUtil.MinusSec(targetTime)
    local gap = math.abs(DateTimeUtil.GetServerTime() - targetTime)
    return gap
end

--获取两个时间戳的差值
--返回格式{day=6, hour=14, min=12, sec=40}
function DateTimeUtil.MinusDay(newTime, oldTime)
    local gap = math.abs(newTime - oldTime)
    local target = {}
    target.day = math.floor(gap / Transform_times.day)
    gap = gap - target.day * Transform_times.day
    target.hour = math.floor(gap / Transform_times.hour)
    gap = gap - target.hour * Transform_times.hour
    target.min = math.floor(gap / Transform_times.min)
    gap = gap - target.min * Transform_times.min
    target.sec = gap
    return target
end
--获取服务器时间与指定日期的时间差
--返回格式{day=6, hour=14, min=12, sec=40}
function DateTimeUtil.MinusServerDay(targetTime)
    return DateTimeUtil.MinusDay(DateTimeUtil.GetServerTime(), targetTime)
end

-------------------------------
--判断某个时间的日期格式化后的数值， 格式为：year*10000 + month*100 + day
-------------------------------
function DateTimeUtil.GetDateFormatNumber(oneTime)
    local time_fmt_tbl = oneTime or DateTimeUtil.Now()
    return time_fmt_tbl.year * 10000 + time_fmt_tbl.month * 100 + time_fmt_tbl.day
end

--获取某个时间的时分字段格式化后的数值，格式为:hour*10000+minute*100+second
function DateTimeUtil.GetHMSFormatNumber(oneTime)
    local time_fmt_tbl = oneTime or DateTimeUtil.Now()
    return time_fmt_tbl.hour * 10000 + time_fmt_tbl.min * 100 + time_fmt_tbl.sec
end



--根据分钟返回**小时**分
function DateTimeUtil:GetMinFormatHHMM(mins)
    local hour = math.floor(mins / 60)
    local min = mins - hour * 60
    if hour == 0 then
        return mins .. LanguageDataHelper.CreateContent(149)
    end
    return hour .. LanguageDataHelper.CreateContent(148) .. min .. LanguageDataHelper.CreateContent(149)
end

--------------------------------
--判断某个时间是否在某个时间段内
--@param one_time: 某个时间
--@param begin_time: 开始时间，格式为：hour*100 + minutes
--@param dur_time: 持续时间，单位为秒
--------------------------------
function DateTimeUtil.IsTimeInHMRange(one_time, begin_time, dur_time)
    local one_time_hms_num = DateTimeUtil.GetHMSFormatNumber(one_time)
    local begin_time_hms_num = begin_time * 100
    local dur_time_hour = math.floor(dur_time / 3600)
    local dur_time_min = math.floor((dur_time - dur_time_hour * 3600) / 60)
    local dur_time_sec = dur_time - dur_time_hour * 3600 - dur_time_min * 60
    local dur_time_num = dur_time_hour * 10000 + dur_time_min * 100 + dur_time_sec
    return one_time_hms_num >= begin_time_hms_num and one_time_hms_num < begin_time_hms_num + dur_time_num
end

--------------------------------
--判断某个时间是否在某个时间段内
--@param one_time: 某个时间
--@param begin_time: 开始时间，单位为秒, 如：14:30开始，则为14*3600 + 30*60
--@param dur_time: 持续时间，单位为秒
--------------------------------
function DateTimeUtil.CheckTimeByCondition(begin_time, dur_time, one_time)
    local time = one_time or DateTimeUtil.Now()
    local seconds = time.hour * DateTimeUtil.OneHourTime + time.min * DateTimeUtil.OneMinTime + time.sec
    local end_time = begin_time + dur_time
    if seconds >= end_time then
        return DateTimeUtil.Over
    elseif seconds < begin_time then
        return DateTimeUtil.Not_Open
    else
        return DateTimeUtil.Openning
    end
end


-- 数字 转化为 中文
-- e.g.  16 = 十六; 10 = 十; 26 = 二十六; 20 = 二十
-- 暂只支持两位数，用于日期
function DateTimeUtil.Num2Chinese(num)
    local bits = {}
    local count = 0
    local temp = num
    while temp > 0 do
        count = count + 1
        bits[count] = temp % 10
        temp = (temp - bits[count] * math.pow(10, count - 1)) / 10
    end
    
    if count > 2 then
        return num
    else
        local tab = {}
        for i = count, 1, -1 do
            if not (bits[i] == 0 or (i == 2 and bits[i] == 1)) then
                table.insert(tab, LanguageDataHelper.CreateContent(bits[i] + 60014))
            end
            table.insert(tab, LanguageDataHelper.CreateContent(60024))
        end
        tab[#tab] = nil
        return table.concat(tab)
    end
end

--- 除去当前hhmmss的时间
function DateTimeUtil.GetDayStartTime(ts)
    local date = DateTimeUtil.SomeDay(ts)
    return ts - ((date.hour - 12) * DateTimeUtil.OneHourTime + date.min * DateTimeUtil.OneMinTime + date.sec)--hour默认是12
end

-- 是否闰年
function DateTimeUtil.isLeapYear(year)
    return ((year % 4 == 0) and (year % 100 ~= 0)) or (year % 400 == 0)
end


-- 获得两个日期之间的时间差
-- daysTab ：[年份] = {每月天数}
-- curDate  当前日期
-- m, d 目标月、日
-- 返回: 天数
function DateTimeUtil.GetDateOffsetDay(daysTab, curDate, m, d)
    local yearDays = daysTab[curDate.year]-- 今年每月天数
    -- if not yearDays then
    --     LoggerHelper.Log({daysTab, curDate})
    -- end
    local days
    if curDate.month == m and d >= curDate.day then
        days = d - curDate.day
    elseif m > curDate.month then
        days = d + yearDays[curDate.month] - curDate.day
        for i = curDate.month + 1, m - 1 do
            days = days + yearDays[i]
        end
    else
        days = d + yearDays[curDate.month] - curDate.day
        for i = curDate.month + 1, 12 do
            days = days + yearDays[i]
        end
        curDate = daysTab[curDate.year + 1]-- 下年每月天数
        for i = 1, m - 1 do
            days = days + yearDays[i]
        end
    end
    return days
end


local _LeapYearDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}-- 闰年
local _YearDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
-- 返回列表：新历年对应的每月天数
-- DateTimeUtil.YearDays[year] = {每月天数}
DateTimeUtil.YearDays = {}
setmetatable(DateTimeUtil.YearDays, {__index = function(t, year)
    return DateTimeUtil.isLeapYear(year) and _LeapYearDays or _YearDays
end})

--获取今天00:00开始已经过去的分钟数
function DateTimeUtil.GetTodayMins()
    local now_tm = DateTimeUtil.Now()
    return now_tm.hour * 60 + now_tm.min
end


--得到当月天数
function DateTimeUtil.GetCurMonthTodayNum()
    local timeDay = DateTimeUtil.Now()
    local yearsday = DateTimeUtil.YearDays[timeDay["year"]]
    return yearsday[timeDay["month"]]
end


--返回当前是星期几，星期一返回1，星期日返回7
function DateTimeUtil.GetWday(oneTime)
    local now_tm = oneTime or DateTimeUtil.Now()
    local wday = now_tm.wday
    wday = wday - 1
    if 0 == wday then
        wday = 7
    end
    return wday
end

--格式化时间为：分：秒的格式，如：21:30
--leaveMin:分
function DateTimeUtil:ParseHourMinTime(leaveMin)
    if leaveMin == 0 then
        return "00:00"
    end
    local hour = 0
    local min = 0
    if leaveMin % 60 == 0 then
        hour = math.ceil(leaveMin / 60)
    else
        hour = math.ceil(leaveMin / 60) - 1
    end
    if hour < 0 then
        hour = 0
    end
    local min = leaveMin % 60
    if string.len(tostring(hour)) == 1 then
        hour = "0" .. hour
    end
    
    if string.len(tostring(min)) == 1 then
        min = "0" .. min
    end
    local timeText = hour .. ":" .. min
    return timeText
end

--格式化时间为：分：秒的格式，如：21:30
--leaveSeconds:秒
function DateTimeUtil:ParseTime(leaveSeconds)
    if leaveSeconds == 0 then
        return "00:00"
    end
    local minute = 0
    local seconds = 0
    if leaveSeconds % 60 == 0 then
        minute = math.ceil(leaveSeconds / 60)
    else
        minute = math.ceil(leaveSeconds / 60) - 1
    end
    if minute < 0 then
        minute = 0
    end
    local seconds = leaveSeconds % 60
    if string.len(tostring(minute)) == 1 then
        minute = "0" .. minute
    end
    
    if string.len(tostring(seconds)) == 1 then
        seconds = "0" .. seconds
    end
    local timeText = minute .. ":" .. seconds
    return timeText
end

-- 传入时间格式为：hour*100 + minutes
-- 返回秒数
function DateTimeUtil:HMToSecond(hmTime)
    local hour = math.floor(hmTime / 100)
    return hour * 3600 + (hmTime - hour * 100) * 60
end

-- 传入时间格式为：date对象
-- 返回秒数
function DateTimeUtil:DateTimeToSecond(dateTime)
    return dateTime.hour * Transform_times.hour + dateTime.min * Transform_times.min + dateTime.sec
end

-- 时间戳
--返回：2017.12.31
function DateTimeUtil.GetDateStr(t)
    local d = _osDate("*t", t)
    return string.format("%s.%s.%s", d.year, d.month, d.day)
end

-- 时间戳
--返回：2017.12.31 11:09:01
function DateTimeUtil.GetDateFullStr(t)
    local d = _osDate("*t", t)
    return string.format("%s.%s.%s %02d:%02d:%02d", d.year, d.month, d.day, d.hour, d.min, d.sec)
end

-- 时间戳
--返回：11:09:01
function DateTimeUtil.GetDateTimeStr(t)
    local d = _osDate("*t", t)
    return string.format("%02d:%02d:%02d", d.hour, d.min, d.sec)
end

-- 时间戳
--返回：11:09
function DateTimeUtil.GetDateHourMinStr(t)
    local d = _osDate("*t", t)
    return string.format("%02d:%02d", d.hour, d.min)
end

-- 时间戳
--返回：2017.12.31
function DateTimeUtil.GetDateMonthDayStr(t)
    local d = _osDate("*t", t)
    return string.format("%s" .. LanguageDataHelper.CreateContent(533) .. "%s" .. LanguageDataHelper.CreateContent(534), d.month, d.day)
end

--格式化时间为：时：分：秒的格式，如：15：21:30
--leaveSeconds:秒
--noSec 不显示秒（可选）
function DateTimeUtil:FormatFullTime(leaveSeconds, noSec, noHour, useChinese)
    local hour = math.modf(leaveSeconds / 3600)
    leaveSeconds = leaveSeconds - 3600 * hour
    local minute = math.modf(leaveSeconds / 60)
    leaveSeconds = leaveSeconds - 60 * minute
    local seconds = leaveSeconds
    
    local strHour = DateTimeUtil:FormatNumber(hour)
    local strMinute = DateTimeUtil:FormatNumber(minute)
    local strSeconds = DateTimeUtil:FormatNumber(seconds)
    
    local tab = {}
    
    if not noHour then
        if useChinese then
            if hour ~= 0 then
                table.insert(tab, hour)
                table.insert(tab, LanguageDataHelper.CreateContent(148))
            end
        else
            table.insert(tab, strHour)
            table.insert(tab, ":")
        end
    end
    
    if useChinese then
        table.insert(tab, minute)
        table.insert(tab, LanguageDataHelper.CreateContent(149))
    else
        table.insert(tab, strMinute)
        table.insert(tab, ":")
    end
    
    if not noSec then
        if useChinese then
            table.insert(tab, seconds)
            table.insert(tab, LanguageDataHelper.CreateContent(150))
        else
            table.insert(tab, strSeconds)
            table.insert(tab, ":")
        end
    end
    if not useChinese then
        tab[#tab] = nil
    end
    
    return table.concat(tab)
end

--格式化时间为：xxxx年xx月xx日的格式
--d:日期对象
function DateTimeUtil.FormatFullDate(d, noYear, noMonth, useChinese)
    if d == nil then
        return ""
    end
    local tab = {}
    if not noYear then
        if useChinese then
            table.insert(tab, d.year)
            table.insert(tab, LanguageDataHelper.CreateContent(535))
        else
            table.insert(tab, d.year)
            table.insert(tab, ".")
        end
    end
    if not noMonth then
        if useChinese then
            table.insert(tab, d.month)
            table.insert(tab, LanguageDataHelper.CreateContent(533))
        else
            table.insert(tab, d.month)
            table.insert(tab, ".")
        end
    end
    
    if useChinese then
        table.insert(tab, d.day)
        table.insert(tab, LanguageDataHelper.CreateContent(534))
    else
        table.insert(tab, d.day)
    end
    
    return table.concat(tab)
end

function DateTimeUtil.NewDate(year, month, day)
    return _osTime({year = year, month = month, day = day, hour = 0, min = 0, sec = 0})
end

function DateTimeUtil.NewTime(year, month, day, hour, min, sec)
    return _osTime({year = year, month = month, day = day, hour = hour, min = min, sec = sec})
end

function DateTimeUtil.AddOneMonth(d, day)
    if d == nil then
        return nil
    end
    local new_year = d.year
    local new_month = d.month
    if new_month == 12 then
        new_year = new_year + 1
        new_month = 1
    else
        new_month = new_month + 1
    end
    
    return _osTime({year = new_year, month = new_month, day = day and day or d.day, hour = 0, min = 0, sec = 0})
end

function DateTimeUtil.MinusOneMonth(d, day)
    if d == nil then
        return nil
    end
    local new_year = d.year
    local new_month = d.month
    if new_month == 1 then
        new_year = new_year - 1
        new_month = 12
    else
        new_month = new_month - 1
    end
    
    return _osTime({year = new_year, month = new_month, day = day and day or d.day, hour = 0, min = 0, sec = 0})
end

function DateTimeUtil:FormatNumber(num)
    if num < 10 then
        num = '0' .. num
    end
    return num
end

-- 服务器算法，计算当前是开服第几周
function DateTimeUtil.get_weekth_of_year()
    local curTime = DateTimeUtil.GetServerTime()
    local curDate = DateTimeUtil.SomeDay(curTime)
    local theTime = os.time({year = curDate.year, month = 1, day = 1})
    local wDay = os.date("*t", theTime).wday - 1
    if wDay <= 0 then wDay = 7 end --星期天变为一周的第七天
    local yDay = curDate.yday
    local theWeek = math.floor((wDay + yDay - 1 + 6) / 7)
    return theWeek
end

--获取某个时间到下个周一五点的秒数
function DateTimeUtil.GetToNextMondayFiveOclockSec(from_time)
    local now = from_time or DateTimeUtil.GetServerTime()
    local now_tm = os.date('*t', now)
    local wday = now_tm.wday --lua中sunday是1
    if wday == 1 then
        wday = 8
    end
    local time1 = now - (wday - 2) * 86400
    local time1_tm = os.date('*t', time1)
    local monday_five_oclock = os.time({year = time1_tm.year, month = time1_tm.month, day = time1_tm.day, hour = 05, min = 00, sec = 00})
    if now > monday_five_oclock then
        --下周一
        time1 = time1 + 7 * 86400
        time1_tm = os.date('*t', time1)
        monday_five_oclock = os.time({year = time1_tm.year, month = time1_tm.month, day = time1_tm.day, hour = 05, min = 00, sec = 00})
    end
    return (monday_five_oclock - now)
end

--格式化时间为：{day=1,hour=1,min=1,sec=1} 优先显示首要单位数据 如day==0&&hour~=0则显示hour，以此类推
--leaveSeconds:秒
function DateTimeUtil.FormatParseTime(leaveSeconds)
    local result = {day = 0, hour = 0, min = 0, sec = 0}
    if leaveSeconds ~= 0 then
        local day = 0
        local hour = 0
        local minute = 0
        if leaveSeconds % 86400 == 0 then
            day = math.ceil(leaveSeconds / 86400)
        else
            day = math.ceil(leaveSeconds / 86400) - 1
        end
        result.day = day
        leaveSeconds = leaveSeconds - day * 86400
        
        if leaveSeconds % 3600 == 0 then
            hour = math.ceil(leaveSeconds / 3600)
        else
            hour = math.ceil(leaveSeconds / 3600) - 1
        end
        result.hour = hour
        leaveSeconds = leaveSeconds - hour * 3600
        
        if leaveSeconds % 60 == 0 then
            minute = math.ceil(leaveSeconds / 60)
        else
            minute = math.ceil(leaveSeconds / 60) - 1
        end
        result.min = minute
        leaveSeconds = leaveSeconds - minute * 60
        
        result.sec = leaveSeconds
    end
    
    if result.day ~= 0 then
        return result.day .. DateTimeUtil.DayString
    end
    if result.hour ~= 0 then
        return result.hour .. DateTimeUtil.HourString
    end
    if result.min ~= 0 then
        return result.min .. DateTimeUtil.MinString
    end
    return result.sec .. DateTimeUtil.SecString
end

--格式化时间为：{hour=1,min=1,sec=1}
--leaveSec:秒
function DateTimeUtil.FormatParseTimeStr(leaveSec)
    local result = {day = 0, hour = 0, min = 0, sec = 0}
    if leaveSec ~= 0 then
        local day = 0
        local hour = 0
        local minute = 0
        if leaveSec % 86400 == 0 then
            day = math.ceil(leaveSec / 86400)
        else
            day = math.ceil(leaveSec / 86400) - 1
        end
        result.day = day
        leaveSec = leaveSec - day * 86400
        
        if leaveSec % 3600 == 0 then
            hour = math.ceil(leaveSec / 3600)
        else
            hour = math.ceil(leaveSec / 3600) - 1
        end
        result.hour = hour
        leaveSec = leaveSec - hour * 3600
        
        if leaveSec % 60 == 0 then
            minute = math.ceil(leaveSec / 60)
        else
            minute = math.ceil(leaveSec / 60) - 1
        end
        result.min = minute
        leaveSec = leaveSec - minute * 60
        
        result.sec = leaveSec
    end
    
    local str = ""
    if result.day ~= 0 then
        result.hour = result.hour + result.day * 24
    end
    
    if result.hour ~= 0 then
        str = str .. result.hour .. DateTimeUtil.HourString
    end
    
    if result.min ~= 0 then
        str = str .. result.min .. DateTimeUtil.MinString
    end
    str = str .. result.sec .. DateTimeUtil.SecString
    
    return str
end


--格式化时间为：1天1小时1分钟 形式
--min:分钟
function DateTimeUtil.FormatParseTimeByMin(min)
    local result = {day = 0, hour = 0, min = 0}
    if min ~= 0 then
        local day = 0
        local hour = 0
        if min % 1440 == 0 then
            day = math.ceil(min / 1440)
        else
            day = math.ceil(min / 1440) - 1
        end
        result.day = day
        min = min - day * 1440
        
        if min % 60 == 0 then
            hour = math.ceil(min / 60)
        else
            hour = math.ceil(min / 60) - 1
        end
        result.hour = hour
        min = min - hour * 60
        
        result.min = min
    end
    local str = ""
    if result.day ~= 0 then
        str = str .. result.day .. DateTimeUtil.DayString
    end
    if result.hour ~= 0 then
        str = str .. result.hour .. DateTimeUtil.HourString
    end
    str = str .. result.min .. DateTimeUtil.MinString
    
    return str
end

--获取时间的年月日表示 2017.01.01
function DateTimeUtil.FormatYMDTime(time)
    local date = DateTimeUtil.SomeDay(time)
    local year = tostring(date.year)
    local month = DateTimeUtil:FormatNumber(date.month)
    local day = DateTimeUtil:FormatNumber(date.day)
    local dateText = string.format("%s.%s.%s", year, month, day)
    return dateText
end

--获取距离开始时间多少天
function DateTimeUtil.GetDaysGoThrough(startTime)
    local now = DateTimeUtil.GetServerTime()
    local dSec = now - startTime
    --还没开始
    if dSec <= 0 then
        return 0
    end
    local startDate = DateTimeUtil.SomeDay(startTime)
    local startDaySecond = startDate.hour*3600 + startDate.min*60 + startDate.sec --第一天剩余秒数
    local fisrtDaySecondLeft = DateTimeUtil.OneDayTime - startDaySecond
    if dSec < fisrtDaySecondLeft then
        return 1
    else
        return math.ceil((dSec-fisrtDaySecondLeft)/DateTimeUtil.OneDayTime) + 1
    end
end

return DateTimeUtil
