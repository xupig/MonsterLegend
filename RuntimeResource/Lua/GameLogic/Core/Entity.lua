local Entity = Class.Entity(ClassTypes.XObject)
--实体定义抽象类，不直接创建，只用于继承

local EntityMgr = Class.EntityMgr(ClassTypes.XObject)
EntityMgr.entities = {}

function EntityMgr:GetEntity(entID)
    return self.entities[entID]
end

function EntityMgr:GetEntityByNpcId(npc_id)
	local list = {}
	for k, v in pairs(self.entities) do
		if v.npc_id == npc_id then
			table.insert(list, v)
		end
	end
	return list
end

function EntityMgr:GetEntityByMonsterId(monster_id)
	local list = {}
	for k, v in pairs(self.entities) do
		if v.monster_id == monster_id then
			table.insert(list, v)
		end
	end
	return list
end

function EntityMgr:GetEntitiesByType(type)
	local list = {}
	for k, v in pairs(self.entities) do
		if v.entityType == type then
			table.insert(list, v)
		end
	end
	return list
end
-------------------------------Entity定义-------------------------------------------------
local table = table
local SetNameMap = {}
function GetSetName(attrName)
	if SetNameMap[attrName] == nil then
		SetNameMap[attrName] = "set_"..attrName
	end
	return SetNameMap[attrName]
end

function Entity:__ctor__(csEntity)
	self.cs = csEntity
	self.id = csEntity.id
	self.PropertyEventName = {}
	self.PropertyListeners = {}
	EntityMgr.entities[self.id] = self
end

function Entity:__desctor__()
	EntityMgr.entities[self.id] = nil
	self.cs = nil
end

function Entity:__reuse__(id)
	self.id = id
	self.PropertyEventName = {}
	self.PropertyListeners = {}
	EntityMgr.entities[self.id] = self
	self:__OnReuse()
end

function Entity:__release__()
	EntityMgr.entities[self.id] = nil
	self:__OnRelease()
end

function Entity:__setattr__(attrName, value)
	local oldValue = self[attrName]
	self[attrName] = value
	self:_callSet(attrName, oldValue)
end

function Entity:__setattrs__(attrNames, attrValues, count)

end

function Entity:__setattrstable__(attrs)
	for k,v in pairs(attrs) do
		self:__setattr__(k, v)
	end
end

function Entity:_callSet(attrName, oldValue)
	local setName = GetSetName(attrName)
	if self[setName] ~= nil then
		self[setName](self, oldValue)
	end
	if self.PropertyListeners[attrName] ~= nil then
		EventDispatcher:TriggerEvent(self:GetPropertyEventName(attrName), self.id)
	end
end

function Entity:OnEnterWorld()

end

function Entity:OnLeaveWorld()

end

function Entity:__OnReuse()

end

function Entity:__OnRelease()

end
-- 用于npc进入主角范围后切换为正式模型
function Entity:__OnEnterDistance()

end

function Entity:OnEnterSpace()

end

function Entity:OnLeaveSpace()

end

function Entity:Teleport(position)
	self.cs:Teleport(position)
end

function Entity:SetEuler(euler)
	self.cs:SetEuler(euler)
end

function Entity:GetPropertyEventName(attrName)
	if self.PropertyEventName[attrName] == nil then
		self.PropertyEventName[attrName] = tostring(self.id)..attrName
	end
	return self.PropertyEventName[attrName]
end

function Entity:AddPropertyListener(attrName, func)
	if self.PropertyListeners[attrName] == nil then
		self.PropertyListeners[attrName] = true
	end
	EventDispatcher:AddEventListener(self:GetPropertyEventName(attrName), func)
end

function Entity:RemovePropertyListener(attrName, func)
	EventDispatcher:RemoveEventListener(self:GetPropertyEventName(attrName), func)
end

function Entity:ParsePB(pbclass, bytes)
    local arrayBytes = {}
    local idx = 1
    for i=0, bytes.Length - 1, 1 do
        arrayBytes[idx] = bytes[i]   
        idx = idx + 1
    end    
    local arrayString = string.char(unpack(arrayBytes))
    local result = pbclass()
    result:ParseFromString(arrayString)  
    return result
end

function Entity:RpcCall()

end

function Entity:UpdateTableAll(attrName, value)
	self[attrName] = value
	self:_callSet(attrName, value)
end

function Entity:UpdateTableAttr(attrName, value)
	local oldValue = self[attrName]
	if oldValue ~= nil then
		table.merge(oldValue, value)
		self:_callSet(attrName, value)
	else
		self[attrName] = value
		self:_callSet(attrName, value)
	end
end

function Entity:RemoveFromTableAttr(attrName, key)
	local oldValue = self[attrName]
	if oldValue ~= nil then
		oldValue[key] = nil
		self:_callSet(attrName, {[table.REMOVE_FLAG] = {key}})
	end
end

function Entity:UpdateTableAttr2(attrName, value)
	local oldValue = self[attrName]
	if oldValue ~= nil then
		table.merge2(oldValue, value)
		self:_callSet(attrName, value)
	else
		self[attrName] = value
		self:_callSet(attrName, value)
	end
end

function Entity:RemoveFromTableAttr2(attrName, key1, key2)
	local oldValue = self[attrName]
	if oldValue ~= nil and oldValue[key1] ~= nil then
		oldValue[key1][key2] = nil
		self:_callSet(attrName, {[table.REMOVE_FLAG] = {key1,key2}})
	end
end

function Entity:ClearTable(attrName)
	self[attrName] = {}
	self:_callSet(attrName, {})
end

local servermt={		   
			  __index = function(t, k)
                    local func = function(...)
						MogoEngine.MogoWorld.RpcCall(k, ...)
					end
					return func
			   end
			  }
Entity.server = {}
setmetatable(Entity.server, servermt)


