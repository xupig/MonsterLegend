local Stack = Class.Stack(ClassTypes.XObject)

function Stack:__ctor__()
    self.stack_table = {}
    self.size_ = 0
end

function Stack:GetDatas()
    return self.stack_table
end

function Stack:SetDatas(table)
    self.stack_table = table
    self.size_ = #table
end

function Stack:Push(element)
    local size = self:Size()
    self.stack_table[size + 1] = element
    self.size_ = self.size_ + 1
end

function Stack:Pop()
    local size = self:Size()
    if self:IsEmpty() then
        LoggerHelper.Error("Error: Stack is empty!")
        return
    end
    self.size_ = self.size_ - 1
    return table.remove(self.stack_table, size)
end

function Stack:Top()
    local size = self:Size()
    if self:IsEmpty() then
        -- printError("Error: Stack is empty!")
        return
    end
    return self.stack_table[size]
end

function Stack:IsEmpty()
    local size = self:Size()
    if size == 0 then
        return true
    end
    return false
end

function Stack:Size()
    return self.size_
end

function Stack:Clear()
    -- body
    self.stack_table = nil
    self.stack_table = {}
    self.size_ = 0
end

function Stack:PrintElement()
    local size = self:Size()
    
    if self:IsEmpty() then
        LoggerHelper.Error("Error: Stack is empty!")
        return
    end
    
    local str = "{" .. self.stack_table[size]
    size = size - 1
    while size > 0 do
        str = str .. ", " .. self.stack_table[size]
        size = size - 1
    end
    str = str .. "}"
    print(str)
end