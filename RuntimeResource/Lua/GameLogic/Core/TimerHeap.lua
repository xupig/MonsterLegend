TimerHeap = {}


function TimerHeap:Init()
    self._maxId = 0
    self._timers = {}
end

--</summary>
--<param name="start">延迟启动时间。（毫秒）</param>
--<param name="interval">重复间隔，为零不重复。（毫秒）</param>
--<param name="end">结束时间，默认0(不自动结束)。（毫秒）</param>
--<param name="func">定时处理方法</param>
--<returns>定时对象Id</returns>
function TimerHeap:AddTimer(start, interval, endTime, func)
    start = start * 0.001
    interval = interval * 0.001
    endTime = endTime * 0.001
    return self:AddSecTimer(start, interval, endTime, func)
end

--</summary>
--<param name="start">延迟启动时间。（秒）</param>
--<param name="interval">重复间隔，为零不重复。（秒）</param>
--<param name="end">结束时间，默认0(不自动结束)。（秒）</param>
--<param name="func">定时处理方法</param>
--<returns>定时对象Id</returns>
function TimerHeap:AddSecTimer(start, interval, endTime, func)
    self._maxId = self._maxId + 1
    local timeId = self._maxId
    local loop = -1
    if endTime > 0 and interval > 0 then
        loop = endTime / interval
    end
    if interval == 0 then
        loop = 1
    end
    local timer = Timer.New(func, start, interval, loop, false)
    self._timers[timeId] = timer
    timer:Start()
    return timeId
end

function TimerHeap:DelTimer(timeId)
    if timeId == nil then
        return
    end
    local timer = self._timers[timeId]
    if timer ~= nil then
        timer:Stop()
        self._timers[timeId] = nil
    end
end

function TimerHeap:ClearData()
    local temp = {}
    for k, v in pairs(self._timers) do
        if not v.running and v.loop == 0 then
            temp[k] = true
        end
    end
    for k, v in pairs(temp) do
        self._timers[k] = nil
    end
end

TimerHeap:Init()
