local LuaBehaviour = Class.LuaBehaviour(ClassTypes.XObject)
--c#层LuaBehaviour对应的lua脚本基类，用来模仿MonoBehaviour的行为，
--一般不直接创建，只用于继承

function LuaBehaviour:__ctor__() end

--C#调用的初始化（禁止子类继承）
function LuaBehaviour:__init__(bh)
	self._csBH = bh
	self.gameObject = bh.gameObject
	self.transform = bh.transform
end

--C#调用的释放（禁止子类继承）
function LuaBehaviour:__destroy__()
	self.gameObject = nil
	self.transform = nil
	self._csBH = nil
	self:OnDestroy()
end

--对应MonoBehaviour.Awake()回调函数，有C#层对应的MonoBehaviour调用触发
function LuaBehaviour:Awake() end
--对应MonoBehaviour.Start()回调函数，有C#层对应的MonoBehaviour调用触发
function LuaBehaviour:Start() end
--对应MonoBehaviour.OnDestroy()回调函数，有C#层对应的MonoBehaviour调用触发
function LuaBehaviour:OnDestroy() end
--对应MonoBehaviour.OnEnable()回调函数，有C#层对应的MonoBehaviour调用触发
function LuaBehaviour:OnEnable() end
--对应MonoBehaviour.OnDisable()回调函数，有C#层对应的MonoBehaviour调用触发
function LuaBehaviour:OnDisable() end




