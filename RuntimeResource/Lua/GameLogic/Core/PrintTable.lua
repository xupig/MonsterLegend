PrintTable = {}

local print = print
local tconcat = table.concat
local tinsert = table.insert
local srep = string.rep
local type = type
local pairs = pairs
local tostring = tostring
local next = next
 
function PrintTable:Print(root)
	local cache = {  [root] = "." }
	local function _dump(t,space,name)
		local temp = {}
		for k,v in pairs(t) do
			local key = tostring(k)
			if cache[v] then
				tinsert(temp,"+" .. key .. " {" .. cache[v].."}")
			elseif type(v) == "table" then
				local new_key = name .. "." .. key
				cache[v] = new_key
				tinsert(temp,"+" .. key .. _dump(v,space .. (next(t,k) and "|" or " " ).. srep(" ",#key),new_key))
			else
				tinsert(temp,"+" .. key .. " [" .. tostring(v).."]")
			end
		end
		return tconcat(temp,"\n"..space)
	end
	print(_dump(root, "",""))
end

function PrintTable:_doTableToStr(t)
	local str = "{"
	local isRealTable = self:IsArrayTable(t)
	if #t == 0  and isRealTable then
		return str .. "},"
	else
		for k,v in pairs(t) do
			if type(v) == "table" then
				str = str .. "\"" .. k .. "\" = " .. self:_doTableToStr(v)
			else
				str = str ..  self:_getKVStr(k, v)
			end
		end
		str = string.sub(str,1,string.len(str)-1)
		str = str .. "},"
	end
	return str
end

function PrintTable:StringToTable(str)
	local ret = loadstring("return "..str)()  
    return ret
end

function PrintTable:TableToStr(t)
	if t == nil then return "" end
	local str = self:_doTableToStr(t)
	str = string.sub(str,1,string.len(str)-1)
	return str
end

function PrintTable:IsArrayTable(t)
	if type(t) ~= "table" then
        return false
    end

    local n = #t
    for i,v in pairs(t) do
        if type(i) ~= "number" then
            return false
        end

        if i > n then
            return false
        end
    end

    return true
end

function PrintTable:_getKVStr(k, v)
	local str = "\"" .. k .. "\" = " .. tostring(v) .. ","
	return str
end

function PrintTable:TableToStrEx(t)
    if t == nil then return "" end
    local retstr= "{"

    local i = 1
    for key,value in pairs(t) do
        local signal = ","
        if i==1 then
          signal = ""
        end

        if key == i then
            retstr = retstr..signal..self:ToStringEx(value)
        else
            if type(key)=='number' or type(key) == 'string' then
                retstr = retstr..signal..'['..self:ToStringEx(key).."]="..self:ToStringEx(value)
            else
                if type(key)=='userdata' then
                    retstr = retstr..signal.."*s"..self:TableToStrEx(getmetatable(key)).."*e".."="..self:ToStringEx(value)
                else
                    retstr = retstr..signal..key.."="..ToStringEx(value)
                end
            end
        end

        i = i+1
    end

     retstr = retstr.."}"
     return retstr
end

function PrintTable:ToStringEx(value)
    if type(value)=='table' then
       return self:TableToStrEx(value)
    elseif type(value)=='string' then
        return "\'"..value.."\'"
    else
       return tostring(value)
    end
end

return PrintTable