local table = table

table.REMOVE_FLAG = -1
-- @param tDest 目标表格(t表示是table)
-- @param tSrc 来源表格(t表示是table)
function table.merge(tDest, tSrc)
    for k, v in pairs(tSrc) do
        tDest[k] = v
    end
end

-- 用于table的二层merge
function table.merge2(tDest, tSrc)
    for k, v in pairs(tSrc) do
        if tDest[k] == nil then
            tDest[k] = v
        else
            table.merge(tDest[k], v)
        end
    end
end

--用于判断tbale是否为空,为空的时候返回true
function table.isEmpty(tDest)
    return _G.next(tDest) == nil
end

function table.getTableCount(t)
    local i = 0
    for k,v in pairs(t) do
        i = i + 1
    end
    return i
end

--用于判断table的value是否包含
function table.containsValue(table, element)
    for k, value in pairs(table) do
        if value == element then
            return true
        end
    end
    return false
end

--深度拷贝
--@depth 深度
--@src 源
--@dest 目标
--@return 同dest
function table.deepCopy(depth, src, dest)
    if type(src) ~= "table" then return src end
    if depth < 1 then return end
    if not dest then dest = {} end
    for k,v in pairs(src) do
        if type(v) == "table" then
            if depth <= 1 then
                dest[k] = v
            else
                dest[k] = table.deepCopy(depth-1, v)
            end
        else
            dest[k] = v
        end
    end
    return dest
end

--克隆表格
function table.cloneTable(src, dest)
    return table.deepCopy(10, src, dest)
end

--deepcopy,只copy1层
function table.deepCopy1(t)
    return table.deepCopy(1, t)
end
