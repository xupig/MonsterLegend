local string = string

function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    
    local result = {}
    for match in (str .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match)
    end
    return result
end

function string.IsNullOrEmpty(str)
    if str == "" or str == nil then
        return true
    else
        return false
    end
end

function string.LastIndexOf(str, seperator)
    local lastIndex = 1
    local s, e = string.find(str, seperator, 0)
    while s do
        lastIndex = s
        s, e = string.find(str, seperator, s + 1)
    end
    return lastIndex
end

function string.StartWith(str, substr)
    if str == nil or substr == nil then
        return nil, "the string or the sub-stirng parameter is nil"
    end
    if string.find(str, substr) ~= 1 then
        return false
    else
        return true
    end
end

function string.GetExtension(str)
    if not string.find(str, ".") then
        return ""
    end
    local index = string.len(str)
    local extIndex = index
    for i = index, 1, -1 do
        if string.sub(str, i, i) == "." then
            extIndex = i
            break
        end
    end
    return string.sub(str, extIndex, index)
end

--根据分隔符切割字符串
--func = nil(默认返回str) or tonumber or
function string.split_str(str, delim, func)
    local i = 0
    local j = 1
    local t = {}
    while i ~= nil do
        i = string.find(str, delim, j)
        if i ~= nil then
            if func then
                table.insert(t, func(string.sub(str, j, i - 1)) or string.sub(str, j, i - 1))
            else
                table.insert(t, string.sub(str, j, i - 1))
            end
            j = i + 1
        else
            if func then
                table.insert(t, func(string.sub(str, j)) or string.sub(str, j))
            else
                table.insert(t, string.sub(str, j))
            end
        end
    end
    return t
end
--返回字符串utf8的长度
function string.utf8len(input)
    local len = string.len(input)
    local left = len
    local cnt = 0
    local arr = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
    while left ~= 0 do
        local tmp = string.byte(input, -left)
        local i = #arr
        while arr[i] do
            if tmp >= arr[i] then
                left = left - i
                break
            end
            i = i - 1
        end
        cnt = cnt + 1
    end
    return cnt
end
--返回字符串utf8的长度,不算颜色配置<color/>的长度
function string.utf8lenwithoutcolor(input)
    local len = string.len(input)
    local left = len
    local cnt = 0
    local arr = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
    while left ~= 0 do
        local tmp = string.byte(input, -left)
        local i = #arr
        while arr[i] do
            if tmp >= arr[i] then
                left = left - i
                break
            end
            i = i - 1
        end
        cnt = cnt + 1
    end
    local i = 0
    local colorCount = 0
    while true do
        i = string.find(input, "<color=#.+</color>", i + 1)
        if i == nil then
            break
        end
        colorCount = colorCount + 1
    end
    return cnt - colorCount * 23
end
