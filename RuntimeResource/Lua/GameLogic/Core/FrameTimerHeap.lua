
FrameTimerHeap = {}


function FrameTimerHeap:Init()
	self._maxId = 0
	self._timers = {}
end

function FrameTimerHeap:AddTimer(count, loop, func)
	local timeId = self._maxId + 1
	local timer = FrameTimer.New(func, count, loop)
	self._timers[timeId] = timer
	timer:Start()
	return timeId
end

function FrameTimerHeap:DelTimer(timeId)
	local timer = self._timers[timeId]
	if timer ~= nil then
		timer:Stop()
		self._timers[timeId] = nil
	end
end

FrameTimerHeap:Init()
