LoggerHelper = {}
local Debugger = Debugger
local PrintTable = GameWorld.PrintTable
local string = string
local logFilter = ""
local debug = debug

local function GetColorString(content, color)
	return string.format("<color=%s>%s</color>", color or "#ffffffff", content)
end

function LoggerHelper.Init()
	logFilter = GameLoader.SystemConfig.logFilter
end

function LoggerHelper.CheckFilter(content)
	if content == nil then return false end
	if #content > 0 then
		local idx = string.find(content, logFilter) 
		return idx ~= nil
	end
	return true
end

function LoggerHelper.LogEx(content)
	if type(content) == "table" then
		content = PrintTable:TableToStr(content) .. " \n traceback: \n " ..debug.traceback()
	else
		content = content .. " \n traceback: \n " ..debug.traceback()
	end
	return LoggerHelper.Log(content)
end

function LoggerHelper.Log(content, color)
	if type(content) == "table" then
		content = PrintTable:TableToStr(content)
	end
	if not LoggerHelper.CheckFilter(tostring(content)) then return end
	Debugger.Log(GetColorString(tostring(content), color))
end

function LoggerHelper.Error(content, printTraceback)
	if not LoggerHelper.CheckFilter(content) then return end
	if printTraceback == true then
		content = content .. " \n traceback: \n " ..debug.traceback()
	end
	Debugger.LogError(content)
end

function LoggerHelper.Warning(content)
	if not LoggerHelper.CheckFilter(content) then return end
	Debugger.LogWarning(content)
end

return LoggerHelper



