Class = {}
ClassTypes = {}
local mt = {}
local rawget = rawget
local debug = debug

local function _visit_constructor(obj, t)
  if obj then
    t[#t+1] = obj
  end

  if obj._base then
    _visit_constructor(obj._base,t)
  else
    return
  end
end

local function BuildClass(classname, base)
   local c = {}    -- a new class instance
   local _baseList = nil
   if type(base) == 'table' then
      -- our new class is a shallow copy of the base class!
      for i,v in pairs(base) do
         c[i] = v
      end
      if UnityEngine.Application.isEditor then --用于检查类方法的重复定义
        if base.____temp then  
            for i,v in pairs(base.____temp) do
                local temp = c.____temp
                c[i] = v
            end
        end
      end
      -- record base
      c._base = base
      -- get base list, 获取父类列表
      _baseList = {}
      _visit_constructor(base,_baseList)
      c._baseList = _baseList
   elseif classname ~= "XObject" then
      Debugger.LogError(classname .. " class parent must be not nil, you can inherit XObject as parent! ")
   end
   -- the class will be the metatable for all its objects,
   -- and they will look up their methods in it.
   c.__index = c
   c._classType = classname
   -- expose a constructor which can be called by <classname>(<args>)
   local mt = {}
   mt.__call = function(class_tbl, ...)
     local obj = {}
     setmetatable(obj,c)
     -- inheri constructor
     if _baseList then
        local _size = #_baseList --依次调用 __ctor__ call
        for i=_size, 1 , -1 do
          _baseList[i].__ctor__(obj,...)
        end
     end
     obj:__ctor__(...)
     return obj
   end

   c.is_a = function(self, klass)
      local m = getmetatable(self)
      while m do
         if m == klass then return true end
         m = m._base
      end
      return false
   end

   if UnityEngine.Application.isEditor then
        local ____temp = {}
        c.____temp = ____temp
        c.__index = function(self, key)

            local value = rawget(self, key)
            if value == nil then 
                local m = getmetatable(self)
                value = rawget(m, key)
            end
            if value == nil then 
                value = ____temp[key]
            end
            return value
        end

        mt.__index = function(self, key)
            return ____temp[key]
        end

        mt.__newindex = function(self, key, value)
            local baseHas = base and base[key] ~= nil or false
            local selfHas = ____temp[key] ~= nil
            if not baseHas and selfHas then
                Debugger.LogError("类" .. tostring(classname) .. "方法定义重复:"..key)
            end
            ____temp[key] = value
        end
   end

   setmetatable(c, mt)
   return c
end

function mt:__index(name)
    return function(...)
        local temp = rawget(ClassTypes, name)
        -- local temp = ClassTypes[name]--getfenv()[name]
        if temp ~= nil then
            Debugger.LogError(string.format("classname %s has been used\n", name) .. debug.traceback())
            --return nil
        end
        local c = Class(name, ...)
        --strict_ignore_set = true;
        --getfenv()[name] = c
        ClassTypes[name] = c
        --strict_ignore_set = false
        return c
    end
end

function mt:__call(classname, super)
    return BuildClass(classname, super)
end

setmetatable(Class, mt)



local XObject = Class.XObject()
function XObject:__ctor__() end


-- 类的正确引用流程:  require -> Class -> ClassTypes
setmetatable(ClassTypes, {
  __index = function(t, k)
        local c = rawget(t, k)
        if c == nil then
          Debugger.LogError(string.format("Class %s is not required\n", k) .. debug.traceback())
        end
        return c
    end
})