-- 游戏启动时第一个应该require的模块
-- 游戏核心API模块，提供除UNITY自身提供的API外所有核心API接口。
-- 每一个接口必须严格定义，并详细注释。
-- 所有core内部定义的类都不应该直接实例化，而应该通过此模块进行获取创建或者删除替换等。
require "UnityEngine/LayerMask"
require "UnityEngine/Mathf"
require "UnityEngine/Quaternion"
require "UnityEngine/Vector4"
require "UnityEngine/Vector3"
require "UnityEngine/Vector2"
require "UnityEngine/Color"
require "UnityEngine/Time"

require "Core/StringEx"
require "Core/TableEx"
require "Core/Class" -- 提供super_class类定义方式
require "Core/LuaBehaviour"
require "Core/EventDispatcher"
require "Core/Entity" -- 提供实体定义Entity 实体管理EntityManger
require "Core/TimerHeap"
require "Core/FrameTimerHeap"
require "Core/Queue"
require "Core/Stack"

GameWorld = {}--游戏全局主模块，大部分核心接口都通过此模块提供

GameWorld.public_config = require("ServerConfig/public_config")
GameWorld.attri_config = require("ServerConfig/attri_config")
GameWorld.error_code = require("ServerConfig/error_code")
GameWorld.action_config = require("ServerConfig/action_config")

GameWorld.PrintTable = require "Core/PrintTable"
GameWorld.LoggerHelper = require "Core/LoggerHelper"

GameWorld.CANVAS_WIDTH = 1280
GameWorld.CANVAS_HEIGHT = 720
GameWorld.ProgressBar = GameLoader.LoadingBar.ProgressBar.Instance
--存放加载生成的BT字典，方便调用
GameWorld.btTable = {}
--注册Update函数
GameWorld.Update = UpdateBeat
--注册LateUpdate函数
GameWorld.LateUpdate = LateUpdateBeat
--注册FixedUpdate函数
GameWorld.FixedUpdate = FixedUpdateBeat
--注册CoUpdate函数
GameWorld.CoUpdate = CoUpdateBeat
--注册Event函数
GameWorld.Event = event

--注册Timer的便捷接口类
GameWorld.TimerHeap = TimerHeap
--注册FrameTimer的便捷接口类
GameWorld.FrameTimerHeap = FrameTimerHeap

--Xml解释器
GameWorld.XMLParser = require("Core/XmlSimple").newParser()

--json
GameWorld.json = require "cjson"

local LuaFacade = GameMain.LuaFacade
local EntityMgr = ClassTypes.EntityMgr

GameWorld.LuaGameState = GameMain.LuaGameState

GameWorld.IsClient = true

-- GameWorld.LastBlock = nil
GameWorld.aiCanThink = true --ai总开关
GameWorld.battleServerMode = true --是否服务器本
GameWorld.isManualReturnLogin = false --标记是否是主动退出登录，不弹断线提示框
GameWorld.isEditor = false
GameWorld.canStartAutoFight = true
GameWorld.playerEnterWorldLog = false

--缓存中转cg生成avatar 同步信息
GameWorld.avatarData = {}

GameWorld.AccountName = "unknow"

--加载指定路径path下的资源
function GameWorld.GetGameObject(path, callback)
    LuaFacade.GetGameObject(path, callback)
end

--加载指定路径path列表下的若干个资源
function GameWorld.GetGameObjects(paths, callback)
    LuaFacade.GetGameObjects(paths, callback)
end

--Destroy资源
function GameWorld.Destroy(gameObject)
    LuaFacade.Destroy(gameObject)
end

--预加载指定路径path列表下的若干个资源，该接口不创建GameObject
function GameWorld.PreloadAsset(paths, callback)
    LuaFacade.PreloadAsset(paths, callback)
end

--预加载LuaPreloadPathsBuilder下设置好的资源，该接口不创建GameObject
function GameWorld.PreloadAssetFromBuilder(callback)
    LuaFacade.PreloadAssetFromBuilder(callback)
end

function GameWorld.PreloadAssetInBackgroundFromBuilder(priority)
    LuaFacade.PreloadAssetInBackgroundFromBuilder(priority)
end

function GameWorld.SetLoadAsync(state)
    LuaFacade.SetLoadAsync(state)
end

--通过后台预加载指定路径path列表下的若干个资源，该接口不创建GameObject
function GameWorld.PreloadAssetInBackground(priority, paths)
    LuaFacade.PreloadAssetInBackground(priority, paths)
end

--清除后台预加载列表内容，通常在切换常见前调用
function GameWorld.ClearPreloadInBackground()
    LuaFacade.ClearPreloadInBackground()
end

--为对应的gameobject捆绑LuaBehaviour某个子类脚本
function GameWorld.AddLuaBehaviour(go, luaClassType)
    return LuaFacade.AddLuaBehaviour(go, luaClassType)
end

--为对应的UIgameobject捆绑LuaBehaviour某个子类脚本
function GameWorld.AddLuaUIComponent(go, componentType, luaClassType)
    return LuaFacade.AddLuaUIComponent(go, componentType, luaClassType)
end

--将hostGo下的srcPath的GameObject复制到parentPath下
function GameWorld.CopyUIGameObject(hostGo, srcPath, parentPath)
    return LuaFacade.CopyUIGameObject(hostGo, srcPath, parentPath)
end

--通过指定的bytes反序列化到整形数组
function GameWorld.BytesDesToIntArray(bytes, start, len)
    return LuaFacade.BytesDesToIntArray(bytes, start, len)
end

--提供loadSceneSetting加载指定场景
function GameWorld.LoadScene(loadSceneSetting, callback)
    LuaFacade.LoadScene(loadSceneSetting, callback)
end

--为对应的UIgameobject捆绑LuaBehaviour某个子类脚本
function GameWorld.CreateSpaceBox(luaClassType, name, position, size, rotation)
    return LuaFacade.CreateSpaceBox(luaClassType, name, position, size, rotation)
end

--为对应的gameobject捆绑Icon
function GameWorld.AddIcon(parent, iconId, colorId, raycastTarget, gray, resetContainer, autoSize, scale)
    return LuaFacade.AddIcon(parent, iconId, colorId or 0, raycastTarget or false, gray or 1, resetContainer or false, autoSize or false, scale or 1)
end

--Http请求
function GameWorld.GetHttpUrl(url, onDone, onFail)
    LuaFacade.GetHttpUrl(url, onDone, onFail)
end

--下载请求
function GameWorld.DownloadString(url, onDone, onFail)
    LuaFacade.DownloadString(url, onDone, onFail)
end

function GameWorld.DownloadStringWithRandomUrl(url, onDone, onFail)
    LuaFacade.DownloadStringWithRandomUrl(url, onDone, onFail)
end

function GameWorld.LoadFile(fileName)
    return LuaFacade.LoadFile(fileName)
end

function GameWorld.DeleteFile(fileName)
    LuaFacade.DeleteFile(fileName)
end

function GameWorld.SaveText(fileName, text)
    LuaFacade.SaveText(fileName, text)
end

function GameWorld.ClearClientAllFile()
    LuaFacade.ClearClientAllFile()
end

function GameWorld.CreateMD5(str)
    return LuaFacade.CreateMD5(str)
end

--读取给定cacheId的string数组数据
--读取和保存数据索引有差异，返回索引从0开始的string[]对象
function GameWorld.ReadCache(cacheId, forCurPlayer)
    return LuaFacade.ReadCache(cacheId, forCurPlayer)
end

--保存给定cacheId的string数组数据
--可以直接传入table建立的string数组，但索引从1开始。
function GameWorld.SaveCache(cacheId, data, forCurPlayer)
    LuaFacade.SaveCache(cacheId, data, forCurPlayer)
end

--获取需要Reload的表格信息，返回类型Dictionary<string, string>
function GameWorld.GetReloadXmlMap()
    return LuaFacade.GetReloadXmlMap()
end

--通知C#层reload xml
function GameWorld.ReloadXml()
    return LuaFacade.ReloadXml()
end

--修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
function GameWorld.SynEntityAttrByte(entityID, attrName, value)
    LuaFacade.SynEntityAttrByte(entityID, attrName, value)
end

--修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
function GameWorld.SynEntityAttrInt(entityID, attrName, value)
    LuaFacade.SynEntityAttrInt(entityID, attrName, value)
end

--修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
function GameWorld.SynEntityAttrFloat(entityID, attrName, value)
    LuaFacade.SynEntityAttrFloat(entityID, attrName, value)
end

--修改实体属性，通过这个接口修改属性，所有监听该属性修改的方法都会响应
function GameWorld.SynEntityAttrString(entityID, attrName, value)
    LuaFacade.SynEntityAttrString(entityID, attrName, value)
end

--修改实体属性，通过这个接口修改属性，
function GameWorld.SynEntityAttrByDrama(entityID, attrName, attrValue)
    LuaFacade.SynEntityAttrByDrama(entityID, attrName, attrValue)
end

--创建一个指定类型的实体，返回对应实体ID
function GameWorld.CreateEntity(entitytype, args)
    return LuaFacade.CreateEntity(entitytype, function(ent)
        for k, v in pairs(args)
        do
            ent[k] = v
            ent.cs[k] = v
        end
    end)
end

--删除指定ID的实体
function GameWorld.DestroyEntity(enitiyid)
    LuaFacade.DestroyEntity(enitiyid)
end

--获取当前实体列表
function GameWorld.Entities()
    return EntityMgr.entities
end

function GameWorld.GetEntity(entID)
    return EntityMgr:GetEntity(entID)
end

function GameWorld.GetEntityByNpcId(npc_id)
    if npc_id and npc_id > 0 then
        return EntityMgr:GetEntityByNpcId(npc_id)
    end
end

function GameWorld.GetEntityByMonsterId(monster_id)
    if monster_id and monster_id > 0 then
        return EntityMgr:GetEntityByMonsterId(monster_id)
    end
end

function GameWorld.GetEntitiesByType(type)
    return EntityMgr:GetEntitiesByType(type)
end

--获取当前player实体
function GameWorld.Player()
    return GameWorld._player
end

--获取当前技能预览实体
function GameWorld.ShowPlayer()
    return GameWorld._showPlayer
end

--获取当前player宠物实体
function GameWorld.PlayerPet()
    return GameWorld._playerPet
end

--加载场景障碍信息
function GameWorld.LoadBlockMap(sceneName)
    LuaFacade.LoadBlockMap(sceneName)
end
--相机切换为锁定目标，位置模式
function GameWorld.ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration)
    LuaFacade.ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration, true)
end
--相机切换为锁定角度，目标，距离模式
function GameWorld.ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
    LuaFacade.ChangeToRotationTargetMotion(target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, true, false)
end
--相机切换为锁定角度，位置模式
function GameWorld.ChangeToRotationPositionMotion(rotation, rotationDuration, rotationAccelerateRate, position, positionDuration)
    LuaFacade.ChangeToRotationPositionMotion(rotation, rotationDuration, rotationAccelerateRate, position, positionDuration, true)
end

--相机切换为锁定角度，目标，距离模式
function GameWorld.ChangeToRotationTargetMotionForLocal(root, target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
    LuaFacade.ChangeToRotationTargetMotionForLocal(root, target, localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
end

-- function GameWorld.UpdateCameraHRotate(elevation, pitchdown, diveH, deltaY)
--     LuaFacade.UpdateCameraHRotate(elevation, pitchdown, diveH, deltaY)
-- end
function GameWorld.RecoverCameraHRotate()
    LuaFacade.RecoverCameraHRotate()
end

function GameWorld.ShakeCamera(id, time)
    LuaFacade.ShakeCamera(id, time)
end

function GameWorld.RecoverCamera(localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
    LuaFacade.RecoverCamera(localEulerAngle, rotationDuration, rotationAccelerateRate, distance, distanceDuration, true, false)
end

function GameWorld.ChangeCameraFOV(fov)
    LuaFacade.ChangeCameraFov(fov)
end
--获取相机角度、位置信息
function GameWorld.GetCameraInfoBackup()
    return LuaFacade.GetCameraInfoBackup()
end

function GameWorld.PauseHandleData(pause)
    LuaFacade.PauseHandleData(pause)
end

--打开阴影、关闭阴影
function GameWorld.OpenShadow(target, orthographicSize)
    LuaFacade.OpenShadow(target, orthographicSize)
end

function GameWorld.CloseShadow()
    LuaFacade.ClosetShadow()
end

function GameWorld.ChangeCameraFarClipPlane(value, duration)
    LuaFacade.ChangeCameraFarClipPlane(value, duration)
end

function GameWorld.ChangeCameraNearClipPlane(value, duration)
    LuaFacade.ChangeCameraNearClipPlane(value, duration)
end

function GameWorld.ChangeFogStartDistance(value, duration)
    LuaFacade.ChangeFogStartDistance(value, duration)
end

function GameWorld.ChangeFogEndDistance(value, duration)
    LuaFacade.ChangeFogEndDistance(value, duration)
end

function GameWorld.ChangeCameraColor(r, g, b)
    LuaFacade.ChangeCameraColor(r, g, b)
end

function GameWorld.BlackCamera(alpha, duration)
    LuaFacade.BlackCamera(alpha, duration)
end

-- function GameWorld.FlyToScene(targetScene)
--     LuaFacade.FlyToScene(targetScene)
-- end

function GameWorld.FaceToNPC(entityID, distance, height)
    LuaFacade.FaceToNPC(entityID, distance, height)
end

function GameWorld.ResetCameraInfo(keepCurrentState)
    LuaFacade.ResetCameraInfo(keepCurrentState)
end

function GameWorld.ResetToNormal(keepCurrentState)
    LuaFacade.ResetToNormal(keepCurrentState)
end

function GameWorld.CSLuaStringToLuaTable(csLuaString, src)
    return LuaFacade.CSLuaStringToLuaTable(csLuaString, src)
end

local _propertyNameCache = {}
function GameWorld.GetPropertyNameById(id)
    if _propertyNameCache[id] == nil then
        _propertyNameCache[id] = LuaFacade.GetPropertyNameById(id)
    end
    return _propertyNameCache[id]
end

function GameWorld.PlaySound(soundId)
    return LuaFacade.PlaySound(soundId)
end

function GameWorld.StopSound(id)
    return LuaFacade.StopSound(id)
end

function GameWorld.ResetAllSound()
    LuaFacade.ResetAllSound()
end

function GameWorld.PlayBgMusic(musicId, fadeOut, fadeIn)
    if musicId == nil then return end
    LuaFacade.PlayBgMusic(musicId, fadeOut == true and true or false, fadeIn == true and true or false)
end

function GameWorld.StopAllBgMusic()
    LuaFacade.StopAllBgMusic()
end

function GameWorld.PlayVideo(id, loop)
    LuaFacade.PlayVideo(id, loop or false)
end

function GameWorld.SetGameQuality(value)
    LuaFacade.SetGameQuality(value)
end

function GameWorld.SetGameQualityDelta(value)
    LuaFacade.SetGameQualityDelta(value)
end

function GameWorld.SetMusicVolume(value)
    LuaFacade.SetMusicVolume(value)
end

function GameWorld.SetSoundVolume(value)
    LuaFacade.SetSoundVolume(value)
end

function GameWorld.SetSpeechVolume(value)
    LuaFacade.SetSpeechVolume(value)
end

-- 测试UI组件
function GameWorld.TestUIComponent(go, value)
    print('GameWorld.TestUIComponent')
    LuaFacade.TestUIComponent(go, value)
end

--过滤文字
function GameWorld.FilterWords(text)
    return LuaFacade.FilterWords(text)
end

function GameWorld.ShowMainCamera()
    LuaFacade.ShowMainCamera()
end

function GameWorld.HideMainCamera()
    LuaFacade.HideMainCamera()
end

function GameWorld.SetCanvasesState(go, state)
    LuaFacade.SetCanvasesState(go, state)
end
--切换相机锁定目标
function GameWorld.SwitchCameraLockTarget(eid)
    LuaFacade.SwitchCameraLockTarget(eid)
end

function GameWorld.SetGlobalRim(flag)
    LuaFacade.SetGlobalRim(flag)
end

function GameWorld.KeepTimeScale(scale, time)
    LuaFacade.KeepTimeScale(scale, time)
end

function GameWorld.SetRadialBlurColorFul(state, darkness)
    LuaFacade.SetRadialBlurColorFul(state, darkness)
end

function GameWorld.PlayScreenWhite(startAlpha, endAlpha, duration)
    LuaFacade.PlayScreenWhite(startAlpha, endAlpha, duration)
end

--玩家位置是否同步设置
function GameWorld.SetSyncPosSpaceFrame(frame)
    LuaFacade.SetSyncPosSpaceFrame(frame)
end

--玩家位置是否同步设置
function GameWorld.SetPlayerSyncPosState(state)
    LuaFacade.SetPlayerSyncPosState(state)
end

--设置当前战斗中是客户端还是服务器
function GameWorld.SetBattleServerMode(state)
    GameWorld.battleServerMode = state
    LuaFacade.SetBattleServerMode(state)
end

--设置当前场景的天平信息
function GameWorld.SetSceneLibra(state)
    LuaFacade.SetSceneLibra(state)
end

function GameWorld.SetRolePreviewCamera(trans, lockPos)
    LuaFacade.SetRolePreviewCamera(trans, lockPos)
end

function GameWorld.SetRolePreviewCameraArgs(distance, rotationX, height, right)
    LuaFacade.SetRolePreviewCameraArgs(distance, rotationX, height, right)
end

function GameWorld.ResetCameraFaceTo(rotationY)
    LuaFacade.ResetCameraFaceTo(rotationY)
end

function GameWorld.SetMapCameraArgs(id)
    LuaFacade.SetMapCameraArgs(id)
end

function GameWorld.SwitchLockTarget()
    LuaFacade.SwitchLockTarget()
end

function GameWorld.ShowWaitingResLoading()
    return LuaFacade.ShowWaitingResLoading()
end

function GameWorld.HideWaitingResLoading()
    return LuaFacade.HideWaitingResLoading()
end

function GameWorld.IsLevelReady(level)
    return LuaFacade.IsLevelReady(level or 200)
end

function GameWorld.GetNecessaryResources(level)
    return LuaFacade.GetNecessaryResources(level)
end

function GameWorld.DownloadNecessaryResources()
    LuaFacade.DownloadNecessaryResources()
end

function GameWorld.PlayFX(path, position, rotation, duration)
    return LuaFacade.PlayFX(path, position, rotation, duration)
end

function GameWorld.StopFX(uid)
    LuaFacade.StopFX(uid)
end

function GameWorld.FindTargetByAI()
    LuaFacade.FindTargetByAI()
end

function GameWorld.GetSceneRootObject(name)
    return LuaFacade.GetSceneRootObject(name)
end

function GameWorld.GetMainCameraGo()
    return LuaFacade.GetMainCameraGo()
end

function GameWorld.GetMainCameraTargetTrans()
    return LuaFacade.GetMainCameraTargetTrans()
end

-- name, vocation, facade, fight_force
function GameWorld.SetAvatarData(data)
    GameWorld.avatarData = data
end

function GameWorld.SetServerId(serverId)
    LuaFacade.SetServerId(serverId)
end

function GameWorld.SetAccount(account)
    LuaFacade.SetAccount(account)
end

function GameWorld.SetAvatarName(avatarName)
    LuaFacade.SetAvatarName(avatarName)
end

function GameWorld.SetSceneId(sceneId)
    LuaFacade.SetSceneId(sceneId)
end

function GameWorld.SetParas(paras)
    LuaFacade.SetParas(paras)
end

function GameWorld.GetParas()
    return LuaFacade.GetParas()
end

--为指定容器追加序列帧动画
function GameWorld.AddIconAnim(parent, iconAnimId, colorId, raycastTarget)
    return LuaFacade.AddIconAnim(parent, iconAnimId, colorId or 0, raycastTarget or false)
end

--为指定容器追加序列帧动画(在指定时间内只播放一次)
function GameWorld.AddIconAnimOnce(parent, iconAnimId, seconds)
    return LuaFacade.AddIconAnimOnce(parent, iconAnimId, seconds)
end

--设置序列帧动画播放状态，true 播放，false 停止
function GameWorld.PlayIconAnim(parent, state)
    return LuaFacade.PlayIconAnim(parent, state)
end

--设置序列帧动画播放速度，单位为每隔多少帧切换一次图片，默认为1，speed为整数，越大播放速度越慢
function GameWorld.SetIconAnimSpeed(parent, speed)
    return LuaFacade.SetIconAnimSpeed(parent, speed)
end

--将主摄像头设置到玩家身上
function GameWorld.SetMainCameraToPlayer()
    LuaFacade.SetMainCameraToPlayer()
end

--将主摄像头从玩家身上移出
function GameWorld.ResetMainCameraFromPlayer()
    LuaFacade.ResetMainCameraFromPlayer()
end

--扩充点击区域
function GameWorld.AddHitArea(parent, width, high, offsetX, offsetY)
    LuaFacade.AddHitArea(parent, width, high, offsetX, offsetY)
end

--mode 0:默认不剔除 1：非全屏panel剔除 2:全屏panel剔除
function GameWorld.SetMainCameraMode(mode)
    LuaFacade.SetMainCameraMode(mode)
end

--设置最大同屏人数
function GameWorld.SetMaxScreenPlayerCount(count)
    LuaFacade.SetMaxScreenPlayerCount(count)
end

--获取最大同屏人数
function GameWorld.GetMaxScreenPlayerCount()
    return LuaFacade.GetMaxScreenPlayerCount()
end

--设置最大同屏怪物
function GameWorld.SetMaxScreenMonsterCount(count)
    LuaFacade.SetMaxScreenMonsterCount(count)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideMonster(mode)
    LuaFacade.SetHideMonster(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherPlayer(mode)
    LuaFacade.SetHideOtherPlayer(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherWing(mode)
    LuaFacade.SetHideOtherWing(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherPet(mode)
    LuaFacade.SetHideOtherPet(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherFabao(mode)
    LuaFacade.SetHideOtherFabao(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherSkillFx(mode)
    LuaFacade.SetHideOtherSkillFx(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideOtherFlow(mode)
    LuaFacade.SetHideOtherFlow(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHideNpc(mode)
    LuaFacade.SetHideNpc(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHidePlayer(mode)
    LuaFacade.SetHidePlayer(mode)
end

--mode 0:显示 1：隐藏
function GameWorld.SetHidePet(mode)
    LuaFacade.SetHidePet(mode)
end

--==============================SDK 登录==============================================
function GameWorld.LoginSDK(loginSuccessFunctionxFunction, loginResultCB)
    LuaFacade.LoginSDK(loginSuccessFunctionxFunction, loginResultCB)
end

function GameWorld.StrToMd5String(strValue)
    return LuaFacade.StrToMd5String(strValue)
end

function GameWorld.OnLoginRspSDK(strValue, loginResultCB)
    LuaFacade.OnLoginRspSDK(strValue, loginResultCB)
end

-- SDK 创角、进入游戏、升级 日志
function GameWorld.ShowSDKLog(functionName, roleId, roleName, roleLevel, vipLevel, serverId, serverName, diamond, guildName, createTime)
    LuaFacade.ShowSDKLog(functionName, roleId, roleName, roleLevel, vipLevel, serverId, serverName, diamond, guildName, createTime)
end


-- 新加的事件日志   GameWorld.LogEvent('每日Boss挑战')
function GameWorld.LogEvent(eventName)
    
    local player = GameWorld.Player()
    if player == nil then
        return
    end
    
    GameWorld.ShowSDKLog("onUpdateRoleEventInfo", tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
        LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), tostring(player.money_coupons),
        player.guild_name, eventName)
end


-- SDK 使用钻石购买物品LOG
-- price 消耗的钻石数量
-- buyAction  充值 , 任务奖励 , 购买道具 , 升级装备 , 钻石抽奖 等类型
-- count 获取 或 购买 的个数 ( 如无个数计算 默认填 1 )
-- itemName // 获得 或 消耗 的物品名称
-- ItemDes // 获得或消耗 详细描述
-- isPayFromCharge  钻石 trun  绑钻 false
-- isGain  获得 trun  消耗  false
function GameWorld.onBuyItemLog(price, buyAction, count, itemName, ItemDes, isPayFromCharge, isGain, leftMoney)
    
    LoggerHelper.Log("----------onBuyItemLog,price:" .. price)
    
    local player = GameWorld.Player()
    LuaFacade.onBuyItemLog("buyItemLog", tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
        LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), tostring(leftMoney),
        player.guild_name, "-1", price, buyAction, count, itemName, ItemDes, isPayFromCharge, isGain)
end

-- SDK 充值
-- shopMoney商品金额(单位:分)
-- shopCount购买数量 ( 如 : 100元宝 填 100 )
-- shopName商品名称 ( 如 : XX元宝 )
-- shopType商品类型(如: 钻石, 礼包)
-- shopId商品ID(需要传递对应的ID)
-- shopDes商品描述
-- shopRate兑换比率 默认 填 10
-- shopCallbackUrl游戏方的支付回调 地址
function GameWorld.BuyMarkt(shopMoney, shopCount, shopName, shopType, shopId, shopDes, shopRate, shopCallbackUrl)
    local orderId = "pay" .. GameUtil.DateTimeUtil.GetServerTime()
    local player = GameWorld.Player()
    LuaFacade.BuyMarkt(orderId, tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
        LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), shopMoney, shopCount, shopName, shopType, shopId, shopDes, shopRate, shopCallbackUrl)
end

function GameWorld.SetSwitchAccountCB()
    LuaFacade.SetSwitchAccountCB(function()GameWorld.LoginOutToLoginPanel() end)
end

function GameWorld.LoginOutToLoginPanel()
    -- -- 临时屏蔽返回登录
    -- local data = {}
    -- local value = {["confirmCB"] = function()
    --     UnityEngine.Application.Quit()
    --     GameWorld.aiCanThink = true
    --     GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
    -- end,
    -- ["cancelCB"] = function()
    --     UnityEngine.Application.Quit()
    --     GameWorld.aiCanThink = true
    --     GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
    -- end,
    -- ["text"] = "请重启客户端重新登录。\n(点击“确定”或“取消”退出游戏)"}
    -- GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    -- LoggerHelper.Log("GameManager.GameStateManager.ReturnLogin")
    GameWorld.playerEnterWorldLog = false
    GameManager.GameStateManager.ReturnLogin(true)
end

function GameWorld.LoginOutSDK()
    LuaFacade.LoginOutSDK()
end

function GameWorld.GetSDKInitState()
    return LuaFacade.GetSDKInitState()
end

function GameWorld.GetChannelId()
    return LuaFacade.GetChannelId()
end

function GameWorld.GetGameId()
    return LuaFacade.GetGameId()
end

function GameWorld.GetAppId()
    return LuaFacade.GetAppId()
end

function GameWorld.GetUserId()
    return LuaFacade.GetUserId()
end

function GameWorld.GetIPAddress()
    return LuaFacade.GetIPAddress()
end

function GameWorld.OnServerCloseLogin(jsonStr)
    return LuaFacade.OnServerCloseLogin(jsonStr)
end

-- 日志接口
function GameWorld.UploadInfo()
    GameMain.LuaFacade.UploadInfo()
end

function GameWorld.UploadLog(data)
    GameMain.LuaFacade.UploadLog(data)
end

function GameWorld.GetAutoQualityLevel()
    return LuaFacade.GetAutoQualityLevel()
end


function GameWorld.LoadCG1v1()
    LuaFacade.LoadCG1v1()
end

function GameWorld.CSGM(cmd)
    LuaFacade.CSGM(cmd)
end

function GameWorld.GetTodayLogText()
    LuaFacade.GetTodayLogText()
end

function GameWorld.GetCurrPage()
    return LuaFacade.GetCurrPage()
end

function GameWorld.GetTotalPage()
    return LuaFacade.GetTotalPage()
end

function GameWorld.LastPage()
    LuaFacade.LastPage()
end

function GameWorld.NextPage()
    LuaFacade.NextPage()
end

function GameWorld.GetCurrPageContent()
    return LuaFacade.GetCurrPageContent()
end

function GameWorld.ClearLog()
    LuaFacade.ClearLog()
end

--同步时间
function GameWorld.SyncTime()
    LuaFacade.SyncTime()
end

function GameWorld.PauseAllSynPos(state)
    LuaFacade.PauseAllSynPos(state)
end


function GameWorld.WriteInfoLog(msg)
    LuaFacade.WriteInfoLog(msg)
end

function GameWorld.SetPauseFps(key, isPause)
    LuaFacade.SetPauseFps(key, isPause)
end

function GameWorld.InitPauseFps(keys, isPause)
    LuaFacade.InitPauseFps(keys, isPause)
end

function GameWorld.GetFps()
    return LuaFacade.GetFps()
end

function GameWorld.GetAverageFps()
    return LuaFacade.GetAverageFps()
end

function GameWorld.GetLowFps()
    return LuaFacade.GetLowFps()
end

function GameWorld.SetLowFps(flag)
    LuaFacade.SetLowFps(flag)
end


function GameWorld.GetCutout()
    return LuaFacade.GetCutout()
end

--light,亮度，0-255,255为最亮
function GameWorld.SetScreenLight(light)
    LuaFacade.SetScreenLight(light)
end

function GameWorld.GetTargetFrameRate()
    return LuaFacade.GetTargetFrameRate()
end

function GameWorld.SetTargetFrameRate(state)
    LuaFacade.SetTargetFrameRate(state)
end

function GameWorld.SetDamageHandlerRunning(state)
    LuaFacade.SetDamageHandlerRunning(state)
end

function GameWorld.SetRealTimeShadow(mode)
    LuaFacade.SetRealTimeShadow(mode)
end

