
GameEvents = {}
local mt={		   
			__newindex = function(t, k, v)
			end
			,
			__index = function(t, k)
				return k
			end
			}
setmetatable(GameEvents, mt)

WaitEvents = {}
local wt={		   
			__newindex = function(t, k, v)
			end
			,
			__index = function(t, k)
				return k
			end
			}
setmetatable(WaitEvents, wt)


EventDispatcher = {}

function EventDispatcher:Init()
	self._listenerList = {}
end

function EventDispatcher:AddEventListener( eventType, fun )
	if (self._listenerList[eventType] == nil) then
		self._listenerList[eventType] = {}
	end
	local t = self._listenerList[eventType];
	for i,ofun in pairs(t) do
		if (ofun == fun) then
			return
		end
	end
	table.insert(t, fun);
end

function EventDispatcher:RemoveEventListener( eventType, fun )
	if (self._listenerList[eventType] == nil) then
		return
	end
	local t = self._listenerList[eventType];
	for i,ofun in pairs(t) do
		if (ofun == fun) then
			table.remove(t, i);
			break;
		end
	end
end

function EventDispatcher:TriggerEvent(eventType, ...)
	if (self._listenerList[eventType] == nil) then
		return
	end
	local t = self._listenerList[eventType];
	for i,ofun in pairs(t) do
		ofun(...);
	end
end

EventDispatcher:Init()
