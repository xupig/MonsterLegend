require "Core.GameWorld"
require "GameDataHelper.GameDataHelper"
require "GameManager.GameManager"
require "GameConfig.GameConfig"
require "GameUtil.GameUtil"
require "PlayerManager.PlayerManager"
require "CSFacade"
require "AI/bt_base_nodes"
require "AI/bt_action_nodes"
require "AI/bt_condition_nodes"

local Application = UnityEngine.Application
local OperationLogManager = GameManager.OperationLogManager

--主入口函数。从这里开始lua逻辑
function Main()
    LoggerHelper.Log("主入口函数Main()。从这里开始lua逻辑");
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_14)
    Init()
    Start()
end

function Init()
    GameWorld.LoggerHelper.Init()
    GameConfig.LocalSetting:Init()
    GameManager.XMLManager.CheckReloadXML()
    GameManager.GUIManager.Init()
    GameManager.ModuleManager.Init()
    GameManager.LoginManager:Init()
    GameManager.CreateRoleManager:Init()
    GameManager.DramaManager:Init()
    GameManager.LoadingBarManager:Init()
    RegisterEntities()
--GameManager.ModuleManager.TestF = function() end
--LoggerHelper.Log("Init1:::",GameManager.XMLManager.map[1])
--LoggerHelper.Log("Init2:::",GameManager.XMLManager.spell[21].group)
end

function RegisterEntities()
    local LuaFacade = GameMain.LuaFacade
    LuaFacade.RegisterEntityType("Avatar", "EntityCreature")
    LuaFacade.RegisterEntityType("PlayerAccount", "EntityAccount")
    LuaFacade.RegisterEntityType("PlayerAvatar", "EntityPlayer")
    LuaFacade.RegisterEntityType("Monster", "EntityCreature")
    LuaFacade.RegisterEntityType("CreateRoleAvatar", "EntityCreature")
    LuaFacade.RegisterEntityType("Collect", "EntityCreature")
    LuaFacade.RegisterEntityType("Dummy", "EntityCreature")
    LuaFacade.RegisterEntityType("NPC", "EntityCreature")
    LuaFacade.RegisterEntityType("Trap", "EntityCreature")
    LuaFacade.RegisterEntityType("ThrowObject", "EntityCreature")
    LuaFacade.RegisterEntityType("SpaceDrop", "EntityStatic")
    LuaFacade.RegisterEntityType("SkillShowAvatar", "EntityCreature")
    LuaFacade.RegisterEntityType("ClientCollectPoint", "EntityCreature")
    LuaFacade.RegisterEntityType("StaticShowAvatar", "EntityCreature")
    LuaFacade.RegisterEntityType("StaticMonster", "EntityCreature")
    LuaFacade.RegisterEntityType("Pet", "EntityCreature")
    LuaFacade.RegisterEntityType("DropItem", "EntityDropItems")
    LuaFacade.RegisterEntityType("Fabao", "EntityCreature")
    LuaFacade.RegisterEntityType("Peri", "EntityCreature")
    LuaFacade.RegisterEntityType("ClickFlag", "EntityCreature")
    LuaFacade.RegisterEntityType("BossTomb", "EntityStatic")
    LuaFacade.RegisterEntityType("MonsterDestroyArea", "EntityStatic")
    LuaFacade.RegisterEntityType("UIShowEntity", "EntityCreature")
    LuaFacade.RegisterEntityType("ServerSpaceDrop", "EntityCreature")

    LuaFacade.AddEntityTypePool("Monster", "monster_id")
end

function PreloadXML()
	local xml = nil
    xml = XMLManager.spell[0]
	xml = XMLManager.attri_config[0]

    xml = XMLManager.item_com[0]
    xml = XMLManager.compose_item[0]
    xml = XMLManager.function_open[0]
    xml = XMLManager.panel_open[0]
    xml = XMLManager.promis_display[0]
    xml = XMLManager.special_dialog[0]
    xml = XMLManager.bubble_dialog[0]
    xml = XMLManager.achievement[0]
    xml = XMLManager.achieve_type[0]
    xml = XMLManager.offline_pvp_reward_1[0]
    xml = XMLManager.card_group[0]
    xml = XMLManager.card_group_level[0]
    xml = XMLManager.card[0]
    xml = XMLManager.mythical_animals[0]
    xml = XMLManager.skill_talent_lv[0]
    xml = XMLManager.skill_talent[0]

    xml = XMLManager.task[0]
    xml = XMLManager.fashion[0]
    xml = XMLManager.welfare_7day_reward[0]
    xml = XMLManager.cumulative_daily_charge[0]
    xml = XMLManager.lord_ring_detail[0]
    xml = XMLManager.marry_childbase[0]
    xml = XMLManager.welfare_notice_reward[0]

    xml = XMLManager.welfare_level_up[0]
    xml = XMLManager.online_reward[0]
    xml = XMLManager.spend_day_reward[0]
    xml = XMLManager.activity_appearance[0]
    xml = XMLManager.activity_daily[0]
    xml = XMLManager.activity_point_reward[0]
    xml = XMLManager.skill_active[0]
    xml = XMLManager.pray[0]
    xml = XMLManager.event_activity[0]

    xml = XMLManager.pet[0]
    xml = XMLManager.world_map[0]
    xml = XMLManager.regional_map[0]
    xml = XMLManager.monster[0]
    xml = XMLManager.role_experience[0]
    xml = XMLManager.font_style[0]
    xml = XMLManager.chinese_voice[0]
    xml = XMLManager.item_equipment[0]
    xml = XMLManager.event_conds[0]

    xml = XMLManager.system_info[0]
    xml = XMLManager.equip_value[0]
    xml = XMLManager.horse[0]
    xml = XMLManager.pet_treasure_skill_ai[0]
    xml = XMLManager.ui_model_view[0]
    xml = XMLManager.equip_score[0]
    xml = XMLManager.rune_config[0]
    xml = XMLManager.rune[0]
    xml = XMLManager.treasure_equip_show[0]

    xml = XMLManager.item_gem[0]
    xml = XMLManager.title[0]
    xml = XMLManager.buff[0]
    xml = XMLManager.lord_ring_total[0]
    xml = XMLManager.equip_strength[0]
    xml = XMLManager.guild_skill[0]
    xml = XMLManager.guild_level[0]
    xml = XMLManager.word_collection[0]
    xml = XMLManager.flash_sale[0]
    xml = XMLManager.special_remind[0]
    xml = XMLManager.circle_task[0]
    xml = XMLManager.change_vocation[0]
    xml = XMLManager.aion[0]
    xml = XMLManager.exp_instance[0]
    xml = XMLManager.add_rate[0]   
    
end

function Start()
    PreloadXML()

    GameWorld.isEditor = Application.isEditor
    GameWorld.platform = tostring(Application.platform)
    GameWorld.PlatformIPhonePlayer = "IPhonePlayer"
    GameWorld.WriteInfoLog("GameWorld.platform: " .. GameWorld.platform)
    if GameWorld.isEditor then
        GameManager.KeyboardManager:Init()
    end
    if GameLoader.SystemConfig.GetValueInCfg('SpecialExamine') == "1" then
        GameWorld.SpecialExamine = true
    end
    
    local functionClose = GameLoader.SystemConfig.GetValueInCfg('FunctionClose')
    if functionClose ~= "" then
        local list = string.split(functionClose, ",")
        GameWorld.FunctionCloseIds = {}
        if list then
            for i, v in ipairs(list) do
                GameWorld.FunctionCloseIds[v] = v
            end
        end
    end
    
    local panelClose = GameLoader.SystemConfig.GetValueInCfg('PanelClose')
    if panelClose ~= "" then
        local list = string.split(panelClose, ",")
        GameWorld.PanelCloseIds = {}
        if list then
            for i, v in ipairs(list) do
                GameWorld.PanelCloseIds[v] = v
            end
        end
    end
    
    GameManager.GameSceneManager:EnterSceneByID(1001, false, nil, function()
            -- CreateRoleManager:CreateRttCamera()
            GameManager.GameStateManager.SetState(GAME_STATE.LOGIN)
    end)
    
    local SystemSettingManager = PlayerManager.SystemSettingManager
    if GameWorld.SpecialExamine then
        EventDispatcher:TriggerEvent(GameEvents.OnMusicChanged, 0)
        EventDispatcher:TriggerEvent(GameEvents.OnSoundChanged, 0)
    end
    RequireBehaviorTree()
end

function RequireBehaviorTree()
    
    
    --[[
    require "AI/bt_base_nodes"
    require "AI/bt_action_nodes"
    require "AI/bt_condition_nodes"
    
    setmetatable(GameWorld.btTable, {
    __index = function(t, k)
    local v = rawget(t, k)
    if v == nil then
    v = require("BT/bt".. tonumber(k))
    t[k] = v
    end
    return v
    end,
    })
    
    --]]
    local all_bt = require("BT/all_bt")
    --local bt
    for k, v in pairs(all_bt) do
        --print("RequireBehaviorTree v=",v)
        local str = "BT/bt" .. v
        local bt = require("BT/bt" .. v)
        --print("RequireBehaviorTree v=",v,"ai_id=",str,"bt=",tostring(bt))
        GameWorld.btTable[v] = bt
    end

    LoggerHelper.Log("_________________________________________________________");

end

function Test()
    local testCall = GameMain.LuaFacade.TestLuaCall
    local t = UnityEngine.Time.realtimeSinceStartup
    local TestF = GameManager.ModuleManager.TestF
    --LoggerHelper.Error("LUA1:".. UnityEngine.Time.realtimeSinceStartup)
    for i = 0, 100000, 1 do
        testCall()
    end
    LoggerHelper.Error("lua call c#:" .. tostring(UnityEngine.Time.realtimeSinceStartup - t))
    t = UnityEngine.Time.realtimeSinceStartup
    for i = 0, 100000, 1 do
        TestF()
    end
    LoggerHelper.Error("lua call lua:" .. tostring(UnityEngine.Time.realtimeSinceStartup - t))
end

function TestLuaCall()

end

Main()
