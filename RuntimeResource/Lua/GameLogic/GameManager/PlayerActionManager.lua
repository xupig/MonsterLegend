-- 场景管理器，用于切换玩家场景
PlayerActionManager = {};

local PlayerActionConfig = GameConfig.PlayerActionConfig

-- 记录当前行爲
local _currentAction = nil

-- 记录默認行爲
local _defaultAction = nil

-- 记录下一个行爲
local _nextAction = nil

-- 等待行爲队列
local _waitingQueue = {}

local _updateFunc = nil

-- 获取动作序列的最高优先级别
local function GetActionQueuePriority( queue )
	local max = PlayerActionConfig.PRIORITY_NONE
	for k,action in pairs(queue) do
		max = math.max(max, action.priority)
	end
	return max
end

function PlayerActionManager:Init()    --单例初始化方法
	
end

function PlayerActionManager:OnPlayerEnterWorld()
	_updateFunc = function() self:Update() end
	GameWorld.Update:Add(_updateFunc)
end

function PlayerActionManager:OnPlayerLeaveWorld()
	GameWorld.Update:Remove(_updateFunc)
	_updateFunc = nil
end

function PlayerActionManager:Update()
	if (GameWorld.Player() == nil) then return end

	if _currentAction == nil then
		_currentAction = self:GetNextAction()
		if _currentAction ~= nil then
			-- LoggerHelper.Error("Sam :_currentAction11" .. tostring(_currentAction._numTT) , true)
		end
	else
		-- LoggerHelper.Error("Sam :_currentAction22222" .. tostring(_currentAction._numTT) , true)
	end

	if _currentAction ~= nil then
		-- LoggerHelper.Error("Sam :_currentAction3333333" .. tostring(_currentAction._numTT) , true)
		local result = _currentAction:DoAction()
		-- LoggerHelper.Error("Sam : result" .. tostring(result) , true)
		if result == PlayerActionConfig.ACTION_DOING then
			return
		elseif result == PlayerActionConfig.ACTION_FAIL then
			self:CleanWaitingQueue()
            _currentAction = nil
        elseif result == PlayerActionConfig.ACTION_END then
        	_currentAction = nil
        else
        	LoggerHelper.Log("不应该到这里", result, _currentAction)
        end
    else
    	_currentAction = _defaultAction
	end
end

-- 添加动作队列
function PlayerActionManager:AddActionQueue( queue )
	-- 跟当前动作相比，如果新的动作队列优先级较高，则顶掉当前队列
	if _currentAction ~= nil then
		local newQueuePriority = GetActionQueuePriority(queue)
		local currentPriority 	= _currentAction.priority
		if currentPriority < newQueuePriority then
			-- LoggerHelper.Error("Sam :打断222222 ====" .. tostring(_currentAction._source), true)
			_currentAction:StopAction()
			_currentAction = nil
		-- 如果优先级别相同，而当前动作可以被打断，则打断
		elseif currentPriority == newQueuePriority and _currentAction.canBreak then
			--LoggerHelper.Log("打断当前动作..........")
			-- LoggerHelper.Error("Sam :打断11111 ====" .. tostring(_currentAction._source), true)
			_currentAction:StopAction()
			_currentAction = nil
		end
	end
	-- 添加进入等待队列
	self:SetWaitingQueue(queue)
end


-- 设置等待队列
function PlayerActionManager:SetWaitingQueue( queue )
	_waitingQueue = queue
	-- LoggerHelper.Error("Sam :设置队列 ====", true)
end

-- 清除队列
function PlayerActionManager:CleanWaitingQueue()
	_waitingQueue = {}
end

-- 获取下一个动作
function PlayerActionManager:GetNextAction()
	if _waitingQueue ~= nil and #_waitingQueue ~= 0 then
		local ret = table.remove(_waitingQueue, 1)
		return ret;
	end
	return nil
end

-- 停止全部动作
function PlayerActionManager:StopAllAction()
	self:StopCurrAction()
	_waitingQueue  = {}
end

function PlayerActionManager:StopAllActionExcept(actionType)
	if _currentAction ~= nil and _currentAction._classType ~= actionType then
		self:StopAllAction()
	end
end

function PlayerActionManager:StopFindPosition()
	--LoggerHelper.Error("PlayerActionManager:StopFindPosition() ==" .. tostring(_currentAction == nil) )
	local classType = _currentAction and _currentAction._classType or nil
	if _currentAction ~= nil and (classType ==  ClassTypes.ActionFindCollectClientResource or classType ==  ClassTypes.ActionFindCollectItem or classType ==  ClassTypes.ActionFindKillMonsterPoint or classType ==  ClassTypes.ActionFindNPC or classType ==  ClassTypes.ActionFindPosition)  then
		self:StopCurrAction()
	end	
end

-- 停止当前动作
function PlayerActionManager:StopCurrAction()
	--LoggerHelper.Error("PlayerActionManager:StopCurrAction() ==" .. tostring(_currentAction == nil) )
	if _currentAction ~= nil then
		-- LoggerHelper.Error("真正停止了" .. tostring(_currentAction._numTT))
		_currentAction:StopAction()
	end
	_currentAction = nil;
end

function PlayerActionManager:IsDoingAction()
	-- LoggerHelper.Error("_currentAction" .. tostring(_currentAction == nil))
	return _currentAction ~= nil and _currentAction.actionType ~= "skill"
end

PlayerActionManager:Init() 
return PlayerActionManager