-- ModelShowManager.lua
-- UI界面模型展示管理器
local Camera = UnityEngine.Camera
local CameraClearFlags = UnityEngine.CameraClearFlags
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath
local LightType = UnityEngine.LightType
local LayerMask = UnityEngine.LayerMask
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
-- local LuaUIRawImage = GameMain.LuaUIRawImage

local ModelShowManager = {}

local Original_Pos = Vector3(-1500, 0, -1500)
local Tmp_Pos = Vector3(-1500, 0, -1500)

function ModelShowManager:Init()
    self:CreateRttCamera()
end

function ModelShowManager:SetModel(model)
    self._modelComponent:ClearModelGameObject()
    self._modelComponent:LoadModel(model)
end

-- function ModelShowManager:SetRawImage(renderTexture)
--     self.rawImage.texture = renderTexture
-- end

-- function ModelShowManager:InitRawImage(go, needAdjust)
--     self.rawImage = go:AddComponent(typeof(LuaUIRawImage))
--     self.rawImage.texture = self.RenderTexture
--     if needAdjust then
--         self.rawImage:AdjustSize()
--     end
-- end

function ModelShowManager:SetShowConfig(relativePos, localEulerAngles)
    Tmp_Pos = Original_Pos + relativePos
    self._modelComponent:SetStartSetting(Tmp_Pos, localEulerAngles, 1)
end

function ModelShowManager:SetModelGo(transform)
    --self.rawImage.RoleTransform = transform
end

function ModelShowManager:CreateRttCamera()
    self.modelShowContainer = GameObject.New("ModelShowContainer")
    self.rttForModel = GameObject.New("RttForModel")
    self.rttForModel.transform:SetParent(self.modelShowContainer.transform)
    GameObject.DontDestroyOnLoad(self.modelShowContainer)
    self.RenderCamera = self.rttForModel:AddComponent(typeof(Camera))
    -- self.RenderCamera.renderingPath = RenderingPath.DeferredLighting --.DeferredShading --Forward--
	self.RenderTexture = RenderTexture.New(1280, 720, 1)
	self.RenderTexture.name = "ModelShowTexture"
    self.RenderCamera.targetTexture = self.RenderTexture
    self.RenderCamera.clearFlags = CameraClearFlags.SolidColor
    self.RenderCamera.backgroundColor = UnityEngine.Color.New(46 / 255, 64 / 255, 83 / 255, 0 / 255)
    self.RenderCamera.fieldOfView = 20
    self.RenderCamera.cullingMask = math.pow(2, LayerMask.NameToLayer("UI"))
    self.RenderCamera.transform.localPosition = Vector3.New(-1500, 0, -1500)
    self.RenderCamera.transform.localEulerAngles = Vector3.New(0, 0, 0)
    
    self._modelComponent = self.modelShowContainer:AddComponent(typeof(ActorModelComponent))
    self._modelComponent:SetDragable(false)
    self._modelComponent:SetStartSetting(Vector3(-1500, 0, -1500), Vector3(0, 0, 0), 1)
end

-- function ModelShowManager:GetRawImage()
--     return self.RenderTexture
-- end

function ModelShowManager:PlayFx(path, duration)
    -- if self.rawImage.RoleTransform ~= nil then
        local fullPath = "Assets/Resources/"..path
        local pos = self.rawImage.RoleTransform.position
        local face = Vector3.zero
        local uid = GameWorld.PlayFX(fullPath, pos, face, duration)
    -- end
end

ModelShowManager:Init()

return ModelShowManager
