--ServerListManager.lua
ServerListManager = {}

local XMLParser = GameWorld.XMLParser
local ConstString = GameConfig.ConstString
local LocalServerConfig = GameConfig.LocalServerConfig
local bit = bit
local ServerStateType = GameConfig.EnumType.ServerStateType
local LoginServerDataStorage = GameConfig.LoginServerDataStorage
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
--PrintTable = require "Core/PrintTable"
local loginLastServerDataList = nil
local loginLastServerNameList = {}
local Servers = {}
local ServerGroups = {}

local Pos_To_Icon = {[ServerStateType.Close] = 67, [ServerStateType.Fluency] = 68, [ServerStateType.Crowd] = 69, [ServerStateType.Hot] = 70}

local Self_Server_Index = -2
local Recommend_Server_Index = -1

local SelectServer = {
    serverId = 0,
    serverName = "",
    serverIp = "",
    serverPort = 0,
    serverFlag = 0,
    detail = "",
    serverGroup = 0,
}

function ServerListManager:Init()
    self.serverFilePath = UnityEngine.Application.persistentDataPath .. ConstString.RutimeResourceConfig .. "/servers.xml" --GameWorld.GetRuntimeResPath().."servers.xml"
    self.serverGroupFilePath = UnityEngine.Application.persistentDataPath .. ConstString.RutimeResourceConfig .. "/servergrouplist.xml"
    
    self:InitEvent()
    self:InitLastSetting()
    EventDispatcher:AddEventListener("GetServerList", function()self:GetServerList() end)
    EventDispatcher:AddEventListener("GetServerGroupList", function()self:GetServerGroupList() end)
end

function ServerListManager:InitLastSetting()
    self._serverId = LocalSetting.settingData.SelectedServerID
end

function ServerListManager:AddServer(tmpServer)
    table.insert(Servers, tmpServer)
end

--把名为name的服务器设置为当前服务器
function ServerListManager:SetSelectServer(name)
    if #Servers == 0 then
        LoggerHelper.Log("Servers len is 0")
    end
    for k, v in pairs(Servers) do
        if v.serverName == name then
            LoggerHelper.Log("currentServerName:" .. name)
            SelectServer = v
            break
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.OnSetSelectServer)
end

--设置为当前服务器
function ServerListManager:SetSelectServerId(id)
    if #Servers == 0 then
        LoggerHelper.Log("Servers len is 0")
    end
    for k, v in pairs(Servers) do
        if v.serverId == id then
            LoggerHelper.Log("currentServerId:" .. id)
            SelectServer = v
            break
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.OnSetSelectServer)
    -- LoggerHelper.Error(SelectServer.serverIp.. "GameLoader.SystemConfig.UploadYunYingLogServer" .. GameLoader.SystemConfig.UploadYunYingLogServer)
    GameLoader.SystemConfig.UploadYunYingLogServer = SelectServer.serverIp
-- LoggerHelper.Error(SelectServer.serverIp .. "GameLoader.SystemConfig.UploadYunYingLogServer" .. GameLoader.SystemConfig.UploadYunYingLogServer)
end

local ServerIdToNameCache = {}
function ServerListManager:GetServerNameById(serverId)--serverId为string类型
    if #Servers == 0 then
        LoggerHelper.Log("Servers len is 0")
        return serverId
    end
    
    if ServerIdToNameCache[serverId] then
        return ServerIdToNameCache[serverId]
    end
    
    for k, v in pairs(Servers) do
        if v.serverId == serverId then
            ServerIdToNameCache[serverId] = v.serverName
            return v.serverName
        end
    end
    return serverId
end

function ServerListManager:SelectedServerId()
    return SelectServer.serverId
end

function ServerListManager:SelectedServerIp()
    return SelectServer.serverIp
end

function ServerListManager:SelectedServerPort()
    return SelectServer.serverPort
end

function ServerListManager:SelectedServerName()
    return SelectServer.serverName
end

function ServerListManager:SelectedServerFlag()
    return SelectServer.serverFlag
end

function ServerListManager:SelectedDetail()
    return SelectServer.detail
end

function ServerListManager:SelectedCloseState()
    local state = ServerListManager:GetServerStateByFlag(SelectServer.serverFlag)
    return state == ServerStateType.Close
end

local bitPos = {ServerStateType.Close, ServerStateType.Fluency, ServerStateType.Crowd, ServerStateType.Hot}
--返回服务器拥堵状态
function ServerListManager:GetServerState(serverId)
    local serverData = self:GetServerDataById(serverId)
    if serverData ~= nil then
        for i = 1, #bitPos do
            local value = bitPos[i]
            local state = (bit.band(serverData.serverFlag, value) ~= 0)
            if state then
                return value
            end
        end
    end
    return ServerStateType.Fluency
end

function ServerListManager:GetServerNew(serverId)
    local serverData = self:GetServerDataById(serverId)
    if serverData ~= nil then
        local state = (bit.band(serverData.serverFlag, ServerStateType.New) ~= 0)
        return state
    end
    return false
end

function ServerListManager:GetServerRecommend(serverId)
    local serverData = self:GetServerDataById(serverId)
    if serverData ~= nil then
        local state = (bit.band(serverData.serverFlag, ServerStateType.Recommend) ~= 0)
        return state
    end
    return false
end

function ServerListManager:GetServerStateByFlag(flag)
    for i = 1, #bitPos do
        local value = bitPos[i]
        local state = (bit.band(flag, value) ~= 0)
        if state then
            return value
        end
    end
    return ServerStateType.Fluency
end

function ServerListManager:GetServerNewByFlag(flag)
    local state = (bit.band(flag, ServerStateType.New) ~= 0)
    return state
end

function ServerListManager:GetServerRecommendByFlag(flag)
    local state = (bit.band(flag, ServerStateType.Recommend) ~= 0)
    return state
end

function ServerListManager:GetServerStateIcon(flag)
    local state = ServerListManager:GetServerStateByFlag(flag)
    return Pos_To_Icon[state]
end

------------------------------------------------------------------------------
function ServerListManager:ServerListFromXml(ServersXmlData)
    Servers = {}
    local serverListId = {}
    for k, v in pairs(ServersXmlData.root:children()) do
        local tmpServer = {}
        for k2, v2 in pairs(v:children()) do
            local v2name = v2:name()
            if v2name == "nn" then
                tmpServer.serverName = v2:value()
            elseif v2name == "id" then
                table.insert(serverListId, tonumber(v2:value()))
                tmpServer.serverId = v2:value()
            elseif v2name == "ip" then
                tmpServer.serverIp = v2:value()
            elseif v2name == "port" then
                tmpServer.serverPort = v2:value()
            elseif v2name == "flag" then
                tmpServer.serverFlag = v2:value()
            elseif v2name == "detail" then
                tmpServer.detail = v2:value()
            elseif v2name == "group" then
                tmpServer.serverGroup = tonumber(v2:value())
            end
        end
        --LoggerHelper.Log("tmpServer.serverName:"..tmpServer.serverName)
        --LoggerHelper.Log("tmpServer.serverIp:"..tmpServer.serverIp)
        ServerListManager:AddServer(tmpServer)
        if tmpServer.serverId == self._serverId then
            SelectServer = tmpServer
        end
    end
    
    self:InitCurServerData()
    
    return serverListId
end

-- 初化当前服务器数据：如果本地没有记录 则选推荐服务器里的第一个
function ServerListManager:InitCurServerData()
    local candiServer = nil
    for i, tmpServer in ipairs(Servers) do
        if tmpServer.serverId == self._serverId then
            candiServer = tmpServer
            break
        end
    end
    if candiServer == nil then
        for i, tmpServer in ipairs(Servers) do
            if self:GetServerRecommendByFlag(tmpServer.serverFlag) then
                candiServer = tmpServer
                break
            end
        end
    end
    if candiServer == nil then
        for i, tmpServer in ipairs(Servers) do
            if self:GetServerNewByFlag(tmpServer.serverFlag) then
                candiServer = tmpServer
                break
            end
        end
    end
    if candiServer == nil then
        for i, tmpServer in ipairs(Servers) do
            if self:GetServerStateByFlag(tmpServer.serverFlag) == ServerStateType.Fluency then
                candiServer = tmpServer
                break
            end
        end
    end
    if candiServer == nil then
        candiServer = Servers[1]
    end
    self._serverId = candiServer.serverId
    SelectServer = candiServer
end

function ServerListManager:GetServerList()
    LoggerHelper.Log("server.xml***************************: " .. self.serverFilePath)
    local xmlText = GameWorld.LoadFile(self.serverFilePath)
    local serverListId
    if xmlText == nil then
        LoggerHelper.Log("file not exist,download from web")
        self:GetServerListFromWeb()
    else
        LoggerHelper.Log("server file exist")
        LoggerHelper.Log("servers xml text"..xmlText)
        serverListId = self:GetServerListFromLocal(xmlText)
        if SelectServer.serverName ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.OnSetSelectServer)
        end
    end
    return serverListId
end

function ServerListManager:GetServerDataById(id)
    for k, v in pairs(Servers) do
        if v.serverId == id then
            return v
        end
    end
    return nil
end

----------------------------------------------------------------------------------------------
function ServerListManager:GetServerListFromLocal(xmlText)
    local ServersXmlData = XMLParser:ParseXmlText(xmlText)
    LoggerHelper.Log("ServersXmlData:", ServersXmlData)
    local serverListId = ServerListManager:ServerListFromXml(ServersXmlData)
    LoggerHelper.Log("serverListId:", serverListId)
    return serverListId
end

function ServerListManager:GetServerListFromWeb()
    local url = GameLoader.SystemConfig.ServerUrl --GameWorld.GetServerListUrl()
    local onDone = function(result)ServerListManager:GetServerListDone(result) end
    local onFail = function(result)EventDispatcher:TriggerEvent("GetServerList") end
    GameWorld.DownloadString(url, onDone, onFail)
end

function ServerListManager:GetServerListDone(content)
    if type(content) ~= "string" then
        return
    end
    GameWorld.SaveText(self.serverFilePath, content)
    EventDispatcher:TriggerEvent("GetServerList")
    EventDispatcher:TriggerEvent(GameEvents.ServerListDataReady)
end

----------------------------------------------------------------------------------------------
local function SortByKey(a, b)
    return tonumber(a.sort) < tonumber(b.sort)
end

function ServerListManager:AddServerGroup(serverGruop)
    if self:ServerGroupIsUse(serverGruop.groupId) then
        table.insert(ServerGroups, serverGruop)
    end
end

function ServerListManager:SortServerGroup()
    table.sort(ServerGroups, SortByKey)
end

local groupListDataCache
function ServerListManager:GetServerListByGroup(groupId)
    if groupListDataCache == nil or groupListDataCache[Recommend_Server_Index] == nil then
        self:RefrshGroupListDataCache()
    end
    return groupListDataCache[groupId] or {}
end

function ServerListManager:RefrshGroupListDataCache()
    groupListDataCache = {}
    --我的服务器
    for k, v in pairs(loginLastServerDataList) do
        if groupListDataCache[Self_Server_Index] == nil then
            groupListDataCache[Self_Server_Index] = {}
        end
        if self:GetServerDataById(k) then
            table.insert(groupListDataCache[Self_Server_Index], k)
        end
    end
    --推荐服务器
    for k, v in ipairs(Servers) do
        if groupListDataCache[Recommend_Server_Index] == nil then
            groupListDataCache[Recommend_Server_Index] = {}
        end
        if self:GetServerRecommendByFlag(v.serverFlag) then
            table.insert(groupListDataCache[Recommend_Server_Index], v.serverId)
        end
    end
    
    for k, v in ipairs(Servers) do
        if groupListDataCache[v.serverGroup] == nil then
            groupListDataCache[v.serverGroup] = {}
        end
        table.insert(groupListDataCache[v.serverGroup], v.serverId)
    end
end

function ServerListManager:GetServerGroupList()
    LoggerHelper.Log("servergrouplist.xml***************************: " .. self.serverGroupFilePath)
    local xmlText = GameWorld.LoadFile(self.serverGroupFilePath)
    if xmlText == nil then
        LoggerHelper.Log("servergrouplist file not exist,download from web")
        self:GetServerGroupListFromWeb()
    else
        LoggerHelper.Log("servergrouplist file exist")
        LoggerHelper.Log("servergrouplist xml text"..xmlText)
        self:GetServerGroupListFromLocal(xmlText)
    end
    self:SortServerGroup()
    return ServerGroups
end

function ServerListManager:ServerGroupIsUse(groupId)
    if tonumber(groupId) < 0 then
        return true
    end
    for k, v in pairs(Servers) do
        if tonumber(groupId) == tonumber(v.serverGroup) then
            return true
        end
    end
    return false
end

function ServerListManager:ServerGroupListFromXml(ServersXmlData)
    ServerGroups = {}
    for k, v in pairs(ServersXmlData.root:children()) do
        local tmpServer = {}
        for k2, v2 in pairs(v:children()) do
            local v2name = v2:name()
            if v2name == "id" then
                tmpServer.groupId = tonumber(v2:value())
            elseif v2name == "groupName" then
                tmpServer.groupName = v2:value()
            elseif v2name == "sort" then
                tmpServer.sort = tonumber(v2:value())
            end
        end
        ServerListManager:AddServerGroup(tmpServer)
    end
    
    --增加本地和推荐
    local tmpServer = {["groupId"] = -2, ["groupName"] = LanguageDataHelper.CreateContent(577), ["sort"] = -2}
    ServerListManager:AddServerGroup(tmpServer)
    local tmpServer = {["groupId"] = -1, ["groupName"] = LanguageDataHelper.CreateContent(578), ["sort"] = -1}
    ServerListManager:AddServerGroup(tmpServer)
end

function ServerListManager:GetServerGroupListFromLocal(xmlText)
    local ServersGroupXmlData = XMLParser:ParseXmlText(xmlText)
    ServerListManager:ServerGroupListFromXml(ServersGroupXmlData)
end

function ServerListManager:GetServerGroupListFromWeb()
    local url = GameLoader.SystemConfig.ServerGroupUrl
    local onDone = function(result)ServerListManager:GetServerGroupListDone(result) end
    local onFail = function(result)EventDispatcher:TriggerEvent("GetServerGroupList") end
    GameWorld.DownloadString(url, onDone, onFail)
end

function ServerListManager:GetServerGroupListDone(content)
    if type(content) ~= "string" then
        return
    end
    GameWorld.SaveText(self.serverGroupFilePath, content)
    EventDispatcher:TriggerEvent("GetServerGroupList")
    EventDispatcher:TriggerEvent(GameEvents.ServerGroupListDataReady)
end
-- ========================================本地储存最近登录服务器数据==============================================================
function ServerListManager:InitEvent()
    EventDispatcher:AddEventListener(GameEvents.ON_ENTER_CHOOSE, function()self:UpdateLastServerData() end)
    --监听客户端强制退出
    EventDispatcher:AddEventListener(GameEvents.UNGINE_ONAPPLICATIONQUIT, function()self:UpdateLastServerData() end)
end

-- 获取本地最近登录数据
function ServerListManager:InitLocalLastServerData()
    loginLastServerDataList = LoginServerDataStorage.Load()
    for k, v in pairs(loginLastServerDataList) do
        table.insert(loginLastServerNameList, k)
    end
    
    self:SortServerListByLastLogin()
end

-- 储存最近登录数据到本地
function ServerListManager:SaveLocalLastServerData()
    LoginServerDataStorage.Save(loginLastServerDataList)
    
    self:SortServerListByLastLogin()
end

function ServerListManager:SortServerListByLastLogin()
    self:RefrshGroupListDataCache()
end

function ServerListManager:GetLocalLastServerData()
    if loginLastServerDataList == nil then
        self:InitLocalLastServerData()
    end
    return loginLastServerNameList
end

--有变化更新
function ServerListManager:UpdateLastServerData()
    if loginLastServerDataList == nil then
        return
    end
    
    local curServerId = SelectServer.serverId
    if loginLastServerDataList[curServerId] == nil then
        loginLastServerDataList[curServerId] = {}
    end
    
    loginLastServerDataList[curServerId].serverid = curServerId
    self:SaveLocalLastServerData()
end

function ServerListManager:GetLastServerDataByServerId(serverId)
    if loginLastServerDataList == nil then
        return nil
    end
    return loginLastServerDataList[serverId]
end

-- =====================================================================================================
function ServerListManager:LoadNoticeFromWeb()
    local template = GameLoader.SystemConfig.GetValueInCfg('NoticeUrl')
    if template == nil or template == '' then
        return
    end
    local onDone = function(result)self:OnNoticeHttpDone(result) end
    local onFail = function(result)self:OnNoticeHttpDone(result) end
    GameWorld.DownloadString(template, onDone, onFail)
end

function ServerListManager:NoticeFromXml(noticeXmlData)
    local noticeData = {}
    for k, v in pairs(noticeXmlData.root:children()) do
        local data = {}
        data.notice = {}
        for k2, v2 in pairs(v:children()) do
            local vname = v2:name()
            if vname == "tabname" then
                data.tabname = v2:value()
            elseif vname == "new" then
                data.new = v2:value()
            elseif vname == "notice" then
                local noticdata = {}
                for k3, v3 in pairs(v2:children()) do
                    local v3name = v3:name()
                    if v3name == 'title' then
                        noticdata.title = v3:value()
                    elseif v3name == 'content' then
                        noticdata.content = v3:value()
                    end
                end
                table.insert(data.notice, noticdata)
            end
        end
        table.insert(noticeData, data)
    end
    self:UpdateTag(noticeData)
end

function ServerListManager:UpdateTag(data)
    self.noticeData = data
end

function ServerListManager:GetNoticeData()
    return self.noticeData or {}
end

function ServerListManager:OnNoticeHttpDone(content)
    if type(content) ~= "string" then
        return
    end
    local noticeXmlData = XMLParser:ParseXmlText(content)
    self:NoticeFromXml(noticeXmlData)
    local md5 = GameWorld.CreateMD5(content)
    if LocalSetting.settingData.NoticeMd5 ~= md5 then
        LocalSetting.settingData.NoticeMd5 = md5
        LocalSetting:Save()
        EventDispatcher:TriggerEvent(GameEvents.OnShowNoticeView)
    end
end

--------------------------------------------------------------------------------------------------------
function ServerListManager:ChangeTestMode()
    -- LoggerHelper.Error(GameWorld.Player().account_name)
    GameManager.WhiteListManager.IsInWhiteListForTester(GameWorld.Player().account_name, function()
        local path = UnityEngine.Application.persistentDataPath .. ConstString.RutimeResourceConfig .. "/cfg.xml"
        local xmlText = GameWorld.LoadFile(path)
        if xmlText == nil then
            self:DownloadCfg()
            GameManager.GUIManager.ShowText(1, "Test Mode")
        else
            GameWorld.DeleteFile(path)
            self:DownloadVersion()
            GameManager.GUIManager.ShowText(1, "Normal Mode")
        end
    end)
end

function ServerListManager:ChangeTestModeByName(fileName)
    GameManager.WhiteListManager.IsInWhiteListForTester(GameWorld.Player().account_name, function()
        self:DownloadCfgByName(fileName)
        GameManager.GUIManager.ShowText(1, fileName .. ".xml Downloaded.")
    end)
end

function ServerListManager:RevertVersion()
    -- LoggerHelper.Error(GameWorld.Player().account_name)
    GameManager.WhiteListManager.IsInWhiteListForTester(GameWorld.Player().account_name, function()
        self:DownloadVersion()
        GameManager.GUIManager.ShowText(1, "Version has revert, reboot please.")
    end)
end

function ServerListManager:DownloadCfgByName(fileName)
    local template = GameLoader.SystemConfig.GetValueInCfg('CfgPath')
    if template == nil or template == '' then
        return
    end
    local onDone = function(result)self:OnCfgHttpDone(result) end
    local onFail = function(result)self:OnCfgHttpDone(result) end
    GameWorld.DownloadString(template .. fileName .. ".xml", onDone, onFail)
end

function ServerListManager:DownloadCfg()
    local template = GameLoader.SystemConfig.GetValueInCfg('CfgTest')
    if template == nil or template == '' then
        return
    end
    local onDone = function(result)self:OnCfgHttpDone(result) end
    local onFail = function(result)self:OnCfgHttpDone(result) end
    GameWorld.DownloadString(template, onDone, onFail)
end

function ServerListManager:OnCfgHttpDone(content)
    if type(content) ~= "string" then
        return
    end
    local path = UnityEngine.Application.persistentDataPath .. ConstString.RutimeResourceConfig .. "/cfg.xml"
    GameWorld.SaveText(path, content)
end

function ServerListManager:DownloadVersion()
    local template = GameLoader.SystemConfig.GetValueInCfg('VersionTest')
    if template == nil or template == '' then
        return
    end
    local onDone = function(result)self:OnVersionHttpDone(result) end
    local onFail = function(result)self:OnVersionHttpDone(result) end
    GameWorld.DownloadString(template, onDone, onFail)
end

function ServerListManager:OnVersionHttpDone(content)
    if type(content) ~= "string" then
        return
    end
    local path = UnityEngine.Application.persistentDataPath .. ConstString.RutimeResourceConfig .. "/VersionInfo.xml"
    GameWorld.SaveText(path, content)
end

ServerListManager:Init()
return ServerListManager
