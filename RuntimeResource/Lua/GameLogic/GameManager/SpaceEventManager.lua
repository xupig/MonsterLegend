local SpaceEventManager = {}

require "GameManager/SpaceSystem/SpaceActionEvent"
require "GameManager/SpaceSystem/SpaceSrcEvent"
require "GameManager/SpaceSystem/SpaceStateEvent"

local action_config = GameWorld.action_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function SpaceEventManager:Init()
	SpaceEventManager.TriggerMap = {}
	SpaceEventManager.TriggerMap["TriggerRegion"] = true
	--SpaceEventManager.TriggerMap["EntityTeleportData"] = true
    SpaceEventManager.TriggerMap["EntityTeleportUnlock"] = true
    SpaceEventManager.TriggerMap["EntityZoneUnlock"] = true
    SpaceEventManager.TriggerMap["EntityTriggerAlert"] = true
    SpaceEventManager.TriggerMap["EntityTowerTrigger"] = true
    SpaceEventManager.TriggerMap["EntityPathPoint"] = true
    SpaceEventManager.TriggerMap["EntityFlyTrigger"] = true
    
    SpaceActionEvent:Init()
    SpaceSrcEvent:Init()
    SpaceStateEvent:Init()
    self:AddEventListener()
end

function SpaceEventManager:AddEventListener()
	EventDispatcher:AddEventListener(GameEvents.ProcSpaceAction, function(eventID) SpaceActionEvent:ExecuteAction(eventID) end)
	EventDispatcher:AddEventListener(GameEvents.ActionMonsterDeath, function(monsterID, eventID) SpaceSrcEvent:MonsterDeath(monsterID, eventID) end)
	EventDispatcher:AddEventListener(GameEvents.DramaPlayEnd, function(drama,mark,eventvalue) SpaceSrcEvent:CGComplete(eventvalue) end)
    EventDispatcher:AddEventListener(GameEvents.EnterSpace, function(type) SpaceSrcEvent:EntityEnterSpace(type) end)

	EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, function() self:LeaveMap() end)
end

--【时机】进入地图
function SpaceEventManager:EnterMap(spaceData)
    -- if GameManager.GameSceneManager:InServerIns() and  GameManager.GameSceneManager:InBattleMap() then
    --      --开启所有AI
    --     GameWorld.LoggerHelper.Error("在服务器战斗本")
    --     if self._isReAttached then
    --         self._isReAttached = false
    --         -- GameWorld.LoggerHelper.Error("断线重连EnterMap")
    --         -- GameWorld.LoggerHelper.Error(tostring(self._isReAttached))
    --         return
    --     end
    -- else
    --     self._isReAttached = false
    -- end

    self._reach_path_idx = {}
    -- GameWorld.aiCanThink = true
    -- GUIManager.ShowPanel(PanelsConfig.SceneUI)
    -- GUIManager.ShowPanel(PanelsConfig.SceneBottomUI)
    SpaceActionEvent:EnterMap(spaceData)
    SpaceSrcEvent:EnterMap(spaceData)
    SpaceStateEvent:EnterMap(spaceData)
    --GameWorld.LoggerHelper.Error("场景进入地图", true)
end

--【时机】退出地图
function SpaceEventManager:LeaveMap()
    -- if GameManager.GameSceneManager:InServerIns() and GameManager.GameSceneManager:InBattleMap()then
    --     if self._isReAttached then
    --         return
    --     end
    -- end
    self._reach_path_idx = {}
    SpaceActionEvent:CleanSpace()
    SpaceSrcEvent:CleanSpace()
    --GUIManager.ClosePanel(PanelsConfig.SceneUI)
    --GUIManager.ClosePanel(PanelsConfig.SceneBottomUI)
end

--【时机】结算界面
function SpaceEventManager:OnBalance()
    SpaceSrcEvent:StopAction()
    --停止所有AI
	GameWorld.aiCanThink = false
end

--【时机】断线重连
function SpaceEventManager:OnReAttached()
    self._isReAttached = true
end

--【时机】当实体进入碰撞盒
function SpaceEventManager:OnTriggerEnter(entityId, cfg)
    -- if not cfg.eventActive then
	-- 	LoggerHelper.Error("action is not active. may be closed by other action")
	-- 	do return end
	-- end
	--LoggerHelper.Error("SpaceEventManager:OnTriggerEnter:"..cfg.id)
    local count = cfg.count or -1
	if count == -1 or cfg.proc_num < count then
        self:TriggerEvent(entityId, cfg, true)
    else
        self:ChangeTriggerState(cfg.id,false)

        --@debug_begin
        -- GameWorld.LoggerHelper.Log(cfg.name.."触发次数达到上线,关闭该碰撞盒")
        --@debug_end
    end
end

--【时机】当实体离开碰撞盒
function SpaceEventManager:OnTriggerExit(entityId, cfg)
    local count = cfg.count or -1
	if count == -1 or cfg.proc_num < count then
        if SpaceEventManager.TriggerMap[cfg.type] then
            SpaceSrcEvent:HandlerTrigger(entityId, cfg, false)
        end
    else
        self:ChangeTriggerState(cfg.id,false)

        --@debug_begin
        -- GameWorld.LoggerHelper.Log(cfg.name.."触发次数达到上线,关闭该碰撞盒")
        --@debug_end
    end
end

function SpaceEventManager:TriggerEvent(entityId, cfg, isEnter)
    local type = cfg.type
    if SpaceEventManager.TriggerMap[cfg.type] then
    	SpaceSrcEvent:HandlerTrigger(entityId, cfg, isEnter)
    end
end

--用于关闭碰撞盒
function SpaceEventManager:ChangeTriggerState(eventId,isEnable)
    SpaceActionEvent:ChangeTriggerState(eventId,isEnable)
end

SpaceEventManager:Init()
return SpaceEventManager