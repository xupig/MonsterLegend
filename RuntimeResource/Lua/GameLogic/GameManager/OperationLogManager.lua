--OperationLogManager.lua
OperationLogManager = {}

local LocalSetting = GameConfig.LocalSetting
local public_config = GameWorld.public_config

function OperationLogManager.BaseLogData(operationType, operationParam)
    local server = tostring(LocalSetting.settingData and LocalSetting.settingData.SelectedServerID or '')
    local player = GameWorld.Player()
    local account_dbid = ''
    local avatar_dbid = tostring(player and player.dbid or '')
    local role_name = tostring(player and player.name or '')
    local role_level = tostring(player and player.level or '')
    local scene = tostring(GameManager.GameSceneManager:GetCurrMapID() or '')
    local operation_time = tostring(os.time())
    local caozuo = tostring(operationType or '')
    local vip_level = tostring(player and player.vip_level or '')
    local account_name = tostring(player and player.account_name or '')
    local operation_param = tostring(operationParam or '')

    GameWorld.UploadLog({"log_type", tostring(public_config.AOFEI_LOG_CLIENT_BEHAVIOR), "server_id", server, "account_dbid", account_dbid, 
        "avatar_dbid", avatar_dbid, "avatar_name", role_name, "avatar_level", role_level, "map_id", scene, "operation_time", operation_time, 
        "operation_name", caozuo, "vip_level", vip_level, "account_name", account_name, "operation_param", operation_param})
end

return OperationLogManager
