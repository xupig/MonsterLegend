DamageType = {}

DamageType.AttackType = {}
DamageType.AttackType.HIT = 1
DamageType.AttackType.CRITICAL = 2
DamageType.AttackType.STRIKE = 3
DamageType.AttackType.MISS = 4
DamageType.AttackType.TREAT = 5
DamageType.AttackType.PARRY = 6  --格挡
DamageType.AttackType.DEADLY = 9  --会心
DamageType.AttackType.ABSORB = 10  --伤害吸收
DamageType.AttackType.INVINCIBLE = 11  --无敌
DamageType.AttackType.GET_HIT = 12  --受击
DamageType.AttackType.PET_HIT = 13  --宠物攻击
DamageType.AttackType.TREASURE_HIT = 14  --法宝攻击
DamageType.AttackType.UNYIELDING = 15  --不屈

DamageType.ArtNumberType = {}
DamageType.ArtNumberType.NOTSHOW = "notshow"
DamageType.ArtNumberType.HIT = "direndiaoxue"
DamageType.ArtNumberType.CRITICAL = "baoji"
DamageType.ArtNumberType.TREAT = "huixue"
DamageType.ArtNumberType.GET_HIT = "diaoxue"
DamageType.ArtNumberType.PET_HIT = "chongwu"
DamageType.ArtNumberType.TREASURE_HIT = "fabao"
DamageType.ArtNumberType.DEADLY = "huixin"

DamageType.ArtWord = {}
DamageType.ArtWord.ADD_YELLOW = "jiahaohuang"
DamageType.ArtWord.ADD_BLUE = "jiahaolan"

DamageType.ArtWord.TREAT = "huixue"
DamageType.ArtWord.MISS = "shanbi"
DamageType.ArtWord.CRITICAL = "baoji"
DamageType.ArtWord.ABSORB = "shanghaixishou"
DamageType.ArtWord.INVINCIBLE = "wudi"
DamageType.ArtWord.DEADLY = "huixin"
DamageType.ArtWord.PARRY = "gedang"
DamageType.ArtWord.UNYIELDING = "wudi" --用无敌的资源

return DamageType