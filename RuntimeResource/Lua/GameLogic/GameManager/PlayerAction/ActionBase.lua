-- 主角行为基类
local ActionBase = Class.ActionBase(ClassTypes.XObject)

local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time

function ActionBase:__ctor__()
	self.priority 	= PlayerActionConfig.PRIORITY_ONE -- 行为优先级
	self.canBreak	= true							-- 是否能被打断
	self.duration	= 0
	self.endTime	= 0								-- 行为结束时间
	self.state		= PlayerActionConfig.ACTION_READY -- 行为状态
	self.player		= nil	                -- 拿到主角
	self.tttype 	= "1"
end

-- 执行这个行为
function ActionBase:DoAction()
	if self.state == PlayerActionConfig.ACTION_END 
			or self.state == PlayerActionConfig.ACTION_FAIL then
		return self.state
	end
	if self.state == PlayerActionConfig.ACTION_READY then		-- 如果行为处于就绪状态
		if self:CanDo() then
			self:ResetDuration(self.duration)
			self:StartAction()
		else
			self:StopAction()
			return self.state
		end
	end
	if self.duration > 0 then
		if Time.realtimeSinceStartup - self.endTime >= 0 then
			self:EndAction()
		end
	end
	if self.state == PlayerActionConfig.ACTION_DOING then
		self:OnDo()
	end
	return self.state
end

-- 开始行为
function ActionBase:StartAction()
	self.state = PlayerActionConfig.ACTION_DOING
	self:OnStart()
end

-- 终止行为
function ActionBase:StopAction()
	if self.tttype == "ActionFindPosition" then
		-- LoggerHelper.LogEx("Sam :StopAction()" .. tostring(self.tttype) , true)
	end
	self.state = PlayerActionConfig.ACTION_FAIL
	self:OnFail()
end

-- 正常终止
function ActionBase:EndAction()
	self.state = PlayerActionConfig.ACTION_END
	self:OnEnd()
end

-- 重置时间
function ActionBase:ResetDuration(duration)
	self.duration = duration
	if self.duration > 0 then
		self.endTime = Time.realtimeSinceStartup + self.duration;
	else
		self.endTime = Time.realtimeSinceStartup;
	end
	--LoggerHelper.Error("ResetDuration:"..self.endTime)
end

-- 以下函数由子类实现，不应手动调用
-- 判断条件
function ActionBase:CanDo()
	return true
end

function ActionBase:OnDo()
	
end

-- 在行为将要开始前被调用
function ActionBase:OnStart()

end

-- 在行为失败时被调用
function ActionBase:OnFail()
	
end

-- 在行为正常结束时被调用
function ActionBase:OnEnd()
	
end