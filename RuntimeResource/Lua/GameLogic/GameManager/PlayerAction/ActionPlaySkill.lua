local ActionPlaySkill = Class.ActionPlaySkill(ClassTypes.ActionBase)

local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time

function ActionPlaySkill:__ctor__(args)
	self.priority 	= PlayerActionConfig.PRIORITY_ONE
	self.canBreak = false
	self.skillPos = args.skillPos
	self.duration = args.duration
	self.player = args.player
	self._source = args.source
	self.actionType = "skill"
end

--override
function ActionPlaySkill:CanDo()
	return true
end

--override
function ActionPlaySkill:OnDo()

end

--override
function ActionPlaySkill:OnStart()
	--LoggerHelper.Error(self.skillPos .. ' OnStart->>>>>>>>>>>:' .. Time.realtimeSinceStartup)
	local newDuration = self.player:PlaySkillByPos(self.skillPos)
	if newDuration > 0 then
		--LoggerHelper.Error(self.skillPos .. ' newDuration:' .. newDuration)
		self:ResetDuration(newDuration)
	else
		self:StopAction()
	end
end

--override
function ActionPlaySkill:OnFail()
	
end

--override
function ActionPlaySkill:OnEnd()
	--LoggerHelper.Error(self.skillPos .. ' OnEnd--<<<<<<<<<<<<:' .. Time.realtimeSinceStartup)
end