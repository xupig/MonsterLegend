--探索野外宝箱
local ActionCommonCollect = Class.ActionCommonCollect(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local CD_INTERVAL = 1000
local CollectManager = GameManager.CollectManager

function ActionCommonCollect:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    --self._targetPosition = args.targetPosition
    self._player = args.player
    --self._treasureId = args.treasureId
    self._cdRemainTime = args.cdtime
    self._onInterrupt = function ()
        self:StopAction()
    end

    self._intervalCb = function()
        if self._cdRemainTime <= 0  and self.state == PlayerActionConfig.ACTION_DOING then
            self:EndAction()
        end
        self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
    end
end

--override
function ActionCommonCollect:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionCommonCollect:OnDo()

end

--override
function ActionCommonCollect:OnStart()
    self._player:SetCollectSingState(true)
    self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, self._intervalCb)
    EventDispatcher:AddEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCommonCollect:OnFail()
    self._player:SetCollectSingState(false)
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:StopCommonCollect()
    EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCommonCollect:OnEnd()
    self._player:SetCollectSingState(false)
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:CompleteCommonCollect()
    EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end