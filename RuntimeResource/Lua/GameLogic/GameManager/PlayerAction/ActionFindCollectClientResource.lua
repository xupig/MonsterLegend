local ActionFindCollectClientResource = Class.ActionFindCollectClientResource(ClassTypes.ActionFindPosition)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time

function ActionFindCollectClientResource:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._source = args.source
    self._player = args.player
    self._endDistance = args.endDistance or 1
    self._targetPosition = args.pos
    self._doMoveDistance = args.doMoveDistance or self._endDistance
    self._onBeginCollectResource = function()self:BeginCollectResource() end
end

--override
function ActionFindCollectClientResource:CanDo()
    if self._targetPosition == nil then
        return false
    end
    local result = self._base.CanDo(self)
    return result
end


function ActionFindCollectClientResource:BeginCollectResource()

    EventDispatcher:RemoveEventListener(GameEvents.PlayerCommandCollectClientResource, self._onBeginCollectResource)
    self:ClearStartCollectTimer()
end

function ActionFindCollectClientResource:ClearStartCollectTimer()
    if self._startCollectTimer then
        TimerHeap:DelTimer(self._startCollectTimer)
        self._startCollectTimer = nil
    end
end

--override
function ActionFindCollectClientResource:OnEnd()
    self._base.OnEnd(self)
    
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandCollectClientResource, self._onBeginCollectResource)
    self:ClearStartCollectTimer()
    self._startCollectTimer = TimerHeap:AddSecTimer(0.1, 1000, 3, function()
        -- LoggerHelper.Error("ActionFindCollectClientResource:START_COLLECT_CLIENT_RESOURCE")
        EventDispatcher:TriggerEvent(GameEvents.START_COLLECT_CLIENT_RESOURCE) end)
--LoggerHelper.Error("ActionFindCollectClientResource:OnEnd()")
--EventDispatcher:TriggerEvent(GameEvents.START_COLLECT_CLIENT_RESOURCE)
end
