-- 用于主角做完action后的callback

local ActionCallBack = Class.ActionCallBack(ClassTypes.ActionBase)

function ActionCallBack:__ctor__( args )
	self.canBreak	= true
	self.cb = args.cb
end


--override
function ActionCallBack:CanDo()
	return true
end

--override
function ActionCallBack:OnDo()
	
end

--override
function ActionCallBack:OnStart()
	
end

--override
function ActionCallBack:OnFail()
	
end

--override
function ActionCallBack:OnEnd()
	
end
