local ActionFindPosition = Class.ActionFindPosition(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local Vector2 = Vector2
local PartnerManager = PlayerManager.PartnerManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager

local num = 1

function ActionFindPosition:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._source = args.source
    self._targetPosition = args.targetPosition
    self._player = args.player
    self._endDistance = args.endDistance or 1
    self._retryCounter = 60
    self._needMoveEvent = args.needMoveEvent
    self._needSetRide = args.needSetRide == nil or args.needSetRide
    self._doMoveDistance = args.doMoveDistance or self._endDistance
    self._stopMoveFlag = false
    
    self._onTeleportPosition = function()self:TeleportPositionCallback() end
    self._onTeleportPositionResp = function(error_code, teleportId)self:TeleportPositionRespCallback(error_code, teleportId) end
    self.tttype = "ActionFindPosition"
    self._numTT = num
    num = num + 1
end

--override
function ActionFindPosition:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionFindPosition:OnDo()

end

--请求传送
function ActionFindPosition:TeleportPositionCallback()
    self:StopMoveWithoutCallback()
end

--开始传送
function ActionFindPosition:TeleportPositionRespCallback(error_code, teleportId)
    if error_code == 0 then
        -- LoggerHelper.Error("ActionFindPosition:TeleportPositionRespCallback(error_code, teleportId)")
        -- if self._teleportTimer ~= nil then
        --     TimerHeap:DelTimer(self._teleportTimer)
        --     self._teleportTimer = nil
        -- end
        -- self._teleportTimer = TimerHeap:AddSecTimer(1, 0, 0, function()self:DoMove() end)
        self:EndAction()--传送成功直接结束，肯定在目标点
    end
end

function ActionFindPosition:MoveCallback()
    -- LoggerHelper.Error("ActionFindPosition:MoveCallback()" .. tostring(self._stopMoveFlag) .. "num ===" .. tostring(self._numTT), true)
    if self._stopMoveFlag then return end
    if self.state == PlayerActionConfig.ACTION_DOING then
        local myPos = self._player:GetPosition()
        local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(self._targetPosition.x, self._targetPosition.z))
        -- LoggerHelper.Log("ActionFindPosition MoveCallback " .. dis .. " " .. self._endDistance .. " " .. PrintTable:TableToStr(self._targetPosition))
        if dis > self._endDistance + 1 then
            if self._moveCallBackTimer ~= nil then
                TimerHeap:DelTimer(self._moveCallBackTimer)
                self._moveCallBackTimer = nil
            end
            self._moveCallBackTimer = TimerHeap:AddSecTimer(1, 0, 0, function()self:DoMove(true) end)
        else
            self:EndAction()
        end
    else
        -- LoggerHelper.LogEx("Sam :ActionFindPosition stop action 11:" .. tostring(self.state) .. "self._numTT ====" .. tostring(self._numTT))
        self:StopAction()
    end
end

function ActionFindPosition:DoMove(moveCallBack)
    -- LoggerHelper.Error("domove" .. tostring(self._numTT))
    if self._retryCounter >= 0 then
        local myPos = self._player:GetPosition()
        local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(self._targetPosition.x, self._targetPosition.z))
        -- LoggerHelper.Log("ActionFindPosition MoveCallback " .. dis .. " " .. self._endDistance .. " " .. PrintTable:TableToStr(self._targetPosition))
        if dis > self._endDistance + 1 then
            self._player:Move(self._targetPosition, -2, function(entity)self:MoveCallback(entity) end, self._endDistance)
            self._stopMoveFlag = false
            -- LoggerHelper.LogEx("Sam :DoMove" .. "self._numTT ====" .. tostring(self._numTT))
            if self._needMoveEvent == true then --给世界地图显示路点用
                EventDispatcher:TriggerEvent(GameEvents.OnPlayerMove)
            end
            self._retryCounter = self._retryCounter - 1
        else
            self:EndAction()
        end
    else
        -- LoggerHelper.LogEx("Sam :ActionFindPosition stop action 22:" .. tostring(self.state) .. "self._numTT ====" .. tostring(self._numTT))
        self:StopAction()
    end
end

--override
function ActionFindPosition:OnStart()
    if GameWorld.SpecialExamine then
        self:EndAction()
        do return end
    end
    if self._player then
        local myPos = self._player:GetPosition()
        local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(self._targetPosition.x, self._targetPosition.z))
        if dis <= self._doMoveDistance then
            self._move = false
            self:EndAction()
            return
        else
            if self._player then
                self._player:StopAutoFight(false, self._source)
                self._player:StopMoveWithoutCallback()
            end
        end
        
        if dis <= self._endDistance + 1 then
            self:EndAction()
            return
        end
        
        if dis > GlobalParamsHelper.GetParamValue(600) and self._player.horse_grade_ride == 0 and self._needSetRide then
            PartnerManager:HorseRideReq()
        end
    end
    -- LoggerHelper.LogEx("Sam :ActionFindPosition OnStart " .. PrintTable:TableToStr(self._targetPosition)  .. "self._numTT ====" .. tostring(self._numTT))
    EventDispatcher:AddEventListener(GameEvents.TeleportPosition, self._onTeleportPosition)
    EventDispatcher:AddEventListener(GameEvents.TeleportPositionResp, self._onTeleportPositionResp)
    if not InstanceManager:HasUseLittleShoeSuccess() then -- 成功使用小飞鞋的先不要动，等传送结束
        self:DoMove()
    else
        if self._teleportTimer ~= nil then
            TimerHeap:DelTimer(self._teleportTimer)
            self._teleportTimer = nil
        end
        self._teleportTimer = TimerHeap:AddSecTimer(1, 0, 0, function()self:DoMove() end)
    end
    EventDispatcher:TriggerEvent(GameEvents.Start_Auto_FindPath)
end

--override
function ActionFindPosition:OnFail()
    -- LoggerHelper.Error("Sam :ActionFindPosition:OnFail()" .. "self._numTT ====" .. tostring(self._numTT), true)
    self:StopMoveWithoutCallback()
    self:StopAllTimer()
    EventDispatcher:RemoveEventListener(GameEvents.TeleportPosition, self._onTeleportPosition)
    EventDispatcher:RemoveEventListener(GameEvents.TeleportPositionResp, self._onTeleportPositionResp)
    EventDispatcher:TriggerEvent(GameEvents.Stop_Auto_FindPath)
end

--override
function ActionFindPosition:OnEnd()
    -- LoggerHelper.Error("ActionFindPosition:OnEnd()")
    self:StopAllTimer()
    EventDispatcher:RemoveEventListener(GameEvents.TeleportPosition, self._onTeleportPosition)
    EventDispatcher:RemoveEventListener(GameEvents.TeleportPositionResp, self._onTeleportPositionResp)
    EventDispatcher:TriggerEvent(GameEvents.Stop_Auto_FindPath)
end


function ActionFindPosition:StopMoveWithoutCallback()
    self._stopMoveFlag = true
    self._player:StopMoveWithoutCallback()
end

function ActionFindPosition:StopAllTimer()
    if self._moveCallBackTimer ~= nil then
        TimerHeap:DelTimer(self._moveCallBackTimer)
        self._moveCallBackTimer = nil
    end
    
    if self._teleportTimer ~= nil then
        TimerHeap:DelTimer(self._teleportTimer)
        self._teleportTimer = nil
    end
end
