local ActionGotoBossSceneElite = Class.ActionGotoBossSceneElite(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local GuildManager = PlayerManager.GuildManager
local GameSceneManager = GameManager.GameSceneManager

function ActionGotoBossSceneElite:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._bossId = args.bossId
    self._player = args.player
    self._targetMapId = args.targetMapId
    
    self._onWorldBossEnter = function(flag)self:OnWorldBossEnter(flag) end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionGotoBossSceneElite:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionGotoBossSceneElite:OnDo()

end

function ActionGotoBossSceneElite:OnWorldBossEnter(flag)
    if not flag then
        self._isFail = true
        self:StopAction()
    else
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionGotoBossSceneElite:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionGotoBossSceneElite:LoadGameSceneEndCallback()
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end
--override
function ActionGotoBossSceneElite:OnStart()
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossEliteEnter, self._onWorldBossEnter)
    if self._targetMapId == GameSceneManager:GetCurrMapID() then
        self:EndAction()
    else
        GuildManager:GetGuildEliteMonsterEnterReq(self._bossId)
    end
end

--override
function ActionGotoBossSceneElite:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossEnter, self._onWorldBossEnter)
end

--override
function ActionGotoBossSceneElite:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossEnter, self._onWorldBossEnter)
end
