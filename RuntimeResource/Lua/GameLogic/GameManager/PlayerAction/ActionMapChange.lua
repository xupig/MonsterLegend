local ActionMapChange = Class.ActionMapChange(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local InstanceManager = PlayerManager.InstanceManager
local GameSceneManager = GameManager.GameSceneManager
local TaskConfig = GameConfig.TaskConfig

function ActionMapChange:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._targetMapId = args.targetMapId
    self._targetPosition = args.targetPosition
    self._player = args.player
    self._useLittleShoes = args.useLittleShoes
    self._source = args.source
    
    self._onUseLittleShoeEnd = function()self:UseLittleShoeEndCallback() end
    self._onUseLittleShoeFail = function()self:UseLittleShoeFailCallback() end
    
    self._onMapChangeFail = function()self:MapChangeFailCallback() end
    self._onMapChangeEnd = function()self:MapChangeEndCallback() end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionMapChange:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionMapChange:OnDo()

end

--使用小飞鞋成功
function ActionMapChange:UseLittleShoeEndCallback()
    --LoggerHelper.Log("UseLittleShoeEndCallback")
    if self._isFail == nil and self._isInSameScene then
        self:EndAction()
    end
end

--使用小飞鞋失败
function ActionMapChange:UseLittleShoeFailCallback()
    -- LoggerHelper.Log("UseLittleShoeFailCallback")
    self._isFail = true
    self:StopAction()
end

--请求切副本失败
function ActionMapChange:MapChangeFailCallback()
    -- LoggerHelper.Log("MapChangeFailCallback")
    self._isFail = true
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
    self:StopAction()
end

--请求切副本成功
function ActionMapChange:MapChangeEndCallback()
    -- LoggerHelper.Log("MapChangeEndCallback")
    if self._isFail == nil then
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionMapChange:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionMapChange:LoadGameSceneEndCallback()
    --LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end

--override
function ActionMapChange:OnStart()
    --LoggerHelper.Log("ActionMapChange OnStart " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._useLittleShoes == TaskConfig.IS_USE_LITTLE_SHOES then
        EventDispatcher:AddEventListener(GameEvents.ON_USE_LITTLE_SHOE_END, self._onUseLittleShoeEnd)
        EventDispatcher:AddEventListener(GameEvents.ON_USE_LITTLE_SHOE_FAIL, self._onUseLittleShoeFail)
        local pos = self._targetPosition
        if self._player then
            self._player:StopAutoFight()
            self._player:StopMoveWithoutCallback()
        end
        if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
            EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
            EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
            self._isInSameScene = false
            InstanceManager:UseLittleShoe(self._targetMapId, true, pos.x, pos.y, pos.z)
        else
            local myPos = self._player:GetPosition()
            local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(pos.x, pos.z))
            if dis <= 10 then
                self:EndAction()
            else
                self._isInSameScene = true
                InstanceManager:UseLittleShoe(self._targetMapId, true, pos.x, pos.y, pos.z)
            end
        end
    elseif self._useLittleShoes == TaskConfig.IS_TELEPORT_TO then
        --直接传送到目标坐标
        EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_FAIL, self._onMapChangeFail)
        EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_END, self._onMapChangeEnd)
        EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_EXIT_FAIL, self._onMapChangeFail)
        EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_EXIT_END, self._onMapChangeEnd)
        local pos = self._targetPosition
        if self._player then
            self._player:StopAutoFight()
            self._player:StopMoveWithoutCallback()
        end
        if self._targetMapId == GameSceneManager:GetCurrMapID() then
            if self._player then
                self._player:Teleport(pos)
            end
            self:EndAction()
        else
            if InstanceManager:CommonChangeMap(self._targetMapId, pos.x, pos.y, pos.z) == false then
                self:StopAction()
            end
        end
    else
        EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_FAIL, self._onMapChangeFail)
        EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_END, self._onMapChangeEnd)
        EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_EXIT_FAIL, self._onMapChangeFail)
        EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_EXIT_END, self._onMapChangeEnd)
        if self._targetMapId == GameSceneManager:GetCurrMapID() then
            self:EndAction()
        else
            if self._player then
                self._player:StopAutoFight()
                self._player:StopMoveWithoutCallback()
            end
            if InstanceManager:CommonChangeMap(self._targetMapId) == false then
                self:StopAction()
            end
        end
    end
end

--override
function ActionMapChange:OnFail()
    if self._useLittleShoes == TaskConfig.IS_USE_LITTLE_SHOES then
        EventDispatcher:RemoveEventListener(GameEvents.ON_USE_LITTLE_SHOE_END, self._onUseLittleShoeEnd)
        EventDispatcher:RemoveEventListener(GameEvents.ON_USE_LITTLE_SHOE_FAIL, self._onUseLittleShoeFail)
    else
        EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_FAIL, self._onMapChangeFail)
        EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_END, self._onMapChangeEnd)
        EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_EXIT_FAIL, self._onMapChangeFail)
        EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_EXIT_END, self._onMapChangeEnd)
    end
end

--override
function ActionMapChange:OnEnd()
    if self._useLittleShoes == TaskConfig.IS_USE_LITTLE_SHOES then
        EventDispatcher:RemoveEventListener(GameEvents.ON_USE_LITTLE_SHOE_END, self._onUseLittleShoeEnd)
        EventDispatcher:RemoveEventListener(GameEvents.ON_USE_LITTLE_SHOE_FAIL, self._onUseLittleShoeFail)
    else
        EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_FAIL, self._onMapChangeFail)
        EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_MAP_CHANGE_END, self._onMapChangeEnd)
        EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_EXIT_FAIL, self._onMapChangeFail)
        EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_EXIT_END, self._onMapChangeEnd)
    end
end
