local ActionGotoGodIslandScene = Class.ActionGotoGodIslandScene(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local GodIslandManager = PlayerManager.GodIslandManager
local GameSceneManager = GameManager.GameSceneManager

function ActionGotoGodIslandScene:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._bossId = args.bossId
    self._player = args.player
    self._targetMapId = args.targetMapId
    
    self._onGodIslandEnter = function(flag)self:OnGodIslandEnter(flag) end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionGotoGodIslandScene:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionGotoGodIslandScene:OnDo()

end

function ActionGotoGodIslandScene:OnGodIslandEnter(flag)
    -- LoggerHelper.Log("OnGodIslandEnter: " .. tostring(flag))
    if not flag then
        self._isFail = true
        self:StopAction()
    else
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionGotoGodIslandScene:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionGotoGodIslandScene:LoadGameSceneEndCallback()
    -- LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end
--override
function ActionGotoGodIslandScene:OnStart()
    -- LoggerHelper.Log("ActionGotoGodIslandScene OnStart " .. self._bossId)
    EventDispatcher:AddEventListener(GameEvents.OnGodIslandEnter, self._onGodIslandEnter)
    if self._targetMapId == GameSceneManager:GetCurrMapID() then
        self:EndAction()
    else
        GodIslandManager:Enter(self._bossId)
    end
end


-- function ActionGotoGodIslandScene:HPChange(change)
--     --玩家失血
--     if change > GameWorld.Player().cur_hp then
--         self:OnFail()
--     end
-- end
--override
function ActionGotoGodIslandScene:OnFail()
    -- LoggerHelper.Log("ActionGotoGodIslandScene OnFail " .. self._bossId)
    EventDispatcher:RemoveEventListener(GameEvents.OnGodIslandEnter, self._onGodIslandEnter)
end

--override
function ActionGotoGodIslandScene:OnEnd()
    -- LoggerHelper.Log("ActionGotoGodIslandScene OnEnd " .. self._bossId)
    EventDispatcher:RemoveEventListener(GameEvents.OnGodIslandEnter, self._onGodIslandEnter)
end
