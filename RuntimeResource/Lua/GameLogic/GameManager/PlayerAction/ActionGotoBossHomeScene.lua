local ActionGotoBossHomeScene = Class.ActionGotoBossHomeScene(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local BossHomeManager = PlayerManager.BossHomeManager
local GameSceneManager = GameManager.GameSceneManager

function ActionGotoBossHomeScene:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._bossId = args.bossId
    self._player = args.player
    self._targetMapId = args.targetMapId
    self._source = args.source
    
    self._onBossHomeEnter = function(flag)self:OnBossHomeEnter(flag) end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionGotoBossHomeScene:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionGotoBossHomeScene:OnDo()

end

function ActionGotoBossHomeScene:OnBossHomeEnter(flag)
    if not flag then
        self._isFail = true
        self:StopAction()
    else
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionGotoBossHomeScene:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionGotoBossHomeScene:LoadGameSceneEndCallback()
    -- LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end
--override
function ActionGotoBossHomeScene:OnStart()
    -- LoggerHelper.Log("ActionGotoBossHomeScene OnStart " .. self._bossId)
    EventDispatcher:AddEventListener(GameEvents.On_Boss_Home_Enter, self._onBossHomeEnter)
    if self._targetMapId == GameSceneManager:GetCurrMapID() then
        self:EndAction()
    else
        BossHomeManager:OnBossEnter(self._bossId)
    end
end


-- function ActionGotoBossHomeScene:HPChange(change)
--     --玩家失血
--     if change > GameWorld.Player().cur_hp then
--         self:OnFail()
--     end
-- end
--override
function ActionGotoBossHomeScene:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.On_Boss_Home_Enter, self._onBossHomeEnter)
end

--override
function ActionGotoBossHomeScene:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.On_Boss_Home_Enter, self._onBossHomeEnter)
end
