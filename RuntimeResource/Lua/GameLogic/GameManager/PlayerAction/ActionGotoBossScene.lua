local ActionGotoBossScene = Class.ActionGotoBossScene(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local WorldBossManager = PlayerManager.WorldBossManager
local GameSceneManager = GameManager.GameSceneManager

function ActionGotoBossScene:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._bossId = args.bossId
    self._player = args.player
    self._targetMapId = args.targetMapId
    self._source = args.source
    
    self._onWorldBossEnter = function(flag)self:OnWorldBossEnter(flag) end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionGotoBossScene:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionGotoBossScene:OnDo()

end

function ActionGotoBossScene:OnWorldBossEnter(flag)
    -- LoggerHelper.Log("OnWorldBossEnter: " .. tostring(flag))
    if not flag then
        self._isFail = true
        self:StopAction()
    else
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionGotoBossScene:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionGotoBossScene:LoadGameSceneEndCallback()
    -- LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end
--override
function ActionGotoBossScene:OnStart()
    -- LoggerHelper.Log("ActionGotoBossScene OnStart " .. self._bossId)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossEnter, self._onWorldBossEnter)
    if self._targetMapId == GameSceneManager:GetCurrMapID() then
        self:EndAction()
    else
        WorldBossManager:OnBossEnter(self._bossId)
    end
end


-- function ActionGotoBossScene:HPChange(change)
--     --玩家失血
--     if change > GameWorld.Player().cur_hp then
--         self:OnFail()
--     end
-- end
--override
function ActionGotoBossScene:OnFail()
    -- LoggerHelper.Log("ActionGotoBossScene OnFail " .. self._bossId)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossEnter, self._onWorldBossEnter)
end

--override
function ActionGotoBossScene:OnEnd()
    -- LoggerHelper.Log("ActionGotoBossScene OnEnd " .. self._bossId)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossEnter, self._onWorldBossEnter)
end
