local ActionPassInst = Class.ActionPassInst(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local GameSceneManager = GameManager.GameSceneManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper

function ActionPassInst:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._targetInstId = args.targetInstId
end

--override
function ActionPassInst:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionPassInst:OnStart()
    local mapIds = InstanceDataHelper.GetInstanceMapIds(self._targetInstId)
    local curMapId = GameSceneManager:GetCurrMapID()
    for i, v in ipairs(mapIds) do
        if curMapId == v then
            self:EndAction()
            return
        end
    end
    self:StopAction()
end

--override
function ActionPassInst:OnEnd()
    local player = GameWorld.Player()
    if player then
        player:StartAutoFight()
    end
-- LoggerHelper.Log("ActionPassInst OnEnd " .. PrintTable:TableToStr(self._targetPosition))
end
