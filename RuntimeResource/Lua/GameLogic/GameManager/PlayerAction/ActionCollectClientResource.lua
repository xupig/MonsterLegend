local ActionCollectClientResource = Class.ActionCollectClientResource(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local CD_INTERVAL = 1000
local CollectManager = GameManager.CollectManager

function ActionCollectClientResource:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    --self._targetPosition = args.targetPosition
    self._player = args.player
    self._entityCfgId = args.entityCfgId
    self._cdRemainTime = args.cdtime
    self._source = args.source

    -- self._onInterrupt = function ()
    --     LoggerHelper.Error("self._onInterrupt")
    --     self:StopAction()
    -- end
end

--override
function ActionCollectClientResource:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionCollectClientResource:OnDo()

end

--override
function ActionCollectClientResource:OnStart()
    self._player:SetCollectSingState(true)
    self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, 
        function()
            if self._cdRemainTime <= 0  and self.state == PlayerActionConfig.ACTION_DOING then
                self:EndAction()
            end
            self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
        end
    )
    --EventDispatcher:AddEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCollectClientResource:OnFail()
    self._player:SetCollectSingState(false)
    --LoggerHelper.Error("ActionCollectClientResource OnFail ")
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:StopCollectClientEntity()
    --EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCollectClientResource:OnEnd()
    self._player:SetCollectSingState(false)
    --LoggerHelper.Error("ActionCollectClientResource OnEnd ")
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:CollectClientEntity()
    --EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end