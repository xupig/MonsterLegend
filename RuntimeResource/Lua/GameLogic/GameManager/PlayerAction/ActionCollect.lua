local ActionCollect = Class.ActionCollect(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local CD_INTERVAL = 1000
local CollectManager = GameManager.CollectManager

function ActionCollect:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    --self._targetPosition = args.targetPosition
    self._player = args.player
    self._itemEid = args.itemEid
    self._cdRemainTime = args.cdtime
    self._source = args.source

    self._onInterrupt = function ()
        self:StopAction()
    end
end

--override
function ActionCollect:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionCollect:OnDo()

end

--override
function ActionCollect:OnStart()
    self._player:SetCollectSingState(true)
    self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, 
        function()
            if self._cdRemainTime <= 0  and self.state == PlayerActionConfig.ACTION_DOING then
                self:EndAction()
            end
            self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
        end
    )
    EventDispatcher:AddEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCollect:OnFail()
    self._player:SetCollectSingState(false)
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:StopCollect()
    EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end

--override
function ActionCollect:OnEnd()
    self._player:SetCollectSingState(false)
    TimerHeap:DelTimer(self._intervalTimerID)
    CollectManager:CollectItem()
    EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_COLLECT,self._onInterrupt)
end