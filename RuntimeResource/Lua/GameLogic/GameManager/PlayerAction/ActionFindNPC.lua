local ActionFindNPC = Class.ActionFindNPC(ClassTypes.ActionFindPosition)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local GameSceneManager = GameManager.GameSceneManager

function ActionFindNPC:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._source = args.source
    self._npcEId = args.npcEId
    self._npcId = args.npcId
    self._player = args.player
    self._endDistance = args.endDistance or 1
    self._doMoveDistance = args.doMoveDistance or self._endDistance
    -- LoggerHelper.Log("Ash: ActionFindNPC:" .. PrintTable:TableToStr(args))
    if args.targetPosition ~= nil then
        self._targetPosition = args.targetPosition
    else
        if self._npcEId == nil then
            self:FindNPC()
        end
        local npc = GameWorld.GetEntity(self._npcEId)
        -- LoggerHelper.Log("ActionFindNPC __ctor__ " .. self._npcEId)
        if npc ~= nil then
            self._targetPosition = npc:GetPosition()
            -- LoggerHelper.Log("ActionFindNPC __ctor__ " .. npc.npc_id .. " " .. npc.id .. " " .. npc.name .. " " .. PrintTable:TableToStr(self._targetPosition))
        end
    end
end

function ActionFindNPC:FindNPC()
    local npcs = GameSceneManager:GetNPCs()
    --LoggerHelper.Log("Ash: ActionFindNPC:" .. PrintTable:TableToStr(npcs))
    for i, npc in pairs(npcs) do
        -- LoggerHelper.Log("Ash: GetTaskNPC for npc.npc_id:" .. npc.npc_id)
        if npc.npc_id == self._npcId then
            -- LoggerHelper.Log("Ash: GetTaskNPC npc.id:" .. npc.id)
            self._npcEId = npc.id
        end
    end
end

--override
function ActionFindNPC:CanDo()
    if self._targetPosition == nil then
        return false
    end
    local result = self._base.CanDo(self)
    return result
end

--override
function ActionFindNPC:OnEnd()
    self._base.OnEnd(self)
    if self._npcEId == nil then
        self:FindNPC()
    end
    local npc = GameWorld.GetEntity(self._npcEId)
    --  LoggerHelper.Log("ActionFindNPC OnEnd " .. tostring(npc ~= nil))
    if npc ~= nil then
        npc:Ask()
    end
end
