local ActionFindTeleportPosition = Class.ActionFindTeleportPosition(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local InstanceManager = PlayerManager.InstanceManager
local GameSceneManager = GameManager.GameSceneManager
local Vector2 = Vector2
local PartnerManager = PlayerManager.PartnerManager

function ActionFindTeleportPosition:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._targetMapId = args.targetMapId
    self._targetPosition = args.targetPosition
    self._player = args.player
    self._source = args.source

    self._endDistance = args.endDistance or 1
    self._retryCounter = 60
    self._needSetRide = args.needSetRide == nil or args.needSetRide
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--切副本过程失败
function ActionFindTeleportPosition:LoadGameSceneStartCallback()
    --LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self:StopAction()
    end
end

--切副本过程成功
function ActionFindTeleportPosition:LoadGameSceneEndCallback()
    self:EndAction()
end

--override
function ActionFindTeleportPosition:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionFindTeleportPosition:OnDo()

end

function ActionFindTeleportPosition:MoveCallback()
    if self.state == PlayerActionConfig.ACTION_DOING then
        local myPos = self._player:GetPosition()
        local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(self._targetPosition.x, self._targetPosition.z))
        if dis > self._endDistance + 1 then
            TimerHeap:AddSecTimer(1, 0, 0, function()self:DoMove() end)
        end
    else
        self:StopAction()
    end
end

function ActionFindTeleportPosition:DoMove()
    if self._retryCounter >= 0 then
        self._player:Move(self._targetPosition, -2, function(entity)self:MoveCallback(entity) end, self._endDistance)
        self._retryCounter = self._retryCounter - 1
    else
        self:StopAction()
    end
end

--override
function ActionFindTeleportPosition:OnStart()
    if self._player and self._player.horse_grade_ride == 0 and self._needSetRide then
        local myPos = self._player:GetPosition()
        local dis = Vector2.Distance(Vector2(myPos.x, myPos.z), Vector2(self._targetPosition.x, self._targetPosition.z))
        if dis > 5 then
            PartnerManager:HorseRideReq()
        end
    end
    if self._player then
        self._player:StopAutoFight()
        self._player:StopMoveWithoutCallback()
    end
    -- LoggerHelper.Log("ActionFindPosition OnStart " .. PrintTable:TableToStr(self._targetPosition))
    self:DoMove()

    EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
    EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
end

--override
function ActionFindTeleportPosition:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
end

--override
function ActionFindTeleportPosition:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
end
