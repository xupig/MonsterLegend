local ActionInstChange = Class.ActionInstChange(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local InstanceManager = PlayerManager.InstanceManager
local GameSceneManager = GameManager.GameSceneManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper

function ActionInstChange:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._targetInstId = args.targetInstId
    self._player = args.player
    self._source = args.source
    
    self._onInstChangeFail = function()self:InstChangeFailCallback() end
    self._onInstChangeEnd = function()self:InstChangeEndCallback() end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionInstChange:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionInstChange:OnDo()

end

--请求切副本失败
function ActionInstChange:InstChangeFailCallback()
    -- LoggerHelper.Log("InstChangeFailCallback")
    self._isFail = true
    self:StopAction()
end

--请求切副本成功
function ActionInstChange:InstChangeEndCallback()
    -- LoggerHelper.Log("InstChangeEndCallback")
    if self._isFail == nil then
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionInstChange:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetInstId .. " " .. GameSceneManager:GetCurrMapID())
    local mapIds = InstanceDataHelper.GetInstanceMapIds(self._targetInstId)
    local curMapId = GameSceneManager:GetCurrMapID()
    for i, v in ipairs(mapIds) do
        if curMapId == v then
            return
        end
    end
    self._isFail = true
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
    EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    self:StopAction()
end

--切副本过程成功
function ActionInstChange:LoadGameSceneEndCallback()
    -- LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end

--override
function ActionInstChange:OnStart()
    -- LoggerHelper.Log("ActionInstChange OnStart " .. self._targetInstId .. " " .. GameSceneManager:GetCurrMapID())
    EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_ENTER_FAIL, self._onInstChangeFail)
    EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_ENTER_END, self._onInstChangeEnd)
    local mapIds = InstanceDataHelper.GetInstanceMapIds(self._targetInstId)
    local curMapId = GameSceneManager:GetCurrMapID()
    for i, v in ipairs(mapIds) do
        if curMapId == v then
            self:EndAction()
            return
        end
    end
    InstanceManager:OnInstanceEnter(self._targetInstId)
end

--override
function ActionInstChange:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_ENTER_FAIL, self._onInstChangeFail)
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_ENTER_END, self._onInstChangeEnd)
end

--override
function ActionInstChange:OnEnd()
    -- LoggerHelper.Log("ActionInstChange OnEnd " .. self._targetInstId)
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_ENTER_FAIL, self._onInstChangeFail)
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_ENTER_END, self._onInstChangeEnd)
end
