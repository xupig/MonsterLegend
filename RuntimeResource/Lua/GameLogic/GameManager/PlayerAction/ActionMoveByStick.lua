local ActionMoveByStick = Class.ActionMoveByStick(ClassTypes.ActionBase)

local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time

function ActionMoveByStick:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._player = args.player
    self._source = args.source
end

--override
function ActionMoveByStick:CanDo()
    return true
end

--override
function ActionMoveByStick:OnDo()

end

--override
function ActionMoveByStick:OnStart()
    self._player:StopMove()
end

--override
function ActionMoveByStick:OnFail()

end

--override
function ActionMoveByStick:OnEnd()

end
