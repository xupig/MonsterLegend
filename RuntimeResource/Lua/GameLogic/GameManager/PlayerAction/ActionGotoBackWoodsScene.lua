local ActionGotoBackWoodsScene = Class.ActionGotoBackWoodsScene(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local BackWoodsManager = PlayerManager.BackWoodsManager
local GameSceneManager = GameManager.GameSceneManager

function ActionGotoBackWoodsScene:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._bossId = args.bossId
    self._player = args.player
    self._targetMapId = args.targetMapId
    
    self._onBackwoodsEnter = function(flag)self:OnBackwoodsEnter(flag) end
    
    self._onLoadGameSceneStart = function()self:LoadGameSceneStartCallback() end
    self._onLoadGameSceneEnd = function()self:LoadGameSceneEndCallback() end
end

--override
function ActionGotoBackWoodsScene:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionGotoBackWoodsScene:OnDo()

end

function ActionGotoBackWoodsScene:OnBackwoodsEnter(flag)
    if not flag then
        self._isFail = true
        self:StopAction()
    else
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:AddEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
    end
end

--切副本过程失败
function ActionGotoBackWoodsScene:LoadGameSceneStartCallback()
    -- LoggerHelper.Log("LoadGameSceneStartCallback: " .. self._targetMapId .. " " .. GameSceneManager:GetCurrMapID())
    if self._targetMapId ~= GameSceneManager:GetCurrMapID() then
        self._isFail = true
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:StopAction()
    end
end

--切副本过程成功
function ActionGotoBackWoodsScene:LoadGameSceneEndCallback()
    -- LoggerHelper.Log("LoadGameSceneEndCallback")
    if self._isFail == nil then
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneStart, self._onLoadGameSceneStart)
        EventDispatcher:RemoveEventListener(GameEvents.OnLoadGameSceneFinish, self._onLoadGameSceneEnd)
        self:EndAction()
    end
end
--override
function ActionGotoBackWoodsScene:OnStart()
    -- LoggerHelper.Log("ActionGotoBackWoodsScene OnStart " .. self._bossId)
    EventDispatcher:AddEventListener(GameEvents.On_Backwoods_Enter, self._onBackwoodsEnter)
    if self._targetMapId == GameSceneManager:GetCurrMapID() then
        self:EndAction()
    else
        BackWoodsManager:OnBossEnter(self._bossId)
    end
end

--override
function ActionGotoBackWoodsScene:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.On_Backwoods_Enter, self._onBackwoodsEnter)
end

--override
function ActionGotoBackWoodsScene:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.On_Backwoods_Enter, self._onBackwoodsEnter)
end
