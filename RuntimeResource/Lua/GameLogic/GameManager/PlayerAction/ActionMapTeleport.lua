local ActionMapTeleport = Class.ActionMapTeleport(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local MapManager = PlayerManager.MapManager

function ActionMapTeleport:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._regionalMapId = args.regionalMapId
    self._player = args.player
    self._source = args.source
    
    self._onTeleportFail = function()self:TeleportFailCallback() end
    self._onTeleportEnd = function()self:TeleportEndCallback() end

    self._onHPChange = function (change)
        self:HPChange(change)
    end
end

--override
function ActionMapTeleport:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionMapTeleport:OnDo()

end

function ActionMapTeleport:TeleportFailCallback()
    --LoggerHelper.Log("TeleportFailCallback")
    self._isFail = true
    self:StopAction()
end

function ActionMapTeleport:TeleportEndCallback()
    --LoggerHelper.Log("TeleportEndCallback")
    if self._isFail == nil then
        self:EndAction()
    end
end

--override
function ActionMapTeleport:OnStart()
    --LoggerHelper.Log("ActionMapTeleport OnStart " .. PrintTable:TableToStr(self._targetPosition))
    if self._player then
        self._player:StopAutoFight()
        self._player:StopMoveWithoutCallback()
    end
    EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_TELEPORT_FAIL, self._onTeleportFail)
    EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_TELEPORT_END, self._onTeleportEnd)
    EventDispatcher:AddEventListener(GameEvents.PROPERTY_CUR_HP_CHANGE,self._onHPChange)
    MapManager:BeginTeleport(self._regionalMapId)
end


function ActionMapTeleport:HPChange(change)
    --玩家失血
    if change > self._player.cur_hp then
        self:OnFail()
    end
end

--override
function ActionMapTeleport:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_FAIL, self._onTeleportFail)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_END, self._onTeleportEnd)
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_CUR_HP_CHANGE,self._onHPChange)
end

--override
function ActionMapTeleport:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_FAIL, self._onTeleportFail)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_END, self._onTeleportEnd)
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_CUR_HP_CHANGE,self._onHPChange)
end
