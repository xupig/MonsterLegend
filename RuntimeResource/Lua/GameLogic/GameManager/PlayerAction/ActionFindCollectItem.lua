local ActionFindCollectItem = Class.ActionFindCollectItem(ClassTypes.ActionFindPosition)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig

function ActionFindCollectItem:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._source = args.source
    self._player = args.player
    self._endDistance = args.endDistance or 1
    self._targetPosition = args.pos
    --self._doMoveDistance = args.doMoveDistance or self._endDistance
end

--override
function ActionFindCollectItem:CanDo()
    if self._targetPosition == nil then
        return false
    end
    local result = self._base.CanDo(self)
    return result
end

--override
function ActionFindCollectItem:OnEnd()
    self._base.OnEnd(self)
    TimerHeap:AddSecTimer(1, 0, 0, function()EventDispatcher:TriggerEvent(GameEvents.START_COLLECT_ITEM) end)
    
end