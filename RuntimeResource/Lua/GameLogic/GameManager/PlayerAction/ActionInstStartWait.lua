local ActionInstStartWait = Class.ActionInstStartWait(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local GameSceneManager = GameManager.GameSceneManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local public_config = GameWorld.public_config

function ActionInstStartWait:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self._targetInstId = args.targetInstId
    self._onInstStageChange = function(data)self:OnInstStageChange(data) end
    self._source = args.source
end

--override
function ActionInstStartWait:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionInstStartWait:OnStart()
    EventDispatcher:AddEventListener(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, self._onInstStageChange)
    local mapIds = InstanceDataHelper.GetInstanceMapIds(self._targetInstId)
    local curMapId = GameSceneManager:GetCurrMapID()
    for i, v in ipairs(mapIds) do
        if curMapId == v then            
            return
        end
    end
    self:StopAction()
end

function ActionInstStartWait:OnInstStageChange(data)
    -- {}
    -- [1] = room_stage,   --房间状态
    -- [2] = start_time,   --状态开始时间
    local type = data[1]
    if type == public_config.INSTANCE_STAGE_4 then
        self:EndAction() 
    end
end

--override
function ActionInstStartWait:OnEnd()
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, self._onInstStageChange)
-- LoggerHelper.Log("ActionInstStartWait OnEnd " .. PrintTable:TableToStr(self._targetPosition))
end

--override
function ActionInstStartWait:OnFail()
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, self._onInstStageChange)
end
