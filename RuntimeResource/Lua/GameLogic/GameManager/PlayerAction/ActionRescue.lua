local ActionRescue = Class.ActionRescue(ClassTypes.ActionBase)

local ControlStickState = GameConfig.ControlStickState
local PlayerActionConfig = GameConfig.PlayerActionConfig
local Time = UnityEngine.Time
local CD_INTERVAL = 1000
local ReviveRescueManager = GameManager.ReviveRescueManager

function ActionRescue:__ctor__(args)
    self.priority = PlayerActionConfig.PRIORITY_ONE
    self.canBreak = true
    self.duration = args.duration
    self._targetPosition = args.targetPosition
    self._player = args.player
    self._source = args.source
    --self._endActionCB = args.endActionCB
    self._playerEid = args.playerEid
    self._cdRemainTime = args.cdtime

    self._onHPChange = function (change)
        self:HPChange(change)
    end
end

--override
function ActionRescue:CanDo()
    if ControlStickState.controlStickStrength > 0 then
        return false
    end
    return true
end

--override
function ActionRescue:OnDo()

end

--override
function ActionRescue:OnStart()
    self._intervalTimerID = TimerHeap:AddTimer(0, CD_INTERVAL, 0, 
        function()
            self._cdRemainTime = self._cdRemainTime - CD_INTERVAL
            if self._cdRemainTime <= 0  and self.state == PlayerActionConfig.ACTION_DOING then
                self:EndAction()
            end
        end
    )
    EventDispatcher:AddEventListener(GameEvents.PROPERTY_CUR_HP_CHANGE,self._onHPChange)
end

function ActionRescue:HPChange(change)
    --玩家失血
    if change > GameWorld.Player().cur_hp then
        self:OnFail()
    end
end

--override
function ActionRescue:OnFail()
    --LoggerHelper.Error("ActionRescue:OnFail")
    TimerHeap:DelTimer(self._intervalTimerID)
    ReviveRescueManager:StopRescue()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP,self._onHPChange)
end

--override
function ActionRescue:OnEnd()
    --LoggerHelper.Error("ActionRescue:OnEnd")
    TimerHeap:DelTimer(self._intervalTimerID)
    ReviveRescueManager:RequestRescue()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP,self._onHPChange)
end