-- GameMgrs.lua
-- 这里提供统一接口，为逻辑层提供大部分manager的获取
GameManager = {}
GameManager._mgrs = {}

local mt={		   
			  __index = function(t, k)
                    local mgr = t._mgrs[k]
                    if (mgr == nil) then
                        mgr = require("GameManager."..k)
                        t._mgrs[k] = mgr
                    end 
					return mgr
			   end
			  }
setmetatable(GameManager, mt)

