local BaseScene = Class.BaseScene(ClassTypes.XObject)

local ClassTypes = ClassTypes
local SpaceData = ClassTypes.SpaceData
local MapDataHelper = GameDataHelper.MapDataHelper
local MapFamilyDataHelper = GameDataHelper.MapFamilyDataHelper
local FileAccessManager = GameLoader.IO.FileAccessManager
local DATA_SPACE = "data/spaces/"
local XML_SUFFIX = ".xml"
local XMLParser = GameWorld.XMLParser
local SceneConfig = GameConfig.SceneConfig
local SceneMgr = MogoEngine.Mgrs.SceneMgr
local LoadSceneSetting = MogoEngine.Mgrs.LoadSceneSetting
local ProgressBar = GameWorld.ProgressBar
local RenderSettings = UnityEngine.RenderSettings
local StringBoolMap = System.Collections.Generic.StringBoolMap
local _lastSpaceData = nil
local _lastMapId = 0
local _lastFlyToScene = 0
-- local PreloadManager = GameManager.PreloadManager
local LoadingBarManager = GameManager.LoadingBarManager
local NPCConfig = GameConfig.NPCConfig
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local EntityType = GameConfig.EnumType.EntityType
local GUIManager = GameManager.GUIManager
local NPCDataHelper = GameDataHelper.NPCDataHelper
local GameSceneManager

function BaseScene:__ctor__()
    self._mapId = 0
    self._instanceId = 0
    self._enterPos = Vector3.zero
    self._loadCallback = nil
    self._spaceName = nil
    self._spaceData = nil
    self._inLoading = false
    self._isUpdateProgress = true
    self._inTheSameMapName = false
    self._npcs = {}
    self._monsters = {}
    self._tempGameObject = {}
    self.waitForDelFx = {}
    self._dramaEntities = {}
    GameSceneManager = GameManager.GameSceneManager
end

function BaseScene:Enter(mapId, enterPos, loadCallback)
    self._mapId = mapId
    self._enterPos = enterPos
    self._loadCallback = loadCallback
    self._instanceId = InstanceDataHelper.GetInstanceIdByMapId(self._mapId)
    self:InitSceneTime()
    self:LoadSpaceData()
    self:LoadScene()
end

function BaseScene:Leave()
    _lastMapId = self._mapId
    _lastSpaceData = self._spaceData
    self:Clean()
    EventDispatcher:TriggerEvent(GameEvents.LEAVE_MAP, self._mapId)
end

function BaseScene:GetLastMapId()
    return _lastMapId
end

function BaseScene:Clean()
    
    self._npcs = {}
    for key, table in pairs(self._tempGameObject) do
        for pos, go in pairs(table) do
            GameWorld.Destroy(go)
        end
    end
    self.tempGameObject = {}
    self.waitForDelFx = {}
end

function BaseScene:InitSceneTime()
--Day or Night
end

function BaseScene:LoadSpaceData()
    --GameWorld.XmlParser
    self._spaceName = self:GetSpaceName()
    --self._spaceName = "copy_f01"
    local path = DATA_SPACE .. self._spaceName .. XML_SUFFIX
    local xmlContent = FileAccessManager.LoadText(path)
    local cfg = GameUtil.TypeUtil.GetTable(xmlContent)
    --LoggerHelper.Log("xmlContent::", path, xmlContent)
    self._spaceData = SpaceData.CreateSpaceData(cfg)
    --LoggerHelper.Log("parseXml::", self._spaceData.root.entities.entity[1].id_i:value())
    self:ResetLoadingAmbientLight(self._spaceData)
end


function BaseScene:SetFlyToScene(targetScene)
    _lastFlyToScene = targetScene
end

function BaseScene:LoadScene()
    local loadSceneSetting = self:GenerateLoadSceneSetting()
    if self:InTheSameMapName(self._spaceData.sceneName) then
        loadSceneSetting.destroyOldSceneObject = false
        loadSceneSetting.resetAllScenes = false
    else
        loadSceneSetting.destroyOldSceneObject = true
    end
    local isTeleport = true
    if _lastFlyToScene ~= 0 then
        isTeleport = false
        _lastFlyToScene = 0
    else
        isTeleport = loadSceneSetting.resetAllScenes
    end
    
    -- if self._instanceId ~= 0 and InstanceDataHelper.GetInstanceKind(self._instanceId) == 1 then
    --     isTeleport = false
    -- end
    -- loadSceneSetting.destroyOldSceneObject = self:InTheSameMapName(self._spaceData.sceneName) == false
    -- LoggerHelper.Error("loadSceneSetting.destroyOldSceneObject: " .. tostring(loadSceneSetting.destroyOldSceneObject))
    self:OnLoadGameSceneStart(isTeleport)
    --GameObjectPoolManager.GetInstance().Clear();
    --if (loadSceneSetting.clearSceneMemory) ACTVisualFXManager.GetInstance().ReleaseACTVisualFx();
    --LoggerHelper.Error("BaseScene:LoadScene")
    GameWorld.LoadScene(loadSceneSetting,
        function()
            --LoadingBarManager:WriteLog("Load scene callback: ")
            -- if self._isUpdateProgress then
            --     ProgressBar:UpdateProgress(0.82, false);
            -- end
            -- PreloadManager:BeforeEnterScene(function()
            self:OnLoadGameSceneFinish(isTeleport)
            GameWorld.PauseHandleData(false)
            if self._loadCallback ~= nil then
                self._loadCallback(isTeleport)
            end
            -- end)
            --LoggerHelper.Error("BaseScene:LoadScene2")
            GameWorld.LoadBlockMap(loadSceneSetting.sceneName)
            self:LoadBgMusic()
        end)
end

function BaseScene:GetSpaceName()
    local spaceName = MapDataHelper.GetSpaceName(self._mapId)
    if spaceName == nil then
        LoggerHelper.Error(string.format("mapid %i can not find spaceName!", self._mapId))
    end
    return spaceName
end

function BaseScene:ResetLoadingAmbientLight(spaceData)
    SceneMgr.defaultAmbientLight = self._spaceData:GetRenderSetting().ambientLight
end

function BaseScene:IsShowProgressBar()
    local t = GameSceneManager:GetCurSceneType()
    if self._isUpdateProgress then
        self._inTheSameMapName = self:InTheSameMapName(self._spaceData.sceneName)
        if self._inTheSameMapName then
            return false
        else
            if t == SceneConfig.SCENE_TYPE_LADDER_1V1 and not GameWorld.Player().isWatchWar then
                return false
            end
        end
        return true
    end
    return false
end

function BaseScene:OnLoadGameSceneStart(isResetAllScenes)
    self._inLoading = true
    GameWorld.StopAllBgMusic()
    -- self:SetLoadingMode()
    if self:IsShowProgressBar() then
        LoadingBarManager:SetShow(true)
    else
        LoadingBarManager:SetShow(false)
    end
    self:ResetAOI()
    EventDispatcher:TriggerEvent(GameEvents.OnLoadGameSceneStart, isResetAllScenes)
    local t = GameSceneManager:GetCurSceneType()
    LoadingBarManager:OnLoadGameSceneStart(t)
    if MapDataHelper.GetSceneType(self._mapId) ~= MapDataHelper.GetSceneType(_lastMapId) then
        EventDispatcher:TriggerEvent(GameEvents.ON_SCENE_TYPE_CHANGE)
    end
    collectgarbage("collect")
end

function BaseScene:ResetAOI()
    local inServerIns = GameSceneManager:InServerIns()
    GameSceneManager:SetAOIState(inServerIns)
end

function BaseScene:OnLoadGameSceneFinish(isResetAllScenes)
    self._inLoading = false
    --self:CreateEntityAction()
    --ProgressBar:Close()
    LoadingBarManager:OnLoadGameSceneFinish()
    self:ResetRenderSetting()
    if self._enterPos == Vector3.zero then
        self._enterPos = self._spaceData:GetEnterPos()
    end
    --设置位置同步状态
    local inServerIns = GameSceneManager:InServerIns()
    GameWorld.SetPlayerSyncPosState(inServerIns)
    if GameWorld.Player() ~= nil then
        GameWorld.Player():Teleport(self._enterPos)
    end
    GameWorld.PauseAllSynPos(false)
    --进入场景时触发的场景事件
    --任务系统需要先刷机关再触发enter_map，暂时将SpaceEventManager:EnterMap放前面
    GameManager.SpaceEventManager:EnterMap(self._spaceData)
    EventDispatcher:TriggerEvent(GameEvents.OnLoadGameSceneFinish, isResetAllScenes)
    EventDispatcher:TriggerEvent(GameEvents.ENTER_MAP, self._mapId)
    EventDispatcher:TriggerEvent(GameEvents.ENTER_MAP_FROM_LAST_MAP, _lastMapId)
    local curSceneType = GameSceneManager:GetCurSceneType()
    if curSceneType == SceneConfig.SCENE_TYPE_COMBAT then
        EventDispatcher:TriggerEvent(GameEvents.INSTANCE_START)
    end
    
    local player = GameWorld.Player()
    if player ~= nil then
        if player.entityType == EntityType.PlayerAvatar then
            player.server.pickle_aoi_req()
        end
    end
    
    GameWorld.SetSceneLibra(MapDataHelper.IsLibra(self._mapId))
    local lastMaxCount = MapDataHelper.GetMaxCount(_lastMapId)
    local curMaxCount = MapDataHelper.GetMaxCount(self._mapId)
    local lastSceneType = MapDataHelper.GetSceneType(_lastMapId)
    if lastMaxCount ~= 1 and curMaxCount ~= 1 and
        (curSceneType == SceneConfig.SCENE_TYPE_MAIN or curSceneType == SceneConfig.SCENE_TYPE_WILD) and
        (lastSceneType == SceneConfig.SCENE_TYPE_MAIN or lastSceneType == SceneConfig.SCENE_TYPE_WILD) then
        local mapChinese = MapDataHelper.GetChinese(self._mapId)
        if mapChinese ~= 0 then
            GUIManager.ShowMapName(LanguageDataHelper.GetLanguageData(mapChinese))
        end
    end
-- LoadingBarManager:WriteLog("Load scene end: ")
end

-------CG Begin------------------
function BaseScene:CreateDramaAvatar(id, tid, pos, face, isBorn)
    local m = RoleDataHelper.GetModelByVocation(tid)
    local p = {}
    p.drama_id = id
    p.spawn_id = -1
    p.isDramaEntity = true
    p.pk_value = 0
    p.title_id = 0
    p.name = GameWorld.avatarData.name
    p.vocation = GameWorld.avatarData.vocation
    p.facade = GameWorld.avatarData.facade
    p.fight_force = GameWorld.avatarData.fight_force
    p.fight_idle_state = 1
    local avatar = GameWorld.CreateEntity("Avatar", p)
    avatar:SetFace(face)
    local point = Vector3(pos[1], pos[2], pos[3])
    avatar:Teleport(point)
    self._dramaEntities[id] = avatar
end

function BaseScene:CreateDramaDummy(id, tid, pos, face, isBorn)
    local m = MonsterDataHelper.GetMonster(tid)
    local p = {}
    p.monster_id = tid
    p.drama_id = id
    p.spawn_id = -1
    p.isDramaEntity = true
    local monster = GameWorld.CreateEntity("Dummy", p)
    monster:SetFace(face)
    local point = Vector3(pos[1], pos[2], pos[3])
    monster:Teleport(point)
    self._dramaEntities[id] = monster
end

function BaseScene:CreateDramaNPC(id, tid, pos, face, isBorn)
    face = face or 0
    local p = {}
    local npcData = NPCDataHelper.GetNPCData(tid)
    p.npc_id = tid
    p.drama_id = id
    p.from = NPCConfig.FROM_DRAMA
    p.face = Vector3.New(0, face * 0.5, 0)
    p.isDramaEntity = true
    p.isStatic = false
    local npc = GameWorld.CreateEntity("NPC", p)
    local point = Vector3(pos[1], pos[2], pos[3])
    npc:Teleport(point)
    self._dramaEntities[id] = npc
    if isBorn == 1 then end
end

function BaseScene:DramaEntityCompleted(id)
    local e = self._dramaEntities[id]
    if e == nil then
        return false
    end
    return e.actorHasLoaded
end

function BaseScene:DestroyDramaEntity(id)
    local entity = self._dramaEntities[id]
    if entity ~= nil then
        GameWorld.DestroyEntity(entity.id)
    end
    self._dramaEntities[id] = nil
end

function BaseScene:DestroyDramaEntities()
    for k, v in pairs(self._dramaEntities) do
        GameWorld.DestroyEntity(v.id)
    end
    self._dramaEntities = {}
end

function BaseScene:RotateDramaEntity(id, rotate)
    local entity = self._dramaEntities[id]
    if entity ~= nil then
        entity:SetFace(rotate[2])
    end
end

function BaseScene:TeleportDramaEntity(id, pos)
    local entity = self._dramaEntities[id]
    if entity ~= nil then
        entity:Teleport(pos)
    end
end

function BaseScene:GetDramaEntityByID(id)
    return self._dramaEntities[id]
end

function BaseScene:IsDramaEntityByEID(eid)
    for k, v in pairs(self._dramaEntities) do
        if eid == v.id then
            return true
        end
    end
    return false
end

function BaseScene:DramaPlayUIFX(path, pos, time)

end

function BaseScene:DramaPlayVisualFX(path, pos, rotation, time)
    GameWorld.GetGameObject(path, function(go)
        if go == nil then
            LoggerHelper.Error("path not found " .. path)
            return
        end
        go.transform.position = pos
        go.transform.localEulerAngles = rotation
        if time ~= -1 then
            GameWorld.TimerHeap:AddTimer(time, 0, 0, function()GameWorld.Destroy(go) end)
        end
    end)
end

function BaseScene:DramaStopAI(t, range, args)
    local entities = GameWorld.Entities()
    for k, v in pairs(entities) do
        if v:is_a(ClassTypes.Dummy) then
            v:TempStopThink(true)
        end
    end
end

function BaseScene:DramaStartAI(t, range, args)
    local entities = GameWorld.Entities()
    for k, v in pairs(entities) do
        if v:is_a(ClassTypes.Dummy) then
            v:TempStopThink(false)
        end
    end
end

function BaseScene:DramaShowNPC(visible)
    self._dramaNpcShow = (visible == 1)
    for k, v in pairs(self._npcs) do
        v:DramaShow(self._dramaNpcShow)
    end
    GameWorld.Player():DramaShow(self._dramaNpcShow)
end
-------CG End------------------
-- function BaseScene:ProcSceneAction(id)
--     local a = self._spaceData:GetActionByID(id)
--     if a == nil then
--         LoggerHelper.Error("not config actiond id " .. id)
--         do return end
--     end
--     EventDispatcher:TriggerEvent(GameEvents.ProcSceneAction, id)
-- end
function BaseScene:TeleportScene(target, posIDs)
    local point = self._spaceData:GetActionByID(posIDs[1])
    TimerHeap:AddTimer(50, 0, 0,
        function()
            GameSceneManager:EnterSceneByID(target, false, 0, nil,
                function()
                    GameWorld.Player():Teleport(point.pos)
                    GameWorld.Player():SetBehaviorTree(2)
                    GameWorld.Player().entityAI:SetThinkState(false)
                end)
        end)
end

function BaseScene:CreateDummy(id, posID, spawn_id)
    local m = MonsterDataHelper.GetMonster(id)
    if GameSceneManager:InServerIns() then
        do return end
    end
    local p = {}
    p.monster_id = id
    p.spawn_id = spawn_id
    local monster = GameWorld.CreateEntity("Dummy", p)
    local point = self._spaceData:GetActionByID(posID)
    monster:SetFace(point.rotation_pos.y)
    monster:Teleport(point.pos)
end

function BaseScene:CreateStaticMonster(id, posID, actionId)
    local m = MonsterDataHelper.GetMonster(id)
    local p = {}
    p.monster_id = id
    p.action_id = actionId
    local monster = GameWorld.CreateEntity("StaticMonster", p)
    local point = self._spaceData:GetActionByID(posID)
    monster:SetFace(point.rotation_pos.y)
    monster:Teleport(point.pos)
end

function BaseScene:ActionFaction(target_type, id, faction_id)

end
function BaseScene:CreateNPC(id, posID)
    -- GameWorld.LoggerHelper.Log("CreateNPC: " .. id)
    local m = nil
    local npc
    if self._npcs[id] then
        npc = self._npcs[id]
    else
        local p = {}
        p.npc_id = id
        local npcData = NPCDataHelper.GetNPCData(id)
        npc = GameWorld.CreateEntity("NPC", p)
        self._npcs[id] = npc
    end
    
    local point = self._spaceData:GetActionByID(posID)
    npc:SetFace(point.rotation_pos.y)
    npc:Teleport(point.pos)
end

--创建客户端采集点
function BaseScene:CreateClientResourcePoint(actionCfg)
    --GameWorld.LoggerHelper.Log("CreateClientResourcePoint: " .. id)
    local p = {}
    local pos = actionCfg.pos
    p.actionId = actionCfg.id
    p.taskEventId = actionCfg.task_event_id
    p.cdTime = actionCfg.cd_time
    p.destroyAfterCollect = actionCfg.destroy_after_collect
    p.collectName = actionCfg.collect_point_name
    p.collectIcon = actionCfg.collect_btn_icon
    p.collectActionArgs = actionCfg.collect_action_args
    local clientResourcePoint = GameWorld.CreateEntity("ClientCollectPoint", p)
    clientResourcePoint:Teleport(pos)
    clientResourcePoint:SetEuler(actionCfg.rotation_pos)
end

function BaseScene:PlayFX(id, posID, path)
    local point = self._spaceData:GetActionByID(posID)
    if point then
        GameWorld.GetGameObject(path, function(go)
            if go == nil then
                LoggerHelper.Error("BaseScene:PlayFX Error" .. path)
                return
            end
            if self.waitForDelFx[id] then
                --self.waitForDelFx[id] = false
                GameWorld.Destroy(go)
                return
            end
            go.transform.position = point.pos
            go.transform.localEulerAngles = point.rotation_pos
            
            if self._tempGameObject[id] == nil then
                self._tempGameObject[id] = {}
            end
            self._tempGameObject[id][posID] = go
        end)
    end
end

function BaseScene:DelFX(id, fx_event_id, path)
    if self._tempGameObject[fx_event_id] == nil then
        self.waitForDelFx[fx_event_id] = true
        return
    end
    for posID, go in pairs(self._tempGameObject[fx_event_id]) do
        if go ~= nil then
            GameWorld.Destroy(go)
        end
    end
    
    self._tempGameObject[fx_event_id] = nil
end

function BaseScene:RefreshNPCState()
    for k, v in pairs(self._npcs) do
        v:ShowTask()
    end
end

function BaseScene:GetNPCs()
    return self._npcs
end

function BaseScene:GetSceneName()
    return self._spaceData.sceneName
end

function BaseScene:GetInLoading()
    return self._inLoading
end

function BaseScene:InTheSameMapName(newMapName)
    return _lastSpaceData ~= nil and (_lastSpaceData.sceneName == newMapName)
end

function BaseScene:GenerateLoadSceneSetting()
    local loadSceneSetting = LoadSceneSetting.New()
    loadSceneSetting.clearSceneMemory = true;
    loadSceneSetting.destroyOldSceneObject = true;
    loadSceneSetting.canCache = false;
    if self._inTheSameMapName then
        loadSceneSetting.destroyOldSceneObject = false;
        loadSceneSetting.clearSceneMemory = false;
        loadSceneSetting.updateProgress = false;
    end
    --if IsWildOrCity(self._mapId) then loadSceneSetting.canCache = true end
    --if (IsWildOrCity(_lastMapId) || (_lastMapId == 0 && _mapId != 0)) loadSceneSetting.clearSceneMemory = false;
    loadSceneSetting.sceneName = self._spaceData.sceneName;
    local pdata = StringBoolMap.New()
    pdata:Add(self._spaceData.mapPath, true)
    loadSceneSetting.prefabsData = pdata;
    loadSceneSetting.sceneTime = 0;
    loadSceneSetting.lightmapPaths = self._spaceData:GetLightmapPaths();
    loadSceneSetting.lightProbesName = "";
    
    local settings = self._spaceData:GetRenderSetting()
    loadSceneSetting.skybox = settings.skybox
    loadSceneSetting.staticPrefabs = settings.staticPrefabs
    loadSceneSetting.useUnity = settings.useUnity
    self._isUpdateProgress = true
    
    local type = MapDataHelper.GetSceneType(self._mapId)
    if type == SceneConfig.SCENE_TYPE_LOGIN or type == SceneConfig.SCENE_TYPE_CHARACTOR then
        loadSceneSetting.resetAllScenes = true
    else
        loadSceneSetting.resetAllScenes = false
    end
    --if type == SceneConfig.SCENE_TYPE_MAIN or type == SceneConfig.SCENE_TYPE_WILD then
    --    loadSceneSetting.canCache = true
    --end
    
    -- local curMapFamily = MapFamilyDataHelper:GetMapFamilyIdBySpaceName(loadSceneSetting.sceneName)
    -- -- LoggerHelper.Log("Ash: curMapFamily: " .. tostring(curMapFamily) .. " - " .. tostring(loadSceneSetting.sceneName))
    -- if curMapFamily ~= nil then
    --     local blockId = MapFamilyDataHelper:GetBlock(curMapFamily)
    --     local mapFamilyIds = MapFamilyDataHelper:GetMapFamilyIdsByBlock(curMapFamily)
    --     local sceneNames = {}
    --     for i, v in ipairs(mapFamilyIds) do
    --         if MapFamilyDataHelper:GetType(v) == 3 then -- 1野外 2副本 3过道
    --             table.insert(sceneNames, MapFamilyDataHelper:GetSpaceName(v))
    --         end
    --     end
    --     loadSceneSetting.addtiveSceneNames = sceneNames
    --     -- LoggerHelper.Log("Ash: blockId: " .. tostring(blockId) .. " - " .. tostring(GameWorld.LastBlock))
    --     if blockId == GameWorld.LastBlock then
    --         self._isUpdateProgress = false
    --         loadSceneSetting.resetAllScenes = false
    --     end
    --     GameWorld.LastBlock = blockId
    -- else
    --     -- LoggerHelper.Log("Ash: GameWorld.LastBlock = 0---")
    --     GameWorld.LastBlock = 0
    -- end
    loadSceneSetting.updateProgress = self._isUpdateProgress;
    -- LoggerHelper.Log("Ash: _lastBlock: " .. tostring(GameWorld.LastBlock) .. " - " .. tostring(self._isUpdateProgress))
    return loadSceneSetting
end

function BaseScene:ResetCamera()

end

function BaseScene:SetLoadingMode()
    local settings = self._spaceData:GetRenderSetting()
    local mapFamilyId = MapFamilyDataHelper:GetMapFamilyIdBySpaceName(self._spaceData.sceneName)
    if mapFamilyId ~= nil and MapFamilyDataHelper:GetType(mapFamilyId) == 1 then -- 1野外 2副本 3过道
        GameWorld.ChangeCameraFarClipPlane(20, 0)
        GameWorld.ChangeFogStartDistance(15, 0)
        GameWorld.ChangeFogEndDistance(20, 0)
    else
        RenderSettings.fog = settings.is_path_fog
        if settings.is_path_fog then
            RenderSettings.fogMode = settings.path_fogMode
            RenderSettings.fogColor = settings.path_fogColor
            RenderSettings.ambientLight = settings.path_ambientLight
        end
        if settings.path_cameraFar ~= nil then
            GameWorld.ChangeCameraFarClipPlane(settings.path_cameraFar, 0)
        end
        if settings.path_fogStartDistance ~= nil then
            GameWorld.ChangeFogStartDistance(settings.path_fogStartDistance, 0)
        end
        if settings.path_fogEndDistance ~= nil then
            GameWorld.ChangeFogEndDistance(settings.path_fogEndDistance, 0)
        end
    end
end

function BaseScene:ResetRenderSetting(isTween)
    local settings = self._spaceData:GetRenderSetting()
    RenderSettings.fog = settings.isFog
    RenderSettings.fogMode = settings.fogMode
    RenderSettings.fogColor = settings.fogColor
    RenderSettings.ambientLight = settings.ambientLight
    GameWorld.ChangeCameraColor(settings.fogColor.r, settings.fogColor.g, settings.fogColor.b)
    local tweenTime = 0
    -- if isTween then
    --     tweenTime = 5
    -- end
    GameWorld.ChangeCameraFarClipPlane(settings.cameraFar, tweenTime)
    GameWorld.ChangeCameraNearClipPlane(settings.cameraNear, tweenTime)
    GameWorld.ChangeFogStartDistance(settings.fogStartDistance, tweenTime)
    GameWorld.ChangeFogEndDistance(settings.fogEndDistance, tweenTime)
end

function BaseScene:GetCfgEntity(id)
    return self._spaceData:GetActionByID(id)
end

function BaseScene:ResetFog()
--Fog_FarPlane
end

-- function BaseScene:GetKillNum()
--     return GameManager.SpaceEventManager:GetKillNum()
-- end
function BaseScene:LoadBgMusic()
    local bgMusicId = MapDataHelper.GetMusic(self._mapId)
    if bgMusicId == 0 then return end
    if bgMusicId == -1 then
        GameWorld.StopAllBgMusic()
    else
        GameWorld.PlayBgMusic(bgMusicId, false, true)
    end
end

function BaseScene:ActionTimeComplete(eventID)
    local events = self._spaceData:GetTimeCompleteActions()
    local actives = {}
    for k, v in pairs(events) do
        if v.event_id == eventID then
            table.insert(actives, v)
        end
    end
    for k1, v1 in pairs(actives) do
        EventDispatcher:TriggerEvent(GameEvents.ProcSceneAction, v1.id)
    end
end

function BaseScene:GetPathPoints()
    return self._spaceData:GetPathPoints()
end

function BaseScene:GetPathPointSeq()
    return self._spaceData:GetPathPointSeq()
end

local LoginScene = Class.LoginScene(ClassTypes.BaseScene)
local CharactorScene = Class.CharactorScene(ClassTypes.BaseScene)
local MainScene = Class.MainScene(ClassTypes.BaseScene)
local WildScene = Class.WildScene(ClassTypes.BaseScene)
local CombatScene = Class.CombatScene(ClassTypes.BaseScene)
local MainGlide = Class.MainGlide(ClassTypes.BaseScene)
local PVP6Scene = Class.PVP6Scene(ClassTypes.BaseScene)
local PVPNewScene = Class.PVPNewScene(ClassTypes.BaseScene)
local CommonBattleScene = Class.CommonBattleScene(ClassTypes.BaseScene)
local HeroBattleScene = Class.HeroBattleScene(ClassTypes.BaseScene)
local HellBattleScene = Class.HellBattleScene(ClassTypes.BaseScene)
local RaidBattleScene = Class.RaidBattleScene(ClassTypes.BaseScene)
local MirrorScene = Class.MirrorScene(ClassTypes.BaseScene)
local WildMirrorScene = Class.WildMirrorScene(ClassTypes.BaseScene)
local FightFriendScene = Class.FightFriendScene(ClassTypes.BaseScene)
local Ladder1V1Scene = Class.Ladder1V1Scene(ClassTypes.BaseScene)
local GuildManorScene = Class.GuildManorScene(ClassTypes.BaseScene)
local AionTowerScene = Class.AionTowerScene(ClassTypes.BaseScene)
local GuildRaidScene = Class.GuildRaidScene(ClassTypes.BaseScene)
local WorldBossScene = Class.WorldBossScene(ClassTypes.BaseScene)
local ExpInstanceScene = Class.ExpInstanceScene(ClassTypes.BaseScene)
local AbyssScene = Class.AbyssScene(ClassTypes.BaseScene)
local PetInstanceScene = Class.PetInstanceScene(ClassTypes.BaseScene)
local BossHomeScene = Class.BossHomeScene(ClassTypes.BaseScene)
local MyBossScene = Class.MyBossScene(ClassTypes.BaseScene)
local GoldInstanceScene = Class.GoldInstanceScene(ClassTypes.BaseScene)
local EquipmentInstanceScene = Class.EquipmentInstanceScene(ClassTypes.BaseScene)
local ForbiddenInstanceScene = Class.ForbiddenInstanceScene(ClassTypes.BaseScene)
local TopAthleticsScene = Class.TopAthleticsScene(ClassTypes.BaseScene)
local GuildMonsterScene = Class.GuildMonsterScene(ClassTypes.BaseScene)
local CloudPeakScene = Class.CloudPeakScene(ClassTypes.BaseScene)
local GuardGoddnessScene = Class.GuardGoddnessScene(ClassTypes.BaseScene)
local GuildBanquetScene = Class.GuildBanquetScene(ClassTypes.BaseScene)
local GuildConquestScene = Class.GuildConquestScene(ClassTypes.BaseScene)
local GodMonsterIslandScene = Class.GodMonsterIslandScene(ClassTypes.BaseScene)
local TowerDefenseScene = Class.TowerDefenseScene(ClassTypes.BaseScene)
local WeddingScene = Class.WeddingScene(ClassTypes.BaseScene)
local AnswerScene = Class.AnswerScene(ClassTypes.BaseScene)
local MarryInstanceScene = Class.MarryInstanceScene(ClassTypes.BaseScene)
local BattleSoulScene = Class.BattleSoulScene(ClassTypes.BaseScene)
local WORLD_BOSS_FAKEScene = Class.WORLD_BOSS_FAKEScene(ClassTypes.BaseScene)
local AncientBattleScene = Class.AncientBattleScene(ClassTypes.BaseScene)

function MainScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function CombatScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function WildScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function PVP6Scene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function PVPNewScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
    EventDispatcher:TriggerEvent(GameEvents.SCENE_ENTER_PVPNEW)
end

function PVPNewScene:Leave()
    self._base.Leave(self)
    EventDispatcher:TriggerEvent(GameEvents.SCENE_LEAVE_PVPNEW)
end

function CommonBattleScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function HeroBattleScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function HellBattleScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function RaidBattleScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function MirrorScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function WildMirrorScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function FightFriendScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function Ladder1V1Scene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuildManorScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function AionTowerScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuildRaidScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function WorldBossScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function ExpInstanceScene:OnLoadGameSceneFinish()
    GameWorld.Player():StartAutoFight()
    self._base.OnLoadGameSceneFinish(self)
end

function MarryInstanceScene:OnLoadGameSceneFinish()
    GUIManager.ClosePanel(PanelsConfig.Team)
    self._base.OnLoadGameSceneFinish(self)
end

function ExpInstanceScene:Leave()
    self._base.Leave(self)
end

function WORLD_BOSS_FAKEScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function AbyssScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function PetInstanceScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function BossHomeScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function MyBossScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GoldInstanceScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function EquipmentInstanceScene:OnLoadGameSceneFinish()
    GUIManager.ClosePanel(PanelsConfig.Team)
    self._base.OnLoadGameSceneFinish(self)
end

function ForbiddenInstanceScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function TopAthleticsScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuildMonsterScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function CloudPeakScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuardGoddnessScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuildBanquetScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function GuildConquestScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
    EventDispatcher:TriggerEvent(GameEvents.Guild_Conquest_Scene_Load_Finish)
end

function GodMonsterIslandScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function TowerDefenseScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function WeddingScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function WeddingScene:Leave()
    self._base.Leave(self)
    GUIManager.ClosePanel(PanelsConfig.Collect)
end

function AnswerScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end

function AncientBattleScene:OnLoadGameSceneFinish()
    self._base.OnLoadGameSceneFinish(self)
end
