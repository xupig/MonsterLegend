local SpaceData = Class.SpaceData(ClassTypes.XObject)

local IntStringArrayMap = System.Collections.Generic.IntStringArrayMap
local FogMode = UnityEngine.FogMode

function SpaceData:__ctor__(cfg)
    self.cfg = cfg
    self.sceneName = self.cfg.global_params.mapname
    self.mapPath = string.format("Scenes/%s/%s.prefab", self.sceneName, self.sceneName);
    self._lightmapPaths = nil
    self._renderSetting = nil
    self._entities = {}
    self._triggers = {}
    self._enterActions = {}
    self._endActions = {}
    self._breakActions = {}
    self._initActions = {}
    self._eventActions = {}
    self._monsterDeathActions = {}
    self._blockActions = {}
    self._timeComplete = {}
    self._cameraWalls = {}
    self._stateAction = {}
    self._pathPoints = {}
    self._pathPointSeq = {}
    self._cgComplete = {}

    self:InitActions()
    self:InitPathPointSeq()
end

function SpaceData:InitActions()
    for k, v in pairs(self.cfg.entities) do
        local d = ClassTypes[v.type](v)
        self._entities[v.id_i] = d
        if v.type == "TriggerRegion" or GameManager.SpaceEventManager.TriggerMap[v.type] then
            --print("SpaceData:InitActions 1 !!!!! ",v.id_i,v.type,type(v.id_i))
            self._triggers[v.id_i] = d
            if v.type == "EntityPathPoint" then
                self._pathPoints[v.id_i] = d
            end
        elseif v.type == "EntityInstanceInit" then
            self._initActions[v.id_i] = d
        elseif v.type == "EntityInstanceEnd" then
            self._endActions[v.id_i] = d
        elseif v.type == "EntityInstanceBreak" then
            self._breakActions[v.id_i] = d
        elseif v.type == "EntityEnterInstance" then
            self._enterActions[v.id_i] = d
        elseif v.type == "EntityEvent" then
            self._eventActions[v.id_i] = d
        elseif v.type == "EntityMonsterDeath" then
            self._monsterDeathActions[v.id_i] = d
        elseif v.type == "EntityBlock" then
            self._blockActions[v.id_i] = d
        elseif v.type == "EntityCameraWall" then
            self._cameraWalls[v.id_i] = d
        elseif v.type == "EntityTimeComplete" then
            self._timeComplete[v.id_i] = d
        elseif v.type == "EntityTask" then
            self._stateAction[v.id_i] = d
        elseif v.type == "EntityCGComplete" then
            self._cgComplete[v.id_i] = d
        end
    end
    self:SetMonsterDeath()
end

function SpaceData:InitPathPointSeq()
    
    --print("SpaceData:InitPathPointSeq !!!!!")
    self._pathPointSeq = {}
    local reverse_pps = {}
    for pp_id, v in pairs(self._pathPoints) do
        if v.idx == 0 then
            table.insert(reverse_pps, pp_id)
        end
    end
    
    if #reverse_pps <= 0 then
        return
    end
    
    local goon = false
    for i = 1, 100 do
        goon = false
        local first_pp_id = reverse_pps[#reverse_pps]
        for pp_id, v in pairs(self._pathPoints) do
            if v.idx == first_pp_id then
                table.insert(reverse_pps, pp_id)
                goon = true
                break
            end
        end
        if not goon then
            break
        end
    end
    
    for i = 1, #reverse_pps do
        self._pathPointSeq[i] = table.remove(reverse_pps)
        --print("SpaceData:InitPathPointSeq !!!!! ",i,self._pathPointSeq[i])
    end
end

function SpaceData:SetMonsterDeath()
    for k, v in pairs(self._monsterDeathActions) do
        if v.death_type == 1 then --对应刷怪点ID
            -- local rs = v.death_args
            -- for _rk, _rv in pairs(rs) do
            --     local mids = self:GetActionByID(_rv).monster_ids
            --     for _i, _id in pairs(mids) do
            --         v:SetMonster(_id)
            --     end
            -- end
            LoggerHelper.Error("EntityMonster不用了，请找御寒改配置"..self.cfg.global_params.spacename)
        elseif v.death_type == 2 then --对应怪物ID
            local ids = v.death_args
            for _k, _v in pairs(ids) do
                v:SetMonster(_v)
            end
        elseif v.death_type == 3 then --对应区域刷怪点ID
            local rs = v.death_args
            for _rk, _rv in pairs(rs) do
                local zoneMonster = self:GetActionByID(_rv)
                local mid = zoneMonster.monster_id
                local count = zoneMonster.monster_count
                v:SetMonster(mid,count)
            end
        end
    end
end

function SpaceData:GetGlobalParams()
    if self._globalParams == nil then
        local setting = {}
        local globalParams = self.cfg.global_params
        local ex = tonumber(globalParams.enter_pos_x_i) * 0.01
        local ey = tonumber(globalParams.enter_pos_y_i) * 0.01
        local ez = tonumber(globalParams.enter_pos_z_i) * 0.01
        setting.enterPos = Vector3(ex, ey, ez)
        self._globalParams = setting
    end
    return self._globalParams
end

function SpaceData:GetEnterPos()
    return self:GetGlobalParams().enterPos
end

function SpaceData:GetLightmapPaths()
    if self._lightmapPaths == nil then
        self._lightmapPaths = IntStringArrayMap.New()
        local baseLightmaps = self.cfg.base_settings.lightmaps_l
        self._lightmapPaths:Add(0, self:TransformPath(baseLightmaps))
    end
    return self._lightmapPaths
end

function SpaceData:GetRenderSetting()
    if self._renderSetting == nil then
        local setting = {}
        local xmlBaseSettings = self.cfg.base_settings
        setting.isFog = xmlBaseSettings.is_fog_i == 1
        setting.useUnity = xmlBaseSettings.use_unity_i == 1
        local lst = string.split(xmlBaseSettings.fog_color_l, ",")
        setting.fogColor = Color(tonumber(lst[1]), tonumber(lst[2]), tonumber(lst[3]))
        setting.fogMode = FogMode.IntToEnum(tonumber(xmlBaseSettings.fog_mode_i))
        setting.fogStartDistance = tonumber(xmlBaseSettings.fog_start_distance_f)
        setting.fogEndDistance = tonumber(xmlBaseSettings.fog_end_distance_f)
        lst = string.split(xmlBaseSettings.ambient_light_l, ',')
        setting.ambientLight = Color(tonumber(lst[1]), tonumber(lst[2]), tonumber(lst[3]))
        lst = string.split(xmlBaseSettings.camera_near_far_l, ",")
        setting.cameraNear = tonumber(lst[1])
        setting.cameraFar = tonumber(lst[2])
        lst = string.split(xmlBaseSettings.staticPrefabs_l, ",")
        if lst ~= nil then
            setting.staticPrefabs = lst
        end
        if xmlBaseSettings.skybox_s ~= nil then
            --LoggerHelper.Error("xmlBaseSettings.skybox_s: " .. xmlBaseSettings.skybox_s)
            setting.skybox = xmlBaseSettings.skybox_s
        end
        
        setting.is_path_fog = xmlBaseSettings.is_path_fog_i == 1
        local lst = string.split(xmlBaseSettings.path_fog_color_l, ",")
        if lst ~= nil then
            setting.path_fogColor = Color(tonumber(lst[1]), tonumber(lst[2]), tonumber(lst[3]))
        end
        setting.path_fogMode = FogMode.IntToEnum(tonumber(xmlBaseSettings.path_fog_mode_i))
        setting.path_fogStartDistance = tonumber(xmlBaseSettings.path_fog_start_distance_f)
        setting.path_fogEndDistance = tonumber(xmlBaseSettings.path_fog_end_distance_f)
        lst = string.split(xmlBaseSettings.path_ambient_light_l, ',')
        if lst ~= nil then
            setting.path_ambientLight = Color(tonumber(lst[1]), tonumber(lst[2]), tonumber(lst[3]))
        end
        lst = string.split(xmlBaseSettings.path_camera_near_far_l, ",")
        if lst ~= nil then
            setting.path_cameraNear = tonumber(lst[1])
            setting.path_cameraFar = tonumber(lst[2])
        end
        
        self._renderSetting = setting
    end
    return self._renderSetting
end

function SpaceData:TransformPath(source)
    local paths = nil
    if source ~= nil then
        paths = {}
        local lst = string.split(source, ",")
        if lst == nil then
            return nil
        end
        for i = 1, #lst do
            paths[i] = string.format("Scenes/%s/%s/%s", self.sceneName, self.sceneName, lst[i]);
        end
    end
    return paths
end

function SpaceData:GetEntityActions()
    return self._entities
end

function SpaceData:GetTriggers()
    return self._triggers
end

function SpaceData:GetInitActions()
    return self._initActions
end

function SpaceData:GetEndActions()
    return self._endActions
end

function SpaceData:GetBreakActions()
    return self._breakActions
end

function SpaceData:GetEnterActions()
    return self._enterActions
end

function SpaceData:GetEventActions()
    return self._eventActions
end

function SpaceData:GetMonsterDeathActions()
    return self._monsterDeathActions
end

function SpaceData:GetBlockActions()
    return self._blockActions
end

function SpaceData:GetActionByID(id)
    local rst = self._entities[id]
    return rst
end

function SpaceData:GetTimeCompleteActions()
    return self._timeComplete
end

function SpaceData:GetCameraWalls()
    return self._cameraWalls
end

function SpaceData:GetStateAction()
    return self._stateAction
end

function SpaceData.CreateSpaceData(cfg)
    return SpaceData(cfg)
end

function SpaceData:GetPathPoints()
    return self._pathPoints
end

function SpaceData:GetPathPointSeq()
    return self._pathPointSeq
end

function SpaceData:GetCGCompletes()
    return self._cgComplete
end

