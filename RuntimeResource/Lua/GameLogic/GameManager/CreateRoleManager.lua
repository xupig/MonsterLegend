--CreateRoleManager.lua
CreateRoleManager = {}

local LocalSetting = GameConfig.LocalSetting
local EntityConfig = GameConfig.EntityConfig
local Application = UnityEngine.Application
local Camera = UnityEngine.Camera
local GameObject = UnityEngine.GameObject
-- local RenderTexture = UnityEngine.RenderTexture
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local public_config = require("ServerConfig/public_config")
local error_code = require("ServerConfig/error_code")
local LoadingBarManager = GameManager.LoadingBarManager

--创角玩家选择数据记录
CreateRoleManager.vocation = 1
CreateRoleManager.facade = ""
CreateRoleManager.race = 1
CreateRoleManager.roleName = "hehe"
CreateRoleManager.gender = 1

CreateRoleManager.ActorId = 1005
CreateRoleManager.HasBuildScene = false

--创角事件，暂时传的是LoginManager:LoginOffline
CreateRoleManager.CreateRoleEnterGameEvent = nil
--渲染纹理相机
CreateRoleManager.RenderCamera = nil

CreateRoleManager.Entity = nil
--buildSceneFinish
CreateRoleManager.buildSceneFinish = nil

--主角部位与装备模型设置id
CreateRoleManager.HeadId = {}--每一个部位数据格式为 装备ID，流光ID，粒子ID，颜色设置RID，颜色设置GID，颜色设置BID
CreateRoleManager.ClothId = {}
CreateRoleManager.WeaponId = {}
CreateRoleManager.WingId = {}
CreateRoleManager.CurrentSelectedCharacter = 0
CreateRoleManager.CurrentSelectedVocation = 100
CreateRoleManager.init = false

function CreateRoleManager:Init()
end
--這裡是进入游戏按钮
function CreateRoleManager:CreateRoleEnterGame()
    GameManager.GUIManager.ClosePanel(PanelsConfig.CreateRole)
    if CreateRoleManager.CreateRoleEnterGameEvent ~= nil then
        CreateRoleManager.CreateRoleEnterGameEvent()
    end
end

--创建RTT相机
function CreateRoleManager:CreateRttCamera()
    if CreateRoleManager.init then
        return
    end
    CreateRoleManager.init = true
    local goCam = GameObject.New("RttForCreateRole")
    CreateRoleManager.rttForCreateRoleGo = goCam
    CreateRoleManager.CameraPos = goCam:AddComponent(typeof(XGameObjectTweenPosition))
    CreateRoleManager.CameraRot = goCam:AddComponent(typeof(XGameObjectTweenRotation))
    CreateRoleManager.RenderCamera = goCam:AddComponent(typeof(Camera))
    CreateRoleManager.RenderCamera.backgroundColor = UnityEngine.Color.New(46 / 255, 64 / 255, 83 / 255, 255 / 255)
    CreateRoleManager.RenderCamera.fieldOfView = 60
    CreateRoleManager.RenderCamera.depth = -1
    
    self:SetCamera()
end

function CreateRoleManager:DestroyRttCamera()
    CreateRoleManager.init = false
    if CreateRoleManager.rttForCreateRoleGo ~= nil then
        GameObject.Destroy(CreateRoleManager.rttForCreateRoleGo)
    end
end

function CreateRoleManager:SetCamera(tran)
    if CreateRoleManager.init then
        local tranCam = CreateRoleManager.rttForCreateRoleGo.transform
        tranCam:SetParent(tran)
        if tran then
            tranCam.localPosition = Vector3.zero
            tranCam.localEulerAngles = Vector3.New(270, 90, 90)
        else
            tranCam.localPosition = Vector3.New(95.2, 49.6, 35.72)
            tranCam.localEulerAngles = Vector3.New(3.449, -0.057, -0.001)
        end
    end
end

function CreateRoleManager:SetCameraAction()
    if CreateRoleManager.init then
        local tranCam = CreateRoleManager.rttForCreateRoleGo.transform
        tranCam.localPosition = Vector3.New(95.2, 49.6, 35.12)
        tranCam.localEulerAngles = Vector3.New(3.449, -0.057, -0.001)
    end
end

function CreateRoleManager:ChangeCameraForRole()
    CreateRoleManager.rttForCreateRoleGo.transform:SetParent(nil)
    CreateRoleManager.CameraPos:SetToPosOnce(Vector3.New(95.2, 49.6, 35.72), 0.5, nil)
    -- CreateRoleManager.CameraRot:SetToRotationOnce(Vector3.New(3.449, -0.057, -0.001), 1)
    -- CreateRoleManager.rttForCreateRoleGo.transform.position = Vector3.New(95.2, 49.6, 35.72)
    CreateRoleManager.rttForCreateRoleGo.transform.eulerAngles = Vector3.New(3.449, -0.057, -0.001)
end

--构建角色 type 0:创角 1:选角
function CreateRoleManager:BuildRole(vocationx, actorIdx, facadeStr, type, finish)
    self:DestroyEntity()
    local entity = GameWorld.CreateEntity("CreateRoleAvatar", {vocation = vocationx, base_speed = 600, speed = 600,
        actorId = actorIdx, facade = facadeStr, createRoleType = type or 0})
    -- entity:SetFace(180)
    CreateRoleManager.buildSceneFinish = finish
    CreateRoleManager.Entity = entity
end

--销毁entity
function CreateRoleManager:DestroyEntity()
    if CreateRoleManager.Entity ~= nil then
        GameWorld.DestroyEntity(CreateRoleManager.Entity.id)
        if CreateRoleManager.Entity.pet_eid ~= 0 then
            GameWorld.DestroyEntity(CreateRoleManager.Entity.pet_eid)
        end
    end
end

function CreateRoleManager:AddEquip(equipId)
    CreateRoleManager.Entity:AddEquip(equipId, 0, 0)
end

function CreateRoleManager:SetCurCloth(clothID)
    CreateRoleManager.Entity:SetCurCloth(clothID)
end

function CreateRoleManager:SetCurHead(headID)
    CreateRoleManager.Entity:SetCurHead(headID)
end

function CreateRoleManager:PlayAction(actionID)
    CreateRoleManager.Entity:PlayAction(actionID)
end

function CreateRoleManager:GetRandomName(vocation)
    GameWorld.Player():get_random_name_req(vocation)
end

function CreateRoleManager:GetRandomNameResp(name)
    EventDispatcher:TriggerEvent(GameEvents.OnGetRandomNameResp, name)
end

function CreateRoleManager:GetDefaultFacade(vocation)
    local wingId = 0
    local headId = RoleDataHelper.GetRoleDefaultHeadByVocation(vocation)
    local clothId = RoleDataHelper.GetRoleDefaultClothByVocation(vocation)
    local weaponId = RoleDataHelper.GetRoleDefaultWeaponByVocation(vocation)
    
    --Cloth          Weapon         Wing     Hair           Head
    --1:1060102,0,0; 11:1010101,0,0;21:0,0,0;41:1050403,0,0;31:1040201,0,0
    local facade = "1:" .. self:GetPartStringEx(clothId) .. ";" ..
        "11:" .. self:GetPartStringEx(weaponId) .. ";" ..
        "21:" .. self:GetPartStringEx(wingId) .. ";" ..
        "31:" .. self:GetPartStringEx(headId)
    return facade
end

function CreateRoleManager:GetGenderByVocation(vocation)
    local group = math.floor(vocation / 100)
    if group == 1 or group == 3 then
        return public_config.GENDER_MALE
    else
        return public_config.GENDER_FEMALE
    end
end

function CreateRoleManager:GetPartStringEx(data)
    return string.format("%d,0,0,0,0,0", data)
end

--请求服务器创角
function CreateRoleManager:CreateAvatarReq()
    if self._createAvataring then
        return
    end
    self._createAvataring = true
    if self._timeOutTimer then
        TimerHeap:DelTimer(self._timeOutTimer)
    end
    self._timeOutTimer = TimerHeap:AddSecTimer(20, 0, 0, function()
        self._createAvataring = false
    end)
    self.facade = self:GetDefaultFacade(CreateRoleManager.CurrentSelectedVocation)
    --LoggerHelper.Error("selfFacade:" .. self.facade .. " vocation:"..self.vocation.. " roleName:"..self.roleName)
    GameWorld.Player():create_avatar_req(self.roleName, self.gender, self.vocation, self.race, self.facade)
end

function CreateRoleManager:CreateAvatarResp(result)
    EventDispatcher:TriggerEvent(GameEvents.ON_CREATE_ROLE_WAITING, false)
    self._createAvataring = false
    if self._timeOutTimer then
        TimerHeap:DelTimer(self._timeOutTimer)
    end
    if result == error_code.ERR_SUCCESSFUL then
        LoadingBarManager:ShowProgressBar(0.1)
        self:DestroyRttCamera()
        self:DestroyEntity()
    end
end

return CreateRoleManager
