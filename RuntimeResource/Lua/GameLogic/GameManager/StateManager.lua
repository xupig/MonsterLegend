local StateManager = Class.StateManager(ClassTypes.XObject)

StateManager.ONE = 1

local EntityConfig = GameConfig.EntityConfig
local StateConfig = GameConfig.StateConfig
local bit = bit
local XMLManager = GameManager.XMLManager
local CharacterAction = GameConfig.EnumType.CharacterAction
local EntityType = GameConfig.EnumType.EntityType
local PanelsConfig = GameConfig.PanelsConfig

function StateManager:__ctor__(owner)
    self._owner = owner
    self._currentState = 0
    self._disptchEnterDead = false
    self._onStateChange = function()self:OnStateChange() end
    self:InitCharacterActionState()
    self._owner:AddPropertyListener(EntityConfig.CURRENT_STATE, self._onStateChange)
    self:InitStateFuncs()
end

function StateManager:InitCharacterActionState()
    self._characterActionStateMap = {}
    self._characterActionStateMap[CharacterAction.ACTIVE_MOVE_ABLE] = 0
    self._characterActionStateMap[CharacterAction.MOVE_ABLE] = 0
    self._characterActionStateMap[CharacterAction.ATTACK_ABLE] = 0
    self._characterActionStateMap[CharacterAction.HIT_ABLE] = 0
    self._characterActionStateMap[CharacterAction.TURN_ABLE] = 0
    self._characterActionStateMap[CharacterAction.VISIBLE] = 0
    self._characterActionStateMap[CharacterAction.SHOW] = 0
    self._characterActionStateMap[CharacterAction.THINK_ABLE] = 0
    self._characterActionStateMap[CharacterAction.COLLIDE_ABLE] = 0
    self._characterActionStateMap[CharacterAction.TRANSMIT_ABLE] = 0
    self._characterActionStateMap[CharacterAction.AUTO_MOUNT_ABLE] = 0
end

function StateManager:InitStateFuncs()
    self._stateFuncMap = {}
    self._stateFuncMap[StateConfig.AVATAR_STATE_DEATH] = {self.OnEnterDeath, self.OnLeaveDeath}
    self._stateFuncMap[StateConfig.AVATAR_STATE_FLY] = {self.EnterFly, self.ExitFly}
    self._stateFuncMap[StateConfig.AVATAR_STATE_GLIDE] = {self.EnterGlide, self.ExitGlide}
    self._stateFuncMap[StateConfig.AVATAR_STATE_TRANSFORM] = {self.EnterTransform, self.ExitTransform}
end

function StateManager:OnStateChange()
    local newState = self._owner[EntityConfig.CURRENT_STATE]
    --有时候会为nil待查
    if newState == nil then
        return
    end
    local cursor = 0
    local newSubState = false
    for pos = 1, StateConfig.AVATAR_STATE_SPELL_MAX do
        cursor = bit.lshift(StateManager.ONE, pos - 1)
        newSubState = (bit.band(newState, cursor) ~= 0)
        self:SetState(pos - 1, newSubState, false)
    end
    self._currentState = newState
    self:RefreshCharacterActionState()
end

function StateManager:SetState(pos, newSubState, refresh)
    refresh = refresh or (refresh ~= false)
    local cursor = bit.lshift(StateManager.ONE, pos)
    local oldSubState = (bit.band(self._currentState, cursor) ~= 0)
    if newSubState then
        self._currentState = bit.bor(self._currentState, cursor)
    else
        self._currentState = bit.band(self._currentState, bit.bnot(cursor))
    end
    if refresh then
        self:RefreshCharacterActionState()
    end
    if newSubState ~= oldSubState then
        self:CallStateChangeHandler(pos, newSubState)
    end
end

function StateManager:CallStateChangeHandler(pos, state)
    if self._stateFuncMap[pos] ~= nil then
        if (state) then
            self._stateFuncMap[pos][1](self)
        else
            self._stateFuncMap[pos][2](self)
        end
    end
end

function StateManager:RefreshCharacterActionState()
    local __active_move_able = 0
    local __move_able = 0
    local __attack_able = 0
    local __hit_able = 0
    local __turn_able = 0
    local __visible = 0
    local __show = 0
    local __think_able = 0
    local __collide_able = 0
    local __transmit_able = 0
    local __auto_mount_able = 0
    local data = nil
    local cursor = 0
    for pos = 1, StateConfig.AVATAR_STATE_SPELL_MAX do
        data = XMLManager.spell_state[pos - 1]
        if data ~= nil then
            cursor = bit.lshift(StateManager.ONE, pos - 1)
            local coefficient = bit.band(self._currentState, cursor) ~= 0 and 1 or 0
            __active_move_able = bit.bor(__active_move_able, coefficient * data.__active_move_able)
            __move_able = bit.bor(__move_able, coefficient * data.__move_able)
            __attack_able = bit.bor(__attack_able, coefficient * data.__attack_able)
            __hit_able = bit.bor(__hit_able, coefficient * data.__hit_able)
            __turn_able = bit.bor(__turn_able, coefficient * data.__turn_able)
            __visible = bit.bor(__visible, coefficient * data.__visible)
            __show = bit.bor(__show, coefficient * data.__show)
            __think_able = bit.bor(__think_able, coefficient * data.__think_able)
            __collide_able = bit.bor(__collide_able, coefficient * data.__collide_able)
            __transmit_able = bit.bor(__transmit_able, coefficient * data.__transmit_able)
            __auto_mount_able = bit.bor(__auto_mount_able, coefficient * data.__auto_mount_able)
        end
    end
    self._characterActionStateMap[CharacterAction.ACTIVE_MOVE_ABLE] = __active_move_able
    self._characterActionStateMap[CharacterAction.MOVE_ABLE] = __move_able
    self._characterActionStateMap[CharacterAction.ATTACK_ABLE] = __attack_able
    self._characterActionStateMap[CharacterAction.HIT_ABLE] = __hit_able
    self._characterActionStateMap[CharacterAction.TURN_ABLE] = __turn_able
    self._characterActionStateMap[CharacterAction.VISIBLE] = __visible
    self._characterActionStateMap[CharacterAction.SHOW] = __show
    self._characterActionStateMap[CharacterAction.THINK_ABLE] = __think_able
    self._characterActionStateMap[CharacterAction.COLLIDE_ABLE] = __collide_able
    self._characterActionStateMap[CharacterAction.TRANSMIT_ABLE] = __transmit_able
    self._characterActionStateMap[CharacterAction.AUTO_MOUNT_ABLE] = __auto_mount_able
    self:OnCharacterActionStateChange()
end

function StateManager:OnCharacterActionStateChange()
    self._owner:SetThinkState(self:CanDo(CharacterAction.THINK_ABLE))
end

function StateManager:CanDo(action)
    return self._characterActionStateMap[action] == 0
end

function StateManager:InState(state)
    return bit.band(self._currentState, bit.lshift(StateManager.ONE, state)) > 0
end

function StateManager:IsPlayer()
    if self._owner.entityType == EntityType.PlayerAvatar then
        return true
    end
    return false
end

--激活发送死亡状态
function StateManager:SetDispatchEnterDead(value)
    self._disptchEnterDead = value
end

---死亡状态处理
function StateManager:OnEnterDeath()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.PlayerDie)
        self._owner:OnDeath()
    end
end

function StateManager:OnLeaveDeath()
    local ret = self:IsPlayer()
    if ret then
        GameManager.GUIManager.ClosePanel(PanelsConfig.Redemption)
    end
end

function StateManager:EnterFly()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.FlyButton_Set_State, 3)
    -- else
    --     EventDispatcher:TriggerEvent(GameEvents.AvatarEnterFly, self._owner.id)
    end
end

function StateManager:ExitFly()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.FlyButton_Set_State, 2)
    -- else
    --     EventDispatcher:TriggerEvent(GameEvents.AvatarExitFly, self._owner.id)
    end
end

function StateManager:EnterGlide()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.FlyButton_Set_State, 3)
    -- else
    --     EventDispatcher:TriggerEvent(GameEvents.AvatarEnterGlide, self._owner.id)
    end
end

function StateManager:ExitGlide()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.FlyButton_Set_State, 2)
    -- else
    --     EventDispatcher:TriggerEvent(GameEvents.AvatarExitGlide, self._owner.id)
    end
end

function StateManager:EnterTransform()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.Show_Fix_Func_Buttons, false)
    end
end

function StateManager:ExitTransform()
    local ret = self:IsPlayer()
    if ret then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
        EventDispatcher:TriggerEvent(GameEvents.Show_Fix_Func_Buttons, true)
    end
end

