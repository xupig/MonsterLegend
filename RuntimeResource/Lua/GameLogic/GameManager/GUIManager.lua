-- GUIManager.lua
require "UIComponent.Base.BaseComponent"
require "UIComponent.Base.BaseLuaUIComponent"
require "UIComponent.Base.BasePanel"
require "UIComponent.Base.UIList"
require "UIComponent.Base.UIComplexList"
require "UIComponent.Base.UIListItem"
require "UIComponent.Base.UIPageableList"
require "UIComponent.Base.UIScrollPage"
require "UIComponent.Base.UIScrollView"
require "UIComponent.Base.UIToggle"
require "UIComponent.Base.UIToggleGroup"
require "UIComponent.Base.UIProgressBar"
require "UIComponent.Base.UIScaleProgressBar"
require "UIComponent.Base.UIParticle"
require "UIComponent.Base.UIInputField"
require "UIComponent.Base.UIChatEmojiItem"
require "UIComponent.Base.UIChatTextBlock"
require "UIComponent.Base.UINavigationMenu"
require 'UIComponent.Base.UIVoiceDriver'
require "UIComponent.Base.UILinkTextMesh"
require "UIComponent.Base.UIInputFieldMesh"
require "UIComponent.Base.UIDial"
require "UIComponent.Base.UIMultipleProgressBar"
require "UIComponent.Base.UIGridLayoutGroup"
require "UIComponent.Base.UIGridLayoutGroupItem"
require "UIComponent.Base.UISlider"
require "UIComponent.Base.UIRoller"
require "UIComponent.Base.UIRollerItem"

-- 这是一个管理游戏UI界面的类
local GUIManager = {}
--共有成员
GUIManager.canvasWidth = 1280
GUIManager.canvasHeight = 720
--私有成员
GUIManager._uiRoot = nil
GUIManager._panelSettings = {}
GUIManager._uiCamera = nil
GUIManager._startPosition = Vector2.zero
GUIManager._rootRectTransform = nil
GUIManager._scaler = nil
GUIManager._uiAudio = nil
GUIManager.reopenSubMainMenu = 0
--CommonUI事件
GUIManager.ShowNext = nil --(text,callback)
local Stack = ClassTypes.Stack
GUIManager._showPanels = Stack()
GUIManager._showPanelNames = {}

GUIManager.ShowMessageBox = nil --(text,okCallback,cancelCallback)
GUIManager.ShowText = nil --(textType,text,duration) type:1是static,2是down2Up,3是right2Left
GUIManager.ShowNotice = nil --(text)
GUIManager.ShowUIFx = nil
GUIManager.ShowNewsTicke = nil
GUIManager.DestroyTime = 300


local ComponentsConfig = GameConfig.ComponentsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UI_LAYER = LayerMask.NameToLayer("UI");
GUIManager.PANEL_WIDTH = 1280;
GUIManager.PANEL_HEIGHT = 720;
local PANEL_PATH_TEMPLATE = {"GUI/", GameConfig.Global.Language, "/", "prefabName", "/", "prefabName", ".", "prefab"}

local COMMONBG_PATH = ""

local GameObject = UnityEngine.GameObject
local EventSystem = UnityEngine.EventSystems.EventSystem
local CanvasScaler = UnityEngine.UI.CanvasScaler
local GraphicRaycaster = UnityEngine.UI.GraphicRaycaster
local Screen = UnityEngine.Screen
local Global = GameConfig.Global
local StandaloneInputModule = UnityEngine.EventSystems.StandaloneInputModule
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent
local UIComponentUtil = GameUtil.UIComponentUtil
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local CommonIconType = GameConfig.EnumType.CommonIconType
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)
GUIManager.isCutoutScreen = GameWorld.LuaGameState.ScreenHasCutout()

function GUIManager.Init()
    GUIManager.CreateEventSystem()
    GUIManager.CreateUICamera()
    GUIManager.CreateUIRoot()
    GUIManager.CreateUIAudio()
    GUIManager.CreateUIBG()
    GUIManager.CreateCommonIcon()
    GUIManager.SetCallback()
    --CreateUnderlay()
    GameManager.GUILayerManager.Init(GUIManager._uiRoot)
    
    if GameWorld.isEditor then
        GUIManager.DestroyTime = 10
    end

    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, GUIManager.OnLeaveAnyScene)
    EventDispatcher:AddEventListener(GameEvents.ON_SCENE_TYPE_CHANGE, GUIManager.OnSceneTypeChange)
    EventDispatcher:AddEventListener(GameEvents.UNGINE_ONAPPLICATIONPAUSE, GUIManager.OnApplicationPause)
end

function GUIManager.ClearPanelData()
    for panelName, setting in pairs(GUIManager._panelSettings) do
        if setting.isActive then
            GUIManager.ClosePanel(panelName)
        end
    end
    for k, setting in pairs(GUIManager._panelSettings) do
        GUIManager.InitPanelSetting(setting)
    end
    GUIManager._showPanels:Clear()
    GUIManager._showPanelNames = {}
end


function GUIManager.CreateEventSystem()
    local go = GameObject("EventSystem")
    local eventSys = go:AddComponent(typeof(EventSystem))
    eventSys.sendNavigationEvents = false
    eventSys.pixelDragThreshold = 15;
    go:AddComponent(typeof(StandaloneInputModule))
    GameObject.DontDestroyOnLoad(go);
end

function GUIManager.CreateUICamera()
    local go = GameObject("UICamera")
    local camera = go:AddComponent(typeof(UnityEngine.Camera))
    camera.clearFlags = UnityEngine.CameraClearFlags.Depth
    camera.orthographic = true
    camera.cullingMask = 2 ^ UI_LAYER
    camera.transform.position = Vector3(1000, 1000, 1000)
    GameObject.DontDestroyOnLoad(go)
    GUIManager._uiCamera = go
    GUIManager._uiCameraComponent = camera
end

function GUIManager.CreateUIRoot()
    local go = GameObject("UIRoot")
    --[[   local canvas =  go:GetComponent(typeof(UnityEngine.Canvas))
    if canvas == nil then
    canvas =  go:AddComponent(typeof(UnityEngine.Canvas))
    end
    canvas.renderMode = UnityEngine.RenderMode.ScreenSpaceCamera;
    canvas.worldCamera = GUIManager._uiCamera:GetComponent(typeof(UnityEngine.Camera));
    canvas.planeDistance = 100.0;
    canvas.pixelPerfect = false;
    canvas.scaleFactor = GUIManager.CalculateCanvasScaleFactor()
    go:AddComponent(typeof(UnityEngine.UI.GraphicRaycaster))
    go:AddComponent(typeof(SortOrderedRenderAgent))
    go.layer = UI_LAYER;
    GUIManager._rootRectTransform = go:GetComponent(typeof(UnityEngine.RectTransform))
    local scaler =go:AddComponent(typeof(CanvasScaler));
    scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
    scaler.referenceResolution = Vector2(GUIManager.PANEL_WIDTH, GUIManager.PANEL_HEIGHT);
    GUIManager._scaler = scaler
    GUIManager.OnResize()
    --go.AddComponent<UIResizer>();
    GameObject.DontDestroyOnLoad(go)
    GUIManager._uiRoot = go ]]
    go:AddComponent(typeof(SortOrderedRenderAgent))
    GameObject.DontDestroyOnLoad(go)
    GUIManager._uiRoot = go
end

-- UI根节点
function GUIManager.GetUIRoot()
    return GUIManager._uiRoot
end

function GUIManager.CalculateCanvasScaleFactor()
    Global.CanvasScaleX = Screen.width / GUIManager.PANEL_WIDTH
    Global.CanvasScaleY = Screen.height / GUIManager.PANEL_HEIGHT
    
    local scaleWidth = Screen.width / GUIManager.canvasWidth;
    local scaleHeight = Screen.height / GUIManager.canvasHeight;
    Global.CanvasScale = scaleWidth < scaleHeight and scaleWidth or scaleHeight;
    if Global.CanvasScale == scaleWidth then
        GUIManager._startPosition = Vector2(0, -(Screen.height / Global.CanvasScale - GUIManager.PANEL_HEIGHT) * 0.5);
    else
        GUIManager._startPosition = Vector2((Screen.width / Global.CanvasScale - GUIManager.PANEL_WIDTH) * 0.5, 0);
    end
    return Global.CanvasScale;
end

function GUIManager.CreateUIAudio()
    local go = GameObject("UIAudio")
    -- go:AddComponent(typeof(UnityEngine.AudioSource))
    -- GUIManager._uiAudioListener = go:AddComponent(typeof(UnityEngine.AudioListener))
    GameObject.DontDestroyOnLoad(go);
    GUIManager._uiAudio = go
end

function GUIManager.OnResize()
    GUIManager.CalculateCanvasScaleFactor()
    local rect = GUIManager._rootRectTransform
    GUIManager.canvasWidth = rect.sizeDelta.x;
    GUIManager.canvasHeight = rect.sizeDelta.y;
-- if GUIManager.canvasWidth / GUIManager.canvasHeight >= GUIManager.PANEL_WIDTH / GUIManager.PANEL_HEIGHT then
--     GUIManager._scaler.matchWidthOrHeight = 1;
-- else
--     GUIManager._scaler.matchWidthOrHeight = 0;
-- end
end

function GUIManager.DestroyPanelsByMode(destroyMode)
    EventDispatcher:TriggerEvent(GameEvents.BeforeDestroyPanels)
    for k, setting in pairs(GUIManager._panelSettings) do
        --setting.layer == GUILayer.LayerUIPanel
        if setting.destroyMode == destroyMode and (setting.panel ~= nil) and not setting.isShowing and not setting.isActive then
            if setting.closeTime and DateTimeUtil.GetServerTime() - setting.closeTime > GUIManager.DestroyTime then
                local panel = setting.panel
                panel.gameObject.Destroy(panel.gameObject)
                setting.panel = nil
                setting.closeTime = nil
            end
        end
    end
end

function GUIManager.OnLeaveAnyScene()
    GUIManager.DestroyPanelsByMode(PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

function GUIManager.OnSceneTypeChange()
    GUIManager.DestroyPanelsByMode(PanelsConfig.DESTROY_ON_SCENE_TYPE_CHANGE)
end

function GUIManager.OnApplicationPause()
    GUIManager.DestroyPanelsByMode(PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.DestroyPanelsByMode(PanelsConfig.DESTROY_ON_SCENE_TYPE_CHANGE)
    GUIManager.DestroyPanelsByMode(PanelsConfig.DESTROY_ON_ON_APPLICATION_PAUSE)
end

function GUIManager.AddPanel(panelName, prefabName, layer, panelClassName,
    hideMainUI, isNotCenterAdjust, mainPanelName, isHideByScale, destroyMode, isNeedLoading)
    local setting = {}
    setting.panelName = panelName
    setting.prefabName = prefabName
    setting.layer = layer
    setting.panelClassName = panelClassName
    setting.hideMainUI = hideMainUI or false
    setting.isNotCenterAdjust = isNotCenterAdjust or false
    setting.mainPanelName = mainPanelName
    setting.destroyMode = destroyMode or PanelsConfig.NOT_DESTROY
    setting.isNeedLoading = isNeedLoading or false
    setting.isHideByScale = isHideByScale or false
    GUIManager.InitPanelSetting(setting)
    GUIManager._panelSettings[panelName] = setting
    setting.closeTime = nil
end

function GUIManager.InitPanelSetting(setting)
    setting.isActive = false
    setting.isShowing = false
    setting.isLoading = false
    setting.panel = nil
    setting.destroyOnClose = false
end

function GUIManager.ShowPanel(panelName, data, callback)
    if GUIManager._panelSettings[panelName] == nil then
        LoggerHelper.Error("error:不存在界面" .. panelName)
        return
    end

    local setting = GUIManager._panelSettings[panelName]
    -- for debug
    -- if panelName == PanelsConfig.GMCommand and setting.panel ~= nil then
    --     GameUtil.ReloadUtil.ReloadPanel("GMCommand")
    -- end
    local panel = setting.panel
    setting.isShowing = true
    if panel ~= nil then        
        GUIManager.AddShowPanel(setting)        
        if setting.isActive then
            panel:SetData(data)
            SortOrderedRenderAgent.ReorderAll()
            if callback ~= nil then
                callback()
            end
        else
            GUIManager.SetPanelActive(panelName, true, data)   
            if callback ~= nil then
                callback()
            end
            SortOrderedRenderAgent.ReorderAll()
        end
    
    else
        GUIManager.LoadPanel(panelName, data, callback)
    end

end

function GUIManager.AddShowPanel(setting)
    if setting.layer == GUILayer.LayerUIPanel and GUIManager._showPanelNames[setting.panelName] == nil then
        -- local data = GUIManager._showPanels[#GUIManager._showPanels]
        local data = GUIManager._showPanels:Top()
        if data and data.panel then
            data.panel:BaseHideModel()
            data.panel:BaseChangeZ(7000)
        end
        GUIManager._showPanels:Push(setting)
        GUIManager._showPanelNames[setting.panelName] = 1
        setting.panel:BaseShowModel()
        -- table.insert(GUIManager._showPanels, setting)
        -- GUIManager._showPanelNames[setting.panelName] = 1
        -- LoggerHelper.Error("添加新成员====" .. tostring(setting.panelName))
    end
end

--根据主功能表ID打开某个面板
function GUIManager.ShowPanelByPanelId(panelId,data)
    local panelNames = FunctionOpenDataHelper:GetButtonFunc(panelId)
    for i, v in pairs(panelNames) do
        GUIManager.ShowPanel(v[1],data)
        break
    end
end

--根据功能开启表ID打开一个面板上某个功能标签
function GUIManager.ShowPanelByFunctionId(funcId,args,notCheckFuncOpen)
    local funcOpenCfg = FunctionOpenDataHelper.GetFunctionOpen(funcId)
    --功能未开启
    if not notCheckFuncOpen and not FunctionOpenDataHelper:CheckFunctionOpen(funcId) then
        PlayerManager.FunctionOpenManager:ShowOpenText(funcOpenCfg)
        return
    end
    if funcOpenCfg.button_func3 then
        local panelName = funcOpenCfg.button_func3[1][1]
        if panelName then
            local data = {["functionId"] = funcId,["args"] = args}
            --已经打开
            if GUIManager.PanelIsActive(panelName) then
                local panel = GUIManager.GetPanel(panelName)
                panel:OnShowSelectTab(data)
            --还没打开
            else
                GUIManager.ShowPanel(panelName,data)
            end
        else
            LoggerHelper.Error("Wrong function id!!!")
        end
    end
end

function GUIManager.ShowPanelByPanelFuncId(panelFuncId)
    -- LoggerHelper.Error("GUIManager.ShowPanelByPanelFuncId(panelFuncId =" ..panelFuncId)
    local func = FunctionOpenDataHelper:GetButtonFunc(panelFuncId)
    if func == nil then
        return
    end
    for k, v in pairs(func) do
        if k == 8 then
            for _, v1 in pairs(v) do
                GUIManager.ShowPanel(v1)
            end
        end
        --公会按钮响应
        if k == 7 then
            if PlayerManager.GuildManager:IsInGuild() then
                GUIManager.ShowPanel(v[2])
            else
                GUIManager.ShowPanel(v[1])
            end
        end
    end
end

-- 面板是否按scale显示
function GUIManager.PanelIsHideByScale(panelName)
    if GUIManager._panelSettings[panelName] == nil then
        return false
    end
    
    return GUIManager._panelSettings[panelName].isHideByScale
end

-- 面板是否显示
function GUIManager.PanelIsActive(panelName)
    if GUIManager._panelSettings[panelName] == nil then
        return false
    end
    
    return GUIManager._panelSettings[panelName].isActive
end

function GUIManager.ClosePanel(panelName)
    local setting = GUIManager._panelSettings[panelName]
    if not setting then
        return
    end
    setting.isShowing = false
    local panel = setting.panel
    if panel ~= nil then
        if setting.layer == GUILayer.LayerUIPanel then
            panel:BaseHideModel()
            GUIManager._showPanelNames[panelName] = nil
            local data = GUIManager._showPanels:Top()
            if data ~= nil then
                if panelName == data.panelName then
                    GUIManager._showPanels:Pop()
                    data = GUIManager._showPanels:Top()
                    if data and data.panel then
                        data.panel:BaseChangeZ(0)
                        data.panel:BaseShowModel()
                    end
                else
                    local newDatas = {}
                    local datas = GUIManager._showPanels:GetDatas()
                    local closePanelSetting
                    for i,v in ipairs(datas) do
                        if v.panelName ~= panelName then
                            table.insert(newDatas, v)
                        else
                            closePanelSetting = v
                        end
                    end
                    GUIManager._showPanels:SetDatas(newDatas)
                    if closePanelSetting and closePanelSetting.panel then
                        closePanelSetting.panel:BaseChangeZ(0)
                    end
                end
            end
        end
        -- if not table.isEmpty(GUIManager._showPanels) then
        --     if panelName == GUIManager._showPanels[#GUIManager._showPanels].panelName then
        --         table.remove(GUIManager._showPanels)
        --         local data = GUIManager._showPanels[#GUIManager._showPanels]
        --         if data and data.panel then
        --             data.panel:BaseShowModel()
        --         end
        --     else
        --         local datas = {}
        --         for i,v in ipairs(GUIManager._showPanels) do
        --             if v.panelName ~= panelName then
        --                 table.insert(datas, v)
        --             end
        --         end
        --         GUIManager._showPanels = datas
        --     end
        -- end
 
        GUIManager.SetPanelActive(panelName, false)
        GUIManager.CheckPanelHasClose(setting)
        setting.closeTime = DateTimeUtil.GetServerTime()
    end
end

function GUIManager.CheckPanelHasClose(setting)
    if setting.panelName ~= PanelsConfig.NPCDialog and (setting.layer == GUILayer.LayerUIPanel or setting.layer == GUILayer.LayerUICommon or setting.panelName == PanelsConfig.FirstCharge) then
        EventDispatcher:TriggerEvent(GameEvents.CLOSE_UI_PANEL)
    end
    if not GUIManager.CheckPanelHasOpen() then
        EventDispatcher:TriggerEvent(GameEvents.ON_NO_PANEL_SHOW, "guide_no_panel_show")
    end
end

function GUIManager.LoadPanel(panelName, data, callback)
    local setting = GUIManager._panelSettings[panelName]
    if setting.isLoading then return end
    setting.isLoading = true
    local path = GUIManager.GetPanelPrefabPath(panelName)
    if setting.isNeedLoading then
        PlayerManager.CommonWaitManager:BeginWaitLoading(panelName)
    end
    GameWorld.GetGameObject(path,
        function(go)
            if setting.isNeedLoading then
                PlayerManager.CommonWaitManager:EndWaitLoading(panelName)
            end
            go.name = setting.prefabName
            GUIManager.OnPrefabLoaded(go, panelName, data, callback)
        end
)
end

function GUIManager.PreloadPanel(panelName, callback)
    local setting = GUIManager._panelSettings[panelName]
    local path = GUIManager.GetPanelPrefabPath(panelName)
    GameWorld.PreloadAsset({path}, callback)
end

function GUIManager.OnPrefabLoaded(go, panelName, data, callback)
    local setting = GUIManager._panelSettings[panelName]
    GUIManager.AddToUILayer(go, panelName)
    setting.panel = GameWorld.AddLuaUIComponent(go, ComponentsConfig.Panel, setting.panelClassName)
    setting.panel.prefabName = panelName

    if GUIManager.isCutoutScreen and ( setting.layer == GUILayer.LayerUIMain or 
        setting.layer == GUILayer.LayerUIMiddle or setting.layer == GUILayer.LayerUIPanel ) then --and setting.panel.CheckiPhoneX then
        setting.panel:AddCutoutAdapter()
    end

    setting.isLoading = false
    go:AddComponent(typeof(SortOrderedRenderAgent))
    
    GUIManager.AddWindowBG(setting)
    GUIManager.AddCommonBG(setting)
    if setting.isShowing then
        setting.isShowing = false
        GUIManager.SetPanelActive(panelName, true, data)
        SortOrderedRenderAgent.ReorderAll()
    else
        GUIManager.SetPanelActive(panelName, false)
    end
    if callback ~= nil then
        callback()
    end
    GUIManager.AddShowPanel(setting)
end

function GUIManager.SetPanelActive(panelName, state, data)
    local setting = GUIManager._panelSettings[panelName]
    setting.isActive = state
    local panel = setting.panel
    if panel ~= nil then
        if setting.isHideByScale then
            if state then
                panel.transform.localScale = ShowScale
            else
                panel.transform.localScale = HideScale
            end
        else
            panel:SetActive(state)
        end
        if state then
            panel:SetAsLastSibling()
            panel:OnBaseShow()
            panel:OnShow(data)
            if GUIManager.IsNeedChangeCamera(setting) then
                GUIManager.SetCameraCulling(panelName, true)
            end
        else
            panel:OnClose()
            panel:OnBaseClose()
            if GUIManager.IsNeedChangeCamera(setting) then
                GUIManager.SetCameraCulling(panelName, false)
            end
        end
    end
end

--panel可设置disableCameraCulling=true来排除生效，默认生效
function GUIManager.IsNeedChangeCamera(setting)
    if (setting.layer == GUILayer.LayerUIPanel or setting.layer == GUILayer.LayerUIMiddle) and setting.panel.disableCameraCulling ~= true then
        return true
    end
    return false
end

GUIManager._cameraCullingMode = 0
function GUIManager.SetCameraCulling(panelName, state)
    if GUIManager._inCGCameraCulling then
        return
    end
    
    local mode = 0
    if state then
        local setting = GUIManager._panelSettings[panelName]
        local type = setting.panel:WinBGType()
        if type == PanelWinBGType.FullSceenBG then
            mode = 2
        else
            mode = 1
        end
    end

    if mode == GUIManager._cameraCullingMode then
        return
    end

    if mode == 0 then
        GUILayerManager.ShowLayer(GUILayer.LayerUIMain)
    -- GUILayerManager.ShowLayer(GUILayer.LayerUIBillboard)
    elseif mode == 2 then
        GUILayerManager.HideLayer(GUILayer.LayerUIMain)
    -- GUILayerManager.HideLayer(GUILayer.LayerUIBillboard)
    end
    GUIManager._cameraCullingMode = mode
    GameWorld.SetMainCameraMode(GUIManager._cameraCullingMode)
end

GUIManager._inCGCameraCulling = false
function GUIManager.SetInCGCameraCulling(state)
    GUIManager._inCGCameraCulling = state
    if state then
        if GUIManager._cameraCullingMode ~= 0 then
            GUIManager._cameraCullingMode = 0
            GUILayerManager.ShowLayer(GUILayer.LayerUIMain)
            GameWorld.SetMainCameraMode(GUIManager._cameraCullingMode)
        end
    end
end

function GUIManager.CheckPanelHasOpen()
    --LoggerHelper.Error("CheckPanelsByCG:" .. tostring(layer) .. ":" .. panelObjName)
    for k, setting in pairs(GUIManager._panelSettings) do
        if (setting.isActive == true or setting.isShowing == true)
            and (setting.layer == GUILayer.LayerUIPanel or setting.layer == GUILayer.LayerUICommon
            or setting.panelName == PanelsConfig.FirstCharge) then
            return true
        end
    end
    return false
end

function GUIManager.CreateUIBG()
    local prefabName = 'Panel_CommonBG'
    PANEL_PATH_TEMPLATE[4] = prefabName
    PANEL_PATH_TEMPLATE[6] = prefabName
    COMMONBG_PATH = table.concat(PANEL_PATH_TEMPLATE)
    GameWorld.GetGameObject(COMMONBG_PATH,
        function(go)
            GUIManager._commonBG = go
            GameObject.DontDestroyOnLoad(go)
        end)
end

function GUIManager.CreateCommonIcon()
    local prefabName = 'Panel_CommonIcon'
    PANEL_PATH_TEMPLATE[4] = prefabName
    PANEL_PATH_TEMPLATE[6] = prefabName
    local path = table.concat(PANEL_PATH_TEMPLATE)
    GameWorld.GetGameObject(path,
        function(go)
            GUIManager._commonIcon = go
            GameObject.DontDestroyOnLoad(go)
        end)
end

--增加九宫格动态图标
function GUIManager.AddCommonIcon(parent,type,id,reverse)
    local name
    local childName
    if type == CommonIconType.Bubble then
        name = "Container_Bubble/Container_Bubble"..id
        childName = "Container_Bubble"..id
    end
    local parentTrans = parent.transform
    if parentTrans.childCount > 0 then
        local child = parentTrans:GetChild(0)
        local startIndex,endIndex = string.find(child.name, childName)
        if startIndex then
            return
        else
            GameObject.Destroy(child.gameObject)
        end
    end
    local itemGo = GameObject.Instantiate(GUIManager._commonIcon.transform:Find(name).gameObject)
    itemGo.transform:SetParent(parent.transform)
    local rect = itemGo:GetComponent(typeof(UnityEngine.RectTransform))
    -- rect.pivot = Vector2(0.5, 0.5)
    -- rect.anchorsMax= Vector2(1,1)
    if reverse then
        rect.localScale = Vector3.New(-1,1,1)
    else
        rect.localScale = Vector3.New(1,1,1)
    end
    rect.localRotation = Vector3.zero
    rect.localPosition = Vector3.zero
    rect.offsetMin = Vector3.zero
    rect.offsetMax = Vector3.zero
    itemGo:SetActive(true)
    return itemGo
end

--增加道具图标
function GUIManager.AddItem(parent, type, size)
    local name
    local rect = parent:GetComponent(typeof(UnityEngine.RectTransform))
    if size == nil then
        size = rect.sizeDelta.x
    end
    if type == 1 then
        --普通道具
        name = "Container_Item_Normal"
    end
    local itemGo = GameObject.Instantiate(GUIManager._commonBG.transform:Find(name).gameObject)
    itemGo.transform:SetParent(parent.transform)
    itemGo:GetComponent(typeof(UnityEngine.RectTransform)).transform.localScale = Vector3.New(size / 84, size / 84, size / 84)
    itemGo:GetComponent(typeof(UnityEngine.RectTransform)).transform.localRotation = Vector3.zero
    itemGo:GetComponent(typeof(UnityEngine.RectTransform)).transform.localPosition = Vector3.zero
    itemGo:SetActive(true)
    return itemGo
end

-- 对面板增加背景
function GUIManager.AddCommonBG(setting, callback)
    local helper = GameUtil.GUIGameObejctHelper
    local panel = setting.panel
    local type = panel:CommonBGType()
    if type > 0 then
        local commonGo = GameObject("Container_CommonBg")
        commonGo.transform:SetParent(panel.transform, false)
        local rect = helper.AddBottomLeftRectTransform(commonGo, Vector2.zero,
            Vector2(GUIManager.canvasWidth, GUIManager.canvasHeight))
        local bgSrcRoot = GUIManager._commonBG.transform:Find("Container_CommonBg")
        local bgSrc = UIComponentUtil.FindChild(bgSrcRoot.gameObject, 'Container_bg' .. panel:CommonBGType())
        local bgNew = GameObject.Instantiate(bgSrc)
        bgNew.name = bgSrc.name
        bgNew.transform:SetParent(commonGo.transform, false)
        bgNew:SetActive(true)
        commonGo.transform:SetSiblingIndex(0)
        panel:HandlerBGEvent(commonGo.gameObject)
        commonGo.transform.localScale = Vector3.one
        commonGo.transform.localRotation = Vector3.zero
        commonGo.transform.anchoredPosition = Vector2.zero
        panel:OnAddCommonBG()
    end
    if callback then
        callback()
    end
end

function GUIManager.AddWindowBG(setting, callback)
    local helper = GameUtil.GUIGameObejctHelper
    local panel = setting.panel
    local type = panel:WinBGType()
    if type > 0 then
        local commonGo = GameObject("Container_WinBg")
        commonGo.transform:SetParent(panel.transform, false)
        commonGo.transform.localPosition = Vector3(0, 0, 5000)
        local rect = helper.AddBottomLeftRectTransform(commonGo, Vector2.zero,
            Vector2(GUIManager.canvasWidth, GUIManager.canvasHeight))
        local bgSrcRoot = GUIManager._commonBG.transform:Find("Container_CommonWindow")
        local bgSrc = UIComponentUtil.FindChild(bgSrcRoot.gameObject, 'Container_win' .. type)
        local bgNew = GameObject.Instantiate(bgSrc)
        bgNew.name = bgSrc.name
        bgNew.transform:SetParent(commonGo.transform, false)
        bgNew:SetActive(true)
        commonGo.transform:SetSiblingIndex(0)
        --panel:HandlerBGEvent(commonGo.gameObject)
        commonGo.transform.localScale = Vector3.one
        commonGo.transform.localRotation = Vector3.zero
        commonGo.transform.anchoredPosition = Vector2.zero
        panel:OnAddCommonWinBG()
    end
    if callback then
        callback()
    end
end

function GUIManager.GetPanelPrefabPath(panelName)
    local setting = GUIManager._panelSettings[panelName]
    PANEL_PATH_TEMPLATE[4] = setting.prefabName
    PANEL_PATH_TEMPLATE[6] = setting.prefabName
    return table.concat(PANEL_PATH_TEMPLATE)
end

function GUIManager.AddToUILayer(go, panelName)
    local setting = GUIManager._panelSettings[panelName]
    -- local trans = GUILayerManager.GetUILayerTransform(setting.layer)
    -- LoggerHelper.Log("-----------trans:" , trans)
    -- go.transform:SetParent(trans)
    go.layer = UI_LAYER
    
    
    local canvas = go:GetComponent(typeof(UnityEngine.Canvas))
    local canvasInited = false
    if canvas == nil then
        canvas = go:AddComponent(typeof(UnityEngine.Canvas))
        go:AddComponent(typeof(UnityEngine.UI.GraphicRaycaster))
        canvasInited = true
    end
    GUILayerManager.PushToChildLayer(go, panelName)
    
    local rect = go:GetComponent(typeof(UnityEngine.RectTransform))
    rect.localScale = Vector3.one
    -- rect.pivot = Vector2(0.5, 0.5)
    -- rect.localPosition = Vector3.zero;
    if setting.isNotCenterAdjust == PanelsConfig.IS_CENTER_ADJUST then
        rect.pivot = Vector2(0.5, 0.5)
        rect.localPosition = Vector3.zero
    elseif setting.isNotCenterAdjust == PanelsConfig.IS_NOT_CENTER_ADJUST then
        rect.localPosition = Vector3.zero
        rect.anchoredPosition = Vector2.zero
    elseif setting.isNotCenterAdjust == PanelsConfig.EXTEND_TO_FIT then
        rect.localPosition = Vector3.zero
        rect.anchoredPosition = Vector2.zero
        rect.sizeDelta = Vector2.zero

        if GUIManager.isCutoutScreen and panelName ~= PanelsConfig.CutoutScreenBorder then
            local cutoutHeight = GUIManager.GetCutoutScreenHeight()
            local offsetMin = Vector2.New(cutoutHeight, 0)
            local offsetMax = Vector2.New(cutoutHeight * -1, 0)
            rect.offsetMin = offsetMin
            rect.offsetMax = offsetMax
        end
    end
    
    if canvasInited then
        GameWorld.SetCanvasesState(go, GUILayerManager.GetLayerVisible(setting.layer))
    end
    
    if setting.isCenterAdjust then
        -- rect.localPosition = Vector3(GUIManager.canvasWidth * 0.5, GUIManager.canvasHeight * 0.5, 0.0);
        else
        -- rect.localPosition = Vector3(GUIManager.PANEL_WIDTH * 0.5, GUIManager.PANEL_HEIGHT * 0.5, 0.0);
        end
-- go.layer = UI_LAYER
-- local canvas = go:GetComponent(typeof(UnityEngine.Canvas))
-- if canvas == nil then
--     canvas = go:AddComponent(typeof(UnityEngine.Canvas))
--     go:AddComponent(typeof(UnityEngine.UI.GraphicRaycaster))
-- end
--SortOrderedRenderAgent agent = go.AddComponent<SortOrderedRenderAgent>();
end

function GUIManager.GetPanel(panelName)
    return GUIManager._panelSettings[panelName].panel
end

function GUIManager.SetCallback()
    GUIManager.ShowMessageBox = function(type, text, okCallback, cancelCallback)
        local value = {
            ["confirmCB"] = okCallback,
            ["cancelCB"] = cancelCallback,
            ["text"] = text
        }
        local msgData = {["id"] = MessageBoxType.Tip, ["value"] = value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    end
end

--充值弹窗
function GUIManager.ShowChargeMessageBox()
    local value = {
        ["confirmCB"] = function()
            GUIManager.ShowPanelByFunctionId(43)
        end,
        ["cancelCB"] = nil,
        ["text"] = LanguageDataHelper.CreateContent(58651)
    }
    local msgData = {["id"] = MessageBoxType.Tip, ["value"] = value}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
end

--获取面板的SortOrderedRenderAgent.order
function GUIManager.GetPanelSortOrder(panelName)
    local setting = GUIManager._panelSettings[panelName] or {}
    local panel = setting.panel
    if panel ~= nil then
        local agent = panel.gameObject:GetComponent(typeof(SortOrderedRenderAgent))
        if agent ~= nil then
            return agent.order
        end
    end
    return 0
end

function GUIManager.Reset()
    for k, setting in pairs(GUIManager._panelSettings) do
        if (setting.isActive == true or setting.isShowing == true) and (setting.panelName ~= PanelsConfig.Login)
            and (setting.layer == GUILayer.LayerUIPanel or setting.layer == GUILayer.LayerUICommon or setting.layer == GUILayer.LayerUIMain
            or setting.layer == GUILayer.LayerUIMiddle) then
            GUIManager.ClosePanel(setting.panelName)
        end
    end
end

--显示今日不再提醒
--type:今日不再提醒类型.如：SettingType.CIRCLE_TASK_FAST_COMPLETE
--text:今日不再提醒，文字内容信息
--cb：点击确定后的回调处理函数
--toogleActive:是否需要显示今日不再提醒,默认为true
function GUIManager.ShowDailyTips(tipType, cb, cancelCB, text, toogleActive, okText, cancelText)
    if toogleActive == nil then toogleActive = true end
    local settingData = PlayerManager.PlayerDataManager.playerSettingData
    local dailyData = settingData:GetData(tipType)
     
    if (not toogleActive) or (toogleActive and (dailyData == nil or dailyData == 0 or (type(dailyData) == "number" and DateTimeUtil.SameDay(dailyData) == false))) then
        local value = {
            ["confirmCB"] = cb,
            ["cancelCB"] = cancelCB,
            -- ["cancleButton"] = false,
            ["text"] = text,
            ["tipType"] = tipType,
            ["title"] = LanguageDataHelper.CreateContent(303),
            ["toogleText"] = LanguageDataHelper.CreateContent(56019),
            ["toogleActive"] = toogleActive,
            ['okText'] = okText,
            ['cancelText'] = cancelText
        }
        local msgData = {["id"] = MessageBoxType.ToggleTips, ["value"] = value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    else
        cb()
    end

    if dailyData and type(dailyData) ~= "number" then
        LoggerHelper.Error("ShowDailyTips: dailyData is not number: " .. tostring(tipType))
    end
end

-- 将刘海屏考虑进屏幕宽度的计算, 在有刘海屏的情况下会减去刘海屏适配的宽度，
-- 一般的界面(不受刘海屏影响)不需要使用这个方法
function GUIManager.GetCutoutScreenPanelWidth()
    if GUIManager.isCutoutScreen then
        local currentCutout = GUIManager.GetCutoutScreenHeight()
        return GUIManager.canvasWidth - currentCutout * 2
    end
    return GUIManager.canvasWidth
end

function GUIManager.GetCutoutScreenHeight()
    local currentCutout = 0
    if GUIManager.isCutoutScreen then
        local iPhoneCutout = 60
        local androidCutout = GameWorld.GetCutout()
        currentCutout = iPhoneCutout
        if GameWorld.LuaGameState.PlatfromIsAndroid() then
            currentCutout = androidCutout
        end
    end
    return currentCutout
end

return GUIManager
