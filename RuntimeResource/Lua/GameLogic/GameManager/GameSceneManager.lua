-- GameSceneManager.lua
local GameSceneManager = {}

require "GameManager/SceneSystem/SpaceData"
require "GameManager/SceneSystem/Scene"
require "GameManager/SpaceSystem/EntityAction"

local SceneConfig = GameConfig.SceneConfig
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local ClassTypes = ClassTypes
local action_config = GameWorld.action_config
local EntityType = GameConfig.EnumType.EntityType
local DATA_SPACE = "data/spaces/"
local XML_SUFFIX = ".xml"
local SpaceData = ClassTypes.SpaceData
local FileAccessManager = GameLoader.IO.FileAccessManager

local SceneTypeMap = {}
SceneTypeMap[SceneConfig.SCENE_TYPE_LOGIN] = ClassTypes.LoginScene
SceneTypeMap[SceneConfig.SCENE_TYPE_CHARACTOR] = ClassTypes.CharactorScene
SceneTypeMap[SceneConfig.SCENE_TYPE_MAIN] = ClassTypes.MainScene
SceneTypeMap[SceneConfig.SCENE_TYPE_WILD] = ClassTypes.WildScene
SceneTypeMap[SceneConfig.SCENE_TYPE_COMBAT] = ClassTypes.CombatScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GLIDE] = ClassTypes.MainGlide
SceneTypeMap[SceneConfig.SCENE_TYPE_PVP6] = ClassTypes.PVP6Scene
SceneTypeMap[SceneConfig.SCENE_TYPE_PVP_NEW] = ClassTypes.PVPNewScene
SceneTypeMap[SceneConfig.SCENE_TYPE_COMMON_BATTLE] = ClassTypes.CommonBattleScene
SceneTypeMap[SceneConfig.SCENE_TYPE_HERO_BATTLE] = ClassTypes.HeroBattleScene
SceneTypeMap[SceneConfig.SCENE_TYPE_HELL_BATTLE] = ClassTypes.HellBattleScene
SceneTypeMap[SceneConfig.SCENE_TYPE_RAID_BATTLE] = ClassTypes.RaidBattleScene
SceneTypeMap[SceneConfig.SCENE_TYPE_MIRROR] = ClassTypes.MirrorScene
SceneTypeMap[SceneConfig.SCENE_TYPE_WILD_MIRROR] = ClassTypes.WildMirrorScene
SceneTypeMap[SceneConfig.SCENE_TYPE_FIGHT_FRIEND] = ClassTypes.FightFriendScene
SceneTypeMap[SceneConfig.SCENE_TYPE_LADDER_1V1] = ClassTypes.Ladder1V1Scene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUILD_MANOR] = ClassTypes.GuildManorScene
SceneTypeMap[SceneConfig.SCENE_TYPE_AION_TOWER] = ClassTypes.AionTowerScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUILD_RAID] = ClassTypes.GuildRaidScene
SceneTypeMap[SceneConfig.SCENE_TYPE_WORLD_BOSS] = ClassTypes.WorldBossScene
SceneTypeMap[SceneConfig.SCENE_TYPE_EXP_INSTANCE] = ClassTypes.ExpInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_ABYSS] = ClassTypes.AbyssScene
SceneTypeMap[SceneConfig.SCENE_TYPE_PET_INSTANCE] = ClassTypes.PetInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_BOSS_HOME] = ClassTypes.BossHomeScene
SceneTypeMap[SceneConfig.SCENE_TYPE_MY_BOSS] = ClassTypes.MyBossScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GOLD] = ClassTypes.GoldInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_EQUIPMENT] = ClassTypes.EquipmentInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_FORBIDDEN] = ClassTypes.ForbiddenInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_ONLINE_PVP] = ClassTypes.TopAthleticsScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUILD_MONSTER] = ClassTypes.GuildMonsterScene
SceneTypeMap[SceneConfig.SCENE_TYPE_CLOUD_PEAK] = ClassTypes.CloudPeakScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUARD_GODDNESS] = ClassTypes.GuardGoddnessScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUILD_BANQUET] = ClassTypes.GuildBanquetScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GUILD_CONQUEST] = ClassTypes.GuildConquestScene
SceneTypeMap[SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND] = ClassTypes.GodMonsterIslandScene
SceneTypeMap[SceneConfig.SCENE_TYPE_TOWER_DEFENSE] = ClassTypes.TowerDefenseScene
SceneTypeMap[SceneConfig.SCENE_TYPE_WEDDING] = ClassTypes.WeddingScene
SceneTypeMap[SceneConfig.SCENE_TYPE_ANSWER] = ClassTypes.AnswerScene
SceneTypeMap[SceneConfig.SCENE_TYPE_MARRY_INSATNCE] = ClassTypes.MarryInstanceScene
SceneTypeMap[SceneConfig.SCENE_TYPE_BATTLE_SOUL] = ClassTypes.BattleSoulScene
SceneTypeMap[SceneConfig.SCENE_TYPE_WORLD_BOSS_FAKE] = ClassTypes.WORLD_BOSS_FAKEScene
SceneTypeMap[SceneConfig.SCENE_TYPE_ANCIENT_BATTLE] = ClassTypes.AncientBattleScene

function GameSceneManager:Init()
    self._scene = nil
    self._mapId = 0
    self._sceneType = 0
    self._preSceneType = 0
    self._enterPos = Vector3.zero
    self._isServerIns = false
    self._instanceId = 0
    self:InitListeners()
end

function GameSceneManager:InitListeners()
    --EventDispatcher:AddEventListener(GameEvents.ProcSceneAction, function(id)self:ProcSceneAction(id) end)
    EventDispatcher:AddEventListener(GameEvents.CreateDummy, function(id, posID, spawn_id)self:CreateDummy(id, posID, spawn_id) end)
    EventDispatcher:AddEventListener(GameEvents.CreateStaticMonster, function(id, posID, actionId)self:CreateStaticMonster(id, posID, actionId) end)
    EventDispatcher:AddEventListener(GameEvents.TeleportScene, function(id, posIDs)self:TeleportScene(id, posIDs) end)
    EventDispatcher:AddEventListener(GameEvents.CreateNPC, function(id, posID)self:CreateNPC(id, posID) end)
    EventDispatcher:AddEventListener(GameEvents.ActionTimeComplete, function(eventID)self:ActionTimeComplete(eventID) end)
    
    EventDispatcher:AddEventListener(GameEvents.ON_ACTION_RESP, function(act_id, errorCode, args)self:OnActionResp(act_id, errorCode, args) end)
    EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, function()self:TaskProcessingChange() end)
    EventDispatcher:AddEventListener(GameEvents.ActiveTeleport, function(action)self:ActiveTeleport(action) end)
    EventDispatcher:AddEventListener(GameEvents.PlayFX, function(id, posID, path)self._scene:PlayFX(id, posID, path) end)
    EventDispatcher:AddEventListener(GameEvents.DelFX, function(id, eid, path)self._scene:DelFX(id, eid, path) end)
    -- EventDispatcher:AddEventListener(GameEvents.FLY_TO_SCENE, function(target_scene)self._scene:SetFlyToScene(target_scene) end)
    
    --EventDispatcher:AddEventListener(GameEvents.ActionFaction, function(target_type, id, faction_id) self._scene:ActionFaction(target_type, id, faction_id) end)
    EventDispatcher:AddEventListener(GameEvents.CreateClientResourcePoint, function(actionCfg)self:CreateClientResourcePoint(actionCfg) end)
    
    -------------CG
    EventDispatcher:AddEventListener(GameEvents.CreateDramaAvatar, function(id, tid, pos, face, isBorn)self._scene:CreateDramaAvatar(id, tid, pos, face, isBorn) end)
    EventDispatcher:AddEventListener(GameEvents.CreateDramaDummy, function(id, tid, pos, face, isBorn)self._scene:CreateDramaDummy(id, tid, pos, face, isBorn) end)
    EventDispatcher:AddEventListener(GameEvents.CreateDramaNPC, function(id, tid, pos, face, isBorn)self._scene:CreateDramaNPC(id, tid, pos, face, isBorn) end)
    EventDispatcher:AddEventListener(GameEvents.TeleportDramaEntity, function(id, pos)self._scene:TeleportDramaEntity(id, pos) end)
    EventDispatcher:AddEventListener(GameEvents.DestroyDramaEntity, function(id)self._scene:DestroyDramaEntity(id) end)
    EventDispatcher:AddEventListener(GameEvents.DestroyDramaEntities, function()self._scene:DestroyDramaEntities() end)
    EventDispatcher:AddEventListener(GameEvents.RotateDramaEntity, function(id, rotate)self._scene:RotateDramaEntity(id, rotate) end)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayUIFX, function(path, pos, time)self._scene:DramaPlayUIFX(path, pos, time) end)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayVisualFX, function(path, pos, rotate, time)self._scene:DramaPlayVisualFX(path, pos, rotate, time) end)
    EventDispatcher:AddEventListener(GameEvents.DramaShowNPC, function(visible)self._scene:DramaShowNPC(visible) end)
    EventDispatcher:AddEventListener(GameEvents.DramaStopAI, function(t, range, args)self._scene:DramaStopAI(t, range, args) end)
    EventDispatcher:AddEventListener(GameEvents.DramaStartAI, function(t, range, args)self._scene:DramaStartAI(t, range, args) end)
end

-- function GameSceneManager:ProcSceneAction(id)
--     self._scene:ProcSceneAction(id)
-- end
function GameSceneManager:GetCurrMapID()
    return self._mapId
end

function GameSceneManager:SetCurrMapID(map_id)
    self._mapId = map_id
    if GameWorld.Player() ~= nil then
        GameWorld.Player():SetCurrMapID(map_id)
    end
end

function GameSceneManager:GetDramaEntityByID(id)
    return self._scene:GetDramaEntityByID(id)
end

function GameSceneManager:IsDramaEntityByEID(id)
    return self._scene:IsDramaEntityByEID(id)
end

function GameSceneManager:CreateDummy(id, posID, spawn_id)
    self._scene:CreateDummy(id, posID, spawn_id)
end

function GameSceneManager:CreateStaticMonster(id, posID, actionId)
    self._scene:CreateStaticMonster(id, posID, actionId)
end

function GameSceneManager:TeleportScene(id, posIDs)
    self._scene:TeleportScene(id, posIDs)
end

function GameSceneManager:CreateNPC(id, posID)
    self._scene:CreateNPC(id, posID)
end

function GameSceneManager:CreateClientResourcePoint(actionCfg)
    self._scene:CreateClientResourcePoint(actionCfg)
end

function GameSceneManager:ActionTimeComplete(eventID)
    self._scene:ActionTimeComplete(eventID)
end

-- function GameSceneManager:GetKillNum()
--     if self._scene == nil then do return 0 end end
--     return self._scene:GetKillNum()
-- end
function GameSceneManager:OnActionResp(act_id, errorCode, args)
    -- LoggerHelper.Error(act_id .. "   " .. errorCode)
    if errorCode ~= error_code.ERR_SUCCESSFUL then do return end end

end

function GameSceneManager:TaskProcessingChange()
    --LoggerHelper.Log("Ash: TaskProcessingChange: " .. tostring(self._scene == nil))
    if self._scene == nil then return end
    self._scene:RefreshNPCState()
end

function GameSceneManager:EnterScene(sceneType, loadCallback)
    self:LeaveScene()
    self._preSceneType = self._sceneType
    self._sceneType = sceneType
    self._scene = self:CreateSceneByType(sceneType)
    self._enterPos = self._enterPos or Vector3.zero
    self._scene:Enter(self._mapId, self._enterPos, loadCallback)
end

function GameSceneManager:EnterSceneByID(mapId, changeLine, enterPos, loadCallback)
    GameWorld.PauseHandleData(true)
    -- LoggerHelper.Log("GameSceneManager:EnterSceneByID: " .. tostring(mapId))
    -- if self._mapId == mapId and changeLine == false and mapId ~= 6 then return end --当前选角场景回退回登录临时处理
    --self._mapId = mapId
    self:SetCurrMapID(mapId)
    self._enterPos = enterPos or Vector3.zero
    self._instanceId = InstanceDataHelper.GetInstanceIdByMapId(self._mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    self:ResetInstanceBattleMode(self._instanceId)
    self:SetMapCamera(self._mapId)
    self:EnterScene(sceneType, loadCallback)
end

function GameSceneManager:LeaveScene()
    if self._scene ~= nil then
        self._scene:Leave()
        self._scene = nil
    end
end

function GameSceneManager:GetInLoading()
    return self._scene:GetInLoading()
end

function GameSceneManager:GetSceneName()
    return self._scene:GetSceneName()
end

function GameSceneManager:SetMapCamera(mapId)
    local id = MapDataHelper.GetMapCameraId(mapId)
    if id > 0 then
        GameWorld.SetMapCameraArgs(id)
    end
end

function GameSceneManager:ResetInstanceBattleMode(instanceId)
    if instanceId == 0 then
        self._isServerIns = true
    else
        -- local kind = InstanceDataHelper.GetInstanceKind(instanceId) or SceneConfig.INSTANCE_CHECK_KIND_SERVER
        -- self._isServerIns = kind == SceneConfig.INSTANCE_CHECK_KIND_SERVER
        self._isServerIns = true
    end
    GameWorld.SetBattleServerMode(self._isServerIns)
end

function GameSceneManager:CreateSceneByType(sceneType)
    return SceneTypeMap[sceneType]()
end

function GameSceneManager:GetCurSceneType()
    return self._sceneType
end

function GameSceneManager:GetPreSceneType()
    return self._preSceneType
end

function GameSceneManager:MonsterDeath(spawn_id)
    self._scene:MonsterDeath(spawn_id)
end

function GameSceneManager:InServerIns()
    return self._isServerIns
end

function GameSceneManager:GetNPCs()
    return self._scene:GetNPCs()
end

function GameSceneManager:GetCfgEntity(id)
    return self._scene:GetCfgEntity(id)
end

function GameSceneManager:SetAOIState(state)
    if GameWorld.Player() == nil or GameWorld.Player().entityType ~= EntityType.PlayerAvatar then return end
end

function GameSceneManager:GetPathPoints()
    return self._scene:GetPathPoints()
end

function GameSceneManager:GetPathPointSeq()
    return self._scene:GetPathPointSeq()
end

function GameSceneManager:GetActionByID(id)
    return self._scene._spaceData:GetActionByID(id)
end

----------------------------------获取非当前地图的信息------------------------------------------------
function GameSceneManager:GetNPCInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local npcs = {}
    for k, v in pairs(actions) do
        if v.type == "NPCRegion" then
            local idx = 0
            local len = table.getn(v.npc_ids)
            while idx < len do
                idx = idx + 1
                local id = v.npc_ids[idx]
                local posID = v.npc_point_ids[idx]
                local point = sd:GetActionByID(posID)
                npcs[id] = point and point.pos or nil
            end
        end
    end
    return npcs
end

function GameSceneManager:GetCollectPointInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local collectPoints = {}
    for k, v in pairs(actions) do
        if v.type == "EntityResourcePoint" then
            collectPoints[v.id] = v
        end
    end
    return collectPoints
end

function GameSceneManager:GetTeleportInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local teleports = {}
    for k, v in pairs(actions) do
        if v.type == "EntityTeleportData" then
            teleports[v.id] = v
        end
    end
    return teleports
end

function GameSceneManager:GetPositionPointInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local postionPoints = {}
    for k, v in pairs(actions) do
        if v.type == "PositionPoint" then
            postionPoints[v.id] = v
        end
    end
    return postionPoints
end

function GameSceneManager:GetClickFlagInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local clickFlags = {}
    for k, v in pairs(actions) do
        if v.type == "EntityClickFlag" then
            clickFlags[v.id] = v.pos
        end
    end
    return clickFlags
end

function GameSceneManager:GetMonsterInfo(mapId)
    local actions, sd = self:GetActions(mapId)
    local monsters = {}
    for k, v in pairs(actions) do
        if v.type == "EntityMonster" or v.type == "EntityZoneMonster" then
            monsters[v.id] = v
            local posPointId = v.refresh_point_ids[1]
            v.truePos = sd:GetActionByID(posPointId).pos
        end
    end
    return monsters
end

--获取某个怪物位置(EntityZoneMonster类型节点)
function GameSceneManager:GetMonsterPos(mapId, entityId)
    local actions, sd = self:GetActions(mapId)
    local pos = nil
    for k, v in pairs(actions) do
        if v.type == "EntityZoneMonster" and v.id == entityId then
            local posPointId = v.refresh_point_ids[1]
            pos = sd:GetActionByID(posPointId).pos
        end
    end
    return pos
end

function GameSceneManager:GetActions(mapId)
    local sn = MapDataHelper.GetSpaceName(mapId)
    local path = DATA_SPACE .. sn .. XML_SUFFIX
    local content = FileAccessManager.LoadText(path)
    if content == nil then
        LoggerHelper.Error("GameSceneManager:GetActions content nil: " .. tostring(mapId) .. " path: " .. tostring(path))
        return {}, nil
    end
    local cfg = GameUtil.TypeUtil.GetTable(content)
    if cfg == nil then
        LoggerHelper.Error("GameSceneManager:GetActions cfg nil: " .. tostring(mapId) .. " path: " .. tostring(path))
        return {}, nil
    end
    local sd = SpaceData.CreateSpaceData(cfg)
    local actions = sd:GetEntityActions()
    return actions, sd
end

function GameSceneManager:CanRide()
    local mapId = GameSceneManager:GetCurrMapID()
    local limit = MapDataHelper.GetLimitPartner(mapId)
    if limit then
        for i = 1, #limit do
            if limit[i] == 3 then
                return false
            end
        end
    end
    return true
end

GameSceneManager:Init()
return GameSceneManager
