-- GUILayerManager.lua
-- 这是一个管理游戏UIlayer 层次的类

GUILayer = {}
local GUILayerNum = {}
GUILayerOrder = {
    "LayerUIBillboard",--名字告示栏层，显示实体名字血量称号等
    "LayerUIMain", --主面板层，内部是同时显示的，不存在层次关系
    "LayerUIMiddle", --介于主面板和普通面板的中间层，方便显示一些自动弹出的ui
    "LayerUIPanel", --普通面板层，内部按照打开顺序只显示一层
    "LayerUICommon", --常用UI层
    "LayerUIFloat", --飘字UI层
    "LayerUIFx",--飘特效
    "LayerUICG", --CG的Layer
    "LayerUITop"--LayerUITop是UI最顶层用来显示刘海屏黑边的，默认不会被关闭，最好不在这个界面添加其他Panel
}


GUILayerManager = {}
GUILayerManager._layerTransformDict = {}
GUILayerManager._layerVisibleDict = {}

local UnityEngine = UnityEngine
local LAYER_Z_INTERVAL = -1000
local GameObject = UnityEngine.GameObject
local GUIManager = GameManager.GUIManager

local CanvasScaler = UnityEngine.UI.CanvasScaler
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent
local UI_LAYER = LayerMask.NameToLayer("UI");

GUILayerManager._inited = false
function GUILayerManager.Init(root)
    GUILayerManager._inited = true
    GUILayerManager.InitEnum()
    GUILayerManager.CreateLayers(root)
end

function GUILayerManager.InitEnum()
    --local i = 0
    for k, v in ipairs(GUILayerOrder) do
        --i = i + 500
        GUILayer[v] = v
        GUILayerNum[v] = k
    end
end

function GUILayerManager.GetLayerNum(name)
    return GUILayerNum[name]
end

function GUILayerManager.CreateLayers(root)
    local helper = GameUtil.GUIGameObejctHelper
    local i = 0
    for k, v in pairs(GUILayerOrder) do
        local go = GameObject(v)
        local rect = helper.AddBottomLeftRectTransform(go, Vector2.zero, 
            Vector2(GUIManager.canvasWidth, GUIManager.canvasHeight))
        go.transform:SetParent(root.transform, false);
        --GameObject.DontDestroyOnLoad(go)
        local zPosition = i * LAYER_Z_INTERVAL
        GUILayerManager.CreateCanvas(go, i)
        rect.localPosition = Vector3(rect.localPosition.x, rect.localPosition.y, zPosition);
        GUILayerManager._layerTransformDict[v] = go.transform
        GUILayerManager._layerVisibleDict[v] = true
        i = i + 1
    end
    GUILayerManager.OnResize()
    --root:AddComponent(typeof(SortOrderedRenderAgent))
end

function GUILayerManager.CreateCanvas(go, idx)
    local canvas = go:AddComponent(typeof(UnityEngine.Canvas))
    canvas.renderMode = UnityEngine.RenderMode.ScreenSpaceCamera;
    canvas.worldCamera = GUIManager._uiCamera:GetComponent(typeof(UnityEngine.Camera));
    canvas.planeDistance = 100.0 * (#GUILayerOrder - idx);
    canvas.pixelPerfect = false;
    canvas.scaleFactor = GUIManager.CalculateCanvasScaleFactor()
    go:AddComponent(typeof(UnityEngine.UI.GraphicRaycaster))
    go:AddComponent(typeof(SortOrderedRenderAgent))
    go.layer = UI_LAYER;
    GUIManager._rootRectTransform = go:GetComponent(typeof(UnityEngine.RectTransform))
    GUIManager.OnResize()
    local scaler = go:AddComponent(typeof(CanvasScaler));
    scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
    scaler.referenceResolution = Vector2(GUIManager.PANEL_WIDTH, GUIManager.PANEL_HEIGHT);
end

function GUILayerManager.OnResize()
    if not GUILayerManager._inited then return end
    local matchWidthOrHeight = 0
    if GUIManager.canvasWidth / GUIManager.canvasHeight >= GUIManager.PANEL_WIDTH / GUIManager.PANEL_HEIGHT then
        matchWidthOrHeight = 1
    end
    for k, layer in ipairs(GUILayerOrder) do
       local trans = GUILayerManager._layerTransformDict[layer]
       local scaler = trans.gameObject:GetComponent(typeof(CanvasScaler));
       scaler.matchWidthOrHeight = matchWidthOrHeight
    end 
end

function GUILayerManager.GetUILayerTransform(layer)
    -- LoggerHelper.Log("layer:", layer)
    return GUILayerManager._layerTransformDict[layer]
end

--增加或者覆盖层内原UI
function GUILayerManager.PushToChildLayer(go,panelName)
    local setting = GUIManager._panelSettings[panelName]  
    local layer = setting.layer
    local parentTrans = GUILayerManager.GetUILayerTransform(layer)
    go.transform:SetParent(parentTrans)
end

function GUILayerManager.ShowLayer(layer)
    local layerTrans = GUILayerManager._layerTransformDict[layer]
    GUILayerManager._layerVisibleDict[layer] = true
    if layerTrans ~= nil then
        GameWorld.SetCanvasesState(layerTrans.gameObject, true)
    end
end

function GUILayerManager.HideLayer(layer)
    if layer == "LayerUITop" then return end
    local layerTrans = GUILayerManager._layerTransformDict[layer]
    GUILayerManager._layerVisibleDict[layer] = false
    if layerTrans ~= nil then
        GameWorld.SetCanvasesState(layerTrans.gameObject, false)
    end
end

function GUILayerManager.ShowOtherLayers(layer)
    for k, otherLayer in ipairs(GUILayerOrder) do
        if otherLayer ~= layer then
            GUILayerManager.ShowLayer(otherLayer)
        end
    end
end

function GUILayerManager.HideOtherLayers(layer)
    for k, otherLayer in ipairs(GUILayerOrder) do
        if otherLayer ~= layer then
            GUILayerManager.HideLayer(otherLayer)
        end
    end
end

function GUILayerManager.ShowAllLayers()
    for k, layer in ipairs(GUILayerOrder) do
        GUILayerManager.ShowLayer(layer)
    end
end

function GUILayerManager.HideAllLayers()
    for k, layer in ipairs(GUILayerOrder) do
        GUILayerManager.HideLayer(layer)
    end
end

function GUILayerManager.ResetAllLayers()
    GUIManager.ClearPanelData()
    for k, layer in ipairs(GUILayerOrder) do
        local trans = GUILayerManager._layerTransformDict[layer]
        for i = 1, trans.childCount do
            local item = trans:GetChild(i-1).gameObject
            GameObject.Destroy(item)
        end
    end
end

function GUILayerManager.GetLayerVisible(layer) 
    return GUILayerManager._layerVisibleDict[layer]
end

return GUILayerManager