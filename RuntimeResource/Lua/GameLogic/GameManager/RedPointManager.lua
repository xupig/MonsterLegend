--RedPointManager.lua
--游戏里功能里的红点提示管理器
local RedPointManager = {}
local allFunctionCheckRedPoint = {}
local public_config = GameWorld.public_config
local PartnerType = GameConfig.EnumType.PartnerType
local PanelsConfig = GameConfig.PanelsConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local FunctionTimingDataHelper = GameDataHelper.FunctionTimingDataHelper
local MenuConfig = GameConfig.MenuConfig
local GUIManager

require "PlayerManager/PlayerData/RedPointData"
local RedPointData = ClassTypes.RedPointData

function RedPointManager:Init()	
	self._allEntityByFuncId = {}
	self._allFirstEntityById = {}
	self._allEntityByPanelName = {}
	self._allEntityByTimingId = {}
	self._rootNode = nil
	self:InitCallbackFunc()
end

function RedPointManager:OnEnterWorld()
	GUIManager = GameManager.GUIManager
	self:InitRedpointData()
	self:AddEventListeners()
end

function RedPointManager:OnLeaveWorld()
	self._allEntityByFuncId = {}
	self._allFirstEntityById = {}
	self._allEntityByPanelName = {}
	self._rootNode = nil
    self:RemoveEventListeners()
end

function RedPointManager:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.RED_POINT_STATUS_CHANGE, self._redPointChangeHandle)
end

function RedPointManager:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.RED_POINT_STATUS_CHANGE, self._redPointChangeHandle)
end

function RedPointManager:InitCallbackFunc()
	self._redPointChangeHandle = function(functionId, status) self:RedPointChangeHandle(functionId, status) end
end

function RedPointManager:RedPointChangeHandle(functionId, status)
	-- LoggerHelper.Log("Sam :RedPointChangeHandle functionId: " .. tostring(functionId) .."status:" .. tostring(status))
	if not FunctionOpenDataHelper:CheckFunctionOpen(functionId) then
		return
	end

	-- LoggerHelper.Log("Sam :functionId: " .. tostring(functionId) .."status:" .. tostring(status))
	local node = self._allEntityByFuncId[functionId]
	if node then
		node:SetRedPoint(status)
	else
		LoggerHelper.Error("RedPointManager can not find node, functionId =====" .. tostring(functionId), true)
	end
end

function RedPointManager:RegisterNodeUpdateFunc(node, func)
	if node then
		node:SetUpdateRedPointFunc(func)
	end
end

function RedPointManager:RegisterHeadUpdateFunc(func)
	self._headNode:SetUpdateRedPointFunc(func)
end

function RedPointManager:RegisterHeadUpdateFunc2(func)
	self._headNode:SetUpdateRedPointFunc2(func)
end

function RedPointManager:GetRedPointDataByFuncId(functionId)
	return self._allEntityByFuncId[functionId]
end

function RedPointManager:GetRedPointDataByFirstId(id)
	return self._allFirstEntityById[id]
end

function RedPointManager:GetRedPointDataByPanelName(panelName)
	return self._allEntityByPanelName[panelName]
end

function RedPointManager:GetRedPointDataByTimingId(timingId)
	return self._allEntityByTimingId[timingId]
end

function RedPointManager:InitRedpointData()
	self._rootNode = self:CreateEmptyNode()
	self._headNode = self:CreateEmptyNode()
	self._headNode:SetParent(self._rootNode)
	-- self._timingNode = self:CreateEmptyNode()
	-- self._timingNode:SetParent(self._rootNode)

	--设置主界面菜单节点
	local mainNodeIds =  FunctionOpenDataHelper:GetAllIdByButtonType(MenuConfig.MAIN_MENU)
	for i,v in ipairs(mainNodeIds) do
		local mainNodes = self:CreatePanelNodes(v)
		if  mainNodes ~= nil and (not table.isEmpty(mainNodes)) then
			for i,mainNode in ipairs(mainNodes) do
				mainNode:SetParent(self._rootNode)
			end 
		end
	end

	--设置二级菜单节点
	local secondMenuNodeIds =  FunctionOpenDataHelper:GetAllIdByButtonType(MenuConfig.SUB_MENU)
	for i,v in ipairs(secondMenuNodeIds) do
		local secondMenuNodes = self:CreatePanelNodes(v)
		if secondMenuNodes ~= nil and (not table.isEmpty(secondMenuNodes)) then
			for i,secondMenuNode in ipairs(secondMenuNodes) do
				secondMenuNode:SetParent(self._headNode)
			end
		end
	end		

	--设置动态菜单
	local dynamicMenuNodeIds =  FunctionOpenDataHelper:GetAllIdByButtonType(MenuConfig.DYNAMIC_MAIN_MENU)
	for i,v in ipairs(dynamicMenuNodeIds) do
		local dynamicMenuNodes = self:CreatePanelNodes(v)
		if dynamicMenuNodes ~= nil and (not table.isEmpty(dynamicMenuNodes)) then
			for i,dynamicMenuNode in ipairs(dynamicMenuNodes) do
				dynamicMenuNode:SetParent(self._rootNode)
			end
		end
	end	

	--设置固定位置菜单
	local staticMenuNodeIds = FunctionOpenDataHelper:GetAllIdByButtonType(MenuConfig.STATIC_MENU)
	for i,v in ipairs(staticMenuNodeIds) do
		local staticMenuNodes = self:CreatePanelNodes(v)
		if staticMenuNodes ~= nil and (not table.isEmpty(staticMenuNodes)) then
			for i,staticMenuNode in ipairs(staticMenuNodes) do
				staticMenuNode:SetParent(self._rootNode)
			end
		end
	end	


	-- --设置限时菜单
	-- local timingIds = FunctionTimingDataHelper:GetAllId()
	-- for i,id in ipairs(timingIds) do
	-- 	local node = self:CreateTimingNode(id)
	-- 	node:SetParent(self._timingNode)
	-- end
end

function RedPointManager:CreatePanelNodes(functionId)
	local mainNodes = {}
	local btnFunc = FunctionOpenDataHelper:GetButtonFunc(functionId)
	if btnFunc ~= nil then
		if btnFunc[MenuConfig.SHOW_MANY_PANEL] then--7 不同状态显示不同的panel
			local panelNames = btnFunc[MenuConfig.SHOW_MANY_PANEL]
			for i,panelName in ipairs(panelNames) do
				local mainNode = self:CreatePanelNode(functionId, panelName)
				table.insert(mainNodes, mainNode)
			end
		elseif btnFunc[MenuConfig.SHOW_PANEL] then--8   --打开面板
			local panelName = btnFunc[MenuConfig.SHOW_PANEL][1]
			local mainNode = self:CreatePanelNode(functionId, panelName)
			table.insert(mainNodes, mainNode)
		elseif btnFunc[MenuConfig.SHOW_PANEL] then--9   --寻找NPC
			return mainNodes
		end
	end

	return mainNodes
end

function RedPointManager:CreatePanelNode(functionId, panelName)
	local mainNode = RedPointData()
	mainNode:SetPanelName(panelName) 
	self:AddPanelChildNode(functionId, panelName, mainNode)
	self._allEntityByPanelName[panelName] = mainNode
	self._allFirstEntityById[functionId] = mainNode
	return mainNode
end

function RedPointManager:AddPanelChildNode(functionId, panelName, parent)	
	-- local btnFunc = FunctionOpenDataHelper:GetButtonFunc(functionId)
	-- if btnFunc ~= nil and btnFunc[MenuConfig.SHOW_PANEL] ~= nil then
	local functionDatas,secTabMaxCount = FunctionOpenDataHelper:GetFunctionOpensInPanel(panelName)
	local tabCount = #functionDatas         --一级标签数量

	if tabCount > 0 then
		local button_func3 = FunctionOpenDataHelper.GetButtonFunc3ByPanelName(panelName)
		if button_func3 and button_func3[2] then
			--处理类似竞技功能,打开的是玩法总入口，里面链接各个玩法
			local funcIds = FunctionOpenDataHelper:GetFuncsByPanelName(panelName)
			for _, id in ipairs(funcIds) do
				local funcNode = self:CreateFuncNode(id)
				funcNode:SetParent(parent)
			end
		else
			for firstIndex, datas in ipairs(functionDatas) do
				local firstNode = self:CreateFirstNode(firstIndex, datas)	
				firstNode:SetParent(parent) 
			end
		end
	end
	-- end
end

function RedPointManager:CreateFirstNode(firstIndex, datas)
	local firstNode = RedPointData()
	firstNode:SetFirstIndex(firstIndex)					
	for secondIndex, data in ipairs(datas) do
		local secondNode = self:CreateSecondNode(firstIndex, secondIndex, data)
		secondNode:SetParent(firstNode) 
	end
	return firstNode
end

function RedPointManager:CreateSecondNode(firstIndex, secondIndex, data)
	local secondNode = RedPointData()	
	secondNode:SetFirstIndex(firstIndex)
	secondNode:GetSecondIndex(secondIndex)
	secondNode:SetFunctionId(data.id)
	self._allEntityByFuncId[data.id] = secondNode
	return secondNode
end

function RedPointManager:CreateFuncNode(functionId)
	local funcNode = RedPointData()
	funcNode:SetFunctionId(functionId)
	self._allEntityByFuncId[functionId] = funcNode
	return funcNode
end

function RedPointManager:CreateTimingNode(timingId)
	local node = RedPointData()
	node:SetTimingId(timingId)
	self._allEntityByTimingId[timingId] = node
	return node
end

function RedPointManager:CreateEmptyNode()
	local emptyNode = RedPointData()
	return emptyNode
end

--创建变强实体
--chineseId：变强显示中文id
--functionId:点击变强后打开的functionId
function RedPointManager:CreateStrengthenNodeByFunId(chineseId, functionId, func)
	local strengthenNode = RedPointData()
	strengthenNode:SetStrengthTextId(chineseId)
	if func == nil then
		func = function()
			GUIManager.ShowPanelByFunctionId(functionId)
		end
	end
	strengthenNode:SetStrengthCallBack(func)
	if self._allEntityByFuncId[functionId] then
		strengthenNode:SetParent(self._allEntityByFuncId[functionId])
	end
	return strengthenNode
end

--创建变强实体
--chineseId：变强显示中文id
--func:点击变强后的回调处理方法
function RedPointManager:CreateStrengthenNodeByFunc(chineseId, func, parentNode)
	local strengthenNode = RedPointData()
	strengthenNode:SetStrengthTextId(chineseId)
	strengthenNode:SetStrengthCallBack(func)
	strengthenNode:SetParent(parentNode)
	return strengthenNode 
end

RedPointManager:Init()

return RedPointManager