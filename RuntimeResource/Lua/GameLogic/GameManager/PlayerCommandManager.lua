-- 主角指令集管理器
require "GameManager/PlayerAction/ActionBase"
require "GameManager/PlayerAction/ActionNormal"
require "GameManager/PlayerAction/ActionPlaySkill"
require "GameManager/PlayerAction/ActionCallBack"
require "GameManager/PlayerAction/ActionFindPosition"
require "GameManager/PlayerAction/ActionMoveByStick"
require "GameManager/PlayerAction/ActionFindNPC"
require "GameManager/PlayerAction/ActionCollect"
require "GameManager/PlayerAction/ActionFindCollectItem"
-- require "GameManager/PlayerAction/ActionRescue"
require "GameManager/PlayerAction/ActionFindKillMonsterPoint"
-- require "GameManager/PlayerAction/ActionMapTeleport"
require "GameManager/PlayerAction/ActionFindCollectClientResource"
require "GameManager/PlayerAction/ActionCommonCollect"
require "GameManager/PlayerAction/ActionMapChange"
require "GameManager/PlayerAction/ActionCollectClientResource"
require "GameManager/PlayerAction/ActionGotoBossScene"
require "GameManager/PlayerAction/ActionInstChange"
require "GameManager/PlayerAction/ActionPassInst"
require "GameManager/PlayerAction/ActionInstStartWait"
require "GameManager/PlayerAction/ActionGotoBossHomeScene"
require "GameManager/PlayerAction/ActionFindTeleportPosition"
require "GameManager/PlayerAction/ActionGotoBossSceneElite"
require "GameManager/PlayerAction/ActionGotoBackWoodsScene"
require "GameManager/PlayerAction/ActionGotoGodIslandScene"

PlayerCommandManager = {}

local PlayerActionManager = GameManager.PlayerActionManager
local ActionFindPosition = ClassTypes.ActionFindPosition
local ControlStickState = GameConfig.ControlStickState
local ActionNormal = ClassTypes.ActionNormal
local ActionPlaySkill = ClassTypes.ActionPlaySkill
local ActionCallBack = ClassTypes.ActionCallBack
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ActionMoveByStick = ClassTypes.ActionMoveByStick
local ActionFindNPC = ClassTypes.ActionFindNPC
local ActionCollect = ClassTypes.ActionCollect
local ActionFindCollectItem = ClassTypes.ActionFindCollectItem
-- local ActionRescue = ClassTypes.ActionRescue
local ActionFindKillMonsterPoint = ClassTypes.ActionFindKillMonsterPoint
-- local ActionMapTeleport = ClassTypes.ActionMapTeleport
local ActionFindCollectClientResource = ClassTypes.ActionFindCollectClientResource
local ActionCommonCollect = ClassTypes.ActionCommonCollect
local ActionMapChange = ClassTypes.ActionMapChange
local ActionCollectClientResource = ClassTypes.ActionCollectClientResource
local ActionGotoBossScene = ClassTypes.ActionGotoBossScene
local ActionInstChange = ClassTypes.ActionInstChange
local ActionPassInst = ClassTypes.ActionPassInst
local ActionInstStartWait = ClassTypes.ActionInstStartWait
local StateConfig = GameConfig.StateConfig
local SkillDataHelper = GameDataHelper.SkillDataHelper
local GameSceneManager = GameManager.GameSceneManager
local ActionGotoBossHomeScene = ClassTypes.ActionGotoBossHomeScene
local ActionFindTeleportPosition = ClassTypes.ActionFindTeleportPosition
local ActionGotoBossSceneElite = ClassTypes.ActionGotoBossSceneElite
local ActionGotoBackWoodsScene = ClassTypes.ActionGotoBackWoodsScene
local ActionGotoGodIslandScene = ClassTypes.ActionGotoGodIslandScene

function PlayerCommandManager:Init()--单例初始化方法
    self.groundSlot = GameWorld.Player().groundPosToSlot
    self.groundPosTransformToSlot = GameWorld.Player().groundPosTransformToSlot
    
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandPlaySkill,
        function(skillPos)
            if self.groundSlot[skillPos] ~= nil and self.groundSlot[skillPos] ~= 0 then
                skillPos = self.groundSlot[skillPos]
            else
                return
            end
            self:PlaySkill(skillPos)
        end)
    
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandReleaseSkill,
        function(skillPos)
            if self.groundSlot[skillPos] ~= nil then
                skillPos = self.groundSlot[skillPos]
            else
                return
            end
            self:ReleaseSkill(skillPos)
        end)
    
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandMoveByStick,
        function()self:MoveByStick() end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandFindPosition,
        function(targetMapId, targetPos, useLittleShoes, needMoveEvent)self:FindOtherScenePosition(targetMapId, targetPos, useLittleShoes, needMoveEvent) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandFindNPC,
        function(targetMapId, npcId, targetPosition, endDistance, useLittleShoes, teleportPos, needMoveEvent)self:FindOtherSceneNPC(targetMapId, npcId, targetPosition, endDistance, useLittleShoes, teleportPos, needMoveEvent) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandKillMonsterPoint,
        function(targetMapId, pos, endDistance, doMoveDistance, useLittleShoes, needMoveEvent)self:FindOtherSceneKillMonsterPoint(targetMapId, pos, endDistance, doMoveDistance, useLittleShoes, needMoveEvent) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandPassInst,
        function(targetInstId)self:FindOtherScenePassInst(targetInstId) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandBossSceneKillMonsterPoint,
        function(bossId, targetMapId, pos, endDistance)self:FindBossSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandGodIslandSceneKillMonsterPoint,
        function(bossId, targetMapId, pos, endDistance)self:FindGodIslandSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandFindCollectClientResource,
        function(targetMapId, pos, endDistance, useLittleShoes)self:FindCollectClientResource(targetMapId, pos, endDistance, useLittleShoes) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandCollectItem,
        function(itemEid, cdtime)self:CollectItem(itemEid, cdtime) end)
    -- EventDispatcher:AddEventListener(GameEvents.PlayerCommandRescue,
    --     function(playerEid, cdtime)self:Rescue(playerEid, cdtime) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandCommonCollect,
        function(cdtime)self:CommonCollect(cdtime) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandCollectClientResource,
        function(entityCfgId, cdtime)self:CollectClientResource(entityCfgId, cdtime) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandBossHomeSceneKillMonsterPoint,
        function(bossId, targetMapId, pos, endDistance)self:FindBossHomeSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandBossSceneKillElitePoint,
        function(bossId, targetMapId, pos, endDistance)self:FindBossSceneKillElitePoint(id, targetMapId, pos, endDistance) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandBackWoodsSceneKillMonsterPoint,
        function(bossId, targetMapId, pos, endDistance)self:FindBackWoodsSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandBackFindPositionAndCollect,
        function(targetPos, itemEid, cdtime)self:FindPositionAndCollect(targetPos, itemEid, cdtime) end)
end

function PlayerCommandManager:OnPlayerEnterWorld()
    self:Init()
    GameWorld.Player():InitSkillSlot()
end

function PlayerCommandManager:OnPlayerLeaveWorld()

end

-- 一般操作
function PlayerCommandManager:MoveByStick(skillPos)
    local queue = {}
    local action = ActionMoveByStick{duration = -1, player = GameWorld.Player(), source = "MoveByStick"}
    table.insert(queue, action)
    PlayerActionManager:AddActionQueue(queue)
end

--
function PlayerCommandManager:PlaySkill(skillPos)
    local queue = {}
    local target = GameWorld.GetEntity(GameWorld.Player().cs.target_id)
    local action
    if target ~= nil then
        local skillId = GameWorld.Player():GetSkillIdByPos(skillPos)
        if skillId > 0 and SkillDataHelper.GetSkillCanAttackRange(skillId) ~= 0 then
            local maxDistance = SkillDataHelper.GetSkillCanAttackRange(skillId) * 0.01
            local curDis = Vector3.Distance(GameWorld.Player():GetPosition(), target:GetPosition())
            if curDis > maxDistance then
                action = ActionFindPosition{targetPosition = target:GetPosition(), duration = 0, endDistance = maxDistance,
                    player = GameWorld.Player(), needSetRide = false, source = "PlaySkill + skillPos ===" .. tostring(skillPos) .. "skillId ==" .. tostring(skillId)
                    .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)}
                table.insert(queue, action)
            end
        end
    end
    
    action = ActionPlaySkill{skillPos = skillPos, duration = 1, player = GameWorld.Player(), source= "ActionPlaySkill"}
    table.insert(queue, action)
    PlayerActionManager:AddActionQueue(queue)
end

--
function PlayerCommandManager:ReleaseSkill(skillPos)
    GameWorld.Player():ReleaseSkillByPos(skillPos)
end

function PlayerCommandManager:FindOtherScenePosition(targetMapId, targetPos, useLittleShoes, needMoveEvent)
    local queue = {}
    if targetMapId == nil then
        targetMapId = GameSceneManager:GetCurrMapID()
    end
    table.insert(queue, ActionMapChange{source = "FindOtherScenePosition:ActionMapChange",targetMapId = targetMapId, targetPosition = targetPos, player = GameWorld.Player(), useLittleShoes = useLittleShoes})
    table.insert(queue, ActionFindPosition{targetPosition = targetPos, duration = 0, player = GameWorld.Player(), needMoveEvent = needMoveEvent,
        source = "FindOtherScenePosition + targetPosition===" .. tostring(targetPos.x) .. ":" .. tostring(targetPos.y) .. ":" .. tostring(targetPos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindOtherSceneNPC(targetMapId, npcId, targetPosition, endDistance, useLittleShoes, teleportPos, needMoveEvent)
    local queue = {}
    if targetMapId == nil then
        targetMapId = GameSceneManager:GetCurrMapID()
    end
    if teleportPos ~= nil then
        --传送点切图
        table.insert(queue, ActionFindTeleportPosition{source = "FindOtherSceneNPC:ActionFindTeleportPosition",targetMapId = targetMapId, targetPosition = teleportPos, duration = 0, player = GameWorld.Player()})
    else
        table.insert(queue, ActionMapChange{source = "FindOtherSceneNPC:ActionMapChange",targetMapId = targetMapId, targetPosition = targetPosition, player = GameWorld.Player(), useLittleShoes = useLittleShoes})
    end
    table.insert(queue, ActionFindNPC{npcId = npcId, targetPosition = targetPosition, duration = 0, endDistance = endDistance, player = GameWorld.Player(), needMoveEvent = needMoveEvent,
        source = "FindOtherSceneNPC + ActionFindNPC targetPosition===" .. tostring(targetPosition.x) .. ":" .. tostring(targetPosition.y) .. ":" .. tostring(targetPosition.z) .. "npcId ===" .. tostring(npcId)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindOtherSceneKillMonsterPoint(targetMapId, pos, endDistance, doMoveDistance, useLittleShoes, needMoveEvent)
    local queue = {}
    if targetMapId == nil then
        targetMapId = GameSceneManager:GetCurrMapID()
    end
    table.insert(queue, ActionMapChange{source = "FindOtherSceneKillMonsterPoint:ActionMapChange",targetMapId = targetMapId, targetPosition = pos, player = GameWorld.Player(), useLittleShoes = useLittleShoes})
    table.insert(queue, ActionFindKillMonsterPoint{pos = pos, duration = 0, endDistance = endDistance, doMoveDistance = doMoveDistance, player = GameWorld.Player(), needMoveEvent = needMoveEvent,
        source = "FindOtherSceneKillMonsterPoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindOtherScenePassInst(targetInstId)
    local queue = {}
    table.insert(queue, ActionInstChange{source = "FindOtherScenePassInst:ActionInstChange",targetInstId = targetInstId, player = GameWorld.Player()})
    table.insert(queue, ActionInstStartWait{source = "FindOtherScenePassInst:ActionInstStartWait",targetInstId = targetInstId, player = GameWorld.Player()})
    table.insert(queue, ActionPassInst{source = "FindOtherScenePassInst:ActionPassInst",targetInstId = targetInstId, player = GameWorld.Player()})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindBossSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance)
    local queue = {}
    table.insert(queue, ActionGotoBossScene{source = "FindBossSceneKillMonsterPoint:ActionGotoBossScene",bossId = bossId, targetMapId = targetMapId, player = GameWorld.Player()})
    table.insert(queue, ActionFindKillMonsterPoint{source = "FindBossSceneKillMonsterPoint:ActionFindKillMonsterPoint",pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
        source = "FindBossSceneKillMonsterPoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindBossHomeSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance)
    local queue = {}
    table.insert(queue, ActionGotoBossHomeScene{source = "FindBossHomeSceneKillMonsterPoint:ActionGotoBossHomeScene",bossId = bossId, targetMapId = targetMapId, player = GameWorld.Player()})
    table.insert(queue, ActionFindKillMonsterPoint{pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
        source = "FindBossHomeSceneKillMonsterPoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindBackWoodsSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance)
    local queue = {}
    table.insert(queue, ActionGotoBackWoodsScene{source = "FindBackWoodsSceneKillMonsterPoint:ActionGotoBackWoodsScene",bossId = bossId, targetMapId = targetMapId, player = GameWorld.Player()})
    table.insert(queue, ActionFindKillMonsterPoint{pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
        source = "FindBackWoodsSceneKillMonsterPoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindBossSceneKillElitePoint(id, targetMapId, pos, endDistance)
    local queue = {}
    table.insert(queue, ActionGotoBossSceneElite{source = "FindBossSceneKillElitePoint:ActionGotoBossSceneElite",bossId = id, targetMapId = targetMapId, player = GameWorld.Player()})
    table.insert(queue, ActionFindKillMonsterPoint{pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
        source = "FindBossSceneKillElitePoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

function PlayerCommandManager:FindGodIslandSceneKillMonsterPoint(bossId, targetMapId, pos, endDistance)
    local queue = {}
    table.insert(queue, ActionGotoGodIslandScene{source = "FindGodIslandSceneKillMonsterPoint:ActionGotoGodIslandScene",bossId = bossId, targetMapId = targetMapId, player = GameWorld.Player()})
    if pos ~= nil then
        table.insert(queue, ActionFindKillMonsterPoint{pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
            source = "FindGodIslandSceneKillMonsterPoint + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
            .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    end
    PlayerActionManager:AddActionQueue(queue)
end

--自动采集客户端实体
function PlayerCommandManager:FindCollectClientResource(targetMapId, pos, endDistance, useLittleShoes)
    local queue = {}
    if targetMapId == nil then
        targetMapId = GameSceneManager:GetCurrMapID()
    end
    table.insert(queue, ActionMapChange{source = "FindCollectClientResource:ActionMapChange",targetMapId = targetMapId, targetPosition = pos, player = GameWorld.Player(), useLittleShoes = useLittleShoes})
    table.insert(queue, ActionFindCollectClientResource{pos = pos, duration = 0, endDistance = endDistance, player = GameWorld.Player(),
        source = "FindCollectClientResource + targetPosition===" .. tostring(pos.x) .. ":" .. tostring(pos.y) .. ":" .. tostring(pos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

--找到一个采集点并采集
function PlayerCommandManager:FindPositionAndCollect(targetPos)
    local queue = {}
    table.insert(queue, ActionFindPosition{targetPosition = targetPos, duration = 0, player = GameWorld.Player(),
        source = "FindPositionAndCollect + ActionFindPosition + targetPosition===" .. tostring(targetPos.x) .. ":" .. tostring(targetPos.y) .. ":" .. tostring(targetPos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    table.insert(queue, ActionFindCollectItem{pos = targetPos, duration = 0, endDistance = 0, player = GameWorld.Player(),
        source = "FindPositionAndCollect + ActionFindCollectItem + targetPosition===" .. tostring(targetPos.x) .. ":" .. tostring(targetPos.y) .. ":" .. tostring(targetPos.z)
        .. "playerposition" .. tostring(GameWorld.Player():GetPosition().x) .. ":" .. tostring(GameWorld.Player():GetPosition().y) .. ":" .. tostring(GameWorld.Player():GetPosition().z)})
    PlayerActionManager:AddActionQueue(queue)
end

--采集
function PlayerCommandManager:CollectItem(itemEid, cdtime)
    local queue = {}
    local action = ActionCollect{source = "CollectItem:ActionCollect",duration = 0, itemEid = itemEid, cdtime = cdtime, player = GameWorld.Player()}
    table.insert(queue, action)
    PlayerActionManager:AddActionQueue(queue)
end

--救赎
-- function PlayerCommandManager:Rescue(playerEid, cdtime)
--     local queue = {}
--     local action = ActionRescue{duration = 0, playerEid = playerEid, cdtime = cdtime, player = GameWorld.Player()}
--     table.insert(queue, action)
--     PlayerActionManager:AddActionQueue(queue)
-- end
--通用采集
function PlayerCommandManager:CommonCollect(cdtime)
    local queue = {}
    local action = ActionCommonCollect{source = "CommonCollect:ActionCommonCollect",duration = 0, cdtime = cdtime, player = GameWorld.Player()}
    table.insert(queue, action)
    PlayerActionManager:AddActionQueue(queue)
end

--采集客户端实体
function PlayerCommandManager:CollectClientResource(entityCfgId, cdtime)
    local queue = {}
    local action = ActionCollectClientResource{source = "CollectClientResource:ActionCollectClientResource",duration = 0, entityCfgId = entityCfgId, cdtime = cdtime, player = GameWorld.Player()}
    table.insert(queue, action)
    PlayerActionManager:AddActionQueue(queue)
end

--传送
-- function PlayerCommandManager:Teleport(regionalMapId)
--     local queue = {}
--     local action = ActionMapTeleport{regionalMapId = regionalMapId, player = GameWorld.Player()}
--     table.insert(queue, action)
--     PlayerActionManager:AddActionQueue(queue)
-- end
--PlayerCommandManager:Init()
return PlayerCommandManager
