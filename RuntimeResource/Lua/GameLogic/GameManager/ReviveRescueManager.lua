--ReviveRescueManager.lua
ReviveRescueManager = {}
local GameSceneManager = GameManager.GameSceneManager
local MapDataHelper = GameDataHelper.MapDataHelper
local action_config = require("ServerConfig/action_config")

function ReviveRescueManager:Init()
    -- self._rescueList = {}--救赎列表
    EventDispatcher:AddEventListener(GameEvents.PlayerDie, function()
        self:GetDieMessage()
    end)
end

function ReviveRescueManager:HandleResp(action_id, error_code, args)
    --LoggerHelper.Log("Ash: ReviveRescueManager HandleResp: " .. action_id .. " " .. error_code .. " " .. PrintTable:TableToStr(args))
    if error_code ~= 0 then
        return
    end
    -- local sceneType = GameSceneManager:GetCurSceneType()
    -- if sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then
    --     return
    -- end
    if action_id == action_config.MGR_REVIVE_APPLY_REVIVE then
        self:OnRevive()
    elseif action_id == action_config.MGR_REVIVE_GET_DIE_MESSAGE then
        if GameWorld.Player().cur_hp == 0 and args and args[1] and args[1] ~= 0 then
            GameManager.GUIManager.ShowPanel(PanelsConfig.Redemption, args)
        end
    end
end

function ReviveRescueManager:GetReviveTireCount()
    return GameWorld.Player().revive_tire_count or {}
end

function ReviveRescueManager:GetReviveTireDelTime()
    return GameWorld.Player().revive_tire_del_time or {}
end

function ReviveRescueManager:OnRevive()
    GameManager.GUIManager.ClosePanel(PanelsConfig.Redemption)
    GameWorld.Player():PlaySfx(VisualFxConfig.Revive)
end

--请求复活
function ReviveRescueManager:RequestRevive(type)
    if GameWorld.Player().cur_hp ~= 0 then--可能已经复活了，把UI关闭掉，为保底还是发复活请求
        GameManager.GUIManager.ClosePanel(PanelsConfig.Redemption)
    end
    GameWorld.Player().server.revive_action_req(action_config.MGR_REVIVE_APPLY_REVIVE, tostring(type))
end

function ReviveRescueManager:GetDieMessage()
    GameWorld.Player().server.revive_action_req(action_config.MGR_REVIVE_GET_DIE_MESSAGE, tostring(0))
end

ReviveRescueManager:Init()
return ReviveRescueManager
