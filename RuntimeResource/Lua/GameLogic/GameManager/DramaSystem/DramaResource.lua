--DramaResource.lua
DramaResource = {}

local DramaConfig = GameConfig.DramaConfig
local DramaDataPool = DramaDataPool
local GameWorld = GameWorld
local LuaPreloadPathsBuilder = GameMain.LuaPreloadPathsBuilder
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local _dramaGetMap = {}

function DramaResource.InitDramaGetMap()
    _dramaGetMap["CreateEntity"] = DramaResource.GetCreateEntityResource
    _dramaGetMap["PlayVisualFX"] = DramaResource.GetPlayVisualFXResource
    _dramaGetMap["PlaySkill"] = DramaResource.GetPlaySkillResource
    _dramaGetMap["PlaySkill"] = DramaResource.GetPlaySkillResource
    _dramaGetMap["ReadyAnimatorController"] = DramaResource.GetAnimatorControllerResource
end

function DramaResource.PreloadDramaResources(dramaNames, callback)
    LuaPreloadPathsBuilder.Clear()
    for i=1, #dramaNames do
        local dramaName = dramaNames[i]
        --LoggerHelper.Error("PreloadDramaResources:" .. dramaName)
        DramaResource.GetDramaPreloadResource(dramaName)
    end
    GameWorld.PreloadAssetFromBuilder(callback)
end

function DramaResource.GetDramaPreloadResource(dramaName)
    local dramaData = DramaDataPool:GetDramaData(dramaName)
    local _actions = dramaData.actionList
    for i = 1,#_actions do
        local actionName = _actions[i][DramaConfig.TAG_ACTION]
        local func = _dramaGetMap[actionName]
        if func ~= nil then
            func(_actions[i]);
        end
    end
end

function DramaResource.GetCreateEntityResource(action)
    local entityType = action[DramaConfig.TAG_ARG1]
    local tid = tonumber(action[DramaConfig.TAG_ARG3])
    local actorId = 0
    if entityType == "Dummy" then
        actorId = MonsterDataHelper.GetMonsterModel(tid)
	elseif entityType == "NPC" then
        local npc = NPCDataHelper.GetNPCData(tid)
        actorId = npc.model
    elseif entityType == "Avatar" then
        actorId =  RoleDataHelper.GetModelByVocation(tid)
    end
    if actorId == 0 then
        return
    end
    LuaPreloadPathsBuilder.AddActorID(actorId)
end

function DramaResource.GetPlayVisualFXResource(action)
    local path = action[DramaConfig.TAG_ARG1]
    LuaPreloadPathsBuilder.AddPath(path)
end

function DramaResource.GetPlaySkillResource(action)
    local skillId = tonumber(action[DramaConfig.TAG_ARG2])
    LuaPreloadPathsBuilder.AddSkillID(skillId)
end

function DramaResource.GetAnimatorControllerResource(action)
    local path = action[DramaConfig.TAG_ARG1]
    LuaPreloadPathsBuilder.AddPath(path)
end

DramaResource.InitDramaGetMap()

return DramaResource