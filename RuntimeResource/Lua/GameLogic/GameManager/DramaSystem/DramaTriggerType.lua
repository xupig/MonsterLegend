DramaTriggerType = {}

DramaTriggerType.AcceptableTask = "ACCEPTABLE_QUEST"
DramaTriggerType.AcceptTask = "ACCEPT_QUEST"
DramaTriggerType.ReachTask = "FINISH_QUEST"
DramaTriggerType.CommitTask = "COMMIT_QUEST"
DramaTriggerType.CreateDramaDummy = "CREATE_DRAMA_DUMMY"
DramaTriggerType.CreateDramaNPC = "CREATE_DRAMA_NPC"
DramaTriggerType.EntityMoveEnd = "ENTITY_MOVE_END"
DramaTriggerType.CreateDramaNPCControllerLoaded = "CREATE_DRAMA_NPC_CONTROLLER_LOADED"

DramaTriggerType.EnterMapType = "ENTER_MAP_TYPE"
DramaTriggerType.EnterMap = "ENTER_MAP"
DramaTriggerType.LeaveMap = "LEAVE_MAP"
DramaTriggerType.CombatWin = "COMBAT_INS_WIN"
DramaTriggerType.CombatLose = "COMBAT_INS_LOSE"
DramaTriggerType.ClickButton = "CLICK_BTN"
DramaTriggerType.FingerGuideEnd = "FINGER_GUIDE_END"
DramaTriggerType.GetItem = "GET_ITEM"
DramaTriggerType.HpChange = "HP_CHANGE"
DramaTriggerType.LevelUp = "LEVEL_UP"
DramaTriggerType.FightForceChange = "Fight_FORCE_CHANGE"
DramaTriggerType.Dead = "DEAD"
DramaTriggerType.OpenFunction = "OPEN_FUNCTION"
DramaTriggerType.JoinActivity = "JOIN_ACTIVITY"
DramaTriggerType.PanelDataResp = "PANEL_DATA_RESP"
DramaTriggerType.ShowPanel = "SHOW_PANEL"
DramaTriggerType.ClosePanel = "CLOSE_PANEL"
DramaTriggerType.FinishFindWay = "FINISH_FIND_WAY"
DramaTriggerType.MonsterDead = "MONSTER_DEAD"
DramaTriggerType.MonsterEnterGame = "MONSTER_ENTER_GAME"
DramaTriggerType.ON_TWEEN_POSITION_END = 'ON_TWEEN_POSITION_END'
DramaTriggerType.CLICK_NPC = 'CLICK_NPC'
DramaTriggerType.HIT_MONSTER = 'HIT_MONSTER'

DramaTriggerType.EQUIP_UPGRADE = "EQUIP_UPGRADE"
DramaTriggerType.CGEnd = "CG_END"
DramaTriggerType.WAIT_SERVER_RESP = "WAIT_SERVER_RESP"
DramaTriggerType.RuneWishEnd = "RuneWishEnd"
DramaTriggerType.StarSoulSystem = "StarSoulSystem"
DramaTriggerType.PetSystem = "PetSystem"

DramaTriggerType.SKIP_DIALOG = "SKIP_DIALOG"

DramaTriggerType.ON_OPEN_VIEW = 'ON_OPEN_VIEW'
DramaTriggerType.ON_CLOSE_VIEW = 'ON_CLOSE_VIEW'
DramaTriggerType.ON_NO_PANEL_SHOW = 'ON_NO_PANEL_SHOW'
DramaTriggerType.GUIDE_FUNCTION_OPEN = 'GUIDE_FUNCTION_OPEN'
DramaTriggerType.ENTER_MAP_FROM_LAST_MAP = "ENTER_MAP_FROM_LAST_MAP"
DramaTriggerType.ENTER_MAP_FROM_LAST_MAP_TYPE = "ENTER_MAP_FROM_LAST_MAP_TYPE"
DramaTriggerType.GUIDE_FUNCTION_OPEN_PLUS = 'ON_FUNCTION_OPEN'

return DramaTriggerType