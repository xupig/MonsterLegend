--Drama.lua
local Drama = Class.Drama(ClassTypes.XObject)

local DramaConfig = GameConfig.DramaConfig
local DramaUtil = GameUtil.DramaUtil
local DramaWaitingEvent = ClassTypes.DramaWaitingEvent
local DataParseHelper = GameDataHelper.DataParseHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EntityType = GameConfig.EnumType.EntityType

function Drama:__ctor__(id,dramaName)
    self._id = id
    self._name = dramaName
    local dramaData = DramaDataPool:GetDramaData(dramaName)
    self._actions = dramaData.actionList
    self._marks = dramaData.mark
    self._state = DramaConfig._STATE_IDLE
    self._eventvalue = dramaData.eventvalue
    self._onEnd = false
    self._preloading = false
    self._waitingEvents = nil
end

function Drama:Start(markName)
    markName = markName or ""
    if self._state ~= DramaConfig._STATE_IDLE then return end
    self:Reset()   
    self._markName = markName
    self._dramaKey = self._name..markName
    if markName == "" then 
        self._curActionPos = 1
    else
        self:SetCurActionPos(markName)
    end
    self._state = DramaConfig._STATE_RUNNING
    self._updateFunc = function () self:Update() end
    DramaManager:AddListener(self._updateFunc)
    self._updateFunc()
end

function Drama:GetNextEndPos()
    local curEndPos = 1
    for i = self._curActionPos + 1,#self._actions do
        local actionName = self._actions[i][1]
        if actionName == "End" then 
            curEndPos = i
            break
        end
    end
    if curEndPos == 1 then
        LoggerHelper.Error("当前剧情配置错误，没有对应的End指令。剧情key为:"..self._dramaKey)
    end
    return curEndPos
end

function Drama:Reset()
    self._onEnd = false
    self._waitingEvents = nil
    self._sleepEndTime = 0
    self._pastTime = 0
end

function Drama:Stop()
    if (not self:IsRunning()) then return end
    DramaManager:RemoveListener(self._updateFunc)
    self._updateFunc = nil
    self._state = DramaConfig._STATE_IDLE
    DramaManager:DramaOnEnd(self)
    EventDispatcher:TriggerEvent(GameEvents.DramaPlayEnd, self._name, self._dramaKey,self._id)
end

function Drama:Update()
    if DramaManager._isBlockingUpdate then return end
    self._pastTime = self._pastTime + Time.deltaTime
    while self._curActionPos <= #self._actions do
        if self._preloading then break end
        if not self:IsRunning() then break end
        if self:CheckWaitingEvent() then break end
        if self:CheckSleepTime() then break end
        self._runningActionName = self._actions[self._curActionPos][DramaConfig.TAG_ACTION]
        self:ExecuteAction(self._actions[self._curActionPos])
        self._curActionPos = self._curActionPos + 1
        if self._runningActionName ~= "Judge" and not DramaConfig:IsMark(self._runningActionName) 
            and not self:IsWaitingEvent(self._actions, self._curActionPos) then
            break
        end
    end
    if self._curActionPos > #self._actions then
        self:Stop()
    end
end

function Drama:IsWaitingEvent(action, curActionPos)
    if action ~= nil and action[curActionPos] ~= nil and action[curActionPos][DramaConfig.TAG_ACTION] ~= nil and 
        action[curActionPos][DramaConfig.TAG_ACTION] == "WaitingEvent" then
        return true
    end
    return false
end

function Drama:CheckWaitingEvent()
    if self._waitingEvents == nil then return false end
    if not self:CheckTimeOut(self._waitingEvents[1]._waitingTime) then return true end
    --_waitingType为1，超时直接跳过指引
    if self._waitingEvents[1]._waitingType == 1 then
        self:SkipDrama()
    end
    self._waitingEvents = nil
    return false
end

function Drama:CheckTimeOut(targetTime)
    return self._pastTime >= targetTime
end

function Drama:CheckSleepTime()
    return not self:CheckTimeOut(self._sleepEndTime)
end 

function Drama:IsRunning()
    if self._state == DramaConfig._STATE_RUNNING then
        return true
    else
        return false
    end
end

function Drama:SetCurActionPos(markName)
    local actionPos = self._marks[markName]
    if actionPos ~= nil then
        self._curActionPos = actionPos
    else
        LoggerHelper.Error("DramaError markName:["..markName.."] doesn't exsit in "..self._name)
    end
end

function Drama:ExecuteAction(action)
    local actionName = action[DramaConfig.TAG_ACTION]
    if UnityEngine.Application.isEditor then
        LoggerHelper.Log(actionName)
    end
    if DramaConfig:IsMark(actionName) then
        if self._onEnd then
            self:Stop()
        end
        return
    end
    local actionFunc = self[actionName]
    if actionFunc ~= nil then
        actionFunc(self,action)
    else
        LoggerHelper.Error("Drama:ExecuteAction Error: ["..actionName.."] is not exist")
    end
end

function Drama:ExecuteActionString(actionStr)
    local actions = DramaUtil:StringToActions(actionStr)
    for i = 1,#actions do
        self:ExecuteAction(actions[i])
    end
end

function Drama:ExecuteConditions(conditions)
    local result = true
    local splits = string.split(conditions,";")
    if string.IsNullOrEmpty(splits[1]) then
        return true
    end
    for i = 1,#splits do
        local condition = splits[i]
        local conditionSP = string.split(condition,":")
        local conditionName = conditionSP[1]
        local isTrue = true
        if string.find(conditionName,"!") ~= nil then
            conditionName = string.gsub(conditionName,"!","")
            isTrue = false
        end
        local conditionValue = ""
        if #conditionSP > 1 then
            conditionValue = conditionSP[2]
        end
        result = DramaJudgment:ExecuteCondition(conditionName,conditionValue)
        if isTrue == false then
            result = not result
        end
        if  not result then
            break
        end
    end
    return result
end

function Drama:TriggerWaitEvent(eventName,eventValue)
    eventValue = tostring(eventValue)
    local waitingEvent = nil
    local hasEnterMap = false
    if self._waitingEvents ~= nil then
        for i,v in ipairs(self._waitingEvents) do
            waitingEvent = self._waitingEvents[i]
            if waitingEvent._eventName == eventName
                and waitingEvent._eventValue == eventValue then
                self._waitingEvents = nil
                self:Update()
                return
            end
            if waitingEvent._eventName == DramaTriggerType.EnterMap then
                hasEnterMap = true
            end
        end
    end
    if eventName == DramaTriggerType.LeaveMap 
            and (self._waitingEvents == nil or not hasEnterMap) 
            and not DramaManager._isBlockingUpdate then
        --self:Stop()
    end
end

function Drama:SkipDrama()
    if not self:IsRunning() then return end
    if self._onEnd then return end
    local curEndPos = 0
    self:Reset()
    if self._actions[self._curActionPos][DramaConfig.TAG_ACTION] == "End" then
        curEndPos = self._curActionPos
    else
        curEndPos = self:GetNextEndPos()
    end
    if curEndPos == 0 then
        LoggerHelper.Error("无法跳过，因当前剧情配置错误，没有对应的End指令。剧情key为: "..self._dramaKey)
        return
    end
    self._curActionPos = curEndPos
    self:End(nil)
    self._curActionPos = self._curActionPos + 1
end


function Drama:CanRemove()
    return self._state == DramaConfig._STATE_IDLE
end

function Drama:RecordCameraInfo()
    local backup = GameWorld.GetCameraInfoBackup()
    self._localEulerAngles = backup[0]
    self._distance = backup[1]
end

--以下为各个行为命令实现方法

function Drama:CreateEntity(action)
    local t= action[DramaConfig.TAG_ARG1]
	local id = tonumber(action[DramaConfig.TAG_ARG2])
	local tid = tonumber(action[DramaConfig.TAG_ARG3])
	local pos = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG4])
	local y = tonumber(action[DramaConfig.TAG_ARG5])
	local isBorn = tonumber(action[DramaConfig.TAG_ARG6])
	if t == "Dummy" then
		EventDispatcher:TriggerEvent(GameEvents.CreateDramaDummy, id, tid, pos, y, isBorn)
	elseif t == "NPC" then
        EventDispatcher:TriggerEvent(GameEvents.CreateDramaNPC, id, tid, pos, y, isBorn)
    elseif t == "Avatar" then
		EventDispatcher:TriggerEvent(GameEvents.CreateDramaAvatar, id, tid, pos, y, isBorn)
	end
end

function Drama:TeleportEntity(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
	if id == GameWorld.Player().id then entity = GameWorld.Player() end
    local position = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG2])
	local pos = Vector3(position[1], position[2], position[3])
	entity:Teleport(pos)
end

function Drama:DestroyEntity(action)
	local id = tonumber(action[DramaConfig.TAG_ARG1])
	EventDispatcher:TriggerEvent(GameEvents.DestroyDramaEntity, id)
end

function Drama:DestroyEntities(action)
	EventDispatcher:TriggerEvent(GameEvents.DestroyDramaEntities)
end

function Drama:RotateEntity(action)
	local id = action[DramaConfig.TAG_ARG1]
    local entity = self:GetEntityByID(id)
	local rotate = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG2])
	entity:SetFace(rotate[2])
end

function Drama:End(action)
    local flag = true
    if self._curActionPos + 1 <= #self._actions and
        self._actions[self._curActionPos + 1][DramaConfig.TAG_ACTION] == DramaConfig.MARK_ON_END then
        self._curActionPos = self._curActionPos + 1
        flag = false
        self._onEnd = true
    end
    if flag then 
        self:Stop()
    end
end

function Drama:WaitingEvent(action)
    local events = {}
    local eventNames = string.split(action[DramaConfig.TAG_ARG1],",")
    local eventValues = string.split(action[DramaConfig.TAG_ARG2],",")
    local type = tonumber(action[DramaConfig.TAG_ARG4] or 0)
    local overTime = 0
    local waitingTime = action[DramaConfig.TAG_ARG3]
    if waitingTime ~= nil then
        overTime = waitingTime * 0.001 + self._pastTime
    else
        overTime = math.huge * 0.001 + self._pastTime
    end
    for i,v in ipairs(eventNames) do
        table.insert( events, DramaWaitingEvent(eventNames[i],eventValues[i],overTime, type) )
    end
    self._waitingEvents = events
end

function Drama:ExAction(action)
    local exActionName = action[DramaConfig.TAG_ARG1]
    --local exActionArg = action[DramaConfig.TAG_ARG2]
    DramaExAction:ExecuteExAction(self,exActionName,action)
end

function Drama:Goto(action)
    local markName = action[DramaConfig.TAG_ARG1]
    self:SetCurActionPos(markName)
end

function Drama:ShowDialog(action)
    local arg2 = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG1])
    PlayerManager.TaskManager:ShowNormalDialog(arg2[1],arg2[2],self._dramaKey)

    --[[
    local entityType = action[DramaConfig.TAG_ARG1]
    local content = action[DramaConfig.TAG_ARG2]
    local duration = tonumber(action[DramaConfig.TAG_ARG3])
    duration = duration * 0.001
    local isWaitingEvent = (action[DramaConfig.TAG_ARG4] == "1")
    GameManager.GUIManager.ShowNext(content,duration,
        function () 
            EventDispatcher:TriggerEvent(GameEvents.SKIP_DIALOG)
        end)
    if isWaitingEvent then
        self._waitingEvents = DramaWaitingEvent(DramaConfig.SKIP_EVENT_NAME,DramaConfig.SKIP_EVENT_VALUE, duration + self._pastTime)
    end
    ]]--
end

function Drama:Judge(action)
    local conditions = action[DramaConfig.TAG_ARG1]
    local action1 = action[DramaConfig.TAG_ARG2]
    local action2 = action[DramaConfig.TAG_ARG3]
    if self:ExecuteConditions(conditions) then
        self:ExecuteActionString(action1)
    else
        self:ExecuteActionString(action2)
    end
end

function Drama:CloseDialog(action)
    LoggerHelper.Error("CloseDialog")
end

function Drama:Sleep(action)
    local sleepTime = action[DramaConfig.TAG_ARG1] * 0.001
    self._sleepEndTime = sleepTime + self._pastTime
end

function Drama:MoveTo(action)
    local position = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG2])
	local pos = Vector3(position[1], position[2], position[3])
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
	if id == GameWorld.Player().id then entity = GameWorld.Player() end
    if entity == nil then 
        return
    end
    local moveSpeed = tonumber(action[DramaConfig.TAG_ARG3])
    entity:MoveByLine(pos, moveSpeed, function()
			GameManager.DramaManager:Trigger(DramaTriggerType.EntityMoveEnd, tostring(entity.id))
		end)
end

function Drama:EnableDramaSkill(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    if entity == nil then return end
    local mode = tonumber(action[DramaConfig.TAG_ARG2])
    entity:EnableDramaSkill(mode)
end

function Drama:PlaySkill(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    if entity == nil then return end
    local skillId = tonumber(action[DramaConfig.TAG_ARG2])
    entity:PlaySkill(skillId)
end

function Drama:PlayAction(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    if entity == nil then return end
    local actionId = tonumber(action[DramaConfig.TAG_ARG2])
    entity:PlayAction(actionId)
end

function Drama:CameraLookAt(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local slotName = action[DramaConfig.TAG_ARG2]
    local rotationDuration = action[DramaConfig.TAG_ARG3]
    local rotationMinSpeed = action[DramaConfig.TAG_ARG4]
    local positionString = action[DramaConfig.TAG_ARG5]
    local positionDuration = action[DramaConfig.TAG_ARG6]
    local target = self:GetTransform(entity,slotName)
    local position = self:GetVector3(positionString)
    GameWorld.ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, position, positionDuration)
end

function Drama:RecoverCamera(action)
    --GameWorld.ChangeToRotationTargetMotion(DramaUtil:GetDefaultCameraTarget(),self._localEulerAngles,0,0,self._distance,0,true,true)
    --self:DestroyCameraAnimationObj()
    GameWorld.RecoverCamera(self._localEulerAngles,0,0,self._distance,0,true,true)
end

function Drama:RotateCamera(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local slotName = action[DramaConfig.TAG_ARG2]
    local rotationString = action[DramaConfig.TAG_ARG3]
    local rotationDuration = action[DramaConfig.TAG_ARG4]
    rotationDuration = rotationDuration * 0.001
    local distance = tonumber(action[DramaConfig.TAG_ARG5])
    local distanceDuration = action[DramaConfig.TAG_ARG6]
    distanceDuration = distanceDuration * 0.001
    local target = self:GetTransform(entity,slotName)
    local rotation = self:GetVector3(rotationString)
    local rotationAccelerateRate = 0
    if rotationString ~= "0" then
        local rotationStringList = string.split(rotationString,",")
        if #rotationStringList >= 4 then
            rotationAccelerateRate = rotationStringList[4]
        end
    end
    GameWorld.ChangeToRotationTargetMotion(target, rotation, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
end

function Drama:MoveCamera(action)
    local rotationString = action[DramaConfig.TAG_ARG1]
    local rotationDuration = action[DramaConfig.TAG_ARG2]
    rotationDuration = rotationDuration * 0.001
    local positionString = action[DramaConfig.TAG_ARG3]
    local positionDuration = action[DramaConfig.TAG_ARG4]
    positionDuration = positionDuration * 0.001
    local rotation = self:GetVector3(rotationString)
    local position = self:GetVector3(positionString)
    local rotationAccelerateRate = 0
    if rotationString ~= "0" then
        local rotationStringList = string.split(rotationString,",")
        if #rotationStringList >= 4 then
            rotationAccelerateRate = rotationStringList[4]
        end
    end
    GameWorld.ChangeToRotationPositionMotion(rotation, rotationDuration, rotationAccelerateRate, position, positionDuration)
end

function Drama:RotateCameraForLocal(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local slotName = action[DramaConfig.TAG_ARG2]
    local rotationString = action[DramaConfig.TAG_ARG3]
    local rotationDuration = tonumber(action[DramaConfig.TAG_ARG4]) * 0.001
    local distance = tonumber(action[DramaConfig.TAG_ARG5])
    local distanceDuration = tonumber(action[DramaConfig.TAG_ARG6]) * 0.001
    local target = self:GetTransform(entity, slotName)
    local rotationList = DataParseHelper.ParseListInt(rotationString)
	local rotation = Vector3(rotationList[1], rotationList[2], rotationList[3])
    local rotationAccelerateRate = 0
    if #rotationList >= 4 then
        rotationAccelerateRate = rotationList[4]
    end
    if target == nil then return end
    GameWorld.ChangeToRotationTargetMotionForLocal(entity:GetTransform(), target, rotation, rotationDuration, rotationAccelerateRate, distance, distanceDuration)
end

function Drama:MoveCameraForLocal(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local rotationString = action[DramaConfig.TAG_ARG2]
    local rotationDuration = tonumber(action[DramaConfig.TAG_ARG3]) * 0.001
    local positionString = action[DramaConfig.TAG_ARG4]
    local positionDuration = tonumber(action[DramaConfig.TAG_ARG5]) * 0.001
    local rotationList = DataParseHelper.ParseListInt(rotationString)
	local rotation = Vector3(rotationList[1], rotationList[2], rotationList[3])
    local p = DataParseHelper.ParseListInt(positionString)
	local position = Vector3(p[1], p[2], p[3])
    local rotationAccelerateRate = 0
    if #rotationList >= 4 then
        rotationAccelerateRate = rotationList[4]
    end
    --todo
    --GameWorld.ChangeToRotationPositionMotionForLocal(entity:GetTransform(), rotation, rotationDuration, rotationAccelerateRate, position, positionDuration)
end

function Drama:PreloadResources(action)
    self._preloading = true
    local callback = function()  
        self._preloading = false
    end
	DramaResource.PreloadDramaResources({self._name}, callback)
end

function Drama:ChangeCameraFOV(action)
    local fov = tonumber(action[DramaConfig.TAG_ARG1])
    GameWorld.ChangeCameraFOV(fov)
end

function Drama:ShowNPC(action)
	local visible = tonumber(action[DramaConfig.TAG_ARG1])
	EventDispatcher:TriggerEvent(GameEvents.DramaShowNPC, visible)
end

function Drama:ShowPanel(action)
    local name = action[DramaConfig.TAG_ARG1]
	local state = tonumber(action[DramaConfig.TAG_ARG2])
	if state == 1 then
		if name == PanelsConfig.BlackSide then
			GameManager.GUIManager.ShowPanel(name, {ShowSkip = true, dramaKey = self._dramaKey})
		else
			GameManager.GUIManager.ShowPanel(name)
		end
	elseif state == 0 then
		GameManager.GUIManager.ClosePanel(name)
	end
end

function Drama:SetAttr(action)
    local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local attriName = action[DramaConfig.TAG_ARG2]
    local attriValue = tonumber(action[DramaConfig.TAG_ARG3])
    GameWorld.SynEntityAttrByDrama(entity.id, attriName, attriValue)
end

function Drama:ShakeCamera(action)
	local id = tonumber(action[DramaConfig.TAG_ARG1])
	local time = tonumber(action[DramaConfig.TAG_ARG2])
    time = time * 0.001
	GameWorld.ShakeCamera(id, time)
end

function Drama:PlayUIFX(action)
	local path = action[DramaConfig.TAG_ARG1]
	local p = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG2])
	local time = tonumber(action[DramaConfig.TAG_ARG3])
	EventDispatcher:TriggerEvent(GameEvents.DramaPlayUIFX, path, p, time)
end

function Drama:PlayVisualFX(action)
	local path = action[DramaConfig.TAG_ARG1]
	local p = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG2])
	local pos = Vector3(p[1], p[2], p[3])
	local time = tonumber(action[DramaConfig.TAG_ARG3])
    local rotate = Vector3.zero
    if action[DramaConfig.TAG_ARG4] ~= nil then
        local r = DataParseHelper.ParseListInt(action[DramaConfig.TAG_ARG4])
        rotate = Vector3(r[1], r[2], r[3])
    end
    if GameWorld.isEditor and time == nil then
        LoggerHelper.Error("[Drama:PlayVisualFX] TAG_ARG3 is config error!")
        return
    end
	EventDispatcher:TriggerEvent(GameEvents.DramaPlayVisualFX, path, pos, rotate, time)
end

function Drama:PlayMusic(action)
    local sid = tonumber(action[DramaConfig.TAG_ARG1])
	GameWorld.PlayBgMusic(sid)
end

function Drama:PlaySound(action)
    local sid = tonumber(action[DramaConfig.TAG_ARG1])
	GameWorld.PlaySound(sid)
end

function Drama:StopSound(action)
    local sid = tonumber(action[DramaConfig.TAG_ARG1])
	GameWorld.StopSound(sid)
end

function Drama:PlayVideo(action)
    local vid = tonumber(action[DramaConfig.TAG_ARG1])
	GameWorld.PlayVideo(vid)
end

function Drama:AddBuff(action)
	local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
	local buffid = tonumber(action[DramaConfig.TAG_ARG2])
	local tim = tonumber(action[DramaConfig.TAG_ARG3])
	if entity == nil then return end
	entity:AddBuff(buffid, tim)
end

function Drama:RemoveBuff(action)
	local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
	local buffid = tonumber(action[DramaConfig.TAG_ARG2])
	if entity == nil then return end
	entity:RemoveBuffer(buffid)
end

function Drama:StopAI(action)
    local onlyNotStopMonster = action[DramaConfig.TAG_ARG1]
    if onlyNotStopMonster then
        local entities = GameWorld.Entities()
        for k, v in pairs(entities) do
            if v.entityType == EntityType.Dummy or v.entityType == EntityType.Monster then
            
            else
                v:TempStopThink(true)
            end
        end
    else
        GameWorld.aiCanThink = false
    end

    if GameWorld.Player() ~= nil then
        GameWorld.Player():StopMove()
    end
end

function Drama:StartAI(action)
    local onlyNotStopMonster = action[DramaConfig.TAG_ARG1]
    if onlyNotStopMonster then
        local entities = GameWorld.Entities()
        for k, v in pairs(entities) do
            if v.entityType == EntityType.Dummy or v.entityType == EntityType.Monster then
                --什么都不做
            else
                v:TempStopThink(false)
            end
        end
    else
        GameWorld.aiCanThink = true
    end
end

function Drama:SetApplyRootMotion(action)
	local id = action[DramaConfig.TAG_ARG1]
	local entity = self:GetEntityByID(id)
    local state = tonumber(action[DramaConfig.TAG_ARG2]) == 1
    entity:SetApplyRootMotion(state)
end

function Drama:KeepTimeScale(action)
    local scale = tonumber(action[DramaConfig.TAG_ARG1])
    local time = tonumber(action[DramaConfig.TAG_ARG2])
    time = time * 0.001
    GameWorld.KeepTimeScale(scale, time)
end

function Drama:ShowSubtitles(action)
	local id = tonumber(action[DramaConfig.TAG_ARG1])
	local time = tonumber(action[DramaConfig.TAG_ARG2])
    time = time * 0.001 --毫秒转换为秒
	local content = LanguageDataHelper.GetLanguageData(id)
	GameManager.GUIManager.ShowText(4, content, time)
end

function Drama:ShowBossIntroduce(action)
    local monsterId = tonumber(action[DramaConfig.TAG_ARG1])
    EventDispatcher:TriggerEvent(GameEvents.SHOW_INTRODUCE, monsterId)
end

function Drama:HideBossIntroduce(action)
    EventDispatcher:TriggerEvent(GameEvents.HIDE_INTRODUCE)
end

function Drama:ShowIntroduce(action)
    local id = tonumber(action[DramaConfig.TAG_ARG1])
    EventDispatcher:TriggerEvent(GameEvents.SHOW_INTRODUCE_NORMAL, id)
end

function Drama:HideIntroduce(action)
    EventDispatcher:TriggerEvent(GameEvents.HIDE_INTRODUCE_NORMAL)
end

function Drama:StopAutoTask(action)
    EventDispatcher:TriggerEvent(GameEvents.TASK_CG_START)
end

function Drama:BeginAutoTask(action)
    EventDispatcher:TriggerEvent(GameEvents.TASK_CG_END)
end

function Drama:SetShowPlayerModel(action)
    local mode = tonumber(action[DramaConfig.TAG_ARG1])
    GameWorld.ShowPlayer():ShowModel(mode)
end

local Random_Damage = {0.8, 0.85, 0.9, 0.95, 1, 1.05, 1.1, 1.15, 1.2}
function Drama:ShowDamage(action)
    local id = action[DramaConfig.TAG_ARG1]
    local lowId = action[DramaConfig.TAG_ARG2]
    local entity = self:GetEntityByID(id)
    local entityLower = self:GetEntityByID(lowId)
    if entity == nil or entityLower == nil then return end
    local isPlayer = id == "ShowPlayer"
    local damage = 0
    local fightForce = 0
    if lowId == "ShowPlayer" then
        fightForce = GameWorld.Player().fight_force
    else
        fightForce = entityLower.fight_force
    end
    local index = math.random(#Random_Damage)
    if id == lowId then
        --低战力受伤
        damage = math.floor(fightForce * 0.3 * Random_Damage[index])
    else
        --高战力受伤
        damage = math.floor(fightForce * 0.2 * Random_Damage[index])
    end
    local pos = entity:GetPosition()
    DamageCGManager:CreateDamageNumber(pos, damage, 1, isPlayer)
    EventDispatcher:TriggerEvent(GameEvents.OnCGShowDamage, isPlayer)
end

function Drama:SetEntityActorMode(action)
	local id = action[DramaConfig.TAG_ARG1]
    local entity = self:GetEntityByID(id)
    local mode = tonumber(action[DramaConfig.TAG_ARG2])
	entity:SetActorMode(mode)
end

function Drama:RefreshTaskListTop(taskId)
    local taskId = action[DramaConfig.TAG_ARG1]
    EventDispatcher:TriggerEvent(GameEvents.GuideRefreshTaskListTop, taskId)
end

function Drama:OnCGTriggerEventType(action)
    local typeId = action[DramaConfig.TAG_ARG1]
    EventDispatcher:TriggerEvent(GameEvents.OnCGTriggerEventType, typeId)
end

function Drama:ReadyAnimatorController(action)
    --占位处理预加载AnimatorController
end

function Drama:ShowPlayerPet(action)
    local show = tonumber(action[DramaConfig.TAG_ARG1]) == 1
    local entity = GameWorld.Player()
    if entity ~= nil then
        local pet = GameWorld.GetEntity(entity.pet_eid)
        if pet ~= nil then
            pet.cs.actorVisible = show
        end
    end
end

--0:显示 1：隐藏
function Drama:HideOtherPlayer(action)
    local mode = tonumber(action[DramaConfig.TAG_ARG1])
    GameWorld.SetHideOtherPlayer(mode)
end

function Drama:HideSelfPlayer(action)
    local show = tonumber(action[DramaConfig.TAG_ARG1]) == 0
    local player = GameWorld.Player()
    if player then
        player.cs.actorVisible = show
    end
end

function Drama:PlayerDismount(action)
    local player = GameWorld.Player()
    if player then
        PlayerManager.PartnerManager:RideHideReq()
    end
end

function Drama:Test(action)
    LoggerHelper.Error("Drama:Test")
end

function Drama:GetTransform(entity,slotName)
    slotName = slotName or ""
    if entity ~= nil and entity.cs.actor ~= nil then
        local slotTrans = entity.cs:GetBoneByName(slotName)
        if slotTrans ~= nil then
            return slotTrans
        end
        return entity.cs:GetBoneByName("slot_camera")
    end
    return nil
end

function Drama:GetVector3(vector3String)
    if vector3String == "0" then
        return Vector3.one * -1
    end
    return DramaUtil:StringToVector3(vector3String)
end

function Drama:GetEntityByID(id)
    local result = nil
	if id == "Player" then
        result = GameWorld.Player()
    elseif id == "ShowPlayer" then
        result = GameWorld.ShowPlayer()
    elseif string.find(id, ":") then
        local temp = string.split(id,":")
        local attrName = temp[1]
        local value = tonumber(temp[2])
        local entities = GameWorld.Entities()
        for _, entity in pairs(entities) do
            if entity[attrName] == value then
                result = entity
            end
        end
	else
        result = DramaUtil:GetCreatureByEntityDramaID(tonumber(id))
	end
    if result == nil then
        LoggerHelper.Error("Drama:GetEntityByID error by " .. id)
    end
    return result
end