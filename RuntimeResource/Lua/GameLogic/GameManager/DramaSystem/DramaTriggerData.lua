--DramaTriggerData.lua
DramaTriggerData = {}

DramaTriggerData._dramaTriggerData = {}

function DramaTriggerData:InitData()
    local drama_trigger_cg = require("Dramas/drama_trigger_cg")
    local drama_trigger_guide = require("Dramas/drama_trigger_guide")
    self._dramaTriggerData = drama_trigger_cg
    for k,v in pairs(drama_trigger_guide) do
        table.insert(self._dramaTriggerData,v)
    end
end

function DramaTriggerData:GetDramaTriggers(eventName,eventValue)
    local TriggerList = {}
    for k,v in pairs(self._dramaTriggerData) do
        if v[2] ~= nil and v[2] == eventName and v[3] == eventValue then
            table.insert(TriggerList,v)
        end
    end
    return TriggerList
end

return DramaTriggerData