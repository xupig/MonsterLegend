--DramaTrigger.lua
DramaTrigger = {}

function DramaTrigger:Start()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, function (mapId) self:EnterMap(mapId) end)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, function(mapId) self:LeaveMap(mapId) end)
    EventDispatcher:AddEventListener(GameEvents.SKIP_DIALOG, function () self:SkipDialog() end)
    EventDispatcher:AddEventListener(GameEvents.ON_CLICK_GUIDE_BUTTON,function(buttonName) self:ClickButton(buttonName) end)
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandPlaySkill, function (pos) self:OnPlayerPlaySkillByPos(pos) end)
    EventDispatcher:AddEventListener(GameEvents.ON_PLAYER_PLAY_SKILL, function (skillId) self:OnPlayerPlaySkillById(skillId) end)
    EventDispatcher:AddEventListener(GameEvents.SceneEvent, function (eventId) self:OnSpaceActionEvent(eventId) end)
    EventDispatcher:AddEventListener(GameEvents.ON_PLAYER_RAGE_CHANGE, function (state) self:OnPlayerRageChange(state) end)
    EventDispatcher:AddEventListener(GameEvents.ON_SHOW_PANEL, function (panelName) self:OnShowPanel(panelName) end)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_PANEL, function (panelName) self:OnClosePanel(panelName) end)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayEnd,function(dramaName, dramaKey,dramaValue) self:OnDramaPlayEnd(dramaName, dramaKey,dramaValue) end)
    EventDispatcher:AddEventListener(GameEvents.NPC_ENTER_WORLD,function(npc_id) self:OnNPCEnterWorld(npc_id) end)
    EventDispatcher:AddEventListener(GameEvents.ON_GUIDE_TWEEN_POSITION_END,function(goName) self:OnTweenPositionEnd(goName) end)
    EventDispatcher:AddEventListener(GameEvents.END_CHAPTER_BALANCE,function(mapId) self:OnEndBalance(mapId) end)
    EventDispatcher:AddEventListener(GameEvents.GUIDE_FUNCTION_OPEN,function(cgId) self:OnGuideFunctionOpen(cgId) end)
    EventDispatcher:AddEventListener(GameEvents.ToggleGuideSelect,function(guideName) self:OnGuideToggleSelect(guideName) end)
    EventDispatcher:AddEventListener(GameEvents.ON_OPEN_VIEW,function(viewName) self:OnOpenView(viewName) end)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_VIEW,function(viewName) self:OnCloseView(viewName) end)
    EventDispatcher:AddEventListener(GameEvents.ON_LIST_ITEM_CLICK,function(itemName) self:OnListItemClick(itemName) end)
    EventDispatcher:AddEventListener(GameEvents.CLICK_NPC,function(npc_id) self:OnClickNPC(npc_id) end)
    EventDispatcher:AddEventListener(GameEvents.OnHitMonster,function(state) self:OnHitMonster(state) end)
    EventDispatcher:AddEventListener(GameEvents.NPC_MOVE_CAMERA,function(npc_id) self:OnNPCMoveCamera(npc_id) end)
    EventDispatcher:AddEventListener(GameEvents.THINK_BACK_ENTER_MAP, function (mapId) self:EnterMap(mapId) end)
    EventDispatcher:AddEventListener(GameEvents.ON_NO_PANEL_SHOW,function(param) self:OnNoPanelShow(param) end)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,function(cgId) self:OnFunctionOpen(cgId) end)

    self._onCommitedTask = function(taskId) self:CommitedTask(taskId) end
    EventDispatcher:AddEventListener(GameEvents.COMMITED_TASK_CALLBACK, self._onCommitedTask)
    self._onAcceptTask = function(taskId) self:AcceptTask(taskId) end
    EventDispatcher:AddEventListener(GameEvents.OnAddTask, self._onAcceptTask)
    self._onLevelUp = function(level) self:LevelUp(level) end
    EventDispatcher:AddEventListener(GameEvents.OnGuideLevelUp, self._onLevelUp)
    self._onEnterMapFromLastMap = function(lastMapId) self:EnterMapFromLastMap(lastMapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP_FROM_LAST_MAP, self._onEnterMapFromLastMap)
    self._onEnterMapFromLastMapType = function(lastMapId) self:EnterMapFromLastMapType(lastMapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP_FROM_LAST_MAP, self._onEnterMapFromLastMapType)
    self._onEnterMapType = function(mapId) self:EnterMapType(mapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMapType)
end

function DramaTrigger:Trigger(type,content)
    DramaManager:Trigger(type,content)
end

function DramaTrigger:EnterMapType(mapId)
    local sceneType = GameDataHelper.MapDataHelper.GetSceneType(mapId)
    self:Trigger(DramaTriggerType.EnterMapType, tostring(sceneType))
    if sceneType == 1 or sceneType == 2 then
        self:Trigger(DramaTriggerType.EnterMapType, "MainOrWild") --主城野外一个单独处理事件
    end
end

function DramaTrigger:EnterMap(mapId)
    self:Trigger(DramaTriggerType.EnterMap,tostring(mapId))
end

function DramaTrigger:LeaveMap(mapId)
    self:Trigger(DramaTriggerType.LeaveMap,mapId)
end

function DramaTrigger:ClickButton(buttonName)
    self:Trigger(DramaTriggerType.ClickButton,buttonName)
end

function DramaTrigger:SkipDialog()
    self:Trigger(DramaTriggerType.SKIP_DIALOG,"0")
end

function DramaTrigger:OnPlayerPlaySkillByPos(pos) 
    --LoggerHelper.Error("DramaTrigger:OnPlayerPlaySkillByPos:" .. tostring(pos))
    self:Trigger(DramaTriggerType.ON_PLAY_SKILL_BY_POS, tostring(pos))
end

function DramaTrigger:OnPlayerPlaySkillById(skillId) 
    --LoggerHelper.Error("DramaTrigger:OnPlayerPlaySkillById:" .. tostring(skillId))
    self:Trigger(DramaTriggerType.ON_PLAY_SKILL_BY_ID, tostring(skillId))
end

function DramaTrigger:OnSpaceActionEvent(eventId) 
    --LoggerHelper.Error("DramaTrigger:OnSpaceActionEvent:" .. tostring(eventId))
    self:Trigger(DramaTriggerType.SPACE_EVENT, tostring(eventId))
end

function DramaTrigger:OnPlayerRageChange(state)
    self:Trigger(DramaTriggerType.RAGE_CHANGE, state)
end

function DramaTrigger:OnShowPanel(panelName)
    self:Trigger(DramaTriggerType.SHOW_PANEL, panelName)
end

function DramaTrigger:OnClosePanel(panelName)
    self:Trigger(DramaTriggerType.CLOSE_PANEL, panelName)
end

function DramaTrigger:OnDramaPlayEnd(dramaName, dramaKey,dramaValue)
    -- LoggerHelper.Error("DramaTrigger:OnDramaPlayEnd:".. dramaKey .. dramaValue)
    self:Trigger(DramaTriggerType.DRAMA_PLAYE_END,tostring(dramaKey))
end

function DramaTrigger:OnNPCEnterWorld(npc_id)
    -- LoggerHelper.Error("DramaTrigger:OnNPCEnterWorld:" .. tostring(npc_id))
    self:Trigger(DramaTriggerType.ON_NPC_ENTER_WORLD,tostring(npc_id))
end

function DramaTrigger:OnTweenPositionEnd(goName)
    self:Trigger(DramaTriggerType.ON_TWEEN_POSITION_END,goName)
end

function DramaTrigger:OnEndBalance(mapId)
    -- LoggerHelper.Error("DramaTrigger:OnEndBalance:".. mapId)
    self:Trigger(DramaTriggerType.END_CHAPTER_BALANCE,tostring(mapId))
end

function DramaTrigger:OnGuideFunctionOpen(cgId)
    -- LoggerHelper.Error("DramaTrigger:OnGuideFunctionOpen:".. cgId)
    self:Trigger(DramaTriggerType.GUIDE_FUNCTION_OPEN,tostring(cgId))
end

function DramaTrigger:OnFunctionOpen(cgId)
    -- LoggerHelper.Error("DramaTrigger:OnGuideFunctionOpen:".. cgId)
    self:Trigger(DramaTriggerType.GUIDE_FUNCTION_OPEN_PLUS,tostring(cgId))
end

function DramaTrigger:OnGuideToggleSelect(guideName)
    -- LoggerHelper.Error("DramaTrigger:OnGuideToggleSelect:".. guideName)
    self:Trigger(DramaTriggerType.GUIDE_TOGGLE_SELECT,tostring(guideName))
end

function DramaTrigger:OnOpenView(viewName)
    self:Trigger(DramaTriggerType.ON_OPEN_VIEW,tostring(viewName))
end

function DramaTrigger:OnListItemClick(itemName)
    self:Trigger(DramaTriggerType.GUIDE_LIST_ITEM_CLICK,tostring(itemName))
    -- LoggerHelper.Error("DramaTrigger:OnListItemClick:".. itemName)
end

function DramaTrigger:OnCloseView(viewName)
    self:Trigger(DramaTriggerType.ON_CLOSE_VIEW,tostring(viewName))
end

function DramaTrigger:OnClickNPC(npc_id)
    -- LoggerHelper.Error("DramaTrigger:OnClickNPC:".. npc_id)
    self:Trigger(DramaTriggerType.CLICK_NPC,tostring(npc_id))
end

function DramaTrigger:OnHitMonster(state)
    self:Trigger(DramaTriggerType.HIT_MONSTER,tostring(state))
end

function DramaTrigger:OnNPCMoveCamera(npc_id)
    -- LoggerHelper.Error("DramaTrigger:OnNPCMoveCamera:".. npc_id)
    self:Trigger(DramaTriggerType.NPC_MOVE_CAMERA,tostring(npc_id))    
end

function DramaTrigger:OnNoPanelShow(param)
    self:Trigger(DramaTriggerType.ON_NO_PANEL_SHOW, param)
end

function DramaTrigger:CommitedTask(taskId)
    if taskId == nil then
        return
    end
    self:Trigger(DramaTriggerType.ReachTask, tostring(taskId))
end

function DramaTrigger:AcceptTask(taskId)
    if taskId == nil then
        return
    end
    self:Trigger(DramaTriggerType.AcceptTask, tostring(taskId))
end

function DramaTrigger:LevelUp(level)
    self:Trigger(DramaTriggerType.LevelUp, tostring(level))
end

--从某个场景退出到新的场景
function DramaTrigger:EnterMapFromLastMap(lastMapId)
    self:Trigger(DramaTriggerType.ENTER_MAP_FROM_LAST_MAP, tostring(lastMapId))
end

function DramaTrigger:EnterMapFromLastMapType(lastMapId)
    local sceneType = GameDataHelper.MapDataHelper.GetSceneType(lastMapId)
    self:Trigger(DramaTriggerType.ENTER_MAP_FROM_LAST_MAP_TYPE, tostring(sceneType))
end

return DramaTrigger