local DramaData = Class.DramaData(ClassTypes.XObject)

DramaData.DATA_DRAMAS = "data/dramas/"
DramaData.DRAMA_LUA = "Dramas/"
local FileAccessManager = GameLoader.IO.FileAccessManager
local XMLParser = GameWorld.XMLParser
local DramaConfig = GameConfig.DramaConfig
local package =package

function DramaData:__ctor__(dramaName)
    self:Init(dramaName)
end

function DramaData:Init(dramaName)
    self:LoadLuaData(dramaName)
end

function DramaData:LoadLuaData(dramaName)
    local moduleName = self.DRAMA_LUA..dramaName
    if GameWorld.isEditor then --在编辑器情况下，每次加载最新的数据
        package.loaded[moduleName] = nil
    end
    local data = require(self.DRAMA_LUA..dramaName)
    self:ParseLuaData(data)
end

function DramaData:ParseLuaData(data)
    self.actionList = data
    self.mark = {}
    local pos = 1
    for k,v in pairs(data) do
        local actionName = v[DramaConfig.TAG_ACTION]  
        if DramaConfig:IsMark(actionName) then 
            if self.mark[actionName] ~= nil then
                LoggerHelper.Error("重复mark:"..actionName)
                self.mark[actionName] = nil
            end           
            self.mark[actionName] = pos
        end
        pos = pos + 1
    end
end

function DramaData:GetMark(mark)
    local result = self.mark[mark]
    return (result ~= nil) and result or -1
end


DramaDataPool = {}

DramaDataPool.dramaDataDict = {}

function DramaDataPool:GetDramaData(dramaName)
    local dramaData = nil
    if self.dramaDataDict[dramaName] == nil then
        dramaData = DramaData(dramaName)
        self.dramaDataDict[dramaName] = dramaData
    else
        dramaData = self.dramaDataDict[dramaName]
    end
    if GameWorld.isEditor then --在编辑器情况下，每次加载最新的数据
        self.dramaDataDict[dramaName] = nil
    end
    return dramaData
end

function DramaDataPool:Clear()
    self.dramaDataDict = nil
end

return DramaDataPool