--DramaExAction.lua
DramaExAction = {}

DramaExAction._drama = nil

local EntityType = GameConfig.EnumType.EntityType
local DramaUtil = GameUtil.DramaUtil
local SystemSettingConfig = GameConfig.SystemSettingConfig

function DramaExAction:ExecuteExAction(drama,actionName,actionValue)
    self._drama = drama
    local func = self[actionName]
    if func ~= nil then
        func(self,actionValue)
    else
        LoggerHelper.Error("ExecuteExAction Error:["..actionName.."] is not exist!")
    end
    self._drama = nil
end

--以下为各个拓展命令实现方法

function DramaExAction:HideSkillBtn(arg)
    LoggerHelper.Error("HideSkillBtn ")
end

function DramaExAction:ShowGuideArrow(arg)
    local data = {}
    data[1] = arg[DramaConfig.TAG_ARG2]
    data[2] = arg[DramaConfig.TAG_ARG3]
    data[3] = arg[DramaConfig.TAG_ARG4]
    data[4] = arg[DramaConfig.TAG_ARG5]
    data[5] = arg[DramaConfig.TAG_ARG6]
    GameManager.GUIManager.ShowPanel(PanelsConfig.GuideArrow, data)
end

function DramaExAction:SetTriggerCount(arg)
    local triggerId = tonumber(arg[DramaConfig.TAG_ARG2])
    GameManager.GuideManager.SetCGComplete(triggerId)
end

function DramaExAction:ShowChangeMask(arg)
    local show = tonumber(arg[DramaConfig.TAG_ARG2]) == 1
    local lid = tonumber(arg[DramaConfig.TAG_ARG3] or 0)
    local fade = tonumber(arg[DramaConfig.TAG_ARG4] or 0) == 1
    local time = tonumber(arg[DramaConfig.TAG_ARG5] or 0)
    time = time *0.001
    if show then
        local msg = GameDataHelper.LanguageDataHelper.GetLanguageData(lid)
        if lid == 0 then
            msg = ""
        end
        GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.Shady, {text = msg, FadeIn = fade, FadeInTime = time})
    else
        EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_SHADY, {FadeOut = fade, FadeOutTime = time})
    end
end

function DramaExAction:SetGlobalRim(arg)
    local flag = tonumber(arg[DramaConfig.TAG_ARG2])
    GameWorld.SetGlobalRim(flag)
end

function DramaExAction:ShowMainUI(arg)
    local state = tonumber(arg[DramaConfig.TAG_ARG2])
    if state == 0 then
        GUILayerManager.HideOtherLayers(GUILayer.LayerUICG)
    else
        GUILayerManager.ShowOtherLayers(GUILayer.LayerUICG)
    end
    EventDispatcher:TriggerEvent(GameEvents.ResetControlStick)
end

function DramaExAction:EnterMap(arg)
    local instance = tonumber(arg[DramaConfig.TAG_ARG2])
    GameManager.GameSceneManager:DramaScene(instance)
end

function DramaExAction:ShowSceneChangeMask(arg)
    local time = tonumber(arg[DramaConfig.TAG_ARG2])
    if time == 0 then
        EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_SHADY, {FadeOut = fade, FadeOutTime = time})
        return
    end
    local lid = tonumber(arg[DramaConfig.TAG_ARG3])
    local fade = tonumber(arg[DramaConfig.TAG_ARG4] or 0) == 1
    local msg = GameDataHelper.LanguageDataHelper.GetLanguageData(lid)
    GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.Shady, {text = msg, FadeIn = fade, FadeInTime = 0})--{[1] = msg, [2] = fade})
    if time > 0 then
        GameWorld.TimerHeap:AddTimer(time, 0, 0, function()
            EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_SHADY, {FadeOut = fade, FadeOutTime = time})
        end)
    end
end

function DramaExAction:SetRadialBlurColorFul(arg)
    local state = tonumber(arg[DramaConfig.TAG_ARG2]) == 1
    local darkness = tonumber(arg[DramaConfig.TAG_ARG3])
    GameWorld.SetRadialBlurColorFul(state, darkness)
end

function DramaExAction:PlayScreenWhite(arg)
    local startAlpha = tonumber(arg[DramaConfig.TAG_ARG2])
    local endAlpha = tonumber(arg[DramaConfig.TAG_ARG3])
    local duration = tonumber(arg[DramaConfig.TAG_ARG4])
    GameWorld.PlayScreenWhite(startAlpha, endAlpha, duration)
end

function DramaExAction:PlayerFaceToCamera(arg)
    GameWorld.Player():FaceToCamera()
end

function DramaExAction:ShowMonstersAndNPCs(arg)
    local state = tonumber(arg[DramaConfig.TAG_ARG2])
    local show = true
    if state == 0 then
        show = false
    end
    local entities = GameWorld.Entities()
    for k, v in pairs(entities) do
        if not DramaUtil:IsDramaEntityByEID(v.id) then
            if v.entityType == EntityType.Dummy or v.entityType == EntityType.Monster or v.entityType == EntityType.NPC then
                v.cs.actorVisible = show
            end
        end
    end
    if state == 0 then
        GameWorld.SetHideMonster(1)
    else
        local value = PlayerManager.SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER)
        PlayerManager.SystemSettingManager:SetMonsterHide(value)
    end
end

return DramaExAction