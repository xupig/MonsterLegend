--DramaWaitingEvent.lua
local DramaWaitingEvent = Class.DramaWaitingEvent(ClassTypes.XObject)

--type:1表示超时直接跳过指引
function DramaWaitingEvent:__ctor__(setEventName,setEventValue,setWaitingTime,type)
    self._eventName = setEventName
    self._eventValue = setEventValue
    self._waitingTime = setWaitingTime
    self._waitingType = type
end
