--DramaJudgment.lua
DramaJudgment = {}

local VIPManager  = PlayerManager.VIPManager

function DramaJudgment:ExecuteCondition(conditionName,conditionValue)
    local func = self[conditionName]
    if func ~= nil then 
        return func(self,conditionValue)
    else
        LoggerHelper.Error("conditionName ["..conditionName.."] is not exist in DramaJudgment!")
        return false
    end
end

--以下为各个判断命令实现方法

function DramaJudgment:PlayerIsLevel(arg)
    local lv = tonumber(arg)
	if lv == GameWorld.Player().level then
		return true
	end
    return false
end

function DramaJudgment:CompletedQuest(arg)
    LoggerHelper.Error("CompletedQuest "..arg)
    return true
end

function DramaJudgment:TriggerCount(arg)
    local list = string.split(arg, ",")
    local triggerId = tonumber(list[1])
    local count = tonumber(list[2])
    local curCount = GameManager.GuideManager.GetCGTriggerCount(triggerId)
    if curCount < count then
        return true
    end
    return false
end

function DramaJudgment:IsStarSoulActive(arg)
    LoggerHelper.Error("IsStarSoulActive "..arg)
    return true
end

function DramaJudgment:ItemNumMoreThan(arg)
    LoggerHelper.Error("ItemNumMoreThan "..arg)
    return true
end

--true, 到期页面,false，体验页面
function DramaJudgment:IsVIPExpire(arg)
    return VIPManager:GetVipPopTypeExpire()
end

function DramaJudgment:IsVocGroup(arg)
    local vocGroup = tonumber(arg)
    return GameWorld.Player():GetVocationGroup() == vocGroup
end

function DramaJudgment:HasPanelShow(arg)
    return GameManager.GUIManager.CheckPanelHasOpen()
end

function DramaJudgment:PlayerLevelSmallerThan(arg)
    local lv = tonumber(arg)
	if GameWorld.Player().level < lv then
		return true
	end
    return false
end

function DramaJudgment:PlayerLevelBiggerThan(arg)
    local lv = tonumber(arg)
	if GameWorld.Player().level > lv then
		return true
	end
    return false
end

function DramaJudgment:HasButtonExistAndShow(arg)
    local path = arg
    local uiRoot = GameManager.GUIManager.GetUIRoot()
	local targetGo = GameUtil.UIComponentUtil.FindChild(uiRoot, path)
	if targetGo == nil or targetGo.activeSelf == false then
        return false
    else
        return true
	end
end

function DramaJudgment:IsInConfigMapType(arg)
    local list = string.split(arg, ",")
    local curSceneType = GameManager.GameSceneManager:GetCurSceneType()
    for k,v in pairs(list) do
        if tonumber(v) == curSceneType then
            return true
        end
    end
    return false
end

function DramaJudgment:IsPanelActive(arg)
    return GameManager.GUIManager.PanelIsActive(arg)
end

return DramaJudgment