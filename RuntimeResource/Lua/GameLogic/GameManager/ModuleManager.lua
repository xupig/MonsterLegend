-- ModuleManager.lua
-- 这是一个注册moudule的管理类
local ModuleManager = {}

function ModuleManager.Init()
    local openModules = GameConfig.ModulesConfig.OpenModules
    for k, v in pairs(openModules) do
        local modulePath = "Modules.Module"..v.."."..v.."Module"
        local module = require(modulePath) 
        module.Init()
    end
end

return ModuleManager