-- ShowAvatarManager.lua
-- 玩家Avatar展示管理器
local Camera = UnityEngine.Camera
local CameraClearFlags = UnityEngine.CameraClearFlags
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath
local LightType = UnityEngine.LightType
-- local LightmapBakeType = UnityEngine.LightmapBakeType
local Light = UnityEngine.Light
local LayerMask = UnityEngine.LayerMask
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper

local ShowAvatarManager = {}

ShowAvatarManager.CameraInited = false
function ShowAvatarManager:Init()
	self._showAvatars = {}
end

function ShowAvatarManager:UpdateAvatars(avatarList)
	for i=1,5 do
		local entity = self._showAvatars[i]
		if avatarList[i] then
			if entity == nil then
				entity = GameWorld.CreateEntity("StaticShowAvatar", {vocation = avatarList[i].vocation, base_speed = 600, speed = 600})
				self._showAvatars[i] = entity
			else
				local actorId = RoleDataHelper.GetModelByVocation(avatarList[i].vocation)
				entity:SetActor(actorId)
			end
			entity:Teleport(Vector3(-1500 - i*1.2,100,0))
			GameWorld.SynEntityAttrString(entity.id, EntityConfig.PROPERTY_FACADE, avatarList[i].facade)
		elseif entity then
			entity:Teleport(Vector3(-1600 ,100,0))
		end
	end
end

function ShowAvatarManager:CreateRttCamera()
	self.showAvatarContainer = GameObject.New("ShowAvatarContainer")
	self.rttForTeamMember = GameObject.New("RttForTeamMember")
	self.rttForTeamMember.transform:SetParent(self.showAvatarContainer.transform)
	GameObject.DontDestroyOnLoad(self.showAvatarContainer)
	self.RenderCameraAvatar = self.rttForTeamMember:AddComponent(typeof(Camera))
	-- self.RenderCameraAvatar.renderingPath = RenderingPath.DeferredLighting --.DeferredShading --Forward--
	
	self.RenderTextureAvatar = RenderTexture.New(1060,332,16)
	self.RenderTextureAvatar.name = "ShowAvatar"
	self.RenderCameraAvatar.targetTexture = self.RenderTextureAvatar
	self.RenderCameraAvatar.clearFlags = CameraClearFlags.SolidColor;
	self.RenderCameraAvatar.backgroundColor = UnityEngine.Color.New(46/255,64/255,83/255,0/255)
	self.RenderCameraAvatar.fieldOfView = 20
	self.RenderCameraAvatar.cullingMask = math.pow(2,LayerMask.NameToLayer("Actor"))
	self.RenderCameraAvatar.transform.localPosition = Vector3.New(-1503.59,1.28,5.1)
	self.RenderCameraAvatar.transform.rotation = Vector3.New(0,-1,0)

	-- self:BuildLight()
end

-- function ShowAvatarManager:BuildLight()
-- 	local lightGo = GameObject.New("ShowDirectLight")
--     lightGo.transform:SetParent(self.showAvatarContainer.transform)
--     lightGo.transform.position = Vector3.New(-1000,500,-1000)
-- 	lightGo.transform.localEulerAngles = Vector3.New(164,1,1.3)
--     GameObject.DontDestroyOnLoad(lightGo)
-- 	local lightComponet = lightGo:AddComponent(typeof(Light))
-- 	lightComponet.type = LightType.Directional
-- 	-- lightComponet.lightmapBakeType = LightmapBakeType.Mixed
-- 	lightComponet.intensity = 1
-- 	lightComponet.cullingMask = math.pow(2,LayerMask.NameToLayer("Actor"))
-- end

function ShowAvatarManager:GetRawImage()
	if ShowAvatarManager.CameraInited ==  false then
		self:CreateRttCamera()
		ShowAvatarManager.CameraInited = true
	end
	return self.RenderTextureAvatar
end
ShowAvatarManager:Init()

return ShowAvatarManager