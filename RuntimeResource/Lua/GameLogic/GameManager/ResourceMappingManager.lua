-- ResourceMappingManager.lua
local ResourceMappingManager = {}

local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local Application = UnityEngine.Application

function ResourceMappingManager:Init()
    self._onLevelChangeCB = function()self:OnLevelChange() end
    self:InitListeners()
end

function ResourceMappingManager:InitListeners()
    EventDispatcher:AddEventListener(GameEvents.OnActionAllPackageFinish, function(isSuccess)self:OnActionAllPackageFinish(isSuccess) end)
end

function ResourceMappingManager:OnPlayerEnterWorld()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function ResourceMappingManager:OnPlayerLeaveWorld()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function ResourceMappingManager:OnLevelChange()
    -- LoggerHelper.Log("Ash: OnLevelChange")
    local entity = GameWorld.Player()
    if entity ~= nil then
        local isReady = GameWorld.IsLevelReady(entity.level)
        if not isReady then
            self:WaitForResourceReady(entity.level)
            return
        end
    end
end

function ResourceMappingManager:OnActionAllPackageFinish(isSuccess)
    if isSuccess then
        GameWorld.canStartAutoFight = true
        GameWorld.HideWaitingResLoading()
        if self._callback then
            self._callback()
            self._callback = nil
        end
    else
        GUIManager.ShowMessageBox(1, "仍有必要资源需要下载，\n是否继续？（取消后退出游戏）",
            function()self:CheckResourceReady(self._callback) end,
            function()Application.Quit() end)
    end
end

function ResourceMappingManager:CheckResourceReady(callback)
    local player = GameWorld.Player()
    if player then
        local level = player.level
        local isReady = GameWorld.IsLevelReady(level)
        if not isReady then
            LoadingBarManager:ShowProgressBar(0.1)
            self:WaitForResourceReady(level, callback)
            return
        end
    end
    if callback then
        callback()
    end
end

function ResourceMappingManager:WaitForResourceReady(level, callback)
    self._callback = callback
    local fileSize = GameWorld.GetNecessaryResources(level)
    GameWorld.ShowWaitingResLoading()
    --LoggerHelper.Error(tostring(Application.internetReachability))
    -- if tostring(Application.internetReachability) ~= "ReachableViaLocalAreaNetwork" then
    --     GUIManager.ShowMessageBox(1, "系统检测到您未使用Wifi网络，\n本次有大约" .. fileSize .. "MB必要资源需要下载，\n是否继续？（取消后退出游戏）",
    --         function()GameWorld.DownloadNecessaryResources() end,
    --         function()Application.Quit() end)
    -- else
    GameWorld.DownloadNecessaryResources()
    GameWorld.canStartAutoFight = false
    local player = GameWorld.Player()
    if player then
        player:StopAutoFight("WaitForResourceReady")
        player:StopMoveWithoutCallback() 
      end
    PlayerActionManager:StopAllAction()

-- end
end

ResourceMappingManager:Init()
return ResourceMappingManager
