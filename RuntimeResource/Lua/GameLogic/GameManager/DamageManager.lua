--DamageManager.lua
require "GameManager/DamageSystem/DamageType"
DamageManager = {}

local Camera = UnityEngine.Camera
local Rect = UnityEngine.Rect
local Screen = UnityEngine.Screen
local Vector3 = UnityEngine.Vector3
local GameObject = UnityEngine.GameObject
local RectTransform = UnityEngine.RectTransform
local AttackType = DamageType.AttackType
local ArtNumberType = DamageType.ArtNumberType
local ArtWord = DamageType.ArtWord
local TimerHeap = TimerHeap

DamageManager.HIDE_POSITION = Vector3.New(-1000,-1400,0)
DamageManager._numberGo = nil
DamageManager._damageGo = nil
DamageManager._wordGo = nil
DamageManager._itemDict = {}
DamageManager._underTransform = nil
DamageManager._topTransform = nil
DamageManager._damageGridStack = {}
DamageManager._damageItemStackDic = {}
-- DamageManager._maxItemStackNum = 7
DamageManager._damageGridNumber = 0
DamageManager._currentCount = 0
DamageManager._maxGridCount = 10
DamageManager._hitTypeArtNumberTypeMap = {}
DamageManager._hitTypeArtWordTypeMap = {}

--===================
-- @YuZhenjian
-- 1、对象池key调整为[类型] -- 以前key是[类型+数字], 在相同数字的伤害时重用率低
-- 2、当没有飘字时，会开始内存回收
-- 3、对常用的飘字类型设置更大容量
local NumKey = "_Num"
local _lastShowTime = 0
local CLEAR_CHECK_TIME = 5
local CLEAR_PER_NUM = 10

-- 对象池容量，超过销毁
local DEFAULT_MAX_ITEM_NUM = 30 -- 默认
local MAX_ITEM_NUM = {
    [ArtNumberType.HIT .. NumKey] = 60,
    [ArtNumberType.CRITICAL .. NumKey] = 50,
    [ArtNumberType.GET_HIT .. NumKey] = 50,
}
-- 清理对象池至该值
local DEFAULT_RESERVE_NUM = 10 -- 默认
local RESERVE_ITEM_NUM = {
    [ArtNumberType.HIT .. NumKey] = 20,
    [ArtNumberType.GET_HIT .. NumKey] = 20,
}
--===================


function DamageManager:Init()
    self._checkItemPoolFunc = function() self:CheckItemPool() end
end

function DamageManager:OnPlayerEnterWorld()
    self:AddTimer()
end

function DamageManager:OnPlayerLeaveWorld()
    self:RemoveTimer()
end

function DamageManager:RemoveTimer()
    if self._itemPoolTimer  then
        TimerHeap.DelTimer(self._itemPoolTimer)
        self._itemPoolTimer = nil
    end
end

function DamageManager:AddTimer()
    self:RemoveTimer()
    self._itemPoolTimer = TimerHeap:AddSecTimer(0, CLEAR_CHECK_TIME, 0, self._checkItemPoolFunc)
end

function DamageManager:SetPanelInfo(numberGo,damageGo,wordGo,underTransform,topTransform)
    self._numberGo = numberGo
    self._damageGo = damageGo
    self._wordGo = wordGo
    self._underTransform = underTransform
    self._topTransform = topTransform
    self._numberGo.transform.localPosition = self.HIDE_POSITION
    self._wordGo.transform.localPosition = self.HIDE_POSITION
    self._damageGo.transform.localPosition = self.HIDE_POSITION
    self:InitItemDict()
    self:InitHitType2ArtNumberType()
    self:InitHitType2ArtWordType()
end

function DamageManager:InitItemDict()
    for i = 1,self._numberGo.transform.childCount do
        local item = self._numberGo.transform:GetChild(i-1).gameObject
        self._itemDict[string.sub(item.name,8,-1)] = item
    end
    for i = 1,self._wordGo.transform.childCount do
        local item = self._wordGo.transform:GetChild(i-1).gameObject
        self._itemDict[string.sub(item.name,9,-1)] = item
    end
end

function DamageManager:InitHitType2ArtNumberType()
    self._hitTypeArtNumberTypeMap[AttackType.HIT] = ArtNumberType.HIT
    self._hitTypeArtNumberTypeMap[AttackType.CRITICAL] = ArtNumberType.CRITICAL
    self._hitTypeArtNumberTypeMap[AttackType.GET_HIT] = ArtNumberType.GET_HIT
    self._hitTypeArtNumberTypeMap[AttackType.TREAT] = ArtNumberType.TREAT
    self._hitTypeArtNumberTypeMap[AttackType.PET_HIT] = ArtNumberType.PET_HIT
    self._hitTypeArtNumberTypeMap[AttackType.TREASURE_HIT] = ArtNumberType.TREASURE_HIT
    self._hitTypeArtNumberTypeMap[AttackType.DEADLY] = ArtNumberType.DEADLY
    self._hitTypeArtNumberTypeMap[AttackType.TREAT] = ArtNumberType.TREAT
end

function DamageManager:InitHitType2ArtWordType()
    self._hitTypeArtWordTypeMap[AttackType.MISS] = ArtWord.MISS
    self._hitTypeArtWordTypeMap[AttackType.CRITICAL] = ArtWord.CRITICAL
    self._hitTypeArtWordTypeMap[AttackType.DEADLY] = ArtWord.DEADLY
    self._hitTypeArtWordTypeMap[AttackType.ABSORB] = ArtWord.ABSORB
    self._hitTypeArtWordTypeMap[AttackType.INVINCIBLE] = ArtWord.INVINCIBLE
    self._hitTypeArtWordTypeMap[AttackType.TREAT] = ArtWord.TREAT
    self._hitTypeArtWordTypeMap[AttackType.PARRY] = ArtWord.PARRY
    self._hitTypeArtWordTypeMap[AttackType.UNYIELDING] = ArtWord.UNYIELDING
end

function DamageManager:HitType2ArtNumberType(type,isPlayer)
    if isPlayer and type == AttackType.HIT then
        return ArtNumberType.GET_HIT
    end
    if self._hitTypeArtNumberTypeMap[type] ~= nil then
        return self._hitTypeArtNumberTypeMap[type]
    end
    return ArtNumberType.NOTSHOW
end

function DamageManager:HitType2ArtWordType(type,isPlayer)
    if self._hitTypeArtWordTypeMap[type] ~= nil then
        return self._hitTypeArtWordTypeMap[type]
    end
    return ""
end

local index = 0
local posY = {0,0.5,0.5,1,1}
local posX = {0,0.4,-0.4,0.8,-0.8}
function DamageManager:CreateDamageNumber(position,damage,type,isPlayer)
    if damage == 0 and (type == AttackType.HIT or type == AttackType.CRITICAL or type == AttackType.GET_HIT) then
        return
    end
    if self._currentCount > self._maxGridCount then return end
    local screenPosition = Camera.main:WorldToScreenPoint(position)
    if self:CheckPositionIsInScreen(screenPosition) then
        local damageGrid = self:GetDamageGrid()
        local colorType = self:HitType2ArtNumberType(type,isPlayer)
        local word = self:HitType2ArtWordType(type,isPlayer)
        --不应该传负数，矫正
        if damage < 0 then
            damage = -damage
        end
        if word ~= "" then
            self:CreateDamageWord(damageGrid,word,isPlayer)
        end
        if colorType ~= ArtNumberType.NOTSHOW then
            damageGrid = self:CreateNumber(damageGrid,colorType,damage,isPlayer)
        end
        if isPlayer then
            index = index % 5 + 1
            position.x = position.x + posX[index]
            position.y = position.y + posY[index]
        end
        damageGrid:Show(position,type)
        self._currentCount = self._currentCount + 1
    end
end

function DamageManager:CreateNumber(damageGrid,colorType,value,isPlayer)
    local str = tostring(value)
    local typeName = colorType .. NumKey
    for i = 1,#str do
        local name = colorType..string.sub(str,i,i)
        local damageItem = self:GetDamageItem(name,isPlayer,typeName)
        damageGrid:AddNumberItem(damageItem)
    end
    return damageGrid
end

function DamageManager:CreateDamageWord(damageGrid,word,isPlayer)
    local damageItem = self:GetDamageItem(word,isPlayer,word)
    damageGrid:AddNumberItem(damageItem)
    return damageGrid
end

function DamageManager:CheckPositionIsInScreen(position)   
    local screenRect = Rect.New(0,0,Screen.width,Screen.height)
    return screenRect:Contains(position)
end

function DamageManager:GetDamageGrid()
    local damageGrid = nil
    if #self._damageGridStack > 0 then
        damageGrid = table.remove(self._damageGridStack)
    else
        damageGrid = self:AddDamageGrid()
    end
    return damageGrid
end

function DamageManager:AddDamageGrid()
    local go = GameObject.Instantiate(self._damageGo)
    self._damageGridNumber = self._damageGridNumber + 1
    go.name = "DamageGrid"..self._damageGridNumber
    local damageGrid = GameWorld.AddLuaBehaviour(go,"Modules.ModuleTextFloat.ChildComponent.DamageGrid")
    local trans = go:GetComponent(typeof(RectTransform))
    if trans == nil then
        trans = go:AddComponent(typeof(RectTransform))
    end
    trans:SetParent(self._underTransform,false)
    return damageGrid
end

function DamageManager:RemoveDamageGrid(damageGrid)
    damageGrid:Hide()
    table.insert(self._damageGridStack,damageGrid)
    self._currentCount = self._currentCount - 1
end

function DamageManager:GetDamageItem(name,isTop,typeName)
    isTop = isTop or false
    local damageItem = nil
    if self._damageItemStackDic[typeName] ~= nil then
        local damageItemStack = self._damageItemStackDic[typeName]
        if #damageItemStack > 0 then
            damageItem = table.remove(damageItemStack)
        end
    end
    if not damageItem then
        damageItem = self:AddItem(name,isTop,typeName)
    else
        damageItem:ChangeImage(name, self._itemDict[name])
    end
    return damageItem
end

function DamageManager:AddItem(name,isTop,typeName)    
    local itemGo = self._itemDict[name]
    if itemGo == nil then
        LoggerHelper.Error(name .. " is not exist in panel")
        return
    end
    local go = GameObject.Instantiate(itemGo)
    go.name = name
    if isTop then
        go.transform:SetParent(self._topTransform)
    else
        go.transform:SetParent(self._underTransform)
    end
    local damageItem = GameWorld.AddLuaBehaviour(go,"Modules.ModuleTextFloat.ChildComponent.DamageItem")
    damageItem.Name = name
    damageItem.TypeName = typeName
    return damageItem
end

function DamageManager:RemoveItemFormList(damageItemList)
    for i = 1,#damageItemList do
        local damageItemStack = nil
        local item = damageItemList[i]
        local typeName = item.TypeName
        if self._damageItemStackDic[typeName] ~= nil then
            damageItemStack = self._damageItemStackDic[typeName]
        else
            damageItemStack = {}
            self._damageItemStackDic[typeName] = damageItemStack
        end
        if #damageItemStack < (MAX_ITEM_NUM[typeName] or DEFAULT_MAX_ITEM_NUM) then
            table.insert(damageItemStack,item)
            item:Hide()
        else
            GameWorld.Destroy(item.gameObject)
        end
    end
end

function DamageManager:ClearPool()
    while #self._damageGridStack > 0 do
        local obj = table.remove(self._damageGridStack)
        GameWorld.Destroy(obj.gameObject)
    end
    self._damageGridStack = {}
    for k,v in pairs(self._damageItemStackDic) do
        while #v > 0 do
            local obj = table.remove(v)
            GameWorld.Destroy(obj.gameObject)
        end
        v = {}
    end
end

function DamageManager:CheckItemPool()
    local count = 0
    local reserveNum
    for typeName, damageItemStack in pairs(self._damageItemStackDic) do
        reserveNum = RESERVE_ITEM_NUM[typeName] or DEFAULT_RESERVE_NUM
        if #damageItemStack > reserveNum then
            for i = #damageItemStack, reserveNum, -1 do
                local obj = damageItemStack[i]
                damageItemStack[i] = nil
                GameWorld.Destroy(obj.gameObject)
                count = count + 1
                -- LoggerHelper.Log("==== CheckItemPool() count = " .. count .. ",typeName="..typeName, "#ff0000ff")
                if count > CLEAR_PER_NUM then
                    return
                end
            end
        end
    end
end

DamageManager:Init()
return DamageManager