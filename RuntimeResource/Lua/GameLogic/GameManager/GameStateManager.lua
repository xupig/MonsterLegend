-- GameStateManager.lua
-- 这是一个管理游戏状态的类
-- 用于定义以及处理各种游戏状态间的转换过程，包括构造以及析构
GameStateManager = {}

GAME_STATE = {
    LOGIN = "login",
    CHOOSE = "choose",
    SCENE = "scene",
}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local SceneConfig = GameConfig.SceneConfig
local public_config = GameWorld.public_config
local GameObject = UnityEngine.GameObject
local CreateRoleManager = GameManager.CreateRoleManager

-- 登陆状态
local function OnEnterLogin()
    --LoggerHelper.Log("OnEnterLogin")
    GameWorld.ProgressBar:Close()
    GUIManager.ShowPanel(PanelsConfig.Login)
    GUIManager.ShowPanel(PanelsConfig.Billboard)
    GUIManager.ShowPanel(PanelsConfig.TextFloat)
    GUIManager.ShowPanel(PanelsConfig.TextFloatCG)
    -- GUIManager.ShowPanel(PanelsConfig.PointingArrow)
    GUIManager.ShowPanel(PanelsConfig.BubbleDialog)
    --先加载，避免时序问题
    GUIManager.ShowPanel(PanelsConfig.BossIntroduce)
    GUIManager.ShowPanel(PanelsConfig.BossState)
    GUIManager.ClosePanel(PanelsConfig.BossState)
    GUIManager.ShowPanel(PanelsConfig.SkillOpen)
    GUIManager.ClosePanel(PanelsConfig.SkillOpen)
    GUIManager.ShowPanel(PanelsConfig.Collect)
    GUIManager.ClosePanel(PanelsConfig.Collect)
    --GUIManager.ShowPanel(PanelsConfig.SelectServer)
    --GUIManager.ShowPanel(GameConfig.PanelsConfig.SkillControl)
    --GUIManager.ShowPanel(PanelsConfig.Demo)
    GUIManager.ShowPanel(PanelsConfig.CommonUI)
    GUIManager.ShowPanel(PanelsConfig.BackwoodsSpaceInfo)
    GUIManager.ClosePanel(PanelsConfig.BackwoodsSpaceInfo)
    GUIManager.ShowPanel(PanelsConfig.GuideArrow)
    GUIManager.ClosePanel(PanelsConfig.GuideArrow)
    GUIManager.ShowPanel(PanelsConfig.NewEquipTip)
    GUIManager.ClosePanel(PanelsConfig.NewEquipTip)
    GUIManager.ShowPanel(PanelsConfig.EnemyAvatarInfo)--进入游戏就打开不关闭

    if GameWorld.LuaGameState.ScreenHasCutout() then
        GUIManager.ShowPanel(PanelsConfig.CutoutScreenBorder)
    end
end

local function OnLeaveLogin()
    --LoggerHelper.Log("OnLeaveLogin")
    GUIManager.ClosePanel(PanelsConfig.Login)
end

-- 选角创角
local function OnEnterChoose()
    --LoggerHelper.Log("OnEnterChoose")
    EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
    EventDispatcher:TriggerEvent(GameEvents.ON_ENTER_CHOOSE)
    CreateRoleManager:CreateRttCamera()
    if GameWorld.Player().roleState == 1 then
        local data = {GameWorld.Player().avatar_tbl, GameWorld.Player().avatar_dbid}
        GUIManager.ShowPanel(PanelsConfig.SelectRole, data)
    elseif GameWorld.Player().roleState == 2 then
        GUIManager.ShowPanel(PanelsConfig.CreateRole)
    end
    GameWorld.canStartAutoFight = true
end

local function OnLeaveChoose()
    --LoggerHelper.Log("OnLeaveChoose")
    GUIManager.ClosePanel(PanelsConfig.CreateRole)
    GUIManager.ClosePanel(PanelsConfig.SelectRole)
    CreateRoleManager:DestroyRttCamera()
    CreateRoleManager:DestroyEntity()
end

-- 游戏场景
local function OnEnterScene()
    local sceneType = GameManager.GameSceneManager:GetCurSceneType()
    local sceneId = GameManager.GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: OnEnterScene:" .. sceneType)
    --LoggerHelper.Log("OnEnterScene")
    GameManager.GuideManager.Init()
    local player = GameWorld.Player()
    if player.firstEnter then
        --进入游戏如果有离线数据显示离线面板
        player.firstEnter = false
        PlayerManager.OffLineManager:ShowOffLine()
    end
    
    GUIManager.ShowPanel(PanelsConfig.PlayerState)
    GUIManager.ShowPanel(PanelsConfig.SubMainMenu)
    if not GameWorld.SpecialExamine then
        GUIManager.ShowPanel(PanelsConfig.MiniMap)
    end
    
    if player.isWatchWar then
        player:HideCreatureBillboard()
        GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
        GUIManager.ClosePanel(PanelsConfig.PlayerState)
        GUIManager.ClosePanel(PanelsConfig.MainMenu)
        GUIManager.ClosePanel(PanelsConfig.MiniMap)
        GUIManager.ClosePanel(PanelsConfig.ChatSmall)
    elseif sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then --巅峰竞技
        -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
        GUIManager.ClosePanel(PanelsConfig.MainMenu)
        GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
        GUIManager.ClosePanel(PanelsConfig.PlayerState)
        GUIManager.ClosePanel(PanelsConfig.MainMenu)
        GUIManager.ClosePanel(PanelsConfig.MiniMap)
        GUIManager.ShowPanel(PanelsConfig.ChatSmall)
        GUIManager.ShowPanel(PanelsConfig.InstQuit, 51214)
        GUIManager.ShowPanel(PanelsConfig.PvpMainUI)
        EventDispatcher:TriggerEvent(GameEvents.BeginMatchSuccess)
        GUIManager.ClosePanel(PanelsConfig.AthleticsEnter)
        GUIManager.ClosePanel(PanelsConfig.OnlinePvp)
    else
        GUIManager.ClosePanel(PanelsConfig.PvpMainUI)
        GUIManager.ShowPanel(PanelsConfig.ChatSmall)
        GUIManager.ShowPanel(PanelsConfig.Remind)
        if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.AionTowerMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
        elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_MANOR then
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ClosePanel(PanelsConfig.MainMenu)
            GUIManager.ClosePanel(PanelsConfig.SubMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        elseif sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then --世界boss
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
            GUIManager.ClosePanel(PanelsConfig.WorldBoss)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
        -- GUIManager.ClosePanel(PanelsConfig.DailyActivity)
        -- GUIManager.ShowPanel(PanelsConfig.WorldBossMainUI)
        elseif sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then --boss之家
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            -- GUIManager.ClosePanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.WorldBoss)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        elseif sceneType == SceneConfig.SCENE_TYPE_MY_BOSS then --个人boss
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            -- GUIManager.ClosePanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.WorldBoss)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
        elseif sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            -- GUIManager.ClosePanel(PanelsConfig.MainMenu)
            GUIManager.ClosePanel(PanelsConfig.WorldServer)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        --经验副本关闭菜单栏、任务菜单
        elseif sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE then --经验副本
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.ExpInstance)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
        elseif sceneType == SceneConfig.SCENE_TYPE_MARRY_INSATNCE then --结婚副本
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        elseif sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE then --守护结界塔
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.PetInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then --混沌之门（圣灵血阵）
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        -- GUIManager.ShowPanel(PanelsConfig.EquipmentInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_GOLD then --众神宝库
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.MoneyInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_COMBAT then --一般副本
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ClosePanel(PanelsConfig.FunctionPreview)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        elseif sceneType == SceneConfig.SCENE_TYPE_ANSWER then --答题场景
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        -- GUIManager.ShowPanel(PanelsConfig.Answer)
        elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_MONSTER then --公会神兽
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.Guild)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.GuildMonsterInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then --守卫女神
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.Guild)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.GuardGoddnessSpace)
        elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then --公会篝火
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.Guild)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.GuildBanquetSpace)
        elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then --公会争霸
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.Guild)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.GuildConquestSpace)
        elseif sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then --蛮荒禁地
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.WorldBoss)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        elseif sceneType == SceneConfig.SCENE_TYPE_CLOUD_PEAK then --青云之巅
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ClosePanel(PanelsConfig.CloudPeak)
            GUIManager.ClosePanel(PanelsConfig.AthleticsEnter)
        elseif sceneType == SceneConfig.SCENE_TYPE_TOWER_DEFENSE then --塔防副本
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
        -- GUIManager.ShowPanel(PanelsConfig.PetInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_WEDDING then --婚礼场景
            -- GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ClosePanel(PanelsConfig.GetMarry)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.MarryMainUI)
        elseif sceneType == SceneConfig.SCENE_TYPE_BATTLE_SOUL then --战场之魂
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.BattleSoul)
        elseif sceneId == SceneConfig.NEW_PLAYER_SCENE then
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
        elseif sceneType == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE then --上古战场
            GUIManager.ShowPanel(PanelsConfig.MainMenu, false)
            GUIManager.ClosePanel(PanelsConfig.LeftMainUI)
            GUIManager.ClosePanel(PanelsConfig.AncientBattle)
            GUIManager.ShowPanel(PanelsConfig.InstQuit)
            GUIManager.ShowPanel(PanelsConfig.AncientBattleSpace)
        else
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)
            GUIManager.ShowPanel(PanelsConfig.MainMenu)
            GUIManager.ClosePanel(PanelsConfig.InstQuit)
        end
        
        player:DisplayCreatureBillboard()
    end
end

local function OnLeaveScene()
    -- LoggerHelper.Log("OnLeaveScene")
    local sceneType = GameManager.GameSceneManager:GetPreSceneType()
    
    if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then --永恒之塔
        GUIManager.ClosePanel(PanelsConfig.AionTowerMainUI)
        GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
    elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_MANOR then
        
        elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_RAID then
        
        elseif sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE then --守护结界塔
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            GUIManager.ClosePanel(PanelsConfig.PetInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then --混沌之门（圣灵血阵）
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            GUIManager.ClosePanel(PanelsConfig.EquipmentInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_GOLD then --众神宝库
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            GUIManager.ClosePanel(PanelsConfig.MoneyInstanceInfo)
        elseif sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE then --经验副本
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            GUIManager.ClosePanel(GameConfig.PanelsConfig.ExpInstance)
        elseif sceneType == SceneConfig.SCENE_TYPE_COMBAT then --一般副本
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            PlayerManager.FunctionOpenManager:RefreshPreviewPanel()
        elseif sceneType == SceneConfig.SCENE_TYPE_ANSWER then --答题场景
            GUIManager.ClosePanel(PanelsConfig.Answer)
        elseif sceneType == SceneConfig.SCENE_TYPE_MY_BOSS then --个人boss
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
        elseif sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then --巅峰竞技
            GUIManager.ShowPanel(PanelsConfig.OnlinePvp)
        elseif sceneType == SceneConfig.SCENE_TYPE_CLOUD_PEAK then --青云之巅
            -- GUIManager.ClosePanel(PanelsConfig.CloudPeakInfo)
            elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_MONSTER then --公会神兽
            GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            GUIManager.ClosePanel(PanelsConfig.GuildMonsterInstanceInfo)
            elseif sceneType == SceneConfig.SCENE_TYPE_TOWER_DEFENSE then --塔防副本
                GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
                GUIManager.ClosePanel(PanelsConfig.TowerInstanceInfo)
            elseif sceneType == SceneConfig.SCENE_TYPE_WEDDING then --婚礼场景
                GUIManager.ClosePanel(PanelsConfig.MarryMainUI)
            elseif sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then --守卫女神
                GUIManager.ClosePanel(PanelsConfig.GuardGoddnessSpace)
            elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then --公会篝火
                GUIManager.ClosePanel(PanelsConfig.GuildBanquetSpace)
            elseif sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then --公会争霸
                GUIManager.ClosePanel(PanelsConfig.GuildConquestSpace)
            elseif sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then --蛮荒禁地
                GUIManager.ClosePanel(PanelsConfig.BackwoodsSpaceInfo)
            elseif sceneType == SceneConfig.SCENE_TYPE_MARRY_INSATNCE then --结婚副本
                GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
            elseif sceneType == SceneConfig.SCENE_TYPE_BATTLE_SOUL then --结婚副本
                GUIManager.ClosePanel(PanelsConfig.BattleSoul)
            elseif sceneType == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE then --上古战场
                GUIManager.ClosePanel(PanelsConfig.AncientBattleSpace)
            else
                
                end
    GUIManager.ClosePanel(PanelsConfig.MainMenu)
    GUIManager.ClosePanel(PanelsConfig.MiniMap)
    GUIManager.ClosePanel(PanelsConfig.PlayerState)
    GUIManager.ClosePanel(PanelsConfig.Firework)
    GUIManager.ClosePanel(PanelsConfig.GiveFlowerFx)
-- GUIManager.ClosePanel(PanelsConfig.ChatSmall)
end


local _currentState = nil -- 当前的游戏状态

local GAME_STATE_MAP = {}
GAME_STATE_MAP[GAME_STATE.LOGIN] = {enter = OnEnterLogin, leave = OnLeaveLogin}-- 登陆状态
GAME_STATE_MAP[GAME_STATE.CHOOSE] = {enter = OnEnterChoose, leave = OnLeaveChoose}-- 选择角色
GAME_STATE_MAP[GAME_STATE.SCENE] = {enter = OnEnterScene, leave = OnLeaveScene}-- 游戏场景


-- 切换不同的游戏状态
-- state 只能是 "login", "choose", "scene", ...
function GameStateManager.SetState(state)
    if GAME_STATE_MAP[state] == nil then
        -- LoggerHelper.Log("GameStateManager has not this state:", state)
        return false
    end
    
    -- 如果存在上个状态，则需要调用上个状态的析构
    if _currentState ~= nil then
        GAME_STATE_MAP[_currentState].leave()
        _currentState = nil
    end
    --	LoggerHelper.Log("run state enter")
    _currentState = state
    GAME_STATE_MAP[state].enter()
    return true
end

-- 获取当前游戏状态
function GameStateManager.GetState()
    return _currentState
end

function GameStateManager.ReturnLogin(isManualReturnLogin)
    GameWorld.isManualReturnLogin = isManualReturnLogin == true
    GameManager.PlayerActionManager:StopAllAction()
    if GameWorld.Player() then
        GameWorld.DestroyEntity(GameWorld.Player().id)
    end
    GameManager.DramaManager:ClearUpdateFunc()
    GUILayerManager.ResetAllLayers()
    GUILayerManager.ShowAllLayers()
    GameManager.GameSceneManager:EnterSceneByID(1001, false, nil, function()
            -- CreateRoleManager:CreateRttCamera()
            GameManager.GameStateManager.SetState(GAME_STATE.LOGIN)
    end)
    GameMain.ServerProxyFacade.IsEnableReconnect(false)
    GameMain.ServerProxyFacade.Disconnect()
--GUIManager.ShowPanel(PanelsConfig.Login) -- 这里加一个show是针对创角的
end

return GameStateManager
