local SystemInfoManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager

local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local error_code = GameWorld.error_code
local action_config = GameWorld.action_config
local SystemSettingManager = PlayerManager.SystemSettingManager

function SystemInfoManager:Init()

end

function SystemInfoManager:ShowRespTip(act_id, errorCode, args)
    -- print(tostring(errorCode) .. '--------------------')
    --LoggerHelper.Error("SystemInfoManager.ShowRespTip")
    if errorCode > 0 then
        if errorCode == error_code.ERR_SETTING_SECURITY_LOCK_IS_EFFECTIVE then
            SystemSettingManager:CheckLock()
            return
        end

        if errorCode == error_code.ERR_COUPONS_BIND_NOT_ENOUGH and act_id == action_config.REWARD_BACK_GET_REWARD then
            return
        end

        if errorCode == error_code.ERR_EQUIP_LOAD_NO_EQUIP then
            --LoggerHelper.Error("error_code:2144")
            return
        end

        
        local data = SystemInfoDataHelper.GetSystemInfoData(errorCode)
        if data == nil then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = errorCode
            GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(-1, argsTable))
            return
        end
        -- print(tostring(data.type) .. '--------------------')
        if data.type == nil then
            LoggerHelper.Error("系统消息类型缺漏：" .. tostring(data.id))
        end
        
        --钻石不足弹窗
        if errorCode == error_code.ERR_COUPONS_NOT_ENOUGH then
            GUIManager.ShowChargeMessageBox()
        end
        
        if args then
            for k, v in pairs(args) do
                if type(v) == "table" then
                    LoggerHelper.Error("ShowRespTip Error, act_id: " .. tostring(act_id)
                        .. " errorCode: " .. tostring(errorCode)
                        .. " args: " .. PrintTable:TableToStr(args))
                    return
                end
            end
        end
        for i = 1, #data.type do
            local channelType = data.type[i]
            if channelType == 24 then
                -- print(tostring(data.type) .. '==============')
                local argsTable = LanguageDataHelper.GetArgsTable()
                for k, v in pairs(args or {}) do
                    argsTable["" .. k] = v
                end
                GUIManager.ShowText(1, SystemInfoDataHelper.CreateContent(errorCode, argsTable))
            elseif channelType == 12 then --公会记事 显示
                EventDispatcher:TriggerEvent(GameEvents.ON_BROADCAST_IN_GUILD_CHAR, act_id, errorCode, args)
                --LoggerHelper.Error("channelType == 12"..PrintTable:TableToStr(args))
                EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red,true)
                
            elseif channelType == public_config.CHAT_CHANNEL_SYSTEM then --系统聊天 显示

                local level = GlobalParamsHelper.GetParamValue(922)
                local errorList = GlobalParamsHelper.GetParamValue(923)
                for i,v in ipairs(errorList) do
                    if tonumber(v) == tonumber(errorCode) then
                        if GameWorld.Player().level<level then
                            return
                        end
                    end
                end

                EventDispatcher:TriggerEvent(GameEvents.ON_BROADCAST_IN_SYS_CHAR, act_id, errorCode, args)


            elseif channelType == 25 or channelType == 26 or channelType == 27 then --跑马灯显示（中）
                local argsTable = LanguageDataHelper.GetArgsTable()
                for k, v in pairs(args or {}) do
                    argsTable[tostring(k)] = v
                end

                local level = GlobalParamsHelper.GetParamValue(922)
                local errorList = GlobalParamsHelper.GetParamValue(923)
                for i,v in ipairs(errorList) do
                    if tonumber(v) == tonumber(errorCode) then
                        if GameWorld.Player().level<level then
                            return
                        end
                    end
                end
                

                GUIManager.ShowNewsTicke(SystemInfoDataHelper.CreateContent(errorCode, argsTable),nil,nil,channelType)
            elseif channelType == 26 then --跑马灯显示(上)
                -- local argsTable = LanguageDataHelper.GetArgsTable()
                -- for k, v in pairs(args or {}) do
                --     argsTable[tostring(k)] = v
                -- end
                -- GUIManager.ShowNewsTicke(SystemInfoDataHelper.CreateContent(errorCode, argsTable))
            elseif channelType == 27 then --跑马灯显示(下)
                -- local argsTable = LanguageDataHelper.GetArgsTable()
                -- for k, v in pairs(args or {}) do
                --     argsTable[tostring(k)] = v
                -- end
                -- GUIManager.ShowNewsTicke(SystemInfoDataHelper.CreateContent(errorCode, argsTable))
            elseif channelType == 31 then --系统喇叭
                local argsTable = LanguageDataHelper.GetArgsTable()
                for k, v in pairs(args or {}) do
                    argsTable[tostring(k)] = v
                end
                
                GUIManager.ShowNewsTicke(SystemInfoDataHelper.CreateContent(errorCode, argsTable),nil,nil,channelType)
            end
        end
    
    end
end

function SystemInfoManager.ShowTextByType(type, text)
    if type == 24 then
        GameManager.GUIManager.ShowText(1, text, 1)
    elseif type == 1 then --聊天 世界频道
        GUIManager.ShowNewsTicke(text,nil,nil,type)
    elseif type == 3 then --聊天 军团频道
        GUIManager.ShowNewsTicke(text,nil,nil,type)
    elseif type == 23 then
        GUIManager.ShowNewsTicke(text,nil,nil,type)
    end
end

function SystemInfoManager:ShowClientTip(id)
    self:ShowRespTip(0, id, nil)
end


SystemInfoManager:Init()
return SystemInfoManager
