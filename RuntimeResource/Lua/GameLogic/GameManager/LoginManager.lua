-- LoginManager.lua
local LoginManager = {}

local LocalSetting = GameConfig.LocalSetting
local EntityConfig = GameConfig.EntityConfig
local Application = UnityEngine.Application
local ServerListManager = GameManager.ServerListManager
local ServerProxyFacade = GameMain.ServerProxyFacade
local Time = Time
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local Login_CD = 5

function LoginManager:Init()
    self._lastLoginTime = 0
    self._loginFunc = function(account, password, serverid)self:Login(account, password, serverid) end
    EventDispatcher:AddEventListener(GameEvents.Login, self._loginFunc)
end

function LoginManager:Login(account, password, serverid)
    if Time.realtimeSinceStartup - self._lastLoginTime < 0 then return end
    self._lastLoginTime = Time.realtimeSinceStartup + Login_CD --连接保护，短时间重复连接会导致主线程阻塞
    local ip = ServerListManager:SelectedServerIp()
    local port = ServerListManager:SelectedServerPort()
    local serverid = ServerListManager:SelectedServerId()
    --没有选择服务器时用默认服务器，没有默认服务器弹出选服界面
    --其实用户名和密码没必要保存,一般由sdk保存，另外明文保存密码有风险
    if ip == "" or port == 0 then
        ip = LocalSetting.settingData.ServerIp
        port = LocalSetting.settingData.ServerPort
        -- LoggerHelper.Log("ip2:" .. tostring(ip) .. "   ,port2:" .. tostring(port))
        if ip == nil or port == nil then
            GameManager.GUIManager.ShowPanel(PanelsConfig.SelectServer)
            return
        end
    end
    LocalSetting.settingData.UserAccount = account
    LocalSetting.settingData.SelectedServerID = serverid
    LocalSetting.settingData.ServerIp = ip
    LocalSetting.settingData.ServerPort = port
    LocalSetting:Save()
    GameWorld.SetServerId(serverid .. " " .. ip .. " " .. port)
    GameWorld.SetAccount(account)
    -- GameWorld.TempConnected = (tonumber(ServerListManager:SelectedServerPort()) > 0)
    -- if GameWorld.TempConnected then
    self:LoginWeb(ip, port, account)
-- else
--     GameManager.GUIManager.ShowPanel(PanelsConfig.CreateRole)
-- end
end

function LoginManager:CreateRole(finish)
    -- LoggerHelper.Log("CreateRole")
    GameManager.GUIManager.ClosePanel(PanelsConfig.Login)
    GameManager.GUIManager.ShowPanel(PanelsConfig.CreateRole)
    GameManager.CreateRoleManager.CreateRoleEnterGameEvent = function()
        LoggerHelper.Log("CreateRoleEnterGameEvent")
        finish()
    end
end

function LoginManager:LoginWeb(ip, port, account, password)
    local template = "http://%s:%d/cgi-bin/login?account=%s"
    local url = string.format(template, ip, port, account)
    local onDone = function(result)self:OnLoginWebDone(result) end
    local onFail = function(result)self:OnLoginWebFail(result) end
    -- LoggerHelper.Log("LoginWeb:", url)
    GameWorld.GetHttpUrl(url, onDone, onFail)
end

function LoginManager:OnLoginWebDone(result)
    -- LoggerHelper.Log("OnLoginWebDone: " .. result)
    --local lstr = tolua.tolstring(result)
    local lst = string.split(result, ',')
    -- LoggerHelper.Log("lst[1]:", lst[1])
    local errorId = tonumber(lst[1])
    if errorId == 0 then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_11)
        self.baseIP = lst[2]
        self.basePort = tonumber(lst[3])
        self.baseLoginKey = lst[4];
        self:LoginPC()
    else
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_12, errorId)
        EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
        local text = LanguageDataHelper.CreateContent(584, errorId)
        local value = {["confirmCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
        end,
        ["cancelCB"] = function()
            GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
        end,
        ["text"] = text}
        GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    end

end

function LoginManager:OnLoginWebFail(result)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_13, result)
    EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
    --LoggerHelper.Error("OnLoginWebFail:" .. result)
    local cfg = GlobalParamsHelper.GetParamValue(884)
    local text = ""
    if cfg[tonumber(result)] == nil then
        text = LanguageDataHelper.CreateContent(584, result)
    else
        text = LanguageDataHelper.CreateContent(cfg[result])
    end
    local value = {["confirmCB"] = function()
        GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["cancelCB"] = function()
        GameManager.GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["text"] = text}
    GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
end

function LoginManager:LoginPC()
    if self._isLinking == true then return end
    self._isLinking = true
    -- LoggerHelper.Log("LoginManager:LoginPC ====== ", self.baseIP, self.basePort)
    local connectedCB = function(result)
        if result then
            ServerProxyFacade.SetBaseServer(self.baseIP, self.basePort)
            ServerProxyFacade.BaseLogin(self.baseLoginKey)
            -- LoggerHelper.Log("LoginPC:", result)
            GameWorld.IsClient = false
        --PlayerTimerManager.GetInstance().enableNetTimeoutCheck(true);
        --ServerProxyFacade.CheckDefMD5()
        -- else
        --     LoggerHelper.Log("LoginPC2:", result)
        end
        self._isLinking = false
    end
    ServerProxyFacade.IsEnableReconnect(true)
    ServerProxyFacade.Connect(self.baseIP, self.basePort, connectedCB)
end

function LoginManager:CrossServer(ip, port, key)
    self.baseIP = ip
    self.basePort = port
    self.baseLoginKey = key
    ServerProxyFacade.Disconnect()
    self:LoginPC()
end

LoginManager.ShowServerNameTextAction = nil
function LoginManager.ShowServerNameText(text)
    if LoginManager.ShowServerNameTextAction ~= nil then
        LoginManager.ShowServerNameTextAction(text)
    end
end

return LoginManager
