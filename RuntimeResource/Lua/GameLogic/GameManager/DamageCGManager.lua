--DamageCGManager.lua
require "GameManager/DamageSystem/DamageType"

DamageCGManager = {}

local Camera = UnityEngine.Camera
local Rect = UnityEngine.Rect
local Screen = UnityEngine.Screen
local Vector3 = UnityEngine.Vector3
local GameObject = UnityEngine.GameObject
local RectTransform = UnityEngine.RectTransform
local AttackType = DamageType.AttackType
local ArtNumberType = DamageType.ArtNumberType
local ArtWord = DamageType.ArtWord

DamageCGManager.HIDE_POSITION = Vector3.New(-1000,-1400,0)
DamageCGManager._numberGo = nil
DamageCGManager._damageGo = nil
DamageCGManager._wordGo = nil
DamageCGManager._itemDict = {}
DamageCGManager._underTransform = nil
DamageCGManager._topTransform = nil
DamageCGManager._damageGridStack = {}
DamageCGManager._damageItemStackDic = {}
DamageCGManager._damageGridNumber = 0
DamageCGManager._currentCount = 0
DamageCGManager._maxItemStackNum = 7
DamageCGManager._maxGridCount = 10
DamageCGManager._hitTypeArtNumberTypeMap = {}
DamageCGManager._hitTypeArtWordTypeMap = {}

function DamageCGManager:SetPanelInfo(numberGo,damageGo,wordGo,underTransform,topTransform)
    self._numberGo = numberGo
    self._damageGo = damageGo
    self._wordGo = wordGo
    self._underTransform = underTransform
    self._topTransform = topTransform
    self._numberGo.transform.localPosition = self.HIDE_POSITION
    self._wordGo.transform.localPosition = self.HIDE_POSITION
    self._damageGo.transform.localPosition = self.HIDE_POSITION
    self:InitItemDict()
    self:InitHitType2ArtNumberType()
    self:InitHitType2ArtWordType()
end

function DamageCGManager:InitItemDict()
    for i = 1,self._numberGo.transform.childCount do
        local item = self._numberGo.transform:GetChild(i-1).gameObject
        self._itemDict[string.sub(item.name,8,-1)] = item
    end
    for i = 1,self._wordGo.transform.childCount do
        local item = self._wordGo.transform:GetChild(i-1).gameObject
        self._itemDict[string.sub(item.name,9,-1)] = item
    end
end

function DamageCGManager:InitHitType2ArtNumberType()
    self._hitTypeArtNumberTypeMap[AttackType.HIT] = ArtNumberType.HIT
    self._hitTypeArtNumberTypeMap[AttackType.CRITICAL] = ArtNumberType.CRITICAL
    self._hitTypeArtNumberTypeMap[AttackType.GET_HIT] = ArtNumberType.GET_HIT
    self._hitTypeArtNumberTypeMap[AttackType.TREAT] = ArtNumberType.TREAT
    self._hitTypeArtNumberTypeMap[AttackType.PET_HIT] = ArtNumberType.PET_HIT
    self._hitTypeArtNumberTypeMap[AttackType.TREASURE_HIT] = ArtNumberType.TREASURE_HIT
    self._hitTypeArtNumberTypeMap[AttackType.DEADLY] = ArtNumberType.DEADLY
    self._hitTypeArtNumberTypeMap[AttackType.TREAT] = ArtNumberType.TREAT
end

function DamageCGManager:InitHitType2ArtWordType()
    self._hitTypeArtWordTypeMap[AttackType.MISS] = ArtWord.MISS
    self._hitTypeArtWordTypeMap[AttackType.CRITICAL] = ArtWord.CRITICAL
    self._hitTypeArtWordTypeMap[AttackType.DEADLY] = ArtWord.DEADLY
    self._hitTypeArtWordTypeMap[AttackType.ABSORB] = ArtWord.ABSORB
    self._hitTypeArtWordTypeMap[AttackType.INVINCIBLE] = ArtWord.INVINCIBLE
    self._hitTypeArtWordTypeMap[AttackType.TREAT] = ArtWord.TREAT
    self._hitTypeArtWordTypeMap[AttackType.PARRY] = ArtWord.PARRY
    self._hitTypeArtWordTypeMap[AttackType.UNYIELDING] = ArtWord.UNYIELDING
end

function DamageCGManager:HitType2ArtNumberType(type,isPlayer)
    if isPlayer and type == AttackType.HIT then
        return ArtNumberType.GET_HIT
    end
    if self._hitTypeArtNumberTypeMap[type] ~= nil then
        return self._hitTypeArtNumberTypeMap[type]
    end
    return ArtNumberType.NOTSHOW
end

function DamageCGManager:HitType2ArtWordType(type,isPlayer)
    if self._hitTypeArtWordTypeMap[type] ~= nil then
        return self._hitTypeArtWordTypeMap[type]
    end
    return ""
end

function DamageCGManager:CreateDamageNumber(position,damage,type,isPlayer)
    if damage == 0 and (type == AttackType.HIT or type == AttackType.CRITICAL or type == AttackType.GET_HIT) then
        return
    end
    if self._currentCount > self._maxGridCount then return end
    local screenPosition = Camera.main:WorldToScreenPoint(position)
    if self:CheckPositionIsInScreen(screenPosition) then
        local damageGrid = self:GetDamageGrid()
        local colorType = self:HitType2ArtNumberType(type,isPlayer)
        local word = self:HitType2ArtWordType(type,isPlayer)
        --不应该传负数，矫正
        if damage < 0 then
            damage = -damage
        end
        if word ~= "" then
            self:CreateDamageWord(damageGrid,word,isPlayer)
        end
        if colorType ~= ArtNumberType.NOTSHOW then
            damageGrid = self:CreateNumber(damageGrid,colorType,damage,isPlayer)
        end        
        damageGrid:Show(position,type)
        self._currentCount = self._currentCount + 1
    end
end

function DamageCGManager:CreateNumber(damageGrid,colorType,value,isPlayer)
    local str = tostring(value)
    for i = 1,#str do
        local name = colorType..string.sub(str,i,i)
        local damageItem = self:GetDamageItem(name,isPlayer)
        damageGrid:AddNumberItem(damageItem)
    end
    return damageGrid
end

function DamageCGManager:CreateDamageWord(damageGrid,word,isPlayer)
    local damageItem = self:GetDamageItem(word,isPlayer)
    damageGrid:AddNumberItem(damageItem)
    return damageGrid
end

function DamageCGManager:CheckPositionIsInScreen(position)   
    local screenRect = Rect.New(0,0,Screen.width,Screen.height)
    return screenRect:Contains(position)
end

function DamageCGManager:GetDamageGrid()
    local damageGrid = nil
    if #self._damageGridStack > 0 then
        damageGrid = table.remove(self._damageGridStack)
    else
        damageGrid = self:AddDamageGrid()
    end
    return damageGrid
end

function DamageCGManager:AddDamageGrid()
    local go = GameObject.Instantiate(self._damageGo)
    self._damageGridNumber = self._damageGridNumber + 1
    go.name = "DamageGridCG"..self._damageGridNumber
    local damageGrid = GameWorld.AddLuaBehaviour(go,"Modules.ModuleTextFloatCG.ChildComponent.DamageGridCG")
    local trans = go:GetComponent(typeof(RectTransform))
    if trans == nil then
        trans = go:AddComponent(typeof(RectTransform))
    end
    trans:SetParent(self._underTransform,false)
    return damageGrid
end

function DamageCGManager:RemoveDamageGrid(damageGrid)
    damageGrid:Hide()
    table.insert(self._damageGridStack,damageGrid)
    self._currentCount = self._currentCount - 1
end

function DamageCGManager:GetDamageItem(name,isTop)
    isTop = isTop or false
    local damageItem = nil
    if self._damageItemStackDic[name] ~= nil then
        local damageItemStack = self._damageItemStackDic[name]
        if #damageItemStack > 0 then
            damageItem = table.remove(damageItemStack)
            return damageItem
        end
    end
    damageItem = self:AddItem(name,isTop)
    return damageItem
end

function DamageCGManager:AddItem(name,isTop)    
    local itemGo = self._itemDict[name]
    if itemGo == nil then
        LoggerHelper.Error(name .. " is not exist in panel")
        return
    end
    local go = GameObject.Instantiate(itemGo)
    go.name = name
    if isTop then
        go.transform:SetParent(self._topTransform)
    else
        go.transform:SetParent(self._underTransform)
    end
    local damageItem = GameWorld.AddLuaBehaviour(go,"Modules.ModuleTextFloatCG.ChildComponent.DamageItemCG")
    damageItem.Name = name
    return damageItem
end

function DamageCGManager:RemoveItemFormList(damageItemList)
    for i = 1,#damageItemList do
        local damageItemStack = nil
        local item = damageItemList[i]
        local itemName = item.Name
        if self._damageItemStackDic[itemName] ~= nil then
            damageItemStack = self._damageItemStackDic[itemName]
        else
            damageItemStack = {}
            self._damageItemStackDic[itemName] = damageItemStack
        end
        if #damageItemStack < self._maxItemStackNum then
            table.insert(damageItemStack,item)
            item:Hide()
        else
            GameWorld.Destroy(item.gameObject)
        end
    end
end

function DamageCGManager:ClearPool()
    while #self._damageGridStack > 0 do
        local obj = table.remove(self._damageGridStack)
        GameWorld.Destroy(obj.gameObject)
    end
    self._damageGridStack = {}
    for k,v in pairs(self._damageItemStackDic) do
        while #v > 0 do
            local obj = table.remove(v)
            GameWorld.Destroy(obj.gameObject)
        end
        v = {}
    end
end

return DamageCGManager