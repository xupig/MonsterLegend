--ClientEntityManager.lua
--静态拾取物品管理器
ClientEntityManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local CollectManager = GameManager.CollectManager
local clientSceneData = PlayerManager.PlayerDataManager.clientSceneData
local InstanceManager = PlayerManager.InstanceManager
local EntityType = GameConfig.EnumType.EntityType
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local BackwoodsDataHelper = GameDataHelper.BackwoodsDataHelper
local GodMonsterIslandDataHelper = GameDataHelper.GodMonsterIslandDataHelper
local BossHomeDataHelper = GameDataHelper.BossHomeDataHelper
local GameSceneManager = GameManager.GameSceneManager
local BossType = GameConfig.EnumType.BossType
local spaceDrops = {}			--所有单个掉落SpaceDrop
local bossTombs = {}			--所有boss坟墓[bossId]= entity
ClientEntityManager.bossargs=nil
function ClientEntityManager.Init()
	-- EventDispatcher:AddEventListener(GameEvents.CheckClientMonsterDeath,function (monsterId,pos)
	-- 	ClientEntityManager.CreateClientSpaceDrop(monsterId,pos)
	-- end)

	EventDispatcher:AddEventListener(GameEvents.ENTER_MAP,function ()
		if ClientEntityManager.bossargs then
			self.UpdateBossTomb(ClientEntityManager.bossargs[1],ClientEntityManager.bossargs[2])
		end
	end)

	EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP,function ()
		ClientEntityManager.bossargs=nil		
	end)
end

-------------------------------------服务端副本掉落处理-----------------------------------------
function ClientEntityManager.CreateSpaceDropByClient(own_drop_id,item_index,item_id,item_count,dropPosition)
	--LoggerHelper.Log("CreateSpaceDropByClient"..own_drop_id)
	local args = {itemId = item_id,itemCount = item_count, ownDropId = own_drop_id, index = item_index, isClient = false}
    local spaceDrop = GameWorld.CreateEntity("SpaceDrop", args)
	spaceDrop:SetRandomPosition(dropPosition,3)
	if spaceDrops[own_drop_id] == nil then
		spaceDrops[own_drop_id] = {}
	end
	spaceDrops[own_drop_id][item_index] = spaceDrop
end

--检查单个掉落是否存在
function ClientEntityManager.CheckExcist(own_drop_id,item_index)
	if spaceDrops[own_drop_id] and spaceDrops[own_drop_id][item_index] then
		return true
	else
		return false
	end
end

--删除单个掉落
function ClientEntityManager.DestroySpaceDrop(own_drop_id,item_index,reason)
	--LoggerHelper.Log("DestroySpaceDrop"..own_drop_id..","..item_index..","..reason)
	if spaceDrops[own_drop_id] == nil then
		return
	end
	local spaceDrop = spaceDrops[own_drop_id][item_index]
	if spaceDrop then
		--1 拾取
		if reason == 1 then
			spaceDrop:FlyToEntity()
		--0 直接删除
		else
			spaceDrop:Destroy()
		end
		spaceDrops[own_drop_id][item_index] = nil
	end
end

--删除掉落堆
function ClientEntityManager.DestroyDropItem(own_drop_id,reason)
	if spaceDrops[own_drop_id] == nil then
		return
	end
	--LoggerHelper.Error("DestroyDropItem"..own_drop_id)
	for item_index,v in pairs(spaceDrops[own_drop_id]) do
		ClientEntityManager.DestroySpaceDrop(own_drop_id,item_index,reason)
	end
	spaceDrops[own_drop_id] = nil
end

--判断某个范围内是否有掉落可以拾取
function ClientEntityManager.HasSpaceDrop(range)
	local entities = GameWorld.Entities()
    local player = GameWorld.Player()
    for k, v in pairs(entities) do
        if v.entityType == EntityType.SpaceDrop and v:GetCanPick() then
            local dis = Vector3.Distance(player:GetPosition(), v:GetPosition())
            if dis <= range and ClientEntityManager.isConfigDropEntity(player, v) then
                return true
            end
        end
    end
    return false
end

--判断是否是设定的自动拾取类型
function ClientEntityManager.isConfigDropEntity(player, entity)
    local setting = player.hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK]
    local itemId = entity.itemId
    -- local isFull = itemId > public_config.MAX_MONEY_ID and bagData:GetPkg(public_config.PKG_TYPE_ITEM):IsFull()
    -- if isFull then
    --     return false
    -- end
    if setting[public_config.AUTO_PICK_WHITE] == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 1 then
            return true
        end
    end

    if setting[public_config.AUTO_PICK_BLUE] == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 3 then
            return true
        end
    end

    if setting[public_config.AUTO_PICK_PURPLE] == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 4 then
            return true
        end
    end

    if setting[public_config.AUTO_PICK_ORANGE] == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality >= 5 then
            return true
        end
    end

    --金币
    if setting[public_config.AUTO_PICK_GOLD] == 1 then
        if itemId == public_config.MONEY_TYPE_GOLD then
            return true
        end
    end

    if setting[public_config.AUTO_PICK_OTHER] == 1 then
        return true
    end
    return false
end

-------------------------------------BOSS坟墓处理-----------------------------------------
function ClientEntityManager.UpdateBossTomb(bossInfos,bossType)
	for i=1,#bossInfos do
		local boss_id = bossInfos[i][1]
		local revive_time = bossInfos[i][2]
		if revive_time == 0 then
			if bossTombs[boss_id] then
				ClientEntityManager.DestroyBossTomb(boss_id)
			end
		else
			if bossTombs[boss_id] == nil then
				local bossName=''
				local regionId
				if bossType==BossType.BossWorld then
					regionId = WorldBossDataHelper:GetBossRegionId(boss_id)	
					bossName = LanguageDataHelper.CreateContent(WorldBossDataHelper:GetBossName(boss_id))
				elseif bossType==BossType.BossHome then
					regionId = BossHomeDataHelper:GetBossRegionId(boss_id)						
					bossName = LanguageDataHelper.CreateContent(BossHomeDataHelper:GetBossName(boss_id))
				elseif bossType==BossType.BackBoss then
					regionId = BackwoodsDataHelper:GetBossRegionId(boss_id)						
					bossName = LanguageDataHelper.CreateContent(BackwoodsDataHelper:GetBossName(boss_id))
				elseif bossType==BossType.GodIsLand then
					regionId = GodMonsterIslandDataHelper:GetBossRegionId(boss_id)						
					bossName = LanguageDataHelper.CreateContent(GodMonsterIslandDataHelper:GetName(boss_id))
				end			
				local s = GameSceneManager:GetActionByID(regionId)
				if s then	
					local Id = s.refresh_point_ids[1]
					local position = GameSceneManager:GetActionByID(Id).pos
					local args = {bossId = boss_id, reviveTime = revive_time,bossname = bossName}
					local bossTomb = GameWorld.CreateEntity("BossTomb", args)
					bossTomb:SetPosition(position)
					bossTombs[boss_id] = bossTomb
				end
	
			end
		end
	end
end

--删除boss坟墓
function ClientEntityManager.DestroyBossTomb(boss_id)
	if boss_id then
		bossTombs[boss_id]:Destroy()
		bossTombs[boss_id] = nil
	end
end
-------------------------------------客户端副本掉落处理-----------------------------------------
--创建客户端副本掉落
-- function ClientEntityManager.CreateClientSpaceDrop(monsterId ,dropPosition)
-- 	local info,index = clientSceneData:GetDropsByMonsterId(monsterId)
-- 	if info then
-- 		for item_id,itemCount in pairs(info) do
-- 			if item_id == public_config.MONEY_TYPE_EXP then
-- 				InstanceManager:RequestClientKillMonster(index,item_id)
-- 			else
-- 				local args = {itemId = item_id, ownDropId = index,isClient = true}
-- 			    local clientspaceDrop = GameWorld.CreateEntity("SpaceDrop", args)
-- 				clientspaceDrop:SetRandomPosition(dropPosition,6)
-- 				if ClientEntityManager.ClientspaceDrops[index] == nil then
-- 					ClientEntityManager.ClientspaceDrops[index] = {}
-- 				end
-- 				ClientEntityManager.ClientspaceDrops[index][item_id] = clientspaceDrop
-- 			end
-- 		end
-- 	end
-- end

-- --拾取客户端副本掉落
-- function ClientEntityManager.PickClientSpaceDrop(own_drop_id,itemId)
-- 	local clientspaceDrop = ClientEntityManager.ClientspaceDrops[own_drop_id][itemId]
-- 	if clientspaceDrop then
-- 		clientspaceDrop:FlyToEntity()
-- 		ClientEntityManager.ClientspaceDrops[own_drop_id][itemId] = nil
-- 		InstanceManager:RequestClientKillMonster(own_drop_id,itemId)
-- 	end
-- end

-- --全部拾取
-- function ClientEntityManager.PickAllClientSpaceDrop()
-- 	for own_drop_id,v in pairs(ClientEntityManager.ClientspaceDrops) do
-- 		for itemId,v1 in pairs(v) do
-- 			InstanceManager:RequestClientKillMonster(own_drop_id,itemId)
-- 		end
-- 	end
-- end


ClientEntityManager.Init()
return ClientEntityManager