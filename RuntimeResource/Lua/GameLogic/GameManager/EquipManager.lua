--EquipManager.lua
local action_config = GameWorld.action_config

local EquipManager = {}
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local EquipDataHelper = GameDataHelper.EquipDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BagManager = PlayerManager.BagManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local TipsManager = GameManager.TipsManager

function EquipManager:Init()
	self._refreshEquipLoadRedPointState = function(bagType,equipType) self:RefreshEquipLoadRedPointState(bagType,equipType) end

	self._refreshStrengthRedPointState = function(pos) self:RefreshStrengthRedPointState(pos) end
	self._refreshStrengthRedPointStateByTimer = function() self:RefreshStrengthRedPointStateByTimer() end
	self._delayEquipStrengthStateHandlerCb = function() self:DelayStrengthRedPointStateHandler() end 
	self._refreshGemInlayRedPointState = function(pos) self:RefreshGemInlayRedPointState(pos) end

	self._refreshGemRefineRedPointState = function(pos) self:RefreshGemRefineRedPointState(pos) end
	self._refreshSuitRedPointState = function(pos) self:RefreshSuitRedPointState(pos) end
	self._refreshWashRedPointState = function() self:RefreshWashRedPointState() end

	self._updateFunctionOpenCb = function(funcId) self:UpdateFunctionOpen(funcId) end
	self._itemCHangeIncreaseCb =  function (pkgType, pos)
		self:CheckBetterEquip(pkgType, pos)
		self:CheckItemADD(pkgType, pos)
	end

	self._gemSlotTypes = GlobalParamsHelper.GetParamValue(760)
	self._vipSlotLevel = GlobalParamsHelper.GetParamValue(469)

	self._washOpenSlotCost = GlobalParamsHelper.GetParamValue(452)
	self._freeWashTimeMax = GlobalParamsHelper.GetParamValue(453)
end

function EquipManager:UpdateFunctionOpen(funcId)
	if funcId == public_config.FUNCTION_ID_EQUIP_STREN then
   		 self:RefreshStrengthRedPointState()
	elseif funcId == public_config.FUNCTION_ID_MOSAIC then
		self:RefreshGemInlayRedPointState()
	elseif funcId == public_config.FUNCTION_ID_REFINING then
		self:RefreshGemRefineRedPointState()
	elseif funcId == public_config.FUNCTION_ID_Kill_XIAN or funcId == public_config.FUNCTION_ID_Kill_SHEN then
		self:RefreshSuitRedPointState()
	elseif funcId == public_config.FUNCTION_ID_EQUIP_WASH then
		self:RefreshWashRedPointState()
	end
end

function EquipManager:OnPlayerEnterWorld()
	--获得物品
	EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_INCREASE_FLOAT, self._itemCHangeIncreaseCb)

	EventDispatcher:AddEventListener(GameEvents.Update_Bag_Equip_State, self._refreshEquipLoadRedPointState)

	EventDispatcher:AddEventListener(GameEvents.Refresh_Equip_Strength_Red_Point_State, self._refreshStrengthRedPointState)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._refreshStrengthRedPointStateByTimer)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Gem_Inlay_Red_Point_State, self._refreshGemInlayRedPointState)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Gem_Refine_Red_Point_State, self._refreshGemRefineRedPointState)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Suit_Red_Point_State, self._refreshSuitRedPointState)

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._refreshWashRedPointState)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._refreshWashRedPointState)

    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._updateFunctionOpenCb)

    self._betterEquipStates = {}
    self._strengthEquipStates = {}
    self._gemInlayEquipStates = {}
    self._gemRefineEquipStates = {}
    self._legendSuitStates = {}
    self._warLordSuitStates = {}

    for i=1,12 do
    	self._betterEquipStates[i] = false
    	self._strengthEquipStates[i] = false
    	self._gemInlayEquipStates[i] = false
    	self._gemRefineEquipStates[i] = false
    	self._legendSuitStates[i] = false
    	self._warLordSuitStates[i] = false
    end
    self._betterEquipNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.EQUIP_LOAD, public_config.FUNCTION_ID_BAG)--更好装备
    self._bagRedpointNode = RedPointManager:CreateEmptyNode()

    self._strengthNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.EQUIP_STRENGTH, public_config.FUNCTION_ID_EQUIP_STREN)--装备强化
    self._gemInlayNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.GEM_INLAY, public_config.FUNCTION_ID_MOSAIC)--宝石镶嵌
    self._gemRefineNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.GEM_REFINE, public_config.FUNCTION_ID_REFINING)--宝石精炼
    self._legendSuitNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.LEGEND_SUIT, public_config.FUNCTION_ID_Kill_XIAN)--传奇套装
    self._warlordSuitNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WARLORD_SUIT, public_config.FUNCTION_ID_Kill_SHEN)--战神套装

    self:RefreshEquipLoadRedPointState()
    self:RefreshStrengthRedPointState()
    self:RefreshGemInlayRedPointState()
    self:RefreshGemRefineRedPointState()
    self:RefreshSuitRedPointState()
    self:RefreshWashRedPointState()

    --小天使续费查看
	self:AngelRenew()
	self:CheckOffineTime()
end

function EquipManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_INCREASE_FLOAT, self._itemCHangeIncreaseCb)
	EventDispatcher:RemoveEventListener(GameEvents.Update_Bag_Equip_State, self._refreshEquipLoadRedPointState)

	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Equip_Strength_Red_Point_State, self._refreshStrengthRedPointState)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._refreshStrengthRedPointStateByTimer)

    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Gem_Inlay_Red_Point_State, self._refreshGemInlayRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Gem_Refine_Red_Point_State, self._refreshGemRefineRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Suit_Red_Point_State, self._refreshSuitRedPointState)

    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._refreshWashRedPointState)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._refreshWashRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._updateFunctionOpenCb)
end

function EquipManager:RegisterNodeUpdateFunc(func)
	RedPointManager:RegisterNodeUpdateFunc(self._bagRedpointNode, func)
end

function EquipManager:RefreshEquipLoadRedPointState(bagType,equipType)
	--初始化
	if bagType == nil then
		local itemList = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
		for k,itemData in pairs(itemList) do
			local equipType1 = itemData:GetType() 
			if itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
				self._betterEquipStates[equipType1] = self._betterEquipStates[equipType1] or self:GetSingleEquipBetterState(equipType1,itemData)
			end
		end
	--背包变化/身上装备变化
	else
		local state = false
		local itemList = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
		for k,itemData in pairs(itemList) do
			if itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP and itemData:GetType() == equipType 
				and self:GetSingleEquipBetterState(equipType,itemData) then
				state = true
				break
			end
		end
		self._betterEquipStates[equipType] = state
	end

	--60级前不触发红点
	if GameWorld.Player().level < 60 then
		return
	end

	for i=1,12 do
		if self._betterEquipStates[i] then
			self._betterEquipNode:CheckStrengthen(true)
			self._bagRedpointNode:SetRedPoint(true)
			return
		end
	end
	self._betterEquipNode:CheckStrengthen(false)
	self._bagRedpointNode:SetRedPoint(false)
end

function EquipManager:GetSingleEquipBetterState(equipType,itemData)
	if itemData:CheckCanLoaded() and itemData:CheckBetterThanLoaded() then
		--守护小天使要特殊判断，只有身上没有才认为有红点
		if equipType == public_config.PKG_LOADED_EQUIP_INDEX_GUARD then
			local equipGuard = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(public_config.PKG_LOADED_EQUIP_INDEX_GUARD)
			if equipGuard[1] or itemData:CheckGuardTimeOut() then
				return false
			else
				return true
			end
		else
			--LoggerHelper.Error("GetSingleEquipBetterState"..itemData.bagPos..","..equipType,true)
			return true
		end
	else
		return false
	end
	
end

--刷新强化红点状态
function EquipManager:RefreshStrengthRedPointState(pos)
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_EQUIP_STREN) then
        return
    end
	local itemList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
	if pos == nil or itemList[pos] == nil then
		for k,itemData in pairs(itemList) do
			if k <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
				self._strengthEquipStates[k] = self:CheckSingleEquipStrength(itemData)
			end
		end
	else
		if pos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
			self._strengthEquipStates[pos] = self:CheckSingleEquipStrength(itemList[pos])
		end
	end
	for i=1,10 do
		if self._strengthEquipStates[i] then
			self._strengthNode:CheckStrengthen(true)
			return
		end
	end
	self._strengthNode:CheckStrengthen(false)
end

function EquipManager:GetStrengthStateByType(equipType)
	return self._strengthEquipStates[equipType]
end

function EquipManager:CheckSingleEquipStrength(itemData)
	local pos = itemData.bagPos
	local strengthInfo = GameWorld.Player().equip_stren_info[pos]
	local level = 0

	if strengthInfo then
		level = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] or 0
	end
	if level >= itemData.cfgData.strengthHighLv then
		return false
	end
	local curCfg = EquipDataHelper.GetStrengthData(level)
	self._needMoney = curCfg.cost_gold
	if self._needMoney <= GameWorld.Player().money_gold then
		return true
	end 
	return false
end

--延迟处理装备强化的红点（金币获取的时候）
function EquipManager:DelayStrengthRedPointStateHandler()
    TimerHeap.DelTimer(self._delayEquipStrengthStateTimer)
    self._delayEquipStrengthStateTimer = nil
    self:RefreshStrengthRedPointState()
end

function EquipManager:RefreshStrengthRedPointStateByTimer()
	 if self._delayEquipStrengthStateTimer then
        TimerHeap.DelTimer(self._delayEquipStrengthStateTimer)
    end 
    self._delayEquipStrengthStateTimer = TimerHeap:AddSecTimer(1, 0, 0, self._delayEquipStrengthStateHandlerCb)
end

--刷新宝石镶嵌红点状态
function EquipManager:RefreshGemInlayRedPointState(pos)
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_MOSAIC) then
        return
    end

	local itemList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()

	if pos == nil or itemList[pos] == nil then
		for k,itemData in pairs(itemList) do
			if k <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
				self._gemInlayEquipStates[k] = self:CheckSingleEquipGemInlay(itemData)
			end
		end
	else
		if pos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
			self._gemInlayEquipStates[pos] = self:CheckSingleEquipGemInlay(itemList[pos])
		end
	end

	for i=1,10 do
		if self._gemInlayEquipStates[i] then
			self._gemInlayNode:CheckStrengthen(true)
			return
		end
	end
	self._gemInlayNode:CheckStrengthen(false)
end

--单件装备是否能装宝石
function EquipManager:CheckSingleEquipGemInlay(itemData)
	local pos = itemData.bagPos
	local gemInfo = GameWorld.Player().equip_gem_info[pos]
	if gemInfo == nil then
		--LoggerHelper.Error("这件装备宝石数据有问题"..itemData.cfg_id..","..pos)
		return false
	end
	local gemSlotCount = gemInfo[public_config.EQUIP_GEM_INFO_HOLE_COUNT]
	for i=1,gemSlotCount do
		if bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasBetterGemByEquipPos(pos,gemInfo[i]) then
			return true
		end
	end
	--vip宝石孔
	local vipLevel = GameWorld.Player().vip_level or 0
	if vipLevel >= self._vipSlotLevel then
		if bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasBetterGemByEquipPos(pos,gemInfo[6]) then
			return true
		end
	end
	return false
end

function EquipManager:GetGemInlayStateByType(equipType)
	return self._gemInlayEquipStates[equipType]
end

--宝石精炼红点
function EquipManager:RefreshGemRefineRedPointState(pos)
end

--套装红点
function EquipManager:RefreshSuitRedPointState(pos)
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_Kill_XIAN) then
        return
    end

    local itemList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()

	if pos == nil or itemList[pos] == nil then
		for k,itemData in pairs(itemList) do
			if k <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
				local sLegend,sWarlord = self:CheckSingleEquipSuit(itemData)
				self._legendSuitStates[k] = sLegend
				self._warLordSuitStates[k] = sWarlord
			end
		end
	else
		if pos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
			local sLegend,sWarlord = self:CheckSingleEquipSuit(itemList[pos])
			self._legendSuitStates[pos] = sLegend
			self._warLordSuitStates[pos] = sWarlord
		end
	end

	local stateLegend = false
	local stateWarlord = false
	for i=1,10 do
		if self._legendSuitStates[i] then
			stateLegend = true
		end

		if self._warLordSuitStates[i] then
			stateWarlord = true
		end
	end
	self._legendSuitNode:CheckStrengthen(stateLegend)
	self._warlordSuitNode:CheckStrengthen(stateWarlord)
end

function EquipManager:CheckSingleEquipSuit(itemData)
	local pos = itemData.bagPos
	local suitData = GameWorld.Player().equip_suit_data or {}
	local suitLevel = suitData[pos] or 0
	local suitId = itemData:GetSuitId()
	local sLegend = false
	local sWarlord = false

	if suitId then
		if suitLevel == 0 then
			local cost = itemData:GetSuitCost()[suitId[1]]
			if self:CheckSuitCost(cost) then
				sLegend = true
			end
		elseif suitLevel == 1 and suitId[2] then
			local cost = itemData:GetSuitCost()[suitId[2]]
			if self:CheckSuitCost(cost) then
				sWarlord = true
			end
		end
	end

	return sLegend,sWarlord
end

function EquipManager:CheckSuitCost(cost)
	local b = true
	for i=1,#cost,2 do
		local itemId = cost[i]
		local needCount = cost[i+1]
		local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagCount < needCount then
			b = false
		end
	end

	return b
end

--洗炼红点
function EquipManager:RefreshWashRedPointState()
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_EQUIP_WASH) then
        return
    end
	local state = false
	local dayCount = GameWorld.Player().wash_cur_day_count or 0
	local leftFreeTimesCount = math.max(0,self._freeWashTimeMax - dayCount)
	if leftFreeTimesCount > 0 then
		state = true
	else
		local itemList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
		local moneyCount = GameWorld.Player().money_coupons + GameWorld.Player().money_coupons_bind
		local minCost = self._washOpenSlotCost[2]
		if minCost < moneyCount then
			local washInfo = GameWorld.Player().equip_wash_info
			for equipPos,info in pairs(washInfo) do
				if itemList[equipPos] then
					local slotCount = info[public_config.EQUIP_WASH_INFO_SLOT_COUNT] or 0
					if slotCount > 0 and slotCount <4 and self._washOpenSlotCost[slotCount+1] <= moneyCount then
						state = true
						break
					end
				end
			end
		end
	end

	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_EQUIP_WASH, state)
end

--判断是否更好装备
function EquipManager:CheckBetterEquip(pkgType, pos)
	local itemData = bagData:GetItem(pkgType,pos)
	if itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP and 
		itemData:CheckCanLoaded() and itemData:CheckBetterThanLoaded() and pkgType == public_config.PKG_TYPE_ITEM then
		GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
			GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddEquipData(itemData)
		end)
	end
end

function EquipManager:HandleResp(act_id, code, args)
	if code == 0 then
		--装备穿戴成功
		if act_id == action_config.EQUIP_LOAD then
			self:ResponseEquipLoad(args)
		elseif act_id == action_config.EQUIP_STREN then
			self:OnStrengthChange(args)
		elseif act_id == action_config.EQUIP_WASH then
		elseif act_id == action_config.EQUIP_GEM_REFINE then
			self:OnGemRefineChange(args)
			--EventDispatcher:TriggerEvent(GameEvents.EQUIP_WASH_SUCCESS)
		elseif act_id == action_config.EQUIP_GEM_LOAD then
		elseif act_id == action_config.EQUIP_GEM_UNLOAD then
			
		elseif act_id == action_config.EQUIP_ADVANCE_REQ then
			self:OnEquipAdvance(args)
		end
	else
		if act_id == action_config.EQUIP_STREN then
			self:OnStrengthFail()
		end
	end
end

function EquipManager:OnUpdateStrength(change)
	self:UpdateFightPower(change)
	if change then
		--强化红点
		for pos,v in pairs(change) do
			self:RefreshStrengthRedPointState(pos)
		end
	end
	
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_STRENGTH_UPDATE,change)
end

--处理强化经验变化
function EquipManager:OnStrengthChange(args)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_STRENGTH_EXP_CHANGE,args)
end

--强化失败的情况增加容错
function EquipManager:OnStrengthFail()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_STRENGTH_FAIL)
end

function EquipManager:OnUpdateWash(change)
	self:UpdateFightPower(change)
	if change then
		self:RefreshWashRedPointState()
	end
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_WASH_UPDATE,change)
end

--更新宝石信息
function EquipManager:OnUpdateGem(change)
	self:UpdateFightPower(change)
	if change then
		--宝石红点
		for pos,v in pairs(change) do
			self:RefreshGemInlayRedPointState(pos)
		end
	end
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_GEM_UPDATE,change)
end

--处理精炼经验变化
function EquipManager:OnGemRefineChange(args)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_GEM_REFINE_EXP_CHANGE,args)
end

--进阶成功
function EquipManager:OnEquipAdvance(args)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_ADVANCE_UPDATE,args)
end

--套装改变
function EquipManager:OnUpdateSuit(change)
	if change then
		--套装红点
		for pos,v in pairs(change) do
			self:RefreshSuitRedPointState(pos)
		end
	end
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_EQUIP_SUIT_UPDATE,change)
end

function EquipManager:UpdateFightPower(change)
	if change then
		for pos,v in pairs(change) do
			local equipdata = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(pos)
			local loadedEquipData = equipdata[1]
			if loadedEquipData then
				loadedEquipData:SetFightPowerChange(true)
			end
		end
	end
end

function EquipManager:ResponseEquipLoad(args)
	local equipId = args[1]
	local cfg = ItemDataHelper.GetItem(equipId)
	--主手、衣服要换装
	if cfg.type == 1 and (not PlayerManager.FashionManager:IsFashionWeaponLoaded()) and (GameWorld.Player().weapon_show == 0) then
		local model = cfg.mode or 0
		if model > 0 then
			GameWorld.Player():SetCurWeapon(model,0,0,0,0,0)
		else
			GameWorld.Player():ShowDefaultWeapon()
		end
	elseif cfg.type == 3 and (not PlayerManager.FashionManager:IsFashionClothLoaded()) and (GameWorld.Player().weapon_show == 0) then
		local model = cfg.mode or 0
		if model > 0 then
			GameWorld.Player():SetCurCloth(model,0,0,0,0,0)
		else
			GameWorld.Player():ShowDefaultClothAndHead()
		end
	end
	
	EventDispatcher:TriggerEvent(GameEvents.ON_EQUIP_SUCCESS)

	--更新战力
	local equipdata = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(cfg.type)
	local loadedEquipData = equipdata[1]
	if loadedEquipData then
		loadedEquipData:SetFightPowerChange(true)
	end
end

--------------装备操作请求------------------------
--穿戴装备
function EquipManager:PutOnEquip(bagPos)
	GameWorld.Player().server.equip_load_req(bagPos)
end

--卸下装备
function EquipManager:UnloadEquip(bagPos)
	GameWorld.Player().server.equip_unload_req(bagPos)
end

--镶嵌宝石
function EquipManager:RequestGemLoad(equip_pos,slot, gem_bag_pos)
	local str = equip_pos..","..slot..","..gem_bag_pos
	GameWorld.Player().server.equip_gem_action_req(action_config.EQUIP_GEM_LOAD, str)
end

--卸下宝石
function EquipManager:RequestGemUnload(equip_pos, slot)
	local str = equip_pos..","..slot
	GameWorld.Player().server.equip_gem_action_req(action_config.EQUIP_GEM_UNLOAD, str)
end

--精炼宝石
function EquipManager:RequestGemRefine(equip_pos, item_pos)
	local str = equip_pos..","..item_pos
	--LoggerHelper.Error("EquipManager:RequestGemRefine"..str)
	GameWorld.Player().server.equip_gem_action_req(action_config.EQUIP_GEM_REFINE, str)
end

-- --合成魔石
-- function EquipManager:ReqestGemCombine(gemId)
-- 	GameWorld.Player().server.gem_compound_req(gemId)
-- end

--强化
function EquipManager:RequestStrength(pos)
	GameWorld.Player().server.equip_stren_action_req(action_config.EQUIP_STREN,tostring(pos))
end

--洗炼
function EquipManager:RequestWash(args)
	GameWorld.Player().server.equip_wash_action_req(action_config.EQUIP_WASH,args)
end

--洗炼开孔
function EquipManager:RequestWashOpenSlot(pos)
	GameWorld.Player().server.equip_wash_action_req(action_config.EQUIP_WASH_OPEN_SLOT,tostring(pos))
end

--装备进阶
function EquipManager:RequestUpgrade(pos)
	GameWorld.Player().server.equip_advance_action_req(action_config.EQUIP_ADVANCE_REQ,tostring(pos))
end

--装备打造套装
function EquipManager:RequestMakeSuit(pos)
	GameWorld.Player().server.equip_suit_action_req(action_config.EQUIP_SUIT_UPLEVEL_REQ,tostring(pos))
end

--------------------------------------时装---------------------------------------------
--穿时装
function EquipManager:RequestLoadFashion(bagPos)
	GameWorld.Player().server.fashion_load_req(bagPos)
end

--脱时装
function EquipManager:RequestUnloadFashion(bagPos)
	GameWorld.Player().server.fashion_unload_req(bagPos)
end

--------------------------------------守护---------------------------------------------
--守护续费
function EquipManager:RequestGuardBuy(bagType,bagPos)
	local str = bagType..","..bagPos
	GameWorld.Player().server.equip_guard_action_req(action_config.EQUIP_GUARD_BUY,str)
end

--小天使续费查看
function EquipManager:AngelRenew()
		local equipPlayerGuard = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(public_config.PKG_LOADED_EQUIP_INDEX_GUARD)
		local equipBagGuard = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetEquipListBySubType(public_config.PKG_LOADED_EQUIP_INDEX_GUARD)
		if equipPlayerGuard[1] then
		elseif equipBagGuard then
			for i,equipGuardItem in ipairs(equipBagGuard) do
				local timeEnd = equipGuardItem:GetTimeEnd()
				local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
				if timeLeft <= 0 then	
					--self._guardTimeOut = true
					GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
						GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddGuardData(equipGuardItem)
					end)

					--local itemData = bagData:GetItem(pkgType,pos)
					return
				else
					-- self._guardTimeOut = false
					-- GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
					-- 	GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddEquipData(equipGuardItem)
					-- end)
				end
			end
		end
end

-----------------------------------------这部分要迁移----------------------------------------------
---------------------------------------跟装备无关的逻辑----------------------------------------------
--物品增加弹窗
function EquipManager:CheckItemADD(pkgType, pos)
	local level = GameWorld.Player().level
	local itemData = bagData:GetItem(pkgType,pos)
	if  ItemDataHelper.GetQuickUse(itemData:GetItemID()) and  pkgType == public_config.PKG_TYPE_ITEM and level >= ItemDataHelper.GetLevelNeed(itemData:GetItemID()) then
		GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
			GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddItemUseData(itemData)
		end)
	end
end



--离线挂机处理
function EquipManager:OffineTimeGuide()
    local offineList = GlobalParamsHelper.GetParamValue(855)
    for i,v in ipairs(offineList) do
        if bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(v[2]) then
            local pkgPos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(v[2])
            local data = bagData:GetItem(public_config.PKG_TYPE_ITEM, pkgPos)
            if data:GetCount()>=1 then
                self:OnBulkUse(data)
            end
            return
        else

        end
	end
	
	self:BuyGuideTip(137)
end


function EquipManager:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end

--检测挂机是否小于某个值
function EquipManager:CheckOffineTime()
	if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DAILY_ACTIVITY) then
		return
	end

	self._dailyOpen = function() GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_DAILY_ACTIVITY) end
    if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DAILY_ACTIVITY) then
		if GameWorld.Player().left_hang_up_time < GlobalParamsHelper.GetParamValue(873)  then
			--LoggerHelper.Error("lefttime"..GameWorld.Player().left_hang_up_time)
		GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
				GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddOffineData(128,self._dailyOpen)
			end)
		else

		end
	end
end


function EquipManager:OnBulkUse(data)
    local itemData = {["id"] = MessageBoxType.BulkUse, ["value"] = data}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, itemData, nil)
    GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

EquipManager:Init()

return EquipManager
