--CollectManager.lua
--采集/客户端采集管理器
--采集物
local CollectItemData = Class.CollectItemData(ClassTypes.XObject)
function CollectItemData:__ctor__(itemEid,collectCfg)
	self.itemEid = itemEid
	self.collectCfg = collectCfg
end

--采集物采集动作数据
local CollectActionData = Class.CollectActionData(ClassTypes.XObject)
function CollectActionData:__ctor__(actorId,actionId,actionType,delay,duration)
	self.actorId = actorId
	self.actionId = actionId
	self.actionType = actionType
	self.delay = delay
	self.duration = duration
end

local AutoFightManager = PlayerManager.AutoFightManager
local GameSceneManager = GameManager.GameSceneManager
local PartnerManager = PlayerManager.PartnerManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local action_config = GameWorld.action_config

CollectManager = {}

function CollectManager:Init()
	--self._spaceDropList = {} --掉落列表{ownDropId}
	self._collectList = {}--采集列表{CollectItemData}
	self._clientCollectList = {}--客户端采集列表{ClientCollectItemData}

	self._autoCollect = false
end

function CollectManager:HandleData(action_id, error_code, args)
	if error_code > 0 then
		return
	end
	if action_id == action_config.COLLECT_REQ then
		self:CompleteCollect(args)
	elseif action_id == action_config.COLLECT_PRE_REQ then
		self:ConfirmStartCollect()
	end
end
------------------------------------------拾取相关-----------------------------------------------------------
--拾取
function CollectManager:PickSpaceDropImmediately(ownDropId,index)
	GameWorld.Player().server.pick_drop_item(ownDropId,index)
end

------------------------------------------采集相关-----------------------------------------------------------
function CollectManager:AddCollectItem(itemEid,collectCfg)
	local cid = CollectItemData(itemEid,collectCfg)
	table.insert(self._collectList,cid)
	if #self._collectList == 1 then
		self._curCollectItemData = cid
		GUIManager.ShowPanel(PanelsConfig.Collect,nil,function ()
			EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_ADD_MODE,1)
		end)
	end
end

function CollectManager:RemoveCollectItem(itemEid)
	local t = self._collectList
	for i=#t, 1, -1 do 
        if t[i].itemEid == itemEid then 
            table.remove(t,i) 
        end 
    end 
    if #t == 0 then
    	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_REMOVE_MODE,1)
    else
    	self._curCollectItemData = self._collectList[1]
    end
    --LoggerHelper.Error("RemoveCollectItem"..#t)
end

function CollectManager:GetCurCollectItemData()
	return self._curCollectItemData
end

--采集吟唱开始，后端确认是否可以
function CollectManager:StartCollectItem()
	if self._curCollectItemData then
		local itemEid = self._curCollectItemData.itemEid
		GameWorld.Player().server.collect_pre_req(itemEid)
	end
end

--后端确认可以采集
function CollectManager:ConfirmStartCollect()
	local panel = GUIManager.GetPanel(PanelsConfig.Collect)
	if panel then
		panel:ConfirmStartCollect()
		AutoFightManager:StickBeginDrag()
	end
end

--采集吟唱完成
function CollectManager:CollectItem()
	if self._curCollectItemData then
		local itemEid = self._curCollectItemData.itemEid
		AutoFightManager:StickEndDrag()
		--采集结束时被采集实体播的技能
		local finishSkill = self._curCollectItemData.collectCfg.skill_time
		if finishSkill then
			local skillId = finishSkill[1]
			local delay = finishSkill[2]
			local collectEntity = GameWorld.GetEntity(itemEid)
			if collectEntity then
				collectEntity:PlaySkillByCreateRoleAvatar(skillId)
			end
			self._collectFinishSkillTimer = TimerHeap:AddSecTimer(delay,0,0,function ()
				self:RequestCollectItem(itemEid)
				TimerHeap:DelTimer(self._collectFinishSkillTimer)
			end)
		else
			self:RequestCollectItem(itemEid)
		end
	end
end

--采集吟唱完成发送请求
function CollectManager:RequestCollectItem(itemEid)
	GameWorld.Player().server.collect_req(itemEid)
end

--采集中断
function CollectManager:StopCollect()
	if self._curCollectItemData then
		local itemEid = self._curCollectItemData.itemEid
		--LoggerHelper.Error("CollectManager:StopCollect()"..itemEid)
		GameWorld.Player().server.collect_break_req(itemEid)
		local panel = GUIManager.GetPanel(PanelsConfig.Collect)
		if panel then
			panel:StopCollect()
			AutoFightManager:StickEndDrag()
		end
	end
end

function CollectManager:CompleteCollect(args)
	local collectId = args[1]
	local itemLeft = args[2]
	local panel = GUIManager.GetPanel(PanelsConfig.Collect)
	if panel then
		panel:CompleteCollect(collectId,itemLeft)
	end
end

-------------------------------------------通用采集相关-------------------------------------------------
--只显示倒计时并执行回调的采集按钮
--
local CommonCollectData = Class.CommonCollectData(ClassTypes.XObject)
function CommonCollectData:__ctor__(entityId,cdTime,collectName,collectIcon,startFun,endFun,interruptFun)
	self.entityId = entityId
	self.cdTime = cdTime
	self.collectName = collectName
	self.collectIcon = collectIcon
	self.startFun = startFun
	self.endFun = endFun
	self.interruptFun = interruptFun
end

--
function CollectManager:SetCommonCollectItem(entityId,cdTime,collectName,collectIcon,startFun,endFun,interruptFun)
	self._curCommonCollectItemData = CommonCollectData(entityId,cdTime,collectName,collectIcon,startFun,endFun,interruptFun)
	GUIManager.ShowPanel(PanelsConfig.Collect,nil,function ()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_ADD_MODE,3)
	end)
end

function CollectManager:RemoveCommonCollectItem(entityId)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_REMOVE_MODE,3)
    
    --LoggerHelper.Error("RemoveCollectItem"..#t)
end

function CollectManager:GetCurCommonCollectData()
	return self._curCommonCollectItemData
end

--吟唱开始
function CollectManager:StartCommonCollect()
	if self._curCommonCollectItemData then
		if self._curCommonCollectItemData.startFun then
			self._curCommonCollectItemData.startFun()
		end
		AutoFightManager:StickBeginDrag()
	end
end

--吟唱完成
function CollectManager:CompleteCommonCollect()
	if self._curCommonCollectItemData then
		if self._curCommonCollectItemData.endFun then
			self._curCommonCollectItemData.endFun()
		end
		AutoFightManager:StickEndDrag()
	end
end

--吟唱中断
function CollectManager:StopCommonCollect()
	if self._curCommonCollectItemData then
		if self._curCommonCollectItemData.interruptFun then
			self._curCommonCollectItemData.interruptFun()
		end
		local panel = GUIManager.GetPanel(PanelsConfig.Collect)
		if panel then
			panel:StopOpenCommonCollect()
			AutoFightManager:StickEndDrag()
		end
	end
end

------------------------------------------客户端采集相关-----------------------------------------------------------
--客户端采集物
local ClientCollectItemData = Class.ClientCollectItemData(ClassTypes.XObject)
function ClientCollectItemData:__ctor__(entityId,entityCfgId,cdTime,eventId,destroyAfterCollect,collectIcon,collectName,collectActionArgs)
	self.entityId = entityId
	self.entityCfgId = entityCfgId
	self.cdTime = cdTime
	self.eventId = eventId
	self.destroyAfterCollect = destroyAfterCollect
	self.collectIcon = collectIcon
	self.collectName = collectName
	self.collectActionArgs = {}
	-- LoggerHelper.Log("ClientCollectItemData:__ctor__")
	-- LoggerHelper.Log(collectActionArgs)
	for k,v in pairs(collectActionArgs) do
		--LoggerHelper.Log(v[1].."/"..v[2].."/"..v[3].."/"..v[4])
		if k >= 0 then
			local cca = CollectActionData(v[1],v[2],v[3],v[4],v[5])
			table.insert(self.collectActionArgs,cca)
		end
	end
end


--获取某个触发时机的动作
function ClientCollectItemData:GetActionDataByType(actionType)
	local result = {}
	for i=1,#self.collectActionArgs do
		local data = self.collectActionArgs[i]
		if data.actionType == actionType then
			table.insert(result,data)
		end
	end
	return result
end
--entityId： 实体id
--entityCfgId：实体配置id
function CollectManager:AddClientCollectEntity(entityId,entityCfgId,cdTime,eventId,destroyAfterCollect,collectIcon,collectName,collectActionArgs)
	local cid = ClientCollectItemData(entityId,entityCfgId,cdTime,eventId,destroyAfterCollect,collectIcon,collectName,collectActionArgs)
	table.insert(self._clientCollectList,cid)
	if #self._clientCollectList == 1 then
		self._curClientCollectEntityData = cid
		GUIManager.ShowPanel(PanelsConfig.Collect,nil,function ()
			EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_ADD_MODE,2)
		end)
	end
end

function CollectManager:RemoveClientCollectEntity(entityCfgId)
	local t = self._clientCollectList
	for i=#t, 1, -1 do 
        if t[i].entityCfgId == entityCfgId then 
            table.remove(t,i) 
        end 
    end 
    if #t == 0 then
    	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_REMOVE_MODE,2)
    else
    	self._curClientCollectEntityData = self._clientCollectList[1]
    end
    --LoggerHelper.Error("RemoveCollectItem"..#t)
end

function CollectManager:GetCurClientCollectEntityData()
	return self._curClientCollectEntityData
end

--
function CollectManager:GetCurClientCollectState()
	return self._clientCollectState
end

--客户端采集吟唱完成
function CollectManager:CollectClientEntity()
	--LoggerHelper.Error("CollectManager:CollectClientEntity")
	local ccid = self._curClientCollectEntityData
	if ccid then
		self._clientCollectState = false
		--LoggerHelper.Error("CollectClientEntity"..ccid.entityCfgId.."/"..ccid.eventId)
		EventDispatcher:TriggerEvent(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE,ccid.entityCfgId,ccid.eventId)
		if ccid.destroyAfterCollect > 0 then
			--LoggerHelper.Error("destroyAfterCollect"..ccid.entityCfgId)
			self:RemoveClientCollectEntity(ccid.entityCfgId)
			GameWorld.DestroyEntity(ccid.entityId)
			self:CollectClientCompleteAction()
			AutoFightManager:StickEndDrag()
		end
	end
end

--客户端采集中断
function CollectManager:StopCollectClientEntity()
	local panel = GUIManager.GetPanel(PanelsConfig.Collect)
	self._clientCollectState = false
	if panel then
		panel:StopCollectClientEntity()
		AutoFightManager:StickEndDrag()
	end
end

--客户端采集动作相关
--客户端采集准备动作处理
function CollectManager:CollectClientPrepareAction()
	self:CollectClientDoAction(1)
end

--客户端采集开始动作处理
function CollectManager:CollectClientStartAction()
	self._clientCollectState = true
	self:CollectClientDoAction(2)
	AutoFightManager:StickBeginDrag()
end

--客户端采集结束动作处理
function CollectManager:CollectClientCompleteAction()
	self:CollectClientDoAction(3)
end

--执行动作播放，先设定延时
function CollectManager:CollectClientDoAction(actionType)
	local ccid = self._curClientCollectEntityData
	if ccid then
		local actionList = ccid:GetActionDataByType(actionType)
		self._actionDelayTimerIds = {}
		self._actionDurationTimerIds = {}
		for i=1,#actionList do
			local actionData = actionList[i]
			self._actionDurationTimerIds[i] = TimerHeap:AddSecTimer(actionData.delay,0,0,function ()
				self:CollectClientOnAction(actionData,i)
				TimerHeap:DelTimer(self._actionDelayTimerIds[i])
			end)
		end
	end
end

--进入动作播放
function CollectManager:CollectClientOnAction(actionData,index)
	local canRide = GameSceneManager:CanRide()
	if actionData.actorId == 0 then
		if canRide then
			PartnerManager:RideHideReq()
		end
		GameWorld.Player():PlayAction(actionData.actionId)
	else
		local npcs = GameSceneManager:GetNPCs()
		for i, npc in pairs(npcs) do
	        -- LoggerHelper.Log("Ash: GetTaskNPC for npc.npc_id:" .. npc.npc_id)
	        if npc.npc_id == actionData.actorId then
	        	npc:PlayAction(actionData.actionId)
	        end
	    end
	end
	--LoggerHelper.Error("CollectClientOnAction")
	self._actionDurationTimerIds[index] = TimerHeap:AddSecTimer(actionData.duration,0,0,function ()
		if actionData.actorId == 0 then
			--LoggerHelper.Error("CollectClientOnAction")
			--GameWorld.Player():ReadyToIdle()
			--GameWorld.Player():IdleToReady()
			if canRide then
				PartnerManager:HorseRideReq()
			end
			GameWorld.Player():PlayAction(101)
		else
			for i, npc in pairs(npcs) do
		        if npc.npc_id == actionData.actorId then
		        	npc:ReadyToIdle()
		        end
		    end
		end
		TimerHeap:DelTimer(self._actionDurationTimerIds[index])
	end)
end

CollectManager:Init()
return CollectManager