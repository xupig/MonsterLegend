--GuideManager.lua
GuideManager = {}
GuideManager.ShowTextAction = nil
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local SceneConfig = GameConfig.SceneConfig
local public_config = GameWorld.public_config
local LocalSetting = GameConfig.LocalSetting
local TimerHeap = GameWorld.TimerHeap


local hasCompleteGuideIds = {}
local timer = nil
local isCGDataArrive = false
local RedDotStateList = {}
local showPanelTimer = 0

function GuideManager.Init()

end

function GuideManager.OnPlayerEnterWorld()
	GuideManager.InitEvent()
end

function GuideManager.OnPlayerLeaveWorld()
	GuideManager.RemoveEvent()
end

function GuideManager.InitEvent()
	EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, GuideManager.OnEnterMap)
	EventDispatcher:AddEventListener(GameEvents.ON_GUIDE_DATA_CHANGE,GuideManager.OnGuideData)
end

function GuideManager.RemoveEvent()
	EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, GuideManager.OnEnterMap)
	EventDispatcher:RemoveEventListener(GameEvents.ON_GUIDE_DATA_CHANGE,GuideManager.OnGuideData)
end

-- 切完场景
function GuideManager.OnEnterMap(mapId)

end

function GuideManager.ShowText(text)
	if GuideManager.ShowTextAction ~= nil then
		GuideManager.ShowTextAction(text)
	end
end

function GuideManager.PlayerLevelUp()
	local mapId = GameManager.GameSceneManager:GetCurSceneType()
	GuideManager.BackHomeAndGuarder(mapId)
end

function GuideManager.HidePanel()
	GUIManager.ClosePanel(PanelsConfig.GuideFunctionOpening)
end

function GuideManager.ClosePanel()
	local mapId = GameManager.GameSceneManager:GetCurSceneType()
	GuideManager.BackHomeAndGuarder(mapId)
end

-- 显示功能开启面板
function GuideManager.ShowOpenFunctionPanel()
	local data = GameDataHelper.FunctionOpenDataHelper.GetOpenFunctionData()
	if data ~= nil then
		GuideManager.SetCGComplete(data.open_function_id)
		GUIManager.ShowPanel(PanelsConfig.GuideFunctionOpening, data)
	end
end

-- 当前有主线任务可进行
function GuideManager.HandlePlot()

end

function GuideManager.GetCGTriggerCount(guideId)
	if GameWorld.Player() == nil then
		return 0
	end

	local data = GameWorld.Player().guide_data
	-- hasCompleteGuideIds = data
	if data ~= nil and data[guideId] ~=nil then
		return data[guideId]
	end
	return 0
end

-- <!--设置已做过的指引-->
function GuideManager.SetCGComplete(guideId)
	local count = GuideManager.GetCGTriggerCount(guideId) + 1  --递增1
	if GameWorld.Player() ~= nil then
		GameWorld.Player().server.set_guide_data(guideId, count)
	end
end

-- <!-- 已做过的指引 -->
function GuideManager.OnGuideData(guideData)
	hasCompleteGuideIds = guideData or {}
	isCGDataArrive = true
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_CENTER_BUTTONS)
end

-- 当前完成的指引CG
function GuideManager.GetCurGuideCG()
	return hasCompleteGuideIds or {}
end


local GMShield = false
function GuideManager.GetShield()
	return GMShield
end

function GuideManager.SetShield()
	GMShield = not(GMShield)
end


local IsShowGuide = false
function GuideManager.ShowGuide(value)
	IsShowGuide = value
end

function GuideManager.GetIsShowGuide()
	return IsShowGuide
end

function GuideManager.UpdateRedDotList(id,state)
	RedDotStateList[id] = state
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_REDDOT_VIEW)
end

function GuideManager.GetRedDotStateById(id)
	if RedDotStateList[id] ~= nil then
		return RedDotStateList[id]
	end
	return false
end

return GuideManager