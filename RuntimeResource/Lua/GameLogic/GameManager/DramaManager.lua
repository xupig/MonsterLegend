--DramaManager.lua
require "GameManager/DramaSystem/DramaWaitingEvent"
require "GameManager/DramaSystem/Drama"
require "GameManager/DramaSystem/DramaDataPool"
require "GameManager/DramaSystem/DramaJudgment"
require "GameManager/DramaSystem/DramaTrigger"
require "GameManager/DramaSystem/DramaTriggerType"
require "GameManager/DramaSystem/DramaExAction"
require "GameManager/DramaSystem/DramaTriggerData"
require "GameManager/DramaSystem/DramaResource"

DramaManager = {}

DramaManager.UpdateFunc = {}
DramaManager._isBlockingUpdate = false
DramaManager._currentShowOtherEntities = true
DramaManager._triggerCountDict = {}
DramaManager._drama = {}
DramaManager._dramaNameList = {}

local SystemSettingConfig = GameConfig.SystemSettingConfig
local GameObject = UnityEngine.GameObject
local FileAccessManager = GameLoader.IO.FileAccessManager
local XMLParser = GameWorld.XMLParser
local Drama = ClassTypes.Drama

function DramaManager:Init()
    GameWorld.Update:Add(
        function()
            self:Update()
        end
    )
    DramaTriggerData:InitData()
    DramaTrigger:Start()
    self:AddEventListeners()
end

function DramaManager:AddEventListeners()
    self._onLeaveMap = function(mapId) self:OnLeaveMap(mapId) end
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.BLACK_SKIP_CG, function()
        for k, v in pairs(self._dramaNameList) do
            self:SkipDrama(v)
        end
    end)
end

function DramaManager:Update()
    if self.UpdateFunc == nil then return end
    for i,v in pairs(self.UpdateFunc) do
        v()
    end
end

function DramaManager:AddListener(func)
    if func == nil then return end
    table.insert(self.UpdateFunc,func)
end

function DramaManager:RemoveListener(func)
    for k,v in pairs(self.UpdateFunc) do
        if v == func then 
            table.remove(self.UpdateFunc,k) break
        end
    end
end

function DramaManager:StoreTriggerCount(triggerId)
    self:AddTiggerCount(triggerId)
    if self._triggerCountDict[triggerId] ~= nil then
        --wait for code
    end
end

function DramaManager:AddTiggerCount(triggerId)
    if self._triggerCountDict[triggerId] ~= nil then 
        self._triggerCountDict[triggerId] = self._triggerCountDict[triggerId] + 1
    else
        self._triggerCountDict[triggerId] = 1
    end
end

function DramaManager:IsRunning(drama,mark)
    local key = drama..mark
    if self._drama[key] ~= nil and self._drama[key]:IsRunning() then
        return true
    end
    return false
end

function DramaManager:IsInViewingMode()
    --wait for code
    return false
end

function DramaManager:GetTriggerCount(triggerId)
    if self._triggerCountDict[triggerId] ~= nil then
        return self._triggerCountDict[triggerId]
    end
    self._triggerCountDict[triggerId] = 0
    return 0
end

function DramaManager:SkipDrama(dramaKey)
    if self._drama[dramaKey] == nil then
        LoggerHelper.Error("跳过剧情失败，当前没有剧情["..dramaKey.."]在播放")
        return
    end
    self._drama[dramaKey]:SkipDrama()
end


function DramaManager:RunDrama(id,dramaName,dramaMark)
    if GameWorld.SpecialExamine then
        EventDispatcher:TriggerEvent(GameEvents.DramaPlayEnd, nil, nil, id)
        return
    end

    self:ClearDramas()
    --只播一个
    if self:HasDramaRunning() then
        return
    end
    local dramaKey = dramaName..dramaMark
    if self._drama[dramaKey] == nil then
        local newDrama = Drama(id,dramaName)
        self._drama[dramaKey] = newDrama
        table.insert(self._dramaNameList,dramaKey)
        self:DoBeforeStartDrama(newDrama)
        newDrama:Start(dramaMark)
    else
        LoggerHelper.Error("播放剧情失败。当前已有相同的名称为["..dramaName.."] mark为["..dramaMark.."]的剧情正在播放。")
    end
end

function DramaManager:RunDramaByName(dramaName)
    self:RunDrama(-1, dramaName, "")
end

function DramaManager:StopDramaByName(dramaName)
    self:SkipDrama(dramaName)
end

function DramaManager:Trigger(eventName,eventValue)
    self:TriggerWaitEvent(eventName,eventValue)
    local triggers = DramaTriggerData:GetDramaTriggers(eventName,eventValue)
    if triggers == nil or #triggers == 0 then return false end
    for i = 1,#triggers do
        local triggerData = triggers[i]
        self:RunDrama(triggerData[1],triggerData[4],triggerData[5])
    end
    return true
end

function DramaManager:DramaOnEnd(drama)
    self:DoAfterEndDrama(drama)
    for i,v in pairs(self._dramaNameList) do
        if drama._dramaKey == v then
            table.remove(self._dramaNameList,i)
        end
    end
    self._drama[drama._dramaKey] = nil
    DramaManager:Trigger(DramaTriggerType.CGEnd, drama._dramaKey)
end

function DramaManager:ClearDramas()
    local dramaName = "" 
    for i = #self._dramaNameList, 1,-1 do
        dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:CanRemove() then
            table.remove(self._dramaNameList,i)
            self._drama[dramaName] = nil
        end
    end
end

function DramaManager:DoBeforeStartDrama(drama)
    drama:RecordCameraInfo()
    EventDispatcher:TriggerEvent(GameEvents.BEFORE_CG_START)
    GameManager.GUIManager.SetInCGCameraCulling(true)
end

function DramaManager:DoAfterEndDrama(drama)
    --wait for code
    EventDispatcher:TriggerEvent(GameEvents.TASK_CG_END)
    GameManager.GUIManager.SetInCGCameraCulling(false)
    if GameWorld.Player() then
        GameWorld.Player():EnableDramaSkill(0)
        GameWorld.Player().cs.actorVisible = true
    end
    GameWorld.SetHideOtherPlayer(0)
    local value = PlayerManager.SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER)
    PlayerManager.SystemSettingManager:SetMonsterHide(value)
end

function DramaManager:TriggerWaitEvent(eventName,eventValue)
    local dramaName = ""
    for i = #self._dramaNameList,1,-1 do
        dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:IsRunning() then
            self._drama[dramaName]:TriggerWaitEvent(eventName,eventValue)
        end
    end
end

function DramaManager:OnLeaveMap()
    do return end --不清除
    local dramaName = ""
    for i = #self._dramaNameList,1,-1 do
        dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:IsRunning() then
            self._drama[dramaName]:Stop()
        end
    end
end

function DramaManager:Reset()
    local dramaName = "" 
    for i = #self._dramaNameList, 1,-1 do
        dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:CanRemove() then
            table.remove(self._dramaNameList,i)
            self._drama[dramaName] = nil
        else
            self._drama[dramaName]:SkipDrama()
        end
    end
end

function DramaManager:HasDramaRunning()
    for i = #self._dramaNameList, 1,-1 do
        local dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:IsRunning() then
            return true
        end
    end
    return false
end

function DramaManager:GetRunningDrameName()
    for i = #self._dramaNameList, 1,-1 do
        local dramaName = self._dramaNameList[i]
        if self._drama[dramaName]:IsRunning() then
            return dramaName
        end
    end
    return nil
end

function DramaManager:SkipCurRunningDrama()
    local name = self:GetRunningDrameName()
    if name ~= nil then
        self:StopDramaByName(name)
    end
end

function DramaManager:Test()
    LoggerHelper.Error("gggggggggg")
end

function DramaManager:ClearUpdateFunc()
    self.UpdateFunc = {}
end

return DramaManager