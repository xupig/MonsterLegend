-- PreloadManager.lua
PreloadManager = {}

function PreloadManager:Init()

end

function PreloadManager:BeforeEnterScene(callback)
    local paths = {}
    GameWorld.PreloadAsset(paths, callback);
end

PreloadManager:Init()
return PreloadManager