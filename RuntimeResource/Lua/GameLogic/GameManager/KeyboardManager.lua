-- KeyboardManager.lua
local KeyboardManager = {}

local Input = UnityEngine.Input
local KeyCode = UnityEngine.KeyCode
local TestState = true 

function KeyboardManager:Init()
    --GameWorld.Update:Add(function() self:Update() end)
end

function KeyboardManager:Update()
    if Input.anyKeyDown then
        self:CheckSkillKey()
        self:CheckShortcut()
    elseif Input.anyKey then
        
    end
end

local state = false
function KeyboardManager:CheckSkillKey()
    if Input.GetKeyDown(KeyCode.C) then
        --GameMain.LuaFacade.TestLuaCall()
        --local entity = GameWorld.ShowPlayer()
        --entity:SyncShowPlayerFacade()
        --GameWorld.Player():SetCurWing(2011)
        --GameWorld.Player():SetActor(1005)
        --GameWorld.Player():AddBuff(65101, 100000)
        --GameWorld.Player():AddBuff(10005, 10000)
        --GameWorld.PlayVideo(2, false)
        --GameManager.GUIManager.ShowText(1,"46596")
        --GameManager.GUIManager.ShowText(6, 4891, 1)
        --GameManager.GUIManager.ShowPanel(PanelsConfig.GodsTemple)
        --GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.NPCDialog, 5)
        --LoggerHelper.Log(GameDataHelper.FashionDataHelper.GetFashionShowData(1))
        --local data = {}
        --GameManager.GUIManager.ShowPanel(PanelsConfig.DailyCharge)
        --GameWorld.FaceToNPC(4, 3, 1)
        --GameWorld.Player():PlaySkill(71)
        --GameManager.DamageManager:CreateDamageNumber(GameWorld.Player():GetPosition(), -417, 10, true)
        --GameWorld.Player():PlayAction(122)
        --GameWorld.Player():AddBuff(4044, 0)
        --GameWorld.Player():SetRide(2701,1)
        --GameManager.GUIManager.ShowPanel(PanelsConfig.SkillOpen,{id=1,slot=2})
        --GameManager.GUIManager.ShowPanel(PanelsConfig.SkillOpen,{id=14001,slot=0})
        --GameManager.GUIManager.ShowPanel(PanelsConfig.SkillOpen,{id=14002,slot=0})
        --GameMain.ServerProxyFacade.Disconnect()
        --Debugger.LogError(GameWorld.Player().vip_level)
        --GameManager.GUIManager.ShowPanel(PanelsConfig.EscortPeriSettle)
        --PlayerManager.EscortPeriManager:GotoAcceptEscort()
        --GameManager.GUIManager.ShowPanel(PanelsConfig.GuardGoddnessSpace)
        --GameWorld.Player()._stateManager:SetState(23, true)
        --GameWorld.Player():SetActor(1302)
        --GameManager.GUIManager.ShowPanel(PanelsConfig.GuideArrow,{"Mask,LayerUIMain,/Panel_ChatSmall/Button_Bag,77107,22,0,Right,0,0,0,0,1.5"})
    end
    if Input.GetKeyDown(KeyCode.B) then
        --local entity = GameWorld.Player()
        --GameWorld.Player():SetActor(1106)
        --GameWorld.ShowPlayer():SetCurWing(2011)
        --GameWorld.Player():AddBuff(65102)
        --GameWorld.PlayVideo(1, true)
        --local data = {text = "msg", FadeIn = true, FadeInTime = 2}
        --GameManager.GUIManager.ShowPanel(PanelsConfig.Shady, data)
        --GameWorld.FaceToNPC(5, 3, 1)
        --GameWorld.Player().cs:PlaySkill(43)
        --PlayerManager.EscortPeriManager:CommitEscort()
        --.Player()._stateManager:SetState(23, false)
        --GameWorld.Player():SetActor(101
        --GameManager.GUIManager.ShowPanel(GameConfig.PanelsConfig.FirstCharge)
        --GameWorld.Player():DeleteBuff(65106)
    end
end

KeyboardManager.ShowSeleteServer = false -- 是否开选服界面
-- 通过键盘决定是否开启功能
function KeyboardManager:CheckShortcut()
    if Input.GetKeyDown(KeyCode.F12) then
        GameWorld.ReloadXml()
        GameManager.XMLManager.CheckReloadXML()
    elseif Input.GetKeyDown(KeyCode.Q) then
        self.ShowSeleteServer = not(self.ShowSeleteServer)
        -- print('ShowSeleteServer:' .. tostring(self.ShowSeleteServer))
    elseif Input.GetKeyDown(KeyCode.Tab) then
        -- GameManager.GUIManager.ShowPanel(PanelsConfig.GMCommand)
    end
end

return KeyboardManager