--游戏内tips管理
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
require "PlayerManager/PlayerData/DragonSoulData"
local DragonSoulItemData = ClassTypes.DragonSoulItemData

local TipsManager = {}

function TipsManager:Init()
end

--道具数据显示tips
--buttonArgs,记录显示哪些按钮的table
--showCompare，是否显示对比tips
function TipsManager:ShowItemTips(itemData,buttonArgs,showCompare,showSuitPreview,otherPlayerArgs)
	local d = {}
	d[1] = 1
	d[2] = itemData
	d[3] = buttonArgs
	d[4] = showCompare
	d[5] = showSuitPreview
	d[6] = otherPlayerArgs
	GUIManager.ShowPanel(PanelsConfig.ItemTips, d)
end

--道具id显示tips
function TipsManager:ShowItemTipsById(itemId,buttonArgs)
	if ItemDataHelper.GetItemType(itemId) == public_config.ITEM_ITEMTYPE_ITEM
        and ItemDataHelper.GetType(itemId) == 13 then
        local dragonSoulItemData = DragonSoulItemData(itemId,1)
		self:ShowDragonSoulTips(dragonSoulItemData)
    else
    	local d = {}
		d[1] = 2
		d[2] = itemId
		d[3] = buttonArgs
    	GUIManager.ShowPanel(PanelsConfig.ItemTips, d)
    end
end

--技能tips
function TipsManager:ShowSkillTips(skillId,conditionLevel,treasureType,systemName)
	local data = {}
	data.tipsType = 1
	data.skillId = skillId
	data.conditionLevel = conditionLevel
	data.treasureType = treasureType
	data.systemName = systemName
	GUIManager.ShowPanel(PanelsConfig.Tips,data)
end

--龙魂tips
function TipsManager:ShowDragonSoulTips(dragonSoulItemData,buttonArgs,isBag,slot)
	local data = {}
	data.tipsType = 2
	data.dragonSoulItemData = dragonSoulItemData
	data.buttonArgs = buttonArgs
	data.isBag = isBag 
	data.slot = slot
	GUIManager.ShowPanel(PanelsConfig.Tips,data)
end

TipsManager:Init()
return TipsManager