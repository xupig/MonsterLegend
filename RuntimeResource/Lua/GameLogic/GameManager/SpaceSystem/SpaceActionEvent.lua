SpaceActionEvent = {}

local GameObject = UnityEngine.GameObject
-- local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
-- local StaticModelDataHelper = GameDataHelper.StaticModelDataHelper
-- local NPCDataHelper = GameDataHelper.NPCDataHelper
function SpaceActionEvent:Init()
end

function SpaceActionEvent:EnterMap(spaceData)
    self._spaceData = spaceData
    self._tempFXUid = {}
    self._isInServer = GameManager.GameSceneManager:InServerIns()
    self._boxActions = {}
    self._walls = {}
    self._triggers = {}
    --self._eventTimers = {}
    self._triggerAlert = {}
    self:CreateEntityAction()
end

--执行Action
function SpaceActionEvent:ExecuteAction(eventID)
    local actionCfg = self._spaceData:GetActionByID(eventID)
    if actionCfg == nil then
        LoggerHelper.Log("场景xml不存在该事件id：" .. eventID)
        return
    end
    
    local func = self[actionCfg.type]
    if func ~= nil then
        --LoggerHelper.Log("行为事件："..actionCfg.type.."("..eventID..")")
        func(self, actionCfg)
    else
        LoggerHelper.Log("该事件类型在代码里还没定义：" .. actionCfg.type)
    end
    
    EventDispatcher:TriggerEvent(GameEvents.SceneEvent, eventID)
end

function SpaceActionEvent:CleanSpace()
    if self._boxActions ~= nil then
        for k, v in pairs(self._boxActions) do
            GameWorld.Destroy(v.gameObject)
        end
    end
    
    if self._walls ~= nil then
        for k, v in pairs(self._walls) do
            GameWorld.Destroy(v.gameObject)
        end
    end
    
    if self._tempFXUid ~= nil then
        for k, v in pairs(self._tempFXUid) do
            for posID, uid in pairs(v) do
                GameWorld.StopFX(uid)
            end
        end
    end

--self:CleanSpawnBuff()
--self:CleanTimer()
end

--暂时没用
-- function SpaceActionEvent:CleanTimer()
--     for id, timer in pairs(self._eventTimers) do
--         TimerHeap:DelTimer(timer)
--     end
-- end
function SpaceActionEvent:CreateEntityAction()
    self._boxActions = {}
    local triggers = self._spaceData:GetTriggers()
    for key, trigger in pairs(triggers) do
        local name = trigger.name
        local lc_pos = trigger.pos
        local size = trigger.boxSize
        local rotation = trigger.rotation_pos
        local act = GameWorld.CreateSpaceBox("GameManager.SpaceSystem.EntityAction", name, lc_pos, size, rotation)
        act:SetData(trigger)
        self._boxActions[trigger.id] = act
        if trigger.type == "EntityTeleportData" or trigger.type == "TriggerRegion" or trigger.type == "EntityTeleportUnlock" or trigger.type == "EntityZoneUnlock" then
            --正式流程不再显示
            --act:ShowTestCube()
            end
        if trigger.type == "EntityTeleportData" then
            act:Enable(false)
        end
        self._triggers[key] = act
    end
    local blocks = self._spaceData:GetBlockActions()
    for k, block in pairs(blocks) do
        local name = block.name
        local lc_pos = block.pos
        local size = block.boxSize
        local rotation = block.rotation_pos
        local act = GameWorld.CreateSpaceBox("GameManager.SpaceSystem.EntityAction", name, lc_pos, size, rotation)
        act:SetData(block)
        self._boxActions[block.id] = act
    end
    local walls = self._spaceData:GetCameraWalls()
    for wk, wv in pairs(walls) do
        local name = wv.name
        local lc_pos = wv.pos
        local size = wv.boxSize
        local rotation = wv.rotation_posc
        local scale = wv.scale
        local act = GameWorld.CreateSpaceBox("GameManager.SpaceSystem.EntityAction", name, lc_pos, size, rotation, scale, false)
        act:SetData(wv)
        self._walls[wv.id] = act
    end
end

function SpaceActionEvent:ChangeTriggerState(eventId, isEnable)
    local trigger = self._triggers[eventId]
    if trigger == nil then
        do return end
    end
    trigger:Enable(isEnable)
end

------------------------------------------------以下开始各种事件---------------------------------------------------
local SceneConfig = GameConfig.SceneConfig
local MapDataHelper = GameDataHelper.MapDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local StateConfig = GameConfig.StateConfig
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EntityConfig = GameConfig.EntityConfig

function SpaceActionEvent:EntityResourcePoint(entity, cfg, enter)

end

function SpaceActionEvent:EntityTeleportData(cfg)
    -- body
    --GameManager.GameSceneManager:LeaveScene()
    cfg.proc_num = cfg.proc_num + 1
    -- EventDispatcher:TriggerEvent(GameEvents.ActionEvent, cfg.id)
    -- if GameWorld.TempConnected then
    if cfg.target_type == 1 then -- 传统副本入口（普通、英雄、噩梦）
        EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_ENTER, cfg.teleport_args, cfg.id)
    -- elseif cfg.target_type == 2 then -- 飞行移动
    --     -- if GameManager.GameSceneManager:InServerIns() then
    --     local targetScene = MapDataHelper.GetSpaceName(cfg.target_scene)
    --     --LoggerHelper.Log("Ash: EntityTeleportData cfg.target_scene:" .. cfg.target_scene .. " " .. targetScene)
    --     EventDispatcher:TriggerEvent(GameEvents.FLY_TO_SCENE, tostring(targetScene))
    --     GameWorld.Player().server.teleport_point_req(cfg.id)
    --     -- GameWorld.PauseAllSynPos(true) -- 机关不暂停同步坐标
    -- -- end
    else
        if cfg.target_scene == 0 then
            --target_scene为0表示本场景位置teleport
            EventDispatcher:TriggerEvent(GameEvents.TeleportPosition, cfg.target_scene, cfg.target_point_ids)
            if cfg.camera_args ~= nil and cfg.camera_times ~= nil then
                if GameWorld.Player() ~= nil then
                    local t = GameWorld.Player():GetCameraTransform()
                    local rotation = Vector3(cfg.camera_args[1], cfg.camera_args[2], cfg.camera_args[3])
                    local distance = cfg.camera_args[4]
                    local rate = cfg.camera_times
                    GameWorld.ChangeToRotationTargetMotion(t, rotation, rate[1] * 0.001, rate[2], distance, rate[3])
                end
            end
        end
        -- if GameManager.GameSceneManager:InServerIns() then
        GameWorld.Player().server.teleport_point_req(cfg.id)
        -- GameWorld.PauseAllSynPos(true) -- 机关不暂停同步坐标
        -- end
    end
-- end
end

function SpaceActionEvent:EntityBroadcast(cfg)

end

function SpaceActionEvent:EntityCameraMove(cfg)
    local lock_target = cfg.camera_type == 1 --1为跟随主角, 2为固定位置
    if cfg.camera_type == 1 then
        local distance = cfg.distance
        local rotation = Vector3(cfg.rotation[1], cfg.rotation[2], cfg.rotation[3])
        local rate = cfg.rotation_rate
        if GameWorld.Player() ~= nil then
            local t = GameWorld.Player():GetCameraTransform()
            GameWorld.ChangeToRotationTargetMotion(t, rotation, 1, rate[1], distance, 1.5)
            --暂不支持
            -- if cfg.camera_h_rotate_params ~= nil then
            --     local _a = cfg.camera_h_rotate_params
            --     GameWorld.UpdateCameraHRotate(_a[1], _a[2], _a[3], _a[4])
            -- end
        end
    elseif cfg.camera_type == 2 then
        
        end
end

function SpaceActionEvent:EntityCameraShake(cfg)
    GameWorld.ShakeCamera(cfg.shake_id, cfg.time)
end

function SpaceActionEvent:EntityCG(cfg)
    for eventvalue, time in pairs(cfg.cg) do
        if time == 0 then
            GameManager.DramaManager:Trigger("PLAY_CG", tostring(eventvalue))
        else
            GameWorld.TimerHeap:AddSecTimer(time, 0, 0, function()
                GameManager.DramaManager:Trigger("PLAY_CG", tostring(eventvalue))
            end)
        end
    end
end

function SpaceActionEvent:EntityCounter(cfg)

end

function SpaceActionEvent:EntityCounterComplete(cfg)

end

function SpaceActionEvent:EntityEnterInstance(cfg)

end

function SpaceActionEvent:EntityEvent(cfg)
    local ids = cfg.trigger_event_ids
    for id, time in pairs(ids) do
        if time == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id)
        else
            GameWorld.TimerHeap:AddSecTimer(time, 0, 0, function()EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id) end)
        end
    end
end

function SpaceActionEvent:EntityHideUI(cfg)

end

function SpaceActionEvent:EntityInstanceBreak(cfg)
    EventDispatcher:TriggerEvent(GameEvents.INSTANCE_FAILE_BALANCE_END)
end

function SpaceActionEvent:EntityInstanceEnd(cfg)
--LoggerHelper.Error("EntityInstanceEnd")
--EventDispatcher:TriggerEvent(GameEvents.INSTANCE_END)
end

function SpaceActionEvent:EntityInstanceInit(cfg)
    if cfg.action_type ~= 1 then
        --1为纯前端处理，2为后端处理，3为后端校验
        do return end
    end
    if cfg.next_action == 0 then
        do return end
    end
    EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, cfg.next_action)
end

--暂由服务端处理
function SpaceActionEvent:EntityZoneMonster(cfg)

end

function SpaceActionEvent:EntityMonster(cfg)
    local idx = 0
    local len = table.getn(cfg.monster_ids)
    while idx < len do
        idx = idx + 1
        local id = cfg.monster_ids[idx]
        --local face = cfg.monster_face[idx]
        local posID = cfg.refresh_point_ids[idx]
        EventDispatcher:TriggerEvent(GameEvents.CreateDummy, id, posID, cfg.id)
    end
end

function SpaceActionEvent:EntityMonsterDeath(cfg)
-- local args = cfg.death_args
-- if cfg.death_type == 1 then
-- elseif cfg._death_type == 2 then
-- end
end

function SpaceActionEvent:EntityStaticMonster(cfg)
    local ids = cfg.monster_ids
    local points = cfg.refresh_point_ids
    local actionIds = cfg.action_ids or {}
    if ids then
        for i = 1, #ids do
            EventDispatcher:TriggerEvent(GameEvents.CreateStaticMonster, ids[i], points[i], actionIds[i])
        end
    end
end

function SpaceActionEvent:EntityReUseMonster(cfg)
    if GameManager.GameSceneManager:InServerIns() then
        --return
        end
    for key, id in pairs(cfg.reuse_args) do
        local dealwith_type = cfg.dealwith_type
        local reuse_type = cfg.reuse_type
        --reuse_type: 0 触发者本身实体  1 回收参数中的ids 2 回收参数中的ids外的 3 回收对应序号刷的ids
        --dealwith_type: 0 正常死亡，有掉落，且有延时消失 1 非正常死亡，没有掉落，直接消失
        local entities = GameWorld.Entities()
        for key, entity in pairs(entities) do
            if reuse_type == 1 and entity.monster_id == id then
                if dealwith_type == 0 then
                    GameWorld.SynEntityAttrInt(entity.id, EntityConfig.PROPERTY_CUR_HP, 0)
                elseif dealwith_type == 1 then
                    GameWorld.DestroyEntity(entity.id)
                end
            
            elseif reuse_type == 3 and entity.spawn_id == id then
                if dealwith_type == 0 then
                    GameWorld.SynEntityAttrInt(entity.id, EntityConfig.PROPERTY_CUR_HP, 0)
                elseif dealwith_type == 1 then
                    GameWorld.DestroyEntity(entity.id)
                end
            end
        end
    end
end

function SpaceActionEvent:EntityShowUI(cfg)

end

function SpaceActionEvent:EntityFindPath(cfg)
    if cfg.hide_all_ui > 0 then
        GUIManager.ClosePanel(PanelsConfig.Stick)
        GUILayerManager.HideAllLayers()
        GUIManager.SetInCGCameraCulling(true)
    end
    
    GameWorld.Player():Move(cfg.destination_pos, -1)
end



function SpaceActionEvent:EntityTimeComplete(cfg)

end

function SpaceActionEvent:NPCRegion(cfg)
    local taskIds = cfg.task_id
    --由任务触发NPC生成
    if taskIds and taskIds[1] > 0 then
        local npcIds = cfg.npc_ids
        --local faces = cfg.foward
        local posIDs = cfg.npc_point_ids
        for i = 1, #taskIds do
            local taskId = taskIds[i]
            EventDispatcher:TriggerEvent(GameEvents.CreateNPCByTask, taskId, npcIds, posIDs)
        end
    else
        local idx = 0
        local len = table.getn(cfg.npc_ids)
        while idx < len do
            idx = idx + 1
            local id = cfg.npc_ids[idx]
            --local face = cfg.foward[idx]
            local posID = cfg.npc_point_ids[idx]
            EventDispatcher:TriggerEvent(GameEvents.CreateNPC, id, posID)
        end
    end
end

function SpaceActionEvent:TimeRegion(cfg)
    local t = cfg.time_second
    --  GameWorld.TimerHeap:AddSecTimer(t, 0, 0, function()
    --      EventDispatcher:TriggerEvent(GameEvents.ActionTimeComplete, cfg.id)
    --  end)
    --显示倒计时UI
    if cfg.show_ui == 1 then
        EventDispatcher:TriggerEvent(GameEvents.ShowTimeUI, cfg)
    end
    
    cfg.timer_id = GameWorld.TimerHeap:AddSecTimer(0, 1, 0, function()
        cfg:Tick()
        EventDispatcher:TriggerEvent(GameEvents.TimeCount)
        if cfg:IsComplete() then
            GameWorld.TimerHeap:DelTimer(cfg.timer_id)
            EventDispatcher:TriggerEvent(GameEvents.ActionTimeComplete, cfg.id)
        end
    end)
end

function SpaceActionEvent:EntityShowHide(cfg)
    local name = cfg.name
    local show = cfg.show == 1
    local sceneName = GameManager.GameSceneManager:GetCurSceneName()
    local scene = GameObject.Find(sceneName)
    if scene == nil then
        LoggerHelper.Error("scene not found " .. sceneName)
        do return end
    end
    --local go = GameObject.Find(name)
    local go = scene.transform:Find(name)
    if go ~= nil then
        go.gameObject:SetActive(show)
    end
end

function SpaceActionEvent:EntityBlock(cfg)

end

function SpaceActionEvent:EntityAniPlay(cfg)
    local speed = 1
    if cfg.play_type == 0 then
        speed = -1
    end
    GameWorld.SceneObjAniPlay(cfg.name, speed)
end

function SpaceActionEvent:EntityChangeBlock(cfg)
    local b = self._boxActions[cfg.block_id]
    if b ~= nil then
        b:ChangeBlock(cfg.block_state)
    end
end

function SpaceActionEvent:EntityTask(cfg)
--EventDispatcher:TriggerEvent(GameEvents.EntityTask,cfg)
end

function SpaceActionEvent:EntityPlayFX(cfg)
    for index, pos in pairs(cfg.pos) do
        EventDispatcher:TriggerEvent(GameEvents.PlayFX, cfg.id, pos, cfg.fx_path)
    end
end

function SpaceActionEvent:EntityDelFX(cfg)
    EventDispatcher:TriggerEvent(GameEvents.DelFX, cfg.id, cfg.fx_event_id, cfg.fx_name)
end

function SpaceActionEvent:EntityCloseEvent(cfg)
    EventDispatcher:TriggerEvent(GameEvents.CloseEvent, cfg.event_id)
end

function SpaceActionEvent:EntityOREvent(cfg)
    --前期判断
    local ids = cfg.trigger_event_ids
    for id, time in pairs(ids) do
        if time == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id)
        else
            GameWorld.TimerHeap:AddSecTimer(time, 0, 0, function()EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id) end)
        end
    end
end

function SpaceActionEvent:EntityWeight(cfg)
    local weight = cfg.weight
    math.randomseed(GameWorld.Player().id)
    local ran = math.random()
    local total = 0
    for k, v in pairs(weight) do
        total = total + v[2]
    end
    local len = #weight
    local idx = 1
    local tmp = 0
    while idx <= len do
        local v1 = weight[idx]
        local eid = v1[1]
        local s = tmp / total
        local w = v1[2] / total
        w = s + w
        tmp = tmp + v1[2]
        idx = idx + 1
        if ran >= s and ran <= w then
            EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, eid)
            break
        end
    end
end

function SpaceActionEvent:ActiveTeleport(cfg)
    
    EventDispatcher:TriggerEvent(GameEvents.ActiveTeleport, cfg)
    local key = cfg.id
    local act = self._triggers[key]
    if act == nil then
        do return end
    end
    act:Enable(true)
end

function SpaceActionEvent:EntityCameraWall(cfg)

end

function SpaceActionEvent:EntityCameraWallChange(cfg)
    EventDispatcher:TriggerEvent(GameEvents.ActionCameraWallChange, cfg.event_id, cfg.active)
    
    local w = self._walls[cfg.event_id]
    if w ~= nil then
        w:ChangeCameraWall(cfg.active)
    end
end

function SpaceActionEvent:EntityAddBuff(cfg)
    local type = cfg.target_type
    local targets = cfg.target_ids
    local buffs = cfg.buff_ids

    --玩家自己
    if type == 1 then
        for k, v in pairs(buffs) do
            GameWorld.Player():AddBuff(v,-1)
        end
        do return end
    end

    local entities = GameWorld.Entities()
    for k, v in pairs(entities) do
        --怪物
        if type == 3 and v:is_a(ClassTypes.Monster) then
            for k, monsterId in pairs(targets) do
                if monsterId == v.monster_id then
                    for k1, v1 in pairs(buffs) do
                        v:AddBuff(v1,-1)
                    end
                end
            end
        --NPC
        elseif type == 2 and v:is_a(ClassTypes.NPC) then
            for k, npcId in pairs(targets) do
                if npcId == v.npc_id then
                    for k1, v1 in pairs(buffs) do
                        v:AddBuff(v1,-1)
                    end
                end
            end
        end
    end
end

function SpaceActionEvent:EntityDelBuff(cfg)
    local type = cfg.target_type
    local targets = cfg.target_ids
    local buffs = cfg.buff_ids

    --玩家自己
    if type == 1 then
        for k, v in pairs(buffs) do
            GameWorld.Player():RemoveBuff(v)
        end
        do return end
    end

    local entities = GameWorld.Entities()
    for k, v in pairs(entities) do
        --怪物
        if type == 3 and v:is_a(ClassTypes.Monster) then
            for k, monsterId in pairs(targets) do
                if monsterId == v.monster_id then
                    for k1, v1 in pairs(buffs) do
                        v:RemoveBuff(v1)
                    end
                end
            end
        --NPC
        elseif type == 2 and v:is_a(ClassTypes.NPC) then
            for k, npcId in pairs(targets) do
                if npcId == v.npc_id then
                    for k1, v1 in pairs(buffs) do
                        v:RemoveBuff(v1)
                    end
                end
            end
        end
    end
end

function SpaceActionEvent:EntityChangeSpawnPoint(cfg)

end

function SpaceActionEvent:EntityTimerAddTime(cfg)
    EventDispatcher:TriggerEvent(GameEvents.ActionTimerAddTime, cfg.target_ids)
end

function SpaceActionEvent:EntityAOINew(cfg)

end

function SpaceActionEvent:EntitySingleSideBlock(cfg)

end

function SpaceActionEvent:EntitySpawnBuff(cfg)

end

function SpaceActionEvent:EntityChangeSpawnBuff(cfg)

end

function SpaceActionEvent:EntityFaction(cfg)
    -- if cfg.spawn_ids == nil then
    --     EventDispatcher:TriggerEvent(GameEvents.ActionFaction, cfg.target_type, nil, cfg.faction_id)
    -- else
    --     for key, id in ipairs(cfg.spawn_ids) do
    --         EventDispatcher:TriggerEvent(GameEvents.ActionFaction, cfg.target_type, id, cfg.faction_id)
    --     end
    -- end
    --[[
    target_type: 1 主角  2  怪物   3 NPC
    faction_id: 目标类型为1的情况下，单个出生点则不用填，多个出生点，则填“改变出生点”对应的序号
    目标类型为2的情况下，填“刷怪”的序号
    目标类型为3的情况下，填“刷NPC”的序号
    ]]
    if GameManager.GameSceneManager:InServerIns() then
        return
    end
    local target_type = cfg.target_type
    local faction_id = cfg.faction_id
    
    if target_type == 1 then
        local id = GameWorld.Player().id
        GameWorld.SynEntityAttrByte(id, EntityConfig.PROPERTY_FACTION_ID, faction_id)
    
    elseif target_type == 2 then
        local entities = GameWorld.Entities()
        for key, entity in pairs(entities) do
            if entity.spawn_id == id then
                GameWorld.SynEntityAttrByte(entity.id, EntityConfig.PROPERTY_FACTION_ID, faction_id)
            end
        end
    
    elseif target_type == 3 then
        local Entity = GameWorld.GetEntityByNpcId(id)
        if table.isEmpty(Entity) or Entity == nil then
            GameWorld.LoggerHelper.Log("找不到该NPC")
            return
        else
            GameWorld.SynEntityAttrByte(Entity.id, EntityConfig.PROPERTY_FACTION_ID, faction_id)
        end
    end
end

function SpaceActionEvent:EntityClientResourcePoint(cfg)
    --LoggerHelper.Error("EntityClientResourcePoint"..cfg.cd_time)
    if cfg.task_event_id and cfg.task_event_id > 0 then
        EventDispatcher:TriggerEvent(GameEvents.CreateClientResourcePointByTask, cfg)
    else
        EventDispatcher:TriggerEvent(GameEvents.CreateClientResourcePoint, cfg)
    end
--EventDispatcher:TriggerEvent(GameEvents.CreateClientResourcePoint, cfg.id, cfg.pos,cfg.task_event_id,cfg.cd_time)
end

function SpaceActionEvent:EntityActivateMonsterAI(cfg)

end

function SpaceActionEvent:EntityActivateAnimate(cfg)
    local go = GameObject.Find(cfg.path)
    if go then
        local animator = go:GetComponent(typeof(UnityEngine.Animator))
        animator.enabled = true
    end
end

function SpaceActionEvent:EntityGameObjectActivate(cfg)
    --local go = GameObject.Find(cfg.path)
    local rootGO = GameObject.Find("/"..cfg.root)
    local trans = rootGO.transform:Find(cfg.path)
    if trans then
        local go = trans.gameObject
        if cfg.activated == 0 then
            go:SetActive(false)
        else
            go:SetActive(true)
        end
    end
end

return SpaceActionEvent
