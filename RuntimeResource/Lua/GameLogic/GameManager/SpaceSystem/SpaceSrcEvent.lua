SpaceSrcEvent = {}

function SpaceSrcEvent:Init()
    self._spaceData = nil
    self._orEventTimers = {}
    self._isStopAction = false
    self._killnum = 0
    self._triggerAlert = {}
end

--重置场景
function SpaceSrcEvent:CleanSpace()
    self._killnum = 0
    self._triggerAlert = {}
    self:StopAction()
end

--停止场景行为
function SpaceSrcEvent:StopAction()
    self._isStopAction = true
    --消除所有定时器
    for id, timer in pairs(self._orEventTimers) do
		TimerHeap:DelTimer(timer)
	end
end

function SpaceSrcEvent:EnterMap(spaceDta)
    self._spaceData = spaceDta
    self._isStopAction = false
    self._isInServer = GameManager.GameSceneManager:InServerIns()
    self:InstanceInit()
end

--【副本初始化】
function SpaceSrcEvent:InstanceInit()
    local initActions = self._spaceData:GetInitActions()
    for k, cfg in pairs(initActions) do
        self:EventTrigger(cfg.id)
        --self:SendToServer(cfg.id)
    end
end

--检测【事件】是否被激活
function SpaceSrcEvent:EventTrigger(eventID)
    if self._isStopAction then
        --@debug_begin
        --GameWorld.LoggerHelper.Log("场景已经结束，阻止起因事件："..eventID)
        --@debug_end
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.SceneEvent,eventID)
    --self:Log(eventID)
    local events = self._spaceData:GetEventActions()
    local actives = {}
    for k, event in pairs(events) do
        local state = event.event_state[eventID]
        --LoggerHelper.Log(event.event_state)
        --有限次数触发的事件
        if state == false then
            event:SetState(eventID, true)
            if event:ActiveAble() then 
                table.insert(actives, event) 
            end
        end
        --能无限次触发的事件
        if state == true and event.count == -1 then
            if event:ActiveAble() then 
                table.insert(actives, event) 
            end
        end
    end

    for k, orEvent in pairs(actives) do
        --@debug_begin
        --GameWorld.LoggerHelper.Log("或事件:"..orEvent.id)
        --@debug_end
        self:ProEvent(orEvent)
    end
    local player = GameWorld.Player()
    if GameManager.GameSceneManager:InServerIns() and player and player.entityType == GameConfig.EnumType.EntityType.PlayerAvatar then
        player.server.instance_action_cell_req(GameWorld.action_config.INSTANCE_TRIGGER_EVENT_TO_CELL, tostring(eventID))
    end
end

--执行【事件】的后置事件
function SpaceSrcEvent:ProEvent(cfg)
    if not cfg.eventActive then
        --@debug_begin
        --GameWorld.LoggerHelper.Log("或事件"..cfg.id.."已经被关闭")
        --@debug_end
		return
	end
    
    if cfg.count == -1 or cfg.proc_num < cfg.count then
		cfg.proc_num = cfg.proc_num + 1
	else
        --@debug_begin
		--GameWorld.LoggerHelper.Log("或事件"..cfg.id.."的可执行次数已达上限："..cfg.proc_num)
        --@debug_end
		return
	end	 
    
	local ids = cfg.trigger_event_ids
	for id, time in pairs(ids) do
		if time == 0 then
			EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id)
		else
			local timer = GameWorld.TimerHeap:AddSecTimer(time, 0, 0, function() EventDispatcher:TriggerEvent(GameEvents.ProcSpaceAction, id) end)
			self._orEventTimers[id] = timer
		end
	end
end

--处理触发器相关方法
function SpaceSrcEvent:HandlerTrigger(entityId, cfg, isEnter)
    local actionCfg = self._spaceData:GetActionByID(cfg.id)
    if actionCfg == nil then
        LoggerHelper.Log("场景xml不存在该事件id："..cfg.id)
        return
    end
    local func = self[actionCfg.type]
    if func ~= nil then
        --LoggerHelper.Log("触发器："..actionCfg.type.."("..cfg.id..")")
        func(self, entityId, cfg, isEnter)
    else
        LoggerHelper.Log("该事件类型在代码里还没定义："..actionCfg.type)
    end
end

function SpaceSrcEvent:TriggerRegion(entity, cfg, enter)
    if enter then
        cfg:Enter(entity)
    else
        cfg:Exit(entity)
    end
    if not cfg:Active() then do return end end
    self:EventTrigger(cfg.id)
    EventDispatcher:TriggerEvent(GameEvents.ActionEvent, cfg.id)
    cfg.proc_num = cfg.proc_num + 1
    --任务事件处理
    if cfg.task_event_id and cfg.task_event_id > 0 then
        EventDispatcher:TriggerEvent(GameEvents.TASK_EVENT_TRIGGER,cfg.id,cfg.task_event_id)
    end
    -- local ids = cfg.trigger_event_ids
    -- LoggerHelper.Error("TriggerRegionaaa")
    -- if ids == nil then do return end end
    -- for id, time in pairs(ids) do
    -- 	--LoggerHelper.Error("TriggerRegion"..id)
    --     if time == 0 then
    --         EventDispatcher:TriggerEvent(GameEvents.ProcSceneAction, id)
    --     else
    --         GameWorld.TimerHeap:AddSecTimer(time, 0, 0, function()EventDispatcher:TriggerEvent(GameEvents.ProcSceneAction, id) end)
    --     end
    -- end
end

function SpaceSrcEvent:EntityPathPoint(entity_eid, pp, is_enter)
    local entity = GameWorld.GetEntity(entity_eid)
    if not entity then
        return
    end
    if entity.isCreature then
        entity:OnTriggerPathPoint(pp.id, is_enter)
    end
end


function SpaceSrcEvent:EntityTeleportUnlock(entity, cfg, enter)
    if entity == GameWorld.Player().id then
        if enter then
            --LoggerHelper.Error("function SpaceActionEvent:EntityTeleportUnlock"..cfg.id)
            EventDispatcher:TriggerEvent(GameEvents.TriggerTeleportUnlock,cfg.unlock_id)
        else
        end
    end
end

function SpaceSrcEvent:EntityZoneUnlock(entity, cfg, enter)
    if entity == GameWorld.Player().id then
        if enter then
            --LoggerHelper.Error("function SpaceActionEvent:EntityZoneUnlock"..cfg.id)
            EventDispatcher:TriggerEvent(GameEvents.TriggerZoneUnlock,cfg.unlock_id)
        else
        end
    end
end

function SpaceSrcEvent:EntityTriggerAlert(entity, cfg, enter)
    if entity == GameWorld.Player().id then
        if enter then
            local duration 
            if cfg.duration and cfg.duration > 0 then
               duration = cfg.duration
            end
            local langId = cfg.lang_id
            --已经触发了的就不会继续触发
            if self._triggerAlert[cfg.id] == nil then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(langId),duration, function ()
                    self._triggerAlert[cfg.id] = nil
                end)
                self._triggerAlert[cfg.id] = true
            end
        else
        end
    end
end

function SpaceSrcEvent:EntityTowerTrigger(entity,cfg, enter)
    if entity == GameWorld.Player().id then
        -- if enter then
        --     --EventDispatcher:TriggerEvent(GameEvents.TriggerFly,cfg.is_start)
        --     LoggerHelper.Error("enter")
        --     EventDispatcher:TriggerEvent(GameEvents.PlayFX, 270001, cfg.id, "Fx/fx_tower_range.prefab")
        -- else
        --     LoggerHelper.Error("del")
        --     EventDispatcher:TriggerEvent(GameEvents.DelFX, "", 270001, "")
        -- end
        
        if enter then
            EventDispatcher:TriggerEvent(GameEvents.TriggerTowerEnter,cfg)
        else
            EventDispatcher:TriggerEvent(GameEvents.TriggerTowerLeave,cfg)
        end
    end
end

function SpaceSrcEvent:EntityFlyTrigger(entity,cfg, enter)
    if cfg.hadTriggered then
        return
    end
    if entity == GameWorld.Player().id then
        if enter then
            --LoggerHelper.Error("SpaceSrcEvent:EntityFlyTrigger"..cfg.id)
            local duration = cfg.duration
            if duration and duration > 0 then
                TimerHeap:AddSecTimer(duration,0,duration,function ()
                    EventDispatcher:TriggerEvent(GameEvents.TriggerFly,cfg.is_start,cfg.skill_id,cfg.face,cfg.pos,cfg.show_ui,cfg.end_position)
                    cfg.hadTriggered = true
                end)
            else
                --LoggerHelper.Error("SpaceSrcEvent:EntityFlyTriggerOut"..cfg.id)
                EventDispatcher:TriggerEvent(GameEvents.TriggerFly,cfg.is_start,cfg.skill_id,cfg.face,cfg.pos,cfg.show_ui,cfg.end_position)
                cfg.hadTriggered = true
            end
        else
            --LoggerHelper.Error("SpaceSrcEvent:EntityFlyTriggerOut"..cfg.id)
        end
    end
end

function SpaceSrcEvent:EntityAmbientSound(entity,cfg, enter)
    if entity == GameWorld.Player().id then
        local soundId = cfg.sound_id
        if enter then
            GameWorld.PlaySound(soundId)
        else
            GameWorld.StopSound(soundId)
        end
    end
end

function SpaceSrcEvent:EntityActivateMonsterAI(entity,cfg, enter)
    if entity == GameWorld.Player().id then
        if enter then
        end
    end
end

--怪物死亡
function SpaceSrcEvent:MonsterDeath(monsterID, spawnID)
    --LoggerHelper.Error("MonsterDeath:"..spawnID)
    self._killnum = self._killnum + 1
    local events = self._spaceData:GetMonsterDeathActions()
    local actives = {}
    for k, v in pairs(events) do
        v:Death(monsterID, spawnID)
        if v:ActiveAble() then table.insert(actives, v) end
    end
    for k1, v1 in pairs(actives) do
        self:EventTrigger(v1.id)
    end
end

function SpaceSrcEvent:GetKillNum()
    return self._killnum
end

--【实体进场】
function SpaceSrcEvent:EntityEnterSpace(type)
    local events = self._spaceData:GetEnterSpace()
    for k, v in pairs(events) do
        if v:ActiveAble(type) then
            self:EventTrigger(v.id)
        end
    end
end

--【CG播放完成】
function SpaceSrcEvent:CGComplete(eventID)
    local events = self._spaceData:GetCGCompletes()
    local actives = {}
    for k, v in pairs(events) do
        if tostring(v.event_id) == eventID then
            table.insert(actives, v)
        end
    end
    for k1, v1 in pairs(actives) do
        self:EventTrigger(v1.id)
        --self:SendToServer(v1.id)
    end
end

return SpaceSrcEvent