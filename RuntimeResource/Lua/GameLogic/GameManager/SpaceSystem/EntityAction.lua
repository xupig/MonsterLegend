require "GameManager/SpaceSystem/SpaceDatas"

local EntityAction = Class.EntityAction(ClassTypes.LuaBehaviour)

function EntityAction:Awake()

end

function EntityAction:Start()

end

function EntityAction:OnDestroy()

end

function EntityAction:OnTriggerEnter(collider)
    -- body
    GameManager.SpaceEventManager:OnTriggerEnter(collider, self.data)
end

function EntityAction:OnTriggerExit(collider)
    -- body
    GameManager.SpaceEventManager:OnTriggerExit(collider, self.data)
end

function EntityAction:SetData(cfgdata)
    -- body
    local t = cfgdata.type
    self.data = cfgdata
    if self._collider == nil then
        self._collider = self.gameObject:GetComponent(typeof(UnityEngine.BoxCollider))
    end
    if t == "EntityBlock" then
        self._collider.isTrigger = false
        self._collider.enabled = self.data.block == 1
    end
    if t == "EntityCameraWall" then
        self._collider.isTrigger = false
        self:Enable(self.data.active == 1)
        self._csBH:SetLayer("CameraWall")
    end
end

function EntityAction:ShowTestCube()
    local p = GameWorld.Player()
    if p == nil then do return end end
    p.cs:CreateTempCube(self.data.pos, self.gameObject)
end

function EntityAction:Enable(e)
    self.gameObject:SetActive(e)
end

function EntityAction:ChangeBlock(state)
    self._collider.enabled = state == 1
end
