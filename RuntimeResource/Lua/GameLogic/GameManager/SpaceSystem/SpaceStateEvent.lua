SpaceStateEvent = {}
--地图编辑器状态类节点处理
local public_config = GameWorld.public_config
require "GameManager/SpaceSystem/SpaceSrcEvent"

function SpaceStateEvent:Init()
	--LoggerHelper.Error("SpaceStateEvent:Init")
	EventDispatcher:AddEventListener(GameEvents.OnTaskChanged, function() self:HandleTaskState() end)
end

function SpaceStateEvent:EnterMap(spaceData)
	self._spaceData = spaceData
end

function SpaceStateEvent:HandleTaskState()
	local stateActions = self._spaceData:GetStateAction()
	for id,cfg in pairs(stateActions) do
		local task = TaskManager:GetTaskById(cfg.task_id)
		if task:GetState() == cfg.task_state then
			SpaceSrcEvent:EventTrigger(id)
			return 
		end

		local hasCommit = PlayerManager.TaskManager:GetTaskHasCommited(cfg.task_id)
		if hasCommit  and task:GetState() == public_config.TASK_STATE_COMMITED and cfg.commitActived == false then
			SpaceSrcEvent:EventTrigger(id)
			cfg:UpdateCommitActived(true)
		end
	end
end

return SpaceStateEvent