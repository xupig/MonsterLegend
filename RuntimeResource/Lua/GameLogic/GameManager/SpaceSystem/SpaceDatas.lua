local EntityData = Class.EntityData(ClassTypes.XObject)
local EntityTeleportData = Class.EntityTeleportData(ClassTypes.EntityData)
local EntityBroadcast = Class.EntityBroadcast(ClassTypes.EntityData)
local EntityCameraMove = Class.EntityCameraMove(ClassTypes.EntityData)
local EntityCameraShake = Class.EntityCameraShake(ClassTypes.EntityData)
local EntityCG = Class.EntityCG(ClassTypes.EntityData)
local EntityCGComplete = Class.EntityCGComplete(ClassTypes.EntityData)
local EntityCounter = Class.EntityCounter(ClassTypes.EntityData)
local EntityCounterComplete = Class.EntityCounterComplete(ClassTypes.EntityData)
local EntityEnterInstance = Class.EntityEnterInstance(ClassTypes.EntityData)
local EntityEvent = Class.EntityEvent(ClassTypes.EntityData)
local EntityHideUI = Class.EntityHideUI(ClassTypes.EntityData)
local EntityInstanceBreak = Class.EntityInstanceBreak(ClassTypes.EntityData)
local EntityInstanceEnd = Class.EntityInstanceEnd(ClassTypes.EntityData)
local EntityInstanceInit = Class.EntityInstanceInit(ClassTypes.EntityData)
local EntityMonster = Class.EntityMonster(ClassTypes.EntityData)
local EntityMonsterDeath = Class.EntityMonsterDeath(ClassTypes.EntityData)
local EntityReUseMonster = Class.EntityReUseMonster(ClassTypes.EntityData)
local EntityShowUI = Class.EntityShowUI(ClassTypes.EntityData)
local EntityTimeComplete = Class.EntityTimeComplete(ClassTypes.EntityData)
local NPCRegion = Class.NPCRegion(ClassTypes.EntityData)
local PositionPoint = Class.PositionPoint(ClassTypes.EntityData)
local TimeRegion = Class.TimeRegion(ClassTypes.EntityData)
local TriggerRegion = Class.TriggerRegion(ClassTypes.EntityData)
local EntityBlock = Class.EntityBlock(ClassTypes.EntityData)
local EntityAniPlay = Class.EntityAniPlay(ClassTypes.EntityData)
local EntityShowHide = Class.EntityShowHide(ClassTypes.EntityData)
local EntityChangeBlock = Class.EntityChangeBlock(ClassTypes.EntityData)
local EntityPlayFX = Class.EntityPlayFX(ClassTypes.EntityData)
local EntityTask = Class.EntityTask(ClassTypes.EntityData)
local EntityResourcePoint = Class.EntityResourcePoint(ClassTypes.EntityData)
local EntityDelFX = Class.EntityDelFX(ClassTypes.EntityData)
local EntityPathPoint = Class.EntityPathPoint(ClassTypes.EntityData)
local EntityCloseEvent = Class.EntityCloseEvent(ClassTypes.EntityData)
local EntityWeight = Class.EntityWeight(ClassTypes.EntityData)
local EntityOREvent = Class.EntityOREvent(ClassTypes.EntityData)
local EntityCameraWall = Class.EntityCameraWall(ClassTypes.EntityData)
local EntityCameraWallChange = Class.EntityCameraWallChange(ClassTypes.EntityData)
local EntityAddBuff = Class.EntityAddBuff(ClassTypes.EntityData)
local EntityDelBuff = Class.EntityDelBuff(ClassTypes.EntityData)
local EntityTimerAddTime = Class.EntityTimerAddTime(ClassTypes.EntityData)
local EntityChangeSpawnPoint = Class.EntityChangeSpawnPoint(ClassTypes.EntityData)
local EntityAOINew = Class.EntityAOINew(ClassTypes.EntityData)
local EntitySingleSideBlock = Class.EntitySingleSideBlock(ClassTypes.EntityData)
local EntitySpawnBuff = Class.EntitySpawnBuff(ClassTypes.EntityData)
local EntityChangeSpawnBuff = Class.EntityChangeSpawnBuff(ClassTypes.EntityData)
local EntityFaction = Class.EntityFaction(ClassTypes.EntityData)
local EntityClientResourcePoint = Class.EntityClientResourcePoint(ClassTypes.EntityData)
local EntityTeleportUnlock = Class.EntityTeleportUnlock(ClassTypes.EntityData)
local EntityZoneUnlock = Class.EntityZoneUnlock(ClassTypes.EntityData)
local EntityTriggerAlert= Class.EntityTriggerAlert(ClassTypes.EntityData)
local EntityStaticMonster= Class.EntityStaticMonster(ClassTypes.EntityData)
local EntityTowerTrigger= Class.EntityTowerTrigger(ClassTypes.EntityData)
local EntityAmbientSound= Class.EntityAmbientSound(ClassTypes.EntityData)
local EntityZoneMonster= Class.EntityZoneMonster(ClassTypes.EntityData)
local EntityActivateMonsterAI= Class.EntityActivateMonsterAI(ClassTypes.EntityData)
local EntityClickFlag = Class.EntityClickFlag(ClassTypes.EntityData)
local EntityFlyTrigger= Class.EntityFlyTrigger(ClassTypes.EntityData)
local EntityFindPath = Class.EntityFindPath(ClassTypes.EntityData)
local EntityActivateAnimate = Class.EntityActivateAnimate(ClassTypes.EntityData)
local EntityGameObjectActivate = Class.EntityGameObjectActivate(ClassTypes.EntityData)
local EntityPeaceZone = Class.EntityPeaceZone(ClassTypes.EntityData)

local DataParseHelper = GameDataHelper.DataParseHelper

function EntityData:__ctor__(cfgdata)
	-- body
	self.id = cfgdata.id_i
    self.type = cfgdata.type
    self.name = self.type..self.id
    self.pos = Vector3(cfgdata.pos_x_i * 0.01, cfgdata.pos_y_i * 0.01, cfgdata.pos_z_i * 0.01)
    self.action_type = 1     --0-服务器控制行为 1-客户端控制行为
    self.rotation_pos = GameUtil.TypeUtil.ToVector3(cfgdata.rotation_pos_l)
    self.boxSize = GameUtil.TypeUtil.ToVector3(cfgdata.boxSize_l)
    self.is_not_notice_all_i = cfgdata.is_not_notice_all_i   --是否同步给所有玩家  0 - 同步所有人 1- 同步给触发者
	self.proc_num = 0 --计数事件处理次数
	self.eventActive = true
end

function EntityResourcePoint:__ctor__(cfgdata)
	self.res_type = cfgdata.res_type_i
	self.level = cfgdata.level_i
	self.open_time = cfgdata.open_time_s
	self.model_id = cfgdata.model_id_i
end

function EntityBroadcast:__ctor__(cfgdata)
	self.broadcast_type = cfgdata.broadcast_type_i
	self.content = cfgdata.content_i
	self.content_args = DataParseHelper.ParseListInt(cfgdata.content_args_l)
	self.time = cfgdata.time_f
	self.priority = cfgdata.priority_i
end

function EntityCameraMove:__ctor__(cfgdata)
	self.camera_type = cfgdata.camera_type_i
	self.camera_type_args = DataParseHelper.ParseListInt(cfgdata.camera_type_args_l)
	self.distance = cfgdata.distance_f
	self.rotation = DataParseHelper.ParseListInt(cfgdata.rotation_l)
	self.rotation_rate = DataParseHelper.ParseListInt(cfgdata.rotation_rate_l)
	self.rotationable = cfgdata.rotationable
	self.camera_h_rotate_params = DataParseHelper.ParseListInt(cfgdata.camera_h_rotate_params_l)
end

function EntityCameraShake:__ctor__(cfgdata)
	self.shake_id = cfgdata.shake_id_i
	self.time = cfgdata.time_f
end

function EntityCG:__ctor__(cfgdata)
	self.cg = DataParseHelper.ParseMSuffix(cfgdata.cg_m)
	self.notify_type = cfgdata.notify_type_i
end

function EntityCGComplete:__ctor__(cfgdata)
	self.event_id = cfgdata.event_id_i --播放的CG的Id
end

function EntityCounter:__ctor__(cfgdata)
	self.counter_type = cfgdata.counter_type_i
	self.counter_args = DataParseHelper.ParseListInt(cfgdata.counter_args_l)
	self.counter_count = DataParseHelper.ParseListInt(cfgdata.counter_count_l)
	self.ui_name = cfgdata.ui_name_s
	self.content = cfgdata.content_i
	self.content_args = DataParseHelper.ParseListInt(cfgdata.content_args_l)
end

function EntityCounterComplete:__ctor__(cfgdata)
	self.event_id = cfgdata.event_id_i
end

function EntityEnterInstance:__ctor__(cfgdata)
	self.action_ids = DataParseHelper.ParseListInt(cfgdata.action_ids_l)
end

function EntityEvent:__ctor__(cfgdata)
	self.src_event = DataParseHelper.ParseListInt(cfgdata.src_event_l)
	self.trigger_event_ids = DataParseHelper.ParseMSuffix(cfgdata.trigger_event_ids_m)
	self.count = cfgdata.count_i or 1
	self.event_state = {}
	self.state_count = 0
	self.state_total = 0
	for k, v in pairs(self.src_event) do
		self.event_state[v] = false
		self.state_total = self.state_total + 1
	end
end

function EntityEvent:SetState(eventID, state)
	self.event_state[eventID] = state
	self.state_count = self.state_count + 1
end

function EntityEvent:ActiveAble()
	return self.state_count == self.state_total
end

function EntityHideUI:__ctor__(cfgdata)
	self.ui_name = cfgdata.ui_name_s
end

function EntityInstanceBreak:__ctor__(cfgdata)

end

function EntityInstanceEnd:__ctor__(cfgdata)

end

function EntityInstanceInit:__ctor__(cfgdata)
	self.action_type = cfgdata.action_type_i
	self.next_action = cfgdata.next_action_i
end

--EntityMonster不再使用了，不要读它
function EntityMonster:__ctor__(cfgdata)
	self.refresh_point_ids = DataParseHelper.ParseListInt(cfgdata.refresh_point_ids_l)
	self.refresh_type = cfgdata.refresh_type_i
	self.monster_ids = DataParseHelper.ParseListInt(cfgdata.monster_ids_l)
	self.monster_face = DataParseHelper.ParseListInt(cfgdata.monster_face_l)
	self.monster_drop = DataParseHelper.ParseListInt(cfgdata.monster_drop_l)
	--self.task_id = cfgdata.task_id_i
end

function EntityMonsterDeath:__ctor__(cfgdata)
	self.death_type = cfgdata.death_type_i
	self.death_args = DataParseHelper.ParseListInt(cfgdata.death_args_l)
	self.monsterids = {}
end

function EntityMonsterDeath:HasFocus(monsterID, spawnID)
	if self.death_type ~= 1 and self.death_type ~= 2 then
		do return false end
	end
	if self.death_type == 1 then
		local b = false
		for k, v in pairs(self.death_args) do
			if v == spawnID then
				b = true
				break
			end
		end
		if not b then do return false end end
	else
		local b = false
		for k, v in pairs(self.death_args) do
			if v == monsterID then
				b = true
				break
			end
		end
		if not b then do return false end end 
	end
	return true
end

function EntityMonsterDeath:SetMonster(monsterID,monsterCount)
	-- if self.monsterids[monsterID] ~= nil then
	-- 	self.monsterids[monsterID] = self.monsterids[monsterID] + 1
	-- else
	-- 	self.monsterids[monsterID] = 1
	-- end

	if monsterCount then
		self.monsterids[monsterID] = monsterCount
	else
		self.monsterids[monsterID] = 1
	end
end

function EntityMonsterDeath:Death(id, spawnID)
	--只有1,2为有效怪物死亡类型
	if self.death_type ~= 1 and self.death_type ~= 2 and self.death_type ~= 3 then
		do return end
	end
	if self.death_type == 3 then
		local b = false
		for k, v in pairs(self.death_args) do
			if v == spawnID then
				b = true
				break
			end
		end
		if not b then do return end end
	end
	if self.monsterids[id] ~= nil then
		self.monsterids[id] = self.monsterids[id] - 1
		if self.monsterids[id] < 0 then self.monsterids[id] = 0 end
	end
end

function EntityMonsterDeath:ActiveAble()
	if self.death_type ~= 1 and self.death_type ~= 2 and self.death_type ~= 3 then
		do return false end
	end
	for k, v in pairs(self.monsterids) do
		if v > 0 then
			do return false end
		end
	end
	return true
end

function EntityReUseMonster:__ctor__(cfgdata)
	self.reuse_type = cfgdata.reuse_type_i
	self.reuse_args = DataParseHelper.ParseListInt(cfgdata.reuse_args_l)
	self.dealwith_type = cfgdata.dealwith_type_i
end

function EntityShowUI:__ctor__(cfgdata)
	self.ui_name = cfgdata.ui_name_s
end

function EntityTeleportData:__ctor__(cfgdata)
	-- body
    self.target_scene = cfgdata.target_scene_i
    self.target_point_ids = DataParseHelper.ParseListInt(cfgdata.target_point_ids_l)
	self.target_type = cfgdata.target_type_i
	self.teleport_args = cfgdata.teleport_args_i
	self.camera_move_type = cfgdata.camera_move_type_i
	self.camera_args = DataParseHelper.ParseListInt(cfgdata.camera_args_l)
	self.camera_times = DataParseHelper.ParseListInt(cfgdata.camera_change_time_l)
end

function EntityTimeComplete:__ctor__(cfgdata)
	self.event_id = cfgdata.event_id_i
end

function NPCRegion:__ctor__(cfgdata)
	self.npc_ids = DataParseHelper.ParseListInt(cfgdata.npc_ids_l)
	self.refresh_type = cfgdata.refresh_type_i
	self.npc_point_ids = DataParseHelper.ParseListInt(cfgdata.npc_point_ids_l)
	self.foward = DataParseHelper.ParseListInt(cfgdata.foward_l)
	self.task_id = DataParseHelper.ParseListInt(cfgdata.task_id_l)
end

function PositionPoint:__ctor__(cfgdata)

end

function TimeRegion:__ctor__(cfgdata)
	self.time_type = cfgdata.time_type_i
	self.time_second = cfgdata.time_second_f
	self.replace_time_id = cfgdata.replace_time_id_i
	self.show_ui = cfgdata.show_ui_i
	self.show_type = cfgdata.show_type_i
	self.ex_second = 0
	self.second_counter = 0
	self.timer_id = 0
end

function TimeRegion:AddTime(second)
	self.ex_second = self.ex_second + second
end

function TimeRegion:Tick()
	self.second_counter = self.second_counter + 1
end

function TimeRegion:IsComplete()
	if self.second_counter >= (self.time_second + self.ex_second) then
		self.ex_second = 0
		self.second_counter = 0
		return true
	else
		return false
	end
end

function TriggerRegion:__ctor__(cfgdata)
	self.trigger_type = cfgdata.trigger_type_i
	self.trigger_target_type = DataParseHelper.ParseListInt(cfgdata.trigger_target_type_l)
	self.or_op = cfgdata.or_i
	self.trigger_target_num = cfgdata.trigger_target_num_i
	self.show_progress = cfgdata.show_progress_i
	self.progress_time = cfgdata.progress_time_f
	self.progress_content = DataParseHelper.ParseListInt(cfgdata.progress_content_l)
	--self.trigger_event_ids = DataParseHelper.ParseMSuffix(cfgdata.trigger_event_ids_m)
	self.count = cfgdata.count_i
	self.task_event_id = cfgdata.task_event_id_i
	self.insideNum = 0
	self.insides = {} --记录在触发器里的id
	self.active = false
end

function TriggerRegion:Enter(entity)
	if self.trigger_type == 1 then do return end end --trigger_type == 1监听出触发区
	local types = self.trigger_target_type --类型列表0为主角 1为NPC 2为怪物
	local or_op = self.or_op
	local target_num = self.trigger_target_num
	local pass = false
	if types == nil then
		if entity == GameWorld.Player().id then self.active = true end
		do return end
	end
	for k, v in pairs(types) do
		if v == 0 and entity == GameWorld.Player().id then pass = true end
		if v == 1 then
			local _e = GameWorld.GetEntity(entity)
			if _e:is_a(ClassTypes.NPC) then pass = true end
		end
		if v == 2 then
			local _e = GameWorld.GetEntity(entity)
			if _e:is_a(ClassTypes.Dummy) or _e:is_a(ClassTypes.Monster) then pass = true end
		end
	end
	if not pass then do return end end
	self.insides[entity] = true
	local total = 0
	for k1, v1 in pairs(self.insides) do
		if v1 == true then total = total + 1 end
	end
	if target_num <= total then self.active = true end
end

function TriggerRegion:Exit(entity)
	self.insides[entity] = nil
	if self.trigger_type == 0 then
		if self.trigger_target_type == nil then
			if entity == GameWorld.Player().id then self.active = true end
			do return end
		end
		local pass = false
		for k, v in pairs(self.trigger_target_type) do
			if v == 0 and entity == GameWorld.Player().id then pass = true end
			if v == 1 then
				local _e = GameWorld.GetEntity(entity)
				if _e:is_a(ClassTypes.NPC) then pass = true end
			end
			if v == 2 then
				local _e = GameWorld.GetEntity(entity)
				if _e:is_a(ClassTypes.Dummy) or _e:is_a(ClassTypes.Monster) then pass = true end
			end
		end
		if not pass then do return end end
		self.active = true
	end
end

function TriggerRegion:Active()
	local rst = self.active
	self.active = false
	return rst
end

function EntityBlock:__ctor__(cfgdata)
	self.block = cfgdata.block_i
end

function EntityAniPlay:__ctor__(cfgdata)
	self.name = cfgdata.name_s
	self.play_type = cfgdata.play_type_i
end

function EntityShowHide:__ctor__(cfgdata)
	self.name = cfgdata.name_s
	self.show = cfgdata.show_i
end

function EntityChangeBlock:__ctor__(cfgdata)
	self.block_id = cfgdata.block_id_i
	self.block_state = cfgdata.block_state_i
end

function EntityPlayFX:__ctor__(cfgdata)
	self.fx_type = cfgdata.fx_type_i
	self.pos = DataParseHelper.ParseListInt(cfgdata.pos_l)
	self.fx_path = cfgdata.fx_path_s
end

function EntityTask:__ctor__(cfgdata)
	self.task_id = cfgdata.task_id_i
	self.task_state = cfgdata.task_state_i
	self.commitActived = false --节点因为提交任务而触发过
end

function EntityTask:UpdaeCommitActived(value)
	self.commitActived = value
end

function EntityDelFX:__ctor__(cfgdata)
	self.fx_event_id = cfgdata.fx_event_id_i
	self.fx_name = cfgdata.fx_name_s
end

function EntityPathPoint:__ctor__(cfgdata)
	self.idx = cfgdata.idx_i
	self.always_exist = cfgdata.always_exist_i
end

function EntityCloseEvent:__ctor__(cfgdata)
	self.event_id = DataParseHelper.ParseListInt(cfgdata.event_id_l)
end

function EntityWeight:__ctor__(cfgdata)
	self.weight = DataParseHelper.ParseNSuffix(cfgdata.weight_n)
end

function EntityOREvent:__ctor__(cfgdata)
	self.src_event = DataParseHelper.ParseNSuffix(cfgdata.src_event_n)
	self.trigger_event_ids = DataParseHelper.ParseMSuffix(cfgdata.trigger_event_ids_m)
	self.event_or_state = {}
	self.event_state = {}
	self.active = false
	for k, v in pairs(self.src_event) do
		for k1, v1 in pairs(v) do
			if self.event_or_state[k] == nil then
				self.event_or_state[k] = {}
			end
			self.event_state[v1] = false
			self.event_or_state[k][v1] = false
		end
	end
end

function EntityOREvent:SetState(eventID, state)
	local hasfalse = false
	for k, v in pairs(self.src_event) do
		hasfalse = false
		for k1, v1 in pairs(v) do
			if v1 == eventID then
				self.event_or_state[k][v1] = state
			end
			if self.event_or_state[k][v1] ~= true then
				hasfalse = true
			end
		end
		if not self.active and not hasfalse then
			self.active = true
		end
	end
end

function EntityOREvent:ActiveAble()
	return self.active
end

function EntityCameraWall:__ctor__(cfgdata)
	self.active = cfgdata.active_i
end

function EntityCameraWallChange:__ctor__(cfgdata)
	self.event_id = cfgdata.event_id_i
	self.active = cfgdata.active_i
end

function EntityAddBuff:__ctor__(cfgdata)
	self.target_type = cfgdata.target_type_i
	self.target_ids = DataParseHelper.ParseListInt(cfgdata.target_ids_l)
	self.buff_ids = DataParseHelper.ParseListInt(cfgdata.buff_ids_l)
end

function EntityDelBuff:__ctor__(cfgdata)
	self.target_type = cfgdata.target_type_i
	self.target_ids = DataParseHelper.ParseListInt(cfgdata.target_ids_l)
	self.buff_ids = DataParseHelper.ParseListInt(cfgdata.buff_ids_l)
end

function EntityChangeSpawnPoint:__ctor__(cfgdata)
	self.team = cfgdata.team_i
	self.spawn_points = DataParseHelper.ParseMSuffix(cfgdata.spawn_points_m)
	self.relation = cfgdata.relation_i
end

function EntityTimerAddTime:__ctor__(cfgdata)
	self.target_ids = DataParseHelper.ParseMSuffix(cfgdata.target_ids_m)
end

function EntityAOINew:__ctor__(cfgdata)
	self.target_type = cfgdata.target_type_i
	self.target_ids = DataParseHelper.ParseListInt(cfgdata.target_ids_l)
	self.active = false
end

function EntityAOINew:Focus(entity, cfgid)
	if self.target_type == 0 then
		if self.target_ids == nil then
			self.active = true
		else

		end
	elseif self.target_type == 1 then
		if self.target_ids == nil and entity:is_a(ClassTypes.PlayerAvatar) then
			self.active = true
		else

		end
	elseif self.target_type == 2 and entity:is_a(ClassTypes.SoulManPet) then
		if self.target_ids == nil then
			self.active = true
		else

		end
	end
end

function EntityAOINew:Active()
	local rst = self.active
	self.active = false
	return rst
end

function EntitySingleSideBlock:__ctor__(cfgdata)
	self.block = cfgdata.block_i
end

function EntitySpawnBuff:__ctor__(cfgdata)
	self.targets = DataParseHelper.ParseListInt(cfgdata.targets_l)
	self.ids = DataParseHelper.ParseListInt(cfgdata.id_l)
	self.buff = DataParseHelper.ParseNSuffix(cfgdata.buff_n)
	self.spawn_type = cfgdata.spawn_type_i
	self.condition = cfgdata.spawn_condition_i
	self.time = cfgdata.spawn_time_f
	self.spawn_active = cfgdata.is_active_i
end

function EntityChangeSpawnBuff:__ctor__(cfgdata)
	self.ids = DataParseHelper.ParseMSuffix(cfgdata.ids_m)
end

function EntityFaction:__ctor__(cfgdata)
	self.target_type = cfgdata.target_type_i
	self.spawn_ids = DataParseHelper.ParseListInt(cfgdata.spawn_ids_l)
	self.faction_id = cfgdata.faction_id_i
end

function EntityClientResourcePoint:__ctor__(cfgdata)
	self.res_type = cfgdata.res_type_i
	self.level = cfgdata.level_i
	self.open_time = cfgdata.open_time_s
	self.model_id = cfgdata.model_id_i
	self.task_event_id = cfgdata.task_event_id_i
	self.cd_time = cfgdata.cd_time_i
	self.destroy_after_collect = cfgdata.destroy_after_collect_i --采集后是否销毁
	self.collect_btn_icon = cfgdata.collect_btn_icon_i			--采集按钮图标
	self.collect_point_name = cfgdata.collect_point_name_i		--采集点名称
	-- 采集点交互动作
 	-- 1：主体id，0是主角，其他是NPC
 	-- 2：动作id
	-- 3：触发时机，1交互前，2交互中，交互后
 	-- 4：延时
	self.collect_action_args = DataParseHelper.ParseNSuffix(cfgdata.collect_action_args_n)
end

function EntityTeleportUnlock:__ctor__(cfgdata)
	self.unlock_id = cfgdata.unlock_id_i
	-- self.insideNum = 0
	-- self.insides = {} --记录在触发器里的id
	-- self.active = false
end

function EntityZoneUnlock:__ctor__(cfgdata)
	self.unlock_id = cfgdata.unlock_id_i
	-- self.insideNum = 0
	-- self.insides = {} --记录在触发器里的id
	-- self.active = false
end

function EntityTriggerAlert:__ctor__(cfgdata)
	self.trigger_type = cfgdata.trigger_type_i;
    self.duration = cfgdata.duration_i;
    self.lang_id = cfgdata.lang_id_i;
end

function EntityStaticMonster:__ctor__(cfgdata)
	self.monster_ids = DataParseHelper.ParseListInt(cfgdata.monster_ids_l)
	self.action_ids = DataParseHelper.ParseListInt(cfgdata.action_ids_l)
	self.refresh_point_ids = DataParseHelper.ParseListInt(cfgdata.refresh_point_ids_l)
end

function EntityTowerTrigger:__ctor__(cfgdata)
	--self.is_start = cfgdata.is_start_i
end

function EntityAmbientSound:__ctor__(cfgdata)
	self.sound_id = cfgdata.sound_id_i
end

function EntityZoneMonster:__ctor__(cfgdata)
	self.refresh_point_ids = DataParseHelper.ParseListInt(cfgdata.refresh_point_ids_l)
	self.monster_count = cfgdata.monster_count_i
	self.monster_id = cfgdata.monster_id_i
	self.duration = cfgdata.duration_i
end

function EntityActivateMonsterAI:__ctor__(cfgdata)
	self.monster_action_id = cfgdata.monster_action_id_i
end

function EntityClickFlag:__ctor__(cfgdata)
end

function EntityFlyTrigger:__ctor__(cfgdata)
	self.is_start = cfgdata.is_start_i
	self.skill_id = cfgdata.skill_id_i
	self.face = cfgdata.face_i
	self.show_ui = cfgdata.show_ui_i
	self.duration = cfgdata.duration_i
	self.end_position = cfgdata.end_position_i
	self.hadTriggered = false
end

function EntityFindPath:__ctor__(cfgdata)
	self.hide_all_ui = cfgdata.hide_all_ui_i
	self.destination_pos = GameUtil.TypeUtil.ToVector3(cfgdata.destination_pos_l)
end

function EntityActivateAnimate:__ctor__(cfgdata)
	self.duraion = cfgdata.duraion_i
	self.path = cfgdata.path_s
end

function EntityGameObjectActivate:__ctor__(cfgdata)
	self.activated = cfgdata.activated_i
	self.root = cfgdata.root_s
	self.path = cfgdata.path_s
end

function EntityPeaceZone:__ctor__(cfgdata)
	self.raid = cfgdata.raid_i
end
