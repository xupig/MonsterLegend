-- PreloadManager.lua
LoadingBarManager = {}

local SceneConfig = GameConfig.SceneConfig
local ProgressBar = GameWorld.ProgressBar
local GUIManager = GameManager.GUIManager
local PauseFpsType = GameConfig.EnumType.PauseFpsType

function LoadingBarManager:Init()
    self._isShow = true
    self._actorCount = 0
    self._needCount = 0
    self._isSceneLoaded = false
    self._isActorLoaded = false
    self._isNeedActorLoaded = false
    self._isFirstLoad = true
    self._openServerDay = 0
    self._level = 0

    self._onAvatarActorLoaded = function()self:OnAvatarActorLoaded() end
    EventDispatcher:AddEventListener(GameEvents.On_Avatar_Actor_Loaded, self._onAvatarActorLoaded)
end

--模型加载完成计数
function LoadingBarManager:OnAvatarActorLoaded()
    if not self._isNeedActorLoaded then
        return
    end
    self._actorCount = self._actorCount + 1
    if self._actorCount == self._needCount then
        self:OnActorLoadFinsh()
    end
end

function LoadingBarManager:OnActorLoadFinsh()
    if self._isSceneLoaded then
        self:CloseProgressBar()
    else
        self._isActorLoaded = true
    end
end

function LoadingBarManager:Reset()
    self._actorCount = 0
    self._isNeedActorLoaded = false
    self._isSceneLoaded = false
    self._isActorLoaded = false
end

function LoadingBarManager:SetOpenServerDay(day)
    self._openServerDay = day
end

function LoadingBarManager:SetPlayerLevel(level)
    self._level = level
end

function LoadingBarManager:CheckNeedActorLoaded()
    local player = GameWorld.Player()
    if player and player.isWatchWar then
        self._isNeedActorLoaded = true
        if self._sceneType == SceneConfig.SCENE_TYPE_LADDER_1V1 then
            self._needCount = 2
        else
            self._needCount = 1
        end
        return
    end
    self._isNeedActorLoaded = false
end

function LoadingBarManager:OnLoadGameSceneStart(sceneType)
    if self._isShow then
        self._sceneType = sceneType
        self:CheckNeedActorLoaded()
        self:ShowProgressBar(0.1)
    end
end

function LoadingBarManager:OnLoadGameSceneFinish()
    if self._isShow then
        if self._isNeedActorLoaded then
            if self._isActorLoaded then
                self:CloseProgressBar()
            else
                self._isSceneLoaded = true
            end
        else
            self:CloseProgressBar()
        end
    end
end

function LoadingBarManager:ShowProgressBar(progress)
    if self._isFirstLoad then
        ProgressBar:ShowChange()
    else
        ProgressBar:ShowChangeByLevelAndOpenServerDay(self._level, self._openServerDay)
    end

    if not self._hasShowFloatText and not self._isFirstLoad then
        ProgressBar:ShowFloatText(true)
        self._hasShowFloatText = true
    end
    
    self._showTime = Time.realtimeSinceStartup
    ProgressBar:UpdateProgress(progress or 0.8, false)
    GameWorld.SetPauseFps(PauseFpsType.SceneChange, true)
end

function LoadingBarManager:WriteLog(msg)
    GameWorld.WriteInfoLog(msg .. (Time.realtimeSinceStartup - self._showTime))
end

function LoadingBarManager:CloseProgressBar()
    if self._isFirstLoad then
        self._isFirstLoad = false
    end
    ProgressBar:Close()
    self:Reset()
    -- self:EnterSpaceInClient()
    GameWorld.SetPauseFps(PauseFpsType.SceneChange, false)
    GameWorld.WriteInfoLog("CloseProgressBar: " .. (Time.realtimeSinceStartup - self._showTime))
end

function LoadingBarManager:SetShow(isShow)
    self._isShow = isShow
end

-- --客户端资源加载完成进入场景
-- function LoadingBarManager:EnterSpaceInClient()
--     local player = GameWorld.Player()
--     --if player.isWatchWar and self._sceneType == SceneConfig.SCENE_TYPE_LADDER_1V1 then
-- end
return LoadingBarManager
