--WhiteListManager.lua
WhiteListManager = {}

local XMLParser = GameWorld.XMLParser
local DateTimeUtil = GameUtil.DateTimeUtil

local ServerListManager = GameManager.ServerListManager
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig

local whiteList = nil
local callbackFun = nil
local checkUserId = nil
local isForTester = false
function WhiteListManager.IsInWhiteList(userId, callback)
    callbackFun = callback
    checkUserId = userId
    if whiteList == nil then
        WhiteListManager.LoadWhiteList()
        return
    end
    WhiteListManager.AccountInWhiteList()
end

-- 进行验证是否在白名单里
function WhiteListManager.AccountInWhiteList()
    -- 现在服务器时间
    local d = DateTimeUtil.GetServerTime()
    -- LoggerHelper.Log(PrintTable:TableToStr(whiteList))
    for k,itemData in pairs(whiteList) do
        if d > tonumber(itemData.beginTime) and d < tonumber(itemData.endTime) then
            -- LoggerHelper.Log("AccountInWhiteList: " .. checkUserId)
            if itemData.accountList[checkUserId] ~= nil then
                if callbackFun ~= nil then
                    callbackFun()
                end
                return
            end
        end
    end
    WhiteListManager.OnShowServerClose()
end

--显示关服提示
function WhiteListManager.OnShowServerClose()
    if isForTester then
        return
    end
    local value = {["text"] = ServerListManager:SelectedDetail()}
    GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
    EventDispatcher:TriggerEvent(GameEvents.OnShowWaitingImage, false)
end

function WhiteListManager.LoadWhiteList()
    local template = GameLoader.SystemConfig.GetValueInCfg('WhiteListUrl')
    if template == nil or template == '' then
        return
    end
    local onDone = function(result) WhiteListManager.OnActivationCodeHttpDone(result) end
    local onFail = function(result) WhiteListManager.OnActivationCodeHttpDone(result) end
    GameWorld.DownloadString(template, onDone, onFail)
end



function WhiteListManager.WhiteListFromXml(WhiteListXmlData)
    whiteList = {}
    for k, v in pairs(WhiteListXmlData.root:children()) do
        local data = {}
        for k2, v2 in pairs(v:children()) do
            local v2name = v2:name()
            if v2name == "begin" then
                data.beginTime = v2:value()
            elseif v2name == "end" then
                data.endTime = v2:value()
            elseif v2name == "list" then
                local accountList = {}
                for k3,v3 in pairs(v2:children()) do
                    accountList[v3:value()] = 1
                end
                data.accountList = accountList
            end
        end
        table.insert( whiteList,data )
    end
    
    -- EventDispatcher:TriggerEvent(GameEvents.Login, tempAccount, tempPassword,tempServerid)
    -- 下载完后看是否在白名单里，如果在就继续登录
    WhiteListManager.AccountInWhiteList()
end

function WhiteListManager.OnActivationCodeHttpDone(content)
    if type(content) ~= "string" then
        return
    end
    -- LoggerHelper.Log(content)
    local WhiteListXmlData = XMLParser:ParseXmlText(content)
    -- print("ServersXmlData:", WhiteListXmlData)
    WhiteListManager.WhiteListFromXml(WhiteListXmlData)
end

function WhiteListManager.OnActivationCodeHttpFail(result)

end

function WhiteListManager.IsInWhiteListForTester(userId, callback)
    callbackFun = callback
    checkUserId = userId
    isForTester = true
    if whiteList == nil then
        WhiteListManager.LoadWhiteList()
        return
    end
    WhiteListManager.AccountInWhiteList()
end

return WhiteListManager