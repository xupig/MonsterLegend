local data_index_pb = require "Protol.data_index_pb"

XMLManager = {}
local table = table
local XMLParser = GameWorld.XMLParser

--对某一表格数据进行初始化，主要是提取子数据位置信息
function XMLManager.InitDataTable(xmlData)
    local fileAccMgr = GameLoader.IO.FileAccessManager
    xmlData.bytes = fileAccMgr.LoadBytes(xmlData.path)
    local bytes = xmlData.bytes
    local arrayPBLen = bytes[0] + bytes[1] * (2 ^ 8)  + bytes[2] * (2 ^ 16) + bytes[3] * (2 ^ 24) 
    xmlData.offset = arrayPBLen + 4    
    local intArray = GameWorld.BytesDesToIntArray(bytes, 4, arrayPBLen)
    local indexInfo = {}
    local keys = {}
    for i=0, intArray.Length - 1, 3 do
        indexInfo[intArray[i]] = {intArray[i+1], intArray[i+2]}
        table.insert(keys, intArray[i])
    end
    xmlData.indexInfo = indexInfo
    xmlData.data = {}
    xmlData.inited = true
    xmlData.keys = keys
end

--通过对应的id找到需要被反序列化的数据段，并返回反序列化后的结果
function XMLManager.LoadPBDataByKey(xmlData, key)
    local result = nil
    if xmlData.indexInfo[key] ~= nil then 
        local bytes = xmlData.bytes
        local localOffset = xmlData.indexInfo[key][1]
        local localLength = xmlData.indexInfo[key][2]
        local arrayBytes = {}
        local idx = 1
        local startIdx = xmlData.offset + localOffset
        for i=startIdx, startIdx + localLength - 1, 1 do
            arrayBytes[idx] = bytes[i]   
            idx = idx + 1
        end    
        local arrayString = string.char(unpack(arrayBytes))
        result = xmlData.pb()
        result:ParseFromString(arrayString)            
    end
    return result
end

function XMLManager.LoadXMLDataByKey(xmlData, key)
    local result = nil
    if xmlData.xml[key] ~= nil then 
        result = {}
        local xmlContent = xmlData.xml[key]
        local children = xmlContent:children()
        for key,v in pairs(children) do
            local key,value = XMLManager.GetKeyValue(v:name(), v:value())
            result[key] = value
            --LoggerHelper.Log("~~~~~~~~~", key, value, type(value))
        end         
    end
    return result
end

function XMLManager.GetKeyValue(key, value)
    local flag = string.sub(key, -2, -2)
    if flag == "_" then
        local tp = string.sub(key, -2, -1)
        if tp == "_i" then return "__"..string.sub(key, 0, -3), tonumber(value) end
        if tp == "_f" then return "__"..string.sub(key, 0, -3), tonumber(value)  end
        return "__"..string.sub(key, 0, -3), value
    else
        return "__"..key, value
    end
end

function XMLManager.CheckReloadXML()
    local map = GameWorld.GetReloadXmlMap()
    local iter = map:GetEnumerator() 
    while iter:MoveNext() do
        local name = iter.Current.Key
        local content = iter.Current.Value
        XMLManager[name]:ReloadByXML(content)
    end
end

local _xmlData = {}
_xmlData.__index = function(xmlData, key)
        local func = rawget(_xmlData, key)
        if func ~= nil then
            return func
        end
        if xmlData.inited == false then
            XMLManager.InitDataTable(xmlData)
        end
        if xmlData.data[key] == nil then
            if xmlData.reloadedByXML == true then
                xmlData.data[key] = XMLManager.LoadXMLDataByKey(xmlData, key)
            else
                xmlData.data[key] = XMLManager.LoadPBDataByKey(xmlData, key)
            end
        end
        return xmlData.data[key]
end

function _xmlData:ReloadByXML(xmlContent)
    local xmlData = XMLParser:ParseXmlText(xmlContent)
    self.reloadedByXML = true
    self.data = {}
    self.keys = {}
    self.inited = true
    self.xml = {}
    local children = xmlData.root:children()  
    local id
    for k, v in pairs(children) do
        id = tonumber(v.id_i:value())
        self.xml[id] = v
        table.insert(self.keys, id)
    end
end

function _xmlData:Keys()
    if self.inited == false then
        XMLManager.InitDataTable(self)
    end
    return self.keys
end

setmetatable(_xmlData, _xmlData)

local metatable = { 
    __index = function(xmlmanager, key)
        local xmlData = {}
        xmlData.path = "data/pbufs/"..key..".pbuf"
        xmlData.inited = false
        xmlData.reloadedByXML = false
        local pbModule = "Protol."..key.."_pb"
        xmlData.pb = require(pbModule)[key]
        setmetatable(xmlData, _xmlData)
        xmlmanager[key] = xmlData
        return xmlData
    end
}

setmetatable(XMLManager, metatable)

return XMLManager