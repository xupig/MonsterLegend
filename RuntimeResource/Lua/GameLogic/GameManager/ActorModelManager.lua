-- ActorModelManager.lua
-- 玩家Avatar展示管理器
local Camera = UnityEngine.Camera
local CameraClearFlags = UnityEngine.CameraClearFlags
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath
local LightType = UnityEngine.LightType
-- local LightmapBakeType = UnityEngine.LightmapBakeType
local Light = UnityEngine.Light
local LayerMask = UnityEngine.LayerMask
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ActorModelComponent = GameMain.ActorModelComponent

local ActorModelManager = {}

--单个模型数据
local SingleActorModel = Class.SingleActorModel(ClassTypes.XObject)

function SingleActorModel:__ctor__(index)
 	self.isActive = false
 	self.model = nil

 	local containerModel = GameObject.New("ModelsContainer")
	containerModel.transform:SetParent(self.actorModelsContainerTrans)

	local modelComponent = containerModel:AddComponent(ActorModelComponent)
    modelComponent:SetStartSetting(Vector3(0, 10, -1000), Vector3(0, 180, 0), 1)
end

function SingleActorModel:SetModel()
	
end

function ActorModelManager:Init()
	self._modelPools = {}
	--LoggerHelper.Error("ActorModelManager:Init()")
	self.actorModelsContainer = GameObject.New("ActorModelsContainer")
	self.actorModelsContainerTrans = self.actorModelsContainer.transform
	self.actorModelsContainerTrans.localPosition = Vector3.New(-1200,0,0)
	GameObject.DontDestroyOnLoad(self.actorModelsContainer)


	-- self:BuildLight()
end

function ActorModelManager:AddModel()
	
end

function ActorModelManager:GetModel(modelId)
	local model
	if #self._modelPools > 0 then
		model = table.remove(self._modelPools, 1)
	else
		self:AddModel()
	end
    model:LoadModel(modelId)
end

function ActorModelManager:ClearModel()
end


function ActorModelManager:CreateRttCamera()
	
	self.rttForTeamMember = GameObject.New("RttForTeamMember")
	self.rttForTeamMember.transform:SetParent(self.actorModelsContainerTrans)
	
	self.RenderCameraAvatar = self.rttForTeamMember:AddComponent(typeof(Camera))
	-- self.RenderCameraAvatar.renderingPath = RenderingPath.Forward--DeferredLighting --.DeferredShading
	
	self.RenderTextureAvatar = RenderTexture.New(1060,332,16)
	self.RenderTextureAvatar.name = "ActorModel"
	self.RenderCameraAvatar.targetTexture = self.RenderTextureAvatar
	self.RenderCameraAvatar.clearFlags = CameraClearFlags.SolidColor;
	self.RenderCameraAvatar.backgroundColor = UnityEngine.Color.New(46/255,64/255,83/255,0/255)
	self.RenderCameraAvatar.fieldOfView = 20
	self.RenderCameraAvatar.cullingMask = math.pow(2,LayerMask.NameToLayer("UI"))
	self.RenderCameraAvatar.transform.localPosition = Vector3.New(-1503.59,1.28,5.1)
	self.RenderCameraAvatar.transform.rotation =  Vector3.New(148,224,-1700)
	
end

-- function ActorModelManager:BuildLight()
-- 	local lightGo = GameObject.New("ShowDirectLight")
--     lightGo.transform:SetParent(self.actorModelsContainerTrans)
--     lightGo.transform.position = Vector3.New(-1200,0,-0)
-- 	lightGo.transform.localEulerAngles = Vector3.New(164,1,1.3)
--     GameObject.DontDestroyOnLoad(lightGo)
-- 	local lightComponet = lightGo:AddComponent(typeof(Light))
-- 	lightComponet.type = LightType.Directional
-- 	-- lightComponet.lightmapBakeType = LightmapBakeType.Mixed
-- 	lightComponet.intensity = 1
-- 	lightComponet.cullingMask = math.pow(2,LayerMask.NameToLayer("UI"))
-- end

-- function ActorModelManager:GetRawImage()
-- 	if ActorModelManager.CameraInited ==  false then
-- 		self:CreateRttCamera()
-- 		ActorModelManager.CameraInited = true
-- 	end
-- 	return self.RenderTextureAvatar
-- end
ActorModelManager:Init()

return ActorModelManager