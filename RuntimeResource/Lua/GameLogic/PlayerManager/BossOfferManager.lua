local BossOfferManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local RedPointManager = GameManager.RedPointManager
local BossOfferData = PlayerManager.PlayerDataManager.bossOfferData

function BossOfferManager:Init()
	
	self._opentimeCB = function() self:OpenTimer() end	
end

function BossOfferManager:OnPlayerEnterWorld()
	self:OpenTimer()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)
	EventDispatcher:AddEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)
end

function BossOfferManager:OnPlayerLeaveWorld()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)	
	EventDispatcher:RemoveEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)	
end

function BossOfferManager:OpenTimer()
	local f = FunctionOpenDataHelper:CheckPanelOpen(411)
	local id = GameWorld.Player().boss_reward_unlocked_step
	if f and not BossOfferData:IsOverNow(id) then
		EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, 411, function()GUIManager.ShowPanel(PanelsConfig.BossOffer)end)	
	else
		EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, 411)				
	end
end

--Resp
function BossOfferManager:HandleResp(action_id,error_id,lua_table)
	if error_id~=0 then
		return
	end
	if action_id == action_config.BOSS_REWARD_UNLOCK_REQ then
		BossOfferManager.BossInfoRpc()
	elseif action_id == action_config.BOSS_REWARD_GET_INFO_REQ then
		BossOfferData:UpdateInfo(lua_table)
		EventDispatcher:TriggerEvent(GameEvents.BossOfferInfoUpdate)  	
		self:UpdataRedPoint()
	elseif action_id == action_config.BOSS_REWARD_REFRESH then
		BossOfferData:UpdateState(lua_table)
		EventDispatcher:TriggerEvent(GameEvents.BossOfferStateUpdate)  		
		self:UpdataRedPoint()
	end
end

function BossOfferManager:UpdataRedPoint()
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_BOSS_REWARD, self:RefreshRedPointState())     
end

function BossOfferManager:RefreshRedPointState()
	local state = BossOfferData:GetInfoState()
	if state and state[1]==public_config.BOSS_REWARD_STATE_COMPLETED then
		return true
	end
	state = BossOfferData:GetState()
	if state then
        local id = GameWorld.Player().boss_reward_unlocked_step
        local s = state[id]
        if s and s[2] then
			local des = s[2]
			if des==public_config.BOSS_REWARD_STATE_COMPLETED then
				return true
			end
        end
    end
	return false
end
-- Rpc
function BossOfferManager.BossRewardRpc()
	GameWorld.Player().server.boss_reward_action_req(action_config.BOSS_REWARD_UNLOCK_REQ,'')	
end

function BossOfferManager.BossInfoRpc()
	GameWorld.Player().server.boss_reward_action_req(action_config.BOSS_REWARD_GET_INFO_REQ,'')	
end

BossOfferManager:Init()
return BossOfferManager