local FlowerManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FlowerPanelType = GameConfig.EnumType.FlowerPanelType
local FlowerData = PlayerManager.PlayerDataManager.flowerData
local SystemSettingManager = PlayerManager.SystemSettingManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper
function FlowerManager:Init()
    self:InitCallbackFunc()
end

function FlowerManager:InitCallbackFunc()

end

function FlowerManager:OnPlayerEnterWorld()

end

function FlowerManager:OnPlayerLeaveWorld()

end

function FlowerManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        return
    end
    if event_id == action_config.SEND_FLOWER_SEND then
        if args and args[4] then
            local d={} 
            local a = GlobalParamsHelper.GetParamValue(795)[args[4]]
            local backdata = {}
            backdata[1] = args[2]
            backdata[2] = args[3]
            FlowerData:UpdataBackFlowerData(backdata)           
            if a then
                d = {['0']=args[3],['1']=ItemDataHelper.GetItemName(args[4]),['2']=a[2],['3']=a[1]}
                FlowerData:UpdateSendFlowerData(d)
                if args and args[1]== public_config.SEND_FLOWER_WAY_HIDE or args[1]== public_config.SEND_FLOWER_WAY_NAME then
                    EventDispatcher:TriggerEvent(GameEvents.FlowerSendResp)    
                end  
            end    
        end    
    elseif event_id == action_config.SEND_FLOWER_RECEIVE then--收到被送花消息
        FlowerData:UpdateReceiveFlowerData(args)
        if GUIManager.PanelIsActive(PanelsConfig.Flower) then
            EventDispatcher:TriggerEvent(GameEvents.FlowerReceiveMessage)   
        else
            GUIManager.ShowPanel(PanelsConfig.Flower,{_index = FlowerPanelType.Report,_data = nil})
        end
    elseif event_id == action_config.SEND_FLOWER_KISS_BACK then--收到回吻
        local showText = LanguageDataHelper.CreateContentWithArgs(80222,{['0']=args[2]})
        GUIManager.ShowText(2, showText)
    elseif event_id == action_config.SEND_FLOWER_KISS then--回吻response
        local showText = LanguageDataHelper.GetContent(80221)
        GUIManager.ShowText(2, showText)
    elseif event_id == action_config.SEND_FLOWER_EFFECT then--全服收到收花消息
        if not SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_FLOWERFX) then
            GUIManager.ShowPanel(PanelsConfig.GiveFlowerFx)
        end
    else
        LoggerHelper.Error("FlowerManager event id not handle:" .. event_id)
    end
end


-- RPC
-- 送花
function FlowerManager:SendFlowerRpc(dbid,itemid,funid,num)
    GameWorld.Player().server.send_flower_action_req(action_config.SEND_FLOWER_SEND,dbid..','..itemid..','..funid..","..num)
end

function FlowerManager:SendFlowerByNameRpc(name,itemid,funid,num)
    GameWorld.Player().server.send_flower_action_req(action_config.SEND_FLOWER_BY_NAME,name..','..itemid..','..funid..','..num)
end
-- 回吻
function FlowerManager:SendKissrRpc(dbid)
    GameWorld.Player().server.send_flower_action_req(action_config.SEND_FLOWER_KISS,dbid..'')
end
-- 收花
function FlowerManager:ReceiveFlowerRpc()
    GameWorld.Player().server.send_flower_action_req(action_config.SEND_FLOWER_RECEIVE,'')
end
FlowerManager:Init()
return FlowerManager
