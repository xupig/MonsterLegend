local AchievementManager = {}

local action_config = require("ServerConfig/action_config")
local achievementData = PlayerManager.PlayerDataManager.achievementData
local public_config = GameWorld.public_config
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local GUIManager = GameManager.GUIManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local CommonWaitManager = PlayerManager.CommonWaitManager

function AchievementManager:Init()
    self._commitList = {}
	self:InitCallbackFunc()
end

function AchievementManager:InitCallbackFunc()
    self._onFunctionOpen = function(funcId) self:OnFunctionOpen(funcId) end
    self._achievementOpenFunc = function() GUIManager.ShowPanel(PanelsConfig.Achievement) end
end

function AchievementManager:OnPlayerEnterWorld()
    achievementData:Init()

    self._achievementNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.ACHIEVEMENT, public_config.FUNCTION_ID_ACHIEVEMENT, self._achievementOpenFunc)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function AchievementManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function AchievementManager:OnFunctionOpen(funcId)
    if funcId == public_config.FUNCTION_ID_ACHIEVEMENT then
        self:GetAchievementListReq()
    end
end

--获取配置的总成就点数
function AchievementManager:GetMaxTotalPoints()
    return achievementData:GetTotalPoints()
end

--根据成就大类id获取该大类的总成就点数(配置数据)
function AchievementManager:GetAchievementPointByBigType(bigType)
    return achievementData:GetAchievementPointByBigType(bigType)
end

--根基成就大类id获取已经获得的成就点数
function AchievementManager:GetAlreadyGetAchievementPointByBigType(bigType)
    return achievementData:GetAlreadyGetAchievementPointByBigType(bigType)
end

--根据成就大类id
--bigType:成就大类id
--groupId：成就子类id
function AchievementManager:GetAchievementListByType(bigType, groupId)
    return achievementData:GetAchievementListByType(bigType, groupId)
end

--根据成就id获取成就完成状态
function AchievementManager:GetAchievementStateById(id)
    return achievementData:GetAchievementStateById(id)
end

--获取成就完成次数
function AchievementManager:GetCompleteNum(id)
    return achievementData:GetCompleteNum(id)
end

--根据成就类型主键id判断该类型下是否有可领成就
function AchievementManager:IsCanGetByAchievementTypeId(id)
    return achievementData:IsCanGetByAchievementTypeId(id)
end

function AchievementManager:GetCanGetRewardAchievementId(menuData)
    return achievementData:GetCanGetRewardAchievementId(menuData)
end


function AchievementManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("Achievement Error:" .. error_id)
        return
    elseif action_id == action_config.ACHIEVEMENT_LIST_REQ then--2701 成就列表信息
        -- LoggerHelper.Error("2701 == " .. PrintTable:TableToStr(args))
        achievementData:InitServerData(args)
        if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ACHIEVEMENT) then
            self._achievementNode:CheckStrengthen(achievementData:GetRedPoint())
        end        
        --LoggerHelper.Error("achievementData:GetRedPoint()=============" .. tostring(achievementData:GetRedPoint())) 
        EventDispatcher:TriggerEvent(GameEvents.ON_GET_ACHIEVEMENT_List_DATA)
    elseif action_id == action_config.ACHIEVEMENT_REFRESH then --2702 成就刷新信息
        -- LoggerHelper.Error("2702 == " .. PrintTable:TableToStr(args))
        achievementData:RefreshData(args)
        if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ACHIEVEMENT) then
            self._achievementNode:CheckStrengthen(achievementData:GetRedPoint())
        end        
        --LoggerHelper.Error("achievementData:GetRedPoint()=============" .. tostring(achievementData:GetRedPoint())) 
        EventDispatcher:TriggerEvent(GameEvents.ACHIEVEMENT_REFRESH)
    elseif action_id == action_config.ACHIEVEMENT_COMMIT then   --2703 成就奖励获取信息
        -- LoggerHelper.Error("2703 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ON_GET_ACHIEVEMENT_REWARD_COMPLETE, args[1])
    else
        LoggerHelper.Error("Achievement action id not handle:" .. action_id)
    end
end

--2701 获取成就列表信息
function AchievementManager:GetAchievementListReq()
    GameWorld.Player().server.achievement_action_req(action_config.ACHIEVEMENT_LIST_REQ, "")
end

--2703 获取成就奖励（服务端通讯）
function AchievementManager:GetAchievementReward(id)
    if self._commitList[id] == nil then
        self._commitList[id] = 1
        GameWorld.Player().server.achievement_action_req(action_config.ACHIEVEMENT_COMMIT, tostring(id))
    end
    -- CommonWaitManager:BeginWaitLoading(WaitEvents.WORLD_BOSS_ENTER)
end

--处理成就列表信息
--id = {1={事件id = 次数},2=状态}
function AchievementManager:HandleList(args)
    achievementData:InitData(args)
end

AchievementManager:Init()
return AchievementManager