local FlyTriggerManager = {}
local GUIManager = GameManager.GUIManager
local GameObject = UnityEngine.GameObject
local GameSceneManager = GameManager.GameSceneManager

function FlyTriggerManager:Init()
	EventDispatcher:AddEventListener(GameEvents.TriggerFly,function (isStart,skillId,face,pos,showUI,endPosId)
		self:OnTriggerFly(isStart,skillId,face,pos,showUI,endPosId)
	end)
end

function FlyTriggerManager:OnTriggerFly(isStart,skillId,face,pos,showUI,endPosId)
	if isStart == 1 then
		if showUI == 1 then
			if GameWorld.Player().level == 1 then
				GUIManager.ShowPanel(PanelsConfig.Trigger,{skillId,face,pos,endPosId})
			end
		else
			self:OnStart(skillId,face,pos,endPosId)
		end
	else
		--LoggerHelper.Error("OnEnd")
		--self:OnEnd()
	end
end

function FlyTriggerManager:OnStart(skillId,face,pos,endPosId)
	self._endPosId = endPosId
	GUIManager.ClosePanel(PanelsConfig.Stick)
	GUILayerManager.HideAllLayers()
	GUIManager.SetInCGCameraCulling(true)
	GameWorld.SetMainCameraToPlayer()
	GameWorld.SetPlayerSyncPosState(false)
	local player = GameWorld.Player()
	player:SetActorControllerEnabled(false)
	player:Teleport(pos)
	player:SetFace(face)
	player:PlaySkill(skillId)
	player:PackUpWeapon(1)
end

function FlyTriggerManager:OnEnd()
	local player = GameWorld.Player()
	GameWorld.SetPlayerSyncPosState(true)
	GameWorld.ResetMainCameraFromPlayer()
	player:SetActorControllerEnabled(true)
	GUILayerManager.ShowAllLayers()
	GUIManager.SetInCGCameraCulling(false)
	GUIManager.ShowPanel(PanelsConfig.Stick)
	player:PackUpWeapon(0)

	self:CheckEndPosition()
end

--检查结束位置
function FlyTriggerManager:CheckEndPosition()
	if self._endPosId then
		local endPosAction = GameSceneManager:GetActionByID(tonumber(self._endPosId))
		--LoggerHelper.Error("self._endPosId"..self._endPosId)
		if endPosAction then
			local player = GameWorld.Player()
			local endPos = endPosAction.pos
			local dis =	Vector3.Distance(endPos, player:GetPosition())
			--LoggerHelper.Error("dis"..dis)
			if dis > 1 then
				player:Teleport(endPos)
				self._endPosId = nil
			-- else
			-- 	player:Teleport(endPos)
			-- 	self._endPosId = nil
			end
		end
	end
end
-- function FlyTriggerManager:OnStart()
-- 	self._waitForStart = true
-- 	local path = "Global/fly.prefab"
-- 	GameWorld.GetGameObject(path, 
--         function(go)
--         	self._flyAnim = go
--             FlyTriggerManager:OnPrefabLoaded(go)
--         end
--     )
-- end

-- function FlyTriggerManager:OnPrefabLoaded(go)
-- 	--LoggerHelper.Error("FlyTriggerManager:OnPrefabLoaded")
-- 	GameObject.DontDestroyOnLoad(go)
-- 	if GameWorld.Player().actorHasLoaded then
-- 		self:CheckStartFly()
-- 	end
-- end

-- function FlyTriggerManager:CheckStartFly()
-- 	if self._waitForStart then
-- 		self._waitForStart = false
-- 		local bodyTrans = UIComponentUtil.FindChild(self._flyAnim, "fly_body").transform
-- 		local player = GameWorld.Player()
-- 		local playerTrans = player:GetTransform()

-- 		playerTrans:SetParent(bodyTrans)
-- 		player:SetCurWing(1016002)
-- 		--LoggerHelper.Error("CheckStartFly11")
-- 		player:AddBuff(65100)
-- 		--LoggerHelper.Error("CheckStartFly22")
-- 		--player:SetGravity(0)
-- 		playerTrans.localPosition = Vector3.New(0,0,0)
-- 		playerTrans.rotation = Vector3.New(80,0,0)
-- 		GameWorld.HideMainCamera()
-- 	end
-- end

-- function FlyTriggerManager:OnEnd()
-- 	local player = GameWorld.Player()
-- 	local playerTrans = player:GetTransform()
-- 	playerTrans:SetParent(nil)
-- 	player:RemoveBuffer(65100)
-- 	player:SetCurWing(0)
-- 	--player:PlaySkill(65005)
-- 	GameWorld.Destroy(self._flyAnim)
-- 	GameWorld.ShowMainCamera()
-- end

FlyTriggerManager:Init() 
return FlyTriggerManager