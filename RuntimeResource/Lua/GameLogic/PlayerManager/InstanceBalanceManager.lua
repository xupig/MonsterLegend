local InstanceBalanceManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local GUIManager = GameManager.GUIManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig

function InstanceBalanceManager:Init()
    self._time = 5
    self:InitCallbackFunc()
end

function InstanceBalanceManager:InitCallbackFunc()
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function InstanceBalanceManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function InstanceBalanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function InstanceBalanceManager:OnLeaveMap(mapId)
    self._callBack = nil
    self:ClearTimer()
end

function InstanceBalanceManager:ShowSweepBalance(serverData, callBack)
    local balanceData = PlayerDataManager.instanceBalanceData
    balanceData:UpdateData(serverData)
    self._callBack = callBack
    self:ShowBalancePanel(true)
end

function InstanceBalanceManager:ShowPvpBalance(serverData, defeat, stageScore, stageInfo, newStageIcon, stageChangeText, callBack)
    local balanceData = PlayerDataManager.instanceBalanceData
    balanceData:UpdateData(serverData)
    balanceData:SetDefeat(defeat)
    balanceData:SetStageScore(stageScore)
    balanceData:SetStageInfo(stageInfo)
    balanceData:SetNewStageIcon(newStageIcon)
    balanceData:SetStageChangeText(stageChangeText)
    self._callBack = callBack
    
    self:ShowBalancePanel()
end

function InstanceBalanceManager:ShowBalance(serverData, callBack)
    -- if GameWorld.Player().autoFight then
    --     GameWorld.Player():ChangeAutoFight()
    -- end
    local balanceData = PlayerDataManager.instanceBalanceData
    balanceData:UpdateData(serverData)
    self._callBack = callBack
    
    local sceneType = balanceData:GetSceneType()
    local result = balanceData:GetResult()
    if (sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE or sceneType == SceneConfig.SCENE_TYPE_GOLD or
        sceneType == SceneConfig.SCENE_TYPE_MY_BOSS) and
        result then
        self:AddTimer()
    else
        self:ShowBalancePanel()
    end
end

function InstanceBalanceManager:AddTimer()
    self._timer = TimerHeap:AddSecTimer(self._time, 0, 0, function()
        self:ShowBalancePanel()
    end)
end

function InstanceBalanceManager:ShowBalancePanel(isSweep)
    local data = {}
    data.instanceBalanceData = PlayerDataManager.instanceBalanceData
    data.callBack = self._callBack
    data.sweep = isSweep
    GUIManager.ShowPanel(PanelsConfig.InstBalance, data)
end

function InstanceBalanceManager:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end


InstanceBalanceManager:Init()
return InstanceBalanceManager
