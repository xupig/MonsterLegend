local InvestManager = {}

local GUIManager = GameManager.GUIManager
local action_config = GameWorld.action_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local InvestData = PlayerManager.PlayerDataManager.investData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareDayRewardDataHelper = GameDataHelper.WelfareDayRewardDataHelper
local WelfareNoticeRewardDataHelper = GameDataHelper.WelfareNoticeRewardDataHelper
local WelfareLevelUpDataHelper = GameDataHelper.WelfareLevelUpDataHelper
local public_config = GameWorld.public_config
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestVipDataHelper = GameDataHelper.InvestVipDataHelper
local TotalChargeDataHelper = GameDataHelper.TotalChargeDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local AddChargeDays = 7
function InvestManager:Init()
    self._checkopenfun = function ()self:CheckInvestAdd()end
    self._addevenfun = function()self:AddChargeEvent() end
end

function InvestManager:HandleData(action_id, error_code, args)
    if error_code == 0 then
        if action_id == action_config.INVEST_VIP_PAY or action_id == action_config.INVEST_VIP_GET_REWARD then
            EventDispatcher:TriggerEvent(GameEvents.VIP_INVEST_UPDATE)
            self:RefreshRed()
            EventDispatcher:TriggerEvent(GameEvents.InvestRedUpdate)
        elseif action_id == action_config.LV_INVEST_PAY or action_id == action_config.LV_INVEST_GET_REWARD then
            EventDispatcher:TriggerEvent(GameEvents.INVEST_LEVEL_UPDATE)
            self:RefreshRed()
            EventDispatcher:TriggerEvent(GameEvents.InvestRedUpdate)
        elseif action_id == action_config.INVESTMENT_PURCHASE_MONTH_CARD_REQ or action_id == action_config.INVESTMENT_MONTH_CARD_DAILY_REFUND_REQ then
            EventDispatcher:TriggerEvent(GameEvents.INVEST_MONTH_UPDATE)
            self:RefreshRed()
            EventDispatcher:TriggerEvent(GameEvents.InvestRedUpdate)
        elseif action_id == action_config.CUMULATIVE_CHARGE_REFUND_REQ then
            EventDispatcher:TriggerEvent(GameEvents.INVEST_ADD_UPDATE)
            self:RefreshRed()   
            EventDispatcher:TriggerEvent(GameEvents.InvestRedUpdate)         
        end
    else
        if action_id == action_config.INVEST_VIP_PAY then
            if error_code==15 then
                local d = {}
                local data = {}
                d.id = MessageBoxType.Tip
                d.value =  data
                data.confirmCB=function()GUIManager.ClosePanel(PanelsConfig.MessageBox) GUIManager.ShowPanel(PanelsConfig.VIP) end
                data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
                data.text = LanguageDataHelper.CreateContent(81670)
                GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
            end
        end
    end

end

-----------------------------------------------------返回---------------------------------------------------------------------------------------------------

function InvestManager:OnPlayerEnterWorld()
    self:EnterFirstRed(true)
    self:AddEventListeners()
    -- self:RefreshRed()
    self:FirstEnterRed()
    self:CheckInvestAdd()
end

function InvestManager:FirstEnterRed()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_VIP_INVEST, self:RefreshRedInvestStateFun()) 
end

function InvestManager:AddEventListeners()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._checkopenfun)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_AMOUNT, self._addevenfun)	
end

function InvestManager:RefreshRed()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MOUTH_INVEST, self:RefreshMonthRed())   
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_LEVEL_INVEST, self:RefreshLevelRed())   
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_VIP_INVEST, self:RefreshVipRed())   
    self:AddChargeEvent()
end

function InvestManager:AddChargeEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_CUMULATE, self:RefreshAddRed())  
end

function InvestManager:OnPlayerLeaveWorld()
    self:RemoveEventListeners()
end


function InvestManager:RemoveEventListeners()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._checkopenfun)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CUMULATIVE_CHARGE_AMOUNT, self._addevenfun)	
end


function InvestManager:RefreshRedInvestStateFun()
    return self._enterRed or self:RefreshMonthRed() or self:RefreshLevelRed() or self:RefreshVipRed() or self:RefreshAddRed()
end
-- 月卡红点
function InvestManager:RefreshMonthRed()
    return not InvestData:IsGetMonthToday()
end

-- 等级红点
function InvestManager:RefreshLevelRed()
    local entity = GameWorld.Player()
    local recordlists = entity.lv_invest_record or {}
    local type = entity.lv_invest_type
    local level = entity.level
    if type~=0 then
        local cfg = InvestLevelDataHelper:GetCfgByType(type)
        for k,v in pairs(cfg) do
            if not recordlists[v.level] then
                if level>=InvestLevelDataHelper:GetLevel(v.id) and type==v.type then                 
                    return true
                end
            end
        end
        return false
    end
    return false
end

-- vip投资红点
function InvestManager:RefreshVipRed()
    local week=1
    local index=1
    week,index = InvestData:GetVipInvestIndex()
    local entity = GameWorld.Player()
    local rewardlists = entity.invest_vip_get_reward_days or {}
    local worldlevel = entity.invest_vip_world_level or 1
    local offvalve = InvestVipDataHelper:GetCfgTypeIndex(worldlevel)-1
    local x = 1    
    for i=index+offvalve,index+6+offvalve do
        if rewardlists[index+x-1]==0 then
            return true
        end
        x = x+1
    end
    return false
end

-- 开服累充红点
function InvestManager:RefreshAddRed()
    if InvestData:IsVipAddClosed() then
        return false
    else
        local ids = TotalChargeDataHelper:GetAllId()
        local enity = GameWorld.Player()
        local vocation = math.floor(enity.vocation/100)
        local record = enity.cumulative_charge_refund_info or {}
        local amount = enity.cumulative_charge_amount or 0
        local cfg = TotalChargeDataHelper:GetRewardsByVocation(vocation)
        for k,v in ipairs(cfg) do
            local cost = v.cost
            if record[v.id]~=1 and cost<=amount then
                return true
            end
        end
    end
    return false
end
-------------------------------------------------------------------------------------------------------------------------------


function InvestManager:EnterFirstRed(state)
    self._enterRed = state
end


---------------------------------------------请求-------------------------------------------------------------------------------------------------------------------

--领取vip投资奖励
function InvestManager:RequestInestGetReward(reward_day)
	GameWorld.Player().server.invest_vip_action_req(action_config.INVEST_VIP_GET_REWARD,reward_day)
end

--vip投资
function InvestManager:RequestVipInest()
	GameWorld.Player().server.invest_vip_action_req(action_config.INVEST_VIP_PAY,'0')
end


--领取等级投资奖励
function InvestManager:RequestLevelIneGetReward(reward_level)
	GameWorld.Player().server.lv_invest_action_req(action_config.LV_INVEST_GET_REWARD,reward_level)
end

--等级投资
function InvestManager:RequestLevelInest(invest_type)
	GameWorld.Player().server.lv_invest_action_req(action_config.LV_INVEST_PAY,invest_type)
end

--领取月卡投资奖励
function InvestManager:RequestMonthIneGetReward()
	GameWorld.Player().server.investment_action_req(action_config.INVESTMENT_MONTH_CARD_DAILY_REFUND_REQ,'')
end

--月卡投资
function InvestManager:RequestMonthInest()
	GameWorld.Player().server.investment_action_req(action_config.INVESTMENT_PURCHASE_MONTH_CARD_REQ,'')
end


--开服累充(领奖励)
function InvestManager:RequestAddIneGetReward(id)
	GameWorld.Player().server.cumulative_charge_action_req(action_config.CUMULATIVE_CHARGE_REFUND_REQ,id..'')
end


-- 等级投资按钮显示限制时间
function InvestManager:LevelInvestmentBtnTimeLimit()
	local d = DateTimeUtil.MinusServerDay(GameWorld.Player().create_time)
	if d['day']<7 then
		-- EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.LevelInvest, function()
			-- local data={}
    		-- data.firstTabIndex=5
			-- GUIManager.ShowPanel(PanelsConfig.VIP,data)
		-- end)
	else
		-- EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.LevelInvest)
	end
end


---------------------------------------------------------------------------------------------------------------------------------------------------------------------
function InvestManager:CheckInvestAdd()
    if not InvestData:IsVipAddClosed() then
        local createtime = GameWorld.Player().create_time
        local dateTime = DateTimeUtil.SomeDay(createtime)
        local _des = DateTimeUtil:DateTimeToSecond(dateTime)
        local s = DateTimeUtil.MinusSec(createtime)
        local total = AddChargeDays*DateTimeUtil.OneDayTime-_des
        if s< total then
            local t = total - s
            self._timeraddid = TimerHeap:AddSecTimer(t, 0, 0,function ()
                self:CheckInvestEven()
                TimerHeap:DelTimer(self._timeraddid)
                self._timeraddid = nil
            end)
        end
    else
        self:CheckInvestEven()
    end
end

function InvestManager:CheckInvestEven()
    local f = not InvestData:IsVipAddClosed()
    EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, public_config.FUNCTION_ID_OPEN_CUMULATE,f)
end


InvestManager:Init()
return InvestManager
