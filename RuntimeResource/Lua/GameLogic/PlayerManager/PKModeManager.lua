--pk模式
local PKModeManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FloatTextType = GameConfig.EnumType.FloatTextType

local ModeToLanguageId = {[0]= 70054, [1]= 70055, [2]= 70056}

function PKModeManager:Init()
end

function PKModeManager:OnPlayerEnterWorld()

end

function PKModeManager:OnPlayerLeaveWorld()

end

function PKModeManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("PKModeManager "..action_id)
    --LoggerHelper.Log(args)
end

function PKModeManager:RefreshPKMode(changeValue)
    if changeValue ~= nil then
        GUIManager.ShowText(FloatTextType.Down2Up, LanguageDataHelper.GetContent(ModeToLanguageId[GameWorld.Player().pk_mode]))
    end
end

-------------------------------------客户端请求-----------------------------------
--切换模式
function PKModeManager:ChangeModeReq(mode)
    local params = string.format("%d", mode)
    GameWorld.Player().server.pk_mode_req(action_config.PK_MODE_SET_MODE, params)
end


PKModeManager:Init()
return PKModeManager