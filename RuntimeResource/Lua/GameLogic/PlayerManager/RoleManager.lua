require "PlayerManager/PlayerDataManager"
require "PlayerManager/PlayerData/CombatData"

local RoleManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local combatData = PlayerDataManager.combatData
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local NobilityDataHelper = GameDataHelper.NobilityDataHelper
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local bagData = PlayerManager.PlayerDataManager.bagData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function RoleManager:Init()
    self:InitNobilityItem()

    self._refreshNobilityRedPointState = function() self:RefreshNobilityRedPointState() end
end

function RoleManager:OnPlayerEnterWorld()
    self:AttriGetReq()

    EventDispatcher:AddEventListener(GameEvents.Refresh_Nobility_Red_Point_State, self._refreshNobilityRedPointState)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._refreshNobilityRedPointState)

    self._nobilityNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.NOBILITY_UPGRADE, public_config.FUNCTION_ID_NOBILITY)--爵位升级
    self:RefreshNobilityRedPointState()
end

function RoleManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Nobility_Red_Point_State, self._refreshNobilityRedPointState)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_FIGHT_FORCE, self._refreshNobilityRedPointState)
end

function RoleManager:AttriGetReq()
    GameWorld.Player().server.attri_get_req()
end

function RoleManager:HandleData(luaTable)
    combatData:RefreshData(luaTable)
end

function RoleManager:CanUpgradeLevel()
    local player = GameWorld.Player()
    local level = GlobalParamsHelper.GetParamValue(659)    
    if player.change_four_succ == 0 and 
        player.level == level and 
        player.money_exp >= tonumber(RoleDataHelper.GetCurLevelExp(level)) then
            return false
    end
    return true
end

----------------------------爵位-----------------------------
function RoleManager:InitNobilityItem()
    self._nobilityItem = NobilityDataHelper:GetNobilityItems()
end

--是否为爵位道具
function RoleManager:IsNobilityItem(itemId)
    if self._nobilityItem[itemId] then
        return true
    end
    return false
end

function RoleManager:RefreshNobilityRedPointState()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_NOBILITY) then
        return
    end
    local curNobility = GameWorld.Player().nobility_rank
    local curCfg = NobilityDataHelper.GetNobilityCfg(curNobility) 
    local nextCfg = NobilityDataHelper.GetNobilityCfg(curNobility+1)
    if nextCfg then
        local canUpgrade = true
        for itemId,count in pairs(curCfg.nobility_levelup_cost) do
            local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
            if count > bagNum then
                canUpgrade = false
            end
        end
        local needFightPower = curCfg.nobility_levelup_need
        local curFightPower = GameWorld.Player().fight_force
        if needFightPower > curFightPower then
            canUpgrade = false
        end
        self._nobilityNode:CheckStrengthen(canUpgrade)
    else
        self._nobilityNode:CheckStrengthen(false)
    end

end

function RoleManager:UpgradeNobility()
	GameWorld.Player().server.upgrade_nobility_req()
end

function RoleManager:OnNobilityUpgrade()
	EventDispatcher:TriggerEvent(GameEvents.NOBILITY_UPDATE)
end


RoleManager:Init()
return RoleManager