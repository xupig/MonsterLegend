--坐骑
local RideManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local bagData = PlayerManager.PlayerDataManager.bagData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function RideManager:Init()
end

function RideManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("RideManager "..action_id)
    --LoggerHelper.Log(args)
end

function RideManager:HandleResp(act_id, code, args)
    if code > 0 then
        return
    end
    if act_id == action_config.RIDE_ACTIVE then
        EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_RIDE_ACTIVE)
    end
end

function RideManager:RefreshActivePos(oldValue, curValue)
    EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_RIDE_DATA)
end

function RideManager:RefreshRidingPos(oldValue, curValue)
    EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_RIDE_STATE)
end

function RideManager:RefreshPkgRideSize()
    EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_RIDE_DATA)
end


function RideManager:GetRideData()
    local data = bagData:GetPkg(public_config.PKG_TYPE_RIDE)
    local result = {} --{{pos,{id}}}
    local count = 0
    for pos, v in pairs(data._serverData) do
        table.insert(result, {pos, v})
        count = count + 1
    end
    for i=1,data:GetBagCurVolumn() - count do
        table.insert(result, {0}) --0表示空
    end
    local configData = GlobalParamsHelper.GetParamValue(375)
    if configData[GameWorld.Player().pkg_ride_buy_count + 1] ~= nil then
        table.insert(result, {-1}) -- -1表示下一个待开启
    end
    return result
end

function RideManager:GetUnlockNextRideCost()
    local configData = GlobalParamsHelper.GetParamValue(375)
    local result = 9999
    if configData[GameWorld.Player().pkg_ride_buy_count + 1] ~= nil then
        result = configData[GameWorld.Player().pkg_ride_buy_count + 1]
    end
    return result
end

-------------------------------------客户端请求-----------------------------------
--激活坐骑
function RideManager:RideActive(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.ride_action_req(action_config.RIDE_ACTIVE, params)
end

--购买格子
function RideManager:RideBuy()
    GameWorld.Player().server.ride_action_req(action_config.RIDE_BUY, "")
end

function RideManager:RideDel(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.ride_action_req(action_config.RIDE_DEL, params)
end

function RideManager:StartRide(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.ride_action_req(action_config.RIDE_RIDE, params)
end

function RideManager:EndRide()
    GameWorld.Player().server.ride_action_req(action_config.RIDE_UNRIDE, "")
end


RideManager:Init() 
return RideManager