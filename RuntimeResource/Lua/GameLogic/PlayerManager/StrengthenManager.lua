local StrengthenManager = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager

function StrengthenManager:Init()
    -- LoggerHelper.Error("StrengthenManager:Init()")
    self._strengthenDatas = {}
    self:InitCallbackFunc()
end

function StrengthenManager:InitCallbackFunc()
    self._removeData = function(data)self:RemoveData(data) end
    self._addData = function(data)self:AddData(data) end
end

function StrengthenManager:OnPlayerEnterWorld()
    -- LoggerHelper.Error("StrengthenManager:OnPlayerEnterWorld()")
    EventDispatcher:AddEventListener(GameEvents.REMOVE_STRENGTH_LIST_CONTENT, self._removeData)
    EventDispatcher:AddEventListener(GameEvents.ADD_STRENGTH_LIST_CONTENT, self._addData)
end

function StrengthenManager:OnPlayerLeaveWorld()
    self._strengthenDatas = {}
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_STRENGTH_LIST_CONTENT, self._removeData)
    EventDispatcher:RemoveEventListener(GameEvents.ADD_STRENGTH_LIST_CONTENT, self._addData)
end

function StrengthenManager:GetStrengthenData()
    return self._strengthenDatas
end

function StrengthenManager:RemoveData(data)
    if table.isEmpty(self._strengthenDatas) then
        LoggerHelper.Error("StrengthenManager:RemoveData(data) data empty")
        return
    end
    local t = {}
    local flag = false
    for i,v in ipairs(self._strengthenDatas) do
        if v == data then
            flag = true
        else
            table.insert(t, v)
        end
    end
    self._strengthenDatas = t
    if flag then
        if table.isEmpty(self._strengthenDatas) then
            GUIManager.ClosePanel(PanelsConfig.Strengthen)
        else
            self:TriggerStrengthenChangeEvent()
        end
    end
end

function StrengthenManager:AddData(data)
    if data and (not table.containsValue(self._strengthenDatas, data)) then
        table.insert(self._strengthenDatas, data)
        self:TriggerStrengthenChangeEvent()
    else
        LoggerHelper.Error("StrengthenManager:AddData(data) has same data" .. tostring(data))
    end
end

function StrengthenManager:TriggerStrengthenChangeEvent()
    GUIManager.ShowPanel(PanelsConfig.Strengthen)
    EventDispatcher:TriggerEvent(GameEvents.STRENGTHEN_CHANGE)
end

StrengthenManager:Init()
return StrengthenManager