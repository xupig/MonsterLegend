local PersonalBossManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local personalBossData = PlayerDataManager.personalBossData
local PersonalBossDataHelper = GameDataHelper.PersonalBossDataHelper
local GameSceneManager = GameManager.GameSceneManager
local InstanceCommonManager  = PlayerManager.InstanceCommonManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local PlayerActionManager = GameManager.PlayerActionManager
local InstanceManager = PlayerManager.InstanceManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskCommonManager = PlayerManager.TaskCommonManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function PersonalBossManager:Init()
    self:InitCallbackFunc()
    self._isShow = false
end

function PersonalBossManager:InitCallbackFunc()
    self._onEnterMap = function(mapId) self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId) self:OnLeaveMap(mapId) end
    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
    self._onVIPLevelChange = function() self:OnVIPLevelChange() end
end

function PersonalBossManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    EventDispatcher:AddEventListener(GameEvents.On_VIP_Level_Change, self._onVIPLevelChange)

    self._showRemainCountRedPoint = false
    self:RefreshRemainCountRedPointState()
end

function PersonalBossManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    EventDispatcher:RemoveEventListener(GameEvents.On_VIP_Level_Change, self._onVIPLevelChange)
end

function PersonalBossManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        if action_id == action_config.PERSONAL_BOSS_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.PERSONAL_BOSS_ENTER)
        end
        return
    end
    
    if action_id == action_config.PERSONAL_BOSS_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.PERSONAL_BOSS_ENTER)
    elseif action_id == action_config.PERSONAL_BOSS_NOTIFY_ROOM_STAGE then
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        -- [3] = cfg_id,       --个人boss表id
        self:ShowInstanceInfo(args[3])
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = GlobalParamsHelper.GetParamValue(521)
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            local instanceTime = GlobalParamsHelper.GetParamValue(522)
            InstanceCommonManager:StartInstanceCountDown(instanceTime, args[2])

            GameWorld.Player():StartAutoFight()
        end
    elseif action_id == action_config.PERSONAL_BOSS_SETTLE then
        local seconds = GlobalParamsHelper.GetParamValue(728)
        local startTime =  DateTimeUtil.GetServerTime()
        local cb = function() InstanceManager:OnInstanceExit() end
        InstanceCommonManager:ShowLeaveCountDown(seconds, startTime, cb)
    end
end

function PersonalBossManager:RefreshPersonalBossInfo(changeValue)
    personalBossData:RefreshPersonalBossInfo(changeValue)
    if changeValue then
        self:RefreshRemainCountRedPointState()
        EventDispatcher:TriggerEvent(GameEvents.OnPersonalBossRefreshBossInfo)
    end
end

-------------------------------------数据处理------------------------------------
function PersonalBossManager:ShowInstanceInfo(id)
    if not self._isShow then
        id = id or 1
        local bossData = personalBossData:GetBossInfo(id)
        local name = LanguageDataHelper.GetContent(bossData:GetBossName())
        local title = LanguageDataHelper.GetContent(56036)
        local target = LanguageDataHelper.CreateContentWithArgs(56044, {["0"]= name})
        local desc = LanguageDataHelper.GetContent(56045)
        InstanceCommonManager:ShowInstanceInfo(target, desc, title)
        self._isShow = true
    end
end

function PersonalBossManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_MY_BOSS then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        InstanceManager:EnterInstance()
        GameWorld.Player():StopAutoFight()
    end    
end

function PersonalBossManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_MY_BOSS then
        self._isShow = false
        GameWorld.Player():StopAutoFight()
    end
end

function PersonalBossManager:GetRemainCount()
    return personalBossData:GetRemainCount()
end

function PersonalBossManager:GetMaxCount()
    return personalBossData:GetMaxCount()
end

function PersonalBossManager:GetAllBossInfo()
    return personalBossData:GetAllBossInfo()
end

function PersonalBossManager:GotoBoss(bossData)
    local cb = function() self:ConfirmGotoBoss(bossData) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function PersonalBossManager:ConfirmGotoBoss(bossData)
    self:OnBossEnter(bossData:GetId())
end

function PersonalBossManager:GetLastEnterTime()
    return personalBossData:GetLastEnterTime()
end
-----------------------------------红点------------------------------
function PersonalBossManager:OnVIPLevelChange()
    EventDispatcher:TriggerEvent(GameEvents.OnPersonalBossRefreshBossInfo)
    self:RefreshRemainCountRedPointState()
end

function PersonalBossManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_PERSON_BOSS then
        self:RefreshRemainCountRedPointState()
    end
end

function PersonalBossManager:RefreshRemainCountRedPointState()
    local isFuncOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PERSON_BOSS)
    if not isFuncOpen then
        return
    end

    local minVipLevel = GlobalParamsHelper.GetParamValue(508)
    self:SetRemainCountRedPointState(self:GetRemainCount()>0 and GameWorld.Player().vip_level >= minVipLevel)
end

function PersonalBossManager:SetRemainCountRedPointState(state)
    if self._showRemainCountRedPoint ~= state then
        self._showRemainCountRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_PERSON_BOSS, self:GetRemainCountRedPointState())
    end
end

function PersonalBossManager:GetRemainCountRedPointState()
    return self._showRemainCountRedPoint
end
-------------------------------------客户端请求-----------------------------------
function PersonalBossManager:OnBossEnter(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.PERSONAL_BOSS_ENTER)
    local params = string.format("%d", id)
    GameWorld.Player().server.personal_boss_action_req(action_config.PERSONAL_BOSS_ENTER, params)
end

PersonalBossManager:Init()
return PersonalBossManager
