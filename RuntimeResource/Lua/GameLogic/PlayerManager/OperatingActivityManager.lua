local OperatingActivityManager = {}
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local InstanceManager = PlayerManager.InstanceManager
local PanelsConfig = GameConfig.PanelsConfig
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local SceneConfig = GameConfig.SceneConfig
local MapDataHelper = GameDataHelper.MapDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local TaskCommonManager = PlayerManager.TaskCommonManager
local bagData = PlayerManager.PlayerDataManager.bagData
local CommonWaitManager = PlayerManager.CommonWaitManager
-- 	   CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C = 4, --周累充
--     CHANNEL_ACTIVITY_ID_WEEK_LOGIN      = 5, --周活动登录
--     CHANNEL_ACTIVITY_ID_FIREWORK        = 6, --庆典烟花
--     CHANNEL_ACTIVITY_ID_HOLY_WEAPON     = 7, --神兵现世
--     CHANNEL_ACTIVITY_ID_EXCHANGE        = 8, --庆典兑换
--     CHANNEL_ACTIVITY_ID_DOUBLE = 9, --周双倍
--     CHANNEL_ACTIVITY_ID_BATTLE_SOUL     = 10, --战场之魂
--	   CHANNEL_ACTIVITY_ID_HI                  = 11, --hi点
 -- CHANNEL_ACTIVITY_KEY_BEGIN_TIME      = 1,
 --    CHANNEL_ACTIVITY_KEY_END_TIME        = 2,
 --    CHANNEL_ACTIVITY_KEY_OPEN_SERVER_DAY = 3,

function OperatingActivityManager:Init()
	self._onActivityTimeChangeCb = function ()
		self:OnActivityTimeChange()
	end

	self._onLevelChangeCb = function ()
		self:OnLevelChange()
	end
	
	self._openFun = function ()
		GUIManager.ShowPanel(PanelsConfig.OperatingActivity)
	end

	self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end

	self._zeroClockCb = function ()
		self:OnActivityTimeChange()
		self:SetInstanceDoubleState()
		self:CheckEnd()
		self:CheckFirstActivityOpen()
	end

	-- self._expDoubleCb = function ()

	-- 	GUIManager.ShowText(2,LanguageDataHelper.CreateContent(81640))
	-- end

	self._instanceDoubleCb = function ()
		GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_CHANNEL_ACTIVITY_DOUBLE)
		--GUIManager.ShowText(2,LanguageDataHelper.CreateContent(81641))
	end
end

function OperatingActivityManager:OnPlayerEnterWorld()
	self._operatingActivityOpenState = {}
	self._operatingActivityRedPointState = {}
	if self:CheckOpen() then
		self._funcOpen = true
	else
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCb)
	end
	self:OnActivityTimeChange()
	self:SetInstanceDoubleState()
	EventDispatcher:AddEventListener(GameEvents.Refresh_Channel_Activity_Times,self._onActivityTimeChangeCb)
	EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._zeroClockCb)
	EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)

	self:RefreshHiRedPoint()
	self:RefreshCelebrationRedPoint()
	self:RefreshLoginGiftRedPoint()
	self:RefreshChargeGiftRedPoint()
end

function OperatingActivityManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Channel_Activity_Times,self._onActivityTimeChangeCb)
	EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._zeroClockCb)
	EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
	if not self._funcOpen then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCb)
	end
	self:RemoveMenuIcon()
end

--嗨点红点
function OperatingActivityManager:RefreshHiRedPoint()
	if not self._funcOpen or not self:GetOpenState(public_config.CHANNEL_ACTIVITY_ID_HI) then
		return
	end
	local state = false
	if self._hiCfgListData == nil then
		self._hiCfgListData = OperatingActivityDataHelper:GetHiPointRewardCfgList()
	end

	local hiRewardInfo = GameWorld.Player().hi_point_reward
	local hiPoint = GameWorld.Player().hi_point
	for i=1,#self._hiCfgListData do
		local cfg = self._hiCfgListData[i]
		local getState = hiRewardInfo[cfg.id]
		if getState == nil and hiPoint >= cfg.cost_point then
			state = true
			break
		end
	end

	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CHANNEL_ACTIVITY_HI, state)  
end

--庆典道具刷新红点
function OperatingActivityManager:RefreshCelebrationRedPointByItem(itemId)
	if self._celebrationCostItems then
		for i=1,#self._celebrationCostItems do
			if self._celebrationCostItems[i] == itemId then
				self:RefreshCelebrationRedPoint()
			end
		end
	end
end

--庆典兑换红点
function OperatingActivityManager:RefreshCelebrationRedPoint()
	if not self._funcOpen or not self:GetOpenState(public_config.CHANNEL_ACTIVITY_ID_EXCHANGE) then
		return
	end

	local state = false
	if self._celebrationCfgListData == nil then
		self._celebrationCfgListData = OperatingActivityDataHelper:GetCelebrationCfgList()
	end

	local exchangeInfo = GameWorld.Player().exchange_info
	for i=1,#self._celebrationCfgListData do
		local itemEnough = false
		local cfg = self._celebrationCfgListData[i]
		for needCount,itemIds in pairs(cfg.cost) do
			if self._celebrationCostItems == nil then
				self._celebrationCostItems = itemIds
			end
			local bagCount = 0
			for i=1,#itemIds do
				bagCount = bagCount + bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemIds[i])
			end
			itemEnough = bagCount >= needCount
		end

		local alreadyGot = exchangeInfo[cfg.id] or 0
		local canGetCount = cfg.exchange - alreadyGot
		if canGetCount > 0 and itemEnough then
			state = true
			break
		end
	end

	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CHANNEL_ACTIVITY_EXCHANGE, state)  
end

--登录奖励红点
function OperatingActivityManager:RefreshLoginGiftRedPoint()
	if not self._funcOpen or not self:GetOpenState(public_config.CHANNEL_ACTIVITY_ID_WEEK_LOGIN) then
		return
	end
	local state = false
	LoggerHelper.Log(GameWorld.Player().week_login_info)
	for k,v in pairs(GameWorld.Player().week_login_info) do
		if k > 0 and v == 0 then
			state = true
		end
	end
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CHANNEL_ACTIVITY_WEEK_LOGIN, state)  
end

--累充红点
function OperatingActivityManager:RefreshChargeGiftRedPoint()
	if not self._funcOpen or not self:GetOpenState(public_config.CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C) then
		return
	end

	local state = false
	if self._chargeCfgListData == nil then
		self._chargeCfgListData = OperatingActivityDataHelper:GetTiringChargeCfgList()
	end

	local chargeInfo = GameWorld.Player().weekly_cumulative_charge_refund_info
	local curCharge = GameWorld.Player().weekly_cumulative_charge_amount
	for i=1,#self._chargeCfgListData do
		local cfg = self._chargeCfgListData[i]
		local getState = chargeInfo[cfg.id]
		local needCharge = cfg.cost
		if getState == nil and curCharge >= needCharge then
			state = true
			break
		end
	end
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WEEKLY_CUMULATIVE_C, state)  
end

--设置副本双倍状态
function OperatingActivityManager:SetInstanceDoubleState()
	self._instanceDoubleState = {}
	if self._operatingActivityOpenState[public_config.CHANNEL_ACTIVITY_ID_DOUBLE] then
		local activityTimes = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_DOUBLE]
		local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
		local whichDay = math.min(DateTimeUtil.GetDaysGoThrough(startTime),3) --现在是第几天
		local openTimeCfg = GlobalParamsHelper.GetParamValue(910)
		local openTime = openTimeCfg[whichDay]
		local nowDate = DateTimeUtil.Now()
		local now = DateTimeUtil.GetServerTime() 
		local timeleft = 0
		if whichDay > 1 then
			local cfgList = OperatingActivityDataHelper:GetDoubleRewardCfgList()
			for i=1,#cfgList do
				local rewardTimes = cfgList[i].day_reward[whichDay]
				if rewardTimes == 2 then
					local funcId = cfgList[i].follow[1][1]
					self._instanceDoubleState[funcId] = true
				end
			end
			timeleft = openTime[3]*3600 + openTime[4]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
			local endStamp = now + timeleft
			--LoggerHelper.Error("SetInstanceDoubleState"..timeleft)
			EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.OperatingActivityInstanceDouble,self._instanceDoubleCb,endStamp)
			EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OperatingActivityExpDouble)
		else
			if nowDate.hour < openTime[1] then
				timeleft  = openTime[1]*3600 + openTime[2]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
				--LoggerHelper.Error("SetInstanceDoubleStateAAAAAAAAAAAAAA"..timeleft)
				self._expDoubleReadyTimer =  TimerHeap:AddSecTimer(timeleft, 0, timeleft, function ()
					TimerHeap.DelTimer(self._expDoubleReadyTimer)
					self:SetInstanceDoubleState()
				end)
			else
				timeleft = openTime[3]*3600 + openTime[4]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
				local endStamp = now + timeleft
				EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.OperatingActivityExpDouble,self._instanceDoubleCb,endStamp)
			end
			EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OperatingActivityInstanceDouble)
		end
	else
		EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OperatingActivityExpDouble)
		EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OperatingActivityInstanceDouble)
	end
end

--获取副本双倍状态
function OperatingActivityManager:GetInstanceDoubleState(funcId)
	return self._instanceDoubleState[funcId]
end

--因为功能开启了显示按钮
function OperatingActivityManager:OnLevelChange()
	if self._showMenuIcon and self:CheckOpen() then
		self._funcOpen = true
		self:ShowMenu()
	end
end

--检查面板是否开启
function OperatingActivityManager:CheckOpen()
	return FunctionOpenDataHelper:CheckPanelOpen(402)
end

--检查是否关闭主界面图标
function OperatingActivityManager:CheckEnd()
	if self._lastActivityEndTime == nil then
		return
	end
	--先删除旧的计时
	local now = DateTimeUtil.GetServerTime()
	local endTimeLeft = self._lastActivityEndTime - now
	--LoggerHelper.Error("endTimeLeft："..endTimeLeft)
	if endTimeLeft > 0 and endTimeLeft < 86400 then
		if self._endTimer then
			TimerHeap:DelTimer(self._endTimer)
			self._endTimer = nil
		end
		self._endTimer =  TimerHeap:AddSecTimer(endTimeLeft, 0, endTimeLeft, function ()
			self:RemoveMenuIcon()
		end)
	end
end

--关闭主界面图标
function OperatingActivityManager:RemoveMenuIcon()
	if self._endTimer then
		TimerHeap:DelTimer(self._endTimer)
		self._endTimer = nil
	end
	EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OperatingActivity)
end
------------------------------------开启时间处理------------------------------------------
function OperatingActivityManager:OnActivityTimeChange()
	self._showMenuIcon = false
	local now = DateTimeUtil.GetServerTime()
	--LoggerHelper.Log(GameWorld.Player().channel_activity_times)
	for i=4,11 do
		local activityTimes = GameWorld.Player().channel_activity_times[i]
		if activityTimes then
			local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
			local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
			--活动还没开始
			if now < startTime then
				if self._firstActivityOpenTime == nil or self._firstActivityOpenTime > startTime then
					self._firstActivityOpenTime = startTime
				end
			end
			--活动已开始判断
			local b = endTime > now and now >= startTime
			self._operatingActivityOpenState[i] = b
			if b then
				--LoggerHelper.Error("OnActivityTimeChange")
				self._showMenuIcon = b
				if self._lastActivityEndTime == nil or self._lastActivityEndTime < endTime then
					self._lastActivityEndTime = endTime
				end
			end
		else
			self._operatingActivityOpenState[i] = false
		end
	end
	--要显示主菜单icon
	if self._showMenuIcon then
		if self._funcOpen then
			self:ShowMenu()
			self:CheckEnd()
		end
	--未开启
	else
		self:CheckFirstActivityOpen()
		self:RemoveMenuIcon()
	end
end

--检查是否需要计时开启主界面图标
function OperatingActivityManager:CheckFirstActivityOpen()
	if self._firstActivityOpenTime == nil then
		return
	end
	
	local now = DateTimeUtil.GetServerTime()
	local timeLeft = self._firstActivityOpenTime - now
	--LoggerHelper.Error("timeLeft"..timeLeft)
	if timeLeft > 0 and timeLeft < 86400 then
		--先删除旧的计时
		if self._firstOpenTimer then
			TimerHeap:DelTimer(self._firstOpenTimer)
			self._firstOpenTimer = nil
		end
		self._firstOpenTimer =  TimerHeap:AddSecTimer(timeLeft, 0, timeLeft, function ()
			TimerHeap.DelTimer(self._firstOpenTimer)
			self:OnActivityTimeChange()
		end)
	end
end

function OperatingActivityManager:ShowMenu()
	EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.OperatingActivity,self._openFun)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_OPERATING_ACTIVITY_FUNCTION_OPEN)
end

function OperatingActivityManager:GetOpenState(activityId)
	return self._operatingActivityOpenState[activityId]
end

--获取开启时间
function OperatingActivityManager:GetOpenTimeStr(activityId)
	local activityTimes = GameWorld.Player().channel_activity_times[activityId]
	if activityTimes then
		local tab = {}
		local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
		local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
		local startDate = DateTimeUtil.SomeDay(startTime)
		local endDate = DateTimeUtil.SomeDay(endTime)
		table.insert(tab,DateTimeUtil.FormatFullDate(startDate,true,false,true))
		table.insert(tab," ")
		table.insert(tab,DateTimeUtil.GetDateHourMinStr(startTime))
		table.insert(tab,"——")
		table.insert(tab,DateTimeUtil.FormatFullDate(endDate,true,false,true))
		table.insert(tab," ")
		table.insert(tab,DateTimeUtil.GetDateHourMinStr(endTime))
		return table.concat(tab)
	else
		return ""
	end
end

------------------------------------服务端返回------------------------------------------

-- 	   BATTLE_SOUL_ENTER                       = 10761, --请求进入
--     BATTLE_SOUL_NOTIFY_END_TIME             = 10762, --通知整个玩法结束的时间 ｛end_time｝
--     BATTLE_SOUL_NOTIFY_DAMAGE_RANK          = 10763, --通知伤害排行 {rank_1, rank_2,rank_3，...self_info}   rank_1 = {名次,name,damage}     self_info 放在最后一个
--     BATTLE_SOUL_NOTIFY_NEXT_BOSS_BORN_TIME  = 10764, --通知下只boss刷新时间
--     BATTLE_SOUL_NOTIFY_ALL_BOSS_DIE_TIME    = 10765, --所有boss都打完的时间戳,通知即将退出场景
function OperatingActivityManager:HandleResp(action_id, error_code, args)
	-- LoggerHelper.Log("OperatingActivityManager:HandleResp"..action_id)
	-- LoggerHelper.Log(args)
	if error_code > 0 then
		if action_id == action_config.BATTLE_SOUL_ENTER then
			-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
			CommonWaitManager:EndWaitLoading(WaitEvents.BATTLE_SOUL_ENTER)
		end
		return
	end
	
	if action_id == action_config.HOLY_WEAPON_RANK_GET_INFO then
		EventDispatcher:TriggerEvent(GameEvents.HOLY_WEAPON_RANK_UPDATE_INFO,args)
	elseif action_id == action_config.BATTLE_SOUL_NOTIFY_END_TIME then
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_END_TIME,args[1])
	elseif action_id == action_config.BATTLE_SOUL_NOTIFY_DAMAGE_RANK then
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_RANK,args)
	elseif action_id == action_config.BATTLE_SOUL_NOTIFY_NEXT_BOSS_BORN_TIME then
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_WAVE_TIME,args[1])
	elseif action_id == action_config.BATTLE_SOUL_NOTIFY_ALL_BOSS_DIE_TIME then
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_QUIT_TIME,args[1])
	elseif action_id == action_config.ACTIVITY_FIREWORK_EFFECT then
		self:HandleFirework()
	elseif action_id == action_config.BATTLE_SOUL_ENTER then
		-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
		CommonWaitManager:EndWaitLoading(WaitEvents.BATTLE_SOUL_ENTER)
		GameWorld.Player():StopAutoFight()
		GameWorld.Player():StopMoveWithoutCallback()
		PlayerActionManager:StopAllAction()
		TaskCommonManager:SetCurAutoTask(nil, nil)
	end
end

function OperatingActivityManager:HandleFirework()
	local fireworkId = GlobalParamsHelper.GetParamValue(913)
	GUIManager.ShowPanel(PanelsConfig.Firework, {fireworkId})
end

function OperatingActivityManager:UpdateLoginRewardState(change)
	if change then
		self:RefreshLoginGiftRedPoint()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_LOGIN_REWARD)
	end
end

function OperatingActivityManager:UpdateChargeRewardState(change)
	if change then
		self:RefreshChargeGiftRedPoint()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_CHARGE_REWARD)
	end
end

function OperatingActivityManager:UpdateCelebrationGetState(change)
	if change then
		self:RefreshCelebrationRedPoint()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_CELEBRATION_REWARD)
	end
end

function OperatingActivityManager:UpdateHiPointAward(change)
	if change then
		self:RefreshHiRedPoint()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_HI_POINT_REWARD)
	end
end

function OperatingActivityManager:UpdateHiPointActivityTimes(change)
	if change then
		self:RefreshHiRedPoint()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_HI_POINT_ACTIVITY)
	end
end

function OperatingActivityManager:OnEnterMap(mapId)
--    LoggerHelper.Error("ExpInstanceManager:OnEnterMap()" .. tostring(mapId))
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_BATTLE_SOUL then
		InstanceManager:EnterInstance()
		GameWorld.Player():StartAutoFight()
    end
end

function OperatingActivityManager:BuyFireworkComplete(action_id, error_code, args)
	if error_code == 0 then
		GUIManager.ShowPanel(PanelsConfig.Firework, {itemid})
	end
end

------------------------------------客户端请求------------------------------------------
--登录领奖
function OperatingActivityManager:RequestGetLoginAward(day)
	GameWorld.Player().server.week_login_action_req(action_config.WEEK_LOGIN_REWARD,tostring(day))
end

--累充领奖
function OperatingActivityManager:RequestGetChargeAward(index)
	GameWorld.Player().server.weekly_cumulative_charge_action_req(action_config.WEEKLY_CUMULATIVE_CHARGE_REFUND_REQ,tostring(index))
end

--庆典兑换
function OperatingActivityManager:RequestGetCelebrationAward(index)
	GameWorld.Player().server.exchange_action_req(action_config.EXCHANGE_REWARD,tostring(index))
end

--神兵现世信息
function OperatingActivityManager:RequestGetWeaponRankInfo()
	GameWorld.Player().server.holy_weapon_cost_action_req(action_config.HOLY_WEAPON_RANK_GET_INFO,"")
end

--购买烟花
function OperatingActivityManager:RequestBuyFirework()
	GameWorld.Player().server.activity_firework_action_req(action_config.ACTIVITY_FIREWORK_BUY,"")
end

--进入战场之魂
function OperatingActivityManager:RequestBattleSoulEnter()
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
	CommonWaitManager:BeginWaitLoading(WaitEvents.BATTLE_SOUL_ENTER)
	GameWorld.Player().server.battle_soul_action_req(action_config.BATTLE_SOUL_ENTER,"")
end

--嗨点领奖
function OperatingActivityManager:RequestGetHiAward(index)
	GameWorld.Player().server.hi_action_req(action_config.HI_REWARD_REQ,tostring(index))
end

OperatingActivityManager:Init() 
return OperatingActivityManager