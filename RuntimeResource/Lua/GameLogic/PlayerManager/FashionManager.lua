local FashionManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FashionDataHelper = GameDataHelper.FashionDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local fashionData = PlayerManager.PlayerDataManager.fashionData
local QualityType = GameConfig.EnumType.QualityType
local RoleDataHelper = GameDataHelper.RoleDataHelper
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function FashionManager:Init()

end

function FashionManager:OnPlayerEnterWorld()
    self._refreshFashionRedPoint = function(type) self:RefreshRedPointState(type) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Fashion_Red_Point, self._refreshFashionRedPoint)

    self._fashionClothUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.FASHION_CLOTH, public_config.FUNCTION_ID_FASHION)
    self._fashionWeaponUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.FASHION_WEAPON, public_config.FUNCTION_ID_FASHION_ARMS)
    self._fashionEffectUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.FASHION_EFFECT, public_config.FUNCTION_ID_FASHION_EFFECT)
    self._fashionPhotoUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.FASHION_PHOTO, public_config.FUNCTION_ID_FASHION_PHOTO)
    self._fashionChatUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.FASHION_CHAT, public_config.FUNCTION_ID_FASHION_CHAT)

    self._showClothRedPoint = false
    self._showWeaponRedPoint = false
    self._showEffectRedPoint = false
    self._showPhotoRedPoint = false
    self._showChatRedPoint = false

    self:RefreshRedPointState(public_config.FASHION_TYPE_CLOTHES)
    self:RefreshRedPointState(public_config.FASHION_TYPE_WEAPON)
    self:RefreshRedPointState(public_config.FASHION_TYPE_SPECIAL)
    self:RefreshRedPointState(public_config.FASHION_TYPE_PHOTO)
    self:RefreshRedPointState(public_config.FASHION_TYPE_CHAT)

    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._onFunctionOpen)
end

function FashionManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Fashion_Red_Point, self._refreshFashionRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._onFunctionOpen)
end

function FashionManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("FashionManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.FASHION_LOAD then
        local model = FashionDataHelper.GetFashionModel(args[1])
        local type = FashionDataHelper.GetFashionType(args[1])
        if type == public_config.FASHION_TYPE_CLOTHES then
            local headModel = FashionDataHelper.GetFashionHeadModel(args[1])
            GameWorld.Player():SetCurCloth(model,0,0,0,0,0)
            GameWorld.Player():SetCurHead(headModel,0,0,0,0,0)
        elseif type == public_config.FASHION_TYPE_WEAPON then
            GameWorld.Player():SetCurWeapon(model,0,0,0,0,0)
        elseif type == public_config.FASHION_TYPE_SPECIAL then
            GameWorld.Player():SetCurEffect(model,0,0)
        end
    elseif action_id == action_config.FASHION_UNLOAD then
        local type = FashionDataHelper.GetFashionType(args[1])
        if type == public_config.FASHION_TYPE_CLOTHES then
            GameWorld.Player():ShowCurClothAndHead()
        elseif type == public_config.FASHION_TYPE_WEAPON then
            GameWorld.Player():ShowCurWeapon()
        elseif type == public_config.FASHION_TYPE_SPECIAL then
            GameWorld.Player():SetCurEffect(0,0,0)
        end
    end
end

-----------------------------数据处理---------------------------
function FashionManager:RefreshFashionInfo(changeValue)
    fashionData:RefreshFashionInfo(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Info)
end

function FashionManager:RefreshPkgFashion(changeValue)
    fashionData:RefreshPkgFashion(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pkg_Fashion)
end

-----------------------------------红点------------------------------
function FashionManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_FASHION then
        self:RefreshRedPointState(public_config.FASHION_TYPE_CLOTHES)
        self:RefreshRedPointState(public_config.FASHION_TYPE_WEAPON)
        self:RefreshRedPointState(public_config.FASHION_TYPE_SPECIAL)
        self:RefreshRedPointState(public_config.FASHION_TYPE_PHOTO)
        self:RefreshRedPointState(public_config.FASHION_TYPE_CHAT)
    end
end

function FashionManager:RefreshRedPointState(type)
    local result = false
    local data = self:GetFashionDataByType(type)
    for k1,v1 in pairs(data) do
        local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v1.id)
        if v1.star == -1 then
            if ownCount > 0 then
                result = true
                break
            end
        else
            local starData = FashionDataHelper.GetStarDataByIdAndStar(v1.id, v1.star)
            if starData.star_lvup_cost ~= nil then
                local _, count = next(starData.star_lvup_cost)
                if ownCount >= count then
                    result = true
                    break
                end
            end
        end
    end
    
    if type == public_config.FASHION_TYPE_CLOTHES then
        self:SetClothRedPoint(result)
    elseif type == public_config.FASHION_TYPE_WEAPON then
        self:SetWeaponRedPoint(result)
    elseif type == public_config.FASHION_TYPE_SPECIAL then
        self:SetEffectRedPoint(result)
    elseif type == public_config.FASHION_TYPE_PHOTO then
        self:SetPhotoRedPoint(result)
    elseif type == public_config.FASHION_TYPE_CHAT then
        self:SetChatRedPoint(result)
    end
end

function FashionManager:SetClothRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FASHION) then
        return
    end
    if self._showClothRedPoint ~= state then
        self._showClothRedPoint = state
        self._fashionClothUpgradeNode:CheckStrengthen(self:GetClothRedPoint())
    end
end

function FashionManager:GetClothRedPoint()
    return self._showClothRedPoint
end

function FashionManager:SetWeaponRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FASHION) then
        return
    end
    if self._showWeaponRedPoint ~= state then
        self._showWeaponRedPoint = state
        self._fashionWeaponUpgradeNode:CheckStrengthen(self:GetWeaponRedPoint())
    end
end

function FashionManager:GetWeaponRedPoint()
    return self._showWeaponRedPoint
end

function FashionManager:SetEffectRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FASHION) then
        return
    end
    if self._showEffectRedPoint ~= state then
        self._showEffectRedPoint = state
        self._fashionEffectUpgradeNode:CheckStrengthen(self:GetEffectRedPoint())
    end
end

function FashionManager:GetEffectRedPoint()
    return self._showEffectRedPoint
end

function FashionManager:SetPhotoRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FASHION) then
        return
    end
    if self._showPhotoRedPoint ~= state then
        self._showPhotoRedPoint = state
        self._fashionPhotoUpgradeNode:CheckStrengthen(self:GetPhotoRedPoint())
    end
end

function FashionManager:GetPhotoRedPoint()
    return self._showPhotoRedPoint
end

function FashionManager:SetChatRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FASHION) then
        return
    end
    if self._showChatRedPoint ~= state then
        self._showChatRedPoint = state
        self._fashionChatUpgradeNode:CheckStrengthen(self:GetChatRedPoint())
    end
end

function FashionManager:GetChatRedPoint()
    return self._showChatRedPoint
end

-------------------------------------功能接口-----------------------------------
--返回 时装类型对应时装列表信息
function FashionManager:GetFashionDataByType(type)
    local result = {}
    local showData = FashionDataHelper.GetFashionShowData(type, GameWorld.Player():GetVocationGroup(), GameWorld.Player().level)
    for k,v in pairs(showData) do
        local showType = FashionDataHelper.GetFashionShowType(v.id)
        local own = bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(v.id)
        local star = fashionData:GetFashionActiveInfo(v.id)
        if showType == 2 then
            if own or star ~= -1 then
                table.insert(result, {id=v.id, hasGot=own, star=star})
            end
        else
            table.insert(result, {id=v.id, hasGot=own, star=star})
        end
    end
    return result
end

function FashionManager:IsFashionLoaded(fashionId)
    local type = FashionDataHelper.GetFashionType(fashionId)
    if fashionData:GetCurLoadFashionId(type) == fashionId then
        return true
    end
    return false
end

function FashionManager:IsFashionClothLoaded()
    return fashionData:IsFashionClothLoaded()
end

function FashionManager:IsFashionWeaponLoaded()
    return fashionData:IsFashionWeaponLoaded()
end

function FashionManager:GetCurLoadFashionId(type)
    return fashionData:GetCurLoadFashionId(type)
end

-------------------------------------客户端请求-----------------------------------
--时装激活
function FashionManager:FashionActiveReq(fashionId)
    local params = string.format("%d", fashionId)
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_ACTIVATE, params)
end

function FashionManager:FashionLoadReq(fashionId)
    local params = string.format("%d", fashionId)
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_LOAD, params)
end

function FashionManager:FashionUnloadReq(fashionId)
    local params = string.format("%d", fashionId)
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_UNLOAD, params)
end

function FashionManager:FashionStarUpReq(fashionId)
    local params = string.format("%d", fashionId)
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_UPGRADE_STAR, params)
end

function FashionManager:FashionLevelUpReq(fashionType)
    local params = string.format("%d", fashionType)
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_UPGRADE_LEVEL, params)
end

function FashionManager:FashionResolveReq(posList)
    local params =""
    for i=1, #posList-1 do
        params = params .. posList[i]..","
    end
    params = params .. posList[#posList]
    GameWorld.Player().server.fashion_action_req(action_config.FASHION_BREAK_DOWN, params)
end


FashionManager:Init()
return FashionManager