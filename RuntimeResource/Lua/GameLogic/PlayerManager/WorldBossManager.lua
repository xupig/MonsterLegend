local WorldBossManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ActivityInfoManager = PlayerManager.ActivityInfoManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local WorldBossData = PlayerDataManager.worldBossData
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local ClientEntityManager = GameManager.ClientEntityManager
local BossType = GameConfig.EnumType.BossType
local TaskCommonManager = PlayerManager.TaskCommonManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function WorldBossManager:Init()
    self:InitCallbackFunc()
end

function WorldBossManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
end

function WorldBossManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)

    self._showRemainCountRedPoint = false
    self:RefreshRemainCountRedPointState()
end

function WorldBossManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function WorldBossManager:OnLeaveMap(mapId)
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        EventDispatcher:TriggerEvent(GameEvents.WORLD_BOSS_TIRE_UPDATE, false)
    end
end

function WorldBossManager:OnEnterMap(mapId)
    --LoggerHelper.Error("PetInstanceManager:OnEnterMap()" .. tostring(mapId))
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        EventDispatcher:TriggerEvent(GameEvents.WORLD_BOSS_TIRE_UPDATE, self:BossTireIsFull())
    end
end

function WorldBossManager:OnGetData()
    GameWorld.Player().server.world_boss_action_req(action_config.WORLD_BOSS_GET_DATA, tostring(0))
end

function WorldBossManager:OnBossEnter(bossId)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.WORLD_BOSS_ENTER)

    GameWorld.Player().server.world_boss_action_req(action_config.WORLD_BOSS_ENTER, tostring(bossId))
end

function WorldBossManager:OnBossIntrest(bossId)
    GameWorld.Player().server.world_boss_action_req(action_config.WORLD_BOSS_INTREST, tostring(bossId))
end

function WorldBossManager:OnBossGetKillerRecords(bossId)
    GameWorld.Player().server.world_boss_action_req(action_config.WORLD_BOSS_GET_KILLER_RECORDS, tostring(bossId))
end

function WorldBossManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        if event_id == action_config.WORLD_BOSS_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.WORLD_BOSS_ENTER)
            EventDispatcher:TriggerEvent(GameEvents.OnWorldBossEnter, false)
        end
        LoggerHelper.Error("WorldBoss Error:" .. error_id)
        return
    elseif event_id == action_config.WORLD_BOSS_GET_DATA then
        self:WorldBossGetInfo(args)
    elseif event_id == action_config.WORLD_BOSS_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.WORLD_BOSS_ENTER)
        self:WorldBossEnter(args)
    elseif event_id == action_config.WORLD_BOSS_INTREST then
        self:WorldBossIntrest(args)
    elseif event_id == action_config.WORLD_BOSS_NOTIFY_BOSS_WILL_REBORN then
        self:WorldBossNotifyBossWillReborn(args)
    elseif event_id == action_config.WORLD_BOSS_SPACE_BOSS_INFO or event_id == action_config.INSTANCE_NOTIFY_WORLD_BOSS_FAKE then
        self:WorldBossSpaceBossInfo(args)
    elseif event_id == action_config.WORLD_BOSS_GET_KILLER_RECORDS then
        self:WorldBossGetKillerRecords(args)
    else
        LoggerHelper.Error("WorldBoss event id not handle:" .. event_id)
    end
end

function WorldBossManager:RefreshTireFlag(changeValue)
    if changeValue then
        self:RefreshRemainCountRedPointState()
        EventDispatcher:TriggerEvent(GameEvents.OnWorldBossRefreshTireFlag)
    end
end

function WorldBossManager:WorldBossGetInfo(args)
    -- LoggerHelper.Log("Ash: WorldBossGetInfo: " .. PrintTable:TableToStr(args))
    WorldBossData:UpdateWorldBossData(args)
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossGetInfo)
end

function WorldBossManager:WorldBossEnter(args)
    -- LoggerHelper.Log("Ash: WorldBossEnter: " .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossEnter, true)
-- GUIManager.ClosePanel(PanelsConfig.WorldBoss)
-- GUIManager.ClosePanel(PanelsConfig.DailyActivity)
end

function WorldBossManager:WorldBossIntrest(args)
    -- LoggerHelper.Log("Ash: WorldBossIntrest: " .. PrintTable:TableToStr(args))
    WorldBossData:UpdateWorldBossIntrest(args)
end

function WorldBossManager:WorldBossNotifyBossWillReborn(args)
    -- LoggerHelper.Log("Ash: WorldBossNotifyBossWillReborn: " .. PrintTable:TableToStr(args))
    local bossInfo = self:GetBossInfo(args[1])
    GUIManager.ShowPanel(PanelsConfig.WorldBossMainUI, bossInfo)
-- self._curWorldBossInspireId = args[1]
-- EventDispatcher:TriggerEvent(GameEvents.OnWorldBossInspireChanged, args[1])
end

function WorldBossManager:WorldBossSpaceBossInfo(args)
    -- LoggerHelper.Log("Ash: WorldBossSpaceBossInfo: " .. PrintTable:TableToStr(args))
    WorldBossData:UpdateWorldBossSpaceBossInfo(args)
    ClientEntityManager.bossargs = {args, BossType.BossWorld}
    ClientEntityManager.UpdateBossTomb(args, BossType.BossWorld)
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossSpaceBossInfo)
end

function WorldBossManager:WorldBossGetKillerRecords(args)
    -- LoggerHelper.Log("Ash: WorldBossGetKillerRecords: " .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossGetKillerRecords, args)
end

function WorldBossManager:GotoBossById(bossId)
    self:GotoBoss(self:GetBossInfo(bossId))
end

function WorldBossManager:GotoBossByMapId(mapId, pos)
    local id = WorldBossDataHelper:GetMonsterByMapId(mapId)
    if id then
        local boss = self:GetBossInfo(id)
        self:GotoBoss(boss, pos)
    end
end

function WorldBossManager:GotoBoss(bossData, targetPos)
    local cb = function() self:ConfirmGotoBoss(bossData, targetPos) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function WorldBossManager:ConfirmGotoBoss(bossData, targetPos)
    --LoggerHelper.Log("Ash: GotoBoss: " .. bossData:GetSenceId() .. " " .. bossData:GetBossRegionId())
    local pos = targetPos or GameSceneManager:GetMonsterPos(bossData:GetSenceId(), bossData:GetBossRegionId())
    --LoggerHelper.Log("Ash: GotoBoss pos: " .. PrintTable:TableToStr(pos))
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBossSceneKillMonsterPoint,
        bossData:GetId(), bossData:GetSenceId(), pos, 5)
end

function WorldBossManager:GetAllBossInfo()
    return WorldBossData:GetAllBossInfo()
end

function WorldBossManager:GetBossTire()
    return WorldBossData:GetBossTire()
end

function WorldBossManager:GetBossInfo(id)
    return WorldBossData:GetBossInfo(id)
end

function WorldBossManager:GetAllSpaceBossInfo()
    return WorldBossData:GetAllSpaceBossInfo()
end

--世界boss疲劳值是否已满
function WorldBossManager:BossTireIsFull()
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        return GameWorld.Player().tire_flag == 1 --self:GetBossTire() >= GlobalParamsHelper.GetParamValue(439)
    else
        return false
    end
end

-----------------------------------红点------------------------------
function WorldBossManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_WORLD_BOSS then
        self:RefreshRemainCountRedPointState()
    end
end

function WorldBossManager:RefreshRemainCountRedPointState()
    local isFuncOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_WORLD_BOSS)
    if not isFuncOpen then
        return
    end
    self:SetRemainCountRedPointState(GameWorld.Player().tire_flag == 0)
end

function WorldBossManager:SetRemainCountRedPointState(state)
    if self._showRemainCountRedPoint ~= state then
        self._showRemainCountRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WORLD_BOSS, self:GetRemainCountRedPointState())
    end
end

function WorldBossManager:GetRemainCountRedPointState()
    return self._showRemainCountRedPoint
end

WorldBossManager:Init()
return WorldBossManager
