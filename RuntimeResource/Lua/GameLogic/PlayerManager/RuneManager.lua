local RuneManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local error_code = GameWorld.error_code
local RuneDataHelper = GameDataHelper.RuneDataHelper
local RuneConfigDataHelper = GameDataHelper.RuneConfigDataHelper
local RuneExchangeDataHelper = GameDataHelper.RuneExchangeDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RuneShowDataHelper = GameDataHelper.RuneShowDataHelper
local RuneData = PlayerManager.PlayerDataManager.runeData
local AionTowerManager = PlayerManager.AionTowerManager
local FloatTextManager = PlayerManager.FloatTextManager
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DelayTime = 0.5

function RuneManager:Init()
	self._oldpkg = {}
	self._oldessence = 0
	self._oldpiece = 0
	self._isresolve = false
	self._timerInid = -1
	self._timerReid = -1
	self._timerPkgid = -1
	self._runeGridChange = function() self:RuneGridChange() end
	self._runePkgChange = function() self:RunePkgChange() end
	self._runePieceChange = function() self:RunePieceChange() end
	self._runeEssenceChange = function() self:RuneEssenceChange()end
	self._updataInRed = function ()self:UpdataInRedPoint()end
	self._updataReRed = function ()self:UpdataReRedPoint()end
	self._updataPkg = function()self:RunePkgUpdata()end
	self._updataPiece = function()self:RunePieceUpate()end
	self._updataEssence = function()self:RuneEssenceUpdate()end
	self._updateInsertRedPoint = function()self:RefreshRuneInsertRedEvent()end
    self._updateResolveRedPoint = function()self:RefreshRuneResolveRedEvent()end
	self._openRuneEven = function(id)self:RefrenRuneRed(id)end
    self._runeslotopen = function()self:RuneSlotOpen()end
	self._runeshow = {}
	self._runeshowle = {}
	self._runeshowdata = {}
end

function RuneManager:GetRuneAtt(att)
	local runeatts = {}		
	if att ~= nil then
		local i = 1
		for k, v in pairs(att) do
			local attrid = tonumber(k)
			local content = AttriDataHelper:GetName(attrid)
			local type = AttriDataHelper:GetShowType(attrid)
			local add = v
			local _d = {}
			_d[0] = content --属性名
			_d[1] = add --加成
			_d[2] = type
			runeatts[i] = _d
			i = i + 1
		end
	end
	return runeatts
end
-- RPC

--穿戴符文
function RuneManager.RunePutOnRpc(pkg_posi,grid_posi)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_PUTON,pkg_posi,grid_posi)
end

--取下符文
function RuneManager.RunePutOffRpc(grid_posi)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_PUTOFF,grid_posi,0)
end

-- 替换已穿戴符文
function RuneManager.RuneReplaceRpc(grid_posi,pkg_posi)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_REPLACE,grid_posi,pkg_posi)
end

-- 符文升级
function RuneManager.RuneUpLevelRpc(grid_posi)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_UPLEVEL,grid_posi,0)
end

--符文分解
function RuneManager.RuneExplodeRpc(pkg_posi)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_EXPLODE,pkg_posi,0)
end

--符文兑换
function RuneManager.RuneExchangeRpc(id)
	GameWorld.Player().server.rune_action_req(action_config.RUNE_EXCHANGE,id,0)
end

--Resp
function RuneManager:HandleResp(action_id,error_id,lua_table)
	if action_id == action_config.RUNE_PUTON then
		self:RunePutOnResp(error_id,lua_table)
	elseif action_id == action_config.RUNE_PUTOFF then
		self:RunePutOffResp(error_id,lua_table)
	elseif action_id == action_config.RUNE_REPLACE then
		self:RuneReplaceResp(error_id,lua_table)
	elseif action_id == action_config.RUNE_UPLEVEL then
		self:RuneUpLevelResp(error_id,lua_table)
	elseif action_id == action_config.RUNE_EXPLODE then
		self:RuneExplodeResp(error_id,lua_table)
	elseif action_id == action_config.RUNE_EXCHANGE then
		self:RuneExchangeResp(error_id,lua_table)
	end
end

function RuneManager:IsResolveNow(state)
	self._isresolve = state
end
--穿戴符文
function RuneManager:RunePutOnResp(error_id,lua_table)
	if error_id == 0 then
		EventDispatcher:TriggerEvent(GameEvents.RunePutOnUpdate)  
		GameWorld.PlaySound(25)
	end
end

--取下符文
function RuneManager:RunePutOffResp(error_id,lua_table)
		
end

-- 替换已穿戴符文
function RuneManager:RuneReplaceResp(error_id,lua_table)
	if error_id == 0 then
		GameWorld.PlaySound(25)
	end
	EventDispatcher:TriggerEvent(GameEvents.RuneReUpdate)  
end

-- 符文升级
function RuneManager:RuneUpLevelResp(error_id,lua_table)
	EventDispatcher:TriggerEvent(GameEvents.RuneUpUpdate)  
end

--符文分解
function RuneManager:RuneExplodeResp(error_id,lua_table)
	if error_id == 0 then
		-- GameWorld.PlaySound(26)		
	end
	EventDispatcher:TriggerEvent(GameEvents.RuneExploadeRespUpdate,error_id)
end

--符文兑换
function RuneManager:RuneExchangeResp(error_id,lua_table)
    EventDispatcher:TriggerEvent(GameEvents.RuneExchangeUpdate)
end

-- 属性change

-- 格子
function RuneManager:RuneGridChange()	

end

-- 背包
function RuneManager:RunePkgChange()
	if self._timerPkgid ~= nil then
		TimerHeap:DelTimer(self._timerPkgid)	
		self._timerPkgid = nil
	end
	self._timerPkgid = TimerHeap:AddSecTimer(DelayTime,0,0,self._updataPkg)
end

function RuneManager:RunePkgUpdata()
	-- LoggerHelper.Log("只更新一次")
	self._timerPkgid = nil
	local newpkg = GameWorld.Player().rune_pkg
	local preruneid = 0
	for k,v in pairs(newpkg) do 
		local runeid = v[public_config.RUNE_ID]
		local num0 = self:GetRuneNum(runeid,newpkg)
		local num1 = self:GetRuneNum(runeid,self._oldpkg)
		if num0>num1 and preruneid ~= runeid then
			local v = num0-num1
			local itemid = RuneDataHelper:GetItemCom(runeid)
			FloatTextManager:OnChange(v,itemid)
			preruneid = runeid
		end
	end
	self._oldpkg = {}
	table.merge(self._oldpkg,newpkg)
end
--符文碎片
function RuneManager:RunePieceChange()
	if self._timerPieceid ~= nil then
		TimerHeap:DelTimer(self._timerPieceid)	
		self._timerPieceid = nil
	end
	self._timerPieceid = TimerHeap:AddSecTimer(DelayTime,0,0,self._updataPiece)
end

function RuneManager:RunePieceUpate()
	-- LoggerHelper.Log("只更新一次")
	local  piece = GameWorld.Player().rune_piece
	if piece>self._oldpiece then
		local v = piece - self._oldpiece
		FloatTextManager:OnChange(v,14)		
	end
	self._oldpiece = piece
end

-- 符文精华
function RuneManager:RuneEssenceChange()
	if self._timerEssenceid ~= nil then
		TimerHeap:DelTimer(self._timerEssenceid)	
		self._timerEssenceid = nil
	end
	self._timerEssenceid = TimerHeap:AddSecTimer(DelayTime,0,0,self._updataEssence)
end

function RuneManager:RuneEssenceUpdate()
	-- LoggerHelper.Log("只更新一次")
	if self._isresolve then
		return
	end
	local essence = GameWorld.Player().rune_essence
	if essence>self._oldessence then
		local v = essence - self._oldessence
		FloatTextManager:OnChange(v,12)		
	end
	self._oldessence = essence
end

function RuneManager:RefrenRuneRed(id)
	if id==PanelsConfig.FUNCTION_ID_SYMBOL_MOSAIC or id==PanelsConfig.FUNCTION_ID_SYMBOL_BREAK or id==PanelsConfig.FUNCTION_ID_SYMBOL_EXCHANGE then
		self:RefrenRuneEven()
	end
end

function RuneManager:RefrenRuneEven()
	self:RefreshRuneInsertRedEvent()
	self:RefreshRuneResolveRedEvent()
end

function RuneManager:RefrenRuneStrength(state)
	self._runeReplaceStrengNode:CheckStrengthen(state)
end

function RuneManager:RefrenRuneEssence()
	local entity = GameWorld.Player()
	self._oldessence = entity.rune_essence
end

-- 进入加监听
function RuneManager:OnPlayerEnterWorld()	
	local entity = GameWorld.Player()
	self._runeslots=self:GetAllOpenSlots()
	self._oldpkg = {}
	table.merge(self._oldpkg,entity.rune_pkg)
	self._oldessence = entity.rune_essence
	self._oldpiece = entity.rune_piece
	self._runeReplaceStrengNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.RUNE_INSERT, public_config.FUNCTION_ID_SYMBOL_MOSAIC)
	self._runeUpStrengNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.RUNE_UP, public_config.FUNCTION_ID_SYMBOL_MOSAIC)
	self:RefrenRuneRed()
	EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._openRuneEven)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._updateInsertRedPoint)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._updateResolveRedPoint)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_GRID, self._updateInsertRedPoint)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._updateResolveRedPoint)
	entity:AddPropertyListener(EntityConfig.PROPERTY_AION_INFO, self._updateInsertRedPoint)
	entity:AddPropertyListener(EntityConfig.PROPERTY_AION_INFO, self._runeslotopen)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_ESSENCE, self._runeEssenceChange)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._runePieceChange)
	entity:AddPropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._runePkgChange)
end


--退出移除监听
function RuneManager:OnPlayerLeaveWorld()
	local entity = GameWorld.Player()
	EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._openRuneEven)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._updateInsertRedPoint)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._updateResolveRedPoint)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_GRID, self._updateInsertRedPoint)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._updateResolveRedPoint)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_AION_INFO, self._updateInsertRedPoint)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_AION_INFO, self._runeslotopen)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_ESSENCE, self._runeEssenceChange)	
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PIECE, self._runePieceChange)
	entity:RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._runePkgChange)
end

function RuneManager:RuneSlotOpen()
	local runeslots =self:GetAllOpenSlots()
	if #runeslots~=1 and #runeslots>#self._runeslots then
		self._runeslots=runeslots
		GUIManager.ShowPanel(PanelsConfig.RuneOpenSlot)
	end
end

function RuneManager:GetAllOpenSlots()
	local slots = {}
	for i=1,8 do
		if not RuneData:IsLayerLock(i) then
			table.insert(slots,i)
		end
	end
	return slots
end

function RuneManager:GetRuneNum(id,data)
	local i=0
	for k,v in pairs(data) do 
		local runeid = v[public_config.RUNE_ID]
		local runelevel = v[public_config.RUNE_LEVEL]
		if runeid == id then
			i=i+1
		end
	end
	return i
end

function RuneManager:RuneShow()
	if not table.isEmpty(self._runeshow) and not table.isEmpty(self._runeshowle)then
		return self._runeshow,self._runeshowle
	end
	local ids = RuneShowDataHelper:GetAllId()
	local j=1
	for k,v in ipairs(ids) do
		local type = RuneShowDataHelper:GetUnlockFloor(v)
		local d = {}
		if self._runeshow[type]==nil then
			self._runeshow[type]={}
			self._runeshowle[j] = type
			j=j+1
		end
		local i = RuneShowDataHelper:GetRuneId(v)
		d['runeid'] = RuneDataHelper:GetItemCom(i)
		d['level'] = RuneDataHelper:GetLevel(i)
		d['name'] = RuneDataHelper:GetName(i)
		d['quality'] = RuneDataHelper:GetQuality(i)
		table.insert(self._runeshow[type],d)
	end
	return self._runeshow,self._runeshowle
end

function RuneManager:RuneShowData()
	if not table.isEmpty(self._runeshowdata) then
		return self._runeshowdata
	end
	local d,les = self:RuneShow()
	local aionInfo = AionTowerManager:GetAionInfo()
	local curLayer = aionInfo[public_config.AION_NOW_LAYER] or 1 --当前永恒之塔层级
	for k,v in ipairs(les) do
		local da = {}
		local c = tonumber(v)
		if c <= curLayer then
			if c==0 then
				table.insert(self._runeshowdata,{1,d[v],c})
			else
				table.insert(self._runeshowdata,{2,d[v],c})
			end
		else
			table.insert(self._runeshowdata,{3,d[v],c})
		end
	end
	return self._runeshowdata
end

-- 镶嵌
function RuneManager:RefreshRuneInsertRedEvent()
	if self._timerInid>-1 then
		TimerHeap:DelTimer(self._timerInid)
	end
	self._timerInid = TimerHeap:AddSecTimer(2,0,0,self._updataInRed)
end

function RuneManager:UpdataInRedPoint()
	TimerHeap:DelTimer(self._timerInid)
	self._timerInid = -1
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SYMBOL_MOSAIC, self:RefreshRedPointState())     
end
-- 分解
function RuneManager:RefreshRuneResolveRedEvent()
	if self._isresolve then
		return
	end
	if self._timerReid>-1 then
		TimerHeap:DelTimer(self._timerReid)	
	end
	self._timerReid = TimerHeap:AddSecTimer(2,0,0,self._updataReRed)			       
end

function RuneManager:UpdataReRedPoint()
	TimerHeap:DelTimer(self._timerReid)
	self._timerReid = -1
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SYMBOL_BREAK, self:RefreshResolveRedPointState())      
end

-- 兑换
function RuneManager:RefreshRuneExchangeRedEvent()
    -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SYMBOL_EXCHANGE, self:RefreshExchangeRedPointState())        
end

-- 镶嵌红点
function RuneManager:RefreshRedPointState()
	local f1 = self:RefreshReplaceRedPointState()
	local f2 = self:RefreshUpRedPointState()
	if f1 or f2 then return true end
	return false
end

-- 替换红点
function RuneManager:RefreshReplaceRedPointState()
	local state = RuneData:GetRepRed() or self:IsSameRuneAndUp()
	self:RefrenRuneStrength(state)
	return state
end

function RuneManager:RefrenRuneUpStrength(state)
	self._runeUpStrengNode:CheckStrengthen(state)
end

-- 升级红点
function RuneManager:RefreshUpRedPointState()
	local state = self:RefreshUpRed()
	self:RefrenRuneUpStrength(state)
	return state
end

function RuneManager:RefreshUpRed()
	local grid = GameWorld.Player().rune_grid
	for i=1,8 do
		local v = grid[i]
		if v then
			if self:RuneUpRed(v[1],v[2]) then
				return true
			end
		end	
	end
	return false
end

-- 是否有更高级的同类符文
function RuneManager:IsSameRuneAndUp()
	local grid = GameWorld.Player().rune_grid
	for i=1,8 do
		local v = grid[i]
		if v then
			if RuneData:IsSameRuneAndUp(v[1],v[2]) then
				return true
			end
		end	
	end
	return false
end
function RuneManager:RuneUpRed(runeid,level)
	local attcfg = RuneConfigDataHelper:GetAttByIdlevel(runeid,level)
	if not attcfg or not attcfg.cost or table.containsValue(attcfg.cost, 0) then 
		return false 
	end
	local cost = attcfg.cost
    local entity = GameWorld.Player()
    local id = 0
    local costnum = 0
    local havenum = 0
	id,costnum = next(cost)
    local pieceid = GlobalParamsHelper.GetParamValue(485)
    local essenceid = GlobalParamsHelper.GetParamValue(484)
    if id == pieceid then
        havenum = entity.rune_piece
    else 
        havenum = entity.rune_essence
    end
    if havenum>= costnum then
        return true
    end
    return false
end

-- 分解红点
function RuneManager:RefreshResolveRedPointState()
	local runepkg = GameWorld.Player().rune_pkg
	for i,v in pairs(runepkg) do
		local q = RuneDataHelper:GetQuality(v[1])
		local types=RuneDataHelper:GetType(v[1])
		if q==1 or q==3 or types[1]==91 or types[2]==91 then
			return true
		end
	end
	return false
end
-- 兑换红点
function RuneManager:RefreshExchangeRedPointState()
	return false
	-- local ids = RuneExchangeDataHelper:GetAllId()
	-- local entity = GameWorld.Player()
	-- for _,id in ipairs(ids) do
	-- 	local runeid = RuneExchangeDataHelper:GetRuneId(id)
    --     local level = RuneExchangeDataHelper:GetLevel(id)
	-- 	local Excfg = RuneExchangeDataHelper:GetCfgByIdLevel(runeid,level)[1]
	-- 	local floor = Excfg.floor
	-- 	local price = Excfg.price[public_config.MONEY_TYPE_RUNE_PIECE]
	-- 	local aionInfo = AionTowerManager:GetAionInfo()
	-- 	local curLayer = aionInfo[public_config.AION_NOW_LAYER] or 0 --当前永恒之塔层级	
	-- 	local piece = entity.rune_piece
	-- 	if curLayer>=floor then
	-- 		if piece>=price then
	-- 			return true
	-- 		end
	-- 	end
	-- 	return false
	-- end	
end
RuneManager:Init()
return RuneManager

	