
local LimitedShoppingManager = {}
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local FlashSale2DataHelper = GameDataHelper.FlashSale2DataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local LimitedShoppingfunctionOpenId = 414
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local EnumType = GameConfig.EnumType

function LimitedShoppingManager:Init()
    
    self._openPanel = function() GUIManager.ShowPanel(PanelsConfig.LimitedShopping) end
    self._isFuncOpen = false
    self._isAllPass = false
end

function LimitedShoppingManager:OnPlayerEnterWorld()
    self._onLimitedShoppingResp = function(actId, code, args) self:OnLimitedShoppingActionResp(actId, code, args) end
    EventDispatcher:AddEventListener(GameEvents.ON_ACTION_RESP, self._onLimitedShoppingResp)
    self._onEnterMap = function() self:OnEnterMap() end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    self._onGuideLevelUp = function() self:OnGuideLevelUp() end
    EventDispatcher:AddEventListener(GameEvents.OnGuideLevelUp, self._onGuideLevelUp)
    self:OnEnterMap()
end

function LimitedShoppingManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ACTION_RESP, self._onLimitedShoppingResp)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.OnGuideLevelUp, self._onGuideLevelUp)
end

function LimitedShoppingManager:OnEnterMap()
    self:CheckPanelOutOfDate()

    self:CheckLimitedShoppingOpen()
    self:CheckNoItemCanBuy()
end

function LimitedShoppingManager:OnGuideLevelUp()
    self:CheckLimitedShoppingOpen()
end

function LimitedShoppingManager:CheckLimitedShoppingOpen()
    if self._isAllPass == true then return end
    if self._isFuncOpen then
        self:ShowLimitedShopping()
        return
    end
    self._isFuncOpen = GameDataHelper.FunctionOpenDataHelper:CheckFunctionOpen(LimitedShoppingfunctionOpenId) 
    if self._isFuncOpen then
        self:ShowLimitedShopping()
    end
end

function LimitedShoppingManager:ShowLimitedShopping()
    EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self._openPanel,EnumType.RemindType.LimitedShopping)
end

function LimitedShoppingManager:OnLimitedShoppingActionResp(actId, code, args)
    if code ~= 0 then
        -- LoggerHelper.Error("限时抢购后端发来了错误码：" .. tostring(code))
        return
    end
    if actId == action_config.LIMITED_SALE_BUY then
        EventDispatcher:TriggerEvent(GameEvents.OnRefreshLimitedShoppingPanel)
    end
end

-- 是否买过该商品
function LimitedShoppingManager:IsBuyed(id)
    if GameWorld.Player() == nil then return false end
    local buyInofs = GameWorld.Player().limited_sale_buy_flag
    if buyInofs[id] == nil then return false end
    return buyInofs[id] == 1
end

-- 关闭提示
function LimitedShoppingManager:CheckPanelOutOfDate()
    local isOutOfDate = self:IsPanelOutOfDate()
    if isOutOfDate then
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitedShopping)
    end
end
-- 没东西可买
function LimitedShoppingManager:CheckNoItemCanBuy()
    -- if GameWorld.Player() == nil then return end
    if self._isAllPass then
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitedShopping)
    end
    local shopItemIdList = FlashSale2DataHelper:GetAllId()
    local playerListData = GameWorld.Player().limited_sale_info-- nil等级还没达到，升级时间
    local buyInofs = GameWorld.Player().limited_sale_buy_flag
    local now = DateTimeUtil.GetServerTime()
    local allBuy = true
-- 未买过的两个条件，flag ~= 1, 时间没过
    for _, id in pairs(shopItemIdList) do
        local shopItemData = FlashSale2DataHelper.GetFlashSale2(id)
        local openLevel = shopItemData.level
        local upTime = playerListData[openLevel]
        if (buyInofs[id]==nil or buyInofs[id] ~= 1) and upTime ~= nil then--买过了
            if upTime + shopItemData.time > now then--有一个时间还没过
                allBuy = false
                break
            end
        end
    end
    if allBuy then
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitedShopping)
    end
end

-- 功能是否关闭
function LimitedShoppingManager:IsPanelOutOfDate()
    -- if GameWorld.Player() == nil then return end
    if self._isAllPass then return self._isAllPass end
    local shopItemIdList = FlashSale2DataHelper:GetAllId()
    local playerListData = GameWorld.Player().limited_sale_info or {}-- nil等级还没达到，升级时间
    local now = DateTimeUtil.GetServerTime()
    local canClose = true

    for _, id in pairs(shopItemIdList) do
        local shopItemData = FlashSale2DataHelper.GetFlashSale2(id)
        local openLevel = shopItemData.level
        local upTime = playerListData[openLevel]
        if upTime == nil then
            canClose = false
            break
        end

        if upTime + shopItemData.time > now then--有一个时间还没过
            canClose = false
            break
        end

    end
    self._isAllPass = canClose
    return canClose
end

-- math.floor((原价*50/100）)
function LimitedShoppingManager:PackShopItemData()
    -- if GameWorld.Player() == nil then return end
    --return {{shopItemId=1}, {shopItemId=2}}
    --
    local shopItemIdList = FlashSale2DataHelper:GetAllId()

    local playerListData = GameWorld.Player().limited_sale_info-- nil等级还没达到，升级时间
    

    local now = DateTimeUtil.GetServerTime()

    local packDatas = {}
    local haveOneUnlock = false

    for _, id in pairs(shopItemIdList) do
        local shopItemData = FlashSale2DataHelper.GetFlashSale2(id)
        local openLevel = shopItemData.level
        local upTime = playerListData[openLevel]
        if upTime == nil then
            if haveOneUnlock == false then
                haveOneUnlock = true
                table.insert(packDatas, {
                    shopItemId = id
                })
            end
        else
            if upTime + shopItemData.time > now then--时间还没过
                table.insert(packDatas, {
                    shopItemId = id,
                    upLevelTime = upTime
                })      
            end
        end
    end

    return packDatas
    
end

function LimitedShoppingManager:CalcTime(outOfDateTime)
    local now = DateTimeUtil.GetServerTime()
    local leftTime = outOfDateTime - now
    if leftTime <= 0 then return false, 0 end

    local dateInfo = DateTimeUtil.MinusDay(outOfDateTime, now)
    local hour = dateInfo.hour
    local hourStr = (hour < 10) and string.format("0%d", hour) or hour
    local min = dateInfo.min
    local minStr = (min < 10) and string.format("0%d", min) or min
    local sec = dateInfo.sec
    local secStr = (sec < 10) and string.format("0%d", sec) or sec

    return true, string.format("%s:%s:%s", hourStr, minStr, secStr)
end

function LimitedShoppingManager:BuyLimitedItem(shopId)
    GameWorld.Player().server.limited_sale_action_req(action_config.LIMITED_SALE_BUY, tostring(shopId))
end

--[[
limited_sale_action_req
LIMITED_SALE_BUY    = 11351, --买  （参数：xml的id）
MAIL_ID_LIMITED_SALE                = 1162, --限时抢购2
<limited_sale_info>
        <Type> LUA_TABLE </Type>
        <Flags> BASE_AND_CLIENT </Flags>
        <Persistent> true </Persistent>
</limited_sale_info>

level是xml里面有的等级，等于0代表买过
我表里的时间戳是开启的时间

<limited_sale_buy_flag>
            <Type> LUA_TABLE </Type>
            <Flags> BASE_AND_CLIENT </Flags>
            <Persistent> true </Persistent>
        </limited_sale_buy_flag>

limited_sale_info = { [level] = 升到该等级的时间，....}
[id] = 1 表示买了
EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind,self._mailmsgfun,EnumType.RemindType.MailUnRead)
--]]
LimitedShoppingManager:Init()
return LimitedShoppingManager