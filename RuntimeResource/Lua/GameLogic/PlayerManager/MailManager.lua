require "PlayerManager/PlayerData/MailData"

local EventDispatcher = EventDispatcher
local action_config = GameWorld.action_config
local MailManager = {}
local table = table
local MailData = ClassTypes.MailData
local Mail = ClassTypes.Mail
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function MailManager:Init()
    self._mailPage = 1
    self._mailData = {}
    self._mailAddData = {}
    self._rewardQueue = {}
    self._mailmsgfun = function()self:MailMsgFunCb()end
    self._numsOfShow = 0
end

function MailManager:OnActionResp(actId, code, args)
    --获取所有邮件
    LoggerHelper.Error("MailManager OnActionResp")
end

function MailManager:MailMsgFunCb()
    -- GUIManager.ShowPanel(PanelsConfig.Mail)
    local data={}
    data.firstTabIndex=2
    GUIManager.ShowPanel(PanelsConfig.Friend,data)
end


function MailManager:MailMsgSend(mails)
    if mails and not table.isEmpty(mails) then
        for k,v in ipairs(mails) do
            local state = v:GetInfoState()
            if state==public_config.MAIL_STATE_HERE or state==public_config.MAIL_STATE_HAVE or state == public_config.MAIL_STATE_NONE then
                return true
            end
        end
    end
    return false
end


function MailManager:OnMailRece(act_id, code, args)
    if act_id == action_config.MAIL_LIST_REQ then
        if args == nil or table.isEmpty(args) then
            self:MailRedPoint(false) 
            EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.MailUnRead)
            return
        else
             --邮件界面打开的时候再刷新界面
            local curmailpage = args[2]
            local totalpage = args[3]
            local unreadcount = args[5]
            local ungetcount = args[6]
            if not self._mailAddData[curmailpage] then
                self._mailAddData[curmailpage]={}
            end 
            self._mailAddData[curmailpage]=args[public_config.MAIL_LIST_KEY_MAILS]
            if unreadcount>0 or ungetcount>0 then     
                self._mailData = MailData(args,self._mailAddData)    
                EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind,self._mailmsgfun,EnumType.RemindType.MailUnRead)
                self:MailRedPoint(true)                         
            else                        
                EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.MailUnRead)
                self:MailRedPoint(false)                         
            end
        end
        EventDispatcher:TriggerEvent(GameEvents.OnReceiveMails)
        self:CheckMailButtonTips()
    elseif act_id == action_config.MAIL_SEND_MAIL_REQ then
    elseif act_id == action_config.MAIL_DEL_REQ then---
        EventDispatcher:TriggerEvent(GameEvents.OnDeleteMail, args)
    elseif act_id == action_config.MAIL_READ_REQ then
    elseif act_id == action_config.MAIL_BATCH_DEL_REQ then
        EventDispatcher:TriggerEvent(GameEvents.OnDeleteMail, args)
    elseif act_id == action_config.MAIL_GET_ATTACHMENT_REQ then       
        EventDispatcher:TriggerEvent(GameEvents.OnColectMail, args,code)
    elseif act_id == action_config.MAIL_ON_NEW_MAIL_RESP then
        local showText = LanguageDataHelper.GetContent(58781)
        GUIManager.ShowText(2, showText)
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind,self._mailmsgfun,EnumType.RemindType.MailUnRead)
        self:MailRedPoint(true)
        if GUIManager.PanelIsActive(PanelsConfig.Friend) then
            EventDispatcher:TriggerEvent(GameEvents.OnReceNewMail, self:CreateMail(args))
        end
    end
end

function MailManager:MailRedPoint(status)
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MAIL, status)    
end

function MailManager:GetMailRedPoint()
    
end


--批量删除邮件
function MailManager:BatchDeleteMails(mailList)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_BATCH_DEL_REQ, mailList)
end
--批量领取邮件附件
function MailManager:BatchColectAdds(mailList)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_BATCH_GET_ATTACHMENT_REQ, mailList)
end
--删除一封邮件
function MailManager:DeleteMailByDBId(dbid)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_DEL_REQ, '' .. dbid)
end
--领取一封邮件附件
function MailManager:ColectMailByDBId(dbid)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_GET_ATTACHMENT_REQ, '' .. dbid)
end
--读取邮件
function MailManager:ReadMailByDBId(dbid)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_READ_REQ, '' .. dbid)
end
--请求邮件
function MailManager:MailReq(page)
    GameWorld.Player().server.mail_action_req(action_config.MAIL_LIST_REQ, '' .. page)
end
--创建一封邮件
function MailManager:CreateMail(mailData)
    return Mail(mailData)
end
--登录立即请求一次
function MailManager:OnPlayerEnterWorld()
    self:MailReq(1)
end

function MailManager:CheckMailButtonTips()
    for k, v in pairs(self:GetMailList() or {}) do
        --出现可以删除的邮件
        if v:GetInfoState()==public_config.MAIL_STATE_NONE or v:GetInfoState()==public_config.MAIL_STATE_HAVE then     
            return
        end
    end
    --取消提示
end
--检查
function MailManager:IsNil()
    if self._mailData == nil or table.isEmpty(self._mailData) then
        return true
    end
    return false
end
--获取邮件列表
function MailManager:GetMailList() 
    if self:IsNil() == false then
        return self._mailData:GetMails() or {}
    end
    return nil
end
--获取当前页
function MailManager:GetCurPage()
    if self:IsNil() == false then
        return self._mailData:GetCurPage() or 0
    end
    return 0
end
--获取总页数
function MailManager:GetTotalPage()
    if self:IsNil() == false then
        return self._mailData:GetTotalPage() or 0
    end
    return 0
end
--获取邮件数量
function MailManager:GetMailCnt()
    if self:IsNil() == false then
        return self._mailData:GetMailCnt() or 0
    end
    return 0
end

function MailManager:SetMailCnt(value)
    if self:IsNil() == false then
        self._mailData:SetMailCnt(value)
    end
end
--获取新邮件数量
function MailManager:GetNewMailCnt()
    if self:IsNil() == false then
        return self._mailData:GetNewMailCnt() or 0
    end
    return 0
end
--获取新邮件数量
function MailManager:GetUngetAttachmentMailCnt()
    if self:IsNil() == false then
        return self._mailData:GetUngetAttachmentMailCnt() or 0
    end
    return 0
end
----------------------------------------------显示奖励相关--永恒不用了
--清除奖励显示列表
function MailManager:CleanRewardQueue()
    self._rewardQueue = {}
    self._numsOfShow = 0
end
--添加奖励显示项
function MailManager:AddShowItem(mailId, item, posData)
    table.insert(self._rewardQueue, {[1]=mailId, [2]=item, [3]=posData})
end
--移除奖励显示项
function MailManager:RemoveShowItem()
    return table.remove(self._rewardQueue, 1)
end
--获得长度
function MailManager:GetLength()
    return #self._rewardQueue
end

function MailManager:AddNumsOfShow()
    self._numsOfShow = self._numsOfShow + 1
end

function MailManager:GetNumsOfShow()
    return self._numsOfShow
end

function MailManager:OnPlayerLeaveWorld()
end
MailManager:Init()

return MailManager