local RouletteManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RouletteData = PlayerManager.PlayerDataManager.rouletteData
local NormalYunpanDataHelper = GameDataHelper.NormalYunpanDataHelper
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local GUIManager = GameManager.GUIManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function RouletteManager:Init()
	self._opentimeCB = function() self:OpenTimer() end
end

-- RPC
--普通奖励
function RouletteManager.RouletteRewardRpc()
	GameWorld.Player().server.roulette_action_req(action_config.ROULETTE_REWARD,'')
end
-- 全服购买数量
function RouletteManager.RouletteTotalCountRpc()
	GameWorld.Player().server.roulette_action_req(action_config.ROULETTE_TOTAL_LUCK_TIMES_REQ,'')
end

--普通奖励获奖列表
function RouletteManager.RoulettePersonalListRpc()
	GameWorld.Player().server.roulette_action_req(action_config.ROULETTE_PERSONAL_LIST,'')
end

-- 幸运大奖获奖列表
function RouletteManager.RouletteBigRewardListRpc()
	GameWorld.Player().server.roulette_action_req(action_config.ROULETTE_BIG_REWARD_LIST,'')
end

--Resp
function RouletteManager:HandleResp(action_id,error_id,lua_table)
	if error_id~=0 then
		return
	end
	if action_id == action_config.ROULETTE_REWARD then
		RouletteData:UpdateRecordReward(lua_table)
	elseif action_id == action_config.ROULETTE_PERSONAL_LIST then
		RouletteData:UpdateRouleReward(lua_table)
	elseif action_id == action_config.ROULETTE_BIG_REWARD_LIST then
		RouletteData:UpdatePreRouleReward(lua_table)
		EventDispatcher:TriggerEvent(GameEvents.RouleBigRecordUpdate,lua_table)
	elseif action_id == action_config.ROULETTE_TOTAL_LUCK_TIMES_REQ then
		EventDispatcher:TriggerEvent(GameEvents.RouletteTotalCount,lua_table)
	end
end
-- 判断活动是否在开启时间段
function RouletteManager:OpenTimer()
	local f = FunctionOpenDataHelper:CheckPanelOpen(302)
	if not f then
		return 
	end

	if self._timerid and self._timerid>-1 then
		TimerHeap:DelTimer(self._timerid)
	end
	local isopen = self:IsOnOpenTime()
	if isopen then 
		local a = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
		if a['hour']<RouletteData:GetTimeEnd() then	
			local endtimestamp = DateTimeUtil.TodayTimeStamp(RouletteData:GetTimeEnd(),0)
			local s = DateTimeUtil.MinusSec(endtimestamp)	
			EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.Rouletter, function()
				GUIManager.ShowPanel(PanelsConfig.Roulette)
			end,endtimestamp)	
			self._timerid = TimerHeap:AddSecTimer(s,0,0,function ()
				EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Rouletter)
				TimerHeap:DelTimer(self._timerid) 				
			end)					
		else
			EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Rouletter)
		end
	else
		EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Rouletter)
	end
end

function RouletteManager:IsOnOpenTime()
	local d = DateTimeUtil.GetServerOpenDays()
	local ids = NormalYunpanDataHelper:GetAllId()
	local f1 = NormalYunpanDataHelper:GetDay(ids[1])
	local f2 = NormalYunpanDataHelper:GetDay(ids[#ids])
	if f1<= d and d<=f2 then 
		return true
	end
	return false
end

-- 进入加监听
function RouletteManager:OnPlayerEnterWorld()		
	-- GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ROULETTE_TIMES, self._runeEssenceChange)
	-- GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ROULETTE_LUCK_TIMES, self._runeEssenceChange)
	EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._opentimeCB)	
	EventDispatcher:AddEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)
end
--退出移除监听
function RouletteManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._opentimeCB)
	EventDispatcher:RemoveEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)	
	-- GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ROULETTE_TIMES, self._runeEssenceChange)	
	-- GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ROULETTE_LUCK_TIMES, self._runeEssenceChange)	
end


RouletteManager:Init()
return RouletteManager

	