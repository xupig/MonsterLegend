local WaitTimeManager = {}

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local SystemSettingManager = PlayerManager.SystemSettingManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager

function WaitTimeManager:Init()
    self:InitCallbackFunc()
end

function WaitTimeManager:InitCallbackFunc()
    self._onLevelChange = function()self:OnLevelChange() end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
end

function WaitTimeManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function WaitTimeManager:OnPlayerLeaveWorld()
    self._init = nil
    self._fpsStatus = nil
    self:ClearTimer()
    self:ClaerFpsTimer()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)
end

function WaitTimeManager:OnEnterMap()
    if self._init == nil then
        self:CheckAutoDoMainTask()
        self:CheckFps()
        self._init = true
    end
end

function WaitTimeManager:CheckFps()
    self._fpsTimer = TimerHeap:AddSecTimer(2, 30, 0, function()
        if (not PlayerManager.PowerSaveManager:IsInPowerSaving()) and GameWorld.GetLowFps() and
            SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY) ~= SystemSettingConfig.QUALITY_LOW then
            GUIManager.ShowPanel(PanelsConfig.QualityChange)
        end 
    end)
end

function WaitTimeManager:CheckAutoDoMainTask()
    if GlobalParamsHelper.GetParamValue(927) >= GameWorld.Player().level then
        --自动回归任务
        self._pos = GameWorld.Player():GetPosition()
        GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)
        self._timer = TimerHeap:AddSecTimer(2, GlobalParamsHelper.GetParamValue(928), 0, function() 
            local nowPos = GameWorld.Player():GetPosition()
            if self._pos.x == nowPos.x and self._pos.y == nowPos.y and self._pos.z == nowPos.z then
                TaskCommonManager:RunMainTask()
            end
            self._pos = GameWorld.Player():GetPosition()
        end)
    end

    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function WaitTimeManager:OnLevelChange()
    if GlobalParamsHelper.GetParamValue(927) < GameWorld.Player().level then
        self:ClearTimer()
    end
end

function WaitTimeManager:ClearTimer()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function WaitTimeManager:ClaerFpsTimer()
    if self._fpsTimer then
        TimerHeap:DelTimer(self._fpsTimer)
        self._fpsTimer = nil
    end
end

WaitTimeManager:Init()
return WaitTimeManager