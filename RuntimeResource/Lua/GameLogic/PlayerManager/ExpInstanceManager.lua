local ExpInstanceManager = {}

local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local PlayerDataManager = PlayerManager.PlayerDataManager
local ExpInstanceDataHelper = GameDataHelper.ExpInstanceDataHelper
local instanceData = PlayerDataManager.instanceData
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GUIManager = GameManager.GUIManager
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceManager = PlayerManager.InstanceManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function ExpInstanceManager:Init()
    self:InitCallbackFunc()
end

function ExpInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function ExpInstanceManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function ExpInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function ExpInstanceManager:OnLeaveMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE then
    end
end

function ExpInstanceManager:OnEnterMap(mapId)
--    LoggerHelper.Error("ExpInstanceManager:OnEnterMap()" .. tostring(mapId))
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE then
        InstanceManager:EnterInstance()
    end
end


function ExpInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("ExpInstanceManager HandleData Error:" .. error_id)
        if action_id == action_config.INSTANCE_EXP_ENTER then--689,     --进入经验副本
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.INSTANCE_EXP_ENTER)
        end
        return
    elseif action_id == action_config.INSTANCE_EXP_ENTER then                 --689,     --进入经验副本
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.INSTANCE_EXP_ENTER)
        TaskCommonManager:RealStop()
        -- LoggerHelper.Log("sam 689 == " .. PrintTable:TableToStr(args))
        self:OnExpInstanceEnter()
    elseif action_id == action_config.INSTANCE_EXP_NOTIFY_INFO then           --692,     --把经验副本信息同步到客户端
        -- LoggerHelper.Log("sam 692 == " .. PrintTable:TableToStr(args))
        self:UpdateExpInstanceInfo(args)
    elseif action_id == action_config.INSTANCE_EXP_SETTLE then                --698,     --经验副本结算
        -- LoggerHelper.Log("sam 698 == " .. PrintTable:TableToStr(args))
        InstanceBalanceManager:ShowBalance(args)
    elseif action_id == action_config.INSTANCE_EXP_BUY_ENTER_NUM then         --700,     --购买经验副本次数
        -- LoggerHelper.Log("sam 700 == " .. PrintTable:TableToStr(args))
        GUIManager.ShowText(2, LanguageDataHelper.CreateContent(58660))
        GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
        -- EventDispatcher:TriggerEvent(GameEvents.EXP_INSTANCE_INFO_CHANGE)
    elseif action_id == action_config.INSTANCE_EXP_NOTIFY_ROOM_STAGE then     --687,     --房间阶段
        -- LoggerHelper.Error("sam 687 == " .. PrintTable:TableToStr(args)) 
        -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = ExpInstanceDataHelper:GetExpInstanceCfg(1).prepare_time
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            InstanceCommonManager:StopBeginCountDown()
            GameWorld.Player():StartAutoFight()
            local instanceTime = ExpInstanceDataHelper:GetExpInstanceCfg(1).last_time
            InstanceCommonManager:StartInstanceCountDown(instanceTime, tonumber(args[2]))
        else
        end
    else
        LoggerHelper.Error("ExpInstanceManager HandleData action id not handle:" .. action_id)
    end
end

--处理伤害统计
function ExpInstanceManager:HandleDamageRank(luaTable)
    -- LoggerHelper.Log("InstanceManager:HandleDamageRank")
    -- LoggerHelper.Log(luaTable)
    instanceData:SetDamageRankList(luaTable)
    EventDispatcher:TriggerEvent(GameEvents.UPDATE_DAMAGE_RANK)
end

function ExpInstanceManager:UpdateExpInstanceInfo(args)
    -- local stage = args[1]
    -- if stage == 0 then
    --     --副本开始倒计时,如：5秒后开始副本
    --     local time = ExpInstanceDataHelper:GetExpInstanceCfg(1).prepare_time
    --     LoggerHelper.Error("ExpInstanceDataHelper"..time)
    --     LoggerHelper.Error("开始倒计时")
    --     InstanceCommonManager:ShowBeginCountDown(time, 0)
    --     self._expInstanceBegin = false
    -- else
    --     if not GameWorld.Player().autoFight and not self._expInstanceBegin then
    --         GameWorld.Player():StartAutoFight()
    --     end
    --     self._expInstanceBegin = true
    -- end
    instanceData:UpdateExpInstanceInfo(args)
    EventDispatcher:TriggerEvent(GameEvents.UPDATE_EXP_INSTANCE_INFO)
end

--进入经验副本自动战斗
function ExpInstanceManager:OnExpInstanceEnter()
    GameWorld.Player():StartAutoFight()
end

--经验副本是否有次数进入
function ExpInstanceManager:GetExpInstanceCanEnter()
    local info = GameWorld.Player().exp_instance_info
    local cfgData = ExpInstanceDataHelper:GetExpInstanceCfg(1)
    local buyNum = info[public_config.EXP_INSTANCE_BUY_ENTER_NUM] or 0
    local itemAddNum = info[public_config.EXP_INSTANCE_ITEM_ADD_ENTER_NUM] or 0
    local totalNum = cfgData.enter_limit + buyNum + itemAddNum
    local timesLeft = totalNum - (info[public_config.EXP_INSTANCE_ENTER_TIMES] or 0)
    return timesLeft > 0
end

--进入经验副本
function ExpInstanceManager:RequestExpInstanceEnter()
    local cb = function() self:ConfirmGotoBoss() end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function ExpInstanceManager:ConfirmGotoBoss()
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.INSTANCE_EXP_ENTER)
    GameWorld.Player().server.exp_instance_action_req(action_config.INSTANCE_EXP_ENTER, "")
end

--经验副本鼓舞
function ExpInstanceManager:RequestExpInstanceInspire(inspireType)
    GameWorld.Player().server.exp_instance_action_req(action_config.INSTANCE_EXP_INSPIRE, tostring(inspireType))
end

--经验副本购买次数
function ExpInstanceManager:BuyExpInstanceEnterNum(num)
    GameWorld.Player().server.exp_instance_action_req(action_config.INSTANCE_EXP_BUY_ENTER_NUM, tostring(num))
end

ExpInstanceManager:Init()
return ExpInstanceManager
