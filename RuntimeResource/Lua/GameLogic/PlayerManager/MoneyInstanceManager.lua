local MoneyInstanceManager = {}

local MoneyInstanceDataHelper = GameDataHelper.MoneyInstanceDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager  = PlayerManager.InstanceCommonManager
local InstanceManager = PlayerManager.InstanceManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local SceneConfig = GameConfig.SceneConfig
local InstanceConfig = GameConfig.InstanceConfig
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RedPointManager = GameManager.RedPointManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function MoneyInstanceManager:Init()
    self:InitCallbackFunc()
end

function MoneyInstanceManager:InitRedpointNodes()
    self._moneyNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_GOLD_INSTANCE)
end

function MoneyInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function MoneyInstanceManager:OnPlayerEnterWorld()
    self:InitRedpointNodes()
    self:UpdateRedPoint()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function MoneyInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function MoneyInstanceManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GOLD then
        self._instanceId = nil
        self._startTime = nil
    end
end

function MoneyInstanceManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GOLD then
        InstanceManager:EnterInstance()
    end    

    -- if self._leaveMapId ~= mapId and MapDataHelper.GetSceneType(self._leaveMapId) == SceneConfig.SCENE_TYPE_GOLD then
    --     GUIManager.ShowPanel(PanelsConfig.InstanceHall,{["functionId"] = 204})
    -- end
end

--星级评价移动完毕
function MoneyInstanceManager:MoveStartEnd(start)
    -- if start == 3 then
    self:BeginCountDown()
    -- end
end

function MoneyInstanceManager:BeginCountDown()
    local instanceTime = MoneyInstanceDataHelper:GetTime(self._instanceId)
    InstanceCommonManager:StartInstanceCountDown(instanceTime, self._startTime)
end

function MoneyInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("MoneyInstanceManager HandleData Error:" .. error_id)
        if action_id == action_config.INSTANCE_GOLD_ENTER then--7401,     --进入副本
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.INSTANCE_GOLD_ENTER)
        end
        return
    elseif action_id == action_config.INSTANCE_GOLD_ENTER then                 --7401,     --进入副本
        --LoggerHelper.Error("sam 7401 == " .. PrintTable:TableToStr(args))
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.INSTANCE_GOLD_ENTER)
        TaskCommonManager:RealStop()
        -- LoggerHelper.Log("sam 7401 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.INSTANCE_GOLD_BUY_ENTER_NUM then         --7402,     --购买次数
        -- LoggerHelper.Log("sam 7402 == " .. PrintTable:TableToStr(args))
        GUIManager.ShowText(2,LanguageDataHelper.CreateContent(58660))
        GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
        -- EventDispatcher:TriggerEvent(GameEvents.MONEY_INSTANCE_INFO_CHANGE)
    elseif action_id == action_config.INSTANCE_GOLD_SWEEP then                 --7403,     --扫荡  
        EventDispatcher:TriggerEvent(GameEvents.SWEEP_INSTANCE_END, true)  
        -- EventDispatcher:TriggerEvent(GameEvents.MONEY_INSTANCE_INFO_CHANGE)
        InstanceBalanceManager:ShowSweepBalance(args)
        -- LoggerHelper.Log("sam 7403 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.INSTANCE_GOLD_SPACE_INFO then            --7404,     --副本状态变化
        -- LoggerHelper.Log("sam 7404 == " .. PrintTable:TableToStr(args)) 
        -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = GameDataHelper.GlobalParamsHelper.GetParamValue(515)
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            --副本开始开始计时，如03:30后结束副本。副本开始计时时需要先显示评价星级
            GameWorld.Player():StartAutoFight()

            self._startTime = tonumber(args[2])
            -- data.start = 3
            data.startTimeData = MoneyInstanceDataHelper:GetStartTime(self._instanceId)
            data.dropMessages = InstanceConfig.MONEY_INSTANCE_START_DROP_MESSAGE_MAP
            data.countDownMessages = InstanceConfig.MONEY_INSTANCE_COUNT_DOWN_MESSAGE_MAP
            data.cb = function(start) self:MoveStartEnd(start) end
            data.startTime = tonumber(args[2])
            InstanceCommonManager:ShowStart(data)
        else
        end
    elseif action_id == action_config.INSTANCE_GOLD_SETTLE then                --7405,     --结算信息
        -- LoggerHelper.Log("sam 7405 == " .. PrintTable:TableToStr(args))
        InstanceBalanceManager:ShowBalance(args)
    elseif action_id == action_config.INSTANCE_GOLD_WAVE_CHANGE then           --7406,     --波数变化
        -- LoggerHelper.Log("sam 7406 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.instanceId = self._instanceId
        data.num = args[1]
        data.type = InstanceConfig.MONEYINSTANCE_WAVE_CHANGE
        GUIManager.ShowPanel(PanelsConfig.MoneyInstanceInfo, data)
    elseif action_id == action_config.INSTANCE_GOLD_NOTIFY_GOLD_COUNT then     -- 7407, --通知金币数量  
        -- LoggerHelper.Error("金币信息====" .. PrintTable:TableToStr(args))
        --{"1" = 1517562}
        local data = {}
        data.money = args[1]
        data.type = InstanceConfig.MONEYINSTANCE_MONEY_CHANGE
        GUIManager.ShowPanel(PanelsConfig.MoneyInstanceInfo, data)
    else
        LoggerHelper.Error("MoneyInstanceManager HandleData action id not handle:" .. action_id)
    end
end

function MoneyInstanceManager:OnUpdateInfo()
    self:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.MONEY_INSTANCE_INFO_CHANGE)
end

function MoneyInstanceManager:UpdateRedPoint()
    if self._moneyNode and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GOLD_INSTANCE) then
        local moneyData = GameWorld.Player().instance_gold_info
        local fightNum = moneyData[public_config.INSTANCE_GOLD_KEY_LEFT_ENTER_NUM]
        self._moneyNode:SetRedPoint(fightNum > 0)
    end
end

--设置副本id
function MoneyInstanceManager:SetInstanceId(instanceId)
    self._instanceId = instanceId
end

--7401,     --进入副本
function MoneyInstanceManager:EnterMoneyInstance(id)
    local cb = function() self:ConfirmGotoBoss(id) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function MoneyInstanceManager:ConfirmGotoBoss(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.INSTANCE_GOLD_ENTER)
    self._instanceId = id
    GameWorld.Player().server.instance_gold_action_req(action_config.INSTANCE_GOLD_ENTER, tostring(id))
end

--7402,     --购买次数
function MoneyInstanceManager:BuyMoneyInstanceEnterNum(num)
    GameWorld.Player().server.instance_gold_action_req(action_config.INSTANCE_GOLD_BUY_ENTER_NUM, tostring(num))
end

--7403,     --扫荡
function MoneyInstanceManager:SweepMoneyInstance(id)
    GameWorld.Player().server.instance_gold_action_req(action_config.INSTANCE_GOLD_SWEEP, tostring(id))
end

MoneyInstanceManager:Init()
return MoneyInstanceManager