local LotteryEquipManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local lotteryEquipData = PlayerManager.PlayerDataManager.lotteryEquipData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData

function LotteryEquipManager:Init()
    self._updateBag = function (pkgType) self:UpdateData(pkgType) end
    self._onRefreshEquipFindKeyInfo = function() self:RefreshKeyRedPointState() end
    self._on_0_Oclock = function()self:RefreshKeyFreeRedPointState() end
    self._onLotteryEquipLastFreeTimeChange = function()self:RefreshKeyFreeRedPointState() end
end

function LotteryEquipManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateBag)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Equip_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.ON_LOTTERY_EQUIP_LAST_FREE_TIME_CHANGE, self._onLotteryEquipLastFreeTimeChange)

    self._showEquipFindRedPoint = false
    self._showWarehouseRedPoint = false
    self._showKeyRedPoint = false
    self._showKeyFreeRedPoint = false

    self:RefreshWarehouseRedPointState()
    self:RefreshKeyRedPointState()
    self:RefreshKeyFreeRedPointState()

    local startTime = GameWorld.Player().lottery_equip_last_free_time + GlobalParamsHelper.GetParamValue(920)
    if startTime > DateTimeUtil.GetServerTime() and DateTimeUtil.SameDay(startTime) then
        local date = DateTimeUtil.SomeDay(startTime)
        self._timer = DateTimeUtil.AddTimeCallback(date.hour, date.min, date.sec, false, self._onLotteryEquipLastFreeTimeChange)
    end
end

function LotteryEquipManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateBag)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Equip_Find_Key_Info, self._onRefreshEquipFindKeyInfo)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.ON_LOTTERY_EQUIP_LAST_FREE_TIME_CHANGE, self._onLotteryEquipLastFreeTimeChange)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
end

function LotteryEquipManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("LotteryEquipManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.LOTTERY_EQUIP_ONE then
        self:RefreshRewardData(args)
        EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Report)
        self:LotteryEquipSelfRecordReq()
        self:LotteryEquipServerRecordReq()
    elseif action_id == action_config.LOTTERY_EQUIP_TEN then
        self:RefreshRewardData(args)
        EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Report)
        self:LotteryEquipSelfRecordReq()
        self:LotteryEquipServerRecordReq()
    elseif action_id == action_config.LOTTERY_EQUIP_FIFTY then
        self:RefreshRewardData(args)
        EventDispatcher:TriggerEvent(GameEvents.Show_Equip_Find_Report)
        self:LotteryEquipSelfRecordReq()
        self:LotteryEquipServerRecordReq()
    elseif action_id == action_config.LOTTERY_EQUIP_GET_PERSONAL_RECORD then
        self:RefreshSelfRecord(args)
    elseif action_id == action_config.LOTTERY_EQUIP_GET_SERVER_RECORD then
        self:RefreshServerRecord(args)
    end
end

function LotteryEquipManager:RefreshSelfRecord(data)
    lotteryEquipData:RefreshSelfRecord(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Equip_Find_Self_Record)
end

function LotteryEquipManager:RefreshServerRecord(data)
    lotteryEquipData:RefreshServerRecord(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Equip_Find_Server_Record)
end

function LotteryEquipManager:RefreshRewardData(data)
    lotteryEquipData:RefreshRewardData(data)
end

function LotteryEquipManager:OnPassDay()
    local startTime = GameWorld.Player().lottery_equip_last_free_time + GlobalParamsHelper.GetParamValue(920)
    if startTime > DateTimeUtil.GetServerTime() and DateTimeUtil.SameDay(startTime) then
        local date = DateTimeUtil.SomeDay(startTime)
        self._timer = DateTimeUtil.AddTimeCallback(date.hour, date.min, date.sec, false, self._onRefreshEquipFindKeyInfo)
    end
end

-----------------------------------红点------------------------------
function LotteryEquipManager:UpdateData(pkgType)
	if pkgType == public_config.PKG_TYPE_LOTTERY_EQUIP then
		self:RefreshWarehouseRedPointState()
	end
end

function LotteryEquipManager:RefreshWarehouseRedPointState()
    local count = bagData:GetPkg(public_config.PKG_TYPE_LOTTERY_EQUIP):GetTotalCount()
    self:SetWarehouseRedPoint(count>0)
end

function LotteryEquipManager:SetWarehouseRedPoint(state)
    if self._showWarehouseRedPoint ~= state then
        self._showWarehouseRedPoint = state
        self:RefreshEquipFindRedPoint()
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Equip_Find_Red_Point)
    end
end

function LotteryEquipManager:GetWarehouseRedPoint()
    return self._showWarehouseRedPoint
end

function LotteryEquipManager:RefreshKeyRedPointState()
    local itemId, needCount = next(GlobalParamsHelper.GetParamValue(750))
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
    local result = false
    if ownCount >= needCount then
        result = true
    end
    self:SetKeyRedPoint(result)
end

function LotteryEquipManager:SetKeyRedPoint(state)
    if self._showKeyRedPoint ~= state then
        self._showKeyRedPoint = state
        self:RefreshEquipFindRedPoint()
    end
end

function LotteryEquipManager:GetKeyRedPoint()
    return self._showKeyRedPoint
end

function LotteryEquipManager:RefreshKeyFreeRedPointState()
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Equip_Find_Key_Red_Point)
    local result = false
    local startTime = GameWorld.Player().lottery_equip_last_free_time + GlobalParamsHelper.GetParamValue(920)
    if startTime <= DateTimeUtil.GetServerTime() + 5 then
        result = true
    end
    self:SetKeyFreeRedPoint(result)
end

function LotteryEquipManager:SetKeyFreeRedPoint(state)
    if self._showKeyFreeRedPoint ~= state then
        self._showKeyFreeRedPoint = state
        self:RefreshEquipFindRedPoint()
    end
end

function LotteryEquipManager:RefreshEquipFindRedPoint()
    local result = self._showKeyRedPoint or self._showWarehouseRedPoint or self._showKeyFreeRedPoint
    if self._showEquipFindRedPoint ~= result then
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_EQUIP_TREASURE, result)
        self._showEquipFindRedPoint = result
    end
end

-------------------------------------客户端请求-----------------------------------
--单抽
function LotteryEquipManager:LotteryEquipOneReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_ONE, "")
end

--10抽
function LotteryEquipManager:LotteryEquipTenReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_TEN, "")
end

--50抽
function LotteryEquipManager:LotteryEquipFiftyReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_FIFTY, "")
end

function LotteryEquipManager:LotteryEquipExchangeReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_EXCHANGE, params)
end

function LotteryEquipManager:LotteryEquipTakeOutAllReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_TAKE_OUT_ALL, "")
end

function LotteryEquipManager:LotteryEquipServerRecordReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_GET_SERVER_RECORD, "")
end

function LotteryEquipManager:LotteryEquipSelfRecordReq()
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_GET_PERSONAL_RECORD, "")
end

--设置今日是否提醒弹窗 参数：0：提醒 1：不提醒
function LotteryEquipManager:LotteryEquipSetRemindReq(state)
    local params = string.format("%d", state)
    GameWorld.Player().server.lottery_equip_action_req(action_config.LOTTERY_EQUIP_SET_REMIND_FLAG, params)
end

LotteryEquipManager:Init() 
return LotteryEquipManager