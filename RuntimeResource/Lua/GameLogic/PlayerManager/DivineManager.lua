--占星
local DivineManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local divineData = PlayerManager.PlayerDataManager.divineData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RewardBoxStateType = GameConfig.EnumType.RewardBoxStateType

function DivineManager:Init()
end

function DivineManager:HandleData(action_id,error_id,args)
    -- LoggerHelper.Log("DivineManager "..action_id)
    -- LoggerHelper.Log(args)
    if action_id == action_config.DIVINE_REQ then
        EventDispatcher:TriggerEvent(GameEvents.Show_Divine_Reward_Result, args)
    elseif action_id == action_config.DIVINE_MULTI_REQ then
        EventDispatcher:TriggerEvent(GameEvents.Show_Divine_Reward_Result, args)
    end
end

function DivineManager:RefreshDivineInfo(changeValue)
    divineData:RefreshDivineInfo(changeValue)
end

--获取累计次数宝箱状态
function DivineManager:GetRewardState(count)
    if count <= divineData.divineWeekCount then
        local hasGot = false
        for _,v in pairs(divineData.divineWeekRewardData) do
            if v == count then
                hasGot = true
            end
        end
        if hasGot then
            return RewardBoxStateType.HasGot
        else
            return RewardBoxStateType.CanGet
        end
    else
        return RewardBoxStateType.NotGet
    end
end

-------------------------------------客户端请求-----------------------------------
--单次占卜
function DivineManager:DivineOneReq()
    GameWorld.Player().server.divine_action_req(action_config.DIVINE_REQ, "")
end

--占卜5次
function DivineManager:DivineFiveReq()
    GameWorld.Player().server.divine_action_req(action_config.DIVINE_MULTI_REQ, "")
end

--获取累计次数奖励
function DivineManager:DivineGetRewardReq(count)
    local params = string.format("%d", count)
    GameWorld.Player().server.divine_action_req(action_config.DIVINE_GET_REWARD, params)
end

DivineManager:Init() 
return DivineManager