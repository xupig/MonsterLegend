local TowerInstanceManager = {}

local public_config = GameWorld.public_config
local action_config = require("ServerConfig/action_config")
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local TowerInstanceConfig = GameConfig.TowerInstanceConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceManager = PlayerManager.InstanceManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local towerInstanceData = PlayerManager.PlayerDataManager.towerInstanceData
local towerData = PlayerManager.PlayerDataManager.towerData
local RedPointManager = GameManager.RedPointManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function TowerInstanceManager:Init()
    self:InitCallbackFunc()
end

function TowerInstanceManager:InitRedpointNodes()
    self._towerNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_TOWER_DEFENSE)
end

function TowerInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onTowerEnter = function(mapId)self:OnTowerEnter(mapId) end
    self._onTowerLeave = function(mapId)self:OnTowerLeave(mapId) end
end

function TowerInstanceManager:OnPlayerEnterWorld()
    self:InitRedpointNodes()
    self:UpdateRedPoint()
    --LoggerHelper.Error("TowerInstanceManager:OnPlayerEnterWorld()")
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.TriggerTowerEnter, self._onTowerEnter)
    EventDispatcher:AddEventListener(GameEvents.TriggerTowerLeave, self._onTowerLeave)
end

function TowerInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.TriggerTowerEnter, self._onTowerEnter)
    EventDispatcher:RemoveEventListener(GameEvents.TriggerTowerLeave, self._onTowerLeave)
end

function TowerInstanceManager:OnTowerEnter(cfg)       
    -- LoggerHelper.Error("id ====" .. tostring(cfg.id)) 
    if towerData:HasBuildTowe(cfg.id) then
        --已经建塔了      
    else
        --还没建塔
        local data = {}
        data.data = towerInstanceData
        data.type = TowerInstanceConfig.SHOW_BNUILD_TOWERT_LIST
        data.towerId = cfg.id
        GUIManager.ShowPanel(PanelsConfig.TowerInstanceInfo, data)  
        -- self:BuildTower(cfg.id, 1)
    end
    
    EventDispatcher:TriggerEvent(GameEvents.PlayFX, 270001, cfg.id, "Fx/fx_tower_range.prefab")
end

function TowerInstanceManager:OnTowerLeave()    
    local data = {}
    data.data = towerInstanceData
    data.type = TowerInstanceConfig.HIDE_BNUILD_TOWERT_LIST
    GUIManager.ShowPanel(PanelsConfig.TowerInstanceInfo, data)  
    EventDispatcher:TriggerEvent(GameEvents.DelFX, "", 270001, "")
end

function TowerInstanceManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_TOWER_DEFENSE then
        self._instanceId = nil
        self._startTime = nil
        self._autoCallMonster = false
        self._stage = nil
        GUIManager.ShowPanel(PanelsConfig.Remind)
    end
end

function TowerInstanceManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_TOWER_DEFENSE then
        InstanceManager:EnterInstance()
    end    

end

function TowerInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("TowerInstanceManager HandleData Error:" .. error_id)
        if action_id == action_config.TOWER_DEFENSE_ENTER then--9001,     --进入副本
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.TOWER_DEFENSE_ENTER)
        end
        return
    elseif action_id == action_config.TOWER_DEFENSE_ENTER then                 --9001,     --进入副本
        --LoggerHelper.Error("sam 9001 == " .. PrintTable:TableToStr(args))
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.TOWER_DEFENSE_ENTER)
        TaskCommonManager:RealStop()
        -- LoggerHelper.Error("sam 9001 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.TOWER_DEFENSE_BUY_ENTER_NUM then         --9002,     --购买次数
        -- LoggerHelper.Error("sam 9002 == " .. PrintTable:TableToStr(args))
        GUIManager.ShowText(2,LanguageDataHelper.CreateContent(58660))
        GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
        -- EventDispatcher:TriggerEvent(GameEvents.TOWER_INSTANCE_INFO_CHANGE)
    elseif action_id == action_config.TOWER_DEFENSE_SWEEP then                 --9003,     --扫荡  
        -- LoggerHelper.Error("TOWER_DEFENSE_SWEEP:"..PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SWEEP_INSTANCE_END, true)  
        -- EventDispatcher:TriggerEvent(GameEvents.TOWER_INSTANCE_INFO_CHANGE)
        InstanceBalanceManager:ShowSweepBalance(args)

    elseif action_id == action_config.TOWER_DEFENSE_BUILD_TOWER then           --9005,     --建塔  
        -- LoggerHelper.Error("sam 9005 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.TOWER_DEFENSE_BUILD_TOWER_SUCCESS)
    elseif action_id == action_config.TOWER_DEFENSE_CALL_MONSTER then     --9006, -- 召唤怪
        -- LoggerHelper.Error("sam 9006 召唤怪 == " .. PrintTable:TableToStr(args)) 
    elseif action_id == action_config.TOWER_DEFENSE_SET_AUTO_CALL then     --9007,     --是否自动召唤怪
        -- LoggerHelper.Error("sam 9007 是否自动召唤怪 == " .. PrintTable:TableToStr(args)) 
        self._autoCallMonster = (args[1] == 1)
    elseif action_id == action_config.TOWER_DEFENSE_NOTIFY_ROOM_STAGE then     --9008,     --副本状态变化
        -- LoggerHelper.Error("sam 9008 == " .. PrintTable:TableToStr(args)) 
        -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = GlobalParamsHelper.GetParamValue(695)
            self._stage = 3
            -- LoggerHelper.Error("服务器传来的开始时间：" .. tostring(args[2]))
            -- LoggerHelper.Error("现在的时间" .. DateTimeUtil.GetServerTime())
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]), 58411)
            end
            EventDispatcher:TriggerEvent(GameEvents.ON_TOWER_INSTANCE_BEGIN_WAIT)
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            --副本开始开始计时，如03:30后结束副本。
            InstanceCommonManager:StopBeginCountDown()
            GameWorld.Player():StartAutoFight()
            EventDispatcher:TriggerEvent(GameEvents.ON_TOWER_INSTANCE_START)
            self._stage = 4
            self._startTime = tonumber(args[2])
            
            local instanceTime = GlobalParamsHelper.GetParamValue(694)
            InstanceCommonManager:StartInstanceCountDown(instanceTime, self._startTime)
        else
            self._stage = 100
            EventDispatcher:TriggerEvent(GameEvents.ON_TOWER_INSTANCE_START)
        end
    elseif action_id == action_config.TOWER_DEFENSE_NOTIFY_WAVE_INFO then   --9009,     --波数变化
        -- LoggerHelper.Error("sam 9009 波数变化 == " .. PrintTable:TableToStr(args))
        towerInstanceData:UpdateData(args)
        local data = {}
        data.data = towerInstanceData
        data.type = TowerInstanceConfig.SHOW_INFO
        GUIManager.ShowPanel(PanelsConfig.TowerInstanceInfo, data)
    elseif action_id == action_config.TOWER_DEFENSE_NOTIFY_TOWER_INFO then   --9010,     --通知建塔信息
        -- LoggerHelper.Error("sam 9010 通知建塔信息 == " .. PrintTable:TableToStr(args))        
        towerData:UpdateData(args)
        local data = {}
        data.data = towerInstanceData
        data.type = TowerInstanceConfig.UPDATE_TOWERT_LIST
        GUIManager.ShowPanel(PanelsConfig.TowerInstanceInfo, data)  
    elseif action_id == action_config.TOWER_DEFENSE_NOTIFY_SETTLE then      --9011,     --通知结算信息
        LoggerHelper.Log("sam 9011 通知结算信息 == " .. PrintTable:TableToStr(args))
        InstanceBalanceManager:ShowBalance(args)
    else
        LoggerHelper.Error("TowerInstanceManager HandleData action id not handle:" .. action_id)
    end
end

function TowerInstanceManager:GetStage()
    return self._stage
end

function TowerInstanceManager:GetAutoFlag()
    return self._autoCallMonster
end

function TowerInstanceManager:IsCanBuildById(typeId)
    return towerData:IsCanBuildById(typeId)
end

function TowerInstanceManager:GetShowTextById(typeId)
    return towerData:GetShowTextById(typeId)
end

function TowerInstanceManager:GetRewardDatas()
    return towerInstanceData:GetItemRewards()
end

function TowerInstanceManager:OnUpdateInfo()
    self:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.TOWER_INSTANCE_INFO_CHANGE)
end

function TowerInstanceManager:UpdateRedPoint()
    if self._towerNode and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_TOWER_DEFENSE) then
        local towerData = GameWorld.Player().tower_defense_info
        local fightNum = towerData[public_config.TOWER_DEFENSE_KEY_ENTER_NUM]
        local buyNum = towerData[public_config.TOWER_DEFENSE_KEY_BUY_NUM]
        local baseNum = GlobalParamsHelper.GetParamValue(696)
        --LoggerHelper.Error("baseNum + buyNum - fightNum =======" .. tostring(baseNum + buyNum - fightNum))
        self._towerNode:SetRedPoint((baseNum + buyNum - fightNum) > 0)
    end
end

--9001,     --进入副本
function TowerInstanceManager:EnterTowerInstance()
    local cb = function() self:ConfirmGotoBoss() end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function TowerInstanceManager:ConfirmGotoBoss(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.TOWER_DEFENSE_ENTER)
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_ENTER, "")
end

--9002,     --买次数
function TowerInstanceManager:BuyTowerInstanceEnterNum(num)
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_BUY_ENTER_NUM, tostring(num))
end

--9003, -- 扫荡
function TowerInstanceManager:SweepPetInstance(num)
    -- LoggerHelper.Error("TowerInstanceManager"..num)
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_SWEEP, tostring(num))
end

--9004, -- 直接开始刷怪
function TowerInstanceManager:StartInstance()
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_START, "")
end

--9005, -- 建塔
function TowerInstanceManager:BuildTower(towerId, typeId)
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_BUILD_TOWER, towerId .. "," .. typeId)    
end

--9006, -- 召唤怪
function TowerInstanceManager:CallBoss()
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_CALL_MONSTER, "")
end

--9007, -- 改变是否自动召唤黄金怪
function TowerInstanceManager:ChangeAuto()
    GameWorld.Player().server.tower_defense_action_req(action_config.TOWER_DEFENSE_SET_AUTO_CALL, "")
end

TowerInstanceManager:Init()
return TowerInstanceManager