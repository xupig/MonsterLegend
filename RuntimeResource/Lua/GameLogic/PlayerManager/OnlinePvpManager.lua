local OnlinePvpManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OnlinePvpDataHelper = nil
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local RedPointManager = GameManager.RedPointManager
local OnlinePvpReward2DataHelper = GameDataHelper.OnlinePvpReward2DataHelper

function OnlinePvpManager:Init()
    self._hasNotGetStageRewards = true
    self._stageRewardsData = {}
    self:InitCallbackFunc()
end

function OnlinePvpManager:InitCallbackFunc()
    self._onEnterMap = function()self:OnEnterMap() end
end

function OnlinePvpManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_CROSS_VS11)
    if isOpen then
        self:GetData()
    end
end
function OnlinePvpManager:GetOnlinePvpViewRedPoint()
--     LoggerHelper.Log("self:HasNotGetStageRewards() " .. tostring(self:HasNotGetStageRewards())
--     .. "self:HasNotGetDailyReward() " .. tostring(self:HasNotGetDailyReward())
--     .. "self:HasNotGetCountRewards() " .. tostring(self:HasNotGetCountRewards())
--     -- .. "self:HasFightCount() " .. tostring(self:HasFightCount())
-- )
    return self:HasNotGetStageRewards() or self:HasNotGetDailyReward() or self:HasNotGetCountRewards()
end

function OnlinePvpManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function OnlinePvpManager:HandleData(event_id, error_id, args)
    
    --获取数据, 返回一个table,每个参数的含义参加上面的key定义
    if event_id == action_config.CROSS_VS11_GET_DATA_REQ then
        -- LoggerHelper.Log("CROSS_VS11_GET_DATA_REQ .." .. PrintTable:TableToStr(args))
        self._serverData = args
        self:LoadStageRewardData()
        EventDispatcher:TriggerEvent(GameEvents.OnlinePvpGetData)
    --参与匹配
    elseif event_id == action_config.CROSS_VS11_BEGIN_MATCH_REQ then
        if error_id > 0 then
            EventDispatcher:TriggerEvent(GameEvents.BeginMatchSuccess)
        end
    -- LoggerHelper.Log("CROSS_VS11_BEGIN_MATCH_REQ .." .. PrintTable:TableToStr(args))
    --购买跨服额外次数
    elseif event_id == action_config.CROSS_VS11_CROSS_BUY_COUNT then
        -- LoggerHelper.Log("CROSS_VS11_CROSS_BUY_COUNT .." .. PrintTable:TableToStr(args))
        self._serverData[public_config.CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT] = args[1]
        local showText = LanguageDataHelper.GetContent(51219)
        GUIManager.ShowText(2, showText)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CROSS_VS11, self:GetOnlinePvpViewRedPoint())
        EventDispatcher:TriggerEvent(GameEvents.OnlinePvpGetData)
    --领取每日段位奖励
    elseif event_id == action_config.CROSS_VS11_GET_DAY_STAGE_RWD then
        -- LoggerHelper.Log("CROSS_VS11_GET_DAY_STAGE_RWD .." .. PrintTable:TableToStr(args))
        self._serverData[public_config.CROSS_VS11_KEY_DATA_DAY_STAGE_RWD] = 1
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CROSS_VS11, self:GetOnlinePvpViewRedPoint())
        EventDispatcher:TriggerEvent(GameEvents.OnlinePvpGetData)
    --领取每日参与次数奖励
    elseif event_id == action_config.CROSS_VS11_GET_DAY_COUNT_RWD then
        -- LoggerHelper.Log("CROSS_VS11_GET_DAY_COUNT_RWD .." .. PrintTable:TableToStr(args))
        for k, v in pairs(args) do
            self._serverData[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD][v] = 1
        end
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CROSS_VS11, self:GetOnlinePvpViewRedPoint())
        EventDispatcher:TriggerEvent(GameEvents.OnlinePvpGetData)
    --领取赛季功勋达标奖励
    elseif event_id == action_config.CROSS_VS11_GET_SEASON_GX_RWD then
        -- LoggerHelper.Log("CROSS_VS11_GET_SEASON_GX_RWD .." .. PrintTable:TableToStr(args))
        for k, v in pairs(args) do
            self._serverData[public_config.CROSS_VS11_KEY_DATA_TOTAL_GX_RWD][v] = 1
            self:LoadStageRewardData()
            EventDispatcher:TriggerEvent(GameEvents.OnlinePvpGetTotalReward, v)
        end
    --取消匹配
    elseif event_id == action_config.CROSS_VS11_CANCEL_MATCH_REQ then
        -- LoggerHelper.Log("CROSS_VS11_CANCEL_MATCH_REQ .." .. PrintTable:TableToStr(args))
        --通知场景状态切换
        elseif event_id == action_config.CROSS_VS11_STAGE_RESP then
        -- LoggerHelper.Log("CROSS_VS11_STAGE_RESP .." .. PrintTable:TableToStr(args))
        if args[1] == 3 then
            self._fightForce = {}
            self._fightForce[args[2]] = args[3]
            self._fightForce[args[4]] = args[5]
            EventDispatcher:TriggerEvent(GameEvents.OnlinePvpUpdateFightForce)
        end
        --结算
        elseif event_id == action_config.CROSS_VS11_SETTLE_RESP then
            -- LoggerHelper.Log("CROSS_VS11_SETTLE_RESP .." .. PrintTable:TableToStr(args))
            self:OnSettle(args)
        --获取跨服排行榜
        elseif event_id == action_config.CROSS_VS11_GET_CROSS_RANK_REQ then
            -- LoggerHelper.Log("CROSS_VS11_GET_CROSS_RANK_REQ .." .. PrintTable:TableToStr(args))
            EventDispatcher:TriggerEvent(GameEvents.OnlinePvpCrossRank, args)
        else
            LoggerHelper.Error("OnlinePvp event id not handle:" .. event_id)
    end
end

function OnlinePvpManager:GetServerData()
    return self._serverData
end

function OnlinePvpManager:LoadStageRewardData()
    local allStageRewards = OnlinePvpReward2DataHelper:GetAllId()
    local result = {}
    local args = self:GetServerData()
    local totalExploit = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_EXPLOIT]
    local exploitRewards = args[public_config.CROSS_VS11_KEY_DATA_TOTAL_GX_RWD]
    
    self._hasNotGetStageRewards = false
    for i, id in ipairs(allStageRewards) do
        local data = {}
        data["id"] = id
        data["finish"] = totalExploit >= OnlinePvpReward2DataHelper:GetExploit(id)
        data["hasGet"] = exploitRewards[id] ~= nil
        if data["finish"] and not data["hasGet"] then
            self._hasNotGetStageRewards = true
        end
        table.insert(result, data)
    end
    self._stageRewardsData = result
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CROSS_VS11, self:GetOnlinePvpViewRedPoint())
-- if self._isEnable then
--     self._imageRedPointStageReward:SetActive(self._hasNotGetStageRewards == true)
-- end
-- return self._hasNotGetStageRewards
end

function OnlinePvpManager:GetStateRewardData()
    return self._stageRewardsData
end

function OnlinePvpManager:HasNotGetStageRewards()
    return self._hasNotGetStageRewards
end

function OnlinePvpManager:HasNotGetCountRewards()
    local countReward = self._serverData[public_config.CROSS_VS11_KEY_DATA_DAY_COUNT_RWD]
    local fightCount = self._serverData[public_config.CROSS_VS11_KEY_DATA_COUNT]
    if countReward[1] == nil then
        if fightCount >= 1 then
            return true
        end
    end
    if countReward[5] == nil then
        if fightCount >= 5 then
            return true
        end
    end
    if countReward[10] == nil then
        if fightCount >= 10 then
            return true
        end
    end
    return false
end

function OnlinePvpManager:HasFightCount()
    local fightCount = self._serverData[public_config.CROSS_VS11_KEY_DATA_COUNT]
    return fightCount < self:GetTotalCount()
end

function OnlinePvpManager:HasNotGetDailyReward()
    return self._serverData[public_config.CROSS_VS11_KEY_DATA_DAY_STAGE_RWD] ~= 1
end

function OnlinePvpManager:GetOnlinePvpDataHelper()
    if OnlinePvpDataHelper == nil then
        if self:IsCrossServer() then
            OnlinePvpDataHelper = GameDataHelper.OnlinePvpDataHelper
        else
            OnlinePvpDataHelper = GameDataHelper.OnlinePvpSingleServerDataHelper
        end
    end
    return OnlinePvpDataHelper
end

function OnlinePvpManager:GetFightForce(eid)
    return self._fightForce and self._fightForce[eid] or nil
end

function OnlinePvpManager:IsCrossServer()
    return self._serverData and self._serverData[public_config.CROSS_VS11_KEY_DATA_SERVER_ID] == 0 --0表示参加的跨服匹配,非0表示本服匹配
-- return DateTimeUtil.GetServerOpenDays() >= GlobalParamsHelper.GetParamValue(575)--巅峰竞技实时1v1单服转跨服时间（单位：天）
end

function OnlinePvpManager:GetCurStageAndNextStageId()
    local curPoint = self._serverData[public_config.CROSS_VS11_KEY_DATA_TOTAL_POINT]
    return OnlinePvpDataHelper:GetIdByPoint(curPoint)
end

function OnlinePvpManager:GetTotalCount()
    if self:IsCrossServer() then
        local crossCount = GlobalParamsHelper.GetParamValue(576)--巅峰竞技实时1v1每日挑战次数
        local extraCount = self._serverData[public_config.CROSS_VS11_KEY_DATA_BUY_CROSS_COUNT]
        return crossCount + extraCount
    else
        return GlobalParamsHelper.GetParamValue(583)--巅峰竞技实时1v1可获得经验奖励场次
    end
end

function OnlinePvpManager:GetBattleDateTime()
    local now = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
    local updateDate = GlobalParamsHelper.GetParamValue(599)--巅峰竞技实时1v1赛季更新日期
    local startDateTime = nil
    local endDateTime = nil
    if now.day >= updateDate then
        startDateTime = DateTimeUtil.SomeDay(DateTimeUtil.NewDate(now.year, now.month, updateDate))
        endDateTime = DateTimeUtil.SomeDay(DateTimeUtil.AddOneMonth(startDateTime, updateDate - 1))
    else
        endDateTime = DateTimeUtil.SomeDay(DateTimeUtil.NewDate(now.year, now.month, updateDate - 1))
        startDateTime = DateTimeUtil.SomeDay(DateTimeUtil.MinusOneMonth(endDateTime, updateDate))
    end
    
    -- LoggerHelper.Log("startDateTime " .. PrintTable:TableToStr(startDateTime))
    -- LoggerHelper.Log("endDateTime " .. PrintTable:TableToStr(endDateTime))
    local startTimeStr = DateTimeUtil.FormatFullDate(startDateTime, true, false, true)
    local endTimeStr = DateTimeUtil.FormatFullDate(endDateTime, true, false, true)
    return startTimeStr .. "-" .. endTimeStr
end

function OnlinePvpManager:OnSettle(args)
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then
        self:GetOnlinePvpDataHelper()
        local serverData = {}
        serverData[public_config.INSTANCE_SETTLE_KEY_MAP_ID] = GameSceneManager:GetCurrMapID()
        serverData[public_config.INSTANCE_SETTLE_KEY_RESULT] = args[1]
        local rewards = args[5]
        if args[4] and args[4] ~= 0 then
            rewards[public_config.MONEY_TYPE_ATHLETICS] = args[4]
        end
        serverData[public_config.INSTANCE_SETTLE_KEY_ITEMS] = rewards
        -- LoggerHelper.Log("OnSettle ret: " .. args[1])
        -- ret, tgt_name, add_point, add_gx, rwd2
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = args[2]
        local defeat = LanguageDataHelper.CreateContent(args[1] == 1 and 51197 or 51198, argsTable)
        argsTable["0"] = args[3]
        local stageScore = LanguageDataHelper.CreateContent(51199, argsTable)
        local curPoint = args[6] or 0
        local orgStageId, orgNextStageId = OnlinePvpDataHelper:GetIdByPoint(curPoint)
        curPoint = curPoint + args[3]
        if curPoint < 0 then
            curPoint = 0
        end
        local curStageId, nextStageId = OnlinePvpDataHelper:GetIdByPoint(curPoint)
        argsTable["0"] = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(nextStageId))
        argsTable["1"] = OnlinePvpDataHelper:GetPoint(nextStageId) - curPoint
        local stageInfo = LanguageDataHelper.CreateContent(51200, argsTable)
        
        if curStageId == nextStageId then
            local showText = LanguageDataHelper.GetContent(51221)
            GUIManager.ShowText(2, showText)
        end
        local newStageIcon, stageChangeText = nil
        if orgNextStageId == curStageId then --升阶
            newStageIcon = OnlinePvpDataHelper:GetIcon(curStageId)
            argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(curStageId))
            stageChangeText = LanguageDataHelper.CreateContent(51204, argsTable)
        elseif orgStageId ~= curStageId then --降阶
            newStageIcon = OnlinePvpDataHelper:GetIcon(curStageId)
            argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(OnlinePvpDataHelper:GetName(curStageId))
            stageChangeText = LanguageDataHelper.CreateContent(51205, argsTable)
        end
        InstanceBalanceManager:ShowPvpBalance(serverData, defeat, stageScore, stageInfo, newStageIcon, stageChangeText)
    end
    self:GetData()
end

function OnlinePvpManager:OnEnterMap()
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then
        GameWorld.Player().server.client_loading_completed("100")--客户端加载完
        self._hasLoadPvpScene = true
    else
        if self._hasLoadPvpScene then
            GUIManager.ClosePanel(PanelsConfig.InstBalance)
            self._hasLoadPvpScene = false
        end
    end
end

function OnlinePvpManager:CanFight()
    if self._serverData then
        return (self._serverData[public_config.CROSS_VS11_KEY_DATA_COUNT] or 0) < self:GetTotalCount()
    else
        return true
    end
end

function OnlinePvpManager:GetData()
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_GET_DATA_REQ")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_GET_DATA_REQ, "")
end

function OnlinePvpManager:BeginMatch()
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_BEGIN_MATCH_REQ")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_BEGIN_MATCH_REQ, "")
end

function OnlinePvpManager:CancelMatch()
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_CANCEL_MATCH_REQ")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_CANCEL_MATCH_REQ, "")
end

function OnlinePvpManager:GetStageReward()
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_GET_DAY_STAGE_RWD")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_GET_DAY_STAGE_RWD, "")
end

function OnlinePvpManager:BuyCount(count)
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_CROSS_BUY_COUNT")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_CROSS_BUY_COUNT, tostring(count))
end

function OnlinePvpManager:GetDailyCountReward(count)
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_GET_DAY_COUNT_RWD")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_GET_DAY_COUNT_RWD, tostring(count))
end

function OnlinePvpManager:GetSeasonReward(id)
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_GET_SEASON_GX_RWD")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_GET_SEASON_GX_RWD, tostring(id))
end

function OnlinePvpManager:GetCrossRank()
    -- LoggerHelper.Log("cross_vs11_req action_config.CROSS_VS11_GET_CROSS_RANK_REQ")
    GameWorld.Player().server.cross_vs11_req(action_config.CROSS_VS11_GET_CROSS_RANK_REQ, "")
end

OnlinePvpManager:Init()
return OnlinePvpManager
