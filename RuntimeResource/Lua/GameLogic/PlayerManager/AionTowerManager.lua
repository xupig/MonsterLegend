local AionTowerManager = {}
require "PlayerManager/PlayerData/EventData"
local TaskRewardData = ClassTypes.TaskRewardData
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local AionDataHelper = GameDataHelper.AionDataHelper
local AionConfig = GameConfig.AionConfig
local InstanceManager = PlayerManager.InstanceManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceBalanceManager= PlayerManager.InstanceBalanceManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local GameSceneManager = GameManager.GameSceneManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function AionTowerManager:Init()
    self:InitCallbackFunc()
end

function AionTowerManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function AionTowerManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function AionTowerManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function AionTowerManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then
    end
end

function AionTowerManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then
        InstanceManager:EnterInstance()
    end

    if self._leaveMapId ~= mapId and MapDataHelper.GetSceneType(self._leaveMapId) == SceneConfig.SCENE_TYPE_AION_TOWER then
        -- GUIManager.ShowPanel(PanelsConfig.InstanceHall)
        if self:GetNowPos() ~= 2 then
            GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_AION)
        end
    end
end

function AionTowerManager:OnAionEnter()
    local cb = function() self:ConfirmGotoBoss() end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function AionTowerManager:ConfirmGotoBoss()
    local sceneType = GameManager.GameSceneManager:GetCurSceneType()
    self._isInScene = sceneType == SceneConfig.SCENE_TYPE_AION_TOWER
    if self._isInScene then
        local pos = GameWorld.Player():GetPosition()
        local params = pos.x *100 .. "," .. pos.y*100 .. "," .. pos.z*100
        --LoggerHelper.Error("参数====" .. tostring(params))
        GameWorld.Player().server.aion_action_req(action_config.AION_ENTER, params)
    else
        -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:BeginWaitLoading(WaitEvents.AION_ENTER)
        GameWorld.Player().server.aion_action_req(action_config.AION_ENTER, "")
    end
end

function AionTowerManager:GetAionInfo()
    return GameWorld.Player().aion_info or {}
end

--获取现在的层数
function AionTowerManager:GetNowPos()
    local data = self:GetAionInfo()
    return data[public_config.AION_NOW_LAYER] + 1
end


function AionTowerManager:GetNextGetRewardLayer(highestLayer)
    local allId = AionDataHelper:GetAllId()
    local allIdCount = #allId
    for layer = highestLayer, allIdCount do
        local targetReward = AionDataHelper:GetTargetReward(layer)
        if targetReward ~= nil then
            return layer
        end
    end
end

function AionTowerManager:HandleData(action_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("AionTower Error:" .. error_id)
        return
    elseif action_id == action_config.AION_NOTIFY_ROOM_STAGE then--3402,     --通知副本状态改变
         -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = 0--GameDataHelper.GlobalParamsHelper.GetParamValue(473)
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            --副本开始开始计时，如03:30后结束副本。副本开始计时时需要先显示评价星级
            GameWorld.Player():StartAutoFight()
            
            local instanceTime = AionDataHelper:GetTime(self:GetNowPos())
            InstanceCommonManager:StartInstanceCountDown(instanceTime)
        else
        end 
    elseif action_id == action_config.AION_SETTLE then--3403,     --结算返回
        InstanceBalanceManager:ShowBalance(args)
    else
        LoggerHelper.Error("Task event id not handle:" .. action_id)
    end
end

function AionTowerManager:HandleResp(act_id, code, args)
    if code ~= 0 then --报错了
        if act_id == action_config.AION_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.AION_ENTER)
        end
        return
    elseif act_id == action_config.AION_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.AION_ENTER)
        TaskCommonManager:RealStop()
        self:AionEnterResp(code)
    end
end

function AionTowerManager:AionEnterResp(code)
    -- LoggerHelper.Log("Ash: AionEnterResp: " .. code)
    if code == 0 then
        if self._isInScene then
            GUIManager.ClosePanel(PanelsConfig.InstBalance)
        else
            GUIManager.ClosePanel(PanelsConfig.DailyActivity)
            GUIManager.ClosePanel(PanelsConfig.InstanceHall)
        end
    end
end

AionTowerManager:Init()
return AionTowerManager
