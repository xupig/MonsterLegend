local SkyDropManager = {}
local InstanceManager = PlayerManager.InstanceManager
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GameSceneManager = GameManager.GameSceneManager
local DateTimeUtil = GameUtil.DateTimeUtil
local CollectManager = GameManager.CollectManager
local SystemInfoManager = GameManager.SystemInfoManager
local error_code = GameWorld.error_code
local TaskCommonManager = PlayerManager.TaskCommonManager

function SkyDropManager:Init()
	self:InitCallbackFunc()
end

function SkyDropManager:InitCallbackFunc()
	self._backToCityCb = function ()
		self:BackToMainCity()
	end
end

function SkyDropManager:OnPlayerEnterWorld()
	EventDispatcher:AddEventListener(GameEvents.Daily_Sky_Drop_Back_To_City, self._backToCityCb)
end

function SkyDropManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.Daily_Sky_Drop_Back_To_City, self._backToCityCb)
end

function SkyDropManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
    	return
    end

    if action_id == action_config.SKY_DROP_COUNT_DOWN then           --11201,     --天降秘宝
        self:UpdateSkyDropCD(args)
    end
end

function SkyDropManager:UpdateSkyDropCD(args)
    local timeleft = args[1] + GlobalParamsHelper.GetParamValue(939) - DateTimeUtil.GetServerTime()
    self._stopCollectTimerId = TimerHeap:AddSecTimer(timeleft, 0, 0, function ()
        EventDispatcher:TriggerEvent(GameEvents.ON_BREAK_COLLECT)
        TimerHeap:DelTimer(self._stopCollectTimerId)
    end)
    GUIManager.ShowPanel(PanelsConfig.SkyDrop,args)
end

function SkyDropManager:ShowSkyDropTip(change)
    if change then
        local count = GameWorld.Player().sky_drop_info[public_config.SKY_DROP_COLLECT_TIMES]
        if count then
            local maxCount = GlobalParamsHelper.GetParamValue(937)
            local args = LanguageDataHelper.GetArgsTable()
            args["0"] = count.."/"..maxCount
            local str = LanguageDataHelper.CreateContentWithArgs(87300,args)
            GUIManager.ShowText(2,str)
        end
    end
end

function SkyDropManager:BackToMainCity()
    --护送女神中
    if GameWorld.Player().guard_goddess_id > 0 then
        SystemInfoManager:ShowClientTip(error_code.ERR_GUARD_GODDESS_HAS)
        return
    end
    local cfg = GlobalParamsHelper.GetParamValue(935)
    for mapId,v in pairs(cfg) do
    	if GameSceneManager:GetCurrMapID() == mapId then
            local str = LanguageDataHelper.CreateContent(87305)
            GUIManager.ShowText(2,str)
        else
            TaskCommonManager:RealStop()
        	InstanceManager:CommonChangeMap(mapId)
        end
    end
end
SkyDropManager:Init()
return SkyDropManager