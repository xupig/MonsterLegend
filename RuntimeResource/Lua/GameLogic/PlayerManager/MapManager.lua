local MapManager = {}
local action_config = require("ServerConfig/action_config")
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local RegionalMapDataHelper = GameDataHelper.RegionalMapDataHelper
local GameSceneManager = GameManager.GameSceneManager
local TeamData = PlayerManager.PlayerDataManager.teamData
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local MapDataHelper = GameDataHelper.MapDataHelper
local UnlockMapDataHelper = GameDataHelper.UnlockMapDataHelper
local EventActivityDataHelper = GameDataHelper.EventActivityDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EventActivityManager = PlayerManager.EventActivityManager
local PlayerActionManager = GameManager.PlayerActionManager
local TaskCommonManager 

function MapManager:Init()
    self._cacheMonstersByMapId = {}
    self._cacheNpcsByMapId = {}
    self._cacheTeleportsByMapId = {}
    self._cacheCollectPointsByMapId = {}
    self:InitCallbackFunc()
end

function MapManager:InitCallbackFunc()
    self._onSingStateChangeCB = function()self:OnSingStateChange() end
    self._onMapUnlockDataChangeCB = function()self:OnMapUnlockDataChange() end
    self._onCancelTeleport = function()self:OnCancelTeleport() end
    self._onBreakTeleport = function()self:OnBreakTeleport() end
    self._onTriggerTeleportUnlock = function(unlock_id)self:SetTeleportUnlock(unlock_id) end
    self._onTriggerZoneUnlock = function(unlock_id)self:SetMapUnlock(unlock_id) end
end

function MapManager:OnPlayerEnterWorld()
    TaskCommonManager = PlayerManager.TaskCommonManager
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_SING_STATE, self._onSingStateChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MAP_UNLOCK_DATA, self._onMapUnlockDataChangeCB)
    EventDispatcher:AddEventListener(GameEvents.ON_CANCEL_TELEPORT, self._onCancelTeleport)
    EventDispatcher:AddEventListener(GameEvents.ON_BREAK_TELEPORT, self._onBreakTeleport)
    EventDispatcher:AddEventListener(GameEvents.TriggerTeleportUnlock, self._onTriggerTeleportUnlock)
    EventDispatcher:AddEventListener(GameEvents.TriggerZoneUnlock, self._onTriggerZoneUnlock)
    self:OnMapUnlockDataChange()
end

function MapManager:OnPlayerLeaveWorld()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_SING_STATE, self._onSingStateChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MAP_UNLOCK_DATA, self._onMapUnlockDataChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CANCEL_TELEPORT, self._onCancelTeleport)
    EventDispatcher:RemoveEventListener(GameEvents.ON_BREAK_TELEPORT, self._onBreakTeleport)
    EventDispatcher:RemoveEventListener(GameEvents.TriggerTeleportUnlock, self._onTriggerTeleportUnlock)
    EventDispatcher:RemoveEventListener(GameEvents.TriggerZoneUnlock, self._onTriggerZoneUnlock)
end

function MapManager:OnSingStateChange()
    -- LoggerHelper.Log("Ash: OnSingStateChange")
    local entity = GameWorld.Player()
    if entity ~= nil then
        if entity.sing_state == public_config.SING_STATE_NONE then
            self:StopClientTeleport()
        end
    end
end

function MapManager:OnMapUnlockDataChange()
    -- LoggerHelper.Log("Ash: OnMapUnlockDataChange before: " .. PrintTable:TableToStr(self._lastMapUnlockData))
    if self._lastMapUnlockData == nil then
        self._lastMapUnlockData = {}-- 记录上次变化但未打开地图的解锁数据
        -- 复制一份数据，直接赋值会保留引用关系，无法判断
        local temp = self:GetMapUnlockData()
        for k, v in pairs(temp) do
            self._lastMapUnlockData[k] = {}
            for kk, vv in pairs(v) do
                self._lastMapUnlockData[k][kk] = vv
            end
        end
    else
        local curMapUnlockData = self:GetMapUnlockData()
        if self._lastChangeUnlockData == nil then
            self._lastChangeUnlockData = {}-- 通过以下判断找出这次变化的数据，用于做动画显示
        end
        if self._lastChangeUnlockDataForWorldMap == nil then
            self._lastChangeUnlockDataForWorldMap = {}-- 通过以下判断找出这次变化的数据，用于做动画显示
        end
        for k, v in pairs(curMapUnlockData) do
            if self._lastMapUnlockData[k] == nil then
                self._lastChangeUnlockDataForWorldMap[k] = v
                self._lastChangeUnlockData[k] = v
                for kv, vv in pairs(v) do
                    self:TriggerUnlockRegion(k, kv)
                end
            else
                for kv, vv in pairs(v) do
                    if self._lastMapUnlockData[k][kv] == nil then
                        if self._lastChangeUnlockData[k] == nil then
                            self._lastChangeUnlockData[k] = {}
                        end
                        self._lastChangeUnlockData[k][kv] = 1
                        self:TriggerUnlockRegion(k, kv)
                    end
                end
            end
        end
        for k, v in pairs(self._lastChangeUnlockData) do
            if self._lastMapUnlockData[k] == nil then
                self._lastMapUnlockData[k] = {}
            end
            for kk, vv in pairs(v) do
                self._lastMapUnlockData[k][kk] = vv
            end
        end
    -- self._lastMapUnlockData = curMapUnlockData
    end
-- LoggerHelper.Log("Ash: OnMapUnlockDataChange end: " .. PrintTable:TableToStr(self._lastMapUnlockData))
-- LoggerHelper.Log("Ash: OnChangeMapUnlockDataChange: " .. PrintTable:TableToStr(self._lastChangeUnlockData))
end

function MapManager:OnCancelTeleport()
    -- LoggerHelper.Log("Ash: OnCancelTeleport")
    self:CancelTeleport()
end

function MapManager:OnBreakTeleport()
    self:CancelTeleport()
end

function MapManager:HandleResp(act_id, code, args)
    if act_id == action_config.INSTANCE_CLIENT_TELEPORT then
        if code ~= 0 then --报错了
            -- LoggerHelper.Log("Ash: INSTANCE_CLIENT_TELEPORT Error:" .. code)
            self:TeleportFail()
            return
        end
        --客户端请求传送回调
        -- LoggerHelper.Log("Ash: INSTANCE_CLIENT_TELEPORT:" .. PrintTable:TableToStr(args))
        self:ProcessClientTeleport(args)
    elseif act_id == action_config.SPACE_GET_LINE_INFO then
        -- LoggerHelper.Log("Ash: SPACE_GET_LINE_INFO:" .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.OnSpaceGetLineInfo, args)
    -- elseif act_id == action_config.SPACE_GO_TO_LINE then
    --     LoggerHelper.Log("Ash: SPACE_GO_TO_LINE:" .. PrintTable:TableToStr(args))
    --     EventDispatcher:TriggerEvent(GameEvents.OnSpaceGotoLine, args)
    else
        LoggerHelper.Error("Map action id not handle:" .. act_id)
    end
end

function MapManager:GetMapUnlockData()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.map_unlock_data or {}
end

function MapManager:GetTeleportUnlockData()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.teleport_unlock_data or {}
end

function MapManager:GetMonsterByMapId(mapId)
    if self._cacheMonstersByMapId[mapId] == nil then
        self._cacheMonstersByMapId[mapId] = GameSceneManager:GetMonsterInfo(mapId)-- 获取当前地图所有怪物
    end
    return self._cacheMonstersByMapId[mapId]
end

function MapManager:GetNPCByMapId(mapId)
    if self._cacheNpcsByMapId[mapId] == nil then
        self._cacheNpcsByMapId[mapId] = GameSceneManager:GetNPCInfo(mapId)-- 获取当前地图所有NPC
    end
    return self._cacheNpcsByMapId[mapId]
end

function MapManager:GetCollectPointInfo(mapId)
    if self._cacheCollectPointsByMapId[mapId] == nil then
        self._cacheCollectPointsByMapId[mapId] = GameSceneManager:GetCollectPointInfo(mapId)-- 获取当前地图所有采集点
    end
    return self._cacheCollectPointsByMapId[mapId]
end

function MapManager:GetTeleportInfo(mapId)
    if self._cacheTeleportsByMapId[mapId] == nil then
        self._cacheTeleportsByMapId[mapId] = GameSceneManager:GetTeleportInfo(mapId)-- 获取当前地图所有传送点
    end
    return self._cacheTeleportsByMapId[mapId]
end

function MapManager:GetRevivePoint(mapId)
    local revivePoints = MapDataHelper.GetRevivePoints(mapId)
    local posPointMap = GameSceneManager:GetPositionPointInfo(mapId)
    local factionId = GameWorld.Player().faction_id
    local result = {}
    if revivePoints ~= nil and revivePoints[factionId] ~= nil then
        for i, v in ipairs(revivePoints[factionId]) do
            if posPointMap[v] ~= nil then
                result[v] = posPointMap[v].pos
            end
        end
    end
    return result
end

function MapManager:GetFadeOutWorldMapRegion()
    return self._lastChangeUnlockDataForWorldMap or {}
end

function MapManager:ResetFadeOutWorldMapRegion()
    self._lastChangeUnlockDataForWorldMap = nil
end

function MapManager:GetFadeOutRegionLock(mapId)
    if self._lastChangeUnlockData == nil or self._lastChangeUnlockData[mapId] == nil
        or GameSceneManager:GetCurrMapID() ~= mapId then
        return {}
    end
    local unlockMapIds = UnlockMapDataHelper:GetIdUnlockMapByMapId(mapId)
    local result = {}
    local curMapUnlockData = self._lastChangeUnlockData[mapId]
    for i, v in ipairs(unlockMapIds) do
        if curMapUnlockData ~= nil then
            local triggerIds = UnlockMapDataHelper:GetTriggerId(v)
            local hasUnlock = false
            for i, v in ipairs(triggerIds) do
                if curMapUnlockData[v] ~= nil then
                    hasUnlock = true
                end
            end
            if hasUnlock then
                result[v] = UnlockMapDataHelper:GetCoverPosition(v)
            end
        end
    end
    -- LoggerHelper.Log("Ash: GetFadeOutRegionLock: " .. PrintTable:TableToStr(result))
    self._lastChangeUnlockData = nil
    return result
end

function MapManager:TriggerUnlockRegion(mapId, triggerId)
    local ids = UnlockMapDataHelper:GetIdUnlockMapByMapIdAndTriggerId(mapId, triggerId)
    for i, v in ipairs(ids) do
        if self:CheckRegionHasUnlock(v) == false then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(UnlockMapDataHelper:GetName(v))
            GUIManager.ShowText(1, LanguageDataHelper.CreateContent(10587, argsTable), 5)-- 发现【{0}】
        end
    end
end

function MapManager:CheckRegionHasUnlock(unlockMapId)
    if self._lastMapUnlockData == nil then
        return false
    end
    local mapId = UnlockMapDataHelper:GetMapId(unlockMapId)
    if self._lastMapUnlockData[mapId] ~= nil then
        local triggerIds = UnlockMapDataHelper:GetTriggerId(unlockMapId)
        for i, v in ipairs(triggerIds) do
            if self._lastMapUnlockData[mapId][v] ~= nil then
                return true
            end
        end
    end
    return false
end

function MapManager:GetRegionLock(mapId)
    -- LoggerHelper.Log("Ash: GetRegionLock:" .. mapId)
    local unlockMapIds = UnlockMapDataHelper:GetIdUnlockMapByMapId(mapId)
    local unlockData = self:GetMapUnlockData()
    local result = {}
    local curMapUnlockData = unlockData[mapId]
    for i, v in ipairs(unlockMapIds) do
        if curMapUnlockData ~= nil then
            local triggerIds = UnlockMapDataHelper:GetTriggerId(v)
            local hasUnlock = false
            for i, v in ipairs(triggerIds) do
                if curMapUnlockData[v] ~= nil then
                    hasUnlock = true
                end
            end
            if hasUnlock == false then
                result[v] = UnlockMapDataHelper:GetCoverPosition(v)
            end
        else
            result[v] = UnlockMapDataHelper:GetCoverPosition(v)
        end
    end
    return result
end

function MapManager:GetTeamMember()
    return TeamData:GetMyTeamMemberInfosWithOutMe()
end

function MapManager:GetMapRegionalInfo(mapId)
    if mapId == nil then
        mapId = GameSceneManager:GetCurrMapID()
    end
    -- LoggerHelper.Log("Ash: GetMapRegionalInfo: " .. tostring(mapId))
    return mapId, self:GetRegionalInfoByMapId(mapId)
end

function MapManager:GetRegionalInfoByMapId(mapId)
    local worldMapId = WorldMapDataHelper:GetWorldMapDataIdBySceneId(mapId)
    if worldMapId ~= nil then
        local rmds = RegionalMapDataHelper:GetRegionalMapDataIdsByModuleId(worldMapId)
        return worldMapId, rmds
    end
    return nil
end

function MapManager:GetNearestTelePoint(mapId, pos)
    local worldMapId, rmds = self:GetRegionalInfoByMapId(mapId)
    if worldMapId == nil then
        return 0
    end
    local minDis = 10000
    local minDisTelPoint = 0
    for i, v in ipairs(rmds) do
        local dis = Vector3.Distance(pos, RegionalMapDataHelper:GetTrueLocation(v))
        if dis < minDis then
            minDis = dis
            minDisTelPoint = v
        end
    end
    return minDisTelPoint
end

function MapManager:IsTeleportTriggerUnlock(teleportId)
    local mapId = WorldMapDataHelper:GetSceneId(RegionalMapDataHelper:GetModuleId(teleportId))
    local teleUnlockData = self:GetTeleportUnlockData()
    if teleUnlockData[mapId] ~= nil then
        local triggerIds = RegionalMapDataHelper:GetTransferTriggerId(teleportId)
        if triggerIds == nil then
            LoggerHelper.Error("empty triggerIds: " .. teleportId)
            return false
        end
        for i, v in ipairs(triggerIds) do
            if teleUnlockData[mapId][v] ~= nil then
                return true
            end
        end
    end
    return false
end

function MapManager:IsTeleportLevelUnlock(teleportId)
    local level = RegionalMapDataHelper:GetTransferLv(teleportId)
    return GameWorld.Player().level >= level
end

function MapManager:GotoDistristShowEvent(eventId)
    local eas = EventActivityManager:GetCurrEventActivitiesByMapId()
    local mapUnlockData = MapManager:GetMapUnlockData()
    -- LoggerHelper.Log("Ash: GotoDistristShowEvent: eas " .. PrintTable:TableToStr(eas))
    -- LoggerHelper.Log("Ash: GotoDistristShowEvent: eventId " .. eventId)
    for _, eaId in ipairs(eas) do
        if eaId == eventId then
            local mapId = EventActivityDataHelper:GetMapId(eaId)
            if mapUnlockData[mapId] == nil then
                GUIManager.ShowText(1, LanguageDataHelper.CreateContent(57022), 5)--57022地图未解锁
                return false
            else
                EventDispatcher:TriggerEvent(GameEvents.OnShowMapFromPrestige, mapId, eventId)
                return true
            end
        end
    end
    GUIManager.ShowText(1, LanguageDataHelper.CreateContent(57021), 5)--57021事件未开启
    return false
end

function MapManager:ShowRecommendMonsterPoint()
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback() 
    PlayerActionManager:StopAllAction()
    TaskCommonManager:SetCurAutoTask(nil, nil)
    GUIManager.ShowPanel(PanelsConfig.WorldMap, "ShowRecommend")
end

function MapManager:ProcessClientTeleport(args)
    local regionId = tonumber(args[1])
    -- LoggerHelper.Log("Ash: ProcessClientTeleport: " .. tostring(regionId))
    self:BeginClientTeleport(regionId)
end

function MapManager:BeginClientTeleport(regionId)
    -- LoggerHelper.Log("Ash: BeginClientTeleport")
    GUIManager.ClosePanel(PanelsConfig.WorldMap)
    --GUIManager.ShowPanel(PanelsConfig.Teleporting, regionId)
    self._teleportRegionId = regionId
    GUIManager.ShowPanel(PanelsConfig.Collect, nil, function()
        EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_ADD_MODE, 5)
    end)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLIENT_TELEPORT_BEGIN)
end

--获取当前传送地图区域ID
function MapManager:GetTeleportRegionId()
    return self._teleportRegionId
end

function MapManager:StopClientTeleport()
    -- LoggerHelper.Log("Ash: StopClientTeleport")
    GUIManager.ClosePanel(PanelsConfig.WorldMap)
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COLLECT_REMOVE_MODE, 5)
    --GUIManager.ClosePanel(PanelsConfig.Teleporting)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLIENT_TELEPORT_END)
end

function MapManager:BeginTeleport(regionalMapId)
    GameWorld.Player().server.instance_action_cell_req(action_config.INSTANCE_CLIENT_TELEPORT, tostring(regionalMapId))
    self:BeginClientTeleport(regionalMapId)
    GameWorld.Player():SetTeleportSingState(true)
end

function MapManager:CancelTeleport()
    GameWorld.Player().server.sing_action_cell_req(action_config.SING_BREAK, tostring(public_config.SING_ID_TELEPORT))
    self:TeleportFail()
end

function MapManager:TeleportFail()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLIENT_TELEPORT_FAIL)
    self:StopClientTeleport()
    GameWorld.Player():SetTeleportSingState(false)
end

function MapManager:SetMapUnlock(regionId)
    local mapId = GameSceneManager:GetCurrMapID()
    GameWorld.Player().server.set_map_unlock(tostring(mapId), tostring(regionId))
end

function MapManager:SetTeleportUnlock(teleportId)
    local mapId = GameSceneManager:GetCurrMapID()
    GameWorld.Player().server.set_teleport_unlock(tostring(mapId), tostring(teleportId))
end

function MapManager:SpaceGetLineInfo()
    local mapId = GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: SpaceGetLineInfo: " .. mapId)
    GameWorld.Player().server.space_action_req(action_config.SPACE_GET_LINE_INFO, tostring(mapId))
end

function MapManager:SpaceGoToLine(lineId)
    local mapId = GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: SpaceGetLineInfo: " .. mapId .. " " .. lineId)
    GameWorld.Player().server.space_action_req(action_config.SPACE_GO_TO_LINE, tostring(mapId) .. "," .. tostring(lineId))
end

MapManager:Init()
return MapManager
