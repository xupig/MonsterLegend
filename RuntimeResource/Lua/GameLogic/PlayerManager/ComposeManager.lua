local ComposeManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local error_code = GameWorld.error_code
local PanelsConfig = GameConfig.PanelsConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local ComposeDataHelper = GameDataHelper.ComposeDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData

function ComposeManager:Init()
	self._composeItemMatIds = ComposeDataHelper:GetComposeMatIds(public_config.FUNCTION_ID_COMPOSITE_PROP)
	self._composeGemMatIds = ComposeDataHelper:GetComposeMatIds(public_config.FUNCTION_ID_COMPOSITE_GEM)
	self._checkComposeItem = function (itemId)
		self:CheckComposeItem(itemId)
	end

	self._checkComposeGem = function (gemId)
		self:CheckComposeGem(gemId)
	end
end

function ComposeManager:HandleResp(act_id, code, args)
	if act_id == action_config.COMPOSE_EQUIP and code == 0 or error_code.ERR_COMPOSE_EQUIP_FAIL then
		EventDispatcher:TriggerEvent(GameEvents.COMPOSE_EQUIP_SUCCESS,code)
	end
	if code > 0 then
		return
	end
	if act_id == action_config.COMPOSE_ITEM then
		EventDispatcher:TriggerEvent(GameEvents.COMPOSE_ITEM_SUCCESS)
	elseif act_id == action_config.DRAGON_SPIRIT_COMPOSE then
		EventDispatcher:TriggerEvent(GameEvents.COMPOSE_SOUL_SUCCESS)
	end


	-- local data = {["id"]=MessageBoxType.BuySucc,["value"]={["count"]=1,["composeItemId"]= 406}}
 --    GameManager.GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
end

function ComposeManager:OnPlayerEnterWorld()
	self:CheckAllCompose()
	EventDispatcher:AddEventListener(GameEvents.Refresh_Compose_Item_Red_Point_State,self._checkComposeItem)
	EventDispatcher:AddEventListener(GameEvents.Refresh_Compose_Gem_Red_Point_State,self._checkComposeGem)
end

function ComposeManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Compose_Item_Red_Point_State,self._checkComposeItem)
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Compose_Gem_Red_Point_State,self._checkComposeGem)
end

function ComposeManager:CheckAllCompose()
	for itemId,v in pairs(self._composeItemMatIds) do
		if self:CheckComposeItem(itemId) then
			break
		end
	end

	for gemId,v in pairs(self._composeGemMatIds) do
		if self:CheckComposeGem(gemId) then
			break
		end
	end
end

function ComposeManager:CheckComposeItem(itemId)
	local state = false
	if self._composeItemMatIds[itemId] then
		for i=1,#self._composeItemMatIds[itemId] do
			local itemCfg = self._composeItemMatIds[itemId][i]
			if self:CheckMatEnough(itemCfg.item1_id) and self:CheckMatEnough(itemCfg.item2_id) 
				and self:CheckMatEnough(itemCfg.item3_id) then
				state = true
				break
			end
		end
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_COMPOSITE_PROP, state)
	end
	return state
end

function ComposeManager:CheckComposeGem(gemId)
	local state = false
	if self._composeGemMatIds[gemId] then
		for i=1,#self._composeGemMatIds[gemId] do
			local itemCfg = self._composeGemMatIds[gemId][i]
			if self:CheckMatEnough(itemCfg.item1_id) and self:CheckMatEnough(itemCfg.item2_id) 
				and self:CheckMatEnough(itemCfg.item3_id) then
				state = true
				break
			end
		end
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_COMPOSITE_GEM, state)  
	end
	return state
end

function ComposeManager:CheckMatEnough(matStruct)
	if matStruct then
		for matId,needCount in pairs(matStruct) do
			return bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(matId) >= needCount
		end
	end
	return true
end

----------------------------------合成请求-----------------------------------------
function ComposeManager:RequestComposeItem(composeId,autoCompose)
	GameWorld.Player().server.compose_action_req(action_config.COMPOSE_ITEM,composeId,autoCompose,0,0,0,0)
end

function ComposeManager:RequestComposeEquip(composeId,pos1,pos2,pos3,pos4,pos5)
	GameWorld.Player().server.compose_action_req(action_config.COMPOSE_EQUIP,composeId,pos1,pos2,pos3,pos4,pos5)
end

--龙魂合成
function ComposeManager:RequestComposeDragonSoul(composeId)
	GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_COMPOSE,composeId,0)
	
end

--检查装备是否匹配
-- function ComposeManager:CheckIsMaterial(itemData)
--     if itemData:GetItemType() ~= public_config.ITEM_ITEMTYPE_EQUIP then
--         return false
--     end
    
--     --部位匹配
--     local equipPart = itemData:GetType()
--     local partList = self._matchPartMap[self._selectedComposeData.equip_part]
--     local partMatch = false
--     for i=1,#partList do
--         if partList[i] == equipPart then
--             partMatch = true
--         end
--     end
--     if partMatch == false and 
--         return false
--     end

--     --星数匹配
--     if itemData.cfgData.star ~= self._selectedComposeData.stuff_star then
--         return false
--     end

--     --阶数匹配
--     if itemData.cfgData.grade ~= self._selectedComposeData.equip_grade then
--         return false
--     end

--     return true
-- end
ComposeManager:Init()
return ComposeManager