local UpgradeManager = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GUIManager = GameManager.GUIManager
local DailyConfig = GameConfig.DailyConfig
local public_config = GameWorld.public_config
local CircleTaskManager = PlayerManager.CircleTaskManager
local ExpInstanceManager = PlayerManager.ExpInstanceManager
local EscortPeriManager = PlayerManager.EscortPeriManager
local UpgradeGuideType = GameConfig.EnumType.UpgradeGuideType
function UpgradeManager:Init()
    self:InitCallbackFunc()
end

function UpgradeManager:InitCallbackFunc()
    --LoggerHelper.Error("UpgradeManager:InitCallbackFunc()")
    self._onDailyClick = function(follow)self:DoFollowEvent(follow) end
end

function UpgradeManager:OnPlayerEnterWorld()
    --LoggerHelper.Error("UpgradeManager:OnPlayerEnterWorld()")
 
    EventDispatcher:AddEventListener(GameEvents.ON_UPGRADE_CLICK, self._onDailyClick)
end

function UpgradeManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_UPGRADE_CLICK, self._onDailyClick)
end


function UpgradeManager:DoFollowEvent(follow)
    if not follow or type(follow) ~= "table" then
        LoggerHelper.Error("Invalid Event follow :" .. tostring(follow))
        return
    end

    if follow[1] == DailyConfig.FOLLOW_OPEN_PANEL then-- 1.打开界面，填功能ID
        GUIManager.ShowPanelByFunctionId(follow[2][1])
    elseif follow[1] == 2 then-- 2.事件,填事件名称
        EventDispatcher:TriggerEvent(follow[2][1])
    end



end


function UpgradeManager:GetUpgradeData()
    local followNew = {}
    local followData =  GlobalParamsHelper.GetNSortValue(870)

    for i,followItemData in ipairs(followData) do

        local isopen = false
        if i == UpgradeGuideType.Gold then
            isopen = (not CircleTaskManager:IsCompleteCircleTask()) and FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        elseif i == UpgradeGuideType.Exp then
            isopen = ExpInstanceManager:GetExpInstanceCanEnter() and FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        elseif i == UpgradeGuideType.Escort then
            isopen = EscortPeriManager:IsHasTimes() and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUARD_GODDESS)
        elseif i == UpgradeGuideType.Map then
            isopen = true
        end


        if isopen then
            table.insert(followNew,followItemData)
        end

    end
    return followNew
end

UpgradeManager:Init()
return UpgradeManager