local VIPLevelGiftManager = {}
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local VipLevelUpDataHelper = GameDataHelper.VipLevelUpDataHelper
local CommonConfig = GameConfig.CommonConfig
-- 未达成，已领取，已错过，未领取
local COLL_TYPE_COLLECTED = CommonConfig.VIP_GIFT_TYPE_COLLECTED--已领取
local COLL_TYPE_CANCOLL = CommonConfig.VIP_GIFT_TYPE_CANCOLL--可领取
local COLL_TYPE_CONDITION = CommonConfig.VIP_GIFT_TYPE_UNFINISH--未达成
local COLL_TYPE_MISS = CommonConfig.VIP_GIFT_TYPE_MISS--已错过

function VIPLevelGiftManager:Init()
    
end

function VIPLevelGiftManager:OnPlayerEnterWorld()
    self._onVipGiftResp = function(actId, code, args) self:OnVipGiftActionResp(actId, code, args) end
    EventDispatcher:AddEventListener(GameEvents.ON_ACTION_RESP, self._onVipGiftResp)
    self._onRedDotFresh = function() self:UpdateVipGiftReddot() end
    EventDispatcher:AddEventListener(GameEvents.OnRefreshVipGiftReddot, self._onRedDotFresh)
    self._onEnterMap = function() self:GetVipLevelHistory() self:UpdateVipGiftReddot() end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function VIPLevelGiftManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ACTION_RESP, self._onVipGiftResp)
    EventDispatcher:RemoveEventListener(GameEvents.OnRefreshVipGiftReddot, self._onRedDotFresh)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function VIPLevelGiftManager:UpdateVipGiftReddot()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_VIPGIFT,self:GetVipGiftRedDot())
    EventDispatcher:TriggerEvent(GameEvents.OnRefreshVipGiftView)
end

function VIPLevelGiftManager:OnVipGiftActionResp(actId, code, args)
    if code ~= 0 then
        if actId == action_config.VIP_LEVEL_UP_REWARD then
    
        elseif actId == action_config.VIP_LEVEL_UP_RECORD then
            self:SaveHistory(args)
        end
        return
    end
    if actId == action_config.VIP_LEVEL_UP_REWARD then
        -- 不用管，其他途径会刷界面
        EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
    elseif actId == action_config.VIP_LEVEL_UP_RECORD then
        self:SaveHistory(args)
        EventDispatcher:TriggerEvent(GameEvents.OnRefreshVipGiftView)
    end
end

-- 封装奖励table
function VIPLevelGiftManager:GetRewardById(id)
    local localRewardData = VipLevelUpDataHelper:GetReward(id)
    local _iconList = {}
    for k, v in pairs(localRewardData) do
        table.insert(_iconList, {_id=k, _num=v, _isBind = true})
    end
    return _iconList
end

function VIPLevelGiftManager:GetGiftLevel(id)
    return VipLevelUpDataHelper:GetLevel(id)
end
--[[

LoggerHelper.Log({
    [1] = GameWorld.Player().vip_level_up_info[1],
})
LoggerHelper.Log(PlayerManager.VIPLevelGiftManager:GetHistory())

--]]

-- 返回值：
-- arg1 : true / false 是否有服务器数量
-- arg2 : int 服务器数量
-- arg3 : int 本地表格数量
function VIPLevelGiftManager:GetServerCount(id)
    local localData = VipLevelUpDataHelper.GetVipLevelUp(id)
    local localCount = localData.count
    if localCount==nil or localCount==0 then return false end
    return true, self:GetHistoryCount(id), localCount
end


function VIPLevelGiftManager:GetCollectStatus(id)
    if GameWorld.Player() == nil then return end
    local vip_level_up_info = GameWorld.Player().vip_level_up_info
    -- local history = self:GetHistory()
    local collect_type = COLL_TYPE_CONDITION
    local localData = VipLevelUpDataHelper.GetVipLevelUp(id)
    if vip_level_up_info[id] == nil then--未达成
        collect_type = COLL_TYPE_CONDITION
    else
        if vip_level_up_info[id] == 1 then--已领取
            collect_type = COLL_TYPE_COLLECTED
        elseif vip_level_up_info[id] == 0 then
            -- 可领取但是数量已经没有了：已错过
            local serverCount = self:GetHistoryCount(id)
            local localCount = localData.count or 0
            -- 可领取，数量有或者没有限制数量：可领取
            if localCount == 0 then --无限制
                collect_type = COLL_TYPE_CANCOLL
            else
                if localCount > serverCount then--数量有
                    collect_type = COLL_TYPE_CANCOLL
                else--已错过
                    collect_type = COLL_TYPE_MISS
                end
            end
        end
    end
    return collect_type
end

function VIPLevelGiftManager:PackVipGiftLocalDatas()
    if GameWorld.Player() == nil then return end
    local vip_level_up_info = GameWorld.Player().vip_level_up_info--0：可领；1：已领取（nil代表条件不够）
    -- 历史
    -- 可领取
    local ids = VipLevelUpDataHelper:GetAllId()
    local vip_lvup_datas = {}
    local vip_can_collect = {}
    local vip_no_collect = {}
    for _, id in pairs(ids) do
        local itemData = VipLevelUpDataHelper.GetVipLevelUp(id)
        if vip_level_up_info[id] == nil then--条件不够
            table.insert(vip_no_collect, 1, itemData)
        else
            if vip_level_up_info[id] ~= 1 and self:IsQualitfied(id, itemData.count) then--可领取
                table.insert(vip_can_collect, 1, itemData)
            else--已领取
                table.insert(vip_lvup_datas, itemData)
            end
        end
    end
    for i=1, #vip_no_collect do
        table.insert(vip_lvup_datas, 1, vip_no_collect[i])
    end
    for i=1, #vip_can_collect do
        table.insert(vip_lvup_datas, 1, vip_can_collect[i])
    end
    return vip_lvup_datas
end


function VIPLevelGiftManager:IsQualitfied(id, count)
    if count ~= nil and count > 0 then
        local serverCount = self:GetHistoryCount(id)
        return serverCount < count
    end
    return true
end

function VIPLevelGiftManager:SaveHistory(history_args)
    self._history = history_args
end

function VIPLevelGiftManager:GetHistory()
    return self._history
end

function VIPLevelGiftManager:GetHistoryCount(id)
    local history = self:GetHistory()
    if history == nil or history[id] == nil then return 0 end
    return history[id]
end

function VIPLevelGiftManager:GetVipGiftRedDot()
    local ids = VipLevelUpDataHelper:GetAllId()
    for _, id in pairs(ids) do
        local coll_type = VIPLevelGiftManager:GetCollectStatus(id)
        if coll_type == COLL_TYPE_CANCOLL then
            return true
        end
    end
    return false
end

function VIPLevelGiftManager:GetVipLevelGift(id)
    GameWorld.Player().server.vip_level_up_action_req(action_config.VIP_LEVEL_UP_REWARD, tostring(id))
end

function VIPLevelGiftManager:GetVipLevelHistory()
    GameWorld.Player().server.vip_level_up_action_req(action_config.VIP_LEVEL_UP_RECORD, "")
end

VIPLevelGiftManager:Init()

return VIPLevelGiftManager

--[[
vip_level_up_action_req
on_action_resp

action_id:
    VIP_LEVEL_UP_REWARD = 11401, --领取
    VIP_LEVEL_UP_RECORD = 11402, --获取记录

数据:
        <vip_level_up_info>
            <Type> LUA_TABLE </Type>
            <Flags> BASE_AND_CLIENT </Flags>
            <Persistent> true </Persistent>
        </vip_level_up_info>

vip_level_up_info = {[xml_id] = 0/1,...} --0：可领；1：已领取（nil代表条件不够）

错误码：
    ERR_VIP_LEVEL_UP_TIMES_LIMIT    = 14851, --次数已达上限
    ERR_VIP_LEVEL_UP_CONDITIONS_LIMIT   = 14852, --条件不足
    ERR_VIP_LEVEL_UP_REWARD_GOT     = 14853, --已领取

邮件ID：
    MAIL_ID_VIP_LEVEL_UP                = 1164, --vip冲级礼包，背包满
--]]