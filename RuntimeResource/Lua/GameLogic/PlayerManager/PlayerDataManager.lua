require "PlayerManager/PlayerData/BagData"
require "PlayerManager/PlayerData/TaskData"
require "PlayerManager/PlayerData/TeamData"
require "PlayerManager/PlayerData/CombatData"
require "PlayerManager/PlayerData/SkillData"
require "PlayerManager/PlayerData/EventActivityData"
require "PlayerManager/PlayerData/FortData"
require "PlayerManager/PlayerData/CompetitionData"
require "PlayerManager/PlayerData/DailyActivityData"
require "PlayerManager/PlayerData/TradeData"
require "PlayerManager/PlayerData/WarListInfoData"
require "PlayerManager/PlayerData/GuildData"
require "PlayerManager/PlayerData/InstanceData"
require "PlayerManager/PlayerData/BbsData"
require "PlayerManager/PlayerData/FunctionOpenData"
require "PlayerManager/PlayerData/ClientSceneData"
require "PlayerManager/PlayerData/PartnerData"
require "PlayerManager/PlayerData/WorldBossData"
require "PlayerManager/PlayerData/FashionData"
require "PlayerManager/PlayerData/TitleData"
require "PlayerManager/PlayerData/AchievementData"
require "PlayerManager/PlayerData/RuneData"
require "PlayerManager/PlayerData/VocationChangeData"
require "PlayerManager/PlayerData/CircleTaskData"
require "PlayerManager/PlayerData/WeekTaskData"
require "PlayerManager/PlayerData/BossHomeData"
require "PlayerManager/PlayerData/PersonalBossData"
require "PlayerManager/PlayerData/InstanceBalanceData"
require "PlayerManager/PlayerData/RankListData"
require "PlayerManager/PlayerData/EscortPeriData"
require "PlayerManager/PlayerData/PlayerSettingData"
require "PlayerManager/PlayerData/PrayData"
require "PlayerManager/PlayerData/AthleticsData"
require "PlayerManager/PlayerData/DailyRewardBackListData"
require "PlayerManager/PlayerData/GuildBanquetData"
require "PlayerManager/PlayerData/GuildConquestData"
require "PlayerManager/PlayerData/BackWoodsData"
require "PlayerManager/PlayerData/AnimalData"
require "PlayerManager/PlayerData/CardData"
require "PlayerManager/PlayerData/PreciousData"
require "PlayerManager/PlayerData/TowerInstanceData"
require "PlayerManager/PlayerData/TowerData"
require "PlayerManager/PlayerData/GodIslandData"
require "PlayerManager/PlayerData/FriendData"
require "PlayerManager/PlayerData/RemindData"
require "PlayerManager/PlayerData/CardLuckData"
require "PlayerManager/PlayerData/WelfareData"
require "PlayerManager/PlayerData/DragonSoulData"
require "PlayerManager/PlayerData/VipData"
require "PlayerManager/PlayerData/RouletteData"
require "PlayerManager/PlayerData/ServiceActivityData"
require "PlayerManager/PlayerData/LotteryEquipData"
require "PlayerManager/PlayerData/LotteryPeakData"
require "PlayerManager/PlayerData/BaseSystemSettingData"
require "PlayerManager/PlayerData/AutoGameSettingData"
require "PlayerManager/PlayerData/MarriageRaiseData"
require "PlayerManager/PlayerData/SweepData"
require "PlayerManager/PlayerData/FlowerData"
require "PlayerManager/PlayerData/InvestData"
require "PlayerManager/PlayerData/SaleData"
require "PlayerManager/PlayerData/BossOfferData"
require "PlayerManager/PlayerData/OpenActivityData"
require "PlayerManager/PlayerData/OpenHappyData"
require "PlayerManager/PlayerData/AncientBattleData"

local InvestData = ClassTypes.InvestData
local SaleData = ClassTypes.SaleData
local BossOfferData = ClassTypes.BossOfferData
local MarriageRaiseData = ClassTypes.MarriageRaiseData
local SweepData = ClassTypes.SweepData
local FlowerData = ClassTypes.FlowerData
local VipData = ClassTypes.VipData
local RouletteData = ClassTypes.RouletteData
local CardData = ClassTypes.CardData
local DragonSoulData = ClassTypes.DragonSoulData
local PreciousData = ClassTypes.PreciousData
local TowerData = ClassTypes.TowerData
local TowerInstanceData = ClassTypes.TowerInstanceData
local BagData = ClassTypes.BagData
local FriendData = ClassTypes.FriendData
local RemindData = ClassTypes.RemindData
local OpenActivityData = ClassTypes.OpenActivityData
local CardLuckData = ClassTypes.CardLuckData
local WelfareData = ClassTypes.WelfareData
local TaskData = ClassTypes.TaskData
local TeamData = ClassTypes.TeamData
local CombatData = ClassTypes.CombatData
local SkillData = ClassTypes.SkillData
local EventActivityData = ClassTypes.EventActivityData
local FortData = ClassTypes.FortData
local CompetitionData = ClassTypes.CompetitionData
local DailyActivityData = ClassTypes.DailyActivityData
local TradeData = ClassTypes.TradeData
local WarListInfoData = ClassTypes.WarListInfoData
local GuildData = ClassTypes.GuildData
local InstanceData = ClassTypes.InstanceData
local BbsData = ClassTypes.BbsData
local FunctionOpenData = ClassTypes.FunctionOpenData
local ClientSceneData = ClassTypes.ClientSceneData
local PartnerData = ClassTypes.PartnerData
local WorldBossData = ClassTypes.WorldBossData
local FashionData = ClassTypes.FashionData
local TitleData = ClassTypes.TitleData
local AchievementData = ClassTypes.AchievementData
local RuneData = ClassTypes.RuneData
local VocationChangeData = ClassTypes.VocationChangeData
local CircleTaskData = ClassTypes.CircleTaskData
local WeekTaskData = ClassTypes.WeekTaskData
local BossHomeData = ClassTypes.BossHomeData
local PersonalBossData = ClassTypes.PersonalBossData
local InstanceBalanceData = ClassTypes.InstanceBalanceData
local EscortPeriData = ClassTypes.EscortPeriData
local RankListData = ClassTypes.RankListData
local PlayerSettingData = ClassTypes.PlayerSettingData
local PrayData = ClassTypes.PrayData
local AthleticsData = ClassTypes.AthleticsData
local GuildBanquetData = ClassTypes.GuildBanquetData
local GuildConquestData = ClassTypes.GuildConquestData
local DailyRewardBackListData = ClassTypes.DailyRewardBackListData
local BackWoodsData = ClassTypes.BackWoodsData
local AnimalData = ClassTypes.AnimalData
local GodIslandData = ClassTypes.GodIslandData
local ServiceActivityData = ClassTypes.ServiceActivityData
local LotteryEquipData = ClassTypes.LotteryEquipData
local LotteryPeakData = ClassTypes.LotteryPeakData
local BaseSystemSettingData = ClassTypes.BaseSystemSettingData
local AutoGameSettingData = ClassTypes.AutoGameSettingData
local OpenHappyData = ClassTypes.OpenHappyData
local AncientBattleData = ClassTypes.AncientBattleData

local PlayerDataManager = {}

function PlayerDataManager:Init()--单例初始化方法
    self:Init1()
    self:Init2()
end
function PlayerDataManager:Init2()--单例初始化方法
    PlayerDataManager.saleData = SaleData()
    PlayerDataManager.bossOfferData = BossOfferData()
    PlayerDataManager.competitionData = CompetitionData()
    PlayerDataManager.dailyActivityData = DailyActivityData()
    PlayerDataManager.tradeData = TradeData()
    PlayerDataManager.warListInfoData = WarListInfoData()
    PlayerDataManager.guildData = GuildData()
    PlayerDataManager.instanceData = InstanceData()
    PlayerDataManager.bbsData = BbsData()
    PlayerDataManager.functionOpenData = FunctionOpenData()
    PlayerDataManager.clientSceneData = ClientSceneData()
    PlayerDataManager.partnerData = PartnerData()
    PlayerDataManager.worldBossData = WorldBossData()
    PlayerDataManager.fashionData = FashionData()
    PlayerDataManager.titleData = TitleData()
    PlayerDataManager.achievementData = AchievementData()
    PlayerDataManager.runeData = RuneData()
    PlayerDataManager.vocationChangeData = VocationChangeData()
    PlayerDataManager.circleTaskData = CircleTaskData()
    PlayerDataManager.weekTaskData = WeekTaskData()
    PlayerDataManager.bossHomeData = BossHomeData()
    PlayerDataManager.personalBossData = PersonalBossData()
    PlayerDataManager.instanceBalanceData = InstanceBalanceData()
    PlayerDataManager.escortPeriData = EscortPeriData()
    PlayerDataManager.rankListData = RankListData()
    PlayerDataManager.playerSettingData = PlayerSettingData()
    PlayerDataManager.playerSettingData:LoadData()
    PlayerDataManager.prayData = PrayData()
    PlayerDataManager.athleticsData = AthleticsData()
    PlayerDataManager.dailyRewardBackListData = DailyRewardBackListData()
    PlayerDataManager.guildBanquetData = GuildBanquetData()
    PlayerDataManager.guildConquestData = GuildConquestData()
    PlayerDataManager.backWoodsData = BackWoodsData()
    PlayerDataManager.animalData = AnimalData()
    PlayerDataManager.godIslandData = GodIslandData()
    PlayerDataManager.friendData = FriendData()
    PlayerDataManager.remindData = RemindData()
    PlayerDataManager.openActivityData = OpenActivityData()
    PlayerDataManager.cardLuckData = CardLuckData()
    PlayerDataManager.welfareData = WelfareData()
    PlayerDataManager.serviceActivityData = ServiceActivityData()
    PlayerDataManager.baseSystemSettingData = BaseSystemSettingData()
    PlayerDataManager.autoGameSettingData = AutoGameSettingData()
    PlayerDataManager.lotteryEquipData = LotteryEquipData()
    PlayerDataManager.lotteryPeakData = LotteryPeakData()
    PlayerDataManager.openHappyData = OpenHappyData()
    PlayerDataManager.ancientBattleData = AncientBattleData()
end
function PlayerDataManager:Init1()--单例初始化方法
    PlayerDataManager.investData = InvestData()
    PlayerDataManager.flowerData = FlowerData()
    PlayerDataManager.marriageRaiseData = MarriageRaiseData()
    PlayerDataManager.sweepData = SweepData()
    PlayerDataManager.rouletteData = RouletteData()
    PlayerDataManager.vipData = VipData()
    PlayerDataManager.dragonSoulData = DragonSoulData()
    PlayerDataManager.preciousData = PreciousData()
    PlayerDataManager.towerInstanceData = TowerInstanceData()
    PlayerDataManager.towerData = TowerData()
    PlayerDataManager.cardData = CardData()
    PlayerDataManager.bagData = BagData()
    PlayerDataManager.taskData = TaskData()
    PlayerDataManager.teamData = TeamData()
    PlayerDataManager.combatData = CombatData()
    PlayerDataManager.skillData = SkillData()
    PlayerDataManager.eventActivityData = EventActivityData()
    PlayerDataManager.fortData = FortData()

end
function PlayerDataManager:OnPlayerEnterWorld()
    PlayerManager.InvestManager:OnPlayerEnterWorld()
    PlayerManager.FunctionOpenManager:OnPlayerEnterWorld()
    PlayerManager.MainUIManager:OnPlayerEnterWorld()
    PlayerManager.SystemSettingManager:OnPlayerEnterWorld()
    PlayerManager.StrengthenManager:OnPlayerEnterWorld()
    PlayerManager.UpgradeManager:OnPlayerEnterWorld()

    GameManager.GuideManager.OnPlayerEnterWorld()
    PlayerManager.TaskManager:OnPlayerEnterWorld()
    PlayerManager.MapManager:OnPlayerEnterWorld()
    PlayerManager.InstanceManager:OnPlayerEnterWorld()
    PlayerManager.EventActivityManager:OnPlayerEnterWorld()
    PlayerManager.OpenActivityManager:OnPlayerEnterWorld()
    PlayerManager.FriendManager:OnPlayerEnterWorld()
    PlayerManager.CardLuckManager:OnPlayerEnterWorld()
    PlayerManager.HappyWishManager:OnPlayerEnterWorld()
    PlayerManager.IdentifyManager:OnPlayerEnterWorld()
    PlayerManager.SpecialDialogManager:OnPlayerEnterWorld()
    PlayerManager.BubbleDialogManager:OnPlayerEnterWorld()
    PlayerManager.DailyActivityManager:OnPlayerEnterWorld()
    PlayerManager.AionTowerManager:OnPlayerEnterWorld()
    PlayerManager.WorldBossManager:OnPlayerEnterWorld()
    PlayerManager.ActivityInfoManager:OnPlayerEnterWorld()
    PlayerManager.RoleManager:OnPlayerEnterWorld()
    PlayerManager.AchievementManager:OnPlayerEnterWorld()
    PlayerManager.InstanceCommonManager:OnPlayerEnterWorld()    
    PlayerManager.RuneManager:OnPlayerEnterWorld()
    PlayerManager.PetInstanceManager:OnPlayerEnterWorld()
    PlayerManager.VocationChangeManager:OnPlayerEnterWorld()
    PlayerManager.CircleTaskManager:OnPlayerEnterWorld()
    PlayerManager.BossHomeManager:OnPlayerEnterWorld()
    PlayerManager.MoneyInstanceManager:OnPlayerEnterWorld()
    PlayerManager.PersonalBossManager:OnPlayerEnterWorld()
    PlayerManager.InstanceBalanceManager:OnPlayerEnterWorld()
    PlayerManager.VIPManager:OnPlayerEnterWorld()
    PlayerManager.PrayManager:OnPlayerEnterWorld()     
    PlayerManager.AthleticsManager:OnPlayerEnterWorld()
    PlayerManager.EquipmentInstanceManager:OnPlayerEnterWorld()
    PlayerManager.BackWoodsManager:OnPlayerEnterWorld()
    PlayerManager.RankExManager:OnPlayerEnterWorld()
    PlayerManager.CardManager:OnPlayerEnterWorld()
    PlayerManager.WeekTaskManager:OnPlayerEnterWorld()
    PlayerManager.CloudPeakInstanceManager:OnPlayerEnterWorld()
    PlayerManager.OnlinePvpManager:OnPlayerEnterWorld()
    PlayerManager.GodIslandManager:OnPlayerEnterWorld()
    PlayerManager.TowerInstanceManager:OnPlayerEnterWorld()
    PlayerManager.AnswerManager:OnPlayerEnterWorld()
    PlayerManager.AnimalManager:OnPlayerEnterWorld()
    PlayerManager.PreciousManager:OnPlayerEnterWorld()
    PlayerManager.MarriageManager:OnPlayerEnterWorld()
    PlayerManager.DragonSoulManager:OnPlayerEnterWorld()
    PlayerManager.TitleManager:OnPlayerEnterWorld()
    PlayerManager.SkillManager:OnPlayerEnterWorld()
    PlayerManager.ShopManager:OnPlayerEnterWorld()
    PlayerManager.PKModeManager:OnPlayerEnterWorld()
    PlayerManager.PartnerManager:OnPlayerEnterWorld()
    PlayerManager.TreasureManager:OnPlayerEnterWorld()
    PlayerManager.OffLineManager:OnPlayerEnterWorld()
    PlayerManager.GuildManager:OnPlayerEnterWorld()
    PlayerManager.GuildConquestManager:OnPlayerEnterWorld()
    PlayerManager.GuildBanquetManager:OnPlayerEnterWorld()
    PlayerManager.FloatTextManager:OnPlayerEnterWorld()
    PlayerManager.FashionManager:OnPlayerEnterWorld()
    PlayerManager.SevenDayLoginManager:OnPlayerEnterWorld()
    PlayerManager.DailyChargeManager:OnPlayerEnterWorld()
    PlayerManager.ServiceActivityManager:OnPlayerEnterWorld()
    PlayerManager.LotteryEquipManager:OnPlayerEnterWorld()
    PlayerManager.LotteryPeakManager:OnPlayerEnterWorld()
    PlayerManager.EscortPeriManager:OnPlayerEnterWorld()
    PlayerManager.MarriageRaiseManager:OnPlayerEnterWorld()
    PlayerManager.RouletteManager:OnPlayerEnterWorld()
    PlayerManager.FlowerManager:OnPlayerEnterWorld()
    PlayerManager.WelfareManager:OnPlayerEnterWorld()
    PlayerManager.ChatManager:OnPlayerEnterWorld()
    PlayerManager.MailManager:OnPlayerEnterWorld()
    GameManager.EquipManager:OnPlayerEnterWorld()
    PlayerManager.ComposeManager:OnPlayerEnterWorld()
    PlayerManager.ExpInstanceManager:OnPlayerEnterWorld()
    PlayerManager.AutoFightManager:OnPlayerEnterWorld()
    PlayerManager.LogsManager:OnPlayerEnterWorld()
    PlayerManager.TransformManager:OnPlayerEnterWorld()
    PlayerManager.FlashSaleManager:OnPlayerEnterWorld()
    PlayerManager.DailyConsumeManager:OnPlayerEnterWorld()
    PlayerManager.ClearDataManager:OnPlayerEnterWorld()
    PlayerManager.TaskCommonManager:OnPlayerEnterWorld()
    PlayerManager.OperatingActivityManager:OnPlayerEnterWorld()
    PlayerManager.TeamManager:OnPlayerEnterWorld()
    PlayerManager.WaitTimeManager:OnPlayerEnterWorld()
    GameManager.DamageManager:OnPlayerEnterWorld()
    PlayerManager.ZeroDollarBuyManager:OnPlayerEnterWorld()
    PlayerManager.SaleManager:OnPlayerEnterWorld()
    PlayerManager.BossOfferManager:OnPlayerEnterWorld()
    PlayerManager.SkyDropManager:OnPlayerEnterWorld()
    PlayerManager.AncientBattleManager:OnPlayerEnterWorld()
    PlayerManager.LimitedShoppingManager:OnPlayerEnterWorld()
    PlayerManager.VIPLevelGiftManager:OnPlayerEnterWorld()
    PlayerManager.PowerSaveManager:OnPlayerEnterWorld()
    PlayerManager.StarManager:OnPlayerEnterWorld()
    --我需要是最后的，不要移走我
    PlayerManager.DailyActivityExManager:OnPlayerEnterWorld()
end

function PlayerDataManager:OnPlayerLeaveWorld()
    PlayerManager.SystemSettingManager:OnPlayerLeaveWorld()
    PlayerManager.StrengthenManager:OnPlayerLeaveWorld()
    PlayerManager.UpgradeManager:OnPlayerLeaveWorld()
    GameManager.GuideManager.OnPlayerLeaveWorld()
    PlayerManager.TaskManager:OnPlayerLeaveWorld()
    PlayerManager.MapManager:OnPlayerLeaveWorld()
    PlayerManager.InstanceManager:OnPlayerLeaveWorld()
    PlayerManager.EventActivityManager:OnPlayerLeaveWorld()
    PlayerManager.FriendManager:OnPlayerLeaveWorld()
    PlayerManager.CardLuckManager:OnPlayerLeaveWorld()
    PlayerManager.HappyWishManager:OnPlayerLeaveWorld()
    PlayerManager.IdentifyManager:OnPlayerLeaveWorld()
    PlayerManager.SpecialDialogManager:OnPlayerLeaveWorld()
    PlayerManager.BubbleDialogManager:OnPlayerLeaveWorld()
    PlayerManager.DailyActivityManager:OnPlayerLeaveWorld()
    PlayerManager.AionTowerManager:OnPlayerLeaveWorld()
    PlayerManager.WorldBossManager:OnPlayerLeaveWorld()
    PlayerManager.ActivityInfoManager:OnPlayerLeaveWorld()
    PlayerManager.FunctionOpenManager:OnPlayerLeaveWorld()
    PlayerManager.RoleManager:OnPlayerLeaveWorld()
    PlayerManager.AchievementManager:OnPlayerLeaveWorld()
    PlayerManager.InstanceCommonManager:OnPlayerLeaveWorld()
    PlayerManager.RuneManager:OnPlayerLeaveWorld()
    PlayerManager.PetInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.VocationChangeManager:OnPlayerLeaveWorld()
    PlayerManager.CircleTaskManager:OnPlayerLeaveWorld()
    PlayerManager.BossHomeManager:OnPlayerLeaveWorld()
    PlayerManager.MoneyInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.PersonalBossManager:OnPlayerLeaveWorld()
    PlayerManager.InstanceBalanceManager:OnPlayerLeaveWorld()
    PlayerManager.VIPManager:OnPlayerLeaveWorld()
    PlayerManager.DailyActivityExManager:OnPlayerLeaveWorld()
    PlayerManager.PrayManager:OnPlayerLeaveWorld() 
    PlayerManager.AthleticsManager:OnPlayerLeaveWorld()
    PlayerManager.EquipmentInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.BackWoodsManager:OnPlayerLeaveWorld()
    PlayerManager.RankExManager:OnPlayerLeaveWorld()
    PlayerManager.CardManager:OnPlayerLeaveWorld()
    PlayerManager.WeekTaskManager:OnPlayerLeaveWorld()
    PlayerManager.CloudPeakInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.OnlinePvpManager:OnPlayerLeaveWorld()
    PlayerManager.GodIslandManager:OnPlayerLeaveWorld()
    PlayerManager.TowerInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.AnswerManager:OnPlayerLeaveWorld()
    PlayerManager.AnimalManager:OnPlayerLeaveWorld()
    PlayerManager.PreciousManager:OnPlayerLeaveWorld()
    PlayerManager.MarriageManager:OnPlayerLeaveWorld()
    PlayerManager.DragonSoulManager:OnPlayerLeaveWorld()
    PlayerManager.TitleManager:OnPlayerLeaveWorld()
    PlayerManager.SkillManager:OnPlayerLeaveWorld()
    PlayerManager.ShopManager:OnPlayerLeaveWorld()
    PlayerManager.PKModeManager:OnPlayerLeaveWorld()
    PlayerManager.PartnerManager:OnPlayerLeaveWorld()
    PlayerManager.TreasureManager:OnPlayerLeaveWorld()
    PlayerManager.OffLineManager:OnPlayerLeaveWorld()
    PlayerManager.GuildManager:OnPlayerLeaveWorld()
    PlayerManager.GuildConquestManager:OnPlayerLeaveWorld()
    PlayerManager.GuildBanquetManager:OnPlayerLeaveWorld()
    PlayerManager.FloatTextManager:OnPlayerLeaveWorld()
    PlayerManager.FashionManager:OnPlayerLeaveWorld()
    PlayerManager.SevenDayLoginManager:OnPlayerLeaveWorld()
    PlayerManager.EscortPeriManager:OnPlayerLeaveWorld()
    PlayerManager.DailyChargeManager:OnPlayerLeaveWorld()
    PlayerManager.ServiceActivityManager:OnPlayerLeaveWorld()
    PlayerManager.LotteryEquipManager:OnPlayerLeaveWorld()
    PlayerManager.LotteryPeakManager:OnPlayerLeaveWorld()
    PlayerManager.MainUIManager:OnPlayerLeaveWorld()
    PlayerManager.MarriageRaiseManager:OnPlayerLeaveWorld()
    PlayerManager.RouletteManager:OnPlayerLeaveWorld()
    PlayerManager.FlowerManager:OnPlayerLeaveWorld()
    PlayerManager.WelfareManager:OnPlayerLeaveWorld()
    PlayerManager.ChatManager:OnPlayerLeaveWorld()
    PlayerManager.MailManager:OnPlayerLeaveWorld()
    GameManager.EquipManager:OnPlayerLeaveWorld()
    PlayerManager.ComposeManager:OnPlayerLeaveWorld()
    PlayerManager.ExpInstanceManager:OnPlayerLeaveWorld()
    PlayerManager.AutoFightManager:OnPlayerLeaveWorld()
    PlayerManager.LogsManager:OnPlayerLeaveWorld()
    PlayerManager.TransformManager:OnPlayerLeaveWorld()
    PlayerManager.FlashSaleManager:OnPlayerLeaveWorld()
    PlayerManager.DailyConsumeManager:OnPlayerLeaveWorld()
    PlayerManager.ClearDataManager:OnPlayerLeaveWorld()
    PlayerManager.TaskCommonManager:OnPlayerLeaveWorld()
    PlayerManager.OperatingActivityManager:OnPlayerLeaveWorld()
    PlayerManager.InvestManager:OnPlayerLeaveWorld()
    PlayerManager.TeamManager:OnPlayerLeaveWorld()
    PlayerManager.WaitTimeManager:OnPlayerLeaveWorld()
    GameManager.DamageManager:OnPlayerLeaveWorld()
    PlayerManager.ZeroDollarBuyManager:OnPlayerLeaveWorld()
    PlayerManager.SaleManager:OnPlayerLeaveWorld()
    PlayerManager.BossOfferManager:OnPlayerLeaveWorld()
    PlayerManager.BagManager:OnPlayerLeaveWorld()
    PlayerManager.OpenActivityManager:OnPlayerLeaveWorld()
    PlayerManager.SkyDropManager:OnPlayerLeaveWorld()
    PlayerManager.AncientBattleManager:OnPlayerLeaveWorld()
    PlayerManager.LimitedShoppingManager:OnPlayerLeaveWorld()
    PlayerManager.VIPLevelGiftManager:OnPlayerLeaveWorld()
    PlayerManager.PowerSaveManager:OnPlayerLeaveWorld()
    PlayerManager.StarManager:OnPlayerLeaveWorld()
end

PlayerDataManager:Init()
return PlayerDataManager
