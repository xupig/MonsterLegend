local BBSManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local bbsData = PlayerManager.PlayerDataManager.bbsData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function BBSManager:Init()
end

function BBSManager:HandleData(action_id,error_id,args)
    -- LoggerHelper.Log("BBSManager "..action_id)
    -- LoggerHelper.Log(args)
    if action_id == action_config.DISCUSSION_GET_INFO then
        self:RefreshDiscussionInfo(args) 
    end
end

function BBSManager:RefreshDiscussionInfo(data)
    bbsData:RefreshDiscussionInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Discussion_Info)
end


-------------------------------------客户端请求-----------------------------------
--获取攻略信息
function BBSManager:DiscussionGetInfoReq(boss_id, page)
    local params = string.format("%d,%d", boss_id, page)
    GameWorld.Player().server.discussion_action_req(action_config.DISCUSSION_GET_INFO, params)
end

--发表攻略信息
function BBSManager:DiscussionAddInfoReq(boss_id, text)
    local params = string.format("%d,%s", boss_id, text)
    GameWorld.Player().server.discussion_action_req(action_config.DISCUSSION_ADD_INFO, params)
end

--点赞与取消操作 state:0-取消点赞，1-点赞
function BBSManager:DiscussionLikeOrNotReq(boss_id, state)
    local params = string.format("%d,%d", boss_id, state)
    GameWorld.Player().server.discussion_action_req(action_config.DISCUSSION_GIVE_SUPPORT, params)
end

BBSManager:Init() 
return BBSManager