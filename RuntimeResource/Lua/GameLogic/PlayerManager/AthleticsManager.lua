local AthleticsManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config

local OfflinePvpRobotDataHelper = GameDataHelper.OfflinePvpRobotDataHelper
local OfflinePvpReward1DataHelper = GameDataHelper.OfflinePvpReward1DataHelper
local OfflinePvpReward2DataHelper = GameDataHelper.OfflinePvpReward2DataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager

local AthleticsData = PlayerManager.PlayerDataManager.athleticsData
local PlayerDataManager = PlayerManager.PlayerDataManager

local PvpTotalDataHelper = GameDataHelper.PvpTotalDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local EventDispatcher = EventDispatcher
local AthleticsConfig = GameConfig.AthleticsConfig
local RenderSettings = UnityEngine.RenderSettings
local error_code = GameWorld.error_code

function AthleticsManager:Init()
    self._initdata = function() self:InitData() end
    self._updateRedEvent = function() self:RefreshRedEvent() end
    self._onLevelChangeCB = function()self:OnLevelChange() end
end

-- Req
--刷新 --打开界面访问一次
function AthleticsManager.PVPRefresh()
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_REFRESH,',')    
end
--挑战。如果成功，修改get_info中的rank_index。如果要非常准确，别人挑战了，那么客户端只能打开界面后轮询了。
function AthleticsManager:PVPChallenge(rank,fight)
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_CHALLENGE,rank..','..fight)    
end
--鼓舞。如果成功，修改get_info中的inspire_count
function AthleticsManager.PVPInspire()
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_INSPIRE,',')    
end
--购买挑战次数
function AthleticsManager.PVPBuy(count)
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_BUY,count)    
end
--领取结算奖励。如果成功，修改get_info中的settle_flag = 0
function AthleticsManager.PVPSettle()
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_SETTLE,',')    
end
--获取相关信息。--鼓舞次数，排名，结算标记。打开界面访问一次。还有22点后结算，打开界面后客户端定那个时刻，然后主动申请一下。
function AthleticsManager.PVPGetInfo()
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_GET_INFO,',')    
end

function AthleticsManager.PVPGetReward()
	GameWorld.Player().server.offline_pvp_action_req(action_config.OFFLINE_PVP_GET_REWARD,',')    
end


-- Resp

function AthleticsManager:HandleData(action_id, error_id, lua_table)
    if action_id == action_config.OFFLINE_PVP_REFRESH then
		self:PVPRefreshResp(lua_table)
	elseif action_id == action_config.OFFLINE_PVP_CHALLENGE then
		self:PVPChallengeResp(error_id,lua_table)
	elseif action_id == action_config.OFFLINE_PVP_INSPIRE then
		self:PVPInspireResp(error_id,lua_table)
	elseif action_id == action_config.OFFLINE_PVP_BUY then
		self:PVPBuyResp(error_id,lua_table)
	elseif action_id == action_config.OFFLINE_PVP_SETTLE then
        self:PVPSettleResp(error_id,lua_table)
        self.PVPGetInfo()
    elseif action_id == action_config.OFFLINE_PVP_GET_INFO then
        self:PVPGetInfoResp(error_id,lua_table)
    elseif action_id == action_config.OFFLINE_PVP_GET_REWARD then
        -- LoggerHelper.Log("奖励拿到")
    else

	end
end

function AthleticsManager:PVPRefreshResp(lua_table)
    AthleticsData:UpdateOffRoles(lua_table)
    EventDispatcher:TriggerEvent(GameEvents.PVPOffUpdateRoles)
end

function AthleticsManager:PVPChallengeResp(error_id,lua_table)
    local changeRankIndex=0
    if error_id == 0 then    
        local win = lua_table[1] -- 赢：1
        changeRankIndex = lua_table[2]
        local reward = lua_table[3]
        local serveData = {}
        serveData[public_config.INSTANCE_SETTLE_KEY_MAP_ID] = nil        
        GUIManager.ClosePanel(PanelsConfig.Athletics)
        if win==1 then
            GameWorld.SetAvatarData(self._cgdata1)
            serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] = 1
            local cg = self:SeleCG(1,self._cgdata1.vocation)
            self._fog = RenderSettings.fog
            GameManager.DramaManager:Trigger("PLAY_CG",cg[2]) --胜利
            RenderSettings.fog=false
            self._headdata.cgf='__begin1'
            self._headdata.cgname=cg[1]
        else
            GameWorld.SetAvatarData(self._cgdata2)
            serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] = 0  
            local cg = self:SeleCG(0,self._cgdata2.vocation)
            self._fog = RenderSettings.fog
            GameManager.DramaManager:Trigger("PLAY_CG",cg[2]) 
            RenderSettings.fog=false
            self._headdata.cgf='__begin1'
            self._headdata.cgname=cg[1]
        end
        GUIManager.SetCameraCulling(PanelsConfig.OffPVPHead, false)
        serveData[public_config.INSTANCE_SETTLE_KEY_ITEMS] = reward               
        serveData[public_config.INSTANCE_SETTLE_KEY_STAR]=3
        GUIManager.ShowPanel(PanelsConfig.OffPVPHead, self._headdata)
        AthleticsData:UpdateCgData(serveData)
    else
        EventDispatcher:TriggerEvent(GameEvents.FightFail)
    end   
    if changeRankIndex>0 then
        self.PVPGetInfo()
    end
end

function AthleticsManager:RestoreFog()
    RenderSettings.fog=self._fog
end

function AthleticsManager:InitCGcfg()
    if self._CGcfg==nil then
        self._CGcfg={}
    end
    self._CGcfg[2]={}
    self._CGcfg[2][2]={}
    self._CGcfg[2][1]={}
    self._CGcfg[2][2][1]={"CG200041","5000010"}
    self._CGcfg[2][2][0]={"CG200042","5000011"}
    self._CGcfg[2][1][1]={"CG200043","5000012"}
    self._CGcfg[2][1][0]={"CG200044","5000013"}

    self._CGcfg[1]={}
    self._CGcfg[1][2]={}
    self._CGcfg[1][1]={}
    self._CGcfg[1][2][1]={"CG200045","5000014"}
    self._CGcfg[1][2][0]={"CG200046","5000015"}
    self._CGcfg[1][1][1]={"CG200047","5000016"}
    self._CGcfg[1][1][0]={"CG200048","5000017"}
end
function AthleticsManager:SeleCG(win,opponentvocation)
    local v = RoleDataHelper.GetVoacationById(opponentvocation)
    local selfVocation = RoleDataHelper.GetVoacationById(GameWorld.Player().vocation)
    return self._CGcfg[selfVocation][v][win]
end

function AthleticsManager:SeleChallRole(item)
    self._cgdata1={name=item:GetName(),vocation=item:GetVacation(),facade=item:GetFacade(),fight_force=item:GetFight()}
    self._cgdata2={name=item:GetName(),vocation=item:GetVacation(),facade=item:GetFacade()}
    self._headdata={name=item:GetName(),vocation=item:GetVacation(),fight_force=item:GetFight()}
end

function AthleticsManager:PVPInspireResp(error_id,lua_table)
    if error_id==0 then
        local showText = LanguageDataHelper.GetContent(51222)
        GUIManager.ShowText(2, showText)
        self.PVPGetInfo()
        EventDispatcher:TriggerEvent(GameEvents.InspireSuccess)
    end
end

function AthleticsManager:PVPBuyResp(error_id,lua_table)
	
end

function AthleticsManager:PVPSettleResp(error_id,lua_table)
	
end

function AthleticsManager:GetRobotVocation(modelid)
    
end

-- 个人信息
function AthleticsManager:PVPGetInfoResp(error_id,lua_table)
     if error_id == 0 then
        AthleticsData:UpdateSeverRoleInfo(lua_table)
        self:RefreshRedEvent()
        EventDispatcher:TriggerEvent(GameEvents.PVPOffUpdateInfo)
    end
end

function AthleticsManager:OnPlayerEnterWorld()	
    local f = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OFFLINE_PVP)
    if f then
        self:InitData()
        self:GetRewardTimer()
        self:LoadCG1v1()
    end
    self:RefreshRedEvent()
    self:InitCGcfg()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_BUY_COUNT, self._updateRedEvent)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_CHALLENGE_COUNT, self._updateRedEvent)    
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function AthleticsManager:OnPlayerLeaveWorld()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_BUY_COUNT, self._updateRedEvent)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_OFFLINE_PVP_CHALLENGE_COUNT, self._updateRedEvent)  
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
end

function AthleticsManager:OnLevelChange()
    local f = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OFFLINE_PVP)
    if f then
        self:LoadCG1v1()
    end
end

function AthleticsManager:LoadCG1v1()
    if self._hasLoaded then
        return
    end

    GameWorld.LoadCG1v1()
    self._hasLoaded = true
end

function AthleticsManager:GetRewardTimer()
    
end


-- 事件
function AthleticsManager:RefreshRedEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OFFLINE_PVP, self:RefreshRedPointState())        
end

-- 红点
function AthleticsManager:RefreshRedPointState()
   return self:CanGetReward() or self:CanFight()
end
-- 奖励红点
function AthleticsManager:CanGetReward()
    local ids = OfflinePvpReward1DataHelper:GetAllId()
    for i,v in ipairs(ids) do
        local rank= OfflinePvpReward1DataHelper:GetRank(v)
        if AthleticsData:IsCanGetRankReward(rank) then
            return true
        end
    end
    return false
end

-- 挑战次数
function AthleticsManager:CanFight()
    local entity = GameWorld.Player()
    local buycount = entity.offline_pvp_buy_count
    local chacount = entity.offline_pvp_challenge_count
    local rawchacount = AthleticsData:GetRawChaCount()
    local cnum = rawchacount+buycount-chacount
    if cnum>0 then
        return true
    end
    return false
end
function AthleticsManager:InitData()
    self.PVPRefresh()
    self.PVPGetInfo()
end
AthleticsManager:Init()
return AthleticsManager
