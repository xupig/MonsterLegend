local DailyActivityManager = {}
local action_config = require("ServerConfig/action_config")
require "PlayerManager/PlayerData/EventData"
local TaskRewardData = ClassTypes.TaskRewardData
local GameSceneManager = GameManager.GameSceneManager
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DailyActivityConfig = GameConfig.DailyActivityConfig
local PointRewardDataHelper = GameDataHelper.PointRewardDataHelper
local PlayerDataManager = PlayerManager.PlayerDataManager
local DailyActivityData = PlayerDataManager.dailyActivityData

function DailyActivityManager:Init()
    self:InitCallbackFunc()
end

function DailyActivityManager:InitCallbackFunc()
end

function DailyActivityManager:OnPlayerEnterWorld()
end

function DailyActivityManager:OnPlayerLeaveWorld()
end

function DailyActivityManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("DailyActivity Error:" .. error_id)
        return
    elseif event_id == action_config.DAILY_TASK_LIST_REQ then
        self:RefreshProcessing(args)
    elseif event_id == action_config.DAILY_TASK_REFRESH then
        self:RefreshProcessing(args)
    elseif event_id == action_config.DAILY_TASK_GROUP_REQ then
        self:ProcessingList(args)
    else
        LoggerHelper.Error("DailyActivity event id not handle:" .. event_id)
    end
end

function DailyActivityManager:HandleResp(act_id, code, args)
    if code ~= 0 then
        return
    elseif act_id == action_config.DAILY_TASK_POINT_REWARD_REQ then
        EventDispatcher:TriggerEvent(GameEvents.DailyTaskPointRewardResp)
    elseif act_id == action_config.DAILY_TASK_WEEK_POINT_REWARD_REQ then
        EventDispatcher:TriggerEvent(GameEvents.WeeklyTaskPointRewardResp)
    end
    LoggerHelper.Log("Ash: DailyActivityManager HandleResp: " .. act_id .. " code: "
        .. code .. " args: " .. PrintTable:TableToStr(args))
end

function DailyActivityManager:ProcessingList(args)
    LoggerHelper.Log("Ash: DailyActivity ProcessingList:" .. PrintTable:TableToStr(args))
    DailyActivityData:ResetDailyActivityData(args)
-- self:TriggerRefreshTasks()
end

function DailyActivityManager:RefreshProcessing(args)
    LoggerHelper.Log("Ash: DailyActivity RefreshProcessing:" .. PrintTable:TableToStr(args))
    DailyActivityData:UpdateDailyActivityData(args)
-- self:TriggerRefreshTasks()
end

function DailyActivityManager:GetDailyActivities()
    return DailyActivityData:GetDailyActivities()
end

function DailyActivityManager:GetDailyActivitiesList()
    return DailyActivityData:GetDailyActivitiesList()
end

function DailyActivityManager:GetSortedDailyActivitiesList()
    local orgList = DailyActivityData:GetDailyActivitiesList()
    local sortedList = {}
    for i, v in ipairs(orgList) do
        if DailyActivityData:GetDailyActivityById(v):GetState() == public_config.DAILY_TASK_STATE_COMPLETED then
            table.insert(sortedList, v)
        end
    end
    for i, v in ipairs(orgList) do
        if DailyActivityData:GetDailyActivityById(v):GetState() == public_config.DAILY_TASK_STATE_ACCEPTED then
            table.insert(sortedList, v)
        end
    end
    for i, v in ipairs(orgList) do
        if DailyActivityData:GetDailyActivityById(v):GetState() == public_config.DAILY_TASK_STATE_COMMITED then
            table.insert(sortedList, v)
        end
    end
    return sortedList
end
function DailyActivityManager:GetDailyActivityById(id)
    return DailyActivityData:GetDailyActivityById(id)
end

function DailyActivityManager:GetDailyTaskPoint()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.daily_task_point or 0
end

function DailyActivityManager:GetDailyTaskPointReward()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.daily_task_point_reward or nil
end

function DailyActivityManager:GetDailyTaskWeekPoint()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.daily_task_week_point or 0
end

function DailyActivityManager:GetDailyTaskWeekPointReward()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.daily_task_week_point_reward or nil
end

function DailyActivityManager:GetAllAndMaxPointReward()
    local prds = PointRewardDataHelper:GetAllId()
    local allDailyPointRewards = {}
    local maxDailyPointRewards = 0
    local allWeeklyPointRewards = {}
    for i, v in ipairs(prds) do
        if PointRewardDataHelper:GetType(v) == DailyActivityConfig.DAILY_VITALITY then
            table.insert(allDailyPointRewards, v)
            if PointRewardDataHelper:GetPoint(v) > maxDailyPointRewards then
                maxDailyPointRewards = PointRewardDataHelper:GetPoint(v)
            end
        elseif PointRewardDataHelper:GetType(v) == DailyActivityConfig.WEEKLY_VITALITY then
            table.insert(allWeeklyPointRewards, v)
        end
    end
    -- LoggerHelper.Log("Ash: DailyActivity allWeeklyPointRewards:" .. PrintTable:TableToStr(allWeeklyPointRewards))
    return allDailyPointRewards, maxDailyPointRewards, allWeeklyPointRewards
end

function DailyActivityManager:GetPointRewardsList(pointRewardsId)
    local rewards = PointRewardDataHelper:GetReward(pointRewardsId)
    local result = {}
    local activitys = PointRewardDataHelper:GetActivitys(pointRewardsId)
    -- LoggerHelper.Log("Ash: GetPointRewardsList pointRewardsId:" .. pointRewardsId)
    -- LoggerHelper.Log("Ash: GetPointRewardsList activitys:" .. PrintTable:TableToStr(activitys))
    local level = GameWorld.Player().level
    if activitys ~= nil and activitys[level] ~= nil then
        table.insert(result, TaskRewardData(DailyActivityConfig.VITALITY_ITEM_ID, activitys[level]))
    end
    for i, v in ipairs(rewards) do
        table.insert(result, TaskRewardData(v[1], v[2]))
    end
    return result
end

function DailyActivityManager:CommitTask(taskId)
    GameWorld.Player().server.daily_task_action_req(action_config.DAILY_TASK_COMMIT, tostring(taskId))
end

function DailyActivityManager:DailyTaskPointRewardReq(rewardId)
    GameWorld.Player().server.daily_task_action_req(action_config.DAILY_TASK_POINT_REWARD_REQ, tostring(rewardId))
end

function DailyActivityManager:WeeklyTaskPointRewardReq(rewardId)
    GameWorld.Player().server.daily_task_action_req(action_config.DAILY_TASK_WEEK_POINT_REWARD_REQ, tostring(rewardId))
end

DailyActivityManager:Init()
return DailyActivityManager
