local TradeManager = {}

local action_config = GameWorld.action_config
local tradeData = PlayerManager.PlayerDataManager.tradeData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager

function TradeManager:Init()
end

function TradeManager:HandleData(action_id, err_code, args)
	-- LoggerHelper.Error("TradeManager:HandleData"..action_id)
	-- LoggerHelper.Log(args)
	--报错了
	if err_code ~= 0 then
		LoggerHelper.Error("Trade Error:" ..err_code)
		return
	--交易行卖
	elseif action_id == action_config.AUCTION_SELL_REQ  then
		self:SellComplete()
	--交易行买
	elseif action_id == action_config.AUCTION_BUY_REQ   then
		self:BuyComplete()
	--交易下架
	elseif action_id == action_config.AUCTION_OFF_REQ   then
		self:UpdateTradeOff(args)
	--摆摊列表
	elseif action_id == action_config.AUCTION_ITEM_LIST   then
		self:UpdateAuctionItemList(args)
	--自己的摆摊列表
	elseif action_id == action_config.AUCTION_ITEM_MY_LIST   then
		self:UpdateMySell(args)
	elseif action_id == action_config.AUCTION_SEARCH then
		self:UpdateSearchByName(args)

	--求购部分
	elseif action_id == action_config.BEG_PUBLISH_REQ then
		self:BegComlete()
	elseif action_id == action_config.BEG_ITEM_MY_LIST then
		self:UpdateMyBeg(args)
	elseif action_id == action_config.BEG_ITEM_LIST  then
		self:UpdateBegItemList(args)
	elseif action_id == action_config.BEG_ITEM_LIST_ALL then
		self:UpdateAllBegItemList(args)
	elseif action_id == action_config.BEG_CANCEL_REQ then
		self:UpdateBegCancel()
	elseif action_id == action_config.BEG_SELL_REQ then
		self:SellBegComplete()
	elseif action_id == action_config.BEG_SEARCH then
		self:UpdateBegSearchByName(args)
	end
end

-- function TradeManager:SetSearchByCondition(args)
-- 	tradeData:SetSearchByCondition(args)
-- 	EventDispatcher:TriggerEvent(GameEvents.UPDATE_TRADE_SEARCH_BY_CONDITION)
-- end
------------------------------**摆摊交易部分**-----------------------------------

function TradeManager:UpdateMySell(args)
	tradeData:UpdateMySellItemInfo(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_MY_SELL_DATA)
end

-- function TradeManager:UpdateBlackMarketData(args)
-- 	tradeData:UpdateBlackMarketData(args)
-- 	EventDispatcher:TriggerEvent(GameEvents.UPDATE_BLACKMARKET_DATA)
-- end
function TradeManager:BuyComplete()
	local showText = LanguageDataHelper.GetContent(73524)
    GUIManager.ShowText(2, showText)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_TRADE_BUY_COMPLETE)
end

function TradeManager:SellComplete()
	--关闭弹出窗口
	self:RequestAuctionItemMyList()
end

function TradeManager:UpdateAuctionItemList(args)
	local auctionType = tradeData:UpdateAllTradeItemList(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_ALL_SELL_DATA,auctionType)
end

function TradeManager:UpdateSearchByName(args)
	tradeData:UpdateSearchByName(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_TRADE_SEARCH_BY_NAME)
end

function TradeManager:UpdateTradeOff()
	self:RequestAuctionItemMyList()
end

------------------------------交易所请求--------------------------

--交易行卖
function TradeManager:RequestAuctionSell(bagType,bagPos,num,price,passwd)
	local str = string.format("%d,%d,%d,%d,%d",bagType,bagPos,num,price,passwd)
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_SELL_REQ,str)
end

--交易行买 
function TradeManager:RequestAuctionBuy(auction_item_dbid,passwd)
	local str = auction_item_dbid..","..passwd
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_BUY_REQ,str)
end

--交易下架
function TradeManager:RequestAuctionOff(auction_item_dbid)
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_OFF_REQ ,tostring(auction_item_dbid))
end

--摆摊列表
function TradeManager:RequestAuctionItemList(auctionType)
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_ITEM_LIST,tostring(auctionType))
end

--自己的摆摊列表
function TradeManager:RequestAuctionItemMyList()
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_ITEM_MY_LIST,"")
end

--字符串搜索道具
function TradeManager:RequestAuctionSearch(str)
	GameWorld.Player().server.auction_action_req(action_config.AUCTION_SEARCH,str)
end

------------------------------**求购部分**-----------------------------------

function TradeManager:UpdateMyBeg(args)
	tradeData:UpdateMyBeg(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_MY_BEG_DATA)
end

function TradeManager:UpdateAllBegItemList(args)
	local auctionType = tradeData:UpdateAllBegItemList(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_ALL_BEG_DATA,auctionType)
end

function TradeManager:UpdateBegItemList(args)
	local auctionType = tradeData:UpdateBegItemListByType(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_BEG_TYPE_DATA,auctionType)
end

function TradeManager:UpdateBegSearchByName(args)
	tradeData:UpdateBegSearchByName(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_BEG_SEARCH_BY_NAME)
end

function TradeManager:BegComlete()
	GUIManager.ShowText(2, LanguageDataHelper.CreateContent(73522))
end

function TradeManager:SellBegComplete()
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_BEG_SELL_COMPLETE)
end

function TradeManager:UpdateBegCancel()
	self:RequestBegMyList()
end

--发布求购
function TradeManager:RequestBegPublish(item_id,count,price)
	local str = string.format("%d,%d,%d",item_id,count,price)
	GameWorld.Player().server.beg_action_req(action_config.BEG_PUBLISH_REQ,str)
end

function TradeManager:RequestBegMyList()
	GameWorld.Player().server.beg_action_req(action_config.BEG_ITEM_MY_LIST,"")
end

function TradeManager:RequestBegItemList(auction_type)
	GameWorld.Player().server.beg_action_req(action_config.BEG_ITEM_LIST,tostring(auction_type))
end

function TradeManager:RequestBegItemListAll()
	GameWorld.Player().server.beg_action_req(action_config.BEG_ITEM_LIST_ALL,"")
end

function TradeManager:RequestBegCancel(beg_item_dbid)
	GameWorld.Player().server.beg_action_req(action_config.BEG_CANCEL_REQ,tostring(beg_item_dbid))
end

function TradeManager:RequestBegSell(beg_item_dbid)
	GameWorld.Player().server.beg_action_req(action_config.BEG_SELL_REQ,tostring(beg_item_dbid))
end

function TradeManager:RequestBegSearch(str)
	GameWorld.Player().server.beg_action_req(action_config.BEG_SEARCH ,str)
end

TradeManager:Init()
return TradeManager