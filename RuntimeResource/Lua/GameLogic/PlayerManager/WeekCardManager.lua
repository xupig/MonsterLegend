local WeekCardManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local EntityType = GameConfig.EnumType.EntityType

function WeekCardManager:Init()

end

function WeekCardManager:OnPlayerEnterWorld()
    
end

function WeekCardManager:OnPlayerLeaveWorld()
    
end

function WeekCardManager:HandleData(action_id,error_id,args)
    -- LoggerHelper.Log("WeekCardManager:HandleData "..action_id)
    -- LoggerHelper.Log(args)
    if action_id == action_config.WEEK_CARD_DAILY_REFUND_REQ then

    end
end

-----------------------------数据处理---------------------------

-------------------------------------功能接口-----------------------------------
function WeekCardManager:GetWeekCardState()
    if GameWorld.Player().week_card_flag == 1 then
        if GameWorld.Player().week_card_daily_refund_flag == 1 then
            --已领取
            return 2
        else
            --可领取
            return 1
        end
    else
        --未购买周卡
        return 0
    end
end
-------------------------------------客户端请求-----------------------------------
function WeekCardManager:GetWeekCradRewardReq()
    GameWorld.Player().server.week_card_action_req(action_config.WEEK_CARD_DAILY_REFUND_REQ, "")
end


WeekCardManager:Init()
return WeekCardManager