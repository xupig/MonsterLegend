local DailyActivityExManager = {}

local action_config = require("ServerConfig/action_config")
local ActivityAppearanceDataHelper = GameDataHelper.ActivityAppearanceDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local GUIManager = GameManager.GUIManager
local DailyConfig = GameConfig.DailyConfig
local CommonTimeListConfig = GameConfig.CommonTimeListConfig
local dailyRewardBackListData = PlayerManager.PlayerDataManager.dailyRewardBackListData
local MapManager = PlayerManager.MapManager
local ActivityPointRewardDataHelper = GameDataHelper.ActivityPointRewardDataHelper
local public_config = GameWorld.public_config
local RedPointManager = GameManager.RedPointManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local DailyActivityConfig = GameConfig.DailyActivityConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local error_code = GameWorld.error_code
local RedPointConfig = GameConfig.RedPointConfig
local ActivityLevelDataHelper = GameDataHelper.ActivityLevelDataHelper

require "PlayerManager/PlayerData/CommonTimeListData"
local CommonTimeListData = ClassTypes.CommonTimeListData

local function SortByRank(a,b)
    return tonumber(ActivityDailyDataHelper:GetRank(a)) > tonumber(ActivityDailyDataHelper:GetRank(b))
end

local function SortByTime(a,b)
    local commonTimeDataA = CommonTimeListData(ActivityDailyDataHelper:GetDayLimit(a), ActivityDailyDataHelper:GetTimeLimit(a))
    local commonTimeDataB = CommonTimeListData(ActivityDailyDataHelper:GetDayLimit(b), ActivityDailyDataHelper:GetTimeLimit(b))
    local startTimeA = commonTimeDataA:GetLatestOpenTime()
    local startTimeB = commonTimeDataB:GetLatestOpenTime()
    if startTimeA == nil and startTimeB ~= nil then
        return false
    elseif startTimeB == nil and startTimeA ~= nil then
        return true
    elseif startTimeA == nil and startTimeB == nil then
        return tonumber(ActivityDailyDataHelper:GetRank(a)) > tonumber(ActivityDailyDataHelper:GetRank(b))
    else
        return startTimeA < startTimeB
    end
end

function DailyActivityExManager:Init()
    self:InitCallbackFunc()
end

function DailyActivityExManager:InitCallbackFunc()
    self._onDailyClick = function(follow)self:DoFollowEvent(follow) end
    self._onDailyMapOpen = function()self:OnDailyMapOpen() end
    self._on_0_Oclock = function()self:OnPassDay() end
    self._onFunctionOpen = function(id)self:OnFunctionOpen(id) end
    self._onGetWorldLevel = function(id)self:OnGetWorldLevel(id) end
    self._onLevelChange = function()self:OnLevelChange() end
end

function DailyActivityExManager:RegisterExFindBackFunc(func)
    self._exNode:SetUpdateRedPointFunc(func)
end

function DailyActivityExManager:RegisterCommonFindBackFunc(func)
    self._commonNode:SetUpdateRedPointFunc(func)
end

function DailyActivityExManager:RegisterAppearFunc(func)
    self._appearNode:SetUpdateRedPointFunc(func)
end

function DailyActivityExManager:OnPlayerEnterWorld()
    self._worldLevelInit = GameWorld.Player().world_level ~= 0--等于0代表未得到初始世界等级
    self._notOpenData = {}
    self._findBackNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_DAILY_RESOURCE_FIND)
    self._exNode = RedPointManager:CreateEmptyNode()
    self._commonNode = RedPointManager:CreateEmptyNode()
    self._exNode:SetParent(self._findBackNode)
    self._commonNode:SetParent(self._findBackNode)
    self._appearNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.APPEAR_LEVEL_UP, public_config.FUNCTION_ID_DAILY_ACTIVITY)--外形升级

    EventDispatcher:AddEventListener(GameEvents.ON_DAILY_CLICK, self._onDailyClick)
    EventDispatcher:AddEventListener(GameEvents.Daily_Map_Open, self._onDailyMapOpen)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    EventDispatcher:AddEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)

    self._canAppearId = ActivityAppearanceDataHelper:GetAppearanceIdByLevel(GameWorld.Player().daily_level)
    self:CheckRedPoint()--检查红点情况

    if self._worldLevelInit then
        self:InitLimitActivitys()
    end
end

function DailyActivityExManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_DAILY_CLICK, self._onDailyClick)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Map_Open, self._onDailyMapOpen)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    EventDispatcher:RemoveEventListener(GameEvents.On_Get_World_Level, self._onGetWorldLevel)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChange)
end

function DailyActivityExManager:InitLimitActivitys()
    --公会争霸开服第几天支持
    local openDay = GlobalParamsHelper.GetParamValue(690)--开服第几天活动开启
    local nowOpenDay = DateTimeUtil.GetServerOpenDays()--现在是开服第几天
    local now = DateTimeUtil.GetServerTime()
    local nowDay = DateTimeUtil.GetWday()--今天是周几
    self._forceOpen = false
    self._forceClose = false
    if nowOpenDay < openDay then
        --开服未够四天,不用处理
        if nowDay == 7 then
            self._forceClose = true
        end
    elseif nowOpenDay > openDay then
        --开服超过四天,不用处理
    else
        --今天是开服第四天
        self._forceOpen = true
    end

    local ids = ActivityDailyDataHelper:GetAllIdByType(DailyConfig.Limit)
    for i,v in ipairs(ids) do
        self:InitLimitActivity(v)
    end
    EventDispatcher:TriggerEvent(GameEvents.LIMIT_ACTIVITY_INIT_COMPLETE)
end

function DailyActivityExManager:OnGetWorldLevel()
    if self._worldLevelInit == false then
        self:InitLimitActivitys()
    else
        self:CheckNotOpenLimitActivety()
    end
    self._worldLevelInit = true
end

function DailyActivityExManager:OnFunctionOpen(id)
    if id == public_config.FUNCTION_ID_DAILY_ACTIVITY or id == public_config.FUNCTION_ID_DAILY_RESOURCE_FIND then
        self:CheckRedPoint()--检查红点情况
    end
end

function DailyActivityExManager:OnLevelChange()
    --LoggerHelper.Error("等级变化")
    self:CheckNotOpenLimitActivety()
end

function DailyActivityExManager:CheckNotOpenLimitActivety()
    for k,v in pairs(self._notOpenData) do
        self:InitLimitActivity(k)
    end
end

function DailyActivityExManager:OnPassDay()
    self:InitLimitActivitys()
end

function DailyActivityExManager:InitLimitActivity(id)
    if id == 22 and self._forceClose then --公会争霸
        return
    end

    -- LoggerHelper.Error("DailyActivityExManager:InitLimitActivity(id)======="  .. tostring(id))
    local cfgData = ActivityDailyDataHelper.GetActivityDaily(id)
    local day_limit = cfgData.day_limit
    local time_limit = cfgData.time_limit
    local open_level = cfgData.open_level
    local openWorldLevel = ActivityDailyDataHelper:GetOpenWorldLevel(id)
    local level = GameWorld.Player().level
    local nowWorldLevel = GameWorld.Player().world_level
    

    if (open_level == -1 or level < open_level) or nowWorldLevel < openWorldLevel then
        if open_level ~= -1 and self._notOpenData[id] == nil then
            self._notOpenData[id] = 1
        end
        -- LoggerHelper.Error("DailyActivityExManager:InitLimitActivity(id) end =====" .. tostring(id))
        return
    else
        if self._notOpenData[id] then
            self._notOpenData[id] = nil
        end
    end

    if self:CheckDayOpen(day_limit, id) then
        local timeData = self:CheckTimeOpen(time_limit, id)
        if timeData.status == DateTimeUtil.Over then 
            -- LoggerHelper.Error("DailyActivityExManager:InitLimitActivity(id) end 1111111 ==============" .. tostring(id))
            return 
        end

        if timeData.status == DateTimeUtil.Openning then
            -- LoggerHelper.Error("开启中====222222222222" .. tostring(id))
            local openTimeData = timeData.timeDatas[timeData.openIndex]
            self:ShowOpenIcon(id, openTimeData.closeTime)
            self:ShowRemindIcon(id, openTimeData.closeTime)
            DateTimeUtil.AddTimeCallback(openTimeData.endHour, openTimeData.endMin, 0, false, function()self:CloseOpenIcon(id) end)
        end

        
        if timeData and timeData.timeDatas then
            -- LoggerHelper.Error("准备开启 ========" .. tostring(id))
            for k,v in pairs(timeData.timeDatas) do
                if v.status == DailyActivityConfig.NOT_OPEN then
                    local now = DateTimeUtil.GetServerTime()
                    local preCloseTime = DateTimeUtil.TodayTimeStamp(v.hour, v.min)
                    if now > DateTimeUtil.TodayTimeStamp(v.preHour, v.preMin) and 
                    now < DateTimeUtil.TodayTimeStamp(v.hour, v.min)  then
                        --打开准备开始倒计时
                        --LoggerHelper.Error("打开准备倒计时 =-=== " .. tostring(now - DateTimeUtil.TodayTimeStamp(v.preHour, v.preMin)))
                        self:ShowPreOpenIcon(id, preCloseTime)
                    else
                        DateTimeUtil.AddTimeCallback(v.preHour, v.preMin, 0, false, function()self:ShowPreOpenIcon(id, preCloseTime) end)
                    end

                    DateTimeUtil.AddTimeCallback(v.hour, v.min, 0, false, function()self:ShowOpenIcon(id,v.closeTime) end)
                    DateTimeUtil.AddTimeCallback(v.endHour, v.endMin, 0, false, function()self:CloseOpenIcon(id) end)
                end
            end
        else
            -- LoggerHelper.Error("什么都没有 ========" .. tostring(id))
        end
    else
        return
    end
end

function DailyActivityExManager:ShowPreOpenIcon(id, closeTime)
    local panelId = ActivityDailyDataHelper:GetPanelOpenId(id)
    EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, panelId, function()
        --活动即将开始
        local name = LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(id))
        GUIManager.ShowText(1, LanguageDataHelper.CreateContent(52106, {
            ["0"] = name
        }))
    end, closeTime, true)
end

function DailyActivityExManager:ShowRemindIcon(id, closeTime)
    EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_REMIND_ICON, id, function()
        local follow = ActivityDailyDataHelper:GetFollow(id)
        self:DoFollowEvent(follow)
    end, closeTime)
end

function DailyActivityExManager:CloseRemindIcon(id)
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_REMIND_ICON, id)
end

function DailyActivityExManager:ShowOpenIcon(id, closeTime)
    -- LoggerHelper.Error("closetime ===" .. PrintTable:TableToStr(DateTimeUtil.SomeDay(closeTime)))
    local panelId = ActivityDailyDataHelper:GetPanelOpenId(id)
    EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, panelId, function()
        local follow = ActivityDailyDataHelper:GetFollow(id)
        self:DoFollowEvent(follow)
    end, closeTime)
    if ActivityDailyDataHelper:GetOpenTipsShow(id) == 1 then
        GUIManager.ShowPanel(PanelsConfig.ActivityOpenTips, id)
    end
end

function DailyActivityExManager:CloseOpenIcon(id)
    self:CloseRemindIcon(id)
    local panelId = ActivityDailyDataHelper:GetPanelOpenId(id)
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, panelId)
end

function DailyActivityExManager:CheckTimeOpen(time_limit)
    local datas = {}
    datas.status = DailyActivityConfig.NOT_OPEN
    datas.timeDatas = {}
    if table.isEmpty(time_limit) then
        --全天开放
        local data = {}
        data.startTime = 0
        data.endTime = DateTimeUtil.OneDayTime
        data.status = DateTimeUtil.Openning
        table.insert(datas.timeDatas, data)
        datas.status = DateTimeUtil.Openning
        datas.openIndex = 1
        return datas
    else
        local num = #time_limit
        local datas = {}
        local endTime = time_limit[num - 3] * DateTimeUtil.OneHourTime + time_limit[num - 2] * DateTimeUtil.OneMinTime + time_limit[num - 1] * DateTimeUtil.OneHourTime + time_limit[num] * DateTimeUtil.OneMinTime
        local startTime = time_limit[1] * DateTimeUtil.OneHourTime + time_limit[2] * DateTimeUtil.OneMinTime
        local time = DateTimeUtil.Now()
        local seconds = time.hour * DateTimeUtil.OneHourTime + time.min * DateTimeUtil.OneMinTime + time.sec
        if seconds >= endTime then
            --已结束
            datas.status = DateTimeUtil.Over
            return datas
        end

        local timeDatas = {}
        for i=1 ,num ,4 do
            if i + 3 > num then
                LoggerHelper.Error("activity_daily.xml time_limit error id ======" .. id)
            else
                local data = {}
                local addHour = 0
                local dur_time = time_limit[i+2] * DateTimeUtil.OneHourTime + time_limit[i+3] * DateTimeUtil.OneMinTime
                data.hour = time_limit[i]
                data.min = time_limit[i+1]
                data.startTime = time_limit[i] * DateTimeUtil.OneHourTime + time_limit[i+1] * DateTimeUtil.OneMinTime
                data.endTime = data.startTime + dur_time
                data.endMin = time_limit[i+1] + time_limit[i+3]
                if data.endMin >= 60 then
                    data.endMin = data.endMin - 60
                    data.endHour = data.hour + time_limit[i+2] + 1
                else
                    data.endHour = data.hour + time_limit[i+2]
                end
                data.closeTime = DateTimeUtil.TodayTimeStamp(data.endHour, data.endMin)
                
                data.status = DateTimeUtil.CheckTimeByCondition(data.startTime, dur_time)

                --准备开始倒计时相关参数
                data.preMin = data.min - GlobalParamsHelper.GetParamValue(888) / DateTimeUtil.OneMinTime
                data.preHour = data.hour
                if data.preMin < 0 then
                    data.preMin = data.preMin + 60
                    data.preHour = data.preHour - 1
                end
                data.preCloseTime = DateTimeUtil.TodayTimeStamp(data.preHour, data.preMin)

                -- LoggerHelper.Error("循环 data.status ====" .. tostring(data.status))
                table.insert(timeDatas, data)
            end
        end
        datas.timeDatas = timeDatas

        if seconds < startTime then
            datas.status = DateTimeUtil.Not_Open            
        else
            local status = DateTimeUtil.Not_Open
            for i,v in ipairs(datas.timeDatas) do
                if v.status == DateTimeUtil.Openning then
                    status = DateTimeUtil.Openning
                    datas.openIndex = i
                    break
                end
            end
            datas.status = status
        end
        return datas
    end
end

function DailyActivityExManager:CheckDayOpen(day_limit, id)
    if self._forceOpen and id == 22 then
        return true
    end

    if table.isEmpty(day_limit) then
        --每天开放
        return true
    else
        local wDay = DateTimeUtil.GetWday()--今天星期几
        if table.containsValue(day_limit, wDay) then
            --今天开放
            return true
        else
            --今天不开放
            return false
        end
    end
end

--检查日常红点情况
function DailyActivityExManager:CheckRedPoint()
    self:CheckMustRedPoint()
    self:GetRewardBackInfo()
end

--检查外形升级红点情况
function DailyActivityExManager:CheckAppearLevelUp()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DAILY_ACTIVITY) then
        return false
    end
    local maxLevel = ActivityLevelDataHelper:GetMaxLevel()
    local exp = GameWorld.Player().daily_point
    local levelUpExp = ActivityLevelDataHelper:GetNextCost(GameWorld.Player().daily_level)
    local flag = true
    if  (exp < levelUpExp) or self._maxLevel == GameWorld.Player().daily_level then
        flag = false
    end

    self._appearNode:CheckStrengthen(flag)
    return flag
end

--检查找回红点情况
function DailyActivityExManager:CheckFindBackRedPoint()
    --LoggerHelper.Error("找回红点=-==" .. tostring(dailyRewardBackListData:IsCanFindBack()))
    self._exNode:SetRedPoint(dailyRewardBackListData:IsCanExFindBack())
    self._commonNode:SetRedPoint(dailyRewardBackListData:IsCanCommonFindBack())
    if dailyRewardBackListData:IsCanExFindBack() and dailyRewardBackListData:IsCanCommonFindBack() and 
        FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DAILY_RESOURCE_FIND) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, function()
            GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_DAILY_RESOURCE_FIND)
        end,EnumType.RemindType.DailyResource)
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.DailyResource)
    end
end

--检查每日必做红点情况
function DailyActivityExManager:CheckMustRedPoint()
    --1.检查是否存在未做的日常任务
    local must = self:CheckHasTaskToDo()
    --2.检查是否存在未领取的任务奖励
    local reward = self:CheckHasRewardToGet()
    --3.外形升级
    local appear = self:CheckAppearLevelUp()

    --LoggerHelper.Error("每日必做红点===" .. tostring(must or reward))
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_DAILY_ACTIVITY, must or reward or appear)
end

--检查是否存在未做的日常任务
function DailyActivityExManager:CheckHasTaskToDo()
    local nowLevel = GameWorld.Player().level
    local ids = ActivityDailyDataHelper:GetAllIdByType(DailyConfig.Must)
    local timesList = GameWorld.Player().daily_activity_cur_counts
    for i,id in ipairs(ids) do
        local openLevel = ActivityDailyDataHelper:GetOpenLevel(id)
        if openLevel ~= -1 then
            if nowLevel < openLevel then
                --未开启                
            else
                --已开启
                local times = ActivityDailyDataHelper:GetTimes(id)
                if times ~= 0 then
                    if timesList[id] and timesList[id] >= times then
                        --已完成的
                    else
                        --存在未完成的
                        return true
                    end
                end
            end
        end
    end
    return false
end

--检查是否存在未领取的任务奖励
function DailyActivityExManager:CheckHasRewardToGet()
    local rewardDatas = ActivityPointRewardDataHelper:GetAllId()
    -- local maxNum = #rewardDatas
    local dailyPoints = GameWorld.Player().daily_activity_point
    local rewardsRecord = GameWorld.Player().daily_activity_point_reward
    local canGetNum = 0
    local alreadyGetNum = 0

    for i,v in ipairs(rewardDatas) do
        if dailyPoints >= ActivityPointRewardDataHelper:GetCostPoint(v) then
            canGetNum = canGetNum + 1
        else
            break
        end
    end

    for k,v in pairs(rewardsRecord) do
        alreadyGetNum = alreadyGetNum + 1
    end

    return alreadyGetNum < canGetNum
end




function DailyActivityExManager:OnDailyMapOpen()
    MapManager:ShowRecommendMonsterPoint()
end

function DailyActivityExManager:GetCanAppearId()
    return self._canAppearId
end

function DailyActivityExManager:SetCanAppearId(appearId)
    self._canAppearId = appearId
end

function DailyActivityExManager:GetLimitDailyListData()
    local openningList = {}--今日开启， 开启中 rank越大排越前
    local waitOpenList = {}--今日开启，未到开启时间 距离开启时间越近排越前
    local completeList = {}--今日开启，已经完成 rank越大排越前
    local overTimeList = {}--今日开启，已过活动时间 rank越大排越前
    local notTodayOpenList = {}--今日不开启，rank越大排越前     
    local notOpenList = {}--未达到开启条件，rank越大排越前
    local ids = ActivityDailyDataHelper:GetAllIdByType(DailyConfig.Limit)
    local nowLevel = GameWorld.Player().level
    local nowWorldLevel = GameWorld.Player().world_level
    local timesList = GameWorld.Player().daily_activity_cur_counts
    local wDay = DateTimeUtil.GetWday()--今天星期几

    for i,id in ipairs(ids) do
        local openLevel = ActivityDailyDataHelper:GetOpenLevel(id)
        local openWorldLevel = ActivityDailyDataHelper:GetOpenWorldLevel(id)
        if openLevel ~= -1 then
            if nowLevel < openLevel then
                table.insert(notOpenList, id)
            elseif nowWorldLevel < openWorldLevel then
                --LoggerHelper.Error("openWorldLevel 未开启")
                table.insert(notOpenList, id)
            else
                local times = ActivityDailyDataHelper:GetTimes(id)
                if timesList[id] and timesList[id] >= times then
                    table.insert(completeList, id)
                else
                    local commonTimeData = CommonTimeListData(ActivityDailyDataHelper:GetDayLimit(id), ActivityDailyDataHelper:GetTimeLimit(id))
                    local status = commonTimeData:GetOpenStatus()
                    if status == CommonTimeListConfig.OPENNING then
                        table.insert(openningList, id)
                    elseif status == CommonTimeListConfig.WAIT_OPEN then
                        table.insert(waitOpenList, id)
                    elseif status == CommonTimeListConfig.OVER_TIME then
                        table.insert(completeList, id)
                    elseif status == CommonTimeListConfig.NOT_OPEN_TODAY then
                        table.insert(notTodayOpenList, id)
                    end           
                end
            end
        end
    end

    table.sort(openningList, SortByRank)
    table.sort(waitOpenList, SortByTime)
    table.sort(completeList, SortByRank)
    table.sort(overTimeList, SortByRank)
    table.sort(notTodayOpenList, SortByRank)
    table.sort(notOpenList, SortByRank)

    
    for i=1,#waitOpenList do
        table.insert(openningList, waitOpenList[i])
    end

    for i=1,#completeList do
        table.insert(openningList, completeList[i])
    end

    for i=1,#overTimeList do
        table.insert(openningList, overTimeList[i])
    end

    for i=1,#notTodayOpenList do
        table.insert(openningList, notTodayOpenList[i])
    end

    for i=1,#notOpenList do
        table.insert(openningList, notOpenList[i])
    end
    -- LoggerHelper.Error("openningList === " .. PrintTable:TableToStr(openningList))
    return openningList
end

function DailyActivityExManager:GetMustDailyListData()
    local mustList = {}
    local notCompleteList = {}
    local completeList = {}
    local notOpenList = {}
    local ids = ActivityDailyDataHelper:GetAllIdByType(DailyConfig.Must)
    local nowLevel = GameWorld.Player().level
    local timesList = GameWorld.Player().daily_activity_cur_counts

    for i,id in ipairs(ids) do
        local openLevel = ActivityDailyDataHelper:GetOpenLevel(id)
        if openLevel ~= -1 then
            DateTimeUtil.GetTodayMins()
            if nowLevel < openLevel then
                table.insert(notOpenList, id)
            else
                local times = ActivityDailyDataHelper:GetTimes(id)
                if timesList[id] and timesList[id] >= times then
                    table.insert(completeList, id)
                else
                    table.insert(notCompleteList, id)
                end
            end
        end
    end

    table.sort(notCompleteList, SortByRank)
    table.sort(completeList, SortByRank)
    table.sort(notOpenList, SortByRank)
    
    for i=1,#completeList do
        table.insert(notCompleteList, completeList[i])
    end

    for i=1,#notOpenList do
        table.insert(notCompleteList, notOpenList[i])
    end

    return notCompleteList
end

--返回显示活动的开始时间列表
--{["startTime"] = 22222, ["endTime"] = 2222222}
function DailyActivityExManager:GetMinsList(openDays)
    local datas = {}
    local endTimes = 0
    for i,v in ipairs(openDays) do
        local n = i % 4
        if n == 1 then
            local data = {}
            local startHour = v[i]          --开始时间：时
            local startMin = v[i+1]         --开始时间：分   
            local dailyHour = v[i+2]        --持续时间：时
            local dailyMin = v[i+3]         --持续时间：分
            data.startTime = startHour * 60 + startMin
            data.endTime = startHour * 60 + startMin + dailyHour * 60 + dailyMin
            table.insert(datas, data)
        end
    end
    return datas
end

function DailyActivityExManager:DoFollowEvent(follow)
    if not follow or type(follow) ~= "table" then
        LoggerHelper.Error("Invalid Event follow :" .. tostring(follow))
        return
    end

    for k, params in pairs(follow) do
        local param = params[1]
        if k == DailyConfig.FOLLOW_OPEN_PANEL then-- 1.打开界面，填功能ID
            GUIManager.ShowPanelByFunctionId(param)
        elseif k == DailyConfig.FOLLOW_EVENT then-- 2.事件,填事件名称
            EventDispatcher:TriggerEvent(GameEvents[param])
        end
    end
end

--获取金币找回列表数据
function DailyActivityExManager:GetCommonRewardListDatas()
    return dailyRewardBackListData:GetCommonRewardListDatas()
end

--获取钻石找回列表数据
function DailyActivityExManager:GetExRewardListDatas()
    return dailyRewardBackListData:GetExRewardListDatas()
end

-- function DailyActivityExManager:HandleRewardBackData(action_id, error_id, args)
--     if error_id ~= 0 then --报错了
--         LoggerHelper.Error("DailyActivity HandleRewardBackData Error: error_id==" .. error_id .. "  action_id ==" .. action_id)
--         return
--     elseif action_id == action_config.REWARD_BACK_GET_INFO then           --8151,     --获取找回信息
--         LoggerHelper.Log("Sam: 资源找回列表数据22 ======" .. PrintTable:TableToStr(args))
--     elseif action_id == action_config.REWARD_BACK_GET_REWARD then           --8152,     --找回奖励
--         LoggerHelper.Log("Sam: 找回奖励22 ======" .. PrintTable:TableToStr(args))        
--     else
--         LoggerHelper.Error("DailyActivityExManager HandleRewardBackData action id not handle:" .. action_id)
--     end
-- end

function DailyActivityExManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("DailyActivity Error: error_id==" .. error_id .. "  action_id ==" .. action_id)
        if action_id == action_config.REWARD_BACK_GET_REWARD and error_id == error_code.ERR_COUPONS_BIND_NOT_ENOUGH then
            GUIManager.ShowChargeMessageBox()
        end
        return
    elseif action_id == action_config.ACTIVITY_POINT_REWARD_REQ then                    --7851,     --领取活跃点奖励
        -- LoggerHelper.Error("7851 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ON_ACTIVITY_GETREWARD_COMPLETE, args[1])
    elseif action_id == action_config.ACTIVITY_SHOW then                                --7852,     --显示幻化
        EventDispatcher:TriggerEvent(GameEvents.ON_ACTIVITY_APPEAR_CHANGE)
        -- LoggerHelper.Error("7852 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.ACTIVITY_UNSHOW then                              --7853,     --不显示
        -- LoggerHelper.Error("7853 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.ACTIVITY_LEVEL_UP then                            --7854,     --升级
        -- LoggerHelper.Error("7854 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ON_ACTIVITY_APPEAR_LEVEL_UP)
    elseif action_id == action_config.REWARD_BACK_GET_INFO then           --8151,     --获取找回信息
        --LoggerHelper.Error("资源找回列表数据 ======" .. PrintTable:TableToStr(args))
        dailyRewardBackListData:ResetData(args)
        EventDispatcher:TriggerEvent(GameEvents.ON_GET_REWARD_BACK_DATA)
        self:CheckFindBackRedPoint()
    elseif action_id == action_config.REWARD_BACK_GET_REWARD then           --8152,     --找回奖励
        -- LoggerHelper.Error("找回奖励 ======" .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.REWARD_BACK_GET_REWARD_COMPLETE)        
    else
        LoggerHelper.Error("DailyActivityExManager action id not handle:" .. action_id)
    end
end

--7851,     --领取活跃点奖励
function DailyActivityExManager:GetReward(id)
    GameWorld.Player().server.activity_action_req(action_config.ACTIVITY_POINT_REWARD_REQ, tostring(id))        
end

--7852,     --显示幻化
function DailyActivityExManager:SetAppearanceShowId(id)
    GameWorld.Player().server.activity_action_req(action_config.ACTIVITY_SHOW, tostring(id))
end

--7853,     --不显示
function DailyActivityExManager:SetAppearanceHide()
    GameWorld.Player().server.activity_action_req(action_config.ACTIVITY_UNSHOW, '')
end

--7854,     --升级
function DailyActivityExManager:LevelUp()
    GameWorld.Player().server.activity_action_req(action_config.ACTIVITY_LEVEL_UP, '')
end

--8151,     --获取找回信息
function DailyActivityExManager:GetRewardBackInfo()
    GameWorld.Player().server.reward_back_action_req(action_config.REWARD_BACK_GET_INFO, '')
end

--8152,     --找回奖励
function DailyActivityExManager:GetRewardBack(activity_id, back_type, count)
    local params = string.format("%d,%d,%d", activity_id, back_type, count)
    GameWorld.Player().server.reward_back_action_req(action_config.REWARD_BACK_GET_REWARD, params)
end



--------------------------------------活动获取方法定义----------------------------------------------
function DailyActivityExManager.CanEnter()
    return true
end

-- function DailyActivityExManager.CanEnter

DailyActivityExManager:Init()
return DailyActivityExManager