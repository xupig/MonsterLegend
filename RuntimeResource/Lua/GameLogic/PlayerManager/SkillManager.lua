local SkillManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local skillData = PlayerDataManager.skillData
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local SkillDataHelper = GameDataHelper.SkillDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function SkillManager:Init()
	
end

function SkillManager:HandleData(luaTable)
   	
end

function SkillManager:OnPlayerEnterWorld()
    self._showAddTalentRedPoint = false

    self:RefreshAddTalentRedPointState()
end

function SkillManager:OnPlayerLeaveWorld()

end

function SkillManager:RefreshLearnedSkills(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Active_Skill_Data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
    if oldValue ~= nil then
        self:RefreshLearnedSkillDict()
    end
    skillData:RefreshLearnedSkills(oldValue)
end

function SkillManager:RefreshLearnedPassiveSpell(oldValue)
    if oldValue ~= nil then
        self:RefreshLearnedSkillDict()
    end
    skillData:RefreshLearnedPassiveSpell(oldValue)
end

function SkillManager:RefreshSkillsAutoCastInfo(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Active_Skill_Auto_Cast)
end

function SkillManager:RefreshTalentInfo(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Skill_Talent_Info)
end

function SkillManager:RefreshTalentSpendInfo(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Skill_Talent_Spend_Info)
end

function SkillManager:SetSkillButtonPosInfo(data)
    skillData:SetSkillButtonPosInfo(data)
end

function SkillManager:RefreshLearnedSkillDict()
    skillData:RefreshLearnedSkillDict()
end

-----------------------------------红点------------------------------
function SkillManager:RefreshAddTalentRedPointState()
    local types = SkillDataHelper.GetAllTalentTypes()
    local showRedPointTypeIndexs = {}
    local vocGroup = GameWorld.Player():GetVocationGroup()
    local state = false
    for k,v in pairs(types) do
        local type = v[1]
        local indexs = GlobalParamsHelper.GetParamValue(811)[type]
        for k1,v1 in pairs(indexs) do
            local index = v1
            local level = self:GetTalentLevel(type, index)
            local maxLevel = SkillDataHelper.GetTalentLvMaxLevel(type, index)
            local cfg = SkillDataHelper.GetSkillTalentData(vocGroup, type, index, level)
            local activeState = SkillManager:GetActiveState(type, index)
            if activeState ~= 0 and level < maxLevel and GameWorld.Player().money_talent >= cfg.levelup_cost then
                state = true
                showRedPointTypeIndexs[k] = 1
                break
            end
        end
    end
    self:SetAddTalentRedPoint(state)
    self:RefreshTalentRedPointTypeData(showRedPointTypeIndexs)
end

function SkillManager:SetAddTalentRedPoint(state)
    if self._showAddTalentRedPoint ~= state then
        self._showAddTalentRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_TALENT, self:GetAddTalentRedPoint())
    end
end

function SkillManager:GetAddTalentRedPoint()
    return self._showAddTalentRedPoint
end

----------------------------------------------客户端接口-----------------------------------------------------------
function SkillManager:IsPassiveSkillGot(skillId)
    return skillData:IsPassiveSkillGot(skillId)
end

function SkillManager:IsActiveSkillGot(id)
    return skillData:IsActiveSkillGot(id)
end

function SkillManager:GetSlotConfigData()
    return skillData:GetSlotConfigData()
end

function SkillManager:GetSkillsAutoCastState(slot)
    return skillData:GetSkillsAutoCastState(slot)
end

function SkillManager:GetSkillsAutoCastStateByAI(slot)
    return skillData:GetSkillsAutoCastStateByAI(slot)
end

function SkillManager:GetTalentLevel(type, group)
    local player = GameWorld.Player()
    if player.talent_info[type] ~= nil and player.talent_info[type][group] ~= nil then
        return player.talent_info[type][group]
    end
    return 0
end

function SkillManager:GetTalentSpend(type)
    local player = GameWorld.Player()
    if player.talent_spend_info[type] ~= nil then
        return player.talent_spend_info[type]
    end
    return 0
end

function SkillManager:GetAllTalentSpend()
    local player = GameWorld.Player()
    local result = 0
    for k,v in pairs(player.talent_spend_info) do
        result = result + v
    end
    return result
end

function SkillManager:GetSkillButtonPosInfo(slot)
    return skillData:GetSkillButtonPosInfo(slot)
end

function SkillManager:CheckShowSkillOpen()
    skillData:CheckShowSkillOpen()
end

function SkillManager:GetActiveState(type, id)
    local vocGroup = RoleDataHelper.GetVocGroup(GameWorld.Player().vocation)
    local level = self:GetTalentLevel(type, id)
    local cfg = SkillDataHelper.GetSkillTalentData(vocGroup, type, id, level)
    local state = 1
    if level > 0 then
        state = 2
        return state
    end
    for i=1,#cfg.condition do
        local config = cfg.condition[i]
        local k = config[1]
        local v = config[2]
        if k == 1 then
            local points = self:GetTalentSpend(1)
            if points < v then
                state = 0
                break
            end
        elseif k == 2 then
            local points = self:GetTalentSpend(2)
            if points < v then
                state = 0
                break
            end
        elseif k == 3 then
            local data = SkillDataHelper.GetSkillTalentCfg(v)
            local curLevel = self:GetTalentLevel(data.type, data.group)
            if curLevel < data.level then
                state = 0
                break
            end
        end
    end
    return state
end

function SkillManager:RefreshTalentRedPointTypeData(data)
    skillData:RefreshTalentRedPointTypeData(data)
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Skill_Talent_Red_Point)
end

function SkillManager:GetTalentRedPointTypeData()
    return skillData:GetTalentRedPointTypeData()
end

----------------------------------------------客户端请求-----------------------------------------------------------
function SkillManager:SkillChangeSlotReq(slot_id ,target_play_slot_id)
    local params = string.format("%d,%d", slot_id, target_play_slot_id)
    GameWorld.Player().server.skill_action_req(action_config.SKILL_CHANGE_SLOT, params)
end

function SkillManager:SkillChangeAutoCastReq(slot_id)
    local params = string.format("%d", slot_id)
    GameWorld.Player().server.skill_action_req(action_config.SKILL_CHANGE_AUTO_CAST, params)
end

function SkillManager:TalentLevelUpReq(talentType, talentGroup)
    local params = string.format("%d,%d", talentType, talentGroup)
    GameWorld.Player().server.talent_action_req(action_config.TALENT_LEVEL_UP, params)
end

function SkillManager:TalentResetReq()
    GameWorld.Player().server.talent_action_req(action_config.TALENT_RESET, "")
end

SkillManager:Init()
return SkillManager