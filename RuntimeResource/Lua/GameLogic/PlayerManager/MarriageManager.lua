local MarriageManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local SystemInfoManager = GameManager.SystemInfoManager
local error_code = GameWorld.error_code
local StringStyleUtil = GameUtil.StringStyleUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local RedPointManager = GameManager.RedPointManager
local PlayerActionManager = GameManager.PlayerActionManager
local TaskConfig = GameConfig.TaskConfig
local TaskCommonManager = PlayerManager.TaskCommonManager

function MarriageManager:Init()
    self._askInviteList = {}
    self._weddingStateTimers = {}
    self:InitCallbackFunc()
end

function MarriageManager:InitCallbackFunc()
    self._onShowGetMarry = function(npcId)self:ShowGetMarry(npcId) end
    self._onMarriageAskInviteChanged = function()self:OnMarriageAskInviteChanged() end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onClientEntityCollectComplete = function(entityCfgId, eventId)self:OnClientEntityCollectComplete(entityCfgId, eventId) end
    self._onGoToMarry = function() self:GotoMarriage() end
end

function MarriageManager:OnPlayerEnterWorld()
    self:CheckWeddingState()
    EventDispatcher:AddEventListener(GameEvents.SHOW_GET_MARRY, self._onShowGetMarry)
    EventDispatcher:AddEventListener(GameEvents.MarriageAskInviteChanged, self._onMarriageAskInviteChanged)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.GO_TO_MARRY, self._onGoToMarry)
    self:QueryInvite()
    self._guestManagerNode = RedPointManager:CreateEmptyNode()
    self._guestManagerNode:SetParent(RedPointManager:GetRedPointDataByPanelName(PanelsConfig.GetMarry))
end

function MarriageManager:OnPlayerLeaveWorld()
    self:CheckWeddingState()
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_GET_MARRY, self._onShowGetMarry)
    EventDispatcher:RemoveEventListener(GameEvents.MarriageAskInviteChanged, self._onMarriageAskInviteChanged)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.GO_TO_MARRY, self._onGoToMarry)
end

function MarriageManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    local player = GameWorld.Player()
    if sceneType == SceneConfig.SCENE_TYPE_WEDDING then
        EventDispatcher:AddEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
        local name1, name2, vocation1, vocation2, dbid1, dbid2 = self:GetCurWeddingHourData()
        if player.dbid == dbid1 or player.dbid == dbid2 then
            local dress = MarriageManager:GetWeddingDress()
            --LoggerHelper.Log("Ash: OnEnterMap: " .. dress)
            if dress then
                if dress[1] then
                    player:SetCurCloth(dress[1])
                end
                if dress[2] then
                    player:SetCurHead(dress[2])
                end
            end
        else
            player:ShowCurClothAndHead()
        end
    else
        player:ShowCurClothAndHead()
    end
end

function MarriageManager:OnLeaveMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_WEDDING then
        GameWorld.Player():ShowCurClothAndHead()
        EventDispatcher:RemoveEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
    end
end

function MarriageManager:OnClientEntityCollectComplete(entityCfgId, eventId)
    -- LoggerHelper.Log("Ash: MARRY_TASTE_FOOD")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_TASTE_FOOD, "")
end


function MarriageManager:RegistGuestManagerUpdateFunc(func)
    self._guestManagerNode:SetUpdateRedPointFunc(func)
end

function MarriageManager:RegistGuestManagerUpdateFunc2(func)
    self._guestManagerNode:SetUpdateRedPointFunc2(func)
end

function MarriageManager:OnMarriageAskInviteChanged()
    local list = self:GetAskInviteList()
    self._guestManagerNode:SetRedPoint(not table.isEmpty(list))
end

function MarriageManager:GotoMarriage()
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
    TaskCommonManager:SetCurAutoTask(nil, nil)

    TaskCommonManager:GoToNpc(GlobalParamsHelper.GetParamValue(714), TaskConfig.IS_TELEPORT_TO)--结婚唯一指定NPC
end

function MarriageManager:BeginCheckWeddingState()
    self:CheckWeddingState()
    local allBookingData = self:GetAllBookingData()
    local now_date = DateTimeUtil.Now()
    local cur_hour = now_date.hour
    
    for k, v in pairs(allBookingData) do
        if not table.isEmpty(v) and k - 1 >= cur_hour then
            local timer1 = DateTimeUtil.AddTimeCallback(k - 1, 55, 10, false, function()self:CheckWeddingState() end)
            local timer2 = DateTimeUtil.AddTimeCallback(k, 0, 10, false, function()self:CheckWeddingState() end)
            -- local timer2 = DateTimeUtil.AddTimeCallback(k, self:GetWeddingDuration(), 10, false, function()self:CheckWeddingState() end)
            -- local timer3 = DateTimeUtil.AddTimeCallback(k, self:GetWeddingDuration() + 1, 10, false, function()self:CheckWeddingState() end)
            -- local timer4 = DateTimeUtil.AddTimeCallback(k, self:GetWeddingDuration() + 2, 10, false, function()self:CheckWeddingState() end)
            -- local timer5 = DateTimeUtil.AddTimeCallback(k, self:GetWeddingDuration() + 3, 10, false, function()self:CheckWeddingState() end)
            table.insert(self._weddingStateTimers, timer1)
            table.insert(self._weddingStateTimers, timer2)
        -- table.insert(self._weddingStateTimers, timer2)
        -- table.insert(self._weddingStateTimers, timer3)
        -- table.insert(self._weddingStateTimers, timer4)
        -- table.insert(self._weddingStateTimers, timer5)
        end
    end
end

function MarriageManager:StopCheckWeddingState()
    self:CheckWeddingState()
    for i, v in ipairs(self._weddingStateTimers) do
        DateTimeUtil.DelTimeCallback(v)
    end
end

function MarriageManager:CheckWeddingState()
    -- LoggerHelper.Log("Ash: CheckWeddingState: ")
    --可优化成算延迟时间一次调用
    local allBookingData = self:GetAllBookingData()
    local now_date = DateTimeUtil.Now()
    local cur_hour = now_date.hour
    local cur_min = now_date.min
    if cur_min >= 55 then
        cur_hour = cur_hour + 1
    elseif cur_min > self:GetWeddingDuration() then
        cur_hour = nil
    end
    
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Wedding)
    if cur_hour and allBookingData[cur_hour] then
        -- LoggerHelper.Log("Ash: CheckWeddingState: " .. cur_hour)
        if cur_min >= 55 then
            local targetTime = DateTimeUtil.TodayTimeStamp(cur_hour, 0)
            EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.Wedding, function()
                GUIManager.ShowPanel(PanelsConfig.GetMarry, {"HoldingWedding"})
            end, targetTime, true)
        else
            local targetTime = DateTimeUtil.TodayTimeStamp(cur_hour, self:GetWeddingDuration())
            EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.Wedding, function()
                GUIManager.ShowPanel(PanelsConfig.GetMarry, {"HoldingWedding"})
            end, targetTime)
        end
        DateTimeUtil.AddTimeCallback(cur_hour, self:GetWeddingDuration(), 0, false, function()
            EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Wedding)
        end)
    end
end

function MarriageManager:Propose(dbid, grade)
    -- LoggerHelper.Log("Ash: Propose: " .. tostring(dbid) .. "," .. tostring(grade))
    GameWorld.Player().server.marry_action_req(action_config.MARRY_PROPOSE, tostring(dbid) .. "," .. tostring(grade))
end

function MarriageManager:RespondPropose(agree)
    -- LoggerHelper.Log("Ash: RespondPropose: " .. tostring(agree))
    GameWorld.Player().server.marry_action_req(action_config.MARRY_RESPOND_PROPOSE, agree and "1" or "0")
end

function MarriageManager:QueryInfo()
    if self:HaveMate() then
        -- LoggerHelper.Log("Ash: QueryInfo")
        GameWorld.Player().server.marry_action_req(action_config.MARRY_QUERY_INFO, "")
    end
end

function MarriageManager:BookInfo()
    if self:HaveMate() then
        -- LoggerHelper.Log("Ash: BookInfo")
        GameWorld.Player().server.marry_action_req(action_config.MARRY_BOOK_INFO, "")
    end
end

function MarriageManager:MarryBook(hour)
    -- LoggerHelper.Log("Ash: MarryBook: " .. tostring(hour))
    GameWorld.Player().server.marry_action_req(action_config.MARRY_BOOK, tostring(hour))
end

function MarriageManager:MarryInvite(dbid)
    -- LoggerHelper.Log("Ash: MarryInvite: " .. tostring(dbid))
    GameWorld.Player().server.marry_action_req(action_config.MARRY_INVITE, tostring(dbid))
end

function MarriageManager:BugInviteCount()
    -- LoggerHelper.Log("Ash: BugInviteCount: ")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_BUY_INVITE_COUNT, "")
end

function MarriageManager:MarryAsk()
    -- LoggerHelper.Log("Ash: MarryAsk: ")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_ASK, "")
end

function MarriageManager:QueryInvite()
    local levelThresHold = 60 --不足110级也要看到按钮，但为优化新玩家性能，定另一个阀值控制 --GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    local player = GameWorld.Player()
    if player.level >= levelThresHold then
        -- LoggerHelper.Log("Ash: QueryInvite: ")
        player.server.marry_action_req(action_config.MARRY_QUERY_INVITE, "")
    end
end

function MarriageManager:MarryEnter()
    -- LoggerHelper.Log("Ash: MarryEnter")
    GameWorld.Player():StopAutoFight()
    -- GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
    GameWorld.Player().server.marry_action_req(action_config.MARRY_ENTER, "")
end

function MarriageManager:MarryCeremony()
    -- LoggerHelper.Log("Ash: MarryCeremony")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_CEREMONY, "")
end

function MarriageManager:MarrySendRed(dbid, count)
    -- LoggerHelper.Log("Ash: MarrySendRed: " .. dbid .. "," .. count)
    GameWorld.Player().server.marry_action_req(action_config.MARRY_SEND_RED, dbid .. "," .. count)
end

function MarriageManager:MarrySendFlower(dbid, itemId)
    -- LoggerHelper.Log("Ash: MarrySendFlower: " .. dbid .. "," .. itemId .. "," .. public_config.SEND_FLOWER_WAY_MARRY)
    --SEND_FLOWER_SEND(to_dbid,item_id,way)
    --送花类型
    -- SEND_FLOWER_WAY_HIDE    = 1, --匿名送花
    -- SEND_FLOWER_WAY_NAME    = 2, --实名送花
    -- SEND_FLOWER_WAY_MARRY   = 3, --结婚送花
    GameWorld.Player().server.send_flower_action_req(action_config.SEND_FLOWER_SEND, dbid .. "," .. itemId .. "," .. public_config.SEND_FLOWER_WAY_MARRY)
end

function MarriageManager:MarryBlessRecord()
    -- LoggerHelper.Log("Ash: MarryBlessRecord")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_BLESS_RECORD, "")
end

function MarriageManager:Divorce()
    -- LoggerHelper.Log("Ash: Divorce")
    GameWorld.Player().server.marry_action_req(action_config.MARRY_DIVORCE, "")
end

function MarriageManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("Marriage Error:" .. error_id)
        return
    elseif event_id == action_config.MARRY_PROPOSE then
        EventDispatcher:TriggerEvent(GameEvents.ProposeReqSuccess)
    elseif event_id == action_config.MARRY_RECEIVE_PROPOSE then
        self:OnReceivePropose(args)
    elseif event_id == action_config.MARRY_RECEIVE_RESPOND_PROPOSE then
        self:OnReceiveRespondPropose(args)
    elseif event_id == action_config.MARRY_RESPOND_PROPOSE then
        -- LoggerHelper.Log("Ash: MARRY_RESPOND_PROPOSE: " .. PrintTable:TableToStr(args))
        -- self:OnReceiveRespondPropose(args)
        elseif event_id == action_config.MARRY_QUERY_INFO then
        self:OnQueryInfo(args)
        elseif event_id == action_config.MARRY_BOOK_INFO then
            self:OnBookInfo(args)
        elseif event_id == action_config.MARRY_BOOK then
            -- LoggerHelper.Log("Ash: MARRY_BOOK: " .. PrintTable:TableToStr(args))
            elseif event_id == action_config.MARRY_INVITE then
            -- LoggerHelper.Log("Ash: MARRY_INVITE: " .. PrintTable:TableToStr(args))
            elseif event_id == action_config.MARRY_BUY_INVITE_COUNT then
                -- LoggerHelper.Log("Ash: MARRY_BUY_INVITE_COUNT: " .. PrintTable:TableToStr(args))
                elseif event_id == action_config.MARRY_ASK then
                local showText = LanguageDataHelper.GetContent(58784)
                GUIManager.ShowText(2, showText)
                elseif event_id == action_config.MARRY_RECEIVE_ASK then
                    self:OnReceiveAsk(args)
                elseif event_id == action_config.MARRY_QUERY_INVITE then
                    self:OnQueryInvite(args)
                elseif event_id == action_config.MARRY_CEREMONY then
                    -- LoggerHelper.Log("Ash: MARRY_CEREMONY: " .. PrintTable:TableToStr(args))
                    elseif event_id == action_config.MARRY_RECIVE_CEREMONY then
                    self:OnMarryReciveCeremony(args)
                    -- LoggerHelper.Log("Ash: MARRY_RECIVE_CEREMONY: " .. PrintTable:TableToStr(args))
                    elseif event_id == action_config.MARRY_QUERY_INVITE then
                        -- LoggerHelper.Log("Ash: MARRY_QUERY_INVITE: " .. PrintTable:TableToStr(args))
                        elseif event_id == action_config.MARRY_SEND_RED then
                        -- LoggerHelper.Log("Ash: MARRY_SEND_RED: " .. PrintTable:TableToStr(args))
                        self:MarryBlessRecord()
                        -- elseif event_id == action_config.MARRY_SEND_FLOWER then
                        --     LoggerHelper.Log("Ash: MARRY_SEND_FLOWER: " .. PrintTable:TableToStr(args))
                        elseif event_id == action_config.MARRY_BLESS_RECORD then
                            EventDispatcher:TriggerEvent(GameEvents.MarryBlessRecord, args)
                        -- LoggerHelper.Log("Ash: MARRY_BLESS_RECORD: " .. PrintTable:TableToStr(args))
                        elseif event_id == action_config.MARRY_NOTIFY_ROOM_STAGE then
                            -- LoggerHelper.Log("Ash: MARRY_NOTIFY_ROOM_STAGE: " .. PrintTable:TableToStr(args))
                            elseif event_id == action_config.MARRY_NOTIFY_BASIC_INFO then
                            -- LoggerHelper.Log("Ash: MARRY_NOTIFY_BASIC_INFO: " .. PrintTable:TableToStr(args))
                            EventDispatcher:TriggerEvent(GameEvents.MarryNotifyBasicInfo, args)
                            elseif event_id == action_config.MARRY_NOTIFY_EXP then
                                -- LoggerHelper.Log("Ash: MARRY_NOTIFY_EXP: " .. PrintTable:TableToStr(args))
                                EventDispatcher:TriggerEvent(GameEvents.MarryNotifyExp, args)
                            elseif event_id == action_config.MARRY_NOTIFY_FOOD then
                                -- LoggerHelper.Log("Ash: MARRY_NOTIFY_FOOD: " .. PrintTable:TableToStr(args))
                                EventDispatcher:TriggerEvent(GameEvents.MarryNotifyFood, args)
                                self._marryFoodTime = args[1]
                            elseif event_id == action_config.MARRY_NOTIFY_GIFT then
                                -- LoggerHelper.Log("Ash: MARRY_NOTIFY_GIFT: " .. PrintTable:TableToStr(args))
                                EventDispatcher:TriggerEvent(GameEvents.MarryNotifyGift, args)
                            elseif event_id == action_config.MARRY_RECIVE_FIREWORKS then
                                -- LoggerHelper.Log("Ash: MARRY_RECIVE_FIREWORKS: " .. PrintTable:TableToStr(args))
                                GUIManager.ShowPanel(PanelsConfig.Firework, args)
                            elseif event_id == action_config.MARRY_RECIVE_SEND_RED then
                                -- LoggerHelper.Log("Ash: MARRY_RECIVE_SEND_RED: " .. PrintTable:TableToStr(args))
                                self:OnReciveSendRed(args)
                            elseif event_id == action_config.SEND_FLOWER_SEND then
                                -- LoggerHelper.Log("Ash: SEND_FLOWER_SEND: " .. PrintTable:TableToStr(args))
                                if args and args[1] == public_config.SEND_FLOWER_WAY_MARRY then --只处理结婚送花
                                    self:MarryBlessRecord()
                                end
                            elseif event_id == action_config.MARRY_TASTE_FOOD then
                                -- LoggerHelper.Log("Ash: MARRY_TASTE_FOOD: " .. PrintTable:TableToStr(args))
                                else
                                LoggerHelper.Error("Marriage event id not handle:" .. event_id)
    end
end

function MarriageManager:OnReceivePropose(args)
    -- LoggerHelper.Log("Ash: OnReceivePropose: " .. PrintTable:TableToStr(args))
    GUIManager.ClosePanel(PanelsConfig.GetMarry)
    GUIManager.ShowPanel(PanelsConfig.GetMarry, {"ReceivePropose", args})
-- WorldBossData:UpdateWorldBossData(args)
-- EventDispatcher:TriggerEvent(GameEvents.OnWorldBossGetInfo)
end

function MarriageManager:OnReceiveRespondPropose(args)
    -- LoggerHelper.Log("Ash: OnReceiveRespondPropose: " .. PrintTable:TableToStr(args))
    if args and args[1] == 1 then
        GUIManager.ClosePanel(PanelsConfig.GetMarry)
        GUIManager.ShowPanel(PanelsConfig.GetMarry, {"ReceiveRespondPropose", args})
    end
-- WorldBossData:UpdateWorldBossData(args)
-- EventDispatcher:TriggerEvent(GameEvents.OnWorldBossGetInfo)
end

function MarriageManager:OnQueryInfo(args)
    -- LoggerHelper.Log("Ash: OnQueryInfo: " .. PrintTable:TableToStr(args))
    -- {to_dbid,to_name,to_facade,to_vocation}
    self._mateInfo = args
    EventDispatcher:TriggerEvent(GameEvents.MarriageQueryInfo)
end

function MarriageManager:OnBookInfo(args)
    -- LoggerHelper.Log("Ash: OnBookInfo: " .. PrintTable:TableToStr(args))
    --{wedding_count,grade,hour,my_book_data} --需要mate_name ~= "" --如果hour为nil就是没预约。--my_book_data结构参考BOOK_KEY_INVITED
    self._bookInfo = args
    EventDispatcher:TriggerEvent(GameEvents.MarriageBookInfo)
end

function MarriageManager:OnQueryInvite(args)
    -- LoggerHelper.Log("Ash: OnQueryInvite: " .. PrintTable:TableToStr(args))
    local levelThresHold = 60 --不足110级也要看到按钮，但为优化新玩家性能，定另一个阀值控制 --GlobalParamsHelper.GetParamValue(744)-- 结婚婚礼玩家参与等级
    local player = GameWorld.Player()
    if player.level >= levelThresHold then
        --{all_book_data,hour} --hour为nil就是没有被邀请,被邀请者包含结婚者。--all_book_data结构{[hour]={BOOK_KEY_DBID1:dbid1,BOOK_KEY_DBID2:dbid2}}
        self._inviteInfo = args
        self:StopCheckWeddingState()
        if not table.isEmpty(args) then
            self:BeginCheckWeddingState()
        end
        EventDispatcher:TriggerEvent(GameEvents.MarriageQueryInvite)
    end
end

function MarriageManager:OnMarryReciveCeremony(args)
    -- LoggerHelper.Log("Ash: OnMarryReciveCeremony: " .. PrintTable:TableToStr(args))
    GameManager.DramaManager:Trigger("PLAY_CG", "5000009")
end

function MarriageManager:OnReceiveAsk(args)
    -- LoggerHelper.Log("Ash: OnReceiveAsk: " .. PrintTable:TableToStr(args))
    --{dbid,name}
    for i, v in ipairs(self._askInviteList) do
        if v[1] == args[1] then
            return
        end
    end
    table.insert(self._askInviteList, args)
    EventDispatcher:TriggerEvent(GameEvents.MarriageAskInviteChanged)
end

function MarriageManager:OnReciveSendRed(args)
    if args then
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = args[1]
        argsTable["1"] = LanguageDataHelper.CreateContent(53145)
        argsTable["2"] = args[2] .. ItemDataHelper.GetItemName(public_config.MONEY_TYPE_COUPONS)
        local showText = LanguageDataHelper.CreateContent(59051, argsTable)
        GUIManager.ShowText(2, showText, 5)
    end
end

function MarriageManager:HaveMate()
    return GameWorld.Player().mate_name ~= ""
end

function MarriageManager:GetMateName()
    return GameWorld.Player().mate_name
end

function MarriageManager:GetWeddingDress()
    local group = GameWorld.Player():GetVocationGroup()
    local vocationDress = GlobalParamsHelper.GetParamValue(817)
    return vocationDress[group]
end

function MarriageManager:GetWeddingDuration()
    return public_config.MARRY_STAGE_4_WAIT_TIME / 60
end

function MarriageManager:GetCanFood()
    return (self._marryFoodTime or 0) ~= GlobalParamsHelper.GetParamValue(768)
end

function MarriageManager:GetWeddingTime(hour)
    return hour .. ":00-" .. hour .. ":" .. self:GetWeddingDuration()
end

function MarriageManager:GetMateVocation()
    return self._mateInfo and self._mateInfo[4] or 0
end

function MarriageManager:GetMateFacade()
    return self._mateInfo and self._mateInfo[3] or ""
end

function MarriageManager:GetMateDbid()
    return self._mateInfo and self._mateInfo[1] or 0
end

function MarriageManager:GetMateTitle()
    return self._mateInfo and self._mateInfo[5] or 0
end

function MarriageManager:GetWeddingCount()
    return self._bookInfo and self._bookInfo[1] or 0
end

function MarriageManager:GetWeddingGrade()
    return self._bookInfo and self._bookInfo[2] or 0
end

function MarriageManager:GetMyBookingHour()
    return self._bookInfo and self._bookInfo[3] or 0
end

function MarriageManager:GetMyWeddingGuest()
    return self._bookInfo and self._bookInfo[4] and self._bookInfo[4][3] or {}
end

function MarriageManager:GetMyWeddingGuestCount()
    return #self:GetMyWeddingGuest()
end

function MarriageManager:GetMyGuestCount()
    return self._bookInfo and self._bookInfo[4] and self._bookInfo[4][4] or 0
-- local weddingGrade = MarriageManager:GetWeddingGrade()
-- local freeGuestCount = GlobalParamsHelper.GetParamValue(740)[weddingGrade]-- 结婚档次对应婚礼免费邀请宾客人数
-- return freeGuestCount + self:GetMyBuyCount()
end

function MarriageManager:GetAllBookingData()
    return self._inviteInfo and self._inviteInfo[1] or {}
end

function MarriageManager:GetMyWeddingHour()
    return self._inviteInfo and self._inviteInfo[2] or 0
end

function MarriageManager:GetWeddingHourData()
    local allBookingData = self:GetAllBookingData()
    local hour = self:GetMyWeddingHour()
    local weddingHourData = allBookingData[hour]
    if weddingHourData then
        return weddingHourData[public_config.BOOK_KEY_NAME1], weddingHourData[public_config.BOOK_KEY_NAME2],
            weddingHourData[public_config.BOOK_KEY_VOCATION1], weddingHourData[public_config.BOOK_KEY_VOCATION2],
            weddingHourData[public_config.BOOK_KEY_DBID1], weddingHourData[public_config.BOOK_KEY_DBID2]
    else
        return nil
    end
end

function MarriageManager:GetCurWeddingHourData()
    local now_date = DateTimeUtil.Now()
    local cur_hour = now_date.hour
    local min = now_date.min
    -- LoggerHelper.Log("Ash GetCurWeddingHourData: " .. cur_hour .. " " .. min)
    if min >= 55 then
        cur_hour = cur_hour + 1
    end
    -- LoggerHelper.Log("Ash GetCurWeddingHourData: " .. cur_hour .. " " .. min)
    local allBookingData = self:GetAllBookingData()
    -- LoggerHelper.Log("Ash GetCurWeddingHourData: allBookingData " .. PrintTable:TableToStr(allBookingData))
    local weddingHourData = allBookingData[cur_hour]
    if weddingHourData then
        return weddingHourData[public_config.BOOK_KEY_NAME1], weddingHourData[public_config.BOOK_KEY_NAME2],
            weddingHourData[public_config.BOOK_KEY_VOCATION1], weddingHourData[public_config.BOOK_KEY_VOCATION2],
            weddingHourData[public_config.BOOK_KEY_DBID1], weddingHourData[public_config.BOOK_KEY_DBID2], cur_hour
    else
        return nil
    end
end

function MarriageManager:BuyInviteCount()
    local topBuyCount = GlobalParamsHelper.GetParamValue(742)-- 结婚邀请宾客数上限
    if MarriageManager:GetMyGuestCount() >= topBuyCount then
        SystemInfoManager:ShowClientTip(error_code.ERR_MARRY_MAX_INVITE_COUNT_LIMIT)
        return
    end
    
    local price = GlobalParamsHelper.GetParamValue(741)-- 结婚额外邀请宾客花费（绑钻/人）
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = price
    local line1 = LanguageDataHelper.CreateContentWithArgs(59070, argsTable) .. "\n"
    local line2 = StringStyleUtil.GetFontSizeString(LanguageDataHelper.CreateContent(59071), -2)
    line2 = StringStyleUtil.GetColorStringWithTextMesh(line2, StringStyleUtil.green)
    GUIManager.ShowMessageBox(2, line1 .. line2,
        function()self:BugInviteCount() end, nil)
end

function MarriageManager:GetAskInviteList()
    return self._askInviteList
end

function MarriageManager:AgreeInviteGuest(data)
    self:MarryInvite(data[1])
    self:RemoveInviteGuest(data)
end

function MarriageManager:RemoveInviteGuest(data)
    local list = self._askInviteList
    local index = 0
    for i, v in ipairs(list) do
        if v[1] == data[1] then
            index = i
            break
        end
    end
    if index ~= 0 then
        table.remove(self._askInviteList, index)
        EventDispatcher:TriggerEvent(GameEvents.MarriageAskInviteChanged)
    end
end

function MarriageManager:AgreeAllInviteGuest()
    for i, v in ipairs(self._askInviteList) do
        self:MarryInvite(v[1])
    end
    self:RemoveAllInviteGuest()
end

function MarriageManager:RemoveAllInviteGuest()
    self._askInviteList = {}
    EventDispatcher:TriggerEvent(GameEvents.MarriageAskInviteChanged)
end

function MarriageManager:ShowGetMarry(npcId)
    GUIManager.ShowPanel(PanelsConfig.GetMarry)
end

MarriageManager:Init()
return MarriageManager
