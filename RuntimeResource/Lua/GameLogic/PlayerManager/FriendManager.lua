local FriendManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local error_code = GameWorld.error_code
local ChatConfig = GameConfig.ChatConfig
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ChatManager = PlayerManager.ChatManager
local RedPointManager = GameManager.RedPointManager
local FontStyleHelper = GameDataHelper.FontStyleHelper
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local TipsManager = GameManager.TipsManager
FriendManager.MAX_CHAT_NUM = 100 -- 聊天显示最大数量

function FriendManager:Init()

end

function FriendManager:OnPlayerEnterWorld()
    --self._friendRedpointNode = RedPointManager:CreateEmptyNode()
    self._friendRedpointNode = RedPointManager:GetRedPointDataByPanelName(PanelsConfig.Friend)
    FriendManager:GetData()
end

function FriendManager:RegisterNodeUpdateFunc(func)
    RedPointManager:RegisterNodeUpdateFunc(self._friendRedpointNode, func)
end

function FriendManager:HandleResp(act_id, code, args)
    if code == 0 then
		if act_id == action_config.FRIEND_RECOMMEND then
			self:GetRecommendListResq(args)
		elseif act_id == action_config.FRIEND_SEARDH then
			self:GetSearchReqByName(args)
        elseif act_id == action_config.FRIEND_APPLY_TO then
            self:GetApplyToFriendResq(args)
		elseif act_id == action_config.FRIEND_APPLY_LIST then
			self:GetApplyListResq(args)
		elseif act_id == action_config.FRIEND_ACCEPT then
			self:GetAgreeFriendResq(args)
		elseif act_id == action_config.FRIEND_REFUSE then
            self:GetRefuseFriendResq(args)
        elseif act_id == action_config.FRIEND_LIST then
            self:GetFriendListResq(args)
        elseif act_id == action_config.FRIEND_DELE then
            self:GetDeleteFriendResq(args)
        elseif act_id == action_config.FRIEND_BLACK then
            self:GetBlackFriendResq(args)
        elseif act_id == action_config.FRIEND_BLACK_LIST then
            self:GetBlackListResq(args)
        elseif act_id == action_config.FRIEND_BLACK_DELE then
            self:GetRemoveBlackResq(args)
        elseif act_id == action_config.FRIEND_NEW_CHAT_LIST then
            self:GetNewChatListResq(args)
        elseif act_id == action_config.FRIEND_FRIENDLY_RESP then   --通知好友度变化（单个好友）
            self:GetFriendlyResq(args)
        elseif act_id == action_config.FRIEND_FOE then   --添加仇敌
            self:GetAddFoeResq(args)
        elseif act_id == action_config.FRIEND_FOE_LIST then   --仇敌列表
            self:GetFoeListResq(args)
        elseif act_id == action_config.FRIEND_FOE_DELE then   --删除仇敌
            self:GetFoeDeleResq(args)
        elseif act_id == action_config.FRIEND_UNREAD_LIST then   --登陆未读信息
            self:GetUnReadListResq(args)   
        elseif act_id == action_config.FRIEND_ONLINE then
            self:GetOnlineTips(args)
            EventDispatcher:TriggerEvent(GameEvents.FriendCountChance)
        elseif act_id == action_config.FRIEND_OFFLINE then
            self:GetOffineTips(args)
            EventDispatcher:TriggerEvent(GameEvents.FriendCountChance)
        end
	end
end


function FriendManager:OnPlayerLeaveWorld()
    
end

--上线通知
function FriendManager:GetOnlineTips(args)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = args[2]
    GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(75679,argsTable))
    FriendData:SetIsOnlineByDbid(args[1],true)
    EventDispatcher:TriggerEvent(GameEvents.GetOnlineFiendResp,args[1])  
end

function FriendManager:GetOffineTips(args)
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = args[2]
    GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(75680,argsTable))
    FriendData:SetIsOnlineByDbid(args[1],false)
    EventDispatcher:TriggerEvent(GameEvents.GetOffineFiendResp,args[1])  
end

-- 处理表情
function FriendManager.HandleEmoji(textStr)
    for w in string.gmatch(textStr, "#(%d+)#") do
        local index = tonumber(w)
        local emojiText = ChatManager.EmojiText(index)
        textStr = string.gsub(textStr,'#'..index .. '#', emojiText )
    end
    return textStr
end

-- ==============================================点击文本链接================================================================
function FriendManager:OnClickTextLink(linkId)
    --LoggerHelper.Log(linkId)
    local bs = string.split(linkId, ",")
    --LoggerHelper.Log(linkId)
    if bs[1] == ChatConfig.LinkTypesMap.Team then --组队链接
        --self:JoinTeam(bs)
    elseif bs[1] == ChatConfig.LinkTypesMap.Item then --查看道具
        self:HandleItemTip(bs)
    elseif bs[1] == ChatConfig.LinkTypesMap.Coordinate then --寻路到目标坐标
        self:HandleCoordinate(bs)
    end
end

--查看道具
function FriendManager:HandleItemTip(bs)
    local itemIndex = tonumber(bs[2])
    local cacheIndex = tonumber(bs[3])
    local serDatas = FriendData.itemCache[cacheIndex]
    if serDatas then
        local itemSerData = serDatas[itemIndex]
        if itemSerData then
            local itemServerData = itemSerData[1]
            local bagType = itemSerData[2]
            local itemData = CommonItemData.GetOneItemData(itemServerData,bagType,0)
            local otherPlayerArgs
            if bagType == public_config.PKG_TYPE_LOADED_EQUIP then
                otherPlayerArgs = {}
                otherPlayerArgs.vipLevel     = itemSerData[3]
                otherPlayerArgs.strengthInfo = itemSerData[4]
                otherPlayerArgs.washInfo     = itemSerData[5]
                otherPlayerArgs.gemInfo      = itemSerData[6]
                otherPlayerArgs.suitData     = itemSerData[7]
                otherPlayerArgs.allEquipIds  = itemSerData[8]
            end
            TipsManager:ShowItemTips(itemData,nil,false,false,otherPlayerArgs)
        end
    end
end



--推荐列表
function FriendManager:HandleCoordinate(bs)
    local tarPos = Vector3.New(bs[3], bs[4], bs[5])
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, tonumber(bs[2]), tarPos, false, false)
end



-----------------------------------------Resp------------------------------------------------

--推荐列表
function FriendManager:GetRecommendListResq(args)
    --LoggerHelper.Log("data GetRecommendListResq.." .. PrintTable:TableToStr(args))
    FriendData:UpdateRecommondListData(args)
    EventDispatcher:TriggerEvent(GameEvents.RecommendDataList)
end

--玩家查找(根据名字)
function FriendManager:GetSearchReqByName(args)
    --LoggerHelper.Log("data GetSearchReqByName.." .. PrintTable:TableToStr(args))
    FriendData:SetSearchResultData(args)
    EventDispatcher:TriggerEvent(GameEvents.RefreshRecommendForSearch)
end

--申请加好友
function FriendManager:GetApplyToFriendResq(args)
    --LoggerHelper.Error("data GetApplyToFriendResq.." .. PrintTable:TableToStr(args))
    --self._friendRedpointNode:SetRedPoint(true)
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, true)
    if not table.isEmpty(args) then
        
        --self._friendRedpointNode:SetRedPoint(true)
        -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, true)
        EventDispatcher:TriggerEvent(GameEvents.GetApplyToFriendResq)
        --LoggerHelper.Error("申请加好友"..PrintTable:TableToStr(FriendData:GetFriendListData()))
        --LoggerHelper.Log("申请加好友"..PrintTable:TableToStr(#(FriendData:GetFriendListData())))
        EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
        
        --EventDispatcher:TriggerEvent(GameEvents.RefreshApplyList)
    end

    self:SendApplyListReq()

end

--申请列表（加我为好友）
function FriendManager:GetApplyListResq(args)
    --LoggerHelper.Log("data GetApplyListResq.." .. PrintTable:TableToStr(args))
    FriendData:UpdateApplyListData(args)
    self:UpdateRed()
    EventDispatcher:TriggerEvent(GameEvents.GetApplyListResq)
end

--同意加好友 发送dbid
function FriendManager:GetAgreeFriendResq(args)
    --LoggerHelper.Error("data ..GetAgreeFriendResq" .. PrintTable:TableToStr(args))
    FriendData:AddFriendForFrendList(args)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.FriendAddFriendList)
    EventDispatcher:TriggerEvent(GameEvents.RefreshApplyList)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFriendList)
    --SendApplyListReq()
end

--拒绝加好友 发送dbid
function FriendManager:GetRefuseFriendResq(args)
    --LoggerHelper.Log("data ..GetRefuseFriendResq" .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshApplyList)

end

--好友列表
function FriendManager:GetFriendListResq(args)
    --LoggerHelper.Error("data ..GetFriendListResq" .. PrintTable:TableToStr(args))
    FriendData:UpdateFriendListData(args)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFriendList)

end

--删除好友
function FriendManager:GetDeleteFriendResq(args)
    --LoggerHelper.Log("data ..GetDeleteFriendResq" .. args[1])
    FriendData:RemoveFriendForFrendList(args[1])
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFriendList)
    EventDispatcher:TriggerEvent(GameEvents.DeleFriendItemData)
end

--拉黑名单
function FriendManager:GetBlackFriendResq(args)
    --LoggerHelper.Log("data ..GetBlackFriendResq" .. PrintTable:TableToStr(args))
    FriendData:RemoveFriendForFrendListBySvr(args)
    FriendData:AddFriendForBlackList(args)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshBlackList)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFriendList)
    EventDispatcher:TriggerEvent(GameEvents.RefreshApplyList)

end

--黑名单列表
function FriendManager:GetBlackListResq(args)
    --LoggerHelper.Log("data ..GetBlackListResq" .. PrintTable:TableToStr(args))
    FriendData:UpdateBlackListData(args)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshBlackList)

end

--从黑名单移除
function FriendManager:GetRemoveBlackResq(args)
    --LoggerHelper.Log("data ..GetRemoveBlackResq" .. PrintTable:TableToStr(args))
    FriendData:RemoveFriendForBlackList(args[1])
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
    EventDispatcher:TriggerEvent(GameEvents.RefreshBlackList)

end

--获取好友私聊信息
function FriendManager:GetNewChatListResq(args)
    --LoggerHelper.Log("data ..GetNewChatListResq" .. PrintTable:TableToStr(args))
    FriendData:UpdateNewChatListData(args)
    EventDispatcher:TriggerEvent(GameEvents.FRIEND_PERSON_CHAT_INFO)
    self:UpdateRed()
end

--登陆获取未读信息
function FriendManager:GetUnReadListResq(args)
    --LoggerHelper.Error("data ..GetUnReadListResq" .. PrintTable:TableToStr(args))
    FriendData:SetUnReadOffineList(args)
    self:UpdateRed()
    --FriendData:UpdateNewChatListData(args)
    --EventDispatcher:TriggerEvent(GameEvents.FRIEND_PERSON_CHAT_INFO)
end

--设置好友
function FriendManager:UpdateRed()
    local recentList =  FriendData:GetFriendRecentListData()
    local applyNum = FriendData:GetTotalNumForApplyList()


    if applyNum > 0 then
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, true)
        local args = {}
        args[1] = EnumType.FriendPanelType.Apply
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, function()
            GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_FRIEND,args)
        end,EnumType.RemindType.FriendApply)
        --self._friendRedpointNode:SetRedPoint(true)
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.FriendApply)

        if recentList then
            if not table.isEmpty(recentList) then
                if FriendData:GetIsReadList(FriendData:GetFoeListData()) or FriendData:GetIsReadList(FriendData:GetFriendListData()) or applyNum>0 then
                    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, true)
                    --self._friendRedpointNode:SetRedPoint(true)
                    
                    local args = {}
                    args[1] = 1
                    EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, function()
                        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_FRIEND,args)
                    end,EnumType.RemindType.FriendNewChat)
                else
                    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, false)
                    EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.FriendNewChat)
                    --self._friendRedpointNode:SetRedPoint(false)
                end
            else
                EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, false)
                --self._friendRedpointNode:SetRedPoint(false)
                -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, false)
            end
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, false)
            --self._friendRedpointNode:SetRedPoint(false)
            -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, false)
        end
    end





    --FriendData:UpdateNewChatListData(args)
    --EventDispatcher:TriggerEvent(GameEvents.FRIEND_PERSON_CHAT_INFO)
end


--检测黑名单列表里面有没有这个还有
function FriendManager:GetDbidFromBlackList(dbid)
    FriendData:GetDbidFromBlackList(dbid)
end

--检测好友列表里面有没有这个还有
function FriendManager:GetDbidFromFriendList(dbid)
    FriendData:GetDbidFromFriendList(dbid)
end

--检测仇敌列表里面有没有这个还有
function FriendManager:GetDbidFromFoeList(dbid)
    FriendData:GetDbidFromFoeList(dbid)
end



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--下拉列表选择
--添加黑名单（menu）
function FriendManager:MenuAddToBlackList(dbid,playerName)
   self._blackDbid = dbid
   local argsTable = LanguageDataHelper.GetArgsTable()
   local friendItem = FriendData:GetFriendInfoByDbid(dbid)
   if friendItem then
        argsTable["0"] = friendItem:GetName() --local itemData = FriendData:GetFriendInfoByDbid(dbid)
   else
        argsTable["0"] = playerName
   end
  
   local str = LanguageDataHelper.CreateContent(75634, argsTable)
   GUIManager.ShowMessageBox(2, str,
   function()self:MenuBlackSure() end,
   function()GUIManager.ClosePanel(PanelsConfig.PlayerInfoMenu) end)
end


--添加黑名单（menu sure）
function FriendManager:MenuBlackSure()
    if self._blackDbid then
        FriendManager:AddBlackFriendReq(self._blackDbid)
        GUIManager.ClosePanel(PanelsConfig.PlayerInfoMenu)
        self._blackDbid = nil
    end

end

--删除好友（menu）
function FriendManager:MenuDeleteFriend(dbid)
    self._deteDbid = dbid
    local argsTable = LanguageDataHelper.GetArgsTable()
    local friendItem = FriendData:GetFriendInfoByDbid(dbid)
    argsTable["0"] = friendItem:GetName() --local itemData = FriendData:GetFriendInfoByDbid(dbid)
    local str = LanguageDataHelper.CreateContent(75633, argsTable)
    GUIManager.ShowMessageBox(2, str,
    function()self:MenuDeleteSure() end,
    function()GUIManager.ClosePanel(PanelsConfig.PlayerInfoMenu) end)

    
end

--删除好友（menu）
function FriendManager:MenuDeleteSure()
    if self._deteDbid then
        FriendManager:SendDeleteFriendReq(self._deteDbid)
        GUIManager.ClosePanel(PanelsConfig.PlayerInfoMenu)
    end

end

--开始聊天（menu）
function FriendManager:MenuStartChat(dbid,playerName)
    local dbid = tonumber(dbid)
    local isFriend = FriendData:GetDbidFromFriendList(dbid) ~= nil
    local isFoe = FriendData:GetDbidFromFoeList(dbid) ~= nil
    --LoggerHelper.Error("FriendManager:MenuStartChat"..dbid)
    if isFriend or isFoe then
        local args = {}
        args[1] = 1
        FriendData:SetUpdateTimeByDbid(dbid,GameUtil.DateTimeUtil.GetServerTime())
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_FRIEND,args)
    else
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(75724))
    end
end

--删除仇敌（menu）仇敌列表
function FriendManager:MenuDeleteFoe(dbid)
    FriendManager:SendFoeDeleReq(dbid)
end

--添加好友
function FriendManager:MenuAddFriend(dbid)
    FriendManager:SendAddFriendReq(dbid)
end

--移除黑名单（menu）
function FriendManager:MenuRemoveFromBlacklist(dbid)
    FriendManager:RemoveBlackFriendReq(dbid)
end

--删除仇敌
function FriendManager:MenuDeleteFoeFriend(dbid)
    FriendManager:SendFoeDeleReq(dbid)
end

--赠送鲜花
function FriendManager:MenuGiveFlower(dbid,playerName)

    if GameManager.GUIManager.PanelIsActive(PanelsConfig.Friend) then
        GUIManager.ClosePanel(PanelsConfig.Friend)
    end

    if GameManager.GUIManager.PanelIsActive(PanelsConfig.Chat) then
        GUIManager.ClosePanel(PanelsConfig.Chat)
    end

    local value = {
        ["dbid"] = dbid,
        ["name"] = playerName
    }

    local msgData = {_index = 1, _data = value}
    GUIManager.ShowPanel(PanelsConfig.Flower,msgData)
    
        --GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(77019))
    
end


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--获取好友亲密度返回
function FriendManager:GetFriendlyResq(args)  
    --LoggerHelper.Error("FriendManager:GetFriendlyResq:"..PrintTable:TableToStr(args))    
    FriendData:SetFriendlyByDbid(tonumber(args[1]),tonumber(args[2]))
    EventDispatcher:TriggerEvent(GameEvents.GetFriendlyResq,args[1],args[2])
end

--添加仇敌
function FriendManager:GetAddFoeResq(args)
    -- body
    FriendData:AddFoeForFoeList(args)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFoeList)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)
end

--仇敌列表
function FriendManager:GetFoeListResq(args)
    --LoggerHelper.Log("data ..GetFoeListResq" .. PrintTable:TableToStr(args))
    FriendData:UpdateFoeListData(args)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFoeList)   
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)     
end

--删除仇敌
function FriendManager:GetFoeDeleResq(args)
    -- body
    FriendData:RemoveFriendForFoeList(args)
    EventDispatcher:TriggerEvent(GameEvents.RefreshFoeList)
    EventDispatcher:TriggerEvent(GameEvents.UpdateFriendChange)  
end



--有聊天新消息提醒
function FriendManager:OnChatNewsResp(dbid,newsNum)
    --LoggerHelper.Error("data ..OnChatNewsResp" .. dbid..newsNum)

    
    FriendData:OnChatNewsResp(dbid,newsNum)
    --EventDispatcher:TriggerEvent(GameEvents.RefreshFoeList)  
    local curChatPerson = FriendData:GetCurChatPersonData() 
    if curChatPerson then
        if not table.isEmpty(curChatPerson) then
            --LoggerHelper.Log("data ..OnChatNewsResp3" .. curChatPerson:GetDbid())
            --LoggerHelper.Log("data ..OnChatNewsResp3" .. dbid)
            if tostring(curChatPerson:GetDbid()) == tostring(dbid) then
                self:SendNewChatListReq(dbid)   
                --LoggerHelper.Error("data ..OnChatNewsResp3" .. PrintTable:TableToStr(curChatPerson))
            end
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FRIEND, true)
    local args = {}
    args[1] = 1
    args[2] = dbid
    EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, function()
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_FRIEND,args)
    end,EnumType.RemindType.FriendNewChat)
    --self._friendRedpointNode:SetRedPoint(true)
end






-------------------------------------------Req------------------------------------------------

--推荐列表
function FriendManager:SendRecommendListReq()
    -- local listType = tostring(TYPE_RECOMMEND)
     GameWorld.Player().server.friend_action_req(action_config.FRIEND_RECOMMEND,"")
end

--玩家查找(根据名字)
function FriendManager:SendSearchReqByName(name)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_SEARDH, name)
end

--申请加好友
function FriendManager:SendAddFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_APPLY_TO,tostring(dbid))
end

--申请列表（加我为好友）
function FriendManager:SendApplyListReq()
    -- local listType = tostring(TYPE_APPLY)
     GameWorld.Player().server.friend_action_req(action_config.FRIEND_APPLY_LIST,"")
end

--同意加好友 发送dbid
function FriendManager:SendAgreeFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_ACCEPT,tostring(dbid))
end

--拒绝加好友 发送dbid
function FriendManager:SendRefuseFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_REFUSE,tostring(dbid))
end

--好友列表
function FriendManager:SendFriendListReq()
    -- local listType = tostring(TYPE_FRIEND)
     --LoggerHelper.Log("FriendManager:SendFriendListReq".."发送好友列表RPC")
     GameWorld.Player().server.friend_action_req(action_config.FRIEND_LIST,"")
    -- GameWorld.LoggerHelper.Log("发送好友列表RPC")
end

--删除好友
function FriendManager:SendDeleteFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_DELE,tostring(dbid))
end

--拉黑名单
function FriendManager:AddBlackFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_BLACK,tostring(dbid))
end

--黑名单列表
function FriendManager:SendBlackListReq()
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_BLACK_LIST,"")
end

--从黑名单移除
function FriendManager:RemoveBlackFriendReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_BLACK_DELE,tostring(dbid))
end

--获取好友私聊信息
function FriendManager:SendNewChatListReq(dbid)
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_NEW_CHAT_LIST,tostring(dbid))
end

--添加仇敌
function FriendManager:SendAddFoeReq(dbid)
    -- body
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_FOE,tostring(dbid))
end

--仇敌列表
function FriendManager:SendFoeListReq()
    -- body
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_FOE_LIST,"")
end

--删除仇敌
function FriendManager:SendFoeDeleReq(dbid)
    -- body
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_FOE_DELE,tostring(dbid))
end

--申请未知消息
function FriendManager:SendUnReadListReq()
    -- body
    GameWorld.Player().server.friend_action_req(action_config.FRIEND_UNREAD_LIST,"")
end





------------------------------------------------FriendManager方法-------------------------------
--获取申请面板最新数据
function FriendManager:GetApplyListData()
    return FriendData:GetApplyListData()
end

--获取好友列表最新数据
function FriendManager:GetFriendListData()
return FriendData:GetFriendListData()
end

--获取好友列表最新数据
function FriendManager:GetFriendRecentListData()
    return FriendData:GetFriendRecentListData()
end



--获取黑名单列表最新数据
function FriendManager:GetBlackListData()
    return FriendData:GetBlackListData()
end

--获取推荐列表最新数据
function FriendManager:GetRecommondListData()
    return FriendData:GetRecommondListData()
end

--获取仇敌列表
function FriendManager:GetFoeListData()
    return FriendData:GetFoeListData()
end


--获取好友列表在线人数
function FriendManager:GetOnlineNumForFriendList()
    return FriendData:GetOnlineNumForFriendList()
end

--获取黑名单列表在线人数
function FriendManager:GetOnlineNumForBlackList()
    return FriendData:GetOnlineNumForBlackList()
end

--获取仇敌列表在线人数
function FriendManager:GetOnlineNumForFoeList()
    return FriendData:GetOnlineNumForFoeList()
end

--获取好友列表人数
function FriendManager:GetTotalNumForFriendList()
    return FriendData:GetTotalNumForFriendList()
end

--获取黑名单列表人数
function FriendManager:GetTotalNumForBlackList()
    return FriendData:GetTotalNumForBlackList()
end

--获取仇敌列表人数
function FriendManager:GetTotalNumForFoeList()
    return FriendData:GetTotalNumForFoeList()
end


--获取好友列表最新数据
function FriendManager:SetDbidByChatList(listData)
    FriendData:SetDbidByChatList(listData)
end

--获取好友列表最新数据
function FriendManager:GetFriendListIndex(chatlist)
    FriendData:GetFriendListIndex(chatlist)
end

--获取个人聊天最新数据
function FriendManager:GetChatListData()
    return FriendData:GetChatListData()
end

--获取好友信息
function FriendManager:GetFriendInfoByDbid(dbid)
    return FriendData:GetFriendInfoByDbid(dbid)
end

--删除申请面板一条数据
function FriendManager:DeleApplyListData(dbid)
    return FriendData:DeleApplyListData(dbid)
end

--面板打开发送
function FriendManager:GetData()
    FriendManager:SendFriendListReq()
    FriendManager:SendBlackListReq()
    FriendManager:SendFoeListReq()
    FriendManager:SendUnReadListReq()
    FriendManager:SendApplyListReq()
end

--面板关闭请求
function FriendManager:GetDataClose()
    FriendManager:SendFriendListReq()
    FriendManager:SendBlackListReq()
    FriendManager:SendFoeListReq()
    FriendManager:SendApplyListReq()
end

--设置自己聊天信息
function FriendManager:SetMyChatListData(data,content)
    FriendData:SetMyChatListData(data,content)
    EventDispatcher:TriggerEvent(GameEvents.FRIEND_PERSON_CHAT_INFO)
end

--系统提示信息
function FriendManager:SetTipChatListData(data,content)
    FriendData:SetTipChatListData(data,content)
    EventDispatcher:TriggerEvent(GameEvents.FRIEND_PERSON_CHAT_INFO)
end

--得到指定dbid的聊天数据
function FriendManager:GetChatListDataByDbid(dbid)
    FriendData:GetChatListDataByDbid(dbid)
end

--设置指定dbid的聊天数据
function FriendManager:SetChatListData(listData)
    FriendData:SetChatListData(listData)
end


--发消息给别人
function FriendManager:SendChatReq(data,chatValue,value,lisstr)
    if lisstr then
        
    end
    --LoggerHelper.Error("lisstrSend"..lisstr)
    local str = public_config.CHAT_CHANNEL_PRIVATE..","..chatValue..","..tostring(data:GetDbid())..","..lisstr
    local content = GameWorld.FilterWords(chatValue)
    content = ChatManager:CheckContentLink(chatValue)
    content = ChatManager.HandleEmoji(chatValue)
    content = FriendData:HandleItem(chatValue,lisstr)
    --LoggerHelper.Error("FriendManager:SendChatReq"..lisstr)  
    --GameWorld.Player().server.chat_action_req(action_config.CHAT_REQ, str)

    GameWorld.Player().server.chat_req(public_config.CHAT_CHANNEL_PRIVATE, tostring(data:GetDbid()), chatValue, lisstr)
    self:SetMyChatListData(data,content)

    ChatManager.OnCheckChatMesseage(chatValue,public_config.CHAT_CHANNEL_PRIVATE) 
end

-- -- 处理物品
-- function FriendManager:HandleItem(textStr,liststr)
--     if liststr ~= "" then
--         local itemSerData = PrintTable:StringToTable(tostring(liststr))
--         local cacheIndex = self.itemCacheIndex
--         self.itemCache[cacheIndex] = itemSerData
--         for w in string.gmatch(textStr, "&&(%d+)&&") do
--             local index = tonumber(w)
--             local itemText = self:ItemText(index,cacheIndex,itemSerData)

--             textStr = string.gsub(textStr, '&&' .. index .. '&&', itemText)
--         end
--         self.itemCacheIndex = cacheIndex+1
--     end
--     return textStr
-- end

-- --匹配物品数据
-- function FriendManager:ItemText(index,cacheIndex,itemSerData)
--     if itemSerData == nil then
--         return '&&' .. index .. '&&'
--     end
    
--     local sendText = ''
--     if itemSerData[index] then
--         local arg = {}
--         local itemId = itemSerData[index][1][public_config.ITEM_KEY_ID]
--         local quality = ItemDataHelper.GetQuality(itemId)
--         local colorId = ItemConfig.QualityTextMap[quality]
--         local color = FontStyleHelper.GetFontStyleData(colorId).color
--         arg["0"] =  ChatConfig.LinkTypesMap.Item..","..index..","..cacheIndex
--         arg["1"] =  color
--         arg["2"] = '['..ItemDataHelper.GetItemName(tonumber(itemId))..']'    
--         sendText = LanguageDataHelper.CreateContent(80532,arg)
--     end
   
--     return sendText
-- end





FriendManager:Init()
return FriendManager