local InstanceManager = {}
require "PlayerManager/PlayerData/EventData"
local TaskRewardData = ClassTypes.TaskRewardData
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PlayerDataManager = PlayerManager.PlayerDataManager
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local instanceData = PlayerDataManager.instanceData
local teamData = PlayerDataManager.teamData
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local bagData = PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local RedPointManager = GameManager.RedPointManager
local Vector3 = Vector3

function InstanceManager:Init()
    self._sweepData = PlayerDataManager.sweepData
    self:InitCallbackFunc()
end

function InstanceManager:InitRedpointNodes()
    self._expNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_EXP_INSTANCE)
end

function InstanceManager:InitCallbackFunc()
    self._onInstanceExit = function()self:OnInstanceExit() end
    self._onInstanceEnter = function(instGroupId, teleportId)self:OnInstanceEnter(instGroupId, teleportId) end
    self._onClinetInstanceEnd = function()self:RequestInstanceClientSettle() end
    -- self._onFlyToScene = function(targetScene)self:OnFlyToScene(targetScene) end
    self._onBagDataUpdate = function(bagType)self:OnBagDataUpdate(bagType) end
end

function InstanceManager:OnPlayerEnterWorld()
    self:InitRedpointNodes()
    self:UpdateRedPoint()
    self._littleShoesId = GlobalParamsHelper.GetParamValue(481)-- little_shoes_i
    self._littleShoesIcon = ItemDataHelper.GetIcon(self._littleShoesId)
    self:UpdateLittleShoesState()
    EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_EXIT, self._onInstanceExit)
    EventDispatcher:AddEventListener(GameEvents.ON_INSTANCE_ENTER, self._onInstanceEnter)
    EventDispatcher:AddEventListener(GameEvents.INSTANCE_END, self._onClinetInstanceEnd)
    -- EventDispatcher:AddEventListener(GameEvents.FLY_TO_SCENE, self._onFlyToScene)
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE, self._onBagDataUpdate)
end

function InstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_EXIT, self._onInstanceExit)
    EventDispatcher:RemoveEventListener(GameEvents.ON_INSTANCE_ENTER, self._onInstanceEnter)
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_END, self._onClinetInstanceEnd)
    -- EventDispatcher:RemoveEventListener(GameEvents.FLY_TO_SCENE, self._onFlyToScene)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._onBagDataUpdate)
end

--重新登录进入副本，重新上线进入副本
--守护结界塔、众神宝库
--        [1] = public_config.MAP_TYPE_HOLY_TOWER,      --场景类型
-- [2] = inst_id,                                --关卡id
function InstanceManager:OnReEnterInstance(data)
    local sceneType = data[1]
    local instanceId = data[2]
    if sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE then --守护结界塔
        PlayerManager.PetInstanceManager:SetInstanceId(instanceId)
    elseif sceneType == SceneConfig.SCENE_TYPE_GOLD then --众神宝库
        PlayerManager.MoneyInstanceManager:SetInstanceId(instanceId)
    elseif sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE then --经验副本
        PlayerManager.ExpInstanceManager:OnExpInstanceEnter(0, 0)
    end
end

function InstanceManager:GoToSceneAny(mapId, x, y, z)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD then
        EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, mapId, Vector3.New(x, y, z), false, false)
    elseif sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS then
        PlayerManager.WorldBossManager:GotoBossByMapId(mapId, Vector3.New(x, y, z))
    elseif sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then
        PlayerManager.BossHomeManager:GotoBossByMapId(mapId, Vector3.New(x, y, z))
    elseif sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
        PlayerManager.BackWoodsManager:GotoBossByMapId(mapId, Vector3.New(x, y, z))
    elseif sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        PlayerManager.GodIslandManager:GotoBossByMapId(mapId, Vector3.New(x, y, z))
    end
end

-- function InstanceManager:OnFlyToScene(targetScene)
--     GameWorld.FlyToScene(targetScene)
-- end

function InstanceManager:OnBagDataUpdate(bagType)
    -- LoggerHelper.Log("Ash: OnBagDataUpdate:" .. tostring(bagType))
    if bagType == public_config.PKG_TYPE_ITEM then
        self:UpdateLittleShoesState()
    end
end

function InstanceManager:UpdateLittleShoesState()
    local hasLittleShoes = bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(self._littleShoesId)
    -- LoggerHelper.Log("Ash: hasLittleShoes:" .. tostring(hasLittleShoes))
    if hasLittleShoes ~= self._hasLittleShoes then
        -- LoggerHelper.Log("Ash: UpdateLittleShoesState:" .. tostring(hasLittleShoes))
        self._hasLittleShoes = hasLittleShoes
        EventDispatcher:TriggerEvent(GameEvents.OnHasLittleShoesChanged)
    end
end

function InstanceManager:GetHasLittleShoes()
    return self._hasLittleShoes
end

function InstanceManager:GetLittleShoesIcon()
    return self._littleShoesIcon
end

function InstanceManager:OnInstanceExit(mapId, x, y, z)
    -- LoggerHelper.Log("Ash: OnInstanceExit" .. tostring(mapId))
    if x == nil and y == nil and z == nil then
        GameWorld.Player().server.instance_action_req(action_config.INSTANCE_EXIT, mapId and tostring(mapId) or "")
    else
        GameWorld.Player().server.instance_action_req(action_config.INSTANCE_EXIT, self:GetMapPosString(mapId, x, y, z))
    end
    GameWorld.PauseAllSynPos(true)
end

--进副本处理
--instId关卡id
--teleportId传送点id
function InstanceManager:OnInstanceEnter(instId, teleportId)
    --LoggerHelper.Error("Ash: OnInstanceEnter: " .. instId)
    --local instId = InstanceGroupDataHelper:GetInstanceId(instGroupId)
    GameWorld.Player().server.instance_action_req(action_config.INSTANCE_ENTER, tostring(instId) .. "," .. tostring(teleportId or 0))
    GameWorld.PauseAllSynPos(true)
--LoggerHelper.Error("进入副本")
end

function InstanceManager:CommonChangeMap(tarMapId, x, y, z)
    local curSceneType = GameSceneManager:GetCurSceneType()
    -- LoggerHelper.Log("Ash: CommonChangeMap: " .. tostring(tarMapId) .. " curSceneType: " .. tostring(curSceneType))
    if curSceneType == SceneConfig.SCENE_TYPE_MAIN or curSceneType == SceneConfig.SCENE_TYPE_WILD then
        local curMapId = GameSceneManager:GetCurrMapID()
        local curMaxCount = MapDataHelper.GetMaxCount(curMapId)
        if curMaxCount == 1 then
            local curMapChinese = MapDataHelper.GetChinese(curMapId)
            local tarMapChinese = MapDataHelper.GetChinese(tarMapId)
            if curMapChinese == tarMapChinese then --目前依赖场景名字区分是否同一个场景外观做野外镜像本，如场景名字不能满足需求，再加字段
                local pos = GameWorld.Player():GetPosition()
                self:OnInstanceMapEnter(tarMapId, pos.x, pos.y, pos.z)
            else
                self:OnInstanceMapEnter(tarMapId, x, y, z)
            end
        else
            self:OnInstanceMapEnter(tarMapId, x, y, z)
        end
    else
        -- self:OnInstanceExit(tarMapId, x, y, z)
        --在副本中不能传送
        -- LoggerHelper.Error('不能传送')
        GUIManager.ShowText(1, LanguageDataHelper.CreateContent(58855))
        return false
    end
    return true
end

--客户端直接进入地图，需要校验是主城或者野外等条件
function InstanceManager:OnInstanceMapEnter(mapId, x, y, z)
    if x == nil and y == nil and z == nil then
        -- LoggerHelper.Log("Ash: OnInstanceMapEnter1 " .. tostring(mapId))
        GameWorld.Player().server.instance_action_req(action_config.CLIENT_MAP_ENTER, tostring(mapId))
    else
        -- LoggerHelper.Log("Ash: OnInstanceMapEnter2 " .. self:GetMapPosString(mapId, x, y, z))
        GameWorld.Player().server.instance_action_req(action_config.CLIENT_MAP_ENTER, self:GetMapPosString(mapId, x, y, z))
    end
    GameWorld.PauseAllSynPos(true)
end

function InstanceManager:GetMapPosString(mapId, x, y, z)
    return tostring(mapId) .. "," .. tostring(Mathf.Floor((x or 0) * 100)) .. "," .. tostring(Mathf.Floor((y or 0) * 100)) .. "," .. tostring(Mathf.Floor((z or 0) * 100))
end

-- --进副本镜像处理
-- function InstanceManager:OnImageMapEnter(mapId)
--     GameWorld.Player().server.instance_action_req(action_config.IMAGE_MAP_ENTER, tostring(mapId))
-- end
function InstanceManager:UseLittleShoe(mapId, isUseLocation, x, y, z)
    -- local para = tostring(mapId) .. "," .. (x or 0) .. "," .. (y or 0) .. "," .. (z or 0) .. "," .. (isUseLocation and 1 or 0)
    GameWorld.Player().server.use_little_shoe(tostring(mapId), Mathf.Floor((x or 0) * 100), Mathf.Floor((y or 0) * 100), Mathf.Floor((z or 0) * 100), (isUseLocation and 1 or 0))
end

--副本每日次数 类型 次数改变
function InstanceManager:OnInstInfoChange(change)
    EventDispatcher:TriggerEvent(GameEvents.INSTANCE_INFO_UPDATE)
end

function InstanceManager:HandleData(action_id, error_id, args)
    -- LoggerHelper.Log("Ash: HandleData:" .. action_id .. " " .. error_id)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("Instance Error:" .. error_id)
        return
    elseif action_id == action_config.INSTANCE_SETTLE then --693 普通副本结算
        -- LoggerHelper.Log("INSTANCE_SETTLE ====" .. PrintTable:TableToStr(args))
        -- self:ProcessServerSettle(args)
        EventDispatcher:TriggerEvent(GameEvents.INSTANCE_NOTIFY_END, args)
    elseif action_id == action_config.INSTANCE_NIGHTMARE then
        --更新噩梦副本信息
        --self:UpdateNightmare(args)
    elseif action_id == action_config.INSTANCE_NOTIFY_ROOM_STAGE then --696 --通知副本stage和开始时间。{1:stage,2:start_time}。光门倒计时的开始和关闭。玩法开始的时间。
        --LoggerHelper.Log("696 ====" ..PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, args)
    elseif action_id == action_config.INSTANCE_NOTIFY_KILL_MONSTERS then --697 通知副本当前已经杀的怪物。{monster_id:num}
        --LoggerHelper.Log("697 ====" ..PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.INSTANCE_NOTIFY_KILL_MONSTERS, args)
    else
        LoggerHelper.Error("Instance action id not handle:" .. action_id)
    end
end

function InstanceManager:HandleResp(act_id, code, args)
    -- LoggerHelper.Log("Ash: HandleResp:" .. act_id .. " " .. code)
    if act_id == action_config.INSTANCE_EXIT then
        --副本退出
        -- LoggerHelper.Log("Ash: Instance exit:" .. PrintTable:TableToStr(args))
        self:ProcessInstExit(args)
        if code == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_EXIT_END)
        else
            EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_EXIT_FAIL)
            GameWorld.PauseAllSynPos(false)
        end
    
    elseif act_id == action_config.CLIENT_MAP_ENTER then
        --进入场景
        -- LoggerHelper.Log("Ash: CLIENT_MAP_ENTER:" .. code)
        if code == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ON_CLIENT_MAP_CHANGE_END)
        else
            EventDispatcher:TriggerEvent(GameEvents.ON_CLIENT_MAP_CHANGE_FAIL)
            GameWorld.PauseAllSynPos(false)
        end
    elseif act_id == action_config.INSTANCE_ENTER then
        --进入副本
        -- LoggerHelper.Log("Ash: INSTANCE_ENTER:" .. code)
        if code == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_ENTER_END)
        else
            EventDispatcher:TriggerEvent(GameEvents.ON_INSTANCE_ENTER_FAIL)
            GameWorld.PauseAllSynPos(false)
        end
    elseif act_id == action_config.ITEM_USE_LITTLE_SHOE then
        --使用小飞鞋
        -- LoggerHelper.Log("Ash: ITEM_USE_LITTLE_SHOE:" .. code)
        if code == 0 then
            EventDispatcher:TriggerEvent(GameEvents.ON_USE_LITTLE_SHOE_END)
            self._hasUseLittleShoeSuccess = true
        else
            EventDispatcher:TriggerEvent(GameEvents.ON_USE_LITTLE_SHOE_FAIL)
        end
    end
end

function InstanceManager:HasUseLittleShoeSuccess()
    if self._hasUseLittleShoeSuccess then
        self._hasUseLittleShoeSuccess = false
        return true
    end
    return false
end

function InstanceManager:ProcessServerSettle(args)
    -- args[public_config.INSTANCE_SETTLE_KEY_MAP_ID] = map_id
    local mapId = args[public_config.INSTANCE_SETTLE_KEY_MAP_ID]
    local sceneType = MapDataHelper.GetSceneType(mapId)
    -- LoggerHelper.Log("Ash: ProcessServerSettle: " .. sceneType)
    if sceneType ~= SceneConfig.SCENE_TYPE_COMBAT then
        GUIManager.ShowPanel(PanelsConfig.InstBalance, {
            MapId = mapId,
            IsSuccess = args[public_config.INSTANCE_SETTLE_KEY_RESULT] == 1,
            Rewards = args[public_config.INSTANCE_SETTLE_KEY_ITEMS],
            Callback = function()self:OnInstanceExit() end
        }, nil)
    end
    EventDispatcher:TriggerEvent(GameEvents.OnServerSettle)
end

function InstanceManager:ProcessInstExit(args)
    GUIManager.ClosePanel(PanelsConfig.InstBalance)
end

--客户端副本结算
function InstanceManager:RequestInstanceClientSettle()
-- GameWorld.Player().server.instance_action_req(action_config.INSTANCE_CLIENT_SETTLE, "") -- 类型已经不存在
end

--694  通知服务端已经进入副本
function InstanceManager:EnterInstance()
    --LoggerHelper.Error("InstanceManager:EnterInstance()")
    GameWorld.Player().server.instance_action_req(action_config.CLIENT_LOADING_COMPLETED, "100")--客户端加载完
end

--客户端副本杀怪拾取
-- function InstanceManager:RequestClientKillMonster(dropId,itemId)
--     local str = dropId..","..itemId
--     GameWorld.Player().server.instance_action_req(action_config.CLIENT_KILL_MONSTER,str)
-- end
--噩梦副本更新
-- function InstanceManager:UpdateNightmare(args)
--     local instId = args[1]
--     instanceData:UpdateNightmare(instId)
--     EventDispatcher:TriggerEvent(GameEvents.INSTANCE_NIGHTMARE_UPDATE)
-- end
------------------------Raid副本相关---------------------------------
--进入场景后判断是否Raid副本
-- function InstanceManager:CheckRaidInstance(map_id)
--     local sceneType = MapDataHelper.GetSceneType(map_id)
--     if sceneType == SceneConfig.SCENE_TYPE_RAID_BATTLE then
--         GUIManager.ShowPanel(PanelsConfig.InstanceProgress, nil, nil)
--     end
-- end
-- function InstanceManager:OnRaidInstanceChange(change)
-- --
-- end
-- function InstanceManager:OnClearRaidInstData()
-- end
-- function InstanceManager:GetRewardsData(rewards)
--     local result = {}
--     for k, v in pairs(rewards) do
--         table.insert(result, TaskRewardData(k, v))
--     end
--     return result
-- end
---------------------------------------------------------------------------------
--打开扫荡面板 类型1
function InstanceManager:OnEnterSweep(data)
    if self:CheckSweep(data) then
        GUIManager.ShowPanel(PanelsConfig.Sweep, data)
    end
end

function InstanceManager:CheckSweep(data)
    if data:GetSweepLevel() > GameWorld.Player().level then
        --等级不足
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = data:GetSweepLevel()
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(76328, argsTable))
        return false
    end
    
    if data:GetStart() == nil then
        --未通过副本
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76327))
        return false
    end
    
    if data:GetSweepNum() == 0 then
        --扫荡次数不足
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76329))
        return false
    end
    return true
end

--打开扫荡面板 类型2
function InstanceManager:OnEnterSweepCost(itemId, itemNum, CostNum, Func)
    local data = {}
    data.seeptype = 2
    data.itemId = itemId
    data.itemNum = itemNum
    data.CostNum = CostNum
    -- data.rewardData = rewardData
    -- data.buttonArgs = buttonArgs
    data.Function = Func
    GUIManager.ShowPanel(PanelsConfig.Sweep, data)
end

function InstanceManager:ResetSweepData(data)
    self._sweepData:ResetData(data)
    return self._sweepData
end

function InstanceManager:GetSweepData()
    return self._sweepData
end

function InstanceManager:SweepAgain()
    if self._sweepData == nil then return end
    self._sweepData:Again()
end

function InstanceManager:IsCanAgain()
    if self._sweepData == nil then return false end
    return self._sweepData:IsCanAgain()
end

function InstanceManager:OnUpdateInfo()
    self:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.EXP_INSTANCE_INFO_CHANGE)
end

function InstanceManager:UpdateRedPoint()
    if self._expNode and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_EXP_INSTANCE) then
        self._expNode:SetRedPoint(PlayerManager.ExpInstanceManager:GetExpInstanceCanEnter())
    end
end

InstanceManager:Init()
return InstanceManager
