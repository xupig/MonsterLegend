local TitleManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FashionDataHelper = GameDataHelper.FashionDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local titleData = PlayerManager.PlayerDataManager.titleData
local EntityType = GameConfig.EnumType.EntityType

function TitleManager:Init()

end

function TitleManager:OnPlayerEnterWorld()
    
end

function TitleManager:OnPlayerLeaveWorld()
    
end

function TitleManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("TitleManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if action_id == action_config.TITLE_TASK_LIST_REQ then
        self:RefreshTitleTaskList(args)
    elseif action_id == action_config.TITLE_TASK_REFRESH then
        self:RefreshTitleTaskOne(args)
    end
end

-----------------------------数据处理---------------------------
function TitleManager:RefreshTitleData(changeValue)
    titleData:RefreshTitleData(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Role_Title_Info)
end

function TitleManager:RefreshTitleTaskList(data)
    titleData:RefreshTitleTaskList(data)
end

function TitleManager:RefreshTitleTaskOne(data)
    titleData:RefreshTitleTaskList(data)
    --不频繁刷新界面
    --EventDispatcher:TriggerEvent(GameEvents.Refresh_Role_Title_Info)
end

function TitleManager:RefreshTitleId(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Role_Title_Info)
end

-------------------------------------功能接口-----------------------------------
function TitleManager:GetStateByTitleId(id)
    return titleData:GetStateByTitleId(id)
end

function TitleManager:GetStartTimestamp(id)
    return titleData:GetStartTimestamp(id)
end

-------------------------------------屏蔽他人称号接口-----------------------------------
function TitleManager:RefreshHideOtherTitle()
    for k,v in pairs(GameWorld.Entities()) do 
        if v.entityType == EntityType.Avatar then
            EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, k)
        end
    end
end

-------------------------------------客户端请求-----------------------------------
--穿戴
function TitleManager:TitleSetTitleReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.title_action_req(action_config.TITLE_SET_TITLE_ID, params)
end

--取消穿戴
function TitleManager:TitleUnsetTitleReq()
    GameWorld.Player().server.title_action_req(action_config.TITLE_UNSET_TITLE_ID, "")
end


TitleManager:Init()
return TitleManager