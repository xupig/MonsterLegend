local ZeroDollarBuyManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local ZeroDollarBuyDataHelper = GameDataHelper.ZeroDollarBuyDataHelper

local Max_Open_Server_Day = 7
local Min_Cfg_Id = 1
local Max_Cfg_Id = 4
local Update_Interval = 5

function ZeroDollarBuyManager:Init()
    self._on_0_Oclock = function()self:OnPassDay() end
    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
    self._onLevelChangeCB = function()self:RefreshRewardRedPoint() end
    self._countDownFunc = function() self:RefreshRewardRedPoint() end
end

function ZeroDollarBuyManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)

    self._showZeroDollarBuyRedPoint = false

    self:RefreshShowState()
end

function ZeroDollarBuyManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)

    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
end

function ZeroDollarBuyManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("ZeroDollarBuyManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.ZERO_PRICE_BUY then
        
    end
end

-------------------------------------数据处理------------------------------------
function ZeroDollarBuyManager:RefreshServerData(changeValue)
    if changeValue then
        self:RefreshShowState()
        EventDispatcher:TriggerEvent(GameEvents.REFRESH_ZERO_PRICE_INFO)
    end
end

-----------------------------------红点------------------------------
function ZeroDollarBuyManager:RefreshRewardRedPoint()
    if not self:CanShow() then
        self:SeRewardRedPoint(false)
    else
        local serverData = GameWorld.Player().zero_price_info
        for id=Min_Cfg_Id, Max_Cfg_Id do
            local data = serverData[id]
            local _, price = next(ZeroDollarBuyDataHelper:GetPrice(id))
            if price == 0 then
                if GameWorld.Player().level >= ZeroDollarBuyDataHelper:GetLevel(id) and data == nil then
                    self:SeRewardRedPoint(true)
                    return
                end
                if data ~= nil and data[2] ~= 1 and DateTimeUtil.GetServerTime() >= (serverData[id][1] + ZeroDollarBuyDataHelper:GetTime(id)) then
                    self:SeRewardRedPoint(true)
                    return
                end
            else
                if data ~= nil and data[2] ~= 1 and DateTimeUtil.GetServerTime() >= (serverData[id][1] + ZeroDollarBuyDataHelper:GetTime(id)) then
                    self:SeRewardRedPoint(true)
                    return
                end
            end
        end
    end
    self:SeRewardRedPoint(false)
end

function ZeroDollarBuyManager:SeRewardRedPoint(state)
    if self._showZeroDollarBuyRedPoint ~= state then
        self._showZeroDollarBuyRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_ZERO_DOLLAR_BUY, self:GetRewardRedPoint())
    end
end

function ZeroDollarBuyManager:GetRewardRedPoint()
    return self._showZeroDollarBuyRedPoint
end

-------------------------------------功能接口------------------------------------
function ZeroDollarBuyManager:RefreshShowState()
    if self:CanShow() then
        self:OpenActivity()
        EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, GameConfig.EnumType.OpenActivityType.BuyZero, true)
    else
        self:CloseActivity()
        EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, GameConfig.EnumType.OpenActivityType.BuyZero, false)
    end
    self:RefreshRewardRedPoint()
end

function ZeroDollarBuyManager:CanShow()
    local isFuncOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ZERO_DOLLAR_BUY)
    if not isFuncOpen then
        return false
    end
    local serverData = GameWorld.Player().zero_price_info
    local now = DateTimeUtil.GetServerTime()
    for i=Min_Cfg_Id, Max_Cfg_Id do
        local data = serverData[i]
        if data ~= nil and data[2] ~= 1 then
            return true
        end
    end

    local openDays = DateTimeUtil.GetServerOpenDays()
    if openDays > Max_Open_Server_Day then
        return false
    end
    return true
end

function ZeroDollarBuyManager:OpenActivity()
    if self:CanShow() then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer=nil
        end
        self._timer = TimerHeap:AddSecTimer(0, Update_Interval, 0, self._countDownFunc)
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.ZeroDollarBuy, function()GUIManager.ShowPanel(PanelsConfig.ZeroDollarBuy) end)
    end
end

function ZeroDollarBuyManager:CloseActivity()
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer=nil
    end
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.ZeroDollarBuy)
end

function ZeroDollarBuyManager:OnPassDay()
    self:RefreshShowState()
end

function ZeroDollarBuyManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_ZERO_DOLLAR_BUY then
        self:RefreshShowState()
    end
end

local _openDaysEndTime
function ZeroDollarBuyManager:GetOpenDaysEndTime()
    if _openDaysEndTime == nil then
        local endDate = DateTimeUtil.SomeDay(DateTimeUtil.GetOpenServerTime() + (Max_Open_Server_Day) * 86400)
        _openDaysEndTime = DateTimeUtil.NewDate(endDate.year, endDate.month, endDate.day)
    end
    return _openDaysEndTime
end

-------------------------------------客户端请求----------------------------------
function ZeroDollarBuyManager:ZeroDollarBuyReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.zero_price_action_req(action_config.ZERO_PRICE_BUY, params)
end

function ZeroDollarBuyManager:ZeroDollarRebateReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.zero_price_action_req(action_config.ZERO_PRICE_REBATE, params)
end

ZeroDollarBuyManager:Init()
return ZeroDollarBuyManager