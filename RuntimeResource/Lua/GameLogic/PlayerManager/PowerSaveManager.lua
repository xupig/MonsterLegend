--PowerSaveManager.lua
local PowerSaveManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local Time = Time
local PauseFpsType = GameConfig.EnumType.PauseFpsType
local ControlStickState = GameConfig.ControlStickState
local SystemSettingManager = PlayerManager.SystemSettingManager

function PowerSaveManager:Init()
end

function PowerSaveManager:OnPlayerEnterWorld()
    self._onTouchScreen = function() self:OnTouchScreen() end
    EventDispatcher:AddEventListener(GameEvents.ENGINE_ONTOUCHSCREEN, self._onTouchScreen)
end

function PowerSaveManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENGINE_ONTOUCHSCREEN, self._onTouchScreen)
    self:SetCanPowerSave(false)
end

function PowerSaveManager:OnTouchScreen()
    self:SetLastClickTime()
end

--=================== 省电模式相关 begin =======
local powerSaveFrameRate = 12 -- 降帧帧率
local powerSaveCheckTime = 30 -- 检查时间s
local lastClickCheckTime = 120 -- 没操作时间s

function PowerSaveManager:InitPowerSave()
    self._isInPowerSaving = false
    self._lastTargetFrameRate = 0
    self._curQualityLevel = SystemSettingConfig.QUALITY_LOW
    self._lastClickTime = Time.realtimeSinceStartup
end

function PowerSaveManager:IsInPowerSaving()
    return self._isInPowerSaving
end

function PowerSaveManager:AddOperationTimer()
    self:RemoveOperationTimer()
    self._operationTimerId = TimerHeap:AddSecTimer(powerSaveCheckTime, powerSaveCheckTime, 0, function() self:OnCheckOperation() end)
end

function PowerSaveManager:RemoveOperationTimer()
    if self._operationTimerId then
        TimerHeap:DelTimer(self._operationTimerId)
        self._operationTimerId = nil
    end
end

function PowerSaveManager:SetCanPowerSave(state)
    self._canPowerSave = state
    if state then
        self:InitPowerSave()
        self:AddOperationTimer()
    else
        if self._isInPowerSaving then 
            self:StopPowerSave()
        end
        self:RemoveOperationTimer()
    end
end

function PowerSaveManager:SetLastClickTime()
    if not self._canPowerSave then return end
    self._lastClickTime = Time.realtimeSinceStartup
    -- LoggerHelper.Error("===SetLastClickTime() ... " .. self._lastClickTime)
    if self._isInPowerSaving then
        self:StopPowerSave()
    end
end

function PowerSaveManager:OnCheckOperation()
    if not self._canPowerSave then return end
    if self._isInPowerSaving then return end
    if ControlStickState.controlStickIsDragging then return end -- 摇杆拖动操作
    local curTime = Time.realtimeSinceStartup
    if (curTime - self._lastClickTime > lastClickCheckTime) then
        self:StartPowerSave()
    end
end

function PowerSaveManager:StartPowerSave()
    if self._isInPowerSaving == true then
        return
    end
    self._isInPowerSaving = true

    self:SetPowerSaveMode(1) -- 打开省电模式界面、屏蔽设置等
    GUIManager.ShowPanel(PanelsConfig.PowerSave)
    GUIManager.ClosePanel(PanelsConfig.Billboard)
    GameWorld.SetDamageHandlerRunning(false)
    self._curQualityLevel = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)
    SystemSettingManager:SetGameQuality(SystemSettingConfig.QUALITY_LOW)
    GameWorld.SetHideNpc(1)
    -- GameWorld.SetHidePlayer(1)
    GameWorld.SetHidePet(1)
    GameWorld.SetHideOtherPlayer(1)
    GameWorld.SetPauseFps(PauseFpsType.PowerSave, true)
    self._lastTargetFrameRate = GameWorld.GetTargetFrameRate()
    GameWorld.SetTargetFrameRate(powerSaveFrameRate)

    EventDispatcher:TriggerEvent(GameEvents.ON_POWER_SAVE_BEGIN) -- 抛出事件
end

function PowerSaveManager:StopPowerSave()
    if self._isInPowerSaving == false then
        return
    end
    self._isInPowerSaving = false

    local currentFrameRate = GameWorld.GetTargetFrameRate()
    if (self._lastTargetFrameRate ~= 0 and currentFrameRate == powerSaveFrameRate) then -- 期间被修改过帧率 则不还原
        GameWorld.SetTargetFrameRate(self._lastTargetFrameRate)
    end
    self._lastTargetFrameRate = 0
    SystemSettingManager:SetGameQuality(self._curQualityLevel)
    GameWorld.SetHideNpc(0)
    -- GameWorld.SetHidePlayer(0)
    GameWorld.SetHidePet(0)
    GameWorld.SetHideOtherPlayer(0)
    GameWorld.SetPauseFps(PauseFpsType.PowerSave, false)
    GameWorld.SetLowFps(false)
    GameWorld.SetDamageHandlerRunning(true)
    GUIManager.ShowPanel(PanelsConfig.Billboard)
    GUIManager.ClosePanel(PanelsConfig.PowerSave)
    self:SetPowerSaveMode(0)

    EventDispatcher:TriggerEvent(GameEvents.ON_POWER_SAVE_END) -- 抛出事件
end

function PowerSaveManager:SetPowerSaveMode(mode)
    if mode == 1 then
        self._lastSetting = {
            [SystemSettingConfig.OTHER_MONSTER] = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER),
            [SystemSettingConfig.OTHER_WING] = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_WING),
            [SystemSettingConfig.OTHER_PET] = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_PET),
            [SystemSettingConfig.OTHER_HUNQI] = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_HUNQI),
            [SystemSettingConfig.OTHER_SKILLFX] = SystemSettingManager:GetPlayerSetting(SystemSettingConfig.OTHER_SKILLFX),
        }
        SystemSettingManager:SetMonsterHide(1)
        SystemSettingManager:SetWingHide(1)
        SystemSettingManager:SetPetHide(1)
        SystemSettingManager:SetFabaoHide(1)
        SystemSettingManager:SetSkillFxHide(1)
    else
        SystemSettingManager:SetMonsterHide(self._lastSetting[SystemSettingConfig.OTHER_MONSTER])
        SystemSettingManager:SetWingHide(self._lastSetting[SystemSettingConfig.OTHER_WING])
        SystemSettingManager:SetPetHide(self._lastSetting[SystemSettingConfig.OTHER_PET])
        SystemSettingManager:SetFabaoHide(self._lastSetting[SystemSettingConfig.OTHER_HUNQI])
        SystemSettingManager:SetSkillFxHide(self._lastSetting[SystemSettingConfig.OTHER_SKILLFX])
    end
end

--=================== 省电模式相关 end ====



PowerSaveManager:Init()
return PowerSaveManager