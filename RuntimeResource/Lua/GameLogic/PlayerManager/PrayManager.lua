local PrayManager = {}

local DateTimeUtil = GameUtil.DateTimeUtil
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config

local error_code = GameWorld.error_code
local PrayDataHelper = GameDataHelper.PrayDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local VIPDataHelper = GameDataHelper.VIPDataHelper
local PrayData = PlayerManager.PlayerDataManager.prayData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper


function PrayManager:Init()
	self._timer = -1
	self._needtime = 0
	self._timerfun = function() self:PrayFreeTimer()end
	self._handleData = function() self:InitHandleData() end
	self._refreshRed = function()self:RefreshRedPointStatus()end	
end

function PrayManager:InitHandleData()
	self:HandleExp()
	self:HandleGold()
	EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_PRAY, self:RefreshRedPointStatus())        
	EventDispatcher:TriggerEvent(GameEvents.PrayUpdate)
end
function PrayManager:RefreshRedPointStatus()
	return PrayData:GetGoldRed()
end
function PrayManager:HandleExp()
	local d = {}	
	local entity = GameWorld.Player()
	local exptimes = entity.pray_exp_times or 0 --经验祈福次数(已经)
	local viplevel = entity.vip_level or 0
	local level = entity.level -- 等级
	local havecount = entity.money_coupons -- 拥有的钻石数量
	local getExp = PrayDataHelper:GetExp(level)--获得经验
	local expcostm = GlobalParamsHelper.GetParamValue(551) -- 费用字典
	local vipcount = VIPDataHelper:GetPrayGoldMaxtimeAdd(viplevel) -- 拥有祈福次数（原始）
	local curcost = 0
	if exptimes+1 > #expcostm then
		curcost= expcostm[exptimes] -- 当前消耗数
	else
		curcost= expcostm[exptimes+1] -- 当前消耗数	
	end
	if curcost <= havecount and vipcount > exptimes then
		PrayData:UpdateExpRed(true)
	else
		PrayData:UpdateExpRed(false)		
	end
	d['curcost'] = curcost
	d['vipcount'] = vipcount
	d['havecount'] = havecount
	d['finishcount'] = exptimes
	d['exp'] = getExp
	PrayData:UpdateExp(d)
end

function PrayManager:HandleGold()
	local d = {}	
	local entity = GameWorld.Player()
	local goldtimes = entity.pray_gold_times or 0 --金币祈福次数
	local goldfreetime = entity.pray_gold_free_time or 0 --最后一次，免费金币祈福时间
	local viplevel = entity.vip_level or 0	
	local havecount = entity.money_coupons -- 拥有的钻石数量	
	local goldcostm = GlobalParamsHelper.GetParamValue(550) -- 费用字典
	local vipcount = VIPDataHelper:GetPrayGoldMaxtimeAdd(viplevel) or 0 -- 拥有祈福次数（原始）
	local curcost = 0
	if goldtimes+1 > #goldcostm then
		curcost= goldcostm[goldtimes] -- 当前消耗数
	else
		curcost= goldcostm[goldtimes+1] -- 当前消耗数	
	end
	local getGold = GlobalParamsHelper.GetParamValue(552)--获得金币
	d['curcost'] = curcost
	d['vipcount'] = vipcount
	d['havecount'] = havecount
	d['finishcount'] = goldtimes
	d['gold'] = getGold	
	local needtime = GlobalParamsHelper.GetParamValue(553)--免费时间间隔
	local _d = DateTimeUtil.MinusServerDay(goldfreetime)
	local f = needtime-(_d['day']*24*60*60+_d['hour']*60*60+_d['min']*60+_d['sec'])
	if f<=0 then
		goldfreetime =0
	end
	if goldfreetime == 0 then
		PrayData:UpdateGoldRed(true)		
		d['type'] = 1 --免费	
	elseif curcost <= havecount and  vipcount > goldtimes then
		PrayData:UpdateGoldRed(false)
		d['type'] = 2 --土豪	
		if self._timer==-1 then
			self._needtime = f
			self._timer=TimerHeap:AddSecTimer(0,1,self._needtime,self._timerfun)
		end
	else
		if self._timer==-1 then
			self._needtime = f
			self._timer=TimerHeap:AddSecTimer(0,1,self._needtime,self._timerfun)
		end
		PrayData:UpdateGoldRed(false)
		d['type'] = 0 --没有		
	end
	PrayData:UpdateGold(d)	
end

function PrayManager:PrayFreeTimer()
	self._needtime=self._needtime-1
	if self._needtime<=0 then
		self._needtime = 0
		self:InitHandleData()
		TimerHeap:DelTimer(self._timer)	
		self._timer=-1			
	end
end
function PrayManager:GetFreeTime()
	return self._needtime
end
-- RPC
-- <!-- 祈福(金币) -->
function PrayManager:PrayActionGoldRpc()
	GameWorld.Player().server.pray_action_req(action_config.PRAY_GOLD_REQ)	
end

function PrayManager:PrayActionExpRpc()
	GameWorld.Player().server.pray_action_req(action_config.PRAY_EXP_REQ)	
end
--Resp



-- 进入加监听
function PrayManager:OnPlayerEnterWorld()
	local f = FunctionOpenDataHelper:CheckFunctionOpen(370)
	if f then
		self:InitHandleData()
	end
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._handleData)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._handleData)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PRAY_EXP_TIMES, self._handleData)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PRAY_GOLD_TIMES, self._handleData)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PRAY_GOLD_FREE_TIME, self._handleData)
end

function PrayManager:AddLevelListener()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._handleData)
end

function PrayManager:RemoveLevelListener()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._handleData)
end
--退出移除监听
function PrayManager:OnPlayerLeaveWorld()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._handleData)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._handleData)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PRAY_EXP_TIMES, self._handleData)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PRAY_GOLD_TIMES, self._handleData)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PRAY_GOLD_FREE_TIME, self._handleData)	
end

function PrayManager:HandleData(action_id,error_code,args)
	if action_id==action_config.PRAY_GOLD_REQ and error_code == 0 then
		local ratio = args[2]
		if ratio <= 1 then
			return
		end
		PrayData:UpdateCtri(ratio)
	else
	end
end

PrayManager:Init()

return PrayManager

	