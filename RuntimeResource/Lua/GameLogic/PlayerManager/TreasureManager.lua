local TreasureManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local bagData = PlayerManager.PlayerDataManager.bagData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function TreasureManager:Init()
	local g1 = GlobalParamsHelper.GetParamValue(420)
	local g2 = GlobalParamsHelper.GetParamValue(421)
	local g3 = GlobalParamsHelper.GetParamValue(422)
	local g4 = GlobalParamsHelper.GetParamValue(423)

	self._soulItemMax = {}
	self._soulItemMax[public_config.TREASURE_TYPE_WING] = g1
	self._soulItemMax[public_config.TREASURE_TYPE_TALISMAN] = g2
	self._soulItemMax[public_config.TREASURE_TYPE_WEAPON] = g3
	self._soulItemMax[public_config.TREASURE_TYPE_CLOAK] = g4

	--特殊道具配置
	self._rareItems ={}
    local rareItemList = ItemDataHelper.GetAllItemByEffectId(public_config.ITEM_EFF_ID_TREASURE_ATTRI)
    for i=1,#rareItemList do
        local itemInfo = rareItemList[i]
        local treasureType = itemInfo.effectId[2]
        if self._rareItems[treasureType] == nil then
            self._rareItems[treasureType] = {}
        end
        table.insert(self._rareItems[treasureType],itemInfo.id)
    end

    --升级道具配置
	self._upgradeItems ={}
    local upgradeItemList = ItemDataHelper.GetAllItemByEffectId(public_config.ITEM_EFF_ID_TREASURE_EXP)
    for i=1,#upgradeItemList do
        local itemInfo = upgradeItemList[i]
        local treasureType = itemInfo.effectId[2]
        if self._upgradeItems[treasureType] == nil then
            self._upgradeItems[treasureType] = {}
        end
        table.insert(self._upgradeItems[treasureType],itemInfo.id)
    end

    self._upgradeNodes = {}
    self._soulNodes = {}
    self._funIds = {}
    self._funIds[public_config.TREASURE_TYPE_WING] = public_config.FUNCTION_ID_WING
    self._funIds[public_config.TREASURE_TYPE_TALISMAN] = public_config.FUNCTION_ID_TALISMAN
    self._funIds[public_config.TREASURE_TYPE_WEAPON] = public_config.FUNCTION_ID_WEAPON
    self:AddEventListeners()
end

function TreasureManager:OnPlayerEnterWorld()
	self._upgradeNodes[public_config.TREASURE_TYPE_WING] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WING_UPGRADE, public_config.FUNCTION_ID_WING)--翅膀升星
	self._soulNodes[public_config.TREASURE_TYPE_WING] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WING_SOUL, public_config.FUNCTION_ID_WING)--翅膀培养

	self._upgradeNodes[public_config.TREASURE_TYPE_TALISMAN] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.TALISMAN_UPGRADE, public_config.FUNCTION_ID_TALISMAN)--法宝升星
	self._soulNodes[public_config.TREASURE_TYPE_TALISMAN] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.TALISMAN_SOUL, public_config.FUNCTION_ID_TALISMAN)--法宝培养
	
	self._upgradeNodes[public_config.TREASURE_TYPE_WEAPON] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WEAPON_UPGRADE, public_config.FUNCTION_ID_WEAPON)--神兵升星
	self._soulNodes[public_config.TREASURE_TYPE_WEAPON] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WEAPON_SOUL, public_config.FUNCTION_ID_WEAPON)--神兵培养

	self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_WING)
	self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_WING)

	self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_TALISMAN)
	self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_TALISMAN)

	self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_WEAPON)
	self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_WEAPON)
end

function TreasureManager:OnPlayerLeaveWorld()

end

function TreasureManager:AddEventListeners()
	self._refreshWingUpgradeRedPointState = function() self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_WING) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Wing_Upgrade_Red_Point_State, self._refreshWingUpgradeRedPointState)
    
    self._refreshWingSoulRedPointState = function() self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_WING) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Wing_Soul_Red_Point_State, self._refreshWingSoulRedPointState)

    self._refreshTalismanUpgradeRedPointState = function() self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_TALISMAN) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Talisman_Upgrade_Red_Point_State, self._refreshTalismanUpgradeRedPointState)
    
    self._refreshTalismanSoulRedPointState = function() self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_TALISMAN) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Talisman_Soul_Red_Point_State, self._refreshTalismanSoulRedPointState)

    self._refreshWeaponUpgradeRedPointState = function() self:RefreshUpgradeRedPointState(public_config.TREASURE_TYPE_WEAPON) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Weapon_Upgrade_Red_Point_State, self._refreshWeaponUpgradeRedPointState)
    
    self._refreshWeaponSoulRedPointState = function() self:RefreshSoulRedPointState(public_config.TREASURE_TYPE_WEAPON) end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Weapon_Soul_Red_Point_State, self._refreshWeaponSoulRedPointState)
end

--红点处理
function TreasureManager:RefreshUpgradeRedPointState(treasureType)
	if not FunctionOpenDataHelper:CheckFunctionOpen(self._funIds[treasureType]) then
		return false
	end
	local itemList = self._upgradeItems[treasureType]
	for i=1,#itemList do
		local itemId = itemList[i]
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagNum > 0 then
			self._upgradeNodes[treasureType]:CheckStrengthen(true)
			return true
		end
	end
	self._upgradeNodes[treasureType]:CheckStrengthen(false)
	return false
end

function TreasureManager:RefreshSoulRedPointState(treasureType)
	if not FunctionOpenDataHelper:CheckFunctionOpen(self._funIds[treasureType]) then
		return false
	end
	local itemList = self._rareItems[treasureType]
	for i=1,#itemList do
		local itemId = itemList[i]
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagNum > 0 then
			self._soulNodes[treasureType]:CheckStrengthen(true)
			return true
		end
	end
	self._soulNodes[treasureType]:CheckStrengthen(false)
	return false
end

function TreasureManager:OnTreasureUpdate()
	EventDispatcher:TriggerEvent(GameEvents.TREASURE_UPDATE)
end

function TreasureManager:HideTreasureModel(treasureType)
	GameWorld.Player().server.treasure_action_req(action_config.TREASURE_CHANGE_HIDE,tostring(treasureType))
end

function TreasureManager:GetTreasureInfo(treasureType)
	if treasureType == public_config.TREASURE_TYPE_WING then
		return GameWorld.Player().wing_info
	elseif treasureType == public_config.TREASURE_TYPE_TALISMAN then
		return GameWorld.Player().talisman_info
	elseif treasureType == public_config.TREASURE_TYPE_WEAPON  then
		return GameWorld.Player().weapon_info
	elseif treasureType == public_config.TREASURE_TYPE_CLOAK then
		return GameWorld.Player().cloak_info
	end
end

function TreasureManager:GetTreasureModelShow(treasureType)
	if treasureType == public_config.TREASURE_TYPE_WING then
		return GameWorld.Player().wing_show
	elseif treasureType == public_config.TREASURE_TYPE_TALISMAN then
		return GameWorld.Player().talisman_show
	elseif treasureType == public_config.TREASURE_TYPE_WEAPON  then
		return GameWorld.Player().weapon_show
	elseif treasureType == public_config.TREASURE_TYPE_CLOAK then
		return GameWorld.Player().cloak_show
	end
end

function TreasureManager:GetTreasureSoulItemUseMax(treasureType,itemId)
	local levelList = self._soulItemMax[treasureType][itemId]
	local playerLevel = GameWorld.Player().level
	for i=1,#levelList,3 do
		if playerLevel >= levelList[i] and playerLevel <= levelList[i+1] then
			return levelList[i+2],levelList[i+3]
		end
	end
	return 0,nil
end

function TreasureManager:GetUpgradeRedPoint(treasureType)
	local itemList

	return false
end

TreasureManager:Init()
return TreasureManager