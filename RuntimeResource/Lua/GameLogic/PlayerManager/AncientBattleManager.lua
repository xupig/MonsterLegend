local AncientBattleManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local ancientBattleData = PlayerDataManager.ancientBattleData
local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper
local GameSceneManager = GameManager.GameSceneManager
local ClientEntityManager = GameManager.ClientEntityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager
local InstanceManager = PlayerManager.InstanceManager
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local DateTimeUtil = GameUtil.DateTimeUtil

function AncientBattleManager:Init()
    self:InitCallbackFunc()
end

function AncientBattleManager:InitCallbackFunc()
    self._onEnterMap = function(mapId) self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId) self:OnLeaveMap(mapId) end
end

function AncientBattleManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function AncientBattleManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function AncientBattleManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE then
        InstanceManager:EnterInstance()
    end    
end

function AncientBattleManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        self:RetSetActionData()
    end
end

function AncientBattleManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("AncientBattleManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    end
    if action_id == action_config.CROSS_ANCIENT_BATTLE_ENTER_REQ then
        TaskCommonManager:RealStop()
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_NOTIFY_ROOM_STAGE then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_ROOM_STAGE, args)
        if args[1] == public_config.INSTANCE_STAGE_5 then
            local seconds = 30
            InstanceCommonManager:ShowLeaveCountDown(seconds, args[2])
        end
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_NOTIFY_END_TIME then
        local endTime = args[1]
        local seconds = endTime - DateTimeUtil.GetServerTime()
        InstanceCommonManager:ShowLeaveCountDown(seconds, DateTimeUtil.GetServerTime())
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_GET_SCORE_RANK then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_GET_SCORE_RANK, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ancient_Battle_Space_Score_Rank)
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_FACTION_INFO, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ancient_Battle_Space_Faction_Info)
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_SELF_INFO then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_SELF_INFO, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ancient_Battle_Space_Self_Info)
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_NOTIFY_SETTLE then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_NOTIFY_SETTLE, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ancient_Battle_Space_Settle_Info)
    elseif action_id == action_config.CROSS_ANCIENT_BATTLE_PICK_BUFF then
        self:RefreshActionData(action_config.CROSS_ANCIENT_BATTLE_PICK_BUFF, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ancient_Battle_Space_Buff_Info)
    end
end

-------------------------------------数据处理------------------------------------
function AncientBattleManager:RefreshActionData(action_id, data)
    ancientBattleData:RefreshActionData(action_id, data)
end

function AncientBattleManager:GetActionData(action_id)
    return ancientBattleData:GetActionData(action_id)
end

function AncientBattleManager:RetSetActionData()
    ancientBattleData:RetSetActionData()
end
-------------------------------------客户端请求-----------------------------------
function AncientBattleManager:OnSpaceEnterReq()
    GameWorld.Player().server.ancient_battle_action_req(action_config.CROSS_ANCIENT_BATTLE_ENTER_REQ, "")
end

function AncientBattleManager:OnGetScoreRankReq()
    GameWorld.Player().server.ancient_battle_action_req(action_config.CROSS_ANCIENT_BATTLE_GET_SCORE_RANK, "")
end

AncientBattleManager:Init()
return AncientBattleManager
