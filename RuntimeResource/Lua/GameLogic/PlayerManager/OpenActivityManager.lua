local OpenActivityManager = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GUIManager = GameManager.GUIManager
local DailyConfig = GameConfig.DailyConfig
local public_config = GameWorld.public_config
local CircleTaskManager = PlayerManager.CircleTaskManager
local ExpInstanceManager = PlayerManager.ExpInstanceManager
local EscortPeriManager = PlayerManager.EscortPeriManager
local OpenActivityData = PlayerManager.PlayerDataManager.openActivityData
local UpgradeGuideType = GameConfig.EnumType.UpgradeGuideType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RedPointManager = GameManager.RedPointManager

function OpenActivityManager:Init()
    self:InitCallbackFunc()
end

function OpenActivityManager:InitCallbackFunc()
    self._ids = GlobalParamsHelper.GetNSortValue(934)
    self._activityIsAdd = function(funcID,isOpen)self:OpenActivityFuncIsAdd(funcID,isOpen) end
end

function OpenActivityManager:OnPlayerEnterWorld()
    self._node = RedPointManager:GetRedPointDataByPanelName(PanelsConfig.Activity)
    for i,v in ipairs(self._ids) do
        local panelId = v[2][1]
        local functionId = v[2][2]
        local node
        if panelId ~= 0 then
            node = RedPointManager:GetRedPointDataByFirstId(panelId)
        end
        if functionId ~= 0 then
            node = RedPointManager:GetRedPointDataByFuncId(functionId)
        end
        node:SetParent(self._node)
    end

    EventDispatcher:AddEventListener(GameEvents.OpenActivityFuncIsAdd, self._activityIsAdd)
end

function OpenActivityManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.OpenActivityFuncIsAdd, self._activityIsAdd)
end


function OpenActivityManager:DoFollowEvent(follow)
    if not follow or type(follow) ~= "table" then
        LoggerHelper.Error("Invalid Event follow :" .. tostring(follow))
        return
    end

    if follow[1] == DailyConfig.FOLLOW_OPEN_PANEL then-- 1.打开界面，填功能ID
        GUIManager.ShowPanelByFunctionId(follow[2][1])
    elseif follow[1] == 2 then-- 2.事件,填事件名称
        EventDispatcher:TriggerEvent(follow[2][1])
    end
end


function OpenActivityManager:OpenActivityFuncIsAdd(funcID,isOpen)
    --LoggerHelper.Error("OpenActivityFuncIsAdd"..funcID)
    OpenActivityData:SetChangeList(funcID, isOpen)
    EventDispatcher:TriggerEvent(GameEvents.OpenActivityListChange)

    -- local followNew = {}
    -- local followData =  GlobalParamsHelper.GetNSortValue(934)

    -- for i,followItemData in ipairs(followData) do
    --     if followItemData[2][1] == funcID then
    --         if isopen then
    --             table.insert(followNew,followItemData)
    --         else
    --             --table.remove()
    --         end
    --     end
    -- end
    -- return followNew

end


function OpenActivityManager:GetUpgradeData()
    local followNew = {}
    local followData =  GlobalParamsHelper.GetNSortValue(934)

    for i,followItemData in ipairs(followData) do
        local isopen = false
        if followItemData[2][1] == public_config.FUNCTION_ID_OPEN_CUMULATE then
            isopen = FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        else
            isopen = FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        end

        if i == UpgradeGuideType.Gold then
              --isopen = FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        elseif i == UpgradeGuideType.Exp then
            --isopen = ExpInstanceManager:GetExpInstanceCanEnter() and FunctionOpenDataHelper:CheckFunctionOpen(followItemData[2][1])
        elseif i == UpgradeGuideType.Escort then
            --isopen = EscortPeriManager:IsHasTimes() and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUARD_GODDESS)
        elseif i == UpgradeGuideType.Map then
            --isopen = true
        end
        if isopen then
            table.insert(followNew,followItemData)
        end
    end
    return followNew
end

OpenActivityManager:Init()
return OpenActivityManager