local CircleTaskManager = {}

local error_code = GameWorld.error_code
local action_config = GameWorld.action_config

local circleTaskData = PlayerManager.PlayerDataManager.circleTaskData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local CircleTaskRewardDataHelper = GameDataHelper.CircleTaskRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local public_config = GameWorld.public_config
local TaskConfig = GameConfig.TaskConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager
local PlayerActionManager = GameManager.PlayerActionManager

require "PlayerManager/PlayerData/CircleTaskData"
local CircleTaskData = ClassTypes.CircleTaskData

require "PlayerManager/PlayerData/TaskData"
local TracingPointData = ClassTypes.TracingPointData

function CircleTaskManager:Init()
    self._initList = false
    self:InitCallbackFunc()
end

function CircleTaskManager:InitCallbackFunc()
    self._onAcceptCircleTask = function(id)self:OnAcceptCircleTask(id) end
    self._onCommitCircleTask = function(id)self:OnCommitCircleTask(id) end
    self._onDailyToCircleTask = function()self:RunCircleTask(true) end
end

function CircleTaskManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ACCEPT_CIRCLE_TASK, self._onAcceptCircleTask)
    EventDispatcher:AddEventListener(GameEvents.COMMIT_CIRCLE_TASK, self._onCommitCircleTask)
    EventDispatcher:AddEventListener(GameEvents.Daily_Circle_Task, self._onDailyToCircleTask)
end

function CircleTaskManager:OnPlayerLeaveWorld()
    self._initList = false
    self:ClearTaskData()
    EventDispatcher:RemoveEventListener(GameEvents.ACCEPT_CIRCLE_TASK, self._onAcceptCircleTask)
    EventDispatcher:RemoveEventListener(GameEvents.COMMIT_CIRCLE_TASK, self._onCommitCircleTask)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Circle_Task, self._onDailyToCircleTask)
end

function CircleTaskManager:ClearTaskData()
    self._completeTask = false
    self._acceptTask = true
    local status = public_config.TASK_STATE_ACCEPTABLE
    self._circleTaskData = CircleTaskData:NewCircleTaskData(nil, status) 
end

function CircleTaskManager:OnCommitCircleTask()
    self:CommitTask()
end

function CircleTaskManager:OnAcceptCircleTask(id)
    self:AcceptTask()
end

function CircleTaskManager:HandleAutoTask()
    self:RunCircleTask()
end

function CircleTaskManager:HandleData(action_id, error_id, args)
    if error_id > error_code.ERR_SUCCESSFUL then
        return
    end

    if action_id == action_config.CIRCLE_TASK_LIST_REQ then--7201, --全部任务列表，服务器推送
        --LoggerHelper.Error("Sam:7201 环任务列表信息====" .. PrintTable:TableToStr(args))
        self:RefreshTaskInfo(args)  
        if not self._initList then      
            EventDispatcher:TriggerEvent(GameEvents.CIRCLE_TASK_LIST)
            self._initList = true
        end
    elseif action_id == action_config.CIRCLE_TASK_REFRESH then--7202, --更新任务状态，服务器推送
        --{"5" = {"1" = {"20004" = 0},"2" = 1}}  {"5" = {"1" = {},"2" = 2}}
        -- id = 1 = {targetId = 完成次数}, 2 = 状态}
        -- 1进行中，有进度的；2已经完成，没了进度信息，要读表； 3已经领取奖励，没了进度信息，要读表
        --LoggerHelper.Error("Sam:7202 更新更新 环任务列表信息====" .. PrintTable:TableToStr(args))
        self:RefreshTaskInfo(args)
    elseif action_id == action_config.CIRCLE_TASK_ACCEPT_REQ then--7203, --接任务，随机一个任务来接。前提是现在没有已接任务。
        -- LoggerHelper.Log("Sam:7203 接任务====" .. PrintTable:TableToStr(args))
        GUIManager.ShowPanel(PanelsConfig.CircleTaskPanel)
    elseif action_id == action_config.CIRCLE_TASK_COMMIT_REQ then--7204, --提交任务。领取奖励
        -- LoggerHelper.Log("Sam:7204 提交任务。领取奖励====" .. PrintTable:TableToStr(args))
        self:CheckIsCompleteAll()
    elseif action_id == action_config.CIRCLE_TASK_QUICK_COMPLETE then--7205, --快速完成，包括提交
        self:CheckIsCompleteAll()
    elseif action_id == action_config.CIRCLE_TASK_ONE_CLICK then--7206, --一键完成全部完成剩余的赏金任务
        -- LoggerHelper.Log("Sam:7206 一键完成全部完成剩余的赏金任务====" .. PrintTable:TableToStr(args))
        local curAutoTask = TaskCommonManager:GetCurAutoTask()
        if curAutoTask and curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
            PlayerActionManager:StopCurrAction()
        end
    end
end

function CircleTaskManager:CheckIsCompleteAll()
    -- LoggerHelper.Error("CircleTaskManager:CheckIsCompleteAll() completeNum =" .. GameWorld.Player().circle_task_cur_day_count)
    local completeNum = GameWorld.Player().circle_task_cur_day_count

    if completeNum == GlobalParamsHelper.GetParamValue(487) then
        local rewards = CircleTaskRewardDataHelper:GetCircleRewardByLevel()
        -- local datas = {}
        -- for i,v in ipairs(rewards) do
        --     local data = {}
        --     data[1] = v[1]
        --     data[2] = v[2]
        --     table.insert(datas, data)
        -- end

        local value = {
            ["text"] = LanguageDataHelper.CreateContent(58653, {["0"] = GlobalParamsHelper.GetParamValue(487)}),
            ["seconds"] = 5,
            ["rewards"] = rewards
        }
        local msgData ={["id"] = MessageBoxType.RewardCountDowenTip,["value"]=value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    end
end

function CircleTaskManager:RefreshTaskInfo(args)    
    local completeNum = GameWorld.Player().circle_task_cur_day_count
    self._completeTask = completeNum == GlobalParamsHelper.GetParamValue(487)
    local oldId = self._circleTaskData and self._circleTaskData:GetId() or nil
    local datas = {}

    if table.isEmpty(args) then
        self._acceptTask = not self._completeTask
        local status = public_config.TASK_STATE_ACCEPTABLE
        if self._completeTask then
            status = public_config.TASK_STATE_COMPLETED
        end

        if self._completeTask and oldId and self._initList then
            local data = {}
            data[TaskConfig.TASK_CHANGE_INFO_ID] = oldId
            data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_CIRCLETASK
            data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
            table.insert(datas, data)
            PlayerManager.TaskManager:UpdateTasksForMainMenu(datas)
        end
        self._circleTaskData = CircleTaskData:NewCircleTaskData(nil, status)        
    else
        self._acceptTask = false
        for circleTaskId, serverData in pairs(args) do
            circleTaskData = CircleTaskData()
            circleTaskData:UpdateData(circleTaskId, serverData)
            if circleTaskData:IsSameTask(self._circleTaskData) then
                return
            end
            if self._initList then
                if oldId == nil then
                    -- LoggerHelper.Error("新增", true)
                    --新增
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = circleTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_CIRCLETASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = circleTaskData
                    table.insert(datas, data)
                elseif oldId == circleTaskId then
                    --更新
                    -- LoggerHelper.Error("更新", true)
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = circleTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_CIRCLETASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = circleTaskData
                    table.insert(datas, data)
                elseif oldId ~= circleTaskId then
                    --删除,新增
                    -- LoggerHelper.Error("删除,新增", true)
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = oldId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_CIRCLETASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
                    table.insert(datas, data)

                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = circleTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_CIRCLETASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = circleTaskData
                    table.insert(datas, data)
                end 
                PlayerManager.TaskManager:UpdateTasksForMainMenu(datas)
            end
            local flag = self._circleTaskData == nil
            self._circleTaskData = circleTaskData
            local task = TaskCommonManager:GetCurAutoTask()
            if (not flag) and task ~= nil and task:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
                TaskCommonManager:SetCurAutoTask(self._circleTaskData, TaskCommonManager:GetNextEvent(self._circleTaskData))
                TaskCommonManager:UpdateAutoTask()
            end
            break
        end
    end
    
    EventDispatcher:TriggerEvent(GameEvents.CIRCLE_TASK_REFRESH)
end

function CircleTaskManager:GetCircleTask()
    if self._acceptTask or self._completeTask or self._circleTaskData == nil   then
        return nil
    end
    return self._circleTaskData
end

function CircleTaskManager:RunCircleTask(manualHandleTask)
    if self._completeTask then return end
    local eventItemData = TaskCommonManager:GetNextEvent(self._circleTaskData)
    local trackTask = TracingPointData(self._circleTaskData, eventItemData)
    if manualHandleTask then
        TaskCommonManager:OnManualHandleTask(trackTask)
    else
        TaskCommonManager:OnClickTrackCurTask(trackTask)    
    end
end

function CircleTaskManager:IsCompleteCircleTask()
    return self._completeTask
end

function CircleTaskManager:NeedAcceptTask()
    return self._acceptTask
end

function CircleTaskManager:GetCircleTaskData()
    return self._circleTaskData
end

function CircleTaskManager:GetTaskMenuData()
    local datas = {}
    local ctData = {}
    table.insert(ctData, {["text"] = LanguageDataHelper.CreateContent(925)})
    local circleTask = self:GetCircleTaskData()
    if circleTask ~= nil then
        local eventItemData = nil
        if not self._completeTask then
            eventItemData = TaskCommonManager:GetNextEvent(circleTask)
        end
        table.insert(ctData, TracingPointData(circleTask, eventItemData))
    end
    table.insert(datas, ctData)
    -- LoggerHelper.Error("数据====" .. PrintTable:TableToStr(datas))
    return datas
end

--获取日常任务的itemdata和eventdata
function CircleTaskManager:GetCircleTaskInfo(npcID)
    local circleTask, eventItemData
    if self._circleTaskData ~= nil and (not self._completeTask) and (not self._acceptTask) then
        local state = self._circleTaskData:GetState()
        if state == public_config.TASK_STATE_ACCEPTABLE and self._circleTaskData:GetAcceptNPC() == npcID then
            circleTask = self._circleTaskData
            eventItemData = TaskCommonManager:GetDialogEvent(self._circleTaskData, npcID)
        elseif state == public_config.TASK_STATE_COMPLETED and self._circleTaskData:GetCommitNPC() == npcID then
            circleTask = self._circleTaskData
            eventItemData = TaskCommonManager:GetDialogEvent(self._circleTaskData, npcID)
        end
    end
    return circleTask, eventItemData
end

--7203, --接任务，随机一个任务来接。前提是现在没有已接任务。
function CircleTaskManager:AcceptTask()
    GameWorld.Player().server.circle_task_action_req(action_config.CIRCLE_TASK_ACCEPT_REQ, "")
end

--7204, --提交任务。领取奖励
function CircleTaskManager:CommitTask()
    -- LoggerHelper.Log("Sam: CircleTaskManager:CommitTask() 提交任务。领取奖励")
    GameWorld.Player().server.circle_task_action_req(action_config.CIRCLE_TASK_COMMIT_REQ, "")
end

--7205, --快速完成，包括提交
function CircleTaskManager:FastCommitTask()
    GameWorld.Player().server.circle_task_action_req(action_config.CIRCLE_TASK_QUICK_COMPLETE, "")
end

--7206 --一键完成全部完成剩余的赏金任务
function CircleTaskManager:FastCommitAllTask()
    GameWorld.Player().server.circle_task_action_req(action_config.CIRCLE_TASK_ONE_CLICK, "")
end

CircleTaskManager:Init()
return CircleTaskManager