local PetInstanceManager = {}

local public_config = GameWorld.public_config
local action_config = require("ServerConfig/action_config")
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceManager = PlayerManager.InstanceManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local instanceBalanceData = PlayerManager.PlayerDataManager.instanceBalanceData
local RedPointManager = GameManager.RedPointManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function PetInstanceManager:Init()
    self:InitCallbackFunc()
end

function PetInstanceManager:InitRedpointNodes()
    self._petNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_HOLY_TOWER)
end

function PetInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function PetInstanceManager:OnPlayerEnterWorld()
    self:InitRedpointNodes()
    self:UpdateRedPoint()
    --LoggerHelper.Error("PetInstanceManager:OnPlayerEnterWorld()")
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function PetInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function PetInstanceManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE then
        self._instanceId = nil
        self._startTime = nil
    end
end

function PetInstanceManager:OnEnterMap(mapId)
   --LoggerHelper.Error("PetInstanceManager:OnEnterMap()" .. tostring(mapId))
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_PET_INSTANCE then
        InstanceManager:EnterInstance()
    end    

    -- if self._leaveMapId ~= mapId and MapDataHelper.GetSceneType(self._leaveMapId) == SceneConfig.SCENE_TYPE_PET_INSTANCE then
        -- GUIManager.ShowPanel(PanelsConfig.InstanceHall,{["functionId"] = 203})
    -- end
end

function PetInstanceManager:BeginCountDown()
    local instanceTime = PetInstanceDataHelper:GetTime(self._instanceId)
    InstanceCommonManager:StartInstanceCountDown(instanceTime, self._startTime)
end

function PetInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("PetInstanceManager HandleData Error:" .. error_id)
        if action_id == action_config.HOLY_TOWER_SWEEP then--6003,     --扫荡
            EventDispatcher:TriggerEvent(GameEvents.SWEEP_INSTANCE_END, false)
        end
        if action_id == action_config.HOLY_TOWER_ENTER then--6001,     --进入副本
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.HOLY_TOWER_ENTER)
        end
        return
    elseif action_id == action_config.HOLY_TOWER_ENTER then                 --6001,     --进入副本
        --LoggerHelper.Error("sam 6001 == " .. PrintTable:TableToStr(args))
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.HOLY_TOWER_ENTER)
        TaskCommonManager:RealStop()
    elseif action_id == action_config.HOLY_TOWER_BUY_ENTER_NUM then         --6002,     --购买次数
        -- LoggerHelper.Log("sam 6002 == " .. PrintTable:TableToStr(args))
        GUIManager.ShowText(2,LanguageDataHelper.CreateContent(58660))
        GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
        -- EventDispatcher:TriggerEvent(GameEvents.PET_INSTANCE_INFO_CHANGE)
    elseif action_id == action_config.HOLY_TOWER_SWEEP then                 --6003,     --扫荡
        --LoggerHelper.Log("sam 6003 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SWEEP_INSTANCE_END, true)  
        -- EventDispatcher:TriggerEvent(GameEvents.PET_INSTANCE_INFO_CHANGE)
        InstanceBalanceManager:ShowSweepBalance(args)
    elseif action_id == action_config.HOLY_TOWER_SPACE_INFO then            --6004,     --副本状态变化
        -- LoggerHelper.Log("sam 6004 == " .. PrintTable:TableToStr(args)) 
        -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then
            --副本开始倒计时,如：5秒后开始副本
            local time = GlobalParamsHelper.GetParamValue(473)
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then
            --副本开始开始计时，如03:30后结束副本。副本开始计时时需要先显示评价星级
            GameWorld.Player():StartAutoFight()

            self._startTime = tonumber(args[2])
            data.start = 3
            data.startTimeData = PetInstanceDataHelper:GetStartTime(self._instanceId)
            data.dropMessages = InstanceConfig.PET_INSTANCE_START_DROP_MESSAGE_MAP
            data.countDownMessages = InstanceConfig.PET_INSTANCE_COUNT_DOWN_MESSAGE_MAP
            data.cb = function(start) self:MoveStartEnd(start) end
            data.startTime = tonumber(args[2])
            InstanceCommonManager:ShowStart(data)  
        else
        end
    elseif action_id == action_config.HOLY_TOWER_SETTLE then                --6005,     --结算信息
        -- LoggerHelper.Log("sam 6005 == " .. PrintTable:TableToStr(args))
        InstanceBalanceManager:ShowBalance(args)
    elseif action_id == action_config.HOLY_TOWER_WAVE_CHANGE then           --6006,     --波数变化
        -- LoggerHelper.Log("sam 6006 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.instanceId = self._instanceId
        data.num = args[1]
        GUIManager.ShowPanel(PanelsConfig.PetInstanceInfo, data)
    else
        LoggerHelper.Error("PetInstanceManager HandleData action id not handle:" .. action_id)
    end
end

function PetInstanceManager:OnUpdateInfo()
    self:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.PET_INSTANCE_INFO_CHANGE)
end

function PetInstanceManager:UpdateRedPoint()
    if self._petNode and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HOLY_TOWER) then
        local petData = GameWorld.Player().holy_tower_info
        local fightNum = petData[public_config.HOLY_TOWER_KEY_LEFT_ENTER_NUM]
        self._petNode:SetRedPoint(fightNum > 0)
    end
end

function PetInstanceManager:SetStartTime(startTime)
    self._startTime = startTime
end

--星级评价移动完毕
function PetInstanceManager:MoveStartEnd(start)
    -- if start == 3 then
    self:BeginCountDown()
    -- end
end

--获取副本开始时间
function PetInstanceManager:GetStartTime()
    return self._startTime
end

--设置副本id
function PetInstanceManager:SetInstanceId(instanceId)
    self._instanceId = instanceId
end

--获取副本id
function PetInstanceManager:GetInstanceId()
    return self._instanceId
end

--6001,     --进入副本
function PetInstanceManager:EnterPetInstance(id)
    local cb = function() self:ConfirmGotoBoss(id) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function PetInstanceManager:ConfirmGotoBoss(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.HOLY_TOWER_ENTER)
    self._instanceId = id
    GameWorld.Player().server.holy_tower_action_req(action_config.HOLY_TOWER_ENTER, tostring(id))
end

--6002,     --购买次数
function PetInstanceManager:BuyPetInstanceEnterNum(num)
    GameWorld.Player().server.holy_tower_action_req(action_config.HOLY_TOWER_BUY_ENTER_NUM, tostring(num))
end

--6003,     --扫荡
function PetInstanceManager:SweepPetInstance(id)
    GameWorld.Player().server.holy_tower_action_req(action_config.HOLY_TOWER_SWEEP, tostring(id))
end

PetInstanceManager:Init()
return PetInstanceManager