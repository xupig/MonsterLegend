local SpecialDialogManager = {}
local Queue = ClassTypes.Queue
local action_config = require("ServerConfig/action_config")
local SpecialDialogDataHelper = GameDataHelper.SpecialDialogDataHelper
local GameSceneManager = GameManager.GameSceneManager
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local BagData = PlayerDataManager.bagData
local SpecialDialogConfig = GameConfig.SpecialDialogConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local EntityType = GameConfig.EnumType.EntityType

function SpecialDialogManager:Init()
    if GameWorld.SpecialExamine then
        return
    end
    self._eventActivity = {}
    -- self._timeStamp = {} -- 暂不实现，等真正有需要的时候再做
    self._acceptTask = {}
    self._completeTask = {}
    self._getItem = {}
    self._areaTrigger = {}
    self._HpValue = {}
    self._dialogFinish = {}
    self._distanceTrigger = {}
    self._enterBattle = {}
    self._leaveBattle = {}
    self._serverSettle = {}
    self._playerDie = {}
    self._showDialogQueue = Queue(100)
    
    self._curSceneAreaTrigger = {}
    self._curSceneHpValue = {}
    self._hasPlayHpValue = {}-- 记录某只怪物一次生命周期内已经执行过的SpecialDialog
    
    self._distanceTriggerEntites = {}
    self._timerShowDialogDurationId = {}
    self._entityInDistanceTimer = {}
    self:InitCallbackFunc()
    local sdds = SpecialDialogDataHelper:GetAllId()
    for i, v in ipairs(sdds) do
        local target = SpecialDialogDataHelper:GetTriggerAnd(v)
        if target == nil then
            target = SpecialDialogDataHelper:GetTriggerOr(v)
        end
        self:SetTrigger(target, v)
    end
end

function SpecialDialogManager:InitCallbackFunc()
    self._onEnterEventActivity = function(eventActivityId)self:OnEnterEventActivity(eventActivityId) end
    self._onItemChangeAdd = function(bagType, pos)self:OnItemChangeAdd(bagType, pos) end
    self._onItemChangeIncrease = function(bagId, bagType, pos, lastCount)self:OnItemChangeIncrease(bagId, bagType, pos, lastCount) end
    self._onAcceptTask = function(taskId)self:AcceptTask(taskId) end
    self._onCommitTask = function(taskId)self:CommitTask(taskId) end
    self._onActionEvent = function(regionId)self:ActionEvent(regionId) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onPlayerDie = function(mapId)self:OnPlayerDie() end
    self._onPropertyIsFightChange = function(isFight)self:OnPropertyIsFightChange(isFight) end
    self._onServerSettle = function(mapId)self:OnServerSettle() end
    self._onMonsterEnterWorld = function(monsterId, entityId)self:OnMonsterEnterWorld(monsterId, entityId) end
    self._onMonsterLeaveWorld = function(monsterId, entityId)self:OnMonsterLeaveWorld(monsterId, entityId) end
    self._onMonsterCurHpChanged = function(entityId)self:OnMonsterCurHpChanged(entityId) end
end

function SpecialDialogManager:OnPlayerEnterWorld()
    if GameWorld.SpecialExamine then
        return
    end
    -- self._timerId = TimerHeap:AddSecTimer(1, 60, 0, function()self:CheckTimeUpdate() end)
    EventDispatcher:AddEventListener(GameEvents.OnEnterEventActivity, self._onEnterEventActivity)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_ADD, self._onItemChangeAdd)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_INCREASE, self._onItemChangeIncrease)
    EventDispatcher:AddEventListener(GameEvents.ACCEPT_TASK_CALLBACK, self._onAcceptTask)
    EventDispatcher:AddEventListener(GameEvents.COMMIT_TASK_CALLBACK, self._onCommitTask)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.PlayerDie, self._onPlayerDie)
    EventDispatcher:AddEventListener(GameEvents.PROPERTY_IS_FIGHT_CHANGE, self._onPropertyIsFightChange)
    EventDispatcher:AddEventListener(GameEvents.OnServerSettle, self._onServerSettle)
    self._tickTimer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnTick() end)
end

function SpecialDialogManager:OnPlayerLeaveWorld()
    if GameWorld.SpecialExamine then
        return
    end
    -- TimerHeap:DelTimer(self._timerId)
    EventDispatcher:RemoveEventListener(GameEvents.OnEnterEventActivity, self._onEnterEventActivity)
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_ADD, self._onItemChangeAdd)
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_INCREASE, self._onItemChangeIncrease)
    EventDispatcher:RemoveEventListener(GameEvents.ACCEPT_TASK_CALLBACK, self._onAcceptTask)
    EventDispatcher:RemoveEventListener(GameEvents.COMMIT_TASK_CALLBACK, self._onCommitTask)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.PlayerDie, self._onPlayerDie)
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_IS_FIGHT_CHANGE, self._onPropertyIsFightChange)
    EventDispatcher:RemoveEventListener(GameEvents.OnServerSettle, self._onServerSettle)
    TimerHeap:DelTimer(self._tickTimer)
end

function SpecialDialogManager:SetTrigger(target, specialDialogId)
    if target == nil then
        return
    end
    for k, v in pairs(target) do
        if SpecialDialogConfig.EVENT_ACTIVITY == k then
            self._eventActivity[specialDialogId] = v[1]
        -- elseif SpecialDialogConfig.TIME_STAMP == k then
        --     self._timeStamp[specialDialogId] = v[1]
        elseif SpecialDialogConfig.ACCEPT_TASK == k then
            self._acceptTask[specialDialogId] = v[1]
        elseif SpecialDialogConfig.COMPLETE_TASK == k then
            self._completeTask[specialDialogId] = v[1]
        elseif SpecialDialogConfig.GET_ITEM == k then
            self._getItem[specialDialogId] = v[1]
        elseif SpecialDialogConfig.AREA_TRIGGER == k then
            self._areaTrigger[specialDialogId] = v
        elseif SpecialDialogConfig.HP_VALUE == k then
            self._HpValue[specialDialogId] = v
        elseif SpecialDialogConfig.DIALOG_FINISH == k then
            self._dialogFinish[specialDialogId] = v[1]
        elseif SpecialDialogConfig.DISTANCE_TRIGGER == k then
            self._distanceTrigger[specialDialogId] = v
        elseif SpecialDialogConfig.ENTER_BATTLE == k then
            self._enterBattle[specialDialogId] = v
        elseif SpecialDialogConfig.LEAVE_BATTLE == k then
            self._leaveBattle[specialDialogId] = v
        elseif SpecialDialogConfig.SERVER_SETTLE == k then
            self._serverSettle[specialDialogId] = v[1]
        elseif SpecialDialogConfig.PLAYER_DIE == k then
            self._playerDie[specialDialogId] = v[1]
        end
    end
end

function SpecialDialogManager:CheckTrigger(sddId, forceShowDialog)
    local target = SpecialDialogDataHelper:GetTriggerAnd(sddId)
    if target ~= nil then -- 判断有并条件的情况
        for k, v in pairs(target) do
            if SpecialDialogConfig.OWN_ITEM == k then
                if BagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(v[1]) == false then
                    return false -- 组合条件里只要有一条不满足，都直接跳过
                end
            end
        end
    end
    -- 能来到这里，要么是或条件，要么并条件判断都通过了
    self:DoTrigger(sddId, forceShowDialog)
    return true
end

function SpecialDialogManager:DoTrigger(sddId, forceShowDialog)
    -- LoggerHelper.Log("Ash: DoTrigger: " .. sddId)
    if forceShowDialog == true then
        self:ShowDialog(sddId)
        return
    end
    self._showDialogQueue:Enqueue(sddId)
    self:ShowNextDialog()
end

function SpecialDialogManager:ShowNextDialog()
    if self._isShowingDialog == true then
        return
    end
    if self._showDialogQueue:IsEmpty() then
        return
    end
    self._isShowingDialog = true
    self:ShowDialog(self._showDialogQueue:Dequeue())
end

function SpecialDialogManager:ShowDialog(sddId)
    local delay = SpecialDialogDataHelper:GetDelay(sddId)
    self._timerShowDialogDelayId = TimerHeap:AddSecTimer(delay, 0, 0, function()self:DoShowDialog(sddId) end)
end

function SpecialDialogManager:DoShowDialog(sddId)
    GUIManager.ShowPanel(PanelsConfig.SpecialDialog, sddId)
    
    local duration = SpecialDialogDataHelper:GetDuration(sddId)
    self._timerShowDialogDurationId = TimerHeap:AddSecTimer(duration, 0, 0,
        function()
            GUIManager.ClosePanel(PanelsConfig.SpecialDialog)
            self._isShowingDialog = false
            if self:OnDialogFinish(sddId) == false then
                self:ShowNextDialog()
            end
        end)
end

-- function SpecialDialogManager:CheckTimeUpdate()
-- end
function SpecialDialogManager:OnEnterEventActivity(eventActivityId)
    for sddId, v in pairs(self._eventActivity) do
        if v == eventActivityId then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:OnItemChangeAdd(bagType, pos)
    if bagType == public_config.PKG_TYPE_ITEM then
        local item = BagData:GetItem(bagType, pos)
        if item ~= nil then
            local itemId = item:GetItemID()
            for sddId, v in pairs(self._getItem) do
                if v == itemId then
                    self:CheckTrigger(sddId)
                end
            end
        else
            LoggerHelper.Error("SpecialDialogManager:OnItemChangeAdd null item Error: " .. tostring(bagType) .. " " .. tostring(pos))
        end
    end
end

function SpecialDialogManager:OnItemChangeIncrease(bagId, bagType, pos, lastCount)
    if bagType == public_config.PKG_TYPE_ITEM then
        local item = BagData:GetItem(bagType, pos)
        local itemId = item:GetItemID()
        for sddId, v in pairs(self._getItem) do
            if v == itemId then
                self:CheckTrigger(sddId)
            end
        end
    end
end

function SpecialDialogManager:AcceptTask(taskId)
    -- LoggerHelper.Log("Ash: AcceptTask: " .. taskId .. " " .. PrintTable:TableToStr(self._acceptTask))
    for sddId, v in pairs(self._acceptTask) do
        if v == taskId then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:CommitTask(taskId)
    for sddId, v in pairs(self._completeTask) do
        if v == taskId then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:ActionEvent(regionId)
    -- LoggerHelper.Log("Ash: ActionEvent: " .. regionId.. " " .. PrintTable:TableToStr(self._curSceneAreaTrigger))
    for sddId, v in pairs(self._curSceneAreaTrigger) do
        if v[2] == regionId then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:OnEnterMap(mapId)
    -- LoggerHelper.Log("Ash: OnEnterMap: " .. mapId)
    local hasAddAreaTriggerListener = false
    local hasAddHpValueListener = false
    for sddId, v in pairs(self._areaTrigger) do
        if v[1] == mapId then
            if hasAddAreaTriggerListener == false then
                EventDispatcher:AddEventListener(GameEvents.ActionEvent, self._onActionEvent)
                hasAddAreaTriggerListener = true
            end
            self._curSceneAreaTrigger[sddId] = v
        end
    end
    for sddId, v in pairs(self._HpValue) do
        if v[1] == mapId then
            if hasAddHpValueListener == false then
                EventDispatcher:AddEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
                EventDispatcher:AddEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
                hasAddHpValueListener = true
            end
            self._curSceneHpValue[sddId] = v
        end
    end
    EventDispatcher:AddEventListener(GameEvents.OnNpcEnterWorld, self._onNpcEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnNpcLeaveWorld, self._onNpcLeaveWorld)
    local npcs = GameSceneManager:GetNPCs()
    for i, v in pairs(npcs) do
        --LoggerHelper.Log("Ash: BubbleDialogManager npcs: " .. i)
        self:OnNpcEnterWorld(v.npc_id, v.id)
    end
    local monster = GameWorld.GetEntitiesByType(EntityType.Monster)
    for i, v in pairs(monster) do
        --LoggerHelper.Log("Ash: BubbleDialogManager Monster: " .. i)
        self:OnMonsterEnterWorld(v.monster_id, v.id)
    end

end

function SpecialDialogManager:OnLeaveMap(mapId)
    EventDispatcher:RemoveEventListener(GameEvents.ActionEvent, self._onActionEvent)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnNpcEnterWorld, self._onNpcEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnNpcLeaveWorld, self._onNpcLeaveWorld)
    self._curSceneAreaTrigger = {}
    self._curSceneHpValue = {}
    self._hasPlayHpValue = {}
end

function SpecialDialogManager:OnPlayerDie()
    for sddId, v in pairs(self._playerDie) do
        if GameSceneManager:GetCurrMapID() == v then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:OnPropertyIsFightChange(isFight)
    if isFight then
        for sddId, v in pairs(self._enterBattle) do
            self:CheckTrigger(sddId)
        end
    else
        for sddId, v in pairs(self._leaveBattle) do
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:OnServerSettle()
    for sddId, v in pairs(self._serverSettle) do
        if GameSceneManager:GetCurrMapID() == v then
            self:CheckTrigger(sddId)
        end
    end
end

function SpecialDialogManager:OnMonsterEnterWorld(monsterId, entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        if v[2] == monsterId then
            local monster = GameWorld.GetEntity(entityId)
            if monster ~= nil then
                self._hasPlayHpValue[entityId] = {}
                monster:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onMonsterCurHpChanged)
            end
        end
    end
end

function SpecialDialogManager:OnMonsterLeaveWorld(monsterId, entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        if v[2] == monsterId then
            local monster = GameWorld.GetEntity(entityId)
            if monster ~= nil then
                self._hasPlayHpValue[entityId] = nil
                monster:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onMonsterCurHpChanged)
            end
        end
    end
end

function SpecialDialogManager:OnMonsterCurHpChanged(entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        local monster = GameWorld.GetEntity(entityId)
        if monster ~= nil then
            if self._hasPlayHpValue[entityId][sddId] == nil
                and (monster.cur_hp or 0) / (monster.max_hp or 1) <= v[3]
                and monster.monster_id == v[2] then
                if self:CheckTrigger(sddId) == true then
                    self._hasPlayHpValue[entityId][sddId] = sddId
                end
            end
        end
    end
end

function SpecialDialogManager:OnNpcEnterWorld(npcId, entityId)
    -- LoggerHelper.Log("Ash: OnNpcEnterWorld: " .. npcId .. " " .. entityId)
    for sddId, v in pairs(self._distanceTrigger) do
        if SpecialDialogDataHelper:GetNpcId(sddId) == npcId then
            self._distanceTriggerEntites[sddId] = {}
            table.insert(self._distanceTriggerEntites[sddId], entityId)
            table.insert(self._distanceTriggerEntites[sddId], v[1])
            table.insert(self._distanceTriggerEntites[sddId], v[2])
        end
    end
end

function SpecialDialogManager:OnNpcLeaveWorld(npcId, entityId)
    for sddId, v in pairs(self._distanceTrigger) do
        if SpecialDialogDataHelper:GetNpcId(sddId) == npcId then
            self._distanceTriggerEntites[sddId] = nil
            self._entityInDistanceTimer[sddId] = nil
        end
    end
end

function SpecialDialogManager:OnTick()
    self._playerPos = nil
    for sddId, v in pairs(self._distanceTriggerEntites) do
        local ety = GameWorld.GetEntity(v[1])
        if ety ~= nil then
            if self._playerPos == nil then
                self._playerPos = GameWorld.Player():GetPosition()
            end
            if Vector3.Distance(ety:GetPosition(), self._playerPos) < v[2] then -- v[2] 距离
                if self._entityInDistanceTimer[sddId] == nil then
                    self._entityInDistanceTimer[sddId] = 0
                    self:CheckTrigger(sddId)
                else
                    if self._entityInDistanceTimer[sddId] >= v[3] then -- v[3] 重复周期时间
                        self._entityInDistanceTimer[sddId] = 0
                        self:CheckTrigger(sddId)
                    end
                end
            end
            if self._entityInDistanceTimer[sddId] ~= nil then
                self._entityInDistanceTimer[sddId] = self._entityInDistanceTimer[sddId] + 1
            end
        end
    end
end

function SpecialDialogManager:OnDialogFinish(dialogId)
    for sddId, v in pairs(self._dialogFinish) do
        if v == dialogId then
            return self:CheckTrigger(sddId, true)
        end
    end
    return false
end

SpecialDialogManager:Init()
return SpecialDialogManager
