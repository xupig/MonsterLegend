local DailyConsumeManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DailyConsumeDataHelper = GameDataHelper.DailyConsumeDataHelper
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function DailyConsumeManager:Init()
    self._refreshDailyConsumeRewardState = function () self:RefreshDailyConsumeRewardState() end
    self._refreshChannelActivityTimes = function () self:RefreshChannelActivityTimes() end
    self._closeActivity = function() self:CloseActivity() end
    self._on_0_Oclock = function()self:OnPassDay() end
    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
end

function DailyConsumeManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Consume_Reward_State, self._refreshDailyConsumeRewardState)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Channel_Activity_Times, self._refreshChannelActivityTimes)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    
    self._showDailyConsumeRewardRedPoint = false
    self:RefreshDailyConsumeRewardState()
    self:RefreshChannelActivityTimes()
    if self:IsOpen() then
        self:OpenActivity()
    else
        self:CloseActivity()
    end
end

function DailyConsumeManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Consume_Reward_State, self._refreshDailyConsumeRewardState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Channel_Activity_Times, self._refreshChannelActivityTimes)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function DailyConsumeManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("DailyConsumeManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    end
    if action_id == action_config.DAILY_CONSUME_REWARD then

    end
end
-------------------------------------数据处理------------------------------------

-------------------------------------接口------------------------------------
function DailyConsumeManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_DAILYCONSUME then
        self:RefreshDailyConsumeRewardState()
        self:RefreshChannelActivityTimes()
    end
end

function DailyConsumeManager:IsOpen()
    local isFuncOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DAILYCONSUME)
    if not isFuncOpen then
        return false
    end
    local data = GameWorld.Player().channel_activity_times
    local now = DateTimeUtil.GetServerTime()
    if data ~= nil and data[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME] ~= nil and 
        data[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME][public_config.CHANNEL_ACTIVITY_KEY_END_TIME] >= now 
        and data[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME][public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME] <= now then
        return true
    end
    return false
end

function DailyConsumeManager:RefreshChannelActivityTimes()
    if self:IsOpen() then
        if self._timer ~= nil then
            DateTimeUtil.DelTimeCallback(self._timer)
            self._timer = nil
        end
        local endTime = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME][public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
        if DateTimeUtil.SameDay(endTime) then
            local date = DateTimeUtil.SomeDay(endTime)
            self._timer = DateTimeUtil.AddTimeCallback(date.hour, date.min, date.sec, false, self._closeActivity)
        end
        self:OpenActivity()
    else
        local serverData = GameWorld.Player().channel_activity_times
        if serverData ~= nil and serverData[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME] ~= nil then
            local startTime = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME][public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME] or 0
            startTime = startTime + 5
            local now = DateTimeUtil.GetServerTime()
            if DateTimeUtil.SameDay(startTime) and now < startTime then
                local date = DateTimeUtil.SomeDay(startTime)
                if self._closeTimer ~= nil then
                    DateTimeUtil.DelTimeCallback(self._closeTimer)
                    self._closeTimer = nil
                end
                self._closeTimer = DateTimeUtil.AddTimeCallback(date.hour, date.min, date.sec, false, self._refreshChannelActivityTimes)
            end
        end
        self:CloseActivity()
    end
end

function DailyConsumeManager:OpenActivity()
    if self:IsOpen() then
        --local endTime = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_DAILY_CUNSUME][public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.DailyConsume, function()
            GUIManager.ShowPanel(PanelsConfig.DailyConsume) end)
        if self._closeTimer ~= nil then
            DateTimeUtil.DelTimeCallback(self._closeTimer)
            self._closeTimer = nil
        end
    end
end

function DailyConsumeManager:CloseActivity()
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.DailyConsume)
    if self._timer ~= nil then
        DateTimeUtil.DelTimeCallback(self._timer)
        self._timer = nil
    end
end

function DailyConsumeManager:OnPassDay()
    self:RefreshChannelActivityTimes()
end

-----------------------------------红点------------------------------
function DailyConsumeManager:RefreshDailyConsumeRewardState(type)
    local result = false
    local cfg = GlobalParamsHelper.GetParamValue(897)
    for k,v in pairs(cfg) do
        if GameWorld.Player().daily_consume_record[v] == 0 then
            result = true
            break
        end
    end

    for i=1,#DailyConsumeDataHelper:GetAllCumulativeDailyConsumeData() do
        if GameWorld.Player().cumulative_consume_record[i] == 0 then
            result = true
            break
        end
    end

    self:SetDailyConsumeRewardRedPoint(result)
end

function DailyConsumeManager:SetDailyConsumeRewardRedPoint(state)
    if self._showDailyConsumeRewardRedPoint ~= state then
        self._showDailyConsumeRewardRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_DAILYCONSUME, self:GetDailyConsumeRewardRedPoint())
    end
end

function DailyConsumeManager:GetDailyConsumeRewardRedPoint()
    return self._showDailyConsumeRewardRedPoint
end
-------------------------------------客户端请求-----------------------------------
function DailyConsumeManager:GetConsumeReward(cost)
    local params = string.format("%d", cost)
    GameWorld.Player().server.daily_consume_action_req(action_config.DAILY_CONSUME_REWARD, params)
end

function DailyConsumeManager:GetConsumeCumulativeReward(day)
    local params = string.format("%d", day)
    GameWorld.Player().server.daily_consume_action_req(action_config.DAILY_CONSUME_CUMULATIVE_REWARD, params)
end

DailyConsumeManager:Init()
return DailyConsumeManager
