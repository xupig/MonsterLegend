local BossHomeManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local bossHomeData = PlayerDataManager.bossHomeData
local BossHomeDataHelper = GameDataHelper.BossHomeDataHelper
local GameSceneManager = GameManager.GameSceneManager
local ClientEntityManager = GameManager.ClientEntityManager
local BossType = GameConfig.EnumType.BossType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function BossHomeManager:Init()
    self:InitCallbackFunc()
end

function BossHomeManager:InitCallbackFunc()
    self._onEnterMap = function(mapId) self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId) self:OnLeaveMap(mapId) end
end

function BossHomeManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function BossHomeManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end


function BossHomeManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then
        TaskCommonManager:SetCurAutoTask(nil, nil)
    end    
end

function BossHomeManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME then
    end
end

function BossHomeManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("BossHomeManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        if action_id == action_config.BOSS_HOME_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.BOSS_HOME_ENTER)
            EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Enter, false)
        end
        return
    end
    if action_id == action_config.BOSS_HOME_GET_DATA then
        self:RefreshBossData(args)
    elseif action_id == action_config.BOSS_HOME_GET_KILLER_RECORDS then
        self:RefreshGetKillerRecords(args)
    elseif action_id == action_config.BOSS_HOME_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.BOSS_HOME_ENTER)
        self:BossHomeEnter(args)
    elseif action_id == action_config.BOSS_HOME_SPACE_BOSS_INFO then
        self:BossHomeSpaceBossInfo(args)
        ClientEntityManager.bossargs={args,BossType.BossHome}
        ClientEntityManager.UpdateBossTomb(args,BossType.BossHome)
    elseif action_id == action_config.BOSS_HOME_NOTIFY_BOSS_WILL_REBORN then
        self:BossHomeNotifyBossWillReborn(args)
    end
end

function BossHomeManager:RefreshBossHomeInterestInfo(changeValue)
    bossHomeData:RefreshBossHomeInterestInfo(changeValue)
end

-------------------------------------数据处理------------------------------------
function BossHomeManager:RefreshBossData(data)
    bossHomeData:RefreshBossData(data)
    EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Get_Info)
end


function BossHomeManager:GetAllBossInfoByFloor(floor)
    return bossHomeData:GetAllBossInfoByFloor(floor)
end

function BossHomeManager:GotoBossById(bossId)
    self:GotoBoss(self:GetBossInfo(bossId))
end

function BossHomeManager:GotoBossByMapId(mapId, pos)
    local id = BossHomeDataHelper:GetMonsterByMapId(mapId)
    if id then
        local boss = self:GetBossInfo(id)
        local levelLimit = boss:GetLevelLimit()
        local vipLevelLimit = boss:GetVIPLevelLimit()
        local minVipLevel = GlobalParamsHelper.GetParamValue(494)
        if GameWorld.Player().vip_level < vipLevelLimit then
            if GameWorld.Player().vip_level < minVipLevel then
                GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56039))
            else
                GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(56038, {["0"] = boss:GetVIPLevelMoney()}), 
                function() self:GotoBoss(boss, pos) end, nil)
            end
        elseif levelLimit > GameWorld.Player().level then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(56016) .. levelLimit .. LanguageDataHelper.CreateContent(56017) .. boss:GetSenceName(), 1)
        else
            self:GotoBoss(boss, pos)
        end
    end
end

function BossHomeManager:GotoBoss(bossData, targetPos)
    local cb = function() self:ConfirmGotoBoss(bossData, targetPos) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function BossHomeManager:ConfirmGotoBoss(bossData, targetPos)
    local pos = targetPos or GameSceneManager:GetMonsterPos(bossData:GetSenceId(), bossData:GetBossRegionId())
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBossHomeSceneKillMonsterPoint,
        bossData:GetId(), bossData:GetSenceId(), pos, 5)
end

function BossHomeManager:GetBossTire()
    return bossHomeData:GetBossTire()
end

function BossHomeManager:RefreshGetKillerRecords(args)
    EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Get_Killer_Records, args)
end

function BossHomeManager:RefreshBossHomeInterestInfo(args)
    bossHomeData:RefreshBossHomeInterestInfo(args)
end

function BossHomeManager:BossHomeEnter(args)
    EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Enter, true)
end

function BossHomeManager:BossHomeSpaceBossInfo(args)
    bossHomeData:RefreshBossHomeSpaceBossInfo(args)
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossSpaceBossInfo)
end

function BossHomeManager:GetAllSpaceBossInfo()
    return bossHomeData:GetAllSpaceBossInfo()
end

function BossHomeManager:BossHomeNotifyBossWillReborn(args)
    local bossInfo = self:GetBossInfo(args[1])
    GUIManager.ShowPanel(PanelsConfig.WorldBossMainUI, bossInfo)
end

function BossHomeManager:GetBossInfo(id)
    return bossHomeData:GetBossInfo(id)
end

function BossHomeManager:GetFloorBossId(bossId)
    local result = 0
    local showCount = GlobalParamsHelper.GetParamValue(624)
    for floor=1,showCount do
        local bossInfos = self:GetAllBossInfoByFloor(floor)
        for i, v in ipairs(bossInfos) do
            if v:GetId() == bossId then
                result = floor
                break
            end
        end
        if result ~= 0 then
            break
        end
    end
    return result
end

-------------------------------------客户端请求-----------------------------------
function BossHomeManager:OnGetData(floor)
    local params = string.format("%d", floor)
    GameWorld.Player().server.boss_home_action_req(action_config.BOSS_HOME_GET_DATA, params)
end

function BossHomeManager:OnBossEnter(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.BOSS_HOME_ENTER)
    local params = string.format("%d", id)
    GameWorld.Player().server.boss_home_action_req(action_config.BOSS_HOME_ENTER, params)
end

function BossHomeManager:OnBossIntrest(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.boss_home_action_req(action_config.BOSS_HOME_INTREST, params)
end

function BossHomeManager:OnBossGetKillerRecords(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.boss_home_action_req(action_config.BOSS_HOME_GET_KILLER_RECORDS, params)
end

BossHomeManager:Init()
return BossHomeManager
