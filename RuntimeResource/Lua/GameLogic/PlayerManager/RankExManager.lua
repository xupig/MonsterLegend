local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local RankConfig = GameConfig.RankConfig
local rankListData = PlayerManager.PlayerDataManager.rankListData

local RankExManager = {}

function RankExManager:Init()

end

function RankExManager:OnPlayerEnterWorld()
end

function RankExManager:OnPlayerLeaveWorld()
end

function RankExManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("RankExManager Error: error_id==" .. error_id .. "  action_id ==" .. action_id)
        return
    elseif action_id == action_config.RANK_LIST_GET_REQ then           --3003, --统一请求排行榜
        -- LoggerHelper.Error("排行榜数据 ======" .. PrintTable:TableToStr(args))
        rankListData:ResetData(args)
        EventDispatcher:TriggerEvent(GameEvents.RANK_LIST_GET_RESP)
    else
        LoggerHelper.Error("RankExManager HandleRewardBackData action id not handle:" .. action_id)
    end
end

function RankExManager:GetRankList()
    return RankConfig.RANK_LIST
end

function RankExManager:GetRankContentListData()
    return rankListData:GetRankListData()
end

function RankExManager:GetMyRank()
   return rankListData:GetMyRank()
end

--3003, --统一请求排行榜
function RankExManager:GetRank(rankType)
    GameWorld.Player().server.rank_list_action_req(action_config.RANK_LIST_GET_REQ, tostring(rankType))
end

RankExManager:Init()
return RankExManager