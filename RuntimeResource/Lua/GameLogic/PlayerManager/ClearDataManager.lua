
CacheNames = {}
local mt={		   
			__newindex = function(t, k, v)
			end
			,
			__index = function(t, k)
				return k
			end
			}
setmetatable(CacheNames, mt)

ClearDataManager = {}

local caches = {}
local removeCacheNames = {}
local perCheckTime = 30
local removeTime = 120
local timerHeapCheckTime = 100


function ClearDataManager:Init()
    if GameWorld.isEditor then
        removeTime = 30
    end
    self:InitCallbackFunc()    
end

function ClearDataManager:InitCallbackFunc()
    self._initServerTime = function()self:OnInitServerTimeComplete() end
end

function ClearDataManager:OnInitServerTimeComplete()
    EventDispatcher:RemoveEventListener(GameEvents.INIT_SERVER_TIME, self._initServerTime)
    self:ResetTime()
    self:AddTimer()
end

function ClearDataManager:ResetTime()
    for k,v in pairs(caches) do
        if v and v.time then
            v.time = DateTimeUtil.GetServerTime()
        end
    end
end

function ClearDataManager:OnPlayerEnterWorld()
    self:AddTimer()
    -- if DateTimeUtil.GetIsInitTime() then
    --     self:AddTimer()
    -- else
    --     EventDispatcher:AddEventListener(GameEvents.INIT_SERVER_TIME, self._initServerTime)
    -- end
end

function ClearDataManager:OnPlayerLeaveWorld()
    self:RemoveTimer()
    caches = {}
end

function ClearDataManager:RemoveTimer()
    if self._cacheTimer  then
        TimerHeap.DelTimer(self._cacheTimer)
        self._cacheTimer = nil
    end
    self:RemoveTimerHeapCheckTimer()
end

function ClearDataManager:RemoveTimerHeapCheckTimer()
    if self._timerHeapCheckTimer  then
        TimerHeap.DelTimer(self._timerHeapCheckTimer)
        self._timerHeapCheckTimer = nil
    end
end

function ClearDataManager:AddTimer()
    self._cacheTimer = TimerHeap:AddSecTimer(perCheckTime, perCheckTime, 0, function()
        removeCacheNames = {}
        local now = DateTimeUtil.GetServerTime()
        for k,v in pairs(caches) do
            if now - v.time > removeTime then
                if v.cb then
                    v.cb()
                end
                table.insert(removeCacheNames, k)
            end
        end
        
        for i,v in ipairs(removeCacheNames) do
            caches[v] = nil
        end
    end)

    self:RemoveTimerHeapCheckTimer()
    self._timerHeapCheckTimer = TimerHeap:AddSecTimer(timerHeapCheckTime, timerHeapCheckTime, 0, function()
        TimerHeap:ClearData()
    end)
end

function ClearDataManager:UseCache(tableName, cb)
    if caches[tableName] ~= nil then
        caches[tableName].time = DateTimeUtil.GetServerTime()
    else
        local data = {}
        data.time = DateTimeUtil.GetServerTime()
        data.cb = cb
        caches[tableName] = data
    end
end

function ClearDataManager:ClearCache()
    local num = 0
    for k,v in pairs(caches) do
        if v and v.cb then
            v.cb()
            num = num + 1
        end
    end    
    caches = {}
end

ClearDataManager:Init()
return ClearDataManager