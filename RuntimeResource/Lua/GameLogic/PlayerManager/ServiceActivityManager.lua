local ServiceActivityManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local LordRingDetailDataHelper = GameDataHelper.LordRingDetailDataHelper
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local error_code = GameWorld.error_code
local DailyConfig = GameConfig.DailyConfig
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local WordCollectionDataHelper = GameDataHelper.WordCollectionDataHelper
local ServiceActivityConfig = GameConfig.ServiceActivityConfig
local OpenRankType = GameConfig.EnumType.OpenRankType
local LordRingType = GameConfig.EnumType.LordRingType
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local OpenActivityType = GameConfig.EnumType.OpenActivityType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local BagData = PlayerManager.PlayerDataManager.bagData
local GuildCallDataHelper = GameDataHelper.GuildCallDataHelper


function ServiceActivityManager:Init()
    self:InitCallbackFunc()
end


function ServiceActivityManager:HandleResp(act_id, code, args)
    if code == 0 then
        if act_id == action_config.OPEN_SERVER_RANK_GET_REWARD then
            self:GetOpenSeverGetRewardResp(args)
            self:RefreshRedPointForOpenRank()
        elseif act_id == action_config.OPEN_SERVER_RANK_GET_INFO then
            self:GetOpenSeverGetInfoResp(args)
            self:RefreshRedPointForOpenRank()
        elseif act_id == action_config.MARRY_RANK_GET_RANK_LIST_REQ then
            self:GetMarryRankGetInfoResp(args)
        elseif act_id == action_config.DEMON_RING_SUB_TARGET_REWARD_REQ then
            --魔戒寻主 目标奖励
            self:GetRingTargetRewardResp(args)
            self:SendRingGetInfoReq()
            self:RefreshRedPointForRing()
        elseif act_id == action_config.DEMON_RING_UNLOCK_REQ then
            --魔戒寻主 目标奖励
            self:GetRingUnlockResp(args)
            self:RefreshRedPointForRing()
        elseif act_id == action_config.DEMON_RING_GET_INFO_REQ then
            --魔戒寻主 目标奖励
            self:GetRingProgressResp(args)
            self:RefreshRedPointForRing()
        elseif act_id == action_config.COLLECT_WORD_EXCHANGE then
            --集字有礼
            self:GetCollectWordResp(args)
            self:WordRedUpdate()
        elseif act_id == action_config.COLLECT_WORD_RECORD then
            --集字有礼
            self:GetAllSerNumResq(args)
            self:WordRedUpdate()
        elseif act_id == action_config.DEMON_RING_REFRESH then
            --魔戒寻主 状态刷新
            self:GetRingState(args)
        elseif act_id == action_config.OPEN_SERVER_RANK_BUY then
            EventDispatcher:TriggerEvent(GameEvents.GetOpenBuy)
        elseif act_id == action_config.OPEN_SERVER_RANK_GET_RANK_LIST then
            self:GetRankListInfoResp(args)
        elseif act_id == action_config.FRAUD_GUILD_REWARD_REQ then --领奖
            self:GetRewardSectResp(args)
        elseif act_id == action_config.FRAUD_GUILD_GET_INFO_REQ then --获取任务状态
            self:GetRewardStateSectResp(args)
        elseif act_id == action_config.FRAUD_GUILD_GET_RECEIVED_INFO_REQ then --获取限制数量已领信息
            self:GetRewardInfoSectResp(args)
        elseif act_id == action_config.FRAUD_GUILD_REFRESH then  --任务完成状态更新
            self:GetTaskInfoSectResp(args)
        elseif act_id == action_config.MARRY_RANK_GET_RANK_INFO_REQ then
            self:GetLoveInfoResp(args)
        end
    end
end



function ServiceActivityManager:GetRingState(args)
    for k, v in pairs(args) do
        if v[2] == 2 then
            local type = LordRingDetailDataHelper:GetType(k)
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, type == LordRingType.Initial)
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, type == LordRingType.Chaos)
            --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, false)
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, type == LordRingType.Guard)
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 620, type == LordRingType.Power)
        end
    end
    
    --LoggerHelper.Error("self.GetRingState" .. PrintTable:TableToStr(args))
end

function ServiceActivityManager:InitCallbackFunc()
    self._onOpenSvrClick = function(follow)self:DoFollowEvent(follow) end
    self._onOpenSvrMapOpen = function()self:OnDailyMapOpen() end
    self._onCheckStatus = function()self:CheckStatus() end
    self._checkRingOpenCb = function (id)
    	if id == public_config.FUNCTION_ID_DEMON_RING then
    		self:CheckStatus()
    	end
    end
    
    self._freshWordRed = function()self:WordRedUpdate() end
end

function ServiceActivityManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.On_OpenSvr_Click, self._onOpenSvrClick)
    EventDispatcher:AddEventListener(GameEvents.OpenSvr_Map_Open, self._onOpenSvrMapOpen)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkRingOpenCb)

    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onCheckStatus)
    
    --LoggerHelper.Error("DateTimeUtil.GetServerOpenDays()"..DateTimeUtil.GetServerOpenDays())
    self._timer = DateTimeUtil.AddTimeCallback(23, 0, 20, true, self._onCheckStatus)
    self._timerWord = DateTimeUtil.AddTimeCallback(23, 0, 5, true, self._freshWordRed)
    self._timerHappy = DateTimeUtil.AddTimeCallback(24, 0, 0, true, self._onCheckStatus)
    self:CheckStatus()
    self:SendRingGetInfoReq()
    self:RefreshRedPointForRing()
    self:WordRedUpdate()

    for i = OpenRankType.Level, OpenRankType.Combat do
        self:SendOpenSeverGetInfoReq(i)
    end

end


function ServiceActivityManager:WordRedUpdate()
    if self:GetIsOpenWord() then
        local isRed = self:RefreshWordRed()
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_COLLECT_WORD, isRed)
    else
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_COLLECT_WORD, false)
    end 
end


function ServiceActivityManager:OnPlayerLeaveWorld()
    DateTimeUtil.DelTimeCallback(self._timer)
    DateTimeUtil.DelTimeCallback(self._timerWord)
    DateTimeUtil.DelTimeCallback(self._timerHappy)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onCheckStatus)
    EventDispatcher:RemoveEventListener(GameEvents.On_OpenSvr_Click, self._onOpenSvrClick)
    EventDispatcher:RemoveEventListener(GameEvents.OpenSvr_Map_Open, self._onOpenSvrMapOpen)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkRingOpenCb)
end

function ServiceActivityManager:DoFollowEvent(follow)
    if not follow or type(follow) ~= "table" then
        LoggerHelper.Error("Invalid Event follow :" .. tostring(follow))
        return
    end
    
    if follow[1] == ServiceActivityConfig.FOLLOW_OPEN_PANEL then -- 1.打开界面，填功能ID
        if FunctionOpenDataHelper:CheckFunctionOpen(follow[2][1]) then
            GUIManager.ShowPanelByFunctionId(follow[2][1])
        else
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = LanguageDataHelper.CreateContent(FunctionOpenDataHelper:GetFunctionOpenName(follow[2][1]))
            --argsTable["1"] = FunctionOpenDataHelper:GetCondition(follow[2][1])[2][1]
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(58719, argsTable))
        --LoggerHelper.Error("follow[2][1]"..follow[2][1])
        --LoggerHelper.Error("follow"..PrintTable:TableToStr(FunctionOpenDataHelper:GetCondition(follow[2][1])))
        end
    
    elseif follow[1] == ServiceActivityConfig.FOLLOW_EVENT then -- 2.事件,填事件名称
        EventDispatcher:TriggerEvent(GameEvents[follow[2][1]])
    elseif follow[1] == ServiceActivityConfig.FOLLOW_GUI_Open then
        local data = {["id"] = follow[2][1]}
        GUIManager.ShowPanel(PanelsConfig.Shop, data)
    end
end

function ServiceActivityManager:OnDailyMapOpen()
    GUIManager.ShowPanel(PanelsConfig.WorldMap, nil, nil)
end

function ServiceActivityManager:CheckStatus()
    self:UpdateOpenRank()
    self:WordRedUpdate()
    PlayerManager.OpenHappyManager:UpdateRedPoint()
    
    EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, OpenActivityType.Ring,FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DEMON_RING))

    EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, OpenActivityType.Rank,self:GetIsOpenRanking() and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OPEN_RANK_LEVEL))

    if self:GetIsOpenRanking() and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OPEN_RANK_LEVEL) then
        self._openFun = function() GUIManager.ShowPanel(PanelsConfig.OpenRank) end
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.OpenRank,self._openFun)
    else
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OpenRank)
    end

    local isHappyAdd = self:GetIsOpenHappy() or self:GetIsOpenWord() or self:GetIsOpenLoveOver() or self:GetIsOpenActivity() or self:GetIsOpenSect()
    if isHappyAdd==true then
        --LoggerHelper.Error("isHappyAddtrue")
        if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_COLLECT_WORD) then
            EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, OpenActivityType.Happy,true)
            self._openOtherFun = function() GUIManager.ShowPanel(PanelsConfig.OpenOther) end
            EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.OpenOther,self._openOtherFun)

        end
    end
    
    if self:GetIsOpenHappy() == false then
        --LoggerHelper.Error("isHappyAddfalse=========================================")
    end




    local isHappyRemove = self:GetIsOpenHappy() == false and self:GetIsOpenWord() == false and self:GetIsOpenLoveOver() == false and self:GetIsOpenActivity() == false and self:GetIsOpenSect() == false
    if isHappyRemove==true then
        EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, OpenActivityType.Happy,false)
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.OpenOther)
    end

    if (self:GetIsOpenRanking()==true and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_OPEN_RANK_LEVEL)==true) or FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DEMON_RING) or self:GetIsOpenActivity() 
    or self:GetIsOpenLoveOver() or self:GetIsOpenWord() or self:GetIsOpenHappy() then
        --显示ICON
        --需要显示
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.Activity, function(position)
            GUIManager.ShowPanel(PanelsConfig.OpenActivity,position)
        end)
    else
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Activity)
    end
end


function ServiceActivityManager:ShowMenu()

end

-- function ServiceActivityManager:ShowMenu()
--     self._openFun = function() GUIManager.ShowPanel(PanelsConfig.OpenOther) end
-- 	EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.OpenOther,self._openFun)
-- end


--请求排行榜返回
function ServiceActivityManager:GetRank(rankType)
    --LoggerHelper.Error("rankType:"..rankType)
    GameWorld.Player().server.open_server_rank_action_req(action_config.OPEN_SERVER_RANK_GET_RANK_LIST, tostring(rankType))
end

--请求排行榜返回
function ServiceActivityManager:GetRankListInfoResp(args)

end

--完美情人
function ServiceActivityManager:SendMarryRankReq()
    GameWorld.Player().server.marry_rank_action_req(action_config.MARRY_RANK_GET_RANK_LIST_REQ, "")
end

--完美情人
function ServiceActivityManager:SendLoveInfoReq()
    GameWorld.Player().server.marry_rank_action_req(action_config.MARRY_RANK_GET_RANK_INFO_REQ, "")
end

--完美情人
function ServiceActivityManager:GetLoveInfoResp(args)
    ServiceActivityData:GetLoveInfoResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetLoveInfoResp)
end



--完美情人
function ServiceActivityManager:SendGetRewardLoveReq()
    GameWorld.Player().server.marry_rank_action_req(action_config.MARRY_RANK_PERFECT_LOVER_HONOR_REQ, "")
end

function ServiceActivityManager:GetMarryRankGetInfoResp(args)
    ServiceActivityData:GetMarryRankGetInfoResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetMarryRankGetInfoResp)
end

--集字有礼
function ServiceActivityManager:GetCollectWordTime()
    return GameWorld.Player().collect_word_exchange_times or {}
end

function ServiceActivityManager:SendCollectWordReq(id)
    GameWorld.Player().server.collect_word_action_req(action_config.COLLECT_WORD_EXCHANGE, tostring(id))
end

function ServiceActivityManager:GetCollectWordResp(args)
    self:SendAllSerNumReq()
    EventDispatcher:TriggerEvent(GameEvents.GetCollectWordResp)
end

function ServiceActivityManager:SendAllSerNumReq()
    GameWorld.Player().server.collect_word_action_req(action_config.COLLECT_WORD_RECORD, "")
end

function ServiceActivityManager:GetAllSerNumResq(args)
    ServiceActivityData:GetAllSerNumResq(args)
    EventDispatcher:TriggerEvent(GameEvents.GetAllSerNumReq)
end

function ServiceActivityManager:BuyOpenItem(id,count)
    local params = string.format("%d,%d", id, count)
    GameWorld.Player().server.open_server_rank_action_req(action_config.OPEN_SERVER_RANK_BUY, params)
end

------------------------------------------开服冲榜-------------------------------------------------------------------------
--开服冲榜请求 领取奖励
function ServiceActivityManager:SendOpenSeverGetRewardReq(type)
    GameWorld.Player().server.open_server_rank_action_req(action_config.OPEN_SERVER_RANK_GET_REWARD, tostring(type))
end

--开服冲榜请求  请求信息
function ServiceActivityManager:SendOpenSeverGetInfoReq(type)
    --LoggerHelper.Error("ServiceActivityManager:SendOpenSeverGetInfoReq" .. type)
    GameWorld.Player().server.open_server_rank_action_req(action_config.OPEN_SERVER_RANK_GET_INFO, tostring(type))

end
--------------------------------------------------------------------------------------------------------------------------
------------------------------------------开服冲榜返回-------------------------------------------------------------------------
--开服冲榜请求 领取奖励
function ServiceActivityManager:GetOpenSeverGetRewardResp(args)
    self:SendOpenSeverGetInfoReq(ServiceActivityData:GetCurOpenType())
end

--开服冲榜请求  请求信息
function ServiceActivityManager:GetOpenSeverGetInfoResp(args)
    --LoggerHelper.Error("ServiceActivityManager:GetOpenSeverGetInfoResp" .. PrintTable:TableToStr(args))
    ServiceActivityData:GetOpenSeverGetInfoResp(args)
    
    EventDispatcher:TriggerEvent(GameEvents.GetOpenSeverGetInfoResp, args[1])
end
--------------------------------------------------------------------------------------------------------------------------
--开宗立派
 --领奖
function ServiceActivityManager:SendGetRewardSectReq(id)
    GameWorld.Player().server.fraud_guild_action_req(action_config.FRAUD_GUILD_REWARD_REQ, tostring(id))
end

 --获取任务状态
function ServiceActivityManager:SendRewardStateSectReq()
    GameWorld.Player().server.fraud_guild_action_req(action_config.FRAUD_GUILD_GET_INFO_REQ,"")
end

--获取限制数量已领信息
function ServiceActivityManager:SendRewardInfoSectReq()
    GameWorld.Player().server.fraud_guild_action_req(action_config.FRAUD_GUILD_GET_RECEIVED_INFO_REQ,"")
end


function ServiceActivityManager:GetRewardSectResp(args)
    --LoggerHelper.Error("ServiceActivityManager:GetRewardSectResp"..PrintTable:TableToStr(args))
end

function ServiceActivityManager:GetRewardStateSectResp(args)
    ServiceActivityData:GetRewardStateSectResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetRewardStateSectResp)
    --LoggerHelper.Error("ServiceActivityManager:GetRewardStateSectResp"..PrintTable:TableToStr(args))
end

function ServiceActivityManager:GetRewardInfoSectResp(args)
    ServiceActivityData:GetRewardInfoSectResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetRewardInfoSectResp)
    --LoggerHelper.Error("ServiceActivityManager:GetRewardInfoSectResp"..PrintTable:TableToStr(args))
end

--任务完成状态更新
function ServiceActivityManager:GetTaskInfoSectResp(args)
    ServiceActivityData:GetTaskInfoSectResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetTaskInfoUpdateSectResp)
    for k, v in pairs(args) do
        if v[2] == 2 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SECT, true)
            return 
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SECT, false)
    --LoggerHelper.Error("ServiceActivityManager:GetTaskInfoSectResp"..PrintTable:TableToStr(args))
end









------------------------------------------魔戒寻主-------------------------------------------------------------------------
--魔戒寻主 目标奖励
function ServiceActivityManager:SendGetTargetRewardReq(id)
    GameWorld.Player().server.demon_ring_action_req(action_config.DEMON_RING_SUB_TARGET_REWARD_REQ, tostring(id))
end

--魔戒寻主 解锁戒指
function ServiceActivityManager:SendUnlockRingReq(index)
    if index == LordRingType.Power then
        GameWorld.Player().server.demon_ring_action_req(action_config.DEMON_RING_UNLOCK_REQ, tostring(index))
    else
        GameWorld.Player().server.demon_ring_action_req(action_config.DEMON_RING_UNLOCK_REQ, "")
    end
end

--魔戒寻主 获取进度信息
function ServiceActivityManager:SendRingGetInfoReq()
    GameWorld.Player().server.demon_ring_action_req(action_config.DEMON_RING_GET_INFO_REQ, "")
end

--魔戒寻主解锁阶段
function ServiceActivityManager:GetRingUnlockStep()
    return GameWorld.Player().demon_ring_unlocked_step or {}
end



function ServiceActivityManager:GetRingTargetRewardResp(args)
    --LoggerHelper.Log("ServiceActivityManager:GetRingTargetRewardResp:" .. PrintTable:TableToStr(args))
    ServiceActivityData:GetRingTargetRewardResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetRingTargetRewardResp)
end

function ServiceActivityManager:GetRingUnlockResp(args)
    --LoggerHelper.Log("ServiceActivityManager:GetRingUnlockResp:" .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.GetRingUnlockResp)
end

function ServiceActivityManager:GetRingProgressResp(args)
    --LoggerHelper.Log("ServiceActivityManager:GetRingProgressResp:" .. PrintTable:TableToStr(args))
    ServiceActivityData:GetRingProgressResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetRingProgressResp)
end


--------------------------------------------------------------------------------------------------------------------------
function ServiceActivityManager:UpdateOpenRank(index)
    self._openRank = {}
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    self._serTimeSetting = GlobalParamsHelper.GetParamValue(758)
    self._serTimeOver = GlobalParamsHelper.GetParamValue(759)
    local secOver = self:GetOpenDiff(self._serTimeOver[1], self._serTimeOver[2])
    for i = OpenRankType.Level, OpenRankType.Combat do
        local daysOpen = self._serTimeSetting[i][1]
        local hoursOpen = self._serTimeSetting[i][2]
        local daysClose = self._serTimeSetting[i][3]
        local hoursClose = self._serTimeSetting[i][4]
        
        local secOpen = self:GetOpenDiff(daysOpen, hoursOpen)
        local secClose = self:GetOpenDiff(daysClose, hoursClose)
        
        
        if timeDiff >= secOver then
            --开服活动关闭
            self:SetIsOpenRanking(false)
            self._openRank[i] = -1
        else
            --开服活动开启
            self:SetIsOpenRanking(true)
            if timeDiff >= secClose then
                --活动已结束
                self._openRank[i] = 0

            else
                if timeDiff >= secOpen then
                    --活动倒计时
                    self._openRank[i] = secClose - timeDiff
                else
                    --活动未开始
                    self._openRank[i] = -1
                end
            end
        end
    end
end

function ServiceActivityManager:GetIsOpenActivity()
    self._serTimeGuildOver = GlobalParamsHelper.GetMSortValue(819)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOver = self:GetOpenDiff(self._serTimeGuildOver[1][1], self._serTimeGuildOver[1][2])
    if timeDiff >= secOver then
        --活动关闭
        return false
    else
        return true
    end
end

function ServiceActivityManager:GetIsOpenLoveOver()
    self._serTimeLoveOver = GlobalParamsHelper.GetMSortValue(820)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOver = self:GetOpenDiff(self._serTimeLoveOver[1][1], self._serTimeLoveOver[1][2])
    if timeDiff >= secOver then
        --活动关闭
        return false
    else
        return true
    end
end

function ServiceActivityManager:GetIsOpenWord()
    self._serTimeOver = GlobalParamsHelper.GetMSortValue(821)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOver = self:GetOpenDiff(self._serTimeOver[1][1], self._serTimeOver[1][2])
    -- LoggerHelper.Error("timeDiff"..timeDiff)
    -- LoggerHelper.Error("secOver"..secOver)
    if timeDiff >= secOver then
        --活动关闭
        return false

    else
        return true
    end
end


function ServiceActivityManager:GetIsOpenSect()
    self._serTimeOver = GlobalParamsHelper.GetMSortValue(974)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOver = self:GetOpenDiff(self._serTimeOver[1][1], self._serTimeOver[1][2])
    -- LoggerHelper.Error("timeDiff"..timeDiff)
    -- LoggerHelper.Error("secOver"..secOver)
    if timeDiff >= secOver then
        --活动关闭
        return false

    else
        return true
    end
end

function ServiceActivityManager:GetIsOpenHappy()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FREAK) then
        return false
    end
    local serTimeHappy = GlobalParamsHelper.GetMSortValue(970)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOpen = self:GetOpenDiff(serTimeHappy[1][1], serTimeHappy[1][2]) -- 1, 24)
    local secOver = self:GetOpenDiff(serTimeHappy[2][1], serTimeHappy[2][2]) -- 4, 24)
    if timeDiff < secOpen or timeDiff > secOver then
        --活动关闭
        return false
    else
        return true
    end
end


function ServiceActivityManager:GetIsOpenHappyValue()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_FREAK) then
        return false
    end
    local serTimeHappy = GlobalParamsHelper.GetMSortValue(970)
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local secOpen = self:GetOpenDiff(serTimeHappy[1][1], serTimeHappy[1][2]) -- 1, 24)
    local secOver = self:GetOpenDiff(serTimeHappy[2][1], serTimeHappy[2][2]) -- 4, 24)
    if timeDiff < secOpen then
        --活动关闭
        return -1
    elseif timeDiff > secOver then
        return -2
    else
        return 1
    end
end



--------------------------------------------------------------------------------------------------------------------------
function ServiceActivityManager:GetOpenRank(index)
    return self._openRank[index]
end

function ServiceActivityManager:GetCurOpenRankIndex()
    for i = OpenRankType.Level, OpenRankType.Combat do
        if self:GetOpenRank(i) > 0 then
            return i
        end
    end
    return 0
end


function ServiceActivityManager:GetIsOpenRanking()
    return self.isOpenRanking
end

function ServiceActivityManager:SetIsOpenRanking(isOpenRank)
    self.isOpenRanking = isOpenRank
end

function ServiceActivityManager:GetOpenDiff(daysClose, hoursClose)
    self._serTimeSetting = GlobalParamsHelper.GetParamValue(758)
    local daysOpenOne = self._serTimeSetting[1][1]
    local hoursOpenOne = self._serTimeSetting[1][2]
    local secDiff = 0
    
    if daysClose - daysOpenOne == 0 then
        secDiff = (hoursClose - hoursOpenOne) * 60 * 60
    elseif daysClose - daysOpenOne >= 1 then
        for i = 1, daysClose do
            if i == daysClose then
                secDiff = secDiff + (hoursClose - hoursOpenOne) * 60 * 60
            else
                secDiff = secDiff + 24 * 60 * 60
            end
        end
    end
    return secDiff
end

--GameWorld.Player().learned_passive_spell
function ServiceActivityManager:GetLearnedPassive()
    return GameWorld.Player().learned_passive_spell or {}
end

function ServiceActivityManager:RefreshActivityWordRedPointByItem(itemId)
    local ids = WordCollectionDataHelper:GetAllId()
    for i,id in ipairs(ids) do
        local costTab = WordCollectionDataHelper:GetCost(id)
        for i, v in ipairs(costTab) do
            if v[1] == itemId then
                self:WordRedUpdate()
                EventDispatcher:TriggerEvent(GameEvents.ItemNumUpdate)
                return
            end
        end
    end
end


function ServiceActivityManager:RefreshRedPointForRing()
    local learnPassive = self:GetLearnedPassive()
    if learnPassive[15003] ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 620, false)
    else
        local isRed = ServiceActivityManager:RingIsShowRed(LordRingType.Power)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 620, isRed)
    end
    
    if self:GetRingUnlockStep() == LordRingType.Initial then
        local isRed = ServiceActivityManager:RingIsShowRed(LordRingType.Initial)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, isRed)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, false)
        --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, false)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, false)
    elseif self:GetRingUnlockStep() == LordRingType.Chaos then
        local isRed = ServiceActivityManager:RingIsShowRed(LordRingType.Chaos)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, isRed)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, false)
        --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, false)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, false)
    elseif self:GetRingUnlockStep() == LordRingType.Destroy then
        --local isRed = ServiceActivityManager:RingIsShowRed(LordRingType.Destroy)
        --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, isRed)
        -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, false)
        -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, false)
        -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, false)
    elseif self:GetRingUnlockStep() == LordRingType.Guard then
        local isRed = ServiceActivityManager:RingIsShowRed(LordRingType.Guard)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, isRed)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, false)
        --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, false)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, false)
    elseif self:GetRingUnlockStep() == LordRingType.Power then
        
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 617, false)
        --EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 618, false)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 619, false)
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, 616, false)
    
    end
end

-- function ServiceActivityManager:SectIsShowRed()
--     local needIds = {}
--     local ids = GuildCallDataHelper:GetAllId()
--     for i, v in ipairs(ids) do
--         local type = LordRingDetailDataHelper:GetType(v)
--         if type ~= 3 then
--             if type == RingType then
--                 table.insert(needIds, v)
--             end
--         end
--     end
    

-- end

function ServiceActivityManager:RingIsShowRed(RingType)
    local needIds = {}
    local ids = LordRingDetailDataHelper:GetAllId()
    for i, v in ipairs(ids) do
        local type = LordRingDetailDataHelper:GetType(v)
        if type ~= 3 then
            if type == RingType then
                table.insert(needIds, v)
            end
        end
    end
    
    for i, v in ipairs(needIds) do
        if self:progressIsShowRed(v) then
            return true
        end
    end
    
    
    local progressTb = ServiceActivityData:GetProgressTable()
    local num = 0
    for i, needId in ipairs(needIds) do
        for k, data in pairs(progressTb) do
            if needId == k then
                if data[2] == 3 then
                    num = num + 1
                end
            end
        end
    end
    
    if num == #needIds then
        return true
    end
    
    return false

end


function ServiceActivityManager:progressIsShowRed(id)
    local progressTb = ServiceActivityData:GetProgressTable()
    for k, v in pairs(progressTb) do
        if k == id then
            if v[2] == 1 then
                --LoggerHelper.Error("LordRingItemONE:id:"..id)
                elseif v[2] == 2 then
                --LoggerHelper.Error("LordRingItemTWO:id:"..id)
                return true
                
                elseif v[2] == 3 then
                    --LoggerHelper.Error("LordRingItemTHREE:id:"..id)
                end
        end
    end
end


function ServiceActivityManager:RefreshRedPointForOpenRank()
    local openRankRedDic = ServiceActivityData:GetOpenRankRedDic()
    --LoggerHelper.Error("openRankRedDic"..PrintTable:TableToStr(openRankRedDic))
    if openRankRedDic[OpenRankType.Level] ~= nil then
        if openRankRedDic[OpenRankType.Level] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_LEVEL, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_LEVEL, false)
        end
    end
    
    if openRankRedDic[OpenRankType.Pet] ~= nil then
        if openRankRedDic[OpenRankType.Pet] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_PET, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_PET, false)
        end
    end
    
    if openRankRedDic[OpenRankType.Horse] ~= nil then
        if openRankRedDic[OpenRankType.Horse] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_HORSE, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_HORSE, false)
        end
    end
    
    if openRankRedDic[OpenRankType.Change] ~= nil then
        --LoggerHelper.Error("openRankRedDic[OpenRankType.Change]"..openRankRedDic[OpenRankType.Change])
        if openRankRedDic[OpenRankType.Change] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_CHARGE, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_CHARGE, false)
        end
    end
    
    if openRankRedDic[OpenRankType.Gem] ~= nil then
        if openRankRedDic[OpenRankType.Gem] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_GEM, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_GEM, false)
        end
    end
    
    if openRankRedDic[OpenRankType.Combat] ~= nil then
        if openRankRedDic[OpenRankType.Combat] == 0 then
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_COMBAT, true)
        else
            EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_OPEN_RANK_COMBAT, false)
        end
    end
end


function ServiceActivityManager:RefreshWordRed()
    local ids = WordCollectionDataHelper:GetAllId()
    for i,id in ipairs(ids) do
        local isRed = self:GetWordItemRed(id)
        if isRed then
            return true       
        end
    end
    return false
end

function ServiceActivityManager:GetWordItemRed(id)
    if id then
        local wordTab = self:GetCollectWordTime()
        local time = WordCollectionDataHelper:GetTime(id)      
        local isEnough = true
        local costTab = WordCollectionDataHelper:GetCost(id)
        for i, v in ipairs(costTab) do
            local bagCount = BagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
            if bagCount < v[2] then
                isEnough = false
                break
            end
        end
        local isTime = (wordTab[id] ~= time[2]) or false
        return isEnough and isTime   
    end
    return false
end



function ServiceActivityManager:RefreshRedPointForRank()

end



ServiceActivityManager:Init()
return ServiceActivityManager
