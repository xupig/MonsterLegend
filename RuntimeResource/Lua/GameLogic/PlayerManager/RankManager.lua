-- RankManager.lua
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")

local RankManager = {}

function RankManager:Init()

end

function RankManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.GET_LADDER_RANK_LIST_REQ then
        --天梯排行榜信息 缓存信息
        self.ladderRankData = args
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Rank_List, args)
    elseif action_id == action_config.GET_AION_RANK_LIST_REQ then
        --永恒之塔排行榜信息 缓存信息
        self.aionRankData = args
        EventDispatcher:TriggerEvent(GameEvents.RefreshAionRankList, args)
    elseif action_id == action_config.RANK_LIST_GET_REQ then
        if args[1] == public_config.RANK_TYPE_ABYSS then
            --深渊排行榜
            self.abyssRankData = args[2] or {}
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Abyss_Rank_List)
        end
    end
end

--统一请求排行榜接口
function RankManager:GetRankListReq(rankType)
    local params = string.format("%d", rankType)
    GameWorld.Player().server.rank_action_req(action_config.RANK_LIST_GET_REQ, params)
end

function RankManager:GetLadderRankListReq()
    GameWorld.Player().server.rank_action_req(action_config.GET_LADDER_RANK_LIST_REQ, "")
end

function RankManager:GetAionRankListReq()
    GameWorld.Player().server.rank_action_req(action_config.GET_AION_RANK_LIST_REQ, "")
end

function RankManager:GetRankListData(rankType)
    if rankType == public_config.RANK_TYPE_ABYSS then
        return self.abyssRankData
    end
end

--返回自己在天梯榜单中的排名 未上榜返回0
function RankManager:GetSelfLadderRank()
    local result = 0
    local dbid = GameWorld.Player().dbid
    for k,v in ipairs(self.ladderRankData) do
        if v[public_config.RANK_KEY_DBID] == dbid then
            result = k
            break
        end
    end
    return result
end

--返回自己在永恒之塔榜单中的排名 未上榜返回0
function RankManager:GetSelfAionRank()
    local result = 0
    local dbid = GameWorld.Player().dbid
    for k,v in ipairs(self.aionRankData) do
        if v[public_config.RANK_KEY_DBID] == dbid then
            result = k
            break
        end
    end
    return result
end

--返回自己在榜单中的排名 未上榜返回0
function RankManager:GetSelfRank(rankType)
    local result = 0
    local rankData = {}
    local dbid = GameWorld.Player().dbid
    if rankType == public_config.RANK_TYPE_ABYSS then
        rankData = self.abyssRankData
    end

    for k,v in ipairs(rankData) do
        if v[public_config.RANK_KEY_DBID] == dbid then
            result = k
            break
        end
    end
    return result
end

RankManager:Init()
return RankManager