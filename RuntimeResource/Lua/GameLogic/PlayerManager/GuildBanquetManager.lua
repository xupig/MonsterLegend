local GuildBanquetManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local guildBanquetData = PlayerManager.PlayerDataManager.guildBanquetData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GameSceneManager = GameManager.GameSceneManager
local InstanceManager = PlayerManager.InstanceManager
local instanceBalanceData = PlayerManager.PlayerDataManager.instanceBalanceData

function GuildBanquetManager:Init()
end

function GuildBanquetManager:OnPlayerEnterWorld()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function GuildBanquetManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function GuildBanquetManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("GuildBanquetManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.GUILD_BONFIRE_NOTIFY_STATE then
        self:RefreshStateData(args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Banquet_State, args)
    elseif action_id == action_config.GUILD_BONFIRE_NOTIFY_GIFT then
        self:RefreshGetState(args)
    elseif action_id == action_config.GUILD_BONFIRE_NOTIFY_EXP_CONTRIBUTE then
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Banquet_Info, args)
    elseif action_id == action_config.GUILD_BONFIRE_NOTIFY_RANK then
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Banquet_Rank, args)
    elseif action_id == action_config.GUILD_BONFIRE_GIFT then
        --领取烧猪成功
        local itemId = next(GlobalParamsHelper.GetParamValue(663))
        local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(itemId)
        if pos > 0 then
            local itemData = bagData:GetItem(public_config.PKG_TYPE_ITEM, pos)
            GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
                GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddItemUseData(itemData)
            end)
        end
    elseif action_id == action_config.GUILD_BONFIRE_ANSWER then
        --答题结果
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Banquet_Question_Result, args)
    elseif action_id == action_config.GUILD_BONFIRE_COMPLETE then
        --结算
        instanceBalanceData:UpdateData(args)
        local data = {}
        data.instanceBalanceData = instanceBalanceData
        GUIManager.ShowPanel(PanelsConfig.InstBalance, data)
    end
end

function GuildBanquetManager:RefreshStateData(data)
    guildBanquetData:RefreshStateData(data)
end

function GuildBanquetManager:RefreshGetState(data)
    guildBanquetData:RefreshGetState(data)
end

--是否可以答题
function GuildBanquetManager:IsOpen()
    if GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        return guildBanquetData:IsInAnswerState()
    end
    return false
end

--是否可以采集烧猪
function GuildBanquetManager:CanGetItem()
    if GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        return guildBanquetData:CanGetItem()
    end
    return false
end

function GuildBanquetManager:GetStateData()
    return guildBanquetData:GetStateData()
end

function GuildBanquetManager:CanGuildBanquetEnter()
    return true
end

-----------------------------副本处理---------------------------
function GuildBanquetManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        guildBanquetData:ResetData()
    end
end

function GuildBanquetManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        local x = GlobalParamsHelper.GetParamValue(668)[1]
        local z = GlobalParamsHelper.GetParamValue(668)[2]
        local pos = Vector3.New(x, 0, z)
        EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, nil, pos, false)
    end
end

-------------------------------------客户端请求-----------------------------------
--进入
function GuildBanquetManager:GuildBanquetEnterReq()
    GameWorld.Player().server.guild_bonfire_action_req(action_config.GUILD_BONFIRE_ENTER, "")
end

--答题
function GuildBanquetManager:GuildBanquetAnswerReq(answer)
    local params = string.format("%s", answer)
    GameWorld.Player().server.guild_bonfire_action_req(action_config.GUILD_BONFIRE_ANSWER, params)
end

--领取烧猪
function GuildBanquetManager:GuildBanquetGetItemReq()
    GameWorld.Player().server.guild_bonfire_action_req(action_config.GUILD_BONFIRE_GIFT, "")
end


GuildBanquetManager:Init() 
return GuildBanquetManager