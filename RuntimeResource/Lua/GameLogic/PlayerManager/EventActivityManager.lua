local EventActivityManager = {}
require "PlayerManager/PlayerData/EventData"
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local EventActivityData = PlayerDataManager.eventActivityData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EventActivityDataHelper = GameDataHelper.EventActivityDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local OpenTimeDataHelper = GameDataHelper.OpenTimeDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskRewardData = ClassTypes.TaskRewardData
local GameSceneManager = GameManager.GameSceneManager
local InstanceManager = PlayerManager.InstanceManager

function EventActivityManager:Init()
    self:InitCallbackFunc()
end

function EventActivityManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function EventActivityManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function EventActivityManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function EventActivityManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("EventActivity Error:" .. error_id)
        return
    elseif action_id == action_config.EVENT_ACTIVITY_LIST_REQ then
        --个人的事件活动，看成多个EVENT_ACTIVITY_REFRESH。上线主动推送
        self:ListReq(args)
    elseif action_id == action_config.EVENT_ACTIVITY_REFRESH then
        --没就加，有就改进度
        self:Refresh(args)
    elseif action_id == action_config.EVENT_ACTIVITY_DEL then
        --删除
        self:Del(args)
    elseif action_id == action_config.EVENT_ACTIVITY_DAILY then
        --随机的日常事件活动字典。上线主动推送
        self:Daily(args)
    elseif action_id == action_config.EVENT_ACTIVITY_ENTER then
        --进入区域。前提客户端对应的事件活动id的进度等客户端已知。
        self:Enter(args)
    elseif action_id == action_config.EVENT_ACTIVITY_LEAVE then
        --离开区域
        self:Leave(args)
    elseif action_id == action_config.EVENT_ACTIVITY_REWARD then
        --发放奖励
        self:Reward(args)
    else
        LoggerHelper.Error("Instance action id not handle:" .. action_id)
    end
end

function EventActivityManager:HandleResp(act_id, code, args)
end

function EventActivityManager:OnEnterMap(mapId)
    local eventId = EventActivityDataHelper:GetEventActivityBySingleMapId(mapId)
    if eventId then
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(10541), 5)
    end
end

function EventActivityManager:OnLeaveMap(mapId)
    local eventId = EventActivityDataHelper:GetEventActivityBySingleMapId(mapId)
    if eventId then
        local args = {}
        args[eventId] = {}
        LoggerHelper.Log("Ash: EventActivity: OnLeaveMap EVENT_ACTIVITY_ENTER " .. PrintTable:TableToStr(args))
        self:Leave(args)
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(10542), 5)
    end
end

function EventActivityManager:Reward(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_REWARD " .. PrintTable:TableToStr(args))
    local id = args[1]
    local rewards = args[2]
    local contribution = args[3]
    local result = {}
    for k, v in pairs(rewards) do
        table.insert(result, TaskRewardData(k, v))
    end
    EventDispatcher:TriggerEvent(GameEvents.OnCompleteEventActivity, id, result, contribution)
end

function EventActivityManager:ListReq(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_LIST_REQ " .. PrintTable:TableToStr(args))
    EventActivityData:ResetEventActivityData(args)
end

function EventActivityManager:Refresh(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_REFRESH " .. PrintTable:TableToStr(args))
    EventActivityData:UpdateEventActivityData(args)
    self:ShowCurrEventActivityProgress(args)
end

function EventActivityManager:Del(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_DEL " .. PrintTable:TableToStr(args))
    EventActivityData:DelEventActivityData(args)
    local currEventActivities = EventActivityData:GetCurrEventActivities()
    for k, v in pairs(args) do
        if currEventActivities[k] ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.OnDelEventActivity, k)
        end
    end
    self:Leave(args)
end

function EventActivityManager:Daily(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_DAILY " .. PrintTable:TableToStr(args))
    EventActivityData:ResetDailyEventActivityData(args)
end

function EventActivityManager:Enter(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_ENTER " .. PrintTable:TableToStr(args))
    for k, v in pairs(args) do
        local eaData = EventActivityData:GetEventActivityData(k)
        if eaData ~= nil then
            if eaData:GetState() == public_config.EVENT_ACTIVITY_STATE_COMPLETED then
                LoggerHelper.Log("Ash: EventActivity Enter: EVENT_ACTIVITY_STATE_COMPLETED " .. k)
                return
            end
        -- self:OnEnterEvent(eventData)
        -- GameManager.GUIManager.ShowText(1, "进入事件: " .. LanguageDataHelper.CreateContent(eaData:GetName()), 5)
        end
        local currEventActivities = EventActivityData:GetCurrEventActivities()
        currEventActivities[k] = k
        EventDispatcher:TriggerEvent(GameEvents.OnEnterEventActivity, k)
    end
    self:ShowCurrEventActivityProgress(args)
end

function EventActivityManager:Leave(args)
    LoggerHelper.Log("Ash: EventActivity: EVENT_ACTIVITY_LEAVE " .. PrintTable:TableToStr(args))
    for k, v in pairs(args) do
        local mapId = EventActivityDataHelper:GetSingleMapId(k)
        if GameSceneManager:GetCurrMapID() ~= mapId then -- 进入了事件的镜像副本不算离开事件
            local eaData = EventActivityData:GetEventActivityData(k)
            if eaData ~= nil then
                if eaData:GetState() == public_config.EVENT_ACTIVITY_STATE_COMPLETED then
                    LoggerHelper.Log("Ash: EventActivity Leave: EVENT_ACTIVITY_STATE_COMPLETED " .. k)
                    return
                end
            -- GameManager.GUIManager.ShowText(1, "离开事件: " .. LanguageDataHelper.CreateContent(eaData:GetName()), 5)
            end
            local currEventActivities = EventActivityData:GetCurrEventActivities()
            currEventActivities[k] = nil
            EventDispatcher:TriggerEvent(GameEvents.OnLeaveEventActivity, k)
        end
    end
end

function EventActivityManager:GetCurrEventActivitiesByMapId(mapId)-- mapId为空则检查所有地图的事件
    local currEventActivities = {}-- 当前发生的事件
    local notHappenEventActivities = {}-- 不在时间段内的事件
    local timeAlertEventActivities = {}-- 即将结束的事件
    local highLevelEventActivities = {}-- 等级不足的事件
    local highLevelTimeAlertEventActivities = {}-- 等级不足并即将结束的事件
    local eaDatas = EventActivityDataHelper:GetEventActivitiesByMapId(mapId)
    local curTime = DateTimeUtil.Now()
    local alertTime = DateTimeUtil.Now(GlobalParamsHelper.GetParamValue(342) * 60)
    -- LoggerHelper.Log("Ash: EventActivity: alertTime " .. PrintTable:TableToStr(alertTime))
    local playerLevel = GameWorld.Player().level
    local dailyEvents = EventActivityData:GetDailyEventActivities()
    for i, v in ipairs(eaDatas) do
        if playerLevel >= EventActivityDataHelper:GetLevelLimit(v) then -- 判断等级条件
            if EventActivityDataHelper:IsDaily(v) and dailyEvents[v] then -- 日常事件
                if self:HasEventActivityDoneBefore(v) == false then -- 做过的日常不显示
                    table.insert(currEventActivities, v)
                end
            elseif not EventActivityDataHelper:IsDaily(v) and EventActivityDataHelper:IsOpen(v, curTime) then -- 特殊事件
                if not EventActivityDataHelper:IsOpen(v, alertTime) then -- 即将结束
                    table.insert(timeAlertEventActivities, v)
                end
                table.insert(currEventActivities, v)
            else
                table.insert(notHappenEventActivities, v)
            end
        else
            if not EventActivityDataHelper:IsDaily(v) and EventActivityDataHelper:IsOpen(v, curTime) then -- 特殊事件
                if not EventActivityDataHelper:IsOpen(v, alertTime) then -- 即将结束
                    table.insert(highLevelTimeAlertEventActivities, v)
                end
                table.insert(highLevelEventActivities, v)
            end
        end
    end
    return currEventActivities, notHappenEventActivities, timeAlertEventActivities, highLevelEventActivities, highLevelTimeAlertEventActivities
end

function EventActivityManager:GetDailyTaskPointReward()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.daily_task_point_reward or {}
end

function EventActivityManager:HasEventActivityDoneBefore(id)
    local dtpr = self:GetDailyTaskPointReward()
    if dtpr ~= nil then
        return dtpr[id] ~= nil
    end
    return false
end

function EventActivityManager:GoToEventActivityPos(id)
    local eventMapId = EventActivityDataHelper:GetMapId(id)
    local pos = EventActivityDataHelper:GetPos(id)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, eventMapId, pos)
end

function EventActivityManager:GetEventActivityTimeLeft(id)
    local curTime = DateTimeUtil.Now()
    return EventActivityDataHelper:GetTimeLeft(id, curTime)
end

function EventActivityManager:GetSpecialEvents()
    local ses = {}
    local eas = EventActivityDataHelper:GetAllId()
    local curTime = DateTimeUtil.Now()
    for i, v in ipairs(eas) do
        if not EventActivityDataHelper:IsDaily(v)-- 是限时事件
            and OpenTimeDataHelper:IsInOpenDate(EventActivityDataHelper:GetOpenTimeId(v), curTime)-- 在开放日期内
        then
            table.insert(ses, v)
        end
    end
    return ses
end

function EventActivityManager:GetEventActivityRewards(id)
    local rewards = EventActivityDataHelper:GetPreviewRewardIds(id)
    local result = {}
    for k, v in pairs(rewards) do
        table.insert(result, TaskRewardData(k, v))
    end
    return result
end

function EventActivityManager:GetEventActivityData(id)
    return EventActivityData:GetEventActivityData(id) or EventActivityData:GetEmptyEventActivityData(id)
end

function EventActivityManager:GetCurrEventActivities()
    return EventActivityData:GetCurrEventActivities()
end

function EventActivityManager:ShowCurrEventActivityProgress(args)
    for k, v in pairs(args) do
        local currEventActivities = EventActivityData:GetCurrEventActivities()
        if currEventActivities[k] ~= nil then
            local eaData = EventActivityData:GetEventActivityData(k)
            if eaData ~= nil then
                if eaData:GetState() == public_config.EVENT_ACTIVITY_STATE_COMPLETED then
                    -- GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(eaData:GetName()) .. " 完成", 5)
                    EventDispatcher:TriggerEvent(GameEvents.OnLeaveEventActivity, k)
                    self:CheckChangeScene(k)
                else
                    local totalScore = eaData:GetTotalScore()
                    if totalScore ~= 0 then
                        -- local progress = (eaData:GetCurrScore() / eaData:GetTotalScore()) * 100
                        -- GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(eaData:GetName()) .. " 更新事件进度: " .. string.format("%d", progress) .. "%", 5)
                        EventDispatcher:TriggerEvent(GameEvents.OnUpdateProgressEventActivity, k)
                    end
                end
            end
        end
    end
end

function EventActivityManager:CheckChangeScene(eventId)
    local imageMapId = EventActivityDataHelper:GetSingleMapId(eventId)
    LoggerHelper.Log("Ash: imageMapId:" .. imageMapId
        .. " GetCurrMapID: " .. GameSceneManager:GetCurrMapID()
        .. " targetMapId: " .. EventActivityDataHelper:GetMapId(eventId))
    if GameSceneManager:GetCurrMapID() == imageMapId then
        InstanceManager:CommonChangeMap(EventActivityDataHelper:GetMapId(eventId))
    end
end

EventActivityManager:Init()
return EventActivityManager
