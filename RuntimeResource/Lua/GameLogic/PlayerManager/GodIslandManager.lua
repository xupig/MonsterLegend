local GodIslandManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GodMonsterIslandDataHelper = GameDataHelper.GodMonsterIslandDataHelper
-- local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local PlayerDataManager = PlayerManager.PlayerDataManager
local GodIslandData = PlayerDataManager.godIslandData
local BossType = GameConfig.EnumType.BossType
local TaskCommonManager = PlayerManager.TaskCommonManager
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function GodIslandManager:Init()
    self:InitCallbackFunc()
end

function GodIslandManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function GodIslandManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function GodIslandManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function GodIslandManager:OnEnterMap(mapId)
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        EventDispatcher:TriggerEvent(GameEvents.GOD_ISLAND_TIRE_UPDATE, self:BossTireIsFull())
    end
end

function GodIslandManager:OnLeaveMap(mapId)
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        EventDispatcher:TriggerEvent(GameEvents.GOD_ISLAND_TIRE_UPDATE, false)
    end
end

function GodIslandManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        if event_id == action_config.GOD_ISLAND_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.GOD_ISLAND_ENTER)
            EventDispatcher:TriggerEvent(GameEvents.OnGodIslandEnter, false)
        end
        LoggerHelper.Error("GodIsland Error:" .. error_id)
        return
    --获取信息
    elseif event_id == action_config.GOD_ISLAND_GET_DATA then
        -- LoggerHelper.Log("GOD_ISLAND_GET_DATA .." .. PrintTable:TableToStr(args))
        self:GetInfo(args)
    elseif event_id == action_config.GOD_ISLAND_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.GOD_ISLAND_ENTER)
        -- LoggerHelper.Log("GOD_ISLAND_ENTER .." .. PrintTable:TableToStr(args))
        self:OnGodIslandEnter(args)
    elseif event_id == action_config.GOD_ISLAND_GET_KILLER_RECORDS then
        -- LoggerHelper.Log("GOD_ISLAND_GET_KILLER_RECORDS .." .. PrintTable:TableToStr(args))
        self:GodIslandGetKillerRecords(args)
    elseif event_id == action_config.GOD_ISLAND_ON_KILL_BOSS then
        -- LoggerHelper.Log("GOD_ISLAND_ON_KILL_BOSS .." .. PrintTable:TableToStr(args))
        elseif event_id == action_config.GOD_ISLAND_NOTIFY_BOSS_WILL_REBORN then
        -- LoggerHelper.Log("GOD_ISLAND_NOTIFY_BOSS_WILL_REBORN .." .. PrintTable:TableToStr(args))
        elseif event_id == action_config.GOD_ISLAND_NOTIFY_SPACE_INFO then
            -- LoggerHelper.Log("GOD_ISLAND_NOTIFY_SPACE_INFO .." .. PrintTable:TableToStr(args))
            self:GodIslandSpaceBossInfo(args)
        else
            LoggerHelper.Error("GodIsland event id not handle:" .. event_id)
    end
end

function GodIslandManager:GetInfo(args)
    GodIslandData:UpdateGodIslandData(args)
    EventDispatcher:TriggerEvent(GameEvents.OnGodIslandGetInfo)
end

function GodIslandManager:OnGodIslandEnter(args)
    EventDispatcher:TriggerEvent(GameEvents.OnGodIslandEnter, true)
-- GUIManager.ClosePanel(PanelsConfig.GodIsland)
-- GUIManager.ClosePanel(PanelsConfig.DailyActivity)
end

function GodIslandManager:GodIslandGetKillerRecords(args)
    -- LoggerHelper.Log("Ash: GodIslandGetKillerRecords: " .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.OnGodIslandGetKillerRecords, args)
end

function GodIslandManager:GodIslandSpaceBossInfo(args)
    -- LoggerHelper.Log("Ash: GodIslandSpaceBossInfo: " .. PrintTable:TableToStr(args))
    GodIslandData:UpdateGodIslandSpaceInfo(args)
    ClientEntityManager.bossargs = {args[3], BossType.GodIsLand}
    ClientEntityManager.UpdateBossTomb(args[3], BossType.GodIsLand)
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossSpaceBossInfo)
end

function GodIslandManager:GetAllFloor()
    return GodIslandData:GetAllInfo()
end


function GodIslandManager:GetData(floor)
    -- GodIslandData:SetCurrentFloor(floor)
    -- LoggerHelper.Log("god_island_action_req action_config.GOD_ISLAND_GET_DATA: " .. floor)
    GameWorld.Player().server.god_island_action_req(action_config.GOD_ISLAND_GET_DATA, tostring(floor))
end

function GodIslandManager:Enter(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.GOD_ISLAND_ENTER)
    -- LoggerHelper.Log("god_island_action_req action_config.GOD_ISLAND_ENTER: " .. id)
    GameWorld.Player().server.god_island_action_req(action_config.GOD_ISLAND_ENTER, tostring(id))
end

function GodIslandManager:GetKillerRecords(id)
    -- LoggerHelper.Log("god_island_action_req action_config.GOD_ISLAND_GET_KILLER_RECORDS: " .. id)
    GameWorld.Player().server.god_island_action_req(action_config.GOD_ISLAND_GET_KILLER_RECORDS, tostring(id))
end

function GodIslandManager:GotoBossByMapId(mapId, pos)
    local id = GodMonsterIslandDataHelper:GetMonsterByMapId(mapId)
    if id then
        local monster = self:GetMonsterItem(id)
        GodIslandManager:GotoBoss(monster, pos)
    end
end

function GodIslandManager:GotoBoss(bossData, targetPos)
    local cb = function() self:ConfirmGotoBoss(bossData, targetPos) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function GodIslandManager:ConfirmGotoBoss(bossData, targetPos)
    --LoggerHelper.Log("Ash: GotoBoss: " .. bossData:GetSenceId() .. " " .. bossData:GetBossRegionId())
    local monsterType = bossData:GetMonsterType()
    local pos = nil
    if targetPos then
        pos = targetPos
    else
        if monsterType == 1 then
            pos = GameSceneManager:GetMonsterPos(bossData:GetSenceId(), bossData:GetRegionId())
        else
            if GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
                local showText = LanguageDataHelper.GetContent(56069)
                GUIManager.ShowText(2, showText)
                return
            end
        end
    end
    --LoggerHelper.Log("Ash: GotoBoss pos: " .. PrintTable:TableToStr(pos))
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandGodIslandSceneKillMonsterPoint,
        bossData:GetId(), bossData:GetSenceId(), pos, 5)
end


function GodIslandManager:GetCurFloorMonsterInfo()
    return GodIslandData:GetCurFloorMonsterInfo()
end

function GodIslandManager:GetTire()
    return GodIslandData:GetTire()
end

function GodIslandManager:GetBigCrystalCollectTimes()
    return GodIslandData:GetBigCrystalCollectTimes()
end

function GodIslandManager:GetMonsterItem(id)
    return GodIslandData:GetMonsterItem(id)
end

function GodIslandManager:SetCurrentFloor(floor)
    return GodIslandData:SetCurrentFloor(floor)
end

function GodIslandManager:GetAllSpaceInfo()
    return GodIslandData:GetAllSpaceInfo()
end

function GodIslandManager:GetCollectRewardLanguageId(godIslandId)
    if self._collectReward == nil then
        self._collectReward = GlobalParamsHelper.GetParamValue(680)--神兽岛界面，采集可获得的奖励，配置表流水id：中文id
    end
    return self._collectReward and self._collectReward[godIslandId] or 0
end

function GodIslandManager:GetCrystalBigLimit()
    if self._crystalBigLimit == nil then
        self._crystalBigLimit = GlobalParamsHelper.GetParamValue(666)--神兽岛大水晶每日可采集数量
    end
    return self._crystalBigLimit
end
--世界boss疲劳值是否已满
function GodIslandManager:BossTireIsFull()
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        return GameWorld.Player().god_island_tire_flag == 1
    else
        return false
    end
end

GodIslandManager:Init()
return GodIslandManager
