--PVP活动Manager
local PVPManager = {}
local action_config = require("ServerConfig/action_config")
local fortData = PlayerManager.PlayerDataManager.fortData
local PanelsConfig = GameConfig.PanelsConfig

function PVPManager:Init()   
	EventDispatcher:AddEventListener(GameEvents.SCENE_ENTER_PVPNEW,function ()
		self:InitFort()
	end)
end

function PVPManager:HandleData(action_id,error_id,args)
	--LoggerHelper.Error("PVPManager:HandleData"..action_id)
	--报错了
	if error_id ~= 0 then
		return
	--获取场景基本信息
	elseif action_id == action_config.EVENT_PVP_MATCH_BASE_INFO then
		self:SetFortBaseInfo(args)
	--更新玩家信息
	elseif action_id == action_config.EVENT_PVP_PLAYER_LIST_REQ then
		self:UpdateFortPlayerInfo(args)
	-- --更新据点信息(直接从Fort实体更新)
	-- elseif action_id == action_config.EVENT_PVP_FORT_INFO then
	-- 	self:UpdateFortInfo(args)
	--结算消息
	elseif action_id == action_config.EVENT_PVP_SETTLE then
		self:FortSettle(args)
	end
end

--初始化据点
function PVPManager:InitFort()
	fortData:InitFortData()
end

function PVPManager:SetFortBaseInfo(args)
	fortData:SetFortBaseInfo(args)
	EventDispatcher:TriggerEvent(GameEvents.SET_FORT_BASE_INFO)
	--玩家集齐，打开同阵营队友信息面板
	if fortData.room_stage >= 2 then
		--LoggerHelper.Error("SetAllPlayer")
		fortData:SetFactionMember()
	end
end

function PVPManager:UpdateFortPlayerInfo(args)
	fortData:UpdateFortPlayerInfo(args)
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_FORT_PLAYER_INFO)
end

function PVPManager:FortSettle(args)
	fortData:FortSettle(args)
	EventDispatcher:TriggerEvent(GameEvents.FORT_SETTLE)
end

-------------------------------------客户端请求-----------------------------------
--请求匹配
function PVPManager:RequestPVPMatch()
	LoggerHelper.Error("RequestPVPMatch")
	GameWorld.Player().server.pvp_match_action_req(action_config.EVENT_PVP_MATCH_REQ,"")
end

--取消匹配
function PVPManager:RequestCancelPVPMatch()
	GameWorld.Player().server.pvp_match_action_req(action_config.EVENT_PVP_MATCH_CANCEL_REQ,"")
end

--请求玩家列表
function PVPManager:RequestPlayerList()
	GameWorld.Player().server.pvp_match_action_req(action_config.EVENT_PVP_PLAYER_LIST_REQ,"")
end

PVPManager:Init() 
return PVPManager