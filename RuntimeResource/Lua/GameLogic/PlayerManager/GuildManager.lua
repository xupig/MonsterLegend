--公会Manager
local GuildManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local guildData = PlayerManager.PlayerDataManager.guildData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GameSceneManager = GameManager.GameSceneManager
local InstanceManager = PlayerManager.InstanceManager
local instanceBalanceData = PlayerManager.PlayerDataManager.instanceBalanceData
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function GuildManager:Init()
    self.openGuildRedEnvelopeViewFunc = function() GUIManager.ShowPanelByFunctionId(412) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onGuildDBIDChange = function()self:OnGuildDBIDChange() end
    self._onGuildContributeChangeCB = function() self:RefreshGuildSkillLevelUpRedPointState() end
    self._refreshGuildHasDailyReward = function() self:RefreshDailyRewardRedPointState() end
    self._dailyGuildConquest = function() self:OpenDaily(417) end
    self._dailyGuildBanquet = function() self:OpenDaily(416) end
    self._dailyGuildMonster = function() self:OpenDaily(415) end
    self._dailyGuildGoddness = function() self:OpenDaily(418) end
    self._onGuildMythicalCreaturesSoulItemChangeCB = function() self:RefreshMythicalCreaturesRedPointState() end
end

function GuildManager:OnPlayerEnterWorld()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_GUILD_CONTRIBUTE, self._onGuildContributeChangeCB)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.On_Guild_DBID_Change, self._onGuildDBIDChange)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Has_Daily_Reward, self._refreshGuildHasDailyReward)
    EventDispatcher:AddEventListener(GameEvents.Daily_Guild_Conquest, self._dailyGuildConquest)
    EventDispatcher:AddEventListener(GameEvents.Daily_Guild_Banquet, self._dailyGuildBanquet)
    EventDispatcher:AddEventListener(GameEvents.Daily_Guild_Monster, self._dailyGuildMonster)
    EventDispatcher:AddEventListener(GameEvents.Daily_Guard_Goddness, self._dailyGuildGoddness)
    EventDispatcher:AddEventListener(GameEvents.On_Guild_DBID_Change, self._onGuildDBIDChange)
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Mythical_Creatures_Soul_Item_Info, self._onGuildMythicalCreaturesSoulItemChangeCB)

    self._showDailyRewardRedPoint = false
    self._showGuildSkillLevelUpRedPoint = false
    self._showGuildRedEnvelopeRedPoint = false
    self._showJoinOrCreateRedPoint = false
    self._showMythicalCreaturesRedPoint = false

    self:RefreshJoinOrCreateRedPointState()
    self:RefreshDailyRewardRedPointState()
    self:RefreshGuildSkillLevelUpRedPointState()
    self:RefreshMythicalCreaturesRedPointState()

    self.hasGuildRedEnvelopeGetState = false
    if self:IsInGuild() then
        self:GetGuildRedEnvelopeListReq()
    end
end

function GuildManager:OnPlayerLeaveWorld()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_GUILD_CONTRIBUTE, self._onGuildContributeChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Guild_Conquest, self._dailyGuildConquest)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Guild_Banquet, self._dailyGuildBanquet)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Guild_Monster, self._dailyGuildMonster)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Guard_Goddness, self._dailyGuildGoddness)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Has_Daily_Reward, self._refreshGuildHasDailyReward)
    EventDispatcher:RemoveEventListener(GameEvents.On_Guild_DBID_Change, self._onGuildDBIDChange)
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Mythical_Creatures_Soul_Item_Info, self._onGuildMythicalCreaturesSoulItemChangeCB)
end

function GuildManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("GuildManager "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.GUILD_BUILD then
        --创建成功
        GUIManager.ClosePanel(PanelsConfig.ApplyForGuild)
        GUIManager.ShowPanel(PanelsConfig.Guild)
    elseif action_id == action_config.GUILD_GET_DETAIL_INFO then
        self:RefreshSelfGuildInfo(args)
    elseif action_id == action_config.GUILD_SET_ANNOUNCEMENT then
        self:SetGuildAnnouncement(args[1])
        self:GetSelfGuildInfo()
    elseif action_id == action_config.GUILD_GET_LIST then
        self:RefreshGuildListInfo(args)
    elseif action_id == action_config.GUILD_APPLY then
        --申请加入
        --local showText = LanguageDataHelper.GetContent(53004)
        --GUIManager.ShowText(2, showText)
    elseif action_id == action_config.GUILD_GET_APPLY_LIST then
        self:RefreshApproveMessage(args)
    elseif action_id == action_config.GUILD_REPLY_APPLY then
        --刷新申请列表信息
        self:GetApplyGuildListReq()
        self:GetSelfGuildInfo()
    elseif action_id == action_config.GUILD_REPLY_APPLY_ALL then
        --刷新申请列表信息
        self:GetApplyGuildListReq()
        self:GetSelfGuildInfo()
    elseif action_id == action_config.GUILD_SET_SETTING then
        self:SetGuildNeedVerify(args[1])
    elseif action_id == action_config.GUILD_SEND_SYS_RED_ENVELOPE then
        --发系统红包
    elseif action_id == action_config.GUILD_GET_SKILL_INFO then

    elseif action_id == action_config.GUILD_SKILL_LEVEL_UP then

    elseif action_id == action_config.GUILD_GRAB_RED_ENVELOPE then
        --抢红包
        self:GrapGuildRedEnvelopeItem(args)
    elseif action_id == action_config.GUILD_INVITE then
        --公会邀请 邀请者返回结果不处理
        if args[1] ~= nil then     
            local data =  {["id"]=MessageBoxType.InviteJoinGuild,["value"]=args}
            GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
        end
    elseif action_id == action_config.GUILD_GET_INFO then
        --查看其他公会详细信息
        self:RefreshOtherGuildInfo(args)
    elseif action_id == action_config.GUILD_GET_SELF_MEMBER_INFO then
        --所在公会成员信息
        self:RefreshGuildSelfMemberInfo(args)
    elseif action_id == action_config.GUILD_GET_ONLINE_INFO then
        --所在公会在线成员信息
        self:RefreshGuildSelfOnlineMemberInfo(args)
    elseif action_id == action_config.GUILD_GET_MEMBER_INFO then
        --查看其他公会成员信息
        self:RefreshGuildMemberInfo(args)
    elseif action_id == action_config.GUILD_GET_RANK then
        --公会排行榜信息
        self:RefreshGuildRankList(args)
    elseif action_id == action_config.GUILD_GET_INFO_BY_NAME then
        --搜索公会
        if args[2] == nil then
            local showText = LanguageDataHelper.GetContent(53005)
            GUIManager.ShowText(1, showText)
        else
            local data = {1,1,{args}}
            self:RefreshGuildListInfo(data)
        end
    elseif action_id == action_config.GUILD_WAREHOUSE_PKG then
        --仓库背包
        self:RefreshGuildWarehouseBag(args)
    elseif action_id == action_config.GUILD_WAREHOUSE_SELL then
        --仓库捐赠成功刷新数据
        self:GuildGetWarehouseBagReq()
        self:GuildWarehouseRecordReq()
        EventDispatcher:TriggerEvent(GameEvents.Guild_Warehouse_Sell_Succ)
    elseif action_id == action_config.GUILD_WAREHOUSE_BUY then
        --仓库兑换成功刷新数据
        self:GuildGetWarehouseBagReq()
        self:GuildWarehouseRecordReq()
    elseif action_id == action_config.GUILD_WAREHOUSE_DESTROY then
        --仓库销毁物品成功刷新数据
        self:GuildGetWarehouseBagReq()
        self:GuildWarehouseRecordReq()
    elseif action_id == action_config.GUILD_WAREHOUSE_RECORD then
        --仓库记录
        self:RefreshGuildWarehouseRecordData(args)
    elseif action_id == action_config.GUILD_WAREHOUSE_BUY_PET then
        --仓库兑换成功刷新数据
        self:GuildWarehouseRecordReq()
    elseif action_id == action_config.GUILD_RED_ENVELOPE_LIST then
        --获取公会所有的红包列表
        self:RefreshGuildRedEnvelopeListData(args)
    elseif action_id == action_config.GUILD_RED_ENVELOPE_RECORD then
        --获取红包记录
        self:RefreshGuildRedEnvelopeRecordData(args)
    elseif action_id == action_config.GUILD_NOTIFY_ADD_RED_ENVELOPE then
        --通知玩家加红包
        self:UpdateGuildRedEnvelopeItem(args)
        self:GetGuildRedEnvelopeRecordReq()
    elseif action_id == action_config.GUILD_NOTIFY_UPDATE_RED_ENVELOPE then
        --通知玩家更新红包
        self:UpdateGuildRedEnvelopeItem(args)
    elseif action_id == action_config.GUILD_NOTIFY_DEL_RED_ENVELOPE then
        --通知玩家删红包
        self:DeleteGuildRedEnvelopeItem(args)
    elseif action_id == action_config.GUILD_SEND_MONEY_RED_ENVELOPE then
        --玩家发个人红包
    elseif action_id == action_config.GUILD_QUIT then
        --退出公会
        if GUIManager.PanelIsActive(PanelsConfig.Guild) then
            GUIManager.ClosePanel(PanelsConfig.Guild)
            GUIManager.ShowPanel(PanelsConfig.ApplyForGuild)
        end
    elseif action_id == action_config.GUILD_NOTIFY_JOIN_IN then
        --通知申请玩家已经通过
        if GUIManager.PanelIsActive(PanelsConfig.ApplyForGuild) then
            GUIManager.ClosePanel(PanelsConfig.ApplyForGuild)
            GUIManager.ShowPanel(PanelsConfig.Guild)
        end
    elseif action_id == action_config.GUILD_SET_POSITION then
        self:GuildSelfMemberInfoReq(1)
    elseif action_id == action_config.GUILD_KICK then
        self:GuildSelfMemberInfoReq(1)
    elseif action_id == action_config.GUILD_BEAST_GET_INFO then
        --获取{次数，兽粮，开始时间}
        self:RefreshGuildMonsterInfo(args)
    elseif action_id == action_config.GUILD_BEAST_GET_BOSS_HP then
        --获取{boss最大血量,当前血量}
        self:RefreshGuildMonsterHP(args)
    elseif action_id == action_config.GUILD_BEAST_BROADCAST_START then
        --通知公会成员公会神兽开启
        GUIManager.ShowPanel(PanelsConfig.GuildMonsterOpenTips)
    elseif action_id == action_config.GUILD_BEAST_NOTIFY_ROOM_STAGE then
        --{stage, stage_start_time}  stage 1:玩法开始时间  2:打死boss时间
        if args[1] == 1 then
            self:SetMonsterSceneData(args[2])
        elseif args[1] == 2 then
            local seconds = GlobalParamsHelper.GetParamValue(621)
            local startTime =  args[2] or 0
            InstanceCommonManager:ShowLeaveCountDown(seconds, startTime)
            self.monsterFlag = 1
            local data = {}
            data.num = 1
            GUIManager.ShowPanel(PanelsConfig.GuildMonsterInstanceInfo, data)

            guildData:RefreshGuildMonsterKillInfo(args)
        end
    elseif action_id == action_config.GUILD_BEAST_START then
        --开启公会神兽成功
        GUIManager.ShowText(2, LanguageDataHelper.GetContent(53206))
    elseif action_id == action_config.GUILD_MONSTER_ENTER_REQ then
        EventDispatcher:TriggerEvent(GameEvents.OnWorldBossEliteEnter, true)
    elseif action_id == action_config.GUILD_MONSTER_GET_DATA then
        self:RefreshGuildTokenMonsterData(args)
    elseif action_id == action_config.GUILD_GUARD_NOTIFY_START_TIME then
        --守卫女神开启时间
        guildData:RefreshGuildGuardGoddnessData(4, args[1])
        EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Start_Time, args[1])
    elseif action_id == action_config.GUILD_GUARD_NOTIFY_GODDESS_INFO then
        --神像{{eid, monsterId}, {eid, monsterId}, {eid, monsterId}}
        guildData:RefreshGuildGuardGoddnessData(1, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Entity_Info, args)
    elseif action_id == action_config.GUILD_GUARD_NOTIFY_DAMAGE_RANK then
        --伤害排行 {rank_1, rank_2,rank_3，self_info} rank_1 = {rank,name,damage} self_info 放在最后一个
        guildData:RefreshGuildGuardGoddnessData(2, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Damage_Info, args)
    elseif action_id == action_config.GUILD_GUARD_NOTIFY_KILL_INFO then
        --波数杀怪信息 ｛总波数，当前波数，杀怪数，获得经验｝
        guildData:RefreshGuildGuardGoddnessData(3, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Kill_Info, args)
    elseif action_id == action_config.GUILD_GUARD_NOTIFY_SETTLE then
        --结算
        instanceBalanceData:UpdateData(args)
        local data = {}
        data.instanceBalanceData = instanceBalanceData
        GUIManager.ShowPanel(PanelsConfig.InstBalance, data)
    elseif action_id == action_config.GUILD_LEVEL_UP then
        --公会升级
        self:GetSelfGuildInfo()
        GUIManager.ShowText(1, LanguageDataHelper.GetContent(53060))
    end
end

function GuildManager:GetGuildId()
    local dbid = guildData:GetGuildId() or 0
    return dbid
end

function GuildManager:IsInGuild()
    local dbid = guildData:GetGuildId()
    if dbid ~= nil and dbid ~= 0 then
        return true
    else
        return false
    end
end

--是否有审批权限
function GuildManager:CanApprove()
    --普通成员没有审批权限
    if guildData.guildPosition == public_config.GUILD_POSITION_NORMAL then
        return false
    end
    return true
end

--是否已申请公会
function GuildManager:HasApprove(guildId)
    for k,v in pairs(guildData.guildApplyList) do
        if v[1] == guildId then
            return true
        end
    end
    return false
end

--是否为会长
function GuildManager:IsPresident()
    return guildData:IsPresident()
end

--是否为副会长
function GuildManager:IsVicePresident()
    return guildData:IsVicePresident()
end

--是否领取今日工资
function GuildManager:HasGottenDailyReward()
    return guildData.hasDailyReward == 1
end

function GuildManager:IsGuildMonsterOpen()
    local config = ActivityDailyDataHelper:GetTimeLimit(public_config.ACTIVITY_ID_GUILD_BEAST)
    local startTime = DateTimeUtil.TodayTimeStamp(config[1], config[2])
    local endTime = DateTimeUtil.TodayTimeStamp(config[1]+config[3], config[2]+config[4])
    if DateTimeUtil.GetServerTime() <= endTime and startTime <= DateTimeUtil.GetServerTime() then
        return true
    end
    return false
end

function GuildManager:CanGuildMonsterEnter()
    return self:IsGuildMonsterOpen()
end

function GuildManager:CanGuardGoddnessEnter()
    return true
end

function GuildManager:OnGuildDBIDChange()
    if self:IsInGuild() then
        self:GetGuildRedEnvelopeListReq()
    end
    self:RefreshJoinOrCreateRedPointState()
    self:RefreshHasGuildRedEnvelopeGet()
    self:RefreshDailyRewardRedPointState()
    self:RefreshGuildSkillLevelUpRedPointState()
    self:RefreshGuildRedEnvelopeRedPointState()
    self:RefreshMythicalCreaturesRedPointState()
end
-----------------------------------红点------------------------------
function GuildManager:RefreshMythicalCreaturesRedPointState()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUILD_ANIMAL) then
        return
    end
    if self:IsInGuild() then
        local itemId = GlobalParamsHelper.GetParamValue(606)
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
        self:SetMythicalCreaturesRedPoint(bagNum>0)
    else
        self:SetMythicalCreaturesRedPoint(false)
    end
end

function GuildManager:SetMythicalCreaturesRedPoint(state)
    if self._showMythicalCreaturesRedPoint ~= state then
        self._showMythicalCreaturesRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUILD_ANIMAL, self:GetMythicalCreaturesRedPoint())
    end
end

function GuildManager:GetMythicalCreaturesRedPoint()
    return self._showMythicalCreaturesRedPoint
end

function GuildManager:RefreshJoinOrCreateRedPointState()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUILD) then
        return
    end
    if not self:IsInGuild() then
        self:SetJoinOrCreateRedPoint(true)
    else
        self:SetJoinOrCreateRedPoint(false)
    end
end

function GuildManager:SetJoinOrCreateRedPoint(state)
    if self._showJoinOrCreateRedPoint ~= state then
        self._showJoinOrCreateRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_APPLY_FOR_GUILD, self:GetJoinOrCreateRedPoint())
    end
end

function GuildManager:GetJoinOrCreateRedPoint()
    return self._showJoinOrCreateRedPoint
end

function GuildManager:RefreshDailyRewardRedPointState()
    if not self:IsInGuild() then
        if self:GetDailyRewardRedPoint() then
            self:SetDailyRewardRedPoint(false)
        end
        return
    end
    self:SetDailyRewardRedPoint(not self:HasGottenDailyReward())
end

function GuildManager:SetDailyRewardRedPoint(state)
    if self._showDailyRewardRedPoint ~= state then
        self._showDailyRewardRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUILD, self:GetDailyRewardRedPoint())
    end
end

function GuildManager:GetDailyRewardRedPoint()
    return self._showDailyRewardRedPoint
end

function GuildManager:RefreshGuildSkillLevelUpRedPointState()
    if not self:IsInGuild() then
        if self:GetGuildSkillLevelUpRedPoint() then
            self:SetGuildSkillLevelUpRedPoint(false)
        end
        return
    end
    local data = guildData:GetSkillInfo()
    local result = false
    for k,v in pairs(data) do
        local cfgData = GuildDataHelper:GetGuildSkillDataBy2(v.type, v.level)
        local maxLevel = GuildDataHelper:GetGuildSkillMaxLevel(v.type)
        if v.unLockLevel == 0 and GameWorld.Player().money_guild_contribute >= cfgData.cost and v.level < maxLevel then
            result = true
            break
        end
    end
    self:SetGuildSkillLevelUpRedPoint(result)
end

function GuildManager:SetGuildSkillLevelUpRedPoint(state)
    if self._showGuildSkillLevelUpRedPoint ~= state then
        self._showGuildSkillLevelUpRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUILD_SKILL, self:GetGuildSkillLevelUpRedPoint())
    end
end

function GuildManager:GetGuildSkillLevelUpRedPoint()
    return self._showGuildSkillLevelUpRedPoint
end

function GuildManager:RefreshGuildRedEnvelopeRedPointState()
    if not self:IsInGuild() then
        if self:GetGuildRedEnvelopeRedPoint() then
            self:SetGuildRedEnvelopeRedPoint(false)
        end
        return
    end

    local result = false
    for k,v in pairs(guildData.guildRedEnvelopeListData) do
        if self:CanRedEnvelopeGetState(v) then
            result = true
            break
        end
    end

    self:SetGuildRedEnvelopeRedPoint(result)
end

function GuildManager:SetGuildRedEnvelopeRedPoint(state)
    if self._showGuildRedEnvelopeRedPoint ~= state then
        self._showGuildRedEnvelopeRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUILD_RED_PACKET, self:GetGuildRedEnvelopeRedPoint())
    end
end

function GuildManager:GetGuildRedEnvelopeRedPoint()
    return self._showGuildRedEnvelopeRedPoint
end
-----------------------------副本处理---------------------------
function GuildManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_MONSTER then
        self.monsterFlag = nil
        guildData.guildMonsterKillInfo = {}
    end
    if sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then
        guildData:ResetGuildGuardGoddnessData()
    end
end

function GuildManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_MONSTER then
        if self.monsterFlag == nil then
            self:SetMonsterSceneData(guildData.guildMonsterInfo[3])
        else
            local seconds = GlobalParamsHelper.GetParamValue(621)
            local startTime =  guildData.guildMonsterKillInfo[2] or 0
            InstanceCommonManager:ShowLeaveCountDown(seconds, startTime)
            local data = {}
            data.num = 1
            GUIManager.ShowPanel(PanelsConfig.GuildMonsterInstanceInfo, data)
        end
    end
    if sceneType == SceneConfig.SCENE_TYPE_GUARD_GODDNESS then
        local entityInfo = guildData:GetGuildGuardGoddnessData(1)
        if entityInfo ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Entity_Info, entityInfo)
        end
        local killInfo = guildData:GetGuildGuardGoddnessData(3)
        if killInfo ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Kill_Info, killInfo)
        end
        local damageInfo = guildData:GetGuildGuardGoddnessData(2)
        if damageInfo ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Damage_Info, damageInfo)
        end
        local startTime = guildData:GetGuildGuardGoddnessData(4)
        if startTime ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guard_Goddness_Start_Time, startTime)
        end
    end
end

function GuildManager:SetMonsterSceneData(time)
    local startTime = time or 0
    local star = 3
    local data = {}
    data.start = star
    data.startTimeData = GuildDataHelper:GetStartTime(1)
    data.dropMessages = InstanceConfig.GUILD_MONSTER_INSTANCE_START_DROP_MESSAGE_MAP
    data.countDownMessages = InstanceConfig.GUILD_MONSTER_INSTANCE_COUNT_DOWN_MESSAGE_MAP
    data.cb = function()  end
    data.startTime = startTime

    InstanceCommonManager:ShowStart(data)

    local instanceTime = GuildDataHelper:GetGuildMonsterTime(1)
    InstanceCommonManager:StartInstanceCountDown(instanceTime, startTime)
end

function GuildManager:GotoTokenMonster(id)
    local bossId = GuildDataHelper:GetTokenBossId(id)
    local mapId = GuildDataHelper:GetTokenMapId(id)
    local regionId = GuildDataHelper:GetTokenRegionId(id)
    local pos = GameSceneManager:GetMonsterPos(mapId, regionId)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBossSceneKillMonsterPoint, bossId, mapId, pos, 0)
end

-----------------------------数据处理---------------------------
function GuildManager:RefreshGuildInfo(changeValue)
    guildData:RefreshGuildInfo(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Show)
end

function GuildManager:RefreshSelfGuildInfo(data)
    guildData:RefreshSelfGuildInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Self_Guild_Info)
end

function GuildManager:SetGuildAnnouncement(data)
    guildData:SetGuildAnnouncement(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Notice)
end

function GuildManager:SetGuildNeedVerify(data)
    guildData:SetGuildNeedVerify(data)
end

function GuildManager:RefreshGuildListInfo(data)
    guildData:RefreshGuildListInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_GuildList_Info)
end

function GuildManager:RefreshApproveMessage(data)
    guildData:RefreshApproveMessage(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Approve_Message)
    EventDispatcher:TriggerEvent(GameEvents.Show_Approve_Red_Dot)
end

function GuildManager:RefreshOtherGuildInfo(data)
    guildData:RefreshOtherGuildInfo(data)
    self:GuildMemberInfoReq(guildData.guildDbidQuery , 1)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Home_Page_Preview)
end

function GuildManager:RefreshGuildSelfMemberInfo(data)
    guildData:RefreshGuildSelfMemberInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Self_Member_Info)
end

function GuildManager:RefreshGuildSelfOnlineMemberInfo(data)
    guildData:RefreshGuildSelfOnlineMemberInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Self_Online_Member_Info)
end

function GuildManager:RefreshGuildMemberInfo(data)
    guildData:RefreshGuildMemberInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Query_Guild_Member_Info)
end

function GuildManager:RefreshGuildRankList(data)
    guildData:RefreshGuildRankList(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Rank_Data)
end

function GuildManager:RefreshGuildWarehouseBag(data)
    --宠物丹单独加入
    local itemId = next(GlobalParamsHelper.GetParamValue(592))
    data[-2] = {[public_config.ITEM_KEY_ID]=itemId}

    bagData:OnPkgChanged(public_config.PKG_TYPE_GUILD_WAREHOUSE, data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Warehouse_Bag)
end

function GuildManager:RefreshGuildWarehouseRecordData(data)
    guildData:RefreshGuildWarehouseRecordData(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Warehouse_Record_Data)
end

function GuildManager:RefreshGuildRedEnvelopeListData(data)
    guildData:RefreshGuildRedEnvelopeListData(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Red_Envelope_List)
    self:RefreshHasGuildRedEnvelopeGet()
    self:RefreshGuildRedEnvelopeRedPointState()
end

function GuildManager:RefreshGuildRedEnvelopeRecordData(data)
    guildData:RefreshGuildRedEnvelopeRecordData(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Red_Envelope_Record)
end

function GuildManager:UpdateGuildRedEnvelopeItem(data)
    guildData:UpdateGuildRedEnvelopeItem(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Red_Envelope_List)
    self:RefreshHasGuildRedEnvelopeGet()
    self:RefreshGuildRedEnvelopeRedPointState()
end

function GuildManager:DeleteGuildRedEnvelopeItem(data)
    guildData:DeleteGuildRedEnvelopeItem(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Red_Envelope_List)
    self:RefreshHasGuildRedEnvelopeGet()
    self:RefreshGuildRedEnvelopeRedPointState()
end

--data: {红包id}
function GuildManager:GrapGuildRedEnvelopeItem(data)
    local id = data[1]
    local itemData = guildData.guildRedEnvelopeListData[id]
    EventDispatcher:TriggerEvent(GameEvents.Show_Red_Envelope_Info_View, itemData)
end

function GuildManager:RefreshGuildMonsterInfo(data)
    guildData:RefreshGuildMonsterInfo(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Monster_Info)
end

function GuildManager:RefreshGuildMonsterHP(data)
    guildData:RefreshGuildMonsterHP(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Monster_HP)
end

function GuildManager:RefreshGuildTokenMonsterData(data)
    guildData:RefreshGuildTokenMonsterData(data)
    EventDispatcher:TriggerEvent(GameEvents.On_Guild_Elite_Monster_Get_Info)
end

function GuildManager:GetGuildSelfMemberList()
    return guildData.guildSelfMemberList
end

function GuildManager:RefreshHasGuildRedEnvelopeGet()
    if not self:IsInGuild() then
        if self.hasGuildRedEnvelopeGetState then
            self.hasGuildRedEnvelopeGetState = false
            EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.GuildGetRed)
        end
        return
    end
    local hasGuildRedEnvelopeGet = false
    for k,v in pairs(guildData.guildRedEnvelopeListData) do
        if self:CanRedEnvelopeGetState(v) then
            hasGuildRedEnvelopeGet = true
            break
        end
    end
    if self.hasGuildRedEnvelopeGetState ~= hasGuildRedEnvelopeGet then
        if hasGuildRedEnvelopeGet then
            EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openGuildRedEnvelopeViewFunc,EnumType.RemindType.GuildGetRed)
        else
            EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.GuildGetRed)
        end
        self.hasGuildRedEnvelopeGetState = hasGuildRedEnvelopeGet
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUILD_RED_PACKET, self.hasGuildRedEnvelopeGetState)
    end
end

function GuildManager:CanRedEnvelopeGetState(data)
    local count = data[public_config.GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY]
    if count >= data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT] then
        return false
    end
    if data[public_config.GUILD_RED_ENVELOPE_INFO_IS_SEND] == 0 then
        if self:IsOwnRedEnvelope(data) then
            return true
        else
            return false
        end
    end
    if self:HasGotten(data) then
        return false
    end
    return true
end

function GuildManager:IsOwnRedEnvelope(data)
    if GameWorld.Player().dbid == data[public_config.GUILD_RED_ENVELOPE_INFO_AVATAR_DBID] then
        return true
    end
    return false
end

function GuildManager:HasGotten(data)
    local record = data[public_config.GUILD_RED_ENVELOPE_INFO_SET] or {}
    for k,v in pairs(record) do
        if GameWorld.Player().dbid == v[1] then
            return true
        end
    end
    return false
end

-------------------------------------红包数据排序---------------------------------------
local function SortByTime(a,b)
    return tonumber(a[1][public_config.GUILD_RED_ENVELOPE_INFO_TIME]) < tonumber(b[1][public_config.GUILD_RED_ENVELOPE_INFO_TIME])
end

local function SortByKey(a,b)
    return a[2] < b[2]
end

--未发放>未领取>已领取>已领完
function GuildManager:GetRedEnvelopeList()
    local result = {}
    for k,v in pairs(guildData.guildRedEnvelopeListData) do
        local state = 4 --已领完
        if v[public_config.GUILD_RED_ENVELOPE_INFO_IS_SEND] == 0 then
            state = 1 --未发放
        elseif self:CanGet(v) then
            state = 2 --未领取
        elseif self:HasUseUp(v) then
            state = 4 --已领完
        elseif self:HasGotten(v) then
            state = 3 --已领取
        end
        table.insert(result, {v,state})
    end
    table.sort(result, SortByTime)
    table.sort(result, SortByKey)
    return result
end

function GuildManager:CanGet(data)
    if self:HasUseUp(data) then
        return false
    end
    if self:HasGotten(data) then
        return false
    end
    return true
end

function GuildManager:HasUseUp(data)
    local count = data[public_config.GUILD_RED_ENVELOPE_INFO_GOTTEN_MONEY]
    if count >= data[public_config.GUILD_RED_ENVELOPE_INFO_MONEY_COUNT] then
        return true
    end
    return false
end
-------------------------------------客户端请求-----------------------------------
--创建公会
function GuildManager:BuildGuild(guildName, iconId, flagId, type)
    local params = string.format("%s,%d,%d,%d", guildName, iconId, flagId, type)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BUILD, params)
end

--快速加入公会
function GuildManager:QuickJoinGuildReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_QUICK_JOIN, "")
end

--获取公会列表
function GuildManager:GetGuildListReq(page)
    local params = string.format("%d", page)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_LIST, params)
end

--获取所在公会详细信息
function GuildManager:GetSelfGuildInfo()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_DETAIL_INFO, "")
end

--设置公会公告 type: 0:不通知 1:通知
function GuildManager:SetGuildBroadcastSettingReq(notice, type)
    type = type or 0
    local params = string.format("%s,%d", notice, type)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SET_ANNOUNCEMENT, params)
    -- 聊天监控：扑街
    PlayerManager.ChatManager.OnCheckChatMesseage(notice, public_config.CHAT_CHANNEL_GUILD_ANNO) 
end

--申请加入公会
function GuildManager:ApplyGuildReq(guildId)
    local params = string.format("%d", guildId)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_APPLY, params)
end

--获取申请列表信息
function GuildManager:GetApplyGuildListReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_APPLY_LIST, "")
end

--审批申请 reply: 0：通过 1：不通过
function GuildManager:ReplyGuildApplyReq(dbid, reply)
    local params = string.format("%d,%d", dbid, reply)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_REPLY_APPLY, params)
end

--审批全部申请 reply: 0：通过 1：不通过
function GuildManager:ReplyAllGuildApplyReq(reply)
    local params = string.format("%d", reply)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_REPLY_APPLY_ALL, params)
end

--设置是否需要审批
function GuildManager:SetGuildApproveReq(reply)
    local params = string.format("%d", reply)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SET_SETTING, params)
end

--公会签到 type: 1:普通 2:金币 3:白金
function GuildManager:GuildSignInReq(type)
    local params = string.format("%d", type)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SIGN_ON, params)
end

--请求退出公会
function GuildManager:GuildQuitdReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_QUIT, "")
end

--发公会系统红包
function GuildManager:GuildSendSysRedEnvelopeReq(id, count)
    local params = string.format("%d,%d", id, count)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SEND_SYS_RED_ENVELOPE, params)
end

--发公会个人红包
function GuildManager:GuildSendRedEnvelopeReq(money, count, blessing)
    local params = string.format("%d,%d,%s", money, count, blessing)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SEND_MONEY_RED_ENVELOPE, params)
end

--公会抢红包
function GuildManager:GuildGetRedEnvelopeReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GRAB_RED_ENVELOPE, params)
end

--获取公会现有红包列表
function GuildManager:GuildRedEnvelopeListReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_RED_ENVELOPE_LIST, "")
end

--获取公会技能信息
function GuildManager:GuildSkillInfoReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_SKILL_INFO, "")
end

--公会技能升级
function GuildManager:GuildSkillLevelUpReq(attri)
    local params = string.format("%d", attri)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SKILL_LEVEL_UP, params)
end

--获取公会商店信息
function GuildManager:GuildShopInfoReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SHOP_GET_INFO, "")
end

--邀请其他玩家加入公会
function GuildManager:GuildInviteReq(dbid)
    local params = string.format("%d", dbid)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_INVITE, params)
end

--回复邀请 result 0:同意 1:拒绝
function GuildManager:GuildReplyInviteReq(guildDbid, result)
    local params = string.format("%d,%d", guildDbid, result)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_REPLY_INVITE, params)
end

--进入公会领地
function GuildManager:GuildReplyInviteReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_TERRITORY_ENTER, "")
end

--设置公会职位
function GuildManager:GuildSetPositionReq(dbid, position)
    local params = string.format("%d,%d", dbid, position)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_SET_POSITION, params)
end

--踢出公会
function GuildManager:GuildKickReq(dbid)
    local params = string.format("%d", dbid)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_KICK, params)
end

--其他公会详细信息
function GuildManager:GuildGetInfoReq(guildId)
    local params = string.format("%d", guildId)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_INFO, params)
end

--所在公会成员信息
function GuildManager:GuildSelfMemberInfoReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_SELF_MEMBER_INFO, "")
end

--所在公会在线成员信息(组队功能需求)
function GuildManager:GuildSelfOnlineMemberInfoReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_ONLINE_INFO, "")
end

--其他公会成员信息
function GuildManager:GuildMemberInfoReq(guildId, page)
    local params = string.format("%d,%d", guildId, page)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_MEMBER_INFO, params)
end

--公会排行榜
function GuildManager:GuildRankListReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_RANK, "")
end

--搜素公会
function GuildManager:GuildResearchByNameReq(name)
    local params = string.format("%s", name)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_INFO_BY_NAME, params)
end


--领取每日公会工资
function GuildManager:GuildGetDailyRewardReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GET_DAILY_REWARD, "")
end

--公会背包
function GuildManager:GuildGetWarehouseBagReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_PKG, "")
end

--公会捐赠
function GuildManager:GuildWarehouseDonateReq(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_SELL, params)
end

--公会仓库批量销毁道具
function GuildManager:GuildWarehouseDestoryListReq(posList)
    local params = ""..posList[1]
    for i=2,#posList do
        params = params..","..posList[i]
    end
    GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_DESTROY, params)
end

--公会兑换
function GuildManager:GuildWarehouseBuyReq(pos, count)
    count = count or 1
    local params = string.format("%d", pos)
    if pos < 0 then
        --宠物丹兑换
        params = string.format("%d", count)
        GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_BUY_PET, params)
    else
        GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_BUY, params)
    end
end

--公会仓库记录
function GuildManager:GuildWarehouseRecordReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_WAREHOUSE_RECORD, "")
end

--获取公会所有的红包列表
function GuildManager:GetGuildRedEnvelopeListReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_RED_ENVELOPE_LIST, "")
end

--红包记录
function GuildManager:GetGuildRedEnvelopeRecordReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_RED_ENVELOPE_RECORD, "")
end

--设置审批最小等级和最小战力
function GuildManager:SetGuildApproveLevelAndFightReq(level, fight)
    local params = string.format("%d,%d", level, fight)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_APPROVE_LEVEL_AND_FIGHT, params)
end

--公会升级
function GuildManager:GuildLevelUpReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_LEVEL_UP, "")
end

-----------------------------------------公会神兽挑战----------------------------------------
--开启公会神兽挑战
function GuildManager:GuildMonsterStartReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BEAST_START, "")
end

--申请进入公会神兽
function GuildManager:GuildMonsterEnterReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BEAST_ENTER, "")
end

--提交灵魂
function GuildManager:GuildMonsterSubmitSoulReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BEAST_SUBMIT_SOUL, "")
end

--获取公会神兽信息
function GuildManager:GetGuildMonsterInfoReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BEAST_GET_INFO, "")
end

--获取公会神兽血量
function GuildManager:GetGuildMonsterHPReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_BEAST_GET_BOSS_HP, "")
end

-----------------------------------------公会令牌获取----------------------------------------
--获取公会精英怪刷新列表
function GuildManager:GetGuildEliteMonsterListReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_MONSTER_GET_DATA, "")
end

--进入世界boss（公会精英怪）
function GuildManager:GetGuildEliteMonsterEnterReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.guild_action_req(action_config.GUILD_MONSTER_ENTER_REQ, params)
end

-----------------------------------------守卫女神----------------------------------------
--进入守卫女神
function GuildManager:GuildGuardGoddnessEnterReq()
    GameWorld.Player().server.guild_action_req(action_config.GUILD_GUARD_ENTER, "")
end

function GuildManager:OpenDaily(funcId)
    if self:IsInGuild() then
        GUIManager.ShowPanelByFunctionId(funcId)
    else
        GUIManager.ShowPanel(PanelsConfig.ApplyForGuild)
    end
end



GuildManager:Init() 
return GuildManager