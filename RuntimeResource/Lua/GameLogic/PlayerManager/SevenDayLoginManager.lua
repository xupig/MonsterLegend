local SevenDayLoginManager = {}

local action_config = GameWorld.action_config
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local GUIManager = GameManager.GUIManager
local Welfare7dayRewardDataHelper = GameDataHelper.Welfare7dayRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RedPointManager = GameManager.RedPointManager
local public_config = GameWorld.public_config
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function SevenDayLoginManager:Init()
    self._weekDay = 7
    self:InitCallbackFunc()
end

function SevenDayLoginManager:InitCallbackFunc()
    self._onLoginChange = function() self:OnLoginChange() end
    self._onFunctionOpen = function(id) self:OnFunctionOpen(id) end
end

function SevenDayLoginManager:OnPlayerEnterWorld()
    self._maxLoginDay = #Welfare7dayRewardDataHelper:GetAllId()
    self:CheckStatus()
    self._canGetRewardNode = RedPointManager:CreateEmptyNode()
    -- self._canGetRewardNode:SetParent(RedPointManager:GetRedPointDataByTimingId(TimeLimitType.SevenDayLogin))
    self._canGetRewardNode:SetRedPoint(self:IsCanGetReward())
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SEVEN_LOGIN, self:IsCanGetReward())   
    EventDispatcher:AddEventListener(GameEvents.LOGIN_DAY_CHANGE, self._onLoginChange)
end

function SevenDayLoginManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.LOGIN_DAY_CHANGE, self._onLoginChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function SevenDayLoginManager:OnLoginChange()
    -- if self._canGetRewardNode then
    -- self._canGetRewardNode:SetRedPoint(self:IsCanGetReward(), true)
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SEVEN_LOGIN, self:IsCanGetReward())
    EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate) 
    EventDispatcher:TriggerEvent(GameEvents.SEVEN_DAY_REFRESH) 
    self:CheckStatus()
    -- end
end

function SevenDayLoginManager:RegistUpdateFunc(func)
    self._canGetRewardNode:SetUpdateRedPointFunc(func)
end

function SevenDayLoginManager:IsOpen()
    local get_reward_num = self:GetRewardDays()
    return FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_SEVEN_LOGIN)
end

function SevenDayLoginManager:CheckStatus()
    local get_reward_num = self:GetRewardDays()

    if get_reward_num >= self._maxLoginDay then
        --七天登录不需要显示
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.SevenDayLogin)
        EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, GameConfig.EnumType.OpenActivityType.SevenDay, false) 
    else
        --显示ICON
        if self:IsOpen() then
            self:ShowMenuIcon()
        else
            EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)            
        end

        --需要显示七天登录
        -- EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.SevenDayLogin, function()
        --     GUIManager.ShowPanel(PanelsConfig.SevenDayLogin)
        -- end)
    end
end

function SevenDayLoginManager:OnFunctionOpen(id)
    if id == public_config.FUNCTION_ID_SEVEN_LOGIN then
        self:ShowMenuIcon()
        EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
    end 
end

function SevenDayLoginManager:ShowMenuIcon()
    EventDispatcher:TriggerEvent(GameEvents.OpenActivityFuncIsAdd, GameConfig.EnumType.OpenActivityType.SevenDay, true) 
    local loginDay = GameWorld.Player().login_days--登录天数
    local getRewardDay = self:GetRewardDays()--已领取天数
    local iconDay = loginDay
    local iconsData
    if loginDay == getRewardDay then
        if getRewardDay == self._maxLoginDay then
            iconDay = self._maxLoginDay
        else
            iconDay = getRewardDay + 1
        end
    else
        if loginDay > self._maxLoginDay then
            iconDay = self._maxLoginDay
        end
    end

    local iconsData = Welfare7dayRewardDataHelper:GetIcons(iconDay)
    local text, icon
    if self:IsCanGetReward() then
        --LoggerHelper.Error("今日可领")
        text = LanguageDataHelper.CreateContent(81647)
    else
        --LoggerHelper.Error("明日可领")
        text = LanguageDataHelper.CreateContent(81648)
    end
    if iconsData and iconsData[5] then
        icon = iconsData[5]
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.SevenDayLogin, function()
            GUIManager.ShowPanel(PanelsConfig.SevenDayLogin)
        end,nil,nil,icon,text)
    else
        -- LoggerHelper.Error("SevenDayLoginManager:ShowMenuIcon loginDay Error =" .. tostring(loginDay))
    end
end

function SevenDayLoginManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        -- LoggerHelper.Log("SevenDayLoginManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.REWARD_SEVENDAY_GET_REWARD then --9101,   -- 领取七天奖励
        -- LoggerHelper.Log("Sam: 9101 == " .. PrintTable:TableToStr(args))
        self:HandleSuccessGetReward(args[1])
    else
        LoggerHelper.Error("SevenDayLoginManager HandleData action id not handle:" .. action_id)
    end
end

function SevenDayLoginManager:GetLoginDays()
    -- LoggerHelper.Log("Sam :登录天数====" .. GameWorld.Player().login_days)
    -- LoggerHelper.Log("Sam :已领取天数====" .. self:GetRewardDays())
    return GameWorld.Player().login_days
end

--获取能获得奖励的最小天数,如没有能获取的天数，则返回已获得奖励的最大天数
function SevenDayLoginManager:GetSelectDay(day)
    local index = 0
    local canGetNum = self:GetCanGetRewardNum()
    local get_reward_day = GameWorld.Player().get_reward_day
    local get_reward_num = self:GetRewardDays()
    local maxDays = #Welfare7dayRewardDataHelper:GetAllId()
    local week = math.ceil(get_reward_num / self._weekDay) - 1

    if get_reward_num == maxDays then
        return maxDays
    end

    if get_reward_num ~= 0 and get_reward_num % self._weekDay == 0 then
        return self._weekDay * (week + 1) + 1
    end

    if week > 0 then
        index = index + self._weekDay * week
    end

    for i=1,self._weekDay do
        if get_reward_day[i+ index] == nil then
            return i + index
        end
    end
    if day ~= nil then return day end
    return canGetNum + index + 1
end

function SevenDayLoginManager:HandleSuccessGetReward(day)
    -- self._canGetRewardNode:SetRedPoint(self:IsCanGetReward())
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SEVEN_LOGIN, self:IsCanGetReward())
    EventDispatcher:TriggerEvent(GameEvents.SEVEN_DAY_GET_REWQARD_SUCCESS, day)     
    self:CheckStatus()
end

--已经领取了奖励的天数
function SevenDayLoginManager:GetRewardDays()
    local get_reward_day = GameWorld.Player().get_reward_day
    local get_reward_num = 0
    for k,v in pairs(get_reward_day) do
        get_reward_num = get_reward_num + 1
    end

    return get_reward_num
end

function SevenDayLoginManager:GetCanGetRewardNum()
    local loginDay = GameWorld.Player().login_days--登录天数
    local getRewardDay = self:GetRewardDays()--已领取天数
    local maxDays = #Welfare7dayRewardDataHelper:GetAllId()
    local canGetNum
    
    if loginDay >= maxDays then
        canGetNum = maxDays - getRewardDay
    else
        canGetNum = loginDay - getRewardDay
    end
    return canGetNum
end
    
function SevenDayLoginManager:IsCanGetReward()
    local canGetNum = self:GetCanGetRewardNum()
    local getRewardDay = self:GetRewardDays()--已领取天数
    return canGetNum > 0
end

function SevenDayLoginManager:GetReward(day)
    GameWorld.Player().server.reward_sevenday_action_req(action_config.REWARD_SEVENDAY_GET_REWARD, tostring(day))
end

SevenDayLoginManager:Init()
return SevenDayLoginManager