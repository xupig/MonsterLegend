local PartnerManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local partnerData = PlayerManager.PlayerDataManager.partnerData
local bagData = PlayerManager.PlayerDataManager.bagData
local QualityType = GameConfig.EnumType.QualityType
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local TipsManager = GameManager.TipsManager
local ItemConfig = GameConfig.ItemConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

local UPDATE_INTERVAL = 200

function PartnerManager:Init()
end

function PartnerManager:TT()
    self._petDevourNode:CheckStrengthen(self:GetPetUpgradeRedPoint())
end

function PartnerManager:TT2()
    self._petDevourNode:CheckStrengthen(not self:GetPetUpgradeRedPoint())
end

function PartnerManager:OnPlayerEnterWorld()
    self._petUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.PET_UPGRADE, public_config.FUNCTION_ID_PET)--宠物升星
    self._petSoulNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.PET_SOUL, public_config.FUNCTION_ID_PET)--宠物兽魂
    self._petDevourNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.PET_DEVOUR, public_config.FUNCTION_ID_PET)--宠物吞噬
    self._rideUpgradeNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.HORSE_UPGRADE, public_config.FUNCTION_ID_HORSE)--坐骑升星
    self._rideSoulNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.HORSE_SOUL, public_config.FUNCTION_ID_HORSE)--坐骑兽魂
 
    self._petStarUpTimer = -1
    self._rideStarUpTimer = -1
    self._starUping = false
    self._starUpSelectedItem = 0
    self._starUpMarkStar = 0
    self._showPetDevourRedPoint = false
    self._showPetUpgradeRedPoint = false
    self._showPetSoulRedPoint = false
    self._showRideUpgradeRedPoint = false
    self._showRideSoulRedPoint = false
    self:AddEventListeners()

    self:RefreshPetDevourRedPointState()
    self:RefreshPetUpgradeRedPointState()
    self:RefreshPetSoulRedPointState()
    self:RefreshRideUpgradeRedPointState()
    self:RefreshRideSoulRedPointState()
    self:RefreshGuardianRedPointState()
end

function PartnerManager:OnPlayerLeaveWorld()
    self:RemoveEventListeners()
end

function PartnerManager:AddEventListeners()
    self._refreshPetDevourRedPointState = function() self:RefreshPetDevourRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Pet_Devour_Red_Point_State, self._refreshPetDevourRedPointState)
    self._refreshPetUpgradeRedPointState = function() self:RefreshPetUpgradeRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Pet_Upgrade_Red_Point_State, self._refreshPetUpgradeRedPointState)
    self._refreshRideUpgradeRedPointState = function() self:RefreshRideUpgradeRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Upgrade_Red_Point_State, self._refreshRideUpgradeRedPointState)
    self._refreshPetSoulRedPointState = function() self:RefreshPetSoulRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Pet_Soul_Red_Point_State, self._refreshPetSoulRedPointState)
    self._refreshRideSoulRedPointState = function() self:RefreshRideSoulRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Soul_Red_Point_State, self._refreshRideSoulRedPointState)
    self._refreshGuardianRedPointState = function() self:RefreshGuardianRedPointState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_Guardian_Red_Point_State, self._refreshGuardianRedPointState)
    self._onFunctionOpen = function(id) self:OnFunctionOpenCheck(id) end
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._onFunctionOpen)
end

function PartnerManager:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Pet_Devour_Red_Point_State, self._refreshPetDevourRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Pet_Upgrade_Red_Point_State, self._refreshPetUpgradeRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Upgrade_Red_Point_State, self._refreshRideUpgradeRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Pet_Soul_Red_Point_State, self._refreshPetSoulRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Soul_Red_Point_State, self._refreshRideSoulRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guardian_Red_Point_State, self._refreshGuardianRedPointState)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._onFunctionOpen)
end

function PartnerManager:HandlePetData(action_id,error_id,args)
    --LoggerHelper.Log("PartnerManager:HandlePetData "..action_id)
    --LoggerHelper.Log(args)
end

function PartnerManager:HandleRideData(action_id,error_id,args)
    --LoggerHelper.Log("HandleRideData "..action_id)
    --LoggerHelper.Log(args)
end


-----------------------------数据处理---------------------------
function PartnerManager:RefreshPetInfo(changeValue)
    partnerData:RefreshPetInfo(changeValue)
end

function PartnerManager:RefreshHorseGradeShow(changeValue)
    if GameWorld.Player().horse_grade_ride ~= 0 then
        self:HorseRideReq()
    end
end

--返回宠物神羽道具使用次数
function PartnerManager:GetPetItemUsedInfo(itemId)
    return partnerData:GetPetItemUsedInfo(itemId)
end

function PartnerManager:RefreshRideInfo(changeValue)
    partnerData:RefreshRideInfo(changeValue)
end

--返回坐骑神羽道具使用次数
function PartnerManager:GetRideItemUsedInfo(itemId)
    return partnerData:GetRideItemUsedInfo(itemId)
end

-------------------------------------功能接口-----------------------------------
-----------------------宠物部分-----------------
function PartnerManager:CheckSelectedBless()
    local blessItemData = PartnerDataHelper.GetPetBlessItems()
    local select = 1
    for i=1, 2 do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(blessItemData[i][1])
        if bagNum > 0 then
            select = i
            break
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Selected_Bless_Item, select)
end

function PartnerManager:SetStarUpSelectedItem(itemId)
    self._starUpSelectedItem = itemId
end

function PartnerManager:StartStarUp()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedItem)
    if pos == 0 then
        self:CheckSelectedBless()
        pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedItem)
    end
    if self._petStarUpTimer == -1 and pos > 0 then
        self._starUpMarkStar = GameWorld.Player().pet_star
        self._petStarUpTimer = GameWorld.TimerHeap:AddTimer(0, UPDATE_INTERVAL, 0, function() self:CheckStarUp() end)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Star_Up_State)
    end

    if pos <= 0 then
        self:BuyGuideTip(self._starUpSelectedItem)
    end
end

function PartnerManager:EndStarUp()
    if self._petStarUpTimer ~= -1 then
        GameWorld.TimerHeap:DelTimer(self._petStarUpTimer)
        self._petStarUpTimer = -1
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Star_Up_State)
    end
end

function PartnerManager:CheckStarUp()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedItem)
    if pos == 0 then
        self:CheckSelectedBless()
        pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedItem)
    end
    if pos <= 0 then
        self:EndStarUp()
        self:BuyGuideTip(self._starUpSelectedItem)
        return
    end
    if self._starUpMarkStar ~= GameWorld.Player().pet_star then
        --self:EndStarUp()
        --return
    end
    self:PetStarUpReq(pos)
end

function PartnerManager:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end

function PartnerManager:InStarUpState()
    if self._petStarUpTimer == -1 then
        return false
    end
    return true
end

function PartnerManager:RefreshPetDevourRedPointState()
    local equips = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemListByItemType(public_config.ITEM_ITEMTYPE_EQUIP)
    local levelData = PartnerDataHelper.GetPetLevelItems()
    for k,v in pairs(levelData) do
        if bagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(k) then
            self:SetPetDevourRedPoint(true)
            return
        end
    end
    for k,v in pairs(equips) do
        --type 11和12不纳入吞噬范围
        if v.cfgData.quality == QualityType.Purple and v.cfgData.grade <= 100 and v.cfgData.star <= 0 and v.cfgData.type <= 10 then
            self:SetPetDevourRedPoint(true)
            return
        end
    end
    self:SetPetDevourRedPoint(false)
end

function PartnerManager:SetPetDevourRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PET) then
        return
    end
    if self._showPetDevourRedPoint ~= state then
        self._showPetDevourRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pet_Devour_Red_Point, self._showPetDevourRedPoint)
        self._petDevourNode:CheckStrengthen(self:GetPetDevourRedPoint())
    end
end

function PartnerManager:GetPetDevourRedPoint()
    self:RefreshPetDevourRedPointState()
    return self._showPetDevourRedPoint
end

function PartnerManager:RefreshPetUpgradeRedPointState()
    local blessItemData = PartnerDataHelper.GetPetBlessItems()
    for k,v in pairs(blessItemData) do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
        if bagNum > 0 then
            self:SetPetUpgradeRedPoint(true)
            return
        end
    end
    self:SetPetUpgradeRedPoint(false)
end

function PartnerManager:SetPetUpgradeRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PET) then
        return
    end
    if self._showPetUpgradeRedPoint ~= state then
        self._showPetUpgradeRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pet_Upgrade_Red_Point, self._showPetUpgradeRedPoint)
        self._petUpgradeNode:CheckStrengthen(self:GetPetUpgradeRedPoint())
    end
end

function PartnerManager:GetPetUpgradeRedPoint()
    self:RefreshPetUpgradeRedPointState()
    return self._showPetUpgradeRedPoint
end

function PartnerManager:RefreshPetSoulRedPointState()
    local featherData = PartnerDataHelper.GetPetEvoItems()
    for k,v in pairs(featherData) do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
        if bagNum > 0 then
            self:SetPetSoulRedPoint(true)
            return
        end
    end
    self:SetPetSoulRedPoint(false)

end

function PartnerManager:SetPetSoulRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_PET) then
        return
    end
    if self._showPetSoulRedPoint ~= state then
        self._showPetSoulRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pet_Soul_Red_Point, self._showPetSoulRedPoint)
        self._petSoulNode:CheckStrengthen(self:GetPetSoulRedPoint())
    end
end

function PartnerManager:GetPetSoulRedPoint()
    self:RefreshPetSoulRedPointState()
    return self._showPetSoulRedPoint
end

--获取宠物红点状态
function PartnerManager:GetPetViewRedPoint()
    return self:GetPetSoulRedPoint() or self:GetPetUpgradeRedPoint() or self:GetPetDevourRedPoint()
end

function PartnerManager:OnFunctionOpenCheck(id)
    if id == public_config.FUNCTION_ID_PET then
        self:RefreshPetSoulRedPointState()
        self:RefreshPetUpgradeRedPointState()
        self:RefreshPetDevourRedPointState()
    end

    if id == public_config.FUNCTION_ID_HORSE then
        self:RefreshRideUpgradeRedPointState()
        self:RefreshRideSoulRedPointState()
    end

    if id == public_config.FUNCTION_ID_GUARDIAN then
        self:RefreshGuardianRedPointState()
    end
end
-----------------------坐骑部分-------------
function PartnerManager:CheckSelectedBlessRide()
    local blessItemData = PartnerDataHelper.GetRideBlessItems()
    local select = 1
    for i=1, 2 do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(blessItemData[i][1])
        if bagNum > 0 then
            select = i
            break
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Selected_Bless_Item, select)
end

function PartnerManager:SetStarUpSelectedRideItem(itemId)
    self._starUpSelectedRideItem = itemId
end

function PartnerManager:StartRideStarUp()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedRideItem)
    if pos <= 0 then
        self:CheckSelectedBlessRide()
        pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedRideItem)
    end
    if self._rideStarUpTimer == -1 and pos > 0 then
        self._starUpRideMarkStar = GameWorld.Player().horse_star
        self._rideStarUpTimer = GameWorld.TimerHeap:AddTimer(0, UPDATE_INTERVAL, 0, function() self:CheckRideStarUp() end)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Star_Up_State)
    end

    if pos <= 0 then
        self:BuyGuideTip(self._starUpSelectedRideItem)
    end
end

function PartnerManager:EndRideStarUp()
    if self._rideStarUpTimer ~= -1 then
        GameWorld.TimerHeap:DelTimer(self._rideStarUpTimer)
        self._rideStarUpTimer = -1
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Star_Up_State)
    end
end

function PartnerManager:CheckRideStarUp()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedRideItem)
    if pos <= 0 then
        self:CheckSelectedBlessRide()
        pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._starUpSelectedRideItem)
    end
    if pos <= 0 then
        self:EndRideStarUp()
        self:BuyGuideTip(self._starUpSelectedRideItem)
        return
    end
    self:RideStarUpReq(pos)
end

function PartnerManager:InRideStarUpState()
    if self._rideStarUpTimer == -1 then
        return false
    end
    return true
end

function PartnerManager:RefreshRideUpgradeRedPointState()
    local blessItemData = PartnerDataHelper.GetRideBlessItems()
    for k,v in pairs(blessItemData) do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
        if bagNum > 0 then
            self:SetRideUpgradeRedPoint(true)
            return
        end
    end
    self:SetRideUpgradeRedPoint(false)
end

function PartnerManager:SetRideUpgradeRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HORSE) then
        return
    end
    if self._showRideUpgradeRedPoint ~= state then
        self._showRideUpgradeRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Ride_Upgrade_Red_Point, self._showRideUpgradeRedPoint)
        self._rideUpgradeNode:CheckStrengthen(self:GetRideUpgradeRedPoint())
    end
end

function PartnerManager:GetRideUpgradeRedPoint()
    self:RefreshRideUpgradeRedPointState()
    return self._showRideUpgradeRedPoint
end

function PartnerManager:RefreshRideSoulRedPointState()
    local featherData = PartnerDataHelper.GetRideEvoItems()
    for k,v in pairs(featherData) do
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(v[1])
        if bagNum > 0 then
            self:SetRideSoulRedPoint(true)
            return
        end
    end
    self:SetRideSoulRedPoint(false)
end

function PartnerManager:SetRideSoulRedPoint(state)
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HORSE) then
        return
    end
    if self._showRideSoulRedPoint ~= state then
        self._showRideSoulRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Ride_Soul_Red_Point, self._showRideSoulRedPoint)
        self._rideSoulNode:CheckStrengthen(self:GetRideSoulRedPoint())
    end
end

function PartnerManager:GetRideSoulRedPoint()
    self:RefreshRideSoulRedPointState()
    return self._showRideSoulRedPoint
end

--获取坐骑红点状态
function PartnerManager:GetRideViewRedPoint()
    return self:GetRideUpgradeRedPoint() or self:GetRideSoulRedPoint()
end

-------------------------------------客户端请求-----------------------------------
-----------------------宠物部分-----------------
--宠物升星
function PartnerManager:PetStarUpReq(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.pet_action_req(action_config.PET_STAR_UP, params)
end

--宠物升级
function PartnerManager:PetLevelUpReq(posList)
    local params =""
    for i=1, #posList-1 do
        params = params .. posList[i]..","
    end
    params = params .. posList[#posList]
    GameWorld.Player().server.pet_action_req(action_config.PET_LEVEL_UP, params)
end

--宠物显示
function PartnerManager:PetShowReq(grade)
    local params = string.format("%d", grade)
    GameWorld.Player().server.pet_action_req(action_config.PET_SHOW, params)
end

--宠物隐藏
function PartnerManager:PetHideReq()
    GameWorld.Player().server.pet_action_req(action_config.PET_UNSHOW, "")
end


-----------------------坐骑部分-----------------
--坐骑升星
function PartnerManager:RideStarUpReq(pos)
    local params = string.format("%d", pos)
    GameWorld.Player().server.horse_action_req(action_config.HORSE_STAR_UP, params)
end

--坐骑勾选
function PartnerManager:RideShowReq(grade)
    local params = string.format("%d", grade)
    GameWorld.Player().server.horse_action_req(action_config.HORSE_SHOW, params)
end

--下马
function PartnerManager:RideHideReq()
    GameWorld.Player().server.horse_action_req(action_config.HORSE_UNRIDE, "")
end

--上马
function PartnerManager:HorseRideReq()
    GameWorld.Player().server.horse_action_req(action_config.HORSE_RIDE, "")
end

-----------------------守护部分-----------------
--守护升级/激活
function PartnerManager:GuardianUpgradeReq(guardianId)
    local params = string.format("%d", guardianId)
    GameWorld.Player().server.guardian_action_req(action_config.GUARDIAN_UPLEVEL, params)
end

--守护出战
function PartnerManager:GuardianSelectFightReq(guardianId)
    local params = string.format("%d", guardianId)
    GameWorld.Player().server.guardian_action_req(action_config.GUARDIAN_SELECT, params)
end

--守护结晶分解
function PartnerManager:GuardianDecomposeReq(posList)
    GameWorld.Player().server.guardian_action_req(action_config.GUARDIAN_EXPLAIN, posList)
end

--分解成功
function PartnerManager:GuardianDecomposeComplete(error_id)
    if error_id == 0 then
        EventDispatcher:TriggerEvent(GameEvents.Guardian_Decompose_Complete)
    end
end

--升级成功
function PartnerManager:GuardianUpgradeComplete(error_id)
    if error_id == 0 then
        self:RefreshGuardianRedPointState()
        EventDispatcher:TriggerEvent(GameEvents.Guardian_Upgrade_Complete)
    end
end

function PartnerManager:GetDecomposeData()
    local baseList = PartnerDataHelper:GetGuardianBaseList()
    local guardianInfo = GameWorld.Player().guardian_info
    local guardianCount = #baseList
    local resolveCfgList = PartnerDataHelper:GetGuardianResolveList()
    local itemIds = {}

    for k,v in pairs(resolveCfgList) do
        itemIds[k] = true
    end

    for i=1,guardianCount do
        local cost = PartnerDataHelper:GetGuardianAttriCfg(i,0).cost
        if cost then
            for k,v in pairs(cost) do
                if guardianInfo[i] then
                    itemIds[k] = true
                else
                    itemIds[k] = false
                end
            end
        end
    end

    local decomposeListData = {}
    local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
    local curVolumn = GameWorld.Player().pkg_items_size
    for i=1,curVolumn do
        local itemData = t[i]
        if itemData then
            if itemIds[itemData.cfg_id] then
                table.insert(decomposeListData,itemData)
            end
        end
    end

    return decomposeListData
end

--刷新守护红点
function PartnerManager:RefreshGuardianRedPointState()
    if not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_GUARDIAN) then
        return
    end
    local state = false
    local baseList = PartnerDataHelper:GetGuardianBaseList()
    local guardianInfo = GameWorld.Player().guardian_info
    for i=1,#baseList do
        local guadianId = baseList[i].id
        local grade = guardianInfo[guadianId] or 0
        local attriCfg = PartnerDataHelper:GetGuardianAttriCfg(guadianId,grade)
        if attriCfg.cost then
            for itemId,needCount in pairs(attriCfg.cost) do
                local hasCount
                if itemId < public_config.MAX_MONEY_ID then
                   hasCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[itemId]]
                else
                   hasCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
                end

                if hasCount >= needCount then
                    if grade > 0 then
                        state = true
                    else
                        local beforeId = guadianId - 1
                        local beforeGrade = guardianInfo[beforeId] or 0
                        if beforeGrade >= 10 then
                            state = true
                        end
                    end
                end
            end
        else
            state = true
        end
    end
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_GUARDIAN, state)
end

PartnerManager:Init()
return PartnerManager