local CommonWaitManager = {}
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local TimerHeap = GameWorld.TimerHeap

local CommonQueue = Class.CommonQueue(ClassTypes.XObject)

function CommonQueue:Init()
    self._common = {}
end

function CommonQueue:AddQueue(data)
    table.insert(self._common, 1, data)
end

function CommonQueue:GetFirstQueue()
    if #self._common > 0 then
        return self._common[#self._common]
    else
        return nil
    end
end

function CommonQueue:GetLastQueue()
    if #self._common > 0 then
        return self._common[1]
    else
        return nil
    end
end

function CommonQueue:RemoveQueue()
    if #self._common <= 0 then
        return nil
    end
    local data = self._common[#self._common]
    table.remove(self._common, #self._common)
    return data
end

function CommonQueue:RemmoveQueueByTitle(title)
    local index = -1
    for i=#self._common - 1, 1, -1 do
        local data = self._common[i]
        if data.wait_title == title then
            if data.wait_times > 1 then
                data.wait_times = data.wait_times - 1
            else
                index = i
            end
            break
        end
    end

    if index ~= -1 then
        table.remove(self._common, index)
    end
end

function CommonQueue:GetLength()
    return #self._common
end

-- 这里需要初始化
function CommonWaitManager:Init()
    self._waitingQueue = CommonQueue()
    self._waitingQueue:Init()

    self._endWaitLoadingFirstFun = function(callback) self:EndWaitLoadingFirst(callback) end
    EventDispatcher:AddEventListener(GameEvents.EndWaitLoadingFirst, self._endWaitLoadingFirstFun)
end

-- 手动开启，
function CommonWaitManager:BeginWaitLoading(waiting_title)
    -- LoggerHelper.Error("手动开启：" .. tostring(waiting_title), true)
    local lastWait = self._waitingQueue:GetLastQueue()
    if lastWait ~= nil and lastWait.wait_title == waiting_title then
        lastWait.wait_times = lastWait.wait_times + 1
        if self._waitingQueue:GetLength() > 1 then return end
    else
        local argsData = {}
        argsData.wait_title = waiting_title
        argsData.wait_times = 1
        self._waitingQueue:AddQueue(argsData)
    end
    

    local firstWait = self._waitingQueue:GetFirstQueue()
    if self._waitingQueue:GetLength() == 1 and firstWait.wait_times == 1 then 
        GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    end
end
-- 手动关闭
function CommonWaitManager:EndWaitLoading(waiting_title)
    if self._waitingQueue:GetLength() < 1 then return end
    -- LoggerHelper.Error(tostring(self._waitingQueue:GetLength()) .. ":手动关闭：" .. tostring(waiting_title), true)
    local curWait = self._waitingQueue:GetFirstQueue()
    if curWait ~= nil and curWait.wait_title == waiting_title then
        curWait.wait_times = curWait.wait_times - 1
        if curWait.wait_times <= 0 then
            self._waitingQueue:RemoveQueue()
        end
    else
        self._waitingQueue:RemmoveQueueByTitle(waiting_title)
    end

    if self._waitingQueue:GetLength() < 1 then
        GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
    end

end

-- 到达15s自动关闭，
function CommonWaitManager:EndWaitLoadingFirst(callback)
    -- LoggerHelper.Error("自动关闭：" .. tostring(self._waitingQueue:GetLength()))
    local curWait = self._waitingQueue:GetFirstQueue()
    if curWait ~= nil  then
        curWait.wait_times = curWait.wait_times - 1
        if curWait.wait_times <= 0 then
            self._waitingQueue:RemoveQueue()
        end
    end
    if self._waitingQueue:GetLength() < 1 then
        GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
    else
        callback()
    end
end

--[[
-- 参数delays:延迟几秒开启等待界面，单位s
function CommonWaitManager:BeginCommonWait(delay)
    local delay = (delay~=nil) and delay or 0

    if self._commonWaitTimer ~= nil then
        TimerHeap:DelTimer(self._commonWaitTimer)
        self._commonWaitTimer = nil
    end
    if delay > 0 then
        self._commonWaitTimer = TimerHeap:AddTimer(delay * 1000, 0, 0, function()
            GUIManager.ShowPanel(PanelsConfig.CommonWait)
        end)
    else
        GUIManager.ShowPanel(PanelsConfig.CommonWait)
    end

end
-- PlayerManager.CommonWaitManager:BeginCommonWait(0)
-- PlayerManager.CommonWaitManager:EndCommonWait()
function CommonWaitManager:EndCommonWait()
    if self._commonWaitTimer ~= nil then
        TimerHeap:DelTimer(self._commonWaitTimer)
        self._commonWaitTimer = nil
    end
    GUIManager.ClosePanel(PanelsConfig.CommonWait)
end
--]]
CommonWaitManager:Init()

return CommonWaitManager