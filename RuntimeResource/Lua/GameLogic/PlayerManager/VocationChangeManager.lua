local VocationChangeManager = {}

local vocationChangeData = PlayerManager.PlayerDataManager.vocationChangeData
local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local StringStyleUtil = GameUtil.StringStyleUtil
local TaskDataHelper = GameDataHelper.TaskDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskConfig = GameConfig.TaskConfig
local GUIManager = GameManager.GUIManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local TaskManager = PlayerManager.TaskManager
local TracingPointData = ClassTypes.TracingPointData
local ItemConfig = GameConfig.ItemConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType

function VocationChangeManager:Init()
    self:InitCallbackFunc()
end

function VocationChangeManager:InitCallbackFunc()
    self._onTaskInitComplete = function()self:OnTaskInitComplete() end
    self._onTaskChanged = function()self:OnTaskChanged() end
    self._onVocationChange = function() self:OnVocationChange() end
end

function VocationChangeManager:OnPlayerEnterWorld()
    self._vocationGroup = GameWorld.Player():GetVocationGroup()
    self._vocationChangeNum = GameWorld.Player():GetVocationChange()

    vocationChangeData:Init()
    EventDispatcher:AddEventListener(GameEvents.TASK_INIT_COMPLETE, self._onTaskInitComplete)
    EventDispatcher:AddEventListener(GameEvents.UPDATE_VOCATION_TASK, self._onTaskChanged)
    EventDispatcher:AddEventListener(GameEvents.ON_VOCATION_CHANGE, self._onVocationChange)
end

function VocationChangeManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.TASK_INIT_COMPLETE, self._onTaskInitComplete)
    EventDispatcher:RemoveEventListener(GameEvents.UPDATE_VOCATION_TASK, self._onTaskChanged)
    EventDispatcher:RemoveEventListener(GameEvents.ON_VOCATION_CHANGE, self._onVocationChange)
end

function VocationChangeManager:OnVocationChange()
    if self._vocationGroup ~= GameWorld.Player():GetVocationGroup() then
        self._vocationGroup = GameWorld.Player():GetVocationGroup()
        self._vocationChangeNum = GameWorld.Player():GetVocationChange()
        do return end
    end

    if GameWorld.Player():GetVocationChange() > self._vocationChangeNum then
        -- GameManager.GUIManager.ShowPanel(PanelsConfig.VocationChange, self._vocationChangeNum)
        self._vocationChangeNum = GameWorld.Player():GetVocationChange()
        self._hasVocation = false
        EventDispatcher:TriggerEvent(GameEvents.OnVocationChangeStatusChange)  
    end

end

--只是提供给主界面菜单使用
function VocationChangeManager:OnTaskChanged()
    -- if self:IsDoingVocationChange() and self._hasVocation == false then        
    --     --登录时没有转职任务
    --     self._hasVocation = true
    --     EventDispatcher:TriggerEvent(GameEvents.OnVocationChangeStatusChange)
    --     -- GameManager.GUIManager.ShowPanel(PanelsConfig.VocationChange)
    -- end
    if TaskManager:IsInitTaskComplete() then
        EventDispatcher:TriggerEvent(GameEvents.OnVocationChangeStatusChange)
    end
end

function VocationChangeManager:OnTaskInitComplete()
    self._hasVocation = self:IsDoingVocationChange()
end

--根据职业组id和转数获取职业名称
--groupId:职业组id
--changeNum:转数
function VocationChangeManager:GetVocationName(groupId, changeNum)
    return vocationChangeData:GetVocationName(groupId, changeNum)
end

--是否在第几转
function VocationChangeManager:IsDoingVocationChangeByNum(num)
    -- LoggerHelper.Error("num ============" .. tostring(num), true)
    local numChangeNum = GameWorld.Player():GetVocationChange() + 1
    if numChangeNum ~= num then
        return false
    end
    return self:IsDoingVocationChange()
end

--是否在转职阶段
function VocationChangeManager:IsDoingVocationChange()
    local task = TaskManager:GetVocationTask()
    if not table.isEmpty(task) then
        -- LoggerHelper.Error("task ===" .. PrintTable:TableToStr(task))
        return true
    else
        --完成的转职任务列表
        local commitedOptionalTask = TaskManager:GetCommitedOptionalTask()
        if table.isEmpty(commitedOptionalTask) then
            return false
        end
        local ids = ChangeVocationDataHelper:GetAllId()
        local lastCompleteId
        local lastCompleteTaskId
        for i,id in ipairs(ids) do
            local taskId = ChangeVocationDataHelper:GetTaskId(id)
            if taskId ~= 0 then
                if commitedOptionalTask[taskId] ~= nil then
                    lastCompleteId = id
                    lastCompleteTaskId = taskId
                else
                    break
                end
            end
        end
        if lastCompleteTaskId == nil then 
            return false
        end
        local changeIds = ChangeVocationDataHelper:GetIdsByTaskId(lastCompleteTaskId)
        local lastStep = ChangeVocationDataHelper:GetStage(changeIds[#changeIds])
        if lastCompleteId == changeIds[#changeIds] then
            return false
        end
        -- LoggerHelper.Log("lastCompleteId ===" .. tostring(lastCompleteId))
        return true
    end
end

--获取下一阶段的转职任务数据(仅在已经开始转职任务，但未完成转职时使用,1-3转使用)
--idx:第几阶段，如果为nil则获取最新任务阶段
function VocationChangeManager:GetNextStepTask(idx)
    --完成的转职任务列表
    local commitedOptionalTask = TaskManager:GetCommitedOptionalTask()
    local ids = ChangeVocationDataHelper:GetAllId()
    local lastCompleteId
    local lastCompleteTaskId
    local isLast = false
    for i,id in ipairs(ids) do
        local taskId = ChangeVocationDataHelper:GetTaskId(id)
        if taskId ~= nil and taskId ~= 0 then
            if commitedOptionalTask[taskId] ~= nil then
                lastCompleteId = id
                lastCompleteTaskId = taskId
            else
                lastCompleteTaskId = taskId
                break
            end
        end
    end
    if idx ~= nil then
        local changeNum = ChangeVocationDataHelper:GetChangeNum(lastCompleteId)
        local changeId = ChangeVocationDataHelper:GetIdByChangeNumAndStage(changeNum, idx)
        lastCompleteTaskId = ChangeVocationDataHelper:GetTaskId(changeId)
    end
    --LoggerHelper.Error("lastCompleteTaskId ====" .. tostring(lastCompleteTaskId))
    return TaskManager:NewTaskItemDataWithState(lastCompleteTaskId, public_config.TASK_STATE_ACCEPTABLE)
end

--获取转职的目标进度描述(转职界面使用, 未完成则进度显示红色，完成则显示绿色)
--vocationNum:几转(如：需要查看1转转职第一阶段的任务目标时传0)
--stage:阶段
--vocationNum、stage为nil时返回当前的任务进度
function VocationChangeManager:GetVocationTargetText(vocationNum, stage)
    local nowVocationNum = GameWorld.Player():GetVocationChange()
    if nowVocationNum == vocationNum then
        local task = VocationChangeManager:GetNextStepTask()
        local nowStage = ChangeVocationDataHelper:GetStage(ChangeVocationDataHelper:GetIdByTaskId(task:GetId()))
        if nowStage == stage then
            return self:GetNowTargetText()
        else
            local taskId = ChangeVocationDataHelper:GetTaskId(ChangeVocationDataHelper:GetIdByChangeNumAndStage(vocationNum, stage))
            local targetsInfo = TaskDataHelper:GetTargetsInfo(taskId)
            if targetsInfo == nil then
                return self:GetTalkToSomeOneContent(TaskDataHelper:GetCommitNPC(taskId))
            else
                local desc = TaskDataHelper:GetTargetsDesc(taskId)
                local totalTimes = targetsInfo[1][2]
                for k,v in pairs(desc) do
                    return self:GetTimesText(totalTimes, totalTimes, LanguageDataHelper.CreateContent(v))
                end
            end
        end
    else
        local taskId = ChangeVocationDataHelper:GetTaskId(ChangeVocationDataHelper:GetIdByChangeNumAndStage(vocationNum, stage))
        local targetsInfo = TaskDataHelper:GetTargetsInfo(taskId)
        if targetsInfo == nil then
            return self:GetTalkToSomeOneContent(TaskDataHelper:GetCommitNPC(taskId))
        else
            local desc = TaskDataHelper:GetTargetsDesc(taskId)
            local totalTimes = targetsInfo[1][2]
            for k,v in pairs(desc) do
                return self:GetTimesText(totalTimes, totalTimes, LanguageDataHelper.CreateContent(v))
            end
        end
    end

    if vocationNum == nil and stage == nil then
        
    else

    end
end

function VocationChangeManager:GetNowTargetText()
    local tasks =  TaskManager:GetVocationTask()   
    local task

    if table.isEmpty(tasks) then        
        local task = self:GetNextStepTask()
        return LanguageDataHelper.CreateContent(TaskDataHelper:GetExplain(task:GetId()))
    else        
        for i, t in pairs(tasks) do
            task = t
        end

        local targetsInfo = task:GetTargetsInfo()
        local targetsDesc = task:GetTargetsDesc()
        local targetsTime = task:GetTargetsCurTimes()
        local taskState = task:GetState()
        local taskItemType = task:GetTaskItemType()

        if taskState == public_config.TASK_STATE_COMPLETED
            or taskState == public_config.TASK_STATE_ACCEPTABLE then
            if task:GetSelfLimit() > GameWorld.Player().level then
                return LanguageDataHelper.CreateContent(179, {["0"] = task:GetSelfLimit()})
            else
                for i, event in ipairs(targetsInfo) do
                    return self:GetTimesText(0, event:GetTotalTimes(), event:GetTalkToSomeOneContent())
                end
            end
        else
            local event = TaskCommonManager:GetNextEvent(task)
            local id = event:GetId()
            return self:GetTimesText(targetsTime[id] or 0, event:GetTotalTimes(), LanguageDataHelper.CreateContent(targetsDesc[id], LanguageDataHelper.GetArgsTable()))
        end
    end
end

function VocationChangeManager:GetTimesText(curTimes, totalTimes, content)
    local colorId = 106
    if curTimes >= totalTimes then
        colorId = 102
    end
    if totalTimes == 1 then
        return content
    end    
    return content .. StringStyleUtil.GetColorStringWithColorId(" (" .. curTimes .. "/" .. totalTimes .. ") ", colorId)
end

function VocationChangeManager:GetTalkToSomeOneContent(npcId)
    local npcData = NPCDataHelper.GetNPCData(npcId)
    local npcName = LanguageDataHelper.CreateContent(npcData.name)
    return LanguageDataHelper.CreateContentWithArgs(TaskConfig.TALK_TO_SOMEONE, {["0"] = npcName})
end

function VocationChangeManager:FastCompleteVocation3()
    local a = {}
    local data = {}
    a.id = MessageBoxType.Tip
    a.value =  data
    data.confirmCB=function()self:FastVocationChange() end
    data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
    data.text = LanguageDataHelper.CreateContent(73135,{["0"] = GlobalParamsHelper.GetParamValue(854)})
    GUIManager.ShowPanel(PanelsConfig.MessageBox, a)
end

function VocationChangeManager:FastVocationChange()
    local cost = GlobalParamsHelper.GetParamValue(854)
    local haveCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]
    if haveCount < cost then
        local a = {}
        local data = {}
        a.id = MessageBoxType.Tip
        a.value =  data
        data.confirmCB=function()self:GotoCharge() end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.Tips) end
        data.text = LanguageDataHelper.CreateContent(73137)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, a)
    else
        self:FastVocationChange()
        GUIManager.ClosePanel(PanelsConfig.Tips)
    end
end

function VocationChangeManager:GotoCharge()
    GUIManager.ClosePanel(PanelsConfig.VocationChange)
    GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_CHARGE)
end

function VocationChangeManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("VocationChangeManager Error: error_id==" .. error_id .. "  action_id ==" .. action_id)
        return
    elseif action_id == action_config.VOC_CHANGE_FOUR_ACTIVATE then--8751, --4转激活命格
        EventDispatcher:TriggerEvent(GameEvents.VOCATION_FOUR_STAGE_CHANGE)
    elseif action_id == action_config.VOC_CHANGE_FOUR_APPLY_SUCC then--8752, --4转转生成功请求
        GUIManager.ClosePanel(PanelsConfig.VocationChange)
        GUIManager.ShowText(1, LanguageDataHelper.CreateContent(73050))
    elseif action_id == action_config.ONE_KEY_FINISH then--10301, --一键转职（三转）
        -- LoggerHelper.Log("Sam：一键转职===" .. tostring(PrintTable:TableToStr(args)))
        GUIManager.ClosePanel(PanelsConfig.VocationChange)
        local vocationTask = TaskManager:GetVocationTask()
        local taskId = GlobalParamsHelper.GetParamValue(866)
        if not table.isEmpty(vocationTask) then
            for i, task in pairs(vocationTask) do
                if task:GetId() == taskId then
                    TaskCommonManager:OnClickTrackCurTask(TracingPointData(task))
                    break
                end
            end
        end
    else
        LoggerHelper.Error("VocationChangeManager action id not handle:" .. action_id)
    end
end

--8751, --4转激活命格
function VocationChangeManager:VocationFourActive(id)
    GameWorld.Player().server.voc_change_four_action_req(action_config.VOC_CHANGE_FOUR_ACTIVATE, tostring(id))
end

--8752, --4转转生成功请求
function VocationChangeManager:VocationFourChange()
    GameWorld.Player().server.voc_change_four_action_req(action_config.VOC_CHANGE_FOUR_APPLY_SUCC, tostring(0))
end

--10301, --一键转职（三转）
function VocationChangeManager:FastVocationChange()
    GameWorld.Player().server.voc_change_action_req(action_config.ONE_KEY_FINISH, "")
end

VocationChangeManager:Init()
return VocationChangeManager