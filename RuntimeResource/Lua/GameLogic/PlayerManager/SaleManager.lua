local SaleManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local RedPointManager = GameManager.RedPointManager
local AddChargeDays = 7
function SaleManager:Init()
	self._saleBuy = function ()self:BuyResp()end
	self._opentimeCB = function() self:OpenTimer() end
end

function SaleManager:OnPlayerEnterWorld()
	self:OpenTimer()
	EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._opentimeCB)	
	EventDispatcher:AddEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DAZZLE_SALE_INFO, self._saleBuy)	

end

function SaleManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._opentimeCB)
	EventDispatcher:RemoveEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._opentimeCB)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._opentimeCB)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DAZZLE_SALE_INFO, self._saleBuy)
end

function SaleManager:BuyResp()
	
end

--Resp
function SaleManager:HandleResp(action_id,error_id,lua_table)
	if error_id~=0 then
		return
	end
	if action_id == action_config.DAZZLE_SALE_BUY then
		EventDispatcher:TriggerEvent(GameEvents.SaleBuyResp)  		
	end
end

-- Rpc
function SaleManager:SaleRpc(id)
	GameWorld.Player().server.dazzle_sale_action_req(action_config.DAZZLE_SALE_BUY,id..'')	
end


function SaleManager:SetSaleBuyName(name)
	self._buyName = name
end

function SaleManager:GetSaleBuyName()
	return self._buyName
end
function SaleManager:OpenTimer()
	local f = FunctionOpenDataHelper:CheckPanelOpen(410)
	if not f then
		return 
	end
	local totaltime = AddChargeDays*DateTimeUtil.OneDayTime
	local createtime = GameWorld.Player().create_time
	local s = DateTimeUtil.MinusSec(createtime)
	if totaltime>s then
		local days=DateTimeUtil.GetCurMonthTodayNum()
		local times = DateTimeUtil.SomeDay(DateTimeUtil.GetServerTime())
		local a = times['day']+AddChargeDays
		if a>days then
			times['month']=times['month']+1
			times['day']=a-days
		else
			times['day']=a
		end
		local endtimestamp = DateTimeUtil.NewTime(times['year'],times['month'],times['day'],0,0,0)
		EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.Sale, function()
			GUIManager.ShowPanel(PanelsConfig.Sale)
		end,endtimestamp)	
		self._timerid = TimerHeap:AddSecTimer(totaltime-s,0,0,function ()
			EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Sale)
			TimerHeap:DelTimer(self._timerid)
		end)			
	else
		
	end
end


SaleManager:Init()
return SaleManager