local EquipmentInstanceManager = {}

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local BloodDungeonDataHelper = GameDataHelper.BloodDungeonDataHelper
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceManager = PlayerManager.InstanceManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local GUIManager = GameManager.GUIManager
local RedPointManager = GameManager.RedPointManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager

function EquipmentInstanceManager:Init()
    self:InitCallbackFunc()
end

function EquipmentInstanceManager:InitRedpointNodes()
    self._equipNode = RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_EQUIPMENT_INSTANCE)
end

function EquipmentInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function EquipmentInstanceManager:OnPlayerEnterWorld()
    self:InitRedpointNodes()
    self:UpdateRedPoint()
    --LoggerHelper.Error("EquipmentInstanceManager:OnPlayerEnterWorld()")
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function EquipmentInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function EquipmentInstanceManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then
        self._instanceId = nil
        self._startTime = nil
    end
end

function EquipmentInstanceManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_EQUIPMENT then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        self._instanceId = BloodDungeonDataHelper:GetIdByMapId(mapId)
        InstanceManager:EnterInstance()
        GameWorld.Player():StartRide()
    end
end

function EquipmentInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("EquipmentInstanceManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.INSTANCE_EQUIPMENT_NOTIFY_ROOM_STAGE then     --7551,     --副本状态变化
        -- LoggerHelper.Error("sam 7551 == " .. PrintTable:TableToStr(args)) 
        -- {}
        -- [1] = room_stage,   --房间状态
        -- [2] = start_time,   --状态开始时间
        local data = {}
        if args[1] == public_config.INSTANCE_STAGE_3 then--等待光门倒计时 --同步给客户端，说明光门开始倒计时
            --副本开始倒计时,如：5秒后开始副本
            local time = GlobalParamsHelper.GetParamValue(536)
            if time ~= 0 then
                InstanceCommonManager:ShowBeginCountDown(time, tonumber(args[2]))
            end
        elseif args[1] == public_config.INSTANCE_STAGE_4 then--同步给客户端，说明光门关闭，开始玩法，刷怪
            --副本开始开始计时，如03:30后结束副本。副本开始计时时需要先显示评价星级
            GameWorld.Player():StartAutoFight()

            self._startTime = tonumber(args[2])
            data.start = 3
            data.startTimeData = GlobalParamsHelper.GetParamValue(534)
            data.dropMessages = InstanceConfig.EQUIPMENT_INSTANCE_START_DROP_MESSAGE_MAP
            data.countDownMessages = InstanceConfig.EQUIPMENT_INSTANCE_COUNT_DOWN_MESSAGE_MAP
            data.cb = function(start) self:MoveStartEnd(start) end
            data.startTime = tonumber(args[2])
            InstanceCommonManager:ShowStart(data)  
        else
        end
    elseif action_id == action_config.INSTANCE_EQUIPMENT_SETTLE then                --7554,     --结算信息
        -- LoggerHelper.Error("sam 7554 == " .. PrintTable:TableToStr(args))
        InstanceBalanceManager:ShowBalance(args)
    elseif action_id == action_config.INSTANCE_EQUIPMENT_NOTIFY_KILL_MONSTERS then  --7553,     --波数变化、杀怪数量、总杀怪数量
        -- LoggerHelper.Error("sam 7553 == " .. PrintTable:TableToStr(args))
        local rideDatas = BloodDungeonDataHelper:GetRide(self._instanceId)
        if rideDatas and args[2] == args[3] and rideDatas[args[1]] == 1 then
            GameWorld.Player():StartRide()
        end

        local data = {}
        data.instanceId = self._instanceId
        data.num = args[1]--当前波数
        data.totalNum = args[2]--前波怪物总数
        data.killNum = args[3]--当前波已杀死怪物数量
        GUIManager.ShowPanel(PanelsConfig.EquipmentInstanceInfo, data)
    else
        LoggerHelper.Error("EquipmentInstanceManager HandleData action id not handle:" .. action_id)
    end
end

--星级评价移动完毕
function EquipmentInstanceManager:MoveStartEnd(start)
    if start == 3 then
        self:BeginCountDown()
    end
end

function EquipmentInstanceManager:BeginCountDown()
    local instanceTime = GlobalParamsHelper.GetParamValue(531)
    InstanceCommonManager:StartInstanceCountDown(instanceTime, self._startTime)
end

function EquipmentInstanceManager:OnUpdateInfo()
    self:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.EQUIPMENT_INSTANCE_INFO_CHANGE)
end

function EquipmentInstanceManager:UpdateRedPoint()
    if self._equipNode and FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_EQUIPMENT_INSTANCE) then
        local equipementInfo = GameWorld.Player().equipment_instance_info
        local commonTimes = GlobalParamsHelper.GetParamValue(532)
        local fightNums = equipementInfo[public_config.INSTANCE_EQUIPMENT_KEY_DAY_PASS_NUM]
        self._equipNode:SetRedPoint((commonTimes - fightNums) > 0)
    end
end

EquipmentInstanceManager:Init()
return EquipmentInstanceManager