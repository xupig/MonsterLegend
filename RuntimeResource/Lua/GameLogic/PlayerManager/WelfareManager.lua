local WelfareManager = {}

local GUIManager = GameManager.GUIManager
local action_config = GameWorld.action_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local WelfareData = PlayerManager.PlayerDataManager.welfareData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareDayRewardDataHelper = GameDataHelper.WelfareDayRewardDataHelper
local WelfareNoticeRewardDataHelper = GameDataHelper.WelfareNoticeRewardDataHelper
local WelfareLevelUpDataHelper = GameDataHelper.WelfareLevelUpDataHelper
local public_config = GameWorld.public_config
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestVipDataHelper = GameDataHelper.InvestVipDataHelper
local TotalChargeDataHelper = GameDataHelper.TotalChargeDataHelper
local OnlineRewardDataHelper = GameDataHelper.OnlineRewardDataHelper
function WelfareManager:Init()
-- EventDispatcher:AddEventListener(GameEvents.ON_ACTION_RESP, function(actId, code, args)
--     self:OnActionResp(actId, code, args)
-- end)
end

function WelfareManager:HandleData(action_id, error_code, args)
    if error_code == 0 then
        if action_id == action_config.BENEFIT_DAILY_BONUS_REQ then
            --领取签到奖励
			self:GetDailyBonusResq(args)
			EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
			self:RefreshRed()
        elseif action_id == action_config.BENEFIT_CUMULATIVE_REWARD_REQ then
            --领取累计签到额外奖励
			self:GetCumulativeRewardResq(args)
			EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
			self:RefreshRed()
        elseif action_id == action_config.BENEFIT_LEVEL_UP_REWARD_REQ then
            --领取冲级奖励
			self:GetLevelUpRewardResq(args)
			EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
            self:RefreshRed()
            
        --EventDispatcher:TriggerEvent(GameEvents.ON_GET_WELFARE_REWARD_COMPLETE, args[1])
        elseif action_id == action_config.BENEFIT_EXCHANGE_COUPON_REQ then
            --领取兑换券奖励（这个没做，说先不用做）
        elseif action_id == action_config.BENEFIT_NOTICE_REWARD_REQ then
            --领取更新公告奖励
			self:GetNoticeRewardResq(args)
			EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
			self:RefreshRed()
        elseif action_id == action_config.BENEFIT_GET_RECEIVED_INFO_REQ then
            --获取已领冲级奖励信息
			self:GetReceivedInfoResq(args)
			EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
            self:RefreshRed()  
        elseif action_id == action_config.ONLINE_REWARD_REQ then
            --LoggerHelper.Error("ONLINE_REWARD_REQ")
            self:GetOnlineReceivedInfoResq(args)
            EventDispatcher:TriggerEvent(GameEvents.WelfareRedUpdate)
            self:RefreshRed()
        end
    end
end

-----------------------------------------------------返回---------------------------------------------------------------------------------------------------
--签到
function WelfareManager:GetDailySignInfo()
    return GameWorld.Player().daily_sign_info
end

--累计签到档位
function WelfareManager:GetCumulativeSignInfo()
    return GameWorld.Player().daily_cumulative_sign_reward_step
end

--累计签到档位
function WelfareManager:GetTotalSignDayNum()
    return table.getTableCount(GameWorld.Player().daily_sign_info)
end


--冲级豪礼奖励
function WelfareManager:GetLevelUpRewardInfo()
    return GameWorld.Player().level_up_reward_info
end



--更新公告奖励
function WelfareManager:GetNoticeRewardInfo()
    return GameWorld.Player().notice_reward
end

--领取签到奖励返回
function WelfareManager:GetDailyBonusResq(args)
    -- local daily = GameWorld.Player().daily_sign_info
    EventDispatcher:TriggerEvent(GameEvents.GetDailyBonusResq)
    --LoggerHelper.Log("data ..GetDailyBonusResq" .. PrintTable:TableToStr(args))
end

--领取累计签到额外奖励
function WelfareManager:GetCumulativeRewardResq(args)
    EventDispatcher:TriggerEvent(GameEvents.GetCumulativeRewardResq)
    --LoggerHelper.Log("data ..GetCumulativeRewardResq" .. PrintTable:TableToStr(args))
end

--领取更新公告奖励
function WelfareManager:GetNoticeRewardResq(args)
    EventDispatcher:TriggerEvent(GameEvents.UpdateAnnnounceReward)
    --LoggerHelper.Log("data ..GetNoticeRewardResq" .. PrintTable:TableToStr(args))
end

--领取冲级奖励
function WelfareManager:GetLevelUpRewardResq(args)
    self:SendGetReceivedInfoReq()
    --EventDispatcher:TriggerEvent(GameEvents.UpdateLevelUpRewardList)
    --LoggerHelper.Log("data ..GetLevelUpRewardResq" .. PrintTable:TableToStr(args))
end

--获取已领冲级奖励信息
function WelfareManager:GetReceivedInfoResq(args)
    WelfareData:UpdateLevelUpListData(args)
    --LoggerHelper.Log("data ..GetReceivedInfoResq" .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.UpdateLevelUpRewardList)
end

--获取冲级奖励信息
function WelfareManager:GetOnlineReceivedInfoResq(args)
    WelfareData:UpdateLevelUpListData(args)
    --LoggerHelper.Log("data ..GetReceivedInfoResq" .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.UpdateOnlineLevelUpRewardList)
end


--每日签到红点
function WelfareManager:GetDailyIsShowRed()
    local dailyInfo = self:GetDailySignInfo()
    local timeDay = DateTimeUtil.Now()
    if dailyInfo[timeDay["day"]] == 1 then  
    else
        return true
    end
	
	
    local ids = WelfareDayRewardDataHelper:GetAllId()
    self._needIds = {}
    for i, id in ipairs(ids) do
        local levelTab = WelfareDayRewardDataHelper:GetLv(id)
        local sumId = {}
        if GameWorld.Player().level >= levelTab[1] and GameWorld.Player().level <= levelTab[2] then
            table.insert(self._needIds, id)
        end
	end
	
	local levelRewardInfo = self:GetCumulativeSignInfo() 
    local dailyCum = self._needIds[levelRewardInfo + 1]
    
    if self:GetTotalSignDayNum() >= WelfareDayRewardDataHelper:GetDay(dailyCum) then
		return true
    end
    
    return false
end

--在线
function WelfareManager:GetOnlineMaxTime()
    local ids = OnlineRewardDataHelper:GetAllId()
    local worldLevel = GameWorld.Player().world_level
    local ordId = {}
    for i,id in ipairs(ids) do
        local levelTab = OnlineRewardDataHelper:GetWorldLevel(id)
        if worldLevel >= levelTab[1] and worldLevel <= levelTab[2] then
            table.insert(ordId,id)
        end
    end

    return  OnlineRewardDataHelper:GetTime(tonumber(ordId[#ordId])) or 0
end

--在线
function WelfareManager:GetOnlineIsShowRed()
    local ids = OnlineRewardDataHelper:GetAllId()

    local levelReward = WelfareManager:GetOnlineRewardInfo()
    local worldLevel = GameWorld.Player().world_level
    local alreadyId = {}
    local noGetId = {}
    local onlineTime =  self:GetDailyOnlineMin()
    --LoggerHelper.Error("onlineTime"..onlineTime)
    for i,id in ipairs(ids) do
        local levelTab = OnlineRewardDataHelper:GetWorldLevel(id)
        
        if worldLevel >= levelTab[1] and worldLevel <= levelTab[2] then
            if levelReward[id] == 1 then
            else
                local onlineTime = self:GetDailyOnlineMin()
                local needTime =  OnlineRewardDataHelper:GetTime(id)
                if onlineTime >= needTime then
                    --LoggerHelper.Error("onlineTime"..onlineTime.."needTime"..needTime)
                    return true
                else    
                end
             end
        end
    end

    return false
end


--冲级红点
function WelfareManager:GetLevelIsShowRed()
	local ids = WelfareLevelUpDataHelper:GetAllId()
	local levelInfo = self:GetLevelUpRewardInfo()
    
	for i,levelId in ipairs(ids) do
		local level = WelfareLevelUpDataHelper:GetLevel(levelId)
		if GameWorld.Player().level >= level then
            if levelInfo[levelId] == 1 then
                
            else
                local leftNumData = WelfareData:GetLevelUpListData()
                local rewardCount = WelfareLevelUpDataHelper:GetGetCount(levelId)
                
                if rewardCount and rewardCount > 0 and leftNumData then
                    --LoggerHelper.Error(" WelfareManager:GetLevelIsShowRed()=============")
                    for k, v in pairs(leftNumData) do
                        if k == levelId then
                            --LoggerHelper.Error(" WelfareManager:GetLevelIsShowRed()=============22")
                            if rewardCount - v > 0 then
                                --LoggerHelper.Error(" WelfareManager:GetLevelIsShowRed()=============")
                                return true
                            end
                        end
                    end
                else
                    return true
                end	
            end
        end
    end

    return false
end


--公告
function WelfareManager:GetAnnounceIsShowRed()
	local ids = WelfareNoticeRewardDataHelper:GetAllId()
    local canAnnounceId = {}
    for i, id in ipairs(ids) do
        if WelfareNoticeRewardDataHelper:GetUse(id) == 1 then
             canAnnounceId = id
        end
	end
	
	local announceReward = self:GetNoticeRewardInfo()
	if canAnnounceId == announceReward then
	
	else
		return true
    end
    
    return false
end



function WelfareManager:OnPlayerEnterWorld()
    self:SendGetReceivedInfoReq()
    self:AddEventListeners()
    self:RefreshRed()
end

function WelfareManager:AddEventListeners()
    self._onWelfareGiftChangeCB = function() self:OnWelfareGiftChange() end
    self._onWelfareOnlineChangeCB = function() self:OnWelfareOnineChange() end		
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onWelfareGiftChangeCB)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DAILY_ONLINE_MIN, self._onWelfareOnlineChangeCB)
end

function WelfareManager:RefreshRed()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_ANNOUNCE, self:GetAnnounceIsShowRed())
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_GIFT, self:GetLevelIsShowRed())
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_SIGN, self:GetDailyIsShowRed()) 
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_ONLINE,self:GetOnlineIsShowRed())
end



function WelfareManager:OnPlayerLeaveWorld()
    self:RemoveEventListeners()
end


function WelfareManager:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onWelfareGiftChangeCB)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DAILY_ONLINE_MIN, self._onWelfareOnlineChangeCB)
end

function WelfareManager:OnWelfareGiftChange()
    --LoggerHelper.Error("WelfareManager:OnWelfareGiftChange()")
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_GIFT, self:GetLevelIsShowRed())
    
end

function WelfareManager:OnWelfareOnineChange()
    --LoggerHelper.Error("OnWelfareOnineChange()")
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_WELFARE_ONLINE,self:GetOnlineIsShowRed())
end

-------------------------------------------------------------------------------------------------------------------------------









---------------------------------------------请求-------------------------------------------------------------------------------------------------------------------
--领取签到奖励
function WelfareManager:SendDailyBonusReq()
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_DAILY_BONUS_REQ, "")
end

--领取累计签到奖励
function WelfareManager:SendCumulativeRewardReq()
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_CUMULATIVE_REWARD_REQ, "")
end

--领取更新公告奖励
function WelfareManager:SendNoticeRewardReq()
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_NOTICE_REWARD_REQ, "")
end

--获取已领冲级奖励信息
function WelfareManager:SendGetReceivedInfoReq()
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_GET_RECEIVED_INFO_REQ, "")
end

--领取冲级奖励
function WelfareManager:SendLevelUpRewardReq(giftId)
    local id = tostring(giftId)
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_LEVEL_UP_REWARD_REQ, id)
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
function WelfareManager:ShowRewardItem()
-- body
end

function WelfareManager:ShowRewardItem(rewardList)
-- GUIManager.ShowPanel(PanelsConfig.GetProps, {items = rewardList, title = "获得物品"})
-- for i, v in ipairs(rewardList) do
-- 	local data = ItemDataHelper.GetItem(v.itemId)
-- 	if data then
-- 		local name = LanguageDataHelper.CreateContent(data.name)
-- 		local tips = LanguageDataHelper.CreateContent(11, {["0"] = name, ["1"] = v.itemNum})
-- 		--"获得" .. name .. " x" .. addValue
-- 		GUIManager.ShowText(1, tips)
-- 	end
-- end
end

--领取冲级奖励
function WelfareManager:SendLevelUpRewardReq(giftId)
    local id = tostring(giftId)
    GameWorld.Player().server.benefit_action_req(action_config.BENEFIT_LEVEL_UP_REWARD_REQ, id)
end


--领取在线奖励
function WelfareManager:SendOnlineRewardReq(giftId)
    local id = tostring(giftId)
    GameWorld.Player().server.online_reward_action_req(action_config.ONLINE_REWARD_REQ,id)
end

--领取在线奖励
function WelfareManager:GetOnlineRewardInfo()
    return GameWorld.Player().online_reward_info or {}
end

--领取在线奖励
function WelfareManager:GetDailyOnlineMin()
    return GameWorld.Player().daily_online_min or {}
end




WelfareManager:Init()
return WelfareManager
