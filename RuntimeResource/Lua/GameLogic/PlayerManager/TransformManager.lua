--幻化
local TransformManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local cnIndex = {80,223,224,225,27059,81,532,40016}
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RedPointManager = GameManager.RedPointManager
local RedPointConfig = GameConfig.RedPointConfig
local TreasureManager = PlayerManager.TreasureManager
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function TransformManager:Init()
	self:InitCallback()
end

function TransformManager:InitCallback()
	self._refreshRedPointCb = function (systemType)
		self:RefreshTransformRedPoint(systemType)
	end

	-- self._refreshPetRedPointCb = function ()
	-- 	self:RefreshPetTransformRedPoint()
	-- end

	-- self._refreshRideRedPointCb = function ()
	-- 	self:RefreshRideTransformRedPoint()
	-- end
end

function TransformManager:OnPlayerEnterWorld()
	self._transformRedPointStates = {}
	self._transformRedPointStates[public_config.TREASURE_TYPE_WING] = false
	self._transformRedPointStates[public_config.TREASURE_TYPE_TALISMAN] = false
	self._transformRedPointStates[public_config.TREASURE_TYPE_WEAPON] = false
	self._transformRedPointStates[public_config.TREASURE_TYPE_PET] = false
	self._transformRedPointStates[public_config.TREASURE_TYPE_HORSE] = false

	self._transformNodes = {}
	self._transformNodes[public_config.TREASURE_TYPE_WING] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WING_TRANSFORM, public_config.FUNCTION_ID_ILLUSION_WING)--翅膀幻化
	self._transformNodes[public_config.TREASURE_TYPE_TALISMAN] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.TALISMAN_TRANSFORM, public_config.FUNCTION_ID_ILLUSION_WEAPON_MAGIC)--魂器幻化
	self._transformNodes[public_config.TREASURE_TYPE_WEAPON] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.WEAPON_TRANSFORM, public_config.FUNCTION_ID_ILLUSION_WEAPON_GOD)--神兵幻化
	self._transformNodes[public_config.TREASURE_TYPE_PET] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.PET_TRANSFORM, public_config.FUNCTION_ID_ILLUSION_PET) --宠物幻化
	self._transformNodes[public_config.TREASURE_TYPE_HORSE] = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.RIDE_TRANSFORM, public_config.FUNCTION_ID_ILLUSION_VS11) --坐骑幻化

	self._transformNodes[public_config.TREASURE_TYPE_WING]:SetParent(RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_WING))
	self._transformNodes[public_config.TREASURE_TYPE_TALISMAN]:SetParent(RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_TALISMAN))
	self._transformNodes[public_config.TREASURE_TYPE_WEAPON]:SetParent(RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_WEAPON))
	self._transformNodes[public_config.TREASURE_TYPE_PET]:SetParent(RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_PET))
	self._transformNodes[public_config.TREASURE_TYPE_HORSE]:SetParent(RedPointManager:GetRedPointDataByFuncId(public_config.FUNCTION_ID_HORSE))

	self._petBlessItemData = PartnerDataHelper.GetPetBlessItems()
	self._rideblessItemData = PartnerDataHelper.GetRideBlessItems()
	EventDispatcher:AddEventListener(GameEvents.Refresh_Transform_Red_Point_State,self._refreshRedPointCb)
	-- EventDispatcher:AddEventListener(GameEvents.Refresh_Pet_Upgrade_Red_Point_State,self._refreshPetRedPointCb)
	-- EventDispatcher:AddEventListener(GameEvents.Refresh_Ride_Upgrade_Red_Point_State,self._refreshRideRedPointCb)

	self:RefreshAllTransformRedPoint()
end

function TransformManager:GetNodeByType(type)
	return self._transformNodes[type]
end

function TransformManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Transform_Red_Point_State,self._refreshRedPointCb)
	-- EventDispatcher:RemoveEventListener(GameEvents.Refresh_Pet_Upgrade_Red_Point_State,self._refreshPetRedPointCb)
	-- EventDispatcher:RemoveEventListener(GameEvents.Refresh_Ride_Upgrade_Red_Point_State,self._refreshRideRedPointCb)
end

function TransformManager:GetTransformRedPointState(type)
	return self._transformRedPointStates[type]
end

function TransformManager:RefreshTransformRedPoint(systemType)
	--宝物
	if systemType <= public_config.TREASURE_TYPE_WEAPON then
		self:CheckTreasureRedPoint(systemType)
	--伙伴
	elseif systemType >= public_config.TREASURE_TYPE_PET then
		self:CheckPartnerRedPoint(systemType)
	end
	EventDispatcher:TriggerEvent(GameEvents.Update_Transform_Red_Point_State)
end

function TransformManager:RefreshAllTransformRedPoint()
	self:CheckTreasureRedPoint(public_config.TREASURE_TYPE_WING)
	self:CheckTreasureRedPoint(public_config.TREASURE_TYPE_TALISMAN)
	self:CheckTreasureRedPoint(public_config.TREASURE_TYPE_WEAPON)

	self:CheckPartnerRedPoint(public_config.TREASURE_TYPE_PET)
	self:CheckPartnerRedPoint(public_config.TREASURE_TYPE_HORSE)

	EventDispatcher:TriggerEvent(GameEvents.Update_Transform_Red_Point_State)
end

function TransformManager:CheckTreasureRedPoint(treasureType)
	if treasureType == public_config.TREASURE_TYPE_WING and not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ILLUSION_WING) then
		return
	end

	if treasureType == public_config.TREASURE_TYPE_TALISMAN and not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ILLUSION_WEAPON_MAGIC) then
		return
	end

	if treasureType == public_config.TREASURE_TYPE_WEAPON and not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ILLUSION_WEAPON_GOD) then
		return
	end

	local srcData = TransformDataHelper:GetTransformTotalCfgList(treasureType)
	local treasureInfo = self:GetTreasureActivateData(treasureType)
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local state = false
	for i=1,#srcData do
		local cfg = srcData[i]
		local star = treasureInfo[cfg.id] or 0
		local attriCfg = TransformDataHelper:GetTreasureStarCfg(treasureType,cfg.id,star)
		local costItem
		local needNum
		local vocationLimit = true
		if star == 0 then
			if cfg.activate_limit then
				local needVocation = cfg.activate_limit[vocationGroup]
				if GameWorld.Player().vocation < needVocation then
					vocationLimit = false
				end
			end
		end
		if star <  5 then
			costItem = attriCfg.star_lvup_cost[vocationGroup]
			needNum = costItem[2]
		else
			costItem = cfg.activate_num[vocationGroup]
			needNum = 1
		end

		local itemId = costItem[1]
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
		if bagNum >= needNum and vocationLimit then
			state = true
			break
		end
	end
	self._transformRedPointStates[treasureType] = state
	self._transformNodes[treasureType]:CheckStrengthen(state)
end

function TransformManager:GetBlessItemData(partnerType)
	if partnerType == public_config.TREASURE_TYPE_PET then
            return self._petBlessItemData
        else
            return self._rideblessItemData
        end
end

function TransformManager:CheckPartnerRedPoint(partnerType)
	-- LoggerHelper.Error("CheckPartnerRedPoint")
	if partnerType == public_config.TREASURE_TYPE_PET and not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ILLUSION_PET) then
		return
	end

	if partnerType == public_config.TREASURE_TYPE_HORSE and not FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ILLUSION_VS11) then
		return
	end

	local srcData = TransformDataHelper:GetTransformTotalCfgList(partnerType)
	local partnerInfoList = self:GetPartnerTransformData(partnerType)
	local state = false
	for i=1,#srcData do
		local cfg = srcData[i]
		local partnerInfo = partnerInfoList[cfg.id]
		--未激活
		if partnerInfo == nil then
			local vocationGroup = math.floor(GameWorld.Player().vocation/100)
			local costItem = cfg.activate_num[vocationGroup]
			local itemId = costItem[1]
			local needNum = costItem[2]
			local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
			if bagNum >= needNum then
				state = true
				break
			end
		--已激活(激活的伙伴幻化暂时屏蔽升级红点)
		-- else
		-- 	local blessItemData = self:GetBlessItemData(partnerType)
			
  --           for i=1,#blessItemData do
  --           	local itemId = blessItemData[i][1]
  --           	local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
  --           	if bagNum >= 1 then
		-- 			state = true
		-- 			break
		-- 		end
  --           end
		end
	end
	self._transformRedPointStates[partnerType] = state
	self._transformNodes[partnerType]:CheckStrengthen(state)
end

function TransformManager:HandleResp(act_id, code, args)
	if code > 0 then
		return
	end
	if act_id == action_config.ILLUSION_ACTIVATE then
		self:UpdateTransformActivate()
	elseif act_id == action_config.ILLUSION_UPGRADE_STAR then
		self:UpdateTransformUpStar()
	elseif act_id == action_config.ILLUSION_UPGRADE_GRADE then
		self:UpdateTransformUpgrade(args)
	elseif act_id == action_config.ILLUSION_LEVEL_UP then
		self:UpdateTransformUpLevel()
	elseif act_id == action_config.ILLUSION_BREAK then
		self:UpdateTransformDecompose()
	elseif act_id == action_config.ILLUSION_SHOW then
		self:UpdateTransformOnShow()
	end
end

function TransformManager:UpdateTransformActivate()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_ACTIVATE_COMPLETE)
end

function TransformManager:UpdateTransformUpStar()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_UPSTAR_COMPLETE)
end

function TransformManager:UpdateTransformUpgrade(args)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_UPGRADE_COMPLETE,args)
end

function TransformManager:UpdateTransformDecompose()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_DCOMPOSE_COMPLETE)
end

function TransformManager:UpdateTransformUpLevel()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_UPLEVEL_COMPLETE,true)
end

function TransformManager:UpdateTransformOnShow()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW)
end

------------------------------------------------------------------------------------------
function TransformManager:GetSystemCn(systemType)
	return LanguageDataHelper.CreateContent(cnIndex[systemType])
end

function TransformManager:GetTreasureActivateData(systemType)
	return self:GetTreasureData(systemType)[public_config.TREASURE_ILLUSION_KEY_ACTIVATE_INFO]
end

function TransformManager:GetTreasureLevel(systemType)
	return self:GetTreasureData(systemType)[public_config.TREASURE_ILLUSION_KEY_LEVEL] or 0
end

function TransformManager:GetTreasureExp(systemType)
	return self:GetTreasureData(systemType)[public_config.TREASURE_ILLUSION_KEY_EXP] or 0
end

function TransformManager:GetTreasureData(systemType)
	if systemType == public_config.TREASURE_TYPE_WING then
		return GameWorld.Player().illusion_wing_info
	elseif systemType == public_config.TREASURE_TYPE_TALISMAN then
		return GameWorld.Player().illusion_talisman_info
	elseif systemType == public_config.TREASURE_TYPE_WEAPON then
		return GameWorld.Player().illusion_weapon_info
	elseif systemType == public_config.TREASURE_TYPE_CLOAK then
		return GameWorld.Player().illusion_cloak_info
	end
end

function TransformManager:CheckTransformShow(systemType,transformId)
	local showInfo = GameWorld.Player().illusion_show_info[systemType]
	if showInfo == nil then
		LoggerHelper.Error("请先跑任务开启相关宝物/伙伴功能")
		return
	end
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_ILLUSION and 
        showInfo[public_config.TREASURE_ILLUSION_SHOW_ID] == transformId then
        return true
   else
   		return false
   end
end

-- function TransformManager:GetTreasureTransformShow(systemType)
-- 	if systemType == public_config.TREASURE_TYPE_WING then
-- 		return GameWorld.Player().illusion_wing_show or 0
-- 	elseif systemType == public_config.TREASURE_TYPE_TALISMAN then
-- 		return GameWorld.Player().illusion_talisman_show or 0
-- 	elseif systemType == public_config.TREASURE_TYPE_WEAPON then
-- 		return GameWorld.Player().illusion_weapon_show or 0
-- 	elseif systemType == public_config.TREASURE_TYPE_CLOAK then
-- 		return GameWorld.Player().illusion_cloak_show or 0
-- 	end
-- end

function TransformManager:GetPartnerTransformData(systemType)
	if systemType == public_config.TREASURE_TYPE_PET then
		return GameWorld.Player().illusion_pet_info
	elseif systemType == public_config.TREASURE_TYPE_HORSE then
		return GameWorld.Player().illusion_horse_info
	end
end

function TransformManager:GetComposeList(systemType)
	--激活过的幻化信息
    local activateInfo
    if systemType <= 4 then 
        activateInfo = self:GetTreasureActivateData(systemType)
    elseif systemType == public_config.TREASURE_TYPE_PET then
        activateInfo = GameWorld.Player().illusion_pet_info
    elseif systemType == public_config.TREASURE_TYPE_HORSE then
        activateInfo = GameWorld.Player().illusion_horse_info
    end

    --{可分解道具Id = 分解经验}
	local composeItemIds = {}
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)

    for transformId,v in pairs(activateInfo) do
    	local cfg = TransformDataHelper.GetTreasureTotalCfg(transformId)
    	local itemId = cfg.activate_num[vocationGroup][1]
        composeItemIds[itemId] = cfg.resolve
    end

    local decomposeListData = {}
    local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
    local curVolumn = GameWorld.Player().pkg_items_size
    for i=1,curVolumn do
        local itemData = t[i]
        if itemData then
            if composeItemIds[itemData.cfg_id] then
                table.insert(decomposeListData,itemData)
            end
        end
    end

    return composeItemIds,decomposeListData
end

function TransformManager:InitPlayerModel()
	local info =  GameWorld.Player().illusion_show_info
	for systemType,v in pairs(info) do
		if systemType == public_config.TREASURE_TYPE_WING or systemType == public_config.TREASURE_TYPE_WEAPON then
			--LoggerHelper.Error("systemType"..systemType)
			self:HandleModelShow(systemType,v[public_config.TREASURE_ILLUSION_SHOW_SYSTEM],v[public_config.TREASURE_ILLUSION_SHOW_ID])
		end
	end
end

function TransformManager:OnModelShow(change)
	if change == nil then
		return
	end
	for systemType,v in pairs(change) do
		if systemType == public_config.TREASURE_TYPE_WING or systemType == public_config.TREASURE_TYPE_WEAPON then
			self:HandleModelShow(systemType,v[public_config.TREASURE_ILLUSION_SHOW_SYSTEM],v[public_config.TREASURE_ILLUSION_SHOW_ID])
		end
	end
end

function TransformManager:GetWeaponModelId()
	return self._weaponModelId or 0
end

--统一处理幻化玩家外观模型
function TransformManager:HandleModelShow(systemType,subSystemType,cfgId)
	if not GameWorld.Player().actorHasLoaded then
		return
	end
	local uiModelCfgId
	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	if subSystemType == public_config.TREASURE_SHOW_ORIGIN_COMMON then
	    uiModelCfgId = GlobalParamsHelper.GetParamValue(703+systemType)[vocationGroup]
	elseif subSystemType == public_config.TREASURE_SHOW_ORIGIN_ILLUSION then
	    uiModelCfgId = TransformDataHelper.GetTreasureTotalCfg(cfgId).model_vocation[vocationGroup]
	end

	local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    local modelId = modelInfo.model

	if systemType == public_config.TREASURE_TYPE_WING then
		GameWorld.Player():SetCurWing(modelId)
	--法宝由实体自行处理
	elseif systemType == public_config.TREASURE_TYPE_TALISMAN then
	elseif systemType == public_config.TREASURE_TYPE_WEAPON  then
		self._weaponModelId = modelId
		GameWorld.Player():ShowCurWeapon()
	elseif systemType == public_config.TREASURE_TYPE_CLOAK then
	end
end

----------------------------------幻化请求-----------------------------------------
--化形激活
function TransformManager:RequestActivateTransform(transformId)
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_ACTIVATE,tostring(transformId))
end

--宝物升星
function TransformManager:RequestTransformUpStar(transformId)
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_UPGRADE_STAR,tostring(transformId))
end

--伙伴升阶
function TransformManager:RequestTransformUpgrade(transformId,bagPos)
	local str = transformId..","..bagPos
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_UPGRADE_GRADE,str)
end

--升级
function TransformManager:RequestTransformUpLevel(type)
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_LEVEL_UP,tostring(type))
end

--分解
function TransformManager:RequestTransformDecompose(param)
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_BREAK,param)
end

--使用外形
--systemType 1翅膀 2法宝……
--subSystemType 1普通，2幻化 
--cfgId 对应的系统配置id
function TransformManager:RequestTransformShow(systemType,subSystemType,cfgId)
	local str = systemType..","..subSystemType..","..cfgId
	GameWorld.Player().server.illusion_action_req(action_config.ILLUSION_SHOW,str)
end

TransformManager:Init()
return TransformManager