local ActivityInfoManager = {}
require "PlayerManager/PlayerData/ActivityTimeData"
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ActivityTimeData = ClassTypes.ActivityTimeData
local AllActivityTimeData = ClassTypes.AllActivityTimeData

function ActivityInfoManager:Init()
    self:InitCallbackFunc()
end

function ActivityInfoManager:InitCallbackFunc()
end

function ActivityInfoManager:OnPlayerEnterWorld()
end

function ActivityInfoManager:OnPlayerLeaveWorld()
end

function ActivityInfoManager:ActivityInfoActionReq(activityType)
    GameWorld.Player().server.activity_info_action_req(action_config.GET_ACTIVITY_TIME, tostring(activityType))
end

function ActivityInfoManager:GetAllActivityTimeReq()
    GameWorld.Player().server.activity_info_action_req(action_config.GET_ALL_ACTIVITY_TIME, "")
end

function ActivityInfoManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("ActivityInfo Error:" .. error_id)
        return
    elseif event_id == action_config.GET_ACTIVITY_TIME then
        self:GetActivityTime(args)
    elseif event_id == action_config.GET_ALL_ACTIVITY_TIME then
        self:GetAllActivityTime(args)
    else
        LoggerHelper.Error("ActivityInfo event id not handle:" .. event_id)
    end
end

function ActivityInfoManager:GetActivityTime(args)
    -- LoggerHelper.Log("Ash: GetActivityTime: " .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.OnGetActivityTime, ActivityTimeData(args))
end

function ActivityInfoManager:GetAllActivityTime(args)
    -- LoggerHelper.Log("Ash: GetAllActivityTime: " .. PrintTable:TableToStr(args))
    EventDispatcher:TriggerEvent(GameEvents.OnGetAllActivityTime, AllActivityTimeData(args))
end

ActivityInfoManager:Init()
return ActivityInfoManager
