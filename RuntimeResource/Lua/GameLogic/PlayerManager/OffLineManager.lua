require "PlayerManager/PlayerData/CommonItemData"

local OffLineManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local partnerData = PlayerManager.PlayerDataManager.partnerData
local bagData = PlayerManager.PlayerDataManager.bagData
local CommonItemData = ClassTypes.CommonItemData

function OffLineManager:Init()

end

function OffLineManager:OnPlayerEnterWorld()
end

function OffLineManager:OnPlayerLeaveWorld()
    self.data = nil
end

function OffLineManager:HandleData(data)
    --LoggerHelper.Log("OffLineManager:HandleData")
    --LoggerHelper.Log(data)
    self.data = data
    self.devourData = {}
    bagData:OnPkgChanged(public_config.PKG_TYPE_OFFLINE, self.data[5])
    for k,v in pairs(self.data[6]) do
        self.devourData[v] = 1
    end
end

-------------------------------------功能接口------------------------------------
function OffLineManager:ShowOffLine()
    if self.data ~= nil then
        GUIManager.ShowPanel(PanelsConfig.OffLine, self.data)
    end
end

function OffLineManager:IsDevour(pos)
    if self.devourData[pos] ~= nil then
        return true
    end
    return false
end

-------------------------------------客户端请求-----------------------------------

OffLineManager:Init()
return OffLineManager