local AnimalManager = {}

local action_config = GameWorld.action_config
local animalData = PlayerManager.PlayerDataManager.animalData

local StringStyleUtil = GameUtil.StringStyleUtil
local AnimalConfig = GameConfig.AnimalConfig
local RedPointManager = GameManager.RedPointManager
local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local public_config = GameWorld.public_config

function AnimalManager:Init()
    self:InitCallbackFunc()
    animalData:InitData()
end

function AnimalManager:InitCallbackFunc()
	self._redPointChangeHandle = function() self:UpdateRedpointData() end
    self._onFunctionOpen = function(id)self:OnFunctionOpen(id) end
end

function AnimalManager:OnFunctionOpen(id)
    if id == public_config.FUNCTION_ID_MYTHICIAL_ANIMAL then
        self:UpdateRedpointData()
    end
end

function AnimalManager:OnPlayerEnterWorld()
    self:UpdateRedpointData()
	self:AddEventListeners()
end

function AnimalManager:OnPlayerLeaveWorld()
    self:RemoveEventListeners()
end

function AnimalManager:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Animal_Red_Point_State, self._redPointChangeHandle)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end

function AnimalManager:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Animal_Red_Point_State, self._redPointChangeHandle)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN, self._onFunctionOpen)
end


function AnimalManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        -- LoggerHelper.Log("AnimalManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.GOD_MONSTER_EQUIP_LOAD then --9301, -- 穿装备
        -- LoggerHelper.Error("sam 9301 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    elseif action_id == action_config.GOD_MONSTER_EQUIP_UNLOAD then --9302, -- 卸装备
        -- LoggerHelper.Error("sam 9302 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    elseif action_id == action_config.GOD_MONSTER_EQUIP_UNLOAD_ALL then --9303, -- 一键卸
        -- LoggerHelper.Error("sam 9303 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    elseif action_id == action_config.GOD_MONSTER_EQUIP_STREN then --9304, -- 强化装备
        LoggerHelper.Error("sam 9304 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_EQUIP_stren)
    elseif action_id == action_config.GOD_MONSTER_ASSIST then --9305, -- 助战
        -- LoggerHelper.Error("sam 9305 == " .. PrintTable:TableToStr(args))
        self:UpdateRedpointData()
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    elseif action_id == action_config.GOD_MONSTER_RECALL then --9306, -- 召回
        -- LoggerHelper.Error("sam 9306 == " .. PrintTable:TableToStr(args))
        self:UpdateRedpointData()
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    elseif action_id == action_config.GOD_MONSTER_ADD_ASSIST then --9307, -- 增加可助战神兽数量
        LoggerHelper.Error("sam 9307 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.ANIMAL_INFO_REFRESH)
    else
        LoggerHelper.Error("AnimalManager HandleData action id not handle:" .. action_id)
    end
end

--根据装备类型id和品质id返回名字(带颜色)
function AnimalManager:GetNameByTypeAndQulity(typeId, qulityId)
    local name = AnimalConfig.NAME_MAP[typeId]
    return StringStyleUtil.GetColorStringWithColorId(name, AnimalConfig.QULITY_COLOR_MAP[qulityId])
end

--根据品质id返回颜色text
function AnimalManager:GetQulityNameByQulityId(qulityId)
    local name = AnimalConfig.QULITY_NAME_MAP[qulityId]
    return StringStyleUtil.GetColorStringWithColorId(name, AnimalConfig.QULITY_COLOR_MAP[qulityId])
end

--获取已经装备的神兽装备列表
function AnimalManager:GetAnimalEquipListData()
    return animalData:GetAnimalEquipListData()
end

--是否存在紫色品质及以下的神兽装备存在
function AnimalManager:UpgradeNeedRedImage()
    return animalData:UpgradeNeedRedImage()
end

--获取有装备的分数最高的神兽
function AnimalManager:GetHigherScoreAnimalId()
    return animalData:GetHigherScoreAnimalId()
end

--获取神兽评分
function AnimalManager:GetAnimalTotalScore(animalId)
    return animalData:GetAnimalTotalScore(animalId)
end

--是否觉醒了
function AnimalManager:IsAwake(animalId)
    return animalData:IsAwake(animalId)
end

--是否助战了
function AnimalManager:IsHelp(animalId)
    return animalData:IsHelp(animalId)
end

--最多能助战的神兽数量
function AnimalManager:GetMaxHelpNum()
    return animalData:GetMaxHelpNum()
end

--获取可助战的神兽数量
function AnimalManager:GetCanHelpNum()
    return animalData:GetCanHelpNum()
end

--背包中是否存在对应位置的装备
function AnimalManager:IsExitEquipByPos(animalId, pos)
    return animalData:IsExitEquipByPos(animalId, pos)
end

--根据神兽装备品质和星级获取神兽背包数据
function AnimalManager:GetBadDataByQualityAndStart(animalId, quality, start, typeValue)
    return animalData:GetBadDataByQualityAndStart(animalId, quality, start, typeValue)
end

--神兽列表是否有红点
--规则：
-- 1.有空位时，可助战的加红点，(选中的神兽对应的助战按钮也红点)
-- 2.助战中的神兽有更高分数的装备可装备时 加红点，(选中的神兽对应的装备背包按钮也红点)
-- 3.(存在紫色及以下装备，神兽强化按钮红点)
--其中本方法实现非括号部分
function AnimalManager:AnimalNeedRedPoint(animalId)
    return animalData:AnimalNeedRedPoint(animalId)
end

--检查背包是否存在更高分数的装备进行替换（助战中）
function AnimalManager:IsHasHigherScore(animalId)
    return animalData:IsHasHigherScore(animalId)
end
--检查神兽是否可助战
function AnimalManager:CheckAnimalCanHelp(animalId)  
    return animalData:CheckAnimalCanHelp(animalId)
end

--获取已经助战的数量
function AnimalManager:GetHelpNum()
    return animalData:GetHelpNum()
end

--根据类型查找神兽背包数据
function AnimalManager:GetDataByType(typeNum)
    return animalData:GetDataByType(typeNum)
end

--根据您神兽装备部位获取神兽背包数据
function AnimalManager:GetBadDataByType(animalId, typeNum)
    return animalData:GetBadDataByType(animalId, typeNum)
end


function AnimalManager:GetAnimalListData()

end

function AnimalManager:UpdateRedpointData()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MYTHICIAL_ANIMAL, self:GetRedpointStatus())
end

function AnimalManager:GetRedpointStatus()
    local upgradeRedpoint = self:UpgradeNeedRedImage()
    local ids = MythicalAnimalsDataHelper:GetAllId()
    local listRepoint = false
    for i,id in ipairs(ids) do
        if self:AnimalNeedRedPoint(id) then
            listRepoint = true
            break
        end
    end
    return upgradeRedpoint or listRepoint
end

--9301, -- 穿装备
function AnimalManager:LoadEquip(animalId, pkgPos)
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_EQUIP_LOAD, tostring(animalId) .. ',' ..tostring(pkgPos))
end

--9302, -- 卸装备
function AnimalManager:UnloadEquip(animalId, pos)
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_EQUIP_UNLOAD, tostring(animalId) .. ',' ..tostring(pos))
end

--9303, -- 一键卸
function AnimalManager:UnloadAllEquip(id)
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_EQUIP_UNLOAD_ALL, tostring(id))
end

--9304, -- 强化装备
function AnimalManager:UpgradeEquip(animalId, typeNum, double, posList)
    local posContent = posList[1] 
    for i=2,#posList do
        posContent = posContent .. "," .. posList[i]
    end 
    local content = animalId .. "," .. typeNum .. "," .. tostring(double) .. "," .. posContent
    -- LoggerHelper.Error("content ====" .. tostring(content))
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_EQUIP_STREN, content)
end

--9305, -- 助战
function AnimalManager:ASSISTAnimal(id)
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_ASSIST, tostring(id))
end

--9306, -- 召回
function AnimalManager:RecallAnimal(id)
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_RECALL, tostring(id))
end

--9307, -- 增加可助战神兽数量
function AnimalManager:AddAssist()
    GameWorld.Player().server.god_monster_action_req(action_config.GOD_MONSTER_ADD_ASSIST, "")
end


AnimalManager:Init()
return AnimalManager