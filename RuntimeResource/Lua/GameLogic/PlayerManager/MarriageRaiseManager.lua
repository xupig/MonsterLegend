local MarriageRaiseManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local WorldBossData = PlayerDataManager.worldBossData
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local MarriageManager = PlayerManager.MarriageManager
local FriendData = PlayerManager.PlayerDataManager.friendData
local MarriageRaiseData = PlayerManager.PlayerDataManager.marriageRaiseData
local MarryChildattriDataHelper = GameDataHelper.MarryChildattriDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local MarryRingDataHelper = GameDataHelper.MarryRingDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local MarryChildbaseDataHelper = GameDataHelper.MarryChildbaseDataHelper
function MarriageRaiseManager:Init()
    self:InitCallbackFunc()
    self._levelCb = function() return self:UpdateringLeve()end
    self._badyCb = function()return self:UpdteBady()end
    self._intimacyCb = function()return self:UpdateIntimacy()end
    self.TitleEvenfuns = {
        [5059]=self._levelCb,
        [5060]=self._badyCb,
        [5061]=self._badyCb,
        [5062]=self._badyCb,
        [5063]=self._badyCb,
        [5064]=self._intimacyCb
    }
end

function MarriageRaiseManager:InitCallbackFunc()
    self._bodyRedEvent = function(pkgType)
        if pkgType==public_config.PKG_TYPE_ITEM then
            self:RefreshBobyRedEvent() 
        end
    end
    self._boxRedEvent = function()self:RefreshBoxRedEvent() end
    self._ringRedEvent = function()self:RefreshRingRedEvent() end
end

function MarriageRaiseManager:OnPlayerEnterWorld()
    self:RefreshBobyRedEvent()
    self:RefreshBoxRedEvent()
    self:RefreshRingRedEvent()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._bodyRedEvent)
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._ringRedEvent)
end

function MarriageRaiseManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._bodyRedEvent)
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._ringRedEvent)
end

function MarriageRaiseManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        
    end
    if event_id == action_config.MARRY_RING_PROMOTE then
        EventDispatcher:TriggerEvent(GameEvents.MarryRingUpdate,error_id)        
    elseif event_id == action_config.MARRY_PET_GRADE_UP then
        EventDispatcher:TriggerEvent(GameEvents.MarryBadyUpdate,error_id)   
    elseif event_id == action_config.MARRY_PET_UNLOCK then
        EventDispatcher:TriggerEvent(GameEvents.MarryBadyActivedate,error_id)  
        if args[1] then
            GUIManager.ShowPanel(PanelsConfig.MarryBabyActiveShow, args[1])
        end
    elseif event_id == action_config.MARRY_INSTANCE_ENTER then

    elseif event_id == action_config.MARRY_INSTANCE_PURCHASE_EXTRA_TIME_REQ then
        if error_id==0 then
            EventDispatcher:TriggerEvent(GameEvents.MarryInstanceUpdateCount)  
        end
    elseif event_id == action_config.MARRY_INSTANCE_SETTLE then--结算        
        local serveData = {}
        serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] = args[public_config.INSTANCE_SETTLE_KEY_RESULT]
        serveData[public_config.INSTANCE_SETTLE_KEY_MAP_ID] = args[public_config.INSTANCE_SETTLE_KEY_RESULT]
        serveData[public_config.INSTANCE_SETTLE_KEY_ITEMS] = args[public_config.INSTANCE_SETTLE_KEY_ITEMS]           
        serveData[public_config.INSTANCE_SETTLE_KEY_STAR]=3
        PlayerDataManager.instanceBalanceData:UpdateData(serveData)
        local data = {}
        data.instanceBalanceData = PlayerDataManager.instanceBalanceData
        GUIManager.ShowPanel(PanelsConfig.InstBalance, data)
    elseif event_id == action_config.MARRY_INSTANCE_ADD_MATE_TIME then
        if error_id==0 then
            EventDispatcher:TriggerEvent(GameEvents.MarryInstanceUpdateCount)  
        end
    elseif event_id == action_config.MARRY_INSTANCE_BEG_MATE_TIME_REQ then

    elseif event_id == action_config.MARRY_INSTANCE_BE_BEGGED_MATE_TIME_REQ then--接收到配偶请求购买的消息
        local showText = LanguageDataHelper.GetContent(58772)
        GUIManager.ShowText(2, showText)
    elseif event_id == action_config.MARRY_BOX_PURCHASE_MARRY_BOX_REQ then--
        self:RefreshBoxRedEvent()     
    elseif event_id == action_config.MARRY_BOX_DAILY_REFUND_REQ then--
        self:RefreshBoxRedEvent()        
    elseif event_id == action_config.MARRY_BOX_REFUND_REQ then--
    elseif event_id == action_config.MARRY_RING_EXTRA_RING then--
        LoggerHelper.Error("MarriageRaise event id not handle:" .. event_id)
    end
end

-- RPC
-- 婚戒吞噬加经验
function MarriageRaiseManager:MarryExtraRingReq(pos)
    GameWorld.Player().server.marry_ring_action_req(action_config.MARRY_RING_EXTRA_RING,tostring(pos))
end

-- 婚戒提升
function MarriageRaiseManager:MarryRingReq(pos)
	GameWorld.Player().server.marry_ring_action_req(action_config.MARRY_RING_PROMOTE,tostring(pos))
end

-- 仙娃激活
function MarriageRaiseManager:MarryPetActiveReq(pos)
	GameWorld.Player().server.marry_pet_action_req(action_config.MARRY_PET_UNLOCK,tostring(pos))
end

-- 仙娃升阶
function MarriageRaiseManager:MarryPetUpReq(id,pos)
	GameWorld.Player().server.marry_pet_action_req(action_config.MARRY_PET_GRADE_UP,tostring(id)..','..tostring(pos))
end

-- 副本--
-- 进入副本
function MarriageRaiseManager:MarryEnterInstanceReq()
	GameWorld.Player().server.marry_instance_action_req(action_config.MARRY_INSTANCE_ENTER,'')
end
-- 购买额外次数
function MarriageRaiseManager:MarryBuyCountInstanceReq()
	GameWorld.Player().server.marry_instance_action_req(action_config.MARRY_INSTANCE_PURCHASE_EXTRA_TIME_REQ,'')
end
-- 增加配偶结婚副本进入次数
function MarriageRaiseManager:MarryAddSpouseCountInstanceReq()
	GameWorld.Player().server.marry_instance_action_req(action_config.MARRY_INSTANCE_ADD_MATE_TIME,'')
end
-- 请求配偶购买
function MarriageRaiseManager:MarryAskSpouseBuyReq()
	GameWorld.Player().server.marry_instance_action_req(action_config.MARRY_INSTANCE_BEG_MATE_TIME_REQ,'')
end

-- 宝匣--
-- 购买宝匣
function MarriageRaiseManager:MarryBuyBoxReq()
	GameWorld.Player().server.marry_box_action_req(action_config.MARRY_BOX_PURCHASE_MARRY_BOX_REQ,'')
end
-- 领取宝匣每日奖励
function MarriageRaiseManager:MarryBoxRewardReq()
	GameWorld.Player().server.marry_box_action_req(action_config.MARRY_BOX_DAILY_REFUND_REQ,'')
end
-- 领取宝匣直接返利
function MarriageRaiseManager:MarryAskChestBuyReq()
	GameWorld.Player().server.marry_box_action_req(action_config.MARRY_BOX_REFUND_REQ,'')
end

function MarriageRaiseManager:UpdateringLeve()
    return GameWorld.Player().marry_ring_level or 0
end

function MarriageRaiseManager:UpdteBady(badyid)
    if MarriageRaiseData:IsActiveBady(badyid) then return 1 end
    return 0
end

function MarriageRaiseManager:UpdateIntimacy()
    local haveMate = MarriageManager:HaveMate()
    if haveMate then
        local item  = FriendData:GetFriendInfoByDbid(MarriageManager:GetMateDbid())
        if item then
            return item:GetFriendlyVal()
        end
    end
    return 0
end
function MarriageRaiseManager:UpdtePro(d)
    local per=0
    local f = false
    local x=0
    for k,v in ipairs(d) do 
        if v.contentid then
            if self.TitleEvenfuns[v.key] then
                x = x+1
                f=true
                local v1=self.TitleEvenfuns[v.key]()
                local v2=v.conditionvalue
                local z = v1/v2
                if z>1 then
                    z=1
                end
                per=per+z
            end
        end
    end
    if not f then
        return 0
    end
    per = per*1/x
    return per
end


-- 宝匣红点
function MarriageRaiseManager:BoxRedPointUpdata()
    if MarriageRaiseData:IsHaveBox() then
        return not MarriageRaiseData:IsGetToday()
    end
    return false
end

-- 宝宝红点
function MarriageRaiseManager:BobyRedPointUpdata()
    return self:BobyRedPointLevelUpdata() or self:BobyRedPointActiveUpdata()
end

-- 升级
function MarriageRaiseManager:BobyRedPointLevelUpdata()
    local infos = MarriageRaiseData:GetMarrayBadyInfo()
    local level=0
    local exp=0
    local needexp=0
    local d={}
    local materialid
    local count=0
    local ownCount=0
    materialid,count = next(GlobalParamsHelper.GetParamValue(781))
    for k,v in pairs(infos) do
        level = v[public_config.MARRY_PET_GRADE]
        exp = v[public_config.MARRY_PET_EXP]
        local f = self:BobyRedPointUp(tonumber(k),level,exp)
        if f then
            return true
        end
    end
    return false
end

function MarriageRaiseManager:BobyRedPointUp(id,level,exp)
    local needexp=0
    local d={}
    local materialid
    local count=0
    local ownCount=0
    materialid,count = next(GlobalParamsHelper.GetParamValue(781))
    d = MarryChildattriDataHelper:GetCfgAttByidLevel(id,level)
    if d then
        if d.exp then
            needexp = d.exp
            if needexp==0 and exp==0 then 
                return false
            end
            ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(materialid)
            if exp>=needexp then
                return true
            end
            if ownCount*count>=needexp-exp then
                return true
            end
        end
    end
    return false
end

-- 激活
function MarriageRaiseManager:BobyRedPointActiveUpdata()
    local ids = MarryChildbaseDataHelper:GetAllId()
    for k,id in ipairs(ids)do
        local f = self:BodyRedPointStateById(id)
        if f then
            return true
        end
    end
    return false
end

function MarriageRaiseManager:BodyRedPointStateById(id)
    local unlock = MarryChildbaseDataHelper:GetUnlock(id)
    if unlock[2]then
        local materialid = unlock[2][1]
        local num = unlock[2][2]
        local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(materialid)
        if ownCount>num then
            return true
        end
    elseif unlock[3] then
        local l = unlock[3][2]
        if l<=GameWorld.Player().marry_ring_level then
            return true
        end
    elseif unlock[1]then
        local id = unlock[1][1]
        local l = unlock[1][2]
        local info = MarriageRaiseData:GetMarrayBadyInfo(id)
        if info and info[id] and info[id][public_config.MARRY_PET_GRADE]>l then
            return true
        end
    end
    return false
end
-- 婚戒红点
function MarriageRaiseManager:RingRedPointUpdata()
    if MarriageRaiseData:IsPutOnRing() then
        local materialid
        local count=0
        local ownCount=0
        materialid,count = next(GlobalParamsHelper.GetParamValue(780))
        local entity = GameWorld.Player()
        local exp = entity.marry_ring_exp
        local id = MarriageRaiseData:GetIdByLevel(entity.marry_ring_level)
        local needexp = MarryRingDataHelper:GetExp(id)      
        if needexp then
            ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(materialid)
            if exp>=needexp then
                return true
            end
            if ownCount*count>=needexp-exp then
                return true
            end
        end
    end 
    return false
end
-- 宝宝
function MarriageRaiseManager:RefreshBobyRedEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MARRY_PET, self:BobyRedPointUpdata())        
end
-- 婚戒
function MarriageRaiseManager:RefreshRingRedEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MARRY_RING, self:RingRedPointUpdata())        
end
-- 宝匣
function MarriageRaiseManager:RefreshBoxRedEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MARRY_BREGUET, self:BoxRedPointUpdata())        
end


function MarriageRaiseManager:OnEnterMap(mapId)
   
end

function MarriageRaiseManager:OnLeaveMap()

end
MarriageRaiseManager:Init()
return MarriageRaiseManager
