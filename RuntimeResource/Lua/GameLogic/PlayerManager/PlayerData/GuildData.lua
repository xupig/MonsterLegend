local GuildDataHelper = GameDataHelper.GuildDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
--local bagData = PlayerManager.PlayerDataManager.bagData

local GuildData = Class.GuildData(ClassTypes.XObject)

function GuildData:__ctor__()
    self.guildId = nil
    self.guildName = nil
    self.guildPosition = nil
    self.guildApplyList = nil
    self.curGuildListPage = 0
    self.signRedEnvelopeRecord = {}
    self.guildSkillInfoData = {}
    self.guildRedEnvelopeRecordData = {}
    self.guildMonsterInfo = {}
    self.guildGuardGoddnessData = {}
    self.guildRedEnvelopeListData = {}
end

function GuildData:RefreshData(luaTable)

end

function GuildData:RefreshGuildInfo(changeValue)
    --LoggerHelper.Log("GuildData:RefreshGuildInfo")
    --LoggerHelper.Log(GameWorld.Player().guild_info)
    local guildDbid = GameWorld.Player().guild_info[public_config.GUILD_INFO_DBID] or 0
    local guildName = GameWorld.Player().guild_info[public_config.GUILD_INFO_NAME]
    local guildPosition = GameWorld.Player().guild_info[public_config.GUILD_INFO_POSITION]
    local guildApplyList = GameWorld.Player().guild_info[public_config.GUILD_INFO_APPLY_LIST] or {}
    local hasDailyReward = GameWorld.Player().guild_info[public_config.GUILD_INFO_HAS_DAILY_REWARD] or 0
    local guildSkillInfo = GameWorld.Player().guild_info[public_config.GUILD_INFO_SKILL] or {}
    local sendRedEnvelopeCount = GameWorld.Player().guild_info[public_config.GUILD_INFO_DAILY_RED_COUNT] or 0
    local warehousePoints = GameWorld.Player().guild_info[public_config.GUILD_INFO_WAREHOUSE_POINTS] or 0

    self.guildId = guildDbid
    self.guildName = guildName
    self.guildPosition = guildPosition
    self.guildApplyList = guildApplyList
    self.hasDailyReward = hasDailyReward
    self.guildSkillInfo = guildSkillInfo
    self.sendRedEnvelopeCount = sendRedEnvelopeCount
    self.warehousePoints = warehousePoints

    if changeValue == nil then
        return
    end

    for k,v in pairs(changeValue) do       
        -- LoggerHelper.Log("change "..k)
        --删除数据
        if k == table.REMOVE_FLAG then
            --v是下标
        else
            if k == public_config.GUILD_INFO_DBID then
                EventDispatcher:TriggerEvent(GameEvents.On_Guild_DBID_Change)
            elseif k == public_config.GUILD_INFO_APPLY_LIST then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_GuildList_Info)
            elseif k == public_config.GUILD_INFO_SKILL then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Skill_Info)
            elseif k == public_config.GUILD_INFO_HAS_DAILY_REWARD then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Has_Daily_Reward)
            elseif k == public_config.GUILD_INFO_WAREHOUSE_POINTS then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Warehouse_Point)
            end
        end
    end
end

--自己所在公会信息
function GuildData:RefreshSelfGuildInfo(data)
    self.guildRank = data[public_config.GUILD_DETAIL_INFO_RANK]
    self.guildDbid = data[public_config.GUILD_DETAIL_INFO_DBID]
    self.guildIcon = data[public_config.GUILD_DETAIL_INFO_ICON_ID]
    self.guildName = data[public_config.GUILD_DETAIL_INFO_NAME]
    self.guildLevel = data[public_config.GUILD_DETAIL_INFO_LEVEL] or 1
    self.guildMemberCount = data[public_config.GUILD_DETAIL_INFO_COUNT]
    self.guildMaxMemberCount = data[public_config.GUILD_DETAIL_INFO_MAX_COUNT]
    self.guildLeaderName = data[public_config.GUILD_DETAIL_INFO_PRESIDENT_NAME]
    if string.IsNullOrEmpty(data[public_config.GUILD_DETAIL_INFO_ANNOUNCEMENT]) then
        self.guildAnnouncement = LanguageDataHelper.CreateContent(53037)
    else
        self.guildAnnouncement = data[public_config.GUILD_DETAIL_INFO_ANNOUNCEMENT]
    end
    self.guildNeedVerify = data[public_config.GUILD_DETAIL_INFO_NEED_VERIFY]
    self.guildExperience = data[public_config.GUILD_DETAIL_INFO_EXPERIENCE] or 0
    self.guildPrestige = data[public_config.GUILD_DETAIL_INFO_PRESTIGE] or 0
    self.guildFlagIcon = data[public_config.GUILD_DETAIL_INFO_FLAG_ID]
    self.guildGrade = data[public_config.GUILD_DETAIL_INFO_GRADE] or 0
    self.guildSetAnnoCount = data[public_config.GUILD_DETAIL_INFO_SET_ANNO_COUNT] or 0
    self.guildFightPower = data[public_config.GUILD_DETAIL_INFO_GUILD_FIGHT_FORCE] or 0
end

--查看其他公会信息
function GuildData:RefreshOtherGuildInfo(data)
    self.guildRankQuery = data[public_config.GUILD_DETAIL_INFO_RANK]
    self.guildDbidQuery = data[public_config.GUILD_DETAIL_INFO_DBID]
    self.guildIconQuery = data[public_config.GUILD_DETAIL_INFO_ICON_ID]
    self.guildNameQuery = data[public_config.GUILD_DETAIL_INFO_NAME]
    self.guildLevelQuery = data[public_config.GUILD_DETAIL_INFO_LEVEL] or 1
    self.guildMemberCountQuery = data[public_config.GUILD_DETAIL_INFO_COUNT]
    self.guildMaxMemberCountQuery = data[public_config.GUILD_DETAIL_INFO_MAX_COUNT]
    self.guildLeaderNameQuery = data[public_config.GUILD_DETAIL_INFO_PRESIDENT_NAME]
    self.guildAnnouncementQuery = data[public_config.GUILD_DETAIL_INFO_ANNOUNCEMENT]
    self.guildNeedVerifyQuery = data[public_config.GUILD_DETAIL_INFO_NEED_VERIFY]
    self.guildExperienceQuery = data[public_config.GUILD_DETAIL_INFO_EXPERIENCE] or 0
    self.guildPrestigeQuery = data[public_config.GUILD_DETAIL_INFO_PRESTIGE] or 0
    self.guildFlagIconQuery = data[public_config.GUILD_DETAIL_INFO_FLAG_ID]
end

function GuildData:SetGuildAnnouncement(announcement)
    self.guildAnnouncement = announcement
end

function GuildData:SetGuildNeedVerify(guildNeedVerify)
    self.guildNeedVerify = guildNeedVerify
end

--获取指定页数的公会列表信息
function GuildData:RefreshGuildListInfo(data)
    self.maxGuildListPage = data[2]
    if data[1] > self.maxGuildListPage then
        return
    end
    if data[1] == 1 then
        self.guildListInfo = data[3]
    elseif data[1] == self.curGuildListPage + 1 then
        for k,v in ipairs(data[3]) do
            table.insert(self.guildListInfo, v)
        end
    end
    if data[1] ~= self.curGuildListPage then
        self.curGuildListPage = data[1]
    end
end

--获取申请加入公会信息
function GuildData:RefreshApproveMessage(data)
    self.approveMessage = data
end

--获取公会签到红包发送记录
function GuildData:RefreshSignRedEnvelopeRecord(data)
    self.signRedEnvelopeRecord = data or {}
end

--获取公会技能
function GuildData:GetSkillInfo()
    local data = self.guildSkillInfo or {}
    self.guildSkillInfoData = {}
    local types = GuildDataHelper:GetGuildSkillAllType()
    for k,v in pairs(types) do
        local openData = GlobalParamsHelper.GetParamValue(574)
        local tmp = {["type"]=v,["level"]=0}
        if data[v] ~= nil then
            tmp.level = data[v]
        end
        if openData[v] ~= nil and openData[v] <= GameWorld.Player().level then
            tmp.unLockLevel = 0 --0表示已解锁
        else
            tmp.unLockLevel = openData[v]
        end
        table.insert(self.guildSkillInfoData, tmp)
    end
    return self.guildSkillInfoData
end

--获取所在公会成员信息
function GuildData:RefreshGuildSelfMemberInfo(data)
    self.guildSelfMemberList = data
end

--获取所在公会在线成员信息
function GuildData:RefreshGuildSelfOnlineMemberInfo(data)
    self.guildSelfOnlineMemberList = {}
    for k,v in ipairs(data) do
        table.insert(self.guildSelfOnlineMemberList, v)
    end
end

--获取其他公会成员信息
function GuildData:RefreshGuildMemberInfo(data)
    self.guildMemberMaxPage = data[public_config.GUILD_DETAIL_INFO_MAX_PAGE]
    local curPage = data[public_config.GUILD_DETAIL_INFO_PAGE]
    if curPage > self.guildMemberMaxPage then
        return
    end
    if curPage == 1 then
        self.guildMemberList = data[public_config.GUILD_DETAIL_INFO_MEMBERS]
    elseif curPage == self.guildMemberCurPage + 1 then
        for k,v in ipairs(data[public_config.GUILD_DETAIL_INFO_MEMBERS]) do
            table.insert(self.guildMemberList, v)
        end
    end
    if curPage ~= self.guildMemberCurPage then
        self.guildMemberCurPage = curPage
    end
end

--获取公会排行榜信息
function GuildData:RefreshGuildRankList(data)
    self.guildRankList = data or {}
end



function GuildData:RefreshGuildWarehouseRecordData(data)
    self.guildWarehouseRecordData = data or {}
end

function GuildData:RefreshGuildRedEnvelopeListData(data)
    self.guildRedEnvelopeListData = data or {}
end

function GuildData:RefreshGuildRedEnvelopeRecordData(data)
    self.guildRedEnvelopeRecordData = data or {}
end

function GuildData:UpdateGuildRedEnvelopeItem(data)
    local id = data[public_config.GUILD_RED_ENVELOPE_INFO_ID]
    self.guildRedEnvelopeListData[id] = data
end

function GuildData:DeleteGuildRedEnvelopeItem(data)
    for _,v in pairs(data) do
        self.guildRedEnvelopeListData[v] = nil
    end
end

--{次数，兽粮，开始时间}
function GuildData:RefreshGuildMonsterInfo(data)
    self.guildMonsterInfo = data
end

--{boss最大血量,当前血量}
function GuildData:RefreshGuildMonsterHP(data)
    self.guildMonsterHP = data
end

function GuildData:RefreshGuildMonsterKillInfo(data)
    self.guildMonsterKillInfo = data
end

--{cfg_id=reborn_time}
function GuildData:RefreshGuildTokenMonsterData(data)
    self.guildTokenMonsterData = data
end

--1:神像eid列表 2:伤害排行榜 3:波数信息 4:开启时间
function GuildData:RefreshGuildGuardGoddnessData(type, data)
    self.guildGuardGoddnessData[type] = data
end

function GuildData:ResetGuildGuardGoddnessData()
    self.guildGuardGoddnessData = {}
end



-------------------------------------------
function GuildData:GetGuildId()
    return self.guildId
end

function GuildData:GetGuildName()
    return self.guildName or ""
end

function GuildData:IsAutoAccept()
    if self.guildNeedVerify == public_config.GUILD_NEED_VERIFY_YES then
        return false
    else
        return true
    end
end

function GuildData:SetSelectGuildRaidId(id)
    self.selectGuildRaidId = id
end

function GuildData:GetSelectGuildRaidId()
    return self.selectGuildRaidId or 1
end

function GuildData:SetSelectGuildRaidData(data)
    self.selectGuildRaidData = data
end

function GuildData:GetSelectGuildRaidData()
    return self.selectGuildRaidData or {}
end

function GuildData:IsPresident()
    return self.guildPosition == public_config.GUILD_POSITION_PRESIDENT
end

function GuildData:IsVicePresident()
    return self.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT
end

function GuildData:GetGuildGuardGoddnessData(type)
    return self.guildGuardGoddnessData[type]
end