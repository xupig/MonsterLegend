require "PlayerManager/PlayerData/EventData"
local TaskRewardData = ClassTypes.TaskRewardData

local EventData = ClassTypes.EventData

local public_config = GameWorld.public_config

local DailyActivityItemData = Class.DailyActivityItemData(ClassTypes.XObject)
local DailyTaskDataHelper = GameDataHelper.DailyTaskDataHelper

function DailyActivityItemData:__ctor__(id, serverData)
    self._id = id
    self:UpdateDailyActivityData(serverData)
end

function DailyActivityItemData:UpdateDailyActivityData(serverData)
    if self._serverData ~= nil then
        self._lastState = self:GetState()
    end
    
    self._serverData = serverData
    
    if serverData == nil then return end
    
    if self._lastState ~= self:GetState() then
        EventDispatcher:TriggerEvent(GameEvents.OnDailyActivityDataStateChanged, self._id)
    end
end

function DailyActivityItemData:GetId()
    return self._id
end

function DailyActivityItemData:GetTargetsInfo()
    if self._targetsData == nil then
        self._targetsData = EventData()
        self._targetsData:UpdateSortedEventData(DailyTaskDataHelper:GetTargets(self._id))
    end
    return self._targetsData:GetSortedEvents()
end

function DailyActivityItemData:GetTargetsCurTimes()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_TARGETS] or 0
end

function DailyActivityItemData:GetProgress()
    local progress = nil
    local targetsInfo = self:GetTargetsInfo()
    local curTimes = self:GetTargetsCurTimes()
    -- LoggerHelper.Log("Ash: DailyActivityItemData GetProgress:" .. PrintTable:TableToStr(targetsInfo))
    for i, ev in ipairs(targetsInfo) do
        local id = ev:GetId()
        if self:GetState() == public_config.DAILY_TASK_STATE_COMPLETED
            or self:GetState() == public_config.DAILY_TASK_STATE_COMMITED then
            progress = ev:GetTotalTimes() .. "/" .. ev:GetTotalTimes()
        else
            progress = tostring(curTimes == 0 and 0 or curTimes[id]) .. "/" .. ev:GetTotalTimes()
        end
        return progress
    end
    
    return progress
end

function DailyActivityItemData:GetState()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_STATE] or public_config.DAILY_TASK_STATE_ACCEPTED
end

function DailyActivityItemData:GetReward()
    local rewards = DailyTaskDataHelper:GetReward(self._id)
    local result = {}
    for i, v in ipairs(rewards) do
        table.insert(result, TaskRewardData(v[1], v[2]))
    end
    return result
end

local DailyActivityData = Class.DailyActivityData(ClassTypes.XObject)

function DailyActivityData:__ctor__()
    self._dailyActivities = {}
    self._dailyActivitiesList = {}
end

function DailyActivityData:ResetDailyActivityData(serverData)
    self._dailyActivities = {}
    self._dailyActivitiesList = {}
    for k, v in pairs(serverData) do
        self._dailyActivities[k] = DailyActivityItemData(k)
        table.insert(self._dailyActivitiesList, k)
    end
end

function DailyActivityData:UpdateDailyActivityData(serverData)
    for id, dailyActivity in pairs(serverData) do
        if self._dailyActivities[id] == nil then
            self._dailyActivities[id] = DailyActivityItemData(id, dailyActivity)
        else
            self._dailyActivities[id]:UpdateDailyActivityData(dailyActivity)
        end
        EventDispatcher:TriggerEvent(GameEvents.OnUpdateDailyActivityData, id)
    end
end

function DailyActivityData:DelDailyActivityData(serverData)
    for k, v in pairs(serverData) do
        local dailyActivity = self._dailyActivities[k]
        if dailyActivity ~= nil then
            self._dailyActivities[k] = nil
        else
            LoggerHelper.Error("Del DailyActivity id not exist:" .. k)
        end
    end
end

function DailyActivityData:GetDailyActivityById(id)
    return self._dailyActivities[id]
end

function DailyActivityData:GetDailyActivities()
    return self._dailyActivities
end

function DailyActivityData:GetDailyActivitiesList()
    return self._dailyActivitiesList
end
