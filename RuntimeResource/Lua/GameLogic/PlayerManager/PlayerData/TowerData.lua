local TowerData = Class.TowerData(ClassTypes.XObject)

local public_config = GameWorld.public_config
local TowerInstanceConfig = GameConfig.TowerInstanceConfig
local TowerDefenseDataHelper = GameDataHelper.TowerDefenseDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function TowerData:__ctor__()
    self._init = false
    self._datas = {}
end

function TowerData:Init()
    self._datas = {}
    self._init = true
end

function TowerData:UpdateData(serveData)
    self:Init()
    self._serveData = serveData

    local ids = TowerDefenseDataHelper:GetAllId()
    local nowNumList = {}

    for k,v in pairs(serveData) do
        if nowNumList[v] then
            nowNumList[v] = nowNumList[v] + 1 
        else
            nowNumList[v] = 1
        end
    end

    for i=1,#ids do
        local limitNum = TowerDefenseDataHelper:GetBuildLimit(i)
        if limitNum == 0 then
            limitNum = TowerInstanceConfig.LIMIT_NUM
        end

        local data = {}
        data[TowerInstanceConfig.TOWER_LIMIT_NUM] = limitNum
        data[TowerInstanceConfig.TOWER_NOW_BUILD_NUM] = nowNumList[i] or 0 
        self._datas[i] = data
    end
    -- LoggerHelper.Error("建塔数据===== " .. PrintTable:TableToStr(self._datas))
end

function TowerData:HasBuildTowe(pointId)
    if not self._init then return false end
    return self._serveData[pointId] ~= nil
end

function TowerData:IsCanBuildById(typeId)
    if not self._init then return false end
    local limitNum = self._datas[typeId][TowerInstanceConfig.TOWER_LIMIT_NUM]
    local buildNum = self._datas[typeId][TowerInstanceConfig.TOWER_NOW_BUILD_NUM]
    if limitNum == TowerInstanceConfig.LIMIT_NUM then
        return true
    end
    return limitNum > buildNum
end

function TowerData:GetShowTextById(typeId)
    if not self._init then return "" end
    -- LoggerHelper.Error("typeId ===" .. tostring(typeId))
    local limitNum = self._datas[typeId][TowerInstanceConfig.TOWER_LIMIT_NUM]
    local buildNum = self._datas[typeId][TowerInstanceConfig.TOWER_NOW_BUILD_NUM]
    local colorId = 102
    if limitNum == TowerInstanceConfig.LIMIT_NUM then
        --无限
        return StringStyleUtil.GetColorStringWithColorId("(" .. LanguageDataHelper.CreateContent(76311) ..")", colorId)
    end
    
    if limitNum <= buildNum then
        colorId = 106
    end
        
    return StringStyleUtil.GetColorStringWithColorId("(" .. buildNum .. "/" .. limitNum .. ")", colorId)
end
