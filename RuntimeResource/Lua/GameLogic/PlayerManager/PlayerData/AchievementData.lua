local AchievementData = Class.AchievementData(ClassTypes.XObject)

local AchievementDataHelper = GameDataHelper.AchievementDataHelper
local public_config = GameWorld.public_config
local AchievementConfig = GameConfig.AchievementConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function AchievementData:__ctor__()
end

function AchievementData:Init()
    local achievenmentPoints, totalPoints = AchievementDataHelper:GetAchievementPointsDatas()
    self._totalAchievement = totalPoints
    local bigTypes = AchievementDataHelper.GetBigTypes()
    -- LoggerHelper.Error("成就类型数据===" .. PrintTable:TableToStr(bigTypes))    
    self._otherAchievement = {}  
    for i,v in ipairs(bigTypes) do
        if achievenmentPoints[v.type] ~= nil then
            self._otherAchievement[v.type] = achievenmentPoints[v.type].points
        end
    end
    
    -- self._otherGetAchievementPoint = {}
    -- for i,v in ipairs(bigTypes) do
    --     self._otherGetAchievementPoint[v.type] = 0
    -- end

    -- LoggerHelper.Error("成就点数据===" .. PrintTable:TableToStr(self._otherAchievement))    
    self._achievementAllData = AchievementDataHelper.GetAllAchievementData()
    self._getRewardList = {}        --完成并已领领取奖励
    self._completeList = {}         --完成但是未领取奖励
    self._notCompleteList = {}      --未完成但是有进度
    self._noDataList = {}           --未完成但是没有进度

    self._achievementState = {}
end 

function AchievementData:InitServerData(serverDatas)
    self._getRewardList = {}        --完成并已领领取奖励
    self._completeList = {}         --完成但是未领取奖励
    self._notCompleteList = {}      --未完成但是有进度
    self._noDataList = {}           --未完成但是没有进度
    self._achievementState = {}

    local bigTypes = AchievementDataHelper.GetBigTypes()
    self._otherGetAchievementPoint = {}
    for i,v in ipairs(bigTypes) do
        self._otherGetAchievementPoint[v.type] = 0
    end

    for i,id in ipairs(self._achievementAllData) do
        local data = serverDatas[id]
        if data == nil then
            --未完成但是没有进度
            self._noDataList[id] = id
            self._achievementState[id] = AchievementConfig.ACHIEVEMENT_NO_PROGRESS
        else
            local type = data[2]
            if type == public_config.ACHIEVEMENT_STATE_ACCEPTED then
                --未完成但是有进度
                self._notCompleteList[id] = data[1]
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_HAS_PROGRESS
            elseif type == public_config.TITLE_TASK_STATE_COMPLETED then
                --完成但是未领取奖励
                self._completeList[id] = id
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_COMPLETE
                self:SetOtherAchievementGetPoint(id)
            elseif type == public_config.TITLE_TASK_STATE_COMMITED then
                --完成并已领领取奖励
                self._getRewardList[id] = id
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_GET_REWARD
                self:SetOtherAchievementGetPoint(id)
            end
        end
    end
    -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_ACHIEVEMENT, not table.isEmpty(self._completeList))
end

--刷新成就数据
function AchievementData:RefreshData(serverData)    
    for id,data in pairs(serverData) do
        local state = self:GetAchievementStateById(id)
        local nowState = data[2]

        if state ~= nowState then
            if state == AchievementConfig.ACHIEVEMENT_NO_PROGRESS then
                self._noDataList[id] = nil
            elseif state == AchievementConfig.ACHIEVEMENT_HAS_PROGRESS then
                self._notCompleteList[id] = nil
            elseif state == AchievementConfig.ACHIEVEMENT_COMPLETE then
                self._completeList[id] = nil
            elseif state == AchievementConfig.ACHIEVEMENT_GET_REWARD then
                self._getRewardList[id] = nil
            end

            if nowState == public_config.ACHIEVEMENT_STATE_ACCEPTED then
                --未完成但是有进度
                self._notCompleteList[id] = data[1]
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_HAS_PROGRESS
            elseif nowState == public_config.TITLE_TASK_STATE_COMPLETED then
                --完成但是未领取奖励
                self._completeList[id] = id
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_COMPLETE
                -- LoggerHelper.Error("完成成就.." .. tostring(id))
                self:SetOtherAchievementGetPoint(id)
                if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_ACHIEVEMENT) then
                    GameManager.GUIManager.ShowPanel(PanelsConfig.AchievementCompleteShow, id)
                end
            elseif nowState == public_config.TITLE_TASK_STATE_COMMITED then
                --完成并已领领取奖励
                self._getRewardList[id] = id
                self._achievementState[id] = AchievementConfig.ACHIEVEMENT_GET_REWARD
                -- self:SetOtherAchievementGetPoint(id)
            end
        else
            if nowState == public_config.ACHIEVEMENT_STATE_ACCEPTED then
                self._notCompleteList[id] = data[1]
            end
        end
    end
    --LoggerHelper.Error("成就红点情况====" .. tostring(not table.isEmpty(self._completeList)))
    -- EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_ACHIEVEMENT, not table.isEmpty(self._completeList))
    
    -- EventDispatcher:TriggerEvent(GameEvents.ACHIEVEMENT_REFRESH)
end

function AchievementData:GetRedPoint()
    return not table.isEmpty(self._completeList)
end

function AchievementData:SetOtherAchievementGetPoint(achievementId)
    -- LoggerHelper.Error("achievementId ====" .. tostring(achievementId))
    local points = AchievementDataHelper.GetPoints(achievementId)
    local typeId = AchievementDataHelper.GetAchievementTypeId(achievementId)
    local type, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(typeId)
    -- LoggerHelper.Error("type ====" .. tostring(type))
    if self._otherGetAchievementPoint[type] ~= nil then
        self._otherGetAchievementPoint[type] = self._otherGetAchievementPoint[type] + points 
    end
end

--获取成就完成次数
function AchievementData:GetCompleteNum(id)
    local state = self:GetAchievementStateById(id)
    -- LoggerHelper.Error("id ====" .. tostring(id) .. "state ===" .. tostring(state))
    if state == AchievementConfig.ACHIEVEMENT_NO_PROGRESS then --未完成但是没有进度
        return 0
    elseif state == AchievementConfig.ACHIEVEMENT_HAS_PROGRESS then  --未完成但是有进度
        local num = 0
        for k,v in pairs(self._notCompleteList[id]) do
            num = v
            break
        end
        return num
    elseif state == AchievementConfig.ACHIEVEMENT_COMPLETE then --完成但是未领取奖励
        return AchievementDataHelper.GetCompleteNum(id)
    elseif state == AchievementConfig.ACHIEVEMENT_GET_REWARD then --完成并已领领取奖励
        return AchievementDataHelper.GetCompleteNum(id)
    end
    return nil
end

--获取配置的总成就点数(配置数据)
function AchievementData:GetTotalPoints()
    return self._totalAchievement
end

--根据成就大类id获取该大类的总成就点数(配置数据)
function AchievementData:GetAchievementPointByBigType(bigType)
    return self._otherAchievement[bigType] or 0
end

--根基成就大类id获取已经获得的成就点数
function AchievementData:GetAlreadyGetAchievementPointByBigType(bigType)
    return self._otherGetAchievementPoint[bigType] or 0
end

--根据成就大类id和子类id获取成就总览数据
--bigType:成就大类id
--groupId：成就子类id
function AchievementData:GetAchievementListByType(bigType, groupId)
    local list = AchievementDataHelper.GetAchievementListByType(bigType, groupId)
    
    return list
end

--根据成就id获取成就完成状态
function AchievementData:GetAchievementStateById(id)
    return self._achievementState[id]
end

--根据成就大类型主键id判断该类型下是否有可领成就小类
--id:成就大类id
function AchievementData:IsCanGetByAchievementTypeId(id)
    local type = AchievementDataHelper.GetType(id)
    local typeIds = AchievementDataHelper.GetTypeIdsByType(type)
    local flag = false

    for i,v in ipairs(typeIds) do
        local achievementIds = AchievementDataHelper.GetAchievementIdsByType(v)
        local r = false
        for _, achievementId in ipairs(achievementIds) do
            if self:GetAchievementStateById(achievementId) == AchievementConfig.ACHIEVEMENT_COMPLETE then
                return true
            end
        end
    end
    return flag
end

--根据成就子类型主键id判断该类型下是否有可领成就
--id:成就子类id
function AchievementData:IsCanGetByGroupId(id)
    local achievementIds = AchievementDataHelper.GetAchievementIdsByType(id)
    local totalNum = #achievementIds
    local flag = false
    local completeNum = 0
    for i,v in ipairs(achievementIds) do
        local state = AchievementManager:GetAchievementStateById(v)
        if state == AchievementConfig.ACHIEVEMENT_COMPLETE then
            return true            
        end
    end
    return flag
end

function AchievementData:GetCanGetRewardAchievementId(menuData)
    local typeIds = AchievementDataHelper:GetAllTypeId()

    for i,v in ipairs(typeIds) do
        if v ~= 0 then
            local allAchievementId = AchievementDataHelper.GetAchievementIdsByType(v)
            for _, achievementId in ipairs(allAchievementId) do
                if self:GetAchievementStateById(achievementId) == AchievementConfig.ACHIEVEMENT_COMPLETE then
                    return achievementId, v
                end
            end
        end
    end
    return 0,nil
    -- for i,value in ipairs(menuData) do
    --     local id = value[1].id
    --     if self:IsCanGetByAchievementTypeId(id) then
    --         for index,v in ipairs(value) do
    --             if index ~= 1 then
    --                 if self:IsCanGetByAchievementTypeId(v.id) then
    --                     local typeId, group = AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(id)
    --                     local data  = self:GetAchievementListByType(typeId, group)
    --                     for i, achievementId in ipairs(data) do
    --                         if self:GetAchievementStateById(achievementId) == AchievementConfig.ACHIEVEMENT_COMPLETE then
    --                             return achievementId
    --                         end
    --                     end
    --                 end
    --             end
    --         end
    --     end
    -- end
    -- return 0
end
