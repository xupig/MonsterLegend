require "PlayerManager/PlayerData/TaskCommonItemData"

local public_config = GameWorld.public_config
local GuardGoddessDataHelper = GameDataHelper.GuardGoddessDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonItemData = ClassTypes.TaskCommonItemData

local IdToColorId = {102, 103, 104, 105}

local EscortPeriTaskData = Class.EscortPeriTaskData(ClassTypes.TaskCommonItemData)

function EscortPeriTaskData:__ctor__()
    self._taskItemType = TaskConfig.TASK_TYPE_GUARD_GODDESS
end

function EscortPeriTaskData:UpdateData(id, _data)
    self._data = _data
    self._id = id
end

function EscortPeriTaskData:GetId()
    return self._id
end

function EscortPeriTaskData:GetState()
    return self._data[public_config.TASK_INFO_KEY_STATE]
end

function EscortPeriTaskData:GetTargetsCurTimes()
    return 1
end

function EscortPeriTaskData:GetName()
    return GuardGoddessDataHelper:GetTitle(self._id)
end

--TODO 
function EscortPeriTaskData:GetSelfLimit()
    return 0
end

function EscortPeriTaskData:GetTargetsDesc()
    return GuardGoddessDataHelper:GetDesc(self._id)
end

function EscortPeriTaskData:GetCommitNPC()
    return GlobalParamsHelper.GetParamValue(530)
end

function EscortPeriTaskData:GetCommitDialog()
    return GuardGoddessDataHelper:GetCommitDialog(self._id)
end

function EscortPeriTaskData:NewTaskData(id, state)
    local serverData = {}
    serverData[public_config.TASK_INFO_KEY_STATE] = state
    local cData = EscortPeriTaskData()
    cData:UpdateData(id, serverData)
    return cData
end

function EscortPeriTaskData:GetTargetsInfoData()
    return nil
end

function EscortPeriTaskData:IsAutoTask()
    return true
end

function EscortPeriTaskData:GetMyRewards()
    local myReward = {}
    if self._id == nil then
        return myReward
    else
        local rewards = GuardGoddessDataHelper:GetRewardConfig(self._id, GameWorld.Player().level)
        for i, v in ipairs(rewards) do
            local itemId = v[1]
            local num = v[2]
            table.insert(myReward, TaskRewardData(itemId, num))
        end
    end
    return myReward
end

function EscortPeriTaskData:GetCommitTaskCallBack()
    return function() EventDispatcher:TriggerEvent(GameEvents.Escort_Peri_Commit_Task) end
end

function EscortPeriTaskData:GetCommitDialogArgs()
    return GuardGoddessDataHelper:GetName(self._id)
end

function EscortPeriTaskData:GetColorId()
    return IdToColorId[self._id]
end

---------------------------------------------------------------------------------------

local EscortPeriItemData = Class.EscortPeriItemData(ClassTypes.XObject)

function EscortPeriItemData:__ctor__(id)
    self._id = id
end

function EscortPeriItemData:GetIcon()
    return GuardGoddessDataHelper:GetIcon(self._id)
end

function EscortPeriItemData:GetCost()
    return GuardGoddessDataHelper:GetCost(self._id)
end

function EscortPeriItemData:GetReward()
    local rewards = GuardGoddessDataHelper:GetRewardConfig(self._id, GameWorld.Player().level)
    local items = {}
    for i, v in ipairs(rewards) do
        table.insert(items, TaskRewardData(v[1], v[2]))
    end
    return items
end

function EscortPeriItemData:GetId()
    return self._id
end

---------------------------------------------------------------------------------------
local EscortPeriData = Class.EscortPeriData(ClassTypes.XObject)

function EscortPeriData:__ctor__()
    self._infoItems = {}
    self:InitAllData()
end

function EscortPeriData:InitAllData()
    local list = GuardGoddessDataHelper:GetAllId()
    for k,v in ipairs(list) do
        table.insert(self._infoItems, EscortPeriItemData(v))
    end
end

function EscortPeriData:GetAllData()
    return self._infoItems
end

function EscortPeriData:GetInfo(id)
    return self._infoItems[id]
end

function EscortPeriData:GetRemainCount()
    return GlobalParamsHelper.GetParamValue(524) - GameWorld.Player().guard_goddess_count
end

function EscortPeriData:GetShowTips()
    local config = GlobalParamsHelper.GetParamValue(525)
    local now = DateTimeUtil.Now()
    local startH, endH ,startM, endM, startStr, endStr
    if now.hour > config[1]+config[3] or (now.hour == config[1]+config[3] and now.min >= config[2]+config[4]) then
        --显示第二时段提示
        startH = config[5]
        startM = config[6]
        endH = config[5]+config[7]
        endM = config[6]+config[8]
        if endM >= 60 then
            endH = endH + 1
            endM = endM - 60
        end
    else
        startH = config[1]
        startM = config[2]
        endH = config[1]+config[3]
        endM = config[2]+config[4]
        if endM >= 60 then
            endH = endH + 1
            endM = endM - 60
        end
    end
    startStr = string.format("%d:%s", startH, DateTimeUtil:FormatNumber(startM))
    endStr = string.format("%d:%s", endH, DateTimeUtil:FormatNumber(endM))
    local count = self:GetRemainCount()
    local text = LanguageDataHelper.CreateContentWithArgs(74902, {["0"]=string.format("%s-%s", startStr, endStr), ["1"]=count})
    return text
end

function EscortPeriData:IsInDoubleTime()
    local config = GlobalParamsHelper.GetParamValue(525)
    local now = DateTimeUtil.Now()
    if now.hour >= config[1] and now.min >= config[2] and now.hour <= config[1]+config[3] and now.min <= config[2]+config[4] then
        return true
    end
    if now.hour >= config[5] and now.min >= config[6] and now.hour <= config[5]+config[7] and now.min <= config[6]+config[8] then
        return true
    end
    return false
end

function EscortPeriData:NewTaskData(id, state)
    return EscortPeriTaskData:NewTaskData(id, state)
end