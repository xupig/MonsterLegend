local public_config = require("ServerConfig/public_config")
--设置一个队员的信息
local TeamMemberInfo = Class.TeamMemberInfo(ClassTypes.XObject)
local GUIManager = GameManager.GUIManager

function TeamMemberInfo:__ctor__(serverData)
	self._serverData 	= serverData
	self._isCaptain		= false --是否队长
	self._agreeToInstance   = false	--是否同意进入副本
end

function TeamMemberInfo:GetDBId()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_DBID]
end

function TeamMemberInfo:GetName()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_NAME]
end

function TeamMemberInfo:GetLevel()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_LEVEL]
end

function TeamMemberInfo:SetLevel(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_LEVEL] = value
end

function TeamMemberInfo:GetVocation()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_VOCATION]
end

function TeamMemberInfo:GetGender()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_RACE]
end

function TeamMemberInfo:GetRace()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_GENDER]
end


function TeamMemberInfo:GetFightForce()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_FIGHT_FORCE] or 0
end

--队员实体ID
function TeamMemberInfo:GetEntityId()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_EID]
end

--队员所在地图id
function TeamMemberInfo:GetMapId()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAP_ID]
end

function TeamMemberInfo:SetMapId(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAP_ID] = value
end

function TeamMemberInfo:GetMapLine()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAP_LINE]
end

function TeamMemberInfo:SetMapLine(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAP_LINE] = value
end

function TeamMemberInfo:GetOnline()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_ONLINE]
end

function TeamMemberInfo:SetOnline(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_ONLINE] = value
end

function TeamMemberInfo:GetHP()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_HP]
end

function TeamMemberInfo:SetHP(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_HP] = value
end


function TeamMemberInfo:GetMaxHP()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAX_HP]
end

function TeamMemberInfo:SetMaxHP(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAX_HP] = value
end

--魔法值
function TeamMemberInfo:GetMP()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MP]
end

function TeamMemberInfo:SetMP(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MP] = value
end

function TeamMemberInfo:GetMaxMP()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAX_MP]
end

function TeamMemberInfo:SetMaxMP(value)
	self._serverData[public_config.TEAM_MEMBER_INFO_KEY_MAX_MP] = value
end

function TeamMemberInfo:GetGuildID()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_GUILD_ID]
end

function TeamMemberInfo:GetIsCaptain()
	return self._isCaptain
end

function TeamMemberInfo:SetIsCaptain(isCaptain)
	self._isCaptain = isCaptain
end

function TeamMemberInfo:GetFacade()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_FACADE]
end

--队伍信息
local TeamInfo = Class.TeamInfo(ClassTypes.XObject)

function TeamInfo:__ctor__(serverData)
	self._serverData = serverData
    self._captainDBId 		= serverData[public_config.TEAM_TEAM_KEY_CAPTAIN_DBID]
    -- self._captainInfo		= nil
    -- self._memberInfos 		= {}
    -- local members 			= serverData[public_config.TEAM_TEAM_KEY_TEAM_MEMBER_INFOS]
    --LoggerHelper.Error("team.team_id"..team.team_id)
    -- if members then
    -- 	--遍历队伍里的队员信息
    -- 	local j = 1
    -- 	--local teamFightPower = 0
    -- 	for k1 = 1,#members do
	 		-- local member = TeamMemberInfo(members[k1])
	 		-- if self._captainDBId == member:GetDBId() then
	 		-- 	member._isCaptain = true
	 		-- 	self._captainInfo = member
	 		-- else
	 		-- end
	 		-- --teamFightPower = teamFightPower + member:GetFightForce()
	 		-- self._memberInfos[j] = member
	 		-- j = j+1
    -- 	end
    -- end
    self:SetHasApplied(false)
end

function TeamInfo:GetTeamId()
	return self._serverData[public_config.TEAM_TEAM_KEY_TEAM_ID]
end

function TeamInfo:GetCaptainDBId()
	return self._serverData[4]
end

function TeamInfo:GetTeamMemberCount()
	return self._serverData[3]
end

function TeamInfo:GetHasApplied()
	return self._hasApplied
end

function TeamInfo:SetHasApplied(hasApplied)
	self._hasApplied = hasApplied
end

function TeamInfo:GetTargetId()
	return self._serverData[2]--public_config.TEAM_TEAM_KEY_TARGET_ID
end

function TeamInfo:GetCaptainName()
	return self._serverData[5]
end

function TeamInfo:GetCaptainLevel()
	return self._serverData[6]
end

function TeamInfo:GetCaptainVocation()
	return self._serverData[7]
end

function TeamInfo:GetTeamMemberInfo()
	return self._memberInfos
end

--附近玩家信息
local TeamNearPlayerInfo = Class.TeamNearPlayerInfo(ClassTypes.XObject)

function TeamNearPlayerInfo:__ctor__(serverData)
	self._serverData 	= serverData
	--self._hasInvited	= false
end

function TeamNearPlayerInfo:GetDBId()
	return self._serverData[public_config.TEAM_MEMBER_INFO_KEY_DBID]
end

function TeamNearPlayerInfo:GetName()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_NAME]
end

function TeamNearPlayerInfo:GetGender()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_GENDER]
end

function TeamNearPlayerInfo:GetVocation()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_VOCATION]
end

function TeamNearPlayerInfo:GetRace()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_RACE]
end

function TeamNearPlayerInfo:GetLevel()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_LEVEL]
end

function TeamNearPlayerInfo:GetFightForce()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_FIGHT_FORCE]
end

function TeamNearPlayerInfo:GetMapId()
	return self._serverData[public_config.TEAM_NEAR_PLAYER_KEY_MAP_ID]
end

-- function TeamNearPlayerInfo:GetHasInvited()
-- 	return self._hasInvited
-- end

-- function TeamNearPlayerInfo:SetHasInvited(hasInvited)
-- 	self._hasInvited = hasInvited
-- end

-----------------------------------TeamData正文------------------------------------

local TeamData = Class.TeamData(ClassTypes.XObject)

function TeamData:__ctor__()
	self.AllTeamList = {}
	self.NearTeamList = {}
	self.MyTeam = {}
	self.FriendTeam = {}
	self.NearPlayer = {}
	self.TotalTeamListPage = 0
	self.TotalPlayerListPage = 0
	self.AutoAcceptApply = false
	self.InvitedPlayer = {}
	self.curMapId = 0 --当前将进入的副本ID
end

--增加某队员
function TeamData:AddTeamMember(serverData)
	if serverData ~= nil then
		local member = TeamMemberInfo(serverData)
		local memberInfos = self.MyTeam.memberInfos
		if member:GetDBId() and memberInfos then
			 for i=1,3 do
			 	if memberInfos[i] == nil then
					memberInfos[i] = member
					-- local str = LanguageDataHelper.GetContent(10023):format(member:GetName())
					-- GUIManager.ShowText(2,str)
					break
			 	end
			 end
		end
	end
end

--删除某队员
function TeamData:DeleteTeamMember(serverData)
	local memberDbid = serverData[1]
	--有自己队伍信息才处理
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == memberDbid then
				memberInfos[k] = nil
			end
		end
	end
end

--更新某人上下线
function TeamData:UpdateTeamMemberOnline(serverData)
	local memberDbid = serverData[1]
	--有自己队伍信息才处理
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == memberDbid then
				member:SetOnline(serverData[2])
			end
		end
	end
end

--更新成员mapId/mapLine
function TeamData:UpdateTeamMemberMapInfo(serverData)
	local memberDbid = serverData[1]
	local mapId = serverData[2]
	local mapLine = serverData[3]
	--LoggerHelper.Error("UpdateTeamMemberMapInfo"..mapId.."/"..mapLine)
	--有自己队伍信息才处理
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == memberDbid then
				member:SetMapId(mapId)
				member:SetMapLine(mapLine)
			end
		end
	end
end

--队长变更
function TeamData:ChangeCaptain(serverData)
	local captainDbid = serverData[1]
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == captainDbid then
				member._isCaptain = true
			elseif member._isCaptain then
				member._isCaptain = false
			end
		end
	end
end

--刷新队员血量
function TeamData:RefeshMemberHP(serverData)
	local memberDbid = serverData[1]
	local curHp = serverData[2]
	local maxHp = serverData[3]
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == memberDbid then
				member:SetHP(curHp)
				member:SetMaxHP(maxHp)
			end
		end
	end
end

--刷新队员魔法值
-- function TeamData:RefeshMemberMP(serverData )
-- 	local memberDbid = serverData[1]
-- 	local curMp = serverData[2]
-- 	local maxMp = serverData[3]
-- 	local memberInfos = self.MyTeam.memberInfos
-- 	for k,member in pairs(memberInfos) do
-- 		if mmember:GetDBId() == memberDbid then
-- 			member:SetMP(curMp)
-- 			member:SetMaxMP(maxMp)
-- 		end
-- 	end
-- end

--刷新队员等级
function TeamData:RefeshMemberLevel(serverData)
	local memberDbid = serverData[1]
	local level = serverData[2]
	local memberInfos = self.MyTeam.memberInfos
	for k,member in pairs(memberInfos) do
		if member:GetDBId() == memberDbid then
			member:SetLevel(level)
		end
	end
end

function TeamData:RefeshTargetId(serverData)
	self.MyTeam.targetId = serverData[1]
	self._readyTarget = serverData[1]
end

--预备设置的targetId
function TeamData:SetReadyTarget(value)
	self._readyTarget = value
end

function TeamData:GetReadyTarget()
	return self._readyTarget
end

--更新我的队伍队员信息
function TeamData:UpdateMyTeam(serverData)
	-- LoggerHelper.Log("TeamData:UpdateMyTeam")
	-- LoggerHelper.Log(serverData)
	local teamId 				= serverData[1]
	local captainId 			= serverData[2]
	local memberInfos 			= serverData[3]
	local targetId 				= serverData[4]
	local auto_accept_apply 	= serverData[5]
	--队伍信息
	local team = {}
	team.teamId 		= teamId
	team.captainId 		= captainId
	team.memberInfos 	= {}
	team.targetId 		= targetId
	team.autoAcceptApply= auto_accept_apply

	local k = 1
	for i=1,#memberInfos do
		local data = memberInfos[i]
		local member = TeamMemberInfo(data)
		member:SetIsCaptain(captainId == data[1])
		team.memberInfos[i] = member
		k = k+1
	end
	self.MyTeam = team
end

function TeamData:GetMyTeam()
	return self.MyTeam
end

--获取我的队伍的队员信息
function TeamData:GetMyTeamMemberInfo()
	return self.MyTeam.memberInfos
end

function TeamData:GetMyTeamMemberCount()
	local count = 0
	for k,v in pairs(self.MyTeam.memberInfos) do
		count = count + 1
	end
	return count
end

function TeamData:GetMyMapId()
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		for k,member in pairs(memberInfos) do
			if member:GetDBId() == GameWorld.Player().dbid then
				return member:GetMapId()
			end
		end
	end
	return 0
end

function TeamData:GetMyTeamMemberInfos()
	-- LoggerHelper.Error("队长id ====" .. tostring(self._captainDBId))
	local datas = {}
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		local j = 1
		for k,member in pairs(memberInfos) do
			datas[j] = member
			j = j+1
		end
	end
	return datas
end

--获取我的队伍除自己以外的队员信息
function TeamData:GetMyTeamMemberInfosWithOutMe()
	local memberInfosWithOutMe = {}
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		local j = 1
		for k,member in pairs(memberInfos) do
			if member:GetDBId() ~= GameWorld.Player().dbid then
				memberInfosWithOutMe[j] = member
				j = j+1
			end
		end
	end
	return memberInfosWithOutMe
end


--获取同mapid 的队员数量（包括自己）
function TeamData:GetNearbyMemberCount()
	local count = 0
	local memberInfos = self.MyTeam.memberInfos
	if memberInfos then
		local mapId = GameManager.GameSceneManager:GetCurrMapID()
		local mapLine = GameWorld.Player().map_line
		for k,member in pairs(memberInfos) do
			if mapId == member:GetMapId() and member:GetOnline() ~=0 then
				count = count+1
			end
		end
	end
	return count
end

--更新全部队伍信息
function TeamData:UpdateAllTeam(serverData)
	self.AllTeamList = {}
	local teamInfos = serverData[public_config.TEAM_TEAMS_KEY_NEAR_TEAMS]

	if teamInfos ~= nil then
		local  teamCurrentPage = serverData[public_config.TEAM_TEAMS_KEY_PAGE]
		self.TotalAllTeamPage = serverData[public_config.TEAM_TEAMS_KEY_TOTAL_PAGE]
		--LoggerHelper.Error("teamCurrentPage:"..teamCurrentPage.." TeamData.TotalTeamListPage"..TeamData.TotalTeamListPage)
		for i = 1, #teamInfos do
			local team = TeamInfo(teamInfos[i])
		    --team.current_page       = teamCurrentPage
		    --设置页面数据
		    -- if self.AllTeamList[teamCurrentPage] == nil then
		    -- 	self.AllTeamList[teamCurrentPage] = {}
		    -- end
			self.AllTeamList[i] = team
		end
	end
end

--更新附近队伍信息
-- function TeamData:UpdateNearTeam(serverData)
-- 	LoggerHelper.Log(serverData[public_config.TEAM_TEAMS_KEY_NEAR_TEAMS])
-- 	local teamInfos = GameWorld.CSLuaStringToLuaTable(serverData[public_config.TEAM_TEAMS_KEY_NEAR_TEAMS]) 
-- 	if teamInfos ~= nil then
-- 		local  teamCurrentPage = serverData[public_config.TEAM_TEAMS_KEY_PAGE]
-- 		self.TotalNearTeamPage = serverData[public_config.TEAM_TEAMS_KEY_TOTAL_PAGE]
-- 		--LoggerHelper.Error("teamCurrentPage:"..teamCurrentPage.." TeamData.TotalTeamListPage"..TeamData.TotalTeamListPage)
-- 		local i = 1
-- 		for k = 1, #teamInfos do
-- 			local v = teamInfos[k]
-- 			local team = TeamInfo(v)
-- 		    --team.current_page       = teamCurrentPage
-- 		    --设置页面数据
-- 		    -- if self.NearTeamList[teamCurrentPage] == nil then
-- 		    -- 	self.NearTeamList[teamCurrentPage] = {}
-- 		    -- end
-- 			self.NearTeamList[i] = team
-- 			i = i+1
-- 		end
-- 	end
-- end

--------------------------------邀请玩家相关处理---------------------------------------
function TeamData:UpdateAllNearPlayer(serverData)
	local playerInfos = serverData[public_config.TEAM_NEAR_PLAYERS_KEY_NEAR_PLAYERS]
	local currentPage = serverData[public_config.TEAM_NEAR_PLAYERS_KEY_PAGE]
	local totalPage   = serverData[public_config.TEAM_NEAR_PLAYERS_KEY_TOTAL_PAGE]
	self._nearPlayerPage = currentPage
	-- if currentPage == 1 then
		
	-- end
	self.NearPlayer = {}
	for i=1,#playerInfos do
		local player = TeamNearPlayerInfo(playerInfos[i])
		table.insert(self.NearPlayer,player)
		--LoggerHelper.Error("TeamData.AllPlayer" .. k)
	end
end

function TeamData:SetHasInvited(playerDBId)
	self.InvitedPlayer[playerDBId] = 1
end

function TeamData:GetHasInvited(playerDBId)
	return self.InvitedPlayer[playerDBId] ~= nil
end
--------------------------------副本相关处理---------------------------------------
function TeamData:StartEnterCopy(serverData)
	self.curMapId = serverData[1]
	--LoggerHelper.Error("self.curMapId"..self.curMapId)
end

function TeamData:ReceiveAgreeResult(serverData)
	local memberDbid = serverData[1]
	local memberInfos = self.MyTeam.memberInfos
	for k,member in pairs(memberInfos) do
		if member:GetDBId() == memberDbid then
			member._agreeToInstance = true
		end
	end
end

--判断是否所有队员已经同意
function TeamData:CheckAllMemberAgree()
	local memberInfos = self.MyTeam.memberInfos
	for k,member in pairs(memberInfos) do
		if member._isCaptain == false and member._agreeToInstance == false then
			return false
		end
	end
	return true
end

--进入副本后清除所有
function TeamData:ClearAgreeToCopy()
	local memberInfos = self.MyTeam.memberInfos
	for k,member in pairs(memberInfos) do
		member._agreeToInstance = false
	end
end