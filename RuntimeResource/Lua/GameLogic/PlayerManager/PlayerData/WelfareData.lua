--福利系统
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InvestVipDataHelper = GameDataHelper.InvestVipDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestMonthDataHelper = GameDataHelper.InvestMonthDataHelper
local TotalChargeDataHelper = GameDataHelper.TotalChargeDataHelper
local WelfareData = Class.WelfareData(ClassTypes.XObject)
local WelfareInveListItemData = Class.WelfareInveListItemData(ClassTypes.XObject)
local WelfareLevelListItemData = Class.WelfareLevelListItemData(ClassTypes.XObject)
local WelfareMonthListItemData = Class.WelfareMonthListItemData(ClassTypes.XObject)
local WelfareAddListItemData = Class.WelfareAddListItemData(ClassTypes.XObject)
local AddChargeDays = 7
function WelfareData:__ctor__(svrData)
    self._levelUpLeftList = {}--冲级豪礼剩下数量列表
end

--更新推荐列表数据
function WelfareData:UpdateLevelUpListData(svrData)
    self._levelUpLeftList = svrData
end

--更新推荐列表数据
function WelfareData:GetLevelUpListData()
    return self._levelUpLeftList
end

--更新在线列表数据
function WelfareData:UpdateOnlineLevelUpListData(svrData)
    self._onlineLevelUpLeftList = svrData
end

--更新在线列表数据
function WelfareData:GetOnlineLevelUpListData()
    return self._onlineLevelUpLeftList
end

function WelfareInveListItemData:__ctor__(week,day,id,flage,canget)
    self._day = day
    self._flage = flage
    self._week = week
    self._id = id
    self._canget = canget
end

-- 是否已经领取
function WelfareInveListItemData:IsGet()
    return self._flage
end
-- 第几天
function WelfareInveListItemData:GetVipday()
    return self._day
end
-- 第几周期
function WelfareInveListItemData:GetVipWeek()
    return self._week
end

function WelfareInveListItemData:GetRewards()
    return InvestVipDataHelper:GetReward(self._id)
end

function WelfareLevelListItemData:__ctor__(id,flage,type)
    self._flage = flage
    self._id = id
    self._type = type
end

-- 是否已经领取
function WelfareLevelListItemData:IsGet()
    return self._flage
end

-- 能领？
function WelfareLevelListItemData:VipCanGet()
    if not self._flage then
        if self:IsEnoughLevel() then
            local t = WelfareData:GetLevelInvesType()
            if t==0 or self._type~=t then
                return false
            end
            return true
        end
    end
    return false
end
function WelfareLevelListItemData:GetRewards()
    return InvestLevelDataHelper:GetReward(self._id)
end

function WelfareLevelListItemData:GetLevel()
    return InvestLevelDataHelper:GetLevel(self._id)
end

function WelfareLevelListItemData:GetType()
    return self._type
end

function WelfareLevelListItemData:IsEnoughLevel()
    local level = GameWorld.Player().level
    if self:GetLevel() <= level then return true end
    return false
end


function WelfareMonthListItemData:__ctor__(id,day,canget,isget)
    self._flage = canget--能领？
    self._id = id
    self._day = day
    self._isget = isget--是否领过？
end

function WelfareMonthListItemData:GetRewards()
    if self._day==1 then
        local r1=InvestMonthDataHelper:GetReward(1)
        local r2=InvestMonthDataHelper:GetReward(2)
        return {r1,r2}
    end
    return InvestMonthDataHelper:GetReward(self._id)
end
-- 能否领取
function WelfareMonthListItemData:IsCanGet()
    return self._flage
end

function WelfareMonthListItemData:GetDay()
    return self._day
end

function WelfareMonthListItemData:GetDes()
    local contentid = InvestMonthDataHelper:GetChinese(self._day)
    if contentid~=0 then
        return LanguageDataHelper.GetContent(contentid)
    end
    return ''
end
-- 
function WelfareMonthListItemData:IsGet()
    return self._isget
end

function WelfareAddListItemData:__ctor__(id,canget,isget)
    self._id = id
    self._canget = canget
    self._isget = isget
end

function WelfareAddListItemData:GetRewards()
    return TotalChargeDataHelper:GetReward(self._id)
end

function WelfareAddListItemData:GetCost()
    return TotalChargeDataHelper:GetCost(self._id)
end


function WelfareData:UpdateMonth()
    local entity = GameWorld.Player()
    local flage = entity.month_card_flag or 0
    local step = entity.month_card_daily_refund_step or 0
    local items = {}
    self._totalNum=0
    self._getNum=0
    if flage==0 then
        local ids = InvestMonthDataHelper:GetAllId()
        for _,id in ipairs(ids) do
            local day = InvestMonthDataHelper:GetDay(id)
            self._totalNum=self._totalNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
            items[day]= WelfareMonthListItemData(id,day,false,false)
        end
    else
        local ids = InvestMonthDataHelper:GetAllId()
        for _,id in ipairs(ids) do
            local day = InvestMonthDataHelper:GetDay(id)
            self._totalNum=self._totalNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
            if not WelfareData:IsGetMonthToday() and day==step+1 and day~=30 then
                items[day]= WelfareMonthListItemData(id,day,true,false)
            else
                if step>=day and step~=0 then
                    items[day]= WelfareMonthListItemData(id,day,false,true)
                else
                    items[day]= WelfareMonthListItemData(id,day,false,false)
                    self._getNum = self._getNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
                end
            end
        end
    end
    table.sort(items,function(a,b)
        if a:IsCanGet()==true and b:IsCanGet()==true then
            if a:GetDay()<b:GetDay() then
                return true
            else
                return false
            end
        else
            if a:IsCanGet()==true then
                return true
            elseif b:IsCanGet()==true then
                return false 
            end
            if a:IsGet()==true and b:IsGet()==true then
                if a:GetDay()>b:GetDay() then
                    return true
                end
                return false
            else
                if a:IsGet()==true and b:IsGet()==false then
                    return false
                elseif a:IsGet()==false and b:IsGet()==true then 
                    return true
                end
                if a:GetDay()<b:GetDay() then
                    return true
                else
                    return false
                end
            end
        end
    end)
    return items
end
function WelfareData:UpdateRemainNum()
    return self._totalNum-self._getNum
end
function WelfareData:UpdateInveList()
    local week=1
    local index=1
    local items = {}
    week,index = WelfareData:GetVipInvestIndex()
    local entity = GameWorld.Player()
    local worldlevel = entity.invest_vip_world_level or 1
    local rewardlists = entity.invest_vip_get_reward_days or {}
    local offvalve = InvestVipDataHelper:GetCfgTypeIndex(worldlevel)-1
    local x = 1
    for i=index+offvalve,index+6+offvalve do
        if rewardlists[index+x-1]==1 then
            items[x] = WelfareInveListItemData(week,index+x-1,i,true,false)
        else
            if rewardlists[index+x-1]==0 then
                items[x] = WelfareInveListItemData(week,index+x-1,i,false,true)
            else
                items[x] = WelfareInveListItemData(week,index+x-1,i,false,false)
            end
        end
        x = x+1
    end
    table.sort(items,function(a,b)
        if a._canget==true and b._canget==false then
            return true
        else
            if a._canget==false and b._canget==true then
                return false
            else
                if a:GetVipday()>b:GetVipday() then
                    return true
                end
                return false
            end
        end
    end)
    return items
end

function WelfareData:GetVipInvestIndex()
    local entity = GameWorld.Player()
    local rewardlists = entity.invest_vip_get_reward_days or {}
    local worldlevel = entity.invest_vip_world_level or 1
    local ids = InvestVipDataHelper:GetAllId()
    local items = {}  
    local indexs = {7,14,21,28}
    local index = 1
    local week = 1
    local y=0
    local a=0
    for k,v in pairs(rewardlists) do
        if v==0 then
            y=tonumber(k)
            break
        end
        a = a+1
    end
    local t
    if y~= 0 then
        t = math.floor(y/7)
    else
        t = math.floor(a/7)
    end
    week = t+1
    index = indexs[week]-6
    return week,index
end

function WelfareData:UpdateLevelList()
    local recordlists = GameWorld.Player().lv_invest_record or {}
    local type = self:GetLevelInvesType()
    self._total = 0
    local items = {}
    if type == 0 then 
        local types = InvestLevelDataHelper:GetAllType()
        local t = types[1]
        local cfg = InvestLevelDataHelper:GetCfgByType(t)
        for k,v in pairs(cfg) do
            items[v.id] = WelfareLevelListItemData(v.id,false,t)
        end
    else
        local cfg = InvestLevelDataHelper:GetCfgByType(type)
        local x = 1
        for k,v in pairs(cfg) do
            if recordlists[v.level] then
                items[x] = WelfareLevelListItemData(v.id,true,type)
            else
                items[x] = WelfareLevelListItemData(v.id,false,type)
                self._total = self._total+items[x]:GetRewards()[public_config.MONEY_TYPE_COUPONS_BIND]
            end
            x = x+1
        end
        table.sort(items,function(a,b)
            if a:IsGet()==true and b:IsGet()==false then
                return false
            elseif a:IsGet()==false and b:IsGet()==true then
                return true
            else
                if a:GetLevel()<b:GetLevel() then
                    return true
                else
                    return false
                end
            end
        end)
    end
    return items
end
function WelfareData:GetVipLevelByType(type)
    local items = {}
    local entity = GameWorld.Player()
    local cfg = InvestLevelDataHelper:GetCfgByType(type)
    local recordlists = entity.lv_invest_record or {}
    local hatype = entity.lv_invest_type
    local x = 1
    for k,v in pairs(cfg) do
        if recordlists[v.level] and type==hatype then
            items[x] = WelfareLevelListItemData(v.id,true,type)
        else
            items[x] = WelfareLevelListItemData(v.id,false,type)
            self._total = self._total+items[x]:GetRewards()[public_config.MONEY_TYPE_COUPONS_BIND]
        end
        x = x+1
    end
    table.sort(items,function(a,b)
        if a:IsGet()==true and b:IsGet()==false then
            return false
        elseif a:IsGet()==false and b:IsGet()==true then
            return true
        else
            if a:GetLevel()<b:GetLevel() then
                return true
            else
                return false
            end
        end
    end)
    return items
end

function WelfareData:GetResidue()
    return self._total
end
function WelfareData:GetInvestDay()
    local recordlists = GameWorld.Player().invest_vip_get_reward_days or {}
    local i=0
    for _,v in pairs(recordlists) do
        if v==1 then
            i = i + 1
        end
    end
    return i
end


function WelfareData:GetVipAdd()
    local items = {}
    local ids = TotalChargeDataHelper:GetAllId()
    local enity = GameWorld.Player()
    local vocation = math.floor(enity.vocation/100)
    local record = enity.cumulative_charge_refund_info or {}
    local amount = enity.cumulative_charge_amount or 0
    local cfg = TotalChargeDataHelper:GetRewardsByVocation(vocation)
    local x=1
    for k,v in ipairs(cfg) do
        if record[v.id]==1 then
            items[x]=WelfareAddListItemData(v.id,false,true)
        else
            local cost = v.cost
            if cost<=amount then 
                items[x]=WelfareAddListItemData(v.id,true,false)
            else
                items[x]=WelfareAddListItemData(v.id,false,false)
            end
        end
        x=x+1
    end
    table.sort(items,function(a,b)
        if a._canget==true and b._canget==false then
            return true
        else
            if a._canget==false and b._canget==true then
                return false
            elseif a._canget and b._canget then
                if a._id>b._id then
                    return false
                end
                return true
            else
                if a._isget==true and b._isget==false then
                    return false
                elseif a._isget==false and b._isget==true then                
                    return true
                else
                    if a._id>b._id then
                        return false
                    end
                    return true
                end
             
            end
        end
    end)
    return items
end

function WelfareData:GetLevelInvesType()
    return GameWorld.Player().lv_invest_type or 0
end
-- 今天是否领了月卡奖励
function WelfareData:IsGetMonthToday()
    local entity = GameWorld.Player()
    if entity.month_card_flag==1 and entity.month_card_daily_refund_flag==0 then
        return false
    end
    return true
end

function WelfareData:GetMonthStep()
    if GameWorld.Player().month_card_flag==0 then return 30 end
    return GameWorld.Player().month_card_daily_refund_step
end
-- 是否充月卡了
function WelfareData:GetIsMonthMon()
    if GameWorld.Player().month_card_flag==1 then
        return true
    end
    return false
end

-- 开服累充活动关闭状态 0: 有效 1: 关闭 --
function WelfareData:IsVipAddClosed()
    local createtime = GameWorld.Player().create_time
    local s = DateTimeUtil.MinusSec(createtime)
    if s>AddChargeDays*DateTimeUtil.OneDayTime then
        return true
    else
        return false
    end

end


