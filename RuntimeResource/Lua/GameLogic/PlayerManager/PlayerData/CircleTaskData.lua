--环任务信息    ------------------------CircleTaskData-------------------------
require "PlayerManager/PlayerData/TaskCommonItemData"
local TaskCommonItemData = ClassTypes.TaskCommonItemData

local CircleTaskData = Class.CircleTaskData(ClassTypes.TaskCommonItemData)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskDataHelper = GameDataHelper.TaskDataHelper
local public_config = GameWorld.public_config
local TaskConfig = GameConfig.TaskConfig
local CircleTaskDataHelper = GameDataHelper.CircleTaskDataHelper
local CircleTaskRewardDataHelper = GameDataHelper.CircleTaskRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskRewardData = ClassTypes.TaskRewardData
local FontStyleHelper = GameDataHelper.FontStyleHelper

function CircleTaskData:__ctor__()
    self._circleTaskId = nil
    self._taskItemType = TaskConfig.TASK_TYPE_CIRCLETASK
end

function CircleTaskData:IsSameTask(circleTask)
    if circleTask == nil then return false end
    if self:GetId() == circleTask:GetId() and self:GetState() == circleTask:GetState() and 
    self:CompareTargets(circleTask:GetTargetsCurTimes()) then
        return true
    end
    return false
end

function CircleTaskData:UpdateData(circleTaskId, serverDatas)
    --{"5" = {"1" = {"20004" = 0},"2" = 1}}
    self._serverData = serverDatas
    self._circleTaskId = circleTaskId

    if self:GetState() == public_config.TASK_STATE_COMPLETED and self._circleTaskId then
        if self:GetCommitNPC() == nil or self:GetCommitNPC() == 0 then
            EventDispatcher:TriggerEvent(GameEvents.COMMIT_CIRCLE_TASK, self:GetId())
        else

        end
    end
end

function CircleTaskData:GetId()
    return self._circleTaskId
end

function CircleTaskData:GetState()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_STATE] or nil
end

function CircleTaskData:CompareTargets(targets)
    local myTargets = self:GetTargetsCurTimes()
    if table.isEmpty(myTargets) then
        if table.isEmpty(targets) then
            return true
        else
            return false
        end
    else
        if table.isEmpty(targets) then
            return false
        else
            for k,v in pairs(myTargets) do
                if v ~= targets[k] then
                    return false
                end
            end
            return true
        end
    end
end

function CircleTaskData:GetTargetsCurTimes()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_TARGETS] or {}
end

function CircleTaskData:GetName()
    return CircleTaskDataHelper:GetName(self._circleTaskId)
end

function CircleTaskData:GetTaskDesc()
    if self._circleTaskId == nil then return "" end
    return LanguageDataHelper.CreateContent(CircleTaskDataHelper:GetDesc(self._circleTaskId))
end

function CircleTaskData:GetTitleNameAndColor()
    local color = FontStyleHelper:GetFontColor(27)
    if self._circleTaskId == nil then
        return LanguageDataHelper.CreateContent(136) .. LanguageDataHelper.CreateContent(925), color
    end
    return LanguageDataHelper.CreateContent(136) ..LanguageDataHelper.CreateContent(self:GetName()), color
end

--TODO 
function CircleTaskData:GetSelfLimit()
    return 0
end

function CircleTaskData:GetTargetsDesc()
    return CircleTaskDataHelper:GetTargetsDesc(self._circleTaskId)
end

function CircleTaskData:GetCommitNPC()
    -- LoggerHelper.Error("NPC ID ==============" .. tostring(CircleTaskDataHelper:GetCommitNpc(self._circleTaskId)))
    -- LoggerHelper.Error("self._circleTaskId ==============" .. tostring(self._circleTaskId))
    return CircleTaskDataHelper:GetCommitNpc(self._circleTaskId)
end

function CircleTaskData:GetCommitDialog()
    return CircleTaskDataHelper:GetCommitDialog(self._circleTaskId)
end

function CircleTaskData:NewCircleTaskData(id, state)
    local serverData = {}
    serverData[public_config.TASK_INFO_KEY_STATE] = state
    local cData = CircleTaskData()
    cData:UpdateData(id, serverData)
    return cData
end

function CircleTaskData:GetAcceptNPC()
    if self._circleTaskId == nil then
        local data = GlobalParamsHelper.GetParamValue(528)
        for k,v in pairs(data) do
            return k
        end
    end
end

function CircleTaskData:GetTargetsInfoData()
    return CircleTaskDataHelper:GetTargets(self._circleTaskId) 
end

function CircleTaskData:GetAcceptDialog()
    if self._circleTaskId == nil then
        local data = GlobalParamsHelper.GetParamValue(528)
        local datas = {}
        for k,v in pairs(data) do
            table.insert(datas, {
                [1] =   1,
                [2] =   {v,0,1},
            })
            return datas
        end
    end
end

function CircleTaskData:IsAutoTask()
    return true--self._circleTaskId ~= nil
end

function CircleTaskData:GetMyRewards()
    local myReward = {}
    if self._circleTaskId == nil then
        return myReward
    else        
        local rewardId = CircleTaskRewardDataHelper:GetRewardIdByLevel()
        if rewardId ~= nil then
            local rewards = CircleTaskRewardDataHelper:GetReward(rewardId)
            for i, v in ipairs(rewards) do
                local itemId = v[1]
                local num = v[2]
                if itemId == public_config.MONEY_TYPE_EXP then
                    local rate = CircleTaskDataHelper:GetRewardRate(self._circleTaskId)
                    num = math.floor((rate/10000) * num)
                end
                table.insert(myReward, TaskRewardData(itemId, num))
            end
        end
    end
    return myReward
end

function CircleTaskData:GetAcceptTaskCallBack()
    return function(id) EventDispatcher:TriggerEvent(GameEvents.ACCEPT_CIRCLE_TASK, id) end
end

function CircleTaskData:GetCommitTaskCallBack()
    return function(id) EventDispatcher:TriggerEvent(GameEvents.COMMIT_CIRCLE_TASK, id) end
end

function CircleTaskData:GetLittleShoesOpen()
    local state = self:GetState()
    if state == public_config.TASK_STATE_ACCEPTABLE then
        return CircleTaskDataHelper:GetLittleShoesOpenStart(self._circleTaskId) == 1
    elseif state == public_config .TASK_STATE_ACCEPTED then
        return CircleTaskDataHelper:GetLittleShoesOpen(self._circleTaskId) == 1
    elseif state == public_config.TASK_STATE_COMPLETED then
        return CircleTaskDataHelper:GetLittleShoesOpenEnd(self._circleTaskId) == 1
    end
end
