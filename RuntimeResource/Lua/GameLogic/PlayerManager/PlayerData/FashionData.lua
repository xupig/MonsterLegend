local FashionDataHelper = GameDataHelper.FashionDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local FashionData = Class.FashionData(ClassTypes.XObject)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function FashionData:__ctor__()

end

------------------------------------时装数据----------------------------------------------------
function FashionData:RefreshFashionInfo(changeValue)

end

function FashionData:RefreshPkgFashion(changeValue)

end

--返回 星级（-1表示未激活）
function FashionData:GetFashionActiveInfo(fashionId)
    local pos = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_FASHION):GetItemPosByItemId(fashionId)
    if pos > 0 then
        local itemData = PlayerManager.PlayerDataManager.bagData:GetItem(public_config.PKG_TYPE_FASHION, pos)
        return itemData:GetStar()
    end
    return -1
end

--返回type的所有激活id
function FashionData:GetFashionActiveIdListByType(type)
    local data = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_FASHION):GetItemInfos()
    local result = {}
    for k,v in pairs(data) do
        local id = v:GetItemID()
        local fashionType = FashionDataHelper.GetFashionType(id)
        if fashionType == type then
            table.insert(result, id)
        end
    end
    return result
end

--返回 时装类型对应的等级信息
function FashionData:GetFashionLevelInfo(type)
    local player = GameWorld.Player()
    local data = player.fashion_info[type]
    return data
end

function FashionData:GetCurLoadFashionId(type)
    local data = self:GetFashionLevelInfo(type)
    return data[public_config.FASHION_INFO_EQUIPPED] or 0
end

function FashionData:GetCurLoadFashionLevel(type)
    local data = self:GetFashionLevelInfo(type)
    return data[public_config.FASHION_INFO_LEVEL] or -1
end

function FashionData:GetActiveFashionAttri(type)
    local result = {}
    local data = self:GetFashionActiveIdListByType(type)
    local attris = GlobalParamsHelper.GetParamValue(824)[type]
    local allAttri = {}
    for k,v in pairs(attris) do
        allAttri[v] = 0
    end

    for k,v in pairs(data) do
        local star = self:GetFashionActiveInfo(v)
        local attri = FashionDataHelper.GetStarAttri(v, star)
        for k1,v1 in pairs(attri) do
            if allAttri[k1] ~= nil then
                allAttri[k1] = allAttri[k1] + v1
            else
                LoggerHelper.Error("时装属性id配置不一致, 时装类型：" ..type)
            end
        end
    end
    for k,v in pairs(allAttri) do
        table.insert(result,{k, v})
    end
    return result
end

function FashionData:GetCurLoadFashionAddition(type)
    local result = {}
    local level = self:GetCurLoadFashionLevel(type)
    if level == 0 then
        local addition = FashionDataHelper.GetLevelAdditionData(type, 0)
        for k,v in pairs(addition) do --用第一条数据attri得到所有属性种类
            table.insert(result, {k, v})
        end
    else
        local addition = FashionDataHelper.GetLevelAdditionData(type, level)
        for k,v in pairs(addition) do
            table.insert(result, {k, v})
        end
    end
    return result
end

--是否有穿戴时装衣服
function FashionData:IsFashionClothLoaded()
    local player = GameWorld.Player()
    local data = player.fashion_info[public_config.FASHION_TYPE_CLOTHES] or {}
    local id = data[public_config.FASHION_INFO_EQUIPPED] or 0
    if id > 0 then
        return true
    end
    return false
end

--是否有穿戴时装武器
function FashionData:IsFashionWeaponLoaded()
    local player = GameWorld.Player()
    local data = player.fashion_info[public_config.FASHION_TYPE_WEAPON] or {}
    local id = data[public_config.FASHION_INFO_EQUIPPED] or 0
    if id > 0 then
        return true
    end
    return false
end