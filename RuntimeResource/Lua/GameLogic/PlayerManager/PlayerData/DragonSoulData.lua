local DragonSoulData = Class.DragonSoulData(ClassTypes.XObject)
local DragonSoulItemData = Class.DragonSoulItemData(ClassTypes.XObject)
local DragonSoulSlotItemData = Class.DragonSoulSlotItemData(ClassTypes.XObject)
local DragonSoulUpItemData = Class.DragonSoulUpItemData(ClassTypes.XObject)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DragonsoulDataHelper = GameDataHelper.DragonsoulDataHelper
local DragonsoulAttriDataHelper = GameDataHelper.DragonsoulAttriDataHelper
local DragonsoulSlotDataHelper = GameDataHelper.DragonsoulSlotDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ComposeDragonsoulDataHelper = GameDataHelper.ComposeDragonsoulDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local  public_config = GameWorld.public_config

function DragonSoulItemData:__ctor__(id,level)
    self._id = id
    self._level = level
    self._att = DragonsoulAttriDataHelper:GetAttByIdle(self._id,level)
end

function DragonSoulItemData:GetIcon()
    return ItemDataHelper.GetIcon(self._id)
end

function DragonSoulItemData:IsFulllevel()
    if self._att==nil then
        return true
    end
    return false
end

function DragonSoulItemData:GetName()
    return ItemDataHelper.GetItemNameWithColor(self._id)
end


-- 龙魂的投放等级
function DragonSoulItemData:GetPutLevel()
end

function DragonSoulItemData:GetLevel()
    return self._att.level
end
function DragonSoulItemData:GetAtt()
    return self._att.attri
end

function DragonSoulItemData:GetUpcost()
    return self._att.cost
end

function DragonSoulItemData:GetResolve()
    return self._att.resolve
end

function DragonSoulItemData:GetQuality()
    return DragonsoulDataHelper:GetQuality(self._id)
end

function DragonSoulItemData:GetSubtype()
    return DragonsoulDataHelper:GetSubType(self._id)
end

function DragonSoulItemData:GetTypes()
    return DragonsoulDataHelper:GetType(self._id)
end

function DragonSoulItemData:GetTypeName()
    if self:GetSubtype()==2 then
        return LanguageDataHelper.GetContent(61013)
    end
    return LanguageDataHelper.GetContent(61012)
end

--是否双属性
function DragonSoulItemData:IsDoubleAtt()
    local type = DragonsoulDataHelper:GetType(self._id)
    if #type>1 then return true end
    return false
end

-- 拆解获得
function DragonSoulItemData:IsDissGet()
    if self:IsDoubleAtt() then
        local get = {}
        if self._level~=1 then
            local upcost={}
            local attcfg = DragonsoulAttriDataHelper:GetAttByIdle(self._id,self._level-1)
            if attcfg then
                upcost = attcfg.cost
            end
            get[1]=upcost
        end
        local comcfg = ComposeDragonsoulDataHelper:GetCfgData(self._id)
        if comcfg and comcfg.dragonsoul_id then
            get[2]=comcfg.dragonsoul_id
        end
        return get
    end
end

function DragonSoulUpItemData:__ctor__(slot,id,level)
    self._slot = slot
    self._id = id
    self._level = level
    self._item = DragonSoulItemData(id,level) 
end


function DragonSoulSlotItemData:__ctor__(slot,flage,id,level)
    self._slot=slot
    self._flage=flage
    self._id = id
    self._level = level
end

-- 槽的开启等级
function DragonSoulSlotItemData:GetOpenlevel()
    return DragonsoulSlotDataHelper:GetLevel(self._slot)
end

function DragonSoulSlotItemData:GetItem()
    return DragonSoulItemData(self._id,self._level)
end
-- 是否有
function DragonSoulSlotItemData:IsHave()
    return self._flage
end

function DragonSoulSlotItemData:IsLock()
    local playerle = GameWorld.Player().level
    if playerle>=self:GetOpenlevel() then
        return false
    end
    return true
end
-- 是否核心slot
function DragonSoulSlotItemData:IsCore()
    if DragonsoulSlotDataHelper:GetType(self._slot)==2 then
        return true
    end
    return false
end

-- slot名字
function DragonSoulSlotItemData:GetSlotName()
    if self:IsCore() then
        return LanguageDataHelper.GetContent(61013)
    end
    return LanguageDataHelper.GetContent(61012)
end

function DragonSoulSlotItemData:IsRed()
    
end



function DragonSoulData:__ctor__()
end

function DragonSoulData:UpdateDragonSoulSlot()
    local item={}
    local attach = GameWorld.Player().dragon_spirit or {}
    for i=1,7 do 
        if attach[i] ==nil then
            item[i]=DragonSoulSlotItemData(i,false)
        else
            item[i]=DragonSoulSlotItemData(i,true,attach[i][1],attach[i][2])
        end
    end
    return item
end

function DragonSoulData:UpdateDragonSoulAtts()
    local items={}
    local attach = GameWorld.Player().dragon_spirit or {}
    for k,v in pairs(attach) do 
        table.insert(items,DragonSoulItemData(v[1],v[2]):GetAtt()) 
    end
    return items
end
function DragonSoulData:UpdateDragonSoulBag()
    local item={}
    local bag = GameWorld.Player().dragon_spirit_bag or {}
    local coutnum = GlobalParamsHelper.GetParamValue(724)
    local i=1
    for k, v in pairs(bag) do
        item[i]={tonumber(k),DragonSoulItemData(v[1],v[2])}
        i=i+1
    end
    table.sort(item,function(a,b)
        if a[2]:GetQuality()>b[2]:GetQuality() then
            return true
        elseif a[2]:GetQuality()<b[2]:GetQuality() then           
            return false
        else
            if a[2]._level>b[2]._level then
                return true
            else
                return false
            end
        end
    end)
    for j=i,coutnum do 
        item[j]=-1
    end
    return item
end

function DragonSoulData:UpdateDragonSoulResBag()
    local items={}
    local bag = GameWorld.Player().dragon_spirit_bag or {}
    local coutnum = GlobalParamsHelper.GetParamValue(724)
    local i=1
    self.total = 0
    for k, v in pairs(bag) do
        local type = DragonsoulDataHelper:GetType(v[1])
        local item = DragonSoulItemData(v[1],v[2])
        local c = item:GetResolve()  
        if #type<2 and c then
            items[i]={tonumber(k),item}
            i=i+1
            local q = item:GetQuality()
            if q==1 or q==3 or item:GetTypes()[1]==99 then
                local n = c[public_config.MONTY_TYPE_DRAGON_PICEC]
                self.total = self.total+n
            end
        end
    end
    for j=i,coutnum do 
        items[j]=-1
    end
    return items
end

function DragonSoulData:GetResNum()
    return self.total or 0
end
function DragonSoulData:GetDragonSoulItem(id,level)
    return DragonSoulItemData(id,level)
end

function DragonSoulData:GetDragonSoulUpItem(slot,id,level)
    return DragonSoulUpItemData(slot,id,level)
end

function DragonSoulData:HandleDragonSoulBag(data)
    self._bag = data
end
-- 根据id得到数量
function DragonSoulData:GetDragonSoulNum(id)
    if self._bag and self._bag[id] then
        return #self._bag[id]
    end
    return 0
end
-- 碎片
function DragonSoulData:GetCost1()
    local piece = GameWorld.Player().money_dragon_piece or 0
    return {piece,23}
end
-- 龙魂石,结晶
function DragonSoulData:GetCost2()
    local bagData = PlayerManager.PlayerDataManager.bagData
    local bagNum1 = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(1950)    
    local bagNum2 = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(1951)  
    return {{bagNum1,1950},{bagNum2,1951}}
end
