local FortData = Class.FortData(ClassTypes.XObject)
local FortPlayerData = Class.FortPlayerData(ClassTypes.XObject)
local CommonItemData = ClassTypes.CommonItemData
local public_config = require("ServerConfig/public_config")
local PVPDataHelper = GameDataHelper.PVPDataHelper
local GameSceneManager = GameManager.GameSceneManager
local EntityType = GameConfig.EnumType.EntityType

--据点PVP数据集
function FortData:__ctor__()
	self._onHPChangeCB = function ()
		EventDispatcher:TriggerEvent(GameEvents.FORT_FACTION_MEMBER_HP_UPDATE)
		--LoggerHelper.Error("FORT_FACTION_MEMBER_HP_UPDATE")
	end
end

function FortData:InitFortData()
	self._playerList = {}--{阵营id = {1 = 玩家信息}}
	self._playerListByRank = {}
	self.finalScore1 = 0 --阵营1的得分
	self.finalScore2 = 0 --阵营2的得分
	self.result = -1	--PVP结果 0--平局 1--阵营1获胜 2--阵营2获胜

	local mapId = GameSceneManager:GetCurrMapID()
	self.fortCfg = PVPDataHelper.GetFort(mapId)
	self.winPoint = self.fortCfg.vicotry_score
end

function FortData:SetFactionMember()
	self._playerAvatarList = {} --从地图直接获取的玩家信息
	
	local allEntities = GameWorld.Entities()
	local myFaction = GameWorld.Player().faction_id
	for k,entity in pairs(allEntities) do
		if entity.entityType == EntityType.Avatar and myFaction == entity.faction_id then
			table.insert(self._playerAvatarList,entity)
			entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
    		entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
		end
	end
end

function FortData:GetFactionMember()
	return self._playerAvatarList
end

function FortData:ClearFactionMember()
	if self._playerAvatarList then
		for k,entity in pairs(self._playerAvatarList) do
			entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChangeCB)
			entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChangeCB)
		end
	end
end

function FortData:SetFortBaseInfo(serverData)
	
	--阶段 1 --等玩家入场 2 --等待光门关闭 3 --光门关闭 4 --结算 5 --弹结算信息给玩家 6 --回收场景
	self.room_stage = serverData[1]
	--TA, --阵营1 id
	self.score1 = serverData[3] --阵营1 积分
	--TB, --阵营2 id
	self.score2 = serverData[5] --阵营2 积分
	self.remain_sec = serverData[6] --这个阶段剩余的时间
	LoggerHelper.Error("FortData:SetFortBaseInfo"..self.room_stage)
end

--更新玩家数据
function FortData:UpdateFortPlayerInfo(serverData)
	for k,v in pairs(serverData) do
		--LoggerHelper.Error("UpdateFortPlayerInfo"..k)
		if self._playerList[k] == nil then
			self._playerList[k] = {}
		end
		for k1,v1 in pairs(v) do
			--LoggerHelper.Error("UpdateFortPlayerInfo1"..k1)
			local playerInfo = self._playerList[k][k1]
			if playerInfo == nil then
				playerInfo = FortPlayerData(v1)
				table.insert(self._playerListByRank,playerInfo)
				playerInfo.faction = k
				self._playerList[k][k1] = playerInfo
			else
				playerInfo:ResetSrvData(v1)
			end
		end
	end
	--按贡献排序
	local sortFun = function (a,b)
		return tonumber(a:GetContribution()) > tonumber(b:GetContribution())
	end
	table.sort( self._playerListByRank, sortFun ) 
end

function FortData:GetPlayerListByFaction()
	local result = {}
	for k,v in pairs(self._playerList) do
		for k1,v1 in pairs(v) do
			table.insert(result,v1) 
		end
	end
	return result
end

function FortData:GetPlayerListByRank()
	return self._playerListByRank
end

-- function FortData:UpdateAllFortInfo(serverData)
-- 	for k,v in pairs(serverData) do
-- 		if self.fortList[k] == nil then
-- 			local fortInfoData = FortInfoData(v)
-- 			self.fortList[k] = fortInfoData
-- 		else
-- 			self.fortList[k]:ResetSrvData(serverData)
-- 		end
-- 	end
-- end

function FortData:FortSettle(serverData)
	self:UpdateFortPlayerInfo(serverData[1])
	self.finalScore1 = serverData[5][1]
	self.finalScore2 = serverData[5][2]
	self.result = serverData[7] --0--平局 1--阵营1获胜 2--阵营2获胜
end

----------------------------单个据点信息-------------------------------
-- function FortInfoData:__ctor__(serverData)
-- 	self:ResetSrvData(serverData)
-- 	-- FORT_KEY_ID = 1 --id
-- 	-- FORT_KEY_FACTION = 2 --阵营
-- 	-- FORT_KEY_POWER = 3 --能量
-- 	-- FORT_KEY_POS = 4 --{x,y,z} 据点的具体位置
-- 	-- FORT_KEY_RAID = 5 --半径
-- 	-- FORT_KEY_NEARBY_COUNT = 6 --半径内的人数 （同一阵营的，有不同阵营的为0）
-- 	-- FORT_KEY_NEARBY_FACTION = 7 --半径内玩家的阵营 （同一阵营的，有不同阵营的为0）
-- end

-- function FortInfoData:ResetSrvData(serverData)
-- 	self._serverData = serverData
-- end

-- --id
-- function FortInfoData:GetId()
	
-- end

-- --阵营
-- function FortInfoData:GetFaction()
	
-- end

-- --能量
-- function FortInfoData:GetPower()
	
-- end

-- --{x,y,z} 据点的具体位置
-- function FortInfoData:GetPos()
	
-- end

-- --半径
-- function FortInfoData:GetRaid()
	
-- end

----------------------------据点玩家信息-------------------------------
function FortPlayerData:__ctor__(serverData)
	self:ResetSrvData(serverData)
	self.faction = 0
	-- PLAYER_KEY_DBID = 1 dbid
	-- PLAYER_KEY_VOCATION = 2 职业
	-- PLAYER_KEY_NAME = 3 名字
	-- PLAYER_KEY_LEVEL = 4 级别
	-- PLAYER_KEY_KILL = 5 击杀数
	-- PLAYER_KEY_DEATH = 6 死亡次数
	-- PLAYER_KEY_CONTRIBUTION = 7 贡献数
end

function FortPlayerData:ResetSrvData(serverData)
	self._serverData = serverData
end

function FortPlayerData:GetDBId()
	return self._serverData[public_config.PVP_PLAYER_KEY_DBID] or 0
end

function FortPlayerData:GetVocation()
	return self._serverData[public_config.PVP_PLAYER_KEY_VOCATION] or 0
end

function FortPlayerData:GetName()
	return self._serverData[public_config.PVP_PLAYER_KEY_NAME] or 0
end

function FortPlayerData:GetLevel()
	return self._serverData[public_config.PVP_PLAYER_KEY_LEVEL] or 0
end

function FortPlayerData:GetKill()
	return self._serverData[public_config.PVP_PLAYER_KEY_KILL] or 0
end

function FortPlayerData:GetDeath()
	return self._serverData[public_config.PVP_PLAYER_KEY_DEATH] or 0
end

function FortPlayerData:GetContribution()
	return self._serverData[public_config.PVP_PLAYER_KEY_CONTRIBUTION] or 0
end