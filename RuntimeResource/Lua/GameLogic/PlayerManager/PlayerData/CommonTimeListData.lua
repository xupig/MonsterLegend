local CommonTimeListConfig = GameConfig.CommonTimeListConfig
local DailyConfig = GameConfig.DailyConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local CommonTimeData = Class.CommonTimeData(ClassTypes.XObject)

function CommonTimeData:__ctor__(timeData)
    self._timeData = timeData or {}

    if self._timeData and self._timeData[1] and self._timeData[2] and self._timeData[3] and self._timeData[4] then
        self._startTime = self._timeData[1] * 60 + self._timeData[2]
        self._endTime = self._startTime + self._timeData[3] * 60 + self._timeData[4]
    end
end

--当天活动是否已经结束
function CommonTimeData:IsOver()
    local nowMins = DateTimeUtil.GetTodayMins()
    return self._endTime <= nowMins
end

--获取开启时间段中文,如：16:30-20:00
function CommonTimeData:GetOpenTimeText()
    -- LoggerHelper.Error("CommonTimeData:GetOpenTimeText() self._startTime ====" .. tostring(self._startTime))
    -- LoggerHelper.Error("CommonTimeData:GetOpenTimeText() self._endTime ====" .. tostring(self._endTime))
    return DateTimeUtil:ParseHourMinTime(tonumber(self._startTime)) .. "-".. DateTimeUtil:ParseHourMinTime(tonumber(self._endTime))
end

--获取开始时间
function CommonTimeData:GetStartTime()
    return self._startTime
end

--获取结束时间
function CommonTimeData:GetEndTime()
    return self._endTime
end

function CommonTimeData:GetOpenStatus()
    local nowMins = DateTimeUtil.GetTodayMins()
    if self._endTime <= nowMins then
        return CommonTimeListConfig.OVER_TIME
    elseif self._endTime > nowMins and self._startTime <= nowMins then
        return CommonTimeListConfig.OPENNING
    elseif self._startTime > nowMins then
        return CommonTimeListConfig.WAIT_OPEN
    end
end

local CommonTimeListData = Class.CommonTimeListData(ClassTypes.XObject)
--dayData:table,{1=1,2=3,3=5}:代表周几开启，table为空时代表每天开启
--timeData:table,{16,0,0,30,21,30,0,30}:每4个代表一个开启时间段，
--4个参数依次：开启时,分,持续时,分;table为空时代表全天开放
function CommonTimeListData:__ctor__(dayData, timeData)
    self._dayData = dayData or {}
    self._timeData = timeData or {}
    self._timeList = {}
    self._lastOpenTime = 0

    if not table.isEmpty(self._timeData) then
        for i,v in ipairs(timeData) do
            local n = i % 4
            if n == 1 then
                local data = {}
                table.insert(data, timeData[i])
                table.insert(data, timeData[i + 1])
                table.insert(data, timeData[i + 2])
                table.insert(data, timeData[i + 3])
                local commonData = CommonTimeData(data)
                if commonData:GetEndTime() > self._lastOpenTime then
                    self._lastOpenTime = commonData:GetEndTime()
                end
                table.insert(self._timeList, CommonTimeData(data))
            end
        end
    else

        self._lastOpenTime = 24 * 60
    end
end

--今天是否开启
function CommonTimeListData:IsOpenToday()
    local wDay = DateTimeUtil.GetWday()--今天星期几
    if table.isEmpty(self._dayData) or table.containsValue(self._dayData, wDay) then
        return true
    end
    return false
end

--获取活动最后结束时间
function CommonTimeListData:GetLastOpenTime()
    return self._lastOpenTime
end

--当天活动是否已经结束
function CommonTimeListData:IsOver()
    local nowMins = DateTimeUtil.GetTodayMins()
    return self._lastOpenTime <= nowMins
end

--获取活动状态
function CommonTimeListData:GetOpenStatus()
    if self:IsOpenToday() then
        if table.isEmpty(self._timeData) then
            return CommonTimeListConfig.OPENNING
        else
            for i,v in ipairs(self._timeList) do
                local status = v:GetOpenStatus()
                if status == CommonTimeListConfig.OPENNING then
                    return CommonTimeListConfig.OPENNING
                elseif status == CommonTimeListConfig.WAIT_OPEN then
                    return CommonTimeListConfig.WAIT_OPEN
                elseif status == CommonTimeListConfig.OVER_TIME then

                end
            end 
            return CommonTimeListConfig.OVER_TIME
        end
    end
    return CommonTimeListConfig.NOT_OPEN_TODAY
end

--获取最近的开始时间
function CommonTimeListData:GetLatestOpenTime()
    local status = self:GetOpenStatus()
    if status ~= CommonTimeListConfig.WAIT_OPEN then
        return nil
    end
    for i,v in ipairs(self._timeList) do
        local status = v:GetOpenStatus()
        if status == CommonTimeListConfig.WAIT_OPEN then
            return v:GetStartTime()
        end
    end
    return nil
end

--获取提示信息
function CommonTimeListData:GetDescText(s)
    local status = s or self:GetOpenStatus()

    if status == CommonTimeListConfig.OPENNING then
        -- return ""
        -- 描述加类型：空的显示：全天候 否则：开放中
        if table.isEmpty(self._timeData) then
            return LanguageDataHelper.CreateContent(51140)
        else
            return LanguageDataHelper.CreateContent(51139)
        end
    elseif status == CommonTimeListConfig.WAIT_OPEN then
        local openTime = self:GetLatestOpenTime()
        if openTime then
            return DateTimeUtil:ParseHourMinTime(openTime) .. LanguageDataHelper.CreateContent(912)
        end
    elseif status == CommonTimeListConfig.OVER_TIME then
        return LanguageDataHelper.CreateContent(52073)
    elseif status == CommonTimeListConfig.NOT_OPEN_TODAY then
        local str = LanguageDataHelper.CreateContent(910)
        for i=1,#self._dayData do
            str = str .. LanguageDataHelper.CreateContent(DailyConfig.WEEK_TEXT_DIC[self._dayData[i]])
            if i ~= #self._dayData then
                str = str .. LanguageDataHelper.CreateContent(911)
            end
        end
        for i=1,#self._timeList do
            str = str .. " " .. self._timeList[i]:GetOpenTimeText()
            if i ~= #self._timeList then
                str = str .. LanguageDataHelper.CreateContent(911)
            end 
        end
        str = str .. LanguageDataHelper.CreateContent(912)
        return str
    end
    return ""
end