local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local OpenHappyDataHelper = GameDataHelper.OpenHappyDataHelper
local OpenHappyConfig = GameConfig.OpenHappyConfig
local REWARD_SORT_WEIGHT = OpenHappyConfig.REWARD_SORT_WEIGHT
local EVENT_SORT_WEIGHT = OpenHappyConfig.EVENT_SORT_WEIGHT

local OpenHappyData = Class.OpenHappyData(ClassTypes.XObject)
local OpenHappyRewardData = Class.OpenHappyRewardData(ClassTypes.XObject) -- 奖励
local OpenHappyEventData = Class.OpenHappyEventData(ClassTypes.XObject) -- 前往

function OpenHappyData:__ctor__()
	self._serverdata = {}
end

function OpenHappyData:SetData(serverdata)
	self._serverdata = serverdata
end

function OpenHappyData:GetRewardDataList(dontSort)
	if not self._rewardDataList then
		self._rewardDataList = {}
		local ids = OpenHappyDataHelper:GetAllRewardIds()
		for _, id in ipairs(ids) do
			table.insert(self._rewardDataList, OpenHappyRewardData(id))
		end
	end
	if not dontSort then
		self:SortRewardDataList()
	end
	return self._rewardDataList
end

function OpenHappyData:SortRewardDataList()
	table.sort(self._rewardDataList, function(a, b)
		local wa, wb = REWARD_SORT_WEIGHT[a:GetState()], REWARD_SORT_WEIGHT[b:GetState()]
		if wa == wb then
			return a._id < b._id
		end
		return wa > wb
	end)
end

function OpenHappyData:GetEventDataList()
	if not self._eventDataList then
		self._eventDataList = {}
		local ids = OpenHappyDataHelper:GetAllEventIds()
		for _, id in ipairs(ids) do
			table.insert(self._eventDataList, OpenHappyEventData(id))
		end
	end
	table.sort(self._eventDataList, function(a, b)
		local wa, wb = REWARD_SORT_WEIGHT[a:GetState()], REWARD_SORT_WEIGHT[b:GetState()]
		if wa == wb then
			return a._id < b._id
		end
		return wa > wb
	end)
	return self._eventDataList
end

-- [id]={1:{event_cond_id:num},2:state}
function OpenHappyData:GetEventPointById(id)
	local data = self._serverdata[id]
	if data then
		local event_cond_id, num = next(data[1])
		if event_cond_id then
			return num
		end
		return -1 -- 完成 
	end
	return nil -- 没进度数据
end

function OpenHappyData:HasRedPoint()
	for i, v in ipairs(self:GetRewardDataList(true)) do
		if v:GetState() == OpenHappyConfig.State_CanReward then
			return true
		end
	end
	return false
end

function OpenHappyData:GetPointValue()
	return self._serverdata[public_config.FREAK_POINT] or 0
end

function OpenHappyData:GetPointValueStr()
	return LanguageDataHelper.CreateContent(80866, {["0"] = self:GetPointValue()})
end

function OpenHappyData:GetEndTime()
	local openStamp = DateTimeUtil.GetOpenServerTime()
	local openDate = DateTimeUtil.SomeDay(openStamp)
	local openSec = ((openDate.hour * 60) + openDate.min) *60 + openDate.sec
	return (openStamp + DateTimeUtil.OneDayTime * OpenHappyConfig.END_SERVER_DAY - openSec)
end

function OpenHappyData:GetRemainTime()
	local t = (self:GetEndTime() - DateTimeUtil.GetServerTime())
	t = t < 0 and 0 or t
	return t
end

function OpenHappyData:GetRemainTimeStr(remainTime)
	local remainTimeStr = DateTimeUtil.FormatParseTimeStr(remainTime)
	return LanguageDataHelper.CreateContent(81519, {["0"] = remainTimeStr}) 
end



--================================ 奖励 ===========================
function OpenHappyRewardData:__ctor__(id)
    self._id = id
end

function OpenHappyRewardData:GetId()
    return self._id
end

function OpenHappyRewardData:GetConfigData()
    return OpenHappyDataHelper:GetRewardDataById(self._id)
end

function OpenHappyRewardData:GetMaxValue()
    return self:GetConfigData().point or 0
end

function OpenHappyRewardData:GetItemList()
	local vg = GameWorld.Player():GetVocationGroup()
	local list = {}
	local rewardCfg = self:GetConfigData().reward
	if rewardCfg then
		local reward = rewardCfg[vg]
		for i = 1, #reward, 2 do
			table.insert(list, {itemId = reward[i], itemNum = reward[i+1]})
		end
	end
	return list
end

function OpenHappyRewardData:GetCurValue()
	return OpenHappyData.GetPointValue(PlayerManager.PlayerDataManager.openHappyData)
end

function OpenHappyRewardData:GetNameStr()
	local curValue = self:GetCurValue()
	local maxValue = self:GetMaxValue()
	curValue = curValue > maxValue and maxValue or curValue
	return LanguageDataHelper.CreateContent(80865, {["0"] = curValue, ["1"] = maxValue})
end

--<!-- 乱嗨领奖info -->
--<freak_reward_info>
--<Type> LUA_TABLE </Type>
--<Flags> BASE_AND_CLIENT </Flags>
--<Persistent> true </Persistent>
--</freak_reward_info>

function OpenHappyRewardData:GetState()
	local info = GameWorld.Player().freak_reward_info
	if info[self._id] == 1 then
		return OpenHappyConfig.State_Rewarded
	end
	local curValue = self:GetCurValue()
	local maxValue = self:GetMaxValue()
	return curValue >= maxValue and OpenHappyConfig.State_CanReward or OpenHappyConfig.State_NoRewad
end




--================================ 前往 ===========================
function OpenHappyEventData:__ctor__(id)
    self._id = id
end

function OpenHappyEventData:GetId()
    return self._id
end

function OpenHappyEventData:GetConfigData()
    return OpenHappyDataHelper:GetEventDataById(self._id)
end

function OpenHappyEventData:GetNameStr()
    return LanguageDataHelper.CreateContent(self:GetConfigData().name, {["0"] = self:GetPoint()})
end

function OpenHappyEventData:GetPoint()
	return self:GetConfigData().point
end

function OpenHappyEventData:GetState()
	local curCount = self:GetCurCount()
	local maxCount = self:GetMaxCount()
	if curCount >= maxCount then
		return OpenHappyConfig.EventState_Finish
	else
		return self:IsOpen() and OpenHappyConfig.EventState_Open or OpenHappyConfig.EventState_NotOpen
	end
end

function OpenHappyEventData:IsOpen()
    if self:IsLevelOpen() then
    	return self:IsTimeOpen()
    end
    return false
end

function OpenHappyEventData:IsLevelOpen()
    local openLevel = self:GetOpenLevel()
	return (openLevel == 0 or GameWorld.Player().level >= openLevel)
end

function OpenHappyEventData:IsTimeOpen()
    local config = self:GetConfigData().time_limit
    if not config then 
    	return true
    end
    local startTime = DateTimeUtil.TodayTimeStamp(config[1], config[2])
    local endTime = DateTimeUtil.TodayTimeStamp(config[1]+config[3], config[2]+config[4])
    local serverTime = DateTimeUtil.GetServerTime()
    if serverTime <= endTime and startTime <= serverTime then
        return true
    end
    return false
end

function OpenHappyEventData:GetFinishStr()
	if self:GetFollow() == nil then
		return LanguageDataHelper.CreateContent(58800)
	end
	return ""
end

function OpenHappyEventData:GetOpenLevel()
    return self:GetConfigData().level or 0
end

function OpenHappyEventData:GetIcon()
    return self:GetConfigData().icon
end

function OpenHappyEventData:GetNotOpenStr()
	if not self:IsLevelOpen() then
    	return LanguageDataHelper.CreateContent(self:GetConfigData().condition, {["0"] = self:GetOpenLevel()})
    else
    	return LanguageDataHelper.CreateContent(self:GetConfigData().condition_limit)
    end
end

function OpenHappyEventData:GetFollow()
    return self:GetConfigData().follow
end

function OpenHappyEventData:GetMaxCount()
	return self:GetConfigData().count
end

function OpenHappyEventData:GetCurCount()
	local count = OpenHappyData.GetEventPointById(PlayerManager.PlayerDataManager.openHappyData, self._id) or 0
	return (count == -1 and self:GetMaxCount() or count)
end

function OpenHappyEventData:GetTimesStr()
	return self:GetCurCount() .. "/" .. self:GetMaxCount()
end