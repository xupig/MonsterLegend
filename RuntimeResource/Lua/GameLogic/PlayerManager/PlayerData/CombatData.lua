local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")

local CombatData = Class.CombatData(ClassTypes.XObject)

function CombatData:__ctor__()
    --self.showData = {}
    self.CombatValue = 0
end

function CombatData:RefreshData(luaTable)
    --LoggerHelper.Log("CombatData:RefreshData")
    --LoggerHelper.Log(luaTable)
    self.battleData = luaTable
    self:RefreshBattleInfo()
    self.showData = {}
    local itemData = AttriDataHelper:GetAttrItems()
    local id, type, tmpCombatValue
    tmpCombatValue = luaTable[attri_config.ATTRI_ID_COMBAT_VALUE]
    if self.CombatValue ~= tmpCombatValue then
        self.CombatValue = tmpCombatValue
        --EventDispatcher:TriggerEvent(GameEvents.ON_COMBATVALUE_CHANGE)
    end
    for i=1,#itemData do
        id = itemData[i]
        type = AttriDataHelper:GetShowType(id)
        local property_order = AttriDataHelper:GetPropertyOrder(id)
        local tmp_Table = {}
        tmp_Table["id"] = id
        if luaTable[id] ~= nil then
            tmp_Table["value"] = luaTable[id]
        else
            tmp_Table["value"] = 0
        end
        tmp_Table["value"] = self:ConvertPercent(tmp_Table["value"],type)
        local category = math.floor(property_order / 100) --第三位表示子类型，用来分开显示
        tmp_Table["category"] = category
        table.insert(self.showData,tmp_Table)
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_ATTR_REFRESH)
end

function CombatData:ConvertPercent(value,type)
    type = type or 1
    if type == 2 then
        local result
        value = string.format("%.2f", value / 100)
        local result = value .. "%"
        return result
    else
        return value
    end
end
--处理过后的数据
function CombatData:GetAttrData()
    return self.showData
end

--根据id分类读取数据，以第三位位id分类 如：101归为id1 201归为id2
local idToData = {}
function CombatData:GetAttrDataById(id)
    local cacheData = {}
    for i=1,#self.showData do
        local data = self.showData[i]
        if data.category == id then
            table.insert(cacheData, data)
        end
    end
    idToData[id] = cacheData
    return cacheData
end
--未处理的服务器完整数据
function CombatData:GetServerAttrData()
    return self.battleData
end

function CombatData:GetCombatValue()
    return self.CombatValue
end

function CombatData:RefreshBattleInfo()
    GameWorld.Player():SetBattleAttributes(self.battleData)
end
