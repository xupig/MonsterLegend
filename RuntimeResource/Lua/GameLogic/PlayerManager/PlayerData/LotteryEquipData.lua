local EquipFindDataHelper = GameDataHelper.EquipFindDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local LotteryEquipData = Class.LotteryEquipData(ClassTypes.XObject)

function LotteryEquipData:__ctor__()

end

function LotteryEquipData:RefreshSelfRecord(data)
    self.selfRecordData = data
end

function LotteryEquipData:RefreshServerRecord(data)
    self.serverRecordData = data
end

function LotteryEquipData:RefreshRewardData(data)
    self.getRewardData = data
end