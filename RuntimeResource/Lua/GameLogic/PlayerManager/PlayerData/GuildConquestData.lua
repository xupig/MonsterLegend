local GuildDataHelper = GameDataHelper.GuildDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local GuildConquestData = Class.GuildConquestData(ClassTypes.XObject)

function GuildConquestData:__ctor__()
    self.conquestListData = {}
    self.conquestActionData = {}
    self.templeData = {}
    self.templeRewardState = 0
    self.canMoveByPath = false
end

--{1:{名字,对战state},2:nil,3,nil}
function GuildConquestData:RefreshListData(data)
    self.conquestListData = data
end

function GuildConquestData:RefreshActionData(action_id, data)
    self.conquestActionData[action_id] = data
end

function GuildConquestData:RetSetActionData()
    self.conquestActionData = {}
end

function GuildConquestData:RefreshTempleData(data)
    self.templeData = data
end

function GuildConquestData:RefreshTempleRewardState(data)
    self.templeRewardState = data[1] --奖励状态 1:已领取 0：其他
end

-------------------------------------------

function GuildConquestData:GetActionData(action_id)
    return self.conquestActionData[action_id]
end

function GuildConquestData:HasTempleData()
    if self.templeData[public_config.OVERLORD_KEY_GUILD_NAME] == nil then
        return false
    end
    return true
end

function GuildConquestData:IsSelfGuild()
    if self:HasTempleData() 
        and self.templeData[public_config.OVERLORD_KEY_GUILD_DBID] == PlayerManager.PlayerDataManager.guildData.guildId then
        return true
    end
    return false
end