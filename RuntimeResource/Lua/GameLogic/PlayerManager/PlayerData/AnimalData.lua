local AnimalData = Class.AnimalData(ClassTypes.XObject)

local MythicalAnimalsDataHelper = GameDataHelper.MythicalAnimalsDataHelper
local PkgEquipMythicalAnimalItemData = ClassTypes.PkgEquipMythicalAnimalItemData
local public_config = GameWorld.public_config
local AnimalConfig = GameConfig.AnimalConfig

require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local function SortEquipList(a,b)
    local typeNumA = a.itemCfgData.type
    local typeNumB = b.itemCfgData.type
    -- LoggerHelper.Error("typeNumA -========" .. tostring(typeNumA))
    -- LoggerHelper.Error("typeNumB -========" .. tostring(typeNumB))
    -- LoggerHelper.Error("selectTypeNum -========" .. tostring(selectTypeNum))
    if typeNumA ~= typeNumB then
        -- if typeNumA == selectTypeNum then
        --     return true
        -- end

        -- if typeNumB == selectTypeNum then
        --     return false
        -- end

        return typeNumA < typeNumB        
    else
        if typeNumA == AnimalConfig.EQUIP_UPGRADE_QULITY_6 then
            return false
        else 
            return a.itemData:GetFightPower() >  b.itemData:GetFightPower()
        end
    end
end


function AnimalData:__ctor__()
end

function AnimalData:InitData()

end

function AnimalData:GetAnimalListData()
    
end

--获取已经装备的神兽装备列表
function AnimalData:GetAnimalEquipListData()
    local datas = {}
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info and (not table.isEmpty(god_monster_info)) then
        for animalId,v in pairs(god_monster_info) do
            if self:IsHelp(animalId) then
                for i=1,5 do
                    local serverData = v[i]
                    if  serverData ~= nil then
                        local data = {}
                        data.pos = i
                        data.animalId = animalId
                        data.itemData = CommonItemData.CreateItemData(serverData,0,i)
                        -- data.itemData:SetAnimalId(animalId)
                        data.itemCfgData = data.itemData:GetCfgItemData()
                        table.insert(datas, data)
                    end
                end
            end
        end
    end
    return datas
end

--背包中是否存在对应位置的装备
function AnimalData:IsExitEquipByPos(animalId, pos)
    local quality_types = MythicalAnimalsDataHelper:GetQualityType(animalId)
    local quality = quality_types[pos]
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    if bag then        
        for k,v in pairs(bag:GetItemInfos()) do
            local itemCfgData = v:GetCfgItemData()
            if itemCfgData.quality >= quality and itemCfgData.type == pos then
                return true
            end
        end
    end
    return false
end

--获取有装备的分数最高的神兽
function AnimalData:GetHigherScoreAnimalId()
    local id = 1
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info == nil or table.isEmpty(god_monster_info) then
        return id
    end
    local score = -1

    for animalId,v in pairs(god_monster_info) do
        -- LoggerHelper.Error("animalId ===" .. tostring(animalId) .. "type ====" .. type(animalId), true)
        -- LoggerHelper.Error("animalId ~= nil" .. tostring(animalId ~= nil), true)
        if self:GetAnimalTotalScore(animalId) > score then
            score = self:GetAnimalTotalScore(animalId)
            id = animalId
        end
    end
    return id
end

--获取神兽评分
function AnimalData:GetAnimalTotalScore(animalId)
    local score = 0
    local baseScore = MythicalAnimalsDataHelper:GetMark(animalId)
    local god_monster_info = GameWorld.Player().god_monster_info
    score = score + baseScore
    if god_monster_info[animalId] ~= nil then
        for i=1,5 do
            local serverData = god_monster_info[animalId][i]
            if  serverData ~= nil then
                local itemData = CommonItemData.CreateItemData(serverData,0,i)
                score = score + itemData:GetFightPower()
            end
        end
    end
    return score
end

--是否存在紫色品质及以下的神兽装备存在
function AnimalData:UpgradeNeedRedImage()
    local helpNum = self:GetHelpNum()    
    if helpNum == 0 then
        return false
    end
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    if bag then
        for k,v in pairs(bag:GetItemInfos()) do
            if v:GetCfgItemData().quality <= 4 then
                return true
            end
        end
    end

    return false
end

--是否觉醒了
function AnimalData:IsAwake(animalId)
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[animalId] ~= nil then
        local num = 0
        for k,v in pairs(god_monster_info[animalId]) do
            num = num + 1
        end
        return num == 5
    end
    return false
end

--是否助战了
function AnimalData:IsHelp(animalId)
    local awakeData = GameWorld.Player().god_monster_assist_info
    if awakeData and awakeData[animalId] then
        return true
    end
    return false
end

--最多能助战的神兽数量
function AnimalData:GetMaxHelpNum()
    local baseNum = GlobalParamsHelper.GetParamValue(730)
    local canAddNum = #GlobalParamsHelper.GetParamValue(731)
    return baseNum + canAddNum
end

--获取可助战的神兽数量
function AnimalData:GetCanHelpNum()
    local baseNum = GlobalParamsHelper.GetParamValue(730)
    local addnum = GameWorld.Player().god_monster_assist_add_count
    return baseNum + addnum
end

--获取已经助战的数量
function AnimalData:GetHelpNum()
    local awakeData = GameWorld.Player().god_monster_assist_info
    local num = 0 
    if awakeData == nil or table.isEmpty(awakeData) then
        return 0        
    end
    for k,v in pairs(awakeData) do
        num = num + 1
    end
    return num
end


--根据神兽id和位置获取当前位置的装备
function AnimalData:GetEquipDataByAnimalIdAndPos(animalId, pos)
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info[self._selectId] == nil or table.isEmpty(god_monster_info[self._selectId]) then
        return nil
    else
        if god_monster_info[self._selectId][pos] then

        else
            return nil
        end
    end
    return nil
end

--神兽列表是否有红点
--规则：
-- 1.有空位时，可助战的加红点，(选中的神兽对应的助战按钮也红点)
-- 2.助战中的神兽有更高分数的装备可装备时 加红点，(选中的神兽对应的装备背包按钮也红点)
-- 3.(存在紫色及以下装备，神兽强化按钮红点)
--其中本方法实现非括号部分
function AnimalData:AnimalNeedRedPoint(animalId)
    if not self:IsHelp(animalId) then
        -- 1.有空位时，可助战的加红点
            -- local ids = MythicalAnimalsDataHelper:GetAllId()
            -- for i=1,#ids do
        return self:CheckAnimalCanHelp(animalId)
            -- end
    else
        --2.助战中的神兽有更高分数的装备可装备时 加红点  
        return self:IsHasHigherScore(animalId)
    end
    return false
end

--检查背包是否存在更高分数的装备进行替换（助战中）
function AnimalData:IsHasHigherScore(animalId)
    local nowScoreList = {}
    local god_monster_info = GameWorld.Player().god_monster_info
    if god_monster_info == nil or god_monster_info[animalId] == nil then
        LoggerHelper.Error("AnimalData:IsHasHigherScore(animalId) Error  animalId ====" .. tostring(animalId))
        return false
    end
    local animalData = god_monster_info[animalId]
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    if bag then        
        for k,v in pairs(bag:GetItemInfos()) do
            local typeNum = v:GetCfgItemData().type
            local score = nowScoreList[typeNum]
            if score == nil then
                if animalData[typeNum] == nil then
                    return false
                end
                local itemData = CommonItemData.CreateItemData(animalData[typeNum],0,typeNum)
                nowScoreList[typeNum] = itemData:GetFightPower()
                score = nowScoreList[typeNum]
            end
            if v:GetFightPower() > score then
                -- LoggerHelper.Error("pos === " .. tostring(typeNum))
                -- LoggerHelper.Error("身上装备的分数=====" .. tostring(score))
                -- LoggerHelper.Error("背包装备的分数=====" .. tostring(v:GetFightPower()))
                return true
            end
        end
    end
    return false
end

--检查神兽是否可助战
function AnimalData:CheckAnimalCanHelp(animalId)    
    local canHelpNum = self:GetCanHelpNum()
    local alreadyHelpNum = self:GetHelpNum()
    if canHelpNum > alreadyHelpNum then
        return self:IsCanAwake(animalId)
    else
        return false
    end
end

--是否能觉醒
function AnimalData:IsCanAwake(animalId)
    if self:IsAwake(animalId) then
        return true
    end
    local god_monster_info = GameWorld.Player().god_monster_info
    local hasEquipPosList = {}
    local needEquipList = {}
    if god_monster_info and god_monster_info[animalId] then
        --当前哪些部位已经装了装备
        for k,v in pairs(god_monster_info[animalId]) do
            hasEquipPosList[k] = 1
        end
    end
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    if bag then
        local quality_types = MythicalAnimalsDataHelper:GetQualityType(animalId)
        --获取哪些位置需要哪种品质装备的列表信息
        for i=1,5 do
            if hasEquipPosList[i] == nil then
                needEquipList[i] = quality_types[i]
            end
        end

        for k,v in pairs(bag:GetItemInfos()) do
            local num = nil
            local typeNum = v:GetCfgItemData().type
            for pos,qulity in pairs(needEquipList) do
                if pos == typeNum and qulity <= v:GetCfgItemData().quality then
                    num = pos
                    break
                end
            end
            if num then
                needEquipList[num] = nil
            end
            if table.isEmpty(needEquipList) then
                return true
            end
        end
    end

    return false
end

--根据神兽装备品质和星级获取神兽背包数据
function AnimalData:GetBadDataByQualityAndStart(animalId, quality, start, typeValue)
    return self:GetBadDataByType(animalId, nil, {
        ["quality"] = quality,
        ["start"] = start,
        ["typeValue"] = typeValue
    })
end

--根据类型查找神兽背包数据
function AnimalData:GetDataByType(typeNum)
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    local datas = {}
    if bag then
        for k,v in pairs(bag:GetItemInfos()) do
            local itemCfgData = v:GetCfgItemData()
            if itemCfgData.type == typeNum then
                local data = {}
                data.pos = k
                data.itemData = v
                data.itemCfgData = itemCfgData
                table.insert(datas, data)
            end
        end
    end

    local emptyNum = GlobalParamsHelper.GetParamValue(727) - #datas
    if emptyNum > 0 then
        for i=1,emptyNum do
            table.insert(datas, {})
        end
    end

    return datas
end

--根据您神兽装备部位获取神兽背包数据
function AnimalData:GetBadDataByType(animalId, typeNum, condition)
    --排序规则：
    --1.typeNum为nil时
    --  1）部位：角、牙、甲、爪、眼 -> 装备综合评分
    --2.typeNum有值时
    --  typeNum对应部位，角、牙、甲、爪、眼（剩余按照顺序排位）-> 装备综合评分
    local datas = {}    
    local typeDatas = {}
    local bag = PlayerManager.PlayerDataManager.bagData:GetPkg(public_config.PKG_TYPE_GOD_MOSNTER)
    if bag then
        for k,v in pairs(bag:GetItemInfos()) do
            local itemCfgData = v:GetCfgItemData()
            local flag = true
            if condition then
                if condition.typeValue == AnimalConfig.QUALITY_EQUAL then
                    if condition["quality"] and condition["quality"] ~= AnimalConfig.ALL_QUALITY_TYPE and condition["quality"] ~= itemCfgData.quality then
                        flag = false
                    end
                    if condition["start"] and condition["start"] ~= AnimalConfig.ALL_START_TYPE and condition["start"] ~= itemCfgData.star then
                        flag = false
                    end
                elseif condition.typeValue == AnimalConfig.QUALITY_LESS_OR_EQUAL then
                    if condition["quality"] and condition["quality"] ~= AnimalConfig.ALL_QUALITY_TYPE and condition["quality"] < itemCfgData.quality then
                        flag = false
                    end
                    if condition["start"] and condition["start"] ~= AnimalConfig.ALL_START_TYPE and condition["start"] < itemCfgData.star then
                        flag = false
                    end
                end
            end

            if flag then
                local data = {}
                data.pos = k
                v:SetAnimalId(animalId)
                data.itemData = v
                data.itemCfgData = itemCfgData
                if data.itemCfgData.type == typeNum then
                    table.insert(typeDatas, data)
                else        
                    table.insert(datas, data)
                end
            end
        end
    end
    if not table.isEmpty(datas) then        
        table.sort(datas, SortEquipList)
        if not table.isEmpty(typeDatas) then        
            table.sort(typeDatas, SortEquipList)
            for i,v in ipairs(datas) do
                table.insert(typeDatas, v)
            end
        else
            typeDatas = datas
        end
    end

    local emptyNum = GlobalParamsHelper.GetParamValue(727) - #typeDatas
    if emptyNum > 0 then
        for i=1,emptyNum do
            table.insert(typeDatas, {})
        end
    end

    return typeDatas
end

--根据条件筛选（品质、星级）,筛选之后的装备按照AnimalData:GetBadDataByType方法进行排序
function AnimalData:GetBadDataByCondition(qulity, start)

end