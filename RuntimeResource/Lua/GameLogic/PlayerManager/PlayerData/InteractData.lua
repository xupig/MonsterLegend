local public_config = GameWorld.public_config

local InteractItemData = Class.InteractItemData(ClassTypes.XObject)

function InteractItemData:__ctor__()
end

function InteractItemData:SetTitle(title)
    self._title = title
end

function InteractItemData:SetDetail(detail)
    self._detail = detail
end

function InteractItemData:GetTitle()
    return self._title
end

function InteractItemData:GetDetail()
    return self._detail
end

local InteractData = Class.InteractData(ClassTypes.XObject)

function InteractData:__ctor__(interactData)
    self._interacts = {}
    local tempInteract = {}
    for i, v in ipairs(interactData) do
        if i % 2 == 1 then
            tempInteract = InteractItemData()
            tempInteract:SetTitle(v)
            table.insert(self._interacts, tempInteract)
        elseif i % 2 == 0 then
            tempInteract:SetDetail(v)
        end
    end
end

function InteractData:GetInteracts()
    return self._interacts
end
