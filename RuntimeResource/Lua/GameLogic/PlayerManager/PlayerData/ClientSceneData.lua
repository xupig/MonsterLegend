local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")

local ClientSceneData = Class.ClientSceneData(ClassTypes.XObject)

function ClientSceneData:__ctor__()
    self.dropsData = {}
end

function ClientSceneData:RefreshDropsData(data)
    self.dropsData = data
end

--未找到返回:nil,nil 否则返回:{item=count,...}, index
function ClientSceneData:GetDropsByMonsterId(monsterID)
    local result, index
    for i=1, #self.dropsData do
        local data = self.dropsData[i]
        if data[1] ~= nil and monsterID == data[1] then
            result = data[2]
            index = i
            self.dropsData[i] = nil
            break
        end
    end
    -- LoggerHelper.Log(result)
    return result, index
end