local public_config = GameWorld.public_config
local MapDataHelper = GameDataHelper.MapDataHelper

local InstanceBalanceData = Class.InstanceBalanceData(ClassTypes.XObject)

--副本结算数据
function InstanceBalanceData:__ctor__()
    
end

function InstanceBalanceData:Init()
    self._mapId = nil
    self._result = nil
    self._rewards = {}
    self._moneyRewards = {}
    self._itemRewards = {}
    self._start = nil
    self._sceneType = nil
end

function InstanceBalanceData:UpdateData(serveData)
    --所有副本结算key
    -- INSTANCE_SETTLE_KEY_MAP_ID = 1, --地图id
    -- INSTANCE_SETTLE_KEY_RESULT = 2, --1成功，0失败。win = 1 or 0 
    -- INSTANCE_SETTLE_KEY_ITEMS  = 3, --奖励的道具 {k:v}
    -- INSTANCE_SETTLE_KEY_STAR   = 4, --星级
    -- INSTANCE_SETTLE_KEY_KILL_NUM    = 5, --击杀怪物数量
    -- INSTANCE_SETTLE_KEY_KILL_EXP    = 6, --守卫公会用的 杀怪经验
    -- INSTANCE_SETTLE_KEY_STAR_EXP   = 7, --守卫公会用的 评分经验
    -- LoggerHelper.Error("serveData =====" .. PrintTable:TableToStr(serveData))
    self:Init()
    if serveData == nil then
        do return end
    end
    
    self._mapId = serveData[public_config.INSTANCE_SETTLE_KEY_MAP_ID]  or nil
    if self._mapId then
        self._sceneType = MapDataHelper.GetSceneType(self._mapId)
    end

    if serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] then
        self._result = serveData[public_config.INSTANCE_SETTLE_KEY_RESULT] == 1
    end

    if serveData[public_config.INSTANCE_SETTLE_KEY_ITEMS] ~= nil and (not table.isEmpty(serveData[public_config.INSTANCE_SETTLE_KEY_ITEMS])) then
        self._rewards = serveData[public_config.INSTANCE_SETTLE_KEY_ITEMS]
        for itemId, num in pairs(self._rewards) do
            local isCanStack = self:IsCanStack(itemId)
            local data = {}
            data.itemId = itemId
            if isCanStack or self:IsMoney(itemId) then
                data.num = num
            end

            if self:IsMoney(itemId) then                
                table.insert(self._moneyRewards, data)
            else
                if isCanStack then
                    table.insert(self._itemRewards, data)
                else
                    data.num = 1
                    for i=1,tonumber(num) do
                        table.insert(self._itemRewards, data)
                    end
                end
            end
        end
    end

    local start
    if self._result then
        start = 3
    else
        start = 0

    end
    self._start = serveData[public_config.INSTANCE_SETTLE_KEY_STAR] or start

    --守卫女神
    self._killMonster = serveData[public_config.INSTANCE_SETTLE_KEY_KILL_NUM] or 0
    self._exp = serveData[public_config.INSTANCE_SETTLE_KEY_KILL_EXP] or 0
    self._starExp = serveData[public_config.INSTANCE_SETTLE_KEY_STAR_EXP] or 0

    --公会争霸
    self._conquestList = serveData[public_config.INSTANCE_SETTLE_KEY_CONQUEST_LIST] or {}
    --公会篝火
    self._banquetList = serveData[public_config.INSTANCE_SETTLE_KEY_BONFIRE_RANK] or {}
end

function InstanceBalanceData:IsMoney(itemId)
    return tonumber(itemId) < public_config.MAX_MONEY_ID
end

function InstanceBalanceData:IsCanStack(itemId)
    local itemType = ItemDataHelper.GetItemType(itemId)
    local iType = ItemDataHelper.GetType(itemId)
    
    if itemType == public_config.ITEM_ITEMTYPE_EQUIP  then
        return false
    else
        if itemType == public_config.ITEM_ITEMTYPE_ITEM and iType == 13 then
            return true
        end
        return ItemDataHelper:IsCanStack(itemId)
    end
end

function InstanceBalanceData:GetMapId()
    return self._mapId
end

function InstanceBalanceData:GetSceneType()
    return self._sceneType
end

function InstanceBalanceData:GetResult()
    return self._result
end

function InstanceBalanceData:GetMoneyRewards()
    return self._moneyRewards
end

function InstanceBalanceData:GetItemRewards()
    return self._itemRewards
end

function InstanceBalanceData:GetStart()
    return self._start
end

function InstanceBalanceData:GetKillMonster()
    return self._killMonster
end

function InstanceBalanceData:GetExp()
    return self._exp
end

function InstanceBalanceData:GetStarExp()
    return self._starExp
end

function InstanceBalanceData:GetConquestList()
    return self._conquestList
end

function InstanceBalanceData:GetBanquetList()
    return self._banquetList
end

function InstanceBalanceData:SetDefeat(defeat)
    self._defeat = defeat
end

function InstanceBalanceData:GetDefeat()
    return self._defeat
end

function InstanceBalanceData:SetStageScore(stageScore)
    self._stageScore = stageScore
end

function InstanceBalanceData:GetStageScore()
    return self._stageScore
end

function InstanceBalanceData:SetStageInfo(stageInfo)
    self._stageInfo = stageInfo
end

function InstanceBalanceData:GetStageInfo()
    return self._stageInfo
end

function InstanceBalanceData:SetNewStageIcon(newStageIcon)
    self._newStageIcon = newStageIcon
end

function InstanceBalanceData:GetNewStageIcon()
    return self._newStageIcon
end

function InstanceBalanceData:SetStageChangeText(stageChangeText)
    self._stageChangeText = stageChangeText
end

function InstanceBalanceData:GetStageChangeText()
    return self._stageChangeText
end