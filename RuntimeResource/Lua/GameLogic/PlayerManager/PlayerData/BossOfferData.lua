local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local BossOfferDataHelper = GameDataHelper.BossOfferDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local BossOfferData = Class.BossOfferData(ClassTypes.XObject)
local BossOfferItemData = Class.BossOfferItemData(ClassTypes.XObject)
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
-- local BOSS_REWARD_STATE_ACCEPTED  = 1 --｛进行中，id}
-- local BOSS_REWARD_STATE_COMPLETED = 2 --{已完成,id}
-- local BOSS_REWARD_STATE_RECEIVED  = 3 --{全部领取,id}

function BossOfferData:__ctor__()
    self._bossInfo = {}
    self._bossState= {}
    
end

function BossOfferItemData:__ctor__(name,iconid,level)
    self._name = name
    self._iconid = iconid
    self._level = level
end

function BossOfferData:UpdateInfo(info)
    self._bossInfo = info
end


function BossOfferData:UpdateState(info)
    self._bossState = info
end

function BossOfferData:GetState()
    return self._bossState
end

--icons
function BossOfferData:GetAllBossIconItems(id)
    local ids = BossOfferDataHelper:GetIcon(id)
    local items = {}
    for k, v in pairs(ids) do
        for k1,v1 in pairs(v) do
            local name = MonsterDataHelper.GetMonsterName(v1)
            local iconid = MonsterDataHelper.GetIcon(v1)
            local level = MonsterDataHelper.GetLv(v1)
            table.insert(items,BossOfferItemData(name,iconid,level))
        end
    end
    return items
end
-- 当前进度
function BossOfferData:GetCurProgress(id)
    local d = {}
    if self._bossInfo and self._bossInfo[id] then
        return self._bossInfo[id][1]
    end
    return d
end

function BossOfferData:GetTargetProgress(id)
    return BossOfferDataHelper:GetTargets(id)
end

-- 奖励
function BossOfferData:GetAllBossRewards(id)
    local d = {}
    local cfg = BossOfferDataHelper:GetReward(id)
    for k, v in pairs(cfg) do
        table.insert(d,{tonumber(k),v})
    end
    return d
end


--结束
function BossOfferData:IsOverNow(id)
    local ids = BossOfferDataHelper:GetAllId()
    if ids then
        if table.containsValue(ids,id) then
            return false
        else--结束
            return true
        end
    end
end
function BossOfferData:GetInfoState()
    if not self._bossInfo or table.isEmpty(self._bossInfo) then
        return {public_config.BOSS_REWARD_STATE_ACCEPTED,1}
    else
        for k,v in ipairs(self._bossInfo) do
            if v then
                if v[2]==public_config.BOSS_REWARD_STATE_COMPLETED then--完成但是没有领
                    return {public_config.BOSS_REWARD_STATE_COMPLETED,k}                    
                elseif v[2]==public_config.BOSS_REWARD_STATE_ACCEPTED then--进行中
                    return {public_config.BOSS_REWARD_STATE_ACCEPTED,k}                           
                end
            end
        end     
        local id = self:GetCurStep()
        return {public_config.BOSS_REWARD_STATE_RECEIVED,id}                           
    end
end

function BossOfferData:GetCurStep()
    local id = GameWorld.Player().boss_reward_unlocked_step
    if self:IsOverNow(id) then
        id = id - 1
    end
    return id
end

