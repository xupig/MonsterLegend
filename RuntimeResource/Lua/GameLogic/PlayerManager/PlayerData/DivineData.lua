local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local DivineData = Class.DivineData(ClassTypes.XObject)

function DivineData:__ctor__()

end

function DivineData:RefreshDivineInfo(changeValue)
    --LoggerHelper.Log("RefreshDivineInfo")
    --LoggerHelper.Log(GameWorld.Player().divine_info)
    local divineWeekCount = GameWorld.Player().divine_info[public_config.DIVINE_WEEK_NUM] or 0
    local divineDayCount = GameWorld.Player().divine_info[public_config.DIVINE_DAY_NUM] or 0
    local divineWeekRewardData = GameWorld.Player().divine_info[public_config.DIVINE_WEEK_NUM_REWARD] or {}
    local divineStarPower = GameWorld.Player().divine_info[public_config.DIVINE_STAR_POWER] or 0

    self.divineWeekCount = divineWeekCount
    self.divineDayCount = divineDayCount
    self.divineWeekRewardData = divineWeekRewardData
    self.divineStarPower = divineStarPower

    if changeValue == nil then
        return
    end
    for k,v in pairs(changeValue) do       
        --LoggerHelper.Log("change "..k)
        --删除数据
        if k == table.REMOVE_FLAG then
            --v是下标
        else
            if k == public_config.DIVINE_WEEK_NUM then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Divine_Week_Count)
            end
            if k == public_config.DIVINE_STAR_POWER then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Divine_Star_Power)
            end
            if k == public_config.DIVINE_WEEK_NUM_REWARD then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Divine_Week_Reward_Data)
            end
        end
    end
end