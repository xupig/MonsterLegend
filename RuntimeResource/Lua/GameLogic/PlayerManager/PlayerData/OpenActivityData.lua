local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper

local StringStyleUtil = GameUtil.StringStyleUtil

local OpenActivityData = Class.OpenActivityData(ClassTypes.XObject)


function OpenActivityData:__ctor__(svrData)
    self._remindList = {}--提醒列表
    self._remindPathList = {}--提醒列表

    self._limitRemindFuncMap = {} --限时活动列表
    self._limitRemidFuncEndTime = {} --限时活动列表
end

function OpenActivityData:SetChangePathList(remindType,path)
    self._remindPathList[remindType] = path
end

function OpenActivityData:GetRemindPathByType(remindType)
    return self._remindPathList[remindType] or false 
end

function OpenActivityData:SetChangeList(remindType,isShow)
    self._remindList[remindType] = isShow
end

function OpenActivityData:GetRemindByType(remindType)
    return self._remindList[remindType] or false
end

function OpenActivityData:SetAddLimitList(id,clickFunc,endTime)    
    self._limitRemindFuncMap[id] = clickFunc
    self._limitRemidFuncEndTime[id] = endTime

    --LoggerHelper.Error("OpenActivityData:SetAddLimitList"..endTime)
    EventDispatcher:TriggerEvent(GameEvents.LimitRemindListChange)
end

function OpenActivityData:SetRemoveLimitList(id)
    self._limitRemindFuncMap[id] = nil
    self._limitRemidFuncEndTime[id] = nil
    EventDispatcher:TriggerEvent(GameEvents.LimitRemindListChange)
end

function OpenActivityData:GetLimitRemindList()
    return self._limitRemindFuncMap
end


function OpenActivityData:CheckLimitRemindOpen(id)
    if self._limitRemindFuncMap[id] then
        return true
    else
        return false
    end
end


function OpenActivityData:GetLimitRemindEndTime(id)
    return self._limitRemidFuncEndTime[id]
end


