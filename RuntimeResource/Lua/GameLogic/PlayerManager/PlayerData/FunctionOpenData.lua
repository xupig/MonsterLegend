local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local MenuConfig = GameConfig.MenuConfig
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenData = Class.FunctionOpenData(ClassTypes.XObject)

function FunctionOpenData:__ctor__()
    self._cgStopOpen = GlobalParamsHelper.GetParamValue(932)
end

function FunctionOpenData:ClearData()
    self.functionOpenData = {}
    self.openShowQueue = {} --用来显示新开启功能tips
    self.init = false
    self._isStopOpenTips = false
end

function FunctionOpenData:RefreshOpenDataAll()
    --LoggerHelper.Error("FunctionOpenData:RefreshOpenDataAll()")
    if self._allId == nil then
        local allId,allIdForTips = FunctionOpenDataHelper:GetAllIdForTipsPanel()
        self._allId = allId
        self._allIdForTips = allIdForTips
    end
    for i, id in pairs(self._allId) do
        if FunctionOpenDataHelper:CheckFunctionOpen(id) then
            if self.functionOpenData[id] == nil then
                --新开启的功能
                --LoggerHelper.Error("functionOpenData"..id)
                self.functionOpenData[id] = 1
                if self.init then
                    EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_MAIN_MENU)
                    EventDispatcher:TriggerEvent(GameEvents.ON_FUNCTION_OPEN,id)
                    if self._cgStopOpen[id] then
                        self._isStopOpenTips = true
                    end

                    --需要显示功能开启面板
                    if self._allIdForTips[id] then
                        table.insert(self.openShowQueue, id)
                    end
                end
            end
        end
    end
    if self.init then
        if not GUIManager.PanelIsActive(PanelsConfig.FunctionOpenTips) then
            --MJ包屏蔽
            if GameWorld.SpecialExamine then
                return
            end
            if not self._isStopOpenTips then
                self:ShowOpenTips()
            end
        end
    else
        self.init = true
        self.openShowQueue = {}
    end
end

function FunctionOpenData:ShowOpenTips()
    if #self.openShowQueue > 0 then
        local id = table.remove(self.openShowQueue, 1)
        EventDispatcher:TriggerEvent(GameEvents.GUIDE_FUNCTION_OPEN,id)
        GUIManager.ShowPanel(PanelsConfig.FunctionOpenTips, {1,id})
    end
end

function FunctionOpenData:ContinueFunctionOpenPanel()
    --LoggerHelper.Error("ContinueFunctionOpenPanel")
    if self._isStopOpenTips then
        self._isStopOpenTips = false
        self:ShowOpenTips()
    end
end