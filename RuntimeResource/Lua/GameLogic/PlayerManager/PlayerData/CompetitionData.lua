local CompetitionData = Class.CompetitionData(ClassTypes.XObject)
local CompetitionPlayerData = Class.CompetitionPlayerData(ClassTypes.XObject)
local public_config = GameWorld.public_config

--切磋玩家数据
function CompetitionPlayerData:__ctor__(svrData)
	self._svrData = svrData
end

function CompetitionPlayerData:GetDBID()
	return self._svrData[public_config.FIGHT_PLAYER_PLAYER_KEY_DBID] or 0
end

function CompetitionPlayerData:GetName()
	return self._svrData[public_config.FIGHT_PLAYER_PLAYER_KEY_NAME] or 0
end


--切磋数据集
function CompetitionData:__ctor__()
	self._competitionPlayerData = nil
	self._isInitiator = false
	self._isDraw = false
end

function CompetitionData:SetCompetitionPlayerData(svrData)
	self._competitionPlayerData = CompetitionPlayerData(svrData)
end

function CompetitionData:GetCompetitionPlayerData()
	return self._competitionPlayerData
end

function CompetitionData:SetIsInitiator(isInitiator)
	self._isInitiator = isInitiator
end

--结算数据处理
function CompetitionData:SettleCompetition(args)
	local result = args[2]
	local playerInfos = args[1]
	if result == 0 then 
		self._isDraw = true
	elseif result == 1 then
		self._isDraw = false
		self._winnerData = CompetitionPlayerData(playerInfos[1])
		self._loserData = CompetitionPlayerData(playerInfos[2])
	else
		self._isDraw = false
		self._winnerData = CompetitionPlayerData(playerInfos[2])
		self._loserData = CompetitionPlayerData(playerInfos[1])
	end
end

function CompetitionData:GetIsDraw()
	return self._isDraw
end

function CompetitionData:GetWinner()
	return self._winnerData
end

function CompetitionData:GetLoser()
	return self._loserData
end




