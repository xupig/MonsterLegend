local public_config = GameWorld.public_config
local EventDataHelper = GameDataHelper.EventDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local TaskConfig = GameConfig.TaskConfig

local TaskRewardData = Class.TaskRewardData(ClassTypes.XObject)

function TaskRewardData:__ctor__(itemId, count)
    self._itemId = itemId
    self._count = count
end

function TaskRewardData:GetItemId()
    return self._itemId
end

function TaskRewardData:GetCount()
    return self._count
end

local EventItemData = Class.EventItemData(ClassTypes.XObject)

function EventItemData:__ctor__(id, totalTimes)
    self._id = id
    self._totalTimes = totalTimes
end

function EventItemData:ResetTaskData(totalTimes)
    self._totalTimes = totalTimes
end

function EventItemData:GetId()
    return self._id
end

function EventItemData:GetEventID()
    if self._id == 0 then
        return nil
    end
    return EventDataHelper:GetEventID(self._id)
end

function EventItemData:GetConds(paraId)
    if self._id == 0 then
        return nil
    end
    return EventDataHelper:GetConds(self._id, paraId)
end

function EventItemData:GetCondsOr()
    if self._id == 0 then
        return nil
    end
    return EventDataHelper:GetCondsOr(self._id)
end

function EventItemData:GetFollow()
    return self._id ~= 0 and EventDataHelper:GetFollow(self._id) or {}
end

-- 获取交付道具
function EventItemData:GetItem()
    local items = {}
    table.insert(items, TaskRewardData(self:GetConds(public_config.EVENT_PARAM_ITEM_ID), self:GetConds(public_config.EVENT_PARAM_NUM)))
    --LoggerHelper.Log("Ash: GetItem:" .. PrintTable:TableToStr(items))
    return items
end

function EventItemData:GetTotalTimes()
    return self._totalTimes
end

function EventItemData:SetNpcId(npcId)
    self._npcId = npcId
end

function EventItemData:GetTalkToSomeOneContent()
    local npcData = NPCDataHelper.GetNPCData(self._npcId)
    local npcName = LanguageDataHelper.CreateContent(npcData.name)
    return LanguageDataHelper.CreateContentWithArgs(TaskConfig.TALK_TO_SOMEONE, {["0"] = npcName})
end

local EventData = Class.EventData(ClassTypes.XObject)

function EventData:__ctor__()
    self._events = {}
    self._sortedEvents = {}
end

function EventData:UpdateSortedEventData(eventsData)
    if eventsData == nil then
        return
    end
    for i, eventData in ipairs(eventsData) do
        local id, totalTimes = eventData[1], eventData[2]
        if self._events[id] == nil then
            self._events[id] = EventItemData(id, totalTimes)
            table.insert(self._sortedEvents, self._events[id])
        else
            self._events[id]:ResetEventData(totalTimes)
        end
    end
end

function EventData:UpdateEventData(eventsData)
    if eventsData == nil then
        return
    end
    for id, totalTimes in pairs(eventsData) do
        if self._events[id] == nil then
            self._events[id] = EventItemData(id, totalTimes)
            table.insert(self._sortedEvents, self._events[id])
        else
            self._events[id]:ResetEventData(totalTimes)
        end
    end
end

function EventData:SetTalkToSomeOneEventData(npcId)
    self._events[0] = EventItemData(0, 1)
    self._events[0]:SetNpcId(npcId)
    table.insert(self._sortedEvents, self._events[0])
end

function EventData:GetSortedEvents()
    return self._sortedEvents
end
