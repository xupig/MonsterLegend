--PlayerSettingData
local PlayerSettingData = Class.PlayerSettingData(ClassTypes.XObject)
local SettingType = GameConfig.SettingType
local PlayerSettingStorage = GameConfig.PlayerSettingStorage
local Application = UnityEngine.Application

function PlayerSettingData:__ctor__()
    self._SettingData = {}
end

function PlayerSettingData:SetData(SettingType,data)
    self._SettingData[SettingType] = data
end

function PlayerSettingData:GetData(SettingType)
    return self._SettingData[SettingType]
end

function PlayerSettingData:GetAllData()
    return self._SettingData
end

function PlayerSettingData:LoadData()
    local data = PlayerSettingStorage:LoadData()

    if data == nil then
        self._SettingData[SettingType.CIRCLE_TASK_FAST_COMPLETE] = 0
        self._SettingData[SettingType.WEEK_TASK_FAST_COMPLETE] = 0
        self._SettingData[SettingType.WEEK_COMMIT_EQUIP_FAST_COMPLETE] = 0
        self._SettingData[SettingType.CARD_LUCK_NEED] = 0
        self._SettingData[SettingType.HAPPY_WISH_NEED] = 0
        self._SettingData[SettingType.IDENTIFY_NEED] = 0
    else
        self._SettingData = self:InitData(data)
    end
    self:SaveData()
end

function PlayerSettingData:SaveData()
    PlayerSettingStorage:SaveData(self._SettingData)
end

function PlayerSettingData:InitData(data)
    local settingData = {}
    settingData[SettingType.CIRCLE_TASK_FAST_COMPLETE] = data[tostring(SettingType.CIRCLE_TASK_FAST_COMPLETE)] or 0
    settingData[SettingType.WEEK_TASK_FAST_COMPLETE] = data[tostring(SettingType.WEEK_TASK_FAST_COMPLETE)] or 0
    settingData[SettingType.WEEK_COMMIT_EQUIP_FAST_COMPLETE] = data[tostring(SettingType.WEEK_COMMIT_EQUIP_FAST_COMPLETE)] or 0
    settingData[SettingType.CARD_LUCK_NEED] = data[tostring(SettingType.CARD_LUCK_NEED)] or 0
    settingData[SettingType.HAPPY_WISH_NEED] = data[tostring(SettingType.HAPPY_WISH_NEED)] or 0
    settingData[SettingType.IDENTIFY_NEED] = data[tostring(SettingType.IDENTIFY_NEED)] or 0

    return settingData
end

