local RouletteData = Class.RouletteData(ClassTypes.XObject)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ImportantYunpanDataHelper = GameDataHelper.ImportantYunpanDataHelper
local NormalYunpanDataHelper = GameDataHelper.NormalYunpanDataHelper
local VipYunpanDataHelper = GameDataHelper.VipYunpanDataHelper
local public_config = GameWorld.public_config
local RoulettePreItemData = Class.RoulettePreItemData(ClassTypes.XObject)
local DateTimeUtil = GameUtil.DateTimeUtil

function RoulettePreItemData:__ctor__(name,time,itemids)
   self._name = name
   self._time = time
   self._itemids = itemids
end

function RoulettePreItemData:GetRewardTime()
    return DateTimeUtil.GetDateStr(self._time)
end

function RoulettePreItemData:GetRewards()
    return self._itemids
end
function RouletteData:__ctor__()
   
end

-- 云购记录
function RouletteData:GetRouleRecordData()
    local items = {}
    if self._rouleReward==nil then
        return items
    end
    local d = self._rouleReward  
    local x=#d
    for _,v in ipairs(d) do
        local name = v[public_config.ROULETTE_LUCK_AVATAR_NAME]
        items[x]=name
        x=x-1
    end
    return items
end
function RouletteData:UpdateRouleReward(data)
    self._rouleReward = data
    EventDispatcher:TriggerEvent(GameEvents.RouleRecordUpdate)
end
-- 奖励
function RouletteData:UpdateRecordReward(data)
    self._recordReward = data
    EventDispatcher:TriggerEvent(GameEvents.RecordRewardUpdate)
end
function RouletteData:GetRecordReward()
    return self._recordReward
end
-- 上期奖励
function RouletteData:UpdatePreRouleReward(data)
    self._roulePreReward = data
end
-- 取得上期记录
function RouletteData:GetPreRouleRewardData()
    local items = {}
    if self._roulePreReward==nil or table.isEmpty(self._roulePreReward) then
        return items
    end
    local d = self._roulePreReward
    for _,v in pairs(d) do
        local name = v[public_config.ROULETTE_LUCK_AVATAR_NAME]
        local time = v[public_config.ROULETTE_TIME]
        local itemid = v[public_config.ROULETTE_REWARD]
        table.insert(items,RoulettePreItemData(name,time,itemid))
    end
    table.sort(items,function(a,b)
        if a._time>b._time then
            return true
        else
            return false
        end
    end)
    return items
end

-- 转盘的奖励显示
function RouletteData:GetRoleRewards()
    local d = DateTimeUtil.GetServerOpenDays()
    local ids = NormalYunpanDataHelper:GetAllId()
    local rewards= {}
    local i=1
    for _,id in ipairs(ids) do
        if d == NormalYunpanDataHelper:GetDay(id) then
            local x = NormalYunpanDataHelper:GetReward(id)
            for id,v in pairs(x) do
                rewards[i]={tonumber(id),v[1]}
                i = i + 1
            end
        end
    end
    local d = self:GetRoleBigRewards()
    if d then
        rewards[9]=d
    end
    return rewards
end

-- 大奖
function RouletteData:GetRoleBigRewards()
    local d = DateTimeUtil.GetServerOpenDays()
    local ids = ImportantYunpanDataHelper:GetAllId()
    for _,id in ipairs(ids) do
        local x = ImportantYunpanDataHelper:GetReward(id)
        if d == ImportantYunpanDataHelper:GetDay(id) then
            local x = ImportantYunpanDataHelper:GetReward(id)
            local vocation = math.floor(GameWorld.Player().vocation/100)
            for id,v in pairs(x) do
                if tonumber(id)==vocation then
                    return v
                end
            end
        end
    end
end
-- 消耗
function RouletteData:GetRoleCost()
    local d = DateTimeUtil.GetServerOpenDays()
    local ids = NormalYunpanDataHelper:GetAllId()
    local cost= 0
    local i=1
    for _,id in ipairs(ids) do
        if d == NormalYunpanDataHelper:GetDay(id) then
            return NormalYunpanDataHelper:GetCost(id)
        end
    end
    return cost
end
-- 不限购时间
function RouletteData:GetLimitTime()
    return VipYunpanDataHelper:GetTime(1)
end

--累计次数
function RouletteData:GetTotalCounts()
    return ImportantYunpanDataHelper:GetNum(1)
end

--结束时间
function RouletteData:GetTimeEnd()
    return VipYunpanDataHelper:GetEndTime(1)
end