--local AbyssDataHelper = GameDataHelper.AbyssDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local AbyssData = Class.AbyssData(ClassTypes.XObject)


function AbyssData:__ctor__()
    self.abyssKeywordData = {}
    self.abyssSpaceInfoScore = 0
    self.preAbyssSpaceInfoState = 0
end

function AbyssData:RefreshAbyssInfo(changeValue)
    -- LoggerHelper.Log("RefreshAbyssInfo")
    -- LoggerHelper.Log(GameWorld.Player().abyss_info)
    local abyssMaxUnlockLayer = GameWorld.Player().abyss_info[public_config.ABYSS_INFO_UNLOCK_LAYER] or 0
    local abyssMaxPassLayer = GameWorld.Player().abyss_info[public_config.ABYSS_INFO_PASS_LAYER] or 0
    local abyssMaxPassInfo = GameWorld.Player().abyss_info[public_config.ABYSS_INFO_PASS_INFO] or {}

    self.abyssMaxUnlockLayer = abyssMaxUnlockLayer
    self.abyssMaxPassLayer = abyssMaxPassLayer
    self.abyssMaxPassInfo = abyssMaxPassInfo

    if changeValue == nil then
        return
    end
    for k,v in pairs(changeValue) do       
        -- LoggerHelper.Log("change "..k)
        --删除数据
        if k == table.REMOVE_FLAG then
            --v是下标
        else
            if k == public_config.ABYSS_INFO_UNLOCK_LAYER then
                
            end
        end
    end
end

function AbyssData:RefreshAbyssKeywordData(data)
    self.abyssKeywordData = data or {} --每周词缀
end

function AbyssData:RefreshAbyssSpaceInfoEnterData(data)
    self.abyssSpaceInfoEnterData = data or {} --
    self.abyssSpaceLayer = data[public_config.ABYSS_ROOM_KEY_LAYER] or 1
    local abyssSpaceKeywords = data[public_config.ABYSS_ROOM_KEY_INST_BUFFS] or {}
    self.abyssSpaceKeywords = {}
    for k,v in pairs(abyssSpaceKeywords) do
        table.insert(self.abyssSpaceKeywords, k)
    end
end

function AbyssData:RefreshAbyssSpaceInfoData(data)
    self.abyssSpaceInfoState = data[public_config.ABYSS_ROOM_KEY_STAGE] or public_config.ABYSS_STAGE_COUNTDOWN
    self.abyssSpaceStartTime = data[public_config.ABYSS_ROOM_KEY_STAGE_START_TIME] or 0
    if self.preAbyssSpaceInfoState ~= self.abyssSpaceInfoState then
        if self.abyssSpaceInfoState == public_config.ABYSS_STAGE_PLAY then
            --第一次变为开启状态，启动倒计时
            EventDispatcher:TriggerEvent(GameEvents.Abyss_Space_Start_Timer)
        end
    end
    self.preAbyssSpaceInfoState = self.abyssSpaceInfoState
end

function AbyssData:RefreshAbyssSpaceInfoScoreData(data)
    self.abyssSpaceInfoScore = data[public_config.ABYSS_ROOM_KEY_SCORE] or 0
end