local public_config = GameWorld.public_config

local FriendChatListData = Class.FriendChatListData(ClassTypes.XObject)
local FriendData = Class.FriendData(ClassTypes.XObject)

local SocietyListStorage = GameConfig.SocietyListStorage
local table = table
local SocietyListUtil = GameUtil.SocietyListUtil
local DateTimeUtil = GameUtil.DateTimeUtil
--local CharacterDataHelper = GameDataHelper.CharacterDataHelper

local TYPE_FRIEND = public_config.FRIEND_LIST_TYPE_FRIEND
local TYPE_RECENT = public_config.FRIEND_LIST_TYPE_RECENT
local TYPE_TEAM = public_config.FRIEND_LIST_TYPE_TEAM
local TYPE_RECOMMEND = public_config.FRIEND_LIST_TYPE_RECOMMEND
local TYPE_APPLY = public_config.FRIEND_LIST_TYPE_APPLY

local DBID = public_config.FRIEND_INFO_KEY_DBID



--------------------------FriendChatListData---------------------------------------------------------------------------

function FriendChatListData:__ctor__()
    --公共字典 (self._commonDictData[dbid] = data)
    self._commonDictData = {}

    --各种列表的字典 (二维 self._societyDictData[listType][dbid] = 1)
    self._societyDictData = {}
    self._societyDictData[TYPE_FRIEND] = {}
    self._societyDictData[TYPE_APPLY] = {}
    self._societyDictData[TYPE_RECOMMEND] = {}
    self._societyDictData[TYPE_RECENT] = {}
    self._societyDictData[TYPE_TEAM] = {}

    --各种列表的顺序表 (二维 self._FriendChatListData[listType][1] = data)
    self._FriendChatListData = {}

    --记录各个列表的数量
    self._listNum = {}
    self._listNum[TYPE_FRIEND] = 0
    self._listNum[TYPE_RECENT] = 0
    self._listNum[TYPE_TEAM] = 0
end

--读取本地数据
function FriendChatListData:LoadRecentList()
    -- local recentList = SocietyListStorage:LoadRecentList()
    
    -- --把list还原成和服务器格式一样
    -- local oriList = self:OriginalData(recentList)

    -- --[公共字典]
    -- self:AddCommonDictByList(oriList)

    -- --[公共字典]修改多条数据(取最近互动时间写入到公共字典里)
    -- self:SetContactTimeByList(recentList)

    -- self:AddSocietyDictByList(TYPE_RECENT,oriList)
end

--读取本地数据
function FriendChatListData:LoadTeamList()
    -- local teamList = SocietyListStorage:LoadTeamList()
    
    -- --把list还原成和服务器格式一样
    -- local oriList = self:OriginalData(teamList)

    -- self:AddCommonDictByList(oriList)

    -- --[公共字典]修改多条数据(取最近互动时间写入到公共字典里))
    -- self:SetContactTimeByList(teamList)

    -- --[公共字典]
    -- self:AddSocietyDictByList(TYPE_TEAM,oriList)
end

--初始化组队数据
function FriendChatListData:InitTeamList(listData)
    -- --如果数据没变化,则不重新存储数据
    -- if self:IsTeamListChange(listData) == false  then
    --     GameWorld.LoggerHelper.Log("本地已有此队伍的组队信息,不再存储")
    --     return
    -- end

    -- --把list还原成和服务器格式一样
    -- local oriList = self:OriginalTeamData(listData)

    -- self:AddCommonDictByList(oriList)
    -- -- self:AddSocietyDictByList(TYPE_TEAM,oriList)

    -- -- --保存数据到本地
    -- -- local teamList = self:GetSocietyListByType(TYPE_TEAM)
    -- -- if teamList ~= nil then
    -- --     SocietyListStorage:SaveTeamList(teamList)
    -- -- end
end

--初始化陌生人数据[如果这个是本地数据有的,直接传本地数据就行]
function FriendChatListData:InitStrangerData(data)

    -- --把陌生人数据还原成和服务器格式一样
    -- local oriData = self:OriginalStrangerData(data)

    -- self:AddCommonDictByOne(oriData)
    -- self:AddSocietyDictByOne(TYPE_RECENT,oriData)
end

---------------------------Add-------------------------------------------
--公共字典
function FriendChatListData:AddCommonDictByList(listData)
    -- if listData == nil then
    --     return
    -- end
    -- --LoggerHelper.Error("FriendChatListData:AddCommonDictByList")
    -- LoggerHelper.Log(listData)
    -- for k,data in pairs(listData) do
    --     local dbid = data[DBID]

    --     --只插入公共字典里面没有的
    --     if self._commonDictData[dbid] == nil then
    --         self._commonDictData[dbid] = FriendData(data)
    --     end
    -- end
end

function FriendChatListData:AddCommonDictByOne(data)
    -- if data ~= nil then
    --     local dbid = data[DBID]
    --     self._commonDictData[dbid] = FriendData(data)
    -- end
end

--各种字典(只存dbid)
function FriendChatListData:AddSocietyDictByList(listType,listData)
    
    -- if listData == nil or table.isEmpty(listData) then
    --     return
    -- end

    -- local num = 0
    -- local dbid = nil
    -- for k,data in pairs(listData) do
    --     dbid = data[DBID]
    --     if self._societyDictData[listType][dbid] == nil then
    --         self._societyDictData[listType][dbid] = 1
    --         num = num +1
    --     end
    -- end

    -- GameWorld.LoggerHelper.Log('给【列表'..listType.."】增加【"..num.."】条新数据")
    -- self:SetListNum(listType,num)

    -- --【重组列表】
    -- self:RebuildSocietyList(listType)

    -- --【排序】
    -- if listType == TYPE_RECENT or listType == TYPE_TEAM then
    --     SocietyListUtil:RecentListSort(self._FriendChatListData[listType])
    -- end
end

--[推荐字典专用]需要筛选掉好友列表里的人
function FriendChatListData:AddRecommendDictByList(listData)
    -- if listData == nil then
    --     return
    -- end

    -- local dbid = nil
    -- for k,data in pairs(listData) do
    --     dbid = data[DBID]
    --     if self._societyDictData[TYPE_FRIEND][dbid] == nil then
    --         self._societyDictData[TYPE_RECOMMEND][dbid] = 1
    --     end
    -- end

    -- --【重组列表】
    -- self:RebuildSocietyList(TYPE_RECOMMEND)
end

function FriendChatListData:AddSocietyDictByOne(listType,data)
    -- local dbid = data[DBID]
    -- self:AddSocietyDictByDBID(listType,dbid)
end

function FriendChatListData:AddSocietyDictByDBID(listType,dbid)
    -- if self._societyDictData[listType][dbid] ~= nil then

    --     --最近联系人列表置顶排序[轻量级]
    --     if listType == TYPE_RECENT then 
    --         self:SortTop(TYPE_RECENT,dbid)
    --     end
    --     return
    -- end

    -- self._societyDictData[listType][dbid] = 1
    -- self:SetListNum(listType,1)

    -- if listType == TYPE_FRIEND then
    --     --【重组列表】
    --     self:RebuildSocietyList(listType)
    --      --排序
    --     SocietyListUtil:FrinedListSort(self._FriendChatListData[TYPE_FRIEND])

    -- elseif listType == TYPE_RECENT  then
    --     self:LimitListNum(TYPE_RECENT)
    --     --【重组+排序】最近联系人列表
    --     self:RebuildSocietyListByUUID(TYPE_RECENT,dbid)

    -- elseif listType == TYPE_TEAM  then
    --     self:LimitListNum(TYPE_TEAM)
    --     --【重组+排序】最近组队列表
    --     self:RebuildSocietyListByUUID(TYPE_TEAM,dbid)

    -- else
    --     --【重组列表】
    --     self:RebuildSocietyList(listType)
    -- end
end

---------------------------Set----------------------------------------------
--设置未读消息数
function FriendChatListData:SetCommonDictCntByDBID(dbid,newsNum)
    -- local data = self._commonDictData[dbid]
    -- if data ~= nil then
    --     data:SetNewChatCnt(newsNum)
    -- end
end

--设置最近互动时间
function FriendChatListData:SetCommonDictCTimeByDBID(dbid)
    -- local data = self._commonDictData[dbid]
    -- if data ~= nil then
    --     local time = DateTimeUtil:GetServerTime()
    --     data:SetContactTime(time)
    -- end
end

--设置最近互动时间
function FriendChatListData:SetContactTimeByList(list)
    -- local data = nil
    -- for k,data in pairs(list) do
    --     local dbid = data["_dbid"]
    --     local contactTime = data["_contactTime"]
    --     self._commonDictData[dbid]:SetContactTime(contactTime)
    -- end
end

--把某个列表的全部设置成在线
function FriendChatListData:SetOfflineTimeOnLine(listType)
    --self:SetOfflineTimeByList(self._societyDictData[listType])
end

--设置下线时间
function FriendChatListData:SetOfflineTimeByList(onLineList)
    -- local data = nil
    -- for dbid,time in pairs(onLineList) do
    --     data = self._commonDictData[tonumber(dbid)]
    --     LoggerHelper.Error(data)
    --     data:SetOfflineTime(time)
    -- end
end

--设置搜索结果
function FriendChatListData:SetSearchResult(data)
    -- if data == nil then
    --     self._searchResultData = nil
    -- else
    --     self._searchResultData = FriendData(data)
    -- end
end
---------------------------Del----------------------------------------------

function FriendChatListData:DelSocietyDictByOne(listType,data)
    -- if data == nil then return end
    -- local dbid = data[DBID]
    -- self:DelSocietyDictByDBID(listType,dbid)
end


function FriendChatListData:DelSocietyDictByDBID(listType,dbid)
    -- self._societyDictData[listType][dbid] = nil
    -- self:SetListNum(listType,-1)
    -- --【重组列表】
    -- self:RebuildSocietyList(listType)
end

---------------------------Get------------------------------------------------

--通过字典重组
function FriendChatListData:RebuildSocietyList(listType)
    -- GameWorld.LoggerHelper.Log("重组列表"..listType.." [耗性能]")

    -- local list = self._societyDictData[listType]

    -- --如果为空返回nil
    -- if table.isEmpty(list) then
    --     self._FriendChatListData[listType] = nil
    --     return
    -- end
    
    -- local listData = {}
    -- for dbid,v in pairs(list) do
    --     local data = self._commonDictData[dbid]
    --     if data ~= nil then
    --         table.insert(listData,data)
    --     end
    -- end

    -- self._FriendChatListData[listType] = listData
end

--直接插到第一个重组
function FriendChatListData:RebuildSocietyListByUUID(listType,dbid)
    -- local data = self._commonDictData[dbid]

    -- if self._FriendChatListData[listType] == nil then
    --     self._FriendChatListData[listType] ={}
    -- end

    -- table.insert(self._FriendChatListData[listType], 1, data)

    -- GameWorld.LoggerHelper.Log("重组列表["..listType.."]直接插入到第一位")
end

function FriendChatListData:GetSocietyListByType(listType)
    return self._FriendChatListData[listType]
end

--取好友列表里在线的
function FriendChatListData:GetOnLineList()
    local onLineList = {}

    local friendList = self._societyDictData[TYPE_FRIEND]
    for dbid,v in pairs(friendList) do
        local data = self._commonDictData[dbid]

        if data:GetOfflineTime() == 1 then
            table.insert(onLineList,data)
        end
    end 

    return onLineList 
end

--获取搜索结果
function FriendChatListData:GetSearchResult()
    return self._searchResultData
end

-------------------------工具类--------------------------------------------------

--打包数据
function FriendChatListData:PackSocietyList(serverData)
    local list = {}

    if serverData == nil or table.isEmpty(serverData) then
        return list
    end

    for k,v in pairs(serverData) do
        table.insert(list, FriendData(v))
    end
    return list
end


--查看某个dbid是否为自己好友(如果是返回 true,不是返回 false)
function FriendChatListData:IsMyFriend(dbid)
    if self._societyDictData[TYPE_FRIEND][dbid] == nil then
        return false
    else
        return true
    end
end

--查看某个dbid是否为自己的最近联系人
function FriendChatListData:IsMyRecent(dbid)
    if self._societyDictData[TYPE_RECENT][dbid] == nil then
        return false
    else
        return true
    end
end

--查看某个dbid是否存在自己的公共字典里
function FriendChatListData:InMyCommon(dbid)
    if self._commonDictData[dbid] == nil then
        return false
    else
        return true
    end
end

--判读某列表是否为空(为空的时候返回true,反之返回false)
function FriendChatListData:IsDictNill(listType)
    local dict = self._societyDictData[listType]
    if dict == nil or table.isEmpty(dict) then
        return true
    else
        return false 
    end
end

--存最近联系人和最近组队列表数据到本地(存整个list完整的数据)
function FriendChatListData:SaveRecentList()
    local recentList = self:GetSocietyListByType(TYPE_RECENT)
    if recentList ~= nil then
        SocietyListStorage:SaveRecentList(recentList)
    end
end

--获取各个列表的数量
function FriendChatListData:GetListNum()
    return self._listNum
end

--设置各个列表的数量
function FriendChatListData:SetListNum(listType,num)
    if listType == TYPE_APPLY or listType == TYPE_RECOMMEND then
        return
    end

    self._listNum[listType] =self._listNum[listType] + num

    --[违规做法]发事件,通知UI刷新界面各个列表数量
    EventDispatcher:TriggerEvent(GameEvents.OnListNumChanged)
end

--判断组队列表的数据是否变化(变化返回true,否则返回false)
function FriendChatListData:IsTeamListChange(teamList)
    for k,data in pairs(teamList) do
        local dbid = data["_dbid"]

        --如果为空,说明组队数据里还没有
        if self._societyDictData[TYPE_TEAM][dbid] == nil then
            return true
        end
    end

    return false
end

--获取dbid在列表里的index
function FriendChatListData:GetListIndexByUUID(listType,dbid)
    if dbid == nil then
        return nil
    end

    if self._FriendChatListData[listType] == nil then
        return nil
    end

    for k,data in ipairs(self._FriendChatListData[listType]) do
        if data:GetUUID() == dbid then
            return k
        end
    end

    return nil
end

--获取dbid字符串列表用于刷新在线数据
function FriendChatListData:GetDBIDString(listType)
    local list = self._societyDictData[listType]

    local dbids = ''

    for dbid,v in pairs(list) do
        dbids = dbids..dbid..'|'
    end

    --截取从1开始到倒数第2位
    dbids = string.sub(dbids,1,-2)

    local type = public_config.FRIEND_LIST_TYPE_SOMEONE_ONLINE
    local str = string.format("%d,%s",type,dbids)

    return str
end

--置顶排序
function FriendChatListData:SortTop(listType,dbid)
    local index = self:GetListIndexByUUID(listType,dbid)
    local data = self._FriendChatListData[listType][index]
    table.remove(self._FriendChatListData[listType],index)
    table.insert(self._FriendChatListData[listType],1,data)
end

--好友列表排序
function FriendChatListData:FrinedListSort()
    SocietyListUtil:FrinedListSort(self._FriendChatListData[TYPE_FRIEND])
end

--列表做数量限制
function FriendChatListData:LimitListNum(listType)
    if self._FriendChatListData[listType] == nil then
        return
    end

    -- 最近组队列表做数量限制
    local limitNum = 20
    if #self._FriendChatListData[listType] == limitNum then
        local dbid = self._FriendChatListData[listType][limitNum]:GetUUID()
        self._societyDictData[listType][dbid] = nil
        self._FriendChatListData[listType][limitNum] = nil
        self:SetListNum(listType,-1)
        GameWorld.LoggerHelper.Log("列表数量达到上限删除最后一个")
    end
end

--把本地存储数据解析成和网络数据格式一样
function FriendChatListData:OriginalData(listdata)
    if table.isEmpty(listdata) then
        return {}
    end

    local oriListData = {}
    for k, data in pairs(listdata) do
        local oriData = {}

        oriData[public_config.FRIEND_INFO_KEY_NAME] = data["_name"]
        oriData[public_config.FRIEND_INFO_KEY_DBID] = data["_dbid"]
        --oriData[public_config.FRIEND_INFO_KEY_CROSS_DBID] = data["_dbid"]
        oriData[public_config.FRIEND_INFO_KEY_LEVEL] = data["_level"]
        --oriData[public_config.FRIEND_INFO_KEY_ICON] = data["_icon"]
        --oriData[public_config.FRIEND_INFO_KEY_GUILD_NAME] = data["_guildName"]
        oriData[public_config.FRIEND_INFO_KEY_OFFLINE_TIME] = data["_offlineTime"]
        oriData[public_config.FRIEND_INFO_KEY_NEW_CHAT_CNT] = data["_newChatCnt"]
        --oriData[public_config.FRIEND_INFO_KEY_GUILD_DBID] = data["_guildDbid"] or 0
        table.insert(oriListData,oriData)
    end

    return oriListData
end

--把和陌生人的数据解析和网络数据格式一样
function FriendChatListData:OriginalStrangerData(data)
    local oriData = {}

     oriData[public_config.FRIEND_INFO_KEY_NAME] = data["name"]
     oriData[public_config.FRIEND_INFO_KEY_DBID] = data["dbid"]
     --oriData[public_config.FRIEND_INFO_KEY_CROSS_DBID] = data["_dbid"] or 0
     oriData[public_config.FRIEND_INFO_KEY_LEVEL] = data["level"]
     --oriData[public_config.FRIEND_INFO_KEY_ICON] = data["icon"]
     --oriData[public_config.FRIEND_INFO_KEY_GUILD_NAME] = data["guildName"]

    --设置互动时间,为了排序时比发过信息的靠后,所以时间除以2
    local time = DateTimeUtil:GetServerTime()
    oriData["contactTime"] = time*0.5
    --oriData[public_config.FRIEND_INFO_KEY_GUILD_DBID] = data["guild_dbid"] or 0
    return oriData
end

--把组队数据解析和网络数据格式一样
function FriendChatListData:OriginalTeamData(listdata)
   local oriListData = {}
    for k, data in pairs(listdata) do
        local oriData = {}

        oriData[public_config.FRIEND_INFO_KEY_NAME] = data["_memberName"]
        oriData[public_config.FRIEND_INFO_KEY_DBID] = data["_dbid"]
        --oriData[public_config.FRIEND_INFO_KEY_CROSS_DBID] = data["_dbid"]
        oriData[public_config.FRIEND_INFO_KEY_LEVEL] = data["_level"]
        --oriData[public_config.FRIEND_INFO_KEY_ICON] = data["_icon"]
        --oriData[public_config.FRIEND_INFO_KEY_GUILD_NAME] = data["_guildName"] or 0
        --设置互动时间
        oriData["contactTime"] = DateTimeUtil:GetServerTime()
        --oriData[public_config.FRIEND_INFO_KEY_GUILD_DBID] = data["_guildDbid"] or 0
        table.insert(oriListData,oriData)
    end

    return oriListData
end

--------------------------FriendData-----------------------------------------------------------------------------

function FriendData:__ctor__(FriendData)
    -- self._name = FriendData[public_config.FRIEND_INFO_KEY_NAME]
    -- self._dbid = FriendData[public_config.FRIEND_INFO_KEY_DBID]
    -- --self._dbid = FriendData[public_config.FRIEND_INFO_KEY_CROSS_DBID]
    -- self._level = FriendData[public_config.FRIEND_INFO_KEY_LEVEL]
    -- -- self._icon = FriendData[public_config.FRIEND_INFO_KEY_ICON]
    -- --self._icon = 0--CharacterDataHelper.GetIconID(FriendData[public_config.FRIEND_INFO_KEY_ICON])
    -- --self._guildName = FriendData[public_config.FRIEND_INFO_KEY_GUILD_NAME]
    -- self._fightPower = FriendData[public_config.FRIEND_INFO_KEY_FIGHT_FORCE] or 0
    -- self._offlineTime = FriendData[public_config.FRIEND_INFO_KEY_OFFLINE_TIME] or 1
    -- self._newChatCnt = FriendData[public_config.FRIEND_INFO_KEY_NEW_CHAT_CNT] or 0

    -- self._contactTime = FriendData["contactTime"] or 0
    -- --self._guildDbid= FriendData[public_config.FRIEND_INFO_KEY_GUILD_DBID] or 0
end

function FriendData:GetName()
   -- return self._name
end

--现阶段永恒的uuid一律使用dbid
function FriendData:GetDBID()
   -- return self._dbid
end

function FriendData:GetCrossDBID()
   -- return self._dbid
end

function FriendData:GetGuildDBID()
   -- return self._guildDbid
end

function FriendData:GetLevel()
   -- return self._level
end

function FriendData:GetIcon()
   -- return self._icon
end

function FriendData:GetGuildName()
   -- return self._guildName
end

function FriendData:GetFightPower()
   -- return self._fightPower
end

--在线时为0,下线时为下线时间
function FriendData:GetOfflineTime()
  --  return self._offlineTime
end

function FriendData:GetNewChatCnt()
   -- return self._newChatCnt
end

function FriendData:GetContactTime()
    --return self._contactTime
end

------------------------Set-------------------------

function FriendData:SetNewChatCnt(newsNum)
    --self._newChatCnt = newsNum
end

function FriendData:SetOfflineTime(time)
    --self._offlineTime = time
end

function FriendData:SetContactTime(time)
    --self._contactTime = time
end
