--环任务信息    ------------------------WeekTaskData-------------------------
require "PlayerManager/PlayerData/TaskCommonItemData"
local TaskCommonItemData = ClassTypes.TaskCommonItemData

local WeekTaskData = Class.WeekTaskData(ClassTypes.TaskCommonItemData)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskDataHelper = GameDataHelper.TaskDataHelper
local public_config = GameWorld.public_config
local TaskConfig = GameConfig.TaskConfig
local GuildWeekTaskDataHelper = GameDataHelper.GuildWeekTaskDataHelper
local GuildWeekTaskRewardDataHelper = GameDataHelper.GuildWeekTaskRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskRewardData = ClassTypes.TaskRewardData
local FontStyleHelper = GameDataHelper.FontStyleHelper

function WeekTaskData:__ctor__()
    self._weekTaskId = nil
    self._taskItemType = TaskConfig.TASK_TYPE_WEEK_TASK
end

function WeekTaskData:IsSameTask(weekTask)
    if weekTask == nil then return false end
    if self:GetId() == weekTask:GetId() and self:GetState() == weekTask:GetState() and 
    self:CompareTargets(weekTask:GetTargetsCurTimes()) then
        return true
    end
    return false
end

function WeekTaskData:UpdateData(weekTaskId, serverDatas)
    --{"5" = {"1" = {"20004" = 0},"2" = 1}}
    self._serverData = serverDatas
    self._weekTaskId = weekTaskId

    if self:GetState() == public_config.TASK_STATE_COMPLETED and self._weekTaskId then
        if self:GetCommitNPC() == nil or self:GetCommitNPC() == 0 then
            EventDispatcher:TriggerEvent(GameEvents.COMMIT_WEEK_TASK, self:GetId())
        else

        end
    end
end

function WeekTaskData:GetId()
    return self._weekTaskId
end

function WeekTaskData:GetState()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_STATE] or nil
end

function WeekTaskData:CompareTargets(targets)
    local myTargets = self:GetTargetsCurTimes()
    if table.isEmpty(myTargets) then
        if table.isEmpty(targets) then
            return true
        else
            return false
        end
    else
        if table.isEmpty(targets) then
            return false
        else
            for k,v in pairs(myTargets) do
                if v ~= targets[k] then
                    return false
                end
            end
            return true
        end
    end
end

function WeekTaskData:GetTargetsCurTimes()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_TARGETS] or {}
end

function WeekTaskData:GetName()
    return GuildWeekTaskDataHelper:GetName(self._weekTaskId)
end

function WeekTaskData:GetTaskDesc()
    if self._weekTaskId == nil then return "" end
    return LanguageDataHelper.CreateContent(GuildWeekTaskDataHelper:GetDesc(self._weekTaskId))
end

function WeekTaskData:GetTitleNameAndColor()
    local color = FontStyleHelper:GetFontColor(27)
    if self._weekTaskId == nil then
        return LanguageDataHelper.CreateContent(301) .. LanguageDataHelper.CreateContent(926), color
    end
    return LanguageDataHelper.CreateContent(301) ..LanguageDataHelper.CreateContent(self:GetName()), color
end

--TODO 
function WeekTaskData:GetSelfLimit()
    return 0
end

function WeekTaskData:GetTargetsDesc()
    return GuildWeekTaskDataHelper:GetTargetsDesc(self._weekTaskId)
end

function WeekTaskData:GetCommitNPC()
    -- LoggerHelper.Error("NPC ID ==============" .. tostring(GuildWeekTaskDataHelper:GetCommitNpc(self._weekTaskId)))
    -- LoggerHelper.Error("self._weekTaskId ==============" .. tostring(self._weekTaskId))
    return GuildWeekTaskDataHelper:GetCommitNpc(self._weekTaskId)
end

function WeekTaskData:GetCommitDialog()
    return GuildWeekTaskDataHelper:GetCommitDialog(self._weekTaskId)
end

function WeekTaskData:NewWeekTaskData(id, state)
    local serverData = {}
    serverData[public_config.TASK_INFO_KEY_STATE] = state
    local cData = WeekTaskData()
    cData:UpdateData(id, serverData)
    return cData
end

function WeekTaskData:GetAcceptNPC()
    if self._weekTaskId == nil then
        local data = GlobalParamsHelper.GetParamValue(654)
        for k,v in pairs(data) do
            return k
        end
    end
end

function WeekTaskData:GetTargetsInfoData()
    return GuildWeekTaskDataHelper:GetTargets(self._weekTaskId) 
end

function WeekTaskData:GetAcceptDialog()
    if self._weekTaskId == nil then
        local data = GlobalParamsHelper.GetParamValue(654)
        local datas = {}
        for k,v in pairs(data) do
            table.insert(datas, {
                [1] =   1,
                [2] =   {v,0,1},
            })
            return datas
        end
    end
end

function WeekTaskData:IsAutoTask()
    return true--self._weekTaskId ~= nil
end

function WeekTaskData:GetMyRewards()
    local myReward = {}
    if self._weekTaskId == nil then
        return myReward
    else        
        local rewardId = GuildWeekTaskRewardDataHelper:GetRewardIdByLevel()
        if rewardId ~= nil then
            local rewards = GuildWeekTaskRewardDataHelper:GetReward(rewardId)
            for i, v in ipairs(rewards) do
                local itemId = v[1]
                local num = v[2]
                if itemId == public_config.MONEY_TYPE_EXP then
                    local rate = GuildWeekTaskDataHelper:GetRewardRate(self._weekTaskId)
                    num = math.floor((rate/10000) * num)
                end
                table.insert(myReward, TaskRewardData(itemId, num))
            end
        end
    end
    return myReward
end

function WeekTaskData:GetMoneyReward()
    local rewardId = GuildWeekTaskRewardDataHelper:GetRewardIdByLevel()
    if rewardId ~= nil then
        return GuildWeekTaskRewardDataHelper:GetMoneyReward(rewardId)
    end
    return 1
end

function WeekTaskData:GetAcceptTaskCallBack()
    return function(id) EventDispatcher:TriggerEvent(GameEvents.ACCEPT_WEEK_TASK, id) end
end

function WeekTaskData:GetCommitTaskCallBack()
    return function(id) EventDispatcher:TriggerEvent(GameEvents.COMMIT_WEEK_TASK, id) end
end

function WeekTaskData:GetLittleShoesOpen()
    local state = self:GetState()
    if state == public_config.TASK_STATE_ACCEPTABLE then
        return GuildWeekTaskDataHelper:GetLittleShoesOpenStart(self._weekTaskId) == 1
    elseif state == public_config .TASK_STATE_ACCEPTED then
        return GuildWeekTaskDataHelper:GetLittleShoesOpen(self._weekTaskId) == 1
    elseif state == public_config.TASK_STATE_COMPLETED then
        return GuildWeekTaskDataHelper:GetLittleShoesOpenEnd(self._weekTaskId) == 1
    end
end
