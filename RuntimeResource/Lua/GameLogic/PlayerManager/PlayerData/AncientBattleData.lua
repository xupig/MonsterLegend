local AncientBattleDataHelper = GameDataHelper.AncientBattleDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local AncientBattleData = Class.AncientBattleData(ClassTypes.XObject)

function AncientBattleData:__ctor__()
    self.actionData = {}
end

function AncientBattleData:RefreshActionData(action_id, data)
    self.actionData[action_id] = data
end

function AncientBattleData:RetSetActionData()
    self.actionData = {}
end

function AncientBattleData:GetActionData(action_id)
    return self.actionData[action_id]
end