local CardData = Class.CardData(ClassTypes.XObject)
local CardDataHelper = GameDataHelper.CardDataHelper
local CardItemData = Class.CardItemData(ClassTypes.XObject)
local CardGroupItemData = Class.CardGroupItemData(ClassTypes.XObject)
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardGroupLevelDataHelper = GameDataHelper.CardGroupLevelDataHelper
local CardGroupDataHelper = GameDataHelper.CardGroupDataHelper
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil
local CardLevelDataHelper = GameDataHelper.CardLevelDataHelper

function CardItemData:__ctor__(id,star)
    self._id = id
    self._star = star or self:CardStar(id)
end

function CardItemData:GetName()
    local nameid = CardDataHelper:GetName(self._id)
    local q = self:GetQuality()
    return CardData:GetNameColor(nameid,q)
end

function CardItemData:GetTypeName()
    local nameid = CardDataHelper:GetTypeName(self._id)
    return nameid
end

function CardItemData:GetLevel()
   return CardDataHelper:GetLevel(self._id) 
end

function CardItemData:IsLock()
    return CardDataHelper:GetLevel(self._id)>GameWorld.Player().level
 end

function CardItemData:CardStar()
    local cardlist = GameWorld.Player().card_list
    for k,v in pairs(cardlist) do
        if self._id == tonumber(k) then
            return v
        end
    end
    return 0
end

function CardItemData:CardIsHave()
    local cardlist = GameWorld.Player().card_list
    for k,v in pairs(cardlist) do
        if self._id == tonumber(k) then
            return true
        end
    end
    return false
end
function CardItemData:GetCardId()
    return self._id
end

function CardItemData:GetIcon()
    return CardDataHelper:GetIcon(self._id)
end

function CardItemData:GetQuality()
    return CardDataHelper:GetQuality(self._id)
end

function CardItemData:GetRank()
    return CardDataHelper:GetRank(self._id)
end

function CardItemData:GetLevelDesc()
    local desid = CardDataHelper:GetLevelDesc(self._id)
    return LanguageDataHelper.GetContent(desid)
end

function CardItemData:GetCardCost()
    return CardLevelDataHelper:GetCardCost(self._id,self._star)
end


function CardItemData:GetType()
    return CardDataHelper:GetType(self._id)
end

function CardGroupItemData:__ctor__(groupid,level)
    self._groupid = groupid
    self._level = level
end

function CardGroupItemData:GetCardId()
    return CardGroupDataHelper:GetCardId(self._groupid)
end

function CardGroupItemData:GetGroupId()
    return self._groupid
end

function CardGroupItemData:GetCardsData()
    local ids = self:GetCardId()
    local cardItems = {}
    for k,v in ipairs(ids) do 
        table.insert(cardItems,CardItemData(v))
    end
    return cardItems
end
function CardGroupItemData:GetName()
    return CardGroupDataHelper:GetName(self._groupid)
end

function CardData:__ctor__()
    
end


-- 构造cardItemdata
function CardData:CreateCardItem(ids)
    local itemdatas = {}
    if ids and not table.isEmpty(ids) then
        for i,v in ipairs(ids) do
            table.insert(itemdatas,CardItemData(v,nil))
        end
    end
    return itemdatas
end

-- 构造cargroupItemdata
function CardData:GetCardGroupItemDatas()
    local groupids = CardGroupDataHelper:GetAllId()
    local itemdatas = {}
    for k,groupid in ipairs(groupids) do
        if groupid ~= 0 then
            local les = self:GetGroupLes(groupid)
            for k,le in ipairs(les) do 
                local f = self:IsActGroup(groupid,le)
                if f then
                    local f = self:IsFullGroup(groupid,le)
                    if f then
                        table.insert(itemdatas, CardGroupItemData(groupid,le))
                    else
                        local cardgroup = GameWorld.Player().card_group
                        local l = cardgroup[groupid]+1
                        table.insert(itemdatas, CardGroupItemData(groupid,l))
                    end
                    break
                else
                    table.insert(itemdatas, CardGroupItemData(groupid,le))
                    break
                end
            end
        end
    end
    return itemdatas
end

function CardData:GetCardItemData(cardid,star)
    return CardItemData(cardid,star)
end

function CardData:IsActGroup(groupid,level)
    return PlayerManager.CardManager:IsActGroup(groupid,level)
end

function CardData:IsFullGroup(groupid,level)
    return PlayerManager.CardManager:IsFullGroup(groupid,level)
end

function CardData:GetGroupLes(groupid)
    local ids = CardGroupLevelDataHelper:GetAllId()
    local les = {}
    for k,v in ipairs(ids) do
        if groupid == CardGroupLevelDataHelper:GetCardGroup(v) then
            table.insert(les,CardGroupLevelDataHelper:GetLevel(v))
        end
    end
    return les
end

function CardData:GetCardTotalLevelByType(type)
    local alllevel={}
    local ids = CardDataHelper:GetAllId()
    for i,id in ipairs(ids) do
        local le = CardDataHelper:GetLevel(id)
        if CardDataHelper:GetType(id)==type and not table.containsValue(alllevel,le) then
            table.insert(alllevel,le)            
        end
    end
    return alllevel
end

function CardData:UpdateGroup(data)
    self._cardgroup=data
end

function CardData:GetGroup()
    return self._cardgroup
end

function CardData:GetNameColor(nameid,quality)
    local colorId = ItemConfig.QualityTextMap[quality]
	local name = StringStyleUtil.GetColorStringWithColorId(LanguageDataHelper.GetContent(nameid),colorId)
	return name
end