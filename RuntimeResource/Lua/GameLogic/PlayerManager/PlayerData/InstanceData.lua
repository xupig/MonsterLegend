local InstanceData = Class.InstanceData(ClassTypes.XObject)
local Scen = 1
--副本数据集
function InstanceData:__ctor__()
	self._damageRankList = nil
	self._showDamageSceneType = {}
	self._showDamageSceneType[10] = true
	self._showDamageSceneType[11] = true
	self._showDamageSceneType[12] = true
	self._showDamageSceneType[13] = true

	self.nightmareInstId = 0

	self.expInstanceInfo = nil
end

function InstanceData:SetDamageRankList(luaTable)
    self._damageRankList = luaTable
end

function InstanceData:GetDamageRankList()
    return self._damageRankList
end

function InstanceData:CheckShowDamage()
	local t = GameManager.GameSceneManager:GetCurSceneType()
	if self._showDamageSceneType[t] then
		return true
	end
	return false
end

function InstanceData:UpdateNightmare(instId)
	self.nightmareInstId = instId
end

------------------------经验副本相关---------------------------------
function InstanceData:UpdateExpInstanceInfo(args)
	self.expInstanceInfo = args
end