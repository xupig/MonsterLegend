--CommonItemData.lua

local ItemDataHelper = GameDataHelper.ItemDataHelper
local EquipDataHelper = GameDataHelper.EquipDataHelper
local MythicalAnimalsLevelDataHelper = GameDataHelper.MythicalAnimalsLevelDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local MarryRingDataHelper = GameDataHelper.MarryRingDataHelper
local BaseUtil = GameUtil.BaseUtil
local DateTimeUtil = GameUtil.DateTimeUtil
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil

local CommonItemData = Class.CommonItemData(ClassTypes.XObject)
local public_config = require("ServerConfig/public_config")
local attri_config = require("ServerConfig/attri_config")

function CommonItemData:__ctor__(svrData,bagType,bagPos)
	if svrData then
		self:ResetServerData(svrData)
	end
	if bagType then
		--物品依赖背包类型和背包位置作为唯一标志，需要在初始化时记录该信息
		self.bagType = bagType
		self.bagPos = bagPos
	end
end

function CommonItemData:ResetServerData(svrData)
    self._serverData = svrData
    self.cfg_id = svrData[public_config.ITEM_KEY_ID]
	self.cfgData = ItemDataHelper.GetItem(self.cfg_id)
	--道具数量发生变化
	local curCount = self:GetCount()
	local dCount = 0	--数量变化
	--判断是否初始化过
	if self._lastCount then
		dCount = curCount - self._lastCount
		if dCount > 0 then
			EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_INCREASE,self.cfg_id,self.bagType,self.bagPos,self._lastCount)
			EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_INCREASE_FLOAT, self.bagType, self.bagPos, self._lastCount)
		elseif dCount < 0 then
			EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_DECREASE,self.cfg_id,self.bagType,self.bagPos,self._lastCount)
		end
	end
	self._lastCount = self:GetCount()
	return dCount
end

--只依赖ID设置数据
function CommonItemData:ResetDataById(itemID)
	--这种道具没serverdata
	self._serverData = {}
	self.cfg_id = itemID
	self.cfgData = ItemDataHelper.GetItem(self.cfg_id)
end

function CommonItemData:GetServerData()
	return self._serverData
end

--获取配置数据
function CommonItemData:GetCfgItemData()
    return self.cfgData
end

--获取背包位置
function CommonItemData:GetPos()
	return self.bagPos
end

--获取ID
function CommonItemData:GetItemID()
	return self.cfg_id
end

--获取icon
function CommonItemData:GetIcon()
	return self.cfgData.icon
end

--获取道具名字
function CommonItemData:GetNameText()
	return LanguageDataHelper.CreateContent(self.cfgData.name)
end

--获取道具类型
function CommonItemData:GetItemType()
	return self.cfgData.itemType
end

--获取道具类型
function CommonItemData:GetType()
	return self.cfgData.type
end

--获取中文名字
function CommonItemData:GetItemName()
	return ItemDataHelper.GetItemName(self.cfg_id)
end

--获取描述中文
function CommonItemData:GetDesc()
	return ItemDataHelper.GetDescription(self.cfg_id)
end

--获取品质
function CommonItemData:GetQuality()
	return self.cfgData.quality or 0
end

--使用等级
function CommonItemData:GetLevelNeed()
	return self.cfgData.levelNeed or 1
end

--获取装备阶数
function CommonItemData:GetGrade()
	return self.cfgData.grade
end

--交易行类别
function CommonItemData:GetAuctionType()
	return self.cfgData.auctionType
end

--交易行子类别
function CommonItemData:GetAuctionSubType()
	return self.cfgData.auctionSubtype
end

--获取数量
function CommonItemData:GetCount()
	return self._serverData[public_config.ITEM_KEY_COUNT] or 1
end

--获取绑定状态,可能为nil
function CommonItemData:BindFlag()
	return self._serverData[public_config.ITEM_KEY_BIND_FLAG]
end

--严格职业匹配(当前能使用)
function CommonItemData:CheckVocationMatch()
	return ItemDataHelper.CheckVocationMatch(self.cfg_id,GameWorld.Player().vocation,false)
end

--广义职业匹配(包含现在不能使用转职后能使用)
function CommonItemData:CheckVocationMatchGeneral()
	return ItemDataHelper.CheckVocationMatch(self.cfg_id,GameWorld.Player().vocation,true)
end

--等级匹配
function CommonItemData:CheckLevelMatch()
	return ItemDataHelper.CheckLevelMatch(self.cfg_id,GameWorld.Player().level)
end

--检查是否能装备(装备/魂器)
function CommonItemData:CheckCanLoaded()
	return self:CheckLevelMatch() and self:CheckVocationMatch() 
end

--从一个serverdata索引的范围里查找所有属性值
function CommonItemData:GetAttris(minIdex,maxIndex,byKey)
	local result = {}
	local j = 1
	for i= minIdex,maxIndex,2 do
		local k = self._serverData[i]
		local v = self._serverData[i+1]
		if k and v then
			local plid = {["attri"] = k, ["value"] = v}
			if byKey then
				result[k] = plid
			else
				result[j] = plid
				j = j+1
			end
		end
	end
	return result
end

--获取装备随机属性组
function CommonItemData:GetRandAttris()
	return self:GetAttris(public_config.ITEM_KEY_EQUIP_RAND_ATTRI_MIN,public_config.ITEM_KEY_EQUIP_RAND_ATTRI_MAX)
end

--获取装备随机属性组(用于展示须排序)
local RandAttrisShowOrder = {4,5,1,2,3}
function CommonItemData:GetRandAttrisShow()
	local src = self:GetRandAttris()
	local result = {}
	for i=1,5 do
		local attri = src[RandAttrisShowOrder[i]]
		if attri then
			table.insert(result,attri)
		end
	end
	return result
end

-------------------------------------普通道具-----------------------------------------------
local PkgCommonItemData = Class.PkgCommonItemData(ClassTypes.CommonItemData)


-------------------------------------装备-----------------------------------------------
local PkgEquipItemData = Class.PkgEquipItemData(ClassTypes.CommonItemData)

--装备随机数据（随机属性/洗炼数据）
local EquipRandAttriData = Class.EquipRandAttriData(ClassTypes.XObject)
function EquipRandAttriData:__ctor__(attri, value)
	self.attri = attri  			--属性id
	self.minvalue = value[1]  		--最小属性值
	self.maxvalue = value[2]		--最大属性值
end

--override数据写入
function PkgEquipItemData:ResetServerData(svrData)
	self._base.ResetServerData(self,svrData)
	self._fightPowerChange = true --战斗力改变
	self._baseFightPower = nil
	return 0
end

-- function PkgEquipItemData:IndexAttriMin()
-- 	return self._serverData[public_config.ITEM_KEY_INDEX_ATTRI_MIN]
-- end

--获取装备综合战力
function PkgEquipItemData:GetFightPower(otherPlayerArgs)
	--已装备的装备计算加成
	--婚戒只计算基础战力
	if self.bagType == public_config.PKG_TYPE_LOADED_EQUIP and self:GetType() ~= public_config.PKG_LOADED_EQUIP_INDEX_RING then
		if self._fightPowerChange then
			local additionPower = 0
			local washScore = 0
			local baseFightPower = self:GetBaseFightPower()
			local equipType = self:GetType()
			--1-10的装备才计算
			if equipType < public_config.PKG_LOADED_EQUIP_INDEX_MAX then
				--LoggerHelper.Error("PkgEquipItemData:GetFightPower")
				local allAttris = {}
				local t = {}
				--强化加成
				local strengthInfo
				if otherPlayerArgs then
					strengthInfo = otherPlayerArgs.strengthInfo or {}
				else
					strengthInfo = GameWorld.Player().equip_stren_info
				end
				strengthInfo = strengthInfo[equipType]
				
				if strengthInfo then
					local strengthLevel = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] or 0
					local maxLevel = self.cfgData.strengthHighLv
					strengthLevel = math.min(strengthLevel,maxLevel)
					local strengthCfg = EquipDataHelper.GetStrengthData(strengthLevel)
					local attriInfo = strengthCfg.attris[equipType]
					if attriInfo then
						for i =1,#attriInfo,2 do
							local attriKey = attriInfo[i]
							local attriValue = attriInfo[i+1]
							t[attriKey] = (t[attriKey] or 0) + attriValue
						end
					end
				end
				--洗炼加成
				washScore = self:GetWashScore(otherPlayerArgs)

				--宝石加成
				local gemInfo
				if otherPlayerArgs then
					gemInfo = otherPlayerArgs.gemInfo or {}
				else
					gemInfo = GameWorld.Player().equip_gem_info
				end
				gemInfo = gemInfo[equipType]
				for i=1, public_config.EQUIP_GEM_INFO_GEM_MAX1 do
					if gemInfo[i] then
						local gemCfg = ItemDataHelper.GetItem(gemInfo[i])
						for i=1,2 do
							if gemCfg.gem_attribute[i] then
								local attriKey = gemCfg.gem_attribute[i][1]
								local attriValue = gemCfg.gem_attribute[i][2]
								t[attriKey] = (t[attriKey] or 0) + attriValue
							end
						end
					end
				end
				for k,v in pairs(t) do
					table.insert(allAttris ,{["attri"] = k, ["value"] = v})
				end
				additionPower = BaseUtil.CalculateFightPower(allAttris,GameWorld.Player().vocation)
			end

			self._fightPower = baseFightPower + additionPower + washScore
			self._fightPowerChange = false
		end
		return self._fightPower

	--未装备的装备用基础战力
	else
		return self:GetBaseFightPower()
	end
end

--获取装备基础战力
function PkgEquipItemData:GetBaseFightPower(otherPlayerArgs)
	if self._baseFightPower == nil then
		local allAttris = {}
		local t = {}
		local baseAttri = {}
		if self:GetType() == public_config.PKG_LOADED_EQUIP_INDEX_RING then
			local ringLevel = GameWorld.Player().marry_ring_level
			local ringAttri = MarryRingDataHelper:GetAttriBase(ringLevel)
			t = ringAttri
		else
			baseAttri = EquipDataHelper:GetBaseAttris(self.cfg_id)
			for i=1,#baseAttri do
				local attriKey = baseAttri[i].attri
				local attriValue = baseAttri[i].value
				t[attriKey] = (t[attriKey] or 0) + attriValue
			end
		end
		
		--极品属性用自己的计算方式
		local randAttriScore = self:CalculateRandAttriScore()

		for k,v in pairs(t) do
			table.insert(allAttris ,{["attri"] = k, ["value"] = v})
		end
		self._baseFightPower = BaseUtil.CalculateFightPower(allAttris,GameWorld.Player().vocation) + randAttriScore
	end
	return self._baseFightPower
end

function PkgEquipItemData:GetWashScore(otherPlayerArgs)
	local washScore = 0
	local washInfo
	if otherPlayerArgs then
		washInfo = otherPlayerArgs.washInfo or {}
	else
		washInfo = GameWorld.Player().equip_wash_info or {}
	end
	washInfo = washInfo[self.bagPos]
	if washInfo then
		local itemWashAttrisLen = washInfo[public_config.EQUIP_WASH_INFO_SLOT_COUNT] or 0
		for i=1,itemWashAttrisLen do
			local baseIndex = (i-1)*3
			local attriKey = washInfo[baseIndex+1]
			local attriValue = washInfo[baseIndex+2]
			local washScoreCfg = EquipDataHelper.GetEquipWashScoreData(attriKey)
			--LoggerHelper.Error("washScoreCfg"..attriKey)
			washScore = washScore + math.floor(attriValue/ washScoreCfg.unit*washScoreCfg.score)
		end
	end
	return washScore
end

--计算极品属性评分
function PkgEquipItemData:CalculateRandAttriScore()
	if self._posFactorCfg == nil then
		--部位系数
		self._posFactorCfg = GlobalParamsHelper.GetParamValue(500)
		--阶数系数
		self._gradeFactorCfg = GlobalParamsHelper.GetParamValue(501)
	end
	local randAttri = self:GetRandAttris()
	local score = 0
	for i=1,#randAttri do
		local attriKey = randAttri[i].attri
		--local attriValue = randAttri[i].value

		local scoreCfg = EquipDataHelper.GetEquipScoreData(attriKey,self:GetQuality())
		if scoreCfg == nil then
			LoggerHelper.Error("出现极品属性没有配颜色:"..attriKey.."/"..self:GetQuality())
		end
		local posFactor = self._posFactorCfg[self:GetType()]
		local gradeFactor = self._gradeFactorCfg[self.cfgData.grade]
		local attriScore = math.floor(scoreCfg.score*posFactor*gradeFactor)
		score = score + attriScore
	end
	return score
end

function PkgEquipItemData:SetFightPowerChange(value)
	self._fightPowerChange = value
end

--获取装备所有随机属性组的随机范围(废弃)
-- function PkgEquipItemData:GetRandAttrisRange()
-- 	local evd = self.equipValue
-- 	local result = {}
-- 	local attris = self:GetAttris(public_config.ITEM_KEY_EQUIP_RAND_ATTRI_MIN,public_config.ITEM_KEY_EQUIP_RAND_ATTRI_MAX,true)
-- 	local attriTypes = {}
-- 	attriTypes[1] = evd.random_attri1
-- 	attriTypes[2] = evd.random_attri2
-- 	attriTypes[3] = evd.random_attri3
-- 	attriTypes[4] = evd.random_attri4
-- 	attriTypes[5] = evd.random_attri5
-- 	for i=1,5 do
-- 		local randDatas = attriTypes[i]
-- 		if randDatas then
-- 			for attriKey,v in pairs(attris) do
-- 				if randDatas[tonumber(attriKey)] then
-- 					for k,a in pairs(randDatas) do
-- 						result[k] = EquipRandAttriData(k,a)
-- 					end
-- 				end
-- 			end
-- 		end
-- 	end
-- 	return result
-- end

-- function PkgEquipItemData:IndexAttriMax()
-- 	return self._serverData[public_config.ITEM_KEY_INDEX_ATTRI_MAX]
-- end

--找是否有更好的装备
function PkgEquipItemData:CheckBetterThanLoaded()
	local isBetter
	-- if self:CheckCanLoaded() == false then
	-- 	return false
	-- end
	local bagData = PlayerManager.PlayerDataManager.bagData
	local loadedEquip = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(self.cfgData.type)[1]
	if loadedEquip then
		isBetter = self:GetBaseFightPower() > loadedEquip:GetBaseFightPower()
	else
		isBetter = true
	end
	return isBetter
end

--showSuitPreview:显示下一级套装前缀
function PkgEquipItemData:GetEquipName(showSuitPreview,suitData)
	local prefix = ""
	if self.bagType == public_config.PKG_TYPE_LOADED_EQUIP then
		if suitData == nil then
			suitData = GameWorld.Player().equip_suit_data or {}
		end
		local suitLevel = suitData[self.bagPos] or 0
		if showSuitPreview then
			suitLevel = suitLevel + 1
		end
		if suitLevel > 0 then
			prefix = "["..LanguageDataHelper.CreateContent(523+suitLevel).."]"
		end
	end
	local quality = self.cfgData.quality
	local colorId = ItemConfig.QualityTextMap[quality]
	return StringStyleUtil.GetColorStringWithColorId(prefix..ItemDataHelper.GetItemName(self.cfg_id),colorId)
end

function PkgEquipItemData:GetTimeEnd()
	return self._serverData[public_config.ITEM_KEY_END_TIME] or 0
end

function PkgEquipItemData:GetSuitId()
	return self.cfgData.suitId
end

function PkgEquipItemData:GetSuitCost()
	return self.cfgData.suitCost
end

--守护是否过期
function PkgEquipItemData:CheckGuardTimeOut()
	self._guardTimeOut = false
	--守护特殊处理
	if self:GetType() == public_config.PKG_LOADED_EQUIP_INDEX_GUARD then
		local timeEnd = self:GetTimeEnd()
		local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
		if timeLeft <= 0 then
			self._guardTimeOut = true
		end
	end
	return self._guardTimeOut
end

-------------------------------------神兽装备-----------------------------------------------
local PkgEquipMythicalAnimalItemData = Class.PkgEquipMythicalAnimalItemData(ClassTypes.CommonItemData)

function PkgEquipMythicalAnimalItemData:SetAnimalId(animalId)
	self._animalId = animalId
end

function PkgEquipMythicalAnimalItemData:GetAnimalId()
	return self._animalId
end

--override数据写入
function PkgEquipMythicalAnimalItemData:ResetServerData(svrData)
	self._base.ResetServerData(self,svrData)
	self._fightPowerChange = true --战斗力改变
	self._baseFightPower = nil
	return 0
end

--获取当前装备的熟练度
function PkgEquipMythicalAnimalItemData:GetExp()
	return self._serverData[public_config.ITEM_KEY_GOD_MONSTER_EQUIP_EXP] or 0
end

--是否强化过
function PkgEquipMythicalAnimalItemData:IsHasUpgrade()
	return self._serverData[public_config.ITEM_KEY_GOD_MONSTER_EQUIP_TOTAL_EXP] ~= nil
end

--获取装备用于强化时价值的熟练度
function PkgEquipMythicalAnimalItemData:GetStrengthCostExp(double)
	local num = 1
	if double then
		num = 2
	end
	local level = self:GetMythicalAnimalEquipLevel()
	if self._serverData[public_config.ITEM_KEY_GOD_MONSTER_EQUIP_TOTAL_EXP] == nil then
		return self:GetCfgItemData().exp * num
	else
		return self._serverData[public_config.ITEM_KEY_GOD_MONSTER_EQUIP_TOTAL_EXP]
	end
end

--获取强化等级
function PkgEquipMythicalAnimalItemData:GetMythicalAnimalEquipLevel()
	return self._serverData[public_config.ITEM_KEY_GOD_MONSTER_EQUIP_LEVEL] or 0
end

--获取神兽装备综合战力
function PkgEquipMythicalAnimalItemData:GetFightPower()
	if self._fightPowerChange then
		local additionPower = 0
		local baseFightPower = self:GetBaseFightPower()
		
		--LoggerHelper.Error("PkgEquipItemData:GetFightPower")
		local allAttris = {}
		local t = {}
		--强化加成
		
		local strengthLevel = self:GetMythicalAnimalEquipLevel()
		local maxLevel = self.cfgData.strengthHighLv
		strengthLevel = math.min(strengthLevel,maxLevel)
		local attriInfo = MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(self.bagPos, strengthLevel)
		for k,v in pairs(attriInfo) do
			table.insert(allAttris ,{["attri"] = k, ["value"] = v})
		end

		additionPower = BaseUtil.CalculateFightPower(allAttris,GameWorld.Player().vocation)
		
		self._fightPower = baseFightPower + additionPower
		self._fightPowerChange = false
	end
	return self._fightPower
end

--获取神兽装备基础战力
function PkgEquipMythicalAnimalItemData:GetBaseFightPower()
	if self._baseFightPower == nil then
		local allAttris = {}
		local t = {}
		local baseAttri = EquipDataHelper:GetBaseAttris(self.cfg_id)
		
		for i=1,#baseAttri do
			local attriKey = baseAttri[i].attri
			local attriValue = baseAttri[i].value
			t[attriKey] = (t[attriKey] or 0) + attriValue
		end

		--极品属性用自己的计算方式
		local randAttriScore = self:CalculateRandAttriScore()

		for k,v in pairs(t) do
			table.insert(allAttris ,{["attri"] = k, ["value"] = v})
		end
		self._baseFightPower = BaseUtil.CalculateFightPower(allAttris,GameWorld.Player().vocation) + randAttriScore
	end
	return self._baseFightPower
end

--计算极品属性评分
function PkgEquipMythicalAnimalItemData:CalculateRandAttriScore()
	local randAttri = self:GetRandAttris()
	local score = 0
	for i=1,#randAttri do
		local attriKey = randAttri[i].attri
		--local attriValue = randAttri[i].value

		local scoreCfg = EquipDataHelper.GetEquipScoreData(attriKey,self:GetQuality())
		if scoreCfg == nil then
			LoggerHelper.Error("出现极品属性没有配颜色:"..attriKey.."/"..self:GetQuality())
		end
		local attriScore = scoreCfg.score
		score = score + attriScore
	end
	return score
end

------------------------------------------魔石-------------------------------------------------
--宝石
local PkgGemItemData = Class.PkgGemItemData(ClassTypes.CommonItemData)

------------------------------------------时装-------------------------------------------------
local PkgFashionItemData = Class.PkgFashionItemData(ClassTypes.CommonItemData)

--override数据写入
function PkgFashionItemData:ResetServerData(svrData)
	local dcount = self._base.ResetServerData(self,svrData)
	self._fightPowerChange = true --战斗力改变
	return dcount
end

--获取剩余时间
function PkgFashionItemData:FashionTime()
	return self._serverData[public_config.ITEM_KEY_TIME] or 1
end

--获取星级
function PkgFashionItemData:GetStar()
	return self._serverData[public_config.ITEM_KEY_FASHION_STAR] or 0
end
----------------------------CommonItemData公共方法-----------------------------
function CommonItemData.CreateItemData(serverItemData,bagType,bagPos)
	local result = nil
    local itemId = serverItemData[public_config.ITEM_KEY_ID]
    -- LoggerHelper.Error("CreateItemData"..tostring(serverItemData[1]))
    -- LoggerHelper.Error("CreateItemData"..serverItemData[21])
    -- LoggerHelper.Error("CreateItemData"..serverItemData[22])
    -- if itemId == nil then
    --     for k,v in pairs(serverItemData) do
            -- print( tostring(serverItemData[1]) .. 'serverItemData[1]' .. public_config.ITEM_KEY_ID .. '=' ..tostring(itemId).. '=public_config.ITEM_KEY_ID===serverItemData======' .. tostring(serverItemData))
        -- end
    -- end
    local cfgData = ItemDataHelper.GetItem(itemId)
    if cfgData == nil then
        do return result end
    end
    if cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP then
        result = PkgEquipItemData(serverItemData,bagType,bagPos)
    elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_FASHION then
    	result = PkgFashionItemData(serverItemData,bagType,bagPos)
    elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_GEM then 
        result = PkgGemItemData(serverItemData,bagType,bagPos)
	 elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_GOD_MONSTER then 
        result = PkgEquipMythicalAnimalItemData(serverItemData,bagType,bagPos)
    else
        result = PkgCommonItemData(serverItemData,bagType,bagPos)
    end
    return result
end

local singleCommonItemData
function CommonItemData.GetOneItemData(serverItemData,bagType,bagPos)
	singleCommonItemData = CommonItemData.CreateItemData(serverItemData,bagType,bagPos)
	return singleCommonItemData
end