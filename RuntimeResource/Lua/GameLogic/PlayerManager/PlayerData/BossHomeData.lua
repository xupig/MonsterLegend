local public_config = GameWorld.public_config
local BossHomeDataHelper = GameDataHelper.BossHomeDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3

local Max_Floor_Count = 7

local BossHomeItemData = Class.BossHomeItemData(ClassTypes.XObject)

function BossHomeItemData:__ctor__(id)
    self._id = id
    self._type = 2
end

function BossHomeItemData:UpdateData(rebornTime, isInstrest)
    self._rebornTime = rebornTime
    self._isInstrest = isInstrest
end

function BossHomeItemData:SetRebornTime(rebornTime)
    self._rebornTime = rebornTime
end

function BossHomeItemData:SetIsIntrest(isInstrest)
    if self._isInstrest ~= isInstrest then
        self._isInstrest = isInstrest
        EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Intrest_Changed, self._id)
    end
end

function BossHomeItemData:SetDeadPos(x, y, z)
    self._deadPos = Vector3(x * 0.01, y * 0.01, z * 0.01)
end

function BossHomeItemData:GetId()
    return self._id
end

function BossHomeItemData:GetIsSelected()
    return self._isSelected
end

function BossHomeItemData:SetIsSelected(isSelected)
    self._isSelected = isSelected
end

function BossHomeItemData:IsDead()
    if self._rebornTime then
        return DateTimeUtil:GetServerTime() < self._rebornTime
    else
        return false
    end
end

function BossHomeItemData:GetRebornTime()
    return self._rebornTime or 0
end

function BossHomeItemData:GetIsIntrest()
    return self._isInstrest or false
end

function BossHomeItemData:GetSenceId()
    return BossHomeDataHelper:GetSenceId(self._id)
end

function BossHomeItemData:GetSenceName()
    local mapId = BossHomeDataHelper:GetSenceId(self._id)
    return LanguageDataHelper.GetContent(MapDataHelper.GetChinese(mapId)) or ""
end

function BossHomeItemData:GetLevelLimit()
    return BossHomeDataHelper:GetLevelLimit(self._id)
end

function BossHomeItemData:GetBossLevel()
    return BossHomeDataHelper:GetBossLevel(self._id)
end

function BossHomeItemData:GetBossName()
    return BossHomeDataHelper:GetBossName(self._id)
end

function BossHomeItemData:GetBossNameIcon()
    return BossHomeDataHelper:GetBossNameIcon(self._id)
end

function BossHomeItemData:GetBossIcon()
    return BossHomeDataHelper:GetBossIcon(self._id)
end

function BossHomeItemData:GetBossModel()
    local monsterId = BossHomeDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetMonsterModel(monsterId)
end

function BossHomeItemData:GetBossHeadIcon()
    local monsterId = BossHomeDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetIcon(monsterId)
end

function BossHomeItemData:GetBossDrops()
    local drops = BossHomeDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    return items
end

function BossHomeItemData:GetBossRegionId()
    return BossHomeDataHelper:GetBossRegionId(self._id)
end

function BossHomeItemData:GetVIPLevelLimit()
    return BossHomeDataHelper:GetVIPLevelLimit(self._id)
end

function BossHomeItemData:GetVIPLevelMoney()
    return BossHomeDataHelper:GetVIPLevelMoney(self._id)
end

function BossHomeItemData:GetType()
    return self._type
end

function BossHomeItemData:GetLevelShow()
    return BossHomeDataHelper:GetLevelShow(self._id)
end

function BossHomeItemData:GetPeaceShow()
    return 0
end

local BossHomeData = Class.BossHomeData(ClassTypes.XObject)

function BossHomeData:__ctor__()
    self._bossItems = {}
    self._sortedBossItems = {}
end

function BossHomeData:RefreshBossData(data)
    local floor = data[1]
    local bossData = self:GetAllBossInfoByFloor(floor)
    for i, v in ipairs(bossData) do
        local rebornTime = data[2][v:GetId()]
        v:SetRebornTime(rebornTime or 0)
    end
    self:RefreshBossHomeInterestInfo()
end

function BossHomeData:RefreshBossHomeInterestInfo(changeValue)
    local player = GameWorld.Player()
    local interestInfo = player.boss_home_interest_info or {}
    for i=1, Max_Floor_Count do
        local data = self._sortedBossItems[i] or {}
        for i, v in ipairs(data) do
            local isInstrest = interestInfo[v:GetId()] ~= nil
            v:SetIsIntrest(isInstrest)
        end
    end
end

function BossHomeData:GetAllBossInfoByFloor(floor)
    if self._sortedBossItems[floor] == nil then
        self._sortedBossItems[floor] = {}
        local bossIds = BossHomeDataHelper:GetIdsByFloor(floor)
        for i, data in ipairs(bossIds) do
            local item = self:GetBossInfo(data.id)
            table.insert(self._sortedBossItems[floor], item)
        end
    end
    return self._sortedBossItems[floor]
end

function BossHomeData:GetBossInfo(id)
    if not self._bossItems[id] then
        self._bossItems[id] = BossHomeItemData(id)
    end
    return self._bossItems[id]
end

function BossHomeData:GetBossTire()
    return self._tireValue or 0
end

function BossHomeData:RefreshBossHomeSpaceBossInfo(spaceBossData)
    self._sortedSpaceBossItems = {}
    for i, v in ipairs(spaceBossData) do
        local bossData = self:GetBossInfo(v[1])
        bossData:SetRebornTime(v[2])
        bossData:SetDeadPos(v[3], v[4], v[5])
        table.insert(self._sortedSpaceBossItems, bossData)
    end
end

function BossHomeData:GetAllSpaceBossInfo()
    return self._sortedSpaceBossItems or {}
end