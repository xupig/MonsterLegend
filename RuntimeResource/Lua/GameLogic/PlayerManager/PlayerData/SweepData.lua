local SweepData = Class.SweepData(ClassTypes.XObject)

local VIPDataHelper = GameDataHelper.VIPDataHelper
local VipConfig = GameConfig.VipConfig

function SweepData:__ctor__()
    self._sweepInstanceEndFunc = function(result) self:SweepInstanceEnd(result) end
end

-- data:
-- fightNumFunc:获取能打的次数的方法
-- sweepLevel:扫荡需要的等级
-- sweepFunc:扫荡方法
-- sweepCostData:扫荡消耗数据
-- vipBuyType: VIP购买次数类型
-- showBuyVipFunc:显示vip购买次数方法
function SweepData:ResetData(data)
    self._fightNumFunc = data.fightNumFunc
    self._sweepLevel = data.sweepLevel
    self._sweepFunc = data.sweepFunc
    self._vipBuyType = data.vipBuyType
    self._showBuyVipFunc = data.showBuyVipFunc
    self._sweepType = data.sweepType
    self._start = data.start
    self._costNum = data.costNum
    --LoggerHelper.Error("data.sweepCostData1"..PrintTable:TableToStr(data.sweepCostData))
    for k,v in pairs(data.sweepCostData) do
        --LoggerHelper.Error("data.sweepCostData2"..PrintTable:TableToStr(data.sweepCostData))
        self._itemId = k
        self._itemNum = v
    end
end

function SweepData:GetCostNum()
    return self._costNum
end

function SweepData:GetStart()
    return self._start
end

function SweepData:GetSweepNum()
    return self._fightNumFunc()
end

function SweepData:GetSweepLevel()
    return self._sweepLevel
end

function SweepData:SetSweepEndListener(cb)
    self._sweepEndFunc = cb
    EventDispatcher:AddEventListener(GameEvents.SWEEP_INSTANCE_END, self._sweepInstanceEndFunc)
end

function SweepData:SweepInstanceEnd(result)
    if self._sweepEndFunc then
        self._sweepEndFunc(result)
        EventDispatcher:RemoveEventListener(GameEvents.SWEEP_INSTANCE_END, self._sweepInstanceEndFunc)
    end
end

function SweepData:SweepInstance(num,isOn)
    if self._sweepFunc then
        self._sweepFunc(num,isOn)
        self._costNum  = num
        self._isOn = isOn 
    end
end

function SweepData:GetIsOn()
    return self._isOn
end

function SweepData:GetSweepType()
    return self._sweepType
end

function SweepData:GetSweepFunc()
    return self._sweepFunc
end

function SweepData:GetItemId()
    return self._itemId
end

function SweepData:GetItemNum()
    return self._itemNum
end

--是否可以再来一次
function SweepData:IsCanAgain()
    if self._fightNumFunc() ~= 0 then
        return true
    else
        if self:IsCanBuy() then
            return true
        else
            return false
        end
    end
end

--是否可以买次数
function SweepData:IsCanBuy()
    local nowEnterTimes = VIPDataHelper:GetButTimesByType(self._vipBuyType)
    local canBuyNum = nowEnterTimes - VipConfig.VIP_BUY_TIMES_MAP[self._vipBuyType](self._vipBuyType)
    return canBuyNum > 0
end

function SweepData:Again()
    if self._fightNumFunc() == 0 then
        self._showBuyVipFunc()
    else
        if self:GetCostNum() then
            if self:GetIsOn() then
                self._sweepFunc(tonumber(self:GetCostNum()))
            else
                self._sweepFunc(0)
            end
        else
            --LoggerHelper.Error("not SweepData:GetCostNum()")
            self._sweepFunc()
        end

    end
end
