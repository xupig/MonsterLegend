local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local DailyActivityConfig = GameConfig.DailyActivityConfig

local ActivityTimeData = Class.ActivityTimeData(ClassTypes.XObject)

function ActivityTimeData:__ctor__(timeData)
    self._timeData = timeData
    self._acitvityId = timeData[1]
    self._open_data = timeData[2]
    if timeData[3][1] ~= 0 then
        self._open_day = timeData[3]
    end
    self._open_time = timeData[4]
    self._duration_time = timeData[5]
end

function ActivityTimeData:GetActivivtId()
    return self._acitvityId
end

function ActivityTimeData:GetDurationTime()
    return self._duration_time
end

function ActivityTimeData:GetDescription()
    local result = ""
    local openDay = self._open_day
    if openDay ~= nil then
        result = LanguageDataHelper.GetContent(910)-- 每逢
        local openDayCount = #openDay
        for i, v in ipairs(openDay) do
            result = result .. LanguageDataHelper.GetContent(901 + v)-- 902~908 一~日
            if i ~= openDayCount then
                result = result .. LanguageDataHelper.GetContent(911)-- 、
            end
        end
    end
    local openTime = self._open_time
    if openTime ~= nil then
        local openTimeCount = #openTime
        for i, v in ipairs(openTime) do
            local seconds = DateTimeUtil:HMToSecond(v)
            result = result .. DateTimeUtil:FormatFullTime(seconds, true)
                .. "~" .. DateTimeUtil:FormatFullTime(seconds + self._duration_time, true)
            if i ~= openTimeCount then
                result = result .. LanguageDataHelper.GetContent(911)-- 、
            end
        end
    end
    if openDay == nil and openTime == nil then
        result = LanguageDataHelper.GetContent(915)-- 全天
    end
    result = result .. LanguageDataHelper.GetContent(912)-- 开启
    return result
end

function ActivityTimeData:IsInOpenTime(curTime)
    if not self._timeData then
        return true
    end
    if curTime == nil then
        curTime = DateTimeUtil.Now()
    end
    --判断日期
    local open_date = self._open_data
    if open_date then
        local is_in_range = false
        local date_number = DateTimeUtil.GetDateFormatNumber(curTime)
        for i = 1, #open_date, 2 do
            if open_date[i] <= date_number and date_number < open_date[i + 1] then
                is_in_range = true
                break
            end
        end
        if not is_in_range then
            return false
        end
    end
    --判断星期
    local open_day = self._open_day
    if open_day and open_day[1] ~= 0 then --有星期的限制
        local week_dayth = DateTimeUtil.GetWday(curTime)
        if not table.containsValue(open_day, week_dayth) then
            return false
        end
    end
    --判断时间段
    local open_time = self._open_time
    if open_time then
        for i = 1, #open_time do
            if DateTimeUtil.IsTimeInHMRange(curTime, open_time[i], self._duration_time) then
                return true, open_time[i]
            end
        end
        return false, open_time[1]
    end
    return true
end

function ActivityTimeData:GetTimeLeft(curTime)
    if curTime == nil then
        curTime = DateTimeUtil.Now()
    end
    
    local isInTime, openTime = self:IsInOpenTime(curTime)
    if openTime ~= nil then
        if isInTime then
            local leftSecond = DateTimeUtil:HMToSecond(openTime) + self._duration_time - DateTimeUtil:DateTimeToSecond(curTime)
            return DateTimeUtil:FormatFullTime(leftSecond, true, false, false)
        else
            local openTimeSec = DateTimeUtil:HMToSecond(openTime)
            local curTimeSec = DateTimeUtil:DateTimeToSecond(curTime)
            -- LoggerHelper.Log("Ash: openTimeSec: " .. openTime)
            -- LoggerHelper.Log("Ash: curTimeSec: " .. PrintTable:TableToStr(curTime))
            if openTimeSec > curTimeSec then
                local leftSecond = openTimeSec - curTimeSec
                return DateTimeUtil:FormatFullTime(leftSecond, true, false, false)
            else
                local leftSecond = DateTimeUtil:HMToSecond(2400) + openTimeSec - curTimeSec
                return DateTimeUtil:FormatFullTime(leftSecond, true, false, false)
            end
        end
    else
        return nil
    end
end

function ActivityTimeData:IsInOpenDate(curTime)
    if not self._timeData then
        return true
    end
    if curTime == nil then
        curTime = DateTimeUtil.Now()
    end
    --判断日期
    local open_date = self._open_data
    if open_date then
        local is_in_range = false
        local date_number = DateTimeUtil.GetDateFormatNumber(curTime)
        for i = 1, #open_date, 2 do
            if open_date[i] <= date_number and date_number < open_date[i + 1] then
                is_in_range = true
                break
            end
        end
        if not is_in_range then
            return false
        end
    end
    return true
end

--获取活动情况：(未开启，即将开启，开启中)
function ActivityTimeData:GetActivityStatus(curTime)
    if self:IsInOpenTime() then--开启中
        return DailyActivityConfig.OPEN
    elseif self:IsInOpenTime(curTime) then--即将开启
        return DailyActivityConfig.OPEN_SOON
    else--未开启
        return DailyActivityConfig.NOT_OPEN
    end
end

local AllActivityTimeData = Class.AllActivityTimeData(ClassTypes.XObject)

function AllActivityTimeData:__ctor__(allTimeData)
    self._data = {}
    for i,v in ipairs(allTimeData) do
        local data = ActivityTimeData(v)
        self._data[data:GetActivivtId()] = ActivityTimeData(v)        
    end
end

function AllActivityTimeData:GetData()
    return self._data
end

function AllActivityTimeData:GetDataById(id)
    return self._data[id]
end
