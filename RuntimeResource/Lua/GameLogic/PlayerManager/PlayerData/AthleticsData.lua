local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
require "PlayerManager/PlayerData/CommonTimeListData"
local EventDispatcher = EventDispatcher
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AthleticsTagItemData = Class.AthleticsTagItemData(ClassTypes.XObject)
local AthleticsRoleItemData = Class.AthleticsRoleItemData(ClassTypes.XObject)
local AthleticsData = Class.AthleticsData(ClassTypes.XObject)
local NPCDataHelper = GameDataHelper.NPCDataHelper
local OfflinePvpRobotDataHelper = GameDataHelper.OfflinePvpRobotDataHelper
local CommonTimeListData = ClassTypes.CommonTimeListData
local AthleticsConfig = GameConfig.AthleticsConfig
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local OfflinePvpReward1DataHelper = GameDataHelper.OfflinePvpReward1DataHelper

function AthleticsRoleItemData:__ctor__(data)
    self.itemdata = data
end
function AthleticsRoleItemData:IsRobot()
    if self.itemdata[public_config.OFFLINE_PVP_KEY_DBID]==0 then 
        return true 
    else 
        return false 
    end 
    
end
function AthleticsRoleItemData:GetOrignRank()
    return self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK]
end
function AthleticsRoleItemData:GetName()
    if self:IsRobot() then
        if self:GetOrignRank() ~= nil then
            return OfflinePvpRobotDataHelper:GetName(self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK])  
        else
            return OfflinePvpRobotDataHelper:GetName(self.itemdata[public_config.OFFLINE_PVP_KEY_RANK])  
        end
    end
    return self.itemdata[public_config.OFFLINE_PVP_KEY_NAME] 
end
-- 机器人
function AthleticsRoleItemData:GetFacade()
    if not self:IsRobot() then
        return self.itemdata[public_config.OFFLINE_PVP_KEY_FACADE]
    end
    local rank
    if self:GetOrignRank() ~= nil then
        rank = self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK] 
    else
        rank = self.itemdata[public_config.OFFLINE_PVP_KEY_RANK]
    end
    local modeid = OfflinePvpRobotDataHelper:GetModel(rank)  
    return NPCDataHelper.GetNPCFacade(modeid)
end

function AthleticsRoleItemData:GetModel()
    if self:IsRobot() then
        local rank
        if self:GetOrignRank() ~= nil then
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK] 
        else
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_RANK]
        end
        local modeid = OfflinePvpRobotDataHelper:GetModel(rank)
        return NPCDataHelper.GetModel(modeid)
    end
end

function AthleticsRoleItemData:GetUseRank()
    return self.itemdata[public_config.OFFLINE_PVP_KEY_RANK] 
end
function AthleticsRoleItemData:GetVacation()
    if self:IsRobot() then
        local rank
        if self:GetOrignRank() ~= nil then
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK] 
        else
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_RANK]
        end
        return OfflinePvpRobotDataHelper:GetProfessional(rank) 
    else
        return self.itemdata[public_config.OFFLINE_PVP_KEY_VOCATION]
    end
end
function AthleticsRoleItemData:GetFight()
    if self:IsRobot() then
        local rank
        if self:GetOrignRank() ~= nil then
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_ORIGNAL_RANK] 
        else
            rank = self.itemdata[public_config.OFFLINE_PVP_KEY_RANK]
        end
        return OfflinePvpRobotDataHelper:GetFight(rank) 
    else
        local a = GlobalParamsHelper.GetParamValue(509)/100       
        local v = self.itemdata[public_config.OFFLINE_PVP_KEY_COMBAT_VALUE]*(1+a*self.itemdata[public_config.OFFLINE_PVP_KEY_INSPIRE_COUNT])       
        return math.floor(v)
    end
end

function AthleticsRoleItemData:GetLevel()
    return GlobalParamsHelper.GetParamValue(596)
end

function AthleticsRoleItemData:GetVisRank()
    return self.itemdata[public_config.OFFLINE_PVP_KEY_RANK]
end

function AthleticsTagItemData:__ctor__(index,funid)
    self._index = index
    self._funid = funid
    self._commonTimeListData = {}
    self._condition = FunctionOpenDataHelper:GetCondition(self._funid)
end

function AthleticsTagItemData:GetIcon()
    return GlobalParamsHelper.GetParamValue(881)[self._funid]
end

function AthleticsTagItemData:IsOpen()
    if self._condition[2][1] == 999 then
        return false
    end
    return true
end

function AthleticsTagItemData:IsLock()
    if GameWorld.Player().level >= self:GetOpenlevel() and self:IsOpen() then
        return false
    end
    return true
end

function AthleticsTagItemData:GetOpenStatus()
    if table.isEmpty(self._commonTimeListData) then
        self._commonTimeListData=CommonTimeListData(self:GetDayLimit(),self:GetTimeLimit())
    end
    return self._commonTimeListData:GetOpenStatus()
end
function AthleticsTagItemData:GetDescText()
    if table.isEmpty(self._commonTimeListData) then
        self._commonTimeListData=CommonTimeListData(self:GetDayLimit(),self:GetTimeLimit())
    end
    return self._commonTimeListData:GetDescText()
end
function AthleticsTagItemData:GetOpenlevel()
    return self._condition[2][1]
end

function AthleticsTagItemData:GetDayLimit()
    return ActivityDailyDataHelper:GetDayLimit(self._index)
end

function AthleticsTagItemData:GetTimeLimit()
    return ActivityDailyDataHelper:GetTimeLimit(self._index)
end

function AthleticsTagItemData:GetFunid()
    return self._funid
end

function AthleticsData:__ctor__()
    self._roles = {}
    self._RoleInfo = {}
end

-- 个人信息
function AthleticsData:UpdateSeverRoleInfo(data)
    self._RoleInfo = data
end

function AthleticsData:GetMRank()
    return self._RoleInfo[public_config.OFFLINE_PVP_INFO_KEY_RANK]
end

function AthleticsData:GetMInspire()
    return self._RoleInfo[public_config.OFFLINE_PVP_INFO_KEY_INSPIRE]
end

function AthleticsData:GetMIsReward()
    if self._RoleInfo[public_config.OFFLINE_PVP_INFO_KEY_SETTLE]==0 then
        return false
    end
    return true
end
 
-- 22点结算时的排名
function AthleticsData:GetSetleRank()
    return self._RoleInfo[public_config.OFFLINE_PVP_INFO_KEY_RANK_22]
end

function AthleticsData:UpdateOpenTime()
    local data = {}
    local indexs = AthleticsConfig.indexs
    local funids = AthleticsConfig.funids
    for i in ipairs(indexs) do 
        if indexs[i] ~= 0 then
            table.insert( data,AthleticsTagItemData(indexs[i],funids[i]))
        end
    end
    return data
end


-- 离线对手
function AthleticsData:UpdateOffRoles(data)
    self._roles = data
end


function AthleticsData:UpdateRoles()
    local d = self._roles
    local items = {}
    for k,v in ipairs(d) do
        table.insert(items,AthleticsRoleItemData(v))
    end
    return items
end

function AthleticsData:UpdateCgData(severdata)
    self._sever = severdata
end

function AthleticsData:GetCgData()
    return self._sever
end


-- 默认挑战次数
function AthleticsData:GetRawChaCount()
    return GlobalParamsHelper.GetParamValue(517)
end

-- 默认购买次数
function AthleticsData:GetBugChaCount()
    return GlobalParamsHelper.GetParamValue(518)
end

-- 默认购买单价
function AthleticsData:GetBuyChaPrice()
    return GlobalParamsHelper.GetParamValue(519)
end
function AthleticsData:GetTotalInfo()
    local info={}
    info['chacount'] = GameWorld.Player().offline_pvp_challenge_count
    info['rawchacount'] = self:GetRawChaCount()
    return info
end


function AthleticsData:GetRankRewards()
    local offreward={}
    local ids = OfflinePvpReward1DataHelper:GetAllId()
    for i,v in ipairs(ids) do
        local d = {}
        d["name"] = LanguageDataHelper.GetContent(OfflinePvpReward1DataHelper:GetName(v))
        d["icon"] = OfflinePvpReward1DataHelper:GetIcon(v)
        d["reward"] = OfflinePvpReward1DataHelper:GetReward(v)
        local rank= OfflinePvpReward1DataHelper:GetRank(v)
        d["rank"] = rank
        d['isCanGet'] = self:IsCanGetRankReward(rank)--是否能领
        d["isGeted"] = self:IsGetRankReward(rank)--是否已经领
        d['id'] = tonumber(i)
        table.insert(offreward,d)
    end
    return offreward
end

function AthleticsData:IsGetRankReward(rank)
    if self:GetMIsReward()==false then
        local x = self:GetSetleRank()
        if x and rank then
            if rank[1]<=x and x<=rank[2] then
                return true
            end
        end
    end
    return false
end

function AthleticsData:IsCanGetRankReward(rank)
    if self:GetMIsReward()==true then
        local x = self:GetSetleRank()
        if x and rank then
            if rank[1]<=x and x<=rank[2] then
                return true
            end
        end
    end
    return false
end