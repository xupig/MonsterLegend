local RedPointData = Class.RedPointData(ClassTypes.XObject)

function RedPointData:__ctor__()
    self._parents = {}
    self._childs = {}
    self._getRedPointFunc = nil
    self._updateRedPointFunc = nil
    self._updateRedPointFunc2 = nil
    self._redPoint = false
    self._strengthen = false
end

--增加儿子(私有)
function RedPointData:_addChild(child)
    table.insert(self._childs, child)
end

--设置父亲（私有）
function RedPointData:_setParent(parent)
    table.insert(self._parents, parent)
end

function RedPointData:GetRedPoint()
    return self._redPoint
end

function RedPointData:SetRedPoint(redPoint, forceUpdateView) 
    -- LoggerHelper.Error("functionid ==========".. tostring(self:GetFunctionId()))
    -- LoggerHelper.Error("改变值 ===" .. tostring(redPoint))
    -- LoggerHelper.Error("现在值 ===" .. tostring(self._redPoint))
    if self._redPoint == redPoint and (not forceUpdateView) then
        return
    end
    self._redPoint = redPoint
    self:UpdataRedPointView()
    self:UpdateParentRedPoint()
end

function RedPointData:UpdateParentRedPoint()
    if table.isEmpty(self._parents) then
        return 
    end

    for i,parent in ipairs(self._parents) do
        parent:CheckRedPoint()
    end
end

--根据自己的儿子节点判断自己的状态
function RedPointData:CheckRedPoint()
    -- LoggerHelper.Error("functionId  ===" .. tostring(self:GetFunctionId()))
    local status = false
    for i,child in ipairs(self._childs) do
        -- LoggerHelper.Error("child id =====" .. tostring(self:GetFunctionId()) .. "status ====" .. tostring(child:GetRedPoint()))
        if child:GetRedPoint() then
            status = true
            break            
        end
    end
    self:SetRedPoint(status)
end

function RedPointData:GetPanelName()
    return self._panelName
end

function RedPointData:SetPanelName(panelName)
    self._panelName = panelName
end

function RedPointData:GetTimingId()
    return self._timingId
end

function RedPointData:SetTimingId(timingId)
    self._timingId = timingId
end

function RedPointData:GetFunctionId()
    return self._functionId
end

function RedPointData:SetFunctionId(functionId)
    self._functionId = functionId
end

function RedPointData:GetFirstIndex()
    return self._firstIndex
end

function RedPointData:SetFirstIndex(firstIndex)
    self._firstIndex = firstIndex
end

function RedPointData:GetSecondIndex()
    return self._secondIndex
end

function RedPointData:SetSecondIndex(secondIndex)
    self._secondIndex = secondIndex
end

--增加节点的儿子
function RedPointData:AddChild(child)
    self:_addChild()    
    child:_setParent(self)
end

--设置节点的父亲
function RedPointData:SetParent(parent)
    self:_setParent(parent)
    parent:_addChild(self)
end

function RedPointData:UpdataData()

end

--设置获取红点状态方法
function RedPointData:SetGetRedPointFunc(func)
    self._getRedPointFunc = func
end

--更新UI红点信息方法
function RedPointData:SetUpdateRedPointFunc(func, notUpdateView)
    self._updateRedPointFunc = func
    if  not notUpdateView then
        self:UpdataRedPointView()
    end
end

--更新UI红点信息方法2（临时添加，简单快捷，后续有更多的时候再考虑封装）
function RedPointData:SetUpdateRedPointFunc2(func, notUpdateView)
    self._updateRedPointFunc2 = func
    if  not notUpdateView then
        self:UpdataRedPointView()
    end
end

--获取红点状态
function RedPointData:GetRePointStatus()
    if self._getRedPointFunc then
        return self._getRedPointFunc()
    end
end

--更新UI红点
function RedPointData:UpdataRedPointView()
    if self._updateRedPointFunc then
        self._updateRedPointFunc(self._redPoint)
    end
    if self._updateRedPointFunc2 then
        self._updateRedPointFunc2(self._redPoint)
    end
end

--根据firstindex和secondeIndex获取节点(panel节点使用)
function RedPointData:GetNode(firstIndex, secondIndex)
    for f,firstNode in ipairs(self._childs) do
        if firstIndex == f and secondIndex == firstNode:GetSecondIndex() then
            return firstNode
        end

        for s,secondNode in ipairs(firstNode._childs) do            
            if firstIndex == f and secondIndex == s then
                return secondNode
            end
        end
    end
    return nil
end

function RedPointData:GetStrengthTextId()
    return self._strengthTextId
end

function RedPointData:SetStrengthTextId(strengthTextId)
    self._strengthTextId = strengthTextId
end

function RedPointData:GetStrengthCallBack()
    return self._strengthCallBack
end

function RedPointData:SetStrengthCallBack(func)
    self._strengthCallBack = func
end

function RedPointData:SetStrengthData(data)
    self._strengthCallBackData = data
end

function RedPointData:GetStrengthData()
    return self._strengthCallBackData
end

--检查强化情况
function RedPointData:CheckStrengthen(status)
    if self._strengthen == status then
        return
    end

    if self._strengthen then
        -- LoggerHelper.Error("发送事件 REMOVE_STRENGTH_LIST_CONTENT")
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_STRENGTH_LIST_CONTENT, self) 
    else
        -- LoggerHelper.Error("发送事件 ADD_STRENGTH_LIST_CONTENT")
        EventDispatcher:TriggerEvent(GameEvents.ADD_STRENGTH_LIST_CONTENT, self) 
    end
    self._strengthen = status
    self:SetRedPoint(status)
end