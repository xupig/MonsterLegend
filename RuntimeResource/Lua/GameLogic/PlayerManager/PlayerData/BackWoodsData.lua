local public_config = GameWorld.public_config
local BackwoodsDataHelper = GameDataHelper.BackwoodsDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3

local Max_Floor_Count = 4

local BackWoodsItemData = Class.BackWoodsItemData(ClassTypes.XObject)

function BackWoodsItemData:__ctor__(id)
    self._id = id
    self._type = 3
end

function BackWoodsItemData:UpdateData(rebornTime, isInstrest)
    self._rebornTime = rebornTime
    self._isInstrest = isInstrest
end

function BackWoodsItemData:SetRebornTime(rebornTime)
    self._rebornTime = rebornTime
end

function BackWoodsItemData:SetIsIntrest(isInstrest)
    if self._isInstrest ~= isInstrest then
        self._isInstrest = isInstrest
        EventDispatcher:TriggerEvent(GameEvents.On_Boss_Home_Intrest_Changed, self._id)
    end
end

function BackWoodsItemData:SetDeadPos(x, y, z)
    self._deadPos = Vector3(x * 0.01, y * 0.01, z * 0.01)
end

function BackWoodsItemData:GetId()
    return self._id
end

function BackWoodsItemData:GetIsSelected()
    return self._isSelected
end

function BackWoodsItemData:SetIsSelected(isSelected)
    self._isSelected = isSelected
end

function BackWoodsItemData:IsDead()
    if self._rebornTime then
        return DateTimeUtil:GetServerTime() < self._rebornTime
    else
        return false
    end
end

function BackWoodsItemData:GetRebornTime()
    return self._rebornTime or 0
end

function BackWoodsItemData:GetIsIntrest()
    return self._isInstrest or false
end

function BackWoodsItemData:GetSenceId()
    return BackwoodsDataHelper:GetSenceId(self._id)
end

function BackWoodsItemData:GetSenceName()
    local mapId = BackwoodsDataHelper:GetSenceId(self._id)
    return LanguageDataHelper.GetContent(MapDataHelper.GetChinese(mapId)) or ""
end

function BackWoodsItemData:GetLevelLimit()
    return BackwoodsDataHelper:GetLevelLimit(self._id)
end

function BackWoodsItemData:GetBossLevel()
    return BackwoodsDataHelper:GetBossLevel(self._id)
end

function BackWoodsItemData:GetBossName()
    return BackwoodsDataHelper:GetBossName(self._id)
end

function BackWoodsItemData:GetBossNameIcon()
    return BackwoodsDataHelper:GetBossNameIcon(self._id)
end

function BackWoodsItemData:GetBossIcon()
    return BackwoodsDataHelper:GetBossIcon(self._id)
end

function BackWoodsItemData:GetBossModel()
    local monsterId = BackwoodsDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetMonsterModel(monsterId)
end

function BackWoodsItemData:GetBossHeadIcon()
    local monsterId = BackwoodsDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetIcon(monsterId)
end

function BackWoodsItemData:GetBossDrops()
    local drops = BackwoodsDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    return items
end

function BackWoodsItemData:GetBossRegionId()
    return BackwoodsDataHelper:GetBossRegionId(self._id)
end

function BackWoodsItemData:GetVIPLevelLimit()
    return BackwoodsDataHelper:GetVIPLevelLimit(self._id)
end

function BackWoodsItemData:GetVIPLevelMoney()
    return BackwoodsDataHelper:GetVIPLevelMoney(self._id)
end

function BackWoodsItemData:GetBossFloor()
    return BackwoodsDataHelper:GetBossFloor(self._id)
end

function BackWoodsItemData:GetType()
    return self._type
end

function BackWoodsItemData:GetLevelShow()
    return BackwoodsDataHelper:GetLevelShow(self._id)
end

function BackWoodsItemData:GetPeaceShow()
    return 0
end

------------------------------------------
local BackWoodsEliteItemData = Class.BackWoodsEliteItemData(ClassTypes.XObject)

function BackWoodsEliteItemData:__ctor__(id)
    self._id = id
    self._type = 2
end

function BackWoodsEliteItemData:GetId()
    return self._id
end

function BackWoodsEliteItemData:GetEliteSenceId()
    return BackwoodsDataHelper:GetEliteSenceId(self._id)
end

function BackWoodsEliteItemData:GetEliteLevel()
    return BackwoodsDataHelper:GetEliteLevel(self._id)
end

function BackWoodsEliteItemData:GetEliteName()
    local monsterId = BackwoodsDataHelper:GetEliteMonsterId(self._id)
    return MonsterDataHelper.GetMonsterName(monsterId)
end

function BackWoodsEliteItemData:GetEliteRegionId()
    return BackwoodsDataHelper:GetEliteRegionId(self._id)
end

function BackWoodsEliteItemData:GetType()
    return self._type
end
---------------------------------------------------------------

local BackWoodsData = Class.BackWoodsData(ClassTypes.XObject)

function BackWoodsData:__ctor__()
    self._bossItems = {}
    self._sortedBossItems = {}
    self._eliteItems = {}
    self._sortedEliteItems = {}
    self.maxCount = 0
    self.enterCount = 0
    self.floorPlayerCount = {}
end

--设置boss的列表数据信息
--{"1" = 1,"2" = {"1" = 0,"2" = 0,"3" = 0,"4" = 0,"5" = 0,"6" = 0,"7" = 0,"8" = 0},"3" = 0,"4" = {"2342" = 0,"2341" = 0}}
function BackWoodsData:RefreshBossData(data)
    local floor = data[1]
    local bossData = self:GetAllBossInfoByFloor(floor)
    for i, v in ipairs(bossData) do
        local rebornTime = data[2][v:GetId()]
        v:SetRebornTime(rebornTime or 0)
        --v:SetHiddenBoss();
    end
    self.floorPlayerCount[floor] = data[3]
    self:RefreshForbiddenInfo()
end

--设置boss信息，中的IsInterest
function BackWoodsData:RefreshForbiddenInfo(changeValue)
    local player = GameWorld.Player()
    local interestInfo = player.forbidden_info[1] or {}
    for i=1, Max_Floor_Count do
        local data = self._sortedBossItems[i] or {}
        for i, v in ipairs(data) do
            local isInstrest = interestInfo[v:GetId()] ~= nil
            v:SetIsIntrest(isInstrest)
        end
    end
    self.enterCount = player.forbidden_info[public_config.FORBIDDEN_KEY_ENTER_TIMES]
    self.maxCount = player.forbidden_info[public_config.FORBIDDEN_KEY_CAN_ENTER_TIMES]
end

--获取bossid列表，然后根据bossId，获取所有boss信息
function BackWoodsData:GetAllBossInfoByFloor(floor)
    if self._sortedBossItems[floor] == nil then
        self._sortedBossItems[floor] = {}
        local bossIds = BackwoodsDataHelper:GetIdsByFloor(floor)
        -- LoggerHelper.Log(bossIds)
        for i, data in ipairs(bossIds) do
            local item = self:GetBossInfo(data.id)
            -- LoggerHelper.Log(item)
            table.insert(self._sortedBossItems[floor], item)
        end
    end
    return self._sortedBossItems[floor]
end

--获取boss信息
function BackWoodsData:GetBossInfo(id)
    if not self._bossItems[id] then
        self._bossItems[id] = BackWoodsItemData(id)
        -- LoggerHelper.Log(BackWoodsItemData)
    end
    return self._bossItems[id]
end

function BackWoodsData:GetBossTire()
    return self._tireValue or 0
end

function BackWoodsData:RefreshBackWoodsSpaceBossInfo(spaceBossData)
    self._sortedSpaceBossItems = {}
    for i, v in ipairs(spaceBossData) do
        local bossData = self:GetBossInfo(v[1])
        bossData:SetRebornTime(v[2])
        bossData:SetDeadPos(v[3], v[4], v[5])
        table.insert(self._sortedSpaceBossItems, bossData)
        self._curSelectFloor = bossData:GetBossFloor()
    end
end

function BackWoodsData:GetAllSpaceBossInfo()
    return self._sortedSpaceBossItems or {}
end

function BackWoodsData:GetEliteInfo(id)
    if not self._eliteItems[id] then
        self._eliteItems[id] = BackWoodsEliteItemData(id)
    end
    return self._eliteItems[id]
end

function BackWoodsData:GetAllSpaceEliteInfo()
    local floor = self._curSelectFloor or 1
    if self._sortedEliteItems[floor] == nil then
        self._sortedEliteItems[floor] = {}
        local eliteIds = BackwoodsDataHelper:GetEliteIdsByFloor(floor)
        for i, data in ipairs(eliteIds) do
            local item = self:GetEliteInfo(data.id)
            table.insert(self._sortedEliteItems[floor], item)
        end
    end
    return self._sortedEliteItems[floor] or {}
end

function BackWoodsData:GetRemainCount()
    return self.maxCount - self.enterCount
end

function BackWoodsData:GetPlayerCountByFloor(floor)
    return self.floorPlayerCount[floor] or 0
end

function BackWoodsData:GetMaxCount()
    return self.maxCount or 0
end