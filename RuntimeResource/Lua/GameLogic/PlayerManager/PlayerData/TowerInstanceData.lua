local TowerInstanceData = Class.TowerInstanceData(ClassTypes.XObject)

local public_config = GameWorld.public_config

local function SortItemList(a,b)
    local qualityA = ItemDataHelper.GetQuality(a.itemId)
    local qualityB = ItemDataHelper.GetQuality(b.itemId)
    return qualityA > qualityB
end

function TowerInstanceData:__ctor__()
    
end

function TowerInstanceData:Init()
    self._totalNum = 0
    self._waveNum = 0
    self._remainNum = 0
    self._escapeNum = 0
    self._itemNum = 0
    self._rewards = {}
    self._moneyRewards = {}
    self._itemRewards = {}
    self._exp = 0
end

function TowerInstanceData:UpdateData(serveData)
    --所有副本结算key
    -- params = {
    --     [1] = total_wave,
    --     [2] = wave,
    --     [3] = left_num,
    --     [4] = escape_num,
    --     [5] = items,
    --     [6] = call_monster, --当前波召唤的数量 
    -- }
    self:Init()
    if serveData == nil then
        do return end
    end

    self._totalNum = serveData[1]
    self._waveNum = serveData[2]
    self._remainNum = serveData[3]
    self._escapeNum = serveData[4]
    self._itemNum = 0

    if serveData[5] ~= nil and (not table.isEmpty(serveData[5])) then
        self._rewards = serveData[5]
        for itemId, num in pairs(self._rewards) do
            local data = {}
            data.itemId = itemId
            data.num = num
            if tonumber(itemId) < public_config.MAX_MONEY_ID then                
                table.insert(self._moneyRewards, data)
            else
                self._itemNum = self._itemNum + num
                table.insert(self._itemRewards, data)
            end

            if tonumber(itemId) == public_config.MONEY_TYPE_EXP then
                self._exp = num
            end
        end
    end
end

function TowerInstanceData:GetItemNum()
    return self._itemNum
end

function TowerInstanceData:GetWaveNum()
    return self._waveNum
end

function TowerInstanceData:GetTotalNum()
    return self._totalNum
end

function TowerInstanceData:GetRemainNum()
    return self._remainNum
end

function TowerInstanceData:GetEscapeNum()
    return self._escapeNum
end

function TowerInstanceData:GetExp()
    return self._exp
end

function TowerInstanceData:GetMoneyReward()
    return self._moneyRewards
end

function TowerInstanceData:GetItemRewards()
    table.sort(self._itemRewards, SortItemList)
    return self._itemRewards
end