local TradeData = Class.TradeData(ClassTypes.XObject)
local BlackMarketItemData = Class.BlackMarketItemData(ClassTypes.XObject)
local TradeItemData = Class.TradeItemData(ClassTypes.XObject)
local BegItemData = Class.BegItemData(ClassTypes.XObject)
local public_config = GameWorld.public_config
local CommonItemData = ClassTypes.CommonItemData
local DateTimeUtil = GameUtil.DateTimeUtil


--交易行数据集
function TradeData:__ctor__()
	self.allTradeItemList = {} 		--全部交易道具
	self.searchByNameList = {} 		--靠名字搜索
	self.myTradeItemList = {}

	self.allBegItemList = {} 			--全部求购道具列表
	self.begItemListByType = {}			--某个种类求购道具列表
	self.searchByNameBegList = {} 		--靠名字搜索
	self.myBegItemList = {}				--自己求购列表
end

function TradeData:UpdateAllTradeItemList(svrData)
	--LoggerHelper.Log(svrData)
	local auctionType = svrData[1]
	local tempList = svrData[2]
	self.allTradeItemList[auctionType] = {}
	for k,v in pairs(tempList) do
		local tradeItemData = TradeItemData(v)
		self.allTradeItemList[auctionType][k] = tradeItemData
	end
	return auctionType
end

function TradeData:UpdateMySellItemInfo(svrData)
	self.myTradeItemList = {}
	for k,v in pairs(svrData) do
		local tradeItemData = TradeItemData(v)
		self.myTradeItemList[k] = tradeItemData
	end
	--LoggerHelper.Log(svrData)
end

function TradeData:UpdateSearchByName(svrData)
	self.searchByNameList = {}
	for k,v in pairs(svrData) do
		local tradeItemData = TradeItemData(v)
		self.searchByNameList[k] = tradeItemData
	end
end

------------------------------------交易道具数据---------------------------------------------
function TradeItemData:__ctor__(svrData)
	self:ResetData(svrData)
end

function TradeItemData:ResetData(svrData)
	--LoggerHelper.Log(svrData)
	self._svrData = svrData
	local itemDetail = self._svrData[public_config.AUCTION_ITEM_ITEM]
	if itemDetail then
		self._itemData = CommonItemData.CreateItemData(itemDetail,0,0)
	end
end

function TradeItemData:GetDBId()
	return self._svrData[public_config.AUCTION_ITEM_DBID] or 0
end

--总价
function TradeItemData:GetPrice()
	return self._svrData[public_config.AUCTION_ITEM_PRICE] or 0
end

--数量
function TradeItemData:GetNum()
	return self._itemData:GetCount() or 1
end

--密码
function TradeItemData:GetPassword()
	return self._svrData[public_config.AUCTION_ITEM_PASSWD]
end

--剩余时间
function TradeItemData:GetTimeLeft()
	local timeStamp = self._svrData[public_config.AUCTION_ITEM_CREATE_TIME]
	local timeLeft = math.max(timeStamp - DateTimeUtil.GetServerTime(), 0)
	return timeLeft
end

--道具具体数据（CommonItemData）
function TradeItemData:GetItemData()
	return self._itemData
end

------------------------------------求购数据处理---------------------------------------------
function TradeData:UpdateAllBegItemList(svrData)
	--LoggerHelper.Log(svrData)
	self.allBegItemList = {}
	for k,v in pairs(svrData) do
		local begItemData = BegItemData(v)
		self.allBegItemList[k] = begItemData
	end
end

function TradeData:UpdateBegItemListByType(svrData)
	--LoggerHelper.Log(svrData)
	local auctionType = svrData[1]
	local tempList = svrData[2]
	self.begItemListByType[auctionType] = {}
	for k,v in pairs(tempList) do
		local begItemData = BegItemData(v)
		self.begItemListByType[auctionType][k] = begItemData
	end
	return auctionType
end


function TradeData:UpdateMyBeg(svrData)
	self.myBegItemList = {}
	for k,v in pairs(svrData) do
		local begItemData = BegItemData(v)
		self.myBegItemList[k] = begItemData
	end
	--LoggerHelper.Log(svrData)
end

function TradeData:UpdateBegSearchByName(svrData)
	self.searchByNameBegList = {}
	for k,v in pairs(svrData) do
		local begItemData = BegItemData(v)
		self.searchByNameBegList[k] = begItemData
	end
end


------------------------------------求购道具数据---------------------------------------------
function BegItemData:__ctor__(svrData)
	self:ResetData(svrData)
end

function BegItemData:ResetData(svrData)
	--LoggerHelper.Log(svrData)
	self._svrData = svrData
	local itemDetail = self._svrData[public_config.BEG_ITEM_ITEM]
	if itemDetail then
		self._itemData = CommonItemData.CreateItemData(itemDetail,0,0)
	end
end

function BegItemData:GetDBId()
	return self._svrData[public_config.BEG_ITEM_DBID] or 0
end

--总价
function BegItemData:GetPrice()
	return self._svrData[public_config.BEG_ITEM_PRICE] or 0
end

--数量
function BegItemData:GetNum()
	return self._itemData:GetCount() or 0
end

--求购者名字
function BegItemData:GetPlayerName()
	return self._svrData[public_config.BEG_ITEM_AVATAR_NAME] or ""
end

--剩余时间
function BegItemData:GetTimeLeft()
	local timeStamp = self._svrData[public_config.BEG_ITEM_CREATE_TIME]
	local timeLeft = math.max(timeStamp - DateTimeUtil.GetServerTime(), 0)
	return timeLeft
end

--道具具体数据（CommonItemData）
function BegItemData:GetItemData()
	return self._itemData
end
