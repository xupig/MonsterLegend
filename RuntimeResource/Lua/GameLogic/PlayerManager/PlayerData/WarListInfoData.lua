local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")

local WarListInfoData = Class.WarListInfoData(ClassTypes.XObject)

function WarListInfoData:__ctor__()

end

function WarListInfoData:RefreshData(data)
    self.warType = data[1]
    self.curPage = data[2]
    self.totalPage = data[3]
    self.infoList = data[4]
end

function WarListInfoData:GetListData()
    return self.infoList or {}
end

function WarListInfoData:GetTotalPage()
    return self.totalPage or 0
end

function WarListInfoData:GetCurPage()
    return self.curPage or 0
end

function WarListInfoData:GetWarType()
    return self.warType or 0
end