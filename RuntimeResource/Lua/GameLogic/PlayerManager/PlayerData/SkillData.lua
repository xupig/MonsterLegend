local SkillDataHelper = GameDataHelper.SkillDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig

local  SkillData = Class. SkillData(ClassTypes.XObject)

local public_config = GameWorld.public_config

local SKILL_BUTTON_SUM = 8

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

function SkillData:__ctor__(id)
	self._skillOpenQueue = {}
	self._learned_skills ={}
	self._learned_passive_spell ={}
	self._learned_skills_map ={} --转换释放槽位
end

function SkillData:RefreshLearnedSkills(changeValue)
	if changeValue ~= nil then
		for k,v in pairs(GameWorld.Player().learned_skills) do
			if self._learned_skills[k] == nil then
				local skill = SkillDataHelper.GetActiveSkillId(k, GameWorld.Player().vocation)[1]
				self:ReadyShowSkillOpen({id=skill, slot=v})
			end
		end
	end
	self._learned_skills = BaseUtil.DeepCopy(1, GameWorld.Player().learned_skills)
	self._learned_skills_map = {}
	for k,v in pairs(self._learned_skills) do
		self._learned_skills_map[v] = k
	end
end

function SkillData:RefreshLearnedPassiveSpell(changeValue)
	if changeValue == nil then
		self._learned_passive_spell = {}
	end
	if changeValue ~= nil then
		for k,v in pairs(changeValue) do       
			--删除数据
			if k == table.REMOVE_FLAG then
				--v是下标
			elseif self._learned_passive_spell[k] == nil then
				self:ReadyShowSkillOpen({id=k, slot=0})
			end
		end
	end
	self._learned_passive_spell = BaseUtil.DeepCopy(1, GameWorld.Player().learned_passive_spell)
end

function SkillData:ReadyShowSkillOpen(data)
	table.insert(self._skillOpenQueue, data)
	self:CheckShowSkillOpen()
end

function SkillData:CheckShowSkillOpen()
	if #self._skillOpenQueue > 0 and not GUIManager.PanelIsActive(PanelsConfig.SkillOpen) then
		local data = table.remove(self._skillOpenQueue, 1)
		GUIManager.ShowPanel(PanelsConfig.SkillOpen, data)
    end
end

function SkillData:GetEquipSkillIdList()
	local learnedActiveSkills = {}
	local player = GameWorld.Player()
	local initSkills = RoleDataHelper.GetSpellLearned(player.vocation) or {}
	for k,v in pairs(player.learned_skills) do
		if v > 0 then
			local skillIds = SkillDataHelper.GetActiveSkillId(k, player.vocation)
			for _,skillId in pairs(skillIds) do
				table.insert(learnedActiveSkills, skillId)
				table.insert(learnedActiveSkills, v)
			end
		end
	end
	for k,v in pairs(initSkills) do
		table.insert(learnedActiveSkills, v)
		table.insert(learnedActiveSkills, 0)
	end
	return learnedActiveSkills
end

--获取技能槽解锁状态
function SkillData:GetGroundSkillSlotState()
	local player = GameWorld.Player()
	local result = {}
	for i=1, SKILL_BUTTON_SUM do
		if player.learned_skills[i] ~= nil and player.learned_skills[i] > 0 then
			local slot = player.learned_skills[i]
			result[slot] = 1
		end
	end
	return result
end

function SkillData:IsPassiveSkillGot(skillId)
	local player = GameWorld.Player()
	if player.learned_passive_spell[skillId] ~= nil then
		return true
	end
	return false
end

function SkillData:IsActiveSkillGot(id)
	local player = GameWorld.Player()
	if player.learned_skills[id] ~= nil then
		return true
	end
	return false
end

--
function SkillData:GetSlotConfigData()
	local result = {}
	for i=2, SKILL_BUTTON_SUM do
		result[i] = 0
	end
	local player = GameWorld.Player()
	for k,v in pairs(player.learned_skills) do
		if k > 1 and v > 0 then
			result[v] = k
		end
	end
	return result
end

--ai释放使用,槽位需转换
function SkillData:GetSkillsAutoCastStateByAI(slot)
	local player = GameWorld.Player()
	local realSlot = self._learned_skills_map[slot]
	if player.skills_auto_cast_info[realSlot] ~= nil and player.skills_auto_cast_info[realSlot] == 1 then
		return true
	end
	return false
end

--ui界面使用
function SkillData:GetSkillsAutoCastState(slot)
	local player = GameWorld.Player()
	if player.skills_auto_cast_info[slot] ~= nil and player.skills_auto_cast_info[slot] == 1 then
		return true
	end
	return false
end

function SkillData:SetSkillButtonPosInfo(data)
	self._posData = data
end

function SkillData:GetSkillButtonPosInfo(slot)
	return self._posData[slot]
end

function SkillData:RefreshLearnedSkillDict()
	GameWorld.Player():SetLearnedSkillDict(self:GetEquipSkillIdList())
end

function SkillData:RefreshTalentRedPointTypeData(data)
	self.talentRedPointTypeData = data
end

function SkillData:GetTalentRedPointTypeData(data)
	return self.talentRedPointTypeData
end