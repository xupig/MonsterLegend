local public_config = GameWorld.public_config
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3

local WorldBossItemData = Class.WorldBossItemData(ClassTypes.XObject)

function WorldBossItemData:__ctor__(id)--, rebornTime, isInstrest
    self._id = id
    self._type = 1
-- self._rebornTime = rebornTime
-- self._isInstrest = isInstrest
end

function WorldBossItemData:UpdateData(rebornTime, isInstrest)
    self._rebornTime = rebornTime
    self._isInstrest = isInstrest
end

function WorldBossItemData:SetRebornTime(rebornTime)
    self._rebornTime = rebornTime
end

function WorldBossItemData:SetIsIntrest(isInstrest)
    if self._isInstrest ~= isInstrest then
        self._isInstrest = isInstrest
        EventDispatcher:TriggerEvent(GameEvents.OnWorldBossIsIntrestChanged, self._id)
    end
end

function WorldBossItemData:SetDeadPos(x, y, z)
    self._deadPos = Vector3(x * 0.01, y * 0.01, z * 0.01)
end

function WorldBossItemData:GetId()
    return self._id
end

function WorldBossItemData:GetIsSelected()
    return self._isSelected
end

function WorldBossItemData:SetIsSelected(isSelected)
    self._isSelected = isSelected
end

function WorldBossItemData:IsDead()
    if self._rebornTime then
        return DateTimeUtil:GetServerTime() < self._rebornTime
    else
        return false
    end
end

function WorldBossItemData:GetRebornTime()
    return self._rebornTime or 0
end

function WorldBossItemData:GetIsIntrest()
    return self._isInstrest or false
end

function WorldBossItemData:GetSenceId()
    return WorldBossDataHelper:GetSenceId(self._id)
end

function WorldBossItemData:GetSenceName()
    local mapId = WorldBossDataHelper:GetSenceId(self._id)
    return LanguageDataHelper.GetContent(MapDataHelper.GetChinese(mapId)) or ""
end

function WorldBossItemData:GetLevelLimit()
    return WorldBossDataHelper:GetLevelLimit(self._id)
end

function WorldBossItemData:GetBossLevel()
    return WorldBossDataHelper:GetBossLevel(self._id)
end

function WorldBossItemData:GetBossName()
    return WorldBossDataHelper:GetBossName(self._id)
end

function WorldBossItemData:GetBossNameIcon()
    return WorldBossDataHelper:GetBossNameIcon(self._id)
end

function WorldBossItemData:GetBossIcon()
    return WorldBossDataHelper:GetBossIcon(self._id)
end

function WorldBossItemData:GetBossModel()
    local monsterId = WorldBossDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetMonsterModel(monsterId)
end

function WorldBossItemData:GetBossHeadIcon()
    local monsterId = WorldBossDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetIcon(monsterId)
end

function WorldBossItemData:GetBossDrops()
    local drops = WorldBossDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    --LoggerHelper.Log("Ash: GetItem:" .. PrintTable:TableToStr(items))
    return items
end

function WorldBossItemData:GetBossRegionId()
    return WorldBossDataHelper:GetBossRegionId(self._id)
end

function WorldBossItemData:GetType()
    return self._type
end

function WorldBossItemData:GetLevelShow()
    return WorldBossDataHelper:GetLevelShow(self._id)
end

function WorldBossItemData:GetPeaceShow()
    return WorldBossDataHelper:GetPeaceShow(self._id)
end

-- function WorldBossItemData:GetTalkToSomeOneContent()
--     local npcData = NPCDataHelper.GetNPCData(self._npcId)
--     local npcName = LanguageDataHelper.CreateContent(npcData.name)
--     return LanguageDataHelper.CreateContentWithArgs(TaskConfig.TALK_TO_SOMEONE, {["0"] = npcName})
-- end
local WorldBossData = Class.WorldBossData(ClassTypes.XObject)

function WorldBossData:__ctor__()
    self._bossItems = {}
-- self._sortedBossItems = {}
-- self._rebornTimeList = {}
-- self._tireValue = {}
end

function WorldBossData:UpdateWorldBossData(worldBossData)
    if worldBossData == nil then
        return
    end
    self:GetAllBossInfo()
    local rebornTime = worldBossData[1]
    for i, v in ipairs(self._sortedBossItems) do
        v:SetRebornTime(rebornTime and rebornTime[v:GetId()] or 0)
    end
    self:UpdateWorldBossIntrest(worldBossData[2])
    self._tireValue = worldBossData[3]
end

function WorldBossData:UpdateWorldBossIntrest(intrestData)
    for i, v in ipairs(self._sortedBossItems) do
        local isInstrest = intrestData and intrestData[v:GetId()] ~= nil or false
        v:SetIsIntrest(isInstrest)
    end
end

function WorldBossData:UpdateWorldBossSpaceBossInfo(spaceBossData)
    self._sortedSpaceBossItems = {}
    for i, v in ipairs(spaceBossData) do
        local bossData = self:GetBossInfo(v[1])
        bossData:SetRebornTime(v[2])
        bossData:SetDeadPos(v[3], v[4], v[5])
        table.insert(self._sortedSpaceBossItems, bossData)
    end
end

function WorldBossData:GetAllBossInfo()
    if self._sortedBossItems == nil then
        self._sortedBossItems = {}
        local bossIds = WorldBossDataHelper:GetAllId()
        for i, id in ipairs(bossIds) do
            local item = self:GetBossInfo(id)
            table.insert(self._sortedBossItems, item)
        end
    end
    return self._sortedBossItems
end

function WorldBossData:GetAllSpaceBossInfo()
    return self._sortedSpaceBossItems or {}
end

function WorldBossData:GetBossInfo(id)
    if not self._bossItems[id] then
        self._bossItems[id] = WorldBossItemData(id)
    end
    return self._bossItems[id]
end

function WorldBossData:GetBossTire()
    return self._tireValue or 0
end
