local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local LotteryPeakData = Class.LotteryPeakData(ClassTypes.XObject)

function LotteryPeakData:__ctor__()

end

function LotteryPeakData:RefreshSelfRecord(data)
    self.selfRecordData = data
end

function LotteryPeakData:RefreshServerRecord(data)
    self.serverRecordData = data
end

function LotteryPeakData:RefreshRewardData(data)
    self.getRewardData = data
end