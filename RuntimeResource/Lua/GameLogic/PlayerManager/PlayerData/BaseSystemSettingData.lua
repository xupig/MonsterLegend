--BaseSystemSettingData
local BaseSystemSettingData = Class.BaseSystemSettingData(ClassTypes.XObject)
local SystemSettingConfig = GameConfig.SystemSettingConfig

function BaseSystemSettingData:__ctor__()
    self._settingData = {}
end

function BaseSystemSettingData:SetData(SystemSettingConfig,data)
    self._settingData[SystemSettingConfig] = data
end

function BaseSystemSettingData:GetData(SystemSettingConfig)
    return self._settingData[SystemSettingConfig]
end

function BaseSystemSettingData:GetAllData()
    return self._settingData
end

function BaseSystemSettingData:LoadSettingData()
    local data = GameWorld.ReadCache(CacheConfig.SystemBaseSettingData, false)
    if data == nil then
        return nil
    else
        local dataJson = GameWorld.json.decode(data[0])
        return dataJson
    end
end

function BaseSystemSettingData:LoadData()
    local data = self:LoadSettingData()
    if data == nil or (type(data) == "table" and table.isEmpty(data)) then
        self._settingData[SystemSettingConfig.RENDER_QUALITY] = GameWorld.GetAutoQualityLevel() or SystemSettingConfig.QUALITY_LOW
        self._settingData[SystemSettingConfig.PLAYER_NUM] = SystemSettingConfig.PLAYER_DEFAULT_NUM
        self._settingData[SystemSettingConfig.MUSIC_VOLUME] = SystemSettingConfig.MUSIC_DEFAULT_NUM / 100
        self._settingData[SystemSettingConfig.SOUND_VOLUME] = SystemSettingConfig.SOUND_DEFAULT_NUM / 100
        self._settingData[SystemSettingConfig.OTHER_PET] = SystemSettingConfig.HIDE_OTHER_PET_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_WING] = SystemSettingConfig.HIDE_OTHER_WING_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_HUNQI] = SystemSettingConfig.HIDE_OTHER_HUNQI_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_TITLE] = SystemSettingConfig.HIDE_OTHER_TITLE_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_SKILLFX] = SystemSettingConfig.HIDE_OTHER_SKILLFX_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_EQUIPFLOW] = SystemSettingConfig.HIDE_OTHER_EQUIPFLOW_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_MONSTER] = SystemSettingConfig.HIDE_OTHER_MONSTER_DEFAULT
        self._settingData[SystemSettingConfig.OTHER_FLOWERFX] = SystemSettingConfig.HIDE_OTHER_FLOWERFX_DEFAULT
        self._settingData[SystemSettingConfig.REALTIME_SHADOWS] = SystemSettingConfig.HIDE_REALTIME_SHADOWS_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_TEAMCONFIRM] = SystemSettingConfig.AUTO_TEAMCONFIRM_DEFAULT
        self._settingData[SystemSettingConfig.SAVE_POWER_MODE] = SystemSettingConfig.SAVE_POWER_MODE_DEFAULT
    else
        self._settingData = self:InitData(data)
    end
    self:SaveData()
    -- LoggerHelper.Error("设置数据===" .. PrintTable:TableToStr(self._settingData))
end

function BaseSystemSettingData:SaveData()
    local dataStr = GameWorld.json.encode(self._settingData)
    GameWorld.SaveCache(CacheConfig.SystemBaseSettingData, {dataStr}, false)
end

function BaseSystemSettingData:InitData(data)
    local settingData = {}
    settingData[SystemSettingConfig.RENDER_QUALITY]     = data[tostring(SystemSettingConfig.RENDER_QUALITY)]
    settingData[SystemSettingConfig.PLAYER_NUM]         = data[tostring(SystemSettingConfig.PLAYER_NUM)]
    settingData[SystemSettingConfig.MUSIC_VOLUME]       = data[tostring(SystemSettingConfig.MUSIC_VOLUME)]
    settingData[SystemSettingConfig.SOUND_VOLUME]       = data[tostring(SystemSettingConfig.SOUND_VOLUME)]
    settingData[SystemSettingConfig.OTHER_PET]          = data[tostring(SystemSettingConfig.OTHER_PET)]
    settingData[SystemSettingConfig.OTHER_WING]         = data[tostring(SystemSettingConfig.OTHER_WING)]
    settingData[SystemSettingConfig.OTHER_HUNQI]        = data[tostring(SystemSettingConfig.OTHER_HUNQI)]
    settingData[SystemSettingConfig.OTHER_TITLE]        = data[tostring(SystemSettingConfig.OTHER_TITLE)]
    settingData[SystemSettingConfig.OTHER_SKILLFX]      = data[tostring(SystemSettingConfig.OTHER_SKILLFX)]
    settingData[SystemSettingConfig.OTHER_EQUIPFLOW]    = data[tostring(SystemSettingConfig.OTHER_EQUIPFLOW)]
    settingData[SystemSettingConfig.OTHER_MONSTER]      = data[tostring(SystemSettingConfig.OTHER_MONSTER)]
    settingData[SystemSettingConfig.OTHER_FLOWERFX]     = data[tostring(SystemSettingConfig.OTHER_FLOWERFX)]
    settingData[SystemSettingConfig.REALTIME_SHADOWS]   = data[tostring(SystemSettingConfig.REALTIME_SHADOWS)]
    settingData[SystemSettingConfig.AUTO_TEAMCONFIRM]   = data[tostring(SystemSettingConfig.AUTO_TEAMCONFIRM)]
    settingData[SystemSettingConfig.SAVE_POWER_MODE]    = data[tostring(SystemSettingConfig.SAVE_POWER_MODE)]

    return settingData
end

