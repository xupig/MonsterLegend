local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local BbsData = Class.BbsData(ClassTypes.XObject)


function BbsData:__ctor__()
    self.discussionBossId = 0
    self.discussionCurPage = 0
    self.discussionMaxPage = 0
end

function BbsData:RefreshDiscussionInfo(data)
    if self.discussionBossId ~= data[1] then
        self.discussionCurPage = 0
        self.discussionBossId = data[1]
    end
    self.discussionMaxPage = data[3] or 0
    if data[2] > self.discussionMaxPage then
        return
    end
    if data[2] == 1 then
        self.discussionDetailInfo = data[4] or {}
    elseif data[2] == self.discussionCurPage + 1 then
        for k,v in ipairs(data[4]) do
            table.insert(self.discussionDetailInfo, v)
        end
    end
    if data[2] ~= self.discussionCurPage then
        self.discussionCurPage = data[2]
    end
end