local public_config = GameWorld.public_config
local PersonalBossDataHelper = GameDataHelper.PersonalBossDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3

local Max_Floor_Count = 7

local PersonalBossItemData = Class.PersonalBossItemData(ClassTypes.XObject)

function PersonalBossItemData:__ctor__(id)
    self._id = id
    self._type = 3
end

function PersonalBossItemData:SetDeadPos(x, y, z)
    self._deadPos = Vector3(x * 0.01, y * 0.01, z * 0.01)
end

function PersonalBossItemData:GetId()
    return self._id
end

function PersonalBossItemData:GetIsSelected()
    return self._isSelected
end

function PersonalBossItemData:SetIsSelected(isSelected)
    self._isSelected = isSelected
end

function PersonalBossItemData:GetSenceId()
    return PersonalBossDataHelper:GetSenceId(self._id)
end

function PersonalBossItemData:GetSenceName()
    local mapId = PersonalBossDataHelper:GetSenceId(self._id)
    return LanguageDataHelper.GetContent(MapDataHelper.GetChinese(mapId)) or ""
end

function PersonalBossItemData:GetLevelLimit()
    return PersonalBossDataHelper:GetLevelLimit(self._id)
end

function PersonalBossItemData:GetBossLevel()
    return PersonalBossDataHelper:GetBossLevel(self._id)
end

function PersonalBossItemData:GetBossName()
    return PersonalBossDataHelper:GetBossName(self._id)
end

function PersonalBossItemData:GetBossNameIcon()
    return PersonalBossDataHelper:GetBossNameIcon(self._id)
end

function PersonalBossItemData:GetBossIcon()
    return PersonalBossDataHelper:GetBossIcon(self._id)
end

function PersonalBossItemData:GetBossModel()
    local monsterId = PersonalBossDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetMonsterModel(monsterId)
end

function PersonalBossItemData:GetBossHeadIcon()
    local monsterId = PersonalBossDataHelper:GetBossMonsterId(self._id)
    return MonsterDataHelper.GetIcon(monsterId)
end

function PersonalBossItemData:GetBossDrops()
    local drops = PersonalBossDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    return items
end

function PersonalBossItemData:GetBossMustDrops()
    local drops = PersonalBossDataHelper:GetBossMustDrops(self._id)
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    return items
end

function PersonalBossItemData:GetBossAllDrops()
    local drops = PersonalBossDataHelper:GetBossMustDrops(self._id)
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, {v,1,1})
    end

    drops = PersonalBossDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    for i, v in ipairs(drops) do
        table.insert(items, {v,1,0})
    end

    return items
end

function PersonalBossItemData:GetType()
    return self._type
end

function PersonalBossItemData:GetLevelShow()
    return PersonalBossDataHelper:GetLevelShow(self._id)
end

function PersonalBossItemData:GetPeaceShow()
    return 0
end

local PersonalBossData = Class.PersonalBossData(ClassTypes.XObject)

function PersonalBossData:__ctor__()
    self._bossItems = {}
end

function PersonalBossData:GetAllBossInfo()
    if self._sortedBossItems == nil then
        self._sortedBossItems = {}
        local bossIds = PersonalBossDataHelper:GetAllId()
        for i, id in ipairs(bossIds) do
            local item = self:GetBossInfo(id)
            table.insert(self._sortedBossItems, item)
        end
    end
    return self._sortedBossItems
end

function PersonalBossData:GetBossInfo(id)
    if not self._bossItems[id] then
        self._bossItems[id] = PersonalBossItemData(id)
    end
    return self._bossItems[id]
end

function PersonalBossData:RefreshPersonalBossInfo(changeValue)

end

function PersonalBossData:GetRemainCount()
    local player = GameWorld.Player()
    local info = player.personal_boss_info or {}
    local maxCount = info[public_config.PERSONAL_BOSS_KEY_CAN_ENTER_NUM]
    local useCount = info[public_config.PERSONAL_BOSS_KEY_ENTER_NUM]
    return maxCount - useCount
end

function PersonalBossData:GetMaxCount()
    local player = GameWorld.Player()
    local info = player.personal_boss_info or {}
    return info[public_config.PERSONAL_BOSS_KEY_CAN_ENTER_NUM]
end

function PersonalBossData:GetLastEnterTime()
    local player = GameWorld.Player()
    local info = player.personal_boss_info or {}
    return info[public_config.PERSONAL_BOSS_KEY_LAST_ENTER_TIME] or 0
end