local public_config = GameWorld.public_config
local table = table

local MailData = Class.MailData(ClassTypes.XObject)
local Mail = Class.Mail(ClassTypes.XObject)
local CommonItemData = ClassTypes.CommonItemData

--MailList
function MailData:__ctor__(serverData,maillist)
    self._mailList = self:PackMailData(maillist)--self:PackMailList(serverData[public_config.MAIL_LIST_KEY_MAILS])
    self._curPage = serverData[public_config.MAIL_LIST_KEY_CUR_PAGE]
    self._totalPage = serverData[public_config.MAIL_LIST_KEY_TOTAL_PAGE]
    self._mailCnt = serverData[public_config.MAIL_LIST_KEY_MAIL_CNT]
    self._newMailCnt = serverData[public_config.MAIL_LIST_KEY_NEW_MAIL_CNT]
    self._ungetAttachmentMailCnt = serverData[public_config.MAIL_LIST_KEY_UNGET_ATTACHEMENT_MAIL_CNT]
end


function MailData:ResetMailData(serverData)

end

function MailData:PackMailData(listdata)
    self._mails = {}
    for k, v in ipairs(listdata) do
        self:PackMailList(v)
    end
    return self._mails
end


--将网络数据打包,缺少对附件参数，拓展参数的处理
function MailData:PackMailList(allmails)
    if allmails==nil or table.isEmpty(allmails) then
        return nil
    end

    for k, v in ipairs(allmails) do
        table.insert(self._mails,Mail(v))
    end
end


-- function MailData:GetMailList(mails)
--     local d={}
--     for k, v in pairs(mails) do
--         table.insert(d, Mail(v))
--     end
--     return d
-- end

function MailData:GetMails()
    return self._mailList
end

function MailData:GetCurPage()
    return self._curPage or 1
end

function MailData:GetTotalPage()
    return self._totalPage or 0
end

function MailData:GetMailCnt()
    return self._mailCnt or 0
end

function MailData:SetMailCnt(value)
    self._mailCnt = value
end

function MailData:GetNewMailCnt()
    return self._newMailCnt or 0
end

function MailData:GetUngetAttachmentMailCnt()
    return self._ungetAttachmentMailCnt or 0
end

function Mail:__ctor__(mailData)
    self._dbid = mailData[public_config.MAIL_INFO_KEY_MAIL_DBID]          --邮件的dbid
    self._mail_type = mailData[public_config.MAIL_INFO_KEY_MAIL_TYPE]          --邮件的类型，0:个人发送邮件，1：系统发送邮件
    self._mail_id = mailData[public_config.MAIL_INFO_KEY_MAIL_ID]              --系统邮件文本类型，0:文本邮件，mail_id：系统发送邮件
    self._info_title = mailData[public_config.MAIL_INFO_KEY_TITLE]                --邮件标题
    self._info_text = mailData[public_config.MAIL_INFO_KEY_TEXT]                    --邮件的内容
    self._info_from = mailData[public_config.MAIL_INFO_KEY_FROM]                    --发件人的姓名
    self._info_time = mailData[public_config.MAIL_INFO_KEY_TIME]                 --发件时间
    self._info_reason = mailData[public_config.MAIL_INFO_KEY_REASON]                --邮件系统来源
    self._info_state = mailData[public_config.MAIL_INFO_KEY_STATE]                  --邮件状态
    self._info_extern = mailData[public_config.MAIL_INFO_KEY_EXTERN_INFO]      --邮件扩展参数。mail_id类型需要用的的格式化字符串。
    -- if self._mail_id==1104 then
    --     LoggerHelper.Log(self._info_extern)
    --     LoggerHelper.Log(self._info_text)
    -- end
  
    self._info_public_mail_dbid = mailData[public_config.MAIL_INFO_KEY_PUBLIC_MAIL_DBID]--公共邮件id。1为从公共邮件转过来。
    self:HandleAttachment(GameWorld.CSLuaStringToLuaTable(mailData[public_config.MAIL_INFO_KEY_ATTACHEMENT]), "Mail")
end

function Mail:GetDBID()
    return self._dbid or -1
end

function Mail:GetMailType()
    return self._mail_type or -1
end

function Mail:GetMailId()
    return self._mail_id or -1
end

function Mail:GetInfoTitle()
    return self._info_title or "nil"
end

function Mail:GetInfoText()
    return self._info_text or "nil"
end

function Mail:GetInfoFrom()
    return self._info_from or "nil"
end

function Mail:GetInfoTime()
    return self._info_time or -1
end

--邮件附件.列表，里面元素每个和道具的结构一致。
function Mail:HandleAttachment(itemDatas)
   self._info_attachement = {}
   for k,v in pairs(itemDatas) do
       local item = CommonItemData.CreateItemData(v)
       table.insert(self._info_attachement,item)
   end
end
function Mail:GetInfoAttachement()
    return self._info_attachement
end

function Mail:GetInfoReason()
    return self._info_reason or "nil"
end

function Mail:GetInfoState()
    return self._info_state or -1
end

function Mail:SetInfoState(state)
    self._info_state = state
end

function Mail:ChangeMailStatus(from, to)
    --0->2/1->3、4/3->4
    if from == public_config.MAIL_STATE_NONE then
        self._info_state = public_config.MAIL_STATE_READ
    elseif from == public_config.MAIL_STATE_HAVE then
        self._info_state = to
    elseif from == public_config.MAIL_STATE_HERE then
        self._info_state = public_config.MAIL_STATE_RECE
    else
        return false
    end
    return true
end

function Mail:GetInfoExtern()
    return self._info_extern or {}
end

function Mail:GetInfoPublicMailDbid()
    return self._info_public_mail_dbid or -1
end