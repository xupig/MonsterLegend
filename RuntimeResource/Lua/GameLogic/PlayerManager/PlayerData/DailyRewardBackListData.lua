local ActivityDailyDataHelper = GameDataHelper.ActivityDailyDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DailyConfig = GameConfig.DailyConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper

local function SortByRank(a,b)
    return tonumber(ActivityDailyDataHelper:GetRewardBackRank(a:GetId())) > tonumber(ActivityDailyDataHelper:GetRewardBackRank(b:GetId()))
end

local DailyRewardBackData = Class.DailyRewardBackData(ClassTypes.XObject)

function DailyRewardBackData:__ctor__(activityId, serverData)
    self._id = activityId
    self._commonTimes = serverData[1]
    self._exTimes = serverData[2]
    self._exRewards = {}
    self._commonRewards = {}

    for k,v in pairs(serverData[3]) do
        local data = {}
        data[1] = k
        data[2] = v
        table.insert(self._exRewards, data)
    end
    
    for k,v in pairs(serverData[4]) do
        local data = {}
        data[1] = k
        data[2] = v
        table.insert(self._commonRewards, data)
    end
end

function DailyRewardBackData:GetId()
    return self._id
end

function DailyRewardBackData:GetCommonTimes()
    return self._commonTimes
end

function DailyRewardBackData:GetExTimes()
    return self._exTimes
end

function DailyRewardBackData:GetCommonRewardData()
    return self._commonRewards
end

function DailyRewardBackData:GetExRewardData()
    return self._exRewards
end

function DailyRewardBackData:SetType(t)
    self._backType = t
end

function DailyRewardBackData:GetType()
    return self._backType
end

function DailyRewardBackData:GetReqType()
    return self._backType + 1
end

function DailyRewardBackData:GetName()
    return LanguageDataHelper.CreateContent(ActivityDailyDataHelper:GetName(self._id))
end

function DailyRewardBackData:GetDesc()    
    local exTimes =  self:GetExTimes()
    if self._backType == DailyConfig.REWARD_BACK_TYPE_COMMON then
        exTimes = 0
    end

    if exTimes == 0 then
        return LanguageDataHelper.CreateContent(52087, {
            ["0"] = self:GetCommonTimes()
        })
    end

    return LanguageDataHelper.CreateContent(52079, {
        ["0"] = self:GetCommonTimes(),
        ["1"] = exTimes
    })
end

function DailyRewardBackData:GetBuyTotalText(num)
    if self:GetType() == DailyConfig.REWARD_BACK_TYPE_EX then--钻石找回
        local exTimes = self:GetExTimes()
        local commonTimes = self:GetCommonTimes()

        local exCommonPrice = self:GetExBaseCost()--基础价格
        local exPrice = self:GetExCost()--额外次数价格
        local totalPrice

        -- LoggerHelper.Error("commonTimes ====" .. tostring(commonTimes))
        -- LoggerHelper.Error("exTimes ====" .. tostring(exTimes))
        -- LoggerHelper.Error("基础价格 ====" .. tostring(exCommonPrice))
        -- LoggerHelper.Error("额外次数价格 ====" .. tostring(exPrice))
        -- LoggerHelper.Error("基础总价格 ====" .. tostring(commonTimes * exCommonPrice))


        if num > commonTimes then
            totalPrice = commonTimes * exCommonPrice + (num - commonTimes) * exPrice
        else
            totalPrice = num * exCommonPrice
        end
        -- LoggerHelper.Error("num ====" .. tostring(num))
        -- LoggerHelper.Error("totalPrice ====" .. tostring(totalPrice))

        return LanguageDataHelper.CreateContent(52085, {
            ["0"] = totalPrice,
            ["1"] = exCommonPrice,
            ["2"] = exPrice,
        })
    end
    local price = self:GetCommonCost()
    return LanguageDataHelper.CreateContent(52088, {
        ["0"] = price * num
    })
end

--获取金币找回消耗,数量、货币id
function DailyRewardBackData:GetCommonCost()
    local data = ActivityDailyDataHelper:GetNormalBackCost(self._id)
    for k,v in pairs(data) do
        return v, ItemDataHelper.GetIcon(k)
    end
    return 0,0
end
    
--获取钻石找回消耗(基础次数),数量、货币id
function DailyRewardBackData:GetExBaseCost()
    local data = ActivityDailyDataHelper:GetPerfectBackCost(self._id)
    for k,v in pairs(data) do
        return v, ItemDataHelper.GetIcon(k)
    end
    return 0,0
end

--获取钻石找回消耗(额外次数),数量、货币id
function DailyRewardBackData:GetExCost()
    local data = ActivityDailyDataHelper:GetPerfectBackExtraCost(self._id)
    for k,v in pairs(data) do
        return v, ItemDataHelper.GetIcon(k)
    end
    return 0,0
end
    
local DailyRewardBackListData = Class.DailyRewardBackListData(ClassTypes.XObject)
function DailyRewardBackListData:__ctor__()
    self._listData = {}
    self._commonTimes = 0
    self._exTimes = 0
end

function DailyRewardBackListData:ResetData(serverData)
    --activity_id  = 剩余可找回的基础次数, 剩余可找回的额外次数, 钻石找回单次奖励，金币找回单次奖励
    -- "11" = {"1" = 4,"2" = 0,"3" = {"1" = 20000},"4" = {"1" = 10000}},
    -- "1" = {"1" = 40,"2" = 0,"3" = {"205" = 1,"1" = 5000},"4" = {"1" = 10000}},
    -- "4" = {"1" = 6,"2" = 0,"3" = {"205" = 1,"1" = 5000},"4" = {"1" = 10000}}}}
    self._listData = {}
    self._commonTimes = 0
    self._exTimes = 0
    for k,v in pairs(serverData) do
        local data = DailyRewardBackData(k, v)
        self._commonTimes = self._commonTimes + data:GetCommonTimes()
        self._exTimes = self._exTimes + data:GetExTimes()
        table.insert(self._listData, data)
    end
end

--钻石找回
function DailyRewardBackListData:IsCanExFindBack()
    -- LoggerHelper.Error("_exTimes ====" .. tostring(self._exTimes))
    -- LoggerHelper.Error("_commonTimes ====" .. tostring(self._commonTimes))
    return (self._commonTimes + self._exTimes) ~= 0
end

--金币找回
function DailyRewardBackListData:IsCanCommonFindBack()
    return self._commonTimes ~= 0
end

function DailyRewardBackListData:IsCanFindBack()
    return (self._commonTimes + self._exTimes) ~= 0
end

--获取金币找回列表数据
function DailyRewardBackListData:GetCommonRewardListDatas()
    local datas = {}
    local notCompleteList = {}
    local completeList = {}

    for k,v in pairs(self._listData) do
        v:SetType(DailyConfig.REWARD_BACK_TYPE_COMMON)
        if v:GetCommonTimes() == 0 then
            table.insert(completeList, v) 
        else
            table.insert(notCompleteList, v)
        end
    end

    if table.isEmpty(notCompleteList) then
        table.sort(completeList, SortByRank)
        return completeList
    elseif table.isEmpty(completeList) then
        table.sort(notCompleteList, SortByRank)
        return notCompleteList
    else
        table.sort(completeList, SortByRank)
        table.sort(notCompleteList, SortByRank)

        for i,v in ipairs(notCompleteList) do
            table.insert(datas, v)
        end

        for i,v in ipairs(completeList) do
            table.insert(datas, v)
        end
    end
    return datas
end

--获取钻石找回列表数据
function DailyRewardBackListData:GetExRewardListDatas()
    local datas = {}
    local notCompleteList = {}
    local completeList = {}

    for k,v in pairs(self._listData) do
        v:SetType(DailyConfig.REWARD_BACK_TYPE_EX)
        if v:GetExTimes() == 0 and v:GetCommonTimes() == 0 then
            table.insert(completeList, v) 
        else
            table.insert(notCompleteList, v)
        end
    end

    if table.isEmpty(notCompleteList) then
        table.sort(completeList, SortByRank)
        return completeList
    elseif table.isEmpty(completeList) then
        table.sort(notCompleteList, SortByRank)
        return notCompleteList
    else
        table.sort(completeList, SortByRank)
        table.sort(notCompleteList, SortByRank)

        for i,v in ipairs(notCompleteList) do
            table.insert(datas, v)
        end

        for i,v in ipairs(completeList) do
            v:SetType(DailyConfig.REWARD_BACK_TYPE_EX)
            table.insert(datas, v)
        end
    end
    return datas
end