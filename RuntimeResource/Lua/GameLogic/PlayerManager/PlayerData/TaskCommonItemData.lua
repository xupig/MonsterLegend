local TaskCommonItemData = Class.TaskCommonItemData(ClassTypes.XObject)

require "PlayerManager/PlayerData/EventData"
local EventData = ClassTypes.EventData

local public_config = GameWorld.public_config

function TaskCommonItemData:__ctor__()
end

function TaskCommonItemData:GetTaskItemType()
    return self._taskItemType
end

function TaskCommonItemData:GetTargetsInfo()
    -- LoggerHelper.Error("state ======" .. tostring(self:GetState()))
    if self:GetState() == public_config.TASK_STATE_COMPLETED then
        self._targetsData = EventData()
        self._targetsData:SetTalkToSomeOneEventData(self:GetCommitNPC())
    elseif self:GetState() == public_config.TASK_STATE_ACCEPTABLE then
        -- LoggerHelper.Error("可接任务")
        self._targetsData = EventData()
        self._targetsData:SetTalkToSomeOneEventData(self:GetAcceptNPC())
    else
        if self._targetsData == nil then
            self._targetsData = EventData()
            -- LoggerHelper.Log("TaskItemData:GetTargetsInfo: " .. PrintTable:TableToStr(TaskDataHelper:GetTargetsInfo(self._id)))
            self._targetsData:UpdateSortedEventData(self:GetTargetsInfoData())
        end
    end
    return self._targetsData:GetSortedEvents()
end

function TaskCommonItemData:GetMyRewards()
end

--由子类实现
function TaskCommonItemData:GetTargetsInfoData()
end

function TaskCommonItemData:GetTargetsCurTimes()
end

function TaskCommonItemData:GetState()
end

function TaskCommonItemData:GetAcceptNPC()
end

function TaskCommonItemData:GetCommitNPC()
end

function TaskCommonItemData:GetAcceptDialog()
end

function TaskCommonItemData:GetAcceptTaskCallBack()
end

function TaskCommonItemData:GetCommitTaskCallBack()
end

function TaskCommonItemData:GetCommitDialogArgs()
end

function TaskCommonItemData:IsAutoTask()
end

function TaskCommonItemData:GetLittleShoesOpen()
    return false
end

function TaskCommonItemData:IsOpen()
end

function TaskCommonItemData:GetTaskType()
end