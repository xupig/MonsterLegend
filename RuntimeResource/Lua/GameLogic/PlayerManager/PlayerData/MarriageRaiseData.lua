    ------------------------MarriageRaiseData-------------------------
local MarriageRaiseData = Class.MarriageRaiseData(ClassTypes.XObject)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MarryRingDataHelper = GameDataHelper.MarryRingDataHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local XMLManager = GameManager.XMLManager
-- local MarriageManager = PlayerManager.MarriageManager
-- local FriendData = PlayerManager.PlayerDataManager.friendData
local MarriageRingData = Class.MarriageRingData(ClassTypes.XObject)
                        ------ 婚戒-------
function MarriageRingData:__ctor__(id)
    self._id = id
end

function MarriageRingData:GetExp()
    return MarryRingDataHelper:GetExp(self._id)
end

function MarriageRingData:GetLevel()
    return MarryRingDataHelper:GetLevel(self._id)
end

function MarriageRingData:GetAttriBase()
    return MarryRingDataHelper:GetAttriBase(self._id)
end

function MarriageRingData:GetAttriMarry()
    return MarryRingDataHelper:GetAttriMarry(self._id)
end

function MarriageRaiseData:__ctor__()

end
-- Ring的属性
function MarriageRaiseData:GetMarrRingAtt()
    if self:IsPutOnRing() then
        local id = self:GetIdByLevel(GameWorld.Player().marry_ring_level)
        return MarriageRingData(id)
    end
end

function MarriageRaiseData:GetIdByLevel(level)
    local ids = MarryRingDataHelper:GetAllId()
    for _,id in ipairs(ids) do
        if level==MarryRingDataHelper:GetLevel(id) then
            return id
        end
    end
end
-- 是否已戴戒指
function MarriageRaiseData:IsPutOnRing()
    if GameWorld.Player().pkg_loaded_equip[public_config.PKG_LOADED_EQUIP_INDEX_RING] then
        return true
    end
    return false
end

                    ------ 仙娃-------
-- 仙娃同步信息
function MarriageRaiseData:GetMarrayBadyInfo()
    return GameWorld.Player().marry_pet_info or {}
end
-- 是否已经激活
function MarriageRaiseData:IsActiveBady(id)
    if self:GetMarrayBadyInfo()[id]~=nil then return true end
    return false
end
-- 技能是不是激活
function MarriageRaiseData:IsSkillActive(conditionLevel,BadyId)
    if not self:IsActiveBady(BadyId) then
        return false
    end
    local level = self:GetMarrayBadyInfo()[BadyId][public_config.MARRY_PET_GRADE]
    if conditionLevel then
        if conditionLevel<=level then 
            return true
        end
        return false
    end
    return true
end

                    ------ 宝匣-------
-- 是否有宝匣
function MarriageRaiseData:IsHaveBox()
    local x = GameWorld.Player().marry_box_info or {}
    return x[public_config.MARRY_BOX_FLAG]==1                  
end
-- 领到第几次
function MarriageRaiseData:GetBoxTimes()
    local x = GameWorld.Player().marry_box_info or {}
    return x[public_config.MARRY_BOX_DAILY_REFUND_STEP]                  
end
-- 今天是否领了
function MarriageRaiseData:IsGetToday()
    local x = GameWorld.Player().marry_box_info or {}
    return x[public_config.MARRY_BOX_DAILY_REFUND_FLAG]==1                  
end
-- 买宝匣直接就能领取的那些奖励是否领了
function MarriageRaiseData:IsImmediateGet()
    local x = GameWorld.Player().marry_box_info or {}
    return x[public_config.MARRY_BOX_REFUND_FLAG]==1                  
end


local MarriageTieleItem = Class.MarriageTieleItem(ClassTypes.XObject)
                    ------信息-------
-- 称号信息
function MarriageRaiseData:GetMadeTitles()
    local items = {}
    local d = self:GetcfgTitles()
    for i=1,#d do
        table.insert(items,MarriageTieleItem(d[i]))
    end
    table.sort(items,function(a,b)
        if a:GetTitleId()<b:GetTitleId() then
            return true
        else
            return false
        end
    end)
    return items
end

function MarriageRaiseData:GetcfgTitles()
    if self.cfgitems then 
        return self.cfgitems 
    end
    self.cfgitems = {}
    local keys = XMLManager.title:Keys()
    for i=1,#keys do
        local data = TitleDataHelper.GetData(keys[i])
        if data.is_love==1 then
            table.insert(self.cfgitems,data)
        end
    end
    return self.cfgitems
end

function MarriageRaiseData:GetMadeTitleItemByData(titleid)
    local data = TitleDataHelper.GetData(titleid)
    if data and data.is_love==1 then
        return MarriageTieleItem(data)
    end
    return nil
end

MarriageRaiseData.TitleEvenids = {
    [5059]=59155,
    [5060]=59145,
    [5061]=59145,
    [5062]=59145,
    [5063]=59145,
    [5064]=59146,
}
function MarriageTieleItem:__ctor__(data)
    self.Itemdata = data
end

function MarriageTieleItem:GetIcon()
    return self.Itemdata.icon
end

function MarriageTieleItem:GetName()
    return self.Itemdata.name
end

function MarriageTieleItem:GetDescribe()
    return self.Itemdata.describe
end

function MarriageTieleItem:GetTargets()
    return self.Itemdata.targets
end

function MarriageTieleItem:GetTitleId()
    return self.Itemdata.id
end


function MarriageTieleItem:GetCondition()
    local tar = self:GetTargets()
    local items={}
    for k, v in pairs(tar) do
        local d = {}
        d.contentid = MarriageRaiseData.TitleEvenids[k]
        d.conditionvalue=v
        d.key = k
        table.insert(items,d)
    end
    return items
end




