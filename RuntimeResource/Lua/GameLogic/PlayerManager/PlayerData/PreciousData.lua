local GUIManager = GameManager.GUIManager
local EventDispatcher = EventDispatcher
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

local PreciousData = Class.PreciousData(ClassTypes.XObject)

function PreciousData:__ctor__()
    self._runePre = {}
    self._runePkg = {}
    self._runefind = {}
    self._runefindGet = {}
end

function PreciousData:UpdateRunePre(data)
    self._runePre = data
    EventDispatcher:TriggerEvent(GameEvents.RunePre)
end

function PreciousData:GetRunePre()
    return self._runePre
end

function PreciousData:UpdateRunePkg(data)
    self._runePkg = data
    EventDispatcher:TriggerEvent(GameEvents.RunePkg)
end

function PreciousData:UpdateRuneFind(data)
    self._runefind =  data
end

function PreciousData:GetRuneFind()
    return self._runefind 
end

function PreciousData:GetRunePkg()
    return self._runePkg
end

function PreciousData:UpdateRuneFindGet(data)
    self._runefindGet = data
    EventDispatcher:TriggerEvent(GameEvents.RuneFindGet)
end

function PreciousData:GetRuneFindGet()
    return self._runefindGet
end



