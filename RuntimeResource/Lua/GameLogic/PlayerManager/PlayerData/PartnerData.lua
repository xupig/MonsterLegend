local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager

local PartnerData = Class.PartnerData(ClassTypes.XObject)

function PartnerData:__ctor__()
    self.petUsedInfo = {}
end

------------------------------------宠物数据----------------------------------------------------
function PartnerData:RefreshPetInfo(changeValue)
    self.petUsedInfo = GameWorld.Player().pet_info[public_config.PET_KEY_USE] or {}
    self.petAttriInfo = GameWorld.Player().pet_info[public_config.PET_KEY_ATTRI] or {}
    self.petAttriPer = GameWorld.Player().pet_info[public_config.PET_KEY_ATTRI_TOTAL_PER] or 0
    if changeValue == nil then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pet_Info)
end

--返回宠物神羽道具使用次数
function PartnerData:GetPetItemUsedInfo(itemId)
    if self.petUsedInfo[itemId] == nil then
        return 0
    end
    return self.petUsedInfo[itemId]
end

function PartnerData:GetPetAttris()
    local roleAttris = {}
    local player = GameWorld.Player()
    local attriData = PartnerDataHelper.GetPetAttri(player.pet_grade, player.pet_star)
    BaseUtil.UnionTable(roleAttris, attriData)
    attriData = PartnerDataHelper.GetPetLevelAtrri(player.pet_level)
    BaseUtil.UnionTable(roleAttris, attriData)
    BaseUtil.UnionTable(roleAttris, self.petAttriInfo)

    local totalPer = self.petAttriPer

    if totalPer > 0 then
        for k,v in pairs(roleAttris) do
            v = v + math.ceil(v * totalPer/100)
        end
    end
    return roleAttris
end


------------------------------------坐骑数据----------------------------------------------------
function PartnerData:RefreshRideInfo(changeValue)
    self.rideUsedInfo = GameWorld.Player().horse_info[public_config.HORSE_KEY_USE] or {}
    self.rideAttriInfo = GameWorld.Player().horse_info[public_config.HORSE_KEY_ATTRI] or {}
    self.rideAttriPer = GameWorld.Player().horse_info[public_config.HORSE_KEY_ATTRI_TOTAL_PER] or 0
    if changeValue == nil then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Ride_Info)
end

--返回坐骑神羽道具使用次数
function PartnerData:GetRideItemUsedInfo(itemId)
    if self.rideUsedInfo[itemId] == nil then
        return 0
    end
    return self.rideUsedInfo[itemId]
end

function PartnerData:GetRideAttris()
    local roleAttris = {}
    local player = GameWorld.Player()
    local attriData = PartnerDataHelper.GetRideAttri(player.horse_grade, player.horse_star)
    BaseUtil.UnionTable(roleAttris, attriData)
    BaseUtil.UnionTable(roleAttris, self.rideAttriInfo)

    local totalPer = self.rideAttriPer

    if totalPer > 0 then
        for k,v in pairs(roleAttris) do
            v = v + math.ceil(v * totalPer/100)
        end
    end
    return roleAttris
end

