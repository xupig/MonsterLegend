--开服活动
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

local RANK_KEY_CROSS_UUID = public_config.RANK_KEY_CROSS_UUID  --用cross_uuid记录，这个也可以得到dbid，兼容假如有一个跨服排行榜
local RANK_KEY_LEVEL = public_config.RANK_KEY_LEVEL            --玩家等级
local RANK_KEY_EXP = public_config.RANK_KEY_EXP                --玩家经验
local RANK_KEY_COMBAT_VALUE = public_config.RANK_KEY_COMBAT_VALUE     --战斗力 
local RANK_KEY_NAME = public_config.RANK_KEY_NAME                   --玩家名字
local RANK_KEY_PET_GRADE = public_config.RANK_KEY_PET_GRADE          --宠物阶级
local RANK_KEY_PET_STAR = public_config.RANK_KEY_PET_STAR          --宠物星级
local RANK_KEY_HORSE_GRADE = public_config.RANK_KEY_HORSE_GRADE          --坐骑阶级
local RANK_KEY_HORSE_STAR = public_config.RANK_KEY_HORSE_STAR          --坐骑星级
local RANK_KEY_GEM_LEVEL = public_config.RANK_KEY_GEM_LEVEL          --宝石总等级
local RANK_KEY_DAY_CHARGE = public_config.RANK_KEY_DAY_CHARGE          --每日充值
local RANK_KEY_RANK_VALUE = public_config.RANK_KEY_RANK_VALUE          --名次


local MyInfoItemData = Class.MyInfoItemData(ClassTypes.XObject)

function MyInfoItemData:__ctor__(serverData)
    self._serverData = serverData
end

function MyInfoItemData:GetName()
    return self._serverData[RANK_KEY_NAME]
end

function MyInfoItemData:GetCrossDbid()
    return self._serverData[RANK_KEY_CROSS_UUID]
end

function MyInfoItemData:GetLevel()
    return self._serverData[RANK_KEY_LEVEL]
end

function MyInfoItemData:GetExp()
    return self._serverData[RANK_KEY_EXP]
end

function MyInfoItemData:GetComBatValue()
    return self._serverData[RANK_KEY_COMBAT_VALUE]
end

function MyInfoItemData:GetPetGrade()
    return self._serverData[RANK_KEY_PET_GRADE]
end

function MyInfoItemData:GetPetStar()
    return self._serverData[RANK_KEY_PET_STAR]
end

function MyInfoItemData:GetHorseGrade()
    return self._serverData[RANK_KEY_HORSE_GRADE]
end

function MyInfoItemData:GetHorseStar()
    return self._serverData[RANK_KEY_HORSE_STAR]
end

function MyInfoItemData:GetGemLevel()
    return self._serverData[RANK_KEY_GEM_LEVEL]
end

function MyInfoItemData:GetDayCharge()
    return self._serverData[RANK_KEY_DAY_CHARGE]
end

function MyInfoItemData:GetRankValue()
    return self._serverData[RANK_KEY_RANK_VALUE]
end




local ServiceActivityData = Class.ServiceActivityData(ClassTypes.XObject)

function ServiceActivityData:__ctor__(svrData)
    self._secIndex = -1
    self._sendInfo = {}
    self._reward_grade = -1
    self._isGetReward = -1
    self._loveList = {}
    self._progress = {}
    self._allSvrNumData = {}

    self._OpenRankRedDic = {}
    self._sectStateInfo = {}
    self._sectStateNumInfo = {}
    self._sectTaskSectInfo = {}
    self._loveInfoResp = {}
end


function ServiceActivityData:UpdateRecommondListData(svrData)

end

function ServiceActivityData:GetOpenSeverGetInfoResp(svrData)
    --LoggerHelper.Log("self.GetOpenSeverGetInf")
    if svrData then
        --LoggerHelper.Log("self.GetOpenSeverGetInfoResp：" .. PrintTable:TableToStr(svrData))
        self._secIndex = svrData[1]
        self._sendInfo = MyInfoItemData(svrData[2][2])
        self._reward_grade = svrData[2][3]
        self._isGetReward = svrData[2][4]
        if self._reward_grade then

            if self._isGetReward == 1 then
            --有领取 已经领取
                self._OpenRankRedDic[self._secIndex] = 1
            elseif self._isGetReward == 0 then
            --有领取 未领取
                self._OpenRankRedDic[self._secIndex] = 0
            else

            end 
        else
            --未达成
            self._OpenRankRedDic[self._secIndex] = 2
        end
        
    end
end

function ServiceActivityData:GetOpenRankRedDic()
    if self._OpenRankRedDic then
        return self._OpenRankRedDic
    end
end

function ServiceActivityData:GetRewardStateSectResp(svrData)
    if svrData then
        self._sectStateInfo = svrData
    end
end

function ServiceActivityData:GetRewardInfoSectResp(svrData)
    if svrData then
        self._sectStateNumInfo = svrData
    end
end

function ServiceActivityData:GetLoveInfoResp(svrData)
    if svrData then
        self._loveInfoResp = svrData
    end
end

function ServiceActivityData:GetLoveInfoTable()
    return  self._loveInfoResp or {}
end

function ServiceActivityData:GetTaskInfoSectResp(svrData)
    if svrData then
        self._sectTaskSectInfo = svrData
    end
end

function ServiceActivityData:GetTaskInfoSectTable()
    return self._sectTaskSectInfo or {}
end

function ServiceActivityData:GetRewardStateSectTable()
    return self._sectStateInfo or {}
end

function ServiceActivityData:GetRewardInfoSectTable()
    return self._sectStateNumInfo or {}
end


function ServiceActivityData:GetRingProgressResp(svrData)
    if svrData then
        self._progress = svrData
    end
end

function ServiceActivityData:GetRingTargetRewardResp(svrData)
    if svrData then

    end
end

function ServiceActivityData:GetAllSerNumResq(svrData)
    if svrData then
        self._allSvrNumData = svrData
    end
end

function ServiceActivityData:GetAllSerNumData()
    return self._allSvrNumData or {}
end


function ServiceActivityData:GetProgressTable()
    if self._progress then
        return self._progress
    end
end



function ServiceActivityData:GetCurOpenType()
    if self._secIndex~=-1 then
        return self._secIndex
    end
end

function ServiceActivityData:SetCurOpenType(index)
     self._secIndex = index
end

function ServiceActivityData:GetMyInfoData()
    if self._sendInfo then
        return self._sendInfo
    end
end

function ServiceActivityData:GetRewardGrade()
    if self._reward_grade~=-1 then
        return self._reward_grade
    end
end
 
function ServiceActivityData:GetIsGetReward()
    if self._isGetReward~=-1 then
        return self._isGetReward or -1
    end
end

function ServiceActivityData:GetMarryRankGetInfoResp(svrData)
    if svrData then
        self._loveList = svrData
    end
end

function ServiceActivityData:GetLoveList()
    if self._loveList then
        return self._loveList
    end
end
