--好友系统
local public_config = GameWorld.public_config
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager


local FontStyleHelper = GameDataHelper.FontStyleHelper
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
-- local TipsManager = GameManager.TipsManager

local StringStyleUtil = GameUtil.StringStyleUtil

local FRIEND_INFO_KEY_NAME = public_config.FRIEND_INFO_KEY_NAME
local FRIEND_INFO_KEY_DBID = public_config.FRIEND_INFO_KEY_DBID
local FRIEND_INFO_KEY_GENDER = public_config.FRIEND_INFO_KEY_GENDER
local FRIEND_INFO_KEY_LEVEL = public_config.FRIEND_INFO_KEY_LEVEL
local FRIEND_INFO_KEY_VOCATION = public_config.FRIEND_INFO_KEY_VOCATION
local FRIEND_INFO_KEY_RACE = public_config.FRIEND_INFO_KEY_RACE
local FRIEND_INFO_KEY_FIGHT_FORCE = public_config.FRIEND_INFO_KEY_FIGHT_FORCE
local FRIEND_INFO_KEY_OFFLINE_TIME = public_config.FRIEND_INFO_KEY_OFFLINE_TIME
local FRIEND_INFO_KEY_ONLINE = public_config.FRIEND_INFO_KEY_ONLINE
local FRIEND_INFO_KEY_ONLINE_TIME = public_config.FRIEND_INFO_KEY_ONLINE_TIME
local FRIEND_INFO_KEY_VIP_LEVEL = public_config.FRIEND_INFO_KEY_VIP_LEVEL
local FRIEND_INFO_KEY_FRIENDLY_VAL = public_config.FRIEND_INFO_KEY_FRIENDLY_VAL
local FRIEND_INFO_KEY_UPDATE_TIME = public_config.FRIEND_INFO_KEY_UPDATE_TIME --最后一次互动时间
local FRIEND_INFO_KEY_NEW_CHAT_CNT = public_config.FRIEND_INFO_KEY_NEW_CHAT_CNT --未读消息条数
local FRIEND_INFO_KEY_MATE_NAME = public_config.FRIEND_INFO_KEY_MATE_NAME --配偶名字



local TYPE_FRIEND = public_config.FRIEND_LIST_TYPE_FRIEND
local TYPE_RECENT = public_config.FRIEND_LIST_TYPE_RECENT
local TYPE_TEAM = public_config.FRIEND_LIST_TYPE_TEAM
local TYPE_RECOMMEND = public_config.FRIEND_LIST_TYPE_RECOMMEND
local TYPE_APPLY = public_config.FRIEND_LIST_TYPE_APPLY

local DBID = public_config.FRIEND_INFO_KEY_DBID





local FriendItemData = Class.FriendItemData(ClassTypes.XObject)

function FriendItemData:__ctor__(serverData)
    self._serverData = serverData
end

function FriendItemData:GetName()
    return self._serverData[FRIEND_INFO_KEY_NAME]
end

function FriendItemData:GetDbid()
    return self._serverData[FRIEND_INFO_KEY_DBID]
end

function FriendItemData:GetSex()
    return self._serverData[FRIEND_INFO_KEY_GENDER]
end

function FriendItemData:GetLevelStr()
    if self._serverData[FRIEND_INFO_KEY_LEVEL] then
        return StringStyleUtil.GetLevelString(self._serverData[FRIEND_INFO_KEY_LEVEL])
    else
        return "Lv".."0"
    end
end

function FriendItemData:GetLevel()
    return self._serverData[FRIEND_INFO_KEY_LEVEL]
end

function FriendItemData:GetVocation()
    return self._serverData[FRIEND_INFO_KEY_VOCATION] or 0
end

function FriendItemData:GetRace()
    return self._serverData[FRIEND_INFO_KEY_RACE] or 0
end

function FriendItemData:GetFightForce()
    return self._serverData[FRIEND_INFO_KEY_FIGHT_FORCE] or 0
end

function FriendItemData:GetOfflineTime()
    return self._serverData[FRIEND_INFO_KEY_OFFLINE_TIME] or 0
end

function FriendItemData:GetIsOnline()
    return self._serverData[FRIEND_INFO_KEY_ONLINE]
end

function FriendItemData:SetIsOnline(isOnline)
     self._serverData[FRIEND_INFO_KEY_ONLINE] = isOnline
end

function FriendItemData:GetOnlineTime()
    return self._serverData[FRIEND_INFO_KEY_ONLINE_TIME] or 0
end

--最后一次互动时间
function FriendItemData:GetUpdateTime()
    return self._serverData[FRIEND_INFO_KEY_UPDATE_TIME] or 0
end

--最后一次互动时间
function FriendItemData:SetUpdateTime(time)
    self._serverData[FRIEND_INFO_KEY_UPDATE_TIME] = time
end

--未读消息条数
function FriendItemData:GetChatCnt()
    return self._serverData[FRIEND_INFO_KEY_NEW_CHAT_CNT]
end

--好友亲密度
function FriendItemData:GetFriendlyVal()
    return self._serverData[FRIEND_INFO_KEY_FRIENDLY_VAL] or 0
end

function FriendItemData:SetFriendlyVal(value)
    self._serverData[FRIEND_INFO_KEY_FRIENDLY_VAL] = value
end

--得到配偶姓名
function FriendItemData:GetMateName()
    return self._serverData[FRIEND_INFO_KEY_MATE_NAME] or 0
end

--得到VIP
function FriendItemData:GetVip()
    return self._serverData[FRIEND_INFO_KEY_VIP_LEVEL] or 0
end

--得到VIPStr
function FriendItemData:GetVipStr()
    local level = self._serverData[FRIEND_INFO_KEY_VIP_LEVEL] or 0
    return "VIP" ..level
end



--聊天未读消息
local FriendChatNumData = Class.FriendChatNumData(ClassTypes.XObject)

function FriendChatNumData:__ctor__(serverData)
    self._serverData = serverData
end

function FriendChatNumData:GetChatDbid()
    return self._serverData[1]
end

function FriendChatNumData:GetChatNum()
    return self._serverData[2]
end






local FriendData = Class.FriendData(ClassTypes.XObject)
local FriendChatListData = Class.FriendChatListData(ClassTypes.XObject)

function FriendData:__ctor__(svrData)
    self._applyList = {}--推荐列表
    self._recommondList = {}
    self._friendDictData = {}
    self._friendList = {}
    self._friendRecentList = {}
    self._curdbid = 0
    self._blackList = {}
    self._foeList = {}
    self._unReadOffineList = {}   --未读离线消息
    self._unReadList = {}   --未读全部消息
    self._curChatPerson = {}
    self._friendChatNumItem = {} --未读新消息
    self._chatOnceTips = {}
    self._curClickIndex = -1

    self._preTime = 0


    self.itemCache = {}
    self.itemCacheIndex = 1
end


--更新推荐列表数据
function FriendData:UpdateRecommondListData(svrData)
    --LoggerHelper.Log("data .." .. PrintTable:TableToStr(svrData))
    self._recommondList = {}
    if svrData then
        for i, item in pairs(svrData) do
            table.insert(self._recommondList, FriendItemData(item))
        end
    end
end

function FriendData:OrderByOnline(t)
    for i = 1, #t do
        for j = #t, i + 1, -1 do
            if t[j - 1]:GetOnlineTime() < t[j]:GetOnlineTime() then
                self:Swap(t, j, j - 1)
            end
        end
    end
    return t
end

function FriendData:OrderByOffine(t)
    for i = 1, #t do
        for j = #t, i + 1, -1 do
            if t[j - 1]:GetOfflineTime() < t[j]:GetOfflineTime() then
                self:Swap(t, j, j - 1)
            end
        end
    end
    return t
end

function FriendData:OrderByChatTime(t)
    for i = 1, #t do
        for j = #t, i + 1, -1 do
            
            if t[j - 1]:GetUpdateTime() < t[j]:GetUpdateTime() then
                self:Swap(t, j, j - 1)
            end
        end
    end
    return t
end

function FriendData:Swap(list,low,high)
	local temp = 0
	temp = list[low]
	list[low] = list[high]
	list[high] = temp
end

--更新推荐列表数据
function FriendData:SetSearchResultData(svrData)
    --LoggerHelper.Log("data .." .. PrintTable:TableToStr(svrData))
    self._recommondList = {}
    if svrData then
        table.insert(self._recommondList, FriendItemData(svrData))
    end
end



--更新申请面板
function FriendData:UpdateApplyListData(svrData)
    --LoggerHelper.Log("data .." .. PrintTable:TableToStr(svrData))
    self._applyList = {}
    if svrData then
        for i, item in pairs(svrData) do
            local friendItem = FriendItemData(item)
            
            if friendItem:GetDbid() then
                if self:GetDbidFromBlackList(friendItem:GetDbid()) or self:GetDbidFromFriendList(friendItem:GetDbid()) then
                else
                    table.insert(self._applyList, friendItem)
                end 
            end
        end
    end
end

--更新好友列表
function FriendData:UpdateFriendListData(svrData)
    self._friendList = {}
    self._friendList = self:orderListData(svrData)
end

--单个好友信息
function FriendData:GetFriendInfoByDbid(dbid)
    if dbid and self._friendList then
        for i, item in pairs(self._friendList) do
            --LoggerHelper.Error("单个好友信息"..PrintTable:TableToStr(item))
            if item:GetDbid() == tonumber(dbid) then 
                return item
            end
            --break
        end
    end
end


function FriendData:GetIsRecommondChange()
    if DateTimeUtil.GetServerTime()  - tonumber(self._preTime) > 10 then  
        self._preTime = DateTimeUtil.GetServerTime()
        return true
    else
        return false
    end
end






--更新黑名单列表
function FriendData:UpdateBlackListData(svrData)
    self._blackList = {}
    self._blackList = self:orderListData(svrData)
end

--更新仇敌列表
function FriendData:UpdateFoeListData(svrData)
    self._foeList = {}
    self._foeList = self:orderListData(svrData)
end

function FriendData:orderListData(svrData)
    local onLineFriend = {}
    local offineLineFriend = {}
    if svrData then
        for i, item in pairs(svrData) do
            local friendItem = FriendItemData(item)
            if friendItem:GetIsOnline()==1 then
                table.insert(onLineFriend, FriendItemData(item))
            else
                table.insert(offineLineFriend, FriendItemData(item))
            end
        end

        return self:AddOnlineOffineList(onLineFriend,offineLineFriend)

    end
end

function FriendData:orderListDataByChatTime(orderList)
    local onLineFriend = {}
    local offineLineFriend = {}
    if orderList then
        for i, item in pairs(orderList) do
            if item:GetIsOnline()==1 then
                table.insert(onLineFriend,item)
            else
                table.insert(offineLineFriend,item)
            end
        end

        return self:AddOnlineOffineList(onLineFriend,offineLineFriend)

    end
end

function FriendData:orderListDataByList(orderList)
    local onLineFriend = {}
    local offineLineFriend = {}
    if orderList then
        for i, item in pairs(orderList) do
            if item:GetIsOnline()==1 then
                table.insert(onLineFriend,item)
            else
                table.insert(offineLineFriend,item)
            end
        end

        return self:AddOnlineOffineList(onLineFriend,offineLineFriend)

    end
end

--在线离线排序列表
function FriendData:AddOnlineOffineList(onlineList,offineList)
    local orderList = {}
    onlineList=self:OrderByOnline(onlineList)
    for i,item in ipairs(onlineList) do
        table.insert(orderList, item)
    end
    offineList=self:OrderByOffine(offineList)
    for i,item in ipairs(offineList) do
        table.insert(orderList, item)
    end
    return orderList
end


--添加仇敌到仇敌列表
function FriendData:AddFoeForFoeList(svrData)
    if svrData then
        table.insert(self._foeList, FriendItemData(svrData))
    end
end

--从仇敌列表移除仇敌
function FriendData:RemoveFriendForFoeList(dbid)
    if dbid then
        for i, item in pairs(self._foeList) do
            if item:GetDbid() == dbid then
                self._foeList[i] = nil
                table.remove(self._foeList,i)
                break
            end 
        end
    end
end

--检测黑名单列表里面有没有这个还有
function FriendData:GetDbidFromBlackList(dbid)
    if dbid then
        for i, item in pairs(self._blackList) do
            if item:GetDbid() == tonumber(dbid) then
                return dbid
            end 
        end
    end
end

--检测好友列表里面有没有这个还有
function FriendData:GetDbidFromFriendList(dbid)
    if dbid then
        for i, item in pairs(self._friendList) do
            if item:GetDbid() == dbid then
                return dbid
            end 
        end
    end
end

--上线下线数据设置 1 0 
function FriendData:SetIsOnlineByDbid(dbid,isOnline)
    if dbid and self._friendList then
        for i, item in pairs(self._friendList) do
            if item:GetDbid() == dbid then
                if isOnline then
                    item:SetIsOnline(1)
                else
                    item:SetIsOnline(0)
                end
                break
            end 
        end
    end
end

function FriendData:SetUpdateTimeByDbid(dbid,time)
    if dbid and self._foeList then
        for i, item in pairs(self._foeList) do
            if item:GetDbid() == dbid then
                item:SetUpdateTime(time)
                break
            end 
        end
    end

    if dbid and self._friendList then
        for i, item in pairs(self._friendList) do
            if item:GetDbid() == dbid then
                item:SetUpdateTime(time)
                break
            end 
        end
    end
end


function FriendData:GetUpdateTimeFromFriendList(dbid)
    if dbid then
        for i, item in pairs(self._friendList) do
            if item:GetDbid() == dbid then
                return item:GetUpdateTime()
            end 
        end
    end
end

--检测仇敌列表里面有没有这个还有
function FriendData:GetDbidFromFoeList(dbid)
    if dbid then
        for i, item in pairs(self._foeList) do
            if item:GetDbid() == dbid then
                return dbid
            end 
        end
    end
end

--上线下线数据设置 1 0 
function FriendData:SetFriendlyByDbid(dbid,friendValue)
    --LoggerHelper.Error("FriendData:SetFriendlyByDbidXXX"..friendValue)
    if dbid and self._friendList then
        for i, item in pairs(self._friendList) do
            --LoggerHelper.Error("FriendData:SetFriendlyByDbidSSS"..item:GetDbid())
            if item:GetDbid() == dbid then
                --LoggerHelper.Error("FriendData:SetFriendlyByDbid"..friendValue)
                item:SetFriendlyVal(friendValue)
                break
            end 
        end
    end
end

-- 处理物品
function FriendData:HandleItem(textStr,liststr)
    if liststr ~= "" then
        local itemSerData = PrintTable:StringToTable(tostring(liststr))
        local cacheIndex = self.itemCacheIndex
        self.itemCache[cacheIndex] = itemSerData
        for w in string.gmatch(textStr, "&&(%d+)&&") do
            local index = tonumber(w)
            local itemText = self:ItemText(index,cacheIndex,itemSerData)

            textStr = string.gsub(textStr, '&&' .. index .. '&&', itemText)
        end
        self.itemCacheIndex = cacheIndex+1
    end
    return textStr
end

--匹配物品数据
function FriendData:ItemText(index,cacheIndex,itemSerData)
    if itemSerData == nil then
        return '&&' .. index .. '&&'
    end
    
    local sendText = ''
    if itemSerData[index] then
        local arg = {}
        local itemId = itemSerData[index][1][public_config.ITEM_KEY_ID]
        local quality = ItemDataHelper.GetQuality(itemId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local color = FontStyleHelper.GetFontStyleData(colorId).color
        arg["0"] =  ChatConfig.LinkTypesMap.Item..","..index..","..cacheIndex
        arg["1"] =  color
        arg["2"] = '['..ItemDataHelper.GetItemName(tonumber(itemId))..']'    
        sendText = LanguageDataHelper.CreateContent(80532,arg)
    end
   
    return sendText
end

--self._curClickIndex
function FriendData:GetCurClickIndex()
    return self._curClickIndex
end

function FriendData:SetCurClickIndex(fIndex)
    self._curClickIndex = fIndex+1
end

--登陆设置离线未读消息
function FriendData:SetUnReadOffineList(svrData)
    self._unReadOffineList = svrData
end

--获取离线未读消息
function FriendData:GetUnReadOffineList(svrData)
    return self._unReadOffineList
end

--获取离线未读消息
function FriendData:ClearUnReadOffineByDbid(dbid)
    if self._unReadOffineList[dbid] then
        self._unReadOffineList[dbid] = nil
    end
end



function FriendData:SetUnReadForList(needlist)
    for i, item in pairs(needlist) do
        if item:GetDbid() then
            self._unReadList[item:GetDbid()] = 0
            if self._friendChatNumItem[item:GetDbid()] and self._unReadOffineList[item:GetDbid()] then
                if self:GetTipChatListData(item:GetDbid()) then
                    self._unReadList[item:GetDbid()] = self._friendChatNumItem[item:GetDbid()] + self._unReadOffineList[item:GetDbid()]                   
                    --LoggerHelper.Error("self._friendChatNumItem[item:GetDbid()]"..self._friendChatNumItem[item:GetDbid()])
                    --self._unReadList[item:GetDbid()] = self._friendChatNumItem[item:GetDbid()] + self._unReadOffineList[item:GetDbid()]+1
                else
                    --LoggerHelper.Error("self._friendChatNumItem[item:GetDbid()]"..self._friendChatNumItem[item:GetDbid()])
                    self._unReadList[item:GetDbid()] = self._friendChatNumItem[item:GetDbid()] + self._unReadOffineList[item:GetDbid()]
                end    
            elseif self._friendChatNumItem[item:GetDbid()] and not self._unReadOffineList[item:GetDbid()] then
                if self:GetTipChatListData(item:GetDbid()) then
                    self._unReadList[item:GetDbid()] = self._friendChatNumItem[item:GetDbid()]
                else
                    self._unReadList[item:GetDbid()] = self._friendChatNumItem[item:GetDbid()]
                end
            elseif self._unReadOffineList[item:GetDbid()] and not self._friendChatNumItem[item:GetDbid()] then
                if self:GetTipChatListData(item:GetDbid()) then
                    self._unReadList[item:GetDbid()] = self._unReadOffineList[item:GetDbid()]
                else
                    self._unReadList[item:GetDbid()] = self._unReadOffineList[item:GetDbid()]
                end
      
            else
                if self:GetTipChatListData(item:GetDbid()) then
                    self._unReadList[item:GetDbid()] = 0
                else
                    self._unReadList[item:GetDbid()] = 0
                end
            end

            -- if self._friendChatNumItem[item:GetDbid()] then
            --     LoggerHelper.Error("self._friendChatNumItem[item:GetDbid()]:"..self._friendChatNumItem[item:GetDbid()].."item:GetDbid()"..item:GetDbid())  
            -- end

            -- if self._unReadOffineList[item:GetDbid()] then
            --     LoggerHelper.Error("self._unReadOffineList[item:GetDbid()]:"..self._unReadOffineList[item:GetDbid()].."item:GetDbid()"..item:GetDbid())  
            -- end
            
        end    
    end
end

--获取获取全部未读消息
function FriendData:GetUnReadList()
    self:SetUnReadForList(self._friendList)
    self:SetUnReadForList(self._foeList)
    return  self._unReadList
end

--获取获取未读消息
function FriendData:GetIsReadList(needList)
    self:GetUnReadList()
    for i,item in ipairs(needList) do
        local num = self._unReadList[item:GetDbid()]
        if num then
            if num > 0 then
                return true
            end
        end
    end
end

function FriendData:SetCurChatPersonData(data)
     self._curChatPerson = data
end

function FriendData:GetCurChatPersonData()
    return self._curChatPerson
end

--更新好友聊天列表
function FriendData:UpdateNewChatListData(svrData)
    self._chatList = {}
    if svrData then
        for i, item in pairs(svrData) do
            --LoggerHelper.Error("data2 ..UpdateNewChatListData" .. PrintTable:TableToStr(item))
            local str = GameWorld.FilterWords(item[3])
            str = ChatManager:CheckContentLink(str)
            str = ChatManager.HandleEmoji(str)
            --需要后端处理好友聊天接口
            str = self:HandleItem(str,item[4])
            --local time = item[1]  --时间            
            local time = item[1]--时间
            local dbid = item[2]--dbid
            local contentStr = str --内容
            local emjo = item[4]
            local sys = item[5]
            if sys == 1 then
                local sysId = GlobalParamsHelper.GetParamValue(931)
                contentStr = LanguageDataHelper.CreateContent(sysId)
            end
            local photoFrameId = item[6]
            local chatFrameId = item[7]
            local _chatItem = {_time = time, _dbid = dbid, _contentStr = contentStr, _emjo = emjo, _type = 1,_photoFrameId = photoFrameId, _chatFrameId = chatFrameId}
            table.insert(self._chatList, _chatItem)
            --self:ClearUnReadOffineByDbid(item[2])

        -- for i,v in pairs(item) do
        --     --LoggerHelper.Log("data2 .." .. PrintTable:TableToStr(v))
        -- end
        end
    end
end

--更新好友聊天列表
function FriendData:SetMyChatListData(data, content)
    self._chatList = {}
    local _chatItem = {_cross_dbid = data:GetDbid(), _contentStr = content, _type = 0}
    table.insert(self._chatList, _chatItem)
end

--首次加好友提示
function FriendData:SetTipChatListData(data, content)
    local _chatItem = {_cross_dbid = data:GetDbid(),_contentStr = content,_type = 2}
    table.insert(self._chatOnceTips, _chatItem)
end

--得到加好友提示
function FriendData:GetTipChatListData(dbid)
    if self._chatOnceTips then
        for i,v in ipairs(self._chatOnceTips) do
            --LoggerHelper.Log("FriendData:SetTipChatListData"..v._cross_dbid)
            if v._cross_dbid == dbid then
                return v
            end
        end
    end
end


--得到加好友提示
function FriendData:ClearTipChatListData(dbid)
    if self._chatOnceTips then
        for i,v in ipairs(self._chatOnceTips) do
            if v._cross_dbid == dbid then
                self._chatOnceTips[i] = nil
            end
        end
    end
end


--添加好友到好友列表
function FriendData:AddFriendForFrendList(svrData)
    --LoggerHelper.Error("FriendData:AddFriendForFrendList"..PrintTable:TableToStr(svrData))
    if svrData then
        if not table.isEmpty(svrData) then
            local friendItem = FriendItemData(svrData)
            FriendData:ClearTipChatListData(friendItem:GetDbid())
            FriendData:ClearChatDataByDbid(friendItem:GetDbid())
            --self:SetTipChatListData(friendItem, "")
            table.insert(self._friendList, friendItem)
        end
    end
end

--从好友列表移除好友
function FriendData:RemoveFriendForFrendList(dbid)
    if dbid then      
        for i, item in pairs(self._friendList) do
            --LoggerHelper.Log("FriendData:RemoveFriendForFrendList62")
            if tonumber(item:GetDbid())  == tonumber(dbid) then
                --LoggerHelper.Log("FriendData:RemoveFriendForFrendList63".. item:GetDbid())
                self._friendList[i] = nil
                table.remove(self._friendList,i)
                --LoggerHelper.Log("FriendData:RemoveFriendForFrendList64".. PrintTable:TableToStr(self._friendList))
            end 
        end
    end
end

--从好友列表移除好友
function FriendData:RemoveFriendForFrendListBySvr(svrData)
    local removeFriendData = FriendItemData(svrData)
    --LoggerHelper.Log("FriendData:RemoveFriendForFrendListBySvr".. removeFriendData:GetDbid())
    if removeFriendData then
        for i, item in pairs(self._friendList) do
            if item:GetDbid() == removeFriendData:GetDbid() then
                self._friendList[i] = nil
                table.remove(self._friendList,i)
            end 
        end
    end
end


--从申请列表移除好友
function FriendData:RemoveFriendForApplyList(dbid)
    if dbid then
        for i, item in pairs(self._applyList) do
            if item:GetDbid() == dbid then
                self._applyList[i] = nil
                table.remove(self._applyList,i)
            end 
        end
    end
end

--添加好友到黑名单列表
function FriendData:AddFriendForBlackList(svrData)
    
    --LoggerHelper.Log("FriendData:AddFriendForBlackList(svrData)81"..PrintTable:TableToStr(self._blackList))
    if svrData then
        --FriendData:RemoveFriendForFrendList()
        
        --LoggerHelper.Log("FriendData:AddFriendForBlackList(svrData)82"..PrintTable:TableToStr(self._blackList))
        table.insert(self._blackList, FriendItemData(svrData))
    end

    --LoggerHelper.Log("FriendData:AddFriendForBlackList(svrData)83"..PrintTable:TableToStr(self._blackList))
end

--从黑名单列表移除好友
function FriendData:RemoveFriendForBlackList(dbid)
    if dbid and dbid~=0 then
        for i, item in pairs(self._blackList) do
            if item:GetDbid() == dbid then
                self._blackList[i] = nil
                table.remove(self._blackList,i)
            end 
        end
    end
end

--申请面板某个item
function FriendData:DeleApplyListData(dbid)
    if self._applyList then
        for i, item in pairs(self._applyList) do
            if dbid == item:GetDbid() then
                self._applyList[i] = nil
                table.remove(self._applyList,i)
            end
        end
    end
end

--清空数据
function FriendData:ClearApplyListData()
    self._applyList = {}
end

--获得申请面板数据
function FriendData:GetApplyListData()
    if self._applyList then
        for i,friendItem in ipairs(self._applyList) do
            if friendItem:GetDbid() then
                if self:GetDbidFromBlackList(friendItem:GetDbid()) or self:GetDbidFromFriendList(friendItem:GetDbid()) then
                    self._applyList[i] = nil
                    table.remove(self._applyList,i)
                else 
 
                end 
            end
        end
    end

    return self._applyList or {}
end

--获得好友列表数据
function FriendData:GetFriendRecentListData()
    self._friendRecentList = {}
    if self:GetFriendListData() then
        for i,v in ipairs(self:GetFriendListData()) do
            local foeDbid = self:GetDbidFromFoeList(v:GetDbid())
            --self:SetUpdateTimeByDbid(v:GetDbid())
            if not foeDbid then
                table.insert(self._friendRecentList,v)
            end
        end   

        for i,v in ipairs(self:GetFoeListData()) do
            table.insert(self._friendRecentList,v)      
        end   
        if  self._friendRecentList then
            if not table.isEmpty(self._friendRecentList) then
                self._friendRecentList = self:OrderByChatTime(self._friendRecentList)
            end    
        end

        return self._friendRecentList
    end  
end


--获得好友列表数据
function FriendData:GetFriendListData()
    if self._friendList then
        self._friendList = self:orderListDataByList(self._friendList)
        return self._friendList
    end
end

--获得好友面板数据
function FriendData:GetBlackListData()
    if self._blackList then
        self._blackList = self:orderListDataByList(self._blackList)
        return self._blackList
    end
end

--获得好友面板数据
function FriendData:GetChatListData()
    if self._chatList then
        return self._chatList
    end
end

--获得好友面板数据
function FriendData:GetRecommondListData()
    if self._recommondList then
        return self._recommondList
    end
end

--获取仇敌列表数据
function FriendData:GetFoeListData()
    if self._foeList then
        self._foeList = self:orderListDataByList(self._foeList)
        return self._foeList
    end
end

--获取好友在线人数（好友列表）
function FriendData:GetOnlineNumForFriendList()
    local onlineNum = 0
    if self._friendList then
        for i, item in pairs(self._friendList) do
            if item:GetIsOnline()==1 then
                onlineNum = onlineNum+1
            end
        end        
    end
    return onlineNum
end

--获取好友在线列表（在线好友列表）
function FriendData:GetOnlineListForFriendList()
    local onlineList = {}
    if self._friendList then
        for i, item in pairs(self._friendList) do
            if item:GetIsOnline()==1 then
                table.insert(onlineList, item)
            end
        end 
    end  
    return onlineList
end

--获取好友是否在线
function FriendData:GetIsOnlineForFriendList(dbid)   
    if self._friendList then
        for i, item in pairs(self._friendList) do
            if tonumber(dbid) == item:GetDbid() then 
                return item:GetIsOnline()
            end

        end 
    end  
end

--获取好友人数（好友列表）
function FriendData:GetTotalNumForApplyList()
    local applyNum = 0
    for i,v in ipairs(self._applyList) do
        if v:GetDbid() then
            applyNum = applyNum+1
        end
    end
    return applyNum
end


--获取好友人数（好友列表）
function FriendData:GetTotalNumForFriendList()
    local friendNum = 0
    for i,v in ipairs(self._friendList) do
        if v:GetDbid() then
            friendNum = friendNum+1
        end
    end


    return friendNum
end

--获取黑名单在线人数（黑名单列表）
function FriendData:GetOnlineNumForBlackList()
    local onlineNum = 0
    if self._blackList then
        for i, item in pairs(self._blackList) do
            if item:GetIsOnline()==1 then
                onlineNum = onlineNum+1
            end
        end        
    end
    return onlineNum
end

--获取黑名单人数（黑名单列表）
function FriendData:GetTotalNumForBlackList()
    local blackNum = 0
    for i,v in ipairs(self._blackList) do
        if v:GetDbid() then
            blackNum = blackNum+1
        end
    end

    return blackNum
end

--获取仇敌在线人数（仇敌列表）
function FriendData:GetOnlineNumForFoeList()
    local onlineNum = 0
    if self._foeList then
        for i, item in pairs(self._foeList) do
            if item:GetIsOnline()==1 then
                onlineNum = onlineNum+1
            end
        end        
    end
    return onlineNum
end

--获取仇敌人数（仇敌列表）
function FriendData:GetTotalNumForFoeList()
    local foeNum = 0
    for i,v in ipairs(self._foeList) do
        if v:GetDbid() then
            foeNum = foeNum+1
        end
    end
    return foeNum
end


function FriendData:SetDbidByChatList(listData)
    self._curdbid = 0
    if listData then
        if listData[1] then
            if listData[1]._type == 1 then
                self._curdbid = listData[1]._dbid
            end
            
            if listData[1]._type == 0 then
                self._curdbid = listData[1]._cross_dbid
            end

            if listData[1]._type == 2 then
                self._curdbid = listData[1]._cross_dbid
            end
            return self._curdbid
        end
    end
end


--存储每个dbid对应的数据
function FriendData:SetChatListData(listData)
    if listData then
        local dbid = self:SetDbidByChatList(listData)
        if dbid and dbid ~= 0 then
            if self._friendDictData[dbid] then
                
            else
                self._friendDictData[dbid] = {}
            end
            
            for k, data in pairs(listData) do
                table.insert(self._friendDictData[dbid], data)
            end
        end
    end
end

--存储每个dbid对应的数据
function FriendData:ClearChatDataByDbid(dbid)
    if self._friendDictData then
        if dbid and dbid ~= 0 then
            if self._friendDictData[dbid] then
                self._friendDictData[dbid] = nil
            end
        end
    end
end

--得到好友列表索引
function FriendData:GetFriendListIndex(listData)
    local dbid = self:SetDbidByChatList(listData)
    local friendList = self:GetFriendListData()
    if dbid and friendList then
        for i, item in pairs(friendList) do
            if item:GetDbid() == dbid then
                return i
            end
        end
    end
end

--根据dbid获取指定聊天信息
function FriendData:GetChatListDataByDbid(dbid)
    if dbid then
        return self._friendDictData[dbid]
    end
end

function FriendData:ClearNewChatNumByDbid(dbid)
    if self._friendChatNumItem[dbid] then
        self._friendChatNumItem[dbid] = nil  
    end
end

function FriendData:OnChatNewsResp(dbid,newsNum)
    local dbid = tonumber(dbid)
    if self._friendChatNumItem[dbid] then
        self._friendChatNumItem[dbid] = nil  
    end

    self._friendChatNumItem[dbid] = newsNum
    --LoggerHelper.Error("newsNum"..newsNum.."dbid"..dbid)  
    self:SetUpdateTimeByDbid(dbid,DateTimeUtil.GetServerTime())
    self:GetUnReadList()
end

function FriendData:GetChatNumChangeItem(dbid,newsNum)
    return self._friendChatNumItem
end

--聊天面板退出时清理数据
function FriendData:ClearChatViewData()
    self:SetCurChatPersonData(nil)
    self:SetCurClickIndex(-1)
end



--------------------------FriendChatListData---------------------------------------------------------------------------




--置顶排序
function FriendChatListData:SortTop(listType, dbid)
-- local index = self:GetListIndexByUUID(listType,dbid)
-- local data = self._FriendChatListData[listType][index]
-- table.remove(self._FriendChatListData[listType],index)
-- table.insert(self._FriendChatListData[listType],1,data)
end

--好友列表排序
function FriendChatListData:FrinedListSort()
-- SocietyListUtil:FrinedListSort(self._FriendChatListData[TYPE_FRIEND])
end



