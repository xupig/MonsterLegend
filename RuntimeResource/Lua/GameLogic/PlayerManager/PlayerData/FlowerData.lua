local FlowerData = Class.FlowerData(ClassTypes.XObject)
function FlowerData:__ctor__()
   self._sendflower={}
   self._receive={}
   self._backdata={}
end

function FlowerData:GetSendFlowerData()
    return self._sendflower
end

function FlowerData:UpdataBackFlowerData(data)
    self._backdata = data
end

function FlowerData:GetBackFlowerData()
    return self._backdata
end

function FlowerData:UpdateSendFlowerData(data)
    self._sendflower=data
end

function FlowerData:GetReceiveFlowerData()
    return self._receive
end

function FlowerData:UpdateReceiveFlowerData(data)
    self._receive=data
end