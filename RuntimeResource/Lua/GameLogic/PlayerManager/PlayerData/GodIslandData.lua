local public_config = GameWorld.public_config
local GodMonsterIslandDataHelper = GameDataHelper.GodMonsterIslandDataHelper
local GodMonsterIslandCrystalDataHelper = GameDataHelper.GodMonsterIslandCrystalDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskConfig = GameConfig.TaskConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local TaskRewardData = ClassTypes.TaskRewardData
local Vector3 = Vector3

local GodIslandItemData = Class.GodIslandItemData(ClassTypes.XObject)

function GodIslandItemData:__ctor__(id)--, rebornTime, isInstrest
    self._id = id
    self._type = 1
    self.IsGodIslandData = true
-- self._rebornTime = rebornTime
-- self._isInstrest = isInstrest
end

function GodIslandItemData:UpdateData(rebornTime, isInstrest)
    self._rebornTime = rebornTime
    self._isInstrest = isInstrest
end

function GodIslandItemData:SetRebornTime(rebornTime)
    self._rebornTime = rebornTime
end

function GodIslandItemData:SetLeftCount(leftCount)
    self._leftCount = leftCount
end

function GodIslandItemData:SetIsIntrest(isInstrest)
    if self._isInstrest ~= isInstrest then
        self._isInstrest = isInstrest
        EventDispatcher:TriggerEvent(GameEvents.OnGodIslandIsIntrestChanged, self._id)
    end
end

function GodIslandItemData:SetDeadPos(x, y, z)
    self._deadPos = Vector3(x * 0.01, y * 0.01, z * 0.01)
end

function GodIslandItemData:GetId()
    return self._id
end

function GodIslandItemData:GetIsSelected()
    return self._isSelected
end

function GodIslandItemData:GetLeftCount()
    return self._leftCount
end

function GodIslandItemData:GetPeaceShow()
    return 0
end

function GodIslandItemData:GetLevelShow()
    return GodMonsterIslandDataHelper:GetLevelShow(self._id)
end

function GodIslandItemData:GetTotalCount()
    return GodMonsterIslandCrystalDataHelper:GetNum(self._id)
end

function GodIslandItemData:SetIsSelected(isSelected)
    self._isSelected = isSelected
end

function GodIslandItemData:IsDead()
    if self._rebornTime then
        local monsterType = self:GetMonsterType()
        if monsterType == 1 then
            return DateTimeUtil:GetServerTime() < self._rebornTime
        else
            if DateTimeUtil:GetServerTime() > self._rebornTime then
                self._rebornTime = self._rebornTime + self:GetReviveTime()
            end
            return true
        end
    else
        return false
    end
end

function GodIslandItemData:GetRebornTime()
    return self._rebornTime or 0
end

function GodIslandItemData:GetReviveTime()
    if self._reviveTime == nil then
        self._reviveTime = GodMonsterIslandDataHelper:GetBossReviveTime(self._id)
    end
    return self._reviveTime
end

function GodIslandItemData:GetIsIntrest()
    return self._isInstrest or false
end

function GodIslandItemData:GetSenceId()
    return GodMonsterIslandDataHelper:GetSenceId(self._id)
end

function GodIslandItemData:GetSenceName()
    local mapId = GodMonsterIslandDataHelper:GetSenceId(self._id)
    return LanguageDataHelper.GetContent(MapDataHelper.GetChinese(mapId)) or ""
end

function GodIslandItemData:GetLevelLimit()
    return GodMonsterIslandDataHelper:GetLevelLimit(self._id)
end

function GodIslandItemData:GetMonsterType()
    return GodMonsterIslandDataHelper:GetMonsterType(self._id)
end

function GodIslandItemData:GetLevel()
    return GodMonsterIslandDataHelper:GetLevel(self._id)
end

function GodIslandItemData:GetName()
    return GodMonsterIslandDataHelper:GetName(self._id)
end

function GodIslandItemData:GetNameChinese()
    return LanguageDataHelper.GetContent(self:GetName())
end

function GodIslandItemData:GetNameIcon()
    return GodMonsterIslandDataHelper:GetNameIcon(self._id)
end

function GodIslandItemData:GetIcon()
    return GodMonsterIslandDataHelper:GetIcon(self._id)
end

function GodIslandItemData:GetFloor()
    return GodMonsterIslandDataHelper:GetFloor(self._id)
end

function GodIslandItemData:GetModel()
    local monsterId = GodMonsterIslandDataHelper:GetMonsterId(self._id)
    return MonsterDataHelper.GetMonsterModel(monsterId)
end

function GodIslandItemData:GetHeadIcon()
    local monsterId = GodMonsterIslandDataHelper:GetMonsterId(self._id)
    return MonsterDataHelper.GetIcon(monsterId)
end

function GodIslandItemData:GetBossDrops()
    local drops = GodMonsterIslandDataHelper:GetBossDrops(self._id)[GameWorld.Player():GetVocationGroup()]
    local items = {}
    for i, v in ipairs(drops) do
        table.insert(items, TaskRewardData(v, 1))
    end
    --LoggerHelper.Log("Ash: GetItem:" .. PrintTable:TableToStr(items))
    return items
end

function GodIslandItemData:GetRegionId()
    return GodMonsterIslandDataHelper:GetBossRegionId(self._id)
end

function GodIslandItemData:GetType()
    return self._type
end

local GodIslandData = Class.GodIslandData(ClassTypes.XObject)

function GodIslandData:__ctor__()
    self._monsterItems = {}
    self._floorsGodIslandData = {}
-- self._rebornTimeList = {}
-- self._tireValue = {}
end

function GodIslandData:UpdateGodIslandData(godIslandData)
    if godIslandData == nil then
        return
    end
    local floors, floorDic = self:GetAllInfo()
    local currentFloor = godIslandData[1]
    self:SetCurrentFloor(currentFloor)
    local monsterItems = floorDic[currentFloor]
    local crystalInfo = godIslandData[2]
    local eliteInfo = godIslandData[3]
    local rebornTime = godIslandData[4]
    if rebornTime then
        for i, v in ipairs(monsterItems) do
            local monsterType = v:GetMonsterType()
            if monsterType == 2 then
                v:SetRebornTime(eliteInfo[2])
                v:SetLeftCount(eliteInfo[3])
            elseif monsterType == 3 then
                v:SetRebornTime(crystalInfo[2])
                v:SetLeftCount(crystalInfo[3])
            else
                v:SetRebornTime(rebornTime[v:GetId()] or 0)
            end
        end
    end
    -- self:UpdateGodIslandIntrest(godIslandData[2])
    self._tireValue = godIslandData[6]
    self._bigCrystalCollectTimes = godIslandData[7]
end

-- function GodIslandData:UpdateGodIslandIntrest(intrestData)
--     for i, v in ipairs(self._sortedMonsterItems) do
--         local isInstrest = intrestData and intrestData[v:GetId()] ~= nil or false
--         v:SetIsIntrest(isInstrest)
--     end
-- end
function GodIslandData:UpdateGodIslandSpaceInfo(spaceData)
    self._sortedSpaceItems = {}
    local crystalInfo = spaceData[1]
    local crystalData = self:GetMonsterItem(crystalInfo[1])
    crystalData:SetRebornTime(crystalInfo[2])
    crystalData:SetLeftCount(crystalInfo[3])
    table.insert(self._sortedSpaceItems, crystalData)
    local eliteInfo = spaceData[2]
    local eliteData = self:GetMonsterItem(eliteInfo[1])
    eliteData:SetRebornTime(eliteInfo[2])
    eliteData:SetLeftCount(eliteInfo[3])
    table.insert(self._sortedSpaceItems, eliteData)
    local bossInfo = spaceData[3]
    for i, v in ipairs(bossInfo) do
        local monsterData = self:GetMonsterItem(v[1])
        monsterData:SetRebornTime(v[2])
        monsterData:SetDeadPos(v[3], v[4], v[5])
        table.insert(self._sortedSpaceItems, monsterData)
    end
end

function GodIslandData:GetAllInfo()
    if self._floors == nil then
        local floors = {}
        local floorDic = {}
        local data = GodMonsterIslandDataHelper:GetAllId()
        for i, v in ipairs(data) do
            if v ~= 0 then
                local floor = GodMonsterIslandDataHelper:GetFloor(v)
                if floorDic[floor] == nil then
                    floorDic[floor] = {}
                    table.insert(floors, floor)
                end
                local item = self:GetMonsterItem(v)
                table.insert(floorDic[floor], item)
            end
        end
        self._floors = floors
        self._floorsGodIslandData = floorDic
    end
    return self._floors, self._floorsGodIslandData
end

function GodIslandData:SetCurrentFloor(floor)
    self._currentFloor = floor
end

function GodIslandData:GetBigCrystalCollectTimes()
    return self._bigCrystalCollectTimes
end

function GodIslandData:GetCurFloorMonsterInfo()
    -- LoggerHelper.Log("Ash: GetCurFloorMonsterInfo:" .. PrintTable:TableToStr(self._floorsGodIslandData))
    return self._floorsGodIslandData[self._currentFloor] or {}
end

function GodIslandData:GetAllSpaceInfo()
    return self._sortedSpaceItems or {}
end

function GodIslandData:GetMonsterItem(id)
    if not self._monsterItems[id] then
        self._monsterItems[id] = GodIslandItemData(id)
    end
    return self._monsterItems[id]
end

function GodIslandData:GetTire()
    return self._tireValue or 0
end
