--广义背包数据，包含背包/仓库/身上装备的各种物品数据
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local PkgEquipItemData = ClassTypes.PkgEquipItemData
local PkgGemItemData = ClassTypes.PkgGemItemData
local PkgCommonItemData = ClassTypes.PkgCommonItemData
local SubBagData = Class.SubBagData(ClassTypes.XObject)

function SubBagData:__ctor__(serverData,pkgType)
    self.pkgType = pkgType
    self._itemInfos = {}
    self._equipedList = {}
    self._itemCounts = {}            --用表记录某种道具总量[itemId] = count
    self:Init(serverData)
end

--初始化背包
function SubBagData:Init(serverData)
    if self.pkgType == public_config.PKG_TYPE_RIDE then
        self._serverData = serverData
    else
        for pos, serverItemData in pairs(serverData) do
            local itemData = CommonItemData.CreateItemData(serverItemData,self.pkgType,pos)
            self._itemInfos[pos] = itemData
            self:UpdateItemCounts(itemData:GetItemID(),itemData:GetCount())
        end
    end
end

function SubBagData:GetItemInfos()
    return self._itemInfos
end

--获取背包当前使用格子数目
function SubBagData:GetTotalCount()
   local count = 0
   for k,v in pairs(self._itemInfos) do
        count = count +1
   end
   return count
end

--返回背包内某个itemType所有道具
function SubBagData:GetItemListByItemType(itemType)
    local result = {}
    for k,v in pairs(self._itemInfos) do
        if v:GetItemType() == itemType then
            table.insert(result,v)
        end
    end
    return result
end

--获取一个物品的总数量
function SubBagData:GetItemCountByItemId(itemId)
    return self._itemCounts[itemId] or 0
end

--是否存在某道具
function SubBagData:HasItemByItemId(itemId)
    return self._itemCounts[itemId] and self._itemCounts[itemId] > 0
end

--更新背包道具数量
function SubBagData:UpdateItemCounts(itemId,count)
    if self._itemCounts[itemId] then
        self._itemCounts[itemId] = self._itemCounts[itemId] + count
    else
        self._itemCounts[itemId] = count
    end
end

--获取某ID道具在背包其中一个位置
function SubBagData:GetItemPosByItemId(itemId)
    for k,v in pairs(self._itemInfos) do
        if v:GetItemID() == itemId then
            return v.bagPos
        end
    end
    return 0
end

--当前容量
function SubBagData:GetBagCurVolumn()
    if self.pkgType == public_config.PKG_TYPE_ITEM then
        return GameWorld.Player().pkg_items_size
    elseif self.pkgType == public_config.PKG_TYPE_WAREHOUSE then
        return GameWorld.Player().pkg_warehouse_size
    elseif self.pkgType == public_config.PKG_TYPE_RIDE then
        return GameWorld.Player().pkg_ride_size
    elseif self.pkgType == public_config.PKG_TYPE_LOTTERY_EQUIP then
        return GameWorld.Player().pkg_lottery_equip
    end
    return 0
end

--获取一个空格子位置
function SubBagData:GetOneEmtpyPos()
    local volumn = self:GetBagCurVolumn()
    for pos=1,volumn do
        if self._itemInfos[pos] == nil then
            return pos
        end
    end
    return 0
end

--背包是否满
function SubBagData:IsFull()
    return self:GetBagCurVolumn() == self:GetTotalCount()
end

--物品变化处理
function SubBagData:OnChanged(serverData, change, getWay)
    --删除物品
    if change[table.REMOVE_FLAG] ~= nil then
        local keys = change[table.REMOVE_FLAG]
        local pos1 = keys[1]
        local pos2 = keys[2]
        if #keys == 1 then
            --LoggerHelper.Log("SubBagData:Delete:"..self.pkgType.." pos:"..pos1,"#33bbff")
            self:UpdateItemCounts(self._itemInfos[pos1]:GetItemID(),-self._itemInfos[pos1]:GetCount())
            --用在需要获取删除item数据时
            EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_READY_REMOVE, self.pkgType, pos1, nil, 1)
            self._itemInfos[pos1] = nil
            EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_REMOVE, self.pkgType, pos1, nil, 2)
        elseif #keys == 2 then
            --LoggerHelper.Log("SubBagData:Delete:"..self.pkgType.." pos1:"..pos1.." pos2:"..pos2,"#33bbff")
            EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_READY_REMOVE2,self.pkgType,pos1,pos2)
            self._itemInfos[pos1][pos2] = nil
            EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_REMOVE2,self.pkgType,pos1,pos2)
        end
    else
        for pos, serverItemData in pairs(change) do
            --更新物品
            if self._itemInfos[pos] ~= nil then
                --LoggerHelper.Error("SubBagData:Update:"..self.pkgType.." pos:"..pos)
                --LoggerHelper.Log(serverData)
                local dCount = self._itemInfos[pos]:ResetServerData(serverData[pos])
                if dCount ~= 0 then
                    self:UpdateItemCounts(self._itemInfos[pos]:GetItemID(),dCount)
                end
                EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_UPDATE,self.pkgType,pos)
            --增加物品
            else
                --LoggerHelper.Log("SubBagData:Add:"..self.pkgType.." pos:"..pos,"#33bbff")
                self._itemInfos[pos] = CommonItemData.CreateItemData(serverItemData,self.pkgType,pos)
                self:UpdateItemCounts(self._itemInfos[pos]:GetItemID(),self._itemInfos[pos]:GetCount())
                EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_ADD,self.pkgType,pos)
                
                if getWay and getWay == public_config.ITEM_GET_WAY_EQUIP_UNLOAD then
                    --卸载不提示
                    return
                end
                if getWay and getWay == public_config.ITEM_GET_WAY_HANG_UP_OFFLINE then
                    --离线获得不提示
                    return
                end
                --获得物品飘字
                EventDispatcher:TriggerEvent(GameEvents.ITEM_CHANGE_INCREASE_FLOAT,self.pkgType, pos, 0)
            end
        end
    end
end

--宝石类型数据
local GemTypeData = Class.GemTypeData(ClassTypes.XObject)

function GemTypeData:__ctor__(gemId, gemNum ,bagType ,bagPos)
    self.gemId = gemId
    self.gemNum = gemNum
    self.bagType = bagType
    if bagPos then
        self:AddPos(bagPos)
    end
end

function GemTypeData:ResetData()
    self.gemNum = 0
    self.bagPosList = {}
end

function GemTypeData:AddNum(num)
    if self.gemNum then
        self.gemNum = self.gemNum + num
    else
        self.gemNum = num
    end
end

function GemTypeData:AddPos(bagPos)
    if self.bagPosList == nil then
        self.bagPosList = {}
    end
    table.insert(self.bagPosList,bagPos)
end

local function SortGemList(a,b)
    return a.gemId > b.gemId
end

--根据宝石孔位类型，按id列出魔石数据
function SubBagData:GetGemListByEquipPos(equipPos)
   local t = {}
   --LoggerHelper.Error("GetGemListByType")
   for pos, item in pairs(self._itemInfos) do
        local itemCfg = item.cfgData
        if itemCfg.itemType == public_config.ITEM_ITEMTYPE_GEM then
            --符合槽位的宝石
            for i=1,#itemCfg.gem_slot do
                if itemCfg.gem_slot[i] == equipPos then
                    local gemId = itemCfg.id
                    if t[gemId] then
                        t[gemId]:AddNum(item:GetCount())
                        t[gemId]:AddPos(pos)
                    else
                        local gtd = GemTypeData(item:GetItemID(),item:GetCount(),self.pkgType,pos)
                        t[gemId] = gtd
                    end
                end
            end
        end
    end
    local result = {}
    for k,v in pairs(t) do
        table.insert(result,v)
    end
    table.sort(result,SortGemList)
    return result
end

--根据宝石孔位类型，检查背包是否有更好宝石合适镶嵌
function SubBagData:HasBetterGemByEquipPos(equipPos,gemId)
    for pos, item in pairs(self._itemInfos) do
        local itemCfg = item.cfgData
        if itemCfg.itemType == public_config.ITEM_ITEMTYPE_GEM then
            --符合槽位的宝石
            for i=1,#itemCfg.gem_slot do
                if itemCfg.gem_slot[i] == equipPos then
                    if gemId then
                        if itemCfg.id > gemId then
                            return true
                        end
                    else
                        --
                        return true
                    end
                end
            end
        end
    end
    return false
end
--所有配置内可合成的魔石，即使背包数量为空()
--{
--[subType] = {
--     [level]={}
--     [level]={}
-- }
--}
function SubBagData:GetAllGemForCombineByType()
    if self._allGemCanCombineList == nil then
        self._allGemCanCombineList = {}
        local allGemCfgList = ItemDataHelper.GetAllGemCanCombine()
        for gemSubType,v in pairs(allGemCfgList) do
            if self._allGemCanCombineList[gemSubType] == nil then
                self._allGemCanCombineList[gemSubType] = {}
            end
            for gemLevel,v1 in pairs(v) do
                local gtd = GemTypeData(v1.id,0,self.pkgType,nil)
                self._allGemCanCombineList[gemSubType][gemLevel] = gtd
            end
        end
    else
        for gemSubType,v in pairs(self._allGemCanCombineList) do
            for gemLevel,v1 in pairs(v) do
                self._allGemCanCombineList[gemSubType][gemLevel]:ResetData()
            end
        end
    end

    local allGemCanCombineList = self._allGemCanCombineList

    for pos, item in pairs(self._itemInfos) do
        local itemCfg = item.cfgData
        --能用于合成的宝石
        if itemCfg.itemType == public_config.ITEM_ITEMTYPE_GEM and itemCfg.next_id > 0 then
            local gemSubType = itemCfg.subtype
            local gemLevel = itemCfg.gem_level
            allGemCanCombineList[gemSubType][gemLevel]:AddNum(item:GetCount())
            allGemCanCombineList[gemSubType][gemLevel]:AddPos(pos)
        end
    end

    return self._allGemCanCombineList
end

--是否存在符合某个槽位的魔石
function SubBagData:HasGemBySlot(slotType)
    for k,v in pairs(self._itemInfos) do
        if v:GetItemType() == public_config.ITEM_ITEMTYPE_GEM then
            local slots = v:GetSlotType()
            for i=1,#slots do
                if slots[i] == slotType then
                    return true
                end
            end
        end
    end
    return false
end

------------------------------------------------时装与已穿戴装备背包方法--------------------------------------------------
function SubBagData:GetLoadedEquipList()
    self._equipedList = {}
    for pos,item in pairs(self._itemInfos) do
        local cfg_id = item.cfg_id
        local type = ItemDataHelper.GetType(cfg_id)
        self._equipedList[type] = cfg_id
    end
    return self._equipedList
end

--按子类型获取装备列表
function SubBagData:GetEquipListBySubType(subType)
    local result = {}
    for pos, item in pairs(self._itemInfos) do
        if item.cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP and item.cfgData.type == subType then
            table.insert(result,item)
        end
    end
    return result
end

--按子类型获取时装列表
function SubBagData:GetFashionListBySubType(subType)
    local result = {}
    for pos, item in pairs(self._itemInfos) do
        if item.cfgData.itemType == public_config.ITEM_ITEMTYPE_FASHION and item.cfgData.type == subType then
            table.insert(result,item)
        end
    end
    return result
end

--------------------------------------------------------背包总数据------------------------------------------------------------------
local BagData = Class.BagData(ClassTypes.XObject)

function BagData:__ctor__()
    self._pkgs = {} --key:背包类型  value:背包对应数据
end

function BagData:ClearData()
    self._pkgs = {}
end

function BagData:OnPkgChanged(pkgType, serverData, change, getWay)
    if change == nil then --完整初始化
        self._pkgs[pkgType] = nil
        self._pkgs[pkgType] = SubBagData(serverData,pkgType)
    else
        self._pkgs[pkgType]:OnChanged(serverData, change, getWay)
    end
end

function BagData:GetPkg(pkgType)
    return self._pkgs[pkgType]
end

--根据背包类型和位置获取物品数据
function BagData:GetItem(pkgType,pkgPos)
    if self._pkgs[pkgType] then
        return self._pkgs[pkgType]:GetItemInfos()[pkgPos]
    end
    return nil
end

