local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local EventDispatcher = EventDispatcher
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

local PrayData = Class.PrayData(ClassTypes.XObject)

function PrayData:__ctor__()
    self._prayExp = {}
    self._prayGold = {}
    self._expRed = false
    self._goldRed = false
    self._ctri = 0
end

function PrayData:UpdateExp(data)
    self._prayExp = data
end

function PrayData:UpdateGold(data)
    self._prayGold = data
end

function PrayData:GetExp()
    return self._prayExp
end

function PrayData:GetGold()
    return self._prayGold
end

function PrayData:UpdateExpRed(f)
    self._expRed = f
end

function PrayData:GetExpRed()
    return self._expRed
end

function PrayData:UpdateGoldRed(f)
    self._goldRed = f
end

function PrayData:GetGoldRed()
    return self._goldRed
end

function PrayData:UpdateCtri(data)
    self._ctri = data
    EventDispatcher:TriggerEvent(GameEvents.PrayCtri)
end

function PrayData:GetCtri()
    return self._ctri
end