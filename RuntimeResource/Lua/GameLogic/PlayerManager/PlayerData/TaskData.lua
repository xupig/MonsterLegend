require "PlayerManager/PlayerData/EventData"
require "PlayerManager/PlayerData/InteractData"
require "PlayerManager/PlayerData/TaskCommonItemData"

local EventData = ClassTypes.EventData
local InteractData = ClassTypes.InteractData
local TaskRewardData = ClassTypes.TaskRewardData
local TaskCommonItemData = ClassTypes.TaskCommonItemData

local public_config = GameWorld.public_config
local TaskDataHelper = GameDataHelper.TaskDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FontStyleHelper = GameDataHelper.FontStyleHelper
local TaskConfig = GameConfig.TaskConfig
local TaskManager

local TracingPointData = Class.TracingPointData(ClassTypes.XObject)

function TracingPointData:__ctor__(task, eventItemData, location)
    self._task = task
    self._eventItemData = eventItemData
    self._location = location
end

function TracingPointData:GetTask()
    return self._task
end

function TracingPointData:GetEventItemData()
    return self._eventItemData
end

function TracingPointData:GetLocation()
    return self._location
end


local TaskItemData = Class.TaskItemData(ClassTypes.TaskCommonItemData)

function TaskItemData:__ctor__(id, serverData)
    self._id = id
    self:UpdateTaskData(serverData)
    self._taskItemType = TaskConfig.TASK_TYPE_COMMON_TASK
end

function TaskItemData:UpdateTaskData(serverData)
    if self._serverData ~= nil then
        self._lastState = self:GetState()
    end
    
    self._serverData = serverData
    
    if serverData == nil then return end
    
    if self._lastState == public_config.TASK_STATE_ACCEPTABLE
        and self:GetState() ~= public_config.TASK_STATE_ACCEPTABLE then
        self._targetsData = nil -- 如果更新前状态是可接，更新后不是可接状态，把目标数据清空
    end
    
    if self:GetState() == public_config.TASK_STATE_ACCEPTABLE then
        if self:GetAcceptNPC() == nil or self:GetAcceptNPC() == 0 then
            -- EventDispatcher:TriggerEvent(GameEvents.ACCEPT_TASK, self._id)
            end
    elseif self:GetState() == public_config.TASK_STATE_COMPLETED then
        local commitNPC = self:GetCommitNPC()
        if commitNPC == nil or commitNPC == 0 then
            -- EventDispatcher:TriggerEvent(GameEvents.COMMIT_TASK, self._id)
        elseif commitNPC == 666 then

            -- EventDispatcher:TriggerEvent(GameEvents.WAIT_FOR_COMMIT_TASK, self._id)
        end
    end
end

function TaskItemData:UpdateTrackMark(serverData)
    self._serverData[public_config.TASK_INFO_KEY_TRACK] = serverData
end

function TaskItemData:GetId()
    return self._id
end

function TaskItemData:GetName()
    return TaskDataHelper:GetName(self._id)
end

function TaskItemData:GetOrder()
    return TaskDataHelper:GetOrder(self._id)
end

function TaskItemData:GetLocation()
    return TaskDataHelper:GetLocation(self._id)
end

function TaskItemData:GetDetail()
    return TaskDataHelper:GetDetail(self._id)
end

function TaskItemData:GetExplain()
    return TaskDataHelper:GetExplain(self._id)
end

function TaskItemData:GetDesc()
    return TaskDataHelper:GetDesc(self._id)
end

function TaskItemData:GetIcon()
    return TaskDataHelper:GetIcon(self._id)
end

function TaskItemData:GetAcceptNPC()
    return TaskDataHelper:GetAcceptNPC(self._id)
end

function TaskItemData:GetCommitNPC()
    return TaskDataHelper:GetCommitNPC(self._id)
end

function TaskItemData:GetAcceptDialog()
    return TaskDataHelper:GetAcceptDialog(self._id)
end

function TaskItemData:GetCommitDialog()
    return TaskDataHelper:GetCommitDialog(self._id)
end

function TaskItemData:GetNextTaskId()
    return TaskDataHelper:GetNextTask(self._id)
end

function TaskItemData:GetNextTaskItemData()
    return TaskItemData(TaskDataHelper:GetNextTask(self._id), nil)
end

function TaskItemData:IsOpen()
    local limit = TaskDataHelper:GetLimit(self._id)
    for type, value in pairs(limit) do
        if type == public_config.LIMIT_LEVEL and GameWorld.Player().level >= value[1] then
            return true
        end
    end
    return false
end

function TaskItemData:GetInteracts()
    return InteractData(TaskDataHelper:GetIntermezzi(self._id)):GetInteracts()
end

function TaskItemData:HasFinishLastTasks(commitedMainTaskId)
    local lastTasks = TaskDataHelper:GetLastTask(self._id)
    local taskType = lastTasks:GetTaskType()
    if taskType == public_config.TASK_TYPE_MAIN then
        --主线任务判断前置任务是否比当前完成的主线任务ID大
        --是则存在未完成完成前置任务
        for i, v in ipairs(lastTasks) do
            if v >= commitedMainTaskId then
                return false
            end
        end
        return true
    elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
        --支线任务根据服务器下发的已完成支线来确定是否已完成前置任务
        for i, v in ipairs(lastTasks) do
            if self._commitedOptionalTasks[v] == nil then
                return false
            end
        end
        return true
    else
        LoggerHelper.Error("Task type not handle:" .. id .. " " .. taskType)
        return false
    end
end

function TaskItemData:GetWorldLimit()
    local limit = self:GetLimit(self._id)
    return limit and limit[public_config.LIMIT_WORLD_LEVEL][1] or 0
end

function TaskItemData:GetSelfLimit()
    local limit = self:GetLimit(self._id)
    return limit and limit[public_config.LIMIT_LEVEL][1] or 0
end

function TaskItemData:GetLimit()
    return TaskDataHelper:GetLimit(self._id)
end

function TaskItemData:GetTargetsInfoData()
    return TaskDataHelper:GetTargetsInfo(self._id)
end

function TaskItemData:GetTargetsDesc()
    return TaskDataHelper:GetTargetsDesc(self._id)
end

function TaskItemData:GetMyRewards()
    local rewards = TaskDataHelper:GetReward(self._id)
    local result = {}
    if rewards then
        local myVocation = GameWorld.Player():GetVocationGroup()
        for i, v in ipairs(rewards) do
            if v[1] == public_config.VOCATION_ALL or v[1] == myVocation then
                --LoggerHelper.Log("Ash: TaskItemData:GetMyRewards: " .. #v[2])
                for i = 1, #v[2], 2 do
                    table.insert(result, TaskRewardData(v[2][i], v[2][i + 1]))
                end
            end
        end
    end
    --LoggerHelper.Log("Ash: TaskItemData:GetMyRewards: " .. PrintTable:TableToStr(result))
    return result
end

function TaskItemData:GetTitleNameAndColor()
    local taskType = self:GetTaskType()
    local state = self:GetState()
    local name, color
    if taskType == public_config.TASK_TYPE_MAIN then
        name = LanguageDataHelper.CreateContent(134) .. LanguageDataHelper.CreateContent(self:GetName())
        color = FontStyleHelper:GetFontColor(25)
    elseif taskType == public_config.TASK_TYPE_BRANCH then
        name = LanguageDataHelper.CreateContent(135) .. LanguageDataHelper.CreateContent(self:GetName())
        color = FontStyleHelper:GetFontColor(26)
    elseif taskType == public_config.TASK_TYPE_VOCATION then
        name = LanguageDataHelper.CreateContent(302) .. LanguageDataHelper.CreateContent(self:GetName())
        color = FontStyleHelper:GetFontColor(29)
    else
        name = LanguageDataHelper.CreateContent(146) .. LanguageDataHelper.CreateContent(self:GetName())
        color = FontStyleHelper:GetFontColor(27)
        LoggerHelper.Error("Task type not handle:" .. self:GetId() .. " " .. taskType)
    end
    return name, color
end

function TaskItemData:GetTaskType()
    return TaskDataHelper:GetTaskType(self._id)
end

function TaskItemData:GetImageMapId()
    return TaskDataHelper:GetImageMapId(self._id)
end

function TaskItemData:IsAutoTask()
    local autoTaskData = TaskDataHelper:GetAutoTaskMechanism(self._id)
    if autoTaskData and autoTaskData[1] then
        if autoTaskData[1] == 1 then --无条件自动完成
            return true
        elseif autoTaskData[1] == 2 and autoTaskData[2] and autoTaskData[3] then --有条件的自动完成
            if autoTaskData[2] == 1 then --VIP等级达标
                --todo
                return true
            elseif autoTaskData[2] == 2 and GameWorld.Player().level >= autoTaskData[3] then --角色等级达标
                return true
            end
        end
    end
    return false
end

function TaskItemData:GetLittleShoesOpen()
    local state = self:GetState()
    if state == public_config.TASK_STATE_ACCEPTABLE then
        return TaskDataHelper:GetLittleShoesOpenStart(self._id) == 1
    elseif state == public_config .TASK_STATE_ACCEPTED then
        return TaskDataHelper:GetLittleShoesOpen(self._id) == 1
    elseif state == public_config.TASK_STATE_COMPLETED then
        return TaskDataHelper:GetLittleShoesOpenEnd(self._id) == 1
    end
end

function TaskItemData:GetTargetsCurTimes()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_TARGETS] or {}
end

function TaskItemData:GetState()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_STATE] or nil
end

function TaskItemData:GetIsTrack()
    if self._serverData ~= nil then
        if self:GetState() == public_config.TASK_STATE_ACCEPTABLE then
            return true
        end
        return self._serverData[public_config.TASK_INFO_KEY_TRACK] == TaskConfig.IS_TRACK
    end
    return false
end

-- 参数为在接了任务后判断是否为当前步骤用
function TaskItemData:GetTrackIcon(eventItemData)
    local state = self:GetState()
    local taskType = self:GetTaskType()
    if state == public_config.TASK_STATE_ACCEPTABLE then
        if taskType == public_config.TASK_TYPE_MAIN then
            return TaskConfig.MAIN_ACCEPTABLE
        elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
            return TaskConfig.BRANCH_ACCEPTABLE
        end
    elseif state == public_config.TASK_STATE_COMPLETED then
        if taskType == public_config.TASK_TYPE_MAIN then
            return TaskConfig.MAIN_COMPLETED
        elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
            return TaskConfig.BRANCH_COMPLETED
        end
    elseif state == public_config.TASK_STATE_ACCEPTED and eventItemData ~= nil then
        if taskType == public_config.TASK_TYPE_MAIN then
            return TaskConfig.MAIN_ACCEPTED
        elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
            return TaskConfig.BRANCH_ACCEPTED
        end
    end
    return nil
end

function TaskItemData:GetAcceptTaskCallBack()
    return function(id)EventDispatcher:TriggerEvent(GameEvents.ACCEPT_TASK, id) end
end

function TaskItemData:GetCommitTaskCallBack()
    return function(id)EventDispatcher:TriggerEvent(GameEvents.COMMIT_TASK, id) end
end

local TaskData = Class.TaskData(ClassTypes.XObject)

function TaskData:__ctor__()
    self._tasks = {}
    self._optionalTask = {}
    self._vocationTask = {}
    self._branchTask = {}
    -- self._curMapTrackingTask = {}
end

function TaskData:ResetTaskData(serverData)
    self._tasks = {}
    self._optionalTask = {}
    self._vocationTask = {}
    self._branchTask = {}
    self:UpdateTaskData(serverData)
end

--获取更新的任务id列表
--{id,id.....}
function TaskData:GetUpdateTaskInfo(serverData)
    local ids = {}
    for id, task in pairs(serverData) do
        table.insert(ids, id)
    end
    return ids
end

function TaskData:UpdateTaskData(serverData)
    local datas = {}
    for id, task in pairs(serverData) do
        local data = {}
        data[TaskConfig.TASK_CHANGE_INFO_ID] = id
        data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_COMMON_TASK
        
        if self._tasks[id] == nil then--增加任务
            self._tasks[id] = TaskItemData(id, task)
            data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
            data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = self._tasks[id]
            if TaskManager == nil then
                TaskManager = PlayerManager.TaskManager
            end 
            if self._tasks[id]:GetTaskType() == public_config.TASK_TYPE_BRANCH then
                EventDispatcher:TriggerEvent(GameEvents.ADD_NEW_BRANCH, id)
            end
            
            if self._tasks[id]:GetTaskType() == public_config.TASK_TYPE_MAIN and TaskManager:GetMainTaskNil() then
                --弹出指引指向主线任务，取消指引指向赏金任务
                TaskManager:SetAgainMainTask(true)
                EventDispatcher:TriggerEvent(GameEvents.CAN_GO_MAIN_TASK_AGAIN)
                TaskManager:SetMainTaskNil(false)
            end
        -- EventDispatcher:TriggerEvent(GameEvents.OnAddTask, id)
        -- LoggerHelper.Error("接受一个新任务")
        else
            self._tasks[id]:UpdateTaskData(task)
            data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE
            data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = self._tasks[id]
        end
        table.insert(datas, data)
        
        local taskType = self._tasks[id]:GetTaskType()
        if taskType == public_config.TASK_TYPE_MAIN then
            self._mainTask = self._tasks[id]
        elseif taskType == public_config.TASK_TYPE_VOCATION then
            self._optionalTask[id] = self._tasks[id]
            self._vocationTask[id] = self._tasks[id]
            EventDispatcher:TriggerEvent(GameEvents.UPDATE_VOCATION_TASK)
        elseif taskType == public_config.TASK_TYPE_BRANCH then
            self._optionalTask[id] = self._tasks[id]
            self._branchTask[id] = self._tasks[id]
        else
            LoggerHelper.Error("Task type not handle:" .. id .. " " .. taskType)
        end
    end
    return datas
end

function TaskData:DelTaskData(serverData)
    local datas = {}
    for k, v in pairs(serverData) do
        local task = self._tasks[k]
        if task ~= nil then
            local data = {}
            data[TaskConfig.TASK_CHANGE_INFO_ID] = k
            data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_COMMON_TASK
            data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
            table.insert(datas, data)
            
            local taskType = task:GetTaskType()
            if taskType == public_config.TASK_TYPE_MAIN then
                self._mainTask = nil
                local next = PlayerManager.TaskCommonManager:GetNextOpenSoonTask(k)
                if not table.isEmpty(next) then
                    table.insert(datas, next)
                    if TaskManager == nil then
                        TaskManager = PlayerManager.TaskManager
                    end    
                    TaskManager:SetNoMainTask(true)
                    TaskManager:SetMainTaskNil(true)
                end
            elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
                self._optionalTask[k] = nil
                if taskType == public_config.TASK_TYPE_VOCATION then
                    self._vocationTask[k] = nil
                    EventDispatcher:TriggerEvent(GameEvents.UPDATE_VOCATION_TASK)
                end
            else
                LoggerHelper.Error("Task type not handle:" .. k .. " " .. taskType)
            end
            self._tasks[k] = nil
        else
            LoggerHelper.Error("Del Task id not exist:" .. k)
        end
        EventDispatcher:TriggerEvent(GameEvents.COMMITED_TASK_CALLBACK_FOR_MENU, k)
        EventDispatcher:TriggerEvent(GameEvents.COMMITED_TASK_CALLBACK, k)
    end
    return datas
end

function TaskData:ResetCommitedOptional(serverData)
    self._commitedOptionalTasks = serverData
end

function TaskData:UpdateCommitedOptional(serverData)
    local datas = {}
    for id, v in pairs(serverData) do
        local next = PlayerManager.TaskCommonManager:GetNextOpenSoonTask(id)
        if not table.isEmpty(next) then
            table.insert(datas, next)
        end
        self._commitedOptionalTasks[id] = v
    -- EventDispatcher:TriggerEvent(GameEvents.COMMITED_TASK_CALLBACK, id)
    end
    return datas
end

function TaskData:RefreshProcessingTrack(serverData)
    for k, v in pairs(serverData) do
        if self._tasks[k] ~= nil then
            self._tasks[k]:UpdateTrackMark(v)
        end
    end
end

function TaskData:NewTaskItemData(id, serverData)
    return TaskItemData(id, serverData)
end

function TaskData:NewTaskItemDataWithState(id, state)
    local serverData = {}
    serverData[public_config.TASK_INFO_KEY_STATE] = state
    return TaskItemData(id, serverData)
end

function TaskData:GetMainTask()
    return self._mainTask
end

--获取支线任务
function TaskData:GetBranchTask()
    return self._branchTask
end

function TaskData:GetOptionalTask()
    return self._optionalTask
end

function TaskData:GetVocationTask()
    return self._vocationTask
end

function TaskData:GetTaskById(id)
    return self._tasks[id]
end

function TaskData:GetTasks()
    return self._tasks
end

function TaskData:GetCommitedOptionalTask()
    return self._commitedOptionalTasks or {}
end

-- function TaskData:SetCurMapTrackingTask(value)
--     self._curMapTrackingTask = value
-- end

-- function TaskData:GetCurMapTrackingTask()
--     return self._curMapTrackingTask
-- end
