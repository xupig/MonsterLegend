local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local EventDispatcher = EventDispatcher

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RuneDataHelper = GameDataHelper.RuneDataHelper
local RuneConfigDataHelper = GameDataHelper.RuneConfigDataHelper
local RuneExchangeDataHelper = GameDataHelper.RuneExchangeDataHelper

local RuneData = Class.RuneData(ClassTypes.XObject)
local RuneExItemData = Class.RuneExItemData(ClassTypes.XObject)
local RuneItemData = Class.RuneItemData(ClassTypes.XObject)
local RuneSlotItemData = Class.RuneSlotItemData(ClassTypes.XObject)
-- RuneItemData
function RuneItemData:__ctor__(runeid,level)
    self._runeid = runeid
    self._level = level
    self._attcfg = RuneConfigDataHelper:GetAttByIdlevel(self._runeid,self._level)
end

function RuneItemData:GetIcon()
    return RuneDataHelper:GetIcon(self._runeid)
end

function RuneItemData:GetAtt()
    return self._attcfg.atrri
end

function RuneItemData:GetUpCost()
    return self._attcfg.cost
end

function RuneItemData:GetResolve()
    return self._attcfg.resolve
end

function RuneItemData:GetExp()
    return self._attcfg.exp
end

function  RuneItemData:GetName()
    local  id = self:GetItemComid()
    return ItemDataHelper.GetItemNameWithColor(id)
end

function  RuneItemData:GetItemComid()
    return RuneDataHelper:GetItemCom(self._runeid)
end

function  RuneItemData:GetType()
    return RuneDataHelper:GetType(self._runeid)
end

function RuneItemData:GetUnLockFloor()
    return RuneDataHelper:GetUnlockFloor(self._runeid)    
end

function RuneItemData:IsFulllevel()
    local d = self:GetUpCost()
    return table.containsValue(d, 0)
end

function RuneItemData:GetQuality()
    return RuneDataHelper:GetQuality(self._runeid)
end

function RuneItemData:GetNextAtt()
    if not self:IsFulllevel() then
        local next = RuneItemData(self._runeid,self._level+1)
        return next:GetAtt()
    end
end

function RuneItemData:GetNextExp()
    if not self:IsFulllevel() then
        local next = RuneItemData(self._runeid,self._level+1)
        return next:GetExp()
    end
end
-- 升级红点
function RuneItemData:IsRed()
    if self:IsFulllevel() then return false end
    local entity = GameWorld.Player()
    local d = self:GetUpCost()
    local id = 0
    local costnum = 0
    local havenum = 0
    for k,num in pairs(d) do
        id = tonumber(id)
        costnum = num
        break
    end
    local pieceid = GlobalParamsHelper.GetParamValue(485)
    local essenceid = GlobalParamsHelper.GetParamValue(484)
    if id == pieceid then
        havenum = entity.rune_piece
    else 
        havenum = entity.rune_essence
    end
    if havenum>= costnum then
        return true
    end
    return false
end

--RuneSlotItemData
function RuneSlotItemData:__ctor__()
    
end

function RuneSlotItemData:SetSlot(slot)
    self.slot = slot
end

function RuneSlotItemData:RuneIdLe(runeid,level)
    self._runeid = runeid
    self._level = level
end

function RuneSlotItemData:IsHave(v)
    self._have = v
end

function RuneSlotItemData:OpenLayer(layer)
    self._layer = layer
end

function RuneSlotItemData:GetRuneItem()
    return RuneItemData(self._runeid,self._level)
end

function RuneSlotItemData:IsRed()
    local runeitem = RuneItemData(self._runeid,self._level)
    return runeitem:IsRed()
end


-- RuneExItemData
function RuneExItemData:__ctor__(id,runeid,level)
    self._runeid = runeid
    self._level = level
    self._id = id
    self._Attcfg = RuneConfigDataHelper:GetAttByIdlevel(runeid,level)
    self._Excfg = RuneExchangeDataHelper:GetCfgByIdLevel(runeid,level)[1]
end

function RuneExItemData:GetPrice()
    return self._Excfg.price[public_config.MONEY_TYPE_RUNE_PIECE]
end

function RuneExItemData:GetFloor()
    return self._Excfg.floor 
end

function RuneExItemData:GetLevel()
    return self._Excfg.level    
end

function RuneExItemData:GetAtt()
    return self._Attcfg.atrri
end

function RuneExItemData:GetCost()
    return self._Attcfg.cost
end

function RuneExItemData:GetRes()
    return self._Attcfg.resolve
end

function RuneExItemData:GetExp()
    return self._Attcfg.exp
end

function RuneExItemData:GetIcon()
    return RuneDataHelper:GetIcon(self._runeid)
end

function RuneExItemData:GetName()
    return RuneDataHelper:GetName(self._runeid)
end

function RuneExItemData:GetPieceIcon()
    return ItemDataHelper.GetIcon(public_config.MONEY_TYPE_RUNE_PIECE)
end

function RuneExItemData:GetRuneQuality()
    return RuneDataHelper:GetQuality(self._runeid)
end

function RuneExItemData:GetItemCom()
    return RuneDataHelper:GetItemCom(self._runeid)
end

-- 是否为经验符文
function RuneExItemData:IsExpRune()
    local type=RuneDataHelper:GetType(self._runeid)
    if type[1]==5 or type[2]==5 then
        return true
    end
    return false
end
-- 分解
local RuneResItemData = Class.RuneResItemData(ClassTypes.RuneItemData)
function RuneResItemData:__ctor__(runeid,level)
    
end

function RuneResItemData:PkgPos(pos)
    self._pkgPos = pos
end

-- 替换
local RuneRepItemData = Class.RuneRepItemData(ClassTypes.RuneItemData)
function RuneRepItemData:__ctor__(runeid,level)
    
end
function RuneRepItemData:PkgPos(pos)
    self._pkgPos = pos
end

function RuneData:__ctor__()
    self._fxpos = {}
end
-- 兑换数据
function RuneData:GetExchangeData()
    local ids = RuneExchangeDataHelper:GetAllId()
    local total = {}
    local items = {}
    items[1]={}--经验符文
    items[2]={}--非经验符文
    for _,id in ipairs(ids) do
        local runeid = RuneExchangeDataHelper:GetRuneId(id)
        local level = RuneExchangeDataHelper:GetLevel(id)
        local type = RuneDataHelper:GetType(runeid)
        if type[1]==91 then
            table.insert(items[1],RuneExItemData(id,runeid,level))
        else
            table.insert(items[2],RuneExItemData(id,runeid,level))
        end
    end
    return items
end

-- 槽数据
function RuneData:GetSlotItemData(slot)
    local grids = GameWorld.Player().rune_grid
    local unlocklevellist = GlobalParamsHelper.GetParamValue(475) -- 解锁levellist
    local hole = grids[slot]
    local item = RuneSlotItemData()
    item:SetSlot(slot)
    item:OpenLayer(unlocklevellist[slot])
    if hole ~= nil  then
        item:IsHave(true)
        item:RuneIdLe(hole[1],hole[2])
    else
        item:IsHave(false)
    end
    return item
end
-- 升级数据
function RuneData:GetRuneUpData(holeindex)
    local grid = GameWorld.Player().rune_grid
    local rune = grid[holeindex]
    if rune then
        return RuneItemData(rune[1],rune[2])
    end
end

-- 分解数据
function RuneData:GetRuneResolve()
    local runepkg = GameWorld.Player().rune_pkg
    local items = {}
    local d1 = {}
    local d2 = {}
    self._resRuneQ = {}
    for i,v in pairs(runepkg) do
        local runeid = v[1]
        local runelevel = v[2]
        local item = RuneResItemData(runeid,runelevel)
        item:PkgPos(i)
        local q = item:GetQuality()
        self._resRuneQ[q]=q
        if item:GetType()[1]==91 then
            table.insert(d1,item)
        else
            table.insert(d2,item)
        end
    end
    table.sort(d1,RuneData.sortRe)
    table.sort(d2,RuneData.sortRe)
    for i,v in pairs(d1) do 
		table.insert(items,v)
	end
	for i,v in pairs(d2) do 
		table.insert(items,v)
    end
    return items
end
-- 替换符文
function RuneData:GetRuneReData()
    local entity =GameWorld.Player()
    local runepkg = entity.rune_pkg
    local runegrid = entity.rune_grid
    local inserttype = {}	
	for i=1, 8 do
		local hole = runegrid[i]
		if hole ~= nil then
			local runeid = hole[1]
			local type = RuneDataHelper:GetType(runeid)
			inserttype[runeid] = type
		end
    end
    local items = {}
    local i = 1
	for k,v in pairs(runepkg) do
		local runeid = v[1]
		local runelevel = v[2]--背包符文类型
		local _typelist = RuneDataHelper:GetType(runeid)--背包符文类型
		local f = false
		for _k,_type in ipairs(_typelist) do
			for key,typelist in pairs(inserttype) do
				for i,type in ipairs(typelist) do	
					if _type==91 then
						f=true
						break
					elseif type == _type then
						if #typelist == #_typelist then
							f = self:IsType(typelist,_typelist)
							if f then
								break
							end
						else
							f=false
						end
					end
				end
				if f then
					break
				end
			end
			if not f and _type ~=91 then
                items[i] = RuneRepItemData(runeid,runelevel)
                items[i]:PkgPos(k)
                i = i + 1
			end
		end
    end
    table.sort(items,RuneData.sortRerune)
	return items
end

function RuneData.sortRerune(first,senond)
    if first:GetQuality() ~= senond:GetQuality() then
        return first:GetQuality()>senond:GetQuality()
    else
        if first._level ~= senond._level then
            return first._level>senond._level
        else
            return first._runeid<senond._runeid
        end
    end
end

function RuneData:GetSameRune(id,level)
	if id ==nil then
		return {}
	end
	local data = {}
	local runepkg = GameWorld.Player().rune_pkg
    local types = RuneDataHelper:GetType(id)
    local i = 1
    for k,v in pairs(runepkg) do
		local runeid = v[1]
		local runelevel = v[2]--背包符文类型
		local _typelist = RuneDataHelper:GetType(runeid)--背包符文类型
		if #types == #_typelist then
			local f = self:IsType(types,_typelist)
			if f and runeid~=id or runeid==id and runelevel~=level then			
                data[i] = RuneRepItemData(runeid,runelevel)
                data[i]:PkgPos(k)
				i = i + 1
			end
		end
	end
	return data
end

function RuneData:IsSameRuneAndUp(id,level)
	if id ==nil then
		return false
	end
	local data = {}
	local runepkg = GameWorld.Player().rune_pkg
    local types = RuneDataHelper:GetType(id)
    for k,v in pairs(runepkg) do
		local runeid = v[1]
		local runelevel = v[2]--背包符文类型
		local _typelist = RuneDataHelper:GetType(runeid)--背包符文类型
		if #types == #_typelist then
			local f = self:IsType(types,_typelist)
			if f and runeid~=id and RuneDataHelper:GetQuality(runeid)>RuneDataHelper:GetQuality(id) or runeid==id and runelevel>level then			
				return true
			end
		end
    end
	return false
end

function RuneData:IsType(type0,type1)
	if #type0 ~=#type1 then
		return false
	end
	local f = false
	for k0, v0 in pairs(type0) do
		for k1,v1 in pairs(type1) do 
			if v0 == v1 then
				f = true
			end
		end
	end
	return f
end

function RuneData:IsHaveQuality(q)
   if self._resRuneQ[q] then return true end
   return false
end

function RuneData.sortRe(first,senond)
    local f = first:GetResolve()
    local s = senond:GetResolve()
    local id = GlobalParamsHelper.GetParamValue(484)
	if f[id] > s[id] then
		return true
	elseif f[id] < s[id] then 
		return false
	else
		if first._runeid > senond._runeid then
			return true
		else 
			return false
		end
	end
end

-- 替换红点
function RuneData:GetRepRed()
    if self:IsHaveEmptySlot() and #(self:GetRuneReData()) > 0 then
        return true
    else
        return false
    end 
end

function RuneData:IsLayerLock(slot)
	local aionInfo = GameWorld.Player().aion_info
	local curLayer = aionInfo[public_config.AION_NOW_LAYER] or 1 --当前永恒之塔层级	
	local unlocklevellist = GlobalParamsHelper.GetParamValue(475) -- 解锁levellist
	return curLayer<unlocklevellist[slot]
end

-- 是否空槽
function RuneData:IsHaveEmptySlot()
    local grids = GameWorld.Player().rune_grid
    for i=1,8 do
        if grids[i]==nil and not self:IsLayerLock(i) then
            return true
        end
    end
    return false
end
 -- 是否空槽或者锁
function RuneData:IsAllLockEmptySlot()
    local grids = GameWorld.Player().rune_grid
    for i=1,8 do
        if grids[i]==nil or self:IsLayerLock(i) then
        else
            return false
        end
    end
    return true
end

 -- 是否全锁
 function RuneData:IsAllLockSlot()
    local grids = GameWorld.Player().rune_grid
    for i=1,8 do
        if not self:IsLayerLock(i) then
            return false
        end
    end
    return true
end

--是否有镶嵌
function RuneData:IsHaveRuneSlot()
    local grids = GameWorld.Player().rune_grid
    for i=1,8 do
        if grids[i]~=nil then
            return true
        end
    end
    return false
end
function RuneData:GetAllRuneDes()
    local grids = GameWorld.Player().rune_grid
    local data = {}
    for l,v in pairs(grids) do      
        local item = RuneItemData(v[1],v[2])
        local att =item:GetAtt()
        if att~=nil then
            for k,num in pairs(att) do
                if data[k]==nil then
                    data[k]=0
                end
                data[k]=data[k]+num
            end
        else
            local f = item:GetExp()
            if f>0 then
                if data[332]==nil then
                    data[332]=0
                end
                data[332]=data[332]+f              
            end
        end
    end
    return data
end

function RuneData:GetRuneEliteNum()
    local num = GameWorld.Player().rune_essence
    return num
end

function RuneData:GetRuneEliteIcon()
    local id =  GlobalParamsHelper.GetParamValue(484)
    local icon = ItemDataHelper.GetIcon(id)
    return icon
end

function RuneData:GetRunePieceNum()
    local num = GameWorld.Player().rune_piece
    return num
end

function RuneData:GetRunePieceIcon()
    local id =  GlobalParamsHelper.GetParamValue(485)
    local icon = ItemDataHelper.GetIcon(id)
    return icon
end

function RuneData:UpdateFxPos(pos)
    self._fxpos = pos
end

