local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DazzleSaleDataHelper = GameDataHelper.DazzleSaleDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local SaleData = Class.SaleData(ClassTypes.XObject)
local SaleListItemData = Class.SaleListItemData(ClassTypes.XObject)
function SaleData:__ctor__(svrData)
    
end

function SaleListItemData:__ctor__(id,name,price,mode,isFirst)
    self._id = id
    self._name = name
    self._price = price
    self._mode = mode
    self._isFirst = isFirst
end

function SaleData:GetSaleListData()
    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local datalist = {}
    local data = XMLManager.dazzle_sale:Keys()
    local info = GameWorld.Player().dazzle_sale_info
    for i, v in ipairs(data) do
        if v ~= 0 then
            local p = DazzleSaleDataHelper:GetPriceFirst(v)
            local isFirst = true
            if info[v]==1 then
                p = DazzleSaleDataHelper:GetPrice(v)
                isFirst = false
            end
            local mode = DazzleSaleDataHelper:GetDisplay(v)[vocationGroup]
            local r = DazzleSaleDataHelper:GetReward(v)
            local id = r[vocationGroup][1]
            local name = ItemDataHelper.GetItemName(id)
            local itemdata = SaleListItemData(i,name,p,mode,isFirst)
            table.insert(datalist,itemdata)
        end
    end
    return datalist
end
