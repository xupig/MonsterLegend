local public_config = GameWorld.public_config
local DialogConfig = GameConfig.DialogConfig
local TaskConfig = GameConfig.TaskConfig

local DialogItemData = Class.DialogItemData(ClassTypes.XObject)

function DialogItemData:__ctor__(isAutoTask)
    self._isAutoTask = isAutoTask
end

function DialogItemData:SetDialog(dialog)
    self._dialogType = DialogConfig.DIALOG_MODE
    self._dialog = dialog
end

function DialogItemData:SetHandOver(dialog, itemsData, confirmText, callback, paras)
    self._dialogType = DialogConfig.HAND_OVER_MODE
    self._dialog = dialog
    self._itemsData = itemsData
    self._confirmText = confirmText
    self._callback = callback
    self._paras = paras
end

function DialogItemData:SetFeedback(dialog, confirmText, callback, paras)
    self._dialogType = DialogConfig.FEEDBACK_MODE
    self._dialog = dialog
    self._confirmText = confirmText
    self._callback = callback
    self._paras = paras
end

function DialogItemData:SetReward(dialog, itemsData, confirmText, callback, paras)
    self._dialogType = DialogConfig.REWARD_MODE
    self._dialog = dialog
    self._itemsData = itemsData
    self._confirmText = confirmText
    self._callback = callback
    self._paras = paras
end

function DialogItemData:SetConfirm(dialog, itemsData, confirmText, callback, paras)
    self._dialogType = DialogConfig.CONFIRM_MODE
    self._dialog = dialog
    self._itemsData = itemsData
    self._confirmText = confirmText
    self._callback = callback
    self._paras = paras
end

function DialogItemData:SetEnterInst(dialog, confirmText, callback, paras)
    self._dialogType = DialogConfig.ENTER_INST_MODE
    self._dialog = dialog
    self._confirmText = confirmText
    self._callback = callback
    self._paras = paras
end

function DialogItemData:SetTurnToNpc(npcId)
    self._npcId = npcId
end

function DialogItemData:GetIsAutoTask()
    return self._isAutoTask
end

function DialogItemData:GetType()
    return self._dialogType
end

function DialogItemData:GetDialog()
    return self._dialog
end

function DialogItemData:GetItemsData()
    return self._itemsData
end

function DialogItemData:GetConfirmText()
    return self._confirmText
end

function DialogItemData:GetTurnToNpc()
    return self._npcId
end

function DialogItemData:HasCallback()
    return self._callback ~= nil
end

function DialogItemData:DoCallback()
    self._callback(self._paras)
end

local DialogData = Class.DialogData(ClassTypes.XObject)

function DialogData:__ctor__(isAutoTask)
    self._isAutoTask = isAutoTask or false
    self._index = 0
    self._dialogs = {}
end

function DialogData:AddDialog(dialog)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetDialog(dialog)
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddFeedback(dialog, buttonId)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetFeedback(dialog, buttonId)-- 对话交互
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddHandOver(dialog, handOverData, callback, paras)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetHandOver(dialog, handOverData, 34, callback, paras)-- 交付道具
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddReward(dialog, rewardData, callback, paras)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetReward(dialog, rewardData, TaskConfig.BUTTON_GET_REWARD, callback, paras)-- 获得奖励
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddConfirm(dialog, rewardData, callback, paras)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetConfirm(dialog, rewardData, TaskConfig.BUTTON_ACCEPT, callback, paras)-- 接受任务
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddEnterInst(dialog, callback, paras)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetEnterInst(dialog, TaskConfig.BUTTON_ENTER_INST, callback, paras)-- 接受任务
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:AddLeave(dialog, callback, paras)
    local dialogItem = DialogItemData(self._isAutoTask)
    dialogItem:SetConfirm(dialog, nil, 61, callback, paras)-- 离开
    table.insert(self._dialogs, dialogItem)
    return dialogItem
end

function DialogData:GetNext()
    self._index = self._index + 1
    if self._index <= #self._dialogs then
        return self._dialogs[self._index]
    else
        return nil
    end
end
