local VocationChangeData = Class.VocationChangeData(ClassTypes.XObject)

local RoleDataHelper = GameDataHelper.RoleDataHelper

function VocationChangeData:__ctor__()
end

function VocationChangeData:Init()
end 

--根据职业组id和转数获取职业名称
--groupId:职业组id
--changeNum:转数
function VocationChangeData:GetVocationName(groupId, changeNum)
    local id = groupId * 100 + changeNum
    return RoleDataHelper.GetRoleNameTextByVocation(id)
end