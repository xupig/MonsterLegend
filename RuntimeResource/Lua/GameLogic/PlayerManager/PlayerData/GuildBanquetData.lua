local GuildDataHelper = GameDataHelper.GuildDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local GuildBanquetData = Class.GuildBanquetData(ClassTypes.XObject)

function GuildBanquetData:__ctor__()
    self.stateData = {}
    self.getState = 1
end

function GuildBanquetData:RefreshStateData(data)
    self.stateData = data
end

function GuildBanquetData:ResetData()
    self.stateData = {}
end

function GuildBanquetData:RefreshGetState(data)
    self.getState = data[1] or 1
end

-------------------------------------------
function GuildBanquetData:GetStateData()
    return self.stateData
end

function GuildBanquetData:IsInAnswerState()
    return self.stateData[1] == public_config.BONFIRE_STATE_ANSWER
end

function GuildBanquetData:CanGetItem()
    return self.getState == 0
end