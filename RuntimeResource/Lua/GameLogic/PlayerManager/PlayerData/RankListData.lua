
local public_config = require("ServerConfig/public_config")
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RankData = Class.RankData(ClassTypes.XObject)

function RankData:__ctor__(serverData, rankType)
    self._serverData = serverData
    self._rankType = rankType
end

-- uuid
function RankData:GetUUID()
    return self._serverData[public_config.RANK_KEY_CROSS_UUID]
end

-- 等级
function RankData:GetLevel()
    return StringStyleUtil.GetLevelString(self._serverData[public_config.RANK_KEY_LEVEL])
end

-- 经验
function RankData:GetExp()
    return self._serverData[public_config.RANK_KEY_EXP]
end

-- 战斗力
function RankData:GetFight()
    return StringStyleUtil.GetLongNumberString(self._serverData[public_config.RANK_KEY_COMBAT_VALUE])
end

-- 玩家名字
function RankData:GetName()
    return self._serverData[public_config.RANK_KEY_NAME]
end

-- 玩家职业名称
function RankData:GetVocation()
    return self._serverData[public_config.RANK_KEY_VOCATION]
end

--玩家外观facade
function RankData:GetFacade()
    return self._serverData[public_config.RANK_KEY_FACADE]
end

-- 爵位
function RankData:GeNobility()
    return self._serverData[public_config.RANK_KEY_NOBILITY]
end

--宠物等级信息（一阶3星）
function RankData:GetPetLevelText()
    return LanguageDataHelper.CreateContent(56232,{
        ["0"] = self:GetPetGrade(),
        ["1"] = self:GetPetStart()
    })
end

--宠物阶级
function RankData:GetPetGrade()
    return self._serverData[public_config.RANK_KEY_PET_GRADE]
end

--宠物星级
function RankData:GetPetStart()
    return self._serverData[public_config.RANK_KEY_PET_STAR]
end

--坐骑等级信息（一阶3星）
function RankData:GetHorseLevelText()
    return LanguageDataHelper.CreateContent(56232,{
        ["0"] = self:GetHorseGrade(),
        ["1"] = self:GetHorseStart()
    })
end

--坐骑阶级
function RankData:GetHorseGrade()
    return self._serverData[public_config.RANK_KEY_HORSE_GRADE]
end

--坐骑星级
function RankData:GetHorseStart()
    return self._serverData[public_config.RANK_KEY_HORSE_STAR]
end

--(翅膀、法宝、神兵、披风)等级
function RankData:GetTreasureLevel()
    return self._serverData[public_config.RANK_KEY_TREASURE_LEVEL]
end

--获取永恒之塔层数
function RankData:GetAionLevel()
    return self._serverData[public_config.RANK_KEY_AION_LAYER]
end

--获取成就点数
function RankData:GetAchievementPoint()
    return self._serverData[public_config.RANK_KEY_ACHIEVEMENT]
end

--获取离线效率
function RankData:GetOffilenExp()
    return LanguageDataHelper.CreateContent(56233,{
        ["0"] = StringStyleUtil.GetLongNumberStringEx(self._serverData[public_config.RANK_KEY_OFFLINE_EXP])
    }) 
end

--公会名
function RankData:GetGuildName()
    if self._serverData[public_config.RANK_KEY_GUILD_NAME] == "" then
        return LanguageDataHelper.CreateContent(0)
    end
    return self._serverData[public_config.RANK_KEY_GUILD_NAME]
end

function RankData:GetCross1V1Point()
    return self._serverData[public_config.RANK_KEY_CROSS_1V1_POINT]
end



function RankData:GetGemLevel()
    return self._serverData[public_config.RANK_KEY_GEM_LEVEL]
end

function RankData:GetDayCharge()
    return self._serverData[public_config.RANK_TYPE_DAY_CHARGE]
end

function RankData:GetVipStr()
    if self._serverData[public_config.RANK_KEY_VIP_LEVEL] == 0 then
        return ""
    else
        return "VIP"..self._serverData[public_config.RANK_KEY_VIP_LEVEL]
    end 
end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

-- function RankData:GetUUID()
--     return self._serverData[public_config.RANK_KEY_CROSS_UUID]
-- end

function RankData:GetFirstKey()
    local rankType = self._rankType
    if rankType == public_config.RANK_TYPE_COMBAT_VALUE or 
       rankType == public_config.RANK_TYPE_AION_TOWER or 
       rankType == public_config.RANK_TYPE_ACHIEVEMENT or
       rankType == public_config.RANK_TYPE_OFFLINE_EXP then
        return LanguageDataHelper.CreateContent(56201)--公会
    elseif rankType == public_config.RANK_TYPE_LEVEL then
        return LanguageDataHelper.CreateContent(56203)--职业名
    elseif rankType == public_config.RANK_TYPE_PET then
        return LanguageDataHelper.CreateContent(56205)--宠物名
    elseif rankType == public_config.RANK_TYPE_HORSE then
        return LanguageDataHelper.CreateContent(56207)--坐骑名
    elseif rankType == public_config.RANK_TYPE_WING then
        return LanguageDataHelper.CreateContent(56208)--翅膀名
    elseif rankType == public_config.RANK_TYPE_TALISMAN then
        return LanguageDataHelper.CreateContent(56209)--法宝名
    elseif rankType == public_config.RANK_TYPE_WEAPON then
        return LanguageDataHelper.CreateContent(56210)--神兵名
    elseif rankType == public_config.RANK_TYPE_CLOAK then
        return LanguageDataHelper.CreateContent(56211)--披风名
    end
end

function RankData:GetFirstValue()
    local rankType = self._rankType
    if rankType == public_config.RANK_TYPE_COMBAT_VALUE or 
       rankType == public_config.RANK_TYPE_AION_TOWER or 
       rankType == public_config.RANK_TYPE_ACHIEVEMENT or
       rankType == public_config.RANK_TYPE_OFFLINE_EXP then
        return self:GetGuildName()--公会
    elseif rankType == public_config.RANK_TYPE_LEVEL then
        return RoleDataHelper.GetRoleNameTextByVocation(self:GetVocation())--职业名
    elseif rankType == public_config.RANK_TYPE_PET then
        return LanguageDataHelper.CreateContent(PartnerDataHelper.GetPetName(self:GetPetGrade()))--宠物名
    elseif rankType == public_config.RANK_TYPE_HORSE then
        return LanguageDataHelper.CreateContent(PartnerDataHelper.GetHorseName(self:GetHorseGrade()))--坐骑名
    elseif rankType == public_config.RANK_TYPE_WING then   
        return LanguageDataHelper.CreateContent(4623)--翅膀名
    elseif rankType == public_config.RANK_TYPE_TALISMAN then
        return LanguageDataHelper.CreateContent(4624)--法宝名
    elseif rankType == public_config.RANK_TYPE_WEAPON then
        return LanguageDataHelper.CreateContent(4625)--神兵名
    elseif rankType == public_config.RANK_TYPE_CLOAK then
        return ""--披风名
    end
    
end

function RankData:GetSecondKey()
    local rankType = self._rankType
    if rankType == public_config.RANK_TYPE_COMBAT_VALUE then
        return LanguageDataHelper.CreateContent(56202)--战力
    elseif rankType == public_config.RANK_TYPE_LEVEL then
        return LanguageDataHelper.CreateContent(56204)--等级
    elseif rankType == public_config.RANK_TYPE_PET then
        return LanguageDataHelper.CreateContent(56206)--宠物阶数
    elseif rankType == public_config.RANK_TYPE_HORSE then
        return LanguageDataHelper.CreateContent(56206)--坐骑阶数
    elseif rankType == public_config.RANK_TYPE_WING then
        return LanguageDataHelper.CreateContent(56204)--翅膀等级
    elseif rankType == public_config.RANK_TYPE_TALISMAN then
        return LanguageDataHelper.CreateContent(56204)--法宝等级
    elseif rankType == public_config.RANK_TYPE_WEAPON then
        return LanguageDataHelper.CreateContent(56204)--神兵等级
    elseif rankType == public_config.RANK_TYPE_CLOAK then
        return LanguageDataHelper.CreateContent(56204)--披风等级
    elseif rankType == public_config.RANK_TYPE_AION_TOWER then
        return LanguageDataHelper.CreateContent(56212)--层数
    elseif rankType == public_config.RANK_TYPE_ACHIEVEMENT then
        return LanguageDataHelper.CreateContent(56213)--成就点
    elseif rankType == public_config.RANK_TYPE_OFFLINE_EXP then
        return LanguageDataHelper.CreateContent(56214)--离线效率
    end
end

function RankData:GetSecondValue()
    local rankType = self._rankType
    if rankType == public_config.RANK_TYPE_COMBAT_VALUE then
        return self:GetFight() --战力
    elseif rankType == public_config.RANK_TYPE_LEVEL then
        return self:GetLevel()--等级
    elseif rankType == public_config.RANK_TYPE_PET then
        return self:GetPetLevelText()--宠物阶数
    elseif rankType == public_config.RANK_TYPE_HORSE then
        return self:GetHorseLevelText()--坐骑阶数
    elseif rankType == public_config.RANK_TYPE_WING then
        return self:GetTreasureLevel()--翅膀等级
    elseif rankType == public_config.RANK_TYPE_TALISMAN then
        return self:GetTreasureLevel()--法宝等级
    elseif rankType == public_config.RANK_TYPE_WEAPON then
        return self:GetTreasureLevel()--神兵等级
    elseif rankType == public_config.RANK_TYPE_CLOAK then
        return self:GetTreasureLevel()--披风等级
    elseif rankType == public_config.RANK_TYPE_AION_TOWER then
        return self:GetAionLevel()--层数
    elseif rankType == public_config.RANK_TYPE_ACHIEVEMENT then
        return self:GetAchievementPoint()--成就点
    elseif rankType == public_config.RANK_TYPE_OFFLINE_EXP then
        return self:GetOffilenExp()--离线效率
    elseif rankType == public_config.RANK_TYPE_GEM then
        return self:GetGemLevel()--宝石总等级
    elseif rankType == public_config.RANK_TYPE_DAY_CHARGE then
        return self:GetDayCharge()--每日充值榜
    end
end

local RankListData = Class.RankListData(ClassTypes.XObject)

function RankListData:__ctor__()
    self._type = nil
    self._datas = {}
    self._rank = nil
end

--{}
--1.类型
--------排行榜类型------------
-- RANK_TYPE_LEVEL             = 1, --等级榜
-- RANK_TYPE_COMBAT_VALUE      = 2, --战力榜
-- RANK_TYPE_PET               = 3, --宠物
-- RANK_TYPE_HORSE             = 4, --坐骑
-- RANK_TYPE_WING              = 5, --翅膀
-- RANK_TYPE_TALISMAN          = 6, --法宝
-- RANK_TYPE_WEAPON            = 7, --神兵
-- RANK_TYPE_CLOAK             = 8, --披风
-- RANK_TYPE_AION_TOWER        = 9, --永恒之塔
-- RANK_TYPE_ACHIEVEMENT       = 10, --成就点
-- RANK_TYPE_OFFLINE_EXP       = 11, --离线效率
--2.排行列表信息
---------排行榜key定义-----------------
-- RANK_KEY_CROSS_UUID             = 1,  --用cross_uuid记录，这个也可以得到dbid，兼容假如有一个跨服排行榜
-- RANK_KEY_LEVEL                  = 2,  -- 玩家等级
-- RANK_KEY_EXP                    = 3,  -- 玩家经验
-- RANK_KEY_COMBAT_VALUE           = 4,  -- 战斗力 
-- RANK_KEY_NAME                   = 5,  -- 玩家名字
-- RANK_KEY_VOCATION               = 6,  -- 玩家职业
-- RANK_KEY_FACADE                 = 7,  --
-- RANK_KEY_NOBILITY               = 8,  -- 爵位
-- RANK_KEY_PET_GRADE              = 9,  -- 宠物阶级
-- RANK_KEY_PET_STAR               = 10, -- 宠物星级
-- RANK_KEY_PET_BLESS              = 11, -- 宠物祝福值
-- RANK_KEY_HORSE_GRADE            = 12, -- 坐骑阶级
-- RANK_KEY_HORSE_STAR             = 13, -- 坐骑星级
-- RANK_KEY_HORSE_BLESS            = 14, -- 坐骑祝福值
-- RANK_KEY_TREASURE_LEVEL         = 15, -- (翅膀、法宝、神兵、披风)等级
-- RANK_KEY_TREASURE_EXP           = 16, -- (翅膀、法宝、神兵、披风)经验
-- RANK_KEY_AION_LAYER             = 17, -- 永恒之塔层数
-- RANK_KEY_ACHIEVEMENT            = 18, -- 成就点
-- RANK_KEY_OFFLINE_EXP            = 19, -- 离线效率(每分钟获得经验值)
-- RANK_KEY_GUILD_NAME             = 20, --公会名
-- RANK_KEY_GUILD_ID               = 21, --公会id
--3.我的排行{1=uuid,2=排名}
function RankListData:ResetData(serverData)
    self._serverData = serverData
    self._type = serverData[1]
    self._rank = serverData[3][2]--nil则为未上榜
    self._datas = {}
    for i,v in ipairs(serverData[2]) do
        table.insert(self._datas, RankData(v, self._type))
    end
end

function RankListData:GetRankListData()
    return self._datas
end

function RankListData:GetMyRank()
    return self._serverData[3][2]
end