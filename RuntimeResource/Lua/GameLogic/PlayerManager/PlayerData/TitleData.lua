local TitleDataHelper = GameDataHelper.TitleDataHelper
local math = math
local attri_config = require("ServerConfig/attri_config")
local public_config = require("ServerConfig/public_config")
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig

local TitleData = Class.TitleData(ClassTypes.XObject)

function TitleData:__ctor__()
    self.titleTaskList = {}
    self._title_data = {}
end

------------------------------------称号数据----------------------------------------------------
function TitleData:RefreshTitleData(changeValue)
    if changeValue == nil then
		self._title_data = {}
	end
    if changeValue ~= nil then
        local id, _ = next(changeValue)
        if id ~= table.REMOVE_FLAG and self._title_data[id] == nil then
            --新获得称号
            GUIManager.ShowPanel(PanelsConfig.TitleTips, id)
        end
    end
    self._title_data = BaseUtil.DeepCopy(1, GameWorld.Player().title_data)
end

function TitleData:RefreshTitleTaskList(data)
    self.titleTaskList = data
end

function TitleData:RefreshTitleTaskOne(data)
    for k,v in pairs(data) do
        self.titleTaskList[k] = v
    end
end

--返回titleId的状态 0：未达成 1：达成未穿戴 2：达成且穿戴
function TitleData:GetStateByTitleId(id)
    local player = GameWorld.Player()
    if player.title_data[id] ~= nil then
        if player.title_id == id then
            return 2
        else
            return 1
        end
    else
        return 0
    end
end

function TitleData:GetStartTimestamp(id)
    local player = GameWorld.Player()
    if player.title_data[id] ~= nil then
        return player.title_data[id]
    else
        return 0
    end
end
