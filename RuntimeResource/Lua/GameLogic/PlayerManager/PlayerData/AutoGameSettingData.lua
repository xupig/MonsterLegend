--AutoGameSettingData
local AutoGameSettingData = Class.AutoGameSettingData(ClassTypes.XObject)
local SystemSettingConfig = GameConfig.SystemSettingConfig

function AutoGameSettingData:__ctor__()
    self._settingData = {}
end

function AutoGameSettingData:SetData(SystemSettingConfig,data)
    self._settingData[SystemSettingConfig] = data
end

function AutoGameSettingData:GetData(SystemSettingConfig)
    return self._settingData[SystemSettingConfig]
end

function AutoGameSettingData:GetAllData()
    return self._settingData
end

function AutoGameSettingData:LoadSettingData()
    local data = GameWorld.ReadCache(CacheConfig.AutoGameSettingData, true)
    if data == nil then
        return nil
    else
        local dataJson = GameWorld.json.decode(data[0])
        return dataJson
    end
end

function AutoGameSettingData:LoadData()
    local data = self:LoadSettingData()
    if data == nil or (type(data) == "table" and table.isEmpty(data)) then
        self._settingData[SystemSettingConfig.AUTO_GET_WHITE] = SystemSettingConfig.AUTO_GET_WHITE_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_GET_BLUE] = SystemSettingConfig.AUTO_GET_BLUE_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_GET_PURPLE] = SystemSettingConfig.AUTO_GET_PURPLE_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_GET_ORANGE] = SystemSettingConfig.AUTO_GET_ORANGE_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_GET_MONEY] = SystemSettingConfig.AUTO_GET_MONEY_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_GET_OTHER] = SystemSettingConfig.AUTO_GET_OTHER_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_SELL] = SystemSettingConfig.AUTO_SELL_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_DEVOUR] = SystemSettingConfig.AUTO_DEVOUR_DEFAULT
        self._settingData[SystemSettingConfig.AUTO_RESURRECT] = SystemSettingConfig.AUTO_RESURRECT_DEFAULT
    else
        self._settingData = self:InitData(data)
    end
    self:SaveData()
end

function AutoGameSettingData:SaveData()
    local dataStr = GameWorld.json.encode(self._settingData)
    GameWorld.SaveCache(CacheConfig.AutoGameSettingData, {dataStr}, true)
end

function AutoGameSettingData:InitData(data)
    local settingData = {}
    settingData[SystemSettingConfig.AUTO_GET_WHITE]     = data[tostring(SystemSettingConfig.AUTO_GET_WHITE)]
    settingData[SystemSettingConfig.AUTO_GET_BLUE]         = data[tostring(SystemSettingConfig.AUTO_GET_BLUE)]
    settingData[SystemSettingConfig.AUTO_GET_PURPLE]       = data[tostring(SystemSettingConfig.AUTO_GET_PURPLE)]
    settingData[SystemSettingConfig.AUTO_GET_ORANGE]       = data[tostring(SystemSettingConfig.AUTO_GET_ORANGE)]
    settingData[SystemSettingConfig.AUTO_GET_MONEY]          = data[tostring(SystemSettingConfig.AUTO_GET_MONEY)]
    settingData[SystemSettingConfig.AUTO_GET_OTHER]         = data[tostring(SystemSettingConfig.AUTO_GET_OTHER)]
    settingData[SystemSettingConfig.AUTO_SELL]        = data[tostring(SystemSettingConfig.AUTO_SELL)]
    settingData[SystemSettingConfig.AUTO_DEVOUR]        = data[tostring(SystemSettingConfig.AUTO_DEVOUR)]
    settingData[SystemSettingConfig.AUTO_RESURRECT]      = data[tostring(SystemSettingConfig.AUTO_RESURRECT)]

    return settingData
end

