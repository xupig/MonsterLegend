--福利系统
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InvestVipDataHelper = GameDataHelper.InvestVipDataHelper
local InvestLevelDataHelper = GameDataHelper.InvestLevelDataHelper
local InvestMonthDataHelper = GameDataHelper.InvestMonthDataHelper
local TotalChargeDataHelper = GameDataHelper.TotalChargeDataHelper
local InvestData = Class.InvestData(ClassTypes.XObject)
local InvestInveListItemData = Class.InvestInveListItemData(ClassTypes.XObject)
local InvestLevelListItemData = Class.InvestLevelListItemData(ClassTypes.XObject)
local InvestMonthListItemData = Class.InvestMonthListItemData(ClassTypes.XObject)
local InvestAddListItemData = Class.InvestAddListItemData(ClassTypes.XObject)
local AddChargeDays = 7
function InvestData:__ctor__(svrData)
    
end


function InvestInveListItemData:__ctor__(week,day,id,flage,canget)
    self._day = day
    self._flage = flage
    self._week = week
    self._id = id
    self._canget = canget
end

-- 是否已经领取
function InvestInveListItemData:IsGet()
    return self._flage
end
-- 第几天
function InvestInveListItemData:GetVipday()
    return self._day
end
-- 第几周期
function InvestInveListItemData:GetVipWeek()
    return self._week
end

function InvestInveListItemData:GetRewards()
    return InvestVipDataHelper:GetReward(self._id)
end

function InvestLevelListItemData:__ctor__(id,flage,type)
    self._flage = flage
    self._id = id
    self._type = type
end

-- 是否已经领取
function InvestLevelListItemData:IsGet()
    return self._flage
end

-- 能领？
function InvestLevelListItemData:VipCanGet()
    if not self._flage then
        if self:IsEnoughLevel() then
            local t = InvestData:GetLevelInvesType()
            if t==0 or self._type~=t then
                return false
            end
            return true
        end
    end
    return false
end
function InvestLevelListItemData:GetRewards()
    return InvestLevelDataHelper:GetReward(self._id)
end

function InvestLevelListItemData:GetLevel()
    return InvestLevelDataHelper:GetLevel(self._id)
end

function InvestLevelListItemData:GetType()
    return self._type
end

function InvestLevelListItemData:IsEnoughLevel()
    local level = GameWorld.Player().level
    if self:GetLevel() <= level then return true end
    return false
end


function InvestMonthListItemData:__ctor__(id,day,canget,isget)
    self._flage = canget--能领？
    self._id = id
    self._day = day
    self._isget = isget--是否领过？
end

function InvestMonthListItemData:GetRewards()
    if self._day==1 then
        local r1=InvestMonthDataHelper:GetReward(1)
        local r2=InvestMonthDataHelper:GetReward(2)
        return {r1,r2}
    end
    return InvestMonthDataHelper:GetReward(self._id)
end
-- 能否领取
function InvestMonthListItemData:IsCanGet()
    return self._flage
end

function InvestMonthListItemData:GetDay()
    return self._day
end

function InvestMonthListItemData:GetDes()
    local contentid = InvestMonthDataHelper:GetChinese(self._day)
    if contentid~=0 then
        return LanguageDataHelper.GetContent(contentid)
    end
    return ''
end
-- 
function InvestMonthListItemData:IsGet()
    return self._isget
end

function InvestAddListItemData:__ctor__(id,canget,isget)
    self._id = id
    self._canget = canget
    self._isget = isget
end

function InvestAddListItemData:GetRewards()
    return TotalChargeDataHelper:GetReward(self._id)
end

function InvestAddListItemData:GetCost()
    return TotalChargeDataHelper:GetCost(self._id)
end


function InvestData:UpdateMonth()
    local entity = GameWorld.Player()
    local flage = entity.month_card_flag or 0
    local step = entity.month_card_daily_refund_step or 0
    local items = {}
    self._totalNum=0
    self._getNum=0
    if flage==0 then
        local ids = InvestMonthDataHelper:GetAllId()
        for _,id in ipairs(ids) do
            local day = InvestMonthDataHelper:GetDay(id)
            self._totalNum=self._totalNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
            items[day]= InvestMonthListItemData(id,day,false,false)
        end
    else
        local ids = InvestMonthDataHelper:GetAllId()
        for _,id in ipairs(ids) do
            local day = InvestMonthDataHelper:GetDay(id)
            self._totalNum=self._totalNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
            if not InvestData:IsGetMonthToday() and day==step+1 then
                items[day]= InvestMonthListItemData(id,day,true,false)
            else
                if step>=day and step~=0 then
                    items[day]= InvestMonthListItemData(id,day,false,true)
                else
                    items[day]= InvestMonthListItemData(id,day,false,false)
                    self._getNum = self._getNum+InvestMonthDataHelper:GetReward(id)[public_config.MONEY_TYPE_COUPONS_BIND]
                end
            end
        end
    end
    table.sort(items,function(a,b)
        if a:IsCanGet()==true and b:IsCanGet()==true then
            if a:GetDay()<b:GetDay() then
                return true
            else
                return false
            end
        else
            if a:IsCanGet()==true then
                return true
            elseif b:IsCanGet()==true then
                return false 
            end
            if a:IsGet()==true and b:IsGet()==true then
                if a:GetDay()>b:GetDay() then
                    return true
                end
                return false
            else
                if a:IsGet()==true and b:IsGet()==false then
                    return false
                elseif a:IsGet()==false and b:IsGet()==true then 
                    return true
                end
                if a:GetDay()<b:GetDay() then
                    return true
                else
                    return false
                end
            end
        end
    end)
    return items
end
function InvestData:UpdateRemainNum()
    return self._totalNum-self._getNum
end
function InvestData:UpdateInveList()
    local week=1
    local index=1
    local items = {}
    week,index = InvestData:GetVipInvestIndex()
    local entity = GameWorld.Player()
    local worldlevel = entity.invest_vip_world_level or 1
    local rewardlists = entity.invest_vip_get_reward_days or {}
    local offvalve = InvestVipDataHelper:GetCfgTypeIndex(worldlevel)-1
    local x = 1
    for i=index+offvalve,index+6+offvalve do
        if rewardlists[index+x-1]==1 then
            items[x] = InvestInveListItemData(week,index+x-1,i,true,false)
        else
            if rewardlists[index+x-1]==0 then
                items[x] = InvestInveListItemData(week,index+x-1,i,false,true)
            else
                items[x] = InvestInveListItemData(week,index+x-1,i,false,false)
            end
        end
        x = x+1
    end
    table.sort(items,function(a,b)
        if a._canget==true and b._canget==false then
            return true
        else
            if a._canget==false and b._canget==true then
                return false
            else
                if a:GetVipday()<b:GetVipday() then
                    return true
                end
                return false
            end
        end
    end)
    return items
end

function InvestData:GetVipInvestIndex()
    local entity = GameWorld.Player()
    local rewardlists = entity.invest_vip_get_reward_days or {}
    local worldlevel = entity.invest_vip_world_level or 1
    local ids = InvestVipDataHelper:GetAllId()
    local items = {}  
    local indexs = {7,14,21,28}
    local index = 1
    local week = 1
    local y=0
    local a=0
    for k,v in pairs(rewardlists) do
        if v==0 then
            y=tonumber(k)
            break
        end
        a = a+1
    end
    local t
    if y~= 0 and y<a then
        t = math.floor(y/7)
    else
        t = math.floor(a/7)
    end
    week = t+1
    if indexs[week] then
        index = indexs[week]-6
    end
    return week,index
end

function InvestData:UpdateLevelList()
    local recordlists = GameWorld.Player().lv_invest_record or {}
    local type = self:GetLevelInvesType()
    self._total = 0
    local items = {}
    if type == 0 then 
        local types = InvestLevelDataHelper:GetAllType()
        local t = types[1]
        local cfg = InvestLevelDataHelper:GetCfgByType(t)
        for k,v in pairs(cfg) do
            items[v.id] = InvestLevelListItemData(v.id,false,t)
        end
    else
        local cfg = InvestLevelDataHelper:GetCfgByType(type)
        local x = 1
        for k,v in pairs(cfg) do
            if recordlists[v.level] then
                items[x] = InvestLevelListItemData(v.id,true,type)
            else
                items[x] = InvestLevelListItemData(v.id,false,type)
                self._total = self._total+items[x]:GetRewards()[public_config.MONEY_TYPE_COUPONS_BIND]
            end
            x = x+1
        end
        table.sort(items,function(a,b)
            if a:IsGet()==true and b:IsGet()==false then
                return false
            elseif a:IsGet()==false and b:IsGet()==true then
                return true
            else
                if a:GetLevel()<b:GetLevel() then
                    return true
                else
                    return false
                end
            end
        end)
    end
    return items
end
function InvestData:GetVipLevelByType(type)
    local items = {}
    local entity = GameWorld.Player()
    local cfg = InvestLevelDataHelper:GetCfgByType(type)
    local recordlists = entity.lv_invest_record or {}
    local hatype = entity.lv_invest_type
    local x = 1
    for k,v in pairs(cfg) do
        if recordlists[v.level] and type==hatype then
            items[x] = InvestLevelListItemData(v.id,true,type)
        else
            items[x] = InvestLevelListItemData(v.id,false,type)
            self._total = self._total+items[x]:GetRewards()[public_config.MONEY_TYPE_COUPONS_BIND]
        end
        x = x+1
    end
    table.sort(items,function(a,b)
        if a:IsGet()==true and b:IsGet()==false then
            return false
        elseif a:IsGet()==false and b:IsGet()==true then
            return true
        else
            if a:GetLevel()<b:GetLevel() then
                return true
            else
                return false
            end
        end
    end)
    return items
end

function InvestData:GetResidue()
    return self._total
end
function InvestData:GetInvestDay()
    local recordlists = GameWorld.Player().invest_vip_get_reward_days or {}
    local i=0
    for _,v in pairs(recordlists) do
        if v==1 then
            i = i + 1
        end
    end
    return i
end


function InvestData:GetVipAdd()
    local items = {}
    local ids = TotalChargeDataHelper:GetAllId()
    local enity = GameWorld.Player()
    local vocation = math.floor(enity.vocation/100)
    local record = enity.cumulative_charge_refund_info or {}
    local amount = enity.cumulative_charge_amount or 0
    local cfg = TotalChargeDataHelper:GetRewardsByVocation(vocation)
    local x=1
    for k,v in ipairs(cfg) do
        if record[v.id]==1 then
            items[x]=InvestAddListItemData(v.id,false,true)
        else
            local cost = v.cost
            if cost<=amount then 
                items[x]=InvestAddListItemData(v.id,true,false)
            else
                items[x]=InvestAddListItemData(v.id,false,false)
            end
        end
        x=x+1
    end
    table.sort(items,function(a,b)
        if a._canget==true and b._canget==false then
            return true
        else
            if a._canget==false and b._canget==true then
                return false
            elseif a._canget and b._canget then
                if a._id>b._id then
                    return false
                end
                return true
            else
                if a._isget==true and b._isget==false then
                    return false
                elseif a._isget==false and b._isget==true then                
                    return true
                else
                    if a._id>b._id then
                        return false
                    end
                    return true
                end
             
            end
        end
    end)
    return items
end

function InvestData:GetLevelInvesType()
    return GameWorld.Player().lv_invest_type or 0
end
-- 今天是否领了月卡奖励
function InvestData:IsGetMonthToday()
    local entity = GameWorld.Player()
    if entity.month_card_flag==1 and entity.month_card_daily_refund_flag==0 then
        return false
    end
    return true
end

function InvestData:GetMonthStep()
    if GameWorld.Player().month_card_flag==0 then return 30 end
    return GameWorld.Player().month_card_daily_refund_step
end
-- 是否充月卡了
function InvestData:GetIsMonthMon()
    if GameWorld.Player().month_card_flag==1 then
        return true
    end
    return false
end

-- 开服累充活动关闭状态
function InvestData:IsVipAddClosed()
    local createtime = GameWorld.Player().create_time
    local dateTime = DateTimeUtil.SomeDay(createtime)
    local _des = DateTimeUtil:DateTimeToSecond(dateTime)
    local s = DateTimeUtil.MinusSec(createtime) 
    if s>AddChargeDays*DateTimeUtil.OneDayTime-_des then
        return true
    else
        return false
    end
end


