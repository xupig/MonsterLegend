require "PlayerManager/PlayerData/EventData"

local EventData = ClassTypes.EventData

local public_config = GameWorld.public_config
local EventActivityDataHelper = GameDataHelper.EventActivityDataHelper
local GameSceneManager = GameManager.GameSceneManager

local EventActivityItemData = Class.EventActivityItemData(ClassTypes.XObject)

function EventActivityItemData:__ctor__(id, serverData)
    self._id = id
    if serverData ~= 1 then
        self:UpdateEventActivityData(serverData)
    end
end

function EventActivityItemData:UpdateEventActivityData(serverData)
    self._serverData = serverData
    if serverData == nil then return end
end

function EventActivityItemData:GetId()
    return self._id
end

function EventActivityItemData:GetName()
    return EventActivityDataHelper:GetName(self._id)
end

function EventActivityItemData:GetDesc()
    return EventActivityDataHelper:GetDesc(self._id)
end

function EventActivityItemData:GetIcon()
    return EventActivityDataHelper:GetIcon(self._id)
end

function EventActivityItemData:GetDaily()
    return EventActivityDataHelper:GetDaily(self._id)
end

function EventActivityItemData:GetPublic()
    return EventActivityDataHelper:GetPublic(self._id)
end

function EventActivityItemData:GetType()
    return EventActivityDataHelper:GetType(self._id)
end

function EventActivityItemData:GetTargets()
    return EventActivityDataHelper:GetTargets(self._id)
end

function EventActivityItemData:GetTargetsDesc()
    return EventActivityDataHelper:GetTargetsDesc(self._id)
end

function EventActivityItemData:GetTargetsScore()
    return EventActivityDataHelper:GetTargetsScore(self._id)
end

function EventActivityItemData:GetMapId()
    return EventActivityDataHelper:GetMapId(self._id)
end

function EventActivityItemData:GetPos()
    return EventActivityDataHelper:GetPos(self._id)
end

function EventActivityItemData:GetRange()
    return EventActivityDataHelper:GetRange(self._id)
end

function EventActivityItemData:GetOpenTimeId()
    return EventActivityDataHelper:GetOpenTimeId(self._id)
end

function EventActivityItemData:GetLimit()
    return EventActivityDataHelper:GetLimit(self._id)
end

function EventActivityItemData:GetFirstRewardId()
    return EventActivityDataHelper:GetFirstRewardId(self._id)
end

function EventActivityItemData:GetFixedRewardId()
    return EventActivityDataHelper:GetFixedRewardId(self._id)
end

function EventActivityItemData:GetTriggerEventIds()
    return EventActivityDataHelper:GetTriggerEventIds(self._id)
end

function EventActivityItemData:GetSingleMapId()
    return EventActivityDataHelper:GetSingleMapId(self._id)
end

function EventActivityItemData:GetTargetsInfo()
    if self._targetsData == nil then
        self._targetsData = EventData()
        self._targetsData:UpdateSortedEventData(self:GetTargets())
    end
    return self._targetsData:GetSortedEvents()
end

function EventActivityItemData:GetTotalScore()
    local targets = self:GetTargets()
    local targetsScore = self:GetTargetsScore()
    local totalScore = 0
    if targets == nil or targetsScore == nil then
        return totalScore
    end
    
    for i, v in ipairs(targets) do
        if targetsScore[v[1]] ~= nil then
            totalScore = totalScore + targetsScore[v[1]] * v[2]
        end
    end
    return totalScore
end

function EventActivityItemData:GetCurrScore()
    local targets = self:GetTargetsCurTimes()
    local targetsScore = self:GetTargetsScore()
    local currScore = 0
    if targets == nil or targetsScore == nil then
        return currScore
    end
    local subProgress = {}
    self:CountPrivateEscort(targets, subProgress)
    self:CountBossHp(targets, subProgress)
    -- LoggerHelper.Log("Ash: targets:" .. PrintTable:TableToStr(targets))
    for k, v in pairs(targets) do
        if targetsScore[k] ~= nil then
            currScore = currScore + targetsScore[k] * (v + (subProgress[k] or 0))
        end
    end
    return currScore
end

function EventActivityItemData:CountPrivateEscort(targets, subProgress)
    if self:GetType() == 6 then -- 个人护送事件 self:GetType() == 5 or
        local curMapId = GameSceneManager:GetCurrMapID()
        if self:GetSingleMapId() == curMapId then
            local targets = self:GetTargetsInfo()
            for i, eventData in ipairs(targets) do
                if eventData:GetEventID() == public_config.EVENT_ID_PRIVATE_ESCORT then
                    local monsterId = eventData:GetConds(public_config.EVENT_PARAM_MONSTER_ID)
                    local posId = eventData:GetConds(public_config.EVENT_PARAM_POINT_ID)
                    local monsters = GameWorld.GetEntityByMonsterId(monsterId)
                    local posPointMap = GameSceneManager:GetPositionPointInfo(curMapId)
                    -- LoggerHelper.Log("Ash: GetCurrScore: monsterId: " .. monsterId .. " posId: " .. posId)
                    -- LoggerHelper.Log("Ash: GetCurrScore: monster: " .. #monsters .. " posPointMap: " .. tostring(posPointMap))
                    if #monsters > 0 and posPointMap[posId] then
                        -- LoggerHelper.Log("Ash: GetCurrScore: posPointMap[posId].pos: " .. PrintTable:TableToStr(posPointMap[posId].pos)
                        --     .. " monsters[1]:GetPosition(): " .. PrintTable:TableToStr(monsters[1]:GetPosition()))
                        if self._totalDis == nil then
                            self._totalDis = Vector3.Distance(posPointMap[posId].pos, monsters[1]:GetPosition())
                        -- LoggerHelper.Log("Ash: self._totalDis:" .. self._totalDis)
                        end
                        local curDis = Vector3.Distance(posPointMap[posId].pos, monsters[1]:GetPosition())
                        -- LoggerHelper.Log("Ash: curDis:" .. curDis)
                        subProgress[eventData:GetId()] = (self._totalDis - curDis) / self._totalDis
                    -- LoggerHelper.Log("Ash: subProgress:" .. eventData:GetId() .. " " .. subProgress[eventData:GetId()])
                    end
                end
            end
        end
    end
end

function EventActivityItemData:CountBossHp(targets, subProgress)
    if self:GetType() == 7 then -- 公共BOSS事件  or self:GetType() == 8
        local targets = self:GetTargetsInfo()
        for i, eventData in ipairs(targets) do
            if eventData:GetEventID() == public_config.EVENT_ID_KILL_MONSTER
                or eventData:GetEventID() == public_config.EVENT_ID_KILL_MONSTER_TEAM then
                local monsterId = eventData:GetConds(public_config.EVENT_PARAM_MONSTER_ID)
                local monsters = GameWorld.GetEntityByMonsterId(monsterId)
                -- LoggerHelper.Log("Ash: CountBossHp: monsterId: " .. monsterId)
                -- LoggerHelper.Log("Ash: CountBossHp: monster: " .. #monsters)
                if #monsters > 0 then
                    subProgress[eventData:GetId()] = (monsters[1].max_hp - monsters[1].cur_hp) / monsters[1].max_hp
                end
            end
        end
    end
end
function EventActivityItemData:GetTargetsCurTimes()
    return self._serverData and self._serverData[public_config.TASK_INFO_KEY_TARGETS] or {}
end

function EventActivityItemData:GetState()
    return self._serverData[public_config.TASK_INFO_KEY_STATE]
end

local EventActivityData = Class.EventActivityData(ClassTypes.XObject)

function EventActivityData:__ctor__()
    self._eventActivitys = {}
    self._dailyEventActivitys = {}
    self._currEventActivitys = {}
end

function EventActivityData:ResetEventActivityData(serverData)
    self._eventActivitys = {}
    self:UpdateEventActivityData(serverData)
end

function EventActivityData:UpdateEventActivityData(serverData)
    self:InnerUpdateEventActivityData(self._eventActivitys, serverData)
end

function EventActivityData:InnerUpdateEventActivityData(localCache, serverData)
    for id, eventActivity in pairs(serverData) do
        if localCache[id] == nil then
            localCache[id] = EventActivityItemData(id, eventActivity)
        else
            localCache[id]:UpdateEventActivityData(eventActivity)
        end
    end
end

function EventActivityData:DelEventActivityData(serverData)
    for k, v in pairs(serverData) do
        local eventActivity = self._eventActivitys[k]
        if eventActivity ~= nil then
            self._eventActivitys[k] = nil
        else
            LoggerHelper.Error("Del EventActivity id not exist:" .. k)
        end
    end
end

function EventActivityData:GetEventActivityData(id)
    return self._eventActivitys[id]
end

function EventActivityData:GetEmptyEventActivityData(id)
    return EventActivityItemData(id)
end

function EventActivityData:GetCurrEventActivities()
    return self._currEventActivitys
end

function EventActivityData:ResetDailyEventActivityData(serverData)
    self._dailyEventActivitys = {}
    self:InnerUpdateEventActivityData(self._dailyEventActivitys, serverData)
end

function EventActivityData:GetDailyEventActivities()
    return self._dailyEventActivitys
end
