local OpenHappyManager = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GUIManager = GameManager.GUIManager
local DailyConfig = GameConfig.DailyConfig
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config

local FOLLOW_OPEN_PANEL = 1 -- 1.打开界面，填子功能ID
local FOLLOW_EVENT = 2 -- 2.事件,填事件名称
local FOLLOW_OPEN_MAIN_PANEL = 3 -- 3.打开界面，填功能总表ID

function OpenHappyManager:Init()
end

function OpenHappyManager:HandleResp(act_id, code, args)
    -- LoggerHelper.Log("====OpenHappyManager:HandleResp act_id = " .. act_id .. ", code =" ..code, "#ff00ffff")
    -- LoggerHelper.Log(args)
    if code ~= 0 then
        return
    end
    if act_id == action_config.FREAK_GET_INFO_REQ then --FREAK_GET_INFO_REQ = 11302, --获取任务状态
        PlayerManager.PlayerDataManager.openHappyData:SetData(args)
        EventDispatcher:TriggerEvent(GameEvents.FREAK_GET_INFO_REQ)
        self:UpdateRedPoint()

    elseif act_id == action_config.FREAK_REWARD_REQ then -- FREAK_REWARD_REQ = 11301, --领奖
        self:UpdateRedPoint()
        EventDispatcher:TriggerEvent(GameEvents.FREAK_REWARD_REQ)
    end
end

function OpenHappyManager:DoFollowEvent(follow)
    if not follow or type(follow) ~= "table" then
        LoggerHelper.Error("Invalid Event follow :" .. tostring(follow))
        return
    end

    for k, params in pairs(follow) do
        local param = params[1]
        if k == FOLLOW_OPEN_PANEL then
            GUIManager.ShowPanelByFunctionId(param)
        elseif k == FOLLOW_EVENT then
            EventDispatcher:TriggerEvent(GameEvents[param])
        elseif k == FOLLOW_OPEN_MAIN_PANEL then
            GUIManager.ShowPanelByPanelFuncId(param)
        end
    end
end

-- FREAK_GET_INFO_REQ = 11302, --获取任务状态（嗨点每次有加这个会推过来）
function OpenHappyManager:RequestState()
    GameWorld.Player().server.freak_action_req(action_config.FREAK_GET_INFO_REQ, "")
end

-- FREAK_REWARD_REQ = 11301, --领奖
function OpenHappyManager:RequestReward(id)
    GameWorld.Player().server.freak_action_req(action_config.FREAK_REWARD_REQ, tostring(id))
end

function OpenHappyManager:UpdateRedPoint()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FREAK, self:HasRedPoint())
end

function OpenHappyManager:HasRedPoint()
    return PlayerManager.ServiceActivityManager:GetIsOpenHappy() and PlayerManager.PlayerDataManager.openHappyData:HasRedPoint()
end


OpenHappyManager:Init()
return OpenHappyManager