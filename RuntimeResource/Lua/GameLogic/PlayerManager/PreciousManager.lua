local PreciousManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local RuneDataHelper = GameDataHelper.RuneDataHelper
local RuneConfigDataHelper = GameDataHelper.RuneConfigDataHelper
local RuneExchangeDataHelper = GameDataHelper.RuneExchangeDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RuneShowDataHelper = GameDataHelper.RuneShowDataHelper
local PreciousData = PlayerManager.PlayerDataManager.preciousData
local FloatTextManager = PlayerManager.FloatTextManager
local PreciousConfig = GameConfig.PreciousConfig
local AionTowerManager = PlayerManager.AionTowerManager
local RuneManager = PlayerManager.RuneManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData

function PreciousManager:Init()
	self._timer = -1
	self._needtime = 0
	self._timerfun = function() self:FreeTimer()end
	self._handlePre = function() self:HandlePreRune() end
	self._handlePkg = function() self:HandlePkg() end
	self._handleTime = function()self:HandleFreeTime()end
end

function PreciousManager:HandleData()
	-- self:HandlePkg()
	self:HandlePreRune()
	self:HandleFreeTime()
end
function PreciousManager:FreeTimer()
	self:HandleFreeTime()
	TimerHeap:DelTimer(self._timer)	
	self._timer=-1			
end
function PreciousManager:HandlePkg()
	local pkg = GameWorld.Player().rune_pkg
	local data = {}
	local runeid = 0
	local runelevel = 0
	local attconfigdata = RuneConfigDataHelper:GetRuneConfigData()
	for k,v in pairs(pkg) do
		local d = {}
		runeid = v[1]
		runelevel = v[2]--背包符文类型
		d.le = runelevel
		d.id = runeid
		d.itemid = RuneDataHelper:GetItemCom(runeid)
		local runeattconf = attconfigdata[runeid][runelevel][1]
		local att = runeattconf['atrri']
		local runeatts = {}
		local types = RuneDataHelper:GetType(runeid)
		if att ~= nil and not table.isEmpty(att) then
			local a = RuneManager:GetRuneAtt(att)				
			if types[1]==5 or types[2]==5 then
				local exp = runeattconf['exp']
				runeatts[0] = 3
				runeatts[1] = {a,exp}
			else
				runeatts[0] = 0
				runeatts[1] = a
			end
		else
			if(types[1]==5) then
				local exp = runeattconf['exp']	
				runeatts[0] = 1
				runeatts[1] = exp
			else
				local re = runeattconf['resolve']
				local a = {}
				for i,v in pairs(re) do
					local id = tonumber(i)
					local num = tonumber(v)
					local _d = {}
					_d[0] = id
					_d[1] = num
					table.insert(a,_d)
				end
				runeatts[0] = 2
				runeatts[1] = a
			end
		end
		d.atts = runeatts
		table.insert(data,d)
	end
	table.sort( data,function(a,b)
		if RuneDataHelper:GetQuality(a.id)>RuneDataHelper:GetQuality(b.id) then
			return true
		else
			return false
		end
	end)
	PreciousData:UpdateRunePkg(data)
end
function PreciousManager:HandlePreRune()
	local aionInfo = AionTowerManager:GetAionInfo()
	local curLayer = aionInfo[public_config.AION_NOW_LAYER] or 0 --当前永恒之塔层级		
	local ids = RuneDataHelper:GetAllId()
	local unlock = 0
	local itemid = 0
	local k = 1
	local q = 1
	local d ={}
	for k,id in pairs(ids) do
		unlock = RuneDataHelper:GetUnlockFloor(id)
		k = RuneDataHelper:GetIcon(id)
		q = RuneDataHelper:GetQuality(id)
		if q~=1 then
			if unlock <= curLayer then
				itemid = RuneDataHelper:GetItemCom(id)
				if d[k]== nil then
					d[k]={}
				end
				table.insert(d[k],itemid)
			end
		end
	end
	local data={}
	for i,v in pairs(d) do 
		local items=v
		if items ~=nil then
			for i,v in pairs(items) do
				table.insert(data, v)
			end
		end
	end
	PreciousData:UpdateRunePre(data)
end
local x=1
function PreciousManager:HandleResp(action_id,error_id,lua_table)
	if error_id ~= 0 then
		return
	end
	if action_id == action_config.RUNE_SINGLE_LUCK or action_id == action_config.RUNE_TEN_LUCK then
		self:HandleFreeTime()
		self:HandleFindGet(lua_table,action_id)
		self:RefreshRedEven()	
		-- EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Precious_Panel_Red_Point)
	elseif action_id == action_config.RUNE_LUCK_LIST then	
		local data = {}	
		for k,v in pairs(lua_table) do
			if not table.isEmpty(v) then
				for k1,v1 in pairs(v) do
					local time = v1[public_config.RUNE_LUCK_TIME]
					local runeid = v1[public_config.RUNE_LUCK_ID]
					local name = v1[public_config.RUNE_LUCK_AVATAR_NAME]
					local d = {time,runeid,name}
					table.insert(data, d)
				end
			end
		end
		EventDispatcher:TriggerEvent(GameEvents.RuneGetList,data)
	end
end
function PreciousManager:HandleFindGet(data,actionid)
	local count = data[1]--碎片
	local runedata = data[2]
	local getdata = {}
	local id = GlobalParamsHelper.GetParamValue(485)
	getdata[1]={ItemDataHelper.GetIcon(id),count}
	getdata[2]={}
	for k,v in ipairs(runedata) do
		local itemid = RuneDataHelper:GetItemCom(v[1])
		table.insert( getdata[2],itemid)
	end
	getdata[3]=actionid
	PreciousData:UpdateRuneFindGet(getdata)	 
end
function PreciousManager:HandleFreeTime()
	local entity = GameWorld.Player()
	local runefreetime = entity.rune_free_time or 0 --最后一次，免费寻宝时间
	local needtime = GlobalParamsHelper.GetParamValue(620)--免费时间间隔
	local siglecost = GlobalParamsHelper.GetParamValue(610)
	local sigCostId=0
	local sigCostNum=0
	for k,v in pairs(siglecost) do
		sigCostId = tonumber(k)
		sigCostNum = v
	end
	local tencost = GlobalParamsHelper.GetParamValue(611)
	local tenCostId=0
	local tenCostNum=0
	for k,v in pairs(tencost) do
		tenCostId = tonumber(k)
		tenCostNum = v
	end
	local _d = DateTimeUtil.MinusServerDay(runefreetime)
	local f = needtime-(_d['day']*24*60*60+_d['hour']*60*60+_d['min']*60+_d['sec'])
	if f<=0 then
		runefreetime =0
	end
	local havecount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(sigCostId)--entity.money_coupons or 0-- 拥有的钻石数量
	local d1 = {}
	local d2 = {}
	if runefreetime == 0 then	
		d1._curtype = 0 --免费	
	elseif sigCostNum <= havecount then
		d1._curtype = 1 
		if self._timer==-1 then
			self._timer=TimerHeap:AddSecTimer(f,0,0,self._timerfun)
		end
	else
		d1._curtype = 2
	end
	if tenCostNum <= havecount then
		d2._curtype = 0 
	else
		d2._curtype = 1 --没有	
	end
	PreciousData:UpdateRuneFind({d1,d2})
end

function PreciousManager:RefreshRedPointStatus()
	local needtime = GlobalParamsHelper.GetParamValue(620)--免费时间间隔
	local runefreetime = GameWorld.Player().rune_free_time or 0
	local _d = DateTimeUtil.MinusServerDay(runefreetime)
	local f = needtime-(_d['day']*24*60*60+_d['hour']*60*60+_d['min']*60+_d['sec'])
	local d1 = GlobalParamsHelper.GetParamValue(610)
	local d2 = GlobalParamsHelper.GetParamValue(611)
	local id,c1 = next(d1)
	local id,c2 = next(d2)
    local ownCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(id)
	if f<=0 or ownCount>=c1 or ownCount>=c2 then
		return true
	end
	return false
end

function PreciousManager:RefreshRedEven()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_SYMBOL_TREASURE, self:RefreshRedPointStatus())        	
end

-- 进入加监听
function PreciousManager:OnPlayerEnterWorld()
	self:RefreshRedEven()	
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._runePkgChange)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_RUNE_FREE_TIME, self._handleTime)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_AION_INFO, self._handlePre)
end
--退出移除监听
function PreciousManager:OnPlayerLeaveWorld()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_PKG, self._runePkgChange)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_RUNE_FREE_TIME, self._handleTime)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_AION_INFO, self._handlePre)
end

-- RPC
-- 单个
function PreciousManager.SigleFindRpc()
	GameWorld.Player().server.rune_action_req(action_config.RUNE_SINGLE_LUCK,0,0)
end
-- 10次
function PreciousManager.FindTenRpc()
	GameWorld.Player().server.rune_action_req(action_config.RUNE_TEN_LUCK,0,0)
end

function PreciousManager.RuneLucklistRpc()
	if GameWorld.Player() then
		GameWorld.Player().server.rune_action_req(action_config.RUNE_LUCK_LIST,0,0)
	end
end


--设置今日是否提醒弹窗 参数：0：提醒 1：不提醒
function PreciousManager:RuneFindSetRemindReq(state)
    GameWorld.Player().server.rune_action_req(action_config.RUNE_SET_REMIND_FLAG, state,0)
end


function PreciousManager:GetRankList()
	local funs = {}
	for _,v in ipairs(PreciousConfig.LIST) do 
		if FunctionOpenDataHelper:CheckFunctionOpen(v['funcId']) then
			table.insert(funs,v)
		end
	end
	table.sort(funs,function(a,b)
		-- local condition1 = FunctionOpenDataHelper:GetCondition(a.funcId)
		-- local condition2 = FunctionOpenDataHelper:GetCondition(b.funcId)
		if a.funcId<b.funcId then
			return true
		else
			return false
		end
	end)
	return funs
end
PreciousManager:Init()
return PreciousManager

	