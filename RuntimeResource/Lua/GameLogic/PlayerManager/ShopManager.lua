--商城Manager
local ShopManager = {}
local action_config = require("ServerConfig/action_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ShopDataHelper = GameDataHelper.ShopDataHelper

function ShopManager:Init()
end

function ShopManager:OnPlayerEnterWorld()

end

function ShopManager:OnPlayerLeaveWorld()

end

function ShopManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("ShopManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.SHOP_BUY_ITEM then
        --购买成功 args[1-商城表id 2-数量]
        local data = {["id"]=MessageBoxType.BuySucc,["value"]={["count"]=args[2],["shopId"]=args[1]}}
        GameManager.GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)

        self:SetBuyShopItemInfoLog(args[2],args[1])

    elseif action_id == action_config.SHOP_DIAMOND_TO_GOLD then
        --兑换成功
    elseif action_id == action_config.SHOP_GET_SELL_LIMIT then
        --返回限购已买信息
         GameWorld.Player().shopping_rush_items = args
         EventDispatcher:TriggerEvent(GameEvents.Refresh_Sell_Limit)
    elseif action_id == action_config.SHOP_SILVER_CHANGE_INFO then
        --返回银币兑换次数信息
        local data ={["id"]=MessageBoxType.ExchangeSilver,["value"]={args[1]}}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
    elseif action_id == action_config.SHOP_SILVER_CHANGE then
        --返回银币兑换次数信息
        local data ={["id"]=MessageBoxType.ExchangeSilver,["value"]={args[1]}}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
        if args[2] ~= 1 then
            --显示暴击提示
            GUIManager.ShowText(2, LanguageDataHelper.CreateContentWithArgs(178, {["0"] = args[2]}))
        end
    end

end

function ShopManager:OnShopWeekLimitChange()
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Week_Limit)
end

function ShopManager:OnShopDayLimitChange()
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Day_Limit)
end

-------------------------------------客户端请求-----------------------------------
--商城购买
function ShopManager:BuyShopItem(id, count, autoUse)
    local version = ShopDataHelper:GetVersion(id)
    local params = string.format("%d,%d,%d,%d", id, count, version ,autoUse)
    GameWorld.Player().server.shop_action_req(action_config.SHOP_BUY_ITEM, params)
end

--引导购买
function ShopManager:BuyGuideItem(id, count)
    local params = string.format("%d,%d", id, count)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_GUIDE_BUY,params) 
end



--商城钻石兑换金币
function ShopManager:DiamondToGold(type)
    local params = string.format("%d", type)
    GameWorld.Player().server.shop_action_req(action_config.SHOP_DIAMOND_TO_GOLD, params)
end

--商城金币兑换银币
function ShopManager:GoldToSilver()
    GameWorld.Player().server.shop_action_req(action_config.SHOP_SILVER_CHANGE, "")
end

--金币兑换银币次数信息
function ShopManager:GetGoldToSilverInfo()
    GameWorld.Player().server.shop_action_req(action_config.SHOP_SILVER_CHANGE_INFO, "")
end

-- SDKLog  购买物品
function ShopManager:SetBuyShopItemInfoLog(count,shopId)
    -- local type, price = GameDataHelper.ShopDataHelper:GetItemTypeAndUnitPrice(shopId)
    -- if tostring(type) == '2' then --钻石
    --     local shopItemId = GameDataHelper.ShopDataHelper:GetItemId(shopId)
    --     local shopItemName = GameDataHelper.ItemDataHelper.GetItemName(shopItemId)
    --     local shopItemDes = GameDataHelper.ItemDataHelper.GetDescription(shopItemId)
    --     GameWorld.onBuyItemLog(tostring(price),"buy_action",tostring(count), tostring(shopItemName),tostring(shopItemDes),true,false)
    -- elseif tostring(type) == '4' then --绑钻
    --     local shopItemId = GameDataHelper.ShopDataHelper:GetItemId(shopId)
    --     local shopItemName = GameDataHelper.ItemDataHelper.GetItemName(shopItemId)
    --     local shopItemDes = GameDataHelper.ItemDataHelper.GetDescription(shopItemId)
    --     GameWorld.onBuyItemLog(tostring(price),"buy_action",tostring(count), tostring(shopItemName),tostring(shopItemDes),true,false)   
    -- end
end

ShopManager:Init() 
return ShopManager