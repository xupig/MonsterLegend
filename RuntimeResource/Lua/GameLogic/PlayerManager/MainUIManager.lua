local MainUIManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AddRateDataHelper = GameDataHelper.AddRateDataHelper
local BuffDataHelper = GameDataHelper.BuffDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local RemindData = PlayerManager.PlayerDataManager.remindData
local RemindType = GameConfig.EnumType.RemindType

function MainUIManager:Init()
    self._csCallControlUIState = true
    self:InitCallbackFunc()
end

function MainUIManager:InitCallbackFunc()
    self._onClientTeleportBegin = function()self:ClientTeleportBegin() end
    self._onClientTeleportEnd = function()self:ClientTeleportEnd() end
    self._onTriggerSceneWaitingArea = function(sceneName)self:TriggerSceneWaitingArea(sceneName) end
    self._onRefreshStick = function(state)self:OnRefreshStick(state) end
    self._onCsCallControlUI = function(state)self:OnCsCallControlUI(state) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onBuffListChange = function(mapId)self:OnBuffListChange(mapId) end
end

function MainUIManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_TELEPORT_BEGIN, self._onClientTeleportBegin)
    EventDispatcher:AddEventListener(GameEvents.ON_CLIENT_TELEPORT_END, self._onClientTeleportEnd)
    EventDispatcher:AddEventListener(GameEvents.ON_TRIGGER_SCENE_WAITING_AREA, self._onTriggerSceneWaitingArea)
    EventDispatcher:AddEventListener(GameEvents.ON_REFRESH_STICK, self._onRefreshStick)
    EventDispatcher:AddEventListener(GameEvents.ON_CSCALL_CONTROL_UI, self._onCsCallControlUI)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    
    EventDispatcher:AddEventListener(GameEvents.UPDATE_ADD_RATE, self._onBuffListChange)
    EventDispatcher:AddEventListener(GameEvents.ON_BUFFER_CHANGE, self._onBuffListChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PK_VALUE, self._onBuffListChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PK_VALUE_ONLINE_TIME_COUNT, self._onBuffListChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_ENDTIME, self._onBuffListChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onBuffListChange)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onBuffListChange)
    EventDispatcher:AddEventListener(GameEvents.OnWorldBossGetInfo, self._onBuffListChange)
    EventDispatcher:AddEventListener(GameEvents.OnGodIslandGetInfo, self._onBuffListChange)
    --各类提醒
    
    --邮件系统 存在附件未领邮件
    self._activityAdd = function(args,remindType)self:ActivityAddRemind(args,remindType) end
    EventDispatcher:AddEventListener(GameEvents.ActivityAddRemind, self._activityAdd)
    --2不存在附件未领邮件 移除  无参数
    self._activityRemove = function(remindType)self:ActivityRemoveRemind(remindType) end
    EventDispatcher:AddEventListener(GameEvents.ActivityRemoveRemind, self._activityRemove)
    
    


--self._activityAdd = function(id,clickFunc,endTime)self:ActivityAddRemind(id,clickFunc,endTime) end
--EventDispatcher:AddEventListener(GameEvents.ADD_MAIN_REMIND_ICON, self._activityAdd)
--EventDispatcher:AddEventListener(GameEvents.ADD_MAIN_MENU_ICON, self._activityAdd)
--self._activityRemove = function(id)self:ActivityRemoveRemind(id) end
--EventDispatcher:AddEventListener(GameEvents.REMOVE_MAIN_REMIND_ICON, self._activityRemove)
end

function MainUIManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_BEGIN, self._onClientTeleportBegin)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLIENT_TELEPORT_END, self._onClientTeleportEnd)
    EventDispatcher:RemoveEventListener(GameEvents.ON_TRIGGER_SCENE_WAITING_AREA, self._onTriggerSceneWaitingArea)
    EventDispatcher:RemoveEventListener(GameEvents.ON_REFRESH_STICK, self._onRefreshStick)
    EventDispatcher:RemoveEventListener(GameEvents.ON_CSCALL_CONTROL_UI, self._onCsCallControlUI)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PK_VALUE, self._onBuffListChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PK_VALUE_ONLINE_TIME_COUNT, self._onBuffListChange)
    EventDispatcher:RemoveEventListener(GameEvents.UPDATE_ADD_RATE, self._onBuffListChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_BUFFER_CHANGE, self._onBuffListChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_ENDTIME, self._onBuffListChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onBuffListChange)

    --各类提醒
    EventDispatcher:RemoveEventListener(GameEvents.ActivityAddRemind, self._activityAdd)
    EventDispatcher:RemoveEventListener(GameEvents.ActivityRemoveRemind, self._activityRemove)

    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onBuffListChange)
    EventDispatcher:RemoveEventListener(GameEvents.OnWorldBossGetInfo, self._onBuffListChange)
    EventDispatcher:RemoveEventListener(GameEvents.OnGodIslandGetInfo, self._onBuffListChange)
--EventDispatcher:RemoveEventListener(GameEvents.ADD_MAIN_REMIND_ICON, self._activityAdd)
--EventDispatcher:RemoveEventListener(GameEvents.REMOVE_MAIN_REMIND_ICON, self._activityRemove)
end



function MainUIManager:OnBuffListChange()
    EventDispatcher:TriggerEvent(GameEvents.BUFF_LIST_CHANGE)
end


function MainUIManager:ClientTeleportBegin()
    self._hideSkillControlByTeleport = true
    self:UpdateSkillPanelActive()
end

function MainUIManager:ClientTeleportEnd()
    self._hideSkillControlByTeleport = false
    self:UpdateSkillPanelActive()
end

function MainUIManager:TriggerSceneWaitingArea(sceneName)
    local curSceneName = GameSceneManager:GetSceneName()
    local sceneInLoading = GameSceneManager:GetInLoading()
    -- LoggerHelper.Log("Ash: TriggerSceneWaitingArea: " .. sceneName .. ", " .. curSceneName .. ", " .. tostring(sceneInLoading))
    if curSceneName == sceneName and sceneInLoading then
        GUIManager.ShowPanel(PanelsConfig.WaitingLoadScene, sceneName)
    end
end

function MainUIManager:OnRefreshStick(state)
    self._hideStickFly = state
    self:UpdateStickPanelActive()
    self:UpdateSkillPanelActive()
end

function MainUIManager:OnCsCallControlUI(state)
    self._csCallControlUIState = state
    self:UpdateSkillPanelActive()
end

function MainUIManager:OnEnterMap(mapId)
    self:UpdateStickPanelActive()
    self:UpdateSkillPanelActive()
end


function MainUIManager:UpdateSkillPanelActive()
    --LoggerHelper.Log("Ash: MainMenuPanel OnShow:" .. tostring(self._isShowTrackedTask) .. tostring(self._isShowSubMenu))
    local sceneType = GameSceneManager:GetCurSceneType()
    local mapId = GameSceneManager:GetCurrMapID()
    if sceneType == SceneConfig.SCENE_TYPE_MAIN -- 在主城隐藏
        -- or sceneType == SceneConfig.SCENE_TYPE_WEDDING -- 在婚礼场景隐藏
        or sceneType == SceneConfig.SCENE_TYPE_ANSWER --在答题场景隐藏
        or mapId == public_config.MAP_ID_NEW --新手场景
        or self._hideSkillControlByTeleport -- 传送中隐藏
        or self._hideStickFly --自动飞行时
        or not self._csCallControlUIState -- CS调过来要关
        or GameWorld.Player().inSafeArea
    then
        -- LoggerHelper.Log("Ash: MainMenuPanel UpdateSkillPanelActive: true sceneType: " .. sceneType .. " mapId: " .. mapId
        --     .. " self._csCallControlUIState: " .. tostring(self._csCallControlUIState) .. " GameWorld.Player().inSafeArea: " .. tostring(GameWorld.Player().inSafeArea))
        GUIManager.ClosePanel(PanelsConfig.SkillControl)-- 注意技能面板开关必须要走这里控制，不能单独控制开关，有疑问找永健
    else
        -- LoggerHelper.Log("Ash: MainMenuPanel UpdateSkillPanelActive: false")
        GUIManager.ShowPanel(PanelsConfig.SkillControl)-- 注意技能面板开关必须要走这里控制，不能单独控制开关，有疑问找永健
    end
end

function MainUIManager:UpdateStickPanelActive()
    local mapId = GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: MainMenuPanel UpdateStickPanelActive:"
    --     .. tostring(self._hideStickFly)
    --     .. tostring(mapId == public_config.MAP_ID_NEW)
    --     .. tostring(GameWorld.Player().isWatchWar)
    -- )
    if self._hideStickFly
        or mapId == public_config.MAP_ID_NEW
        or GameWorld.Player().isWatchWar --观战隐藏
    then
        GUIManager.ClosePanel(PanelsConfig.Stick)
    else
        GUIManager.ShowPanel(PanelsConfig.Stick)
    end
end

function MainUIManager:TriggerSceneWaitingArea(sceneName)
    local curSceneName = GameSceneManager:GetSceneName()
    local sceneInLoading = GameSceneManager:GetInLoading()
    -- LoggerHelper.Log("Ash: TriggerSceneWaitingArea: " .. sceneName .. ", " .. curSceneName .. ", " .. tostring(sceneInLoading))
    if curSceneName == sceneName and sceneInLoading then
        GUIManager.ShowPanel(PanelsConfig.WaitingLoadScene, sceneName)
    end
end

function MainUIManager:GetCurBuffList()
    local _buffList = {}
    local pkValue = GameWorld.Player().pk_value
    if pkValue > 0 then
        local pkTime = GameWorld.Player().pk_value_online_time_count
        --LoggerHelper.Log("pkValue=======pkTime=============" .. pkTime)
        local pkData = GlobalParamsHelper.GetParamValue(832)
        --LoggerHelper.Error("pkData"..PrintTable:TableToStr(pkData))
        local buffTotalTime = tonumber(GlobalParamsHelper.GetParamValue(443))
        local buffTime = (pkValue - 1) * buffTotalTime + (buffTotalTime - pkTime)
        local buffName = LanguageDataHelper.CreateContent(pkData[2])
        local buffDesc = LanguageDataHelper.CreateContent(pkData[3])
        local buffIcon = pkData[1]
        local buffSpecial = GameWorld.Player().pk_value
        local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = buffTime, _special = buffSpecial}
        table.insert(_buffList, _buffItem)
    end
    
    
    
    local addInfo = GameWorld.Player().add_rate_info
    if addInfo then
        local nowTime = DateTimeUtil.GetServerTime()
        for i, addItem in ipairs(addInfo) do
            --AddRateDataHelper
            local addInfoData = GlobalParamsHelper.GetParamValue(833)
            for k, addItemInfo in pairs(addInfoData) do
                if k == addItem[1] then
                    if nowTime - addItem[2] >= addItem[3] then
                        else
                        local buffTime = addItem[3] - (nowTime - addItem[2])
                        local buffName = LanguageDataHelper.CreateContent(addItemInfo[2])
                        local buffDesc = LanguageDataHelper.CreateContent(addItemInfo[3])
                        local buffIcon = addItemInfo[1]
                        local buffSpecial = -1
                        local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = buffTime, _special = buffSpecial}
                        table.insert(_buffList, _buffItem)
                    end
                end
            end
        end
    end
    
    
    
    local addInfoData = GlobalParamsHelper.GetParamValue(833)
    local vipData = GlobalParamsHelper.GetParamValue(837)
    
    local playerVipLevel = GameWorld.Player().vip_level
    local endTime = GameWorld.Player().vip_endtime
    local nowTime = DateTimeUtil.GetServerTime()
    local dTime = math.max(0, endTime - nowTime)
    local dDay = math.ceil(dTime / DateTimeUtil.OneDayTime)
    if dDay > 0 then
        local vipData = GlobalParamsHelper.GetParamValue(837)
        for k, vipItemData in pairs(vipData) do
            if GameWorld.Player().vip_level == k then
                local buffName = LanguageDataHelper.CreateContent(vipItemData[2])
                local buffDesc = LanguageDataHelper.CreateContent(vipItemData[3])
                local buffIcon = vipItemData[1]
                local buffSpecial = -1
                local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = -1, _special = buffSpecial, _day = dDay}
                table.insert(_buffList, _buffItem)
            end
        end
    end


    local tireCount = GameWorld.Player().revive_tire_count
    local endTime = GameWorld.Player().revive_tire_del_time
    local curTime = DateTimeUtil.GetServerTime()
    if endTime > 0 then
        if curTime - endTime < 0 then
            local itemData = GlobalParamsHelper.GetParamValue(835)
            local buffTime = endTime - curTime
            local buffName = LanguageDataHelper.CreateContent(itemData[2])
            local buffDesc = LanguageDataHelper.CreateContent(itemData[3])
            local buffIcon = itemData[1]
    
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = tireCount
            local buffSpecial = LanguageDataHelper.CreateContent(56094,argsTable)
            local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = buffTime, _special = buffSpecial}
            table.insert(_buffList, _buffItem)
        end
    end
    
    local buffList = GameWorld.Player():GetCurBufferIDList()
    if buffList then
        
        for i, v in ipairs(buffList) do
            
            local isShow = BuffDataHelper:GetClientShow(tonumber(v))
            if isShow then
                if isShow == 1 then
                    local buffName = BuffDataHelper:GetBuffName(tonumber(v))
                    local buffDesc = BuffDataHelper:GetBuffDesc(tonumber(v))
                    local buffIcon = BuffDataHelper:GetBuffIcon(tonumber(v))
                    local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = -1, _special = -1}
                    table.insert(_buffList, _buffItem)
                
                end
            end
        
        end
    end
    
    local titleInfoData = GlobalParamsHelper.GetParamValue(893)
    --LoggerHelper.Error("titleInfoData"..PrintTable:TableToStr(titleInfoData))
    local player = GameWorld.Player()
    for k, titeleItem in pairs(titleInfoData) do
        if player.title_data[k] ~= nil then
            local buffName = LanguageDataHelper.CreateContent(tonumber(titeleItem[1]))
            local buffDesc = LanguageDataHelper.CreateContent(tonumber(titeleItem[2]))
            local buffIcon = tonumber(titeleItem[3])
            local _buffItem = {_name = buffName, _desc = buffDesc, _icon = buffIcon, _time = -1, _special = -1}
            table.insert(_buffList, _buffItem)
        end
    end
    
    
    return _buffList
end

--游戏提醒类型
-- EnumType.RemindType = {}
-- EnumType.RemindType.BagFill = 1 --背包是否满
-- EnumType.RemindType.MailUnRead = 2 --邮件未读
-- EnumType.RemindType.ConvoyHave = 3 --是否在护送中
-- EnumType.RemindType.GuildGetRed = 4 --公会是否有可以拿的红包
-- EnumType.RemindType.DailyResource = 5 --日常活动是否有可进行的资源
-------------------------------------------------------------各类提醒--------------------------------------------------------------------------

function MainUIManager:ActivityAddRemind(args,remindType)
    RemindData:SetChangeList(remindType, true)
    RemindData:SetChangePathList(remindType, args)
    EventDispatcher:TriggerEvent(GameEvents.RemindListChange)
end

function MainUIManager:ActivityRemoveRemind(remindType)
    RemindData:SetChangeList(remindType, false)
    EventDispatcher:TriggerEvent(GameEvents.RemindListChange)
end


-- function MainUIManager:ActivityAddRemind(id, clickFunc, endTime)
--     --LoggerHelper.Error("MainUIManager:ActivityAddRemind")
--     --LoggerHelper.Error("MainUIManager:ActivityAddRemind"..endTime)
--     RemindData:SetAddLimitList(id, clickFunc, endTime)
--     EventDispatcher:TriggerEvent(GameEvents.LimitRemindListChange)
-- end

-- function MainUIManager:ActivityRemoveRemind(id)
--     --LoggerHelper.Error("MainUIManager:ActivityRemoveRemind")
--     RemindData:SetRemoveLimitList(id)
--     EventDispatcher:TriggerEvent(GameEvents.LimitRemindListChange)
-- end








MainUIManager:Init()
return MainUIManager
