local CheckOtherPlayerManager = {}

local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local ChatManager = PlayerManager.ChatManager

function CheckOtherPlayerManager:Init()
end

function CheckOtherPlayerManager:HandleResp(act_id, code, args)
	if code > 0 then
		return
	end
	if act_id == action_config.PEEP_SEND_INFO then
		self:HandleOtherPlayerData(args)
	else
	end
end

--查看玩家返回处理
function CheckOtherPlayerManager:HandleOtherPlayerData(args)
	GUIManager.ShowPanel(PanelsConfig.CheckOtherPlayer,args)
end

--查看玩家信息
function CheckOtherPlayerManager:RequestCheckPlayer(playerName)
	GameWorld.Player().server.peep_action_req(action_config.PEEP_OTHER_INFO,playerName)
end

--超链接根据名字查看获得玩家dbid
function CheckOtherPlayerManager:RequestGetPlayerDBIdByName(playerName)
	GameWorld.Player().server.inquire_other_info_req(playerName)
end


----------------------UserMgr数据里面key------------------------
    -- USERINFO_KEY_MAILBOX          = 1,     --用户信息,base的mailbox str
    -- USERINFO_KEY_DBID             = 2,     --dbid
    -- USERINFO_KEY_NAME             = 3,     --姓名
    -- USERINFO_KEY_GENDER           = 4,     --性别
    -- USERINFO_KEY_VOCATION         = 5,     --职业
    -- USERINFO_KEY_LEVEL            = 6,     --等级
    -- USERINFO_KEY_ONLINE           = 7,     --是否在线 1:在线 0：否

--超链接根据名字查看获得玩家dbid返回处理
function CheckOtherPlayerManager:ResponseGetPlayerDBIdByName(args)
	local dbid = args[public_config.USERINFO_KEY_DBID]
	if dbid == GameWorld.Player().dbid then
		return
	end
	local d = {}
	d.playerName = args[public_config.USERINFO_KEY_NAME]
    d.dbid = dbid
	local posVec = ChatManager.ClickPosition
    posVec.x =  posVec.x
    posVec.y =  posVec.y
    d.listPos = posVec
    GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu,d)
end

CheckOtherPlayerManager:Init()
return CheckOtherPlayerManager