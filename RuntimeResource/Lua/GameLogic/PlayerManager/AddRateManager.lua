--加成Manager
local AddRateManager = {}
local action_config = GameWorld.action_config

function AddRateManager:Init()
end

function AddRateManager:HandleData(action_id,error_id,args)
	if action_id == action_config.AVATAR_UPDATE_ADD_RATE then
		self:UpdateAddRate(args)
	else
	end
end

function AddRateManager:UpdateAddRate(args)
	GameWorld.Player().add_rate_info = args
	EventDispatcher:TriggerEvent(GameEvents.UPDATE_ADD_RATE)
end

AddRateManager:Init() 
return AddRateManager