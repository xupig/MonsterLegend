local StarManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local RouletteData = PlayerManager.PlayerDataManager.rouletteData
local NormalYunpanDataHelper = GameDataHelper.NormalYunpanDataHelper
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local GUIManager = GameManager.GUIManager
local StarVeinDataHelper = GameDataHelper.StarVeinDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function StarManager:Init()
	self._refreshRed = function() self:RefreshRedPoint() end
end

-- RPC
--升级
function StarManager.RouletteRewardRpc()
	GameWorld.Player().server.zodiac_action_req(action_config.ZODIAC_UPDATE_REQ,'')
end

--Resp
function StarManager:HandleResp(action_id,error_id,lua_table)
	if error_id~=0 then
		return
	end
	if action_id == action_config.ZODIAC_UPDATE_REQ then
		
	end
end

function StarManager:RefreshRedPoint()
	EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_STAR, self:RefreshResolveRedPointState())      
end

function StarManager:RefreshResolveRedPointState()
	local info = GameWorld.Player().zodiac_info 
    local mater = GameWorld.Player().zodiac_material 
    local rank = info[public_config.ZODIAC_RANK] or 1
	local level = info[public_config.ZODIAC_LEVEL] or 0
    local curid = StarVeinDataHelper:GetDataByRankLevel(rank,level)
	if StarVeinDataHelper:IsFunRank(curid+1) then
		return false
	else
		if StarVeinDataHelper:IsFunLevel(rank,level) then
			local nextid = StarVeinDataHelper:GetDataByRankLevel(rank+1,0)
			local cost = StarVeinDataHelper:GetCost(nextid)
			if cost<=mater then 
				return true 
			end 
		else
			local cost = StarVeinDataHelper:GetCost(curid)
			if cost<=mater then 
				return true 
			end 
		end
	end
	return false
end
-- 进入加监听
function StarManager:OnPlayerEnterWorld()		
	self:RefreshRedPoint()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ZODIAC_MATERIAL, self._refreshRed)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_ZODIAC_INFO, self._refreshRed)
end
--退出移除监听
function StarManager:OnPlayerLeaveWorld()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ZODIAC_MATERIAL, self._refreshRed)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_ZODIAC_INFO, self._refreshRed)	
end
StarManager:Init()
return StarManager

	