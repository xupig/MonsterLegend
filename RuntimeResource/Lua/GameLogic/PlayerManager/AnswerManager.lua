local AnswerManager = {}

local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local action_config = GameWorld.action_config
local AnswerConfig = GameConfig.AnswerConfig
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TeamManager = PlayerManager.TeamManager

function AnswerManager:Init()
    self._showList = {}
    self._showing = false
    self:InitCallbackFunc()
end

function AnswerManager:InitCallbackFunc()
    self._showPanelCallBack = function() self:ShowPanelCallBack() end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._enterAnswer = function() self:EnterAnswer() end
    self._changeAnswer = function(target_scene, target_point_ids) self:ChangeAnswer(target_scene, target_point_ids) end
end

function AnswerManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.Enter_Happy_Answer, self._enterAnswer)
end

function AnswerManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.Enter_Happy_Answer, self._enterAnswer)
end

function AnswerManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_ANSWER then
        self:ClearTickTimer()
        EventDispatcher:RemoveEventListener(GameEvents.TeleportPosition, self._changeAnswer)
    end
    self._showList = {}
    self._showing = false
end

function AnswerManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_ANSWER then
        -- GameWorld.SyncTime()
        local posX = GlobalParamsHelper.GetParamValue(879)--116.8
        local x = GameWorld.Player():GetPosition().x
        if x < 116.8 then
            self._answer = 1
        else
            self._answer = 2
        end
        self:Answer(self._answer)
        -- self._answer = 1--1:左边；2：右边
        EventDispatcher:AddEventListener(GameEvents.TeleportPosition, self._changeAnswer)
        -- InstanceManager:EnterInstance()
        -- GameWorld.StartSyncServerTime()
    end    
end

function AnswerManager:GetAnswer()
    return self._answer
end

function AnswerManager:ChangeAnswer(target_scene, target_point_ids)
    local data = GlobalParamsHelper.GetParamValue(876)
    for k,v in pairs(data) do
        local point = v[2]
        if table.containsValue(target_point_ids, point) then
            self._answer = k
            EventDispatcher:TriggerEvent(GameEvents.ANSWER_CHANGE, self._answer)
            break
        end
    end
end

function AnswerManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        -- LoggerHelper.Log("AnswerManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.TOWER_DEFENSE_ENTER then --8801, --进入答题系统场景
        -- LoggerHelper.Log("sam 8801 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.QUESTION_ANSWER then --8802, --回答(answer)
        -- LoggerHelper.Error("sam 8802  结果== " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.QUESTION_NOTIFY_STATE then --8803, --通知活动状态
        -- 1.状态
        -- 2.开始时间
        -- 3.选题开始时间
        -- 4.第几题
        -- 5.题目id
        -- 状态：
        -- QUESTION_STATE_NONE               = 0,
        -- QUESTION_STATE_PREPARE            = 1,
        -- QUESTION_STATE_ANSWER             = 2,  --真正回答开始
        -- QUESTION_STATE_FINISH_ANSWER      = 3,  --回答结束，但活动时间还没结束，需要等待看排行，然后再全部拉出场景
        -- LoggerHelper.Log("sam 8803 == " .. PrintTable:TableToStr(args))
        local state = args[1]
        if state == public_config.QUESTION_STATE_NONE then
            self:ClearTickTimer()
        elseif state == public_config.QUESTION_STATE_PREPARE then
            --准备开始阶段
            local data = {}
            data.type = AnswerConfig.SHOW_BEGIN_COUNT_DOWN
            data.instanceTime = public_config.QUESTION_PREPARE_TIME
            data.startTime = args[2]
            self:ShowAnswerPanel(data)
        elseif state == public_config.QUESTION_STATE_ANSWER then
            --答题阶段
            local data = {}
            data.type = AnswerConfig.SHOW_ANSWER_INFO
            data.startTime = args[2]
            data.questionBeginTime = args[3]
            data.num = args[4]
            data.titleId = args[5]
            self:ShowAnswerPanel(data)
        elseif state == public_config.QUESTION_STATE_FINISH_ANSWER then
            --结束答题阶段
            local data = {}
            data.type = AnswerConfig.SHOW_END_COUNT_DOWN
            data.instanceTime = public_config.QUESTION_PREPARE_TIME
            data.startTime = args[2]
            self:ShowAnswerPanel(data)
        end

        self:AddTickTimer()

    elseif action_id == action_config.QUESTION_NOTIFY_EXP then --8804, --通知统计的累计经验和积分
        -- LoggerHelper.Error("sam 8804 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.type = AnswerConfig.SHOW_EXP_SCORE
        data.exp = args and args[1] or 0
        data.score = args and args[2] or 0
        self:ShowAnswerPanel(data)
    elseif action_id == action_config.QUESTION_RANK_LIST then --8805, --排行榜
        -- LoggerHelper.Error("sam 8805 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.type = AnswerConfig.SHOW_ANSWER_RANK
        data.rankData = args
        self:ShowAnswerPanel(data)
    elseif action_id == action_config.QUESTION_NOTIFY_ANSWER then --8806, --通知答题对错
        -- LoggerHelper.Error("sam 8806 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.type = AnswerConfig.SHOW_ANSWER_RESULT
        data.result = args[1] == 1
        self:ShowAnswerPanel(data)
    else
        LoggerHelper.Error("AnswerManager HandleData action id not handle:" .. action_id)
    end
end

--添加副本计时器
function AnswerManager:AddTickTimer()
    if self._tickTimer == nil then
        self._tickTimer = TimerHeap:AddSecTimer(0, 1, 0, function()
            local data = {}
            data.type = AnswerConfig.INSTANCE_TICK_TIMER
            self:ShowAnswerPanel(data)
        end)
    end
end

function AnswerManager:ClearTickTimer()
    if self._tickTimer ~= nil then
        TimerHeap:DelTimer(self._tickTimer)
        self._tickTimer = nil
    end
end

function AnswerManager:ShowAnswerPanel(data)
    local setting = GUIManager._panelSettings[PanelsConfig.Answer]
    if not setting.isActive then
        if self._showing then
            table.insert(self._showList, data)
        else
            self._showing = true
            GUIManager.ShowPanel(PanelsConfig.Answer, data, self._showPanelCallBack)
        end
    else
        GUIManager.ShowPanel(PanelsConfig.Answer, data)
    end
end

function AnswerManager:ShowPanelCallBack()
    self._showing = false
    for i,v in ipairs(self._showList) do
        GUIManager.ShowPanel(PanelsConfig.Answer, v)
    end
end

function AnswerManager:CanEnter()
    return true
end

--8801, --进入答题系统场景
function AnswerManager:EnterAnswer()
    local cb = function() self:GoToInstance() end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function AnswerManager:GoToInstance()
    GameWorld.Player().server.question_action_req(action_config.QUESTION_ENTER, "")
end

--8802, --回答(answer) 
--answer: 1:左边；2：右边
function AnswerManager:Answer(answer)
    GameWorld.Player().server.question_action_req(action_config.QUESTION_ANSWER, tostring(answer))
end

--8805, --排行榜
function AnswerManager:GetRankList()
    GameWorld.Player().server.question_action_req(action_config.QUESTION_RANK_LIST, "")
end

AnswerManager:Init()
return AnswerManager