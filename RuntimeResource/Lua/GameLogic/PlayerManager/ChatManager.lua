--ChatManager.lua
ChatManager = {}
local GuildManager = GameManager.GUIManager
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local error_code = GameWorld.error_code
local SystemInfoManager = GameManager.SystemInfoManager
local DateTimeUtil = GameUtil.DateTimeUtil
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local FontStyleHelper = GameDataHelper.FontStyleHelper
local LinkTypesMap = ChatConfig.LinkTypesMap
local InstanceManager = PlayerManager.InstanceManager

function ChatManager.Init()
    ChatManager.InitVoice()
    ChatManager:InitCallback()
end

-- ====================================聊天信息实体=================================================
local ChatServerData = Class.ChatServerData(ClassTypes.XObject)
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ChatDataHelper = GameDataHelper.ChatDataHelper
local SystemInfoDataHelper = GameDataHelper.SystemInfoDataHelper
local TipsManager = GameManager.TipsManager
local ServerListManager = GameManager.ServerListManager

local base = _G
local tostring = base.tostring

-- 1 是频道， 2是说话人的dbid， 3是说话人的名字， 4是说话人icon，5是说话人等级，6是说话内容
function ChatServerData:__ctor__()
    self.channel = 0
    self.uuid = 0
    self.dbid = 0
    self.name = ''
    self.vocation = 1
    self.level = 1
    self.vipLevel = 0
    self.content = ''
    self.photoFrameId = 0
    self.chatFrameId = 0
end

--（cross_uuid, dbid, name, vocation, level, vip_level）
function ChatServerData:SetData(channel_id, paramstr, contentstr)
    self.channel = channel_id
    self.paramstr = paramstr
    --self.liststr = liststr or ''
    local bs = string.split(paramstr, ",")
    
    
    if bs ~= nil then
        self.uuid = bs[1]
        
        local uuids = string.split(self.uuid, "_")
        if uuids ~= nil then
            self.serId = tonumber(uuids[1])
            --LoggerHelper.Error("self.serId"..self.serId)
            self.serIdName = ServerListManager:GetServerNameById(tostring(self.serId))
        
        end
        
        self.dbid = bs[2]
        self.name = bs[3]
        self.vocation = tonumber(bs[4])
        self.level = tonumber(bs[5])
        self.vipLevel = tonumber(bs[6])
        self.photoFrameId = tonumber(bs[7])
        self.chatFrameId = tonumber(bs[8])
    end
    
    
    
    self.content = contentstr
    self.show = false
    self.smallShow = false
end

-- 处理物品
function ChatServerData:HandleItem(textStr, liststr)
    --LoggerHelper.Error("HandleItem"..liststr)
    if liststr ~= "" then
        local itemSerData = PrintTable:StringToTable(tostring(liststr))
        local cacheIndex = ChatManager.itemCacheIndex
        ChatManager.itemCache[cacheIndex] = itemSerData
        for w in string.gmatch(textStr, "&&(%d+)&&") do
            local index = tonumber(w)
            local itemText = ChatServerData:ItemText(index, cacheIndex, itemSerData)
            
            textStr = string.gsub(textStr, '&&' .. index .. '&&', itemText)
        end
        ChatManager.itemCacheIndex = cacheIndex + 1
    end
    
    return textStr
end

--匹配物品数据
function ChatServerData:ItemText(index, cacheIndex, itemSerData)
    if itemSerData == nil then
        return '&&' .. index .. '&&'
    end
    
    local sendText = ''
    if itemSerData[index] then
        local arg = {}
        local itemId = itemSerData[index][1][public_config.ITEM_KEY_ID]
        local quality = ItemDataHelper.GetQuality(itemId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local color = FontStyleHelper.GetFontStyleData(colorId).color
        local itemData = ChatManager.itemCache[cacheIndex][index]
        --套装名字特殊处理
        local prefix = ""
        if itemData[2] == public_config.PKG_TYPE_LOADED_EQUIP then
            if itemData[7] then
                local equipType = ItemDataHelper.GetType(itemId)
                local suitLevel = itemData[7][equipType]
                if suitLevel then
                    prefix = "[" .. LanguageDataHelper.CreateContent(523 + suitLevel) .. "]"
                end
            end
        end
        arg["0"] = LinkTypesMap.Item .. "," .. index .. "," .. cacheIndex
        arg["1"] = color
        arg["2"] = '[' .. prefix .. ItemDataHelper.GetItemName(tonumber(itemId)) .. ']'
        sendText = LanguageDataHelper.CreateContent(80532, arg)
    end
    
    return sendText
end


-----------------聊天频道类型-----------------
-- CHAT_CHANNEL_SYSTEM         = 1, --系统频道
-- CHAT_CHANNEL_WORLD          = 2, --世界频道
-- CHAT_CHANNEL_VICINITY       = 3, --附近的人聊天
-- CHAT_CHANNEL_SEEK_TEAM      = 4, --组队(喊话)
-- CHAT_CHANNEL_TEAM           = 5, --队伍频道
-- CHAT_CHANNEL_GUILD          = 6, --公会频道
-- CHAT_CHANNEL_CROSS_WORLD    = 7, --跨服频道
-- CHAT_CHANNEL_HORN           = 8, --小喇叭
ChatManager.CurChannel = public_config.CHAT_CHANNEL_WORLD
-- =============================定义变量=====================================================================
ChatManager.MAX_CHAT_NUM = 50 -- 聊天显示最大数量
ChatManager.MAX_LATEST_CHAT_NUM = 5

--对应public_config里频道类型的映射
local chatReflect = {public_config.CHAT_CHANNEL_SYSTEM, public_config.CHAT_CHANNEL_WORLD
    , public_config.CHAT_CHANNEL_VICINITY, public_config.CHAT_CHANNEL_SEEK_TEAM
    , public_config.CHAT_CHANNEL_TEAM, public_config.CHAT_CHANNEL_GUILD
    , public_config.CHAT_CHANNEL_CROSS_WORLD}
local chatTypeIcon = {13505, 13504, 13501, 13506, 13500, 13502, 13503, 13521}
--获取
function ChatManager:GetChatTypeIcon(channel)
    return chatTypeIcon[channel]
end

function ChatManager:InitCallback()
    self._systemChannelCallback = function(act_id, errorCode, args)
        ChatManager:HandleSystemMessage(act_id, errorCode, args)
    end
    
    self._systemChannelHornCallback = function(act_id, errorCode, args)
        ChatManager:HandleGuildMessage(act_id, errorCode, args)
    end
end

function ChatManager:InitData()
    ChatManager.ChatContent = {}
    ChatManager.ChatLatestContent = {}
    ChatManager.SheildPlayerList = {}
    self._itemToServerData = {}
    self._preTalkTime = 0
    local timer = nil
    
    ChatManager.itemCache = {}
    ChatManager.itemCacheIndex = 1
    
    self.isAutoVoice = true
end

function ChatManager:ResetTalkTime()
    self._preTalkTime = DateTimeUtil.GetServerTime()
end

function ChatManager:GetIsCanTalk()
    if DateTimeUtil.GetServerTime() - tonumber(self._preTalkTime) > 2 then
        return true
    else
        return false
    end
end

function ChatManager:OnPlayerEnterWorld()
    self:InitData()
    EventDispatcher:AddEventListener(GameEvents.CHAT_TEXT_CLICK, ChatManager.ChatTextClick)
    EventDispatcher:AddEventListener(GameEvents.CHAT_ADD_SHIELD_PLAYER, ChatManager.AddShieldPlayer)
    
    EventDispatcher:AddEventListener(GameEvents.ON_BROADCAST_IN_SYS_CHAR, self._systemChannelCallback)
    EventDispatcher:AddEventListener(GameEvents.ON_BROADCAST_IN_GUILD_CHAR, self._systemChannelHornCallback)

end

function ChatManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_TEXT_CLICK, ChatManager.ChatTextClick)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_ADD_SHIELD_PLAYER, ChatManager.AddShieldPlayer)
    
    EventDispatcher:RemoveEventListener(GameEvents.ON_BROADCAST_IN_SYS_CHAR, self._systemChannelCallback)
    EventDispatcher:RemoveEventListener(GameEvents.ON_BROADCAST_IN_GUILD_CHAR, self._systemChannelHornCallback)

end

function ChatManager:HandleSystemMessage(act_id, errorCode, args)
    local argsTable = LanguageDataHelper.GetArgsTable()
    for k, v in pairs(args or {}) do
        argsTable["" .. k] = v
    end
    
    --LoggerHelper.Error("args"..PrintTable:TableToStr(args))
    local str = SystemInfoDataHelper.CreateContent(errorCode, argsTable)
    ChatManager.ReceiveChatContent(public_config.CHAT_CHANNEL_SYSTEM, '', str, '')
end

function ChatManager:HandleGuildMessage(act_id, errorCode, args)
    local argsTable = LanguageDataHelper.GetArgsTable()
    for k, v in pairs(args or {}) do
        argsTable["" .. k] = v
    end
    local str = SystemInfoDataHelper.CreateContent(errorCode, argsTable)
    ChatManager.ReceiveChatContent(public_config.CHAT_CHANNEL_GUILD, '', str, '')
end




--channelIndex：UI上的索引
function ChatManager.SetChannel(channelIndex)
    ChatManager.CurChannel = chatReflect[channelIndex]
    --LoggerHelper.Error("ChatManager.SetChannel"..ChatManager.CurChannel)
    EventDispatcher:TriggerEvent(GameEvents.CHAT_CONTENT_VIEW_CHANGE)
end

function ChatManager.ShowGM(str)
    if str == '@showGM' then
        -- GUIManager.ShowPanel(PanelsConfig.GMCommand)
        return true
    elseif str == "@showlog" then
        GUIManager.ShowPanel(PanelsConfig.Logs)
        return true
    elseif str == "@showfps" then
        GameWorld.showFps = not GameWorld.showFps
        return true
    elseif str == "@whosyourfather" then
        GameWorld.CSGM("@console")
        return true
    elseif str == "@revert" then
        GameManager.ServerListManager:RevertVersion()
        return true
    elseif str == "@clearallfile" then
        GameWorld.ClearClientAllFile()
        return true
    elseif string.StartWith(str, "@test") then
        local paras = string.split_str(str, " ")
        if #paras == 1 then
            GameManager.ServerListManager:ChangeTestMode()
        else
            GameManager.ServerListManager:ChangeTestModeByName(paras[2])
        end
        -- LoggerHelper.Error("paras: " .. #paras .. " " .. PrintTable:TableToStr(paras))
        return true
    end
    return false
end

function ChatManager:SetIsAutoVoice(isAuto)
    self.isAutoVoice = isAuto
end

function ChatManager:GetIsAutoVoice()
    return self.isAutoVoice
end


-- "频道id,聊天内容"
function ChatManager.SendChatContent(text, channel, liststr)
    local str = text --ChatManager.CurChannel .. ',' .. text
    if ChatManager.ShowGM(str) then
        return
    end
    -- print('..........' .. str)
    if ChatManager.CurChannel == public_config.CHAT_CHANNEL_TEAM then --聊天频道，是否在队伍中
        if GameWorld.Player().team_id == 0 then --不在队伍中
            GameManager.SystemInfoManager:ShowClientTip(2519)
            return
        end
    end
    
    if ChatManager.CurChannel == public_config.CHAT_CHANNEL_GUILD then --公会频道，是否在公会中
        if not (PlayerManager.GuildManager:IsInGuild()) then --不在公会中
            GameManager.SystemInfoManager:ShowClientTip(2518)
            return
        end
        --公会篝火答题时调用答题接口
        if GuildBanquetManager:IsOpen() then
            GuildBanquetManager:GuildBanquetAnswerReq(str)
        end
    end
    
    --LoggerHelper.Error(str .. '==========================' .. ChatManager.CurChannel)
    local sendChannel = channel
    if sendChannel == nil then
        sendChannel = ChatManager.CurChannel
    end
    --LoggerHelper.Error("sendChannel")
    ChatManager.RequestChat(sendChannel, str, liststr)
end

function ChatManager:SetSliderEnable(value)
    self._sliderEnable = value
end

function ChatManager:GetSliderEnable()
    return self._sliderEnable
end

function ChatManager:AddItemStr(itemId)
    self._itemId = itemId
end

function ChatManager:GetItemStr()
    return self._itemId
end

function ChatManager.RequestChat(channel, text, liststr)
    liststr = liststr or ''
    GameWorld.Player().server.chat_req(channel, '', text, liststr)
    ChatManager.OnCheckChatMesseage(text, channel)
end

function ChatManager.ReceiveChatContent(channel_id, paramstr, contentstr, liststr)
    -- contentstr = '\nhello<sprite anim="0, 2, 12">表情<sprite anim="0, 2, 12">'
    --LoggerHelper.Log('channel_id:' .. channel_id .. "contentstr:" .. contentstr.."liststr"..liststr)
    if contentstr == nil or contentstr == '' then
        return
    end
    local ChatData = ChatServerData()
    local str
    --系统消息不过滤
    if channel_id == public_config.CHAT_CHANNEL_SYSTEM or channel_id == public_config.CHAT_CHANNEL_SEEK_TEAM or public_config.CHAT_CHANNEL_GUILD then
        str = contentstr
    else
        str = GameWorld.FilterWords(contentstr)
    end
    str = ChatManager:CheckContentLink(str)
    str = ChatManager.HandleEmoji(str, liststr)
    
    str = ChatData:HandleItem(str, liststr)--ChatManager.HandleItem(str, liststr)
    
    if channel_id == public_config.CHAT_CHANNEL_SEEK_TEAM then
        --LoggerHelper.Error("paramstr:"..paramstr)
        local playerInfo = string.split(paramstr, ',')
        str = StringStyleUtil.GetColorStringWithTextMesh(playerInfo[3], StringStyleUtil.yellow1) .. ":" .. str
    end
    
    if channel_id == public_config.CHAT_CHANNEL_HORN then
        --CommonUIPanel:ShowHornNewsTicke(text, count, callback,channelType)
        local playerInfo = string.split(paramstr, ',')
        str = StringStyleUtil.GetColorStringWithTextMesh(playerInfo[3], StringStyleUtil.yellow1) .. ":" .. str
    end
    
    
    ChatData:SetData(channel_id, paramstr, str)
    
    if channel_id == public_config.CHAT_CHANNEL_HORN then
        --CommonUIPanel:ShowHornNewsTicke(text, count, callback,channelType)
        local strInfo = string.split(str, ':')
        local playerInfo = string.split(paramstr, ',')
        if playerInfo then
            str = StringStyleUtil.GetColorStringWithTextMesh(playerInfo[3], StringStyleUtil.green) .. ":" .. tostring(strInfo[2])
        end
        GUIManager.ShowNewsTicke(str, nil, nil, channel_id)
        channel_id = public_config.CHAT_CHANNEL_SYSTEM
    end
    
    
    
    ChatManager:ClearToSerData()
    
    
    -- 是否是屏蔽玩家
    for k, v in pairs(ChatManager.SheildPlayerList) do
        -- LoggerHelper.Log(v .. '=====================')
        if ChatData.uuid == v then
            return
        end
    end
    
    
    
    -- if channel_id == public_config.CHAT_CHANNEL_TEAM then --聊天频道,头像冒泡
    --     EventDispatcher:TriggerEvent(GameEvents.TEAM_CHAT, ChatData)
    -- end
    -- if not (GUIManager.PanelIsActive(PanelsConfig.Chat)) then
    --     -- 主界面的浮动聊天框
    --     if channel_id == ChatManager.CurChannel then
    --         timer = nil
    --         EventDispatcher:TriggerEvent(GameEvents.CHAT_FLOAT_PANEL_ADD, ChatData)
    --     end
    --     -- return
    -- end
    if ChatManager.ChatContent[channel_id] == nil then
        ChatManager.ChatContent[channel_id] = {}
    end
    local channelDataList = ChatManager.ChatContent[channel_id]
    local len = #channelDataList
    -- local remove = false
    if len > (ChatManager.MAX_CHAT_NUM - 1) then
        local removeCount = len - ChatManager.MAX_CHAT_NUM + 1
        for i = 1, removeCount do
            table.remove(channelDataList, 1)
        end
    -- remove = true
    end
    table.insert(channelDataList, ChatData)
    
    --最近的50条
    local latestContent = ChatManager.ChatLatestContent
    local len1 = #latestContent
    if len1 > (ChatManager.MAX_LATEST_CHAT_NUM - 1) then
        local removeCount = len1 - ChatManager.MAX_LATEST_CHAT_NUM + 1
        --LoggerHelper.Error("latestContent"..removeCount)
        for i = 1, removeCount do
            table.remove(latestContent, 1)
        end
    end
    table.insert(latestContent, ChatData)
    
    EventDispatcher:TriggerEvent(GameEvents.CHAT_CONTENT_ADD, channel_id)
    
    if channel_id == public_config.CHAT_CHANNEL_GUILD then
        local playerInfo = string.split(paramstr, ',')
        --LoggerHelper.Error(" OpenRankView:ShowView()"..playerInfo[2])
        if playerInfo then
            if tonumber(playerInfo[2]) == GameWorld.Player().dbid then
                else
                if GameManager.GUIManager.PanelIsActive(PanelsConfig.Chat) then
                    if ChatManager.CurChannel == public_config.CHAT_CHANNEL_GUILD then
                        EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, false)
                    else
                        EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, true)
                    end
                else
                    EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, true)
                end
            end
        else
            if GameManager.GUIManager.PanelIsActive(PanelsConfig.Chat) then
                if ChatManager.CurChannel == public_config.CHAT_CHANNEL_GUILD then
                    EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, false)
                else
                    EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, true)
                end
            else
                EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red, true)
            end
        end
    
    end






-- if GUIManager.PanelIsActive(PanelsConfig.Chat) then --聊天面板打开
--     if channel_id == ChatManager.CurChannel and channel_id ~= public_config.CHAT_CHANNEL_SYSTEM then --公告实时刷新
--         if timer == nil then
--             timer = TimerHeap:AddSecTimer(0.5, 0, 0, ChatManager.SendEventUpdateView)--当前显示的频道 1秒刷新一次数据
--         end
--     else
--         -- ChatManager.SendEventUpdateView(channel_id)
--         EventDispatcher:TriggerEvent(GameEvents.CHAT_CONTENT_ADD, channel_id)
--     end
-- end
end

function ChatManager.SendEventUpdateView()
    timer = nil
    EventDispatcher:TriggerEvent(GameEvents.CHAT_CONTENT_ADD, ChatManager.CurChannel)
end

function ChatManager.GetChatContents()
    return ChatManager.ChatContent[ChatManager.CurChannel] or {}
end

--获取最近3条聊天信息（缩略框使用）
function ChatManager.GetLatestChatContents()
    --LoggerHelper.Error("ChatManager.GetLatestChatContents"..#ChatManager.ChatLatestContent)
    return ChatManager.ChatLatestContent
end

function ChatManager.ChatFastContent(text)
    ChatManager.SendChatContent(text, public_config.CHAT_CHANNEL_TEAM)
end

-- 处理表情
function ChatManager.HandleEmoji(textStr)
    for w in string.gmatch(textStr, "#(%d+)#") do
        local index = tonumber(w)
        local emojiText = ChatManager.EmojiText(index)
        textStr = string.gsub(textStr, '#' .. index .. '#', emojiText)
    end
    return textStr
end

function ChatManager.EmojiText(index)
    local emojiData = ChatDataHelper.GetEmojiData(index)
    if emojiData == nil then
        return '#' .. index .. '#'
    end
    local animationData = ChatDataHelper.GetEmojiAnimationData(emojiData.content)
    if animationData == nil then
        return '#' .. index .. '#'
    end
    local sendText = ''
    if animationData.frameRate > 0 then
        sendText = '<size=28>' .. '<sprite anim="' .. animationData.startIndex .. ',' .. animationData.endIndex .. ',' .. animationData.frameRate .. '">' .. '</size>'
    else
        sendText = '<size=28>' .. '<sprite=' .. animationData.startIndex .. '>' .. '</size>'
    end
    return sendText
end

-- ==============================================点击聊天文本数据处理======================================================
local TimerHeap = GameWorld.TimerHeap

-- 屏蔽玩家
function ChatManager.AddShieldPlayer(uuid)
    -- LoggerHelper.Log('uuid===========' .. uuid)
    table.insert(ChatManager.SheildPlayerList, uuid)
    -- 提示 重新登录后自动取消屏蔽
    GameManager.SystemInfoManager:ShowClientTip(16010)
end

function ChatManager.ChatTextClick(dataText)
    print('----------' .. dataText)
end


ChatManager.GMIndex = 0
function ChatManager.AutoSendMsg()
    ChatManager.GMIndex = ChatManager.GMIndex + 1
    TimerHeap:AddSecTimer(0, 0.1, 0, ChatManager.GMSendMsg)
end

function ChatManager.GMSendMsg()
    ChatManager.GMIndex = ChatManager.GMIndex + 1
    
    ChatManager.SendChatContent(ChatManager.GMIndex .. '天,剖腹一刀五千几,剛比,剿共复国,剿共復國,劈开你妈两条腿,劉俊國,劉凱')--'一条一行，不会自动换行。显示不下就截断') -- '你好公会[公会,3]|[3;22;22;22]')
end

-- ===============================================好友聊天信息处理==========================================================================
-- local ChatFriendInfoStorage = GameConfig.ChatFriendInfoStorage
-- -- 好友聊天信息
-- local ChatFriendData = Class.ChatFriendData(ClassTypes.XObject)
-- --     CHAT_RECORD_KEY_TIMESTAMP     = 1,    --消息发送时间
-- --     CHAT_RECORD_KEY_FROM_UUID     = 2,    --发送人uuid
-- --     CHAT_RECORD_KEY_TEXT          = 3,    --消息内容
-- --     CHAT_RECORD_KEY_ITEM_DATA     = 4,    --消息附带装备数据
-- function ChatFriendData:__ctor__()
--     self.channel = 2
--     self.time = 1
--     self.content = ''
--     self.itemData = ''
--     self.type = 0 --类型 0 自己说话 1 好友说话
-- end
-- function ChatFriendData:SetData(channel, time, content, itemData, type)
--     self.channel = channel
--     self.time = time
--     self.content = content
--     self.itemData = itemData
--     self.type = type --类型 0 自己说话 1 好友说话
-- end
-- local ChatFriendDataCache = {}
-- local ChatFriendDataChange = {}
-- ChatManager.curFriendData = '' --当前打开好友数据
-- function ChatManager.GetFriendChatInfos(uuid)
--     local contentList = ChatFriendDataCache[uuid]
--     if contentList == nil then
--         contentList = ChatFriendInfoStorage:Load(uuid)
--     end
--     ChatFriendDataCache[uuid] = contentList
--     return contentList
-- end
-- -- 增加好友聊天信息
-- function ChatManager.AddFriendChatInfos(uuid, chatList)
--     -- LoggerHelper.Log(uuid .. '')
--     -- LoggerHelper.Log(chatList)
--     local contentList = ChatManager.GetFriendChatInfos(uuid)
--     -- 哪个好友数据有变化
--     ChatFriendDataChange[uuid] = true
--     for k, v in pairs(chatList) do
--         local newData = ChatFriendData()
--         local str = GameWorld.FilterWords(v[public_config.CHAT_RECORD_KEY_TEXT])
--         str = ChatManager:CheckContentLink(str)
--         newData:SetData(2, v[public_config.CHAT_RECORD_KEY_TIMESTAMP], str, v[public_config.CHAT_RECORD_KEY_ITEM_DATA], 1)
--         if (#contentList) > (ChatManager.MAX_CHAT_NUM - 1) then
--             table.remove(contentList, 1)
--         end
--         table.insert(contentList, newData)
--     end
--     EventDispatcher:TriggerEvent(GameEvents.CHAT_FRIEND_CONTENT_ADD, uuid)
-- end
-- -- 增加自己说话内容
-- function ChatManager.AddSelfChatInfos(uuid, chatContent, time, dataText)
--     local contentList = ChatManager.GetFriendChatInfos(uuid)
--     -- 哪个好友数据有变化
--     ChatFriendDataChange[uuid] = true
--     local str = GameWorld.FilterWords(chatContent)
--     str = ChatManager:CheckContentLink(str)
--     local newData = ChatFriendData()
--     newData:SetData(2, time, str, dataText, 0)
--     if (#contentList) > (ChatManager.MAX_CHAT_NUM - 1) then
--         table.remove(contentList, 1)
--     end
--     table.insert(contentList, newData)
--     EventDispatcher:TriggerEvent(GameEvents.CHAT_FRIEND_CONTENT_ADD, uuid)
-- end
-- function ChatManager.InitChatFriend()
--     EventDispatcher:AddEventListener(GameEvents.OnChatListResp, ChatManager.AddFriendChatInfos)
--     -- 退出程序时保存数据
--     EventDispatcher:AddEventListener(GameEvents.UNGINE_ONAPPLICATIONQUIT, ChatManager.SaveFriendChatData)
-- end
-- -- 保存好友聊天数据到本地
-- function ChatManager.SaveFriendChatData()
--     for k, v in pairs(ChatFriendDataChange) do
--         if v then
--             local contentList = ChatManager.GetFriendChatInfos(k)
--             ChatFriendInfoStorage:Save(k, contentList)
--         end
--     end
-- end
-- =============================================语音聊天接口======================================================================
local UIVoiceDriver = ClassTypes.UIVoiceDriver
local voiceDriver = nil
local IsTalking = false

-- 初始化
function ChatManager.InitVoice()
    ChatManager.InitVoiceListener()
    EventDispatcher:AddEventListener(GameEvents.CHAT_VOICE_START, ChatManager.StartTalk)
    EventDispatcher:AddEventListener(GameEvents.CHAT_VOICE_STOP, ChatManager.StopTalk)
    EventDispatcher:AddEventListener(GameEvents.PlayVoiceError, ChatManager.PlayVoiceError)
end

-- 监听 语音 SDK
function ChatManager.InitVoiceListener()
    voiceDriver = UIVoiceDriver.AddVoiceDriver(GUILayerManager.GetUILayerTransform(GUILayer.LayerUIMain).gameObject)
-- voiceDriver:SetOnConvertVoiceMessageCB(ChatManager.VoiceMessage)
end

-- function ChatManager.VoiceMessage(msg)
--     ChatManager.SendChatContent(msg,ChatManager.CurChannel)
-- end
-- 开始 语音
function ChatManager.StartTalk()
    
    LoggerHelper.Log('[CHAT_VOICE_START]')
    if IsTalking then
        return
    end
    if voiceDriver ~= nil then
        IsTalking = true
        voiceDriver:OnStartToTalk()
    end
end

-- 结束 语音
function ChatManager.StopTalk()
    IsTalking = false
    if voiceDriver ~= nil then
        voiceDriver:OnSDKStopTalk()
    end
--self:ResetTalkTime()
--self._preTalkTime = DateTimeUtil.GetServerTime()
-- LoggerHelper.Log('[CHAT_VOICE_STOP]')
end

-- 播放 语音
function ChatManager.OnStartPlayVoice(path, time)
    if voiceDriver ~= nil then
        LoggerHelper.Log('[CHAT_VOICE_play] PATH:' .. path)
        voiceDriver:OnStartPlayVoice(path, time)
    end
end

-- 播放语音 错误
function ChatManager.PlayVoiceError(tipsId)
    GameManager.SystemInfoManager:ShowClientTip(tipsId)
-- LoggerHelper.Log('ChatManager.PlayVoiceError:' .. tipsId)
end

-- 是否发送 语音聊天内容
function ChatManager.SendVoiceMessageType(type)
    if voiceDriver ~= nil then
        voiceDriver:SendVoiceMessageType(type)
    end
end

-- for TEST 测试用以后修正
-- 解析超连接
function ChatManager:CheckContentLink(content)
    -- local content = "[LINK_51457|guildk18,8196162]"
    --                  [LINK_51456|yzj016,guildk6,8196154]
    local textId, param = string.match(content, "%[LINK_(%d+)|(.+)%]")
    local paramText = param
    if textId then
        param = string.split(param, ",")
        local args = {}
        for i, v in ipairs(param) do
            args[tostring(i - 1)] = v
        end
        
        local text = tostring(textId)
        if text == '51456' then
            args['2'] = LinkTypesMap.Invitation .. ',' .. paramText
        elseif text == '51457' then
            args['1'] = LinkTypesMap.Recruit .. ',' .. paramText
        elseif text == '53335' then
            args['1'] = LinkTypesMap.Training .. ',' .. paramText
        end
        
        
        textId = tonumber(textId)
        content = LanguageDataHelper.CreateContent(textId, args)
    
    -- for TEST
    -- if textId == 51456 then
    --     local guildData = {
    --         name = param[2],
    --         dbid = tonumber(param[3]),
    --     }
    --     GuildManager.ShowPanel(PanelsConfig.GuildInvite, guildData)
    -- end
    end
    return content
end

-- ==============================================点击文本链接================================================================
ChatManager.ClickPosition = nil
function ChatManager:OnClickTextLink(linkId)
    --LoggerHelper.Error(linkId)
    local bs = string.split(linkId, ",")
    if bs[1] == LinkTypesMap.Team then --组队链接
        self:JoinTeam(bs)
    elseif bs[1] == LinkTypesMap.Item then --查看道具
        --LoggerHelper.Error("查看道具")
        self:HandleItemTip(bs)
    elseif bs[1] == LinkTypesMap.Coordinate then --寻路到目标坐标
        self:HandleCoordinate(bs)
    elseif bs[1] == LinkTypesMap.PlayerName then --查看玩家
        self:LookOverPlayer(bs)
    elseif bs[1] == LinkTypesMap.WorldBoss then --世界boss
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_WORLD_BOSS, tonumber(bs[2]))
    elseif bs[1] == LinkTypesMap.BossHome then --boss之家
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_HOME_BOSS, tonumber(bs[2]))
    elseif bs[1] == LinkTypesMap.BackWoods then --黑暗禁地
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_BARBAROUS, tonumber(bs[2]))
    elseif bs[1] == LinkTypesMap.GodMonsterIsland then --神兽岛
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_ISLAND_ANIMAL, tonumber(bs[2]))
    elseif bs[1] == LinkTypesMap.AssistBattle then --前往助战
        InstanceManager:GoToSceneAny(tonumber(bs[2]), tonumber(bs[3]) * 0.01, tonumber(bs[4]) * 0.01, tonumber(bs[5]) * 0.01)
    elseif bs[1] == LinkTypesMap.Trade then --交易行
        GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_MARKET_BUY)
    elseif bs[1] == LinkTypesMap.ItemSys then
        --LoggerHelper.Error("1111111111")
        self:HandleItemSysTip(bs)
    elseif bs[1] == LinkTypesMap.WithGirl then
        self:HandleGirlWithTip(bs)
    end
end

function ChatManager:LookOverPlayer(bs)
    PlayerManager.CheckOtherPlayerManager:RequestGetPlayerDBIdByName(bs[2])
--LoggerHelper.Error("查看玩家"..PrintTable:TableToStr(bs))
end


-- 处理 点击了 组队 链接
function ChatManager:JoinTeam(bs)
    if GameWorld.Player() == nil or GameWorld.Player().team_id > 0 then --组队中
        GameManager.SystemInfoManager:ShowClientTip(2603)
        return
    end
    
    PlayerManager.TeamManager:TeamApply(bs[2])
end


--系统查看道具
function ChatManager:HandleItemSysTip(bs)
    if bs[2] then
        --LoggerHelper.Error("ChatManager:HandleItemSysTip(bs)"..PrintTable:TableToStr(bs))
        TipsManager:ShowItemTipsById(tonumber(bs[2]))
    end
end

--护送女神
function ChatManager:HandleGirlWithTip(bs)
    if bs[2] then
        -- TipsManager:ShowItemTipsById(tonumber(bs[2]))
        EventDispatcher:TriggerEvent(GameEvents.Daily_EscortPeri)
    end
end

--查看道具
function ChatManager:HandleItemTip(bs)
    -- if bs[2] then
    --     TipsManager:ShowItemTipsById(tonumber(bs[2]))
    -- end
    local itemIndex = tonumber(bs[2])
    local cacheIndex = tonumber(bs[3])
    local serDatas = ChatManager.itemCache[cacheIndex]
    if serDatas then
        local itemSerData = serDatas[itemIndex]
        if itemSerData then
            local itemServerData = itemSerData[1]
            local bagType = itemSerData[2]
            local itemData = CommonItemData.GetOneItemData(itemServerData, bagType, 0)
            local otherPlayerArgs
            if bagType == public_config.PKG_TYPE_LOADED_EQUIP then
                otherPlayerArgs = {}
                otherPlayerArgs.vipLevel = itemSerData[3]
                otherPlayerArgs.strengthInfo = itemSerData[4]
                otherPlayerArgs.washInfo = itemSerData[5]
                otherPlayerArgs.gemInfo = itemSerData[6]
                otherPlayerArgs.suitData = itemSerData[7]
                otherPlayerArgs.allEquipIds = itemSerData[8]
            end
            TipsManager:ShowItemTips(itemData, nil, false, false, otherPlayerArgs)
        end
    end
end

--寻路到目标坐标
function ChatManager:HandleCoordinate(bs)
    local tarPos = Vector3.New(bs[3], bs[4], bs[5])
    
    --LoggerHelper.Error("bs[2]" .. bs[2])
    --LoggerHelper.Error("tarPos.x" .. tonumber(tarPos.x))
    --LoggerHelper.Error("tarPos.y" .. tonumber(tarPos.y))
    --LoggerHelper.Error("tarPos.z" .. tonumber(tarPos.z))
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, tonumber(bs[2]), tarPos, false, false)
end

function ChatManager:HandlePlayerName(bs)

end

--清楚物品数据
function ChatManager:ClearToSerData()
    self._itemToServerData = {}
end

function ChatManager:GetToSerData()
    return self._itemToServerData or {}
end


--添加物品数据
function ChatManager:AddItemToSerData(itemClickData)
    if self._itemToServerData then
        table.insert(self._itemToServerData, itemClickData)
    end
end




------------------------------------------------设置常用语-----------------------------------------
function ChatManager:RequestEditFastChat(index, content)
    local str = index .. "," .. content
    GameWorld.Player().server.chat_action_req(action_config.CHAT_EDIT_OFTEN_USE, str)
end

function ChatManager:OnFastChatEdit()
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_UPDATE_FAST_CHAT)
end



------------------------------------------------聊天监控-----------------------------------------------------------
function ChatManager.OnCheckChatMesseage(msgTex, channel_id)
    local template = 'http://msgapi.aiyinghun.com/fx?gid=%s&aid=%s&sid=%s&uid=%s&rid=%s&rn=%s&ch=%s&ip=%s&vip=%s&level=%s&msg=%s&sign=%s'
    local keySign = '987cff01ed902a1edf86312e2bebacf3'
    
    -- local gid =  '120000070' -- GameWorld.GetGameId()
    -- local aid =  'yhphs_110000263' -- GameWorld.GetAppId()
    -- local sid = '1' --tostring(LocalSetting.settingData.SelectedServerID)
    -- local uid =  'u29782000896' -- GameWorld.GetUserId()
    -- local rid = '1048622'--tostring(GameWorld.Player().dbid)
    -- local rn = tostring(GameWorld.Player().name)
    -- local ch = LanguageDataHelper.CreateContent(86184 + tonumber(channel_id))
    -- local ip =  '192.168.0.1'--GameWorld.GetIPAddress()
    -- local vip = tostring(GameWorld.Player().vip_level)
    -- local level = tostring(GameWorld.Player().level)
    -- local msg = string.gsub(msgTex, "([^%w%.%- ])", function(c) return string.format("%%%02X", string.byte(c)) end)
    -- local sign = GameWorld.StrToMd5String('userId=' .. uid .. keySign)
    local gid = GameWorld.GetGameId()
    local aid = GameWorld.GetAppId()
    local sid = tostring(LocalSetting.settingData.SelectedServerID)
    local uid = GameWorld.GetUserId()
    local rid = tostring(GameWorld.Player().dbid)
    local rn = tostring(GameWorld.Player().name)
    local ch = LanguageDataHelper.CreateContent(86184 + tonumber(channel_id))
    local ip = GameWorld.GetIPAddress()
    local vip = tostring(GameWorld.Player().vip_level)
    local level = tostring(GameWorld.Player().level)
    local msg = string.gsub(msgTex, "([^%w%.%- ])", function(c) return string.format("%%%02X", string.byte(c)) end)
    -- LoggerHelper.Error("encode msg:" .. msg)
    local sign = GameWorld.StrToMd5String('userId=' .. uid .. keySign)
    
    
    local url = string.format(template, gid, aid, sid, uid, rid, rn, ch, ip, vip, level, msg, sign)
    local onDone = function(result)ChatManager.OnHttpDone(result) end
    local onFail = function(result)ChatManager.OnHttpDone(result) end
    -- LoggerHelper.Error("chat monitor:" .. url)
    GameWorld.GetHttpUrl(url, onDone, onFail)
end

function ChatManager.OnHttpDone(result)


end




ChatManager.Init()
return ChatManager
