local FunctionOpenManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local functionOpenData = PlayerManager.PlayerDataManager.functionOpenData
local MenuConfig = GameConfig.MenuConfig

local DynamicMenuData = Class.DynamicMenuData(ClassTypes.XObject)

function DynamicMenuData:__ctor__(cfgId,clickFunc,endTime,getReady,menuIcon,menuName)
    self.cfgId = cfgId
    self.clickFunc = clickFunc
    self.endTime = endTime
    self.getReady = getReady
    self.menuIcon = menuIcon
    self.menuName = menuName
end

function FunctionOpenManager:Init()
    self._onConditionChangeCB = function()self:OnConditionChange() end

    self._onAddDynamicMenuIcon = function (cfgId,clickFunc,endTime,getReady,menuIcon,menuName)
        self:AddDynamicMenuIcon(cfgId,clickFunc,endTime,getReady,menuIcon,menuName)
    end

    self._onRemoveDynamicMenuIcon = function (cfgId)
        self:RemoveDynamicMenuIcon(cfgId)
    end

    self._continueFunctionOpenPanelCb = function ()
        self:ContinueFunctionOpenPanel()
    end
end

function FunctionOpenManager:OnPlayerEnterWorld()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onConditionChangeCB)
    EventDispatcher:AddEventListener(GameEvents.COMMITED_TASK_CALLBACK_FOR_MENU, self._onConditionChangeCB)
    EventDispatcher:AddEventListener(GameEvents.TASK_INIT_COMPLETE, self._onConditionChangeCB)
    EventDispatcher:AddEventListener(GameEvents.OnVocationChangeStatusChange, self._onConditionChangeCB)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._onConditionChangeCB)
    EventDispatcher:AddEventListener(GameEvents.On_FunctionO_Condition_Change, self._onConditionChangeCB)

    --
    EventDispatcher:AddEventListener(GameEvents.ADD_MAIN_MENU_ICON, self._onAddDynamicMenuIcon)
    EventDispatcher:AddEventListener(GameEvents.REMOVE_MAIN_MENU_ICON, self._onRemoveDynamicMenuIcon)

    EventDispatcher:AddEventListener(GameEvents.TASK_CG_END, self._continueFunctionOpenPanelCb)
    --所有动态按钮
    --cfgId = clickFunc
    self._dynamicFuncMap = {}
    -- self._dynamicFuncEndTime = {}  --结束时间
    -- self._dynamicFuncGetReady = {} --是否在准备阶段
    -- self._dynamicFuncMenuIcon = {} --动态图标
    -- self._dynamicFuncMenuName = {} --动态名字
    functionOpenData:ClearData()
end

function FunctionOpenManager:OnPlayerLeaveWorld()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onConditionChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.COMMITED_TASK_CALLBACK_FOR_MENU, self._onConditionChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.TASK_INIT_COMPLETE, self._onConditionChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.OnVocationChangeStatusChange, self._onConditionChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._onConditionChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.On_FunctionO_Condition_Change, self._onConditionChangeCB)

    EventDispatcher:RemoveEventListener(GameEvents.ADD_MAIN_MENU_ICON, self._onAddDynamicMenuIcon)
    EventDispatcher:RemoveEventListener(GameEvents.REMOVE_MAIN_MENU_ICON, self._onRemoveDynamicMenuIcon)
end

--CG后继续打开功能开启面板
function FunctionOpenManager:ContinueFunctionOpenPanel()
    functionOpenData:ContinueFunctionOpenPanel()
end

function FunctionOpenManager:OnConditionChange()
    self:RefreshPreviewPanel()
    --EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_MAIN_MENU)
    self:RefreshOpenData()
end

function FunctionOpenManager:RefreshPreviewPanel()
    local data = FunctionOpenDataHelper:GetFuncPreviewData(GameWorld.Player().level)
    if data ~= nil then
        GUIManager.ShowPanel(PanelsConfig.FunctionPreview, data)
    else
        if GUIManager.PanelIsActive(PanelsConfig.FunctionPreview) then
            GUIManager.ClosePanel(PanelsConfig.FunctionPreview)
        end
    end
end

function FunctionOpenManager:RefreshOpenData()
    functionOpenData:RefreshOpenDataAll()
end

function FunctionOpenManager:CheckShowOpenTips()
    functionOpenData:ShowOpenTips()
end

-- 当前已经开了的 次级主界面 功能 数据
function FunctionOpenManager:GetSubMainFuncDatas()
    local keys = FunctionOpenDataHelper:GetAllIdByButtonType(MenuConfig.SUB_MENU)
    local curOpenPanels = {}
    for k, v in pairs(keys) do
        local panelData = FunctionOpenDataHelper.GetPanelOpen(v)
        table.insert(curOpenPanels, panelData)
    end
    table.sort(curOpenPanels, function(a, b) return a.position[1] > b.position[1] end)
    --LoggerHelper.Log(curOpenPanels)
    return curOpenPanels
end

function FunctionOpenManager:CheckDynamicMenuOpen(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return true
    else
        return false
    end
end

function FunctionOpenManager:GetDynamicMenu()
    return self._dynamicFuncMap
end

function FunctionOpenManager:GetDynamicMenuClickFun(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return self._dynamicFuncMap[cfgId].clickFunc
    end
    return nil
end

function FunctionOpenManager:GetDynamicMenuEndTime(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return self._dynamicFuncMap[cfgId].endTime
    end
    return nil
end

function FunctionOpenManager:GetDynamicMenuGetReady(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return self._dynamicFuncMap[cfgId].getReady
    end
    return nil
end

function FunctionOpenManager:GetDynamicMenuIcon(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return self._dynamicFuncMap[cfgId].menuIcon
    end
    return nil
end

function FunctionOpenManager:GetDynamicMenuName(cfgId)
    if self._dynamicFuncMap[cfgId] then
        return self._dynamicFuncMap[cfgId].menuName
    end
    return nil
end

function FunctionOpenManager:AddDynamicMenuIcon(cfgId,clickFunc,endTime,getReady,menuIcon,menuName)
    local dmd = DynamicMenuData(cfgId,clickFunc,endTime,getReady,menuIcon,menuName)
    self._dynamicFuncMap[cfgId] = dmd
    EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_MAIN_MENU)
    if dmd.menuIcon then
        EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_MAIN_MENU_DYNAMIC_ICON, cfgId)
    end
end

function FunctionOpenManager:RemoveDynamicMenuIcon(cfgId)
    if self._dynamicFuncMap[cfgId] then
        self._dynamicFuncMap[cfgId] = nil
        EventDispatcher:TriggerEvent(GameEvents.ON_REFRESH_MAIN_MENU)
    end
end

------------------------------一些公用方法----------------------------------------
--提示多少级开启
function FunctionOpenManager:ShowOpenText(funcCfg)
    local openLevel
    if funcCfg.condition and funcCfg.condition[2] then
        openLevel = funcCfg.condition[2][1]
    elseif funcCfg.preview_level then
        openLevel = funcCfg.preview_level[1]
    end
    if openLevel then
        local args = LanguageDataHelper.GetArgsTable()
        args["0"] = LanguageDataHelper.CreateContent(funcCfg.name) 
        args["1"] = openLevel
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(606,args))
    end
end

FunctionOpenManager:Init() 
return FunctionOpenManager