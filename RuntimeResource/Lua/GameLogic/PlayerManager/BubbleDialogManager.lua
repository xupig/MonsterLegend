local BubbleDialogManager = {}
local Queue = ClassTypes.Queue
local action_config = require("ServerConfig/action_config")
local BubbleDialogDataHelper = GameDataHelper.BubbleDialogDataHelper
local ChineseVoiceDataHelper = GameDataHelper.ChineseVoiceDataHelper
local GameSceneManager = GameManager.GameSceneManager
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local BagData = PlayerDataManager.bagData
local SpecialDialogConfig = GameConfig.SpecialDialogConfig
local DateTimeUtil = GameUtil.DateTimeUtil
local EntityType = GameConfig.EnumType.EntityType

function BubbleDialogManager:Init()
    if GameWorld.SpecialExamine then
        return
    end
    self._eventActivity = {}
    -- self._timeStamp = {} -- 暂不实现，等真正有需要的时候再做
    self._acceptTask = {}
    self._completeTask = {}
    self._getItem = {}
    self._areaTrigger = {}
    self._HpValue = {}
    self._dialogFinish = {}
    self._distanceTrigger = {}
    self._enterBattle = {}
    self._leaveBattle = {}
    self._serverSettle = {}
    self._playerDie = {}
    -- self._showDialogQueue = Queue(100)
    self._curSceneAreaTrigger = {}
    self._curSceneHpValue = {}
    self._hasPlayHpValue = {}-- 记录某只怪物一次生命周期内已经执行过的SpecialDialog
    
    self._distanceTriggerEntites = {}
    self._timerShowDialogDurationId = {}
    self._entityInDistanceTimer = {}
    self:InitCallbackFunc()
    local sdds = BubbleDialogDataHelper:GetAllId()
    for i, v in ipairs(sdds) do
        local target = BubbleDialogDataHelper:GetTriggerAnd(v)
        if target == nil then
            target = BubbleDialogDataHelper:GetTriggerOr(v)
        end
        self:SetTrigger(target, v)
    end
    --LoggerHelper.Log("Ash: Init: " .. PrintTable:TableToStr(self._acceptTask))
end

function BubbleDialogManager:InitCallbackFunc()
    self._onEnterEventActivity = function(eventActivityId)self:OnEnterEventActivity(eventActivityId) end
    self._onItemChangeAdd = function(bagType, pos)self:OnItemChangeAdd(bagType, pos) end
    self._onItemChangeIncrease = function(bagId, bagType, pos, lastCount)self:OnItemChangeIncrease(bagId, bagType, pos, lastCount) end
    self._onAcceptTask = function(taskId)self:AcceptTask(taskId) end
    self._onCommitTask = function(taskId)self:CommitTask(taskId) end
    self._onActionEvent = function(regionId)self:ActionEvent(regionId) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onPlayerDie = function(mapId)self:OnPlayerDie() end
    self._onPropertyIsFightChange = function(isFight)self:OnPropertyIsFightChange(isFight) end
    self._onServerSettle = function(mapId)self:OnServerSettle() end
    self._onMonsterEnterWorld = function(monsterId, entityId)self:OnMonsterEnterWorld(monsterId, entityId) end
    self._onMonsterLeaveWorld = function(monsterId, entityId)self:OnMonsterLeaveWorld(monsterId, entityId) end
    self._onNpcEnterWorld = function(npcId, entityId)self:OnNpcEnterWorld(npcId, entityId) end
    self._onNpcLeaveWorld = function(npcId, entityId)self:OnNpcLeaveWorld(npcId, entityId) end
    self._onMonsterCurHpChanged = function(entityId)self:OnMonsterCurHpChanged(entityId) end
end

function BubbleDialogManager:OnPlayerEnterWorld()
    if GameWorld.SpecialExamine then
        return
    end
    -- self._timerId = TimerHeap:AddSecTimer(1, 60, 0, function()self:CheckTimeUpdate() end)
    EventDispatcher:AddEventListener(GameEvents.OnEnterEventActivity, self._onEnterEventActivity)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_ADD, self._onItemChangeAdd)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_INCREASE, self._onItemChangeIncrease)
    EventDispatcher:AddEventListener(GameEvents.ACCEPT_TASK_CALLBACK, self._onAcceptTask)
    EventDispatcher:AddEventListener(GameEvents.COMMIT_TASK_CALLBACK, self._onCommitTask)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.PlayerDie, self._onPlayerDie)
    EventDispatcher:AddEventListener(GameEvents.PROPERTY_IS_FIGHT_CHANGE, self._onPropertyIsFightChange)
    EventDispatcher:AddEventListener(GameEvents.OnServerSettle, self._onServerSettle)
    self._tickTimer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnTick() end)
end

function BubbleDialogManager:OnPlayerLeaveWorld()
    if GameWorld.SpecialExamine then
        return
    end
    -- TimerHeap:DelTimer(self._timerId)
    EventDispatcher:RemoveEventListener(GameEvents.OnEnterEventActivity, self._onEnterEventActivity)
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_ADD, self._onItemChangeAdd)
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_INCREASE, self._onItemChangeIncrease)
    EventDispatcher:RemoveEventListener(GameEvents.ACCEPT_TASK_CALLBACK, self._onAcceptTask)
    EventDispatcher:RemoveEventListener(GameEvents.COMMIT_TASK_CALLBACK, self._onCommitTask)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.PlayerDie, self._onPlayerDie)
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_IS_FIGHT_CHANGE, self._onPropertyIsFightChange)
    EventDispatcher:RemoveEventListener(GameEvents.OnServerSettle, self._onServerSettle)
    TimerHeap:DelTimer(self._tickTimer)
end

function BubbleDialogManager:SetTrigger(target, bubbleDialogId)
    if target == nil then
        return
    end
    --LoggerHelper.Log("Ash: SetTrigger: " .. PrintTable:TableToStr(target))
    for k, v in pairs(target) do
        if SpecialDialogConfig.EVENT_ACTIVITY == k then
            self._eventActivity[bubbleDialogId] = v[1]
        -- elseif SpecialDialogConfig.TIME_STAMP == k then
        --     self._timeStamp[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.ACCEPT_TASK == k then
            self._acceptTask[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.COMPLETE_TASK == k then
            self._completeTask[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.GET_ITEM == k then
            self._getItem[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.AREA_TRIGGER == k then
            self._areaTrigger[bubbleDialogId] = v
        elseif SpecialDialogConfig.HP_VALUE == k then
            self._HpValue[bubbleDialogId] = v
        elseif SpecialDialogConfig.DIALOG_FINISH == k then
            self._dialogFinish[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.DISTANCE_TRIGGER == k then
            self._distanceTrigger[bubbleDialogId] = v
        elseif SpecialDialogConfig.ENTER_BATTLE == k then
            self._enterBattle[bubbleDialogId] = v
        elseif SpecialDialogConfig.LEAVE_BATTLE == k then
            self._leaveBattle[bubbleDialogId] = v
        elseif SpecialDialogConfig.SERVER_SETTLE == k then
            self._serverSettle[bubbleDialogId] = v[1]
        elseif SpecialDialogConfig.PLAYER_DIE == k then
            self._playerDie[bubbleDialogId] = v[1]
        end
    end
end

function BubbleDialogManager:CheckTrigger(sddId, forceShowDialog)
    local target = BubbleDialogDataHelper:GetTriggerAnd(sddId)
    if target ~= nil then -- 判断有并条件的情况
        for k, v in pairs(target) do
            if SpecialDialogConfig.OWN_ITEM == k then
                if BagData:GetPkg(public_config.PKG_TYPE_ITEM):HasItemByItemId(v[1]) == false then
                    return false -- 组合条件里只要有一条不满足，都直接跳过
                end
            end
        end
    end
    -- 能来到这里，要么是或条件，要么并条件判断都通过了
    self:DoTrigger(sddId, forceShowDialog)
    return true
end

function BubbleDialogManager:DoTrigger(sddId, forceShowDialog)
    -- LoggerHelper.Log("Ash: DoTrigger: " .. sddId)
    self:ShowDialog(sddId)
-- if forceShowDialog == true then
--     self:ShowDialog(sddId)
--     return
-- end
-- -- self._showDialogQueue:Enqueue(sddId)
-- self:ShowNextDialog()
end

-- function BubbleDialogManager:ShowNextDialog()
--     if self._isShowingDialog == true then
--         return
--     end
--     if self._showDialogQueue:IsEmpty() then
--         return
--     end
--     self._isShowingDialog = true
--     self:ShowDialog(self._showDialogQueue:Dequeue())
-- end
function BubbleDialogManager:ShowDialog(sddId)
    local delay = BubbleDialogDataHelper:GetDelay(sddId)
    self._timerShowDialogDelayId = TimerHeap:AddSecTimer(delay, 0, 0, function()self:DoShowDialog(sddId) end)
end

function BubbleDialogManager:DoShowDialog(sddId)
    local targetType = BubbleDialogDataHelper:GetType(sddId)
    local eties = {}
    local entID = BubbleDialogDataHelper:GetTargetId(sddId)
    if targetType == SpecialDialogConfig.SELF then -- 自己
        table.insert(eties, GameWorld.Player())
    elseif targetType == SpecialDialogConfig.NPC then -- npc
        eties = GameWorld.GetEntityByNpcId(entID)
    elseif targetType == SpecialDialogConfig.MONSTER then -- 怪物
        eties = GameWorld.GetEntityByMonsterId(entID)
    end
    
    if #eties > 0 then
        local dialogs = BubbleDialogDataHelper:GetDialog(sddId)
        local index = math.random(1, #dialogs)
        local voice = ChineseVoiceDataHelper:GetVoice(dialogs[index])
        local etyId = eties[1].id
        -- LoggerHelper.Log("Ash: DoShowDialog: " .. sddId .. " " .. etyId .. " " .. PrintTable:TableToStr(self._timerShowDialogDurationId))
        if self._timerShowDialogDurationId[etyId] ~= nil then
            TimerHeap:DelTimer(self._timerShowDialogDurationId[etyId])
            EventDispatcher:TriggerEvent(GameEvents.REMOVE_ENTITY_BUBBLE_DIALOG, etyId)
        end
        EventDispatcher:TriggerEvent(GameEvents.ADD_ENTITY_BUBBLE_DIALOG, etyId, dialogs[index])
        if voice ~= nil and voice ~= 0 then
            GameWorld.PlaySound(voice)
        end
        local duration = BubbleDialogDataHelper:GetDuration(sddId)
        self._timerShowDialogDurationId[etyId] = TimerHeap:AddSecTimer(duration, 0, 0,
            function()
                EventDispatcher:TriggerEvent(GameEvents.REMOVE_ENTITY_BUBBLE_DIALOG, etyId)
                self._isShowingDialog = false
                self:OnDialogFinish(sddId)
                self._timerShowDialogDurationId[etyId] = nil
            end)
    end
end

function BubbleDialogManager:OnEnterEventActivity(eventActivityId)
    for sddId, v in pairs(self._eventActivity) do
        if v == eventActivityId then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:OnItemChangeAdd(bagType, pos)
    if bagType == public_config.PKG_TYPE_ITEM then
        local item = BagData:GetItem(bagType, pos)
        if item ~= nil then
            local itemId = item:GetItemID()
            for sddId, v in pairs(self._getItem) do
                if v == itemId then
                    self:CheckTrigger(sddId)
                end
            end
        else
            LoggerHelper.Error("BubbleDialogManager:OnItemChangeAdd null item Error: " .. tostring(bagType) .. " " .. tostring(pos))
        end
    end
end

function BubbleDialogManager:OnItemChangeIncrease(bagId, bagType, pos, lastCount)
    if bagType == public_config.PKG_TYPE_ITEM then
        local item = BagData:GetItem(bagType, pos)
        local itemId = item:GetItemID()
        for sddId, v in pairs(self._getItem) do
            if v == itemId then
                self:CheckTrigger(sddId)
            end
        end
    end
end

function BubbleDialogManager:AcceptTask(taskId)
    -- LoggerHelper.Log("Ash: AcceptTask: " .. taskId .. " " .. PrintTable:TableToStr(self._acceptTask))
    for sddId, v in pairs(self._acceptTask) do
        if v == taskId then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:CommitTask(taskId)
    for sddId, v in pairs(self._completeTask) do
        if v == taskId then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:ActionEvent(regionId)
    -- LoggerHelper.Log("Ash: ActionEvent: " .. regionId.. " " .. PrintTable:TableToStr(self._curSceneAreaTrigger))
    for sddId, v in pairs(self._curSceneAreaTrigger) do
        if v[2] == regionId then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:OnEnterMap(mapId)
    local hasAddAreaTriggerListener = false
    local hasAddHpValueListener = false
    for sddId, v in pairs(self._areaTrigger) do
        if v[1] == mapId then
            if hasAddAreaTriggerListener == false then
                EventDispatcher:AddEventListener(GameEvents.ActionEvent, self._onActionEvent)
                hasAddAreaTriggerListener = true
            end
            self._curSceneAreaTrigger[sddId] = v
        end
    end
    EventDispatcher:AddEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
    EventDispatcher:AddEventListener(GameEvents.OnNpcEnterWorld, self._onNpcEnterWorld)
    EventDispatcher:AddEventListener(GameEvents.OnNpcLeaveWorld, self._onNpcLeaveWorld)
    for sddId, v in pairs(self._HpValue) do
        if v[1] == mapId then
            self._curSceneHpValue[sddId] = v
        end
    end
    local npcs = GameSceneManager:GetNPCs()
    for i, v in pairs(npcs) do
        --LoggerHelper.Log("Ash: BubbleDialogManager npcs: " .. i)
        self:OnNpcEnterWorld(v.npc_id, v.id)
    end
    local monster = GameWorld.GetEntitiesByType(EntityType.Monster)
    for i, v in pairs(monster) do
        --LoggerHelper.Log("Ash: BubbleDialogManager Monster: " .. i)
        self:OnMonsterEnterWorld(v.monster_id, v.id)
    end
end

function BubbleDialogManager:OnLeaveMap(mapId)
    EventDispatcher:RemoveEventListener(GameEvents.ActionEvent, self._onActionEvent)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterEnterWorld, self._onMonsterEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnMonsterLeaveWorld, self._onMonsterLeaveWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnNpcEnterWorld, self._onNpcEnterWorld)
    EventDispatcher:RemoveEventListener(GameEvents.OnNpcLeaveWorld, self._onNpcLeaveWorld)
    self._curSceneAreaTrigger = {}
    self._curSceneHpValue = {}
    self._hasPlayHpValue = {}
end

function BubbleDialogManager:OnPlayerDie()
    for sddId, v in pairs(self._playerDie) do
        if GameSceneManager:GetCurrMapID() == v then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:OnPropertyIsFightChange(isFight)
    if isFight then
        for sddId, v in pairs(self._enterBattle) do
            self:CheckTrigger(sddId)
        end
    else
        for sddId, v in pairs(self._leaveBattle) do
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:OnServerSettle()
    for sddId, v in pairs(self._serverSettle) do
        if GameSceneManager:GetCurrMapID() == v then
            self:CheckTrigger(sddId)
        end
    end
end

function BubbleDialogManager:OnMonsterEnterWorld(monsterId, entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        if v[2] == monsterId then
            local monster = GameWorld.GetEntity(entityId)
            if monster ~= nil then
                self._hasPlayHpValue[entityId] = {}
                monster:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onMonsterCurHpChanged)
            end
        end
    end
    for sddId, v in pairs(self._distanceTrigger) do
        if BubbleDialogDataHelper:GetType(sddId) == SpecialDialogConfig.MONSTER
            and BubbleDialogDataHelper:GetTargetId(sddId) == monsterId
        then
            self._distanceTriggerEntites[sddId] = {}
            table.insert(self._distanceTriggerEntites[sddId], entityId)
            table.insert(self._distanceTriggerEntites[sddId], v[1])
            table.insert(self._distanceTriggerEntites[sddId], v[2])
        end
    end
end

function BubbleDialogManager:OnMonsterLeaveWorld(monsterId, entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        if v[2] == monsterId then
            local monster = GameWorld.GetEntity(entityId)
            if monster ~= nil then
                self._hasPlayHpValue[entityId] = nil
                monster:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onMonsterCurHpChanged)
            end
        end
    end
    for sddId, v in pairs(self._distanceTrigger) do
        if BubbleDialogDataHelper:GetType(sddId) == SpecialDialogConfig.MONSTER
            and BubbleDialogDataHelper:GetTargetId(sddId) == monsterId
        then
            self._distanceTriggerEntites[sddId] = nil
            self._entityInDistanceTimer[sddId] = nil
        end
    end
end

function BubbleDialogManager:OnNpcEnterWorld(npcId, entityId)
    --LoggerHelper.Log("Ash: OnNpcEnterWorld1: " .. npcId .. " " .. entityId)
    for sddId, v in pairs(self._distanceTrigger) do
        if BubbleDialogDataHelper:GetType(sddId) == SpecialDialogConfig.NPC
            and BubbleDialogDataHelper:GetTargetId(sddId) == npcId
        then
            --LoggerHelper.Log("Ash: OnNpcEnterWorld2: " .. sddId .. " " .. entityId)
            self._distanceTriggerEntites[sddId] = {}
            table.insert(self._distanceTriggerEntites[sddId], entityId)
            table.insert(self._distanceTriggerEntites[sddId], v[1])
            table.insert(self._distanceTriggerEntites[sddId], v[2])
        end
    end
end

function BubbleDialogManager:OnNpcLeaveWorld(npcId, entityId)
    for sddId, v in pairs(self._distanceTrigger) do
        if BubbleDialogDataHelper:GetType(sddId) == SpecialDialogConfig.NPC
            and BubbleDialogDataHelper:GetTargetId(sddId) == npcId
        then
            self._distanceTriggerEntites[sddId] = nil
            self._entityInDistanceTimer[sddId] = nil
        end
    end
end

function BubbleDialogManager:OnMonsterCurHpChanged(entityId)
    for sddId, v in pairs(self._curSceneHpValue) do
        local monster = GameWorld.GetEntity(entityId)
        if monster ~= nil then
            if self._hasPlayHpValue[entityId][sddId] == nil
                and (monster.cur_hp or 0) / (monster.max_hp or 1) <= v[3]
                and monster.monster_id == v[2] then
                if self:CheckTrigger(sddId) == true then
                    self._hasPlayHpValue[entityId][sddId] = sddId
                end
            end
        end
    end
end

function BubbleDialogManager:OnTick()
    self._playerPos = nil
    for sddId, v in pairs(self._distanceTriggerEntites) do
        local ety = GameWorld.GetEntity(v[1])
        if ety ~= nil then
            if self._playerPos == nil then
                self._playerPos = GameWorld.Player():GetPosition()
            end
            -- LoggerHelper.Log("Ash: OnTick: " .. v[1] .. " " .. v[2]
            --     .. " " .. Vector3.Distance(ety:GetPosition(), self._playerPos)
            --     .. " " .. PrintTable:TableToStr(self._playerPos)
            --     .. " " .. PrintTable:TableToStr(ety:GetPosition())
            -- )
            if Vector3.Distance(ety:GetPosition(), self._playerPos) < v[2] then -- v[2] 距离
                if self._entityInDistanceTimer[sddId] == nil then
                    self._entityInDistanceTimer[sddId] = 0
                    self:CheckTrigger(sddId)
                else
                    if self._entityInDistanceTimer[sddId] >= v[3] then -- v[3] 重复周期时间
                        self._entityInDistanceTimer[sddId] = 0
                        self:CheckTrigger(sddId)
                    end
                end
            end
            if self._entityInDistanceTimer[sddId] ~= nil then
                self._entityInDistanceTimer[sddId] = self._entityInDistanceTimer[sddId] + 1
            end
        end
    end
end

function BubbleDialogManager:OnDialogFinish(dialogId)
    for sddId, v in pairs(self._dialogFinish) do
        if v == dialogId then
            return self:CheckTrigger(sddId, true)
        end
    end
    return false
end

BubbleDialogManager:Init()
return BubbleDialogManager
