--飘字调用Manager
local FloatTextManager = {}
local PlayerDataManager = PlayerManager.PlayerDataManager
local BagData = PlayerDataManager.bagData
local action_config = require("ServerConfig/action_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local EntityConfig = GameConfig.EntityConfig
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local public_config = GameWorld.public_config
local StringStyleUtil = GameUtil.StringStyleUtil
local RoleDataHelper = GameDataHelper.RoleDataHelper
local FloatTextType = EnumType.FloatTextType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function FloatTextManager:Init()
end

function FloatTextManager:OnPlayerEnterWorld()
    self._onItemChangeIncrease = function(pkgType, pos, lastCount) self:OnGetItem(pkgType, pos, lastCount) end
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_INCREASE_FLOAT, self._onItemChangeIncrease)
    self._onFightForceChange = function(oldValue) self:OnFightForceChange(oldValue) end
    EventDispatcher:AddEventListener(GameEvents.PROPERTY_FIGHT_FORCE_CHANGE, self._onFightForceChange)
end

function FloatTextManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ITEM_CHANGE_INCREASE_FLOAT, self._onItemChangeIncrease)
    EventDispatcher:RemoveEventListener(GameEvents.PROPERTY_FIGHT_FORCE_CHANGE, self._onFightForceChange)
end

function FloatTextManager:HandleData(action_id,error_id,args)

end

--货币变化提示
function FloatTextManager:OnMoneyChange(moenyProp,moneyId,oldValue)
    if oldValue == nil then
        return
    end
    local changeValue = moenyProp - oldValue
    if changeValue > 0 then
        local quality = ItemDataHelper.GetQuality(moneyId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local text = StringStyleUtil.GetColorStringWithColorId(ItemDataHelper.GetItemName(moneyId),colorId)
        local showText = LanguageDataHelper.CreateContentWithArgs(53205, {["0"]=text, ["1"]=changeValue})
        GUIManager.ShowText(2, showText)
    end
end

function FloatTextManager:OnGetItem(pkgType, pos, lastCount)
    if pkgType == public_config.PKG_TYPE_ITEM or pkgType == public_config.PKG_TYPE_CARD_PIECE or pkgType == public_config.PKG_TYPE_GOD_MOSNTER then
        local item = BagData:GetItem(pkgType, pos)
        local itemId = item:GetItemID()
        local itemCount = item:GetCount() - lastCount
        local quality = ItemDataHelper.GetQuality(itemId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local text = StringStyleUtil.GetColorStringWithColorId(ItemDataHelper.GetItemName(itemId),colorId)
        local showText = LanguageDataHelper.CreateContentWithArgs(53205, {["0"]=text, ["1"]=itemCount})
        GUIManager.ShowText(2, showText)
    end
end

function FloatTextManager:OnFightForceChange(oldValue)
    if oldValue == nil then
        return
    end
    local fight_force = GameWorld.Player().fight_force
    local changeValue = fight_force - oldValue
    if changeValue > 0 then
        GUIManager.ShowText(FloatTextType.PowerAdd, changeValue, 1)
        GameWorld.PlaySound(9)
    end
end
-- 飘字
function FloatTextManager:OnChange(changeValue,itemId)
    if changeValue > 0 then
        local quality = ItemDataHelper.GetQuality(itemId)
        local colorId = ItemConfig.QualityTextMap[quality]
        local text = StringStyleUtil.GetColorStringWithColorId(ItemDataHelper.GetItemName(itemId),colorId)
        local showText = LanguageDataHelper.CreateContentWithArgs(53205, {["0"]=text, ["1"]=changeValue})
        GUIManager.ShowText(2, showText)
    end
end
FloatTextManager:Init() 
return FloatTextManager