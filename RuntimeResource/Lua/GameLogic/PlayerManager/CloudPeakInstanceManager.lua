local CloudPeakInstanceManager = {}

local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local public_config = GameWorld.public_config
local action_config = require("ServerConfig/action_config")
local InstanceManager = PlayerManager.InstanceManager
local InstanceConfig = GameConfig.InstanceConfig
local GUIManager = GameManager.GUIManager
local TeamManager = PlayerManager.TeamManager

function CloudPeakInstanceManager:Init()
    self:InitCallbackFunc()
end

function CloudPeakInstanceManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function CloudPeakInstanceManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function CloudPeakInstanceManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function CloudPeakInstanceManager:OnLeaveMap(mapId)    
    self._leaveMapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_CLOUD_PEAK then
    end

end

function CloudPeakInstanceManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_CLOUD_PEAK then
        -- InstanceManager:EnterInstance()
        GameWorld.Player():StartAutoFight()
    end
    
    if self._leaveMapId ~= mapId and MapDataHelper.GetSceneType(self._leaveMapId) == SceneConfig.SCENE_TYPE_CLOUD_PEAK then
        GUIManager.ClosePanel(PanelsConfig.CloudPeakInfo)
    end
end

function CloudPeakInstanceManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("CloudPeakInstanceManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.CROSS_CLOUD_PEAK_ENTER_REQ then--8451, --申请进入青云之巅
        -- LoggerHelper.Error("申请进入青云之巅sam 8451 == " .. PrintTable:TableToStr(args))
    elseif action_id == action_config.CROSS_CLOUD_PEAK_NOTIFY_SPACE_INFO then--8452, --通知场景里信息
        -- {
        --     [1] = start_time,     --开始时间
        --     [2] = duration_time , --持续时间
        --     [3] = layer,          --当前层数
        --     [4] = need_kill_num,  --需要击杀
        --     [5] = kill_num，      --已经击杀   
        -- }
        -- LoggerHelper.Error("通知场景里信息sam 8452 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.type = InstanceConfig.CLOUD_PEAK_TYPE_INFO_UPDATE
        data.data = args
        self._datas = data
        GUIManager.ShowPanel(PanelsConfig.CloudPeakInfo, data)
    elseif action_id == action_config.CROSS_CLOUD_PEAK_NOTIFY_PASS_ALL then--8453, --已经通关全部了 即将被踢出去

        -- LoggerHelper.Error("已经通关全部了 即将被踢出去sam 8453 == " .. PrintTable:TableToStr(args))
        local data = {}
        data.type = InstanceConfig.CLOUD_PEAK_TYPE_PASS
        data.data = args
        GUIManager.ShowPanel(PanelsConfig.CloudPeakInfo, data)
    else
        LoggerHelper.Error("CloudPeakInstanceManager HandleData action id not handle:" .. action_id)
    end
end

function CloudPeakInstanceManager:GetDatas()
    return self._datas
end

--8451, --申请进入青云之巅
function CloudPeakInstanceManager:EnterInstance()
    -- LoggerHelper.Error("CloudPeakInstanceManager:EnterInstance()")
    local cb = function() self:GoToInstance() end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function CloudPeakInstanceManager:GoToInstance()
    GameWorld.Player().server.cloud_peak_action_req(action_config.CROSS_CLOUD_PEAK_ENTER_REQ, "")
end

CloudPeakInstanceManager:Init()
return CloudPeakInstanceManager