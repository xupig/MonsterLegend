local VIPManager = {}
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local TaskManager = PlayerManager.TaskManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local RedPointManager = GameManager.RedPointManager
local VIPDataHelper = GameDataHelper.VIPDataHelper

function VIPManager:Init()
	self._onTaskCommitCB = function ()
		self:OnTaskCommit()
	end
	self._onTaskInitCB = function ()
		self:OnTaskInit()
	end

	self._refreshVipChargeState = function () self:RefreshChargeRedPoint() end
	self._refreshVipAwardState = function () self:RefreshAwardRedPoint() end
	self._levelTimeLimit = function()
		self:CheckExpire(true)
		--self:LevelInvestmentBtnTimeLimit()
	end
	self._expericeTaskCfg = GlobalParamsHelper.GetParamValue(563)
	self._vipPopTypeExpire = 0

	self._maxLevel = VIPDataHelper:GetMaxVIPLevel()
	self._awardRedPointState = {}
end

function VIPManager:OnPlayerEnterWorld()
	
	self:CheckExpire()

	EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._levelTimeLimit)

	EventDispatcher:AddEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._levelTimeLimit)
	--检查VIP体验
	EventDispatcher:AddEventListener(GameEvents.TASK_INIT_COMPLETE, self._onTaskInitCB)

	--检查VIP过期
	--self._timerId = TimerHeap:AddTimer(0,2000,0,function () self:CheckExpire(true) end)
	--self._vipRedpointNode
	
	EventDispatcher:AddEventListener(GameEvents.Refresh_Vip_Charge_State, self._refreshVipChargeState)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._refreshVipAwardState)

	self._showChargeRedPoint = false
	self:RefreshChargeRedPoint()

	--EventDispatcher:AddEventListener(GameEvents.Refresh_Vip_Award_State, self._refreshVipChargeState)
	
	self._showAwardRedPoint = false
	self:RefreshAwardRedPoint()
end

function VIPManager:OnPlayerLeaveWorld()
	EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._levelTimeLimit)
	EventDispatcher:RemoveEventListener(GameEvents.MAIN_MENU_ICON_UPDATE, self._levelTimeLimit)
	EventDispatcher:RemoveEventListener(GameEvents.TASK_INIT_COMPLETE, self._onTaskInitCB)
	EventDispatcher:RemoveEventListener(GameEvents.Refresh_Vip_Charge_State, self._refreshVipChargeState)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._refreshVipAwardState)
	TimerHeap:DelTimer(self._timerId)
end

function VIPManager:RegisterNodeUpdateFunc(func)
	--RedPointManager:RegisterNodeUpdateFunc(self._vipRedpointNode, func)
end

function VIPManager:OnLevelRewardUpdate()
	self:RefreshAwardRedPoint()
	EventDispatcher:TriggerEvent(GameEvents.VIP_LEVEL_REWARD_UPDATE)
	
end

-- function VIPManager:OnWeekRewardUpdate()
-- 	self:RefreshAwardRedPoint()
-- 	EventDispatcher:TriggerEvent(GameEvents.VIP_WEEK_REWARD_UPDATE)
-- end

function VIPManager:OnDailyRewardUpdate()
	self:RefreshAwardRedPoint()
	EventDispatcher:TriggerEvent(GameEvents.VIP_DAILY_REWARD_UPDATE)
end

function VIPManager:OnTaskInit()
	if TaskManager:GetTaskHasCommited(self._expericeTaskCfg[1]) then
		return
	end
	EventDispatcher:AddEventListener(GameEvents.COMMITED_TASK_CALLBACK, self._onTaskCommitCB)
end

--true,到期页面
--false,体验页面
function VIPManager:GetVipPopTypeExpire()
	return self._vipPopTypeExpire
end

function VIPManager:SetVipPopTypeExpire(value)
	self._vipPopTypeExpire = value
end

function VIPManager:OnTaskCommit()
	if TaskManager:GetTaskHasCommited(self._expericeTaskCfg[1]) and self._expericeShow == nil then
		self._expericeShow = true
		self._vipPopTypeExpire = false
		GUIManager.ShowPanel(PanelsConfig.VIPPop,{["showType"] = 1,["cardId"] = self._expericeTaskCfg[2]})
	end
end

--checkReal 检查是否是真实VIP
function VIPManager:CheckExpire(checkReal)
	self._vipPopTypeExpire = false
	local vipReal = GameWorld.Player().vip_real or 0
	if checkReal and vipReal == 0 then
		return
	end
	local expireTime = GameWorld.Player().vip_endtime or 0
	if expireTime > 0 then
		local dtime = expireTime - DateTimeUtil.GetServerTime()
		if dtime < 0 then
			self:HandleExpire()
		else
			--在一天内到期执行计时，一天以上用On_0_Oclock再次调用次方法
			if dtime < DateTimeUtil.OneDayTime then
				self._timerId = TimerHeap:AddSecTimer(dtime, 0, dtime, function ()
					self:HandleExpire()
					TimerHeap:DelTimer(self._timerId)
				end)
			end
		end
	end
end

function VIPManager:HandleExpire()
	self._vipPopTypeExpire = true
	local vipReal = GameWorld.Player().vip_real or 0
	local vipLevel =  GameWorld.Player().vip_level or 0
	--已经是VIP不在弹出过期提示
	if vipReal > 0 and vipLevel > 0 then
		return
	end
	GUIManager.ShowPanel(PanelsConfig.VIPPop,{["showType"] = 2})
end

function VIPManager:HandleResp(act_id, code, args)
	if code > 0 then
		return
	end
	if act_id == action_config.VIP_LEVEL_REWARD_REQ then
		self:OnLevelRewardUpdate()
	-- elseif act_id == action_config.VIP_WEEK_REWARD_REQ then
	-- 	self:OnWeekRewardUpdate()
	elseif act_id == action_config.VIP_WEEK_REWARD_REQ then
		self:OnWeekRewardUpdate()
	end
end

-----------------------------------红点------------------------------
function VIPManager:RefreshChargeRedPoint(type)
	local state = PlayerManager.WeekCardManager:GetWeekCardState()
	if state == 1 then
		self:SetChargeRedPoint(true)
	else
		self:SetChargeRedPoint(false)
	end
end

function VIPManager:SetChargeRedPoint(state)
    if self._showChargeRedPoint ~= state then
        self._showChargeRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_CHARGE, self:GetChargeRedPoint())
    end
end

function VIPManager:GetChargeRedPoint()
    return self._showChargeRedPoint
end


function VIPManager:RefreshAwardRedPoint()
	local levelAward = GameWorld.Player().vip_level_reward
	local dailyAward = GameWorld.Player().vip_daily_reward or 0
	local playerVipLevel = GameWorld.Player().vip_level or 0
	local vipReal = GameWorld.Player().vip_real or 0
	local state = false
	for i=1,self._maxLevel do
		self._awardRedPointState[i] = false
	end
	if vipReal > 0 and playerVipLevel > 0 then
		for i=1,playerVipLevel do
			if levelAward[i] == nil then
				self._awardRedPointState[i] = true
				state = true
			end
		end
		if dailyAward < playerVipLevel then
			if dailyAward == 0 then
				self._awardRedPointState[playerVipLevel] = true
			else
				for i=dailyAward+1,playerVipLevel do
					self._awardRedPointState[i] = true
				end
			end
			
			state = true
		end
	end
	self:SetAwardRedPoint(state)
end

function VIPManager:SetAwardRedPoint(state)
    if self._showAwardRedPoint ~= state then
        self._showAwardRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_VIP, self:GetAwardRedPoint())
    end
end

function VIPManager:GetAwardRedPointStates()
	return self._awardRedPointState
end

function VIPManager:GetAwardRedPoint()
    return self._showAwardRedPoint
end

-----------------------------客户端发送请求--------------------------------
--领取等级奖励
function VIPManager:RequestLevelReward(vipLevel)
	GameWorld.Player().server.vip_reward_req(action_config.VIP_LEVEL_REWARD_REQ,vipLevel)
end

--领取周奖励
-- function VIPManager:RequestWeekReward()
-- 	GameWorld.Player().server.vip_reward_req(action_config.VIP_WEEK_REWARD_REQ,0)
-- end
function VIPManager:RequestDailyReward()
	GameWorld.Player().server.vip_reward_req(action_config.VIP_DAILY_REWARD_REQ,0)
end

-- --领取vip投资奖励
-- function VIPManager:RequestInestGetReward(reward_day)
-- 	GameWorld.Player().server.invest_vip_action_req(action_config.INVEST_VIP_GET_REWARD,reward_day)
-- end

-- --vip投资
-- function VIPManager:RequestVipInest()
-- 	GameWorld.Player().server.invest_vip_action_req(action_config.INVEST_VIP_PAY,'0')
-- end


-- --领取等级投资奖励
-- function VIPManager:RequestLevelIneGetReward(reward_level)
-- 	GameWorld.Player().server.lv_invest_action_req(action_config.LV_INVEST_GET_REWARD,reward_level)
-- end

-- --等级投资
-- function VIPManager:RequestLevelInest(invest_type)
-- 	GameWorld.Player().server.lv_invest_action_req(action_config.LV_INVEST_PAY,invest_type)
-- end

-- --领取月卡投资奖励
-- function VIPManager:RequestMonthIneGetReward()
-- 	GameWorld.Player().server.investment_action_req(action_config.INVESTMENT_MONTH_CARD_DAILY_REFUND_REQ,'')
-- end

-- --月卡投资
-- function VIPManager:RequestMonthInest()
-- 	GameWorld.Player().server.investment_action_req(action_config.INVESTMENT_PURCHASE_MONTH_CARD_REQ,'')
-- end


-- --开服累充(领奖励)
-- function VIPManager:RequestAddIneGetReward(id)
-- 	GameWorld.Player().server.cumulative_charge_action_req(action_config.CUMULATIVE_CHARGE_REFUND_REQ,id..'')
-- end


-- -- 等级投资按钮显示限制时间
-- function VIPManager:LevelInvestmentBtnTimeLimit()
-- 	local d = DateTimeUtil.MinusServerDay(GameWorld.Player().create_time)
-- 	if d['day']<7 then
-- 		-- EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.LevelInvest, function()
-- 			-- local data={}
--     		-- data.firstTabIndex=5
-- 			-- GUIManager.ShowPanel(PanelsConfig.VIP,data)
-- 		-- end)
-- 	else
-- 		-- EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.LevelInvest)
-- 	end
-- end

VIPManager:Init()
return VIPManager