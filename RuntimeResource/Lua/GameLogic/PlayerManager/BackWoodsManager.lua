--黑暗禁地管理器-管理网络消息分发
local BackWoodsManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local backWoodsData = PlayerDataManager.backWoodsData
local BackwoodsDataHelper = GameDataHelper.BackwoodsDataHelper
local GameSceneManager = GameManager.GameSceneManager
local ClientEntityManager = GameManager.ClientEntityManager
local BossType = GameConfig.EnumType.BossType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig
local TeamManager = PlayerManager.TeamManager
local CommonWaitManager = PlayerManager.CommonWaitManager

function BackWoodsManager:Init()
    self:InitCallbackFunc()
end

function BackWoodsManager:InitCallbackFunc()
    self._onEnterMap = function(mapId) self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId) self:OnLeaveMap(mapId) end
end

function BackWoodsManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function BackWoodsManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function BackWoodsManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
    end    
end

function BackWoodsManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN then
        TaskCommonManager:SetCurAutoTask(nil, nil)
    end
end

function BackWoodsManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("BackWoodsManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        if action_id == action_config.FORBIDDEN_ENTER then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
            CommonWaitManager:EndWaitLoading(WaitEvents.FORBIDDEN_ENTER)
            EventDispatcher:TriggerEvent(GameEvents.On_Backwoods_Enter, false)
        end
        return
    end
    if action_id == action_config.FORBIDDEN_GET_DATA then
        self:RefreshBossData(args)
    elseif action_id == action_config.FORBIDDEN_GET_KILLER_RECORDS then
        self:RefreshGetKillerRecords(args)
    elseif action_id == action_config.FORBIDDEN_ENTER then
        -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
        CommonWaitManager:EndWaitLoading(WaitEvents.FORBIDDEN_ENTER)
        self:BackWoodsEnter(args)
    elseif action_id == action_config.FORBIDDEN_SPACE_BOSS_INFO then
        self:BackWoodsSpaceBossInfo(args)
        ClientEntityManager.bossargs={args,BossType.BackBoss}
        ClientEntityManager.UpdateBossTomb(args,BossType.BackBoss)
    elseif action_id == action_config.FORBIDDEN_NOTIFY_BOSS_WILL_REBORN then
        self:BossHomeNotifyBossWillReborn(args)
    elseif action_id == action_config.FORBIDDEN_NOTIFY_CHANGE_ANGER then
        --通知怒气值 返回：{angry}
        GUIManager.ShowPanel(PanelsConfig.BackwoodsSpaceInfo, {angry=args[1]})
    elseif action_id == action_config.FORBIDDEN_NOTIFY_WILL_EXIT then
        --通知怒气值已满，即将被踢出去 返回: {怒气值满的时间 }
        GUIManager.ShowPanel(PanelsConfig.BackwoodsSpaceInfo, {timestamp=args[1]})
    end
end

function BackWoodsManager:RefreshForbiddenInfo(changeValue)
    backWoodsData:RefreshForbiddenInfo(changeValue)
end

-------------------------------------数据处理------------------------------------
--{"1" = 1,"2" = {"1" = 0,"2" = 0,"3" = 0,"4" = 0,"5" = 0,"6" = 0,"7" = 0,"8" = 0},"3" = 0,"4" = {"2342" = 0,"2341" = 0}}
function BackWoodsManager:RefreshBossData(data)
    backWoodsData:RefreshBossData(data)
    EventDispatcher:TriggerEvent(GameEvents.On_Backwoods_Get_Info)
end

--根据楼层获取boss信息列表
function BackWoodsManager:GetAllBossInfoByFloor(floor)
    return backWoodsData:GetAllBossInfoByFloor(floor)
end

function BackWoodsManager:GotoBossByMapId(mapId, pos)
    local id = BackwoodsDataHelper:GetMonsterByMapId(mapId)
    if id then
        local boss = self:GetBossInfo(id)
        local data = {}
        data.id = MessageBoxType.BackWoodsTicket
        data.value = {boss, pos}
        GameManager.GUIManager.ShowPanel(PanelsConfig.MessageBox, data)
        --self:GotoBoss(boss, pos)
    end
end

function BackWoodsManager:GotoBoss(bossData, targetPos)
    local cb = function() self:ConfirmGotoBoss(bossData, targetPos) end
    TeamManager:CheckMatchStatusToInstance(cb)
end

function BackWoodsManager:ConfirmGotoBoss(bossData, targetPos)
    local pos = targetPos or GameSceneManager:GetMonsterPos(bossData:GetSenceId(), bossData:GetBossRegionId())
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBackWoodsSceneKillMonsterPoint,
        bossData:GetId(), bossData:GetSenceId(), pos, 5)
end

function BackWoodsManager:GotoElite(eliteData)
    local pos = GameSceneManager:GetMonsterPos(eliteData:GetEliteSenceId(), eliteData:GetEliteRegionId())
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBackWoodsSceneKillMonsterPoint,
        eliteData:GetId(), eliteData:GetEliteSenceId(), pos, 5)
end

function BackWoodsManager:GetBossTire()
    return backWoodsData:GetBossTire()
end

function BackWoodsManager:RefreshGetKillerRecords(args)
    EventDispatcher:TriggerEvent(GameEvents.On_Backwoods_Get_Killer_Records, args)
end

function BackWoodsManager:RefreshBossHomeInterestInfo(args)
    backWoodsData:RefreshBossHomeInterestInfo(args)
end

function BackWoodsManager:BackWoodsEnter(args)
    EventDispatcher:TriggerEvent(GameEvents.On_Backwoods_Enter, true)
end

function BackWoodsManager:BackWoodsSpaceBossInfo(args)
    backWoodsData:RefreshBackWoodsSpaceBossInfo(args)
    EventDispatcher:TriggerEvent(GameEvents.OnWorldBossSpaceBossInfo)
end

function BackWoodsManager:GetAllSpaceBossInfo()
    return backWoodsData:GetAllSpaceBossInfo()
end

function BackWoodsManager:GetAllSpaceEliteInfo()
    return backWoodsData:GetAllSpaceEliteInfo()
end

function BackWoodsManager:BossHomeNotifyBossWillReborn(args)
    local bossInfo = self:GetBossInfo(args[1])
    GUIManager.ShowPanel(PanelsConfig.WorldBossMainUI, bossInfo)
end

function BackWoodsManager:GetBossInfo(id)
    return backWoodsData:GetBossInfo(id)
end

function BackWoodsManager:GetRemainCount()
    return backWoodsData:GetRemainCount()
end

function BackWoodsManager:GetMaxCount()
    return backWoodsData:GetMaxCount()
end

function BackWoodsManager:GetPlayerCountByFloor(floor)
    return backWoodsData:GetPlayerCountByFloor(floor)
end

function BackWoodsManager:GetFloorBossId(bossId)
    local result = 0
    local showCount = GlobalParamsHelper.GetParamValue(625)
    for floor=1,showCount do
        local bossInfos = self:GetAllBossInfoByFloor(floor)
        for i, v in ipairs(bossInfos) do
            if v:GetId() == bossId then
                result = floor
                break
            end
        end
        if result ~= 0 then
            break
        end
    end
    return result
end
-------------------------------------客户端请求-----------------------------------
function BackWoodsManager:OnGetData(floor)
    local params = string.format("%d", floor)
    GameWorld.Player().server.forbidden_action_req(action_config.FORBIDDEN_GET_DATA, params)
end

function BackWoodsManager:OnBossEnter(id)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
    CommonWaitManager:BeginWaitLoading(WaitEvents.FORBIDDEN_ENTER)
    local params = string.format("%d", id)
    GameWorld.Player().server.forbidden_action_req(action_config.FORBIDDEN_ENTER, params)
end

function BackWoodsManager:OnBossIntrest(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.forbidden_action_req(action_config.FORBIDDEN_INTREST, params)
end

function BackWoodsManager:OnBossGetKillerRecords(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.forbidden_action_req(action_config.FORBIDDEN_GET_KILLER_RECORDS, params)
end

BackWoodsManager:Init()
return BackWoodsManager
