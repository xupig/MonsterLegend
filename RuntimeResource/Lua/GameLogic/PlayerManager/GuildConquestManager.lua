local GuildConquestManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local guildConquestData = PlayerManager.PlayerDataManager.guildConquestData
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local InstanceConfig = GameConfig.InstanceConfig
local SceneConfig = GameConfig.SceneConfig
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GameSceneManager = GameManager.GameSceneManager
local InstanceManager = PlayerManager.InstanceManager
local instanceBalanceData = PlayerManager.PlayerDataManager.instanceBalanceData

function GuildConquestManager:Init()
end

function GuildConquestManager:OnPlayerEnterWorld()
    self.sceneLoaded = false
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)

    self._sceneLoadFinish = function() self:SceneLoadFinish() end
    EventDispatcher:AddEventListener(GameEvents.Guild_Conquest_Scene_Load_Finish, self._sceneLoadFinish)

    self._showGodsTempleRedPoint = false
    GuildConquestManager:GuildTempleInfoReq()
end

function GuildConquestManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.Guild_Conquest_Scene_Load_Finish, self._sceneLoadFinish)
end

function GuildConquestManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("GuildConquestManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.GUILD_CONQUEST_LIST then
        self:RefreshListData(args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Conquest_List)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_BASIC_INFO then
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_BASIC_INFO, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Info, args)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_PLAYER_NUM then
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_PLAYER_NUM, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Player_Count, args)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_RESOURCE then
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_RESOURCE, args)
        EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Resource, args)
    elseif action_id == action_config.GUILD_CONQUEST_SETTLE then
        --结算
        EventDispatcher:TriggerEvent(GameEvents.On_Guild_Conquest_Settle, args)
        instanceBalanceData:UpdateData(args)
        local data = {}
        data.instanceBalanceData = instanceBalanceData
        GUIManager.ShowPanel(PanelsConfig.InstBalance, data)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_ROOM_STAGE then
        --通知副本stage和开始时间。{1:stage,2:start_time}。光门倒计时的开始和关闭。玩法开始的时间。
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_ROOM_STAGE, args)
    elseif action_id == action_config.GUILD_OVERLORD_GET_INFO then
        --查看主宰神殿信息
        self:RefreshTempleData(args)
    elseif action_id == action_config.GUILD_OVERLORD_GET_REWARD_STATE then
        --主宰神殿每日奖励领取状态
        self:RefreshTempleRewardState(args)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_CLICK_FLAG then
        --{orignal_id:faction_id}
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_CLICK_FLAG, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Conquest_Click_Flag_Info)
    elseif action_id == action_config.GUILD_CONQUEST_NOTIFY_PERSONAL_INFO then
        --{击杀，死亡，夺点}
        self:RefreshActionData(action_config.GUILD_CONQUEST_NOTIFY_PERSONAL_INFO, args)
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Conquest_Show_Personal_Info)
    end
end

function GuildConquestManager:RefreshActionData(action_id, data)
    guildConquestData:RefreshActionData(action_id, data)
end

function GuildConquestManager:RefreshListData(data)
    guildConquestData:RefreshListData(data)
end

function GuildConquestManager:RefreshTempleData(data)
    guildConquestData:RefreshTempleData(data)
    --变化同时请求奖励领取状态
    self:GuildTempleRewardStateReq()
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Temple_Info)
end

function GuildConquestManager:RefreshTempleRewardState(data)
    guildConquestData:RefreshTempleRewardState(data)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guild_Temple_Reward_State)
    self:RefreshGodsTempleRedPointState()
end

function GuildConquestManager:SceneLoadFinish()
    self.sceneLoaded = true
    EventDispatcher:TriggerEvent(GameEvents.Check_Guild_Conquest_Close_Block)
end

function GuildConquestManager:GetSceneLoadFinish()
    return self.sceneLoaded
end

function GuildConquestManager:GetListData()
    return guildConquestData.conquestListData
end

function GuildConquestManager:GetActionData(action_id)
    return guildConquestData:GetActionData(action_id)
end

function GuildConquestManager:HasTempleData()
    return guildConquestData:HasTempleData()
end

function GuildConquestManager:IsSelfGuild()
    return guildConquestData:IsSelfGuild()
end

function GuildConquestManager:CanGuildConquestEnter()
    return true
end

function GuildConquestManager:GetClickFlagInfo()
    return guildConquestData:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_CLICK_FLAG) or {}
end

function GuildConquestManager:SetCanMoveByPath(state)
    guildConquestData.canMoveByPath = state
end

function GuildConquestManager:GetCanMoveByPath()
    return guildConquestData.canMoveByPath
end
-----------------------------------红点------------------------------
function GuildConquestManager:RefreshGodsTempleRedPointState()
    if self:IsSelfGuild() and guildConquestData.templeRewardState == 0 then
        self:SetGodsTempleRedPoint(true)
    else
        self:SetGodsTempleRedPoint(false)
    end
end

function GuildConquestManager:SetGodsTempleRedPoint(state)
    if self._showGodsTempleRedPoint ~= state then
        self._showGodsTempleRedPoint = state
        EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_MASTER_TEMPLE, self:GetGodsTempleRedPoint())
    end
end

function GuildConquestManager:GetGodsTempleRedPoint()
    return self._showGodsTempleRedPoint
end

-----------------------------副本处理---------------------------
function GuildConquestManager:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        GameWorld.Player():StopMoveWithoutCallback()
        self.sceneLoaded = false
        guildConquestData:RetSetActionData()
        EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, GameWorld.Player().id)
    end
end

function GuildConquestManager:OnEnterMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_CONQUEST then
        GameWorld.Player():StopMoveWithoutCallback()
        local data = self:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_BASIC_INFO)
        if data ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Info, data)
        end
        data = self:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_PLAYER_NUM)
        if data ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Player_Count, data)
        end
        data = self:GetActionData(action_config.GUILD_CONQUEST_NOTIFY_RESOURCE)
        if data ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Set_Guild_Conquest_Resource, data)
        end
        EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, GameWorld.Player().id)
        self:SetCanMoveByPath(false)
    end
end

-------------------------------------客户端请求-----------------------------------
--进入
function GuildConquestManager:GuildConquestEnterReq()
    GameWorld.Player().server.guild_conquest_action_req(action_config.GUILD_CONQUEST_ENTER, "")
end

function GuildConquestManager:GuildConquestListReq()
    GameWorld.Player().server.guild_conquest_action_req(action_config.GUILD_CONQUEST_LIST, "")
end

--查看主宰神殿信息
function GuildConquestManager:GuildTempleInfoReq()
    GameWorld.Player().server.guild_conquest_action_req(action_config.GUILD_OVERLORD_GET_INFO, "")
end

--领取主宰神殿每日奖励
function GuildConquestManager:GuildTempleRewardReq()
    GameWorld.Player().server.guild_conquest_action_req(action_config.GUILD_OVERLORD_GET_REWARD, "")
end

--主宰神殿每日奖励领取状态
function GuildConquestManager:GuildTempleRewardStateReq()
    GameWorld.Player().server.guild_conquest_action_req(action_config.GUILD_OVERLORD_GET_REWARD_STATE, "")
end

--占领资源点
function GuildConquestManager:GuildConquestOccupyReq(id)
    GameWorld.Player().server.take_click_flag(id)
end

--开始占领资源点
function GuildConquestManager:GuildConquestStartOccupyReq(id)
    GameWorld.Player().server.take_click_flag_pre(id)
end

--中断占领资源点
function GuildConquestManager:GuildConquestInterruptOccupyReq(id)
    GameWorld.Player().server.take_click_flag_break(id)
end

GuildConquestManager:Init() 
return GuildConquestManager