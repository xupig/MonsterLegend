local CrossServerManager = {}
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig


function CrossServerManager:IsInCrossScene()
    local sceneType = GameSceneManager:GetCurSceneType()
    if sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        return true
    elseif sceneType == SceneConfig.SCENE_TYPE_ONLINE_PVP then
    	return PlayerManager.OnlinePvpManager:IsCrossServer()
    end
    return false
end

return CrossServerManager