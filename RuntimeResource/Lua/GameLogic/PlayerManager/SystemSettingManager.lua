--SystemSettingManager.lua
local SystemSettingManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local SystemSettingConfig = GameConfig.SystemSettingConfig
local action_config = GameWorld.action_config
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local Time = Time
local PauseFpsType = GameConfig.EnumType.PauseFpsType

function SystemSettingManager:Init()
    self._baseSystemSettingData = PlayerDataManager.baseSystemSettingData
    self._autoGameSettingData = PlayerDataManager.autoGameSettingData

    EventDispatcher:AddEventListener(GameEvents.OnMusicChanged, function(num) self:OnMusicChanged(num) end)
    EventDispatcher:AddEventListener(GameEvents.OnSoundChanged, function(num) self:OnSoundChanged(num) end)
    EventDispatcher:AddEventListener(GameEvents.OnQualityChanged, function() self:OnQualityChanged() end)
    
    self._baseSystemSettingData:LoadData()
    self._autoGameSettingData:LoadData()

    EventDispatcher:TriggerEvent(GameEvents.OnMusicChanged)
    EventDispatcher:TriggerEvent(GameEvents.OnSoundChanged)
end

function SystemSettingManager:OnPlayerEnterWorld()
    self:SetPlayerNum(self:GetPlayerSetting(SystemSettingConfig.PLAYER_NUM))--同屏人数设置
    self:SetMonsterHide(self:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER))--其他怪物屏蔽
    self:SetWingHide(self:GetPlayerSetting(SystemSettingConfig.OTHER_WING))--翅膀屏蔽
    self:SetPetHide(self:GetPlayerSetting(SystemSettingConfig.OTHER_PET))--宠物屏蔽
    self:SetFabaoHide(self:GetPlayerSetting(SystemSettingConfig.OTHER_HUNQI))--法宝屏蔽
    self:SetSkillFxHide(self:GetPlayerSetting(SystemSettingConfig.OTHER_SKILLFX))--他人技能特效屏蔽

    self:SetRealTimeShadow(self:GetPlayerSetting(SystemSettingConfig.REALTIME_SHADOWS))--实时阴影
    self:SetCanPowerSave(self:GetPlayerSetting(SystemSettingConfig.SAVE_POWER_MODE))--省电模式

    
    EventDispatcher:TriggerEvent(GameEvents.OnQualityChanged)
    self:AddFpsTimer()
    self:InitPauseFps()
end

function SystemSettingManager:OnPlayerLeaveWorld()
    self:RemoveFpsTimer()
end

--同屏人数设置
function SystemSettingManager:SetPlayerNum(num)
    GameWorld.SetMaxScreenPlayerCount(num)
end

--他人技能特效屏蔽
function SystemSettingManager:SetSkillFxHide(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetHideOtherSkillFx(mode)--0:显示 1：隐藏
end

--法宝屏蔽
function SystemSettingManager:SetFabaoHide(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetHideOtherFabao(mode)--0:显示 1：隐藏
end


--宠物屏蔽
function SystemSettingManager:SetPetHide(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetHideOtherPet(mode)--0:显示 1：隐藏
end

--其他怪物屏蔽
function SystemSettingManager:SetMonsterHide(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetHideMonster(mode)--0:显示 1：隐藏
end

--翅膀屏蔽
function SystemSettingManager:SetWingHide(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetHideOtherWing(mode)--0:显示 1：隐藏
end

-- 实时阴影：1是开，0是关
function SystemSettingManager:SetRealTimeShadow(value)
    local mode = 0
    if value then
        mode = 1
    end
    GameWorld.SetRealTimeShadow(mode)
end

-- 组队自动同意
function SystemSettingManager:SetAutoTeamConfirm(value)
    -- do something you want
end

-- 省电模式
function SystemSettingManager:SetSavePowerModeSetting(value)
    self:SetCanPowerSave(value)
end

function SystemSettingManager:SetCanPowerSave(value)
    PlayerManager.PowerSaveManager:SetCanPowerSave(value)
end

function SystemSettingManager:SetPlayerSetting(settingType,data)
    local changeTitle = false
    if settingType == SystemSettingConfig.PLAYER_NUM and self:GetPlayerSetting(SystemSettingConfig.PLAYER_NUM) ~= data then
        self:SetPlayerNum(data)
    elseif settingType == SystemSettingConfig.OTHER_MONSTER and self:GetPlayerSetting(SystemSettingConfig.OTHER_MONSTER) ~= data then
        self:SetMonsterHide(data)
    elseif settingType == SystemSettingConfig.OTHER_WING and self:GetPlayerSetting(SystemSettingConfig.OTHER_WING) ~= data then
        self:SetWingHide(data)
    elseif settingType == SystemSettingConfig.OTHER_PET and self:GetPlayerSetting(SystemSettingConfig.OTHER_PET) ~= data then
        self:SetPetHide(data)
    elseif settingType == SystemSettingConfig.OTHER_HUNQI and self:GetPlayerSetting(SystemSettingConfig.OTHER_HUNQI) ~= data then
        self:SetFabaoHide(data)
    elseif settingType == SystemSettingConfig.OTHER_SKILLFX and self:GetPlayerSetting(SystemSettingConfig.OTHER_SKILLFX) ~= data then
        self:SetSkillFxHide(data)
    elseif settingType == SystemSettingConfig.OTHER_TITLE and self:GetPlayerSetting(SystemSettingConfig.OTHER_TITLE) ~= data then
        changeTitle = true
    elseif settingType == SystemSettingConfig.REALTIME_SHADOWS and self:GetPlayerSetting(SystemSettingConfig.REALTIME_SHADOWS) ~= data then
        self:SetRealTimeShadow(data)
    elseif settingType == SystemSettingConfig.AUTO_TEAMCONFIRM and self:GetPlayerSetting(SystemSettingConfig.AUTO_TEAMCONFIRM) ~= data then
        self:SetAutoTeamConfirm(data)
    elseif settingType == SystemSettingConfig.SAVE_POWER_MODE and self:GetPlayerSetting(SystemSettingConfig.SAVE_POWER_MODE) ~= data then
        self:SetSavePowerModeSetting(data)
    end

    self._baseSystemSettingData:SetData(settingType,data)
    self._baseSystemSettingData:SaveData()

    if changeTitle then
        PlayerManager.TitleManager:RefreshHideOtherTitle(data)
    end
end

function SystemSettingManager:GetPlayerSetting(settingType)
    return self._baseSystemSettingData:GetData(settingType)
end

function SystemSettingManager:GetAllPlayerSetting()
    return self._baseSystemSettingData:GetAllData()
end

function SystemSettingManager:GetAutoGameSetting(settingType)
    return self._autoGameSettingData:GetData(settingType)
end

function SystemSettingManager:IsCanGetByQuality(quality)
    if quality == ItemConfig.ItemQualityWhite then--白色装备
        return self:GetAutoGameSetting(SystemSettingConfig.AUTO_GET_WHITE)
    elseif quality == ItemConfig.ItemQualityBlue then--蓝色装备
        return self:GetAutoGameSetting(SystemSettingConfig.AUTO_GET_BLUE)
    elseif quality == ItemConfig.ItemQualityPurple then--紫色装备
        return self:GetAutoGameSetting(SystemSettingConfig.AUTO_GET_PURPLE)
    elseif quality >= ItemConfig.ItemQualityOrange then--橙色以及以上装备
        return self:GetAutoGameSetting(SystemSettingConfig.AUTO_GET_ORANGE)
    end
end

function SystemSettingManager:SetAutoGameSetting(settingType, data)
    self._autoGameSettingData:SetData(settingType,data)
    self._autoGameSettingData:SaveData()
end

function SystemSettingManager:OnMusicChanged(num)
    if num == nil then
        num = self:GetPlayerSetting(SystemSettingConfig.MUSIC_VOLUME)
    else
        self:SetPlayerSetting(SystemSettingConfig.MUSIC_VOLUME, num)
    end    
    GameWorld.SetMusicVolume(num)
end

function SystemSettingManager:OnSoundChanged(num)
    if num == nil then
        num = self:GetPlayerSetting(SystemSettingConfig.SOUND_VOLUME)
    else
        self:SetPlayerSetting(SystemSettingConfig.SOUND_VOLUME, num)
    end
    GameWorld.SetSoundVolume(num)
end

function SystemSettingManager:OnQualityChanged()
    --LoggerHelper.Error("SystemSettingManager:OnQualityChanged()" .. tostring(self:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY)))
    GameWorld.SetGameQuality(self:GetPlayerSetting(SystemSettingConfig.RENDER_QUALITY))
end

function SystemSettingManager:CheckLock(cb)
    if self:IsLock() then
        GUIManager.ShowPanel(PanelsConfig.SystemSettingShow, cb)
    else
        if cb then
            cb()
        end
    end
end

function SystemSettingManager:IsLock()
    local info = GameWorld.Player().security_lock_info
    if table.isEmpty(info) or (info[public_config.SECURITY_LOCK_FLAG] == nil or info[public_config.SECURITY_LOCK_FLAG] ~= 1) then
        return false
    end
    return true 
end

function SystemSettingManager:IsHasPassword()
    local info = GameWorld.Player().security_lock_info
    if table.isEmpty(info) or (info[public_config.SECURITY_LOCK_PWD] == nil) then
        return false
    end
    return true
end

function SystemSettingManager:CheckPasswordToDo(password, cb)
    self._toDo = true
    self._toDoFunc = cb
    self:Unlock(password)
end

function SystemSettingManager:HandleData(action_id, error_id, args)
    if error_id ~= 0 then --报错了
        LoggerHelper.Error("SystemSettingManager HandleData Error:" .. error_id)
        return
    elseif action_id == action_config.SETTING_SET_SECURITY_LOCK_REQ then --10251, --设置安全锁密码
        -- LoggerHelper.Log("sam 10251 设置安全锁密码 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SYSTEM_SAFE_SETTING_SET_PASSWORD)
    elseif action_id == action_config.SETTING_CLEAR_SECURITY_LOCK_REQ then --10252, --清除密码
        -- LoggerHelper.Log("sam 10252 清除密码 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SYSTEM_SAFE_SETTING_CLEAR_PASSWORD)
    elseif action_id == action_config.SETTING_UNLOCK_SECURITY_LOCK_REQ then --10253, --解除安全锁
        -- LoggerHelper.Log("sam 10253 解除安全锁 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SYSTEM_SAFE_SETTING_UNLOCK)
        if self._toDo then
            self._toDo = false
            GUIManager.ClosePanel(PanelsConfig.SystemSettingShow)
            if self._toDoFunc then
                self._toDoFunc()
            end
        end
    elseif action_id == action_config.SETTING_LOCK_SECURITY_LOCK_REQ then --10254, --上锁安全锁
        -- LoggerHelper.Log("sam 10254 上锁安全锁 == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SYSTEM_SAFE_SETTING_LOCK)
    elseif action_id == action_config.SETTING_MODIFY_SECURITY_LOCK_PWD_REQ then --10255, 修改安全锁密码(密保问题答案, 新密码)
        -- LoggerHelper.Log("sam 10255 修改安全锁密码(密保问题答案, 新密码) == " .. PrintTable:TableToStr(args))
        EventDispatcher:TriggerEvent(GameEvents.SYSTEM_SAFE_SETTING_MODIFY)
    else 
        LoggerHelper.Error("SystemSettingManager HandleData action id not handle:" .. action_id)
    end
end


--是否配置了自动复活
function SystemSettingManager:IsCanAutoRevival()
    return GameWorld.Player().hang_up_setting[public_config.HANG_UP_KEY_AUTO_BUY_REVIVAL] == 1
end

--7151, --自动拾取
function SystemSettingManager:SetAutoGetSetting(type)
    GameWorld.Player().server.hang_up_action_req(action_config.HANG_UP_CHANGE_AUTO_PICK, tostring(type))
end

--7152, --其他设置自动
function SystemSettingManager:SetAutoSetting(type)
    GameWorld.Player().server.hang_up_action_req(action_config.HANG_UP_CHANGE_AUTO_SETTING, tostring(type))
end

--10251, --设置安全锁密码
function SystemSettingManager:SetLockPassword(password, questionId, answer)
    GameWorld.Player().server.setting_action_req(action_config.SETTING_SET_SECURITY_LOCK_REQ, tostring(password) .. "," .. tostring(questionId).. "," .. tostring(answer))
end

--10252, --清除密码
function SystemSettingManager:ClearPassword(answer)
    GameWorld.Player().server.setting_action_req(action_config.SETTING_CLEAR_SECURITY_LOCK_REQ, tostring(answer))
end

--10253, --解除安全锁
function SystemSettingManager:Unlock(password)
    GameWorld.Player().server.setting_action_req(action_config.SETTING_UNLOCK_SECURITY_LOCK_REQ, tostring(password))
end

--10254, --上锁安全锁
function SystemSettingManager:Lock()
    GameWorld.Player().server.setting_action_req(action_config.SETTING_LOCK_SECURITY_LOCK_REQ, "")
end

--10255, 修改安全锁密码(密保问题答案, 新密码)
function SystemSettingManager:ModifyPassword(answer, password)
    GameWorld.Player().server.setting_action_req(action_config.SETTING_MODIFY_SECURITY_LOCK_PWD_REQ, tostring(answer) .. "," .. tostring(password))
end

--=================== 调整同屏人数 begin ====
local reduceNum = 4
local fpsCheckTime = 26
function SystemSettingManager:AddFpsTimer()
    self:RemoveFpsTimer()
    self._fpsTimer = TimerHeap:AddSecTimer(fpsCheckTime, fpsCheckTime, 0, function()
        if PlayerManager.PowerSaveManager:IsInPowerSaving() then return end
        self:AdjustPlayerNum()
    end)
end

function SystemSettingManager:RemoveFpsTimer()
    if self._fpsTimer then
        TimerHeap:DelTimer(self._fpsTimer)
        self._fpsTimer = nil
        self:ResetPlayerNumSetting()
    end
end

function SystemSettingManager:AdjustPlayerNum()
    local lowFps = GameWorld.GetLowFps()
    if lowFps then
        local max_num = self._tempPlayerNum or GameWorld.GetMaxScreenPlayerCount()
        if max_num > 0 then
            max_num = max_num - reduceNum
            max_num = max_num < 0 and 0 or max_num
            self._tempPlayerNum = max_num
            self:SetPlayerNum(max_num)
        end
    else
        self:ResetPlayerNumSetting()
    end
end

function SystemSettingManager:ResetPlayerNumSetting()
    if self._tempPlayerNum then
        local setting_num = self:GetPlayerSetting(SystemSettingConfig.PLAYER_NUM)
        if self._tempPlayerNum ~= setting_num then
            self:SetPlayerNum(setting_num)
        end
    end
    self._tempPlayerNum = nil
end
--=================== 调整同屏人数 end ====


function SystemSettingManager:InitPauseFps()
    local keys = {PauseFpsType.PowerSave, PauseFpsType.SceneChange}
    GameWorld.InitPauseFps(keys, false)
end

function SystemSettingManager:SetGameQuality(value)
    GameWorld.SetGameQuality(value)
    EventDispatcher:TriggerEvent(GameEvents.OnQualityChanged, value)
end

SystemSettingManager:Init()
return SystemSettingManager