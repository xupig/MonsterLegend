local BagManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local bagData = PlayerDataManager.bagData
local PkgConfig = GameConfig.PkgConfig
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RoleManager = PlayerManager.RoleManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function BagManager:Init()
    self._pkgsType = {}
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_ITEMS]               = public_config.PKG_TYPE_ITEM
    self._pkgsType[PkgConfig.WAREHOUSE_TYPE_PKG_ITEMS]         = public_config.PKG_TYPE_WAREHOUSE
    self._pkgsType[PkgConfig.LOADED_EQUIP_TYPE_PKG_ITEMS]      = public_config.PKG_TYPE_LOADED_EQUIP
    self._pkgsType[PkgConfig.SOLD_TMP_TYPE_PKG_ITEMS]          = public_config.PKG_TYPE_SOLD_TMP
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_FASHION]             = public_config.PKG_TYPE_FASHION
    self._pkgsType[PkgConfig.LOADED_FASHION_TYPE_PKG_FASHION]  = public_config.PKG_TYPE_LOADED_FASHION
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_RIDE]                = public_config.PKG_TYPE_RIDE
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_GOD_MOSNTER]         = public_config.PKG_TYPE_GOD_MOSNTER
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_LOTTERY_EQUIP]       = public_config.PKG_TYPE_LOTTERY_EQUIP
    self._pkgsType[PkgConfig.BAG_TYPE_PKG_CARD_PIECE]          = public_config.PKG_TYPE_CARD_PIECE
    
    self._pkgItemChange = function(pkgType, pos1, pos2, flag) self:PkgItemChange(pkgType, pos1, pos2, flag) end
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_REMOVE, self._pkgItemChange)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_REMOVE2, self._pkgItemChange)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_UPDATE, self._pkgItemChange)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_ADD, self._pkgItemChange)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_READY_REMOVE, self._pkgItemChange)
    EventDispatcher:AddEventListener(GameEvents.ITEM_CHANGE_READY_REMOVE2, self._pkgItemChange)

    --打开背包回调
    self._openBagFun = function ()
        GUIManager.ShowPanelByFunctionId(2)
    end

    self._requestUseCb = function ()
        self:RequestUseOnce()
    end

    self._delayBagEquipStateHandlerCb = function ()
        self:DelayBagEquipStateHandler()
    end
end

function BagManager:OnPlayerLeaveWorld()
    bagData:ClearData()
end


function BagManager:GetBagType(pkgname)
    return self._pkgsType[pkgname]
end

function BagManager:GetEntityProp(pkgType)
    for k,v in pairs(self._pkgsType) do
        if pkgType == v then
            return k
        end
    end
    return nil
end

--背包数据发生变化
function BagManager:OnChanged(pkgname, data, change, getWay)
	--LoggerHelper.Log(pkgname)
	--self._pkgs 的key统一使用pkgtype(即数字1,2,3),pkgname在manager层面进行转化，data层不再使用字符串pkgname
    local pkgType = self:GetBagType(pkgname)
	bagData:OnPkgChanged(pkgType, data, change, getWay)
	EventDispatcher:TriggerEvent(GameEvents.BAG_DATA_UPDATE,pkgType)

    self:CheckFull(pkgType)
end

--背包整理
function BagManager:OnSort(pkgType,data)
    --整理背包的时候要将一份背包数据重新覆盖PlayerAvatar字段上
    GameWorld.Player()[self:GetEntityProp(pkgType)] = data
    bagData:OnPkgChanged(pkgType, data, nil)
    EventDispatcher:TriggerEvent(GameEvents.BAG_DATA_UPDATE,pkgType)
end

function BagManager:OnVoulumnChanged(pkgType)
    EventDispatcher:TriggerEvent(GameEvents.BAG_VOLUMN_UPDATE,pkgType)

    self:CheckFull(pkgType)
end

--背包满的提示
function BagManager:CheckFull(pkgType)
    if pkgType == public_config.PKG_TYPE_ITEM then
        if bagData:GetPkg(public_config.PKG_TYPE_ITEM):IsFull() then
            EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self._openBagFun,EnumType.RemindType.BagFill)
        else
            EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.BagFill)
        end
    end
end

function BagManager:OpenStorage()
    GUIManager.ShowPanel(PanelsConfig.Bag,nil,function ()
            GUIManager.GetPanel(PanelsConfig.Bag):ShowStorage()
        end)
end

--qulity:品质(4:紫色)
--level:品阶(10)
--condition:限定的装备类型列表
--根据装备的品质和品阶获取：大于品阶的品质的装备；如：获取大于10阶的紫色装备
function BagManager:GetEquipByQulityAndLevel(quality, level, typeList)
    local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
	local curVolumn = GameWorld.Player().pkg_items_size
	local data = {}
	for i=1,curVolumn do
		local itemData = t[i]
         if itemData then
            if itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP and 
                itemData:GetQuality() == quality and 
                itemData:GetGrade() >= level then
                if typeList == nil or  table.containsValue(typeList, itemData:GetType()) then
                    table.insert(data,itemData)
                end
            end
	 	end
    end
    return data
end

local removeData
--背包物品发生变化 table双层数据时pos2有值 ITEM_CHANGE_READY_REMOVE时flag为1 ITEM_CHANGE_REMOVE为2
function BagManager:PkgItemChange(pkgType, pos1, pos2, flag)
    local data = bagData:GetItem(pkgType,pos1)
    if flag == 1 then
        removeData = data
        data = nil
    elseif flag == 2 then
        data = removeData
    end
    --背包
    if pkgType == public_config.PKG_TYPE_ITEM then
        if data ~= nil then
            local cfgData = data.cfgData
            if cfgData.itemType == public_config.ITEM_ITEMTYPE_ITEM then
                --宠物相关道具变化
                if self:IsPetDevourItem(data.cfg_id) then
                    --宠物吞噬道具
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Pet_Devour_Red_Point_State)
                end
                if cfgData.type == 4 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Pet_Item_Data)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Pet_Upgrade_Red_Point_State)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Pet_Soul_Red_Point_State)
                end
                --坐骑相关道具变化
                if cfgData.type == 5 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Ride_Item_Data)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Upgrade_Red_Point_State)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Ride_Soul_Red_Point_State)
                end

                --战魂相关道具变化
                if cfgData.type == 14 then
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Guardian_Red_Point_State)
                end

                --套装相关道具变化
                if cfgData.type == 15 then
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Suit_Red_Point_State)
                end

                --翅膀相关道具变化
                if cfgData.type == 6 then
                    --EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Wing_Item_Data)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Wing_Upgrade_Red_Point_State)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Wing_Soul_Red_Point_State)
                end

                --法宝相关道具变化
                if cfgData.type == 7 then
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Talisman_Upgrade_Red_Point_State)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Talisman_Soul_Red_Point_State)
                end

                --神兵相关道具变化
                if cfgData.type == 8 then
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Weapon_Upgrade_Red_Point_State)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Weapon_Soul_Red_Point_State)
                end

                --幻化相关道具变化
                if cfgData.type >= 16 and cfgData.type <= 21 then
                    local systemType = cfgData.type-15
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Transform_Red_Point_State,systemType)
                end

                 --爵位相关道具变化
                if RoleManager:IsNobilityItem(data.cfg_id) then
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Nobility_Red_Point_State)
                end
                
                --公会令牌道具变化
                local itemId, _ = next(GlobalParamsHelper.GetParamValue(556))
                if data.cfg_id == itemId then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Guild_Token_Info)
                end
                --减少PK值道具变化
                local itemId = GlobalParamsHelper.GetParamValue(919)
                if data.cfg_id == itemId then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_PK_Item_Info)
                end

                --公会神兽兽核道具变化
                local itemId = GlobalParamsHelper.GetParamValue(606)
                if data.cfg_id == itemId then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Mythical_Creatures_Soul_Item_Info)
                end

                --合成刷新
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Compose_Item_Red_Point_State,data.cfg_id)

                --限时活动道具
                PlayerManager.OperatingActivityManager:RefreshCelebrationRedPointByItem(data.cfg_id)
                

                --活动道具
                PlayerManager.ServiceActivityManager:RefreshActivityWordRedPointByItem(data.cfg_id)

            elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP then
                --背包里的装备发生变化
                if self._delayBagEquipState == nil then
                    self._delayBagEquipState = {}
                end
                self._delayBagEquipState[cfgData.type] = true
                if self._delayBagEquipStateTimer then
                    TimerHeap.DelTimer(self._delayBagEquipStateTimer)
                end 
                self._delayBagEquipStateTimer = TimerHeap:AddSecTimer(1, 0, 0, self._delayBagEquipStateHandlerCb)
            --宝石发生变化
            elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_GEM then
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Gem_Inlay_Red_Point_State)
                EventDispatcher:TriggerEvent(GameEvents.Refresh_Compose_Gem_Red_Point_State,cfgData.id)
            elseif cfgData.itemType == public_config.ITEM_ITEMTYPE_FASHION then
                --时装cloth道具变化
                if cfgData.type == 1 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Cloth_Info)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Red_Point, public_config.FASHION_TYPE_CLOTHES)
                end
                --时装weapon道具变化
                if cfgData.type == 2 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Weapon_Info)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Red_Point, public_config.FASHION_TYPE_WEAPON)
                end
                --时装特效道具变化
                if cfgData.type == 3 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Effect_Info)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Red_Point, public_config.FASHION_TYPE_SPECIAL)
                end
                --时装头像框道具变化
                if cfgData.type == 4 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Photo_Info)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Red_Point, public_config.FASHION_TYPE_PHOTO)
                end
                --时装聊天框道具变化
                if cfgData.type == 5 then
                    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Fashion_Chat_Info)
                    EventDispatcher:TriggerEvent(GameEvents.Refresh_Fashion_Red_Point, public_config.FASHION_TYPE_CHAT)
                end
            end

            --装备寻宝道具变化
            local itemId, _ = next(GlobalParamsHelper.GetParamValue(750))
            if data.cfg_id == itemId then
                EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Equip_Find_Key_Info)
            end
            --符文寻宝道具变化
            local itemId, _ = next(GlobalParamsHelper.GetParamValue(610))
            if data.cfg_id == itemId then
                EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Rune_Find_Key_Info)
            end
        end
    end
    --装备背包变化
    if pkgType == public_config.PKG_TYPE_LOADED_EQUIP then
        if data ~= nil then
            local cfgData = data.cfgData
            
            EventDispatcher:TriggerEvent(GameEvents.Update_Bag_Equip_State,pkgType,cfgData.type)
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Gem_Inlay_Red_Point_State,pos1)
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Equip_Strength_Red_Point_State,pos1)
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Gem_Refine_Red_Point_State,pos1)
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Suit_Red_Point_State,pos1)
        end
    end

    --神兽背包变化
    if pkgType == public_config.PKG_TYPE_GOD_MOSNTER then
        if data ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Animal_Red_Point_State)
        end        
    end
    --图鉴背包变化
    if pkgType == public_config.PKG_TYPE_CARD_PIECE then
        if data ~= nil then
            EventDispatcher:TriggerEvent(GameEvents.Refresh_Card_Piece_Red_Point_State)
        end        
    end
end

--延迟处理背包增加装备的红点相关
function BagManager:DelayBagEquipStateHandler()
    for equipType,_ in pairs(self._delayBagEquipState) do
        EventDispatcher:TriggerEvent(GameEvents.Update_Bag_Equip_State,public_config.PKG_TYPE_ITEM,equipType)       
    end
    self._delayBagEquipState = {}
    TimerHeap.DelTimer(self._delayBagEquipStateTimer)
    self._delayBagEquipStateTimer = nil
    --刷新是否有可自动吞噬装备
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Pet_Devour_Red_Point_State)
end

--道具分解完成
function BagManager:OnSellComplete(code)
    if code == 0 then
        
        EventDispatcher:TriggerEvent(GameEvents.UIEVENT_BAG_SELL_COMPLETE)
    end
end

--背包整理完成
function BagManager:OnSortComplete(code)
    if code == 0 then
        GUIManager.ClosePanel(PanelsConfig.NewEquipTip)
    end
end

function BagManager:IsPetDevourItem(itemId)
    for k,v in pairs(PartnerDataHelper.GetPetLevelItems()) do
        if k == itemId then
            return true
        end
    end
    return false
end

--------------------------------------------背包类请求---------------------------------------------------------
--开启背包格子请求
function BagManager:RequestBuyBagCount(buyCount)
    GameWorld.Player().server.buy_bag_count_req(buyCount)
end

--开启仓库格子请求
function BagManager:RequestBuyWarehouseCount(buyCount)
    GameWorld.Player().server.buy_warehouse_count_req(buyCount)
end

--整理背包
function BagManager:RequestTiny(pkgType)
   GameWorld.Player().server.bag_action_req(action_config.ITEM_SORT_REQ,tostring(pkgType))
end

--移动背包物品
function BagManager:RequestMoveItem(srcBagType,srcBagPos,targetBagType,targetBagPos)
    local str = tostring(srcBagType)..","..tostring(srcBagPos)..","..tostring(targetBagType)..","..tostring(targetBagPos)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_MOVE_REQ,str)
end

--道具使用
function BagManager:RequsetUse(pkg_pos,count)
    local str = tostring(pkg_pos)..","..tostring(count)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_USE_REQ,str)
end

--道具出售
function BagManager:RequestSell(paramStr)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_SELL_REQ,paramStr)
end

--装备分解
function BagManager:RequestBreak(bagPosList)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_BREAK_REQ,bagPosList)
end

--道具使用
--useIndex,多选一道具使用时候选择的索引
function BagManager:RequestUse(bagPos, count , useIndex)
    local str = tostring(bagPos)..","..tostring(count)..","..tostring(useIndex)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_USE_REQ, str)
end

--道具拆分
function BagManager:RequestSplit(bagPos, count)
    local str = tostring(bagPos)..","..tostring(count)
    GameWorld.Player().server.bag_action_req(action_config.ITEM_PKG_SPLIT_REQ, str) 
end


--修理装备
function BagManager:RequestRepairEquip(pkgType,bagPos)
    GameWorld.Player().server.bag_action_req(action_config.EQUIP_REPAIR_REQ, pkgType..","..bagPos)
end

--修理所有身上的装备
function BagManager:RequestRepairAllEquipLoaded(pkgType)
    GameWorld.Player().server.bag_action_req(action_config.EQUIP_REPAIR_ONE_KEY_REQ, tostring(pkgType))
end


-- 钻石、绑钻 获得 消耗 时上传log
function BagManager:OnBagActionResp(act_id, code, args)  
-- LoggerHelper.LogEx('=============OnBagActionResp=============act_id:' .. act_id)
    if act_id == action_config.ITEM_NOTIFY_GET_MONEY then  --通知客户端获得钻石和绑钻{item_id,item_count,get_way}
        -- LoggerHelper.LogEx(args)
        local way = LanguageDataHelper.GetContent(86000 + args[3]) 
        
        if args[1] == 2 then  --钻石
            GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),'', tostring(way),tostring(way) .. 'x' .. args[2],true,true,GameWorld.Player().money_coupons)
        else
            GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),'', tostring(way),tostring(way) .. 'x' .. args[2],false,true,GameWorld.Player().money_coupons_bind)
        end
    elseif act_id == action_config.ITEM_NOTIFY_COST_MONEY then  --通知客户端消耗钻石和绑钻{item_id,item_count,cost_way,add_item_id,add_count}
        -- LoggerHelper.LogEx(args)
        local way = LanguageDataHelper.GetContent(85000 + args[3]) 

        if args[3] == public_config.ITEM_COST_WAY_SHOP_BUY then
            local shopItemName = GameDataHelper.ItemDataHelper.GetItemName(args[4])
            local shopItemDes = GameDataHelper.ItemDataHelper.GetDescription(args[4])
            if args[1] == 2 then  --钻石
                GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),tostring(args[5]), tostring(shopItemName),tostring(shopItemDes),true,false,GameWorld.Player().money_coupons)
            else
                GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),tostring(args[5]), tostring(shopItemName),tostring(shopItemDes),false,false,GameWorld.Player().money_coupons_bind)
            end
        else
            if args[1] == 2 then  --钻石
                GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),tostring(args[5]), tostring(way),tostring(way) .. 'x' .. args[2],true,false,GameWorld.Player().money_coupons)
            else
                GameWorld.onBuyItemLog(tostring(args[2]),tostring(way),tostring(args[5]), tostring(way),tostring(way) .. 'x' .. args[2],false,false,GameWorld.Player().money_coupons_bind)
            end
        end
    elseif act_id == action_config.ITEM_DROP_ITEMS then  
        if ItemDataHelper.GetItemType(args[1]) == 2 and ItemDataHelper.GetType(args[1]) == 10 then
            local msgData = {["id"] = MessageBoxType.GiftSucc, ["value"] = args}
            GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData) 
        end
    elseif act_id == action_config.ITEM_USE_REQ then
        local itemTip =  GlobalParamsHelper.GetMSortValue(886)
        for i,item in ipairs(itemTip) do
            if item[1] == tonumber(args[1]) then
                GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(item[2]))
                return
            end
        end

        -- LoggerHelper.Error("action_config.ITEM_USE_REQ"..PrintTable:TableToStr(itemTip))  
        -- LoggerHelper.Error("args.args"..PrintTable:TableToStr(args))
    end
end

function BagManager:OpenPanelFunction(funcId,itemId)
    local funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(funcId)
    if funcOpen then
        GUIManager.ShowPanelByFunctionId(funcId,{itemId})
        return true
    else
        local condition = FunctionOpenDataHelper:GetCondition(funcId)
        local funcName = LanguageDataHelper.CreateContentWithArgs(FunctionOpenDataHelper:GetFunctionOpenName(funcId))
        local str
        if condition and condition[public_config.LIMIT_LEVEL] then
            local args = LanguageDataHelper.GetArgsTable()
            args["0"] = funcName
            args["1"] = condition[public_config.LIMIT_LEVEL][1]
            str = LanguageDataHelper.CreateContentWithArgs(58783,args)
        else
            str = funcName .. LanguageDataHelper.CreateContent(52072)
        end
        GameManager.GUIManager.ShowText(1, str)
        return false
    end
end

function BagManager:OnUseClick(curitemData,func)  
    self._itemData = curitemData
    self._itemCfg = curitemData.cfgData
    self._func = func
    local effectIdType
    if self._itemCfg.effectId then
        effectIdType = self._itemCfg.effectId[1]
    end
    --宝石开镶嵌
    if self._itemData:GetItemType() == public_config.ITEM_ITEMTYPE_GEM then
        self:OpenPanelFunction(13)
        
    --界面跳转
    elseif self._itemCfg.open_in_bag then
        local funcId = self._itemCfg.open_in_bag[1][1]
        self:OpenPanelFunction(funcId,self._itemCfg.id)
    --多选一
    elseif effectIdType == 18 then
        local data = {["id"] = MessageBoxType.SelectUse, ["value"] = self._itemData}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, data)
    --花费金钱
    elseif effectIdType == 25 then
        local args = LanguageDataHelper.GetArgsTable()
        args["0"] = self._itemCfg.effectId[3]
        args["1"] = ItemDataHelper.GetItemNameWithColor(self._itemCfg.effectId[2])
        args["2"] = self._itemCfg.effectId[4]
        args["3"] = ItemDataHelper.GetItemNameWithColor(self._itemCfg.effectId[2])
        args["4"] = ItemDataHelper.GetItemNameWithColor(self._itemCfg.id)
        local str = LanguageDataHelper.CreateContentWithArgs(81540,args)

        local itemData = {}
        itemData._itemData = self._itemData
        itemData._func = self._requestUseCb
        local data = {["id"] = MessageBoxType.FirstCharge, ["value"] = itemData}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, data)

        --GUIManager.ShowMessageBox(2,str,self._requestUseCb)
    --经验道具
    elseif effectIdType == 12 then
        local addInfo = GameWorld.Player().add_rate_info
        if addInfo then
            for i, addItem in ipairs(addInfo) do
                local addInfoData = GlobalParamsHelper.GetParamValue(833)
                for k, addItemInfo in pairs(addInfoData) do
                    if k == addItem[1] then
                        local args = LanguageDataHelper.GetArgsTable()
                        args["0"] = LanguageDataHelper.CreateContent(addItemInfo[2])
                        args["1"] = ItemDataHelper.GetItemNameWithColor(self._itemCfg.id)
                        local cnId
                        if k == self._itemCfg.effectId[2] then
                            cnId = 58775
                        else
                            cnId = 58770
                        end
                        local str = LanguageDataHelper.CreateContentWithArgs(cnId,args)
                        GUIManager.ShowMessageBox(2,str,self._requestUseCb)
                        return
                    end
                end
            end
        end
        self:RequestUseOnce()
    --背包开格子
    elseif effectIdType == 33 then
        local bagCost = GlobalParamsHelper.GetParamValue(23)
        local onceCount = bagCost[self._itemCfg.id]
        if onceCount then
            local openNum = math.max(math.floor(self._itemData:GetCount()/onceCount),1)
            local data = {["id"]=MessageBoxType.OpenBag,["value"]={["openCount"] = openNum,["pkgType"] = public_config.PKG_TYPE_ITEM}}
            GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
        end
    --发送请求让后端使用
    else
        local noBatch = self._itemCfg.no_batch_use
        if self._itemData:GetCount() == 1 then
            self:RequestUseOnce()
        elseif self._itemData:GetCount() > 1 then
            if noBatch > 0 then
                self:RequestUseOnce()
            else
                self:OnClickForBulkUse()
            end
        end
    end
    --调用回调
    if self._func then
        self._func()
    end
end

function BagManager:RequestUseOnce()
    BagManager:RequestUse(self._itemData.bagPos, 1, 0)
    GUIManager.ClosePanel(PanelsConfig.ItemTips)
end

--批量使用物品
function BagManager:OnClickForBulkUse()
    local data = {["id"] = MessageBoxType.BulkUse, ["value"] = self._itemData}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, data, nil)
end

function BagManager:GetCommonItemCount(itemId)
    return bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
end

BagManager:Init()
return BagManager
