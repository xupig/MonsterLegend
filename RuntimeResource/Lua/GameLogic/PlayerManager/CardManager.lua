local CardManager = {}
local CardDataHelper = GameDataHelper.CardDataHelper
local CardLevelDataHelper = GameDataHelper.CardLevelDataHelper
local CardGroupDataHelper = GameDataHelper.CardGroupDataHelper
local CardGroupLevelDataHelper = GameDataHelper.CardGroupLevelDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local CardData = PlayerManager.PlayerDataManager.cardData
local action_config = GameWorld.action_config
local FloatTextManager = PlayerManager.FloatTextManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local RedPointConfig = GameConfig.RedPointConfig
local RedPointManager = GameManager.RedPointManager

function CardManager:Init()  
    self._isUp = false
    self._grouplevel = {}
    self._groupid = {}
    self._cardid = {}
    self._oldPkg = {}
    self._ishandle = {}
    self._updateGroupRedPoint = function()self:RefreshCardGroupEvent()end
    self._updateTotalRedPoint = function()self:RefreshCardTotalEvent()end
    self._openRuneEven = function(id)self:RefrenCardRed(id)end
end
-- 图鉴总览
function CardManager:RefreshCardTotalRedPointStatus()
    local f = self:RefreshTabsRedPointState()
    self:RefrenCardStrength(f)
    return f
end
-- 图鉴组合
function CardManager:RefreshCardGroupRedPointStatus()
    return self:RefreshGroupRedPointState()
end

function CardManager:RefreshCardGroupEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_PICTURE_COMBINATION, self:RefreshCardGroupRedPointStatus())        
end

function CardManager:RefreshCardTotalEvent()
    if self._isUp then--正在升级不更红点。
        return 
    end
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_PICTURE_VIEW, self:RefreshCardTotalRedPointStatus())        
end

-- 更新tabs列表红点,(总的)
function CardManager:RefreshTabsRedPointState()
    local data = CardDataHelper:GetCarSubTypes()
    for t,subtypes in pairs(data) do
        for k,v in ipairs(subtypes) do
            local r = self:RefreshSigleTabRedPointState(t,v)
            if r==true then 
                return true 
            end
        end
    end
    return false
end

-- 
function CardManager:RefreshTypeRedPointState(curt)
    local data = CardDataHelper:GetCarSubTypes()
    local subtypes = data[curt]
    if subtypes then
        for k,v in ipairs(subtypes) do
            local r = self:RefreshSigleTabRedPointState(curt,v)
            if r==true then 
                return true 
            end
        end
    end  
    return false
end

-- 更新tabs列表红点,(单个)
function CardManager:RefreshSigleTabRedPointState(t,subtype)
    local totals = CardDataHelper:GetCardTotalSubByType(t)
    if totals and totals[subtype] then
        local ids = totals[subtype]
        for k,id in ipairs(ids) do
            local star = self:CardStar(id)
            if not self:IsCardLock(id) and CardLevelDataHelper:GetCost(id) then
                local r = self:CardTotalRed(id,star)[1]
                if r==true then 
                    return true 
                end
            end
        end
    end
    return false
end


-- 获得card的star
function CardManager:CardStar(cardid)
    local cardlist = GameWorld.Player().card_list
    for k,v in pairs(cardlist) do
        if cardid == tonumber(k) then
            return v
        end
    end
    return 0
end

-- 更新组合红点
function CardManager:RefreshGroupRedPointState()
    local groupids = CardGroupDataHelper:GetAllId()
    for k,groupid in ipairs(groupids) do
        local les = CardData:GetGroupLes(groupid)
        for k,le in ipairs(les) do            
            if not self:IsActGroup(groupid,le) and self:GetGroupRed(groupid,le)==true then           
                return true 
            end
        end
    end
    return false
end

function CardManager:IsCardLock(id)
    local l = CardDataHelper:GetLevel(id)
    if l>GameWorld.Player().level then
        return true
    else
        return false
    end
end

function CardManager:GetPiece(id)
    local cardbag = GameWorld.Player().pkg_card_piece
    local i=0
    for k,v in pairs(cardbag) do
        if id == v[1] then
            local a = v[2] or 1
            i = i + a
        end
    end
    return i
end

function CardManager:IsHave(id)
    local cardlist = GameWorld.Player().card_list
    if cardlist[id] then
        return true,cardlist[id]
    end
    return false,0
end

function CardManager:IsActGroup(groupid,level)
    local cardgroup = GameWorld.Player().card_group
    if cardgroup[groupid] and cardgroup[groupid]>=level then
        return true
    end
    return false
end

function CardManager:GetStarNeed(groupid,level)
    local i=self:GetGroupid(groupid,level)
    return CardGroupLevelDataHelper:GetStarNeed(i)
end

function CardManager:GetCardCost(id,star)
    return CardLevelDataHelper:GetCardCost(id,star)
end

function CardManager:GetCarAtt(id,star)
    local ids = CardLevelDataHelper:GetAllId()
    local data = {}
    for k,v in ipairs(ids) do
        if v ~=0 and id == CardLevelDataHelper:GetCardId(v) and star == CardLevelDataHelper:GetStar(v) then
            local atts = CardLevelDataHelper:GetAttri(v)
            for attid,num in pairs(atts) do 
                local i = tonumber(attid)
                local content = AttriDataHelper:GetDesc(i)
                local type = AttriDataHelper:GetShowType(i)--1:直接读,否则%
                table.insert(data,{i,num})
            end
            return data
        end
    end
end

function CardManager:GetGroupAtt(groupid,level)
    local i=self:GetGroupid(groupid,level)
    return CardGroupLevelDataHelper:GetAttri(i)
end

function CardManager:GetGroupRed(groupid,level)
    local ids = CardGroupDataHelper:GetCardId(groupid)
    local star = self:GetStarNeed(groupid,level)
    return self:CanAct(ids,star)
end

function CardManager:GetPieceNum(id,data)
	local i=0
	for k,v in pairs(data) do 
		if v == id then
			i=i+1
		end
	end
	return i
end

function CardManager:ishanle(id)
   for k,v in pairs(self._ishandle) do
        if id==v then
            return true
        end
   end
   table.insert(self._ishandle,id)
   return false
end



function CardManager:RefrenCardStrength(state)
	self._carActiveUpStrengNode:CheckStrengthen(state)
end

function CardManager:OnPlayerEnterWorld()
    self._carActiveUpStrengNode = RedPointManager:CreateStrengthenNodeByFunId(RedPointConfig.CARD_RED, public_config.FUNCTION_ID_PICTURE_VIEW)
    self:RefreshCardGroupEvent()
    self:RefreshCardTotalEvent()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Card_Piece_Red_Point_State,self._updateGroupRedPoint)
    EventDispatcher:AddEventListener(GameEvents.Refresh_Card_Piece_Red_Point_State,self._updateTotalRedPoint)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CARD_GROUP, self._updateGroupRedPoint) 
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._updateTotalRedPoint)
end

function CardManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Card_Piece_Red_Point_State,self._updateGroupRedPoint)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Card_Piece_Red_Point_State,self._updateTotalRedPoint)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CARD_GROUP, self._updateGroupRedPoint)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._updateTotalRedPoint)
end

function CardManager:CardTotalRed(id,star)
    if CardLevelDataHelper:isFullLevel(id,star) then
        return {false,0}
    end
    local cost = self:GetCardCost(id,star)
    local costid=0
    local num=0
    costid,num = next(cost)
    -- local i=0
    -- local cardbag = GameWorld.Player().pkg_card_piece
    -- for k,id in pairs(cardbag) do 
    --     if id[1]==costid then
    --         local v = id[2] or 1
    --         i = i + v
    --     end
    -- end
    local i = GameWorld.Player().pkg_card_piece.money_card_exp or 0
    if costid==nil or num==nil then
        return {false,i}
    end
    if i>0 and i>=num then
        return {true,i}
    end
    return {false,i}
end

function CardManager:GetGroupsAtt()
    local ids = CardDataHelper:GetAllId()
    local groupatt={}
    for k, id in ipairs(ids) do
        if id~=0 then
            local les = self:GetGroupLe(id)
            for k,le in ipairs(les) do
                local group = id
                local level = le
                local f = self:IsActGroup(group,level)--已经激活
                if f then
                    local i=self:GetGroupid(group,level)
                    local atts = CardGroupLevelDataHelper:GetAttri(i)
                    for k,v in pairs(atts) do 
                        if groupatt[k]==nil then
                            groupatt[k]=0
                        end
                        groupatt[k]=v+groupatt[k]
                    end              
                end
            end
        end
    end
    return groupatt
end

function CardManager:IsFullGroup(group,level)
    local les = self:GetGroupLe(group)
    for i in pairs(les) do
        if les[i]==level then
            return false
        end
    end
    return true
end
function CardManager:GetCardAttlist()
    local cardbag = GameWorld.Player().card_list
    local cardatt={}
    for cardid, level in pairs(cardbag) do
        if cardid~=0 then 
            local i = self:GetCardid(tonumber(cardid),level)
            local atts = CardLevelDataHelper:GetAttri(i)
            for k,v in pairs(atts) do 
                if cardatt[k]==nil then
                    cardatt[k]=0
                end
                cardatt[k]=v+cardatt[k]
            end
        end
    end
    return cardatt
end
function CardManager:GetGroupsAttDes()
    local groupatts = self:GetGroupsAtt()
    local d = self:GetCardsAtt(groupatts)
    return self:DiveAtt(d)
end

function CardManager:GetCardsAttDes()
    local cardatts = self:GetCardAttlist()
    local d = self:GetCardsAtt(cardatts)
    return self:DiveAtt(d)
end


function CardManager:DiveAtt(d)
    local data = {}
    local len=2
    local j=1
    local temp={}
    for i=1,#d do
        if j==len then
            if d[i] then
                table.insert(temp,d[i])
                j=1
            end
            table.insert(data,temp)
            temp={}
        else
            j=j+1
            table.insert(temp,d[i])
        end
    end
    if not table.isEmpty(temp) then
        table.insert(data,temp)
    end
    return data
end

function CardManager:GetBagPiece()
    return bagData:GetPkg(public_config.PKG_TYPE_CARD_PIECE):GetItemInfos() or {}
     
end

function CardManager:GetBagPieceByQ(quality)
    local d = self:GetBagPiece()
    local items={}
    for _,item in pairs(d) do
        if item ~=-1 then
            if quality==ItemDataHelper.GetQuality(item.cfg_id) then 
                table.insert(items,item)
            end
        end
    end
    for i=#items+1,200 do
        table.insert(items,i,-1)
    end
    return items
end
function CardManager:GetAllBagPiece()
    local items = self:GetBagPiece()
    local d={}
    for k,v in pairs(items) do
        table.insert(d,v)
    end
    for i=#d+1,200 do
        d[i]=-1
    end
    return d
end
function CardManager:GetBagDrop()
    local d= {}
    table.insert(d,{75518,3})
    table.insert(d,{75519,4})
    table.insert(d,{75520,5})
    table.insert(d,{75521,6})
    table.insert(d,{75522,7})
    table.insert(d,{75523,0})
    return d
end
function CardManager:GetGroupid(groupid,le)
    if self._groupid[groupid] and self._groupid[groupid][le] then
        return self._groupid[groupid][le]
    end
    if self._groupid[groupid]== nil then
        self._groupid[groupid]={}
    end
    self._groupid[groupid][le]={}
    local ids = CardGroupLevelDataHelper:GetAllId()
    for k, id in ipairs(ids) do
        if groupid==CardGroupLevelDataHelper:GetCardGroup(id) and le == CardGroupLevelDataHelper:GetLevel(id)then
            self._groupid[groupid][le]=id
        end
    end
    return self._groupid[groupid][le]
end

function CardManager:GetGroupLe(groupid)
    if self._grouplevel[groupid]  then
        return self._grouplevel[groupid]
    end
    self._grouplevel[groupid]={}
    local ids = CardGroupLevelDataHelper:GetAllId()
    for k, id in ipairs(ids) do
        if groupid==CardGroupLevelDataHelper:GetCardGroup(id) then
            table.insert(self._grouplevel[groupid],CardGroupLevelDataHelper:GetLevel(id))
        end
    end
    return self._grouplevel[groupid]
end

function CardManager:CanAct(cardids,star)
    for k,id in pairs(cardids) do
        local f,v=self:IsHave(id,star)    
        if not f or v< star then
            return false
        end
    end
    return true
end

function CardManager:GetCardid(groupid,le)
    if self._cardid[groupid] and self._cardid[groupid][le] then
        return self._cardid[groupid][le]
    end
    if self._cardid[groupid]== nil then
        self._cardid[groupid]={}
    end
    self._cardid[groupid][le]={}
    local ids = CardLevelDataHelper:GetAllId()
    for k, id in ipairs(ids) do
        if groupid==CardLevelDataHelper:GetCardId(id) and le == CardLevelDataHelper:GetStar(id)then
            self._cardid[groupid][le]=id
        end
    end
    return self._cardid[groupid][le]
end

-- REQ
function CardManager:CardUpLevel(arg)
    GameWorld.Player().server.card_action_req(action_config.CARD_UPLEVEL_REQ,arg)
end

function CardManager:CardGroupUpLevel(arg)
    GameWorld.Player().server.card_action_req(action_config.CARD_GROUP_UPLEVEL_REQ,arg)
end
--图鉴分解,参数 位置,位置,位置....
function CardManager:CardResolve(arg)
    GameWorld.Player().server.card_action_req(action_config.CARD_RESOLVE_REQ,arg)
end

function CardManager:GetAttInfo(attid,num)
    local content = AttriDataHelper:GetName(attid)
    local type = AttriDataHelper:GetShowType(attid)--1:直接读,否则%
    return {content,num,type}
end

function CardManager:GetCardsUpAtt(atts)
    local data={}
    for k,d in pairs(atts) do
        local a = self:GetAttInfo(d[1],d[2]) 
        table.insert(data,a)
    end
    return data
end

function CardManager:GetCardsAtt(atts)
    local data={}
    for attid,num in pairs(atts) do
        local a = self:GetAttInfo(tonumber(attid),num) 
        table.insert(data,a)
    end
    return data
end


function CardManager:HandleData(action_id, error_code, args)
    if error_code ~= 0 then
        -- return
    end
    if action_id == action_config.CARD_UPLEVEL_REQ then
        EventDispatcher:TriggerEvent(GameEvents.CardUpdate)
    elseif action_id == action_config.CARD_GROUP_UPLEVEL_REQ then
        EventDispatcher:TriggerEvent(GameEvents.CardGroupdate)
    elseif action_id == action_config.CARD_RESOLVE_REQ then--分解图鉴碎片
        
    end
end

function CardManager:SetUpIng(state)
  self._isUp = state
end

CardManager:Init() 
return CardManager