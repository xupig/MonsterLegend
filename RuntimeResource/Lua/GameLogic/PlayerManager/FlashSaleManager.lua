local FlashSaleManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local FlashSaleDataHelper = GameDataHelper.FlashSaleDataHelper

function FlashSaleManager:Init()
    self.openFlashSalePanelFunc_1 = function() GUIManager.ShowPanel(PanelsConfig.FlashSale, 1) end
    self.openFlashSalePanelFunc_2 = function() GUIManager.ShowPanel(PanelsConfig.FlashSale, 2) end
    self.openFlashSalePanelFunc_3 = function() GUIManager.ShowPanel(PanelsConfig.FlashSale, 3) end
    self.refreshFlashSaleData = function() self:RefreshFlashSaleData() end
    self._updateCountDown = function() self:UpdateCountDown() end
end

function FlashSaleManager:OnPlayerEnterWorld()
    self.isOpenState = {}
    if FlashSaleManager:CanShowFlashSale(1) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_1,EnumType.RemindType.LimitActivity)
        self.isOpenState[1] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivity)
        self.isOpenState[1] = 0
    end
    if FlashSaleManager:CanShowFlashSale(2) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_2,EnumType.RemindType.LimitActivityM)
        self.isOpenState[2] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivityM)
        self.isOpenState[2] = 0
    end
    if FlashSaleManager:CanShowFlashSale(3) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_3,EnumType.RemindType.LimitActivityH)
        self.isOpenState[3] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivityH)
        self.isOpenState[3] = 0
    end

    EventDispatcher:AddEventListener(GameEvents.Refresh_Flash_Sale_Data, self.refreshFlashSaleData)

    self._timer = TimerHeap:AddSecTimer(0, 1, 0, self._updateCountDown)
end

function FlashSaleManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Flash_Sale_Data, self.refreshFlashSaleData)

    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function FlashSaleManager:HandleData(action_id,error_id,args)
    --LoggerHelper.Log("FlashSaleManager:HandleData "..action_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    elseif action_id == action_config.LEVELED_UP_LIMITED_TIME_SALE_PURCHASE_REQ then
        
    end
end

function FlashSaleManager:UpdateCountDown()
    for i=1,3 do
        if self.isOpenState[i] == 1 and (not self:CanShowFlashSale(i)) then
            EventDispatcher:TriggerEvent(GameEvents.LimitActivityRemoveRemind)
            self.isOpenState[i] = 0
        end
    end
end

-------------------------------------数据处理------------------------------------
function FlashSaleManager:RefreshFlashSaleData()
    if FlashSaleManager:CanShowFlashSale(1) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_1,EnumType.RemindType.LimitActivity)
        self.isOpenState[1] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivity)
        self.isOpenState[1] = 0
    end

    if FlashSaleManager:CanShowFlashSale(2) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_2,EnumType.RemindType.LimitActivityM)
        self.isOpenState[2] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivityM)
        self.isOpenState[2] = 0
    end

    if FlashSaleManager:CanShowFlashSale(3) then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.openFlashSalePanelFunc_3,EnumType.RemindType.LimitActivityH)
        self.isOpenState[3] = 1
    else
        EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.LimitActivityH)
        self.isOpenState[3] = 0
    end
end

-------------------------------------功能接口------------------------------------
function FlashSaleManager:CanShowFlashSale(id)
    local data = {}
    if GameWorld.Player().level_up_sale_info ~= nil then
        data = GameWorld.Player().level_up_sale_info[id] or {}
    end
    if type(data) ~= "table" then
        return false
    end
    if data[public_config.LEVEL_UP_SALE_STEP] == nil or self:IsTimeOut(id) or data[public_config.LEVEL_UP_SALE_PRESENT_IS_EXPIRED] == 1 or
        (data[public_config.LEVEL_UP_SALE_NORMAL] == 1 and data[public_config.LEVEL_UP_SALE_SUPREME] == 1) then
        return false
    end
    return true
end

function FlashSaleManager:IsTimeOut(id)
    local data = {}
    if GameWorld.Player().level_up_sale_info ~= nil then
        data = GameWorld.Player().level_up_sale_info[id] or {}
    end
    if type(data) ~= "table" then
        return true
    end
    local endTime = data[public_config.LEVEL_UP_SALE_TIME_STAMP] + FlashSaleDataHelper.GetTime(id)
    local now = DateTimeUtil.GetServerTime()
    if endTime >= now then
        return false
    end
    return true
end

-------------------------------------客户端请求----------------------------------
function FlashSaleManager:FlashSaleBuyReq(id, type)
    local params = string.format("%d,%d", id, type)
    GameWorld.Player().server.level_up_sale_action_req(action_config.LEVELED_UP_LIMITED_TIME_SALE_PURCHASE_REQ, params)
end

FlashSaleManager:Init()
return FlashSaleManager