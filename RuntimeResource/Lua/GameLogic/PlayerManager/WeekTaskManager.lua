local WeekTaskManager = {}

local error_code = GameWorld.error_code
local action_config = GameWorld.action_config

local weekTaskData = PlayerManager.PlayerDataManager.weekTaskData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local CircleTaskRewardDataHelper = GameDataHelper.CircleTaskRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local public_config = GameWorld.public_config
local TaskConfig = GameConfig.TaskConfig
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager
local GuildWeekTaskRewardDataHelper = GameDataHelper.GuildWeekTaskRewardDataHelper
local GuildManager = PlayerManager.GuildManager

require "PlayerManager/PlayerData/WeekTaskData"
local WeekTaskData = ClassTypes.WeekTaskData

require "PlayerManager/PlayerData/TaskData"
local TracingPointData = ClassTypes.TracingPointData

function WeekTaskManager:Init()
    self._initList = false
    self:InitCallbackFunc()
end

function WeekTaskManager:InitCallbackFunc()
    self._onAcceptWeekTask = function(id)self:OnAcceptWeekTask(id) end
    self._onCommitWeekTask = function(id)self:OnCommitWeekTask(id) end
    self._onDailyToWeekTask = function()self:RunWeekTask(true) end
end

function WeekTaskManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ACCEPT_WEEK_TASK, self._onAcceptWeekTask)
    EventDispatcher:AddEventListener(GameEvents.COMMIT_WEEK_TASK, self._onCommitWeekTask)
    EventDispatcher:AddEventListener(GameEvents.Daily_Week_Task, self._onDailyToWeekTask)
end

function WeekTaskManager:OnPlayerLeaveWorld()
    self._initList = false
    EventDispatcher:RemoveEventListener(GameEvents.ACCEPT_WEEK_TASK, self._onAcceptWeekTask)
    EventDispatcher:RemoveEventListener(GameEvents.COMMIT_WEEK_TASK, self._onCommitWeekTask)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_Week_Task, self._onDailyToWeekTask)
end

function WeekTaskManager:ClearTaskData()
    self._completeTask = false
    self._acceptTask = true
    local status = public_config.TASK_STATE_ACCEPTABLE
    self._weekTaskData = WeekTaskData:NewWeekTaskData(nil, status)
end

function WeekTaskManager:OnCommitWeekTask()
    self:CommitTask()
end

function WeekTaskManager:OnAcceptWeekTask(id)
    self:AcceptTask()
end

function WeekTaskManager:HandleAutoTask()
    if GuildManager:IsInGuild() then
        self:RunWeekTask()
    end
end

function WeekTaskManager:HandleData(action_id, error_id, args)
    if error_id > error_code.ERR_SUCCESSFUL then
        return
    end

    if action_id == action_config.WEEK_TASK_LIST_REQ then--8601, --全部任务列表，服务器推送
        -- LoggerHelper.Error("8601 周任务列表信息====" .. PrintTable:TableToStr(args))   
        self:RefreshTaskInfo(args)     
        if not self._initList then
            EventDispatcher:TriggerEvent(GameEvents.WEEK_TASK_LIST)
            self._initList = true
        end
    elseif action_id == action_config.WEEK_TASK_REFRESH then--8602, --更新任务状态，服务器推送
        --{"5" = {"1" = {"20004" = 0},"2" = 1}}  {"5" = {"1" = {},"2" = 2}}
        -- id = 1 = {targetId = 完成次数}, 2 = 状态}
        -- 1进行中，有进度的；2已经完成，没了进度信息，要读表； 3已经领取奖励，没了进度信息，要读表
        -- LoggerHelper.Error("Sam: 8602 更新更新 周任务列表信息====" .. PrintTable:TableToStr(args))
        self:RefreshTaskInfo(args)
    elseif action_id == action_config.WEEK_TASK_ACCEPT_REQ then--8603, --接任务，随机一个任务来接。前提是现在没有已接任务。
        -- LoggerHelper.Error("Sam: 8603 接任务====" .. PrintTable:TableToStr(args))
        GUIManager.ShowPanel(PanelsConfig.CircleTaskPanel)
    elseif action_id == action_config.WEEK_TASK_COMMIT_REQ then--8604, --提交任务。领取奖励
        -- LoggerHelper.Error("Sam: 8604 提交任务。领取奖励====" .. PrintTable:TableToStr(args))
        self:CheckIsCompleteAll()
    elseif action_id == action_config.WEEK_TASK_QUICK_COMPLETE then--8605, --快速完成，包括提交
        self:CheckIsCompleteAll()
    elseif action_id == action_config.WEEK_TASK_ONE_CLICK then--8606, --一键完成一环任务中剩下的任务
        -- LoggerHelper.Error("Sam:8606 一键完成一环任务中剩下的任务====" .. PrintTable:TableToStr(args))
        self:CheckIsCompleteAll()
    end
end

function WeekTaskManager:CheckIsCompleteAll()
    -- LoggerHelper.Error("WeekTaskManager:CheckIsCompleteAll() completeNum =" .. GameWorld.Player().week_task_cur_week_count)
    local completeNum = GameWorld.Player().week_task_cur_week_count

    if completeNum % GlobalParamsHelper.GetParamValue(637) == 0 then
        local rewards = GuildWeekTaskRewardDataHelper:GetWeekRewardByLevel()
        -- local datas = {}
        -- for i,v in ipairs(rewards) do
        --     local data = {}
        --     data[1] = v[1]
        --     data[2] = v[2]
        --     table.insert(datas, data)
        -- end

        local value = {
            ["text"] = LanguageDataHelper.CreateContent(53245, {["0"] = GlobalParamsHelper.GetParamValue(637)}),
            ["seconds"] = 5,
            ["rewards"] = rewards
        }
        local msgData ={["id"] = MessageBoxType.RewardCountDowenTip,["value"]=value}
        GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
    end

    if completeNum == GlobalParamsHelper.GetParamValue(636) then
        local curAutoTask = TaskCommonManager:GetCurAutoTask()
        if curAutoTask and curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
            PlayerActionManager:StopCurrAction()
        end
    end
end

function WeekTaskManager:RefreshTaskInfo(args)    
    --LoggerHelper.Error("WeekTaskManager:RefreshTaskInfo(args)    ")
    local completeNum = GameWorld.Player().week_task_cur_week_count
    self._completeTask = completeNum == GlobalParamsHelper.GetParamValue(636)
    local oldId = self._weekTaskData and self._weekTaskData:GetId() or nil
    local datas = {}

    if table.isEmpty(args) then
        self._acceptTask = not self._completeTask
        local status = public_config.TASK_STATE_ACCEPTABLE
        if self._completeTask then
            status = public_config.TASK_STATE_COMPLETED
        end

        if oldId and self._initList then
            local data = {}
            data[TaskConfig.TASK_CHANGE_INFO_ID] = oldId
            data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_WEEK_TASK
            data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
            table.insert(datas, data)
            PlayerManager.TaskManager:UpdateTasksForMainMenu(datas)
        end
        self._weekTaskData = WeekTaskData:NewWeekTaskData(nil, status)        
    else
        self._acceptTask = false
        for weekTaskId, serverData in pairs(args) do
            weekTaskData = WeekTaskData()
            weekTaskData:UpdateData(weekTaskId, serverData)            
            if weekTaskData:IsSameTask(self._weekTaskData) then
                return
            end
            if self._initList then
                if oldId == nil then
                    --新增
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = weekTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_WEEK_TASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = weekTaskData
                    table.insert(datas, data)
                elseif oldId == weekTaskId then
                    --更新
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = weekTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_WEEK_TASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = weekTaskData
                    table.insert(datas, data)
                elseif oldId ~= weekTaskId then
                    --删除,新增
                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = oldId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_WEEK_TASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
                    table.insert(datas, data)

                    local data = {}
                    data[TaskConfig.TASK_CHANGE_INFO_ID] = weekTaskId
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_WEEK_TASK
                    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
                    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = weekTaskData
                    table.insert(datas, data)
                end 
                PlayerManager.TaskManager:UpdateTasksForMainMenu(datas)
            end
            local flag = self._weekTaskData == nil
            self._weekTaskData = weekTaskData
            local task = TaskCommonManager:GetCurAutoTask()
            if not flag and task ~= nil and task:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
                TaskCommonManager:SetCurAutoTask(self._weekTaskData, TaskCommonManager:GetNextEvent(self._weekTaskData))
                TaskCommonManager:UpdateAutoTask()
            end
            break
        end
    end
    
    EventDispatcher:TriggerEvent(GameEvents.WEEK_TASK_REFRESH)
    --LoggerHelper.Error("任务刷新")
end

function WeekTaskManager:GetWeekTask()
    if self._acceptTask or self._completeTask or self._weekTaskData == nil   then
        return nil
    end
    return self._weekTaskData
end

function WeekTaskManager:RunWeekTask(manualHandleTask)
    if self._completeTask then return end
    local eventItemData = TaskCommonManager:GetNextEvent(self._weekTaskData)
    if eventItemData:GetEventID() == public_config.EVENT_ID_GIVE_ITEM_TO_NPC then
        TaskCommonManager:SetCurAutoTask(nil, nil)
        return
    end
    local trackTask = TracingPointData(self._weekTaskData, eventItemData)
    if manualHandleTask then
        TaskCommonManager:OnManualHandleTask(trackTask)    
    else
        TaskCommonManager:OnClickTrackCurTask(trackTask)    
    end
end

function WeekTaskManager:IsCompleteWeekTask()
    return self._completeTask
end

function WeekTaskManager:NeedAcceptTask()
    return self._acceptTask
end

function WeekTaskManager:GetWeekTaskData()
    return self._weekTaskData
end

function WeekTaskManager:GetTaskMenuData()
    local datas = {}
    local wtData = {}
    table.insert(wtData, {["text"] = LanguageDataHelper.CreateContent(926)})
    local tsk = self:GetWeekTaskData()
    if tsk ~= nil then
        local eventItemData = nil
        if not self._completeTask then
            eventItemData = TaskCommonManager:GetNextEvent(tsk)
        end
        table.insert(wtData, TracingPointData(tsk, eventItemData))
    end
    table.insert(datas, wtData)
    -- LoggerHelper.Error("数据====" .. PrintTable:TableToStr(datas))
    return datas
end

--获取日常任务的itemdata和eventdata
function WeekTaskManager:GetCircleTaskInfo(npcID)
    local circleTask, eventItemData
    if self._weekTaskData ~= nil and (not self._completeTask) and (not self._acceptTask) then
        local state = self._weekTaskData:GetState()
        if state == public_config.TASK_STATE_ACCEPTABLE and self._weekTaskData:GetAcceptNPC() == npcID then
            circleTask = self._weekTaskData
            eventItemData = TaskCommonManager:GetDialogEvent(self._weekTaskData, npcID)
        elseif state == public_config.TASK_STATE_COMPLETED and self._weekTaskData:GetCommitNPC() == npcID then
            circleTask = self._weekTaskData
            eventItemData = TaskCommonManager:GetDialogEvent(self._weekTaskData, npcID)
        end
    end
    return circleTask, eventItemData
end

--8603, --接任务，随机一个任务来接。前提是现在没有已接任务。
function WeekTaskManager:AcceptTask()
    GameWorld.Player().server.week_task_action_req(action_config.WEEK_TASK_ACCEPT_REQ, "")
end

--8604, --提交任务。领取奖励
function WeekTaskManager:CommitTask()
    -- LoggerHelper.Log("Sam: WeekTaskManager:CommitTask()")
    GameWorld.Player().server.week_task_action_req(action_config.WEEK_TASK_COMMIT_REQ, "")
end

--8605, --快速完成，包括提交
function WeekTaskManager:FastCommitTask()
    GameWorld.Player().server.week_task_action_req(action_config.WEEK_TASK_QUICK_COMPLETE, "")
end

--8606, --一键完成一环任务中剩下的任务
function WeekTaskManager:FastCommitCircleTask()
    GameWorld.Player().server.week_task_action_req(action_config.WEEK_TASK_ONE_CLICK, "")
end

--提交装备给系统
function WeekTaskManager:CommitEquip(pos, num)
    GameWorld.Player().server.give_item_to_npc_req_by_pos(tostring(pos) .. "," .. num)
end



WeekTaskManager:Init()
return WeekTaskManager