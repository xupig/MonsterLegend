local CardLuckManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local error_code = GameWorld.error_code
local ChatConfig = GameConfig.ChatConfig
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ChatManager = PlayerManager.ChatManager
local RedPointManager = GameManager.RedPointManager
local FontStyleHelper = GameDataHelper.FontStyleHelper
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local TipsManager = GameManager.TipsManager
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function CardLuckManager:Init()
    self._channelChange = function()self:OnActivityTimeChange() end
    self._checkLuckOpenCb = function (id)
    	if id == public_config.FUNCTION_ID_CARD_LUCK then
    		self:OnActivityTimeChange()
    	end
    end

    self._openFun = function() GUIManager.ShowPanel(PanelsConfig.CardLuck) end

    
end

function CardLuckManager:OnPlayerEnterWorld()
    self._funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_CARD_LUCK)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkLuckOpenCb)
    EventDispatcher:AddEventListener(GameEvents.UpdateLuckCard,self._channelChange)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock,self._channelChange)
    --GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._channelChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CHANNEL_ACTIVITY_TIMES, self._channelChange)
    self:OnActivityTimeChange()
  
    --self:SendOpenCardReq(3)
end

-- function CardLuckManager:OnActivityTimeChange()
--     local channelTime =  self:GetChannelTime()
--     local leftTime = self:GetLeftTime()
--     local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_CARD_LUCK)
--     for k,v in pairs(channelTime) do
--         if k == public_config.CHANNEL_ACTIVITY_ID_OPEN_CARD and isOpen and self:GetLeftTime()>0 then
--             EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.CardLuck, self._openFun,
--             tonumber(leftTime)+DateTimeUtil.GetServerTime())
--             return
--         end
--     end
--     EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.CardLuck)
-- end

function CardLuckManager:OnActivityTimeChange()
	    self._showMenuIcon = false
    	local now = DateTimeUtil.GetServerTime()
		local activityTimes = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_OPEN_CARD]
		if activityTimes then
			local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
			local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
			--活动还没开始
			if now < startTime then
				if self._firstActivityOpenTime == nil or self._firstActivityOpenTime > startTime then
					self._firstActivityOpenTime = startTime
				end
			end
			--活动已开始判断
			local b = endTime > now and now >= startTime

			if b then
				--LoggerHelper.Error("OnActivityTimeChange")
				self._showMenuIcon = b
				if self._lastActivityEndTime == nil or self._lastActivityEndTime < endTime then
					self._lastActivityEndTime = endTime
				end
			end
		else

		end
	
	--要显示主菜单icon
	if self._showMenuIcon then
		if self._funcOpen then
			self:ShowMenu()
			self:CheckEnd()
		end
	--未开启
	else
		self:CheckFirstActivityOpen()
		self:RemoveMenuIcon()
	end
end

--检查面板是否开启
function CardLuckManager:CheckOpen()
	return FunctionOpenDataHelper:CheckPanelOpen(402)
end

function CardLuckManager:ShowMenu()
    local leftTime = self:GetLeftTime()
	EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.CardLuck,self._openFun,tonumber(leftTime)+DateTimeUtil.GetServerTime())
end

--检查是否需要计时开启主界面图标
function CardLuckManager:CheckFirstActivityOpen()
	if self._firstActivityOpenTime == nil then
		return
	end
	
	local now = DateTimeUtil.GetServerTime()
	local timeLeft = self._firstActivityOpenTime - now
	--LoggerHelper.Error("endTimeLeft："..endTimeLeft)
	if timeLeft > 0 and timeLeft < 86400 then
		--先删除旧的计时
		if self._firstOpenTimer then
			TimerHeap:DelTimer(self._firstActivityOpenTimer)
			self._firstOpenTimer = nil
		end
		self._firstOpenTimer =  TimerHeap:AddSecTimer(timeLeft, 0, timeLeft, function ()
			TimerHeap.DelTimer(self._firstOpenTimer)
			self:OnActivityTimeChange()
		end)
	end
end

--检查是否关闭主界面图标
function CardLuckManager:CheckEnd()
	if self._lastActivityEndTime == nil then
		return
	end
	--先删除旧的计时
	local now = DateTimeUtil.GetServerTime()
	local endTimeLeft = self._lastActivityEndTime - now
	--LoggerHelper.Error("endTimeLeft："..endTimeLeft)
	if endTimeLeft > 0 and endTimeLeft < 86400 then
		if self._endTimer then
			TimerHeap:DelTimer(self._endTimer)
			self._endTimer = nil
		end
		self._endTimer =  TimerHeap:AddSecTimer(endTimeLeft, 0, endTimeLeft, function ()
			self:RemoveMenuIcon()
		end)
	end
end

--关闭主界面图标
function CardLuckManager:RemoveMenuIcon()
	if self._endTimer then
		TimerHeap:DelTimer(self._endTimer)
		self._endTimer = nil
	end
	EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.CardLuck)
end

function CardLuckManager:GetLeftTime()
    local channelTime =  self:GetChannelTime()
    local time = 0
    if channelTime and channelTime[public_config.CHANNEL_ACTIVITY_ID_OPEN_CARD]then
        time = channelTime[public_config.CHANNEL_ACTIVITY_ID_OPEN_CARD][public_config.CHANNEL_ACTIVITY_KEY_END_TIME] - DateTimeUtil.GetServerTime()
    end
    return time or 0
end

function CardLuckManager:HandleData(action_id, error_code, args)
    if error_code ~= 0 then
        return
    end
    if action_id == action_config.OPEN_CARD_REQ then
        --翻牌
        self:OpenCardResp(args)
    elseif action_id == action_config.OPEN_CARD_SUPREME_REWARD_REQ then
        --领取福字奖励
        self:GetBlessRewardResp(args)   
    end
end

function CardLuckManager:GetChannelTime()
	return  GameWorld.Player().channel_activity_times or {}
end

function CardLuckManager:OpenCardResp(args)
    EventDispatcher:TriggerEvent(GameEvents.OpenCardResp)
end

function CardLuckManager:GetBlessRewardResp(args)
    EventDispatcher:TriggerEvent(GameEvents.GetBlessRewardResp)
end

--翻牌
function CardLuckManager:SendOpenCardReq(position)
    GameWorld.Player().server.open_card_action_req(action_config.OPEN_CARD_REQ, tostring(position))
end

--领取奖励
function CardLuckManager:SendBlessRewardReq(id)
    GameWorld.Player().server.open_card_action_req(action_config.OPEN_CARD_SUPREME_REWARD_REQ, tostring(id))
end

--领取奖励
function CardLuckManager:GetRewardInfo()
    return GameWorld.Player().open_card_received_info or {}
end

--领取奖励
function CardLuckManager:GetCardInfo()
    return GameWorld.Player().open_card_info or {}
end


--领取位置
function CardLuckManager:GetPositionInfo()
    return GameWorld.Player().open_card_position_info or {}
end


function CardLuckManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.UpdateLuckCard,self._channelChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkLuckOpenCb)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock,self._channelChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CHANNEL_ACTIVITY_TIMES, self._channelChange)
    --GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._channelChange)
end



CardLuckManager:Init()
return CardLuckManager