local DropRecordManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function DropRecordManager:Init()
    self:InitCallbackFunc()
end

function DropRecordManager:InitCallbackFunc()
end

-- function DropRecordManager:OnPlayerEnterWorld()
-- end

-- function DropRecordManager:OnPlayerLeaveWorld()
-- end

function DropRecordManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("DropRecord Error:" .. error_id)
        return
    --请求本服掉落记录
    elseif event_id == action_config.DROP_RECORD_GET_LOCAL then
        -- LoggerHelper.Log("DROP_RECORD_GET_LOCAL .." .. PrintTable:TableToStr(args))
        self:OnGetLocal(args)
    --请求跨服掉落记录
    elseif event_id == action_config.DROP_RECORD_GET_CROSS then
        -- LoggerHelper.Log("DROP_RECORD_GET_CROSS .." .. PrintTable:TableToStr(args))
        self:OnGetCross(args)
    else
        LoggerHelper.Error("DropRecord event id not handle:" .. event_id)
    end
end

function DropRecordManager:OnGetLocal(args)
    EventDispatcher:TriggerEvent(GameEvents.OnDropRecordGetLocal, args)
end

function DropRecordManager:OnGetCross(args)
    EventDispatcher:TriggerEvent(GameEvents.OnDropRecordGetCross, args)
end

function DropRecordManager:GetLocal()
    -- LoggerHelper.Log("drop_record_action_req action_config.DROP_RECORD_GET_LOCAL")
    GameWorld.Player().server.drop_record_action_req(action_config.DROP_RECORD_GET_LOCAL, "")
end

function DropRecordManager:GetCross()
    -- LoggerHelper.Log("drop_record_action_req action_config.DROP_RECORD_GET_CROSS")
    GameWorld.Player().server.drop_record_action_req(action_config.DROP_RECORD_GET_CROSS, "")
end

DropRecordManager:Init()
return DropRecordManager
