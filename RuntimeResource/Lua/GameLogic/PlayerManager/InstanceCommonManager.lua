local InstanceCommonManager = {}

local GUIManager = GameManager.GUIManager
local InstanceCommonConfig = GameConfig.InstanceCommonConfig
local InstanceDataHelper = GameDataHelper.InstanceDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SceneConfig = GameConfig.SceneConfig
local InstanceManager = PlayerManager.InstanceManager
local public_config = GameWorld.public_config
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager

function InstanceCommonManager:Init()
    self._showPanelCallBack = function()self:ShowPanelCallBack() end
    self._showList = {}
    self._showing = false
    self:InitCallbackFunc()
end

function InstanceCommonManager:InitCallbackFunc()
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onNotifyKillMonsters = function(data)self:OnNotifyKillMonsters(data) end
    self._onNotifyInstanceStage = function(data)self:OnNotifyInstanceStage(data) end
    self._onNotifyInstanceEnd = function(data)self:OnNotifyInstanceEnd(data) end
end

function InstanceCommonManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.INSTANCE_NOTIFY_KILL_MONSTERS, self._onNotifyKillMonsters)
    EventDispatcher:AddEventListener(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, self._onNotifyInstanceStage)
    EventDispatcher:AddEventListener(GameEvents.INSTANCE_NOTIFY_END, self._onNotifyInstanceEnd)
end

function InstanceCommonManager:OnPlayerLeaveWorld()
    self:ClearTimer()
    self:ClearNotifyInstanceEndTimer()
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_NOTIFY_KILL_MONSTERS, self._onNotifyKillMonsters)
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_NOTIFY_ROOM_STAGE, self._onNotifyInstanceStage)
    EventDispatcher:RemoveEventListener(GameEvents.INSTANCE_NOTIFY_END, self._onNotifyInstanceEnd)
end

function InstanceCommonManager:OnNotifyInstanceEnd(data)
    GameManager.PlayerActionManager:StopAllAction()
    -- GameWorld.Player():StopMoveWithoutCallback()
    if data and data[1] then
        local mapId = data[public_config.INSTANCE_SETTLE_KEY_MAP_ID] or nil
        if mapId then
            local instanceId = InstanceDataHelper.GetInstanceIdByMapId(mapId)
            local result = data[public_config.INSTANCE_SETTLE_KEY_RESULT] == 1
            if not result then
                --副本失败时，停止自动战斗
                GameWorld.Player():StopAutoFight()
            end
            if instanceId and InstanceDataHelper.GetSubType(instanceId) == 0 then
                local id = 497
                if mapId == public_config.FAKE_WORLD_BOSS_MAP_ID then
                    id = 933
                end
                self:ShowLeaveCountDown(GlobalParamsHelper.GetParamValue(id), DateTimeUtil.GetServerTime(), function()
                    self:EndInstance(true)
                end)
            elseif instanceId and InstanceDataHelper.GetSubType(instanceId) == 1 then
                --弹出结算界面，胜利和失败都不需要显示掉落物品，显示经验和金币;副本中玩家死亡就是失败结算
                InstanceBalanceManager:ShowBalance(data)
            elseif instanceId and InstanceDataHelper.GetSubType(instanceId) == 2 then
                --不显示胜利结算界面，只显示失败结算界面
                if result then
                    --胜利
                    self:ShowLeaveCountDown(GlobalParamsHelper.GetParamValue(497), DateTimeUtil.GetServerTime(), function()
                        self:EndInstance(true)
                    end)
                else
                    --失败
                    InstanceBalanceManager:ShowBalance(data)
                end
            else
                self:EndInstance()
            end
        else
            self:EndInstance()
        end
    else
        self:EndInstance()
    end
end

function InstanceCommonManager:EndInstance(immediately)
    immediately = immediately or false
    -- LoggerHelper.Log("InstanceCommonManager:EndInstance() " .. tostring(immediately))
    if immediately then
        InstanceManager:OnInstanceExit()
    else
        self._instanceEndTimer = TimerHeap:AddSecTimer(GlobalParamsHelper.GetParamValue(497), 0, 0, function()
            self:ClearNotifyInstanceEndTimer()
            InstanceManager:OnInstanceExit()
        end)
    end
end

function InstanceCommonManager:ClearNotifyInstanceEndTimer()
    if self._instanceEndTimer ~= nil then
        TimerHeap:DelTimer(self._instanceEndTimer)
        self._instanceEndTimer = nil
    end
end

function InstanceCommonManager:OnNotifyInstanceStage(data)
    -- {}
    -- [1] = room_stage,   --房间状态
    -- [2] = start_time,   --状态开始时间
    local type = data[1]
    local startTime = tonumber(data[2])
    if type == public_config.INSTANCE_STAGE_3 then --等待光门倒计时 --同步给客户端，说明光门开始倒计时
        if public_config.INSTANCE_STAGE_3_WAIT_TIME ~= 0 then
            self:ShowBeginCountDown(public_config.INSTANCE_STAGE_3_WAIT_TIME, startTime)
        end
    elseif type == public_config.INSTANCE_STAGE_4 then --同步给客户端，说明光门关闭，开始玩法，刷怪
        -- local sceneType = MapDataHelper.GetSceneType(mapId)
        -- if sceneType == SceneConfig.SCENE_TYPE_AION_TOWER then
        GameWorld.Player():StartAutoFight()
        -- end
        local instanceTime = InstanceDataHelper.GetInstanceTime(self._instanceId)
        self:StartInstanceCountDown(instanceTime, startTime)
    end
end

function InstanceCommonManager:OnNotifyKillMonsters(data)
    if self._instanceId == 0 then return end
    if self._type ~= InstanceCommonConfig.COMMON_INSTANCE_INFO_DESC_TYPE1 then
        return
    end
    self:RefreshInstanceInfo(data)
end

function InstanceCommonManager:OnLeaveMap(mapId)
    local sceneType = MapDataHelper.GetSceneType(mapId)
    GUIManager.ClosePanel(PanelsConfig.InstanceCommon)
    if sceneType == SceneConfig.SCENE_TYPE_COMBAT then
        self._type = nil
        self._instanceId = nil
    end
    self._showList = {}
    self._showing = false
    self:StopInstanceCountDown()
    self:ClearNotifyInstanceEndTimer()
end

function InstanceCommonManager:OnEnterMap(mapId)
    self._mapId = mapId
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_COMBAT or sceneType == SceneConfig.SCENE_TYPE_MARRY_INSATNCE then --普通副本处理
        -- if GameWorld.Player().autoFight then
        --     GameWorld.Player():ChangeAutoFight()
        -- end
        InstanceManager:EnterInstance()
        self._instanceId = InstanceDataHelper.GetInstanceIdByMapId(mapId)
        if self._instanceId == 0 then return end
        if mapId ~= public_config.FAKE_WORLD_BOSS_MAP_ID then
            self:RefreshInstanceInfo(nil)
        else            
            GUIManager.ShowPanel(PanelsConfig.LeftMainUI, SceneConfig.SCENE_TYPE_WORLD_BOSS)
        end
    end
end

function InstanceCommonManager:RefreshInstanceInfo(data)
    local targetText = self:GetTargetText(data)
    local instanceName = LanguageDataHelper.CreateContent(InstanceDataHelper.GetInstanceName(self._instanceId))
    local descText = LanguageDataHelper.CreateContent(InstanceDataHelper.GetInstanceDescribe(self._instanceId))
    self:ShowInstanceInfo(targetText, descText, instanceName)
end

function InstanceCommonManager:GetTargetText(data)
    local type, params
    for k, v in pairs(InstanceDataHelper.GetTarget(self._instanceId)) do
        type = k
        params = v
    end
    self._type = type
    
    if type == InstanceCommonConfig.COMMON_INSTANCE_INFO_DESC_TYPE1 then
        -- 杀怪类型，参数：中文ID,中文ID，怪物ID，数量,怪物ID，数量……
        -- 显示格式 【击败：怪物10/20】
        local str = ""
        local chinese1 = params[1]
        local chinese2 = params[2]
        local mosterIds = {}
        local totalNums = {}
        local num = 0
        str = str .. LanguageDataHelper.CreateContent(chinese1)
        
        for i, v in ipairs(params) do
            if i > 2 then
                if (i - 2) % 2 == 1 then
                    table.insert(mosterIds, v)
                elseif (i - 2) % 2 == 0 then
                    table.insert(totalNums, v)
                    num = num + 1
                end
            end
        end
        
        for i = 1, num do
            str = str .. LanguageDataHelper.CreateContent(chinese2, {
                ["0"] = MonsterDataHelper.GetMonsterName(mosterIds[i]),
                ["1"] = data and data[mosterIds[i]] or 0,
                ["2"] = totalNums[i]
            }) .. "\n"
        end
        return str
    elseif type == InstanceCommonConfig.COMMON_INSTANCE_INFO_DESC_TYPE2 then
        local str = LanguageDataHelper.CreateContent(params[1], {
            ["0"] = data or params[2],
            ["1"] = params[2]
        })
        return str
    end
end

function InstanceCommonManager:StopBeginCountDown()
    local data = {
        ["type"] = InstanceCommonConfig.COMMON_INSTANCE_STOP_COUNTDOWN
    }
    self:ShowCommonPanel(data)
end

--显示开始副本前的倒计时
--seconds:倒计时时间
--startTime：副本开始时间（单位：秒)
--cb:回调函数
function InstanceCommonManager:ShowBeginCountDown(seconds, startTime, textId, cb)
    textId = textId or 579
    seconds = seconds or 5
    startTime = startTime or DateTimeUtil.GetServerTime()
    local data = {
        ["startTime"] = startTime,
        ["seconds"] = seconds,
        ["cb"] = cb,
        ["textId"] = textId,
        ["type"] = InstanceCommonConfig.COMMON_INSTANCE_BEGIN_COUNTDOWN
    }
    -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
    self:ShowCommonPanel(data)
end

--显示自动离开副本的倒计时
--seconds:倒计时时间
--startTime：开始时间（单位：秒)
--cb:回调函数
function InstanceCommonManager:ShowLeaveCountDown(seconds, startTime, cb)
    seconds = seconds or 10
    startTime = startTime or DateTimeUtil.GetServerTime()
    local data = {
        ["startTime"] = startTime,
        ["seconds"] = seconds,
        ["cb"] = cb,
        ["type"] = InstanceCommonConfig.COMMON_INSTANCE_LEAVE_COUNTDOWN
    }
    self:ShowCommonPanel(data)
end

--显示副本结束时间
--seconds:副本结束时间
--startTime:副本开始时间
-- function InstanceCommonManager:ShowInstanceEndCountDown(seconds, startTime)
--     if seconds == nil then return end
--     startTime = startTime or DateTimeUtil.GetServerTime()
--     local data = {
--         ["startTime"] = startTime,
--         ["seconds"] = seconds,
--         ["type"] = InstanceCommonConfig.COMMON_INSTANCE_BEGIN_COUNTDOWN
--     }
--     GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
-- end
--显示星级评价
--data:table
--start:星级
--startTimeData:各星级时间段
-- [
--     0 = [
--         [1] = 301，
--         [2] = 600
--     ],
--     1 = [
--         [1] = 180
--         [2] = 300
--     ]
-- ]
--dropMessages:掉落显示文字信息列表
--countDownMessages:评价倒计时文字信息列表
--cb:回调函数:
--startTime:副本开始时间
function InstanceCommonManager:ShowStart(data)
    if data.startTimeData == nil or data.dropMessages == nil or data.countDownMessages == nil then return end
    data.startTime = data.startTime or DateTimeUtil.GetServerTime()
    local data = {
        ["startTime"] = data.startTime,
        ["startTimeData"] = data.startTimeData,
        ["start"] = data.start,
        ["dropMessages"] = data.dropMessages,
        ["countDownMessages"] = data.countDownMessages,
        ["cb"] = data.cb,
        ["type"] = InstanceCommonConfig.COMMON_INSTANCE_SHOW_START
    }
    -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
    self:ShowCommonPanel(data)
end

--副本倒计时开始
--instanceTime:副本时间
--startTime:副本开始时间
function InstanceCommonManager:StartInstanceCountDown(instanceTime, startTime)
    startTime = startTime or DateTimeUtil.GetServerTime()
    self:AddTimer(instanceTime, startTime)
    self:ShowCountDown(instanceTime, startTime)
end

--副本倒计时结束
function InstanceCommonManager:StopInstanceCountDown()
    self:ClearTimer()
end

--添加副本计时器
--instanceTime:副本时间
--startTime:副本开始时间
function InstanceCommonManager:AddTimer(instanceTime, startTime)
    startTime = startTime or DateTimeUtil.GetServerTime()
    local endTime = startTime + instanceTime
    self:ClearTimer()
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()
        local data = {}
        data.type = InstanceCommonConfig.COMMON_INSTANCE_COUNT_DOWN
        data.instanceTime = instanceTime
        -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
        self:ShowCommonPanel(data)
        if endTime - DateTimeUtil.GetServerTime() <= 0 then
            self:ClearTimer()
        end
    end)
end

--清除副本计时器
function InstanceCommonManager:ClearTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end


--显示副本结束的倒计时
--seconds:倒计时秒数
--cb:倒计时结束的回调函数
function InstanceCommonManager:ShowCountDown(instanceTime, startTime)
    local data = {}
    data.type = InstanceCommonConfig.COMMON_INSTANCE_SHOW_COUNTDOWN
    data.instanceTime = instanceTime
    data.startTime = startTime or DateTimeUtil.GetServerTime()
    -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
    self:ShowCommonPanel(data)
end

--显示左侧副本信息：通关目标、副本描述
--targetText:通关目标文字
--descText:副本描述
--instanceName:副本名称
function InstanceCommonManager:ShowInstanceInfo(targetText, descText, instanceName)
    local data = {}
    data.type = InstanceCommonConfig.COMMON_INSTANCE_SHOW_INFO
    data.targetText = targetText
    data.descText = descText
    data.instanceName = instanceName
    -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
    self:ShowCommonPanel(data)
end

--进入副本
--copyId 副本ID
function InstanceCommonManager:ShowEnterCopyInfo(copyId, confirmCB)
    local data = {}
    data.type = InstanceCommonConfig.COMMON_INSTANCE_ENTER_INFO
    data.copyId = copyId
    data.confirmCB = confirmCB
    -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
    self:ShowCommonPanel(data)
end




function InstanceCommonManager:ShowCommonPanel(data)
    local setting = GUIManager._panelSettings[PanelsConfig.InstanceCommon]
    if not setting.isActive then
        if self._showing then
            table.insert(self._showList, data)
        else
            self._showing = true
            GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data, self._showPanelCallBack)
        end
    else
        -- GUIManager.ShowPanel(PanelsConfig.InstanceCommon, data)
        EventDispatcher:TriggerEvent(GameEvents.INSTANCECOMMON_UPDATE, data)
    end
end

function InstanceCommonManager:ShowPanelCallBack()
    self._showing = false
    for i, v in ipairs(self._showList) do
        GUIManager.ShowPanel(PanelsConfig.InstanceCommon, v)
    end
end

InstanceCommonManager:Init()
return InstanceCommonManager
