local DragonSoulManager = {}
local DragonsoulDataHelper = GameDataHelper.DragonsoulDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local action_config = GameWorld.action_config
local ItemConfig = GameConfig.ItemConfig
local DragonsoulSlotDataHelper = GameDataHelper.DragonsoulSlotDataHelper
local DragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local  public_config = GameWorld.public_config
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local FloatTextManager = PlayerManager.FloatTextManager
local DragonsoulAttriDataHelper = GameDataHelper.DragonsoulAttriDataHelper

function DragonSoulManager:Init()  
    self._oldpkg = {}
    self._isresolve = false
    self._dragonBagUpdate = function()self:UpdateBag() end
    self._findsort = function(a,b) self:FindSort(a,b) end
    self._refreshRedPoint = function() self:UpdateRedEvent()end
end

function DragonSoulManager:UpdateRedEvent()
    EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_DRAGON_SPIRIT, self:RefreshRedPoint())                
end
function DragonSoulManager:GetDragonAtt(atts)
    local data={}
    for attid,num in pairs(atts) do
        local a = {tonumber(attid),num}
        table.insert(data,a)
    end
    return data
end

function DragonSoulManager:GetAttInfo(atts)--1:直接读,否则%
    local data = {}
    for attid,num in pairs(atts) do
        local a = tonumber(attid)
        local content,type = self:GetAttType(a)
        table.insert(data,{content,num,type})
    end
    return data
end

function DragonSoulManager:GetAttType(attid)
    local content = AttriDataHelper:GetName(attid)
    local type = AttriDataHelper:GetShowType(attid)--1:直接读,否则%
    return content,type
end

function DragonSoulManager:IsResolveNow(state)
	self._isresolve = state
end

function DragonSoulManager:GetAttDes(curatts,nextatts)
    local att1=self:GetDragonAtt(curatts)
    local att2=self:GetDragonAtt(nextatts)
    local data={}
    for k,v in ipairs(att1) do
        local a = att2[k]
        if v[1]==a[1] and v[2]~=0 and a[2] ~= 0 then
            local content = AttriDataHelper:GetName(v[1])
            local cur = v[2]
            local next = a[2]
            local type = AttriDataHelper:GetShowType(v[1])--1:直接读,否则%
            table.insert(data,{content,cur,next,type})
        end
    end
    return data
end
-- 所有属性叠加
function  DragonSoulManager:GetTotalAtts(attslist)
    local a={}
    for _,atts in ipairs(attslist) do 
        for attid,value in pairs(atts) do
            local i = tonumber(attid)
            if a[i]==nil then
                a[i]=0
            end
            a[i]=a[i]+value
        end
    end
    return a
end
function DragonSoulManager:HandleData(action_id, error_code, args) 
    if error_code ~= 0 then
        -- return
    end
    if action_id == action_config.DRAGON_SPIRIT_PUTON or action_id == action_config.DRAGON_SPIRIT_PUTOFF then
        EventDispatcher:TriggerEvent(GameEvents.DrgonSoulSlotUpdate)
    elseif action_id == action_config.DRAGON_SPIRIT_UPLEVEL then
        EventDispatcher:TriggerEvent(GameEvents.DragonSoulUp,error_code)
    elseif action_id == action_config.DRAGON_SPIRIT_COMPOSE then
        -- EventDispatcher:TriggerEvent(GameEvents.CardGroupdate)
    elseif action_id == action_config.DRAGON_SPIRIT_RESOLVE then
        EventDispatcher:TriggerEvent(GameEvents.DragonSoulRes,error_code)   
    elseif action_id == action_config.DRAGON_SPIRIT_RESTORE then
        EventDispatcher:TriggerEvent(GameEvents.DragonSoulDis)   
    end
end
-- RPC---
-- 龙魂穿戴
function DragonSoulManager.PutOnRpc(pos)
    GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_PUTON,pos,0)
end
-- 龙魂卸下
function DragonSoulManager.PutOffRpc(pos)
    GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_PUTOFF,pos,0)
end
--龙魂升级
function DragonSoulManager.UPLevelRpc(slot)
    GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_UPLEVEL,slot,0)
end
--龙魂分解
function DragonSoulManager.PutResolveRpc(pos)
    GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_RESOLVE,pos,0)
end
--龙魂拆解(1:拆身上 2：背包)
function DragonSoulManager.PutRestoreRpc(pos,type)
    GameWorld.Player().server.dragon_spirit_action_req(action_config.DRAGON_SPIRIT_RESTORE,type,pos)
end


function DragonSoulManager.IsOnlyElite()
    local bags = GameWorld.Player().dragon_spirit_bag
    for k,v in pairs(bags)do
        local curtype = DragonsoulDataHelper:GetType(v[1])
        if curtype[1]~=99 and #curtype==1 then
            return false
        end
    end
    return true
end


-- 是否镶嵌相同类型
function DragonSoulManager:GetPutOnPos()
    local player = GameWorld.Player()
    local attach = player.dragon_spirit---已镶嵌
    local bag = player.dragon_spirit_bag---背包中
    local samesouls = {}--相同类型的
    local diffsouls = {}--未镶嵌过的类型
    if table.isEmpty(bag) or not self:isEmptySlot() then
        return -1
    else
        for pos,v in pairs(bag) do
            local curtype = DragonsoulDataHelper:GetType(v[1])
            local flage = true
            for slot,value in pairs(attach) do
                local findtype = DragonsoulDataHelper:GetType(value[1])
                if self:IsSame(curtype,findtype) then
                    flage = false
                    samesouls[pos]={}
                    samesouls[pos]={pos,v[1],v[2]}
                    break
                end
            end
            if flage then
                diffsouls[pos]={}
                diffsouls[pos]={pos,v[1],v[2]}
            end
        end
    end
    --优先未镶嵌
    if #diffsouls>0 then
        table.sort(diffsouls,self._findsort)
        return diffsouls[#diffsouls]
    end

    if #samesouls>0 then
        table.sort(samesouls,self._findsort)
        return samesouls[#samesouls]
    end
    return -1
end
-- 
function DragonSoulManager:IsHaveType(dragonid)
    local player = GameWorld.Player()
    local attach = player.dragon_spirit---已镶嵌
    local bag = player.dragon_spirit_bag---背包中
    if table.isEmpty(bag) or not self:isEmptySlot() then
        return true
    else
        local curtype = DragonsoulDataHelper:GetType(dragonid)
        for slot,value in pairs(attach) do
            local findtype = DragonsoulDataHelper:GetType(value[1])
            if self:IsSame(curtype,findtype) then
                return true
            end
        end
    end
    return false
end
function DragonSoulManager:FindSort(a,b)
    local q1 = DragonsoulDataHelper:GetQuality(a[2])
    local q2 = DragonsoulDataHelper:GetQuality(b[2])
    if q1<q2 then
        return true
    else
        return false
    end
end

function DragonSoulManager:isEmptySlot()
    local player = GameWorld.Player()
    local attach = player.dragon_spirit---已镶嵌
    local playerle = player.level
    for i=1,7 do
        if attach[i]==nil then
            if DragonsoulSlotDataHelper:GetLevel(i)<= playerle then
                return true
            end
        end
    end
    return false
end
function  DragonSoulManager:IsSame(curtype,findtype)
    if #curtype ~= #findtype then
        return false
    else
        for k,v in ipairs(curtype) do
            local f = table.containsValue(findtype, v) 
            if not f then
                return false
            end
        end
        return true
    end 
end
function DragonSoulManager:UpdateBag()
    self:UpdateBagData()
    self:PkgFloat()
end

function DragonSoulManager:UpdateBagData()
    local item={}
    local bag = GameWorld.Player().dragon_spirit_bag
    for k,v in pairs(bag) do
        local id = v[1]
        local level = v[2]
        if item[id]==nil then
            item[id] = {}
        end
        table.insert(item[id],level)
    end
    DragonSoulData:HandleDragonSoulBag(item)
end

function DragonSoulManager:PkgFloat()	
	local newpkg = GameWorld.Player().dragon_spirit_bag
	local predragonid = 0
	for k,v in pairs(newpkg) do 
		local id = v[1]
		local level = v[2]
		local num0 = self:GetDragonNum(id,level,newpkg)
		local num1 = self:GetDragonNum(id,level,self._oldpkg)
		if num0>num1 and predragonid ~= id then
			local v = num0-num1
			local itemid = id
			FloatTextManager:OnChange(v,itemid)
			predragonid = id
		end
	end
	self._oldpkg = {}
	table.merge(self._oldpkg,newpkg)
end

function DragonSoulManager:GetDragonNum(id,level,data)
	local i=0
	for k,v in pairs(data) do 
		local dragonid = v[1]
		local dragonlevel = v[2]
		if dragonid == id and dragonlevel == level then
			i=i+1
		end
	end
	return i
end

function DragonSoulManager:RefreshRedPoint()
    return self:RefreshInsertRedPoint() or self:RefreshUpRedPoint() or self:RefreshResolvetRedPoint()
end

-- 镶嵌红点
function DragonSoulManager:RefreshInsertRedPoint()
    local attach = GameWorld.Player().dragon_spirit or {}
    for i=1,7 do 
        if attach[i] ==nil then
            if i==7 then
                local f = self:DragonSoulBagType(2)
                if f==true then return true end
            else
                local f = self:DragonSoulBagType(1)
                if f==true then return true end
            end
        end
    end
    return false
end
-- 1:普通 2：核心
function DragonSoulManager:DragonSoulBagType(dragonType)
    local bag = GameWorld.Player().dragon_spirit_bag or {}
    for k, v in pairs(bag) do
        if DragonsoulDataHelper:GetSubType(v[1])==dragonType and not self:IsPutOnType(v[1])then
            return true
        end
    end
    return false
end
function DragonSoulManager:RefrenDragonpiece()
	local entity = GameWorld.Player()
	self._oldpiece = entity.money_dragon_piece
end

function DragonSoulManager:OnMoneyChange(money_dragon_piece,oldValue)
    if self._isresolve then
        return 
    end
    FloatTextManager:OnMoneyChange(money_dragon_piece, public_config.MONTY_TYPE_DRAGON_PICEC, oldValue)
end
function DragonSoulManager:UpdataPieceFloat()
    self._isresolve = false
    LoggerHelper.Log(GameWorld.Player().money_dragon_piece)
    LoggerHelper.Log(self._oldpiece)
    FloatTextManager:OnMoneyChange(GameWorld.Player().money_dragon_piece, public_config.MONTY_TYPE_DRAGON_PICEC, self._oldpiece)
end
function DragonSoulManager:IsPutOnType(dragonid)
    local attach = GameWorld.Player().dragon_spirit or {}
    local t1 = DragonsoulDataHelper:GetType(dragonid)
    if t1[1]==99 then
        return true
    end
    for i=1,7 do 
        if attach[i] ~=nil then
            local id = attach[i][1]
            local level = attach[i][2]
            if id == dragonid then
                return true
            else
                if self:IsHaveType(id) then
                    return true
                end
            end
        end
    end
    return false
end


-- 升级红点
function DragonSoulManager:RefreshUpRedPoint()
    local attach = GameWorld.Player().dragon_spirit or {}
    for i=1,7 do 
        if attach[i] then
           local id = attach[i][1]
           local level = attach[i][2]
           local f = self:UpRedPoint(id,level)
           if f then
                return true
           end
        end
    end
    return false
end

function DragonSoulManager:UpRedPoint(id,level)
    local piece = GameWorld.Player().money_dragon_piece or 0
    local att = DragonsoulAttriDataHelper:GetAttByIdle(id,level)
    if att and att.cost then
        local cost = att.cost[public_config.MONTY_TYPE_DRAGON_PICEC]
        if cost<=piece then
            return true
        end
    end
    return false
end


-- 分解红点
function DragonSoulManager:RefreshResolvetRedPoint()
    local bag = GameWorld.Player().dragon_spirit_bag or {}
    for k, v in pairs(bag) do
        local type = DragonsoulDataHelper:GetType(v[1])
        if #type<2 then
           return true
        end
    end
    return false
end

-- 进入加监听
function DragonSoulManager:OnPlayerEnterWorld()	
    self:UpdateBagData()
    local f = FunctionOpenDataHelper:CheckFunctionOpen(370)
	if f then
		self:UpdateRedEvent()
    end
    local entity = GameWorld.Player()
	self._oldpkg = {}
    table.merge(self._oldpkg,entity.dragon_spirit_bag)
    
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEG_DRAGON_PIECE, self._refreshRedPoint)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT, self._refreshRedPoint)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._refreshRedPoint)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)
end
--退出移除监听
function DragonSoulManager:OnPlayerLeaveWorld()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEG_DRAGON_PIECE, self._refreshRedPoint)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT, self._refreshRedPoint)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._refreshRedPoint)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG, self._dragonBagUpdate)	
end

DragonSoulManager:Init() 
return DragonSoulManager