local EscortPeriManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local PlayerDataManager = PlayerManager.PlayerDataManager
local escortPeriData = PlayerDataManager.escortPeriData
local GuardGoddessDataHelper = GameDataHelper.GuardGoddessDataHelper
local GameSceneManager = GameManager.GameSceneManager
local GUIManager = GameManager.GUIManager
local TaskManager = PlayerManager.TaskManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskCommonManager = PlayerManager.TaskCommonManager
local TaskConfig = GameConfig.TaskConfig
local PlayerActionManager = GameManager.PlayerActionManager

require "PlayerManager/PlayerData/TaskData"
local TracingPointData = ClassTypes.TracingPointData

function EscortPeriManager:Init()
    self.escortPeriGoToNpcFunc = function() self:OnEscortPeriEvent() end
    self.refreshMainMenuIconFunc = function() self:RefreshMainMenuIcon() end
    self._escortPeriCommitTask = function() self:OnEscortFinishReq() end
    self._escortPeriEvent = function() self:OnEscortPeriEvent() end
end

function EscortPeriManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.Escort_Peri_Commit_Task, self._escortPeriCommitTask)
    EventDispatcher:AddEventListener(GameEvents.Daily_EscortPeri, self._escortPeriEvent)
    EventDispatcher:AddEventListener(GameEvents.LIMIT_ACTIVITY_INIT_COMPLETE, self.refreshMainMenuIconFunc)

    if GameWorld.Player().guard_goddess_id > 0 then
        EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.escortPeriGoToNpcFunc,EnumType.RemindType.ConvoyHave)
    end

    if GameWorld.Player().guard_goddess_count >= GlobalParamsHelper.GetParamValue(524) then
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, 303)
    end
end

function EscortPeriManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Escort_Peri_Commit_Task, self._escortPeriCommitTask)
    EventDispatcher:RemoveEventListener(GameEvents.Daily_EscortPeri, self._escortPeriEvent)
    EventDispatcher:RemoveEventListener(GameEvents.LIMIT_ACTIVITY_INIT_COMPLETE, self.refreshMainMenuIconFunc)
end

function EscortPeriManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("EscortPeriManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    end
    if action_id == action_config.GUARD_GODDNESS_GUARD then
        self:CommitEscort()
    elseif action_id == action_config.GUARD_GODDNESS_GUARD_FINISH then
        --完成任务        
        -- EventDispatcher:TriggerEvent(GameEvents.ON_GUARD_GODDESS_CHANGE)
        GUIManager.ShowPanel(PanelsConfig.EscortPeriSettle, args)
    end
end

function EscortPeriManager:ClearTask()
    local datas = {}
    local data = {}
    data[TaskConfig.TASK_CHANGE_INFO_ID] = self._guard_goddess_id
    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_GUARD_GODDESS
    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL
    table.insert(datas,data)
    TaskManager:UpdateTasksForMainMenu(datas)
    
    EventDispatcher:TriggerEvent(GameEvents.ActivityRemoveRemind,EnumType.RemindType.ConvoyHave)
end

function EscortPeriManager:RefreshMainMenuIcon()
    if GameWorld.Player().guard_goddess_count >= GlobalParamsHelper.GetParamValue(524) then
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, 303)
    end
end
-------------------------------------数据处理------------------------------------
function EscortPeriManager:GetAllData()
    return escortPeriData:GetAllData()
end

function EscortPeriManager:GetShowTips()
    return escortPeriData:GetShowTips()
end

function EscortPeriManager:IsInDoubleTime()
    return escortPeriData:IsInDoubleTime()
end

function EscortPeriManager:GetRemainCount()
    return escortPeriData:GetRemainCount()
end


function EscortPeriManager:TalkToNpc(npcId)
    local player = GameWorld.Player()
    if npcId == GlobalParamsHelper.GetParamValue(529) then
        if player.guard_goddess_id > 0 then
            EventDispatcher:TriggerEvent(GameEvents.SHOW_DIALOG, npcId)
        else
            GUIManager.ShowPanel(PanelsConfig.EscortPeri)
        end
    end
end

function EscortPeriManager:GotoAcceptEscort()
    TaskCommonManager:GoToNpc(GlobalParamsHelper.GetParamValue(529), TaskConfig.IS_TELEPORT_TO)
end

function EscortPeriManager:CommitEscort()
    --接了任务
    EventDispatcher:TriggerEvent(GameEvents.ActivityAddRemind, self.escortPeriGoToNpcFunc,EnumType.RemindType.ConvoyHave)
    self._guard_goddess_id = GameWorld.Player().guard_goddess_id
    local task = escortPeriData:NewTaskData(self._guard_goddess_id, public_config.TASK_STATE_COMPLETED)

    local datas = {}
    local data = {}
    data[TaskConfig.TASK_CHANGE_INFO_ID] = self._guard_goddess_id
    data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_GUARD_GODDESS
    data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
    data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = task
    table.insert(datas, data)
    TaskManager:UpdateTasksForMainMenu(datas)

    -- EventDispatcher:TriggerEvent(GameEvents.ON_GUARD_GODDESS_CHANGE)
    TaskCommonManager:SetCurAutoTask(task, nil)
    TaskCommonManager:UpdateAutoTask()
    --TaskManager:GoToNpc(GlobalParamsHelper.GetParamValue(530))
end

function EscortPeriManager:GetTaskInfo(npcId)
    --LoggerHelper.Error("GameWorld.Player().guard_goddess_id ====" .. tostring(GameWorld.Player().guard_goddess_id))
    if npcId == GlobalParamsHelper.GetParamValue(530) and GameWorld.Player().guard_goddess_id > 0 then
        local task = escortPeriData:NewTaskData(GameWorld.Player().guard_goddess_id, public_config.TASK_STATE_COMPLETED)
        return task, nil
    end
    return nil, nil
end

function EscortPeriManager:HandleAutoTask()
    if GameWorld.Player().guard_goddess_id > 0 then
        local task = escortPeriData:NewTaskData(GameWorld.Player().guard_goddess_id, public_config.TASK_STATE_COMPLETED)
        local trackTask = TracingPointData(task, nil)
        TaskCommonManager:OnManualHandleTask(trackTask)  
    end
end

function EscortPeriManager:GetCurTaskData()
    if GameWorld.Player().guard_goddess_id > 0 then
        local task = escortPeriData:NewTaskData(GameWorld.Player().guard_goddess_id, public_config.TASK_STATE_COMPLETED)
        return task, nil
    end
    return nil, nil
end

function EscortPeriManager:OnEscortPeriEvent()
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
    TaskCommonManager:SetCurAutoTask(nil, nil)

    if GameWorld.Player().guard_goddess_id > 0 then
        TaskCommonManager:GoToNpc(GlobalParamsHelper.GetParamValue(530), false, true)
    else
        self:GotoAcceptEscort()
    end
end

function EscortPeriManager:CanEscortPeri()
    return true
    -- if self:GetRemainCount() > 0 then
    --     return true
    -- else
    --     return false
    -- end
end

function EscortPeriManager:IsHasTimes()
    if self:GetRemainCount() > 0 then
        return true
    else
        return false
    end
end

-------------------------------------客户端请求-----------------------------------
function EscortPeriManager:OnEscortReq(id)
    local params = string.format("%d", id)
    GameWorld.Player().server.guard_goddess_action_req(action_config.GUARD_GODDNESS_GUARD, params)
end

function EscortPeriManager:OnEscortFinishReq()
    GameWorld.Player().server.guard_goddess_action_req(action_config.GUARD_GODDNESS_GUARD_FINISH, "")
end

EscortPeriManager:Init()
return EscortPeriManager
