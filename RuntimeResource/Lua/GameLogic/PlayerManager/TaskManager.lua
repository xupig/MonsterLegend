local TaskManager = {}
require "PlayerManager/PlayerData/DialogData"
-- require "PlayerManager/PlayerData/TaskData"

local action_config = require("ServerConfig/action_config")
local PlayerDataManager = PlayerManager.PlayerDataManager
local GameSceneManager = GameManager.GameSceneManager
local TaskData = PlayerDataManager.taskData
local public_config = GameWorld.public_config
local DialogConfig = GameConfig.DialogConfig
local NPCDataHelper = GameDataHelper.NPCDataHelper
local DialogData = ClassTypes.DialogData
local TracingPointData = ClassTypes.TracingPointData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local TaskDataHelper = GameDataHelper.TaskDataHelper
local MapManager = PlayerManager.MapManager
local InstanceManager = PlayerManager.InstanceManager
local GUIManager = GameManager.GUIManager
local TaskCommonManager = PlayerManager.TaskCommonManager
local TaskConfig = GameConfig.TaskConfig
local SceneConfig = GameConfig.SceneConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local CircleTaskManager = PlayerManager.CircleTaskManager
local WeekTaskManager = PlayerManager.WeekTaskManager
local RedPointManager = GameManager.RedPointManager

function TaskManager:Init()
    -- self._pointArrows = {}-- 缓存当前场景的任务追踪图标id
    self._createNPCByTaskTable = {}-- 缓存当前场景任务动态创建NPC的请求
    self._createResourcePointByTaskTable = {}-- 缓存当前场景任务动态创建客户端采集点的请求
    self._hasGuideList = {}
    self._addTaskList = {}
    self:InitCallbackFunc()

    local data = GlobalParamsHelper.GetParamValue(948)--心魔任务
    for k,v in pairs(data) do
        self._xinmoTextId = k--LanguageDataHelper.CreateContent(k)
        self._xinmos = v
    end
    self._node = RedPointManager:CreateStrengthenNodeByFunId(self._xinmoTextId, 147258)--心魔副本
    self._node:SetStrengthCallBack(self._strengthXinmoClick)
end

function TaskManager:InitCallbackFunc()
    self._onLevelChangeCB = function()self:OnLevelChange() end
    self._onTaskTrackMarkChanged = function(taskId, flag)self:ChangeTrackMark(taskId, flag) end
    self._onShowMainDialog = function(npcId)self:ShowMainDialog(npcId) end
    self._onAcceptTask = function(taskId)self:AcceptTask(taskId) end
    self._onCommitTask = function(taskId)self:CommitTask(taskId) end
    -- self._onWaitForCommitTask = function(taskId)self:WaitForCommitTask(taskId) end
    self._onHandOverItem = function(items)self:HandOverItem(items) end
    self._onDoEvent = function(eventItemData)self:DoEvent(eventItemData) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onCreateNPCByTask = function(taskId, npcIds, posIDs)self:OnCreateNPCByTask(taskId, npcIds, posIDs) end
    self._onCreateClientResourcePointByTask = function(actionCfg)self:OnCreateClientResourcePointByTask(actionCfg) end
    self._onClientEntityCollectComplete = function(entityCfgId, eventId)self:OnClientEntityCollectComplete(entityCfgId, eventId) end
    self._onTaskEventTrigger = function(id, task_event_id)self:OnTaskEventTrigger(id, task_event_id) end
    self._closeUIPanel = function() self:OnHandleClosePanel() end
    self._guideRefreshTaskListTop = function(taskId) self:OnGuideRefreshTaskListTop(taskId) end
    self._on_0_Oclock = function()self:OnPassDay() end
    self._on_Week_0_Oclock = function()self:OnPassWeek() end
    self._onContinueTask = function()self:ContinueTask() end
    self._addNewBranch = function(id)self:AddNewBranch(id) end
    self._strengthXinmoClick = function(id)self:StrengthXinmoClick(id)end
end

function TaskManager:OnPlayerEnterWorld()
    self._initTaskComplete = false
    -- local task = ClassTypes.TaskData
    -- TaskData = task()
    -- GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
    EventDispatcher:AddEventListener(GameEvents.OnTaskTrackMarkChanged, self._onTaskTrackMarkChanged)
    EventDispatcher:AddEventListener(GameEvents.SHOW_DIALOG, self._onShowMainDialog)
    EventDispatcher:AddEventListener(GameEvents.ACCEPT_TASK, self._onAcceptTask)
    EventDispatcher:AddEventListener(GameEvents.COMMIT_TASK, self._onCommitTask)
    -- EventDispatcher:AddEventListener(GameEvents.WAIT_FOR_COMMIT_TASK, self._onWaitForCommitTask)
    EventDispatcher:AddEventListener(GameEvents.HAND_OVER_ITEM, self._onHandOverItem)
    EventDispatcher:AddEventListener(GameEvents.TASK_DO_EVENT, self._onDoEvent)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.CreateNPCByTask, self._onCreateNPCByTask)
    EventDispatcher:AddEventListener(GameEvents.CreateClientResourcePointByTask, self._onCreateClientResourcePointByTask)
    EventDispatcher:AddEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
    EventDispatcher:AddEventListener(GameEvents.TASK_EVENT_TRIGGER, self._onTaskEventTrigger)-- 到达目的地事件触发
    EventDispatcher:AddEventListener(GameEvents.CLOSE_UI_PANEL, self._closeUIPanel)
    EventDispatcher:AddEventListener(GameEvents.GuideRefreshTaskListTop, self._guideRefreshTaskListTop)--任务置顶
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock_Login, self._on_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.On_Week_0_Oclock, self._on_Week_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.On_Week_0_Oclock_Login, self._on_Week_0_Oclock)
    EventDispatcher:AddEventListener(GameEvents.ADD_NEW_BRANCH, self._addNewBranch)
end

function TaskManager:OnPlayerLeaveWorld()
    self._noMainTask = nil
    self._nilMainTask = nil
    self._againMainTask = nil
    self._initTaskComplete = false
    self._result = nil
    -- local task = ClassTypes.TaskData
    -- TaskData = task()
    -- GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._onLevelChangeCB)
    EventDispatcher:RemoveEventListener(GameEvents.OnTaskTrackMarkChanged, self._onTaskTrackMarkChanged)
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_DIALOG, self._onShowMainDialog)
    EventDispatcher:RemoveEventListener(GameEvents.ACCEPT_TASK, self._onAcceptTask)
    EventDispatcher:RemoveEventListener(GameEvents.COMMIT_TASK, self._onCommitTask)
    -- EventDispatcher:RemoveEventListener(GameEvents.WAIT_FOR_COMMIT_TASK, self._onWaitForCommitTask)
    EventDispatcher:RemoveEventListener(GameEvents.HAND_OVER_ITEM, self._onHandOverItem)
    EventDispatcher:RemoveEventListener(GameEvents.TASK_DO_EVENT, self._onDoEvent)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.CreateNPCByTask, self._onCreateNPCByTask)
    EventDispatcher:RemoveEventListener(GameEvents.CreateClientResourcePointByTask, self._onCreateClientResourcePointByTask)
    EventDispatcher:RemoveEventListener(GameEvents.CLIENT_ENTITY_COLLECT_COMPLETE, self._onClientEntityCollectComplete)
    EventDispatcher:RemoveEventListener(GameEvents.TASK_EVENT_TRIGGER, self._onTaskEventTrigger)-- 到达目的地事件触发
    EventDispatcher:RemoveEventListener(GameEvents.CLOSE_UI_PANEL, self._closeUIPanel)
    EventDispatcher:RemoveEventListener(GameEvents.GuideRefreshTaskListTop, self._guideRefreshTaskListTop)--任务置顶
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock_Login, self._on_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.On_Week_0_Oclock, self._on_Week_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.On_Week_0_Oclock_Login, self._on_Week_0_Oclock)
    EventDispatcher:RemoveEventListener(GameEvents.ADD_NEW_BRANCH, self._addNewBranch)
end

function TaskManager:GetTaskByID(id)
    for k,v in pairs(self._result) do
        if v:GetTask():GetId() == id then
            return v
        end
    end
end


function TaskManager:StrengthXinmoClick(id)
    local taskData = self:GetTaskByID(id)
    if taskData ~= nil then
        TaskCommonManager:OnClickTrackCurTask(taskData)
    end
end

function TaskManager:AddNewBranch(id)
    if table.containsValue(self._xinmos, id) then
        self._node:SetStrengthData(id)
        self._node:CheckStrengthen(true)
    end
end

function TaskManager:DelBranch(id)
    if table.containsValue(self._xinmos, id) then
        self._node:CheckStrengthen(false)
    end
end

function TaskManager:OnPassWeek()
    --LoggerHelper.Error("过了一周")
    local task = TaskCommonManager:GetCurAutoTask()
    if task and task:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
        --如果正在做周常任务则停止自动任务
        GameWorld.Player():StopAutoFight()
        GameWorld.Player():StopMoveWithoutCallback()
        PlayerActionManager:StopAllAction()
        TaskCommonManager:SetCurAutoTask(nil, nil)
    end

    --LoggerHelper.Error("现在任务数量===" .. #self._result)
    --删除周常任务
    for i,v in ipairs(self._result) do
        local thisTask = v:GetTask()
        if thisTask:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
            table.remove(self._result, i)
        end
    end

    if self._laterMainTask and self._laterMainTask:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
        self._needLaterRun = false
        self._laterMainTask = nil
    end

    WeekTaskManager:ClearTaskData()
    --LoggerHelper.Error("现在任务数量===" .. #self._result)
    EventDispatcher:TriggerEvent(GameEvents.ON_TASK_MENU_CHANGE)
    EventDispatcher:TriggerEvent(GameEvents.ON_WEEK_TASK_PASS_WEEK)
end

function TaskManager:OnPassDay()    
    --LoggerHelper.Error("过了一天")
    local task = TaskCommonManager:GetCurAutoTask()
    if task and task:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
        --如果正在做赏金任务则停止自动任务
        GameWorld.Player():StopAutoFight()
        GameWorld.Player():StopMoveWithoutCallback()
        PlayerActionManager:StopAllAction()
        TaskCommonManager:SetCurAutoTask(nil, nil)
    end

    --LoggerHelper.Error("现在任务数量===" .. #self._result)
    --删除赏金任务
    for i,v in ipairs(self._result) do
        local thisTask = v:GetTask()
        if thisTask:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
            table.remove(self._result, i)
        end
    end

    if self._laterMainTask and self._laterMainTask:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
        self._needLaterRun = false
        self._laterMainTask = nil
    end

    CircleTaskManager:ClearTaskData()
    --LoggerHelper.Error("现在任务数量===" .. #self._result)
    EventDispatcher:TriggerEvent(GameEvents.ON_TASK_MENU_CHANGE)
    EventDispatcher:TriggerEvent(GameEvents.ON_CIRCLE_TASK_PASS_Day)
end

function TaskManager:OnLevelChange()
    -- LoggerHelper.Log("Ash: OnLevelChange")
    --local sceneType = GameSceneManager:GetCurSceneType()
    --GUIManager.ShowPanel(PanelsConfig.LeftMainUI, sceneType)

    local entity = GameWorld.Player()
    if entity ~= nil then
        -- EventDispatcher:TriggerEvent(GameEvents.OnTaskChanged)
    end
end

function TaskManager:OnEnterMap(mapId)
    -- LoggerHelper.Log("Ash: OnEnterMap " .. tostring(mapId))
    -- self:ClearTracingMapPoint()
    -- self:UpdateCurMapTrackTask(mapId)
    self:RefreshTracingMapPoint(mapId)
end

function TaskManager:OnLeaveMap(mapId)
    -- LoggerHelper.Log("Ash: OnLeaveMap " .. tostring(mapId))
    -- self:ClearTracingMapPoint()
    self._createNPCByTaskTable = {}
    self._createResourcePointByTaskTable = {}
end

function TaskManager:OnCreateClientResourcePointByTask(actionCfg)
    local id = actionCfg.id
    -- LoggerHelper.Log("Ash: OnCreateClientResourcePointByTask " .. tostring(id))
    self._createResourcePointByTaskTable[id] = {}
    self._createResourcePointByTaskTable[id]["actionCfg"] = actionCfg
-- self._createResourcePointByTaskTable[id]["pos"] = pos
-- self._createResourcePointByTaskTable[id]["task_event_id"] = task_event_id
-- self._createResourcePointByTaskTable[id]["cd_time"] = cd_time
-- self._createResourcePointByTaskTable[id]["destroy_after_collect"] = destroy_after_collect
-- self._createResourcePointByTaskTable[id]["collect_btn_icon"] = collect_btn_icon
-- self._createResourcePointByTaskTable[id]["collect_point_name"] = collect_point_name
end

function TaskManager:OnCreateNPCByTask(taskId, npcIds, posIDs)
    -- LoggerHelper.Log("Ash: OnCreateNPCByTask " .. tostring(taskId))
    LoggerHelper.Log(npcIds)
    local task = self:GetTaskById(taskId)
    if task ~= nil then
        local idx = 0
        local len = table.getn(npcIds)
        while idx < len do
            idx = idx + 1
            local id = npcIds[idx]
            local posID = posIDs[idx]
            --LoggerHelper.Error("OnCreateNPCByTask"..taskId.."/"..id)
            EventDispatcher:TriggerEvent(GameEvents.CreateNPC, id, posID)
        end
    else
        if self._createNPCByTaskTable[taskId] == nil then
            self._createNPCByTaskTable[taskId] = {}
        end
        local d = {["npcIds"] = npcIds, ["posIDs"] = posIDs}
        table.insert(self._createNPCByTaskTable[taskId], d)
    end
end

function TaskManager:CheckCreateNPCByTask()
    for taskId, v in pairs(self._createNPCByTaskTable) do
        local task = self:GetTaskById(taskId)
        if task ~= nil then
            for i = 1, #v do
                --d，一个npcregion节点里面的npc信息
                local d = v[i]
                -- LoggerHelper.Log("Ash: CheckCreateNPCByTask " .. tostring(taskId))
                local idx = 0
                local len = table.getn(d["npcIds"])
                while idx < len do
                    idx = idx + 1
                    local id = d["npcIds"][idx]
                    local posID = d["posIDs"][idx]
                    EventDispatcher:TriggerEvent(GameEvents.CreateNPC, id, posID)
                end
                self._createNPCByTaskTable[taskId] = nil
            end
        end
    end
end

function TaskManager:HandleData(event_id, error_id, args)
    --报错了
    if error_id ~= 0 then
        LoggerHelper.Error("Task Error:" .. error_id)
        return --获取正在处理的任务列表。任务状态为可接,已接，已完成
    elseif event_id == action_config.TASK_PROCESSING_LIST then
        -- LoggerHelper.Error("Sam: 任务列表 .." .. PrintTable:TableToStr(args))
        self:ProcessingList(args)
    --任务刷新，完成和进度。如果有更新，没有就是新加的进行中的任务。
    elseif event_id == action_config.TASK_REFRESH_PROCESSING then
        -- LoggerHelper.Error("Sam: 更新任务 .." .. PrintTable:TableToStr(args))
        self:RefreshProcessing(args)
    --删除进行中的任务。放弃，或者已提交。
    elseif event_id == action_config.TASK_DEL_PROCESSING then
        -- LoggerHelper.Error("Sam: 删除任务 .." .. PrintTable:TableToStr(args))
        self:DelProcessing(args)
    --获取已经提交的支线任务列表。
    elseif event_id == action_config.TASK_COMMITED_LIST_BRANCH then
        self:CommitedListBranch(args)
        EventDispatcher:TriggerEvent(GameEvents.TASK_INIT_COMPLETE)
        self._initTaskComplete = true
    --增加已提交的支线任务。
    elseif event_id == action_config.TASK_ADD_COMMITED_BRANCH then
        -- LoggerHelper.Error("Sam: 增加提交的支线任务 .." .. PrintTable:TableToStr(args))
        self:AddCommitedBranch(args)
    --追踪更新,和任务状态等比是独立的一个变量处理
    elseif event_id == action_config.TASK_REFRESH_PROCESSING_TRACK then
        self:RefreshProcessingTrack(args)
    else
        LoggerHelper.Error("Task event id not handle:" .. event_id)
    end
end

function TaskManager:HandleResp(act_id, code, args)
    if code ~= 0 then
        return
    end
    if act_id == action_config.TASK_ACCEPT then
        EventDispatcher:TriggerEvent(GameEvents.OnTaskUpdateSuccess)
    -- EventDispatcher:TriggerEvent(GameEvents.ACCEPT_TASK_CALLBACK)
    elseif act_id == action_config.TASK_COMMIT then
        EventDispatcher:TriggerEvent(GameEvents.OnTaskUpdateSuccess)
    -- EventDispatcher:TriggerEvent(GameEvents.COMMITED_TASK_CALLBACK)
    elseif act_id == action_config.TASK_PROCESSING_LIST then
        EventDispatcher:TriggerEvent(GameEvents.OnTaskUpdateSuccess)
    elseif act_id == action_config.TASK_COMMITED_LIST_BRANCH then
        EventDispatcher:TriggerEvent(GameEvents.OnTaskUpdateSuccess)
    end
end

function TaskManager:ProcessingList(args)
    -- LoggerHelper.Log("Ash: ProcessingList:" .. PrintTable:TableToStr(args))
    TaskData:ResetTaskData(args)
    self:TriggerRefreshTasks()
    
    -- -- 处理需要进入镜像副本需求
    -- local tasks = TaskData:GetTasks()
    -- for k, task in pairs(tasks) do
    --     if task:GetImageMapId() ~= nil and task:GetImageMapId() ~= 0
    --         and task:GetState() == public_config.TASK_STATE_ACCEPTED
    --         and GameSceneManager:GetCurrMapID() ~= task:GetImageMapId() then
    --         InstanceManager:OnImageMapEnter(task:GetImageMapId())
    --         return
    --     end
    -- end
end

function TaskManager:RefreshProcessing(args)
    -- LoggerHelper.Log("Ash: RefreshProcessing:" .. PrintTable:TableToStr(args))
    local data = TaskData:UpdateTaskData(args)
    if not table.isEmpty(data) then
        -- LoggerHelper.Error("data =======" .. PrintTable:TableToStr(data))
        self:UpdateTasksForMainMenu(data)
    end
    self:TriggerRefreshTasks()
end

function TaskManager:DelProcessing(args)
    -- LoggerHelper.Log("Ash: DelProcessing:" .. PrintTable:TableToStr(args))
    local data = TaskData:DelTaskData(args)
    if not table.isEmpty(data) then
        self:UpdateTasksForMainMenu(data)
    end
    self:TriggerRefreshTasks()
end

function TaskManager:CommitedListBranch(args)
    -- LoggerHelper.Log("Ash: CommitedListBranch:" .. PrintTable:TableToStr(args))
    TaskData:ResetCommitedOptional(args)
end

function TaskManager:AddCommitedBranch(args)
    -- LoggerHelper.Log("Ash: AddCommitedBranch:" .. PrintTable:TableToStr(args))
    local data = TaskData:UpdateCommitedOptional(args)
    for id, v in pairs(args) do
        self:DelBranch(id)
    end
    -- LoggerHelper.Error("datas ======= " .. PrintTable:TableToStr(data))
    if not table.isEmpty(data) then
        -- LoggerHelper.Error("data =======" .. PrintTable:TableToStr(data))
        self:UpdateTasksForMainMenu(data)
    end
    self:TriggerRefreshTasks()
end

function TaskManager:RefreshProcessingTrack(args)
    -- LoggerHelper.Log("Ash: RefreshProcessingTrack:" .. PrintTable:TableToStr(args))
    TaskData:RefreshProcessingTrack(args)
    self:TriggerRefreshTasks()
end

function TaskManager:TriggerRefreshTasks()
    -- self:ClearTracingMapPoint()
    -- self:UpdateCurMapTrackTask(GameSceneManager:GetCurrMapID())
    self:RefreshTracingMapPoint(GameSceneManager:GetCurrMapID())
    self:CheckCreateNPCByTask()
    EventDispatcher:TriggerEvent(GameEvents.OnTaskChanged)
    TaskCommonManager:UpdateAutoTask()
end

function TaskManager:HandleAutoTask()
    local curAutoTask = TaskCommonManager:GetCurAutoTask()
    -- LoggerHelper.Error("curAutoTask ======" .. PrintTable:TableToStr(curAutoTask))
    local taskType = curAutoTask:GetTaskType()
    if taskType == public_config.TASK_TYPE_MAIN then
        local mainTask = self:GetMainTask()
        if mainTask then
            -- LoggerHelper.Error("自动任务..... " .. PrintTable:TableToStr(mainTask))
            -- LoggerHelper.Log("Ash: UpdateAutoTask TASK_TYPE_MAIN: " .. PrintTable:TableToStr(mainTask))
            TaskCommonManager:OnClickTrackCurTask(TracingPointData(mainTask))
        end
    elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
        -- if GameManager.GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_COMBAT then
        --     self._enterMap = function(mapId)
        --         local sceneType = MapDataHelper.GetSceneType(mapId)
        --         if sceneType ~= SceneConfig.SCENE_TYPE_COMBAT then
        --             self:HandleAutoTask()
        --             EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._enterMap)
        --         end
        --     end
        --     EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._enterMap)
        --     return
        -- end
        
        local cot = self:GetCommitedOptionalTask()
        local taskId = nil
        if cot[curAutoTask:GetId()] then
            if taskType == public_config.TASK_TYPE_BRANCH then
                -- taskId = curAutoTask:GetNextTaskId()
                TaskCommonManager:SetCurAutoTask(nil, nil)
                return
            else
                taskId = curAutoTask:GetNextTaskId()
            end
        else
            taskId = curAutoTask:GetId()
        end
        local optTask = self:GetOptionalTask()
        if optTask[taskId] then
            TaskCommonManager:OnClickTrackCurTask(TracingPointData(optTask[taskId]))
        end
    else
        LoggerHelper.Error("Task type not handle:" .. k .. " " .. taskType)
    end
end

function TaskManager:RefreshTracingMapPoint(mapId)
    -- local locationCache = {}
    local tracingMapPoint = self:GetTracingMapPoints(mapId)
    -- LoggerHelper.Log("Ash: RefreshTracingMapPoint: " .. PrintTable:TableToStr(tracingMapPoint))
    for k, v in pairs(tracingMapPoint) do
        -- 处理客户端采集点加载
        local eventItemData = v:GetEventItemData()
        if eventItemData:GetEventID() == public_config.EVENT_ID_CLIENT_PICK_FINISH
            or eventItemData:GetEventID() == public_config.EVENT_ID_CLIENT_PICK_FINISH_TEAM then
            local taskEventId = eventItemData:GetConds(public_config.EVENT_PARAM_CLIENT_COLLECT_ID)
            --LoggerHelper.Log("Ash: CreateClientResourcePoint: taskEventId " .. taskEventId .. " " .. PrintTable:TableToStr(self._createResourcePointByTaskTable))
            for k, v in pairs(self._createResourcePointByTaskTable) do
                --LoggerHelper.Log("Ash: CreateClientResourcePoint: k " .. k)
                if k == taskEventId then
                    EventDispatcher:TriggerEvent(GameEvents.CreateClientResourcePoint, v["actionCfg"])
                end
            end
        end
    
    -- -- 处理任务指示标志
    -- local loc = tostring(v:GetLocation())
    -- if locationCache[loc] ~= nil then --相同坐标的优先显示主线任务
    --     if v:GetTask():GetTaskType() == public_config.TASK_TYPE_MAIN then
    --         locationCache[loc] = v
    --     end
    -- else
    --     locationCache[loc] = v
    -- end
    end
-- LoggerHelper.Log("Ash: RefreshTracingMapPoint: " .. PrintTable:TableToStr(locationCache))
-- for k, v in pairs(locationCache) do
--     table.insert(self._pointArrows, k)--缓存当前场景添加了的标识id，用于统一清除
--     local task = v:GetTask()
--     local eventItemData = v:GetEventItemData()
--     EventDispatcher:TriggerEvent(GameEvents.ADD_POINTING_ARROW, k, task:GetIcon(), v:GetLocation())
-- end
end

function TaskManager:GetTracingMapPoints(mapId)
    local trackedTask = self:GetTrackedTasks()
    -- LoggerHelper.Error("Ash: GetTracingMapPoints: " .. PrintTable:TableToStr(trackedTask), true)
    local tracingMapPoint = {}
    for i, task in ipairs(trackedTask) do
        local state = task:GetState()
        if state == public_config.TASK_STATE_ACCEPTED then
            local eventItemData = TaskCommonManager:GetMapPosEvent(task, mapId)
            -- LoggerHelper.Log("Ash: eventItemData: " .. PrintTable:TableToStr(eventItemData))
            if eventItemData ~= nil then
                local pointArrowId = "scene" .. tostring(mapId) .. ":" .. task:GetId()
                local follow = eventItemData:GetFollow()
                local location = Vector3(follow[mapId][1], follow[mapId][2], follow[mapId][3])
                tracingMapPoint[pointArrowId] = TracingPointData(task, eventItemData, location)
            end
        end
    end
    return tracingMapPoint
end

-- function TaskManager:ClearTracingMapPoint()
-- -- for i, v in ipairs(self._pointArrows) do
-- --     EventDispatcher:TriggerEvent(GameEvents.REMOVE_POINTING_ARROW, v)
-- -- end
-- -- self._pointArrows = {}
-- end

function TaskManager:GetTaskById(id)
    return TaskData:GetTaskById(id)
end

function TaskManager:GetMainTask()
    return TaskData:GetMainTask()
end

function TaskManager:GetAllTask()
    return TaskData:GetTasks()
end

--获取支线任务
function TaskManager:GetBranchTask()
    return TaskData:GetBranchTask()
end

function TaskManager:GetOptionalTask()
    return TaskData:GetOptionalTask()
end

function TaskManager:GetVocationTask()
    return TaskData:GetVocationTask()
end

function TaskManager:GetCommitedOptionalTask()
    return TaskData:GetCommitedOptionalTask()
end

function TaskManager:GetTaskHasCommited(id)
    local task = TaskData:GetTaskById(id)
    if task ~= nil then -- 当前拥有的任务都是未提交的
        return false
    else
        local taskType = TaskDataHelper:GetTaskType(id)
        if taskType == public_config.TASK_TYPE_MAIN then
            local cmti = self:GetCommitedMainTaskId()
            if id <= cmti then -- 主线任务比较与当前已提交的主线任务的大小决定
                return true
            else
                return false
            end
        elseif taskType == public_config.TASK_TYPE_BRANCH or taskType == public_config.TASK_TYPE_VOCATION then
            local ot = TaskData:GetCommitedOptionalTask()-- 支线任务直接查询是否已提交
            if ot ~= nil and ot[id] ~= nil then
                return true
            else
                return false
            end
        else
            return false
        end
    end
end

function TaskManager:GetCommitedMainTaskId()
    local entity = GameWorld.Player()
    -- LoggerHelper.Log("Ash: GetCommitedMainTaskId:" .. entity.commited_main_task_id)
    return entity and entity.commited_main_task_id or -1
end

function TaskManager:GetCommitedMainTask()
    return TaskData:NewTaskItemData(self:GetCommitedMainTaskId(), nil)
end

function TaskManager:NewTaskItemData(id, serverData)
    return TaskData:NewTaskItemData(id, serverData)
end

function TaskManager:NewTaskItemDataWithState(id, state)
    return TaskData:NewTaskItemDataWithState(id, state)
end

function TaskManager:GetNextTask()
    local commitedMainTask = self:GetCommitedMainTask()
    -- LoggerHelper.Log("Ash: commitedMainTask:" .. PrintTable:TableToStr(commitedMainTask))
    return commitedMainTask:GetNextTaskItemData()
end

-- function TaskManager:GetTaskForMainUI()
--     local curMapTrackTask = TaskData:GetCurMapTrackingTask()
--     if #curMapTrackTask == 0 then -- 当前地图没有追踪任务，取所有追踪任务第一个显示图标
--         local trackedTasks = self:GetTrackedTasks()
--         for k, v in pairs(trackedTasks) do
--             -- if v:GetState() ~= public_config.TASK_STATE_ACCEPTABLE then -- 可接状态不作为跟踪目标
--             return TracingPointData(v)
--         -- end
--         end
--         return nil
--     end
--     local pos = GameWorld.Player():GetPosition()
    
--     local minDis = 10000
--     local minTrackTask = nil
--     local curTaskType = public_config.TASK_TYPE_BRANCH
--     for i, v in ipairs(curMapTrackTask) do --按距离排序最近的任务
--         local dis = Vector3.Distance(pos, v:GetLocation())
--         if minDis > dis then
--             minDis = dis
--             minTrackTask = v
--             curTaskType = v:GetTask():GetTaskType()
--         elseif minDis == dis
--             and curTaskType == public_config.TASK_TYPE_BRANCH
--             and v:GetTask():GetTaskType() == public_config.TASK_TYPE_MAIN then
--             -- 同等距离时优先显示主线任务
--             minDis = dis
--             minTrackTask = v
--             curTaskType = v:GetTask():GetTaskType()
--         end
--     end
--     return minTrackTask
-- end

-- function TaskManager:GetTasksForMainMenu()
--     local curMapTrackTask = TaskData:GetCurMapTrackingTask()
--     local pos = GameWorld.Player():GetPosition()
--     local curTasks = {}
--     local minDis = {}
--     for i, v in ipairs(curMapTrackTask) do
--         local taskId = v:GetTask():GetId()
--         local dis = Vector3.Distance(pos, v:GetLocation())
--         if minDis[taskId] ~= nil then --相同任务的不同步骤以最近的显示
--             if minDis[taskId] > dis then
--                 minDis[taskId] = dis
--                 curTasks[taskId] = v
--             end
--         else
--             minDis[taskId] = dis
--             curTasks[taskId] = v
--         end
--     end
--     local result = {}
--     local trackedTasks = self:GetTrackedTasks()
--     for i, task in ipairs(trackedTasks) do --以所有跟踪任务为基本，有当前地图的任务就把跟踪任务的内容替换掉，没有则按未完成步骤优先显示event
--         if curTasks[task:GetId()] ~= nil then
--             table.insert(result, curTasks[task:GetId()])
--         else
--             local eventItemData = TaskCommonManager:GetNextEvent(task)
--             table.insert(result, TracingPointData(task, eventItemData))
--         -- end
--         end
--     end
--     return result
-- end
--tasks:多个task
--task:
--任务类型
-- TaskConfig.TASK_TYPE_CIRCLETASK = 4             --赏金任务(日常)
-- TaskConfig.TASK_TYPE_GUARD_GODDESS = 5          --护送女神
-- TaskConfig.TASK_TYPE_COMMON_TASK = 6            --一般任务（主线、支线、转职）
--任务变化类型
-- TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE = 1          --任务变化类型（更新）
-- TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL = 2             --任务变化类型（删除）
-- TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD = 3             --任务变化类型（新增）
----任务变化实体属性定义
-- TaskConfig.TASK_CHANGE_INFO_ID = 1              --任务变化属性定义（任务id）
-- TaskConfig.TASK_CHANGE_INFO_TASK_TYPE = 2       --任务变化属性定义（任务类型:参照上面）
-- TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE = 3    --任务变化属性定义（任务变化类型:参照上面）
function TaskManager:UpdateTasksForMainMenu(tasks)
    -- LoggerHelper.Error("变化前任务列表信息======" .. PrintTable:TableToStr(self._result))
    if tasks == nil or self._result == nil then return end
    local delFlag = false
    local addFlag = false
    local delTaskType
    local addTaskItemType
    local addId

    for _, task in ipairs(tasks) do
        if table.isEmpty(self._result) then
            if task[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] == TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD then
                --新增
                local newTask = task[TaskConfig.TASK_CHANGE_INFO_TASK_DATA]
                local eventItemData = TaskCommonManager:GetNextEvent(newTask)
                table.insert(self._result, 1, TracingPointData(newTask, eventItemData))
                addId = newTask:GetId()
                addFlag = true
                addTaskItemType = newTask:GetTaskItemType()
                break
            end
        -- self:GetTasksForMainMenu()
        else
            local priority = self:GetPriority(task[TaskConfig.TASK_CHANGE_INFO_TASK_DATA])

            for i, v in ipairs(self._result) do
                local thisTask = v:GetTask()
                local id = thisTask:GetId()
                local type = thisTask:GetTaskItemType()
                --护送女神任务特殊处理
                if type == task[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] and type == TaskConfig.TASK_TYPE_GUARD_GODDESS and 
                   task[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] == TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL then
                    delFlag = true
                    delTaskType = thisTask:GetTaskType()
                    table.remove(self._result, i)
                    break
                end

                if id == task[TaskConfig.TASK_CHANGE_INFO_ID] and type == task[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] then
                    if task[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] == TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE then
                        --更新
                        local updateTask = task[TaskConfig.TASK_CHANGE_INFO_TASK_DATA]
                        local eventItemData = TaskCommonManager:GetNextEvent(updateTask)
                        table.remove(self._result, i)
                        self:AddTaskToMenuTaskData(updateTask, priority)
                    elseif task[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] == TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL then
                        --删除
                        delFlag = true
                        delTaskType = thisTask:GetTaskType()
                        table.remove(self._result, i)
                    end
                    break
                end
                if task[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] == TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD then
                    --新增
                    local newTask = task[TaskConfig.TASK_CHANGE_INFO_TASK_DATA]
                    local eventItemData = TaskCommonManager:GetNextEvent(newTask)
                    local pos = self:GetTaskPos(task[TaskConfig.TASK_CHANGE_INFO_ID])
                    if pos ~= 0 then
                        table.remove(self._result, pos)
                    end
                    self:AddTaskToMenuTaskData(newTask, priority)
                    addFlag = true
                    addId = newTask:GetId()
                    addTaskItemType = newTask:GetTaskItemType()
                    break
                end
            end
        end
    end

    if delFlag and delTaskType == public_config.TASK_TYPE_MAIN and #tasks == 1 then 
        return
    end
    
    if addFlag then
        self._addTaskList[addId] = 1
        EventDispatcher:TriggerEvent(GameEvents.OnAddTask, addId)
    end

    if addId and addTaskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and TaskDataHelper:GetTaskGuide(addId) ~= 0 then
        self:GuideRefreshTaskListTop(addId)
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_TASK_MENU_CHANGE)
end

function TaskManager:SetHasGuidTask(taskId)
    self._hasGuideList[taskId] = 1
end

function TaskManager:GetHasGuidTask(taskId)
    return self._hasGuideList[taskId]
end

function TaskManager:GetAddTask(taskId)
    return self._addTaskList[taskId]
end

--任务置顶
function TaskManager:GuideRefreshTaskListTop(taskId)
    if table.isEmpty(self._result) then
        return
    else
        for i, v in ipairs(self._result) do
            local thisTask = v:GetTask()
            local id = thisTask:GetId()
            if id == taskId then
                table.remove(self._result, i)
                if table.isEmpty(self._result) then
                    table.insert(self._result, 1, v)
                else
                    table.insert(self._result, 2, v)
                end
                break
            end
        end
    end
end

function TaskManager:AddTaskToMenuTaskData(task, priority)
    local eventItemData = TaskCommonManager:GetNextEvent(task)
    if priority == 1 then--最高优先级
        table.insert(self._result, 1, TracingPointData(task, eventItemData))
    else
        local add = false
        for i,v in ipairs(self._result) do
            local thisPriority = self:GetPriority(v:GetTask())
            if thisPriority >= priority then
                add = true
                table.insert(self._result, i, TracingPointData(task, eventItemData))
                break
            end 
        end
        if not add then
            table.insert(self._result, TracingPointData(task, eventItemData))
        end
    end
end

function TaskManager:GetPriority(task)
    if task == nil then return nil end
    local taskType = task:GetTaskItemType()
    if taskType == TaskConfig.TASK_TYPE_COMMON_TASK then
        taskType = task:GetTaskType()
    end
    return TaskConfig.TASK_TYPE_PRIORITY_MAP[taskType]
end

function TaskManager:GetTaskPos(taskId)
    for i, v in ipairs(self._result) do
        if v:GetTask():GetId() == taskId then
            return i
        end
    end
    return 0
end

function TaskManager:GetUpdateTasksForMainMenu()
    -- LoggerHelper.Error("3====任务信息=====" .. PrintTable:TableToStr(self._result))
    return self._result
end

function TaskManager:GetTasksForMainMenu()
    --顺序如下：
    -- 1.引导任务
    -- 2.主线任务
    -- 3.转职任务
    -- 4.护送任务
    -- 5.日常任务(赏金任务)
    -- 6.周常任务
    -- 7.支线任务
    self._result = {}
    for i,v in ipairs(TaskConfig.PRIORITY_MAP) do
        if v == TaskConfig.TASK_TYPE_MAIN then
            self:AddMainTask()
        elseif v == TaskConfig.TASK_TYPE_VOCATIONCHANGE then
            self:AddVocationTask()
        elseif v == TaskConfig.TASK_TYPE_GUARD_GODDESS then
            self:AddGuardGoddessTask()
        elseif v == TaskConfig.TASK_TYPE_CIRCLETASK then
            self:AddCircleTask()
        elseif v == TaskConfig.TASK_TYPE_WEEK_TASK then
            self:AddWeekTask()
        elseif v == TaskConfig.TASK_TYPE_COMMON_BRANCH then
            self:AddBranchTask()
        end
    end
    
    return self._result
end

function TaskManager:GetNoMainTask()
    return self._noMainTask
end


function TaskManager:SetNoMainTask(flag)
    self._noMainTask = flag
end

function TaskManager:GetMainTaskNil()
    return self._nilMainTask
end

function TaskManager:SetMainTaskNil(flag)
    self._nilMainTask = flag
end

function TaskManager:GetAgainMainTask()
    return self._againMainTask
end

function TaskManager:SetAgainMainTask(flag)
    self._againMainTask = flag
end

function TaskManager:AddMainTask()
    local mainTask = TaskData:GetMainTask()
    --2.主线任务
    if mainTask ~= nil then
        local eventItemData = TaskCommonManager:GetNextEvent(mainTask)
        table.insert(self._result, TracingPointData(mainTask, eventItemData))
    else
        self:SetNoMainTask(true)
        self:SetMainTaskNil(true)
        local task = self:GetNextTask()
        local eventItemData = TaskCommonManager:GetNextEvent(task)
        table.insert(self._result, TracingPointData(task, eventItemData))
    end
end

function TaskManager:AddVocationTask()
    --3.转职任务
    local vocationTask = self:GetVocationTask()
    if table.isEmpty(vocationTask) then
        local VocationChangeManager = PlayerManager.VocationChangeManager
        if VocationChangeManager:IsDoingVocationChange() then
            local task = VocationChangeManager:GetNextStepTask()
            local eventItemData = TaskCommonManager:GetNextEvent(task)
            table.insert(self._result, TracingPointData(task, eventItemData))
        end
    else
        for i, task in pairs(vocationTask) do
            local eventItemData = TaskCommonManager:GetNextEvent(task)
            table.insert(self._result, TracingPointData(task, eventItemData))
        end
    end
end

function TaskManager:AddGuardGoddessTask()
    --4.护送任务
    local escortPeri = PlayerManager.EscortPeriManager:GetCurTaskData()
    if escortPeri ~= nil then
        table.insert(self._result, TracingPointData(escortPeri, nil))
    end
end

function TaskManager:AddCircleTask()
    --5.日常任务
    local circleTask = PlayerManager.CircleTaskManager:GetCircleTask()
    if circleTask ~= nil then
        local eventItemData = TaskCommonManager:GetNextEvent(circleTask)
        table.insert(self._result, TracingPointData(circleTask, eventItemData))
    end
end

function TaskManager:AddWeekTask()
    --6.周常任务（公会任务）
    local weekTask = PlayerManager.WeekTaskManager:GetWeekTask()
    if weekTask ~= nil then
        local eventItemData = TaskCommonManager:GetNextEvent(weekTask)
        table.insert(self._result, TracingPointData(weekTask, eventItemData))
    end
end

function TaskManager:AddBranchTask()
    --7.支线任务
    local branchTask = self:GetBranchTask()
    for i, task in pairs(branchTask) do
        local eventItemData = TaskCommonManager:GetNextEvent(task)
        table.insert(self._result, TracingPointData(task, eventItemData))
    end

    --8.即将开启的支线任务
    local commitBranchTask = self:GetCommitedOptionalTask()
    --LoggerHelper.Error("commitBranchTask ====" .. PrintTable:TableToStr(commitBranchTask))
    for taskId, _ in pairs(commitBranchTask) do
        local task = TaskData:NewTaskItemData(taskId, nil)
        if task:GetTaskType() == public_config.TASK_TYPE_BRANCH then
            --LoggerHelper.Error("现在的任务id =====" .. tostring(taskId))
            local next = TaskCommonManager:GetNextOpenSoonTask(taskId)
            if not table.isEmpty(next) then
                local nextTask = next[TaskConfig.TASK_CHANGE_INFO_TASK_DATA]
                local eventItemData = TaskCommonManager:GetNextEvent(nextTask)
                table.insert(self._result, TracingPointData(nextTask, eventItemData))
            end
        end
    end
end

-- function TaskManager:UpdateCurMapTrackTask()
--     local curMapTrackTask = {}
--     local mapId = GameSceneManager:GetCurrMapID()
--     local tracingMapPoints = self:GetTracingMapPoints(mapId)
--     for k, tracingMapPoint in pairs(tracingMapPoints) do
--         local task = tracingMapPoint:GetTask()
--         table.insert(curMapTrackTask, tracingMapPoint)
--     end
--     local npcs = GameSceneManager:GetNPCInfo(mapId)
--     for npcId, position in pairs(npcs) do
--         local task, eventItemData = self:GetNPCTask(npcId)
--         if task ~= nil and eventItemData ~= nil and task:GetIsTrack() then
--             table.insert(curMapTrackTask, TracingPointData(task, eventItemData, position))
--         end
--     end
--     TaskData:SetCurMapTrackingTask(curMapTrackTask)
-- end

function TaskManager:GetTrackedTasks()
    local trackedTasks = {}
    local task = TaskData:GetMainTask()
    if task ~= nil and task:GetIsTrack() then
        table.insert(trackedTasks, task)
    end
    local optionalTask = TaskData:GetOptionalTask()
    for k, v in pairs(optionalTask) do
        if v ~= nil and v:GetIsTrack() then
            table.insert(trackedTasks, v)
        end
    end
    return trackedTasks
end

function TaskManager:GetUntrackedOptionalTasks()
    local trackedTasks = {}
    -- local task = TaskData:GetMainTask()
    -- if task ~= nil and task:GetIsTrack() then
    --     table.insert(trackedTasks, task)
    -- end
    local optionalTask = TaskData:GetOptionalTask()
    for k, v in pairs(optionalTask) do
        if v ~= nil and v:GetIsTrack() == false then
            table.insert(trackedTasks, v)
        end
    end
    return trackedTasks
end

function TaskManager:CanAcceptTask(task)
    if task:GetSelfLimit() > GameWorld.Player().level then
        return false
    else
        return true
    end
end

function TaskManager:GetNPCTasks(npcID)
    local tasks = TaskData:GetTasks()
    local taskList = {}
    local eventItemList = {}
    for k, task in pairs(tasks) do
        local state = task:GetState()
        -- LoggerHelper.Log("Ash: GetNPCTask:" .. npcID .. " " .. PrintTable:TableToStr(task))
        if state == public_config.TASK_STATE_ACCEPTABLE and task:GetAcceptNPC() == npcID then
            taskList[task:GetId()] = task
            eventItemList[task:GetId()] = TaskCommonManager:GetDialogEvent(task, npcID)
        elseif state == public_config.TASK_STATE_COMPLETED and task:GetCommitNPC() == npcID then
            taskList[task:GetId()] = task
            eventItemList[task:GetId()] = TaskCommonManager:GetDialogEvent(task, npcID)
        elseif state == public_config.TASK_STATE_ACCEPTED then
            local eventItemData = TaskCommonManager:GetDialogEvent(task, npcID)
            if eventItemData ~= nil then
                taskList[task:GetId()] = task
                eventItemList[task:GetId()] = eventItemData
            end
        end
    end
    return taskList, eventItemList
end

function TaskManager:GetNPCTask(npcID)
    local tasks = TaskData:GetTasks()
    for k, task in pairs(tasks) do
        local state = task:GetState()
        -- LoggerHelper.Log("Ash: GetNPCTask:" .. npcID .. " " .. PrintTable:TableToStr(task))
        if state == public_config.TASK_STATE_ACCEPTABLE and task:GetAcceptNPC() == npcID then
            return task, TaskCommonManager:GetDialogEvent(task, npcID)
        elseif state == public_config.TASK_STATE_COMPLETED and task:GetCommitNPC() == npcID then
            return task, TaskCommonManager:GetDialogEvent(task, npcID)
        elseif state == public_config.TASK_STATE_ACCEPTED then
            local eventItemData = TaskCommonManager:GetDialogEvent(task, npcID)
            if eventItemData ~= nil then
                return task, eventItemData
            end
        end
    end
    return nil
end

function TaskManager:ShowMainDialog(npcId)
    -- LoggerHelper.Error("TaskManager:ShowMainDialog(npcId)")
    if not GUIManager.CheckPanelHasOpen() then
        GUIManager.ShowPanel(GameConfig.PanelsConfig.NPCDialog, npcId)
    else        
        self._needLaterRun = true
        -- self._needLaterRun = TaskCommonManager:CurTaskIsMainTask()
        -- if self._needLaterRun then
        self._laterMainTask = TaskCommonManager:GetCurAutoTask()
        -- end
        -- LoggerHelper.Error("有窗口打开中，不能进行任务， 任务id =====" .. tostring(self._laterMainTask:GetId()))
        -- LoggerHelper.Error("任务类型 =====" .. tostring(self._laterMainTask:GetTaskItemType()))        
    end
end

function TaskManager:OnHandleClosePanel()
    if self._needLaterRun and (not GUIManager.CheckPanelHasOpen()) and self._laterMainTask then
        -- LoggerHelper.Error("继续任务")
        self._needLaterRun = false
        -- TaskCommonManager:OnManualHandleTask(TracingPointData(self._laterMainTask))
        self:ClearTimer()
        self._continueTimer = TimerHeap:AddSecTimer(0.1, 0, 0, self._onContinueTask)
    end
end

function TaskManager:ClearTimer()
    if self._continueTimer then
        TimerHeap:DelTimer(self._continueTimer)
        self._continueTimer = nil
    end
end

function TaskManager:ContinueTask()
    if (not PlayerActionManager:IsDoingAction()) then
        TaskCommonManager:UpdateAutoTask()
    end
end

-- function TaskManager:WaitForCommitTask(taskId)
--     local npcs = GameSceneManager:GetNPCs()
--     local task = self:GetTaskById(taskId)
--     if npcs == nil or task == nil then
--         return
--     end
--     local npc = npcs[task:GetCommitNPC()]
--     if npc ~= nil then
--         if Vector3.Distance(GameWorld.Player():GetPosition(), npc:GetPosition()) <= tonumber(GlobalParamsHelper.GetParamValue(30)) then
--             npc:Ask()
--         end
--     end
-- end
function TaskManager:AcceptTask(taskId)
    GameWorld.Player().server.task_action_req(action_config.TASK_ACCEPT, tostring(taskId))
    EventDispatcher:TriggerEvent(GameEvents.ACCEPT_TASK_CALLBACK, taskId)
end

function TaskManager:CommitTask(taskId)
    -- LoggerHelper.Error("提交任务 ===" .. taskId, true)
    GameWorld.Player().server.task_action_req(action_config.TASK_COMMIT, tostring(taskId))
    EventDispatcher:TriggerEvent(GameEvents.COMMIT_TASK_CALLBACK, taskId)
end

function TaskManager:HandOverItem(items)
    local para = "";
    for i, v in ipairs(items) do
        para = para .. v:GetItemId() .. "," .. v:GetCount()
        if i ~= #items then
            para = para .. ","
        end
    end
    -- LoggerHelper.Log("Ash: HandOverItem " .. para)
    GameWorld.Player().server.give_item_to_npc_req(para)
end

function TaskManager:DoEvent(eventItemData)
-- GameWorld.Player().server.client_trigger_event(tostring(public_config.EVENT_ID_CLIENT_NPC_DIALOG_FINISH),
--     tostring(public_config.EVENT_EVENT_COND_ID) .. ":" .. tostring(eventItemData:GetId()))
end

function TaskManager:OnClientEntityCollectComplete(entityCfgId, eventId)
    -- LoggerHelper.Error("TaskManager:OnClientEntityCollectComplete " .. entityCfgId .. " " .. eventId)
    GameWorld.Player().server.client_trigger_event(tostring(public_config.EVENT_ID_CLIENT_PICK_FINISH),
        tostring(public_config.EVENT_PARAM_CLIENT_COLLECT_ID) .. ":" .. tostring(entityCfgId))
end

function TaskManager:OnTaskEventTrigger(id, task_event_id)
    -- LoggerHelper.Log("Ash: OnTaskEventTrigger " .. tostring(id) .. " " .. tostring(task_event_id))
    GameWorld.Player().server.client_trigger_event(tostring(public_config.EVENT_ID_CLIENT_GOTO_POS),
        tostring(public_config.EVENT_PARAM_CLIENT_GOTO_POS_ID) .. ":" .. tostring(task_event_id))
end

function TaskManager:ChangeTrackMark(taskId, flag)
    local task = self:GetTaskById(taskId)
    if task:GetIsTrack() ~= flag then
        -- LoggerHelper.Log("Ash: ChangeTrackMark " .. tostring(taskId) .. " " .. tostring(flag))
        GameWorld.Player().server.task_action_req(action_config.TASK_SET_TRACK, tostring(taskId) .. "," .. (flag and "1" or "0"))
    end
end

function TaskManager:GetNormalTaskMenuData()
    local datas = {}
    local mainTaskData = {}
    local optionalTaskData = {}
    table.insert(mainTaskData, {["text"] = LanguageDataHelper.CreateContent(10502)})
    table.insert(optionalTaskData, {["text"] = LanguageDataHelper.CreateContent(10503)})
    
    local mainTask = TaskData:GetMainTask()
    if mainTask ~= nil then
        local eventItemData = TaskCommonManager:GetNextEvent(mainTask)
        table.insert(mainTaskData, TracingPointData(mainTask, eventItemData))
    else
        local task = self:GetNextTask()
        local eventItemData = TaskCommonManager:GetNextEvent(task)
        table.insert(mainTaskData, TracingPointData(task, eventItemData))
    end
    
    local branchTask = self:GetBranchTask()
    for i, task in pairs(branchTask) do
        local eventItemData = TaskCommonManager:GetNextEvent(task)
        table.insert(optionalTaskData, TracingPointData(task, eventItemData))
    end
    
    local commitBranchTask = self:GetCommitedOptionalTask()
    for taskId, _ in pairs(commitBranchTask) do
        local task = TaskData:NewTaskItemData(taskId, nil)
        if task:GetTaskType() == public_config.TASK_TYPE_BRANCH then
            local next = TaskCommonManager:GetNextOpenSoonTask(taskId)
            if not table.isEmpty(next) then
                local nextTask = next[TaskConfig.TASK_CHANGE_INFO_TASK_DATA]
                local eventItemData = TaskCommonManager:GetNextEvent(nextTask)
                table.insert(optionalTaskData, TracingPointData(nextTask, eventItemData))
            end
        end
    end
    table.insert(datas, mainTaskData)
    table.insert(datas, optionalTaskData)
    return datas
end

function TaskManager:IsInitTaskComplete()
    return self._initTaskComplete
end

TaskManager:Init()
return TaskManager
