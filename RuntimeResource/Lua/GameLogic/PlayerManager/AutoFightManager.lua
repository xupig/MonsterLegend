local AutoFightManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local PlayerActionManager = GameManager.PlayerActionManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function AutoFightManager:Init()

end

function AutoFightManager:OnPlayerEnterWorld()
    self._stickBeginDrag = function() self:StickBeginDrag() end
    EventDispatcher:AddEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    self._stickEndDrag = function() self:StickEndDrag() end
    EventDispatcher:AddEventListener(GameEvents.Stick_End_Drag, self._stickEndDrag)
    self._stickReset = function() self:StickEndDrag() end
    EventDispatcher:AddEventListener(GameEvents.Stick_Reset, self._stickReset)
    self._onPlayerSkillButton = function() self:OnPlayerSkillButton() end
    EventDispatcher:AddEventListener(GameEvents.PlayerCommandPlaySkill, self._onPlayerSkillButton)

    self._check = false
    self._checkTime = GlobalParamsHelper.GetParamValue(882)
    self._lastTime = Time.realtimeSinceStartup 
    self._updata = function() self:Update() end
    self._timer = TimerHeap:AddSecTimer(0, 1, 0, self._updata)
end

function AutoFightManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    EventDispatcher:RemoveEventListener(GameEvents.Stick_End_Drag, self._stickEndDrag)
    EventDispatcher:RemoveEventListener(GameEvents.Stick_Reset, self._stickReset)
    EventDispatcher:RemoveEventListener(GameEvents.PlayerCommandPlaySkill, self._onPlayerSkillButton)

    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
    end
end

function AutoFightManager:StickBeginDrag()
    self._check = false
    GameWorld.Player():StopAutoFight(true)
    -- GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopFindPosition()
end

function AutoFightManager:StickEndDrag()
    self._lastTime = Time.realtimeSinceStartup
    self._check = true
end

function AutoFightManager:OnPlayerSkillButton()
    --GameWorld.Player():StopAutoFight()
    --PlayerActionManager:StopFindPosition()
end

function AutoFightManager:Update()
    if GameWorld.Player() == nil or not self._check or not GameWorld.Player().temporaryAutoFight then
        return
    end
    if Time.realtimeSinceStartup - self._lastTime > self._checkTime and GameWorld.Player().temporaryAutoFight then
        self._check = false
        GameWorld.Player():SetTemporaryAutoFight(false)
        GameWorld.Player():StartAutoFight()
    end
end

AutoFightManager:Init()
return AutoFightManager