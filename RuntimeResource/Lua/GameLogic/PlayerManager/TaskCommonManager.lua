local TaskCommonManager = {}

local public_config = GameWorld.public_config
local WorldMapDataHelper = GameDataHelper.WorldMapDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local MapManager = PlayerManager.MapManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskConfig = GameConfig.TaskConfig
local MenuConfig = GameConfig.MenuConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local TaskData = PlayerDataManager.taskData
local TaskDataHelper = GameDataHelper.TaskDataHelper
local EventDataHelper = GameDataHelper.EventDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local BagManager = PlayerManager.BagManager
local SceneConfig = GameConfig.SceneConfig
local ClientEntityManager = GameManager.ClientEntityManager
local GameSceneManager = GameManager.GameSceneManager
local PlayerActionManager = GameManager.PlayerActionManager
local CollectManager = GameManager.CollectManager
local ChangeVocationDataHelper = GameDataHelper.ChangeVocationDataHelper
local CircleTaskManager
local WeekTaskManager
local DramaManager

function TaskCommonManager:Init()
    self._playingCG = false
    self._initTaskComplete = false
    self._taskInitList = {
        [TaskConfig.TASK_TYPE_COMMON_TASK] = false, --一般任务，主线、支线、转职
        -- [TaskConfig.TASK_TYPE_GUARD_GODDESS] = false,               --护送女神
        [TaskConfig.TASK_TYPE_CIRCLETASK] = false, --赏金任务
        [TaskConfig.TASK_TYPE_WEEK_TASK] = false --公会任务
    }
    
    self._pvpCGNames = GlobalParamsHelper.GetParamValue(962)

    self._stickBeginDrag = function()self:StickBeginDrag() end
    self._circleTaskListInit = function()self:TaskInitList(TaskConfig.TASK_TYPE_CIRCLETASK) end
    self._commonTaskListInit = function()self:TaskInitList(TaskConfig.TASK_TYPE_COMMON_TASK) end
    self._weekTaskListInit = function()self:TaskInitList(TaskConfig.TASK_TYPE_WEEK_TASK) end
    self._taskStartCG = function()self:StartPlayCG() end
    self._taskEndCG = function(_,name)self:EndPlayCG(_,name) end
    self._onAICheckTask = function()self:OnAICheckTask() end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
    self._onFunctionOpenStart = function()self:OnPauseAutoTask() end
    self._onFunctionOpenEnd = function()self:OnReStartAutoTask() end
    self._onSkillOpenStart = function()self:OnPauseAutoTask() end
    self._onSkillOpenEnd = function()self:OnReStartAutoTask() end
    self._onCGContinueTask = function()self:CGContinueTask() end
end

function TaskCommonManager:OnPlayerEnterWorld()
    CircleTaskManager = PlayerManager.CircleTaskManager
    WeekTaskManager = PlayerManager.WeekTaskManager
    DramaManager = GameManager.DramaManager

    EventDispatcher:AddEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    EventDispatcher:AddEventListener(GameEvents.CIRCLE_TASK_LIST, self._circleTaskListInit)
    EventDispatcher:AddEventListener(GameEvents.TASK_INIT_COMPLETE, self._commonTaskListInit)
    EventDispatcher:AddEventListener(GameEvents.WEEK_TASK_LIST, self._weekTaskListInit)
    EventDispatcher:AddEventListener(GameEvents.TASK_CG_START, self._taskStartCG)
    EventDispatcher:AddEventListener(GameEvents.DramaPlayEnd, self._taskEndCG)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:AddEventListener(GameEvents.FUNCITON_OPEN_START, self._onFunctionOpenStart)
    EventDispatcher:AddEventListener(GameEvents.FUNCITON_OPEN_END, self._onFunctionOpenEnd)
    EventDispatcher:AddEventListener(GameEvents.SKILL_OPEN_START, self._onSkillOpenStart)
    EventDispatcher:AddEventListener(GameEvents.SKILL_OPEN_END, self._onSkillOpenEnd)
end

function TaskCommonManager:OnPlayerLeaveWorld()
    self._playingCG = false
    self._initTaskComplete = false
    self:ClearTimer()
    
    EventDispatcher:RemoveEventListener(GameEvents.Stick_Begin_Drag, self._stickBeginDrag)
    EventDispatcher:RemoveEventListener(GameEvents.CIRCLE_TASK_LIST, self._circleTaskListInit)
    EventDispatcher:RemoveEventListener(GameEvents.TASK_INIT_COMPLETE, self._commonTaskListInit)
    EventDispatcher:RemoveEventListener(GameEvents.WEEK_TASK_LIST, self._weekTaskListInit)
    EventDispatcher:RemoveEventListener(GameEvents.TASK_CG_START, self._taskStartCG)
    EventDispatcher:RemoveEventListener(GameEvents.DramaPlayEnd, self._taskEndCG)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
    EventDispatcher:RemoveEventListener(GameEvents.FUNCITON_OPEN_START, self._onFunctionOpenStart)
    EventDispatcher:RemoveEventListener(GameEvents.FUNCITON_OPEN_END, self._onFunctionOpenEnd)
    EventDispatcher:RemoveEventListener(GameEvents.SKILL_OPEN_START, self._onSkillOpenStart)
    EventDispatcher:RemoveEventListener(GameEvents.SKILL_OPEN_END, self._onSkillOpenEnd)
end

function TaskCommonManager:OnEnterMap(mapId)    
    --LoggerHelper.Error("TaskCommonManager:OnEnterMap(mapId) === " .. tostring(mapId))
    if self:CheckIsInCanAutoTaskMap() then
        -- LoggerHelper.Error("自动任务10010101010")
        if not DramaManager:HasDramaRunning() then
            self:UpdateAutoTask()
        end
    end
end

function TaskCommonManager:OnPauseAutoTask()
    if not self:CheckIsInCanAutoTaskMap() then
        return
    end
    
    if self._curAutoTask ~= nil then
        self._pauseAutoTask = self._curAutoTask
        self._pauseAutoTarget = self._curAutoTarget
        self:SetCurAutoTask(nil, nil)
        GameWorld.Player():StopAutoFight("OnPauseAutoTask" .. tostring(GameSceneManager:GetCurrMapID()))
        GameWorld.Player():StopMoveWithoutCallback()
        PlayerActionManager:StopAllAction()
    end
end

function TaskCommonManager:OnReStartAutoTask()
    if self._pauseAutoTask ~= nil then
        self:SetCurAutoTask(self._pauseAutoTask, self._pauseAutoTarget)
        self._pauseAutoTask = nil
        self._pauseAutoTarget = nil
        self:UpdateAutoTask()
    end
end

function TaskCommonManager:OnLeaveMap(mapId)
    self:ClearTimer()
    local player = GameWorld.Player()
    if player then
        player:StopAutoFight()
        -- LoggerHelper.Error("OnLeaveMap: " .. mapId)
        PlayerActionManager:StopFindPosition()
    -- player:StopMoveWithoutCallback()
    end
    GUIManager.ClosePanel(PanelsConfig.NPCDialog)
    self._laterTrackTask, self._laterUseLittleShoes = nil, nil
    EventDispatcher:RemoveEventListener(GameEvents.AICheckTask, self._onAICheckTask)
end

function TaskCommonManager:TaskInitList(type)
    self._taskInitList[type] = true
    
    local flag = true
    for k, v in pairs(self._taskInitList) do
        if v == false then
            flag = false
            break
        end
    end
    if flag then
        self._initTaskComplete = true
        EventDispatcher:TriggerEvent(GameEvents.TASK_LIST_INIT_COMPLETE)
        EventDispatcher:RemoveEventListener(GameEvents.CIRCLE_TASK_LIST, self._circleTaskListInit)
        EventDispatcher:RemoveEventListener(GameEvents.TASK_INIT_COMPLETE, self._commonTaskListInit)
        EventDispatcher:RemoveEventListener(GameEvents.WEEK_TASK_LIST, self._weekTaskListInit)
        
        PlayerManager.TaskManager:GetTasksForMainMenu()
    -- EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
    end
end

function TaskCommonManager:IsInitComplete()
    return self._initTaskComplete
end

function TaskCommonManager:StickBeginDrag()
    self:SetCurAutoTask(nil, nil)
end

function TaskCommonManager:StartPlayCG()
    --LoggerHelper.Error("TaskCommonManager:StartPlayCG()", true)
    local drameName = DramaManager:GetRunningDrameName()
    if drameName and table.containsValue(self._pvpCGNames, drameName) then        
        return
    end

    self._playingCG = true
    if self:IsInExpInstance() then
        return
    end
    
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
end

function TaskCommonManager:EndPlayCG(_,name)
    -- LoggerHelper.Error("TaskCommonManager:EndPlayCG()", true)
    self._playingCG = false

    local drameName = DramaManager:GetRunningDrameName()
    if drameName and table.containsValue(self._pvpCGNames, drameName) then        
        return
    end
    
    if name == "drama_guide__FirstTask" then
        self:RunFirstMainTask()
        return
    end

    if self:IsInExpInstance() then
        return
    end
    -- self:UpdateAutoTask()
    self:ClearTimer()
    self._continueTimer = TimerHeap:AddSecTimer(0.1, 0, 0, self._onCGContinueTask)
end

function TaskCommonManager:ClearTimer()
    if self._continueTimer then
        TimerHeap:DelTimer(self._continueTimer)
        self._continueTimer = nil
    end
end

function TaskCommonManager:CGContinueTask()
    if (not PlayerActionManager:IsDoingAction()) and (not GUIManager.PanelIsActive(PanelsConfig.NPCDialog)) then
        self:UpdateAutoTask()
    end
end

function TaskCommonManager:RunMainTask()
    local mainTask = TaskData:GetMainTask()
    if mainTask then
        local eventItemData = self:GetNextEvent(mainTask)
        self:SetCurAutoTask(mainTask, eventItemData)
        self:UpdateAutoTask()
        return true
    end
end

function TaskCommonManager:RunFirstMainTask()
    local mainTask = TaskData:GetMainTask()
    if mainTask and mainTask:GetId() == 1 then
        --第一个任务自动执行  
        local eventItemData = self:GetNextEvent(mainTask)
        self:SetCurAutoTask(mainTask, eventItemData)
        self:UpdateAutoTask()
        return true
    end
end

function TaskCommonManager:SetCurAutoTask(task, target)
    --LoggerHelper.Error("TaskCommonManager:SetCurAutoTask(task, target)" .. tostring(task))
    --LoggerHelper.Error("TaskCommonManager:SetCurAutoTask(task, target)" .. tostring(target))
    self._curAutoTask = task
    self._curAutoTarget = target
end

function TaskCommonManager:GetCurAutoTask()
    return self._curAutoTask, self._curAutoTarget
end

--当前任务是否主线任务
function TaskCommonManager:CurTaskIsMainTask()
    local curAutoTask = self:GetCurAutoTask()
    if curAutoTask then
        if curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_COMMON_TASK and curAutoTask:GetTaskType() == public_config.TASK_TYPE_MAIN then
            return true
        end
    end
    return false
end

function TaskCommonManager:UpdateAutoTask()
    local curAutoTask = self:GetCurAutoTask()
    if curAutoTask then
        -- local player = GameWorld.Player()
        -- if player then
        --     player:StopAutoFight()
        -- end
        if curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_CIRCLETASK then
            CircleTaskManager:HandleAutoTask()
        elseif curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_GUARD_GODDESS then
            PlayerManager.EscortPeriManager:HandleAutoTask()
        elseif curAutoTask:GetTaskItemType() == TaskConfig.TASK_TYPE_WEEK_TASK then
            WeekTaskManager:HandleAutoTask()
        else
            PlayerManager.TaskManager:HandleAutoTask()
        end
    end
end

function TaskCommonManager:IsInExpInstance()
    local sceneType = GameSceneManager:GetCurSceneType()
    return sceneType == SceneConfig.SCENE_TYPE_EXP_INSTANCE
end

function TaskCommonManager:CheckIsInCanAutoTaskMap()
    -- self._checkAutoTaskWhenEnterMap = self._sceneType == SceneConfig.SCENE_TYPE_COMBAT
    -- return self._sceneType ~= SceneConfig.SCENE_TYPE_COMBAT
    local sceneType = GameSceneManager:GetCurSceneType()
    return (sceneType == SceneConfig.SCENE_TYPE_MAIN or sceneType == SceneConfig.SCENE_TYPE_WILD) and (not DramaManager:HasDramaRunning())
end

function TaskCommonManager:GetNextEvent(task)
    local taskState = task:GetState()
    -- if taskState ~= public_config.TASK_STATE_ACCEPTABLE then --未接取任务不显示
    local targetsInfo, targetsTime = task:GetTargetsInfo(), task:GetTargetsCurTimes()
    local eventItemData = nil
    for i, event in ipairs(targetsInfo) do
        local id = event:GetId()
        if taskState == public_config.TASK_STATE_COMPLETED
            or taskState == public_config.TASK_STATE_ACCEPTABLE then
            eventItemData = event
        else
            if event:GetTotalTimes() > (targetsTime[id] or 0) then
                eventItemData = event
                break
            end
        end
    end
    return eventItemData
end

function TaskCommonManager:OnAICheckTask()
    -- LoggerHelper.Error("Ash: OnAICheckTask ")
    self:OnClickTrackCurTask(self._laterTrackTask, self._laterUseLittleShoes, true)
    self._laterTrackTask, self._laterUseLittleShoes = nil, nil
    EventDispatcher:RemoveEventListener(GameEvents.AICheckTask, self._onAICheckTask)
-- LoggerHelper.Error("取消监听AICheckTask")
end

function TaskCommonManager:CheckDoTaskLater(trackTask, useLittleShoes)
    if not self:CheckIsInCanAutoTaskMap() then
        -- LoggerHelper.Error("在战斗副本，停止自动任务")
        return true
    end
    
    if self._playingCG then
        -- GUIManager.ShowText(1, "播放CG中: " .. tostring(GameManager.DramaManager:GetRunningDrameName()))
        if GameWorld.isEditor then
            LoggerHelper.Error("播放CG中: " .. tostring(DramaManager:GetRunningDrameName()))
        end
        return true
    end
    
    --先判断是否有东西可以拾取
    if ClientEntityManager.HasSpaceDrop(GlobalParamsHelper.GetParamValue(831)) then
        EventDispatcher:RemoveEventListener(GameEvents.AICheckTask, self._onAICheckTask)
        EventDispatcher:AddEventListener(GameEvents.AICheckTask, self._onAICheckTask)
        GameWorld.Player():StopAutoFight()
        GameWorld.Player():StartAutoFight()
        self._laterTrackTask, self._laterUseLittleShoes = trackTask, useLittleShoes
        return true
    end
    
    return false
end

function TaskCommonManager:OnManualHandleTask(trackTask, useLittleShoes, handClick)
    self:OnClickTrackCurTask(trackTask, useLittleShoes, true, handClick)
end

function TaskCommonManager:OnClickTrackCurTask(trackTask, useLittleShoes, forceDoTask, handClick)
    if trackTask ~= nil then
        -- LoggerHelper.Error("任务id =====" .. tostring(trackTask:GetTask():GetId()), true)
        -- if forceDoTask then
        --     GameWorld.Player():StopAutoFight()
        -- else
        --     GameWorld.Player():StartAutoFight()
        -- end
        --开始任务前的检查（1.在副本中不能继续任务；2.播放CG时不能继续任务；3.开始任务前需要拾取道具后才开始任务）
        if not forceDoTask and self:CheckDoTaskLater(trackTask, useLittleShoes) then
            -- LoggerHelper.Error("停止自动任务 forceDoTask ==== " .. tostring(forceDoTask))
            return
        end
        local task = trackTask:GetTask()
        local target = trackTask:GetEventItemData()
        local taskItemType = task:GetTaskItemType()
        local taskType = task:GetTaskType()

        self._lastAutoTask, self._lastAutoTarget = self:GetCurAutoTask()
        if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and taskType == public_config.TASK_TYPE_VOCATION then
            local taskId = task:GetId()
            local id = ChangeVocationDataHelper:GetIdByTaskId(taskId)
            if id and ChangeVocationDataHelper:GetUIShow(id) == 1 and handClick ~= TaskConfig.VOCATION_TO_TASK and 
                (self._lastAutoTask == nil or self._lastAutoTask:GetId() ~= taskId) then
                -- LoggerHelper.Error("Error", true)
                GUIManager.ShowPanel(PanelsConfig.VocationChange)
                return
            end
        end

        if taskItemType == TaskConfig.TASK_TYPE_GUARD_GODDESS then
            GameWorld.Player():StopAutoFight()
            GameWorld.Player():StopMoveWithoutCallback()
            PlayerActionManager:StopAllAction()
            self:SetCurAutoTask(nil, nil)
            self:GoToNpc(GlobalParamsHelper.GetParamValue(530), false, true)
            return
        end
        
        if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and taskType == public_config.TASK_TYPE_VOCATION and
            --做四转任务时
            GameWorld.Player():GetVocationChange() == 3 then
            GUIManager.ShowPanel(PanelsConfig.VocationChange)
            return
        end
        
        if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and (not task:IsOpen()) then
            --一般任务，主线、支线、转职,未达到开启等级
            self:SetCurAutoTask(nil, nil)
            return
        end
        
        if target == nil then
            target = self:GetNextEvent(task)
        -- LoggerHelper.Error("target ====" .. PrintTable:TableToStr(target))
        end
        self._lastAutoTask, self._lastAutoTarget = self:GetCurAutoTask()
        self:SetCurAutoTask(task, target)
        
        --判断当前任务状态
        local state = task:GetState()
        if state == public_config.TASK_STATE_ACCEPTABLE then
            local acceptNPC = task:GetAcceptNPC()
            if acceptNPC == nil or acceptNPC == 0 then
                return
            end
        end
        
        if state == public_config.TASK_STATE_COMPLETED then
            local commitNPC = task:GetCommitNPC()
            if commitNPC == nil or commitNPC == 0 then
                return
            elseif commitNPC == 666 then
                if handClick == TaskConfig.IS_HAND_CLICK then
                    self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
                    EventDispatcher:TriggerEvent(GameEvents.COMMIT_TASK, task:GetId())
                end
                return
            end
        end

        --如果是日常任务未接取则直接传送到接任务NPC旁边
        if state == public_config.TASK_STATE_ACCEPTABLE and taskItemType == TaskConfig.TASK_TYPE_CIRCLETASK and CircleTaskManager:NeedAcceptTask() then
            self:HandleFindNPC(task, target, TaskConfig.IS_TELEPORT_TO, CircleTaskManager:GetCircleTaskData():GetAcceptNPC())
            return
        end

        --如果是周常任务未接取则直接传送到接任务NPC旁边
        if state == public_config.TASK_STATE_ACCEPTABLE and taskItemType == TaskConfig.TASK_TYPE_WEEK_TASK and WeekTaskManager:NeedAcceptTask()  then
            self:HandleFindNPC(task, target, TaskConfig.IS_TELEPORT_TO, WeekTaskManager:GetWeekTaskData():GetAcceptNPC())
            return
        end

        local eventID = target:GetEventID()        

        --打世界boss
        if eventID == public_config.EVENT_ID_KILLED_WORLD_BOSS then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_WORLD_BOSS)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        --竞技场任务
        if eventID == public_config.EVENT_ID_SLAY_BOT then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanel(PanelsConfig.AthleticsEnter)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        --接取一个公会任务
        if eventID == public_config.EVENT_ID_ACCEPT_GUILD_TASK then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanel(PanelsConfig.DailyActivityEx)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        --加入一个公会
        if eventID == public_config.EVENT_ID_IN_GUILD then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanel(PanelsConfig.ApplyForGuild)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        --加好友
        if eventID == public_config.EVENT_ID_MOB_WITH then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_FRIEND)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        --世界发言
        if eventID == public_config.EVENT_ID_GOSSIP then
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = public_config.CHAT_CHANNEL_WORLD})
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end

        if eventID == public_config.EVENT_ID_GIVE_ITEM_TO_NPC then
            local follow = target:GetFollow()
            if follow and follow[1] ~= nil then --公会任务上交
                if handClick == TaskConfig.IS_HAND_CLICK then
                    local id = target:GetId()
                    local quality = EventDataHelper:GetConds(id, public_config.EVENT_PARAM_ITEM_QUALITY)--品质
                    local level = EventDataHelper:GetConds(id, public_config.EVENT_PARAM_ITEM_GRADE)--品阶
                    -- local data = BagManager:GetEquipByQulityAndLevel(quality, level)
                    local data = {}
                    data.quality = quality
                    data.level = level
                    GUIManager.ShowPanel(PanelsConfig.GuildTaskCommitEquip, data)
                    self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
                end
                return
            end
        end
        
        if eventID == public_config.EVENT_ID_PASS_INST then
            local follow = target:GetFollow()
            if table.isEmpty(follow) then
                self:EnterInst(target)
                return
            elseif follow and follow[1] ~= nil then
                --找NPC对话
                local nId = follow[1][1]
                if (self:HandleFindNPC(task, target, useLittleShoes, nId)) then
                    return
                end
            elseif follow and follow[2] ~= nil then
                --打开某功能id的窗口
                if handClick == TaskConfig.IS_HAND_CLICK then
                    self:OpenPanel(follow)
                    self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
                end
                return
            end
        end
        
        if eventID == public_config.EVENT_ID_GET_ITEM then
            local follow = target:GetFollow()
            if follow and follow[2] ~= nil then
                if handClick == TaskConfig.IS_HAND_CLICK then
                    self:OpenPanel(follow)
                    self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
                end
                return
            end
        end
        
        if eventID == public_config.EVENT_ID_KILL_MONSTER then
            local follow = target:GetFollow()
            if follow and follow[0] ~= nil then
                --打开某功能id的窗口
                -- self:OpenPanel(follow)
                -- GameWorld.Player():StopAutoFight()
                -- GameWorld.Player():StopMoveWithoutCallback()
                if handClick == TaskConfig.IS_HAND_CLICK then
                    self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
                    local functionId = follow[0][1]
                    if functionId ~= nil then
                        GUIManager.ShowPanelByFunctionId(functionId)
                    end
                end
                return
            end
        end
        
        if (self:HandleFindNPC(task, target, useLittleShoes)) then -- 找npc
            return
        end
        
        local mapId, pos = self:GetTaskMapPos(task, target)-- 找地图点
        if mapId ~= nil then
            if eventID == public_config.EVENT_ID_KILL_MONSTER
                or eventID == public_config.EVENT_ID_KILL_MONSTER_TEAM
                or eventID == public_config.EVENT_ID_GET_ITEM
                or eventID == public_config.EVENT_ID_HAS_ITEM_COUNT then
                EventDispatcher:TriggerEvent(GameEvents.PlayerCommandKillMonsterPoint, mapId, pos, 4, 10, useLittleShoes)
            elseif eventID == public_config.EVENT_ID_CLIENT_PICK_FINISH
                or eventID == public_config.EVENT_ID_CLIENT_PICK_FINISH_TEAM then
                --正在采集状态
                if CollectManager:GetCurClientCollectState() then
                    return
                end
                EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindCollectClientResource, mapId, pos, 0, useLittleShoes)
            elseif eventID == public_config.EVENT_ID_COLLECT then
                --正在采集状态
                if CollectManager:GetCurClientCollectState() then
                    return
                end
                EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindCollectClientResource, mapId, pos, 0, useLittleShoes)
            elseif eventID == public_config.EVENT_ID_CLIENT_GOTO_POS then
                EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindPosition, mapId, pos, useLittleShoes)
            end
            return
        end
        
        local funcId = self:GetOpenFuncId(target)--打开窗口
        if funcId ~= nil then
            -- GameWorld.Player():StopAutoFight()
            -- GameWorld.Player():StopMoveWithoutCallback()
            if handClick == TaskConfig.IS_HAND_CLICK then
                GUIManager.ShowPanelByFunctionId(funcId)
                self:SetCurAutoTask(self._lastAutoTask, self._lastAutoTarget)
            end
            return
        end
        
        if self:IsGuardGoddessEvent(target) then --护送事件
            GameWorld.Player():StopAutoFight()
            GameWorld.Player():StopMoveWithoutCallback()
            EventDispatcher:TriggerEvent(GameEvents.Daily_EscortPeri)
            return
        end
    
    -- LoggerHelper.Error("TaskCommonManager:HandleGoToNpc error" .. tostring(task and task:GetId() or nil))
    -- LoggerHelper.Error("TaskCommonManager status ===" .. tostring(task and task:GetState() or nil))
    end
end

function TaskCommonManager:OpenPanel(follow)
    -- GameWorld.Player():StopAutoFight()
    -- GameWorld.Player():StopMoveWithoutCallback()
    local functionId = follow[2][1]
    if functionId ~= nil then
        GUIManager.ShowPanelByFunctionId(functionId)
    end
end

function TaskCommonManager:EnterInst(eventItemData)
    -- LoggerHelper.Log("Ash: EnterInst: " .. PrintTable:TableToStr(eventItemData))
    -- GameWorld.Player():StopAutoFight()
    -- LoggerHelper.Error("TaskCommonManager 进入副本", true)
    local instId = eventItemData:GetConds(public_config.EVENT_PARAM_INST_ID)
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandPassInst, instId)
end

function TaskCommonManager:HandleFindNPC(task, target, useLittleShoes, nId)
    -- GameWorld.Player():StopAutoFight()
    local npcId = nId or self:GetTaskNPCId(task, target)
    -- LoggerHelper.Log("Ash: GetTaskNPCInfo npcId:" .. tostring(npcId) .. " task: " .. PrintTable:TableToStr(task))
    if npcId == nil or npcId == 0 then return false end
    return self:GoToNpc(npcId, useLittleShoes, false)
end

function TaskCommonManager:GoToNpc(npcId, useLittleShoes, onFootChangeMap)
    if useLittleShoes == nil then
        useLittleShoes = false
    end
    -- useLittleShoes = useLittleShoes == true
    local curMapId = GameSceneManager:GetCurrMapID()
    local curNpcs = MapManager:GetNPCByMapId(curMapId)
    if onFootChangeMap or GameWorld.Player().level <= GlobalParamsHelper.GetParamValue(846) then
        return self:DoFindNPC(curNpcs, curMapId, npcId, useLittleShoes, true)
    else
        return self:DoFindNPC(curNpcs, curMapId, npcId, useLittleShoes, false)
    end
    return false
end

function TaskCommonManager:DoFindNPC(curNpcs, curMapId, npcId, useLittleShoes, onFootChangeMap)
    local curTeles = nil
    if onFootChangeMap then -- 要求走路切场景就获取一下当前场景所有传送点
        curTeles = MapManager:GetTeleportInfo(curMapId)
    end
    --找一遍同场景的NPC看找不找到对应的NPC，逻辑需关闭强制走传送点，不然必定找不到
    local result = self:HandleGoToNpc(curNpcs, curTeles, curMapId, npcId, useLittleShoes, false)
    if result then
        return true
    else
        local mapId = NPCDataHelper.GetNPCLocation(npcId)
        -- local worldMapIds = WorldMapDataHelper:GetAllId()
        -- for i, worldMapId in ipairs(worldMapIds) do
        --     local mapId = WorldMapDataHelper:GetSceneId(worldMapId)
        if mapId ~= curMapId then
            -- LoggerHelper.Log("Ash: mapId:" .. mapId .. " npcId: " .. npcId .. " onFootChangeMap: " .. tostring(onFootChangeMap))
            local npcs = MapManager:GetNPCByMapId(mapId)
            local res = self:HandleGoToNpc(npcs, curTeles, mapId, npcId, useLittleShoes, onFootChangeMap)
            if res then
                return true
            end
        end
    -- end
    end
    return false
end

function TaskCommonManager:HandleGoToNpc(npcs, teleports, mapId, npcId, useLittleShoes, onFootChangeMap)
    if npcs and npcs[npcId] then
        local teleportPos = nil
        if onFootChangeMap and teleports then --如果强制走传送并且有传送点信息，必须找到可以走的传送点，不然不走
            for k, v in pairs(teleports) do
                if v.target_scene == mapId then
                    --有传送点，走传送点
                    teleportPos = v.pos
                    break
                end
            end
            if teleportPos == nil then --强制必须走路切场景
                return false
            end
        end
        EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindNPC, mapId, npcId, npcs[npcId], tonumber(GlobalParamsHelper.GetParamValue(30)), useLittleShoes, teleportPos)
        return true
    else
        return false
    end
end

function TaskCommonManager:GetTaskNPCId(task, target)
    local state = task:GetState()
    -- LoggerHelper.Log("Ash: GetTaskNPC:" .. PrintTable:TableToStr(task))
    if state == public_config.TASK_STATE_ACCEPTABLE then
        -- LoggerHelper.Log("Ash: GetTaskNPC GetAcceptNPC:" .. task:GetAcceptNPC())
        return task:GetAcceptNPC()
    elseif state == public_config.TASK_STATE_COMPLETED then
        -- LoggerHelper.Log("Ash: GetTaskNPC GetCommitNPC:" .. task:GetCommitNPC())
        return task:GetCommitNPC()
    elseif state == public_config.TASK_STATE_ACCEPTED then
        -- local targets = task:GetTargetsInfo()
        -- for i, target in ipairs(targets) do
        --     local id = target:GetId()
        --     local targetsCurTimes = task:GetTargetsCurTimes()
        --     if target:GetTotalTimes() > targetsCurTimes[id] then -- 未完成该事件
        if target ~= nil and self:IsDialogEvent(target) then
            local follow = target:GetFollow()
            -- LoggerHelper.Log("Ash: IsDialogEvent:" .. PrintTable:TableToStr(follow))
            if follow then
                local eventID = target:GetEventID()
                if eventID == public_config.EVENT_ID_PASS_INST then
                    if follow[1] and follow[1][1] then
                        return follow[1][1]
                    end
                else
                    for npcId, v in pairs(follow) do
                        return npcId
                    end
                end
            end
        --     end
        -- end
        end
    end
end

function TaskCommonManager:GetDialogEvent(task, npcID)
    local targets = task:GetTargetsInfo()
    if (task:GetState() == public_config.TASK_STATE_COMPLETED and task:GetCommitNPC() == npcID)
        or (task:GetState() == public_config.TASK_STATE_ACCEPTABLE and task:GetAcceptNPC() == npcID) then
        for i, target in ipairs(targets) do
            return target
        end
    end
    for i, target in ipairs(targets) do
        local id = target:GetId()
        local targetsCurTimes = task:GetTargetsCurTimes()
        -- LoggerHelper.Log("Ash: GetDialogEvent:" .. target:GetTotalTimes() .. " id: " .. id .. " " .. PrintTable:TableToStr(targetsCurTimes))
        if target:GetTotalTimes() > targetsCurTimes[id] then -- 未完成该事件
            local follow = target:GetFollow()
            if self:IsDialogEvent(target)
                and follow and follow[npcID] ~= nil then -- NPCID对应的对话事件
                return target
            end
        end
    end
end

function TaskCommonManager:IsDialogEvent(target)
    local eventID = target:GetEventID()
    if eventID == public_config.EVENT_ID_CLIENT_NPC_DIALOG_FINISH
        or eventID == public_config.EVENT_ID_GIVE_ITEM_TO_NPC
        or eventID == public_config.EVENT_ID_PASS_INST then
        return true
    else
        return false
    end
end

function TaskCommonManager:GetTaskMapPos(task, target)
    if self:IsMapPosEvent(target) then
        local follow = target:GetFollow()
        for mapId, pos in pairs(follow) do
            -- LoggerHelper.Log("Ash: GetTaskMapPos mapId:" .. mapId .. " pos: " .. PrintTable:TableToStr(pos))
            return mapId, Vector3(pos[1], pos[2], pos[3])
        end
    end
end

function TaskCommonManager:GetMapPosEvent(task, mapId)
    local targets = task:GetTargetsInfo()
    -- LoggerHelper.Log("Ash: GetMapPosEvent GetTargetsInfo:" .. mapId .. " targets: " .. PrintTable:TableToStr(targets))
    for i, target in ipairs(targets) do
        local id = target:GetId()
        local targetsCurTimes = task:GetTargetsCurTimes()
        -- LoggerHelper.Log("Ash: GetMapPosEvent target:GetTotalTimes():" .. target:GetTotalTimes() .. " targetsCurTimes[id]: " .. targetsCurTimes[id])
        if target:GetTotalTimes() > targetsCurTimes[id] then -- 未完成该事件
            local follow = target:GetFollow()
            -- LoggerHelper.Log("Ash: GetMapPosEvent follow:" .. PrintTable:TableToStr(follow))
            if self:IsMapPosEvent(target)
                and follow[mapId] ~= nil then
                return target
            end
        end
    end
end

function TaskCommonManager:GetOpenFuncId(target)
    if self:IsOpenFuncEvent(target) then
        local follow = target:GetFollow()
        for _, funcIds in pairs(follow) do
            -- LoggerHelper.Log("Ash: GetTaskMapPos mapId:" .. mapId .. " pos: " .. PrintTable:TableToStr(pos))
            return funcIds[1]
        end
    end
end

function TaskCommonManager:IsOpenFuncEvent(target)
    local eventID = target:GetEventID()
    if eventID == public_config.EVENT_ID_GET_ITEM
        or eventID == public_config.EVENT_ID_HAS_EQUIP_STREN_COUNT
        or eventID == public_config.EVENT_ID_PASS_INST_EXP
        or eventID == public_config.EVENT_ID_ACCEPT_CIRCLE_TASK
        or eventID == public_config.EVENT_ID_HAS_DAILY_ACTIVITY
        or eventID == public_config.EVENT_ID_HAS_AION_LAYER then
        return true
    else
        return false
    end
end

function TaskCommonManager:IsGuardGoddessEvent(target)
    local eventID = target:GetEventID()
    if eventID == public_config.EVENT_ID_GUARD_GODDESS then
        return true
    else
        return false
    end
end

function TaskCommonManager:IsMapPosEvent(target)
    local eventID = target:GetEventID()
    if eventID == public_config.EVENT_ID_KILL_MONSTER
        or eventID == public_config.EVENT_ID_KILL_MONSTER_TEAM
        or eventID == public_config.EVENT_ID_CLIENT_PICK_FINISH
        or eventID == public_config.EVENT_ID_CLIENT_PICK_FINISH_TEAM
        or eventID == public_config.EVENT_ID_COLLECT
        or eventID == public_config.EVENT_ID_GET_ITEM
        or eventID == public_config.EVENT_ID_HAS_ITEM_COUNT
        or eventID == public_config.EVENT_ID_CLIENT_GOTO_POS then
        return true
    else
        return false
    end
end


function TaskCommonManager:GetTaskCurStepText(task)
    -- LoggerHelper.Error("task =====" .. PrintTable:TableToStr(task))
    local targetsInfo = task:GetTargetsInfo()
    local targetsDesc = task:GetTargetsDesc()
    local targetsTime = task:GetTargetsCurTimes()
    local taskState = task:GetState()
    local taskItemType = task:GetTaskItemType()
    
    if taskItemType == TaskConfig.TASK_TYPE_GUARD_GODDESS then
        return task:GetTargetsDesc()
    end
    
    if taskItemType == TaskConfig.TASK_TYPE_COMMON_TASK and (not task:IsOpen()) then
        -- and task:GetTaskType() ~= public_config.TASK_TYPE_VOCATION  then
        --未达到开启等级，显示前言
        return task:GetExplain() and LanguageDataHelper.CreateContent(task:GetExplain()) or ""
    end
    
    if table.isEmpty(targetsInfo) then
        return ""
    end
    
    if taskState == public_config.TASK_STATE_COMPLETED
        or taskState == public_config.TASK_STATE_ACCEPTABLE then
        if task:GetSelfLimit() > GameWorld.Player().level then
            return LanguageDataHelper.CreateContent(179, {["0"] = task:GetSelfLimit()})
        else
            if task:GetCommitNPC() == 666 and taskState == public_config.TASK_STATE_COMPLETED then
                return LanguageDataHelper.CreateContent(10590)--"点击领取任务"
            end

            for i, event in ipairs(targetsInfo) do
                return self:GetStepText(event, event:GetTalkToSomeOneContent(), 0)
            end
        end
    else
        local event = self:GetNextEvent(task)
        local id = event:GetId()
        return self:GetStepText(event, LanguageDataHelper.CreateContent(targetsDesc[id], LanguageDataHelper.GetArgsTable()), targetsTime[id] or 0)
    end
end

function TaskCommonManager:GetStepText(targetInfo, targetDescContent, curTimes)
    local totalTimes = targetInfo:GetTotalTimes()
    if totalTimes == 1 then --一个目标时不显示进度条
        return targetDescContent
    else
        return targetDescContent .. " (" .. curTimes .. "/" .. totalTimes .. ") "
    end
end

function TaskCommonManager:IsMainTask(taskItemData)
    if taskItemData:GetTaskItemType() == TaskConfig.TASK_TYPE_COMMON_TASK and
        taskItemData:GetTaskType() == public_config.TASK_TYPE_MAIN then
        return true
    end
    return false
end


--获取下一个即将开启的任务，没有则返回{}
function TaskCommonManager:GetNextOpenSoonTask(id)
    local data = {}
    local nextTaskId = TaskDataHelper:GetNextTask(id)
    local nextTask = TaskData:NewTaskItemData(nextTaskId, nil)
    if (not nextTask:IsOpen()) and nextTaskId ~= 0 and nextTask:GetExplain() ~= 0 then
        data[TaskConfig.TASK_CHANGE_INFO_ID] = id
        data[TaskConfig.TASK_CHANGE_INFO_TASK_TYPE] = TaskConfig.TASK_TYPE_COMMON_TASK
        data[TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE] = TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD
        data[TaskConfig.TASK_CHANGE_INFO_TASK_DATA] = nextTask
    end
    return data
end

function TaskCommonManager:RealStop()
    GameWorld.Player():StopAutoFight()
    GameWorld.Player():StopMoveWithoutCallback()
    PlayerActionManager:StopAllAction()
    self:SetCurAutoTask(nil, nil)
end

TaskCommonManager:Init()
return TaskCommonManager
