local BillBoardManager = {}
local GameSceneManager = GameManager.GameSceneManager
local SceneConfig = GameConfig.SceneConfig
local EntityType = GameConfig.EnumType.EntityType
local BillBoardType = GameConfig.EnumType.BillBoardType

function BillBoardManager:GetShowType(eniID)
    local entity = GameWorld.GetEntity(eniID)
    local showType = BillBoardType.Normal
    if entity == nil then
    	return showType
    end

    if entity.entityType == EntityType.BossTomb then
		showType = BillBoardType.DeathFlage
    elseif entity.entityType == EntityType.PlayerAvatar then
        showType = BillBoardType.Npc
    elseif entity.entityType == EntityType.Avatar then
    	local sceneType = GameSceneManager:GetCurSceneType()
        if sceneType == SceneConfig.SCENE_TYPE_MAIN then
            showType = BillBoardType.Npc
        else
            showType = BillBoardType.OtherPlayer
        end
    elseif entity.entityType == EntityType.NPC then
        showType = BillBoardType.Npc
    elseif entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
        if self:IsFriend(entity) then
            showType = BillBoardType.Npc
        end
        if entity:IsBoss() then
            showType = BillBoardType.Boss
        end
    elseif entity.entityType == EntityType.SpaceDrop then
        showType = BillBoardType.SpaceDrop
    elseif entity.entityType == EntityType.Collect then
        showType = BillBoardType.Collect
    elseif entity.entityType == EntityType.ClickFlag then
        showType = BillBoardType.Collect
    elseif entity.entityType == EntityType.ServerSpaceDrop then
        showType = BillBoardType.Collect
    end
    return showType
end


function BillBoardManager:GetColor(eniID)
    local entity = GameWorld.GetEntity(eniID)
    local honorColor = "#64DC23FF"
    local guildColor = "#00FFAAFF"
    local nameColor = "#FFFFFFFF"
    if entity ~= nil then
        while true do
            if entity.entityType == EntityType.PlayerAvatar then
                if entity.pk_value > 0 then
                    nameColor = "#FF3C3CFF"
                else
                    nameColor = "#C8F5CCFF"
                end
                break
            end
            if entity.entityType == EntityType.NPC then
                nameColor = "#FFAA22FF"
                honorColor = "#3DFF00FF"
                break
            end
            if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
                if self:IsFriend(entity) then
                    nameColor = "#FFC8C8FF"
                    break
                end
                if entity:IsBoss() then
                    nameColor = "#FF3C3CFF"
                else
                    nameColor = "#FF0000FF"
                end
                break
            end
            if entity.entityType == EntityType.Avatar then
                if entity.pk_value > 0 then
                    nameColor = "#FF3C3CFF"
                end
                if (GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_GUILD_CONQUEST or GameSceneManager:GetCurSceneType() == SceneConfig.SCENE_TYPE_ANCIENT_BATTLE) 
                and entity.faction_id ~= GameWorld.Player().faction_id then
                    nameColor = "#FF3C3CFF"
                    guildColor = "#FF3C3CFF"
                end
                break
            end
            if entity.entityType == EntityType.ClickFlag then
                if entity.faction_id == 0 then
                    nameColor = "#FFFFFFFF"
                elseif entity.faction_id == GameWorld.Player().faction_id then
                    nameColor = "#64DC23FF"
                else
                    nameColor = "#FF3232FF"
                end
                break
            end
            break
        end
    end
    return honorColor, guildColor, nameColor
end

function BillBoardManager:IsFriend(entity)
    local player = GameWorld.Player()
    if player and entity.faction_id == player.faction_id then
        return true
    end
    return false
end


return BillBoardManager
