local TeamManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local teamData = PlayerManager.PlayerDataManager.teamData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local ColorConfig = GameConfig.ColorConfig
local StringStyleUtil = GameUtil.StringStyleUtil
local RoleDataHelper = GameDataHelper.RoleDataHelper
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local CommonWaitManager = PlayerManager.CommonWaitManager

function TeamManager:Init()   
    self:InitCallbackFunc()
end

function TeamManager:InitCallbackFunc()
	self._onMatchChange = function() self:OnMatchChange() end
	self._onCancelMatch = function() self:OnCancelMatch() end
end

function TeamManager:OnPlayerEnterWorld()
    EventDispatcher:AddEventListener(GameEvents.MATCH_STATE_CHANGE, self._onMatchChange)
end

function TeamManager:OnPlayerLeaveWorld()
	self._cb = nil
	self._init = nil
	self._initTime = nil
    EventDispatcher:RemoveEventListener(GameEvents.MATCH_STATE_CHANGE, self._onMatchChange)
end

function TeamManager:OnMatchChange()
	-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
	local match_state = GameWorld.Player().match_state
	if match_state == 0 then
		--取消匹配
		CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_CANCEL_MATCH)
		EventDispatcher:TriggerEvent(GameEvents.MATCH_END)
	else
		--开始匹配
		CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_MATCH)
		EventDispatcher:TriggerEvent(GameEvents.MATCH_BEGIN)
		GUIManager.ClosePanel(PanelsConfig.InstanceHall)
	end
end

--进入副本前检查匹配状态
function TeamManager:CheckMatchStatusToInstance(cb)
	local match_state = GameWorld.Player().match_state
	if match_state == 0 then
		--不在匹配
		if cb then
			cb()
		end
	else
		self._cb = cb
		local confirmCB = function() self:CancelMatchAndGotoInstance() end
		--匹配中
		local value = {
			["confirmCB"] = confirmCB,
			["cancelCB"] = self._onCancelMatch,
			["text"] = LanguageDataHelper.CreateContent(10306)
		}
		local msgData = {["id"] = MessageBoxType.Tip, ["value"] = value}
		GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
	end	
end

function TeamManager:OnCancelMatch()
	self._cb = nil
end

function TeamManager:CancelMatchAndGotoInstance()
	self:RequestTeamMatchCancel()
end

--设置刚上线时是否有队伍
function TeamManager:SetHasTeamInOnline(flag)
	self._init = not flag
	self._initTime = DateTimeUtil.GetServerTime()
end

------------------------统一在这个方法处理接收到的组队相关response----------------------
function TeamManager:HandleData(action_id,error_id,args)
	--报错了
	if error_id ~= 0 then
		-- LoggerHelper.Error("Team Error:" ..error_id)
		--申请加入队伍
		if action_id == action_config.TEAM_APPLY then
			EventDispatcher:TriggerEvent(GameEvents.TEAM_APPLY_FAIL)
			-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
			CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_APPLY)
		end

		if action_id == action_config.TEAM_APPLY_BY_DBID then
			--EventDispatcher:TriggerEvent(GameEvents.TEAM_APPLY_FAIL)
			-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
			CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_APPLY_BY_DBID)
		end

		--开始匹配
		if action_id == action_config.TEAM_MATCH then
            -- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
			CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_MATCH)
		end
		--取消匹配
		if action_id == action_config.TEAM_CANCEL_MATCH then
            self._cb = nil
			CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_CANCEL_MATCH)
		end

        return
	--更新我的队伍
	elseif action_id == action_config.TEAM_REFRESH_ALL then--上线、加入队伍、创建队伍收到消息
		--LoggerHelper.Error("更新我的队伍信息" .. tostring(self._init))
		self:UpdateMyTeam(args)
		if self._init and DateTimeUtil.GetServerTime() - self._initTime > 5 then
			GUIManager.ShowPanel(PanelsConfig.Team,{firstTabIndex = 2})
			EventDispatcher:TriggerEvent(GameEvents.TEAM_TEAM_CREATE)
		end
		self._init = true
	--更新退队
	elseif action_id == action_config.TEAM_REFRESH_QUIT then
		-- LoggerHelper.Error("更新退队")
		self:QuitTeam(args)
	--更新成员上下线
	elseif action_id == action_config.TEAM_REFRESH_ONLINE then
		-- LoggerHelper.Error("更新成员上下线")
		self:UpdateTeamMemberOnline(args)
	--更新成员mapId/mapLine
	elseif action_id == action_config.TEAM_REFRESH_MAP_ID then
		-- LoggerHelper.Error("更新成员mapId/mapLine")
		self:UpdateTeamMemberMapInfo(args)
	--成员加入
	elseif action_id == action_config.TEAM_REFRESH_ADD then
		-- LoggerHelper.Error("成员加入")
		self:AddTeamMember(args)
	--成员删除
	elseif action_id == action_config.TEAM_REFRESH_DEL then
		-- LoggerHelper.Error("成员删除")
		self:DeleteTeamMember(args)
	--队长变更
	elseif action_id == action_config.TEAM_REFRESH_CAPTAIN_DBID then
		-- LoggerHelper.Error("队长变更")
		self:ChangeCaptain(args)
	--队员的血量改变
	elseif action_id == action_config.TEAM_REFRESH_HP then
		self:RefeshMemberHP(args)
	--队员的魔法量改变
	elseif action_id == action_config.TEAM_REFRESH_MP then
		--self:RefeshMemberMP(args)
	--队员的等级改变
	elseif action_id == action_config.TEAM_REFRESH_LEVEL then
		self:RefeshMemberLevel(args)
	--队伍目标id改变
	elseif action_id == action_config.TEAM_REFRESH_TARGET_ID then
		self:RefeshTargetId(args)
	--自动接受申请
	elseif action_id == action_config.TEAM_REFRESH_AUTO_ACCEPT_APPLY then 
		self:UpdateAutoAcceptApply(args)
	--收到邀请
	elseif action_id == action_config.TEAM_RECEIVE_INVITE then 
		-- LoggerHelper.Error("收到邀请")
		self:ReceiveInvite(args)
	--收到申请
	elseif action_id == action_config.TEAM_RECEIVE_APPLY then 
		-- LoggerHelper.Error("收到申请")
		self:ReceiveApply(args)
	--收到召唤
	elseif action_id == action_config.TEAM_RECEIVE_CALL then 
		self:ReceiveCall(args)
	--收到进入副本询问
	elseif action_id == action_config.TEAM_RECEIVE_START then
		self:ReceiveStart(args)
	--收到进入副本同意结果
	elseif action_id == action_config.TEAM_RECEIVE_AGREE_RESULT then	
		self:ReceiveAgreeResult(args)

	--非自己队伍相关返回
	--更新所有附近玩家
	elseif action_id == action_config.TEAM_NEAR_PLAYERS then 
		self:UpdateAllNearPlayer(args)
    --收到附近队伍信息
	elseif action_id == action_config.TEAM_NEAR_TEAMS then 
		self:UpdateNearTeam(args)
	--收到所有队伍信息
	elseif action_id == action_config.TEAM_ALL_TEAMS then
		self:UpdateAllTeam(args)
	--申请加入队伍
	elseif action_id == action_config.TEAM_APPLY then
		-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
		CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_APPLY)
	elseif action_id == action_config.TEAM_APPLY_BY_DBID then
		-- GUIManager.ClosePanel(PanelsConfig.WaitingLoadUI)
		CommonWaitManager:EndWaitLoading(WaitEvents.TEAM_APPLY_BY_DBID)
	elseif action_id == action_config.TEAM_MATCH then--开始匹配
		
	elseif action_id == action_config.TEAM_CANCEL_MATCH then--取消匹配
		if self._cb then
			self._cb()
			self._cb = nil
		end
	elseif action_id == action_config.TEAM_ALL_MEMBER_AGREE then--通知所有成员即将进入副本（队伍或匹配）客户端停自动任务
		--
	end
end

function TeamManager:GetMyMapId()
	return teamData:GetMyMapId()
end

------------------------------处理自己队伍相关信息------------------------------------
function TeamManager:OpenTeamPanel(data)
	--开启队伍面板
	GUIManager.ShowPanel(PanelsConfig.Team, data)
end

function TeamManager:OnCreateTeam(code)
	-- if code > 0 then
	-- 	return
	-- end
	-- GUIManager.ShowPanel(PanelsConfig.Team,{firstTabIndex = 2})
	-- EventDispatcher:TriggerEvent(GameEvents.TEAM_TEAM_CREATE)
end

--增加某队员
function TeamManager:AddTeamMember(luaTable)
	teamData:AddTeamMember(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MY_UPDATE)
	
end

--删除某队员
function TeamManager:DeleteTeamMember(luaTable)
	teamData:DeleteTeamMember(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MY_UPDATE)
end

--更新某人上下线
function TeamManager:UpdateTeamMemberOnline(luaTable)
	teamData:UpdateTeamMemberOnline(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MY_UPDATE)
end

--更新成员mapId/mapLine
function TeamManager:UpdateTeamMemberMapInfo(luaTable)
	teamData:UpdateTeamMemberMapInfo(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MEMBER_MAPINFO_UPDATE)
end

--队长变更
function TeamManager:ChangeCaptain(luaTable)
	teamData:ChangeCaptain(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MY_UPDATE)
end

--队员血量变更
function TeamManager:RefeshMemberHP(luaTable)
	teamData:RefeshMemberHP(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_REFRESH_HP)
end

--队员魔法量变更(废弃)
-- function TeamManager:RefeshMemberMP(luaTable)
-- 	teamData:RefeshMemberMP(luaTable)
-- 	EventDispatcher:TriggerEvent(GameEvents.TEAM_REFRESH_MP)
-- end

function TeamManager:RefeshMemberLevel(luaTable)
	teamData:RefeshMemberLevel(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_REFRESH_LEVEL)
end

function TeamManager:RefeshTargetId(luaTable)
	teamData:RefeshTargetId(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_REFRESH_TARGET_ID)
end

--退队
function TeamManager:QuitTeam(luaTable)
	local reason = luaTable[1]
	-- local LEAVE_RESON_SELF    = 1 --自己退出 
	-- local LEAVE_RESON_KICK    = 2 --被队长踢
	-- local LEAVE_RESON_DESTROY = 3 --掉线超过5分钟后销毁玩家时退出
	-- local LEAVE_RESON_DISMISS = 4 --队伍解散
	teamData.MyTeam = {}
	EventDispatcher:TriggerEvent(GameEvents.TEAM_QUIT)
end

--更新我的队伍信息
function TeamManager:UpdateMyTeam(luaTable)
	teamData:UpdateMyTeam(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_MY_UPDATE)
end

--team_id
--team_status
function TeamManager:UpdateAutoAcceptApply(luaTable)
	local auto_accept_apply = luaTable[1]
	teamData.AutoAcceptApply = auto_accept_apply
	EventDispatcher:TriggerEvent(GameEvents.TEAM_AUTO_ACCEPT_APPLY)
end

-------------------------------处理别的队伍的信息---------------------------------
function TeamManager:UpdateNearTeam(luaTable)
	teamData:UpdateNearTeam(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_NEAR_UPDATE)
end

function TeamManager:UpdateAllTeam(luaTable)
	teamData:UpdateAllTeam(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_ALL_UPDATE)
end

-------------------------------玩家申请邀请相关信息处理----------------------------------
function TeamManager:UpdateAllNearPlayer(luaTable)
	teamData:UpdateAllNearPlayer(luaTable)
	EventDispatcher:TriggerEvent(GameEvents.TEAM_ALL_NEAR_PLAYER)
end

function TeamManager:ReceiveInvite(luaTable)
	--邀请人信息
	local team_id 		= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_TEAM_ID]
   	local dbid 			= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_DBID]
    local name 			= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_NAME]
    local gender 		= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_GENDER]
    local vocation 		= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_VOCATION]
    local race 			= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_RACE]
    local level 		= luaTable[public_config.TEAM_RECEIVE_INVITE_KEY_LEVEL]

    local attach = "("..StringStyleUtil.GetLevelString(level)..RoleDataHelper.GetRoleNameTextByVocation(vocation)..")"
    attach = name..StringStyleUtil.GetColorStringWithTextMesh(attach,ColorConfig.E)
    local cnId = 10001
    if GameWorld.Player().team_status > 0 then
    	cnId = 10305
    end
	local str = LanguageDataHelper.GetContent(cnId):format(attach)
	GUIManager.ShowMessageBox(2,str,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_ACCEPT,"1,"..team_id..","..dbid) end,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_ACCEPT,"0,"..team_id..","..dbid) end
	)
end

function TeamManager:ReceiveApply(luaTable)
	--申请人信息
	local team_id 	        = luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_TEAM_ID]
   	local applyer_dbid 		= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_DBID]
    local applyer_name 		= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_NAME]
    local applyer_gender 	= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_GENDER]
    local applyer_vocation 	= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_VOCATION]
    local applyer_race 		= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_RACE]
    local applyer_level 	= luaTable[public_config.TEAM_RECEIVE_APPLY_KEY_APPLYER_LEVEL]

    local attach = "("..StringStyleUtil.GetLevelString(applyer_level)..RoleDataHelper.GetRoleNameTextByVocation(applyer_vocation)..")"
    attach = applyer_name..StringStyleUtil.GetColorStringWithTextMesh(attach,ColorConfig.E)
    local str = LanguageDataHelper.GetContent(10000):format(attach)
	GUIManager.ShowMessageBox(2,str,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_ACCEPT_APPLY,"1,"..applyer_dbid..","..team_id) end,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_ACCEPT_APPLY,"0,"..applyer_dbid..","..team_id) end
	)
end

--收到召唤信息
function TeamManager:ReceiveCall(luaTable)
    local str = LanguageDataHelper.GetContent(10002)
    local dbid = luaTable[1]
	GUIManager.ShowMessageBox(2,str,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_CALL_ACCEPT,"1"..","..dbid) end,
		function() GameWorld.Player().server.team_action_req(action_config.TEAM_CALL_ACCEPT,"0"..","..dbid) end
	)
end

--接收开始进入副本
function TeamManager:ReceiveStart(luaTable)
	--个人进入不需要处理
	if teamData:GetMyTeamMemberCount() < 2 then
		return
	end

	teamData:StartEnterCopy(luaTable)
	GUIManager.ShowPanel(PanelsConfig.TeamEnterInstance,nil,function ()
		GUIManager.GetPanel(PanelsConfig.TeamEnterInstance):UpdateTeamInfo()
	end)
end

function TeamManager:ReceiveAgreeResult(luaTable)
	--同意
	if luaTable[2] > 0 then
		teamData:ReceiveAgreeResult(luaTable)
		EventDispatcher:TriggerEvent(GameEvents.TEAM_ENTER_COPY_UPDATE)
		if teamData:CheckAllMemberAgree() then
			GUIManager.ClosePanel(PanelsConfig.TeamEnterInstance)
			teamData:ClearAgreeToCopy()
		end
	--拒绝
	else
		GUIManager.ClosePanel(PanelsConfig.TeamEnterInstance)
	end
end

---------------------------------------客户端请求--------------------------------------------------

function TeamManager:GetNearPlayer(page)
	GameWorld.Player().server.team_action_req(action_config.TEAM_NEAR_PLAYERS,tostring(page))
end

-- function TeamManager:TeamCall()
-- 	GameWorld.Player().server.team_action_req(action_config.TEAM_CALL,"0")
-- end

function TeamManager:RequestChangeTargetId(targetId)
	GameWorld.Player().server.team_action_req(action_config.TEAM_CHANGE_TARGET_ID,tostring(targetId))
end

function TeamManager:TeamApply(teamId)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
	CommonWaitManager:BeginWaitLoading(WaitEvents.TEAM_APPLY)
	GameWorld.Player().server.team_action_req(action_config.TEAM_APPLY,tostring(teamId))
end

function TeamManager:TeamApplyByDBId(dbId)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
	CommonWaitManager:BeginWaitLoading(WaitEvents.TEAM_APPLY_BY_DBID)
	GameWorld.Player().server.team_action_req(action_config.TEAM_APPLY_BY_DBID,tostring(dbId))
end

function TeamManager:TeamCreate(targetId)
	GameWorld.Player().server.team_action_req(action_config.TEAM_CREATE,tostring(targetId))
end

-- function TeamManager:RequestJoin(teamId)
-- 	GameWorld.Player().server.team_action_req(action_config.TEAM_JOIN,"0")
-- end

function TeamManager:TeamInvite(dbid)
	GameWorld.Player().server.team_action_req(action_config.TEAM_INVITE,tostring(dbid))
end

function TeamManager:TeamKick(dbid)
	GameWorld.Player().server.team_action_req(action_config.TEAM_KICK,tostring(dbid))
end

function TeamManager:TeamLeave()
	GameWorld.Player().server.team_action_req(action_config.TEAM_LEAVE,"");
end

function TeamManager:TeamSetAutoAcceptApply(state)
	--LoggerHelper.Error("TeamManager:TeamSetAutoAcceptApply(state)" .. tostring(state))
	GameWorld.Player().server.team_action_req(action_config.TEAM_SET_AUTO_ACCEPT_APPLY,tostring(state))
end

--获取附近队伍
function TeamManager:RequestNearTeams(page)
	--LoggerHelper.Error("获取附近队伍")
	GameWorld.Player().server.team_action_req(action_config.TEAM_NEAR_TEAMS,tostring(page))
end

--获取全部队伍
function TeamManager:RequestAllTeams(page)
	--LoggerHelper.Error("获取全部队伍")
	GameWorld.Player().server.team_action_req(action_config.TEAM_ALL_TEAMS,tostring(page))
end

--获取
function TeamManager:RequestNearPlayers(page)
	--获取第一页数据
	GameWorld.Player().server.team_action_req(action_config.TEAM_NEAR_PLAYERS,tostring("0"))
end

--获取最近组队玩家
function TeamManager:RequestRecentPlayers()
	GameWorld.Player().server.team_action_req(action_config.TEAM_NEAR_PLAYERS,"")
end

function TeamManager:TeamAgreeEnterCopy(isAgree,instanceId)
	GameWorld.Player().server.team_action_req(action_config.TEAM_AGREE,tostring(isAgree)..","..tostring(instanceId))
end

------------------召唤相关---------------------------
--召唤所有队员
function TeamManager:RequestCallAll()
	GameWorld.Player().server.team_action_req(action_config.TEAM_CALL_ALL,"");
end

--传送到队长
function TeamManager:RequestAcceptCall()
	local dbid = teamData:GetMyTeam().captainId
	if dbid then
		GameWorld.Player().server.team_action_req(action_config.TEAM_CALL_ACCEPT,"1"..","..dbid)
	end
end

---------------------组队匹配-------------------------------
--开始匹配
function TeamManager:RequestTeamMatch(targetId)
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
	CommonWaitManager:BeginWaitLoading(WaitEvents.TEAM_MATCH)
	GameWorld.Player().server.team_action_req(action_config.TEAM_MATCH,tostring(targetId))
end

--取消匹配
function TeamManager:RequestTeamMatchCancel()
    -- GUIManager.ShowPanel(PanelsConfig.WaitingLoadUI)
	CommonWaitManager:BeginWaitLoading(WaitEvents.TEAM_CANCEL_MATCH)
	GameWorld.Player().server.team_action_req(action_config.TEAM_CANCEL_MATCH,"")
end

---------------------组队开始副本-------------------------------
function TeamManager:RequestTeamStart(mapId)
	GameWorld.Player().server.team_action_req(action_config.TEAM_START, tostring(mapId))
end

 --通知所有成员即将进入副本（队伍或匹配）客户端停自动任务


TeamManager:Init() 
return TeamManager