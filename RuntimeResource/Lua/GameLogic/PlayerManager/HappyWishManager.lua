local HappyWishManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local error_code = GameWorld.error_code
local ChatConfig = GameConfig.ChatConfig
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ChatManager = PlayerManager.ChatManager
local RedPointManager = GameManager.RedPointManager
local FontStyleHelper = GameDataHelper.FontStyleHelper
local PromisDisplayDataHelper = GameDataHelper.PromisDisplayDataHelper
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local TipsManager = GameManager.TipsManager
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local MessageBoxType = GameConfig.EnumType.MessageBoxType

function HappyWishManager:Init()
    self._changeUpdate = function()self:OnChangeUpdate() end
    self._dayUpdate = function()self:OnDayUpdate() end
    self._checkHappyOpenCb = function (id)
    	if id == public_config.FUNCTION_ID_HAPPYWISH then
    		self:OnChangeUpdate()
    	end
    end
end

function HappyWishManager:OnPlayerEnterWorld()
    --local price = GlobalParamsHelper.GetMSortValue(865)[1][2]
    --LoggerHelper.Error("price:"..price)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkHappyOpenCb)
    EventDispatcher:AddEventListener(GameEvents.UpdateHappy,self._changeUpdate)
    self._timer = DateTimeUtil.AddTimeCallback(24, 0, 20, true, self._dayUpdate)
    --GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._changeUpdate)
    self:OnChangeUpdate()
end

function HappyWishManager:OnChangeUpdate()

    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_HAPPYWISH)
    local leftTime = self:GetLeftTime()
    if tonumber(leftTime)>1 and isOpen then
        EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON, TimeLimitType.HappyWish, function()
            GUIManager.ShowPanel(PanelsConfig.HappyWish)
        end,tonumber(leftTime%(24*60*60))+DateTimeUtil.GetServerTime())
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.HappyWish)


end


function HappyWishManager:OnDayUpdate()
    EventDispatcher:TriggerEvent(GameEvents.OnDayUpdate)
    self:OnChangeUpdate()
    --LoggerHelper.Error("self:OnChangeUpdate()")
end

function HappyWishManager:GetNeedId()
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local ids = PromisDisplayDataHelper:GetAllId()

    for i,id in ipairs(ids) do
        if i+1<=#ids then
            local startTimePre = PromisDisplayDataHelper:GetDay(ids[i])
            local startTimeAfter = PromisDisplayDataHelper:GetDay(ids[i+1])
            local secOpenPre = self:GetOpenDiff(startTimePre, 0)
            local secOpenAfter = self:GetOpenDiff(startTimeAfter, 0)
    
            if timeDiff>=secOpenPre and timeDiff<secOpenAfter then
                return ids[i]
            end
        else
            local startTime = PromisDisplayDataHelper:GetDay(ids[i])
            local secOpen = self:GetOpenDiff(startTime, 0)
            if timeDiff>=secOpen then
                return ids[i]
            end
        end
    end

end




function HappyWishManager:GetLeftTime()
    local timeDiff = DateTimeUtil.MinusSec(DateTimeUtil.GetOpenServerTime())
    local ids = PromisDisplayDataHelper:GetAllId()
    local startTime = PromisDisplayDataHelper:GetDay(1)
    local stopTime = PromisDisplayDataHelper:GetDay(#ids)

    local daysOpen = startTime
    local secOpen = self:GetOpenDiff(startTime, 0)
    local secOver = self:GetOpenDiff(stopTime, 0)+24*60*60
    if timeDiff >= secOver then
        --活动关闭
        return -1 
    else
        if timeDiff >= secOpen then
                --活动开始
            return secOver - timeDiff
        else
            return -1 
        end
    end

    return -1 
end


function HappyWishManager:OnPlayerLeaveWorld()
    DateTimeUtil.DelTimeCallback(self._timer)
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkHappyOpenCb)
    EventDispatcher:RemoveEventListener(GameEvents.UpdateHappy,self._changeUpdate)
    --GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._changeUpdate)
end

function HappyWishManager:HandleData(action_id, error_code, args)
if error_code ~= 0 then
        return
    end
    if action_id == action_config.WISH_WISH then
        --翻牌
        self:GetWishResp(args)  
    end
end

function HappyWishManager:GetOpenDiff(daysClose, hoursClose)
    local daysOpenOne = 0
    local hoursOpenOne = 0
    local secDiff = 0

    if daysClose - daysOpenOne == 0 then
        secDiff = (hoursClose - hoursOpenOne) * 60 * 60
    elseif daysClose - daysOpenOne >= 1 then
        for i = 1, daysClose do
            if i == daysClose then
                secDiff = secDiff + (hoursClose - hoursOpenOne) * 60 * 60
            else
                secDiff = secDiff + 24 * 60 * 60
            end
        end
    end
    return secDiff
end

function HappyWishManager:GetWishResp(args)
    EventDispatcher:TriggerEvent(GameEvents.HappyWishResp)

    if not table.isEmpty(args) then
		local data = {}
		data[1] = -1
		data[2] = args
		local msgData = {["id"] = MessageBoxType.GiftSucc, ["value"] = data}
		GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData) 
	end
end

--许愿请求
function HappyWishManager:SendWishReq()
    GameWorld.Player().server.wish_action_req(action_config.WISH_WISH,"")
end


function HappyWishManager:GetWishLuck()
    return GameWorld.Player().wish_luck or {}
end

--领取奖励
function HappyWishManager:GetWishEnd()
    return GameWorld.Player().wish_end_flag or {}
end


HappyWishManager:Init()
return HappyWishManager