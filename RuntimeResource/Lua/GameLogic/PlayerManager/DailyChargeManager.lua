local DailyChargeManager = {}
local action_config = require("ServerConfig/action_config")
local public_config = GameWorld.public_config
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local DateTimeUtil = GameUtil.DateTimeUtil
local bossHomeData = PlayerDataManager.bossHomeData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local DailyChargeDataHelper = GameDataHelper.DailyChargeDataHelper

function DailyChargeManager:Init()

end

function DailyChargeManager:OnPlayerEnterWorld()
    self._refreshFirstChargeRewardState = function () self:RefreshFirstChargeRewardState() end
    EventDispatcher:AddEventListener(GameEvents.Refresh_First_Charge_Reward_State, self._refreshFirstChargeRewardState)
    self._refreshDailyChargeRewardState = function () self:RefreshDailyChargeRewardState() end
	EventDispatcher:AddEventListener(GameEvents.Refresh_Daily_Charge_Reward_State, self._refreshDailyChargeRewardState)
	
	self._showFirstChargeRewardRedPoint = false
    self:RefreshFirstChargeRewardState()

    self._showDailyChargeRewardRedPoint = false
    self:RefreshDailyChargeRewardState()
end

function DailyChargeManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_First_Charge_Reward_State, self._refreshFirstChargeRewardState)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Daily_Charge_Reward_State, self._refreshDailyChargeRewardState)
end

function DailyChargeManager:HandleData(action_id, error_id, args)
    --LoggerHelper.Log("DailyChargeManager:HandleData "..action_id .. " " .. error_id)
    --LoggerHelper.Log(args)
    if error_id ~= 0 then
        return
    end
    if action_id == action_config.DAILY_CHARGE_REWARD then

    end
end
-------------------------------------数据处理------------------------------------

-------------------------------------接口------------------------------------

-----------------------------------红点------------------------------
function DailyChargeManager:RefreshFirstChargeRewardState(type)
    if GameWorld.Player().first_charge_reward_got == 2 then
        self:SeFirstChargeRewardRedPoint(true)
    else
        self:SeFirstChargeRewardRedPoint(false)
    end
end

function DailyChargeManager:SeFirstChargeRewardRedPoint(state)
    if self._showFirstChargeRewardRedPoint ~= state then
        self._showFirstChargeRewardRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_FIRST_CHARGE, self:GetFirstChargeRewardRedPoint())
    end
end

function DailyChargeManager:GetFirstChargeRewardRedPoint()
    return self._showFirstChargeRewardRedPoint
end

function DailyChargeManager:RefreshDailyChargeRewardState(type)
    local result = false
    local cfg = GlobalParamsHelper.GetParamValue(766)
    for k,v in pairs(cfg) do
        if GameWorld.Player().daily_charge_reward_record[v] == 0 then
            result = true
            break
        end
    end

    for i=1,#DailyChargeDataHelper:GetAllCumulativeDailyChargeData() do
        if GameWorld.Player().daily_charge_cumulative_record[i] == 0 then
            result = true
            break
        end
    end

    self:SetDailyChargeRewardRedPoint(result)
end

function DailyChargeManager:SetDailyChargeRewardRedPoint(state)
    if self._showDailyChargeRewardRedPoint ~= state then
        self._showDailyChargeRewardRedPoint = state
		EventDispatcher:TriggerEvent(GameEvents.RED_POINT_STATUS_CHANGE, public_config.FUNCTION_ID_DAY_FIRST_CHARGE, self:GetDailyChargeRewardRedPoint())
    end
end

function DailyChargeManager:GetDailyChargeRewardRedPoint()
    return self._showDailyChargeRewardRedPoint
end

-------------------------------------客户端请求-----------------------------------
function DailyChargeManager:GetChargeReward(cost)
    local params = string.format("%d", cost)
    GameWorld.Player().server.daily_charge_action_req(action_config.DAILY_CHARGE_REWARD, params)
end

function DailyChargeManager:GetChargeCumulativeReward(day)
    local params = string.format("%d", day)
    GameWorld.Player().server.daily_charge_action_req(action_config.DAILY_CHARGE_CUMULATIVE_REWARD, params)
end

DailyChargeManager:Init()
return DailyChargeManager
