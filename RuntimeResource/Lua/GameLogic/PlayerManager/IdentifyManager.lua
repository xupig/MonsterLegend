local IdentifyManager = {}

local PlayerDataManager = PlayerManager.PlayerDataManager
local public_config = GameWorld.public_config
local action_config = GameWorld.action_config
local error_code = GameWorld.error_code
local ChatConfig = GameConfig.ChatConfig
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ChatManager = PlayerManager.ChatManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local RedPointManager = GameManager.RedPointManager
local FontStyleHelper = GameDataHelper.FontStyleHelper
local WholePeopleJianbaoDataHelper = GameDataHelper.WholePeopleJianbaoDataHelper
require "PlayerManager/PlayerData/CommonItemData"
local CommonItemData = ClassTypes.CommonItemData
local TipsManager = GameManager.TipsManager
local TimeLimitType = GameConfig.EnumType.TimeLimitType
local DateTimeUtil = GameUtil.DateTimeUtil
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper



function IdentifyManager:Init()
    self._channelChange = function()self:OnActivityTimeChange() end
    self._checkIdentifyOpenCb = function (id)
    	if id == public_config.FUNCTION_ID_IDENTIFY then
    		self:OnActivityTimeChange()
    	end
    end

    self._openFun = function() GUIManager.ShowPanel(PanelsConfig.Identify) end
end

function IdentifyManager:OnPlayerEnterWorld()
    self._funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_IDENTIFY)
    --EventDispatcher:TriggerEvent(GameEvents.ON_FUNCTION_OPEN,id)
    EventDispatcher:AddEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkIdentifyOpenCb)
    EventDispatcher:AddEventListener(GameEvents.UpdateIdentify,self._channelChange)
    EventDispatcher:AddEventListener(GameEvents.On_0_Oclock, self._channelChange)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CHANNEL_ACTIVITY_TIMES, self._channelChange)
    --GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._channelChange)
    self:OnActivityTimeChange()

end


function IdentifyManager:OnActivityTimeChange()
	    self._showMenuIcon = false
    	local now = DateTimeUtil.GetServerTime()
		local activityTimes = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_JIANBAO]
		if activityTimes then
			local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
			local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
			--活动还没开始
			if now < startTime then
				if self._firstActivityOpenTime == nil or self._firstActivityOpenTime > startTime then
					self._firstActivityOpenTime = startTime
				end
			end
			--活动已开始判断
			local b = endTime > now and now >= startTime

			if b then
				--LoggerHelper.Error("OnActivityTimeChange")
				self._showMenuIcon = b
				if self._lastActivityEndTime == nil or self._lastActivityEndTime < endTime then
					self._lastActivityEndTime = endTime
				end
			end
		else

		end
	
	--要显示主菜单icon
	if self._showMenuIcon then
		if self._funcOpen then
			self:ShowMenu()
			self:CheckEnd()
		end
	--未开启
	else
		self:CheckFirstActivityOpen()
		self:RemoveMenuIcon()
	end
end

--检查是否需要计时开启主界面图标
function IdentifyManager:CheckFirstActivityOpen()
	if self._firstActivityOpenTime == nil then
		return
	end
	
	local now = DateTimeUtil.GetServerTime()
	local timeLeft = self._firstActivityOpenTime - now
	--LoggerHelper.Error("endTimeLeft："..endTimeLeft)
	if timeLeft > 0 and timeLeft < 86400 then
		--先删除旧的计时
		if self._firstOpenTimer then
			TimerHeap:DelTimer(self._firstActivityOpenTimer)
			self._firstOpenTimer = nil
		end
		self._firstOpenTimer =  TimerHeap:AddSecTimer(timeLeft, 0, timeLeft, function ()
			TimerHeap.DelTimer(self._firstOpenTimer)
			self:OnActivityTimeChange()
		end)
	end
end


--关闭主界面图标
function IdentifyManager:RemoveMenuIcon()
	if self._endTimer then
		TimerHeap:DelTimer(self._endTimer)
		self._endTimer = nil
	end
	EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, TimeLimitType.Identify)
end


--检查是否关闭主界面图标
function IdentifyManager:CheckEnd()
	if self._lastActivityEndTime == nil then
		return
	end
	--先删除旧的计时
	local now = DateTimeUtil.GetServerTime()
	local endTimeLeft = self._lastActivityEndTime - now
	--LoggerHelper.Error("endTimeLeft："..endTimeLeft)
	if endTimeLeft > 0 and endTimeLeft < 86400 then
		if self._endTimer then
			TimerHeap:DelTimer(self._endTimer)
			self._endTimer = nil
		end
		self._endTimer =  TimerHeap:AddSecTimer(endTimeLeft, 0, endTimeLeft, function ()
			self:RemoveMenuIcon()
		end)
	end
end

function IdentifyManager:ShowMenu()
    local leftTime = self:GetLeftTime()
	EventDispatcher:TriggerEvent(GameEvents.ADD_MAIN_MENU_ICON,TimeLimitType.Identify,self._openFun,tonumber(leftTime%(24*60*60))+DateTimeUtil.GetServerTime())
end

function IdentifyManager:GetChannelTime()
	return  GameWorld.Player().channel_activity_times or {}
end

function IdentifyManager:GetNeedId()
    local ids = WholePeopleJianbaoDataHelper:GetAllId()
    local level = self:GetIdentifyLevel()
    for i,id in ipairs(ids) do    
        local startPre = WholePeopleJianbaoDataHelper:GetWorldLevel(ids[i])
        local turnTime = WholePeopleJianbaoDataHelper:GetTurn(ids[i])
        if level>=startPre[1] and level<=startPre[2] then
            local identifyInfo = IdentifyManager:GetIdentifyInfo()
            if turnTime == identifyInfo[public_config.JIANBAO_ROUND] then
                return ids[i]
            end      
        end
    end
end



function IdentifyManager:OnPlayerLeaveWorld()
    EventDispatcher:RemoveEventListener(GameEvents.ON_FUNCTION_OPEN,self._checkIdentifyOpenCb)
    EventDispatcher:RemoveEventListener(GameEvents.UpdateIdentify,self._channelChange)
    EventDispatcher:RemoveEventListener(GameEvents.On_0_Oclock,self._channelChange)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CHANNEL_ACTIVITY_TIMES, self._channelChange)
    --GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._channelChange)
end

function IdentifyManager:GetLeftTime()
    local channelTime =  self:GetChannelTime()
    local time = 0
    if channelTime and channelTime[public_config.CHANNEL_ACTIVITY_ID_JIANBAO]then
        time = channelTime[public_config.CHANNEL_ACTIVITY_ID_JIANBAO][public_config.CHANNEL_ACTIVITY_KEY_END_TIME] - DateTimeUtil.GetServerTime()
    end
    return time or 0
end

function IdentifyManager:HandleData(action_id, error_code, args)
	if error_code ~= 0 then
        return
    end
    if action_id == action_config.JIANBAO_REWARD then
        --鉴宝
        self:GetIdentifyResp(args)  
    end
end



function IdentifyManager:GetIdentifyResp(args)
	EventDispatcher:TriggerEvent(GameEvents.IdentifyResp)
	local identifyInfo = IdentifyManager:GetIdentifyInfo()


	if not table.isEmpty(args) then
		local data = {}
		data[1] = -1
		data[2] = args
		local msgData = {["id"] = MessageBoxType.GiftSucc, ["value"] = data}
		GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData) 
	end

    --LoggerHelper.Error("IdentifyManager:identifLevel"..PrintTable:TableToStr(args))
end

--鉴宝请求
function IdentifyManager:SendIdentifyReq()
    GameWorld.Player().server.jianbao_action_req(action_config.JIANBAO_REWARD,"")
end


function IdentifyManager:GetIdentifyInfo()
    return GameWorld.Player().jianbao_info or {}
end

--鉴宝世界等级
function IdentifyManager:GetIdentifyLevel()
    return GameWorld.Player().jianbao_world_level or {}
end


IdentifyManager:Init()
return IdentifyManager