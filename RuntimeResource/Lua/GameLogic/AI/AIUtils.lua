
AIUtils = {}

AIUtils.CmpType = {}
AIUtils.CmpType.lt = 1
AIUtils.CmpType.le = 2
AIUtils.CmpType.eq = 3
AIUtils.CmpType.ge = 4
AIUtils.CmpType.gt = 5

AIUtils.Switch = {[1] = function(lv,rv) return lv < rv end,
                  [2] = function(lv,rv) return lv <= rv end,
                  [3] = function(lv,rv) return lv == rv end,
                  [4] = function(lv,rv) return lv >= rv end,
                  [5] = function(lv,rv) return lv > rv end }


function AIUtils:CmpTypeMethod(cmpType,lv,rv)
    local func = self.Switch[cmpType]
    if func == nil then return false end
    return func(lv,rv)
end

return AIUtils