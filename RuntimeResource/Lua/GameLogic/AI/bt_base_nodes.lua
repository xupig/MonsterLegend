local ai_utils = require "AI/ai_utils"

local BT_RNT_COMPLETE   = ai_utils.BT_RNT_COMPLETE
local BT_RNT_PREFAIL    = ai_utils.BT_RNT_PREFAIL
local BT_RNT_EXECUTING  = ai_utils.BT_RNT_EXECUTING
local BT_RNT_ABORT      = ai_utils.BT_RNT_ABORT

local math = math

local bt_base_node = {}
bt_base_node.__index = bt_base_node

function bt_base_node:new()
    local tmp = {index=1}
    setmetatable(tmp, {__index = bt_base_node})
    tmp.__index = tmp
    
    return tmp
end

function bt_base_node:tick_proc(entity)
    return BT_RNT_PREFAIL
end

function bt_base_node:tick_start(entity)
    return self:tick_proc(entity)
end

function bt_base_node:update_proc(entity)
    return BT_RNT_PREFAIL
end

function bt_base_node:add_child(node_child)

end


function bt_base_node:child_call_back(rnt, entity, node_child_index)
end

-----------------------------------------------------------------------

local bt_root = bt_base_node:new()

function bt_root:new(t_myid)
    local tmp = {myid = t_myid}
    setmetatable(tmp, {__index = bt_root})
    tmp.__index = tmp

    return tmp
end

function bt_root:tick_start(entity)

    --LoggerHelper.Error("-------bt_root:tick_start------"..tostring(#self.children))
    local node_child = self.children[1]
    node_child:tick_start(entity)
end

function bt_root:add_child(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_root:child_call_back(rnt, entity, node_child_index)
    entity.executing = 0
    entity.bt_node = self
    entity:OnLoopEnd()
    -- if entity:get_imm_root_flag() == 1 then
    --     entity:set_imm_root_flag(0)
    --     --LoggerHelper.Error("child_call_back" )
    --     return self:tick_start(entity)
    -- end
end
-----------------------------------------------------------------------

local bt_sub_root = bt_base_node:new()

function bt_sub_root:new(t_myid)
    local tmp = {myid = t_myid}
    setmetatable(tmp, {__index = bt_sub_root})
    tmp.__index = tmp

    return tmp
end

function bt_sub_root:tick_start(entity)

    local node_child = self.children[1]
    node_child:tick_start(entity)
end

function bt_sub_root:add_child(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_sub_root:child_call_back(rnt, entity, node_child_index)
    entity.executing = 0
    entity.bt_node = {}

    entity:ReStartMainAI()
end

-----------------------------------------------------------------------
local bt_not = bt_base_node:new()

function bt_not:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_not})
    tmp.__index = tmp

    return tmp
end

function bt_not:proxy(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_not:tick_start(entity)
    if #self.children > 0 then
        local node_child = self.children[1]
        entity.bt_node = node_child
        return entity.bt_node:tick_start(entity)
    end

    return BT_RNT_PREFAIL
end

function bt_not:child_call_back(rnt, entity, node_child_index)

    if rnt == BT_RNT_COMPLETE then
        rnt = BT_RNT_PREFAIL
        self.node_parent:child_call_back(not rnt, entity, self.index)

    elseif rnt == BT_RNT_PREFAIL then
        rnt = BT_RNT_COMPLETE
        self.node_parent:child_call_back(not rnt, entity, self.index)

    elseif rnt == BT_RNT_ABORT then
    end
end

-----------------------------------------------------------------------

local bt_condition = bt_base_node:new()

function bt_condition:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_condition})
    tmp.__index = tmp

    return tmp
end

function bt_condition:tick_start(entity)
    local rnt = self:tick_proc(entity)
    if rnt ~= BT_RNT_EXECUTING then
        self.node_parent:child_call_back(rnt, entity, self.index)
    end
    return rnt
end

-----------------------------------------------------------------------

local bt_sequence = bt_base_node:new()

function bt_sequence:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_sequence})
    tmp.__index = tmp

    return tmp
end

function bt_sequence:tick_start(entity)
    --LoggerHelper.Error("-------bt_sequence:tick_start------"..tostring(#self.children))


    --print("bt_sequence:tick_start fffffffffffffffffffffff",self.id)

    if #self.children > 0 then
        local node_child = self.children[1]
        entity.bt_node = node_child
        return entity.bt_node:tick_start(entity)
    end

    return BT_RNT_PREFAIL 
end

function bt_sequence:add_child(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_sequence:child_call_back(rnt, entity, node_child_index)

    if rnt == BT_RNT_COMPLETE then
        local next_node_child_index = node_child_index + 1
        if self.children[next_node_child_index] then

            entity.bt_node = self.children[next_node_child_index]
            entity.bt_node:tick_start(entity)
        else
            self.node_parent:child_call_back(rnt, entity, self.index)
        end

    elseif rnt == BT_RNT_PREFAIL then
        self.node_parent:child_call_back(rnt, entity, self.index)

    elseif rnt == BT_RNT_ABORT then
    end
end

-----------------------------------------------------------------------

local bt_prior_selector = bt_base_node:new()

function bt_prior_selector:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_prior_selector})
    tmp.__index = tmp

    return tmp
end

function bt_prior_selector:tick_start(entity)
    if #self.children > 0 then
        local node_child = self.children[1]

        entity.bt_node = node_child
        return entity.bt_node:tick_start(entity)
    end
    return BT_RNT_PREFAIL 
end

function bt_prior_selector:add_child(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_prior_selector:child_call_back(rnt, entity, node_child_index)
    if rnt == BT_RNT_COMPLETE then
        self.node_parent:child_call_back(rnt, entity, self.index)

    elseif rnt == BT_RNT_PREFAIL then
        local next_node_child_index = node_child_index + 1
        if self.children[next_node_child_index] then

            entity.bt_node = self.children[next_node_child_index]
            entity.bt_node:tick_start(entity)
        else
            self.node_parent:child_call_back(rnt, entity, self.index)
        end

    elseif rnt == BT_RNT_ABORT then
    end
end

--------------------------------------------------------------------------------

local bt_random_selector = bt_base_node:new()

function bt_random_selector:new(...)
    local tmp = {_weights = {...}}
    setmetatable(tmp, {__index = bt_random_selector})
    tmp.__index = tmp

    return tmp
end

function bt_random_selector:add_child(node_child)
    if not self.children then
        self.children = {}
    end

    node_child.node_parent = self
    table.insert(self.children, node_child)
    node_child.index = #self.children
end

function bt_random_selector:tick_start(entity)

    if #self.children > 0 then
        local node_index = 1

        local weights = self._weights
        local w_sum = 0
        for i, v in pairs(weights) do
            w_sum = w_sum + v
        end

        local random_v = math.random(1, w_sum)
        w_sum = 0

        for idx, v in pairs(weights) do
            w_sum = w_sum + v
            if w_sum >= random_v then
                node_index = idx
                break
            end
        end

        local node_child = self.children[node_index]

        entity.bt_node = node_child
        return node_child:tick_start(entity)
    end
    return BT_RNT_PREFAIL
end

function bt_random_selector:child_call_back(rnt, entity, node_child_index)
    if rnt == BT_RNT_COMPLETE or rnt == BT_RNT_PREFAIL then
        self.node_parent:child_call_back(rnt, entity, self.index)
    elseif rnt == BT_RNT_ABORT then
    end
end

-----------------------------------------------------------------------

local bt_action = bt_base_node:new()

function bt_action:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_action})
    tmp.__index = tmp

    return tmp
end

function bt_action:pre_proc(entity)
    return BT_RNT_PREFAIL
end

function bt_action:tick_start(entity)
    --[[
    if entity.cur_ai_id == 10000 then
        LoggerHelper.Error("-------bt_action:tick_start--------"..tostring(self.__debug_name))
    end
    ]]--

    if entity.executing == 0 then
        local rnt = self:pre_proc(entity)
        --LoggerHelper.Error("-------bt_action:tick_start------2--"..tostring(rnt))
        if rnt == BT_RNT_EXECUTING then
            --LoggerHelper.Error("-------bt_action:tick_start------3--"..tostring(entity.executing))
            rnt = self:update_proc(entity)
            if rnt == BT_RNT_EXECUTING then
                entity.executing = 1
            else
                entity.executing = 0
                self.node_parent:child_call_back(rnt, entity, self.index)
            end

            --LoggerHelper.Error("-------bt_action:tick_start------4--"..tostring(rnt))
            return rnt
        elseif rnt == BT_RNT_PREFAIL or rnt == BT_RNT_COMPLETE then
            --LoggerHelper.Error("-------bt_action:tick_start------5--"..tostring(entity.executing))
            entity.executing = 0
            self.node_parent:child_call_back(rnt, entity, self.index)
        else
            --前提判断失败
            entity.executing = 0
            return BT_RNT_PREFAIL 
        end
    else

        --LoggerHelper.Error("-------bt_action:tick_start------6--"..tostring(entity.executing))
        local rnt = self:update_proc(entity)
        if rnt == BT_RNT_EXECUTING then
            entity.executing = 1
        else
            entity.executing = 0
            self.node_parent:child_call_back(rnt, entity, self.index)
        end

        return rnt
    end
end

-----------------------------------------------------------------------

bt = {
    
    bt_root = bt_root,
    bt_not = bt_not,
    bt_action = bt_action,
    bt_condition = bt_condition,
    bt_sequence = bt_sequence,
    bt_prior_selector = bt_prior_selector,
    bt_random_selector = bt_random_selector,
    bt_sub_root = bt_sub_root,
    cmp_type = { lt = 1, le = 2, eq = 3, ge = 4, gt = 5 },
    ai_faction_type = { enemy = 1, teammate = 2, all = 3 },
    ai_target_type = {
        aggro = 1,
        self = 2,
        master = 3,
        servant = 4,
        hp_lowest = 5,
        hp_highest = 6,
        closest = 7,
        farthest = 8,
        has_buff = 9,
        master_target = 10,
        drop = 11,
        friend_random = 20,
        friend_hp_lowest = 21,
        friend_hp_highest = 22,
        friend_closest = 23,
        friend_farthest = 24,
        friend_has_buff = 25,
        friend_range_random = 26,
    },
    ai_patrol_type = {stand = 1, range = 2, path = 3},
}

return bt







