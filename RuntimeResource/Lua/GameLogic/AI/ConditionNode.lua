--条件判断类,叶子节点
local AI_ConditionNode = Class.AI_ConditionNode(ClassTypes.IBehaviorTreeNode)

function AI_ConditionNode:Proc(theOwner)
    return false
end
--简单测试用例
local AI_IsSmallerCondition = Class.AI_IsSmallerCondition(ClassTypes.AI_ConditionNode)

function AI_IsSmallerCondition:__ctor__(hp)
    self._hp = hp or 0
end

function AI_IsSmallerCondition:Proc(theOwner)
    return theOwner:ProcIsSmallerCondition(self._hp)
end

local AI_CmpRate = Class.AI_CmpRate(ClassTypes.AI_ConditionNode)

function AI_CmpRate:__ctor__(cmpType,value)
    self._cmpType = cmpType or AIUtils.CmpType.eq
    self._rate = value
end

function AI_CmpRate:Proc(theOwner)
    local rate = theOwner:Random(0,100)
    return AIUtils:CmpTypeMethod(self._cmpType,rate,self._rate)
end

local AI_CanMove = Class.AI_CanMove(ClassTypes.AI_ConditionNode)

function AI_CanMove:Proc(theOwner)
    return theOwner:ProcCanMove()
end

local AI_CanCastSpell = Class.AI_CanCastSpell(ClassTypes.AI_ConditionNode)

function AI_CanCastSpell:Proc(theOwner)
    return theOwner:ProcCanCastSpell()
end

local AI_HasTarget = Class.AI_HasTarget(ClassTypes.AI_ConditionNode)

function AI_HasTarget:Proc(theOwner)
    return theOwner:ProcHasTarget()
end

local AI_InSky = Class.AI_InSky(ClassTypes.AI_ConditionNode)

function AI_InSky:Proc(theOwner)
    return theOwner:ProcInSky()
end

local AI_InSkillCoolDown = Class.AI_InSkillCoolDown(ClassTypes.AI_ConditionNode)

function AI_InSkillCoolDown:__ctor__(skillLibIndex)
    self._skillLibIndex = skillLibIndex
end

function AI_InSkillCoolDown:Proc(theOwner)
    return theOwner:ProcInSkillCoolDown(self._skillLibIndex)
end

local AI_InSkillRange = Class.AI_InSkillRange(ClassTypes.AI_ConditionNode)

function AI_InSkillRange:__ctor__(skillLibIndex)
    self._skillLibIndex = skillLibIndex
end

function AI_InSkillRange:Proc(theOwner)
    return theOwner:ProcInSkillRange(self._skillLibIndex)
end

local AI_CmpHorizontalDistance = Class.AI_CmpHorizontalDistance(ClassTypes.AI_ConditionNode)

function AI_CmpHorizontalDistance:__ctor__(cmpType,distance)
    self._distance = distance
    self._cmpType = cmpType
end

function AI_CmpHorizontalDistance:Proc(theOwner)
    local hDistance = theOwner:GetHorizontalDistance() * 100
    return AIUtils:CmpTypeMethod(self._cmpType,hDistance,self._distance)
end

local AI_CmpVerticalDistance = Class.AI_CmpVerticalDistance(ClassTypes.AI_ConditionNode)

function AI_CmpVerticalDistance:__ctor__(cmpType,distance)
    self._distance = distance
    self._cmpType = cmpType
end

function AI_CmpVerticalDistance:Proc(theOwner)
    local vDistance = theOwner:GetVerticalDistance() * 100
    return AIUtils:CmpTypeMethod(self._cmpType,vDistance,self._distance)
end

local AI_CmpSelfHP = Class.AI_CmpSelfHP(ClassTypes.AI_ConditionNode)

function AI_CmpSelfHP:__ctor__(cmpType,rate)
    self._rate = rate
    self._cmpType = cmpType
end

function AI_CmpSelfHP:Proc(theOwner)
    local rate = theOwner:GetCurHPRate() * 100
    return AIUtils:CmpTypeMethod(self._cmpType,rate,self._rate)
end

local AI_CmpTargetHP = Class.AI_CmpTargetHP(ClassTypes.AI_ConditionNode)

function AI_CmpTargetHP:__ctor__(cmpType,rate)
    self._rate = rate
    self._cmpType = cmpType
end

function AI_CmpTargetHP:Proc(theOwner)
    local rate = theOwner:GetTargetHPRate()
    return AIUtils:CmpTypeMethod(self._cmpType,rate,self._rate)
end

local AI_CmpNumberOfEnemy = Class.AI_CmpNumberOfEnemy(ClassTypes.AI_ConditionNode)

function AI_CmpNumberOfEnemy:__ctor__(cmpType,count,range)
    self._count = count
    self._cmpType = cmpType
    self._range = range
end

function AI_CmpNumberOfEnemy:Proc(theOwner)
    local count = theOwner:GetNumberOfEnemyInRange(self._range)
    return AIUtils:CmpTypeMethod(self._cmpType,count,self._count)
end

local AI_CheckSelfBuffId = Class.AI_CheckSelfBuffId(ClassTypes.AI_ConditionNode)

function AI_CheckSelfBuffId:__ctor__(buffId)
    self._buffId = buffId
end

function AI_CheckSelfBuffId:Proc(theOwner)
    return theOwner:ProcCheckSelfBuffId(self._buffId)
end

local AI_CheckTargetBuffId = Class.AI_CheckTargetBuffId(ClassTypes.AI_ConditionNode)

function AI_CheckTargetBuffId:__ctor__(buffId)
    self._buffId = buffId
end

function AI_CheckTargetBuffId:Proc(theOwner)
    return theOwner:ProcCheckTargetBuffId(self._buffId)
end

local AI_CheckSelfMarkId = Class.AI_CheckSelfMarkId(ClassTypes.AI_ConditionNode)

function AI_CheckSelfMarkId:__ctor__(markId)
    self._markId = markId
end

function AI_CheckSelfMarkId:Proc(theOwner)
    return theOwner:ProcCheckSelfMarkId(self._markId)
end