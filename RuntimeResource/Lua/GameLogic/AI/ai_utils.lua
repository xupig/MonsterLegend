
local ai_utils = {
    BB_REMAIN_TICK = 1,
    BB_TARGET_CHECK = 2,
    BB_TARGET_EID = 3,
    BB_IN_BATTLE = 4,
    BB_AGGRO_VAL = 5,
    BB_UNUSE_SKILL_REMAIN_TICK = 6,
    BB_LOOK_ON_MODE = 7,
    BB_LOOK_ON_REMAIN_ACT_COUNT = 8,
    BB_LAST_PATROL_POS = 9,
    BB_PATH_IDX = 10,
    BB_PATH_CFG_INFO = 11,
    BB_WANDER_PATH = 12,
    BB_TEST_COORDS = 13,
    BB_ESCAPE_INFO = 14,
    BB_ZFLAG_INFO = 15,
    BB_LAST_PATROL_FACE = 16,
    BB_REMAIN_CAN_MOVE_TIME = 17,
    BB_LAST_MOVE_TAR_POS = 18,
    BB_PET_SKILL = 19,
    BB_NEW_TRAGET_LAST_POS = 20,



    BT_RNT_COMPLETE     = 0,
    BT_RNT_PREFAIL      = 1,
    BT_RNT_EXECUTING    = 2,
    BT_RNT_ABORT        = 3,

    ONE_TICK_MS = 100,
    ONE_SEC_MS = 1000,
    ONE_TICK_COST_SEC = 0.5,

    LOOK_ON_NONE = 0,
    LOOK_ON_REST = 1,
    LOOK_ON_LEFT_AROUND = 2,
    LOOK_ON_RIGHT_AROUND = 3,
    LOOK_ON_CLOSE_TO = 4,
    LOOK_ON_AWAY_TO = 5,

    THREAT_REASON_HP_CHANGE = 1,
    THREAT_REASON_CHANGE_AGGRO = 2,
    THREAT_REASON_SEARCH_TARGET = 3,
    THREAT_REASON_BY_BUFF = 4,

    THINK_IMM_REASON_MOVE_COMPLETE = 1,
    THINK_IMM_REASON_CAST_COMPLETE = 2,

    THINK_IMM_ROOT_REASON_CAST_COMPLETE = 1,
}


ai_utils.Switch = {[1] = function(lv,rv) return lv < rv end,
                  [2] = function(lv,rv) return lv <= rv end,
                  [3] = function(lv,rv) return lv == rv end,
                  [4] = function(lv,rv) return lv >= rv end,
                  [5] = function(lv,rv) return lv > rv end }


function ai_utils:CmpTypeMethod(cmpType,lv,rv)
    local func = self.Switch[cmpType]
    if func == nil then return false end
    return func(lv,rv)
end


return ai_utils