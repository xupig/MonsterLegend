local ai_utils = require "AI/ai_utils"
local mgr_entity_ai_cell = require "AI/mgr_entity_ai_cell"

local BT_RNT_COMPLETE   = ai_utils.BT_RNT_COMPLETE
local BT_RNT_PREFAIL    = ai_utils.BT_RNT_PREFAIL
local BT_RNT_EXECUTING  = ai_utils.BT_RNT_EXECUTING
local BT_RNT_ABORT      = ai_utils.BT_RNT_ABORT


---------------------------------------------------------
local bt_cmp_entity_num = bt.bt_condition:new()

function bt_cmp_entity_num:new(faction_type, cmp, num, dis)
    local tmp = {_faction_type = faction_type, _cmp = cmp, _num = num, _dis = dis}
    setmetatable(tmp, {__index = bt_cmp_entity_num})

    return tmp
end

function bt_cmp_entity_num:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_entity_num(entity, self._faction_type, self._cmp, self._num, self._dis)
end

---------------------------------------------------------
local bt_cmp_rate = bt.bt_condition:new()

function bt_cmp_rate:new(cmp, num)
    local tmp = {_cmp = cmp, _num = num}
    setmetatable(tmp, {__index = bt_cmp_rate})

    return tmp
end

function bt_cmp_rate:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_rate(entity, self._cmp, self._num)
end

---------------------------------------------------------
local bt_cmp_hp = bt.bt_condition:new()

function bt_cmp_hp:new(target_type, target_type_param, cmp, num)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _cmp = cmp, _num = num}
    setmetatable(tmp, {__index = bt_cmp_hp})

    return tmp
end

function bt_cmp_hp:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_hp(entity, self._target_type, self._target_type_param, self._cmp, self._num)
end

---------------------------------------------------------
local bt_cmp_target_dis = bt.bt_condition:new()

function bt_cmp_target_dis:new(target_type, target_type_param, cmp, dis)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _cmp = cmp, _dis = dis}
    setmetatable(tmp, {__index = bt_cmp_target_dis})

    return tmp
end

function bt_cmp_target_dis:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_target_dis(entity, self._target_type, self._target_type_param, self._cmp, self._dis)
end

---------------------------------------------------------
local bt_has_buff = bt.bt_condition:new()

function bt_has_buff:new(target_type, target_type_param, buff_id)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _buff_id = buff_id}
    setmetatable(tmp, {__index = bt_has_buff})

    return tmp
end

function bt_has_buff:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_has_buff(entity, self._target_type, self._target_type_param, self._buff_id)
end

---------------------------------------------------------
local bt_cmp_has_buff_entity_num = bt.bt_condition:new()

function bt_cmp_has_buff_entity_num:new(faction_type, cmp, num, buff_id)
    local tmp = {_faction_type = faction_type, _cmp = cmp, _num = num, _buff_id = buff_id}
    setmetatable(tmp, {__index = bt_cmp_has_buff_entity_num})

    return tmp
end

function bt_cmp_has_buff_entity_num:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_has_buff_entity_num(entity, self._faction_type, self._cmp, self._num, self._buff_id)
end

---------------------------------------------------------
local bt_check_dead = bt.bt_condition:new()

function bt_check_dead:new(target_type, target_type_param)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param}
    setmetatable(tmp, {__index = bt_check_dead})

    return tmp
end

function bt_check_dead:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_check_dead(entity, self._target_type, self._target_type_param)
end

---------------------------------------------------------
local bt_change_aggro = bt.bt_condition:new()

function bt_change_aggro:new(target_type, target_type_param)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param}
    setmetatable(tmp, {__index = bt_change_aggro})

    return tmp
end

function bt_change_aggro:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_change_aggro(entity, self._target_type, self._target_type_param)
end

---------------------------------------------------------
local bt_can_cast = bt.bt_condition:new()

function bt_can_cast:new(skill_id)
    local tmp = {_skill_id = skill_id}
    setmetatable(tmp, {__index = bt_can_cast})

    return tmp
end

function bt_can_cast:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_can_cast(entity, self._skill_id)
end

---------------------------------------------------------
local bt_player_in_attack = bt.bt_condition:new()

function bt_player_in_attack:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_player_in_attack})

    return tmp
end

function bt_player_in_attack:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_player_in_attack(entity)
end

---------------------------------------------------------
local bt_cmp_attri = bt.bt_condition:new()

function bt_cmp_attri:new(target_type, target_type_param, attri_name, cmp, num)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _attri_name = attri_name, _cmp = cmp, _num = num}
    setmetatable(tmp, {__index = bt_cmp_attri})

    return tmp
end

function bt_cmp_attri:tick_proc(entity)
    return mgr_entity_ai_cell:proc_bt_cmp_attri(entity, self._target_type, self._target_type_param, self._attri_name, 
    self._cmp, self._num)
end

---------------------------------------------------------
bt.bt_cmp_entity_num = bt_cmp_entity_num
bt.bt_cmp_rate = bt_cmp_rate
bt.bt_cmp_hp = bt_cmp_hp
bt.bt_cmp_target_dis = bt_cmp_target_dis
bt.bt_has_buff = bt_has_buff
bt.bt_cmp_has_buff_entity_num = bt_cmp_has_buff_entity_num
bt.bt_check_dead = bt_check_dead
bt.bt_change_aggro = bt_change_aggro
bt.bt_can_cast = bt_can_cast
bt.bt_player_in_attack = bt_player_in_attack
bt.bt_cmp_attri = bt_cmp_attri
