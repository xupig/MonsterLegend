--具体的行为实现类,叶子节点
local AI_ActionNode = Class.AI_ActionNode(ClassTypes.IBehaviorTreeNode)

function AI_ActionNode:Proc(theOwner)
    return false
end

--简单测试用例
local AI_DoTest1 = Class.AI_DoTest1(ClassTypes.AI_ActionNode)

function AI_DoTest1:Proc(theOwner)
    theOwner:ProcDoTest1()
    return true
end

local AI_DoTest2 = Class.AI_DoTest2(ClassTypes.AI_ActionNode)

function AI_DoTest2:Proc(theOwner)
    theOwner:ProcDoTest2()
    return true
end

local AI_DoTest3 = Class.AI_DoTest3(ClassTypes.AI_ActionNode)

function AI_DoTest3:Proc(theOwner)
    theOwner:ProcDoTest3()
    return true
end

local AI_MoveToTarget = Class.AI_MoveToTarget(ClassTypes.AI_ActionNode)

function AI_MoveToTarget:__ctor__(distance)
    self._distance = distance
end

function AI_MoveToTarget:Proc(theOwner)
    theOwner:ProcMoveToTarget(self._distance)
    return true
end

local AI_SearchTarget = Class.AI_SearchTarget(ClassTypes.AI_ActionNode)

function AI_SearchTarget:__ctor__(distance,targetType,reSearchProbability)
    self._distance = distance
    self._targetType = targetType
    self._reSearchProbability = reSearchProbability
end

function AI_SearchTarget:Proc(theOwner)
    return theOwner:ProcSearchTarget(self._distance,self._targetType,self._reSearchProbability)
    --return true
end

local AI_AddMark = Class.AI_AddMark(ClassTypes.AI_ActionNode)

function AI_AddMark:__ctor__(markId)
    self._markId = markId
end

function AI_AddMark:Proc(theOwner)
    theOwner:ProcAddMark(self._markId)
    return true
end

local AI_DeleteMark = Class.AI_DeleteMark(ClassTypes.AI_ActionNode)

function AI_DeleteMark:__ctor__(markId)
    self._markId = markId
end

function AI_DeleteMark:Proc(theOwner)
    theOwner:ProcDeleteMark(self._markId)
    return true
end

local AI_CastSpell = Class.AI_CastSpell(ClassTypes.AI_ActionNode)

function AI_CastSpell:__ctor__(skillId)
    self._skillId = skillId
end

function AI_CastSpell:Proc(theOwner)
    theOwner:ProcCastSpell(self._skillId)
    return true
end

local AI_LookOn = Class.AI_LookOn(ClassTypes.AI_ActionNode)

function AI_LookOn:__ctor__(maxDistance,mixDistance,goHeadProbability,goBackProbability,goLeftProbability,goRightProbability,
                            goHeadTime,goBackTime,goLeftTime,goRightTime,stopTime)
    self._probabilityArray = {goHeadProbability,goBackProbability,goLeftProbability,goRightProbability}
    self._timeArray = {goHeadTime * 0.001,goBackTime * 0.001,goLeftTime * 0.001,goRightTime * 0.001}
    self._maxDistance = maxDistance * 0.01
    self._minDistance = mixDistance * 0.01
    self._stopTime = stopTime
end

function AI_LookOn:Proc(theOwner)   
    theOwner:ProcLookOn(self._maxDistance,self._minDistance,self._probabilityArray,self._timeArray,self._stopTime)
    return true
end

local AI_Runaway = Class.AI_Runaway(ClassTypes.AI_ActionNode)

function AI_Runaway:__ctor__(distance)
    self._distance = distance
end

function AI_Runaway:Proc(theOwner)
    theOwner:ProcRunaway(self._distance)
    return true
end

local AI_Patrol = Class.AI_Patrol(ClassTypes.AI_ActionNode)

function AI_Patrol:__ctor__(radius)
    self._radius = radius   
end

function AI_Patrol:Proc(theOwner)
    theOwner:ProcPatrol(self._radius)
    return true
end

local AI_Sleep = Class.AI_Sleep(ClassTypes.AI_ActionNode)

function AI_Sleep:__ctor__(stopTime)
    self._stopTime = stopTime
end

function AI_Sleep:Proc(theOwner)
    theOwner:ProcSleep(self._stopTime)
    return true
end

local AI_DeleteTarget = Class.AI_DeleteTarget(ClassTypes.AI_ActionNode)

function AI_DeleteTarget:Proc(theOwner)
    theOwner:ProcDeleteTarget()
    return true
end

local AI_AddBuff = Class.AI_AddBuff(ClassTypes.AI_ActionNode)

function AI_AddBuff:__ctor__(buffId)
    self._buffId = buffId
end

function AI_AddBuff:Proc(theOwner)
    theOwner:ProcAddBuff(self._buffId)
    return true
end

local AI_DeleteBuff = Class.AI_DeleteBuff(ClassTypes.AI_ActionNode)

function AI_DeleteBuff:__ctor__(buffId)
    self._buffId = buffId
end

function AI_DeleteBuff:Proc(theOwner)
    theOwner:ProcDeleteBuff(self._buffId)
    return true
end

local AI_MoveToPoint = Class.AI_MoveToPoint(ClassTypes.AI_ActionNode)

function AI_MoveToPoint:__ctor__(x,z,y)
    self._x = x
    self._y = y
    self._z = z
end

function AI_MoveToPoint:Proc(theOwner)
    theOwner:ProcMoveToPoint(self._x,self._z,self._y)
    return true
end