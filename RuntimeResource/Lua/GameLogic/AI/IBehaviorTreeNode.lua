--行为树节点基类
local IBehaviorTreeNode = Class.IBehaviorTreeNode(ClassTypes.XObject)

function IBehaviorTreeNode:__ctor__()
end

function IBehaviorTreeNode:Proc(theOwner)
    return false
end

--导出的AI逻辑继承这个类
local BehaviorTreeRoot = Class.BehaviorTreeRoot(ClassTypes.IBehaviorTreeNode)

function BehaviorTreeRoot:AddChild(root)
    self.root = root
end

function BehaviorTreeRoot:Proc(theOwner)
    return self.root:Proc(theOwner)
end