local ai_utils = require "AI/ai_utils"
local mgr_entity_aggro_cell = require "AI/mgr_entity_aggro_cell"

local AIPathDataHelper = GameDataHelper.AIPathDataHelper
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local EntityType = GameConfig.EnumType.EntityType
local SpaceEventManager = GameManager.SpaceEventManager
local GameSceneManager = GameManager.GameSceneManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local SkillManager = PlayerManager.SkillManager
--local bagData = PlayerManager.PlayerDataManager.bagData
local Vector3 = Vector3

local BT_RNT_COMPLETE   = ai_utils.BT_RNT_COMPLETE
local BT_RNT_PREFAIL    = ai_utils.BT_RNT_PREFAIL
local BT_RNT_EXECUTING  = ai_utils.BT_RNT_EXECUTING
local BT_RNT_ABORT      = ai_utils.BT_RNT_ABORT

local math = math
local sqrt  = math.sqrt

local cmp_switch = {
    [1] = function(lv,rv) return lv < rv end,
    [2] = function(lv,rv) return lv <= rv end,
    [3] = function(lv,rv) return lv == rv end,
    [4] = function(lv,rv) return lv >= rv end,
    [5] = function(lv,rv) return lv > rv end,
}


local mgr_entity_ai_cell = {}

function mgr_entity_ai_cell:set_reach_idx(entity_ai, new_reach_idx)
    local owner_entity = entity_ai:GetOwner()
    local ai_reach_paths = SpaceEventManager._reach_path_idx
    ai_reach_paths[owner_entity.id] = new_reach_idx 
end

function mgr_entity_ai_cell:get_reach_idx(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local ai_reach_paths = SpaceEventManager._reach_path_idx
    local reach_idx = ai_reach_paths[owner_entity.id]
    
    if not reach_idx then
        reach_idx = 0
        ai_reach_paths[owner_entity.id] = reach_idx
    end
    return reach_idx  
end

function mgr_entity_ai_cell:debug_str(entity_ai,str)
    -- body
end

function mgr_entity_ai_cell:get_bt(id)

    return GameWorld.btTable[id]
end

function mgr_entity_ai_cell:reset_entity_data(entity_ai)
    
end

function mgr_entity_ai_cell:rereset_entity_data(entity_ai)
    --entity.executing = 0

end

function mgr_entity_ai_cell:init_entity_data(entity_ai)

    local owner_entity = entity_ai:GetOwner()

    local new_bb = {

        [ai_utils.BB_REMAIN_TICK] = 0,
        [ai_utils.BB_TARGET_CHECK] = {},
        [ai_utils.BB_TARGET_EID] = {},
        [ai_utils.BB_IN_BATTLE] = 0,
        [ai_utils.BB_AGGRO_VAL] = {},
        [ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] = 0,
        [ai_utils.BB_LOOK_ON_MODE] = ai_utils.LOOK_ON_NONE,
        [ai_utils.BB_LOOK_ON_REMAIN_ACT_COUNT] = 0,
        [ai_utils.BB_LAST_PATROL_POS] = {},
        [ai_utils.BB_WANDER_PATH] = {},
        [ai_utils.BB_ESCAPE_INFO] = {0, 0},
        [ai_utils.BB_LAST_PATROL_FACE] = 0,
        [ai_utils.BB_REMAIN_CAN_MOVE_TIME] = 0,
        [ai_utils.BB_LAST_MOVE_TAR_POS] = nil,
        [ai_utils.BB_NEW_TRAGET_LAST_POS] = nil,
    }

    entity_ai.blackboard = new_bb
    entity_ai.sensor_info = {}
    entity_ai.bt_node = {}
    entity_ai.executing = 0
end

function mgr_entity_ai_cell:on_loop_end(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    bb[ai_utils.BB_TARGET_CHECK] = {} --思考前清空targetcheck
end

function mgr_entity_ai_cell:start_ai(entity_ai, ai_id)
    
    local bb = entity_ai.blackboard
    local new_bb = {
        [ai_utils.BB_REMAIN_TICK] = 0,
        [ai_utils.BB_TARGET_CHECK] = {},
        [ai_utils.BB_TARGET_EID] = {},
        [ai_utils.BB_IN_BATTLE] = 0,
        [ai_utils.BB_AGGRO_VAL] = bb[ai_utils.BB_AGGRO_VAL],
        [ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] = 0,
        [ai_utils.BB_LOOK_ON_MODE] = ai_utils.LOOK_ON_NONE,
        [ai_utils.BB_LOOK_ON_REMAIN_ACT_COUNT] = 0,
        [ai_utils.BB_LAST_PATROL_POS] = bb[ai_utils.BB_LAST_PATROL_POS],
        [ai_utils.BB_WANDER_PATH] = {},
        [ai_utils.BB_ESCAPE_INFO] = {0, 0},
        [ai_utils.BB_ZFLAG_INFO] = bb[ai_utils.BB_ZFLAG_INFO]or {},
        [ai_utils.BB_LAST_PATROL_FACE] = bb[ai_utils.BB_LAST_PATROL_FACE],
        [ai_utils.BB_REMAIN_CAN_MOVE_TIME] = 0,
        [ai_utils.BB_LAST_MOVE_TAR_POS] = nil,
        [ai_utils.BB_NEW_TRAGET_LAST_POS] = bb[ai_utils.BB_NEW_TRAGET_LAST_POS],
    }

    entity_ai.blackboard = new_bb
    entity_ai.sensor_info = {}
    entity_ai.bt_node = {}
    entity_ai.executing = 0


    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local tmp_bt_node = self:get_bt(ai_id)

    if not tmp_bt_node then
        -- LoggerHelper.Log({"mgr_entity_ai_cell:start_ai", "find not found ai_id=%d", ai_id})
        return false
    end

    entity_ai.cur_ai_id = ai_id
    entity_ai.bt_node = tmp_bt_node

    return true
end

--主角找目标
function mgr_entity_ai_cell:get_target_by_player(entity_ai, target_type)
    GameWorld.FindTargetByAI()
    local targetEntity
    local bb = entity_ai.blackboard
    local owner_entity = entity_ai:GetOwner()
    local targetId = owner_entity:GetTargetId()
    if targetId ~= 0 then
        targetEntity = GameWorld.GetEntity(targetId)
        bb[ai_utils.BB_TARGET_EID][target_type] = targetId
    else
        bb[ai_utils.BB_TARGET_EID][target_type] = nil
    end
    bb[ai_utils.BB_TARGET_CHECK][target_type] = 1

    return targetEntity
end

function mgr_entity_ai_cell:get_target(entity_ai, target_type, param)
    if not target_type then
        target_type = bt.ai_target_type.self
    end

    local bb = entity_ai.blackboard
    
    if bb[ai_utils.BB_TARGET_CHECK][target_type] then
        return GameWorld.GetEntity(bb[ai_utils.BB_TARGET_EID][target_type])
    end

    bb[ai_utils.BB_TARGET_EID][target_type] = nil

    if bt.ai_target_type.aggro == target_type then
        
        local owner_entity = entity_ai:GetOwner()
        local p_target_eid = owner_entity:get_special_prior_target_eid()
        if p_target_eid then
            bb[ai_utils.BB_TARGET_EID][target_type] = p_target_eid
        else
            local new_aggro_eid = mgr_entity_aggro_cell:get_target(entity_ai, 0, 1)
            bb[ai_utils.BB_TARGET_EID][target_type] = new_aggro_eid
        end

    elseif bt.ai_target_type.self == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = entity_ai:GetOwner().id

    elseif bt.ai_target_type.master == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = entity_ai:GetOwner().owner_id

    elseif bt.ai_target_type.servant == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        bb[ai_utils.BB_TARGET_EID][target_type] = nil
        for k, v in pairs(entities) do
            if v.owner_id and v.owner_id == owner_entity.id then
                bb[ai_utils.BB_TARGET_EID][target_type] = k
                break
            end
        end

    elseif bt.ai_target_type.master_target == target_type then
        local master_entity = GameWorld.GetEntity(entity_ai:GetOwner().owner_id)
        if master_entity ~= nil then
            bb[ai_utils.BB_TARGET_EID][target_type] = master_entity:GetTargetId()
        end
    elseif bt.ai_target_type.drop == target_type then
        local entities = GameWorld.Entities()
        local owner_entity = entity_ai:GetOwner()
        local range = param * 0.01
        local min_range = range
        local ownerPos = owner_entity:GetPosition()
        for k, v in pairs(entities) do
            if v.entityType == EntityType.SpaceDrop and v:GetCanPick() then
                local dis = self:ai_distance(ownerPos, v:GetPosition())
                if dis < range and self:is_config_drop_entity(entity_ai, v) then
                    if dis < min_range then
                        min_range = dis
                        bb[ai_utils.BB_TARGET_EID][target_type] = v.id
                    end
                    --bb[ai_utils.BB_TARGET_EID][target_type] = v.id
                    --break
                end
            end
        end

    elseif bt.ai_target_type.hp_lowest == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = mgr_entity_aggro_cell:get_target_cur_hp_in_range(entity_ai, 0, 1, param) or 0


    elseif bt.ai_target_type.hp_highest == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = mgr_entity_aggro_cell:get_target_cur_hp_in_range(entity_ai, 1, 1, param) or 0


    elseif bt.ai_target_type.closest == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = mgr_entity_aggro_cell:get_target_dis(entity_ai, 1, 1) or 0


    elseif bt.ai_target_type.farthest == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = mgr_entity_aggro_cell:get_target_dis(entity_ai, 0, 1) or 0


    elseif bt.ai_target_type.has_buff == target_type then
        bb[ai_utils.BB_TARGET_EID][target_type] = mgr_entity_aggro_cell:get_target_has_buff(entity_ai, param) or 0

    elseif bt.ai_target_type.friend_random      == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_entity:Faction() and
                    v.cur_hp > 0 and v.id ~= owner_entity.id then
                   -- self:ai_distance(owner_entity:GetPosition(), v:GetPosition()) <= (range_cm * 0.01) then
                bb[ai_utils.BB_TARGET_EID][target_type] = v.id
                break
            end
        end

    elseif bt.ai_target_type.friend_hp_lowest   == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local tar_eid = nil
        local lowest_hp = 9999999
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_entity:Faction() and
                    v.cur_hp < lowest_hp and v.cur_hp > 0 and v.id ~= owner_entity.id then
                -- self:ai_distance(owner_entity:GetPosition(), v:GetPosition()) <= (range_cm * 0.01) then
                tar_eid = v.id
                lowest_hp = v.cur_hp
            end
        end
        bb[ai_utils.BB_TARGET_EID][target_type] = tar_eid

    elseif bt.ai_target_type.friend_hp_highest  == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local tar_eid = nil
        local highest_hp = 0
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_entity:Faction() and
                    v.cur_hp > highest_hp and v.id ~= owner_entity.id then
                -- self:ai_distance(owner_entity:GetPosition(), v:GetPosition()) <= (range_cm * 0.01) then
                tar_eid = v.id
                highest_hp = v.cur_hp
            end
        end
        bb[ai_utils.BB_TARGET_EID][target_type] = tar_eid

    elseif bt.ai_target_type.friend_closest     == target_type then

        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local tar_eid = nil
        local closest_dis = 9999999
        local ownerPos = owner_entity:GetPosition()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_entity:Faction() and
                    v.cur_hp > 0 and v.id ~= owner_entity.id then
                local dis = self:ai_distance(ownerPos, v:GetPosition())
                if dis <= closest_dis then
                    tar_eid = v.id
                    closest_dis = dis
                end
            end
        end

        bb[ai_utils.BB_TARGET_EID][target_type] = tar_eid

    elseif bt.ai_target_type.friend_farthest    == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local tar_eid = nil
        local farthest_dis = 0
        local ownerPos = owner_entity:GetPosition()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_entity:Faction() and
                    v.cur_hp > 0 and v.id ~= owner_entity.id then
                local dis = self:ai_distance(ownerPos, v:GetPosition())
                if dis >= farthest_dis then
                    tar_eid = v.id
                    farthest_dis = dis
                end
            end
        end
        bb[ai_utils.BB_TARGET_EID][target_type] = tar_eid

    elseif bt.ai_target_type.friend_has_buff    == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local buff_id = param
        for k, v in pairs(entities) do
            if v.isCreature and
                    v:Faction() == owner_entity:Faction() and
                    v.cur_hp > 0 and
                    v:ContainsBuffer(buff_id) and
                    v.id ~= owner_entity.id then
                bb[ai_utils.BB_TARGET_EID][target_type] = v.id
                break
            end
        end

    elseif bt.ai_target_type.friend_range_random == target_type then
        local owner_entity = entity_ai:GetOwner()
        local entities = GameWorld.Entities()
        local range = param * 0.01
        local ownerPos = owner_entity:GetPosition()
        for k, v in pairs(entities) do
            if v.isCreature and
                    v:Faction() == owner_entity:Faction() and
                    v.cur_hp > 0 and
                    v.id ~= owner_entity.id then
                local dis = self:ai_distance(ownerPos, v:GetPosition())
                if dis <= range then
                    bb[ai_utils.BB_TARGET_EID][target_type] = v.id
                    break
                end
            end
        end

    end



    bb[ai_utils.BB_TARGET_CHECK][target_type] = 1

    return GameWorld.GetEntity(bb[ai_utils.BB_TARGET_EID][target_type])
end


function mgr_entity_ai_cell:update(entity_ai)

    entity_ai.sensor_info = {}

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    --LoggerHelper.Error("-------mgr_entity_ai_cell:update------11---"..tostring(owner_entity.ai_id))
    

    -- bb[ai_utils.BB_TARGET_CHECK] = {}--思考前清空targetcheck


    if owner_entity.entityType == EntityType.Pet then

    elseif owner_entity.entityType == EntityType.PlayerAvatar then
        if self:need_owner_off_play(entity_ai) then
            bb[ai_utils.BB_AGGRO_VAL] = {}
            self:start_ai(entity_ai, owner_entity.owner_off_play_args[1])
        end
    else
        if self:need_off_play(entity_ai) then
            bb[ai_utils.BB_AGGRO_VAL] = {}
            self:start_ai(entity_ai, owner_entity.off_play_args[1])
        end
    end

    

    if not entity_ai.bt_node then
        return
    end
    if not entity_ai.bt_node.tick_start then
        return
    end
    
    --LoggerHelper.Error("-------mgr_entity_ai_cell:update------22------"..tostring(entity_ai.bt_node))

    return entity_ai.bt_node:tick_start(entity_ai)
end

function mgr_entity_ai_cell:cmp_to(cmp_type, lv, rv)
    local func = cmp_switch[cmp_type]
    if func == nil then return false end
    return func(lv, rv)
end

--接口不明，先返回true
function mgr_entity_ai_cell:can_move(owner_entity)
    return owner_entity.cs:CanActiveMove()
end

function mgr_entity_ai_cell:reset_unuse_skill_remain_tick(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local off_play_args = owner_entity.off_play_args
    if not off_play_args then
        return
    end
    local bb = entity_ai.blackboard
    bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] = math.ceil(off_play_args[2]/entity_ai:one_tick_ms())
end

function mgr_entity_ai_cell:need_off_play(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    local off_play_args = owner_entity.off_play_args


    if not off_play_args then
        return false --没有脱战AI
    end

    --必须在主ai中
    if entity_ai.cur_ai_id ~= owner_entity.ai_id then 
        return false
    end

    if bb[ai_utils.BB_IN_BATTLE] == 0 then 
        return false --不在战斗状态
    end

    --必须不在使用技能中(没这样的接口)
    --if owner_entity:HavePlayingSkill() then
        --return false
    --end

    --仇恨列表内没目标了
    if not self:get_target(entity_ai, bt.ai_target_type.aggro) then
        return true
    end


    --没使用技能时长判断
    bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] = bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] - 1

    if bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] < 0 then
        return true
    end

    --出生点距离判断
    local born_pos = entity_ai.born_pos
    local dis_to_born = self:ai_distance(owner_entity:GetPosition(), born_pos)
    if dis_to_born >= (off_play_args[3] * 0.01) then
        return true
    end

    return false
end

function mgr_entity_ai_cell:need_owner_off_play(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local off_play_args = owner_entity.owner_off_play_args
    if not off_play_args then 
        return false
    end

    --必须在主ai中
    if entity_ai.cur_ai_id ~= owner_entity.ai_id then 
        return false
    end

    --与上次索敌位置距离判断
    local bb = entity_ai.blackboard
    local last_pos = bb[ai_utils.BB_NEW_TRAGET_LAST_POS]
    if last_pos ~= nil then 
        local dis = self:ai_distance(owner_entity:GetPosition(), last_pos)
        if dis >= off_play_args[2] then
            return true
        end
    end

    return false
end


function mgr_entity_ai_cell:pet_need_off_play(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    local off_play_args = owner_entity.off_play_args

    if not off_play_args then
        return false --没有脱战AI
    end

    --必须在主ai中
    --print("mgr_entity_ai_cell:pet_need_off_play1 ",entity_ai.cur_ai_id,owner_entity.ai_id)
    if entity_ai.cur_ai_id ~= owner_entity.ai_id then 
        return false
    end

    --print("mgr_entity_ai_cell:pet_need_off_play 2 ",bb[ai_utils.BB_IN_BATTLE])
    if bb[ai_utils.BB_IN_BATTLE] == 0 then 
        return false --不在战斗状态
    end


    --print("mgr_entity_ai_cell:pet_need_off_play 3 ")
    --仇恨列表内没目标了
    if not self:get_target(entity_ai, bt.ai_target_type.aggro) then
        return true
    end


    --print("mgr_entity_ai_cell:pet_need_off_play 4 ",bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK])
    --没使用技能时长判断
    bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] = bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] - 1
    if bb[ai_utils.BB_UNUSE_SKILL_REMAIN_TICK] < 0 then
        return true
    end

    local owner_id = owner_entity:GetOwnerId()

    --print("mgr_entity_ai_cell:pet_need_off_play 5",owner_id)
    --查找主人
    local master_entity = GameWorld.GetEntity(owner_id)
    if not master_entity then
        return false
    end

    local dis = self:ai_distance(master_entity:GetPosition(), owner_entity:GetPosition())
    if dis >= (off_play_args[3] * 0.01) then
        return true
    end


    return false
end

function mgr_entity_ai_cell:move_to_path_point(entity_ai, tar_pos)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    if not self:can_move(owner_entity) then
        return
    end
    if bb[ai_utils.BB_LAST_MOVE_TAR_POS] and
            self:ai_distance(bb[ai_utils.BB_LAST_MOVE_TAR_POS], tar_pos) <= 0.5 and 
            owner_entity:IsMoving() then
        --和上一个点一样
        if bb[ai_utils.BB_REMAIN_CAN_MOVE_TIME] > 0 and
                bb[ai_utils.BB_REMAIN_CAN_MOVE_TIME] > Time.realtimeSinceStartup then
            return
        end
    end


    local new_pos = {}
    for k, v in pairs(tar_pos) do
        new_pos[k] = v
    end

    if owner_entity.entityType == EntityType.PlayerAvatar then 
        owner_entity:Move(tar_pos, -2, nil, nil, true)
    else
        owner_entity:Move(tar_pos, owner_entity:GetSpeed(), nil, nil, true)
    end
    bb[ai_utils.BB_LAST_MOVE_TAR_POS] = new_pos
    bb[ai_utils.BB_REMAIN_CAN_MOVE_TIME] = Time.realtimeSinceStartup + 10
end

function mgr_entity_ai_cell:stop_move(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    bb[ai_utils.BB_REMAIN_CAN_MOVE_TIME] = 0
    bb[ai_utils.BB_LAST_MOVE_TAR_POS] = nil
    owner_entity:StopMove()
end

function mgr_entity_ai_cell:move_to_pos(entity_ai, tar_pos, stop_dis)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

   
    if not self:can_move(owner_entity) then
        return
    end

    if bb[ai_utils.BB_LAST_MOVE_TAR_POS] and
            self:ai_distance(bb[ai_utils.BB_LAST_MOVE_TAR_POS], tar_pos) <= 0.5 and
            owner_entity:IsMoving() then
        --正在往上一个目标点节点走，新目标点也是上一个目标点，那么不调用move了
        return
    end

    local new_pos = {}
    for k, v in pairs(tar_pos) do
        new_pos[k] = v
    end

    if stop_dis then
        stop_dis = stop_dis - 0.2--0.2是不要太绝对碰到界限值，再近一点点好些
    end    

    owner_entity:Move(tar_pos, owner_entity:GetSpeed(), function() self:on_move_to_pos_callback(entity_ai) end, stop_dis, true)

    bb[ai_utils.BB_LAST_MOVE_TAR_POS] = new_pos
    bb[ai_utils.BB_REMAIN_CAN_MOVE_TIME] = 0
   
end

function mgr_entity_ai_cell:on_move_to_pos_callback(entity_ai)
    entity_ai:set_think_imm_flag(1, ai_utils.THINK_IMM_REASON_MOVE_COMPLETE)--触发立刻思考
end

--离自身最近的加上仇恨值
function mgr_entity_ai_cell:on_bt_move_avatar_and_robot_search(entity_ai, target_type, target_type_param)
    local owner_entity = entity_ai:GetOwner()
    if not owner_entity.need_on_bt_move_search then
        return
    end

    local bb = entity_ai.blackboard

    local target_entity = self:get_target(entity_ai, target_type, target_type_param)
    if not target_entity then
        return
    end

    local select_not_main = 0

    local ownerPos = owner_entity:GetPosition()
    local dis_to_target_entity = self:ai_distance(ownerPos, target_entity:GetPosition())

    local entities = GameWorld.Entities()
    local closest_dis = dis_to_target_entity
    local closest_eid = 0
    for k, v in pairs(entities) do
        if v.isCreature and v:Faction() ~= owner_entity:Faction() and v.cur_hp > 0 and v.id ~= target_entity.id then
            local dis = self:ai_distance(ownerPos, v:GetPosition())
            if dis < closest_dis then

                if select_not_main and select_not_main == 0 and v:GetDelTargetLock() then
                    --策划填不选非主目标的怪物但这是一个非主目标的怪物
                else
                    closest_dis = dis
                    closest_eid = v.id
                end
            end
        end
    end
    if closest_eid > 0 then

        bb[ai_utils.BB_AGGRO_VAL] = {}
        bb[ai_utils.BB_IN_BATTLE] = 1

        mgr_entity_aggro_cell:add_threat(entity_ai, closest_eid, 1, ai_utils.THREAT_REASON_SEARCH_TARGET)

        return
    end
end

function mgr_entity_ai_cell:ai_distance(pos1, pos2)
  
    local pos = Vector3(pos2['x'], pos1['y'], pos2['z'])

    return Vector3.Distance(pos1, pos)
end

function mgr_entity_ai_cell:need_block(entity_ai,is_be_attack,attack_type)

    --[[
    local owner_entity = entity_ai:GetOwner()
    local block_ai_args = owner_entity.block_ai_args

    --脱战AI，受击,剩余血量值，暴击
    if not block_ai_args then
        return false
    end

    --必须在主ai中
    if entity_ai.cur_ai_id ~= owner_entity.ai_id then
        return false
    end

    --必须不在使用技能中
    --if owner_entity:HavePlayingSkill() then
        --return false
    --end


    --判断帧受击
    if block_ai_args[2] > 0 then
        if not is_be_attack then
            return false
        end

        --是被打了再判断暴击
        if block_ai_args[4] > 0 and
           attack_type ~= SkillConfig.ATTACK_TYPE_CRITICAL then
            return false
        end

        if math.random(1, 100) > block_ai_args[5] then
            return false
        end
    end
    --]]
    return true
end

--判断是否是设定的自动拾取类型
function mgr_entity_ai_cell:is_config_drop_entity(entity_ai, entity)
    local setting = entity_ai.hang_up_setting
    local itemId = entity.itemId
    local value = setting[public_config.AUTO_PICK_WHITE]
    -- local isFull = itemId > public_config.MAX_MONEY_ID and bagData:GetPkg(public_config.PKG_TYPE_ITEM):IsFull()
    -- if isFull then
    --     return false
    -- end
    if value == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 1 then
            return true
        end
    end

    value = setting[public_config.AUTO_PICK_BLUE]
    if value == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 3 then
            return true
        end
    end

    value = setting[public_config.AUTO_PICK_PURPLE]
    if value == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality == 4 then
            return true
        end
    end

    value = setting[public_config.AUTO_PICK_ORANGE]
    if value == 1 then
        local itemType = ItemDataHelper.GetItemType(itemId)
        local quality = ItemDataHelper.GetQuality(itemId)
        if itemType == public_config.ITEM_ITEMTYPE_EQUIP and quality >= 5 then
            return true
        end
    end

    --金币
    value = setting[public_config.AUTO_PICK_GOLD]
    if value == 1 then
        if itemId == public_config.MONEY_TYPE_GOLD then
            return true
        end
    end

    value = setting[public_config.AUTO_PICK_OTHER]
    if value == 1 then
        return true
    end

    return false
end


----------------------start:action nodes-----------------------
local BB_PATH_CFG_TYPE_PATH = 1 --路径
local BB_PATH_CFG_TYPE_WANDER = 2 --游荡
local BB_PATH_CFG_TYPE_IDLE = 3 --休息

local BB_PATH_CFG_INFO_KEY_TYPE = 1
local BB_PATH_CFG_INFO_KEY_TYPE_ID = 2
local BB_PATH_CFG_INFO_KEY_ARG_1 = 3
local BB_PATH_CFG_INFO_KEY_ARG_2 = 4
local BB_PATH_CFG_INFO_KEY_ID = 5


function mgr_entity_ai_cell:pre_bt_search_target(entity_ai, patrol_type, patrol_range_radius, patrol_range_interval_ms, patrol_path_id, range_cm, select_not_main)


    --LoggerHelper.Error("-------mgr_entity_ai_cell:pre_bt_search_target------11------"..tostring(patrol_type))

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    if range_cm == 1 then
        --range_cm==1 1cm是特殊的 就是瞎了 永远不会找到敌人 0cm就是被动怪，被打还是可以反击的
    else
        if self:get_target(entity_ai, bt.ai_target_type.aggro) then
 
            return BT_RNT_COMPLETE
        end
    end

    if bt.ai_patrol_type.range == patrol_type then
        bb[ai_utils.BB_REMAIN_TICK] = 0
    elseif bt.ai_patrol_type.path == patrol_type then
        local cfg_id = AIPathDataHelper.GetRandomSerial(patrol_path_id)
        bb[ai_utils.BB_PATH_CFG_INFO] = {}

        if cfg_id then
            bb[ai_utils.BB_PATH_CFG_INFO][BB_PATH_CFG_INFO_KEY_ID] = cfg_id
            local cfg_team = AIPathDataHelper.GetAIPathTeamCfgData(cfg_id)
            local cfg_type = cfg_team['type']
            local cfg_type_id = cfg_team['type_id']


            if cfg_type == BB_PATH_CFG_TYPE_PATH then
                --init
                bb[ai_utils.BB_REMAIN_TICK] = 0
                bb[ai_utils.BB_PATH_IDX] = 0
                local cfg_poss = AIPathDataHelper.GetPoss(cfg_type_id)

                local min_dis = 99900
                local tar_idx = 0
                local ownerPos = owner_entity:GetPosition()
                for k, pos in pairs(cfg_poss) do
                    local cur_dis = self:ai_distance(ownerPos, pos)
                    if cur_dis < min_dis then
                        tar_idx = k
                        min_dis = cur_dis
                    end
                end
                if tar_idx > 0 then
                    bb[ai_utils.BB_PATH_IDX] = tar_idx
                else
                    bb[ai_utils.BB_PATH_CFG_INFO] = {}
                end
            elseif cfg_type == BB_PATH_CFG_TYPE_WANDER then
                --init
                local cfg_info = AIPathDataHelper.GetAIWanderCfgData(cfg_type_id)
                if self:pre_bt_wander(entity_ai, cfg_info['wander_max_range'],
                    cfg_info['wander_min_interval'],
                    cfg_info['wander_max_interval'],
                    cfg_info['wander_times_min'],
                    cfg_info['wander_times_max']) ~= BT_RNT_EXECUTING then
                    bb[ai_utils.BB_PATH_CFG_INFO] = {}
                end

            elseif cfg_type == BB_PATH_CFG_TYPE_IDLE then
                --init
                local cfg_info = AIPathDataHelper.GetAIRelaxCfgData(cfg_type_id)
                local time_ms = math.random(cfg_info['relax_min_time'], cfg_info['relax_max_time'])
                if self:pre_bt_idle(entity_ai, time_ms) ~= BT_RNT_EXECUTING then
                    bb[ai_utils.BB_PATH_CFG_INFO] = {}
                end
            end
        end
    end

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_search_target(entity_ai, patrol_type, patrol_range_radius, patrol_range_interval_ms, patrol_path_id, range_cm, select_not_main)

    local param_str = string.format( "patrol_type=%s, patrol_range_radius=%s, patrol_range_interval_ms=%s, patrol_path_id=%s, range_cm=%s, select_not_main=%s,ai_id=%s",
    patrol_type, patrol_range_radius, patrol_range_interval_ms, patrol_path_id, range_cm, select_not_main,entity_ai:GetOwner().ai_id)


    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    --用眼睛找人
    if range_cm == 1 then
        --range_cm==1 1cm是特殊的 就是瞎了 永远不会找到敌人 0cm就是被动怪，被打还是可以反击的
    else
        if self:get_target(entity_ai, bt.ai_target_type.aggro) then
            return BT_RNT_COMPLETE
        else

            local entities = GameWorld.Entities()
            local random_list = {}
            local ownerPos = owner_entity:GetPosition()
            for k, v in pairs(entities) do
                if v.isCreature and v:Faction() ~= owner_entity:Faction() and v.cur_hp > 0 and
                        self:ai_distance(ownerPos, v:GetPosition()) <= (range_cm * 0.01) then
                    
                    if select_not_main and select_not_main == 0 and v:GetDelTargetLock() then
                        --策划填不选非主目标的怪物但这是一个非主目标的怪物
                    else
                        table.insert(random_list, v.id)

                    end
                end
            end

            if #random_list > 0 then
                local random_eid = random_list[math.random(1, #random_list)]

                bb[ai_utils.BB_IN_BATTLE] = 1

                mgr_entity_aggro_cell:add_threat(entity_ai, random_eid, 1, ai_utils.THREAT_REASON_SEARCH_TARGET)

                self:reset_unuse_skill_remain_tick(entity_ai)

                bb[ai_utils.BB_LAST_PATROL_POS] = {x=owner_pos.x, y=owner_pos.y, z=owner_pos.z}
                bb[ai_utils.BB_LAST_PATROL_FACE] = owner_entity:GetFace()
                return BT_RNT_COMPLETE
            end
        end
    end


    --巡逻:范围内
    if bt.ai_patrol_type.range == patrol_type then

        local remain_tick = bb[ai_utils.BB_REMAIN_TICK]

        if remain_tick <= 0 then
            if self:can_move(owner_entity) then
                local patrol_radius = patrol_range_radius
                local x = math.random(patrol_radius * -1, patrol_radius)
                local z = math.random(patrol_radius * -1, patrol_radius)

                local curPos = owner_entity:GetPosition()
                --entity_ai.born_pos
                local pos = Vector3(x * 0.01 + curPos.x, curPos.y, z * 0.01 + curPos.z)

                self:move_to_pos(entity_ai, pos)
                
                local use_time_ms = math.random(patrol_range_interval_ms/10, patrol_range_interval_ms)
                bb[ai_utils.BB_REMAIN_TICK] = math.ceil(use_time_ms/entity_ai:one_tick_ms())
            end
        else
            remain_tick = remain_tick - 1

            bb[ai_utils.BB_REMAIN_TICK] = remain_tick
        end

    elseif bt.ai_patrol_type.path == patrol_type then


        local bb_path_info = bb[ai_utils.BB_PATH_CFG_INFO]
        local cfg_id = bb_path_info[BB_PATH_CFG_INFO_KEY_ID]

        local cfg_team = nil
        if cfg_id then
            cfg_team = AIPathDataHelper.GetAIPathTeamCfgData(cfg_id)
        end

        local need_next = false

        if cfg_team then
            local cfg_type = cfg_team['type']
            local cfg_type_id = cfg_team['type_id']

            if cfg_type == BB_PATH_CFG_TYPE_PATH then
                --go on
                if bb[ai_utils.BB_PATH_IDX] ~= 0 then
                    local cfg_poss = AIPathDataHelper.GetPoss(cfg_type_id)
                    local tar_pos = cfg_poss[bb[ai_utils.BB_PATH_IDX]]
                    local dis = self:ai_distance(tar_pos, owner_entity:GetPosition())

                    local stop_distance = 2
                    if dis <= stop_distance then

                        bb[ai_utils.BB_PATH_IDX] = bb[ai_utils.BB_PATH_IDX] + 1
                        if bb[ai_utils.BB_PATH_IDX] > #cfg_poss then
                            --到了最后的尾点了
                            bb[ai_utils.BB_PATH_IDX] = 0
                            self:stop_move(entity_ai)
                            need_next = true
                        else
                            --还没到尾点，跑向下一个点
                            tar_pos = cfg_poss[bb[ai_utils.BB_PATH_IDX]]
                            self:move_to_pos(entity_ai, tar_pos)
                        end
                    else
                        --还没到目的地
                        self:move_to_pos(entity_ai, tar_pos)

                    end
                end
            elseif cfg_type == BB_PATH_CFG_TYPE_WANDER then
                --go on
                local cfg_info = AIPathDataHelper.GetAIWanderCfgData(cfg_type_id)
                if mgr_entity_ai_cell:update_bt_wander(entity_ai, cfg_info['wander_max_range'],
                    cfg_info['wander_min_interval'],
                    cfg_info['wander_max_interval'],
                    cfg_info['wander_times_min'],
                    cfg_info['wander_times_max']) ~= BT_RNT_EXECUTING then
                    need_next = true
                end
            elseif cfg_type == BB_PATH_CFG_TYPE_IDLE then
                --go on
                if mgr_entity_ai_cell:update_bt_idle(entity_ai) ~= BT_RNT_EXECUTING then
                    need_next = true
                end
            end

            if need_next then
                local cfg_next_ids = cfg_team['next_id']
                if cfg_next_ids then
                    local next_cfg_id = cfg_next_ids[math.random(1, #cfg_next_ids)]
                    cfg_team = AIPathDataHelper.GetAIPathTeamCfgData(next_cfg_id)
                    cfg_id = next_cfg_id
                    cfg_type = cfg_team['type']
                    cfg_type_id = cfg_team['type_id']
                    if cfg_id then
                        bb[ai_utils.BB_PATH_CFG_INFO] = {
                            [BB_PATH_CFG_INFO_KEY_ID] = cfg_id,
                        }

                        if cfg_type == BB_PATH_CFG_TYPE_PATH then
                            --init
                            bb[ai_utils.BB_REMAIN_TICK] = 0
                            bb[ai_utils.BB_PATH_IDX] = 0
                            local cfg_poss = AIPathDataHelper.GetPoss(cfg_type_id)

                            local min_dis = 99900
                            local tar_idx = 0
                            local ownerPos = owner_entity:GetPosition()
                            for k, pos in pairs(cfg_poss) do
                                local cur_dis = self:ai_distance(ownerPos, pos)
                                if cur_dis < min_dis then
                                    tar_idx = k
                                    min_dis = cur_dis
                                end
                            end
                            if tar_idx > 0 then
                                bb[ai_utils.BB_PATH_IDX] = tar_idx
                            else
                                bb[ai_utils.BB_PATH_CFG_INFO] = {}
                            end
                        elseif cfg_type == BB_PATH_CFG_TYPE_WANDER then
                            --init
                            local cfg_info = AIPathDataHelper.GetAIWanderCfgData(cfg_type_id)

                            if mgr_entity_ai_cell:pre_bt_wander(entity_ai, cfg_info['wander_max_range'],
                                cfg_info['wander_min_interval'],
                                cfg_info['wander_max_interval'],
                                cfg_info['wander_times_min'],
                                cfg_info['wander_times_max']) ~= BT_RNT_EXECUTING then
                                bb[ai_utils.BB_PATH_CFG_INFO] = {}
                            end

                        elseif cfg_type == BB_PATH_CFG_TYPE_IDLE then
                            --init
                            local cfg_info = AIPathDataHelper.GetAIRelaxCfgData(cfg_type_id)
                            local time_ms = math.random(cfg_info['relax_min_time'], cfg_info['relax_max_time'])
                            if mgr_entity_ai_cell:pre_bt_idle(entity_ai, time_ms) ~= BT_RNT_EXECUTING then
                                bb[ai_utils.BB_PATH_CFG_INFO] = {}
                            end
                        end

                    else
                        bb[ai_utils.BB_PATH_CFG_INFO] = {}
                    end
                else
                    bb[ai_utils.BB_PATH_CFG_INFO] = {}
                end
            end
        end
    end


    return BT_RNT_EXECUTING
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_cast(entity_ai, target_type, target_type_param, skill_ids_index, face_target, check_cd, check_dis, go_public_cd)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    --if owner_entity.entityType == EntityType.Pet then 
        --print("mgr_entity_ai_cell:pre_bt_cast 1 ",target_type, target_type_param, skill_ids_index, face_target, check_cd, check_dis, go_public_cd)
    --end
    
    local target_entity = self:get_target(entity_ai, target_type, target_type_param)--无目标用self
    if not target_entity then
        return BT_RNT_PREFAIL
    end

    if owner_entity.entityType == EntityType.PlayerAvatar then 
        if not SkillManager:GetSkillsAutoCastStateByAI(skill_ids_index) then
            return BT_RNT_PREFAIL
        end
    end

    if not owner_entity:SkillCanPlayByPos(skill_ids_index) then
        return BT_RNT_PREFAIL
    end

    --[[
    if check_dis == 1 then
    
        local target_radius = target_entity:GetBoxRadius()
        local dis = self:ai_distance(target_entity:GetPosition(), owner_entity:GetPosition())
        local skill_range = owner_entity:GetSkillAIRangeByPos(skill_ids_index) * 0.01

        if dis - target_radius > skill_range then

            return BT_RNT_PREFAIL
        end
    end
    --]]
    owner_entity:SetTargetId(target_entity.id)
    if face_target == 1 then
        owner_entity:FaceToPosition(target_entity:GetPosition())
    end

    --cut_time要先获得，因为使用了技能之后有可能同一个skill_ids_index会换技能，得到的cut_time就不是之前使用技能的了
    local cut_time = owner_entity:GetActionCutTimeByPos(skill_ids_index)

    owner_entity:PlaySkillByPos(skill_ids_index)
    owner_entity:ReleaseSkillByPos(skill_ids_index)

    --bb[ai_utils.BB_REMAIN_TICK] = math.ceil(cut_time/entity_ai:one_tick_ms())
    bb[ai_utils.BB_REMAIN_TICK] = 0

    self:reset_unuse_skill_remain_tick(entity_ai)
    entity_ai.casting = 1
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_cast(entity_ai)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    bb[ai_utils.BB_REMAIN_TICK] = bb[ai_utils.BB_REMAIN_TICK] - 1
    if bb[ai_utils.BB_REMAIN_TICK] <= 0 then
        bb[ai_utils.BB_REMAIN_TICK] = 0
        entity_ai:set_imm_root_flag(1, ai_utils.THINK_IMM_ROOT_REASON_CAST_COMPLETE)
        return BT_RNT_COMPLETE
    end

    return BT_RNT_EXECUTING
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_move_to_target(entity_ai, target_type, target_type_param, skill_idx, stop_distance)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_move_to_target(entity_ai, target_type, target_type_param, skill_idx, stop_distance)

    local owner_entity = entity_ai:GetOwner()

    local bb = entity_ai.blackboard


    if not self:can_move(owner_entity) then
        return BT_RNT_EXECUTING
    end

    self:on_bt_move_avatar_and_robot_search(entity_ai, target_type, target_type_param)

    local target_entity = self:get_target(entity_ai, target_type, target_type_param)
    if not target_entity then
        return BT_RNT_PREFAIL
    end
    
    
    if skill_idx > 0 then--技能槽id优先考虑     
        stop_distance = owner_entity:GetSkillAIRangeByPos(skill_idx)
    end
    if stop_distance < 0 then
        return BT_RNT_PREFAIL
    end


    stop_distance = stop_distance * 0.01

    local target_pos = target_entity:GetPosition()
    --local target_radius = target_entity:GetBoxRadius()
    local dis = self:ai_distance(target_pos, owner_entity:GetPosition())
    local dis_max =  stop_distance
    --local dis_min = target_radius + stop_distance * 0.5

    if dis <= stop_distance then
        self:stop_move(entity_ai)
        return BT_RNT_COMPLETE
    end

    --owner_entity:Move(target_pos, owner_entity:GetSpeed(), nil, nil, true)
    
    self:move_to_pos(entity_ai, target_pos, dis_max)

    return BT_RNT_EXECUTING
end

------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_idle(entity_ai, time_ms)

    local bb = entity_ai.blackboard

    bb[ai_utils.BB_REMAIN_TICK] = math.ceil(time_ms/entity_ai:one_tick_ms())

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_idle(entity_ai, time_ms)

    local owner_entity = entity_ai:GetOwner()
    
    local bb = entity_ai.blackboard
    local remain_tick_idle = bb[ai_utils.BB_REMAIN_TICK]

    remain_tick_idle = remain_tick_idle - 1

    if remain_tick_idle < 0 then
        return BT_RNT_COMPLETE
    end

    bb[ai_utils.BB_REMAIN_TICK] = remain_tick_idle

    return BT_RNT_EXECUTING
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_speech(entity_ai, id)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_speech(entity_ai, id)
    return BT_RNT_COMPLETE
end

-----------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_monster_speech(entity_ai, monster_id, chinese_id, exist_time_sec)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_monster_speech(entity_ai, monster_id, chinese_id, exist_time_sec)

    --local send_tbl = {['monster_id']=monster_id, ['chinese_id']=chinese_id, ['exist_time']=exist_time_sec,}
    --EventDispatcher:TriggerEvent(GameEvents.ActionUIMonsterSpeak, send_tbl)
    return BT_RNT_COMPLETE
end
------------------------------------------------------------
--休息 左绕 右绕 接近 远离
function mgr_entity_ai_cell:pre_bt_look_on(entity_ai, to_distance_min, to_distance_max, weights, use_times_ms)


    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    if not self:can_move(owner_entity) then
        return BT_RNT_COMPLETE
    end

    if not self:get_target(entity_ai, bt.ai_target_type.aggro) then
        return BT_RNT_COMPLETE
    end

    local bb = entity_ai.blackboard
    bb[ai_utils.BB_LOOK_ON_REMAIN_ACT_COUNT] = 1
    bb[ai_utils.BB_LOOK_ON_MODE] = ai_utils.LOOK_ON_NONE

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_look_on(entity_ai, to_distance_min, to_distance_max, weights, use_times_ms)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    local cur_mode = bb[ai_utils.BB_LOOK_ON_MODE]
    local remain_act_count = bb[ai_utils.BB_LOOK_ON_REMAIN_ACT_COUNT]

    local target_entity = self:get_target(entity_ai, bt.ai_target_type.aggro)
    if not target_entity then
        return BT_RNT_COMPLETE
    end

    to_distance_min = to_distance_min * 0.01
    to_distance_max = to_distance_max * 0.01

    if cur_mode > ai_utils.LOOK_ON_NONE then
        --继续计时


        local remain_tick = bb[ai_utils.BB_REMAIN_TICK]

        remain_tick = remain_tick - 1

        if remain_tick < 0 then
            --走完了
            self:stop_move(entity_ai)
            if remain_act_count <= 0 then
                --行为次数用完

                return BT_RNT_COMPLETE
            end
        else
            --还没消耗完时间
            bb[ai_utils.BB_REMAIN_TICK] = remain_tick

            if cur_mode == ai_utils.LOOK_ON_CLOSE_TO and
                self:ai_distance(target_entity:GetPosition(), owner_entity:GetPosition()) < to_distance_min then
                self:stop_move(entity_ai)
                if remain_act_count <= 0 then
                    --行为次数用完
                    return BT_RNT_COMPLETE
                end
                --走太近了 重新规划 不return
            elseif  cur_mode == ai_utils.LOOK_ON_AWAY_TO and
                    self:ai_distance(target_entity:GetPosition(), owner_entity:GetPosition()) > to_distance_max then

                self:stop_move(entity_ai)
                if remain_act_count <= 0 then
                    --行为次数用完
                    return BT_RNT_COMPLETE
                end
                --走太远了 重新规划 不return
            else
                --继续走
                return BT_RNT_EXECUTING
            end
        end
    end

    --重新规划


    local to_target_dis = self:ai_distance(owner_entity:GetPosition(), target_entity:GetPosition())
    local last_mode = cur_mode

    local sum = 0
    local new_weights = {}
    for mode, w in pairs(weights) do
        table.insert(new_weights, w)
    end
    --new_weights[ai_utils.LOOK_ON_REST] = 0
    --new_weights[ai_utils.LOOK_ON_LEFT_AROUND] = 0
    --new_weights[ai_utils.LOOK_ON_RIGHT_AROUND] = 0
    if to_target_dis >= to_distance_max then
        --不能远离
        new_weights[ai_utils.LOOK_ON_AWAY_TO] = 0
    end
    if to_target_dis <= to_distance_min then
        --不能接近
        new_weights[ai_utils.LOOK_ON_CLOSE_TO] = 0
        new_weights[ai_utils.LOOK_ON_LEFT_AROUND] = 0
        new_weights[ai_utils.LOOK_ON_RIGHT_AROUND] = 0
    end
    --不执行上一次执行的
    --if last_mode >= ai_utils.LOOK_ON_NONE then
    --    new_weights[last_mode] = 0
    --end

    for mode, w in pairs(new_weights) do
        sum = sum + w
    end

    local random_w = math.random(1, sum)
    sum = 0

    for mode, w in pairs(new_weights) do
        sum = sum + w
        if sum >= random_w then
            cur_mode = mode
            break
        end
    end

    bb[ai_utils.BB_LOOK_ON_MODE] = cur_mode
    local target_entity_eid = target_entity.id

    local use_time_ms = math.random(use_times_ms[cur_mode*2-1], use_times_ms[cur_mode*2])

    local use_time_sec = use_time_ms * 0.001
    bb[ai_utils.BB_REMAIN_TICK] = math.ceil(use_time_ms/entity_ai:one_tick_ms())

    if cur_mode == ai_utils.LOOK_ON_REST then

    elseif cur_mode == ai_utils.LOOK_ON_LEFT_AROUND then
        owner_entity:MoveByDirection(target_entity_eid, -1 * owner_entity:GetSpeed(), use_time_sec, true)
    elseif cur_mode == ai_utils.LOOK_ON_RIGHT_AROUND then
        owner_entity:MoveByDirection(target_entity_eid, owner_entity:GetSpeed(),      use_time_sec, true)
    elseif cur_mode == ai_utils.LOOK_ON_CLOSE_TO then
        owner_entity:MoveByDirection(target_entity_eid, owner_entity:GetSpeed(),      use_time_sec, false)
    elseif cur_mode == ai_utils.LOOK_ON_AWAY_TO then
        owner_entity:MoveByDirection(target_entity_eid, -1 * owner_entity:GetSpeed(), use_time_sec, false)
    end

    remain_act_count = remain_act_count - 1
    bb[ai_utils.BB_LOOK_ON_REMAIN_ACT_COUNT] = remain_act_count
    return BT_RNT_EXECUTING
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_off_play(entity_ai)

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_off_play(entity_ai)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard
    local last_patrol_pos = bb[ai_utils.BB_LAST_PATROL_POS]


    local stop_distance = 200
    stop_distance = stop_distance * 0.01
    if self:ai_distance(last_patrol_pos, owner_entity:GetPosition()) <= 1 then

        self:stop_move(entity_ai)
       
        owner_entity:Teleport(last_patrol_pos)
        owner_entity:SetFace(bb[ai_utils.BB_LAST_PATROL_FACE])
        return BT_RNT_COMPLETE
    end

    self:move_to_pos(entity_ai, last_patrol_pos)
   
    return BT_RNT_EXECUTING
end
------------------------------------------------------------
function mgr_entity_ai_cell:update_bt_add_buff(entity_ai, target_type, target_type_param, buff_id, duration)
  
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then
        return BT_RNT_PREFAIL
    end

    if entity_target:AddBuff(buff_id, duration) then
        return BT_RNT_COMPLETE
    end

    return BT_RNT_PREFAIL
end

function mgr_entity_ai_cell:update_bt_del_buff_self(entity_ai, buff_id)

    local owner_entity = entity_ai:GetOwner()

    owner_entity:RemoveBuffer(buff_id)
    return BT_RNT_COMPLETE
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_pet_search_target(entity_ai, stop_distance, range_cm, select_not_main)


    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_pet_search_target(entity_ai, stop_distance, range_cm, select_not_main)
    --暂时只做了跟随的逻辑

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local owner_id = owner_entity:GetOwnerId()


    --查找主人
    local master_entity = GameWorld.GetEntity(owner_id)
    if not master_entity then
        return BT_RNT_PREFAIL
    end

    if self:get_target(entity_ai, bt.ai_target_type.aggro) then
        bb[ai_utils.BB_IN_BATTLE] = 1
        return BT_RNT_COMPLETE
    end

    local dis = self:ai_distance(master_entity:GetPosition(), owner_entity:GetPosition())

    local offset_pos = master_entity:GetPosition()
    if dis > 40 then
        owner_entity:Teleport(offset_pos)
    elseif dis > 3 then
        self:move_to_pos(entity_ai, offset_pos)
    end
    
    return BT_RNT_COMPLETE
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_owner_search_target(entity_ai, range_cm, select_not_main)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_owner_search_target(entity_ai, range_cm, select_not_main)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local target_type = bt.ai_target_type.aggro
    local lastTargetId = bb[ai_utils.BB_TARGET_EID][target_type]

    if self:get_target_by_player(entity_ai, target_type) then
        if lastTargetId ~= bb[ai_utils.BB_TARGET_EID][target_type] then
            bb[ai_utils.BB_NEW_TRAGET_LAST_POS] = owner_entity:GetPosition()
        end
        return BT_RNT_COMPLETE
    end
    bb[ai_utils.BB_NEW_TRAGET_LAST_POS] = nil

    --寻路点逻辑
    if not self:can_move(owner_entity) then
        return BT_RNT_EXECUTING
    end

    local reach_pp_idx = self:get_reach_idx(entity_ai)
    local next_pp_idx = reach_pp_idx + 1
    local pp_seq = GameSceneManager:GetPathPointSeq()

    local next_pp_id = pp_seq[next_pp_idx]

    --print("update_bt_owner_search_target ",reach_pp_idx,pp_seq,next_pp_id)

    if next_pp_id then

        local pp = GameSceneManager:GetPathPoints()
        local next_pp_info = pp[next_pp_id]
        if next_pp_info then

                --没完成路点，需要继续靠近
                local tar_pos = {x = next_pp_info.pos.x,
                                 y = next_pp_info.pos.y,
                                 z = next_pp_info.pos.z }

                if self:ai_distance(tar_pos, owner_entity:GetPosition()) <= 1.5 then

                    self:stop_move(entity_ai)
                else
                    self:move_to_path_point(entity_ai, tar_pos)
                end
            --end
        end
    end

    --return BT_RNT_EXECUTING
    return BT_RNT_PREFAIL
end
------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_escape(entity_ai, time_ms, speed_percent)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    if not self:can_move(owner_entity) then
        return BT_RNT_PREFAIL
    end

    --反方向跑 计时间
    local target_entity = self:get_target(entity_ai, bt.ai_target_type.aggro)
    if not target_entity then
        return BT_RNT_COMPLETE
    end

    local speed = owner_entity:GetSpeed() * (speed_percent * 0.0001)
    local use_time_sec = time_ms 
    
    bb[ai_utils.BB_REMAIN_TICK] = math.ceil(use_time_sec /entity_ai:one_tick_ms())

    owner_entity:MoveToDirection(target_entity:GetPosition(), -1 * speed, use_time_sec+0.5, true)


    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_escape(entity_ai, time_ms, speed_percent)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local remain_tick_idle = bb[ai_utils.BB_REMAIN_TICK]

    remain_tick_idle = remain_tick_idle - 1


    if remain_tick_idle < 0 then
        return BT_RNT_COMPLETE
    end


    bb[ai_utils.BB_REMAIN_TICK] = remain_tick_idle

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:pre_bt_remove_aggro_list(entity_ai)
    local bb = entity_ai.blackboard
    bb[ai_utils.BB_AGGRO_VAL] = {}
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:update_bt_remove_aggro_list(entity_ai)
    return BT_RNT_COMPLETE
end

------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_wander(entity_ai, range_cm_max, time_ms_per_duan_min, time_ms_per_duan_max, duan_min, duan_max)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    if not self:can_move(owner_entity) then
        return BT_RNT_COMPLETE
    end

    local path_tbl = {}
    local duan_count = math.random(duan_min, duan_max)
    local cur_pos = owner_entity:GetPosition()
    for i=1, duan_count do
        local x = math.random(range_cm_max * -1, range_cm_max)
        local z = math.random(range_cm_max * -1, range_cm_max)

        local new_pos = Vector3(x * 0.01 + cur_pos.x, cur_pos.y, z * 0.01 + cur_pos.z)
        table.insert(path_tbl, new_pos)
    end
    bb[ai_utils.BB_WANDER_PATH] = path_tbl

    if #bb[ai_utils.BB_WANDER_PATH] <= 0 then
        return BT_RNT_COMPLETE
    end

    local path = bb[ai_utils.BB_WANDER_PATH]
    local tar_pos = path[1]
    local speed = owner_entity:GetSpeed()
    local time_ms = math.random(time_ms_per_duan_min, time_ms_per_duan_max)
    local use_time_sec = time_ms * 0.001

    bb[ai_utils.BB_REMAIN_TICK] = math.ceil(time_ms/entity_ai:one_tick_ms())
   
    owner_entity:MoveToDirection(tar_pos, speed, use_time_sec, true)

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_wander(entity_ai, range_cm_max, time_ms_per_duan_min, time_ms_per_duan_max, duan_min, duan_max)
    
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local path = bb[ai_utils.BB_WANDER_PATH]

    local remain_tick_idle = bb[ai_utils.BB_REMAIN_TICK]
    remain_tick_idle = remain_tick_idle - 1

    if remain_tick_idle < 0 then--or (self:ai_distance(owner_entity:GetPosition(), path_center_pos) > (range_cm_max * 0.01))
        table.remove(path, 1)
        if #path <= 0 then
            self:stop_move(entity_ai)
            return BT_RNT_COMPLETE
        end

        local tar_pos = path[1]
        local speed = owner_entity:GetSpeed()
        local time_ms = math.random(time_ms_per_duan_min, time_ms_per_duan_max)
        local use_time_sec = time_ms * 0.001
        bb[ai_utils.BB_REMAIN_TICK] = math.ceil(time_ms/entity_ai:one_tick_ms())
        self:stop_move(entity_ai)
        owner_entity:MoveToDirection(tar_pos, speed, use_time_sec, true)
       
        return BT_RNT_EXECUTING
    end

    bb[ai_utils.BB_REMAIN_TICK] = remain_tick_idle

    return BT_RNT_EXECUTING
end

-------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_move_to_pos(entity_ai, x_m, y_m, z_m)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_move_to_pos(entity_ai, x_m, y_m, z_m)
    local owner_entity = entity_ai:GetOwner()
    if not self:can_move(owner_entity) then
        return BT_RNT_EXECUTING
    end

    local stop_distance = 1
    local target_pos = Vector3(x_m, y_m, z_m)
    local dis = self:ai_distance(target_pos, owner_entity:GetPosition())
    if dis <= stop_distance then
        self:stop_move(entity_ai)
        return BT_RNT_COMPLETE
    end

    self:move_to_pos(entity_ai, target_pos)

    return BT_RNT_EXECUTING
end

-------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_guard_search_target(entity_ai, tower_monster_id, atk_player_range_cm)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_guard_search_target(entity_ai, tower_buff_id, atk_player_range_cm)
    return BT_RNT_COMPLETE
end

---------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_pet_off_play(entity_ai)
    
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_pet_off_play(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local owner_id = owner_entity:GetOwnerId()


    local master_entity = GameWorld.GetEntity(owner_id)
    if not master_entity then
        return BT_RNT_PREFAIL
    end

    local dis = self:ai_distance(master_entity:GetPosition(), owner_entity:GetPosition())

    --清空技能和战斗状态
    bb[ai_utils.BB_IN_BATTLE] = 0
    bb[ai_utils.BB_PET_SKILL] = {}

    local offset_pos = master_entity:GetPosition()
    if dis > 40 then
        owner_entity:Teleport(offset_pos)
    elseif dis > 3 then
        self:move_to_pos(entity_ai, offset_pos)
    end

    return BT_RNT_COMPLETE
end

---------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_pet_keep_dis_to_owner(entity_ai)
    
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_pet_keep_dis_to_owner(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local owner_id = owner_entity:GetOwnerId()
    local master_entity = GameWorld.GetEntity(owner_id)
    if not master_entity then
        return BT_RNT_PREFAIL
    end

    local dis = self:ai_distance(master_entity:GetPosition(), owner_entity:GetPosition())

    local offset_pos = master_entity:GetPosition()
    if dis > 40 then
        owner_entity:Teleport(offset_pos)
    elseif dis > 3 then
        self:move_to_pos(entity_ai, offset_pos)
    end

    return BT_RNT_COMPLETE
end


---------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_call_method(entity_ai, method_name, params)
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:update_bt_call_method(entity_ai, method_name, params)
    return BT_RNT_COMPLETE
end

------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_owner_off_play(entity_ai)
    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:update_bt_owner_off_play(entity_ai)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local last_pos = bb[ai_utils.BB_NEW_TRAGET_LAST_POS]
    if self:ai_distance(last_pos, owner_entity:GetPosition()) <= 1 then
        self:stop_move(entity_ai)
        return BT_RNT_COMPLETE
    end

    self:move_to_pos(entity_ai, last_pos)
   
    return BT_RNT_EXECUTING
end

---------------------------------------------------------------
function mgr_entity_ai_cell:pre_bt_pet_assist_cast(entity_ai, target_type, target_type_param,  face_target, check_cd, check_dis)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local target_entity = self:get_target(entity_ai, target_type, target_type_param)--无目标用self
    if not target_entity then
        return BT_RNT_PREFAIL
    end

    local skill_list = bb[ai_utils.BB_PET_SKILL] or {}
    if #skill_list <= 0 then
        return BT_RNT_PREFAIL
    end


    local skill_ids_index = skill_list[1]
    
    if not owner_entity:SkillCanPlayByPos(skill_ids_index) then
        return BT_RNT_PREFAIL
    end

    table.remove(skill_list,1)


    owner_entity:SetTargetId(target_entity.id)
    if face_target == 1 then
        owner_entity:FaceToPosition(target_entity:GetPosition())
    end

    local cut_time = owner_entity:GetActionCutTimeByPos(skill_ids_index)
    

    owner_entity:PlaySkillByPos(skill_ids_index)
    owner_entity:ReleaseSkillByPos(skill_ids_index)

    bb[ai_utils.BB_REMAIN_TICK] = math.ceil(cut_time/entity_ai:one_tick_ms())

    self:reset_unuse_skill_remain_tick(entity_ai)
    entity_ai.casting = 1
    return BT_RNT_EXECUTING

end

function mgr_entity_ai_cell:update_bt_pet_assist_cast(entity_ai)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    bb[ai_utils.BB_REMAIN_TICK] = bb[ai_utils.BB_REMAIN_TICK] - 1
    if bb[ai_utils.BB_REMAIN_TICK] <= 0 then
         bb[ai_utils.BB_REMAIN_TICK] = 0

        entity_ai:set_imm_root_flag(1, ai_utils.THINK_IMM_ROOT_REASON_CAST_COMPLETE)
        return BT_RNT_COMPLETE
    end

    return BT_RNT_EXECUTING
end

function mgr_entity_ai_cell:pre_bt_search_drop(entity_ai, range_cm)
    local range_dis = range_cm
    if self:get_target(entity_ai, bt.ai_target_type.drop, range_dis) then
        return BT_RNT_COMPLETE
    end
    
    return BT_RNT_PREFAIL
end

function mgr_entity_ai_cell:update_bt_search_drop(entity_ai, range_cm)
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:pre_bt_check_task(entity_ai)  
    EventDispatcher:TriggerEvent(GameEvents.AICheckTask)
    return BT_RNT_PREFAIL
end

function mgr_entity_ai_cell:update_bt_check_task(entity_ai)
    return BT_RNT_COMPLETE
end

---------------------------------------------------------------


----------------------end:action nodes-----------------------



------------------start:condition nodes-----------------------

function mgr_entity_ai_cell:proc_bt_cmp_entity_num(entity_ai, faction_type, cmp, num, dis)
    local owner_entity = entity_ai:GetOwner()

    local num2 = 0

    dis = dis * 0.01

    local entities = GameWorld.Entities()
    local ownerPos = owner_entity:GetPosition()
    if bt.ai_faction_type.enemy == faction_type then
        local owner_faction_id = owner_entity:Faction()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() ~= owner_faction_id and
                    self:ai_distance(ownerPos, v:GetPosition()) <= dis then
                num2 = num2 + 1
            end
        end
    elseif bt.ai_faction_type.teammate == faction_type then
        local owner_faction_id = owner_entity:Faction()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_faction_id and
                    self:ai_distance(ownerPos, v:GetPosition()) <= dis then
                num2 = num2 + 1
            end
        end
    else
        for k, v in pairs(entities) do
            if self:ai_distance(ownerPos, v:GetPosition()) <= dis then
                num2 = num2 + 1
            end
        end
    end

    local result = self:cmp_to(cmp, num2, num)
    if not result then
        return BT_RNT_PREFAIL
    end
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_cmp_rate(entity_ai, cmp, num)
    local num2 = math.random(1, 100)
    local result = self:cmp_to(cmp, num2, num)
    
    if result then
        return BT_RNT_COMPLETE
    else
        return BT_RNT_PREFAIL
    end
end


function mgr_entity_ai_cell:proc_bt_cmp_hp(entity_ai, target_type, target_type_param, cmp, num)


    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then

        return BT_RNT_PREFAIL
    end

    local num2 = entity_target.cur_hp / entity_target.max_hp * 100
  
    local result = self:cmp_to(cmp, num2, num)

    
    if not result then

        return BT_RNT_PREFAIL
    end

    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_cmp_target_dis(entity_ai, target_type, target_type_param, cmp, dis)

    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then
        return BT_RNT_PREFAIL
    end


    local num = dis * 0.01
    local num2 = self:ai_distance(owner_entity:GetPosition(), entity_target:GetPosition())

    local result = self:cmp_to(cmp, num2, num)

    
    if not result then
        return BT_RNT_PREFAIL
    end
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_has_buff(entity_ai, target_type, target_type_param, buff_id)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then

        return BT_RNT_PREFAIL
    end


    local result = entity_target:ContainsBuffer(buff_id)

    
    if not result then
        return BT_RNT_PREFAIL
    end

    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_cmp_has_buff_entity_num(entity_ai, faction_type, cmp, num, buff_id)
    local owner_entity = entity_ai:GetOwner()

    local num2 = 0

    local entities = GameWorld.Entities()
    if bt.ai_faction_type.enemy == faction_type then
        local owner_faction_id = owner_entity:Faction()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() ~= owner_faction_id and
                    v:ContainsBuffer(buff_id) then
                num2 = num2 + 1
            end
        end
    elseif bt.ai_faction_type.teammate == faction_type then
        local owner_faction_id = owner_entity:Faction()
        for k, v in pairs(entities) do
            if v.isCreature and v:Faction() == owner_faction_id and
                    v:ContainsBuffer(buff_id) then
                num2 = num2 + 1
            end
        end
    else
        for k, v in pairs(entities) do
            if v.isCreature and v:ContainsBuffer(buff_id) then
                num2 = num2 + 1
            end
        end

    end

    local result = self:cmp_to(cmp, num2, num)

    if not result then
        return BT_RNT_PREFAIL
    end
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_check_dead(entity_ai, target_type, target_type_param)

    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then
        return BT_RNT_PREFAIL
    end

    local result = (entity_target.cur_hp <= 0)
    if not result then
        return BT_RNT_PREFAIL
    end
    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_change_aggro(entity_ai, target_type, target_type_param)
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then
        return BT_RNT_PREFAIL
    end

    local new_aggro_eid = mgr_entity_aggro_cell:get_target(entity_ai, 0, 1)
    if new_aggro_eid then
        if new_aggro_eid == entity_target.id then
            --当前entity_target就是挑衅目标
            return BT_RNT_COMPLETE
        end


        local cur_aggro_val = bb[ai_utils.BB_AGGRO_VAL][new_aggro_eid]
        local new_aggro_val = cur_aggro_val * 2.5
        mgr_entity_aggro_cell:add_threat(entity_ai, entity_target.id, new_aggro_val, ai_utils.THREAT_REASON_CHANGE_AGGRO)
        return BT_RNT_COMPLETE
    end

    return BT_RNT_PREFAIL
end

function mgr_entity_ai_cell:proc_bt_can_cast(entity_ai, skill_ids_index)

    local owner_entity = entity_ai:GetOwner()

    if owner_entity.entityType == EntityType.PlayerAvatar then 
        if not SkillManager:GetSkillsAutoCastStateByAI(skill_ids_index) then
            return BT_RNT_PREFAIL
        end
    end

    if not owner_entity:SkillCanPlayByPos(skill_ids_index) then
        return BT_RNT_PREFAIL
    end

    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_player_in_attack(entity_ai)
    --local owner_entity = entity_ai:GetOwner()
    local player = GameWorld.Player()
    if player == nil or not player.inAttackState then
        return BT_RNT_PREFAIL
    end

    return BT_RNT_COMPLETE
end

function mgr_entity_ai_cell:proc_bt_cmp_attri(entity_ai, target_type, target_type_param, attri_name, cmp, num)
    local bb = entity_ai.blackboard

    local entity_target = self:get_target(entity_ai, target_type, target_type_param)
    if not entity_target then
        return BT_RNT_PREFAIL
    end

    if entity_target[attri_name] == nil then
        return BT_RNT_COMPLETE
    end

    local num2 = entity_target[attri_name]

    local result = self:cmp_to(cmp, num2, num)
    if not result then
        return BT_RNT_PREFAIL
    end
    return BT_RNT_COMPLETE
end

------------------end:condition nodes-----------------------

function mgr_entity_ai_cell:ai_distance(p1, p2)
    return sqrt(((p1.x - p2.x)^2 + (p1.z - p2.z)^2))
end

return mgr_entity_ai_cell
