--装饰类
local AI_DecoratorNode = Class.AI_DecoratorNode(ClassTypes.IBehaviorTreeNode)

function AI_DecoratorNode:Proc(theOwner)
    return self.child:Proc(theOwner)
end

function AI_DecoratorNode:Proxy(child)
    self.child = child
end

local AI_Not = Class.AI_Not(ClassTypes.AI_DecoratorNode)

function AI_Not:Proc(theOwner)       
    return not AI_Not._base.Proc(self,theOwner)
end

local AI_Success = Class.AI_Success(ClassTypes.AI_DecoratorNode)

function AI_Success:Proc(theOwner)
    return AI_Success._base.Proc(self,theOwner)
end    