--复合节点，不能为叶子节点
local AI_CompositeNode = Class.AI_CompositeNode(ClassTypes.IBehaviorTreeNode)

function AI_CompositeNode:__ctor__()
    self.children = {}
end

function AI_CompositeNode:Proc(theOwner)
    return true
end

function AI_CompositeNode:AddChild(node)
    table.insert(self.children,node)
end

function AI_CompositeNode:DelChild(node)
    for index = #self.children,1,-1 do
        if a[index] == node then table.remove(self.children,index)
        end
    end
end

function AI_CompositeNode:ClearChildren()
    self.children = {}
end  

local AI_PrioritySelectorNode = Class.AI_PrioritySelectorNode(ClassTypes.AI_CompositeNode)

function AI_PrioritySelectorNode:Proc(theOwner)
    for i = 1,#self.children do
        if self.children[i]:Proc(theOwner) == true then
            return true
        end
    end
    return false
end  

local AI_SelectorNode = Class.AI_SelectorNode(ClassTypes.AI_CompositeNode)

function AI_SelectorNode:Proc(theOwner)
--顺序遍历
    for i = 1,#self.children do
        if self.children[i]:Proc(theOwner) == true then
            return true
        end
    end
    return false
end

local AI_SequenceNode = Class.AI_SequenceNode(ClassTypes.AI_CompositeNode)

function AI_SequenceNode:Proc(theOwner)
    for i = 1,#self.children do
        if self.children[i]:Proc(theOwner) == false then
            return false
        end
    end
    return true
end