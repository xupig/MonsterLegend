local ai_utils = require "AI/ai_utils"
local mgr_entity_ai_cell = require "AI/mgr_entity_ai_cell"

local BT_RNT_COMPLETE   = ai_utils.BT_RNT_COMPLETE
local BT_RNT_PREFAIL    = ai_utils.BT_RNT_PREFAIL
local BT_RNT_EXECUTING  = ai_utils.BT_RNT_EXECUTING
local BT_RNT_ABORT      = ai_utils.BT_RNT_ABORT


---------------------------------------------------------
local bt_search_target = bt.bt_action:new()

function bt_search_target:new(patrol_type, patrol_range_radius, patrol_range_interval_ms, patrol_path_id, range_cm, select_not_main)
    local tmp = {_patrol_type = patrol_type,
        _patrol_range_radius = patrol_range_radius,
        _patrol_range_interval_ms = patrol_range_interval_ms,
        _patrol_path_id = patrol_path_id,
        _range_cm = range_cm,
        _select_not_main = select_not_main}
    setmetatable(tmp, {__index = bt_search_target})
    tmp.__index = tmp

    return tmp
end
function bt_search_target:to_string()
    return "bt_search_target .. " .. tostring(self.index)
end

function bt_search_target:pre_proc(entity)
    local ai_id = entity:GetOwner().ai_id
    return mgr_entity_ai_cell:pre_bt_search_target(entity, self._patrol_type, self._patrol_range_radius, self._patrol_range_interval_ms, self._patrol_path_id ,self._range_cm, self._select_not_main)
end

function bt_search_target:update_proc(entity)

    local ai_id = entity:GetOwner().ai_id
    if ai_id == 12012 then
        --print("bt_search_target:update_proc",self._patrol_type,ai_id,self._patrol_range_radius)
    end
    return mgr_entity_ai_cell:update_bt_search_target(entity, self._patrol_type, self._patrol_range_radius, self._patrol_range_interval_ms, self._patrol_path_id ,self._range_cm, self._select_not_main)
end

---------------------------------------------------------
local bt_cast = bt.bt_action:new()

function bt_cast:new(target_type, target_type_param, skill_ids_index, face_target, check_cd, check_dis, go_public_cd)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _skill_ids_index = skill_ids_index, _face_target = face_target, _check_cd = check_cd, _check_dis = check_dis, _go_public_cd = go_public_cd}
    setmetatable(tmp, {__index = bt_cast})
    tmp.__index = tmp

    return tmp
end
function bt_cast:to_string()
    return "bt_cast .. " .. tostring(self.index) .." 技能id:"..self._skill_ids_index
end

function bt_cast:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_cast(entity, self._target_type, self._target_type_param, self._skill_ids_index, self._face_target, self._check_cd, self._check_dis, self._go_public_cd)
end

function bt_cast:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_cast(entity, self._target_type, self._target_type_param, self._skill_ids_index, self._face_target, self._check_cd, self._check_dis, self._go_public_cd)
end

---------------------------------------------------------
local bt_idle = bt.bt_action:new()

function bt_idle:new(time_ms)
    local tmp = {_time_ms = time_ms}
    setmetatable(tmp, {__index = bt_idle})

    return tmp
end
function bt_idle:to_string()
    return "bt_idle .. " .. tostring(self.index)
end

function bt_idle:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_idle(entity, self._time_ms)
end

function bt_idle:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_idle(entity)
end



---------------------------------------------------------
local bt_move_to_target = bt.bt_action:new()

function bt_move_to_target:new(target_type, target_type_param, skill_idx, stop_distance)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _skill_idx = skill_idx, _stop_distance = stop_distance}
    setmetatable(tmp, {__index = bt_move_to_target})
    tmp.__index = tmp

    return tmp
end
function bt_move_to_target:to_string()
    return "bt_move_to_target .. " .. tostring(self.index)
end

function bt_move_to_target:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_move_to_target(entity, self._target_type, self._target_type_param, self._skill_idx, self._stop_distance)
end

function bt_move_to_target:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_move_to_target(entity, self._target_type, self._target_type_param, self._skill_idx, self._stop_distance)
end


---------------------------------------------------------
local bt_speech = bt.bt_action:new()

function bt_speech:new(id)
    local tmp = {_id = id}
    setmetatable(tmp, {__index = bt_speech})
    tmp.__index = tmp


    return tmp
end
function bt_speech:to_string()
    return "bt_speech .. " .. tostring(self.index)
end

function bt_speech:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_speech(entity, self._id)
end

function bt_speech:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_speech(entity, self._id)
end

----------------------------------------------------------
local bt_monster_speech = bt.bt_action:new()

function bt_monster_speech:new(id)
    local tmp = {_id = id}
    setmetatable(tmp, {__index = bt_monster_speech})
    tmp.__index = tmp

    return tmp
end
function bt_monster_speech:to_string()
    return "bt_monster_speech .. " .. tostring(self.index)
end

function bt_monster_speech:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_monster_speech(entity, self._id)
end

function bt_monster_speech:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_monster_speech(entity, self._id)
end

---------------------------------------------------------
local bt_look_on = bt.bt_action:new()

function bt_look_on:new(to_distance_min, to_distance_max, weight_mode_rest,
weight_mode_left_around, weight_mode_right_around, weight_mode_close_to, weight_mode_away_to,
time_ms_mode_rest_min, time_ms_mode_rest_max, time_ms_mode_left_around_min, time_ms_mode_left_around_max, time_ms_mode_right_around_min, time_ms_mode_right_around_max,
time_ms_mode_close_to_min, time_ms_mode_close_to_max, time_ms_mode_away_to_min, time_ms_mode_away_to_max)
    local tmp = {_to_distance_min = to_distance_min,
        _to_distance_max = to_distance_max,
        _weights = {weight_mode_rest, weight_mode_left_around, weight_mode_right_around,
            weight_mode_close_to, weight_mode_away_to},
        _use_times_ms = {time_ms_mode_rest_min, time_ms_mode_rest_max, time_ms_mode_left_around_min, time_ms_mode_left_around_max, time_ms_mode_right_around_min, time_ms_mode_right_around_max,
            time_ms_mode_close_to_min, time_ms_mode_close_to_max, time_ms_mode_away_to_min, time_ms_mode_away_to_max},}
    setmetatable(tmp, {__index = bt_look_on})
    tmp.__index = tmp

    return tmp
end
function bt_look_on:to_string()
    return "bt_look_on .. " .. tostring(self.index)
end

function bt_look_on:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_look_on(entity, self._to_distance_min, self._to_distance_max, self._weights, self._use_times_ms)
end

function bt_look_on:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_look_on(entity, self._to_distance_min, self._to_distance_max, self._weights, self._use_times_ms)
end

------------------------------------------------------
local bt_off_play = bt.bt_action:new()

function bt_off_play:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_off_play})
    tmp.__index = tmp
    return tmp
end
function bt_off_play:to_string()
    return "bt_off_play .. " .. tostring(self.index)
end

function bt_off_play:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_off_play(entity)
end

function bt_off_play:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_off_play(entity)
end

------------------------------------------------------
local bt_owner_off_play = bt.bt_action:new()

function bt_owner_off_play:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_owner_off_play})
    tmp.__index = tmp
    return tmp
end
function bt_owner_off_play:to_string()
    return "bt_owner_off_play .. " .. tostring(self.index)
end

function bt_owner_off_play:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_owner_off_play(entity)
end

function bt_owner_off_play:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_owner_off_play(entity)
end

---------------------------------------------------------
local bt_add_buff = bt.bt_action:new()

function bt_add_buff:new(target_type, target_type_param, buff_id, duration_ms)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _buff_id = buff_id, _duration_ms = duration_ms or -1}
    setmetatable(tmp, {__index = bt_add_buff})
    tmp.__index = tmp
    return tmp
end
function bt_add_buff:to_string()
    return "bt_add_buff .. " .. tostring(self.index)
end

function bt_add_buff:pre_proc(entity)
    return BT_RNT_EXECUTING
end

function bt_add_buff:update_proc(entity)
    local duration_sec = self._duration_ms
    if self._duration_ms > 0 then
        duration_sec = self._duration_ms * 0.001
    end
    return mgr_entity_ai_cell:update_bt_add_buff(entity, self._target_type, self._target_type_param, self._buff_id, duration_sec)
end
---------------------------------------------------------
local bt_del_buff_self = bt.bt_action:new()

function bt_del_buff_self:new(buff_id)
    local tmp = {_buff_id = buff_id}
    setmetatable(tmp, {__index = bt_del_buff_self})
    tmp.__index = tmp
    return tmp
end
function bt_del_buff_self:to_string()
    return "bt_del_buff_self .. " .. tostring(self.index)
end

function bt_del_buff_self:pre_proc(entity)
    return BT_RNT_EXECUTING
end

function bt_del_buff_self:update_proc(entity)

    return mgr_entity_ai_cell:update_bt_del_buff_self(entity, self._buff_id)
end

---------------------------------------------------------
local bt_pet_search_target = bt.bt_action:new()

function bt_pet_search_target:new(stop_distance, range_cm, select_not_main)
    local tmp = {_stop_distance = stop_distance, _range_cm = range_cm, _select_not_main = select_not_main}
    setmetatable(tmp, {__index = bt_pet_search_target})
    tmp.__index = tmp

    return tmp
end
function bt_pet_search_target:to_string()
    return "bt_pet_search_target .. " .. tostring(self.index)
end

function bt_pet_search_target:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_pet_search_target(entity, self._stop_distance, self._range_cm, self._select_not_main)
end

function bt_pet_search_target:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_pet_search_target(entity, self._stop_distance, self._range_cm, self._select_not_main)
end
---------------------------------------------------------
local bt_owner_search_target = bt.bt_action:new()

function bt_owner_search_target:new(range_cm, select_not_main)
    local tmp = {_range_cm = range_cm, _select_not_main = select_not_main}
    setmetatable(tmp, {__index = bt_owner_search_target})
    tmp.__index = tmp

    return tmp
end
function bt_owner_search_target:to_string()
    return "bt_owner_search_target .. " .. tostring(self.index)
end

function bt_owner_search_target:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_owner_search_target(entity, self._range_cm, self._select_not_main)
end

function bt_owner_search_target:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_owner_search_target(entity, self._range_cm, self._select_not_main)
end

---------------------------------------------------------
local bt_call_method = bt.bt_action:new()

function bt_call_method:new(method_name, params)
    local tmp = {_method_name = method_name, _params = params}
    setmetatable(tmp, {__index = bt_call_method})
    tmp.__index = tmp

    return tmp
end
function bt_call_method:to_string()
    return "bt_call_method .. " .. tostring(self.index)
end

function bt_call_method:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_call_method(entity, self._method_name, self._params)
end

function bt_call_method:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_call_method(entity, self._method_name, self._params)
end

---------------------------------------------------------
local bt_escape = bt.bt_action:new()

function bt_escape:new( time_ms, speed_percent)
    
    local tmp = {_time_ms = time_ms, _speed_percent = speed_percent}
    setmetatable(tmp, {__index = bt_escape})
    tmp.__index = tmp
    return tmp
end
function bt_escape:to_string()
    return "bt_escape .. " .. tostring(self.index)
end

function bt_escape:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_escape(entity, self._time_ms, self._speed_percent)
end

function bt_escape:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_escape(entity,self._time_ms, self._speed_percent)
end

---------------------------------------------------------
local bt_wander = bt.bt_action:new()

function bt_wander:new(range_cm_max, time_ms_per_duan_min, time_ms_per_duan_max, duan_min, duan_max)
    local tmp = {_range_cm_max = range_cm_max, _time_ms_per_duan_min = time_ms_per_duan_min, _time_ms_per_duan_max = time_ms_per_duan_max, _duan_min = duan_min, _duan_max = duan_max}
    setmetatable(tmp, {__index = bt_wander})
    tmp.__index = tmp

    return tmp
end
function bt_wander:to_string()
    return "bt_wander .. " .. tostring(self.index)
end

function bt_wander:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_wander(entity, self._range_cm_max, self._time_ms_per_duan_min, self._time_ms_per_duan_max, self._duan_min, self._duan_max)
end

function bt_wander:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_wander(entity, self._range_cm_max, self._time_ms_per_duan_min, self._time_ms_per_duan_max, self._duan_min, self._duan_max)
end
---------------------------------------------------------
local bt_move_to_pos = bt.bt_action:new()

function bt_move_to_pos:new(x_cm, y_cm, z_cm)
    local tmp = {_x_m = x_cm * 0.01, _y_m = y_cm * 0.01, _z_m = z_cm * 0.01}
    setmetatable(tmp, {__index = bt_move_to_pos})
    tmp.__index = tmp

    return tmp
end
function bt_move_to_pos:to_string()
    return "bt_move_to_pos .. " .. tostring(self.index)
end

function bt_move_to_pos:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_move_to_pos(entity, self._x_m, self._y_m, self._z_m)
end

function bt_move_to_pos:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_move_to_pos(entity, self._x_m, self._y_m, self._z_m)
end

---------------------------------------------------------
local bt_guard_search_target = bt.bt_action:new()

function bt_guard_search_target:new(tower_buff_id, atk_player_range_cm)
    local tmp = {_tower_buff_id = tower_buff_id, _atk_player_range_cm = atk_player_range_cm}
    setmetatable(tmp, {__index = bt_guard_search_target})
    tmp.__index = tmp

    return tmp
end
function bt_guard_search_target:to_string()
    return "bt_guard_search_target .. " .. tostring(self.index)
end

function bt_guard_search_target:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_guard_search_target(entity, self._tower_buff_id, self._atk_player_range_cm)
end

function bt_guard_search_target:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_guard_search_target(entity, self._tower_buff_id, self._atk_player_range_cm)
end
---------------------------------------------------------
local bt_tip_ui = bt.bt_action:new()

function bt_tip_ui:new(ui_type, chinese_id, text_time_ms, bg_time_ms)
    local tmp = {_ui_type = ui_type, _chinese_id = chinese_id, _text_time_sec = text_time_ms*0.001, _bg_time_sec = bg_time_ms*0.001}
    setmetatable(tmp, {__index = bt_tip_ui})
    tmp.__index = tmp

    return tmp
end
function bt_tip_ui:to_string()
    return "bt_tip_ui .. " .. tostring(self.index)
end

function bt_tip_ui:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_tip_ui(entity, self._ui_type, self._chinese_id, self._text_time_sec, self._bg_time_sec)
end

function bt_tip_ui:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_tip_ui(entity, self._ui_type, self._chinese_id, self._text_time_sec, self._bg_time_sec)
end

---------------------------------------------------------
local bt_pve_robot_search_target = bt.bt_action:new()

function bt_pve_robot_search_target:new(range_cm, select_not_main)
    local tmp = {_range_cm = range_cm, _select_not_main = select_not_main}
    setmetatable(tmp, {__index = bt_pve_robot_search_target})
    tmp.__index = tmp

    return tmp
end
function bt_pve_robot_search_target:to_string()
    return "bt_pve_robot_search_target .. " .. tostring(self.index)
end

function bt_pve_robot_search_target:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_pve_robot_search_target(entity, self._range_cm, self._select_not_main)
end

function bt_pve_robot_search_target:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_pve_robot_search_target(entity, self._range_cm, self._select_not_main)
end

---------------------------------------------------------
local bt_pet_off_play = bt.bt_action:new()

function bt_pet_off_play:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_pet_off_play})
    tmp.__index = tmp

    return tmp
end
function bt_pet_off_play:to_string()
    return "bt_pet_off_play .. " .. tostring(self.index)
end

function bt_pet_off_play:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_pet_off_play(entity)
end

function bt_pet_off_play:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_pet_off_play(entity)
end

------------------------------------------------------
local bt_pet_keep_dis_to_owner = bt.bt_action:new()

function bt_pet_keep_dis_to_owner:new(stop_distance)
    local tmp = {_stop_distance = stop_distance}
    setmetatable(tmp, {__index = bt_pet_keep_dis_to_owner})
    tmp.__index = tmp

    return tmp
end

function bt_pet_keep_dis_to_owner:to_string()
    return "bt_pet_keep_dis_to_owner .. " .. tostring(self.index)
end

function bt_pet_keep_dis_to_owner:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_pet_keep_dis_to_owner(entity)
end

function bt_pet_keep_dis_to_owner:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_pet_keep_dis_to_owner(entity)
end

---------------------------------------------------------
local bt_pet_assist_cast = bt.bt_action:new()

function bt_pet_assist_cast:new(target_type, target_type_param, face_target, check_cd, check_dis)
    local tmp = {_target_type = target_type, _target_type_param = target_type_param, _face_target = face_target, _check_cd = check_cd, _check_dis = check_dis}
    setmetatable(tmp, {__index = bt_pet_assist_cast})
    tmp.__index = tmp

    return tmp
end
function bt_pet_assist_cast:to_string()
    return "bt_pet_assist_cast .. " .. tostring(self.index)
end

function bt_pet_assist_cast:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_pet_assist_cast(entity, self._target_type, self._target_type_param, self._face_target, self._check_cd, self._check_dis)
end

function bt_pet_assist_cast:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_pet_assist_cast(entity, self._target_type, self._target_type_param, self._face_target, self._check_cd, self._check_dis)
end

---------------------------------------------------------
local bt_search_drop = bt.bt_action:new()

function bt_search_drop:new(target_type)
    local tmp = {_target_type = target_type}
    setmetatable(tmp, {__index = bt_search_drop})
    tmp.__index = tmp

    return tmp
end
function bt_search_drop:to_string()
    return "bt_search_drop .. " .. tostring(self.index)
end

function bt_search_drop:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_search_drop(entity, self._target_type)
end

function bt_search_drop:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_search_drop(entity, self._target_type)
end

---------------------------------------------------------
local bt_check_task = bt.bt_action:new()

function bt_check_task:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_check_task})
    tmp.__index = tmp

    return tmp
end
function bt_check_task:to_string()
    return "bt_check_task .. " .. tostring(self.index)
end

function bt_check_task:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_check_task(entity)
end

function bt_check_task:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_check_task(entity)
end

---------------------------------------------------------
local bt_remove_aggro_list = bt.bt_action:new()

function bt_remove_aggro_list:new()
    local tmp = {}
    setmetatable(tmp, {__index = bt_remove_aggro_list})
    tmp.__index = tmp

    return tmp
end
function bt_remove_aggro_list:to_string()
    return "bt_remove_aggro_list .. " .. tostring(self.index)
end

function bt_remove_aggro_list:pre_proc(entity)
    return mgr_entity_ai_cell:pre_bt_remove_aggro_list(entity)
end

function bt_remove_aggro_list:update_proc(entity)
    return mgr_entity_ai_cell:update_bt_remove_aggro_list(entity)
end

---------------------------------------------------------
--只后端用
local bt_search_buff_target = bt.bt_action:new()

function bt_search_buff_target:new(buff_id, range_cm)
    local tmp = {}
    setmetatable(tmp, {__index = bt_search_buff_target, _buff_id = buff_id, _range_cm = range_cm})
    tmp.__index = tmp

    return tmp
end
function bt_search_buff_target:to_string()
    return "bt_search_buff_target .. " .. tostring(self.index)
end

function bt_search_buff_target:pre_proc(entity)
    --return mgr_entity_ai_cell:pre_bt_search_buff_target(entity, self._buff_id, self._range_cm)
end

function bt_search_buff_target:update_proc(entity)
    --return mgr_entity_ai_cell:update_bt_search_buff_target(entity, self._buff_id, self._range_cm)
end

---------------------------------------------------------
bt.bt_search_target = bt_search_target
bt.bt_move_to_target = bt_move_to_target
bt.bt_cast = bt_cast
bt.bt_idle = bt_idle
bt.bt_speech = bt_speech
bt.bt_monster_speech = bt_monster_speech
bt.bt_look_on = bt_look_on
bt.bt_off_play = bt_off_play
bt.bt_add_buff = bt_add_buff
bt.bt_del_buff_self = bt_del_buff_self
bt.bt_pet_search_target = bt_pet_search_target
bt.bt_owner_search_target = bt_owner_search_target
bt.bt_escape = bt_escape
bt.bt_wander = bt_wander
bt.bt_move_to_pos = bt_move_to_pos
bt.bt_guard_search_target = bt_guard_search_target
bt.bt_tip_ui = bt_tip_ui
bt.bt_pve_robot_search_target = bt_pve_robot_search_target
bt.bt_pet_off_play = bt_pet_off_play
bt.bt_pet_keep_dis_to_owner = bt_pet_keep_dis_to_owner
bt.bt_pet_assist_cast = bt_pet_assist_cast
bt.bt_search_drop = bt_search_drop
bt.bt_check_task = bt_check_task
bt.bt_remove_aggro_list = bt_remove_aggro_list
bt.bt_search_buff_target = bt_search_buff_target
bt.bt_call_method = bt_call_method
bt.bt_owner_off_play = bt_owner_off_play