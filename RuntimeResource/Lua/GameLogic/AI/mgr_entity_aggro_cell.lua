--仇恨管理

local ai_utils = require "AI/ai_utils"

local pairs = pairs

local mgr_entity_aggro_cell = {}


function mgr_entity_aggro_cell:clean_threat(entity_ai)
    local bb = entity_ai.blackboard

    bb[ai_utils.BB_AGGRO_VAL] = {}
end

function mgr_entity_aggro_cell:add_threat(entity_ai, target_eid, value, reason)
    local bb = entity_ai.blackboard

    local cur_aggro = bb[ai_utils.BB_AGGRO_VAL][target_eid] or 0
    cur_aggro = cur_aggro + value
    if cur_aggro <= 0 then
        bb[ai_utils.BB_AGGRO_VAL][target_eid] = nil
    else
        bb[ai_utils.BB_AGGRO_VAL][target_eid] = cur_aggro
    end

    --尝试转换挑衅目标
    local cur_aggro_target_eid = bb[ai_utils.BB_TARGET_EID][bt.ai_target_type.aggro]
    if cur_aggro_target_eid then
        --有挑衅目标
        local target_aggro = bb[ai_utils.BB_AGGRO_VAL][cur_aggro_target_eid]
        if (not target_aggro) or (cur_aggro/target_aggro) >= 1.2 then
            --在仇恨列表中没有或者被OT了
            bb[ai_utils.BB_TARGET_EID][bt.ai_target_type.aggro] = target_eid
        end
    else
        --当前没挑衅目标
        bb[ai_utils.BB_TARGET_EID][bt.ai_target_type.aggro] = target_eid
    end
end

function mgr_entity_aggro_cell:get_target(entity_ai, order, tar_rank)
    local bb = entity_ai.blackboard

    local aggro_ranks = {}
    local aggro_vals = bb[ai_utils.BB_AGGRO_VAL]
    local del_eids = {}
    for eid, val in pairs(aggro_vals) do
        local entity = GameWorld.GetEntity(eid)
        if not entity or entity.cur_hp <= 0 then
            table.insert(del_eids, eid)
        else
            if not entity:GetDelTargetLock() and entity.cur_hp > 0 then
                table.insert(aggro_ranks, {eid, val})
            end
        end
    end

    for _, eid in pairs(del_eids) do
        aggro_vals[eid] = nil
    end

    if order == 0 then
        --顺序
        table.sort(aggro_ranks, function(a, b) return a[2] > b[2] end)
    else
        --倒序
        table.sort(aggro_ranks, function(a, b) return a[2] < b[2] end)
    end

    if aggro_ranks[tar_rank] then
        return aggro_ranks[tar_rank][1]
    end
end

function mgr_entity_aggro_cell:get_target_cur_hp_in_range(entity_ai, order, tar_rank, range)
    local owner_entity = entity_ai:GetOwner()
    local bb = entity_ai.blackboard

    local aggro_ranks = {}
    local aggro_vals = bb[ai_utils.BB_AGGRO_VAL]
    local del_eids = {}
    for eid, val in pairs(aggro_vals) do
        local entity = GameWorld.GetEntity(eid)
        if not entity or entity.cur_hp <= 0 then
            table.insert(del_eids, eid)
        else
            if Vector3.Distance(owner_entity:GetPosition(), entity:GetPosition()) <= range and not entity:GetDelTargetLock() then
                table.insert(aggro_ranks, {eid, entity.cur_hp})
            end
        end
    end

    for _, eid in pairs(del_eids) do
        aggro_vals[eid] = nil
    end

    if order == 0 then
        --顺序
        table.sort(aggro_ranks, function(a, b) return a[2] > b[2] end)
    else
        --倒序
        table.sort(aggro_ranks, function(a, b) return a[2] < b[2] end)
    end

    if aggro_ranks[tar_rank] then
        return aggro_ranks[tar_rank][1]
    end
end

function mgr_entity_aggro_cell:get_target_dis(entity_ai, order, tar_rank)
    local owner_entity = entity_ai:GetOwner()
    local owner_pos = owner_entity:GetPosition()
    local bb = entity_ai.blackboard

    local aggro_ranks = {}
    local aggro_vals = bb[ai_utils.BB_AGGRO_VAL]
    local del_eids = {}
    for eid, val in pairs(aggro_vals) do
        local entity = GameWorld.GetEntity(eid)
        if not entity or entity.cur_hp <= 0 then
            table.insert(del_eids, eid)
        else
            local dis = Vector3.Distance(owner_pos,  entity:GetPosition())
            if not entity:GetDelTargetLock() then
                table.insert(aggro_ranks, {eid, dis})
            end
        end
    end

    for _, eid in pairs(del_eids) do
        aggro_vals[eid] = nil
    end

    if order == 0 then
        --顺序
        table.sort(aggro_ranks, function(a, b) return a[2] > b[2] end)
    else
        --倒序
        table.sort(aggro_ranks, function(a, b) return a[2] < b[2] end)
    end

    if aggro_ranks[tar_rank] then
        return aggro_ranks[tar_rank][1]
    end
end

function mgr_entity_aggro_cell:get_target_has_buff(entity_ai, buff_id)
    local owner_entity = entity_ai:GetOwner()
    --local owner_pos = owner_entity:GetPosition()
    local bb = entity_ai.blackboard

    local aggro_ranks = {}
    local aggro_vals = bb[ai_utils.BB_AGGRO_VAL]
    local del_eids = {}
    for eid, val in pairs(aggro_vals) do
        local entity = GameWorld.GetEntity(eid)
        if not entity or entity.cur_hp <= 0 then
            table.insert(del_eids, eid)
        else
            if entity:ContainsBuffer(buff_id) and not entity:GetDelTargetLock() then
                table.insert(aggro_ranks, eid)
            end
        end
    end

    for _, eid in pairs(del_eids) do
        aggro_vals[eid] = nil
    end

    local size = #aggro_ranks
    if size > 0 then
        local tar_rank = math.random(1, size)
        return aggro_ranks[tar_rank]
    end
end

return mgr_entity_aggro_cell
