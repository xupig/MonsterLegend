-- EntityConfig.lua
EntityConfig = {}

--服务器同步属性
EntityConfig.PROPERTY_CUR_HP = "cur_hp"
EntityConfig.PROPERTY_DAZZLE_SALE_INFO = "dazzle_sale_info"
EntityConfig.PROPERTY_MAX_HP = "max_hp"
EntityConfig.PROPERTY_SPEED = "speed"
EntityConfig.PROPERTY_BASE_SPEED = "base_speed"
EntityConfig.PROPERTY_LEVEL = "level"
EntityConfig.PROPERTY_VOCATION = "vocation"
EntityConfig.PROPERTY_EXP = "money_exp"
EntityConfig.PROPERTY_SP = "money_spell_sp"
EntityConfig.PROPERTY_SING_STATE = "sing_state"
EntityConfig.PROPERTY_DELEGATE_EID = "delegate_eid"
EntityConfig.PROPERTY_CUR_ENDURE = "cur_endure"
EntityConfig.PROPERTY_MAX_ENDURE = "max_endure"
EntityConfig.PROPERTY_FLY_ENERGY = "fly_energy"
EntityConfig.PROPERTY_FLY_MAX_ENERGY = "fly_max_energy"
EntityConfig.PROPERTY_FACTION_ID = "faction_id"
EntityConfig.PROPERTY_MATE_NAME = "mate_name"

EntityConfig.PROPERTY_MONEY_COUPONS_BIND = "money_coupons_bind"
EntityConfig.PROPERTY_MONEY_GOLD = "money_gold"
EntityConfig.PROPERTY_MONEY_COUPONS = "money_coupons"

EntityConfig.PROPERTY_MONEY_GUILD_CONTRIBUTE = "money_guild_contribute"
EntityConfig.PROPERTY_MONEY_ATHLETICS = "money_athletics"
EntityConfig.PROPERTY_MONEY_ACTIVITY = "money_activity"
EntityConfig.PROPERTY_MONEY_SPELL_SP = "money_spell_sp"
EntityConfig.PROPERTY_LADDER_SCORE = "ladder_score"
EntityConfig.PROPERTY_CUR_MP = "cur_mp"
EntityConfig.PROPERTY_MAX_MP = "max_mp"
EntityConfig.PROPERTY_STORE_HP = "store_hp"
EntityConfig.PROPERTY_STORE_MP = "store_mp"
EntityConfig.PROPERTY_MAP_UNLOCK_DATA = "map_unlock_data"
EntityConfig.PROPERTY_PKG_WING_RARE = "pkg_wing_rare"
EntityConfig.PROPERTY_WING_LOAD = "wing_load"
EntityConfig.PROPERTY_WING_RARE_LOAD = "wing_rare_load"
EntityConfig.PROPERTY_FACADE = "facade"
EntityConfig.PROPERTY_FIGHT_FORCE = "fight_force"
EntityConfig.PROPERTY_FLY_SPEED = "fly_speed"
EntityConfig.PROPERTY_PK_MODE = "pk_mode"
EntityConfig.PROPERTY_PK_VALUE = "pk_value"
EntityConfig.PROPERTY_PK_VALUE_ONLINE_TIME_COUNT = "pk_value_online_time_count"
EntityConfig.PROPERTY_PET_EXP = "pet_exp"
EntityConfig.PROPERTY_PET_BLESS = "pet_bless"
EntityConfig.PROPERTY_PET_LEVEL = "pet_level"
EntityConfig.PROPERTY_PET_STAR = "pet_star"
EntityConfig.PROPERTY_PET_GRADE = "pet_grade"
EntityConfig.PROPERTY_HORSE_BLESS = "horse_bless"
EntityConfig.PROPERTY_HORSE_STAR = "horse_star"
EntityConfig.PROPERTY_HORSE_GRADE = "horse_grade"
EntityConfig.PROPERTY_TITLE_ID = "title_id"
EntityConfig.PROPERTY_BOSS_DROP_FLAG = "boss_drop_flag"
EntityConfig.PROPERTY_NOBILITY_RANK = "nobility_rank"
EntityConfig.PROPERTY_MONEY_TALENT = "money_talent"
EntityConfig.PROPERTY_MONEY_FASHION_CLOTHES = "money_fashion_clothes"
EntityConfig.PROPERTY_MONEY_FASHION_WEAPON = "money_fashion_weapon"
EntityConfig.PROPERTY_MONEY_FASHION_SPECIAL = "money_fashion_special"
EntityConfig.PROPERTY_MONEY_FASHION_PHOTO = "money_fashion_photo"
EntityConfig.PROPERTY_MONEY_FASHION_CHAT = "money_fashion_chat"
EntityConfig.PROPERTY_RUNE_PIECE = "rune_piece"
EntityConfig.PROPERTY_RUNE_GRID = "rune_grid"
EntityConfig.PROPERTY_RUNE_PKG = "rune_pkg"
EntityConfig.PROPERTY_RUNE_ESSENCE = "rune_essence"
EntityConfig.PROPERTY_RUNE_FREE_TIME = "rune_free_time"
EntityConfig.PROPERTY_AION_INFO = "aion_info"
EntityConfig.PROPERTY_GUARD_GODDESS_ID = "guard_goddess_id"
EntityConfig.PROPERTY_VIP_LEVEL = "vip_level"
EntityConfig.PROPERTY_PRAY_EXP_TIMES = "pray_exp_times"
EntityConfig.PROPERTY_PRAY_GOLD_TIMES = "pray_gold_times"
EntityConfig.PROPERTY_PRAY_GOLD_FREE_TIME = "pray_gold_free_time"
EntityConfig.PROPERTY_OFFLINE_PVP_BUY_COUNT = "offline_pvp_buy_count"
EntityConfig.PROPERTY_OFFLINE_PVP_CHALLENGE_COUNT = "offline_pvp_challenge_count"
EntityConfig.PROPERTY_CARD_BAG = "card_bag"
EntityConfig.PROPERTY_PKG_CARD_PIECE = "pkg_card_piece"
EntityConfig.PROPERTY_CARD_LIST = "card_list"
EntityConfig.PROPERTY_CARD_GROUP = "card_group"
EntityConfig.PROPERTY_GUARDIAN_INFO = "guardian_info"
EntityConfig.PROPERTY_GUARDIAN_ON = "guardian_on"

EntityConfig.PROPERTY_MONEG_DRAGON_PIECE = "money_dragon_piece"
EntityConfig.PROPERTY_DRAGON_SPIRIT = "dragon_spirit"
EntityConfig.PROPERTY_DRAGON_SPIRIT_BAG = "dragon_spirit_bag"

EntityConfig.PROPERTY_INVEST_VIP_DAYS = "invest_vip_flag"
EntityConfig.PROPERTY_INVEST_VIP_GET_REWARD_DAYS = "invest_vip_get_reward_days"

EntityConfig.PROPERTY_HORSE_RIDE_MODEL = "horse_ride_model"

EntityConfig.PROPERTY_LV_INVEST_TYPE = "lv_invest_type"
EntityConfig.PROPERTY_LV_INVEST_RECORD = "lv_invest_record"

EntityConfig.PROPERTY_MONTH_CARD_FLAG = "month_card_flag"
EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_STEP = "month_card_daily_refund_step"
EntityConfig.PROPERTY_MONTH_CARD_DAILY_REFUND_FLAG = "month_card_daily_refund_flag"

EntityConfig.PROPERTY_CUMULATIVE_CHARGE_AMOUNT = "cumulative_charge_amount"
EntityConfig.PROPERTY_CUMULATIVE_CHARGE_REFUND_INFO = "cumulative_charge_refund_info"
EntityConfig.PROPERTY_CUMULATIVE_CHARGE_EVENT_CLOSED = "cumulative_charge_event_closed"

EntityConfig.PROPERTY_ROULETTE_TIMES = "roulette_times"
EntityConfig.PROPERTY_ROULETTE_LUCK_TIMES = "roulette_luck_times"

EntityConfig.PROPERTY_CUMULATIVE_CHARGE_CURRENT_DAY = "cumulative_charge_current_day"

EntityConfig.PROPERTY_LOTTERY_EQUIP_SCORE = "lottery_equip_score"
EntityConfig.PROPERTY_LOTTERY_EQUIP_LUCKY_VALUE = "lottery_equip_lucky_value"

EntityConfig.PROPERTY_MARRY_RING_LEVEL = "marry_ring_level"
EntityConfig.PROPERTY_MARRY_RING_EXP = "marry_ring_exp"

EntityConfig.PROPERTY_MARRY_PET_INFO = "marry_pet_info"

EntityConfig.PROPERTY_MARRY_INSTANCE_CHALLENGED_TIME = "marry_instance_challenged_time"
EntityConfig.PROPERTY_MARRY_INSTANCE_EXTRA_TIME = "marry_instance_extra_time"
EntityConfig.PROPERTY_MARRY_INSTANCE_MATE_TIME = "marry_instance_mate_time"

EntityConfig.PROPERTY_MARRY_BOX_INFO = "marry_box_info"

EntityConfig.PROPERTY_RING_UNLOCK_STEP = "demon_ring_unlocked_step"
EntityConfig.PROPERTY_LEARN_PASSIVE_SPELL = "learned_passive_spell"

EntityConfig.PROPERTY_CHARM = "charm"

EntityConfig.PROPERTY_VIP_ENDTIME = "vip_endtime"

EntityConfig.PROPERTY_LEFT_HANG_UP_TIME = "left_hang_up_time"

EntityConfig.PROPERTY_LEVEL_UP_REWARD_INFO = "level_up_reward_info"

EntityConfig.PROPERTY_DROP_OWNER = "drop_owner"


EntityConfig.PROPERTY_DAILY_ONLINE_MIN = "daily_online_min"

EntityConfig.PROPERTY_BOSS_REWARD_UNLOCKED_STEP = "boss_reward_unlocked_step"

--渠道活动
EntityConfig.PROPERTY_CHANNEL_ACTIVITY_TIMES = "channel_activity_times"

EntityConfig.PROPERTY_ZODIAC_MATERIAL = "zodiac_material"
EntityConfig.PROPERTY_ZODIAC_INFO = "zodiac_info"


--纯客户端属性
EntityConfig.PROPERTY_ON_GROUND = "on_ground"
EntityConfig.CURRENT_STATE = "current_state"


return EntityConfig