-- InstanceConfig.lua
local InstanceConfig = {}

InstanceConfig.PET_INSTANCE_START_DROP_MESSAGE_MAP = {
    [0] = LanguageDataHelper.CreateContent(71016),
    [1] = LanguageDataHelper.CreateContent(71015),
    [2] = LanguageDataHelper.CreateContent(71014),
    [3] = LanguageDataHelper.CreateContent(71001)
}

InstanceConfig.PET_INSTANCE_COUNT_DOWN_MESSAGE_MAP = {
    [0] = 58673,
    [1] = 71002,
    [2] = 71002,
    [3] = 71002
}

InstanceConfig.MONEY_INSTANCE_START_DROP_MESSAGE_MAP = {
    [0] = LanguageDataHelper.CreateContent(74717),
    [1] = LanguageDataHelper.CreateContent(74716),
    [2] = LanguageDataHelper.CreateContent(74715),
    [3] = LanguageDataHelper.CreateContent(74714)
}

InstanceConfig.MONEY_INSTANCE_COUNT_DOWN_MESSAGE_MAP = {
    [0] = 58673,
    [1] = 74702,
    [2] = 74702,
    [3] = 74702
}

InstanceConfig.GUILD_MONSTER_INSTANCE_START_DROP_MESSAGE_MAP = {
    [0] = LanguageDataHelper.CreateContent(53201),
    [1] = LanguageDataHelper.CreateContent(53200),
    [2] = LanguageDataHelper.CreateContent(53199),
    [3] = LanguageDataHelper.CreateContent(53198),
    [4] = LanguageDataHelper.CreateContent(53197),
}

InstanceConfig.GUILD_MONSTER_INSTANCE_COUNT_DOWN_MESSAGE_MAP = {
    [0] = 53204,
    [1] = 74702,
    [2] = 74702,
    [3] = 74702,
    [4] = 74702
}

InstanceConfig.EQUIPMENT_INSTANCE_START_DROP_MESSAGE_MAP = {
    [0] = LanguageDataHelper.CreateContent(75025),
    [1] = LanguageDataHelper.CreateContent(75024),
    [2] = LanguageDataHelper.CreateContent(75023),
    [3] = LanguageDataHelper.CreateContent(75022)
}

InstanceConfig.EQUIPMENT_INSTANCE_COUNT_DOWN_MESSAGE_MAP = {
    [0] = 58673,
    [1] = 74702,
    [2] = 74702,
    [3] = 74702
}

InstanceConfig.CLOUD_PEAK_TYPE_INFO_UPDATE = 1
InstanceConfig.CLOUD_PEAK_TYPE_PASS = 2

InstanceConfig.MONEYINSTANCE_WAVE_CHANGE    = 1
InstanceConfig.MONEYINSTANCE_MONEY_CHANGE   = 2


return InstanceConfig