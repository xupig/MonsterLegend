-- AchievementConfig.lua
local AchievementConfig = {}


AchievementConfig.TOTAL_ACHIEVEMENT = 1                 --成就总览ID(成就分类表中的type属性)
-- AchievementConfig.

AchievementConfig.NONE_ACHIEVEMENT_GROUP = 0            --空的成就子类id

--成就进度
AchievementConfig.ACHIEVEMENT_HAS_PROGRESS = 1          --未完成但是有进度
AchievementConfig.ACHIEVEMENT_COMPLETE = 2              --完成但是未领取奖励
AchievementConfig.ACHIEVEMENT_GET_REWARD = 3            --完成并已领领取奖励
AchievementConfig.ACHIEVEMENT_NO_PROGRESS = 4           --未完成但是没有进度

return AchievementConfig
