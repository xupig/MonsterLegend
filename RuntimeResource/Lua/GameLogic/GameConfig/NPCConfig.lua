--NPCConfig.lua
NPCConfig = {}

--[[
npc的创建来历：
0 通过space配置创建，
1 通过剧情CG创建，
2 通过章节本配置创建 
]]

NPCConfig.FROM_SPACE =  0               --0为场景中正常NPC
NPCConfig.FROM_DRAMA =  1               --1为drama npc
NPCConfig.FROM_MONSTER_COLLECT = 2      --2为章节副本中间场景npc

--[[
npc的正常作用类型：
0 普通NPC，
1 点击直接打开UI的NPC，
2 点击则寻路过去，进入触发范围则打开UI 
3 点击进入剧情任务
4 点击则寻路过去，进入触发范围则进入副本；直接跑过去进入范围也会进入副本（副本专用传送口）
5.点击则镜头迅速拉近到对应的位置然后打开ui
6.点击则镜头迅速拉近到对应的位置然后进入剧情 (没做)
7.进入范围则传送到对应地图
]]

NPCConfig.NPC_TYPE_COMMON = 0
NPCConfig.NPC_TYPE_CLICK_UI = 1
NPCConfig.NPC_TYPE_RANGE_UI = 2
NPCConfig.NPC_TYPE_CLICK_ENTER_INS = 3
NPCConfig.NPC_TYPE_RANGE_ENTER_INS = 4
NPCConfig.NPC_TYPE_CAMERA_UI = 5
NPCConfig.NPC_TYPE_CAMERA_DRAMA = 6
NPCConfig.NPC_TYPE_TRANSFER = 7

NPCConfig.NPC_ID_BigMap = 6
NPCConfig.NPC_ID_BulletinBoard = 25

return NPCConfig