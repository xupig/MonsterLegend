-- RankConfig.lua
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RankConfig = {}

-- RankConfig.NAME_MAP = {
--     [public_config.RANK_TYPE_LEVEL] = LanguageDataHelper.CreateContent()
-- }

RankConfig.RANK_LIST = {
    [1] = {
        ["name"] = LanguageDataHelper.CreateContent(56218),
        ["rankType"] = public_config.RANK_TYPE_COMBAT_VALUE--战力榜
    },
    [2] = {
        ["name"] = LanguageDataHelper.CreateContent(56219),
        ["rankType"] = public_config.RANK_TYPE_LEVEL--等级榜
    },
    [3] = {
        ["name"] = LanguageDataHelper.CreateContent(56220),
        ["rankType"] = public_config.RANK_TYPE_PET--宠物
    },
    [4] = {
        ["name"] = LanguageDataHelper.CreateContent(56221),
        ["rankType"] = public_config.RANK_TYPE_HORSE--坐骑
    },
    [5] = {
        ["name"] = LanguageDataHelper.CreateContent(56222),
        ["rankType"] = public_config.RANK_TYPE_WING--翅膀
    },
    [6] = {
        ["name"] = LanguageDataHelper.CreateContent(56223),
        ["rankType"] = public_config.RANK_TYPE_TALISMAN--法宝
    },
    [7] = {
        ["name"] = LanguageDataHelper.CreateContent(56224),
        ["rankType"] = public_config.RANK_TYPE_WEAPON--神兵
    },
    [8] = {
        ["name"] = LanguageDataHelper.CreateContent(56226),
        ["rankType"] = public_config.RANK_TYPE_AION_TOWER--永恒之塔
    },
    [9] = {
        ["name"] = LanguageDataHelper.CreateContent(56227),
        ["rankType"] = public_config.RANK_TYPE_ACHIEVEMENT--成就点
    },
    [10] = {
        ["name"] = LanguageDataHelper.CreateContent(56228),
        ["rankType"] = public_config.RANK_TYPE_OFFLINE_EXP--离线效率
    }
}

return RankConfig