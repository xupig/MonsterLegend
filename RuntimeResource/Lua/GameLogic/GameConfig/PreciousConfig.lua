-- PreciousConfig.lua
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PreciousConfig = {}

PreciousConfig.Type = {}
PreciousConfig.Type.RuneFind=1;--符文寻宝
PreciousConfig.Type.EquipFind=2;--装备寻宝
PreciousConfig.Type.PeakFind=3;--巅峰寻宝

PreciousConfig.LIST = {
    [1] = {
        ["name"] = LanguageDataHelper.CreateContent(964),
        ["rankType"] = PreciousConfig.Type.RuneFind,
        ["funcId"] = public_config.FUNCTION_ID_SYMBOL_TREASURE
    },
    [2] = {
        ["name"] = LanguageDataHelper.CreateContent(76215),
        ["rankType"] = PreciousConfig.Type.EquipFind,
        ["funcId"] = public_config.FUNCTION_ID_EQUIP_TREASURE
    },
    [3] = {
        ["name"] = LanguageDataHelper.CreateContent(76229),
        ["rankType"] = PreciousConfig.Type.PeakFind,
        ["funcId"] = public_config.FUNCTION_ID_PEAK_TREASURE
    }
}

return PreciousConfig