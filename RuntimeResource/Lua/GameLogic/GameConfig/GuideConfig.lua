--GuideConfig.lua
GuideConfig = {}


GuideConfig.Mask = 'Mask' --带遮罩箭头指引
GuideConfig.NoMask = 'NoMask' --没遮罩箭头指引
GuideConfig.Dialogue = 'Dialogue' --对话框
GuideConfig.DialogueWithMask = 'DialogueWithMask'

GuideConfig.TaskType = {
    Task = '1', --剧情任务
    Battle = '2', -- 副本
    NewFunction = '3', --功能新开
    Activity = '4', --限时活动
    Guide = '5', --指引
    CircleTask = '6', --灵域任务
    BranchTask = '7', --支线任务
}

GuideConfig.TaskTypeIcon = {
    Task = 9948,
    Activity = 9946,
    BranchTask = 9947,
}

GuideConfig.CGType = {
    Center = 1, --中间出现功能按钮新手指引
    List = 2, --右边列表出现功能按钮新手指引
}

GuideConfig.ArrowDirection = {
    Left = 'Left',
    Right = 'Right',
}


GuideConfig.ButtonType = {
    LeftTop = 3,
    RightBottom = 2,
}


return GuideConfig