-- DialogConfig.lua
local DialogConfig = {}

DialogConfig.DIALOG_MODE = 1   --对话模式
DialogConfig.REWARD_MODE = 2   --奖励模式
DialogConfig.CONFIRM_MODE = 3   --确定模式
DialogConfig.HAND_OVER_MODE = 4   --交付模式
DialogConfig.FEEDBACK_MODE = 5   --对话交互模式
DialogConfig.ENTER_INST_MODE = 6   --进入副本模式

DialogConfig.DIALOG_DATA = "dialogData"   --对话数据
DialogConfig.MODEL_ID = "modelId"   --模型id
DialogConfig.DISTINGUISH = "distinguish"   --npc类型区分，1为角色模型做npc，0或者不填为普通npc
DialogConfig.NPC_NAME = "npcName"   --npc名字
DialogConfig.NPC_TITLE = "npcTitle"   --npc称号

DialogConfig.DISTINGUISH_PLAYER_MODEL = 1

return DialogConfig
