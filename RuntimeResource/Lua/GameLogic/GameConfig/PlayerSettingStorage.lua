--PlayerSettingStorage.lua

PlayerSettingStorage = {}
local CacheConfig = GameConfig.CacheConfig

function PlayerSettingStorage:Init()
    -- CacheConfig.PlayerSettingData
end

--【游戏设置】保存数据
function PlayerSettingStorage:SaveData(data)
    local dataStr = GameWorld.json.encode(data)
    GameWorld.SaveCache(CacheConfig.PlayerSettingData, {dataStr}, false)
end

--【游戏设置】读取数据
function PlayerSettingStorage:LoadData()
    local data = GameWorld.ReadCache( CacheConfig.PlayerSettingData, false)
    if data == nil then
        return nil
    else
        local dataJson = GameWorld.json.decode(data[0])
        return dataJson
    end
    
end


return PlayerSettingStorage