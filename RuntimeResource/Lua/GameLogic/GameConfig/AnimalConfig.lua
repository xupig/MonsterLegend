-- AnimalConfig.lua
local AnimalConfig = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

AnimalConfig.NAME_MAP = {
    [1] = LanguageDataHelper.CreateContent(76443),
    [2] = LanguageDataHelper.CreateContent(76444),
    [3] = LanguageDataHelper.CreateContent(76445),
    [4] = LanguageDataHelper.CreateContent(76446),
    [5] = LanguageDataHelper.CreateContent(76447)
}

AnimalConfig.QULITY_NAME_MAP = {
    [1] = LanguageDataHelper.CreateContent(76437),--白色
    [3] = LanguageDataHelper.CreateContent(76438),--蓝色
    [4] = LanguageDataHelper.CreateContent(76439),--紫色
    [5] = LanguageDataHelper.CreateContent(76440),--橙色
    [6] = LanguageDataHelper.CreateContent(76441),--红色
    [7] = LanguageDataHelper.CreateContent(76442)--金色
}

AnimalConfig.QULITY_COLOR_MAP = {
    [1] = 101,--白色
    [3] = 103,--蓝色
    [4] = 104,--紫色
    [5] = 105,--橙色
    [6] = 106,--红色
    [7] = 107--金色
}

AnimalConfig.ALL_QUALITY_TYPE = 100         --全部品质
AnimalConfig.QUALITY_1        = 1           --白色
AnimalConfig.QUALITY_3        = 3           --蓝色
AnimalConfig.QUALITY_4        = 4           --紫色
AnimalConfig.QUALITY_5        = 5           --橙色
AnimalConfig.QUALITY_6        = 6           --红色
AnimalConfig.QUALITY_7        = 7           --金色

AnimalConfig.QUALITY_SELECT_NAME_MAP = {
    [AnimalConfig.ALL_QUALITY_TYPE] = LanguageDataHelper.CreateContent(76426),--全部品质
    [AnimalConfig.QUALITY_1] = LanguageDataHelper.CreateContent(76437),--白色
    [AnimalConfig.QUALITY_3] = LanguageDataHelper.CreateContent(76438),--蓝色
    [AnimalConfig.QUALITY_4] = LanguageDataHelper.CreateContent(76439),--紫色
    [AnimalConfig.QUALITY_5] = LanguageDataHelper.CreateContent(76440),--橙色
    [AnimalConfig.QUALITY_6] = LanguageDataHelper.CreateContent(76441),--红色
    [AnimalConfig.QUALITY_7] = LanguageDataHelper.CreateContent(76442) --金色
}

AnimalConfig.ALL_START_TYPE   = 100         --全部星级
AnimalConfig.START_1          = 1           --1星装备
AnimalConfig.START_2          = 2           --2星装备
AnimalConfig.START_3          = 3           --3星装备

AnimalConfig.START_SELECT_NAME_MAP = {
    [AnimalConfig.ALL_START_TYPE] = LanguageDataHelper.CreateContent(76432),--全部星级
    [AnimalConfig.START_1] = LanguageDataHelper.CreateContent(76433),--1星装备
    [AnimalConfig.START_2] = LanguageDataHelper.CreateContent(76434),--2星装备
    [AnimalConfig.START_3] = LanguageDataHelper.CreateContent(76435)--3星装备
}

AnimalConfig.QUALITY_EQUAL = 1              --相同品质
AnimalConfig.QUALITY_LESS_OR_EQUAL = 2      --相同或以下品质

--神兽系统强化筛选品质
AnimalConfig.EQUIP_UPGRADE_QULITY_ALL = 100 --全部
AnimalConfig.EQUIP_UPGRADE_QULITY_6 = 6     --兽血灵晶
AnimalConfig.EQUIP_UPGRADE_QULITY_3 = 3     --蓝色
AnimalConfig.EQUIP_UPGRADE_QULITY_4 = 4     --紫色
AnimalConfig.EQUIP_UPGRADE_QULITY_5 = 5     --橙色

AnimalConfig.EQUIP_UPGRADE_QULITY__NAME_MAP = {
    [AnimalConfig.EQUIP_UPGRADE_QULITY_ALL] = LanguageDataHelper.CreateContent(76416),--全部
    [AnimalConfig.EQUIP_UPGRADE_QULITY_6] = LanguageDataHelper.CreateContent(76417),--兽血灵晶
    [AnimalConfig.EQUIP_UPGRADE_QULITY_3] = LanguageDataHelper.CreateContent(76418),--蓝色及以下
    [AnimalConfig.EQUIP_UPGRADE_QULITY_4] = LanguageDataHelper.CreateContent(76419),--紫色及以下
    [AnimalConfig.EQUIP_UPGRADE_QULITY_5] = LanguageDataHelper.CreateContent(76420)--橙色及以下
}

return AnimalConfig
