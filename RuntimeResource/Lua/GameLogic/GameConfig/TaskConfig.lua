-- TaskConfig.lua
local TaskConfig = {}


TaskConfig.IS_TRACK = 1   --任务追踪状态
TaskConfig.IS_NOT_TRACK = 0   --任务非追踪状态

TaskConfig.TALK_TO_SOMEONE = 10543 -- 中文表id：与{0}对话

TaskConfig.MAIN_ACCEPTABLE = 40 -- 主线可接任务
TaskConfig.MAIN_ACCEPTED = 42 -- 主线正在做的任务
TaskConfig.MAIN_COMPLETED = 44 -- 主线可交付任务
TaskConfig.BRANCH_ACCEPTABLE = 41 -- 支线可接任务
TaskConfig.BRANCH_ACCEPTED = 43 -- 支线正在做的任务
TaskConfig.BRANCH_COMPLETED = 45 -- 支线可交付任务

TaskConfig.BUTTON_OK = 13 -- 中文表id：确定
TaskConfig.BUTTON_ACCEPT = 10570 -- 中文表id：领取任务
TaskConfig.BUTTON_COMMIT = 10571 -- 中文表id：完成任务
TaskConfig.BUTTON_GET_REWARD = 10572 -- 中文表id：领取奖励
TaskConfig.BUTTON_ENTER_INST = 10573 -- 中文表id：进入副本

--任务类型
TaskConfig.TASK_TYPE_MAIN = 1                   --主线任务
TaskConfig.TASK_TYPE_COMMON_BRANCH = 2          --支线任务
TaskConfig.TASK_TYPE_VOCATIONCHANGE = 3         --转职任务
TaskConfig.TASK_TYPE_CIRCLETASK = 4             --赏金任务(日常)
TaskConfig.TASK_TYPE_GUARD_GODDESS = 5          --护送女神
TaskConfig.TASK_TYPE_COMMON_TASK = 6            --一般任务（主线、支线、转职）
TaskConfig.TASK_TYPE_WEEK_TASK = 7              --周常任务
  
--任务变化类型
TaskConfig.TASK_CHANGE_OPERATE_TYPE_CHANGE = 1          --任务变化类型（更新）
TaskConfig.TASK_CHANGE_OPERATE_TYPE_DEL = 2             --任务变化类型（删除）
TaskConfig.TASK_CHANGE_OPERATE_TYPE_ADD = 3             --任务变化类型（新增）

--任务变化实体属性定义
TaskConfig.TASK_CHANGE_INFO_ID = 1              --任务变化属性定义（任务id）
TaskConfig.TASK_CHANGE_INFO_TASK_TYPE = 2       --任务变化属性定义（任务类型:参照上面）
TaskConfig.TASK_CHANGE_INFO_OPERATE_TYPE = 3    --任务变化属性定义（任务变化类型:参照上面）更新、删除、新增
TaskConfig.TASK_CHANGE_INFO_TASK_DATA = 4       --任务变化属性定义（任务itemData）


--定义任务优先级
TaskConfig.PRIORITY_MAP = {
    [1] = TaskConfig.TASK_TYPE_MAIN,
    [2] = TaskConfig.TASK_TYPE_VOCATIONCHANGE,
    [3] = TaskConfig.TASK_TYPE_GUARD_GODDESS,
    [4] = TaskConfig.TASK_TYPE_CIRCLETASK,
    [5] = TaskConfig.TASK_TYPE_WEEK_TASK,
    [6] = TaskConfig.TASK_TYPE_COMMON_BRANCH
}

TaskConfig.TASK_TYPE_PRIORITY_MAP = {
    [TaskConfig.TASK_TYPE_MAIN] = 1,
    [TaskConfig.TASK_TYPE_VOCATIONCHANGE] = 2,
    [TaskConfig.TASK_TYPE_GUARD_GODDESS] = 3,
    [TaskConfig.TASK_TYPE_CIRCLETASK] = 4,
    [TaskConfig.TASK_TYPE_WEEK_TASK] = 5,
    [TaskConfig.TASK_TYPE_COMMON_BRANCH] = 6
   
}

-----------------------------小飞鞋设置--------------------------------------------
TaskConfig.IS_USE_LITTLE_SHOES = true
TaskConfig.IS_NOT_USE_LITTLE_SHOES = false
TaskConfig.IS_TELEPORT_TO = 1

-----------------------------任务来源--------------------------------------------
TaskConfig.IS_HAND_CLICK = true
TaskConfig.VOCATION_TO_TASK = 1


return TaskConfig
