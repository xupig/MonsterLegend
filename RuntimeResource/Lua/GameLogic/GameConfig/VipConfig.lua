-- VipConfig.lua
local VipConfig = {}
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local ExpInstanceDataHelper = GameDataHelper.ExpInstanceDataHelper
local GetBuyTimes = function(type)
    if type == public_config.MAP_TYPE_HOLY_TOWER then 
        return GameWorld.Player().holy_tower_info[public_config.HOLY_TOWER_KEY_BUY_NUM]
    elseif type == public_config.MAP_TYPE_EXP then 
        return GameWorld.Player().exp_instance_info[public_config.EXP_INSTANCE_BUY_ENTER_NUM]
    elseif type == public_config.MAP_TYPE_GOLD then 
        return GameWorld.Player().instance_gold_info[public_config.INSTANCE_GOLD_KEY_BUY_NUM]
    elseif type == public_config.MAP_TYPE_TOWER_DEFENSE then 
        return GameWorld.Player().tower_defense_info[public_config.TOWER_DEFENSE_KEY_BUY_NUM]
    end    
end

VipConfig.VIP_BUY_TIMES_TYPE_MAP =  {
    [public_config.MAP_TYPE_HOLY_TOWER] = "GetPetInstanceBuyTimes",
    [public_config.MAP_TYPE_EXP] = "GetExpInstanceBuyTimes",
    [public_config.MAP_TYPE_GOLD] = "GetMoneyInstanceBuyTimes",
    [public_config.MAP_TYPE_TOWER_DEFENSE] = "GetTowerInstanceBuyTimes"
}

VipConfig.VIP_BUY_PRICE_MAP = {
    [public_config.MAP_TYPE_HOLY_TOWER] = GlobalParamsHelper.GetParamValue(572),
    [public_config.MAP_TYPE_EXP] = GlobalParamsHelper.GetParamValue(571),
    [public_config.MAP_TYPE_GOLD] = GlobalParamsHelper.GetParamValue(573),
    [public_config.MAP_TYPE_TOWER_DEFENSE] = GlobalParamsHelper.GetParamValue(717)
}

-- --各个副本的进入次数
-- VipConfig.VIP_ENTER_TIMES_MAP = {
--     [public_config.MAP_TYPE_HOLY_TOWER] = GlobalParamsHelper.GetParamValue(474),
--     [public_config.MAP_TYPE_EXP] = ExpInstanceDataHelper:GetExpInstanceCfg(1).enter_limit,
--     [public_config.MAP_TYPE_GOLD] = GlobalParamsHelper.GetParamValue(516)
-- }

--各个副本购买的次数
VipConfig.VIP_BUY_TIMES_MAP = {
    [public_config.MAP_TYPE_HOLY_TOWER] = GetBuyTimes,
    [public_config.MAP_TYPE_EXP] = GetBuyTimes,
    [public_config.MAP_TYPE_GOLD] = GetBuyTimes,
    [public_config.MAP_TYPE_TOWER_DEFENSE] = GetBuyTimes,
}


return VipConfig