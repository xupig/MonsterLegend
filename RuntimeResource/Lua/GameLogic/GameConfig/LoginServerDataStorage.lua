-- LoginServerDataStorage.lua
local LoginServerDataStorage = {}

local CacheConfig = GameConfig.CacheConfig

function LoginServerDataStorage.Init()
    -- self:Load()   
end


function LoginServerDataStorage.Load()
    local data = GameWorld.ReadCache( tostring( CacheConfig.LoginServerData ), false)
    if data == nil then
        return {}
    else
        local dataJson = GameWorld.json.decode(data[0])
        local dataList = {}
        for k,v in pairs(dataJson) do
            dataList[k] = GameWorld.json.decode(v)
        end
        return dataList
    end
end

function LoginServerDataStorage.Save(list)
    local tempData = {}
    for k,v in pairs(list) do
        tempData[k] = GameWorld.json.encode(v)
    end

    local dataStr = GameWorld.json.encode(tempData)
    GameWorld.SaveCache( tostring( CacheConfig.LoginServerData ) , {dataStr}, false)
end

return LoginServerDataStorage