--用来统一定义声明枚举类型
EnumType = {}

EnumType.CharacterAction = {}
EnumType.CharacterAction.ACTIVE_MOVE_ABLE = 1
EnumType.CharacterAction.MOVE_ABLE = 2
EnumType.CharacterAction.ATTACK_ABLE = 3
EnumType.CharacterAction.HIT_ABLE = 4
EnumType.CharacterAction.TURN_ABLE = 5
EnumType.CharacterAction.VISIBLE = 6
EnumType.CharacterAction.SHOW = 7
EnumType.CharacterAction.THINK_ABLE = 8
EnumType.CharacterAction.COLLIDE_ABLE = 9
EnumType.CharacterAction.TRANSMIT_ABLE = 10
EnumType.CharacterAction.AUTO_MOUNT_ABLE = 11

EnumType.RolePanelShowType = {}
EnumType.RolePanelShowType.Heraldry = 1
EnumType.RolePanelShowType.Color = 2

EnumType.SpawnType = {}
EnumType.SpawnType.ThrowObject = 1
EnumType.SpawnType.Trap = 3

EnumType.TimeLimitType = {} -- 对应panel_open的id
EnumType.TimeLimitType.Wedding = 301
EnumType.TimeLimitType.SevenDayLogin = 407--七天登录
EnumType.TimeLimitType.Rouletter = 302 --云购
EnumType.TimeLimitType.Activity = 5 --开服活动
EnumType.TimeLimitType.LevelInvest = 6 --等级投资
EnumType.TimeLimitType.CardLuck = 401 --幸运翻牌
EnumType.TimeLimitType.HappyWish = 400 --欢乐许愿
EnumType.TimeLimitType.DailyConsume = 403
EnumType.TimeLimitType.Identify = 404
EnumType.TimeLimitType.OperatingActivity = 402 --运营活动
EnumType.TimeLimitType.OperatingActivityExpDouble = 405 --运营活动经验双倍
EnumType.TimeLimitType.OperatingActivityInstanceDouble = 406 --运营活动副本双倍
EnumType.TimeLimitType.ZeroDollarBuy = 409 --0元购
EnumType.TimeLimitType.Sale = 410 --特卖
EnumType.TimeLimitType.OpenOther = 413
EnumType.TimeLimitType.OpenRank = 127

EnumType.MonsterType = {}
EnumType.MonsterType.Normal = 1
EnumType.MonsterType.Elite = 2
EnumType.MonsterType.NormalBoss = 3
EnumType.MonsterType.WorldBoss = 4
EnumType.MonsterType.SleipmonBoss = 5
EnumType.MonsterType.IsBoss = function(monsterType)
    local MonsterType = EnumType.MonsterType
    if monsterType == MonsterType.NormalBoss or monsterType == MonsterType.WorldBoss or
        monsterType == MonsterType.SleipmonBoss then
        return true
    end
    return false
end


EnumType.BillBoardType = {}
EnumType.BillBoardType.Normal = 1
EnumType.BillBoardType.Elite = 2
EnumType.BillBoardType.Npc = 3
EnumType.BillBoardType.OtherPlayer = 4
EnumType.BillBoardType.Boss = 5
EnumType.BillBoardType.SpaceDrop = 6
EnumType.BillBoardType.Collect = 7
EnumType.BillBoardType.DeathFlage = 8

EnumType.EntityType = {}
EnumType.EntityType.Avatar = 1
EnumType.EntityType.PlayerAccount = 2
EnumType.EntityType.PlayerAvatar = 3
EnumType.EntityType.CreateRoleAvatar = 4
EnumType.EntityType.Collect = 5
EnumType.EntityType.Dummy = 6
EnumType.EntityType.NPC = 7
EnumType.EntityType.Trap = 8
EnumType.EntityType.ThrowObject = 9
EnumType.EntityType.Monster = 10
EnumType.EntityType.SpaceDrop = 11
EnumType.EntityType.SkillShowAvatar = 12
EnumType.EntityType.ClienntCollectPoint = 16
EnumType.EntityType.StaticShowAvatar = 17
EnumType.EntityType.StaticMonster = 18
EnumType.EntityType.Pet = 19
EnumType.EntityType.Fabao = 20
EnumType.EntityType.Peri = 21
EnumType.EntityType.BossTomb = 22
EnumType.EntityType.MonsterDestroyArea = 23
EnumType.EntityType.ClickFlag = 24
EnumType.EntityType.UIShowEntity = 25
EnumType.EntityType.ServerSpaceDrop = 26

--MessageBox子类型
EnumType.MessageBoxType = {}
EnumType.MessageBoxType.Exchange = 1
EnumType.MessageBoxType.BuySucc = 2
EnumType.MessageBoxType.Tip = 3
EnumType.MessageBoxType.InviteJoinGuild = 4
EnumType.MessageBoxType.RedEnvelope = 5
EnumType.MessageBoxType.RedEnvelopeInfo = 6
EnumType.MessageBoxType.DescriptionText = 7
EnumType.MessageBoxType.ExchangeSilver = 8
EnumType.MessageBoxType.OpenBag = 9
EnumType.MessageBoxType.ToggleTips = 10
EnumType.MessageBoxType.RewardCountDowenTip = 11
EnumType.MessageBoxType.BuyCountView = 12
EnumType.MessageBoxType.Tip1 = 13
EnumType.MessageBoxType.TradeSell = 14
EnumType.MessageBoxType.BulkUse = 15
EnumType.MessageBoxType.PropSplit = 16
EnumType.MessageBoxType.SelectUse = 17
EnumType.MessageBoxType.BackWoodsTicket = 18
EnumType.MessageBoxType.GiftSucc = 19
EnumType.MessageBoxType.FirstCharge = 20

--结算界面类型
EnumType.SettleType = {}
EnumType.SettleType.Ladder = 1

--部位类型
EnumType.RolePartType = {}
EnumType.RolePartType.MainHand = 1
EnumType.RolePartType.OffHand = 2
EnumType.RolePartType.Hair = 3
EnumType.RolePartType.Cloth = 4

--外观显示类型
EnumType.EquipShowType = {}
EnumType.EquipShowType.NormalEquip = 0
EnumType.EquipShowType.Fashion = 1

--公会捐赠排行榜类型
EnumType.GuildDonateRankType = {}
EnumType.GuildDonateRankType.Today = 1
EnumType.GuildDonateRankType.History = 2

--宝箱领取状态类型
EnumType.RewardBoxStateType = {}
EnumType.RewardBoxStateType.NotGet = 1
EnumType.RewardBoxStateType.CanGet = 2
EnumType.RewardBoxStateType.HasGot = 3

--商城类型
EnumType.ShopType = {}
EnumType.ShopType.Normal = 1
EnumType.ShopType.Token = 2
EnumType.ShopType.Charge = 3

--商城页签类型
EnumType.ShopPageType = {}
EnumType.ShopPageType.WeekSell = 1
EnumType.ShopPageType.Common = 2
EnumType.ShopPageType.Fashion = 3
EnumType.ShopPageType.GrowUp = 4
EnumType.ShopPageType.BindCoupons = 5
EnumType.ShopPageType.Honor = 6
EnumType.ShopPageType.Marriage = 7

--Partner类型
EnumType.PartnerType = {}
EnumType.PartnerType.Pet = 1
EnumType.PartnerType.Ride = 2
EnumType.PartnerType.Guardian = 3
EnumType.PartnerType.SoulFire = 4

--装备品质类型
EnumType.QualityType = {}
EnumType.QualityType.White = 1
EnumType.QualityType.Green = 2
EnumType.QualityType.Blue = 3
EnumType.QualityType.Purple = 4
EnumType.QualityType.Orange = 5
EnumType.QualityType.Red = 6
EnumType.QualityType.Pink = 7

--飘字类型
EnumType.FloatTextType = {}
EnumType.FloatTextType.Static = 1
EnumType.FloatTextType.Down2Up = 2
EnumType.FloatTextType.Right2Left = 3
EnumType.FloatTextType.Subtitle = 4
EnumType.FloatTextType.ExpAdd = 5
EnumType.FloatTextType.PowerAdd = 6

--Role界面类型
EnumType.RolePanelType = {}
EnumType.RolePanelType.Attri = 1
EnumType.RolePanelType.Bag = 2
EnumType.RolePanelType.Duke = 3
EnumType.RolePanelType.Fashion = 4
EnumType.RolePanelType.Title = 5
EnumType.RolePanelType.Star = 6

--Boss界面类型
EnumType.BossPanelType = {}
EnumType.BossPanelType.WorldBoss = 1
EnumType.BossPanelType.PersonalBoss = 2
EnumType.BossPanelType.BossHome = 3
EnumType.BossPanelType.BackWoods = 4
EnumType.BossPanelType.DropRecord = 5

--结婚界面类型
EnumType.MarriagePanelType = {}
EnumType.MarriagePanelType.Info = 1
EnumType.MarriagePanelType.WeddingRing = 2
EnumType.MarriagePanelType.Baby = 3
EnumType.MarriagePanelType.Instance = 4
EnumType.MarriagePanelType.Chest = 5

--送花界面类型
EnumType.FlowerPanelType = {}
EnumType.FlowerPanelType.Give = 1
EnumType.FlowerPanelType.Success = 2
EnumType.FlowerPanelType.Report = 3

--模型界面显示类型
EnumType.PlayerModelType = {}
EnumType.PlayerModelType.ModelAndStaticHead = 1 --普通模型展示(静态头像)
EnumType.PlayerModelType.SkillPreview = 2 --技能模型展示
EnumType.PlayerModelType.DynamicHead = 3 --界面不展示模型，只显示动态头像
EnumType.PlayerModelType.ModelAndNoHead = 4 --普通模型展示(不处理头像)

--合成类型
EnumType.ComposeType = {}
EnumType.ComposeType.Item = 1
EnumType.ComposeType.Gem = 2
EnumType.ComposeType.Soul = 3
EnumType.ComposeType.Equip = 4

--VIP界面标签类型
EnumType.VIPType = {}
EnumType.VIPType.VIP = 1
EnumType.VIPType.VIPCharge = 2
EnumType.VIPType.MonthCard = 3
EnumType.VIPType.Investment = 4
EnumType.VIPType.Level = 5
EnumType.VIPType.Add = 6

--公会界面标签类型
EnumType.GuildType = {}
EnumType.GuildType.Normal = 1
EnumType.GuildType.Warehouse = 2
EnumType.GuildType.Skill = 3
EnumType.GuildType.Activity = 4

--公会成员操作类型
EnumType.GuildAction = {}
EnumType.GuildAction.LookOver = 1 --查看信息
EnumType.GuildAction.GiveFlower = 2 --赠送鲜花
EnumType.GuildAction.Chat = 3 --开始聊天
EnumType.GuildAction.InviteTeam = 4 --邀请入队
EnumType.GuildAction.ApplyForTeam = 5 --申请入队
EnumType.GuildAction.AddFriend = 6 --添加好友
EnumType.GuildAction.DeleteFriend = 7 --删除好友
EnumType.GuildAction.AddToBlacklist = 8 --加入黑名单
EnumType.GuildAction.RemoveFromBlacklist = 9 --移除黑名单
EnumType.GuildAction.DeleteFoeFriend = 10 --删除仇敌
EnumType.GuildAction.KickOutTeam = 11 --踢出队伍

EnumType.GuildAction.UpToViceLeader = 12 --升为副会长
EnumType.GuildAction.KickOutViceLeader = 13 --解除副会长
EnumType.GuildAction.KickOutElite = 14 --解除长老
EnumType.GuildAction.DownToElite = 15 --降为长老
EnumType.GuildAction.UpToElite = 16 --升为长老
EnumType.GuildAction.KickOutGuild = 17 --踢出公会
EnumType.GuildAction.MakeOverLeader = 18 --转让会长

--好友聊天面板类型
EnumType.FriendChatType = {}
EnumType.FriendChatType.Recent = 1 --最近联系人
EnumType.FriendChatType.Friend = 2 --好友列表
EnumType.FriendChatType.Foe = 3 --仇敌列表
EnumType.FriendChatType.Black = 4 --黑名单

--图鉴面板类型
EnumType.CardType = {}
EnumType.CardType.Total = 1 --总览
EnumType.CardType.Groud = 2 --组
EnumType.CardType.Up = 3 --升级
EnumType.CardType.Att = 4 --属性
EnumType.CardType.Bag = 5 --背包


--好友界面类型
EnumType.FriendPanelType = {}
EnumType.FriendPanelType.Chat = 1 --聊天面板
EnumType.FriendPanelType.Apply = 2 --申请面板
EnumType.FriendPanelType.Add = 3 --添加好友面板
EnumType.FriendPanelType.Att = 4 --亲密度属性面板

--升级指引
EnumType.UpgradeGuideType = {}
EnumType.UpgradeGuideType.Gold = 1 --赏金任务
EnumType.UpgradeGuideType.Exp = 2 --经验副本
EnumType.UpgradeGuideType.Escort = 3 --护送女神
EnumType.UpgradeGuideType.Map = 4 --挂机


--福利面板
EnumType.WelfarePanelType = {}
EnumType.WelfarePanelType.Sign = 1 --聊天面板
EnumType.WelfarePanelType.Gift = 2 --申请面板
EnumType.WelfarePanelType.Exchange = 3 --添加好友面板
EnumType.WelfarePanelType.Announce = 4 --亲密度属性面板



--开服活动
EnumType.ServiceActivityType = {}
EnumType.ServiceActivityType.Rank = 1 --开服冲榜
EnumType.ServiceActivityType.Ring = 2 --魔戒寻主
EnumType.ServiceActivityType.Collect = 3 --集字有礼
EnumType.ServiceActivityType.Love = 4 --完美情人
EnumType.ServiceActivityType.Union = 5 --工会争霸

--开服冲榜类型
EnumType.OpenRankType = {}
EnumType.OpenRankType.Level = 1 --冲级达人
EnumType.OpenRankType.Pet = 2   --坐骑进阶
EnumType.OpenRankType.Horse = 3 --驯宠达人
EnumType.OpenRankType.Change = 4 --今日充值
EnumType.OpenRankType.Gem = 5 --宝石镶嵌
EnumType.OpenRankType.Combat = 6 --战力排行

--魔戒寻主类型
EnumType.LordRingType = {}
EnumType.LordRingType.Initial = 1 --初始之戒
EnumType.LordRingType.Chaos = 2   --混沌之戒
EnumType.LordRingType.Destroy = 3 --毁灭之戒
EnumType.LordRingType.Guard = 4 --守护之戒
EnumType.LordRingType.Power = 5 --神力之戒

--游戏提醒类型
EnumType.RemindType = {}
EnumType.RemindType.MailUnRead = 1 --邮件未读
EnumType.RemindType.GuildGetRed = 2 --公会是否有可以拿的红包
EnumType.RemindType.BagFill = 3 --背包是否满
EnumType.RemindType.DailyResource = 4 --日常活动是否有可进行的资源
EnumType.RemindType.ConvoyHave = 5 --是否在护送中
EnumType.RemindType.LimitActivity = 6
EnumType.RemindType.LimitActivityM = 7
EnumType.RemindType.LimitActivityH = 8
EnumType.RemindType.FriendNewChat = 9
EnumType.RemindType.FriendApply   = 10
EnumType.RemindType.LimitedShopping = 11--限时抢购

--开服活动类型
EnumType.OpenActivityType = {}
EnumType.OpenActivityType.Happy = 413
EnumType.OpenActivityType.Rank = 127
EnumType.OpenActivityType.Ring = 412
EnumType.OpenActivityType.BuyZero = 409
EnumType.OpenActivityType.Invest = 635
EnumType.OpenActivityType.SevenDay = 407



--装备小面板显示类型 
EnumType.NewEquipType = {}
EnumType.NewEquipType.Equip = 1 --装备
EnumType.NewEquipType.Use = 2 --使用
EnumType.NewEquipType.Renew = 3 --续费
EnumType.NewEquipType.OffineRemind = 4 --离线提醒



--TextAnchor enum
EnumType.TextAnchor = {}
EnumType.TextAnchor.UpperLeft = 0
EnumType.TextAnchor.UpperCenter = 1
EnumType.TextAnchor.UpperRight = 2
EnumType.TextAnchor.MiddleLeft = 3
EnumType.TextAnchor.MiddleCenter = 4
EnumType.TextAnchor.MiddleRight = 5
EnumType.TextAnchor.LowerLeft = 6
EnumType.TextAnchor.LowerCenter = 7
EnumType.TextAnchor.LowerRight = 8


--世界服界面类型
EnumType.WorldServerPanelType = {}
-- EnumType.WorldServerPanelType.GodCity          = 1
EnumType.WorldServerPanelType.GodMonsterIsland = 1
-- EnumType.WorldServerPanelType.BossHome         = 3
-- EnumType.WorldServerPanelType.BackWoods        = 4
EnumType.WorldServerPanelType.DropRecord = 2

--装备模型类型
EnumType.EquipModelType = {}
EnumType.EquipModelType.Cloth = 1 
EnumType.EquipModelType.Weapon = 11 
EnumType.EquipModelType.Wing = 21
EnumType.EquipModelType.Head = 31
EnumType.EquipModelType.Hair = 41
EnumType.EquipModelType.DeputyWeapon = 51
EnumType.EquipModelType.Manteau = 61
EnumType.EquipModelType.Effect = 71

--服务器状态类型位
EnumType.ServerStateType = {}
EnumType.ServerStateType.Close     = 1
EnumType.ServerStateType.Fluency   = 2
EnumType.ServerStateType.Crowd     = 4
EnumType.ServerStateType.Hot       = 8
EnumType.ServerStateType.New       = 16
EnumType.ServerStateType.Recommend = 32

-- Boss类型
EnumType.BossType = {}
EnumType.BossType.BossHome     = 1
EnumType.BossType.BossWorld     = 2
EnumType.BossType.BackBoss     = 3
EnumType.BossType.GodIsLand     = 4

--九宫格动态图标类型
EnumType.CommonIconType = {}
EnumType.CommonIconType.Bubble     = 1

-- fps停止检测类型
EnumType.PauseFpsType = {}
EnumType.PauseFpsType.PowerSave     = 1
EnumType.PauseFpsType.SceneChange     = 2

return EnumType
