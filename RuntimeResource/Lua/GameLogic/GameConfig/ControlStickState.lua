-- Global.lua
ControlStickState = {}

local LuaGameState = GameWorld.LuaGameState

ControlStickState.controlStickDirection = Vector2.zero
ControlStickState.controlStickStrength = 0
ControlStickState.controlStickIsDragging = false
ControlStickState.controlStickFingerId = 0

function ControlStickState:SetControlStickDirection(direction)
    self.controlStickDirection = direction
    LuaGameState.controlStickDirection = direction
end

function ControlStickState:SetControlStickStrength(strength)
    self.controlStickStrength = strength
    LuaGameState.controlStickStrength = strength
end

function ControlStickState:SetControlStickIsDragging(isDraging)
    self.controlStickIsDragging = isDraging
    LuaGameState.controlStickIsDragging = isDraging
end

function ControlStickState:SetControlStickFingerId(fingerId)
    self.controlStickFingerId = fingerId
    LuaGameState.controlStickFingerId = fingerId
end   

return ControlStickState