-- OpenHappyConfig.lua
local OpenHappyConfig = {}

OpenHappyConfig.State_NoRewad = 0
OpenHappyConfig.State_CanReward = 1
OpenHappyConfig.State_Rewarded = 2


OpenHappyConfig.EventState_NotOpen = 0
OpenHappyConfig.EventState_Open = 1
OpenHappyConfig.EventState_Finish = 2

OpenHappyConfig.BEGIN_SERVER_DAY = 2 -- 开服第2天开始
OpenHappyConfig.END_SERVER_DAY = 4 -- 开服第四天结束

OpenHappyConfig.REWARD_SORT_WEIGHT = {
	[OpenHappyConfig.State_CanReward] = 3,
	[OpenHappyConfig.State_NoRewad] = 2,
	[OpenHappyConfig.State_Rewarded] = 1,
}

OpenHappyConfig.EVENT_SORT_WEIGHT = {
	[OpenHappyConfig.EventState_Open] = 3,
	[OpenHappyConfig.EventState_NotOpen] = 2,
	[OpenHappyConfig.EventState_Finish] = 1,
}


return OpenHappyConfig