-- WingConfig.lua
local WingConfig = {}

WingConfig.GemQualityText = {[2] = "C", [3] = "B", [4] = "A", [5] = "S"}--宝珠品质文字
WingConfig.GemQualityIcon = {[2] = 5091, [3] = 5092, [4] = 5093, [5] = 5094}--宝珠品质背景图

return WingConfig
