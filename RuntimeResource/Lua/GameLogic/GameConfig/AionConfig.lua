-- AionConfig.lua
local AionConfig = {}


AionConfig.LOCK = 1   --未解锁
AionConfig.CURRENT = 2   --当前层
AionConfig.UNLOCK = 3   --已解锁

AionConfig.POPUP_CONFIRM = 1   --确定框
AionConfig.POPUP_START_SWEEP = 2   --开始扫荡
AionConfig.POPUP_SWEEPING = 3   --扫荡中
AionConfig.POPUP_FIRST_REWARD = 4   --首通奖励
AionConfig.POPUP_SUCCESS = 5   --挑战成功
AionConfig.POPUP_FAIL = 6   --挑战失败
AionConfig.POPUP_RANKING_VIEW = 7   --排行榜
AionConfig.POPUP_MY_LAYER_DATA = 8   --通关情况
AionConfig.POPUP_SWEEP_SETTLE = 9   --扫荡结算

AionConfig.AION_UNLOCK_RUNE = 1         --解锁符文槽
AionConfig.AION_UNLOCK_NEW  = 2         --解锁新符文

return AionConfig
