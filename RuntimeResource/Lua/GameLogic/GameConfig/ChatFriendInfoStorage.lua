-- ChatFriendInfoStorage.lua
ChatFriendInfoStorage = {}

local CacheConfig = GameConfig.CacheConfig

function ChatFriendInfoStorage:Init()
    -- self:Load()   
end


function ChatFriendInfoStorage:Load(uuid)
    local data = GameWorld.ReadCache( (CacheConfig.ChatFriendData .. uuid), true)
    if data == nil then
        return {}
    else
        local dataJson = GameWorld.json.decode(data[0])
        local dataList = {}
        for k,v in pairs(dataJson) do
            dataList[k] = GameWorld.json.decode(v)
        end
        return dataList
    end
end

function ChatFriendInfoStorage:Save(uuid,list)
    local tempData = {}
    for k,v in pairs(list) do
        tempData[k] = GameWorld.json.encode(v)
    end

    local dataStr = GameWorld.json.encode(tempData)
    GameWorld.SaveCache((CacheConfig.ChatFriendData .. uuid), {dataStr}, true)
end

return ChatFriendInfoStorage