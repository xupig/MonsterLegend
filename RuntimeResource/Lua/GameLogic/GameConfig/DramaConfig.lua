--DramaConfig.lua
DramaConfig = {}

DramaConfig.TAG_ACTION = 1
DramaConfig.TAG_ARG1 = 2
DramaConfig.TAG_ARG2 = 3
DramaConfig.TAG_ARG3 = 4
DramaConfig.TAG_ARG4 = 5
DramaConfig.TAG_ARG5 = 6
DramaConfig.TAG_ARG6 = 7
DramaConfig.TAG_ARG7 = 8
DramaConfig.TAG_ARG = 1
DramaConfig.MARK_ON_END = "_ON_END"

DramaConfig._STATE_IDLE = 0
DramaConfig._STATE_lOADING = 1
DramaConfig._STATE_RUNNING = 2  

DramaConfig.SKIP_EVENT_NAME = "SKIP_DIALOG"
DramaConfig.SKIP_EVENT_VALUE = "0"


function DramaConfig:IsInternalKeyWord(actionName)
    if actionName == self.MARK_ON_END then
        return true
    else
        return false
    end
end

function DramaConfig:IsMark(actionName)
    if "__" == string.sub(actionName,1,2) then
        return true
    else
        return false
    end
end

return DramaConfig