-- InstanceCommonConfig.lua
local InstanceCommonConfig = {}

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

InstanceCommonConfig.COMMON_INSTANCE_SHOW_COUNTDOWN = 1                 --显示副本结束的倒计时
InstanceCommonConfig.COMMON_INSTANCE_BEGIN_COUNTDOWN = 2                --副本开始倒计时
InstanceCommonConfig.COMMON_INSTANCE_SHOW_START = 3                     --显示评价等级
InstanceCommonConfig.COMMON_INSTANCE_COUNT_DOWN = 4                     --更新时间(1秒执行一次)
InstanceCommonConfig.COMMON_INSTANCE_SHOW_INFO = 5                      --显示左侧副本信息：通关目标、副本描述
InstanceCommonConfig.COMMON_INSTANCE_LEAVE_COUNTDOWN = 6                --显示自动离开副本的倒计时
InstanceCommonConfig.COMMON_INSTANCE_STOP_COUNTDOWN = 7                 --停止副本开始倒计时
InstanceCommonConfig.COMMON_INSTANCE_ENTER_INFO = 8                 --进入副本界面


InstanceCommonConfig.START_DROP_MESSAGE_MAP = {
    [0] = LanguageDataHelper.CreateContent(71016),
    [1] = LanguageDataHelper.CreateContent(71015),
    [2] = LanguageDataHelper.CreateContent(71014),
    [3] = LanguageDataHelper.CreateContent(71001)
}

InstanceCommonConfig.COUNT_DOWN_MESSAGE_MAP = {
    [0] = 71002,
    [1] = 71002,
    [2] = 71002,
    [3] = 71002
}

InstanceCommonConfig.COMMON_INSTANCE_INFO_DESC_TYPE1 = 1                --普通副本显示副本描述类型1；1.杀怪类型，参数：中文ID，怪物ID，数量,怪物ID，数量
InstanceCommonConfig.COMMON_INSTANCE_INFO_DESC_TYPE2 = 2                --普通副本显示副本描述类型2；2.坚持数波怪物，参数：中文ID，总波数，间隔时间(秒) 显示格式： 击败所有怪物 剩余波数  2/5

return InstanceCommonConfig
