-- RedPointConfig.lua
local RedPointConfig = {}

--节点类型
RedPointConfig.TYPE_ROOT = 1                    --根节点(1级)
RedPointConfig.TYPE_HEAD = 2                    --头像节点
RedPointConfig.TYPE_FIRST_MENU = 3              --主界面图标节点
RedPointConfig.TYPE_SECOND_MENU = 4             --头像二级界面图标节点
RedPointConfig.TYPE_PANEL = 5                   --panel节点
RedPointConfig.TYPE_PANEL_FIRST = 6             --panel一级标签节点
RedPointConfig.TYPE_PANEL_SECOND = 7            --panel二级标签节点

--变强中文
RedPointConfig.CARD_RED             = 80045     --图鉴
RedPointConfig.NOBILITY_UPGRADE     = 80000     --爵位
RedPointConfig.EQUIP_LOAD          	= 80001     --更换装备

RedPointConfig.PET_UPGRADE          = 80021     --宠物升星
RedPointConfig.PET_SOUL             = 80024     --宠物兽魂
RedPointConfig.PET_DEVOUR           = 76912     --宠物吞噬
RedPointConfig.HORSE_UPGRADE        = 80022     --坐骑升星
RedPointConfig.HORSE_SOUL           = 80026     --坐骑兽魂
RedPointConfig.FASHION_CLOTH        = 80041     --时装衣服
RedPointConfig.FASHION_WEAPON       = 80042     --时装武器
RedPointConfig.FASHION_EFFECT       = 80043     --时装特效
RedPointConfig.FASHION_PHOTO        = 630       --时装武器
RedPointConfig.FASHION_CHAT         = 631       --时装特效

RedPointConfig.RUNE_INSERT          = 80028     --符文镶嵌
RedPointConfig.RUNE_UP              = 81634     --符文升级

RedPointConfig.WING_UPGRADE          = 80012     --翅膀升星
RedPointConfig.WING_SOUL             = 80015     --翅膀培养
RedPointConfig.TALISMAN_UPGRADE      = 80013     --法宝升星
RedPointConfig.TALISMAN_SOUL         = 80017     --法宝培养
RedPointConfig.WEAPON_UPGRADE        = 80014     --神兵升星
RedPointConfig.WEAPON_SOUL           = 80019     --神兵培养

RedPointConfig.WING_TRANSFORM        = 80016     --翅膀幻化
RedPointConfig.TALISMAN_TRANSFORM    = 80018     --法宝幻化
RedPointConfig.WEAPON_TRANSFORM      = 80020     --神兵幻化

RedPointConfig.PET_TRANSFORM    	= 80025     --宠物幻化
RedPointConfig.RIDE_TRANSFORM      	= 80027     --坐骑幻化

RedPointConfig.EQUIP_STRENGTH       = 80007     --装备强化
RedPointConfig.GEM_INLAY    		= 80008     --宝石镶嵌
RedPointConfig.GEM_REFINE    		= 80009     --宝石精炼
RedPointConfig.LEGEND_SUIT    		= 80010     --传奇套装
RedPointConfig.WARLORD_SUIT    		= 80011     --战神套装

RedPointConfig.ACHIEVEMENT          = 80046     --成就获取

RedPointConfig.APPEAR_LEVEL_UP      = 80051     --外形升级
return RedPointConfig