-- DailyConfig.lua
local DailyConfig = {}

---- 跳转类型
DailyConfig.FOLLOW_OPEN_PANEL = 1                   -- 打开界面，填功能ID
DailyConfig.FOLLOW_EVENT = 2                        -- 事件,填事件名称

--日常奖励状态
DailyConfig.None = 1                                --未到达可领取状态
DailyConfig.CAN_GET = 2                             --可领取
DailyConfig.ALREADY_GET = 3                         --已经领取

--活动类型
DailyConfig.Must = 1                                --每日必做
DailyConfig.Limit = 2                               --限时活动

--资源找回类型
DailyConfig.REWARD_BACK_TYPE_EX = 0                 --钻石找回
DailyConfig.REWARD_BACK_TYPE_COMMON = 1             --金币找回

DailyConfig.WEEK_TEXT_DIC = {
    [1] = 902,--一
    [2] = 903,--二
    [3] = 904,--三
    [4] = 905,--四
    [5] = 906,--五
    [6] = 907,--六
    [7] = 908 --日
}

-- ============================聊天框 动画 坐标==============================================
DailyConfig.TipRightPos = {
    x = 220,
    y = 120,
}

DailyConfig.TipLeftPos = {
    x = 680,
    y = 80,
}


return DailyConfig
