-- 定义主角行为的一些宏
PlayerActionConfig = {}

-- 优先级别定义
PlayerActionConfig.PRIORITY_NONE 	= 0			-- 没有优先级
PlayerActionConfig.PRIORITY_ONE 	= 1 		-- 优先级别一
PlayerActionConfig.PRIORITY_TWO	    = 2 		-- 优先级别二
PlayerActionConfig.PRIORITY_THREE	= 3 		-- 优先级别三

PlayerActionConfig.ACTION_READY	= 0 		-- 动作准备
PlayerActionConfig.ACTION_DOING	= 1 		-- 动作进行
PlayerActionConfig.ACTION_FAIL	= 2 		-- 动作失败
PlayerActionConfig.ACTION_END   = 3 		-- 动作完成

return PlayerActionConfig