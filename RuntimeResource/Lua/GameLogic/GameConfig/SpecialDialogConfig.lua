-- SpecialDialogConfig.lua
local SpecialDialogConfig = {}


SpecialDialogConfig.EVENT_ACTIVITY = 1   --事件触发
SpecialDialogConfig.TIME_STAMP = 2   --时间点
SpecialDialogConfig.ACCEPT_TASK = 3 -- 接取任务
SpecialDialogConfig.COMPLETE_TASK = 4 -- 完成任务
SpecialDialogConfig.GET_ITEM = 5 -- 获得物品
SpecialDialogConfig.AREA_TRIGGER = 6 -- 区域触发
SpecialDialogConfig.HP_VALUE = 7 -- BOSS血量
SpecialDialogConfig.OWN_ITEM = 8 -- 拥有某物品
SpecialDialogConfig.DIALOG_FINISH = 9 -- 对话结束
SpecialDialogConfig.DISTANCE_TRIGGER = 10 -- 距离触发
SpecialDialogConfig.ENTER_BATTLE = 11 -- 进入战斗
SpecialDialogConfig.LEAVE_BATTLE = 12 -- 离开战斗
SpecialDialogConfig.SERVER_SETTLE = 13 -- 副本结算
SpecialDialogConfig.PLAYER_DIE = 14 -- 角色死亡

SpecialDialogConfig.SELF = 1 -- 自己
SpecialDialogConfig.NPC = 2 -- npc
SpecialDialogConfig.MONSTER = 3 -- 怪物

return SpecialDialogConfig
