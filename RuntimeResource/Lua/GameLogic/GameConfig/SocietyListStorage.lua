--SocietyListStorage.lua
SocietyListStorage = {}
local CacheConfig = GameConfig.CacheConfig

function SocietyListStorage:Init()

end

--最近联系人
function SocietyListStorage:LoadRecentList()
    local data = GameWorld.ReadCache( tostring(CacheConfig.RecentListData), true)
    if data == nil then
        return {}
    else
        local dataJson = GameWorld.json.decode(data[0])
        local dataList = {}
        for k,v in pairs(dataJson) do
            dataList[k] = GameWorld.json.decode(v)
        end

        -- GameWorld.LoggerHelper.Log("【最近联系】取出数据")
        -- GameWorld.LoggerHelper.Log(dataList)
        return dataList
    end
end

function SocietyListStorage:SaveRecentList(list)
    local tempData = {}
    for k,v in pairs(list) do
        tempData[k] = GameWorld.json.encode(v)
    end

    local dataStr = GameWorld.json.encode(tempData)
    GameWorld.SaveCache(tostring(CacheConfig.RecentListData), {dataStr}, true)

    -- GameWorld.LoggerHelper.Log("【最近联系】保存数据")
    -- GameWorld.LoggerHelper.Log(list)
end

--最近组队
function SocietyListStorage:LoadTeamList()

    local data = GameWorld.ReadCache( tostring(CacheConfig.TeamListData), true)
    if data == nil then
        return {}
    else
        local dataJson = GameWorld.json.decode(data[0])
        local dataList = {}
        for k,v in pairs(dataJson) do
            dataList[k] = GameWorld.json.decode(v)
        end

        -- GameWorld.LoggerHelper.Log("【最近组队】取出数据")
        -- GameWorld.LoggerHelper.Log(dataList)
        return dataList
    end
end

function SocietyListStorage:SaveTeamList(list)
    local tempData = {}
    for k,v in pairs(list) do
        tempData[k] = GameWorld.json.encode(v)
    end

    local dataStr = GameWorld.json.encode(tempData)
    GameWorld.SaveCache(tostring(CacheConfig.TeamListData), {dataStr}, true)
    
    -- GameWorld.LoggerHelper.Log("【最近组队】保存数据")
    -- GameWorld.LoggerHelper.Log(list)
end


return SocietyListStorage
