-- MenuConfig.lua
local MenuConfig = {}

MenuConfig.MAIN_MENU = 1        --主界面菜单
MenuConfig.SUB_MENU = 2         --二级菜单
MenuConfig.DYNAMIC_MAIN_MENU = 3  --动态创建主界面菜单
MenuConfig.STATIC_MENU = 4  	--固定位置并且遵循子菜单功能的菜单

MenuConfig.SHOW_MANY_PANEL = 7   --不同状态显示不同的panel
MenuConfig.SHOW_PANEL = 8   --打开面板
MenuConfig.FIND_NPC = 9   --寻找NPC

return MenuConfig
