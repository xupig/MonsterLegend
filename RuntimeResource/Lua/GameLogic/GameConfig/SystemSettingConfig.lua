-- SystemSettingConfig.lua
local SystemSettingConfig = {}

--同屏人数设置
SystemSettingConfig.PLAYER_MIN_NUM          = 0             --同屏人数设置最小值
SystemSettingConfig.PLAYER_MAX_NUM          = 25            --同屏人数设置最大值
SystemSettingConfig.PLAYER_DEFAULT_NUM      = 10            --同屏人数设置默认值

--画质设置
-- SystemSettingConfig.IMAGE_DEFAULT           = 2             --画质设置默认值(1.急速；2.高清；3.超清)
SystemSettingConfig.QUALITY_LOW	= 2 		    -- 画质流畅 (1.急速)
SystemSettingConfig.QUALITY_MIDDLE	= 1 		-- 画质中等(2.高清)
SystemSettingConfig.QUALITY_HIGH	= 0 		-- 画质精细(3.超清)

--音乐设置
SystemSettingConfig.MUSIC_MIN_NUM           = 0             --音乐设置最小值
SystemSettingConfig.MUSIC_MAX_NUM           = 100           --音乐设置最大值
SystemSettingConfig.MUSIC_DEFAULT_NUM       = 75            --音乐设置默认值

--音效设置
SystemSettingConfig.SOUND_MIN_NUM           = 0             --音效设置最小值
SystemSettingConfig.SOUND_MAX_NUM           = 100           --音效设置最大值
SystemSettingConfig.SOUND_DEFAULT_NUM       = 75            --音效设置默认值

--功能设置
SystemSettingConfig.HIDE_OTHER_PET_DEFAULT          = false         --屏蔽他人宠物设置
SystemSettingConfig.HIDE_OTHER_WING_DEFAULT         = false         --屏蔽他人翅膀设置
SystemSettingConfig.HIDE_OTHER_HUNQI_DEFAULT        = false         --屏蔽他人魂器设置
SystemSettingConfig.HIDE_OTHER_TITLE_DEFAULT        = false         --屏蔽他人称号设置
SystemSettingConfig.HIDE_OTHER_SKILLFX_DEFAULT      = false         --屏蔽他人技能特效设置
SystemSettingConfig.HIDE_OTHER_EQUIPFLOW_DEFAULT    = false         --屏蔽他人装备流光设置
SystemSettingConfig.HIDE_OTHER_MONSTER_DEFAULT      = false         --屏蔽怪物设置
SystemSettingConfig.HIDE_OTHER_FLOWERFX_DEFAULT     = false         --屏蔽送花特效设置
SystemSettingConfig.HIDE_REALTIME_SHADOWS_DEFAULT   = false         --实时阴影
SystemSettingConfig.AUTO_TEAMCONFIRM_DEFAULT        = false         --组队自动确认
SystemSettingConfig.SAVE_POWER_MODE_DEFAULT         = true          --省电模式

--自动拾取
SystemSettingConfig.AUTO_GET_WHITE_DEFAULT  = true          --白色装备
SystemSettingConfig.AUTO_GET_BLUE_DEFAULT   = true          --蓝色装备
SystemSettingConfig.AUTO_GET_PURPLE_DEFAULT = true          --紫色装备
SystemSettingConfig.AUTO_GET_ORANGE_DEFAULT = true          --橙色及以上装备
SystemSettingConfig.AUTO_GET_MONEY_DEFAULT  = true          --金币
SystemSettingConfig.AUTO_GET_OTHER_DEFAULT  = true          --其他

--自动出售
SystemSettingConfig.AUTO_SELL_DEFAULT       = true          --背包小于5格时自动出售白蓝装备

--自动吞噬
SystemSettingConfig.AUTO_DEVOUR_DEFAULT     = false         --背包小于5格时自动吞噬评分低的紫装

--自动复活
SystemSettingConfig.AUTO_RESURRECT_DEFAULT  = false         --在线状态死亡时，5秒后自动扣除20钻石（优先绑钻）原地复活



--基本设置定义（基于设备）
SystemSettingConfig.RENDER_QUALITY          = "1"           --画质
SystemSettingConfig.PLAYER_NUM              = "2"           --同屏人数
SystemSettingConfig.MUSIC_VOLUME            = "3"           --音乐
SystemSettingConfig.SOUND_VOLUME            = "4"           --音效
SystemSettingConfig.OTHER_PET               = "5"           --屏蔽他人宠物设置
SystemSettingConfig.OTHER_WING              = "6"           --屏蔽他人翅膀设置
SystemSettingConfig.OTHER_HUNQI             = "7"           --屏蔽他人魂器设置
SystemSettingConfig.OTHER_TITLE             = "8"           --屏蔽他人称号设置
SystemSettingConfig.OTHER_SKILLFX           = "9"           --屏蔽他人技能特效设置
SystemSettingConfig.OTHER_EQUIPFLOW         = "10"          --屏蔽他人装备流光设置
SystemSettingConfig.OTHER_MONSTER           = "11"          --屏蔽怪物设置
SystemSettingConfig.OTHER_FLOWERFX          = "12"          --屏蔽送花特效设置
SystemSettingConfig.REALTIME_SHADOWS        = "13"          --实时阴影
SystemSettingConfig.AUTO_TEAMCONFIRM        = "14"          --自动组队确认
SystemSettingConfig.SAVE_POWER_MODE         = "15"          --省电模式

--挂机设置（基于角色）
SystemSettingConfig.AUTO_GET_WHITE          = "1"         --白色装备
SystemSettingConfig.AUTO_GET_BLUE           = "2"         --蓝色装备
SystemSettingConfig.AUTO_GET_PURPLE         = "3"         --紫色装备
SystemSettingConfig.AUTO_GET_ORANGE         = "4"         --橙色及以上装备
SystemSettingConfig.AUTO_GET_MONEY          = "5"         --金币
SystemSettingConfig.AUTO_GET_OTHER          = "6"         --其他
SystemSettingConfig.AUTO_SELL               = "7"         --背包小于5格时自动出售白蓝装备
SystemSettingConfig.AUTO_DEVOUR             = "8"         --背包小于5格时自动吞噬评分低的紫装
SystemSettingConfig.AUTO_RESURRECT          = "9"         --在线状态死亡时，5秒后自动扣除20钻石（优先绑钻）原地复活


return SystemSettingConfig