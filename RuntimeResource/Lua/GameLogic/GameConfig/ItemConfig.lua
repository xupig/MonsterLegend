--ItemConfig.lua
ItemConfig = {}

local Color = require "UnityEngine.Color"
local public_config = GameWorld.public_config
local x = 1
ItemConfig.EXPBOOK= 7

ItemConfig.ItemQualityWhite = 1
ItemConfig.ItemQualityGreen = 2
ItemConfig.ItemQualityBlue = 3
ItemConfig.ItemQualityPurple = 4
ItemConfig.ItemQualityOrange = 5
ItemConfig.ItemQualityRed   = 6
ItemConfig.ItemQualityGold = 7

ItemConfig.MoneyTypeMap = {
    [public_config.MONEY_TYPE_COUPONS] = "money_coupons",
    [public_config.MONEY_TYPE_COUPONS_BIND] = "money_coupons_bind",
    [public_config.MONEY_TYPE_GOLD] = "money_gold",
    [public_config.MONEY_TYPE_ACTIVITY] = "money_activity",
    [public_config.MONEY_TYPE_GUILD_CONTRIBUTE] = "money_guild_contribute",
    [public_config.MONEY_TYPE_ATHLETICS] = "money_athletics",
    [public_config.MONEY_TYPE_GUARDIAN_DIAMOND] = "money_guardian",
    [public_config.MONTY_TYPE_DRAGON_PICEC] = "money_dragon_piece"
}

--物品品质对应名字颜色(font_style.xml)
ItemConfig.QualityTextMap = {
    [ItemConfig.ItemQualityWhite]   = 101,
    [ItemConfig.ItemQualityGreen]   = 102,
    [ItemConfig.ItemQualityBlue]    = 103,
    [ItemConfig.ItemQualityPurple]  = 104,
    [ItemConfig.ItemQualityOrange]  = 105,
    [ItemConfig.ItemQualityRed]     = 106,
    [ItemConfig.ItemQualityGold]    = 107
}

ItemConfig.RuneBgMap = {
    [ItemConfig.ItemQualityWhite]   = 13051,
    [ItemConfig.ItemQualityGreen]   = 13051,
    [ItemConfig.ItemQualityBlue]    = 13052,
    [ItemConfig.ItemQualityPurple]  = 13053,
    [ItemConfig.ItemQualityOrange]  = 13054,
    [ItemConfig.ItemQualityRed]     = 13055,
    [ItemConfig.ItemQualityGold]    = 13055
}

ItemConfig.QualityCN = {137,138,139,140,141,53164,142}   --品质颜色中文表对应的中文

ItemConfig.ItemNormal = 50     --普通白
ItemConfig.ItemForbidden = 57   --数值不足红 
ItemConfig.ItemAttriValue = 59 --数值颜色绿
ItemConfig.ItemDisabled = 52  --禁用灰
ItemConfig.SkillDesc = 56       --技能橙

ItemConfig.RankC   = 1
ItemConfig.RankB   = 2
ItemConfig.RankA   = 3
ItemConfig.RankS   = 4

ItemConfig.RankMap = {
    [ItemConfig.RankC] = 102,
    [ItemConfig.RankB] = 103,
    [ItemConfig.RankA] = 104,
    [ItemConfig.RankS] = 105,
}

return ItemConfig