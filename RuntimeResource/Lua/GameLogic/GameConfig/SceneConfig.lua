-- SceneConfig.lua
local SceneConfig = {}


SceneConfig.SCENE_TYPE_MAIN = 1                         --主场景野外
SceneConfig.SCENE_TYPE_WILD = 2                         --主场景野外
SceneConfig.SCENE_TYPE_PVP6 = 3                         --6V6PVP
SceneConfig.SCENE_TYPE_PVP_NEW = 4                      --新PVP场景
SceneConfig.SCENE_TYPE_COMBAT = 6                       --战斗副本
SceneConfig.SCENE_TYPE_GLIDE = 7                        --滑翔场景
SceneConfig.SCENE_TYPE_FIGHT_FRIEND = 8                 --好友切磋场景
SceneConfig.SCENE_TYPE_LOGIN = 101                      --登陆
SceneConfig.SCENE_TYPE_CHARACTOR = 102                  --创角选角
SceneConfig.SCENE_TYPE_COMMON_BATTLE = 10               --普通副本
SceneConfig.SCENE_TYPE_HERO_BATTLE = 11                 --英雄副本
SceneConfig.SCENE_TYPE_HELL_BATTLE = 12                 --噩梦副本
SceneConfig.SCENE_TYPE_RAID_BATTLE = 13                 --Raid副本
SceneConfig.SCENE_TYPE_ABYSS = 14                       --深渊副本
SceneConfig.SCENE_TYPE_MIRROR = 15                      --镜像副本
SceneConfig.SCENE_TYPE_WILD_MIRROR = 16                 --野外镜像副本
SceneConfig.SCENE_TYPE_LADDER_1V1  = 17                 --天梯1v1
SceneConfig.SCENE_TYPE_GUILD_MANOR = 18                 --公会领地
SceneConfig.SCENE_TYPE_AION_TOWER = 19                  --永恒之塔
SceneConfig.SCENE_TYPE_GUILD_RAID = 20                  --公会Raid
SceneConfig.SCENE_TYPE_WORLD_BOSS = 21                  --世界BOSS
SceneConfig.SCENE_TYPE_EXP_INSTANCE = 23                --经验副本
SceneConfig.SCENE_TYPE_PET_INSTANCE = 24                --守护结界塔
SceneConfig.SCENE_TYPE_BOSS_HOME = 25                   --BOSS之家
SceneConfig.SCENE_TYPE_MY_BOSS = 26                     --个人BOSS
SceneConfig.SCENE_TYPE_GOLD = 27                        --众神宝库
SceneConfig.SCENE_TYPE_EQUIPMENT = 28                   --混沌之门（圣灵血阵）
SceneConfig.SCENE_TYPE_FORBIDDEN = 29                   --蛮荒禁地
SceneConfig.SCENE_TYPE_ONLINE_PVP = 30                  --巅峰竞技
SceneConfig.SCENE_TYPE_GUILD_MONSTER = 31               --公会神兽
SceneConfig.SCENE_TYPE_CLOUD_PEAK = 32                  --青云之巅
SceneConfig.SCENE_TYPE_GUARD_GODDNESS = 33              --守卫女神
SceneConfig.SCENE_TYPE_GUILD_BANQUET = 34               --公会篝火
SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND = 35          --神兽岛
SceneConfig.SCENE_TYPE_GUILD_CONQUEST = 36              --公会争霸
SceneConfig.SCENE_TYPE_ANSWER = 37                      --答题场景
SceneConfig.SCENE_TYPE_TOWER_DEFENSE = 38               --塔防副本
SceneConfig.SCENE_TYPE_WEDDING = 39                     --婚礼场景
SceneConfig.SCENE_TYPE_MARRY_INSATNCE = 40              --结婚副本
SceneConfig.SCENE_TYPE_BATTLE_SOUL = 41              	--战场之魂
SceneConfig.SCENE_TYPE_WORLD_BOSS_FAKE = 42             --支线世界boss 
SceneConfig.SCENE_TYPE_ANCIENT_BATTLE   = 43           --上古战场

SceneConfig.INSTANCE_CHECK_KIND_CLIENT = 1              --纯客户端本
SceneConfig.INSTANCE_CHECK_KIND_SERVER = 0              --服务器本

SceneConfig.NEW_PLAYER_SCENE = 100013              		--新手场景

SceneConfig.IsBossScene = function(sceneType)
    if sceneType == SceneConfig.SCENE_TYPE_WORLD_BOSS or sceneType == SceneConfig.SCENE_TYPE_BOSS_HOME
        or sceneType == SceneConfig.SCENE_TYPE_FORBIDDEN or sceneType == SceneConfig.SCENE_TYPE_GOD_MONSTER_ISLAND then
        return true
    end
    return false
end

return SceneConfig
