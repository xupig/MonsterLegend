SkillStickState = {}

local LuaGameState = GameWorld.LuaGameState

SkillStickState.skillStickDirection = Vector2.zero
SkillStickState.skillStickStrength = 0
SkillStickState.skillStickIsDragging = false
SkillStickState.skillStickFingerId = 0

function SkillStickState:SetSkillStickDirection(direction)
    self.skillStickDirection = direction
    LuaGameState.skillStickDirection = direction
end

function SkillStickState:SetSkillStickStrength(strength)
    self.skillStickStrength = strength
    LuaGameState.skillStickStrength = strength
end

function SkillStickState:SetSkillStickIsDragging(isDraging)
    self.skillStickIsDragging = isDraging
    LuaGameState.skillStickIsDragging = isDraging
end

function SkillStickState:SetSkillStickFingerId(fingerId)
    self.skillStickFingerId = fingerId
    LuaGameState.skillStickFingerId = fingerId
end   

return SkillStickState