-- CacheConfig.lua
CacheConfig = {}

CacheConfig.LocalSetting = 101
CacheConfig.LoginSetting = 103
CacheConfig.ChatFriendData = 230 -- 好友聊天数据 
CacheConfig.RecentListData = 235 -- 好友系统-最近联系人列表
CacheConfig.TeamListData = 236   -- 好友系统-最近组队列表
CacheConfig.PlayerSettingData = 237   --存储游戏设置
CacheConfig.LoginServerData = 238 --最近登录服务器数据
CacheConfig.SystemBaseSettingData = 300   --系统基础设置信息
CacheConfig.AutoGameSettingData = 301   --系统挂机设置信息
return CacheConfig