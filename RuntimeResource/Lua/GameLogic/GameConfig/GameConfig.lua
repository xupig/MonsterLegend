-- GameConfig.lua
-- 这里提供统一接口，为逻辑层提供大部分config的获取
GameConfig = {}
GameConfig._cfgs = {}

local mt={		   
			  __index = function(t, k)
                    local mgr = t._cfgs[k]
                    if (mgr == nil) then
                        mgr = require("GameConfig."..k) 
                        t._cfgs[k] = mgr
                    end 
					return mgr
			   end
			  }
setmetatable(GameConfig, mt)

