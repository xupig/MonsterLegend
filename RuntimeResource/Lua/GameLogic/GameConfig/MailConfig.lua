--MailConfig.lua
MailConfig = {}
--邮件状态
MailConfig.MAIL_STATUS = {
    [1] = 3001,--未读
    [2] = 3002,--已读
    [3] = 3003,--未领取
    [4] = 3004,--已领取
}

MailConfig.MAX_MAILS = 50

return MailConfig