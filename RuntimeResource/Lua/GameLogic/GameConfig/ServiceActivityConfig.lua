-- ServiceActivityConfig.lua
local ServiceActivityConfig = {}

---- 跳转类型
ServiceActivityConfig.FOLLOW_OPEN_PANEL = 1                   -- 打开界面，填功能ID
ServiceActivityConfig.FOLLOW_EVENT = 2                        -- 事件,填事件名称
ServiceActivityConfig.FOLLOW_GUI_Open = 3                        -- 打开界面，通过GUIManager直接打开

--日常奖励状态
ServiceActivityConfig.None = 1                                --未到达可领取状态
ServiceActivityConfig.CAN_GET = 2                             --可领取
ServiceActivityConfig.ALREADY_GET = 3                         --已经领取

--活动类型
ServiceActivityConfig.Must = 1                                --每日必做
ServiceActivityConfig.Limit = 2                               --显示活动


--资源找回类型
ServiceActivityConfig.REWARD_BACK_TYPE_EX = 0                 --钻石找回
ServiceActivityConfig.REWARD_BACK_TYPE_COMMON = 1             --金币找回

ServiceActivityConfig.WEEK_TEXT_DIC = {
    [1] = 902,--一
    [2] = 903,--二
    [3] = 904,--三
    [4] = 905,--四
    [5] = 906,--五
    [6] = 907,--六
    [7] = 908 --日
}

return ServiceActivityConfig
