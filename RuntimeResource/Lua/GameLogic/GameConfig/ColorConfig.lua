-- ColorConfig.lua
ColorConfig = {}

ColorConfig.A = "#ffffff"
ColorConfig.B = "#9bb4d7"
ColorConfig.C = "#828385"
ColorConfig.D = "#301e17"
ColorConfig.E = "#ffee44"
ColorConfig.F = "#ffaa22"
ColorConfig.G = "#ff6633"
ColorConfig.H = "#ff3232"
ColorConfig.I = "#eae4c2"
ColorConfig.J = "#64dc23"
ColorConfig.K = "#33bbff"
ColorConfig.L = "#ff55ff"
ColorConfig.M = "#e9c06c"
ColorConfig.N = "#c8f5ff"
ColorConfig.O = "#22ddbb"
ColorConfig.P = "#ff7766"
ColorConfig.Q = "#961e1e"
ColorConfig.R = "#ffc8c8"

return ColorConfig