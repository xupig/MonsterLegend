LockStickState = {}

local LuaGameState = GameWorld.LuaGameState

LockStickState.lockStickDirection = Vector2.zero
LockStickState.lockStickStrength = 0
LockStickState.lockStickIsDragging = false
LockStickState.lockStickFingerId = 0

function LockStickState:SetLockStickDirection(direction)
    self.skillStickDirection = direction
    LuaGameState.lockStickDirection = direction
end

function LockStickState:SetLockStickStrength(strength)
    self.skillStickStrength = strength
    LuaGameState.lockStickStrength = strength
end

function LockStickState:SetLockStickIsDragging(isDraging)
    self.skillStickIsDragging = isDraging
    LuaGameState.lockStickIsDragging = isDraging
end

function LockStickState:SetLockStickFingerId(fingerId)
    self.skillStickFingerId = fingerId
    LuaGameState.lockStickFingerId = fingerId
end   

return LockStickState