-- StateConfig.lua
StateConfig = {}
--Avatar实体的状态定义，在cell上的状态不能大于31，在base上的状态不能超过61
--Avatar技能状态

--死亡状态
StateConfig.AVATAR_STATE_DEATH            = 0           

--///                              1           --不可操作，客户端已占用

--时空裂隙状态
StateConfig.AVATAR_STATE_SPACETIME_RIFT   = 2           

--不可移动
StateConfig.AVATAR_STATE_NOT_MOVE         = 11          

--不可转向
StateConfig.AVATAR_STATE_NOT_TURN         = 12          

--无碰撞盒
StateConfig.AVATAR_STATE_NOT_COLLIDE      = 13          

--飞行状态
StateConfig.AVATAR_STATE_FLY              = 28 

--滑翔状态
StateConfig.AVATAR_STATE_GLIDE            = 29 

--隐身状态  机关怪隐身
StateConfig.AVATAR_STATE_INVISIBLE        = 15          

--电梯状态
StateConfig.AVATAR_STATE_IN_LIFT          = 16          

--///                              17          --客户端已用 --cg怪隐身

--停掉ai,播放cg的时候，所有实体加这个buff
StateConfig.AVATAR_STATE_STOP_AI          = 18          

--不可受击
StateConfig.AVATAR_STATE_NOT_ATTACK       = 19          

--不可视
StateConfig.AVATAR_STATE_HIDE             = 20          

--不可视,不可受击
StateConfig.AVATAR_STATE_DRIVER_HIDE      = 21          

--决斗场可下注效果（头顶按钮）
StateConfig.AVATAR_STATE_DUEL_BET         = 22          

--变身状态
StateConfig.AVATAR_STATE_TRANSFORM       = 23          

--跟随状态
StateConfig.AVATAR_STATE_CHASE            = 24          

--战斗状态
StateConfig.AVATAR_STATE_FIGHTING         = 25          

--死亡灵魂状态 死亡且复活次数用完 组队本生效(关卡表team_type = 2)
StateConfig.AVATAR_STATE_DEAD_GHOST       = 26          

--队伍跟随状态
StateConfig.AVATAR_STATE_TEAM_FOLLOW      = 29          

--组队状态
StateConfig.AVATAR_STATE_TEAMING          = 30          

--是否队长
StateConfig.AVATAR_STATE_TEAM_LEADER      = 31          

--技能状态结束。注意这个放技能状态最后
StateConfig.AVATAR_STATE_SPELL_MAX        = 32          

--Avatar base上的状态定义
--传送状态
StateConfig.AVATAR_STATE_IN_TELEPORT      = 32           

--进入某个场景
StateConfig.AVATAR_STATE_ENTERING_SPACE   = 33           

--正在销毁
StateConfig.AVATAR_STATE_DESTROYING       = 34           

--正在创建cell
StateConfig.AVATAR_STATE_CREATING_CELL    = 35           

--在打单机副本
StateConfig.AVATAR_STATE_CONSOLE          = 36           

--处于跨服状态
StateConfig.AVATAR_STATE_CROSSING         = 37           

--玩家正从跨服回到本服
StateConfig.AVATAR_STATE_CROSS_TO_ORIGIN  = 38           

--玩家的战斗属性是否已初始化
StateConfig.AVATAR_STATE_BA_INITED        = 39           

--正在向跨服服务器申请副本
StateConfig.AVATAR_STATE_APPLY_CROSS_INST = 40           
    
return StateConfig
