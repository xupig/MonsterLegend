-- LocalSetting.lua
LocalSetting = {}

LocalSetting.settingData = {}

local CacheConfig = GameConfig.CacheConfig

function LocalSetting:Init()
    self:Load()   
end

function LocalSetting:InitSettingData()
    self.settingData = {}
    self.settingData.UserAccount = ""
    self.settingData.SelectedServerID = 0
    self.settingData.SelectedCharacter = "";
    self.settingData.SelectedAvatarDbid = 0;
    self.settingData.CreateRoleServerList = 0;
    self.settingData.ServerIp = ""
    self.settingData.ServerPort = 0
    self.settingData.NoticeMd5 = ""
    self.settingData.FirstChangePKMode = 0
end

function LocalSetting:Load()
    local data = GameWorld.ReadCache(CacheConfig.LocalSetting, false)
    if data == nil then
        self:InitSettingData()
        self:Save()
    else
        self.settingData = GameWorld.json.decode(data[0])
    end
end

function LocalSetting:Save()
    local dataStr = GameWorld.json.encode(self.settingData);
    GameWorld.SaveCache(CacheConfig.LocalSetting, {dataStr}, false)
end

return LocalSetting