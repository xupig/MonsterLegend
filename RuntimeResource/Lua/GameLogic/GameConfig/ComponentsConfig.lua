-- ComponentsConfig.lua
ComponentsConfig = {}

ComponentsConfig.Component = "Component"
ComponentsConfig.PointableComponent = "PointableComponent"
ComponentsConfig.DragableComponent = "DragableComponent"
ComponentsConfig.PointableDragableComponent = "PointableDragableComponent"

ComponentsConfig.Panel = "Panel"
ComponentsConfig.List = "List"
ComponentsConfig.ComplexList = "ComplexList"
ComponentsConfig.PageableList = "PageableList"
ComponentsConfig.ScrollView = "ScrollView"
ComponentsConfig.ScrollPage = "ScrollPage"
ComponentsConfig.Toggle = "Toggle"
ComponentsConfig.ToggleGroup = "ToggleGroup"
ComponentsConfig.ProgressBar = "ProgressBar"
ComponentsConfig.InputField = "InputField"
ComponentsConfig.ChatEmojiItem = "ChatEmojiItem"
ComponentsConfig.ChatTextBlock = "ChatTextBlock"
ComponentsConfig.NavigationMenu = "NavigationMenu"
ComponentsConfig.VoiceDriver = 'VoiceDriver'
ComponentsConfig.LinkTextMesh = 'LinkTextMesh'
ComponentsConfig.InputFieldMesh = 'InputFieldMesh'
ComponentsConfig.ScaleProgressBar = "ScaleProgressBar"
ComponentsConfig.Dial = "Dial"
ComponentsConfig.MultipleProgressBar = "MultipleProgressBar"
ComponentsConfig.UIGridLayoutGroup = "LuaUIGridLayoutGroup"
ComponentsConfig.Slider = "LuaUISlider"
ComponentsConfig.Roller = "Roller"

ComponentsConfig.Particle = "Particle"

return ComponentsConfig