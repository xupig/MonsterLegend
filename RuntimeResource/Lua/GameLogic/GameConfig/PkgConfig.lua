-- PkgConfig.lua
PkgConfig = {}

PkgConfig.BAG_TYPE_PKG_ITEMS = "pkg_items"
PkgConfig.WAREHOUSE_TYPE_PKG_ITEMS = "pkg_warehouse"
PkgConfig.LOADED_EQUIP_TYPE_PKG_ITEMS = "pkg_loaded_equip"
PkgConfig.SOLD_TMP_TYPE_PKG_ITEMS = "pkg_sold_tmp"
PkgConfig.BAG_TYPE_PKG_FASHION = "pkg_fashion"
PkgConfig.LOADED_FASHION_TYPE_PKG_FASHION = "pkg_loaded_fashion"
PkgConfig.BAG_TYPE_PKG_RIDE = "pkg_ride"
PkgConfig.BAG_TYPE_PKG_GOD_MOSNTER = "pkg_god_monster"
PkgConfig.BAG_TYPE_PKG_LOTTERY_EQUIP = "pkg_lottery_equip"
PkgConfig.BAG_TYPE_PKG_CARD_PIECE = "pkg_card_piece"

return PkgConfig