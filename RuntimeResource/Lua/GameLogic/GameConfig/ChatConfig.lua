--ChatConfig.lua
ChatConfig = {}

local PanelsConfig = GameConfig.PanelsConfig
local public_config = GameWorld.public_config
-- ============================公告图标===========================================
ChatConfig.ChannelIcon = {
    [public_config.CHAT_CHANNEL_SYSTEM] = 13505,
    [public_config.CHAT_CHANNEL_WORLD] = 13504,
    [public_config.CHAT_CHANNEL_VICINITY] = 13501,
    [public_config.CHAT_CHANNEL_SEEK_TEAM] = 13506,
    [public_config.CHAT_CHANNEL_TEAM] = 13500,
    [public_config.CHAT_CHANNEL_GUILD] = 13502,
    [public_config.CHAT_CHANNEL_CROSS_WORLD] = 13503
}

-- =======================表情框底图 坐标  1世界聊天 2 快捷聊天  3 好友聊天====================================================
ChatConfig.EmojiBGPositionMaps = {
    [1] = {x = -556, y = -276},
    [2] = {x = -556, y = -94},
    [3] = {x = 46, y = -240},
}

-- =============================表情框表情 坐标  1世界聊天 2 快捷聊天  3 好友聊天======================================================================
ChatConfig.EmojiPositionMaps = {
    [1] = {x = -640, y = -360},
    [2] = {x = -640, y = -178},
    [3] = {x = -36, y = -324},
}

-- ============================聊天框 动画 坐标==============================================
ChatConfig.AnimationStartPos = {
    x = -510,
    y = 0,
}

ChatConfig.AnimationEndPos = {
    x = 0,
    y = 0,
}

-- ============================语音提示===============================================
ChatConfig.Voice = 1
ChatConfig.CancelVoice = 2

ChatConfig.VoiceTipsMap = {
    [ChatConfig.Voice] = 229,
    [ChatConfig.CancelVoice] = 230,
}

ChatConfig.VoicTipsPosMap = {
    [PanelsConfig.Chat] = {x = 0,y = 0},
    [PanelsConfig.ChatSmall] = {x = 250,y = 0},
    [PanelsConfig.Friend] = {x = 423,y = 0},
}

-- =============================语音回调错误=====================================================
ChatConfig.VoiceErrorMap = {
    ['1'] = 16007,-- 语音文件不存在
}

-- ===============================链接类型================================================================
ChatConfig.LinkTypesMap = {
    Team = '1',
    Item = '2',
    Coordinate = '3',
    PlayerName = '4',
    WorldBoss = '5',
    BossHome = '6',
    BackWoods = '7',
    GodMonsterIsland = '8',
    AssistBattle = '9',
    Trade = '10',
    ItemSys = '11',
    WithGirl = '12'
}

return ChatConfig