--SettingType.lua
-- 定义游戏设置的一些宏
SettingType = {}

--设置定义
SettingType.DAILY_TIPS = "0"                        --每日不再提醒

-- SettingType.MUSIC	= "0"			                -- 音乐
-- SettingType.SOUND 	= "1" 		                -- 音效
-- SettingType.SPEECH	    = "2" 		            -- 语音
-- SettingType.RENDER_QUALITY	= "3" 		        -- 画质
-- SettingType.CAMERA_MODE	= "4" 		            -- 视角模式
-- SettingType.AUTO_FIGHT = "5"                      -- 自动战斗  --废弃
-- SettingType.INSTANCE_BATTLEARRAY = "6"            -- 副本临时阵容数据 
-- SettingType.AUTO_FIGHT_INT = "7"                      -- 自动战斗整型

--每日不再提醒类型定义
SettingType.CIRCLE_TASK_FAST_COMPLETE         = "1"           --赏金任务立即完成
SettingType.WEEK_TASK_FAST_COMPLETE           = "2"           --周常任务立即完成
SettingType.WEEK_COMMIT_EQUIP_FAST_COMPLETE   = "3"           --赏金任务、周常任务立即完成
SettingType.CARD_LUCK_NEED                    = "4"           --幸运翻牌
SettingType.HAPPY_WISH_NEED                   = "5"           --欢乐许愿
SettingType.IDENTIFY_NEED                     = "6"           --全民鉴宝
SettingType.FIREWORK						  = "7"           --烟花庆典
	
--画质定义
SettingType.QUALITY_LOW	= 2 		    -- 画质流畅
SettingType.QUALITY_MIDDLE	= 1 		-- 画质中等
SettingType.QUALITY_HIGH	= 0 		-- 画质精细

SettingType.CAMERA_MODE_3D	= 3 		-- 3D模式视角
SettingType.CAMERA_MODE_2D	= 2 		-- 2.5D模式视角


SettingType.AUTO_TYPE_MANUAL = 0 -- 手动
SettingType.AUTO_TYPE_AUTO = 1  -- 全自动
SettingType.AUTO_TYPE_SEMIAUTO = 2  -- 半自动

SettingType.AUTO_TYPE_DEFAULT_ON = SettingType.AUTO_TYPE_AUTO
SettingType.AUTO_TYPE_MIN = SettingType.AUTO_TYPE_MANUAL
SettingType.AUTO_TYPE_MAX = SettingType.AUTO_TYPE_SEMIAUTO


return SettingType