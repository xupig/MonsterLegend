local AnswerConfig = {}

AnswerConfig.SHOW_EXP_SCORE = 1             --显示经验和积分
AnswerConfig.SHOW_BEGIN_COUNT_DOWN = 2      --显示距离答题开始的倒计时
AnswerConfig.SHOW_ANSWER_COUNT_DOWN = 3     --显示答题倒计时
AnswerConfig.SHOW_END_COUNT_DOWN = 4        --显示答题结束倒计时
AnswerConfig.SHOW_ANSWER_RESULT = 5         --显示答题是否正确
AnswerConfig.SHOW_ANSWER_INFO = 6           --显示答题相关信息（题目、本题答题剩余时间）
AnswerConfig.SHOW_ANSWER_RANK = 7           --显示答题排行榜
AnswerConfig.INSTANCE_TICK_TIMER = 8        --副本时间计时器


return AnswerConfig