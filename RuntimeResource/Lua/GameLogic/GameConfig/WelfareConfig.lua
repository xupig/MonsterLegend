-- WelfareConfig.lua
WelfareConfig = {}

local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

WelfareConfig.REWARD_STATE = {
    noReward = 0, --不可领取
    canReward = 1, -- 可领取
    rewarded = 2, -- 已领取
}

-- 越小排序越靠前
WelfareConfig.REWARD_STATE_WEIGHT = {
    [1] = 1, 
    [0] = 2, 
    [2] = 3, 
}

WelfareConfig.WEEKDAY_REWARD_ID = 1 --灵玉

WelfareConfig.WEEKDAY_REWARD_NUM = {
   [3] = GlobalParamsHelper.GetParamValue(326), -- weekday_reward_3_i  连续3天签到的额外奖励
   [2] = GlobalParamsHelper.GetParamValue(327), -- weekday_reward_2_i  连续2天签到的额外奖励
}

WelfareConfig.SIGN_NPC_LIST = GlobalParamsHelper.GetParamValue(328) --随机npc图
WelfareConfig.SIGN_LUNAR_DAYS = GameDataHelper.DataParseHelper.ParseNSuffix(GlobalParamsHelper.GetParamValue(329)) --农历年={每个月份天数,...}
-- WelfareConfig.SIGN_LUNAR_DAYS = {[2017]={29,30,29,30,29,59,29,30,29,30,30,30},
-- 		[2018]={29,30,29,30,29,29,30,29,30,29,30,30}}


return WelfareConfig