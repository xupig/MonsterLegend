-- LocalServerConfig.lua
local LocalServerConfig = {}

LocalServerConfig.isClose     = "1" -- 是关服 标志
LocalServerConfig.isNew       = "2" -- 是新服 标志
LocalServerConfig.isBusy      = "3" -- 是繁忙 标志
LocalServerConfig.isRecommend = "4" -- 是推荐服务器标志

return LocalServerConfig