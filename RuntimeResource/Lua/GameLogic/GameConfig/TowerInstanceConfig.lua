local TowerInstanceConfig = {}

TowerInstanceConfig.SHOW_INFO = 1                       --显示副本信息
TowerInstanceConfig.SHOW_BNUILD_TOWERT_LIST = 2         --显示建塔列表
TowerInstanceConfig.HIDE_BNUILD_TOWERT_LIST = 3         --隐藏建塔列表
TowerInstanceConfig.UPDATE_TOWERT_LIST = 4              --更新建塔数量信息


TowerInstanceConfig.TOWER_LIMIT_NUM = 1
TowerInstanceConfig.TOWER_NOW_BUILD_NUM = 2


TowerInstanceConfig.LIMIT_NUM = -1

return TowerInstanceConfig