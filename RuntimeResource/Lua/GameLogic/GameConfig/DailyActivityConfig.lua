-- DailyActivityConfig.lua
local DailyActivityConfig = {}

DailyActivityConfig.DAILY_VITALITY = 1   --日活跃
DailyActivityConfig.WEEKLY_VITALITY = 2   --周活跃

DailyActivityConfig.VITALITY_ITEM_ID = 6   --活跃度道具ID

DailyActivityConfig.REWARD_NONE = 1         --未达到领取条件
DailyActivityConfig.REWARD_CAN_GET = 2      --可以领取奖励但未领取
DailyActivityConfig.REWARD_GET = 3          --奖励已经领取


DailyActivityConfig.NOT_OPEN = 1            --未开启
DailyActivityConfig.OPEN_SOON = 2           --即将开始
DailyActivityConfig.OPEN = 3                --开启中

DailyActivityConfig.IS_OPEN_IN_OPENTIME = 1     --在开启时间内，此时是否能进入

DailyActivityConfig.GetLeftTimes = "GetLeftTimes"   --获取剩余次数

return DailyActivityConfig
