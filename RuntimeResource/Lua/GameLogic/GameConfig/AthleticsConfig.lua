-- AthleticsConfig.lua
local AthleticsConfig = {}
AthleticsConfig.funids = {}
AthleticsConfig.indexs = {}
-- 日常索引：
    -- 竞技离线：10
    -- 天梯赛：16
    -- 天堂之巅：17
    -- 地狱战场：18
    -- 神魔战场：23
    -- 巅峰对决：19
AthleticsConfig.indexs[1]=10
AthleticsConfig.indexs[2]=16
AthleticsConfig.indexs[3]=17
AthleticsConfig.indexs[4]=18
AthleticsConfig.indexs[5]=23
AthleticsConfig.indexs[6]=19
-- 功能开启id
    -- 竞技离线：500
    -- 天梯赛：501
    -- 天堂之巅：502
    -- 地狱战场：503
    -- 神魔战场：504
    -- 巅峰对决：505
AthleticsConfig.funids[1] = 500
AthleticsConfig.funids[2] = 501
AthleticsConfig.funids[3] = 502
AthleticsConfig.funids[4] = 503
AthleticsConfig.funids[5] = 504
AthleticsConfig.funids[6] = 505

-- 竞技离线：1
-- 天梯赛：2
-- 天堂之巅：3
-- 地狱战场：4
-- 神魔战场：5
-- 巅峰对决：6
AthleticsConfig.Aion=1
AthleticsConfig.Heaven=2
AthleticsConfig.Peak=3
AthleticsConfig.Hell=4
AthleticsConfig.Dark=5
AthleticsConfig.Expect=6

return AthleticsConfig
