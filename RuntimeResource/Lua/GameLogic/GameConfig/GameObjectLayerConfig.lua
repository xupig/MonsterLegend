-- GameObjectLayerConfig.lua
GameObjectLayerConfig = {}

GameObjectLayerConfig.Avatar = "Avatar"
GameObjectLayerConfig.Monster = "Monster"
GameObjectLayerConfig.NPC = "NPC"
GameObjectLayerConfig.Ghost = "Ghost"
GameObjectLayerConfig.Dummy = "Dummy"
GameObjectLayerConfig.Pet = "Pet"
GameObjectLayerConfig.Terrain = "Terrain"
GameObjectLayerConfig.Default = "Default"
GameObjectLayerConfig.Actor = "Actor"
GameObjectLayerConfig.ThrowObject = "ThrowObject"
GameObjectLayerConfig.Trigger = "Trigger"
GameObjectLayerConfig.ShowAvatar = "ShowAvatar"
GameObjectLayerConfig.Pet = "Pet"
GameObjectLayerConfig.UI = "UI"

return GameObjectLayerConfig
