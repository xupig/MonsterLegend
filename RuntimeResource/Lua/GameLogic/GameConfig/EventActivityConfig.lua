-- EventActivityConfig.lua
local EventActivityConfig = {}

EventActivityConfig.INFO = 1   --显示信息
EventActivityConfig.SELECTION = 2   --事件选择
EventActivityConfig.DOING = 3  --事件进行中

return EventActivityConfig
