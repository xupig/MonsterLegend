-- VocationChangeConfig.lua
local VocationChangeConfig = {}
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

VocationChangeConfig.TITLE_NAME_MAP = {
    [0] = 73029,
    [1] = 73030,
    [2] = 73031
}

VocationChangeConfig.VOCATION_STATE_COMPLETE = 1        --已经完成的任务步骤
VocationChangeConfig.VOCATION_STATE_DOING = 2           --正在做的任务步骤
VocationChangeConfig.VOCATION_STATE_NONE = 3            --未完成前置任务步骤的步骤

VocationChangeConfig.STAGE_CHINESE_MAP = {
    [1] = LanguageDataHelper.CreateContent(73089),
    [2] = LanguageDataHelper.CreateContent(73090),
    [3] = LanguageDataHelper.CreateContent(73091),
    [4] = LanguageDataHelper.CreateContent(73092),
    [5] = LanguageDataHelper.CreateContent(73093),
    [6] = LanguageDataHelper.CreateContent(73094)
}

return VocationChangeConfig
