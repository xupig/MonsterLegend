--用来要监听按下和抬起逻辑的按钮
local PointableButton = Class.PointableButton(ClassTypes.BaseComponent)

PointableButton.interface = GameConfig.ComponentsConfig.PointableComponent
PointableButton.classPath = "UIComponent.Extend.PointableButton"

function PointableButton:Awake()

end

function PointableButton:OnDestroy()

end

function PointableButton:OnPointerDown(pointerEventData)
    if self._onPointerDownFunc ~= nil then
        self._onPointerDownFunc(pointerEventData)
    end
end

function PointableButton:OnPointerUp(pointerEventData)	
    if self._onPointerUpFunc ~= nil then
        self._onPointerUpFunc(pointerEventData)
    end
end

function PointableButton:SetOnPointerDownFunc(func)
    self._onPointerDownFunc = func
end

function PointableButton:SetOnPointerUpFunc(func)
    self._onPointerUpFunc = func
end