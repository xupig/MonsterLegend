--货币组件，显示当前某X种货币
-- CurrencyCom.lua

local CurrencyCom = Class.CurrencyCom(ClassTypes.BaseLuaUIComponent)

CurrencyCom.interface = GameConfig.ComponentsConfig.Component
CurrencyCom.classPath = "UIComponent.Extend.CurrencyCom"
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local ShopType = GameConfig.EnumType.ShopType

function CurrencyCom:Awake()
	self._currencyTypes = {}
end

function CurrencyCom:OnShow()
	if self._inited == nil then
		self:SetCurrencyTypes({public_config.MONEY_TYPE_COUPONS,public_config.MONEY_TYPE_COUPONS_BIND,public_config.MONEY_TYPE_GOLD})
	end
	self:InitListener()
	self:Update()
end

function CurrencyCom:OnClose()
	self:RemoveListener()
end

--支持动态设定货币类型
--arg = {public_config.MONEY_ID1,public_config.MONEY_ID2...}
function CurrencyCom:SetCurrencyTypes(arg)
	for i=1,#arg do
		self._currencyTypes[arg[i]] = 1
	end
	self._inited = true
	self:InitCallBack()
	self:InitComps()
end

function CurrencyCom:InitComps()
	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS] then
		self._btnMoneyCoupons = self:FindChildGO("Button_MoneyCoupon")
		self._csBH:AddClick(self._btnMoneyCoupons, function() self:OnClickAddCoupons() end )
		self._txtCoupons = self:GetChildComponent("Text_MoneyCoupon","TextMeshWrapper")
		self._txtCoupons.raycastTarget = false
		self._iconContainerCoupons = self:FindChildGO("Container_IconCoupon")
		GameWorld.AddIcon(self._iconContainerCoupons, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	end

	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS_BIND] then
		self._btnMoneyGold = self:FindChildGO("Button_MoneyGold")
		self._csBH:AddClick(self._btnMoneyGold, function() self:OnClickAddGold() end )
		self._txtGold = self:GetChildComponent("Text_MoneyGold","TextMeshWrapper")
		self._txtGold.raycastTarget = false
		self._iconContainerGold = self:FindChildGO("Container_IconGold")
		GameWorld.AddIcon(self._iconContainerGold, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS_BIND))
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GOLD] then
		self._btnMoneySilver = self:FindChildGO("Button_MoneySilver")
		self._csBH:AddClick(self._btnMoneySilver, function() self:OnClickAddSilver() end )
		self._txtSilver = self:GetChildComponent("Text_MoneySilver","TextMeshWrapper")
		self._txtSilver.raycastTarget = false
		self._iconContainerSilver = self:FindChildGO("Container_IconSilver")
		GameWorld.AddIcon(self._iconContainerSilver, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GOLD))
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GUILD_CONTRIBUTE] then
		self._btnMoneyGuildContribute = self:FindChildGO("Button_MoneyGuildContribute")
		self._csBH:AddClick(self._btnMoneyGuildContribute, function() self:OnClickAddSilver() end )
		self._txtGuildContribute = self:GetChildComponent("Text_MoneyGuildContribute","TextMeshWrapper")
		self._txtGuildContribute.raycastTarget = false
		self._iconContainerGuildContribute = self:FindChildGO("Container_IconGuildContribute")
		GameWorld.AddIcon(self._iconContainerGuildContribute, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GUILD_CONTRIBUTE))
	end
	
	if self._currencyTypes[public_config.MONEY_TYPE_ATHLETICS] then
		self._btnMoneyAthletics = self:FindChildGO("Button_MoneyAthletics")
		self._csBH:AddClick(self._btnMoneyAthletics, function() self:OnClickAddAthletics() end )
		self._txtAthletics = self:GetChildComponent("Text_MoneyAthletics","TextMeshWrapper")
		self._txtAthletics.raycastTarget = false
		self._iconContainerAthletics = self:FindChildGO("Container_IconAthletics")
		GameWorld.AddIcon(self._iconContainerAthletics, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_ATHLETICS))
	end
end

function CurrencyCom:InitCallBack()
	self._callbacks = {}

	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS] then
		self._onCouponsChangeCB = function ()
			self:OnCouponsChange()
		end
		self._callbacks[public_config.MONEY_TYPE_COUPONS] = self._onCouponsChangeCB
	end

	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS_BIND] then
		self._onGoldChangeCB = function ()
			self:OnGoldChange()
		end
		self._callbacks[public_config.MONEY_TYPE_COUPONS_BIND]   = self._onGoldChangeCB
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GOLD] then
		self._onSilverChangeCB = function ()
			self:OnSilverChange()
		end
		self._callbacks[public_config.MONEY_TYPE_GOLD] = self._onSilverChangeCB
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GUILD_CONTRIBUTE] then
		self._onGuildContributeChangeCB = function ()
			self:OnGuildContributeChange()
		end
		self._callbacks[public_config.MONEY_TYPE_GUILD_CONTRIBUTE] = self._onGuildContributeChangeCB
	end

	if self._currencyTypes[public_config.MONEY_TYPE_ATHLETICS] then
		self._onAthleticsChangeCB = function ()
			self:OnAthleticsChange()
		end
		self._callbacks[public_config.MONEY_TYPE_ATHLETICS] = self._onAthleticsChangeCB
	end
end

function CurrencyCom:InitListener()
	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS] then
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._onCouponsChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS_BIND] then
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._onGoldChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GOLD] then
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._onSilverChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GUILD_CONTRIBUTE] then
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_GUILD_CONTRIBUTE, self._onGuildContributeChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_ATHLETICS] then
		GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_MONEY_ATHLETICS, self._onAthleticsChangeCB)
	end
end

function CurrencyCom:RemoveListener()
	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS] then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS, self._onCouponsChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_COUPONS_BIND] then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_COUPONS_BIND, self._onGoldChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GOLD] then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_GOLD, self._onSilverChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_GUILD_CONTRIBUTE] then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_GUILD_CONTRIBUTE, self._onGuildContributeChangeCB)
	end

	if self._currencyTypes[public_config.MONEY_TYPE_ATHLETICS] then
		GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_MONEY_ATHLETICS, self._onAthleticsChangeCB)
	end
end

function CurrencyCom:Update()
	for k,v in pairs(self._currencyTypes) do
		self._callbacks[k]()
	end
end

function CurrencyCom:OnGoldChange()
	self._txtGold.text = tostring(GameWorld.Player().money_coupons_bind)
end

function CurrencyCom:OnSilverChange()
	self._txtSilver.text = tostring(GameWorld.Player().money_gold)
end

function CurrencyCom:OnCouponsChange()
	self._txtCoupons.text = tostring(GameWorld.Player().money_coupons)
end

function CurrencyCom:OnGuildContributeChange()
	self._txtGuildContribute.text = tostring(GameWorld.Player().money_guild_contribute)
end

function CurrencyCom:OnAthleticsChange()
	self._txtAthletics.text = tostring(GameWorld.Player().money_athletics)
	
end

--点击银币
function CurrencyCom:OnClickAddSilver()
	PlayerManager.ShopManager:GetGoldToSilverInfo()
end

--点击金币
function CurrencyCom:OnClickAddGold()

end

--点击点券
function CurrencyCom:OnClickAddCoupons()
	--todo 跳转到充值界面
end

--点击帮派贡献
function CurrencyCom:OnClickAddGuildContribute()
	
end

--点击竞技点
function CurrencyCom:OnClickAddAthletics()
	
end