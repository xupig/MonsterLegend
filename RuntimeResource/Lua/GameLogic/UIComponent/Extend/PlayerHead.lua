-- PlayerHead.lua
local PlayerHead = Class.PlayerHead(ClassTypes.BaseLuaUIComponent)

PlayerHead.interface = GameConfig.ComponentsConfig.PointableComponent
PlayerHead.classPath = "UIComponent.Extend.PlayerHead"

--local TipsManager = GameManager.TipsManager
function PlayerHead:Awake()
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._textLevel = self:GetChildComponent("Text_Level", "TextMeshWrapper")
    self._headIcon = self:FindChildGO("Container_HeadIcon")
    self._tipsData = nil
    self._canClick = true
    self._headId = -1
end

--override
function PlayerHead:OnDestroy()

end

--override
function PlayerHead:OnPointerDown(pointerEventData)

end

--override
function PlayerHead:OnPointerUp(pointerEventData)
    if self._clickFun then
        self._clickFun(pointerEventData.position)
        -- LoggerHelper.Log("PlayerHead:ClickFun"..PrintTable:TableToStr(pointerEventData.position))
    end
    -- LoggerHelper.Log("PlayerHead:OnPointerUp")
--pointerEventData = Global.ChangeScreenClickPosition( pointerEventData )
--这里showtips
--local clickPos = pointerEventData.position
-- {4, {[1]=玩家数据，使用self:CreateAccountData()方法生成}, {[1]=,[2]=,[3]=}}--第三个参数为Tips显示位置
--TipsManager:ShowTips(4, {[1] = self._tipsData}, {[1] = clickPos.x, [2] = clickPos.y, [3] = 1})
end

function PlayerHead:SetPlayerHeadData(name, level, headIconId, func,gray)--, uuid, guildName,cross_dbid, guild_dbid
    self._textName.text = name
    self._textLevel.text = level
    self._headId = headIconId
    GameWorld.AddIcon(self._headIcon, self._headId,0,func ~= nil,gray)
    self._clickFun = func

-- LoggerHelper.Log('SetPlayerHeadData' .. headIconId)
--self._tipsData = TipsManager:CreateAccountData(name, uuid, guildName, level, headIconId,cross_dbid, guild_dbid)
-- LoggerHelper.Log({"=========== SetPlayerHeadData====== ", self._tipsData})
end



