-- StarCom.lua
-- 星星组件
local StarCom = Class.StarCom(ClassTypes.BaseLuaUIComponent)

StarCom.interface = GameConfig.ComponentsConfig.Component
StarCom.classPath = "UIComponent.Extend.StarCom"

function StarCom:Awake()
	self._template = self:FindChildGO("Container_Stars/Image_Star")
end

--showDark 是否显示暗色星星
function StarCom:SetStarTotal(totalNum,showDark)
	self._totalNum = totalNum
	self._showDark = showDark
	--self._items = {}
	self._itemGOs = {}
	self._itemGOs[1] = self:FindChildGO("Container_Stars/Image_Star")
    for i = 2, totalNum do
        self._itemGOs[i] = self:CopyUIGameObject("Container_Stars/Image_Star", "Container_Stars")
    end

    -- for i=1,totalNum do
    -- 	local go = self._itemGOs[i]
    -- 	local item = go:GetComponent("ImageWrapper")
    -- 	self._items[i] = item
    -- end

    
    if showDark then
    	--self._darkItems = {}
    	self._darkItemGOs = {}
    	self._darkItemGOs[1] = self:FindChildGO("Container_DarkStars/Image_DarkStar")
    	for i = 2, totalNum do
       		self._darkItemGOs[i] = self:CopyUIGameObject("Container_DarkStars/Image_DarkStar", "Container_DarkStars")
    	end

    	-- for i=1,totalNum do
	    -- 	local go = self._darkItemGOs[i]
	    -- 	local item = go:GetComponent("ImageWrapper")
	    -- 	self._darkItems[i] = item
	    -- end
    end

    
end

function StarCom:SetStar(num)
	for i=1,self._totalNum do
		if i <= num then
			self._itemGOs[i]:SetActive(true)
			-- if self._showDark then
			-- 	self._darkItemGOs[i]:SetActive(false)
			-- end
		else
			self._itemGOs[i]:SetActive(false)
			-- if self._showDark then
			-- 	self._darkItemGOs[i]:SetActive(true)
			-- end
		end
	end
	if self._nextStar then
		if num == self._totalNum or num == 0 then
			self._nextStar:SetActive(false)
		else
			self._nextStar:SetActive(true)
			local pos = self._nextStar.transform.localPosition
			self._nextStar.transform.localPosition = Vector3(self._xPadding*(num) ,pos.y,pos.z)
		end
	end
end

--显示下一个星星的预览星星图片
function StarCom:ActivateNextStar(xPadding)
	if self._nextStar == nil then
		self._xPadding = xPadding
		self._nextStar = self:FindChildGO("Image_NextStar")
		self._nextStar:SetActive(false)
	end
end
