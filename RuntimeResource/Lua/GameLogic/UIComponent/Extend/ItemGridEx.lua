-- ItemGridEx.lua
--道具组件，显示数字/背包数量，道具名字等等
local ItemGridEx = Class.ItemGridEx(ClassTypes.BaseComponent)
ItemGridEx.interface = GameConfig.ComponentsConfig.Component
ItemGridEx.classPath = "UIComponent.Extend.ItemGridEx"
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function ItemGridEx:Awake()
    self._itemManager = ItemManager()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
    self._showBagCount = true
    self._showName = true
    
    self._moneyCallbackInited = false
    self._updateCountCB = function()
        self:UpdateCountText()
    end
end

function ItemGridEx:ActivateTips()
    self._iconContainer:ActivateTipsById(true)
end

--是否显示背包数量
function ItemGridEx:SetShowBagNum(showBagCount)
    self._showBagCount = showBagCount
end

--是否显示道具名字
function ItemGridEx:SetShowName(showName)
    self._showName = showName
end

--将格子设为空
function ItemGridEx:SetEmpty()
    if self._txtName then
        self._txtName.text = ""
    end
    self._iconContainer:SetItem(nil)
    if self._textNum then
        self._textNum.text = ""
    end
    self._count = nil
end

--itemId
--count，显示的数量，一般为材料数量/奖励数量等
function ItemGridEx:UpdateData(itemId, count)
    self._itemId = itemId
    if itemId < public_config.MAX_MONEY_ID and not self._moneyCallbackInited then
        GameWorld.Player():AddPropertyListener(ItemConfig.MoneyTypeMap[self._itemId], self._updateCountCB)
        self._moneyCallbackInited = true
    end
    
    if self._showName then
        if self._txtName == nil then
            self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
        end
        self._textName.text = ItemDataHelper.GetItemNameWithColor(itemId)
    end
    
    if self._showBagCount then
        if self._textNum == nil then
            self._textNum = self:GetChildComponent("Text_Count", "TextMeshWrapper")
        end
        self._iconContainer:SetItem(itemId)
        self:UpdateCount(count)
    else
        self._iconContainer:SetItem(itemId, count)
    end
end
function ItemGridEx:SetUpdateCountCB(callback)
    self._updateCountCustomCB = callback
end

function ItemGridEx:UpdateCount(count)
    self._count = count
    self:UpdateCountText()
end

function ItemGridEx:UpdateCountText()
    if self._showBagCount and self._count then
        --货币
        if self._itemId < public_config.MAX_MONEY_ID then
            self._haveCount = GameWorld.Player()[ItemConfig.MoneyTypeMap[self._itemId]]
        --道具
        else
            self._haveCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemId)
        end
        
        local str = self._haveCount .. "/" .. self._count
        if self._haveCount < self._count then
            self._textNum.text = StringStyleUtil.GetColorStringWithColorId(str, ItemConfig.ItemForbidden)
        else
            self._textNum.text = str
        end
    else
        self._iconContainer:SetCount(self._count)
    end
    if self._itemId and self._updateCountCustomCB then
        self._updateCountCustomCB()
    end
end

--获取数量，当前背包数量
function ItemGridEx:GetCount()
    return self._count, self._haveCount
end

--获取是否足够
function ItemGridEx:IsEnough()
    return self._count <= self._haveCount
end


function ItemGridEx:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE, self._updateCountCB)
end

function ItemGridEx:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._updateCountCB)
    if self._moneyCallbackInited then
        GameWorld.Player():RemovePropertyListener(ItemConfig.MoneyTypeMap[self._itemId], self._updateCountCB)
        self._moneyCallbackInited = false
    end
end
