-- ItemGrid.lua
--道具格子类(装备，普通道具，魔石)
local ItemGrid = Class.ItemGrid(ClassTypes.BaseLuaUIComponent)

ItemGrid.interface = GameConfig.ComponentsConfig.PointableComponent
ItemGrid.classPath = "UIComponent.Extend.ItemGrid"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemConfig = GameConfig.ItemConfig
local public_config = GameWorld.public_config
local WingConfig = GameConfig.WingConfig
local TipsManager = GameManager.TipsManager
local StringStyleUtil = GameUtil.StringStyleUtil
local BaseUtil = GameUtil.BaseUtil
local XGameObjectComplexIcon = GameMain.XGameObjectComplexIcon
local flag = false

require "UIComponent.Extend.StarCom"
local StarCom = ClassTypes.StarCom

--override
function ItemGrid:Awake()
    self._itemIconObject = self:FindChildGO("Container_ItemIcon")
    self._textNum = self:GetChildComponent("Text_Num", "TextMeshWrapper")
    self._frameContainer = self:FindChildGO("Container_Frame")
    self._runeBg = self:FindChildGO("Container_RuneBg")
    self._imgBind = self:FindChildGO("Image_Bind")
    BaseUtil.SetObjectActive(self._imgBind,false)
    self._animContainer = self:FindChildGO("Container_Anim")
    --各个container激活
    self._animContainerActive = false
    --self._numBgContainer = self:FindChildGO("Container_Num_BG")
    self._runeBgActive = false
    self._imgBindActive = false
end

function ItemGrid:InitGrade()
   self._containerGrade = self:FindChildGO("Container_Grade")
   self._containerGrade:SetActive(true)
   self._gradeText1 = self:FindChildGO("Container_Grade/Container_Text1")
   self._gradeText2 = self:FindChildGO("Container_Grade/Container_Text2")
   local gradeText3 = self:FindChildGO("Container_Grade/Container_Text3")

   GameWorld.AddIcon(self._gradeText1,4080)
   GameWorld.AddIcon(gradeText3,4070)
end

--override
function ItemGrid:OnDestroy()

end

--override
function ItemGrid:OnPointerDown(pointerEventData)
    self._downPosition = pointerEventData.position
    if self._pdFunc ~= nil then
        self._pdFunc(pointerEventData)
    end
end

--override
function ItemGrid:OnPointerUp(pointerEventData)
    if not self._downPosition then
		return
	end

    if self._puFunc ~= nil then
        local position = pointerEventData.position
        if math.abs(self._downPosition.x - position.x) > 20 or 
            math.abs(self._downPosition.y - position.y) > 20 then
            return
        end
        self._puFunc(pointerEventData)
    end
end

function ItemGrid:SetPointerDownCB(func)
    self._pdFunc = func
end

function ItemGrid:SetPointerUpCB(func)
    self._puFunc = func
end

--一般道具tips
function ItemGrid:ActivateTipsByItemData(buttonArgs)
    self._puFunc = function()
        if self._itemData == nil then
            do return end
        end
        TipsManager:ShowItemTips(self._itemData,buttonArgs)
    end
end

--一般道具tips，只使用道具id作参数
function ItemGrid:ActivateTipsById()
    self._puFunc = function()
        if self._itemId == nil then
            do return end
        end
        TipsManager:ShowItemTipsById(self._itemId)
    end
end

function ItemGrid:SetValues(args)
    if args == nil then 
        self._noFrame = nil
        self._noAnimFrame = nil
        return 
    end
    self._noAnimFrame = args.noAnimFrame
    self._noFrame = args.noFrame
end

--显示套装icon
function ItemGrid:ActivateSuitIcon(suitData)
    if self._containerSuit == nil then
        self._containerSuit = self:FindChildGO("Container_SuitIcon")
        self._containerSuit:SetActive(true)
    end

    self._suitData = suitData
end

function ItemGrid:SetItemData(itemData, notShowNum , notShowStrength)
    --空格子的时候
    if itemData == nil then
        self:SetItem(nil)
        do return end
    end

    self._itemData = itemData
    if notShowNum then
        self:SetItem(itemData.cfg_id)
    else
        self:SetItem(itemData.cfg_id, itemData:GetCount())
    end

    --身上装备显示强化
    -- if itemData.cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP and 
    --     itemData.bagType == public_config.PKG_TYPE_LOADED_EQUIP and
    --     itemData.bagPos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
    --     local strengthInfo = GameWorld.Player().equip_stren_info[itemData.bagPos]
    --     if strengthInfo and strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL] then
    --         local strengthLevel = strengthInfo[public_config.EQUIP_STREN_INFO_LEVEL]
    --         self._textNum.text = StringStyleUtil.GetColorStringWithColorId("+"..strengthLevel,ItemConfig.ItemAttriValue)
    --     end
    -- end

    --需要显示套装前缀
    if self._suitData then
        if itemData.cfgData.itemType == public_config.ITEM_ITEMTYPE_EQUIP and  itemData.bagType == public_config.PKG_TYPE_LOADED_EQUIP and
            itemData.bagPos <= public_config.PKG_LOADED_EQUIP_INDEX_MAX then
            local suitLevel = self._suitData[itemData.bagPos]
            if suitLevel and suitLevel > 0 then
                BaseUtil.SetObjectActive(self._containerSuit,true,0.8)
                GameWorld.AddIcon(self._containerSuit,suitLevel+5073)
            else
                BaseUtil.SetObjectActive(self._containerSuit,false)
            end
        else
            BaseUtil.SetObjectActive(self._containerSuit,false)
        end
    else
        if self._containerSuit then
            BaseUtil.SetObjectActive(self._containerSuit,false)
        end
    end

    if itemData.cfgData.itemType == public_config.ITEM_ITEMTYPE_GOD_MONSTER then
        local strengthLevel = self._itemData:GetMythicalAnimalEquipLevel()
        if strengthLevel > 0 then
            self._textNum.text = StringStyleUtil.GetColorStringWithColorId("+"..strengthLevel,ItemConfig.ItemAttriValue)
        else
            self._textNum.text = ""
        end
    end

    local bind = itemData:BindFlag()
    if bind and bind == 1 then
        if not self._imgBindActive then
            self._imgBindActive = true
            self._imgBind:SetActive(true)
        end
        BaseUtil.SetObjectActive(self._imgBind,true,0.6)
    else
        BaseUtil.SetObjectActive(self._imgBind,false)
    end
end

function ItemGrid:SetItem(itemId, itemNum, colorId, raycastTarget, gray)
    --空格子的时候
    if itemId == nil then
        if self._noFrame == nil or self._noFrame == false then
            BaseUtil.SetObjectActive(self._frameContainer,true)
            GameWorld.AddIcon(self._frameContainer, 4101)
        else
            BaseUtil.SetObjectActive(self._frameContainer,false)
        end
        BaseUtil.SetObjectActive(self._itemIconObject,false)
        if self._dynamicItemIconObject then
            BaseUtil.SetObjectActive(self._dynamicItemIconObject,false)
        end
        self._textNum.text = ""
        BaseUtil.SetObjectActive(self._imgBind,false)
        if self._animIcon then
            self._animIcon:SetIsAnimation(false)
        end
        BaseUtil.SetObjectActive(self._animContainer,false)
        if self._starCom then
            BaseUtil.SetObjectActive(self._starCom,false)
        end
        if self._containerGrade then
            BaseUtil.SetObjectActive(self._containerGrade,false)
        end

        do return end
    else
        BaseUtil.SetObjectActive(self._itemIconObject,true)
    end

    self._itemId = itemId
    self:SetCount(itemNum)
    
    local itemCfg = ItemDataHelper.GetItem(tonumber(itemId))

    self:UpdateGrade(itemCfg)
    self:UpdateQuality(itemCfg)

    if itemCfg ~= nil then
        local itemComId = tonumber(itemCfg.icon)
        local qualityIcon = itemCfg.quality + 4100
        --有点击方法raycastTarget设为true
        if self._pdFunc or self._puFunc then
            raycastTarget = true
        end
        local iconType = ItemDataHelper.GetIconType(self._itemId)
        if iconType == 1 or iconType == nil then
            BaseUtil.SetObjectActive(self._itemIconObject,true)
            GameWorld.AddIcon(self._itemIconObject, itemComId, colorId, raycastTarget, gray)
            if self._dynamicItemIconObject then
                BaseUtil.SetObjectActive(self._dynamicItemIconObject,false)
            end
        else
            if self._dynamicItemIconObject == nil then
                self._dynamicItemIconObject = self:FindChildGO("Container_DynamicItemIcon")
                self._dynamicItemIconObject:SetActive(true)
                self._complexIcon = self._dynamicItemIconObject:AddComponent(typeof(XGameObjectComplexIcon))
            end
            BaseUtil.SetObjectActive(self._dynamicItemIconObject,true)
            BaseUtil.SetObjectActive(self._itemIconObject,false)
            self._complexIcon:AddComplexIcon(itemComId)
            self._complexIcon:StartPlay()
        end
        if self._noFrame == nil or self._noFrame == false then
            BaseUtil.SetObjectActive(self._frameContainer,true)
            GameWorld.AddIcon(self._frameContainer, qualityIcon, colorId, raycastTarget, gray)
        else
            BaseUtil.SetObjectActive(self._frameContainer,false)
        end

        --装备显示星星
        if itemCfg.itemType == public_config.ITEM_ITEMTYPE_EQUIP or itemCfg.itemType == public_config.ITEM_ITEMTYPE_GOD_MONSTER then
            if self._starCom == nil then
                self._starCom = self:AddChildLuaUIComponent("Container_Stars", StarCom)
                self._starCom:SetActive(true)
                self._starCom:SetStarTotal(3,false)  
            end
            BaseUtil.SetObjectActive(self._starCom,true)
            self._starCom:SetStar(itemCfg.star)
        else
            if self._starCom then
                BaseUtil.SetObjectActive(self._starCom,false)
            end
        end

        --符文显示底
        if itemCfg.itemType == public_config.ITEM_ITEMTYPE_ITEM and itemCfg.type == 11 then
            if not self._runeBgActive then
                self._runeBg:SetActive(true)
                self._runeBgActive = true
            end
            BaseUtil.SetObjectActive(self._runeBg,true)
            GameWorld.AddIcon(self._runeBg, ItemConfig.RuneBgMap[itemCfg.quality], colorId, raycastTarget, gray)
        else
            BaseUtil.SetObjectActive(self._runeBg,false)
        end
    end
end


function ItemGrid:UpdateGrade(itemCfg)
    --装备显示阶数
    if itemCfg.itemType == public_config.ITEM_ITEMTYPE_EQUIP then 
        if self._containerGrade == nil then
            self:InitGrade()
        end
        BaseUtil.SetObjectActive(self._containerGrade,true)
        local grade
        if self._itemData and self._itemData.bagType == public_config.PKG_TYPE_LOADED_EQUIP and
            self._itemData.bagPos == public_config.PKG_LOADED_EQUIP_INDEX_RING then
            local ringLevel = GameWorld.Player().marry_ring_level
            grade = math.max(1,math.ceil(ringLevel/10))
        else
            grade = itemCfg.grade
        end
        
        if grade > 10 then
            self._gradeText1:SetActive(true)
            self._gradeText2:SetActive(true)
            GameWorld.AddIcon(self._gradeText2,4070+grade-10)
        elseif grade < 10 then
            self._gradeText1:SetActive(false)
            self._gradeText2:SetActive(true)
            GameWorld.AddIcon(self._gradeText2,4070+grade)
        else
            self._gradeText1:SetActive(true)
            self._gradeText2:SetActive(false)
        end
    else
        if self._containerGrade then
            BaseUtil.SetObjectActive(self._containerGrade,false)
        end
    end
end

function ItemGrid:UpdateQuality(itemCfg)
    --橙色以上品质道具动态特效
    if itemCfg.quality >= ItemConfig.ItemQualityOrange and (not self._noAnimFrame) then
        local animateId = itemCfg.quality + 100
        if not self._animContainerActive then
            self._animContainerActive = true
            self._animContainer:SetActive(true) 
        end
        BaseUtil.SetObjectActive(self._animContainer,true)
        local color = 0--ItemConfig.QualityTextMap[itemCfg.quality]
        if self._animIcon then
            self._animIcon:SetIsAnimation(true)
        end
        self._animIcon = GameWorld.AddIconAnim(self._animContainer, animateId,color)
        
        GameWorld.SetIconAnimSpeed(self._animContainer, 2)
    else
        if self._animIcon then
            self._animIcon:SetIsAnimation(false)
        end
        BaseUtil.SetObjectActive(self._animContainer,false)
    end
end

--设置堆叠数量
function ItemGrid:SetCount(count)
    if count and count > 1 then
        self:CheckTextActive()
        self._textNum.text = StringStyleUtil.GetLongNumberString(count)
    else
        self._textNum.text = ""
    end
end

--设置数量文本（为了包含样式）
function ItemGrid:SetCountString(countStr)
    self:CheckTextActive()
    self._textNum.text = countStr
end

function ItemGrid:CheckTextActive()
    if self._textNumGO == nil then
        self._textNumGO = self:FindChildGO("Text_Num")
        self._textNumGO:SetActive(true)
    end
end

function ItemGrid:SetBind(isBind)
    self._imgBind:SetActive(isBind)
end

--直接设定Icon，有些地方可能会有特殊道具图标（不依赖item表icon值）
-- function ItemGrid:SetIcon(iconId)
--     if iconId then
--         GameWorld.AddIcon(self._itemIconObject, itemComId)
--     end
-- end
