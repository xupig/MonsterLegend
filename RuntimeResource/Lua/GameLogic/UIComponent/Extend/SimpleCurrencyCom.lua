--单个小货币组件
-- SimpleCurrencyCom.lua
local SimpleCurrencyCom = Class.SimpleCurrencyCom(ClassTypes.BaseLuaUIComponent)

SimpleCurrencyCom.interface = GameConfig.ComponentsConfig.Component
SimpleCurrencyCom.classPath = "UIComponent.Extend.SimpleCurrencyCom"
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig

function SimpleCurrencyCom:Awake()
end

function SimpleCurrencyCom:InitComps(moneyId)
	self._txtNum = self:GetChildComponent("Text_Num","TextMeshWrapper")
	self._txtNum.raycastTarget = false
	self._iconContainer = self:FindChildGO("Container_MoneyIcon")
	if moneyId then
		self:SetMoneyId(moneyId)
	end
end

--金钱不足的时候是否标红
function SimpleCurrencyCom:SetShowLackMoney(showLackMoney)
	self._showLackMoney = showLackMoney
end

function SimpleCurrencyCom:SetMoneyNum(num)
	if self._moneyId and self._showLackMoney then
		if num > GameWorld.Player()[ItemConfig.MoneyTypeMap[self._moneyId]] then
			self._txtNum.text = StringStyleUtil.GetColorStringWithColorId(tostring(num),ItemConfig.ItemForbidden)
			self._isLackMoney = true
		else
			self._txtNum.text = tostring(num)
			self._isLackMoney = false
		end
	else
		self._txtNum.text = tostring(num)
	end
end

function SimpleCurrencyCom:IsLackMoney()
	return self._isLackMoney
end

function SimpleCurrencyCom:SetBigMoneyNum(num)
	self._txtNum.text = StringStyleUtil.GetLongNumberString(num)
end

function SimpleCurrencyCom:SetMoneyId(moneyId)
	self._moneyId = moneyId
	GameWorld.AddIcon(self._iconContainer, ItemDataHelper.GetIcon(moneyId))
end