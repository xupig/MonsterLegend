-- PanelTabCom.lua
-- 面板功能tab组件
local PanelTabCom = Class.PanelTabCom(ClassTypes.BaseLuaUIComponent)

PanelTabCom.interface = GameConfig.ComponentsConfig.Component
PanelTabCom.classPath = "UIComponent.Extend.PanelTabCom"

function PanelTabCom:Awake()
	self._txtBg = self:GetChildComponent("Background/Label","TextMeshWrapper")
	self._txtCheckmark = self:GetChildComponent("Checkmark/Label","TextMeshWrapper")

	self._imgBg = self:FindChildGO("Background")
	self._imgCheckMark = self:FindChildGO("Checkmark")

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgRedPoint:SetActive(false)

	self._tabType = 0 --1：未开启时隐藏 2：一直显示但未开启不可打开 3：到达预览等级时显示但不可打开
	self._functionOpen = false
	self._updateRedPointFunc = function(flag) self:SetRedPointActive(flag) end
end

function PanelTabCom:OnEnable()		
	if self._redpointData ~= nil then
		self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end

function PanelTabCom:OnDisable()
	if self._redpointData then 
		self._redpointData:SetUpdateRedPointFunc(nil)
	end
end

function PanelTabCom:SetName(str)
	self._txtBg.text = str
	self._txtCheckmark.text = str
end

function PanelTabCom:SetSelected(isSelected)
	self._imgBg:SetActive(not isSelected)
	self._imgCheckMark:SetActive(isSelected)
end

function PanelTabCom:SetTabType(tabType)
	self._tabType = tabType
end

function PanelTabCom:GetTabType()
	return self._tabType
end

--功能是否已开启
function PanelTabCom:SetFunctionOpen(functionOpen)
	self._functionOpen = functionOpen
end

function PanelTabCom:GetFunctionOpen()
	return self._functionOpen
end

--是否显示红点
function PanelTabCom:SetRedPointActive(isActive)
	-- LoggerHelper.Error("PanelTabCom:SetRedPointActive(isActive) ===" .. tostring(isActive))
	self._imgRedPoint:SetActive(isActive)
end

function PanelTabCom:SetRedPointData(redpointData)
	if self._redpointData ~= nil then
		self._redpointData:SetUpdateRedPointFunc(nil)
	end

	if redpointData ~= nil then
		self._redpointData = redpointData
		self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
	end
end