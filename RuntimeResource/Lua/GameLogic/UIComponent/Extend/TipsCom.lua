-- TipsCom.lua
--自适应大小的提示说明显示
local TipsCom = Class.TipsCom(ClassTypes.BaseLuaUIComponent)

TipsCom.interface = GameConfig.ComponentsConfig.Component
TipsCom.classPath = "UIComponent.Extend.TipsCom"

--override
function TipsCom:Awake()
    self._bgButton = self:FindChildGO("Button_Bg")
    self._csBH:AddClick(self._bgButton, function() self:OnBGButtonClick() end)
    self._descText = self:GetChildComponent("Image_Bg/Text_Desc", "TextMeshWrapper")

    self._bgImage = self:GetChildComponent("Image_Bg", 'ImageWrapper')
    self._bgImage:SetContinuousDimensionDirty(true)
    self._frameImage = self:GetChildComponent("Image_Bg/Image_Frame", 'ImageWrapper')
    self._frameImage:SetContinuousDimensionDirty(true)

    if self._showText ~= nil then
        self._descText.text = self._showText
    end
end

--override
function TipsCom:OnDestroy()

end

function TipsCom:OnBGButtonClick()
    self.gameObject:SetActive(false)
end

function TipsCom:Show()
    self.gameObject:SetActive(true)
end

function TipsCom:SetText(text)
    self._showText = text
    if self._descText == nil then
        return
    end
    self._descText.text = text
end