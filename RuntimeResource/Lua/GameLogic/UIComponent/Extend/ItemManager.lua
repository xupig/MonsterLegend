-- ItemManager.lua
require "UIComponent.Extend.ItemGrid"
local ItemManager = Class.ItemManager(ClassTypes.BaseComponent)
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
ItemManager.interface = GameConfig.ComponentsConfig.Component
ItemManager.classPath = "UIComponent.Extend.ItemManager"

--override
function ItemManager:Awake()

end

--override
function ItemManager:OnDestroy() 

end

--args:
--isSuit:是否套装
--noFrame:不需要边框(上方金钱栏)
function ItemManager:GetLuaUIComponent(itemId, parent, args)
    -- local cfg = ItemDataHelper.GetItem(itemId)
    -- local itemType = cfg.itemType
    local itemType
    if itemId then
        itemType = ItemDataHelper.GetItemType(itemId)
    end
    -- 暂时不区分itemType
    local size = args and args.size
    
    
    if self._itemGo == nil then
        self._itemGo = GUIManager.AddItem(parent, 1, size)
    end
    if self._itemView == nil then
        self._itemView = UIComponentUtil.AddLuaUIComponent(self._itemGo, ItemGrid) 
        self._itemView:SetValues(args)
    end
    return self._itemView
    --self:SetItemActive(true, false, false, false)
    --self._itemView:SetValues(args)
    --self._type = ItemTipsType.NORMAL
    
end
