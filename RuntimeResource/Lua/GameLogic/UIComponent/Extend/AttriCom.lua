-- AttriCom.lua
--一般属性文本显示容器
local AttriCom = Class.AttriCom(ClassTypes.BaseComponent)
AttriCom.interface = GameConfig.ComponentsConfig.Component
AttriCom.classPath = "UIComponent.Extend.AttriCom"

function AttriCom:Awake()
	self._csBH = self:GetComponent("LuaUIComponent")
	self._txtAttriName = self:GetChildComponent("Text_AttriName",'TextMeshWrapper')
end

function AttriCom:UpdateItem(name,value,concat,addition)
	--属性名和属性值连起来显示
	if concat then
		self._txtAttriName.text = name..value
	else
		if self._txtAttriValue == nil then
			self._txtAttriValue = self:GetChildComponent("Text_AttriValue",'TextMeshWrapper')
		end
		self._txtAttriName.text = name
		self._txtAttriValue.text = value
	end

	if addition then
		if self._txtAddValue == nil then
			self._txtAddValue = self:GetChildComponent("Text_AddValue",'TextMeshWrapper')
		end
		self._txtAddValue.text = addition
	end
end

--获取组件高度
function AttriCom:GetHeight()
	return math.ceil(self._txtAttriName.preferredHeight)
end