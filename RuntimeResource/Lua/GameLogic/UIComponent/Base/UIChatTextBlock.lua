-- UIChatTextBlock.lua
local UIChatTextBlock = Class.UIChatTextBlock(ClassTypes.BaseLuaUIComponent)

UIChatTextBlock.interface = GameConfig.ComponentsConfig.ChatTextBlock
UIChatTextBlock.classPath = "UIComponent.Base.UIChatTextBlock"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIChatTextBlock.AddChatTextBlock(hostGO, childPath)
    local child = nil
    if childPath == nil then
        child = hostGO
    else
        child = UIComponentUtil.FindChild(hostGO, childPath)
    end
    local ChatEmojiItem = UIComponentUtil.AddLuaUIComponent(child, UIChatTextBlock)
    return ChatEmojiItem
end

function UIChatTextBlock:Awake()
    self._csBH = self:GetComponent('LuaUIChatTextBlock')
end

function UIChatTextBlock:OnDestroy()
    self._csBH = nil
end

function UIChatTextBlock:AddTextBlock(channel, paramstr, liststr, content)
    return self._csBH:AddTextBlock(channel, paramstr, liststr, content)
end
