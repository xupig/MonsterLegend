-- UIProgressBar.lua
-- Container_PointEnd是进度条末端的特效容器，特效要放到容器正中央
-- 使用SetProgress或SetProgressByValue方法设置进度
local UIProgressBar = Class.UIProgressBar(ClassTypes.BaseLuaUIComponent)

UIProgressBar.interface = GameConfig.ComponentsConfig.ProgressBar
UIProgressBar.classPath = "UIComponent.Base.UIProgressBar"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIProgressBar.AddProgressBar(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local progressBar = UIComponentUtil.AddLuaUIComponent(child, UIProgressBar)
    return progressBar
end

function UIProgressBar:Awake()
    self._csBH = self:GetComponent('LuaUIProgressBar')
end

function UIProgressBar:OnDestroy() 
    self._csBH = nil
end

--设置进度条类型
function UIProgressBar:SetType(type)
    
end

--设置barDelay延迟播放时间(ms)
function UIProgressBar:SetBarDelayTime(ms)
    self._csBH:SetBarDelayTime(ms)
end

--设置是否显示缓动
function UIProgressBar:ShowTweenAnimate(shouldShow)
    self._csBH:ShowTweenAnimate(shouldShow)
end

--设定progress值大于1时是否进行多次缓动
function UIProgressBar:SetMutipleTween(isMultipleTweenByProgress)
    self._csBH:SetMutipleTween(isMultipleTweenByProgress)
end

--设定progress值大于1时进行多次缓动如果progress值为整数最后一次动画之后进度是否归零
function UIProgressBar:SetIsResetToZero(isResetToZero)
    self._csBH:SetIsResetToZero(isResetToZero)
end

--进度条末端动画渐入渐出效果暂时先不用
-- function UIProgressBar:SetPointEndFadeInTime(time)
--     self._csBH:SetPointEndFadeInTime(shouldShow)
-- end

-- function UIProgressBar:SetPointEndFadeOutTime(time)
--     self._csBH:SetPointEndFadeOutTime(shouldShow)
-- end

--直接设置进度条进度百分比（即使使用TweenAnimate）
function UIProgressBar:DirectSetProgress(percent)
    self._percent = percent
    self._csBH:DirectSetProgress(percent)
end

--设置进度条进度百分比
function UIProgressBar:SetProgress(percent)
    self._percent = percent
    self._csBH:SetProgress(percent)
end

--设置当前值和最大值
function UIProgressBar:SetProgressByValue(current , max , showText)
    if max == 0 then
        current = 0
        max = 1
    end
    if max then
        self:SetProgress(current/max)
    else
        self:SetProgress(0)
    end
    if showText then
        self:SetProgressText(tostring(current).."/"..tostring(max))
    end
end

--设定动画变化所需时间
function UIProgressBar:SetTweenTime(sec)
    self._csBH:SetTweenTime(sec)
end

--设置进度条上的文本
function UIProgressBar:SetProgressText(text)
    self._csBH:SetProgressText(text)
end

--设置进度条动画播放完成回调函数
function UIProgressBar:SetTweenCompleteCB(func)
    self._onTweenCompleteCB = func
end

--播放结束执行回调(限定有进度动画的进度条使用)
function UIProgressBar:OnProgressTweenCompleted()
    if self._onTweenCompleteCB ~= nil then
        self._onTweenCompleteCB()
    end
end

--播放完一段动画之后执行的回调(限定有多次进度动画的进度条使用)
function UIProgressBar:SetNextTweenCB(func)
    self._onNextTweenCB = func
end

function UIProgressBar:OnProgressNextTween()
    if self._onNextTweenCB ~= nil then
        self._onNextTweenCB()
    end
end

--获取当前进度百分比
function UIProgressBar:GetProgress()
    if self._percent ~= nil then
        return self._percent
    else
        return 0
    end
end