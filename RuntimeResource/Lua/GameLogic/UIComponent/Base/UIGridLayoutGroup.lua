-- UIGridLayoutGroup.lua
local UIGridLayoutGroup = Class.UIGridLayoutGroup(ClassTypes.LuaBehaviour)

UIGridLayoutGroup.interface = GameConfig.ComponentsConfig.UIGridLayoutGroup
UIGridLayoutGroup.classPath = "UIComponent.Base.UIGridLayoutGroup"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIGridLayoutGroup.AddGroup(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local group = UIComponentUtil.AddLuaUIComponent(child, UIGridLayoutGroup)            
    return group
end

function UIGridLayoutGroup:Awake()

end

function UIGridLayoutGroup:OnDestroy() 

end

function UIGridLayoutGroup:SetDataList(dataList)
    if dataList ~= nil then
        self._dataList = {}
        for i, data in ipairs(dataList) do
            table.insert(self._dataList, data)
        end
        self._csBH:SetLength(#self._dataList)
    else 
        self:RemoveAll() 
    end
end 

function UIGridLayoutGroup:RemoveAll()
    return self._csBH:RemoveAll()
end

-- 是否用对象池
function UIGridLayoutGroup:UseObjectPool(value)
    self._csBH:UseObjectPool(value)
end

function UIGridLayoutGroup:GetItem(idx)
    return self._csBH:GetItem(idx)
end

function UIGridLayoutGroup:SetItemType(class)
    self._csBH.luaItemType = class.classPath
end 

function UIGridLayoutGroup:OnItemClicked(item)
    if self._onItemClickedCB ~= nil then
        self._onItemClickedCB(item:GetIndex())
    end
end

function UIGridLayoutGroup:SetItemClickedCB(func)
    self._onItemClickedCB = func
end

function UIGridLayoutGroup:GetDataByIndex(idx)
    return self._dataList[idx + 1]
end

function UIGridLayoutGroup:SetChildAlignment(childAlignment)
    self._csBH:SetChildAlignment(childAlignment)
end