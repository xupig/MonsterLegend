-- UINavigationMenuItem.lua
local UINavigationMenuItem = Class.UINavigationMenuItem(ClassTypes.BaseLuaUIComponent)

UINavigationMenuItem.interface = GameConfig.ComponentsConfig.Component
UINavigationMenuItem.classPath = "UIComponent.Base.UINavigationMenuItem"

UINavigationMenuItem.GetDataByIndex = function(idx) return nil end

function UINavigationMenuItem:Awake()
    self._csBH = self:GetComponent('LuaUINavigationMenuItem')
end

function UINavigationMenuItem:OnDestroy() 
    self._csBH = nil
end

function UINavigationMenuItem:GetFIndex()
    return self._csBH.FIndex
end

function UINavigationMenuItem:GetSIndex()
    return self._csBH.SIndex
end

function UINavigationMenuItem:__belonglist__(listObj)
    self._belongList = listObj
end

function UINavigationMenuItem:__refresh__()
	local data
	local fidex = self:GetFIndex()
	local sidex = self:GetSIndex()
	--fidex是-1的元素是一级菜单项
	if fidex == -1 then
		data = self._belongList:GetFirstLevelDataByIndex(fidex)
	else
		data = self._belongList:GetSecondLevelDataByIndex(fidex,sidex)
	end
    
    if data then
    	self:OnRefreshData(data)
    else
    	LoggerHelper.Error("UINavigationMenuItem data is nil")
    end
end

function UINavigationMenuItem:OnRefreshData(data)
   	
end

function UINavigationMenuItem:SetDataDirty()
    self._csBH:SetDataDirty()
end