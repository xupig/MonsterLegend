-- UIList.lua
local UIPageableList = Class.UIPageableList(ClassTypes.UIList)

UIPageableList.interface = GameConfig.ComponentsConfig.PageableList
UIPageableList.classPath = "UIComponent.Base.UIPageableList"

require "UIComponent.Base.UIListItem"
require "UIComponent.Base.UIScrollPage"
local UIScrollPage = ClassTypes.UIScrollPage
local UIComponentUtil = GameUtil.UIComponentUtil

function UIPageableList.AddScrollPageList(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local scrollPage = UIComponentUtil.AddLuaUIComponent(child, UIScrollPage)            
    local list = UIComponentUtil.AddChildLuaUIComponent(child, "Mask/Content", UIPageableList)
    return scrollPage, list
end

function UIPageableList:Awake()
    self._csBH = self:GetComponent('LuaUIPageableList')
end

function UIPageableList:OnDestroy() 
    self._csBH = nil
end

function UIPageableList:SetDataList(dataList)
    self:RemoveAll()
    self._dataList = dataList
    if self._dataList ~= nil then
        self._csBH:SetLength(0, #self._dataList, 0)
    end
end

function UIPageableList:SetItemClickedCB(func)
    self._onItemClickedCB = func
end

function UIPageableList:OnItemClicked(item)
    if self._onItemClickedCB ~= nil then
        self._onItemClickedCB(item:GetIndex())
    end
end

function UIPageableList:SetItemType(class)
    self._csBH.luaItemType = class.classPath
end 

function UIPageableList:SetPadding(top, right, bottom, left)
    self._csBH:SetPadding(top, right, bottom, left)
end

function UIPageableList:SetGap(topToDownGap, leftToRightGap)
    self._csBH:SetGap(topToDownGap, leftToRightGap)
end

function UIPageableList:SetDirection(direction, leftToRightCount, topToDownCount)
    self._csBH:SetDirection(direction, leftToRightCount, topToDownCount)
end

function UIPageableList:SetLength(len)
    self._csBH:SetLength(self._csBH.CurrentPage, len, 0)
end





