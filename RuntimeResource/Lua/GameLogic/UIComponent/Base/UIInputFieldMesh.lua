-- UIInputFieldMesh.lua
local UIInputFieldMesh = Class.UIInputFieldMesh(ClassTypes.BaseLuaUIComponent)

UIInputFieldMesh.interface = GameConfig.ComponentsConfig.InputFieldMesh
UIInputFieldMesh.classPath = "UIComponent.Base.UIInputFieldMesh"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIInputFieldMesh.AddInputFieldMesh(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local inputField = UIComponentUtil.AddLuaUIComponent(child, UIInputFieldMesh)        
    return inputField
end

function UIInputFieldMesh:Awake()
    self._csBH = self:GetComponent('LuaUIInputFieldMesh')
end

function UIInputFieldMesh:OnDestroy() 
    self._csBH = nil
end

function UIInputFieldMesh:SetOnEndMeshEditCB(func)
    self._onOnEndMeshEditCB = func
end

function UIInputFieldMesh:OnTextMeshEndEdit(text)
    if self._onOnEndMeshEditCB ~= nil then
        self._onOnEndMeshEditCB(text)
    end
end
