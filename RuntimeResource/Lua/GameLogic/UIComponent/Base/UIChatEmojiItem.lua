-- UIChatEmojiItem.lua
local UIChatEmojiItem = Class.UIChatEmojiItem(ClassTypes.BaseLuaUIComponent)

UIChatEmojiItem.interface = GameConfig.ComponentsConfig.ChatEmojiItem
UIChatEmojiItem.classPath = "UIComponent.Base.UIChatEmojiItem"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIChatEmojiItem.AddChatEmojiItem(hostGO, childPath)
    local child = hostGO
    if childPath ~= nil and childPath ~= "" then
        child = UIComponentUtil.FindChild(hostGO, childPath)
    end
    local ChatEmojiItem = UIComponentUtil.AddLuaUIComponent(child, UIChatEmojiItem)        
    return ChatEmojiItem
end

function UIChatEmojiItem:Awake()
    self._csBH = self:GetComponent('LuaUIChatEmojiItem')
end

function UIChatEmojiItem:OnDestroy() 
    self._csBH = nil
end

function UIChatEmojiItem:SetData(value)
    self._csBH:SetChatEmojiItem(value)
end

-- 表情是否动画 true 动画  false 不动画
function UIChatEmojiItem:ShowAnimation(value)
    -- LoggerHelper.Log(tostring(value) .. '--------------------------------')
    self._csBH:SetAnimation(value)
    -- LoggerHelper.Log(tostring(value) .. '-------7777-------------------------')
end