local BasePanel = Class.BasePanel(ClassTypes.BaseComponent)
--Panel基类

local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local PanelTabType = GameConfig.PanelsConfig.PanelTabType
local UIComponentUtil = GameUtil.UIComponentUtil
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RedPointManager = GameManager.RedPointManager

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

require "UIComponent.Base.UIParticle"
local UIParticle = ClassTypes.UIParticle

require "UIComponent.Extend.PanelTabCom"
local PanelTabCom = ClassTypes.PanelTabCom

--override
function BasePanel:Awake()
end

--override
function BasePanel:Start()
end

--override
function BasePanel:OnDestroy()
end

--for override 此处添加面板显示时的相关处理，比如添加事件处理
function BasePanel:OnShow(data)
end

--for override 此处添加面板关闭时的相关处理，比如移除事件处理
function BasePanel:OnClose()
end

--not for override 此处添加面板开启时的特殊处理
function BasePanel:OnBaseShow()
    if GameManager.GUIManager.PanelIsHideByScale(self.prefabName) and self._containerFirstTab then
        self._containerFirstTab:SetActive(true)
    end
end

--not for override 此处添加面板关闭时的选中View相关处理，比如移除事件处理
function BasePanel:OnBaseClose()
    if self._selectedView then
        self._selectedView:CloseView()
        if self:ScaleToHide() then
            self._selectedViewGO.transform.localScale = HideScale
        end
    end

    if GameManager.GUIManager.PanelIsHideByScale(self.prefabName) and self._containerFirstTab then
        self._containerFirstTab:SetActive(false)
    end
    -- if self:WinBGType() == GameConfig.PanelsConfig.FullSceenBG then
    -- end
end

function BasePanel:GetCanvas()
    if self._canvas == nil then
        self._canvas = go:GetComponent(typeof(UnityEngine.Canvas))
    end
    return self._canvas
end

function BasePanel:EnableCanvas()
    self:GetCanvas().enabled = true
end

function BasePanel:DisableCanvas()
    self:GetCanvas().enabled = false
end

function BasePanel:SetData(data)
end

--动画相关参数

function BasePanel:PositionFrom(pos, duration)
    self._positionFrom_fromPos = pos
    self._positionFrom_toPos = self.transform.localPosition
    self._positionFrom_duration = duration
    return self
end

function BasePanel:RotationFrom(rotation, duration)
    self._rotationFrom_fromRotation = rotation
    self._rotationFrom_toRoation = self.transform.localRotation
    self._rotationFrom_duration = duration
    return self
end

function BasePanel:ScaleFrom(scale, duration)
    self._scaleFrom_fromScale = scale
    self._scaleFrom_toScale = self.transform.localScale
    self._scaleFrom_duration = duration
    return self
end

function BasePanel:PositionTo(pos, duration)
    self._positionTo_fromPos = self.transform.localPosition
    self._positionTo_pos = pos
    self._positionTo_duration = duration
    return self
end

function BasePanel:RotationTo(rotation, duration)
    self._rotationTo_fromRotation = self.transform.localRotation
    self._rotationTo_roation = rotation
    self._rotationTo_duration = duration
    return self
end

function BasePanel:ScaleTo(scale, duration)
    self._scaleTo_fromScale = self.transform.localScale
    self._scaleTo_scale = scale
    self._scaleTo_duration = duration
    return self
end

-- -- 是否有背景 0 没有  1 清晰底板  2 模糊底板  3 二级面板遮罩
function BasePanel:CommonBGType()
    return PanelBGType.NoBG
end

--暂时不用
-- function BasePanel:CommonInnerBGType()
--     return PanelInnerBGType.NoBG
-- end

function BasePanel:OnAddCommonBG()
end

function BasePanel:HandlerBGEvent(BgGo)
end

--初始化背景
function BasePanel:OnAddCommonWinBG()
    local winType = self:WinBGType()
    if winType > 0 then
        --关闭按钮
        local root = "Container_WinBg/Container_win" .. winType
        self._btnClose = self:FindChildGO(root .. "/Button_Close")
        self._csBH:AddClick(
            self._btnClose,
            function()
                self:OnCloseClick()
            end
        )
        
        --标题
        if self:CustomizeTitle() == false and winType < 4 then
            local title = FunctionOpenDataHelper:GetTitleByPanelName(self.prefabName)
            if title then
                local txtTitle = self:GetChildComponent(root .. "/Text_Title", "TextMeshWrapper")
                txtTitle.text = LanguageDataHelper.CreateContent(title)
            end
        end
        --全屏UI不执行功能开启部分
        if winType == PanelWinBGType.AthleticsBG then
            return
        end
        if winType == PanelWinBGType.FullSceenBG then
            self._iconBg = self:FindChildGO(root..'/Image_Bg')
            GameWorld.AddIcon(self._iconBg,3316)
            return 
        end
        --self:InitListener()
        self:InitTabs()
    --self:UpdateFunctionOpen()
    end
end

--监听等级变化开启功能
-- function BasePanel:InitListener()
--     GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, function ()
--         self:UpdateFunctionOpen()
--     end)
-- end

--初始化标签
function BasePanel:InitTabs()
    local winType = self:WinBGType()
    local root = "Container_WinBg/Container_win" .. winType
    
    local functionDatas, secTabMaxCount = FunctionOpenDataHelper:GetFunctionOpensInPanel(self.prefabName)
    self._functionDatas = functionDatas
    self._tabCount = #functionDatas --一级标签数量
    self._secTabMaxCount = secTabMaxCount --二级标签最多数量
    self._secTabCount = 1
    self._panelRedpointData = RedPointManager:GetRedPointDataByPanelName(self.prefabName)

    --设置一级页签
    self._tabs = {}
    local tabGos = {}

    if self._tabCount > 1 then
        self._containerFirstTab = self:FindChildGO(root .. "/ToggleGroup_Function")
        self._containerFirstTab:SetActive(true)
        tabGos[1] = self:FindChildGO(root .. "/ToggleGroup_Function/Toggle")
        
        for i = 2, self._tabCount do
            local go = self:CopyUIGameObject(root .. "/ToggleGroup_Function/Toggle", root .. "/ToggleGroup_Function")
            tabGos[i] = go
        end
        for i = 1, self._tabCount do
            local go = tabGos[i]
            local item = UIComponentUtil.AddLuaUIComponent(go, PanelTabCom)
            
            -- LoggerHelper.Error("设置1级标签" .. tostring(i))
            if self._panelRedpointData then
                item:SetRedPointData(self._panelRedpointData:GetNode(i, nil))
            end
            self._csBH:AddClick(item.gameObject,function() self:OnFirstTabClick(i) end)
            local d = functionDatas[i][1]
            local tabName
            local tabType
            for k, v in pairs(d.button_func2) do
                tabName = k
                tabType = v[1]
            end
            item.index = i
            item:SetName(LanguageDataHelper.CreateContent(tabName))
            item:SetSelected(false)
            item:SetTabType(tabType)
            self._tabs[i] = item
        end

        --一级页签特效
        if winType == PanelWinBGType.NormalBG or winType == PanelWinBGType.SpecialBG then
            local tabsFx = {} --标签特效go
            tabsFx[1] = self:FindChildGO(root .. "/ToggleGroup_Function/Toggle/Checkmark/Container_Fx")
            for i = 2, self._tabCount do
                tabsFx[i] = UIComponentUtil.FindChild(tabGos[i], "Checkmark/Container_Fx")
            end
            for i = 1, self._tabCount do
                UIParticle.AddParticle(tabsFx[i], "fx_ui_30018")
            end
        end
    else
        --没有一级标签，_selectedIndex默认为1
        self._selectedIndex = 1
        -- if self._containerFirstTab ~= nil then
        --     self._containerFirstTab:SetActive(false)
        -- end
    end

    if self:CustomizeSecondTab() then
        return
    end
    --设置二级页签
    if self._secTabMaxCount > 1 then
        self._containerSecTab = self:FindChildGO(root .. "/ToggleGroup_SecTab")
        self._imgTabLine = self:FindChildGO(root .. "/Image_Line")
        -- if self._imgTabLine ~= nil then
        --     self._imgTabLine:SetActive(false)
        -- end
        self._secTabs = {}
        local secTabGos = {}
        self._containerSecTab:SetActive(true)
        secTabGos[1] = self:FindChildGO(root .. "/ToggleGroup_SecTab/Toggle")
        for i = 2, self._secTabMaxCount do
            local go = self:CopyUIGameObject(root .. "/ToggleGroup_SecTab/Toggle", root .. "/ToggleGroup_SecTab")
            secTabGos[i] = go
        end
        for i = 1, self._secTabMaxCount do
            local go = secTabGos[i]
            local item = UIComponentUtil.AddLuaUIComponent(go, PanelTabCom)
            local itemTrans = item.gameObject.transform
            itemTrans.localPosition = Vector3.New(itemTrans.localPosition.x, itemTrans.localPosition.y, -1000)
            self._csBH:AddClick(item.gameObject,function() self:OnSecTabClick(i) end)
            item:SetSelected(false)
            self._secTabs[i] = item
        end
        --只有二级标签
        if self._tabCount <= 1 then
            self:UpdateSecTab(false)
        end
    else
        -- if self._containerSecTab ~= nil then
        --     self._containerSecTab:SetActive(false)
        -- end
        --单独一个页面直接初始化
        if self._tabCount == 1 and self:StructingViewInit() then
            self:ChangeTab(self._views[1])
        end
    end
end

--一级标签点击
function BasePanel:OnFirstTabClick(index,args)
    local item = self._tabs[index]

    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_base_panel_first_tab_click_" .. index)
    if self._customizeFunctionOpenTextCB then
        --执行自定义提示
        if not item:GetFunctionOpen() then
            self._customizeFunctionOpenTextCB(index, 1)
            return
        end
    else
        if not item:GetFunctionOpen() then
            PlayerManager.FunctionOpenManager:ShowOpenText(self._functionDatas[index][1])
            return
        end
    end
    --local index = item.index
    self._selectedIndex = index
    if self._selectedTabItem then
        self._selectedTabItem:SetSelected(false)
    end
    self._selectedTabItem = item
    self._selectedTabItem:SetSelected(true)

    --先执行一级回调
    if self._tabCB then
        self._tabCB(index)
    end

    self:UpdateSecTab(true,args)

    --执行一级后置回调
    if self._tabCB2 then
        self._tabCB2(index)
    end
end

--切换tab
function BasePanel:ChangeTab(childPath,args)
    --还是在上一次关闭的页面
    if self._selectedPath and self._selectedPath == childPath then
        if self:ScaleToHide() then
            self._selectedViewGO.transform.localScale = ShowScale
        end
        self._selectedView:ShowView(args)
        return
    end
    local view = self:GetViewComponent(childPath)
    local viewGO = self:GetViewGameObject(childPath)
    if self._selectedView then
        self._selectedView:CloseView()
        if self:ScaleToHide() then
            self._selectedViewGO.transform.localScale = HideScale
        else
            self._selectedViewGO:SetActive(false)
        end
    end
    self._selectedPath = childPath
    self._selectedView = view
    self._selectedViewGO = viewGO
    if not viewGO.activeSelf then
        viewGO:SetActive(true)
    end
    if self:ScaleToHide() then
        viewGO.transform.localScale = ShowScale
    end
    view:ShowView(args)
end

function BasePanel:UpdateSecTab(autoSelect,args)
    --有二级标签处理
    local secTabDatas = self._functionDatas[self._selectedIndex]
    self._secTabCount = #secTabDatas

    --只有一级标签
    if self._secTabCount < 2 then
        if self._imgTabLine then
            self._imgTabLine:SetActive(false)
        end
        if self._containerSecTab then
            self._containerSecTab:SetActive(false)
        end

        if self:StructingViewInit() then
            local childPath = self._views[self._selectedIndex]
            self:ChangeTab(childPath,args)
        end
    --有二级标签
    else
        self._imgTabLine:SetActive(true)
        self._containerSecTab:SetActive(true)
        --更新二级标签数据(选中一级页签的时候就会处理)
        for i = 1, #self._secTabs do
            local item = self._secTabs[i]
            if i <= #secTabDatas then
                item:SetActive(true)
                local d = secTabDatas[i]
                local tabName
                local tabType
                for k, v in pairs(d.button_func1) do
                    tabName = k
                    tabType = v[1]
                end
                item.index = i
                item:SetName(LanguageDataHelper.CreateContent(tabName))
                item:SetSelected(false)
                item:SetTabType(tabType)
                -- LoggerHelper.Error("设置2级标签[" .. tostring(self._selectedIndex) .. "]:[" .. tostring(i) .. "]")
                if self._panelRedpointData then
                    item:SetRedPointData(self._panelRedpointData:GetNode(self._selectedIndex, i))
                end
                self:CheckOneTabOpen(item, d)
            else
                item:SetActive(false)
            end
        end

        --按记录的二级页签自动选择
        if autoSelect then
            local secIndex = self._selectedSecIndexes[self._selectedIndex]
            self:OnSecTabClick(secIndex,args)
        end
    end
end

--检查一个页签是否开启
function BasePanel:CheckOneTabOpen(item, data)
    local playerLevel = GameWorld.Player().level
    local tabType = item:GetTabType()
    if FunctionOpenDataHelper:CheckFunctionOpen(data.id) then
        item:SetActive(true)
        item:SetFunctionOpen(true)
    else
        --MJ包屏蔽功能
        if GameWorld.SpecialExamine and GameWorld.FunctionCloseIds[data.id] then
            item:SetActive(false)
            item:SetFunctionOpen(false)
            return
        end

        if tabType == 1 then
            item:SetActive(false)
        elseif tabType == 3 then
            local previewLevel = data.preview_level
            if previewLevel and playerLevel >= previewLevel[1] then
                item:SetActive(true)
            else
                item:SetActive(false)
            end
        else
            item:SetActive(true)
        end
        item:SetFunctionOpen(false)
    end
end

--更新功能开启状态
--一级页签
--功能面板OnShow()需要调用
function BasePanel:UpdateFunctionOpen()
    if self._tabCount > 1 then
        for i = 1, self._tabCount do
            local item = self._tabs[i]
            local d = self._functionDatas[i][1]
            self:CheckOneTabOpen(item, d)
        end
    elseif self._secTabMaxCount > 1 then
        for i = 1, self._secTabMaxCount do
            local item = self._secTabs[i]
            local d = self._functionDatas[1][i]
            self:CheckOneTabOpen(item, d)
        end
    end
end

--二级标签点击
function BasePanel:OnSecTabClick(index,args)
    --LoggerHelper.Error("OnSecTabClick"..index,true)
    local item = self._secTabs[index]
    if item == nil then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_base_panel_sec_tab_click_" .. index)
    if self._customizeFunctionOpenTextCB then
        --执行自定义提示
        if not item:GetFunctionOpen() then
            self._customizeFunctionOpenTextCB(self._selectedIndex, index)
            return
        end
    else
        if not item:GetFunctionOpen() then
            FunctionOpenManager:ShowOpenText(self._functionDatas[self._selectedIndex][index])
            return
        end
    end
    self._selectedSecIndexes[self._selectedIndex] = index
    self._selectedSecIndex = index
    if self._selectedSecTabItem then
        self._selectedSecTabItem:SetSelected(false)
    end
    self._selectedSecTabItem = self._secTabs[index]
    self._selectedSecTabItem:SetSelected(true)

    if self:StructingViewInit() then
        local childPath = self._views[self._selectedIndex][index]
        self:ChangeTab(childPath,args)
    end

    if self._secTabCB then
        self._secTabCB(index)
    end
end

--OnShow时调用,只有有标签的面板才允许调用
--data.functionId 选中的功能id，
--data.firstTabIndex 选中的一级标签
--data.secondTabIndex 选中的二级标签
function BasePanel:OnShowSelectTab(data)
    if self._secTabMaxCount > 1 then
        --记录所有需要自动选中的二级页签的索引
        self._selectedSecIndexes = {}
        for i = 1, self._tabCount do
            self._selectedSecIndexes[i] = 1
        end
    end

    if data then
        local firstTabIndex
        local secondTabIndex
        if data.functionId then
            for i = 1, #self._functionDatas do
                for j = 1, #self._functionDatas[i] do
                    if self._functionDatas[i][j].id == data.functionId then
                        firstTabIndex = i
                        if self._selectedSecIndexes then
                            secondTabIndex = j
                            self._selectedSecIndexes[firstTabIndex] = j
                        end
                    end
                end
            end
        else
            firstTabIndex = data.firstTabIndex
            if self._selectedSecIndexes then
                self._selectedSecIndexes[firstTabIndex] = data.secondTabIndex or 1
            end
        end

        --只处理一级点击，会自动执行二级页签点击
        if #self._tabs > 1 then
            if firstTabIndex then
                self:OnFirstTabClick(firstTabIndex,data.args)
            else
                self:OnFirstTabClick(self:GetFirstOpenFirstTab(),data.args)
            end
        else
            --只有二级菜单
            if secondTabIndex then
                self:OnSecTabClick(secondTabIndex,data.args)
            end
        end
    else
        if #self._tabs > 1 then
            self:OnFirstTabClick(self:GetFirstOpenFirstTab())
        elseif self._secTabMaxCount > 1 then
            self:OnSecTabClick(self:GetSecOpenFirstTab())
        end
    end
end

--第一个开启的一级页签
function BasePanel:GetFirstOpenFirstTab()
    for i = 1, #self._tabs do
        if self._tabs[i]:GetFunctionOpen() then
            return i
        end
    end
    return 1
end

--第一个开启的二级页签
function BasePanel:GetSecOpenFirstTab()
    for i = 1, #self._secTabs do
        if self._secTabs[i]:GetFunctionOpen() then
            return i
        end
    end
    return 1
end

--初始化View的方法
--childPath路径
--view的类
--fidx一级标签索引
--sidx二级标签索引
--addComponentCallback回调
function BasePanel:InitView(childPath, view, fidx, sidx, addComponentCallback)
    if self._viewMap == nil then
        self._viewMap = {}
        self._views = {}
        self._callbackMap = {}
        self._componentMap = {}
        self._gameObjectMap = {}
        self._rightTabsTrans = {}
    end

    self._viewMap[childPath] = view
    if sidx then
        if self._views[fidx] == nil then
            self._views[fidx] = {}
        end
        self._views[fidx][sidx] = childPath
    else
        self._views[fidx] = childPath
    end
    if addComponentCallback then
        self._callbackMap[childPath] = addComponentCallback
    end
end

function BasePanel:GetViewComponent(childPath)
    if not self._componentMap[childPath] then
        self._componentMap[childPath] = self:AddChildLuaUIComponent(childPath, self._viewMap[childPath])
        if self._callbackMap[childPath] then
            self._callbackMap[childPath]()
        end
    end
    return self._componentMap[childPath]
end

function BasePanel:GetViewGameObject(childPath)
    if not self._gameObjectMap[childPath] then
        local go = self:FindChildGO(childPath)
        self._gameObjectMap[childPath] = go
    -- self._rightTabsTrans[childPath]=go:GetComponent(typeof(UnityEngine.RectTransform))
    end
    return self._gameObjectMap[childPath]
end

--关闭按钮
function BasePanel:OnCloseClick()
    if self._closeCB then
        self._closeCB()
    else
        if self:ReopenSubMainPanel() and GameManager.GUIManager.reopenSubMainMenu > 0 then
            GameManager.GUIManager.reopenSubMainMenu = GameManager.GUIManager.reopenSubMainMenu - 1
            if GameManager.GUIManager.reopenSubMainMenu == 0 then
                GameManager.GUIManager.ShowPanel(PanelsConfig.SubMainUI)
            end
        end
        GameManager.GUIManager.ClosePanel(self.prefabName)
    end
    self._iconBg=nil
end

-------------------------------------------各种奇奇怪怪的需求-----------------------------------------------
--根据外部条件开启一级页签
function BasePanel:SetFirstTabActive(index, isActivated)
    local item = self._tabs[index]
    item:SetActive(isActivated)
    item:SetFunctionOpen(isActivated)
end

--根据外部条件开启二级页签
function BasePanel:SetSecTabActive(index, isActivated)
    local item = self._secTabs[index]
    item:SetActive(isActivated)
    item:SetFunctionOpen(isActivated)
end

--根据外部条件开启一级页签功能
function BasePanel:SetFirstTabFunctionActive(index, isActivated)
    local item = self._tabs[index]
    item:SetFunctionOpen(isActivated)
end

--根据外部条件直接开启二级页签功能
function BasePanel:SetSecTabFunctionActive(index, isActivated)
    local item = self._secTabs[index]
    item:SetFunctionOpen(isActivated)
end

--根据外部方法提示功能未开启
--所有功能开启方式将按回调方式进行
function BasePanel:SetCustomizeFunctionOpenTextCB(customizeCB)
    self._customizeFunctionOpenTextCB = customizeCB
end

--5号界面左侧背景控制
function BasePanel:RightBgIsActive(isActive)
    local winType = self:WinBGType()
    if winType > 0 then
        --关闭按钮
        if winType == PanelWinBGType.RedNewBG then
            local root = "Container_WinBg/Container_win" .. winType
            self._win5RightBg = self:FindChildGO(root .. "/Image_Bg_Right")
            self._win5RightBg:SetActive(isActive)
        end
    end
end
--------------------------------------for setting-------------------------------------------
--有特殊情况才需要设定，一般情况是直接关闭界面
function BasePanel:SetCloseCallBack(closeCB)
    self._closeCB = closeCB
end

--设定一级菜单点击回调
function BasePanel:SetTabClickCallBack(tabCB)
    self._tabCB = tabCB
end

--设定一级菜单点击回调（更新二级页签后执行）
function BasePanel:SetTabClickAfterUpdateSecTabCB(tabCB2)
    self._tabCB2 = tabCB2
end

--设定二级菜单点击回调
function BasePanel:SetSecondTabClickCallBack(secTabCB)
    self._secTabCB = secTabCB
end

--设定一级菜单z坐标
function BasePanel:SetTabContainerZ(value)
    local trans = self._containerFirstTab.transform
    trans.localPosition = Vector3.New(trans.localPosition.x, trans.localPosition.y, value)
end

--设定二级菜单z坐标
function BasePanel:SetSecTabContainerZ(value)
    if self._containerSecTab then
        local trans = self._containerSecTab.transform
        trans.localPosition = Vector3.New(trans.localPosition.x, trans.localPosition.y, value)
    end
end
-------------------------------------for override-------------------------------------------
-- 是否有背景 0 无底板 1 普通底板
function BasePanel:WinBGType()
    return PanelWinBGType.NoBG
end

--是否自定义二级标签(如果要自定义二级标签继承此方法并将返回值改成true)
function BasePanel:CustomizeSecondTab()
    return false
end

--是否自定义标题(如果要自定义标题继承此方法并将返回值改成true)
function BasePanel:CustomizeTitle()
    return false
end

--使用结构化View初始化方式，所有tab点击由BasePanel控制
function BasePanel:StructingViewInit()
    return false
end

--关闭的时候重新开启二级主菜单
function BasePanel:ReopenSubMainPanel()
    return false
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
function BasePanel:ScaleToHide()
    return false
end

function BasePanel:BaseShowModel()

end

function BasePanel:BaseHideModel()

end

function BasePanel:BaseChangeZ(z)
    local position = self.transform.localPosition
    self.transform.localPosition = Vector3.New(position.x, position.y, z)
end

function BasePanel:AddCutoutAdapter()
    self:AddComponent(typeof(GameMain.XCutoutAdapter))
end