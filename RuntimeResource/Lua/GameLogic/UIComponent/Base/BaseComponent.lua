local BaseComponent = Class.BaseComponent(ClassTypes.LuaBehaviour)

local UIComponentUtil = GameUtil.UIComponentUtil
BaseComponent.HIDE_POSITION = Vector3.New(-1000,-1400,0)

function BaseComponent:FindChildGO(childPath) 
	return UIComponentUtil.FindChild(self.gameObject, childPath)
end

-- 获取当前Behaviour对应GameObject上的某指定名称类型的脚本实例，
-- 等同C#中的GetComponent<typeName>()
-- 返回类型：Component的子类实例
function BaseComponent:GetComponent(compName) 
	return self.transform:GetComponent(compName)
end

-- 在当前Behaviour对应GameObject上增加对应类型的Component，
-- 等同C#中的AddComponent<typeName>()
-- 返回类型：Component的子类实例
function BaseComponent:AddComponent(comType) 
	return self.gameObject:AddComponent(comType)
end

function BaseComponent:AddClick(childPath, callback)
	local go = self:FindChildGO(childPath) 
	if go ~= nil then
		self._csBH:AddClick(go, callback)
	end
end

function BaseComponent:GetChildComponent(childPath, compName) 
	return UIComponentUtil.GetChildComponent(self.gameObject, childPath, compName)
end

function BaseComponent:AddChildComponent(childPath, class) 
	return UIComponentUtil.AddChildComponent(self.gameObject, childPath, class)
end

function BaseComponent:AddLuaUIComponent(class) 
	return self:AddGameObjectLuaUIComponent(self.gameObject, class)
end

function BaseComponent:AddChildLuaUIComponent(childPath, class) 
	return UIComponentUtil.AddChildLuaUIComponent(self.gameObject, childPath, class)
end

function BaseComponent:CopyUIGameObject(srcPath, parentPath) 
	return UIComponentUtil.CopyUIGameObject(self.gameObject, srcPath, parentPath)
end

function BaseComponent:AddScrollViewList(childPath)
    return ClassTypes.UIList.AddScrollViewList(self.gameObject, childPath)
end

function BaseComponent:AddScrollPageList(childPath)
    return ClassTypes.UIPageableList.AddScrollPageList(self.gameObject, childPath)
end

-- 把当前的transform调整到其父节点中所有子节点的最后，主要是为了调整渲染的顺序
function BaseComponent:SetAsLastSibling()
	self.transform:SetAsLastSibling() 
end

function BaseComponent:SetActive(state)
	self.gameObject:SetActive(state)
end

function BaseComponent:isActiveAndEnabled()
	return self._csBH.isActiveAndEnabled
end

function BaseComponent:ExistChild(childPath) 
	return UIComponentUtil.ExistChild(self.gameObject, childPath)
end

function BaseComponent:SetGameObjectColor(hostGO,color)
	return UIComponentUtil.SetGameObjectColor(hostGO,color)
end

function BaseComponent:AddGameObjectLuaUIComponent(hostGO, class) 
    return	UIComponentUtil.AddLuaUIComponent(hostGO, class) 
end