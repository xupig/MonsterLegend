-- UIToggleGroup.lua
local UIToggleGroup = Class.UIToggleGroup(ClassTypes.BaseLuaUIComponent)

UIToggleGroup.interface = GameConfig.ComponentsConfig.ToggleGroup
UIToggleGroup.classPath = "UIComponent.Base.UIToggleGroup"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIToggleGroup.AddToggleGroup(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local toggleGroup = UIComponentUtil.AddLuaUIComponent(child, UIToggleGroup)        
    return toggleGroup
end

function UIToggleGroup:Awake()
    self._csBH = self:GetComponent('LuaUIToggleGroup')
end

function UIToggleGroup:OnDestroy() 
    self._csBH = nil
end

function UIToggleGroup:SetOnSelectedIndexChangedCB(func)
    self._onSelectedIndexChangedCB = func
end

function UIToggleGroup:OnSelectedIndexChanged(index)
    if self._onSelectedIndexChangedCB ~= nil then
        self._onSelectedIndexChangedCB(index)
    end
end

function UIToggleGroup:SetSelectIndex(index)
    self._csBH:SetSelectIndex(index)
end

function UIToggleGroup:SetToggleVisible(index, state)
    self._csBH:SetToggleVisible(index, state)
end

function UIToggleGroup:SetRepeatClick(state)
    self._csBH:SetRepeatClick(state)
end