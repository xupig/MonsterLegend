-- UIInputField.lua
local UIInputField = Class.UIInputField(ClassTypes.BaseLuaUIComponent)

UIInputField.interface = GameConfig.ComponentsConfig.InputField
UIInputField.classPath = "UIComponent.Base.UIInputField"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIInputField.AddInputField(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local inputField = UIComponentUtil.AddLuaUIComponent(child, UIInputField)        
    return inputField
end

function UIInputField:Awake()
    self._csBH = self:GetComponent('LuaUIInputField')
end

function UIInputField:OnDestroy() 
    self._csBH = nil
end

function UIInputField:SetOnEndEditCB(func)
    self._onOnEndEditCB = func
end

function UIInputField:OnTextEndEdit(text)
    if self._onOnEndEditCB ~= nil then
        self._onOnEndEditCB(text)
    end
end
