-- UIList.lua
local UIScrollPage = Class.UIScrollPage(ClassTypes.BaseLuaUIComponent)

UIScrollPage.interface = GameConfig.ComponentsConfig.ScrollPage
UIScrollPage.classPath = "UIComponent.Base.UIScrollPage"

UIScrollPage.DirectionTopToDown = 0
UIScrollPage.DirectionLeftToRight = 1

function UIScrollPage:Awake()
    self._csBH = self:GetComponent('LuaUIScrollPage')
end

function UIScrollPage:OnDestroy() 
    self._csBH = nil
end

function UIScrollPage:SetTotalPage(page)
    self._csBH.TotalPage = page
end

function UIScrollPage:SetCurrentPage(page)
    self._csBH.CurrentPage = page
end

function UIScrollPage:GetCurrentPage()
    return self._csBH.CurrentPage
end

function UIScrollPage:SetOnCurrentPageChangedCB(func)
    self._onCurrentPageChangedCB = func
end

function UIScrollPage:OnCurrentPageChanged(page)
    if self._onCurrentPageChangedCB ~= nil then
        self._onCurrentPageChangedCB(page)
    end
end

function UIScrollPage:SetOnRequestPrePageCB(func)
    self._onRequestPrePage = func
end

function UIScrollPage:OnRequestPrePage()
    if self._onRequestPrePage ~= nil then
        self._onRequestPrePage()
    end
end

function UIScrollPage:SetOnRequestNextPageCB(func)
    self._onRequestNextPage = func
end

function UIScrollPage:OnRequestNextPage()
    if self._onRequestNextPage ~= nil then
        self._onRequestNextPage()
    end
end


