-- UISlider.lua
local UISlider = Class.UISlider(ClassTypes.BaseLuaUIComponent)

UISlider.interface = GameConfig.ComponentsConfig.Slider
UISlider.classPath = "UIComponent.Base.UISlider"

local UIComponentUtil = GameUtil.UIComponentUtil

function UISlider.AddSlider(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local slider = UIComponentUtil.AddLuaUIComponent(child, UISlider)        
    return slider
end

function UISlider:Awake()
    self._csBH = self:GetComponent('LuaUISlider')
    if self:ExistChild("Handle Slide Area/Handle/Container_Num") then
        self._numContainer = self:FindChildGO("Handle Slide Area/Handle/Container_Num")
        self._numText = self:GetChildComponent("Handle Slide Area/Handle/Container_Num/Image_Bg1/Text_Num",'TextMeshWrapper')
        self._numContainer:SetActive(true)
    end
end

function UISlider:OnDestroy() 
    self._csBH = nil
end

function UISlider:SetOnValueChangedCB(func)
    self._onValueChangedCB = func
end

function UISlider:OnValueChanged(value)
    if self._onValueChangedCB ~= nil then
        self._onValueChangedCB(value)
    end
    if self._numText then
        self._numText.text = value
    end
end

function UISlider:SetValue(value)
    self._csBH:SetValue(value)
    if self._numText then
        self._numText.text = value
    end
end

function UISlider:GetValue()
    return self._csBH:GetValue()
end

function UISlider:SetMinValue(value)
    self._csBH:SetMinValue(value)
end

function UISlider:SetMaxValue(value)
    self._csBH:SetMaxValue(value)
end

function UISlider:SetEnabled(value)
    self._csBH:SetEnabled(value)
end