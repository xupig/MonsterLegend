-- UIRollerItem.lua
local UIRollerItem = Class.UIRollerItem(ClassTypes.BaseLuaUIComponent)

UIRollerItem.interface = GameConfig.ComponentsConfig.Component
UIRollerItem.classPath = "UIComponent.Base.UIRollerItem"

function UIRollerItem:Awake()
    self._csBH = self:GetComponent('LuaUIRollerItem')
end

function UIRollerItem:OnDestroy() 
    self._csBH = nil
end

function UIRollerItem:GetIndex()
    return self._csBH.Index
end

function UIRollerItem:__belonglist__(listObj)
    self._belongList = listObj
end

function UIRollerItem:GetListObj()
    return self._belongList
end

function UIRollerItem:__refresh__()
    local data = self._belongList:GetDataByIndex(self:GetIndex())
    self:OnRefreshData(data)
end

function UIRollerItem:OnRefreshData(data)
   	
end

function UIRollerItem:Stop()
    self._belongList:Stop()
end
