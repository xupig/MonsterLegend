-- UIParticle.lua
local UIParticle = Class.UIParticle(ClassTypes.LuaBehaviour)

UIParticle.interface = GameConfig.ComponentsConfig.Particle
UIParticle.classPath = "UIComponent.Base.UIParticle"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIParticle.AddParticle(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local particle = UIComponentUtil.AddLuaUIComponent(child, UIParticle)            
    return particle
end

function UIParticle:Awake()

end

function UIParticle:OnDestroy() 
    
end

--特效播放接口
--isLoop：是否循环，默认false
--reset：是否重头开始播放，默认true
function UIParticle:Play(isLoop, reset)
    isLoop = isLoop or false
    reset = reset or true
    self._csBH:Play(isLoop, reset)
end

--特效停止播放接口
function UIParticle:Stop()
    self._csBH:Stop()
end

function UIParticle:SetTimeScaleEnable(value)
    self._csBH:SetTimeScaleEnable(value)
end

--设置特效生命周期，持续的特效不用设置，单位：秒
function UIParticle:SetLifeTime(value)
    self._csBH:SetLifeTime(value)
end

function UIParticle:IsSortWhenEnable(value)
    self._csBH:IsSortWhenEnable(value)
end

function UIParticle:ForceReorderAll()
    self._csBH:ForceReorderAll()
end

--设置位置
--pos:Vector3
function UIParticle:SetPosition(pos)
    self._csBH.Position = pos
end

--设置缩放比例
--value:float,特效默认缩放为1
function UIParticle:SetScale(value)
    self._csBH.Scale = value
end

--设置播放完毕后的回调，只对isLoop==false的特效生效
function UIParticle:SetPlayEndCB(func)
    self._onPlayEndCB = func
end

--CS底层回调接口
function UIParticle:__onplayend__()
    if self._onPlayEndCB ~= nil then
        self._onPlayEndCB()
    end
end


