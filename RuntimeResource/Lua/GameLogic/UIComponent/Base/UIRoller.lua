-- UIRoller.lua 道具跑马灯
local UIRoller = Class.UIRoller(ClassTypes.BaseLuaUIComponent)

UIRoller.interface = GameConfig.ComponentsConfig.Roller
UIRoller.classPath = "UIComponent.Base.UIRoller"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIRoller.AddRoller(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local dial = UIComponentUtil.AddLuaUIComponent(child, UIRoller)
    return dial
end

function UIRoller:Awake()

end

function UIRoller:OnDestroy()

end

function UIRoller:SetItemType(class)
    self._csBH:SetItemType(class.classPath)
end

function UIRoller:SetDataList(dataList)
    if dataList ~= nil then
        self._dataList = {}
        for i, data in ipairs(dataList) do
            table.insert(self._dataList, data)
        end
        self._csBH:SetLength(#self._dataList)
    end
end

function UIRoller:SetFromToPos(fromPos, toPos, duration, itemSize, startItemCount)
    self._csBH:SetFromToPos(fromPos, toPos, duration, itemSize, startItemCount or 0)
end

function UIRoller:OnStart()
    self._csBH:OnStart()
end

function UIRoller:OnStop()
    self._csBH:OnStop()
end

function UIRoller:Continue()
    self._csBH:OnContinue()
end

function UIRoller:GetDataByIndex(idx)
    return self._dataList[idx]
end

--CS底层回调接口
function UIRoller:__endroundturn__()
    if self._onEndRoundTurnCB ~= nil then
        self._onEndRoundTurnCB()
    end
end


