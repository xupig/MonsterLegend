-- UIList.lua
local UIScrollView = Class.UIScrollView(ClassTypes.BaseLuaUIComponent)

UIScrollView.interface = GameConfig.ComponentsConfig.ScrollView
UIScrollView.classPath = "UIComponent.Base.UIScrollView"

function UIScrollView:Awake()
    self._csBH = self:GetComponent('LuaUIScrollView')
end

function UIScrollView:OnDestroy() 
    self._csBH = nil
end

function UIScrollView:SetOnRequestPreCB(func)
    self._onRequestPreCB = func
end

function UIScrollView:OnRequestPre()
    if self._onRequestPreCB ~= nil then
        self._onRequestPreCB()
    end
end

function UIScrollView:SetOnRequestNextCB(func)
    self._onRequestNextCB = func
end

function UIScrollView:OnRequestNext()
    if self._onRequestNextCB ~= nil then
        self._onRequestNextCB()
    end
end

function UIScrollView:SetOnMoveEndCB(func)
    self._onMoveEndCB = func
end

function UIScrollView:OnMoveEnd()
    if self._onMoveEndCB ~= nil then
        self._onMoveEndCB()
    end
end

function UIScrollView:SetOnClickCB(func)
    self._onClickCB = func
end

function UIScrollView:OnClick()
    if self._onClickCB ~= nil then
        self._onClickCB()
    end
end

function UIScrollView:SetScrollRectState(state)
    self._csBH:SetScrollRectState(state)
end

function UIScrollView:SetHorizontalMove(state)
    self._csBH:SetHorizontalMove(state)
end

function UIScrollView:SetVerticalMove(state)
    self._csBH:SetVerticalMove(state)
end

function UIScrollView:SetScrollRectEnable(state)
    self._csBH:SetScrollRectEnable(state)
end

function UIScrollView:SetCheckEnded(state)
    self._csBH:SetCheckEnded(state)
end

function UIScrollView:RefreshContent()
    self._csBH:RefreshContent()
end

--回到起始位置
function UIScrollView:ResetContentPosition()
    self._csBH:ResetContentPosition()
end

function UIScrollView:ResetMaskSize(x, y)
    self._csBH:ResetMaskSize(x, y)
end

function UIScrollView:UpdateArrow()
    self._csBH:UpdateArrow()
end