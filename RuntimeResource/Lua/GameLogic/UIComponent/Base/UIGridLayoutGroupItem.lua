-- UIGridLayoutGroupItem.lua
local UIGridLayoutGroupItem = Class.UIGridLayoutGroupItem(ClassTypes.BaseLuaUIComponent)

UIGridLayoutGroupItem.interface = GameConfig.ComponentsConfig.Component
UIGridLayoutGroupItem.classPath = "UIComponent.Base.UIGridLayoutGroupItem"

function UIGridLayoutGroupItem:Awake()
    self._csBH = self:GetComponent('UIGridLayoutGroupItem')
end

function UIGridLayoutGroupItem:OnDestroy() 
    self._csBH = nil
end

function UIGridLayoutGroupItem:GetIndex()
    return self._csBH.Index
end

function UIGridLayoutGroupItem:__belonglist__(listObj)
    self._belongList = listObj
end

function UIGridLayoutGroupItem:GetListObj()
    return self._belongList
end

function UIGridLayoutGroupItem:__refresh__()
    local data = self._belongList:GetDataByIndex(self:GetIndex())
    self:OnRefreshData(data)
end

function UIGridLayoutGroupItem:OnRefreshData(data)
   	
end
