-- UIList.lua
local UIList = Class.UIList(ClassTypes.BaseLuaUIComponent)

UIList.interface = GameConfig.ComponentsConfig.List
UIList.classPath = "UIComponent.Base.UIList"

UIList.DirectionTopToDown = GameMain.LuaUIListDirection.TopToDown
UIList.DirectionLeftToRight = GameMain.LuaUIListDirection.LeftToRight

require "UIComponent.Base.UIListItem"
require "UIComponent.Base.UIScrollView"
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil

function UIList.AddScrollViewList(hostGO, childPath)
    local child = nil
    if childPath == nil then
        child = hostGO
    else
        child = UIComponentUtil.FindChild(hostGO, childPath)
    end
    local scrollView = UIComponentUtil.AddLuaUIComponent(child, UIScrollView)            
    local mask = UIComponentUtil.FindChild(hostGO, childPath .."/Mask")
    local list = UIComponentUtil.AddChildLuaUIComponent(child, "Mask/Content", UIList)
    local item = UIComponentUtil.FindChild(hostGO, childPath .."/Mask/Content/item")
    list._itemSize = item:GetComponent(typeof(UnityEngine.RectTransform)).sizeDelta
    list._pageSize = mask:GetComponent(typeof(UnityEngine.RectTransform)).sizeDelta
    list._content = UIComponentUtil.FindChild(hostGO, childPath .."/Mask/Content")
    list._contentPosition = list._content.transform.localPosition
    list._scrollView = scrollView
    return scrollView, list
end

function UIList:__ctor__()
    self._itemSize = nil
    self._pageSize = nil
    self._content = nil
    self._contentPosition = nil
    self._scrollView = nil
    self._dataList = {}
end

function UIList:Awake()
end

function UIList:OnDestroy() 
    self._scrollView = nil
    self._itemSize = nil
    self._content = nil
    self._contentPosition = nil
end

function UIList:SetDataList(dataList, coroutineCreateCount)
    coroutineCreateCount = coroutineCreateCount or 0
    if dataList ~= nil then
        self._dataList = {}
        for i, data in ipairs(dataList) do
            table.insert(self._dataList, data)
        end
        self._csBH:SetLength(#self._dataList, coroutineCreateCount)
    else 
        self:RemoveAll() 
    end
    if self._scrollView:isActiveAndEnabled() then
        self._scrollView:UpdateArrow()
    end
end

function UIList:SetItemAnimType(itemAnimType)
    self._csBH:SetItemAnimType(itemAnimType)
end

function UIList:GetDataByIndex(idx)
    return self._dataList[idx + 1]
end

function UIList:SetItemClickedCB(func)
    self._onItemClickedCB = func
end

function UIList:OnItemClicked(item)
    if self._onItemClickedCB ~= nil then
        local index = item:GetIndex()
        self:SetSelectedIndex(index)
        self._onItemClickedCB(index)
    end
end

function UIList:SetItemType(class)
    self._csBH.luaItemType = class.classPath
end 

function UIList:SetSelectedIndex(index)
    self._csBH.SelectedIndex = index
end 

function UIList:SetListItemRenderingAgent(enable)
    self._csBH:SetListItemRenderingAgent(enable)
end

function UIList:SetPadding(top, right, bottom, left)
    self._topPadding = top
    self._csBH:SetPadding(top, right, bottom, left)
end

function UIList:SetGap(topToDownGap, leftToRightGap)
    self._topToDownGap = topToDownGap
    self._leftToRightGap = leftToRightGap
    self._csBH:SetGap(topToDownGap, leftToRightGap)
end

function UIList:SetDirection(direction, leftToRightCount, topToDownCount)
    self._direction = direction
    self._leftToRightCount = leftToRightCount
    self._topToDownCount = topToDownCount
    self._csBH:SetDirection(direction, leftToRightCount, topToDownCount)
end

function UIList:GetLength(len)
    if self._dataList ~= nil then
        return #self._dataList
    end
    return 0
end

function UIList:AddItem(layoutImmediately)
    return self._csBH:AddItem(layoutImmediately)
end

function UIList:AddItemAt(index, layoutImmediately)
    return self._csBH:AddItemAt(index, layoutImmediately)
end

function UIList:RemoveItemAt(index)
    return self._csBH:RemoveItemAt(index)
end

function UIList:RemoveAll()
    return self._csBH:RemoveAll()
end

function UIList:RemoveItemRange(start, count)
    return self._csBH:RemoveItemRange(start, count)
end

function UIList:GetItem(idx)
    return self._csBH:GetItem(idx)
end

function UIList:SetAllItemDirty()
    self._csBH:SetAllItemDirty()
end

function UIList:RefreshAllItemData()
    self._csBH:RefreshAllItemData()
end

function UIList:DoLayout()
    self._csBH:DoLayout()
end

function UIList:MoveToBottom()
    self._csBH:MoveToBottom()
end

function UIList:MoveToTop()
    self._csBH:MoveToTop()
end

function UIList:InBottom()
    return self._csBH:InBottom()
end

-- 是否用对象池
function UIList:UseObjectPool(value)
    self._csBH:UseObjectPool(value)
end

function UIList:IsItemAddRenderingAgent(state)
    self._csBH.isItemAddRenderingAgent = state
end

--菜单分帧创建完成的回调
function UIList:SetOnAllItemCreatedCB(func)
    self._onAllItemCreatedCB = func
end

function UIList:OnAllItemCreated()
    if self._onAllItemCreatedCB ~= nil then
        self._onAllItemCreatedCB()
    end    
end

function UIList:SetPositionByNum(num)
    if self._direction == UIList.DirectionTopToDown then
        self._content.transform.localPosition = Vector3.New(0,(self._topToDownGap + self._itemSize.y) * (num-1) ,0) + self._contentPosition
    elseif self._direction == UIList.DirectionLeftToRight then
        self._content.transform.localPosition = Vector3.New(-(self._leftToRightGap + self._itemSize.x) * (num-1), 0 ,0) + self._contentPosition
    end
end

function UIList:SetToZero()
    self._content.transform.localPosition = Vector3.zero
end

function UIList:SetRow(row)
    if row == 1 then
        self._content.transform.localPosition = self._contentPosition
    else
        self._content.transform.localPosition = Vector3.New(0, (self._topToDownGap + self._topPadding + self._itemSize.y) * row, 0)
    end
end

function UIList:GetItemSize()
    return self._itemSize
end

function UIList:GetPageSize()
    return self._pageSize
end

-- 如果列表没填满一页，此接口会自动居中
-- offset可选, 居中位置的偏移量
function UIList:SetCenter(offset, horizontalMove)
    offset = offset or 0
    if self._direction == UIList.DirectionLeftToRight then --水平滚动
        local len = self:GetLength()
        local itemSize = self:GetItemSize()
        local curWidth = len * itemSize.x
        local pageWidth = self:GetPageSize().x

        local perLeftToRightCount = self._csBH.LeftToRightCount
        local totalNum = self:GetLength()
        local needCenter = (perLeftToRightCount > totalNum)
        if needCenter then
            self._content.transform.localPosition = Vector3.New(((pageWidth - curWidth) * 0.5) + offset, 0 ,0) + self._contentPosition
        else
            self._content.transform.localPosition = self._contentPosition
        end
        if horizontalMove ~= nil then
            self._scrollView:SetHorizontalMove(horizontalMove)
        else    
            self._scrollView:SetHorizontalMove(not needCenter)
        end
	end
end