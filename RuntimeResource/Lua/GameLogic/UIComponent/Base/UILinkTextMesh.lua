-- UILinkTextMesh.lua
local UILinkTextMesh = Class.UILinkTextMesh(ClassTypes.BaseLuaUIComponent)

UILinkTextMesh.interface = GameConfig.ComponentsConfig.LinkTextMesh
UILinkTextMesh.classPath = "UIComponent.Base.UILinkTextMesh"

local UIComponentUtil = GameUtil.UIComponentUtil

function UILinkTextMesh.AddLinkTextMesh(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local linkTextMesh = UIComponentUtil.AddLuaUIComponent(child, UILinkTextMesh)
    return linkTextMesh
end

function UILinkTextMesh:Awake()
    self._csBH = self:GetComponent('LuaUILinkTextMesh')
end

function UILinkTextMesh:OnDestroy()
    self._csBH = nil
end

function UILinkTextMesh:SetOnClickCB(func)
    self._onOnClickCB = func
end

function UILinkTextMesh:OnClickLink(linkId, position)
    if self._onOnClickCB ~= nil then
        self._onOnClickCB(linkId, position)
    end
end
