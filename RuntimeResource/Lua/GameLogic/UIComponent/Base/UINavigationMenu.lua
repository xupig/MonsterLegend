-- UINavigationMenu.lua
--导航二级菜单组件
local UINavigationMenu = Class.UINavigationMenu(ClassTypes.BaseLuaUIComponent)

UINavigationMenu.interface = GameConfig.ComponentsConfig.NavigationMenu
UINavigationMenu.classPath = "UIComponent.Base.UINavigationMenu"

require "UIComponent.Base.UINavigationMenuItem"
require "UIComponent.Base.UIScrollView"

local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil

function UINavigationMenu.AddScrollViewMenu(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local scrollView = UIComponentUtil.AddLuaUIComponent(child, UIScrollView)
    local navigationMenu = UIComponentUtil.AddChildLuaUIComponent(child, "Mask/Content", UINavigationMenu)
    local item = UIComponentUtil.FindChild(hostGO, childPath .."/Mask/Content/FirstLevelItem")
    navigationMenu._content = UIComponentUtil.FindChild(hostGO, childPath .."/Mask/Content")
    navigationMenu._itemSize = item:GetComponent(typeof(UnityEngine.RectTransform)).sizeDelta
    navigationMenu._contentPosition = navigationMenu._content.transform.localPosition
    if navigationMenu == nil then
        LoggerHelper.Error("navigationMenu is nil")
    end
    return scrollView, navigationMenu
end

function UINavigationMenu:Awake()
    self._csBH = self:GetComponent('LuaUINavigationMenu')
end

function UINavigationMenu:OnDestroy()
    self._csBH = nil
end

--数据格式--第二级的table里第一项必须是一级菜单的数据，从第二项开始是二级菜单数据，
--一级菜单数据必须存在，二级菜单数据可以为空
-- local demoNavigationData =
--         {
--           {{text = "FirstLevelItem1"}, {text = "SecondLevelItem1"}, {text = "SecondLevelItem2"}},
--           {{text = "FirstLevelItem2"}, {text = "SecondLevelItem3"}},
--           {{text = "FirstLevelItem3"}, {text = "SecondLevelItem4"}},
--           {{text = "FirstLevelItem4"}}
--         }
function UINavigationMenu:SetDataList(dataList,num)
    self:RemoveAll()
    self._dataList = dataList
    self._dataLength = {}
    
    if self._dataList ~= nil then
        for i = 1, #dataList do
            self._dataLength[i] = #dataList[i];
        end
        self._csBH:SetDataList(self._dataLength, num or 0)
    end
end

--获取一级菜单数据
function UINavigationMenu:GetFirstLevelDataByIndex(idx)
    return self._dataList[idx + 1][1]
end

--获取二级菜单数据
function UINavigationMenu:GetSecondLevelDataByIndex(fidx, sidx)
    return self._dataList[fidx + 1][sidx + 2]
end

--一级菜单点击回调
function UINavigationMenu:SetFirstLevelMenuItemClickedCB(func)
    self._onFirstLevelMenuItemClickedCB = func
end

--二级菜单点击回调
function UINavigationMenu:SetSecondLevelMenuItemClickedCB(func)
    self._onSecondLevelMenuItemClickedCB = func
end

--一级菜单点击响应
function UINavigationMenu:OnFirstLevelMenuItemClicked(item)
    if self._onFirstLevelMenuItemClickedCB ~= nil then
        self._onFirstLevelMenuItemClickedCB(item:GetFIndex())
    end
end

--二级菜单点击响应
function UINavigationMenu:OnSecondLevelMenuItemClicked(item)
    if self._onSecondLevelMenuItemClickedCB ~= nil then
        self._onSecondLevelMenuItemClickedCB(item:GetFIndex(), item:GetSIndex())
    end
end

--设定一级菜单Lua类
function UINavigationMenu:SetFirstLevelItemType(class)
    self._csBH.luaFItemType = class.classPath
end

--设定二级菜单Lua类
function UINavigationMenu:SetSecondLevelItemType(class)
    self._csBH.luaSItemType = class.classPath
end

function UINavigationMenu:SetPadding(top, right, bottom, left)
    self._csBH:SetPadding(top, right, bottom, left)
end

function UINavigationMenu:SetGap(topToDownGap, leftToRightGap)
    self._csBH:SetGap(topToDownGap, leftToRightGap)
end

-- function UINavigationMenu:SetDirection(direction, leftToRightCount, topToDownCount)
--     self._csBH:SetDirection(direction, leftToRightCount, topToDownCount)
-- end
--设定是否始终只有一个一级菜单展开，展开另外的一级菜单会缩起其他一级菜单
function UINavigationMenu:SetOnlyOneFItemExpand(oneExpand)
    self._csBH:SetOnlyOneFItemExpand(oneExpand)
end

--展开所有一级菜单
function UINavigationMenu:ExpandAllFItem()
    self._csBH:ExpandAllFItem()
end

--一级菜单长度
function UINavigationMenu:GetFirstLevelLength()
    if self._dataList ~= nil then
        return #self._dataList
    end
    return 0
end

--二级菜单长度,从0开始
function UINavigationMenu:GetSecondLevelLength(fidx)
    if self._dataLength ~= nil and self._dataLength[fidx + 1] then
        return self._dataLength[fidx + 1] - 1
    end
    return 0
end

-- function UINavigationMenu:AddItem(layoutImmediately)
--     return self._csBH:AddItem(layoutImmediately)
-- end
-- function UINavigationMenu:AddItemAt(index, layoutImmediately)
--     return self._csBH:AddItemAt(index, layoutImmediately)
-- end
-- function UINavigationMenu:RemoveItemAt(index)
--     return self._csBH:RemoveItemAt(index)
-- end
function UINavigationMenu:RemoveAll()
    return self._csBH:RemoveAll()
end

-- function UINavigationMenu:RemoveItemRange(start, count)
--     return self._csBH:RemoveItemRange(start, count)
-- end
--获取一级菜单元素luatable,从0开始
function UINavigationMenu:GetFItem(idx)
    return self._csBH:GetFItem(idx)
end

--获取二级菜单元素luatable,从0开始
function UINavigationMenu:GetSItem(fidx, sidx)
    return self._csBH:GetSItem(fidx, sidx)
end

--选中一级菜单元素,展开一级菜单,从0开始
function UINavigationMenu:SelectFItem(idx)
    self._csBH:SelectFItem(idx)
end

function UINavigationMenu:DoLayout()
    self._csBH:DoLayout()
end

-- 是否用对象池
function UINavigationMenu:UseObjectPool(value)
    self._csBH:UseObjectPool(value)
end

--菜单分帧创建完成的回调
function UINavigationMenu:SetOnAllItemCreatedCB(func)
    self._onAllItemCreatedCB = func
end

function UINavigationMenu:OnAllItemCreated()
    if self._onAllItemCreatedCB ~= nil then
        self._onAllItemCreatedCB()
    end    
end

function UINavigationMenu:SetPositionByNum(num)
    self._content.transform.localPosition = Vector3.New(0,(self._itemSize.y) * (num-1) ,0) + self._contentPosition
end
