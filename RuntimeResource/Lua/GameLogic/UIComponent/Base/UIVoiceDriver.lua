-- UIVoiceDriver.lua
local UIVoiceDriver = Class.UIVoiceDriver(ClassTypes.BaseLuaUIComponent)

UIVoiceDriver.interface = GameConfig.ComponentsConfig.VoiceDriver
UIVoiceDriver.classPath = "UIComponent.Base.UIVoiceDriver"

local UIComponentUtil = GameUtil.UIComponentUtil
local ChatConfig = GameConfig.ChatConfig
local IsSendVoiceMessage = true
local SystemSettingConfig = GameConfig.SystemSettingConfig

function UIVoiceDriver.AddVoiceDriver(hostGO, childPath)
    local child = hostGO
    if childPath ~= nil and childPath ~= "" then
        child = UIComponentUtil.FindChild(hostGO, childPath)
    end
    local VoiceDriver = UIComponentUtil.AddLuaUIComponent(child, UIVoiceDriver)        
    return VoiceDriver
end

function UIVoiceDriver:Awake()
    self._csBH = self:GetComponent('LuaUIVoiceDriver')
    EventDispatcher:AddEventListener(GameEvents.OnSoundChanged, function(num) self:OnSoundChanged(num) end)
end

function UIVoiceDriver:OnDestroy() 
    self._csBH = nil
    EventDispatcher:RemoveEventListener(GameEvents.OnSoundChanged, function(num) self:OnSoundChanged(num) end)
end

function UIVoiceDriver:OnSoundChanged(num)
    if num == nil then
        num = PlayerManager.SystemSettingManager:GetPlayerSetting(SystemSettingConfig.SOUND_VOLUME)
    else
        PlayerManager.SystemSettingManager:GetPlayerSetting(SystemSettingConfig.SOUND_VOLUME, num)
    end

    if num < 0.15 then
        IsSendVoiceMessage = false
        --LoggerHelper.Error("error 音效过小,无法播放语音")
    end
    
end

--  记录 语音 SDK 回调
function UIVoiceDriver:OnStartVoiceTalk()
    
end

function UIVoiceDriver:OnConvertVoiceMessage(msg)
    if IsSendVoiceMessage then
        if GameManager.GUIManager.PanelIsActive(PanelsConfig.Friend) then
            EventDispatcher:TriggerEvent(GameEvents.ConvertVoiceMessageFriend, msg)
        else
            EventDispatcher:TriggerEvent(GameEvents.ConvertVoiceMessage, msg)
        end   
    end
end

function UIVoiceDriver:OnShowVoiceTalkError(errorId)
    -- LoggerHelper.Log('[UIVoiceDriver:OnDownVoiceErrorCallback]=====================:' .. tostring( errorId ) )
end

-- 调用SDK 记录 语音
function UIVoiceDriver:OnStartToTalk()
    self._csBH:OnStartTalk()
end

-- 调用SDK 停止 语音
function UIVoiceDriver:OnSDKStopTalk()
    self._csBH:OnSDKStopTalk()
end

function UIVoiceDriver:OnPlayVoiceError(errorId)
    local tipsId = ChatConfig.VoiceErrorMap[errorId]
    -- LoggerHelper.Log('UIVoiceDriver:OnPlayVoiceError:' .. errorId)
    if tipsId ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.PlayVoiceError, tipsId)
    end
end

-- function UIVoiceDriver:SetOnConvertVoiceMessageCB(func)
--     self._onConvertVoiceMessageCB = func
-- end

function UIVoiceDriver:OnVoiceVolumeChange(volume)

end

function UIVoiceDriver:OnStartPlayVoice(path,time)
    self._csBH:OnSDKPlayVoice(path,time)
end

function UIVoiceDriver:SendVoiceMessageType(type)
    if type == ChatConfig.Voice then
        IsSendVoiceMessage = true
    else
        IsSendVoiceMessage = false
    end
end