-- UIListItem.lua
local UIListItem = Class.UIListItem(ClassTypes.BaseLuaUIComponent)

UIListItem.interface = GameConfig.ComponentsConfig.Component
UIListItem.classPath = "UIComponent.Base.UIListItem"

function UIListItem:Awake()
    self._csBH = self:GetComponent('LuaUIListItem')
end

function UIListItem:OnDestroy() 
    self._csBH = nil
end

function UIListItem:GetIndex()
    return self._csBH.Index
end

function UIListItem:__belonglist__(listObj)
    self._belongList = listObj
end

function UIListItem:GetListObj()
    return self._belongList
end

function UIListItem:__refresh__()
    local data = self._belongList:GetDataByIndex(self:GetIndex())
    self:OnRefreshData(data)
end

function UIListItem:OnRefreshData(data)
   	
end

function UIListItem:SetDataDirty()
    self._csBH:SetDataDirty()
end


