-- UIComplexList.lua
local UIComplexList = Class.UIComplexList(ClassTypes.UIList)

UIComplexList.interface = GameConfig.ComponentsConfig.ComplexList
UIComplexList.classPath = "UIComponent.Base.UIComplexList"


require "UIComponent.Base.UIListItem"
require "UIComponent.Base.UIScrollView"
local UIScrollView = ClassTypes.UIScrollView
local UIComponentUtil = GameUtil.UIComponentUtil

function UIComplexList.AddScrollViewComplexList(hostGO, childPath)
    local child = nil
    if childPath == nil then
        child = hostGO
    else
        child = UIComponentUtil.FindChild(hostGO, childPath)
    end
    local scrollView = UIComponentUtil.AddLuaUIComponent(child, UIScrollView)            
    local list = UIComponentUtil.AddChildLuaUIComponent(child, "Mask/Content", UIComplexList)
    return scrollView, list
end

function UIComplexList:SetDataList(dataList, coroutineCreateCount)
    LoggerHelper.Error("UIComplexList 不能使用SetDataList 接口，请用其他接口代替")
end

function UIComplexList:AppendItemData(data,doLayout)
    table.insert(self._dataList, data)
    self._csBH:AddItem(false)
    self._csBH:RefreshAllDirtyItem()
    if doLayout == nil or doLayout == true then
        -- self._csBH:DoLayout()
        self:DoLayout()
    end
end

--传入index从1开始的table
function UIComplexList:AppendItemDataList(dataList,doLayout)
    for i, data in ipairs(dataList) do
        table.insert(self._dataList, data)
    end
    self._csBH:AddItems(#dataList, false)
    self._csBH:RefreshAllDirtyItem()
    if doLayout == nil or doLayout == true then
        -- self._csBH:DoLayout()
        self:DoLayout()
    end
end

--传入index从1开始
function UIComplexList:RemoveDataAt(index)
    self:RemoveItemAt(index - 1)
end

--传入index从0开始
function UIComplexList:RefreshItem(index)
    self._csBH:SetItemDirtyAt(index)
    self._csBH:RefreshAllDirtyItem()
    self._csBH:DoLayout()
end

--传入index从0开始
function UIComplexList:RemoveItemAt(index,doLayout)
    table.remove(self._dataList, index + 1)
    self._csBH:RemoveItemAt(index)
    if doLayout == nil or doLayout == true then
        -- self._csBH:DoLayout()
        self:DoLayout()
    end
end

function UIComplexList:RemoveAll()
    self._dataList = {}
    self._csBH:RemoveAll()
end

--传入index从0开始
function UIComplexList:RemoveItemRange(start, count,doLayout)
    -- for i=1, count do
    --     table.remove(self._dataList, start + i)
    -- end 


    local datalist = {}
    if start == 0 then
        for i=count+1, #self._dataList do
            table.insert( datalist,self._dataList[i])
        end
    else
        for i=1, start do
            table.insert( datalist,self._dataList[i])
        end 

        for i=count+1, #self._dataList do
            table.insert( datalist,self._dataList[i])
        end 
    end

    self._dataList = datalist


    self._csBH:RemoveItemRange(start, count)
    if doLayout == nil or doLayout == true then
        -- self._csBH:DoLayout()
        self:DoLayout()
    end
end

-- 手动布局 很消耗性能，不建议多次调用
function UIComplexList:DoLayout()
    self._csBH:DoLayout()
end