-- UIToggle.lua
local UIToggle = Class.UIToggle(ClassTypes.BaseLuaUIComponent)

UIToggle.interface = GameConfig.ComponentsConfig.Toggle
UIToggle.classPath = "UIComponent.Base.UIToggle"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIToggle.AddToggle(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local toggle = UIComponentUtil.AddLuaUIComponent(child, UIToggle)
    return toggle
end

function UIToggle:Awake()
    self._csBH = self:GetComponent('LuaUIToggle')
    self._toggleWrapper = self:GetComponent('ToggleWrapper')
end

function UIToggle:OnDestroy()
    self._csBH = nil
    self._toggleWrapper = nil
end

function UIToggle:SetOnValueChangedCB(func)
    self._onValueChangedCB = func
end

function UIToggle:OnValueChanged(state)
    if self._onValueChangedCB ~= nil then
        self._onValueChangedCB(state)
    end
end

function UIToggle:GetToggleWrapper()
    return self._toggleWrapper
end

function UIToggle:SetIsOn(state)
    self._csBH:SetIsOn(state)
end

function UIToggle:SetText(str)
    if self._txt == nil then
        self._txt = self:GetChildComponent("Label", "TextMeshWrapper")
    end
    self._txt.text = str
end