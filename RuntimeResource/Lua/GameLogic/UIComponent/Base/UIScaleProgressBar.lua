-- UIScaleProgressBar.lua
-- Container_PointEnd是进度条末端的特效容器，特效要放到容器正中央
-- 使用SetProgress或SetProgressByValue方法设置进度 制作ui时初始把所需的scale设为0
local UIScaleProgressBar = Class.UIScaleProgressBar(ClassTypes.BaseLuaUIComponent)

UIScaleProgressBar.interface = GameConfig.ComponentsConfig.ScaleProgressBar
UIScaleProgressBar.classPath = "UIComponent.Base.UIScaleProgressBar"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIScaleProgressBar.AddProgressBar(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local progressBar = UIComponentUtil.AddLuaUIComponent(child, UIScaleProgressBar)
    return progressBar
end

function UIScaleProgressBar:Awake()
    self._csBH = self:GetComponent('LuaUIScaleProgressBar')
end

function UIScaleProgressBar:OnDestroy() 
    self._csBH = nil
end

--设置进度条类型
function UIScaleProgressBar:SetType(type)
    
end

--设置进度条方向，默认水平 false为垂直 在ShowTweenAnimate和SetTweenTime之前调用
function UIScaleProgressBar:SetHorizontal(isHorizontal)
    self._csBH:SetHorizontal(isHorizontal)
end

--设置barDelay延迟播放时间(ms)
function UIScaleProgressBar:SetBarDelayTime(ms)
    self._csBH:SetBarDelayTime(ms)
end

--设置是否显示缓动
function UIScaleProgressBar:ShowTweenAnimate(shouldShow)
    self._csBH:ShowTweenAnimate(shouldShow)
end

--设定progress值大于1时是否进行多次缓动
function UIScaleProgressBar:SetMutipleTween(isMultipleTweenByProgress)
    self._csBH:SetMutipleTween(isMultipleTweenByProgress)
end

--设定progress值大于1时进行多次缓动如果progress值为整数最后一次动画之后进度是否归零
function UIScaleProgressBar:SetIsResetToZero(isResetToZero)
    self._csBH:SetIsResetToZero(isResetToZero)
end

--进度条末端动画渐入渐出效果暂时先不用
-- function UIScaleProgressBar:SetPointEndFadeInTime(time)
--     self._csBH:SetPointEndFadeInTime(shouldShow)
-- end

-- function UIScaleProgressBar:SetPointEndFadeOutTime(time)
--     self._csBH:SetPointEndFadeOutTime(shouldShow)
-- end

--设置进度条进度百分比
function UIScaleProgressBar:SetProgress(percent)
    self._percent = percent
    self._csBH:SetProgress(percent)
end

--设置当前值和最大值
function UIScaleProgressBar:SetProgressByValue(current , max , showText)
    if max == 0 then
        current = 0
        max = 1
    end
    if max then
        self:SetProgress(current/max)
    else
        self:SetProgress(0)
    end
    if showText then
        self:SetProgressText(tostring(current).."/"..tostring(max))
    end
end

--设定动画变化所需时间
function UIScaleProgressBar:SetTweenTime(sec)
    self._csBH:SetTweenTime(sec)
end

--设置进度条上的文本
function UIScaleProgressBar:SetProgressText(text)
    self._csBH:SetProgressText(text)
end

--设置进度条动画播放完成回调函数
function UIScaleProgressBar:SetTweenCompleteCB(func)
    self._onTweenCompleteCB = func
end

--播放结束执行回调(限定有进度动画的进度条使用)
function UIScaleProgressBar:OnProgressTweenCompleted()
    if self._onTweenCompleteCB ~= nil then
        self._onTweenCompleteCB()
    end
end

--播放完一段动画之后执行的回调(限定有多次进度动画的进度条使用)
function UIScaleProgressBar:SetNextTweenCB(func)
    self._onNextTweenCB = func
end

function UIScaleProgressBar:OnProgressNextTween()
    if self._onNextTweenCB ~= nil then
        self._onNextTweenCB()
    end
end

--获取当前进度百分比
function UIScaleProgressBar:GetProgress()
    if self._percent ~= nil then
        return self._percent
    else
        return 0
    end
end