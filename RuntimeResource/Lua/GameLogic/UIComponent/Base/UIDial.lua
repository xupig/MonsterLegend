-- UIDial.lua 转盘抽奖
local UIDial = Class.UIDial(ClassTypes.BaseLuaUIComponent)

UIDial.interface = GameConfig.ComponentsConfig.Dial
UIDial.classPath = "UIComponent.Base.UIDial"

local UIComponentUtil = GameUtil.UIComponentUtil

function UIDial.AddDial(hostGO, childPath)
    local child = UIComponentUtil.FindChild(hostGO, childPath)
    local dial = UIComponentUtil.AddLuaUIComponent(child, UIDial)            
    return dial
end

function UIDial:Awake()

end

function UIDial:OnDestroy() 

end

--开始抽奖 data为结果list 例如{0} {0,1,1,1,1}
function UIDial:StartTurn(data)
    self._csBH:StartTurn(data)
end

--设置抽奖结束回调
function UIDial:SetEndRoundTurnCB(func)
    self._onEndRoundTurnCB = func
end

--CS底层回调接口
function UIDial:__endroundturn__()
    if self._onEndRoundTurnCB ~= nil then
        self._onEndRoundTurnCB()
    end
end


