-- Icon.lua

local ClassTypes = ClassTypes
local UIListItem = ClassTypes.UIListItem
local Icon = Class.Icon(UIListItem)

Icon.interface = GameConfig.ComponentsConfig.PointableComponent
Icon.classPath = "UIComponent.Icon"

function Icon:Awake()
	UIListItem.Awake(self)
	self._text = self:FindChildGO("Text"):GetComponent("TextMeshWrapper")
end

function Icon:OnDestroy()

end

function Icon:OnPointerDown(pointerEventData)

end

--override
function Icon:OnRefreshData(data)
	if data == nil then
		self._text.text = "nil"
	else
		self._text.text = data.cfg_id .. '-'.. data:Count()
	end
end
