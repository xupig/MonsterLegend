require "Entities/Avatar"
local public_config = require("ServerConfig/public_config")

local Monster = Class.Monster(ClassTypes.Creature)
local MonsterDataHelper = GameDataHelper.MonsterDataHelper

--local EntityConfig = GameConfig.EntityConfig
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local GUIManager = GameManager.GUIManager
local MonsterType = GameConfig.EnumType.MonsterType
local BillBoardType = GameConfig.EnumType.BillBoardType
local EntityType = GameConfig.EnumType.EntityType

function Monster:__ctor__(csEntity)
    self.entityType = EntityType.Monster
end

function Monster:OnEnterWorld()
    math.randomseed(self.id)
    self:HasActorDeathController(true)
    self.cs.clientCamp = 1
    self.cs.canRelease = true
    self.name = MonsterDataHelper.GetMonsterName(self.monster_id)
    self:UpdateLevel()
    self.scale = MonsterDataHelper.GetMonsterScale(self.monster_id)
    self.cs.monsterId = self.monster_id
    self.monsterType = MonsterDataHelper.GetMonsterType(self.monster_id)
    
    local m = MonsterDataHelper.GetMonster(self.monster_id)
    self.model_low = MonsterDataHelper.GetMonsterModel(self.monster_id)
    self.ai_id = MonsterDataHelper.GetMonsterAI(self.monster_id)
    self.build_type = MonsterDataHelper.GetMonsterBuildType(self.monster_id)
    local buffs = MonsterDataHelper.GetMonsterBuffs(self.monster_id)
    Monster._base.OnEnterWorld(self)
    self:SetLearnedSkillList(MonsterDataHelper.GetMonsterSkills(self.monster_id))
    local speed = MonsterDataHelper.GetMonsterSpeed(self.monster_id)
    self:SetActor(self.model_low)
    self.cs:SetCreatureType(2)--指定类型为怪物2
    self:SetCityState(false)
	self.spawn_id = self.monster_region_id
    self.has_set_delegate_eid = false
    EventDispatcher:TriggerEvent(GameEvents.OnMonsterEnterWorld, self.monster_id, self.id)
end

function Monster:OnLeaveWorld()
    EventDispatcher:TriggerEvent(GameEvents.OnMonsterLeaveWorld, self.monster_id, self.id)
    Monster._base.OnLeaveWorld(self)
end

function Monster:__OnReuse()
    if self.showBillboard then
        EventDispatcher:TriggerEvent(GameEvents.ADD_CREATURE_BILLBOARD, self.id, self.entityType)
    end
    self:UpdateLevel()
    self.has_set_delegate_eid = false
    EventDispatcher:TriggerEvent(GameEvents.OnMonsterEnterWorld, self.monster_id, self.id)
end

function Monster:__OnRelease()
    if self.showBillboard then
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_CREATURE_BILLBOARD, self.id, self.entityType)
    end
    EventDispatcher:TriggerEvent(GameEvents.OnMonsterLeaveWorld, self.monster_id, self.id)
end

function Monster:GetDefaultLayer()
    return GameObjectLayerConfig.Monster
end

function Monster:OnActorLoaded()
    self.cs:SetSelectTrigger()
    --LoggerHelper.Error("OnActorLoaded ")
    self.actorHasLoaded = true
    --self:SetBehaviorTree(self.ai_id)
    --初始模型缩放设置
    self.cs:InitActorScale(self.scale)
    --self.entityAI:SetBornPos(self.cs:GetTransform().position)
    --self:SetThinkState(false)  --设置停止思考
    self:IdleToReady()
end

function Monster:OnDeath()
    EventDispatcher:TriggerEvent(GameEvents.ActionMonsterDeath, self.monster_id, self.spawn_id)
    Monster._base.OnDeath(self)
end


function Monster:set_delegate_eid()
    self.has_set_delegate_eid = true
    self:init_delegate()
end

function Monster:init_delegate()
    if self.isInWorld and self.actorHasLoaded then
		if self.delegate_eid > 0 then
			GameWorld.Player().server.get_bb_req(self.id)
		end
	end
end

function Monster:IsBoss()
	return MonsterType.IsBoss(self.monsterType)
end

function Monster:set_level(oldValue)
    self:UpdateLevel()
end

function Monster:UpdateLevel() -- 客户端处理怪物等级
    if not self.monster_id then 
        return
    end
    local levelType = MonsterDataHelper.GetMonsterLevelType(self.monster_id)
    if levelType == 1 then
        self.level = GameWorld.Player().level
    elseif levelType == 2 then
        self.level = GameWorld.Player().world_level
    else
        self.level = MonsterDataHelper.GetLv(self.monster_id)
    end
end