--法宝类型
require "Entities/Creature"


local ai_utils = require "AI/ai_utils"
local Fabao = Class.Fabao(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local EntityType = GameConfig.EnumType.EntityType
local public_config = GameWorld.public_config
local TransformDataHelper = GameDataHelper.TransformDataHelper
local ONE_ASSIST_CAST_MS = 1

function Fabao:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.Fabao
    self.isShow = true
    self.isInit = false
    self.isEnter = false
end

function Fabao:OnEnterWorld()
    self.isEnter = true
    self._base.OnEnterWorld(self)
    
    self:SetLearnedSkillList({self.skill})
end

function Fabao:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function Fabao:OnActorLoaded()
    self:SetBehaviorTree(self.ai_id)
    self:SetThinkState(true)
    self.isInit = true
    self:SetVisible(self.isShow)
end

function Fabao:set_delegate_eid()
    self.cs.delegateEid = self.delegate_eid
end

function Fabao:set_model_id()
    self.isInit = false
    self.ai_id = TransformDataHelper:GetPetAndTreasureAI(self.model_id)
    self.skill = TransformDataHelper:GetPetAndTreasureSkill(self.model_id)
    if self.isEnter then
        self:SetLearnedSkillList({self.skill})
    end
    local model = TransformDataHelper:GetPetAndTreasureModel(self.model_id)
    if model then
        self:SetActor(model)
    end
end

--<!-- 0：隐藏 1：显示-->
function Fabao:set_show_state()
    if self.isInit then
        if self.show_state > 0 then
            self.isShow = true
            self:SetVisible(self.isShow)
        else
            self.isShow = false
            self:SetVisible(self.isShow)
        end
    end
end

function Fabao:GetDefaultLayer()
    return GameObjectLayerConfig.Actor
end
