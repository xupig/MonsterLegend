--Boss坟墓实体
require "Entities/StaticEntity"
local BossTomb = Class.BossTomb(ClassTypes.StaticEntity)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local EntityType = GameConfig.EnumType.EntityType
local WorldBossDataHelper = GameDataHelper.WorldBossDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function BossTomb:__ctor__(csEntity)
	self.entityType = EntityType.BossTomb
	self.showBillboard = true
	self.bossId = 0
	self.reviveTime = 0
end

function BossTomb:OnEnterWorld()
	BossTomb._base.OnEnterWorld(self)
	self.name = LanguageDataHelper.CreateContent(WorldBossDataHelper:GetBossName(self.bossId))
	local modelId = GlobalParamsHelper.GetParamValue(557)
	if modelId then
		self:SetModel(modelId)
	end
end

function BossTomb:OnLeaveWorld()
    BossTomb._base.OnLeaveWorld(self)
end

function BossTomb:Destroy()
	GameWorld.DestroyEntity(self.id)
end