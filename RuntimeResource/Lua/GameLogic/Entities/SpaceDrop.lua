--客户端掉落物
require "Entities/StaticEntity"
local SpaceDrop = Class.SpaceDrop(ClassTypes.StaticEntity)
local ItemDataHelper = GameDataHelper.ItemDataHelper
local Vector3 = Vector3
local GameWorld = GameWorld
local TimerHeap = TimerHeap
local EntityType = GameConfig.EnumType.EntityType
local ItemConfig = GameConfig.ItemConfig
local CollectManager = GameManager.CollectManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ClientEntityManager = GameManager.ClientEntityManager
local DateTimeUtil = GameUtil.DateTimeUtil
local EntityMgr = ClassTypes.EntityMgr
--local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local BaseUtil = GameUtil.BaseUtil

function SpaceDrop:__ctor__(csEntity)
	--LoggerHelper.Error("SpaceDrop:__ctor__")
	self.ownDropId = 0 --所属DropItem实体id
	self.index = 0		--所属DropItem.str_items的index
	self.itemId = 0
	self.itemCount = 0
	self.isClient = false --true 客户端本掉落，false 服务端本掉落
	self._canPick = false
	--self._autoPickRange = GlobalParamsHelper.GetParamValue(344)
	--self._autoPickCD = GlobalParamsHelper.GetParamValue(369)
	self._range = GlobalParamsHelper.GetParamValue(343)
	--self._demonMap = GlobalParamsHelper.GetParamValue(872)
	self.entityType = EntityType.SpaceDrop
    self.showBillboard = true
    --self._needDestroy = true --
end


function SpaceDrop:OnEnterWorld()
	--设置小恶魔拾取范围(废弃)
	-- local equipGuard = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetEquipListBySubType(public_config.PKG_LOADED_EQUIP_INDEX_GUARD)
	-- if equipGuard[1] then
	-- 	local equipGuardId = equipGuard[1].cfg_id
	-- 	if self._demonMap[equipGuardId] then
	-- 		self._range = self._demonMap[equipGuardId]
	-- 	end
	-- end

	self._picking = false
	local quality = ItemDataHelper.GetQuality(self.itemId)
    local colorId = ItemConfig.QualityTextMap[quality]

    local nameStr = ItemDataHelper.GetItemName(self.itemId)
    if self.itemId < public_config.MAX_MONEY_ID then
    	nameStr = StringStyleUtil.GetLongNumberStringEx(self.itemCount)..nameStr
    end
    nameStr = StringStyleUtil.GetColorStringWithColorId(nameStr,colorId)
    self.name = nameStr
	--LoggerHelper.Error("SpaceDrop:OnEnterWorld"..self.name)
	SpaceDrop._base.OnEnterWorld(self)
	--音效配置
	self._sfxDropLoot = ItemDataHelper.GetSfxDropLoot(self.itemId)

	--暂时用随机时间制造掉落间隔效果
	local randtime = Mathf.Random(500)
	self._timerId = TimerHeap:AddTimer(randtime, 0, 0, function() 
		if self._sfxDropLoot[1] then
			GameWorld.PlaySound(self._sfxDropLoot[1])
		end
	end)
	
	local modelId = ItemDataHelper.GetLook(self.itemId)
	if modelId then
		self:SetModel(modelId)
	end
	
	local effmodelId = quality + 100
	if quality > 1 then
		self:SetEffectModel(effmodelId)
	end
	
	local dropItem = EntityMgr:GetEntity(self.ownDropId)
	if dropItem == nil then
		LoggerHelper.Error("No Drop Item")
		return
	end
	local allPickTime = GlobalParamsHelper.GetParamValue(440)
	local dtime = dropItem.create_time + allPickTime - DateTimeUtil.GetServerTime()
	if dtime <= 0 then
		self._canPick = true
	else
		if not next(dropItem.owners) then
			self._canPick = true
		else
			if dropItem.owners[GameWorld.Player().id] then
	            self._canPick = true
	        end
		end
	end

	if self._canPick then
		self:SetUpPick()
	else
		--LoggerHelper.Error("dtime"..dtime)
		--计时一下然后允许拾取
		self._openPickTimeId = TimerHeap:AddSecTimer(dtime, 0, 0, function() 
			self._canPick = true
			self:SetUpPick() 
			TimerHeap:DelTimer(self._openPickTimeId)
		end)
	end
end

function SpaceDrop:OnModelLoaded()

end

--是否能拾取
function SpaceDrop:GetCanPick()
	--金币的情况return false,不需主动前往拾取
	if self.itemId < public_config.MAX_MONEY_ID then
    	return false
    end
	return self._canPick
end

--设定拾取
function SpaceDrop:SetUpPick()
	--金币直接拾取
	if self.itemId < public_config.MAX_MONEY_ID then
		local delayTimeCfg = GlobalParamsHelper.GetParamValue(941)
		local delayTime = math.random(delayTimeCfg[1]*10,delayTimeCfg[2]*10)/10
		self._timerId = TimerHeap:AddSecTimer(delayTime, 0, 0, function() self:HandlePick() end)
    	return
    end
	self._timerId = TimerHeap:AddSecTimer(1, 0.3, 0, function() self:_Checking() end)
	--self._autoPickTimerId = TimerHeap:AddSecTimer(self._autoPickCD, 0, 0, function() self:AutoPick() end)
end

function SpaceDrop:OnGround()
	
end

function SpaceDrop:OnLeaveWorld()
    SpaceDrop._base.OnLeaveWorld(self)
    --LoggerHelper.Error("SpaceDrop:OnLeaveWorld()")
    if self._isMyPick and self._sfxDropLoot[2] then
		--LoggerHelper.Error("self._sfxDropLoot2"..self._sfxDropLoot[2])
		GameWorld.PlaySound(self._sfxDropLoot[2])
	end
    TimerHeap:DelTimer(self._timerId)
end

--玩家在掉落旁边的时候拾取
function SpaceDrop:_Checking()
	--此实体已删除，定时器不作处理
	if self.cs == nil then
		return
	end

	-- if bagData:GetPkg(public_config.PKG_TYPE_ITEM):IsFull() and self.itemId > public_config.MAX_MONEY_ID then
	-- 	return
	-- end
	-- GameWorld.Player():Teleport(Vector3.New(128,35,52))
	-- print(GameWorld.Player():GetPosition())
	local dis =	BaseUtil.DistanceByXZ(self:GetPosition(), GameWorld.Player():GetPosition())
	if dis <= self._range then
		if not self._picking then
			self._picking = true
			self:HandlePick()
		end
	else
		if self._picking then
			self._picking = false
		end
	end
end

function SpaceDrop:HandlePick()
	-- LoggerHelper.Error("self.ownDropId"..self.ownDropId.."self.index"..self.index)
	local player = GameWorld.Player()
	if ClientEntityManager.isConfigDropEntity(player, self) then
		CollectManager:PickSpaceDropImmediately(self.ownDropId,self.index)
	end
	--end
end

--自动拾取（废弃）
-- function SpaceDrop:AutoPick()
-- 	TimerHeap:DelTimer(self._autoPickTimerId)
-- 	--此实体已删除，定时器不作处理
-- 	if self.cs == nil then
-- 		return
-- 	end
-- 	local dis = Vector3.Distance(self:GetPosition(), GameWorld.Player():GetPosition())
-- 	if dis <= self._autoPickRange then
-- 		self:HandlePick()
-- 	end
-- end

--模型加载完之后检查是否被拾取
function SpaceDrop:OnModelLoaded()
	if not ClientEntityManager.CheckExcist(self.ownDropId,self.index) then
		self:Destroy()
	end
end

--只有这种方式才是本人拾取
function SpaceDrop:FlyToEntity()
	--LoggerHelper.Error("SpaceDrop:FlyToEntity")
	self.cs:FlyToEntity()
	self._isMyPick = true
end

-- function SpaceDrop:set_belong_eid(oldValue)
-- 	if self.belong_eid > 0 then
-- 		local entity = GameWorld.GetEntity(self.belong_eid)
-- 		if entity ~= nil then
-- 			--播放飞行特效
-- 		end 
-- 	end
-- end

function SpaceDrop:Destroy()
	--LoggerHelper.Error("SpaceDrop:Destroy = " .. self.id)
	GameWorld.DestroyEntity(self.id)
end