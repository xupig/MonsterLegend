require "Entities/Avatar"
local SkillShowAvatar = Class.SkillShowAvatar(ClassTypes.Creature)

local EntityConfig = GameConfig.EntityConfig
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local EntityType = GameConfig.EnumType.EntityType
local RoleDataHelper = GameDataHelper.RoleDataHelper
local Camera = UnityEngine.Camera
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath
local LightType = UnityEngine.LightType
-- local LightmapBakeType = UnityEngine.LightmapBakeType
local Light = UnityEngine.Light
local LayerMask = UnityEngine.LayerMask
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local RolePartType = GameConfig.EnumType.RolePartType
local RenderTextureFormat = UnityEngine.RenderTextureFormat
local PlayerModelType = GameConfig.EnumType.PlayerModelType

function SkillShowAvatar:__ctor__(csEntity)
	self.entityType = EntityType.SkillShowAvatar
    self.showBillboard = false
end

function SkillShowAvatar:OnEnterWorld()
	self._base.OnEnterWorld(self)
    GameWorld._showPlayer = self
    self.actorId = RoleDataHelper.GetModelByVocation(self.vocation)
	self:SetActor(self.actorId)
	self:SetCityState(false)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_CUR_HP, 100)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_MAX_HP, 100)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_BASE_SPEED, 6)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_SPEED, 6)
end

function SkillShowAvatar:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
	GameObject.Destroy(self.rttForCreateRoleGo)
	GameObject.Destroy(self.rttForCreateRoleGoHead)
    GameWorld._showPlayer = nil
end

function SkillShowAvatar:GetDefaultLayer()
	return GameObjectLayerConfig.ShowAvatar
end

function SkillShowAvatar:OnActorLoaded()
	self.actorHasLoaded = true
    self.skillPreview = GameObject.Find("SkillPreviewPool").transform
    self:CreateRttCamera()
	self:CreateHeadRttCamera()
	self:SyncShowPlayerFacade()
	self.cs:SetActorLayer(self:GetDefaultLayer(), true)
	self:SetActorMode(0)
	self:ShowModel(3)
end

function SkillShowAvatar:SyncShowPlayerFacade()
	self.cs:SyncShowPlayerFacade()
end

function SkillShowAvatar:SetSkillPreviewCamera(trans)
	self.cs:SetSkillPreviewCamera(trans)
end

function SkillShowAvatar:SetSkillPreviewCameraArgs(distance,rotationX,rotationY,rectX)
	self.cs:SetSkillPreviewCameraArgs(distance,rotationX,rotationY,rectX)
end

--创建RTT相机
function SkillShowAvatar:CreateRttCamera()
	self.cs:GetTransform().gameObject.layer = LayerMask.NameToLayer("Avatar")
	self.rttForCreateRoleGo = GameObject.New("RttForCreateRole")
	self.rttForCreateRoleGo.transform:SetParent(self.skillPreview)
	-- GameObject.DontDestroyOnLoad(self.rttForCreateRoleGo)
	self.RenderCamera = self.rttForCreateRoleGo:AddComponent(typeof(Camera))
	-- self.RenderCamera.renderingPath = RenderingPath.Forward--DeferredLighting --.DeferredShading
	self.RenderTexture = RenderTexture.New(640,360,16)
	self.RenderTexture.name = "SkillShowAvatar"
	--self.RenderTexture.format = RenderTextureFormat.RGB565
	self.RenderCamera.targetTexture = self.RenderTexture
	self.RenderCamera.backgroundColor = UnityEngine.Color.New(30/255,34/255,45/255,255/255)
	self.RenderCamera.fieldOfView = 20
	self:SetSkillPreviewCamera(self.rttForCreateRoleGo.transform)
	self:SetSkillPreviewCameraArgs(12, 10, 180, 0)
	--self:BuildLight()
end
--动态头像显示
function SkillShowAvatar:CreateHeadRttCamera()
	self.rttForCreateRoleGoHead = GameObject.New("RttForCreateRoleHead")
	self.rttForCreateRoleGoHead.transform:SetParent(self.skillPreview)
	-- GameObject.DontDestroyOnLoad(self.rttForCreateRoleGoHead)
	self.RenderCameraHead = self.rttForCreateRoleGoHead:AddComponent(typeof(Camera))
	-- self.RenderCameraHead.renderingPath = RenderingPath.Forward--DeferredLighting --.DeferredShading
	self.RenderTextureHead = RenderTexture.New(320,180,16)
	self.RenderTextureHead.name = "AvatarHead"
	self.RenderCameraHead.targetTexture = self.RenderTextureHead
	self.RenderCameraHead.backgroundColor = UnityEngine.Color.New(46/255,64/255,83/255,255/255)
	self.RenderCameraHead.fieldOfView = 20

	self.cs:SetHeadPreviewCamera(self.rttForCreateRoleGoHead.transform)
	self._bgColor = UnityEngine.Color.New(46/255,64/255,83/255,0/255)
	self.RenderCameraHead.backgroundColor = self._bgColor
	local params = GlobalParamsHelper.GetParamValue(75)[GameWorld.Player().vocation]
	self.cs:SetHeadPreviewCameraArgs(params[1],params[2],params[3],0,params[4],params[5])
end

-- function SkillShowAvatar:BuildLight()
-- 	local lightGo = GameObject.New("ShowDirectLight")
--     lightGo.transform:SetParent(self.skillPreview)
--     lightGo.transform.position = Vector3.New(-1000,0,-1000)
-- 	lightGo.transform.localEulerAngles = Vector3.New(164,1,1.3)
--     GameObject.DontDestroyOnLoad(lightGo)
-- 	local lightComponet = lightGo:AddComponent(typeof(Light))
-- 	lightComponet.type = LightType.Directional
-- 	-- lightComponet.lightmapBakeType = LightmapBakeType.Mixed
-- 	lightComponet.intensity = 1
-- 	lightComponet.cullingMask = math.pow(2,LayerMask.NameToLayer("ShowAvatar"))
-- end

--获取头像模型截图
function SkillShowAvatar:GetHeadPicture()
	self.textureHead = self.cs:GetHeadPicture(self.RenderTextureHead)
end

--mode 1:普通模型展示(静态头像) 2:技能模型展示 3:界面不展示模型(动态头像) 4:普通模型展示(不处理头像)
function SkillShowAvatar:ShowModel(mode)
	if mode == PlayerModelType.ModelAndStaticHead then
		self:SetActorMode(0)
		self:GetHeadPicture()
		EventDispatcher:TriggerEvent(GameEvents.Refresh_Head_Image, 1)
	elseif mode == PlayerModelType.SkillPreview then
		self:SetSkillPreviewCameraArgs(12, 10, 180, 0)
		self:SetActorMode(1)
		self:GetHeadPicture()
		EventDispatcher:TriggerEvent(GameEvents.Refresh_Head_Image, 1)
	elseif mode == PlayerModelType.DynamicHead then
		self:SetActorMode(0)
		self.cs:ResetSkillShowAvater()
		EventDispatcher:TriggerEvent(GameEvents.Refresh_Head_Image, 0)
	elseif mode == PlayerModelType.ModelAndNoHead then
		self:SetActorMode(0)
		EventDispatcher:TriggerEvent(GameEvents.Refresh_Head_Image, 0)
	end
end

function SkillShowAvatar:Show()
	local go = self.cs:GetGameObject()
	if go then
		go:SetActive(true)
	end
end

function SkillShowAvatar:Hide()
	local go = self.cs:GetGameObject()
	if go then
		go:SetActive(false)
	end
end