require "Entities/Avatar"
--Class.CreateRoleAvatar(Avatar)
local CreateRoleAvatar = Class.CreateRoleAvatar(ClassTypes.Avatar)

local EntityConfig = GameConfig.EntityConfig
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local Camera = UnityEngine.Camera
local GameObject = UnityEngine.GameObject
local RenderTexture = UnityEngine.RenderTexture
local RenderingPath = UnityEngine.RenderingPath
local LightType = UnityEngine.LightType
local Light = UnityEngine.Light
local LayerMask = UnityEngine.LayerMask
local EntityType = GameConfig.EnumType.EntityType
local RoleDataHelper = GameDataHelper.RoleDataHelper
local EquipModelType = GameConfig.EnumType.EquipModelType

function CreateRoleAvatar:__ctor__(csEntity)
	self.showBillboard = false
	self.entityType = EntityType.CreateRoleAvatar
	self.pet_eid = 0
end

function CreateRoleAvatar:OnEnterWorld()
	CreateRoleAvatar._base.OnEnterWorld(self)
	self:SetActor(self.actorId)
	self:SetCityState(false)
	self:Teleport(Vector3(0, 20, 0))
end

function CreateRoleAvatar:OnLeaveWorld()
	CreateRoleAvatar._base.OnLeaveWorld(self)
	CreateRoleManager:SetCamera()
end

function CreateRoleAvatar:GetDefaultLayer()
	return GameObjectLayerConfig.Actor
end

function CreateRoleAvatar:OnActorLoaded()
	self:Teleport(Vector3(95.2, 48.4, 38))
	self:SetFace(180)
	if self.createRoleType == 0 then
		CreateRoleManager:SetCameraAction()
	else
		CreateRoleManager:SetCamera()
	end

	self.cs:GetTransform().gameObject.layer = LayerMask.NameToLayer("Avatar")
	local selected = CreateRoleManager.CurrentSelectedCharacter
	-- 衣服、武器、头部、翅膀
	local equipData = RoleDataHelper.GetCreateRoleEquipByVocation(self.vocation)
	if CreateRoleManager.HeadId[selected]==nil then			
		local headCfg = equipData[EquipModelType.Head]
		CreateRoleManager.HeadId[selected] = {headCfg[1], headCfg[2], headCfg[3], 0, 0, 0}
	end
	if CreateRoleManager.ClothId[selected]==nil then
		local clothCfg = equipData[EquipModelType.Cloth]
		CreateRoleManager.ClothId[selected] = {clothCfg[1], clothCfg[2], clothCfg[3], 0, 0, 0}
	end
	if CreateRoleManager.WeaponId[selected]==nil then
		local weaponCfg = equipData[EquipModelType.Weapon]
		CreateRoleManager.WeaponId[selected] = {weaponCfg[1], weaponCfg[2],weaponCfg[3], 0, 0, 0}
	end
	if CreateRoleManager.WingId[selected]==nil then
		local wingCfg = equipData[EquipModelType.Wing]
		CreateRoleManager.WingId[selected] = {wingCfg[1], wingCfg[2], wingCfg[3], 0, 0, 0}
	end
	
	--self:Teleport(Vector3(95.2, 48.4, 38))
	-- self:SetFace(180)
	self.actorHasLoaded = true
	local headData = CreateRoleManager.HeadId[selected]
	local clothData = CreateRoleManager.ClothId[selected]
	local weaponData = CreateRoleManager.WeaponId[selected]
	local WingData = CreateRoleManager.WingId[selected]
	self:AddEquip(headData[1],headData[2],headData[3],headData[4],headData[5],headData[6])
	self:AddEquip(clothData[1],clothData[2],clothData[3],clothData[4],clothData[5],clothData[6])
	self:AddEquip(weaponData[1],weaponData[2],weaponData[3],weaponData[4],weaponData[5],weaponData[6])
	self:AddEquip(WingData[1],WingData[2],WingData[3],WingData[4],WingData[5],WingData[6])

	if CreateRoleManager.buildSceneFinish ~= nil then
		CreateRoleManager.buildSceneFinish()
	end
end

function CreateRoleAvatar:OnControllerLoaded()
	GameWorld.TimerHeap:AddSecTimer(0.3,0,1,function()
		if self.cs ~= nil then
			self:SetReady(true)
			local skill = RoleDataHelper.GetRoleBornSkillByVocation(self.vocation)
			if skill ~= 0 then
				self:PlaySkillByCreateRoleAvatar(skill)
			end
		end
	end)
end