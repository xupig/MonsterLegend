--技能生成陷阱类型
require "Entities/Creature"

local Trap = Class.Trap(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local SpawnType = GameConfig.EnumType.SpawnType
local SpawnEntityDataHelper = GameDataHelper.SpawnEntityDataHelper
local EntityType = GameConfig.EnumType.EntityType

function Trap:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.Trap
end

function Trap:OnEnterWorld()
    Trap._base.OnEnterWorld(self)
    --处理客户端本生成逻辑
    if self.cs.spawnObjectId ~= 0 then
        self.cfg_id = self.cs.spawnObjectId
    end
    self.model = SpawnEntityDataHelper.GetTrapModelId(self.cfg_id)
    self:SetActor(self.model)
    self:SetCityState(false)
end

function Trap:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function Trap:OnActorLoaded()
end

function Trap:GetDefaultLayer()   
    return GameObjectLayerConfig.Ghost
end