require "Entities/Creature"
local Collect = Class.Collect(ClassTypes.Creature)
local CollectManager = GameManager.CollectManager
local TimerHeap = GameWorld.TimerHeap
local EntityType = GameConfig.EnumType.EntityType
local CollectDataHelper  = GameDataHelper.CollectDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MarriageManager = PlayerManager.MarriageManager
local BaseUtil = GameUtil.BaseUtil

function Collect:__ctor__(entityid)
	self.entityType = EntityType.Collect
	self:SetDefaultLayer(GameObjectLayerConfig.Trigger)
	self.showBillboard = true
end

function Collect:OnEnterWorld()
	self._cfg = CollectDataHelper.GetCollectCfg(self.collect_id)
	if self._cfg.level and self._cfg.level > 0 then
		self.name = LanguageDataHelper.CreateContent(self._cfg.name).."("..self._cfg.level..LanguageDataHelper.CreateContent(69)..")"
	else
		self.name = LanguageDataHelper.CreateContent(self._cfg.name)
	end
	self._base.OnEnterWorld(self)
	
	self._timer = TimerHeap:AddSecTimer(0, 0.3, 0, function() self:TestPlayer() end)
	self._playerTransform = GameWorld.Player().cs:GetTransform()
	self._triggered = false --已触发
	self.cs:SetActor(self._cfg.model_id)
	self._range = GlobalParamsHelper.GetParamValue(345)
end

function Collect:TestPlayer()
	local p = self._playerTransform.position
    local len = BaseUtil.DistanceByXZ(p, self:GetPosition())
	if len < self._range then
		if not self._triggered then
	        self._triggered = true
	        --结婚场景判断
	        -- if GameManager.GameSceneManager:GetCurSceneType() == GameConfig.SceneConfig.SCENE_TYPE_WEDDING and
	        -- 	not MarriageManager:GetCanFood() then
	        -- 	return
	        -- end
	        --self:PlaySkillByCreateRoleAvatar(2417)
			CollectManager:AddCollectItem(self.id,self._cfg)
		end
	else
		if self._triggered then
			CollectManager:RemoveCollectItem(self.id)
			self._triggered = false
		end
	end
end

function Collect:PlaySkillByCreateRoleAvatar(skillId)
	self.cs:PlaySkillByCreateRoleAvatar(skillId)
end

function Collect:OnActorLoaded()
    self.cs:SetSelectTrigger()
    local scale = self._cfg.collected_scale
    if scale and scale > 0 then
    	self.cs:InitActorScale(scale)
    end
end

function Collect:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
	CollectManager:RemoveCollectItem(self.id)
    TimerHeap:DelTimer(self._timer)
end

function Collect:OnDeath()

end

function Collect:OnTouchEntity()
	--LoggerHelper.Error("Collect:OnTouchEntity")

	local cd = self._cfg.collect_time*1000
	local pos = self:GetPosition()
	EventDispatcher:TriggerEvent(GameEvents.PlayerCommandBackFindPositionAndCollect,pos, self.id,cd)
end
