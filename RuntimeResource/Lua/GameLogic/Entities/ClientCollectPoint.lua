--客户端创建的采集点（用于任务）
require "Entities/Avatar"
local ClientCollectPoint = Class.ClientCollectPoint(ClassTypes.Entity)
local GameSceneManager = GameManager.GameSceneManager
local TimerHeap = GameWorld.TimerHeap
local EntityType = GameConfig.EnumType.EntityType
local CollectManager  = GameManager.CollectManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local MarriageManager = PlayerManager.MarriageManager
local BaseUtil = GameUtil.BaseUtil

function ClientCollectPoint:__ctor__(csEntity)
	self.entityType = EntityType.ClienntCollectPoint
	self.actionId = 0
	self.cdTime = 5
	self.taskEventId = 0
	self.destroyAfterCollect = 1
	self.collectName = 0
	self.collectIcon = 0
	self.collectActionArgs = nil
end

function ClientCollectPoint:OnEnterWorld()
	--LoggerHelper.Error("ClientCollectPoint:__ctor__"..self.id.."/"..self.cdTime.."/"..self.taskEventId)
	self._base.OnEnterWorld(self)
	local cfg = GameSceneManager:GetCfgEntity(self.actionId)
	--self.cs:CreateTempCube(cfg.pos)
	--self.name = "ClientCollectPoint"
	self._timer = TimerHeap:AddSecTimer(0, 1, 0, function() self:TestPlayer() end)
	
	self._pos = cfg.pos
	self._triggered = false --已触发
	self.cs:SetActor(cfg.model_id)
	self._range = GlobalParamsHelper.GetParamValue(345)
end

function ClientCollectPoint:TestPlayer()
	if self._playerTransform == nil then
		self._playerTransform = GameWorld.Player().cs:GetTransform()
	end

	if self._playerTransform == nil then
		LoggerHelper.Error("ClientCollectPoint:TestPlayer")
	end
	
	local p = self._playerTransform.position
    local len = BaseUtil.DistanceByXZ(p, self._pos)
    --LoggerHelper.Error("ClientCollectPoint:TestPlayer"..len)
	if len < self._range then
		if not self._triggered then
			--公会篝火判断是否显示采集
			if GameManager.GameSceneManager:GetCurSceneType() == GameConfig.SceneConfig.SCENE_TYPE_GUILD_BANQUET and 
				not PlayerManager.GuildBanquetManager:CanGetItem() then
				return
			end
			--结婚场景判断品尝美食
	        if GameManager.GameSceneManager:GetCurSceneType() == GameConfig.SceneConfig.SCENE_TYPE_WEDDING and
	        	not MarriageManager:GetCanFood() then
	        	return
	        end
			self._triggered = true
			CollectManager:AddClientCollectEntity(self.id,self.actionId,self.cdTime,self.taskEventId,self.destroyAfterCollect,self.collectIcon,self.collectName,self.collectActionArgs)
		end
	else
		if self._triggered then
			CollectManager:RemoveClientCollectEntity(self.actionId)
			self._triggered = false
		end
	end
end

function ClientCollectPoint:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
	CollectManager:RemoveClientCollectEntity(self.actionId)
    TimerHeap:DelTimer(self._timer)
end

function ClientCollectPoint:Destroy()
	GameWorld.DestroyEntity(self.id)
end

function ClientCollectPoint:OnActorLoaded()

end

function ClientCollectPoint:OnDeath()

end