--一堆掉落物
local DropItem = Class.DropItem(ClassTypes.Entity)
local ClientEntityManager = GameManager.ClientEntityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TaskManager = PlayerManager.TaskManager
local ItemDataHelper = GameDataHelper.ItemDataHelper

function DropItem:__ctor__(csEntity)
	--转职任务道具处理
	self._vocationTaskItems = GlobalParamsHelper.GetParamValue(493)
end

function DropItem:OnEnterWorld()
	DropItem._base.OnEnterWorld(self)
	self.itemList = GameWorld.CSLuaStringToLuaTable(self.str_items, "DropItem:OnEnterWorld()")
	--LoggerHelper.Log("DropItem:OnEnterWorld()")
	--LoggerHelper.Log(self.itemList)
	self:CreateItems()
end

function DropItem:OnLeaveWorld()
	DropItem._base.OnLeaveWorld(self)
	ClientEntityManager.DestroyDropItem(self.id,0)
	self.itemList = nil
end

function DropItem:CreateItems()
	for index,data in pairs(self.itemList) do
		--data可能为空，被别人捡过
		if data then
			local itemId
			local itemCount
			for k,v in pairs(data) do
				itemId = k
				itemCount = v
			end

			local shouldCreate = true
			--转职任务道具未获得任务/不属于自己不生成
			local vTaskId = self._vocationTaskItems[itemId]
			if vTaskId then
				if TaskManager:GetTaskById(vTaskId) == nil or self.owners[GameWorld.Player().id] == nil then
					GameWorld.Player().server.del_drop_item(self.id,index)
					shouldCreate = false
				end
			end
			--每日掉落限制
			local limitCount = ItemDataHelper:GetItemDayDropLimit(itemId)
			if limitCount then
				if limitCount <= (GameWorld.Player().drop_times_by_item[itemId] or 0) then
					shouldCreate = false
				end
			end
			-- data.itemId = itemId
			-- data.itemCount = itemCount
			if shouldCreate then
				ClientEntityManager.CreateSpaceDropByClient(self.id,index,itemId,itemCount,self:GetPosition())
			end
		end
	end
end

--str_items发生变化时
--用于其他人拾取后删除
function DropItem:set_str_items(change)
	--初始化的时候不处理
	if self.itemList == nil then
		return
	end
	local t = GameWorld.CSLuaStringToLuaTable(self.str_items, "DropItem:set_str_items")
	for k,v in pairs(self.itemList) do
		if t[k] == nil then
			--LoggerHelper.Error("DropItem"..k)
			ClientEntityManager.DestroySpaceDrop(self.id,k,0)
		end
	end
	self.itemList = t
end

function DropItem:GetPosition()
    return self.cs.position
end
