local EntityAI = Class.EntityAI(ClassTypes.XObject)

local math = math
local Time = Time
local Vector2 = Vector2
local Vector3 = Vector3

function EntityAI:__ctor__(theEntity)
    self._theEntity = theEntity or {}
    GameWorld.Update:Add(self.Update,self)
    self._time = Time.realtimeSinceStartup
    self._canThink = false
    self._cs = theEntity.cs
    self._targetEntity = nil
    self._markSet = {}
    self._isSleepState = false
    self._sleepTime = 0
    self._cursleepTime = 0
    self._selfPosV2 = Vector2.New(0,0)
    self._targetPosV2 = Vector2.New(0,0)
    self._randomTime = 0
    self._isMoveToTarget = false
    self._moveRange = 0
    self._probabilityArray = {0,0,0,0}
    self._isLookOn = false
    self._lookOnMode = 0
    self._lookOnTime = 0
    self._lastLookOnTime = 0.1
    self._minLookOnDist = 0
    self._maxLookOnDist = 0
    self._bornPos = Vector3.New(0,0,0)
    self._pointPos = Vector3.New(0,0,0)
end

function EntityAI:SetBT(btNumber)
    if btNumber == 0 then
        return
    end
    --local bt = GameWorld.btTable[btNumber]
    if bt == nil then 
        LoggerHelper.Error("the BT number is not exist! Check it!")
        return
    end
    self._theBTRoot = bt
end

function EntityAI:SetBornPos(pos)
    self._bornPos = pos
end

function EntityAI:Proc()
    if self._theBTRoot == nil then
        return
    end
    self._theBTRoot:Proc(self)
end

function EntityAI:Think()  
    self:Proc()
end

function EntityAI:SetThinkState(state)
    self._canThink = state
end

function EntityAI:GetThinkState()
    return self._canThink
end

function EntityAI:Update()
    if self._canThink == false then
        return
    end
    
    if self._isMoveToTarget and self._targetEntity ~= nil then
        if Vector3.Distance(self._targetEntity:GetPosition(),self._theEntity:GetPosition()) < self._moveRange then
            self._theEntity:StopMove()
            self._isMoveToTarget = false
        end
    end

    if Time.realtimeSinceStartup - self._time > 0.5 then
        self._time = Time.realtimeSinceStartup
        if Time.realtimeSinceStartup - self._randomTime > 5 then
            --更新随机种子
            math.randomseed(tostring(os.time()):reverse():sub(1,6))
            self._randomTime = Time.realtimeSinceStartup
        end

        if self._isSleepState then
            if Time.realtimeSinceStartup - self._cursleepTime > self._sleepTime then
                self._isSleepState = false
            end
            return
        end
        self:Think()
    end

    if self._isLookOn and (Time.realtimeSinceStartup > self._lastLookOnTime) then
        self._lastLookOnTime = self._lastLookOnTime + 0.1
        self:ProcessLookOnEnd()
    end
end

function EntityAI:GetHorizontalDistance()
    self._selfPosV2.x = self._theEntity:GetPosition().x
    self._selfPosV2.y = self._theEntity:GetPosition().z
    self._targetPosV2.x = self._targetEntity:GetPosition().x
    self._targetPosV2.y = self._targetEntity:GetPosition().z
    local distance = Vector2.Distance(self._selfPosV2,self._targetPosV2)
    return distance
end

function EntityAI:GetVerticalDistance()
    local distance = math.abs(self._theEntity:GetPosition().y - self._targetEntity:GetPosition().y)
    return distance
end

function EntityAI:GetNumberOfEnemyInRange(range)
    local number = 0
    for k,v in pairs(GameWorld.Entities()) do
        if Vector3.Distance(self._theEntity:GetPosition(),v:GetPosition()) <= range then
            number = number + 1
        end
    end
    return number
end

function EntityAI:SearchTarget(distance,targetType)
    local target = nil
    --[[if self._targetEntity == nil then
        for k,v in pairs(GameWorld.Entities()) do
            if k == self._theEntity.id then
            --continue
            else
                if Vector3.Distance(self._theEntity:GetPosition(),v:GetPosition()) <= distance * 0.01 then
                    target = v
                end
            end
        end
    end]]
    --目前处理为怪物只搜索主角
    if self._targetEntity == nil and GameWorld.Player().cs.cur_hp ~= 0 then
        if Vector3.Distance(self._theEntity:GetPosition(),GameWorld.Player():GetPosition()) <= distance * 0.01 then
            target = GameWorld.Player()
        end
    end
    return target
end

--行为节点的具体实现
function EntityAI:ProcDoTest1()
    local LuaFacade = GameMain.LuaFacade
    local thePlayer = GameWorld._player
    local position = thePlayer.cs:GetTransform().position
    self._theEntity:Move(position,6)
end

function EntityAI:ProcDoTest2()
    --LoggerHelper.Log("ProcDoTest2")
end

function EntityAI:ProcDoTest3()
    LoggerHelper.Log("ProcDoTest3")
end

function EntityAI:ProcAddMark(markId)
    if self._markSet[markId] ~= nil then return end
    self._markSet[markId] = 1
end

function EntityAI:ProcDeleteMark(markId)
    if markId == 0 then
        for k,v in pairs(self._markSet) do
            self._markSet[k] = nil
        end
    else
        if self._markSet[markId] ~= nil then
            self._markSet[markId] = nil
        end
    end
end

function EntityAI:ProcSleep(stopTime)
    self._isSleepState = true
    self._cursleepTime = Time.realtimeSinceStartup
    self._sleepTime = stopTime * 0.001
end

function EntityAI:ProcMoveToTarget(distance)
    if not self:ProcCanMove() then
        return
    end
    distance = distance * 0.01
    if Vector3.Distance(self._targetEntity:GetPosition(),self._theEntity:GetPosition()) < distance then
        return
    end
    self._isMoveToTarget = true
    self._moveRange = distance
    self._theEntity:Move(self._targetEntity:GetPosition(),self._theEntity.cs.speed * 0.01)
end

function EntityAI:ProcSearchTarget(distance,targetType,reSearchProbability)
    local target = nil
    if self._targetEntity == nil then
        target = self:SearchTarget(distance,targetType)
    else
        local random = self:Random(0,100)
        if random <= reSearchProbability then
            target = self:SearchTarget(distance,targetType)
        end
    end
    self._targetEntity = target
    return (target ~= nil)
end

function EntityAI:ProcCastSpell(skillPos)
    self._theEntity:PlaySkillByPos(skillPos)
end

function EntityAI:ProcLookOn(maxDistance,minDistance,probabilityArray,timeArray,stopTime)
    --LoggerHelper.Error("ProcLookOn")
    if not self:ProcCanMove() then
        return
    end
    --方向顺序为前后左右
    if self._isLookOn then
        return
    end

    for i=1,#self._probabilityArray do
        self._probabilityArray[i] = probabilityArray[i]
    end
    self._minLookOnDist = minDistance
    self._maxLookOnDist = maxDistance
    local distance = Vector3.Distance(self._theEntity:GetPosition(),self._targetEntity:GetPosition())
    if distance < minDistance then
        self._probabilityArray[1] = 0
        self._probabilityArray[2] = 100
    end
    if distance > maxDistance then
        self._probabilityArray[1] = 100
        self._probabilityArray[2] = 0
    end

    local random = self:Random(0,100)
    if random <= self._probabilityArray[1] then
        self:SetLookOnState(1,timeArray[1])
        self._theEntity:MoveByDirection(self._targetEntity.id,self._theEntity.cs.speed * 0.01,timeArray[1],false)
    elseif random <= self._probabilityArray[1] + self._probabilityArray[2] then
        self:SetLookOnState(2,timeArray[2])        
        self._theEntity:MoveByDirection(self._targetEntity.id,self._theEntity.cs.speed * -0.01,timeArray[2],false)
    elseif random <= self._probabilityArray[1] + self._probabilityArray[2] + self._probabilityArray[3] then
        self:SetLookOnState(3,timeArray[3])
        self._theEntity:MoveByDirection(self._targetEntity.id,self._theEntity.cs.speed * -0.01,timeArray[3],true)
    elseif random <= self._probabilityArray[1] + self._probabilityArray[2] + self._probabilityArray[3] + self._probabilityArray[4] then
        self:SetLookOnState(4,timeArray[4])
        self._theEntity:MoveByDirection(self._targetEntity.id,self._theEntity.cs.speed * 0.01,timeArray[4],true)
    else
        self:ProcSleep(stopTime)
    end
end

function EntityAI:SetLookOnState(mode,time)
    self._isLookOn = true
    self._lookOnTime = time
    self._lookOnMode = mode
end

function EntityAI:ProcessLookOnEnd()
    if self._targetEntity == nil then
        self._theEntity:StopMove()
        self._isLookOn = false
        return
    end
    local distance = Vector3.Distance(self._theEntity:GetPosition(),self._targetEntity:GetPosition())
    if self._lookOnMode == 1 or self._lookOnMode == 2 then
        if distance > self._maxLookOnDist or distance < self._minLookOnDist then
            self._theEntity:StopMove()
            self._isLookOn = false
        end 
    end

    if self._lookOnTime <= 0 then
        self._theEntity:StopMove()
        self._isLookOn = false
    end
    self._lookOnTime = self._lookOnTime - 0.1
end

function EntityAI:ProcRunaway(distance)
    --LoggerHelper.Error("ProcRunaway")
    if not self:ProcCanMove() then
        return
    end
    local dir = self._theEntity:GetPosition() - self._targetEntity:GetPosition()
    dir.y = 0
    dir = Vector3.Normalize(dir)
    dir = dir * (distance * 0.01)
    local targetPos = self._theEntity:GetPosition() + dir
    self._theEntity:Move(targetPos,self._theEntity.cs.speed * 0.01)
end

function EntityAI:ProcPatrol(radius)
    --LoggerHelper.Error("ProcPatrol")
    if not self:ProcCanMove() then
        return
    end
    local pos = Vector3(0,0,0)
    local x = self:Random(radius * -1,radius)
    local z = self:Random(radius * -1,radius)
    --local curPos = self._theEntity:GetPosition()
    local curPos = self._bornPos
    pos.x = x * 0.01 + curPos.x
    pos.y = curPos.y
    pos.z = z * 0.01 + curPos.z
    self._theEntity:Move(pos,self._theEntity.cs.speed * 0.01)
end

function EntityAI:ProcDeleteTarget()
    self._targetEntity = nil
end

function EntityAI:ProcAddBuff(buffId)
    self._theEntity:AddBuff(buffId)
end

function EntityAI:ProcDeleteBuff(buffId)
    self._theEntity:DeleteBuff(buffId)
end

function EntityAI:ProcMoveToPoint(x,z,y)
    if not self:ProcCanMove() then
        return
    end
    self._pointPos.x = x
    self._pointPos.y = y
    self._pointPos.z = z
    self._theEntity:Move(self._pointPos,self._theEntity.cs.speed * 0.01)
end

--条件节点的具体实现
function EntityAI:ProcIsSmallerCondition(value)   
    local thePlayer = GameWorld._player
    if not thePlayer.actorHasLoaded or not self._theEntity.actorHasLoaded then
        return true
    end
    local position = thePlayer.cs:GetTransform().position
    local playerPos = Vector2(position.x,position.z)
    local selfPosition = self._theEntity.cs:GetTransform().position
    local selfPos = Vector2(selfPosition.x,selfPosition.z)
    if Vector2.Distance(selfPos,playerPos) < value then
        return true
    end
    return false
end

function EntityAI:Random(min,max)   
    return math.random(min,max)
end

function EntityAI:GetCurHPRate()
    return self._theEntity:GetCurHPRate()
end

function EntityAI:GetTargetHPRate()
    return self._targetEntity:GetCurHPRate()
end

function EntityAI:ProcCanMove()
    return self._theEntity.cs:CanActiveMove()
end

function EntityAI:ProcCanCastSpell()
    return self._theEntity:CanAttack()
end

function EntityAI:ProcHasTarget()
    if self._targetEntity ~= nil and self._targetEntity.cs.cur_hp ~= 0 then
        return true
    end
    return false
end

function EntityAI:ProcInSky()
    return self._theEntity:IsOnAir()
end

function EntityAI:ProcInSkillCoolDown(skillPos)
    return self._theEntity:SkillCanPlayByPos(skillPos)
end

function EntityAI:ProcInSkillRange(skillPos)
    local range = self._theEntity:GetSkillAIRangeByPos(skillPos) * 0.01
    local distance = Vector3.Distance(self._theEntity:GetPosition(),self._targetEntity:GetPosition())
    return range >= distance
end

function EntityAI:ProcCheckSelfBuffId(buffId)
    return self._theEntity:ContainsBuffer(buffId)
end

function EntityAI:ProcCheckTargetBuffId(buffId)
    return self._targetEntity:ContainsBuffer(buffId)
end

function EntityAI:ProcCheckSelfMarkId(markId)
    if self._markSet[markId] ~= nil then
        return true
    end
    return false
end