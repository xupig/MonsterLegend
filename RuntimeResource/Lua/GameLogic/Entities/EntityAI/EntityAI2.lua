local ai_utils = require "AI/ai_utils"
local mgr_entity_ai_cell = require "AI/mgr_entity_ai_cell"
local mgr_entity_aggro_cell = require "AI/mgr_entity_aggro_cell"



local EntityAI2 = Class.EntityAI2(ClassTypes.XObject)
local EntityType = GameConfig.EnumType.EntityType


function EntityAI2:__ctor__(theEntity)
    self.theEntity = theEntity or {}

    self:Reset()
    self._timerID = 0
end

function EntityAI2:Reset(theEntity)
    self.canThink = true
    self.lastThinkTime = 0
    self.born_pos = nil
    self.cur_ai_id = 0
    self.one_tick_cost_ms = 100
    self.think_imm_flag = 0
    self.think_imm_root = 0
    self.casting = 0
    self.hang_up_setting = {}
    

    mgr_entity_ai_cell:init_entity_data(self)
end

function EntityAI2:OnEnterSpace()
    self:Reset()
end

function EntityAI2:OnLeaveSpace()
    self:Reset()
end

function EntityAI2:StartAI(ai_id)

    local owner_entity = self:GetOwner()

    if ai_id <= 0 then
        return
    end

    if not self.born_pos then
        self.born_pos = self.theEntity:GetPosition()
    end

    --local ai_tick_delay_cost_sec = self:GetOwner().ai_tick_delay_cost_sec
    --self:set_one_tick_ms(ai_tick_delay_cost_sec * 1000)

    --LoggerHelper.Error("-------EntityAI2:start_ai------"..tostring(ai_id))

    mgr_entity_ai_cell:init_entity_data(self)

    if not mgr_entity_ai_cell:start_ai(self, ai_id) then
        return
    end

    if self._timerID == 0 then
        self._timerID = TimerHeap:AddSecTimer(0, 0.1, 0, slot(self.Update, self))
    end
end

function EntityAI2:ReStartMainAI()
    local owner_entity = self:GetOwner()
    local ai_id = owner_entity.ai_id

    if ai_id <= 0 then
        return
    end

    if not mgr_entity_ai_cell:start_ai(self, ai_id) then
        return
    end

end

-- 遍历完成后调用
function EntityAI2:OnLoopEnd()
    mgr_entity_ai_cell:on_loop_end(self)
end

function EntityAI2:Update()
    if not GameWorld.aiCanThink then
        return
    end

    local owner_entity = self:GetOwner()
    if not self.canThink or owner_entity:CanThink() > 0 then

        return
    end


    local imm_flag = 0
    if self.casting == 1  then
        self.casting = 0
        imm_flag = 1
    end


    local ai_tick_delay_cost_sec = self.one_tick_cost_ms /1000

    if (Time.realtimeSinceStartup - self.lastThinkTime >= ai_tick_delay_cost_sec) or (self:get_think_imm_flag() == 1) or (imm_flag == 1) then
       --- LoggerHelper.Error("-------EntityAI2:update5555------")
        self.lastThinkTime = Time.realtimeSinceStartup
        self.casting = 0
        self:set_think_imm_flag(0)
        self:set_imm_root_flag(0)
        mgr_entity_ai_cell:update(self)
    end


end


function EntityAI2:set_one_tick_ms(value)
    self.one_tick_cost_ms = value
end

function EntityAI2:one_tick_ms()
    return self.one_tick_cost_ms
end

function EntityAI2:GetOwner()
    return self.theEntity
end

function EntityAI2:OnDeath()

    TimerHeap:DelTimer(self._timerID)
end

function EntityAI2:OnDestroy()

    TimerHeap:DelTimer(self._timerID)
end


function EntityAI2:SetThinkState(state)
    self.canThink = state
end


function EntityAI2:GetThinkState()
    return self.canThink
end


function EntityAI2:ExtendThinkTime(increase)
    self.lastThinkTime = Time.realtimeSinceStartup + increase
end

function EntityAI2:on_change_hp(casterId, delta, attackType)--delta<0就是伤害

    local owner_entity = self:GetOwner()

    if owner_entity.entityType == EntityType.Pet then 
        print("EntityAI2:on_change_hp ",casterId, delta, attackType)
    end
    if delta >= 0 then
        --不是扣血
        return
    end

    delta = delta*-1

    mgr_entity_aggro_cell:add_threat(self, casterId, delta, ai_utils.THREAT_REASON_HP_CHANGE)

    --local is_be_attack = true
    --if mgr_entity_ai_cell:need_block(self, is_be_attack, attackType) then
        --local owner_entity = self:GetOwner()
        --mgr_entity_ai_cell:start_ai(self, owner_entity.block_ai_args[1])
    --end

end


function EntityAI2:get_think_imm_flag()
    return self.think_imm_flag
end

function EntityAI2:set_think_imm_flag(new_value, reason)
    self.think_imm_flag = new_value
end

function EntityAI2:get_imm_root_flag()
    return self.think_root_flag
end

function EntityAI2:set_imm_root_flag(new_value, reason)
    self.think_root_flag = new_value
end

function EntityAI2:set_hang_up_setting(value)
    self.hang_up_setting = value or {}
end