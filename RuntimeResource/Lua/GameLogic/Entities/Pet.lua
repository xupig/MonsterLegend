--宠物类型
require "Entities/Creature"


local ai_utils = require "AI/ai_utils"
local Pet = Class.Pet(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local EntityType = GameConfig.EnumType.EntityType
local TransformDataHelper = GameDataHelper.TransformDataHelper

local ONE_ASSIST_CAST_MS = 1

function Pet:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.Pet
    self.isShow = true
    self.isInit = false
    self.isEnter = false
end

function Pet:OnEnterWorld()
    self.isEnter = true
    self._base.OnEnterWorld(self)
    if GameWorld.Player() ~= nil and GameWorld.Player().pet_eid ~= nil then
        if GameWorld.Player().pet_eid == self.id then
            GameWorld._playerPet = self
        end
    end
    
    self.owner_last_spell = 0
    self.owner_last_spell_time = 0
    
    self:SetLearnedSkillList({self.skill})
    self:SetCityState(false)
end

function Pet:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function Pet:OnActorLoaded()
    self:SetBehaviorTree(self.ai_id)
    self:SetThinkState(true)
    self.isInit = true
    self:SetVisible(self.isShow)
end

function Pet:GetDefaultLayer()
    return GameObjectLayerConfig.Pet
end

function Pet:GetOwnerId()
    return self.cs.owner_id
end

function Pet:GetSpeed()
    return self.speed * 0.01
end

function Pet:set_delegate_eid()
    self.cs.delegateEid = self.delegate_eid
end

function Pet:set_model_id()
    self.isInit = false
    self.ai_id = TransformDataHelper:GetPetAndTreasureAI(self.model_id)
    self.skill = TransformDataHelper:GetPetAndTreasureSkill(self.model_id)
    if self.isEnter then
        self:SetLearnedSkillList({self.skill})
    end
    local model = TransformDataHelper:GetPetAndTreasureModel(self.model_id)
    if model then
        self:SetActor(model)
    end
end

--<!-- 0：隐藏 1：显示-->
function Pet:set_show_state()
    if self.isInit then
        if self.show_state > 0 then
            self.isShow = true
            self:SetVisible(self.isShow)
        else
            self.isShow = false
            self:SetVisible(self.isShow)
        end
    end
end
