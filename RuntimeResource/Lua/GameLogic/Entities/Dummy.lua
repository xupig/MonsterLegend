require "Entities/Avatar"
local Dummy = Class.Dummy(ClassTypes.Creature)
local MonsterDataHelper = GameDataHelper.MonsterDataHelper

local EntityConfig = GameConfig.EntityConfig
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local EntityType = GameConfig.EnumType.EntityType
local DramaTriggerType = require "GameManager/DramaSystem/DramaTriggerType"
local attri_config = GameWorld.attri_config
local MonsterType = GameConfig.EnumType.MonsterType

function Dummy:__ctor__(csEntity)
	self.entityType = EntityType.Dummy
end

function Dummy:OnEnterWorld()
	self.name = MonsterDataHelper.GetMonsterName(self.monster_id)
	self.level = MonsterDataHelper.GetLv(self.monster_id)
	self._base.OnEnterWorld(self)
	self:InitLocalData()
end

function Dummy:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
end

function Dummy:InitLocalData()
	self:HasActorDeathController(true)
	self.cs.clientCamp = 1
    self.scale = MonsterDataHelper.GetMonsterScale(self.monster_id)
	self.cs.monsterId = self.monster_id
    self.monsterType = MonsterDataHelper.GetMonsterType(self.monster_id)

	self.model_low = MonsterDataHelper.GetMonsterModel(self.monster_id)
	self.ai_id = MonsterDataHelper.GetMonsterAI(self.monster_id)
	self.build_type = MonsterDataHelper.GetMonsterBuildType(self.monster_id)
	self.level =  MonsterDataHelper.GetMonster(self.monster_id).lv
	self.off_play_args = MonsterDataHelper.GetMonsterOffPlayArgs(self.monster_id)

	self:SetLearnedSkillList(MonsterDataHelper.GetMonsterSkills(self.monster_id))
	local speed = MonsterDataHelper.GetMonsterSpeed(self.monster_id)
	self:SetActor(self.model_low)
	self.cs:SetCreatureType(2)--指定类型为怪物2
	self:SetCityState(false)
	local hp = MonsterDataHelper.GetMonsterHP(self.monster_id)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_CUR_HP, hp)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_MAX_HP, hp)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_BASE_SPEED, speed)
	GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_SPEED, speed)
	local attributes = MonsterDataHelper.GetMonsterAttris(self.monster_id)
	self:SetBattleAttributes(attributes)
	self:SetBattleAttribute(attri_config.ATTRI_ID_LEVEL, self.level)
end

function Dummy:GetDefaultLayer()
	return GameObjectLayerConfig.Monster
end

function Dummy:OnActorLoaded()
	self.actorHasLoaded = true
	self:AddBornBuff()
	--self:SetBehaviorTree(self.ai_id)
	if self.drama_id ~= nil then
		GameManager.DramaManager:Trigger(DramaTriggerType.CreateDramaDummy, tostring(self.drama_id))
	else
		self:SetBehaviorTree(self.ai_id)
	end
	--初始模型缩放设置
    self.cs:InitActorScale(self.scale)
	self:IdleToReady()
end

function Dummy:OnDeath()
	EventDispatcher:TriggerEvent(GameEvents.ActionMonsterDeath, self.monster_id, self.spawn_id)
	--客户端本
    --if GameManager.GUIManager.InServerIns() == false then
    EventDispatcher:TriggerEvent(GameEvents.CheckClientMonsterDeath, self.monster_id, self:GetPosition())
    --end
	self._base.OnDeath(self)
end

function Dummy:AddBornBuff()
	local buffs = MonsterDataHelper.GetMonsterBuffs(self.monster_id)
	if buffs == nil then return end
	local percent = math.random()
	for k, v in pairs(buffs) do
		local bid = k
		local p = v[1] * 0.0001
		local time = v[2]
		if p == 1 or p <= percent then
            self:AddBuff(bid, time)
        end
	end
end

function Dummy:IsBoss()
	return MonsterType.IsBoss(self.monsterType)
end
