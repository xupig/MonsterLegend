require "Entities/EntityAI/EntityAI2"
require "GameManager/StateManager"
local Creature = Class.Creature(ClassTypes.Entity)
local EntityAI = ClassTypes.EntityAI2
local EntityConfig = GameConfig.EntityConfig
local BaseUtil = GameUtil.BaseUtil
local StateManager = ClassTypes.StateManager
local BillBoardType = GameConfig.EnumType.BillBoardType
local EntityType = GameConfig.EnumType.EntityType
local SkillDataHelper = GameDataHelper.SkillDataHelper
local StateConfig = GameConfig.StateConfig
local FlyTriggerManager = PlayerManager.FlyTriggerManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function Creature:__ctor__(csEntity)
    self.entityAI = EntityAI(self)
    --self._stateManager = StateManager(self)
    self.actorHasLoaded = false
    self.showBillboard = true

    self._thinkable = true
    self._tempStopThink = false

    self.isCreature = true
    self.faction_id = 0
end

function Creature:OnEnterWorld()
    Creature._base.OnEnterWorld(self)
    self._isDead = false
    self.isInWorld = true
    if self.showBillboard then
        EventDispatcher:TriggerEvent(GameEvents.ADD_CREATURE_BILLBOARD, self.id, self.entityType)
    end
    --GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_SPEED, 600)
end

function Creature:OnLeaveWorld()
    Creature._base.OnLeaveWorld(self)
    self.isInWorld = false
    if self.showBillboard then
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_CREATURE_BILLBOARD, self.id, self.entityType)
    end
    if self.entityAI ~= nil then
        self.entityAI:OnDestroy()
        self.entityAI = nil
    end
end

function Creature:OnCellAttached()

end

function Creature:OnDeath()
    self._isDead = true
end

function Creature:OnAnimationEvent(param)
    if param == "cg1" then
        FlyTriggerManager:OnEnd()
    elseif param == "cg2" then
        FlyTriggerManager:OnEnd()
    elseif param == "CreateRoleCG" then
        EventDispatcher:TriggerEvent(GameEvents.On_Create_Role_CG_End)
    elseif param == "CreateRoleCGBegin" then
        EventDispatcher:TriggerEvent(GameEvents.On_Create_Role_CG_Begin)
    end
 end

function Creature:OnRelive()
    self._isDead = false
end

function Creature:OnEnterTransform()
    
end

function Creature:OnLeaveTransform()
    
end

function Creature:OnChangeHp(casterId, oldHp, newHp, attackType)
	if self:GetEntityAI() then
        self:GetEntityAI():on_change_hp(casterId, newHp - oldHp, attackType)
	end
end

function Creature:OnActorLoaded()
    self.actorHasLoaded = true
end

function Creature:SetActorControllerEnabled(state)
    self.cs:SetActorControllerEnabled(state)
end

function Creature:SetActor(actorID)
    self:SetDefaultLayer(self:GetDefaultLayer())
    self.cs:SetActor(actorID)
end

function Creature:SetCityState(value)
    self.cs:SetCityState(value)
end

function Creature:HasActorDeathController(value)
    self.cs.hasActorDeathController = value
end

function Creature:SetDefaultLayer(value)
    self.cs.defaultLayer = value
end

function Creature:GetDefaultLayer()
    return self.cs.defaultLayer
end

function Creature:SetLearnedSkillList(data)
    local dict = {}
    for k, v in pairs(data) do
        table.insert(dict, v)
        table.insert(dict, 0)
    end
    self:SetLearnedSkillDict(dict)
end

function Creature:SetLearnedSkillDict(data)
    return self.cs:SetLearnedSkillDict(data)
end

function Creature:AddEquip(equipID, particleID, flowID, colorR, colorG, colorB)
    particleID = particleID or 0
    flowID = flowID or 0
    colorR = colorR or 0
    colorG = colorG or 0
    colorB = colorB or 0
    return self.cs:AddEquip(equipID, particleID, flowID, colorR, colorG, colorB)
end

function Creature:SetReady(isReady)
    return self.cs:SetReady(isReady)
end

function Creature:ResetDefaultEquip(type)
    return self.cs:ResetDefaultEquip(type)
end

function Creature:SetCurHead(headID, particleID, flowID, r, g, b)
    particleID = particleID or 0
    flowID = flowID or 0
    r = r or 0
    g = g or 0
    b = b or 0
    return self.cs:SetCurHead(headID, particleID, flowID, r, g, b)
end

function Creature:SetCurCloth(clothID, particleID, flowID, r, g, b)
    particleID = particleID or 0
    flowID = flowID or 0
    r = r or 0
    g = g or 0
    b = b or 0
    return self.cs:SetCurCloth(clothID, particleID, flowID, r, g, b)
end

function Creature:SetCurWeapon(weaponId, particleID, flowID, r, g, b)
    particleID = particleID or 0
    flowID = flowID or 0
    r = r or 0
    g = g or 0
    b = b or 0
    return self.cs:SetCurWeapon(weaponId, particleID, flowID, r, g, b)
end

function Creature:SetCurWing(wingId, particleID, flowID, r, g, b)
    particleID = particleID or 0
    flowID = flowID or 0
    r = r or 0
    g = g or 0
    b = b or 0
    return self.cs:SetCurWing(wingId, particleID, flowID, r, g, b)
end

function Creature:SetCurEffect(effectId, particleID, flowID, r, g, b)
    particleID = particleID or 0
    flowID = flowID or 0
    r = r or 0
    g = g or 0
    b = b or 0
    return self.cs:SetCurEffect(effectId, particleID, flowID, r, g, b)
end

function Creature:SetVisible(visible)
	self.cs.visible = visible
end

function Creature:SetApplyRootMotion(state)
    self.cs:SetApplyRootMotion(state)
end

function Creature:PlaySkillBySkillPlayer(skillId)
    if self.entityType == EntityType.SkillShowAvatar then
        skillId = SkillDataHelper.GetPreviewSkill(skillId)
        self.cs:PlaySkillBySkillPlayer(skillId)
    end
end

function Creature:PlaySkillByCreateRoleAvatar(skillId)
    if self.entityType == EntityType.CreateRoleAvatar then
        skillId = SkillDataHelper.GetPreviewSkill(skillId)
        self.cs:PlaySkillByCreateRoleAvatar(skillId)
    end
end

--mode 1:表示cg技能
function Creature:EnableDramaSkill(mode)
	self.cs:SetDramaSkill(mode)
end

function Creature:PlaySkill(skillId)
    return self.cs:PlaySkill(skillId)
end

function Creature:PlaySkillByPos(skillPos)
    return self.cs:PlaySkillByPos(skillPos)
end

function Creature:ReleaseSkillByPos(skillPos)
    return self.cs:ReleaseSkillByPos(skillPos)
end

function Creature:SkillCanPlayByPos(skillPos)
    return self.cs:SkillCanPlayByPos(skillPos)
end

function Creature:GetSkillAIRangeByPos(skillPos)
    return self.cs:GetSkillAIRangeByPos(skillPos)
end

function Creature:GetActionCutTimeByPos(skillPos)
    return self.cs:GetActionCutTimeByPos(skillPos)
end

--公共cd判断 暂没
function  Creature:GetCommonCDByPos(skillPos)
    return 0
end

function Creature:Move(destination, speed, luafunc, endDistance)
    return self.cs:Move(destination, speed, luafunc, endDistance or 0)
end

function Creature:MoveByLine(destination, speed, callback, endDistance, returnReady)
    return self.cs:MoveByLine(destination, speed, callback, endDistance or 0, returnReady or false)
end

function Creature:MoveByDirection(targetID, speed, moveTime, right)
    moveTime = moveTime or 1
    right = right or false
    return self.cs:MoveByDirection(targetID, speed, moveTime, right)
end

function Creature:MoveToDirection(targetPosition,speed,moveTime, returnReady)
    moveTime = moveTime or 1
    return self.cs:MoveToDirection(targetPosition,speed,moveTime, returnReady or false)
end

function Creature:MoveByPathFly(speed, luafunc, endDistance)
    return self.cs:MoveByPathFly(speed, luafunc, endDistance or 0)
end

function Creature:PauseFly(state)
    return self.cs:PauseFly(state)
end

function Creature:StopMove()
    return self.cs:StopMove()
end

function Creature:StopMoveWithoutCallback()
    return self.cs:StopMoveWithoutCallback()
end

function Creature:GetEntityAI()
    return self.entityAI
end

function Creature:SetBehaviorTree(btNumber)
    if self:GetEntityAI() then
        self:GetEntityAI():StartAI(btNumber)
        self:SetThinkState(true)
    end
end

function Creature:SetAiControlling(state)
    self.cs.aiControlling = state
end

function Creature:SetThinkState(state)
    if self:GetEntityAI() then
        self:GetEntityAI():SetThinkState(state)
        --self:SetAiControlling(state)
    end
end

function Creature:ExtendThinkTime(increate)
    if self:GetEntityAI() then
        self:GetEntityAI():ExtendThinkTime(increase)
    end
end


function Creature:PlayAction(actionId)
    self.cs:PlayAction(actionId)
end

function Creature:PlaySfx(sfxId, duration)
    self.cs:PlaySfx(sfxId, duration or 2)
end

function Creature:GetBoneByName(slotName)
    return self.cs:GetBoneByName(slotName)
end

function Creature:CanAttack()
    return self.cs:CanAttack()
end

function Creature:IsOnAir()
    return self.cs:IsOnAir()
end

function Creature:IsMoving()
    return self.cs:IsMoving()
end

function Creature:GetCurHPRate()
    return self.cs.curHPRate
end

function Creature:ContainsBuffer(buffId)
    return self.cs:ContainsBuffer(buffId)
end

function Creature:GetPosition()
    return self.cs.position
end

function Creature:GetFace()
    return self.cs.face
end

function Creature:SetFace(y)
    local f = Vector3(0, y * 0.5, 0)
    self.cs.face = f
end

function Creature:AddBuff(buffId, duration)
    duration = duration or 0 --默认参数
    return self.cs:AddBuffer(buffId, duration)
end

function Creature:DeleteBuff(buffId)
    return self.cs:RemoveBuffer(buffId)
end

function Creature:RemoveBuffer(buffId)
    return self.cs:RemoveBuffer(buffId)
end

function Creature:IsStickSkill(pos)
    return self.cs:IsStickSkill(pos)
end

function Creature:Teleport(Position)
    return self.cs:Teleport(Position)
end

function Creature:GetCameraTransform()
    return self.cs:GetCameraTransform()
end

function Creature:InState(state)
    return self.cs:InState(state)
end

function Creature:SetTeleportSingState(state)
    self.cs:SetTeleportSingState(state)
end

function Creature:SetCollectSingState(state)
    self.cs.isCollectSing = state
end

function Creature:DisplayCreatureBillboard()
    EventDispatcher:TriggerEvent(GameEvents.DISPLAY_CREATURE_BILLBOARD, self.id)
end

function Creature:HideCreatureBillboard()
    EventDispatcher:TriggerEvent(GameEvents.HIDE_CREATURE_BILLBOARD, self.id)
end

function Creature:SetGravity(gravity)
    self.cs:SetGravity(gravity)
end

-- 获取最新释放技能和时间戳
function Creature:GetLastSkillIdAndTimestamp()
    return 1, 0
end

-- 战斗伤害等属性
function Creature:SetBattleAttributes(attributes)
    self._battleAttributes = attributes
    local keys = {}
    local values = {}
    for key, value in pairs(attributes) do
        table.insert( keys, key )
        table.insert( values, value )
    end
    self.cs:SetBattleAttributes(keys, values)
end

function Creature:SetBattleAttribute(akey, value)
    self.cs:SetBattleAttribute(akey, value)
end

function Creature:_set_thinkable(state)
    self._thinkable = state
end

function Creature:TempStopThink(state)
    self._tempStopThink = state
end

function Creature:CanThink()
    if not self._thinkable then
        return 1
    end
    if self._tempStopThink then
        return 2
    end
    if self._isDead then
        return 3
    end
    return 0
end

--
function Creature:GetSpeed()
    if self.speed == nil then
        return 6
    end
    return self.speed * 0.01
end

function Creature:IdleToReady()
    self.cs:IdleToReady()
end

function Creature:ReadyToIdle()
    self.cs:ReadyToIdle()
end

function Creature:get_special_prior_target_eid()
end

function Creature:Faction()
	return self.faction_id
end

function Creature:GetDelTargetLock()
    --暂时返回false
    return false
end

function Creature:SetTargetId(value)
    self.cs.target_id = value
end

function Creature:FaceToPosition(position)
    self.cs:FaceToPosition(position)
end

function Creature:SetRide(actorID, modelScale)
    self.cs:SetRide(actorID, modelScale or -1)
end

--mode 0:普通待机 1:战斗待机
function Creature:SetActorMode(mode)
    self.cs:SetActorMode(mode)
end

function Creature:OnTriggerPathPoint(pp_id, is_enter)
end

function Creature:GetSkillIdByPos(pos)
    return self.cs:GetSkillIdByPos(pos)
end

function Creature:GetTargetId()
    return self.cs.target_id
end

function Creature:RefreshTargetId(id)

end

function Creature:OnTouchEntity()

end

function Creature:IsDeath()
    return self:InState(StateConfig.AVATAR_STATE_DEATH)
end

function Creature:GetCurBufferIDList()
    local data = self.cs:GetCurBufferIDList()
    local list = string.split(data, ",")
    return list
end

function Creature:GetBufferRemainTime(bufferId)
    --remainTime小于0表示永久buff
    local remainTime = self.cs:GetBufferRemainTime(bufferId)
    return remainTime
end

function Creature:GetTransform()
    return self.cs:GetTransform()
end

--state 0:不收起 1：收起
function Creature:PackUpWeapon(state)
    self.cs:PackUpWeapon(state)
end

function Creature:CanExitMapByHit()
    local state = self.cs:CanExitMap() or 1
    if state == 0 then
        GUIManager.ShowText(1, LanguageDataHelper.CreateContent(58446))
    end
    return state == 1
end