require "Entities/Creature"
local Avatar = Class.Avatar(ClassTypes.Creature)

local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper
local EntityType = GameConfig.EnumType.EntityType
local Vector3 = Vector3
local StateConfig = GameConfig.StateConfig
local ReviveRescueManager = GameManager.ReviveRescueManager
local VisualFxConfig = GameConfig.VisualFxConfig
local PartnerDataHelper = GameDataHelper.PartnerDataHelper
local EscortPeriManager = PlayerManager.EscortPeriManager

function Avatar:__ctor__(csEntity)
    self.state = 0
    self.entityType = EntityType.Avatar
    self.showBillboard = true
    self.range = 7
    self.horse_grade_ride = 0
end

function Avatar:OnEnterWorld()
    self.guildName = self.guild_name
    Avatar._base.OnEnterWorld(self)
    self.actorId = RoleDataHelper.GetModelByVocation(self.vocation)
    self:SetActor(self.actorId)
    self:SetCityState(false)
    EventDispatcher:TriggerEvent(GameEvents.OnAvatarEnterWorld, self.id)
end

function Avatar:OnLeaveWorld()
    EventDispatcher:TriggerEvent(GameEvents.OnAvatarLeaveWorld, self.id)
    Avatar._base.OnLeaveWorld(self)
    TimerHeap:DelTimer(self._timerId)
end

function Avatar:OnActorLoaded()
    self.cs:SetSelectTrigger()
    self.actorHasLoaded = true
    EventDispatcher:TriggerEvent(GameEvents.On_Avatar_Actor_Loaded)
    self:SetReady(true)
    self:PlayAction(42)
    self:set_horse_ride_model()
    self:set_guard_goddess_id(self.guard_goddess_id)
end

function Avatar:GetDefaultLayer()
    return GameObjectLayerConfig.Avatar
end

function Avatar:TestRPC(ttt, tttt)
    print(ttt, tttt)
end

function Avatar:set_state(oldstate)
    -- LoggerHelper.Log("&&&&&&：" .. oldstate .. ":" .. self.state)
end


function Avatar:set_level(change)
    if change ~= nil and change ~= self.level and self.actorHasLoaded then
        if self.horse_ride_model > 0 then
            self:PlaySfx(VisualFxConfig.LevelUpRide, 2)
        else
            self:PlaySfx(VisualFxConfig.LevelUp, 2)
        end
        GameWorld.PlaySound(8)
    end
end

function Avatar:Get_Pet_Id()
    return self.pet_eid
end

function Avatar:OnChangeHp(casterId, oldHp, newHp, attackType)
    local change_hp = oldHp - newHp
    if change_hp < 0 then
        local pet_eid = self:Get_Pet_Id()
        local pet_entity = GameWorld.GetEntity(pet_eid)
        if pet_entity and pet_entity:GetEntityAI() then
            pet_entity:GetEntityAI():on_change_hp(casterId, change_hp, attackType)
        end
    
    end
end

--职业组,100->1,201->2
function Avatar:GetVocationGroup()
    return math.floor(self.vocation / 100)
end

--职业第几转,100->0,203->3
function Avatar:GetVocationChange()
    return self.vocation % 100
end

function Avatar:set_horse_ride_model()
    if self.horse_ride_model == nil or not self.isInWorld then
        return
    end
    if self.horse_ride_model > 0 then
        if self:InState(StateConfig.AVATAR_STATE_TRANSFORM) then
            return
        end
        self:SetRide(self.horse_ride_model, -1)
    else
        self:SetRide(0)
    end
end

function Avatar:set_pk_value(oldValue)
    if oldValue ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, self.id)
    end
end

function Avatar:set_guard_goddess_id(oldValue)
    if oldValue == nil then
        return
    end
    if self.guard_goddess_id > 0 and self.cs.peri_eid == 0 then
        local attris = {}
        attris.peri_id = self.guard_goddess_id
        attris.owner_id = self.id
        attris.cur_hp = 10000
        attris.max_hp = 10000
        attris.faction_id = self.faction_id
        attris.position = self:GetPosition()
        local peri = GameWorld.CreateEntity("Peri", attris)
    elseif self.guard_goddess_id == 0 then
        GameWorld.DestroyEntity(self.cs.peri_eid)
        self.cs.peri_eid = 0
        if self.id == GameWorld.Player().id then
            EscortPeriManager:ClearTask()
        end
    end
end

--点击Avatar弹窗
function Avatar:OnTouchEntity()
    --点自己不处理
    if self.id == GameWorld.Player().id then
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.SHOW_OTHER_PLAYER_HEAD, self, true)
end