require "Entities/Avatar"
require "PlayerManager/PlayerData/DialogData"
local NPC = Class.NPC(ClassTypes.Creature)

local DialogData = ClassTypes.DialogData

local EntityConfig = GameConfig.EntityConfig
local TaskManager = PlayerManager.TaskManager
local NPCDataHelper = GameDataHelper.NPCDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local BagConfig = GameConfig.BagConfig
local public_config = GameWorld.public_config
local math = math
local BillBoardType = GameConfig.EnumType.BillBoardType
local EntityType = GameConfig.EnumType.EntityType
local NPCConfig = GameConfig.NPCConfig
local DramaTriggerType = require "GameManager/DramaSystem/DramaTriggerType"
local EscortPeriManager = PlayerManager.EscortPeriManager
local RoleDataHelper = GameDataHelper.RoleDataHelper

local fxcfg = {}
fxcfg[3] = "Fx/fx_prefab/ui/fx_ui_30001_gantanhao.prefab" --感叹号
fxcfg[4] = "Fx/fx_prefab/ui/fx_ui_30002_wenhao.prefab" --问号
fxcfg[1] = "Fx/fx_prefab/ui/fx_ui_30003_duihuakuang.prefab" --对话框
fxcfg[2] = "Fx/fx_prefab/ui/fx_ui_30004_liangbadao.prefab" --两把刀

local Update_Time = 500
local Default_Model = 2410
local Show_Distance = 15

function NPC:__ctor__(csEntity)
    self.entityType = EntityType.NPC
    self:SetDefaultLayer(GameObjectLayerConfig.Trigger)
    self.showBillboard = true
    self._dramaShow = true
    self.inCity = true
    self.cs.isStatic = true
end

function NPC:OnEnterWorld()
    self.name = NPCDataHelper.GetNPCName(self.npc_id)
    self.scale = NPCDataHelper.GetNPCScale(self.npc_id)
    self.honorName = NPCDataHelper.GetNPCFunctionHonor(self.npc_id)
    self._base.OnEnterWorld(self)
    local npc = NPCDataHelper.GetNPCData(self.npc_id)
    self.model = npc.model
    if self:NeedDelayedShow() then
        self:SetActor(Default_Model)
    else
        self:LoadNPCModel()
    end
    local standby = NPCDataHelper.GetNPCStandby(self.npc_id)
    self.inCity = standby == 0
    --战斗待机
    if standby == 2 then
        self.cs.fight_idle_state = 1
    end
    self:SetCityState(self.inCity)
    EventDispatcher:TriggerEvent(GameEvents.OnNpcEnterWorld, self.npc_id, self.id)
--LoggerHelper.Log("Ash: OnEnterWorld: " .. self.npc_id .. " " .. self.id .. " " .. self.name .. " " .. PrintTable:TableToStr(self:GetPosition()))
end

function NPC:OnLeaveWorld()
    EventDispatcher:TriggerEvent(GameEvents.OnNpcLeaveWorld, self.npc_id, self.id)
    -- self:RemovePointingArrow()
    self._base.OnLeaveWorld(self)
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    self.init = nil
end

function NPC:OnActorLoaded()
    self.cs:SetSelectTrigger()
    self:ShowTask()
    if self.from == NPCConfig.FROM_DRAMA then
        GameManager.DramaManager:Trigger(DramaTriggerType.CreateDramaNPC, tostring(self.drama_id))
    end
    self.cs:InitActorScale(self.scale)
    
    if self:NeedDelayedShow() then
		-- 等待进入主角范围内才创建真正模型
    else
        self:RefreshRideModel()
    end
    if self.init then
        self:RefreshRideModel()
    end
end

function NPC:RefreshRideModel()
    local rideModel = self:GetRideModel()
    if rideModel > 0 then
        self:SetRide(rideModel, -1)
    end
end

function NPC:GetRideModel()
    if self._rideModel == nil then
        self._rideModel = NPCDataHelper.GetNPCRideModel(self.npc_id)
    end
    return self._rideModel
end

function NPC:NeedDelayedShow()
    local npc = NPCDataHelper.GetNPCData(self.npc_id)
    if npc.npc_visible == 1 or self.from == NPCConfig.FROM_DRAMA or self.init then
        return false
    end
    return true
end

-- override 进入玩家范围
function NPC:__OnEnterDistance()
    if self.init then
        return
    end
    self.init = true
    self:LoadNPCModel()
end

function NPC:LoadNPCModel()
    local npc = NPCDataHelper.GetNPCData(self.npc_id)
    if npc.npc_distinguish ~= 0 then
        self.cs.facade = npc.npc_facade
        self.cs.vocation = npc.npc_distinguish
    end
    self:SetActor(self.model)
end

function NPC:OnControllerLoaded()
    if self.from == NPCConfig.FROM_DRAMA then
        GameManager.DramaManager:Trigger(DramaTriggerType.CreateDramaNPCControllerLoaded, tostring(self.drama_id))
    end
end

function NPC:ShowTask()
    self._curTaskId = nil
    self.cs:HideNPCIcon()
    -- self:RemovePointingArrow()
    local task, eventItemData = TaskManager:GetNPCTask(self.npc_id)
    -- LoggerHelper.Log("Ash: ShowTask: " .. PrintTable:TableToStr(task))
    if task ~= nil and task:GetIsTrack() and
        (task:GetState() ~= public_config.TASK_STATE_ACCEPTABLE or task:GetTaskType() == public_config.TASK_TYPE_MAIN) then
        self._curTaskId = task:GetId()
        -- self:ShowPointingArrow(task, eventItemData)
        local p = self:GetIconByTask(task)
        if p ~= 0 then
            local h = NPCDataHelper.GetNPCHeight(self.npc_id)
            self.cs:ShowNPCIcon(fxcfg[p], Vector3(0, h + 0.3, 0))
        end
    end
    if self.npc_id == GlobalParamsHelper.GetParamValue(529) then
        --接护送女神任务次数未用完显示感叹号
        if EscortPeriManager:GetRemainCount() > 0 then
            local h = NPCDataHelper.GetNPCHeight(self.npc_id)
            self.cs:ShowNPCIcon(fxcfg[3], Vector3(0, h + 0.3, 0))
        end
    end
end

-- function NPC:ShowPointingArrow(task, eventItemData)
--     local trackIcon = task:GetIcon()
--     if trackIcon ~= nil then
--         self:AddPointingArrow(trackIcon)
--     end
-- end
function NPC:Ask()
    local rideModel = self:GetRideModel()
    if rideModel == 0 then
        self:FaceToPosition(GameWorld.Player():GetPosition())
    end
    -- GameWorld.Player():FaceToPosition(self:GetPosition())
    if self.npc_id == GlobalParamsHelper.GetParamValue(529) then
        --请求护送女神
        EscortPeriManager:TalkToNpc(self.npc_id)
        return
    end
    EventDispatcher:TriggerEvent(GameEvents.SHOW_DIALOG, self.npc_id)
    return
-- PlayerManager.PlayerDataManager.taskData.acceptingId = taskid
-- EventDispatcher:TriggerEvent(GameEvents.TASK_TALK)
end

function NPC:GetCurTask()
    return self._curTaskId
end

function NPC:OnTouchEntity()
    EventDispatcher:TriggerEvent(GameEvents.PlayerCommandFindNPC, nil, self.npc_id, self:GetPosition(), tonumber(GlobalParamsHelper.GetParamValue(30)))--npc_talk_distance_i
    --LoggerHelper.Log("on touch " .. self.id)
    -- 指引需要 点击NPC
    EventDispatcher:TriggerEvent(GameEvents.CLICK_NPC, self.npc_id)
end

-- function NPC:AddPointingArrow(icon)
--     local h = NPCDataHelper.GetNPCHeight(self.npc_id)
--     EventDispatcher:TriggerEvent(GameEvents.ADD_ENTITY_POINTING_ARROW, self.id, icon, h + 0.3)
-- -- LoggerHelper.Log("Ash: AddPointingArrow " .. tostring(self.id) .. tostring(h + 0.3))
-- end
-- function NPC:RemovePointingArrow(icon)
--     EventDispatcher:TriggerEvent(GameEvents.REMOVE_ENTITY_POINTING_ARROW, self.id)
-- -- LoggerHelper.Log("Ash: RemovePointingArrow " .. tostring(self.npc_id))
-- end
function NPC:GetIconByTask(task)
    local state = task:GetState()
    if state == public_config.TASK_STATE_ACCEPTABLE then
        return 3
    elseif state == public_config.TASK_STATE_ACCEPTED then
        return 1
    elseif state == public_config.TASK_STATE_COMPLETED then
        return 4
    else
        return 0
    end
end

function NPC:DramaShow(visible)
    self._dramaShow = visible
    if not visible then
        self:SetVisible(visible)
    else
        self:ShowActorByTask()
    end
end

function NPC:ShowActorByTask()
    if not self._dramaShow then return end
--暂时屏蔽镇魂街移植部分
--local isVisible, taskId = NPCDataHelper.IsVisibleByTask(self.npc_id)
--self:SetVisible(isVisible)
end
