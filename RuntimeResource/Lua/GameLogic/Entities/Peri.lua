--女神类型
require "Entities/Creature"


local ai_utils = require "AI/ai_utils"
local Peri = Class.Peri(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local EntityType = GameConfig.EnumType.EntityType
local GuardGoddessDataHelper = GameDataHelper.GuardGoddessDataHelper

function Peri:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.Peri
end

function Peri:OnEnterWorld()
    self._base.OnEnterWorld(self)
    self.ai_id = GuardGoddessDataHelper:GetAI(self.peri_id)
    self.model = GuardGoddessDataHelper:GetModel(self.peri_id)
    self:SetActor(self.model)
    self:SetCityState(false)
    self:GetOwner().cs.peri_eid = self.id
end

function Peri:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function Peri:OnActorLoaded()
    self:SetBehaviorTree(self.ai_id)
    self:SetThinkState(true)
    self:Teleport(self:GetOwner():GetPosition())
    local ride_id = GuardGoddessDataHelper:GetRideModel(self.peri_id)
    self:SetRide(ride_id, -1)
end

function Peri:GetDefaultLayer()   
    return GameObjectLayerConfig.Actor
end

function Peri:GetOwnerId()
    return self.cs.owner_id
end

function Peri:GetOwner()
    return GameWorld.GetEntity(self.cs.owner_id)
end

function Peri:GetSpeed()
    return self:GetOwner().speed * 0.01
end