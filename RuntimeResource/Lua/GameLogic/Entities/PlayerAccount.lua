local PlayerAccount = Class.PlayerAccount(ClassTypes.Entity)

require "RPCProtol.character_pb"
local public_config = require("ServerConfig/public_config")
local error_code = require("ServerConfig/error_code")
local LocalSetting = GameConfig.LocalSetting
local GUIManager = GameManager.GUIManager
local EntityType = GameConfig.EnumType.EntityType
local CreateRoleManager = GameManager.CreateRoleManager
local LoadingBarManager = GameManager.LoadingBarManager
local SystemInfoManager = GameManager.SystemInfoManager
local DateTimeUtil = GameUtil.DateTimeUtil

function PlayerAccount:__ctor__(csEntity)
    -- LoggerHelper.Error("PlayerAccount __ctor__")
    self.entityType = EntityType.PlayerAccount
    self.roleState = 1
    GameWorld._player = self
end

function PlayerAccount:OnEnterWorld()
    -- LoggerHelper.Error("PlayerAccount OnEnterWorld" .. self.name)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_2)
    GameWorld.AccountName = self.name
    PlayerAccount._base.OnEnterWorld(self)
    self:CreateOrSelectRole()
    self._heartBeatTimer = TimerHeap:AddSecTimer(0, 60, 0, function()
        -- LoggerHelper.Log("server_open_time_req")
        self.server.server_open_time_req()
    end)
    DateTimeUtil.Init()
end

function PlayerAccount:OnLeaveWorld()
    -- LoggerHelper.Error("PlayerAccount OnLeaveWorld")
    DateTimeUtil.Clear()
    if self._heartBeatTimer then
        TimerHeap:DelTimer(self._heartBeatTimer)
    end
    PlayerAccount._base.OnLeaveWorld(self)
    GameWorld._player = nil
end

function PlayerAccount:CreateOrSelectRole()
    --LoggerHelper.Log(self.avatar_tbl)
    ---[[
    if GameWorld.SpecialExamine then
        GameWorld.Player():choose_avatar_req(self.avatar_tbl[1][public_config.CHARACTER_KEY_DBID])
        return
    end
    --]]
    if #self.avatar_tbl > 0 then
        self.roleState = 1 --选角
    else
        self.roleState = 2 --创角
    end
    GameManager.GameSceneManager:EnterSceneByID(6, false, nil, function()
            --CreateRoleManager:CreateRttCamera()
            GameManager.GameStateManager.SetState(GAME_STATE.CHOOSE)
    end)
end

function PlayerAccount:create_avatar_req(name, gender, vocation, race, facade)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_3)
    self.server.create_avatar_req(name, gender, vocation, race, facade)
end

function PlayerAccount:choose_avatar_req(dbid)
    --LoggerHelper.Error("-------choose_avatar_req------"..tostring(dbid))
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_4)
    self.server.choose_avatar_req(dbid)
end

function PlayerAccount:get_random_name_req(vocation)
    --LoggerHelper.Error("-------choose_avatar_req------"..tostring(dbid))
    vocation = vocation or 100
    self.server.get_random_name_req(vocation)
end

function PlayerAccount:create_avatar_resp(result)
    CreateRoleManager:CreateAvatarResp(result)
    if result == error_code.ERR_SUCCESSFUL then
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_5)
        LoggerHelper.Log("create_avatar_resp success")
        GUIManager.ClosePanel(PanelsConfig.CreateRole)
        
        -- if GameLoader.SystemConfig.IsUsePlatformSdk then
        TimerHeap:AddSecTimer(1, 0, 0, function()
            local player = GameWorld.Player()
            -- LoggerHelper.Log("----------CreateRoleLog,roleId:"..LocalSetting.settingData.UserAccount
            -- ..",roleName:"..player.name
            -- ..",serverId:"..LocalSetting.settingData.SelectedServerID
            -- ..",serverName:"..ServerListManager:SelectedServerName())
            GameWorld.ShowSDKLog("onUploadCreateRole", tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
                LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), tostring(player.money_coupons),
                player.guild_name, tostring(player.money_coupons), "-1")
        end)
    -- end
    else
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_6, result)
        LoggerHelper.Error("create_avatar_resp fail with error code " .. result)
        if result > 0 then
            SystemInfoManager:ShowRespTip(0, result, {})
        end
    
    end
end

function PlayerAccount:get_avatar_tbl_resp(luatable)
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_7)
    do
        if #luatable > 0 then
            --LoggerHelper.Error("-------get_avatar_tbl_resp------"..tostring(luatable[1][public_config.CHARACTER_KEY_DBID]))
            self.server.choose_avatar_req(luatable[#luatable][public_config.CHARACTER_KEY_DBID])
        end
        return
    end
end
function PlayerAccount:get_random_name_resp(name)
    CreateRoleManager:GetRandomNameResp(name)
end

function PlayerAccount:heart_beat_resp()
-- LoggerHelper.Log("heart_beat_resp")
--LoggerHelper.Log(string.format("heart_beat_resp %s ", tostring(DateTimeUtil.GetServerTime())), "#00fff0")
end

function PlayerAccount:on_udid_ban_resp()
    GameManager.GameStateManager.ReturnLogin(true)
    local value = {["confirmCB"] = function()
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["cancelCB"] = function()
        GUIManager.ClosePanel(PanelsConfig.SystemTips)
    end,
    ["text"] = GameDataHelper.LanguageDataHelper.CreateContent(58778)}

    TimerHeap:AddTimer(1000, 0, 0, 
        function()
            GameManager.GUIManager.ShowPanel(PanelsConfig.SystemTips, value)
        end
    )
end

function PlayerAccount:choose_avatar_resp(result)
    EventDispatcher:TriggerEvent(GameEvents.ON_SELECT_ROLE_WAITING, false)
    if result == 0 then
        --选角成功
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_8)
        LoadingBarManager:ShowProgressBar()
        EventDispatcher:TriggerEvent(GameEvents.ON_SELECT_ROLE_SUCC)
    else
        GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_9, result)
        SystemInfoManager:ShowClientTip(result)
    end
end

function PlayerAccount:SetCurrMapID()

end

function PlayerAccount:server_open_time_resp(serverTime)
    self._serverOpenTime = serverTime
    if serverTime == 0 then
        LoggerHelper.Info("server_open_time_resp:" .. tostring(serverTime))
        GameManager.GameStateManager.ReturnLogin()
        return
    end
    DateTimeUtil.ResetOpenServerTime(serverTime)
    LoadingBarManager:SetOpenServerDay(DateTimeUtil.GetServerOpenDays())
    GameWorld.UploadInfo()
end

function PlayerAccount:sync_time_resp(serverTime)
    DateTimeUtil.ResetServerTime(serverTime)
end

function PlayerAccount:StopAutoFight()

end

function PlayerAccount:StopMoveWithoutCallback()

end

function PlayerAccount:GetPosition()
    return Vector3.zero
end
