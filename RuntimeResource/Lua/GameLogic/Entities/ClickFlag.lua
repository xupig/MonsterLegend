--资源点

require "Entities/Creature"
local ClickFlag = Class.ClickFlag(ClassTypes.Creature)
local TimerHeap = GameWorld.TimerHeap
local EntityType = GameConfig.EnumType.EntityType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local CollectManager = GameManager.CollectManager
local GuildConquestManager = PlayerManager.GuildConquestManager

local CD_Time = 5

function ClickFlag:__ctor__(entityid)
	self.entityType = EntityType.ClickFlag
	self.showBillboard = true
	self.name = LanguageDataHelper.CreateContent(53181)
end

function ClickFlag:OnEnterWorld()
	self._base.OnEnterWorld(self)
    
    self._playerTransform = GameWorld.Player().cs:GetTransform()
	self._timer = TimerHeap:AddSecTimer(0, 0.5, 0, function() self:TestPlayer() end)
	self._triggered = false --已触发
	self._range = GlobalParamsHelper.GetParamValue(345)
	self._startFunc = function() self:OnStartFunc() end
	self._endFunc = function() self:OnEndFunc() end
	self._interruptFunc = function() self:OnInterruptFunc() end
    EventDispatcher:TriggerEvent(GameEvents.OnClickFlagEnterWorld, self.id)
end

function ClickFlag:TestPlayer()
	local p = self._playerTransform.position
    local len = Vector3.Distance(p, self:GetPosition())
	if len < self._range and self.faction_id ~= GameWorld.Player().faction_id then
		if not self._triggered then
			self._triggered = true
			local name = GlobalParamsHelper.GetParamValue(53)[2]
			local icon = GlobalParamsHelper.GetParamValue(53)[1]
			CollectManager:SetCommonCollectItem(self.id, CD_Time, name, icon, self._startFunc, self._endFunc, self._interruptFunc)
		end
	else
		if self._triggered then
			self._triggered = false
			CollectManager:RemoveCommonCollectItem(self.id)
		end
	end
end

function ClickFlag:OnStartFunc()
	GuildConquestManager:GuildConquestStartOccupyReq(self.id)
end

function ClickFlag:OnEndFunc()
	GuildConquestManager:GuildConquestOccupyReq(self.id)
end

function ClickFlag:OnInterruptFunc()
	GuildConquestManager:GuildConquestInterruptOccupyReq(self.id)
end

function ClickFlag:set_faction_id()
	local modelId = GlobalParamsHelper.GetParamValue(716)[1]
	if self.faction_id == 0 then
		modelId = GlobalParamsHelper.GetParamValue(716)[1]
		self.name = LanguageDataHelper.CreateContent(53181)
	elseif self.faction_id == GameWorld.Player().faction_id then
		modelId = GlobalParamsHelper.GetParamValue(716)[2]
		self.name = LanguageDataHelper.CreateContent(53179)
	else
		modelId = GlobalParamsHelper.GetParamValue(716)[3]
		self.name = LanguageDataHelper.CreateContent(53180)
	end
	EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, self.id)
	self:SetActor(modelId)
end

function ClickFlag:OnActorLoaded()

end

function ClickFlag:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
    TimerHeap:DelTimer(self._timer)
    EventDispatcher:TriggerEvent(GameEvents.OnClickFlagLeaveWorld, self.id)
    if self._triggered then
		self._triggered = false
		CollectManager:RemoveCommonCollectItem(self.id)
	end
end

function ClickFlag:OnDeath()

end

function ClickFlag:OnTouchEntity()

end

function ClickFlag:GetDefaultLayer()
    return GameObjectLayerConfig.Default
end
