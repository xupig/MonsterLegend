local StaticEntity = Class.StaticEntity(ClassTypes.Entity)
local EntityConfig = GameConfig.EntityConfig

function StaticEntity:__ctor__(csEntity)

end

--CS层回调接口
function StaticEntity:OnEnterWorld()
    StaticEntity._base.OnEnterWorld(self)
    EventDispatcher:TriggerEvent(GameEvents.ADD_CREATURE_BILLBOARD, self.id, self.entityType)
end

--CS层回调接口
function StaticEntity:OnLeaveWorld()
    StaticEntity._base.OnLeaveWorld(self)
    EventDispatcher:TriggerEvent(GameEvents.REMOVE_CREATURE_BILLBOARD, self.id, self.entityType)
end

--CS层回调接口
function StaticEntity:OnModelLoaded()

end

--CS层回调接口
function StaticEntity:OnEffectModelLoaded()

end

function StaticEntity:SetModel(modelId)
	self.cs:SetModel(modelId)
end

--设置特效id
function StaticEntity:SetEffectModel(modelId)
	self.cs:SetEffectModel(modelId)
end

function StaticEntity:SetPosition(position)
	self.cs:SetPosition(position)
end

function StaticEntity:SetRandomPosition(position,randRate)
	self.cs:SetRandomPosition(position,randRate)
end

function StaticEntity:GetPosition()
	return self.cs.position
end

function StaticEntity:FlyToEntity()
	self.cs:FlyToEntity()
end

--设置自增ID
-- function StaticEntity:SetOwnDropId(ownDropId)
-- 	self.ownDropId = ownDropId
-- end

--出生时落地
function StaticEntity:OnGround()
end
