--掉落buff

require "Entities/Creature"
local ServerSpaceDrop = Class.ServerSpaceDrop(ClassTypes.Creature)
local TimerHeap = GameWorld.TimerHeap
local EntityType = GameConfig.EnumType.EntityType
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local BuffItemDataHelper = GameDataHelper.BuffItemDataHelper

local CD_Time = 5

function ServerSpaceDrop:__ctor__(entityid)
	self.entityType = EntityType.ServerSpaceDrop
	self.showBillboard = true
end

function ServerSpaceDrop:OnEnterWorld()
	self._base.OnEnterWorld(self)
    self.name = BuffItemDataHelper:GetBuffItemData(self.special_item_id)
    self._playerTransform = GameWorld.Player().cs:GetTransform()
	self._timer = TimerHeap:AddSecTimer(0, 0.5, 0, function() self:TestPlayer() end)
	self._range = BuffItemDataHelper:GetRange(self.special_item_id)
    local modelId = BuffItemDataHelper:GetModel(self.special_item_id)
	self:SetActor(modelId)
end

function ServerSpaceDrop:TestPlayer()
	local p = self._playerTransform.position
    local len = Vector3.Distance(p, self:GetPosition())
	if len <= self._range then
		GameWorld.Player().server.pick_special_item_req(self.id)
	end
end

function ServerSpaceDrop:OnActorLoaded()

end

function ServerSpaceDrop:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
    TimerHeap:DelTimer(self._timer)
end

function ServerSpaceDrop:OnDeath()

end

function ServerSpaceDrop:OnTouchEntity()

end

function ServerSpaceDrop:GetDefaultLayer()
    return GameObjectLayerConfig.Default
end
