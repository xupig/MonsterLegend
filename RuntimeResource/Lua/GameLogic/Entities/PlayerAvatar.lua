require "Entities/Avatar"
local PlayerAvatar = Class.PlayerAvatar(ClassTypes.Avatar)

local EntityConfig = GameConfig.EntityConfig
local PkgConfig = GameConfig.PkgConfig
local action_config = require("ServerConfig/action_config")
local public_config = require("ServerConfig/public_config")
local StateConfig = GameConfig.StateConfig

local mgr_entity_ai_cell = require "AI/mgr_entity_ai_cell"
local GameSceneManager = GameManager.GameSceneManager
local ResourceMappingManager = GameManager.ResourceMappingManager
-- local ModelShowManager = GameManager.ModelShowManager
local RedPointManager = GameManager.RedPointManager
local EntityMgr = ClassTypes.EntityMgr
local GameWorld = GameWorld

local SystemInfoManager = GameManager.SystemInfoManager
local PlayerActionManager = GameManager.PlayerActionManager
local PlayerCommandManager = GameManager.PlayerCommandManager
local PlayerDataManager = PlayerManager.PlayerDataManager
local BagManager = PlayerManager.BagManager
local SkillManager = PlayerManager.SkillManager
local RoleManager = PlayerManager.RoleManager
local TeamManager = PlayerManager.TeamManager
local TaskManager = PlayerManager.TaskManager
local MapManager = PlayerManager.MapManager
local InstanceManager = PlayerManager.InstanceManager
local ExpInstanceManager = PlayerManager.ExpInstanceManager
local EventActivityManager = PlayerManager.EventActivityManager
local AchievementManager = PlayerManager.AchievementManager
local SystemSettingManager = PlayerManager.SystemSettingManager
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
local PetInstanceManager = PlayerManager.PetInstanceManager
local ClearDataManager = PlayerManager.ClearDataManager
local TowerInstanceManager = PlayerManager.TowerInstanceManager
local AnswerManager = PlayerManager.AnswerManager
local AnimalManager = PlayerManager.AnimalManager
local EquipmentInstanceManager = PlayerManager.EquipmentInstanceManager
local EquipManager = GameManager.EquipManager
local ClientEntityManager = GameManager.ClientEntityManager
local ReviveRescueManager = GameManager.ReviveRescueManager
local RoleDataHelper = GameDataHelper.RoleDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local BillBoardType = GameConfig.EnumType.BillBoardType
local EntityType = GameConfig.EnumType.EntityType
local FloatTextType = EnumType.FloatTextType
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local VisualFxConfig = GameConfig.VisualFxConfig
local MailManager = PlayerManager.MailManager
local ChatManager = PlayerManager.ChatManager
local skillData = PlayerDataManager.skillData
local FriendManager = PlayerManager.FriendManager
local PVPManager = PlayerManager.PVPManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local ShopManager = PlayerManager.ShopManager
local RankManager = PlayerManager.RankManager
local RankExManager = PlayerManager.RankExManager
local TradeManager = PlayerManager.TradeManager
local DailyActivityManager = PlayerManager.DailyActivityManager
local FloatTextManager = PlayerManager.FloatTextManager
local GuildManager = PlayerManager.GuildManager
local AionTowerManager = PlayerManager.AionTowerManager
local WorldBossManager = PlayerManager.WorldBossManager
local BBSManager = PlayerManager.BBSManager
local ActivityInfoManager = PlayerManager.ActivityInfoManager
local AthleticsManager = PlayerManager.AthleticsManager
local EquipShowType = GameConfig.EnumType.EquipShowType
local RolePartType = GameConfig.EnumType.RolePartType
local FlyTriggerManager = PlayerManager.FlyTriggerManager
-- local ActorModelManager = GameManager.ActorModelManager
local clientSceneData = PlayerManager.PlayerDataManager.clientSceneData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PKModeManager = PlayerManager.PKModeManager
local PartnerManager = PlayerManager.PartnerManager
local TreasureManager = PlayerManager.TreasureManager
local FashionManager = PlayerManager.FashionManager
local TitleManager = PlayerManager.TitleManager
local AddRateManager = PlayerManager.AddRateManager
local RuneManager = PlayerManager.RuneManager
local AutoFightManager = PlayerManager.AutoFightManager
local CircleTaskManager = PlayerManager.CircleTaskManager
local WeekTaskManager = PlayerManager.WeekTaskManager
local BossHomeManager = PlayerManager.BossHomeManager
local VocationChangeManager = PlayerManager.VocationChangeManager
local MoneyInstanceManager = PlayerManager.MoneyInstanceManager
local PersonalBossManager = PlayerManager.PersonalBossManager
local ComposeManager = PlayerManager.ComposeManager
local EscortPeriManager = PlayerManager.EscortPeriManager
local InstanceBalanceManager = PlayerManager.InstanceBalanceManager
local OffLineManager = PlayerManager.OffLineManager
local VIPManager = PlayerManager.VIPManager
local DailyActivityExManager = PlayerManager.DailyActivityExManager
local CloudPeakInstanceManager = PlayerManager.CloudPeakInstanceManager
local PrayManager = PlayerManager.PrayManager
local OnlinePvpManager = PlayerManager.OnlinePvpManager
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local BackWoodsManager = PlayerManager.BackWoodsManager
local GuildConquestManager = PlayerManager.GuildConquestManager
local CardManager = PlayerManager.CardManager
local CollectManager = GameManager.CollectManager
local GodIslandManager = PlayerManager.GodIslandManager
local PreciousManager = PlayerManager.PreciousManager
local TransformManager = PlayerManager.TransformManager
local DropRecordManager = PlayerManager.DropRecordManager
local DragonSoulManager = PlayerManager.DragonSoulManager
local MarriageManager = PlayerManager.MarriageManager
local CheckOtherPlayerManager = PlayerManager.CheckOtherPlayerManager
local WelfareManager = PlayerManager.WelfareManager
local DailyChargeManager = PlayerManager.DailyChargeManager
local RouletteManager = PlayerManager.RouletteManager
local LotteryEquipManager = PlayerManager.LotteryEquipManager
local LotteryPeakManager = PlayerManager.LotteryPeakManager
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local MarriageRaiseManager = PlayerManager.MarriageRaiseManager
local FlowerManager = PlayerManager.FlowerManager
local FashionDataHelper = GameDataHelper.FashionDataHelper
local WeekCardManager = PlayerManager.WeekCardManager
local FlashSaleManager = PlayerManager.FlashSaleManager
local CardLuckManager = PlayerManager.CardLuckManager
local HappyWishManager = PlayerManager.HappyWishManager
local IdentifyManager = PlayerManager.IdentifyManager
local InvestManager = PlayerManager.InvestManager
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local ZeroDollarBuyManager = PlayerManager.ZeroDollarBuyManager
local SaleManager = PlayerManager.SaleManager
local BossOfferManager = PlayerManager.BossOfferManager
local CommonWaitManager = PlayerManager.CommonWaitManager
local SkyDropManager = PlayerManager.SkyDropManager
local LoadingBarManager = GameManager.LoadingBarManager
local AncientBattleManager = PlayerManager.AncientBattleManager
local LimitedShoppingManager = PlayerManager.LimitedShoppingManager
local VIPLevelGiftManager = PlayerManager.VIPLevelGiftManager
local StarManager = PlayerManager.StarManager

function PlayerAvatar:__ctor__(csEntity)
    self.entityType = EntityType.PlayerAvatar
    self.showBillboard = true
    self.showGlideBtn = false
    self:ActionResponseRegister()
    GameWorld._player = self
    self.groundTransformSlot = {}
    self.airTransformSlot = {}
    self._dramaShow = true
    self.lastSpell = 0
    self.lastSpellTimeStamp = 0
    self.ai_id = tonumber(GlobalParamsHelper.GetParamValue(419))
    self.autoFight = false
    self.world_level = 0
    self.inAttackState = false
    self.firstEnter = true
    local taskCommonManager = PlayerManager.TaskCommonManager
    self.inSafeArea = false
    self.temporaryAutoFight = false
    self._goldTotalChange = 0
    self._goldChangeCB = function()self:ShowMoneyGoldFloatText() end
end

function PlayerAvatar:OnEnterWorld()
    GameManager.OperationLogManager.BaseLogData(GameWorld.public_config.OPERATION_LOG_10)
    
    PlayerAvatar._base.OnEnterWorld(self)
    GameWorld.SetAvatarName(self.name)
    --self._stateManager:SetDispatchEnterDead(true)
    self:CheckNewPlayer()
    self:SetPosToSlot()
    RedPointManager:OnEnterWorld()
    PlayerDataManager:OnPlayerEnterWorld()
    PlayerActionManager:OnPlayerEnterWorld()
    PlayerCommandManager:OnPlayerEnterWorld()
    ResourceMappingManager:OnPlayerEnterWorld()
    self:SetLearnedSkillDict(skillData:GetEquipSkillIdList())
    self:PlayerEnterWorldLog()
    GameWorld.SetMaxScreenMonsterCount(GlobalParamsHelper.GetParamValue(916))
    if GameStateManager.GetState() ~= GAME_STATE.SCENE then --遮盖创角选角短暂穿帮问题
        LoadingBarManager:ShowProgressBar(0.1)
    end
end

function PlayerAvatar:OnLeaveWorld()
    PlayerAvatar._base.OnLeaveWorld(self)
    self:SetThinkState(false)
    GameManager.DramaManager:Reset()
    GameManager.GUIManager.Reset()
    RedPointManager:OnLeaveWorld()
    PlayerCommandManager:OnPlayerLeaveWorld()
    PlayerActionManager:OnPlayerLeaveWorld()
    PlayerDataManager:OnPlayerLeaveWorld()
    ResourceMappingManager:OnPlayerLeaveWorld()
    --self._stateManager:SetDispatchEnterDead(false)
    GameManager.CreateRoleManager.init = false
    GameWorld._player = nil
end

function PlayerAvatar:OnCellAttached()

end

function PlayerAvatar:GetTransform()
    return self.cs:GetTransform()
end

--清除当前锁定目标
function PlayerAvatar:ClearTarget()
    return self.cs:ClearTarget()
end

--技能按钮与槽位对应关系
function PlayerAvatar:SetPosToSlot()
    self.groundPosToSlot = {1, 2, 3, 4, 5, 6, 7, 8}
    self.groundPosTransformToSlot = {41, 42, 43, 44, 45, 6, 7, 8}
end

--新手预加载TriggerPanel
function PlayerAvatar:CheckNewPlayer()
    if self.level == 1 then
        GUIManager.ShowPanel(PanelsConfig.Trigger)
        GUIManager.ClosePanel(PanelsConfig.Trigger)
    end
end

function PlayerAvatar:InitSkillSlot()
    local groudDict = {}
    for k, v in pairs(self.groundPosToSlot) do
        table.insert(groudDict, k)
        table.insert(groudDict, v)
    end
    self.cs:SetPlayerGroundSkillPosList(groudDict)
    
    --提前初始化监听事件
    GUIManager.ShowPanel(PanelsConfig.SkillControl)
    GUIManager.ClosePanel(PanelsConfig.SkillControl)
end


function PlayerAvatar:CallRefreshSkillButtons()
    self.cs:InitSkillButtons()
end

function PlayerAvatar:FaceToCamera()
    self.cs:FaceToCamera()
end

function PlayerAvatar:RefreshLastSpellInfo(spell)
    self.lastSpell = spell
    self.lastSpellTimeStamp = DateTimeUtil.GetServerTime()
end

function PlayerAvatar:ShowDefaultClothAndHead()
    local clothId = RoleDataHelper.GetRoleDefaultClothByVocation(self.vocation)
    local headId = RoleDataHelper.GetRoleDefaultHeadByVocation(self.vocation)
    self:SetCurCloth(clothId, 0, 0, 0, 0, 0)
    self:SetCurHead(headId, 0, 0, 0, 0, 0)
end

function PlayerAvatar:ShowDefaultWeapon()
    local weaponId = RoleDataHelper.GetRoleDefaultWeaponByVocation(self.vocation)
    self:SetCurWeapon(weaponId, 0, 0, 0, 0, 0)
end

function PlayerAvatar:ShowCurClothAndHead()
    local clothId = RoleDataHelper.GetRoleDefaultClothByVocation(self.vocation)
    local headId = RoleDataHelper.GetRoleDefaultHeadByVocation(self.vocation)
    
    --装备模型
    local equipList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetLoadedEquipList()
    local itemId = equipList[3]
    if itemId then
        --衣服
        local model = ItemDataHelper.GetEquipModel(itemId)
        if model > 0 then
            clothId = ItemDataHelper.GetEquipModel(itemId)
        end
    end
    
    --时装模型
    local fashinCloth = self.fashion_info[public_config.FASHION_TYPE_CLOTHES]
    if fashinCloth ~= nil then
        local fashionId = fashinCloth[public_config.FASHION_INFO_EQUIPPED]
        if fashionId ~= nil and fashionId > 0 then
            clothId = FashionDataHelper.GetFashionModel(fashionId)
            headId = FashionDataHelper.GetFashionHeadModel(fashionId)
        end
    end
    
    self:SetCurCloth(clothId, 0, 0, 0, 0, 0)
    self:SetCurHead(headId, 0, 0, 0, 0, 0)
end

function PlayerAvatar:ShowCurWeapon()
    --时装武器>神兵>装备武器>默认
    local weaponId = RoleDataHelper.GetRoleDefaultWeaponByVocation(self.vocation)
    local godWeaponModel = TransformManager:GetWeaponModelId()
    if godWeaponModel ~= 0 then
        --神兵
        weaponId = godWeaponModel
    else
        local equipList = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetLoadedEquipList()
        local loadedFashion
        local itemId = equipList[1]
        if itemId then
            local model = ItemDataHelper.GetEquipModel(itemId)
            if model > 0 then
                weaponId = ItemDataHelper.GetEquipModel(itemId)
            end
        end
    end
    
    --时装模型
    local fashinCloth = self.fashion_info[public_config.FASHION_TYPE_WEAPON]
    if fashinCloth ~= nil then
        local fashionId = fashinCloth[public_config.FASHION_INFO_EQUIPPED]
        if fashionId ~= nil and fashionId > 0 then
            weaponId = FashionDataHelper.GetFashionModel(fashionId)
        end
    end
    
    self:SetCurWeapon(weaponId, 0, 0, 0, 0, 0)
end

--锁定目标id 0表示无选中目标
function PlayerAvatar:RefreshTargetId(id)
    local entity = EntityMgr:GetEntity(id)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Target_Id, id)
    if entity == nil then
        EventDispatcher:TriggerEvent(GameEvents.SHOW_OTHER_PLAYER_HEAD, nil, false)
        return
    end
    --攻击玩家
    if entity.entityType == EntityType.Avatar then
        EventDispatcher:TriggerEvent(GameEvents.SHOW_OTHER_PLAYER_HEAD, entity, true)
    else
        EventDispatcher:TriggerEvent(GameEvents.SHOW_OTHER_PLAYER_HEAD, nil, false)
    end
    
    if entity.entityType == EntityType.Monster or entity.entityType == EntityType.Dummy then
        EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, id)
    end
end

function PlayerAvatar:EnterSpaceResp(map_id, map_line, x, y, z)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("EnterSpaceResp map_id:%s map_line:%s - %s,%s,%s", map_id,
            map_line, x, y, z))
    end
    self:SetCityState(false)
    x = x * 0.01
    y = y * 0.01
    z = z * 0.01
    local enterPos = Vector3(x, y, z)
    self.map_line = map_line
    GameSceneManager:EnterSceneByID(map_id, self.map_line ~= map_line, enterPos, function(teleportToPoint)
        -- GameWorld.Player():Teleport(enterPos)
        end)
    -- LoadingBarManager:ShowProgressBar(0.1)
    GameStateManager.SetState(GAME_STATE.SCENE)
--InstanceManager:CheckRaidInstance(map_id)
end

--礼包类型返回
function PlayerAvatar:on_bag_action_resp(act_id, code, args)
    --on_bag_action_resp(action_config.ITEM_USE_REQ,0,{item_id})
    --BagManager:OnItemUse(code,args)
    --BagManager:HandleItemResp(act_id, code, args)
    BagManager:OnBagActionResp(act_id, code, args)
end



---------------------后端设置同步-----------------------
function PlayerAvatar:on_enter_space_resp(map_id, map_line, x, y, z)
    x = x or 0
    y = y or 0
    z = z or 0
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("on_enter_space_resp map_id:%s map_line:%s - %s,%s,%s", map_id,
            map_line, x, y, z))
    end
    GameWorld.SetSceneId(map_id)
    ResourceMappingManager:CheckResourceReady(function()self:EnterSpaceResp(map_id, map_line, x, y, z) end)
end

--服务器同步属性，图鉴背包 LuaTable
function PlayerAvatar:set_pkg_card_piece(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_CARD_PIECE, self.pkg_card_piece, change)
end

--服务器同步属性，物品背包 LuaTable
function PlayerAvatar:set_pkg_items(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_ITEMS, self.pkg_items, change)
end

--服务器同步属性，仓库  LuaTable()
function PlayerAvatar:set_pkg_warehouse(change)
    BagManager:OnChanged(PkgConfig.WAREHOUSE_TYPE_PKG_ITEMS, self.pkg_warehouse, change)
end

--服务器同步属性，身上装备  LuaTable()
function PlayerAvatar:set_pkg_loaded_equip(change)
    BagManager:OnChanged(PkgConfig.LOADED_EQUIP_TYPE_PKG_ITEMS, self.pkg_loaded_equip, change)
end

--时装背包格子数量
function PlayerAvatar:set_pkg_fashion_size(change)
    BagManager:OnVoulumnChanged(public_config.PKG_TYPE_FASHION)
end

--服务器同步属性，身上时装  LuaTable()
function PlayerAvatar:set_pkg_loaded_fashion(change)
    -- LoggerHelper.Log("pkg_loaded_fashion")
    -- LoggerHelper.Log(self.pkg_loaded_fashion)
    BagManager:OnChanged(PkgConfig.LOADED_FASHION_TYPE_PKG_FASHION, self.pkg_loaded_fashion, change)
end

--服务器同步属性，身上魂器 LuaTable
function PlayerAvatar:set_pkg_equip_soul(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_EQUIP_SOUL, self.pkg_equip_soul, change)
end


--背包格子数量
function PlayerAvatar:set_pkg_items_size(change)
    BagManager:OnVoulumnChanged(public_config.PKG_TYPE_ITEM)
end

--仓库格子数量
function PlayerAvatar:set_pkg_warehouse_size(change)
    BagManager:OnVoulumnChanged(public_config.PKG_TYPE_WAREHOUSE)
end

--背包整理
function PlayerAvatar:on_sort_item_resp(pkgType, luaTable)
    BagManager:OnSort(pkgType, luaTable)
end

--道具分解完成
function PlayerAvatar:item_break_resp(luaTable)
    BagManager:OnDecomposeComplete(luaTable)
end
--血量变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_cur_hp(change)
    EventDispatcher:TriggerEvent(GameEvents.PROPERTY_CUR_HP_CHANGE, change)
end

----------------------------------------------------货币变化-------------------------------------------------------------
--钻石变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_coupons(oldValue)
    FloatTextManager:OnMoneyChange(self.money_coupons, public_config.MONEY_TYPE_COUPONS, oldValue)
end

--绑钻变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_coupons_bind(oldValue)
    FloatTextManager:OnMoneyChange(self.money_coupons_bind, public_config.MONEY_TYPE_COUPONS_BIND, oldValue)
end

--金币变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_gold(oldValue)
    if oldValue == nil then
        return
    end
    self._goldTotalChange = self._goldTotalChange + self.money_gold - oldValue
    if self._goldTimer then
        TimerHeap:DelTimer(self._goldTimer)
        self._goldTimer = nil
    end
    self._goldTimer = TimerHeap:AddTimer(500, 0, 0, self._goldChangeCB)
end

function PlayerAvatar:ShowMoneyGoldFloatText()
    local oldValue = math.max(self.money_gold - self._goldTotalChange, 0)
    FloatTextManager:OnMoneyChange(self.money_gold, public_config.MONEY_TYPE_GOLD, oldValue)
    self._goldTotalChange = 0
end

--公会贡献变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_guild_contribute(oldValue)
    FloatTextManager:OnMoneyChange(self.money_guild_contribute, public_config.MONEY_TYPE_GUILD_CONTRIBUTE, oldValue)
end

--竞技点变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_athletics(oldValue)
    FloatTextManager:OnMoneyChange(self.money_athletics, public_config.MONEY_TYPE_ATHLETICS, oldValue)
end

--守护结晶变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_guardian(oldValue)
    FloatTextManager:OnMoneyChange(self.money_guardian, public_config.MONEY_TYPE_GUARDIAN_DIAMOND, oldValue)
end

--竞技点变化,可能使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_money_dragon_piece(oldValue)
    DragonSoulManager:OnMoneyChange(self.money_dragon_piece, oldValue)
end
----------------------------------------------------货币变化End-------------------------------------------------------------
--战斗力变化,使用到变化量,不走AddProptertyListener
function PlayerAvatar:set_fight_force(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.PROPERTY_FIGHT_FORCE_CHANGE, oldValue)
end

--战斗力状态  --服务器已废弃
function PlayerAvatar:set_is_fight(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.PROPERTY_IS_FIGHT_CHANGE, oldValue)
end

function PlayerAvatar:set_watch_war_status(oldValue)
    if self.watch_war_status == 1 then
        self.isWatchWar = true
    else
        self.isWatchWar = false
    end
end

function PlayerAvatar:set_pet_eid(oldValue)
    self.cs.pet_eid = self.pet_eid
end

-- --同步技能学习
-- function PlayerAvatar:set_learned_spells(change)
--     LoggerHelper.Error("PlayerAvatar:set_learned_spells")
--     SkillManager:OnLearnedChanged(change)
-- end
-- --同步技能装备
-- function PlayerAvatar:set_spell_slots(change)
--     LoggerHelper.Error("PlayerAvatar:set_spell_slots")
--     SkillManager:OnSlotChanged(change)
-- end
-----------------------------副本每日次数 类型 次数----------------------------------------------
function PlayerAvatar:set_instance_type_info(change)
    InstanceManager:OnInstInfoChange(change)
end
--function PlayerAvatar:InState(state)
--    return self.cs:InState(state)
--end
function PlayerAvatar:RemoveBuffer(buffer_id)
    self.cs:RemoveBuffer(buffer_id)
end

function PlayerAvatar:FlyControl(start_fly, end_fly, rotation_rate, hspeed, hdeceleration, v_speed, gravity, min_height, cx, cy, cz)
    self.cs:FlyControl(start_fly, end_fly, rotation_rate, hspeed, hdeceleration, v_speed, gravity, min_height, cx, cy, cz)
end

function PlayerAvatar:sync_time_resp(serverTime)
    -- LoggerHelper.Log(string.format("sync_time_resp %s , %s ", tostring(serverTime), tostring(os.time())), "#00fff0")
    DateTimeUtil.ResetServerTime(serverTime)
end

function PlayerAvatar:heart_beat_resp()
    --LoggerHelper.Log("heart_beat_resp")
    EventDispatcher:TriggerEvent(GameEvents.Update_Heart_Beat)
end

function PlayerAvatar:on_multi_login()
    GameManager.GameStateManager.ReturnLogin(true)
end

function PlayerAvatar:OnActorLoaded()
    self.actorHasLoaded = true
    local selected = CreateRoleManager.CurrentSelectedCharacter
    
    --变身情况
    if self:InState(StateConfig.AVATAR_STATE_TRANSFORM) then
        --self:SetCurHead(-1, 0, 0, 0, 0, 0)
        --self:SetCurCloth(-1, 0, 0, 0, 0, 0)
        --self:SetCurWeapon(-1, 0, 0, 0, 0, 0)
        end
    
    self:SetReady(true)
    self:SetActorMode(1)
    --新手关检查是否加载完玩家然后开始飞行
    --FlyTriggerManager:CheckStartFly()
    self:DramaShow(self._dramaShow)
    self:set_horse_ride_model()
    self:set_guard_goddess_id(self.guard_goddess_id)
    PartnerManager:HorseRideReq()
    --宝物显示
    TransformManager:InitPlayerModel()
end

function PlayerAvatar:DramaShow(visible)
    self._dramaShow = visible
    self:SetVisible(self._dramaShow)
end

function PlayerAvatar:StartRide()
    PartnerManager:HorseRideReq()
end

function PlayerAvatar:EndRide()
    if self.horse_grade_ride ~= 0 then
        PartnerManager:RideHideReq()
    end
end


function PlayerAvatar:OnDeath()
    EventDispatcher:TriggerEvent(GameEvents.PlayerDie)
    self:EndRide()
end

function PlayerAvatar:OnRelive()
    GameManager.GUIManager.ClosePanel(PanelsConfig.Redemption)
end

function PlayerAvatar:OnEnterTransform()
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
    EventDispatcher:TriggerEvent(GameEvents.Show_Fix_Func_Buttons, false)
end

function PlayerAvatar:OnLeaveTransform()
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Buttons_State)
    EventDispatcher:TriggerEvent(GameEvents.Show_Fix_Func_Buttons, true)
end

function PlayerAvatar:SetCurrMapID(map_id)
    self.cs.map_id = map_id
end

function PlayerAvatar:SetPreviewState(state)
    self.cs.isPreviewState = state
end

---------------服务器回调函数
function PlayerAvatar:pkg_add_resp(table_id, pos, item, get_way)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("pkg_add_resp %s ： %s - %s - %s", tostring(table_id), tostring(pos), tostring(item), tostring(get_way)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    local svrd = GameWorld.CSLuaStringToLuaTable(item, "pkg_add_resp: " .. propName)
    self[propName][pos] = svrd
    BagManager:OnChanged(propName, self[propName], {[pos] = svrd}, get_way)
end

function PlayerAvatar:table_update_all_resp(table_id, item)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("table_update_all_resp %s ： %s", tostring(table_id), tostring(item)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    local svrd = GameWorld.CSLuaStringToLuaTable(item, "table_update_all_resp: " .. propName)
    self:UpdateTableAll(propName, svrd)
end

function PlayerAvatar:table_update_resp(table_id, item)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("table_update_resp %s ： %s", tostring(table_id), tostring(item)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    local svrd = GameWorld.CSLuaStringToLuaTable(item, "table_update_resp: " .. propName)
    self:UpdateTableAttr(propName, svrd)
end

function PlayerAvatar:table_remove_resp(table_id, key)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("table_remove_resp %s ： %s", tostring(table_id), tostring(key)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    self:RemoveFromTableAttr(propName, key)
end

function PlayerAvatar:table_update_dp2_resp(table_id, item)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("table_update_dp2_resp %s ： %s", tostring(table_id), tostring(item)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    local svrd = GameWorld.CSLuaStringToLuaTable(item, "table_update_dp2_resp: " .. propName)
    self:UpdateTableAttr2(propName, svrd)
end

function PlayerAvatar:table_remove_dp2_resp(table_id, key1, key2)
    if GameWorld.isEditor then
        LoggerHelper.Log(string.format("table_remove_dp2_resp %s ： %s - %s", tostring(table_id), tostring(key1), tostring(key2)), "#00ff00")
    end
    local propName = GameWorld.GetPropertyNameById(table_id)
    self:RemoveFromTableAttr2(propName, key1, key2)
end

function PlayerAvatar:table_clear_resp(table_id)
    local propName = GameWorld.GetPropertyNameById(table_id)
    self:ClearTable(propName)
end

--type 1=登陆跨天 2=在线跨天
function PlayerAvatar:on_reset_data_resp(type)

end

function PlayerAvatar:on_sys_info_resp(code, args)
    SystemInfoManager:ShowRespTip(0, code, args)
end

function PlayerAvatar:on_sys_info_text_resp(code, content)
    SystemInfoManager.ShowTextByType(code, content)
end

function PlayerAvatar:on_action_resp(act_id, code, args)
    if GameWorld.isEditor then
        LoggerHelper.Log("on_action_resp " .. act_id .. " code " .. code)
    end
    self:ActionResponseHandler(act_id, code, args)
    --错误码大于0执行错误处理
    if code > 0 then
        SystemInfoManager:ShowRespTip(act_id, code, args)
    --LoggerHelper.Error("on_action_resp " .. act_id .. " Error " .. code .. " args: " .. PrintTable:TableToStr(args))
    end
end

--初始朝向时机
function PlayerAvatar:on_face_y_resp(rotationY)
    rotationY = math.floor(rotationY * 0.5)
    self.cs.face = Vector3.New(0, rotationY, 0)
end

--注册返回处理方法
function PlayerAvatar:ActionResponseRegister()
    self._actionRespFunList = {}
    --组队系统
    self._actionRespFunList[action_config.TEAM_CREATE] = function(act_id, code, args)TeamManager:OnCreateTeam(code) end
    --任务系统
    self._actionRespFunList[action_config.TASK_ACCEPT] = function(act_id, code, args)TaskManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.TASK_COMMIT] = function(act_id, code, args)TaskManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.TASK_PROCESSING_LIST] = function(act_id, code, args)TaskManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.TASK_COMMITED_LIST_BRANCH] = function(act_id, code, args)TaskManager:HandleResp(act_id, code, args) end
    --装备系统
    self._actionRespFunList[action_config.EQUIP_STREN] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_WASH] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_LOAD] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_GEM_REFINE] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_GEM_LOAD] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_GEM_UNLOAD] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.EQUIP_ADVANCE_REQ] = function(act_id, code, args)EquipManager:HandleResp(act_id, code, args) end
    --副本系统
    self._actionRespFunList[action_config.ITEM_USE_LITTLE_SHOE] = function(act_id, code, args)InstanceManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_EXIT] = function(act_id, code, args)InstanceManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.CLIENT_MAP_ENTER] = function(act_id, code, args)InstanceManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_ENTER] = function(act_id, code, args)InstanceManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_CLIENT_TELEPORT] = function(act_id, code, args)MapManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.SPACE_GET_LINE_INFO] = function(act_id, code, args)MapManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.SPACE_GO_TO_LINE] = function(act_id, code, args)MapManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_LIST_REQ] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_REFRESH] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_COMMIT] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_POINT_REWARD_REQ] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_GROUP_REQ] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DAILY_TASK_WEEK_POINT_REWARD_REQ] = function(act_id, code, args)DailyActivityManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.AION_ENTER] = function(act_id, code, args)AionTowerManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.WORLD_BOSS_ENTER] = function(act_id, code, args)WorldBossManager:HandleData(act_id, code, args) end
    -- self._actionRespFunList[action_config.AION_RESET_LAYER] = function(act_id, code, args)AionTowerManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.AION_SWEEP] = function(act_id, code, args)AionTowerManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.AION_SWEEP_COST_GOLD] = function(act_id, code, args)AionTowerManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.AION_GET_FIRST_REWARD] = function(act_id, code, args)AionTowerManager:HandleResp(act_id, code, args) end
    --玩家加成
    self._actionRespFunList[action_config.AVATAR_UPDATE_ADD_RATE] = function(act_id, code, args)AddRateManager:HandleData(act_id, code, args) end
    --经验副本信息
    self._actionRespFunList[action_config.INSTANCE_EXP_ENTER] = function(act_id, code, args)ExpInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_EXP_NOTIFY_INFO] = function(act_id, code, args)ExpInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_EXP_SETTLE] = function(act_id, code, args)ExpInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_EXP_BUY_ENTER_NUM] = function(act_id, code, args)ExpInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_EXP_NOTIFY_ROOM_STAGE] = function(act_id, code, args)ExpInstanceManager:HandleData(act_id, code, args) end
    --成就系统
    self._actionRespFunList[action_config.ACHIEVEMENT_COMMIT] = function(act_id, code, args)AchievementManager:HandleData(act_id, code, args) end
    --七天登录系统
    self._actionRespFunList[action_config.REWARD_SEVENDAY_GET_REWARD] = function(act_id, code, args)SevenDayLoginManager:HandleData(act_id, code, args) end
    --符文
    self._actionRespFunList[action_config.RUNE_PUTON] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_PUTOFF] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_REPLACE] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_UPLEVEL] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_EXPLODE] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_EXCHANGE] = function(act_id, code, args)RuneManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_SINGLE_LUCK] = function(act_id, code, args)PreciousManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_TEN_LUCK] = function(act_id, code, args)PreciousManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.RUNE_LUCK_LIST] = function(act_id, code, args)PreciousManager:HandleResp(act_id, code, args) end
    --守护结界塔系统
    self._actionRespFunList[action_config.HOLY_TOWER_ENTER] = function(act_id, code, args)PetInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.HOLY_TOWER_BUY_ENTER_NUM] = function(act_id, code, args)PetInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.HOLY_TOWER_SWEEP] = function(act_id, code, args)PetInstanceManager:HandleData(act_id, code, args) end
    --装备副本（圣灵血阵）
    --众神宝库系统
    self._actionRespFunList[action_config.INSTANCE_GOLD_ENTER] = function(act_id, code, args)MoneyInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_GOLD_BUY_ENTER_NUM] = function(act_id, code, args)MoneyInstanceManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.INSTANCE_GOLD_SWEEP] = function(act_id, code, args)MoneyInstanceManager:HandleData(act_id, code, args) end
    --赏金任务系统
    self._actionRespFunList[action_config.CIRCLE_TASK_COMMIT_REQ] = function(act_id, code, args)CircleTaskManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.CIRCLE_TASK_QUICK_COMPLETE] = function(act_id, code, args)CircleTaskManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.CIRCLE_TASK_ONE_CLICK] = function(act_id, code, args)CircleTaskManager:HandleData(act_id, code, args) end
    --周常任务系统
    self._actionRespFunList[action_config.WEEK_TASK_COMMIT_REQ] = function(act_id, code, args)WeekTaskManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.WEEK_TASK_QUICK_COMPLETE] = function(act_id, code, args)WeekTaskManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.WEEK_TASK_ONE_CLICK] = function(act_id, code, args)WeekTaskManager:HandleData(act_id, code, args) end
    --合成系统
    self._actionRespFunList[action_config.COMPOSE_ITEM] = function(act_id, code, args)ComposeManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.COMPOSE_EQUIP] = function(act_id, code, args)ComposeManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.DRAGON_SPIRIT_COMPOSE] = function(act_id, code, args)ComposeManager:HandleResp(act_id, code, args) end
    --转职系统
    self._actionRespFunList[action_config.VOC_CHANGE_FOUR_ACTIVATE] = function(act_id, code, args)VocationChangeManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.VOC_CHANGE_FOUR_APPLY_SUCC] = function(act_id, code, args)VocationChangeManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.ONE_KEY_FINISH] = function(act_id, code, args)VocationChangeManager:HandleData(act_id, code, args) end
    
    --日常系统
    self._actionRespFunList[action_config.ACTIVITY_POINT_REWARD_REQ] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.ACTIVITY_SHOW] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.ACTIVITY_UNSHOW] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.ACTIVITY_LEVEL_UP] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.REWARD_BACK_GET_INFO] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.REWARD_BACK_GET_REWARD] = function(act_id, code, args)DailyActivityExManager:HandleData(act_id, code, args) end
    
    --护送女神系统
    self._actionRespFunList[action_config.GUARD_GODDNESS_GUARD] = function(action_id, error_code, args)EscortPeriManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GUARD_GODDNESS_GUARD_FINISH] = function(action_id, error_code, args)EscortPeriManager:HandleData(action_id, error_code, args) end
    
    --开心答题
    self._actionRespFunList[action_config.QUESTION_ENTER] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.QUESTION_ANSWER] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.QUESTION_NOTIFY_STATE] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.QUESTION_NOTIFY_EXP] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.QUESTION_RANK_LIST] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.QUESTION_NOTIFY_ANSWER] = function(action_id, error_code, args)AnswerManager:HandleData(action_id, error_code, args) end
    
    --神兽
    self._actionRespFunList[action_config.GOD_MONSTER_EQUIP_LOAD] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_EQUIP_UNLOAD] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_EQUIP_UNLOAD_ALL] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_EQUIP_STREN] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_ASSIST] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_RECALL] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.GOD_MONSTER_ADD_ASSIST] = function(action_id, error_code, args)AnimalManager:HandleData(action_id, error_code, args) end
    
    --系统设置
    self._actionRespFunList[action_config.SETTING_SET_SECURITY_LOCK_REQ] = function(action_id, error_code, args)SystemSettingManager:HandleData(action_id, error_code, args) end
    
    -- 离线竞技场
    self._actionRespFunList[action_config.OFFLINE_PVP_REFRESH] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_CHALLENGE] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_INSPIRE] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_BUY] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_SETTLE] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_GET_INFO] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.OFFLINE_PVP_GET_REWARD] = function(action_id, error_code, args)AthleticsManager:HandleData(action_id, error_code, args) end
    
    -- 天梯赛
    self._actionRespFunList[action_config.CROSS_VS11_BEGIN_MATCH_REQ] = function(action_id, error_code, args)OnlinePvpManager:HandleData(action_id, error_code, args) end
    -- 祈祷
    self._actionRespFunList[action_config.PRAY_GOLD_REQ] = function(action_id, error_code, args)PrayManager:HandleData(action_id, error_code, args) end
    
    --VIP系统
    self._actionRespFunList[action_config.VIP_LEVEL_REWARD_REQ] = function(action_id, error_code, args)VIPManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.VIP_WEEK_REWARD_REQ] = function(action_id, error_code, args)VIPManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.VIP_DAILY_REWARD_REQ] = function(action_id, error_code, args)VIPManager:HandleResp(action_id, error_code, args) end
    
    --时装系统
    self._actionRespFunList[action_config.FASHION_LOAD] = function(action_id, error_code, args)FashionManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.FASHION_UNLOAD] = function(action_id, error_code, args)FashionManager:HandleData(action_id, error_code, args) end
    -- 图鉴
    self._actionRespFunList[action_config.CARD_UPLEVEL_REQ] = function(action_id, error_code, args)CardManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.CARD_GROUP_UPLEVEL_REQ] = function(action_id, error_code, args)CardManager:HandleData(action_id, error_code, args) end
    
    --采集东西
    self._actionRespFunList[action_config.COLLECT_REQ] = function(action_id, error_code, args)CollectManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.COLLECT_PRE_REQ] = function(action_id, error_code, args)CollectManager:HandleData(action_id, error_code, args) end
    --守护
    self._actionRespFunList[action_config.GUARDIAN_EXPLAIN] = function(action_id, error_code, args)PartnerManager:GuardianDecomposeComplete(error_code) end
    self._actionRespFunList[action_config.GUARDIAN_UPLEVEL] = function(action_id, error_code, args)PartnerManager:GuardianUpgradeComplete(error_code) end
    
    --幻化
    self._actionRespFunList[action_config.ILLUSION_ACTIVATE] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ILLUSION_UPGRADE_STAR] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ILLUSION_UPGRADE_GRADE] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ILLUSION_LEVEL_UP] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ILLUSION_BREAK] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ILLUSION_SHOW] = function(action_id, error_code, args)TransformManager:HandleResp(action_id, error_code, args) end
    
    --好友系统
    self._actionRespFunList[action_config.FRIEND_RECOMMEND] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_SEARDH] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_APPLY_TO] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_APPLY_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_ACCEPT] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_REFUSE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_DELE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_BLACK] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_BLACK_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_BLACK_DELE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_NEW_CHAT_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_FOE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_FOE_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_FOE_DELE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_FRIENDLY_RESP] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_UNREAD_LIST] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_ONLINE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.FRIEND_OFFLINE] = function(act_id, code, args)FriendManager:HandleResp(act_id, code, args) end
    
    --组队
    self._actionRespFunList[action_config.TEAM_APPLY] = function(act_id, code, args)TeamManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.TEAM_APPLY_BY_DBID] = function(act_id, code, args)TeamManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.TEAM_MATCH] = function(act_id, code, args)TeamManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.TEAM_CANCEL_MATCH] = function(act_id, code, args)TeamManager:HandleData(act_id, code, args) end
    
    --个人boss
    self._actionRespFunList[action_config.PERSONAL_BOSS_ENTER] = function(act_id, code, args)PersonalBossManager:HandleData(act_id, code, args) end
    --BOSS之家
    self._actionRespFunList[action_config.BOSS_HOME_ENTER] = function(act_id, code, args)BossHomeManager:HandleData(act_id, code, args) end
    --黑暗禁地
    self._actionRespFunList[action_config.FORBIDDEN_ENTER] = function(act_id, code, args)BackWoodsManager:HandleData(act_id, code, args) end
    --神兽岛
    self._actionRespFunList[action_config.GOD_ISLAND_ENTER] = function(act_id, code, args)GodIslandManager:HandleData(act_id, code, args) end
    --龙魂要塞
    self._actionRespFunList[action_config.TOWER_DEFENSE_ENTER] = function(act_id, code, args)TowerInstanceManager:HandleData(act_id, code, args) end
    
    -- 龙魂
    self._actionRespFunList[action_config.DRAGON_SPIRIT_PUTON] = function(act_id, code, args)DragonSoulManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.DRAGON_SPIRIT_PUTOFF] = function(act_id, code, args)DragonSoulManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.DRAGON_SPIRIT_UPLEVEL] = function(act_id, code, args)DragonSoulManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.DRAGON_SPIRIT_RESOLVE] = function(act_id, code, args)DragonSoulManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.DRAGON_SPIRIT_RESTORE] = function(act_id, code, args)DragonSoulManager:HandleData(act_id, code, args) end
    
    -- 结婚
    self._actionRespFunList[action_config.MARRY_PROPOSE] = function(act_id, code, args)MarriageManager:HandleData(act_id, code, args) end
    --结婚养成start--
    -- 婚戒
    self._actionRespFunList[action_config.MARRY_RING_PROMOTE] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    -- 仙娃
    self._actionRespFunList[action_config.MARRY_PET_GRADE_UP] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_PET_UNLOCK] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    -- 副本
    self._actionRespFunList[action_config.MARRY_INSTANCE_ENTER] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_INSTANCE_PURCHASE_EXTRA_TIME_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_INSTANCE_SETTLE] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_INSTANCE_ADD_MATE_TIME] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_INSTANCE_BEG_MATE_TIME_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_INSTANCE_BE_BEGGED_MATE_TIME_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    -- 宝匣
    self._actionRespFunList[action_config.MARRY_BOX_PURCHASE_MARRY_BOX_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_BOX_DAILY_REFUND_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.MARRY_BOX_REFUND_REQ] = function(act_id, code, args)MarriageRaiseManager:HandleData(act_id, code, args) end
    --结婚养成end--
    -- 查看玩家
    self._actionRespFunList[action_config.PEEP_SEND_INFO] = function(act_id, code, args)CheckOtherPlayerManager:HandleResp(act_id, code, args) end
    -- 背包
    self._actionRespFunList[action_config.ITEM_SELL_REQ] = function(act_id, code, args)BagManager:OnSellComplete(code) end
    self._actionRespFunList[action_config.ITEM_SORT_REQ] = function(act_id, code, args)BagManager:OnSortComplete(code) end
    
    
    -- 送花、收花
    self._actionRespFunList[action_config.SEND_FLOWER_SEND] = function(act_id, code, args)MarriageManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.SEND_FLOWER_SEND] = function(act_id, code, args)FlowerManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.SEND_FLOWER_RECEIVE] = function(act_id, code, args)FlowerManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.SEND_FLOWER_KISS] = function(act_id, code, args)FlowerManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.SEND_FLOWER_KISS_BACK] = function(act_id, code, args)FlowerManager:HandleData(act_id, code, args) end
    self._actionRespFunList[action_config.SEND_FLOWER_EFFECT] = function(act_id, code, args)FlowerManager:HandleData(act_id, code, args) end
    
    -- 云购
    self._actionRespFunList[action_config.ROULETTE_REWARD] = function(act_id, code, args)RouletteManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.ROULETTE_PERSONAL_LIST] = function(act_id, code, args)RouletteManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.ROULETTE_BIG_REWARD_LIST] = function(act_id, code, args)RouletteManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.ROULETTE_TOTAL_LUCK_TIMES_REQ] = function(act_id, code, args)RouletteManager:HandleResp(act_id, code, args) end
    
    --开服活动
    self._actionRespFunList[action_config.MARRY_RANK_GET_RANK_LIST_REQ] = function(act_id, code, args)ServiceActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DEMON_RING_SUB_TARGET_REWARD_REQ] = function(act_id, code, args) ServiceActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DEMON_RING_UNLOCK_REQ] = function(act_id, code, args) ServiceActivityManager:HandleResp(act_id, code, args) end
    -- self._actionRespFunList[action_config.DEMON_RING_GET_INFO_REQ] = function(act_id, code, args) ServiceActivityManager:HandleResp(act_id, code, args) end
    self._actionRespFunList[action_config.COLLECT_WORD_EXCHANGE] = function(act_id, code, args)ServiceActivityManager:HandleResp(act_id, code, args) end
    
    --投资
    self._actionRespFunList[action_config.INVEST_VIP_PAY] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.INVEST_VIP_GET_REWARD] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.LV_INVEST_PAY] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.LV_INVEST_GET_REWARD] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.INVESTMENT_PURCHASE_MONTH_CARD_REQ] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.INVESTMENT_MONTH_CARD_DAILY_REFUND_REQ] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    self._actionRespFunList[action_config.CUMULATIVE_CHARGE_REFUND_REQ] = function(action_id, error_code, args)InvestManager:HandleData(action_id, error_code, args) end
    
    
    
    self._actionRespFunList[action_config.MAIL_GET_ATTACHMENT_REQ] = function(action_id, error_code, args)MailManager:OnMailRece(action_id, error_code, args) end
    
    --许愿
    self._actionRespFunList[action_config.WISH_WISH] = function(action_id, error_code, args)HappyWishManager:HandleData(action_id, error_code, args) end
    
    --运营活动
    self._actionRespFunList[action_config.HOLY_WEAPON_RANK_GET_INFO] = function(action_id, error_code, args)OperatingActivityManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.ACTIVITY_FIREWORK_EFFECT] = function(action_id, error_code, args)OperatingActivityManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.BATTLE_SOUL_ENTER] = function(action_id, error_code, args)OperatingActivityManager:HandleResp(action_id, error_code, args) end
    
    --鉴宝
    self._actionRespFunList[action_config.JIANBAO_REWARD] = function(action_id, error_code, args)IdentifyManager:HandleData(action_id, error_code, args) end
    
    -- 特卖
    self._actionRespFunList[action_config.DAZZLE_SALE_BUY] = function(action_id, error_code, args)SaleManager:HandleResp(action_id, error_code, args) end
    
    --悬赏
    self._actionRespFunList[action_config.BOSS_REWARD_UNLOCK_REQ] = function(action_id, error_code, args)BossOfferManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.BOSS_REWARD_GET_INFO_REQ] = function(action_id, error_code, args)BossOfferManager:HandleResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.BOSS_REWARD_REFRESH] = function(action_id, error_code, args)BossOfferManager:HandleResp(action_id, error_code, args) end
    
    --天降秘宝
    self._actionRespFunList[action_config.SKY_DROP_COUNT_DOWN] = function(action_id, error_code, args)SkyDropManager:HandleData(action_id, error_code, args) end
    
    -- 限时抢购
    self._actionRespFunList[action_config.LIMITED_SALE_BUY] = function(action_id, error_code, args)LimitedShoppingManager:OnLimitedShoppingActionResp(action_id, error_code, args) end
    -- vip礼包
    self._actionRespFunList[action_config.VIP_LEVEL_UP_REWARD] = function(action_id, error_code, args) VIPLevelGiftManager:OnVipGiftActionResp(action_id, error_code, args) end
    self._actionRespFunList[action_config.VIP_LEVEL_UP_RECORD] = function(action_id, error_code, args) VIPLevelGiftManager:OnVipGiftActionResp(action_id, error_code, args) end

    --星脉
    self._actionRespFunList[action_config.ZODIAC_UPDATE_REQ] = function(action_id, error_code, args) StarManager:HandleResp(action_id, error_code, args) end

end

--开宗立派
function PlayerAvatar:on_fraud_guild_action_resp(action_id, error_code, args)
    ServiceActivityManager:HandleResp(action_id, error_code, args)
end



--翻牌集福
function PlayerAvatar:on_open_card_action_resp(action_id, error_code, args)
    CardLuckManager:HandleData(action_id, error_code, args)
end


-- 福利系统
function PlayerAvatar:on_benefit_action_resp(action_id, error_code, args)
    WelfareManager:HandleData(action_id, error_code, args)
end

-- 结婚养成系统
--(婚戒)
function PlayerAvatar:on_marry_ring_action_resp(action_id, error_code, args)
    MarriageRaiseManager:HandleData(action_id, error_code, args)
end
-- (仙娃)
function PlayerAvatar:on_marry_pet_action_resp(action_id, error_code, args)
    MarriageRaiseManager:HandleData(action_id, error_code, args)
end
-- (副本)
function PlayerAvatar:on_marry_instance_action_resp(action_id, error_code, args)
    MarriageRaiseManager:HandleData(action_id, error_code, args)
end
-- (宝匣)
function PlayerAvatar:on_marry_box_action_resp(action_id, error_code, args)
    MarriageRaiseManager:HandleData(action_id, error_code, args)
end
-- 开服活动
function PlayerAvatar:on_open_server_rank_action_resp(action_id, error_code, args)
    ServiceActivityManager:HandleResp(action_id, error_code, args)
end

function PlayerAvatar:on_collect_word_action_resp(action_id, error_code, args)
    ServiceActivityManager:HandleResp(action_id, error_code, args)
end


function PlayerAvatar:on_demon_ring_action_resp(action_id, error_code, args)
    ServiceActivityManager:HandleResp(action_id, error_code, args)
end


function PlayerAvatar:on_marry_rank_action_resp(action_id, error_code, args)
    ServiceActivityManager:HandleResp(action_id, error_code, args)
end


--处理on_action_resp
function PlayerAvatar:ActionResponseHandler(act_id, code, args)
    if self._actionRespFunList[act_id] then
        self._actionRespFunList[act_id](act_id, code, args)
    end
end

function PlayerAvatar:cross_resp(ip, port, key)
    GameWorld.WriteInfoLog("cross_resp " .. ip .. " port: " .. port .. " key: " .. key)
    GameManager.LoginManager:CrossServer(ip, port, key)
end

-- 组队
function PlayerAvatar:on_team_action_resp(action_id, error_id, args)
    --LoggerHelper.Error("Team Action_id " .. action_id .. " error_id: " .. error_id)
    TeamManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_team_id(change)
    if change == nil then
        --设置刚上线时是否有队伍
        TeamManager:SetHasTeamInOnline(self.team_id > 0)
    end
end

-- 匹配状态 0:未匹配 其他:target_map_id
function PlayerAvatar:set_match_state(change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.MATCH_STATE_CHANGE)
    end
end

-- 任务
function PlayerAvatar:on_task_action_resp(event_id, error_id, args)
    TaskManager:HandleData(event_id, error_id, args)
end

--客户端获取战斗属性
function PlayerAvatar:attri_get_resp(luaTable)
    RoleManager:HandleData(luaTable)
end

function PlayerAvatar:on_instance_action_resp(action_id, error_id, args)
    -- LoggerHelper.Error("on_instance_action_resp(action_id, error_id, args)" .. tostring(action_id))
    if action_id == action_config.INSTANCE_NOTIFY_WORLD_BOSS_FAKE then --支线世界boss数据
        WorldBossManager:HandleData(action_id, error_id, args)
    else
        InstanceManager:HandleData(action_id, error_id, args)
    end
end

function PlayerAvatar:on_event_activity_action_resp(action_id, error_id, args)
    EventActivityManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_diff_level_resp(diff_level)
    EventDispatcher:TriggerEvent(GameEvents.ON_LEVEL_EXP_CHANGE, diff_level)
    if diff_level ~= 0 and self.actorHasLoaded then
        if self.horse_ride_model > 0 then
            self:PlaySfx(VisualFxConfig.LevelUpRide, 2)
        else
            self:PlaySfx(VisualFxConfig.LevelUp, 2)
        end
        GameWorld.PlaySound(8)
    end
end

function PlayerAvatar:set_level(change)
    --self._base.set_level(self, change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.OnGuideLevelUp, self.level)
        GameWorld.PlaySound(8)
        if self.level == GlobalParamsHelper.GetParamValue(778)[1] and self.first_charge_reward_time == 0 then
            GUIManager.ShowPanel(PanelsConfig.FirstChargeTips)
        end
        if self.level == GlobalParamsHelper.GetParamValue(778)[2] and self.first_charge_reward_time == 0 then
            GUIManager.ShowPanel(PanelsConfig.FirstCharge)
        end
    end
    
    if change and self.level and self.level > 1 and self.level > change then
        -- if GameLoader.SystemConfig.IsUsePlatformSdk then
        -- TimerHeap:AddSecTimer(1, 0, 0, function()
        local player = GameWorld.Player()
        if player == nil then
            return
        end
        -- LoggerHelper.Error("----------onUpdateRoleInfo,roleId:" .. LocalSetting.settingData.UserAccount
        --     .. ",roleName:" .. player.name
        --     .. ",serverId:" .. LocalSetting.settingData.SelectedServerID
        --     .. ",serverName:" .. ServerListManager:SelectedServerName()
        --     .. ',level:' .. self.level)
        GameWorld.ShowSDKLog("onUpdateRoleInfo", tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
            LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), tostring(player.money_coupons),
            player.guild_name, tostring(player.money_coupons), "-1")
    -- end)
    -- end
    end
    LoadingBarManager:SetPlayerLevel(self.level)
end

function PlayerAvatar:on_world_level_up_resp(level)
    self.world_level = level
    EventDispatcher:TriggerEvent(GameEvents.On_Get_World_Level)
end

function PlayerAvatar:on_achievement_action_resp(action_id, error_id, args)
    AchievementManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_reward_sevenday_action_resp(action_id, error_id, args)
    SevenDayLoginManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_holy_tower_action_resp(action_id, error_id, args)
    PetInstanceManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_instance_equipment_resp(action_id, error_id, args)
    EquipmentInstanceManager:HandleData(action_id, error_id, args)
end


function PlayerAvatar:on_circle_task_action_resp(action_id, error_id, args)
    CircleTaskManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_week_task_action_resp(action_id, error_id, args)
    WeekTaskManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_instance_gold_resp(action_id, error_id, args)
    MoneyInstanceManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:on_reset_data_0clock_resp(reason)
    if reason == public_config.RESET_DAY_REASON_ONLINE then
        --在线跨天
        EventDispatcher:TriggerEvent(GameEvents.On_0_Oclock)
    elseif reason == public_config.RESET_DAY_REASON_ANOTHER_DAY then
        --跨天登陆
        EventDispatcher:TriggerEvent(GameEvents.On_0_Oclock_Login)
    end
end

function PlayerAvatar:on_reset_data_week_0clock_resp(reason)
    if reason == public_config.RESET_DAY_REASON_ONLINE then
        --在线跨周
        EventDispatcher:TriggerEvent(GameEvents.On_Week_0_Oclock)
    elseif reason == public_config.RESET_DAY_REASON_ANOTHER_DAY then
        --跨周登陆
        EventDispatcher:TriggerEvent(GameEvents.On_Week_0_Oclock_Login)
    end
end

function PlayerAvatar:on_reset_data_month_0clock_resp(reason)
    if reason == public_config.RESET_DAY_REASON_ONLINE then
        --在线跨月
        EventDispatcher:TriggerEvent(GameEvents.On_Month_0_Oclock)
    elseif reason == public_config.RESET_DAY_REASON_ANOTHER_DAY then
        --跨月登陆
        EventDispatcher:TriggerEvent(GameEvents.On_Month_0_Oclock_Login)
    end
end

----------------物品掉落处理-------------------
-- function PlayerAvatar:create_own_drop_resp(own_drop_id, item_id, item_cnt, entitiy_x, entitiy_y, entitiy_z)
--     --LoggerHelper.Error("own_drop_id"..own_drop_id.."item_id"..item_id.."item_cnt"..item_cnt.."entitiy_x"..entitiy_x.."entitiy_y"..entitiy_y.."entitiy_z"..entitiy_z)
--     ClientEntityManager.CreateSpaceDropByClient(own_drop_id, item_id, item_cnt, Vector3(entitiy_x / 100, entitiy_y / 100, entitiy_z / 100))
-- end
-- function PlayerAvatar:destroy_own_drop_resp(own_drop_id, reason)
--     --LoggerHelper.Error("destroy_own_drop_resp"..own_drop_id)
--     ClientEntityManager.DestroySpaceDrop(own_drop_id, reason)
-- end
-- function PlayerAvatar:pick_own_drop_resp(error_id, own_drop_id, item_id, item_cnt)
-- --LoggerHelper.Error("pick_own_drop_resp"..own_drop_id)
-- end
function PlayerAvatar:pick_drop_item_resp(own_drop_id, item_Index)
    ClientEntityManager.DestroySpaceDrop(own_drop_id, item_Index, 1)
end

--经验掉落
function PlayerAvatar:add_own_drop_exp_resp(exp_add, rate)
    --int64位显示处理,末尾几位可能存在精度问题
    local str
    if exp_add > 1000000000000 then
        local tmp = exp_add / 1000000000000
        str = string.format("%s%s", math.floor(tmp), string.format("%012s", math.floor((tmp % 1) * 1000000000000)))
    else
        str = tostring(exp_add)
    end
    local text = ""
    if rate == 0 then
        text = string.format("+%s", str)
    else
        text = string.format("+%s(+%s%%)", str, rate)
    end
    GUIManager.ShowText(FloatTextType.ExpAdd, text)
end

-------------------复活/救赎--------------------------
--判断自己是否能复活
-- function PlayerAvatar:on_can_revive_resp(error_id, remain_cnt, remain_cd)
--     if error_id > 0 then
--         SystemInfoManager:ShowRespTip(nil, error_id, nil)
--     else
--         ReviveRescueManager:OnCanRevive(remain_cnt, remain_cd)
--     end
-- end
-- function PlayerAvatar:on_revive_resp(error_id)
--     if error_id > 0 then
--         SystemInfoManager:ShowRespTip(nil, error_id, nil)
--     else
--         ReviveRescueManager:OnRevive()
--         self:PlaySfx(VisualFxConfig.Revive)
--     end
-- end
function PlayerAvatar:revive_action_resp(action_id, error_code, args)
    ReviveRescueManager:HandleResp(action_id, error_code, args)
-- if error_id > 0 then
--     SystemInfoManager:ShowRespTip(nil, error_id, nil)
-- else
--     ReviveRescueManager:OnRevive()
--     self:PlaySfx(VisualFxConfig.Revive)
-- end
end

--主角玩家不继承该方法
function PlayerAvatar:SetUpChecking()

end

--判断救赎玩家是否能被救
function PlayerAvatar:on_can_rescue_resp(error_id, avatar_eid, remain_cnt, remain_cd)
    if error_id > 0 then
        SystemInfoManager:ShowRespTip(nil, error_id, nil)
    else
        ReviveRescueManager:OnCanRescue(avatar_eid, remain_cnt, remain_cd)
    end
end

function PlayerAvatar:on_rescue_resp(error_id, avatar_eid)
    if error_id > 0 then
        SystemInfoManager:ShowRespTip(nil, error_id, nil)
    else
        ReviveRescueManager:OnRescue(avatar_eid)
    end
end

--玩家被救
function PlayerAvatar:on_be_rescue_resp()
    ReviveRescueManager:OnBeRescued()
    self:PlaySfx(VisualFxConfig.Revive)
end

function PlayerAvatar:set_login_days()
    EventDispatcher:TriggerEvent(GameEvents.LOGIN_DAY_CHANGE)
end
--Raid 副本进度
function PlayerAvatar:set_raid_inst_data(change)
    InstanceManager:OnRaidInstanceChange()
end

function PlayerAvatar:on_clear_raid_inst_data_resp()
    InstanceManager:OnClearRaidInstData()
end

function PlayerAvatar:set_vocation(change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.ON_VOCATION_CHANGE)
    end
end

-------------------------------------------技能养成/装备处理---------------------------------------------
-- function PlayerAvatar:set_learned_spells(change)
--     LoggerHelper.Error("PlayerAvatar:set_learned_spells")
--     LoggerHelper.Log(self.learned_spells)
-- end
function PlayerAvatar:on_spell_change_resp(args)
    self.learned_spells = args
    --LoggerHelper.Error("on_spell_change_resp")
    -- LoggerHelper.Log(args)
    SkillManager:OnLearnedChanged()
end

function PlayerAvatar:on_spell_slot_resp(args)
    --LoggerHelper.Error("on_spell_slot_resp")
    -- LoggerHelper.Log(args)
    self.spell_slots = args
    SkillManager:OnSlotChanged()
end

function PlayerAvatar:on_spell_rune_resp(act_id, code, args)
    SkillManager:HandleRuneData(act_id, code, args)
end

-------------------------------------------技能系统---------------------------------------------
function PlayerAvatar:set_learned_skills(oldValue)
    SkillManager:RefreshLearnedSkills(oldValue)
end

function PlayerAvatar:set_learned_passive_spell(oldValue)
    SkillManager:RefreshLearnedPassiveSpell(oldValue)
end

function PlayerAvatar:set_skills_auto_cast_info(oldValue)
    SkillManager:RefreshSkillsAutoCastInfo(oldValue)
end

function PlayerAvatar:set_talent_info(oldValue)
    SkillManager:RefreshTalentInfo(oldValue)
end

function PlayerAvatar:set_talent_spend_info(oldValue)
    SkillManager:RefreshTalentSpendInfo(oldValue)
end

function PlayerAvatar:set_money_talent(oldValue)
    if oldValue ~= nil then
        SkillManager:RefreshAddTalentRedPointState()
    end
end

function PlayerAvatar:set_vip_level_up_info()
    VIPLevelGiftManager:UpdateVipGiftReddot()
end
-----------------------------------------指引----------------------------------
--已做过的指引
function PlayerAvatar:set_guide_data(change)
    EventDispatcher:TriggerEvent(GameEvents.ON_GUIDE_DATA_CHANGE, self.guide_data)
end

-----------------------------------------邮件
function PlayerAvatar:on_mail_action_resp(act_id, code, args)
    MailManager:OnMailRece(act_id, code, args)
end

----------------------------------------聊天相关----------------------------------------------------
function PlayerAvatar:chat_resp(channel_id, paramstr, contentstr, liststr)
    -- print('========================')
    --LoggerHelper.Error('channel_id:' .. channel_id .. ' paramstr:' .. paramstr .. ' contentstr:' .. contentstr .. ' liststr:' .. liststr)
    ChatManager.ReceiveChatContent(channel_id, paramstr, contentstr, liststr)

end

function PlayerAvatar:chat_action_resp(act_id, code, args)
end

function PlayerAvatar:set_chat_often_use_info(change)
    ChatManager:OnFastChatEdit()
end

----------------------------------------好友相关----------------------------------------------------
function PlayerAvatar:friend_add_apply(table_friend)
--FriendManager:OnFriendApplyResp(table_friend)
end

function PlayerAvatar:friend_add_resp(table_friend)
--FriendManager:OnFriendAddResp(table_friend)
end

function PlayerAvatar:friend_del_resp(uuid)
--FriendManager:OnFriendDeleteResp(uuid)
end

function PlayerAvatar:friend_list_resp(listType, table_friendList)
--FriendManager:OnSocietyListResp(listType, table_friendList)
end

function PlayerAvatar:friend_new_chat(uuid, newsNum)
    FriendManager:OnChatNewsResp(uuid, newsNum)
end

function PlayerAvatar:friend_list_chat_resp(uuid, table_chat)
--FriendManager:OnChatListResp(uuid, table_chat)
end

function PlayerAvatar:friend_list_count_resp(countType, num, uuidTable)
--FriendManager:OnListCountResp(countType, num, uuidTable)
end

function PlayerAvatar:friend_list_1_resp(table_friend)
--FriendManager:OnFriendList1Resp(table_friend)
end

function PlayerAvatar:friend_list_crossdbid_resp(data)
--FriendManager:OnCrossdDBIDResp(data)
end

---------------------------------------PVP相关---------------------------------------------------
function PlayerAvatar:on_pvp_match_action_resp(act_id, code, args)
    --LoggerHelper.Error("on_pvp_match_action_resp")
    PVPManager:HandleData(act_id, code, args)
end

--------------------------------------切磋相关--------------------------------------------------
function PlayerAvatar:on_fight_friend_action_resp(act_id, code, args)
    LoggerHelper.Error("on_fight_friend_action_resp" .. act_id)
end

--------------------------------------商城相关------------------------------------------------------
--商城周限购刷新
function PlayerAvatar:set_shop_week_limit()
    -- LoggerHelper.Log(self.shop_week_limit)
    ShopManager:OnShopWeekLimitChange()
end

function PlayerAvatar:on_shop_resp(act_id, err_code, args)
    ShopManager:HandleData(act_id, err_code, args)
end

--商城周限购刷新
function PlayerAvatar:set_shop_day_limit()
    ShopManager:OnShopDayLimitChange()
end



--=====福利系统=====
function PlayerAvatar:set_welfare_level_up_reward(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.WELFARE_LEVEL_UP_REWARD, self.welfare_level_up_reward, oldValue)
end

function PlayerAvatar:set_welfare_online_reward(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.WELFARE_ONLINE_REWARD, self.welfare_online_reward, oldValue)
end

function PlayerAvatar:set_welfare_free_tili_time(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.WELFARE_FREE_TILI_TIME, self.welfare_free_tili_time, oldValue)
end

function PlayerAvatar:set_welfare_weekday_reward_time(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.WELFARE_WEEKDAY_REWARD_TIME, self.welfare_weekday_reward_time, oldValue)
end

function PlayerAvatar:set_welfare_7day_reward_time(oldValue)
    EventDispatcher:TriggerEvent(GameEvents.WELFARE_7DAY_REWARD_TIME, self.welfare_7day_reward_time, oldValue)
end

--------------------------------------排行榜相关---------------------------------------------------
---
function PlayerAvatar:on_rank_resp(act_id, err_code, args)
    RankManager:HandleData(act_id, err_code, args)
end

function PlayerAvatar:on_rank_list_action_resp(act_id, err_code, args)
    RankExManager:HandleData(act_id, err_code, args)
    ServiceActivityManager:HandleResp(act_id, err_code, args)
end

--------------------------------------交易行相关---------------------------------------------------
function PlayerAvatar:on_auction_action_resp(act_id, err_code, args)
    TradeManager:HandleData(act_id, err_code, args)
end

function PlayerAvatar:set_auction_logs(change)

end

function PlayerAvatar:on_beg_action_resp(act_id, err_code, args)
    TradeManager:HandleData(act_id, err_code, args)
end
--------------------------------------VIP相关---------------------------------------------------
function PlayerAvatar:on_invest_vip_action_resp(act_id, err_code, args)
    InvestManager:HandleData(act_id, err_code, args)
end

function PlayerAvatar:on_lv_invest_action_resp(act_id, err_code, args)
    InvestManager:HandleData(act_id, err_code, args)
end
function PlayerAvatar:on_investment_action_resp(act_id, err_code, args)
    InvestManager:HandleData(act_id, err_code, args)
end

function PlayerAvatar:on_cumulative_charge_action_resp(act_id, err_code, args)
    InvestManager:HandleData(act_id, err_code, args)
end
--------------------------------------天堂之巅相关---------------------------------------------------
function PlayerAvatar:on_cloud_peak_resp(act_id, err_code, args)
    CloudPeakInstanceManager:HandleData(act_id, err_code, args)
end

--------------------------------------日常活动与活跃奖励---------------------------------------------------
function PlayerAvatar:on_daily_task_action_resp(event_id, error_id, args)
    DailyActivityManager:HandleData(event_id, error_id, args)
end

--外形点数更新
function PlayerAvatar:set_daily_point(change)
    if change ~= nil then
        DailyActivityExManager:CheckAppearLevelUp()
    end
end

--每日必做次数更新
function PlayerAvatar:set_daily_activity_cur_counts(change)
    --LoggerHelper.Error("每日必做次数更新")
    if change ~= nil then
        -- LoggerHelper.Error("PlayerAvatar:set_daily_activity_cur_counts(change) ===" .. PrintTable:TableToStr(self.daily_activity_cur_counts))
        DailyActivityExManager:CheckMustRedPoint()
        EventDispatcher:TriggerEvent(GameEvents.DAILY_CUR_COUNT_UPDATE)
    end
end

--每日必做奖励变化
function PlayerAvatar:set_daily_activity_point_reward(change)
    --LoggerHelper.Error("每日必做奖励变化")
    if change ~= nil then
        -- LoggerHelper.Error("PlayerAvatar:set_daily_activity_point_reward(change) ===" .. PrintTable:TableToStr(self.daily_activity_point_reward))
        DailyActivityExManager:CheckMustRedPoint()
        EventDispatcher:TriggerEvent(GameEvents.DAILY_POINT_REWARD_UPDATE)
    end
end

--每日活跃度变化
function PlayerAvatar:set_daily_activity_point(change)
    --LoggerHelper.Error("活跃度变化")
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.DAILY_POINT_COUNT_UPDATE)
    end
end

--------------------------------------周常任务---------------------------------------------------
--周常任务次数更新
function PlayerAvatar:set_week_task_cur_week_count(change)
    --LoggerHelper.Error("周常任务次数更新")
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.WEEK_TASK_CUR_COUNT_UPDATE)
    end
end

--------------------------------------日常任务---------------------------------------------------
--日常任务次数更新
function PlayerAvatar:set_circle_task_cur_day_count(change)
    --LoggerHelper.Error("日常任务次数更新")
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.CIRCLE_TASK_CUR_COUNT_UPDATE)
    end
end


--------------------------------------宝箱探索---------------------------------------------------
function PlayerAvatar:wild_treasure_resp(action_id, error_id, args)
    ClientEntityManager:HandleWildTreasureData(action_id, error_id, args)
end

--------------------------------------公会系统---------------------------------------------------
function PlayerAvatar:on_guild_resp(action_id, error_id, args)
    GuildManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_guild_name(changeValue)
    EventDispatcher:TriggerEvent(GameEvents.REFRESH_CREATURE_BILLBOARD, GameWorld.Player().id)
end

function PlayerAvatar:set_guild_info(changeValue)
    GuildManager:RefreshGuildInfo(changeValue)
end

--------------------------------------公会篝火---------------------------------------------------
function PlayerAvatar:on_guild_bonfire_action_resp(action_id, error_id, args)
    GuildBanquetManager:HandleData(action_id, error_id, args)
end

--------------------------------------公会争霸---------------------------------------------------
function PlayerAvatar:on_guild_conquest_action_resp(action_id, error_id, args)
    GuildConquestManager:HandleData(action_id, error_id, args)
end

--------------------------------------伤害统计---------------------------------------------------
function PlayerAvatar:damage_record_rank_resp(luaTable)
    ExpInstanceManager:HandleDamageRank(luaTable)
end
--------------------------------------永恒之塔---------------------------------------------------
function PlayerAvatar:on_aion_action_resp(action_id, error_id, args)
    AionTowerManager:HandleData(action_id, error_id, args)
end
--------------------------------------世界boss---------------------------------------------------
function PlayerAvatar:world_boss_resp(event_id, error_id, args)
    WorldBossManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_tire_flag(changeValue)
    if changeValue ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.WORLD_BOSS_TIRE_UPDATE, WorldBossManager:BossTireIsFull())
    end
    WorldBossManager:RefreshTireFlag(changeValue)
end

--------------------------------------boss之家---------------------------------------------------
function PlayerAvatar:boss_home_resp(event_id, error_id, args)
    BossHomeManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_boss_home_interest_info(changeValue)
    BossHomeManager:RefreshBossHomeInterestInfo(changeValue)
end

--------------------------------------个人boss---------------------------------------------------
function PlayerAvatar:personal_boss_resp(event_id, error_id, args)
    PersonalBossManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_personal_boss_info(changeValue)
    PersonalBossManager:RefreshPersonalBossInfo(changeValue)
end

--------------------------------------蛮荒境地---------------------------------------------------
function PlayerAvatar:on_forbidden_resp(event_id, error_id, args)
    BackWoodsManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_forbidden_info(changeValue)
    BackWoodsManager:RefreshForbiddenInfo(changeValue)
end

--------------------------------------攻略讨论系统---------------------------------------------------
function PlayerAvatar:on_discussion_action_resp(event_id, error_id, args)
    BBSManager:HandleData(event_id, error_id, args)
end

--------------------------------------活动系统---------------------------------------------------
function PlayerAvatar:on_activity_info_resp(event_id, error_id, args)
    ActivityInfoManager:HandleData(event_id, error_id, args)
end

--------------------------------------天降秘宝---------------------------------------------------
function PlayerAvatar:set_sky_drop_info(change)
    SkyDropManager:ShowSkyDropTip(change)
end

--------------------------------------竞技系统---------------------------------------------------
-- function PlayerAvatar:on_athletics_action_resp(event_id, error_id, args)
--     AthleticsManager:HandleData(event_id, error_id, args)
-- end
function PlayerAvatar:on_offline_pvp_action_resp(action_id, error_code, args)
    AthleticsManager:HandleData(action_id, error_code, args)
end

--------------------------------------------客户端本部分数据----------------------------------------------
function PlayerAvatar:on_get_client_drops_resp(send_client_tbl)
    clientSceneData:RefreshDropsData(send_client_tbl)
end


--------------------------------------获取上次释放技能id和时间---------------------------------------------------
function PlayerAvatar:GetLastSkillIdAndTimestamp()
    return self.lastSpell, self.lastSpellTimeStamp
end


--------------------------------------触发自动挂机---------------------------------------------------
function PlayerAvatar:TriggerRobot(isOpen)
    if self.ai_id > 0 then
        self:SetBehaviorTree(self.ai_id)
        self:StopMove()
        --print("PlayerAvatar:TriggerRobot ",isOpen)
        self:SetThinkState(isOpen)
        self:_set_thinkable(isOpen)
    end
end

function PlayerAvatar:OnTriggerPathPoint(pp_id, is_enter)
    
    --print("PlayerAvatar:OnTriggerPathPoint ",pp_id,is_enter)
    if is_enter == true then
        
        local reach_pp_idx = mgr_entity_ai_cell:get_reach_idx(self:GetEntityAI())
        local pp_seq = GameSceneManager:GetPathPointSeq()
        local tar_pp_id = pp_seq[reach_pp_idx + 1]
        local tar_action_data = GameSceneManager:GetActionByID(tar_pp_id)
        local always_exist = tar_action_data and (tar_action_data.always_exist == 1) or false
        if tar_pp_id and tar_pp_id == pp_id and (not always_exist) then
            mgr_entity_ai_cell:set_reach_idx(self:GetEntityAI(), (reach_pp_idx + 1))
        end
    
    --发事件通知寻路指示UI
    --EventDispatcher:TriggerEvent(GameEvents.PlayerEnterPathPoint)
    end
end

function PlayerAvatar:SetTemporaryAutoFight(state)
    self.temporaryAutoFight = state
end

function PlayerAvatar:StartAutoFight()
    if self.autoFight then return end
    if GameWorld.canStartAutoFight then
        -- LoggerHelper.LogEx("Sam : PlayerAvatar:StartAutoFight()", true)
        self.autoFight = true
        self:TriggerAutoChangeEvent()
    end
end

function PlayerAvatar:StopAutoFight(temporaryAuto, source)
    -- LoggerHelper.Error("StopAutoFight", true)
    if not self.autoFight then return end
    -- LoggerHelper.LogEx("Sam : PlayerAvatar:StopAutoFight()" .. tostring(source))
    local state = temporaryAuto == true
    self:SetTemporaryAutoFight(state)
    self.autoFight = false
    self:TriggerAutoChangeEvent()
end

function PlayerAvatar:ChangeAutoFight()
    if self.temporaryAutoFight then
        self.autoFight = false
        self.temporaryAutoFight = false
    else
        self.autoFight = not self.autoFight
    end
    --self.autoFight = not self.autoFight
    self:TriggerAutoChangeEvent()
end

function PlayerAvatar:TriggerAutoChangeEvent()
    self:TriggerRobot(self.autoFight)
    EventDispatcher:TriggerEvent(GameEvents.CHANGE_AUTO_FIGHT)
end

--------------------------------------PK模式---------------------------------------------------
function PlayerAvatar:on_pk_mode_resp(event_id, error_id, args)
    PKModeManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_pk_mode(changeValue)
    PKModeManager:RefreshPKMode(changeValue)
end

--------------------------------------伙伴模块---------------------------------------------------
function PlayerAvatar:set_pet_info(changeValue)
    PartnerManager:RefreshPetInfo(changeValue)
end

function PlayerAvatar:on_pet_action_resp(event_id, error_id, args)
    PartnerManager:HandlePetData(event_id, error_id, args)
end

function PlayerAvatar:set_horse_info(changeValue)
    PartnerManager:RefreshRideInfo(changeValue)
end

function PlayerAvatar:on_horse_action_resp(event_id, error_id, args)
    PartnerManager:HandleRideData(event_id, error_id, args)
end

function PlayerAvatar:set_horse_grade_show(changeValue)
    PartnerManager:RefreshHorseGradeShow(changeValue)
end


--------------------------------------资源找回---------------------------------------------------
-- function PlayerAvatar:on_reward_back_resp(event_id, error_id, args)
--DailyActivityExManager:HandleRewardBackData(event_id, error_id, args)
-- end
--------------------------------------宝物---------------------------------------------------
function PlayerAvatar:set_wing_info(change)
    TreasureManager:OnTreasureUpdate()
end

function PlayerAvatar:set_talisman_info(change)
    TreasureManager:OnTreasureUpdate()
end

function PlayerAvatar:set_weapon_info(change)
    TreasureManager:OnTreasureUpdate()
end

function PlayerAvatar:set_cloak_info(change)
    TreasureManager:OnTreasureUpdate()
end

function PlayerAvatar:set_wing_show(change)
    TreasureManager:OnTreasureModelShow(public_config.TREASURE_TYPE_WING)
end

function PlayerAvatar:set_talisman_show(change)
    TreasureManager:OnTreasureModelShow(public_config.TREASURE_TYPE_TALISMAN)
end

function PlayerAvatar:set_weapon_show(change)
    TreasureManager:OnTreasureModelShow(public_config.TREASURE_TYPE_WEAPON)
end

function PlayerAvatar:set_cloak_show(change)
    TreasureManager:OnTreasureModelShow(public_config.TREASURE_TYPE_CLOAK)
end

-----------------------------------幻化-------------------------------------------------
--暂时不用
function PlayerAvatar:set_illusion_show_system(change)
--TransformManager:UpdateTransformShow(change)
end

--幻形
function PlayerAvatar:set_illusion_show_info(change)
    TransformManager:OnModelShow(change)
end
--------------------------------------时装模块---------------------------------------------------
function PlayerAvatar:set_fashion_info(changeValue)
    FashionManager:RefreshFashionInfo(changeValue)
end

--服务器同步属性，时装背包 LuaTable
function PlayerAvatar:set_pkg_fashion(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_FASHION, self.pkg_fashion, change)
    FashionManager:RefreshPkgFashion(change)
end

--------------------------------------称号模块---------------------------------------------------
function PlayerAvatar:on_title_action_resp(action_id, error_id, args)
    TitleManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_title_data(changeValue)
    TitleManager:RefreshTitleData(changeValue)
end

function PlayerAvatar:set_title_id()
    TitleManager:RefreshTitleId()
end

--------------------------------------爵位模块---------------------------------------------------
function PlayerAvatar:set_nobility_rank()
    RoleManager:OnNobilityUpgrade()
end

--------------------------------------装备模块---------------------------------------------------
--强化
function PlayerAvatar:set_equip_stren_info(change)
    EquipManager:OnUpdateStrength(change)
end

--洗炼
function PlayerAvatar:set_equip_wash_info(change)
    EquipManager:OnUpdateWash(change)
end

-- function PlayerAvatar:set_wash_cur_day_count()
--     EquipManager:OnUpdateWashFreeTime()
-- end
--宝石
function PlayerAvatar:set_equip_gem_info(change)
    EquipManager:OnUpdateGem(change)
end

--套装
function PlayerAvatar:set_equip_suit_data(change)
    EquipManager:OnUpdateSuit(change)
end

--------------------------------------开心答题模块---------------------------------------------------
function PlayerAvatar:on_question_action_resp(action_id, error_id, args)
    AnswerManager:HandleData(action_id, error_id, args)
end

--------------------------------------系统设置模块---------------------------------------------------
function PlayerAvatar:on_setting_action_resp(action_id, error_code, args)
    SystemSettingManager:HandleData(action_id, error_code, args)
end

--------------------------------------神兽模块---------------------------------------------------
function PlayerAvatar:on_god_monster_action_resp(action_id, error_id, args)
    AnimalManager:HandleData(action_id, error_id, args)
end

--服务器同步属性，物品背包 LuaTable
function PlayerAvatar:set_pkg_god_monster(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_GOD_MOSNTER, self.pkg_god_monster, change)
end

--------------------------------------经验副本---------------------------------------------------
function PlayerAvatar:set_exp_instance_info(change)
    if change then
        InstanceManager:OnUpdateInfo()
    end
end

--------------------------------------混沌之门---------------------------------------------------
function PlayerAvatar:set_equipment_instance_info(change)
    if change then
        EquipmentInstanceManager:OnUpdateInfo()
    end
end

--------------------------------------龙魂要塞模块---------------------------------------------------
function PlayerAvatar:on_tower_defense_action_resp(action_id, error_id, args)
    TowerInstanceManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_tower_defense_info(change)
    if change then
        TowerInstanceManager:OnUpdateInfo()
    end
end

--------------------------------------众神宝库模块---------------------------------------------------
function PlayerAvatar:set_instance_gold_info(change)
    if change then
        MoneyInstanceManager:OnUpdateInfo()
    end
end

--------------------------------------守护结界塔模块---------------------------------------------------
function PlayerAvatar:set_holy_tower_info(change)
    if change then
        PetInstanceManager:OnUpdateInfo()
    end
end

--重连进入副本
function PlayerAvatar:re_on_enter_space_resp(data)
    -- PetInstanceManager:SetStartTime(data[1])
    InstanceManager:OnReEnterInstance(data)
end

--------------------------------------挂机设置模块---------------------------------------------------
function PlayerAvatar:set_hang_up_setting(change)
    self:GetEntityAI():set_hang_up_setting(self.hang_up_setting[public_config.HANG_UP_KEY_AUTO_PICK])
end

--------------------------------------VIP模块---------------------------------------------------
function PlayerAvatar:set_vip_level_reward(change)
    VIPManager:OnLevelRewardUpdate(change)
end

-- function PlayerAvatar:set_vip_week_reward(change)
--     VIPManager:OnWeekRewardUpdate(change)
-- end
function PlayerAvatar:set_vip_daily_reward(change)
    VIPManager:OnDailyRewardUpdate(change)
end

function PlayerAvatar:set_charge_listing_first_flag(change)
    if change then
        EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Charge_List)
    end
end

function PlayerAvatar:set_vip_level(change)
    if change then
        EventDispatcher:TriggerEvent(GameEvents.On_VIP_Level_Change)
    end
end
--------------------------------------离线挂机模块---------------------------------------------------
function PlayerAvatar:hang_up_resp(data)
    OffLineManager:HandleData(data)
end

--------------------------------------开服首充模块---------------------------------------------------
function PlayerAvatar:set_first_charge_reward_got(change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_First_Charge_Reward_State)
    end
end

function PlayerAvatar:set_first_charge_reward_time(change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.On_FunctionO_Condition_Change)
    end
end

function PlayerAvatar:first_charge_action_resp(action_id, error_id, args)

end



function PlayerAvatar:on_online_reward_action_resp(action_id, error_id, args)
    WelfareManager:HandleData(action_id, error_id, args)
end
--------------------------------------每日首充模块---------------------------------------------------
function PlayerAvatar:on_daily_charge_action_resp(data)
    DailyChargeManager:HandleData(data)
end

function PlayerAvatar:set_daily_charge_reward_record(change)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Charge_Reward_Record)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Charge_Reward_State)
end

function PlayerAvatar:set_daily_charge_cumulative_record(change)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Charge_Cumulative_Record)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Charge_Reward_State)
end

function PlayerAvatar:set_daily_charge_amount(change)

end

--------------------------------------连续消费模块---------------------------------------------------
function PlayerAvatar:set_channel_activity_times(change)
    if change ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Channel_Activity_Times)
    end
end

function PlayerAvatar:set_daily_consume_record(change)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Consume_Reward_Record)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Consume_Reward_State)
end

function PlayerAvatar:set_cumulative_consume_record(change)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Consume_Cumulative_Record)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Consume_Reward_State)
end

function PlayerAvatar:set_daily_cunsume_world_level(change)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Daily_Consume_Reward_Record)
end

--------------------------------------实时1v1模块---------------------------------------------------
function PlayerAvatar:on_cross_vs11_resp(event_id, error_id, args)
    OnlinePvpManager:HandleData(event_id, error_id, args)
end

--------------------------------------转职模块---------------------------------------------------
--4转命格阶段改变
function PlayerAvatar:set_change_four_stage(change)
-- EventDispatcher:TriggerEvent(GameEvents.VOCATION_FOUR_STAGE_CHANGE)
end

function PlayerAvatar:on_voc_change_action_resp(act_id, code, args)
    VocationChangeManager:HandleData(act_id, code, args)
end

--------------------------------------神兽岛模块---------------------------------------------------
function PlayerAvatar:god_island_resp(event_id, error_id, args)
    GodIslandManager:HandleData(event_id, error_id, args)
end

function PlayerAvatar:set_god_island_tire_flag(changeValue)
    if changeValue ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.GOD_ISLAND_TIRE_UPDATE, GodIslandManager:BossTireIsFull())
    end
end

--------------------------------------掉落记录模块---------------------------------------------------
function PlayerAvatar:on_drop_record_action_resp(event_id, error_id, args)
    DropRecordManager:HandleData(event_id, error_id, args)
end

--------------------------------------结婚模块---------------------------------------------------
function PlayerAvatar:on_marry_action_resp(event_id, error_id, args)
    MarriageManager:HandleData(event_id, error_id, args)
end

--------------------------------------云购模块---------------------------------------------------
function PlayerAvatar:on_roulette_action_resp(event_id, error_id, args)
    RouletteManager:HandleResp(event_id, error_id, args)
end

--------------------------------------装备寻宝模块---------------------------------------------------
function PlayerAvatar:lottery_equip_action_resp(action_id, error_id, args)
    LotteryEquipManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_lottery_equip_last_free_time()
    EventDispatcher:TriggerEvent(GameEvents.ON_LOTTERY_EQUIP_LAST_FREE_TIME_CHANGE)
end

--------------------------------------周卡模块---------------------------------------------------
function PlayerAvatar:on_week_card_action_resp(action_id, error_id, args)
    WeekCardManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_week_card_flag()
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Charge_List)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Vip_Charge_State)
end

function PlayerAvatar:set_week_card_daily_refund_flag()
    EventDispatcher:TriggerEvent(GameEvents.On_Refresh_Charge_List)
    EventDispatcher:TriggerEvent(GameEvents.Refresh_Vip_Charge_State)
end

--------------------------------------点击玩家名字超链接---------------------------------------------------
function PlayerAvatar:inquire_other_info_resp(args)
    CheckOtherPlayerManager:ResponseGetPlayerDBIdByName(args)
end

--------------------------------------护送女神模块---------------------------------------------------
function PlayerAvatar:set_guard_goddess_count(changeValue)
    if changeValue ~= nil and self.guard_goddess_count >= GlobalParamsHelper.GetParamValue(524) then
        EventDispatcher:TriggerEvent(GameEvents.REMOVE_MAIN_MENU_ICON, 303)
    end
end

--------------------------------------限时抢购模块---------------------------------------------------
function PlayerAvatar:on_level_up_sale_action_resp(action_id, error_id, args)
    FlashSaleManager:HandleData(action_id, error_id, args)
end

function PlayerAvatar:set_level_up_sale_info(changeValue)
    if changeValue ~= nil then
        EventDispatcher:TriggerEvent(GameEvents.Refresh_Flash_Sale_Data)
    end
end

--------------------------------------0元购模块---------------------------------------------------
function PlayerAvatar:set_zero_price_info(changeValue)
    ZeroDollarBuyManager:RefreshServerData(changeValue)
end

--------------------------------------上古战场模块---------------------------------------------------
function PlayerAvatar:ancient_battle_action_resp(action_id, error_id, args)
    AncientBattleManager:HandleData(action_id, error_id, args)
end

--------------------------------------运营活动模块---------------------------------------------------
function PlayerAvatar:on_weekly_cumulative_charge_action_resp(action_id, error_id, args)
    OperatingActivityManager:HandleResp(action_id, error_id, args)
end

function PlayerAvatar:set_week_login_info(change)
    OperatingActivityManager:UpdateLoginRewardState(change)
end

function PlayerAvatar:set_weekly_cumulative_charge_refund_info(change)
    OperatingActivityManager:UpdateChargeRewardState(change)
end

function PlayerAvatar:set_weekly_cumulative_charge_amount(change)
    OperatingActivityManager:UpdateChargeRewardState(change)
end

function PlayerAvatar:on_battle_soul_action_resp(action_id, error_id, args)
    OperatingActivityManager:HandleResp(action_id, error_id, args)
end

function PlayerAvatar:set_exchange_info(change)
    OperatingActivityManager:UpdateCelebrationGetState(change)
end

function PlayerAvatar:set_hi_point(change)
    OperatingActivityManager:UpdateHiPointAward(change)
end

function PlayerAvatar:set_hi_point_reward(change)
    OperatingActivityManager:UpdateHiPointAward(change)
end

function PlayerAvatar:set_hi_point_times(change)
    OperatingActivityManager:UpdateHiPointActivityTimes(change)
end


function PlayerAvatar:on_boss_reward_action_resp(action_id, error_id, args)
    BossOfferManager:HandleResp(action_id, error_id, args)
end


--------------------------------------传送协议返回---------------------------------------------------
function PlayerAvatar:teleport_point_resp(err_id, point_id)
    EventDispatcher:TriggerEvent(GameEvents.TeleportPositionResp, err_id, point_id)
    if err_id > 0 then
        SystemInfoManager:ShowRespTip(0, err_id)
        GameWorld.PauseAllSynPos(false)
    end
end

--服务器同步属性，物品背包 LuaTable
function PlayerAvatar:set_pkg_lottery_equip(change)
    BagManager:OnChanged(PkgConfig.BAG_TYPE_PKG_LOTTERY_EQUIP, self.pkg_lottery_equip, change)
end


-- ======================================================================================================
function PlayerAvatar:PlayerEnterWorldLog()
    if GameWorld.playerEnterWorldLog then return end
    GameWorld.playerEnterWorldLog = true
    
    --客户端到达游戏界面日志
    -- if GameLoader.SystemConfig.IsUsePlatformSdk then
    TimerHeap:AddSecTimer(1, 0, 0, function()
        local player = GameWorld.Player()
        -- LoggerHelper.Log("----------onLoginRoleInfo,roleId:" .. LocalSetting.settingData.UserAccount
        --     .. ",roleName:" .. player.name
        --     .. ",serverId:" .. LocalSetting.settingData.SelectedServerID
        --     .. ",serverName:" .. ServerListManager:SelectedServerName())
        GameWorld.ShowSDKLog("onLoginRoleInfo", tostring(player.dbid), player.name, tostring(player.level), tostring(player.vip_level),
            LocalSetting.settingData.SelectedServerID, ServerListManager:SelectedServerName(), tostring(player.money_coupons),
            player.guild_name, tostring(player.money_coupons), "-1")
    end)
-- end
end

function PlayerAvatar:on_new_charge_resp()
    GameWorld.Player().server.get_charge()
end

-- 开服狂嗨
function PlayerAvatar:on_freak_action_resp(action_id, error_code, args)
    PlayerManager.OpenHappyManager:HandleResp(action_id, error_code, args)
end
