--UI展示效果类型  开宝箱之类
require "Entities/Creature"

local UIShowEntity = Class.UIShowEntity(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local EntityType = GameConfig.EnumType.EntityType
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig

function UIShowEntity:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.UIShowEntity
    self.scale = 1
    self.sort = -1
    self.face = Vector3(0,0,0)
    self.skillId = 0
end

function UIShowEntity:OnEnterWorld()
    self._base.OnEnterWorld(self)
end

function UIShowEntity:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function UIShowEntity:OnActorLoaded()
    self.cs:SetUpdateFaceEnabled(false)
    self.cs:InitActorScale(self.scale)
    self:SetActorSortingOrder(self.sort)
    self:SetFaceVec(self.face)
    self:PlaySkillByUIShowEntity(self.skillId)
end

function UIShowEntity:GetDefaultLayer()   
    return GameObjectLayerConfig.UI
end

function UIShowEntity:TeleportClient(pos)   
    self.cs:TeleportClient(pos)
end

function UIShowEntity:SetFaceVec(face)
    local f = Vector3(face.x * 0.5, face.y * 0.5, face.z * 0.5)
    self.cs.face = f
end

function UIShowEntity:SetActorSortingOrder(sort)
    self.cs:SetActorSortingOrder(sort)
end

function UIShowEntity:PlaySkillByUIShowEntity(skillId)
    self.skillId = skillId
    if self:GetTransform() ~= nil and self.skillId > 0 then
        self.cs:PlaySkillByCreateRoleAvatar(self.skillId)
    end
end