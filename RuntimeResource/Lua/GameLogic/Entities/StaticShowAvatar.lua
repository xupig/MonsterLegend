require "Entities/Avatar"
--静态展示玩家模型
local StaticShowAvatar = Class.StaticShowAvatar(ClassTypes.Creature)
local EntityType = GameConfig.EnumType.EntityType
local RoleDataHelper = GameDataHelper.RoleDataHelper

function StaticShowAvatar:__ctor__(csEntity)
	self.entityType = EntityType.StaticShowAvatar
    self.showBillboard = false
	self._facadeFuncs = {}
end

function StaticShowAvatar:OnEnterWorld()
    StaticShowAvatar._base.OnEnterWorld(self)
    self.actorId = RoleDataHelper.GetModelByVocation(self.vocation)
    if self.actorId ==nil then
        self.actorId=1005
    end
    self:SetActor(self.actorId)
    --EventDispatcher:TriggerEvent(GameEvents.OnShowAvatarEnterWorld, self.id)
end

function StaticShowAvatar:OnActorLoaded()
	self.actorHasLoaded = true
	--演示用的模型不需要重力
	self:SetGravity(0)
end