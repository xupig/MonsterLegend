--技能生成投射物类型
require "Entities/Creature"

local ThrowObject = Class.ThrowObject(ClassTypes.Creature)

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local SpawnType = GameConfig.EnumType.SpawnType
local SpawnEntityDataHelper = GameDataHelper.SpawnEntityDataHelper
local EntityType = GameConfig.EnumType.EntityType

function ThrowObject:__ctor__()
    self.showBillboard = false
    self.entityType = EntityType.ThrowObject
end

function ThrowObject:OnEnterWorld()
    ThrowObject._base.OnEnterWorld(self)
    --处理客户端本生成逻辑
    if self.cs.spawnObjectId ~= 0 then
        self.cfg_id = self.cs.spawnObjectId
    end
    self.model = SpawnEntityDataHelper.GetThrowObjectModelId(self.cfg_id)
    self:SetActor(self.model)
    self:SetCityState(false)
end

function ThrowObject:OnLeaveWorld()
    self._base.OnLeaveWorld(self)
end

function ThrowObject:OnActorLoaded()
end

function ThrowObject:GetDefaultLayer()   
    return GameObjectLayerConfig.ThrowObject
end