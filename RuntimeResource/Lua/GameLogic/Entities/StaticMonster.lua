require "Entities/Avatar"
local StaticMonster = Class.StaticMonster(ClassTypes.Creature)
local MonsterDataHelper = GameDataHelper.MonsterDataHelper

local EntityConfig = GameConfig.EntityConfig
local GameObjectLayerConfig = GameConfig.GameObjectLayerConfig
local EntityType = GameConfig.EnumType.EntityType

function StaticMonster:__ctor__(csEntity)
	self.entityType = EntityType.StaticMonster
	self.action_id = 0
	self.cs.isStatic = true
end

function StaticMonster:OnEnterWorld()
	self._base.OnEnterWorld(self)
	self:InitLocalData()
end

function StaticMonster:OnLeaveWorld()
	self._base.OnLeaveWorld(self)
end

function StaticMonster:InitLocalData()
	self:HasActorDeathController(true)
	self.cs.clientCamp = 1
	self.name = MonsterDataHelper.GetMonsterName(self.monster_id)
	self.scale = MonsterDataHelper.GetMonsterScale(self.monster_id)
    self.cs.monsterId = self.monster_id
    
    self.showBillboard = true

	self.model_low = MonsterDataHelper.GetMonsterModel(self.monster_id)
	-- self.ai_id = MonsterDataHelper.GetMonsterAI(self.monster_id)
	-- self.build_type = MonsterDataHelper.GetMonsterBuildType(self.monster_id)
	-- self.level =  MonsterDataHelper.GetMonster(self.monster_id).lv
	-- self.off_play_args = MonsterDataHelper.GetMonsterOffPlayArgs(self.monster_id)

	-- self:SetLearnedSkillList(MonsterDataHelper.GetMonsterSkills(self.monster_id))
	--local speed = MonsterDataHelper.GetMonsterSpeed(self.monster_id)
	self:SetActor(self.model_low)
	-- self.cs:SetCreatureType(2)--指定类型为怪物2
	-- self:SetCityState(false)
	-- local hp = MonsterDataHelper.GetMonsterHP(self.monster_id)
	-- GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_CUR_HP, hp)
	-- GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_MAX_HP, hp)
	-- GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_BASE_SPEED, speed)
	-- GameWorld.SynEntityAttrInt(self.id, EntityConfig.PROPERTY_SPEED, speed)
	-- local attributes = MonsterDataHelper.GetMonsterAttris(self.monster_id)
	-- self:SetBattleAttributes(attributes)
	-- self:SetBattleAttribute(attri_config.ATTRI_ID_LEVEL, self.level)
end

function StaticMonster:GetDefaultLayer()
	return GameObjectLayerConfig.Monster
end

function StaticMonster:OnActorLoaded()
	--初始模型缩放设置
    self.cs:InitActorScale(self.scale)
    if self.action_id > 0 then
    	self:PlayAction(self.action_id)
    end
	self:IdleToReady()
end