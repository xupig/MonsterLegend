-- LordRingDetailDataHelper.lua
local LordRingDetailDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local LordRingDetail = Class.LordRingDetail(ClassTypes.XObject)

local lordRingDetailCache = {}

function LordRingDetail:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.name = cfg.__name
    self.item = DataParseHelper.ParseNSuffixSort(cfg.__item)
    self.other = DataParseHelper.ParseNSuffixSort(cfg.__other)
end

function LordRingDetailDataHelper.GetLordRingDetail(id)
    ClearDataManager:UseCache(CacheNames.lord_ring_detail, LordRingDetailDataHelper.ClearCache)
    local data = lordRingDetailCache[id]
    if data == nil then
        local cfg = XMLManager.lord_ring_detail[id]
        if cfg ~= nil then
            data = LordRingDetail(cfg)
            lordRingDetailCache[data.id] = data
        else
            LoggerHelper.Error("LordRingDetail id not exist:" .. id)
        end
    end
    return data
end

function LordRingDetailDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.lord_ring_detail:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function LordRingDetailDataHelper:GetType(id)
    local data = self.GetLordRingDetail(id)
    return data and data.type or nil
end

function LordRingDetailDataHelper:GetTargets(id)
    local data = self.GetLordRingDetail(id)
    return data and data.targets or nil
end

function LordRingDetailDataHelper:GetReward(id)
    local data = self.GetLordRingDetail(id)
    return data and data.reward or nil
end

function LordRingDetailDataHelper:GetName(id)
    local data = self.GetLordRingDetail(id)
    return data and data.name or nil
end

function LordRingDetailDataHelper:GetItem(id)
    local data = self.GetLordRingDetail(id)
    return data and data.item or nil
end

function LordRingDetailDataHelper:GetOther(id)
    local data = self.GetLordRingDetail(id)
    return data and data.other or nil
end

return LordRingDetailDataHelper
