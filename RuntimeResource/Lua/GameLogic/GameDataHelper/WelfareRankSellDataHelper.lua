-- WelfareRankSellDataHelper.lua
local WelfareRankSellDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareRankSell = Class.WelfareRankSell(ClassTypes.XObject)

local welfareRankSellCache = {}

function WelfareRankSell:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.item = DataParseHelper.ParseMSuffixSort(cfg.__item)
    self.num = cfg.__num
    self.orig = DataParseHelper.ParseMSuffixSort(cfg.__orig)
    self.price = DataParseHelper.ParseMSuffixSort(cfg.__price)
end

function WelfareRankSellDataHelper.GetWelfareRankSell(id)
    ClearDataManager:UseCache(CacheNames.welfare_rank_sell, WelfareRankSellDataHelper.ClearCache)
    local data = welfareRankSellCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_rank_sell[id]
        if cfg ~= nil then
            data = WelfareRankSell(cfg)
            welfareRankSellCache[data.id] = data
        else
            LoggerHelper.Error("WelfareRankSell id not exist:" .. id)
        end
    end
    return data
end

function WelfareRankSellDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_rank_sell:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareRankSellDataHelper:GetType(id)
    local data = self.GetWelfareRankSell(id)
    return data and data.type or nil
end

function WelfareRankSellDataHelper:GetItem(id)
    local data = self.GetWelfareRankSell(id)
    return data and data.item or nil
end

function WelfareRankSellDataHelper:GetNum(id)
    local data = self.GetWelfareRankSell(id)
    return data and data.num or nil
end

function WelfareRankSellDataHelper:GetOrig(id)
    local data = self.GetWelfareRankSell(id)
    return data and data.orig or nil
end

function WelfareRankSellDataHelper:GetPrice(id)
    local data = self.GetWelfareRankSell(id)
    return data and data.price or nil
end

return WelfareRankSellDataHelper
