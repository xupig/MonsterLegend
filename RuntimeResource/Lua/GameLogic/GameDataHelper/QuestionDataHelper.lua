-- QuestionDataHelper.lua
local QuestionDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Question = Class.Question(ClassTypes.XObject)

local questionCache = {}
function QuestionDataHelper.ClearCache()
    questionCache = {}
end

function Question:__ctor__(cfg)
    self.id = cfg.__id
    self.question_desc = cfg.__question_desc
    self.answer1_desc = cfg.__answer1_desc
    self.answer2_desc = cfg.__answer2_desc
end

function QuestionDataHelper.GetQuestion(id)
    ClearDataManager:UseCache(CacheNames.question, QuestionDataHelper.ClearCache)
    local data = questionCache[id]
    if data == nil then
        local cfg = XMLManager.question[id]
        if cfg ~= nil then
            data = Question(cfg)
            questionCache[data.id] = data
        else
            LoggerHelper.Error("Question id not exist:" .. id)
        end
    end
    return data
end

function QuestionDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.question:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function QuestionDataHelper:GetQuestionDesc(id)
    local data = self.GetQuestion(id)
    return data and data.question_desc or nil
end

function QuestionDataHelper:GetAnswer1Desc(id)
    local data = self.GetQuestion(id)
    return data and data.answer1_desc or nil
end

function QuestionDataHelper:GetAnswer2Desc(id)
    local data = self.GetQuestion(id)
    return data and data.answer2_desc or nil
end

return QuestionDataHelper
