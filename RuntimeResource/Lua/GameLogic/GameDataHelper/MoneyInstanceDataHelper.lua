-- MoneyInstanceDataHelper.lua
local MoneyInstanceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GodsTreasury = Class.GodsTreasury(ClassTypes.XObject)

local godsTreasuryCache = {}
function MoneyInstanceDataHelper.ClearCache()
    godsTreasuryCache = {}
end

function GodsTreasury:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.time = cfg.__time
    self.star_time = DataParseHelper.ParseNSuffix(cfg.__star_time)
    self.preview_reward = DataParseHelper.ParseListInt(cfg.__preview_reward)
end

function MoneyInstanceDataHelper.GetGodsTreasury(id)
    ClearDataManager:UseCache(CacheNames.gods_treasury, MoneyInstanceDataHelper.ClearCache)
    local data = godsTreasuryCache[id]
    if data == nil then
        local cfg = XMLManager.gods_treasury[id]
        if cfg ~= nil then
            data = GodsTreasury(cfg)
            godsTreasuryCache[data.id] = data
        else
            LoggerHelper.Error("GodsTreasury id not exist:" .. id)
        end
    end
    return data
end

function MoneyInstanceDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.gods_treasury:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MoneyInstanceDataHelper:GetLevel(id)
    local data = self.GetGodsTreasury(id)
    return data and data.level or nil
end

function MoneyInstanceDataHelper:GetName(id)
    local data = self.GetGodsTreasury(id)
    return data and data.name or nil
end

function MoneyInstanceDataHelper:GetIcon(id)
    local data = self.GetGodsTreasury(id)
    return data and data.icon or nil
end

function MoneyInstanceDataHelper:GetDesc(id)
    local data = self.GetGodsTreasury(id)
    return data and data.desc or nil
end

function MoneyInstanceDataHelper:GetTime(id)
    local data = self.GetGodsTreasury(id)
    return data and data.time or nil
end

function MoneyInstanceDataHelper:GetStartTime(id)
    local data = self.GetGodsTreasury(id)
    local returnData = {}
    for k,v in pairs(data.star_time) do
        returnData[k] = v
    end
    return returnData
end

function MoneyInstanceDataHelper:GetPreviewReward(id)
    local data = self.GetGodsTreasury(id)
    return data and data.preview_reward or nil
end

return MoneyInstanceDataHelper
