-- LordRingTotalDataHelper.lua
local LordRingTotalDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local LordRingTotal = Class.LordRingTotal(ClassTypes.XObject)

local lordRingTotalCache = {}
function LordRingTotalDataHelper.ClearCache()
    lordRingTotalCache = {}
end

function LordRingTotal:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.des = cfg.__des
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.type = cfg.__type
end

function LordRingTotalDataHelper.GetLordRingTotal(id)
    ClearDataManager:UseCache(CacheNames.lord_ring_total, LordRingTotalDataHelper.ClearCache)
    local data = lordRingTotalCache[id]
    if data == nil then
        local cfg = XMLManager.lord_ring_total[id]
        if cfg ~= nil then
            data = LordRingTotal(cfg)
            lordRingTotalCache[data.id] = data
        else
            LoggerHelper.Error("LordRingTotal id not exist:" .. id)
        end
    end
    return data
end

function LordRingTotalDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.lord_ring_total:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function LordRingTotalDataHelper:GetName(id)
    local data = self.GetLordRingTotal(id)
    return data and data.name or nil
end

function LordRingTotalDataHelper:GetIcon(id)
    local data = self.GetLordRingTotal(id)
    return data and data.icon or nil
end

function LordRingTotalDataHelper:GetDes(id)
    local data = self.GetLordRingTotal(id)
    return data and data.des or nil
end

function LordRingTotalDataHelper:GetReward(id)
    local data = self.GetLordRingTotal(id)
    return data and data.reward or nil
end

function LordRingTotalDataHelper:GetType(id)
    local data = self.GetLordRingTotal(id)
    return data and data.type or nil
end

return LordRingTotalDataHelper
