-- GameDataHelper.lua
-- 这里提供统一接口，为逻辑层提供大部分datahelper的获取
GameDataHelper = {}
GameDataHelper._mgrs = {}

local mt={		   
			  __index = function(t, k)
                    local mgr = t._mgrs[k]
                    if (mgr == nil) then
                        mgr = require("GameDataHelper."..k)
                        t._mgrs[k] = mgr
                    end 
					return mgr
			   end
			  }
setmetatable(GameDataHelper, mt)

