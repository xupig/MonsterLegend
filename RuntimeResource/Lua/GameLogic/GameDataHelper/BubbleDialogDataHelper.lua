-- BubbleDialogDataHelper.lua
local BubbleDialogDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local BubbleDialog = Class.BubbleDialog(ClassTypes.XObject)

local bubbleDialogCache = {}
function BubbleDialogDataHelper.ClearCache()
    bubbleDialogCache = {}
end

function BubbleDialog:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.target_id = cfg.__target_id
    self.dialog = DataParseHelper.ParseListInt(cfg.__dialog)
    self.voice = DataParseHelper.ParseListInt(cfg.__voice)
    self.delay = cfg.__delay
    self.duration = cfg.__duration
    self.trigger_and = DataParseHelper.ParseNSuffix(cfg.__trigger_and)
    self.trigger_or = DataParseHelper.ParseNSuffix(cfg.__trigger_or)
end

function BubbleDialogDataHelper.GetBubbleDialog(id)
    ClearDataManager:UseCache(CacheNames.bubble_dialog, BubbleDialogDataHelper.ClearCache)
    local data = bubbleDialogCache[id]
    if data == nil then
        local cfg = XMLManager.bubble_dialog[id]
        if cfg ~= nil then
            data = BubbleDialog(cfg)
            bubbleDialogCache[data.id] = data
        else
            LoggerHelper.Error("BubbleDialog id not exist:" .. id)
        end
    end
    return data
end

function BubbleDialogDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.bubble_dialog:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function BubbleDialogDataHelper:GetType(id)
    local data = self.GetBubbleDialog(id)
    return data and data.type or nil
end

function BubbleDialogDataHelper:GetTargetId(id)
    local data = self.GetBubbleDialog(id)
    return data and data.target_id or nil
end

function BubbleDialogDataHelper:GetDialog(id)
    local data = self.GetBubbleDialog(id)
    return data and data.dialog or nil
end

function BubbleDialogDataHelper:GetVoice(id)
    local data = self.GetBubbleDialog(id)
    return data and data.voice or nil
end

function BubbleDialogDataHelper:GetDelay(id)
    local data = self.GetBubbleDialog(id)
    return data and data.delay or nil
end

function BubbleDialogDataHelper:GetDuration(id)
    local data = self.GetBubbleDialog(id)
    return data and data.duration or nil
end

function BubbleDialogDataHelper:GetTriggerAnd(id)
    local data = self.GetBubbleDialog(id)
    return data and data.trigger_and or nil
end

function BubbleDialogDataHelper:GetTriggerOr(id)
    local data = self.GetBubbleDialog(id)
    return data and data.trigger_or or nil
end

return BubbleDialogDataHelper
