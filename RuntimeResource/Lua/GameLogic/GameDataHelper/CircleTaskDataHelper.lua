-- CircleTaskDataHelper.lua
local CircleTaskDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CircleTask = Class.CircleTask(ClassTypes.XObject)

local circleTaskCache = {}
function CircleTaskDataHelper.ClearCache()
    circleTaskCache = {}
end

function CircleTask:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.desc = cfg.__desc
    self.commit_npc = cfg.__commit_npc
    self.commit_dialog = DataParseHelper.ParseNSuffixSort(cfg.__commit_dialog)
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.reward_rate = cfg.__reward_rate
    self.targets_desc = DataParseHelper.ParseMSuffix(cfg.__targets_desc)
    self.little_shoes_open = cfg.__little_shoes_open
    self.little_shoes_open_start = cfg.__little_shoes_open_start
    self.little_shoes_open_end = cfg.__little_shoes_open_end
end

function CircleTaskDataHelper.GetCircleTask(id)
    ClearDataManager:UseCache(CacheNames.circle_task, CircleTaskDataHelper.ClearCache)
    local data = circleTaskCache[id]
    if data == nil then
        local cfg = XMLManager.circle_task[id]
        if cfg ~= nil then
            data = CircleTask(cfg)
            circleTaskCache[data.id] = data
        else
            LoggerHelper.Error("CircleTask id not exist:" .. id)
        end
    end
    return data
end

function CircleTaskDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.circle_task:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CircleTaskDataHelper:GetName(id)
    local data = self.GetCircleTask(id)
    return data and data.name or nil
end

function CircleTaskDataHelper:GetDesc(id)
    local data = self.GetCircleTask(id)
    return data and data.desc or nil
end

function CircleTaskDataHelper:GetCommitNpc(id)
    local data = self.GetCircleTask(id)
    return data and data.commit_npc or nil
end

function CircleTaskDataHelper:GetCommitDialog(id)
    local data = self.GetCircleTask(id)
    return data and data.commit_dialog or nil
end

function CircleTaskDataHelper:GetTargets(id)
    local data = self.GetCircleTask(id)
    return data and data.targets or nil
end

function CircleTaskDataHelper:GetRewardRate(id)
    local data = self.GetCircleTask(id)
    return data and data.reward_rate or nil
end

function CircleTaskDataHelper:GetTargetsDesc(id)
    local data = self.GetCircleTask(id)
    return data and data.targets_desc or nil
end

function CircleTaskDataHelper:GetLittleShoesOpen(id)
    local data = self.GetCircleTask(id)
    return data and data.little_shoes_open or 0
end

function CircleTaskDataHelper:GetLittleShoesOpenStart(id)
    local data = self.GetCircleTask(id)
    return data and data.little_shoes_open_start or 0
end

function CircleTaskDataHelper:GetLittleShoesOpenEnd(id)
    local data = self.GetCircleTask(id)
    return data and data.little_shoes_open_end or 0
end

return CircleTaskDataHelper
