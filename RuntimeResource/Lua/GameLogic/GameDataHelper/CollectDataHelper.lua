-- CollectDataHelper.lua
--采集配置
local CollectDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CollectCfg = Class.CollectCfg(ClassTypes.XObject)

local collectCache = {}
function CollectDataHelper.ClearCache()
    collectCache = {}
end

function CollectCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.model_id = cfg.__model_id
	self.name = cfg.__name
	self.collect_time = cfg.__collect_time
	self.collecting_tips = cfg.__collecting_tips
	self.collected_tips = cfg.__collected_tips
    self.level = cfg.__level
    self.collected_scale = cfg.__collected_scale
    self.skill_time = DataParseHelper.ParseListInt(cfg.__skill_time)
end

function CollectDataHelper.GetCollectCfg(id)
    ClearDataManager:UseCache(CacheNames.collect, CollectDataHelper.ClearCache)
    local data = collectCache[id]
    if data == nil then
        local cfg = XMLManager.collect[id]
        if cfg ~= nil then
            data = CollectCfg(cfg)
            collectCache[data.id] = data
        end
    end
    return data
end

return CollectDataHelper