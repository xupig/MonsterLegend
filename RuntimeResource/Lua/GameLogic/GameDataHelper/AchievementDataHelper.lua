-- AchievementDataHelper.lua
local AchievementDataHelper = {}
local Achievement = Class.Achievement(ClassTypes.XObject)
local AchievementType = Class.AchievementType(ClassTypes.XObject)

local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local AchievementConfig = GameConfig.AchievementConfig
local ClearDataManager = PlayerManager.ClearDataManager

function Achievement:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.name = LanguageDataHelper.CreateContent(cfg.__name)
	self.desc = cfg.__describe and LanguageDataHelper.CreateContent(cfg.__describe) or ""
	self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
	self.achieve_point = cfg.__achieve_point
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward) or {}
    self.desc_type = cfg.__desc_type
end

function AchievementType:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
    self.type_name = LanguageDataHelper.CreateContent(cfg.__type_name)
    self.group = cfg.__group or 0
	self.group_name = LanguageDataHelper.CreateContent(cfg.__group_name)
end

local configCache = {}

function AchievementDataHelper.ClearAchievementCache()
    configCache = {}
end

function AchievementDataHelper.GetAchievementData(id)
	if not id then
		return
    end    
    ClearDataManager:UseCache(CacheNames.achievement, AchievementDataHelper.ClearAchievementCache)
	local data = configCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.achievement[id]
    if data ~= nil then
	    data = Achievement(data) 
    end
	configCache[id] = data
	return data
end

function AchievementDataHelper.GetAllAchievementData()
    local datas = {}
    local keys = XMLManager.achievement:Keys()
    for k, id in pairs(keys) do
        table.insert(datas, id)
    end
    return datas
end

--获取完成成就条件次数
function AchievementDataHelper.GetCompleteNum(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    if data and data.targets and data.targets[1] and data.targets[1][2] then
        return data.targets[1][2] or 0 
    end
    return 0    
end

--获取是否需要显示完成进度
function AchievementDataHelper.GetNeedProgress(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    local type = data and data.desc_type
    if type ~= nil and type == 2 then
        return true
    end
    return false
end

--获取成就的分类主键id
function AchievementDataHelper.GetAchievementTypeId(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    return data and data.type or ""
end

--获取成就标题
function AchievementDataHelper.GetTitle(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    return data and data.name or ""
end

--获取成就描述
function AchievementDataHelper.GetDesc(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    return data and data.desc or ""
end

--获取成就点数
function AchievementDataHelper.GetPoints(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    return data and data.achieve_point or 0
end

--获取成就奖励
function AchievementDataHelper.GetReward(id)
    local data = AchievementDataHelper.GetAchievementData(id)
    local vocation = GameWorld.Player():GetVocationGroup()
    if data and data.reward and data.reward[vocation] then
        local rewardData = {}
        for i=1,#data.reward[vocation],2 do
            table.insert(rewardData, {data.reward[vocation][i], data.reward[vocation][i+1]})
        end
        return rewardData
    end
    return {}
end

--根据成就分类主键id获取成就id列表
function AchievementDataHelper.GetAchievementIdsByType(typeId)
    local datas = {}
    local keys = XMLManager.achievement:Keys()
    for k, id in pairs(keys) do
        local achievementData = AchievementDataHelper.GetAchievementData(id)
        if achievementData.type == typeId then
            table.insert(datas, id)
        end
    end
    return datas
end

--获取成就点统计数据
function AchievementDataHelper:GetAchievementPointsDatas()
    local datas = {}
    local totalPoints = 0
    local keys = XMLManager.achievement:Keys()
    for k, id in pairs(keys) do
        local data = AchievementDataHelper.GetAchievementData(id)
        if data.type ~= AchievementConfig.TOTAL_ACHIEVEMENT and data.type ~= 0  then
            totalPoints = totalPoints + data.achieve_point
            local typeData = AchievementDataHelper.GetAchievementTypeData(data.type)
            local bigType = typeData.type
            if datas[bigType] == nil then
                local d = {}
                d.typeId = bigType
                d.points = data.achieve_point
                datas[bigType] = d
            else
                datas[bigType].points= datas[bigType].points + data.achieve_point
            end
        end
    end
    -- LoggerHelper.Error("数据 ====" .. PrintTable:TableToStr(datas))
    return datas, totalPoints
end

--获取成就总览数据
function AchievementDataHelper.GetAchievementListByType(bigType, groupId)
    local datas = {}
    local keys = XMLManager.achievement:Keys()
    for k, id in pairs(keys) do
        local data = AchievementDataHelper.GetAchievementData(id)
        local typeData = AchievementDataHelper.GetAchievementTypeData(data.type)
        local bt = typeData.type
        local gid = typeData.group
        if bt == bigType and gid == groupId then
            table.insert(datas, id)
        end
    end
    return datas
end

local typeCache = {}
function AchievementDataHelper.ClearAchievementTypeCache()
    typeCache = {}
end

function AchievementDataHelper:GetAllTypeId()
    local ids = {}
    local data = XMLManager.achieve_type:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function AchievementDataHelper.GetAchievementTypeData(id)
	if not id then
		return
    end
    ClearDataManager:UseCache(CacheNames.achieve_type, AchievementDataHelper.ClearAchievementTypeCache)
	local data = typeCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.achieve_type[id]
    if data ~= nil then
	    data = AchievementType(data) 
    end
	typeCache[id] = data
	return data
end

--根据成就类型主键id获取成就大类和成就子类id
function AchievementDataHelper.GetTypeAndGroupByAchievementTypeId(id)
    local typeData = AchievementDataHelper.GetAchievementTypeData(id)
    return typeData.type, typeData.group
end

--获取成就目录数据
function AchievementDataHelper.GetTitleList()
    local typeIds = {}
    local datas = {}
    local keys = XMLManager.achieve_type:Keys()
    local num = 1
    for k, id in pairs(keys) do
        if id ~= AchievementConfig.TOTAL_ACHIEVEMENT and id ~= 0 then
            local typeData = AchievementDataHelper.GetAchievementTypeData(id)
            local bigType = typeData.type
            local group = typeData.group
            if typeIds[bigType] == nil then
                local typeDatas = {}
                typeIds[bigType] = num
                local fData = {}
                fData.id = id
                table.insert(typeDatas, fData)
                if group ~= 0 then
                    local sData = {}
                    sData.id = id
                    table.insert(typeDatas, sData)
                end
                datas[num] = typeDatas
                num = num + 1
            else
                if group ~= 0 then
                    local sData = {}
                    sData.id = id
                    table.insert(datas[typeIds[bigType]], sData)
                end
            end
        end
    end
    -- LoggerHelper.Error("成就列表数据=-===== " .. PrintTable:TableToStr(datas))
    return datas
end

function AchievementDataHelper.GetTypeName(id)
    local data = AchievementDataHelper.GetAchievementTypeData(id)
    return data and data.type_name or ""
end

function AchievementDataHelper.GetGroupName(id)
    local data = AchievementDataHelper.GetAchievementTypeData(id)
    return data and data.group_name or nil
end

function AchievementDataHelper.GetType(id)
    local data = AchievementDataHelper.GetAchievementTypeData(id)
    return data and data.type or nil
end

--根据成就大类获取成就分类主键id列表
function AchievementDataHelper.GetTypeIdsByType(type)
    local datas = {}
    local keys = XMLManager.achieve_type:Keys()
    for k, id in pairs(keys) do
        local typeData = AchievementDataHelper.GetAchievementTypeData(id)
        if type == typeData.type then
            table.insert(datas, id)
        end
    end
    return datas
end

--获取成就目录大类
function AchievementDataHelper.GetBigTypes()
    local typeIds = {}
    local datas = {}
    local keys = XMLManager.achieve_type:Keys()
    for k, id in pairs(keys) do
        local typeData = AchievementDataHelper.GetAchievementTypeData(id)
        if typeData.type ~= AchievementConfig.TOTAL_ACHIEVEMENT and typeData.type ~= 0 then
            if typeIds[typeData.type] == nil then
                typeIds[typeData.type] = typeData.type
                local data = {}
                data.id = id
                data.type = typeData.type
                data.typeName = typeData.type_name
                table.insert(datas, data)
            end
        end
    end
    return datas
end

return AchievementDataHelper
