-- OpenHappyDataHelper.lua
local OpenHappyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager


--========================奖励
local OpenHappyRewardCfgData = Class.OpenHappyRewardCfgData(ClassTypes.XObject)
local openHappyRewardCache = {}

function OpenHappyDataHelper.ClearCache()
    openHappyRewardCache = {}
end

function OpenHappyRewardCfgData:__ctor__(cfg)
    self.id = cfg.__id
    self.point = cfg.__point
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function OpenHappyDataHelper:GetRewardDataById(id)
    ClearDataManager:UseCache(CacheNames.just_hai_reward, OpenHappyDataHelper.ClearCache)
    local data = openHappyRewardCache[id]
    if data == nil then
        local cfg = XMLManager.just_hai_reward[id]
        if cfg ~= nil then
            data = OpenHappyRewardCfgData(cfg)
            openHappyRewardCache[data.id] = data
        else
            LoggerHelper.Error("OpenHappyRewardCfgData id not exist:" .. id)
        end
    end
    return data
end

function OpenHappyDataHelper:GetAllRewardIds()
    local ids = {}
    local data = XMLManager.just_hai_reward:Keys()
    for k, v in pairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end


--========================前往
local OpenHappyEventCfgData = Class.OpenHappyEventCfgData(ClassTypes.XObject)
local openHappyEventCache = {}

function OpenHappyDataHelper.ClearCache()
    openHappyEventCache = {}
end

function OpenHappyEventCfgData:__ctor__(cfg)
    self.id = cfg.__id
    self.targets = DataParseHelper.ParseMSuffix(cfg.__targets)
    self.count = cfg.__count
    self.point = cfg.__point
    self.name = cfg.__name
    self.condition = cfg.__condition
    self.condition_limit = cfg.__condition_limit
    self.time_limit = DataParseHelper.ParseListInt(cfg.__time_limit)
    self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
    self.level = cfg.__level
    self.icon = cfg.__icon
end

function OpenHappyDataHelper:GetEventDataById(id)
    ClearDataManager:UseCache(CacheNames.just_hai, OpenHappyDataHelper.ClearCache)
    local data = openHappyEventCache[id]
    if data == nil then
        local cfg = XMLManager.just_hai[id]
        if cfg ~= nil then
            data = OpenHappyEventCfgData(cfg)
            openHappyEventCache[data.id] = data
        else
            LoggerHelper.Error("OpenHappyEventCfgData id not exist:" .. id)
        end
    end
    return data
end

function OpenHappyDataHelper:GetAllEventIds()
    local ids = {}
    local data = XMLManager.just_hai:Keys()
    for k, v in pairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end


return OpenHappyDataHelper
