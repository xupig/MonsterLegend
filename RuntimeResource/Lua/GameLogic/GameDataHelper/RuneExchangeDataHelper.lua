-- RuneExchangeDataHelper.lua
local RuneExchangeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local RuneExchange = Class.RuneExchange(ClassTypes.XObject)

local runeExchangeCache = {}
function RuneExchangeDataHelper.ClearCache()
    runeExchangeCache = {}
end

local runeConfigData = nil

function RuneExchange:__ctor__(cfg)
    self.id = cfg.__id
    self.rune_id = cfg.__rune_id
    self.price = DataParseHelper.ParseMSuffix(cfg.__price)
    self.floor = cfg.__floor
    self.level = cfg.__level
end

function RuneExchangeDataHelper.GetRuneExchange(id)
    ClearDataManager:UseCache(CacheNames.rune_exchange, RuneExchangeDataHelper.ClearCache)
    local data = runeExchangeCache[id]
    if data == nil then
        local cfg = XMLManager.rune_exchange[id]
        if cfg ~= nil then
            data = RuneExchange(cfg)
            runeExchangeCache[data.id] = data
        else
            LoggerHelper.Error("RuneExchange id not exist:" .. id)
        end
    end
    return data
end

function RuneExchangeDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.rune_exchange:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function RuneExchangeDataHelper:GetRuneId(id)
    local data = self.GetRuneExchange(id)
    return data and data.rune_id or nil
end

function RuneExchangeDataHelper:GetPrice(id)
    local data = self.GetRuneExchange(id)
    return data and data.price or nil
end

function RuneExchangeDataHelper:GetFloor(id)
    local data = self.GetRuneExchange(id)
    return data and data.floor or nil
end

function RuneExchangeDataHelper:GetLevel(id)
    local data = self.GetRuneExchange(id)
    return data and data.level or nil
end

-- runefloor\level为key构建cofigdata
function RuneExchangeDataHelper.GetRuneConfigData()
    if runeConfigData == nil then
        runeConfigData = {}
        local ids = RuneExchangeDataHelper:GetAllId()                        
        for i,v in ipairs(ids) do          
            local id = v
            local runeconfig = RuneExchangeDataHelper.GetRuneExchange(id)
            if runeconfig ~= nil then
                local runefloor = runeconfig.floor
                local runelevel = runeconfig.level
                if runeConfigData[runefloor] == nil then
                    runeConfigData[runefloor] = {}
                end
                if runeConfigData[runefloor][runelevel] == nil then
                    runeConfigData[runefloor][runelevel] = {}
                end
                table.insert(runeConfigData[runefloor][runelevel],runeconfig)
            end
        end
        return runeConfigData            
    else
        return runeConfigData
    end
end

-- runeid\level为key构建cofigdata
function RuneExchangeDataHelper:GetRuneConfigData1()
    if self._runeConfigData == nil then
        self._runeConfigData = {}
        local ids = RuneExchangeDataHelper:GetAllId()                        
        for i,v in ipairs(ids) do          
            local id = v
            local runeconfig = RuneExchangeDataHelper.GetRuneExchange(id)
            if runeconfig ~= nil then
                local runeid = runeconfig.rune_id
                local runelevel = runeconfig.level
                if self._runeConfigData[runeid] == nil then
                    self._runeConfigData[runeid] = {}
                end
                if self._runeConfigData[runeid][runelevel] == nil then
                    self._runeConfigData[runeid][runelevel] = {}
                end
                table.insert(self._runeConfigData[runeid][runelevel],runeconfig)
            end
        end
        return self._runeConfigData            
    else
        return self._runeConfigData
    end
end

function RuneExchangeDataHelper:GetCfgByIdLevel(runeid,level)
    local d = self:GetRuneConfigData1()
    if d and d[runeid] and d[runeid][level] then
        return d[runeid][level]
    end
end
return RuneExchangeDataHelper
