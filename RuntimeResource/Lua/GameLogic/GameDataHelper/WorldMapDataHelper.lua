-- WorldMapDataHelper.lua
local WorldMapDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WorldMap = Class.WorldMap(ClassTypes.XObject)

local worldMapCache = {}
function WorldMapDataHelper.ClearCache()
    worldMapCache = {}
end

function WorldMap:__ctor__(cfg)
    self.id = cfg.__id
    self.button = DataParseHelper.ParseListInt(cfg.__button)
    self.button_name = cfg.__button_name
    self.type_map_icon = cfg.__type_map_icon
    self.regional_icon = cfg.__regional_icon
    self.scene_id = cfg.__scene_id
    self.down_position = DataParseHelper.ParseListInt(cfg.__down_position)
    self.up_position = DataParseHelper.ParseListInt(cfg.__up_position)
end

function WorldMapDataHelper.GetWorldMap(id)
    ClearDataManager:UseCache(CacheNames.world_map, WorldMapDataHelper.ClearCache)
    local data = worldMapCache[id]
    if data == nil then
        local cfg = XMLManager.world_map[id]
        data = WorldMap(cfg)
        worldMapCache[data.id] = data
    end
    return data
end

function WorldMapDataHelper:GetWorldMapDataIdBySceneId(sceneId)
    local ids = {}
    local data = XMLManager.world_map:Keys()
    for i, v in ipairs(data) do
        if self:GetSceneId(v) == sceneId then
            return v
        end
    end
    return nil
end

function WorldMapDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.world_map:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WorldMapDataHelper:GetSceneId(id)
    local data = self.GetWorldMap(id)
    return data and data.scene_id or nil
end

function WorldMapDataHelper:GetTypeMapIcon(id)
    local data = self.GetWorldMap(id)
    return data and data.type_map_icon or nil
end

function WorldMapDataHelper:GetRegionalIcon(id)
    local data = self.GetWorldMap(id)
    return data and data.regional_icon or nil
end

function WorldMapDataHelper:GetButton(id)
    local data = self.GetWorldMap(id)
    -- LoggerHelper.Log("Ash: GetButton:" .. tostring(id) .. " " .. tostring(data.button[1]) .. " " .. tostring(data.button[2]))
    return data and data.button and Vector2(data.button[1], data.button[2]) or nil
end

function WorldMapDataHelper:GetButtonName(id)
    local data = self.GetWorldMap(id)
    -- LoggerHelper.Log("Ash: GetButtonName:" .. tostring(id) .. " " .. tostring(data.button_name))
    return data and data.button_name or nil
end

function WorldMapDataHelper:GetDownPosition(id)
    local data = self.GetWorldMap(id)
    return data and Vector2(data.down_position[1], data.down_position[2]) or nil
end

function WorldMapDataHelper:GetUpPosition(id)
    local data = self.GetWorldMap(id)
    return data and Vector2(data.up_position[1], data.up_position[2]) or nil
end

return WorldMapDataHelper
