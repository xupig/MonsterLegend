-- OnlinePvpSingleServerDataHelper.lua
local OnlinePvpSingleServerDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvpSingleServer = Class.OnlinePvpSingleServer(ClassTypes.XObject)

local onlinePvpSingleServerCache = {}
function OnlinePvpSingleServerDataHelper.ClearCache()
    onlinePvpSingleServerCache = {}
end

function OnlinePvpSingleServer:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.type_name = cfg.__type_name
    self.rank = cfg.__rank
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.point = cfg.__point
    self.credit_point_reward = cfg.__credit_point_reward
end

function OnlinePvpSingleServerDataHelper.GetOnlinePvpSingleServer(id)
    ClearDataManager:UseCache(CacheNames.online_pvp_single_server, OnlinePvpSingleServerDataHelper.ClearCache)
    local data = onlinePvpSingleServerCache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp_single_server[id]
        if cfg ~= nil then
            data = OnlinePvpSingleServer(cfg)
            onlinePvpSingleServerCache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvpSingleServer id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpSingleServerDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp_single_server:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpSingleServerDataHelper:GetIdByPoint(curPoint)
    local data = XMLManager.online_pvp_single_server:Keys()
    local curStageId = 0
    local nextStageId = 0
    for i, v in ipairs(data) do
        if v ~= 0 then
            local point = self:GetPoint(v)
            if point <= curPoint then
                curStageId = v
            else
                nextStageId = v
                break
            end
        end
    end
    if nextStageId == 0 then
        nextStageId = curStageId
    end
    return curStageId, nextStageId
end

function OnlinePvpSingleServerDataHelper:GetType(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.type or nil
end

function OnlinePvpSingleServerDataHelper:GetTypeName(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.type_name or nil
end

function OnlinePvpSingleServerDataHelper:GetRank(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.rank or nil
end

function OnlinePvpSingleServerDataHelper:GetName(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.name or nil
end

function OnlinePvpSingleServerDataHelper:GetIcon(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.icon or nil
end

function OnlinePvpSingleServerDataHelper:GetPoint(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.point or nil
end

function OnlinePvpSingleServerDataHelper:GetCreditPointReward(id)
    local data = self.GetOnlinePvpSingleServer(id)
    return data and data.credit_point_reward or nil
end

return OnlinePvpSingleServerDataHelper
