-- MapFamilyDataHelper.lua
local MapFamilyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local MapFamily = Class.MapFamily(ClassTypes.XObject)

local mapFamilyCache = {}
local blockMapCache = {}

function MapFamily:__ctor__(cfg)
    self.id = cfg.__id
    self.space_name = cfg.__space_name
    self.type = cfg.__type
    self.block = cfg.__block
end

function MapFamilyDataHelper.GetMapFamily(id)
    local data = mapFamilyCache[id]
    if data == nil then
        local cfg = XMLManager.map_family[id]
        data = MapFamily(cfg)
        mapFamilyCache[data.id] = data
    end
    return data
end

function MapFamilyDataHelper:GetMapFamilyIdBySpaceName(spaceName)
    local data = XMLManager.map_family:Keys()
    for i, v in ipairs(data) do
        if self:GetSpaceName(v) == spaceName then
            return v
        end
    end
    return nil
end

function MapFamilyDataHelper:GetMapFamilyIdsByBlock(id)
    local ids = {}
    local curBlock = self:GetBlock(id)
    if blockMapCache[curBlock] ~= nil then
        return blockMapCache[curBlock]
    end
    local data = XMLManager.map_family:Keys()
    for i, v in ipairs(data) do
        if self:GetBlock(v) == curBlock then
            table.insert(ids, v)
        end
    end
    blockMapCache[curBlock] = ids
    return ids
end

function MapFamilyDataHelper:GetSpaceName(id)
    local data = self.GetMapFamily(id)
    return data and data.space_name or nil
end

function MapFamilyDataHelper:GetType(id)
    local data = self.GetMapFamily(id)
    return data and data.type or nil
end

function MapFamilyDataHelper:GetBlock(id)
    local data = self.GetMapFamily(id)
    return data and data.block or nil
end

return MapFamilyDataHelper
