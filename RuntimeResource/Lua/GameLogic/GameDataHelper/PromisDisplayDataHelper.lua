-- PromisDisplayDataHelper.lua
local PromisDisplayDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local PromisDisplay = Class.PromisDisplay(ClassTypes.XObject)

local promisDisplayCache = {}

function PromisDisplay:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
    self.max_promis = cfg.__max_promis
end

function PromisDisplayDataHelper.GetPromisDisplay(id)
    local data = promisDisplayCache[id]
    if data == nil then
        local cfg = XMLManager.promis_display[id]
        if cfg ~= nil then
            data = PromisDisplay(cfg)
            promisDisplayCache[data.id] = data
        else
            LoggerHelper.Error("PromisDisplay id not exist:" .. id)
        end
    end
    return data
end

function PromisDisplayDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.promis_display:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PromisDisplayDataHelper:GetDay(id)
    local data = self.GetPromisDisplay(id)
    return data and data.day or nil
end

function PromisDisplayDataHelper:GetReward(id)
    local data = self.GetPromisDisplay(id)
    return data and data.reward or nil
end

function PromisDisplayDataHelper:GetMaxPromis(id)
    local data = self.GetPromisDisplay(id)
    return data and data.max_promis or nil
end

return PromisDisplayDataHelper
