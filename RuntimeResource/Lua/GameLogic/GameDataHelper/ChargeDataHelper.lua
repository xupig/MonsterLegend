-- SystemInfoDataHelper.lua
local ChargeDataHelper = {}

local XMLManager = GameManager.XMLManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ChargeDataHelper = Class.ChargeDataHelper(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.listing_order) < tonumber(b.listing_order)
end

local chargeDataCache = {}
function ChargeDataHelper.ClearCache()
    chargeDataCache = {}
end

local ChargeDataCfg = Class.ChargeDataCfg(ClassTypes.XObject)

function ChargeDataCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.listing_order = cfg.__listing_order
    self.listing_id = cfg.__listing_id
    self.icon = cfg.__icon
    self.money = cfg.__money
    self.coupons = cfg.__coupons
    self.first_bind_coupons = cfg.__first_bind_coupons
    self.week_card = cfg.__week_card
    self.enable = cfg.__enable
    self.chinese = cfg.__chinese
end

function ChargeDataHelper:GetChargeData(id)
    ClearDataManager:UseCache(CacheNames.charge, ChargeDataHelper.ClearCache)
    local data = chargeDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.charge[id]
    if data~=nil then
	    data = ChargeDataCfg(data)
    end
	chargeDataCache[id] = data
	return data
end

--返回排序后的table(包含所有充值表id)
function ChargeDataHelper:GetShowChargeData()
    local showData = {}
    local data
    local keys = XMLManager.charge:Keys()
    for i=1,#keys do
        data = self:GetChargeData(keys[i])
        if data.enable == 1 then
            local tmp = {}
            tmp.id = keys[i]
            tmp.listing_order = data.listing_order
            table.insert(showData,tmp)
        end
    end
    table.sort(showData, SortByOrder)
    return showData
end

function ChargeDataHelper:GetIcon(id)
    local data = self:GetChargeData(id)
    return data.icon
end

function ChargeDataHelper:GetMoney(id)
    local data = self:GetChargeData(id)
    return data.money
end

function ChargeDataHelper:GetCoupons(id)
    local data = self:GetChargeData(id)
    return data.coupons
end

function ChargeDataHelper:GetFirstBindCoupons(id)
    local data = self:GetChargeData(id)
    return data.first_bind_coupons
end

function ChargeDataHelper:GetWeekCard(id)
    local data = self:GetChargeData(id)
    return data.week_card or 0
end

function ChargeDataHelper:GetName(id)
    local data = self:GetChargeData(id)
    return LanguageDataHelper.CreateContent(data.chinese)
end

function ChargeDataHelper:GetListingId(id)
    local data = self:GetChargeData(id)
    return data.listing_id or 0
end

return ChargeDataHelper
