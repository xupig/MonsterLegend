-- SpecialRemindDataHelper.lua
local SpecialRemindDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local SpecialRemind = Class.SpecialRemind(ClassTypes.XObject)

local specialRemindCache = {}
function SpecialRemindDataHelper.ClearCache()
    specialRemindCache = {}
end

function SpecialRemind:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.circle = cfg.__circle
end

function SpecialRemindDataHelper.GetSpecialRemind(id)
    ClearDataManager:UseCache(CacheNames.special_remind, SpecialRemindDataHelper.ClearCache)
    local data = specialRemindCache[id]
    if data == nil then
        local cfg = XMLManager.special_remind[id]
        if cfg ~= nil then
            data = SpecialRemind(cfg)
            specialRemindCache[data.id] = data
        else
            LoggerHelper.Error("SpecialRemind id not exist:" .. id)
        end
    end
    return data
end

function SpecialRemindDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.special_remind:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function SpecialRemindDataHelper:GetName(id)
    local data = self.GetSpecialRemind(id)
    return data and data.name or nil
end

function SpecialRemindDataHelper:GetIcon(id)
    local data = self.GetSpecialRemind(id)
    return data and data.icon or nil
end

function SpecialRemindDataHelper:GetCircle(id)
    local data = self.GetSpecialRemind(id)
    return data and data.circle or nil
end

return SpecialRemindDataHelper
