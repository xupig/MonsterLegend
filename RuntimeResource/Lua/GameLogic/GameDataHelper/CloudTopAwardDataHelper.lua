-- CloudTopAwardDataHelper.lua
local CloudTopAwardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CloudTopAward = Class.CloudTopAward(ClassTypes.XObject)

local cloudTopAwardCache = {}
function CloudTopAwardDataHelper.ClearCache()
    cloudTopAwardCache = {}
end

function CloudTopAward:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.floor1 = DataParseHelper.ParseMSuffixSort(cfg.__floor1)
    self.floor2 = DataParseHelper.ParseMSuffixSort(cfg.__floor2)
    self.floor3 = DataParseHelper.ParseMSuffixSort(cfg.__floor3)
    self.floor4 = DataParseHelper.ParseMSuffixSort(cfg.__floor4)
    self.floor5 = DataParseHelper.ParseMSuffixSort(cfg.__floor5)
    self.floor6 = DataParseHelper.ParseMSuffixSort(cfg.__floor6)
    self.floor7 = DataParseHelper.ParseMSuffixSort(cfg.__floor7)
    self.floor8 = DataParseHelper.ParseMSuffixSort(cfg.__floor8)
    self.floor9 = DataParseHelper.ParseMSuffixSort(cfg.__floor9)
end

function CloudTopAwardDataHelper.GetCloudTopAward(id)
    ClearDataManager:UseCache(CacheNames.cloud_top_award, CloudTopAwardDataHelper.ClearCache)
    local data = cloudTopAwardCache[id]
    if data == nil then
        local cfg = XMLManager.cloud_top_award[id]
        if cfg ~= nil then
            data = CloudTopAward(cfg)
            cloudTopAwardCache[data.id] = data
        else
            LoggerHelper.Error("CloudTopAward id not exist:" .. id)
        end
    end
    return data
end

function CloudTopAwardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.cloud_top_award:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CloudTopAwardDataHelper:GetLevel(id)
    local data = self.GetCloudTopAward(id)
    return data and data.level or nil
end

function CloudTopAwardDataHelper:GetFloor1(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor1 or nil
end

function CloudTopAwardDataHelper:GetFloor2(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor2 or nil
end

function CloudTopAwardDataHelper:GetFloor3(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor3 or nil
end

function CloudTopAwardDataHelper:GetFloor4(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor4 or nil
end

function CloudTopAwardDataHelper:GetFloor5(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor5 or nil
end

function CloudTopAwardDataHelper:GetFloor6(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor6 or nil
end

function CloudTopAwardDataHelper:GetFloor7(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor7 or nil
end

function CloudTopAwardDataHelper:GetFloor8(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor8 or nil
end

function CloudTopAwardDataHelper:GetFloor9(id)
    local data = self.GetCloudTopAward(id)
    return data and data.floor9 or nil
end

function CloudTopAwardDataHelper:GetRewardByPos(pos)
    local data = {}
    local level = GameWorld.Player().level
    local datas = XMLManager.cloud_top_award:Keys()
    for i, v in ipairs(datas) do
        if v ~= 0 and self:GetLevel(v) == level then
            local cloudData = self.GetCloudTopAward(v)
            data = cloudData and cloudData["floor" .. pos] or {}
            -- LoggerHelper.Error("datas ======" .. PrintTable:TableToStr(data))
            return data             
        end
    end
    return data
end

return CloudTopAwardDataHelper
