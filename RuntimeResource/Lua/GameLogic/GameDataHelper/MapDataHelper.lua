-- MapDataHelper.lua
local MapDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local SceneConfig = GameConfig.SceneConfig
local mapCfgCache = {}
local reviveCfgCache = {}

local MapCfg = Class.MapCfg(ClassTypes.XObject)

function MapCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.space_name = cfg.__space_name
    self.type = cfg.__type
    self.max_count = cfg.__max_count
    self.icon = cfg.__icon
    -- self.revive_id = cfg.__revive_id
    self.revive_points = DataParseHelper.ParseNSuffix(cfg.__revive_points)
    self.music = cfg.__music
    self.chinese = cfg.__chinese
    self.libra = cfg.__libra
    self.map_camera_id = cfg.__map_camera_id
    self.limit_level = cfg.__limit_level
    self.limit_partner = DataParseHelper.ParseListInt(cfg.__limit_partner)
    self.pvp_show_mode = cfg.__pvp_show_mode
end

function MapDataHelper.GetMapCfg(mapId)
    local data = mapCfgCache[mapId]
    if data == nil then
        local cfg = XMLManager.map[mapId]
        if cfg ~= nil then
            data = MapCfg(cfg)
            mapCfgCache[data.id] = data
        else
            LoggerHelper.Error("Map id not exist:" .. mapId)
        end
    end
    return data
end

function MapDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.map:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MapDataHelper.GetIdsBySceneType(sceneType)
    local ids = {}
    local data = XMLManager.map:Keys()
    for i, v in ipairs(data) do
        if MapDataHelper.GetSceneType(v) == sceneType then
            table.insert(ids, v)
        end
    end
    return ids
end

-- function MapDataHelper.GetReviveId(id)
--     local data = MapDataHelper.GetMapCfg(id)
--     return data and data.revive_id or nil
-- end

function MapDataHelper.GetSpaceName(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.space_name or nil
end

function MapDataHelper.IsInCommonInstance(id)
    local type = MapDataHelper.GetSceneType(id)
    return type == SceneConfig.SCENE_TYPE_COMBA
end

function MapDataHelper.GetSceneType(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.type or nil
end

function MapDataHelper.GetMaxCount(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.max_count or nil
end

function MapDataHelper.GetMusic(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.music or nil
end

function MapDataHelper.GetChinese(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.chinese or nil
end

function MapDataHelper.GetRevivePoints(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.revive_points or nil
end

function MapDataHelper.IsLibra(id)
    local data = MapDataHelper.GetMapCfg(id)
    local libra = data.libra or 0
    if libra > 0 then
        return true
    else
        return false
    end
end

function MapDataHelper.GetMapCameraId(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.map_camera_id or 0
end

function MapDataHelper.GetLimitLevel(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data and data.limit_level or 0
end

--
function MapDataHelper.GetLimitPartner(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data.limit_partner
end

function MapDataHelper.GetPvpShowMode(id)
    local data = MapDataHelper.GetMapCfg(id)
    return data.pvp_show_mode or 0
end

--复活配置
local ReviveCfg = Class.ReviveCfg(ClassTypes.XObject)

function ReviveCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.can_revive = cfg.__can_revive
    self.revive_wait_beging = cfg.__revive_wait_beging
    self.revive_wait_punish = cfg.__revive_wait_punish
    self.revive_wait_limit = cfg.__revive_wait_limit
    self.revive_times_limit = cfg.__revive_times_limit
    self.revive_cost = cfg.__revive_cost
    self.can_be_rescue = cfg.__can_be_rescue
    self.rescue_time_progress = cfg.__rescue_time_progress
    self.rescue_wait_punish = cfg.__rescue_wait_punish
    self.rescue_wait_limit = cfg.__rescue_wait_limit
    self.be_rescue_times_limit = cfg.__be_rescue_times_limit
    self.rescue_cost = cfg.__rescue_cost
end

function MapDataHelper.GetReviveCfgByMapId(mapId)
    local mapdata = MapDataHelper.GetMapCfg(mapId)
    if mapdata then
        return MapDataHelper.GetReviveCfg(mapdata.revive_id)
    end
    return nil
end

function MapDataHelper.GetReviveCfg(id)
    local data = reviveCfgCache[id]
    if data == nil then
        local cfg = XMLManager.revive[id]
        data = ReviveCfg(cfg)
        reviveCfgCache[data.id] = data
    end
    return data
end

function MapDataHelper.GetCanRevive()
end

return MapDataHelper
