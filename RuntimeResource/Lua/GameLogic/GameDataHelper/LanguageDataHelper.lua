-- LanguageDataHelper.lua
LanguageDataHelper = {}

local XMLManager = GameManager.XMLManager
local GetLanguageContent = GameMain.LuaFacade.GetLanguageContent
local GameWorld = GameWorld
local ItemDataHelper = GameDataHelper.ItemDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local FontStyleHelper = GameDataHelper.FontStyleHelper
local ItemConfig = GameConfig.ItemConfig
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local MoneyTypeNameMap = {
    [public_config.MONEY_TYPE_GOLD] = 20,
    [public_config.MONEY_TYPE_COUPONS] = 17,
    [public_config.MONEY_TYPE_COUPONS_BIND] = 19,
    [public_config.MONEY_TYPE_ACTIVITY] = 37,
    [public_config.MONEY_TYPE_GUILD_CONTRIBUTE] = 38,
}

function LanguageDataHelper.GetContent(id)
    local data = XMLManager.chinese[id]
    if (data ~= nil) then
        return data.__content
    end
    return nil
end

--local PATTERN_NEED_REPLACE = "{%d+[,%w+]*}" --匹配{1} 或{1,PlayerName}
local PATTERN_TAKE_MARK = "{([%w_]+)}" --匹配{0}或{PlayerName}
local PATTERN_TAKE_MARK2 = "(%w+)_(%w+)" --匹配ItemName_1、SceneName_1

local _contentCache = {}
local _argsTableCache = {}
_argsTableCache._innerTable = {}

function LanguageDataHelper.GetLanguageData(id)
    local data = _contentCache[id]
    if data ~= nil then
        return data
    end
    _contentCache[id] = GetLanguageContent(id)
    return _contentCache[id]
end

function LanguageDataHelper.GetArgsTable()
    return _argsTableCache
end

function LanguageDataHelper.CreateContent(id, lookupTable)
    local result = LanguageDataHelper.GetLanguageData(id)
    if (lookupTable ~= nil) then
        result = string.gsub(result, PATTERN_TAKE_MARK, lookupTable)
    end
    result = string.gsub(result, "\\n", "\n")
    return result
end

function LanguageDataHelper.CreateContentWithArgs(id, args)
    if args == nil then
        return LanguageDataHelper.CreateContent(id)
    end
    local contentTable = LanguageDataHelper.GetArgsTable()
    for k, v in pairs(args) do
        contentTable[k] = v
    end
    return LanguageDataHelper.CreateContent(id, contentTable)
end

function LanguageDataHelper.CreateContentByString(content, argsTable)
    if (argsTable ~= nil) then
        -- LoggerHelper.Error("argsTable： " .. PrintTable:TableToStr(argsTable))
        -- -- local argsTable1 = table.cloneTable(argsTable)
        -- local argsTable1 = {}
        -- argsTable1._innerTable = {}
        -- -- local orgTable = argsTable._innerTable and argsTable._innerTable or argsTable
        -- for k, v in pairs(argsTable._innerTable) do
        --     argsTable1._innerTable[k] = v
        -- end
        -- --最多两层结构，匹配两层
        -- _argsTableCache = argsTable1
        -- LoggerHelper.Error("argsTable1： " .. PrintTable:TableToStr(argsTable1))
        -- LoggerHelper.Error("content 2a： " .. content .. " " .. PrintTable:TableToStr(argsTable))
        content = string.gsub(content, PATTERN_TAKE_MARK, argsTable)
        -- LoggerHelper.Error("argsTable1： " .. PrintTable:TableToStr(argsTable1))
        -- LoggerHelper.Error("content 2b： " .. content .. " " .. PrintTable:TableToStr(argsTable))
        -- LoggerHelper.Error("content 1a： " .. content .. " " .. PrintTable:TableToStr(argsTable))
        -- _argsTableCache = argsTable1
        content = string.gsub(content, PATTERN_TAKE_MARK, argsTable)
        -- LoggerHelper.Error("content 1b： " .. content .. " " .. PrintTable:TableToStr(argsTable))
        _argsTableCache._innerTable = {}
    end
    content = string.gsub(content, "\\n", "\n")
    return content
end

function LanguageDataHelper.GetMoneyContent(moneyType)
    local id = MoneyTypeNameMap[moneyType]
    return LanguageDataHelper.GetContent(id)
end

local _argsMT = {}

local _argsGetter = {}

function _argsGetter.PlayerName()
    if GameWorld.Player() ~= nil then
        return GameWorld.Player().name
    end
    return "PlayerName"
end

-- function _argsGetter.PlayerName(args)
--     local result
--     if args then
--         local name = args
--         local arg = {}
--         arg["0"] = ChatConfig.LinkTypesMap.PlayerName..","..name
--         arg["1"] = FontStyleHelper:GetFontColor(1)
--         arg["2"] = name
--         result = LanguageDataHelper.CreateContentWithArgs(80532,arg)
--     else
--         result = GameWorld.Player().name
--     end
--     return result
-- end
function _argsGetter.ItemName(itemId)
    local result = ItemDataHelper.GetItemName(tonumber(itemId))
    --根据物品品质添加颜色
    local qualityId = ItemDataHelper.GetQuality(tonumber(itemId))
    local colorId = ItemConfig.QualityTextMap[qualityId]
    result = StringStyleUtil.GetColorStringWithColorId("[" .. result .. "]", colorId)
    
    return result
end

function _argsGetter.ItemNameEx(itemId)
    if itemId == nil then
        LoggerHelper.Error("error_code:系统发道具itemId为空")
    end

    local result = {}
    --根据物品品质添加颜色
    local qualityId = ItemDataHelper.GetQuality(tonumber(itemId))
    local colorId = ItemConfig.QualityTextMap[qualityId]
    local color = FontStyleHelper.GetFontStyleData(colorId).color
    local arg = {}
    arg["0"] = ChatConfig.LinkTypesMap.ItemSys.. "," .. itemId
    arg["1"] = color
    arg["2"] = '[' ..ItemDataHelper.GetItemName(tonumber(itemId)) .. ']'
    result = LanguageDataHelper.CreateContent(80532, arg)
    return result
end

function _argsGetter.HappenTime(time)
    local result = DateTimeUtil.GetDateStr(time).." "..DateTimeUtil.GetDateHourMinStr(time)
    return result
end
function _argsGetter.SceneName(sceneId)
    local id = GameDataHelper.MapDataHelper.GetChinese(tonumber(sceneId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 103)
    return result
end

function _argsGetter.MonsterName(monsterId)
    local result = GameDataHelper.MonsterDataHelper.GetMonsterName(tonumber(monsterId))
    result = StringStyleUtil.GetColorStringWithColorId(result, 6)
    return result
end

function _argsGetter.WorldBossName(bossId)
    local id = GameDataHelper.WorldBossDataHelper:GetBossName(tonumber(bossId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 6)
    return result
end

function _argsGetter.BossHomeName(bossId)
    local id = GameDataHelper.BossHomeDataHelper:GetBossName(tonumber(bossId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 6)
    return result
end

function _argsGetter.BackWoodsName(bossId)
    local id = GameDataHelper.BackwoodsDataHelper:GetBossName(tonumber(bossId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 6)
    return result
end

function _argsGetter.GodMonsterIslandName(bossId)
    local id = GameDataHelper.GodMonsterIslandDataHelper:GetName(tonumber(bossId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 6)
    return result
end

function _argsGetter.BabyName(babyId)
    local id = GameDataHelper.MarryChildbaseDataHelper:GetName(tonumber(babyId))
    local result = LanguageDataHelper.GetContent(id)
    result = StringStyleUtil.GetColorStringWithColorId(result, 103)
    return result
end

function _argsGetter.VocationName(vocationId)
    local result = GameDataHelper.RoleDataHelper.GetRoleNameTextByVocation(tonumber(vocationId))
    result = StringStyleUtil.GetColorStringWithColorId(result, 105)
    return result
end

function _argsGetter.GuildPosition(position)
    return LanguageDataHelper.GetContent(GlobalParamsHelper.GetParamValue(570)[tonumber(position)])
end

function _argsGetter.OnlinePVPRank(onlinePvpId)
    local onlinePvpDataHelper = PlayerManager.OnlinePvpManager:GetOnlinePvpDataHelper()
    local id = onlinePvpDataHelper:GetName(tonumber(onlinePvpId))
    local result = LanguageDataHelper.GetContent(id)
    return result
end

--洗炼属性专用颜色显示
local washQualityMap = {1,3,4,5,6}
function _argsGetter.ColorName(quality)
    quality = tonumber(quality)
    quality = washQualityMap[quality]
    local qualityStr = LanguageDataHelper.CreateContent(ItemConfig.QualityCN[quality])
    qualityStr = StringStyleUtil.GetColorStringWithColorId(qualityStr,ItemConfig.QualityTextMap[quality])
    return qualityStr
end

function _argsGetter.AvatarName(name)
    local arg = {}
    arg["0"] = ChatConfig.LinkTypesMap.PlayerName .. "," .. name
    arg["1"] = FontStyleHelper:GetFontColor(1)
    arg["2"] = name
    return LanguageDataHelper.CreateContentWithArgs(80532, arg)
end

_argsMT.__newindex = function(t, k, v)
    t._innerTable[k] = v
end

_argsMT.__index = function(t, k)
    local result = t._innerTable[k]
    -- t._innerTable[k] = nil
    if _argsGetter[k] ~= nil then
        return _argsGetter[k](result)
    else
        local funcName, arg = string.match(k, PATTERN_TAKE_MARK2)
        if funcName ~= nil and _argsGetter[funcName] ~= nil then
            if result ~= nil then
                return _argsGetter[funcName](result)
            else
                return _argsGetter[funcName](arg)
            end
        end
    end
    return result
end

setmetatable(_argsTableCache, _argsMT)

return LanguageDataHelper
