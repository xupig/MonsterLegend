-- AncientBattleDataHelper.lua
local AncientBattleDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

---------------------------------------积分排行榜奖励配置----------------------------
local AncientBattleFieldRankAward = Class.AncientBattleFieldRankAward(ClassTypes.XObject)

function AncientBattleFieldRankAward:__ctor__(cfg)
    self.id = cfg.__id
    self.rank = DataParseHelper.ParseListInt(cfg.__rank)
    self.award = DataParseHelper.ParseNSuffix(cfg.__award)
end

local ancientBattleFieldRankAwardCache = {}
function AncientBattleDataHelper.ClearCache()
    ancientBattleFieldRankAwardCache = {}
end

function AncientBattleDataHelper:GetRankAwardData(id)
    ClearDataManager:UseCache(CacheNames.ancient_battlefield_rank_award, AncientBattleDataHelper.ClearCache)
    local data = ancientBattleFieldRankAwardCache[id]
    if data == nil then
        local cfg = XMLManager.ancient_battlefield_rank_award[id]
        if cfg ~= nil then
            data = AncientBattleFieldRankAward(cfg)
            ancientBattleFieldRankAwardCache[data.id] = data
        else
            LoggerHelper.Error("ancient_battlefield_rank_award id not exist:" .. id)
        end
    end
    return data
end

function AncientBattleDataHelper:GetAllRankAwardId()
    local ids = {}
    local data = XMLManager.ancient_battlefield_rank_award:Keys()
    for i, v in ipairs(data) do  --配表id从1开始且连续
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function AncientBattleDataHelper:GetAncientBattleFieldRankAward(id, level)
    local data = self:GetRankAwardData(id)
    local reward = data.award
    local result
    for k,v in pairs(reward) do
        if v[1] <= level and v[2] >= level then
            result = v
            break
        end
    end
    return result
end

function AncientBattleDataHelper:GetAncientBattleFieldRank(id)
    local data = self:GetRankAwardData(id)
    return data and data.rank or nil
end

---------------------------------------积分任务奖励配置----------------------------
local AncientBattleFieldPointAward = Class.AncientBattleFieldPointAward(ClassTypes.XObject)

function AncientBattleFieldPointAward:__ctor__(cfg)
    self.id = cfg.__id
    self.point = cfg.__point
    self.award = DataParseHelper.ParseNSuffix(cfg.__award)
end

local ancientBattleFieldPointAwardCache = {}
function AncientBattleDataHelper.ClearCache1()
    ancientBattleFieldPointAwardCache = {}
end

function AncientBattleDataHelper:GetPointAwardData(id)
    ClearDataManager:UseCache(CacheNames.ancient_battlefield_point_award, AncientBattleDataHelper.ClearCache1)
    local data = ancientBattleFieldPointAwardCache[id]
    if data == nil then
        local cfg = XMLManager.ancient_battlefield_point_award[id]
        if cfg ~= nil then
            data = AncientBattleFieldPointAward(cfg)
            ancientBattleFieldPointAwardCache[data.id] = data
        else
            LoggerHelper.Error("ancient_battlefield_point_award id not exist:" .. id)
        end
    end
    return data
end

function AncientBattleDataHelper:GetAncientBattleFieldPoint(id)
    local data = self:GetPointAwardData(id)
    return data and data.point or nil
end

function AncientBattleDataHelper:GetAncientBattleFieldAward(id, level)
    local data = self:GetPointAwardData(id)
    local reward = data.award
    local result
    for k,v in pairs(reward) do
        if v[1] <= level and v[2] >= level then
            result = v
            break
        end
    end
    return result
end

function AncientBattleDataHelper:GetAncientBattleFieldPointByPoint(point)
    local id = 1
    local data = XMLManager.ancient_battlefield_point_award:Keys()
    for i=1,#data do  --配表id从1开始且连续
        if data[i] ~= 0 then
            local cfg = self:GetPointAwardData(data[i])
            if cfg.point > point then
                id = data[i]
                break
            end
            id = data[i]
        end
    end
    return id
end

function AncientBattleDataHelper:GetAncientBattleFieldMaxPoint()
    local data = XMLManager.ancient_battlefield_point_award:Keys()
    local maxId = data[#data]
    local cfg = self:GetPointAwardData(maxId)
    return cfg.point
end

return AncientBattleDataHelper
