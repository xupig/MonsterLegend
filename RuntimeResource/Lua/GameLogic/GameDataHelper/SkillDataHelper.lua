-- SkillDataHelper.lua
local SkillDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local Skill = Class.Skill(ClassTypes.XObject)
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

local skillCache = {}
local skillLvUpCfgCache = {} --技能升级配置缓存
local skillUpGradeCache = {} --以职业为key
local skillRuneCache = {}
local skillRuneCacheBySkillId --以技能Id为Key
local skillSlotCache = {}

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

---------------------------------------技能配置表-----------------------------------------------
function Skill:__ctor__(cfg)
	self.id = cfg.__id
	self.name = cfg.__name
	self.desc = cfg.__desc
	self.icon = cfg.__icon
	self.group = cfg.__group
	self.sp_level = cfg.__sp_level
	self.spell_type = cfg.__spell_type
	self.spell_zone = cfg.__spell_zone
	self.vocation = cfg.__vocation
	self.cost = cfg.__cost
	self.cd = DataParseHelper.ParseListInt(cfg.__cd)
	self.pos = cfg.__pos
	self.perview = cfg.__perview
	self.action_cut_time = cfg.__action_cut_time
	self.can_attack_range = cfg.__can_attack_range
end

--获取所有技能配置数据
function SkillDataHelper.GetAllSkill()
	local keys = XMLManager.spell:Keys()
	for i=1,#keys do
		if skillCache[keys[i]] == nil then
			--将数据写入cache
			local data = SkillDataHelper.GetSkill(keys[i])
			--LoggerHelper.Log(data)
		end
	end
	return skillCache
end

function SkillDataHelper.GetSkill(id)
	local data = skillCache[id]
	if data == nil then
		local cfg = XMLManager.spell[id]
		if cfg ~= nil then
			data = Skill(cfg)
			skillCache[data.id] = data
		else
            LoggerHelper.Error("Skill id not exist:" .. id)
		end
	end
	return data
end

function SkillDataHelper.GetName(id)
	local data = SkillDataHelper.GetSkill(id)
	return LanguageDataHelper.CreateContent(data.name) or nil
end

function SkillDataHelper.GetDesc(id)
	local data = SkillDataHelper.GetSkill(id)
	return LanguageDataHelper.CreateContent(data.desc) or nil
end

function SkillDataHelper.GetSkillType(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.spell_type or 0
end

function SkillDataHelper.GetSkillGroup(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.group or 0
end

function SkillDataHelper:GetSkillPos(id)
	local data = self.GetSkill(id)
	return data.pos or 0
end

function SkillDataHelper.GetSkillIcon(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.icon or 0
end

function SkillDataHelper.GetPreviewSkill(id)
	local data = SkillDataHelper.GetSkill(id)
	local result = data.id
	if data.perview ~= nil and data.perview ~= 0 then
		result = data.perview
	end
	return result
end

function SkillDataHelper.GetSkillActionCutTime(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.action_cut_time or 0
end

function SkillDataHelper.GetSkillCanAttackRange(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.can_attack_range or 0
end

function SkillDataHelper.GetSkillSelfCD(id)
	local data = SkillDataHelper.GetSkill(id)
	return data.cd[1] or 0
end

--------------------------------------主动技能表------------------------------------------
local SkillActiveCfg = Class.SkillActiveCfg(ClassTypes.XObject)

function SkillActiveCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.desc = cfg.__desc
	self.skill_ids =  DataParseHelper.ParseNSuffix(cfg.__skill_ids)
end

local skillActiveCache = {}
function SkillDataHelper.GetSkillActiveCfg(id)
	local data = skillActiveCache[id]
	if data == nil then
		local cfg = XMLManager.skill_active[id]
		if cfg ~= nil then
			data = SkillActiveCfg(cfg)
			skillActiveCache[data.id] = data
		end
	end
	return data
end

local allActiveData = nil
function SkillDataHelper.GetAllSkillActiveData()
	if allActiveData == nil then
		local keys = XMLManager.skill_active:Keys()
		allActiveData = {}
		for i=1,#keys do
			if keys[i] > 0 then
				local data = SkillDataHelper.GetSkillActiveCfg(keys[i])
				table.insert(allActiveData, {data.id})
			end
		end
		table.sort(allActiveData, SortByKey1)
	end
	return allActiveData
end

function SkillDataHelper.GetActiveSkillId(id, vocation)
	local data = SkillDataHelper.GetSkillActiveCfg(id)
	return data.skill_ids[vocation]
end

function SkillDataHelper.GetActiveSkillDesc(id)
	local data = SkillDataHelper.GetSkillActiveCfg(id)
	return LanguageDataHelper.CreateContent(data.desc)
end

--------------------------------------被动技能表------------------------------------------
local SkillPassiveCfg = Class.SkillPassiveCfg(ClassTypes.XObject)

function SkillPassiveCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.voc_group = cfg.__voc_group
	self.skill_id = cfg.__skill_id
	self.desc = cfg.__desc
end

local skillPassiveCache = {}
function SkillDataHelper.GetSkillPassiveCfg(id)
	local data = skillPassiveCache[id]
	if data == nil then
		local cfg = XMLManager.skill_passive[id]
		if cfg ~= nil then
			data = SkillPassiveCfg(cfg)
			skillPassiveCache[data.id] = data
		end
	end
	return data
end

local allPassiveData = {}
function SkillDataHelper.GetAllSkillPassiveData(vocation)
	local vocGroup = RoleDataHelper.GetVocGroup(vocation)
	if allPassiveData[vocGroup] == nil then
		local keys = XMLManager.skill_passive:Keys()
		local tmp = {}
		for i=1,#keys do
			local data = SkillDataHelper.GetSkillPassiveCfg(keys[i])
			if data.voc_group == vocGroup then
				table.insert(tmp, {data.id})
			end
		end
		table.sort(tmp, SortByKey1)
		allPassiveData[vocGroup] = tmp
	end
	return allPassiveData[vocGroup]
end

function SkillDataHelper.GetPassiveSkillId(id)
	local data = SkillDataHelper.GetSkillPassiveCfg(id)
	return data.skill_id
end

function SkillDataHelper.GetPassiveSkillLockDesc(id)
	local data = SkillDataHelper.GetSkillPassiveCfg(id)
	return LanguageDataHelper.CreateContent(data.desc)
end

--------------------------------------技能天赋表------------------------------------------
local SkillTalentCfg = Class.SkillTalentCfg(ClassTypes.XObject)

function SkillTalentCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.voc_group = cfg.__voc_group
	self.type = cfg.__type
	self.group = cfg.__group
	self.skill_id = cfg.__skill_id
	self.level = cfg.__level
	self.levelup_cost = cfg.__levelup_cost
	self.condition = DataParseHelper.ParseMSuffixSort(cfg.__condition) or {}
end

local skillTalentCache = {}
function SkillDataHelper.GetSkillTalentCfg(id)
	local data = skillTalentCache[id]
	if data == nil then
		local cfg = XMLManager.skill_talent[id]
		if cfg ~= nil then
			data = SkillTalentCfg(cfg)
			skillTalentCache[data.id] = data
		end
	end
	return data
end

local skillTalentDataCache = nil
function SkillDataHelper.GetSkillTalentData(vocGroup, type, group, level)
	if skillTalentDataCache == nil then
		skillTalentDataCache = {}
		local keys = XMLManager.skill_talent:Keys()
		for i=1,#keys do
			if keys[i] > 0 then
				local data = SkillDataHelper.GetSkillTalentCfg(keys[i])
				if skillTalentDataCache[data.voc_group] == nil then
					skillTalentDataCache[data.voc_group] = {}
				end
				if skillTalentDataCache[data.voc_group][data.type] == nil then
					skillTalentDataCache[data.voc_group][data.type] = {}
				end
				if skillTalentDataCache[data.voc_group][data.type][data.group] == nil then
					skillTalentDataCache[data.voc_group][data.type][data.group] = {}
				end
				skillTalentDataCache[data.voc_group][data.type][data.group][data.level] = data
			end
		end
	end
	return skillTalentDataCache[vocGroup][type][group][level]
end

local skillTalentTypeCache = nil
function SkillDataHelper.GetAllTalentTypes()
	if skillTalentTypeCache == nil then
		skillTalentTypeCache = {}
		for k,v in pairs(GlobalParamsHelper.GetParamValue(467)) do
			table.insert(skillTalentTypeCache, {k})
		end
		table.sort(skillTalentTypeCache, SortByKey1)
	end
	return skillTalentTypeCache
end

--------------------------------------技能天赋等级表------------------------------------------
local SkillTalentLvCfg = Class.SkillTalentLvCfg(ClassTypes.XObject)

function SkillTalentLvCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.group = cfg.__group
	self.max_level = cfg.__max_level
end

local skillTalentLvCache = {}
function SkillDataHelper.GetSkillTalentLvCfg(id)
	local data = skillTalentLvCache[id]
	if data == nil then
		local cfg = XMLManager.skill_talent_lv[id]
		if cfg ~= nil then
			data = SkillTalentLvCfg(cfg)
			skillTalentLvCache[data.id] = data
		end
	end
	return data
end

local typeAndGroupTalentLvData = nil
function SkillDataHelper.GetSkillTalentLvDataByTypeAndGroup(type, group)
	if typeAndGroupTalentLvData == nil then
		typeAndGroupTalentLvData = {}
		local keys = XMLManager.skill_talent_lv:Keys()
		for i=1,#keys do
			if keys[i] > 0 then
				local data = SkillDataHelper.GetSkillTalentLvCfg(keys[i])
				if typeAndGroupTalentLvData[data.type] == nil then
					typeAndGroupTalentLvData[data.type] = {}
				end
				typeAndGroupTalentLvData[data.type][data.group] = data
			end
		end
	end
	return typeAndGroupTalentLvData[type][group]
end

local skillTalentLvTypeCache = {}
function SkillDataHelper.GetAllTalentLvDataByType(type)
	local result = skillTalentLvTypeCache[type]
	if result == nil then
		local keys = XMLManager.skill_talent_lv:Keys()
		for i=1,#keys do
			if keys[i] > 0 then
				local data = SkillDataHelper.GetSkillTalentLvCfg(keys[i])
				if skillTalentLvTypeCache[data.type] == nil then
					skillTalentLvTypeCache[data.type] = {}
				end
				table.insert(skillTalentLvTypeCache[data.type], data.group)
			end
		end
		result = skillTalentLvTypeCache[type]
	end
	return result
end

function SkillDataHelper.GetTalentLvMaxLevel(type, group)
	local data = SkillDataHelper.GetSkillTalentLvDataByTypeAndGroup(type, group)
	return data.max_level
end

return SkillDataHelper