-- CardLevelDataHelper.lua
local CardLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CardLevel = Class.CardLevel(ClassTypes.XObject)

local cardLevelCache = {}
local cardCostCache = {}
local cardFullLevelCache = {}
function CardLevelDataHelper.ClearCache()
    cardLevelCache = {}
    cardCostCache = {}
    cardFullLevelCache = {}
end

function CardLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.card_id = cfg.__card_id
    self.star = cfg.__star
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

function CardLevelDataHelper.GetCardLevel(id)
    ClearDataManager:UseCache(CacheNames.card_level, CardLevelDataHelper.ClearCache)
    local data = cardLevelCache[id]
    if data == nil then
        local cfg = XMLManager.card_level[id]
        if cfg ~= nil then
            data = CardLevel(cfg)
            cardLevelCache[data.id] = data
        else
            LoggerHelper.Error("CardLevel id not exist:" .. id)
        end
    end
    return data
end

function CardLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.card_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CardLevelDataHelper:GetCardId(id)
    local data = self.GetCardLevel(id)
    return data and data.card_id or nil
end

function CardLevelDataHelper:GetStar(id)
    local data = self.GetCardLevel(id)
    return data and data.star or nil
end

function CardLevelDataHelper:GetCost(id)
    local data = self.GetCardLevel(id)
    return data and data.cost or nil
end

function CardLevelDataHelper:GetAttri(id)
    local data = self.GetCardLevel(id)
    return data and data.attri or nil
end


function CardLevelDataHelper:GetCardCost(id,star)
    if cardCostCache[id] and cardCostCache[id][star] then
        return cardCostCache[id][star]
    end
    local data = XMLManager.card_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            if id == CardLevelDataHelper:GetCardId(v) and star == CardLevelDataHelper:GetStar(v) then
                if not cardCostCache[id] then 
                    cardCostCache[id] = {} 
                end
                if not cardCostCache[id][star] then 
                    cardCostCache[id][star] = {} 
                end             
                cardCostCache[id][star] = CardLevelDataHelper:GetCost(v)
                return cardCostCache[id][star]
            end
        end
    end
end

function CardLevelDataHelper:isFullLevel(id,star)
    if cardFullLevelCache[id] and cardFullLevelCache[id][star] then
        return cardFullLevelCache[id][star]
    end
    local data = XMLManager.card_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            if id == CardLevelDataHelper:GetCardId(v) and star == CardLevelDataHelper:GetStar(v) then
                if not cardFullLevelCache[id] then 
                    cardFullLevelCache[id] = {} 
                end
                if not cardFullLevelCache[id][star] then 
                    cardFullLevelCache[id][star] = {} 
                end          
                if not CardLevelDataHelper:GetCost(v)then
                    cardFullLevelCache[id][star] = true
                else
                    cardFullLevelCache[id][star] = false
                end   
                return cardFullLevelCache[id][star]
            end
        end
    end
end


return CardLevelDataHelper
