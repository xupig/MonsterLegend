-- PrayDataHelper.lua
local PrayDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Pray = Class.Pray(ClassTypes.XObject)

local prayCache = {}
function PrayDataHelper.ClearCache()
    prayCache = {}
end


function Pray:__ctor__(cfg)
    self.id = cfg.__id
    self.exp = cfg.__exp
end

function PrayDataHelper.GetPray(id)
    ClearDataManager:UseCache(CacheNames.pray, PrayDataHelper.ClearCache)
    local data = prayCache[id]
    if data == nil then
        local cfg = XMLManager.pray[id]
        if cfg ~= nil then
            data = Pray(cfg)
            prayCache[data.id] = data
        else
            LoggerHelper.Error("Pray id not exist:" .. id)
        end
    end
    return data
end

function PrayDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.pray:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PrayDataHelper:GetExp(id)
    local data = self.GetPray(id)
    return data and data.exp or nil
end

return PrayDataHelper
