-- OpenCardCrystalDataHelper.lua
local OpenCardCrystalDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OpenCardCrystal = Class.OpenCardCrystal(ClassTypes.XObject)

local openCardCrystalCache = {}
function OpenCardCrystalDataHelper.ClearCache()
    openCardCrystalCache = {}
end

function OpenCardCrystal:__ctor__(cfg)
    self.id = cfg.__id
    self.step = cfg.__step
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.prop = cfg.__prop
end

function OpenCardCrystalDataHelper.GetOpenCardCrystal(id)
    ClearDataManager:UseCache(CacheNames.open_card_crystal, OpenCardCrystalDataHelper.ClearCache)
    local data = openCardCrystalCache[id]
    if data == nil then
        local cfg = XMLManager.open_card_crystal[id]
        if cfg ~= nil then
            data = OpenCardCrystal(cfg)
            openCardCrystalCache[data.id] = data
        else
            LoggerHelper.Error("OpenCardCrystal id not exist:" .. id)
        end
    end
    return data
end

function OpenCardCrystalDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.open_card_crystal:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OpenCardCrystalDataHelper:GetStep(id)
    local data = self.GetOpenCardCrystal(id)
    return data and data.step or nil
end

function OpenCardCrystalDataHelper:GetReward(id)
    local data = self.GetOpenCardCrystal(id)
    return data and data.reward or nil
end

function OpenCardCrystalDataHelper:GetProp(id)
    local data = self.GetOpenCardCrystal(id)
    return data and data.prop or nil
end

return OpenCardCrystalDataHelper
