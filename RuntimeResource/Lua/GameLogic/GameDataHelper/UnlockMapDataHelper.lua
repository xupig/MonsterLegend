-- UnlockMapDataHelper.lua
local UnlockMapDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local UnlockMap = Class.UnlockMap(ClassTypes.XObject)

local unlockMapCache = {}
function UnlockMapDataHelper.ClearCache()
    unlockMapCache = {}
end

function UnlockMap:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.map_id = cfg.__map_id
    self.trigger_id = DataParseHelper.ParseListInt(cfg.__trigger_id)
    self.cover_position = DataParseHelper.ParseListInt(cfg.__cover_position)
end

function UnlockMapDataHelper.GetUnlockMap(id)
    ClearDataManager:UseCache(CacheNames.unlock_map, UnlockMapDataHelper.ClearCache)
    local data = unlockMapCache[id]
    if data == nil then
        local cfg = XMLManager.unlock_map[id]
        if cfg ~= nil then
            data = UnlockMap(cfg)
            unlockMapCache[data.id] = data
        else
            LoggerHelper.Error("UnlockMap id not exist:" .. id)
        end
    end
    return data
end

function UnlockMapDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.unlock_map:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function UnlockMapDataHelper:GetIdUnlockMapByMapId(mapId)
    local ids = {}
    local data = XMLManager.unlock_map:Keys()
    for i, v in ipairs(data) do
        if self:GetMapId(v) == mapId then
            table.insert(ids, v)
        end
    end
    return ids
end

function UnlockMapDataHelper:GetIdUnlockMapByMapIdAndTriggerId(mapId, triggerId)
    local ids = {}
    local data = XMLManager.unlock_map:Keys()
    for i, v in ipairs(data) do
        if self:GetMapId(v) == mapId then
            local triggerIds = self:GetTriggerId(v)
            for ii, vv in ipairs(triggerIds) do
                if triggerId == vv then
                    table.insert(ids, v)
                end
            end
        end
    end
    return ids
end

function UnlockMapDataHelper:GetName(id)
    local data = self.GetUnlockMap(id)
    return data and data.name or nil
end

function UnlockMapDataHelper:GetMapId(id)
    local data = self.GetUnlockMap(id)
    return data and data.map_id or nil
end

function UnlockMapDataHelper:GetTriggerId(id)
    local data = self.GetUnlockMap(id)
    return data and data.trigger_id or nil
end

function UnlockMapDataHelper:GetCoverPosition(id)
    local data = self.GetUnlockMap(id)
    return data and Vector2(data.cover_position[1], data.cover_position[2]) or nil
end

return UnlockMapDataHelper
