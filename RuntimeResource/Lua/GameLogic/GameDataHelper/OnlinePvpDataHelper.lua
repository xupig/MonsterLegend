-- OnlinePvpDataHelper.lua
local OnlinePvpDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvp = Class.OnlinePvp(ClassTypes.XObject)

local onlinePvpCache = {}
function OnlinePvpDataHelper.ClearCache()
    onlinePvpCache = {}
end

function OnlinePvp:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.type_name = cfg.__type_name
    self.rank = cfg.__rank
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.point = cfg.__point
    self.credit_point_reward = cfg.__credit_point_reward
end

function OnlinePvpDataHelper.GetOnlinePvp(id)
    ClearDataManager:UseCache(CacheNames.online_pvp, OnlinePvpDataHelper.ClearCache)
    local data = onlinePvpCache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp[id]
        if cfg ~= nil then
            data = OnlinePvp(cfg)
            onlinePvpCache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvp id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpDataHelper:GetIdByPoint(curPoint)
    local data = XMLManager.online_pvp:Keys()
    local curStageId = 0
    local nextStageId = 0
    for i, v in ipairs(data) do
        if v ~= 0 then
            local point = self:GetPoint(v)
            if point <= curPoint then
                curStageId = v
            else
                nextStageId = v
                break
            end
        end
    end
    if nextStageId == 0 then
        nextStageId = curStageId
    end
    return curStageId, nextStageId
end

function OnlinePvpDataHelper:GetType(id)
    local data = self.GetOnlinePvp(id)
    return data and data.type or nil
end

function OnlinePvpDataHelper:GetTypeName(id)
    local data = self.GetOnlinePvp(id)
    return data and data.type_name or nil
end

function OnlinePvpDataHelper:GetRank(id)
    local data = self.GetOnlinePvp(id)
    return data and data.rank or nil
end

function OnlinePvpDataHelper:GetName(id)
    local data = self.GetOnlinePvp(id)
    return data and data.name or nil
end

function OnlinePvpDataHelper:GetIcon(id)
    local data = self.GetOnlinePvp(id)
    return data and data.icon or nil
end

function OnlinePvpDataHelper:GetPoint(id)
    local data = self.GetOnlinePvp(id)
    return data and data.point or nil
end

function OnlinePvpDataHelper:GetCreditPointReward(id)
    local data = self.GetOnlinePvp(id)
    return data and data.credit_point_reward or nil
end

return OnlinePvpDataHelper
