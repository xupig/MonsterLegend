-- ChangeVocationViewDataHelper.lua
local ChangeVocationViewDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ChangeVocationView = Class.ChangeVocationView(ClassTypes.XObject)

local changeVocationViewCache = {}
function ChangeVocationViewDataHelper.ClearCache()
    changeVocationViewCache = {}
end

function ChangeVocationView:__ctor__(cfg)
    self.id = cfg.__id
    self.vocation = cfg.__vocation
    self.change_num = cfg.__change_num
    self.equip_open = DataParseHelper.ParseListInt(cfg.__equip_open)
    self.model_open = DataParseHelper.ParseListInt(cfg.__model_open)
    self.atrri = DataParseHelper.ParseMSuffix(cfg.__atrri)
    self.attack_up = DataParseHelper.ParseNSuffix(cfg.__attack_up)
    self.skill_up = DataParseHelper.ParseNSuffix(cfg.__skill_up)
    self.new_skill = DataParseHelper.ParseNSuffix(cfg.__new_skill)
end

function ChangeVocationViewDataHelper.GetChangeVocationView(id)
    ClearDataManager:UseCache(CacheNames.change_vocation_view, ChangeVocationViewDataHelper.ClearCache)
    local data = changeVocationViewCache[id]
    if data == nil then
        local cfg = XMLManager.change_vocation_view[id]
        if cfg ~= nil then
            data = ChangeVocationView(cfg)
            changeVocationViewCache[data.id] = data
        else
            LoggerHelper.Error("ChangeVocationView id not exist:" .. id)
        end
    end
    return data
end

function ChangeVocationViewDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.change_vocation_view:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ChangeVocationViewDataHelper:GetVocation(id)
    local data = self.GetChangeVocationView(id)
    return data and data.vocation or nil
end

function ChangeVocationViewDataHelper:GetChangeNum(id)
    local data = self.GetChangeVocationView(id)
    return data and data.change_num or nil
end

function ChangeVocationViewDataHelper:GetEquipOpen(id)
    local data = self.GetChangeVocationView(id)
    return data and data.equip_open or nil
end

function ChangeVocationViewDataHelper:GetModelOpen(id)
    local data = self.GetChangeVocationView(id)
    return data and data.model_open or nil
end

function ChangeVocationViewDataHelper:GetAtrri(id)
    local data = self.GetChangeVocationView(id)
    return data and data.atrri or nil
end

function ChangeVocationViewDataHelper:GetAttackUp(id)
    local data = self.GetChangeVocationView(id)
    return data and data.attack_up or nil
end

function ChangeVocationViewDataHelper:GetSkillUp(id)
    local data = self.GetChangeVocationView(id)
    return data and data.skill_up or nil
end

function ChangeVocationViewDataHelper:GetNewSkill(id)
    local data = self.GetChangeVocationView(id)
    return data and data.new_skill or nil
end

return ChangeVocationViewDataHelper
