-- PartnerDataHelper.lua
PartnerDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local attri_config = GameWorld.attri_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end


-----------------------------------------宠物配置--------------------------------------------------
local PartnerPetData = Class.PartnerPetData(ClassTypes.XObject)

function PartnerPetData:__ctor__(cfg)
    self.id = cfg.__id
    self.grade = cfg.__grade
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.grade_icon = cfg.__grade_icon
    self.desc = cfg.__desc
    self.mode = cfg.__mode
    self.ai_id = cfg.__ai_id
    self.speed = cfg.__speed
    self.scale = cfg.__scale
    self.rotate = cfg.__rotate
    self.skill = DataParseHelper.ParseListInt(cfg.__skill)
    self.passive_skill = DataParseHelper.ParseListInt(cfg.__passive_skill)
    self.skill_ids = DataParseHelper.ParseListInt(cfg.__skill_ids)
end

local petCache = nil
function PartnerDataHelper.ClearCache()
    petCache = nil
end

function PartnerDataHelper.GetPet(grade)
    ClearDataManager:UseCache(CacheNames.pet, PartnerDataHelper.ClearCache)
    if petCache == nil then
        petCache = {}
        local keys = XMLManager.pet:Keys()
        for i=1,#keys do
            local cfg = XMLManager.pet[keys[i]]
            local data = PartnerPetData(cfg)
            petCache[data.grade] = data
		end     
    end
	return petCache[grade]
end

function PartnerDataHelper.GetPetModel(grade)
	local data = PartnerDataHelper.GetPet(grade)
	if data ~= nil then
		return data.mode
	end
	return 0
end

function PartnerDataHelper.GetPetAI(grade)
	local data = PartnerDataHelper.GetPet(grade)
	return data.ai_id
end

function PartnerDataHelper.GetPetAISkills(grade)
	local data = PartnerDataHelper.GetPet(grade)
	return data.skill_ids
end

function PartnerDataHelper.GetPetSpeed(grade)
	local data = PartnerDataHelper.GetPet(grade)
	return data.speed
end

function PartnerDataHelper.GetPetScale(grade)
	local data = PartnerDataHelper.GetPet(grade)
	return data.scale * 0.01
end

function PartnerDataHelper.GetPetName(grade)
    local data = PartnerDataHelper.GetPet(grade)
	return data.name
end

function PartnerDataHelper.GetPetRotate(grade)
    local data = PartnerDataHelper.GetPet(grade)
	return data.rotate or 90
end

function PartnerDataHelper.GetPetNameIcon(grade)
	local data = PartnerDataHelper.GetPet(grade)
	if data ~= nil then
		return data.icon
	end
	return 0
end

function PartnerDataHelper.GetPetGradeIcon(grade)
	local data = PartnerDataHelper.GetPet(grade)
	if data ~= nil then
		return data.grade_icon
	end
	return 0
end

local allSkills = nil
function PartnerDataHelper.GetPetAllSkills()
    if allSkills == nil then
        allSkills = {}
        local data = GlobalParamsHelper.GetParamValue(425)
        for skill, grade in pairs(data) do
            table.insert(allSkills, {["skill"]=skill,["grade"]=grade,["order"]=grade})
        end
        table.sort(allSkills,SortByOrder)
    end
	return allSkills
end

function PartnerDataHelper.GetPetSkills(grade)
    local allData = PartnerDataHelper.GetPetAllSkills()
    local data = PartnerDataHelper.GetPet(grade)
    local result = {}
    local unLockData = {}
	if data ~= nil then
        for i=1, #data.skill do
            unLockData[data.skill[i]] = 1
        end
        if data.passive_skill ~= nil then
            for i=1, #data.passive_skill do
                unLockData[data.passive_skill[i]] = 1
            end
        end
    end
    for i=1, #allData do
        if unLockData[allData[i].skill] ~= nil then
            table.insert(result, {["skill"]=allData[i].skill, ["lock"]=0, ["grade"]=allData[i].grade})
        else
            table.insert(result, {["skill"]=allData[i].skill, ["lock"]=1, ["grade"]=allData[i].grade})
        end
    end
	return result --{{skill,lock,grade}}
end

--{itemId,blessValue}
function PartnerDataHelper.GetPetBlessItems()
    local data = GlobalParamsHelper.GetParamValue(424)
    local result = {}
    for k,v in pairs(data) do
        table.insert(result, {k,v})
    end
    table.sort(result, SortByKey1)
    return result
end

function PartnerDataHelper.GetPetEvoItems()
    local result = {}
    local data = GlobalParamsHelper.GetParamValue(426)
    for k,v in pairs(data) do
        table.insert(result, {k,v})
    end
    table.sort(result, SortByKey1)
    return result
end

function PartnerDataHelper.GetPetLevelItems()
    local result = GlobalParamsHelper.GetParamValue(520)
    return result
end

-----------------------------------------宠物升星配置--------------------------------------------------
local PartnerPetEvoData = Class.PartnerPetEvoData(ClassTypes.XObject)

function PartnerPetEvoData:__ctor__(cfg)
    self.id = cfg.__id
    self.grade = cfg.__grade
    self.star = cfg.__star
    self.bless = cfg.__bless
    self.role_atrri = DataParseHelper.ParseMSuffix(cfg.__role_atrri)
end

local petEvoCache = {}
function PartnerDataHelper.ClearPetEvoCache()
    petEvoCache = {}
end

function PartnerDataHelper.GetPetEvoCfg(id)
    ClearDataManager:UseCache(CacheNames.pet_evo, PartnerDataHelper.ClearPetEvoCache)
	local data = petEvoCache[id]
    if data == nil then
		local cfg = XMLManager.pet_evo[id]
		if cfg ~= nil then
			data = PartnerPetEvoData(cfg)
			petEvoCache[data.id] = data
		end
	end
	return data
end

local petEvoByGradeAndStarCache = {}
function PartnerDataHelper.GetPetEvoByGradeAndStar(grade,star)
	if petEvoByGradeAndStarCache[grade] == nil then
		petEvoByGradeAndStarCache[grade] = {}
	end
	if petEvoByGradeAndStarCache[grade][star] == nil then
		local keys = XMLManager.pet_evo:Keys()
		for i=1,#keys do
			local data = PartnerDataHelper.GetPetEvoCfg(keys[i])
			if data.grade == grade and data.star == star then
				petEvoByGradeAndStarCache[grade][star] = data
			end
		end
	end
	return petEvoByGradeAndStarCache[grade][star]
end

function PartnerDataHelper.GetPetAttri(grade, star)
    local data = PartnerDataHelper.GetPetEvoByGradeAndStar(grade,star)
    if data == nil then
        return nil
    end
    return data.role_atrri
end

function PartnerDataHelper.GetPetBless(grade, star)
    local data = PartnerDataHelper.GetPetEvoByGradeAndStar(grade,star)
    return data.bless
end

function PartnerDataHelper.GetPetAttriNextDelta(grade, star)
    local result = {}
    local isFull = false
    local data = PartnerDataHelper.GetPetAttri(grade, star)
    local nextData = PartnerDataHelper.GetPetAttri(grade, star+1)
    if nextData == nil then
        nextData = PartnerDataHelper.GetPetAttri(grade+1, 1)
    end
    --满阶满星
    if nextData == nil then
        isFull = true
    end
    for k,v in pairs(data) do
        local delta = 0
        if not isFull then
            delta = nextData[k] - v
        end
        result[k] = delta
    end
    return result
end


-----------------------------------------宠物升级配置--------------------------------------------------
local PartnerPetLevelData = Class.PartnerPetLevelData(ClassTypes.XObject)

function PartnerPetLevelData:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.next_exp = cfg.__next_exp
    self.role_atrri = DataParseHelper.ParseMSuffix(cfg.__role_atrri)
end

local petLevelCache = nil
function PartnerDataHelper.ClearPetLevelCache()
    petLevelCache = nil
end

function PartnerDataHelper.GetPetLevelCfg(level)
    ClearDataManager:UseCache(CacheNames.pet_level, PartnerDataHelper.ClearPetLevelCache)
    if petLevelCache == nil then
        petLevelCache = {}
        local keys = XMLManager.pet_level:Keys()
        for i=1,#keys do
            local cfg = XMLManager.pet_level[keys[i]]
            local data = PartnerPetLevelData(cfg)
            petLevelCache[data.level] = data
		end     
    end
	return petLevelCache[level]
end

function PartnerDataHelper.GetPetLevelExp(level)
    local data = PartnerDataHelper.GetPetLevelCfg(level)
    if data == nil then
        return nil
    end
    return data.next_exp
end

function PartnerDataHelper.GetPetLevelAtrri(level)
    local data = PartnerDataHelper.GetPetLevelCfg(level)
    return data.role_atrri
end


-----------------------------------------坐骑配置--------------------------------------------------
local PartnerRideData = Class.PartnerRideData(ClassTypes.XObject)

function PartnerRideData:__ctor__(cfg)
    self.id = cfg.__id
    self.grade = cfg.__grade
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.grade_icon = cfg.__grade_icon
    self.desc = cfg.__desc
    self.mode = cfg.__mode
    self.scale = cfg.__scale
    self.skill = DataParseHelper.ParseListInt(cfg.__skill)
    self.passive_skill = DataParseHelper.ParseListInt(cfg.__passive_skill)
end

local rideCache = nil
function PartnerDataHelper.ClearRideCache()
    rideCache = nil
end

function PartnerDataHelper.GetRide(grade)
    ClearDataManager:UseCache(CacheNames.horse, PartnerDataHelper.ClearRideCache)
    if rideCache == nil then
        rideCache = {}
        local keys = XMLManager.horse:Keys()
        for i=1,#keys do
            local cfg = XMLManager.horse[keys[i]]
            local data = PartnerRideData(cfg)
            rideCache[data.grade] = data
		end     
    end
	return rideCache[grade]
end

function PartnerDataHelper.GetRideModel(grade)
	local data = PartnerDataHelper.GetRide(grade)
	if data ~= nil then
		return data.mode
	end
	return 0
end

function PartnerDataHelper.GetRideScale(grade)
	local data = PartnerDataHelper.GetRide(grade)
	if data ~= nil then
		return data.scale * 0.01
	end
	return 0
end

function PartnerDataHelper.GetHorseName(grade)
    local data = PartnerDataHelper.GetRide(grade)
    return data.name
end

function PartnerDataHelper.GetRideNameIcon(grade)
	local data = PartnerDataHelper.GetRide(grade)
	if data ~= nil then
		return data.icon
	end
	return 0
end

function PartnerDataHelper.GetRideGradeIcon(grade)
	local data = PartnerDataHelper.GetRide(grade)
	if data ~= nil then
		return data.grade_icon
	end
	return 0
end

local allSkills = nil
function PartnerDataHelper.GetRideAllSkills()
    if allSkills == nil then
        allSkills = {}
        local data = GlobalParamsHelper.GetParamValue(428)
        for skill, grade in pairs(data) do
            table.insert(allSkills, {["skill"]=skill,["grade"]=grade,["order"]=grade})
        end
        table.sort(allSkills,SortByOrder)
    end
	return allSkills
end

function PartnerDataHelper.GetRideSkills(grade)
    local allData = PartnerDataHelper.GetRideAllSkills()
    local data = PartnerDataHelper.GetRide(grade)
    local result = {}
    local unLockData = {}
	if data ~= nil then
        for i=1, #data.skill do
            unLockData[data.skill[i]] = 1
        end
        if data.passive_skill ~= nil then
            for i=1, #data.passive_skill do
                unLockData[data.passive_skill[i]] = 1
            end
        end
    end
    for i=1, #allData do
        if unLockData[allData[i].skill] ~= nil then
            table.insert(result, {["skill"]=allData[i].skill,["lock"]=0, ["grade"]=allData[i].grade})
        else
            table.insert(result, {["skill"]=allData[i].skill,["lock"]=1, ["grade"]=allData[i].grade})
        end
    end
	return result --{{skill,lock,grade}}
end

--{itemId,blessValue}
function PartnerDataHelper.GetRideBlessItems()
    local data = GlobalParamsHelper.GetParamValue(427)
    local result = {}
    for k,v in pairs(data) do
        table.insert(result, {k,v})
    end
    table.sort(result, SortByKey1)
    return result
end

function PartnerDataHelper.GetRideEvoItems()
    local result = {}
    local data = GlobalParamsHelper.GetParamValue(429)
    for k,v in pairs(data) do
        table.insert(result, {k,v})
    end
    table.sort(result, SortByKey1)
    return result
end


-----------------------------------------坐骑升星配置--------------------------------------------------
local PartnerRideEvoData = Class.PartnerRideEvoData(ClassTypes.XObject)

function PartnerRideEvoData:__ctor__(cfg)
    self.id = cfg.__id
    self.grade = cfg.__grade
    self.star = cfg.__star
    self.bless = cfg.__bless
    self.role_atrri = DataParseHelper.ParseMSuffix(cfg.__role_atrri)
end

local rideEvoCache = {}
function PartnerDataHelper.ClearRideEvoCache()
    rideEvoCache = {}
end

function PartnerDataHelper.GetRideEvoCfg(id)
    ClearDataManager:UseCache(CacheNames.horse_evo, PartnerDataHelper.ClearRideEvoCache)
	local data = rideEvoCache[id]
	if data == nil then
		local cfg = XMLManager.horse_evo[id]
		if cfg ~= nil then
			data = PartnerRideEvoData(cfg)
			rideEvoCache[data.id] = data
		end
	end
	return data
end

local rideEvoByGradeAndStarCache = {}
function PartnerDataHelper.GetRideEvoByGradeAndStar(grade,star)
	if rideEvoByGradeAndStarCache[grade] == nil then
		rideEvoByGradeAndStarCache[grade] = {}
	end
	if rideEvoByGradeAndStarCache[grade][star] == nil then
		local keys = XMLManager.horse_evo:Keys()
		for i=1,#keys do
			local data = PartnerDataHelper.GetRideEvoCfg(keys[i])
			if data.grade == grade and data.star == star then
				rideEvoByGradeAndStarCache[grade][star] = data
			end
		end
	end
	return rideEvoByGradeAndStarCache[grade][star]
end

function PartnerDataHelper.GetRideAttri(grade, star)
    local data = PartnerDataHelper.GetRideEvoByGradeAndStar(grade,star)
    if data == nil then
        return nil
    end
    return data.role_atrri
end

function PartnerDataHelper.GetRideBless(grade, star)
    local data = PartnerDataHelper.GetRideEvoByGradeAndStar(grade,star)
    return data.bless
end

function PartnerDataHelper.GetRideAttriNextDelta(grade, star)
    local result = {}
    local isFull = false
    local data = PartnerDataHelper.GetRideAttri(grade, star)
    local nextData = PartnerDataHelper.GetRideAttri(grade, star+1)
    if nextData == nil then
        nextData = PartnerDataHelper.GetRideAttri(grade+1, 1)
    end
    --满阶满星
    if nextData == nil then
        isFull = true
    end
    for k,v in pairs(data) do
        local delta = 0
        if not isFull then
            delta = nextData[k] - v
        end
        result[k] = delta
    end
    return result
end

-----------------------------------------守护配置--------------------------------------------------
local GuardianBaseCfg = Class.GuardianBaseCfg(ClassTypes.XObject)
local GuardianAttriCfg = Class.GuardianAttriCfg(ClassTypes.XObject)
local GuardianResolveCfg = Class.GuardianResolveCfg(ClassTypes.XObject)

local guardianBaseCache = {}
function PartnerDataHelper.ClearGrardianCache()
    guardianBaseCache = {}
end

local guardianAttriStructCache
function PartnerDataHelper.ClearGrardianAttriCache()
    guardianAttriStructCache = nil
end

local guardianResolveCache
function PartnerDataHelper.ClearGrardianResolveCache()
    guardianResolveCache = nil
end

--守护基础配置
function GuardianBaseCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.model = cfg.__model
    self.scale = cfg.__scale
    self.active_skill = DataParseHelper.ParseMSuffixSort(cfg.__active_skill)
    self.passive_skill = DataParseHelper.ParseMSuffixSort(cfg.__passive_skill)
    self.unlock = DataParseHelper.ParseMSuffix(cfg.__unlock)
    self.pos = DataParseHelper.ParseListInt(cfg.__pos)
    self.rotation = DataParseHelper.ParseListInt(cfg.__rotation)
end

--守护属性
function GuardianAttriCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.guardian = cfg.__guardian
    self.level = cfg.__level
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.attri = DataParseHelper.ParseMSuffixSort(cfg.__attri)
end

--守护激活材料分解
function GuardianResolveCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.item_id = cfg.__item_id
    self.resolve = cfg.__resolve
end

function PartnerDataHelper.GetGuardianBaseCfg(id)
    ClearDataManager:UseCache(CacheNames.guardian_base, PartnerDataHelper.ClearGrardianCache)
    local data = guardianBaseCache[id]
    if data == nil then
        local cfg = XMLManager.guardian_base[id]
        if cfg ~= nil then
            data = GuardianBaseCfg(cfg)
            if data.id > 0 then
                guardianBaseCache[data.id] = data
            end
        end
    end
    return data
end

function PartnerDataHelper:GetGuardianBaseList()
    if guardianBaseCache[1] == nil then
        local keys = XMLManager.guardian_base:Keys()
        for i=1,#keys do
            local data = PartnerDataHelper.GetGuardianBaseCfg(keys[i])
        end
    end
    return guardianBaseCache
end

function PartnerDataHelper:GetGuardianAttriCfg(guardianId,level)
    ClearDataManager:UseCache(CacheNames.guardian_attri, PartnerDataHelper.ClearGrardianAttriCache)
    if guardianAttriStructCache == nil then
        guardianAttriStructCache = {}
        local keys = XMLManager.guardian_attri:Keys()
        for i=1,#keys do
            local cfg = XMLManager.guardian_attri[keys[i]]
            local data = GuardianAttriCfg(cfg)
            if guardianAttriStructCache[data.guardian] == nil then
                guardianAttriStructCache[data.guardian] = {}
            end
            guardianAttriStructCache[data.guardian][data.level] = data
        end
    end
    return guardianAttriStructCache[guardianId][level]
end

function PartnerDataHelper.GetGuardianResolveCfg(itemId)
    local t = PartnerDataHelper:GetGuardianResolveList()
    return t[itemId]
end

function PartnerDataHelper:GetGuardianResolveList()
    ClearDataManager:UseCache(CacheNames.guardian_resolve, PartnerDataHelper.ClearGrardianResolveCache)
    if guardianResolveCache == nil then
        guardianResolveCache = {}
        local keys = XMLManager.guardian_resolve:Keys()
        for i=1,#keys do
            local cfg = XMLManager.guardian_resolve[keys[i]]
            local data = GuardianResolveCfg(cfg)
            guardianResolveCache[data.item_id] = data
        end
    end
    return guardianResolveCache
end

return PartnerDataHelper
