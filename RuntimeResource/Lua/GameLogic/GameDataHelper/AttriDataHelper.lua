-- AttriDataHelper.lua
--属性命名配置
local AttriDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local attri_config = GameWorld.attri_config
local ClearDataManager = PlayerManager.ClearDataManager

local Attri = Class.Attri(ClassTypes.XObject)

local attriCache = {}
function AttriDataHelper.ClearCache()
    attriCache = {}
end
local attrItemCache = {}

function Attri:__ctor__(cfg)
	self.id = cfg.__id
	self.ename = cfg.__name		--这个是属性英文标志
	self.name = LanguageDataHelper.GetContent(cfg.__property_desc) --这个才是属性中文字符
	self.atrri_color = cfg.__atrri_color
	self.property_order = cfg.__property_order
	self.property_det_desc = DataParseHelper.ParseMSuffix(cfg.__property_det_desc)
	self.value_type = cfg.__value_type
	self.spirit_sea_param_type = cfg.__spirit_sea_param_type
	self.spirit_sea_param = DataParseHelper.ParseMSuffix(cfg.__spirit_sea_param)
end

function AttriDataHelper.GetAttri(id)
    ClearDataManager:UseCache(CacheNames.attri_config, AttriDataHelper.ClearCache)
	local data = attriCache[id]
	if data == nil then
		local cfg = XMLManager.attri_config[id]
		if cfg ~= nil then
			data = Attri(cfg)
			attriCache[data.id] = data
		end
	end
	return data
end

function AttriDataHelper:GetName(id)
	local data = self.GetAttri(id)
	return data and data.name or nil
end

function AttriDataHelper:GetPropertyOrder(id)
	local data = self.GetAttri(id)
	return data and data.property_order or 0
end

function AttriDataHelper:GetDesc(id,vocation)
	vocation = vocation or 0
	local result = nil
	local data = self.GetAttri(id)
	if data ~= nil then
		local value = data.property_det_desc[0]
		if value ~= nil then
			result = LanguageDataHelper.GetContent(value)
		else
			value = data.property_det_desc[vocation]
			result = LanguageDataHelper.GetContent(value)
		end
	end
	return result
end

function AttriDataHelper:GetShowType(id)
	local data = self.GetAttri(id)
	return data and data.value_type
end

function AttriDataHelper:GetAttriColor(id)
	local data = self.GetAttri(id)
	return data and data.atrri_color
end

--1直接显示
--2显示百分比
function AttriDataHelper:ConvertAttriValue(key,value)
	local type = AttriDataHelper:GetShowType(key)
    type = type or 1
    if type == 2 then
        local result
        if value >= 100 then
        	local len = string.len(tostring(value))
	        local n1 = string.sub(value,1,len-2)
	        local n2 = string.sub(value,len-1,len)
	        result = n1.."."..n2 .. "%"
        else
        	result = tostring(value/100).."%"
        end
        return result
    else
        return value
    end
end

function AttriDataHelper:GetAttrItems()
	if #attrItemCache > 0 then
		return attrItemCache
	end
	local data,index,value
	local orderTable = {}
	local showCache = {}
	local keys = XMLManager.attri_config:Keys()
	for i=1,#keys do
        index = keys[i]
		data = self.GetAttri(index)
		value = data.property_order
		if value ~= 0 then
			table.insert(orderTable,value)
			showCache[value] = index
		end
    end
	table.sort(orderTable)
	for i=1,#orderTable do
		local id = showCache[orderTable[i]]
		table.insert(attrItemCache,id)
	end
	return attrItemCache
end

local PBHeadNameMap = nil

function AttriDataHelper.InitPBHeadNameMap()
	for id = attri_config.ATTRI_ID_HP_BASE, attri_config.MAX_ATTRI2_ID do
		local data = AttriDataHelper.GetAttri(id)
		if data ~= nil then
			PBHeadNameMap[id] = {data.ename, "__" .. data.ename}
		end
	end
end

function AttriDataHelper.GetPBHeadNameMap()
	if PBHeadNameMap == nil then
		PBHeadNameMap = {}
		AttriDataHelper.InitPBHeadNameMap()
	end
	return PBHeadNameMap
end

return AttriDataHelper