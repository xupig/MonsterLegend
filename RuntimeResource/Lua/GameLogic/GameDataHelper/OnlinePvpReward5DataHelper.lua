-- OnlinePvpReward5DataHelper.lua
local OnlinePvpReward5DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvpReward5 = Class.OnlinePvpReward5(ClassTypes.XObject)

local onlinePvpReward5Cache = {}
function OnlinePvpReward5DataHelper.ClearCache()
    onlinePvpReward5Cache = {}
end

function OnlinePvpReward5:__ctor__(cfg)
    self.id = cfg.__id
    self.pvp_id = cfg.__pvp_id
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function OnlinePvpReward5DataHelper.GetOnlinePvpReward5(id)
    ClearDataManager:UseCache(CacheNames.online_pvp_reward_5, OnlinePvpReward5DataHelper.ClearCache)
    local data = onlinePvpReward5Cache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp_reward_5[id]
        if cfg ~= nil then
            data = OnlinePvpReward5(cfg)
            onlinePvpReward5Cache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvpReward5 id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpReward5DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp_reward_5:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpReward5DataHelper:GetPvpId(id)
    local data = self.GetOnlinePvpReward5(id)
    return data and data.pvp_id or nil
end

function OnlinePvpReward5DataHelper:GetReward(id)
    local data = self.GetOnlinePvpReward5(id)
    return data and data.reward or nil
end

return OnlinePvpReward5DataHelper
