-- SystemInfoDataHelper.lua
local SystemInfoDataHelper = {}

local XMLManager = GameManager.XMLManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ConvertStyle = GameMain.LuaFacade.ConvertStyle
local SystemInfoItem = Class.SystemInfoItem(ClassTypes.XObject)
local DataParseHelper = GameDataHelper.DataParseHelper

function SystemInfoItem:__ctor__(cfg)
    self.id = cfg.__id
    self.descript = ConvertStyle(cfg.__descript)
    self.type = DataParseHelper.ParseListInt(cfg.__type)
end

local _cache = {}

function SystemInfoDataHelper.GetSystemInfoData(id)
    local data = _cache[id]
    if data ~= nil then
        return data
    end
    data = XMLManager.system_info[id]
    if data ~= nil then
        _cache[id] = SystemInfoItem(data)
    else
        LoggerHelper.Error("SystemInfo id Error by :" .. tostring(id))
    end
    return _cache[id]
end

function SystemInfoDataHelper.GetArgsTable()
    return LanguageDataHelper.GetArgsTable()
end

function SystemInfoDataHelper.CreateContent(id, argsTable)
    local data = SystemInfoDataHelper.GetSystemInfoData(id)
    return LanguageDataHelper.CreateContentByString(data and data.descript or "", argsTable)
end

function SystemInfoDataHelper.GetType(id)
    local data = SystemInfoDataHelper.GetSystemInfoData(id)
    return data and data.type or {}
end

return SystemInfoDataHelper
