-- EquipDataHelper.lua
local EquipDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local EquipStrengthData = Class.EquipStrengthData(ClassTypes.XObject)
local EquipSuitData = Class.EquipSuitData(ClassTypes.XObject)
local EquipValueData = Class.EquipValueData(ClassTypes.XObject)
local EquipWashData = Class.EquipWashData(ClassTypes.XObject)
local EquipGemRefineData = Class.EquipGemRefineData(ClassTypes.XObject)
local EquipScoreData = Class.EquipScoreData(ClassTypes.XObject)
local EquipWashScoreData = Class.EquipWashScoreData(ClassTypes.XObject)
local EquipAdvanceData = Class.EquipAdvanceData(ClassTypes.XObject)

--强化配置
function EquipStrengthData:__ctor__(cfg)
	self.id = cfg.__id
	self.cost_gold = cfg.__cost_gold
	self.next_exp = cfg.__next_exp
	self.add_exp = cfg.__add_exp
	self.crit = cfg.__crit
	self.attris = DataParseHelper.ParseNSuffix(cfg.__attris)
end

--洗炼配置
function EquipWashData:__ctor__(cfg)
	self.id = cfg.__id
	self.open_level = cfg.__open_level
	self.attri = DataParseHelper.ParseNSuffix(cfg.__attri)
end

--宝石精炼配置
function EquipGemRefineData:__ctor__(cfg)
	self.id = cfg.__id
	self.next_exp = cfg.__next_exp
	self.attri_add = cfg.__attri_add
end

--套装配置
function EquipSuitData:__ctor__(cfg)
	self.id = cfg.__id
	self.name = cfg.__name
	self.type = cfg.__type
	self.sub_type = cfg.__sub_type_i
	self.attri = DataParseHelper.ParseNSuffix(cfg.__attri)
end

--装备属性配置
function EquipValueData:__ctor__(cfg)
	self.id = cfg.__id
	self.basic_attri_value = DataParseHelper.ParseMSuffixSort(cfg.__basic_attri_value)

	self.random_attri1 = DataParseHelper.ParseNSuffixSort(cfg.__random_attri1)
	self.random_attri2 = DataParseHelper.ParseNSuffixSort(cfg.__random_attri2)
	self.random_attri3 = DataParseHelper.ParseNSuffixSort(cfg.__random_attri3)
	self.random_attri4 = DataParseHelper.ParseNSuffixSort(cfg.__random_attri4)
	self.random_attri5 = DataParseHelper.ParseNSuffixSort(cfg.__random_attri5)
end

--极品属性评分配置
function EquipScoreData:__ctor__(cfg)
	self.id = cfg.__id
	self.random_attr = cfg.__random_attr
	self.quality = cfg.__quality
	self.score = cfg.__score
end

--洗炼评分配置
function EquipWashScoreData:__ctor__(cfg)
	self.id = cfg.__id
	self.attri = cfg.__attri
	self.unit = cfg.__unit
	self.score = cfg.__score
end

--进阶配置
function EquipAdvanceData:__ctor__(cfg)
	self.id = cfg.__id
	self.equip_id = cfg.__equip_id
	self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
	self.advanced_equip = cfg.__advanced_equip
end

local strengthCache = {}
function EquipDataHelper.ClearStrengthCache()
    strengthCache = {}
end

local washCache = {}
function EquipDataHelper.ClearWashCache()
    washCache = {}
end

local suitCache = {}
function EquipDataHelper.ClearSuitCache()
    suitCache = {}
end

local equipvalueCache = {}
function EquipDataHelper.ClearEquipvalueCache()
    equipvalueCache = {}
end

local gemRefineCache = {}
function EquipDataHelper.ClearGenRefineCache()
    gemRefineCache = {}
end

local equipScoreCache
function EquipDataHelper.ClearScoreCache()
    equipScoreCache = nil
end

local equipWashScoreCache
function EquipDataHelper.ClearWashScoreCache()
    equipWashScoreCache = nil
end

local equipAdvanceCache
function EquipDataHelper.ClearCache()
    equipAdvanceCache = nil
end

function EquipDataHelper.GetEquipAdvanceData(equipId)
    ClearDataManager:UseCache(CacheNames.equip_advance, EquipDataHelper.ClearCache)
	if equipAdvanceCache == nil then
		equipAdvanceCache = {}
		local keys = XMLManager.equip_advance:Keys()
		for i=1,#keys do
			local data = XMLManager.equip_advance[keys[i]]
			if data then
				data = EquipAdvanceData(data)
				equipAdvanceCache[data.equip_id] = data
			end
		end
	end
	return equipAdvanceCache[equipId]
end

function EquipDataHelper.GetEquipScoreData(attri_id,quality)
    ClearDataManager:UseCache(CacheNames.equip_score, EquipDataHelper.ClearScoreCache)
	if equipScoreCache == nil then
		equipScoreCache = {}
		local keys = XMLManager.equip_score:Keys()
		for i=1,#keys do
			local data = XMLManager.equip_score[keys[i]]
			if data then
				data = EquipScoreData(data) 
				if equipScoreCache[data.random_attr] == nil then
					equipScoreCache[data.random_attr] = {}
				end
				equipScoreCache[data.random_attr][data.quality] = data
			end
		end
	end

	if equipScoreCache[attri_id] == nil or equipScoreCache[attri_id][quality] == nil then
		LoggerHelper.Error("equip_score表attri_id："..attri_id.."没有配品质")
	end
	return equipScoreCache[attri_id][quality]
end

function EquipDataHelper.GetEquipWashScoreData(attri_id)
    ClearDataManager:UseCache(CacheNames.equip_washscore, EquipDataHelper.ClearWashScoreCache)
	if equipWashScoreCache == nil then
		equipWashScoreCache = {}
		local keys = XMLManager.equip_washscore:Keys()
		for i=1,#keys do
			local data = XMLManager.equip_washscore[keys[i]]
			if data then
				data = EquipWashScoreData(data)
				equipWashScoreCache[data.attri] = data
			end
		end
	end
	return equipWashScoreCache[attri_id]
end

function EquipDataHelper.GetStrengthData(id)
    ClearDataManager:UseCache(CacheNames.equip_strength, EquipDataHelper.ClearStrengthCache)
	local data = strengthCache[id]
	if data == nil then
		data = XMLManager.equip_strength[id]
		if data ~= nil then data = EquipStrengthData(data) end
	end
	if data ~= nil then
		-- for k,v in pairs(data) do
		-- 	LoggerHelper.Error(k.."       ")
		-- end
		strengthCache[id] = data
	end
	return data
end

function EquipDataHelper.GetWashData(id)
    ClearDataManager:UseCache(CacheNames.equip_wash, EquipDataHelper.ClearWashCache)
	local data = washCache[id]
	if data == nil then
		data = XMLManager.equip_wash[id]
		if data ~= nil then data = EquipWashData(data) end
	end

	if data ~= nil then
		washCache[id] = data
	end
	return data
end

function EquipDataHelper:GetWashOpenLevel(id)
	local data = EquipDataHelper.GetWashData(id)
	return data and data.open_level
end

function EquipDataHelper.GetGemRefineData(id)
    ClearDataManager:UseCache(CacheNames.equip_gem_refine, EquipDataHelper.ClearGenRefineCache)
	local data = gemRefineCache[id]
	if data == nil then
		data = XMLManager.equip_gem_refine[id]
		if data ~= nil then data = EquipGemRefineData(data) end
	end

	if data ~= nil then
		gemRefineCache[id] = data
	end
	return data
end

--装备套装数据
function EquipDataHelper.GetSuitData(id)
    ClearDataManager:UseCache(CacheNames.equip_suit, EquipDataHelper.ClearSuitCache)
	local data = suitCache[id]
	if data == nil then
		data = XMLManager.equip_suit[id]
		if data ~= nil then data = EquipSuitData(data) end
	end
	if data ~= nil then
		suitCache[id] = data
	end
	return data
end

--装备属性数据
function EquipDataHelper.GetEquipValueData(id)
    ClearDataManager:UseCache(CacheNames.equip_value, EquipDataHelper.ClearEquipvalueCache)
	local data = equipvalueCache[id]
	if data == nil then
		data = XMLManager.equip_value[id]
		if data ~= nil then data = EquipValueData(data) end
	end
	if data ~= nil then
		equipvalueCache[id] = data
	end
	return data
end

function EquipDataHelper:GetBaseAttris(equipId)
	local result = {}
	local baseAttri = EquipDataHelper.GetEquipValueData(equipId).basic_attri_value
	for i=1,#baseAttri do
 		table.insert(result,{["attri"] = baseAttri[i][1], ["value"] = baseAttri[i][2]})
	end
	return result
end

function EquipDataHelper:GetRecommandRandAttris(equipId)
	local result = {}
	local randomAttri = {}
	randomAttri[3] = EquipDataHelper.GetEquipValueData(equipId).random_attri1
	randomAttri[4] = EquipDataHelper.GetEquipValueData(equipId).random_attri2
	randomAttri[5] = EquipDataHelper.GetEquipValueData(equipId).random_attri3
	randomAttri[1] = EquipDataHelper.GetEquipValueData(equipId).random_attri4
	randomAttri[2] = EquipDataHelper.GetEquipValueData(equipId).random_attri5

	for i=1,5 do
		if randomAttri[i] then
			local showRecommand = #randomAttri[i] > 1
			table.insert(result,{["attri"] = randomAttri[i][1][1], ["value"] = randomAttri[i][1][2][1],["showRecommand"] = showRecommand})
		end
	end
	return result
end

return EquipDataHelper
