-- TeamDataHelper.lua
local TeamDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local teamTargetCache = {}
function TeamDataHelper.ClearCache()
    teamTargetCache = {}
end

local teamTargetCacheByType
local teamTargetCacheByMap
local TeamTarget = Class.TeamTarget(ClassTypes.XObject)
function TeamTarget:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.sub_type = cfg.__sub_type
    self.map_id = cfg.__map_id
    self.count = cfg.__count
    self.icon = cfg.__icon
    self.level = cfg.__level
end

function TeamDataHelper.GetTeamTarget(id)
    ClearDataManager:UseCache(CacheNames.team_target, TeamDataHelper.ClearCache)
	local data = teamTargetCache[id]
    if data == nil then
        local cfg = XMLManager.team_target[id]
        if cfg ~= nil then
            data = TeamTarget(cfg)
            teamTargetCache[data.id] = data
        end
    end
    return data
end

function TeamDataHelper.GetTeamTargetMap()
	if teamTargetCacheByType == nil then
		TeamDataHelper.ConstructTargetMap()
	end
	return teamTargetCacheByType
end

--结构化targetid数据
function TeamDataHelper.ConstructTargetMap()
    teamTargetCacheByType = {}
    teamTargetCacheByMap = {}
    local keys = XMLManager.team_target:Keys()
    for i=1,#keys do
        local data = TeamDataHelper.GetTeamTarget(keys[i])
        if teamTargetCacheByType[data.type] == nil then
            teamTargetCacheByType[data.type] = {}
        end
        table.insert(teamTargetCacheByType[data.type],data)
        teamTargetCacheByMap[data.map_id] = data
    end
end

function TeamDataHelper.GetTeamTargetSubTypeList(type)
	return teamTargetCacheByType[type]
end

-- function TeamDataHelper.GetTeamTargetByType(type,subType)
-- 	return teamTargetCacheByType[type][subType].id
-- end

function TeamDataHelper.GetTeamTargetByMap(mapId)
    if teamTargetCacheByMap == nil then
        TeamDataHelper.ConstructTargetMap()
    end
    return teamTargetCacheByMap[mapId]
end

function TeamDataHelper.GetType(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.type or nil
end

function TeamDataHelper.GetSubType(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.sub_type or nil
end

function TeamDataHelper.GetMapId(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.map_id or nil
end

function TeamDataHelper.GetMemberCount(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.count or nil
end

function TeamDataHelper.GetIcon(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.icon or nil
end

function TeamDataHelper.GetLevel(id)
    local data = TeamDataHelper.GetTeamTarget(id)
    return data and data.level or nil
end

function TeamDataHelper.GetNameByMapId(mapId)
    local data = XMLManager.team_target:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and TeamDataHelper.GetMapId(v) == mapId then
            return LanguageDataHelper.CreateContent(TeamDataHelper.GetSubType(v))
        end
    end
    return ""
end

return TeamDataHelper
