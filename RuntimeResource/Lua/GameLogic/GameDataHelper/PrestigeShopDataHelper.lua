-- PrestigeShopDataHelper.lua
local PrestigeShopDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local PrestigeShop = Class.PrestigeShop(ClassTypes.XObject)

local prestigeShopCache = {}
function PrestigeShopDataHelper.ClearCache()
    prestigeShopCache = {}
end

function PrestigeShop:__ctor__(cfg)
    self.id = cfg.__id
    self.prestige_type = cfg.__prestige_type
    self.item_id = cfg.__item_id
    self.price = DataParseHelper.ParseMSuffix(cfg.__price)
    self.prestige_level = cfg.__prestige_level
end

function PrestigeShopDataHelper.GetPrestigeShop(id)
    ClearDataManager:UseCache(CacheNames.prestige_shop, PrestigeShopDataHelper.ClearCache)
    local data = prestigeShopCache[id]
    if data == nil then
        local cfg = XMLManager.prestige_shop[id]
        if cfg ~= nil then
            data = PrestigeShop(cfg)
            prestigeShopCache[data.id] = data
        else
            LoggerHelper.Error("PrestigeShop id not exist:" .. id)
        end
    end
    return data
end

function PrestigeShopDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.prestige_shop:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

--根据阵营获取声望商店物品数据
function PrestigeShopDataHelper:GetShopDataByPrestige(typeId)
    local ids = {}
    local data = XMLManager.prestige_shop:Keys()
    local datas = {}
    for i, v in ipairs(data) do
        if typeId == self:GetPrestigeType(v) then
            local shopData = {}
            shopData.id = v
            shopData.itemId = self:GetItemId(v)
            for k,v in pairs(self:GetPrice(v)) do
                shopData.coinType = k
                shopData.cost = v
                break
            end
            shopData.level = self:GetPrestigeLevel(v)
            table.insert(datas, shopData)
        end
    end
    return datas
end

function PrestigeShopDataHelper:GetPrestigeType(id)
    local data = self.GetPrestigeShop(id)
    return data and data.prestige_type or nil
end

function PrestigeShopDataHelper:GetItemId(id)
    local data = self.GetPrestigeShop(id)
    return data and data.item_id or nil
end

function PrestigeShopDataHelper:GetPrice(id)
    local data = self.GetPrestigeShop(id)
    return data and data.price or nil
end

function PrestigeShopDataHelper:GetPrestigeLevel(id)
    local data = self.GetPrestigeShop(id)
    return data and data.prestige_level or nil
end

return PrestigeShopDataHelper
