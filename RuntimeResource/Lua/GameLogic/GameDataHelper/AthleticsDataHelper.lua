-- AthleticsDataHelper.lua
local AthleticsDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Athletics = Class.Athletics(ClassTypes.XObject)

local athleticsCache = {}
function AthleticsDataHelper.ClearCache()
    athleticsCache = {}
end

function Athletics:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.time = cfg.__time
    self.tag = cfg.__tag
    self.level = cfg.__level
    self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
end

function AthleticsDataHelper.GetAthletics(id)
    ClearDataManager:UseCache(CacheNames.athletics, AthleticsDataHelper.ClearCache)
    local data = athleticsCache[id]
    if data == nil then
        local cfg = XMLManager.athletics[id]
        if cfg ~= nil then
            data = Athletics(cfg)
            athleticsCache[data.id] = data
        else
            LoggerHelper.Error("Athletics id not exist:" .. id)
        end
    end
    return data
end

function AthleticsDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.athletics:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function AthleticsDataHelper:GetName(id)
    local data = self.GetAthletics(id)
    return LanguageDataHelper.CreateContent(data.name)
end

function AthleticsDataHelper:GetIcon(id)
    local data = self.GetAthletics(id)
    return data and data.icon or nil
end

function AthleticsDataHelper:GetDesc(id)
    local data = self.GetAthletics(id)
    return LanguageDataHelper.CreateContent(data.desc)
end

function AthleticsDataHelper:GetTime(id)
    local data = self.GetAthletics(id)
    return data and data.time or nil
end

function AthleticsDataHelper:GetTag(id)
    local data = self.GetAthletics(id)
    return data and data.tag or nil
end

function AthleticsDataHelper:GetLevel(id)
    local data = self.GetAthletics(id)
    return data and data.level or nil
end

function AthleticsDataHelper:GetFollow(id)
    local data = self.GetAthletics(id)
    return data and data.follow or nil
end

return AthleticsDataHelper
