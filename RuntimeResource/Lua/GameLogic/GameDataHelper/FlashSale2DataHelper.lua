-- FlashSale2DataHelper.lua
local FlashSale2DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local FlashSale2 = Class.FlashSale2(ClassTypes.XObject)

local flashSale2Cache = {}

function FlashSale2:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.item = DataParseHelper.ParseMSuffix(cfg.__item)
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.discount = cfg.__discount
    self.time = cfg.__time
end

function FlashSale2DataHelper.GetFlashSale2(id)
    local data = flashSale2Cache[id]
    if data == nil then
        local cfg = XMLManager.flash_sale2[id]
        if cfg ~= nil then
            data = FlashSale2(cfg)
            flashSale2Cache[data.id] = data
        else
            LoggerHelper.Error("FlashSale2 id not exist:" .. id)
        end
    end
    return data
end

function FlashSale2DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.flash_sale2:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function FlashSale2DataHelper:GetLevel(id)
    local data = self.GetFlashSale2(id)
    return data and data.level or nil
end

function FlashSale2DataHelper:GetItem(id)
    local data = self.GetFlashSale2(id)
    return data and data.item or nil
end

function FlashSale2DataHelper:GetCost(id)
    local data = self.GetFlashSale2(id)
    return data and data.cost or nil
end

function FlashSale2DataHelper:GetDiscount(id)
    local data = self.GetFlashSale2(id)
    return data and data.discount or nil
end

function FlashSale2DataHelper:GetTime(id)
    local data = self.GetFlashSale2(id)
    return data and data.time or nil
end

return FlashSale2DataHelper
