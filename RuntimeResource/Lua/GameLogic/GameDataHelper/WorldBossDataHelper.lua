-- WorldBossDataHelper.lua
local WorldBossDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WorldBoss = Class.WorldBoss(ClassTypes.XObject)

local worldBossCache = {}
function WorldBossDataHelper.ClearCache()
    worldBossCache = {}
end

function WorldBoss:__ctor__(cfg)
    self.id = cfg.__id
    self.sence_id = cfg.__sence_id
    self.level_limit = cfg.__level_limit
    self.boss_level = cfg.__boss_level
    self.boss_name = cfg.__boss_name
    self.boss_name_icon = cfg.__boss_name_icon
    self.boss_icon = cfg.__boss_icon
    self.boss_monster_id = cfg.__boss_monster_id
    self.boss_drops = DataParseHelper.ParseNSuffix(cfg.__boss_drops)
    self.boss_region_id = cfg.__boss_region_id
    self.level_show = cfg.__level_show
    self.peace_show = cfg.__peace_show
end

function WorldBossDataHelper.GetWorldBoss(id)
    ClearDataManager:UseCache(CacheNames.world_boss, WorldBossDataHelper.ClearCache)
    local data = worldBossCache[id]
    if data == nil then
        local cfg = XMLManager.world_boss[id]
        if cfg ~= nil then
            data = WorldBoss(cfg)
            worldBossCache[data.id] = data
        else
            LoggerHelper.Error("WorldBoss id not exist:" .. id)
        end
    end
    return data
end

function WorldBossDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.world_boss:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WorldBossDataHelper:GetMonsterByMapId(mapId)
    local data = XMLManager.world_boss:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetSenceId(v) == mapId then
            return v
        end
    end
    return nil
end

function WorldBossDataHelper:GetSenceId(id)
    local data = self.GetWorldBoss(id)
    return data and data.sence_id or nil
end

function WorldBossDataHelper:GetLevelLimit(id)
    local data = self.GetWorldBoss(id)
    return data and data.level_limit or nil
end

function WorldBossDataHelper:GetBossLevel(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_level or nil
end

function WorldBossDataHelper:GetBossName(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_name or nil
end

function WorldBossDataHelper:GetBossNameIcon(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_name_icon or nil
end

function WorldBossDataHelper:GetBossIcon(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_icon or nil
end

function WorldBossDataHelper:GetBossMonsterId(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_monster_id or nil
end

function WorldBossDataHelper:GetBossDrops(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_drops or nil
end

function WorldBossDataHelper:GetBossRegionId(id)
    local data = self.GetWorldBoss(id)
    return data and data.boss_region_id or nil
end

function WorldBossDataHelper:GetLevelShow(id)
    local data = self.GetWorldBoss(id)
    return data and data.level_show or 0
end

function WorldBossDataHelper:GetPeaceShow(id)
    local data = self.GetWorldBoss(id)
    return data and data.peace_show or 0
end

return WorldBossDataHelper
