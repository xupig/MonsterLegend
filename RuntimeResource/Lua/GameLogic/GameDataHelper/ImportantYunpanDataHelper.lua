-- ImportantYunpanDataHelper.lua
local ImportantYunpanDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ImportantYunpan = Class.ImportantYunpan(ClassTypes.XObject)

local importantYunpanCache = {}
function ImportantYunpanDataHelper.ClearCache()
    importantYunpanCache = {}
end

function ImportantYunpan:__ctor__(cfg)
    self.id = cfg.__id
    self.num = cfg.__num
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function ImportantYunpanDataHelper.GetImportantYunpan(id)
    ClearDataManager:UseCache(CacheNames.important_yunpan, ImportantYunpanDataHelper.ClearCache)
    local data = importantYunpanCache[id]
    if data == nil then
        local cfg = XMLManager.important_yunpan[id]
        if cfg ~= nil then
            data = ImportantYunpan(cfg)
            importantYunpanCache[data.id] = data
        else
            LoggerHelper.Error("ImportantYunpan id not exist:" .. id)
        end
    end
    return data
end

function ImportantYunpanDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.important_yunpan:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ImportantYunpanDataHelper:GetNum(id)
    local data = self.GetImportantYunpan(id)
    return data and data.num or nil
end

function ImportantYunpanDataHelper:GetDay(id)
    local data = self.GetImportantYunpan(id)
    return data and data.day or nil
end

function ImportantYunpanDataHelper:GetReward(id)
    local data = self.GetImportantYunpan(id)
    return data and data.reward or nil
end

return ImportantYunpanDataHelper
