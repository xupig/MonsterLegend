-- PointRewardDataHelper.lua
local PointRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local PointReward = Class.PointReward(ClassTypes.XObject)

local pointRewardCache = {}
function PointRewardDataHelper.ClearCache()
    pointRewardCache = {}
end

function PointReward:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.point = cfg.__point
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.activitys = DataParseHelper.ParseMSuffix(cfg.__activitys)
    self.icon = cfg.__icon
end

function PointRewardDataHelper.GetPointReward(id)
    ClearDataManager:UseCache(CacheNames.point_reward, PointRewardDataHelper.ClearCache)
    local data = pointRewardCache[id]
    if data == nil then
        local cfg = XMLManager.point_reward[id]
        if cfg ~= nil then
            data = PointReward(cfg)
            pointRewardCache[data.id] = data
        else
            LoggerHelper.Error("PointReward id not exist:" .. id)
        end
    end
    return data
end

function PointRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.point_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PointRewardDataHelper:GetType(id)
    local data = self.GetPointReward(id)
    return data and data.type or nil
end

function PointRewardDataHelper:GetPoint(id)
    local data = self.GetPointReward(id)
    return data and data.point or nil
end

function PointRewardDataHelper:GetReward(id)
    local data = self.GetPointReward(id)
    return data and data.reward or nil
end

function PointRewardDataHelper:GetActivitys(id)
    local data = self.GetPointReward(id)
    return data and data.activitys or nil
end

function PointRewardDataHelper:GetIcon(id)
    local data = self.GetPointReward(id)
    return data and data.icon or nil
end

return PointRewardDataHelper
