-- VIPDataHelper.lua
--VIP配置
local VIPDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local VipConfig = GameConfig.VipConfig
local ClearDataManager = PlayerManager.ClearDataManager

local VIPCfg = Class.VIPCfg(ClassTypes.XObject)
local vipCache = {}
function VIPDataHelper.ClearCache()
    vipCache = {}
end


function VIPCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.next_exp = cfg.__next_exp
	self.privilege = DataParseHelper.ParseListInt(cfg.__privilege)
	self.level_reward = DataParseHelper.ParseNSuffix(cfg.__level_reward)
    --self.weekly_reward = DataParseHelper.ParseNSuffix(cfg.__weekly_reward)
    self.daily_reward = DataParseHelper.ParseNSuffix(cfg.__daily_reward)
    self.privilege = DataParseHelper.ParseListInt(cfg.__privilege)
    self.pray_gold_maxtime_add = cfg.__pray_gold_maxtime_add
    self.pray_goldcrit = DataParseHelper.ParseMSuffix(cfg.__pray_goldcrit)
    self.exp_instance_benter_add = cfg.__exp_instance_benter_add
    self.protect_holy_tower_benter_add = cfg.__protect_holy_tower_benter_add
    self.gods_treasury_benter_add = cfg.__gods_treasury_benter_add
    self.tower_defense_benter_add = cfg.__tower_defense_benter_add
    self.vip_revenue = cfg.__vip_revenue
end

function VIPDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.vip:Keys()
    for i, v in ipairs(data) do
        -- if v ~= 0 then
		table.insert(ids, v)
        -- end
    end
    return ids
end

function VIPDataHelper.GetVIPCfg(id)
    ClearDataManager:UseCache(CacheNames.vip, VIPDataHelper.ClearCache)
	local data = vipCache[id]
	if data == nil then
		local cfg = XMLManager.vip[id]
		if cfg ~= nil then
			data = VIPCfg(cfg)
			vipCache[data.id] = data
		end
	end
	return data
end

function VIPDataHelper:GetNextExp(id)
	local data = VIPDataHelper.GetVIPCfg(id)
	return data and data.next_exp or nil
end

function VIPDataHelper:GetPrivilege(id)
	local data = VIPDataHelper.GetVIPCfg(id)
	return data and data.privilege or nil
end

function VIPDataHelper:GetLevelReward(id)
	local data = VIPDataHelper.GetVIPCfg(id)
	return data and data.level_reward or nil
end

-- function VIPDataHelper:GetWeekReward(id)
-- 	local data = VIPDataHelper.GetVIPCfg(id)
-- 	return data and data.weekly_reward or nil
-- end

function VIPDataHelper:GetDailyReward(id)
	local data = VIPDataHelper.GetVIPCfg(id)
	return data and data.daily_reward or nil
end

function VIPDataHelper:GetMaxVIPLevel()
	local maxLevel = 0
	local keys = XMLManager.vip:Keys()
	for i=1,#keys do
		local vipCfg = VIPDataHelper.GetVIPCfg(keys[i])
		if vipCfg.id > maxLevel then
			maxLevel = vipCfg.id
		end
	end
	return math.min(9,maxLevel)
end

function VIPDataHelper:GetPrivilege(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.privilege or nil
end

function VIPDataHelper:GetPrayGoldMaxtimeAdd(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.pray_gold_maxtime_add or nil
end

function VIPDataHelper:GetPrayGoldcrit(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.pray_goldcrit or nil
end

--经验副本购买次数
function VIPDataHelper:GetExpInstanceBuyTimes(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.exp_instance_benter_add or nil
end

--守护结界塔副本购买次数
function VIPDataHelper:GetPetInstanceBuyTimes(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.protect_holy_tower_benter_add or nil
end

--众神宝库副本购买次数
function VIPDataHelper:GetMoneyInstanceBuyTimes(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.gods_treasury_benter_add or nil
end

--塔防副本购买次数
function VIPDataHelper:GetTowerInstanceBuyTimes(id)
    local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.tower_defense_benter_add or nil
end

function VIPDataHelper:GetButTimesByType(type)
	local id = GameWorld.Player().vip_level
	return VIPDataHelper[VipConfig.VIP_BUY_TIMES_TYPE_MAP[type]](self, id)
end

function VIPDataHelper:GetNextBuyTimesAndVip(id, type)
	local ids = self:GetAllId()
	if id == (#ids - 1) then return nil, nil end

	local nowTimes = VIPDataHelper[VipConfig.VIP_BUY_TIMES_TYPE_MAP[type]](self, id)
	for i=(id+1),(#ids - 1) do		
		local nextTimes = VIPDataHelper[VipConfig.VIP_BUY_TIMES_TYPE_MAP[type]](self, i)
		if nextTimes > nowTimes then
			return i, nextTimes
		end
	end
	return nil, nil
end

function VIPDataHelper:GetVipRevenue(id)
	local data = VIPDataHelper.GetVIPCfg(id)
    return data and data.vip_revenue or nil
end

return VIPDataHelper