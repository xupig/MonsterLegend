local TransformDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local SkillDataHelper = GameDataHelper.SkillDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = require("ServerConfig/public_config")
local ClearDataManager = PlayerManager.ClearDataManager

local transformData = {}
local TransformTotalCfg = Class.TransformTotalCfg(ClassTypes.XObject)
local TransformLevelCfg = Class.TransformLevelCfg(ClassTypes.XObject)
local TransformPetGradeCfg = Class.TransformPetGradeCfg(ClassTypes.XObject)
local TransformWingsStarCfg = Class.TransformWingsStarCfg(ClassTypes.XObject)

local transformTotalCache = {}
function TransformDataHelper.ClearCache()
    transformTotalCache = {}
end

local transformTotalStructCache
function TransformDataHelper.ClearTransformTotalStructCache()
    transformTotalStructCache = nil
end

local transformLevelStructCache
function TransformDataHelper.ClearTransformLevelStructCache()
    transformLevelStructCache = nil
end

local transformPetGradeStructCache
function TransformDataHelper.ClearTransformPetGradeStructCache()
    transformPetGradeStructCache = nil
end

local transformWingsStarStructCache
function TransformDataHelper.ClearTransformWingsStarStructCache()
    transformWingsStarStructCache = nil
end

--总配置
function TransformTotalCfg:__ctor__(cfg)
    self.id = cfg.__id
    --self.item_id = cfg.__item_id
    self.name = DataParseHelper.ParseMSuffix(cfg.__name)
    self.icon = DataParseHelper.ParseMSuffix(cfg.__icon)
    self.type = cfg.__type
    self.show_type = cfg.__show_type
    self.model_vocation = DataParseHelper.ParseMSuffix(cfg.__model_vocation)
    self.resolve = cfg.__resolve
    self.max_star = cfg.__max_star
    self.activate_num = DataParseHelper.ParseNSuffix(cfg.__activate_num)
    self.activate_limit = DataParseHelper.ParseMSuffix(cfg.__activate_limit)
    self.passive_skill = DataParseHelper.ParseMSuffixSort(cfg.__passive_skill)
    self.sort_num = cfg.__sort_num
    self.unlock_chinese = cfg.__unlock_chinese
end

--幻化系统等级配置
function TransformLevelCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.level = cfg.__level
    self.levelup_cost = cfg.__levelup_cost
    self.attri_percent_up = DataParseHelper.ParseMSuffixSort(cfg.__attri_percent_up)
end

function TransformPetGradeCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.transform_id = cfg.__transform_id
    self.grade = cfg.__grade
    self.bless = cfg.__bless
    self.attri = DataParseHelper.ParseMSuffixSort(cfg.__attri)
end

function TransformWingsStarCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.transform_id = cfg.__transform_id
    self.star = cfg.__star
    self.star_lvup_cost = DataParseHelper.ParseNSuffix(cfg.__star_lvup_cost)
    self.attri = DataParseHelper.ParseMSuffixSort(cfg.__attri)
end

function TransformDataHelper.GetTreasureTotalCfg(id)
    ClearDataManager:UseCache(CacheNames.transform_total, TransformDataHelper.ClearCache)
    local data = transformTotalCache[id]
    if data == nil then
        local cfg = XMLManager.transform_total[id]
        if cfg ~= nil then
            data = TransformTotalCfg(cfg)
            transformTotalCache[data.id] = data
        else
            LoggerHelper.Error("TransformTotalCfg id not exist:" .. id)
        end
    end
    return data
end

function TransformDataHelper:GetTransformTotalCfgList(type)
    TransformDataHelper:SetTransformTotalStructCache()
    return transformTotalStructCache[type]
end

local function SortTransformTotal(a, b)
    return a.sort_num < b.sort_num
end

function TransformDataHelper:SetTransformTotalStructCache()
    ClearDataManager:UseCache(CacheNames.transformTotalStructCache, TransformDataHelper.ClearTransformTotalStructCache)
    if transformTotalStructCache == nil then
        transformTotalStructCache = {}
        local keys = XMLManager.transform_total:Keys()
        for i = 1, #keys do
            local data = TransformDataHelper.GetTreasureTotalCfg(keys[i])
            if transformTotalStructCache[data.type] == nil then
                transformTotalStructCache[data.type] = {}
            end
            table.insert(transformTotalStructCache[data.type], data)
        end

        for type,tab in pairs(transformTotalStructCache) do
            table.sort(tab,SortTransformTotal)
        end
    end
end

function TransformDataHelper:GetTransformLevelUpCfg(type, level)
    TransformDataHelper:SetTransformLevelStructCache()
    return transformLevelStructCache[type][level]
end
--
function TransformDataHelper:SetTransformLevelStructCache()
    ClearDataManager:UseCache(CacheNames.transformLevelStructCache, TransformDataHelper.ClearTransformLevelStructCache)
    if transformLevelStructCache == nil then
        transformLevelStructCache = {}
        local keys = XMLManager.transform_level:Keys()
        for i = 1, #keys do
            local cfg = XMLManager.transform_level[keys[i]]
            local data = TransformLevelCfg(cfg)
            if transformLevelStructCache[data.type] == nil then
                transformLevelStructCache[data.type] = {}
            end
            transformLevelStructCache[data.type][data.level] = data
        end
    end
end

--宠物幻化升阶
function TransformDataHelper:SetTransformPartnerGradeStructCache()
    ClearDataManager:UseCache(CacheNames.transformPetGradeStructCache, TransformDataHelper.ClearTransformPetGradeStructCache)
    if transformPetGradeStructCache == nil then
        transformPetGradeStructCache = {}
        local keys = XMLManager.transform_pet_grade:Keys()
        for i = 1, #keys do
            local cfg = XMLManager.transform_pet_grade[keys[i]]
            local data = TransformPetGradeCfg(cfg)
            if transformPetGradeStructCache[data.type] == nil then
                transformPetGradeStructCache[data.type] = {}
            end
            
            if transformPetGradeStructCache[data.type][data.transform_id] == nil then
                transformPetGradeStructCache[data.type][data.transform_id] = {}
            end
            transformPetGradeStructCache[data.type][data.transform_id][data.grade] = data
        end
    end
end

function TransformDataHelper:GetPartnerGradeCfg(type, transformId, grade)
    TransformDataHelper:SetTransformPartnerGradeStructCache()
    --LoggerHelper.Error(type.."/"..transformId.."/"..grade)
    return transformPetGradeStructCache[type][transformId][grade]
end

--宝物升星配置
function TransformDataHelper:SetTransformWingsStarStructCache()
    ClearDataManager:UseCache(CacheNames.transformWingsStarStructCache, TransformDataHelper.ClearTransformWingsStarStructCache)
    if transformWingsStarStructCache == nil then
        transformWingsStarStructCache = {}
        local keys = XMLManager.transform_wings_star:Keys()
        for i = 1, #keys do
            local cfg = XMLManager.transform_wings_star[keys[i]]
            local data = TransformWingsStarCfg(cfg)
            if transformWingsStarStructCache[data.type] == nil then
                transformWingsStarStructCache[data.type] = {}
            end
            
            if transformWingsStarStructCache[data.type][data.transform_id] == nil then
                transformWingsStarStructCache[data.type][data.transform_id] = {}
            end
            transformWingsStarStructCache[data.type][data.transform_id][data.star] = data
        end
    end
end

function TransformDataHelper:GetTreasureStarCfg(type, transformId, star)
    TransformDataHelper:SetTransformWingsStarStructCache()
    --LoggerHelper.Error(type.."/"..transformId.."/"..star)
    return transformWingsStarStructCache[type][transformId][star]
end

-----------------------------------------------------------------------------------------
local uiModelViewCache = {}
function TransformDataHelper.ClearUIModelViewCache()
    uiModelViewCache = {}
end

local UIModelViewCfg = Class.UIModelViewCfg(ClassTypes.XObject)
function UIModelViewCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.model = cfg.__model
    self.model_ui = cfg.__model_ui
    self.pos = DataParseHelper.ParseListInt(cfg.__pos)
    self.rotation = DataParseHelper.ParseListInt(cfg.__rotation)
    self.scale = cfg.__scale
end

function TransformDataHelper.GetUIModelViewCfg(id)
    ClearDataManager:UseCache(CacheNames.ui_model_view, TransformDataHelper.ClearUIModelViewCache)
    local data = uiModelViewCache[id]
    if data == nil then
        local cfg = XMLManager.ui_model_view[id]
        if cfg ~= nil then
            data = UIModelViewCfg(cfg)
            uiModelViewCache[data.id] = data
        end
    end
    return data
end

---------------------------------------宠物、法宝模型模型配置表--------------------------------------------------
local petAndTreasureCache = {}
function TransformDataHelper.ClearPetAndTreasureCache()
    petAndTreasureCache = {}
end
local PetAndTreasureCfg = Class.PetAndTreasureCfg(ClassTypes.XObject)
function PetAndTreasureCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.skill = cfg.__skill
    self.model = cfg.__model
    self.ai = cfg.__ai
end

function TransformDataHelper:GetPetAndTreasureCfg(id)
    ClearDataManager:UseCache(CacheNames.pet_treasure_skill_ai, TransformDataHelper.ClearPetAndTreasureCache)
    local data = petAndTreasureCache[id]
    if data == nil then
        local cfg = XMLManager.pet_treasure_skill_ai[id]
        if cfg ~= nil then
            data = PetAndTreasureCfg(cfg)
            petAndTreasureCache[data.id] = data
        else
            LoggerHelper.Error("TransformData id not exist:" .. id)
        end
    end
    return data
end

function TransformDataHelper:GetPetAndTreasureModel(id)
    local data = self:GetPetAndTreasureCfg(id)
    return data and data.model or nil
end

function TransformDataHelper:GetPetAndTreasureSkill(id)
    local data = self:GetPetAndTreasureCfg(id)
    return data and data.skill or nil
end

function TransformDataHelper:GetPetAndTreasureAI(id)
    local data = self:GetPetAndTreasureCfg(id)
    return data and data.ai or nil
end

return TransformDataHelper
