-- QuestionRankRewardDataHelper.lua
local QuestionRankRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local QuestionRankReward = Class.QuestionRankReward(ClassTypes.XObject)

local questionRankRewardCache = {}
function QuestionRankRewardDataHelper.ClearCache()
    questionRankRewardCache = {}
end

function QuestionRankReward:__ctor__(cfg)
    self.id = cfg.__id
    self.rank = DataParseHelper.ParseListInt(cfg.__rank)
    self.level = cfg.__level
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.reward_show = DataParseHelper.ParseMSuffixSort(cfg.__reward_show)
end

function QuestionRankRewardDataHelper.GetQuestionRankReward(id)
    ClearDataManager:UseCache(CacheNames.question_rank_reward, QuestionRankRewardDataHelper.ClearCache)
    local data = questionRankRewardCache[id]
    if data == nil then
        local cfg = XMLManager.question_rank_reward[id]
        if cfg ~= nil then
            data = QuestionRankReward(cfg)
            questionRankRewardCache[data.id] = data
        else
            LoggerHelper.Error("QuestionRankReward id not exist:" .. id)
        end
    end
    return data
end

function QuestionRankRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.question_rank_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function QuestionRankRewardDataHelper:GetRank(id)
    local data = self.GetQuestionRankReward(id)
    return data and data.rank or nil
end

function QuestionRankRewardDataHelper:GetLevel(id)
    local data = self.GetQuestionRankReward(id)
    return data and data.level or nil
end

function QuestionRankRewardDataHelper:GetIcon(id)
    local data = self.GetQuestionRankReward(id)
    return data and data.icon or nil
end

function QuestionRankRewardDataHelper:GetDesc(id)
    local data = self.GetQuestionRankReward(id)
    return data and data.desc or nil
end

function QuestionRankRewardDataHelper:GetRewardShow(id)
    local data = self.GetQuestionRankReward(id)
    return data and data.reward_show or nil
end

function QuestionRankRewardDataHelper:GetRewardListByMyLevel()
    local ids = {}
    local level = GameWorld.Player().level
    local data = XMLManager.question_rank_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetLevel(v) == level then
            table.insert(ids, v)
        end
    end
    return ids
end

return QuestionRankRewardDataHelper
