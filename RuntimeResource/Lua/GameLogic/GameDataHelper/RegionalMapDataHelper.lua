-- RegionalMapDataHelper.lua
local RegionalMapDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local RegionalMap = Class.RegionalMap(ClassTypes.XObject)

local regionalMapCache = {}
function RegionalMapDataHelper.ClearCache()
    regionalMapCache = {}
end

function RegionalMap:__ctor__(cfg)
    self.id = cfg.__id
    self.module_id = cfg.__module_id
    self.subtype_button_name = cfg.__subtype_button_name
    self.true_location = DataParseHelper.ParseListInt(cfg.__true_location)
    self.transfer_lv = cfg.__transfer_lv
    self.transfer_trigger_id = DataParseHelper.ParseListInt(cfg.__transfer_trigger_id)
end

function RegionalMapDataHelper.GetRegionalMap(id)
    ClearDataManager:UseCache(CacheNames.regional_map, RegionalMapDataHelper.ClearCache)
    local data = regionalMapCache[id]
    if data == nil then
        local cfg = XMLManager.regional_map[id]
        if cfg ~= nil then
            data = RegionalMap(cfg)
            regionalMapCache[data.id] = data
        else
            LoggerHelper.Error("RegionalMap id not exist:" .. id)
        end
    end
    return data
end

function RegionalMapDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.regional_map:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function RegionalMapDataHelper:GetRegionalMapDataIdsByModuleId(moduleId)
    local ids = {}
    local data = XMLManager.regional_map:Keys()
    for i,v in ipairs(data) do
        if self:GetModuleId(v) == moduleId then
            table.insert(ids, v)
        end
    end
    return ids
end

function RegionalMapDataHelper:GetModuleId(id)
    local data = self.GetRegionalMap(id)
    return data and data.module_id or nil
end

function RegionalMapDataHelper:GetSubtypeButtonName(id)
    local data = self.GetRegionalMap(id)
    return data and data.subtype_button_name or nil
end

function RegionalMapDataHelper:GetTrueLocation(id)
    local data = self.GetRegionalMap(id)
    return data and Vector3(data.true_location[1], data.true_location[2], data.true_location[3]) or nil
end

function RegionalMapDataHelper:GetTransferLv(id)
    local data = self.GetRegionalMap(id)
    return data and data.transfer_lv or nil
end

function RegionalMapDataHelper:GetTransferTriggerId(id)
    local data = self.GetRegionalMap(id)
    return data and data.transfer_trigger_id or nil
end

return RegionalMapDataHelper
