-- CardPieceDataHelper.lua
local CardPieceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CardPiece = Class.CardPiece(ClassTypes.XObject)

local cardPieceCache = {}
function CardPieceDataHelper.ClearCache()
    cardPieceCache = {}
end

function CardPiece:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.quality = cfg.__quality
    self.level = cfg.__level
    self.item_id = cfg.__item_id
end

function CardPieceDataHelper.GetCardPiece(id)
    ClearDataManager:UseCache(CacheNames.card_piece, CardPieceDataHelper.ClearCache)
    local data = cardPieceCache[id]
    if data == nil then
        local cfg = XMLManager.card_piece[id]
        if cfg ~= nil then
            data = CardPiece(cfg)
            cardPieceCache[data.id] = data
        else
            LoggerHelper.Error("CardPiece id not exist:" .. id)
        end
    end
    return data
end

function CardPieceDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.card_piece:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CardPieceDataHelper:GetName(id)
    local data = self.GetCardPiece(id)
    return data and data.name or nil
end

function CardPieceDataHelper:GetQuality(id)
    local data = self.GetCardPiece(id)
    return data and data.quality or nil
end

function CardPieceDataHelper:GetLevel(id)
    local data = self.GetCardPiece(id)
    return data and data.level or nil
end

function CardPieceDataHelper:GetItemId(id)
    local data = self.GetCardPiece(id)
    return data and data.item_id or nil
end

return CardPieceDataHelper
