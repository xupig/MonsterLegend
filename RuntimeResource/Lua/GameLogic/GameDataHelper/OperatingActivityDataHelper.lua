-- OperatingActivityDataHelper.lua
--属性命名配置
local OperatingActivityDataHelper = {}
local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local HiPointCfg = Class.HiPointCfg(ClassTypes.XObject)
local HiPointRewardCfg = Class.HiPointRewardCfg(ClassTypes.XObject)
local WeaponGodsCfg = Class.WeaponGodsCfg(ClassTypes.XObject)
local TiringChargeCfg = Class.TiringChargeCfg(ClassTypes.XObject)
local FireworksCfg = Class.FireworksCfg(ClassTypes.XObject)
local CelebrationCfg = Class.CelebrationCfg(ClassTypes.XObject)
local SoulBattleRewardCfg = Class.SoulBattleRewardCfg(ClassTypes.XObject)
local DoubleRewardCfg = Class.DoubleRewardCfg(ClassTypes.XObject)
local LoginRewardCfg = Class.LoginRewardCfg(ClassTypes.XObject)

local hiPointCfgCache = {}
function OperatingActivityDataHelper.ClearCache()
    hiPointCfgCache = {}
end

local hiPointRewardCfgCache = {}
function OperatingActivityDataHelper.ClearHiPointRewardCfgCache()
    hiPointRewardCfgCache = {}
end

local weaponGodsCfgCache = {}
function OperatingActivityDataHelper.ClearWeaponGodsCfgCache()
    weaponGodsCfgCache = {}
end

local tiringChargeCfgCache = {}
function OperatingActivityDataHelper.ClearTiringChargeCfgCache()
    tiringChargeCfgCache = {}
end

local fireworksCfgCache = {}
function OperatingActivityDataHelper.ClearFireworksCfgCache()
    fireworksCfgCache = {}
end

local celebrationCfgCache = {}
function OperatingActivityDataHelper.ClearCelebrationCfgCache()
    celebrationCfgCache = {}
end

local soulBattleRewardCfgCache = {}
function OperatingActivityDataHelper.ClearSoulBattleRewardCfgCache()
    soulBattleRewardCfgCache = {}
end

local doubleRewardCfgCache = {}
function OperatingActivityDataHelper.ClearDoubleRewardCfgCache()
    doubleRewardCfgCache = {}
end

local loginRewardCfgCache = {}
function OperatingActivityDataHelper.ClearLoginRewardCfgCache()
    loginRewardCfgCache = {}
end

function HiPointCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.name = cfg.__name
	self.desc = cfg.__desc
	self.icon = cfg.__icon
	self.times = cfg.__times
	self.reward_point = cfg.__reward_point
	self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
end

function HiPointRewardCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.cost_point = cfg.__cost_point
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function WeaponGodsCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.rank = cfg.__rank
	self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function TiringChargeCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.cost = cfg.__cost
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function FireworksCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.cost = cfg.__cost
	self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function CelebrationCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.cost = DataParseHelper.ParseNSuffix(cfg.__cost)
	self.exchange = cfg.__exchange
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function SoulBattleRewardCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.rank = cfg.__cost
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function DoubleRewardCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.day_reward = DataParseHelper.ParseListInt(cfg.__day_reward)
	self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
end

function LoginRewardCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.day = cfg.__day
	self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function OperatingActivityDataHelper.GetHiPointCfg(id)
    ClearDataManager:UseCache(CacheNames.hi_point, OperatingActivityDataHelper.ClearCache)
	local data = hiPointCfgCache[id]
	if data == nil then
		local cfg = XMLManager.hi_point[id]
		if cfg ~= nil then
			data = HiPointCfg(cfg)
			hiPointCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetHiPointCfgList()
	local result = {}
	local keys = XMLManager.hi_point:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetHiPointCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

function OperatingActivityDataHelper.GetHiPointRewardCfg(id)
    ClearDataManager:UseCache(CacheNames.hi_point_reward, OperatingActivityDataHelper.ClearHiPointRewardCfgCache)
	local data = hiPointRewardCfgCache[id]
	if data == nil then
		local cfg = XMLManager.hi_point_reward[id]
		if cfg ~= nil then
			data = HiPointRewardCfg(cfg)
			hiPointRewardCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetHiPointRewardCfgList()
	local result = {}
	local keys = XMLManager.hi_point_reward:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetHiPointRewardCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

function OperatingActivityDataHelper.GetWeaponGodsCfg(id)
    ClearDataManager:UseCache(CacheNames.weapon_gods, OperatingActivityDataHelper.ClearWeaponGodsCfgCache)
	local data = weaponGodsCfgCache[id]
	if data == nil then
		local cfg = XMLManager.weapon_gods[id]
		if cfg ~= nil then
			data = WeaponGodsCfg(cfg)
			weaponGodsCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetWeaponGodsCfgList(worldLevel)
	local result = {}
	local keys = XMLManager.weapon_gods:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetWeaponGodsCfg(keys[i])
		if cfg.id > 0 then
			if worldLevel >= cfg.world_level[1] and worldLevel <= cfg.world_level[2] then
				table.insert(result,cfg)
			end
		end
	end
	return result
end

function OperatingActivityDataHelper.GetTiringChargeCfg(id)
    ClearDataManager:UseCache(CacheNames.tiring_charge, OperatingActivityDataHelper.ClearTiringChargeCfgCache)
	local data = tiringChargeCfgCache[id]
	if data == nil then
		local cfg = XMLManager.tiring_charge[id]
		if cfg ~= nil then
			data = TiringChargeCfg(cfg)
			tiringChargeCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetTiringChargeCfgList()
	local result = {}
	local keys = XMLManager.tiring_charge:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetTiringChargeCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

function OperatingActivityDataHelper.GetFireworksCfg(id)
    ClearDataManager:UseCache(CacheNames.fireworks, OperatingActivityDataHelper.ClearFireworksCfgCache)
	local data = fireworksCfgCache[id]
	if data == nil then
		local cfg = XMLManager.fireworks[id]
		if cfg ~= nil then
			data = FireworksCfg(cfg)
			fireworksCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetFireworksCfgByWorldLevel(worldLevel)
	local keys = XMLManager.fireworks:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetFireworksCfg(keys[i])
		if cfg.id > 0 then
			if worldLevel >= cfg.world_level[1] and worldLevel <= cfg.world_level[2] then
				return cfg
			end
		end
	end
	return nil
end

function OperatingActivityDataHelper.GetCelebrationCfg(id)
    ClearDataManager:UseCache(CacheNames.celebration, OperatingActivityDataHelper.ClearCelebrationCfgCache)
	local data = celebrationCfgCache[id]
	if data == nil then
		local cfg = XMLManager.celebration[id]
		if cfg ~= nil then
			data = CelebrationCfg(cfg)
			celebrationCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetCelebrationCfgList()
	local result = {}
	local keys = XMLManager.celebration:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetCelebrationCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

function OperatingActivityDataHelper.GetSoulBattleRewardCfg(id)
    ClearDataManager:UseCache(CacheNames.soul_battle_reward, OperatingActivityDataHelper.ClearSoulBattleRewardCfgCache)
	local data = soulBattleRewardCfgCache[id]
	if data == nil then
		local cfg = XMLManager.soul_battle_reward[id]
		if cfg ~= nil then
			data = SoulBattleRewardCfg(cfg)
			soulBattleRewardCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper.GetDoubleRewardCfg(id)
    ClearDataManager:UseCache(CacheNames.double_reward, OperatingActivityDataHelper.ClearDoubleRewardCfgCache)
	local data = doubleRewardCfgCache[id]
	if data == nil then
		local cfg = XMLManager.double_reward[id]
		if cfg ~= nil then
			data = DoubleRewardCfg(cfg)
			doubleRewardCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetDoubleRewardCfgList()
	local result = {}
	local keys = XMLManager.double_reward:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetDoubleRewardCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

function OperatingActivityDataHelper.GetLoginRewardCfg(id)
    ClearDataManager:UseCache(CacheNames.login_reward, OperatingActivityDataHelper.ClearLoginRewardCfgCache)
	local data = loginRewardCfgCache[id]
	if data == nil then
		local cfg = XMLManager.login_reward[id]
		if cfg ~= nil then
			data = LoginRewardCfg(cfg)
			loginRewardCfgCache[data.id] = data
		end
	end
	return data
end

function OperatingActivityDataHelper:GetLoginRewardCfgList()
	local result = {}
	local keys = XMLManager.login_reward:Keys()
	for i=1,#keys do
		local cfg = OperatingActivityDataHelper.GetLoginRewardCfg(keys[i])
		if cfg.id > 0 then
			table.insert(result,cfg)
		end
	end
	return result
end

return OperatingActivityDataHelper
