-- PvpTotalDataHelper.lua
local PvpTotalDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local PvpTotal = Class.PvpTotal(ClassTypes.XObject)

local pvpTotalCache = {}
function PvpTotalDataHelper.ClearCache()
    pvpTotalCache = {}
end

function PvpTotal:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.open = cfg.__open
end

function PvpTotalDataHelper.GetPvpTotal(id)
    ClearDataManager:UseCache(CacheNames.pvp_total, PvpTotalDataHelper.ClearCache)
    local data = pvpTotalCache[id]
    if data == nil then
        local cfg = XMLManager.pvp_total[id]
        if cfg ~= nil then
            data = PvpTotal(cfg)
            pvpTotalCache[data.id] = data
        else
            LoggerHelper.Error("PvpTotal id not exist:" .. id)
        end
    end
    return data
end

function PvpTotalDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.pvp_total:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PvpTotalDataHelper:GetName(id)
    local data = self.GetPvpTotal(id)
    return data and data.name or nil
end

function PvpTotalDataHelper:GetIcon(id)
    local data = self.GetPvpTotal(id)
    return data and data.icon or nil
end

function PvpTotalDataHelper:GetOpen(id)
    local data = self.GetPvpTotal(id)
    return data and data.open or nil
end

return PvpTotalDataHelper
