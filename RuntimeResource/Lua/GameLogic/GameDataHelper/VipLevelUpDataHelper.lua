-- VipLevelUpDataHelper.lua
local VipLevelUpDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local VipLevelUp = Class.VipLevelUp(ClassTypes.XObject)

local vipLevelUpCache = {}

function VipLevelUp:__ctor__(cfg)
    self.id = cfg.__id
    self.vip_level = cfg.__vip_level
    self.level = cfg.__level
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
    self.icon = cfg.__icon
    self.count = cfg.__count
end

function VipLevelUpDataHelper.GetVipLevelUp(id)
    local data = vipLevelUpCache[id]
    if data == nil then
        local cfg = XMLManager.vip_level_up[id]
        if cfg ~= nil then
            data = VipLevelUp(cfg)
            vipLevelUpCache[data.id] = data
        else
            LoggerHelper.Error("VipLevelUp id not exist:" .. id)
        end
    end
    return data
end

function VipLevelUpDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.vip_level_up:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function VipLevelUpDataHelper:GetVipLevel(id)
    local data = self.GetVipLevelUp(id)
    return data and data.vip_level or nil
end

function VipLevelUpDataHelper:GetLevel(id)
    local data = self.GetVipLevelUp(id)
    return data and data.level or nil
end

function VipLevelUpDataHelper:GetReward(id)
    local data = self.GetVipLevelUp(id)
    return data and data.reward or nil
end

function VipLevelUpDataHelper:GetIcon(id)
    local data = self.GetVipLevelUp(id)
    return data and data.icon or nil
end

function VipLevelUpDataHelper:GetCount(id)
    local data = self.GetVipLevelUp(id)
    return data and data.count or nil
end

return VipLevelUpDataHelper
