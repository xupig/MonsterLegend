-- OfflinePvpReward1DataHelper.lua
local OfflinePvpReward1DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OfflinePvpReward1 = Class.OfflinePvpReward1(ClassTypes.XObject)

local offlinePvpReward1Cache = {}
function OfflinePvpReward1DataHelper.ClearCache()
    offlinePvpReward1Cache = {}
end


function OfflinePvpReward1:__ctor__(cfg)
    self.id = cfg.__id
    self.rank = DataParseHelper.ParseListInt(cfg.__rank)
    self.icon = cfg.__icon
    self.name = cfg.__name
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
end

function OfflinePvpReward1DataHelper.GetOfflinePvpReward1(id)
    ClearDataManager:UseCache(CacheNames.offline_pvp_reward_1, OfflinePvpReward1DataHelper.ClearCache)
    local data = offlinePvpReward1Cache[id]
    if data == nil then
        local cfg = XMLManager.offline_pvp_reward_1[id]
        if cfg ~= nil then
            data = OfflinePvpReward1(cfg)
            offlinePvpReward1Cache[data.id] = data
        else
            LoggerHelper.Error("OfflinePvpReward1 id not exist:" .. id)
        end
    end
    return data
end

function OfflinePvpReward1DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.offline_pvp_reward_1:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OfflinePvpReward1DataHelper:GetRank(id)
    local data = self.GetOfflinePvpReward1(id)
    return data and data.rank or nil
end

function OfflinePvpReward1DataHelper:GetIcon(id)
    local data = self.GetOfflinePvpReward1(id)
    return data and data.icon or nil
end

function OfflinePvpReward1DataHelper:GetName(id)
    local data = self.GetOfflinePvpReward1(id)
    return data and data.name or nil
end

function OfflinePvpReward1DataHelper:GetReward(id)
    local data = self.GetOfflinePvpReward1(id)
    return data and data.reward or nil
end

return OfflinePvpReward1DataHelper
