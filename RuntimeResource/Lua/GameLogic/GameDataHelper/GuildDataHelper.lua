local GuildDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local OpenTimeDataHelper = GameDataHelper.OpenTimeDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = require("ServerConfig/public_config")
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

------------------------------公会徽章表-------------------
local guildIconData = {}
function GuildDataHelper.ClearCache()
    guildIconData = {}
end

local GuildIconCfg = Class.GuildIconCfg(ClassTypes.XObject)
local badgeIconData = nil
local flagIconData = nil

function GuildIconCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.icon = cfg.__icon
    self.bg_icon = cfg.__bg_icon
end

function GuildDataHelper:GetGuildIconData(id)
    ClearDataManager:UseCache(CacheNames.guild_icon, GuildDataHelper.ClearCache)
    local data = guildIconData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_icon[id]
    if data~=nil then
	    data = GuildIconCfg(data)
    end
	guildIconData[id] = data
	return data
end

function GuildDataHelper:GetGuildIconId(id)
    local data = self:GetGuildIconData(id)
    return data.icon
end

function GuildDataHelper:GetGuildBGIconId(id)
    local data = self:GetGuildIconData(id)
    return data.bg_icon
end

function GuildDataHelper:GetAllBadgeIcon()
    if badgeIconData == nil then
        badgeIconData = {}
        local keys = XMLManager.guild_icon:Keys()
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                table.insert(badgeIconData, self:GetGuildIconId(id))
            end
        end
    end
    return badgeIconData
end

function GuildDataHelper:GetAllFlagIcon()
    if flagIconData == nil then
        flagIconData = {}
        local keys = XMLManager.guild_icon:Keys()
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                table.insert(flagIconData, self:GetGuildBGIconId(id))
            end
        end
    end
    return flagIconData
end

------------------------------公会等级表-------------------
local guildLevelData = {}
function GuildDataHelper.ClearGuildLevelCache()
    guildLevelData = {}
end
local GuildLevelCfg = Class.GuildLevelCfg(ClassTypes.XObject)

function GuildLevelCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.member_num = cfg.__member_num
end

function GuildDataHelper:GetGuildLevelData(id)
    ClearDataManager:UseCache(CacheNames.guild_level, GuildDataHelper.ClearGuildLevelCache)
    local data = guildLevelData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_level[id]
    if data~=nil then
	    data = GuildLevelCfg(data)
    end
	guildLevelData[id] = data
	return data
end

function GuildDataHelper:GetGuildLevelCost(level)
    local data = self:GetGuildLevelData(level)
    return data.cost
end

function GuildDataHelper:GetGuildMaxMemberCount(level)
    local data = self:GetGuildLevelData(level)
    return data.member_num
end

------------------------------公会红包表-------------------
local guildRedEnvelopeData = {}
function GuildDataHelper.ClearGuildCache()
    guildRedEnvelopeData = {}
end
local GuildRedEnvelopeCfg = Class.GuildRedEnvelopeCfg(ClassTypes.XObject)
local signRedEnvelopeData

function GuildRedEnvelopeCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.type = cfg.__type
    self.desc_record = cfg.__desc_record
    self.packet_reward = DataParseHelper.ParseMSuffix(cfg.__packet_reward)
end

function GuildDataHelper:GetGuildRedEnvelopeData(id)
    ClearDataManager:UseCache(CacheNames.guild_red_packet, GuildDataHelper.ClearGuildCache)
    local data = guildRedEnvelopeData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_red_packet[id]
    if data~=nil then
	    data = GuildRedEnvelopeCfg(data)
    end
	guildRedEnvelopeData[id] = data
	return data
end

function GuildDataHelper:GetGuildSignRedEnvelopeData()
    if signRedEnvelopeData == nil then
        signRedEnvelopeData = {}
        local keys = XMLManager.guild_red_packet:Keys()
		for i=1,#keys do
			local data = self:GetGuildRedEnvelopeData(keys[i])
			if data.type == public_config.GUILD_RED_ENVELOPE_TYPE_SIGN_UP then
				local tmp = {["id"]=keys[i], ["order"]=data.param[1]}
                table.insert(signRedEnvelopeData, tmp)
			end
		end
        table.sort(signRedEnvelopeData,SortByOrder)
    end
    return signRedEnvelopeData
end

function GuildDataHelper:GetGuildRedEnvelopeName(id)
    local data = self:GetGuildRedEnvelopeData(id)
    return LanguageDataHelper.CreateContent(data.name)
end

function GuildDataHelper:GetGuildRedEnvelopeRecordDesc(id)
    local data = self:GetGuildRedEnvelopeData(id)
    return data.desc_record
end

function GuildDataHelper:GetGuildRedEnvelopeReward(id)
    local data = self:GetGuildRedEnvelopeData(id)
    return data.packet_reward
end

------------------------------公会技能表-------------------
local guildSkillData = {}
function GuildDataHelper.ClearGuildSkillCache()
    guildSkillData = {}
end

local guildSkillDataCache = {}
function GuildDataHelper.ClearGuildSkillDataCache()
    guildSkillDataCache = {}
end

local guildSkillMaxLevelCache = {}
function GuildDataHelper.ClearSkillMaxLevelCache()
    guildSkillMaxLevelCache = {}
end

local guildSkillTypeCache = nil
function GuildDataHelper.ClearSkillTypeCache()
    guildSkillTypeCache = nil
end

local GuildSkillCfg = Class.GuildSkillCfg(ClassTypes.XObject)

function GuildSkillCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.cost = cfg.__cost
    self.level = cfg.__level
    self.icon = cfg.__icon
    self.type = cfg.__type
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

function GuildDataHelper:GetGuildSkillData(id)
    ClearDataManager:UseCache(CacheNames.guild_skill, GuildDataHelper.ClearGuildSkillCache)
    local data = guildSkillData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_skill[id]
    if data~=nil then
	    data = GuildSkillCfg(data)
    end
	guildSkillData[id] = data
	return data
end

function GuildDataHelper:GetGuildSkillDataBy2(type, level)
    ClearDataManager:UseCache(CacheNames.guildSkillDataCache, GuildDataHelper.ClearGuildSkillDataCache)
    if guildSkillDataCache[type] == nil then
		guildSkillDataCache[type] = {}
	end
    if guildSkillDataCache[type][level] == nil then
		local keys = XMLManager.guild_skill:Keys()
		for i=1,#keys do
			local data = self:GetGuildSkillData(keys[i])
			if data.type == type and data.level == level then
				guildSkillDataCache[type][level] = data
			end
		end
	end
	return guildSkillDataCache[type][level]
end

function GuildDataHelper:GetGuildSkillMaxLevel(type)
    ClearDataManager:UseCache(CacheNames.guildSkillMaxLevelCache, GuildDataHelper.ClearSkillMaxLevelCache)
    if guildSkillMaxLevelCache[type] == nil then
		local maxLevel = 0
        local keys = XMLManager.guild_skill:Keys()
		for i=1,#keys do
			local data = self:GetGuildSkillData(keys[i])
			if data.type == type then
				if data.level > maxLevel then
                    maxLevel = data.level
                end
			end
		end
        guildSkillMaxLevelCache[type] = maxLevel
	end
	return guildSkillMaxLevelCache[type]
end

function GuildDataHelper:GetGuildSkillAllType()
    ClearDataManager:UseCache(CacheNames.guildSkillTypeCache, GuildDataHelper.ClearSkillTypeCache)
    if guildSkillTypeCache == nil then
        guildSkillTypeCache = {}
        local tmp = {}
        local keys = XMLManager.guild_skill:Keys()
		for i=1,#keys do
            if keys[i] ~= 0 then
                local data = self:GetGuildSkillData(keys[i])
                if data.type ~= 0 and tmp[data.type] == nil then
                    tmp[data.type] = 1
                    table.insert(guildSkillTypeCache, data.type)
                end
            end
		end
	end
	return guildSkillTypeCache
end

------------------------------公会令牌表-------------------
local guildTokenData = {}
function GuildDataHelper.ClearGuildTokenDataCache()
    guildTokenData = {}
end
local GuildTokenCfg = Class.GuildTokenCfg(ClassTypes.XObject)

function GuildTokenCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.level = cfg.__level
    self.map_desc = cfg.__map_desc
    self.map_id = cfg.__map_id
    self.boss_region_id = cfg.__boss_region_id
    self.boss_id = cfg.__boss_id
end

function GuildDataHelper:GetGuildTokenData(id)
    ClearDataManager:UseCache(CacheNames.guild_token, GuildDataHelper.ClearGuildTokenDataCache)
    local data = guildTokenData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_token[id]
    if data~=nil then
	    data = GuildTokenCfg(data)
    end
	guildTokenData[id] = data
	return data
end

function GuildDataHelper:GetAllTokenId()
    local keys = XMLManager.guild_token:Keys()
    local result = {}
    for i=1,#keys do
        if keys[i] ~= 0 then
            table.insert(result, {keys[i]})
        end
    end
    table.sort(result, SortByKey1)
    return result
end

function GuildDataHelper:GetTokenMonsterName(id)
    local data = self:GetGuildTokenData(id)
    return LanguageDataHelper.GetContent(data.name)
end

function GuildDataHelper:GetTokenMapDesc(id)
    local data = self:GetGuildTokenData(id)
    return data.map_desc
end

function GuildDataHelper:GetTokenMapId(id)
    local data = self:GetGuildTokenData(id)
    return data.map_id
end

function GuildDataHelper:GetTokenRegionId(id)
    local data = self:GetGuildTokenData(id)
    return data.boss_region_id
end

function GuildDataHelper:GetTokenBossId(id)
    local data = self:GetGuildTokenData(id)
    return data.boss_id
end
------------------------------公会神兽表-------------------
local guildMonsterData = {}
function GuildDataHelper.ClearGuildMonsterCache()
    guildMonsterData = {}
end
local GuildMonsterCfg = Class.GuildMonsterCfg(ClassTypes.XObject)

function GuildMonsterCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.icon = cfg.__icon
    self.monster_id = cfg.__monster_id
    self.time = cfg.__time
    self.star_time = DataParseHelper.ParseNSuffix(cfg.__star_time)
end

function GuildDataHelper:GetGuildMonsterData(id)
    ClearDataManager:UseCache(CacheNames.guild_monster, GuildDataHelper.ClearGuildMonsterCache)
    local data = guildMonsterData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_monster[id]
    if data~=nil then
	    data = GuildMonsterCfg(data)
    end
	guildMonsterData[id] = data
	return data
end

function GuildDataHelper:GetGuildMonsterModel(id)
    local data = self:GetGuildMonsterData(id)
    return MonsterDataHelper.GetMonsterModel(data.monster_id)
end

function GuildDataHelper:GetGuildMonsterNameIcon(id)
    local data = self:GetGuildMonsterData(id)
    return data.icon
end

function GuildDataHelper:GetStartTime(id)
    local data = self:GetGuildMonsterData(id)
    local returnData = {}
    for k,v in pairs(data.star_time) do
        returnData[k] = v
    end
    return returnData
end

function GuildDataHelper:GetGuildMonsterTime(id)
    local data = self:GetGuildMonsterData(id)
    return data.time
end

------------------------------公会神兽奖励表-------------------
local guildMonsterRewardData = {}
function GuildDataHelper.ClearGuildMonsterRewardCache()
    guildMonsterRewardData = {}
end

local GuildMonsterRewardCfg = Class.GuildMonsterRewardCfg(ClassTypes.XObject)

function GuildMonsterRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.world_level = cfg.__world_level
    self.preview_reward = DataParseHelper.ParseNSuffix(cfg.__preview_reward)
end

function GuildDataHelper:GetGuildMonsterRewardData(id)
    ClearDataManager:UseCache(CacheNames.guild_monster_reward, GuildDataHelper.ClearGuildMonsterRewardCache)
    local data = guildMonsterRewardData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_monster_reward[id]
    if data~=nil then
	    data = GuildMonsterRewardCfg(data)
    end
	guildMonsterRewardData[id] = data
	return data
end

local guildMonsterRewardDataCache
function GuildDataHelper:GetGuildMonsterReward(worldLevel)
    if guildMonsterRewardDataCache == nil then
        guildMonsterRewardDataCache = {}
        local keys = XMLManager.guild_monster_reward:Keys()
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                local data = self:GetGuildMonsterRewardData(id)
                guildMonsterRewardDataCache[data.world_level] = data
            end
        end
    end
    return guildMonsterRewardDataCache[worldLevel].preview_reward
end

------------------------------公会守护女神表-------------------
local guildGuardGoddnessData = {}
function GuildDataHelper.ClearGuildGuardGoddnessDataCache()
    guildGuardGoddnessData = {}
end
local GuildGuardGoddnessCfg = Class.GuildGuardGoddnessCfg(ClassTypes.XObject)

function GuildGuardGoddnessCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.world_level = cfg.__world_level
    self.time = cfg.__time
end

function GuildDataHelper:GetGuildGuardGoddnessData(id)
    ClearDataManager:UseCache(CacheNames.guild_protect, GuildDataHelper.ClearGuildGuardGoddnessDataCache)
    local data = guildGuardGoddnessData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_protect[id]
    if data~=nil then
	    data = GuildGuardGoddnessCfg(data)
    end
	guildGuardGoddnessData[id] = data
	return data
end

local guildGuardGoddnessDataCache
function GuildDataHelper:GetGuildGuardGoddnessTime(worldLevel)
    if guildGuardGoddnessDataCache == nil then
        guildGuardGoddnessDataCache = {}
        local keys = XMLManager.guild_protect:Keys()
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                local data = self:GetGuildGuardGoddnessData(id)
                guildGuardGoddnessDataCache[data.world_level] = data
            end
        end
    end
    return guildGuardGoddnessDataCache[worldLevel].time
end

------------------------------公会篝火表-------------------
local guildBanquetData = {}
function GuildDataHelper.ClearGuildBanquetDataCache()
    guildBanquetData = {}
end
local GuildBanquetCfg = Class.GuildBanquetCfg(ClassTypes.XObject)

function GuildBanquetCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.question_desc = cfg.__question_desc
end

function GuildDataHelper:GetGuildBanquetData(id)
    ClearDataManager:UseCache(CacheNames.guild_bonfire, GuildDataHelper.ClearGuildBanquetDataCache)
    local data = guildBanquetData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_bonfire[id]
    if data~=nil then
	    data = GuildBanquetCfg(data)
    end
	guildBanquetData[id] = data
	return data
end

function GuildDataHelper:GetGuildBanquetQuestionDesc(id)
    local data = self:GetGuildBanquetData(id)
    return data.question_desc
end

------------------------------公会篝火奖励表-------------------
local guildBanquetRewardData = {}
function GuildDataHelper.ClearGuildBanquetRewardDataCache()
    guildBanquetRewardData = {}
end
local GuildBanquetRewardCfg = Class.GuildBanquetRewardCfg(ClassTypes.XObject)

function GuildBanquetRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.reward_win = DataParseHelper.ParseMSuffix(cfg.__reward_win)
    self.reward_lose = DataParseHelper.ParseMSuffix(cfg.__reward_lose)
end

function GuildDataHelper:GetGuildBanquetRewardData(id)
    ClearDataManager:UseCache(CacheNames.guild_conquest_reward, GuildDataHelper.ClearGuildBanquetRewardDataCache)
    local data = guildBanquetRewardData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.guild_conquest_reward[id]
    if data~=nil then
	    data = GuildBanquetRewardCfg(data)
    end
	guildBanquetRewardData[id] = data
	return data
end

function GuildDataHelper:GetGuildBanquetWinReward(id)
    local data = self:GetGuildBanquetRewardData(id)
    return data.reward_win
end

function GuildDataHelper:GetGuildBanquetLoseReward(id)
    local data = self:GetGuildBanquetRewardData(id)
    return data.reward_lose
end

------------------------------公会主宰神殿连胜配置表-------------------
local dominateGuildRewardCache = {}
function GuildDataHelper.ClearDominateGuildRewardCache()
    dominateGuildRewardCache = {}
end
local DominateGuildRewardCfg = Class.DominateGuildRewardCfg(ClassTypes.XObject)

function DominateGuildRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.win = cfg.__win
    self.attri_num = cfg.__attri_num
    self.level = DataParseHelper.ParseListInt(cfg.__level)
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
    self.reward_match = DataParseHelper.ParseMSuffix(cfg.__reward_match)
end

function GuildDataHelper:GetDominateGuildRewardData(id)
    ClearDataManager:UseCache(CacheNames.dominate_guild_reward, GuildDataHelper.ClearDominateGuildRewardCache)
    local data = dominateGuildRewardCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.dominate_guild_reward[id]
    if data~=nil then
	    data = DominateGuildRewardCfg(data)
    end
	dominateGuildRewardCache[id] = data
	return data
end

function GuildDataHelper:GetAllDominateGuildRewardData(level)
    local result = {}
    local keys = XMLManager.dominate_guild_reward:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetDominateGuildRewardData(id)
            if data.level[1] <= level and data.level[2] >= level then
                table.insert(result, {data.win, data.id})
            end
        end
    end
    table.sort(result, SortByKey1)
    return result
end


return GuildDataHelper