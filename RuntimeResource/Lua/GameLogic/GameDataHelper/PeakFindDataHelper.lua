--巅峰寻宝
local PeakFindDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

local TreasurePeakShowCfg = Class.TreasurePeakShowCfg(ClassTypes.XObject)

function TreasurePeakShowCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.item_id = cfg.__item_id
    self.icon_special = cfg.__icon_special
    self.level = DataParseHelper.ParseListInt(cfg.__level)
    self.vocation = DataParseHelper.ParseListInt(cfg.__vocation)
end

local treasureTopShowCache = {}
function PeakFindDataHelper.ClearCache()
    treasureTopShowCache = {}
end

function PeakFindDataHelper:GetPeakFindShowData(id)
    ClearDataManager:UseCache(CacheNames.treasure_top_show, PeakFindDataHelper.ClearCache)
    local data = treasureTopShowCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.treasure_top_show[id]
    if data~=nil then
	    data = TreasurePeakShowCfg(data)
    end
	treasureTopShowCache[id] = data
	return data
end

function PeakFindDataHelper:GetEquipFindType(id)
    local data = self:GetPeakFindShowData(id)
    if data~=nil then
	    return data.type or 3
    end
    return 3
end

function PeakFindDataHelper:GetEquipFindLevel(id)
    local data = self:GetPeakFindShowData(id)
    if data~=nil then
	    return data.level
    end
    return nil
end

function PeakFindDataHelper:GetEquipFindItemId(id)
    local data = self:GetPeakFindShowData(id)
    if data~=nil then
	    return data.item_id
    end
    return nil
end

function PeakFindDataHelper:GetEquipFindNormalData(level, vocGroup)
    local result = {}
    local keys = XMLManager.treasure_top_show:Keys()
    for i=1,#keys do
        local id = keys[i]
        local data = self:GetPeakFindShowData(id)
        if id > 0 and data.type == 3 then
            if data.level ~= nil then
                if data.level[1] <= level and data.level[2] >= level then
                    for k,v in pairs(data.vocation) do
                        if v == vocGroup then
                            table.insert(result, id)
                            break
                        end
                    end
                end
            else
                for k,v in pairs(data.vocation) do
                    if v == vocGroup then
                        table.insert(result, id)
                        break
                    end
                end
            end
        end
    end
    return result
end

function PeakFindDataHelper:GetEquipFindSpecialData(level, vocGroup)
    local result = {}
    local mark
    local keys = XMLManager.treasure_top_show:Keys()
    for i=1,#keys do
        local id = keys[i]
        local data = self:GetPeakFindShowData(id)
        if id > 0 and data.type < 3 then
            if data.level ~= nil then
                if data.level[1] <= level and data.level[2] >= level then
                    for k,v in pairs(data.vocation) do
                        if v == vocGroup then
                            if data.type == 1 then
                                mark = id
                            else
                                table.insert(result, id)
                            end
                            break
                        end
                    end
                end
            else
                for k,v in pairs(data.vocation) do
                    if v == vocGroup then
                        if data.type == 1 then
                            mark = id
                        else
                            table.insert(result, id)
                        end
                        break
                    end
                end
            end
        end
    end
    table.insert(result, mark) --特别稀有保持在最后
    return result
end

----------------------------积分兑换配置表---------------------------
local TreasureTopPointCfg = Class.TreasureTopPointCfg(ClassTypes.XObject)

function TreasureTopPointCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.item_id = cfg.__item_id
    self.point = cfg.__point
    self.vocation = DataParseHelper.ParseListInt(cfg.__vocation)
end

local treasureEquipPointCache = {}
function PeakFindDataHelper.ClearEquipFindCache()
    treasureEquipPointCache = {}
end


function PeakFindDataHelper:GetPeakFindPointData(id)
    ClearDataManager:UseCache(CacheNames.treasure_top_point, PeakFindDataHelper.ClearEquipFindCache)
    local data = treasureEquipPointCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.treasure_top_point[id]
    if data~=nil then
	    data = TreasureTopPointCfg(data)
    end
	treasureEquipPointCache[id] = data
	return data
end

local allEquipFindPointCache
function PeakFindDataHelper:GetAllEquipFindPoint(vocGroup)
    if allEquipFindPointCache == nil then
        allEquipFindPointCache = {}
        local keys = XMLManager.treasure_top_point:Keys()
        for i=1,#keys do
            local id = keys[i]
            local data = self:GetPeakFindPointData(id)
            if id > 0 then
                for k,v in pairs(data.vocation) do
                    if v == vocGroup then
                        table.insert(allEquipFindPointCache, {id})
                        break
                    end
                end
            end
        end
        table.sort(allEquipFindPointCache, SortByKey1)
    end
    return allEquipFindPointCache
end

function PeakFindDataHelper:GetEquipFindPointItemId(id)
    local data = self:GetPeakFindPointData(id)
    if data~=nil then
	    return data.item_id
    end
    return nil
end

function PeakFindDataHelper:GetEquipFindPoint(id)
    local data = self:GetPeakFindPointData(id)
    if data~=nil then
	    return data.point
    end
    return nil
end


return PeakFindDataHelper