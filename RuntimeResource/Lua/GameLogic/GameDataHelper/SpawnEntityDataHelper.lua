SpawnEntityDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local TrapData = Class.TrapData(ClassTypes.XObject)
local ThrowObjectData = Class.ThrowObjectData(ClassTypes.XObject)

local trapData = {}
local throwObjectData = {}

function TrapData:__ctor__(cfg)
    self.id = cfg.__id
    self.model = cfg.__model
end

function ThrowObjectData:__ctor__(cfg)
    self.id = cfg.__id
    self.model_id = cfg.__model_id
end

function SpawnEntityDataHelper.GetTrapData(id)
    local data = trapData[id]
	if data == nil then
		local cfg = XMLManager.monster_summon[id]
		data = TrapData(cfg)
		trapData[data.id] = data
	end
	return data
end

function SpawnEntityDataHelper.GetTrapModelId(id)
    local data = SpawnEntityDataHelper.GetTrapData(id)
	if (data~=nil) then 
        return data.model
    end
	return nil
end

function SpawnEntityDataHelper.GetThrowObjectData(id)
    local data = throwObjectData[id]
	if data == nil then
		local cfg = XMLManager.throw_Object[id]
		data = ThrowObjectData(cfg)
		throwObjectData[data.id] = data
	end
	return data
end

function SpawnEntityDataHelper.GetThrowObjectModelId(id)
    local data = SpawnEntityDataHelper.GetThrowObjectData(id)
	if (data~=nil) then 
        return data.model_id
    end
	return nil
end

return SpawnEntityDataHelper