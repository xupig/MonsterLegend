local TreasureDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local TreasureExperienceCfg = Class.TreasureExperienceCfg(ClassTypes.XObject)
local TreasureSkillCfg = Class.TreasureSkillCfg(ClassTypes.XObject)
local TreasureModelCfg = Class.TreasureModelCfg(ClassTypes.XObject)

local treasureExperienceCache = {}
function TreasureDataHelper.ClearCache()
    treasureExperienceCache = {}
end

local treasureExperienceStructCache = {}
function TreasureDataHelper.ClearExpCache()
    treasureExperienceStructCache = {}
end

local treasureSkillCache = {}
function TreasureDataHelper.ClearSkillCache()
    treasureSkillCache = {}
end

function TreasureExperienceCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.type = cfg.__type
    self.wingLevelExp = cfg.__wingLevelExp
    self.wing_proportion = DataParseHelper.ParseMSuffix(cfg.__wing_proportion)
end

function TreasureDataHelper.GetTreasureExperience(id)
    ClearDataManager:UseCache(CacheNames.wing_experience, TreasureDataHelper.ClearCache)
    local data = treasureExperienceCache[id]
    if data == nil then
        local cfg = XMLManager.wing_experience[id]
        if cfg ~= nil then
            data = TreasureExperienceCfg(cfg)
            treasureExperienceCache[data.id] = data
        else
            LoggerHelper.Error("TreasureExperienceCfg id not exist:" .. id)
        end
    end
    return data
end

function TreasureDataHelper.GetTreasureExperienceByLevelType(treasureType,level)
    ClearDataManager:UseCache(CacheNames.wing_experience, TreasureDataHelper.ClearExpCache)
	if treasureExperienceStructCache[treasureType] == nil then
		local keys = XMLManager.wing_experience:Keys()
		for i=1,#keys do
			local expCfg = TreasureDataHelper.GetTreasureExperience(keys[i])
			local t = expCfg.type
			if treasureExperienceStructCache[t] == nil then
				treasureExperienceStructCache[t] = {}
			end
			treasureExperienceStructCache[t][expCfg.level] = expCfg
		end
	end
	return treasureExperienceStructCache[treasureType][level]
end


function TreasureSkillCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.level_skill = DataParseHelper.ParseMSuffixSort(cfg.__level_skill)
end

function TreasureDataHelper.GetTreasureSkill(id)
    ClearDataManager:UseCache(CacheNames.wing_skills, TreasureDataHelper.ClearSkillCache)
    local data = treasureSkillCache[id]
    if data == nil then
        local cfg = XMLManager.wing_skills[id]
        if cfg ~= nil then
            data = TreasureSkillCfg(cfg)
            treasureSkillCache[data.id] = data
        else
            LoggerHelper.Error("TreasureSkillCfg id not exist:" .. id)
        end
    end
    return data
end

return TreasureDataHelper