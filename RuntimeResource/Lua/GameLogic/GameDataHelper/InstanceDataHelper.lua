-- InstanceDataHelper.lua
local InstanceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local InstanceCfg = Class.InstanceCfg(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

local instanceCfgCache = {}
function InstanceDataHelper.ClearCache()
    instanceCfgCache = {}
end
local instanceIds = nil
local instanceCfgByMapType = nil

function InstanceCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.map_ids = DataParseHelper.ParseListInt(cfg.__map_ids)
    self.sub_type = cfg.__sub_type
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.describe = cfg.__describe
    self.target = DataParseHelper.ParseNSuffix(cfg.__target)
    self.instance_open = cfg.__instance_open
    self.instance_time = cfg.__instance_time
end

function InstanceDataHelper.GetInstanceCfg(id)
    ClearDataManager:UseCache(CacheNames.instance, InstanceDataHelper.ClearCache)
    local data = instanceCfgCache[id]
    if data == nil then
        local cfg = XMLManager.instance[id]
        data = InstanceCfg(cfg)
        instanceCfgCache[data.id] = data
    end
    return data
end

function InstanceDataHelper.GetInstanceMapIds(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.map_ids or nil
end

function InstanceDataHelper.GetInstanceMapId(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.map_ids[1] or nil
end

function InstanceDataHelper.GetSubType(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.sub_type or nil
end

function InstanceDataHelper.GetInstanceName(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.name or nil
end

function InstanceDataHelper.GetInstanceIcon(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.icon or nil
end

function InstanceDataHelper.GetInstanceDescribe(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.describe or nil
end

function InstanceDataHelper.GetTarget(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.target or nil
end

--获取副本开始的倒计时
function InstanceDataHelper.GetInstanceOpenTime(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.instance_open or 5
end

--获取副本时长
function InstanceDataHelper.GetInstanceTime(id)
    local data = InstanceDataHelper.GetInstanceCfg(id)
    return data and data.instance_time or 300
end

--根据地图ID查找副本ID
function InstanceDataHelper.GetInstanceIdByMapId(mapId)
    if instanceIds == nil then
        instanceIds = {}
        local keys = XMLManager.instance:Keys()
        for i = 1, #keys do
            local instanceCfg = InstanceDataHelper.GetInstanceCfg(keys[i])
            for j = 1, #instanceCfg.map_ids do
                instanceIds[instanceCfg.map_ids[j]] = instanceCfg.id
            end
        end
    end
    local instanceId = instanceIds[mapId]
    if instanceId == nil then
        instanceId = 0
    end
    return instanceId
end

--获取所有某地图类型的副本id
function InstanceDataHelper.GetInstancesCfgByMapType(mapType)
    if instanceCfgByMapType == nil then
        InstanceDataHelper.ConstructInstanceCfgByMapType()
    end
    return instanceCfgByMapType[mapType]
end

function InstanceDataHelper.ConstructInstanceCfgByMapType()
    instanceCfgByMapType = {}
    local keys = XMLManager.instance:Keys()
    for i = 1, #keys do
        local instanceCfg = InstanceDataHelper.GetInstanceCfg(keys[i])
        local mapId = instanceCfg.map_ids[1]
        local mapCfg = MapDataHelper.GetMapCfg(mapId)
        local mapType = mapCfg.type
        --LoggerHelper.Error("ConstructInstanceCfgByMapType"..mapType)
        if instanceCfgByMapType[mapType] == nil then
            instanceCfgByMapType[mapType] = {}
        end
        table.insert(instanceCfgByMapType[mapType], instanceCfg)
    end
end

return InstanceDataHelper
