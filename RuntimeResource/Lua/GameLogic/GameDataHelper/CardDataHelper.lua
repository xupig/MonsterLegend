-- CardDataHelper.lua
local CardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Card = Class.Card(ClassTypes.XObject)

local cardCache = {}
local cardIdsCache = {}
local cardTypesCache = {}
local cardAllTypeCache = {}
local cardSubTypeCache = {}
function CardDataHelper.ClearCache()
    cardCache = {}
    cardIdsCache = {}
    cardTypesCache = {}
    cardAllTypeCache = {}
    cardSubTypeCache = {}
end

function Card:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.type = cfg.__type
    self.type_name = cfg.__type_name
    self.quality = cfg.__quality
    self.level = cfg.__level
    self.level_desc = cfg.__level_desc
    self.rank = cfg.__rank
    self.sub_type = DataParseHelper.ParseMSuffix(cfg.__sub_type)
    self.sub_type_name = cfg.__sub_type_name
end

function CardDataHelper:GetSubType(id)
    local data = self.GetCard(id)
    return data and data.sub_type or nil
end

function CardDataHelper:GetSubTypeName(id)
    local data = self.GetCard(id)
    return data and data.sub_type_name or nil
end

function CardDataHelper.GetCard(id)
    ClearDataManager:UseCache(CacheNames.card, CardDataHelper.ClearCache)
    local data = cardCache[id]
    if data == nil then
        local cfg = XMLManager.card[id]
        if cfg ~= nil then
            data = Card(cfg)
            cardCache[data.id] = data
        else
            LoggerHelper.Error("Card id not exist:" .. id)
        end
    end
    return data
end

function CardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.card:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CardDataHelper:GetName(id)
    local data = self.GetCard(id)
    return data and data.name or nil
end

function CardDataHelper:GetIcon(id)
    local data = self.GetCard(id)
    return data and data.icon or nil
end

function CardDataHelper:GetType(id)
    local data = self.GetCard(id)
    return data and data.type or nil
end

function CardDataHelper:GetTypeName(id)
    local data = self.GetCard(id)
    return data and data.type_name or nil
end

function CardDataHelper:GetQuality(id)
    local data = self.GetCard(id)
    return data and data.quality or nil
end

function CardDataHelper:GetLevel(id)
    local data = self.GetCard(id)
    return data and data.level or nil
end

function CardDataHelper:GetLevelDesc(id)
    local data = self.GetCard(id)
    return data and data.level_desc or nil
end

function CardDataHelper:GetRank(id)
    local data = self.GetCard(id)
    return data and data.rank or nil
end


function CardDataHelper:GetCarSubTypes()
    if not table.isEmpty(cardIdsCache) then
        return cardIdsCache
    end
    cardIdsCache = {}
    local data = XMLManager.card:Keys()
    for i, id in ipairs(data) do
        if id ~= 0 then
            local t = CardDataHelper:GetType(id)
            if not cardIdsCache[t] then
                cardIdsCache[t]={}
            end
            local sub = CardDataHelper:GetSubType(id)
            if sub then
                local s,q = next(sub)
                if not table.containsValue(cardIdsCache[t],s) then
                    table.insert(cardIdsCache[t],s)
                end
            end
        end
    end
    return cardIdsCache
end

function CardDataHelper:GetCardTotalAllType()
    if not table.isEmpty(cardTypesCache)then 
        return cardTypesCache
    end    
    local data = XMLManager.card:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local t = CardDataHelper:GetType(v)            
            if not table.containsValue(cardTypesCache,t) then           
                cardTypesCache[t]={v,t}
            end     
        end
    end  
    return cardTypesCache
end

function CardDataHelper:GetCardTotal()
    if not table.isEmpty(cardAllTypeCache)then 
        return cardAllTypeCache
    end
    local data = XMLManager.card:Keys()
    local d = CardDataHelper:GetCardTotalAllType()
    for k,v in pairs(d) do
        local temple={}
        local mt = v[2]
        table.insert(temple,v[1])
        local subids = CardDataHelper:GetCardTotalSubByType(mt)
        for i, ids in ipairs(subids) do
            table.insert(temple,ids)
        end
        table.insert(cardAllTypeCache,temple)
    end 
    return cardAllTypeCache
end

function CardDataHelper:GetCardTotalSubByType(t)
    if cardSubTypeCache[t] and not table.isEmpty(cardSubTypeCache)then 
        return cardSubTypeCache[t]
    end
    cardSubTypeCache[t]={}
    local data = XMLManager.card:Keys()
    for k, id in ipairs(data) do
        if id ~= 0 then
            local cuttype = CardDataHelper:GetType(id)
            if cuttype==t then
                local m = CardDataHelper:GetSubType(id)
                if m then
                    local sub,q = next(m)
                    if not cardSubTypeCache[t][sub] then
                        cardSubTypeCache[t][sub] = {}
                    end
                    table.insert(cardSubTypeCache[t][sub],id)
                end        
            end
        end
    end  
    return cardSubTypeCache[t]
end

return CardDataHelper
