-- ComposeDragonsoulDataHelper.lua
local ComposeDragonsoulDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ComposeDragonsoul = Class.ComposeDragonsoul(ClassTypes.XObject)

local composeDragonsoulCache = {}
function ComposeDragonsoulDataHelper.ClearCache()
    composeDragonsoulCache = {}
end

function ComposeDragonsoul:__ctor__(cfg)
    self.id = cfg.__id
    self.item_comp_type = cfg.__item_comp_type
    self.item_tag = cfg.__item_tag
    self.item_tag_desc = cfg.__item_tag_desc
    self.dragonsoul_id = DataParseHelper.ParseMSuffix(cfg.__dragonsoul_id)
    self.item_id = DataParseHelper.ParseMSuffix(cfg.__item_id)
    self.dragonsoul_comp_id = cfg.__dragonsoul_comp_id
    self.sucess_prop = cfg.__sucess_prop
    self.level_limit = cfg.__level_limit
end

function ComposeDragonsoulDataHelper.GetComposeDragonsoul(id)
    ClearDataManager:UseCache(CacheNames.compose_dragonsoul, ComposeDragonsoulDataHelper.ClearCache)
    local data = composeDragonsoulCache[id]
    if data == nil then
        local cfg = XMLManager.compose_dragonsoul[id]
        if cfg ~= nil then
            data = ComposeDragonsoul(cfg)
            composeDragonsoulCache[data.id] = data
        else
            LoggerHelper.Error("ComposeDragonsoul id not exist:" .. id)
        end
    end
    return data
end

function ComposeDragonsoulDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.compose_dragonsoul:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ComposeDragonsoulDataHelper:GetItemCompType(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.item_comp_type or nil
end

function ComposeDragonsoulDataHelper:GetItemTag(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.item_tag or nil
end

function ComposeDragonsoulDataHelper:GetItemTagDesc(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.item_tag_desc or nil
end

function ComposeDragonsoulDataHelper:GetDragonsoulId(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.dragonsoul_id or nil
end

function ComposeDragonsoulDataHelper:GetItemId(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.item_id or nil
end

function ComposeDragonsoulDataHelper:GetDragonsoulCompId(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.dragonsoul_comp_id or nil
end

function ComposeDragonsoulDataHelper:GetSucessProp(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.sucess_prop or nil
end

function ComposeDragonsoulDataHelper:GetLevelLimit(id)
    local data = self.GetComposeDragonsoul(id)
    return data and data.level_limit or nil
end

function ComposeDragonsoulDataHelper:GetCfgData(dragonId)
    if self.cfgdata==nil then
        self.cfgdata={}
        local data = XMLManager.compose_dragonsoul:Keys()
        for i, id in ipairs(data) do
            if id ~= 0 then
                local data = self.GetComposeDragonsoul(id)
                self.cfgdata[data.dragonsoul_comp_id]=data
            end
        end
    end
    return self.cfgdata[dragonId]
end


return ComposeDragonsoulDataHelper
