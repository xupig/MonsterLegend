-- PVPDataHelper.lua
local PVPDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local FortCfg = Class.FortCfg(ClassTypes.XObject)

local fortCache = {}
function PVPDataHelper.ClearCache()
    fortCache = {}
end

function FortCfg:__ctor__(cfg)
		self.id = cfg.__id
		self.fort_id = DataParseHelper.ParseMSuffix(cfg.__fort_id)
		self.capture = cfg.__capture
		self.capture_speed = DataParseHelper.ParseMSuffix(cfg.__capture_speed)
		self.fort_num_score = DataParseHelper.ParseMSuffix(cfg.__fort_num_score)
		self.kill_score = cfg.__kill_score
		self.vicotry_score = cfg.__vicotry_score
		self.time = cfg.__time
end

--获取所有技能配置数据
function PVPDataHelper.GetFort(id)
    ClearDataManager:UseCache(CacheNames.fort, PVPDataHelper.ClearCache)
	local data = fortCache[id]
	if data == nil then
		local cfg = XMLManager.fort[id]
		if cfg ~= nil then
			data = FortCfg(cfg)
			fortCache[data.id] = data
		else
            LoggerHelper.Error("Fort id not exist:" .. id)
		end
	end
	return data
end

return PVPDataHelper