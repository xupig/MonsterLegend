-- FlashSaleDataHelper.lua
local FlashSaleDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

------------------------------限时抢购表------------------------------------
local FlashSaleCfgData = Class.FlashSaleCfgData(ClassTypes.XObject)

function FlashSaleCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.level = cfg.__level
	self.time = cfg.__time
    self.low_reward = DataParseHelper.ParseNSuffix(cfg.__low_reward)
    self.high_reward = DataParseHelper.ParseNSuffix(cfg.__high_reward)
end

local FlashSaleCfgDataCache = {}
function FlashSaleDataHelper.ClearCache()
    FlashSaleCfgDataCache = {}
end

function FlashSaleDataHelper.GetData(id)
    ClearDataManager:UseCache(CacheNames.flash_sale, FlashSaleDataHelper.ClearCache)
	local data = FlashSaleCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.flash_sale[id]
    if data~=nil then
	    data = FlashSaleCfgData(data) 
    end
	FlashSaleCfgDataCache[id] = data
	return data
end

function FlashSaleDataHelper.GetTime(id)
	local data = FlashSaleDataHelper.GetData(id)
	return data.time
end

return FlashSaleDataHelper