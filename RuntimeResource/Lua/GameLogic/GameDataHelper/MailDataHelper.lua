--MailDataHelper.lua
local MailDataHelper = {}
local XMLManager = GameManager.XMLManager
local MailCfgData = Class.MailCfgData(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager
local mailCache = {}
function MailDataHelper.ClearCache()
    mailCache = {}
end

function MailCfgData:__ctor__(cfg)
    self.id = cfg.__id
    self.from = cfg.__id
    self.title = cfg.__title
    self.text = cfg.__text
end

function MailCfgData:GetID()
    return self.id
end

function MailCfgData:GetFrom()
    return self.from or ""
end

function MailCfgData:GetTitle()
    return self.title or ""
end

function MailCfgData:GetText()
    return self.text or ""
end

function MailDataHelper.GetMailById(id)
    ClearDataManager:UseCache(CacheNames.mail, MailDataHelper.ClearCache)
    local data = mailCache[id]
    if data ~= nil then
        return data
    end

    data = XMLManager.mail[id]
    if data ~= nil then
        data = MailCfgData(data)
    else--data == nil
        LoggerHelper.Error("mail.xml does not exist id " .. id)
    end
    mailCache[id] = data
    return data
end

return MailDataHelper
