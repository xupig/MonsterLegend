-- FashionDataHelper.lua
local FashionDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

------------------------------时装表------------------------------------
local FashionCfgData = Class.FashionCfgData(ClassTypes.XObject)

function FashionCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.show_type = cfg.__show_type
	self.mode_vocation = cfg.__mode_vocation
	self.head_vocation = cfg.__head_vocation
	self.resolve = DataParseHelper.ParseMSuffix(cfg.__resolve)
	self.max_star = cfg.__max_star
	self.vocation = DataParseHelper.ParseListInt(cfg.__vocation)
	self.order = cfg.__order
	self.level_show = cfg.__level_show
end

local FashionCfgDataCache = {}
function FashionDataHelper.ClearCache()
    FashionCfgDataCache = {}
end

function FashionDataHelper.GetData(id)
    ClearDataManager:UseCache(CacheNames.fashion, FashionDataHelper.ClearCache)
	local data = FashionCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.fashion[id]
    if data~=nil then
	    data = FashionCfgData(data) 
    end
	FashionCfgDataCache[id] = data
	return data
end

function FashionDataHelper.GetFashionShowData(type, vocGroup, level)
	local result = {}
	local keys = XMLManager.fashion:Keys()
	for i=1,#keys do
		local flag = FashionDataHelper.GetFashionInVocationList(keys[i], vocGroup)
		if flag then
			local data = FashionDataHelper.GetData(keys[i])
			if data.type == type and level >= data.level_show then
				table.insert(result, {id=data.id, order=data.order})
			end
		end
	end
	for k,v in pairs(result) do
		table.sort(v, SortByOrder)
	end
	return result
end

function FashionDataHelper.GetFashionShowType(id)
	local data = FashionDataHelper.GetData(id)
	return data.show_type
end

function FashionDataHelper.GetFashionType(id)
	local data = FashionDataHelper.GetData(id)
	return data.type
end

function FashionDataHelper.GetFashionModel(id)
	local data = FashionDataHelper.GetData(id)
	return data.mode_vocation or 0
end

function FashionDataHelper.GetFashionHeadModel(id)
	local data = FashionDataHelper.GetData(id)
	return data.head_vocation or 0
end

function FashionDataHelper.GetFashionResolve(id)
	local data = FashionDataHelper.GetData(id)
	return data.resolve
end

function FashionDataHelper.GetFashionMaxStar(id)
	local data = FashionDataHelper.GetData(id)
	return data.max_star
end

function FashionDataHelper.GetFashionVocation(id)
	local data = FashionDataHelper.GetData(id)
	return data.vocation
end

function FashionDataHelper.GetFashionInVocationList(id, vocGroup)
    local data = FashionDataHelper.GetFashionVocation(id)
    if data == nil then
        return true
    end
    for k,v in pairs(data) do
        if v == vocGroup then
            return true
        end
    end
    return false
end


------------------------------时装星级表------------------------------------
local FashionStarCfgData = Class.FashionStarCfgData(ClassTypes.XObject)

function FashionStarCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.fashion_id = cfg.__fashion_id
	self.star = cfg.__star
	self.star_lvup_cost = DataParseHelper.ParseMSuffix(cfg.__star_lvup_cost)
	self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

local FashionStarCfgDataCache = {}
function FashionDataHelper.ClearStarCfgCache()
    FashionStarCfgDataCache = {}
end

function FashionDataHelper.GetStarData(id)
    ClearDataManager:UseCache(CacheNames.fashion_star, FashionDataHelper.ClearStarCfgCache)
	local data = FashionStarCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.fashion_star[id]
    if data~=nil then
	    data = FashionStarCfgData(data) 
    end
	FashionStarCfgDataCache[id] = data
	return data
end

local IdAndStarDataCache = nil
function FashionDataHelper.GetStarDataByIdAndStar(fashion_id, star)
	if IdAndStarDataCache == nil then
		IdAndStarDataCache = {}
		local keys = XMLManager.fashion_star:Keys()
		for i=1,#keys do
			local data = FashionDataHelper.GetStarData(keys[i])
			if IdAndStarDataCache[data.fashion_id] == nil then
				IdAndStarDataCache[data.fashion_id] = {}
			end
			IdAndStarDataCache[data.fashion_id][data.star] = data
		end
	end
	return IdAndStarDataCache[fashion_id][star]
end

function FashionDataHelper.GetStarAttri(fashion_id, star)
	local data = FashionDataHelper.GetStarDataByIdAndStar(fashion_id, star)
	return data.attri
end

function FashionDataHelper.GetStarAttriAndNextDelta(fashion_id, star)
	local curStar = math.max(star, 0)
	local nextStar = star + 1
	local data = FashionDataHelper.GetStarDataByIdAndStar(fashion_id, curStar)
	local nextData = FashionDataHelper.GetStarDataByIdAndStar(fashion_id, nextStar)
	local delta = 0
	local result = {}
	local value
	for k,v in pairs(data.attri) do
		local value = v
		if nextData ~= nil then
			if star < 0 then
				value = 0
				delta = v
			else
				delta = nextData.attri[k] - v
			end
		end
		table.insert(result, {k, value, delta})
	end
	return result
end



------------------------------时装等级表------------------------------------
local FashionLevelCfgData = Class.FashionLevelCfgData(ClassTypes.XObject)

function FashionLevelCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.level = cfg.__level
	self.attri_percent_up = DataParseHelper.ParseMSuffix(cfg.__attri_percent_up)
	self.levelup_cost = DataParseHelper.ParseMSuffix(cfg.__levelup_cost)
end

local FashionLevelCfgDataCache = {}
function FashionDataHelper.ClearLevelCfgCache()
    FashionLevelCfgDataCache = {}
end

function FashionDataHelper.GetLevelData(id)
    ClearDataManager:UseCache(CacheNames.fashion_level, FashionDataHelper.ClearLevelCfgCache)
	local data = FashionLevelCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.fashion_level[id]
    if data~=nil then
	    data = FashionLevelCfgData(data) 
    end
	FashionLevelCfgDataCache[id] = data
	return data
end

local TypeAndLevelDataCache = nil
function FashionDataHelper.GetLevelDataByTypeAndLevel(type, level)
	if TypeAndLevelDataCache == nil then
		TypeAndLevelDataCache = {}
		local keys = XMLManager.fashion_level:Keys()
		for i=1,#keys do
			local data = FashionDataHelper.GetLevelData(keys[i])
			if TypeAndLevelDataCache[data.type] == nil then
				TypeAndLevelDataCache[data.type] = {}
			end
			TypeAndLevelDataCache[data.type][data.level] = data
		end
	end
	return TypeAndLevelDataCache[type][level]
end

function FashionDataHelper.GetLevelAdditionData(type, level)
	local data = FashionDataHelper.GetLevelDataByTypeAndLevel(type, level)
	return data.attri_percent_up
end


return FashionDataHelper