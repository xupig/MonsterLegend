-- SpecialDialogDataHelper.lua
local SpecialDialogDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local SpecialDialog = Class.SpecialDialog(ClassTypes.XObject)

local specialDialogCache = {}
function SpecialDialogDataHelper.ClearCache()
    specialDialogCache = {}
end

function SpecialDialog:__ctor__(cfg)
    self.id = cfg.__id
    self.npc_id = cfg.__npc_id
    self.dialog = cfg.__dialog
    self.voice = cfg.__voice
    self.delay = cfg.__delay
    self.duration = cfg.__duration
    self.trigger_and = DataParseHelper.ParseNSuffix(cfg.__trigger_and)
    self.trigger_or = DataParseHelper.ParseNSuffix(cfg.__trigger_or)
end

function SpecialDialogDataHelper.GetSpecialDialog(id)
    ClearDataManager:UseCache(CacheNames.special_dialog, SpecialDialogDataHelper.ClearCache)
    local data = specialDialogCache[id]
    if data == nil then
        local cfg = XMLManager.special_dialog[id]
        data = SpecialDialog(cfg)
        specialDialogCache[data.id] = data
    end
    return data
end

function SpecialDialogDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.special_dialog:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function SpecialDialogDataHelper:GetNpcId(id)
    local data = self.GetSpecialDialog(id)
    return data and data.npc_id or nil
end

function SpecialDialogDataHelper:GetDialog(id)
    local data = self.GetSpecialDialog(id)
    return data and data.dialog or nil
end

function SpecialDialogDataHelper:GetVoice(id)
    local data = self.GetSpecialDialog(id)
    return data and data.voice or nil
end

function SpecialDialogDataHelper:GetDelay(id)
    local data = self.GetSpecialDialog(id)
    return data and data.delay or nil
end

function SpecialDialogDataHelper:GetDuration(id)
    local data = self.GetSpecialDialog(id)
    return data and data.duration or nil
end

function SpecialDialogDataHelper:GetTriggerAnd(id)
    local data = self.GetSpecialDialog(id)
    return data and data.trigger_and or nil
end

function SpecialDialogDataHelper:GetTriggerOr(id)
    local data = self.GetSpecialDialog(id)
    return data and data.trigger_or or nil
end

return SpecialDialogDataHelper
