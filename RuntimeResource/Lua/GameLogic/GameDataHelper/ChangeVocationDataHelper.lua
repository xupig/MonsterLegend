-- ChangeVocationDataHelper.lua
local ChangeVocationDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ChangeVocation = Class.ChangeVocation(ClassTypes.XObject)

local changeVocationCache = {}
function ChangeVocationDataHelper.ClearCache()
    changeVocationCache = {}
end

function ChangeVocation:__ctor__(cfg)
    self.id = cfg.__id
    self.change_num = cfg.__change_num
    self.stage = cfg.__stage
    self.task_id = cfg.__task_id
    self.stage_atrri_show = DataParseHelper.ParseMSuffixSort(cfg.__stage_atrri_show)
    self.ui_show = cfg.__ui_show
    -- LoggerHelper.Error("id ====" .. tostring(cfg.__id))
    -- LoggerHelper.Error("stage_atrri_show ====" .. tostring(cfg.__stage_atrri_show))
end

function ChangeVocationDataHelper.GetChangeVocation(id)
    ClearDataManager:UseCache(CacheNames.change_vocation, ChangeVocationDataHelper.ClearCache)
    local data = changeVocationCache[id]
    if data == nil then
        local cfg = XMLManager.change_vocation[id]
        if cfg ~= nil then
            data = ChangeVocation(cfg)
            changeVocationCache[data.id] = data
        else
            LoggerHelper.Error("ChangeVocation id not exist:" .. id)
        end
    end
    return data
end

function ChangeVocationDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.change_vocation:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ChangeVocationDataHelper:GetChangeNum(id)
    local data = self.GetChangeVocation(id)
    return data and data.change_num or nil
end

function ChangeVocationDataHelper:GetStage(id)
    local data = self.GetChangeVocation(id)
    return data and data.stage or nil
end

function ChangeVocationDataHelper:GetTaskId(id)
    local data = self.GetChangeVocation(id)
    return data and data.task_id or nil
end

function ChangeVocationDataHelper:GetUIShow(id)
    local data = self.GetChangeVocation(id)
    return data and data.ui_show or 0
end


function ChangeVocationDataHelper:GetStageAtrri(id)
    local data = self.GetChangeVocation(id)
    return data and data.stage_atrri_show or nil
end

function ChangeVocationDataHelper:GetIdByChangeNumAndStage(changeNum, stage)
    local data = XMLManager.change_vocation:Keys()
    for _, id in ipairs(data) do
        if ChangeVocationDataHelper:GetChangeNum(id) == changeNum and ChangeVocationDataHelper:GetStage(id) == stage then
            return id
        end
    end
    return nil
end

function ChangeVocationDataHelper:GetMaxStageByChangeNum(changeNum)
    local maxStage = 0
    local data = XMLManager.change_vocation:Keys()
    for _, id in ipairs(data) do
        if ChangeVocationDataHelper:GetChangeNum(id) == changeNum and ChangeVocationDataHelper:GetStage(id) > maxStage then
            maxStage = ChangeVocationDataHelper:GetStage(id)
        end
    end
    return maxStage
end

function ChangeVocationDataHelper:GetIdByTaskId(taskId)
    local ids = {}
    local data = XMLManager.change_vocation:Keys()
    for i, v in ipairs(data) do
        if self:GetTaskId(v) == taskId then
            return v
        end
    end
    return nil
end

function ChangeVocationDataHelper:GetIdsByTaskId(taskId)
    local ids = {}
    local data = XMLManager.change_vocation:Keys()
    local changeNum
    for i, v in ipairs(data) do
        if self:GetTaskId(v) == taskId then
            changeNum = self:GetChangeNum(v)
            break
        end
    end
    for _, id in ipairs(data) do
        if id~= 0 and ChangeVocationDataHelper:GetTaskId(id) ~= 0 and ChangeVocationDataHelper:GetChangeNum(id) == changeNum then
            table.insert(ids, id)
        end
    end
    return ids
end

function ChangeVocationDataHelper:GetAttriDatasByChangeNumAndStage(changeNum, stage)
    local lastId
    local maxStage = self:GetMaxStageByChangeNum(changeNum)
    local datas = {}
    local valueKeys = {}
    local data = XMLManager.change_vocation:Keys()
    for i, v in ipairs(data) do 
        local s = self:GetStage(v)
        if s~= 0 and s <= stage and self:GetChangeNum(v) == changeNum then
            local attriData = self:GetStageAtrri(v)
            if attriData ~= nil then
                if table.isEmpty(datas) then
                    for i,v1 in ipairs(attriData) do
                        valueKeys[v1[1]] = i
                        table.insert(datas, {v1[1], v1[2]})
                    end
                else
                    for i,v2 in ipairs(attriData) do
                        local num = datas[valueKeys[v2[1]]][2]
                        datas[valueKeys[v2[1]]] = {v2[1], v2[2] + num}
                    end
                end
            end
        end
        lastId = v
    end
    if table.isEmpty(datas) then
        return self:GetBaseAttriData(changeNum)
    else
        if stage > maxStage then
            local attriData = self:GetStageAtrri(lastId + 1)
            if attriData ~= nil then
                for i,v3 in ipairs(attriData) do
                    datas[valueKeys[v3[1]]][2] = datas[valueKeys[v3[1]]][2] + v3[2]
                end
            end
        end
    end
    return datas
end

function ChangeVocationDataHelper:GetBaseAttriData(changeNum)
    local datas = {}
    local data = XMLManager.change_vocation:Keys()
    for i, v in ipairs(data) do
        if self:GetChangeNum(v) == changeNum and self:GetStageAtrri(v) ~= nil then
            for i,v in ipairs(self:GetStageAtrri(v)) do
                table.insert(datas, {v[1],0})
            end
            return datas
        end
    end
    return datas
end

return ChangeVocationDataHelper
