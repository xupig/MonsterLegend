-- ChangeVocationNum4DataHelper.lua
local ChangeVocationNum4DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ChangeVocationNum4 = Class.ChangeVocationNum4(ClassTypes.XObject)

local changeVocationNum4Cache = {}
function ChangeVocationNum4DataHelper.ClearCache()
    changeVocationNum4Cache = {}
end

function ChangeVocationNum4:__ctor__(cfg)
    self.id = cfg.__id
    self.stage = cfg.__stage
    self.desc_id = cfg.__desc_id
    self.icon_id = cfg.__icon_id
    self.item_need = DataParseHelper.ParseMSuffixSort(cfg.__item_need)
    self.exp_need = DataParseHelper.ParseMSuffixSort(cfg.__exp_need)
    self.atrri = DataParseHelper.ParseMSuffixSort(cfg.__atrri)
end

function ChangeVocationNum4DataHelper.GetChangeVocationNum4(id)
    ClearDataManager:UseCache(CacheNames.change_vocation_num_4, ChangeVocationNum4DataHelper.ClearCache)
    local data = changeVocationNum4Cache[id]
    if data == nil then
        local cfg = XMLManager.change_vocation_num_4[id]
        if cfg ~= nil then
            data = ChangeVocationNum4(cfg)
            changeVocationNum4Cache[data.id] = data
        else
            LoggerHelper.Error("ChangeVocationNum4 id not exist:" .. id)
        end
    end
    return data
end

function ChangeVocationNum4DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.change_vocation_num_4:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ChangeVocationNum4DataHelper:GetStage(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.stage or nil
end

function ChangeVocationNum4DataHelper:GetDescId(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.desc_id or nil
end

function ChangeVocationNum4DataHelper:GetIconId(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.icon_id or nil
end

function ChangeVocationNum4DataHelper:GetItemNeed(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.item_need or nil
end

function ChangeVocationNum4DataHelper:GetExpNeed(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.exp_need or nil
end

function ChangeVocationNum4DataHelper:GetAtrri(id)
    local data = self.GetChangeVocationNum4(id)
    return data and data.atrri or nil
end

--获取id之前的属性之和
function ChangeVocationNum4DataHelper:GetAllAttriBeforId(id)
    local datas = {}
    local keys = {}
    for i=1,id do
        local attrs = self:GetAtrri(i)
        for _,v in pairs(attrs) do
            if keys[v[1]] == nil then
                keys[v[1]] = #datas + 1
                table.insert(datas, {v[1], v[2]})
            else
                datas[keys[v[1]]][2] = datas[keys[v[1]]][2] + v[2]
            end
        end
    end
    return datas
end

return ChangeVocationNum4DataHelper
