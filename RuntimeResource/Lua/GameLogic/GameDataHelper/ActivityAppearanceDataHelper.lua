-- ActivityAppearanceDataHelper.lua
local ActivityAppearanceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ActivityAppearance = Class.ActivityAppearance(ClassTypes.XObject)

local activityAppearanceCache = {}
function ActivityAppearanceDataHelper.ClearCache()
    activityAppearanceCache = {}
end

function ActivityAppearance:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.icon = cfg.__icon
    self.model = cfg.__model
end

function ActivityAppearanceDataHelper.GetActivityAppearance(id)
    ClearDataManager:UseCache(CacheNames.activity_appearance, ActivityAppearanceDataHelper.ClearCache)
    local data = activityAppearanceCache[id]
    if data == nil then
        local cfg = XMLManager.activity_appearance[id]
        if cfg ~= nil then
            data = ActivityAppearance(cfg)
            activityAppearanceCache[data.id] = data
        else
            LoggerHelper.Error("ActivityAppearance id not exist:" .. id)
        end
    end
    return data
end

function ActivityAppearanceDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity_appearance:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityAppearanceDataHelper:GetLevel(id)
    local data = self.GetActivityAppearance(id)
    return data and data.level or nil
end

function ActivityAppearanceDataHelper:GetIcon(id)
    local data = self.GetActivityAppearance(id)
    return data and data.icon or nil
end

function ActivityAppearanceDataHelper:GetModel(id)
    local data = self.GetActivityAppearance(id)
    return data and data.model or nil
end

function ActivityAppearanceDataHelper:GetAppearanceIdByLevel(level)
    local data = XMLManager.activity_appearance:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local lv = self:GetLevel(v)
            local nextLv = self:GetLevel(v + 1)
            if level >= lv and (nextLv == nil or nextLv > level) then
                return v
            end
        end
    end
end

return ActivityAppearanceDataHelper
