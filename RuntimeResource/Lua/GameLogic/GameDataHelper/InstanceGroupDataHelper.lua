-- InstanceGroupDataHelper.lua
local InstanceGroupDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local InstanceGroup = Class.InstanceGroup(ClassTypes.XObject)

local instanceGroupCache = {}
function InstanceGroupDataHelper.ClearCache()
    instanceGroupCache = {}
end

function InstanceGroup:__ctor__(cfg)
    self.id = cfg.__id
    self.group_id = cfg.__group_id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.describe = cfg.__describe
    self.group_pic = cfg.__group_pic
    self.instance_id = DataParseHelper.ParseListInt(cfg.__instance_id)
end

function InstanceGroupDataHelper.GetInstanceGroup(id)
    ClearDataManager:UseCache(CacheNames.instance_group, InstanceGroupDataHelper.ClearCache)
    local data = instanceGroupCache[id]
    if data == nil then
        local cfg = XMLManager.instance_group[id]
        if cfg ~= nil then
            data = InstanceGroup(cfg)
            instanceGroupCache[data.id] = data
        else
            LoggerHelper.Error("InstanceGroup id not exist:" .. id)
        end
    end
    return data
end

function InstanceGroupDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.instance_group:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function InstanceGroupDataHelper:GetIdByInstanceId(id)
    local ids = self:GetAllId()
    for i, v in ipairs(ids) do
        local instanceIds = self:GetInstanceId(v)
        for j, iid in ipairs(instanceIds) do
            if iid == id then
                return v
            end
        end
    end
    return nil
end

function InstanceGroupDataHelper:GetGroupId(id)
    local data = self.GetInstanceGroup(id)
    return data and data.group_id or nil
end

function InstanceGroupDataHelper:GetName(id)
    local data = self.GetInstanceGroup(id)
    return data and data.name or nil
end

function InstanceGroupDataHelper:GetIcon(id)
    local data = self.GetInstanceGroup(id)
    return data and data.icon or nil
end

function InstanceGroupDataHelper:GetDescribe(id)
    local data = self.GetInstanceGroup(id)
    return data and data.describe or nil
end

function InstanceGroupDataHelper:GetGroupPic(id)
    local data = self.GetInstanceGroup(id)
    return data and data.group_pic or nil
end

function InstanceGroupDataHelper:GetInstanceId(id)
    local data = self.GetInstanceGroup(id)
    return data and data.instance_id or nil
end

return InstanceGroupDataHelper
