-- WelfareRankWayDataHelper.lua
local WelfareRankWayDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareRankWay = Class.WelfareRankWay(ClassTypes.XObject)

local welfareRankWayCache = {}
function WelfareRankWayDataHelper.ClearCache()
    welfareRankWayCache = {}
end

function WelfareRankWay:__ctor__(cfg)
    self.id = cfg.__id
    self.follow = DataParseHelper.ParseNSuffixSort(cfg.__follow)
    self.desc_1 = cfg.__desc_1
    self.desc_2 = cfg.__desc_2
    self.desc_3 = cfg.__desc_3
end

function WelfareRankWayDataHelper.GetWelfareRankWay(id)
    ClearDataManager:UseCache(CacheNames.welfare_rank_way, WelfareRankWayDataHelper.ClearCache)
    local data = welfareRankWayCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_rank_way[id]
        if cfg ~= nil then
            data = WelfareRankWay(cfg)
            welfareRankWayCache[data.id] = data
        else
            LoggerHelper.Error("WelfareRankWay id not exist:" .. id)
        end
    end
    return data
end

function WelfareRankWayDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_rank_way:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareRankWayDataHelper:GetFollow(id)
    local data = self.GetWelfareRankWay(id)
    return data and data.follow or nil
end

function WelfareRankWayDataHelper:GetDesc1(id)
    local data = self.GetWelfareRankWay(id)
    return data and data.desc_1 or nil
end

function WelfareRankWayDataHelper:GetDesc2(id)
    local data = self.GetWelfareRankWay(id)
    return data and data.desc_2 or nil
end

function WelfareRankWayDataHelper:GetDesc3(id)
    local data = self.GetWelfareRankWay(id)
    return data and data.desc_3 or nil
end

return WelfareRankWayDataHelper
