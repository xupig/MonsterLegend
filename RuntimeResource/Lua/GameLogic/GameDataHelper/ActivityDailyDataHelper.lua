-- ActivityDailyDataHelper.lua
local ActivityDailyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ActivityDaily = Class.ActivityDaily(ClassTypes.XObject)

local activityDailyCache = {}
function ActivityDailyDataHelper.ClearCache()
    activityDailyCache = {}
end

function ActivityDaily:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.type = cfg.__type
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.desc_tip_time = cfg.__desc_tip_time
    self.desc_data = cfg.__desc_data
    self.desc_level = cfg.__desc_level
    self.times = cfg.__times
    self.reward_point = cfg.__reward_point
    self.reward_preview = DataParseHelper.ParseNSuffixSort(cfg.__reward_preview)
    self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
    self.func = DataParseHelper.ParseNSuffixSort(cfg.__func, true)
    self.rank = cfg.__rank
    self.open_level = cfg.__open_level
    self.open_world_level = cfg.__open_world_level
    self.day_limit = DataParseHelper.ParseListInt(cfg.__day_limit) or {}
    self.time_limit = DataParseHelper.ParseListInt(cfg.__time_limit) or {}
    self.perfect_back_cost = DataParseHelper.ParseMSuffix(cfg.__perfect_back_cost)
    self.perfect_back_extra_cost = DataParseHelper.ParseMSuffix(cfg.__perfect_back_extra_cost)
    self.normal_back_cost = DataParseHelper.ParseMSuffix(cfg.__normal_back_cost)
    self.function_params = DataParseHelper.ParseMSuffix(cfg.__function_params, true)
    self.reward_back_rank = cfg.__reward_back_rank
    self.panel_open_id = cfg.__panel_open_id
    self.reward_show = DataParseHelper.ParseListInt(cfg.__reward_show) or {}
    self.limit_act_show = cfg.__limit_act_show
end

function ActivityDailyDataHelper.GetActivityDaily(id)
    ClearDataManager:UseCache(CacheNames.activity_daily, ActivityDailyDataHelper.ClearCache)
    local data = activityDailyCache[id]
    if data == nil then
        local cfg = XMLManager.activity_daily[id]
        if cfg ~= nil then
            data = ActivityDaily(cfg)
            activityDailyCache[data.id] = data
        else
            LoggerHelper.Error("ActivityDaily id not exist:" .. id)
        end
    end
    return data
end

function ActivityDailyDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity_daily:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityDailyDataHelper:GetAllIdByType(type)
    local ids = {}
    local data = XMLManager.activity_daily:Keys()
    for i, v in ipairs(data) do
        local t = self:GetType(v)
        if v ~= 0 and t == type then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityDailyDataHelper:GetName(id)
    local data = self.GetActivityDaily(id)
    return data and data.name or nil
end

function ActivityDailyDataHelper:GetType(id)
    local data = self.GetActivityDaily(id)
    return data and data.type or nil
end

function ActivityDailyDataHelper:GetIcon(id)
    local data = self.GetActivityDaily(id)
    return data and data.icon or nil
end

function ActivityDailyDataHelper:GetDesc(id)
    local data = self.GetActivityDaily(id)
    return data and data.desc or nil
end

function ActivityDailyDataHelper:GetDescTipTime(id)
    local data = self.GetActivityDaily(id)
    return data and data.desc_tip_time or nil
end

function ActivityDailyDataHelper:GetDescData(id)
    local data = self.GetActivityDaily(id)
    return data and data.desc_data or nil
end

function ActivityDailyDataHelper:GetDescLevel(id)
    local data = self.GetActivityDaily(id)
    return data and data.desc_level or nil
end

function ActivityDailyDataHelper:GetTimes(id)
    local data = self.GetActivityDaily(id)
    return data and data.times or nil
end

function ActivityDailyDataHelper:GetRewardPoint(id)
    local data = self.GetActivityDaily(id)
    return data and data.reward_point or nil
end

function ActivityDailyDataHelper:GetRewardPreview(id)
    local data = self.GetActivityDaily(id)
    return data and data.reward_preview or nil
end

function ActivityDailyDataHelper:GetFollow(id)
    local data = self.GetActivityDaily(id)
    return data and data.follow or nil
end

function ActivityDailyDataHelper:GetFunc(id)
    local data = self.GetActivityDaily(id)
    return data and data.func or nil
end

function ActivityDailyDataHelper:GetRank(id)
    local data = self.GetActivityDaily(id)
    return data and data.rank or nil
end

function ActivityDailyDataHelper:GetOpenLevel(id)
    local data = self.GetActivityDaily(id)
    return data and data.open_level or nil
end

function ActivityDailyDataHelper:GetOpenWorldLevel(id)
    local data = self.GetActivityDaily(id)
    return data and data.open_world_level or nil
end

function ActivityDailyDataHelper:GetDayLimit(id)
    local data = self.GetActivityDaily(id)
    return data and data.day_limit or {}
end

function ActivityDailyDataHelper:GetTimeLimit(id)
    local data = self.GetActivityDaily(id)
    return data and data.time_limit or {}
end

--限时活动配置,功能开启表ID
function ActivityDailyDataHelper:GetPanelOpenId(id)
    local data = self.GetActivityDaily(id)
    return data and data.panel_open_id
end

--获取资源找回排序（值越大排越前）
function ActivityDailyDataHelper:GetRewardBackRank(id)
    local data = self.GetActivityDaily(id)
    return data and data.reward_back_rank or {}
end

--钻石找回消耗（基础次数花费）
function ActivityDailyDataHelper:GetPerfectBackCost(id)
    local data = self.GetActivityDaily(id)
    return data and data.perfect_back_cost or {}
end

--钻石找回消耗（额外次数花费）
function ActivityDailyDataHelper:GetPerfectBackExtraCost(id)
    local data = self.GetActivityDaily(id)
    return data and data.perfect_back_extra_cost or {}
end

--金币找回消耗
function ActivityDailyDataHelper:GetNormalBackCost(id)
    local data = self.GetActivityDaily(id)
    return data and data.normal_back_cost or nil
end

--日常活动查询函数定义
-- DailyActivityConfig.IS_OPEN_IN_OPENTIME = 1     --在开启时间内，此时是否能进入
function ActivityDailyDataHelper:GetFunctionParams(id)
    local data = self.GetActivityDaily(id)
    return data and data.function_params or nil
end

function ActivityDailyDataHelper:GetRewardShowList(id)
    local data = self.GetActivityDaily(id)
    return data and data.reward_show or {}
end

function ActivityDailyDataHelper:GetOpenTipsShow(id)
    local data = self.GetActivityDaily(id)
    return data and data.limit_act_show or 0
end


return ActivityDailyDataHelper
