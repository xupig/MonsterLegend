-- BossOfferDataHelper.lua
local BossOfferDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local BossOffer = Class.BossOffer(ClassTypes.XObject)

local bossOfferCache = {}

function BossOffer:__ctor__(cfg)
    self.id = cfg.__id
    self.icon = DataParseHelper.ParseNSuffix(cfg.__icon)
    self.targets = DataParseHelper.ParseMSuffix(cfg.__targets)
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
    self.name = cfg.__name
end

function BossOfferDataHelper.GetBossOffer(id)
    local data = bossOfferCache[id]
    if data == nil then
        local cfg = XMLManager.boss_offer[id]
        if cfg ~= nil then
            data = BossOffer(cfg)
            bossOfferCache[data.id] = data
        else
            LoggerHelper.Error("BossOffer id not exist:" .. id)
        end
    end
    return data
end

function BossOfferDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.boss_offer:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function BossOfferDataHelper:GetIcon(id)
    local data = self.GetBossOffer(id)
    return data and data.icon or nil
end

function BossOfferDataHelper:GetTargets(id)
    local data = self.GetBossOffer(id)
    return data and data.targets or nil
end

function BossOfferDataHelper:GetReward(id)
    local data = self.GetBossOffer(id)
    return data and data.reward or nil
end

function BossOfferDataHelper:GetName(id)
    local data = self.GetBossOffer(id)
    return data and data.name or nil
end

return BossOfferDataHelper
