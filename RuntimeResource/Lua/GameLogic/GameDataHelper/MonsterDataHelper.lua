-- MonsterDataHelper.lua
MonsterDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper
local attri_config = GameWorld.attri_config

local MonsterData = Class.MonsterData(ClassTypes.XObject)
local MonsterValue = Class.MonsterValue(ClassTypes.XObject)
local MonsterEndure = Class.MonsterEndure(ClassTypes.XObject)

function MonsterData:__ctor__(cfg)
	self.id = cfg.__id
	self.type = cfg.__type
	self.icon = cfg.__icon
	self.is_part_judge = cfg.__is_part_judge
	self.lv = cfg.__lv
	self.model_id = cfg.__model_id
	self.ai_id = cfg.__ai_id
	self.skill_ids = DataParseHelper.ParseListInt(cfg.__skill_ids)
	self.skill_buff = DataParseHelper.ParseNSuffix(cfg.__skill_buff)
	self.property_id = cfg.__property_id
	self.endure_id = cfg.__endure_id
	self.base_speed = DataParseHelper.ParseListInt(cfg.__base_speed)
	self.born_face = cfg.__born_face
	self.be_hit_radius = cfg.__be_hit_radius
	self.be_hit_height = cfg.__be_hit_height
	self.build_type = cfg.__build_type
	self.monster_type = cfg.__monster_type
	self.monster_name = cfg.__monster_name
	self.model_scale = cfg.__model_scale
	self.off_play_args = DataParseHelper.ParseListInt(cfg.__break_up_action_ai)
	self.hp_num = cfg.__hp_num
	self.angry = cfg.__angry
	self.level_type = cfg.__level_type
end

function MonsterValue:__ctor__(cfg)
	self.id = cfg.__id
	self.max_hp = cfg.__max_hp
	self.hp = cfg.__hp
	self.phy_atk_base = cfg.__phy_atk_base
	self.mag_atk_base = cfg.__mag_atk_base
	self.phy_def_base = cfg.__phy_def_base
	self.mag_def_base = cfg.__mag_def_base
	self.hit_rate = cfg.__hit_rate
	self.miss_rate = cfg.__miss_rate
	self.broken_atk_rate = cfg.__broken_atk_rate
	self.crit = cfg.__crit
	self.anti_crit = cfg.__anti_crit
	self.extra_crit_rate = cfg.__extra_crit_rate
	self.extra_anti_crit_rate = cfg.__extra_anti_crit_rate
	self.crit_atk_rate = cfg.__crit_atk_rate
	self.phy_def_penetration = cfg.__phy_def_penetration
	self.extra_phy_def_reduce_rate = cfg.__extra_phy_def_reduce_rate
	self.extra_phy_def_penetration_rate = cfg.__extra_phy_def_penetration_rate
	self.mag_def_penetration = cfg.__mag_def_penetration
	self.extra_mag_def_reduce_rate = cfg.__extra_mag_def_reduce_rate
	self.extra_mag_def_penetration_rate = cfg.__extra_mag_def_penetration_rate
	self.slashing_property = cfg.__slashing_property
	self.piercing_property = cfg.__piercing_property
	self.blow_property = cfg.__blow_property
	self.all_phy_property = cfg.__all_phy_property
	self.slashing_property_defense = cfg.__slashing_property_defense
	self.piercing_property_defense = cfg.__piercing_property_defense
	self.blow_property_defense = cfg.__blow_property_defense
	self.all_phy_property_defense = cfg.__all_phy_property_defense
	self.fire_property = cfg.__fire_property
	self.ice_property = cfg.__ice_property
	self.light_property = cfg.__light_property
	self.dark_property = cfg.__dark_property
	self.all_mag_property = cfg.__all_mag_property
	self.fire_property_defense = cfg.__fire_property_defense
	self.ice_property_defense = cfg.__ice_property_defense
	self.light_property_defense = cfg.__light_property_defense
	self.dark_property_defense = cfg.__dark_property_defense
	self.all_mag_property_defense = cfg.__all_mag_property_defense
	self.float_atk = cfg.__float_atk
	self.damage_reduce = cfg.__damage_reduce
	self.hard = cfg.__hard
	self.hard_res = cfg.__hard_res
	self.hp_recover_base = cfg.__hp_recover_base

	self.attris = {} --用属性ID保存一份，方便使用
	local pbHeadNameMap = AttriDataHelper.GetPBHeadNameMap()
	for attriId, names in pairs(pbHeadNameMap) do
		local value = cfg[names[2]]
		if value ~= nil then 
			self[names[1]] = value
			self.attris[attriId] = value
		end
	end
	self.attris[attri_config.ATTRI_ID_MAX_HP] = self.max_hp
	self.attris[attri_config.ATTRI_ID_CUR_HP] = self.max_hp
end

function MonsterEndure:__ctor__(cfg)
	self.id = cfg.__id
	self.endure_max = cfg.__endure_max
	self.endure_recovery_time = cfg.__endure_recovery_time
	self.endure_recovery_speed = cfg.__endure_recovery_speed
	self.endure_empty_time = cfg.__endure_empty_time
	self.endure_recovery = cfg.__endure_recovery
	self.endure_buff = DataParseHelper.ParseNSuffix(cfg.__endure_buff)
	self.unendure_buff = DataParseHelper.ParseNSuffix(cfg.__unendure_buff)
end

local monsterCache = {}
local valueCache = {}
local endureCache= {}

function MonsterDataHelper.GetMonster(id)
	if id == nil then
		LoggerHelper.Error("MonsterDataHelper id is nil")
		return nil
	end

	local data = monsterCache[id]
	if data == nil then
		local cfg = XMLManager.monster[id]
		if cfg ~= nil then
			data = MonsterData(cfg)
			monsterCache[data.id] = data
		else
			LoggerHelper.Error("MonsterDataHelper id not exist:" .. id)
		end
	end
	return data
end

function MonsterDataHelper.GetMonsterValue(id)
	local data = valueCache[id]
	if data == nil then
		local cfg = XMLManager.monster_value[id]
		data = MonsterValue(cfg)
		valueCache[data.id] = data
	end
	return data
end

function MonsterDataHelper.GetMonsterEndure(id)
	local data = endureCache[id]
	if data == nil then
		local cfg = XMLManager.monster_endure[id]
		data = MonsterEndure(cfg)
		endureCache[data.id] = data
	end
	return data
end

function MonsterDataHelper.GetMonsterSkills(id)
    local data = MonsterDataHelper.GetMonster(id)
	if (data~=nil) then 
        return data.skill_ids
    end
	return nil
end

function MonsterDataHelper.GetMonsterSpeed(id)
    local data = MonsterDataHelper.GetMonster(id)
	if (data~=nil) then 
		local b = data.base_speed
        return b[1]
    end
	return 0
end

function MonsterDataHelper.GetMonsterModel(id)
	local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.model_id
	end
	return 0
end

function MonsterDataHelper.GetMonsterScale(id)
	local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.model_scale * 0.001
	end
	return 1
end

function MonsterDataHelper.GetMonsterAI(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.ai_id
	end
	return 0
end

function MonsterDataHelper.GetMonsterBuildType(id)
	local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.build_type or 0
	end
	return 0
end

function MonsterDataHelper.GetMonsterHP(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		local pid = data.property_id
		local prop = MonsterDataHelper.GetMonsterValue(pid)
		return prop.hp
	end
	return 0
end

function MonsterDataHelper.GetMonsterBuffs(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		local b = data.skill_buff
		return b
	end
	return {}
end

function MonsterDataHelper.GetIsPartJudge(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.is_part_judge
	end
	return 0
end

function MonsterDataHelper.GetBeHitRadius(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.be_hit_radius
	end
	return 0
end

function MonsterDataHelper.GetBeHitHeight(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.be_hit_height
	end
	return 0
end

function MonsterDataHelper.GetLv(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.lv
	end
	return 0
end

function MonsterDataHelper.GetMonsterType(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.monster_type
	end
	return 0
end

function MonsterDataHelper.GetIcon(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.icon
	end
	return 0
end

function MonsterDataHelper.GetMonsterName(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return LanguageDataHelper.GetLanguageData(data.monster_name)
	end
	return ""
end

function MonsterDataHelper.GetMonsterHPNum(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.hp_num or 1
	end
	return 1
end

function MonsterDataHelper.GetEndureId(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		return data.endure_id
	end
	return 0
end

--获取怪物霸体表数据
function MonsterDataHelper:GetEndureEmptyTime(id)
	local endure_id = MonsterDataHelper.GetEndureId(id)
	local data = MonsterDataHelper.GetMonsterEndure(endure_id)
	if data ~= nil then
		return data.endure_empty_time
	end
	return 0
end

function MonsterDataHelper:GetEndureRecovery(id)
	local endure_id = MonsterDataHelper.GetEndureId(id)
	local data = MonsterDataHelper.GetMonsterEndure(endure_id)
	if data ~= nil then
		return data.endure_recovery
	end
	return 0
end

function MonsterDataHelper.GetMonsterAttris(id)
	local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil then
		local pid = data.property_id
		local propData = MonsterDataHelper.GetMonsterValue(pid)
		return propData.attris
	end
	return {}
end

function MonsterDataHelper.GetMonsterOffPlayArgs(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil  then 
        return data.off_play_args
    end
	return {}
end

function MonsterDataHelper.GetMonsterAngry(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil  then 
        return data.angry or 0
    end
	return 0
end

function MonsterDataHelper.GetMonsterLevelType(id)
    local data = MonsterDataHelper.GetMonster(id)
	if data ~= nil  then 
        return data.level_type or 0
    end
	return 0
end

return MonsterDataHelper
