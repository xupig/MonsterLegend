-- BloodDungeonDataHelper.lua
local BloodDungeonDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local BloodDungeon = Class.BloodDungeon(ClassTypes.XObject)

local bloodDungeonCache = {}
function BloodDungeonDataHelper.ClearCache()
    bloodDungeonCache = {}
end

function BloodDungeon:__ctor__(cfg)
    self.id = cfg.__id
    self.desc = cfg.__desc
    self.sence_id = cfg.__sence_id
    self.level = cfg.__level
    self.icon = cfg.__icon
    self.commend = cfg.__commend
    self.award = DataParseHelper.ParseNSuffix(cfg.__award)
    self.dungeon_desc = DataParseHelper.ParseNSuffix(cfg.__dungeon_desc)
    self.ride = DataParseHelper.ParseMSuffix(cfg.__ride)
end

function BloodDungeonDataHelper.GetBloodDungeon(id)
    ClearDataManager:UseCache(CacheNames.blood_dungeon, BloodDungeonDataHelper.ClearCache)
    local data = bloodDungeonCache[id]
    if data == nil then
        local cfg = XMLManager.blood_dungeon[id]
        if cfg ~= nil then
            data = BloodDungeon(cfg)
            bloodDungeonCache[data.id] = data
        else
            LoggerHelper.Error("BloodDungeon id not exist:" .. id)
        end
    end
    return data
end

function BloodDungeonDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.blood_dungeon:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function BloodDungeonDataHelper:GetDesc(id)
    local data = self.GetBloodDungeon(id)
    return data and data.desc or nil
end

function BloodDungeonDataHelper:GetSenceId(id)
    local data = self.GetBloodDungeon(id)
    return data and data.sence_id or nil
end

function BloodDungeonDataHelper:GetIdByMapId(mapId)
    local data = XMLManager.blood_dungeon:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetSenceId(v) == mapId then
            return v
        end
    end
    return nil
end

function BloodDungeonDataHelper:GetLevel(id)
    local data = self.GetBloodDungeon(id)
    return data and data.level or nil
end

function BloodDungeonDataHelper:GetCommend(id)
    local data = self.GetBloodDungeon(id)
    return data and data.commend or nil
end

function BloodDungeonDataHelper:GetIcon(id)
    local data = self.GetBloodDungeon(id)
    return data and data.icon or nil
end

function BloodDungeonDataHelper:GetAward(id)
    local data = self.GetBloodDungeon(id)
    return data and data.award or nil
end

function BloodDungeonDataHelper:GetDungeonDesc(id)
    local data = self.GetBloodDungeon(id)
    return data and data.dungeon_desc or nil
end

function BloodDungeonDataHelper:GetRide(id)
    local data = self.GetBloodDungeon(id)
    return data and data.ride or nil
end

return BloodDungeonDataHelper
