-- RuneShowDataHelper.lua
local RuneShowDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local RuneShow = Class.RuneShow(ClassTypes.XObject)

local runeShowCache = {}
function RuneShowDataHelper.ClearCache()
    runeShowCache = {}
end

function RuneShow:__ctor__(cfg)
    self.id = cfg.__id
    self.rune_id = cfg.__rune_id
    self.unlock_floor = cfg.__unlock_floor
end

function RuneShowDataHelper.GetRuneShow(id)
    ClearDataManager:UseCache(CacheNames.rune_show, RuneShowDataHelper.ClearCache)
    local data = runeShowCache[id]
    if data == nil then
        local cfg = XMLManager.rune_show[id]
        if cfg ~= nil then
            data = RuneShow(cfg)
            runeShowCache[data.id] = data
        else
            LoggerHelper.Error("RuneShow id not exist:" .. id)
        end
    end
    return data
end

function RuneShowDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.rune_show:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function RuneShowDataHelper:GetRuneId(id)
    local data = self.GetRuneShow(id)
    return data and data.rune_id or nil
end

function RuneShowDataHelper:GetUnlockFloor(id)
    local data = self.GetRuneShow(id)
    return data and data.unlock_floor or nil
end

return RuneShowDataHelper
