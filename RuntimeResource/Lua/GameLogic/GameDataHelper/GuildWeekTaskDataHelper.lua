-- GuildWeekTaskDataHelper.lua
local GuildWeekTaskDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GuildWeekTask = Class.GuildWeekTask(ClassTypes.XObject)

local guildWeekTaskCache = {}
function GuildWeekTaskDataHelper.ClearCache()
    guildWeekTaskCache = {}
end

function GuildWeekTask:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.desc = cfg.__desc
    self.commit_npc = cfg.__commit_npc
    self.commit_dialog = DataParseHelper.ParseNSuffixSort(cfg.__commit_dialog)
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.reward_rate = cfg.__reward_rate
    self.targets_desc = DataParseHelper.ParseMSuffix(cfg.__targets_desc)
    self.little_shoes_open = cfg.__little_shoes_open
    self.little_shoes_open_start = cfg.__little_shoes_open_start
    self.little_shoes_open_end = cfg.__little_shoes_open_end
end

function GuildWeekTaskDataHelper.GetGuildWeekTask(id)
    ClearDataManager:UseCache(CacheNames.guild_week_task, GuildWeekTaskDataHelper.ClearCache)
    local data = guildWeekTaskCache[id]
    if data == nil then
        local cfg = XMLManager.guild_week_task[id]
        if cfg ~= nil then
            data = GuildWeekTask(cfg)
            guildWeekTaskCache[data.id] = data
        else
            LoggerHelper.Error("GuildWeekTask id not exist:" .. id)
        end
    end
    return data
end

function GuildWeekTaskDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.guild_week_task:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GuildWeekTaskDataHelper:GetName(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.name or nil
end

function GuildWeekTaskDataHelper:GetDesc(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.desc or nil
end

function GuildWeekTaskDataHelper:GetCommitNpc(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.commit_npc or nil
end

function GuildWeekTaskDataHelper:GetCommitDialog(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.commit_dialog or nil
end

function GuildWeekTaskDataHelper:GetTargets(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.targets or nil
end

function GuildWeekTaskDataHelper:GetRewardRate(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.reward_rate or nil
end

function GuildWeekTaskDataHelper:GetTargetsDesc(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.targets_desc or nil
end

function GuildWeekTaskDataHelper:GetLittleShoesOpen(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.little_shoes_open or 0
end

function GuildWeekTaskDataHelper:GetLittleShoesOpenStart(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.little_shoes_open_start or 0
end

function GuildWeekTaskDataHelper:GetLittleShoesOpenEnd(id)
    local data = self.GetGuildWeekTask(id)
    return data and data.little_shoes_open_end or 0
end


return GuildWeekTaskDataHelper
