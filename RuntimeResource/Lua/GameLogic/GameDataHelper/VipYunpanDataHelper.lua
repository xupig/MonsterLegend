-- VipYunpanDataHelper.lua
local VipYunpanDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local VipYunpan = Class.VipYunpan(ClassTypes.XObject)

local vipYunpanCache = {}
function VipYunpanDataHelper.ClearCache()
    vipYunpanCache = {}
end

function VipYunpan:__ctor__(cfg)
    self.id = cfg.__id
    self.num = cfg.__num
    self.vip = cfg.__vip
    self.time = cfg.__time
    self.end_time = cfg.__end_time
end

function VipYunpanDataHelper.GetVipYunpan(id)
    ClearDataManager:UseCache(CacheNames.vip_yunpan, VipYunpanDataHelper.ClearCache)
    local data = vipYunpanCache[id]
    if data == nil then
        local cfg = XMLManager.vip_yunpan[id]
        if cfg ~= nil then
            data = VipYunpan(cfg)
            vipYunpanCache[data.id] = data
        else
            LoggerHelper.Error("VipYunpan id not exist:" .. id)
        end
    end
    return data
end

function VipYunpanDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.vip_yunpan:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function VipYunpanDataHelper:GetNum(id)
    local data = self.GetVipYunpan(id)
    return data and data.num or nil
end

function VipYunpanDataHelper:GetVip(id)
    local data = self.GetVipYunpan(id)
    return data and data.vip or nil
end

function VipYunpanDataHelper:GetTime(id)
    local data = self.GetVipYunpan(id)
    return data and data.time or nil
end

function VipYunpanDataHelper:GetEndTime(id)
    local data = self.GetVipYunpan(id)
    return data and data.end_time or nil
end

function VipYunpanDataHelper:GetIdByVip(vip)
    local data = XMLManager.vip_yunpan:Keys()
    local x=0
    for i, v in ipairs(data) do
        if v ~= 0 then
            if VipYunpanDataHelper:GetVip(v)==vip then
                return v
            end
        end
        x=x+1
    end
    return data[x]
end

function VipYunpanDataHelper:GetNumByVip(vip)
    return self:GetNum(self:GetIdByVip(vip))
end
return VipYunpanDataHelper
