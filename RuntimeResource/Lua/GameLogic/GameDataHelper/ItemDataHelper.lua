-- ItemDataHelper.lua
ItemDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemRoot = Class.ItemRoot(ClassTypes.XObject)
local ItemCommon = Class.ItemCommon(ClassTypes.ItemRoot)
local ItemAnimal = Class.ItemAnimal(ClassTypes.ItemRoot)
local ItemEquip = Class.ItemEquip(ClassTypes.ItemRoot)
local ItemGem = Class.ItemGem(ClassTypes.ItemRoot)
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local public_config = GameWorld.public_config

function ItemRoot:__ctor__(cfg)
    self.id = cfg.__id
	self.name = cfg.__name
	self.itemType = cfg.__itemType
    self.type = cfg.__type
	self.description = cfg.__description
	self.icon = cfg.__icon
	self.look = cfg.__look
	self.quality = cfg.__quality
	self.levelNeed = cfg.__levelNeed
	self.useVocation = DataParseHelper.ParseListInt(cfg.__useVocation)
	self.allowSale = cfg.__allowSale
	self.salePrice = cfg.__salePrice
	self.bindType = cfg.__bindType
	self.sfxDropLoot = DataParseHelper.ParseListInt(cfg.__sfx_drop_loot)
	self.auctionType = cfg.__auction_type
	self.auctionSubtype = cfg.__auction_subtype
	self.quickUse = cfg.__quick_use
	self.guideBuyCost = DataParseHelper.ParseMSuffixSort(cfg.__guide_buy_cost)
end

--一般道具
function ItemCommon:__ctor__(cfg)
	self.maxStack = cfg.__maxStack
	self.autoUse = cfg.__autoUse	
	self.effectId = DataParseHelper.ParseListInt(cfg.__effectId)
	self.open_in_bag = DataParseHelper.ParseMSuffixSort(cfg.__open_in_bag)
	self.achieve_chinese = cfg.__achieve_chinese
	self.icon_type = cfg.__icon_type
	self.no_batch_use = cfg.__no_batch_use
	self.mode_vocation = DataParseHelper.ParseMSuffix(cfg.__mode_vocation)
end

--装备
function ItemEquip:__ctor__(cfg)
	self.mode = cfg.__mode
	self.jewelSlot = DataParseHelper.ParseListInt(cfg.__jewelSlot)
	self.grade = cfg.__grade
	self.strengthHighLv = cfg.__strengthHighLv
	self.petExp = cfg.__pet_exp
	self.star = cfg.__star
	self.suitId = DataParseHelper.ParseListInt(cfg.__suit_id)
	self.suitCost = DataParseHelper.ParseNSuffix(cfg.__suit_cost)
	self.guildWarehousePoints = cfg.__guild_warehouse_points
	self.tradeStarType = cfg.__trade_star_type
end

--神兽装备
function ItemAnimal:__ctor__(cfg)
	self.star = cfg.__star
	self.exp = cfg.__exp
	self.strengthHighLv = cfg.__strengthHighLv
	self.achieve_chinese = cfg.__achieve_chinese
end

--宝石
function ItemGem:__ctor__(cfg)
	self.maxStack = cfg.__maxStack
	self.gem_corner_icon = cfg.__corner_icon
	self.gem_slot = DataParseHelper.ParseListInt(cfg.__gem_slot)
	self.gem_level = cfg.__gem_level
	self.gem_attribute = DataParseHelper.ParseMSuffixSort(cfg.__gem_attribute)
end

local cache = {}

function ItemDataHelper.GetItem(id)
	
	local data = cache[id]
	if data == nil then
		data = XMLManager.item_com[id]
		if data ~= nil then data = ItemCommon(data) end
	end
	if data == nil then
		data = XMLManager.item_equipment[id]
		if data ~= nil then data = ItemEquip(data) end
	end
	if data == nil then
		data = XMLManager.item_gem[id]
		if data ~= nil then data = ItemGem(data) end
	end

	if data == nil then
		data = XMLManager.item_equip_animals[id]
		if data ~= nil then data = ItemAnimal(data) end
	end

	if data ~= nil then
		cache[data.id] = data
	else 
		LoggerHelper.Error("item id Error by :"..tostring(id))
	end

	return data
end

function ItemDataHelper.GetItemName(id)
	local data = ItemDataHelper.GetItem(id)
	return data and LanguageDataHelper.GetContent(data.name) or nil
end

function ItemDataHelper.GetItemNameWithColor(id)
	local data = ItemDataHelper.GetItem(id)
	local name
	if data == nil then
		return name
	end
	local quality = ItemDataHelper.GetQuality(id)
	local colorId = ItemConfig.QualityTextMap[quality]
	name = StringStyleUtil.GetColorStringWithColorId(LanguageDataHelper.GetContent(data.name),colorId)
	return name
end

function ItemDataHelper.GetItemColorFormat(id, content)
	local data = ItemDataHelper.GetItem(id)
	local name
	if data == nil then
		return name
	end
	local quality = ItemDataHelper.GetQuality(id)
	local colorId = ItemConfig.QualityTextMap[quality]
	name = StringStyleUtil.GetColorStringWithColorId(content,colorId)
	return name
end

function ItemDataHelper.GetItemType(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.itemType or nil
end

function ItemDataHelper.GetIconType(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.icon_type or nil
end

function ItemDataHelper.GetType(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.type or nil
end

function ItemDataHelper.GetSubtype(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.subype or nil
end

function ItemDataHelper.GetDescription(id)
	local data = ItemDataHelper.GetItem(id)
	if data.description and data.description > 0 then
		return LanguageDataHelper.CreateContent(data.description)
	end
	return nil
end

function ItemDataHelper.GetLevelNeed(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.levelNeed or nil
end

--等级匹配
function ItemDataHelper.CheckLevelMatch(id,level)
	local data = ItemDataHelper.GetItem(id)
	return data.levelNeed <= level or false
end

function ItemDataHelper:GetMaxStack(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.maxStack or nil
end

function ItemDataHelper:IsCanStack(id)
	local maxStack = self:GetMaxStack(id)
	if maxStack == 0 or maxStack == 1 then
		return false
	end	
	return true
end

function ItemDataHelper.GetIcon(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.icon or nil
end

function ItemDataHelper.GetLook(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.look or nil
end

function ItemDataHelper.GetQuality(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.quality or nil
end

function ItemDataHelper.GetAllowSale(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.allowSale or nil
end

function ItemDataHelper.GetSalePrice(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.salePrice or 0
end

function ItemDataHelper.GetSfxDropLoot(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.sfxDropLoot or {}
end

function ItemDataHelper.GetQuickUse(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.quickUse == 1
end

function ItemDataHelper.GetGuideBuyCost(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.guideBuyCost or 0
end

function ItemDataHelper.GetVocationString(id)
	local data = ItemDataHelper.GetItem(id)
	local result = ""
	local useVocation = data.useVocation
	--全职业
	if useVocation == nil or useVocation[1] == 0 then
		result = GameDataHelper.LanguageDataHelper.CreateContent(0)
		return result
	end

	for i=1,#useVocation do
		result = result..GameDataHelper.RoleDataHelper.GetRoleNameTextByVocation(useVocation[i])
		if i<#useVocation then
			result = result..","
		end
	end
	return result
end

--职业匹配
--是否广义职业匹配 isGeneral：true(包含现在不能使用转职后能使用),false(当前能使用)
function ItemDataHelper.CheckVocationMatch(id,vocation,isGeneral)
	local data = ItemDataHelper.GetItem(id)
	local useVocation = data.useVocation
	if useVocation == nil or useVocation[1] == 0 then
		return true
	end

	for i=1,#useVocation do
		local uv = useVocation[i]
		if math.floor(uv/100) == math.floor(vocation/100) then
			--广义职业匹配
			if isGeneral then 
				return true
			else
				--严格职业匹配
				if vocation%100 >= uv%100 then
					return true
				end
			end
		end
	end
	return false
end

function ItemDataHelper.GetItemEffect(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.effectId or nil
end

--获取当前最高能装备的魔石等级
function ItemDataHelper.GetMaxGemLevel(playerLevel)
	local keys = XMLManager.item_gem:Keys()
	local maxLevel = 0
	for i=1,#keys do
		local gemData = ItemDataHelper.GetItem(keys[i])
		if gemData.levelNeed <= playerLevel and maxLevel < gemData.gem_level then
			maxLevel = gemData.gem_level
		end
	end
	return maxLevel
end

--根据类型和等级获取魔石配置
function ItemDataHelper.GetGemIdByTypeAndLevel(gemType,gemLevel)
	local keys = XMLManager.item_gem:Keys()
	for i=1,#keys do
		local gemData = ItemDataHelper.GetItem(keys[i])
		if gemData.subtype == gemType and gemData.gem_level == gemLevel then
			return gemData
		end
	end
	return nil
end

--获取同一个effectId所有道具
function ItemDataHelper.GetAllItemByEffectId(effectId)
	local keys = XMLManager.item_com:Keys()
	local result = {}
	for i=1,#keys do
		local item = ItemDataHelper.GetItem(keys[i])
		if item.effectId and item.effectId[1] == effectId then
			table.insert(result,item)
		end
	end
	return result
end

function ItemDataHelper.GetEquipModel(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.mode or nil
end

function ItemDataHelper.GetEquipGrade(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.grade or nil
end

function ItemDataHelper.GetSuitId(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.suitId or nil
end

--根据type, subType获取道具表对应的所有itemId
function ItemDataHelper.GetAllItemByTypeAndSubType(type, subType)
	local keys = XMLManager.item_com:Keys()
	local result = {}
	for i=1,#keys do
		local item = ItemDataHelper.GetItem(keys[i])
		if item.type and item.type == type and item.subtype and item.subtype == subType then
			table.insert(result,keys[i])
		end
	end
	return result
end
-----------------------------------------特殊道具-------------------------------------------------------
-- local ItemSpecial = Class.ItemSpecial(ClassTypes.XObject)

-- local specailItemCache = {}

-- function ItemSpecial:__ctor__(cfg)
-- 	self.id = cfg.__id
-- 	self.name = cfg.__name
-- 	self.icon = cfg.__icon
-- 	self.look = cfg.__look
-- 	self.range = cfg.__range
-- 	self.buff_id = cfg.__buff_id
-- end

-- function ItemDataHelper.GetSpecailItem(id)
-- 	local data = specailItemCache[id]
-- 	if data == nil then
-- 		data = XMLManager.special_item[id]
-- 		if data ~= nil then 
-- 			data = ItemSpecial(data) 
-- 			specailItemCache[id] = data
-- 		end
-- 	end
-- 	return data
-- end

-- function ItemDataHelper.GetSpecialItemName(id)
-- 	local data = ItemDataHelper.GetSpecailItem(id)
-- 	return data and LanguageDataHelper.GetContent(data.name) or nil
-- end

-- function ItemDataHelper.GetSpecialItemIcon(id)
-- 	local data = ItemDataHelper.GetSpecailItem(id)
-- 	return data and data.icon or nil
-- end

-- function ItemDataHelper.GetSpecialItemLook(id)
-- 	local data = ItemDataHelper.GetSpecailItem(id)
-- 	return data and data.look or nil
-- end

-------------------------------------------道具类型配置--------------------------------------------------------
local ItemTypeCfg = Class.ItemTypeCfg(ClassTypes.XObject)

local itemTypeCache = {}
local itemTypeCacheByType = {}

function ItemTypeCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.item_type = cfg.__item_type
	self.type = cfg.__type
	self.chinese = cfg.__chinese
end

function ItemDataHelper.GetTypeNameByType(itemType,type)
	if itemTypeCacheByType[itemType] == nil then
		itemTypeCacheByType[itemType] = {}
	end
	if itemTypeCacheByType[itemType][type] == nil then
		local keys = XMLManager.item_type:Keys()
		for i=1,#keys do
			local data = itemTypeCache[keys[i]] 
			if data == nil then
				--将数据写入cache
				data = ItemDataHelper.GetTypeCfg(keys[i])
			end

			if data.item_type == itemType and data.type == type then
				itemTypeCacheByType[itemType][type] = data.chinese
			end 
		end
	end
	return itemTypeCacheByType[itemType][type]
end

function ItemDataHelper.GetTypeCfg(id)
	local data = itemTypeCache[id]
	if data == nil then
		local cfg = XMLManager.item_type[id]
		if cfg ~= nil then
			data = ItemTypeCfg(cfg)
			itemTypeCache[data.id] = data
		else
            LoggerHelper.Error("Type id not exist:" .. id)
		end
	end
	return data
end

-------------------------------------------道具掉落限制配置--------------------------------------------------------
-- local ItemDayDropLimitCfg = Class.ItemDayDropLimitCfg(ClassTypes.XObject)

local itemDayDropLimitCfgCache

-- function ItemDayDropLimitCfg:__ctor__(cfg)
-- 	self.id = cfg.__id
-- 	self.item_id = cfg.__item_id
-- 	self.count = cfg.__count
-- end

function ItemDataHelper:GetItemDayDropLimit(itemId)
	local keys = XMLManager.item_drop_day_limit:Keys()

	if itemDayDropLimitCfgCache == nil then
		itemDayDropLimitCfgCache = {}
		for i=1,#keys do
			local data = XMLManager.item_drop_day_limit[keys[i]] 
			itemDayDropLimitCfgCache[data.__item_id] = data.__count
		end
	end

	return itemDayDropLimitCfgCache[itemId]
end

---------------------------------------交易行相关-----------------------------------------
local tradeCache

function ItemDataHelper.GetAuctionType(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.auctionType or nil
end

function ItemDataHelper.GetAuctionSubType(id)
	local data = ItemDataHelper.GetItem(id)
	return data and data.auctionSubtype or nil
end

local function TradeSort(a,b)
	return a[1].id < b[1].id
end

function ItemDataHelper:GetTradeItemTypeList()
	if tradeCache == nil then
		tradeCache = {}
		local keys = XMLManager.item_com:Keys()
		for i=1,#keys do
			local itemData = ItemDataHelper.GetItem(keys[i])
			if itemData.auctionType and itemData.auctionType > 0 then
				if tradeCache[itemData.auctionType] == nil then
					tradeCache[itemData.auctionType] = {}
				end

				if tradeCache[itemData.auctionType][itemData.auctionSubtype] == nil then
					tradeCache[itemData.auctionType][itemData.auctionSubtype] = {}
				end
				table.insert(tradeCache[itemData.auctionType][itemData.auctionSubtype],{itemData})
			end
		end

		keys = XMLManager.item_gem:Keys()
		for i=1,#keys do
			local itemData = ItemDataHelper.GetItem(keys[i])
			if itemData.auctionType and itemData.auctionType > 0 then
				if tradeCache[itemData.auctionType] == nil then
					tradeCache[itemData.auctionType] = {}
				end

				if tradeCache[itemData.auctionType][itemData.auctionSubtype] == nil then
					tradeCache[itemData.auctionType][itemData.auctionSubtype] = {}
				end
				table.insert(tradeCache[itemData.auctionType][itemData.auctionSubtype],{itemData})
			end
		end
		
		keys = XMLManager.item_equipment:Keys()
		local tempEquipList = {}
		for i=1,#keys do
			local itemData = ItemDataHelper.GetItem(keys[i])
			if itemData.tradeStarType and itemData.tradeStarType > 0 then
				if tempEquipList[itemData.tradeStarType] == nil then
					tempEquipList[itemData.tradeStarType] = {}
				end
				table.insert(tempEquipList[itemData.tradeStarType],itemData)
			end
		end
		
		for k,v in pairs(tempEquipList) do
			local itemData = v[1]
			if tradeCache[itemData.auctionType] == nil then
				tradeCache[itemData.auctionType] = {}
			end

			if tradeCache[itemData.auctionType][itemData.auctionSubtype] == nil then
				tradeCache[itemData.auctionType][itemData.auctionSubtype] = {}
			end
			table.insert(tradeCache[itemData.auctionType][itemData.auctionSubtype],v)
		end

		for auctionType,v in pairs(tradeCache) do
			for auctionSubtype,v1 in pairs(v) do
				table.sort(v1, TradeSort)
			end
		end
	end

	return tradeCache
end

--查找道具
function ItemDataHelper:BegSearchByName(str)
	local list = self:GetTradeItemTypeList()
	local result = {}

	for i=1,#list do
		for k1,v1 in pairs(list[i]) do
			for j=1,#v1 do
				local itemData = v1[j][1]
				local name = ItemDataHelper.GetItemName(itemData.id)
				local startIndex,endIndex = string.find(name,str)
				if startIndex then
					table.insert(result,v1[j])
				end
			end
		end
	end

	return result
end
	
return ItemDataHelper
