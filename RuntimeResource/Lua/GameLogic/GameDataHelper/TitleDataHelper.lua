-- TitleDataHelper.lua
local TitleDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ClearDataManager = PlayerManager.ClearDataManager

local Max_Title_Index = 4
local Min_Title_Index = 1

local TitleCfgData = Class.TitleCfgData(ClassTypes.XObject)

function TitleCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.name = cfg.__name
    self.type = cfg.__type
    self.icon = cfg.__icon
    self.describe = cfg.__describe
    self.time_limit = cfg.__time_limit
    self.title_color = cfg.__title_color
    self.wear_immediately = cfg.__wear_immediately
    self.is_love = cfg.__is_love
	self.targets = DataParseHelper.ParseMSuffix(cfg.__targets)
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

local TitleCfgDataCache = {}
function TitleDataHelper.ClearCache()
    TitleCfgDataCache = {}
end

function TitleDataHelper.GetData(id)
    ClearDataManager:UseCache(CacheNames.title, TitleDataHelper.ClearCache)
    local data = TitleCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.title[id]
    if data~=nil then
	    data = TitleCfgData(data) 
    end
	TitleCfgDataCache[id] = data
	return data
end

local typeTitleDataCache = nil
function TitleDataHelper.GetShowDataByType(type)
    if typeTitleDataCache == nil then
        typeTitleDataCache = {}
        local keys = XMLManager.title:Keys()
		for i=1,#keys do
			local data = TitleDataHelper.GetData(keys[i])
			if typeTitleDataCache[data.type] == nil then
				typeTitleDataCache[data.type] = {}
			end
			table.insert(typeTitleDataCache[data.type], data.id)
		end
    end
    return typeTitleDataCache[type]
end

local titleShowDataCache = nil
function TitleDataHelper.GetTitleShowData()
    if titleShowDataCache ~= nil then
        return titleShowDataCache
    end
    titleShowDataCache = {}
    for i=Min_Title_Index, Max_Title_Index do
        local typeData = TitleDataHelper.GetShowDataByType(i)
        if typeData ~= nil then
            local data = {}
            table.insert(data,{i})
            for j=1, #typeData do
                table.insert(data,{typeData[j]})
            end
            table.insert(titleShowDataCache, data)
        end
    end
    return titleShowDataCache
end

function TitleDataHelper.GetTitleDescribe(id)
    local data = TitleDataHelper.GetData(id)
    return LanguageDataHelper.GetContent(data.describe)
end

function TitleDataHelper.GetTitleName(id)
    local data = TitleDataHelper.GetData(id)
    return LanguageDataHelper.GetContent(data.name)
end

function TitleDataHelper.GetIslove(id)
    local data = TitleDataHelper.GetData(id)
    return data.is_love
end

function TitleDataHelper.GetTitleNameColor(id)
    local data = TitleDataHelper.GetData(id)
    return data.title_color
end

function TitleDataHelper.GetTitleTimeLimit(id)
    local data = TitleDataHelper.GetData(id)
    return data.time_limit
end

function TitleDataHelper.GetTitleAttri(id)
    local result = {}
    local data = TitleDataHelper.GetData(id)
    for k,v in pairs(data.attri) do
        table.insert(result, {["attri"]=k, ["value"]=v})
    end
    return result
end

--返回字符串则直接显示文字， 否则显示icon
function TitleDataHelper.GetTitleShowName(id)
    local data = TitleDataHelper.GetData(id)
    if data.icon == 0 then
        return StringStyleUtil.GetColorStringWithColorId(LanguageDataHelper.GetContent(data.name), TitleDataHelper.GetTitleNameColor(id))
    end
    return data.icon
end

function TitleDataHelper.GetTitleWearImmediately(id)
    local data = TitleDataHelper.GetData(id)
    return data.wear_immediately or 0
end

return TitleDataHelper