-- MythicalAnimalsLevelDataHelper.lua
local MythicalAnimalsLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local MythicalAnimalsLevel = Class.MythicalAnimalsLevel(ClassTypes.XObject)

local mythicalAnimalsLevelCache = {}
function MythicalAnimalsLevelDataHelper.ClearCache()
    mythicalAnimalsLevelCache = {}
end

function MythicalAnimalsLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.level = cfg.__level
    self.cost = cfg.__cost
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

function MythicalAnimalsLevelDataHelper.GetMythicalAnimalsLevel(id)
    ClearDataManager:UseCache(CacheNames.mythical_animals_level, MythicalAnimalsLevelDataHelper.ClearCache)
    local data = mythicalAnimalsLevelCache[id]
    if data == nil then
        local cfg = XMLManager.mythical_animals_level[id]
        if cfg ~= nil then
            data = MythicalAnimalsLevel(cfg)
            mythicalAnimalsLevelCache[data.id] = data
        else
            LoggerHelper.Error("MythicalAnimalsLevel id not exist:" .. id)
        end
    end
    return data
end

function MythicalAnimalsLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.mythical_animals_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MythicalAnimalsLevelDataHelper:GetType(id)
    local data = self.GetMythicalAnimalsLevel(id)
    return data and data.type or nil
end

function MythicalAnimalsLevelDataHelper:GetLevel(id)
    local data = self.GetMythicalAnimalsLevel(id)
    return data and data.level or nil
end

function MythicalAnimalsLevelDataHelper:GetCost(id)
    local data = self.GetMythicalAnimalsLevel(id)
    return data and data.cost or nil
end

function MythicalAnimalsLevelDataHelper:GetAttri(id)
    local data = self.GetMythicalAnimalsLevel(id)
    return data and data.attri or nil
end

--根据神兽装备的部位类型和强化等级返回升级消耗
function MythicalAnimalsLevelDataHelper:GetLevelUpCostByTypeAndLevel(typeNum, level)
    local maxLevel = self:GetMaxLevel(typeNum)
    if maxLevel == level then
        return -1
    end

    local data = XMLManager.mythical_animals_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and typeNum == self:GetType(v) and level == self:GetLevel(v)  then
            return self:GetCost(v)
        end
    end
    return 0
end

--根据神兽装备的部位类型和强化等级返回当前属性值
function MythicalAnimalsLevelDataHelper:GetAttriByTypeAndLevel(typeNum, level)
    local data = XMLManager.mythical_animals_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and typeNum == self:GetType(v) and level == self:GetLevel(v)  then
            return self:GetAttri(v)
        end
    end
    return {}
end

--根据神兽装备的部位类型获取最高强化等级
function MythicalAnimalsLevelDataHelper:GetMaxLevel(typeNum)
    local data = XMLManager.mythical_animals_level:Keys()
    local maxLevel = 0
    for i, v in ipairs(data) do
        if v ~= 0 and typeNum == self:GetType(v) and self:GetLevel(v) > maxLevel then
            maxLevel = self:GetLevel(v)
        end
    end
    return maxLevel
end


return MythicalAnimalsLevelDataHelper
