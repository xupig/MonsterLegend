-- GuildConquestOpenServerDataHelper.lua
local GuildConquestOpenServerDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GuildConquestOpenServer = Class.GuildConquestOpenServer(ClassTypes.XObject)

local guildConquestOpenServerCache = {}
function GuildConquestOpenServerDataHelper.ClearCache()
    guildConquestOpenServerCache = {}
end

function GuildConquestOpenServer:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.des = cfg.__des
end

function GuildConquestOpenServerDataHelper.GetGuildConquestOpenServer(id)
    ClearDataManager:UseCache(CacheNames.guild_conquest_open_server, GuildConquestOpenServerDataHelper.ClearCache)
    local data = guildConquestOpenServerCache[id]
    if data == nil then
        local cfg = XMLManager.guild_conquest_open_server[id]
        if cfg ~= nil then
            data = GuildConquestOpenServer(cfg)
            guildConquestOpenServerCache[data.id] = data
        else
            LoggerHelper.Error("GuildConquestOpenServer id not exist:" .. id)
        end
    end
    return data
end

function GuildConquestOpenServerDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.guild_conquest_open_server:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GuildConquestOpenServerDataHelper:GetId(id)
    local data = self.GetGuildConquestOpenServer(id)
    return data and data.id or nil
end

function GuildConquestOpenServerDataHelper:GetType(id)
    local data = self.GetGuildConquestOpenServer(id)
    return data and data.type or nil
end

function GuildConquestOpenServerDataHelper:GetReward(id)
    local data = self.GetGuildConquestOpenServer(id)
    return data and data.reward or nil
end

function GuildConquestOpenServerDataHelper:GetDes(id)
    local data = self.GetGuildConquestOpenServer(id)
    return data and data.des or nil
end

return GuildConquestOpenServerDataHelper
