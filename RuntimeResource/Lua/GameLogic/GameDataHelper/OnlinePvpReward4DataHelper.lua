-- OnlinePvpReward4DataHelper.lua
local OnlinePvpReward4DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvpReward4 = Class.OnlinePvpReward4(ClassTypes.XObject)

local onlinePvpReward4Cache = {}
function OnlinePvpReward4DataHelper.ClearCache()
    onlinePvpReward4Cache = {}
end

function OnlinePvpReward4:__ctor__(cfg)
    self.id = cfg.__id
    self.reward_show = DataParseHelper.ParseMSuffixSort(cfg.__reward_show)
end

function OnlinePvpReward4DataHelper.GetOnlinePvpReward4(id)
    ClearDataManager:UseCache(CacheNames.online_pvp_reward_4, OnlinePvpReward4DataHelper.ClearCache)
    local data = onlinePvpReward4Cache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp_reward_4[id]
        if cfg ~= nil then
            data = OnlinePvpReward4(cfg)
            onlinePvpReward4Cache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvpReward4 id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpReward4DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp_reward_4:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpReward4DataHelper:GetRewardShow(id)
    local data = self.GetOnlinePvpReward4(id)
    return data and data.reward_show or nil
end

return OnlinePvpReward4DataHelper
