-- WordCollectionDataHelper.lua
local WordCollectionDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WordCollection = Class.WordCollection(ClassTypes.XObject)

local wordCollectionCache = {}
function WordCollectionDataHelper.ClearCache()
    wordCollectionCache = {}
end

function WordCollection:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = DataParseHelper.ParseMSuffixSort(cfg.__cost)
    self.time = DataParseHelper.ParseListInt(cfg.__time)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function WordCollectionDataHelper.GetWordCollection(id)
    ClearDataManager:UseCache(CacheNames.word_collection, WordCollectionDataHelper.ClearCache)
    local data = wordCollectionCache[id]
    if data == nil then
        local cfg = XMLManager.word_collection[id]
        if cfg ~= nil then
            data = WordCollection(cfg)
            wordCollectionCache[data.id] = data
        else
            LoggerHelper.Error("WordCollection id not exist:" .. id)
        end
    end
    return data
end

function WordCollectionDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.word_collection:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WordCollectionDataHelper:GetCost(id)
    local data = self.GetWordCollection(id)
    return data and data.cost or nil
end

function WordCollectionDataHelper:GetTime(id)
    local data = self.GetWordCollection(id)
    return data and data.time or nil
end

function WordCollectionDataHelper:GetReward(id)
    local data = self.GetWordCollection(id)
    return data and data.reward or nil
end

return WordCollectionDataHelper
