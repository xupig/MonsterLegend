-- WelfareMonthRewardDataHelper.lua
local WelfareMonthRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareMonthReward = Class.WelfareMonthReward(ClassTypes.XObject)

local welfareMonthRewardCache = {}
function WelfareMonthRewardDataHelper.ClearCache()
    welfareMonthRewardCache = {}
end

function WelfareMonthReward:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.lv = DataParseHelper.ParseListInt(cfg.__lv)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.vip_level = cfg.__vip_level
    self.vip_rate = cfg.__vip_rate
    self.vip_desc = cfg.__vip_desc
end

function WelfareMonthRewardDataHelper.GetWelfareMonthReward(id)
    ClearDataManager:UseCache(CacheNames.welfare_month_reward, WelfareMonthRewardDataHelper.ClearCache)
    local data = welfareMonthRewardCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_month_reward[id]
        if cfg ~= nil then
            data = WelfareMonthReward(cfg)
            welfareMonthRewardCache[data.id] = data
        else
            LoggerHelper.Error("WelfareMonthReward id not exist:" .. id)
        end
    end
    return data
end

function WelfareMonthRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_month_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareMonthRewardDataHelper:GetDay(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.day or nil
end

function WelfareMonthRewardDataHelper:GetLv(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.lv or nil
end

function WelfareMonthRewardDataHelper:GetReward(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.reward or nil
end

function WelfareMonthRewardDataHelper:GetBindFlag(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.bind_flag or nil
end

function WelfareMonthRewardDataHelper:GetVipLevel(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.vip_level or nil
end

function WelfareMonthRewardDataHelper:GetVipRate(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.vip_rate or nil
end

function WelfareMonthRewardDataHelper:GetVipDesc(id)
    local data = self.GetWelfareMonthReward(id)
    return data and data.vip_desc or nil
end

return WelfareMonthRewardDataHelper
