-- ColorDataHelper.lua
local ColorDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local Color = Color
local ColorDataCfgData = Class.ColorDataCfgData(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

function ColorDataCfgData:__ctor__(cfg)
	self.id = cfg.__id
    self.r = cfg.__r
    self.g = cfg.__g
    self.b = cfg.__b
    self.color = Color(self.r/255, self.g/255, self.b/255,1)
end

local ColorDataCfgDataCache = {}
function ColorDataHelper.ClearCache()
    ColorDataCfgDataCache = {}
end

function ColorDataHelper.GetColorData(id)
    ClearDataManager:UseCache(CacheNames.color, ColorDataHelper.ClearCache)
	local data = ColorDataCfgDataCache[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.color[id]
    if data~=nil then
	    data = ColorDataCfgData(data) 
    end
	ColorDataCfgDataCache[id] = data
	return data
end


return ColorDataHelper