-- PrestigeLevelDataHelper.lua
local PrestigeLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local PrestigeLevel = Class.PrestigeLevel(ClassTypes.XObject)

local prestigeLevelCache = {}
function PrestigeLevelDataHelper.ClearCache()
    prestigeLevelCache = {}
end

function PrestigeLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.chinese = cfg.__chinese
    self.exp = cfg.__exp
end

function PrestigeLevelDataHelper.GetPrestigeLevel(id)
    ClearDataManager:UseCache(CacheNames.prestige_level, PrestigeLevelDataHelper.ClearCache)
    local data = prestigeLevelCache[id]
    if data == nil then
        local cfg = XMLManager.prestige_level[id]
        if cfg ~= nil then
            data = PrestigeLevel(cfg)
            prestigeLevelCache[data.id] = data
        else
            LoggerHelper.Error("PrestigeLevel id not exist:" .. id)
        end
    end
    return data
end

function PrestigeLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.prestige_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PrestigeLevelDataHelper:GetChinese(id)
    local data = self.GetPrestigeLevel(id)
    return data and data.chinese or nil
end

function PrestigeLevelDataHelper:GetExp(id)
    local data = self.GetPrestigeLevel(id)
    return data and data.exp or nil
end

--根据总的声望值获取升级经验以及当前的经验
function PrestigeLevelDataHelper:GetUpgrageLevelInfoByTotalPrestige(totalNum)
    local data = XMLManager.prestige_level:Keys()
    local exp = 0
    local levelData = {}
    local nowNum = totalNum
    for i, v in ipairs(data) do
        local levelUpExp =  PrestigeLevelDataHelper:GetExp(v)
        if levelUpExp == 0 then
            levelData.fill = 1
            levelData.name = LanguageDataHelper.CreateContent(PrestigeLevelDataHelper:GetChinese(v))
            levelData.value = "MAX"
            break
        end

        exp = exp + levelUpExp
        if totalNum < exp then
            levelData.value = nowNum .. LanguageDataHelper.CreateContent(900) .. levelUpExp
            levelData.name = LanguageDataHelper.CreateContent(PrestigeLevelDataHelper:GetChinese(v))
            levelData.fill = nowNum / levelUpExp
            break
        end
        nowNum = nowNum - levelUpExp
    end
    return levelData
end

return PrestigeLevelDataHelper
