-- TradeDataHelper.lua
--交易行配置
local TradeDataHelper = {}
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XMLManager = GameManager.XMLManager
local ClearDataManager = PlayerManager.ClearDataManager

local TradeCfg = Class.TradeCfg(ClassTypes.XObject)

local tradeCache = {}
function TradeDataHelper.ClearCache()
    tradeCache = {}
end

local tradeItemTypeCache
-----------------------------------------交易基础配置-----------------------------------------
function TradeCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.auctionType = cfg.__auction_type
	self.typeName = cfg.__type_name
	self.auctionSubtype = cfg.__auction_subtype
	self.subtypeName = cfg.__subtype_name
	self.subtypeIcon = cfg.__subtype_icon
	self.filter =DataParseHelper.ParseListInt(cfg.__filter)
	self.gradeType = cfg.__grade_type
end

--
function TradeDataHelper.GetTradeCfg(id)
    ClearDataManager:UseCache(CacheNames.trade, TradeDataHelper.ClearCache)
	local data = tradeCache[id]
	if data == nil then
		local cfg = XMLManager.trade[id]
		if cfg ~= nil then
			data = TradeCfg(cfg)
			tradeCache[data.id] = data
		else
            LoggerHelper.Error("TradeCfg id not exist:" .. id)
		end
	end
	return data
end

function TradeDataHelper:GetTradeItemTypeList()
	if tradeItemTypeCache == nil then
		tradeItemTypeCache = {}
		local keys = XMLManager.trade:Keys()
		for i=1,#keys do
			local cfg = XMLManager.trade[keys[i]]
			if cfg ~= nil then
				local data = TradeCfg(cfg)
				if tradeItemTypeCache[data.auctionType] == nil then
					tradeItemTypeCache[data.auctionType] = {}
				end
				table.insert(tradeItemTypeCache[data.auctionType],data)
			end
		end
	end
 	--LoggerHelper.Error("tradeItemTypeCache")
	--LoggerHelper.Log(tradeItemTypeCache)
	return tradeItemTypeCache
end

function TradeDataHelper:GetGradeTypeByType(auctionType)
	local t = self:GetTradeItemTypeList()[auctionType]
	return t[1].gradeType
end

function TradeDataHelper:GetTradeCfgByType(auctionType,auctionSubtype)
	local t = self:GetTradeItemTypeList()[auctionType]
	for i=1,#t do
		if t[i].auctionSubtype == auctionSubtype then
			return t[i]
		end
	end
	return nil
end

return TradeDataHelper

