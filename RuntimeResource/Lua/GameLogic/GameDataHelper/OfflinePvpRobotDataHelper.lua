-- OfflinePvpRobotDataHelper.lua
local OfflinePvpRobotDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OfflinePvpRobot = Class.OfflinePvpRobot(ClassTypes.XObject)

local offlinePvpRobotCache = {}
function OfflinePvpRobotDataHelper.ClearCache()
    offlinePvpRobotCache = {}
end

function OfflinePvpRobot:__ctor__(cfg)
    self.id = cfg.__id
    self.fight = cfg.__fight
    self.model = cfg.__model
    self.name = cfg.__name
    self.professional = cfg.__professional
end

function OfflinePvpRobotDataHelper.GetOfflinePvpRobot(id)
    ClearDataManager:UseCache(CacheNames.offline_pvp_robot, OfflinePvpRobotDataHelper.ClearCache)
    local data = offlinePvpRobotCache[id]
    if data == nil then
        local cfg = XMLManager.offline_pvp_robot[id]
        if cfg ~= nil then
            data = OfflinePvpRobot(cfg)
            offlinePvpRobotCache[data.id] = data
        else
            LoggerHelper.Error("OfflinePvpRobot id not exist:" .. id)
        end
    end
    return data
end

function OfflinePvpRobotDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.offline_pvp_robot:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OfflinePvpRobotDataHelper:GetFight(id)
    local data = self.GetOfflinePvpRobot(id)
    return data and data.fight or nil
end

function OfflinePvpRobotDataHelper:GetModel(id)
    local data = self.GetOfflinePvpRobot(id)
    return data and data.model or nil
end

function OfflinePvpRobotDataHelper:GetName(id)
    local data = self.GetOfflinePvpRobot(id)
    return data and data.name or nil
end
function OfflinePvpRobotDataHelper:GetProfessional(id)
    local data = self.GetOfflinePvpRobot(id)
    return data and data.professional or nil
end
return OfflinePvpRobotDataHelper
