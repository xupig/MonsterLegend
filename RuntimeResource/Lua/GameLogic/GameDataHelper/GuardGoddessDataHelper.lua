-- GuardGoddessDataHelper.lua
local GuardGoddessDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

local GuardGoddess = Class.GuardGoddess(ClassTypes.XObject)

function GuardGoddess:__ctor__(cfg)
    self.id = cfg.__id
    self.model = cfg.__model
    self.icon = cfg.__icon
    self.name = cfg.__name
    self.title = cfg.__title
    self.desc = cfg.__desc
    self.ai_id = cfg.__ai_id
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.commit_dialog = DataParseHelper.ParseNSuffixSort(cfg.__commit_dialog)
    self.ride_model =cfg.__ride_model
end

local guardGoddessCache = {}
function GuardGoddessDataHelper.ClearCache()
    guardGoddessCache = {}
end

function GuardGoddessDataHelper:GetData(id)
    ClearDataManager:UseCache(CacheNames.guard_goddess, GuardGoddessDataHelper.ClearCache)
    local data = guardGoddessCache[id]
    if data == nil then
        local cfg = XMLManager.guard_goddess[id]
        if cfg ~= nil then
            data = GuardGoddess(cfg)
            guardGoddessCache[data.id] = data
        else
            LoggerHelper.Error("GuardGoddess id not exist:" .. id)
        end
    end
    return data
end

local listDataCache
function GuardGoddessDataHelper:GetAllId()
    if listDataCache == nil then
        listDataCache = {}
        local ids = {}
        local data = XMLManager.guard_goddess:Keys()
        for i, v in ipairs(data) do
            if v ~= 0 then
                local tmp = {v}
                table.insert(ids, tmp)
            end
        end
        table.sort(ids, SortByKey1)
        for k,v in ipairs(ids) do
            table.insert(listDataCache, v[1])
        end
    end

    return listDataCache
end

function GuardGoddessDataHelper:GetModel(id)
    local data = self:GetData(id)
    return data and data.model
end

function GuardGoddessDataHelper:GetIcon(id)
    local data = self:GetData(id)
    return data and data.icon
end

function GuardGoddessDataHelper:GetName(id)
    local data = self:GetData(id)
    return LanguageDataHelper.GetContent(data.name)
end

function GuardGoddessDataHelper:GetTitle(id)
    local data = self:GetData(id)
    return LanguageDataHelper.GetContent(data.title)
end

function GuardGoddessDataHelper:GetDesc(id)
    local data = self:GetData(id)
    return LanguageDataHelper.GetContent(data.desc)
end

function GuardGoddessDataHelper:GetCost(id)
    local data = self:GetData(id)
    return data.cost
end

function GuardGoddessDataHelper:GetCommitDialog(id)
    local data = self:GetData(id)
    return data.commit_dialog
end

function GuardGoddessDataHelper:GetOpenTime()
    local data = GlobalParamsHelper.GetParamValue(525)
end

function GuardGoddessDataHelper:GetAI(id)
    local data = self:GetData(id)
    return data.ai_id
end

function GuardGoddessDataHelper:GetRideModel(id)
    local data = self:GetData(id)
    return data.ride_model or 0
end

------------------------------奖励表----------------------------------------
local GuardGoddessReward = Class.GuardGoddessReward(ClassTypes.XObject)

function GuardGoddessReward:__ctor__(cfg)
    self.id = cfg.__id
    self.goddess_id = cfg.__goddess_id
    self.level = DataParseHelper.ParseListInt(cfg.__level)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

local guardGoddessRewardCache = {}
function GuardGoddessDataHelper.ClearRewardCache()
    guardGoddessRewardCache = {}
end
function GuardGoddessDataHelper:GetRewardData(id)
    ClearDataManager:UseCache(CacheNames.guard_goddess_reward, GuardGoddessDataHelper.ClearRewardCache)
    local data = guardGoddessRewardCache[id]
    if data == nil then
        local cfg = XMLManager.guard_goddess_reward[id]
        if cfg ~= nil then
            data = GuardGoddessReward(cfg)
            guardGoddessRewardCache[data.id] = data
        else
            LoggerHelper.Error("GuardGoddessReward id not exist:" .. id)
        end
    end
    return data
end

function GuardGoddessDataHelper:GetRewardConfig(goddess_id, level)
    local keys = XMLManager.guard_goddess_reward:Keys()
    for k, id in pairs(keys) do
        if id ~= 0 then
            local data = self:GetRewardData(id)
            if data.goddess_id == goddess_id and level >= data.level[1] and level <= data.level[2] then
                return data.reward   
            end
        end
    end
end

return GuardGoddessDataHelper
