-- GemLevelTotalDataHelper.lua
local GemLevelTotalDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local GemLevelTotal = Class.GemLevelTotal(ClassTypes.XObject)

local gemLevelTotalCache = {}

function GemLevelTotal:__ctor__(cfg)
    self.id = cfg.__id
    self.range = DataParseHelper.ParseListInt(cfg.__gem_level_total)
    self.attris = DataParseHelper.ParseMSuffix(cfg.__attris)
end

function GemLevelTotalDataHelper.GetGemLevelTotal(id)
    local data = gemLevelTotalCache[id]
    if data == nil then
        local cfg = XMLManager.gem_level_total[id]
        if cfg ~= nil then
            data = GemLevelTotal(cfg)
            gemLevelTotalCache[data.id] = data
        else
            LoggerHelper.Error("GemLevelTotal id not exist:" .. id)
        end
    end
    return data
end

function GemLevelTotalDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.gem_level_total:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GemLevelTotalDataHelper:GetRange(id)
    local data = self.GetGemLevelTotal(id)
    return data and data.range or nil
end

function GemLevelTotalDataHelper:GetAttris(id)
    local data = self.GetGemLevelTotal(id)
    return data and data.attris or nil
end

function GemLevelTotalDataHelper:BinarySerch(num)
    if num == 0 then
        return 0
    end
    
    local ids = self:GetAllId()
    local mid = 0
    local low = 1
    local high = #ids
    while low <= high do
        mid = math.floor((low+high)/2)
        --LoggerHelper.Log("中间数 "..mid)
        local range = self:GetRange(mid)
        if num <range[1] then
            high = mid -1
        elseif num >range[2] then
            low = mid + 1
        else
            return mid
        end
    end
    
    if mid == 1 then
        return 0
    else
        return mid
    end
end

return GemLevelTotalDataHelper
