-- ActivityPointRewardDataHelper.lua
local ActivityPointRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ActivityPointReward = Class.ActivityPointReward(ClassTypes.XObject)

local activityPointRewardCache = {}
function ActivityPointRewardDataHelper.ClearCache()
    activityPointRewardCache = {}
end

function ActivityPointReward:__ctor__(cfg)
    self.id = cfg.__id
    self.cost_point = cfg.__cost_point
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function ActivityPointRewardDataHelper.GetActivityPointReward(id)
    ClearDataManager:UseCache(CacheNames.activity_point_reward, ActivityPointRewardDataHelper.ClearCache)
    local data = activityPointRewardCache[id]
    if data == nil then
        local cfg = XMLManager.activity_point_reward[id]
        if cfg ~= nil then
            data = ActivityPointReward(cfg)
            activityPointRewardCache[data.id] = data
        else
            LoggerHelper.Error("ActivityPointReward id not exist:" .. id)
        end
    end
    return data
end

function ActivityPointRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity_point_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityPointRewardDataHelper:GetCostPoint(id)
    local data = self.GetActivityPointReward(id)
    return data and data.cost_point or nil
end

function ActivityPointRewardDataHelper:GetReward(id)
    local data = self.GetActivityPointReward(id)
    return data and data.reward or nil
end

function ActivityPointRewardDataHelper:GetMaxPoint()
    local data = XMLManager.activity_point_reward:Keys()
    local max = 0
    for i, v in ipairs(data) do
        if v ~= 0 then
            local point = self:GetCostPoint(v)
            if point > max then
                max = point
            end
        end
    end
    return max
end

return ActivityPointRewardDataHelper
