-- InvestMonthDataHelper.lua
local InvestMonthDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local InvestMonth = Class.InvestMonth(ClassTypes.XObject)

local investMonthCache = {}
function InvestMonthDataHelper.ClearCache()
    investMonthCache = {}
end

function InvestMonth:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
    self.chinese = cfg.__chinese
end

function InvestMonthDataHelper.GetInvestMonth(id)
    ClearDataManager:UseCache(CacheNames.invest_month, InvestMonthDataHelper.ClearCache)
    local data = investMonthCache[id]
    if data == nil then
        local cfg = XMLManager.invest_month[id]
        if cfg ~= nil then
            data = InvestMonth(cfg)
            investMonthCache[data.id] = data
        else
            LoggerHelper.Error("InvestMonth id not exist:" .. id)
        end
    end
    return data
end

function InvestMonthDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.invest_month:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function InvestMonthDataHelper:GetDay(id)
    local data = self.GetInvestMonth(id)
    return data and data.day or nil
end

function InvestMonthDataHelper:GetReward(id)
    local data = self.GetInvestMonth(id)
    return data and data.reward or nil
end

function InvestMonthDataHelper:GetChinese(id)
    local data = self.GetInvestMonth(id)
    return data and data.chinese or nil
end

return InvestMonthDataHelper
