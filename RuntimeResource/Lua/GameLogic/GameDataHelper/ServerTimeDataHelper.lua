-- ServerTimeDataHelper.lua
local ServerTimeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = DateTimeUtil
local ClearDataManager = PlayerManager.ClearDataManager

local _osTime = os.time
local _osDate = os.date
local _oneWeekTime = DateTimeUtil.OneWeekTime
local _oneDayTime = DateTimeUtil.OneDayTime
local base = _G
local floor = math.floor


local ServerTime = Class.ServerTime(ClassTypes.XObject)

local cache = {}
function ServerTimeDataHelper.ClearCache()
    cache = {}
end

function ServerTimeDataHelper.Init()

end

function ServerTime:__ctor__(cfg)
    self.id = cfg.__id
    self.start_time_type = cfg.__start_time_type
    self.start_time_param1 = cfg.__start_time_param1
    self.start_time_param2 = cfg.__start_time_param2
    self.start_time_param3 = cfg.__start_time_param3
    self.end_time_type = cfg.__end_time_type
    self.end_time_param1 = cfg.__end_time_param1
    self.end_time_param2 = cfg.__end_time_param2
    self.end_time_param3 = cfg.__end_time_param3
    self.circle = cfg.__circle
    self.circle_times = cfg.__circle_times
end

function ServerTimeDataHelper.GetServerTime(id)
    ClearDataManager:UseCache(CacheNames.server_time, ServerTimeDataHelper.ClearCache)
    local data = cache[id]
    if data ~= nil then
        return data
    end
    local cfg = XMLManager.server_time[id]
    if cfg == nil then
        LoggerHelper.Error("ServerTimeDataHelper.GetServerTime has no this id:" .. tostring(id))
    else
        data = ServerTime(cfg)
        cache[data.id] = data
    end
    return data
end

--获取开始时间类型 1 自然时间 ；2 服务器开启第几周  ； 3 服务器开启第几天 
function ServerTimeDataHelper.GetStartTimeType(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    return data and data.start_time_type or nil
end

--获取开始时间 时间参数 
--参数1：HH/MM/SS，填时/分/秒的方式
--参数2：和时间类型对应：1 格式：YY/MM/DD，年/月/日的方式填写 ； 2 填周几 ； 3 填开服多少天
--参数3：和时间类型对应 1 无 ； 2 开服第几周 ； 3 无
function ServerTimeDataHelper.GetStartTimeParams(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    if data == nil then 
        return nil
    end
    local param = {}
    table.insert(param, data.start_time_param1)
    table.insert(param, tostring(data.start_time_param2))
    table.insert(param, tostring(data.start_time_param3))
    return param
end

--获取结束时间类型 1 自然时间 ；2 服务器开启第几周  ； 3 服务器开启第几天 
function ServerTimeDataHelper.GetEndTimeType(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    return data and data.end_time_type or niu
end

--获取结束时间 时间参数 
--参数1：HH/MM/SS，填时/分/秒的方式
--参数2：和时间类型对应：1 格式：YY/MM/DD，年/月/日的方式填写 ； 2 填周几 ； 3 填开服多少天
--参数3：和时间类型对应 1 无 ； 2 开服第几周 ； 3 无
function ServerTimeDataHelper.GetEndTimeParams(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
     if data == nil then 
        return nil
    end
    local param = {}
    table.insert(param, data.end_time_param1)
    table.insert(param, data.end_time_param2)
    table.insert(param, data.end_time_param3)
    return param
end

--获取循环间隔：天数
function ServerTimeDataHelper.GetCircle(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    return data and data.circle or nil
end

--获取循环次数：-1代表无限循环
function ServerTimeDataHelper.GetCircleTimes(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    return data and data.circle_times or nil
end




-- 当前是开服第几周
-- curTime [可选] 默认是服务器当前时间
function ServerTimeDataHelper.GetOpenServerWeek(curTime)
    local week = 1
    local openTime = DateTimeUtil.GetOpenServerTime()
    local openDate = DateTimeUtil.SomeDay(openTime)
    local openWDay = DateTimeUtil.WhatDay(openDate.wday)

    curTime = curTime or DateTimeUtil.GetServerTime()
    local serverDate = DateTimeUtil.SomeDay(curTime)
    local serverWDay = DateTimeUtil.WhatDay(serverDate.wday)

    local leftDay = 7 - openWDay
    local passDay = math.floor((curTime - openTime)/_oneDayTime)
    if passDay <= leftDay then
        week = 1
    else
        week = math.ceil((passDay - leftDay) / 7) + 1
    end
    return week
end

--判断当前时间是否在配置中。
--参数1：id 
function ServerTimeDataHelper.CheckCurrentTime(id)
    local data = ServerTimeDataHelper.GetServerTime(id)
    if not data then
        LoggerHelper.Error("ServerTimeDataHelper.CheckCurrentTime no data, return " .. tostring(id))
        return false
    end
    
    local serverTime = DateTimeUtil.GetServerTime()
    local openTime = DateTimeUtil.GetOpenServerTime()
    local startTime
    local endTime

    if data.start_time_type == 1 then
        startTime = _osTime(DateTimeUtil.GetDateByStr(data.start_time_param2 .. "/" .. data.start_time_param1))
        endTime = _osTime(DateTimeUtil.GetDateByStr(data.end_time_param2 .. "/" .. data.end_time_param1))
    elseif data.start_time_type == 2 then
        local openDate = DateTimeUtil.SomeDay(openTime)
        local openWDay = DateTimeUtil.WhatDay(openDate.wday)

        local startWDay = base.tonumber(data.start_time_param2)
        local endWDay = base.tonumber(data.end_time_param2)
        -- if data.start_time_param3 == 1 and startWDay < openWDay then
        --     LoggerHelper.Error("server_time config error：start_time_param2=" .. startWDay .. " < openDay" .. openWDay)
        --     return
        -- end
        -- if data.end_time_param3 == 1 and endWDay < openWDay then
        --     LoggerHelper.Error("server_time config error：end_time_param2=" .. endWDay .. " < openDay" .. openWDay)
        --     return
        -- end

        -- 开服第param3周、周param2、时间param1
        local paramTime1 = DateTimeUtil.GetSec(data.start_time_param1)
        local paramTime2 = DateTimeUtil.GetSec(data.end_time_param1)

        startTime = openTime + _oneWeekTime * (data.start_time_param3 - 1) + _oneDayTime * (startWDay - openWDay) + paramTime1
        endTime = openTime + _oneWeekTime * (data.end_time_param3 - 1) + _oneDayTime * (endWDay - openWDay) + paramTime2

    elseif data.start_time_type == 3 then
        local paramTime1 = DateTimeUtil.GetSec(data.start_time_param1)
        local paramTime2 = DateTimeUtil.GetSec(data.end_time_param1)

        startTime = openTime + _oneDayTime * base.tonumber(data.start_time_param2 - 1) + paramTime1
        endTime = openTime + _oneDayTime * base.tonumber(data.end_time_param2 - 1) + paramTime2
    end

    if endTime <= startTime then
        LoggerHelper.Error("server_time config error ：endTime < startTime")
        return
    end

    if serverTime >= startTime and serverTime <= endTime then
        -- LoggerHelper.Log("111")
        return true
    else
        local oneCircleTime = data.circle * _oneDayTime
        local duration = endTime - startTime
        if oneCircleTime <= duration then
            LoggerHelper.Error("server_time config error：circleTime < endTime - StartTime")
            return
        end
        local gap = serverTime - startTime
        local circle = floor(gap / oneCircleTime)

        if (data.circle_times == -1 or circle <= data.circle_times) then
            local relativeTime = gap % oneCircleTime
            if relativeTime <= duration then
                -- LoggerHelper.Log("222")
                return true
            end
        else
            -- LoggerHelper.Log("=== circle out")
        end
    end
    
    return false
end

ServerTimeDataHelper.Init()
return ServerTimeDataHelper