-- OnlineRewardDataHelper.lua
local OnlineRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local OnlineReward = Class.OnlineReward(ClassTypes.XObject)

local onlineRewardCache = {}

function OnlineReward:__ctor__(cfg)
    self.id = cfg.__id
    self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
    self.time = cfg.__time
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function OnlineRewardDataHelper.GetOnlineReward(id)
    local data = onlineRewardCache[id]
    if data == nil then
        local cfg = XMLManager.online_reward[id]
        if cfg ~= nil then
            data = OnlineReward(cfg)
            onlineRewardCache[data.id] = data
        else
            LoggerHelper.Error("OnlineReward id not exist:" .. id)
        end
    end
    return data
end

function OnlineRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlineRewardDataHelper:GetWorldLevel(id)
    local data = self.GetOnlineReward(id)
    return data and data.world_level or nil
end

function OnlineRewardDataHelper:GetTime(id)
    local data = self.GetOnlineReward(id)
    return data and data.time or nil
end

function OnlineRewardDataHelper:GetReward(id)
    local data = self.GetOnlineReward(id)
    return data and data.reward or nil
end

return OnlineRewardDataHelper
