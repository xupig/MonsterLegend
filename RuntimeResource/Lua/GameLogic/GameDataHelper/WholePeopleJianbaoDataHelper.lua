-- WholePeopleJianbaoDataHelper.lua
local WholePeopleJianbaoDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WholePeopleJianbao = Class.WholePeopleJianbao(ClassTypes.XObject)

local wholePeopleJianbaoCache = {}
function WholePeopleJianbaoDataHelper.ClearCache()
    wholePeopleJianbaoCache = {}
end

function WholePeopleJianbao:__ctor__(cfg)
    self.id = cfg.__id
    self.turn = cfg.__turn
    self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
    self.turn_reward = DataParseHelper.ParseNSuffixSort(cfg.__turn_reward)
    self.important = DataParseHelper.ParseListInt(cfg.__important)
    self.normal = DataParseHelper.ParseListInt(cfg.__normal)
    self.dislpay_first = DataParseHelper.ParseListInt(cfg.__dislpay_first)
    self.dislpay_turn = DataParseHelper.ParseListInt(cfg.__dislpay_turn)
end

function WholePeopleJianbaoDataHelper.GetWholePeopleJianbao(id)
    ClearDataManager:UseCache(CacheNames.whole_people_jianbao, WholePeopleJianbaoDataHelper.ClearCache)
    local data = wholePeopleJianbaoCache[id]
    if data == nil then
        local cfg = XMLManager.whole_people_jianbao[id]
        if cfg ~= nil then
            data = WholePeopleJianbao(cfg)
            wholePeopleJianbaoCache[data.id] = data
        else
            LoggerHelper.Error("WholePeopleJianbao id not exist:" .. id)
        end
    end
    return data
end

function WholePeopleJianbaoDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.whole_people_jianbao:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WholePeopleJianbaoDataHelper:GetTurn(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.turn or nil
end

function WholePeopleJianbaoDataHelper:GetWorldLevel(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.world_level or nil
end

function WholePeopleJianbaoDataHelper:GetReward(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.reward or nil
end

function WholePeopleJianbaoDataHelper:GetTurnReward(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.turn_reward or nil
end

function WholePeopleJianbaoDataHelper:GetImportant(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.important or nil
end

function WholePeopleJianbaoDataHelper:GetNormal(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.normal or nil
end

function WholePeopleJianbaoDataHelper:GetDislpayFirst(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.dislpay_first or nil
end

function WholePeopleJianbaoDataHelper:GetDislpayTurn(id)
    local data = self.GetWholePeopleJianbao(id)
    return data and data.dislpay_turn or nil
end

return WholePeopleJianbaoDataHelper
