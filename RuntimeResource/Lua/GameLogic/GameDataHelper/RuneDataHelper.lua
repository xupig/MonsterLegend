-- RuneDataHelper.lua
local RuneDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Rune = Class.Rune(ClassTypes.XObject)

local runeCache = {}
function RuneDataHelper.ClearCache()
    runeCache = {}
end

function Rune:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.type = DataParseHelper.ParseListInt(cfg.__type)
    self.quality = cfg.__quality
    self.level = cfg.__level
    self.item_com = cfg.__item_com
    self.unlock_floor = cfg.__unlock_floor
    self.random_weight = cfg.__random_weight
end

function RuneDataHelper.GetRune(id)
    ClearDataManager:UseCache(CacheNames.rune, RuneDataHelper.ClearCache)
    local data = runeCache[id]
    if data == nil then
        local cfg = XMLManager.rune[id]
        if cfg ~= nil then
            data = Rune(cfg)
            runeCache[data.id] = data
        else
            LoggerHelper.Error("Rune id not exist:" .. id)
        end
    end
    return data
end

function RuneDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.rune:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function RuneDataHelper:GetName(id)
    local data = self.GetRune(id)
    return data and data.name or nil
end

function RuneDataHelper:GetIcon(id)
    local data = self.GetRune(id)
    return data and data.icon or nil
end

function RuneDataHelper:GetType(id)
    local data = self.GetRune(id)
    return data and data.type or nil
end

function RuneDataHelper:GetQuality(id)
    local data = self.GetRune(id)
    return data and data.quality or nil
end

function RuneDataHelper:GetLevel(id)
    local data = self.GetRune(id)
    return data and data.level or nil
end

function RuneDataHelper:GetItemCom(id)
    local data = self.GetRune(id)
    return data and data.item_com or nil
end

function RuneDataHelper:GetUnlockFloor(id)
    local data = self.GetRune(id)
    return data and data.unlock_floor or nil
end

function RuneDataHelper:GetRandomWeight(id)
    local data = self.GetRune(id)
    return data and data.random_weight or nil
end

return RuneDataHelper
