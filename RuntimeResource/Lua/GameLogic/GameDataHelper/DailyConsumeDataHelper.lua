local DailyConsumeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = require("ServerConfig/public_config")
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

------------------------------世界等级奖励表-------------------
local SpendLevelRewardCfg = Class.SpendLevelRewardCfg(ClassTypes.XObject)

local spendLevelRewardCfgCache = {}
function DailyConsumeDataHelper.ClearCache()
    spendLevelRewardCfgCache = {}
end

function SpendLevelRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.edition = cfg.__edition
    self.level = DataParseHelper.ParseListInt(cfg.__level)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function DailyConsumeDataHelper:GetLevelData(id)
    ClearDataManager:UseCache(CacheNames.spend_level_reward, DailyConsumeDataHelper.ClearCache)
    local data = spendLevelRewardCfgCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.spend_level_reward[id]
    if data~=nil then
	    data = SpendLevelRewardCfg(data)
    end
	spendLevelRewardCfgCache[id] = data
	return data
end

function DailyConsumeDataHelper:GetDailyConsumeDataByArgs(cost, worldLevel)
    local keys = XMLManager.spend_level_reward:Keys()
    local result
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetLevelData(id)
            if data.edition == cost and data.level[1] <= worldLevel 
                    and data.level[2] >= worldLevel then
                result = data
                break
            end
        end
    end
    return result
end

------------------------------每日花费奖励表-------------------
local SpendDailyRewardCfg = Class.SpendDailyRewardCfg(ClassTypes.XObject)

local spendDailyRewardCache = {}
function DailyConsumeDataHelper.ClearSpendCache()
    spendDailyRewardCache = {}
end

function SpendDailyRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.cost = cfg.__cost
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
end

function DailyConsumeDataHelper:GetDailyRewardData(id)
    ClearDataManager:UseCache(CacheNames.spend_day_reward, DailyConsumeDataHelper.ClearSpendCache)
    local data = spendDailyRewardCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.spend_day_reward[id]
    if data~=nil then
	    data = SpendDailyRewardCfg(data)
    end
	spendDailyRewardCache[id] = data
	return data
end

local allCumulativeDailyConsumeCache
function DailyConsumeDataHelper:GetAllCumulativeDailyConsumeData()
    if allCumulativeDailyConsumeCache == nil then
        allCumulativeDailyConsumeCache = {}
        local keys = XMLManager.spend_day_reward:Keys()
        local result = {}
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                table.insert(allCumulativeDailyConsumeCache, {id})
            end
        end
        table.sort(allCumulativeDailyConsumeCache, SortByKey1)
    end
    return allCumulativeDailyConsumeCache
end

return DailyConsumeDataHelper