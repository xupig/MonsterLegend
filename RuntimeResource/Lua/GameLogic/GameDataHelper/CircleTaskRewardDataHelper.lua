-- CircleTaskRewardDataHelper.lua
local CircleTaskRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CircleTaskReward = Class.CircleTaskReward(ClassTypes.XObject)

local circleTaskRewardCache = {}
function CircleTaskRewardDataHelper.ClearCache()
    circleTaskRewardCache = {}
end

function CircleTaskReward:__ctor__(cfg)
    self.id = cfg.__id
    self.lv = DataParseHelper.ParseListInt(cfg.__lv)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.circle_reward = DataParseHelper.ParseMSuffixSort(cfg.__circle_reward)
end

function CircleTaskRewardDataHelper.GetCircleTaskReward(id)
    ClearDataManager:UseCache(CacheNames.circle_task_reward, CircleTaskRewardDataHelper.ClearCache)
    local data = circleTaskRewardCache[id]
    if data == nil then
        local cfg = XMLManager.circle_task_reward[id]
        if cfg ~= nil then
            data = CircleTaskReward(cfg)
            circleTaskRewardCache[data.id] = data
        else
            LoggerHelper.Error("CircleTaskReward id not exist:" .. id)
        end
    end
    return data
end

function CircleTaskRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.circle_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CircleTaskRewardDataHelper:GetLv(id)
    local data = self.GetCircleTaskReward(id)
    return data and data.lv or nil
end

function CircleTaskRewardDataHelper:GetReward(id)
    local data = self.GetCircleTaskReward(id)
    return data and data.reward or nil
end

function CircleTaskRewardDataHelper:GetCircleReward(id)
    local data = self.GetCircleTaskReward(id)
    return data and data.circle_reward or nil
end
function CircleTaskRewardDataHelper:GetCircleRewardByLevel()
    local level = GameWorld.Player().level
    local ids = {}
    local data = XMLManager.circle_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local lv = self:GetLv(v)
            if lv[1] <= level and level <= lv[2] then
                return self:GetCircleReward(v)
            end
        end
    end
    return nil
end

function CircleTaskRewardDataHelper:GetRewardIdByLevel()
    local level = GameWorld.Player().level
    local ids = {}
    local data = XMLManager.circle_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local lv = self:GetLv(v)
            if lv[1] <= level and level <= lv[2] then
                return v
            end
        end
    end
    return nil
end

return CircleTaskRewardDataHelper
