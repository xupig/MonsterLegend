-- TowerDefenseDataHelper.lua
local TowerDefenseDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local TowerDefense = Class.TowerDefense(ClassTypes.XObject)

local towerDefenseCache = {}
function TowerDefenseDataHelper.ClearCache()
    towerDefenseCache = {}
end

function TowerDefense:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.monster = cfg.__monster
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.build_limit = cfg.__build_limit
end

function TowerDefenseDataHelper.GetTowerDefense(id)
    ClearDataManager:UseCache(CacheNames.tower_defense, TowerDefenseDataHelper.ClearCache)
    local data = towerDefenseCache[id]
    if data == nil then
        local cfg = XMLManager.tower_defense[id]
        if cfg ~= nil then
            data = TowerDefense(cfg)
            towerDefenseCache[data.id] = data
        else
            LoggerHelper.Error("TowerDefense id not exist:" .. id)
        end
    end
    return data
end

function TowerDefenseDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.tower_defense:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function TowerDefenseDataHelper:GetType(id)
    local data = self.GetTowerDefense(id)
    return data and data.type or nil
end

function TowerDefenseDataHelper:GetMonster(id)
    local data = self.GetTowerDefense(id)
    return data and data.monster or nil
end

function TowerDefenseDataHelper:GetIcon(id)
    local data = self.GetTowerDefense(id)
    return data and data.icon or nil
end

function TowerDefenseDataHelper:GetDesc(id)
    local data = self.GetTowerDefense(id)
    return data and data.desc or nil
end

function TowerDefenseDataHelper:GetBuildLimit(id)
    local data = self.GetTowerDefense(id)
    return data and data.build_limit or nil
end

return TowerDefenseDataHelper
