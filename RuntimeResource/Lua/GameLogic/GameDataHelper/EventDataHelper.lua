-- EventDataHelper.lua
local EventDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local Event = Class.Event(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

local eventCache = {}
function EventDataHelper.ClearCache()
    eventCache = {}
end

function Event:__ctor__(cfg)
    self.id = cfg.__id
    self.event_id = cfg.__event_id
    -- LoggerHelper.Log("Ash: id: " .. cfg.__id .. " ParseConds:" .. cfg.__conds)
    if not (cfg.__conds == nil or cfg.__conds == "") then
        self.conds = EventDataHelper.ParseConds(cfg.__conds)
    end
    -- self.conds_or = DataParseHelper.ParseMSuffixSort(cfg.__conds_or)
    self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
end

function EventDataHelper.GetEvent(id)
    ClearDataManager:UseCache(CacheNames.event_conds, EventDataHelper.ClearCache)
    local data = eventCache[id]
    if data == nil then
        local cfg = XMLManager.event_conds[id]
        data = Event(cfg)
        eventCache[data.id] = data
    end
    return data
end

function EventDataHelper:GetEventID(id)
    local data = self.GetEvent(id)
    return data and data.event_id or nil
end

function EventDataHelper:GetConds(id, paramId)
    local data = self.GetEvent(id)
    if data and data.conds then
        for _, one_cond in pairs(data.conds) do
            if one_cond[1] == paramId then
                return one_cond[2][1]
            end
        end
    end
    return nil
end

-- function EventDataHelper:GetCondsOr(id)
--     local data = self.GetEvent(id)
--     return data and data.conds_or or nil
-- end
function EventDataHelper:GetFollow(id)
    local data = self.GetEvent(id)
    return data and data.follow or nil
end

--处理各种逻辑判断
local cond_operators = {'=', '}', '{', '@', '#'}--定义条件表达式中的条件判断操作符

--根据一个条件字符串生成复合条件表达式
function EventDataHelper.ParseConds(conds_str)
    local conds_split = EventDataHelper.split_str(conds_str, ';')--多个条件之间用分号隔开，表示与操作
    --LoggerHelper.Log("Ash: ParseConds:" .. PrintTable:TableToStr(conds_split))
    local conds_list = {}
    --    local string = string
    for _, cond_info in ipairs(conds_split) do
        local one_cond
        local cond_key, cond_value, sep_index
        for _, cond_op in pairs(cond_operators) do
            sep_index = string.find(cond_info, cond_op)
            if sep_index ~= nil then
                cond_key = string.sub(cond_info, 1, sep_index - 1)
                cond_value = string.sub(cond_info, sep_index + #cond_op)
                cond_value = EventDataHelper.split_str(cond_value, ',', tonumber)--value可以是多个值
                one_cond = {tonumber(cond_key), cond_value, cond_op}
                break
            end
        end
        if one_cond then
            table.insert(conds_list, one_cond)
        else
            assert(false, "EventDataHelper:ParseConds conds_str=" .. conds_str)
        end
    end
    return conds_list
end

--根据分隔符切割字符串
--func = nil(默认返回str) or tonumber or
function EventDataHelper.split_str(str, delim, func)
    local i = 0
    local j = 1
    local t = {}
    while i ~= nil do
        i = string.find(str, delim, j)
        if i ~= nil then
            if func then
                table.insert(t, func(string.sub(str, j, i - 1)) or string.sub(str, j, i - 1))
            else
                table.insert(t, string.sub(str, j, i - 1))
            end
            j = i + 1
        else
            if func then
                table.insert(t, func(string.sub(str, j)) or string.sub(str, j))
            else
                table.insert(t, string.sub(str, j))
            end
        end
    end
    return t
end

return EventDataHelper
