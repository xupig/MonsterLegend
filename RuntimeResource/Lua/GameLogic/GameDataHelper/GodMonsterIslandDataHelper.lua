-- GodMonsterIslandDataHelper.lua
local GodMonsterIslandDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GodMonsterIsland = Class.GodMonsterIsland(ClassTypes.XObject)

local godMonsterIslandCache = {}
function GodMonsterIslandDataHelper.ClearCache()
    godMonsterIslandCache = {}
end


function GodMonsterIsland:__ctor__(cfg)
    self.id = cfg.__id
    self.floor = cfg.__floor
    self.sence_id = cfg.__sence_id
    self.level_limit = cfg.__level_limit
    self.monster_type = cfg.__monster_type
    self.level = cfg.__level
    self.name = cfg.__name
    self.name_icon = cfg.__name_icon
    self.icon = cfg.__icon
    self.monster_id = cfg.__monster_id
    self.boss_drops = DataParseHelper.ParseNSuffix(cfg.__boss_drops)
    self.boss_region_id = cfg.__boss_region_id
    self.boss_revive_time = cfg.__boss_revive_time
    self.level_show = cfg.__level_show
end

function GodMonsterIslandDataHelper.GetGodMonsterIsland(id)
    ClearDataManager:UseCache(CacheNames.god_monster_island, GodMonsterIslandDataHelper.ClearCache)
    local data = godMonsterIslandCache[id]
    if data == nil then
        local cfg = XMLManager.god_monster_island[id]
        if cfg ~= nil then
            data = GodMonsterIsland(cfg)
            godMonsterIslandCache[data.id] = data
        else
            LoggerHelper.Error("GodMonsterIsland id not exist:" .. id)
        end
    end
    return data
end

function GodMonsterIslandDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.god_monster_island:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GodMonsterIslandDataHelper:GetMonsterByMapId(mapId)
    local data = XMLManager.god_monster_island:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetSenceId(v) == mapId then
            return v
        end
    end
    return nil
end

function GodMonsterIslandDataHelper:GetFloor(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.floor or nil
end

function GodMonsterIslandDataHelper:GetSenceId(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.sence_id or nil
end

function GodMonsterIslandDataHelper:GetLevelLimit(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.level_limit or nil
end

function GodMonsterIslandDataHelper:GetMonsterType(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.monster_type or nil
end

function GodMonsterIslandDataHelper:GetLevel(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.level or nil
end

function GodMonsterIslandDataHelper:GetName(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.name or nil
end

function GodMonsterIslandDataHelper:GetNameIcon(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.name_icon or nil
end

function GodMonsterIslandDataHelper:GetIcon(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.icon or nil
end

function GodMonsterIslandDataHelper:GetMonsterId(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.monster_id or nil
end

function GodMonsterIslandDataHelper:GetBossDrops(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.boss_drops or nil
end

function GodMonsterIslandDataHelper:GetBossRegionId(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.boss_region_id or nil
end

function GodMonsterIslandDataHelper:GetBossReviveTime(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.boss_revive_time or nil
end

function GodMonsterIslandDataHelper:GetLevelShow(id)
    local data = self.GetGodMonsterIsland(id)
    return data and data.level_show or 0
end

return GodMonsterIslandDataHelper
