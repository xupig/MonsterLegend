--FontStyleHelper.lua
local FontStyleHelper = {}
local XMLManager = GameManager.XMLManager

local FontStyleData = Class.FontStyleData(ClassTypes.XObject)
local Color = Color
local ClearDataManager = PlayerManager.ClearDataManager

function FontStyleData:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.color = cfg.__color
    self.size = cfg.__size
    self.bold = cfg.__bold
    self.italic = cfg.__italic
    self.underline = cfg.__underline
end

local FontStyleCache = {}
function FontStyleHelper.ClearCache()
    FontStyleCache = {}
end

--根据id获取数据项
function FontStyleHelper.GetFontStyleData(id)
    ClearDataManager:UseCache(CacheNames.font_style, FontStyleHelper.ClearCache)
    local data = FontStyleCache[id]
    if data ~= nil then
        return data
    end
    data = XMLManager.font_style[id]
    
    if data ~= nil then
        data = FontStyleData(data)
    else
        LoggerHelper.Error("can not read from font_style.pbufs")
    end
    
    return data
end

local hexTable = {[0] = 0, [1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 6, [7] = 7, [8] = 8, [9] = 9, a = 10,
    A = 10, b = 11, B = 11, c = 12, C = 12, d = 13, D = 13, e = 14, E = 14, f = 15, F = 15, }
--1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
--A, B, C, D, E, F
--将#11223344 装换为Color(r, g, b, a)
function FontStyleHelper:StringCastToColor(colorString)
    local r = string.sub(colorString, 2, 3)
    local g = string.sub(colorString, 4, 5)
    local b = string.sub(colorString, 6, 7)
    local a = string.sub(colorString, 8, 9)
    
    if a == nil or a == "" then
        return Color.New(self:HexCastToDec(r) / 255, self:HexCastToDec(g) / 255, self:HexCastToDec(b) / 255, 1)
    else
        return Color.New(self:HexCastToDec(r) / 255, self:HexCastToDec(g) / 255, self:HexCastToDec(b) / 255, self:HexCastToDec(a) / 255)
    end
end

function FontStyleHelper:GetFontColor(id)
    local data = self.GetFontStyleData(id)
    -- LoggerHelper.Log("Ash: GetFontColor: " .. data.color .. PrintTable:TableToStr(self:StringCastToColor(data.color)))
    return self:StringCastToColor(data.color)
end

--将十六进制'xx'装换成十进制
function FontStyleHelper:HexCastToDec(twoChars)
    local first = hexTable[string.sub(twoChars, 1, 1)]
    local last = hexTable[string.sub(twoChars, 2, 2)]
    if first == nil then
        first = hexTable[tonumber(string.sub(twoChars, 1, 1))]
    end
    if last == nil then
        last = hexTable[tonumber(string.sub(twoChars, 2, 2))]
    end
    return first * 16 + last
end

return FontStyleHelper
