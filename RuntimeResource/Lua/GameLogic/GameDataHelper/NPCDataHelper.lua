-- NPCDataHelper.lua
local NPCDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local NPCCfgData = Class.NPCCfgData(ClassTypes.XObject)
local table = table

function NPCCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.npc_distinguish = cfg.__npc_distinguish
	self.name = cfg.__name
	self.function_honor = cfg.__function_honor
	self.model = cfg.__model
	self.model_action_standby = cfg.__model_action_standby
	self.map = cfg.__map
	self.location = DataParseHelper.ParseListInt(cfg.__location)
	self.dialog_txt = DataParseHelper.ParseListInt(cfg.__dialog_txt)
	self.dialog_voice_distance = cfg.__dialog_voice_distance
	self.scale = cfg.__scale
	self.npc_facade = cfg.__npc_facade
	self.npc_height = cfg.__npc_height
	self.npc_function = cfg.__npc_function
	self.npc_function_chinese = cfg.__npc_function_chinese
	self.npc_location = cfg.__npc_location
	self.npc_camera = DataParseHelper.ParseListInt(cfg.__npc_camera)
	self.npc_visible = cfg.__npc_visible
	self.npc_ride_model = cfg.__npc_ride_model
end

local npcDataCache = {}

function NPCDataHelper.GetNPCData(id)
	local data = npcDataCache[id]
	if data == nil then
		local cfg = XMLManager.npc[id]
        if cfg ~= nil then
            data = NPCCfgData(cfg)
            npcDataCache[data.id] = data
        else
            LoggerHelper.Error("NPC id not exist:" .. id)
        end
	end
	return data
end

function NPCDataHelper.GetNPCName(id)
	local data = NPCDataHelper.GetNPCData(id)
	local name = data and data.name or 0
	return LanguageDataHelper.GetLanguageData(name)
end

function NPCDataHelper.GetModel(id)
	local data = NPCDataHelper.GetNPCData(id)
    return data and data.model or nil
end

function NPCDataHelper.GetNPCHeight(id)
	local data = NPCDataHelper.GetNPCData(id)
    return data and data.npc_height * 0.01 or nil
end

function NPCDataHelper.GetNPCFunction(id)
	local data = NPCDataHelper.GetNPCData(id)
    return data and data.npc_function or nil
end

function NPCDataHelper.GetNPCFunctionChinese(id)
	local data = NPCDataHelper.GetNPCData(id)
    return data and data.npc_function_chinese or nil
end

function NPCDataHelper.GetNPCFunctionHonor(id)
	local data = NPCDataHelper.GetNPCData(id)
	if data.function_honor == 0 then
		return ""
	end
	local name = LanguageDataHelper.GetLanguageData(data.function_honor)
	return name
end

function NPCDataHelper.GetNPCCamera(id)
	local data = NPCDataHelper.GetNPCData(id)	
    return data and data.npc_camera or nil
end

function NPCDataHelper.GetNPCFacade(id)
	local data = NPCDataHelper.GetNPCData(id)
	return data and data.npc_facade or nil
end

function NPCDataHelper.GetNPCLocation(id)
	local data = NPCDataHelper.GetNPCData(id)
	return data and data.npc_location or nil
end

function NPCDataHelper.GetNPCScale(id)
	local data = NPCDataHelper.GetNPCData(id)
	return data and data.scale *0.01 or 1
end

function NPCDataHelper.GetNPCStandby(id)
	local data = NPCDataHelper.GetNPCData(id)
	return data and data.model_action_standby or 0
end

function NPCDataHelper.GetNPCRideModel(id)
	local data = NPCDataHelper.GetNPCData(id)
	return data and data.npc_ride_model or 0
end


function NPCDataHelper.Init()

end

NPCDataHelper.Init()
return NPCDataHelper
