-- DragonsoulAttriDataHelper.lua
local DragonsoulAttriDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local DragonsoulAttri = Class.DragonsoulAttri(ClassTypes.XObject)

local dragonsoulAttriCache = {}
function DragonsoulAttriDataHelper.ClearCache()
    dragonsoulAttriCache = {}
end

function DragonsoulAttri:__ctor__(cfg)
    self.id = cfg.__id
    self.ds_id = cfg.__ds_id
    self.level = cfg.__level
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.resolve = DataParseHelper.ParseMSuffix(cfg.__resolve)
end

function DragonsoulAttriDataHelper.GetDragonsoulAttri(id)
    ClearDataManager:UseCache(CacheNames.dragonsoul_attri, DragonsoulAttriDataHelper.ClearCache)
    local data = dragonsoulAttriCache[id]
    if data == nil then
        local cfg = XMLManager.dragonsoul_attri[id]
        if cfg ~= nil then
            data = DragonsoulAttri(cfg)
            dragonsoulAttriCache[data.id] = data
        else
            LoggerHelper.Error("DragonsoulAttri id not exist:" .. id)
        end
    end
    return data
end

function DragonsoulAttriDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.dragonsoul_attri:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function DragonsoulAttriDataHelper:GetDsId(id)
    local data = self.GetDragonsoulAttri(id)
    return data and data.ds_id or nil
end

function DragonsoulAttriDataHelper:GetLevel(id)
    local data = self.GetDragonsoulAttri(id)
    return data and data.level or nil
end

function DragonsoulAttriDataHelper:GetAttri(id)
    local data = self.GetDragonsoulAttri(id)
    return data and data.attri or nil
end

function DragonsoulAttriDataHelper:GetCost(id)
    local data = self.GetDragonsoulAttri(id)
    return data and data.cost or nil
end

function DragonsoulAttriDataHelper:GetResolve(id)
    local data = self.GetDragonsoulAttri(id)
    return data and data.resolve or nil
end

function DragonsoulAttriDataHelper:GetAttByIdle(id,level)
    if self._cfgAtt and self._cfgAtt[id]  and self._cfgAtt[id][level] then
        return self._cfgAtt[id][level]
    end
    if self._cfgAtt==nil then
        self._cfgAtt={}
    end
    local ids = self:GetAllId()
    for l,id in ipairs(ids) do
        if id ~=0 then
            local dsid = self:GetDsId(id)
            local le = self:GetLevel(id)
            if self._cfgAtt[dsid]==nil then
                self._cfgAtt[dsid]={}
                self._cfgAtt[dsid][le]={}
            elseif self._cfgAtt[dsid][le]==nil then
                self._cfgAtt[dsid][le]={}
            end
            self._cfgAtt[dsid][le]=self.GetDragonsoulAttri(id)
        end
    end
    if self._cfgAtt[id]  and self._cfgAtt[id][level] then
        return self._cfgAtt[id][level]
    else
        return nil
    end
end

return DragonsoulAttriDataHelper
