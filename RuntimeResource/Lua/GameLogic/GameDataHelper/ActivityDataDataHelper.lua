-- ActivityDataDataHelper.lua
local ActivityDataDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ActivityData = Class.ActivityData(ClassTypes.XObject)

local activityDataCache = {}
function ActivityDataDataHelper.ClearCache()
    activityDataCache = {}
end

function ActivityData:__ctor__(cfg)
    self.id = cfg.__id
    self.activity_id = cfg.__activity_id
    self.calendar = DataParseHelper.ParseListInt(cfg.__calendar)
    self.time = cfg.__time
end

function ActivityDataDataHelper.GetActivityData(id)
    ClearDataManager:UseCache(CacheNames.activity_data, ActivityDataDataHelper.ClearCache)
    local data = activityDataCache[id]
    if data == nil then
        local cfg = XMLManager.activity_data[id]
        if cfg ~= nil then
            data = ActivityData(cfg)
            activityDataCache[data.id] = data
        else
            LoggerHelper.Error("ActivityData id not exist:" .. id)
        end
    end
    return data
end

function ActivityDataDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity_data:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityDataDataHelper:GetActivityId(id)
    local data = self.GetActivityData(id)
    return data and data.activity_id or nil
end

function ActivityDataDataHelper:GetCalendar(id)
    local data = self.GetActivityData(id)
    return data and data.calendar or nil
end

function ActivityDataDataHelper:GetTime(id)
    local data = self.GetActivityData(id)
    return data and data.time or nil
end

function ActivityDataDataHelper:GetCalendarData()
    local times = {}
    local data = XMLManager.activity_data:Keys()
    local datas = {}
    for i, v in ipairs(data) do
        if v ~= 0 then
            if not table.containsValue(times,self:GetTime(v)) then
                table.insert(times, self:GetTime(v))
            end
        end
    end

    table.sort(times, function(a,b)
        local numA = string.split(a, ":")[1] * 60 + string.split(a, ":")[2]
        local numB = string.split(b, ":")[1] * 60 + string.split(b, ":")[2]
        return numA < numB
    end)

    for i, v in ipairs(times) do
        local cData = {}
        cData[10] = v
        table.insert(datas, cData)
    end

    for _, id in ipairs(data) do
        for i=1,#datas do
            if self:GetTime(id) == datas[i][10] then
                local calendar = self:GetCalendar(id)
                for __,num in ipairs(calendar) do
                    datas[i][num] = self:GetActivityId(id)
                end
                break                
            end
        end
    end

    return datas
end

return ActivityDataDataHelper
