-- FunctionOpenDataHelper.lua
local FunctionOpenDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local MenuConfig = GameConfig.MenuConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByPosition(a,b)
    return tonumber(a.position[1]) < tonumber(b.position[1])
end

local FunctionOpen = Class.FunctionOpen(ClassTypes.XObject)
local PanelOpen = Class.PanelOpen(ClassTypes.XObject)

local functionOpenCache = {}
local panelOpenCache = {}
local panelOpenConditionCache = {} --界面开启条件缓存
local functionOpenStuctCache = {}
local functionSecTabCount = {} 

-----------------------------------界面开启配置-------------------------------------------
function PanelOpen:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.icon_anim = cfg.__icon_anim
    self.describe = cfg.__describe
    self.titleIcon = cfg.__title_icon
    self.button_type = cfg.__button_type
    self.position = DataParseHelper.ParseListInt(cfg.__position)
    self.button_func = DataParseHelper.ParseNSuffix(cfg.__button_func)
    self.show_condition = DataParseHelper.ParseNSuffix(cfg.__show_condition)
    self.circle = cfg.__circle
end

function FunctionOpenDataHelper:GetPanelOpenName(id)
    local data = self.GetPanelOpen(id)
    if data ~= nil and data.name ~= nil then
        return LanguageDataHelper.GetContent(data.name)
    end
    return nil
end

function FunctionOpenDataHelper:GetPanelOpenIcon(id)
    local data = self.GetPanelOpen(id)
    return data and data.icon or nil
end

function FunctionOpenDataHelper:GetPanelOpenIconAnim(id)
    local data = self.GetPanelOpen(id)
    return data and data.icon_anim or nil
end

function FunctionOpenDataHelper:GetDescribe(id)
    local data = self.GetPanelOpen(id)
    if data ~= nil and data.describe ~= nil then
        return LanguageDataHelper.GetContent(data.describe)
    end
    return nil
end

function FunctionOpenDataHelper:GetTitleIcon(id)
    local data = self.GetPanelOpen(id)
    return data and data.titleIcon or nil
end

function FunctionOpenDataHelper:GetButtonType(id)
    local data = self.GetPanelOpen(id)
    return data and data.button_type or nil
end

function FunctionOpenDataHelper:GetPosition(id)
    local data = self.GetPanelOpen(id)
    return data and data.position or nil
end

function FunctionOpenDataHelper:GetButtonFunc(id)
    local data = self.GetPanelOpen(id)
    return data and data.button_func or nil
end

function FunctionOpenDataHelper:GetCircle(id)
    local data = self.GetPanelOpen(id)
    return data and data.circle or nil
end

function FunctionOpenDataHelper.GetPanelOpen(id)
    local data = panelOpenCache[id]
    if data == nil then
        local cfg = XMLManager.panel_open[id]
        if cfg ~= nil then
            data = PanelOpen(cfg)
            panelOpenCache[data.id] = data
        else
            LoggerHelper.Error("PanelOpen id not exist:" .. id)
        end
    end
    return data
end

function FunctionOpenDataHelper:GetAllIdByButtonType(buttonType)
    local ids = {}
    local data = XMLManager.panel_open:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetButtonType(v) == buttonType then
            table.insert(ids, v)
        end
    end
    return ids
end

-- function FunctionOpenDataHelper:GetAllCfgByButtonType(buttonType)
--     local cfgs = {}
--     local data = XMLManager.panel_open:Keys()
--     for i, v in ipairs(data) do
--         if v ~= 0 and self:GetButtonType(v) == buttonType then
--             local d = FunctionOpenDataHelper.GetPanelOpen(v)
--             table.insert(cfgs, d)
--         end
--     end
--     table.sort(cfgs,SortByPosition)
--     return cfgs
-- end

function FunctionOpenDataHelper:GetAllMainMenuCfg()
    local cfgs = {}
    local data = XMLManager.panel_open:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and (self:GetButtonType(v) == MenuConfig.MAIN_MENU or self:GetButtonType(v) == MenuConfig.DYNAMIC_MAIN_MENU)then
            local d = FunctionOpenDataHelper.GetPanelOpen(v)
            table.insert(cfgs, d)
        end
    end
    table.sort(cfgs,SortByPosition)
    return cfgs
end

--获取面板标题名
function FunctionOpenDataHelper:GetTitleByPanelName(panelName)
    local keys = XMLManager.panel_open:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = FunctionOpenDataHelper.GetPanelOpen(id)
            if data.button_func then
                for k,v in pairs(data.button_func) do
                    for i=1,#v do
                        if v[i] == panelName then
                            return data.name
                        end
                    end
                end
            end
        end
    end
    return nil
end

-----------------------------------功能开启配置-------------------------------------------
function FunctionOpen:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.icon_show = cfg.__icon_show
    self.preview = cfg.__preview
    self.preview_text = cfg.__preview_text
    self.preview_level = DataParseHelper.ParseListInt(cfg.__preview_level)
    self.condition = DataParseHelper.ParseNSuffix(cfg.__condition)
    self.button_func1 = DataParseHelper.ParseNSuffix(cfg.__button_func1)
    self.button_func2 = DataParseHelper.ParseNSuffix(cfg.__button_func2)
    self.button_func3 = DataParseHelper.ParseNSuffix(cfg.__button_func3)
    self.action_show = cfg.__action_show
end

function FunctionOpen:isOpen()
    if condition[2] then
        return condition[2] <= GameWorld.Player().level
    end
    return true
end

function FunctionOpenDataHelper:GetFunctionOpenName(id)
    local data = self.GetFunctionOpen(id)
    return data and data.name or nil
end

function FunctionOpenDataHelper:GetFunctionOpenIcon(id)
    local data = self.GetFunctionOpen(id)
    return data and data.icon or nil
end

function FunctionOpenDataHelper:GetFunctionOpenIconShow(id)
    local data = self.GetFunctionOpen(id)
    return data and data.icon_show or nil
end

function FunctionOpenDataHelper:GetPreview(id)
    local data = self.GetFunctionOpen(id)
    return data and data.preview or nil
end

function FunctionOpenDataHelper:GetPreviewText(id)
    local data = self.GetFunctionOpen(id)
    return data and data.preview_text or 0
end

function FunctionOpenDataHelper:GetPreviewLevel(id)
    local data = self.GetFunctionOpen(id)
    return data and data.preview_level or nil
end

function FunctionOpenDataHelper:GetCondition(id)
    local data = self.GetFunctionOpen(id)
    return data and data.condition or nil
end

function FunctionOpenDataHelper:GetButtonFunc1(id)
    local data = self.GetFunctionOpen(id)
    return data and data.button_func1 or nil
end

function FunctionOpenDataHelper:GetButtonFunc2(id)
    local data = self.GetFunctionOpen(id)
    return data and data.button_func2 or nil
end

function FunctionOpenDataHelper:GetButtonFunc3(id)
    local data = self.GetFunctionOpen(id)
    return data and data.button_func3 or nil
end

function FunctionOpenDataHelper.GetFunctionOpen(id)
    local data = functionOpenCache[id]
    if data == nil then
        local cfg = XMLManager.function_open[id]
        if cfg ~= nil then
            data = FunctionOpen(cfg)
            functionOpenCache[data.id] = data
        else
            LoggerHelper.Error("FunctionOpen id not exist:" .. id)
        end
    end
    return data
end

function FunctionOpenDataHelper:GetFuncsByPanelName(panelName)
    local ids = {}
    local data = XMLManager.function_open:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local button_func3 = FunctionOpenDataHelper:GetButtonFunc3(v)
            if button_func3  and button_func3[1] and button_func3[1][1] == panelName then
                table.insert(button_func3, v) 
            end
        end
    end
    return {}
end

function FunctionOpenDataHelper:GetButtonFunc3ByPanelName(panelName)
    local ids = {}
    local data = XMLManager.function_open:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local button_func3 = FunctionOpenDataHelper:GetButtonFunc3(v)
            if button_func3  and button_func3[1][1] == panelName then
                return button_func3
            end
        end
    end
    return nil
end

--一个面板内所有功能数据
function FunctionOpenDataHelper:GetFunctionOpensInPanel(panelName)
    if functionOpenStuctCache[panelName] == nil then
        local t = {}
        local keys = XMLManager.function_open:Keys()
        local firstTabCN = {}
        local secTabCount = 0
        local index = 0
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                local data = FunctionOpenDataHelper.GetFunctionOpen(id)
                if data.button_func3 then
                    --LoggerHelper.Error("button_func3"..data.button_func3[1][1])
                    if data.button_func3[1][1] == panelName then
                        -- LoggerHelper.Error("button_func3"..data.button_func3[1][1])
                        -- LoggerHelper.Log(data)
                        --有一级标签
                        if data.button_func2 then
                            --k,标签中文id
                            for k,v in pairs(data.button_func2) do
                                if firstTabCN[k] == nil then
                                    index = index + 1
                                    t[index] = {}
                                    firstTabCN[k] = index
                                end
                                table.insert(t[firstTabCN[k]],data)
                            end 
                        --仅有二级标签
                        else
                            if t[1] == nil then
                                t[1] = {}
                            end
                            table.insert(t[1],data)
                        end
                    end
                end
            end
        end

        for i=1,#t do
            secTabCount = math.max(secTabCount,#t[i])
        end
        functionOpenStuctCache[panelName] = t
        functionSecTabCount[panelName] = secTabCount
    end
    
    return functionOpenStuctCache[panelName],functionSecTabCount[panelName]
end

--获取所有用于显示
function FunctionOpenDataHelper:GetAllIdForTipsPanel()
    local idsForTips = {}
    local ids = {}
    local keys = XMLManager.function_open:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = FunctionOpenDataHelper.GetFunctionOpen(id)
            ids[i] = data.id
            if data.action_show and data.action_show  == 1 then
                idsForTips[data.id] = 1
            end
        end
    end
    return ids,idsForTips
end

--功能开启预览数据
function FunctionOpenDataHelper:GetFuncPreviewData(level)
    local resultData = {}
    local keys = XMLManager.function_open:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = FunctionOpenDataHelper.GetFunctionOpen(id)
            if data.preview_level ~= nil then
                local addData = false
                --这个功能是转职且未开启
                --转职特殊判断
                if data.condition and data.condition[MenuConfig.FIND_NPC] then
                    if data.preview_level and level < data.preview_level[1] then
                        addData = true
                    end
                --普通未开启功能
                else
                    addData = not self:CheckFunctionOpen(data.id)
                end
                --获取未开启的且要预览的功能
                if addData then
                    local tmp = {["id"]=data.id, ["order"] = data.preview_level[1]}
                    table.insert(resultData, tmp)
                end
            end
        end
    end
    table.sort(resultData, SortByOrder)
    if #resultData > 0 then
        return FunctionOpenDataHelper.GetFunctionOpen(resultData[1].id)
    end
    return
end

--根据面板名字从func_open表获取面板开启条件
function FunctionOpenDataHelper:GetPanelOpenCondition(panelName)
    if panelOpenConditionCache[panelName] == nil then
        local keys = XMLManager.function_open:Keys()
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                local data = FunctionOpenDataHelper.GetFunctionOpen(id)
                if data.button_func3 and data.button_func3[1][1] == panelName then
                    panelOpenConditionCache[panelName] = data.condition
                    break
                end
            end
        end
    end
    return panelOpenConditionCache[panelName]
end

--检查面板开启
function FunctionOpenDataHelper:CheckPanelOpen(panelId)
    --MJ包屏蔽功能
    if GameWorld.SpecialExamine and GameWorld.PanelCloseIds[panelId] then
        return false
    end

    local panelData = FunctionOpenDataHelper.GetPanelOpen(panelId)
    --优先判断panel配置下的show_condition
    if panelData.show_condition then
        return self:CheckFunctionOpenByCondition(panelData.show_condition)
    end
    local condition
    if panelData.button_func then
        if panelData.button_func[MenuConfig.SHOW_PANEL] then
            if panelId == 16 then--转职
                condition = {[9] = {
                    [1] = GameWorld.Player():GetVocationChange() + 1
                }}
            else
                condition = FunctionOpenDataHelper:GetPanelOpenCondition(panelData.button_func[MenuConfig.SHOW_PANEL][1])
            end
            
        end

        if panelData.button_func[MenuConfig.SHOW_MANY_PANEL] then
            condition = FunctionOpenDataHelper:GetPanelOpenCondition(panelData.button_func[MenuConfig.SHOW_MANY_PANEL][2])
        end
    end
    return FunctionOpenDataHelper:CheckFunctionOpenByCondition(condition)
end

--检查功能开启
function FunctionOpenDataHelper:CheckFunctionOpen(funcId)
    --MJ包屏蔽功能
    if GameWorld.SpecialExamine and GameWorld.FunctionCloseIds[funcId] then
        return false
    end
    local condition = FunctionOpenDataHelper:GetCondition(funcId)
    return FunctionOpenDataHelper:CheckFunctionOpenByCondition(condition)
end

function FunctionOpenDataHelper:CheckFunctionOpenByCondition(condition)
    if condition == nil then
        return true
    end
    local player = GameWorld.Player()

    --表示个人等级（大于等于）
    if condition[public_config.LIMIT_LEVEL] and condition[public_config.LIMIT_LEVEL][1] > player.level then
        return false
    end

    --个人等级小于满足条件
    if condition[public_config.LIMIT_LEVEL_LT] and condition[public_config.LIMIT_LEVEL_LT][1] < player.level then
        return false
    end

    --任务提交
    if condition[public_config.LIMIT_COMMITED_TASK] and not PlayerManager.TaskManager:GetTaskHasCommited(condition[public_config.LIMIT_COMMITED_TASK][1]) then
        return false
    end

    --转职任务
    if condition[public_config.LIMIT_HAS_VOCATION_TASK] and (not PlayerManager.VocationChangeManager:IsDoingVocationChangeByNum(condition[public_config.LIMIT_HAS_VOCATION_TASK][1])) then
        return false
    end

    --首充判定 0:未首充显示 1：完成首充第二天起显示 2:首充当日显示
    if condition[10] then
        if condition[public_config.LIMIT_FIRST_CHARGE][1] == 0 and player.first_charge_reward_time > 0 then
            return false
        end
        if condition[public_config.LIMIT_FIRST_CHARGE][1] == 1 and (player.first_charge_reward_time == 0 or
        (player.first_charge_reward_time > 0 and DateTimeUtil.SameDay(player.first_charge_reward_time))) then
            return false
        end
        if condition[public_config.LIMIT_FIRST_CHARGE][1] == 2 and (player.first_charge_reward_time == 0 or 
        (player.first_charge_reward_time > 0 and not DateTimeUtil.SameDay(player.first_charge_reward_time))) then
            return false
        end
    end

    return true
end 

function FunctionOpenDataHelper:GetFunctionPanelIconPosition(funcId)
    local row = 0
    local index = 0 --从0开始
    local bigCount = 0 --大图标数量
    local funcOpenCfg = FunctionOpenDataHelper.GetFunctionOpen(funcId)
    local keys = XMLManager.panel_open:Keys()
    local panelOpenCfg
    --获取功能所在面板
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = FunctionOpenDataHelper.GetPanelOpen(id)
            if data.button_func then
               for _,v in pairs(data.button_func) do
                    for i=1,#v do
                        if v[i] ==  funcOpenCfg.button_func3[1][1] then
                            --LoggerHelper.Error("data.button_func[8][1]"..data.button_func[8][1])
                            panelOpenCfg = data
                            break
                        end
                    end
               end
            end
        end
    end

    --主面板计算所在位置
    local menuType = panelOpenCfg.button_type or 0
    if menuType == MenuConfig.MAIN_MENU then
        row = math.floor(panelOpenCfg.position[1] / 100)
        local posIndex = panelOpenCfg.position[1] % 100
        local fo = FunctionOpenDataHelper:GetAllMainMenuCfg()
        for i, cfg in ipairs(fo) do
            if cfg.id ~= panelOpenCfg.cfg and cfg.position then
                local row1 = math.floor(cfg.position[1] / 100)
                local index1 = cfg.position[1] % 100
                if row1 == row and index1<posIndex then
                    if cfg.button_type == MenuConfig.MAIN_MENU and FunctionOpenDataHelper:CheckPanelOpen(cfg.id) then
                        index = index + 1
                    elseif cfg.button_type == MenuConfig.DYNAMIC_MAIN_MENU and PlayerManager.FunctionOpenManager:CheckDynamicMenuOpen(cfg.id) then
                        index = index + 1
                    end
                end
                if row1 == 0 and FunctionOpenDataHelper:CheckPanelOpen(cfg.id) then
                    bigCount = bigCount + 1
                end
            end
        end
    end

    return menuType,row,index,bigCount
end

return FunctionOpenDataHelper
