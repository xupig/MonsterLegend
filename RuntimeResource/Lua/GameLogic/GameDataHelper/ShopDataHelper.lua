local ShopDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local OpenTimeDataHelper = GameDataHelper.OpenTimeDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ShopType = GameConfig.EnumType.ShopType
local ShopPageType = GameConfig.EnumType.ShopPageType
local ClearDataManager = PlayerManager.ClearDataManager

local _shopData = {}
function ShopDataHelper.ClearCache()
    _shopData = {}
end

local _shopTypeAndTagData = nil
function ShopDataHelper.ClearShopTypeAndTagDataCache()
    _shopTypeAndTagData = nil
end

local _shopTagsData = nil
function ShopDataHelper.ClearShopTagsDataCache()
    _shopTagsData = nil
end


local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

function ShopDataHelper:InitShopData()
    local data
    local keys = XMLManager.shop_all:Keys()
    for i=1,#keys do
        data = XMLManager.shop_all[keys[i]]
        _shopData[keys[i]] = data
    end
end

function ShopDataHelper:GetShopData(id)
    ClearDataManager:UseCache(CacheNames.shop_all, ShopDataHelper.ClearCache)
    if _shopData[id] == nil then
        self:InitShopData()
    end
    return _shopData[id]
end

function ShopDataHelper:InitShopTypeAndTagData()
    _shopTypeAndTagData = {}
    local data
    local markList = {}
    local keys = XMLManager.shop_all:Keys()
    for i=1,#keys do
        data = XMLManager.shop_all[keys[i]]
        if data.__state == 1 then
            if markList[data.__page_type] == nil then
                _shopTypeAndTagData[data.__page_type] = {}
                markList[data.__page_type] = 1
            end
            local tmp = {}
            tmp.id = keys[i]
            tmp.order = data.__order
            table.insert(_shopTypeAndTagData[data.__page_type],tmp)
        end
    end
    for tag,_ in pairs(markList) do
        table.sort(_shopTypeAndTagData[tag], SortByOrder)
    end
end

function ShopDataHelper:GetLevelLimit(id)
    local data = self:GetShopData(id)
    if data ~= nil and data.__level ~= nil then
        return DataParseHelper.ParseListInt(data.__level)
    end
    return nil
end

function ShopDataHelper:GetVIPLevelLimit(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__vip_limit
    end
    return 0
end

function ShopDataHelper:GetShopDataByTagAndLevel(tag, level, vocGroup)
    ClearDataManager:UseCache(CacheNames.shopTypeAndTagData, ShopDataHelper.ClearShopTypeAndTagDataCache)
    if _shopTypeAndTagData == nil then
        self:InitShopTypeAndTagData()
    end
    local result = {}
    for k,v in ipairs(_shopTypeAndTagData[tag]) do
        local flag = self:GetShopInVocationList(v.id, vocGroup) 
        if flag then
            local levelData = self:GetLevelLimit(v.id)
            if levelData ~= nil then
                if levelData[1] <= level and level <= levelData[2] then
                    table.insert(result, v)
                end
            else
                table.insert(result, v)
            end
        end
    end
    return result
end

--不加入结婚商城
function ShopDataHelper:InitShopTagsData()
    _shopTagsData = {}
    local data
    local markList = {}
    local keys = XMLManager.shop_all:Keys()
    for i=1,#keys do
        data = XMLManager.shop_all[keys[i]]
        if data.__state == 1 and data.__page_type ~= ShopPageType.Marriage then
            if markList[data.__page_type] == nil then
                markList[data.__page_type] = 1
                local tmp = {}
                tmp.id = data.__page_type
                tmp.order = ShopDataHelper:GetShopTagOrder(data.__page_type)
                table.insert(_shopTagsData,tmp)
            end
        end
    end
    table.sort(_shopTagsData, SortByOrder)
end

function ShopDataHelper:GetShopPages()
    ClearDataManager:UseCache(CacheNames.shopTagsData, ShopDataHelper.ClearShopTagsDataCache)
    if _shopTagsData == nil then
        self:InitShopTagsData()
    end
    return _shopTagsData
end

function ShopDataHelper:GetShopIndexByTag(tag)
    if _shopTagsData == nil then
        self:InitShopTagsData()
    end
    local index = 0
    local data = _shopTagsData
    for i=1, #data do
        if tag == data[i].id then
            index = i-1
            break
        end
    end
    return index
end


function ShopDataHelper:GetItemId(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__item_id
    end
    return 0
end

function ShopDataHelper:GetItemIcon(id)
    local itemId= self:GetItemId(id)
    return ItemDataHelper.GetIcon(itemId)
end

function ShopDataHelper:GetItemName(id)
    local itemId= self:GetItemId(id)
    return ItemDataHelper.GetItemNameWithColor(itemId)
end

function ShopDataHelper:GetItemDesc(id)
    local itemId= self:GetItemId(id)
    return ItemDataHelper.GetDescription(itemId)
end

function ShopDataHelper:GetItemTypeAndUnitPrice(id)
    local data = self:GetShopData(id)
    local isInOpen = self:IsInOpen(id)
    local type, price = self:GetItemTypeAndOriPrice(id)
    if isInOpen then
        local discount = self:GetDiscount(id)
        price = price * discount
    end
    return type, price
end

function ShopDataHelper:GetItemTypeAndOriPrice(id)
    local data = self:GetShopData(id)
    local tmp = DataParseHelper.ParseMSuffix(data.__price)
    local type, price
    for _type, _price in pairs(tmp) do
        type = _type
        price = _price
    end
    return type, price
end

function ShopDataHelper:GetDiscount(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__discount * 0.01
    end
    return 1
end

function ShopDataHelper:IsInOpen(id)
    local discount = self:GetDiscount(id) or 100
    if discount < 100 then
        return true
    end
    return false
end

function ShopDataHelper:IsInDiscount(id)
     local ret = false
     if self:IsInOpen(id) and self:GetDiscount(id) ~= 1 then
        ret = true
     end
     return ret
end

function ShopDataHelper:GetTag(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__tag
    end
    return 0
end

function ShopDataHelper:GetWeekLimit(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__week_limit or 0
    end
    return 0
end

function ShopDataHelper:GetDayLimit(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__day_limit or 0
    end
    return 0
end

function ShopDataHelper:GetVersion(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__version
    end
    return 0
end

function ShopDataHelper:GetPageType(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return data.__page_type
    end
    return 0
end

function ShopDataHelper:GetShopDataByItemId(itemId)
    local result = {}
    local keys = XMLManager.shop_all:Keys()
    for i=1,#keys do
        local data = XMLManager.shop_all[keys[i]]
        if data.__item_id == itemId then
            table.insert(result,data)
        end
    end
    return result
end

function ShopDataHelper:GetShopVocation(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return DataParseHelper.ParseListInt(data.__vocation)
    end
end

function ShopDataHelper:GetShopInVocationList(id, vocGroup)
    local data = self:GetShopVocation(id)
    if data == nil then
        return true
    end
    for k,v in pairs(data) do
        if v == vocGroup then
            return true
        end
    end
    return false
end

function ShopDataHelper:GetShopModel(id)
    local data = self:GetShopData(id)
    if data ~= nil then
        return DataParseHelper.ParseMSuffix(data.__mode_vocation)
    end
end





------------------------------代币商店表
function ShopDataHelper:GetTokenData(id)
    local data = XMLManager.shop_token[id]
    return data
end

function ShopDataHelper:GetAllTokenData()
    local tokenData = {}
    local data
    local keys = XMLManager.shop_token:Keys()
    for i=1,#keys do
        data = XMLManager.shop_token[keys[i]]
        local tmp = {}
        tmp.id = keys[i]
        tmp.order = data.__order
        table.insert(tokenData,tmp)
    end
    table.sort(tokenData,SortByOrder)
    return tokenData
end

function ShopDataHelper:GetTokenIcon(id)
    local data = self:GetTokenData(id)
    if data ~= nil then
        return data.__icon
    end
    return 0
end

function ShopDataHelper:GetTokenName(id)
    local data = self:GetTokenData(id)
    local lang_id = 0
    if data ~= nil then
        lang_id = data.__name
    end
    return LanguageDataHelper.GetContent(lang_id)
end

function ShopDataHelper:GetTokenShowPanel(id)
    local data = self:GetTokenData(id)
    if data ~= nil then
        return data.__show_panel
    end
    return 0
end

------------------------------天梯商店表
function ShopDataHelper:GetLadderData(id)
    local data = XMLManager.shop_ladder[id]
    return data
end

function ShopDataHelper:GetLadderItemId(id)
    local data = self:GetLadderData(id)
    return data.__item_id
end

function ShopDataHelper:GetLadderCount(id)
    local data = self:GetLadderData(id)
    return data.__count
end

function ShopDataHelper:GetLadderPrice(id)
    local data = self:GetLadderData(id)
    return data.__price
end

------------------------------商城页签表
local shopTagCache = {}
function ShopDataHelper.ClearShopTagCache ()
    shopTagCache = {}
end

local ShopTag = Class.ShopTag(ClassTypes.XObject)
function ShopTag:__ctor__(cfg)
    self.id = cfg.__id
    self.tag_name = cfg.__tag_name
    self.order = cfg.__order
end

function ShopDataHelper:GetShopTagData(id)
    ClearDataManager:UseCache(CacheNames.shop_tag, ShopDataHelper.ClearShopTagCache)
    local data = shopTagCache[id]
    if data ~= nil then
        return data
    end
    local cfg = XMLManager.shop_tag[id]
    if cfg == nil then
        LoggerHelper.Error("ShopDataHelper:GetShopTagData has no this id:" .. tostring(id))
    else
        data = ShopTag(cfg)
        shopTagCache[data.id] = data
    end
    return data
end

function ShopDataHelper:GetShopTagName(id)
    local data = self:GetShopTagData(id)
    return LanguageDataHelper.GetContent(data.tag_name)
end

function ShopDataHelper:GetShopTagOrder(id)
    local data = self:GetShopTagData(id)
    return data.order
end

return ShopDataHelper