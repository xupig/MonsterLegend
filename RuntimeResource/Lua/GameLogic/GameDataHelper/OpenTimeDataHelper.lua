-- OpenTimeDataHelper.lua
local OpenTimeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OpenTime = Class.OpenTime(ClassTypes.XObject)

local openTimeCache = {}
function OpenTimeDataHelper.ClearCache()
    openTimeCache = {}
end

function OpenTime:__ctor__(cfg)
    self.id = cfg.__id
    self.open_data = DataParseHelper.ParseListInt(cfg.__open_data)
    if cfg.__open_day ~= nil and cfg.__open_day ~= "0" then
        self.open_day = DataParseHelper.ParseListInt(cfg.__open_day)
    end
    self.open_time = DataParseHelper.ParseListInt(cfg.__open_time)
    self.duration_time = cfg.__duration_time
end

function OpenTimeDataHelper.GetOpenTimeItem(id)
    ClearDataManager:UseCache(CacheNames.open_time, OpenTimeDataHelper.ClearCache)
    local data = openTimeCache[id]
    if data == nil then
        local cfg = XMLManager.open_time[id]
        if cfg ~= nil then
            data = OpenTime(cfg)
            openTimeCache[data.id] = data
        else
            LoggerHelper.Error("OpenTime id not exist:" .. id)
        end
    end
    return data
end

function OpenTimeDataHelper:IsInOpenTime(id, curTime)
    local data = self.GetOpenTimeItem(id)
    if not data then
        return true
    end
    
    --判断日期
    local open_date = data.open_data
    if open_date then
        local is_in_range = false
        local date_number = DateTimeUtil.GetDateFormatNumber(curTime)
        for i = 1, #open_date, 2 do
            if open_date[i] <= date_number and date_number < open_date[i + 1] then
                is_in_range = true
                break
            end
        end
        if not is_in_range then
            return false
        end
    end
    --判断星期
    local open_day = data.open_day
    if open_day and open_day[1] ~= 0 then --有星期的限制
        local week_dayth = DateTimeUtil.GetWday(curTime)
        if not table.containsValue(open_day, week_dayth) then
            return false
        end
    end
    --判断时间段
    local open_time = data.open_time
    if open_time then
        for i = 1, #open_time do
            if DateTimeUtil.IsTimeInHMRange(curTime, open_time[i], data.duration_time) then
                return true, open_time[i]
            end
        end
        return false
    end
    return true
end

function OpenTimeDataHelper:GetTimeLeft(id, curTime)
    local isInTime, openTime = self:IsInOpenTime(id, curTime)
    if isInTime and openTime ~= nil then
        local leftSecond = DateTimeUtil:HMToSecond(openTime) + self:GetDurationTime(id) - DateTimeUtil:DateTimeToSecond(curTime)
        return DateTimeUtil:FormatFullTime(leftSecond, true, false, true)
    else
        return nil
    end
end

function OpenTimeDataHelper:IsInOpenDate(id, curTime)
    local data = self.GetOpenTimeItem(id)
    if not data then
        return true
    end
    
    --判断日期
    local open_date = data.open_data
    if open_date then
        local is_in_range = false
        local date_number = DateTimeUtil.GetDateFormatNumber(curTime)
        for i = 1, #open_date, 2 do
            if open_date[i] <= date_number and date_number < open_date[i + 1] then
                is_in_range = true
                break
            end
        end
        if not is_in_range then
            return false
        end
    end
    return true
end

function OpenTimeDataHelper:GetDescription(id)
    local result = ""
    local openDay = self:GetOpenDay(id)
    if openDay ~= nil then
        result = LanguageDataHelper.GetContent(909)-- 每逢
        local openDayCount = #openDay
        for i, v in ipairs(openDay) do
            result = result .. LanguageDataHelper.GetContent(901 + v)-- 902~908 一~日
            if i ~= openDayCount then
                result = result .. LanguageDataHelper.GetContent(911)-- 、
            end
        end
    end
    local openTime = self:GetOpenTime(id)
    if openTime ~= nil then
        local openTimeCount = #openTime
        for i, v in ipairs(openTime) do
            local seconds = DateTimeUtil:HMToSecond(v)
            result = result .. DateTimeUtil:FormatFullTime(seconds, true)
                .. "~" .. DateTimeUtil:FormatFullTime(seconds + self:GetDurationTime(id), true)
            if i ~= openTimeCount then
                result = result .. LanguageDataHelper.GetContent(911)-- 、
            end
        end
    end
    result = result .. LanguageDataHelper.GetContent(912)-- 开启
    return result
end

function OpenTimeDataHelper:GetOpenDate(id)
    local data = self.GetOpenTimeItem(id)
    return data and data.open_data or nil
end

function OpenTimeDataHelper:GetOpenDay(id)
    local data = self.GetOpenTimeItem(id)
    return data and data.open_day or nil
end

function OpenTimeDataHelper:GetOpenTime(id)
    local data = self.GetOpenTimeItem(id)
    return data and data.open_time or nil
end

function OpenTimeDataHelper:GetDurationTime(id)
    local data = self.GetOpenTimeItem(id)
    return data and data.duration_time or nil
end

return OpenTimeDataHelper
