local ComposeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ComposeItemCfg = Class.ComposeItemCfg(ClassTypes.XObject)
local ComposeEquipCfg = Class.ComposeEquipCfg(ClassTypes.XObject)
local ComposeDragonSoulCfg = Class.ComposeDragonSoulCfg(ClassTypes.XObject)

local ClearDataManager = PlayerManager.ClearDataManager

local composeItemCache = {}
function ComposeDataHelper.ClearItemCache()
    composeItemCache = {}
end

local composeEquipCache = {}
function ComposeDataHelper.ClearEquipCache()
    composeEquipCache = {}
end

local composeDragonCache = {}
function ComposeDataHelper.ClearDragonCache()
    composeDragonCache = {}
end

local composeItemStructCache = {}
function ComposeDataHelper.ClearItemStructCache()
    composeItemStructCache = {}
end

local composeEquipStructCache = {}
function ComposeDataHelper.ClearEquipStructCache()
    composeEquipStructCache = {}
end

local composeDragonsoulStructCache = {}
function ComposeDataHelper.ClearDragonsoulStructCache()
    composeDragonsoulStructCache = {}
end

function ComposeItemCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.item_comp_type = cfg.__item_comp_type
	self.item_tag = cfg.__item_tag
	self.item_tag_desc = cfg.__item_tag_desc
	self.item1_id = DataParseHelper.ParseMSuffix(cfg.__item1_id)
	self.item2_id = DataParseHelper.ParseMSuffix(cfg.__item2_id)
	self.item3_id = DataParseHelper.ParseMSuffix(cfg.__item3_id)
	self.item_comp_id = DataParseHelper.ParseMSuffix(cfg.__item_comp_id)
	self.money = cfg.__money
	self.show_level = cfg.__show_level
end

function ComposeEquipCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.equip_comp_type = cfg.__equip_comp_type
	self.vocation = cfg.__vocation
	self.equip_type = cfg.__equip_type
	self.equip_tag_desc = cfg.__equip_tag_desc
	self.equip_grade = cfg.__equip_grade
	self.equip_part = cfg.__equip_part
	self.stuff_quality = cfg.__stuff_quality
	self.stuff_star = cfg.__stuff_star
	self.comp_equip_id = cfg.__comp_equip_id
	self.godstone_id = DataParseHelper.ParseMSuffix(cfg.__godstone_id)
	self.condition_level = cfg.__condition_level
end

function ComposeDragonSoulCfg:__ctor__(cfg)
	self.id = cfg.__id
	self.item_comp_type = cfg.__item_comp_type
	self.item_tag = cfg.__item_tag
	self.item_tag_desc = cfg.__item_tag_desc
	self.dragonsoul_id = DataParseHelper.ParseMSuffixSort(cfg.__dragonsoul_id)
	self.item_id = DataParseHelper.ParseMSuffixSort(cfg.__item_id)
	self.dragonsoul_comp_id = cfg.__dragonsoul_comp_id
	self.sucess_prop = cfg.__sucess_prop
	self.level_limit = cfg.__level_limit
end

function ComposeDataHelper.GetComposeItemCfg(id)
    ClearDataManager:UseCache(CacheNames.compose_item, ComposeDataHelper.ClearItemCache)
    local data = composeItemCache[id]
	if data == nil then
        local cfg = XMLManager.compose_item[id]
        if cfg ~= nil then
            data = ComposeItemCfg(cfg)
            composeItemCache[data.id] = data
        else
            LoggerHelper.Error("ComposeItemCfg id not exist:" .. id)
        end
    end
    return data
end

function ComposeDataHelper.GetComposeEquipCfg(id)
    ClearDataManager:UseCache(CacheNames.compose_equip, ComposeDataHelper.ClearEquipCache)
    local data = composeEquipCache[id]
    if data == nil then
        local cfg = XMLManager.compose_equip[id]
        if cfg ~= nil then
            data = ComposeEquipCfg(cfg)
            composeEquipCache[data.id] = data
        else
            LoggerHelper.Error("ComposeEquipCfg id not exist:" .. id)
        end
    end
    return data
end

function ComposeDataHelper.GetComposeDragonSoulCfg(id)
    ClearDataManager:UseCache(CacheNames.compose_dragonsoul, ComposeDataHelper.ClearDragonCache)
    local data = composeDragonCache[id]
	if data == nil then
        local cfg = XMLManager.compose_dragonsoul[id]
        if cfg ~= nil then
            data = ComposeDragonSoulCfg(cfg)
            composeDragonCache[data.id] = data
        else
            LoggerHelper.Error("ComposeDragonSoulCfg id not exist:" .. id)
        end
    end
    return data
end

--根据功能开启获取道具合成列表
function ComposeDataHelper:GetAllComposeItemDataByFunc(funcId)
    ClearDataManager:UseCache(CacheNames.composeItemStructCache, ComposeDataHelper.ClearItemStructCache)
	if composeItemStructCache[funcId] == nil then
		local keys = XMLManager.compose_item:Keys()
		for i=1,#keys do
			local itemCfg = ComposeDataHelper.GetComposeItemCfg(keys[i])
			local t = itemCfg.item_comp_type
			if composeItemStructCache[t] == nil then
				composeItemStructCache[t] = {}
			end

			local typeHasInit = false
			for j=1,#composeItemStructCache[t] do
				if composeItemStructCache[t][j][1] == itemCfg.item_tag_desc then
					table.insert(composeItemStructCache[t][j],itemCfg)
					typeHasInit = true
				end
			end
			if typeHasInit == false then
				local typeTable = {}
				typeTable[1] = itemCfg.item_tag_desc
				table.insert(typeTable,itemCfg)
				table.insert(composeItemStructCache[t],typeTable)
			end
		end
	end
	return composeItemStructCache[funcId]
end

function ComposeDataHelper:GetComposeMatIds(funcId)
	local result = {}
	local filtIds = GlobalParamsHelper.GetParamValue(924)
	local dataList = self:GetAllComposeItemDataByFunc(funcId)
	for i=1,#dataList do
		for j=2,#dataList[i] do
			local itemCfg = dataList[i][j]
			self:SetMatInfo(result,itemCfg.item1_id,itemCfg,filtIds)
			self:SetMatInfo(result,itemCfg.item2_id,itemCfg,filtIds)
			self:SetMatInfo(result,itemCfg.item3_id,itemCfg,filtIds)
		end
	end

	return result
end

function ComposeDataHelper:SetMatInfo(t,matStruct,itemCfg,filtIds)
	if matStruct then
		for matId,v in pairs(matStruct) do
			if filtIds[itemCfg.id] then
				return
			end
			if t[matId] == nil then
				t[matId] = {}
			end
			table.insert(t[matId],itemCfg)
		end
	end
end

--根据功能开启获取装备合成列表
function ComposeDataHelper:GetAllComposeEquipDataByFunc(funcId)
    ClearDataManager:UseCache(CacheNames.composeEquipStructCache, ComposeDataHelper.ClearEquipStructCache)
	if composeEquipStructCache[funcId] == nil then
		local keys = XMLManager.compose_equip:Keys()
		for i=1,#keys do
			local equipCfg = ComposeDataHelper.GetComposeEquipCfg(keys[i])
			local t = equipCfg.equip_comp_type
			if t > 0 then
				if composeEquipStructCache[t] == nil then
					composeEquipStructCache[t] = {}
				end
				--LoggerHelper.Log("t"..t)
				local listByFunc = composeEquipStructCache[t]
				local typeHasInit = false
				for j=1,#listByFunc do
					if listByFunc[j][1] == equipCfg.equip_tag_desc then
						--LoggerHelper.Log("equipCfg.equip_tag_desc"..equipCfg.equip_tag_desc)
						local gradeHasInit = false
						for h=2,#listByFunc[j] do
							if listByFunc[j][h][1].equip_grade == equipCfg.equip_grade then
								table.insert(listByFunc[j][h],equipCfg)
								gradeHasInit = true
							end
						end

						if gradeHasInit == false then
							local gradeTable = {}
							gradeTable[1] = equipCfg
							table.insert(listByFunc[j],gradeTable)
						end
						typeHasInit = true
					end
				end
				if typeHasInit == false then
					local typeTable = {}
					local gradeTable = {}
					typeTable[1] = equipCfg.equip_tag_desc
					gradeTable[1] = equipCfg
					table.insert(typeTable,gradeTable)
					table.insert(composeEquipStructCache[t],typeTable)
				end
			end
		end
	end
	-- LoggerHelper.Log("GetAllComposeEquipDataByFunc"..funcId)
	-- LoggerHelper.Log(composeEquipStructCache)
	return composeEquipStructCache[funcId]
end

--根据功能开启获取龙魂合成列表
function ComposeDataHelper:GetAllComposeDragonSoulDataByFunc(funcId)
    ClearDataManager:UseCache(CacheNames.composeDragonsoulStructCache, ComposeDataHelper.ClearDragonsoulStructCache)
	if composeDragonsoulStructCache[funcId] == nil then
		local keys = XMLManager.compose_dragonsoul:Keys()
		for i=1,#keys do
			local dsCfg = ComposeDataHelper.GetComposeDragonSoulCfg(keys[i])
			local t = dsCfg.item_comp_type
			if composeDragonsoulStructCache[t] == nil then
				composeDragonsoulStructCache[t] = {}
			end

			local typeHasInit = false
			for j=1,#composeDragonsoulStructCache[t] do
				if composeDragonsoulStructCache[t][j][1] == dsCfg.item_tag_desc then
					table.insert(composeDragonsoulStructCache[t][j],dsCfg)
					typeHasInit = true
				end
			end
			if typeHasInit == false then
				local typeTable = {}
				typeTable[1] = dsCfg.item_tag_desc
				table.insert(typeTable,dsCfg)
				table.insert(composeDragonsoulStructCache[t],typeTable)
			end
		end
	end
	return composeDragonsoulStructCache[funcId]
end
return ComposeDataHelper