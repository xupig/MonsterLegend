-- WelfareDataHelper.lua
local WelfareDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ServerTimeDataHelper = GameDataHelper.ServerTimeDataHelper
local public_config = GameWorld.public_config

local WelfareConfig = GameConfig.WelfareConfig
local REWARD_STATE_WEIGHT = WelfareConfig.REWARD_STATE_WEIGHT
local REWARD_STATE = WelfareConfig.REWARD_STATE
local DateTimeUtil = GameUtil.DateTimeUtil


--===========  数据解析 ==============
local WelfareLevelUp = Class.WelfareLevelUp(ClassTypes.XObject)
local WelfareOnline = Class.WelfareOnline(ClassTypes.XObject)
local WelfareTili = Class.WelfareTili(ClassTypes.XObject)
local WelfareSign = Class.WelfareSign(ClassTypes.XObject)
local WelfareSevenDay = Class.WelfareSevenDay(ClassTypes.XObject)
local WelfareDes = Class.WelfareDes(ClassTypes.XObject)

function WelfareLevelUp:__ctor__(cfg)
    self.id = cfg.__id
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward) or {}
    self.rewardList = {}
    for i, v in ipairs(self.reward) do
        self.rewardList[i] = {itemId = v[1], itemNum = v[2]}
    end
    self.GetState = function(self)
        if not GameWorld.Player() or not GameWorld.Player().welfare_level_up_reward then
            return REWARD_STATE.noReward
        end
        return (GameWorld.Player().welfare_level_up_reward[self.id] and REWARD_STATE.rewarded
            or (GameWorld.Player().level >= self.id and REWARD_STATE.canReward 
            or REWARD_STATE.noReward))
    end
end

function WelfareOnline:__ctor__(cfg)
    self.id = cfg.__id
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward) or {}
    self.rewardList = {}
    for i, v in ipairs(self.reward) do
        self.rewardList[i] = {itemId = v[1], itemNum = v[2]}
    end
    self.GetTime = function(self)
        return (self.id - GameDataHelper.WelfareDataHelper:GetOnlinePreTime(self.id))
    end
    self.GetName = function(self)
        return (self:GetTime()/60) -- 秒-》分钟
    end
    self.GetState = function(self)
        if not GameWorld.Player() or not GameWorld.Player().welfare_online_reward then
            return REWARD_STATE.noReward
        end
        return (GameWorld.Player().welfare_online_reward[self.id] and REWARD_STATE.rewarded
            or ((self.id == GameDataHelper.WelfareDataHelper:GetOnlineCurrentId() and GameDataHelper.WelfareDataHelper:GetOnlineTime() >= self:GetTime()) and REWARD_STATE.canReward 
            or REWARD_STATE.noReward))
    end
end

function WelfareTili:__ctor__(cfg)
    self.id = cfg.__id
    self.time = cfg.__time
    self.tili = cfg.__tili
    self.timeList = {} -- {{hour=12,min=0}, {hour=14,min=0}}  -- startTime, endTime
    local strs = string.split(cfg.__time, "-")
    for i, v in ipairs(strs) do
        local t = string.split(v, ":")
        self.timeList[i] = {hour = tonumber(t[1]), min = tonumber(t[2])} 
    end
    self.time = string.gsub(self.time, "-", "~")
end

-- 每日一签
function WelfareSign:__ctor__(cfg)
    self.id = cfg.__id
    self.weekday = cfg.__weekday
    self.group = cfg.__group
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward) or {}
    self.rewardList = {}
    for i, v in ipairs(self.reward) do
        self.rewardList[i] = {itemId = v[1], itemNum = v[2]}
    end
    self.GetState = function(self)
        if not GameWorld.Player() or not GameWorld.Player().welfare_weekday_reward_time then
            return REWARD_STATE.noReward
        end
        --local ServerTimeDataHelper = GameDataHelper.ServerTimeDataHelper
        local rewardTime = GameWorld.Player().welfare_weekday_reward_time[self.weekday]
        if rewardTime and ServerTimeDataHelper.GetOpenServerWeek() == ServerTimeDataHelper.GetOpenServerWeek(rewardTime) then
            return REWARD_STATE.rewarded
        else
            local wday = DateTimeUtil.GetWday()
            return self.weekday == wday and REWARD_STATE.canReward or REWARD_STATE.noReward
        end
    end
end

function WelfareSevenDay:__ctor__(cfg)
    self.id = cfg.__id
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward) or {}
    self.reward_preview = DataParseHelper.ParseListInt(cfg.__reward_preview) or nil
    self.rewardList = {}
    for i, v in ipairs(self.reward) do
        self.rewardList[i] = {itemId = v[1], itemNum = v[2]}
    end
    self.GetState = function(self)
        if not GameWorld.Player() or not GameWorld.Player().welfare_7day_reward_time then
            return REWARD_STATE.noReward
        end
        local fetchTime = GameWorld.Player().welfare_7day_reward_time[self.id]
        return (fetchTime == 0 and REWARD_STATE.canReward 
            or ((fetchTime and fetchTime > 0) and REWARD_STATE.rewarded
            or REWARD_STATE.noReward))
    end
end


function WelfareDes:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.name = LanguageDataHelper.CreateContent(cfg.__name)
    self.des = cfg.__des ~= 0 and LanguageDataHelper.CreateContent(cfg.__des) or cfg.__des
    self.day = DataParseHelper.ParseListInt(cfg.__day)
end


--===========配置缓存==============

local cacheLevelUp = {}
local cacheOnline = {}
local cacheTili = {}
local cacheSign = {}
local cacheSevenDay = {}
local cacheDes = {}

function WelfareDataHelper:GetLevelUpById(id)
    local data = cacheLevelUp[id]
    if data == nil then
      data = XMLManager.welfare_level_up[id]
      if data ~= nil then data = WelfareLevelUp(data) end
    end
    if data ~= nil then cacheLevelUp[data.id] = data else LoggerHelper.Error("welfare_level_up id Error by :"..tostring(id)..debug.traceback()) end
    return data
end

function WelfareDataHelper:GetOnlineById(id)
    local data = cacheOnline[id]
    if data == nil then
      data = XMLManager.welfare_online_time[id]
      if data ~= nil then data = WelfareOnline(data) end
    end
    if data ~= nil then cacheOnline[data.id] = data else LoggerHelper.Error("welfare_online_time id Error by :"..tostring(id)..debug.traceback()) end
    return data
end

function WelfareDataHelper:GetTiliById(id)
    local data = cacheTili[id]
    if data == nil then
      data = XMLManager.welfare_free_tili[id]
      if data ~= nil then data = WelfareTili(data) end
    end
    if data ~= nil then cacheTili[data.id] = data else LoggerHelper.Error("welfare_free_tili id Error by :"..tostring(id)..debug.traceback()) end
    return data
end

function WelfareDataHelper:GetSignById(id)
    local data = cacheSign[id]
    if data == nil then
      data = XMLManager.welfare_weekday_reward[id]
      if data ~= nil then data = WelfareSign(data) end
    end
    if data ~= nil then cacheSign[data.id] = data else LoggerHelper.Error("welfare_weekday_reward id Error by :"..tostring(id)..debug.traceback()) end
    return data
end


function WelfareDataHelper:GetSevenDayById(id)
    local data = cacheSevenDay[id]
    if data == nil then
      data = XMLManager.welfare_7day_reward[id]
      if data ~= nil then data = WelfareSevenDay(data) end
    end
    if data ~= nil then cacheSevenDay[data.id] = data else LoggerHelper.Error("welfare_7day_reward id Error by :"..tostring(id)..debug.traceback()) end
    return data
end

function WelfareDataHelper:GetDesById(id)
    local data = cacheDes[id]
    if data == nil then
      data = XMLManager.welfare_des[id]
      if data ~= nil then data = WelfareDes(data) end
    end
    if data ~= nil then cacheDes[data.id] = data else LoggerHelper.Error("welfare_des id Error by :"..tostring(id)..debug.traceback()) end
    return data
end



--==============以下是获取列表接口 ===========
function WelfareDataHelper:GetLevelUpList()
    local list = {}
    for i, v in pairs(self._listLevelUp) do table.insert(list, v) end
    table.sort(list, function(a, b)
        local wa, wb = REWARD_STATE_WEIGHT[a:GetState()], REWARD_STATE_WEIGHT[b:GetState()]
        if wa ~= wb then return wa < wb end
        return a.id < b.id
    end)
    return list
end

function WelfareDataHelper:GetOnlineList()
    local list = {}
    for i, v in pairs(self._listOnline) do table.insert(list, v) end
    table.sort(list, function(a, b)
        local wa, wb = REWARD_STATE_WEIGHT[a:GetState()], REWARD_STATE_WEIGHT[b:GetState()]
        if wa ~= wb then return wa < wb end
        return a.id < b.id
    end)
    return list
end

function WelfareDataHelper:GetSignList(weekDay)
    if not weekDay then -- 客户端判断
        weekDay = self:GetOpenServerWeek()
    end
    local list = {}
    -- LoggerHelper.Log({"=====_listSign===", self._listSign})
    for i, v in pairs(self._listSign[weekDay]) do table.insert(list, v) end
    return list
end


function WelfareDataHelper:GetSevenDayList()
    local list = {}
    -- LoggerHelper.Log({"=====_listSevenDay===", self._listSevenDay})
    for i, v in pairs(self._listSevenDay) do table.insert(list, v) end
    table.sort(list, function(a, b)
        local wa, wb = REWARD_STATE_WEIGHT[a:GetState()], REWARD_STATE_WEIGHT[b:GetState()]
        if wa ~= wb then return wa < wb end
        return a.id < b.id
    end)
    return list
end




---============= 其他接口=================

-- 连续签到奖励
function WelfareDataHelper:GetSignExReward()
    local ServerTimeDataHelper = GameDataHelper.ServerTimeDataHelper
    local openWDay = ServerTimeDataHelper.GetOpenServerWeek()
    local isTodayReward = true -- 连续可领取
    local count = 0-- 签到次数
    if GameWorld.Player() and GameWorld.Player().welfare_weekday_reward_time then
        local today = DateTimeUtil.GetWday()
        local record = GameWorld.Player().welfare_weekday_reward_time
        local day = today
        for i = 1, 7 do
            if i == 1 then
                count = count + 1
            elseif record[day] and openWDay == ServerTimeDataHelper.GetOpenServerWeek(record[day]) then -- 有本周领取奖励
                count = count + 1
            else  --中断
                break
            end
            day = day - 1
            if day == 0 then
                day = 7
            end
        end
        local todayState = self:GetSignList()[today]:GetState()
        if todayState ~= REWARD_STATE.rewarded then
            count = count - 1 
        end
        if count == 0 or todayState == REWARD_STATE.rewarded then -- 
            isTodayReward = false-- 明日可领
        end
        -- LoggerHelper.Log({"======== ex reward1 ", count, todayState, day})
    end
    local index = count < 2 and 2 or 3
    -- LoggerHelper.Log({"======== ex reward2 ", index, isTodayReward, count})
    return isTodayReward, {itemId = WelfareConfig.WEEKDAY_REWARD_ID, itemNum = WelfareConfig.WEEKDAY_REWARD_NUM[index]}
end

-- 获取在线时间
function WelfareDataHelper:GetOnlineTime()
    local curTime = DateTimeUtil.GetServerTime()
    local sumTime = GameWorld.Player().welfare_online_time[public_config.WELFARE_ONLINE_TIME_SUM]
    local lastSumTime = GameWorld.Player().welfare_online_time[public_config.WELFARE_ONLINE_TIME_LAST_UPDATE_TIME]
    local onlineTime = curTime - lastSumTime + sumTime - WelfareDataHelper:GetOnlineLastRewardedTime() -- 后端时间仍保留前置配置的时间，需减掉
    -- LoggerHelper.Log({"======= onlineTime", curTime,lastSumTime , sumTime , WelfareDataHelper:GetOnlineLastRewardedTime()})
    return onlineTime
end

-- 上次领取的配置时间
function WelfareDataHelper:GetOnlineLastRewardedTime()
    local lastTime = 0
    local timeId
    for k, v in pairs(GameWorld.Player().welfare_online_reward) do
        if not timeId or k > timeId then
            timeId = k
        end
    end
    if timeId then
        local data = WelfareDataHelper:GetOnlineById(timeId)
        if data then
            lastTime = data.id
        end
    end
    return lastTime
end


-- 每日一签 当前id
function WelfareDataHelper:GetOnlineCurrentId()
    local record = GameWorld.Player().welfare_online_reward
    local currentId
    for i, v in pairs(self._listOnline) do
        if not record[v.id] then
            currentId = v.id
            break
        end
    end
    return currentId
end

-- 每日一签 前置配置的时间
function WelfareDataHelper:GetOnlinePreTime(id)
    local record = GameWorld.Player().welfare_online_reward
    local preTime = 0
    for i = #self._listOnline, 1, -1 do
        local v = self._listOnline[i]
        if v.id < id then
            preTime = v.id
            break
        end
    end
    return preTime
end

-- 免费午餐、晚餐，领取id
function WelfareDataHelper:GetTiliStateAndId()
    if not GameWorld.Player() then
        return REWARD_STATE.noReward
    end
    local curTime = DateTimeUtil.GetServerTime()
    local curDate = DateTimeUtil.SomeDay(curTime)

    local info = GameWorld.Player().welfare_free_tili_time
    local state = REWARD_STATE.noReward
    local rewardId 
    for id, v in ipairs(self._listTili) do
        local startTime = os.time({year=curDate.year, month=curDate.month, day=curDate.day, hour=v.timeList[1].hour, min=v.timeList[1].min, sec=0})
        local endTime = os.time({year=curDate.year, month=curDate.month, day=curDate.day, hour=v.timeList[2].hour, min=v.timeList[2].min, sec=0})
        
        local fetchTime = info[id]
        if fetchTime and startTime <= fetchTime and fetchTime <= endTime then
            state = REWARD_STATE.rewarded
        else
            if startTime <= curTime and curTime <= endTime then
                state = REWARD_STATE.canReward
                rewardId = id
                break
            else
                state = REWARD_STATE.noReward
            end
        end
    end
    return state, rewardId
end

--
-- function WelfareDataHelper:GetOpenServerWeek()
--     local week = GameDataHelper.ServerTimeDataHelper.GetOpenServerWeek()
--     week = ((week - 1) % self._signLoopWeek) + 1
--     return week
-- end
function WelfareDataHelper:GetOpenServerWeek()
    local week = DateTimeUtil.get_weekth_of_year()
    -- LoggerHelper.Log("group " .. week .. ", loop " .. self._signLoopWeek)
    week = math.fmod(week, self._signLoopWeek) + 1
    return week
end


-- 七天登录：获取今天的登录天数
function WelfareDataHelper:GetSevenDayToday()
    if not GameWorld.Player() or not GameWorld.Player().welfare_7day_reward_time then
        return 1
    end
    local today
    for k, v in pairs(GameWorld.Player().welfare_7day_reward_time) do
        if not today or k > today then
            today = k
        end
    end
    return today or 1
end
-- 七天登录奖励预览
function WelfareDataHelper:GetSevenDayPreview()
    local today = self:GetSevenDayToday()
    if not today then
        return
    end
    
    local nextDay, data7
    local data = self._listSevenDay[today + 1]
    if data and data.reward_preview and data.reward_preview[1] == 1 then -- 道具
        nextDay = {data = data, str = LanguageDataHelper.CreateContent(60004)} --明天

    else
        data = self._listSevenDay[today + 2]
        if data and data.reward_preview and data.reward_preview[1] == 1 then -- 道具
            nextDay = {data = data, str = LanguageDataHelper.CreateContent(60005)} --- 后天
        end
    end

    data = self._listSevenDay[7]
    if data and data.reward_preview then
        local str = LanguageDataHelper.CreateContent(60007) --"第七天"
        if 7 - today == 3 then
            str = LanguageDataHelper.CreateContent(60006) -- "大后天"
        end
        data7 = {data = data, str = str}
    end
    return nextDay, data7
end

-- 获取是否可领奖的状态
-- 返回 boolean map
function WelfareDataHelper:GetRewardState()
    local wday = DateTimeUtil.GetWday()
    local states = {["Sign"] = false, ["SevenDay"] = false, ["Online"] = false, ["LevelUp"] = false}

    for i, v in pairs(self:GetSignList()) do
        if v:GetState() == REWARD_STATE.canReward then
            states["Sign"] = true
            break
        end
    end

    for i, v in pairs(self._listSevenDay) do
        if v:GetState() == REWARD_STATE.canReward then
            states["SevenDay"] = true
            break
        end
    end

    for i, v in pairs(self._listOnline) do
        if v:GetState() == REWARD_STATE.canReward then
            states["Online"] = true
            break
        end
    end
    if not states["Online"] then
        local state = WelfareDataHelper:GetTiliStateAndId()
        states["Online"] = (state == REWARD_STATE.canReward)
    end

    for i, v in pairs(self._listLevelUp) do
        if v:GetState() == REWARD_STATE.canReward then
            states["LevelUp"] = true
            break
        end
    end

    return states
end

function WelfareDataHelper:IsSevenDayAllRewarded()
    local ret = true
    for i, v in pairs(self._listSevenDay) do
        if v:GetState() ~= REWARD_STATE.rewarded then
            ret = false
            break
        end
    end
    return ret
end


--==============初始化 ===========
function WelfareDataHelper:Init()
    local tempLevelUp = {}
    local tempOnline = {}
    local tempTili = {}
    local tempSign = {}
    local tempSevenDay = {}

    for k, v in pairs(XMLManager.welfare_level_up:Keys()) do
        if k ~= 0 then table.insert(tempLevelUp, self:GetLevelUpById(v)) end
    end

    for k, v in pairs(XMLManager.welfare_online_time:Keys()) do
        if k ~= 0 then table.insert(tempOnline, self:GetOnlineById(v)) end
    end
    table.sort(tempOnline, function(a, b) return a.id < b.id end)

    for k, v in pairs(XMLManager.welfare_free_tili:Keys()) do
        tempTili[v] = self:GetTiliById(v)
    end

    for k, v in pairs(XMLManager.welfare_weekday_reward:Keys()) do
        local data = self:GetSignById(v)
        if not tempSign[data.group] then
            tempSign[data.group] = {}
        end
        table.insert(tempSign[data.group], data)
        if not self._signLoopWeek or data.group > self._signLoopWeek then
            self._signLoopWeek = data.group
        end
    end

    for k, v in pairs(XMLManager.welfare_7day_reward:Keys()) do
        if k ~= 0 then tempSevenDay[v] = self:GetSevenDayById(v) end
    end

    self._listLevelUp = tempLevelUp
    self._listOnline = tempOnline
    self._listTili = tempTili
    self._listSign = tempSign
    self._signLoopWeek = self._signLoopWeek or 1
    self._listSevenDay = tempSevenDay
end

function WelfareDataHelper:InitSignText()
    if self._desType then
        return
    end
    math.randomseed(DateTimeUtil.GetServerTime())
    local temp = {}
    for k, v in pairs(XMLManager.welfare_des:Keys()) do
        if k ~= 0 then 
            local data = self:GetDesById(v)
            if not temp[data.type] then
                temp[data.type] = {}
            end
            table.insert(temp[data.type], data)
        end
    end
    self._desType = temp
end


WelfareDataHelper:Init()
return WelfareDataHelper