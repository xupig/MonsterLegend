-- WelfareRankRewardDataHelper.lua
local WelfareRankRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareRankReward = Class.WelfareRankReward(ClassTypes.XObject)

local welfareRankRewardCache = {}
function WelfareRankRewardDataHelper.ClearCache()
    welfareRankRewardCache = {}
end

function WelfareRankReward:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.type_last = cfg.__type_last
    self.rank = DataParseHelper.ParseListInt(cfg.__rank)
    self.targets = DataParseHelper.ParseListInt(cfg.__targets)
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
    self.desc = cfg.__desc
end

function WelfareRankRewardDataHelper.GetWelfareRankReward(id)
    ClearDataManager:UseCache(CacheNames.welfare_rank_reward, WelfareRankRewardDataHelper.ClearCache)
    local data = welfareRankRewardCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_rank_reward[id]
        if cfg ~= nil then
            data = WelfareRankReward(cfg)
            welfareRankRewardCache[data.id] = data
        else
            LoggerHelper.Error("WelfareRankReward id not exist:" .. id)
        end
    end
    return data
end

function WelfareRankRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_rank_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareRankRewardDataHelper:GetType(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.type or nil
end

function WelfareRankRewardDataHelper:GetTypeLast(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.type_last or nil
end

function WelfareRankRewardDataHelper:GetRank(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.rank or nil
end

function WelfareRankRewardDataHelper:GetTargets(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.targets or nil
end

function WelfareRankRewardDataHelper:GetReward(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.reward or nil
end

function WelfareRankRewardDataHelper:GetDesc(id)
    local data = self.GetWelfareRankReward(id)
    return data and data.desc or nil
end

return WelfareRankRewardDataHelper
