-- PersonalBossDataHelper.lua
local PersonalBossDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local PersonalBoss = Class.PersonalBoss(ClassTypes.XObject)

function PersonalBoss:__ctor__(cfg)
    self.id = cfg.__id
    self.sence_id = cfg.__sence_id
    self.level_limit = cfg.__level_limit
    self.boss_level = cfg.__boss_level
    self.boss_name = cfg.__boss_name
    self.boss_name_icon = cfg.__boss_name_icon
    self.boss_icon = cfg.__boss_icon
    self.boss_monster_id = cfg.__boss_monster_id
    self.boss_drops = DataParseHelper.ParseNSuffix(cfg.__boss_drops)
    self.boss_must_drops = DataParseHelper.ParseMSuffix(cfg.__boss_must_drops)
    self.level_show = cfg.__level_show
end

local personalBossCache = {}
function PersonalBossDataHelper.ClearCache()
    personalBossCache = {}
end

function PersonalBossDataHelper:GetBossData(id)
    ClearDataManager:UseCache(CacheNames.personal_boss, PersonalBossDataHelper.ClearCache)
    local data = personalBossCache[id]
    if data == nil then
        local cfg = XMLManager.personal_boss[id]
        if cfg ~= nil then
            data = PersonalBoss(cfg)
            personalBossCache[data.id] = data
        else
            LoggerHelper.Error("PersonalBoss id not exist:" .. id)
        end
    end
    return data
end

function PersonalBossDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.personal_boss:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PersonalBossDataHelper:GetSenceId(id)
    local data = self:GetBossData(id)
    return data and data.sence_id or nil
end

function PersonalBossDataHelper:GetLevelLimit(id)
    local data = self:GetBossData(id)
    return data and data.level_limit or nil
end

function PersonalBossDataHelper:GetBossLevel(id)
    local data = self:GetBossData(id)
    return data and data.boss_level or nil
end

function PersonalBossDataHelper:GetBossName(id)
    local data = self:GetBossData(id)
    return data and data.boss_name or nil
end

function PersonalBossDataHelper:GetBossNameIcon(id)
    local data = self:GetBossData(id)
    return data and data.boss_name_icon or nil
end

function PersonalBossDataHelper:GetBossIcon(id)
    local data = self:GetBossData(id)
    return data and data.boss_icon or nil
end

function PersonalBossDataHelper:GetBossMonsterId(id)
    local data = self:GetBossData(id)
    return data and data.boss_monster_id or nil
end

function PersonalBossDataHelper:GetBossDrops(id)
    local data = self:GetBossData(id)
    return data and data.boss_drops or nil
end

function PersonalBossDataHelper:GetBossMustDrops(id)
    local data = self:GetBossData(id)
    return data and data.boss_must_drops or nil
end

function PersonalBossDataHelper:GetLevelShow(id)
    local data = self:GetBossData(id)
    return data and data.level_show or 0
end

return PersonalBossDataHelper
