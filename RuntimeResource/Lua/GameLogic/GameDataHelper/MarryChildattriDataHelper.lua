-- MarryChildattriDataHelper.lua
local MarryChildattriDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local MarryChildattri = Class.MarryChildattri(ClassTypes.XObject)

local marryChildattriCache = {}
function MarryChildattriDataHelper.ClearCache()
    marryChildattriCache = {}
end

function MarryChildattri:__ctor__(cfg)
    self.id = cfg.__id
    self.child_id = cfg.__child_id
    self.level = cfg.__level
    self.exp = cfg.__exp
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

function MarryChildattriDataHelper.GetMarryChildattri(id)
    ClearDataManager:UseCache(CacheNames.marry_childattri, MarryChildattriDataHelper.ClearCache)
    local data = marryChildattriCache[id]
    if data == nil then
        local cfg = XMLManager.marry_childattri[id]
        if cfg ~= nil then
            data = MarryChildattri(cfg)
            marryChildattriCache[data.id] = data
        else
            LoggerHelper.Error("MarryChildattri id not exist:" .. id)
        end
    end
    return data
end

function MarryChildattriDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.marry_childattri:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MarryChildattriDataHelper:GetChildId(id)
    local data = self.GetMarryChildattri(id)
    return data and data.child_id or nil
end

function MarryChildattriDataHelper:GetLevel(id)
    local data = self.GetMarryChildattri(id)
    return data and data.level or nil
end

function MarryChildattriDataHelper:GetExp(id)
    local data = self.GetMarryChildattri(id)
    return data and data.exp or nil
end

function MarryChildattriDataHelper:GetAttri(id)
    local data = self.GetMarryChildattri(id)
    return data and data.attri or nil
end

function MarryChildattriDataHelper:GetCfgAttByidLevel(id,level)
   if self._cfg==nil then
    self._cfg={}
    local Keys = XMLManager.marry_childattri:Keys()
    for i=1,#Keys do
        local level = MarryChildattriDataHelper:GetLevel(i)
        local childId = MarryChildattriDataHelper:GetChildId(i)
        if self._cfg[childId]==nil then 
            self._cfg[childId] = {}
            self._cfg[childId][level] = {}
        end
        if not self._cfg[childId][level] then
            self._cfg[childId][level]={}
        end
        self._cfg[childId][level]=self.GetMarryChildattri(i)
    end        
   end
   return self._cfg[id][level]
end

return MarryChildattriDataHelper
