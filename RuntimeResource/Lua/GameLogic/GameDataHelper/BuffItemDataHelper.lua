local BuffItemDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = require("ServerConfig/public_config")
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local buffItemData = {}
local BuffItemCfg = Class.BuffItemCfg(ClassTypes.XObject)

function BuffItemCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.look = cfg.__look
	self.range = cfg.__range
end

function BuffItemDataHelper:GetBuffItemData(id)
    local data = buffItemData[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.buff_item[id]
    if data~=nil then
	    data = BuffItemCfg(data)
    end
	buffItemData[id] = data
	return data
end

function BuffItemDataHelper:GetName(id)
	local data = self:GetBuffItemData(id)
	return LanguageDataHelper.GetContent(data.name)
end

function BuffItemDataHelper:GetModel(id)
	local data = self:GetBuffItemData(id)
	return data and data.look or nil
end

function BuffItemDataHelper:GetRange(id)
	local data = self:GetBuffItemData(id)
	return data and data.range or nil
end

return BuffItemDataHelper