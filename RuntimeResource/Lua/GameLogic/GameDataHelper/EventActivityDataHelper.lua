-- EventActivityDataHelper.lua
local EventActivityDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local OpenTimeDataHelper = GameDataHelper.OpenTimeDataHelper
local ClearDataManager = PlayerManager.ClearDataManager

local EventActivity = Class.EventActivity(ClassTypes.XObject)

local eventActivityCache = {}
function EventActivityDataHelper.ClearCache()
    eventActivityCache = {}
end

function EventActivity:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.desc = cfg.__desc
    self.icon = cfg.__icon
    self.daily = cfg.__daily
    self.public = cfg.__public
    self.type = cfg.__type
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.targets_desc = DataParseHelper.ParseMSuffix(cfg.__targets_desc)
    self.targets_score = DataParseHelper.ParseMSuffix(cfg.__targets_score)
    self.map_id = cfg.__map_id
    self.pos = DataParseHelper.ParseListInt(cfg.__pos)
    self.range = cfg.__range
    self.open_time_id = cfg.__open_time_id
    self.limit = DataParseHelper.ParseNSuffix(cfg.__limit)
    self.first_reward_id = cfg.__first_reward_id
    self.fixed_reward_id = cfg.__fixed_reward_id
    self.preview_reward_ids = DataParseHelper.ParseMSuffix(cfg.__preview_reward_ids)
    self.trigger_event_ids = DataParseHelper.ParseMSuffixSort(cfg.__trigger_event_ids)
    self.single_map_id = cfg.__single_map_id
end

function EventActivityDataHelper.GetEventActivity(id)
    ClearDataManager:UseCache(CacheNames.event_activity, EventActivityDataHelper.ClearCache)
    local data = eventActivityCache[id]
    if data == nil then
        local cfg = XMLManager.event_activity[id]
        if cfg ~= nil then
            data = EventActivity(cfg)
            eventActivityCache[data.id] = data
        else
            LoggerHelper.Error("EventActivity id not exist:" .. id)
        end
    end
    return data
end

function EventActivityDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.event_activity:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function EventActivityDataHelper:GetEventActivitiesByMapId(mapId)
    local ids = {}
    if mapId == nil then
        return self:GetAllId()
    end
    local data = XMLManager.event_activity:Keys()
    for i, v in ipairs(data) do
        if self:GetMapId(v) == mapId then
            table.insert(ids, v)
        end
    end
    return ids
end

function EventActivityDataHelper:GetEventActivityBySingleMapId(mapId)
    local data = XMLManager.event_activity:Keys()
    for i, v in ipairs(data) do
        if self:GetSingleMapId(v) == mapId then
            return v
        end
    end
end

function EventActivityDataHelper:IsOpen(id, curTime)
    local cfg = self.GetEventActivity(id)
    if not cfg then
        return false
    end
    
    if cfg.daily and cfg.daily == 1 then
        return true
    end
    return OpenTimeDataHelper:IsInOpenTime(cfg.open_time_id, curTime)
end

function EventActivityDataHelper:GetTimeLeft(id, curTime)
    local cfg = self.GetEventActivity(id)
    if not cfg then
        return nil
    end
    
    if cfg.daily and cfg.daily == 1 then
        return nil
    end
    return OpenTimeDataHelper:GetTimeLeft(cfg.open_time_id, curTime)
end

function EventActivityDataHelper:GetName(id)
    local data = self.GetEventActivity(id)
    return data and data.name or nil
end

function EventActivityDataHelper:GetDesc(id)
    local data = self.GetEventActivity(id)
    return data and data.desc or nil
end

function EventActivityDataHelper:GetIcon(id)
    local data = self.GetEventActivity(id)
    return data and data.icon or nil
end

function EventActivityDataHelper:GetDaily(id)
    local data = self.GetEventActivity(id)
    return data and data.daily or nil
end

function EventActivityDataHelper:IsDaily(id)
    return self:GetDaily(id) == 1
end

function EventActivityDataHelper:GetLevelLimit(id)
    local data = self.GetEventActivity(id)
    return data and data.limit and data.limit[1] and data.limit[1][1] or -1
end

function EventActivityDataHelper:GetPublic(id)
    local data = self.GetEventActivity(id)
    return data and data.public or nil
end

function EventActivityDataHelper:GetType(id)
    local data = self.GetEventActivity(id)
    return data and data.type or nil
end

function EventActivityDataHelper:GetTypeLanguageId(id)
    local type = self:GetType(id)
    if type == 1 or type == 2 then
        return 10592
    elseif type == 3 or type == 4 then
        return 10593
    elseif type == 5 or type == 6 then
        return 10594
    elseif type == 7 or type == 8 then
        return 10595
    else
        return 0
    end
end

function EventActivityDataHelper:GetTargets(id)
    local data = self.GetEventActivity(id)
    return data and data.targets or nil
end

function EventActivityDataHelper:GetTargetsDesc(id)
    local data = self.GetEventActivity(id)
    return data and data.targets_desc or nil
end

function EventActivityDataHelper:GetTargetsScore(id)
    local data = self.GetEventActivity(id)
    return data and data.targets_score or nil
end

function EventActivityDataHelper:GetMapId(id)
    local data = self.GetEventActivity(id)
    return data and data.map_id or nil
end

function EventActivityDataHelper:GetPos(id)
    local data = self.GetEventActivity(id)
    return data and data.pos and Vector3(data.pos[1] * 0.01, data.pos[2] * 0.01, data.pos[3] * 0.01) or nil
end

function EventActivityDataHelper:GetRange(id)
    local data = self.GetEventActivity(id)
    return data and data.range or nil
end

function EventActivityDataHelper:GetOpenTimeId(id)
    local data = self.GetEventActivity(id)
    return data and data.open_time_id or nil
end

function EventActivityDataHelper:GetLimit(id)
    local data = self.GetEventActivity(id)
    return data and data.limit or nil
end
function EventActivityDataHelper:GetFirstRewardId(id)
    local data = self.GetEventActivity(id)
    return data and data.first_reward_id or nil
end

function EventActivityDataHelper:GetFixedRewardId(id)
    local data = self.GetEventActivity(id)
    return data and data.fixed_reward_id or nil
end

function EventActivityDataHelper:GetPreviewRewardIds(id)
    local data = self.GetEventActivity(id)
    return data and data.preview_reward_ids or nil
end

function EventActivityDataHelper:GetTriggerEventIds(id)
    local data = self.GetEventActivity(id)
    return data and data.trigger_event_ids or nil
end

function EventActivityDataHelper:GetSingleMapId(id)
    local data = self.GetEventActivity(id)
    return data and data.single_map_id or nil
end

return EventActivityDataHelper
