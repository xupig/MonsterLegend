-- DragonsoulDataHelper.lua
local DragonsoulDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Dragonsoul = Class.Dragonsoul(ClassTypes.XObject)

local dragonsoulCache = {}
function DragonsoulDataHelper.ClearCache()
    dragonsoulCache = {}
end

function Dragonsoul:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.type = DataParseHelper.ParseListInt(cfg.__type)
    self.sub_type = cfg.__sub_type
    self.fx = cfg.__fx
    self.quality = cfg.__quality
    self.level = cfg.__level
end

function DragonsoulDataHelper.GetDragonsoul(id)
    ClearDataManager:UseCache(CacheNames.dragonsoul, DragonsoulDataHelper.ClearCache)
    local data = dragonsoulCache[id]
    if data == nil then
        local cfg = XMLManager.dragonsoul[id]
        if cfg ~= nil then
            data = Dragonsoul(cfg)
            dragonsoulCache[data.id] = data
        else
            LoggerHelper.Error("Dragonsoul id not exist:" .. id)
        end
    end
    return data
end

function DragonsoulDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.dragonsoul:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function DragonsoulDataHelper:GetName(id)
    local data = self.GetDragonsoul(id)
    return data and data.name or nil
end

function DragonsoulDataHelper:GetType(id)
    local data = self.GetDragonsoul(id)
    return data and data.type or nil
end

function DragonsoulDataHelper:GetSubType(id)
    local data = self.GetDragonsoul(id)
    return data and data.sub_type or nil
end

function DragonsoulDataHelper:GetFx(id)
    local data = self.GetDragonsoul(id)
    return data and data.fx or nil
end

function DragonsoulDataHelper:GetQuality(id)
    local data = self.GetDragonsoul(id)
    return data and data.quality or nil
end

function DragonsoulDataHelper:GetLevel(id)
    local data = self.GetDragonsoul(id)
    return data and data.level or nil
end

return DragonsoulDataHelper
