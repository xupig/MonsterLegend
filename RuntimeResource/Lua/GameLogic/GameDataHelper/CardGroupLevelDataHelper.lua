-- CardGroupLevelDataHelper.lua
local CardGroupLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CardGroupLevel = Class.CardGroupLevel(ClassTypes.XObject)

local cardGroupLevelCache = {}
function CardGroupLevelDataHelper.ClearCache()
    cardGroupLevelCache = {}
end

function CardGroupLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.card_group = cfg.__card_group
    self.level = cfg.__level
    self.star_need = cfg.__star_need
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
end

function CardGroupLevelDataHelper.GetCardGroupLevel(id)
    ClearDataManager:UseCache(CacheNames.card_group_level, CardGroupLevelDataHelper.ClearCache)
    local data = cardGroupLevelCache[id]
    if data == nil then
        local cfg = XMLManager.card_group_level[id]
        if cfg ~= nil then
            data = CardGroupLevel(cfg)
            cardGroupLevelCache[data.id] = data
        else
            LoggerHelper.Error("CardGroupLevel id not exist:" .. id)
        end
    end
    return data
end

function CardGroupLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.card_group_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CardGroupLevelDataHelper:GetCardGroup(id)
    local data = self.GetCardGroupLevel(id)
    return data and data.card_group or nil
end

function CardGroupLevelDataHelper:GetLevel(id)
    local data = self.GetCardGroupLevel(id)
    return data and data.level or nil
end

function CardGroupLevelDataHelper:GetStarNeed(id)
    local data = self.GetCardGroupLevel(id)
    return data and data.star_need or nil
end

function CardGroupLevelDataHelper:GetAttri(id)
    local data = self.GetCardGroupLevel(id)
    return data and data.attri or nil
end

return CardGroupLevelDataHelper
