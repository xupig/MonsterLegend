-- MarryRingDataHelper.lua
local MarryRingDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local MarryRing = Class.MarryRing(ClassTypes.XObject)

local marryRingCache = {}
function MarryRingDataHelper.ClearCache()
    marryRingCache = {}
end

function MarryRing:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.exp = cfg.__exp
    self.attri_base = DataParseHelper.ParseMSuffix(cfg.__attri_base)
    self.attri_marry = DataParseHelper.ParseMSuffix(cfg.__attri_marry)
end

function MarryRingDataHelper.GetMarryRing(id)
    ClearDataManager:UseCache(CacheNames.marry_ring, MarryRingDataHelper.ClearCache)
    local data = marryRingCache[id]
    if data == nil then
        local cfg = XMLManager.marry_ring[id]
        if cfg ~= nil then
            data = MarryRing(cfg)
            marryRingCache[data.id] = data
        else
            LoggerHelper.Error("MarryRing id not exist:" .. id)
        end
    end
    return data
end

function MarryRingDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.marry_ring:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MarryRingDataHelper:GetLevel(id)
    local data = self.GetMarryRing(id)
    return data and data.level or nil
end

function MarryRingDataHelper:GetExp(id)
    local data = self.GetMarryRing(id)
    return data and data.exp or nil
end

function MarryRingDataHelper:GetAttriBase(id)
    local data = self.GetMarryRing(id)
    return data and data.attri_base or nil
end

function MarryRingDataHelper:GetAttriMarry(id)
    local data = self.GetMarryRing(id)
    return data and data.attri_marry or nil
end

return MarryRingDataHelper
