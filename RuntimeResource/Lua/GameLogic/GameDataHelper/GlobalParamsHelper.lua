--全局参数配置解析类
local GlobalParamsHelper = {}
local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local GlobalParam = Class.GlobalParam(ClassTypes.XObject)
local globalParamsCache = {}

function GlobalParam:__ctor__(cfg)
    self.id = cfg.__id
    local key,value = self:ConvertValue(cfg.__key,cfg.__value)
    self.key = key
    self.paramValue = value
end

function GlobalParam:ConvertValue(key,value)
    local result
    local prefix = string.sub(key, -2)
    local key2 = string.sub(key, 0, -3)
    if prefix == "_i" then
        result = tonumber(value)
    elseif prefix == "_f" then
        result = tonumber(value)
    elseif prefix == "_s" then
        result = tostring(value)
    elseif prefix == "_l" then
        --list
        result = DataParseHelper.ParseListInt(value)
    elseif prefix == "_m" then
        --msuffix
        result= DataParseHelper.ParseMSuffix(value)
    elseif prefix == '_n' then
        result = DataParseHelper.ParseNSuffix(value)
    else
        result = value
    end
    return key2,result
end


function GlobalParamsHelper.GetParamValue(id)
    local data = globalParamsCache[id]
    if data == nil then
        local cfg = XMLManager.global_params[id]
        if cfg ~= nil then
            data = GlobalParam(cfg)
            globalParamsCache[data.id] = data
        else
            LoggerHelper.Error("GlobalParam id not exist:" .. id)
        end
    end
    return data.paramValue
end

function GlobalParamsHelper.GetMStringValue(id)
    local cfg = XMLManager.global_params[id]
    if cfg ~= nil then
        return DataParseHelper.ParseMSuffix(cfg.__value, true)
    else
        LoggerHelper.Error("GlobalParam id not exist:" .. id)
    end
end

--特殊处理_m sort
function GlobalParamsHelper.GetMSortValue(id)
    local cfg = XMLManager.global_params[id]
    if cfg ~= nil then
        return DataParseHelper.ParseMSuffixSort(cfg.__value)
    else
        LoggerHelper.Error("GlobalParam id not exist:" .. id)
    end
end

--特殊处理_n sort
function GlobalParamsHelper.GetNSortValue(id)
    local cfg = XMLManager.global_params[id]
    if cfg ~= nil then
        return DataParseHelper.ParseNSuffixSort(cfg.__value)
    else
        LoggerHelper.Error("GlobalParam id not exist:" .. id)
    end
end

return GlobalParamsHelper