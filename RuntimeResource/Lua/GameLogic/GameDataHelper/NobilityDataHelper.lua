-- NobilityDataHelper.lua
--爵位配置
local NobilityDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local nobilityCfgCache = {}
function NobilityDataHelper.ClearCache()
    nobilityCfgCache = {}
end

local NobilityCfg = Class.NobilityCfg(ClassTypes.XObject)
function NobilityCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.nobility_name = cfg.__nobility_name
    self.nobility_icon = cfg.__nobility_icon
    self.nobility_show = cfg.__nobility_show
    self.nobility_atrri = DataParseHelper.ParseMSuffixSort(cfg.__nobility_atrri)
    self.nobility_levelup_need = cfg.__nobility_levelup_need
    self.nobility_levelup_cost = DataParseHelper.ParseMSuffix(cfg.__nobility_levelup_cost)
end

function NobilityDataHelper.GetNobilityCfg(id)
    ClearDataManager:UseCache(CacheNames.nobility, NobilityDataHelper.ClearCache)
	local data = nobilityCfgCache[id]
    if data == nil then
        local cfg = XMLManager.nobility[id]
        data = NobilityCfg(cfg)
        nobilityCfgCache[data.id] = data
    end
    return data
end

function NobilityDataHelper:GetNobilityItems()
    local result = {}
    local keys = XMLManager.nobility:Keys()
    for i=1,#keys do
        local cfg = NobilityDataHelper.GetNobilityCfg(keys[i])
        for k,v in pairs(cfg.nobility_levelup_cost) do
            result[k] = v
        end
    end
    return result
end

function NobilityDataHelper:GetNobilityName(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_name or nil
end

function NobilityDataHelper:GetNobilityIcon(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_icon or nil
end

function NobilityDataHelper:GetNobilityShow(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_show or nil
end

function NobilityDataHelper:GetNobilityAtrri(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_atrri or nil
end

function NobilityDataHelper:GetNobilityLevelupNeed(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_levelup_need or nil
end

function NobilityDataHelper:GetNobilityLevelupCost(id)
    local data = self.GetNobilityCfg(id)
    return data and data.nobility_levelup_cost or nil
end

return NobilityDataHelper