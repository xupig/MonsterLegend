-- AionDataHelper.lua
local AionDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Aion = Class.Aion(ClassTypes.XObject)

local aionCache = {}
function AionDataHelper.ClearCache()
    aionCache = {}
end

function Aion:__ctor__(cfg)
    self.id = cfg.__id
    self.map_id = cfg.__map_id
    self.monster_id = DataParseHelper.ParseListInt(cfg.__monster_id)
    self.time = cfg.__time
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.score_propese = cfg.__score_propese
    self.mail_reward = DataParseHelper.ParseMSuffix(cfg.__mail_reward)
    self.target_reward = DataParseHelper.ParseNSuffix(cfg.__target_reward)
    self.reward_show = DataParseHelper.ParseMSuffixSort(cfg.__reward_show)
end

function AionDataHelper.GetAion(id)
    ClearDataManager:UseCache(CacheNames.aion, AionDataHelper.ClearCache)
    local data = aionCache[id]
    if data == nil then
        local cfg = XMLManager.aion[id]
        if cfg ~= nil then
            data = Aion(cfg)
            aionCache[data.id] = data
        else
            LoggerHelper.Error("Aion id not exist:" .. id)
        end
    end
    return data
end

function AionDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.aion:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function AionDataHelper:GetMapId(id)
    local data = self.GetAion(id)
    return data and data.map_id or nil
end

function AionDataHelper:GetMonsterId(id)
    local data = self.GetAion(id)
    return data and data.monster_id or nil
end

function AionDataHelper:GetTime(id)
    local data = self.GetAion(id)
    return data and data.time or nil
end

function AionDataHelper:GetReward(id)
    local data = self.GetAion(id)
    return data and data.reward or nil
end

function AionDataHelper:GetScorePropese(id)
    local data = self.GetAion(id)
    return data and data.score_propese or nil
end

function AionDataHelper:GetMailReward(id)
    local data = self.GetAion(id)
    return data and data.mail_reward or nil
end

function AionDataHelper:GetTargetReward(id)
    local data = self.GetAion(id)
    return data and data.target_reward or nil
end

function AionDataHelper:GetRewardShow(id)
    local data = self.GetAion(id)
    return data and data.reward_show or nil
end

return AionDataHelper
