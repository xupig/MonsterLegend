-- PetInstanceDataHelper.lua
local PetInstanceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ProtectHolyTower = Class.ProtectHolyTower(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

local protectHolyTowerCache = {}
function PetInstanceDataHelper.ClearCache()
    protectHolyTowerCache = {}
end

function ProtectHolyTower:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.name = LanguageDataHelper.CreateContent(cfg.__name)
    self.icon = cfg.__icon
    self.describe = LanguageDataHelper.CreateContent(cfg.__describe)
    self.time = cfg.__time
    self.preview_reward = DataParseHelper.ParseListInt(cfg.__preview_reward)
    self.star_time = DataParseHelper.ParseNSuffix(cfg.__star_time)
end

function PetInstanceDataHelper.GetProtectHolyTower(id)
    ClearDataManager:UseCache(CacheNames.protect_holy_tower, PetInstanceDataHelper.ClearCache)
    local data = protectHolyTowerCache[id]
    if data == nil then
        local cfg = XMLManager.protect_holy_tower[id]
        if cfg ~= nil then
            data = ProtectHolyTower(cfg)
            protectHolyTowerCache[data.id] = data
        else
            LoggerHelper.Error("ProtectHolyTower id not exist:" .. id)
        end
    end
    return data
end

function PetInstanceDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.protect_holy_tower:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PetInstanceDataHelper:GetLevel(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.level or nil
end

function PetInstanceDataHelper:GetIcon(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.icon or nil
end

function PetInstanceDataHelper:GetName(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.name or nil
end

function PetInstanceDataHelper:GetDescribe(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.describe or nil
end

function PetInstanceDataHelper:GetTime(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.time or nil
end

function PetInstanceDataHelper:GetPreviewReward(id)
    local data = self.GetProtectHolyTower(id)
    return data and data.preview_reward or nil
end

function PetInstanceDataHelper:GetStartTime(id)
    local data = self.GetProtectHolyTower(id)
    local returnData = {}
    for k,v in pairs(data.star_time) do
        returnData[k] = v
    end
    return returnData
end

return PetInstanceDataHelper
