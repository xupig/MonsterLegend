-- ZeroDollarBuyDataHelper.lua
local ZeroDollarBuyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local ZeroDollarBuy = Class.ZeroDollarBuy(ClassTypes.XObject)

function ZeroDollarBuy:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
    self.price = DataParseHelper.ParseMSuffix(cfg.__price)
    self.return_reward = DataParseHelper.ParseMSuffix(cfg.__return_reward)
    self.return_time = cfg.__return_time
    self.level = cfg.__level
    self.display = DataParseHelper.ParseNSuffix(cfg.__display)
    self.icon = DataParseHelper.ParseListInt(cfg.__icon)
end

local zeroDollarBuyCache = {}
function ZeroDollarBuyDataHelper.ClearCache()
    zeroDollarBuyCache = {}
end

function ZeroDollarBuyDataHelper:GetData(id)
    ClearDataManager:UseCache(CacheNames.zero_price, ZeroDollarBuyDataHelper.ClearCache)
    local data = zeroDollarBuyCache[id]
    if data == nil then
        local cfg = XMLManager.zero_price[id]
        if cfg ~= nil then
            data = ZeroDollarBuy(cfg)
            zeroDollarBuyCache[data.id] = data
        else
            LoggerHelper.Error("zero_price id not exist:" .. id)
        end
    end
    return data
end

function ZeroDollarBuyDataHelper:GetPrice(id)
    local data = self:GetData(id)
    return data and data.price or nil
end

function ZeroDollarBuyDataHelper:GetIcon(id)
    local data = self:GetData(id)
    return data and data.icon or nil
end

function ZeroDollarBuyDataHelper:GetReturnReward(id)
    local data = self:GetData(id)
    return data and data.return_reward or nil
end

function ZeroDollarBuyDataHelper:GetReward(id)
    local data = self:GetData(id)
    return data and data.reward or nil
end

function ZeroDollarBuyDataHelper:GetDisplay(id)
    local data = self:GetData(id)
    return data and data.display or nil
end

function ZeroDollarBuyDataHelper:GetTime(id)
    local data = self:GetData(id)
    return data and data.return_time or 0
end

function ZeroDollarBuyDataHelper:GetLevel(id)
    local data = self:GetData(id)
    return data and data.level or 0
end

return ZeroDollarBuyDataHelper
