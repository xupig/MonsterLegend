-- Welfare7dayRewardDataHelper.lua
local Welfare7dayRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Welfare7dayReward = Class.Welfare7dayReward(ClassTypes.XObject)

local welfare7dayRewardCache = {}
function Welfare7dayRewardDataHelper.ClearCache()
    welfare7dayRewardCache = {}
end

function Welfare7dayReward:__ctor__(cfg)
    self.id = cfg.__id
    self.days = cfg.__days
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
    self.icon = DataParseHelper.ParseListInt(cfg.__icon)
    self.big_pic = DataParseHelper.ParseListInt(cfg.__big_pic)
end

function Welfare7dayRewardDataHelper.GetWelfare7dayReward(id)
    ClearDataManager:UseCache(CacheNames.welfare_7day_reward, Welfare7dayRewardDataHelper.ClearCache)
    local data = welfare7dayRewardCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_7day_reward[id]
        if cfg ~= nil then
            data = Welfare7dayReward(cfg)
            welfare7dayRewardCache[data.id] = data
        else
            LoggerHelper.Error("Welfare7dayReward id not exist:" .. id)
        end
    end
    return data
end

function Welfare7dayRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_7day_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function Welfare7dayRewardDataHelper:GetDays(id)
    local data = self.GetWelfare7dayReward(id)
    return data and data.days or nil
end

function Welfare7dayRewardDataHelper:GetReward(id)
    local data = self.GetWelfare7dayReward(id)
    return data and data.reward or nil
end

function Welfare7dayRewardDataHelper:GetIcons(id)
    local data = self.GetWelfare7dayReward(id)
    return data and data.icon or nil
end

function Welfare7dayRewardDataHelper:GetBigPic(id)
    local data = self.GetWelfare7dayReward(id)
    return data and data.big_pic or {}
end

function Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, id)
    local data = self:GetReward(id)
    local rewardDatas = data[group] 
    if data == nil or rewardDatas == nil then return nil end
    local rewards = {}
    for i=1,#rewardDatas,2 do
        local reward = {}
        reward.id = rewardDatas[i]
        reward.num = rewardDatas[i+1]
        table.insert(rewards, reward)
    end
    return rewards
end

return Welfare7dayRewardDataHelper
