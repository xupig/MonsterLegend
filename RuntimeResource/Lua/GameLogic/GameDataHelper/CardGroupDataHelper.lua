-- CardGroupDataHelper.lua
local CardGroupDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local CardGroup = Class.CardGroup(ClassTypes.XObject)

local cardGroupCache = {}
function CardGroupDataHelper.ClearCache()
    cardGroupCache = {}
end

function CardGroup:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.card_id = DataParseHelper.ParseListInt(cfg.__card_id)
end

function CardGroupDataHelper.GetCardGroup(id)
    ClearDataManager:UseCache(CacheNames.card_group, CardGroupDataHelper.ClearCache)
    local data = cardGroupCache[id]
    if data == nil then
        local cfg = XMLManager.card_group[id]
        if cfg ~= nil then
            data = CardGroup(cfg)
            cardGroupCache[data.id] = data
        else
            LoggerHelper.Error("CardGroup id not exist:" .. id)
        end
    end
    return data
end

function CardGroupDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.card_group:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function CardGroupDataHelper:GetName(id)
    local data = self.GetCardGroup(id)
    return data and data.name or nil
end

function CardGroupDataHelper:GetCardId(id)
    local data = self.GetCardGroup(id)
    return data and data.card_id or nil
end

return CardGroupDataHelper
