-- ActivityLevelDataHelper.lua
local ActivityLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ActivityLevel = Class.ActivityLevel(ClassTypes.XObject)

local activityLevelCache = {}
function ActivityLevelDataHelper.ClearCache()
    activityLevelCache = {}
end

function ActivityLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.next_cost = cfg.__next_cost
    self.atrri = DataParseHelper.ParseMSuffixSort(cfg.__atrri)
end

function ActivityLevelDataHelper.GetActivityLevel(id)
    ClearDataManager:UseCache(CacheNames.activity_level, ActivityLevelDataHelper.ClearCache)
    local data = activityLevelCache[id]
    if data == nil then
        local cfg = XMLManager.activity_level[id]
        if cfg ~= nil then
            data = ActivityLevel(cfg)
            activityLevelCache[data.id] = data
        else
            LoggerHelper.Error("ActivityLevel id not exist:" .. id)
        end
    end
    return data
end

function ActivityLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityLevelDataHelper:GetNextCost(id)
    local data = self.GetActivityLevel(id)
    return data and data.next_cost or nil
end

function ActivityLevelDataHelper:GetAtrri(id)
    local data = self.GetActivityLevel(id)
    return data and data.atrri or nil
end

function ActivityLevelDataHelper:GetMaxLevel()
    local data = XMLManager.activity_level:Keys()
    if data[0] ~= nil then
        return #data - 1
    end
    return #data
end

return ActivityLevelDataHelper
