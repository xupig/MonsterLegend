-- ActivityDataHelper.lua
local ActivityDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local Activity = Class.Activity(ClassTypes.XObject)

local activityCache = {}
function ActivityDataHelper.ClearCache()
    activityCache = {}
end

function Activity:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.type = cfg.__type
    self.icon = cfg.__icon
    self.desc = cfg.__desc
    self.times = cfg.__times
    self.time = cfg.__time
    self.tag = DataParseHelper.ParseMSuffixSort(cfg.__tag)
    self.level = cfg.__level
    self.reward_preview = DataParseHelper.ParseListInt(cfg.__reward_preview)
    self.follow = DataParseHelper.ParseNSuffix(cfg.__follow)
    self.func = DataParseHelper.ParseNSuffixSort(cfg.__func, true)
end

function ActivityDataHelper.GetActivity(id)
    ClearDataManager:UseCache(CacheNames.activity, ActivityDataHelper.ClearCache)
    local data = activityCache[id]
    if data == nil then
        local cfg = XMLManager.activity[id]
        if cfg ~= nil then
            data = Activity(cfg)
            activityCache[data.id] = data
        else
            LoggerHelper.Error("Activity id not exist:" .. id)
        end
    end
    return data
end

function ActivityDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.activity:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function ActivityDataHelper:GetName(id)
    local data = self.GetActivity(id)
    return data and data.name or nil
end

function ActivityDataHelper:GetType(id)
    local data = self.GetActivity(id)
    return data and data.type or nil
end

function ActivityDataHelper:GetIcon(id)
    local data = self.GetActivity(id)
    return data and data.icon or nil
end

function ActivityDataHelper:GetDesc(id)
    local data = self.GetActivity(id)
    return data and data.desc or nil
end

function ActivityDataHelper:GetTimes(id)
    local data = self.GetActivity(id)
    return data and data.times or nil
end

function ActivityDataHelper:GetTime(id)
    local data = self.GetActivity(id)
    return data and data.time or nil
end

function ActivityDataHelper:GetTag(id)
    local data = self.GetActivity(id)
    return data and data.tag or nil
end

function ActivityDataHelper:GetLevel(id)
    local data = self.GetActivity(id)
    return data and data.level or nil
end

function ActivityDataHelper:GetRewardPreview(id)
    local data = self.GetActivity(id)
    return data and data.reward_preview or nil
end

function ActivityDataHelper:GetFollow(id)
    local data = self.GetActivity(id)
    return data and data.follow or nil
end

function ActivityDataHelper:GetFunc(id)
    local data = self.GetActivity(id)
    return data and data.func or nil
end

return ActivityDataHelper
