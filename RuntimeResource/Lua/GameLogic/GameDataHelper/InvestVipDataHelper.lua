-- InvestVipDataHelper.lua
local InvestVipDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local InvestVip = Class.InvestVip(ClassTypes.XObject)

local investVipCache = {}
function InvestVipDataHelper.ClearCache()
    investVipCache = {}
end

function InvestVip:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
    self.world = DataParseHelper.ParseListInt(cfg.__world)
end

function InvestVipDataHelper.GetInvestVip(id)
    ClearDataManager:UseCache(CacheNames.invest_vip, InvestVipDataHelper.ClearCache)
    local data = investVipCache[id]
    if data == nil then
        local cfg = XMLManager.invest_vip[id]
        if cfg ~= nil then
            data = InvestVip(cfg)
            investVipCache[data.id] = data
        else
            LoggerHelper.Error("InvestVip id not exist:" .. id)
        end
    end
    return data
end

function InvestVipDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.invest_vip:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function InvestVipDataHelper:GetDay(id)
    local data = self.GetInvestVip(id)
    return data and data.day or nil
end

function InvestVipDataHelper:GetReward(id)
    local data = self.GetInvestVip(id)
    return data and data.reward or nil
end

function InvestVipDataHelper:GetWorld(id)
    local data = self.GetInvestVip(id)
    return data and data.world or nil
end

function InvestVipDataHelper:GetCfgType()
    if self._cfg==nil then 
        self._cfg={} 
        local ids = InvestVipDataHelper:GetAllId()
        for i,id in ipairs(ids) do
            if InvestVipDataHelper:GetDay(id)==1 then
                self._cfg[i]=InvestVipDataHelper:GetWorld(id)
            end
        end
    end
    return self._cfg
end

function InvestVipDataHelper:GetCfgTypeIndex(worldlevel)
    local cfg = self:GetCfgType()
    -- LoggerHelper.Log(cfg)
    for k,v in pairs(cfg) do
        if v[1]<=worldlevel and v[2]>=worldlevel then
            return tonumber(k)
        end
    end
    return 1
end
return InvestVipDataHelper
