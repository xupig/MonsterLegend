local BuffDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = require("ServerConfig/public_config")
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local buffData = {}
local BuffCfg = Class.BuffCfg(ClassTypes.XObject)

function BuffCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.desc = cfg.__desc
	self.icon = cfg.__icon
    self.client_show = cfg.__client_show
end

function BuffDataHelper:GetBuffData(id)
    local data = buffData[id]
	if data ~= nil then
		return data
	end
	data = XMLManager.buff[id]
    if data~=nil then
	    data = BuffCfg(data)
    end
	buffData[id] = data
	return data
end

function BuffDataHelper:GetBuffName(id)
	local data = self:GetBuffData(id)
	return LanguageDataHelper.GetContent(data.name)
end

function BuffDataHelper:GetBuffDesc(id)
	local data = self:GetBuffData(id)
	return LanguageDataHelper.GetContent(data.desc)
end

function BuffDataHelper:GetBuffIcon(id)
	local data = self:GetBuffData(id)
	return data and data.icon or nil
end

function BuffDataHelper:GetClientShow(id)
    local data = self:GetBuffData(id)
    return data and data.client_show or nil
end

return BuffDataHelper