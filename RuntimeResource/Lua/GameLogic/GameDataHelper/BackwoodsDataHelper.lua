-- BackwoodsDataHelper.lua
local BackwoodsDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local Backwoods = Class.Backwoods(ClassTypes.XObject)

function Backwoods:__ctor__(cfg)
    self.id = cfg.__id
    self.floor = cfg.__floor
    self.sence_id = cfg.__sence_id
    self.level_limit = cfg.__level_limit
    self.boss_level = cfg.__boss_level
    self.boss_name = cfg.__boss_name
    self.boss_name_icon = cfg.__boss_name_icon
    self.boss_icon = cfg.__boss_icon
    self.boss_monster_id = cfg.__boss_monster_id
    self.boss_drops = DataParseHelper.ParseNSuffix(cfg.__boss_drops)
    self.boss_region_id = cfg.__boss_region_id
    self.level_show = cfg.__level_show
end

local backwoodsCache = {}
function BackwoodsDataHelper.ClearCache()
    backwoodsCache = {}
end

function BackwoodsDataHelper:GetBackwoodsData(id)
    ClearDataManager:UseCache(CacheNames.forbidden_place, BackwoodsDataHelper.ClearCache)
    local data = backwoodsCache[id]
    if data == nil then
        local cfg = XMLManager.forbidden_place[id]
        if cfg ~= nil then
            data = Backwoods(cfg)
            backwoodsCache[data.id] = data
        else
            LoggerHelper.Error("Backwoods id not exist:" .. id)
        end
    end
    return data
end

function BackwoodsDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.forbidden_place:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function BackwoodsDataHelper:GetMonsterByMapId(mapId)
    local data = XMLManager.forbidden_place:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetSenceId(v) == mapId then
            return v
        end
    end
    return nil
end

--根据层，获取所有的bossId
local floorIdsData = {}
function BackwoodsDataHelper:GetIdsByFloor(floor)
    if floorIdsData[floor] == nil then
        floorIdsData[floor] = {}
    end
    local keys = XMLManager.forbidden_place:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetBackwoodsData(id)
            if data.floor == floor then
                local tmp = {}
                tmp.id = id
                tmp.order = data.boss_level
                table.insert(floorIdsData[floor],tmp)
            end
            table.sort(floorIdsData[floor], SortByOrder)
        end
    end

    return floorIdsData[floor]
end

function BackwoodsDataHelper:GetSenceId(id)
    local data = self:GetBackwoodsData(id)
    return data and data.sence_id or nil
end

function BackwoodsDataHelper:GetLevelLimit(id)
    local data = self:GetBackwoodsData(id)
    return data and data.level_limit or nil
end

function BackwoodsDataHelper:GetBossLevel(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_level or nil
end

function BackwoodsDataHelper:GetBossName(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_name or nil
end

function BackwoodsDataHelper:GetBossNameIcon(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_name_icon or nil
end

function BackwoodsDataHelper:GetBossIcon(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_icon or nil
end

function BackwoodsDataHelper:GetBossMonsterId(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_monster_id or nil
end

function BackwoodsDataHelper:GetBossDrops(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_drops or nil
end

function BackwoodsDataHelper:GetBossRegionId(id)
    local data = self:GetBackwoodsData(id)
    return data and data.boss_region_id or nil
end

function BackwoodsDataHelper:GetBossFloor(id)
    local data = self:GetBackwoodsData(id)
    return data and data.floor or nil
end

function BackwoodsDataHelper:GetLevelShow(id)
    local data = self:GetBackwoodsData(id)
    return data and data.level_show or 0
end

---------------------------------------蛮荒禁地精英怪配置------------------
local BackwoodsElite = Class.BackwoodsElite(ClassTypes.XObject)

function BackwoodsElite:__ctor__(cfg)
    self.id = cfg.__id
    self.floor = cfg.__floor
    self.sence_id = cfg.__sence_id
    self.monster_level = cfg.__monster_level
    self.monster_id = cfg.__monster_id
    self.angry_num = cfg.__angry_num
    self.monster_region_id = cfg.__monster_region_id
end

local backwoodsEliteCache = {}
function BackwoodsDataHelper.ClearEliteCache()
    backwoodsEliteCache = {}
end

function BackwoodsDataHelper:GetBackwoodsEliteData(id)
    ClearDataManager:UseCache(CacheNames.forbidden_place_monsters, BackwoodsDataHelper.ClearEliteCache)
    local data = backwoodsEliteCache[id]
    if data == nil then
        local cfg = XMLManager.forbidden_place_monsters[id]
        if cfg ~= nil then
            data = BackwoodsElite(cfg)
            backwoodsEliteCache[data.id] = data
        else
            LoggerHelper.Error("BackwoodsElite id not exist:" .. id)
        end
    end
    return data
end

local floorIdsEliteData = {}
function BackwoodsDataHelper:GetEliteIdsByFloor(floor)
    if floorIdsEliteData[floor] == nil then
        floorIdsEliteData[floor] = {}
    end
    local keys = XMLManager.forbidden_place_monsters:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetBackwoodsEliteData(id)
            if data.floor == floor then
                local tmp = {}
                tmp.id = id
                tmp.order = data.id
                table.insert(floorIdsEliteData[floor],tmp)
            end
            table.sort(floorIdsEliteData[floor], SortByOrder)
        end
    end
    return floorIdsEliteData[floor]
end

function BackwoodsDataHelper:GetEliteSenceId(id)
    local data = self:GetBackwoodsEliteData(id)
    return data.sence_id
end

function BackwoodsDataHelper:GetEliteRegionId(id)
    local data = self:GetBackwoodsEliteData(id)
    return data.monster_region_id
end

function BackwoodsDataHelper:GetEliteMonsterId(id)
    local data = self:GetBackwoodsEliteData(id)
    return data.monster_id
end

function BackwoodsDataHelper:GetEliteLevel(id)
    local data = self:GetBackwoodsEliteData(id)
    return data.monster_level
end

function BackwoodsDataHelper:GetEliteAngry(id)
    local data = self:GetBackwoodsEliteData(id)
    return data.angry_num
end

return BackwoodsDataHelper
