-- PromisImportantDataHelper.lua
local PromisImportantDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local PromisImportant = Class.PromisImportant(ClassTypes.XObject)

local promisImportantCache = {}
function PromisImportantDataHelper.ClearCache()
    promisImportantCache = {}
end

function PromisImportant:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.important = cfg.__important
    self.normal = cfg.__normal
    self.display = DataParseHelper.ParseListInt(cfg.__display)
end

function PromisImportantDataHelper.GetPromisImportant(id)
    ClearDataManager:UseCache(CacheNames.promis_important, PromisImportantDataHelper.ClearCache)
    local data = promisImportantCache[id]
    if data == nil then
        local cfg = XMLManager.promis_important[id]
        if cfg ~= nil then
            data = PromisImportant(cfg)
            promisImportantCache[data.id] = data
        else
            LoggerHelper.Error("PromisImportant id not exist:" .. id)
        end
    end
    return data
end

function PromisImportantDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.promis_important:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function PromisImportantDataHelper:GetDay(id)
    local data = self.GetPromisImportant(id)
    return data and data.day or nil
end

function PromisImportantDataHelper:GetImportant(id)
    local data = self.GetPromisImportant(id)
    return data and data.important or nil
end

function PromisImportantDataHelper:GetNormal(id)
    local data = self.GetPromisImportant(id)
    return data and data.normal or nil
end

function PromisImportantDataHelper:GetDisplay(id)
    local data = self.GetPromisImportant(id)
    return data and data.display or nil
end

return PromisImportantDataHelper
