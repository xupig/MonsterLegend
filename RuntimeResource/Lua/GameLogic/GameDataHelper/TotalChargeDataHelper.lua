-- TotalChargeDataHelper.lua
local TotalChargeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local TotalCharge = Class.TotalCharge(ClassTypes.XObject)

local totalChargeCache = {}
function TotalChargeDataHelper.ClearCache()
    totalChargeCache = {}
end

function TotalCharge:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.vocation = cfg.__vocation
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
end

function TotalChargeDataHelper.GetTotalCharge(id)
    ClearDataManager:UseCache(CacheNames.total_charge, TotalChargeDataHelper.ClearCache)
    local data = totalChargeCache[id]
    if data == nil then
        local cfg = XMLManager.total_charge[id]
        if cfg ~= nil then
            data = TotalCharge(cfg)
            totalChargeCache[data.id] = data
        else
            LoggerHelper.Error("TotalCharge id not exist:" .. id)
        end
    end
    return data
end

function TotalChargeDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.total_charge:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function TotalChargeDataHelper:GetCost(id)
    local data = self.GetTotalCharge(id)
    return data and data.cost or nil
end

function TotalChargeDataHelper:GetReward(id)
    local data = self.GetTotalCharge(id)
    return data and data.reward or nil
end

function TotalChargeDataHelper:GetVocation(id)
    local data = self.GetTotalCharge(id)
    return data and data.vocation or nil
end

function TotalChargeDataHelper:GetCfg()
    if self._cfg==nil then
        self._cfg={}
    else
        return self._cfg
    end
    local ids = self:GetAllId()
    for _,v in ipairs(ids) do
        local vocation = self:GetVocation(v)
        local cost = self:GetCost(v)
        if self._cfg[vocation]==nil then
            self._cfg[vocation]={}
        end
        table.insert(self._cfg[vocation],self.GetTotalCharge(v))
    end
    return self._cfg
end

function TotalChargeDataHelper:GetRewardsByVocation(vocation)
   local cfg = self:GetCfg()
   if cfg and cfg[vocation] then
        return cfg[vocation]
    end
end

return TotalChargeDataHelper
