-- DragonsoulSlotDataHelper.lua
local DragonsoulSlotDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local DragonsoulSlot = Class.DragonsoulSlot(ClassTypes.XObject)

local dragonsoulSlotCache = {}
function DragonsoulSlotDataHelper.ClearCache()
    dragonsoulSlotCache = {}
end

function DragonsoulSlot:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.level = cfg.__level
end

function DragonsoulSlotDataHelper.GetDragonsoulSlot(id)
    ClearDataManager:UseCache(CacheNames.dragonsoul_slot, DragonsoulSlotDataHelper.ClearCache)
    local data = dragonsoulSlotCache[id]
    if data == nil then
        local cfg = XMLManager.dragonsoul_slot[id]
        if cfg ~= nil then
            data = DragonsoulSlot(cfg)
            dragonsoulSlotCache[data.id] = data
        else
            LoggerHelper.Error("DragonsoulSlot id not exist:" .. id)
        end
    end
    return data
end

function DragonsoulSlotDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.dragonsoul_slot:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function DragonsoulSlotDataHelper:GetType(id)
    local data = self.GetDragonsoulSlot(id)
    return data and data.type or nil
end

function DragonsoulSlotDataHelper:GetLevel(id)
    local data = self.GetDragonsoulSlot(id)
    return data and data.level or nil
end

return DragonsoulSlotDataHelper
