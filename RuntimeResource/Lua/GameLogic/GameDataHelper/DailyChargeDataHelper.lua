local DailyChargeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = require("ServerConfig/public_config")
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local function SortByKey1(a,b)
    return tonumber(a[1]) < tonumber(b[1])
end

------------------------------循环奖励表-------------------
local DailyChargeCfg = Class.DailyChargeCfg(ClassTypes.XObject)

local dailyChargeCache = {}
function DailyChargeDataHelper.ClearCache()
    dailyChargeCache = {}
end

function DailyChargeCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.day = cfg.__day
    self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function DailyChargeDataHelper:GetDailyChargeData(id)
    ClearDataManager:UseCache(CacheNames.daily_charge, DailyChargeDataHelper.ClearCache)
    local data = dailyChargeCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.daily_charge[id]
    if data~=nil then
	    data = DailyChargeCfg(data)
    end
	dailyChargeCache[id] = data
	return data
end

function DailyChargeDataHelper:GetDailyChargeDataByArgs(cost, day, worldLevel)
    local keys = XMLManager.daily_charge:Keys()
    local result
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetDailyChargeData(id)
            if data.cost == cost and data.day == day and data.world_level[1] <= worldLevel 
                    and data.world_level[2] >= worldLevel then
                result = data
                break
            end
        end
    end
    return result
end

------------------------------前7天奖励表-------------------
local MorrowDailyChargeCfg = Class.MorrowDailyChargeCfg(ClassTypes.XObject)

local morrowDailyChargeCache = {}
function DailyChargeDataHelper.ClearMorrowDailyChargeCache()
    morrowDailyChargeCache = {}
end

function MorrowDailyChargeCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function DailyChargeDataHelper:GetMorrowDailyChargeData(id)
    ClearDataManager:UseCache(CacheNames.morrow_daily_charge, DailyChargeDataHelper.ClearMorrowDailyChargeCache)
    local data = morrowDailyChargeCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.morrow_daily_charge[id]
    if data~=nil then
	    data = MorrowDailyChargeCfg(data)
    end
	morrowDailyChargeCache[id] = data
	return data
end

function DailyChargeDataHelper:GetMorrowDailyChargeDataByArgs(cost, day)
    local keys = XMLManager.morrow_daily_charge:Keys()
    local result
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetMorrowDailyChargeData(id)
            if data.cost == cost and data.day == day then
                result = data
                break
            end
        end
    end
    return result
end

------------------------------累计天数奖励表-------------------
local CumulativeDailyChargeCfg = Class.CumulativeDailyChargeCfg(ClassTypes.XObject)

local cumulativeDailyChargeCache = {}
function DailyChargeDataHelper.ClearCumulativeDailyChargeCache()
    cumulativeDailyChargeCache = {}
end

function CumulativeDailyChargeCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
end

function DailyChargeDataHelper:GetCumulativeDailyChargeData(id)
    ClearDataManager:UseCache(CacheNames.cumulative_daily_charge, DailyChargeDataHelper.ClearCumulativeDailyChargeCache)
    local data = cumulativeDailyChargeCache[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.cumulative_daily_charge[id]
    if data~=nil then
	    data = CumulativeDailyChargeCfg(data)
    end
	cumulativeDailyChargeCache[id] = data
	return data
end

local allCumulativeDailyChargeCache
function DailyChargeDataHelper:GetAllCumulativeDailyChargeData()
    if allCumulativeDailyChargeCache == nil then
        allCumulativeDailyChargeCache = {}
        local keys = XMLManager.cumulative_daily_charge:Keys()
        local result = {}
        for i=1,#keys do
            local id = keys[i]
            if id ~= 0 then
                table.insert(allCumulativeDailyChargeCache, {id})
            end
        end
        table.sort(allCumulativeDailyChargeCache, SortByKey1)
    end
    return allCumulativeDailyChargeCache
end

return DailyChargeDataHelper