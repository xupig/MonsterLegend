-- RoleDataHelper.lua
RoleDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function RoleDataHelper.GetModelByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__model
    end
    return nil
end
function RoleDataHelper.GetModeById(id)
    local data = XMLManager.role_data[id]
    if (data~=nil) then 
        return data.__model
    end
    return nil
end
function RoleDataHelper.GetVoacationById(id)
    local data = XMLManager.role_data[id]
    if (data~=nil) then 
        return data.__voc_group
    end
    return nil
end
function RoleDataHelper.GetRoleNameTextByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return LanguageDataHelper.GetContent(data.__role_desc_id)
    end
    return nil
end
--英文名称
function RoleDataHelper.GetRoleName2TextByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return LanguageDataHelper.GetContent(data.__role_desc2_id)
    end
    return nil
end
--半身像icon
function RoleDataHelper.GetIconIdByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__role_icon_id
    end
    return 0
end

function RoleDataHelper.GetNameIconIdByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__role_name_icon
    end
    return 0
end

--名字颜色
function RoleDataHelper.GetColorIdByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__role_color
    end
    return 0
end

function RoleDataHelper.GetIconId2ByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__role_icon2_id
    end
    return 0
end

--小头像
function RoleDataHelper.GetHeadIconByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__head_icon
    end
    return 0
end

function RoleDataHelper.GetRoleDescTextByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return LanguageDataHelper.GetContent(data.__role_desc_for_detail)
    end
    return nil
end

--初始展示动作技能
function RoleDataHelper.GetRoleBornSkillByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__skill_born
    end
    return nil
end

function RoleDataHelper.GetRoleRaceByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return LanguageDataHelper.GetContent(data.__race)
    end
    return nil
end

function RoleDataHelper.GetRoleRaceDescByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return LanguageDataHelper.CreateContent(data.__race_desc)
    end
    return nil
end

function RoleDataHelper.GetRoleRaceIconByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__race_icon
    end
    return 0
end

function RoleDataHelper.GetRoleBigIconByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__skillui_icon
    end
    return 0
end


function RoleDataHelper.GetRoleUseStarsByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__use_stars
    end
    return 1
end

function RoleDataHelper.GetRoleShowSkillByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return DataParseHelper.ParseListInt(data.__skill_show)
    end
    return {}
end

local roleHeadData = {}
function RoleDataHelper.GetRoleHeadByVocation(vocation)
    if roleHeadData[vocation] == nil then
        local data = XMLManager.role_data[vocation]
        roleHeadData[vocation] = {}
        for k,v in pairs(DataParseHelper.ParseNSuffixSort(data.__head_choose)) do
            table.insert(roleHeadData[id], {v[1],v[2]})
        end
    end
    return roleHeadData[vocation]
end

function RoleDataHelper.GetRoleHairColorByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return DataParseHelper.ParseNSuffix(data.__hair_color)
    end
    return {}
end

function RoleDataHelper.GetRoleEquipColorByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return DataParseHelper.ParseNSuffix(data.__equip_color)
    end
    return {}
end

function RoleDataHelper.GetRoleDefaultClothByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__default_cloth
    end
    return 0
end

function RoleDataHelper.GetRoleDefaultWeaponByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__default_weapon
    end
    return 0
end

function RoleDataHelper.GetRoleDefaultHeadByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__default_head
    end
    return 0
end

--是否可以选择状态
function RoleDataHelper.GetRoleStateByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__open_state
    end
    return 0
end

function RoleDataHelper.GetVocGroup(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return data.__voc_group
    end
    return 0
end

function RoleDataHelper.GetSpellLearned(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return DataParseHelper.ParseListInt(data.__spell_learned)
    end
end

function RoleDataHelper.GetCreateRoleEquipByVocation(vocation)
    local data = XMLManager.role_data[vocation]
    if (data~=nil) then 
        return DataParseHelper.ParseNSuffix(data.__create_equips)
    end
    return {}
end

----------------------------------

function RoleDataHelper.GetRoleDataById(value)
    local data = XMLManager.role_data[value]
    return data
end

function RoleDataHelper.GetAllVocationList()
    local data
    local vocationListData = {}
    local keys = XMLManager.role_data:Keys()
    for i=1,#keys do 
        data = RoleDataHelper.GetRoleDataById(keys[i])
        if data.__id % 100 == 0 then
            table.insert(vocationListData, data.__id)
        end
    end
    return vocationListData
end

function RoleDataHelper.GetVocationData()
    local data
    local VocationListData = {}
    local keys = XMLManager.role_data:Keys()
    for i=1,#keys do 
        data = RoleDataHelper.GetRoleDataById(keys[i])
        VocationListData[data.__id] = {nameid = data.__role_desc_id,iconid = data.__role_icon_id}
    end
    return VocationListData
end


--获取某个等级升级所需的经验
function RoleDataHelper.GetCurLevelExp(level)
    local data = XMLManager.role_experience[level]
    if data then
        return data.__nextLevelExp
    end
    return 0
end

function RoleDataHelper.GetCurLevelMaxActivity(level)
    local data = XMLManager.role_experience[level]
    if data then
        return data.__max_activity
    end
    return 0
end


return RoleDataHelper

