-- OfflinePvpReward2DataHelper.lua
local OfflinePvpReward2DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OfflinePvpReward2 = Class.OfflinePvpReward2(ClassTypes.XObject)

local offlinePvpReward2Cache = {}
function OfflinePvpReward2DataHelper.ClearCache()
    offlinePvpReward2Cache = {}
end

function OfflinePvpReward2:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.reward_win = DataParseHelper.ParseMSuffix(cfg.__reward_win)
    self.reward_lose = DataParseHelper.ParseMSuffix(cfg.__reward_lose)
end

function OfflinePvpReward2DataHelper.GetOfflinePvpReward2(id)
    ClearDataManager:UseCache(CacheNames.offline_pvp_reward_2, OfflinePvpReward2DataHelper.ClearCache)
    local data = offlinePvpReward2Cache[id]
    if data == nil then
        local cfg = XMLManager.offline_pvp_reward_2[id]
        if cfg ~= nil then
            data = OfflinePvpReward2(cfg)
            offlinePvpReward2Cache[data.id] = data
        else
            LoggerHelper.Error("OfflinePvpReward2 id not exist:" .. id)
        end
    end
    return data
end

function OfflinePvpReward2DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.offline_pvp_reward_2:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OfflinePvpReward2DataHelper:GetLevel(id)
    local data = self.GetOfflinePvpReward2(id)
    return data and data.level or nil
end

function OfflinePvpReward2DataHelper:GetRewardWin(id)
    local data = self.GetOfflinePvpReward2(id)
    return data and data.reward_win or nil
end

function OfflinePvpReward2DataHelper:GetRewardLose(id)
    local data = self.GetOfflinePvpReward2(id)
    return data and data.reward_lose or nil
end

return OfflinePvpReward2DataHelper
