-- WelfareLevelUpDataHelper.lua
local WelfareLevelUpDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareLevelUp = Class.WelfareLevelUp(ClassTypes.XObject)

local welfareLevelUpCache = {}
function WelfareLevelUpDataHelper.ClearCache()
    welfareLevelUpCache = {}
end

function WelfareLevelUp:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
    self.get_count = cfg.__get_count
end

function WelfareLevelUpDataHelper.GetWelfareLevelUp(id)
    ClearDataManager:UseCache(CacheNames.welfare_level_up, WelfareLevelUpDataHelper.ClearCache)
    local data = welfareLevelUpCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_level_up[id]
        if cfg ~= nil then
            data = WelfareLevelUp(cfg)
            welfareLevelUpCache[data.id] = data
        else
            LoggerHelper.Error("WelfareLevelUp id not exist:" .. id)
        end
    end
    return data
end

function WelfareLevelUpDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_level_up:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareLevelUpDataHelper:GetLevel(id)
    local data = self.GetWelfareLevelUp(id)
    return data and data.level or nil
end

function WelfareLevelUpDataHelper:GetReward(id)
    local data = self.GetWelfareLevelUp(id)
    return data and data.reward or nil
end

function WelfareLevelUpDataHelper:GetGetCount(id)
    local data = self.GetWelfareLevelUp(id)
    return data and data.get_count or nil
end

return WelfareLevelUpDataHelper
