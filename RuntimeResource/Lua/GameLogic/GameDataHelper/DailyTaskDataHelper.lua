-- DailyTaskDataHelper.lua
local DailyTaskDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local DailyTask = Class.DailyTask(ClassTypes.XObject)

local dailyTaskCache = {}
function DailyTaskDataHelper.ClearCache()
    dailyTaskCache = {}
end

function DailyTask:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.desc = cfg.__desc
    self.icon = cfg.__icon
    self.group = cfg.__group
    self.level = cfg.__level
    self.world_level = cfg.__world_level
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.active_point = cfg.__active_point
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function DailyTaskDataHelper.GetDailyTask(id)
    ClearDataManager:UseCache(CacheNames.daily_task, DailyTaskDataHelper.ClearCache)
    local data = dailyTaskCache[id]
    if data == nil then
        local cfg = XMLManager.daily_task[id]
        if cfg ~= nil then
			data = DailyTask(cfg)
			dailyTaskCache[data.id] = data
        else
            LoggerHelper.Error("DailyTask id not exist:" .. id)
        end
    end
    return data
end

function DailyTaskDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.daily_task:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function DailyTaskDataHelper:GetName(id)
    local data = self.GetDailyTask(id)
    return data and data.name or nil
end

function DailyTaskDataHelper:GetDesc(id)
    local data = self.GetDailyTask(id)
    return data and data.desc or nil
end

function DailyTaskDataHelper:GetIcon(id)
    local data = self.GetDailyTask(id)
    return data and data.icon or nil
end

function DailyTaskDataHelper:GetGroup(id)
    local data = self.GetDailyTask(id)
    return data and data.group or nil
end

function DailyTaskDataHelper:GetLevel(id)
    local data = self.GetDailyTask(id)
    return data and data.level or nil
end

function DailyTaskDataHelper:GetWorldLevel(id)
    local data = self.GetDailyTask(id)
    return data and data.world_level or nil
end

function DailyTaskDataHelper:GetTargets(id)
    local data = self.GetDailyTask(id)
    return data and data.targets or nil
end

function DailyTaskDataHelper:GetActivePoint(id)
    local data = self.GetDailyTask(id)
    return data and data.active_point or nil
end

function DailyTaskDataHelper:GetReward(id)
    local data = self.GetDailyTask(id)
    return data and data.reward or nil
end

return DailyTaskDataHelper
