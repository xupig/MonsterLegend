-- ChineseVoiceDataHelper.lua
local ChineseVoiceDataHelper = {}

local XMLManager = GameManager.XMLManager

local ChineseVoice = Class.ChineseVoice(ClassTypes.XObject)
local ClearDataManager = PlayerManager.ClearDataManager

local chineseVoiceCache = {}
function ChineseVoiceDataHelper.ClearCache()
    chineseVoiceCache = {}
end

function ChineseVoice:__ctor__(cfg)
    self.id = cfg.__id
    self.voice = cfg.__voice
end

function ChineseVoiceDataHelper.GetChineseVoice(id)
    ClearDataManager:UseCache(CacheNames.chinese_voice, ChineseVoiceDataHelper.ClearCache)
    local data = chineseVoiceCache[id]
    if data == nil then
        local cfg = XMLManager.chinese_voice[id]
        if cfg ~= nil then
            data = ChineseVoice(cfg)
            chineseVoiceCache[data.id] = data
        end
    end
    return data
end

function ChineseVoiceDataHelper:GetVoice(id)
    local data = self.GetChineseVoice(id)
    return data and data.voice or 0
end

return ChineseVoiceDataHelper
