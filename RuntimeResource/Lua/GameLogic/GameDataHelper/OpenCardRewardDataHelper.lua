-- OpenCardRewardDataHelper.lua
local OpenCardRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OpenCardReward = Class.OpenCardReward(ClassTypes.XObject)

local openCardRewardCache = {}
function OpenCardRewardDataHelper.ClearCache()
    openCardRewardCache = {}
end

function OpenCardReward:__ctor__(cfg)
    self.id = cfg.__id
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
end

function OpenCardRewardDataHelper.GetOpenCardReward(id)
    ClearDataManager:UseCache(CacheNames.open_card_reward, OpenCardRewardDataHelper.ClearCache)
    local data = openCardRewardCache[id]
    if data == nil then
        local cfg = XMLManager.open_card_reward[id]
        if cfg ~= nil then
            data = OpenCardReward(cfg)
            openCardRewardCache[data.id] = data
        else
            LoggerHelper.Error("OpenCardReward id not exist:" .. id)
        end
    end
    return data
end

function OpenCardRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.open_card_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OpenCardRewardDataHelper:GetReward(id)
    local data = self.GetOpenCardReward(id)
    return data and data.reward or nil
end

return OpenCardRewardDataHelper
