-- MarryChildbaseDataHelper.lua
local MarryChildbaseDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local MarryChildbase = Class.MarryChildbase(ClassTypes.XObject)

local marryChildbaseCache = {}
function MarryChildbaseDataHelper.ClearCache()
    marryChildbaseCache = {}
end

function MarryChildbase:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.mode = cfg.__mode
    self.passive_skill = DataParseHelper.ParseMSuffix(cfg.__passive_skill)
    self.ai_id = cfg.__ai_id
    self.skill_ids = DataParseHelper.ParseListInt(cfg.__skill_ids)
    self.speed = cfg.__speed
    self.unlock = DataParseHelper.ParseNSuffix(cfg.__unlock)
    self.unlock_chinese = cfg.__unlock_chinese
    self.scale = cfg.__scale
end

function MarryChildbaseDataHelper.GetMarryChildbase(id)
    ClearDataManager:UseCache(CacheNames.marry_childbase, MarryChildbaseDataHelper.ClearCache)
    local data = marryChildbaseCache[id]
    if data == nil then
        local cfg = XMLManager.marry_childbase[id]
        if cfg ~= nil then
            data = MarryChildbase(cfg)
            marryChildbaseCache[data.id] = data
        else
            LoggerHelper.Error("MarryChildbase id not exist:" .. id)
        end
    end
    return data
end

function MarryChildbaseDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.marry_childbase:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MarryChildbaseDataHelper:GetType(id)
    local data = self.GetMarryChildbase(id)
    return data and data.type or nil
end

function MarryChildbaseDataHelper:GetName(id)
    local data = self.GetMarryChildbase(id)
    return data and data.name or nil
end

function MarryChildbaseDataHelper:GetIcon(id)
    local data = self.GetMarryChildbase(id)
    return data and data.icon or nil
end

function MarryChildbaseDataHelper:GetMode(id)
    local data = self.GetMarryChildbase(id)
    return data and data.mode or nil
end

function MarryChildbaseDataHelper:GetPassiveSkill(id)
    local data = self.GetMarryChildbase(id)
    return data and data.passive_skill or nil
end

function MarryChildbaseDataHelper:GetAiId(id)
    local data = self.GetMarryChildbase(id)
    return data and data.ai_id or nil
end

function MarryChildbaseDataHelper:GetSkillIds(id)
    local data = self.GetMarryChildbase(id)
    return data and data.skill_ids or nil
end

function MarryChildbaseDataHelper:GetSpeed(id)
    local data = self.GetMarryChildbase(id)
    return data and data.speed or nil
end

function MarryChildbaseDataHelper:GetUnlock(id)
    local data = self.GetMarryChildbase(id)
    return data and data.unlock or nil
end

function MarryChildbaseDataHelper:GetUnlockChinese(id)
    local data = self.GetMarryChildbase(id)
    return data and data.unlock_chinese or nil
end

function MarryChildbaseDataHelper:GetScale(id)
    local data = self.GetMarryChildbase(id)
    return data and data.scale or nil
end

function MarryChildbaseDataHelper:GetTypeDataCfg()
    if self._childcfg==nil then
        local Keys = XMLManager.marry_childbase:Keys()
        self._childcfg = {}
        local types={}
        local x=1
        for i=1,#Keys-1 do
            local t = MarryChildbaseDataHelper:GetType(i)
            if types[t]==nil then 
                types[t]=x  
                self._childcfg[x] = {}
                x = x +1
            end
            self._childcfg[types[t]][1]=t
            table.insert(self._childcfg[types[t]],self.GetMarryChildbase(i))
        end
    end
    return self._childcfg
end

return MarryChildbaseDataHelper
