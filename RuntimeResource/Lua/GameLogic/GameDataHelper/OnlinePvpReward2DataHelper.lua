-- OnlinePvpReward2DataHelper.lua
local OnlinePvpReward2DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvpReward2 = Class.OnlinePvpReward2(ClassTypes.XObject)

local onlinePvpReward2Cache = {}
function OnlinePvpReward2DataHelper.ClearCache()
    onlinePvpReward2Cache = {}
end

function OnlinePvpReward2:__ctor__(cfg)
    self.id = cfg.__id
    self.desc = cfg.__desc
    self.exploit = cfg.__exploit
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function OnlinePvpReward2DataHelper.GetOnlinePvpReward2(id)
    ClearDataManager:UseCache(CacheNames.online_pvp_reward_2, OnlinePvpReward2DataHelper.ClearCache)
    local data = onlinePvpReward2Cache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp_reward_2[id]
        if cfg ~= nil then
            data = OnlinePvpReward2(cfg)
            onlinePvpReward2Cache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvpReward2 id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpReward2DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp_reward_2:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpReward2DataHelper:GetDesc(id)
    local data = self.GetOnlinePvpReward2(id)
    return data and data.desc or nil
end

function OnlinePvpReward2DataHelper:GetExploit(id)
    local data = self.GetOnlinePvpReward2(id)
    return data and data.exploit or nil
end

function OnlinePvpReward2DataHelper:GetReward(id)
    local data = self.GetOnlinePvpReward2(id)
    return data and data.reward or nil
end

return OnlinePvpReward2DataHelper
