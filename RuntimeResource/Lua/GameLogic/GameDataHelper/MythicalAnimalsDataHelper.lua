-- MythicalAnimalsDataHelper.lua
local MythicalAnimalsDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local MythicalAnimals = Class.MythicalAnimals(ClassTypes.XObject)

local mythicalAnimalsCache = {}
function MythicalAnimalsDataHelper.ClearCache()
    mythicalAnimalsCache = {}
end

function MythicalAnimals:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.model = cfg.__model
    self.icon = cfg.__icon
    self.icon_head = cfg.__icon_head
    self.quality_type = DataParseHelper.ParseMSuffix(cfg.__quality_type)
    self.buff_id = DataParseHelper.ParseMSuffixSort(cfg.__buff_id)
    self.attri = DataParseHelper.ParseMSuffix(cfg.__attri)
    self.mark = cfg.__mark
    self.pos_x = cfg.__pos_x
    self.pos_y = cfg.__pos_y
    self.scale_x = cfg.__scale_x
    self.scale_y = cfg.__scale_y
end

function MythicalAnimalsDataHelper.GetMythicalAnimals(id)
    ClearDataManager:UseCache(CacheNames.mythical_animals, MythicalAnimalsDataHelper.ClearCache)
    local data = mythicalAnimalsCache[id]
    if data == nil then
        local cfg = XMLManager.mythical_animals[id]
        if cfg ~= nil then
            data = MythicalAnimals(cfg)
            mythicalAnimalsCache[data.id] = data
        else
            LoggerHelper.Error("MythicalAnimals id not exist:" .. id)
        end
    end
    return data
end

function MythicalAnimalsDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.mythical_animals:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MythicalAnimalsDataHelper:GetName(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.name or nil
end

function MythicalAnimalsDataHelper:GetIcon(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.icon or nil
end

function MythicalAnimalsDataHelper:GetMiddleIcon(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.icon_head or nil
end

function MythicalAnimalsDataHelper:GetModel(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.model or nil
end

function MythicalAnimalsDataHelper:GetQualityType(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.quality_type or nil
end

function MythicalAnimalsDataHelper:GetBuffId(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.buff_id or nil
end

function MythicalAnimalsDataHelper:GetAttri(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.attri or nil
end

function MythicalAnimalsDataHelper:GetMark(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.mark or nil
end

function MythicalAnimalsDataHelper:GetPosX(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.pos_x or nil
end

function MythicalAnimalsDataHelper:GetPosY(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.pos_y or nil
end

function MythicalAnimalsDataHelper:GetScaleX(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.scale_x or nil
end

function MythicalAnimalsDataHelper:GetScaleY(id)
    local data = self.GetMythicalAnimals(id)
    return data and data.scale_y or nil
end
return MythicalAnimalsDataHelper
