-- WelfareNoticeRewardDataHelper.lua
local WelfareNoticeRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareNoticeReward = Class.WelfareNoticeReward(ClassTypes.XObject)

local welfareNoticeRewardCache = {}
function WelfareNoticeRewardDataHelper.ClearCache()
    welfareNoticeRewardCache = {}
end

function WelfareNoticeReward:__ctor__(cfg)
    self.id = cfg.__id
    self.notice = cfg.__notice
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.bind_flag = cfg.__bind_flag
    self.use = cfg.__use
end

function WelfareNoticeRewardDataHelper.GetWelfareNoticeReward(id)
    ClearDataManager:UseCache(CacheNames.welfare_notice_reward, WelfareNoticeRewardDataHelper.ClearCache)
    local data = welfareNoticeRewardCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_notice_reward[id]
        if cfg ~= nil then
            data = WelfareNoticeReward(cfg)
            welfareNoticeRewardCache[data.id] = data
        else
            LoggerHelper.Error("WelfareNoticeReward id not exist:" .. id)
        end
    end
    return data
end

function WelfareNoticeRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_notice_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareNoticeRewardDataHelper:GetNotice(id)
    local data = self.GetWelfareNoticeReward(id)
    return data and data.notice or nil
end

function WelfareNoticeRewardDataHelper:GetReward(id)
    local data = self.GetWelfareNoticeReward(id)
    return data and data.reward or nil
end

function WelfareNoticeRewardDataHelper:GetBindFlag(id)
    local data = self.GetWelfareNoticeReward(id)
    return data and data.bind_flag or nil
end

function WelfareNoticeRewardDataHelper:GetUse(id)
    local data = self.GetWelfareNoticeReward(id)
    return data and data.use or nil
end

return WelfareNoticeRewardDataHelper
