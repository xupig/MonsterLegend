-- DataParseHelper.lua
local DataParseHelper = {}

function DataParseHelper.ParseListInt(source)
    local result = nil
    if source ~= nil and source ~= "" then
        result = {}
        local splits = string.split(source, ",")
        for i = 1, #splits do
            local sub = splits[i]
            result[i] = tonumber(sub) or tostring(sub)--优先tonumber，解析失败则tostring
        end
    end
    return result
end

-- 支持数字和字符串参数
-- 1:2,3;4:"name",6
-- return {
--     [1] = {2,3},
--     [4] = {"name",6},
-- }
function DataParseHelper.ParseNSuffix(source, keyIsString)
    keyIsString = keyIsString or false
    local result = nil
    if source ~= nil and source ~= "" then
        result = {}
        local bs = string.split(source, ";")
        for i = 1, #bs do
            if bs[i] ~= nil and bs[i] ~= "" then
                local b = string.split(bs[i], ":")
                local t = string.split(b[2], ",")
                for j = 1, #t do
                    t[j] = tonumber(t[j]) or tostring(t[j])  --优先tonumber，解析失败则tostring
                end
                local id = (keyIsString and tostring(b[1])) or tonumber(b[1])
                result[id] = t
            else
                assert(false, "ParseNSuffix error, data: " .. source)
            end
        end
    end
    return result
end

-- 1:2,3;4:5,6
-- return {
--     {1, {2,3}},
--     {4, {5,6}},
-- }
function DataParseHelper.ParseNSuffixSort(source, keyIsString)
    keyIsString = keyIsString or false
    local result = nil
    if source ~= nil and source ~= "" then
        result = {}
        local bs = string.split(source, ";")
        for i = 1, #bs do
            if bs[i] ~= nil and bs[i] ~= "" then
                local b = string.split(bs[i], ":")
                local t = string.split(b[2], ",")
                for j = 1, #t do
                    t[j] = tonumber(t[j]) or tostring(t[j])  --优先tonumber，解析失败则tostring
                end
                local id = (keyIsString and tostring(b[1])) or tonumber(b[1])
                result[i] = {[1] = id, [2] = t}
            else
                assert(false, "ParseNSuffixSort error, data: " .. source)
            end
        end
    end
    return result
end

-- 1:2;3:4
-- return {
--     [1] = 2,
--     [3] = 4,
-- }
function DataParseHelper.ParseMSuffix(source, valueIsString)
    local result = nil
    if source ~= nil and source ~= "" then
        result = {}
        local bs = string.split(source, ";")
        for i = 1, #bs do
            if bs[i] ~= nil and bs[i] ~= "" then
                local b = string.split(bs[i], ":")
                result[tonumber(b[1])] = (valueIsString and tostring(b[2])) or tonumber(b[2])
            else
                assert(false, "ParseMSuffix error, data: " .. source)
            end
        end
    end
    return result
end

-- 1:2;3:4
-- return {
--     {1,2},
--     {3,4},
-- }
function DataParseHelper.ParseMSuffixSort(source, valueIsString)
    local result = nil
    if source ~= nil and source ~= "" then
        result = {}
        local bs = string.split(source, ";")
        for i = 1, #bs do
            if bs[i] ~= nil and bs[i] ~= "" then
                local b = string.split(bs[i], ":")
                result[i] = {[1] = tonumber(b[1]), [2] = (valueIsString and tostring(b[2])) or tonumber(b[2])}
            else
                assert(false, "ParseMSuffixSort error, data: " .. source)
            end
        end
    end
    return result
end

function DataParseHelper.ParseYSuffix(value)
    local result = nil
    if value ~= nil and value ~= "" then
        --2014-01-25 0:12:00
        local i = string.find(value,' ',1)
        local str_data = string.sub(value,1,i-1)
        local str_time = string.sub(value,i+1)
        local dd = string.split_str(str_data,'-',tonumber)
        local tt = string.split_str(str_time,':',tonumber)
        result = os.time{year=dd[1],month=dd[2],day=dd[3],hour=tt[1],min=tt[2],sec=tt[3]}
    end
    return result
end

return DataParseHelper
