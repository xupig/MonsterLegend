-- FunctionTimingDataHelper.lua
local FunctionTimingDataHelper = {}

local functionTimingCache = {}

local FunctionTiming = Class.FunctionTiming(ClassTypes.XObject)
function FunctionTiming:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.icon = cfg.__icon
    self.line = cfg.__line
end

function FunctionTimingDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.function_timing:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function FunctionTimingDataHelper.GetFunctionTiming(id)
    local data = functionTimingCache[id]
    if data == nil then
        local cfg = XMLManager.function_timing[id]
        if cfg ~= nil then
            data = FunctionTiming(cfg)
            functionTimingCache[data.id] = data
        else
            --LoggerHelper.Error("FunctionTiming id not exist:" .. id)
        end
    end
    return data
end

return FunctionTimingDataHelper