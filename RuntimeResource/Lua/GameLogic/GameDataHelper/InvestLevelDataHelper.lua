-- InvestLevelDataHelper.lua
local InvestLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local InvestLevel = Class.InvestLevel(ClassTypes.XObject)

local investLevelCache = {}
function InvestLevelDataHelper.ClearCache()
    investLevelCache = {}
end

function InvestLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.level = cfg.__level
    self.reward = DataParseHelper.ParseMSuffix(cfg.__reward)
end

function InvestLevelDataHelper.GetInvestLevel(id)
    ClearDataManager:UseCache(CacheNames.invest_level, InvestLevelDataHelper.ClearCache)
    local data = investLevelCache[id]
    if data == nil then
        local cfg = XMLManager.invest_level[id]
        if cfg ~= nil then
            data = InvestLevel(cfg)
            investLevelCache[data.id] = data
        else
            LoggerHelper.Error("InvestLevel id not exist:" .. id)
        end
    end
    return data
end

function InvestLevelDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.invest_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function InvestLevelDataHelper:GetType(id)
    local data = self.GetInvestLevel(id)
    return data and data.type or nil
end

function InvestLevelDataHelper:GetLevel(id)
    local data = self.GetInvestLevel(id)
    return data and data.level or nil
end

function InvestLevelDataHelper:GetReward(id)
    local data = self.GetInvestLevel(id)
    return data and data.reward or nil
end

function InvestLevelDataHelper:GetCfgByType(t)
    if self._cfg and self._cfg[t] then
        return self._cfg[t]
    end
    self:HandleCfg()
    return self._cfg[t]
end

function InvestLevelDataHelper:HandleCfg()
    if self._cfg == nil then 
        self._cfg = {}
        self._types = {}
    end
    local data = XMLManager.invest_level:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local cfg = self.GetInvestLevel(v)
            if self._cfg[cfg.type]==nil then
                self._cfg[cfg.type] = {}
                table.insert(self._types,cfg.type)
            end
            self._cfg[cfg.type][cfg.level]=cfg
        end
    end
end
function InvestLevelDataHelper:GetAllType()
    if self._types==nil then
        self:HandleCfg()
    end
    return self._types
end
return InvestLevelDataHelper
