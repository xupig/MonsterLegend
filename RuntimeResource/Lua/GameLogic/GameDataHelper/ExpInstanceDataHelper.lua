-- ExpInstanceDataHelper.lua
--经验副本配置
local ExpInstanceDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local ExpInstanceCfg = Class.ExpInstanceCfg(ClassTypes.XObject)
local expInstanceCfgCache =  {}
function ExpInstanceDataHelper.ClearCache()
    expInstanceCfgCache = {}
end


function ExpInstanceCfg:__ctor__(cfg)
    self.id = cfg.__id
	self.map_id = cfg.__map_id
	self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
	self.enter_limit = cfg.__enter_limit
	self.prepare_time = cfg.__prepare_time
	self.last_time = cfg.__last_time
	self.exp_inst_item =  DataParseHelper.ParseNSuffix(cfg.__exp_inst_item)
end

function ExpInstanceDataHelper:GetExpInstanceCfg(id)
    ClearDataManager:UseCache(CacheNames.exp_instance, ExpInstanceDataHelper.ClearCache)
	local data = expInstanceCfgCache[id]
    if data == nil then
        local cfg = XMLManager.exp_instance[id]
        data = ExpInstanceCfg(cfg)
        expInstanceCfgCache[data.id] = data
    end
    return data
end
return ExpInstanceDataHelper