-- AddRateDataHelper.lua
local AddRateDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local addRateCfgCache = {}
function AddRateDataHelper.ClearCache()
    addRateCfgCache = {}
end

local AddRateCfg = Class.AddRateCfg(ClassTypes.XObject)
function AddRateCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.effect = cfg.__effect
    self.effect_value = cfg.__effect_value
    self.effect_time = cfg.__effect_time
end

function AddRateDataHelper.GetAddRateCfg(id)
    ClearDataManager:UseCache(CacheNames.add_rate, AddRateDataHelper.ClearCache)
	local data = addRateCfgCache[id]
    if data == nil then
        data = XMLManager.add_rate[id]
        if data ~= nil then data = AddRateCfg(data) end
    end

    if data ~= nil then
        addRateCfgCache[id] = data
    end
    return data
end

function AddRateDataHelper:GetEffect(id)
    local data = self.GetAddRateCfg(id)
    return data and data.effect or nil
end

function AddRateDataHelper:GetEffectValue(id)
    local data = self.GetAddRateCfg(id)
    return data and data.effect_value or nil
end

function AddRateDataHelper:GetEffectTime(id)
    local data = self.GetAddRateCfg(id)
    return data and data.effect_time or nil
end

return AddRateDataHelper