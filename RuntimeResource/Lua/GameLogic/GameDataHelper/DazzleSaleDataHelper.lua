-- DazzleSaleDataHelper.lua
local DazzleSaleDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local DazzleSale = Class.DazzleSale(ClassTypes.XObject)

local dazzleSaleCache = {}

function DazzleSale:__ctor__(cfg)
    self.id = cfg.__id
    self.price = cfg.__price
    self.price_first = cfg.__price_first
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
    self.display = DataParseHelper.ParseNSuffix(cfg.__display)
end

function DazzleSaleDataHelper.GetDazzleSale(id)
    local data = dazzleSaleCache[id]
    if data == nil then
        local cfg = XMLManager.dazzle_sale[id]
        if cfg ~= nil then
            data = DazzleSale(cfg)
            dazzleSaleCache[data.id] = data
        else
            LoggerHelper.Error("DazzleSale id not exist:" .. id)
        end
    end
    return data
end

function DazzleSaleDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.dazzle_sale:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function DazzleSaleDataHelper:GetPrice(id)
    local data = self.GetDazzleSale(id)
    return data and data.price or nil
end

function DazzleSaleDataHelper:GetPriceFirst(id)
    local data = self.GetDazzleSale(id)
    return data and data.price_first or nil
end

function DazzleSaleDataHelper:GetReward(id)
    local data = self.GetDazzleSale(id)
    return data and data.reward or nil
end

function DazzleSaleDataHelper:GetDisplay(id)
    local data = self.GetDazzleSale(id)
    return data and data.display or nil
end

return DazzleSaleDataHelper
