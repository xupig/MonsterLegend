-- WelfareDayRewardDataHelper.lua
local WelfareDayRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WelfareDayReward = Class.WelfareDayReward(ClassTypes.XObject)

local welfareDayRewardCache = {}
function WelfareDayRewardDataHelper.ClearCache()
    welfareDayRewardCache = {}
end

function WelfareDayReward:__ctor__(cfg)
    self.id = cfg.__id
    self.day = cfg.__day
    self.lv = DataParseHelper.ParseListInt(cfg.__lv)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.bind_flag = cfg.__bind_flag
end

function WelfareDayRewardDataHelper.GetWelfareDayReward(id)
    ClearDataManager:UseCache(CacheNames.welfare_day_reward, WelfareDayRewardDataHelper.ClearCache)
    local data = welfareDayRewardCache[id]
    if data == nil then
        local cfg = XMLManager.welfare_day_reward[id]
        if cfg ~= nil then
            data = WelfareDayReward(cfg)
            welfareDayRewardCache[data.id] = data
        else
            LoggerHelper.Error("WelfareDayReward id not exist:" .. id)
        end
    end
    return data
end

function WelfareDayRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.welfare_day_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function WelfareDayRewardDataHelper:GetDay(id)
    local data = self.GetWelfareDayReward(id)
    return data and data.day or nil
end

function WelfareDayRewardDataHelper:GetMaxDay()
    local data = XMLManager.welfare_day_reward:Keys()
    local max = 0
    for i, v in ipairs(data) do
        if v ~= 0 then
            local day = self:GetDay(v)
            if day > max then
                max = day
            end
        end
    end
    return max
end

function WelfareDayRewardDataHelper:GetLv(id)
    local data = self.GetWelfareDayReward(id)
    return data and data.lv or nil
end

function WelfareDayRewardDataHelper:GetReward(id)
    local data = self.GetWelfareDayReward(id)
    return data and data.reward or nil
end

function WelfareDayRewardDataHelper:GetBindFlag(id)
    local data = self.GetWelfareDayReward(id)
    return data and data.bind_flag or nil
end

return WelfareDayRewardDataHelper
