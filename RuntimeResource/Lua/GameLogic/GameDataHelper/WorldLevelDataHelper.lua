-- WorldLevelDataHelper.lua
local WorldLevelDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local WorldLevel = Class.WorldLevel(ClassTypes.XObject)

local worldLevelCache = {}
function WorldLevelDataHelper.ClearCache()
    worldLevelCache = {}
end

function WorldLevel:__ctor__(cfg)
    self.id = cfg.__id
    self.world_level = DataParseHelper.ParseListInt(cfg.__world_level)
    self.deviation_level = DataParseHelper.ParseListInt(cfg.__deviation_level)
    self.correction_percent = cfg.__correction_percent

end

function WorldLevelDataHelper.GetWorldLevelCfg(id)
    ClearDataManager:UseCache(CacheNames.world_level, WorldLevelDataHelper.ClearCache)
    local data = worldLevelCache[id]
    if data == nil then
        local cfg = XMLManager.world_level[id]
        if cfg ~= nil then
            data = WorldLevel(cfg)
            worldLevelCache[data.id] = data
        end
    end
    return data
end


function WorldLevelDataHelper.GetWorldLevelData(worldLevel, deviationLevel)
    local keys = XMLManager.world_level:Keys()
    for i=1,#keys do
        local data = WorldLevelDataHelper.GetWorldLevelCfg(keys[i])
        if worldLevel >= data.world_level[1] and worldLevel <= data.world_level[2] and
            deviationLevel >= data.deviation_level[1] and deviationLevel <= data.deviation_level[2] then
            return data
        end
    end
end

function WorldLevelDataHelper.GetWorldLevelCorrection(worldLevel, deviationLevel)
    local data = WorldLevelDataHelper.GetWorldLevelData(worldLevel, deviationLevel) or {}
    return data.correction_percent or 0
end


return WorldLevelDataHelper
