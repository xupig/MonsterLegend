-- OnlinePvpReward3DataHelper.lua
local OnlinePvpReward3DataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local OnlinePvpReward3 = Class.OnlinePvpReward3(ClassTypes.XObject)

local onlinePvpReward3Cache = {}
function OnlinePvpReward3DataHelper.ClearCache()
    onlinePvpReward3Cache = {}
end

function OnlinePvpReward3:__ctor__(cfg)
    self.id = cfg.__id
    self.level = cfg.__level
    self.time = cfg.__time
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
end

function OnlinePvpReward3DataHelper.GetOnlinePvpReward3(id)
    ClearDataManager:UseCache(CacheNames.online_pvp_reward_3, OnlinePvpReward3DataHelper.ClearCache)
    local data = onlinePvpReward3Cache[id]
    if data == nil then
        local cfg = XMLManager.online_pvp_reward_3[id]
        if cfg ~= nil then
            data = OnlinePvpReward3(cfg)
            onlinePvpReward3Cache[data.id] = data
        else
            LoggerHelper.Error("OnlinePvpReward3 id not exist:" .. id)
        end
    end
    return data
end

function OnlinePvpReward3DataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.online_pvp_reward_3:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function OnlinePvpReward3DataHelper:GetIdByLevelAndTime(level, time)
    local data = XMLManager.online_pvp_reward_3:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetLevel(v) == level and self:GetTime(v) == time then
            return v
        end
    end
    return 0
end

function OnlinePvpReward3DataHelper:GetLevel(id)
    local data = self.GetOnlinePvpReward3(id)
    return data and data.level or nil
end

function OnlinePvpReward3DataHelper:GetTime(id)
    local data = self.GetOnlinePvpReward3(id)
    return data and data.time or nil
end

function OnlinePvpReward3DataHelper:GetReward(id)
    local data = self.GetOnlinePvpReward3(id)
    return data and data.reward or nil
end

return OnlinePvpReward3DataHelper
