-- BossHomeDataHelper.lua
local BossHomeDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local BossHome = Class.BossHome(ClassTypes.XObject)

function BossHome:__ctor__(cfg)
    self.id = cfg.__id
    self.floor = cfg.__floor
    self.vip_level_limit = cfg.__vip_level_limit
    self.vip_level_money = cfg.__vip_level_money
    self.sence_id = cfg.__sence_id
    self.level_limit = cfg.__level_limit
    self.boss_level = cfg.__boss_level
    self.boss_name = cfg.__boss_name
    self.boss_name_icon = cfg.__boss_name_icon
    self.boss_icon = cfg.__boss_icon
    self.boss_monster_id = cfg.__boss_monster_id
    self.boss_drops = DataParseHelper.ParseNSuffix(cfg.__boss_drops)
    self.boss_region_id = cfg.__boss_region_id
    self.level_show = cfg.__level_show
end

local bossHomeCache = {}
function BossHomeDataHelper.ClearCache()
    bossHomeCache = {}
end

function BossHomeDataHelper:GetBossData(id)
    ClearDataManager:UseCache(CacheNames.boss_home, BossHomeDataHelper.ClearCache)
    local data = bossHomeCache[id]
    if data == nil then
        local cfg = XMLManager.boss_home[id]
        if cfg ~= nil then
            data = BossHome(cfg)
            bossHomeCache[data.id] = data
        else
            LoggerHelper.Error("BossHome id not exist:" .. id)
        end
    end
    return data
end

function BossHomeDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.boss_home:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function BossHomeDataHelper:GetMonsterByMapId(mapId)
    local data = XMLManager.boss_home:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 and self:GetSenceId(v) == mapId then
            return v
        end
    end
    return nil
end

local floorIdsData = {}
function BossHomeDataHelper:GetIdsByFloor(floor)
    if floorIdsData[floor] == nil then
        floorIdsData[floor] = {}
    end
    local keys = XMLManager.boss_home:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = self:GetBossData(id)
            if data.floor == floor then
                local tmp = {}
                tmp.id = id
                tmp.order = data.boss_level
                table.insert(floorIdsData[floor],tmp)
            end
            table.sort(floorIdsData[floor], SortByOrder)
        end
    end

    return floorIdsData[floor]
end

function BossHomeDataHelper:GetSenceId(id)
    local data = self:GetBossData(id)
    return data and data.sence_id or nil
end

function BossHomeDataHelper:GetLevelLimit(id)
    local data = self:GetBossData(id)
    return data and data.level_limit or nil
end

function BossHomeDataHelper:GetBossLevel(id)
    local data = self:GetBossData(id)
    return data and data.boss_level or nil
end

function BossHomeDataHelper:GetBossName(id)
    local data = self:GetBossData(id)
    return data and data.boss_name or nil
end

function BossHomeDataHelper:GetBossNameIcon(id)
    local data = self:GetBossData(id)
    return data and data.boss_name_icon or nil
end

function BossHomeDataHelper:GetBossIcon(id)
    local data = self:GetBossData(id)
    return data and data.boss_icon or nil
end

function BossHomeDataHelper:GetBossMonsterId(id)
    local data = self:GetBossData(id)
    return data and data.boss_monster_id or nil
end

function BossHomeDataHelper:GetBossDrops(id)
    local data = self:GetBossData(id)
    return data and data.boss_drops or nil
end

function BossHomeDataHelper:GetBossRegionId(id)
    local data = self:GetBossData(id)
    return data and data.boss_region_id or nil
end

function BossHomeDataHelper:GetVIPLevelLimit(id)
    local data = self:GetBossData(id)
    return data and data.vip_level_limit or nil
end

function BossHomeDataHelper:GetVIPLevelMoney(id)
    local data = self:GetBossData(id)
    return data and data.vip_level_money or nil
end

function BossHomeDataHelper:GetLevelShow(id)
    local data = self:GetBossData(id)
    return data and data.level_show or 0
end

return BossHomeDataHelper
