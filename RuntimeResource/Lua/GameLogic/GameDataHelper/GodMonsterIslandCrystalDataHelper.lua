-- GodMonsterIslandCrystalDataHelper.lua
local GodMonsterIslandCrystalDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GodMonsterIslandCrystal = Class.GodMonsterIslandCrystal(ClassTypes.XObject)

local godMonsterIslandCrystalCache = {}
function GodMonsterIslandCrystalDataHelper.ClearCache()
    godMonsterIslandCrystalCache = {}
end

function GodMonsterIslandCrystal:__ctor__(cfg)
    self.id = cfg.__id
    self.collect_type = cfg.__collect_type
    self.type = cfg.__type
    self.num = cfg.__num
end

function GodMonsterIslandCrystalDataHelper.GetGodMonsterIslandCrystal(id)
    ClearDataManager:UseCache(CacheNames.god_monster_island_crystal, GodMonsterIslandCrystalDataHelper.ClearCache)
    local data = godMonsterIslandCrystalCache[id]
    if data == nil then
        local cfg = XMLManager.god_monster_island_crystal[id]
        if cfg ~= nil then
            data = GodMonsterIslandCrystal(cfg)
            godMonsterIslandCrystalCache[data.id] = data
        else
            LoggerHelper.Error("GodMonsterIslandCrystal id not exist:" .. id)
        end
    end
    return data
end

function GodMonsterIslandCrystalDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.god_monster_island_crystal:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GodMonsterIslandCrystalDataHelper:GetCollectType(id)
    local data = self.GetGodMonsterIslandCrystal(id)
    return data and data.collect_type or nil
end

function GodMonsterIslandCrystalDataHelper:GetType(id)
    local data = self.GetGodMonsterIslandCrystal(id)
    return data and data.type or nil
end

function GodMonsterIslandCrystalDataHelper:GetNum(id)
    local data = self.GetGodMonsterIslandCrystal(id)
    return data and data.num or nil
end

return GodMonsterIslandCrystalDataHelper
