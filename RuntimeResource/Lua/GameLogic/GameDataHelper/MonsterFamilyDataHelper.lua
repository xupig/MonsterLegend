-- MonsterFamilyDataHelper.lua
local MonsterFamilyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local MonsterFamily = Class.MonsterFamily(ClassTypes.XObject)

local monsterFamilyCache = {}

function MonsterFamily:__ctor__(cfg)
    self.id = cfg.__id
    self.map_id = cfg.__map_id
    self.monster_trigger_id = DataParseHelper.ParseListInt(cfg.__monster_trigger_id)
    self.max_level = cfg.__max_level
    self.min_level = cfg.__min_level
    self.best_protection = cfg.__best_protection
    self.equipment_rate = DataParseHelper.ParseNSuffix(cfg.__equipment_rate)
    self.exp_exp = cfg.__exp_exp
end

function MonsterFamilyDataHelper.GetMonsterFamily(id)
    local data = monsterFamilyCache[id]
    if data == nil then
        local cfg = XMLManager.monster_family[id]
        if cfg ~= nil then
            data = MonsterFamily(cfg)
            monsterFamilyCache[data.id] = data
        else
            LoggerHelper.Error("MonsterFamily id not exist:" .. id)
        end
    end
    return data
end

function MonsterFamilyDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.monster_family:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function MonsterFamilyDataHelper:GetIdByMapAndTrigger(mapId, triggerId)
    local data = XMLManager.monster_family:Keys()
    for i, v in ipairs(data) do
        if self:GetMapId(v) == mapId then
            local triggers = self:GetMonsterTriggerId(v)
            for _, trigger in ipairs(triggers) do
                if trigger == triggerId then
                    return v
                end
            end
        end
    end
end

function MonsterFamilyDataHelper:GetMapId(id)
    local data = self.GetMonsterFamily(id)
    return data and data.map_id or nil
end

function MonsterFamilyDataHelper:GetMonsterTriggerId(id)
    local data = self.GetMonsterFamily(id)
    return data and data.monster_trigger_id or nil
end

function MonsterFamilyDataHelper:GetMaxLevel(id)
    local data = self.GetMonsterFamily(id)
    return data and data.max_level or nil
end

function MonsterFamilyDataHelper:GetMinLevel(id)
    local data = self.GetMonsterFamily(id)
    return data and data.min_level or nil
end

function MonsterFamilyDataHelper:GetBestProtection(id)
    local data = self.GetMonsterFamily(id)
    return data and data.best_protection or nil
end

function MonsterFamilyDataHelper:GetEquipmentRate(id)
    local data = self.GetMonsterFamily(id)
    return data and data.equipment_rate or nil
end

function MonsterFamilyDataHelper:GetExpExp(id)
    local data = self.GetMonsterFamily(id)
    return data and data.exp_exp or nil
end

return MonsterFamilyDataHelper
