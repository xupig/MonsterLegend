-- GuildWeekTaskRewardDataHelper.lua
local GuildWeekTaskRewardDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local GuildWeekTaskReward = Class.GuildWeekTaskReward(ClassTypes.XObject)

local guildWeekTaskRewardCache = {}
function GuildWeekTaskRewardDataHelper.ClearCache()
    guildWeekTaskRewardCache = {}
end

function GuildWeekTaskReward:__ctor__(cfg)
    self.id = cfg.__id
    self.lv = DataParseHelper.ParseListInt(cfg.__lv)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.circle_reward = DataParseHelper.ParseMSuffixSort(cfg.__circle_reward)
    self.money_reward = cfg.__money_reward
end

function GuildWeekTaskRewardDataHelper.GetGuildWeekTaskReward(id)
    ClearDataManager:UseCache(CacheNames.guild_week_task_reward, GuildWeekTaskRewardDataHelper.ClearCache)
    local data = guildWeekTaskRewardCache[id]
    if data == nil then
        local cfg = XMLManager.guild_week_task_reward[id]
        if cfg ~= nil then
            data = GuildWeekTaskReward(cfg)
            guildWeekTaskRewardCache[data.id] = data
        else
            LoggerHelper.Error("GuildWeekTaskReward id not exist:" .. id)
        end
    end
    return data
end

function GuildWeekTaskRewardDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.guild_week_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GuildWeekTaskRewardDataHelper:GetLv(id)
    local data = self.GetGuildWeekTaskReward(id)
    return data and data.lv or nil
end

function GuildWeekTaskRewardDataHelper:GetReward(id)
    local data = self.GetGuildWeekTaskReward(id)
    return data and data.reward or nil
end

function GuildWeekTaskRewardDataHelper:GetMoneyReward(id)
    local data = self.GetGuildWeekTaskReward(id)
    return data and data.money_reward or 0
end

function GuildWeekTaskRewardDataHelper:GetCircleReward(id)
    local data = self.GetGuildWeekTaskReward(id)
    return data and data.circle_reward or nil
end


function GuildWeekTaskRewardDataHelper:GetWeekRewardByLevel()
    local level = GameWorld.Player().level
    local ids = {}
    local data = XMLManager.guild_week_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local lv = self:GetLv(v)
            if lv[1] <= level and level <= lv[2] then
                return self:GetCircleReward(v)
            end
        end
    end
    return nil
end

function GuildWeekTaskRewardDataHelper:GetRewardIdByLevel()
    local level = GameWorld.Player().level
    local ids = {}
    local data = XMLManager.guild_week_task_reward:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            local lv = self:GetLv(v)
            if lv[1] <= level and level <= lv[2] then
                return v
            end
        end
    end
    return nil
end

return GuildWeekTaskRewardDataHelper
