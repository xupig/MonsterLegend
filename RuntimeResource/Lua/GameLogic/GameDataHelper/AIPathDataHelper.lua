--AIPathDataHelper.lua
local AIPathDataHelper = {}

local XMLManager = GameManager.XMLManager
local AIPathDataCfgData = Class.AIPathDataCfgData(ClassTypes.XObject)
local DataParseHelper = GameDataHelper.DataParseHelper

function AIPathDataCfgData:__ctor__(cfg)
    self.id = cfg.__id
    self.team = cfg.__team
    self.serial = cfg.__serial
    self.path_pos = DataParseHelper.ParseNSuffix(cfg.__path_pos)
end

local AIPathDataCfgDataCache = {}

local AIPathTeamCfgDataRandomCache = {}

function AIPathDataHelper.InitData()
    local tmp_tbl = {}
    for id, v in pairs(XMLManager.aipath:Keys()) do
        tmp_tbl[v] = AIPathDataCfgData(XMLManager.aipath[v])
    end
    tmp_tbl[0] = nil

    for k, v in pairs(tmp_tbl) do


        local cfg_pos = {}
        local dst_pos = {}
        for k2, v2 in pairs(v.path_pos) do
            table.insert(cfg_pos, {k2, {['x']=v2[1], ['y']=v2[2], ['z']=v2[3]}})
        end
        table.sort(cfg_pos, function(a, b) return a[1] < b[1] end)
        for k2, v2 in pairs(cfg_pos) do
            table.insert(dst_pos, v2[2])
        end

        AIPathDataCfgDataCache[k] = dst_pos
    end

    --team表
    --tmp_tbl = {}
    for k, v in pairs(XMLManager.aipathteam:Keys()) do

        if v > 0 then
            local tmp_team_cfg = AIPathDataHelper.GetAIPathTeamCfgData(v)
            if tmp_team_cfg and tmp_team_cfg['team'] and tmp_team_cfg['serial'] then
                local team_id = tmp_team_cfg['team']
                if not AIPathTeamCfgDataRandomCache[team_id] then
                    AIPathTeamCfgDataRandomCache[team_id] = {}
                end
                local serial_id = tmp_team_cfg['serial']
                local need_new = true
                for k2, v2 in pairs(AIPathTeamCfgDataRandomCache[team_id]) do
                    if v2[1] == serial_id then
                        need_new = false
                        if v2[2] > v then
                            v2[2] = v
                            break
                        end

                    end
                end
                if need_new then
                    table.insert(AIPathTeamCfgDataRandomCache[team_id], {serial_id, v})
                end
            end
        end
    end

end




function AIPathDataHelper.GetPoss(id)
    return AIPathDataCfgDataCache[id]
end


--======================================游荡数据表=================================================
local AIWanderCfgData = Class.AIWanderCfgData(ClassTypes.XObject)

function AIWanderCfgData:__ctor__(cfg)
	self.id = cfg.__id
    self.wander_max_range = cfg.__wander_max_range
    self.wander_min_interval = cfg.__wander_min_interval
    self.wander_max_interval = cfg.__wander_max_interval
    self.wander_times_min = cfg.__wander_times_min
    self.wander_times_max = cfg.__wander_times_max
end

local AIWanderCfgDataCache = {}

function AIPathDataHelper.GetAIWanderCfgData(id)
	local data = AIWanderCfgDataCache[id]
	if data ~= nil then
		return data
	end

    data = XMLManager.aiwander[id]
    if data~=nil then
	    data = AIWanderCfgData(data) 
    else
        LoggerHelper.Error("AIWanderCfgData key Error :" .. tostring(id))
    end
	AIWanderCfgDataCache[id] = data
	return data
end

--======================================休息数据表=================================================
local AIRelaxCfgData = Class.AIRelaxCfgData(ClassTypes.XObject)

function AIRelaxCfgData:__ctor__(cfg)
	self.id = cfg.__id
	self.relax_min_time = cfg.__relax_min_time
    self.relax_max_time = cfg.__relax_max_time
end

local AIRelaxCfgDataCache = {}

function AIPathDataHelper.GetAIRelaxCfgData(id)
	local data = AIRelaxCfgDataCache[id]
	if data ~= nil then
		return data
	end

    data = XMLManager.airelax[id]
    if data~=nil then
	    data = AIRelaxCfgData(data) 
    else
        LoggerHelper.Error("AIRelaxCfgData key Error :" .. tostring(id))
    end
	AIRelaxCfgDataCache[id] = data
	return data
end

--======================================寻路组组合数据表=================================================
local AIPathTeamCfgData = Class.AIPathTeamCfgData(ClassTypes.XObject)

function AIPathTeamCfgData:__ctor__(cfg)
	self.id = cfg.__id
    self.team = cfg.__team
    self.serial = cfg.__serial
    self.type = cfg.__type
    self.type_id = cfg.__type_id
    self.next_id = DataParseHelper.ParseListInt(cfg.__next_id)
end

local AIPathTeamCfgDataCache = {}

function AIPathDataHelper.GetAIPathTeamCfgData(id)
	local data = AIPathTeamCfgDataCache[id]
	if data ~= nil then
		return data
	end

    data = XMLManager.aipathteam[id]
    if data~=nil then
	    data = AIPathTeamCfgData(data) 
    else
        LoggerHelper.Error("AIPathTeamCfgData key Error :" .. tostring(id))
    end
	AIPathTeamCfgDataCache[id] = data
	return data
end

function AIPathDataHelper.GetRandomSerial(team)
    local tmp_team_tbl = AIPathTeamCfgDataRandomCache[team]

    local tmp_serial_tbl = tmp_team_tbl[math.random(1, #tmp_team_tbl)]

    local cfg_id = tmp_serial_tbl[2]


    return cfg_id
end




AIPathDataHelper.InitData()
return AIPathDataHelper