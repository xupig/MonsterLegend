-- FriendIntimacyDataHelper.lua
local FriendIntimacyDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local FriendIntimacy = Class.FriendIntimacy(ClassTypes.XObject)

local friendIntimacyCache = {}
function FriendIntimacyDataHelper.ClearCache()
    friendIntimacyCache = {}
end

function FriendIntimacy:__ctor__(cfg)
    self.id = cfg.__id
    self.name = cfg.__name
    self.level = cfg.__level
    self.num = cfg.__num
    self.desc_attri = cfg.__desc_attri
    self.attri = DataParseHelper.ParseMSuffixSort(cfg.__attri)
    self.quality_type = cfg.__quality_type
end

function FriendIntimacyDataHelper.GetFriendIntimacy(id)
    ClearDataManager:UseCache(CacheNames.friend_intimacy, FriendIntimacyDataHelper.ClearCache)
    local data = friendIntimacyCache[id]
    if data == nil then
        local cfg = XMLManager.friend_intimacy[id]
        if cfg ~= nil then
            data = FriendIntimacy(cfg)
            friendIntimacyCache[data.id] = data
        else
            LoggerHelper.Error("FriendIntimacy id not exist:" .. id)
        end
    end
    return data
end

function FriendIntimacyDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.friend_intimacy:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function FriendIntimacyDataHelper:GetMaxNum()
    local data = self.GetFriendIntimacy(21)
    return data and data.num or nil
end

function FriendIntimacyDataHelper:GetDataTable()
    local dataList = {}
    local ids = self:GetAllId()
    for i, id in pairs(ids) do
        -- local num = FriendIntimacyDataHelper:GetNum(id)
        -- local name = LanguageDataHelper.GetLanguageData(FriendIntimacyDataHelper:GetName(id))
        local data = self.GetFriendIntimacy(id)
        --data._type = type
        table.insert(dataList, data)
    end
    return dataList
end

function FriendIntimacyDataHelper:GetTableThree()
    local dataList = self:GetDataTable()
    local ListTemp = {}
    local List = {}
    for i, v in ipairs(dataList) do
        --v._type = type
        table.insert(ListTemp, v)
        if i % 3 == 0 then
            LoggerHelper.Log("FriendIntimacyDataHelper:GetTableThree" .. i)
            table.insert(List, ListTemp)
            ListTemp = {}
        end
    end
    
    return List
end

function FriendIntimacyDataHelper:GetPercent(friendlyValue)
    if friendlyValue then
        local value = friendlyValue / self:GetMaxNum()
        return value
    end
end

function FriendIntimacyDataHelper:GetName(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.name or nil
end

function FriendIntimacyDataHelper:GetLevel(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.level or nil
end

function FriendIntimacyDataHelper:GetNum(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.num or nil
end

function FriendIntimacyDataHelper:GetDescAttri(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.desc_attri or nil
end

function FriendIntimacyDataHelper:GetAttri(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.attri or nil
end

function FriendIntimacyDataHelper:GetQualityType(id)
    local data = self.GetFriendIntimacy(id)
    return data and data.quality_type or nil
end

return FriendIntimacyDataHelper
