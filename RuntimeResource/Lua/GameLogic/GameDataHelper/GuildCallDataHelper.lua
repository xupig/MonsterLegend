-- GuildCallDataHelper.lua
local GuildCallDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local GuildCall = Class.GuildCall(ClassTypes.XObject)

local guildCallCache = {}

function GuildCall:__ctor__(cfg)
    self.id = cfg.__id
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.reward = DataParseHelper.ParseMSuffixSort(cfg.__reward)
    self.count = cfg.__count
    self.name = cfg.__name
end

function GuildCallDataHelper.GetGuildCall(id)
    local data = guildCallCache[id]
    if data == nil then
        local cfg = XMLManager.guild_call[id]
        if cfg ~= nil then
            data = GuildCall(cfg)
            guildCallCache[data.id] = data
        else
            LoggerHelper.Error("GuildCall id not exist:" .. id)
        end
    end
    return data
end

function GuildCallDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.guild_call:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function GuildCallDataHelper:GetTargets(id)
    local data = self.GetGuildCall(id)
    return data and data.targets or nil
end

function GuildCallDataHelper:GetReward(id)
    local data = self.GetGuildCall(id)
    return data and data.reward or nil
end

function GuildCallDataHelper:GetCount(id)
    local data = self.GetGuildCall(id)
    return data and data.count or nil
end

function GuildCallDataHelper:GetName(id)
    local data = self.GetGuildCall(id)
    return data and data.name or nil
end

return GuildCallDataHelper
