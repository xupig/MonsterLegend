-- ChatDataHelper.lua
local ChatDataHelper = {}
local XMLManager = GameManager.XMLManager
local ClearDataManager = PlayerManager.ClearDataManager

local EmojiData = Class.EmojiData(ClassTypes.XObject)
local EmojiDataCache = {}
function ChatDataHelper.ClearCache()
    EmojiDataCache = {}
end

function EmojiData:__ctor__(cfg)
     self.id = cfg.__id
     self.mode = cfg.__mode
     self.type = cfg.__type
     self.content = cfg.__content
end

function ChatDataHelper.GetEmojiData(id)
    ClearDataManager:UseCache(CacheNames.chat_emoji, ChatDataHelper.ClearCache)
    local data = EmojiDataCache[id]
	if data == nil then
		local cfg = XMLManager.chat_emoji[id]
        if cfg == nil then
            LoggerHelper.Error("ChatDataHelper.GetPath has no this id:" .. tostring(id),true)
        else
            data = EmojiData(cfg)
		    EmojiDataCache[data.id] = data
        end

	end
	return data
end

local ChatEmoji = {}
-- 聊天框表情
function ChatDataHelper.GetChatEmojiDatas(mode)
    if ChatEmoji[mode] ~= nil then
        return ChatEmoji[mode] 
    end
    ChatEmoji[mode] = {}
    local keys = XMLManager.chat_emoji:Keys()
    for k,v in pairs(keys) do
        local data = ChatDataHelper.GetEmojiData(v)
        if data.mode == mode then
            table.insert( ChatEmoji[mode],data )
        end
    end
    return ChatEmoji[mode]
end

-- ===================================================================================================
local EmojiAnimationData  = Class.EmojiAnimationData(ClassTypes.XObject)
local EmojiAnimationDataCache = {}
function ChatDataHelper.ClearAnimationCache()
    EmojiAnimationDataCache = {}
end
function EmojiAnimationData:__ctor__(cfg)
     self.id = cfg.__id
     self.name = cfg.__name
     self.startIndex = cfg.__start_index
     self.endIndex = cfg.__end_index
     self.frameRate = cfg.__frame_rate
end


function ChatDataHelper.GetEmojiAnimationData(id)
    ClearDataManager:UseCache(CacheNames.chat_emoji_animation, ChatDataHelper.ClearAnimationCache)
    local data = EmojiAnimationDataCache[id]
	if data == nil then
		local cfg = XMLManager.chat_emoji_animation[id]
        if cfg == nil then
            LoggerHelper.Error("GetEmojiAnimationData.GetPath has no this id:" .. tostring(id))
        else
            data = EmojiAnimationData(cfg)
		    EmojiAnimationDataCache[data.id] = data
        end

	end
	return data
end

return ChatDataHelper