-- NormalYunpanDataHelper.lua
local NormalYunpanDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local NormalYunpan = Class.NormalYunpan(ClassTypes.XObject)

local normalYunpanCache = {}
function NormalYunpanDataHelper.ClearCache()
    normalYunpanCache = {}
end

function NormalYunpan:__ctor__(cfg)
    self.id = cfg.__id
    self.cost = cfg.__cost
    self.day = cfg.__day
    self.reward = DataParseHelper.ParseNSuffix(cfg.__reward)
end

function NormalYunpanDataHelper.GetNormalYunpan(id)
    ClearDataManager:UseCache(CacheNames.normal_yunpan, NormalYunpanDataHelper.ClearCache)
    local data = normalYunpanCache[id]
    if data == nil then
        local cfg = XMLManager.normal_yunpan[id]
        if cfg ~= nil then
            data = NormalYunpan(cfg)
            normalYunpanCache[data.id] = data
        else
            LoggerHelper.Error("NormalYunpan id not exist:" .. id)
        end
    end
    return data
end

function NormalYunpanDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.normal_yunpan:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function NormalYunpanDataHelper:GetCost(id)
    local data = self.GetNormalYunpan(id)
    return data and data.cost or nil
end

function NormalYunpanDataHelper:GetDay(id)
    local data = self.GetNormalYunpan(id)
    return data and data.day or nil
end

function NormalYunpanDataHelper:GetReward(id)
    local data = self.GetNormalYunpan(id)
    return data and data.reward or nil
end

return NormalYunpanDataHelper
