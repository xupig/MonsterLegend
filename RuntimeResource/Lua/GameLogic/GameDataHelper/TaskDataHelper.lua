-- TaskDataHelper.lua
local TaskDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local Task = Class.Task(ClassTypes.XObject)

local taskCache = {}

function Task:__ctor__(cfg)
    self.id = cfg.__id
    self.type = cfg.__type
    self.limit = DataParseHelper.ParseNSuffix(cfg.__limit)
    self.last_task = DataParseHelper.ParseListInt(cfg.__last_task)
    self.next_task = cfg.__next_task
    self.name = cfg.__name
    self.desc_orders = cfg.__desc_orders
    self.desc_place = cfg.__desc_place
    self.desc_detail = cfg.__desc_detail
    self.desc = cfg.__desc
    self.explain = cfg.__explain
    self.intermezzi = DataParseHelper.ParseListInt(cfg.__intermezzi)
    self.icon = cfg.__icon
    self.targets = DataParseHelper.ParseMSuffixSort(cfg.__targets)
    self.targets_desc = DataParseHelper.ParseMSuffix(cfg.__targets_desc)
    self.accept_npc = cfg.__accept_npc
    self.commit_npc = cfg.__commit_npc
    self.accept_dialog = DataParseHelper.ParseNSuffixSort(cfg.__accept_dialog)
    self.commit_dialog = DataParseHelper.ParseNSuffixSort(cfg.__commit_dialog)
    self.reward = DataParseHelper.ParseNSuffixSort(cfg.__reward)
    self.image_map_id = cfg.__image_map_id
    self.auto_task_mechanism = DataParseHelper.ParseListInt(cfg.__auto_task_mechanism)
    self.little_shoes_open = cfg.__little_shoes_open
    self.little_shoes_open_start = cfg.__little_shoes_open_start
    self.little_shoes_open_end = cfg.__little_shoes_open_end
    self.task_guide = cfg.__task_guide
    self.task_arrowguide = cfg.__task_arrowguide
end

function TaskDataHelper.GetTask(id)
    local data = taskCache[id]
    if data == nil then
        local cfg = XMLManager.task[id]
        if cfg ~= nil then
            data = Task(cfg)
            taskCache[data.id] = data
        else
            LoggerHelper.Error("Task id not exist:" .. id)
        end
    end
    return data
end

function TaskDataHelper:GetDesc(id)
    local data = self.GetTask(id)
    return data and data.desc or nil
end

function TaskDataHelper:GetIcon(id)
    local data = self.GetTask(id)
    return data and data.icon or nil
end

function TaskDataHelper:GetAcceptNPC(id)
    local data = self.GetTask(id)
    return data and data.accept_npc or nil
end

function TaskDataHelper:GetCommitNPC(id)
    local data = self.GetTask(id)
    return data and data.commit_npc or nil
end

function TaskDataHelper:GetAcceptDialog(id)
    local data = self.GetTask(id)
    return data and data.accept_dialog or nil
end

function TaskDataHelper:GetCommitDialog(id)
    local data = self.GetTask(id)
    return data and data.commit_dialog or nil
end

function TaskDataHelper:GetName(id)
    local data = self.GetTask(id)
    return data and data.name or nil
end

function TaskDataHelper:GetTaskType(id)
    local data = self.GetTask(id)
    return data and data.type or nil
end

function TaskDataHelper:GetOrder(id)
    local data = self.GetTask(id)
    return data and data.desc_orders or nil
end

function TaskDataHelper:GetLocation(id)
    local data = self.GetTask(id)
    return data and data.desc_place or nil
end

function TaskDataHelper:GetDetail(id)
    local data = self.GetTask(id)
    return data and data.desc_detail or nil
end

function TaskDataHelper:GetExplain(id)
    local data = self.GetTask(id)
    return data and data.explain or nil
end

function TaskDataHelper:GetNextTask(id)
    local data = self.GetTask(id)
    return data and data.next_task or nil
end

function TaskDataHelper:GetLastTask(id)
    local data = self.GetTask(id)
    return data and data.last_task or nil
end

function TaskDataHelper:GetLimit(id)
    local data = self.GetTask(id)
    return data and data.limit or nil
end

function TaskDataHelper:GetIntermezzi(id)
    local data = self.GetTask(id)
    return data and data.intermezzi or nil
end

function TaskDataHelper:GetTargetsInfo(id)
    local data = self.GetTask(id)
    return data and data.targets or nil
end

function TaskDataHelper:GetTargetsDesc(id)
    local data = self.GetTask(id)
    return data and data.targets_desc or nil
end

function TaskDataHelper:GetReward(id)
    local data = self.GetTask(id)
    return data and data.reward or nil
end

function TaskDataHelper:GetImageMapId(id)
    local data = self.GetTask(id)
    return data and data.image_map_id or nil
end

function TaskDataHelper:GetAutoTaskMechanism(id)
    local data = self.GetTask(id)
    return data and data.auto_task_mechanism or nil
end

function TaskDataHelper:GetLittleShoesOpen(id)
    local data = self.GetTask(id)
    return data and data.little_shoes_open or 0
end

function TaskDataHelper:GetLittleShoesOpenStart(id)
    local data = self.GetTask(id)
    return data and data.little_shoes_open_start or 0
end

function TaskDataHelper:GetLittleShoesOpenEnd(id)
    local data = self.GetTask(id)
    return data and data.little_shoes_open_end or 0
end

function TaskDataHelper:GetTaskGuide(id)
    local data = self.GetTask(id)
    return data and data.task_guide or 0
end

function TaskDataHelper:GetTaskArrowGuide(id)
    local data = self.GetTask(id)
    return data and data.task_arrowguide or 0
end

return TaskDataHelper
