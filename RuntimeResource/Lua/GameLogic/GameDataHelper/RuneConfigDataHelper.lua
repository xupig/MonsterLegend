-- RuneConfigDataHelper.lua
local RuneConfigDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ClearDataManager = PlayerManager.ClearDataManager

local RuneConfig = Class.RuneConfig(ClassTypes.XObject)

local runeConfigCache = {}
function RuneConfigDataHelper.ClearCache()
    runeConfigCache = {}
end
local runeConfigData = nil

function RuneConfig:__ctor__(cfg)
    self.id = cfg.__id
    self.rune_id = cfg.__rune_id
    self.level = cfg.__level
    self.atrri = DataParseHelper.ParseMSuffix(cfg.__atrri)
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.resolve = DataParseHelper.ParseMSuffix(cfg.__resolve)
    self.exp = cfg.__exp
end

function RuneConfigDataHelper.GetRuneConfig(id)
    ClearDataManager:UseCache(CacheNames.rune_config, RuneConfigDataHelper.ClearCache)
    local data = runeConfigCache[id]
    if data == nil then
        local cfg = XMLManager.rune_config[id]
        if cfg ~= nil then
            data = RuneConfig(cfg)
            runeConfigCache[data.id] = data
        else
            LoggerHelper.Error("RuneConfig id not exist:" .. id)
        end
    end
    return data
end

function RuneConfigDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.rune_config:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function RuneConfigDataHelper:GetCost(id)
    local data = self.GetRuneConfig(id)
    return data and data.cost or nil
end

function RuneConfigDataHelper:GetRuneId(id)
    local data = self.GetRuneConfig(id)
    return data and data.rune_id or nil
end

function RuneConfigDataHelper:GetLevel(id)
    local data = self.GetRuneConfig(id)
    return data and data.level or nil
end

function RuneConfigDataHelper:GetAtrri(id)
    local data = self.GetRuneConfig(id)
    return data and data.atrri or nil
end

function RuneConfigDataHelper:GetResolve(id)
    local data = self.GetRuneConfig(id)
    return data and data.resolve or nil
end

function RuneConfigDataHelper:GetExp(id)
    local data = self.GetRuneConfig(id)
    return data and data.exp or nil
end

-- 以rune_id\level为key构建cofigdata
function RuneConfigDataHelper:GetRuneConfigData()
    if self._runeConfigData == nil then
        self._runeConfigData = {}
        local ids = RuneConfigDataHelper:GetAllId()                        
        for i,v in ipairs(ids) do          
            local id = v
            local runeconfig = RuneConfigDataHelper.GetRuneConfig(id)
            if runeconfig ~= nil then
                local runeid = runeconfig.rune_id
                local runelevel = runeconfig.level
                if self._runeConfigData[runeid] == nil then
                    self._runeConfigData[runeid] = {}
                end
                if self._runeConfigData[runeid][runelevel] == nil then
                    self._runeConfigData[runeid][runelevel] = {}
                end
                table.insert(self._runeConfigData[runeid][runelevel],runeconfig)
            end
        end        
    end
    return self._runeConfigData
end

function RuneConfigDataHelper:GetAttByIdlevel(runeid,level)
    local d = self:GetRuneConfigData()
    if d and d[runeid] and d[runeid][level] then
        return d[runeid][level][1]
    end
end

return RuneConfigDataHelper
