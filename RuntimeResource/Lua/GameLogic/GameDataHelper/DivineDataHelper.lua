local DivineDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = require("ServerConfig/public_config")
local ClearDataManager = PlayerManager.ClearDataManager

local function SortByOrder(a,b)
    return tonumber(a.order) < tonumber(b.order)
end

local divineData = {}
function DivineDataHelper.ClearCache()
    divineData = {}
end
local DivineCfg = Class.DivineCfg(ClassTypes.XObject)

function DivineCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.deduction = cfg.__deduction
    self.limit = cfg.__limit
    self.cost = DataParseHelper.ParseMSuffix(cfg.__cost)
    self.wholesale = DataParseHelper.ParseMSuffix(cfg.__wholesale)
    self.grand = DataParseHelper.ParseNSuffix(cfg.__grand)
end

function DivineDataHelper:GetDivineData(id)
    ClearDataManager:UseCache(CacheNames.divine, DivineDataHelper.ClearCache)
    local data = divineData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.divine[id]
    if data~=nil then
	    data = DivineCfg(data)
    end
	divineData[id] = data
	return data
end

function DivineDataHelper:DivineCost(id)
	local data = self:GetDivineData(id)
	return data.cost
end

function DivineDataHelper:DivineWholesale(id)
	local data = self:GetDivineData(id)
	return data.wholesale
end

local divineGrandCache = {}
function DivineDataHelper:DivineGrand(id)
    if divineGrandCache[id] ~= nil then
        return divineGrandCache[id]
    end
    divineGrandCache[id] = {}
    local data = self:GetDivineData(id).grand
    for k,v in pairs(data) do
        local subData = {}
        subData.order = k
        subData.itemId = v[1]
        subData.count = v[2]
        table.insert(divineGrandCache[id], subData)
        table.sort(divineGrandCache[id], SortByOrder)
    end
	return divineGrandCache[id]
end

----------------占星奖励表----------------------
local divineRewardData = {}
function DivineDataHelper.ClearRewardCache()
    divineRewardData = {}
end
local DivineRewardCfg = Class.DivineRewardCfg(ClassTypes.XObject)

function DivineRewardCfg:__ctor__(cfg)
    self.id = cfg.__id
    self.item = cfg.__item
    self.count = cfg.__count
    self.type = cfg.__type
    self.order = cfg.__order
end

function DivineDataHelper:GetDivineRewardCfgData(id)
    ClearDataManager:UseCache(CacheNames.divine_reward, DivineDataHelper.ClearRewardCache)
    local data = divineRewardData[id]
	if data ~= nil then
		return data
    end
	data = XMLManager.divine_reward[id]
    if data~=nil then
	    data = DivineRewardCfg(data)
    end
	divineRewardData[id] = data
	return data
end

local divineRewardListData = {}
function DivineDataHelper:GetDivineRewardListData()
    if #divineRewardListData > 0 then
        return divineRewardListData
    end
    local keys = XMLManager.divine_reward:Keys()
    for i=1,#keys do
        local id = keys[i]
        if id ~= 0 then
            local data = DivineDataHelper:GetDivineRewardCfgData(id)
            table.insert(divineRewardListData, data)
        end
    end
    table.sort(divineRewardListData, SortByOrder)
	return divineRewardListData
end

return DivineDataHelper