-- StarVeinDataHelper.lua
local StarVeinDataHelper = {}

local XMLManager = GameManager.XMLManager
local DataParseHelper = GameDataHelper.DataParseHelper

local StarVein = Class.StarVein(ClassTypes.XObject)

local starVeinCache = {}
local starVeinRanlLevelCache = {}


function StarVein.ClearCache()
    starVeinCache = {}
    starVeinRanlLevelCache = {}
end

function StarVein:__ctor__(cfg)
    self.id = cfg.__id
    self.rank = cfg.__rank
    self.level = cfg.__level
    self.cost = cfg.__cost
    self.attri_level = DataParseHelper.ParseMSuffix(cfg.__attri_level)
    self.attri_rank = DataParseHelper.ParseMSuffix(cfg.__attri_rank)
    self.attri_percent = cfg.__attri_percent
    self.icon = cfg.__icon
    self.constellation = cfg.__constellation
    self.bg = cfg.__bg
    self.position = DataParseHelper.ParseListInt(cfg.__position)
end

function StarVeinDataHelper.GetStarVein(id)
    local data = starVeinCache[id]
    if data == nil then
        local cfg = XMLManager.star_vein[id]
        if cfg ~= nil then
            data = StarVein(cfg)
            starVeinCache[data.id] = data
        else
            LoggerHelper.Error("StarVein id not exist:" .. id)
        end
    end
    return data
end

function StarVeinDataHelper:GetAllId()
    local ids = {}
    local data = XMLManager.star_vein:Keys()
    for i, v in ipairs(data) do
        if v ~= 0 then
            table.insert(ids, v)
        end
    end
    return ids
end

function StarVeinDataHelper:GetRank(id)
    local data = self.GetStarVein(id)
    return data and data.rank or nil
end

function StarVeinDataHelper:GetLevel(id)
    local data = self.GetStarVein(id)
    return data and data.level or nil
end

function StarVeinDataHelper:GetCost(id)
    local data = self.GetStarVein(id)
    return data and data.cost or nil
end

function StarVeinDataHelper:GetAttriLevel(id)
    local data = self.GetStarVein(id)
    return data and data.attri_level or nil
end

function StarVeinDataHelper:GetAttriRank(id)
    local data = self.GetStarVein(id)
    return data and data.attri_rank or nil
end

function StarVeinDataHelper:GetAttriPercent(id)
    local data = self.GetStarVein(id)
    return data and data.attri_percent or nil
end

function StarVeinDataHelper:GetIcon(id)
    local data = self.GetStarVein(id)
    return data and data.icon or nil
end

function StarVeinDataHelper:GetConstellation(id)
    local data = self.GetStarVein(id)
    return data and data.constellation or nil
end

function StarVeinDataHelper:GetBg(id)
    local data = self.GetStarVein(id)
    return data and data.bg or nil
end

function StarVeinDataHelper:GetPosition(id)
    local data = self.GetStarVein(id)
    return data and data.position or nil
end

function StarVeinDataHelper:GetDataByRankLevel(rank,level)
    if starVeinRanlLevelCache and starVeinRanlLevelCache[rank] and starVeinRanlLevelCache[rank][level] then
        return starVeinRanlLevelCache[rank][level]
    end
    starVeinRanlLevelCache = self:HandleDataCache()
    return starVeinRanlLevelCache[rank][level]
end

function StarVeinDataHelper:HandleDataCache()
    local d={}
    local data = XMLManager.star_vein:Keys()
    for k, id in ipairs(data) do
        if id ~= 0 then
            local rank = self:GetRank(id)
            local level = self:GetLevel(id)
            if not d[rank] then
                d[rank] = {}
            end
            d[rank][level] = id
        end
    end
    return d
end

function StarVeinDataHelper:GetDataRank(rank)
    return starVeinRanlLevelCache[rank]
end

function StarVeinDataHelper:GetPreRankIds(rank)
    local ids = {}
    for i=1,rank-1 do
        local r = self:GetDataRank(i)
        for k,id in pairs(r) do
            table.insert(ids,id)
        end
    end
    return ids
end

function StarVeinDataHelper:GetCurRankIds(rank)
    local ids = {}
    local r = self:GetDataRank(rank)
    for k,id in pairs(r) do
        table.insert(ids,id)
    end
    return ids
end

function StarVeinDataHelper:GetPositionByIds(ids)
    local pos = {}
    for k,id in pairs(ids)do
        table.insert(pos,self:GetPosition(id))
    end
    return pos
end

function StarVeinDataHelper:GetPositionByRank(rank)
    local ids = self:GetCurRankIds(rank)
    return self:GetPositionByIds(ids)
end

--找回所有属性
function StarVeinDataHelper:GetAttByIds(ids)
    local atts={}
    for k, id in pairs(ids)do
        local d = self:GetAttriLevel(id)
        for attname,v in pairs(d) do
            if not atts[attname] then
                atts[attname] = 0
            end
            atts[attname] = atts[attname]+v
        end
    end
    return atts
end

--是否最终
function StarVeinDataHelper:IsFunRank(id)
    if table.containsValue(self:GetAllId(),id) then
        return false
    end
    return true
end

--是否阶满了
function StarVeinDataHelper:IsFunLevel(rank,level)
    if table.isEmpty(starVeinRanlLevelCache)then
        starVeinRanlLevelCache = self:HandleDataCache()
    end
    
    if starVeinRanlLevelCache[rank] and table.containsValue(starVeinRanlLevelCache[rank],level) then
        return false
    end
    return true
end

return StarVeinDataHelper
