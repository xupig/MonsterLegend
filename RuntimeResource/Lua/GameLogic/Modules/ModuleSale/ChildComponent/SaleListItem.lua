-- SaleListItem.lua
local SaleListItem = Class.SaleListItem(ClassTypes.UIListItem)
SaleListItem.interface = GameConfig.ComponentsConfig.Component
SaleListItem.classPath = "Modules.ModuleSale.ChildComponent.SaleListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local SaleManager = PlayerManager.SaleManager
local ActorModelComponent = GameMain.ActorModelComponent
local EquipModelComponent = GameMain.EquipModelComponent
local TransformDataHelper = GameDataHelper.TransformDataHelper
local NPCDataHelper = GameDataHelper.NPCDataHelper
local public_config = GameWorld.public_config
local XImageFlowLight = GameMain.XImageFlowLight
local XArtNumber = GameMain.XArtNumber
local GUIManager = GameManager.GUIManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local UIParticle = ClassTypes.UIParticle
local UIComponentUtil = GameUtil.UIComponentUtil
function SaleListItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function SaleListItem:InitView()
    self._txtname = self:GetChildComponent("Container_content/Text_Name",'TextMeshWrapper')
    self._txtprice = self:GetChildComponent("Container_content/Button_Buy/Text_Price",'TextMeshWrapper')
    local buttonGo = self:FindChildGO("Container_content/Button_Buy")
    self._titleGo = self:FindChildGO("Container_content/Image_Title")
    self.RolemodeGo = self:FindChildGO("Container_content/Container_Mode0");
    self.Rolemode = self:AddChildComponent("Container_content/Container_Mode0",ActorModelComponent);
    self.EquipmodeGo = self:FindChildGO("Container_content/Container_Mode1");
    self.Equipmode = self:AddChildComponent("Container_content/Container_Mode1",EquipModelComponent);
    self.Rolemode:SetStartSetting(Vector3(0, 0, -500), Vector3(0, 180, 0), 1)
    self.Equipmode:SetStartSetting(Vector3(0, 0, -500), Vector3(0, 180, 0), 1)
    self.iconGo = self:FindChildGO("Container_content/Button_Buy/Image_Icon");
    self._goButtonFlowLight = self:AddChildComponent("Container_content/Button_Buy", XImageFlowLight)
    self._csBH:AddClick(buttonGo,function() self:onBuy() end)
    self._fashionNumber = self:AddChildComponent("Container_content/Container_FightPower", XArtNumber)
    self._contentTran = self:FindChildGO("Container_content").transform

    local fxgo = self:FindChildGO("Container_content/Container_Fx")
    self._Fx = UIComponentUtil.AddLuaUIComponent(fxgo, UIParticle)
    self._Fx:Play(true,true)
    self._Fx:SetPosition(Vector3(40,80,0))
end

function SaleListItem:onBuy()
    if self._itemdata then
        local d = {}
        local data = {}
        d.id = MessageBoxType.Tip
        d.value =  data
        data.confirmCB=function()SaleManager:SaleRpc(self._itemdata._id) GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        data.cancelCB=function ()GUIManager.ClosePanel(PanelsConfig.MessageBox) end
        local arg = LanguageDataHelper.GetArgsTable()
        local v = self._itemdata._price
        arg['0']=v
        arg['1']=self._itemdata._name
        SaleManager:SetSaleBuyName(self._itemdata._name)
        data.text = LanguageDataHelper.CreateContent(81672,arg)
        GUIManager.ShowPanel(PanelsConfig.MessageBox, d)
    end
end

function SaleListItem:OnDestroy()
    self._isnumRole = nil
    self._isnumMode = nil
    self._isSet = nil
end

function SaleListItem:OnRefreshData(data)
    local modelInfo = data._mode
    if modelInfo[1] == 1 then
        self.Equipmode:Show()
        self.EquipmodeGo:SetActive(true)
        self.RolemodeGo:SetActive(false)
        local modelId = modelInfo[2]
        local posx = modelInfo[7]
        local posy = modelInfo[8]
        local posz = modelInfo[9]
        local rotationx = modelInfo[4]
        local rotationy = modelInfo[5]
        local rotationz = modelInfo[6]
        local scale = modelInfo[3]
        self.Equipmode:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
        self.Equipmode:LoadEquipModel(modelId,"")
        if not self._isnumMode then
            self._isnumMode = true
            self._fashionNumber:SetNumber(4000)
        end
        self.Equipmode:SetSelfRotate(true)
    elseif modelInfo[1] == 4 then
        self.Rolemode:Show()
        self.RolemodeGo:SetActive(true)
        self.EquipmodeGo:SetActive(false)
        local facade = NPCDataHelper.GetNPCFacade(modelInfo[2])
        self.Rolemode:LoadAvatarModel(GameWorld.Player().vocation,facade,true)
        if not self._isnumRole then
            self._isnumRole = true
            self._fashionNumber:SetNumber(15000)
        end
        self.Equipmode:SetSelfRotate(false)
    end
    if not self._isSet then
        self._isSet = true
        local diamonIcon = ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS)
        GameWorld.AddIcon(self.iconGo,diamonIcon)
    end
    if data._isFirst then
        self._titleGo:SetActive(true)
    else
        self._titleGo:SetActive(false)
    end
    self._itemdata = data
    self._txtname.text = data._name
    self._txtprice.text = data._price
end

function SaleListItem:SetModelState(state)
    -- if self._curstate~=state then
        self.RolemodeGo:SetActive(state)
        self.EquipmodeGo:SetActive(state)
        self._curstate = state
        if state then
            self._Fx:Play(true,true)
        else
            self._Fx:Stop()
        end
    -- end
end

function SaleListItem:SetModelHideFx()
    self.Rolemode:HideFx()
    self.Equipmode:HideFx()
end

function SaleListItem:SetModelShowFx()
    self.Rolemode:ShowFx()
    self.Equipmode:ShowFx()
end
