-- SaleReportView.lua
local SaleReportView = Class.SaleReportView(ClassTypes.BaseComponent)
SaleReportView.interface = GameConfig.ComponentsConfig.Component
SaleReportView.classPath = "Modules.ModuleSale.ChildView.SaleReportView"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local action_config = GameWorld.action_config
local UIParticle = ClassTypes.UIParticle
local SaleManager = PlayerManager.SaleManager

function SaleReportView:Awake()
    self._csBH = self:GetComponent("LuaUIComponent")
    self:InitView()
end
    
function SaleReportView:InitView()
    self._textName = self:GetChildComponent("Text_Name","TextMeshWrapper")
    local btnCon = self:FindChildGO("Button_Confirm")
    self._csBH:AddClick(btnCon,function() self:onBtnCon() end)
    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30009")
end

function SaleReportView:onBtnCon()
    self:OnClose()
end


function SaleReportView:OnDestroy()
    self._csBH = nil
end


function SaleReportView:OnClose()
    self._iconBg1=nil
    self._iconBg2=nil
    self.gameObject:SetActive(false) 
    self._fx:Stop()
end

function SaleReportView:OnShow()
    self._iconBg1 = self:FindChildGO("Image_Bg1")
    self._iconBg2 = self:FindChildGO("Image_Bg2")
    GameWorld.AddIcon(self._iconBg1,13523)
    GameWorld.AddIcon(self._iconBg2,13523)
    GameWorld.PlaySound(15)
    self._fx:Play(true,false)
    self._textName.text=SaleManager:GetSaleBuyName()
end

