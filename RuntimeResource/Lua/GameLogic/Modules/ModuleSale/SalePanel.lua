--SalePanel
require "Modules.ModuleSale.ChildComponent.SaleListItem"
require "Modules.ModuleSale.ChildView.SaleReportView"
local SalePanel = Class.SalePanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local SaleManager = PlayerManager.SaleManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIPageableList = ClassTypes.UIPageableList
local SaleListItem = ClassTypes.SaleListItem
local AddChargeDays = 7
local SaleData = PlayerManager.PlayerDataManager.saleData
local SaleReportView = ClassTypes.SaleReportView

function SalePanel:Awake()
    self._timerCountDownId=-1
    self:InitFuns()
    self:InitComps()
end

function SalePanel:InitComps()   
    self._textLeft = self:GetChildComponent("Text_Lefttime","TextMeshWrapper")
    self._leftgo = self:FindChildGO("Image_Left")
    self._rightgo = self:FindChildGO("Image_Right")
    local btngo = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._leftgo,function() self:ClickLeft() end)
    self._csBH:AddClick(self._rightgo,function() self:ClickRight() end)
    self._csBH:AddClick(btngo,function() GUIManager.ClosePanel(PanelsConfig.Sale) end)

    local scrollview,list = UIPageableList.AddScrollPageList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(SaleListItem)
    self._list:SetPadding(0,0,0,0)
    self._list:SetGap(0,10)
    local itemClickedCB = function(index)self:OnItemClicked(index)end
    self._list:SetItemClickedCB(itemClickedCB)
    self._list:UseObjectPool(false)
    local func = function(page)self:PageChangedend(page)end
    self._scrollview:SetOnCurrentPageChangedCB(func)
    self._ReportView = self:AddChildLuaUIComponent("Container_Report", SaleReportView)
end

function SalePanel:PageChangedend(page)
    self:SetPage(page)
end

function SalePanel:SetPage(page)
    if page>0 and page <self._totalPage then
        self._leftgo:SetActive(true)
        self._rightgo:SetActive(true)
    elseif page==0 then
        self._leftgo:SetActive(false)
        self._rightgo:SetActive(true)
    elseif page==self._totalPage then
        self._leftgo:SetActive(true)
        self._rightgo:SetActive(false)
    else
        self._leftgo:SetActive(false)
        self._rightgo:SetActive(false)
    end
    self:HandleModelFx(false)
end

function SalePanel:HandleModelFx(state)
    local len = self._list:GetLength()-1
    if len>0 then
        local pox = math.abs(self._list.transform.localPosition.x)
        local index = math.modf(pox/343)
        for i=0, len do
            local item = self._list:GetItem(i)           
            if item then
                if index>0 then
                    if i<index then
                        item:SetModelState(state)
                    else
                        item:SetModelState(not state)
                    end
                else
                    if i<3 then
                        item:SetModelState(not state)
                    else
                        item:SetModelState(state)
                    end
                end
            end
        end
    end
end


function SalePanel:CountDownTimer()
    local a=self:LeftTimeSec()
    if a<=0 then
        if self._timerCountDownId~=-1 then
            TimerHeap:DelTimer(self._timerCountDownId)
            self._timerCountDownId=-1
        end
        self._timeText.text=LanguageDataHelper.GetContent(76951)
        return 
    end
    self:SetLefeTime(a)    
end


function SalePanel:InitCountDownTimer()
    local a=self:LeftTimeSec()
    if a>0 then
        if self._timerCountDownId==-1 then
            self._timerCountDownId=TimerHeap:AddSecTimer(0,1,0,self.timrCountTimer)
        end
    else
        if self._timerCountDownId~=-1 then
            TimerHeap:DelTimer(self._timerCountDownId)
            self._timerCountDownId=-1
        end
    end
    self:SetLefeTime(a)
end

function SalePanel:SetLefeTime(lefttime)
    self._textLeft.text=LanguageDataHelper.GetContent(80820)..DateTimeUtil:FormatFullTime(lefttime)    
end

function SalePanel:LeftTimeSec()
    local createtime = GameWorld.Player().create_time
    local s = DateTimeUtil.MinusSec(createtime)
    local a = self.totaltime-s
    if a<=0 then
        return 0
    else
        return a
    end
end

function SalePanel:OnItemClicked(index)
   
end



function SalePanel:ClickLeft()
    local page = self._scrollview:GetCurrentPage()
    local page = page - 1
    if page>=0 then
        self._scrollview:SetCurrentPage(page)
        self:SetPage(page)  
    end
end

function SalePanel:ClickRight()
    local page = self._scrollview:GetCurrentPage()
    local page = page + 1
    if page<=self._totalPage then
        self._scrollview:SetCurrentPage(page)
        self:SetPage(page)  
    else
    end
end

function SalePanel:OnDestroy()

end

--override
function SalePanel:OnShow(data)
    self._iconBg = self:FindChildGO("Image_bg")
    GameWorld.AddIcon(self._iconBg,13512)
    self:AddEventListeners()
    self.totaltime = AddChargeDays*DateTimeUtil.OneDayTime
    self:InitCountDownTimer()
    local data = SaleData:GetSaleListData()
    local v0,v1 = math.modf(#data/3)
    self._list:SetDataList(data)
    if v1>0 then
        self._totalPage = v0
        self._scrollview:SetTotalPage(v0+1)
    else
        self._totalPage = v0-1
        self._scrollview:SetTotalPage(v0)
    end
    if self._totalPage==0 then
        self._leftgo:SetActive(false)
        self._rightgo:SetActive(false)
    else
        self._leftgo:SetActive(false)
        self._rightgo:SetActive(true)
    end
    self._scrollview:SetCurrentPage(0)
    self:PageChangedend(0)
end

function SalePanel:UpdateList()
    local data = SaleData:GetSaleListData()
    if self._list then
        self._list:SetDataList(data)
    end
    -- self:FindGet()
    local page = self._scrollview:GetCurrentPage()
    self:PageChangedend(page)
end
--override
function SalePanel:OnClose()
    self._iconBg = nil
    self:RemoveEventListeners()
end

function SalePanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.SaleBuyResp, self.updataFun)
    
end

function SalePanel:RemoveEventListeners()   
    EventDispatcher:RemoveEventListener(GameEvents.SaleBuyResp, self.updataFun)
 
end

function SalePanel:FindGet()
    -- self._ReportView:SetActive(true)
    -- self._ReportView:OnShow()
    -- self:HandleModelFx(true)
end

function SalePanel:InitFuns()
    self.timrCountTimer = function()self:CountDownTimer()end
    self.updataFun = function()self:UpdateList()end
end

function SalePanel:WinBGType()
    return PanelWinBGType.NoBG
end
