--SaleModule.lua
SaleModule = {}
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
function SaleModule.Init()
     GUIManager.AddPanel(PanelsConfig.Sale,"Panel_Sale",GUILayer.LayerUIPanel,"Modules.ModuleSale.SalePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end
return SaleModule