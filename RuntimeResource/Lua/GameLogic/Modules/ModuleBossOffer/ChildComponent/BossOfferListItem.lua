-- BossOfferListItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local BossOfferListItem = Class.BossOfferListItem(ClassTypes.UIListItem)
BossOfferListItem.interface = GameConfig.ComponentsConfig.Component
BossOfferListItem.classPath = "Modules.ModuleBossOffer.ChildComponent.BossOfferListItem"
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

function BossOfferListItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function BossOfferListItem:InitView()
    
    self._containerItem = self:FindChildGO("Container_Item")
    local a = GUIManager.AddItem(self._containerItem, 1)
    self._item = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._item:ActivateTipsById()
end


function BossOfferListItem:OnDestroy()
    self._containerItem = nil
end

function BossOfferListItem:OnRefreshData(data)
    if data then
        self._item:SetItem(data[1],data[2])
    end
end

