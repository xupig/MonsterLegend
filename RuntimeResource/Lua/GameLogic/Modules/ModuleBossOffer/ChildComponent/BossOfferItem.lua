-- BossOfferItem.lua
local BossOfferItem = Class.BossOfferItem(ClassTypes.UIListItem)
BossOfferItem.interface = GameConfig.ComponentsConfig.Component
BossOfferItem.classPath = "Modules.ModuleBossOffer.ChildComponent.BossOfferItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
function BossOfferItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function BossOfferItem:InitView()

    self._txtname = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._txtlevel = self:GetChildComponent("Text_Level",'TextMeshWrapper')
    self._iconGo = self:FindChildGO("Image_Icon")  
end

function BossOfferItem:OnDestroy()
    self._txtname = nil
    self._txtlevel = nil
    self._iconGo = nil
end

function BossOfferItem:OnRefreshData(itemdata)
    if itemdata then
        if self._curname ~= itemdata._name then
            self._curname = itemdata._name
            self._txtname.text = self._curname
        end
        if self._curiconid ~= itemdata._iconid then
            self._curiconid = itemdata._iconid
            GameWorld.AddIcon(self._iconGo,self._curiconid)
        end
        if self._curlevel ~= itemdata._level then
            self._curlevel = itemdata._level
            self._txtlevel.text = "Lv."..self._curlevel
        end
    end
end
