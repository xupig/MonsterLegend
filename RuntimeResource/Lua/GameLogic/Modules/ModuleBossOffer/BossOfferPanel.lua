--BossOfferPanel
require "Modules.ModuleBossOffer.ChildComponent.BossOfferListItem"
require "Modules.ModuleBossOffer.ChildComponent.BossOfferItem"
local BossOfferPanel = Class.BossOfferPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local BossOfferManager = PlayerManager.BossOfferManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIList = ClassTypes.UIList
local BossOfferListItem = ClassTypes.BossOfferListItem
local BossOfferItem = ClassTypes.BossOfferItem
local BossOfferData = PlayerManager.PlayerDataManager.bossOfferData
local UIComponentUtil = GameUtil.UIComponentUtil
local BossOfferDataHelper = GameDataHelper.BossOfferDataHelper
local public_config = GameWorld.public_config
function BossOfferPanel:Awake()
    self:InitFuns()
    self:InitComps()
end

function BossOfferPanel:InitComps()   
    self._texttips = self:GetChildComponent("Text_Tips","TextMeshWrapper")
    self._btngetgo = self:FindChildGO("Button_Get")
    self._btngo = self:FindChildGO("Button_Go")
    self._redgo = self:FindChildGO("Button_Get/Image_Red")
    self._btnwrap = self:GetChildComponent("Button_Get","ButtonWrapper")
    self._csBH:AddClick(self._btngetgo,function() self:ClickGetFun() end)
    self._csBH:AddClick(self._btngo,function() self:ClickGoFun() end)

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list = list
    self._scrollview = scrollview
    self._list:SetItemType(BossOfferListItem)
    self._list:SetPadding(0,0,0,0)
    self._list:SetGap(0,20)
    local itemClickedCB = function(index)self:OnItemClicked(index)end
    self._list:SetItemClickedCB(itemClickedCB)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    self._items={}
    self._itemgos={}  
    for i=0,2 do
        local itemGo = self:FindChildGO("Container_Boss/Container_Item"..i)
        local item = UIComponentUtil.AddLuaUIComponent(itemGo, BossOfferItem) 
        self._itemgos[i] = itemGo
   		self._items[i] = item
    end
    self._scrollview:SetHorizontalMove(true)
    self._scrollview:SetVerticalMove(false)
end

function BossOfferPanel:ClickGetFun()
    BossOfferManager.BossRewardRpc()    
end

function BossOfferPanel:ClickGoFun()
    GUIManager.ShowPanel(PanelsConfig.WorldBoss)
    GUIManager.ClosePanel(PanelsConfig.BossOffer)
end


function BossOfferPanel:OnItemClicked(index)
   
end


function BossOfferPanel:OnDestroy()

end

--override
function BossOfferPanel:OnShow(data)
    self:AddEventListeners()
    BossOfferManager.BossInfoRpc()
    self:InitInfo()
end

function BossOfferPanel:InitInfo()
    self:InitIconList()
    self:InitRewardList()
    self:InitPro()
end
function BossOfferPanel:InitRewardList()
    local info = BossOfferData:GetInfoState()
    if info then
        if info[1] == public_config.BOSS_REWARD_STATE_RECEIVED then
            local id = BossOfferData:GetCurStep()
            self:SetRewardList(id)            
        else
            self:SetRewardList(info[2])
        end
    end
end

function BossOfferPanel:UpdateRewardList()
    local state = BossOfferData:GetState()
    if state then
        local id = BossOfferData:GetCurStep()
        self:SetRewardList(id)            
    end
end

function BossOfferPanel:SetRewardList(id)
    if self._curRewardid ~= id then
        self._curRewardid = id
        local items = BossOfferData:GetAllBossRewards(self._curRewardid)
        self._list:SetDataList(items)
    end
end

function BossOfferPanel:InitPro()
    local info = BossOfferData:GetInfoState()
    if info then
        self:SetPro(info[2],info[1],0)
    end
end

function BossOfferPanel:SetPro(id,state,flage)
    self._btnwrap.interactable = false
    self._redgo:SetActive(false)
    if state==public_config.BOSS_REWARD_STATE_COMPLETED then
        local targetpro = BossOfferDataHelper:GetTargets(id)
        if targetpro then
            local _,n = next(targetpro)
            local nameid = BossOfferDataHelper:GetName(id)
            self:SetTextPro(nameid,n,n)                
        end
        self._btnwrap.interactable = true
        self._redgo:SetActive(true)
    elseif state==public_config.BOSS_REWARD_STATE_ACCEPTED then
        local targetpro = BossOfferDataHelper:GetTargets(id)
        local curpro = {}
        if flage==0 then
            curpro = BossOfferData:GetCurProgress(id)
        else
            local curstate = BossOfferData:GetState()
            local curid = id
            if not table.isEmpty(curstate) then
                local s = curstate[curid]
                if s then
                    curpro = s[1]
                end
            end
        end
        if targetpro and curpro then
            local _,n1 = next(targetpro)
            local _,n2 = next(curpro)
            local nameid = BossOfferDataHelper:GetName(id)
            self:SetTextPro(nameid,n1,n2)
        end
    elseif state==public_config.BOSS_REWARD_STATE_RECEIVED then     
        local stepid = id
        local targetpro = BossOfferDataHelper:GetTargets(stepid)
        if targetpro then
            local _,n = next(targetpro)
            local curid = BossOfferDataHelper:GetName(stepid)
            self:SetTextPro(curid,n,0)
        end
    end
end

function BossOfferPanel:SetTextPro(id,n1,n2)
    local p1 = n1 or 0
    local p2 = n2 or 0
    self._texttips.text = LanguageDataHelper.CreateContentWithArgs(id,{['0']=p2,['1']=p1})
end

function BossOfferPanel:InitIconList()
    local info = BossOfferData:GetInfoState()
    if info then
        if info[1] == public_config.BOSS_REWARD_STATE_RECEIVED then
            local id = BossOfferData:GetCurStep()
            self:SetIconList(id)            
        else
            if self._curIconid ~= info[2] then
                self:SetIconList(info[2])
            end
        end
    end
end

function BossOfferPanel:SetIconList(id)
    if self._curIconid~= id then
        self._curIconid = id
        local items = BossOfferData:GetAllBossIconItems(self._curIconid)
        local i = 0
        for k,v in ipairs(items) do
            self._items[i]:OnRefreshData(v)
            i = i + 1
        end
    end
end

function BossOfferPanel:UpdataPro()
    local state = BossOfferData:GetState()
    if state then
        local id = GameWorld.Player().boss_reward_unlocked_step
        local s = state[id]
        if s then
            local pro = s[1]
            local des = s[2]
            self:SetPro(id,des,1)
        end
    end
end

function BossOfferPanel:Update()
    self:UpdataPro()
    self:UpdataIconList()
    self:UpdateRewardList()
end

function BossOfferPanel:UpdataIconList()
    local state = BossOfferData:GetState()
    if state then
        local id = BossOfferData:GetCurStep()
        self:SetIconList(id)
    end
end

--override
function BossOfferPanel:OnClose()
    self:RemoveEventListeners()
    BossOfferManager:OpenTimer()
    BossOfferManager.BossInfoRpc()
end

function BossOfferPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.BossOfferGetResp, self.updataFun)
    EventDispatcher:AddEventListener(GameEvents.BossOfferStateUpdate, self.updataFun)
    EventDispatcher:AddEventListener(GameEvents.BossOfferInfoUpdate, self.initinfFun)
end

function BossOfferPanel:RemoveEventListeners()   
    EventDispatcher:AddEventListener(GameEvents.BossOfferGetResp, self.updataFun)
    EventDispatcher:RemoveEventListener(GameEvents.BossOfferStateUpdate, self.updataFun)
    EventDispatcher:RemoveEventListener(GameEvents.BossOfferInfoUpdate, self.initinfFun)
end

function BossOfferPanel:InitFuns()
    self.updataFun = function()self:Update()end
    self.initinfFun = function()self:InitInfo()end
end

function BossOfferPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end
