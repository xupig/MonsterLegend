--BossOfferModule.lua
BossOfferModule = {}
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
function BossOfferModule.Init()
      GUIManager.AddPanel(PanelsConfig.BossOffer,"Panel_BossOffer",GUILayer.LayerUIPanel,"Modules.ModuleBossOffer.BossOfferPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end
return BossOfferModule