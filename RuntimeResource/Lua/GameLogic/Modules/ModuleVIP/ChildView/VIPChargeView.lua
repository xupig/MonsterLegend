require "Modules.ModuleVIP.ChildComponent.ChargeListItem"

local VIPChargeView = Class.VIPChargeView(ClassTypes.BaseLuaUIComponent)

VIPChargeView.interface = GameConfig.ComponentsConfig.Component
VIPChargeView.classPath = "Modules.ModuleVIP.ChildView.VIPChargeView"

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local ChargeListItem = ClassTypes.ChargeListItem
local ChargeDataHelper = GameDataHelper.ChargeDataHelper

function VIPChargeView:Awake()
    self:InitView()
end
--override
function VIPChargeView:OnDestroy() 

end

function VIPChargeView:OnEnable()
    self._refreshChargeList = function() self:UpdateView() end
    EventDispatcher:AddEventListener(GameEvents.On_Refresh_Charge_List, self._refreshChargeList)
end

function VIPChargeView:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.On_Refresh_Charge_List, self._refreshChargeList)
end

function VIPChargeView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(ChargeListItem)
    self._list:SetPadding(10, 0, 0, 20)
    self._list:SetGap(10, 20)
    self._list:SetDirection(UIList.DirectionTopToDown, 4, 1)
	local itemClickedCB = 
	function(idx)
		self:OnListItemClicked(idx)
	end
	self._list:SetItemClickedCB(itemClickedCB)
end

function VIPChargeView:OnListItemClicked(idx)
    self._list:GetItem(idx):ShowSelected(true)
end

function VIPChargeView:UpdateView()
    self._listData = ChargeDataHelper:GetShowChargeData()
    self._list:SetDataList(self._listData)
end

function VIPChargeView:ShowView()
    self:UpdateView()
end

function VIPChargeView:CloseView()
    
end