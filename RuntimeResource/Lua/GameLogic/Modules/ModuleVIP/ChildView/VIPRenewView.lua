-- VIPRenewView.lua
--续费弹窗
local VIPRenewView = Class.VIPRenewView(ClassTypes.BaseComponent)

VIPRenewView.interface = GameConfig.ComponentsConfig.Component
VIPRenewView.classPath = "Modules.ModuleVIP.ChildView.VIPRenewView"

require "Modules.ModuleVIP.ChildComponent.VIPRenewCardItem"
local VIPRenewCardItem = ClassTypes.VIPRenewCardItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function VIPRenewView:Awake()
	self:InitComps()
end
function VIPRenewView:ShowView()
end

function VIPRenewView:CloseView()
    
end
function VIPRenewView:InitComps()
	self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnClickClose() end)

    self._cards = {}
    local cardIds = GlobalParamsHelper.GetParamValue(544)
    for i=1,3 do
    	local card = self:AddChildLuaUIComponent("Container_Cards/Container_Item"..i,VIPRenewCardItem)
    	card:UpdateData(cardIds[i])
    	self._cards[i] = card
    end
end

function VIPRenewView:OnClickClose()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_CLOSE_RENEW)
end

function VIPRenewView:OnEnable()
end

function VIPRenewView:OnDisable()
end