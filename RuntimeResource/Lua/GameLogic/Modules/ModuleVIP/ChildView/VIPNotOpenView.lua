-- VIPNotOpenView.lua
local VIPNotOpenView = Class.VIPNotOpenView(ClassTypes.BaseComponent)

VIPNotOpenView.interface = GameConfig.ComponentsConfig.Component
VIPNotOpenView.classPath = "Modules.ModuleVIP.ChildView.VIPNotOpenView"

require "Modules.ModuleVIP.ChildComponent.VIPCardItem"

local VIPCardItem = ClassTypes.VIPCardItem

local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local XArtNumber = GameMain.XArtNumber
local ShopManager = PlayerManager.ShopManager
local ShopDataHelper = GameDataHelper.ShopDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ActorModelComponent = GameMain.ActorModelComponent
local TransformDataHelper = GameDataHelper.TransformDataHelper
local XImageFlowLight = GameMain.XImageFlowLight

function VIPNotOpenView:Awake()
    self:InitCallBack()
	self:InitComps()
end

function VIPNotOpenView:InitCallBack()
	self._selectCardCb = function (index)
		self:OnSelectCard(index)
	end
end

function VIPNotOpenView:InitComps()
    self._containrPrivilege = {}
    self._containrPrivilege[1] = self:FindChildGO("Container_Privilege1")
    self._containrPrivilege[2] = self:FindChildGO("Container_Privilege2")
    self._containrPrivilege[3] = self:FindChildGO("Container_Privilege3")

    self._containerModel = self:FindChildGO("Container_Privilege1/Container_Model")
    self._modelComponent = self:AddChildComponent('Container_Privilege1/Container_Model', ActorModelComponent)

    local transformCfgId = tonumber(GlobalParamsHelper.GetParamValue(930))
    local partnerTransformCfg = TransformDataHelper.GetTreasureTotalCfg(transformCfgId)
    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local uiModelCfgId = partnerTransformCfg.model_vocation[vocationGroup]
    local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    
    local modelId = modelInfo.model_ui
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
    self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    self._modelComponent:LoadModel(modelId)

    self._cards = {}
    self._shopCfgList = {}
    self._cardIds = GlobalParamsHelper.GetParamValue(544)
    for i=1,3 do
    	local card = self:AddChildLuaUIComponent("Container_Cards/Container_Item"..i,VIPCardItem)
    	card:SetIndex(i)
    	card:UpdateData(self._cardIds[i])
    	self._cards[i] = card
    	self._shopCfgList[i] = ShopDataHelper:GetShopDataByItemId(self._cardIds[i])[1]
    end

    self._btnOpen = self:FindChildGO("Button_Open")
   	self._csBH:AddClick(self._btnOpen, function()self:OnClickOpen() end)
   	self._txtOpen = self:GetChildComponent("Button_Open/Text",'TextMeshWrapper')
   	self._goButtonFlowLight = self:AddChildComponent("Button_Open", XImageFlowLight)

   	self._btnPrivilege = self:FindChildGO("Button_Privilege")
   	self._csBH:AddClick(self._btnPrivilege, function()self:OnClickCheckPrivilege() end)
   	local txtCheck = self:GetChildComponent("Button_Privilege/Text",'TextMeshWrapper')
   	txtCheck.text = "<u>"..LanguageDataHelper.CreateContent(612).."</u>"
   	--self._vipStraightLevels = GlobalParamsHelper.GetParamValue(546)
end

function VIPNotOpenView:OnSelectCard(index)
	if self._selectedCard then
		self._selectedCard:SetSelected(false)
	end
	self._selectedCard = self._cards[index]
	self._selectedCard:SetSelected(true)

	self._selectedCardId = self._cardIds[index]
	self._shopCfg = self._shopCfgList[index]
	self._txtOpen.text = LanguageDataHelper.CreateContent(608+index)
    
	for i=1,3 do
		if i == index then
			self._containrPrivilege[i]:SetActive(true)
		else
			self._containrPrivilege[i]:SetActive(false)
		end
	end
end

function VIPNotOpenView:OnClickOpen()
	if self._shopCfg then
		ShopManager:BuyShopItem(self._shopCfg.__id,1,1)
	end
end

function VIPNotOpenView:OnClickCheckPrivilege()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_SHOW_PRIVILEGE,true,self._selectedCardId)
end

function VIPNotOpenView:OnEnable()
	self:OnSelectCard(1)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SELECT_VIP_CARD,self._selectCardCb)
end

function VIPNotOpenView:OnDisable()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SELECT_VIP_CARD,self._selectCardCb)
end

function VIPNotOpenView:ShowModel()
	self._containerModel:SetActive(true)
end

function VIPNotOpenView:HideModel()
	if self._containerModel then
		self._containerModel:SetActive(false)
	end
end