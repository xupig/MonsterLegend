-- VIPView.lua
local VIPView = Class.VIPView(ClassTypes.BaseComponent)

VIPView.interface = GameConfig.ComponentsConfig.Component
VIPView.classPath = "Modules.ModuleVIP.ChildView.VIPView"

require "Modules.ModuleVIP.ChildView.VIPNotOpenView"
require "Modules.ModuleVIP.ChildView.VIPOpenView"
require "Modules.ModuleVIP.ChildView.VIPPrivilegeView"

local VIPNotOpenView = ClassTypes.VIPNotOpenView
local VIPOpenView = ClassTypes.VIPOpenView
local VIPPrivilegeView = ClassTypes.VIPPrivilegeView

function VIPView:Awake()
    self:InitCallBack()
	self:InitComps()
end

function VIPView:InitCallBack()
	self._onVIPChange = function ()
		self:UpdateView()
	end

	self._onShowPrivilege = function (shouldShow,cardId)
		self:ShowPrivilege(shouldShow,cardId)
	end
end

function VIPView:ShowView()
	self:UpdateView()
end

function VIPView:InitComps()
    self._vipNotOpenView 	= self:AddChildLuaUIComponent("Container_NotOpen", VIPNotOpenView)
	self._vipOpenView 		= self:AddChildLuaUIComponent("Container_Open", VIPOpenView)
	self._vipPrivilegeView 		= self:AddChildLuaUIComponent("Container_Privilege", VIPPrivilegeView)
end

function VIPView:UpdateView()
	--LoggerHelper.Error("VIPView:UpdateView()")
	local vipLevel = GameWorld.Player().vip_level or 0
	local vipReal = GameWorld.Player().vip_real or 0

	if vipLevel > 0 and vipReal > 0 then
		self._vipNotOpenView:SetActive(false)
		self._vipOpenView:SetActive(true)
		self._vipOpenView:UpdateView()
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_LEFT_BG_CHANGE,2)
	else
		self._vipNotOpenView:SetActive(true)
		self._vipNotOpenView:ShowModel()
		self._vipOpenView:SetActive(false)
		EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_LEFT_BG_CHANGE,1)
	end
end

function VIPView:ShowPrivilege(shouldShow,cardId)
	self._vipPrivilegeView:SetActive(shouldShow)
	if shouldShow then
		self._vipPrivilegeView:UpdateView(cardId)
		self._vipNotOpenView:HideModel()
	else
		self._vipNotOpenView:ShowModel()
	end
end

function VIPView:CloseView()
	self._vipNotOpenView:HideModel()
end

function VIPView:OnEnable()
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onVIPChange)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_VIP_SHOW_PRIVILEGE,self._onShowPrivilege)
end

function VIPView:OnDisable()
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_LEVEL, self._onVIPChange)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_VIP_SHOW_PRIVILEGE,self._onShowPrivilege)
end