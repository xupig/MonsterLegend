-- VIPPrivilegeView.lua
local VIPPrivilegeView = Class.VIPPrivilegeView(ClassTypes.BaseComponent)

VIPPrivilegeView.interface = GameConfig.ComponentsConfig.Component
VIPPrivilegeView.classPath = "Modules.ModuleVIP.ChildView.VIPPrivilegeView"

require "Modules.ModuleVIP.ChildComponent.VIPPrivilegeListItem"
local UIList = ClassTypes.UIList
local VIPPrivilegeListItem = ClassTypes.VIPPrivilegeListItem
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

function VIPPrivilegeView:Awake()
    self:InitCallBack()
	self:InitComps()
end


function VIPPrivilegeView:InitCallBack()
end

function VIPPrivilegeView:InitComps()
	self._vipCardPrivilege = GlobalParamsHelper.GetParamValue(545)
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Privilege")
    self._listPrivilege = list
    self._scrollViewGemBag = scrollView
    self._listPrivilege:SetItemType(VIPPrivilegeListItem)
    self._listPrivilege:SetPadding(10, 0, 0, 0)
    self._listPrivilege:SetGap(1, 0)
    self._listPrivilege:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._iconBg = self:FindChildGO("Container_Bg")
    GameWorld.AddIcon(self._iconBg,13512)

    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnClose() end)
end


function VIPPrivilegeView:UpdateView(cardId)
	local privilegeData = self._vipCardPrivilege[cardId]
	self._listPrivilege:SetDataList(privilegeData)
end

function VIPPrivilegeView:OnClose()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_SHOW_PRIVILEGE,false)
end