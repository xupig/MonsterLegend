-- VIPOpenView.lua
local VIPOpenView = Class.VIPOpenView(ClassTypes.BaseComponent)

VIPOpenView.interface = GameConfig.ComponentsConfig.Component
VIPOpenView.classPath = "Modules.ModuleVIP.ChildView.VIPOpenView"

require "Modules.ModuleVIP.ChildView.VIPRenewView"
local VIPRenewView = ClassTypes.VIPRenewView

require "Modules.ModuleVIP.ChildComponent.VIPPrivilegeListItem"
require "Modules.ModuleVIP.ChildComponent.VIPPreviewListItem"
require "UIComponent.Extend.ItemGridEx"

local ItemGridEx = ClassTypes.ItemGridEx
local VIPPreviewListItem = ClassTypes.VIPPreviewListItem
local VIPPrivilegeListItem = ClassTypes.VIPPrivilegeListItem
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local VIPDataHelper = GameDataHelper.VIPDataHelper
local XArtNumber = GameMain.XArtNumber
local UIComponentUtil = GameUtil.UIComponentUtil
local VIPManager = PlayerManager.VIPManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TitleDataHelper = GameDataHelper.TitleDataHelper
local Vector2 = Vector2

function VIPOpenView:Awake()
    self:InitCallBack()
	self:InitComps()
end

function VIPOpenView:InitCallBack()
	self._closeRenewCb = function ()
		self:CloseRenew()
	end

	self._updateAwardCb = function ()
		if self._selectedLevel then
			self:UpdateAward(self._selectedLevel)
		end
	end

	self._updateViewCb = function ()
		self:UpdateView()
	end
end

function VIPOpenView:ShowView()
	self:UpdateView()
end

function VIPOpenView:CloseView()
    
end
function VIPOpenView:InitComps()
	self._expProgress = UIScaleProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Exp")
    self._expProgress:SetProgress(0)
    self._expProgress:ShowTweenAnimate(true)
    self._expProgress:SetMutipleTween(true)
    self._expProgress:SetIsResetToZero(false)
    self._expProgress:SetTweenTime(0.5)

    -- self._containerTitle = self:FindChildGO("Container_Title")
    -- self._titleMap = GlobalParamsHelper.GetParamValue(851)

	self._btnRenew = self:FindChildGO("Button_Renew")
    self._csBH:AddClick(self._btnRenew, function() self:OnClickRenew() end)

    self._renewView = self:AddChildLuaUIComponent("Container_Renew", VIPRenewView)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Privilege")
    self._listPrivilege = list
    self._scrollViewGemBag = scrollView
    self._listPrivilege:SetItemType(VIPPrivilegeListItem)
    self._listPrivilege:SetPadding(10, 0, 0, 0)
    self._listPrivilege:SetGap(1, 0)
    self._listPrivilege:SetDirection(UIList.DirectionTopToDown, 1, 1)

    local scrollPage, pageableList = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_VIP")
	self._pageableList = pageableList
	self._scrollPage = scrollPage
	self._maxLevel = VIPDataHelper:GetMaxVIPLevel()
	local vipDataList = {}
	--LoggerHelper.Error("self._maxLevel"..self._maxLevel)
	for i=1,self._maxLevel do
		vipDataList[i] = i
	end
	-- local pageNum = math.ceil(self._maxLevel/10)
	-- self._scrollPage:SetTotalPage(pageNum) --必须设置正确的总页数
    self._pageableList:SetItemType(VIPPreviewListItem) --每个List有独立对应的Item类进行匹配
    self._pageableList:SetPadding(0, 0, 0, 0); --设置List离边框的间距，可用于后期调整List在父界面中的位置
    self._pageableList:SetGap(10, -1); --设置item间的横竖间距
    self._pageableList:SetDirection(UIList.DirectionTopToDown, 1 ,self._maxLevel); --设置item的填充方向
	self._pageableList:SetDataList(vipDataList)
	local itemClickedCB =                --设置按钮点击的回调
	function(idx)
		--LoggerHelper.Log("InitScrollPageList idx:"..idx)
		self:OnVIPListItemClicked(idx)
	end
	self._pageableList:SetItemClickedCB(itemClickedCB)

	self._curVipNumber = self:AddChildComponent('Container_CurVIP', XArtNumber)

	self._nextVipNumber = self:AddChildComponent('Container_NextVIP', XArtNumber)
	self._nextVipNumber:SetXGap(-2)
	self._awardVipNumber = self:AddChildComponent('Container_AwardVIP', XArtNumber)
	self._awardVipNumber:SetXGap(-2)
	self._awardVipNumberTrans = self:FindChildGO('Container_AwardVIP').transform
	--self._txtAwardTitle = self:GetChildComponent("Text_AwardTitle",'TextMeshWrapper')
	self._txtTimeLeft = self:GetChildComponent("Text_TimeLeft",'TextMeshWrapper')

	self._awardList = {}
	for i=1,4 do
		local awardItem = self:AddChildLuaUIComponent("Container_Awards/Container_Item"..i,ItemGridEx)
		awardItem:SetShowBagNum(false)
		awardItem:ActivateTips()
		self._awardList[i] = awardItem
	end

	self._btnGetAward = self:FindChildGO("Button_GetAward")
	self._btnGetAwardWrapper = self:GetChildComponent("Button_GetAward",'ButtonWrapper')
	self._txtBtnGetAward = self:GetChildComponent("Button_GetAward/Text",'TextMeshWrapper')
    self._csBH:AddClick(self._btnGetAward, function() self:OnClickGetAward() end)
    self._imgAwardGot = self:FindChildGO("Image_AwardGot")

    self._imgAwardTitle1 = self:FindChildGO("Image_AwardTitle1")
    self._imgAwardTitle2 = self:FindChildGO("Image_AwardTitle2")
end

function VIPOpenView:OnVIPListItemClicked(idx)
	if self._selectedListItem then
		self._selectedListItem:SetSelected(false)
	end
	self._selectedListItem = self._pageableList:GetItem(idx)
	self._selectedListItem:SetSelected(true)

	self._selectedLevel = idx + 1
	self:UpdatePrivilege(self._selectedLevel)
	self:UpdateAward(self._selectedLevel)
end

--更新特权信息
function VIPOpenView:UpdatePrivilege(vipLevel)
	local privilegeData = VIPDataHelper:GetPrivilege(vipLevel)
	self._listPrivilege:SetDataList(privilegeData)

	-- local icon =  TitleDataHelper.GetTitleShowName(self._titleMap[vipLevel])
	-- GameWorld.AddIconAnim(self._containerTitle, icon)
 --    GameWorld.SetIconAnimSpeed(self._containerTitle, 5)

 	self._nextVipNumber:SetNumber(vipLevel)
 	self._awardVipNumber:SetNumber(vipLevel)
 	if vipLevel > 9 then
 		self._awardVipNumberTrans.anchoredPosition = Vector2.New(1005,355)
 	else
 		self._awardVipNumberTrans.anchoredPosition = Vector2.New(1012,355)
 	end
end

function VIPOpenView:UpdateView()
	local playerVipLevel = GameWorld.Player().vip_level
	local vipExp = GameWorld.Player().vip_exp or 0
	local nextExp = VIPDataHelper:GetNextExp(playerVipLevel)
	self._expProgress:SetProgressByValue(vipExp,nextExp,true)

	self._curVipNumber:SetNumber(playerVipLevel)

	self:UpdateTimeLeft()
	self:OnVIPListItemClicked(playerVipLevel - 1)
	self:UpdatePrivilege(playerVipLevel)
	self:UpdateAward(playerVipLevel)
end

function VIPOpenView:UpdateTimeLeft()
	local endTime = GameWorld.Player().vip_endtime
	local nowTime = DateTimeUtil.GetServerTime()
	local dTime = math.max(0,endTime - nowTime)
	local dDay = math.ceil(dTime/DateTimeUtil.OneDayTime)

	--LoggerHelper.Error("endTime"..endTime)
	--LoggerHelper.Error("nowTime"..nowTime)
	--DateTimeUtil.OneDayTime
	self._txtTimeLeft.text = dDay..LanguageDataHelper.CreateContent(147)
end

function VIPOpenView:UpdateAward(vipLevel)
	local levelAward = GameWorld.Player().vip_level_reward[vipLevel]
	local dailyAward = GameWorld.Player().vip_daily_reward or 0
	local playerVipLevel = GameWorld.Player().vip_level

	local awardData
	local args = {["0"] = vipLevel}
	self._awardType = 0

	--还没达到
	if playerVipLevel < vipLevel then
		awardData = VIPDataHelper:GetLevelReward(vipLevel)
		self._btnGetAward:SetActive(true)
		self._imgAwardGot:SetActive(false)
		self._btnGetAwardWrapper.interactable  = false
		self._txtBtnGetAward.text = LanguageDataHelper.CreateContent(40035)
		self._imgAwardTitle1:SetActive(true)
		self._imgAwardTitle2:SetActive(false)
		--self._txtAwardTitle.text = LanguageDataHelper.CreateContentWithArgs(512,args)
	else
		--等级礼包没领
		if levelAward == nil then
			awardData = VIPDataHelper:GetLevelReward(vipLevel)
			self._btnGetAwardWrapper.interactable = true
			self._btnGetAward:SetActive(true)
			self._imgAwardGot:SetActive(false)
			self._txtBtnGetAward.text = LanguageDataHelper.CreateContent(40037)
			self._imgAwardTitle1:SetActive(true)
			self._imgAwardTitle2:SetActive(false)
			--self._txtAwardTitle.text = LanguageDataHelper.CreateContentWithArgs(512,args)
			self._awardType = 1
		else
			--当前等级的每日礼包
			if vipLevel == playerVipLevel then
				if dailyAward < vipLevel then
					awardData = VIPDataHelper:GetDailyReward(vipLevel)
					self._btnGetAwardWrapper.interactable = true
					self._btnGetAward:SetActive(true)
					self._imgAwardGot:SetActive(false)
					self._txtBtnGetAward.text = LanguageDataHelper.CreateContent(40037)
					self._imgAwardTitle1:SetActive(false)
					self._imgAwardTitle2:SetActive(true)
					--self._txtAwardTitle.text = LanguageDataHelper.CreateContentWithArgs(513,args)
					self._awardType = 2
				else
					awardData = VIPDataHelper:GetDailyReward(vipLevel)
					self._btnGetAward:SetActive(false)
					self._imgAwardGot:SetActive(true)
					self._imgAwardTitle1:SetActive(false)
					self._imgAwardTitle2:SetActive(true)
					--self._txtAwardTitle.text = LanguageDataHelper.CreateContentWithArgs(513,args)
				end
			--其他等级礼包
			else
				awardData = VIPDataHelper:GetLevelReward(vipLevel)
				self._btnGetAward:SetActive(false)
				self._imgAwardGot:SetActive(true)
				self._imgAwardTitle1:SetActive(true)
				self._imgAwardTitle2:SetActive(false)
				--self._txtAwardTitle.text = LanguageDataHelper.CreateContentWithArgs(512,args)
			end
		end
	end
	
	for i=1,#awardData do
		local item = self._awardList[i]
		self._awardList[i]:SetActive(true)
		self._awardList[i]:UpdateData(awardData[i][1],awardData[i][2]) 
	end

	for i=#awardData+1,4 do
		self._awardList[i]:SetActive(false)
	end

	local redPointState = VIPManager:GetAwardRedPointStates()
	for i=1,self._maxLevel do
		local vipItem = self._pageableList:GetItem(i-1)
		vipItem:SetRedPoint(redPointState[i])
	end
end

function VIPOpenView:OnClickGetAward()
	if self._awardType == 1 then
		--LoggerHelper.Error("OnClickGetAward()"..self._selectedLevel)
		VIPManager:RequestLevelReward(self._selectedLevel)
	elseif self._awardType == 2 then
		VIPManager:RequestDailyReward()
	end
end

function VIPOpenView:OnClickRenew()
	self._renewView:SetActive(true)
end

function VIPOpenView:CloseRenew()
	self._renewView:SetActive(false)
end

function VIPOpenView:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_VIP_CLOSE_RENEW,self._closeRenewCb)
	EventDispatcher:AddEventListener(GameEvents.VIP_LEVEL_REWARD_UPDATE,self._updateAwardCb)
	EventDispatcher:AddEventListener(GameEvents.VIP_DAILY_REWARD_UPDATE,self._updateAwardCb)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_VIP_ENDTIME, self._updateViewCb)
end

function VIPOpenView:OnDisable()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_VIP_CLOSE_RENEW,self._closeRenewCb)
	EventDispatcher:RemoveEventListener(GameEvents.VIP_LEVEL_REWARD_UPDATE,self._updateAwardCb)
	EventDispatcher:RemoveEventListener(GameEvents.VIP_DAILY_REWARD_UPDATE,self._updateAwardCb)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_VIP_ENDTIME, self._updateViewCb)
end