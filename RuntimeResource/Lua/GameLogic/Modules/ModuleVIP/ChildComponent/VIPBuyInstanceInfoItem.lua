-- VIPBuyInstanceInfoItem.lua
local VIPBuyInstanceInfoItem = Class.VIPBuyInstanceInfoItem(ClassTypes.BaseComponent)

VIPBuyInstanceInfoItem.interface = GameConfig.ComponentsConfig.PointableComponent
VIPBuyInstanceInfoItem.classPath = "Modules.ModuleVIP.ChildComponent.VIPBuyInstanceInfoItem"

local XArtNumber = GameMain.XArtNumber

--override
function VIPBuyInstanceInfoItem:Awake()
    self._buyText = self:GetChildComponent("Text_Buy_Info",'TextMeshWrapper')    
    self._vipNumber = self:AddChildComponent('Container_Vip_Num', XArtNumber)
end

--override
function VIPBuyInstanceInfoItem:OnDestroy() 
    self._buyText = nil
    self._vipNumber = nil
end

function VIPBuyInstanceInfoItem:SetValues(vip, buyText)
    self._vipNumber:SetNumber(vip)
    self._buyText.text = buyText
end
