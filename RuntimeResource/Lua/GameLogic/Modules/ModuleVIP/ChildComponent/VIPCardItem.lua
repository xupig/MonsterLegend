-- VIPCardItem.lua
local VIPCardItem = Class.VIPCardItem(ClassTypes.BaseComponent)

VIPCardItem.interface = GameConfig.ComponentsConfig.PointableComponent
VIPCardItem.classPath = "Modules.ModuleVIP.ChildComponent.VIPCardItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local ShopDataHelper = GameDataHelper.ShopDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

function VIPCardItem:Awake()
    self:InitCallBack()
	self:InitComps()
end

function VIPCardItem:InitCallBack()
end

function VIPCardItem:InitComps()
	self._imgBg = self:FindChildGO("Image_Bg")
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected1 = self:FindChildGO("Image_Selected1")
	local moneyIcon = self:FindChildGO("Container_Icon")
    GameWorld.AddIcon(moneyIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    self._txtMoneyCount = self:GetChildComponent("Text_Count",'TextMeshWrapper')

    local parent = self:FindChildGO("Container_ItemIcon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
end

function VIPCardItem:UpdateData(itemId)
	self._shopCfg = ShopDataHelper:GetShopDataByItemId(itemId)[1]
	local type, price = ShopDataHelper:GetItemTypeAndUnitPrice(self._shopCfg.__id)
	self._txtMoneyCount.text = tostring(price)
	self._itemIcon:SetItem(itemId)
	if self._index > 1 then
		self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
		self._txtName.text = ItemDataHelper.GetItemNameWithColor(itemId)
	end
end

function VIPCardItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
	self._imgSelected1:SetActive(isSelected)
	self._imgBg:SetActive(not isSelected)
end

function VIPCardItem:SetIndex(index)
	self._index = index
end

function VIPCardItem:OnPointerDown()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_VIP_CARD,self._index)
end

function VIPCardItem:OnPointerUp()
	
end