-- VIPRenewCardItem.lua
local VIPRenewCardItem = Class.VIPRenewCardItem(ClassTypes.BaseComponent)

VIPRenewCardItem.interface = GameConfig.ComponentsConfig.Component
VIPRenewCardItem.classPath = "Modules.ModuleVIP.ChildComponent.VIPRenewCardItem"

local ShopManager = PlayerManager.ShopManager
local ShopDataHelper = GameDataHelper.ShopDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config

function VIPRenewCardItem:Awake()
	self:InitComps()
end

function VIPRenewCardItem:InitComps()
	self._btnBuy = self:FindChildGO("Button_Pay")
    self._csBH:AddClick(self._btnBuy, function() self:OnBuy() end)

    local moneyIcon = self:FindChildGO("Button_Pay/Container_Icon")
    GameWorld.AddIcon(moneyIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
    self._txtMoneyCount = self:GetChildComponent("Button_Pay/Text_Count",'TextMeshWrapper')
    self._txtCardName = self:GetChildComponent("Text_CardName",'TextMeshWrapper')
end

function VIPRenewCardItem:UpdateData(itemId)
	self._shopCfg = ShopDataHelper:GetShopDataByItemId(itemId)[1]
	local type, price = ShopDataHelper:GetItemTypeAndUnitPrice(self._shopCfg.__id)
	self._txtMoneyCount.text = tostring(price)
	self._txtCardName.text = ItemDataHelper.GetItemNameWithColor(itemId)
end

function VIPRenewCardItem:OnBuy()
	ShopManager:BuyShopItem(self._shopCfg.__id,1,0)
	--EventDispatcher:TriggerEvent(GameEvents.UIEVENT_VIP_CLOSE_RENEW)
end

function VIPRenewCardItem:OnEnable()
end

function VIPRenewCardItem:OnDisable()
end