--VIPPrivilegeListItem.lua
local UIListItem = ClassTypes.UIListItem
local VIPPrivilegeListItem = Class.VIPPrivilegeListItem(UIListItem)
VIPPrivilegeListItem.interface = GameConfig.ComponentsConfig.Component
VIPPrivilegeListItem.classPath = "Modules.ModuleVIP.ChildComponent.VIPPrivilegeListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function VIPPrivilegeListItem:Awake()
    self._base.Awake(self)
    self._txtDesc = self:GetChildComponent("Text_Desc",'TextMeshWrapper')
end

function VIPPrivilegeListItem:OnDestroy()
	self._txtDesc = nil
end

--override
function VIPPrivilegeListItem:OnRefreshData(data)
	local tab = {}
	-- table.insert(tab,LanguageDataHelper.CreateContent(613))
	-- table.insert(tab,tostring(self:GetIndex()+1))
	table.insert(tab,"◆")
	table.insert(tab,LanguageDataHelper.CreateContent(data))
	self._txtDesc.text = table.concat(tab)
end