local ChargeListItem = Class.ChargeListItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
ChargeListItem.interface = GameConfig.ComponentsConfig.Component
ChargeListItem.classPath = "Modules.ModuleVIP.ChildComponent.ChargeListItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ChargeDataHelper = GameDataHelper.ChargeDataHelper
local QualityType = GameConfig.EnumType.QualityType
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WeekCardManager = PlayerManager.WeekCardManager

local QUALITY_TO_ICON = {[QualityType.Green] = 4120, [QualityType.Blue] = 4121, [QualityType.Purple] = 4122, [QualityType.Orange] = 4123,
    [QualityType.Red] = 4124, [QualityType.Pink] = 4125}

function ChargeListItem:Awake()
    self:InitView()
end

function ChargeListItem:OnDestroy()

end

function ChargeListItem:InitView()
    self._lightIcon = self:FindChildGO("Container_Icon")
    self._iconContainer = self:FindChildGO("Container_Item")
    
    self.nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self.priceText = self:GetChildComponent("Container_ItemPrice/Text_Price", "TextMeshWrapper")

    self._firstRewardGO = self:FindChildGO("Container_FirstReward")
    self._firstRewardText = self:GetChildComponent("Container_FirstReward/Text_Count", "TextMeshWrapper")
    
    self._redPointImager = self:FindChildGO("Image_RedPoint")
    self._redPointImager:SetActive(false)
end

--override {[id]}
function ChargeListItem:OnRefreshData(data)
    self.data = data
    self:ShowDesc(data.id)
end

function ChargeListItem:ShowSelected(state)
    local isWeekCard = ChargeDataHelper:GetWeekCard(self.data.id) == 1
    local state = WeekCardManager:GetWeekCardState()
    if isWeekCard then
        if state == 0 then
            self:OnCharge(self.data.id)
        elseif state == 1 then
            WeekCardManager:GetWeekCradRewardReq()
        end
    else
        self:OnCharge(self.data.id)
    end
end

function ChargeListItem:OnCharge(id)
    local money = ChargeDataHelper:GetMoney(id) * 100
    if GameLoader.SystemConfig.IsChargeTest then
        money = 1
    end
    -- LoggerHelper.LogEx(ChargeDataHelper:GetName(id))
    local listingId = ChargeDataHelper:GetListingId(id)
    local template = GameLoader.SystemConfig.ChargeDomainName .. "roleID=%s&username=%s&serverID=%s&productID=%s&account_name=%s"
    local url = string.format(template, LocalSetting.settingData.ServerIp,  tostring(GameWorld.Player().dbid), GameWorld.Player().name, LocalSetting.settingData.SelectedServerID, listingId,GameWorld.AccountName)
    GameWorld.BuyMarkt(tostring(money), '1', ChargeDataHelper:GetName(id), '钻石', tostring(listingId), '我要购买' .. ChargeDataHelper:GetName(id), '10', url)
end

function ChargeListItem:ShowDesc(chargeId)
    local icon = ChargeDataHelper:GetIcon(chargeId)
    local money = ChargeDataHelper:GetMoney(chargeId)
    local name = ChargeDataHelper:GetName(chargeId)
    local isWeekCard = ChargeDataHelper:GetWeekCard(chargeId) == 1
    local firstBindCoupons = ChargeDataHelper:GetFirstBindCoupons(chargeId)
    GameWorld.AddIcon(self._iconContainer, icon)
    local listingId = ChargeDataHelper:GetListingId(chargeId)

    --table更新类型问题，双重取值
    local flag = GameWorld.Player().charge_listing_first_flag[tostring(listingId)]
    local isFirstBuy = (flag ~= 1)
    
    self.nameText.text = name
    if isWeekCard then
        local state = WeekCardManager:GetWeekCardState()
        if state == 0 then
            --未购买周卡
            self._redPointImager:SetActive(false)
            self.priceText.text = LanguageDataHelper.CreateContentWithArgs(58690, {["0"] = money})
        elseif state == 1 then
            --可领取
            self._redPointImager:SetActive(true)
            self.priceText.text = LanguageDataHelper.CreateContent(562)
        elseif state == 2 then
            --已领取
            self._redPointImager:SetActive(false)
            self.priceText.text = LanguageDataHelper.CreateContent(563)
        end
    else
        self.priceText.text = LanguageDataHelper.CreateContentWithArgs(58690, {["0"] = money})
        if isFirstBuy then
            self._firstRewardGO:SetActive(true)
            self._firstRewardText.text = firstBindCoupons
        else
            self._firstRewardGO:SetActive(false)
        end
    end
end
