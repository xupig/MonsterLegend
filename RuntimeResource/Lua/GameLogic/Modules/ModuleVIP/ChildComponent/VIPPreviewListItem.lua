--VIPPreviewListItem.lua
local UIListItem = ClassTypes.UIListItem
local VIPPreviewListItem = Class.VIPPreviewListItem(UIListItem)
VIPPreviewListItem.interface = GameConfig.ComponentsConfig.Component
VIPPreviewListItem.classPath = "Modules.ModuleVIP.ChildComponent.VIPPreviewListItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function VIPPreviewListItem:Awake()
    self._base.Awake(self)
    self._imgSelected = self:FindChildGO("Image_Selected")
    self._imgSelected:SetActive(false)
    self._imgBg = self:FindChildGO("Image_Bg")

    self._txtVIP = self:GetChildComponent("Text_VIP",'TextMeshWrapper')

    self._imgRedPoint = self:FindChildGO("Image_RedPoint")
end

function VIPPreviewListItem:OnDestroy()
	self._vippNumber = nil
end

--override
function VIPPreviewListItem:OnRefreshData(data)
    self._txtVIP.text = LanguageDataHelper.CreateContent(267)..data
end

function VIPPreviewListItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
	self._imgBg:SetActive(not isSelected)
end

function VIPPreviewListItem:SetRedPoint(shouldShow)
	self._imgRedPoint:SetActive(shouldShow)
end