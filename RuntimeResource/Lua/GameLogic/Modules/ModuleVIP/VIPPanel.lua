-- VIPPanel.lua
local VIPPanel = Class.VIPPanel(ClassTypes.BasePanel)
require "Modules.ModuleVIP.ChildView.VIPView"
require "Modules.ModuleVIP.ChildView.VIPChargeView"

local VIPView = ClassTypes.VIPView
local VIPChargeView = ClassTypes.VIPChargeView
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local VIPType = GameConfig.EnumType.VIPType
local VipData = PlayerManager.PlayerDataManager.vipData
local VIPType = GameConfig.EnumType.VIPType

function VIPPanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
    self:InitComps()
	self._leftBgChange = function (type)
		self:OnLeftBgChange(type)
	end
end

function VIPPanel:InitComps()  
    self:InitView("Container_VIP", VIPView,VIPType.VIP)
    self:InitView("Container_Charge", VIPChargeView,VIPType.VIPCharge) 

    self:SetTabClickCallBack(function (index)
    	self:OnTabClick(index)
    end)
end

function VIPPanel:OnShow(data)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_VIP_LEFT_BG_CHANGE,self._leftBgChange)
	self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function VIPPanel:OnTabClick(index)
	if index == 1 then
		self:RightBgIsActive(true)
	else
		self:RightBgIsActive(false)
	end
end

function VIPPanel:OnLeftBgChange(type)
	if type == 1 then
		self._win5RightBg.transform.sizeDelta = Vector2.New(306,627)
	else
		self._win5RightBg.transform.sizeDelta = Vector2.New(270,627)
	end
end

function VIPPanel:OnClose()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_VIP_LEFT_BG_CHANGE,self._leftBgChange)
end

function VIPPanel:StructingViewInit()
    return true
end

function VIPPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end