-- VIPPopPanel.lua
local VIPPopPanel = Class.VIPPopPanel(ClassTypes.BasePanel)

require "Modules.ModuleVIP.ChildView.VIPRenewView"
local VIPRenewView = ClassTypes.VIPRenewView
require "Modules.ModuleVIP.ChildComponent.VIPPrivilegeListItem"
local VIPPrivilegeListItem = ClassTypes.VIPPrivilegeListItem
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local GUIManager = GameManager.GUIManager
local BagManager = PlayerManager.BagManager
local PanelsConfig = GameConfig.PanelsConfig
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local VIPDataHelper = GameDataHelper.VIPDataHelper
local TaskCommonManager = PlayerManager.TaskCommonManager

function VIPPopPanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self:InitCallBack()
	self:InitComps()
end

--初始化回调
function VIPPopPanel:InitCallBack()
	self._closeRenewCb = function ()
		self:CloseRenew()
	end
end

function VIPPopPanel:InitComps()
	self._containerExperience = self:FindChildGO("Container_Experience")
	self._containerExpire = self:FindChildGO("Container_Expire")
	
	self._imgBgWrapper = self:GetChildComponent("Image_Bg",'ImageWrapper')
    
	self._btnExperience = self:FindChildGO("Container_Experience/Button_Experience")
    self._csBH:AddClick(self._btnExperience, function() self:OnExperience() end)

    self._btnRenew= self:FindChildGO("Container_Expire/Button_Renew")
    self._csBH:AddClick(self._btnRenew, function() self:OnClickRenew() end)

    self._btnClose= self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnClickClose() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Privilege")
    self._listPrivilege = list
    self._scrollViewGemBag = scrollView
    self._listPrivilege:SetItemType(VIPPrivilegeListItem)
    self._listPrivilege:SetPadding(10, 0, 0, 0)
    self._listPrivilege:SetGap(1, 0)
    self._listPrivilege:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._renewView = self:AddChildLuaUIComponent("Container_Renew", VIPRenewView)
    self._expCardPrivilege = GlobalParamsHelper.GetParamValue(562)
end

function VIPPopPanel:OnShow(data)
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleVIP.VIPPopPanel")

	EventDispatcher:AddEventListener(GameEvents.UIEVENT_VIP_CLOSE_RENEW,self._closeRenewCb)
	self._showType = data.showType 
	if data.showType == 1 then
		self._imgBgWrapper:SetGray(1)
		self._containerExperience:SetActive(true)
		self._containerExpire:SetActive(false)
		self._listPrivilege:SetDataList(self._expCardPrivilege)
		self._expCardId = data.cardId
		self._btnClose:SetActive(false)
		TaskCommonManager:OnPauseAutoTask()
	elseif data.showType == 2 then
		self._imgBgWrapper:SetGray(0)
		self._containerExperience:SetActive(false)
		self._containerExpire:SetActive(true)

		--取之前的VIP等级显示特权
		local vipReal = GameWorld.Player().vip_real or 0
		if vipReal == 0 then
			vipReal = 1
		end
		local privilegeData = VIPDataHelper:GetPrivilege(vipReal)
		self._listPrivilege:SetDataList(privilegeData)
		self._btnClose:SetActive(true)
	end
end

function VIPPopPanel:OnExperience()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_vip_pop_experience_button")
	local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._expCardId)
	BagManager:RequsetUse(pos,1)
	TaskCommonManager:OnReStartAutoTask()
	GUIManager.ClosePanel(PanelsConfig.VIPPop)
end

function VIPPopPanel:OnClickRenew()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_vip_pop_renew_button")
	self._renewView:SetActive(true)
end

function VIPPopPanel:CloseRenew()
	self._renewView:SetActive(false)
end

function VIPPopPanel:OnClose()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLOSE_VIEW, "Modules.ModuleVIP.VIPPopPanel")
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_VIP_CLOSE_RENEW,self._closeRenewCb)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_BAG_VIEW_SHOW_ROLE_MODEL)
end

function VIPPopPanel:OnClickClose()
	GUIManager.ClosePanel(PanelsConfig.VIPPop)
end