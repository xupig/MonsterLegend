-- VIPModule.lua
VIPModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function VIPModule.Init()
    GUIManager.AddPanel(PanelsConfig.VIP, "Panel_VIP", GUILayer.LayerUIPanel, "Modules.ModuleVIP.VIPPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)    
    GUIManager.AddPanel(PanelsConfig.VIPPop, "Panel_VIPPop", GUILayer.LayerUIPanel, "Modules.ModuleVIP.VIPPopPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.VIPBuyInstancePanel, "Panel_BuyInstanceTimes", GUILayer.LayerUICommon, "Modules.ModuleVIP.VIPBuyInstancePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return VIPModule