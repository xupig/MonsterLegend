-- VIPBuyInstancePanel.lua
local VIPBuyInstancePanel = Class.VIPBuyInstancePanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local VIPDataHelper = GameDataHelper.VIPDataHelper
local VipConfig = GameConfig.VipConfig
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager

require "Modules.ModuleVIP.ChildComponent.VIPBuyInstanceInfoItem"
local VIPBuyInstanceInfoItem = ClassTypes.VIPBuyInstanceInfoItem

function VIPBuyInstancePanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self:InitCallBack()
	self:InitViews()
end

--初始化回调
function VIPBuyInstancePanel:InitCallBack()
end

function VIPBuyInstancePanel:InitViews()
    self._numText = self:GetChildComponent("Container_PopupSmall/Container_Content/Text_Buy",'TextMeshWrapper')

    self._nextImage = self:FindChildGO("Container_PopupSmall/Container_Content/Image_Next")
    self._nextBuyInfoContainer = self:FindChildGO("Container_PopupSmall/Container_Content/Container_Message/Container_Message_Next")
    self._nowBuyInfoContainer = self:FindChildGO("Container_PopupSmall/Container_Content/Container_Message/Container_Message_Now")

    self._nowVip = UIComponentUtil.AddLuaUIComponent(self._nowBuyInfoContainer, VIPBuyInstanceInfoItem)
    self._nextVip = UIComponentUtil.AddLuaUIComponent(self._nextBuyInfoContainer, VIPBuyInstanceInfoItem)

    self._cancelButton = self:FindChildGO("Container_PopupSmall/Button_Cancel")
    self._csBH:AddClick(self._cancelButton, function() self:OnCancleButtonClick() end)

    self._buyButton = self:FindChildGO("Container_PopupSmall/Button_Buy")
    self._csBH:AddClick(self._buyButton, function() self:OnBuyButtonClick() end)

    self._closeButton = self:FindChildGO("Container_PopupSmall/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)
end

--data {}
--type:VipConfig.VIP_BUY_TIMES_TYPE_MAP
--cb:购买的回调函数
function VIPBuyInstancePanel:OnShow(data)
    local vipLevel = GameWorld.Player().vip_level
    local type = data.type
    local nowEnterTimes = VIPDataHelper:GetButTimesByType(type)
    -- LoggerHelper.Error("nowEnterTimes ===" .. tostring(nowEnterTimes))
    -- LoggerHelper.Error("VipConfig.VIP_BUY_TIMES_MAP[type] ===" .. tostring(VipConfig.VIP_BUY_TIMES_MAP[type](type)))
    -- LoggerHelper.Error("GameWorld.Player().holy_tower_info ===" .. PrintTable:TableToStr(GameWorld.Player().holy_tower_info ))
    local canBuyNum = nowEnterTimes - VipConfig.VIP_BUY_TIMES_MAP[type](type)
    self._cb = data.cb
    self._numText.text = LanguageDataHelper.CreateContent(518, {
        ["0"] = VipConfig.VIP_BUY_PRICE_MAP[type],
        ["1"] = 1
    })

    self._nowVip:SetValues(vipLevel, LanguageDataHelper.CreateContent(522,{
        ["0"] = canBuyNum .. "/" .. nowEnterTimes
    }))

    local vip, times = VIPDataHelper:GetNextBuyTimesAndVip(vipLevel, type)
    if vip == nil or times == nil then
        self._nextImage:SetActive(false)
        self._nextBuyInfoContainer:SetActive(false)
    else
        self._nextImage:SetActive(true)
        self._nextBuyInfoContainer:SetActive(true)
        self._nextVip:SetValues(vip, LanguageDataHelper.CreateContent(522,{
            ["0"] = times
        }))
    end
end

function VIPBuyInstancePanel:OnClose()
end

function VIPBuyInstancePanel:OnDisable()

end

function VIPBuyInstancePanel:OnCancleButtonClick()
	GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
end

function VIPBuyInstancePanel:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.VIPBuyInstancePanel)
end

function VIPBuyInstancePanel:OnBuyButtonClick()
    self._cb()
end