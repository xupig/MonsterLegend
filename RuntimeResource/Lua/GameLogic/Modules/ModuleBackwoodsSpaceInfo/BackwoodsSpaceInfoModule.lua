BackwoodsSpaceInfoModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function BackwoodsSpaceInfoModule.Init()
    GUIManager.AddPanel(PanelsConfig.BackwoodsSpaceInfo,"Panel_BackwoodsSpaceInfo",GUILayer.LayerUIMiddle,"Modules.ModuleBackwoodsSpaceInfo.BackwoodsSpaceInfoPanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return BackwoodsSpaceInfoModule