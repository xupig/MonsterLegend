local BackwoodsSpaceInfoPanel = Class.BackwoodsSpaceInfoPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceManager = PlayerManager.InstanceManager

--override
function BackwoodsSpaceInfoPanel:Awake()
    self.disableCameraCulling = true
    self:InitView()
end

--override
function BackwoodsSpaceInfoPanel:OnDestroy()
end

function BackwoodsSpaceInfoPanel:OnShow(data)
    self:AddEventListeners()
    self:SetData(data)
end

function BackwoodsSpaceInfoPanel:SetData(data)
    self:RefreshView(data)
end

function BackwoodsSpaceInfoPanel:OnClose()
    self:Reset()
    self:RemoveEventListeners()
    self._dialogueContainer:SetActive(false)
end

function BackwoodsSpaceInfoPanel:AddEventListeners()
    
end

function BackwoodsSpaceInfoPanel:RemoveEventListeners()

end

function BackwoodsSpaceInfoPanel:InitView()
    self._angryContainer = self:FindChildGO("Container_Angry")
    self._angryText = self:GetChildComponent("Container_Angry/Text_Angry", "TextMeshWrapper")
    self._countDownText = self:GetChildComponent("Container_Angry/Text_CountDown", "TextMeshWrapper")
    self:InitProgress()

    self._dialogueContainer = self:FindChildGO("Container_Angry/Container_Dialogue")
    self._dialogueText = self:GetChildComponent("Container_Angry/Container_Dialogue/Image_Bg/Text_Desc", "TextMeshWrapper")
    self._dialogueContainer:SetActive(false)

    self._leaveMapContainer = self:FindChildGO("Container_LeaveMap")
    self._okButton = self:FindChildGO("Container_LeaveMap/Button_OK")
    self._csBH:AddClick(self._okButton,function() self:OnOKButtonClick() end)
    self._okText = self:GetChildComponent("Container_LeaveMap/Button_OK/Text", "TextMeshWrapper")
    self._leaveMapContainer:SetActive(false)
end

function BackwoodsSpaceInfoPanel:InitProgress()
    self._Comp = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Angry/ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
end

function BackwoodsSpaceInfoPanel:OnAngryChange(curAngry)
    local maxAngry = GlobalParamsHelper.GetParamValue(547)
    curAngry = curAngry or 0
    if curAngry > maxAngry then
        curAngry = maxAngry
    end
    self._angryText.text = LanguageDataHelper.CreateContentWithArgs(56048, {["0"]=curAngry})
    self._Comp:SetProgressByValue(curAngry , maxAngry)
end

function BackwoodsSpaceInfoPanel:RefreshView(data)
    if data == nil then
        return
    end
    if data.angry ~= nil then
        self:OnAngryChange(data.angry)
    end
    if data.timestamp ~= nil then
        --怒气值已满
        self.endTimestamp = data.timestamp + GlobalParamsHelper.GetParamValue(560)
        self.leaveTimestamp = data.timestamp + GlobalParamsHelper.GetParamValue(560) + GlobalParamsHelper.GetParamValue(631)
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateFullCountDown() end)

        self._dialogueContainer:SetActive(true)
    end
end

function BackwoodsSpaceInfoPanel:OnOKButtonClick()
    GUIManager.ClosePanel(PanelsConfig.BackwoodsSpaceInfo)
    InstanceManager:OnInstanceExit()
end

function BackwoodsSpaceInfoPanel:UpdateFullCountDown()
    local leaveSec = self.endTimestamp - DateTimeUtil.GetServerTime()
    if leaveSec < 0 then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        self._countDownText.text = ""
        self._leaveMapContainer:SetActive(true)
        self._leaveTimer = TimerHeap:AddSecTimer(0, 1, 0, function()self:UpdateLeaveCountDown() end)
        return 
    end
    self._countDownText.text = LanguageDataHelper.CreateContentWithArgs(56047, {["0"]=leaveSec})
end

function BackwoodsSpaceInfoPanel:UpdateLeaveCountDown()
    local leaveSec = self.leaveTimestamp - DateTimeUtil.GetServerTime()
    if leaveSec < 0 then
        if self._leaveTimer then
            TimerHeap:DelTimer(self._leaveTimer)
            self._leaveTimer = nil
        end
        self:OnOKButtonClick()
        return
    end
    self._okText.text = LanguageDataHelper.CreateContentWithArgs(56046, {["0"]=leaveSec})
end

function BackwoodsSpaceInfoPanel:Reset()
    self._countDownText.text = ""
    self._leaveMapContainer:SetActive(false)
    self.endTimestamp = 0
    self.leaveTimestamp = 0
    if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
    if self._leaveTimer then
        TimerHeap:DelTimer(self._leaveTimer)
        self._leaveTimer = nil
    end
    self:OnAngryChange(1)
end



function BackwoodsSpaceInfoPanel:WinBGType()
    return PanelWinBGType.NoBG
end