local PlayerInfoMenuItem = Class.PlayerInfoMenuItem(ClassTypes.BaseLuaUIComponent)

PlayerInfoMenuItem.interface = GameConfig.ComponentsConfig.PointableComponent
PlayerInfoMenuItem.classPath = "Modules.ModuleDynamicMenu.ChildComponent.PlayerInfoMenuItem"

local GlobalParamsHelper =  GameDataHelper.GlobalParamsHelper
local LanguageDataHelper =  GameDataHelper.LanguageDataHelper

function PlayerInfoMenuItem:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
end
--override
function PlayerInfoMenuItem:OnDestroy() 

end

function PlayerInfoMenuItem:InitView()
    self._descText = self:GetChildComponent("Text", "TextMeshWrapper")
end

function PlayerInfoMenuItem:OnPointerDown(pointerEventData)
    EventDispatcher:TriggerEvent(GameEvents.Player_Menu_Action_Resp, self.data)
end

function PlayerInfoMenuItem:OnPointerUp(pointerEventData)

end

--data 配置选项索引
function PlayerInfoMenuItem:SetData(data)
    self.data = data
    local actionTextData = GlobalParamsHelper.GetParamValue(601)
    self._descText.text = LanguageDataHelper.GetContent(actionTextData[data])
end