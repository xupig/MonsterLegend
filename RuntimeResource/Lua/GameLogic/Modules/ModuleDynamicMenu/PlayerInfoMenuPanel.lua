require "Modules.ModuleDynamicMenu.ChildComponent.PlayerInfoMenuItem"

local PlayerInfoMenuPanel = Class.PlayerInfoMenuPanel(ClassTypes.BasePanel)

local UIList = ClassTypes.UIList
local GUIManager = GameManager.GUIManager
local CheckOtherPlayerManager = PlayerManager.CheckOtherPlayerManager
local PlayerInfoMenuItem = ClassTypes.PlayerInfoMenuItem
local guildData = PlayerManager.PlayerDataManager.guildData
local GuildDataHelper = GameDataHelper.GuildDataHelper
local GuildManager = PlayerManager.GuildManager
local FriendManager = PlayerManager.FriendManager
local TeamManager = PlayerManager.TeamManager

local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendChatType = GameConfig.EnumType.FriendChatType
local public_config = require("ServerConfig/public_config")
local UIComponentUtil = GameUtil.UIComponentUtil
local GuildAction = GameConfig.EnumType.GuildAction
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PanelsConfig = GameConfig.PanelsConfig

function PlayerInfoMenuPanel:Awake()
    self.disableCameraCulling = true
    self._itemPool = {}
    self._items = {}
    self:InitView()
    self:InitCallback()
end

function PlayerInfoMenuPanel:InitCallback()
    self._actionResp = function(action)self:ActionResp(action) end
end
function PlayerInfoMenuPanel:OnDestroy()
    self.dbid = nil
    self.playerName = nil
    
    --工会信息
    self.guildInfo = nil
    
    --好友信息
    self.friendInfo = nil


end

function PlayerInfoMenuPanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.Player_Menu_Action_Resp, self._actionResp)
end

function PlayerInfoMenuPanel:OnShow(data)
    self.data = data
    self:RefreshView()
    
    EventDispatcher:AddEventListener(GameEvents.Player_Menu_Action_Resp, self._actionResp)
end

function PlayerInfoMenuPanel:InitView()
    self._buttonBGGO = self:FindChildGO("Button_Bg")
    self._csBH:AddClick(self._buttonBGGO, function()self:OnBGButtonClick() end)
    
    self._actionListTrans = self:FindChildGO("Container_Action").transform
    self.image1 = self:GetChildComponent("Container_Action/Image_BG1", 'ImageWrapper')
    self.image1:SetContinuousDimensionDirty(true)
    self.image2 = self:GetChildComponent("Container_Action/Image_BG1/Image_BG2", 'ImageWrapper')
    self.image2:SetContinuousDimensionDirty(true)
    
    self._itemTemp = self:FindChildGO("Container_Action/Container_Item")
    self._itemTemp:SetActive(false)
    
    self:InitActionResp()
end

function PlayerInfoMenuPanel:OnBGButtonClick()
    GUIManager.ClosePanel(PanelsConfig.PlayerInfoMenu)
--self.gameObject:SetActive(false)
end

function PlayerInfoMenuPanel:RefreshView()
    self.dbid = self.data.dbid
    self.playerName = self.data.playerName
    
    --工会信息
    self.guildInfo = self.data.guildInfo
    
    --好友信息
    self.friendInfo = self.data.friendInfo
    
    --组队系统点开
    self.teamInfo = self.data.teamInfo
    
    if self.guildInfo then
        --self.dbid = self.guildInfo[public_config.GUILD_MEMBER_INFO_DBID]
        self.position = self.guildInfo[public_config.GUILD_MEMBER_INFO_POSITION]
    end
    self:SetActionList()
    self:UpdateLayOut()
end

function PlayerInfoMenuPanel:UpdateLayOut()
    if self.data.rawListPos then
        --self._actionListTrans.anchoredPosition = self.data.rawListPos
        local listPos = self.data.rawListPos
        local menuY = listPos.y
        local menuX = listPos.x

        if menuY < 0 then
            menuY = 0
        end

        if menuY > 720-#self.actionData*60 then
            menuY = 720-#self.actionData*60
        end
        self._actionListTrans.anchoredPosition = Vector2.New(menuX, menuY)
    else
        local listPos = self.data.listPos
        if listPos then
            local menuY = listPos.y / Global.CanvasScaleY-(#self.actionData*30)
            local menuX = listPos.x / Global.CanvasScaleX
    
            if menuY < 0 then
                menuY = 0
            end
    
            if menuY > 720-#self.actionData*60 then
                menuY = 720-#self.actionData*60
            end
    
            self._actionListTrans.anchoredPosition = Vector2.New(menuX, menuY)
        else
            LoggerHelper.Error("请设定菜单位置data.listPos")
            return
        end
    end


end




function PlayerInfoMenuPanel:InitActionResp()
    self._actions = {}
    self._actions[GuildAction.LookOver] = function()CheckOtherPlayerManager:RequestCheckPlayer(self.playerName) end
    self._actions[GuildAction.GiveFlower] = function()FriendManager:MenuGiveFlower(self.dbid,self.playerName) end
    self._actions[GuildAction.Chat] = function() FriendManager:MenuStartChat(self.dbid,self.playerName) end
    self._actions[GuildAction.InviteTeam] = function()TeamManager:TeamInvite(self.dbid) end
    self._actions[GuildAction.ApplyForTeam] = function()TeamManager:TeamApplyByDBId(self.dbid) end
    self._actions[GuildAction.AddFriend] = function()FriendManager:MenuAddFriend(self.dbid) end
    self._actions[GuildAction.DeleteFriend] = function()FriendManager:MenuDeleteFriend(self.dbid) end
    self._actions[GuildAction.AddToBlacklist] = function()FriendManager:MenuAddToBlackList(self.dbid,self.playerName) end
    self._actions[GuildAction.RemoveFromBlacklist] = function()FriendManager:MenuRemoveFromBlacklist(self.dbid) end
    self._actions[GuildAction.DeleteFoeFriend] = function()FriendManager:MenuDeleteFoeFriend(self.dbid) end
    self._actions[GuildAction.KickOutTeam] = function()TeamManager:TeamKick(self.dbid) end
    
    self._actions[GuildAction.UpToViceLeader] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_VICE_PRESIDENT) end
    self._actions[GuildAction.KickOutViceLeader] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_NORMAL) end
    self._actions[GuildAction.MakeOverLeader] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_PRESIDENT) end
    self._actions[GuildAction.KickOutElite] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_NORMAL) end
    self._actions[GuildAction.DownToElite] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_ELITE) end
    self._actions[GuildAction.UpToElite] = function()GuildManager:GuildSetPositionReq(self.dbid, public_config.GUILD_POSITION_ELITE) end
    self._actions[GuildAction.KickOutGuild] = function()GuildManager:GuildKickReq(self.dbid) end
end

function PlayerInfoMenuPanel:SetActionList()
    self.actionData = {}
    --内外都有
    self.actionData[1] = GuildAction.LookOver
    
    local isFriend = FriendData:GetDbidFromFriendList(self.dbid) ~= nil
    local isBlackList = FriendData:GetDbidFromBlackList(self.dbid) ~= nil
    local isFoe = FriendData:GetDbidFromFoeList(self.dbid) ~= nil

    --好友系统点开时黑名单特殊处理
    if self.friendInfo and isBlackList then
        table.insert(self.actionData, GuildAction.RemoveFromBlacklist)
        self:SetData(self.actionData)
        self:UpdateData(self.actionData)
        return
    end
    

    --送花
    if not isBlackList then
        table.insert(self.actionData, GuildAction.GiveFlower)
    end
    
    --添加好友
    if not isFriend then
        table.insert(self.actionData, GuildAction.AddFriend)
    end
    
    --聊天
    if self.friendInfo == nil and (isFriend or isFoe) then
        table.insert(self.actionData, GuildAction.Chat)
    end
    
    --删除好友
    if isFriend then
        if self.friendInfo and FriendData:GetCurClickIndex() == FriendChatType.Recent then
            --最近联系人不算关系，点击不会出现删好友两个选项
            else
            table.insert(self.actionData, GuildAction.DeleteFriend)
        end
    end
    
    --邀请入队
    if self.teamInfo then
        if GameWorld.Player().team_status == 2 then
            table.insert(self.actionData, GuildAction.KickOutTeam)
        end
    else
        if GameWorld.Player().team_status == 2 then
            table.insert(self.actionData, GuildAction.InviteTeam)
        end
        --申请入队
        table.insert(self.actionData, GuildAction.ApplyForTeam)
    end
    
    
    --删除仇敌
    if isFoe then
        if self.friendInfo and FriendData:GetCurClickIndex() == FriendChatType.Recent then
            --最近联系人不算关系，点击不会出现删仇敌两个选项
            else
            table.insert(self.actionData, GuildAction.DeleteFoeFriend)
        end
    end
    
    
    --加入黑名单
    if not isBlackList then
        table.insert(self.actionData, GuildAction.AddToBlacklist)
    end
    
    --移除黑名单
    if isBlackList then
        table.insert(self.actionData, GuildAction.RemoveFromBlacklist)
    end
    
    --公会成员列表处理
    if self.guildInfo then
        if guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
            --自己是会长，选中副会长
            table.insert(self.actionData, GuildAction.MakeOverLeader)
            table.insert(self.actionData, GuildAction.KickOutViceLeader)
            table.insert(self.actionData, GuildAction.DownToElite)
            table.insert(self.actionData, GuildAction.KickOutGuild)
        elseif guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_ELITE then
            --自己是会长，选中长老
            table.insert(self.actionData, GuildAction.UpToViceLeader)
            table.insert(self.actionData, GuildAction.KickOutElite)
            table.insert(self.actionData, GuildAction.KickOutGuild)
        elseif guildData.guildPosition == public_config.GUILD_POSITION_PRESIDENT and self.position == public_config.GUILD_POSITION_NORMAL then
            --自己是会长，选中成员
            table.insert(self.actionData, GuildAction.UpToViceLeader)
            table.insert(self.actionData, GuildAction.UpToElite)
            table.insert(self.actionData, GuildAction.KickOutGuild)
        elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_PRESIDENT then
            --自己是副会长，选中会长
            elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
            --自己是副会长，选中副会长
            elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_ELITE then
                --自己是副会长，选中长老
                table.insert(self.actionData, GuildAction.UpToViceLeader)
                table.insert(self.actionData, GuildAction.KickOutElite)
                table.insert(self.actionData, GuildAction.KickOutGuild)
            elseif guildData.guildPosition == public_config.GUILD_POSITION_VICE_PRESIDENT and self.position == public_config.GUILD_POSITION_NORMAL then
                --自己是副会长，选中成员
                table.insert(self.actionData, GuildAction.UpToViceLeader)
                table.insert(self.actionData, GuildAction.UpToElite)
                table.insert(self.actionData, GuildAction.KickOutGuild)
            elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_PRESIDENT then
                --自己是长老，选中会长
                elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_VICE_PRESIDENT then
                --自己是长老，选中副会长
                elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_ELITE then
                    --自己是长老，选中长老
                    elseif guildData.guildPosition == public_config.GUILD_POSITION_ELITE and self.position == public_config.GUILD_POSITION_NORMAL then
                    --自己是长老，选中成员
                    table.insert(self.actionData, GuildAction.KickOutGuild)
                    elseif guildData.guildPosition == public_config.GUILD_POSITION_NORMAL then
                        --自己是成员
                        end
    end
    
    self:UpdateData(self.actionData)
    
end

--工会
function PlayerInfoMenuPanel:ActionResp(action)
    self:OnBGButtonClick()
    if self._actions[action] ~= nil then
        if action == GuildAction.KickOutGuild then
            --踢出公会弹框
            local name = self.data.playerName
            GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53186, {["0"] = name}),
                function()self._actions[action]() end, nil)
            return
        end
        if action == GuildAction.MakeOverLeader then
            --转让会长弹框
            GUIManager.ShowMessageBox(2, LanguageDataHelper.CreateContentWithArgs(53185),
                function()self._actions[action]() end, nil)
            return
        end
        
        self._actions[action]()
    end
end


function PlayerInfoMenuPanel:UpdateData(data)
    self:RemoveAll()
    for i, v in pairs(data) do
        self:AddItem(v)
    end
end

function PlayerInfoMenuPanel:CreateItem()
    local itemPool = self._itemPool
    if #itemPool > 0 then
        return table.remove(itemPool, 1)
    end
    local go = self:CopyUIGameObject("Container_Action/Container_Item", "Container_Action")
    local item = UIComponentUtil.AddLuaUIComponent(go, PlayerInfoMenuItem)
    return item
end

function PlayerInfoMenuPanel:AddItem(data)
    local item = self:CreateItem()
    item.gameObject:SetActive(true)
    table.insert(self._items, item)
    item:SetData(data)
end

function PlayerInfoMenuPanel:RemoveAll()
    for i = #self._items, 1, -1 do
        local item = self._items[i]
        item.gameObject:SetActive(false)
        self:ReleaseItem(item)
    end
    self._items = {}
end

function PlayerInfoMenuPanel:ReleaseItem(item)
    table.insert(self._itemPool, 1, item)
end
