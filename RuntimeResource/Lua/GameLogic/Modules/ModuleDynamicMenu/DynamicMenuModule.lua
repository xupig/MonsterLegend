DynamicMenuModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function DynamicMenuModule.Init()
    GUIManager.AddPanel(PanelsConfig.PlayerInfoMenu,"Panel_PlayerInfoMenu",GUILayer.LayerUIFloat,"Modules.ModuleDynamicMenu.PlayerInfoMenuPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return DynamicMenuModule