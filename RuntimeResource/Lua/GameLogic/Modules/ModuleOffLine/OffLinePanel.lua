require "Modules.ModuleRole.ChildComponent.BagListItem"
require "UIComponent.Extend.ItemGrid"

local OffLinePanel = Class.OffLinePanel(ClassTypes.BasePanel)
local PanelBGType = GameConfig.PanelsConfig.PanelBGType

local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local BagListItem = ClassTypes.BagListItem
local DateTimeUtil = GameUtil.DateTimeUtil
local bagData = PlayerManager.PlayerDataManager.bagData
local TipsManager = GameManager.TipsManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemGrid = ClassTypes.ItemGrid
local StringStyleUtil = GameUtil.StringStyleUtil
local OffLineManager = PlayerManager.OffLineManager

local Min_Volumn = 15

--override
function OffLinePanel:Awake()
    self:InitView()
end

--override
function OffLinePanel:OnDestroy()

end

--override
function OffLinePanel:OnClose()
    self:RemoveEventListeners()
end

function OffLinePanel:OnShow(data)
    self:AddEventListeners()
    self.data = data
    self:RefreshView()
end

function OffLinePanel:AddEventListeners()

end

function OffLinePanel:RemoveEventListeners()

end

function OffLinePanel:InitView()
    self._setting = self:FindChildGO("Button_Setting")
    self._csBH:AddClick(self._setting,function () self:OnSettingButtonClick() end)
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)
    self._ok = self:FindChildGO("Button_OK")
    self._csBH:AddClick(self._ok,function () self:OnCloseButtonClick() end)

    self._expIconGo = self:FindChildGO("Text_Exp/Container_Icon")
    local itemGo = GUIManager.AddItem(self._expIconGo, 1)
   	self._expIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._expIcon:SetItem(public_config.MONEY_TYPE_EXP)

    self._timeText = self:GetChildComponent("Text_Time/Text_Value", "TextMeshWrapper")
    self._beforeLevelText = self:GetChildComponent("Text_LevelUp/Text_Before", "TextMeshWrapper")
    self._afterLevelText = self:GetChildComponent("Text_LevelUp/Text_After", "TextMeshWrapper")
    self._expText = self:GetChildComponent("Text_Exp/Text_Value", "TextMeshWrapper")
    self._petExpText = self:GetChildComponent("Text_PetExp/Text_Value", "TextMeshWrapper")
    self._autoSellText = self:GetChildComponent("Text_AutoSell/Text_Value", "TextMeshWrapper")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
	self._scrollView = scrollView
    self._list:SetItemType(BagListItem)
    self._list:SetPadding(10, 0, 0, 10)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, 5, 100)
    self._list:SetItemClickedCB(function(idx) self:OnItemClicked(idx) end)
end

function OffLinePanel:OnItemClicked(index)
	local data = self._bagData[index+1]
	if data == "nil" then
		return
	end

	TipsManager:ShowItemTips(data,nil,true)
end

function OffLinePanel:OnCloseButtonClick()
    GUIManager.ClosePanel(PanelsConfig.OffLine)
end

function OffLinePanel:OnSettingButtonClick()
    GUIManager.ClosePanel(PanelsConfig.OffLine)
    GUIManager.ShowPanelByFunctionId(94)
end

function OffLinePanel:RefreshView()
    self._timeText.text = LanguageDataHelper.CreateContentWithArgs(52065, {["0"]=DateTimeUtil.FormatParseTimeByMin(self.data[1]),["1"]=DateTimeUtil.FormatParseTimeByMin(self.data[2])})
    self._beforeLevelText.text = self.data[3]..LanguageDataHelper.GetContent(69)
    self._afterLevelText.text = GameWorld.Player().level..LanguageDataHelper.GetContent(69)
    self._expText.text = StringStyleUtil.GetLongNumberString(self.data[7])
    self._petExpText.text = StringStyleUtil.GetLongNumberString(self.data[4])

    local equipCount = self.data[8] or 0
    local goldCount = self.data[9] or 0
    self._autoSellText.text = LanguageDataHelper.CreateContentWithArgs(52089, {["0"]=equipCount, ["1"]=StringStyleUtil.GetLongNumberString(goldCount)})

    local itemInfos = bagData:GetPkg(public_config.PKG_TYPE_OFFLINE):GetItemInfos()
    local bagCount =  bagData:GetPkg(public_config.PKG_TYPE_OFFLINE):GetTotalCount()
    self._bagData = {}
    self.count = math.max(bagCount, Min_Volumn)
    --填充空数据为了生成空格
	for i=1,self.count do
		if itemInfos[i] ~= nil then
            self._bagData[i] = itemInfos[i]
		else
			self._bagData[i] = "nil"
		end
    end
    
    self._list:SetDataList(self._bagData)
    self:UpdatePowerCompare()
    self:UpdateTip()
end

function OffLinePanel:UpdatePowerCompare()
	--检查装备是否能装备/战力是否更强
	for i=1,self.count do
		local itemData = self._bagData[i]
		if itemData ~= "nil" and itemData:GetItemType() == public_config.ITEM_ITEMTYPE_EQUIP then
			if itemData:CheckVocationMatchGeneral() then
				if itemData:CheckBetterThanLoaded() then
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,true)
				else
					self._list:GetItem(i-1):SetUseAndPowerCompare(true,false,false)
				end
			else
				self._list:GetItem(i-1):SetUseAndPowerCompare(true,true)
			end
		else
			self._list:GetItem(i-1):SetUseAndPowerCompare(false)
		end
	end
end

function OffLinePanel:UpdateTip()
	for i=1,self.count do
		local itemData = self._bagData[i]
        if itemData ~= "nil" then
            if OffLineManager:IsDevour(i) then
                self._list:GetItem(i-1):ShowTipInfo(LanguageDataHelper.GetContent(52068))
            else
                self._list:GetItem(i-1):ShowTipInfo("")
            end
		end
	end
end

function OffLinePanel:WinBGType()
    return PanelWinBGType.NoBG
end