OffLineModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function OffLineModule.Init()
	GUIManager.AddPanel(PanelsConfig.OffLine,"Panel_OffLine",GUILayer.LayerUIPanel,"Modules.ModuleOffLine.OffLinePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return OffLineModule