-- ComposePanel.lua
local ComposePanel = Class.ComposePanel(ClassTypes.BasePanel)
require "Modules.ModuleCompose.ChildView.ComposeItemViewBase"
require "Modules.ModuleCompose.ChildView.ComposeItemView"
require "Modules.ModuleCompose.ChildView.ComposeGemView"
require "Modules.ModuleCompose.ChildView.ComposeSoulView"
require "Modules.ModuleCompose.ChildView.ComposeEquipView"

local ComposeItemView = ClassTypes.ComposeItemView
local ComposeGemView = ClassTypes.ComposeGemView
local ComposeSoulView = ClassTypes.ComposeSoulView
local ComposeEquipView = ClassTypes.ComposeEquipView
local ComposeType = GameConfig.EnumType.ComposeType

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType

function ComposePanel:Awake()
	self._csBH = self:GetComponent("LuaUIPanel")
	self:InitCallBack()
	self:InitViews()
end

--初始化回调
function ComposePanel:InitCallBack()
end

function ComposePanel:InitViews()
	self:InitView("Container_Item", ComposeItemView,1)
	self:InitView("Container_Gem", ComposeGemView,2)
	self:InitView("Container_Soul", ComposeSoulView,3)
	self:InitView("Container_Equip", ComposeEquipView,4)
end


function ComposePanel:OnShow(data)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)

	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleCompose.ComposePanel")
end

function ComposePanel:OnClose()
end

function ComposePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

function ComposePanel:StructingViewInit()
     return true
end

--关闭的时候重新开启二级主菜单
function ComposePanel:ReopenSubMainPanel()
    return true
end

-- function ComposePanel:ScaleToHide()
--     return true
-- end
