-- ComposeEquipTypeItem.lua
--武器合成武器类型Item类
local ComposeEquipTypeItem = Class.ComposeEquipTypeItem(ClassTypes.BaseComponent)
ComposeEquipTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeEquipTypeItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeEquipTypeItem"

local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function ComposeEquipTypeItem:Awake()
	self._txtTypeName =  self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
end

function ComposeEquipTypeItem:UpdateData(composeData)
	local equipType = composeData.equip_part
	local typeCfg = ItemDataHelper.GetTypeNameByType(public_config.ITEM_ITEMTYPE_EQUIP,equipType)
	self._txtTypeName.text = LanguageDataHelper.CreateContent(typeCfg)
end

function ComposeEquipTypeItem:SetIndex(index)
	self._index = index
end

function ComposeEquipTypeItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

function ComposeEquipTypeItem:OnPointerDown(pointerEventData)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COMPOSE_SELECT_EQUIP_TYPE ,self._index)
end

function ComposeEquipTypeItem:OnPointerUp(pointerEventData)
	
end