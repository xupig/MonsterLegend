-- ComposeSecGradeTypeItem.lua
--装备合成二级级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local ComposeSecGradeTypeItem = Class.ComposeSecGradeTypeItem(UINavigationMenuItem)
ComposeSecGradeTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeSecGradeTypeItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeSecGradeTypeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function ComposeSecGradeTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgLock = self:FindChildGO("Image_Lock")
end

function ComposeSecGradeTypeItem:OnPointerDown(pointerEventData)

end

--override {type}
function ComposeSecGradeTypeItem:OnRefreshData(data)
	local gradeData = data[1]
	local cnGrade = StringStyleUtil.GetChineseNumber(gradeData.equip_grade)
	self._conditionLevel = gradeData.condition_level
	self._str = LanguageDataHelper.CreateContentWithArgs(72009,{["0"] = cnGrade})
	if GameWorld.Player().level >= self._conditionLevel then
		self._lock = false
		self._imgLock:SetActive(false)
	else
		self._lock = true
		self._imgLock:SetActive(true)
	end
	self:SetNormalTypeName()
end

function ComposeSecGradeTypeItem:GetLock()
	return self._lock
end

function ComposeSecGradeTypeItem:GetConditionLevel()
	return self._conditionLevel
end

function ComposeSecGradeTypeItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
	if isSelected then
		local str = BaseUtil.GetColorString(self._str, ColorConfig.A)
		self._txtTypeName.text = str
	else
		self:SetNormalTypeName()
	end
end

function ComposeSecGradeTypeItem:SetNormalTypeName()
	if self._lock then
		local str = BaseUtil.GetColorString(self._str, ColorConfig.C)
		self._txtTypeName.text = str
	else
		self._txtTypeName.text = self._str
	end
end