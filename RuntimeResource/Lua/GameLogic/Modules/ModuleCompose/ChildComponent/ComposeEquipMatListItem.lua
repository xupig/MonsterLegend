-- ComposeEquipMatListItem.lua
--合成选择装备材料列表Item类
local UIListItem = ClassTypes.UIListItem
local ComposeEquipMatListItem = Class.ComposeEquipMatListItem(UIListItem)
local public_config = GameWorld.public_config

ComposeEquipMatListItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeEquipMatListItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeEquipMatListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local ItemDataHelper = GameDataHelper.ItemDataHelper

function ComposeEquipMatListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateTipsByItemData()

	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
end

function ComposeEquipMatListItem:OnRefreshData(equipData)
	self._iconContainer:SetItemData(equipData)
	self._txtName.text = ItemDataHelper.GetItemNameWithColor(equipData.cfg_id)
end