-- ComposeEquipMaterialItem.lua
--武器合成材料Item类
local ComposeEquipMaterialItem = Class.ComposeEquipMaterialItem(ClassTypes.BaseComponent)
ComposeEquipMaterialItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeEquipMaterialItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeEquipMaterialItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function ComposeEquipMaterialItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)

	self._imgCross = self:FindChildGO("Image_Cross")
	self._imgBgWrapper = self:GetChildComponent("Image_Bg",'ImageWrapper')
	self._imgBgWrapper1 = self:GetChildComponent("Image_Bg/Image_Bg",'ImageWrapper')
end

function ComposeEquipMaterialItem:SetIndex(index)
	self._index = index
end

function ComposeEquipMaterialItem:UpdateData(itemData)
	if itemData then
		self._iconContainer:SetActive(true)
		self._iconContainer:SetItemData(itemData)
		self._imgCross:SetActive(false)
	else
		self._iconContainer:SetActive(false)
		self._imgCross:SetActive(true)
	end
end

--第四个格子会有锁定状态
function ComposeEquipMaterialItem:SetLock(isLock)
	if self._imgLock == nil then
		self._imgLock = self:FindChildGO("Image_Lock")
	end
	if isLock then
		self._iconContainer:SetActive(false)
		self._imgBgWrapper:SetGray(0)
		self._imgBgWrapper1:SetGray(0)
		self._imgCross:SetActive(false)
		self._imgLock:SetActive(true)
	else
		self._imgBgWrapper:SetGray(1)
		self._imgBgWrapper1:SetGray(1)
		self._imgCross:SetActive(true)
		self._imgLock:SetActive(false)
	end
end

function ComposeEquipMaterialItem:OnPointerDown(pointerEventData)

end

function ComposeEquipMaterialItem:OnPointerUp(pointerEventData)
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_COMPOSE_EQUIP_OPEN_SELECT,self._index)
end