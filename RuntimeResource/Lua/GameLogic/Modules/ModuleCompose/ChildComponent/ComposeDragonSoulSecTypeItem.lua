-- ComposeDragonSoulSecTypeItem.lua
--龙魂合成二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local ComposeDragonSoulSecTypeItem = Class.ComposeDragonSoulSecTypeItem(UINavigationMenuItem)
ComposeDragonSoulSecTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeDragonSoulSecTypeItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeDragonSoulSecTypeItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local bagData = PlayerManager.PlayerDataManager.bagData
local dragonSoulData = PlayerManager.PlayerDataManager.dragonSoulData
local public_config = GameWorld.public_config

function ComposeDragonSoulSecTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtProductName = self:GetChildComponent("Text_ProductName",'TextMeshWrapper')
	self._txtCount = self:GetChildComponent("Text_Count",'TextMeshWrapper')

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgSelected = self:FindChildGO("Image_Selected")

	--当前选中的成品icon
    local parent = self:FindChildGO("Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._productionIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._productionIcon:ActivateTipsById()
end

--override {type}
function ComposeDragonSoulSecTypeItem:OnRefreshData(data)
	self._data = data
	self._soulId = data.dragonsoul_comp_id
	self._txtProductName.text = ItemDataHelper.GetItemNameWithColor(self._soulId)
	self._productionIcon:SetItem(self._soulId)
	self:UpdateCount()
end

function ComposeDragonSoulSecTypeItem:UpdateCount()
	if self._data == nil then
		return
	end
	self._txtCount.text = tostring(dragonSoulData:GetDragonSoulNum(self._soulId))

	local matInfo = {}
	if self._data.dragonsoul_id then
		matInfo[1] = self._data.dragonsoul_id[1]
		matInfo[3] = self._data.dragonsoul_id[2]
	end
	matInfo[2] = self._data.item_id[1]

	local showRed = true
	for i=1,3 do
		if matInfo[i] then
			local itemId = matInfo[i][1]
			local itemCount = matInfo[i][2]
			local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
			if itemCount > bagCount then
				showRed = false
			end
		end
	end

	self._imgRedPoint:SetActive(showRed)
end

function ComposeDragonSoulSecTypeItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end