-- ComposeSecTypeItem.lua
--道具合成二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local ComposeSecTypeItem = Class.ComposeSecTypeItem(UINavigationMenuItem)
ComposeSecTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeSecTypeItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeSecTypeItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local bagData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config

function ComposeSecTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtProductName = self:GetChildComponent("Text_ProductName",'TextMeshWrapper')
	self._txtCount = self:GetChildComponent("Text_Count",'TextMeshWrapper')

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgSelected = self:FindChildGO("Image_Selected")

	--当前选中的成品icon
    local parent = self:FindChildGO("Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._productionIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._productionIcon:ActivateTipsById()
end

--override {type}
function ComposeSecTypeItem:OnRefreshData(data)
	self._data = data
	for k,v in pairs(data.item_comp_id) do
		self._itemId = k
		self._txtProductName.text = ItemDataHelper.GetItemNameWithColor(k)
		self._productionIcon:SetItem(k)
		self:UpdateCount()
	end
end

function ComposeSecTypeItem:UpdateCount()
	--可能外部更新的时候还没激活
	if self._data == nil then
		return
	end
	self._txtCount.text = tostring(bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemId))
	local matInfo = {}
	matInfo[1] = self._data.item1_id
	matInfo[2] = self._data.item2_id
	matInfo[3] = self._data.item3_id

	local showRed = true
	for i=1,3 do
		if matInfo[i] then
			for itemId,itemCount in pairs(matInfo[i]) do
				local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
				if itemCount > bagCount then
					showRed = false
				end
			end
		end
	end

	self._imgRedPoint:SetActive(showRed)
end

function ComposeSecTypeItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end