-- ComposeTypeItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local ComposeTypeItem = Class.ComposeTypeItem(UINavigationMenuItem)
ComposeTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
ComposeTypeItem.classPath = "Modules.ModuleCompose.ChildComponent.ComposeTypeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil

function ComposeTypeItem:Awake()
	UINavigationMenuItem.Awake(self)

	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._selected = false
end

function ComposeTypeItem:OnPointerDown(pointerEventData)

end

function ComposeTypeItem:SetSelected(isSelected)
	self._selected = isSelected
	if isSelected then
		local str = BaseUtil.GetColorString(self._str, ColorConfig.A)
		self._txtTypeName.text = str
	else
		self._txtTypeName.text = self._str
	end
end

function ComposeTypeItem:ToggleSelected()
	self:SetSelected(not self._selected)
end

--override {type}
function ComposeTypeItem:OnRefreshData(data)
	self._str = LanguageDataHelper.CreateContent(data)
	self._txtTypeName.text = self._str
end