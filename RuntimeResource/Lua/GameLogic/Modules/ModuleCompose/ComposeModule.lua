-- ComposeModule.lua
ComposeModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ComposeModule.Init()
    GUIManager.AddPanel(PanelsConfig.Compose, "Panel_Compose", GUILayer.LayerUIPanel, "Modules.ModuleCompose.ComposePanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return ComposeModule