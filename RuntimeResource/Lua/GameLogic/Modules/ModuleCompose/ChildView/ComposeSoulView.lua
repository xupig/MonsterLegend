-- ComposeSoulView.lua
local ComposeSoulView = Class.ComposeSoulView(ClassTypes.BaseComponent)

ComposeSoulView.interface = GameConfig.ComponentsConfig.Component
ComposeSoulView.classPath = "Modules.ModuleCompose.ChildView.ComposeSoulView"

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

require"Modules.ModuleCompose.ChildComponent.ComposeDragonSoulSecTypeItem"
require"Modules.ModuleCompose.ChildComponent.ComposeTypeItem"
require "PlayerManager/PlayerData/DragonSoulData"

local DragonSoulItemData = ClassTypes.DragonSoulItemData
local ComposeDragonSoulSecTypeItem = ClassTypes.ComposeDragonSoulSecTypeItem
local ComposeTypeItem = ClassTypes.ComposeTypeItem
local UINavigationMenu = ClassTypes.UINavigationMenu

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local ComposeManager = PlayerManager.ComposeManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ComposeDataHelper = GameDataHelper.ComposeDataHelper
local TipsManager = GameManager.TipsManager
local UIParticle = ClassTypes.UIParticle
local DragonsoulAttriDataHelper = GameDataHelper.DragonsoulAttriDataHelper
local AttriDataHelper = GameDataHelper.AttriDataHelper

function ComposeSoulView:Awake()
	self._funcId = 37
    self:InitCallBack()
	self:InitComps()
end

function ComposeSoulView:InitCallBack()
	self._updateCountCB = function ()
		self:UpdateSelectSItemCount()
	end

	self._composeSuccessCB = function ()
		self:OnComposeSuccess()
	end
end

function ComposeSoulView:InitComps()
	self._fxCompose = UIParticle.AddParticle(self.gameObject,"Container_ComposeSuccess/fx_ui_30031")
	self._fxCompose:Stop()

	self._btnCompose = self:FindChildGO("Button_Compose")
    self._csBH:AddClick(self._btnCompose, function()self:OnClickCompose() end)

    self._txtNeedLevel = self:GetChildComponent("Container_NeedLevel/Text_NeedLevel",'TextMeshWrapper')
    self._containerNeedLevel = self:FindChildGO("Container_NeedLevel")
    self._containerNeedLevel:SetActive(false)

    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu_Item")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(ComposeTypeItem)
	self._menu:SetSecondLevelItemType(ComposeDragonSoulSecTypeItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(3, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)

    self._mats = {}
    self._matLocks = {}
    self._matInfo = {}
    self._btnMatTouch = {}
    for i=1,3 do
    	local mat = self:AddChildLuaUIComponent("Container_Mats/Container_Mat"..i,ItemGridEx)
    	mat:SetShowName(false)
    	self._mats[i] = mat
    	self._matLocks[i] = self:FindChildGO("Container_Mats/Container_Mat"..i.."/Image_Lock")

    	self._btnMatTouch[i] = self:FindChildGO("Container_Mats/Container_Mat"..i.."/Button_Touch")
    	self._csBH:AddClick(self._btnMatTouch[i], function()self:OnClickMatTips(i) end)
    end

    --当前选中的成品icon
    local parent = self:FindChildGO("Container_Production/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._productionIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	--self._productionIcon:ActivateTipsById()
   	self._btnProductionTouch = self:FindChildGO("Container_Production/Button_Touch")
    self._csBH:AddClick(self._btnProductionTouch, function()self:OnClickTips() end)
   	self._txtProductName = self:GetChildComponent("Container_Production/Text_Name",'TextMeshWrapper')

   	--self._txtSuccessRate = self:GetChildComponent("Container_Production/Text_SuccessRate",'TextMeshWrapper')
   	self._txtAttris = {}
	self._txtAttris[1] = self:GetChildComponent("Container_Production/Text_Attri1",'TextMeshWrapper')
	self._txtAttris[2] = self:GetChildComponent("Container_Production/Text_Attri2",'TextMeshWrapper')
end

function ComposeSoulView:OnMenuFItemClicked(idx)
	if self._selectedFItem then
		self._selectedFItem:SetSelected(false)
		if self._selectedFIndex and self._selectedFIndex == idx then
			return
		end
	end

	self._selectedFItem = self._menu:GetFItem(idx)
	self._selectedFIndex = idx
	self._selectedFItem:ToggleSelected()
end

function ComposeSoulView:OnMenuSItemClicked(fIndex,sIndex)
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
	end
	self._selectedComposeData = self._menu:GetSecondLevelDataByIndex(fIndex,sIndex)
	self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:SetSelected(true)
    self:UpdateComposeInfo()
end

function ComposeSoulView:OnClickMatTips(index)
	local singleMatInfo = self._matInfo[index]
	if singleMatInfo == nil then
		return
	end
	local itemId =  singleMatInfo[1]
	TipsManager:ShowItemTipsById(itemId)
end
-- function ComposeSoulView:OnClickComposeAll()
-- 	if self._selectedComposeData then
-- 		ComposeManager:RequestComposeItem(self._selectedComposeData.id,1)
-- 	end
-- end

function ComposeSoulView:OnClickTips()
	if self._selectedComposeData then
		local dragonSoulItemData = DragonSoulItemData(self._selectedComposeData.dragonsoul_comp_id,1)
		TipsManager:ShowDragonSoulTips(dragonSoulItemData)
	end
end

function ComposeSoulView:OnClickCompose()
	if self._selectedComposeData then
		ComposeManager:RequestComposeDragonSoul(self._selectedComposeData.id)
	end
end

function ComposeSoulView:UpdateComposeInfo()
	self._matInfo = {}
	if self._selectedComposeData.dragonsoul_id then
		self._matInfo[1] = self._selectedComposeData.dragonsoul_id[1]
		self._matInfo[3] = self._selectedComposeData.dragonsoul_id[2]
	end
	self._matInfo[2] = self._selectedComposeData.item_id[1]

	for i=1,3 do
		if self._matInfo[i] then
			local itemId = self._matInfo[i][1]
			local itemCount = self._matInfo[i][2]
			self._mats[i]:UpdateData(itemId,itemCount)
			self._matLocks[i]:SetActive(false)
		else
			self._mats[i]:SetEmpty()
			self._matLocks[i]:SetActive(true)
		end
	end

	local productId = self._selectedComposeData.dragonsoul_comp_id
	self._txtProductName.text = ItemDataHelper.GetItemNameWithColor(productId)
	self._productionIcon:SetItem(productId)
	--self._txtSuccessRate.text = self._selectedComposeData.sucess_prop.."%"
	local attriCfg = DragonsoulAttriDataHelper:GetAttByIdle(productId,1)
	local attriList = attriCfg.attri
	local h = 0
	if attriList then
		for k,v in pairs(attriList) do
			local str = AttriDataHelper:GetName(k)
			str = str.."+"..AttriDataHelper:ConvertAttriValue(k,v)
			h = h+1
			-- self._txtAttgos[h]:SetActive(true)
			self._txtAttris[h].text = str
		end
	end
	if h <= 1 then
		self._txtAttris[2].text = ""
	end

	if self._selectedComposeData.level_limit > GameWorld.Player().level then
		self._containerNeedLevel:SetActive(true)
		self._txtNeedLevel.text = tostring(self._selectedComposeData.level_limit)
	else
		self._containerNeedLevel:SetActive(false)
	end
end

function ComposeSoulView:ShowView()
	self._menuData = ComposeDataHelper:GetAllComposeDragonSoulDataByFunc(self._funcId)
	self._menu:SetDataList(self._menuData)
	self._selectedComposeData = nil
	for i=1,3 do
		self._mats[i]:SetEmpty()
		self._matLocks[i]:SetActive(true)
	end
	self._productionIcon:SetItem(nil)
	self._txtProductName.text = ""
	-- self:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	EventDispatcher:AddEventListener(GameEvents.COMPOSE_SOUL_SUCCESS,self._composeSuccessCB)
end

--更新二级菜单选中的item
function ComposeSoulView:UpdateSelectSItemCount()
	for i=1,#self._menuData do
		for j=2,#self._menuData[i] do
			local item = self._menu:GetSItem(i-1,j-2)
			item:UpdateCount()
		end
	end
end

function ComposeSoulView:OnComposeSuccess()
	self:UpdateSelectSItemCount()
	--合成声效
    GameWorld.PlaySound(27)
	self._fxCompose:Play(true,true)
end

function ComposeSoulView:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	EventDispatcher:AddEventListener(GameEvents.COMPOSE_SOUL_SUCCESS,self._composeSuccessCB)
end

function ComposeSoulView:OnDisable()
	-- self._selectedSItem = nil
	-- self._selectedFItem = nil
	-- self._selectedFIndex = -1
	-- EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	-- EventDispatcher:RemoveEventListener(GameEvents.COMPOSE_SOUL_SUCCESS,self._composeSuccessCB)
end

function ComposeSoulView:CloseView()
	self._fxCompose:Stop()
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
	end
	-- self:OnDisable()
	self._txtAttris[1].text =""
	self._txtAttris[2].text =""
	self._selectedSItem = nil
	self._selectedFItem = nil
	self._selectedFIndex = -1
	EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	EventDispatcher:RemoveEventListener(GameEvents.COMPOSE_SOUL_SUCCESS,self._composeSuccessCB)
end