-- ComposeEquipView.lua
local ComposeEquipView = Class.ComposeEquipView(ClassTypes.BaseComponent)

ComposeEquipView.interface = GameConfig.ComponentsConfig.Component
ComposeEquipView.classPath = "Modules.ModuleCompose.ChildView.ComposeEquipView"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx

require"Modules.ModuleCompose.ChildComponent.ComposeSecGradeTypeItem"
require"Modules.ModuleCompose.ChildComponent.ComposeTypeItem"
require"Modules.ModuleCompose.ChildComponent.ComposeEquipTypeItem"
require"Modules.ModuleCompose.ChildComponent.ComposeEquipMatListItem"
require"Modules.ModuleCompose.ChildComponent.ComposeEquipMaterialItem"

local ComposeSecGradeTypeItem = ClassTypes.ComposeSecGradeTypeItem
local ComposeTypeItem = ClassTypes.ComposeTypeItem
local ComposeEquipMatListItem = ClassTypes.ComposeEquipMatListItem
local ComposeEquipMaterialItem = ClassTypes.ComposeEquipMaterialItem
local ComposeEquipTypeItem = ClassTypes.ComposeEquipTypeItem
local UINavigationMenu = ClassTypes.UINavigationMenu

local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ComposeDataHelper = GameDataHelper.ComposeDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local bagData = PlayerManager.PlayerDataManager.bagData
local ComposeManager = PlayerManager.ComposeManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local UIParticle = ClassTypes.UIParticle

function ComposeEquipView:Awake()
    self:InitCallBack()
	self:InitComps()
	self._funcId = 38
end

function ComposeEquipView:InitCallBack()

    self._openSelectCb = function (index)
        self:OpenSelect(index)
    end

    self._selectEquipTypeCb = function (index)
        self:SelectEquipType(index)
    end

    self._composeSuccessCB = function (code)
        self:OnComposeSuccess(code)
    end
end

function ComposeEquipView:InitComps()
    self._fxCompose = UIParticle.AddParticle(self.gameObject,"Container_ComposeSuccess/fx_ui_30031")
    self._fxCompose:Stop()

	self._btnCompose = self:FindChildGO("Button_Compose")
    self._csBH:AddClick(self._btnCompose, function()self:OnClickCompose() end)

    self._btnAddMat = self:FindChildGO("Button_AddMat")
    self._csBH:AddClick(self._btnAddMat, function()self:OnClickAddMat() end)

    self._toggleCurVocation = UIToggle.AddToggle(self.gameObject, "Toggle_ShowCurVocation")
	self._toggleCurVocation:SetIsOn(false)
    self._toggleCurVocation:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

	local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu_Equip")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(ComposeTypeItem)
	self._menu:SetSecondLevelItemType(ComposeSecGradeTypeItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(3, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)

    self._mats = {}
    for i=1,5 do
    	local mat = self:AddChildLuaUIComponent("Container_Equip/Container_Material"..i,ComposeEquipMaterialItem)
    	mat:SetIndex(i)
        self._mats[i] = mat
    end

    self._godStoneMat = self:AddChildLuaUIComponent("Container_GodStone",ItemGridEx)
    self._godStoneMat:SetShowName(false)
    self._godStoneMat:ActivateTips()
    self._godStoneMat:SetActive(false)

    --当前选中的成品icon
    local parent = self:FindChildGO("Container_Production/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._productionIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._productionIcon:ActivateTipsById()
   	self._txtProductName = self:GetChildComponent("Container_Production/Text_Name",'TextMeshWrapper')

   	self._txtSuccessRate = self:GetChildComponent("Container_Production/Text_SuccessRate",'TextMeshWrapper')

    self._containerEquipType = self:FindChildGO("Container_EquipType")
    self._typeBg = self:FindChildGO("Container_EquipType/Image_Bg")
    self._typeBgRectTransform = self._typeBg:GetComponent(typeof(UnityEngine.RectTransform))
   	local equipTypeItemGOs = {}
   	self._equipTypeItems = {}
	equipTypeItemGOs[1] = self:FindChildGO("Container_EquipType/Container_EquipTypeList/Container_Equip")
    for i = 2, 8 do
        local go = self:CopyUIGameObject("Container_EquipType/Container_EquipTypeList/Container_Equip", "Container_EquipType/Container_EquipTypeList")
    	equipTypeItemGOs[i] = go
    end

    for i=1,8 do
    	local go = equipTypeItemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, ComposeEquipTypeItem)
        item:SetIndex(i)
    	self._equipTypeItems[i] = item
    end

   	----------------------选择装备--------------------------------
    --装备对应材料部位配置
    self._matchPartMap = GlobalParamsHelper.GetParamValue(486)
    --成功率配置
    self._successRateCfg = GlobalParamsHelper.GetParamValue(523)

    self._containerSelectEquip = self:FindChildGO("Container_SelectEquip")

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_SelectEquip/List_Equip")
    self._listEquip = list
    self._scrollView = scrollView
    self._listEquip:SetItemType(ComposeEquipMatListItem)
    self._listEquip:SetPadding(0, 0, 0, 0)
    self._listEquip:SetGap(0, 0)
    self._listEquip:SetDirection(UIList.DirectionTopToDown, 1, 1)
    self._listEquip:SetItemClickedCB(function (idx)
    	self:OnEquipSelect(idx)
    end)

    --选中的装备
    self._selectedMat = {}

    self._btnClose = self:FindChildGO("Container_SelectEquip/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:OnClickCloseSelect() end)

    self._btnUnloadMat = self:FindChildGO("Container_SelectEquip/Button_Unload")
    self._csBH:AddClick(self._btnUnloadMat, function()self:OnClickUnloadMat() end)

    ---------------------------合成说明------------------------------------
    self._txtExplain = self:GetChildComponent("Text_Info",'TextMeshWrapper')
    self._txtExplain.text = ""

    ---------------------------添加3把剑------------------------------------
    self._swordIconObj1 = self:FindChildGO("Container_Bg/Container_SwordIcon1")
    self._swordIconObj2 = self:FindChildGO("Container_Bg/Container_SwordIcon2")
    self._swordIconObj3 = self:FindChildGO("Container_Bg/Container_SwordIcon3")
    GameWorld.AddIcon(self._swordIconObj1 ,13549)
    GameWorld.AddIcon(self._swordIconObj2 ,13549)
    GameWorld.AddIcon(self._swordIconObj3 ,13550)
end

function ComposeEquipView:OnMenuFItemClicked(idx)
    self._containerEquipType:SetActive(false)
	if self._selectedFItem then
        self._selectedFItem:SetSelected(false)
        if self._selectedFIndex and self._selectedFIndex == idx then
            return
        end
    end

    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFIndex = idx
    self._selectedFItem:ToggleSelected()
end

function ComposeEquipView:OnMenuSItemClicked(fIndex, sIndex)
    local curItem = self._menu:GetSItem(fIndex,sIndex)
    if curItem:GetLock() then
        local args = LanguageDataHelper.GetArgsTable()
        args["0"] = curItem:GetConditionLevel()
        local str = LanguageDataHelper.CreateContentWithArgs(179,args)
        GUIManager.ShowText(2,str)
        return
    end
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
	end
	self._selectedSItem = curItem
    self._selectedSItem:SetSelected(true)
    self._selectedGradeData = self._menu:GetSecondLevelDataByIndex(fIndex,sIndex)
    self:UpdateEquipType()
end

--一键填充材料
function ComposeEquipView:OnClickAddMat()
	if self._selectedComposeData then
        local fillNum
        if self._godStoneId > 0 then
            fillNum = 3
        else
            fillNum = 5
        end

        local equipMatList = self:GetMatListData()
        for i=1,fillNum do
            if self._selectedMat[i] == nil then
                if #equipMatList > 0 then
                    local equipData = table.remove(equipMatList,1)
                    self._mats[i]:UpdateData(equipData)
                    self._selectedMat[i] = equipData
                else
                    break
                end
            end
        end
        
        self:UpdateSuccessRate()
    end
end

function ComposeEquipView:OnClickCompose()
    if self._successRate == 0 then
        --LoggerHelper.Log("No enough Material")
        --return
    end
	if self._selectedComposeData then
        local posList = {}
        if self._godStoneId > 0 then
            for i=1,3 do
                if self._selectedMat[i] then
                    posList[i] = self._selectedMat[i].bagPos
                else
                    posList[i] = 0
                end
            end
            posList[4] = 0
            posList[5] = 0 --bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._godStoneId)
        else
            for i=1,5 do
                if self._selectedMat[i] then
                    posList[i] = self._selectedMat[i].bagPos
                else
                    posList[i] = 0
                end
            end
        end
		--LoggerHelper.Error(posList[1]..posList[2]..posList[3]..posList[4]..posList[5])
		ComposeManager:RequestComposeEquip(self._selectedComposeData.id,posList[1],posList[2],posList[3],posList[4],posList[5])
	end
end

--选择装备类型
function ComposeEquipView:UpdateEquipType()
    self._containerEquipType:SetActive(true)
    --LoggerHelper.Error("UpdateEquipType"..#self._selectedGradeData)
    self._typeBgRectTransform.sizeDelta = Vector2(120,30+44*(#self._selectedGradeData))
    self._typeBg:SetActive(false)
    self._typeBg:SetActive(true)
    for i=1,8 do
        if self._selectedGradeData[i] then
            self._equipTypeItems[i]:SetActive(true)
            self._equipTypeItems[i]:UpdateData(self._selectedGradeData[i])
        else
            self._equipTypeItems[i]:SetActive(false)
        end
    end

    self:SelectEquipType(1)
    self:OnClickCloseSelect()
end

--
function ComposeEquipView:OnToggleValueChanged(state)
    if state then
        self._showCurVocation = true
    else
        self._showCurVocation = false
    end
    self:UpdateMenu()
end

function ComposeEquipView:UpdateMenu()
    self._menuData = {}
    local srcData = ComposeDataHelper:GetAllComposeEquipDataByFunc(self._funcId)
    if self._showCurVocation then
        local vocationType = math.floor(GameWorld.Player().vocation/100)
        for i=1,#srcData do
            if srcData[i][2][1].vocation == vocationType then
                table.insert(self._menuData,srcData[i])
            end
        end
    else
        self._menuData = srcData
    end
    
    self._menu:SetDataList(self._menuData)

    self._selectedSItem = nil
    self._selectedFItem = nil
    self._selectedFIndex = -1

    for i=1,8 do
        self._equipTypeItems[i]:SetActive(false)
    end
    self._containerEquipType:SetActive(false)
end

function ComposeEquipView:ShowView()
	self._toggleCurVocation:SetIsOn(true)
    self:UpdateMenu()

    self._godStoneId = 0
    self._successRate = 0
    self._selectedMat = {}
    -- self:OnEnable()
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_COMPOSE_EQUIP_OPEN_SELECT,self._openSelectCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_COMPOSE_SELECT_EQUIP_TYPE ,self._selectEquipTypeCb)
    EventDispatcher:AddEventListener(GameEvents.COMPOSE_EQUIP_SUCCESS ,self._composeSuccessCB)
end

--打开选择
function ComposeEquipView:OpenSelect(index)
    if self._selectedComposeData then
        if self._godStoneId > 0 and index > 3 then
            return
        end
        self._selectedMatSlot = index  --点击的格子
        self._containerSelectEquip:SetActive(true)
        if self._selectedMat[self._selectedMatSlot] then
            self._btnUnloadMat:SetActive(true)
        else
            self._btnUnloadMat:SetActive(false)
        end

        self._listEquip:SetDataList(self:GetMatListData())
    end
end

function ComposeEquipView:GetMatListData()
    local d = {}
    local allBagData = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
    for k,itemData in pairs(allBagData) do
        if self:CheckIsMaterial(itemData) then
            table.insert(d,itemData)
        end
    end
    return d
end

--检查装备是否匹配
function ComposeEquipView:CheckIsMaterial(itemData)
    if itemData:GetItemType() ~= public_config.ITEM_ITEMTYPE_EQUIP then
        return false
    end

    --是否已在材料中
    local loaded = false
    for k,v in pairs(self._selectedMat) do
        if v.bagPos == itemData.bagPos then
            return false
        end
    end
    if loaded then
        return false
    end

    local itemCfg = itemData.cfgData
    --职业匹配
    local voctaionMatch = false
    for i=1,#itemCfg.useVocation do
        local vocationType = math.floor(itemCfg.useVocation[i]/100)
        if vocationType == self._selectedComposeData.vocation then
            voctaionMatch = true
        end
    end

    if voctaionMatch == false then 
        return false
    end

    --部位匹配
    local equipPart = itemData:GetType()
    local partList = self._matchPartMap[self._selectedComposeData.equip_part]
    local partMatch = false
    for i=1,#partList do
        if partList[i] == equipPart then
            partMatch = true
        end
    end
    if partMatch == false then 
        return false
    end

    --星数匹配
    if itemCfg.star ~= self._selectedComposeData.stuff_star then
        return false
    end

    --阶数匹配
    if itemCfg.grade ~= self._selectedComposeData.equip_grade then
        return false
    end

    --品质匹配
    if itemCfg.quality ~= self._selectedComposeData.stuff_quality then
        return false
    end

    return true
end

function ComposeEquipView:OnClickCloseSelect()
    self._containerSelectEquip:SetActive(false)
end

--选择材料装备
function ComposeEquipView:OnEquipSelect(idx)
    if self._selectedMatSlot then
        local equipData = self._listEquip:GetDataByIndex(idx)
        self._mats[self._selectedMatSlot]:UpdateData(equipData)
        self._selectedMat[self._selectedMatSlot] = equipData
        self:OnClickCloseSelect()
        self:UpdateSuccessRate()
    end
end

--卸下材料装备
function ComposeEquipView:OnClickUnloadMat()
    if self._selectedMatSlot then
        self._mats[self._selectedMatSlot]:UpdateData(nil)
        self._selectedMat[self._selectedMatSlot] = nil
        self:OnClickCloseSelect()
        self:UpdateSuccessRate()
    end
end

--更新成功率
function ComposeEquipView:UpdateSuccessRate()
    local total = 0
    for i=1,5 do
        if self._selectedMat[i] then
            total = total + 1
        end
    end

    if self._godStoneId > 0 then
        if total == 3 then
            self._successRate = 100
        else
            self._successRate = 0
        end
    else
        self._successRate = (self._successRateCfg[total] or 0)*100
    end
    
    self._txtSuccessRate.text = tostring(self._successRate).."%"
end

function ComposeEquipView:SelectEquipType(index)
    if self._selectedEquipTypeItem then
        self._selectedEquipTypeItem:SetSelected(false)
    end
    self._selectedEquipTypeItem = self._equipTypeItems[index]
    self._selectedEquipTypeItem:SetSelected(true)

    self._selectedComposeData = self._selectedGradeData[index]
    self._selectEquipType = index
    self:OnClickCloseSelect()
    self:UpdateEquipInfo()
end

function ComposeEquipView:UpdateEquipInfo()
    --神装
    if self._selectedComposeData.godstone_id then
        for i=1,3 do
            self._mats[i]:UpdateData(nil)
        end
        self._mats[4]:SetLock(true)
        self._godStoneMat:SetActive(true)
        for k,v in pairs(self._selectedComposeData.godstone_id) do
            self._godStoneId = k
            self._needStoneCount = v
            self._godStoneMat:UpdateData(k,v)
        end
    --普通装 
    else
        self._godStoneId = 0
        for i=1,5 do
            self._mats[i]:UpdateData(nil)
        end
        self._mats[4]:SetLock(false)
        self._godStoneMat:SetActive(false)
    end

    self._selectedMat = {}

    local productId = self._selectedComposeData.comp_equip_id
    self._txtProductName.text = ItemDataHelper.GetItemNameWithColor(productId)
    self._productionIcon:SetItem(productId)

    local grade = self._selectedComposeData.equip_grade
    local star = self._selectedComposeData.stuff_star
    local quality = self._selectedComposeData.stuff_quality
    local qualityStr = LanguageDataHelper.CreateContent(ItemConfig.QualityCN[quality])
    qualityStr = StringStyleUtil.GetColorStringWithColorId(qualityStr,ItemConfig.QualityTextMap[quality])
    local args = LanguageDataHelper.GetArgsTable()
    args["0"] = grade
    args["1"] = star
    args["2"] = qualityStr
    local equipCn
    if self._selectEquipType < 7 then
        equipCn = LanguageDataHelper.CreateContent(54)
    else
        local cnType = ItemDataHelper.GetTypeNameByType(public_config.ITEM_ITEMTYPE_EQUIP,self._selectEquipType)
        equipCn = LanguageDataHelper.CreateContent(cnType)
    end
    args["3"] = equipCn

    if self._selectedComposeData.godstone_id then
        args["4"] = self._needStoneCount
        self._txtExplain.text = LanguageDataHelper.CreateContentWithArgs(72024,args)
    else
        self._txtExplain.text = LanguageDataHelper.CreateContentWithArgs(72021,args)
    end
    
    self:UpdateSuccessRate()
end

function ComposeEquipView:OnComposeSuccess(code)
    --合成成功
    if code == 0 then
        --合成声效
        GameWorld.PlaySound(27)
        self._fxCompose:Play(true,true)
    end
    self:UpdateEquipInfo()
end

function ComposeEquipView:ClearView()
    self._txtProductName.text = ""
    self._productionIcon:SetItem(nil)
    for i=1,5 do
        self._mats[i]:UpdateData(nil)
    end
    self._txtExplain.text = ""
    self._txtSuccessRate.text = ""
end

function ComposeEquipView:CloseView()
    self._fxCompose:Play(false)
    if self._selectedSItem then
        self._selectedSItem:SetSelected(false)
    end
    self:OnClickCloseSelect()
    self:ClearView()
    -- self:OnDisable()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COMPOSE_EQUIP_OPEN_SELECT,self._openSelectCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COMPOSE_SELECT_EQUIP_TYPE ,self._selectEquipTypeCb)
    EventDispatcher:RemoveEventListener(GameEvents.COMPOSE_EQUIP_SUCCESS ,self._composeSuccessCB)
end

function ComposeEquipView:OnEnable()
    -- EventDispatcher:AddEventListener(GameEvents.UIEVENT_COMPOSE_EQUIP_OPEN_SELECT,self._openSelectCb)
    -- EventDispatcher:AddEventListener(GameEvents.UIEVENT_COMPOSE_SELECT_EQUIP_TYPE ,self._selectEquipTypeCb)
    -- EventDispatcher:AddEventListener(GameEvents.COMPOSE_EQUIP_SUCCESS ,self._composeSuccessCB)
end

function ComposeEquipView:OnDisable()
    -- EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COMPOSE_EQUIP_OPEN_SELECT,self._openSelectCb)
    -- EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_COMPOSE_SELECT_EQUIP_TYPE ,self._selectEquipTypeCb)
    -- EventDispatcher:RemoveEventListener(GameEvents.COMPOSE_EQUIP_SUCCESS ,self._composeSuccessCB)
end
