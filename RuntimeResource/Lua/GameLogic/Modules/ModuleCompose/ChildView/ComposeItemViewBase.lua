-- ComposeItemViewBase.lua
local ComposeItemViewBase = Class.ComposeItemViewBase(ClassTypes.BaseComponent)

ComposeItemViewBase.interface = GameConfig.ComponentsConfig.Component
ComposeItemViewBase.classPath = "Modules.ModuleCompose.ChildView.ComposeItemViewBase"

require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

require"Modules.ModuleCompose.ChildComponent.ComposeSecTypeItem"
require"Modules.ModuleCompose.ChildComponent.ComposeTypeItem"

local ComposeSecTypeItem = ClassTypes.ComposeSecTypeItem
local ComposeTypeItem = ClassTypes.ComposeTypeItem
local UINavigationMenu = ClassTypes.UINavigationMenu

local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local ComposeManager = PlayerManager.ComposeManager
local UIComponentUtil = GameUtil.UIComponentUtil
local ComposeDataHelper = GameDataHelper.ComposeDataHelper
local UIParticle = ClassTypes.UIParticle

function ComposeItemViewBase:Awake()
    self:InitCallBack()
	self:InitComps()
end

function ComposeItemViewBase:InitCallBack()
	self._updateCountCB = function ()
		self:UpdateSelectSItemCount()
	end

	self._composeSuccessCB = function ()
		self:OnComposeSuccess()
	end
end

function ComposeItemViewBase:ClearAttri()
end

function ComposeItemViewBase:UpdateAttri(itemId)
	
end

function ComposeItemViewBase:InitProductAttri()
	
end

function ComposeItemViewBase:InitComps()
	self._fxCompose = UIParticle.AddParticle(self.gameObject,"Container_ComposeSuccess/fx_ui_30031")
	self._fxCompose:Stop()

	self._btnCompose = self:FindChildGO("Button_Compose")
    self._csBH:AddClick(self._btnCompose, function()self:OnClickCompose() end)

    self._btnOneClickCompose = self:FindChildGO("Button_OneClickCompose")
    self._csBH:AddClick(self._btnOneClickCompose, function()self:OnClickComposeAll() end)

    self._txtPrice = self:GetChildComponent("Text_Price",'TextMeshWrapper')
    local moneyIcon = self:FindChildGO("Container_MoneyIcon")
    GameWorld.AddIcon(moneyIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GOLD))

    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu_Item")
	self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(ComposeTypeItem)
	self._menu:SetSecondLevelItemType(ComposeSecTypeItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(3, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
    self._menuCfgData = ComposeDataHelper:GetAllComposeItemDataByFunc(self._funcId)
	
    self._mats = {}
    self._matLocks = {}
    for i=1,3 do
    	local mat = self:AddChildLuaUIComponent("Container_Mats/Container_Mat"..i,ItemGridEx)
    	mat:SetShowName(false)
    	mat:ActivateTips()
    	self._mats[i] = mat
    	self._matLocks[i] = self:FindChildGO("Container_Mats/Container_Mat"..i.."/Image_Lock")
    end

    --当前选中的成品icon
    local parent = self:FindChildGO("Container_Production/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._productionIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	self._productionIcon:ActivateTipsById()
   	self._txtProductName = self:GetChildComponent("Container_Production/Text_Name",'TextMeshWrapper')
   	self:InitProductAttri()
end

function ComposeItemViewBase:OnMenuFItemClicked(idx)
	if idx == 0 then
		EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_compose_menu_fitem_button")
	end
	if self._selectedFItem then
		self._selectedFItem:SetSelected(false)
		if self._selectedFIndex and self._selectedFIndex == idx then
			return
		end
	end

	self._selectedFItem = self._menu:GetFItem(idx)
	self._selectedFIndex = idx
	self._selectedFItem:ToggleSelected()
end

function ComposeItemViewBase:OnMenuSItemClicked(fIndex,sIndex)
	if fIndex == 0 and sIndex == 0 then
		EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_compose_menu_sitem_button")
	end
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
	end
	self._selectedComposeData = self._menu:GetSecondLevelDataByIndex(fIndex,sIndex)
	self._selectedSItem = self._menu:GetSItem(fIndex,sIndex)
    self._selectedSItem:SetSelected(true)
    self:UpdateComposeInfo()
end

function ComposeItemViewBase:OnClickComposeAll()
	if self._selectedComposeData then
		EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_compose_all_button")
		ComposeManager:RequestComposeItem(self._selectedComposeData.id,1)
	end
end

function ComposeItemViewBase:OnClickCompose()
	if self._selectedComposeData then
		ComposeManager:RequestComposeItem(self._selectedComposeData.id,0)
	end
end

function ComposeItemViewBase:UpdateComposeInfo()
	local matInfo = {}
	matInfo[1] = self._selectedComposeData.item1_id
	matInfo[2] = self._selectedComposeData.item2_id
	matInfo[3] = self._selectedComposeData.item3_id

	for i=1,3 do
		if matInfo[i] then
			for itemId,itemCount in pairs(matInfo[i]) do
				self._mats[i]:UpdateData(itemId,itemCount)
				self._matLocks[i]:SetActive(false)
			end
		else
			self._mats[i]:SetEmpty()
			self._matLocks[i]:SetActive(true)
		end
	end

	for itemId,v in pairs(self._selectedComposeData.item_comp_id) do
		self._txtProductName.text = ItemDataHelper.GetItemNameWithColor(itemId)
		self._productionIcon:SetItem(itemId)
		self:UpdateAttri(itemId)
	end
	self._txtPrice.text = tostring(self._selectedComposeData.money)
end

function ComposeItemViewBase:ShowView()
	self._txtPrice.text = "0"
	for i=1,3 do
		self._mats[i]:SetEmpty()
		self._matLocks[i]:SetActive(true)
	end

	self:UpdateMenu()

	self._productionIcon:SetItem(nil)
	self._txtProductName.text = ""
	-- self:OnEnable()
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	EventDispatcher:AddEventListener(GameEvents.COMPOSE_ITEM_SUCCESS,self._composeSuccessCB)
end

function ComposeItemViewBase:UpdateMenu()
	local playerLevel = GameWorld.Player().level
	self._menuData = {}
	for i=1,#self._menuCfgData do
		local t
		for j=2,#self._menuCfgData[i] do
			local cfg = self._menuCfgData[i][j]
			if cfg.show_level == nil or cfg.show_level <= playerLevel then
				if t == nil then
					t = {}
					t[1] = self._menuCfgData[i][1]
				end
				table.insert(t,cfg)
			end
		end
		if t then
			table.insert(self._menuData,t)
		end
	end
	self._menu:SetDataList(self._menuData)
end

--更新二级菜单选中的item
function ComposeItemViewBase:UpdateSelectSItemCount()
	for i=1,#self._menuData do
		for j=2,#self._menuData[i] do
			local item = self._menu:GetSItem(i-1,j-2)
			item:UpdateCount()
		end
	end
end

function ComposeItemViewBase:OnComposeSuccess()
	--合成声效
    GameWorld.PlaySound(27)
	self._fxCompose:Play(true,true)
end

function ComposeItemViewBase:CloseView()
	self._fxCompose:Stop()
	if self._selectedSItem then
		self._selectedSItem:SetSelected(false)
	end
	self._selectedSItem = nil
	self._selectedFItem = nil
	self._selectedFIndex = -1
	self:ClearAttri()
	EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE,self._updateCountCB)
	EventDispatcher:RemoveEventListener(GameEvents.COMPOSE_ITEM_SUCCESS,self._composeSuccessCB)
end