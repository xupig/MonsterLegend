-- ComposeItemView.lua
local ComposeItemView = Class.ComposeItemView(ClassTypes.ComposeItemViewBase)

ComposeItemView.interface = GameConfig.ComponentsConfig.Component
ComposeItemView.classPath = "Modules.ModuleCompose.ChildView.ComposeItemView"

local public_config = GameWorld.public_config

function ComposeItemView:Awake()
	self._funcId = 35--public_config.TREASURE_TYPE_WING
	self._base.Awake(self)
end