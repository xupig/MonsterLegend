-- ComposeGemView.lua
local ComposeGemView = Class.ComposeGemView(ClassTypes.ComposeItemViewBase)

ComposeGemView.interface = GameConfig.ComponentsConfig.Component
ComposeGemView.classPath = "Modules.ModuleCompose.ChildView.ComposeGemView"

local public_config = GameWorld.public_config
local AttriDataHelper = GameDataHelper.AttriDataHelper

function ComposeGemView:Awake()
	self._funcId = 36--public_config.TREASURE_TYPE_WING
	self._base.Awake(self)
end

function ComposeGemView:UpdateAttri(itemId)
	local gemCfg = ItemDataHelper.GetItem(itemId)
	for i=1,2 do
		if gemCfg.gem_attribute[i] then
			local attriKey = gemCfg.gem_attribute[i][1]
			local attriValue = gemCfg.gem_attribute[i][2]
			self._txtAttris[i].text = AttriDataHelper:GetName(attriKey).."+"..AttriDataHelper:ConvertAttriValue(attriKey,attriValue)
		else
			self._txtAttris[i].text = ""
		end
	end
end

function ComposeGemView:InitProductAttri()
	self._txtAttris = {}
	self._txtAttris[1] = self:GetChildComponent("Container_Production/Text_Attri1",'TextMeshWrapper')
	self._txtAttris[2] = self:GetChildComponent("Container_Production/Text_Attri2",'TextMeshWrapper')
end

function ComposeGemView:ClearAttri()
	self._txtAttris[1].text = ""
	self._txtAttris[2].text = ""
end