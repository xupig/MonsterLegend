--ChatQuestionView
local ChatQuestionView = Class.ChatQuestionView(ClassTypes.BaseLuaUIComponent)

ChatQuestionView.interface = GameConfig.ComponentsConfig.Component
ChatQuestionView.classPath = "Modules.ModuleChat.ChildView.ChatQuestionView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GuildDataHelper = GameDataHelper.GuildDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

local QUESTION_DURATION = 60
local result_scale_time = 0.5
local result_time = 1

function ChatQuestionView:Awake()
    self:InitView()
    self.timer = -1
    self.update = function() self:OneSecTick() end
    self:InitListerFunc()
end

function ChatQuestionView:InitListerFunc()
    self._refreshGuildBanquetQuestionInfo = function() self:RefreshQuestionInfo() end
    self._refreshGuildBanquetQuestionResult = function(data) self:ShowAnswerResult(data) end
end

function ChatQuestionView:OnDestroy()
end

function ChatQuestionView:OnEnable()
    self:AddEventListeners()
    self:RefreshQuestionInfo()
    self.timer = TimerHeap:AddSecTimer(0, 1, 0, self.update)
end

function ChatQuestionView:OnDisable()
    self:RemoveEventListeners()
    if self.timer ~= -1 then
        TimerHeap:DelTimer(self.timer)
    end
    self._correcrtImageGo:SetActive(false)
    self._errorImageGo:SetActive(false)
end

function ChatQuestionView:ShowView()

end

function ChatQuestionView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.Set_Guild_Banquet_State, self._refreshGuildBanquetQuestionInfo)

    EventDispatcher:AddEventListener(GameEvents.Refresh_Guild_Banquet_Question_Result, self._refreshGuildBanquetQuestionResult)
end

function ChatQuestionView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Set_Guild_Banquet_State, self._refreshGuildBanquetQuestionInfo)
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Guild_Banquet_Question_Result, self._refreshGuildBanquetQuestionResult)
end

function ChatQuestionView:InitView()
    self._closeButton = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._closeButton,function() self:OnCloseButtonClick() end)
    self._questionTitleText = self:GetChildComponent("Image_Title/Text_Title", "TextMeshWrapper")
    self._questionText = self:GetChildComponent("Text_Question", "TextMeshWrapper")
    self._questionTimeText = self:GetChildComponent("Text_Time", "TextMeshWrapper")

     --答题答案显示
     self._correcrtImageGo = self:FindChildGO("Container_Result/Image_Correct")
     self._correcrtImageScale = self._correcrtImageGo.gameObject:AddComponent(typeof(XGameObjectTweenScale))
     self._errorImageGo = self:FindChildGO("Container_Result/Image_Error")
     self._errorImageScale = self._errorImageGo.gameObject:AddComponent(typeof(XGameObjectTweenScale))
end

function ChatQuestionView:OnCloseButtonClick()
    self.gameObject:SetActive(false)
end

function ChatQuestionView:RefreshQuestionInfo()
    local data = GuildBanquetManager:GetStateData()
    self._questionText.text = GuildDataHelper:GetGuildBanquetQuestionDesc(data[3])
    self._questionTitleText.text = LanguageDataHelper.CreateContentWithArgs(53172, {["0"]=data[5]})
    self.endTime = data[4] + QUESTION_DURATION
    local curTime = DateTimeUtil.GetServerTime()
    local leaveTime = math.max(data[4] + QUESTION_DURATION - curTime, 0)
    self._questionTimeText.text = LanguageDataHelper.CreateContentWithArgs(53173, {["0"]=leaveTime})
end

function ChatQuestionView:OneSecTick()
    local curTime = DateTimeUtil.GetServerTime()
    local leaveTime = math.max(self.endTime - curTime, 0)
    self._questionTimeText.text = LanguageDataHelper.CreateContentWithArgs(53173, {["0"]=leaveTime})
end

function ChatQuestionView:ShowAnswerResult(data)
    local right = data[1] == 1
    if right then        
        self._correcrtImageGo.transform.localScale = Vector3.zero
        self._correcrtImageGo:SetActive(true)
        self._correcrtImageScale:SetFromToScale(0, 1, result_scale_time, 1, nil)
        self._resultTimer = TimerHeap:AddTimer((result_scale_time + result_time) * 1000, 0, 0, function()
            self._correcrtImageGo:SetActive(false)
            self:ClearResultTimer()
        end)
    else
        self._errorImageGo.transform.localScale = Vector3.zero
        self._errorImageGo:SetActive(true)
        self._errorImageScale:SetFromToScale(0, 1, result_scale_time, 1, nil)
        self._resultTimer = TimerHeap:AddTimer((result_scale_time + result_time) * 1000, 0, 0, function()
            self._errorImageGo:SetActive(false)
            self:ClearResultTimer()
        end)
    end
 end

 function ChatQuestionView:ClearResultTimer()
    if self._resultTimer ~= nil then
        TimerHeap:DelTimer(self._resultTimer)
        self._resultTimer = nil
    end
 end