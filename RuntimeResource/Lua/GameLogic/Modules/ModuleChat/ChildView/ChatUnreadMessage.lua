--ChatUnreadMessage
local ChatUnreadMessage = Class.ChatUnreadMessage(ClassTypes.BaseLuaUIComponent)

ChatUnreadMessage.interface = GameConfig.ComponentsConfig.Component
ChatUnreadMessage.classPath = "Modules.ModuleChat.ChildView.ChatUnreadMessage"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function ChatUnreadMessage:Awake()
    self:InitVar()
    self:InitComps()
end

function ChatUnreadMessage:OnDestroy()
end

function ChatUnreadMessage:InitVar()
    self._unReadMsgNum = 0
end

function ChatUnreadMessage:InitComps()
    self._textMsg = self:GetChildComponent("Text_UnreadMessage", "TextMeshWrapper")

    self._button = self:FindChildGO("Image_Light")
    self._csBH:AddClick(self._button, function() self:OnClick() end )
end

function ChatUnreadMessage:AddMsgNum()
    self._unReadMsgNum = self._unReadMsgNum + 1
    self:UpdateView()
end

function ChatUnreadMessage:UpdateView()
    self._textMsg.text = LanguageDataHelper.CreateContent(112,{['0'] = self._unReadMsgNum})
    self:SetActive(true)
end

function ChatUnreadMessage:OnClick()
    EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
    self._unReadMsgNum = 0
end