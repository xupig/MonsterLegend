--ChatContent
local ChatContent = Class.ChatContent(ClassTypes.BaseLuaUIComponent)

ChatContent.interface = GameConfig.ComponentsConfig.Component
ChatContent.classPath = "Modules.ModuleChat.ChildView.ChatContent"

require "Modules.ModuleChat.ChildComponent.ChatContentItem"

local ChatContentItem = ClassTypes.ChatContentItem

local ChatManager = PlayerManager.ChatManager

local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList

local public_config = GameWorld.public_config
--对应public_config里频道类型的映射
local chatReflect = {}

function ChatContent:Awake()
    self:InitChannelRelf()
    self._curIndex = 1
    self:InitVar()
    self:InitComps()
end

function ChatContent:InitChannelRelf()
    chatReflect[public_config.CHAT_CHANNEL_SYSTEM] = 1
    chatReflect[public_config.CHAT_CHANNEL_WORLD] = 2
    chatReflect[public_config.CHAT_CHANNEL_VICINITY] = 3
    chatReflect[public_config.CHAT_CHANNEL_SEEK_TEAM] = 4
    chatReflect[public_config.CHAT_CHANNEL_TEAM] = 5
    chatReflect[public_config.CHAT_CHANNEL_GUILD] = 6
    chatReflect[public_config.CHAT_CHANNEL_CROSS_WORLD] = 7
    chatReflect[public_config.CHAT_CHANNEL_HORN] = 8
end

function ChatContent:OnDestroy()

end

function ChatContent:InitEvent() 
    -- if self._viewList[ChatManager.CurChannel] then
    --     self._viewList[ChatManager.CurChannel]:SetScrollRectState(true)
    -- end
    EventDispatcher:AddEventListener(GameEvents.CHAT_CONTENT_ADD,self._updateContentCb)
    EventDispatcher:AddEventListener(GameEvents.CHAT_CONTENT_VIEW_CHANGE, self._changeContentCb)
    EventDispatcher:AddEventListener(GameEvents.CHAT_SLIDE_END, self._updateListViewCb)
end

function ChatContent:RemoveEvent()
    -- if self._viewList[ChatManager.CurChannel] then
    --     self._viewList[ChatManager.CurChannel]:SetScrollRectState(false)
    -- end
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_CONTENT_ADD,self._updateContentCb)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_CONTENT_VIEW_CHANGE, self._changeContentCb)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_SLIDE_END, self._updateListViewCb)
end


function ChatContent:InitVar()
    self._contentNum = 7
    self._contentGoList = {}
    self._scrollList = {}
    self._viewList = {}
    self._updateIteming = false
    self._needUpdateItem = false
    self._contentNumList = {}

    self._updateContentCb = function(channel_id)self:UpdateContent(channel_id) end
    self._changeContentCb = function()self:ChangeContent() end
    self._updateListViewCb = function()self:UpdateListView() end
end

function ChatContent:InitComps()
    self:InitScrollViewList()
end

function ChatContent:ChangeContent()
    self._curIndex = chatReflect[ChatManager.CurChannel]
    
    for i = 1, self._contentNum do
        if i == self._curIndex then
            self._contentGoList[i]:SetActive(true)
            self._viewList[i]:SetCheckEnded(true)
        else
            self._contentGoList[i]:SetActive(false)
            self._viewList[i]:SetCheckEnded(false)
        end
    end
    
    self:UpdateListView()
end

function ChatContent:UpdateContent(channel_id)
    --LoggerHelper.Error("ChatContent:UpdateContent"..channel_id)

    if channel_id == ChatManager.CurChannel then --当前频道
        local channelIndex = chatReflect[channel_id]
        if self._scrollList[channelIndex]:InBottom() then
            self:UpdateListView()
        else
            EventDispatcher:TriggerEvent(GameEvents.CHAT_MSG_NUM_ADD)
        -- self:UpdateListViewData()
        end
    else
        EventDispatcher:TriggerEvent(GameEvents.CHAT_HAS_MSG_SIGN, channel_id)
    end
end

function ChatContent:InitScrollViewList()
    self._contentGoList[1] = self:FindChildGO("ScrollViewList")
    for i = 2, self._contentNum do
        self._contentGoList[i] = self:CopyUIGameObject('ScrollViewList', '')
    end
    
    for i = 1, self._contentNum do
        local scrollView, list = UIComplexList.AddScrollViewComplexList(self._contentGoList[i])
        self._scrollList[i] = list
        self._viewList[i] = scrollView
        self._scrollList[i]:SetItemType(ChatContentItem)
        self._scrollList[i]:SetPadding(20, 0, 0, 0)
        self._scrollList[i]:SetGap(5, 0)
        self._scrollList[i]:SetDirection(UIList.DirectionTopToDown, 1, -1)
        self._scrollList[i]:AppendItemDataList({})
        self._scrollList[i]:UseObjectPool(true)
        local itemClickedCB =
            function(idx)
                self:OnListItemClicked(idx)
            end
        -- self._scrollList[i]:SetItemClickedCB(itemClickedCB)
        self._viewList[i]:SetOnMoveEndCB(function()self:DifficultScrollEnd() end)
    end
end

-- 滑动到最后
function ChatContent:OnGotoTail()
    if not (self._scrollList[self._curIndex]:InBottom()) then
        self._scrollList[self._curIndex]:MoveToBottom()
    -- EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
    end
end

function ChatContent:OnListItemClicked(idx)
-- LoggerHelper.Log("OnListItemClicked:"..idx)
end


function ChatContent:UpdateListView()
    self:UpdateListViewData()
    if ChatManager:GetSliderEnable() then
        self:OnGotoTail()
    end
end

-- 更新LIST数据
function ChatContent:UpdateListViewData()

    if self._updateIteming then --是否在更新中
        self._needUpdateItem = true
        --LoggerHelper.Error("是否在更新中")
        return
    end
    --LoggerHelper.Error("是否在更新中")
    --在更新ing
    self._updateIteming = true
    --取要更新的数据
    local listData = ChatManager.GetChatContents()
    local addDataList = {}
    if listData then
        for k, v in ipairs(listData) do
            if v.show == false then
                v.show = true
                table.insert(addDataList, v)
            end
        end
    end


    -- if self._curIndex == public_config.CHAT_CHANNEL_SYSTEM then
    --     local listDataHorn =  ChatManager.ChatContent[public_config.CHAT_CHANNEL_HORN]
    --     if listDataHorn then
    --         for k, v in pairs(listDataHorn) do
    --             if v.show == false then
    --                 v.show = true
    --                 table.insert(addDataList, v)
    --             end
    --         end
    --     end
    -- end

    local updateChannel = self._curIndex

    -- if self._contentNumList[updateChannel] == nil then
    --     self._contentNumList[updateChannel] = 0
    -- end

    if self._contentNumList[updateChannel] == nil then
        self._contentNumList[updateChannel] = 0
    end
    
    -- LoggerHelper.Log(listData)
    --把多的ITEM删除
    local curCount = self._contentNumList[updateChannel]-- self._scrollList[updateChange]:GetCurCount()
    local addCount = #addDataList
    if addCount > 0 then
        local allCount = curCount + addCount
         --LoggerHelper.Error(updateChannel .. 'curCount:' .. curCount)
        local removeCount = allCount - ChatManager.MAX_CHAT_NUM
         --LoggerHelper.Error('allCount:' .. allCount)
        if removeCount > 0 then
            self._contentNumList[updateChannel] = self._contentNumList[updateChannel] - removeCount
            self._scrollList[updateChannel]:RemoveItemRange(0, removeCount, false)
        end
        --把新ITEM加进去
        self._contentNumList[updateChannel] = self._contentNumList[updateChannel] + #addDataList
        self._scrollList[updateChannel]:AppendItemDataList(addDataList)
    end
    --更新完成
    self._updateIteming = false
    
    --在更新过程是否有新数据来
    if self._needUpdateItem then
        self._needUpdateItem = false
        self:UpdateListViewData()
    end
end

-- 滚动到最后了
function ChatContent:DifficultScrollEnd()
    if self._scrollList[self._curIndex]:InBottom() then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
    end
end
