--ChatBottomChat
local ChatBottomChat = Class.ChatBottomChat(ClassTypes.BaseLuaUIComponent)

ChatBottomChat.interface = GameConfig.ComponentsConfig.Component
ChatBottomChat.classPath = "Modules.ModuleChat.ChildView.ChatBottomChat"

require "Modules.ModuleChat.ChildView.ChatVoiceButtonView"

local ChatVoiceButtonView = ClassTypes.ChatVoiceButtonView

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local public_config = GameWorld.public_config

function ChatBottomChat:Awake()
    self:InitComps()
    self:InitEvent()
end

function ChatBottomChat:OnDestroy()
end

function ChatBottomChat:InitComps()
    self._buttonVoice = self:FindChildGO("Button_Voice")
    -- self._csBH:AddClick(self._buttonVoice, function() self:OnClickVoice() end )
    self._buttonVoiceView = self:AddChildLuaUIComponent('Button_Voice', ChatVoiceButtonView)
    
    self._buttonAnnounce = self:FindChildGO("Button_Announce")
    self._csBH:AddClick(self._buttonAnnounce, function()self:OnClickAnnounce() end)
    
    self._buttonEmoji = self:FindChildGO("Button_Emoji")
    self._csBH:AddClick(self._buttonEmoji, function()self:OnClickEmoji() end)
    
    self._buttonSend = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._buttonSend, function()self:OnClickSend() end)
    
    self._inputText = self:GetChildComponent("InputField", 'InputFieldMeshWrapper')
    --self._inputText.placeholder.text = ""--LanguageDataHelper.CreateContent(124)
    -- 最多输入100个字符
    self._inputText.characterLimit = 100
    
    self._chatType = 1

-- if GameLoader.SystemConfig.ForBanShu then
--     self._buttonVoice:SetActive(false)
-- end
end

function ChatBottomChat:InitEvent()

end

function ChatBottomChat:OnClickAnnounce()
    GUIManager.ShowPanel(PanelsConfig.ChatAnnounce)
end

function ChatBottomChat:OnClickVoice()

end

-- 1世界聊天 2 快捷聊天 3 好友聊天
function ChatBottomChat:SetChatType(type)
    self._chatType = type
end

function ChatBottomChat:OnClickEmoji()
    --LoggerHelper.Error("OnClickEmoji"..self._chatType)

    GUIManager.ShowPanel(PanelsConfig.ChatEmoji,self._chatType or 1)
end


function ChatBottomChat:OnClickSend()
    local textStr = self._inputText.text
    if textStr == '' then
        return
    end

    local liststr =  self._liststr
    local serData = ChatManager:GetToSerData()
    for i=#serData,1,-1 do
        local itemName = ItemDataHelper.GetItemName(tonumber(serData[i][1][public_config.ITEM_KEY_ID]))
        local itemNameText = '['..itemName..']'
        local start_i = 0;
        local end_j = 0;
        local sunstr = "";
        start_i,end_j,sunstr=string.find(textStr,itemNameText,1,-2);

        local str = ""
        local str2 = ""
        if start_i then
            if start_i == 1 then
                str = ""
            else
                str = string.sub(textStr,1,start_i-1)
            end

            if end_j == string.len(textStr) then
                str2 = ""
            else
                str2 = string.sub(textStr,end_j+1,string.len(textStr))
            end
        
            textStr = str.."&&"..i.."&&"..str2
        end
    end

    EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, textStr,nil,liststr)
    self._inputText.text = ''
    
    EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)


end

----------------------------------------------------------------------------
function ChatBottomChat:SetFastChat(text, liststr)
    local textStr = self._inputText.text
    textStr = textStr .. text
    self._inputText.text = textStr
    self._liststr = liststr


    -- self._inputText:MoveTextEnd(false)
    -- self._inputText.selectionFocusPosition = 3
    -- self._inputText:ActivateInputField()
    -- self._inputText.caretPosition = #textStr
    -- self._inputText:ActivateInputField()
    -- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)

end


function ChatBottomChat:SetFastPosChat(text)
    -- LoggerHelper.Error('ChatBottomChat:SetFastChat:' .. text)
    local textStr = self._inputText.text
    textStr = textStr .. text
    self._inputText.text = textStr
    
    local textStr = self._inputText.text
    if textStr == '' then
        return
    end
    
    --textStr = ChatManager.HandleEmoji(textStr)
    EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, textStr)
    self._inputText.text = ''
    
    EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
-- self._inputText:MoveTextEnd(false)
-- self._inputText.selectionFocusPosition = 3
-- self._inputText:ActivateInputField()
-- self._inputText.caretPosition = #textStr
-- self._inputText:ActivateInputField()
-- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)
end



-- <sprite=\"EmojiOne\" anim=\"0, 12, 12\"> 表情
function ChatBottomChat:OnFocusInputFiled()
-- self._inputText:ActivateInputField()
-- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)
end
