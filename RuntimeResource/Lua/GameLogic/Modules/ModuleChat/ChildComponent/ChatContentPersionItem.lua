--ChatContentPersionItem
local ChatContentPersionItem = Class.ChatContentPersionItem(ClassTypes.BaseLuaUIComponent)

ChatContentPersionItem.interface = GameConfig.ComponentsConfig.Component
ChatContentPersionItem.classPath = "Modules.ModuleChat.ChildComponent.ChatContentPersionItem"

local UIChatTextBlock = ClassTypes.UIChatTextBlock
local RectTransform = UnityEngine.RectTransform

local ChatManager = PlayerManager.ChatManager
--local CharacterDataHelper = GameDataHelper.CharacterDataHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GUIManager = GameManager.GUIManager
require "UIComponent.Extend.PointableButton"
local PointableButton = ClassTypes.PointableButton
local CommonIconType = GameConfig.EnumType.CommonIconType
local FashionDataHelper = GameDataHelper.FashionDataHelper

function ChatContentPersionItem:Awake()
    self:InitVar()
    self:InitComps()
    self:InitEvent()
end

function ChatContentPersionItem:OnDestroy()

end

function ChatContentPersionItem:IsMe(value)
    self._isMe = value
end

function ChatContentPersionItem:InitVar()
    self._isVisible = true
end

function ChatContentPersionItem:InitEvent()
end

function ChatContentPersionItem:InitComps()
    self._textBlock = UIChatTextBlock.AddChatTextBlock(self.gameObject)
    self._textMessageTrans = self:FindChildGO("Text_Message").transform
    self._textMessage = self:GetChildComponent("Text_Message", "TextMeshWrapper")
   
    self._textMessage.text = ''
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Message')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)
    
    self._containerMessageBG = self:FindChildGO("Container_BG/Image_MessageBG")
    self._MessageBGRect = self:GetChildComponent("Container_BG/Image_MessageBG", "RectTransform")
    
    --self._nameSize = self:GetChildComponent("Container_BG/Image_NameBG", "RectTransform") --nameBGGo:GetComponent(typeof(RectTransform))
    self._textName = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._headContainer = self:FindChildGO("Container_HeadIcon")
    self._headFrameContainer = self:FindChildGO("Container_HeadFrame")
    --self._btnHead = self:FindChildGO("Button_Head")
    
    self._btnHead = self:AddChildLuaUIComponent("Button_Head", PointableButton)
    self._btnHead:SetOnPointerDownFunc(function(pointerEventData) self:OnOpenPlayerMenu(pointerEventData) end)
    
    --self._csBH:AddClick(self._btnHead, function() self:OnOpenPlayerMenu() end )
    -- 语音
    self._voiceContainer = self:FindChildGO("Container_Voice")
    self._voiceContainer:SetActive(false)
    self._voiceButton = self:FindChildGO("Container_Voice/Button_Play")
    self._voiceRed = self:FindChildGO("Container_Voice/Image_Sign")
    self._csBH:AddClick(self._voiceButton, function() self:OnPlayVoice() end )
    self._voiceTimeText = self:GetChildComponent("Container_Voice/Text", "TextMeshWrapper")
    self._messageConteiner = self:FindChildGO('Container_Message')
end

function ChatContentPersionItem:OnClickLink(linkId)
    -- LoggerHelper.Log('linkId:' .. linkId)
    if linkId ~= nil and linkId ~= '' then
        ChatManager:OnClickTextLink(linkId)
    end
end

local posVec = Vector2.zero
function ChatContentPersionItem:OnOpenPlayerMenu(pointerEventData)
    if self._isMe then
        return
    end
    local d = {}
    d.playerName = self._data.name
    d.dbid = self._data.dbid
    posVec = pointerEventData.position

    --LoggerHelper.Error(posVec.x..","..posVec.y)
    d.listPos = posVec
    GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu,d)
end

function ChatContentPersionItem:UpdateItem(data)
    local headIcon = RoleDataHelper.GetHeadIconByVocation(data.vocation)
    GameWorld.AddIcon(self._headContainer,headIcon)
    self._data = data
    local nameStr = data.name

    if data.vipLevel > 0 then
        nameStr = nameStr.."  VIP"..data.vipLevel
    end

    if data.serId > 0 then
        nameStr = nameStr.." "..data.serIdName
    end

    --头像框
    if (data.photoFrameId or 0) > 0 then
        local modelId = FashionDataHelper.GetFashionModel(data.photoFrameId)
        GameWorld.AddIcon(self._headFrameContainer,modelId,0,false,1,true,false)
    else
        GameWorld.AddIcon(self._headFrameContainer,9047,0,false,1,true,false)
    end

    --气泡
    if (data.chatFrameId or 0) > 0 then
        local modelId = FashionDataHelper.GetFashionModel(data.chatFrameId)
        GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,modelId,self._isMe)
        GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,modelId,self._isMe)
        self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble"..modelId.."(Clone)".."/Image", "ImageWrapper")
    else
        if self._isMe then
            GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,0,true)
            GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,0,true)
            self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble".."0".."(Clone)".."/Image", "ImageWrapper")
        else
            GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,1)
            GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,1)
            self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble".."1".."(Clone)".."/Image", "ImageWrapper")
        end
    end

    self._textName.text = nameStr
    --self:UpdateNameBG(data.name)
    self._textMessage.text = ''
    local newContent = ''
    if string.find(data.content,'<vmsg=' ) then
        if self:HandlerVoice(data.content) then
            newContent = self:HandlerVoiceContent(data.content)
            local textHeight = self:UpdateVoiceText(newContent)
            local textWidth = self._textMessage.preferredWidth + 20
            self:UpdateTextBG(textWidth,textHeight+20)
            if self._isMe then
                --LoggerHelper.Error("self._isMe2121===================================".. data._contentStr)
                self._MessageBGRect.anchoredPosition = Vector2(-85,-82)
                if textWidth < 268 then
                    self._textMessageTrans.anchoredPosition = Vector2(260-textWidth+30,-86)
                else
                    self._textMessageTrans.anchoredPosition = Vector2(-15,-86)
                end
            else
                self._MessageBGRect.anchoredPosition = Vector2(76,-82)
                self._textMessageTrans.anchoredPosition = Vector2(112,-86)
            end

            -- if ChatManager:GetIsAutoVoice() then
            --     local activeSelf = self._voiceRed.activeSelf
            --     if activeSelf then
            --         self:OnPlayVoice()
            --     end
            -- end

            return textHeight+20
        end
    end

    self._textMessage.text = data.content
    local height = self._textMessage.preferredHeight
    local width = self._textMessage.preferredWidth
    if self._isMe then
        if width < 268 then
            self._textMessageTrans.anchoredPosition = Vector2(260-width,-60)
        else
            self._textMessageTrans.anchoredPosition = Vector2(-16,-60)
        end
    end
    -- LoggerHelper.Log("self._textMessage.flexibleHeight:" .. self._textMessage.flexibleHeight)
    -- LoggerHelper.Log("self._textMessage.preferredHeight:" .. self._textMessage.preferredHeight)
    -- LoggerHelper.Log("self._textMessage.renderedHeight:" .. self._textMessage.renderedHeight)
    self:UpdateTextBG(width + 40,height + 58)
    return height
end

-- 语音 文字特殊处理
function ChatContentPersionItem:UpdateVoiceText(content)
    self._messageConteiner:SetActive(false)
    self._textMessage.text = content
    return self._textMessage.preferredHeight + 16
end

-- 更新 文本 背景 大小
function ChatContentPersionItem:UpdateTextBG(width,height)
    if width > 320 then
        width = 320
    end
    self._MessageBGRect.sizeDelta = Vector2.New(width, height )
    if self._starState1Image then
        self._starState1Image:SetAllDirty()
    end
end

-- 更新名字背景长度
-- function ChatContentPersionItem:UpdateNameBG(name)
--     self._nameSize.sizeDelta = Vector2.New(#name * 10 + 30, 24)
-- end

function ChatContentPersionItem:SetMessageBlock(channel, paramstr, liststr, content)
    self._messageConteiner:SetActive(true)
    local height = 0
    if self._textBlock ~= nil then
        content = ChatManager:CheckContentLink(content)
        height = height + self._textBlock:AddTextBlock(channel, paramstr, liststr, content)
    end
    return height
end

function ChatContentPersionItem:SetVisible(value)
    self:SetActive(value)
    self._isVisible = value
end

function ChatContentPersionItem:IsVisible()
    return self._isVisible
end

-- 播放语音
function ChatContentPersionItem:OnPlayVoice()
    self._voiceRed.gameObject:SetActive(false)
    ChatManager.OnStartPlayVoice(self._voiceId,self._voiceTime)
end

-- 是否显示语音
function ChatContentPersionItem:OnShowVoice(value)
    self._voiceContainer:SetActive(value)
end

-- 是否是语音文件
function ChatContentPersionItem:HandlerVoice(content)
    if  string.find( content,'<vmsg=' ) then
        self:OnShowVoice(true)
        self:HandlerVoiceData(content)
        self._voiceTimeText.text = string.format("%.1f", tonumber( self._voiceTime ) / 1000) 
        return true
    else
        self:OnShowVoice(false)
        return false
    end
    return false
end

-- 语音数据
function ChatContentPersionItem:HandlerVoiceData(content)
    self._voiceId = self:GetDataByContent(content,'<vmsg=' )
    self._voiceTime = self:GetDataByContent(content,'<vtime=' )
end

function ChatContentPersionItem:GetDataByContent(content,pattern)
    local chatId_start_i,chatId_start_j = string.find( content,pattern )
    local chatId_end_i,chatId_end_j = string.find( content,'>',chatId_start_j)
    local data = string.sub( content, chatId_start_j + 1 , chatId_end_i - 1 )
    return data
end

function ChatContentPersionItem:HandlerVoiceContent(content)
    local pattern = '<vmsg=' .. self._voiceId .. '>'
    content = string.gsub(content,pattern,'')
    pattern = '<vtime=' .. self._voiceTime .. '>'
    content = string.gsub(content,pattern,'')
    --content = '\n' .. content
    return content
end