-- ChatContentItem.lua
local ChatContentItem = Class.ChatContentItem(ClassTypes.UIListItem)
ChatContentItem.interface = GameConfig.ComponentsConfig.Component
ChatContentItem.classPath = "Modules.ModuleChat.ChildComponent.ChatContentItem"

require "Modules.ModuleChat.ChildComponent.ChatContentPersionItem"
local ChatContentPersionItem = ClassTypes.ChatContentPersionItem

require "Modules.ModuleChat.ChildComponent.ChatContentSysItem"
local ChatContentSysItem = ClassTypes.ChatContentSysItem

local ChatManager = PlayerManager.ChatManager
local public_config = GameWorld.public_config

function ChatContentItem:Awake()
    self._base.Awake(self)
    self:InitComps()
end

function ChatContentItem:OnDestroy()

end

function ChatContentItem:InitComps()
    self._systemView = self:AddChildLuaUIComponent('Container_Notice', ChatContentSysItem)
    self._persionView = self:AddChildLuaUIComponent('Container_Message', ChatContentPersionItem)
    self._persionView:IsMe(false)
    self._persionSelfView = self:AddChildLuaUIComponent('Container_MessageSelf', ChatContentPersionItem)
    self._persionSelfView:IsMe(true)
    -- self._items = self:FindChildGO('Container_Items')
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
end

--override
function ChatContentItem:OnRefreshData(data)
    
    local height = 10
    --LoggerHelper.Error(tostring(data.channel) .. '=====================================================')
    if data.channel == public_config.CHAT_CHANNEL_SYSTEM or data.channel == public_config.CHAT_CHANNEL_SEEK_TEAM or 
    data.channel == public_config.CHAT_CHANNEL_HORN or
         data.name == '' then
        if not(self._systemView:IsVisible()) then
            self._systemView:SetVisible(true)
            self._systemView:SetChannel(data.channel)
            self._persionView:SetVisible(false)
            self._persionSelfView:SetVisible(false)
        end
        height = self._systemView:UpdateItem(data)
    else
        -- print(GameWorld.Player().id .. 'GameWorld.Player().dbid' .. GameWorld.Player().cs.dbid .. ' ' .. data.dbid)
        local uuid = 1
        if data.show ~= nil then
            uuid = data.uuid
            data.show = true
        else
            local type = data.type
            if type == 0 then --类型 0 自己说话 1 好友说话
                uuid = GameWorld.Player().cross_uuid
            end
        end
        
        -- LoggerHelper.Error(GameWorld.Player().cross_uuid .. 'GameWorld.Player().cross_uuid')
        if uuid == GameWorld.Player().cross_uuid then
            -- self._persionSelfView:SetActive(true)
            if not(self._persionSelfView:IsVisible()) then
                self._systemView:SetVisible(false)
                self._persionView:SetVisible(false)
                self._persionSelfView:SetVisible(true)
            end
            height = self._persionSelfView:UpdateItem(data)
        else
            -- self._persionView:SetActive(true)
            if not(self._persionView:IsVisible()) then
                self._systemView:SetVisible(false)
                self._persionView:SetVisible(true)
                self._persionSelfView:SetVisible(false)
            end
            height = self._persionView:UpdateItem(data)
        end
    
    end
    self:UpdateItemHeight(height)
end

function ChatContentItem:UpdateItemHeight(height)
    local size = self._rectTransform.sizeDelta
    size.y = height + 80
    self._rectTransform.sizeDelta = size
    
    -- self._items.transform.localPosition = Vector3.New(0, 0, 0)
end
