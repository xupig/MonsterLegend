-- ChatTabCom.lua
-- 面板功能tab组件
local ChatTabCom = Class.ChatTabCom(ClassTypes.BaseLuaUIComponent)

ChatTabCom.interface = GameConfig.ComponentsConfig.Component
ChatTabCom.classPath = "Modules.ModuleChat.ChildComponent.ChatTabCom"

function ChatTabCom:Awake()
	self._imgBg = self:FindChildGO("Background")
	self._imgCheckMark = self:FindChildGO("Checkmark")

	self._imgRedPoint = self:FindChildGO("Image_RedPoint")
	self._imgRedPoint:SetActive(false)

	self._functionOpen = false
end

function ChatTabCom:SetSelected(isSelected)
	self._imgBg:SetActive(not isSelected)
	self._imgCheckMark:SetActive(isSelected)
end

--功能是否已开启
function ChatTabCom:SetFunctionOpen(functionOpen)
	self._functionOpen = functionOpen
end

function ChatTabCom:GetFunctionOpen()
	return self._functionOpen
end

-- --是否显示红点
-- function ChatTabCom:SetRedPointActive(isActive)
-- 	-- LoggerHelper.Error("ChatTabCom:SetRedPointActive(isActive) ===" .. tostring(isActive))
-- 	self._imgRedPoint:SetActive(isActive)
-- end

-- function ChatTabCom:SetRedPointData(redpointData)
-- 	if self._redpointData ~= nil then
-- 		self._redpointData:SetUpdateRedPointFunc(nil)
-- 	end

-- 	if redpointData ~= nil then
-- 		self._redpointData = redpointData
-- 		self._redpointData:SetUpdateRedPointFunc(self._updateRedPointFunc)
-- 	end
-- end