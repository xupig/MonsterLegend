require 'UIComponent.Extend.PlayerHead'

-- ChatSmallItem.lua
local ChatSmallItem = Class.ChatSmallItem(ClassTypes.UIListItem)
ChatSmallItem.interface = GameConfig.ComponentsConfig.Component
ChatSmallItem.classPath = "Modules.ModuleChat.ChildComponent.ChatSmallItem"

local ChatManager = PlayerManager.ChatManager
local PlayerHead = ClassTypes.PlayerHead
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local UIChatTextBlock = ClassTypes.UIChatTextBlock
local RectTransform = UnityEngine.RectTransform
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local LinkTypesMap = ChatConfig.LinkTypesMap

function ChatSmallItem:Awake()
	self:InitComps()
    self:InitNoticeComps()
    self:InitMessageComps()
    self:InitEvent()
end

function ChatSmallItem:InitEvent()
end

function ChatSmallItem:InitComps()
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    self._containerChannel = self:FindChildGO("Container_content/Container_Channel")
end

--系统消息
function ChatSmallItem:InitNoticeComps()
	self._containerNotice = self:FindChildGO("Container_content/Container_Notice")
	-- self._containerNotice:SetActive(true)
    --self._buttonSize = self:GetChildComponent("Conatiner_Notice/Button_Join", "RectTransform")

    self._textMessageNotice = self:GetChildComponent("Container_content/Container_Notice/Text_Notice", "TextMeshWrapper")

    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Container_content/Container_Notice/Text_Notice')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)
    -- self._typeIcon = self:FindChildGO('Container_TypeIcon')

    -- self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Container_content/Container_Notice/Text_Notice')
    -- self._linkTextMesh:SetOnClickCB(function(linkIndex,position) self:OnClickLink(linkIndex,position) end)

    -- GameWorld.AddIcon(self._typeIcon, ChatConfig.System)
end

function ChatSmallItem:OnRefreshData(data)
    self._data = data
    GameWorld.AddIcon(self._containerChannel, ChatManager:GetChatTypeIcon(data.channel))
    --系统消息
    if data.channel == public_config.CHAT_CHANNEL_SYSTEM or data.channel == public_config.CHAT_CHANNEL_SEEK_TEAM then
	    self._textMessageNotice.text = data.content
	    local height = self._textMessageNotice.preferredHeight
	    self:UpdateItemHeight(height)
        self._containerNotice:SetActive(true)
        self._containerMessage:SetActive(false)
        
	--玩家聊天
	else
        self._containerNotice:SetActive(false)
        self._containerMessage:SetActive(true)
		self:UpdateWorldItem(data)
	end
end

function ChatSmallItem:OnClickLink(linkId)
    --LoggerHelper.Error('linkId:' .. linkId)
    if linkId ~= nil and linkId ~= '' then
        local bs = string.split(linkId, ",")
        if bs[1] == LinkTypesMap.Team or bs[1] == LinkTypesMap.Trade then --组队链接
            ChatManager:OnClickTextLink(linkId)
            return
        end
    end

    if self._data then
        local itemData = self._data
        if public_config.CHAT_CHANNEL_HORN == itemData.channel then
            GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = public_config.CHAT_CHANNEL_WORLD})
        else
            GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = itemData.channel})
        end
    else
        GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = public_config.CHAT_CHANNEL_WORLD})
    end

end

------------------------------------------Message-------------------------------------

function ChatSmallItem:InitMessageComps()
	self._containerMessage = self:FindChildGO("Container_content/Container_Message")

    --self._textBlock = UIChatTextBlock.AddChatTextBlock(self._containerMessage.gameObject)
    
    self._textMessage = self:GetChildComponent("Container_content/Container_Message/Text_Message", "TextMeshWrapper")
    self._textMessage.text = ''
    -- self._linkTextMesh1 = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Container_content/Container_Message/Text_Message')
    -- self._linkTextMesh1:SetOnClickCB(function(linkIndex,position) self:OnClickLink(linkIndex,position) end)
    
    --self._MessageBGRect = self:GetChildComponent("Container_BG/Image_MessageBG", "RectTransform")
    
    -- 语音
    self._voiceContainer = self:FindChildGO("Container_content/Container_Message/Container_Voice")
    self._voiceContainer:SetActive(false)
    --self._voiceButton = self:FindChildGO("Container_content/Container_Message/Container_Voice/Button_Play")
    --self._csBH:AddClick(self._voiceButton, function() self:OnPlayVoice() end )
    --self._voiceTimeText = self:GetChildComponent("Container_content/Container_Message/Container_Voice/Text", "TextMeshWrapper")
    --self._messageConteiner = self:FindChildGO('Container_Message/Container_Message')
end

--小面板链接不处理
-- function ChatSmallItem:OnClickLink(linkId,position)
--     -- LoggerHelper.Log('linkId:' .. linkId)
--     if linkId ~= nil and linkId ~= '' then
--         ChatManager:OnClickTextLink(linkId)
--         --LoggerHelper.Error(position.x..","..position.y)
--         return
--     end
--     GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = public_config.CHAT_CHANNEL_WORLD})
-- end

-- 世界聊天
function ChatSmallItem:UpdateWorldItem(data)

    -- self:UpdateHeadData(data)
    -- self:UpdateNameBG(data.name)
    self._textMessage.text = ''
    local newContent = ''
    --处理声音
    if self:HandlerVoice(data.content) then
        newContent = self:HandlerVoiceContent(data.content)
        --local textHeight = self:UpdateVoiceText(data.content)
        -- local textWidth = self._textMessage.preferredWidth + 20
        -- self:UpdateTextBG(textWidth,textHeight)
    else
        newContent = data.content
    end

    if data.channel == public_config.CHAT_CHANNEL_HORN then
        --CommonUIPanel:ShowHornNewsTicke(text, count, callback,channelType)
        local playerInfo = string.split(newContent, ':')
        if playerInfo then
            newContent = tostring(playerInfo[2]) 
        end
    end
    
    self._textMessage.text =  StringStyleUtil.GetColorStringWithTextMesh(data.name..":".." ",StringStyleUtil.yellow1)..newContent
    local height = self._textMessage.preferredHeight
    -- local width = self._textMessage.preferredWidth
    -- -- LoggerHelper.Log("self._textMessage.flexibleHeight:" .. self._textMessage.flexibleHeight)
    -- -- LoggerHelper.Log("self._textMessage.preferredHeight:" .. self._textMessage.preferredHeight)
    -- -- LoggerHelper.Log("self._textMessage.renderedHeight:" .. self._textMessage.renderedHeight)
    self:UpdateItemHeight(height)
end

function ChatSmallItem:UpdateItemHeight(height)
    local size = self._rectTransform.sizeDelta
    size.y = height + 5
    self._rectTransform.sizeDelta = size
    
    -- self._items.transform.localPosition = Vector3.New(0, 0, 0)
end

------------------------------------------------语音---------------------------------------------
-- 语音 文字特殊处理
function ChatSmallItem:UpdateVoiceText(content)
    --self._messageConteiner:SetActive(false)
    self._textMessage.text = content
    return self._textMessage.preferredHeight + 16
end

-- 更新 文本 背景 大小
-- function ChatSmallItem:UpdateTextBG(width,height)
--     self._MessageBGRect.sizeDelta = Vector2.New(width, height )
-- end

-- function ChatSmallItem:SetMessageBlock(channel, paramstr, liststr, content)
--     --self._messageConteiner:SetActive(true)
--     local height = 0
--     if self._textBlock ~= nil then
--         content = ChatManager:CheckContentLink(content)
--         height = height + self._textBlock:AddTextBlock(channel, paramstr, liststr, content)
--     end
--     return height
-- end

-- 播放语音
function ChatSmallItem:OnPlayVoice()
    ChatManager.OnStartPlayVoice(self._voiceId,self._voiceTime)
end

-- -- 是否显示语音
function ChatSmallItem:OnShowVoice(value)
    self._voiceContainer:SetActive(false)
end

-- -- 是否是语音文件
function ChatSmallItem:HandlerVoice(content)
    if  string.find( content,'<vmsg=' ) then
        self:OnShowVoice(true)
        self:HandlerVoiceData(content)

        if ChatManager:GetIsAutoVoice() then
            self:OnPlayVoice()
        end
        --self._voiceTimeText.text = tonumber( self._voiceTime ) / 1000
        return true
    else
        self:OnShowVoice(false)
        return false
    end
    return false
end

-- -- 语音数据
function ChatSmallItem:HandlerVoiceData(content)
    self._voiceId = self:GetDataByContent(content,'<vmsg=' )
    self._voiceTime = self:GetDataByContent(content,'<vtime=' )
end

function ChatSmallItem:GetDataByContent(content,pattern)
    local chatId_start_i,chatId_start_j = string.find( content,pattern )
    local chatId_end_i,chatId_end_j = string.find( content,'>',chatId_start_j)
    local data = string.sub( content, chatId_start_j + 1 , chatId_end_i - 1 )
    return data
end

function ChatSmallItem:HandlerVoiceContent(content)
    local pattern = '<vmsg=' .. self._voiceId .. '>'
    content = string.gsub(content,pattern,'')
    pattern = '<vtime=' .. self._voiceTime .. '>'
    content = string.gsub(content,pattern,'')
    content = '[语音]' .. content
    return content
end
