-- ChatEmojiItem.lua
local ChatEmojiItem = Class.ChatEmojiItem(ClassTypes.UIListItem)
ChatEmojiItem.interface = GameConfig.ComponentsConfig.Component
ChatEmojiItem.classPath = "Modules.ModuleChat.ChildComponent.ChatEmojiItem"

local UIChatEmojiItem = ClassTypes.UIChatEmojiItem
local ChatDataHelper = GameDataHelper.ChatDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function ChatEmojiItem:Awake()
    self._base.Awake(self)
    self:InitComps()
end

function ChatEmojiItem:OnDestroy() 

end

--override
function ChatEmojiItem:OnRefreshData(data)
    if data.type == 1 then
        -- local EmojiAnimationData = ChatDataHelper.GetEmojiAnimationData(data.content)
        -- self._emoji:SetData(EmojiAnimationData.name)
        self._textMessage.text = PlayerManager.ChatManager.EmojiText(data.id)
    end
end

function ChatEmojiItem:InitComps()
    -- self._emoji = UIChatEmojiItem.AddChatEmojiItem(self.gameObject, 'Container_Icon')
    self._textMessage = self:GetChildComponent("Text_Msg", "TextMeshWrapper")

    --self._buttonRect = self:GetChildComponent("Button_Emoji", "RectTransform")
end

function ChatEmojiItem:ShowEmojiAnimation(value)
-- LoggerHelper.Log(tostring(value) .. '-------------------22-------------')
    -- if self._emoji then
    --     self._emoji:ShowAnimation(value)
    -- end
    
end

-- function ChatEmojiItem:UpdateButtonWidth(content)
--     local num = #content
--     if num < 7 then
--         return
--     end
--     local size = self._buttonRect.sizeDelta
--     size.x = #content * 7
--     self._buttonRect.sizeDelta = size
-- end