-- ChatShowItemListItem.lua
local ChatShowItemListItem = Class.ChatShowItemListItem(ClassTypes.UIListItem)
ChatShowItemListItem.interface = GameConfig.ComponentsConfig.Component
ChatShowItemListItem.classPath = "Modules.ModuleChat.ChildComponent.ChatShowItemListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function ChatShowItemListItem:Awake()
    self._base.Awake(self)
    self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
end

function ChatShowItemListItem:OnDestroy() 

end

--override
function ChatShowItemListItem:OnRefreshData(data)
	self._iconContainer:SetItemData(data)
end