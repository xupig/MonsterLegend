--ChatContentSysItem
local ChatContentSysItem = Class.ChatContentSysItem(ClassTypes.BaseLuaUIComponent)

ChatContentSysItem.interface = GameConfig.ComponentsConfig.Component
ChatContentSysItem.classPath = "Modules.ModuleChat.ChildComponent.ChatContentSysItem"

local TeamManager = PlayerManager.TeamManager
local GUIManager = GameManager.GUIManager
local ChatManager = PlayerManager.ChatManager
local SystemInfoManager = GameManager.SystemInfoManager
local ChatConfig = GameConfig.ChatConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh

function ChatContentSysItem:Awake()
    self:InitVar()
    self:InitComps()
    self:InitEvent()
end

function ChatContentSysItem:OnDestroy()

end

function ChatContentSysItem:InitVar()
    self._isVisible = true
end

function ChatContentSysItem:InitEvent()
end

function ChatContentSysItem:InitComps()
    -- self._buttonJoin = self:FindChildGO("Button_Join")
    -- self._csBH:AddClick(self._buttonJoin, function() self:OnJoin() end )

    --self._buttonSize = self:GetChildComponent("Button_Join", "RectTransform")

    self._textMessage = self:GetChildComponent("Text_Notice", "TextMeshWrapper")

    self._typeIcon = self:FindChildGO('Container_TypeIcon')

    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Notice')
    self._linkTextMesh:SetOnClickCB(function(linkIndex,position) self:OnClickLink(linkIndex,position) end)
end

function ChatContentSysItem:SetChannel(channel)
    GameWorld.AddIcon(self._typeIcon, ChatManager:GetChatTypeIcon(channel))
end

function ChatContentSysItem:OnClickLink(linkId,position)
    -- LoggerHelper.Log('linkId:' .. linkId)
    if linkId ~= nil and linkId ~= '' then
        ChatManager:OnClickTextLink(linkId)
        ChatManager.ClickPosition = position
    end
end

function ChatContentSysItem:UpdateItem(data)
    self._data = data
    self._textMessage.text = data.content
    local height = self._textMessage.preferredHeight
    --self:UpdateButtonSize(height)
    return height - 50
end

-- function ChatContentSysItem:UpdateButtonSize(height)
--     self._buttonSize.sizeDelta = Vector2.New(self._textMessage.preferredWidth,height)
-- end

function ChatContentSysItem:SetVisible(value)
    self:SetActive(value)
    self._isVisible = value
end

function ChatContentSysItem:IsVisible()
    return self._isVisible
end