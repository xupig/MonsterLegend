-- ChatFastChatItem.lua
local ChatFastChatItem = Class.ChatFastChatItem(ClassTypes.UIListItem)
ChatFastChatItem.interface = GameConfig.ComponentsConfig.Component
ChatFastChatItem.classPath = "Modules.ModuleChat.ChildComponent.ChatFastChatItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function ChatFastChatItem:Awake()
    self._base.Awake(self)
    self._text = self:GetChildComponent("Text_Content", "TextMeshWrapper")

    self._btnEdit = self:FindChildGO("Button_Edit")
	self._csBH:AddClick(self._btnEdit, function() self:OnClickEdit() end )
end

function ChatFastChatItem:OnDestroy() 

end

function ChatFastChatItem:OnClickEdit()
	local index = self:GetIndex()+1
	EventDispatcher:TriggerEvent(GameEvents.CHAT_EDIT_FAST_CHAT,true, index)
end

--override
function ChatFastChatItem:OnRefreshData(data)
	self._text.text = data
end