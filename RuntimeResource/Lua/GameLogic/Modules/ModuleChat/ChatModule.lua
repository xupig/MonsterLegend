-- ChatModule.lua  by lhs
ChatModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ChatModule.Init()
    GUIManager.AddPanel(PanelsConfig.Chat, "Panel_Chat", GUILayer.LayerUIPanel, "Modules.ModuleChat.ChatPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.ChatSmall, "Panel_ChatSmall", GUILayer.LayerUIMain, "Modules.ModuleChat.ChatSmallPanel", true, PanelsConfig.IS_NOT_CENTER_ADJUST)
    GUIManager.AddPanel(PanelsConfig.ChatEmoji, "Panel_ChatEmoji", GUILayer.LayerUIPanel, "Modules.ModuleChat.ChatEmojiPanel", true, false)
    GUIManager.AddPanel(PanelsConfig.ChatVoice, "Panel_ChatVoice", GUILayer.LayerUIPanel, "Modules.ModuleChat.ChatVoicePanel", true, false)
    GUIManager.AddPanel(PanelsConfig.ChatAnnounce, "Panel_ChatAnnounce", GUILayer.LayerUIPanel, "Modules.ModuleChat.ChatAnnouncePanel", true, false)
end

return ChatModule