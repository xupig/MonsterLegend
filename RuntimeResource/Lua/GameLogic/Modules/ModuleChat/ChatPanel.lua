-- ChatPanel.lua
local ChatPanel = Class.ChatPanel(ClassTypes.BasePanel)

local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local GUIManager = GameManager.GUIManager
local UIToggleGroup = ClassTypes.UIToggleGroup

require "Modules.ModuleChat.ChildView.ChatBottomChat"
local ChatBottomChat = ClassTypes.ChatBottomChat

require "Modules.ModuleChat.ChildView.ChatUnreadMessage"
local ChatUnreadMessage = ClassTypes.ChatUnreadMessage

require "Modules.ModuleChat.ChildView.ChatContent"
local ChatContent = ClassTypes.ChatContent

require "Modules.ModuleChat.ChildComponent.ChatTabCom"
local ChatTabCom = ClassTypes.ChatTabCom

require "Modules.ModuleChat.ChildView.ChatQuestionView"
local ChatQuestionView = ClassTypes.ChatQuestionView

local ChatManager = PlayerManager.ChatManager

local ChatConfig = GameConfig.ChatConfig
local UIToggle = ClassTypes.UIToggle
local public_config = GameWorld.public_config
local GuildBanquetManager = PlayerManager.GuildBanquetManager
local MapDataHelper = GameDataHelper.MapDataHelper
local SceneConfig = GameConfig.SceneConfig

function ChatPanel:Awake()
    self.disableCameraCulling = true
    self:InitVar()
    self:InitAnimation()
    self:InitComp()
    self:InitFun()
end

function ChatPanel:InitAnimation()
    self._tweenPosition = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
end

function ChatPanel:InitComp()
    self._buttonClose = self:FindChildGO("Button_Clsoe")
    self._csBH:AddClick(self._buttonClose, function()self:OnClosePanelAnimation() end)
    
    self._buttonMask = self:FindChildGO("Button_Mask")
    self._csBH:AddClick(self._buttonMask, function()self:OnClosePanelAnimation() end)
    
    self._chatBottomBan = self:FindChildGO('Container_BottomBan')
    self._chatBottomChat = self:AddChildLuaUIComponent('Container_BottomChat', ChatBottomChat)
    self._chatBottomChat:SetChatType(1)
    --self._chatUnreadMessage = self:AddChildLuaUIComponent('Container_UnreadMessage', ChatUnreadMessage)
    --self._chatUnreadMessage:SetActive(false)
    self._chatContent = self:AddChildLuaUIComponent('Container_Content', ChatContent)
    
    self._channelTabs = {}
    for i=1,7 do
        self._channelTabs[i] = self:AddChildLuaUIComponent('ToggleGroup/Toggle'..i, ChatTabCom)
        self._channelTabs[i]:SetSelected(false)
        local go = self:FindChildGO('ToggleGroup/Toggle'..i)
        self._csBH:AddClick(go, function()self:OnTabBarClick(i) end)
    end

    self._buttonSetting = self:FindChildGO("Button_Setting")
    self._csBH:AddClick(self._buttonSetting, function()self:OnClickSetting() end)

    self._buttonLock = self:FindChildGO("Container_Lock/Button_Slider")
    self._csBH:AddClick(self._buttonLock, function()self:OnClickLock() end)
    self._imgSliderLock = self:FindChildGO("Container_Lock/Button_Slider/Image")
    self._txtUnLock = self:FindChildGO("Container_Lock/Text_Unlock")
    self._isLock = true
    self:OnClickLock()

    self._toggleAutoVoice = UIToggle.AddToggle(self.gameObject, "Toggle_AutoVoice")
    self._toggleAutoVoice:SetIsOn(true)
    ChatManager:SetIsAutoVoice(true)
    self._toggleAutoVoice:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)

    self._questionContainer = self:FindChildGO("Container_Question")
    self._questionView = self:AddChildLuaUIComponent('Container_Question', ChatQuestionView)
    self._questionContainer:SetActive(false)

    self:OnTabBarClick(1)
end

function ChatPanel:OnToggleValueChanged(state)
    ChatManager:SetIsAutoVoice(state)
end

function ChatPanel:OnClickLock()
    self._isLock = not self._isLock
    ChatManager:SetSliderEnable(not self._isLock)
    self._imgSliderLock:SetActive(self._isLock)
    self._txtUnLock:SetActive(self._isLock)
    if self._isLock then
        self._buttonLock.transform.anchoredPosition = Vector3(69,0,0)
    else
        self._buttonLock.transform.anchoredPosition = Vector3(21,0,0)
    end
end

function ChatPanel:OnClickSetting()
    
end

function ChatPanel:InitVar()
    self._index = 0
end

function ChatPanel:OnDestroy()

end

function ChatPanel:OnShow(data)
    self:OnOpenPanelAnimation()
    self:UpdatePaneData()
    self:InitEvent()
    self._chatContent:InitEvent()
    if data and data.channelIndex then
        self:OnTabBarClick(data.channelIndex)
    end
    if GuildBanquetManager:IsOpen() then
        --策划要求公会篝火自动打开公会tab
        self:OnTabBarClick(6)
    end
end

function ChatPanel:OnClose()
    self:RemoveEvent()
    self._chatContent:RemoveEvent()
    self:CloseQuestionView()
end

--基于结构化View初始化方式，隐藏View使用Scale的方式处理
-- function ChatPanel:ScaleToHide()
--     return true
-- end

function ChatPanel:InitFun()
    self._showMsgSign = function(channelID)self:ShowChannelMsgSign(channelID) end
    --self._hideUnreadMsg = function()self:HideUnreadMsg() end
    --self._showUnreadMsg = function()self._chatUnreadMessage:AddMsgNum() end
    self._sendChatText = function(text,Channel,liststr)self:SendChatText(text,Channel,liststr) end
    self._setFastChat = function(msg,liststr)self._chatBottomChat:SetFastChat(msg,liststr) end
    self._setFastPosChat = function(msg)self._chatBottomChat:SetFastPosChat(msg) end
    self._autoSendContent = function() self._chatBottomChat:OnClickSend() end
    self._focusInputFiled = function() self._chatBottomChat:OnFocusInputFiled() end
    self._onLeaveMap = function(mapId)self:OnLeaveMap(mapId) end
end

function ChatPanel:InitEvent()
    EventDispatcher:AddEventListener(GameEvents.CHAT_HAS_MSG_SIGN, self._showMsgSign)
    EventDispatcher:AddEventListener(GameEvents.CHAT_SLIDE_END, self._hideUnreadMsg)
    --EventDispatcher:AddEventListener(GameEvents.CHAT_MSG_NUM_ADD, self._showUnreadMsg)
    EventDispatcher:AddEventListener(GameEvents.CHAT_SEND_CHAT_TEXT, self._sendChatText)
    EventDispatcher:AddEventListener(GameEvents.CHAT_ENTER_EMOJI_CONTENT, self._setFastChat)
    EventDispatcher:AddEventListener(GameEvents.CHAT_ENTER_POSITION_CONTENT, self._setFastPosChat)
    EventDispatcher:AddEventListener(GameEvents.CHAT_AUTO_SEND_CONTENT,self._autoSendContent)
    EventDispatcher:AddEventListener(GameEvents.CHAT_FOCUS_INPUTFILED,self._focusInputFiled)
    EventDispatcher:AddEventListener(GameEvents.ConvertVoiceMessage,self._sendChatText)
    EventDispatcher:AddEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function ChatPanel:RemoveEvent()
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_HAS_MSG_SIGN, self._showMsgSign)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_SLIDE_END, self._hideUnreadMsg)
    --EventDispatcher:RemoveEventListener(GameEvents.CHAT_MSG_NUM_ADD, self._showUnreadMsg)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_SEND_CHAT_TEXT, self._sendChatText)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_ENTER_EMOJI_CONTENT, self._setFastChat)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_ENTER_POSITION_CONTENT, self._setFastPosChat)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_AUTO_SEND_CONTENT,self._autoSendContent)
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_FOCUS_INPUTFILED,self._focusInputFiled)
    EventDispatcher:RemoveEventListener(GameEvents.ConvertVoiceMessage,self._sendChatText)
    EventDispatcher:RemoveEventListener(GameEvents.LEAVE_MAP, self._onLeaveMap)
end

function ChatPanel:OnOpenPanelAnimation()
    -- GUIManager.GetCutoutScreenPanelWidth() GUIManager.canvasWidth
    local dWidth = (GUIManager.GetCutoutScreenPanelWidth() - 1280)/2
    local dHight = (GUIManager.canvasHeight - 720)/2
    self._tweenPosition.IsTweening = false
    self.transform.localPosition = Vector3.New(ChatConfig.AnimationStartPos.x - dWidth, ChatConfig.AnimationStartPos.y-dHight,  0)
    self._tweenPosition:SetToPosOnce(Vector3.New(ChatConfig.AnimationEndPos.x - dWidth, ChatConfig.AnimationEndPos.y-dHight, 0), 0.4, nil)
end

function ChatPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    local dWidth = (GUIManager.GetCutoutScreenPanelWidth() - 1280)/2
    local dHight = (GUIManager.canvasHeight - 720)/2
    self.transform.localPosition = Vector3.New(ChatConfig.AnimationEndPos.x - dWidth, ChatConfig.AnimationEndPos.y-dHight, 0)
    self._tweenPosition:SetToPosOnce(Vector3.New(ChatConfig.AnimationStartPos.x - dWidth, ChatConfig.AnimationStartPos.y-dHight,  0), 0.4, nil)
    self._timer = GameWorld.TimerHeap:AddSecTimer(0.4, 0, 1, function()self:OnClosePanel() end)
end

function ChatPanel:OnClosePanel()
    GUIManager.ClosePanel(PanelsConfig.Chat)
end

function ChatPanel:OnTabBarClick(index)
    if self._selectedTab then
        self._selectedTab:SetSelected(false)
    end

    self._selectedTab = self._channelTabs[index]
    self._selectedTab:SetSelected(true)
    
    self._index = index
    self:UpdatePaneData()
    local num = index
    ChatManager.SetChannel(num)
    if num == public_config.CHAT_CHANNEL_GUILD and GuildBanquetManager:IsOpen() then
        self:ShowQuestionView()
    else
        self:CloseQuestionView()
    end

    if num == public_config.CHAT_CHANNEL_GUILD then
        EventDispatcher:TriggerEvent(GameEvents.Guild_Btn_Red,false)
    end

    --self._signList[num]:SetActive(false)
    --self:HideUnreadMsg()
end

function ChatPanel:UpdatePaneData()
    local index = self._index
    
    if index == public_config.CHAT_CHANNEL_SYSTEM or index == public_config.CHAT_CHANNEL_SEEK_TEAM then
        self._chatBottomBan:SetActive(true)
        self._chatBottomChat:SetActive(false)
    else
        self._chatBottomBan:SetActive(false)
        self._chatBottomChat:SetActive(true)
    end
end

function ChatPanel:ShowChannelMsgSign(channelID)
    --self._signList[channelID]:SetActive(true)
end

-- function ChatPanel:HideUnreadMsg()
--     self._chatUnreadMessage:SetActive(false)
-- end

function ChatPanel:SendChatText(text,Channel,liststr)
    ChatManager.SendChatContent(text,Channel,liststr)
end

function ChatPanel:ShowQuestionView()
    self._questionContainer:SetActive(true)
    self._questionView:ShowView()
end

function ChatPanel:CloseQuestionView()
    self._questionContainer:SetActive(false)
end

function ChatPanel:OnLeaveMap(mapId)    
    local sceneType = MapDataHelper.GetSceneType(mapId)
    if sceneType == SceneConfig.SCENE_TYPE_GUILD_BANQUET then
        self:CloseQuestionView()
    end
end