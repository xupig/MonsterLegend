-- ChatEmojiPanel.lua
local ChatEmojiPanel = Class.ChatEmojiPanel(ClassTypes.BasePanel)

--local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local GUIManager = GameManager.GUIManager

require "Modules.ModuleChat.ChildComponent.ChatEmojiItem"
local ChatEmojiItem = ClassTypes.ChatEmojiItem

require "Modules.ModuleChat.ChildComponent.ChatFastChatItem"
local ChatFastChatItem = ClassTypes.ChatFastChatItem

require "Modules.ModuleChat.ChildComponent.ChatShowItemListItem"
local ChatShowItemListItem = ClassTypes.ChatShowItemListItem

local ChatDataHelper = GameDataHelper.ChatDataHelper
local UIList = ClassTypes.UIList
local ChatManager = PlayerManager.ChatManager
local ChatConfig = GameConfig.ChatConfig
local UIToggleGroup = ClassTypes.UIToggleGroup
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local bagData = PlayerManager.PlayerDataManager.bagData
local chatData = PlayerManager.PlayerDataManager.bagData
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ItemConfig = GameConfig.ItemConfig
local FontStyleHelper = GameDataHelper.FontStyleHelper
local GameSceneManager = GameManager.GameSceneManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local MapDataHelper = GameDataHelper.MapDataHelper
local StringStyleUtil = GameUtil.StringStyleUtil

function ChatEmojiPanel:Awake()
    self.disableCameraCulling = true
    self:InitAnimation()
    self:InitComp()
    self:InitFun()
end

function ChatEmojiPanel:InitAnimation()
    --self._tweenPosition = self.gameObject:AddComponent(typeof(XGameObjectTweenPosition))
end

function ChatEmojiPanel:InitComp()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Emojis/ScrollViewList")
    self._listEmoji = list
    self._scrollViewEmoji = scrollView
    self._listEmoji:SetItemType(ChatEmojiItem)
    self._listEmoji:SetPadding(0, 0, 0, 16);
    self._listEmoji:SetGap(0, 0);
    self._listEmoji:SetDirection(UIList.DirectionLeftToRight, 7, 5);
    self._listEmojiData = ChatDataHelper.GetChatEmojiDatas(1)
    self._listEmoji:SetDataList(self._listEmojiData)
    local emojiItemClickedCB =
        function(idx)
            self:OnEmojiListItemClicked(idx)
        end
    self._listEmoji:SetItemClickedCB(emojiItemClickedCB)

    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "Container_Item/ScrollViewList")
    self._listItem = list1
    self._scrollViewItem = scrollView1
    self._listItem:SetItemType(ChatShowItemListItem)
    self._listItem:SetPadding(0, 0, 0, 0);
    self._listItem:SetGap(0, 5);
    self._listItem:SetDirection(UIList.DirectionLeftToRight, 7, 100);
    local itemClickedCB =
        function(idx)
            self:OnItemListItemClicked(idx)
        end
    self._listItem:SetItemClickedCB(itemClickedCB)


    local scrollView2, list2 = UIList.AddScrollViewList(self.gameObject, "Container_FastChat/ScrollViewList")
    self._listFastChat = list2
    self._scrollViewFastChat = scrollView2
    self._listFastChat:SetItemType(ChatFastChatItem)
    self._listFastChat:SetPadding(0, 0, 0, 0);
    self._listFastChat:SetGap(5,0);
    self._listFastChat:SetDirection(UIList.DirectionLeftToRight, 1, 10);
    local fastChatItemClickedCB =
        function(idx)
            self:OnFastChatListItemClicked(idx)
        end
    self._listFastChat:SetItemClickedCB(fastChatItemClickedCB)


    --self._chatEmojis:InitScrollViewList(1)
    -- self._chatFastChat = self:AddChildLuaUIComponent('Container_FastChat',ChatFastChat)
    -- self._chatFastChat:InitScrollViewList(2)
    
    self._buttonClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._buttonClose, function() self:OnClosePanel() end )

    --self._bg = self:FindChildGO("Image_BG")
    self._toggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Tab")
    --self._toggleGroup:SetRepeatClick(true)
    self._toggleGroup:SetOnSelectedIndexChangedCB(function(index) self:OnToggleGroupClick(index) end)

    self._containers = {}
    self._containers[1] = self:FindChildGO("Container_Emojis")
    self._containers[2] = self:FindChildGO("Container_FastChat")
    self._containers[3] = self:FindChildGO("Container_Item")
    for i=1,3 do
         self._containers[i]:SetActive(false)
    end

    -------------------------------------常用语编辑-------------------------------------------------
    self._containerEditFastChat = self:FindChildGO("Container_EditFastChat")
    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "Container_EditFastChat/InputField_Input")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("Container_EditFastChat/InputField_Input", 'InputFieldMeshWrapper')
    self._maxCount = GlobalParamsHelper.GetParamValue(816)

    self._btnCloseFastChat = self:FindChildGO("Container_EditFastChat/Button_Close")
    self._csBH:AddClick(self._btnCloseFastChat, function() self:OnCloseEditFastChat() end )

    self._btnConfirmFastChat = self:FindChildGO("Container_EditFastChat/Button_Confirm")
    self._csBH:AddClick(self._btnConfirmFastChat, function() self:ConfirmEdit() end )


    self._containerEditFastChat:SetActive(false)
end

function ChatEmojiPanel:OnDestroy() 

end

function ChatEmojiPanel:OnShow(data) 

    --LoggerHelper.Error("PanelsConfig.ChatEmoji:"..data)
    self._chatType = data
    self._toggleGroup:SetSelectIndex(0)
    self:OnToggleGroupClick(0)
    -- self:SetBGPosition(data)
    self:InitEvent()
    self:UpdateFastChat()
    self:StartEmojiAni()
    --LoggerHelper.Error("ChatEmojiPanel:OnShow")
end

function ChatEmojiPanel:OnToggleGroupClick(index)
    if index < 3 then
        for i=1,3 do
            if i == index +1 then
                self._containers[i]:SetActive(true)
            else
                self._containers[i]:SetActive(false)
            end

            if i == 2 then
                
            end

            if i == 3 then
                local t = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
                local t1 = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
                self._itemListData = {}
                for k,v in pairs(t1) do
                    table.insert(self._itemListData,v)
                end
                for k,v in pairs(t) do
                    table.insert(self._itemListData,v)
                end
                self._listItem:SetDataList(self._itemListData)
            end
        end
    --发送坐标
    else

        self:SendCoordinate()
    end
end

function ChatEmojiPanel:SetPosition(pos)
    -- local pos = ChatConfig.EmojiBGPositionMaps[data]
    self.gameObject.transform.localPosition = Vector3.New(pos.x,pos.y,0)
end

--override
function ChatEmojiPanel:OnClose() 
    self:RemoveEvent()
    self:StopEmojiAni()
    --LoggerHelper.Error("ChatEmojiPanel:OnClose")
end

function ChatEmojiPanel:InitFun()
    self._closePanelFun = function() self:OnClosePanel() end
    self._editFastChatCb = function(shouldShow,index) self:ShowEditFastChatView(shouldShow,index) end
    self._updateFastChatCb = function () self:UpdateFastChat() end
end

function ChatEmojiPanel:InitEvent()
    EventDispatcher:AddEventListener(GameEvents.CHAT_EDIT_FAST_CHAT,self._editFastChatCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_FAST_CHAT,self._updateFastChatCb)
end

function ChatEmojiPanel:RemoveEvent()
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_EDIT_FAST_CHAT,self._editFastChatCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_FAST_CHAT,self._updateFastChatCb)
end

---------------------------------------常用语列表/编辑------------------------------------------------
function ChatEmojiPanel:UpdateFastChat()
    local fastChat  = GameWorld.Player().chat_often_use_info
    self._listFastChat:SetDataList(fastChat)
end

--发送快捷聊天
function ChatEmojiPanel:OnFastChatListItemClicked(idx)
    local content = self._listFastChat:GetDataByIndex(idx)

    if self._chatType == 1 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, content)
    elseif self._chatType == 2 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, content)
    elseif self._chatType == 3 then
        EventDispatcher:TriggerEvent(GameEvents.FRIEND_SEND_CHAT_TEXT, content)
    end

    
    self:OnClosePanel()
end

function ChatEmojiPanel:OnCloseEditFastChat()
    EventDispatcher:TriggerEvent(GameEvents.CHAT_EDIT_FAST_CHAT,false)
end

function ChatEmojiPanel:ShowEditFastChatView(shouldShow,index)
   self._containerEditFastChat:SetActive(shouldShow)
   if shouldShow then
        self._editIndex  = index
   end
end

function ChatEmojiPanel:ConfirmEdit()
    local content = self._inputText.text
    ChatManager:RequestEditFastChat(self._editIndex,content)
    EventDispatcher:TriggerEvent(GameEvents.CHAT_EDIT_FAST_CHAT,false)
end

function ChatEmojiPanel:OnEndEdit()
    
end

---------------------------------------表情列表-------------------------------------------------

-- 停止表情动画
function ChatEmojiPanel:StopEmojiAni()
    --self._chatFastChat:ShowEmojiAnimation(false)
    self:ShowEmojiAnimation(false)
end

-- 开始表情动画
function ChatEmojiPanel:StartEmojiAni()
    --self._chatFastChat:ShowEmojiAnimation(true)
    self:ShowEmojiAnimation(true)
end

--发送表情
function ChatEmojiPanel:OnEmojiListItemClicked(idx)
    local data = self._listEmojiData[idx + 1]
    local sendText = ''
    if data.type == 1 then --表情
        -- local animationData = ChatDataHelper.GetEmojiAnimationData(data.content)
        -- if animationData.frameRate > 0 then
        --     -- sendText = '[E' .. (data.content - 1) .. ']'
        --     sendText = '<sprite anim="' .. animationData.startIndex .. ',' .. animationData.endIndex .. ',' .. animationData.frameRate .. '">'
        -- else
        --     sendText = '<sprite=' .. animationData.startIndex .. '>'
        -- end
        sendText = '#'..data.id.."#"
    -- else
    --     -- 文字
    --     sendText = LanguageDataHelper.CreateContent(data.content)
    end

    if self._chatType == 1 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ENTER_EMOJI_CONTENT, sendText)
    elseif self._chatType == 2 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ANN_ENTER_EMOJI_CONTENT, sendText)
    elseif self._chatType == 3 then
        EventDispatcher:TriggerEvent(GameEvents.FRIEND_CHAT_ENTER_EMOJI_CONTENT, sendText)
    end
   
    
    self:OnClosePanel()
end


-- function ChatEmojiView:OnSendEmoji()
--     EventDispatcher:TriggerEvent(GameEvents.CHAT_AUTO_SEND_CONTENT)--发送表情
-- end

-- function ChatEmojiView:OnSendFocus()
--     EventDispatcher:TriggerEvent(GameEvents.CHAT_FOCUS_INPUTFILED)--焦点输入文本
-- end

function ChatEmojiPanel:ShowEmojiAnimation(value)
    for i = 1, #self._listEmojiData do
        -- LoggerHelper.Log(tostring(value) .. '----------------------555----------')
        local item = self._listEmoji:GetItem(i - 1)
        -- LoggerHelper.Log(tostring(value) .. '----------------------444----------')
        if item ~= nil then
            -- LoggerHelper.Log(tostring(value) .. '----------------------3333----------')
            item:ShowEmojiAnimation(value)
        
        -- LoggerHelper.Log(tostring(value) .. '----------------------66666----------')
        end
    end
end

---------------------------------------道具列表-------------------------------------------------
--"[@link="{0}"@][@{1}@][@u@]{2}[@/u@][@/color@][@/link@]"
function ChatEmojiPanel:OnItemListItemClicked(idx)
    local itemData = self._listItem:GetDataByIndex(idx)
    local itemDataStr = itemData:GetServerData()

    local _iconItem = {}
    _iconItem[1] = itemDataStr
    _iconItem[2] = itemData.bagType
    if itemData.bagType == public_config.PKG_TYPE_LOADED_EQUIP then
        local equipType = itemData.bagPos
        local vipLevel = GameWorld.Player().vip_level
        local strengthInfo = GameWorld.Player().equip_stren_info
        local washInfo = GameWorld.Player().equip_wash_info
        local gemInfo = GameWorld.Player().equip_gem_info
        local suitData = GameWorld.Player().equip_suit_data or {}
        _iconItem[3] = vipLevel
        _iconItem[4] = {[equipType] = strengthInfo[equipType]}
        _iconItem[5] = {[equipType] = washInfo[equipType]}
        _iconItem[6] = {[equipType] = gemInfo[equipType]}
        _iconItem[7] = suitData
        local loaded = {}
        local t = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()
        for pos,v in pairs(t) do
            loaded[pos] = v:GetItemID()
        end
        _iconItem[8] = loaded
    end
    --table.insert(self._itemToServerData, _iconItem)   
    ChatManager:AddItemToSerData(_iconItem)

    local itemDataList = ChatManager:GetToSerData()

    local liststr  = PrintTable:TableToStrEx(ChatManager:GetToSerData())
   
    local sendText = '['..itemData:GetItemName()..']'

    if self._chatType == 1 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ENTER_EMOJI_CONTENT, sendText,liststr)
    elseif self._chatType == 2 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ANN_ENTER_EMOJI_CONTENT, sendText,liststr)
    elseif self._chatType == 3 then
        EventDispatcher:TriggerEvent(GameEvents.FRIEND_CHAT_ENTER_EMOJI_CONTENT, sendText,liststr)
    end
   
 
    
	self:OnClosePanel()
end

---------------------------------------坐标-------------------------------------------------
function ChatEmojiPanel:SendCoordinate()
    
    local mapId = GameSceneManager:GetCurrMapID()
    -- LoggerHelper.Log("Ash: MiniMapPanel OnShow: " .. tostring(mapId))
    local pos = GameWorld.Player():GetPosition()
    --LoggerHelper.Error("坐标===============================".."pos.x"..pos.x)
    --local posLine = 
    --LoggerHelper.Log("posLine"..posLine)

    -- local quality = itemData:GetQuality()
    -- local colorId = ItemConfig.QualityTextMap[quality]
    -- local color = FontStyleHelper.GetFontStyleData(colorId).color
    -- local itemDataStr = PrintTable:TableToStr(itemData:GetServerData())
    local arg = {}
    arg["0"] = ChatConfig.LinkTypesMap.Coordinate..","..mapId..","..pos.x..","..pos.y..","..pos.z
    arg["1"] = StringStyleUtil.green
    arg["2"] = LanguageDataHelper.GetLanguageData(MapDataHelper.GetChinese(mapId) or 0).."["..math.ceil(pos.x)..","..math.ceil(pos.z).."]"
    local sendText = LanguageDataHelper.CreateContentWithArgs(80532,arg)

    if self._chatType == 1 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ENTER_POSITION_CONTENT, sendText)
    elseif self._chatType == 2 then
        EventDispatcher:TriggerEvent(GameEvents.CHAT_ENTER_POSITION_CONTENT, sendText)
    elseif self._chatType == 3 then
        EventDispatcher:TriggerEvent(GameEvents.FRIEND_ENTER_POSITION_CONTENT, sendText)
    end

  
    self:OnClosePanel()
end

function ChatEmojiPanel:OnOpenPanelAnimation()
    -- self._tweenPosition.IsTweening = false
    -- self.transform.localPosition = Vector3.New(130,50,0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(640, 50, 0), 0.4, nil)

    -- self.transform.localPosition = Vector3.New(0, -310, 0)
end

function ChatEmojiPanel:OnClosePanel()
    -- self._tweenPosition.IsTweening = false
    -- self.transform.localPosition = Vector3.New(640, 50, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(130, 50, 0), 0.4, nil)
    -- self._timer = GameWorld.TimerHeap:AddSecTimer(0.4, 0, 1, function() self:OnClosePanel() end)
    -- self.transform.localPosition = Vector3.New(-510, -310, 0)
    GUIManager.ClosePanel(PanelsConfig.ChatEmoji)
end