--ChatSmallPanel
local ChatSmallPanel = Class.ChatSmallPanel(ClassTypes.BasePanel)

ChatSmallPanel.interface = GameConfig.ComponentsConfig.Component
ChatSmallPanel.classPath = "Modules.ModuleChat.ChildView.ChatSmallPanel"

require "Modules.ModuleChat.ChildView.ChatVoiceButtonView"

local ChatVoiceButtonView = ClassTypes.ChatVoiceButtonView

require "Modules.ModuleChat.ChildComponent.ChatSmallItem"
local ChatSmallItem = ClassTypes.ChatSmallItem

local ChatDataHelper = GameDataHelper.ChatDataHelper
local ChatManager = PlayerManager.ChatManager
local FriendManager = PlayerManager.FriendManager
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local EscortPeriManager = PlayerManager.EscortPeriManager
local EntityConfig = GameConfig.EntityConfig
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local public_config = GameWorld.public_config
local EquipManager = GameManager.EquipManager

local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList
local XAutoCombat = GameMain.XAutoCombat

function ChatSmallPanel:Awake()
    self:InitCallbackFunc()
    self:InitComps()
end

function ChatSmallPanel:InitCallbackFunc()
    self._updateContent = function(channel_id) self:UpdateContent(channel_id) end
    self._onAutoFightChange = function() self:OnAutoFightChange() end
    self._onUpdateWorldBossTire = function(flag) self:OnUpdateWorldBossTire(flag) end
    self._onUpdateGodIslandTire = function(flag) self:OnUpdateGodIslandTire(flag) end
    --self._refreshEscortPeriState = function() self:RefreshEscortPeriState() end
    self._updateFriendRedpoint = function(flag) self:SetFriendRedpoint(flag) end
    self._updateBagRedpoint = function(flag) self:SetBagRedpoint(flag) end

    self._sendChatText = function(text,liststr)self:SendChatText(text,liststr) end
    self._guildShowText = function()self:GuildShow()end
    self._guildRedUpdate = function(isShow)self:GuildRedShow(isShow)end

    self._startAutoPathCB = function () self:StartAutoPathState() end
    self._stopAutoPathCB = function () self:StopAutoPathState() end
end

function ChatSmallPanel:InitComps()
    self._bossTireImage = self:FindChildGO("Image_WorldBoss_Tire")
    self._godIslandTireImage = self:FindChildGO("Image_GodIsland_Tire")
    self._btnVoice = self:FindChildGO("Container_Voice/Button_Voice")
    self._csBH:AddClick(self._btnVoice, function() self:OpenGuild() end)

    self._btnVoiceRed = self:FindChildGO("Container_Voice/Button_Voice/Image_RedPoint")
	--self._buttonVoiceView = self:AddChildLuaUIComponent('Container_Voice/Button_Voice', ChatVoiceButtonView)

	-- self._items = {}
	-- local itemGOs = {}
	-- itemGOs[1] = self:FindChildGO("Container_Message/item")
 --    for i = 2, 3 do
 --        itemGOs[i] = self:CopyUIGameObject("Container_Message/item", "Container_Message")
 --    end

 --    for i=1,3 do
 --    	local go = itemGOs[i]
 --    	local item = UIComponentUtil.AddLuaUIComponent(go, ChatSmallItem)
 --    	self._items[i] = item
 --    end
    self._contentGoList = self:FindChildGO("ScrollViewList_Message")
    local scrollView, list = UIComplexList.AddScrollViewComplexList(self._contentGoList)
    self._scrollList = list
    self._viewList = scrollView
    self._scrollList:SetItemType(ChatSmallItem)
    self._scrollList:SetPadding(0, 0, 0, 0)
    self._scrollList:SetGap(0, 5)
    self._scrollList:SetDirection(UIList.DirectionTopToDown, 1, -1)
    self._scrollList:AppendItemDataList({})
    self._scrollList:UseObjectPool(true)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    self._scrollList:SetItemClickedCB(itemClickedCB)
    -- self._viewList:SetOnMoveEndCB(function()self:DifficultScrollEnd() end)

    self._btnExpand = self:FindChildGO("Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end)

    -- self._escortPeriButton = self:FindChildGO("Button_EscortPeri")
    -- self._csBH:AddClick(self._escortPeriButton, function() self:OnEscortPeriButtonClick() end)
    -- self:RefreshEscortPeriState()
    -- GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_GUARD_GODDESS_ID, self._refreshEscortPeriState)

    self._btnAutoFight = self:FindChildGO("Button_Auto")
    self._csBH:AddClick(self._btnAutoFight, function() self:OnAutoFightClick() end)
    self._autoFightLight = self:FindChildGO("Image_Light")
    self._autoFightLightRotation = self._autoFightLight:AddComponent(typeof(XGameObjectTweenRotation))
    self._autoFightLight:SetActive(false)

    self._imgAutoOn = self:FindChildGO("Button_Auto/Image_On")
    self._imgAutoOn:SetActive(false)
    self._imgAutoOff = self:FindChildGO("Button_Auto/Image_Off")
    self._imgAutoOff:SetActive(true)

    -- self._imgAutoFight = self:FindChildGO("Image_AutoFight")
    -- self._imgAutoFight:SetActive(false)
    self._containerAutoFight = self:FindChildGO("Container_AutoFight")
    self._xAutoCombat = self._containerAutoFight:AddComponent(typeof(XAutoCombat))

    self._containerAutoFindPath = self:FindChildGO("Container_AutoFindPath")
    self._xAutoFindPath = self._containerAutoFindPath:AddComponent(typeof(XAutoCombat))
    self._containerAutoFindPath:SetActive(false)

    self._isInAutoFight = false --记录自动战斗状态

    self._btnBag = self:FindChildGO("Button_Bag")
    self._csBH:AddClick(self._btnBag, function() self:OpenBag() end)
    self._btnBagRedPoint = self:FindChildGO("Button_Bag/Image_RedPoint")

    self._btnFriend = self:FindChildGO("Button_Friend")
    self._csBH:AddClick(self._btnFriend, function() self:OpenFriend() end)
    self._btnFriendRedPoint = self:FindChildGO("Button_Friend/Image_RedPoint")
end

function ChatSmallPanel:InitEvent()
    EventDispatcher:AddEventListener(GameEvents.CHAT_CONTENT_ADD, self._updateContent)
    EventDispatcher:AddEventListener(GameEvents.CHANGE_AUTO_FIGHT, self._onAutoFightChange)
    EventDispatcher:AddEventListener(GameEvents.WORLD_BOSS_TIRE_UPDATE, self._onUpdateWorldBossTire)
    EventDispatcher:AddEventListener(GameEvents.GOD_ISLAND_TIRE_UPDATE, self._onUpdateGodIslandTire)
    EventDispatcher:AddEventListener(GameEvents.ConvertVoiceMessage,self._sendChatText)

    EventDispatcher:AddEventListener(GameEvents.Guild_Btn_Show,self._guildShowText)
    EventDispatcher:AddEventListener(GameEvents.Guild_Btn_Red,self._guildRedUpdate)

    EventDispatcher:AddEventListener(GameEvents.Start_Auto_FindPath,self._startAutoPathCB)
    EventDispatcher:AddEventListener(GameEvents.Stop_Auto_FindPath,self._stopAutoPathCB)
end

function ChatSmallPanel:RemoveEvent()
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_CONTENT_ADD, self._updateContent)
    EventDispatcher:RemoveEventListener(GameEvents.CHANGE_AUTO_FIGHT, self._onAutoFightChange)
    EventDispatcher:RemoveEventListener(GameEvents.WORLD_BOSS_TIRE_UPDATE, self._onUpdateWorldBossTire)
    EventDispatcher:RemoveEventListener(GameEvents.GOD_ISLAND_TIRE_UPDATE, self._onUpdateGodIslandTire)
    EventDispatcher:RemoveEventListener(GameEvents.ConvertVoiceMessage,self._sendChatText)

    EventDispatcher:RemoveEventListener(GameEvents.Guild_Btn_Show,self._guildShowText)
    EventDispatcher:RemoveEventListener(GameEvents.Guild_Btn_Red,self._guildRedUpdate)

    EventDispatcher:RemoveEventListener(GameEvents.Start_Auto_FindPath,self._startAutoPathCB)
    EventDispatcher:RemoveEventListener(GameEvents.Stop_Auto_FindPath,self._stopAutoPathCB)
end

function ChatSmallPanel:OnShow()
    FriendManager:RegisterNodeUpdateFunc(self._updateFriendRedpoint)
    EquipManager:RegisterNodeUpdateFunc(self._updateBagRedpoint)
    self:InitEvent()
    self:UpdateListViewData()
    self:GuildShow()
end

function ChatSmallPanel:OnClose()
    FriendManager:RegisterNodeUpdateFunc(nil)
    self:RemoveEvent()
end

function ChatSmallPanel:UpdateContent(channel_id)
	self:UpdateListViewData()
    self._scrollList:MoveToBottom()
end

function ChatSmallPanel:OpenFriend()
    local data ={["id"]=FriendPanelType.Chat,["value"]=1}
    GUIManager.ShowPanel(PanelsConfig.Friend,data,nil)
end

function ChatSmallPanel:OpenBag()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_chat_small_bag_button")
    GUIManager.ShowPanelByFunctionId(2)
end

local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)

function ChatSmallPanel:OnAutoFightClick()
    GameWorld.Player():ChangeAutoFight()
end

function ChatSmallPanel:OnAutoFightChange()
    if GameWorld.Player().autoFight or GameWorld.Player().temporaryAutoFight then
        if not self._isInAutoFight then
            self._isInAutoFight = true
            self._autoFightLight:SetActive(true)
            self._autoFightLightRotation.IsTweening = true
            self._autoFightLightRotation:SetFromToRotation(start, stop, 2, 2,nil)

            if not self._isInAutoFindPath then
                self._containerAutoFight:SetActive(true)
                self._xAutoCombat:StartAnimation()
            end
            
            self._imgAutoOn:SetActive(true)
            self._imgAutoOff:SetActive(false)
        end
    else
        if self._isInAutoFight then
            self._isInAutoFight = false
            self._autoFightLightRotation.IsTweening = false
            self._autoFightLight:SetActive(false)

            self._containerAutoFight:SetActive(false)
            self._xAutoCombat.IsTweening = false
            self._imgAutoOn:SetActive(false)
            self._imgAutoOff:SetActive(true)
        end
    end
end

function ChatSmallPanel:StartAutoPathState()
    self._isInAutoFindPath = true
    if self._isInAutoFight then
        self._containerAutoFight:SetActive(false)
        self._xAutoCombat.IsTweening = false
    end
    self._containerAutoFindPath:SetActive(true)
    self._xAutoFindPath:StartAnimation()
end

function ChatSmallPanel:StopAutoPathState()
    self._isInAutoFindPath = false
    if self._isInAutoFight then
        self._containerAutoFight:SetActive(true)
        self._xAutoCombat:StartAnimation()
    end
    self._containerAutoFindPath:SetActive(false)
    self._xAutoFindPath.IsTweening = false

end

function ChatSmallPanel:OnUpdateWorldBossTire(flag)
    self._bossTireImage:SetActive(flag)
end

function ChatSmallPanel:OnUpdateGodIslandTire(flag)
    self._godIslandTireImage:SetActive(flag)
end

function ChatSmallPanel:OnExpand(channel)
    GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = channel or public_config.CHAT_CHANNEL_WORLD})
end

function ChatSmallPanel:OpenGuild()
    GUIManager.ShowPanel(PanelsConfig.Chat,{channelIndex = public_config.CHAT_CHANNEL_GUILD})
end


function ChatSmallPanel:GuildShow()
    local guildDbid = GameWorld.Player().guild_info[public_config.GUILD_INFO_DBID] or 0
    if guildDbid == 0 then
        self._btnVoice.gameObject:SetActive(false)
    else
        self._btnVoice.gameObject:SetActive(true)
    end
end

function ChatSmallPanel:GuildRedShow(isShow)
    if self._btnVoiceRed then
        self._btnVoiceRed.gameObject:SetActive(isShow)
    end
end




-- function ChatSmallPanel:RefreshEscortPeriState()
--     if GameWorld.Player().guard_goddess_id > 0 then
--         self._escortPeriButton:SetActive(true)
--     else
--         self._escortPeriButton:SetActive(false)
--     end
-- end

-- function ChatSmallPanel:OnEscortPeriButtonClick()
--     EscortPeriManager:CommitEscort()
-- end

function ChatSmallPanel:ChangeContent()
    -- for i = 1, self._contentNum do
    --     self._contentGoList[i]:SetActive(false)
    --     self._viewList[i]:SetCheckEnded(false)
    -- end
    -- self._contentGoList[ChatManager.CurChannel]:SetActive(true)
    -- self._viewList[ChatManager.CurChannel]:SetCheckEnded(true)
    -- self:UpdateListView()
end

function ChatSmallPanel:OnListItemClicked(idx)
    local itemData = self._scrollList:GetDataByIndex(idx)
    if public_config.CHAT_CHANNEL_HORN == itemData.channel then
        self:OnExpand()
    else
        self:OnExpand(itemData.channel)
    end
end

-- 更新LIST数据
function ChatSmallPanel:UpdateListViewData()
    if self._updateIteming then --是否在更新中
        self._needUpdateItem = true
        return
    end
    
    --在更新ing
    self._updateIteming = true
    
    local listData = ChatManager.GetLatestChatContents()
    local addDataList = {}
    for k, v in pairs(listData) do
        if v.smallShow == false then
            v.smallShow = true
            table.insert(addDataList, v)
        end
    end

    if self._contentNum == nil then
        self._contentNum = 0
    end

    local addCount = #addDataList
    if addCount > 0 then
        local allCount = self._contentNum + addCount
        -- LoggerHelper.Error(updateChannel .. 'curCount:' .. curCount)
        local removeCount = allCount - ChatManager.MAX_LATEST_CHAT_NUM
        -- LoggerHelper.Error('allCount:' .. allCount)
        if removeCount > 0 then
            self._contentNum = self._contentNum - removeCount
            self._scrollList:RemoveItemRange(0, removeCount, false)
        end
        --把新ITEM加进去
        self._contentNum = self._contentNum + #addDataList
        self._scrollList:AppendItemDataList(addDataList)
    end

    --在更新过程是否有新数据来
    if self._needUpdateItem then
        self._needUpdateItem = false
        self:UpdateListViewData()
    end
    self._updateIteming = false

end

function ChatSmallPanel:SetBagRedpoint(value)
    self._btnBagRedPoint:SetActive(value)
end

function ChatSmallPanel:SetFriendRedpoint(value)
    self._btnFriendRedPoint:SetActive(value)
end

function ChatSmallPanel:SendChatText(text,liststr)
   --LoggerHelper.Error("ChatSmallPanel:SendChatText")
    --ChatManager.RequestChat(public_config.CHAT_CHANNEL_WORLD,text,liststr)
end