-- ChatVoicePanel.lua
local ChatVoicePanel = Class.ChatVoicePanel(ClassTypes.BasePanel)

local ChatConfig = GameConfig.ChatConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ChatManager = PlayerManager.ChatManager

function ChatVoicePanel:Awake()
    self.disableCameraCulling = true
    self:InitVar()
    self:InitComp()
    self:InitFun()
end

function ChatVoicePanel:InitComp()
    self._containerVoice = self:FindChildGO("Container_Content/Container_Voice")
    self._containerCancel = self:FindChildGO("Container_Content/Container_Cancel")
    --self._textMessage = self:GetChildComponent("Container_Content/Text_Message", 'TextMeshWrapper')
    self._containerContent = self:FindChildGO("Container_Content")
end

function ChatVoicePanel:InitVar()
    self._data = nil
    self._type = -1
    self._panel = nil
end

function ChatVoicePanel:OnDestroy()

end

function ChatVoicePanel:OnShow(data)
    self._data = data
    self:UpdatePanel(data.type)
    self:UpdatePanelPos(data.panel)
end

function ChatVoicePanel:SetData(data)
    self._data = data
    self:UpdatePanel(data.type)
    self:UpdatePanelPos(data.panel)
end

--override
function ChatVoicePanel:OnDisable()
    self:RemoveEvent()
end

--override
function ChatVoicePanel:OnEnable()
    self:InitEvent()
end

function ChatVoicePanel:InitFun()
end

function ChatVoicePanel:InitEvent()
    EventDispatcher:TriggerEvent(GameEvents.CHAT_VOICE_START)

end

function ChatVoicePanel:RemoveEvent()
    EventDispatcher:TriggerEvent(GameEvents.CHAT_VOICE_STOP)
    PlayerManager.ChatManager:ResetTalkTime()
end

function ChatVoicePanel:UpdatePanel(type)
    if type ~= self._type then
        if type == ChatConfig.Voice then
            self._containerVoice:SetActive(true)
            self._containerCancel:SetActive(false)
        elseif type == ChatConfig.CancelVoice then
            self._containerVoice:SetActive(false)
            self._containerCancel:SetActive(true)
        end
        --self._textMessage.text = LanguageDataHelper.CreateContent(ChatConfig.VoiceTipsMap[type])
        self._type = type
    end

    ChatManager.SendVoiceMessageType(type)
end


function ChatVoicePanel:UpdatePanelPos(panel)
    if self._panel == panel then
        return
    end
    local pos = ChatConfig.VoicTipsPosMap[panel]
    self._containerContent.transform.anchoredPosition = Vector3.New(pos.x,pos.y,0)
end