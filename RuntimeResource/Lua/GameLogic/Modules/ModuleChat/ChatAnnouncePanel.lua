-- ChatAnnouncePanel.lua
local ChatAnnouncePanel = Class.ChatAnnouncePanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
require "Modules.ModuleChat.ChildView.ChatVoiceButtonView"
local ChatVoiceButtonView = ClassTypes.ChatVoiceButtonView

local ChatManager = PlayerManager.ChatManager
local ChatConfig = GameConfig.ChatConfig
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIInputFieldMesh = ClassTypes.UIInputFieldMesh
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local bagData = PlayerManager.PlayerDataManager.bagData

function ChatAnnouncePanel:Awake()
    self.disableCameraCulling = true
	self:InitComp()
end


function ChatAnnouncePanel:InitComp()
	self._buttonEmoji = self:FindChildGO("Button_Emoji")
    self._csBH:AddClick(self._buttonEmoji, function()self:OnClickEmoji() end)
    
    self._buttonSend = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._buttonSend, function()self:OnClickSend() end)
    
    self._inputFieldMesh = UIInputFieldMesh.AddInputFieldMesh(self.gameObject, "InputField_Input")
    self._inputFieldMesh:SetOnEndMeshEditCB(function(text) self:OnEndEdit(text) end)
    self._inputText = self:GetChildComponent("InputField_Input", 'InputFieldMeshWrapper')
    self._maxCount = GlobalParamsHelper.GetParamValue(812)

    self._txtCountLeft = self:GetChildComponent("Text_CountLeft", 'TextMeshWrapper')

    --self._buttonVoice = self:FindChildGO("Button_Voice")
    self._buttonVoiceView = self:AddChildLuaUIComponent('Button_Voice', ChatVoiceButtonView)

    self._buttonClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._buttonClose, function()self:OnClickClose() end)

    self._sendChatText =  function(msg,liststr)self:SetFastChat(msg,liststr) end
end

function ChatAnnouncePanel:OnShow()
    EventDispatcher:AddEventListener(GameEvents.CHAT_ANN_ENTER_EMOJI_CONTENT, self._sendChatText)
    local left = self._maxCount
    self._inputText.characterLimit = left
    self._txtCountLeft.text = LanguageDataHelper.CreateContentWithArgs(80516,{["0"] = left})
end


function ChatAnnouncePanel:OnClose()
    EventDispatcher:RemoveEventListener(GameEvents.CHAT_ANN_ENTER_EMOJI_CONTENT, self._sendChatText)
end

function ChatAnnouncePanel:AddListeners()

end

function ChatAnnouncePanel:RemoveListeners()

end

function ChatAnnouncePanel:SetFastChat(text, liststr)
    local textStr = self._inputText.text
    textStr = textStr .. text
    self._inputText.text = textStr
    self._liststr = liststr
end


function ChatAnnouncePanel:OnClickEmoji()
	GUIManager.ShowPanel(PanelsConfig.ChatEmoji, 2)
end

function ChatAnnouncePanel:OnEndEdit(text)
    local left = self._maxCount - string.utf8len(text)
    self._txtCountLeft.text = LanguageDataHelper.CreateContentWithArgs(80516,{["0"] = left})
end


function ChatAnnouncePanel:OnClickSend()
    local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(130)
    if bagCount <= 0 then 
        GameManager.EquipManager:BuyGuideTip(130)
    end

    local textStr = self._inputText.text
    if textStr == '' then
        return
    end

    local liststr =  self._liststr
    local serData = ChatManager:GetToSerData()
    for i=#serData,1,-1 do
        local itemName = ItemDataHelper.GetItemName(tonumber(serData[i][1][public_config.ITEM_KEY_ID]))
        local itemNameText = '['..itemName..']'
        local start_i = 0;
        local end_j = 0;
        local sunstr = "";
        start_i,end_j,sunstr=string.find(textStr,itemNameText,1,-2);

        local str = ""
        local str2 = ""
        if start_i then
            if start_i == 1 then
                str = ""
            else
                str = string.sub(textStr,1,start_i-1)
            end

            if end_j == string.len(textStr) then
                str2 = ""
            else
                str2 = string.sub(textStr,end_j+1,string.len(textStr))
            end
        
            textStr = str.."&&"..i.."&&"..str2
        end
    end

    EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, textStr,public_config.CHAT_CHANNEL_HORN,liststr)
    self._inputText.text = ''
    
    EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)

    local left = self._maxCount
    self._inputText.characterLimit = left
    self._txtCountLeft.text = LanguageDataHelper.CreateContentWithArgs(80516,{["0"] = left})
end

-- function ChatAnnouncePanel:OnClickSend()
--  	local textStr = self._inputText.text
--     if textStr == '' then
--         return
--     end
    
--     textStr = self:HandleEmoji(textStr)
    
--     ChatManager.RequestChat(public_config.CHAT_CHANNEL_HORN,textStr)
--     --EventDispatcher:TriggerEvent(GameEvents.CHAT_SEND_CHAT_TEXT, textStr)
--     self._inputText.text = ''
-- end

function ChatAnnouncePanel:HandleEmoji(textStr)
    textStr = ChatManager.HandleEmoji(textStr)
    return textStr
end

function ChatAnnouncePanel:OnClickClose()
    GUIManager.ClosePanel(PanelsConfig.ChatAnnounce)
end