--FriendPanel
require "Modules.ModuleFriend.ChildView.FriendViewEx"
-- require "Modules.ModuleFriend.ChildView.MailViewEx"
require "Modules.ModuleMail.MailPanel"
local FriendPanel = Class.FriendPanel(ClassTypes.BasePanel)


local FriendViewEx = ClassTypes.FriendViewEx
local MailViewEx = ClassTypes.MailPanel
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ChatManager = PlayerManager.ChatManager
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local UIComponentUtil = GameUtil.UIComponentUtil
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function FriendPanel:Awake()
	--self:InitCallBack()
	self:InitComps()
end

-- function FriendPanel:InitCallBack()
--     -- self._tabClickCB = function (index)
--     --     self:OnToggleGroupClick(index)
--     -- end
-- end

-- --tab点击处理
-- function FriendPanel:OnToggleGroupClick(index)
--     --LoggerHelper.Error("OpenRankView:OnToggleGroupClick")
--     if self._select and index ~= self._select then
--         self._views[self._select]:CloseViewEx()
--         self._views[self._select].transform.localScale = HideScale
--         --self._views[self._select]:SetActive(false)
--     end

--     for i,v in ipairs(self._views) do
--         --self._views[i]:SetActive(false)
--         self._views[i].transform.localScale = HideScale
--     end

--     self._select = index
--     --self._views[self._select]:SetActive(true)
--     self._views[self._select].transform.localScale = ShowScale
--     self._views[self._select]:RefreshView(self._select,self.data)

-- end



--情况4: 在好友系统的添加里点发消息
function FriendPanel:OnSendMsgInFriend(uuid)

end



--基于结构化View初始化方式，隐藏View使用Scale的方式处理
-- function FriendPanel:ScaleToHide()
--     return true
-- end


function FriendPanel:InitComps()
    --self._views = {}
    -- self._friendView = self:AddChildLuaUIComponent("Container_Friend", FriendViewEx)
    -- self._mailView = self:AddChildLuaUIComponent("Container_Mail", MailViewEx)

	self:InitView("Container_Friend", FriendViewEx,1)
	self:InitView("Container_Mail", MailViewEx,2)

    -- self._views[1] = self._friendView                --每日必做
    -- self._views[2] = self._mailView  
    --self._mailView:AbleScr(false)
    -- for i,v in ipairs(self._views) do
    --     --self._views[i]:SetActive(false)
    --     self._views[i].transform.localScale = HideScale
    -- end

    --self:SetTabClickCallBack(self._tabClickCB)
end

function FriendPanel:CloseBtnClick()
	GUIManager.ClosePanel(PanelsConfig.Friend)
end

function FriendPanel:OnShow(data)
    self.data = data
    --LoggerHelper.Error("FriendPanel:OnShow(data):"..PrintTable:TableToStr(data))
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)
end

function FriendPanel:OnClose()
    -- self._views[self._select]:CloseViewEx()
    -- self._select = nil
    -- --self._friendView:SetActive(false)
    -- self._views[1].transform.localScale = HideScale
end

function FriendPanel:StructingViewInit()
    return true
end

function FriendPanel:WinBGType()
    return PanelWinBGType.NormalBG
end