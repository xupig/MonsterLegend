--FriendModule.lua
FriendModule = {}

 local PanelsConfig = GameConfig.PanelsConfig
 local GUIManager = GameManager.GUIManager

function FriendModule.Init()
     GUIManager.AddPanel(PanelsConfig.Friend,"Panel_Friend",GUILayer.LayerUIPanel,"Modules.ModuleFriend.FriendPanel",true,false,nil,false)
     --GUIManager.AddPanel(PanelsConfig.Mail, "Panel_Mail", GUILayer.LayerUIPanel, "Modules.ModuleMail.MailPanel", true, false)
end

return FriendModule