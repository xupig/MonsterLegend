--FriendChatContentPersionItem
local FriendChatContentPersionItem = Class.FriendChatContentPersionItem(ClassTypes.BaseLuaUIComponent)

FriendChatContentPersionItem.interface = GameConfig.ComponentsConfig.Component
FriendChatContentPersionItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendChatContentPersionItem"

local UIChatTextBlock = ClassTypes.UIChatTextBlock
local RectTransform = UnityEngine.RectTransform

require 'UIComponent.Extend.PlayerHead'
local PlayerHead = ClassTypes.PlayerHead
local public_config = GameWorld.public_config
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendManager = PlayerManager.FriendManager
--local CharacterDataHelper = GameDataHelper.CharacterDataHelper
local FashionDataHelper = GameDataHelper.FashionDataHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local FashionManager = PlayerManager.FashionManager
local FashionDataHelper = GameDataHelper.FashionDataHelper
local CommonIconType = GameConfig.EnumType.CommonIconType
local GUIManager = GameManager.GUIManager

function FriendChatContentPersionItem:Awake()
    self:InitVar()
    self:InitComps()
    self:InitEvent()
end

function FriendChatContentPersionItem:OnDestroy()

end

function FriendChatContentPersionItem:InitVar()
    self._isVisible = true
end

function FriendChatContentPersionItem:InitEvent()
end

function FriendChatContentPersionItem:InitComps()
    self._textBlock = UIChatTextBlock.AddChatTextBlock(self.gameObject)
    
    self._textMessageTrans = self:FindChildGO("Text_Message").transform
    self._textMessage = self:GetChildComponent("Text_Message", "TextMeshWrapper")
    self._textMessage.text = ''
    self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Message')
    self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)
    
    self._typeIcon = self:FindChildGO("Container_TypeIcon")
    self._typeIcon:SetActive(false)
    self._containerMessageBG = self:FindChildGO("Container_BG/Image_MessageBG")

    self._MessageBGRect = self:GetChildComponent("Container_BG/Image_MessageBG", "RectTransform")
    --self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG", "ImageWrapper")
    self._MessageBG = self:FindChildGO("Container_BG/Image_MessageBG")
    
    self._nameSize = self:GetChildComponent("Container_BG/Image_NameBG", "RectTransform") --nameBGGo:GetComponent(typeof(RectTransform))

    self._nameText = self:GetChildComponent("Container_PlayerHead/Text_Name", "TextMeshWrapper")
    --self._level = self:GetChildComponent("Container_PlayerHead/Text_Level", "TextMeshWrapper")
    --self._playerHead = self:FindChildGO("Container_PlayerHead/Container_HeadIcon")
    self._playerHead = self:AddChildLuaUIComponent('Container_PlayerHead', PlayerHead)

    -- 语音
    self._voiceContainer = self:FindChildGO("Container_Voice")
    -- self._voiceContainer:SetActive(false)
    self._voiceRed = self:FindChildGO("Container_Voice/Image_Sign")
    self._voiceButton = self:FindChildGO("Container_Voice/Button_Play")
    self._csBH:AddClick(self._voiceButton, function() self:OnPlayVoice() end )
    self._voiceTimeText = self:GetChildComponent("Container_Voice/Text", "TextMeshWrapper")
    self._messageConteiner = self:FindChildGO('Container_Message')

    self._headFrameContainer = self:FindChildGO("Container_HeadFrame")
end

function FriendChatContentPersionItem:OnClickLink(linkId)
    -- LoggerHelper.Log('linkId:' .. linkId)
    if linkId ~= nil and linkId ~= '' then
        FriendManager:OnClickTextLink(linkId)
    end
end

function FriendChatContentPersionItem:UpdateItem(data)
    self._data = data
    --LoggerHelper.Log("FriendChatContentPersionItem:UpdateItem:".. PrintTable.TableToStr(self._data)) 
    return self:UpdateFriendItem(data)
    -- if data.show ~= nil then
    --     return self:UpdateWorldItem(data)
    -- else
        
    -- end
end


function FriendChatContentPersionItem:IsMe(value)
    self._isMe = value
end

function FriendChatContentPersionItem:UpdateItemCB(data)
    self._data = data
    --LoggerHelper.Log("FriendChatContentPersionItem:UpdateItem:".. PrintTable.TableToStr(self._data)) 
end

-- 世界聊天
function FriendChatContentPersionItem:UpdateWorldItem(data)

    self:UpdateHeadData(data)
    self:UpdateNameBG(data.name)
    self._textMessage.text = ''

    if self:HandlerVoice(data.content) then
        data.content = self:HandlerVoiceContent(data.content)
        local textHeight = self:UpdateVoiceText(data.content)
        local textWidth = self._textMessage.preferredWidth + 20
        self:UpdateTextBG(textWidth,textHeight)
        return textHeight
    end
    
    self._textMessage.text = data.content
    local height = self._textMessage.preferredHeight
    local width = self._textMessage.preferredWidth
    -- LoggerHelper.Log("self._textMessage.flexibleHeight:" .. self._textMessage.flexibleHeight)
    -- LoggerHelper.Log("self._textMessage.preferredHeight:" .. self._textMessage.preferredHeight)
    -- LoggerHelper.Log("self._textMessage.renderedHeight:" .. self._textMessage.renderedHeight)
    self:UpdateTextBG(width + 40,height + 12)
    return height
end

-- 语音 文字特殊处理
function FriendChatContentPersionItem:UpdateVoiceText(content)
    self._messageConteiner:SetActive(false)
    self._textMessage.text = content
    return self._textMessage.preferredHeight + 16
end

-- 更新 文本 背景 大小
function FriendChatContentPersionItem:UpdateTextBG(width,height)
    if width > 300 then
        width = 300
    end
    self._MessageBGRect.sizeDelta = Vector2.New(width, height)
    if self._starState1Image then
        self._starState1Image:SetAllDirty()
    end
end


-- 更新头像信息
function FriendChatContentPersionItem:UpdateHeadData(data)
    self._playerHead:SetPlayerHeadData(data.name, data.level, self:GetHeadIconId( data.icon ),data.uuid,'guidname',data.cross_dbid, data.guild_dbid)
end

-- 更新名字背景长度
function FriendChatContentPersionItem:UpdateNameBG(name)
    self._nameSize.sizeDelta = Vector2.New(#name * 10 + 30, 24)
end

function FriendChatContentPersionItem:GetHeadIconId(id)
    local headIconId = 0--CharacterDataHelper.GetIconID(id)
    return headIconId
end

-- 好友聊天聊天
function FriendChatContentPersionItem:UpdateFriendItem(data)
    --LoggerHelper.Log("FriendChatContentPersionItem:UpdateFriendItem:".. PrintTable.TableToStr(data))

    -- 1 是频道， 2是说话人的dbid， 3是说话人的名字， 4是说话人icon，5是说话人等级，6是说话内容
    local paramstr = ""
    local name = ""

    --data._contentStr = data._contentStr.."<vmsg=1.0><vtime=1.7>"

    
     if data._type == 0 then --自己的聊天内容
           name = GameWorld.Player().name
           self._nameText.text = "Lv"..GameWorld.Player().level.." "..name
           if GameWorld.Player().vocation and GameWorld.Player().vocation~=0 then
                local icon = RoleDataHelper.GetHeadIconByVocation(GameWorld.Player().vocation)
                --GameWorld.AddIcon(self._playerHead, icon)
                local nameLevel = name.."  ".."Lv"..GameWorld.Player().level
                self._playerHead:SetPlayerHeadData(nameLevel,"", icon)
           end 

           self:UpdateHeadFrame()
           self:UpdateChatFrame()
    --     local headIconId =     
         --self._playerHead:SetPlayerHeadData(name, GameWorld.Player().level)
    --     paramstr = GameWorld.Player().cross_uuid .. ',1,' .. GameWorld.Player().name .. ',aa,' .. GameWorld.Player().level
     else
            --头像框
        if (data._photoFrameId or 0) > 0 then
            local modelId = FashionDataHelper.GetFashionModel(data._photoFrameId)
            GameWorld.AddIcon(self._headFrameContainer,modelId,0,false,1,true,false)
        else
            GameWorld.AddIcon(self._headFrameContainer,9047,0,false,1,true,false)
        end

            --气泡
    if (data._chatFrameId or 0) > 0 then
        local modelId = FashionDataHelper.GetFashionModel(data._chatFrameId)
        GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,modelId,self._isMe)
        GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,modelId,self._isMe)
        
        self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble"..modelId.."(Clone)".."/Image", "ImageWrapper")
        --self._starState1Image:SetAllDirty()
    else
        if self._isMe then
            GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,0,true)
            GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,0,true)
            self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble".."0".."(Clone)".."/Image", "ImageWrapper")
            --self._starState1Image:SetAllDirty()
        else
            GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,1)
            GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,1)
            self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble".."1".."(Clone)".."/Image", "ImageWrapper")
            --self._starState1Image:SetAllDirty()
        end
    end

           local friendlist = FriendData:GetFriendListData()
           --LoggerHelper.Error("friendlist"..PrintTable:TableToStr(friendlist))
           local friendInfo = FriendData:GetFriendInfoByDbid(data._dbid)
           if friendInfo then
                self._nameText.text = friendInfo:GetName().." "..friendInfo:GetLevelStr()
                --LoggerHelper.Error("friendInfo"..PrintTable:TableToStr(friendInfo))
                if friendInfo:GetVocation() and friendInfo:GetVocation()~=0 then
                    local icon = RoleDataHelper.GetHeadIconByVocation(friendInfo:GetVocation())
                    --GameWorld.AddIcon(self._playerHead, icon)
                    local nameLevel = friendInfo:GetName().."  "..friendInfo:GetLevelStr()
                    self._playerHead:SetPlayerHeadData(nameLevel,"", icon)
                end 
           end
     end
    self:UpdateNameBG(name)

    self._textMessage.text = ''
    


    self:HandlerVoice(data._contentStr)
    local content = ""
    if string.find( data._contentStr,'<vmsg=' ) then
        if self:HandlerVoice(data._contentStr) then
            content = self:HandlerVoiceContent(data._contentStr)
            --content = ""
            local textHeight = self:UpdateVoiceText(content)
        local textWidth = self._textMessage.preferredWidth
            self:UpdateTextBG(textWidth+20,textHeight+20)
            --LoggerHelper.Error("self._MessageBGRect.localPosition"..self._MessageBGRect.localPosition.y)
            --self._MessageBGRect.localPosition = Vector3.New(288,70,0)
            
            --LoggerHelper.Error("self._MessageBGRect.localPositionY"..self._MessageBGRect.localPosition.y)
            --LoggerHelper.Error("notself._isMe===================================")
            if self._isMe then
                --LoggerHelper.Error("self._isMe2121===================================".. data._contentStr)
                --self._MessageBGRect.anchoredPosition = Vector2(288,-80)
                self._MessageBGRect.anchoredPosition = Vector2(265,-80)
                if textWidth < 268 then
                    --self._textMessageTrans.anchoredPosition = Vector2(268-textWidth,20)
                    
                    self._textMessageTrans.anchoredPosition = Vector2(235-width,3)
                else
                    --self._textMessageTrans.anchoredPosition = Vector2(0,20)
                    self._textMessageTrans.anchoredPosition = Vector2(-26,3)
                end
            else
                self._MessageBGRect.anchoredPosition = Vector2(62,-68)
                self._textMessageTrans.anchoredPosition = Vector2(143,5)
                --LoggerHelper.Error("Notself._isMe2121===================================".. data._contentStr)
            end
            return textHeight+20
        end
    end


    
    local length = string.utf8lenwithoutcolor(data._contentStr)
    local subContent = ""
    subContent = data._contentStr
    -- if length < 26 then
    --     subContent = data._contentStr
    -- else
    --     subContent = string.sub(data._contentStr, 1, length/2).."\n"..string.sub(data._contentStr,length/2+1, length)
    -- end
    self._textMessage.text = subContent
    --LoggerHelper.Log("FriendChatContentPersionItem:sub"..subContent)
    
    local height = self._textMessage.preferredHeight
    local width = self._textMessage.preferredWidth

    --LoggerHelper.Error("self._textMessageTrans.anchoredPosition11")
    if self._isMe then
        if width < 268 then    
            self._MessageBGRect.anchoredPosition = Vector2(265,-57)
            self._textMessageTrans.anchoredPosition = Vector2(230-width,18)
        else
            self._textMessageTrans.anchoredPosition = Vector2(-26,18)
            self._MessageBGRect.anchoredPosition = Vector2(265,-57)
            --LoggerHelper.Error("self._textMessageTrans.anchoredPosition22")
            --LoggerHelper.Error("self._textMessageTrans.anchoredPosition"..self._textMessageTrans.anchoredPosition.y)
        end
    else
        self._MessageBGRect.anchoredPosition = Vector2(62,-43)
        self._textMessageTrans.anchoredPosition = Vector2(151.2,28)
    end

    --LoggerHelper.Log("FriendChatContentPersionItem:UpdateFriendItem"..length)
    --local height = self._textMessage.preferredHeight
    self:UpdateTextBG(width + 40,height + 50)
    --LoggerHelper.Error("self._textMessageTrans.anchoredPositiongggggg"..self._textMessageTrans.anchoredPosition.y)
    
    return height+50



end

function FriendChatContentPersionItem:UpdateHeadFrame()
    local frameId = FashionManager:GetCurLoadFashionId(public_config.FASHION_TYPE_PHOTO)
    if frameId == self._frameId then
        return
    else
        self._frameId = frameId
    end
    if frameId and frameId > 0 then
        local modelId = FashionDataHelper.GetFashionModel(frameId)
        GameWorld.AddIcon(self._headFrameContainer,modelId,0,false,1,true,false)
    else
        GameWorld.AddIcon(self._headFrameContainer,9047,0,false,1,true,false)
    end
end

function FriendChatContentPersionItem:UpdateChatFrame()
    local chatFrameId = FashionManager:GetCurLoadFashionId(public_config.FASHION_TYPE_CHAT)
    if chatFrameId == self._chatFrameId then
        return
    else
        self._chatFrameId = chatFrameId
    end
    if chatFrameId and chatFrameId > 0 then
        local modelId = FashionDataHelper.GetFashionModel(chatFrameId)
        GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,modelId,self._isMe)
        GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,modelId,self._isMe)
        
        self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble"..modelId.."(Clone)".."/Image", "ImageWrapper")
        --self._starState1Image:SetAllDirty()
    else
        GUIManager.AddCommonIcon(self._containerMessageBG,CommonIconType.Bubble,0,self._isMe)
        GUIManager.AddCommonIcon(self._voiceButton,CommonIconType.Bubble,0,self._isMe)
        self._starState1Image = self:GetChildComponent("Container_BG/Image_MessageBG/Container_Bubble".."0".."(Clone)".."/Image", "ImageWrapper")
        --self._starState1Image:SetAllDirty()
    end
end

function FriendChatContentPersionItem:SetMessageBlock(channel, paramstr, liststr, content)
    self._messageConteiner:SetActive(true)
    local height = 0
    if self._textBlock ~= nil then
        content = ChatManager:CheckContentLink(content)
        height = height + self._textBlock:AddTextBlock(channel, paramstr, liststr, content)
    end
    return height
end

function FriendChatContentPersionItem:SetVisible(value)
    self:SetActive(value)
    self._isVisible = value
end

function FriendChatContentPersionItem:IsVisible()
    return self._isVisible
end

-- 播放语音
function FriendChatContentPersionItem:OnPlayVoice()
    self._voiceRed.gameObject:SetActive(false)
    ChatManager.OnStartPlayVoice(self._voiceId,self._voiceTime)
end

-- 是否显示语音
function FriendChatContentPersionItem:OnShowVoice(value)
    self._voiceContainer:SetActive(value)
    --self._MessageBG:SetActive(not value)
    --self._textMessage.gameObject:SetActive(not value)
end

-- 是否是语音文件
function FriendChatContentPersionItem:HandlerVoice(content)
    if  string.find( content,'<vmsg=' ) then
        self:OnShowVoice(true)
        self:HandlerVoiceData(content)
        self._voiceTimeText.text = string.format("%.1f", tonumber( self._voiceTime ) / 1000) 
        return true
    else
        self:OnShowVoice(false)
        return false
    end
    return false
end

-- 语音数据
function FriendChatContentPersionItem:HandlerVoiceData(content)
    self._voiceId = self:GetDataByContent(content,'<vmsg=' )
    self._voiceTime = self:GetDataByContent(content,'<vtime=' )
end

function FriendChatContentPersionItem:GetDataByContent(content,pattern)
    local chatId_start_i,chatId_start_j = string.find( content,pattern )
    local chatId_end_i,chatId_end_j = string.find( content,'>',chatId_start_j)
    local data = string.sub( content, chatId_start_j + 1 , chatId_end_i - 1 )
    return data
end

function FriendChatContentPersionItem:HandlerVoiceContent(content)
    local pattern = '<vmsg=' .. self._voiceId .. '>'
    content = string.gsub(content,pattern,'')
    pattern = '<vtime=' .. self._voiceTime .. '>'
    content = string.gsub(content,pattern,'')
    return content
end