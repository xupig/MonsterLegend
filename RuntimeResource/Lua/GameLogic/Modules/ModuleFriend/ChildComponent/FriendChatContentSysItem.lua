--FriendChatContentSysItem
local FriendChatContentSysItem = Class.FriendChatContentSysItem(ClassTypes.BaseLuaUIComponent)

FriendChatContentSysItem.interface = GameConfig.ComponentsConfig.Component
FriendChatContentSysItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendChatContentSysItem"

local TeamManager = PlayerManager.TeamManager
local GUIManager = GameManager.GUIManager
--local TeamConfig = GameConfig.TeamConfig
local SystemInfoManager = GameManager.SystemInfoManager
local ChatConfig = GameConfig.ChatConfig
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UILinkTextMesh = ClassTypes.UILinkTextMesh
local RectTransform = UnityEngine.RectTransform

function FriendChatContentSysItem:Awake()
    self:InitVar()
    self:InitComps()
    -- self:InitEvent()
end

function FriendChatContentSysItem:OnDestroy()

end

function FriendChatContentSysItem:InitVar()
    self._isVisible = false
end

function FriendChatContentSysItem:InitEvent()
end

function FriendChatContentSysItem:InitComps()
    -- self._buttonJoin = self:FindChildGO("Button_Join")
    -- self._csBH:AddClick(self._buttonJoin, function() self:OnJoin() end )

    self._buttonSize = self:GetChildComponent("Button_Join", "RectTransform")
    self._textMessage = self:GetChildComponent("Text_Notice", "TextMeshWrapper")
    -- self._typeIcon = self:FindChildGO('Container_TypeIcon')

    -- self._linkTextMesh = UILinkTextMesh.AddLinkTextMesh(self.gameObject,'Text_Notice')
    -- self._linkTextMesh:SetOnClickCB(function(linkIndex) self:OnClickLink(linkIndex) end)

    -- GameWorld.AddIcon(self._typeIcon, ChatConfig.System)
end

-- function FriendChatContentSysItem:OnClickLink(linkId)
--     -- LoggerHelper.Log('linkId:' .. linkId)
--     if linkId ~= nil and linkId ~= '' then
--         self:OnJoin(linkId)
--     end
-- end

-- function FriendChatContentSysItem:UpdateItem(data)
--     self._data = data
--     self._textMessage.text = data.content
--     local height = self._textMessage.preferredHeight
--     self:UpdateButtonSize(height)
--     return height - 50
-- end

function FriendChatContentSysItem:UpdateSysItem(data)
    self.gameObject:GetComponent(typeof(RectTransform)).transform.localPosition = Vector3.New(60,-60,0)
    self._data = data
    self._textMessage.text = LanguageDataHelper.CreateContent(75658)
    local height = 100
    --self:UpdateButtonSize(height)
    return height - 50
end

function FriendChatContentSysItem:UpdateButtonSize(height)
    --self._buttonSize.sizeDelta = Vector2.New(self._textMessage.preferredWidth,height)
end

function FriendChatContentSysItem:SetVisible(value)
    self:SetActive(value)
    self._isVisible = value
end

function FriendChatContentSysItem:IsVisible()
    return self._isVisible
end

function FriendChatContentSysItem:OnJoin(linkId)
    --ChatManager:OnClickTextLink(linkId)

    -- local bs = string.split(linkId, ",")

    -- if bs[1] == ChatConfig.LinkTypesMap.Team then --组队链接
    --     self:JoinTeam(bs)
    -- end
end

