-- FriendChatContentItem.lua
local FriendChatContentItem = Class.FriendChatContentItem(ClassTypes.UIListItem)
UIListItem = ClassTypes.UIListItem
FriendChatContentItem.interface = GameConfig.ComponentsConfig.Component
FriendChatContentItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendChatContentItem"

require "Modules.ModuleFriend.ChildComponent.FriendChatContentPersionItem"
local FriendChatContentPersionItem = ClassTypes.FriendChatContentPersionItem

require "Modules.ModuleFriend.ChildComponent.FriendChatContentSysItem"
local FriendChatContentSysItem = ClassTypes.FriendChatContentSysItem
-- local ChatManager = PlayerManager.ChatManager
function FriendChatContentItem:Awake()
    self._base.Awake(self)
    self:InitComps()
end

function FriendChatContentItem:OnDestroy()

end

function FriendChatContentItem:InitComps()
    self._items = self:FindChildGO('Container_Items')
    self._systemView = self:AddChildLuaUIComponent('Container_Items/Container_Notice', FriendChatContentSysItem)
    self._persionView = self:AddChildLuaUIComponent('Container_Items/Container_Message', FriendChatContentPersionItem)
    self._persionSelfView = self:AddChildLuaUIComponent('Container_Items/Container_MessageSelf', FriendChatContentPersionItem)
    self._persionView:IsMe(false)
    self._persionSelfView:IsMe(true)
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    self._systemView:SetVisible(false)

    
end

--override
function FriendChatContentItem:OnRefreshData(data)
    -- LoggerHelper.Log("FriendChatContentItem" .. PrintTable:TableToStr(data))
    self._data = data
    local height = 50
    
    if data._type == 2 then
        if self._systemView then
            self._systemView:SetVisible(true)
            self._persionView:SetVisible(false)
            self._persionSelfView:SetVisible(false)
            height = self._systemView:UpdateSysItem(self._data)
        end
    end
 
    --别人说话
    if data._type == 1 then
        if self._persionView then
            self._persionView:SetVisible(true)
            self._systemView:SetVisible(false)
            self._persionSelfView:SetVisible(false)
            height = self._persionView:UpdateItem(self._data)
        end

    --自己说话
    elseif data._type == 0 then
        if self._persionView then
            self._persionView:SetVisible(false)
            self._systemView:SetVisible(false)
            self._persionSelfView:SetVisible(true)
            height = self._persionSelfView:UpdateItem(self._data)
        end

    end

    -- LoggerHelper.Log("FriendChatContentItem88" .. PrintTable:TableToStr(self._data))
    --
    -- --  LoggerHelper.Log(tostring(data.name) .. '=====================================================')
    -- if data.channel == 1 or data.name == '' then
    --     if not(self._systemView:IsVisible()) then
    --         self._systemView:SetVisible(true)
    --         self._persionView:SetVisible(false)
    --         self._persionSelfView:SetVisible(false)
    --     end
    --     height = self._systemView:UpdateItem(data)
    -- else
    --     -- print(GameWorld.Player().id .. 'GameWorld.Player().dbid' .. GameWorld.Player().cs.dbid .. ' ' .. data.dbid)
    --     local uuid = 1
    --     if data.show ~= nil then
    --         uuid = data.uuid
    --         data.show = true
    --     else
    --         local type = data.type
    --         if type == 0 then --类型 0 自己说话 1 好友说话
    --             uuid = GameWorld.Player().cross_uuid
    --         end
    --     end
    --     -- LoggerHelper.Error(GameWorld.Player().cross_uuid .. 'GameWorld.Player().cross_uuid')
    --     if uuid == GameWorld.Player().cross_uuid then
    --         -- self._persionSelfView:SetActive(true)
    --         if not(self._persionSelfView:IsVisible()) then
    --             self._systemView:SetVisible(false)
    --             self._persionView:SetVisible(false)
    --             self._persionSelfView:SetVisible(true)
    --         end
    --         height = self._persionSelfView:UpdateItem(data)
    --     else
    --         -- self._persionView:SetActive(true)
    --         if not(self._persionView:IsVisible()) then
    --             self._systemView:SetVisible(false)
    --             self._persionView:SetVisible(true)
    --             self._persionSelfView:SetVisible(false)
    --         end
    --         height = self._persionView:UpdateItem(data)
    --     end
    -- end
    self:UpdateItemHeight(height)
end

function FriendChatContentItem:UpdateItemHeight(height)
    if self._rectTransform then
        local size = self._rectTransform.sizeDelta
        size.y = height + 40
        self._rectTransform.sizeDelta = size
        
        self._items.transform.localPosition = Vector3.New(0, 0, 0)
    end

end
