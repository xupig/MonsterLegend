-- FriendChatSecItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local FriendChatSecItem = Class.FriendChatSecItem(UINavigationMenuItem)
FriendChatSecItem.interface = GameConfig.ComponentsConfig.PointableComponent
FriendChatSecItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendChatSecItem"

local FriendIntimacyDataHelper = GameDataHelper.FriendIntimacyDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil

require 'UIComponent.Extend.PlayerHead'
local PlayerHead = ClassTypes.PlayerHead

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local public_config = GameWorld.public_config
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendChatType = GameConfig.EnumType.FriendChatType

function FriendChatSecItem:Awake()
    UINavigationMenuItem.Awake(self)
    
    self._txtFriendName = self:GetChildComponent("Text_FriendName", 'TextMeshWrapper')
    self._txtVip = self:GetChildComponent("Text_Vip", 'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Level", 'TextMeshWrapper')
    self._imgSelected = self:FindChildGO("Image_Selected")
    self._imgSelected:SetActive(false)
    
    self._imgNoRead = self:FindChildGO("Image_No_Read")
    self._txtNoRead = self:GetChildComponent("Image_No_Read/Text_No_Read", 'TextMeshWrapper')


    self._csBH = self:GetComponent("LuaUIComponent")
    
    self._heartButton = self:FindChildGO("ProgressBar_Heart")
    --self._csBH:AddClick(self._heartButton, function()self:OnHeartClick() end)
    self._heartButton:SetActive(false)

    self._txtFriendly = self:GetChildComponent("Text_Heart", 'TextMeshWrapper')

    self._heartButtonNew = self:FindChildGO("Button_Heart")
    self._csBH:AddClick(self._heartButtonNew, function()self:OnHeartClick() end)
    --self._containerHead = self:FindChildGO("Container_Icon")
    self._containerHead = self:AddChildLuaUIComponent('Container_Icon', PlayerHead)
    
    self:InitProgress()
    self._unReadInfo = function()self:UpdateUnReadInfo() end
    self._imgNoRead.gameObject:SetActive(false)
    EventDispatcher:AddEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self._unReadInfo)
    
    self._onlineFriendUpdate = function(friendDbid)self:GetOnlineResp(friendDbid) end
    EventDispatcher:AddEventListener(GameEvents.GetOnlineFiendResp,self._onlineFriendUpdate)

    self._offineFriendUpdate = function(friendDbid)self:GetOffineResp(friendDbid) end
    EventDispatcher:AddEventListener(GameEvents.GetOffineFiendResp,self._offineFriendUpdate)
end

function FriendChatSecItem:UpdateUnReadInfo()
    local dbid = self._dbid
    if dbid then


    end
end

function FriendChatSecItem:GetOffineResp(friendDbid)
    --LoggerHelper.Log("FriendChatTypeItem:GetOffineResp: ")
    local dbid = self._dbid
    if dbid then
       if dbid == friendDbid then   
        if self.vocation then
            --LoggerHelper.Log("FriendChatTypeItem:GetOnlineResp22: ")
            local icon = RoleDataHelper.GetHeadIconByVocation(self._data:GetVocation())
            self._containerHead:SetPlayerHeadData(self._data:GetName(), self._data:GetLevelStr(), icon, function(posVec)self:OnViewInfoClick(posVec) end,0)
        end
       end
    end
end

function FriendChatSecItem:GetOnlineResp(friendDbid)
    --LoggerHelper.Log("FriendChatTypeItem:GetOnlineResp: ")
    local dbid = self._dbid
    if dbid then
        if dbid == friendDbid then
            if self.vocation then
                --LoggerHelper.Log("FriendChatTypeItem:GetOnlineResp22: ")
                local icon = RoleDataHelper.GetHeadIconByVocation(self._data:GetVocation())
                self._containerHead:SetPlayerHeadData(self._data:GetName(), self._data:GetLevelStr(), icon, function(posVec)self:OnViewInfoClick(posVec) end,1)
            end
        end

    end
end

--override {type}
function FriendChatSecItem:OnRefreshData(data)
    if  table.isEmpty(data)~=true then
        --LoggerHelper.Error("self:GetSIndex(): " .. self:GetSIndex())
        if self:GetSIndex() == 0 then
            if self._imgSelected then
                self._imgSelected:SetActive(true)
            end
        else
            if self._imgSelected then
                self._imgSelected:SetActive(false)
            end
        end

        self._data = data
        self._txtFriendName.text = ""
        self._txtLevel.text = ""
        self._txtVip.text = data:GetVipStr()
        self._dbid = data:GetDbid()
        self._friendly = data:GetFriendlyVal()
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = data:GetFriendlyVal()
        self._txtFriendly.text = LanguageDataHelper.CreateContent(75624, argsTable)
        --local percent = FriendIntimacyDataHelper:GetPercent(self._friendly)
        --LoggerHelper.Log("FriendChatTypeItem:OnRefreshData:percent " .. percent)
        local dbid = self._dbid
        local chatnum = FriendData:GetUnReadList()
        if chatnum[dbid] and chatnum[dbid]>0 then
            if FriendData:GetCurClickIndex() == FriendChatType.Black then
                self._imgNoRead.gameObject:SetActive(false)
                FriendData:ClearTipChatListData(curChatPerson:GetDbid())
            else
                self._imgNoRead.gameObject:SetActive(true)
                self._txtNoRead.text = chatnum[dbid]
            end
        else
            self._imgNoRead.gameObject:SetActive(false)
        end

        -- if percent then        
        --     self._pbHeat:SetProgress(percent)
        -- end
  
        local activeSelf = self._imgSelected.activeSelf
        if activeSelf then
            EventDispatcher:TriggerEvent(GameEvents.UpdateFriendListRed)
            self._imgNoRead.gameObject:SetActive(false)
        end


        self.vocation = data:GetVocation() 
        if data:GetVocation() then
            local icon = RoleDataHelper.GetHeadIconByVocation(data:GetVocation())
            self._containerHead:SetPlayerHeadData(data:GetName(), data:GetLevelStr(), icon, function(posVec)self:OnViewInfoClick(posVec) end,data:GetIsOnline())
            --LoggerHelper.Log("FriendChatTypeItem:GetOnlineResp22: "..data:GetIsOnline())
        -- GameWorld.AddIcon(self._containerHead, icon)
        end
    end
end

function FriendChatSecItem:OnViewInfoClick(posVec)
    --LoggerHelper.Log("FriendChatSecItem:OnViewInfoClick"..PrintTable:TableToStr(posVec))

    --local pos = Vector2.New(posVec["x"]+50, posVec["y"]-600)
    -- local pinX = 50--(50*)/1280
    -- local pinY = 600--(600*900)/720
    -- local pos = Vector2.New(posVec["x"]+pinX, posVec["y"]-pinY)
    --LoggerHelper.Log("FriendChatSecItem:OnViewInfoClickpos"..PrintTable:TableToStr(pos))
    local d = {}
    d.playerName = self._data:GetName()
    d.dbid = self._data:GetDbid()
    d.friendInfo = self._data
    d.listPos = posVec
    --LoggerHelper.Log("FriendChatSecItem:OnViewInfoClick"..PrintTable:TableToStr(d.listPos))

    --local screenPosition = Camera.main:WorldToScreenPoint(position)
    
    GUIManager.ShowPanel(PanelsConfig.PlayerInfoMenu, d)
end


function FriendChatSecItem:InitProgress()
    -- self._pbHeat = UIScaleProgressBar.AddProgressBar(self.gameObject, "ProgressBar_Heart")
    -- self._pbHeat:SetProgress(0)
    -- self._pbHeat:ShowTweenAnimate(false)
    -- self._pbHeat:SetMutipleTween(false)
    -- self._pbHeat:SetIsResetToZero(false)
end

function FriendChatSecItem:UpdateCount()
    self._txtCount.text = tostring(bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._itemId))
    local matInfo = {}
    matInfo[1] = self._data.item1_id
    matInfo[2] = self._data.item2_id
    matInfo[3] = self._data.item3_id
    
    local showRed = true
    for i = 1, 3 do
        if matInfo[i] then
            for itemId, itemCount in pairs(matInfo[i]) do
                local bagCount = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
                if itemCount > bagCount then
                    showRed = false
                end
            end
        end
    end
    
    self._imgRedPoint:SetActive(showRed)
end

function FriendChatSecItem:SetSelected(isSelected)
    if self._imgSelected then
        self._imgSelected:SetActive(isSelected)
    end

    --self._imgSelected:SetActive(isSelected)

    if isSelected then
        local curChatPerson = FriendData:GetCurChatPersonData() 
        FriendData:ClearUnReadOffineByDbid(curChatPerson:GetDbid())
        --FriendData:ClearTipChatListData(curChatPerson:GetDbid())
        FriendData:ClearNewChatNumByDbid(curChatPerson:GetDbid())
        EventDispatcher:TriggerEvent(GameEvents.UpdateFriendListRed)
        self._imgNoRead.gameObject:SetActive(false)  
    end
end

function FriendChatSecItem:OnHeartClick()
    --LoggerHelper.Log("--------------------OnHeartClick")
    if self._data then
        EventDispatcher:TriggerEvent(GameEvents.OPEN_FRIEND_ATT,self._data)
        -- local data = {["id"] = FriendPanelType.Att, ["value"] = self._data}
        -- GUIManager.ShowPanel(PanelsConfig.Friend, data, nil)
    end
end

function FriendChatSecItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self._unReadInfo)
    EventDispatcher:RemoveEventListener(GameEvents.GetOnlineFiendResp,self._onlineFriendUpdate)
    EventDispatcher:RemoveEventListener(GameEvents.GetOffineFiendResp,self._offineFriendUpdate)
    --LoggerHelper.Log("FriendChatTypeItem:OnDestroy===RemoveEventListener===UpdateUnReadInfo======================: ")
end
