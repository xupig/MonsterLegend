-- FriendRecommondItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local FriendRecommondItem = Class.FriendRecommondItem(ClassTypes.UIListItem)
FriendRecommondItem.interface = GameConfig.ComponentsConfig.Component
FriendRecommondItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendRecommondItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local FriendManager = PlayerManager.FriendManager



function FriendRecommondItem:Awake()
    self:InitItem()
end

function FriendRecommondItem:OnDestroy()
-- self._textInfo = nil
-- self._containerDisActive = nil
-- self._containerActive = nil
end

function FriendRecommondItem:InitItem()
    self._txtFriendName = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._txtVip = self:GetChildComponent("Text_Vip", 'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Title", 'TextMeshWrapper')
    self._imgSelected = self:FindChildGO("Image_Sele")
    self._imgSelected:SetActive(false)
    self._containerHead = self:FindChildGO("Container_Icon")
    
    self._csBH = self:GetComponent("LuaUIComponent")
    self._btnAdd = self:FindChildGO("Button_Add")
    self._csBH:AddClick(self._btnAdd, function()self:AddFriendClick() end)
end

function FriendRecommondItem:AddFriendClick()
    -- GameWorld.LoggerHelper.Log("发送加好友RPC")
    local dbid = FriendManager:GetDbidFromBlackList(self._dbid)
    if dbid then
    else
        FriendManager:SendAddFriendReq(self._dbid)
        self._imgSelected:SetActive(true)
        self._btnAdd:SetActive(false)
    end
end

function FriendRecommondItem:SetActive(flag)
    -- self._containerActive:SetActive(flag)
    -- self._cont\
    ainerDisActive:SetActive(not flag)
end

function FriendRecommondItem:OnRefreshData(data)
    if data then
        -- LoggerHelper.Log("FriendRecommondItem:OnRefreshData .." .. PrintTable:TableToStr(data))
        self._txtFriendName.text = data:GetName()
        self._txtLevel.text = data:GetLevelStr()
        self._dbid = data:GetDbid()
        self._txtVip.text = data:GetVipStr()
        self._imgSelected:SetActive(false)
        self._btnAdd:SetActive(true)
        -- LoggerHelper.Log("FriendRecommondItem:OnRefreshData .." .. PrintTable:TableToStr(data))
        if data:GetVocation() and data:GetVocation() then
            local icon = RoleDataHelper.GetHeadIconByVocation(data:GetVocation())
            GameWorld.AddIcon(self._containerHead, icon)
        end
    end
-- self._textInfo.text = LanguageDataHelper.CreateContent(data[1])
-- self:SetActive(data[3] == true)
end
