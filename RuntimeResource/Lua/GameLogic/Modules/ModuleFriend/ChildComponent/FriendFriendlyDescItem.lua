-- FriendFriendlyDescItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local FriendFriendlyDescItem = Class.FriendFriendlyDescItem(ClassTypes.UIListItem)
FriendFriendlyDescItem.interface = GameConfig.ComponentsConfig.Component
FriendFriendlyDescItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendFriendlyDescItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendIntimacyDataHelper = GameDataHelper.FriendIntimacyDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local FriendManager = PlayerManager.FriendManager

function FriendFriendlyDescItem:Awake()
    self:InitItem()
end

function FriendFriendlyDescItem:OnDestroy()
-- self._textInfo = nil
-- self._containerDisActive = nil
-- self._containerActive = nil
end

function FriendFriendlyDescItem:InitItem()
    self._txtDesc = self:GetChildComponent("Text_Des_1", 'TextMeshWrapper')
    self._txtAttack = self:GetChildComponent("Text_Des_2", 'TextMeshWrapper')
-- self._txtVip = self:GetChildComponent("Text_Vip", 'TextMeshWrapper')
-- self._txtLevel = self:GetChildComponent("Text_Title", 'TextMeshWrapper')
-- self._imgSelected = self:FindChildGO("Image_Sele")
-- self._imgSelected:SetActive(false)
-- self._containerHead = self:FindChildGO("Container_Icon")
-- self._csBH = self:GetComponent("LuaUIComponent")
-- self._btnAdd = self:FindChildGO("Button_Add")
-- self._csBH:AddClick(self._btnAdd, function()self:AddFriendClick() end)
-- self._imgRedPoint = self:FindChildGO("Image_RedPoint")
-- self._imgRedPoint:SetActive(false)
-- --当前选中的成品icon
-- self._textInfo = self:GetChildComponent("Text_Info", "TextMeshWrapper")
-- self._containerDisActive = self:FindChildGO("Container_DisActive")
-- self._containerActive = self:FindChildGO("Container_Active")
end

function FriendFriendlyDescItem:AddFriendClick()
    -- GameWorld.LoggerHelper.Log("发送加好友RPC")
    FriendManager:SendAddFriendReq(self._dbid)
    self._imgSelected:SetActive(true)
    self._btnAdd:SetActive(false)

--FriendManager:DeleteRecommendList(self._dbid)
end

function FriendFriendlyDescItem:SetActive(flag)
    -- self._containerActive:SetActive(flag)
    ainerDisActive:SetActive(not flag)
end

function FriendFriendlyDescItem:OnRefreshData(id)
    if id then
        --LoggerHelper.Log("FriendFriendlyDescItem:OnRefreshData1 .." .. PrintTable:TableToStr(data))
        local name = LanguageDataHelper.CreateContent(FriendIntimacyDataHelper:GetName(id))
        self._txtDesc.text = name
        --LoggerHelper.Log("FriendFriendlyDescItem:OnRefreshData2 .." .. PrintTable:TableToStr(data.desc_attri))
        local argsTable = LanguageDataHelper.GetArgsTable()


        --LoggerHelper.Error("AttDescList"..PrintTable:TableToStr(AttDescList))

        argsTable["0"] = FriendIntimacyDataHelper:GetAttri(id)[1][2]
        argsTable["1"] = FriendIntimacyDataHelper:GetAttri(id)[2][2]
        if  FriendIntimacyDataHelper:GetAttri(id)[3]~=nil then
            argsTable["2"] = FriendIntimacyDataHelper:GetAttri(id)[3][2]
        else
            argsTable["2"] = 0
        end

        self._txtAttack.text = LanguageDataHelper.CreateContent(FriendIntimacyDataHelper:GetDescAttri(id), argsTable) 

    end
-- self._textInfo.text = LanguageDataHelper.CreateContent(data[1])
-- self:SetActive(data[3] == true)
end
