--ApplyListItem.lua
require 'UIComponent.Extend.PlayerHead'

local ApplyListItem = Class.ApplyListItem(ClassTypes.UIListItem)
ApplyListItem.interface = GameConfig.ComponentsConfig.Component
ApplyListItem.classPath = "Modules.ModuleFriend.ChildComponent.ApplyListItem"

local FriendManager = PlayerManager.FriendManager
-- local PlayerHead = ClassTypes.PlayerHead
function ApplyListItem:Awake()
    self._base.Awake(self)
    self:InitComps()

end

function ApplyListItem:OnDestroy()

end

function ApplyListItem:OnRefreshData(data)
    if data then
        -- LoggerHelper.Log("FriendRecommondItem:OnRefreshData .." .. PrintTable:TableToStr(data))
        self._txtFriendName.text = data:GetName()
        self._txtLevel.text = data:GetLevelStr()
        self._dbid = data:GetDbid()
        self._txtVip.text = data:GetVipStr()
       -- LoggerHelper.Log("ApplyListItem:OnAgreeClick()2222"..data[1]["11"])
        if data:GetVocation() and data:GetVocation() ~= 0 then
            local icon = RoleDataHelper.GetHeadIconByVocation(data:GetVocation())
            GameWorld.AddIcon(self._containerHead, icon)
        end
    end

-- self._uuid = data:GetUUID()
-- self._playerHead:SetPlayerHeadDataByTable(data)
end

function ApplyListItem:InitComps()
    self._csBH = self:GetComponent("LuaUIComponent")
    
    self._agreeButton = self:FindChildGO("Button_Agree")
    self._csBH:AddClick(self._agreeButton, function()self:OnAgreeClick() end)
    
    self._refuseButton = self:FindChildGO("Button_Refuse")
    self._csBH:AddClick(self._refuseButton, function()self:OnRefuseClick() end)
    
    self._blackButton = self:FindChildGO("Button_Black")
    self._csBH:AddClick(self._blackButton, function()self:OnBlackClick() end)
    
    
    self._txtFriendName = self:GetChildComponent("Text_Name", 'TextMeshWrapper')
    self._txtVip = self:GetChildComponent("Text_Vip", 'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Title", 'TextMeshWrapper')
    
    self._containerHead = self:FindChildGO("Container_Icon")


end

function ApplyListItem:OnAgreeClick()
    -- LoggerHelper.Log("ApplyListItem:OnAgreeClick()"..self._dbid)
    
    FriendManager:SendAgreeFriendReq(self._dbid)
    FriendManager:DeleApplyListData(self._dbid)
end

function ApplyListItem:OnRefuseClick()
    -- GameWorld.LoggerHelper.Log("拒绝:"..self._uuid)
    -- local str = string.format("%s,%d",self._uuid,0)
    
    FriendManager:SendRefuseFriendReq(self._dbid)
    FriendManager:DeleApplyListData(self._dbid)

end

function ApplyListItem:OnBlackClick()
    
    FriendManager:AddBlackFriendReq(self._dbid)
    FriendManager:DeleApplyListData(self._dbid)
end
