-- FriendFriendlyItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
require "Modules.ModuleFriend.ChildComponent.FriendFriendlyDescItem"
local FriendFriendlyDescItem = ClassTypes.FriendFriendlyDescItem
local ItemGridEx = ClassTypes.ItemGridEx
local FriendFriendlyItem = Class.FriendFriendlyItem(ClassTypes.UIListItem)
FriendFriendlyItem.interface = GameConfig.ComponentsConfig.Component
FriendFriendlyItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendFriendlyItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendIntimacyDataHelper = GameDataHelper.FriendIntimacyDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local FriendManager = PlayerManager.FriendManager
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config


function FriendFriendlyItem:Awake()
    self:InitItem()
end

function FriendFriendlyItem:OnDestroy()
-- self._textInfo = nil
-- self._containerDisActive = nil
-- self._containerActive = nil
end

function FriendFriendlyItem:InitItem()
    self._txtDesc = self:GetChildComponent("Text_Des", 'TextMeshWrapper')
end


function FriendFriendlyItem:SetActive(flag)
    -- self._containerActive:SetActive(flag)
    ainerDisActive:SetActive(not flag)
end

--初始化位置表，储存各种点击状态的位置
function FriendFriendlyItem:InitPostionTable()

end



function FriendFriendlyItem:OnRefreshData(id)
    if id then
        local name = LanguageDataHelper.GetLanguageData(FriendIntimacyDataHelper:GetName(id))
        self._txtDesc.text = name .. ":" .. FriendIntimacyDataHelper:GetNum(id)
    --    self._txtDescTwo.text = name .. ":" .. data.num
    --    self._txtDescThree.text = name .. ":" .. data.num
    --     self._txtFriendName.text = data:GetName()
    --     self._txtLevel.text = data:GetLevel()
    --     self._dbid = data:GetDbid()
    --     LoggerHelper.Log("FriendFriendlyItem:OnRefreshData .." .. PrintTable:TableToStr(data))
    --     if data:GetVocation() and data:GetVocation() then
    --         local icon = RoleDataHelper.GetHeadIconByVocation(data:GetVocation())
    --         GameWorld.AddIcon(self._containerHead, icon)
    --     end
    end
-- self._textInfo.text = LanguageDataHelper.CreateContent(data[1])
-- self:SetActive(data[3] == true)
end
