--RecommendListItem.lua
-- require 'UIComponent.Extend.PlayerHead'
local RecommendListItem = Class.RecommendListItem(ClassTypes.UIListItem)

-- RecommendListItem.interface = GameConfig.ComponentsConfig.PointableComponent
-- RecommendListItem.classPath = "Modules.ModuleFriend.ChildComponent.RecommendListItem"

-- local FriendManager = PlayerManager.FriendManager
-- local PlayerHead = ClassTypes.PlayerHead

function RecommendListItem:Awake()
     self._base.Awake(self)
     self:InitComps()

end

function RecommendListItem:OnDestroy() 

end

function RecommendListItem:OnRefreshData(data)

    -- self._playerHead:SetPlayerHeadDataByTable(data)
    -- self._dbid = data:GetDBID()
end

function RecommendListItem:InitComps()
    self._txtFriendName = self:GetChildComponent("Text_FriendName",'TextMeshWrapper')
    self._txtVip = self:GetChildComponent("Text_Vip",'TextMeshWrapper')
    self._txtLevel = self:GetChildComponent("Text_Level",'TextMeshWrapper')
    self._imgSelected = self:FindChildGO("Image_Selected")
    self._imgSelected:SetActive(false)
   -- self._imgRedPoint = self:FindChildGO("Image_RedPoint")
   -- self._imgRedPoint:SetActive(false)


   -- --当前选中的成品icon
    local parent = self:FindChildGO("Container_Icon")
       local itemGo = GUIManager.AddItem(parent, 1)
       self._headIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
       self._headIcon:ActivateTipsById()
    -- self._playerHead = self:AddChildLuaUIComponent("Container_Head", PlayerHead)

    -- self._addButton = self:FindChildGO("Button_Add")
    -- self._csBH:AddClick(self._addButton, function() self:OnAddClick() end )

end

function RecommendListItem:OnAddClick()
    -- GameWorld.LoggerHelper.Log("发送加好友RPC")
    -- FriendManager:SendAddFriendReq(self._dbid)
    -- FriendManager:DeleteRecommendList(self._dbid)
end

