-- FriendChatTypeItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local FriendChatTypeItem = Class.FriendChatTypeItem(UINavigationMenuItem)
FriendChatTypeItem.interface = GameConfig.ComponentsConfig.PointableComponent
FriendChatTypeItem.classPath = "Modules.ModuleFriend.ChildComponent.FriendChatTypeItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ColorConfig = GameConfig.ColorConfig
local FriendChatType = GameConfig.EnumType.FriendChatType
local BaseUtil = GameUtil.BaseUtil
local FriendData = PlayerManager.PlayerDataManager.friendData
function FriendChatTypeItem:Awake()
	UINavigationMenuItem.Awake(self)
	
	self._navRed = self:FindChildGO("Image_Red")
	self._txtTypeName = self:GetChildComponent("Text_TypeName",'TextMeshWrapper')
	self._selected = false
	self._isGetSelect = 0
	self._navRed:SetActive(false)
	-- self._imgSelect = self:FindChildGO("Image_Selected")
	self._onlineFriendUpdate = function(friendDbid)self:GetOnlineChange(friendDbid) end
    EventDispatcher:AddEventListener(GameEvents.GetOnlineFiendResp,self._onlineFriendUpdate)

    self._offineFriendUpdate = function(friendDbid)self:GetOnlineChange(friendDbid) end
    EventDispatcher:AddEventListener(GameEvents.GetOffineFiendResp,self._offineFriendUpdate)
end

function FriendChatTypeItem:OnPointerDown(pointerEventData)

end

function FriendChatTypeItem:GetOnlineChange(friendDbid)
	if self:GetFIndex()+1 == FriendChatType.Recent then
		local argsTable = LanguageDataHelper.GetArgsTable()
		argsTable["0"] = FriendData:GetOnlineNumForFriendList()
		argsTable["1"] = FriendData:GetTotalNumForFriendList()
		local firstDesc = LanguageDataHelper.CreateContent(75662, argsTable)
		self._str = firstDesc
		self._txtTypeName.text = firstDesc
	elseif self:GetFIndex()+1 == FriendChatType.Friend then
		local argsTableFri = LanguageDataHelper.GetArgsTable()
		argsTableFri["0"] = FriendData:GetOnlineNumForFriendList()
		argsTableFri["1"] = FriendData:GetTotalNumForFriendList()
		local secDesc = LanguageDataHelper.CreateContent(75663, argsTableFri)
		self._str = secDesc
		self._txtTypeName.text = secDesc
	elseif self:GetFIndex()+1 == FriendChatType.Foe then
		local argsTableFoe = LanguageDataHelper.GetArgsTable()
		argsTableFoe["0"] = FriendData:GetOnlineNumForFoeList()
		argsTableFoe["1"] = FriendData:GetTotalNumForFoeList()
		local threeDesc = LanguageDataHelper.CreateContent(75664, argsTableFoe)
		self._str = threeDesc
		self._txtTypeName.text = threeDesc
	elseif self:GetFIndex()+1 == FriendChatType.Black then
		local argsTableBlack = LanguageDataHelper.GetArgsTable()
		argsTableBlack["0"] = FriendData:GetOnlineNumForBlackList()
		argsTableBlack["1"] = FriendData:GetTotalNumForBlackList()
		local fourDesc = LanguageDataHelper.CreateContent(75665, argsTableBlack)
		self._str = fourDesc
		self._txtTypeName.text = fourDesc
	end	
	self:UpdateRed()
end



function FriendChatTypeItem:SetSelected(isSelected)
	self._selected = isSelected
	if isSelected then
		local str = BaseUtil.GetColorString(self._str, ColorConfig.A)
		self._txtTypeName.text = str
	else
		self._txtTypeName.text = self._str
	end
end

function FriendChatTypeItem:ToggleSelected()
	self:SetSelected(not self._selected)
end

function FriendChatTypeItem:SetGetSelect()
	if self._isGetSelect == 1 then
		self._isGetSelect = 0
	elseif self._isGetSelect == 0 then
		self._isGetSelect = 1
	end
end

function FriendChatTypeItem:InitFSelect()
	self._isGetSelect = 0
	self._selected = false
end




function FriendChatTypeItem:GetSelected()
	return self._isGetSelect 
	
end

--override {type}
function FriendChatTypeItem:OnRefreshData(data)
	 if data then
		--LoggerHelper.Log("FriendChatTypeItem:index: " ..self:GetFIndex())
		self:GetOnlineChange(1)
	 end
end




function FriendChatTypeItem:UpdateRed()
	   if self:GetFIndex()+1 == FriendChatType.Recent then
		   if FriendData:GetIsReadList(FriendData:GetFoeListData()) or FriendData:GetIsReadList(FriendData:GetFriendListData()) then
			   self._navRed:SetActive(true)
		   else
			   self._navRed:SetActive(false)
		   end
	   elseif self:GetFIndex()+1 == FriendChatType.Friend then
		   if FriendData:GetIsReadList(FriendData:GetFriendListData()) then
			   self._navRed:SetActive(true)
		   else
			   self._navRed:SetActive(false)
		   end
	   elseif self:GetFIndex()+1 == FriendChatType.Foe then
		   if FriendData:GetIsReadList(FriendData:GetFoeListData()) then
			   self._navRed:SetActive(true)
		   else
			   self._navRed:SetActive(false)
		   end
	   elseif self:GetFIndex()+1 == FriendChatType.Black then
		   self._navRed:SetActive(false)
	   end	
end

function FriendChatTypeItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.GetOnlineFiendResp,self._onlineFriendUpdate)
	EventDispatcher:RemoveEventListener(GameEvents.GetOffineFiendResp,self._offineFriendUpdate)
	
end