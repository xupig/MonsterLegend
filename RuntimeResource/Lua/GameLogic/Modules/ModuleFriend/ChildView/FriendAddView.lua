--FriendAddView
-- require "Modules.ModuleFriend.ChildView.FriendApplyView"
require"Modules.ModuleFriend.ChildComponent.FriendRecommondItem"


local FriendAddView = Class.FriendAddView(ClassTypes.BaseLuaUIComponent)
FriendAddView.interface = GameConfig.ComponentsConfig.Component
FriendAddView.classPath = "Modules.ModuleFriend.ChildView.FriendAddView"
-- FriendAddView.interface = GameConfig.ComponentsConfig.Component
-- FriendAddView.classPath = "Modules.ModuleFriend.ChildView.FriendAddView"
local PanelsConfig = GameConfig.PanelsConfig
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local FriendRecommondItem = ClassTypes.FriendRecommondItem
-- local FriendManager = PlayerManager.FriendManager
-- local FriendApplyView = ClassTypes.FriendApplyView
-- local table = table
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local FriendData = PlayerManager.PlayerDataManager.friendData

function FriendAddView:Awake()
     self._csBH = self:GetComponent('LuaUIComponent')
     self:InitView()

     self:InitRecommondList()
     self._refreshRecommondFun = function () self:OnRefreshRecommendList() end
    -- --只发一次RPC
    -- FriendManager:SendApplyListReq()
    -- GameWorld.LoggerHelper.Log("发送申请列表RPC！！！！！！")

    -- self._canSendRecommendRPC = true
    -- self._isFirstOpen = true
    -- self._canSearch = true
end

function FriendAddView:ShowView(data)    
    self:AddEventListeners()
    self._recommondList:SetDataList(data)
    -- self:RefreshView()
    -- GameWorld.ShowPlayer():ShowModel(1)
    -- self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
end

function FriendAddView:CloseView() 
    --LoggerHelper.Error("FriendAddView:CloseView()")   
    self:RemoveEventListeners()

    -- self:RefreshView()
    -- GameWorld.ShowPlayer():ShowModel(1)
    -- self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
end

function FriendAddView:UpdateRecommondView()

end

function FriendAddView:InitRecommondList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._recommondList = list
    self._recommondListScrollView = scrollView
    self._recommondListScrollView:SetHorizontalMove(false)
    self._recommondListScrollView:SetVerticalMove(true)
    self._recommondList:SetItemType(FriendRecommondItem)
    self._recommondList:SetPadding(20, 20, 10, 20)
    self._recommondList:SetGap(0, 0)
    self._recommondList:SetDirection(UIList.DirectionLeftToRight, 2, 100)    
    -- local itemClickedCB = 
	-- function(idx)
	-- 	self:OnRecommondListItemClicked(idx)
	-- end
	-- self._recommondList:SetItemClickedCB(itemClickedCB)
end


function FriendAddView:OnDestroy()
end


function FriendAddView:AddEventListeners()
    --EventDispatcher:TriggerEvent(GameEvents.RefreshRecommendList)

       EventDispatcher:AddEventListener(GameEvents.RefreshRecommendForSearch, self._refreshRecommondFun)
end

function FriendAddView:OnRefreshRecommendList()
    local data = FriendManager:GetRecommondListData()
    self._recommondList:SetDataList(data)
end

function FriendAddView:RemoveEventListeners()

    EventDispatcher:RemoveEventListener(GameEvents.RefreshRecommendForSearch, self._refreshRecommondFun)
end


function FriendAddView:InitView()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnCloseClick() end)

    self._findButton = self:FindChildGO("Button_Find")
    self._csBH:AddClick(self._findButton, function() self:OnFindClick() end )

    self._changeButton = self:FindChildGO("Button_Change")
    self._csBH:AddClick(self._changeButton, function() self:OnChangeClick() end )

    
     self._searchText = self:GetChildComponent("Input_Search", "InputFieldMeshWrapper")
end

function FriendAddView:OnCloseClick()
    --GUIManager.ClosePanel(PanelsConfig.Friend)
end

function FriendAddView:OnChangeClick()
    if FriendData:GetIsRecommondChange() then
        FriendManager:SendRecommendListReq()
    else
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(75644))
    end
end

function FriendAddView:RefreshAddView()

end

----------------------------------------------Resp---------------------------------------
function FriendAddView:OnApplyListResp()
    --返回的列表可能为空，所以需要判断
    --self:RefreshAddView()
end

function FriendAddView:OnRecommendListResp()
    --self:ShowRecommendView()
end

function FriendAddView:OnAllApplyHandle()
    --self:ShowRecommendView()
end

function FriendAddView:OnFindResp()
    --self._canSearch = true
end
-----------------------------------------------------------------------------------------
function FriendAddView:OnFindClick()
    -- if self._canSearch == false then return end

    -- --客户端判断 是否是数字还有位数
     local name = self._searchText.text

     if name == '' then
         --GameManager.SystemInfoManager:ShowClientTip(3457)
        --  GameWorld.LoggerHelper.Log("输入内容为空！！")
         return
     end

    -- self:ShowSearchView()

    -- if name == nil then return end

    -- --不超过8位字符
    -- if string.len(name) >8 then
    --     -- GameManager.SystemInfoManager:ShowClientTip(3458)
    --     GameWorld.LoggerHelper.Log("字符串超过8位！！")
    --     return 
    -- end
     FriendManager:SendSearchReqByName(name)
     
    -- self._canSearch = false

end

function FriendAddView:OnCancelClick()
    -- self:RefreshAddView()
    -- self._searchText.text = ''
    -- GameWorld.LoggerHelper.Log("搜索栏清空")
end

function FriendAddView:ShowSearchView()
    -- self._friendApplyView:SetActive(false)
end

function FriendAddView:ShowRecommendView()
    -- self._friendApplyView:SetActive(false)
end

function FriendAddView:ShowApplyView()
    -- self._friendApplyView:SetActive(true)
end

function FriendAddView:SetCanSendRPC()
    --self._canSendRecommendRPC = true
end