
--FriendPanel
require "Modules.ModuleFriend.ChildView.FriendChatView"
require "Modules.ModuleFriend.ChildView.FriendAddView"
require "Modules.ModuleFriend.ChildView.FriendApplyView"
require "Modules.ModuleFriend.ChildView.FriendAttView"

local FriendViewEx = Class.FriendViewEx(ClassTypes.BaseLuaUIComponent)

FriendViewEx.interface = GameConfig.ComponentsConfig.Component
FriendViewEx.classPath = "Modules.ModuleFriend.ChildView.FriendViewEx"

local UIToggleGroup = ClassTypes.UIToggleGroup
local FriendChatView = ClassTypes.FriendChatView
local FriendAddView = ClassTypes.FriendAddView
local FriendApplyView = ClassTypes.FriendApplyView
local FriendAttView = ClassTypes.FriendAttView

local PanelsConfig = GameConfig.PanelsConfig
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ChatManager = PlayerManager.ChatManager
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local FriendChatType = GameConfig.EnumType.FriendChatType
local UINavigationMenu = ClassTypes.UINavigationMenu
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendData = PlayerManager.PlayerDataManager.friendData
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
function FriendViewEx:Awake()
    self._selectTab = -1
    self._curShowView = 0
    self._indexToGO = {}
    self._indexToView = {}
    self:InitViews()
    self._recommondDataListFun = function()self:OnRecommendDataList() end
    self._openAttFun = function(data)self:OnOpenAttFun(data) end
    self._friendApplyList = function()self:OnFriendApplyListRed() end
end


--情况4: 在好友系统的添加里点发消息
function FriendViewEx:OnSendMsgInFriend(uuid)

end

function FriendViewEx:OnOpenAttFun(data)
    self._indexToGO[FriendPanelType.Att]:SetActive(true)
    self._indexToView[FriendPanelType.Att]:ShowView(data)
end


function FriendViewEx:InitViews()   
    self._indexToGO[FriendPanelType.Chat] = self:FindChildGO("Container_Chat")
    self._indexToGO[FriendPanelType.Apply] = self:FindChildGO("Container_Apply")
    self._indexToGO[FriendPanelType.Add] = self:FindChildGO("Container_Add")
    self._indexToGO[FriendPanelType.Att] = self:FindChildGO("Container_Att")
    
    self._indexToView[FriendPanelType.Chat] = self:AddChildLuaUIComponent("Container_Chat", FriendChatView)
    self._indexToView[FriendPanelType.Apply] = self:AddChildLuaUIComponent("Container_Apply", FriendApplyView)
    self._indexToView[FriendPanelType.Add] = self:AddChildLuaUIComponent("Container_Add", FriendAddView)
    self._indexToView[FriendPanelType.Att] = self:AddChildLuaUIComponent("Container_Att", FriendAttView)
    
    for i = 1, #self._indexToGO do
        self._indexToGO[i]:SetActive(false)
    end

    self._btnCloseApply = self:FindChildGO("Container_Apply/Button_Close")
    self._csBH:AddClick(self._btnCloseApply, function()self:OnCloseApplyClick() end)

    self._btnCloseAdd = self:FindChildGO("Container_Add/Button_Close")
    self._csBH:AddClick(self._btnCloseAdd, function()self:OnCloseAddClick() end)

    self._btnCloseAtt = self:FindChildGO("Container_Att/Button_Close")
    self._csBH:AddClick(self._btnCloseAtt, function()self:OnCloseAttClick() end)


    self._btnAddFriend = self:FindChildGO("Container_Chat/Button_Add")
    self._csBH:AddClick(self._btnAddFriend, function()self:OnAddFriendClick() end)
    
    self._btnApplyFriend = self:FindChildGO("Container_Chat/Button_Apply")
    self._csBH:AddClick(self._btnApplyFriend, function()self:OnApplyFriendClick() end)
    self._applyFriendRed = self:FindChildGO("Container_Chat/Image_Red")
    self._applyFriendRed:SetActive(false)
end


function FriendViewEx:OnFriendApplyListRed()
    local applyNum = FriendData:GetTotalNumForApplyList()
    --LoggerHelper.Error("applyNum"..applyNum)
    if applyNum > 0 then
        self._applyFriendRed:SetActive(true)
    else
        self._applyFriendRed:SetActive(false)
    end
end

--加好友
function FriendViewEx:OnAddFriendClick()
    FriendManager:SendRecommendListReq()
end

--申请好友
function FriendViewEx:OnApplyFriendClick()
    FriendManager:SendApplyListReq()
    local data = self._applyList
    self._indexToGO[FriendPanelType.Apply]:SetActive(true)
    self._indexToView[FriendPanelType.Apply]:ShowView(data)
end


function FriendViewEx:OnCloseApplyClick()
    self._indexToView[FriendPanelType.Apply]:CloseView()
    self._indexToGO[FriendPanelType.Apply]:SetActive(false)
    FriendManager:SendApplyListReq()
end

function FriendViewEx:OnCloseAddClick()
    self._indexToView[FriendPanelType.Add]:CloseView()
    self._indexToGO[FriendPanelType.Add]:SetActive(false)
end

function FriendViewEx:OnCloseAttClick()
    self._indexToView[FriendPanelType.Att]:CloseView()
    self._indexToGO[FriendPanelType.Att]:SetActive(false)
end





--override
function FriendViewEx:OnDestroy()
-- self._csBH = nil
--self:RemoveEventListeners()
end

--override

function FriendViewEx:SetData(data)
    local index = data.id or 0
    local value = data.value
    self:ShowView(index, value)
end

--override
function FriendViewEx:CloseView()
    --LoggerHelper.Error("FriendPanel:OnClose()")
    self:RemoveEventListeners()

    if self._indexToView[FriendPanelType.Chat] then
        --LoggerHelper.Error("FriendPanel:OnClose()2222")
        self._indexToView[FriendPanelType.Chat]:CloseView()
    end
    FriendManager:GetDataClose()
end


function FriendViewEx:ShowView(data)


    self:AddEventListeners()
    --LoggerHelper.Error("FriendViewEx:OnShow(data):"..PrintTable:TableToStr(data))
    local index = FriendPanelType.Chat
    local value = 1
 
    if data then
        if data[1] == 2 then
            self:OnApplyFriendClick()
        end
       
        if data[1] == 1 then
            value = data
            --LoggerHelper.Error("FriendViewEx:ShowView(data)")
        end
    else
        
    end

    self._indexToGO[index]:SetActive(true)
    if self._indexToView[index] ~= nil then
        self._indexToView[index]:ShowView(data)
        self._curShowView = index
    end

    self:OnFriendApplyListRed()
end


function FriendViewEx:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.GetApplyListResq, self._friendApplyList)
    EventDispatcher:AddEventListener(GameEvents.RecommendDataList, self._recommondDataListFun)
    EventDispatcher:AddEventListener(GameEvents.OPEN_FRIEND_ATT, self._openAttFun)
end

function FriendViewEx:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.GetApplyListResq, self._friendApplyList)
    EventDispatcher:RemoveEventListener(GameEvents.RecommendDataList, self._recommondDataListFun)
    EventDispatcher:RemoveEventListener(GameEvents.OPEN_FRIEND_ATT, self._openAttFun)

end


--基于结构化View初始化方式，隐藏View使用Scale的方式处理
function FriendViewEx:ScaleToHide()
    return true
end

function FriendViewEx:OnRecommendDataList()
        local _recommondList = FriendManager:GetRecommondListData()
        self._indexToGO[FriendPanelType.Add]:SetActive(true)
        self._indexToView[FriendPanelType.Add]:ShowView(_recommondList)
        self._indexToView[FriendPanelType.Add]:OnRefreshRecommendList()
end






