--FriendChatView
-- require "Modules.ModuleFriend.ChildComponent.FriendListItem"
-- require "Modules.ModuleFriend.ChildView.ChatBoxView"
require "Modules.ModuleFriend.ChildComponent.FriendChatSecItem"
require "Modules.ModuleFriend.ChildComponent.FriendChatTypeItem"
require "Modules.ModuleFriend.ChildView.FriendChatContent"
require "Modules.ModuleFriend.ChildView.FriendChatBottomChat"


require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

local FriendChatView = Class.FriendChatView(ClassTypes.BaseLuaUIComponent)

FriendChatView.interface = GameConfig.ComponentsConfig.Component
FriendChatView.classPath = "Modules.ModuleFriend.ChildView.FriendChatView"

local PanelsConfig = GameConfig.PanelsConfig
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ChatManager = PlayerManager.ChatManager
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local FriendChatType = GameConfig.EnumType.FriendChatType
local FriendChatSecItem = ClassTypes.FriendChatSecItem
local FriendChatTypeItem = ClassTypes.FriendChatTypeItem
local FriendChatContent = ClassTypes.FriendChatContent
local FriendChatBottomChat = ClassTypes.FriendChatBottomChat
local UINavigationMenu = ClassTypes.UINavigationMenu
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendData = PlayerManager.PlayerDataManager.friendData
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
local TimerHeap = GameWorld.TimerHeap

function FriendChatView:Awake()
    -- FriendManager:GetData()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitViews()
    self:InitCallBack()

    --好友列表
    self._friendListResp = function()self:OnFriendListResp() end
    --黑名单列表
    self._blackListResp = function()self:OnBlackListResp() end

    --仇敌列表
    self._foeListResp = function()self:OnFoeListResp() end

    --列表人数变化
    self._perNumChange = function()self:OnPerNumChange() end

    self._sendFriendChatText = function(text,liststr)self:SendChatText(text,liststr) end

    --位置
    self._setFastPosChat = function(msg)self._friendChatBottomChat:SetFastPosChat(msg) end

    self._friendAddResp = function()self:OnFriendAddResp() end

    self._friendUpdateRed = function()self:OnFriendUpdateRed() end

    

    self._setFastChat = function(msg,liststr)self._friendChatBottomChat:SetFastChat(msg,liststr) end


    self._friendlyChange = function(friendDbid,value)self:OnFriendlyChange(friendDbid,value) end

end

function FriendChatView:ShowView(data)
    if self._scrollPage then
        self._scrollPage:SetScrollRectState(true)
    end


    if type(data) == "table" then
        if data.functionId then
            if data.args[2] then
                --LoggerHelper.Error("type(data) == ")
              
                
            end
        end
    end


    self._friendChatBottomChat:SetActive(false)
    self._friendChatContent:SetActive(false)
    self:AddEventListeners()
    self:SetTypeNavData()

    



end


function FriendChatView:InitGameFirst()
    self:InitFSelect()
    self._menu:SelectFItem(0)
end

function FriendChatView:InitFSelect()
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Recent - 1)
    self._selectedFItem:InitFSelect()
    
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Friend - 1)
    self._selectedFItem:InitFSelect()
    
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Foe - 1)
    self._selectedFItem:InitFSelect()

    self._selectedFItem = self._menu:GetFItem(FriendChatType.Black - 1)
    self._selectedFItem:InitFSelect()
end

function FriendChatView:CloseView()
    if self._scrollPage then
        self._scrollPage:SetScrollRectState(false)
    end

    if self._selectedSItem then
        self._selectedSItem:SetSelected(false)
    end

    self._friendChatContent:RemoveEvent()
    self:RemoveEventListeners()
    FriendData:ClearChatViewData()
end





function FriendChatView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.GetFriendlyResq, self._friendlyChange)

    EventDispatcher:AddEventListener(GameEvents.RefreshFriendList, self._friendListResp)
    

    EventDispatcher:AddEventListener(GameEvents.RefreshBlackList, self._blackListResp)
    

    EventDispatcher:AddEventListener(GameEvents.RefreshFoeList, self._foeListResp)
    

    EventDispatcher:AddEventListener(GameEvents.UpdateFriendChange, self._perNumChange)
    

    EventDispatcher:AddEventListener(GameEvents.FRIEND_SEND_CHAT_TEXT, self._sendFriendChatText)

    --声音
    EventDispatcher:AddEventListener(GameEvents.ConvertVoiceMessageFriend, self._sendFriendChatText)
    

    EventDispatcher:AddEventListener(GameEvents.FRIEND_ENTER_POSITION_CONTENT, self._setFastPosChat)


    EventDispatcher:AddEventListener(GameEvents.FriendAddFriendList, self._friendAddResp)
    

    EventDispatcher:AddEventListener(GameEvents.UpdateFriendListRed, self._friendUpdateRed)
    


    
    

    EventDispatcher:AddEventListener(GameEvents.FRIEND_CHAT_ENTER_EMOJI_CONTENT, self._setFastChat)
    



end


function FriendChatView:RemoveEventListeners()
    --好友列表
    EventDispatcher:RemoveEventListener(GameEvents.RefreshFriendList, self._friendListResp)
    --黑名单列表
    EventDispatcher:RemoveEventListener(GameEvents.RefreshBlackList, self._blackListResp)
    --仇敌列表
    EventDispatcher:RemoveEventListener(GameEvents.RefreshFoeList, self._foeListResp)
    
    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_SEND_CHAT_TEXT, self._sendFriendChatText)
    
    EventDispatcher:RemoveEventListener(GameEvents.FriendAddFriendList, self._friendAddResp)
    
    EventDispatcher:RemoveEventListener(GameEvents.UpdateFriendChange, self._perNumChange)
    
    EventDispatcher:RemoveEventListener(GameEvents.UpdateFriendListRed, self._friendUpdateRed)
    

    
    
    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_CHAT_ENTER_EMOJI_CONTENT, self._setFastChat)
    

    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self._friendListResp)

    --声音
    EventDispatcher:RemoveEventListener(GameEvents.ConvertVoiceMessageFriend, self._sendFriendChatText)

    --位置
    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_ENTER_POSITION_CONTENT, self._setFastPosChat)

    EventDispatcher:RemoveEventListener(GameEvents.GetFriendlyResq, self._friendlyChange)
end

function FriendChatView:OnFriendAddResp()
    self:RefreshFriendList()
end

function FriendChatView:OnFriendlyChange(friendDbid,value)
    --LoggerHelper.Error("FriendChatView:OnFriendlyChangeXXXX")
    local friendlistData = FriendManager:GetFriendListData()
    for i, v in pairs(self.demoNavigationData[FriendChatType.Friend]) do
        if v:GetDbid() then
            if tonumber(v:GetDbid()) == friendDbid then
                self._selectedData = self._menu:GetSecondLevelDataByIndex(FriendChatType.Friend,i)
                local item = self._menu:GetSItem(FriendChatType.Friend,i) 
                --LoggerHelper.Error("FriendChatView:OnFriendlyChange"..PrintTable:TableToStr(self._selectedData))
            end
        end
        -- self.demoNavigationData[FriendChatType.Friend][i + 1] = v
    end
end






function FriendChatView:OnFriendUpdateRed()
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Recent - 1)
    self._selectedFItem:UpdateRed()
    
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Friend - 1)
    self._selectedFItem:UpdateRed()
    
    self._selectedFItem = self._menu:GetFItem(FriendChatType.Foe - 1)
    self._selectedFItem:UpdateRed()
end

function FriendChatView:OnPerNumChange()
    self:UpdateNavData()
    self:RefreshDataForList()
end

function FriendChatView:OnBlackListResp(isSetData)
    --self:UpdateNavData()
    for i = 1, #self.demoNavigationData[FriendChatType.Black] do
        self.demoNavigationData[FriendChatType.Black][i + 1] = nil
    end
    
    
    self._listData = FriendManager:GetBlackListData()
    --LoggerHelper.Log("FriendChatView:OnBlackListResp()" .. #self._listData)
    for i, v in pairs(self._listData) do
        if v then
            self.demoNavigationData[FriendChatType.Black][i + 1] = v
        end
    end
    if self._menu then
        self._menu:SetDataList(self.demoNavigationData,3)
    end
end

function FriendChatView:RefreshDataForList()
    self._friendChatContent:SetActive(false)
    self._friendChatBottomChat:SetActive(false)


    for i = 1, #self.demoNavigationData[FriendChatType.Black] do
        self.demoNavigationData[FriendChatType.Black][i + 1] = nil
    end
    
    for i = 1, #self.demoNavigationData[FriendChatType.Foe] do
        self.demoNavigationData[FriendChatType.Foe][i + 1] = nil
    end
    
    for i = 1, #self.demoNavigationData[FriendChatType.Recent] do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = nil
    end
    
    for i = 1, #self.demoNavigationData[FriendChatType.Friend] do
        self.demoNavigationData[FriendChatType.Friend][i + 1] = nil
    end
    
    
    local blackListData = FriendManager:GetBlackListData()
    for i, v in pairs(blackListData) do
        if v then
            self.demoNavigationData[FriendChatType.Black][i + 1] = v
        end
    end
    
    local friendlistData = FriendManager:GetFriendListData()
    for i, v in pairs(friendlistData) do
        self.demoNavigationData[FriendChatType.Friend][i + 1] = v
    end
    
    
    local recentListData = FriendManager:GetFriendRecentListData()
    for i, v in pairs(recentListData) do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = v
    end
    
    
    local foeListData = FriendManager:GetFoeListData()
    for i, v in pairs(foeListData) do
        self.demoNavigationData[FriendChatType.Foe][i + 1] = v
    end
    
    if self._menu then
        self._menu:SetDataList(self.demoNavigationData,3)
    end
end


function FriendChatView:OnFoeListResp()
    for i = 1, #self.demoNavigationData[FriendChatType.Foe] do
        self.demoNavigationData[FriendChatType.Foe][i + 1] = nil
    end
    
    for i = 1, #self.demoNavigationData[FriendChatType.Recent] do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = nil
    end
    
    self._recentListData = FriendManager:GetFriendRecentListData()
    
    for i, v in pairs(self._recentListData) do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = v
    end
    
    
    self._listData = FriendManager:GetFoeListData()
    for i, v in pairs(self._listData) do
        self.demoNavigationData[FriendChatType.Foe][i + 1] = v
    end
    
    if self._menu then
        self._menu:SetDataList(self.demoNavigationData,3)
    end

end


function FriendChatView:OnFriendListResp()
    self:RefreshFriendList()
end


function FriendChatView:RefreshFriendList()
    for i = 1, #self.demoNavigationData[FriendChatType.Friend] do
        self.demoNavigationData[FriendChatType.Friend][i + 1] = nil
    end
    
    for i = 1, #self.demoNavigationData[FriendChatType.Recent] do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = nil
    end
    
    self._recentListData = FriendManager:GetFriendRecentListData()
    
    self._listData = FriendManager:GetFriendListData()
    
    for i, v in pairs(self._listData) do
        self.demoNavigationData[FriendChatType.Friend][i + 1] = v
    end
    
    for i, v in pairs(self._recentListData) do
        self.demoNavigationData[FriendChatType.Recent][i + 1] = v
    end
    

    if self._menu then
        self._menu:SetDataList(self.demoNavigationData,3)
    end

    
end


function FriendChatView:InitViews()
    
    self._btnGiveFriend = self:FindChildGO("Button_Give")
    self._csBH:AddClick(self._btnGiveFriend, function()self:OnGiveFriendClick() end)
    
    self._imgGirl = self:FindChildGO("Image_Girl")
    self._imgNPC = self:FindChildGO("Image_NPC")
    self._txtNPC = self:GetChildComponent("Image_NPC/Text_NPC", "TextMeshWrapper")
    
    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "NavigationMenu_Item")
    self._menu = menu
    self._scrollPage = scroll
    self._menu:SetFirstLevelItemType(FriendChatTypeItem)
    self._menu:SetSecondLevelItemType(FriendChatSecItem)
    self._menu:SetPadding(0, 0, 0, 0)
    self._menu:SetGap(3, 0)
    self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx)self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex)self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
    if self._scrollPage then
        self._scrollPage:SetScrollRectState(true)
    end

    self._friendChatBottomChat = self:AddChildLuaUIComponent('Container_BottomChat', FriendChatBottomChat)
    self._friendChatBottomChat:SetChatType(1)
    
    
    self._friendChatContent = self:AddChildLuaUIComponent('Container_Content', FriendChatContent)
    self._friendChatContent:SetActive(false)
    self._friendChatBottomChat:SetActive(false)
    
    self._imgGirl:SetActive(false)
    self._imgNPC:SetActive(false)
    if FriendData:GetCurClickIndex() == -1 then
        self._imgGirl:SetActive(true)
        self._imgNPC:SetActive(true)
        self._txtNPC.text = LanguageDataHelper.CreateContent(75636)
    end
    

end


function FriendChatView:UpdateNavData()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTable["1"] = FriendData:GetTotalNumForFriendList()
    
    local firstDesc = LanguageDataHelper.CreateContent(75662, argsTable)
    
    local argsTableFri = LanguageDataHelper.GetArgsTable()
    argsTableFri["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTableFri["1"] = FriendData:GetTotalNumForFriendList()
    local secDesc = LanguageDataHelper.CreateContent(75663, argsTableFri)
    
    local argsTableFoe = LanguageDataHelper.GetArgsTable()
    argsTableFoe["0"] = FriendManager:GetOnlineNumForFoeList()
    argsTableFoe["1"] = FriendManager:GetTotalNumForFoeList()
    local threeDesc = LanguageDataHelper.CreateContent(75664, argsTableFoe)
    
    local argsTableBlack = LanguageDataHelper.GetArgsTable()
    argsTableBlack["0"] = FriendManager:GetOnlineNumForBlackList()
    argsTableBlack["1"] = FriendManager:GetTotalNumForBlackList()
    local fourDesc = LanguageDataHelper.CreateContent(75665, argsTableBlack)
    
    self.demoNavigationData =
        {
            {{title = firstDesc}},
            {{title = secDesc}},
            {{title = threeDesc}},
            {{title = fourDesc}}
        }
    

    self._menu:SetDataList(self.demoNavigationData,3)
    self._selectedFIndex = -1
    self._selectedSItem = nil
    self._selectedFItem = nil
end

function FriendChatView:SetTypeNavData()
    
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTable["1"] = FriendData:GetTotalNumForFriendList()
    
    local firstDesc = LanguageDataHelper.CreateContent(75662, argsTable)
    
    local argsTableFri = LanguageDataHelper.GetArgsTable()
    argsTableFri["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTableFri["1"] = FriendData:GetTotalNumForFriendList()
    local secDesc = LanguageDataHelper.CreateContent(75663, argsTableFri)
    
    local argsTableFoe = LanguageDataHelper.GetArgsTable()
    argsTableFoe["0"] = FriendManager:GetOnlineNumForFoeList()
    argsTableFoe["1"] = FriendManager:GetTotalNumForFoeList()
    local threeDesc = LanguageDataHelper.CreateContent(75664, argsTableFoe)
    
    local argsTableBlack = LanguageDataHelper.GetArgsTable()
    argsTableBlack["0"] = FriendManager:GetOnlineNumForBlackList()
    argsTableBlack["1"] = FriendManager:GetTotalNumForBlackList()
    local fourDesc = LanguageDataHelper.CreateContent(75665, argsTableBlack)
    
    self.demoNavigationData =
        {
            {{title = firstDesc}},
            {{title = secDesc}},
            {{title = threeDesc}},
            {{title = fourDesc}}
        }
    

        for i = 1, #self.demoNavigationData[FriendChatType.Black] do
            self.demoNavigationData[FriendChatType.Black][i + 1] = nil
        end
        
        for i = 1, #self.demoNavigationData[FriendChatType.Foe] do
            self.demoNavigationData[FriendChatType.Foe][i + 1] = nil
        end
        
        for i = 1, #self.demoNavigationData[FriendChatType.Recent] do
            self.demoNavigationData[FriendChatType.Recent][i + 1] = nil
        end
        
        for i = 1, #self.demoNavigationData[FriendChatType.Friend] do
            self.demoNavigationData[FriendChatType.Friend][i + 1] = nil
        end
        
        
        local blackListData = FriendManager:GetBlackListData()
        for i, v in pairs(blackListData) do
            if v then
                self.demoNavigationData[FriendChatType.Black][i + 1] = v
            end
        end
        
        local friendlistData = FriendManager:GetFriendListData()
        for i, v in pairs(friendlistData) do
            self.demoNavigationData[FriendChatType.Friend][i + 1] = v
        end
        
        
        local recentListData = FriendManager:GetFriendRecentListData()
        for i, v in pairs(recentListData) do
            self.demoNavigationData[FriendChatType.Recent][i + 1] = v
        end
        
        
        local foeListData = FriendManager:GetFoeListData()
        for i, v in pairs(foeListData) do
            self.demoNavigationData[FriendChatType.Foe][i + 1] = v
        end
        

        if self._menu then
            self._menu:SetOnAllItemCreatedCB(function() self:InitGameFirst() end)
            self._menu:SetDataList(self.demoNavigationData,3)
        end
         self._selectedFIndex = -1
         self._selectedSItem = nil
         self._selectedFItem = nil
    --LoggerHelper.Log({"======== fridend demoNavigationData ", self.demoNavigationData})

end

function FriendChatView:UpdateFirstNum()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTable["1"] = FriendData:GetTotalNumForFriendList()
    
    local firstDesc = LanguageDataHelper.CreateContent(75662, argsTable)
    
    local argsTableFri = LanguageDataHelper.GetArgsTable()
    argsTableFri["0"] = FriendManager:GetOnlineNumForFriendList()
    argsTableFri["1"] = FriendData:GetTotalNumForFriendList()
    local secDesc = LanguageDataHelper.CreateContent(75663, argsTableFri)
    
    local argsTableFoe = LanguageDataHelper.GetArgsTable()
    argsTableFoe["0"] = FriendManager:GetOnlineNumForFoeList()
    argsTableFoe["1"] = FriendManager:GetTotalNumForFoeList()
    local threeDesc = LanguageDataHelper.CreateContent(75664, argsTableFoe)
    
    local argsTableBlack = LanguageDataHelper.GetArgsTable()
    argsTableBlack["0"] = FriendManager:GetOnlineNumForBlackList()
    argsTableBlack["1"] = FriendManager:GetTotalNumForBlackList()
    local fourDesc = LanguageDataHelper.CreateContent(75665, argsTableBlack)
    
    self.demoNavigationData =
        {
            {{title = firstDesc}},
            {{title = secDesc}},
            {{title = threeDesc}},
            {{title = fourDesc}}
        }
    self._menu:SetDataList(self.demoNavigationData,3)
    self._selectedFIndex = -1
    self._selectedSItem = nil
    self._selectedFItem = nil
--self._menu:SetDataList(self.demoNavigationData)
end


function FriendChatView:InitCallBack()
    self._updateCountCB = function()
        self:UpdateSelectSItemCount()
    end
end

--更新二级菜单选中的item
function FriendChatView:UpdateSelectSItemCount()
    if self._selectedSItem then
        self._selectedSItem:UpdateCount()
        --LoggerHelper.Error("UpdateCount")
    end
end


function FriendChatView:OnMenuFItemClicked(idx)
    if self._selectedFItem then
        self._selectedFItem:SetSelected(false)


        if self._selectedFIndex and self._selectedFIndex == idx then
            -- if self._menu then
            --     self:InitFSelect()
            -- end
            --return
        else
            if self._menu then
                self:InitFSelect()
            end
        end
    end
    
    --LoggerHelper.Error("FriendChatView:OnMenuFItemClicked:" .. idx)
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFIndex = idx
    self._selectedFItem:ToggleSelected()
    self._selectedFItem:SetGetSelect()
    self:SetChatShow(idx)
--LoggerHelper.Log("self._selectedFItem:SetGetSelect():" ..idx.."kaiqi".. self._selectedFItem:GetSelected())
end



function FriendChatView:SetChatShow(idx)
    self._friendChatContent:SetActive(false)
    self._friendChatBottomChat:SetActive(false)
    FriendData:SetCurClickIndex(idx)
    if FriendData:GetCurClickIndex() == -1 then
        self._imgGirl:SetActive(true)
        self._imgNPC:SetActive(true)
        self._txtNPC.text = LanguageDataHelper.CreateContent(75636)
    end
    
    if FriendData:GetCurClickIndex() == FriendChatType.Recent then
        if FriendData:GetTotalNumForFriendList() == 0 then
            self._imgGirl:SetActive(true)
            self._imgNPC:SetActive(true)
            self._txtNPC.text = LanguageDataHelper.CreateContent(75637)
        elseif FriendData:GetTotalNumForFriendList() > 0 then
            self._imgGirl:SetActive(false)
            self._imgNPC:SetActive(false)
            --LoggerHelper.Log("FriendData:GetTotalNumForFriendList()q"..PrintTable:TableToStr(FriendData:GetFriendListData()))
            --LoggerHelper.Error("FriendChatView:11:" .. idx)
            if self._selectedFItem then
                --LoggerHelper.Error("FriendChatView:OnMenuFItemClicked22:" .. idx)
                if self._selectedFItem:GetSelected() == 1 then
                    --LoggerHelper.Error("FriendChatView:OnMenuFItemClicked66:" .. idx)
                    self:OnMenuSItemClicked(idx, 0)
                else
                    
                end
            else
                
            end
        
        end
    end
    
    if FriendData:GetCurClickIndex() == FriendChatType.Friend then
        if FriendData:GetTotalNumForFriendList() == 0 then
            self._imgGirl:SetActive(true)
            self._imgNPC:SetActive(true)
            self._txtNPC.text = LanguageDataHelper.CreateContent(75637)
        elseif FriendData:GetTotalNumForFriendList() > 0 then
            self._imgGirl:SetActive(false)
            self._imgNPC:SetActive(false)
            --LoggerHelper.Log("FriendData:GetTotalNumForFriendList()w"..PrintTable:TableToStr(FriendData:GetFriendListData()))
            if self._selectedFItem then
                if self._selectedFItem:GetSelected() == 1 then
                    self:OnMenuSItemClicked(idx, 0)
                else
                    
                end
            else
                
            end
        end
    end
    
    if FriendData:GetCurClickIndex() == FriendChatType.Foe then
        if FriendData:GetTotalNumForFoeList() == 0 then
            self._imgNPC:SetActive(true)
            self._imgGirl:SetActive(true)
            self._txtNPC.text = LanguageDataHelper.CreateContent(75638)
        elseif FriendData:GetTotalNumForFoeList() > 0 then
            self._imgNPC:SetActive(false)
            self._imgGirl:SetActive(false)
            --LoggerHelper.Log("FriendData:GetTotalNumForFriendList()z"..PrintTable:TableToStr(FriendData:GetFriendListData()))
            if self._selectedFItem then
                if self._selectedFItem:GetSelected() == 1 then
                    self:OnMenuSItemClicked(idx, 0)
                else
                    
                end
            else
                
                end
        end
    end
    
    if FriendData:GetCurClickIndex() == FriendChatType.Black then
        self._imgNPC:SetActive(true)
        self._imgGirl:SetActive(true)
        self._txtNPC.text = LanguageDataHelper.CreateContent(75639)
    end
end

function FriendChatView:OnMenuSItemClicked(fIndex, sIndex)
    if self._selectedSItem then
        self._selectedSItem:SetSelected(false)
    end

    local secondLength = self._menu:GetSecondLevelLength(fIndex)
    --LoggerHelper.Error("secondLength:"..secondLength)
    if secondLength > 0 then
        for i=1,secondLength do
            --LoggerHelper.Error("secondLength:"..i)
            self._selectedSItem = self._menu:GetSItem(fIndex, secondLength-1)
            self._selectedSItem:SetSelected(false)
        end
    end
    



    self._selectedComposeData = self._menu:GetSecondLevelDataByIndex(fIndex, sIndex)
    FriendData:SetCurChatPersonData(self._selectedComposeData)
    if FriendData:GetCurClickIndex() == FriendChatType.Black then
        self._friendChatContent:SetActive(false)
        self._friendChatBottomChat:SetActive(false)
    else
        self._friendChatContent:SetActive(true)
        self._friendChatContent:InitEvent()
        self._friendChatBottomChat:SetActive(true)
        self._friendChatContent:ScrollViewShow(self._selectedComposeData:GetDbid())
        FriendManager:SendNewChatListReq(self._selectedComposeData:GetDbid())
        self._friendChatBottomChat:SetData(self._selectedComposeData)
    end
    --LoggerHelper.Error("FriendChatView:OnMenuSItemClicked:")

    self._selectedSItem = self._menu:GetSItem(fIndex, sIndex)
    self._selectedSItem:SetSelected(true)
-- self:UpdateComposeInfo()
end



--送花
function FriendChatView:OnGiveFriendClick()
    GUIManager.ClosePanel(PanelsConfig.Friend)
    GUIManager.ShowPanel(PanelsConfig.Flower, {_index = 1, _data = nil})
end

--聊天类型
function FriendChatView:OnChatFriendClick()

end

--发语音
function FriendChatView:OnVoiceFriendClick()

end

--发送
function FriendChatView:OnSendFriendClick()

end


function FriendChatView:SendChatText(text,liststr)

    local chatValue = text
    if chatValue ~= nil then

        if liststr then
            FriendManager:SendChatReq(self._selectedComposeData, chatValue, '',liststr)
        else
            FriendManager:SendChatReq(self._selectedComposeData, chatValue, '','')
        end
        --LoggerHelper.Log("自己的聊天内容直接保存到本地"..chatValue)
        
        
    -- 自己的聊天内容直接保存到本地
    --ChatManager.AddSelfChatInfos(self._friendData:GetUUID(),chatValue,0,'')
    end
    --LoggerHelper.Error("FriendChatView:SendChatText"..liststr)
    GameWorld.LoggerHelper.Log("发送内容：" .. chatValue)

end


function FriendChatView:SetOnLine()
    self._onLineFriendList = true
    self._onLineRecentList = true
    self._onLineTeamList = true
end
