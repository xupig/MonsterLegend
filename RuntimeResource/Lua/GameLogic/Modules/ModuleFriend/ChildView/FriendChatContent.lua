--FriendChatContent
local FriendChatContent = Class.FriendChatContent(ClassTypes.BaseLuaUIComponent)

FriendChatContent.interface = GameConfig.ComponentsConfig.Component
FriendChatContent.classPath = "Modules.ModuleFriend.ChildView.FriendChatContent"

require "Modules.ModuleFriend.ChildComponent.FriendChatContentItem"

local FriendChatContentItem = ClassTypes.FriendChatContentItem

local ChatManager = PlayerManager.ChatManager
local FriendManager = PlayerManager.FriendManager
local FriendData = PlayerManager.PlayerDataManager.friendData

local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList

local public_config = GameWorld.public_config
--对应public_config里频道类型的映射
local chatReflect = {}

function FriendChatContent:Awake()
    self:InitChannelRelf()
    
    self._curIndex = 1
    
    self:InitVar()
    
    self:InitComps()
    self.updateChat =  function()self:UpdateChatView() end
    self:InitEvent()
  
end

function FriendChatContent:InitChannelRelf()
end

function FriendChatContent:OnDestroy()
    self:RemoveEvent()
end

function FriendChatContent:InitEvent()
   
    --EventDispatcher:AddEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self.updateChat)
    EventDispatcher:AddEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self.updateChat)
    --LoggerHelper.Error(" FriendChatContent:InitEvent()")
end

function FriendChatContent:RemoveEvent()
    --EventDispatcher:RemoveEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO,  self.updateChat)
    EventDispatcher:RemoveEventListener(GameEvents.FRIEND_PERSON_CHAT_INFO, self.updateChat)
end

function FriendChatContent:OnClose()

end


function FriendChatContent:UpdateChatView()
    --self:UpdateScrollViewNum()
    --LoggerHelper.Error("FriendChatContent:UpdateChatView")
    local listData = {}
    local curChatPerson = FriendData:GetCurChatPersonData()
    if curChatPerson then
        local listTip = FriendData:GetTipChatListData(curChatPerson:GetDbid())
        FriendData:ClearTipChatListData(curChatPerson:GetDbid())
        --LoggerHelper.Log("FriendChatContent:UpdateChatViewTip" .. PrintTable:TableToStr(listTip))

        if listTip then
        --     if not table.isEmpty(listTip) then
        --         if table.isEmpty(listData) then
        --             self._scrollList:RemoveAll()
        --             FriendData:ClearChatDataByDbid(curChatPerson:GetDbid())
        --             listData = FriendData:GetChatListData()
        --             table.insert(listData, 1, listTip)
        --         else
        --             --LoggerHelper.Log("FriendChatContent:listData[1]" .. PrintTable:TableToStr(listData))
        --             if listData[1] and listData[1]._type ~= 2 then
        --                 --LoggerHelper.Log("FriendChatContent:listData[1]" .. PrintTable:TableToStr(listData))
        --                 self._scrollList:RemoveAll()
        --                 FriendData:ClearChatDataByDbid(curChatPerson:GetDbid())
        --                 listData = FriendData:GetChatListData()
        --                 table.insert(listData, 1, listTip)
        --             else
        --                 listData = FriendData:GetChatListData()
        --             end
        --         end
        --     else
        --         listData = FriendData:GetChatListData()
        --     end
        -- else
        --     listData = FriendData:GetChatListData()
        end


        listData = FriendData:GetChatListData()
    
        if table.isEmpty(listData) then
        else
            --LoggerHelper.Log("FriendChatContent:UpdateChatView" .. PrintTable:TableToStr(listData))
            local addDataList = {}
            for k, v in pairs(listData) do
                if v.show == false then
                    v.show = true
                    table.insert(addDataList, v)
                end
            end
            
            FriendData:SetChatListData(listData)
            local dbid = FriendData:SetDbidByChatList(listData)
            local chatListData = FriendData:GetChatListDataByDbid(dbid)
            if chatListData then
                self._scrollList:RemoveAll()
                --LoggerHelper.Error("FriendChatContent:chatListData")
                self._scrollList:AppendItemDataList(chatListData)
            end
        end
    
        self:OnGotoTail()
    end
    




end

function FriendChatContent:UpdateScrollViewNum()
    local friendList = FriendManager.GetFriendListData()
    self._contentNum = #friendList
    self:InitScrollViewList()
end

function FriendChatContent:InitVar()
    
    self._contentNum = 10
    self._contentGoList = {}
    self._scrollList = {}
    self._viewList = {}
-- self._updateIteming = false
-- self._needUpdateItem = false
-- self._contentNumList = {}
end

function FriendChatContent:InitComps()
    self:InitScrollViewList()
    --LoggerHelper.Log("FriendChatContent:UpdateChatView22")
end

function FriendChatContent:ChangeContent()

end

function FriendChatContent:UpdateContent(channel_id)

end

function FriendChatContent:InitScrollViewList()
    
    
    local friendList = FriendManager.GetFriendListData()
    if friendList then
        --self._contentNum = #friendList
    end
    
    self._contentGoList[1] = self:FindChildGO("ScrollViewList")
    
    local scrollView, list = UIComplexList.AddScrollViewComplexList(self._contentGoList[1])
    self._scrollList = list
    self._viewList = scrollView
    self._scrollList:SetItemType(FriendChatContentItem)
    self._scrollList:SetPadding(40, 0, 0, 0)
    self._scrollList:SetGap(20, 20)
    self._scrollList:SetDirection(UIList.DirectionTopToDown, 1, -1)
    self._scrollList:AppendItemDataList({})
    self._scrollList:UseObjectPool(true)
    local itemClickedCB =
        function(idx)
            self:OnListItemClicked(idx)
        end
    --self._scrollList:SetItemClickedCB(itemClickedCB)
    self._viewList:SetOnMoveEndCB(function()self:DifficultScrollEnd() end)

end


function FriendChatContent:ScrollViewShow(dbid)
    -- for i = 1, self._contentNum do
    --     self._scrollList[i]:SetActive(false)
    -- end
    if dbid then
        self._scrollList:RemoveAll()
        local chatListData = FriendData:GetChatListDataByDbid(dbid)
        --local index = FriendData:GetFriendListIndex(listData)
        if chatListData then
            self._scrollList:AppendItemDataList(chatListData)
        end
        self._scrollList:SetActive(true)
    end
end


function FriendChatContent:ScrollViewHide()
    self._scrollList:RemoveAll()
    self._scrollList:SetActive(false)
end



-- 滑动到最后
function FriendChatContent:OnGotoTail()
    if not (self._scrollList:InBottom()) then
        self._scrollList:MoveToBottom()
    -- EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
    end
end

function FriendChatContent:OnListItemClicked(idx)
--LoggerHelper.Log("OnListItemClicked:"..idx)
end


function FriendChatContent:UpdateListView()
-- self:UpdateListViewData()
-- self:OnGotoTail()
end

-- 更新LIST数据
function FriendChatContent:UpdateListViewData()
-- if self._updateIteming then --是否在更新中
--     self._needUpdateItem = true
--     return
-- end
-- --在更新ing
-- self._updateIteming = true
-- --取要更新的数据
-- local listData = ChatManager.GetFriendChatContents()
-- local addDataList = {}
-- for k, v in pairs(listData) do
--     if v.show == false then
--         v.show = true
--         table.insert(addDataList, v)
--     end
-- end
-- local updateChannel = self._curIndex
-- if self._contentNumList[updateChannel] == nil then
--     self._contentNumList[updateChannel] = 0
-- end
-- -- LoggerHelper.Log('updateChannel:' .. updateChannel)
-- -- LoggerHelper.Log(listData)
-- --把多的ITEM删除
-- local curCount = self._contentNumList[updateChannel]-- self._scrollList[updateChange]:GetCurCount()
-- local addCount = #addDataList
-- if addCount > 0 then
--     local allCount = curCount + addCount
--     -- LoggerHelper.Error(updateChannel .. 'curCount:' .. curCount)
--     local removeCount = allCount - ChatManager.MAX_CHAT_NUM
--     -- LoggerHelper.Error('allCount:' .. allCount)
--     if removeCount > 0 then
--         self._contentNumList[updateChannel] = self._contentNumList[updateChannel] - removeCount
--         self._scrollList[updateChannel]:RemoveItemRange(0, removeCount, false)
--     end
--     --把新ITEM加进去
--     self._contentNumList[updateChannel] = self._contentNumList[updateChannel] + #addDataList
--     self._scrollList[updateChannel]:AppendItemDataList(addDataList)
-- end
-- --更新完成
-- self._updateIteming = false
-- --在更新过程是否有新数据来
-- if self._needUpdateItem then
--     self._needUpdateItem = false
--     self:UpdateListViewData()
-- end
end

-- 滚动到最后了
function FriendChatContent:DifficultScrollEnd()
-- if self._scrollList[self._curIndex]:InBottom() then
--     EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
-- end
end
