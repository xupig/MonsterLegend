--FriendChatBottomChat
local FriendChatBottomChat = Class.FriendChatBottomChat(ClassTypes.BaseLuaUIComponent)

FriendChatBottomChat.interface = GameConfig.ComponentsConfig.Component
FriendChatBottomChat.classPath = "Modules.ModuleFriend.ChildView.FriendChatBottomChat"

require "Modules.ModuleFriend.ChildView.FriendVoiceButtonView"

local FriendVoiceButtonView = ClassTypes.FriendVoiceButtonView

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local TimerHeap = GameWorld.TimerHeap
local ChatDataHelper = GameDataHelper.ChatDataHelper
local ChatManager = PlayerManager.ChatManager
local ItemDataHelper = GameDataHelper.ItemDataHelper
local public_config = GameWorld.public_config

function FriendChatBottomChat:Awake()
    self:InitComps()
    self:InitEvent()
end

function FriendChatBottomChat:OnDestroy()
end

function FriendChatBottomChat:InitComps()
    self._buttonVoice = self:FindChildGO("Button_Voice")
    -- self._csBH:AddClick(self._buttonVoice, function() self:OnClickVoice() end )
    self._buttonVoiceView = self:AddChildLuaUIComponent('Button_Voice', FriendVoiceButtonView)
    
    self._buttonEmoji = self:FindChildGO("Button_Chat")
    self._csBH:AddClick(self._buttonEmoji, function()self:OnClickEmoji() end)
    
    self._buttonSend = self:FindChildGO("Button_Send")
    self._csBH:AddClick(self._buttonSend, function()self:OnClickSend() end)
    
    self._inputText = self:GetChildComponent("InputFriend", 'InputFieldMeshWrapper')
    self._inputText.placeholder.text = ""--LanguageDataHelper.CreateContent(124)
    -- 最多输入100个字符
    self._inputText.characterLimit = 45
    
    self._chatType = 1
    
    
    -- if GameLoader.SystemConfig.ForBanShu then
    --     self._buttonVoice:SetActive(false)
    -- end
end

function FriendChatBottomChat:InitEvent()

end

function FriendChatBottomChat:SetData(data)

    if data == nil then
        return
    end
    self._friendData = data

    -- self._name = self._friendData:GetName()

    -- --现阶段永恒的uuid一律使用dbid
    -- self._uuid = self._friendData:GetUUID()

    -- --如果不是自己的好友,隐藏删除按钮
    -- if FriendManager:IsMyFriend(self._uuid) then
    --     self._deleteButton:SetActive(true)
    --     self._fightButton:SetActive(true)
    -- else
    --     self._deleteButton:SetActive(false)
    --     self._fightButton:SetActive(false)
    -- end

    -- self._playerHead:SetPlayerHeadDataByTable(self._friendData)
    -- self._chatFriendView:SetFriend(data)

    -- -- 保存聊天数据
    -- ChatManager.SaveFriendChatData()
end


function FriendChatBottomChat:OnClickVoice()

end
-- 1世界聊天 2 快捷聊天 3 好友聊天
function FriendChatBottomChat:SetChatType(type)
    self._chatType = type
end

function FriendChatBottomChat:OnClickEmoji()
    GUIManager.ShowPanel(PanelsConfig.ChatEmoji,3)
end

function FriendChatBottomChat:OnClickSend()
    --LoggerHelper.Log("FriendChatBottomChat:OnClickSend========================================")
    local textStr = self._inputText.text
    if textStr == '' then
        return
    end 
    textStr = self:HandleEmoji(textStr)



    local liststr =  self._liststr
    local serData = ChatManager:GetToSerData()
    --LoggerHelper.Error("serData"..PrintTable:TableToStr(serData))
    for i=#serData,1,-1 do
        local itemName = ItemDataHelper.GetItemName(tonumber(serData[i][1][public_config.ITEM_KEY_ID]))
        local itemNameText = '['..itemName..']'
        local start_i = 0;
        local end_j = 0;
        local sunstr = "";
        start_i,end_j,sunstr=string.find(textStr,itemNameText,1,-2);

        local str = ""
        local str2 = ""
        if start_i then
            if start_i == 1 then
                str = ""
            else
                str = string.sub(textStr,1,start_i-1)
            end

            if end_j == string.len(textStr) then
                str2 = ""
            else
                str2 = string.sub(textStr,end_j+1,string.len(textStr))
            end
        
            textStr = str.."&&"..i.."&&"..str2
        end
    end


    --LoggerHelper.Log("FriendChatBottomChat:OnClickSend========================================"..liststr)
    EventDispatcher:TriggerEvent(GameEvents.FRIEND_SEND_CHAT_TEXT, textStr,liststr)
    self._inputText.text = ''
    --EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
end

function FriendChatBottomChat:HandleEmoji(textStr)
    -- local textList = string.split(textStr, '%[') or {}
    -- local newText = ''
    -- for key, itemText in pairs(textList) do
    --     LoggerHelper.Log(itemText)
    --     local contentList = itemText.split(itemText, '%[') or {}
    --     for itemKey, contentText in pairs(contentList) do
    --         local num = tonumber(contentText)
    --         if num ~= nil then
    --             contentText = self:EmojiText(num)
    --         else
    --             contentText = '[' .. contentText .. ']'
    --         end
    --         newText = newText .. contentText
    --     end
    -- end
    textStr = ChatManager.HandleEmoji(textStr)
    -- LoggerHelper.Log('textStr:' .. textStr)
    -- LoggerHelper.Log('textStr:' .. textStr)
    return textStr
end

function FriendChatBottomChat:EmojiText(index)
    local emojiData = ChatDataHelper.GetEmojiData(index)
    if emojiData == nil then
        return '#' .. index
    end
    local animationData = ChatDataHelper.GetEmojiAnimationData(emojiData.content)
    if animationData == nil then
        return '#' .. index
    end
    local sendText = ''
    if animationData.frameRate > 0 then
        sendText = '<size=28>'..'<sprite anim="' .. animationData.startIndex .. ',' .. animationData.endIndex .. ',' .. animationData.frameRate .. '">'..'</size>'
    else
        sendText = '<size=28>'..'<sprite=' .. animationData.startIndex .. '>'..'</size>'
    end
    return sendText
end

function FriendChatBottomChat:SetFastChat(text,liststr)
    -- LoggerHelper.Error('FriendChatBottomChat:SetFastChat:' .. text)
    local textStr = self._inputText.text
    textStr = textStr .. text
    self._inputText.text = textStr

    --LoggerHelper.Error("FriendChatBottomChat:SetFastChat22"..liststr)
    self._liststr = liststr
-- self._inputText:MoveTextEnd(false)
-- self._inputText.selectionFocusPosition = 3
-- self._inputText:ActivateInputField()
-- self._inputText.caretPosition = #textStr
-- self._inputText:ActivateInputField()
-- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)
end

function FriendChatBottomChat:SetFastPosChat(text)
        -- LoggerHelper.Error('ChatBottomChat:SetFastChat:' .. text)
        local textStr = self._inputText.text
        textStr = textStr .. text
        self._inputText.text = textStr
    
        local textStr = self._inputText.text
        if textStr == '' then
            return
        end
        
        --textStr = ChatManager.HandleEmoji(textStr)
        EventDispatcher:TriggerEvent(GameEvents.FRIEND_SEND_CHAT_TEXT, textStr)
        self._inputText.text = ''
        --EventDispatcher:TriggerEvent(GameEvents.CHAT_SLIDE_END)
    -- self._inputText:MoveTextEnd(false)
    -- self._inputText.selectionFocusPosition = 3
    -- self._inputText:ActivateInputField()
    -- self._inputText.caretPosition = #textStr
    -- self._inputText:ActivateInputField()
    -- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)
end




-- <sprite=\"EmojiOne\" anim=\"0, 12, 12\"> 表情
function FriendChatBottomChat:OnFocusInputFiled()
-- self._inputText:ActivateInputField()
-- TimerHeap:AddSecTimer(0.01, 0, 1, function() self._inputText:MoveTextEnd(false) end)
end
