--FriendAttView
-- require "Modules.ModuleFriend.ChildComponent.FriendListItem"
-- require "Modules.ModuleFriend.ChildView.ChatBoxView"
require "Modules.ModuleFriend.ChildComponent.FriendFriendlyItem"
require "Modules.ModuleFriend.ChildComponent.FriendFriendlyDescItem"
local FriendAttView = Class.FriendAttView(ClassTypes.BaseLuaUIComponent)
FriendAttView.interface = GameConfig.ComponentsConfig.Component
FriendAttView.classPath = "Modules.ModuleFriend.ChildView.FriendAttView"

local PanelsConfig = GameConfig.PanelsConfig
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendIntimacyDataHelper = GameDataHelper.FriendIntimacyDataHelper
local FriendFriendlyItem = ClassTypes.FriendFriendlyItem
local FriendFriendlyDescItem = ClassTypes.FriendFriendlyDescItem
-- local UIToggleGroup = ClassTypes.UIToggleGroup
local UIList = ClassTypes.UIList
local FriendManager = PlayerManager.FriendManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType




function FriendAttView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitViews()
    -- self:InitPostionTable()
    self:InitScrollViewList()


end

function FriendAttView:ShowView(data)

  
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = data:GetLevel()
    self._level.text = LanguageDataHelper.CreateContent(75623, argsTable)
    self._nameText.text = data:GetName() 
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = data:GetFriendlyVal()
    self._friendlyText.text = LanguageDataHelper.CreateContent(75624, argsTable)


    local ids = FriendIntimacyDataHelper:GetAllId()
    local argsTable = LanguageDataHelper.GetArgsTable()
    for i,id in ipairs(ids) do
        if i<=#ids-1 then
            local preNum = FriendIntimacyDataHelper:GetNum(ids[i])
            local curNum = FriendIntimacyDataHelper:GetNum(ids[i+1])
            if data:GetFriendlyVal() >= preNum and data:GetFriendlyVal() < curNum then 
                local AttDescList = FriendIntimacyDataHelper:GetAttri(ids[i])
                --LoggerHelper.Error("AttDescList"..PrintTable:TableToStr(AttDescList))
                argsTable["0"] = AttDescList[1][2]
                argsTable["1"] = AttDescList[2][2]
                if  AttDescList[3]~=nil then
                    argsTable["2"] = AttDescList[3][2]
                else
                    argsTable["2"] = 0
                end
             
                self._attDesText.text = LanguageDataHelper.CreateContent(75622, argsTable)           
                return
            end
        elseif i == #ids then
            local attNum = FriendIntimacyDataHelper:GetNum(ids[i])
            if data:GetFriendlyVal() >= attNum then 
                local AttDescList = FriendIntimacyDataHelper:GetAttri(ids[i])
                --LoggerHelper.Error("AttDescList"..PrintTable:TableToStr(AttDescList))
                argsTable["0"] = AttDescList[1][2]
                argsTable["1"] = AttDescList[2][2]
                if  AttDescList[3]~=nil then
                    argsTable["2"] = AttDescList[3][2]
                else
                    argsTable["2"] = 0
                end
                self._attDesText.text = LanguageDataHelper.CreateContent(75622, argsTable)      
                return
            end            
        end
    end



-- self:RefreshView()
-- GameWorld.ShowPlayer():ShowModel(1)
-- self._modelComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)
end

function FriendAttView:CloseView()

end

function FriendAttView:OnDestroy()
end

function FriendAttView:OnEnable()
-- self:AddEventListeners()
-- self:RefreshNum()
-- FriendManager:SetSendTip(false)
end

function FriendAttView:OnDisable()
-- self:RemoveEventListeners()
-- FriendManager:SetSendTip(true)
end

------------------------------------------------各种打开模式---------------------------------------------------------------
--每次打开进入好友系统时
-- function FriendAttView:OnEnterFriendOpen()
--     --打开好友系统-默认选中好友列表-第一个好友
--     self._barIndex = 0
--     self._uuid = nil
-- end
--切换到好友时打开[好友、添加、空间之间切换时]
function FriendAttView:OnFunctionBarOpen()

--self:OnListBarClickByUUID(self._barIndex,self._uuid)
--self._friendToggleGroup:SetSelectIndex(self._barIndex)
end

--在Tip里点发消息时打开
-- function FriendAttView:OnSendMsgOpen(uuid)
--     self._barIndex = 1
--     self._uuid = uuid
-- end
function FriendAttView:SetBarIndex(barIndex, uuid)
-- self._barIndex = barIndex
-- self._uuid = uuid
end

------------------------------------------------初始化各种数据---------------------------------------------------------
function FriendAttView:AddEventListeners()

end

function FriendAttView:RemoveEventListeners()


end


function FriendAttView:InitViews()

    self._nameText = self:GetChildComponent("Container_Left/Text_Name", "TextMeshWrapper")
    self._level = self:GetChildComponent("Container_Left/Text_Level", "TextMeshWrapper")
    self._friendlyText = self:GetChildComponent("Container_Left/Text_Friendly", "TextMeshWrapper")
    self._attDesText = self:GetChildComponent("Container_Left/Text_AttDes", "TextMeshWrapper")

-- self._btnAddFriend = self:FindChildGO("Button_Add")
-- self._csBH:AddClick(self._btnAddFriend,function () self:OnAddFriendClick() end)
-- self._worldLevelButton = self:FindChildGO("Container_AttriShow/Button_WorldLevel")
-- self._csBH:AddClick(self._worldLevelButton,function () self:OnWorldLevelButtonClick() end)
-- self._clearButton = self:FindChildGO("Container_AttriShow/Button_Clear")
-- self._csBH:AddClick(self._clearButton,function () self:OnClearButtonClick() end)
-- self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
-- self._fpNumber:SetNumber(0)

-- self._nameText.text = GameWorld.Player().name
-- self._chatBoxView = self:AddChildLuaUIComponent("Container_ChatBox", ChatBoxView)
-- self._chatBoxView:SetActive(false)
--self._friendToggleGroup = UIToggleGroup.AddToggleGroup(self.gameObject, "ToggleGroup_Friend")
--self._friendToggleGroup:SetOnSelectedIndexChangedCB(function(index)self:OnListBarClick(index) end)
--左边Toggle列表位置
-- self._friendToggleRects = {}
-- --self._friendNumText = {}
-- self._signRect = {}
-- for i=0,2 do
--     local index = i+1
--     self._friendToggleRects[i] = self:GetChildComponent("ToggleGroup_Friend/Toggle"..index, "RectTransform")
--     --self._friendNumText[i] = self:GetChildComponent("ToggleGroup_Friend/Toggle"..index.."/Text_Num", "TextMeshWrapper")
--     self._signRect[i] = self:GetChildComponent("ToggleGroup_Friend/Toggle"..index.."/Image_Sign", "RectTransform")
-- end
-- self._friendListRect = self:GetChildComponent("ScrollViewList_Friend","RectTransform")
end



--加好友
function FriendAttView:OnAddFriendClick()

end

--初始化位置表，储存各种点击状态的位置
function FriendAttView:InitPostionTable()
-- local ToggleHeight =  Vector3.New(0,50,0)
-- local ListHeight = Vector3.New(0,390,0)
-- local listPos = self._friendListRect.localPosition
-- local toggle1Pos = self._friendToggleRects[0].localPosition
-- self.postionTable = {}
-- self.postionTable[0]={[0]= listPos,[1] = toggle1Pos - ListHeight - ToggleHeight,[2] = toggle1Pos - ListHeight - ToggleHeight - ToggleHeight}
-- self.postionTable[1]={[0]= listPos - ToggleHeight,[1] = toggle1Pos - ToggleHeight,[2] = toggle1Pos - ListHeight - ToggleHeight - ToggleHeight}
-- self.postionTable[2]={[0]= listPos - ToggleHeight - ToggleHeight,[1] = toggle1Pos - ToggleHeight,[2] = toggle1Pos - ToggleHeight - ToggleHeight}
end


function FriendAttView:InitScrollViewList()
    -- local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Friend")
    -- self._list = list
    -- self._scrollView = scrollView
    -- self._scrollView:SetHorizontalMove(false)
    -- self._scrollView:SetVerticalMove(true)
    -- self._list:SetItemType(FriendListItem)
    -- self._list:SetPadding(0, 0, 0, 11);
    -- self._list:SetGap(4, 4);
    -- self._list:SetDirection(UIList.DirectionTopToDown, 1, -1);
    -- local itemClickedCB = function(idx) self:OnListItemClicked(idx) end
    -- self._list:SetItemClickedCB(itemClickedCB)
    --亲密度
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Friendly/ScrollViewList")
    self._friendlyList = list
    self._friendlyListScrollView = scrollView
    self._friendlyListScrollView:SetHorizontalMove(false)
    self._friendlyListScrollView:SetVerticalMove(true)
    self._friendlyList:SetItemType(FriendFriendlyItem)
    self._friendlyList:SetPadding(0, 0, 0, 0)
    self._friendlyList:SetGap(0, 0)
    self._friendlyList:SetDirection(UIList.DirectionLeftToRight, 3, 100)

    local dataList = FriendIntimacyDataHelper:GetAllId()
    self._friendlyList:SetDataList(dataList)
    

    --获得属性
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_AttDes/ScrollViewList")
    self._friendAttList = list
    self._friendAttListScrollView = scrollView
    self._friendAttListScrollView:SetHorizontalMove(false)
    self._friendAttListScrollView:SetVerticalMove(true)
    self._friendAttList:SetItemType(FriendFriendlyDescItem)
    self._friendAttList:SetPadding(20, 20, 0, 20)
    self._friendAttList:SetGap(0, 0)
    self._friendAttList:SetDirection(UIList.DirectionTopToDown, 1, 100)


    local list = FriendIntimacyDataHelper:GetAllId()
    self._friendAttList:SetDataList(list)
    -- local data = {"1"={},"2"={}}
-- local itemClickedCB =
-- function(idx)
-- 	self:OnRecommondListItemClicked(idx)
-- end
-- self._recommondList:SetItemClickedCB(itemClickedCB)
end

function FriendAttView:WinBGType()
    return PanelWinBGType.NormalBG
end

function FriendAttView:OnListItemClicked(idx)
-- --self._listData为空的时候,显示空白
-- if self._listData ==  nil then
--     --self._chatBoxView:SetActive(false)
--     return
-- end
-- --显示右侧信息
-- --self._chatBoxView:SetActive(true)
-- --读取好友消息
-- --self._chatBoxView:SetData(self._listData[idx+1])
-- --记录uuid
-- self._uuid = self._listData[idx+1]:GetUUID()
-- --设置高亮
-- for i=0,#(self._listData)-1 do
--     self._list:GetItem(i):CancelSelect()
-- end
-- self._list:GetItem(idx):OnSelect()
-- --发获取聊天内容RPC
-- local newNum = self._listData[idx+1]:GetNewChatCnt()
-- if newNum ~= 0 then
--     GameWorld.LoggerHelper.Error("发获取聊天内容", true)
--     FriendManager:SendGetChatReq(self._uuid)
-- end
end

--默认选中第一个[主要给列表按钮使用]
function FriendAttView:OnListBarClick(index)
--self:OnListBarClickByUUID(index,nil)
end

--根据传入的uuid选中，如果无则选中第一个
function FriendAttView:OnListBarClickByUUID(index, uuid)

-- self._barIndex = index
-- self._uuid = uuid
-- self:OpenBar(index)
-- if self:QueryOnLine(index) then
--     return
-- end
-- if index == 0  then
--     self:RefreshFriendList()
-- elseif index == 1  then
--     self:RefreshRecentList()
-- elseif index == 2  then
--     self:RefreshTeamList()
-- end
-- -- if self._listData == nil then
-- --     return nil
-- -- end
-- local listIndex = self:GetListIndexByUUID(uuid) or 0
-- self:OnListItemClicked(listIndex)
end


----------------------------------------RefreshList--------------------------------------------------
function FriendAttView:RefreshFriendList()
-- self._listData = FriendManager:GetFriendList()
-- --LoggerHelper.Log({"======== fridend list ", self._listData})
-- self._list:SetDataList(self._listData)
end

function FriendAttView:RefreshRecentList()
-- self._listData = FriendManager:GetRecentList()
-- self._list:SetDataList(self._listData)
end

function FriendAttView:RefreshTeamList()
-- --LoggerHelper.Log({"======== team list ", self._listData})
-- self._listData = FriendManager:GetTeamList()
-- self._list:SetDataList(self._listData)
end

----------------------------------------Resp-------------------------------------------------
function FriendAttView:OnFriendDeleteResp()
-- self:OnListBarClickByUUID(0,nil)
-- --self._friendToggleGroup:SetSelectIndex(0)
-- GameWorld.LoggerHelper.Log("在最近联系人列表里删除好友后，跳到好友列表并选中第一个"..self._barIndex)
end

function FriendAttView:OnFriendAddResp()
-- if self._barIndex == 0 then
--     self:RefreshFriendList()
-- end
-- GameWorld.LoggerHelper.Log("加好友的时候刷新了一次当前界面"..self._barIndex)
end

function FriendAttView:OnChatNewsResp(uuid)
-- --自动获取聊天信息
-- if uuid == self._uuid then
--     GameWorld.LoggerHelper.Log("自动获取聊天信息" )
--     FriendManager:SendGetChatReq(uuid)
-- else
--     if self._barIndex == 0 then
--         self:RefreshFriendList()
--     elseif self._barIndex == 1 then
--         self:RefreshRecentList()
--     elseif self._barIndex == 2 then
--         self:RefreshTeamList()
--     end
--      --刷新后选中之前选中的人
--     local listIndex = self:GetListIndexByUUID(self._uuid) or 0
--     self:OnListItemClicked(listIndex)
--     GameWorld.LoggerHelper.Log("收到新消息提醒时刷新了一次当前界面"..self._barIndex)
-- end
end

function FriendAttView:OnListNumChanged()
-- GameWorld.LoggerHelper.Log("某个列表上的数量有变化")
-- self:RefreshNum()
end

function FriendAttView:OnSortTop()

-- if self._barIndex == 1  then
--     self:RefreshRecentList()
--     self:OnListItemClicked(0)
-- elseif self._barIndex == 2  then
--     self:RefreshTeamList()
--     self:OnListItemClicked(0)
-- end
-- GameWorld.LoggerHelper.Log("置顶排序时刷新列表")
end

function FriendAttView:OnChatListResp(uuid, chatList)

-- local index = self:GetListIndexByUUID(uuid)
-- self._list:GetItem(index):CancelRedCnt()
end

--返回在线时间再刷新列表
function FriendAttView:OnLineListResp()
-- if self._barIndex == 0  then
--     self:RefreshFriendList()
-- elseif self._barIndex == 1  then
--     self:RefreshRecentList()
-- elseif self._barIndex == 2  then
--     self:RefreshTeamList()
-- end
-- local listIndex = self:GetListIndexByUUID(self._uuid) or 0
-- self:OnListItemClicked(listIndex)
-- GameWorld.LoggerHelper.Log("刷新【在线状态】" )
end

-------------------------------工具类-----------------------------------------
--发送检查这个列表是否在线
function FriendAttView:CheckOnLineByList(list)
-- local uuid = nil
-- local reqString = ''
-- for k,v in pairs(list) do
--     uuid = v:GetUUID()
--     reqString = reqString..uuid..'|'
-- end
-- --截取从1开始到倒数第2位
-- reqString = string.sub(reqString,1,-2)
-- GameWorld.LoggerHelper.Log("发送的在线请求"..reqString)
-- FriendManager:SendOfflineListReq(reqString)
end

--刷新列表上的数目
function FriendAttView:RefreshNum()
-- local listNum = FriendManager:GetListNum()
-- local level = GameWorld.Player().level or 1
-- local num1Limit = 20
-- if level >= 20 then
--     num1Limit = 30
--     if level >= 30 then
--         num1Limit = 40
--         if level >=40 then
--             num1Limit = 50
--         end
--     end
-- end
-- local num1 = string.format("%d/%d",listNum[1],num1Limit)
-- local num2 = string.format("%d/%d",listNum[2],20)
-- local num3 = string.format("%d/%d",listNum[3],20)
--self._friendNumText[0].text = num1
--self._friendNumText[1].text = num2
--self._friendNumText[2].text = num3
end


function FriendAttView:OpenBar(index)
--    --分别设置好友列表List、最近联系人栏、最近组队栏的位置(好友栏的位置是恒定不变的)
--     self._friendListRect.localPosition = self.postionTable[index][0]
--     self._friendToggleRects[1].localPosition = self.postionTable[index][1]
--     self._friendToggleRects[2].localPosition = self.postionTable[index][2]
--     --设置三角型的变化
--     for i=0,2 do
--         if i == index then
--             self._signRect[i].localEulerAngles = Vector3.New(0,0,0)
--         else
--             self._signRect[i].localEulerAngles = Vector3.New(0,0,90)
--         end
--     end
end

--获取UUID在List中的位置[刷新List时可用]
function FriendAttView:GetListIndexByUUID(uuid)
-- if uuid == nil then
--     return nil
-- end
-- if self._listData == nil then
--     return nil
-- end
-- for k,data in ipairs(self._listData) do
--     if data:GetUUID() == uuid then
--         return k-1
--     end
-- end
-- return nil
end

--查询在线数据,如果需要查询返回true,不需要返回false
function FriendAttView:QueryOnLine(index)

-- if index == 0  and self._onLineFriendList then
--     self._onLineFriendList = false
--     if FriendManager:IsFriendListNill() then
--         return false
--     else
--         FriendManager:SendFriendOfflineListReq()
--         return true
--     end
-- elseif index == 1 and self._onLineRecentList then
--     self._onLineRecentList = false
--     if FriendManager:IsRecentListNill() then
--         return false
--     else
--          FriendManager:SendRecentOfflineListReq()
--         return true
--     end
-- elseif index == 2 and self._onLineTeamList then
--     self._onLineTeamList = false
--     if FriendManager:IsTeamListNill() then
--         return false
--     else
--         FriendManager:SendTeamOfflineListReq()
--         return true
--     end
-- end
-- return false
end

function FriendAttView:SetOnLine()
    self._onLineFriendList = true
    self._onLineRecentList = true
    self._onLineTeamList = true
end
