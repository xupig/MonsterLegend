-- FriendVoiceButtonView.lua
local FriendVoiceButtonView = Class.FriendVoiceButtonView(ClassTypes.BaseLuaUIComponent)

FriendVoiceButtonView.interface = GameConfig.ComponentsConfig.PointableDragableComponent
FriendVoiceButtonView.classPath = "Modules.ModuleFriend.ChildView.FriendVoiceButtonView"

local PointerInputModule = UnityEngine.EventSystems.PointerInputModule
local RectTransformUtility = UnityEngine.RectTransformUtility
local MAX_DIS = 50
local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager
local ChatConfig = GameConfig.ChatConfig

function FriendVoiceButtonView:Awake()
    self._isDraging = false
    self._SourcePosition = Vector2.zero
    self._SourcePosition.x = self.transform.localPosition.x
    self._SourcePosition.y = self.transform.localPosition.y
    self._CenterOffset = Vector2.zero

end


function FriendVoiceButtonView:OnDestroy()
end

function FriendVoiceButtonView:OnPointerDown(pointerEventData)
    if self._isDraging then 
        return 
    end
    self._isDraging = true
    self:OnDrag(pointerEventData)
end

function FriendVoiceButtonView:OnPointerUp(pointerEventData)
    self:ResetVoiceButtonView()
end

function FriendVoiceButtonView:OnBeginDrag(pointerEventData)
end

function FriendVoiceButtonView:OnDrag(pointerEventData)
    if self._isDraging ~= true then
        do return end
    end
    if pointerEventData.pointerId >= 0 or pointerEventData.pointerId == PointerInputModule.kMouseLeftId then
        self:OnDragView(pointerEventData)
    end
end





function FriendVoiceButtonView:OnEndDrag(pointerEventData)
end

function FriendVoiceButtonView:ResetVoiceButtonView()
    self._isDraging = false
    GUIManager.ClosePanel(PanelsConfig.ChatVoice)
end

function FriendVoiceButtonView:OnDragView(pointerEventData)
    local panelType = PanelsConfig.Friend
    for k,v in pairs(ChatConfig.VoicTipsPosMap) do
        if GUIManager.PanelIsActive(k) then
            panelType = k
        end
    end

    local result = false
    local fingerPos = Vector2.zero
    result, fingerPos = RectTransformUtility.ScreenPointToLocalPointInRectangle(self.transform ,
        pointerEventData.position, pointerEventData.pressEventCamera, fingerPos)
    if result then
        local center = self._SourcePosition + self._CenterOffset
        local direction = (fingerPos - center)
        -- LoggerHelper.Error("pointerEventData.position"..pointerEventData.position.x..","..pointerEventData.position.y)
        -- LoggerHelper.Error("fingerPos"..fingerPos.x..","..fingerPos.y)
        -- LoggerHelper.Error("center"..center.x..","..center.y)
        -- LoggerHelper.Error("direction.magnitude"..direction.magnitude)
        if (direction.magnitude < MAX_DIS) then
            GUIManager.ShowPanel(PanelsConfig.ChatVoice,{type = ChatConfig.Voice,panel = panelType})
        else
            GUIManager.ShowPanel(PanelsConfig.ChatVoice,{type = ChatConfig.CancelVoice,panel = panelType})
        end
    end
end
