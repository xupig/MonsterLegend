--MailViewEx
-- require "Modules.ModuleFriend.ChildComponent.FriendListItem"
-- require "Modules.ModuleFriend.ChildView.ChatBoxView"
require "Modules.ModuleFriend.ChildComponent.FriendChatSecItem"
require "Modules.ModuleFriend.ChildComponent.FriendChatTypeItem"
require "Modules.ModuleFriend.ChildView.FriendChatContent"
require "Modules.ModuleFriend.ChildView.FriendChatBottomChat"


require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid

local MailViewEx = Class.MailViewEx(ClassTypes.BaseLuaUIComponent)

MailViewEx.interface = GameConfig.ComponentsConfig.Component
MailViewEx.classPath = "Modules.ModuleFriend.ChildView.MailViewEx"

local PanelsConfig = GameConfig.PanelsConfig
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local ChatManager = PlayerManager.ChatManager
local FriendPanelType = GameConfig.EnumType.FriendPanelType
local FriendChatType = GameConfig.EnumType.FriendChatType
local FriendChatSecItem = ClassTypes.FriendChatSecItem
local FriendChatTypeItem = ClassTypes.FriendChatTypeItem
local FriendChatContent = ClassTypes.FriendChatContent
local FriendChatBottomChat = ClassTypes.FriendChatBottomChat
local UINavigationMenu = ClassTypes.UINavigationMenu
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FriendData = PlayerManager.PlayerDataManager.friendData
local public_config = GameWorld.public_config
local UIComponentUtil = GameUtil.UIComponentUtil
function MailViewEx:Awake()
    


end

function MailViewEx:ShowView(data)

end

function MailViewEx:CloseViewEx()

end





function MailViewEx:AddEventListeners()



end


function MailViewEx:RemoveEventListeners()


end

function MailViewEx:OnFriendAddResp()
 
end

function MailViewEx:OnFriendlyChange(friendDbid,value)


end

function MailViewEx:RefreshView()

end

function MailViewEx:OnFriendApplyListRed()


end


function MailViewEx:OnFriendUpdateRed()

end

function MailViewEx:OnPerNumChange()

end

function MailViewEx:OnBlackListResp(isSetData)

end

function MailViewEx:RefreshDataForList()




    
end


function MailViewEx:OnFoeListResp()


end


function MailViewEx:OnFriendListResp()

end


function MailViewEx:RefreshFriendList()

end


function MailViewEx:InitViews()
    

    
    

end

-- function MailViewEx:OnShow()
--     SetTypeNavData()
-- end
function MailViewEx:UpdateNavData()

end

function MailViewEx:SetTypeNavData()
    


end

function MailViewEx:UpdateFirstNum()

end


function MailViewEx:InitCallBack()

end

--更新二级菜单选中的item
function MailViewEx:UpdateSelectSItemCount()

end


function MailViewEx:OnMenuFItemClicked(idx)

    

end



function MailViewEx:SetChatShow(idx)


end

function MailViewEx:OnMenuSItemClicked(fIndex, sIndex)


end

--加好友
function MailViewEx:OnAddFriendClick()

end

--申请好友
function MailViewEx:OnApplyFriendClick()

end

--送花
function MailViewEx:OnGiveFriendClick()

end

--聊天类型
function MailViewEx:OnChatFriendClick()

end

--发语音
function MailViewEx:OnVoiceFriendClick()

end

--发送
function MailViewEx:OnSendFriendClick()

end


function MailViewEx:SendChatText(text,liststr)



end


function MailViewEx:SetOnLine()

end






