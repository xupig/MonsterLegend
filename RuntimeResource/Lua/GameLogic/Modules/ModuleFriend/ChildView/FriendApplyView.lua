--FriendApplyView
require "Modules.ModuleFriend.ChildComponent.ApplyListItem"

local FriendApplyView = Class.FriendApplyView(ClassTypes.BaseLuaUIComponent)
FriendApplyView.interface = GameConfig.ComponentsConfig.Component
FriendApplyView.classPath = "Modules.ModuleFriend.ChildView.FriendApplyView"
local PanelsConfig = GameConfig.PanelsConfig
local FriendData = PlayerManager.PlayerDataManager.friendData
local FriendManager = PlayerManager.FriendManager
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local ApplyListItem = ClassTypes.ApplyListItem


function FriendApplyView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')

    self:InitView()
    self:InitRecommondList()

    self._refreshApplyListFun = function () 
        self:RefreshApplyListData()
    end
end

function FriendApplyView:InitView()
    self._btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btnClose, function() self:OnCloseClick() end)

    self._btnAllAgree = self:FindChildGO("Button_AllAgree")
    self._csBH:AddClick(self._btnAllAgree, function() self:OnAllAgreeClick() end)

    self._btnAllReject = self:FindChildGO("Button_AllReject")
    self._csBH:AddClick(self._btnAllReject, function() self:OnAllRejectClick() end)
end

function FriendApplyView:OnAllAgreeClick()
    local _applyListData = FriendManager:GetApplyListData()
    --LoggerHelper.Error("FriendApplyView:OnAllAgreeClick"..PrintTable:TableToStr(_applyListData))
    --LoggerHelper.Error("FriendApplyView:OnAllAgreeClick"..#_applyListData)

    for i=1,#_applyListData do
        --LoggerHelper.Error("_applyListData"..PrintTable:TableToStr(_applyListData[i]))
        --LoggerHelper.Error("_applyListData".._applyListData[i]:GetDbid())
        FriendManager:SendAgreeFriendReq(_applyListData[i]:GetDbid())   
    end

    if _applyListData then
        FriendData:ClearApplyListData()
    end

    
end

function FriendApplyView:OnAllRejectClick()
    local _applyListData = FriendManager:GetApplyListData()
    for i=1,#_applyListData do
        FriendManager:SendRefuseFriendReq(_applyListData[i]:GetDbid())   
    end

    if _applyListData then
        FriendData:ClearApplyListData()
    end
end

function FriendApplyView:OnCloseClick()
    --GUIManager.ClosePanel(PanelsConfig.Friend)
end

function FriendApplyView:InitRecommondList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._applyList = list
    self._applyListScrollView = scrollView
    self._applyListScrollView:SetHorizontalMove(false)
    self._applyListScrollView:SetVerticalMove(true)
    self._applyList:SetItemType(ApplyListItem)
    self._applyList:SetPadding(20, 20, 0, 20)
    self._applyList:SetGap(0, 0)
    self._applyList:SetDirection(UIList.DirectionTopToDown, 1, -1)    
end

-- function FriendApplyView:OnDestroy()
-- end
function FriendApplyView:ShowView(data)


    EventDispatcher:AddEventListener(GameEvents.RefreshApplyList, self._refreshApplyListFun)
    EventDispatcher:AddEventListener(GameEvents.GetApplyListResq,self._refreshApplyListFun)
    self._applyList:SetDataList(data)
end

function FriendApplyView:CloseView()
    EventDispatcher:RemoveEventListener(GameEvents.RefreshApplyList, self._refreshApplyListFun)
    EventDispatcher:RemoveEventListener(GameEvents.GetApplyListResq,self._refreshApplyListFun)
end




function FriendApplyView:AddEventListeners()

end

function FriendApplyView:RemoveEventListeners()

end

function FriendApplyView:OnDeleteApplyList()

end

function FriendApplyView:OnFriendAddResp() 

end

function FriendApplyView:OnFriendApply()

end

function FriendApplyView:RefreshApplyListData()
     self._applyList:RemoveAll()
     local _applyListData = FriendData:GetApplyListData()  
     --LoggerHelper.Error("RefreshApplyListData"..PrintTable:TableToStr(_applyListData))
     local _friendData = FriendData:GetFriendListData()
     --LoggerHelper.Error("_friendData"..PrintTable:TableToStr(_applyListData))
     self._applyList:SetDataList(_applyListData)
end

function FriendApplyView:InitScrollViewList()

end

