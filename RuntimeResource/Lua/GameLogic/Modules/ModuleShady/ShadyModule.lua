ShadyModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function ShadyModule.Init()
	GUIManager.AddPanel(PanelsConfig.Shady, "Panel_Shady", GUILayer.LayerUICG, "Modules.ModuleShady.ShadyPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
    GUIManager.AddPanel(PanelsConfig.BossIntroduce,'Panel_BossIntroduce', GUILayer.LayerUICG, "Modules.ModuleShady.BossIntroducePanel", true, PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return ShadyModule