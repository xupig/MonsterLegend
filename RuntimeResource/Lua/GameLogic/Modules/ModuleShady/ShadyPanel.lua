-- ShadyPanel.lua
local ShadyPanel = Class.ShadyPanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting

local XImageTweenAlpha = GameMain.XImageTweenAlpha
local TimerHeap = GameWorld.TimerHeap

function ShadyPanel:Awake()
    self:InitComps()
end

function ShadyPanel:OnDestroy()

end

function ShadyPanel:OnClose()
    self:ResetAlpha(1.0)
end

function ShadyPanel:InitComps()
    self._bg = self:FindChildGO("Container_Content/Image_bg")
    self._tweenAlpha = self._bg:AddComponent(typeof(XImageTweenAlpha))
    self._showText = self:GetChildComponent("Container_Content/Text", "TextMeshWrapper")
    self._onClosePanelFun = function(data) self:ClosePanel(data) end
end

function ShadyPanel:OnShow(data)
    EventDispatcher:AddEventListener(GameEvents.ON_CLOSE_SHADY, self._onClosePanelFun)
    self:ShowText(data)
end

function ShadyPanel:SetData(data)
    self:ShowText(data)
end

function ShadyPanel:ShowText(data)
    if data.FadeIn ~= nil and data.FadeIn == false then
        self._showText.text = data.text
        self:ResetAlpha(1.0)
        return
    end
    
    self._showText.text = ''
    self._text = data.text
    self:ResetAlpha(0.4)
    self:UpdateAlpha(1.0,data.FadeInTime)
end

function ShadyPanel:UpdateAlpha(value,time)
    self._tweenAlpha:SetToAlphaOnce(value, time)
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
    end
    self._timer = TimerHeap:AddSecTimer(time, 0, 1, function()self:SetText() end)
end

function ShadyPanel:SetText()
    self._showText.text = self._text
end

function ShadyPanel:ResetAlpha(value)
    self._tweenAlpha.IsTweening = false
    self._tweenAlpha:SetColor(value)
end

function ShadyPanel:OnClose() 
    EventDispatcher:RemoveEventListener(GameEvents.ON_CLOSE_SHADY, self._onClosePanelFun)
end

function ShadyPanel:ClosePanel(data)
    if data ~= nil and  data.FadeOut == true then
        self:ResetAlpha(1.0)
        self:ClosePanelAlpha(0.3,data.FadeOutTime)
    else
        self:OnClosePanel()
    end
end

function ShadyPanel:OnClosePanel()
    GameManager.GUIManager.ClosePanel(GameConfig.PanelsConfig.Shady)
end

function ShadyPanel:ClosePanelAlpha(value,time)
    self._tweenAlpha:SetToAlphaOnce(value, time)
    self._showText.text = ''
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
    end
    self._timer = TimerHeap:AddSecTimer(time, 0, 1, function()self:OnClosePanel() end)
end