local BossIntroducePanel = Class.BossIntroducePanel(ClassTypes.BasePanel)
local LocalSetting = GameConfig.LocalSetting

local RawImage = UnityEngine.UI.RawImage
local Locater = GameMain.Locater
local TimerHeap = GameWorld.TimerHeap
local XIntroduceBoss = GameMain.XIntroduceBoss
local XIntroduceNormal = GameMain.XIntroduceNormal
local MonsterDataHelper = GameDataHelper.MonsterDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function BossIntroducePanel:Awake()
    self._introduce = self:AddChildComponent('Container_Sboss', XIntroduceBoss)
    self._introduce.gameObject:SetActive(false)

    self._normalIntroduce = self:AddChildComponent('Container_Normal', XIntroduceNormal)
    self._normalIntroduce.gameObject:SetActive(false)

    self:AddListeners()
end

function BossIntroducePanel:OnDestroy()
    self:RemoveListeners()
end

function BossIntroducePanel:AddListeners()
    self._showIntroduce = function(monsterId) self:ShowIntroduce(monsterId) end
    EventDispatcher:AddEventListener(GameEvents.SHOW_INTRODUCE, self._showIntroduce)
    self._hideIntroduce = function() self:HideIntroduce() end
    EventDispatcher:AddEventListener(GameEvents.HIDE_INTRODUCE, self._hideIntroduce)

    self._showIntroduceNormal = function(id) self:ShowIntroduceNormal(id) end
    EventDispatcher:AddEventListener(GameEvents.SHOW_INTRODUCE_NORMAL, self._showIntroduceNormal)
    self._hideIntroduceeNormal = function() self:HideIntroduceNormal() end
    EventDispatcher:AddEventListener(GameEvents.HIDE_INTRODUCE_NORMAL, self._hideIntroduceeNormal)
end

function BossIntroducePanel:RemoveListeners()
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_INTRODUCE, self._showIntroduce)
    EventDispatcher:RemoveEventListener(GameEvents.HIDE_INTRODUCE, self._hideIntroduce)
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_INTRODUCE_NORMAL, self._showIntroduceNormal)
    EventDispatcher:RemoveEventListener(GameEvents.HIDE_INTRODUCE_NORMAL, self._hideIntroduceeNormal)
end

function BossIntroducePanel:OnShow(data)

end

function BossIntroducePanel:OnClose()
    
end

function BossIntroducePanel:ShowIntroduce(monsterId)
    local name = MonsterDataHelper.GetMonsterName(monsterId)
    local describe = ""
    --MonsterDataHelper.GetMonsterDescribe(monsterId)
    local firstName = GameUtil.StringUtil.SubStringUTF8(name, 1, 1)
    local lastName = GameUtil.StringUtil.SubStringUTF8(name, 2)
    self._introduce:Play(firstName, lastName, describe)
end

function BossIntroducePanel:HideIntroduce()
    self._introduce:FadeOut()
end

function BossIntroducePanel:ShowIntroduceNormal(id)
    local name = LanguageDataHelper.CreateContentWithArgs(id)
    self._normalIntroduce:Play(name, "")
end

function BossIntroducePanel:HideIntroduceNormal()
    self._normalIntroduce:FadeOut()
end