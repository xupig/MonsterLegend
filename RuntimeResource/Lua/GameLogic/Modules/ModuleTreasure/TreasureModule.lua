TreasureModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TreasureModule.Init()
    GUIManager.AddPanel(PanelsConfig.Treasure,"Panel_Treasure",GUILayer.LayerUIPanel,"Modules.ModuleTreasure.TreasurePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TreasureModule