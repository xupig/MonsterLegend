local TreasurePanel = Class.TreasurePanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleTreasure.ChildView.TreasureViewBase"
require "Modules.ModuleTreasure.ChildView.TreasureSoulView"
require "Modules.ModuleTreasure.ChildView.WingView"
require "Modules.ModuleTreasure.ChildView.SoulVesselView"
require "Modules.ModuleTreasure.ChildView.HolyWeaponView"
require "Modules.ModuleTreasure.ChildView.MantleView"

local WingView = ClassTypes.WingView
local SoulVesselView = ClassTypes.SoulVesselView
local HolyWeaponView = ClassTypes.HolyWeaponView
local MantleView = ClassTypes.MantleView
local TreasureSoulView = ClassTypes.TreasureSoulView

function TreasurePanel:Awake()
	self:InitCallBack()
	self:InitComps()

end

function TreasurePanel:InitCallBack()
	self._openSoulCb = function (treasureType,itemIds)
		self:OpenSoul(treasureType,itemIds)
	end

	self._closeSoulCb = function ()
		self:CloseSoul()
	end

	self._showModelCb = function ()
		self:ShowModel()
	end

	self._hideModelCb = function ()
		self:HideModel()
	end
end

--override 
function TreasurePanel:OnShow(data)
	self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3317)
	self:UpdateFunctionOpen()
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_OPEN_TREASURE_SOUL,self._openSoulCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_CLOSE_TREASURE_SOUL,self._closeSoulCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_SHOW_TREASURE_MODEL,self._showModelCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_HIDE_TREASURE_MODEL,self._hideModelCb)
	self:OnShowSelectTab(data)
end

--override
function TreasurePanel:OnClose()
    self._iconBg = nil
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_OPEN_TREASURE_SOUL,self._openSoulCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_CLOSE_TREASURE_SOUL,self._closeSoulCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SHOW_TREASURE_MODEL,self._showModelCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_HIDE_TREASURE_MODEL,self._hideModelCb)
	--self._modelComponent:ClearModelGameObject()
end

function TreasurePanel:InitComps()
	self:InitView("Container_Wing", WingView,1)
	self:InitView("Container_SoulVessel", SoulVesselView,2)
	self:InitView("Container_HolyWeapon", HolyWeaponView,3)
	self:InitView("Container_Mantle", MantleView,4)
end

function TreasurePanel:StructingViewInit()
     return true
end

function TreasurePanel:OpenSoul(treasureType,itemIds)
	if self._treasureSoulView == nil then
		self._treasureSoulView = self:AddChildLuaUIComponent("Container_Soul", TreasureSoulView)
	end
	self._treasureSoulView:SetActive(true)
	self._treasureSoulView:ShowView(treasureType,itemIds)
end

function TreasurePanel:CloseSoul()
	self._selectedView:UpdateSoulRedPoint()
	self._treasureSoulView:SetActive(false)
end

function TreasurePanel:HideModel()
	self._selectedView:HideModel()
	self._containerFirstTab:SetActive(false)
end

function TreasurePanel:ShowModel()
	self._selectedView:ShowModel()
	self._containerFirstTab:SetActive(true)
end

-- -- 0 没有  1 普通底板
function TreasurePanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function TreasurePanel:ReopenSubMainPanel()
    return true
end

function TreasurePanel:BaseShowModel()
	self._selectedView:ShowModel()
end

function TreasurePanel:BaseHideModel()
	self._selectedView:HideModel()
end

-- function TreasurePanel:ScaleToHide()
--     return true
-- end