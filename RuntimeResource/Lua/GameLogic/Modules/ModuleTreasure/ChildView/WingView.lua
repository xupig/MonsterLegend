-- WingView.lua
local WingView = Class.WingView(ClassTypes.TreasureViewBase)

WingView.interface = GameConfig.ComponentsConfig.Component
WingView.classPath = "Modules.ModuleTreasure.ChildView.WingView"

local public_config = GameWorld.public_config

function WingView:Awake()
	self._base.Awake(self)
	self._treasureType = public_config.TREASURE_TYPE_WING
	--幻化功能ID
    self._transformFuncId = 605
	self:InitInfo()
end
