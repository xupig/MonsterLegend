require "Modules.ModuleTreasure.ChildComponent.TreasureRareItem"

local TreasureSoulView = Class.TreasureSoulView(ClassTypes.BaseLuaUIComponent)

TreasureSoulView.interface = GameConfig.ComponentsConfig.Component
TreasureSoulView.classPath = "Modules.ModuleTreasure.ChildView.TreasureSoulView"

local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local EntityConfig = GameConfig.EntityConfig
local TreasureManager = PlayerManager.TreasureManager
local partnerData = PlayerManager.PlayerDataManager.partnerData
local TreasureRareItem = ClassTypes.TreasureRareItem
local bagData = PlayerManager.PlayerDataManager.bagData
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local QualityType = GameConfig.EnumType.QualityType
local StringStyleUtil = GameUtil.StringStyleUtil

function TreasureSoulView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitView()
    self:AddEventListeners()
end
--override
function TreasureSoulView:OnDestroy() 
    self:RemoveEventListeners()
end
--data结构
function TreasureSoulView:ShowView(treasureType,itemIds)
    self._treasureType = treasureType
    self._itemIds = itemIds
    self:RefreshView()
end

function TreasureSoulView:AddEventListeners()
    self._updateCB = function ()
        self:RefreshView()
    end
    EventDispatcher:AddEventListener(GameEvents.TREASURE_UPDATE,self._updateCB)
end

function TreasureSoulView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.TREASURE_UPDATE,self._updateCB)
end

function TreasureSoulView:InitView()
    self._close = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._close,function () self:OnCloseButtonClick() end)

    self._descText = self:GetChildComponent("Text_Info", "TextMeshWrapper")

    self._txtTitle = self:GetChildComponent("Text_Title", "TextMeshWrapper")
    self._soulList = {}
    for i=1, 3 do
        self._soulList[i] = self:AddChildLuaUIComponent("Container_Items/Container_Item"..i, TreasureRareItem)
    end
end

function TreasureSoulView:RefreshView()
    self._txtTitle.text = LanguageDataHelper.CreateContent(27052+self._treasureType)
    local data = TreasureManager:GetTreasureInfo(self._treasureType)
    local itemsUsedNum = data[public_config.TREASURE_KEY_ITEM_USE_NUM]
    for i=1,#self._itemIds do
        local itemId = self._itemIds[i]
        local curCount = itemsUsedNum[itemId] or 0
        local useMax,nextPlayerLevel = TreasureManager:GetTreasureSoulItemUseMax(self._treasureType,itemId)
        self._nextPlayerLevel = nextPlayerLevel
        local soulData = {itemId,curCount,useMax}
        self._soulList[i]:OnRefreshData(soulData)

    end
    if self._nextPlayerLevel then
        local str = StringStyleUtil.GetLevelString(self._nextPlayerLevel,nil,true)
        self._descText.text = LanguageDataHelper.CreateContentWithArgs(70059,{["0"] = str})
    else
        self._descText.text = ""
    end
end

function TreasureSoulView:OnCloseButtonClick()
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_CLOSE_TREASURE_SOUL)
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SHOW_TREASURE_MODEL)
end
