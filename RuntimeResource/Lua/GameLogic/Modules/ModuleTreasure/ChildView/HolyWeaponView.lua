-- HolyWeaponView.lua
local HolyWeaponView = Class.HolyWeaponView(ClassTypes.TreasureViewBase)

HolyWeaponView.interface = GameConfig.ComponentsConfig.Component
HolyWeaponView.classPath = "Modules.ModuleTreasure.ChildView.HolyWeaponView"

local public_config = GameWorld.public_config

function HolyWeaponView:Awake()
	self._base.Awake(self)
	self._treasureType = public_config.TREASURE_TYPE_WEAPON
	--幻化功能ID
    self._transformFuncId = 607
	self:InitInfo()
end