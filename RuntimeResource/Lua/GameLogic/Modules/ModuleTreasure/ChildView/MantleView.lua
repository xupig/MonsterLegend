-- MantleView.lua
local MantleView = Class.MantleView(ClassTypes.TreasureViewBase)

MantleView.interface = GameConfig.ComponentsConfig.Component
MantleView.classPath = "Modules.ModuleTreasure.ChildView.MantleView"

local public_config = GameWorld.public_config

function MantleView:Awake()
	self._base.Awake(self)
	self._treasureType = public_config.TREASURE_TYPE_CLOAK
	--幻化功能ID
    self._transformFuncId = 608
	self:InitInfo()
end