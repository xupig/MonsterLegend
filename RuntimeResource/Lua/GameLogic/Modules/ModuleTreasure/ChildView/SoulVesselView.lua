-- SoulVesselView.lua
local SoulVesselView = Class.SoulVesselView(ClassTypes.TreasureViewBase)

SoulVesselView.interface = GameConfig.ComponentsConfig.Component
SoulVesselView.classPath = "Modules.ModuleTreasure.ChildView.SoulVesselView"

local public_config = GameWorld.public_config

function SoulVesselView:Awake()
	self._base.Awake(self)
	self._treasureType = public_config.TREASURE_TYPE_TALISMAN
	--幻化功能ID
    self._transformFuncId = 606
	self:InitInfo()
end