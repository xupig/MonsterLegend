-- TreasureViewBase.lua
local TreasureViewBase = Class.TreasureViewBase(ClassTypes.BaseComponent)

TreasureViewBase.interface = GameConfig.ComponentsConfig.Component
TreasureViewBase.classPath = "Modules.ModuleTreasure.ChildView.TreasureViewBase"

require "Modules.ModuleTreasure.ChildComponent.TreasureMaterialItem"
require "Modules.ModuleTreasure.ChildComponent.TreasureSkillItem"
require "Modules.ModuleTreasure.ChildComponent.TreasureAttrListItem"

require "Modules.ModuleTreasure.ChildView.TreasureExpTipsView"


local TreasureMaterialItem = ClassTypes.TreasureMaterialItem
local TreasureSkillItem = ClassTypes.TreasureSkillItem
local TreasureAttrListItem = ClassTypes.TreasureAttrListItem

local UIComponentUtil = GameUtil.UIComponentUtil
local UIList = ClassTypes.UIList
local UIProgressBar = ClassTypes.UIProgressBar
local XArtNumber = GameMain.XArtNumber
local public_config = GameWorld.public_config
local bagData = PlayerManager.PlayerDataManager.bagData
local BagManager = PlayerManager.BagManager
local TreasureManager = PlayerManager.TreasureManager
local BaseUtil = GameUtil.BaseUtil
local TreasureDataHelper = GameDataHelper.TreasureDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TipsManager = GameManager.TipsManager
local TreasureExpTipsView = ClassTypes.TreasureExpTipsView
local EquipModelComponent = GameMain.EquipModelComponent
local GUIManager = GameManager.GUIManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local TransformDataHelper = GameDataHelper.TransformDataHelper
local TransformManager = PlayerManager.TransformManager
local UIParticle = ClassTypes.UIParticle
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function TreasureViewBase:Awake()
    self:InitCallBack()
	self:InitComps()
end

function TreasureViewBase:InitComps()
	self._btnTransform = self:FindChildGO("Button_Transform")
    self._csBH:AddClick(self._btnTransform, function()self:OnClickTransform() end)
    self._imgTransformTip = self:FindChildGO("Button_Transform/Image_Tip")

    self._btnOpenSoul = self:FindChildGO("Button_OpenSoul")
    self._csBH:AddClick(self._btnOpenSoul, function()self:OnClickOpenSoul() end)
    self._imgOpenSoulTip = self:FindChildGO("Button_OpenSoul/Image_Tip")

	self._btnUpgrade = self:FindChildGO("Button_Upgrade")
    self._csBH:AddClick(self._btnUpgrade, function()self:Upgrade() end)
    self._imgUpgradeTip = self:FindChildGO("Button_Upgrade/Image_Tip")
    self._imgUpgradeTip:SetActive(false)

    self._btnAutoUpgrade = self:FindChildGO("Button_AutoUpgrade")
    self._csBH:AddClick(self._btnAutoUpgrade, function()self:AutoUpgrade() end)
    self._imgAutoUpgradeTip = self:FindChildGO("Button_AutoUpgrade/Image_Tip")
    self._imgAutoUpgradeTip:SetActive(false)
    self._inAuto = false

    self._btnStopAuto = self:FindChildGO("Button_StopAuto")
    self._csBH:AddClick(self._btnStopAuto, function()self:StopAutoUpgrade() end)
    self._btnStopAuto:SetActive(false)

    self._materialItems = {}
	local materialItemGOs = {}
	materialItemGOs[1] = self:FindChildGO("Container_Material/Container_Item")
    for i = 2, 3 do
        local go = self:CopyUIGameObject("Container_Material/Container_Item", "Container_Material")
    	materialItemGOs[i] = go
    end

    for i=1,3 do
    	local go = materialItemGOs[i]
    	local item = UIComponentUtil.AddLuaUIComponent(go, TreasureMaterialItem)
        item:SetIndex(i)
    	self._materialItems[i] = item
    end
    self._txtMaterialName = self:GetChildComponent("Text_MaterialName","TextMeshWrapper")

    self._skillItems = {}
    for i=1,5 do
    	local item = self:AddChildLuaUIComponent("Container_Skill/Container_Skills/Container_Skill"..i, TreasureSkillItem)
    	self._skillItems[i] = item
    end

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "List_Attr")
    self._listAttr = list
    self._scrollViewGemBag = scrollView
    self._listAttr:SetItemType(TreasureAttrListItem)
    self._listAttr:SetPadding(0, 0, 0, 0)
    self._listAttr:SetGap(0, 0)
    self._listAttr:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._expProgress = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar_Exp")
    self._expProgress:SetProgress(0)
    self._expProgress:ShowTweenAnimate(true)
    self._expProgress:SetMutipleTween(true)
    self._expProgress:SetIsResetToZero(false)
    self._expProgress:SetTweenTime(0.2)

    self._lastLevel = 0
    self._txtLevel = self:GetChildComponent("Text_Level","TextMeshWrapper")
    self._containerName = self:FindChildGO("Container_Name")
    self._levelUpTip = self:FindChildGO("Image_LevelUp").transform
    self._levelUpTip.localScale = Vector3.zero
    self._tweenScaleLevelUp = self:AddChildComponent('Image_LevelUp', XGameObjectTweenScale)

    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._fpNumber:SetNumber(0)

    self._btnOnShow = self:FindChildGO("Toggle_Show/Background")
    self._csBH:AddClick(self._btnOnShow,function () self:OnShowClick() end)
    self._checkmarkShow = self:FindChildGO("Toggle_Show/Checkmark")

    self._tipsShowView = self:AddChildLuaUIComponent("Container_TipsShow", TreasureExpTipsView)

    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()
    self:SetScroEnable(false)
end

function TreasureViewBase:SetScroEnable(state)
    if self._scrollViewGemBag then
        self._scrollViewGemBag:SetScrollRectState(state)
    end
end

function TreasureViewBase:InitCallBack()
    self._updateTransformRedPointCb = function ()
        self:UpdateTransformRedPoint()
    end

     self._selectMaterialCB = function (index)
        self:SelectMaterial(index)
    end

    self._updateOnShowCb =function ()
        self:UpdateOnShow()
    end
    
    self._updateCB = function () self:UpdateData() end
end

function TreasureViewBase:AutoUpgrade()
    if self._selectedMaterialNum == 0 then
        if self._selectedUpgradeItemId then
            self:BuyGuideTip(self._selectedUpgradeItemId)
        end
        return
    end
    self._inAuto = true
    self._btnStopAuto:SetActive(true)
    self._btnAutoUpgrade:SetActive(false)
    self._autoTimer = TimerHeap:AddSecTimer(0,0.2,0,function ()
        self:Upgrade()
    end)
end

function TreasureViewBase:StopAutoUpgrade()
    if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._autoTimer)
        self._btnStopAuto:SetActive(false)
        self._btnAutoUpgrade:SetActive(true)
    end
end

function TreasureViewBase:Upgrade()
    if self._selectedUpgradeItemId and self._selectedMaterialNum > 0 then
        local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self._selectedUpgradeItemId)
        if pos > 0 then
            BagManager:RequsetUse(pos,1)
        else
            if self._selectedUpgradeItemId then
                self:BuyGuideTip(self._selectedUpgradeItemId)
            end
        end
    end
end

--自动选中
function TreasureViewBase:CheckSelectMaterial()
    if self._selectedMaterialNum > 0 then
        return true
    else
        for i=1,#self._materialItems do
            if self._materialItems[i].num > 0 then
                self:SelectMaterial(i)
                return true
            end
        end 
    end
    return false
end

function TreasureViewBase:SelectMaterial(index)
    local selectMaterialItem = self._materialItems[index]
    if self._selectMaterialItem then
        if self._selectMaterialItem.itemId == selectMaterialItem.itemId then
            TipsManager:ShowItemTipsById(self._selectMaterialItem.itemId)
            return
        end
        self._selectMaterialItem:SetSelected(false)
    end

    self._selectMaterialItem = selectMaterialItem
    self._selectMaterialItem:SetSelected(true)
    self._selectedUpgradeItemId = self._selectMaterialItem.itemId
    self._txtMaterialName.text = ItemDataHelper.GetItemNameWithColor(self._selectedUpgradeItemId)
    self:UpdateUpgradeTip()
end

function TreasureViewBase:CloseView()
    self:StopAutoUpgrade()
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_SELECT_TREASURE_MATERIAL,self._selectMaterialCB)
    EventDispatcher:RemoveEventListener(GameEvents.TREASURE_UPDATE,self._updateCB)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:RemoveEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)

    self:HideModel()
    self:SetScroEnable(false)
end

function TreasureViewBase:ShowView()
    if self._selectMaterialItem then
        self._selectMaterialItem:SetSelected(false)
        self._selectMaterialItem = nil
    end
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_SELECT_TREASURE_MATERIAL,self._selectMaterialCB)
    EventDispatcher:AddEventListener(GameEvents.TREASURE_UPDATE,self._updateCB)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_TRANSFORM_UPDATE_ON_SHOW,self._updateOnShowCb)
    EventDispatcher:AddEventListener(GameEvents.Update_Transform_Red_Point_State,self._updateTransformRedPointCb)
    
    self._dLevel = 0
    self._lastExp = nil
    self:UpdateData()
    self:UpdateModel()
    self:ShowModel()
    self:CheckTransformButtonOpen()
    self:SelectMaterial(1)
    self:CheckSelectMaterial()
    self:UpdateSoulRedPoint()
    self:SetScroEnable(true)
end

function TreasureViewBase:UpdateData()
    --LoggerHelper.Error("TreasureViewBase:UpdateData"..self._treasureType)

    local attris = {} --加成属性key:值
    local dAttris = {} --下级跟这级相差的加成属性key:值
    local attriAddRate = 0 --加成百分比
    local data = TreasureManager:GetTreasureInfo(self._treasureType)
    local level = data[public_config.TREASURE_KEY_LEVEL]
    local exp = data[public_config.TREASURE_KEY_EXP]
    local itemsUsedNum = data[public_config.TREASURE_KEY_ITEM_USE_NUM]
    local lastLevelCfg = TreasureDataHelper.GetTreasureExperienceByLevelType(self._treasureType,level-1)
    local upgradeCfg = TreasureDataHelper.GetTreasureExperienceByLevelType(self._treasureType,level)
    local nextLevelCfg = TreasureDataHelper.GetTreasureExperienceByLevelType(self._treasureType,level+1)

    for k,v in pairs(upgradeCfg.wing_proportion) do
        if nextLevelCfg then
            dAttris[k] = nextLevelCfg.wing_proportion[k] - v
        end
        if attris[k] then
            attris[k] = attris[k] + v
        else
            attris[k] = v
        end
    end

    self._txtLevel.text = "Lv."..level
    local expAdd = 0
    if self._lastLevel > 0 then
        self._dLevel = level - self._lastLevel
    end
    if self._lastExp then
        --LoggerHelper.Error("exp"..exp.."?"..self._lastExp)
        --有经验变化
        if exp ~= self._lastExp then
            if self._dLevel > 0 then
                expAdd = lastLevelCfg.wingLevelExp - self._lastExp + exp
            else
                expAdd = exp - self._lastExp
            end
            self._tipsShowView:ShowView(expAdd)
        end
    end
    self._lastExp = exp
   
    if self._dLevel > 0 then
        self._tweenScaleLevelUp:OnceOutQuad(1, 1, function()  
            self._levelUpTip.localScale = Vector3.zero
        end)
        self._fx:Play()
    end

    self._expProgress:SetProgress(exp/upgradeCfg.wingLevelExp + self._dLevel)
    self._expProgress:SetProgressText(tostring(exp).."/"..tostring(upgradeCfg.wingLevelExp))
    self._lastLevel = level
    
    --稀有道具加成
    for i=1,#self._rareItems do
        local itemId = self._rareItems[i]
        local usedNum = itemsUsedNum[itemId] or 0
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
        --item:UpdateNum(bagNum,usedNum)

        local itemCfg = ItemDataHelper.GetItem(itemId)
        for i=4,#itemCfg.effectId,2 do
            local k = itemCfg.effectId[i]
            local v = itemCfg.effectId[i+1]
            if attris[k] then
                attris[k] = attris[k] + v*usedNum
            else
                attris[k] = v*usedNum
            end
        end
        --稀有道具加成百分比
        attriAddRate = attriAddRate + itemCfg.effectId[3]/100*usedNum
    end

    for i=1,#self._materialItems do
        local item = self._materialItems[i]
        local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(item.itemId)
        item:UpdateNum(bagNum)
    end
    self:UpdateUpgradeTip()
    self:CheckCanAuto()

    local attris1 = {}
    for k,v in pairs(attris) do
        local v1 = math.ceil((1+attriAddRate)*v)
        --LoggerHelper.Error("dAttris"..k)
        local d1
        if dAttris[k] then
            d1 = math.ceil((1+attriAddRate)*dAttris[k])
        end
        table.insert(attris1,{["attri"] = k,["value"] = v1,["add"] = d1})
    end
    local fightPower = BaseUtil.CalculateFightPower(attris1,GameWorld.Player().vocation)
    --LoggerHelper.Error("fightPower"..fightPower)
    self._fpNumber:SetNumber(fightPower)

    self._listAttr:SetDataList(attris1)

    for i=1,#self._skillItems do
        self._skillItems[i]:UpdateLock(level)
    end

    self:UpdateOnShow()
end

function TreasureViewBase:OnShowClick()
    TransformManager:RequestTransformShow(self._treasureType,public_config.TREASURE_SHOW_ORIGIN_COMMON,1)
end

function TreasureViewBase:UpdateOnShow()
    local showInfo = GameWorld.Player().illusion_show_info[self._treasureType]
    if showInfo == nil then
        LoggerHelper.Error("请先跑任务开启相关宝物功能")
        return
    end
    if showInfo[public_config.TREASURE_ILLUSION_SHOW_SYSTEM] == public_config.TREASURE_SHOW_ORIGIN_COMMON then
        self._btnOnShow:SetActive(false)
        self._checkmarkShow:SetActive(true)
    else
        self._btnOnShow:SetActive(true)
        self._checkmarkShow:SetActive(false)
    end
end

--红点提示
function TreasureViewBase:UpdateUpgradeTip()
    if self._selectMaterialItem then
        self._selectedMaterialNum = self._selectMaterialItem.num
        self._imgUpgradeTip:SetActive(self._selectedMaterialNum > 0)
        self._imgAutoUpgradeTip:SetActive(self._selectedMaterialNum > 0)
        
    end
end

--检查是否能自动提升
function TreasureViewBase:CheckCanAuto()
    if self._inAuto then
        if not self:CheckSelectMaterial() then
            self:StopAutoUpgrade()
            if self._selectedUpgradeItemId then
                self:BuyGuideTip(self._selectedUpgradeItemId)
            end
        end
        --LoggerHelper.Error("TreasureViewBase:UpdateUpgradeTip()")
    end
end

function TreasureViewBase:BuyGuideTip(itemId)
    if ItemDataHelper.GetGuideBuyCost(itemId)~=0 then
        local btnArgs = {}
        btnArgs.guideBuy = {itemId}
        TipsManager:ShowItemTipsById(itemId,btnArgs)
    end
end



--初始化界面基础信息
function TreasureViewBase:InitInfo()
    local expItemList = ItemDataHelper.GetAllItemByEffectId(public_config.ITEM_EFF_ID_TREASURE_EXP)
    local h = 1
    for i=1,#expItemList do
        local itemInfo = expItemList[i]
        if itemInfo.effectId[2] == self._treasureType then
            self._materialItems[h]:SetItem(itemInfo.id)
            h = h+1
        end
    end

    self._rareItems ={}
    local rareItemList = ItemDataHelper.GetAllItemByEffectId(public_config.ITEM_EFF_ID_TREASURE_ATTRI)
    h = 1
    for i=1,#rareItemList do
        local itemInfo = rareItemList[i]
        if itemInfo.effectId[2] == self._treasureType then
            self._rareItems[h] = itemInfo.id
            h = h+1
        end
    end
    
    local skillList = TreasureDataHelper.GetTreasureSkill(self._treasureType).level_skill
    for i=1,#skillList do
        self._skillItems[i]:SetTreasureType(self._treasureType)
        self._skillItems[i]:UpdateData(skillList[i])
    end

    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local nameIcon = GlobalParamsHelper.GetParamValue(707+self._treasureType)[vocationGroup]
    GameWorld.AddIcon(self._containerName,nameIcon)
end

function TreasureViewBase:OnClickOpenSoul()
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_OPEN_TREASURE_SOUL,self._treasureType,self._rareItems)
    self:HideModel()
end

function TreasureViewBase:UpdateSoulRedPoint()
    local shouldShow = TreasureManager:RefreshSoulRedPointState(self._treasureType)
    self._imgOpenSoulTip:SetActive(shouldShow)
end

function TreasureViewBase:UpdateTransformRedPoint()
    local shouldShow = TransformManager:GetTransformRedPointState(self._treasureType)
    self._imgTransformTip:SetActive(shouldShow)
end

function TreasureViewBase:UpdateModel()
    if self._modelComponent then
        return
    else
        self._containerModel = self:FindChildGO("Container_Model")
        self._modelComponent = self:AddChildComponent('Container_Model', EquipModelComponent)
    end

    local vocationGroup = math.floor(GameWorld.Player().vocation/100)
    local uiModelCfgId = GlobalParamsHelper.GetParamValue(703+self._treasureType)[vocationGroup]
    local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
    
    local modelId = modelInfo.model_ui
    local posx = modelInfo.pos[1]
    local posy = modelInfo.pos[2]
    local posz = modelInfo.pos[3]
    local rotationx = modelInfo.rotation[1]
    local rotationy = modelInfo.rotation[2]
    local rotationz = modelInfo.rotation[3]
    local scale = modelInfo.scale
    self._modelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
    
    --神兵自转
    if self._treasureType == public_config.TREASURE_TYPE_WEAPON then
        self._modelComponent:SetSelfRotate(true)
    end

    if self._treasureType == public_config.TREASURE_TYPE_TALISMAN then
        --法宝替换controller
        local controllerPath = GlobalParamsHelper.GetParamValue(692)
        self._modelComponent:LoadEquipModel(modelId, controllerPath)
    else
        self._modelComponent:LoadEquipModel(modelId, "")
    end
end

function TreasureViewBase:HideModel()
   self._containerModel:SetActive(false)
end

function TreasureViewBase:ShowModel()
    self._containerModel:SetActive(true) 
end

function TreasureViewBase:CheckTransformButtonOpen()
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(self._transformFuncId)
    self._btnTransform:SetActive(isOpen)
    if isOpen then
        self:UpdateTransformRedPoint()
    end
end

function TreasureViewBase:OnClickTransform()
    GUIManager.ShowPanelByFunctionId(self._transformFuncId)
    EventDispatcher:TriggerEvent(GameEvents.UIEVENT_HIDE_TREASURE_MODEL)
end