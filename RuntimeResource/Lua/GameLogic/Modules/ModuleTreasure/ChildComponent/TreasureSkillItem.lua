-- TreasureSkillItem.lua
local TreasureSkillItem = Class.TreasureSkillItem(ClassTypes.BaseComponent)

TreasureSkillItem.interface = GameConfig.ComponentsConfig.PointableComponent
TreasureSkillItem.classPath = "Modules.ModuleTreasure.ChildComponent.TreasureSkillItem"

local SkillDataHelper = GameDataHelper.SkillDataHelper
local TipsManager = GameManager.TipsManager

function TreasureSkillItem:Awake()
	self._base.Awake(self)
	self._containerIcon = self:FindChildGO("Container_Icon")
end

function TreasureSkillItem:SetTreasureType(treasureType)
	self._treasureType = treasureType
end

function TreasureSkillItem:UpdateData(data)
	self._skillId = data[2] 
	self._levelOpen = data[1] --开启等级
	self._icon = SkillDataHelper.GetSkillIcon(self._skillId)
end

function TreasureSkillItem:UpdateLock(treasurelevel)
	self._treasurelevel = treasurelevel
	if self._levelOpen then
		if self._levelOpen > treasurelevel then
			GameWorld.AddIcon(self._containerIcon,self._icon,0,false,0)
		else
			GameWorld.AddIcon(self._containerIcon,self._icon)
		end
	end
end

function TreasureSkillItem:OnPointerDown()
	local condition
	if self._levelOpen > self._treasurelevel then
		condition = self._levelOpen
	end
	TipsManager:ShowSkillTips(self._skillId,condition,self._treasureType)
end

function TreasureSkillItem:OnPointerUp()
	
end