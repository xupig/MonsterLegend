-- TreasureMaterialItem.lua
local TreasureMaterialItem = Class.TreasureMaterialItem(ClassTypes.BaseComponent)

TreasureMaterialItem.interface = GameConfig.ComponentsConfig.PointableComponent
TreasureMaterialItem.classPath = "Modules.ModuleTreasure.ChildComponent.TreasureMaterialItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function TreasureMaterialItem:Awake()
	self._base.Awake(self)
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_Icon")
	self._icon = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._textNum = self:GetChildComponent("Text_Num", "TextMeshWrapper")
	self._imgSelected = self:FindChildGO("Image_Selected")
	self._imgSelected:SetActive(false)
	self.num = 0
end

function TreasureMaterialItem:SetItem(itemId)
	self.itemId = itemId
	self._icon:SetItem(itemId)
end

function TreasureMaterialItem:UpdateNum(num)
	self._textNum.text = tostring(num)
	self.num = num
end

function TreasureMaterialItem:SetIndex(index)
	self._index = index
end

function TreasureMaterialItem:SetSelected(isSelected)
	self._imgSelected:SetActive(isSelected)
end

function TreasureMaterialItem:OnPointerDown()
	EventDispatcher:TriggerEvent(GameEvents.UIEVENT_SELECT_TREASURE_MATERIAL,self._index)
end

function TreasureMaterialItem:OnPointerUp()
	
end