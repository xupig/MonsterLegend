--TreasureAttrListItem.lua
local UIListItem = ClassTypes.UIListItem
local TreasureAttrListItem = Class.TreasureAttrListItem(UIListItem)
TreasureAttrListItem.interface = GameConfig.ComponentsConfig.Component
TreasureAttrListItem.classPath = "Modules.ModuleTreasure.ChildComponent.TreasureAttrListItem"

local AttriDataHelper = GameDataHelper.AttriDataHelper

function TreasureAttrListItem:Awake()
    self._base.Awake(self)
    self._txtAttrName = self:GetChildComponent("Text_AttrName",'TextMeshWrapper')
    self._txtAttrValue = self:GetChildComponent("Text_AttrValue",'TextMeshWrapper')
    self._txtAddAttrValue = self:GetChildComponent("Text_AddAttrValue",'TextMeshWrapper')
    self._imgAdd = self:FindChildGO("Image_Add")
end

function TreasureAttrListItem:OnDestroy()
end

--override
function TreasureAttrListItem:OnRefreshData(data)
	self._txtAttrName.text = AttriDataHelper:GetName(data.attri)
	self._txtAttrValue.text =  AttriDataHelper:ConvertAttriValue(data.attri,data.value)
	if data.add then
		self._txtAddAttrValue.text = AttriDataHelper:ConvertAttriValue(data.attri,data.add)
	else
		self._txtAddAttrValue.text = ""
	end
end