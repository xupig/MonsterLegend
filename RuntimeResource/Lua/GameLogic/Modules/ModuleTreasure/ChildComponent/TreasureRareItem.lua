local TreasureRareItem = Class.TreasureRareItem(ClassTypes.BaseLuaUIComponent)

TreasureRareItem.interface = GameConfig.ComponentsConfig.Component
TreasureRareItem.classPath = "Modules.ModuleTreasure.ChildComponent.TreasureRareItem"

require "UIComponent.Extend.ItemManager"
require "UIComponent.Extend.ItemGrid"
local ItemManager = ClassTypes.ItemManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local BagManager = PlayerManager.BagManager
local TipsManager = GameManager.TipsManager
local UIProgressBar = ClassTypes.UIProgressBar
local AttriDataHelper = GameDataHelper.AttriDataHelper
local ColorConfig = GameConfig.ColorConfig
local ItemGrid = ClassTypes.ItemGrid
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local TransformManager = PlayerManager.TransformManager

function TreasureRareItem:Awake()
    self._base.Awake(self)
    self.itemId = 0
    self:InitView()
end

function TreasureRareItem:InitView()
    self._useButton = self:FindChildGO("Button_Use")
    self._csBH:AddClick(self._useButton,function () self:OnUseButtonClick() end)

    self._tips = self:FindChildGO("Button_Use/Image_Tip")
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
    self._countText = self:GetChildComponent("Text_Count", "TextMeshWrapper")
    self._infoText = self:GetChildComponent("Text_Info", "TextMeshWrapper")
    local itemGo = GUIManager.AddItem(self._iconContainer, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
    self._icon:ActivateTipsById()

    self:InitProgress()
end

function TreasureRareItem:InitProgress()
    self._Comp = UIProgressBar.AddProgressBar(self.gameObject,"ProgressBar")
    self._Comp:SetProgress(0)
    self._Comp:ShowTweenAnimate(true)
    self._Comp:SetMutipleTween(true)
    self._Comp:SetIsResetToZero(false)
    self._Comp:SetTweenTime(0.2)
end

function TreasureRareItem:OnUseTimeChange()
    local itemId = self.data[1]
    local cur_count = self.data[2]
    if cur_count == self.useCount then
        return
    end
    local max_count = self.data[3]
    self._Comp:SetProgress(cur_count/max_count)
    self._Comp:SetProgressText(tostring(cur_count).."/"..tostring(max_count))
    self.useCount = cur_count
end

--data: {itemId,{等级下限，等级上限，数量,...}}
function TreasureRareItem:OnRefreshData(data)
    self.data = data
    local itemId = self.data[1]
    self._icon:SetItem(itemId)
    local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemId)
   	
    local showText = bagNum.."/1"
    if bagNum > 0 then
        self._tips:SetActive(true)
        showText = BaseUtil.GetColorString(showText, ColorConfig.J)
    else
        self._tips:SetActive(false)
        showText = BaseUtil.GetColorString(showText, ColorConfig.H)
    end

    self._countText.text = showText
    self._nameText.text = ItemDataHelper.GetItemNameWithColor(itemId)
    self._infoText.text = self:GetAttriInfo(itemId)
    self:OnUseTimeChange()
end

function TreasureRareItem:OnUseButtonClick()
    local pos = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemPosByItemId(self.data[1])
    if pos <= 0 then
        return
    end
    BagManager:RequsetUse(pos, 1)
end

function TreasureRareItem:GetAttriInfo(itemId)
    local effectData = ItemDataHelper.GetItemEffect(itemId)
    local result = ""
    for i=4,#effectData,2 do
        local attri = effectData[i]
        local value = effectData[i+1]
        result = result .. AttriDataHelper:GetName(attri).."+"..value.." "
    end
   
    if effectData[3] > 0 then
        local systemCn = TransformManager:GetSystemCn(effectData[2])
        local t = LanguageDataHelper.GetArgsTable()
        t["0"] = systemCn
        t["1"] = effectData[3]
        result = result..LanguageDataHelper.CreateContentWithArgs(58776, t)
    end
    return result
end