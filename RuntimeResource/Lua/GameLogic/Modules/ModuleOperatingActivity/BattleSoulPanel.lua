local BattleSoulPanel = Class.BattleSoulPanel(ClassTypes.BasePanel)

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

require "Modules.ModuleOperatingActivity.ChildComponent.BattleSoulRankListItem"
local BattleSoulRankListItem = ClassTypes.BattleSoulRankListItem
local UIList = ClassTypes.UIList
local StringStyleUtil = GameUtil.StringStyleUtil

function BattleSoulPanel:Awake()
	self.disableCameraCulling = true
	self:InitCallBack()
	self:InitComps()
end

function BattleSoulPanel:OnShow(data)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_END_TIME,self._updateEndTimeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_WAVE_TIME,self._updateWaveTimeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_QUIT_TIME,self._updateQuitTimeCb)
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_RANK,self._updateRankCb)
end

function BattleSoulPanel:OnClose()
	if self._timerId then
		TimerHeap:DelTimer(self._timerId)
		self._timerId = nil
	end

	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_END_TIME,self._updateEndTimeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_WAVE_TIME,self._updateWaveTimeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_QUIT_TIME,self._updateQuitTimeCb)
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_BATTLE_SOUL_RANK,self._updateRankCb)
end

function BattleSoulPanel:InitCallBack()
	self._updateEndTimeCb = function (endTime)
		self:UpdateEndTime(endTime)
	end

	self._updateWaveTimeCb = function (endTime)
		self:UpdateWaveTime(endTime)
	end

	self._updateQuitTimeCb = function (endTime)
		self:UpdateQuitTime(endTime)
	end

	self._updateRankCb = function (args)
		self:UpdateRank(args)
	end
end

function BattleSoulPanel:OnDestroy()
	self._scrollView = nil
	self._listRank= nil

    self._txtMyRank = nil
    self._txtMyDamage = nil

    self._containerInstanceTime  = nil

    self._containerWaveTime  = nil
    self._txtWaveTimeTitle = nil
    self._txtWaveTime = nil

    self._txtInstanceCDTime = nil

    self._timerId = nil
end

function BattleSoulPanel:InitComps()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Left/ScrollViewList")
    self._listRank = list
    self._scrollView = scrollView
    self._listRank:SetItemType(BattleSoulRankListItem)
    self._listRank:SetPadding(0, 0, 0, 0)
    self._listRank:SetGap(10, 0)
    self._listRank:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._txtMyRank = self:GetChildComponent("Container_Left/Text_Rank",'TextMeshWrapper')
    self._txtMyDamage = self:GetChildComponent("Container_Left/Text_Damage",'TextMeshWrapper')

    self._containerInstanceTime  = self:FindChildGO("Container_InstanceTipTime")

    self._containerWaveTime  = self:FindChildGO("Container_InstanceTipTime/Container_WaveTime")
    self._txtWaveTimeTitle = self:GetChildComponent("Container_InstanceTipTime/Container_WaveTime/Text_Time",'TextMeshWrapper')
    self._txtWaveTime = self:GetChildComponent("Container_InstanceTipTime/Container_WaveTime/Text_Time",'TextMeshWrapper')
    self._containerWaveTime:SetActive(false)

    self._txtInstanceCDTime = self:GetChildComponent("Container_InstanceTipTime/Container_InstanceTime/Text_Time",'TextMeshWrapper')
end

function BattleSoulPanel:UpdateEndTime(endTime)
	GUIManager.ClosePanel(PanelsConfig.OperatingActivity)
	self._endTime = endTime - DateTimeUtil.GetServerTime()
	self._timerId = TimerHeap:AddSecTimer(0, 1, 0, function () self:UpdateTimer() end)
end

function BattleSoulPanel:UpdateWaveTime(endTime)
	self._showWaveTime = true
	self._waveTime = endTime - DateTimeUtil.GetServerTime()
	self._containerWaveTime:SetActive(true)
end

function BattleSoulPanel:UpdateQuitTime(endTime)
	self._showWaveTime = true
	self._waveTime = endTime - DateTimeUtil.GetServerTime()
	self._containerWaveTime:SetActive(true)
end

function BattleSoulPanel:UpdateTimer()
	if self._endTime then
		if self._endTime <= 0 then
			TimerHeap:DelTimer(self._timerId)
		else
			self._txtInstanceCDTime.text = DateTimeUtil:FormatFullTime(self._endTime,false,true)
			self._endTime = self._endTime - 1
		end
	end

	if self._showWaveTime then
		if self._waveTime <= 0 then
			self._showWaveTime = false
			self._containerWaveTime:SetActive(false)
		else
			self._txtWaveTime.text = DateTimeUtil:FormatFullTime(self._waveTime,false,true)
			self._waveTime = self._waveTime - 1
		end
	end
end

function BattleSoulPanel:UpdateRank(args)
	local listData = {}
	local myData = args[#args]
	self._txtMyRank.text = tostring(myData[1])
	self._txtMyDamage.text =  StringStyleUtil.GetLongNumberString(myData[3])
	for i=1,#args-1 do
		listData[i] = args[i]
	end
	self._listRank:SetDataList(listData)
end