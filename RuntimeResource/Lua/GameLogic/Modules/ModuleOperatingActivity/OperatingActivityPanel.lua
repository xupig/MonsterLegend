local OperatingActivityPanel = Class.OperatingActivityPanel(ClassTypes.BasePanel)

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager

require "Modules.ModuleOperatingActivity.ChildView.OperatingActivityViewBase"
require "Modules.ModuleOperatingActivity.ChildView.LoginGiftView"
require "Modules.ModuleOperatingActivity.ChildView.ChargeGiftView"
require "Modules.ModuleOperatingActivity.ChildView.FireworkView"
require "Modules.ModuleOperatingActivity.ChildView.CelebrationView"
require "Modules.ModuleOperatingActivity.ChildView.WeaponTurnUpView"
require "Modules.ModuleOperatingActivity.ChildView.HighActivityView"
require "Modules.ModuleOperatingActivity.ChildView.InstanceDoubleView"
require "Modules.ModuleOperatingActivity.ChildView.BattlefieldSoulView"

local LoginGiftView = ClassTypes.LoginGiftView
local ChargeGiftView = ClassTypes.ChargeGiftView
local FireworkView = ClassTypes.FireworkView
local CelebrationView = ClassTypes.CelebrationView
local WeaponTurnUpView = ClassTypes.WeaponTurnUpView
local HighActivityView = ClassTypes.HighActivityView
local InstanceDoubleView = ClassTypes.InstanceDoubleView
local BattlefieldSoulView = ClassTypes.BattlefieldSoulView
local public_config = GameWorld.public_config
local OperatingActivityManager = PlayerManager.OperatingActivityManager

-- 	   CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C = 4, --周累充
--     CHANNEL_ACTIVITY_ID_WEEK_LOGIN      = 5, --周活动登录
--     CHANNEL_ACTIVITY_ID_FIREWORK        = 6, --庆典烟花
--     CHANNEL_ACTIVITY_ID_HOLY_WEAPON     = 7, --神兵现世
--     CHANNEL_ACTIVITY_ID_EXCHANGE        = 8, --庆典兑换
--     CHANNEL_ACTIVITY_ID_DOUBLE 			= 9, 	--周双倍
--     CHANNEL_ACTIVITY_ID_BATTLE_SOUL     = 10, --战场之魂
--	   CHANNEL_ACTIVITY_ID_HI                  = 11, --hi点

function OperatingActivityPanel:Awake()
	self._viewIndex = {}
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C] = 2
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_WEEK_LOGIN] = 1
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_FIREWORK] = 4
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_HOLY_WEAPON] = 3
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_EXCHANGE] = 7
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_DOUBLE] = 5
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_BATTLE_SOUL] = 8
	self._viewIndex[public_config.CHANNEL_ACTIVITY_ID_HI] = 6
	self:InitCallBack()
	self:InitComps()
end

function OperatingActivityPanel:InitCallBack()
	-- self._openSoulCb = function (treasureType,itemIds)
	-- 	self:OpenSoul(treasureType,itemIds)
	-- end
	self._openTabCb = function ()
		self:UpdateFunctionOpen()
	end
end
--override 
function OperatingActivityPanel:OnShow(data)
	self:UpdateFunctionOpen()
	self:OnShowSelectTab(data)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_OPERATING_ACTIVITY_FUNCTION_OPEN,self._openTabCb)

end

function OperatingActivityPanel:OnClose()
	self:ClearDataInited()
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_OPERATING_ACTIVITY_FUNCTION_OPEN,self._openTabCb)
end

function OperatingActivityPanel:InitComps()
	self:InitView("Container_LoginGift", LoginGiftView,1,1)
	self:InitView("Container_ChargeGift", ChargeGiftView,1,2)
	self:InitView("Container_WeaponTurnUp", WeaponTurnUpView,1,3)
	self:InitView("Container_Firework", FireworkView,1,4)
	self:InitView("Container_InstanceDouble", InstanceDoubleView,1,5)
	self:InitView("Container_HighActivity", HighActivityView,1,6)
	self:InitView("Container_Celebration", CelebrationView,1,7)
	self:InitView("Container_BattlefieldSoul", BattlefieldSoulView,1,8)
end

function OperatingActivityPanel:StructingViewInit()
     return true
end

-- -- 0 没有  1 普通底板
function OperatingActivityPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--重写功能开启方法
function OperatingActivityPanel:UpdateFunctionOpen()
	for k,v in pairs(self._viewIndex) do
		local isOpen = OperatingActivityManager:GetOpenState(k)
		self:SetSecTabActive(v,isOpen)
	end
end

--还原数据初始化状态
function OperatingActivityPanel:ClearDataInited()
	for k,view in pairs(self._componentMap) do
		view:ClearDataInited()
	end
end

