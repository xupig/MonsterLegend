-- OperatingActivityViewBase.lua
local OperatingActivityViewBase = Class.OperatingActivityViewBase(ClassTypes.BaseComponent)

OperatingActivityViewBase.interface = GameConfig.ComponentsConfig.Component
OperatingActivityViewBase.classPath = "Modules.ModuleOperatingActivity.ChildView.OperatingActivityViewBase"

local OperatingActivityManager = PlayerManager.OperatingActivityManager

function OperatingActivityViewBase:Awake()
	self._dataInited = false
    self:InitCallBack()
	self:InitBaseComps()
end

function OperatingActivityViewBase:InitBaseComps()
	self._txtTime = self:GetChildComponent("Text_Time",'TextMeshWrapper')
	self._txtDesc = self:GetChildComponent("Text_Desc",'TextMeshWrapper')
	self:UpdateOpenTimeText()
	self:InitComps()
end


function OperatingActivityViewBase:UpdateOpenTimeText()
	if self._activityId then
		self._txtTime.text = OperatingActivityManager:GetOpenTimeStr(self._activityId)
	else
		self._txtTime.text = ""
	end
end

--for override
function OperatingActivityViewBase:InitCallBack()
end
--for override
function OperatingActivityViewBase:InitComps()

end

--for override
function OperatingActivityViewBase:ShowView()
	
end

--for override
function OperatingActivityViewBase:CloseView()
	
end

--
function OperatingActivityViewBase:ClearDataInited()
	self._dataInited = false
end