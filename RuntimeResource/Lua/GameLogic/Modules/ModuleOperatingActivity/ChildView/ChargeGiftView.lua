-- ChargeGiftView.lua
local ChargeGiftView = Class.ChargeGiftView(ClassTypes.OperatingActivityViewBase)

ChargeGiftView.interface = GameConfig.ComponentsConfig.Component
ChargeGiftView.classPath = "Modules.ModuleOperatingActivity.ChildView.ChargeGiftView"

require "Modules.ModuleOperatingActivity.ChildComponent.ChargeGiftAwardListItem"
local ChargeGiftAwardListItem = ClassTypes.ChargeGiftAwardListItem
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local DateTimeUtil = GameUtil.DateTimeUtil
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper

function ChargeGiftView:Awake()
    self._activityId = public_config.CHANNEL_ACTIVITY_ID_WEEKLY_CUMULATIVE_C
   	self._base.Awake(self)
end

function ChargeGiftView:InitCallBack()
    self._timerCb = function ()
        self:UpdateTimer()
    end

    self._updateListCb = function ()
        self:UpdateList()
    end
end

function ChargeGiftView:InitBaseComps()
    self:InitComps()
end

function ChargeGiftView:InitComps()
	self._txtTime = self:GetChildComponent("Text_Time",'TextMeshWrapper')

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(ChargeGiftAwardListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 0)
    self._listAward:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._listData = OperatingActivityDataHelper:GetTiringChargeCfgList()
end


function ChargeGiftView:ShowView()
	local activityTimes = GameWorld.Player().channel_activity_times[self._activityId]
    if activityTimes then
        local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
        self._timeleft = endTime - DateTimeUtil.GetServerTime()
        self._timerId = TimerHeap:AddSecTimer(0, 1, 0, self._timerCb)
    end
    self:UpdateList()

    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_CHARGE_REWARD,self._updateListCb)
    
end

function ChargeGiftView:UpdateTimer()
    if self._timeleft <= 0 then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
    end
    self._timeleft = self._timeleft - 1
    self._txtTime.text = DateTimeUtil:FormatFullTime(self._timeleft)
end

local function Sort(a,b)
    local states = GameWorld.Player().weekly_cumulative_charge_refund_info
    local aState = states[a.id] ~= nil
    local bState = states[b.id] ~= nil
    if aState == bState then
        return a.id < b.id
    else
        return bState
    end
end

function ChargeGiftView:UpdateList()
    table.sort(self._listData,Sort)
    self._listAward:SetDataList(self._listData)
end

function ChargeGiftView:CloseView()
	if self._timerId then
       TimerHeap:DelTimer(self._timerId)
       self._timerId = nil
    end

    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_CHARGE_REWARD,self._updateListCb)
    
end