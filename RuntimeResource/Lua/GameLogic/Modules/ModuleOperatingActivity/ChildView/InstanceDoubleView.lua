-- InstanceDoubleView.lua
local InstanceDoubleView = Class.InstanceDoubleView(ClassTypes.OperatingActivityViewBase)

InstanceDoubleView.interface = GameConfig.ComponentsConfig.Component
InstanceDoubleView.classPath = "Modules.ModuleOperatingActivity.ChildView.InstanceDoubleView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper
local GUIManager = GameManager.GUIManager
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper

function InstanceDoubleView:Awake()
	self._activityId = public_config.CHANNEL_ACTIVITY_ID_DOUBLE
    self._base.Awake(self)
end

function InstanceDoubleView:InitCallBack()
end

function InstanceDoubleView:InitComps()
	self._containerToday = self:FindChildGO("Container_TodayDouble")
	self._txtInstanceName = self:GetChildComponent("Container_TodayDouble/Text_InstanceName",'TextMeshWrapper')

	self._containerOpen = self:FindChildGO("Container_Open")
	self._btnGo = self:FindChildGO("Container_Open/Button_Go")
	self._csBH:AddClick(self._btnGo,function() self:OnClickGo() end)

	
	self._containerNotOpen = self:FindChildGO("Container_NotOpen")

	self._txtCD = self:GetChildComponent("Text_CD",'TextMeshWrapper')
	self._openTimeCfg = GlobalParamsHelper.GetParamValue(910)
end

function InstanceDoubleView:ShowView()
	if not self._dataInited then
		--local  = 1
		local activityTimes = GameWorld.Player().channel_activity_times[self._activityId]
		local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
		self._whichDay = math.min(DateTimeUtil.GetDaysGoThrough(startTime),3)--现在是第几天,容错设置如果大于3就变成3
		local nowDate = DateTimeUtil.Now()
		local openTime = self._openTimeCfg[self._whichDay]
		if openTime then
			local nowDateStr = DateTimeUtil.FormatFullDate(nowDate,true,false,true)
			local tab = {}
			table.insert(tab,nowDateStr)
			table.insert(tab," ")
			table.insert(tab,string.format("%02d:%02d", openTime[1], openTime[2]))
			table.insert(tab,"——")
			table.insert(tab,nowDateStr)
			table.insert(tab," ")
			table.insert(tab,string.format("%02d:%02d", openTime[3], openTime[4]))
			self._txtTime.text = table.concat(tab)
		end
		self._dataInited = true
		self._cfgList = OperatingActivityDataHelper:GetDoubleRewardCfgList()
	end

	--第一天特殊处理
	local openTime = self._openTimeCfg[self._whichDay]
	local nowDate = DateTimeUtil.Now()
	if self._whichDay == 1 then
		self._txtDesc.text = LanguageDataHelper.CreateContent(81578)
		if nowDate.hour < openTime[1] then
			self._timeleft  = openTime[1]*3600 + openTime[2]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
			self._containerOpen:SetActive(false)
			self._containerNotOpen:SetActive(true)
		else
			self._timeleft = openTime[3]*3600 + openTime[4]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
			self._containerOpen:SetActive(true)
			self._containerNotOpen:SetActive(false)
		end
		self._containerToday:SetActive(false)
	else
		self._timeleft = openTime[3]*3600 + openTime[4]*60 - nowDate.hour*3600 - nowDate.min*60 - nowDate.sec
		self._txtDesc.text = LanguageDataHelper.CreateContent(81579)
		self._containerOpen:SetActive(true)
		self._containerNotOpen:SetActive(false)
		self._containerToday:SetActive(true)

		local tab = {}
		for i=1,#self._cfgList do
			local rewardTimes = self._cfgList[i].day_reward[self._whichDay]
			if rewardTimes == 2 then
				local nameId = FunctionOpenDataHelper:GetFunctionOpenName(self._cfgList[i].follow[1][1])
				local str = LanguageDataHelper.CreateContent(nameId)
				table.insert(tab,str)
			end
		end

		self._txtInstanceName.text = table.concat(tab,"、")
	end
	
	self._timerId = TimerHeap:AddSecTimer(0, 1, 0, function () self:UpdateTimer() end)
end

function InstanceDoubleView:UpdateTimer()
	if self._timeleft then
		if self._timeleft <= 0 then
			TimerHeap:DelTimer(self._timerId)
			self._timerId = nil
		else
			self._txtCD.text = DateTimeUtil:FormatFullTime(self._timeleft)
			self._timeleft = self._timeleft - 1
		end
	end
end

function InstanceDoubleView:OnClickGo()
	if self._whichDay == 1 then
		GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_EXP_INSTANCE)
	else
		for i=1,#self._cfgList do
			local rewardTimes = self._cfgList[i].day_reward[self._whichDay]
			if rewardTimes == 2 then
				local funcId = self._cfgList[i].follow[1][1]
				if FunctionOpenDataHelper:CheckFunctionOpen(funcId) then
					GUIManager.ShowPanelByFunctionId(funcId)
					return
				end
			end
		end
		GUIManager.ShowText(2,LanguageDataHelper.CreateContent(81635))
	end
end

function InstanceDoubleView:CloseView()
	if self._timerId then
		TimerHeap:DelTimer(self._timerId)
		self._timerId = nil
	end
end

--不在此方法处理时间
function InstanceDoubleView:UpdateOpenTimeText()

end