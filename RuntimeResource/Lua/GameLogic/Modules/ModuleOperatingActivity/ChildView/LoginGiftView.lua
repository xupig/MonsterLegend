-- LoginGiftView.lua
local LoginGiftView = Class.LoginGiftView(ClassTypes.OperatingActivityViewBase)

LoginGiftView.interface = GameConfig.ComponentsConfig.Component
LoginGiftView.classPath = "Modules.ModuleOperatingActivity.ChildView.LoginGiftView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

require "Modules.ModuleOperatingActivity.ChildComponent.LoginGiftAwardListItem"
local LoginGiftAwardListItem = ClassTypes.LoginGiftAwardListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper

function LoginGiftView:Awake()
    self._activityId = public_config.CHANNEL_ACTIVITY_ID_WEEK_LOGIN
  	self._base.Awake(self)
end

function LoginGiftView:InitCallBack()
    self._updateListCb = function ()
        self:UpdateList()
    end
end


function LoginGiftView:InitComps()
	self._txtDesc.text = LanguageDataHelper.CreateContent(81582)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(LoginGiftAwardListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 0)
    self._listAward:SetDirection(UIList.DirectionTopToDown, 1, 1)
    --self._listAward:SetItemClickedCB()
end

function LoginGiftView:ShowView()
    if not self._dataInited then
        self._listData = OperatingActivityDataHelper:GetLoginRewardCfgList()
        self:UpdateList()
        self._dataInited = true
    end

    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_LOGIN_REWARD,self._updateListCb)
end

function LoginGiftView:UpdateList()
    if self._listData then
        self._listAward:SetDataList(self._listData)
    end
end

function LoginGiftView:CloseView()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_LOGIN_REWARD,self._updateListCb)
end