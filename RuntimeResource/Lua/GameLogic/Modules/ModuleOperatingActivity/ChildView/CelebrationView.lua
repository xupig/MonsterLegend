-- CelebrationView.lua
local CelebrationView = Class.CelebrationView(ClassTypes.OperatingActivityViewBase)

CelebrationView.interface = GameConfig.ComponentsConfig.Component
CelebrationView.classPath = "Modules.ModuleOperatingActivity.ChildView.CelebrationView"

require "Modules.ModuleOperatingActivity.ChildComponent.CelebrationAwardListItem"
local CelebrationAwardListItem = ClassTypes.CelebrationAwardListItem
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper

function CelebrationView:Awake()
    self._activityId = public_config.CHANNEL_ACTIVITY_ID_EXCHANGE
    self._base.Awake(self)
end

function CelebrationView:InitCallBack()
    self._updateListCb = function ()
        self:UpdateList()
    end
end

function CelebrationView:InitComps()
	self._txtDesc.text = LanguageDataHelper.CreateContent(81568)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(CelebrationAwardListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 10)
    self._listAward:SetDirection(UIList.DirectionLeftToRight, 2, 10)
end

function CelebrationView:ShowView()
    if not self._dataInited then
        self._listData = OperatingActivityDataHelper:GetCelebrationCfgList()
        self:UpdateList()
        self._dataInited = true
    end
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_CELEBRATION_REWARD,self._updateListCb)
end

function CelebrationView:UpdateList()
    if self._listData then
        self._listAward:SetDataList(self._listData)
    end
end

function CelebrationView:CloseView()
	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_CELEBRATION_REWARD,self._updateListCb)
end