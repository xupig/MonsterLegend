-- FireworkView.lua
local FireworkView = Class.FireworkView(ClassTypes.OperatingActivityViewBase)

FireworkView.interface = GameConfig.ComponentsConfig.Component
FireworkView.classPath = "Modules.ModuleOperatingActivity.ChildView.FireworkView"

require "Modules.ModuleOperatingActivity.ChildComponent.SimpleItemListItem"
local SimpleItemListItem = ClassTypes.SimpleItemListItem
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIList = ClassTypes.UIList
local public_config = GameWorld.public_config
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local ItemDataHelper = GameDataHelper.ItemDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local SettingType = GameConfig.SettingType

function FireworkView:Awake()
	self._activityId = public_config.CHANNEL_ACTIVITY_ID_FIREWORK
    self._base.Awake(self)
end

function FireworkView:InitCallBack()
	self._requestCb = function ()
		self:RequestBuy()
	end
end

function FireworkView:InitComps()
	self._txtDesc.text = LanguageDataHelper.CreateContent(81575)

	self._txtItemName = self:GetChildComponent("Text_FireworkName",'TextMeshWrapper')
	--self._txtMoneyCount = self:GetChildComponent("Text_MoneyCount",'TextMeshWrapper')

	self._btnToCharge = self:FindChildGO("Button_Charge")
	self._csBH:AddClick(self._btnToCharge,function() self:OnClickCharge() end)

	self._btnBuy = self:FindChildGO("Button_Buy")
	self._csBH:AddClick(self._btnBuy,function() self:OnClickBuy() end)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(SimpleItemListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 10)
    self._listAward:SetDirection(UIList.DirectionLeftToRight, 4, 10)

    local parent = self:FindChildGO("Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	local itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	itemIcon:ActivateTipsById()

   	local fireworkId = GlobalParamsHelper.GetParamValue(913)
   	self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(fireworkId)
   	itemIcon:SetItem(fireworkId)

   	local diamondIcon = self:FindChildGO("Container_Diamond")
	GameWorld.AddIcon(diamondIcon, ItemDataHelper.GetIcon(fireworkId))
end

function FireworkView:OnClickCharge()
	GUIManager.ShowPanelByFunctionId(43)
end

function FireworkView:OnClickBuy()
	local args = LanguageDataHelper.GetArgsTable()
	args["0"] = self._cost
	args["1"] = "1"
	local text = LanguageDataHelper.CreateContentWithArgs(81622,args)

	GUIManager.ShowDailyTips(SettingType.FIREWORK, self._requestCb, nil, text)
end

function FireworkView:RequestBuy()
	OperatingActivityManager:RequestBuyFirework()
end

function FireworkView:ShowView()
	if not self._dataInited then
		local worldLevel = GameWorld.Player().activity_firework_world_level
		local cfg = OperatingActivityDataHelper:GetFireworksCfgByWorldLevel(worldLevel)
		self._cost = cfg.cost
		--self._txtMoneyCount.text = "[0/1]"--tostring(cfg.cost)
		local vocationGroup = math.floor(GameWorld.Player().vocation/100)
		local reward = cfg.reward[vocationGroup]
		local listData = {}
		for i=1,#reward,5 do
			table.insert(listData,{reward[i],reward[i+1]})
		end
		self._listAward:SetDataList(listData)
		self._dataInited = true
	end
end

function FireworkView:CloseView()
	
end