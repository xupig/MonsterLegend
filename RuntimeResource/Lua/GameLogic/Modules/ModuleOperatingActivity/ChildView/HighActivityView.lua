-- HighActivityView.lua
local HighActivityView = Class.HighActivityView(ClassTypes.OperatingActivityViewBase)

HighActivityView.interface = GameConfig.ComponentsConfig.Component
HighActivityView.classPath = "Modules.ModuleOperatingActivity.ChildView.HighActivityView"

require "Modules.ModuleOperatingActivity.ChildComponent.HighActivityAwardListItem"
local HighActivityAwardListItem = ClassTypes.HighActivityAwardListItem

require "Modules.ModuleOperatingActivity.ChildComponent.HighActivityListItem"
local HighActivityListItem = ClassTypes.HighActivityListItem

local UIList = ClassTypes.UIList
--local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper
local DateTimeUtil = GameUtil.DateTimeUtil
local public_config = GameWorld.public_config

function HighActivityView:Awake()
    self._activityId = public_config.CHANNEL_ACTIVITY_ID_HI
    self._base.Awake(self)
end

function HighActivityView:InitCallBack()
    self._timerCb = function ()
        self:UpdateTimer()
    end
    
    self._updateAwardCb = function ()
        self:UpdateAward()
    end

    self._updateActivityCb = function ()
        self:UpdateActivity()
    end
end

function HighActivityView:InitBaseComps()
    self:InitComps()
end

function HighActivityView:InitComps()
    self._txtTime = self:GetChildComponent("Text_Time",'TextMeshWrapper')
	self._txtCount = self:GetChildComponent("Text_Count",'TextMeshWrapper')

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(HighActivityAwardListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 0)
    self._listAward:SetDirection(UIList.DirectionTopToDown, 1, 1)

    local scrollView1, list1 = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Activity")
    self._listActivity = list1
    self._scrollView1 = scrollView1
    self._listActivity:SetItemType(HighActivityListItem)
    self._listActivity:SetPadding(0, 0, 0, 0)
    self._listActivity:SetGap(10, 0)
    self._listActivity:SetDirection(UIList.DirectionTopToDown, 1, 1)

    self._activtyListData = OperatingActivityDataHelper:GetHiPointCfgList()
    self._awardListData = OperatingActivityDataHelper:GetHiPointRewardCfgList()
end

function HighActivityView:ShowView()
    self:UpdateAward()
    self:UpdateActivity()
    local activityTimes = GameWorld.Player().channel_activity_times[self._activityId]
    if activityTimes then
        local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
        self._timeleft = endTime - DateTimeUtil.GetServerTime()
        self._timerId = TimerHeap:AddSecTimer(0, 1, 0, self._timerCb)
    end
	EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_HI_POINT_REWARD,self._updateAwardCb)
    EventDispatcher:AddEventListener(GameEvents.UIEVENT_UPDATE_HI_POINT_ACTIVITY,self._updateActivityCb)
end

local function SortActiveAward(a,b)
    local states = GameWorld.Player().hi_point_reward
    local aState = states[a.id] ~= nil
    local bState = states[b.id] ~= nil
    if aState == bState then
        return a.id < b.id
    else
        return bState
    end
end

function HighActivityView:UpdateAward()
    --LoggerHelper.Error('HighActivityView:UpdateAward()')
    self._txtCount.text = tostring(GameWorld.Player().hi_point)
    table.sort(self._awardListData,SortActiveAward)
    self._listAward:SetDataList(self._awardListData)
end

local function SortActive(a,b)
    local times = GameWorld.Player().hi_point_times
    local aFinish = (times[a.id] or 0) >= a.times
    local bFinish = (times[b.id] or 0) >= b.times
    if aFinish == bFinish then
        return a.id < b.id
    else
        return bFinish
    end
end

function HighActivityView:UpdateActivity()
    table.sort(self._activtyListData,SortActive)
    self._listActivity:SetDataList(self._activtyListData)
end

function HighActivityView:UpdateTimer()
    if self._timeleft <= 0 then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
    end
    self._timeleft = self._timeleft - 1
    self._txtTime.text = DateTimeUtil:FormatFullTime(self._timeleft)
end

function HighActivityView:CloseView()
    if self._timerId then
       TimerHeap:DelTimer(self._timerId)
       self._timerId = nil
    end

	EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_HI_POINT_REWARD,self._updateAwardCb)
    EventDispatcher:RemoveEventListener(GameEvents.UIEVENT_UPDATE_HI_POINT_ACTIVITY,self._updateActivityCb)
end