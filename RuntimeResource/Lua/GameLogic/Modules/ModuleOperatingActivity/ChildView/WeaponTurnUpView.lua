-- WeaponTurnUpView.lua
local WeaponTurnUpView = Class.WeaponTurnUpView(ClassTypes.OperatingActivityViewBase)

WeaponTurnUpView.interface = GameConfig.ComponentsConfig.Component
WeaponTurnUpView.classPath = "Modules.ModuleOperatingActivity.ChildView.WeaponTurnUpView"

require "Modules.ModuleOperatingActivity.ChildComponent.WeaponTurnUpAwardListItem"
local WeaponTurnUpAwardListItem = ClassTypes.WeaponTurnUpAwardListItem
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local OperatingActivityDataHelper = GameDataHelper.OperatingActivityDataHelper
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local DateTimeUtil = GameUtil.DateTimeUtil

function WeaponTurnUpView:Awake()
    self._activityId = public_config.CHANNEL_ACTIVITY_ID_HOLY_WEAPON
    self._base.Awake(self)
    --
end

function WeaponTurnUpView:InitCallBack()
    self._updateInfoCb = function (args)
        self:UpdateInfo(args)
    end

    self._timerCb = function ()
        self:UpdateTimer()
    end
end

function WeaponTurnUpView:InitComps()
	self._txtDesc.text = LanguageDataHelper.CreateContent(81514)
	self._txtTime = self:GetChildComponent("Text_CD",'TextMeshWrapper')

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Award")
    self._listAward = list
    self._scrollView = scrollView
    self._listAward:SetItemType(WeaponTurnUpAwardListItem)
    self._listAward:SetPadding(0, 0, 0, 0)
    self._listAward:SetGap(10, 0)
    self._listAward:SetDirection(UIList.DirectionTopToDown, 1, 1)
end

function WeaponTurnUpView:ShowView()
	if not self._dataInited then
        --LoggerHelper.Error("WeaponTurnUpView:ShowView()")
        OperatingActivityManager:RequestGetWeaponRankInfo()
        self._dataInited = true
    end
    local activityTimes = GameWorld.Player().channel_activity_times[self._activityId]
    if activityTimes then
        local endTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_END_TIME]
        self._timeleft = endTime - DateTimeUtil.GetServerTime()
        self._timerId = TimerHeap:AddSecTimer(0, 1, 0, self._timerCb)
    end
    EventDispatcher:AddEventListener(GameEvents.HOLY_WEAPON_RANK_UPDATE_INFO,self._updateInfoCb)
end

function WeaponTurnUpView:UpdateTimer()
    if self._timeleft <= 0 then
        TimerHeap:DelTimer(self._timerId)
        self._timerId = nil
    end
    self._timeleft = self._timeleft - 1
    self._txtTime.text = DateTimeUtil:FormatFullTime(self._timeleft)
end

function WeaponTurnUpView:CloseView()
	if self._timerId then
       TimerHeap:DelTimer(self._timerId)
       self._timerId = nil
    end
    EventDispatcher:RemoveEventListener(GameEvents.HOLY_WEAPON_RANK_UPDATE_INFO,self._updateInfoCb)
end

function WeaponTurnUpView:UpdateInfo(args)
    local worldLevel = args[1]
    --LoggerHelper.Error("worldLevel"..worldLevel)
    local names = args[2] or {}
    local cfgList = OperatingActivityDataHelper:GetWeaponGodsCfgList(worldLevel)
    local listData = {}
    for i=1,5 do
        listData[i] = {cfgList[i],names[i]}
    end
    self._listAward:SetDataList(listData)
end