-- BattlefieldSoulView.lua
local BattlefieldSoulView = Class.BattlefieldSoulView(ClassTypes.OperatingActivityViewBase)

BattlefieldSoulView.interface = GameConfig.ComponentsConfig.Component
BattlefieldSoulView.classPath = "Modules.ModuleOperatingActivity.ChildView.BattlefieldSoulView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local OperatingActivityManager = PlayerManager.OperatingActivityManager

function BattlefieldSoulView:Awake()
	self._activityId = public_config.CHANNEL_ACTIVITY_ID_BATTLE_SOUL
    self._base.Awake(self)
end

function BattlefieldSoulView:InitCallBack()
end

function BattlefieldSoulView:InitComps()
	self._txtDesc.text = LanguageDataHelper.CreateContent(81572)
	self._btnGo = self:FindChildGO("Button_Go")
	self._csBH:AddClick(self._btnGo,function() self:OnClickGo() end)
end

function BattlefieldSoulView:OnClickGo()
	OperatingActivityManager:RequestBattleSoulEnter()
end

function BattlefieldSoulView:ShowView()
	
end

function BattlefieldSoulView:CloseView()
	
end