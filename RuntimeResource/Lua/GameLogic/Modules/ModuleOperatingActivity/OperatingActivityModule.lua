OperatingActivityModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function OperatingActivityModule.Init()
	GUIManager.AddPanel(PanelsConfig.OperatingActivity,"Panel_OperatingActivity",GUILayer.LayerUIPanel,"Modules.ModuleOperatingActivity.OperatingActivityPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
	GUIManager.AddPanel(PanelsConfig.BattleSoul,"Panel_BattleSoul",GUILayer.LayerUIPanel,"Modules.ModuleOperatingActivity.BattleSoulPanel",true,PanelsConfig.EXTEND_TO_FIT, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return OperatingActivityModule