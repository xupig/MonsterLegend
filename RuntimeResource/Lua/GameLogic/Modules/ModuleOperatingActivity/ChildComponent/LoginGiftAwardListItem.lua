-- LoginGiftAwardListItem.lua
local UIListItem = ClassTypes.UIListItem
local LoginGiftAwardListItem = Class.LoginGiftAwardListItem(UIListItem)

LoginGiftAwardListItem.interface = GameConfig.ComponentsConfig.PointableComponent
LoginGiftAwardListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.LoginGiftAwardListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local DateTimeUtil = GameUtil.DateTimeUtil
local public_config = GameWorld.public_config

function LoginGiftAwardListItem:Awake()
	self._txtDate = self:GetChildComponent("Container_content/Text_Date",'TextMeshWrapper')
	
	self._btnGet = self:FindChildGO("Container_content/Button_Get")
	self._btnGetWrapper = self._btnGet:GetComponent("ButtonWrapper")
	self._csBH:AddClick(self._btnGet,function() self:GetAward() end)

	self._imgGot = self:FindChildGO("Container_content/Image_Got")
	self._imgMiss = self:FindChildGO("Container_content/Image_Miss")
end

--override
function LoginGiftAwardListItem:OnRefreshData(data)
	self._day = data.day
	local state = GameWorld.Player().week_login_info[self._day]
	if state == 1 then
		self._btnGet:SetActive(false)
		self._imgGot:SetActive(true)
		self._imgMiss:SetActive(false)
	elseif state == 0 then
		self._btnGetWrapper.interactable = true
		self._btnGet:SetActive(true)
		self._imgGot:SetActive(false)
		self._imgMiss:SetActive(false)
	else
		if self._isMiss then
			self._imgMiss:SetActive(true)
			self._btnGet:SetActive(false)
		else
			self._btnGet:SetActive(true)
			self._imgMiss:SetActive(false)
			self._btnGetWrapper.interactable = false
		end
		self._imgGot:SetActive(false)
	end

	--信息未初始化
	if self._awardItems == nil then
		local activityTimes = GameWorld.Player().channel_activity_times[public_config.CHANNEL_ACTIVITY_ID_WEEK_LOGIN]
		local startTime = activityTimes[public_config.CHANNEL_ACTIVITY_KEY_BEGIN_TIME]
		local thisTime = startTime + (self._day-1)*86400
		self._isMiss = self._day < DateTimeUtil.GetDaysGoThrough(startTime)
		local thisDate = DateTimeUtil.SomeDay(thisTime)
		self._txtDate.text = DateTimeUtil.FormatFullDate(thisDate,true,false,true)
		self._awardItems = {}
		local vocationGroup = math.floor(GameWorld.Player().vocation/100)
		local reward = data.reward[vocationGroup]
		for i=1,#reward,3 do
			local index = math.ceil(i/3)
			self:AddAwardItem(index)
			self._awardItems[index]:SetItem(reward[i],reward[i+1])
		end
	end
end

function LoginGiftAwardListItem:AddAwardItem(index)
	local parent = self:FindChildGO("Container_content/Container_Items/Container_Icon"..index)
   	local itemGo = GUIManager.AddItem(parent, 1)
   	local itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	itemIcon:ActivateTipsById()
   	self._awardItems[index] = itemIcon
end

function LoginGiftAwardListItem:GetAward()
	OperatingActivityManager:RequestGetLoginAward(self._day)
end