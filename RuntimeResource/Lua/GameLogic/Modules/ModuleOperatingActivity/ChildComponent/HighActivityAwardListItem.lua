-- HighActivityAwardListItem.lua
local UIListItem = ClassTypes.UIListItem
local HighActivityAwardListItem = Class.HighActivityAwardListItem(UIListItem)

HighActivityAwardListItem.interface = GameConfig.ComponentsConfig.PointableComponent
HighActivityAwardListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.HighActivityAwardListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local OperatingActivityManager = PlayerManager.OperatingActivityManager

function HighActivityAwardListItem:Awake()
	self._txtInfo = self:GetChildComponent("Container_content/Text_Info",'TextMeshWrapper')

	self._btnGet = self:FindChildGO("Container_content/Button_Get")
	self._csBH:AddClick(self._btnGet,function() self:GetAward() end)

	self._imgGot = self:FindChildGO("Container_content/Image_Got")
	self._imgNotMeet = self:FindChildGO("Container_content/Image_NotMeet")
end

--override
function HighActivityAwardListItem:OnRefreshData(data)
	self._id = data.id
	local args = LanguageDataHelper.GetArgsTable()
	args["0"] = data.cost_point
	self._txtInfo.text = LanguageDataHelper.CreateContentWithArgs(81522,args)

	local state = GameWorld.Player().hi_point_reward[self._id]
	if state then
		self._btnGet:SetActive(false)
		self._imgGot:SetActive(true)
		self._imgNotMeet:SetActive(false)
	elseif data.cost_point <= (GameWorld.Player().hi_point or 0)  then
		self._btnGet:SetActive(true)
		self._imgGot:SetActive(false)
		self._imgNotMeet:SetActive(false)
	else
		self._btnGet:SetActive(false)
		self._imgGot:SetActive(false)
		self._imgNotMeet:SetActive(true)
	end

	--信息未初始化
	if self._awardItems == nil then
		self._awardItems = {}
		local vocationGroup = math.floor(GameWorld.Player().vocation/100)
		local reward = data.reward[vocationGroup]
		for i=1,#reward,3 do
			local index = math.ceil(i/3)
			self:AddAwardItem(index)
			self._awardItems[index]:SetItem(reward[i],reward[i+1])
		end
	end
end

function HighActivityAwardListItem:AddAwardItem(index)
	local parent = self:FindChildGO("Container_content/Container_Items/Container_Icon"..index)
   	local itemGo = GUIManager.AddItem(parent, 1)
   	local itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	itemIcon:ActivateTipsById()
   	self._awardItems[index] = itemIcon
end

function HighActivityAwardListItem:GetAward()
	OperatingActivityManager:RequestGetHiAward(self._id)
end