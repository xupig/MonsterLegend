-- WeaponTurnUpAwardListItem.lua
local UIListItem = ClassTypes.UIListItem
local WeaponTurnUpAwardListItem = Class.WeaponTurnUpAwardListItem(UIListItem)

WeaponTurnUpAwardListItem.interface = GameConfig.ComponentsConfig.Component
WeaponTurnUpAwardListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.WeaponTurnUpAwardListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager

function WeaponTurnUpAwardListItem:Awake()
	local parent = self:FindChildGO("Container_content/Container_Icon")
	local itemGo = GUIManager.AddItem(parent, 1)
	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
	self._itemIcon:ActivateTipsById()

	self._txtPlayer = self:GetChildComponent("Container_content/Text_Player",'TextMeshWrapper')

	self._containerRank = self:FindChildGO("Container_content/Container_Rank")
end

function WeaponTurnUpAwardListItem:OnDestroy()
	self._itemIcon = nil
end

--override
function WeaponTurnUpAwardListItem:OnRefreshData(data)
   local name = data[2]
	local cfg = data[1]
	local iconId = self:GetIndex() + 13515
	GameWorld.AddIcon(self._containerRank,iconId)
   if name then
      self._txtPlayer.text = name
   else
      self._txtPlayer.text = ""
   end
   local vocationGroup = math.floor(GameWorld.Player().vocation/100)
   local reward = cfg.reward[vocationGroup]
	self._itemIcon:SetItem(reward[1],reward[2])
end