-- BattleSoulRankListItem.lua
local UIListItem = ClassTypes.UIListItem
local BattleSoulRankListItem = Class.BattleSoulRankListItem(UIListItem)

BattleSoulRankListItem.interface = GameConfig.ComponentsConfig.Component
BattleSoulRankListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.BattleSoulRankListItem"

local StringStyleUtil = GameUtil.StringStyleUtil

function BattleSoulRankListItem:Awake()
	self._txtRank = self:GetChildComponent("Text_Rank",'TextMeshWrapper')
	self._txtName = self:GetChildComponent("Text_Name",'TextMeshWrapper')
	self._txtDamage = self:GetChildComponent("Text_Damage",'TextMeshWrapper')
end

function BattleSoulRankListItem:OnDestroy()
	
end

--override
function BattleSoulRankListItem:OnRefreshData(data)
	self._txtRank.text = tostring(data[1])
	self._txtName.text = data[2]
	self._txtDamage.text = StringStyleUtil.GetLongNumberString(data[3])
end