-- SimpleItemListItem.lua
local UIListItem = ClassTypes.UIListItem
local SimpleItemListItem = Class.SimpleItemListItem(UIListItem)

SimpleItemListItem.interface = GameConfig.ComponentsConfig.Component
SimpleItemListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.SimpleItemListItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function SimpleItemListItem:Awake()
	self._itemManager = ItemManager()
	self._itemContainer = self:FindChildGO("Container_content/Container_Icon")
	self._iconContainer = self._itemManager:GetLuaUIComponent(nil, self._itemContainer)
	self._iconContainer:ActivateTipsById()
end

function SimpleItemListItem:OnDestroy()
	self._iconContainer = nil
end

--override
function SimpleItemListItem:OnRefreshData(data)
	local itemId = data[1]
	local itemCount = data[2]
	self._iconContainer:SetItem(itemId,itemCount)
end