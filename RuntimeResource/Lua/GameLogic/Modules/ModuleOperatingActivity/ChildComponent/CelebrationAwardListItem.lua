-- CelebrationAwardListItem.lua
local UIListItem = ClassTypes.UIListItem
local CelebrationAwardListItem = Class.CelebrationAwardListItem(UIListItem)

CelebrationAwardListItem.interface = GameConfig.ComponentsConfig.PointableComponent
CelebrationAwardListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.CelebrationAwardListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local ItemDataHelper = GameDataHelper.ItemDataHelper
local OperatingActivityManager = PlayerManager.OperatingActivityManager
local bagData = PlayerManager.PlayerDataManager.bagData
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig

function CelebrationAwardListItem:Awake()
	self._txtItemName = self:GetChildComponent("Container_content/Text_ItemName",'TextMeshWrapper')
	self._txtCanGet = self:GetChildComponent("Container_content/Text_CanGet",'TextMeshWrapper')
	self._txtCount = self:GetChildComponent("Container_content/Text_Count",'TextMeshWrapper')

	self._btnGet = self:FindChildGO("Container_content/Button_Get")
	self._btnGetWrapper = self._btnGet:GetComponent("ButtonWrapper")
	self._csBH:AddClick(self._btnGet,function() self:GetAward() end)

	local parent = self:FindChildGO("Container_content/Container_Icon")
   	local itemGo = GUIManager.AddItem(parent, 1)
   	self._itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	--self._itemIcon:ActivateTipsById()

   	local parent1 = self:FindChildGO("Container_content/Container_AwardIcon")
   	local itemGo1 = GUIManager.AddItem(parent1, 1)
   	self._awardItemIcon = UIComponentUtil.AddLuaUIComponent(itemGo1, ItemGrid)
   	self._awardItemIcon:ActivateTipsById()
end

--override
function CelebrationAwardListItem:OnRefreshData(data)
	self._id = data.id
	local itemEnough = false
	for needCount,itemIds in pairs(data.cost) do
		local itemId = itemIds[1]
		self._itemIcon:SetItem(itemId)
		local bagCount = 0
		for i=1,#itemIds do
			bagCount = bagCount + bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(itemIds[i])
		end
			
		local str = bagCount.."/"..needCount
		itemEnough = bagCount >= needCount
		if itemEnough then
			self._txtCount.text = str
        else
            self._txtCount.text = StringStyleUtil.GetColorStringWithColorId(str, ItemConfig.ItemForbidden)
        end
	end

	local vocationGroup = math.floor(GameWorld.Player().vocation/100)
	local reward = data.reward[vocationGroup]
	local awardItemId = reward[1]
	local awardItemCount = reward[2]
	self._awardItemIcon:SetItem(awardItemId,awardItemCount)
	self._txtItemName.text = ItemDataHelper.GetItemNameWithColor(awardItemId)
	
	local alreadyGot = GameWorld.Player().exchange_info[self._id] or 0
	local canGetCount = data.exchange - alreadyGot
	self._txtCanGet.text = tostring(canGetCount)

	self._btnGetWrapper.interactable = itemEnough and canGetCount > 0
end


function CelebrationAwardListItem:GetAward()
	OperatingActivityManager:RequestGetCelebrationAward(self._id)
end