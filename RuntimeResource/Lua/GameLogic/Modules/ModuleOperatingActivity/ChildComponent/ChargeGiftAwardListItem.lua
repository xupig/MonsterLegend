-- ChargeGiftAwardListItem.lua
local UIListItem = ClassTypes.UIListItem
local ChargeGiftAwardListItem = Class.ChargeGiftAwardListItem(UIListItem)

ChargeGiftAwardListItem.interface = GameConfig.ComponentsConfig.PointableComponent
ChargeGiftAwardListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.ChargeGiftAwardListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local public_config = GameWorld.public_config
local StringStyleUtil = GameUtil.StringStyleUtil
local ItemConfig = GameConfig.ItemConfig
local OperatingActivityManager = PlayerManager.OperatingActivityManager

function ChargeGiftAwardListItem:Awake()
	self._txtCount = self:GetChildComponent("Container_content/Text_Count",'TextMeshWrapper')

	self._btnGet = self:FindChildGO("Container_content/Button_Get")
	self._csBH:AddClick(self._btnGet,function() self:GetAward() end)

	self._btnCharge = self:FindChildGO("Container_content/Button_Charge")
	self._csBH:AddClick(self._btnCharge,function() self:ToCharge() end)

	self._imgGot = self:FindChildGO("Container_content/Image_Got")
	self._containerTitleTrans = self:FindChildGO("Container_content/Container_Title").transform

	local diamondIcon = self:FindChildGO("Container_content/Container_Title/Container_Diamond")
	GameWorld.AddIcon(diamondIcon, ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
end

--override
function ChargeGiftAwardListItem:OnRefreshData(data)
	self._id = data.id
	local needCharge = data.cost
	local curCharge = GameWorld.Player().weekly_cumulative_charge_amount
	local str = curCharge .."/".. needCharge

	if curCharge < needCharge then
        self._txtCount.text = StringStyleUtil.GetColorStringWithColorId(str, ItemConfig.ItemForbidden)
    else
        self._txtCount.text = str
    end
    self._containerTitleTrans.localPosition = Vector3.New(self._txtCount.preferredWidth + 120,103)
	--LoggerHelper.Log(GameWorld.Player().weekly_cumulative_charge_refund_info)
	local state = GameWorld.Player().weekly_cumulative_charge_refund_info[self._id]
	if state then
		self._btnGet:SetActive(false)
		self._imgGot:SetActive(true)
		self._btnCharge:SetActive(false)
	elseif curCharge >= needCharge then
		self._btnGet:SetActive(true)
		self._imgGot:SetActive(false)
		self._btnCharge:SetActive(false)
	else
		self._btnGet:SetActive(false)
		self._imgGot:SetActive(false)
		self._btnCharge:SetActive(true)
	end
	
	--信息未初始化
	if self._awardItems == nil then
		self._awardItems = {}
		local vocationGroup = math.floor(GameWorld.Player().vocation/100)
		local reward = data.reward[vocationGroup]
		for i=1,#reward,3 do
			local index = math.ceil(i/3)
			self:AddAwardItem(index)
			self._awardItems[index]:SetItem(reward[i],reward[i+1])
		end
	end
end

function ChargeGiftAwardListItem:AddAwardItem(index)
	local parent = self:FindChildGO("Container_content/Container_Items/Container_Icon"..index)
   	local itemGo = GUIManager.AddItem(parent, 1)
   	local itemIcon = UIComponentUtil.AddLuaUIComponent(itemGo, ItemGrid)
   	itemIcon:ActivateTipsById()
   	self._awardItems[index] = itemIcon
end

function ChargeGiftAwardListItem:GetAward()
	OperatingActivityManager:RequestGetChargeAward(self._id)
end

function ChargeGiftAwardListItem:ToCharge()
	GUIManager.ShowPanelByFunctionId(public_config.FUNCTION_ID_CHARGE)
end