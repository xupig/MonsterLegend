-- HighActivityListItem.lua
local UIListItem = ClassTypes.UIListItem
local HighActivityListItem = Class.HighActivityListItem(UIListItem)

HighActivityListItem.interface = GameConfig.ComponentsConfig.PointableComponent
HighActivityListItem.classPath = "Modules.ModuleOperatingActivity.ChildComponent.HighActivityListItem"

require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local GuildManager = PlayerManager.GuildManager
local public_config = GameWorld.public_config

function HighActivityListItem:Awake()
	self._txtInfo = self:GetChildComponent("Container_content/Text_Info",'TextMeshWrapper')
	self._txtCount = self:GetChildComponent("Container_content/Text_Count",'TextMeshWrapper')
	self._containerActivityIcon = self:FindChildGO("Container_content/Container_Icon")

	self._btnGo = self:FindChildGO("Container_content/Button_Go")
	self._csBH:AddClick(self._btnGo,function() self:OnClickGo() end)
end

--override
function HighActivityListItem:OnRefreshData(data)
	self._data = data
	local finishCount = GameWorld.Player().hi_point_times[data.id] or 0
	self._txtCount.text = finishCount.."/"..data.times
	if self._inited == nil then
		local args = LanguageDataHelper.GetArgsTable()
		args["0"] = data.reward_point
		self._txtInfo.text = LanguageDataHelper.CreateContentWithArgs(data.desc,args)
		GameWorld.AddIcon(self._containerActivityIcon,data.icon)
		self._inited = true
	end
end

function HighActivityListItem:OnClickGo()
	local follow = self._data.follow
	if follow[1] then
		local funcId = follow[1][1] 
		local funcOpen = FunctionOpenDataHelper:CheckFunctionOpen(funcId)
		local showPanel
		if funcOpen then
			if self._data.id == public_config.HI_BONEFIRE then
				showPanel = GuildManager:IsInGuild()
			else
				showPanel = true
			end
		end
		if showPanel then
			GUIManager.ShowPanelByFunctionId(funcId)
		else
			GUIManager.ShowText(2,LanguageDataHelper.CreateContent(81636))
		end
		
	elseif follow[2] then
		EventDispatcher:TriggerEvent(GameEvents[follow[2]])
	end
end