-- CardPanel.lua
require "Modules.ModuleCard.ChildView.CardTotalView"
require "Modules.ModuleCard.ChildView.CardGroupView"
require "Modules.ModuleCard.ChildView.CardUpView"
require "Modules.ModuleCard.ChildView.CardAttView"
require "Modules.ModuleCard.ChildView.CardBagView"

local CardPanel = Class.CardPanel(ClassTypes.BasePanel)

local CardManager = PlayerManager.CardManager
local CardData = PlayerManager.PlayerDataManager.cardData

local GUIManager = GameManager.GUIManager
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local CardTotalView = ClassTypes.CardTotalView
local CardGroupView = ClassTypes.CardGroupView
local CardUpView = ClassTypes.CardUpView
local CardAttView = ClassTypes.CardAttView
local CardBagView = ClassTypes.CardBagView
local HidePosition = Vector3.New(0,4000,0)
local ShowPosition = Vector3.New(0,0,0)
local AttPosition = Vector3.New(422,84,0)
local HideScale = Vector3.New(0,1,1)
local ShowScale = Vector3.one
local CardType = GameConfig.EnumType.CardType

function CardPanel:Awake()
    self:InitViews()
    self._cardclick=function(data) self:onCardClick(data)end
    self._attclick=function(data) self:onAttClick(data)end
    self._bagclick=function(data) self:onBagClick(data)end
end

function CardPanel:InitViews()
    self._views={}
    self._viewsgo={}
    self._viewPathClass={}
    self:OnInitClassView(CardType.Total,"Container_Total",CardTotalView)
    self:OnInitClassView(CardType.Groud,"Container_Com",CardGroupView)
    self:OnInitClassView(CardType.Up,"Container_Tips",CardUpView)
    self:OnInitClassView(CardType.Att,"Container_AttAll",CardAttView)
    self:OnInitClassView(CardType.Bag,"Container_Bag",CardBagView)
    self._tabClickCB = function(index) self:tageClick(index) end 
    self:SetSecondTabClickCallBack(self._tabClickCB)
    -- self:tageClick(CardType.Total)
end

function CardPanel:OnInitClassView(index,path,view)
    self._viewPathClass[index]={path,view}
end

function CardPanel:OnGetView(index)
    if self._views[index] and self._viewsgo[index] then     
        self._viewsgo[index].transform.anchoredPosition=ShowPosition  
        return self._views[index],self._viewsgo[index]
    end
    local viewpath = self._viewPathClass[index][1]
    local view = self._viewPathClass[index][2]
    self._viewsgo[index] = self:FindChildGO(viewpath)
    if not self._viewsgo[index].activeSelf then
        self._viewsgo[index]:SetActive(true)
        self._viewsgo[index].transform.localScale=HideScale
    end
    self._views[index] = self:AddChildLuaUIComponent(viewpath,view)
    return self._views[index],self._viewsgo[index]
end


function CardPanel:tageClick(index)
    if self._seleviewIndex~=nil and self._seleviewIndex==index then
        self._viewsgo[index].transform.localScale=ShowScale 
        -- self._views[self._seleviewIndex]:Open()
    else
        if self._seleviewIndex then
            self._viewsgo[self._seleviewIndex].transform.localScale=HideScale
            self._views[self._seleviewIndex]:Close()
        end
        local view
        local viewgo 
        self._seleviewIndex=index
        view,viewgo=self:OnGetView(index)
        viewgo.transform.localScale=ShowScale
        view:Open()
    end
end

function CardPanel:onCardClick(data)
    local view
    local viewgo 
    view,viewgo=self:OnGetView(CardType.Up)
    viewgo.transform.localScale=ShowScale
    view:Open(data)
end

function CardPanel:onAttClick(data)
    local view
    local viewgo 
    view,viewgo=self:OnGetView(CardType.Att)
    viewgo.transform.localScale=ShowScale
    view:Open(data)
end

function CardPanel:onBagClick(data)
    local view
    local viewgo 
    view,viewgo=self:OnGetView(CardType.Bag)
    viewgo.transform.localScale=ShowScale
    view:Open(data)
end
function CardPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.CardClick, self._cardclick)
    EventDispatcher:AddEventListener(GameEvents.AttClick, self._attclick)
    EventDispatcher:AddEventListener(GameEvents.BagClick, self._bagclick)
end

function CardPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.CardClick, self._cardclick)
    EventDispatcher:RemoveEventListener(GameEvents.AttClick, self._attclick)
    EventDispatcher:RemoveEventListener(GameEvents.BagClick, self._bagclick)

end
    
function CardPanel:OnDestroy()
end

function CardPanel:OnShow(data)
    self:OnShowSelectTab(data)
    self:UpdateFunctionOpen()
    self:AddEventListeners()
    -- self:tageClick(1)
end

function CardPanel:OnClose()
    self:RemoveEventListeners()
    self._viewsgo[self._seleviewIndex].transform.localScale=HideScale
    self._views[self._seleviewIndex]:Close()
    self._seleviewIndex = nil
end


function CardPanel:CloseBtnClick()
    GUIManager.ClosePanel(PanelsConfig.Card)
end

-- -- 0 没有  1 暗红底板  2 蓝色底板  3 遮罩底板
function CardPanel:CommonBGType()
    return PanelBGType.NoBG
end

function CardPanel:WinBGType()
    return PanelWinBGType.NormalBG
end

--关闭的时候重新开启二级主菜单
function CardPanel:ReopenSubMainPanel()
    return true
end