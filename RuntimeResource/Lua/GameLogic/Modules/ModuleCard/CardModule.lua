CardModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function CardModule.Init()
    GUIManager.AddPanel(PanelsConfig.Card,"Panel_Card",GUILayer.LayerUIPanel,"Modules.ModuleCard.CardPanel",true,false,nil,true)
end

return CardModule