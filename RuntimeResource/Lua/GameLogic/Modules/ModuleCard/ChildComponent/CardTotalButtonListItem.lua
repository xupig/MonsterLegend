-- CardTotalButtonListItem.lua
local CardTotalButtonListItem = Class.CardTotalButtonListItem(ClassTypes.UIListItem)
CardTotalButtonListItem.interface = GameConfig.ComponentsConfig.Component
CardTotalButtonListItem.classPath = "Modules.ModuleCard.ChildComponent.CardTotalButtonListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardManager = PlayerManager.CardManager
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function CardTotalButtonListItem:Awake()
    self._state=nil
    self._redstate=false
    self:InitItem()
end

function CardTotalButtonListItem:OnDestroy()
    
end
function CardTotalButtonListItem:InitItem()
    self._unselected = self:FindChildGO("Container_content/UnSelected").transform
    self._selected = self:FindChildGO("Container_content/Selected").transform
    self._redImage = self:FindChildGO("Container_content/Image_Red").transform
    self._text1 = self:GetChildComponent("Container_content/UnSelected/Text","TextMeshWrapper")
    self._text2 = self:GetChildComponent("Container_content/Selected/Text","TextMeshWrapper")
end

function CardTotalButtonListItem:OnRefreshData(data)
    self._index = data[1]
    if self._lastTextId ~= data[2] then
        local t = LanguageDataHelper.GetContent(data[2])
        self._text1.text=t
        self._text2.text=t
        self._lastTextId = data[2]
    end
    if self._index==1 then
        self:SetState(1)
    else
        self:SetState(0)
    end
    self:RefreshRed()
end

function CardTotalButtonListItem:RefreshRed()
    if self._index then
        local r = CardManager:RefreshSigleTabRedPointState(self._index)
        if r ~= self._redstate then
            if r then
                self._redImage.localScale = ShowScale
            else
                self._redImage.localScale = HideScale
            end
            self._redstate = r
        end
    end
end

function CardTotalButtonListItem:SetState(state)
    if state~=self._state then
        if state==0 then
            self._unselected.localScale = ShowScale
            self._selected.localScale = HideScale
        else
            self._unselected.localScale = HideScale
            self._selected.localScale = ShowScale
        end
        self._state=state
    end
end