-- CardGroupListItem.lua
require"Modules.ModuleCard.ChildComponent.CardGroupAttListItem"
require"Modules.ModuleCard.ChildComponent.CardItem"
local CardGroupListItem = Class.CardGroupListItem(ClassTypes.UIListItem)
CardGroupListItem.interface = GameConfig.ComponentsConfig.Component
CardGroupListItem.classPath = "Modules.ModuleCard.ChildComponent.CardGroupListItem"
local CardGroupAttListItem = ClassTypes.CardGroupAttListItem
local CardItem = ClassTypes.CardItem
local UIList = ClassTypes.UIList
local CardManager=PlayerManager.CardManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardDataHelper = GameDataHelper.CardDataHelper

function CardGroupListItem:Awake()
    self:InitItem()
end

function CardGroupListItem:InitItem()
    local scroAttGo = self:FindChildGO("Container_content/Container_Att")
    local scrollView, list = UIList.AddScrollViewList(scroAttGo, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._list:SetItemType(CardGroupAttListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:UseObjectPool()
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._btActGo = self:FindChildGO("Container_content/Container_Att/Button_Active")
    self._redGo = self:FindChildGO("Container_content/Container_Att/Button_Active/Image_Red")
    self._csBH:AddClick(self._btActGo,function() self:btnClick() end)

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_content/ScrollViewList")
	self._list1 = list
	self._listView1 = scrollView 
    self._list1:SetItemType(CardItem)
    self._list1:SetPadding(0, 0, 0, 0)
    self._list1:SetGap(0, 20)
    self._list1:UseObjectPool()
    self._listView1:SetHorizontalMove(true)
    self._listView1:SetVerticalMove(false)
    self._list1:SetDirection(UIList.DirectionLeftToRight, 5, -1)

    self._textName = self:GetChildComponent("Container_content/Container_Att/Text_Name","TextMeshWrapper")
    self._btnwrap=self._btActGo:GetComponent("ButtonWrapper")

    self._conRay0 = self:FindChildGO("Container_content/Container_Att/Container_Raycast")
    self._conRay1 = self:FindChildGO("Container_content/Container_Raycast")
    -- self._listView1:SetScrollRectState(false)

end

function CardGroupListItem:btnClick()
    CardManager:CardGroupUpLevel(self._groupid)    
end
function CardGroupListItem:OnDestroy()

end

--override
function CardGroupListItem:OnRefreshData(item)
    -- self._listView1:SetScrollRectState(true)
    local carditems = item:GetCardsData()
    self._conRay0:SetActive(true)
    self._conRay1:SetActive(true)
    if #carditems>4 then
        self._conRay1:SetActive(false)
    end
    self._list1:SetDataList(carditems)
    local star = CardManager:GetStarNeed(item._groupid,item._level)
    local d=LanguageDataHelper.GetArgsTable()
    d['0']=star
    self._textName.text=LanguageDataHelper.CreateContent(item:GetName(),d)..'Lv'..star
    local red = CardManager:GetGroupRed(item._groupid,item._level)
    if CardManager:IsActGroup(item._groupid,item._level) or not red then
        self._btnwrap.interactable=false
        self._redGo:SetActive(false)
    elseif red then
        self._btnwrap.interactable=true
        self._redGo:SetActive(true)
    end
    local groupatt = CardManager:GetGroupAtt(item._groupid,item._level)
    local atts = CardManager:GetCardsAtt(groupatt)
    if #atts>4 then
        self._conRay0:SetActive(false)        
    end
    self._list:SetDataList(atts)
    self._groupid=item:GetGroupId()
    self._groupstar=star
    if CardManager:IsFullGroup(item._groupid,item._level) then
        self._btnwrap.text = LanguageDataHelper.GetContent(75527)
    else
        self._btnwrap.text = LanguageDataHelper.GetContent(75529)
    end
end

function CardGroupListItem:GetGroupId()
    return self._groupid
end

function CardGroupListItem:GetGroupStar()
    return self._groupstar
end
