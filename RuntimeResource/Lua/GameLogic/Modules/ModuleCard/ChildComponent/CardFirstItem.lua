-- CardFirstItem.lua
--一级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local CardFirstItem = Class.CardFirstItem(UINavigationMenuItem)

CardFirstItem.interface = GameConfig.ComponentsConfig.PointableComponent
CardFirstItem.classPath = "Modules.ModuleCard.ChildComponent.CardFirstItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardDataHelper = GameDataHelper.CardDataHelper
local CardManager = PlayerManager.CardManager
function CardFirstItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self._isGetSelect = false
    self:IninView()
end

function CardFirstItem:OnDestroy()

end

function CardFirstItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._selectImage = self:FindChildGO("Image_Select")
    self._redImage = self:FindChildGO("Image_Red")
    self._jianSelectImage = self:FindChildGO("Image_Jian_Select")
    self._jianUnSelectImage = self:FindChildGO("Image_Jian_UnSelect")
    self._jianSelectImage:SetActive(true)
    self._jianUnSelectImage:SetActive(false)
end

function CardFirstItem:OnPointerDown(pointerEventData)

end

--override {type}
function CardFirstItem:OnRefreshData(data)
    self._itemdata = data
    local nameid = CardDataHelper:GetTypeName(data)
    self._nameText.text = LanguageDataHelper.GetContent(nameid)
    self:SetSelectImage(false)
    self:RefreshRed()
end

function CardFirstItem:RefreshRed()
    local t = CardDataHelper:GetType(self._itemdata)
    local r = CardManager:RefreshTypeRedPointState(t)
    if r ~= self._red then
        self._redImage:SetActive(r)
        self._red  = r
    end
end

function CardFirstItem:SetSelect(flag)
    self._isExpanded = flag
    self._selectImage:SetActive(flag)
end

function CardFirstItem:SetSelectImage(flag)
    self._jianSelectImage:SetActive(not flag)
    self._jianUnSelectImage:SetActive(flag)
    self._isGetSelect = flag
end

function CardFirstItem:ToggleSetSelect()
    if self._isGetSelect then
        self._jianSelectImage:SetActive(true)
        self._jianUnSelectImage:SetActive(false)
        self._isGetSelect = false
    else
        self._jianSelectImage:SetActive(false)
        self._jianUnSelectImage:SetActive(true)
        self._isGetSelect = true
    end
end