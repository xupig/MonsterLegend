-- CardBagDropItem.lua
local CardBagDropItem = Class.CardBagDropItem(ClassTypes.UIListItem)
CardBagDropItem.interface = GameConfig.ComponentsConfig.Component
CardBagDropItem.classPath = "Modules.ModuleCard.ChildComponent.CardBagDropItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper

function CardBagDropItem:Awake()
    self._base.Awake(self)
    self:InitView()
end

function CardBagDropItem:InitView()
	self._txtInfo = self:GetChildComponent("Text_Info",'TextMeshWrapper')
end
function CardBagDropItem:OnDestroy()

end

function CardBagDropItem:OnRefreshData(data)
    self._quality = data[2]
	self._txtInfo.text = LanguageDataHelper.GetContent(data[1])
end

function CardBagDropItem:GetQ()
    return self._quality
end
