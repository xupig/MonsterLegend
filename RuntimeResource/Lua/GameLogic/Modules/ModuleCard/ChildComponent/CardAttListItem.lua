-- CardAttListItem.lua
local CardAttListItem = Class.CardAttListItem(ClassTypes.UIListItem)
CardAttListItem.interface = GameConfig.ComponentsConfig.Component
CardAttListItem.classPath = "Modules.ModuleCard.ChildComponent.CardAttListItem"

function CardAttListItem:Awake()
    self._textdes1 = self:GetChildComponent("Container_content/Text_Des1","TextMeshWrapper")
    self._textdes2 = self:GetChildComponent("Container_content/Text_Des2","TextMeshWrapper")
end

function CardAttListItem:OnDestroy()
   
end

--override
function CardAttListItem:OnRefreshData(data)
    local d1=data[1]
    local d2=data[2]
   if d1 then
        if d1[3]==1 then
            self._textdes1.text=d1[1]..":  "..d1[2]
        else
            self._textdes1.text=d1[1]..":  "..(d1[2]/100-d1[2]/100%0.01).."%"
        end
   else
    self._textdes1.text = ''
   end
    if d2 then
        if d2[3]==1 then
            self._textdes2.text=d2[1]..":  "..d2[2]
        else
            self._textdes2.text=d2[1]..":  "..(d2[2]/100-d2[2]/100%0.01).."%"
        end
    else
        self._textdes2.text=''
    end
end


