-- CardTotalListItem.lua
local CommonTimeListConfig = GameConfig.CommonTimeListConfig
local CardTotalListItem = Class.CardTotalListItem(ClassTypes.UIListItem)
local PanelsConfig = GameConfig.PanelsConfig
CardTotalListItem.interface = GameConfig.ComponentsConfig.Component
CardTotalListItem.classPath = "Modules.ModuleCard.ChildComponent.CardTotalListItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local CardManager=PlayerManager.CardManager
local CardLevelDataHelper = GameDataHelper.CardLevelDataHelper
local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)

function CardTotalListItem:Awake()
    self._base.Awake(self)
    self._bgq = {}
    self._bgq[3]= 3
    self._bgq[4]= 4
    self._bgq[5]= 5
    self._bgq[6]= 6
    self._bgq[7]= 7
    self:InitView()
end

function CardTotalListItem:InitView()
    self._curcardstar = nil--当前卡star
    self._curcarid = nil--当前卡id
    self._curitemtype = nil
    self._imgq={}
    self._imgq[3] = self:FindChildGO("Container_content/itembg/Image_0").transform
    self._imgq[4] = self:FindChildGO("Container_content/itembg/Image_1").transform
    self._imgq[5] = self:FindChildGO("Container_content/itembg/Image_2").transform
    self._imgq[6] = self:FindChildGO("Container_content/itembg/Image_3").transform
    self._imgq[7] = self:FindChildGO("Container_content/itembg/Image_4").transform
    self._imageIconGo = self:FindChildGO('Container_content/Image_Icon')
    self._imageRedGo = self:FindChildGO('Container_content/Image_Red').transform
    self._containerLockGo = self:FindChildGO("Container_content/Container_Lock").transform
    self._containerUnActiveGo = self:FindChildGO("Container_content/Container_UnActive").transform
    self._containerStars = self:FindChildGO("Container_content/Container_Stars").transform
    self._containerNum = self:FindChildGO("Container_content/Container_Num").transform
    self._starState1Rect = self:GetChildComponent("Container_content/Container_Stars/Image_State1", "RectTransform")
    self._starState1Image = self:GetChildComponent("Container_content/Container_Stars/Image_State1", "ImageWrapper")
    self._oneStarWidth = self._starState1Rect.sizeDelta.x/5
    self._oneStarHeight = self._starState1Rect.sizeDelta.y
    self._textNum = self:GetChildComponent('Container_content/Text_Num', 'TextMeshWrapper')
    self._textName = self:GetChildComponent('Container_content/Text_Name', 'TextMeshWrapper')
    self._textLock = self:GetChildComponent('Container_content/Container_Lock/Text_Unlock', 'TextMeshWrapper')
    self._starNum = self:AddChildComponent('Container_content/Container_Num', XArtNumber)  
end
function CardTotalListItem:OnDestroy()

end

function CardTotalListItem:SetCardStar(star)
    if self._curcardstar~=star then
        self._curcardstar = star
        if self._curcardstar<=5 then
            self._containerStars.localScale = ShowScale
            self._containerNum.localScale = HideScale
            self._starState1Image:SetAllDirty()
            self._starState1Rect.sizeDelta = Vector3.New(self._oneStarWidth*self._curcardstar, self._oneStarHeight, 0)
        else
            self._starNum:SetNumber(self._curcardstar)  
            self._containerStars.localScale = HideScale
            self._containerNum.localScale = ShowScale
        end
    end
end

function CardTotalListItem:SetLock(itemdata)
    self._containerStars.localScale = ShowScale
    self._starState1Image:SetAllDirty()
    self._starState1Rect.sizeDelta = Vector3.New(0, self._oneStarHeight, 0)
    self._imageRedGo.localScale = HideScale
    self._containerNum.localScale = HideScale
    self._containerLockGo.localScale = ShowScale
    self._containerUnActiveGo.localScale = HideScale
    self._textLock.text = itemdata:GetLevelDesc()
end

function CardTotalListItem:OnRefreshData(itemdata) 
    if self._curcardstar~=itemdata:CardStar() or self._curcarid~=itemdata:GetCardId() then
        self._textName.text = itemdata:GetName()
        local cost=itemdata:GetCardCost()
        if not itemdata:IsLock(itemdata._id) then       
            if itemdata:CardIsHave() then
                self:SetCardStar(itemdata:CardStar())            
                self._containerLockGo.localScale = HideScale   
                self._containerUnActiveGo.localScale = HideScale   
            else
                self:SetCardStar(0)    
                self._containerLockGo.localScale = HideScale       
                self._containerUnActiveGo.localScale = ShowScale       
            end     
            if not cost then
                self._imageRedGo.localScale = HideScale
                self._textNum.text = LanguageDataHelper.GetContent(75526)
                self:SetIcon(itemdata:GetIcon())
                self:SeleQualityBg(itemdata:GetQuality())
                return
            else
                self:SetRed(itemdata)
            end  
        else
            self:SetLock(itemdata)
        end
        self:SetNum(itemdata)
        self:SetIcon(itemdata:GetIcon())
        self:SeleQualityBg(itemdata:GetQuality())
    end
end

function CardTotalListItem:SetRed(itemdata)
    local isred = CardManager:CardTotalRed(itemdata._id,itemdata._star)[1]
    if isred then
        self._imageRedGo.localScale = ShowScale
    else
        self._imageRedGo.localScale = HideScale
    end
end

function CardTotalListItem:SetNum(itemdata)
    local costid=0
    local num=0
    local cost=itemdata:GetCardCost()
    if cost then
        costid,num = next(cost)
        local havema = CardManager:CardTotalRed(itemdata._id,itemdata._star)[2]
        if itemdata:CardIsHave() then
            if not CardLevelDataHelper:isFullLevel(itemdata._id,itemdata._star) then
                if havema >=num then
                    self._textNum.text=BaseUtil.GetColorString(havema.."/"..num,ColorConfig.J)
                else
                    self._textNum.text=BaseUtil.GetColorString(havema.."/"..num,ColorConfig.H)
                end
            else
                self._textNum.text = ''
            end
        else
            if havema >=num then
                self._textNum.text=BaseUtil.GetColorString(havema.."/"..num,ColorConfig.J)
            else
                self._textNum.text=BaseUtil.GetColorString(havema.."/"..num,ColorConfig.H)
            end
        end
    end
end

function CardTotalListItem:SetIcon(icon)
    if self._icon~=icon then
        self._icon = icon
        GameWorld.AddIcon(self._imageIconGo,icon,nil,true)
    end
end
function CardTotalListItem:SeleQualityBg(quality)
    if quality ~= self._quality then
        local q = self._bgq[quality]
        self._imgq[q].localScale = ShowScale
        if self._quality then
            self._imgq[self._quality].localScale = HideScale
        end
        self._quality = quality
    end
end