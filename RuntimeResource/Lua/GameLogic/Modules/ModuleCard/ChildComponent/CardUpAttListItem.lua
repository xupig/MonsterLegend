-- CardUpAttListItem.lua
local CardUpAttListItem = Class.CardUpAttListItem(ClassTypes.UIListItem)
CardUpAttListItem.interface = GameConfig.ComponentsConfig.Component
CardUpAttListItem.classPath = "Modules.ModuleCard.ChildComponent.CardUpAttListItem"

function CardUpAttListItem:Awake()
    self._textcur = self:GetChildComponent("Container_content/Text_Cur","TextMeshWrapper")
    self._textnext = self:GetChildComponent("Container_content/Text_Next","TextMeshWrapper")
    
end

function CardUpAttListItem:OnDestroy()

end
--override
function CardUpAttListItem:OnRefreshData(data)
    local d1=data[1]
    local d2=data[2]
    if d1[3]==1 then
        self._textcur.text=d1[1]..":  "..d1[2]
    else
        self._textcur.text=d1[1]..":  "..(d1[2]/100-d1[2]/100%0.01).."%"
    end
    if d2[3]==1 then
        self._textnext.text=""..d2[2]
    else
        self._textnext.text=(d2[2]/100-d2[2]/100%0.01).."%"
    end
end
