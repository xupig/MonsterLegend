-- CardCurAttListItem.lua
local CardCurAttListItem = Class.CardCurAttListItem(ClassTypes.UIListItem)
CardCurAttListItem.interface = GameConfig.ComponentsConfig.Component
CardCurAttListItem.classPath = "Modules.ModuleCard.ChildComponent.CardCurAttListItem"

function CardCurAttListItem:Awake()
    self._textdes = self:GetChildComponent("Text_Cur","TextMeshWrapper")
end

function CardCurAttListItem:OnDestroy()
   
end

--override
function CardCurAttListItem:OnRefreshData(data)
   if data[3]==1 then
        self._textdes.text=data[1]..":  "..data[2]
   else
        self._textdes.text=data[1]..":  "..(data[2]/100-data[2]/100%0.01).."%"
   end
end


