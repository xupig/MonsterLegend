-- CardGroupAttListItem.lua
local CardGroupAttListItem = Class.CardGroupAttListItem(ClassTypes.UIListItem)
CardGroupAttListItem.interface = GameConfig.ComponentsConfig.Component
CardGroupAttListItem.classPath = "Modules.ModuleCard.ChildComponent.CardGroupAttListItem"

function CardGroupAttListItem:Awake()
    self._textdes = self:GetChildComponent("Text_Des","TextMeshWrapper")
end

function CardGroupAttListItem:OnDestroy()
   
end

--override
function CardGroupAttListItem:OnRefreshData(data)
   if data[3]==1 then
        self._textdes.text=data[1]..":  "..data[2]
   else
        self._textdes.text=data[1]..":  "..(data[2]/100-data[2]/100%0.01).."%"
   end
end


