-- CardAttListItem1.lua
require"Modules.ModuleCard.ChildComponent.CardAttListItem"
local CardAttListItem1 = Class.CardAttListItem1(ClassTypes.UIListItem)
CardAttListItem1.interface = GameConfig.ComponentsConfig.Component
CardAttListItem1.classPath = "Modules.ModuleCard.ChildComponent.CardAttListItem1"
local UIList = ClassTypes.UIList
local CardAttListItem = ClassTypes.CardAttListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function CardAttListItem1:Awake()
    self._base.Awake(self)
    self._data = {}
    self:InitView()    
end

function CardAttListItem1:InitView()
    self._rectTransform = self:GetComponent(typeof(UnityEngine.RectTransform))
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollView_Suit")
    self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(CardAttListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)  
    self._textdes = self:GetChildComponent("Text_Des","TextMeshWrapper")

    if self._data[1] and not table.isEmpty(self._data[1]) then
        local height = (#self._data[1])*40+60
        self.transform.localPosition.y=self.transform.localPosition.y-height
        local size = self._rectTransform.sizeDelta
        size.y = height
        self._rectTransform.sizeDelta = size
        self._list:SetDataList(self._data[1])
    end
end
function CardAttListItem1:OnDestroy()
   
end

--override
function CardAttListItem1:OnRefreshData(data)
    if not self._list then
        self._data = data
        return
    end
    self._textdes.text = LanguageDataHelper.CreateContent(data[2])
    local height = (#data[1])*40+60
    self.transform.localPosition.y=self.transform.localPosition.y-height
    local size = self._rectTransform.sizeDelta
    size.y = height
    self._rectTransform.sizeDelta = size
    self._list:SetDataList(data[1])
end


