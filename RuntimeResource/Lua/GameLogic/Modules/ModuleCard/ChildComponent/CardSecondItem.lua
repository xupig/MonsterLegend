-- CardSecondItem.lua
--二级菜单Item
local UINavigationMenuItem = ClassTypes.UINavigationMenuItem
local CardSecondItem = Class.CardSecondItem(UINavigationMenuItem)

CardSecondItem.interface = GameConfig.ComponentsConfig.PointableComponent
CardSecondItem.classPath = "Modules.ModuleCard.ChildComponent.CardSecondItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardDataHelper = GameDataHelper.CardDataHelper
local CardManager = PlayerManager.CardManager

local ShowScale = Vector3.one
local HideScale = Vector3.New(0, 1, 1)
local ItemConfig = GameConfig.ItemConfig
local StringStyleUtil = GameUtil.StringStyleUtil

function CardSecondItem:Awake()
    UINavigationMenuItem.Awake(self)
    self._isExpanded = false
    self:IninView()
end

function CardSecondItem:OnDestroy()

end

function CardSecondItem:IninView()
    self._nameText = self:GetChildComponent("Text_Name",'TextMeshWrapper')
    self._selectImage = self:FindChildGO("Image_Select")
    self._redImage = self:FindChildGO("Image_Red")
    self._qualityImage={}
    local j = 0
    for i=3,7 do
        j = j + 1
        self._qualityImage[i] = self:FindChildGO("Image_"..j).transform
    end
end

function CardSecondItem:OnPointerDown(pointerEventData)

end

--override {type}
function CardSecondItem:OnRefreshData(data)
    self._itemData = data
    if not data then
        return 
    end
    local s = CardDataHelper:GetSubType(self._itemData[1])
    if s then
        local subtype,q = next(s)
        if self._curquality ~= q then
            local nameid = CardDataHelper:GetSubTypeName(data[1])
            if nameid ~= self._nameid then
                self._nameid = nameid
                local colorId = ItemConfig.QualityTextMap[q]
                local name = StringStyleUtil.GetColorStringWithColorId(LanguageDataHelper.GetContent(nameid),colorId)
                self._nameText.text = name
            end
            self:SetQualityImage(q,true)
            self._curquality = q
        end
    end
    self:RefreshRed()
end

function CardSecondItem:SetQualityImage(quality,state)
    for i=3,7 do
        if i==quality and state then
            self._qualityImage[i].localScale = ShowScale
        else 
            self._qualityImage[i].localScale = HideScale
        end
    end
end


function CardSecondItem:SetSelect(flag)
    self._selectImage:SetActive(flag)
end

function CardSecondItem:RefreshRed()
    if self._itemData then
        local s = CardDataHelper:GetSubType(self._itemData[1])
        if s then
            local subtype,q = next(s)
            local t = CardDataHelper:GetType(self._itemData[1])
            local r = CardManager:RefreshSigleTabRedPointState(t,subtype)
            if r ~= self._curred then
                self._curred = r
                self._redImage:SetActive(r)
            end
            return 
        end
    end
    self._redImage:SetActive(false)
end

function CardSecondItem:GetItemData()
    return self._itemData
end