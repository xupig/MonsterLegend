-- CardItem.lua
local CardItem = Class.CardItem(ClassTypes.UIListItem)
local PanelsConfig = GameConfig.PanelsConfig
CardItem.interface = GameConfig.ComponentsConfig.Component
CardItem.classPath = "Modules.ModuleCard.ChildComponent.CardItem"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local ColorConfig = GameConfig.ColorConfig
local XArtNumber = GameMain.XArtNumber
local CardManager=PlayerManager.CardManager

function CardItem:Awake()
    self._base.Awake(self)
    self._bgq = {}
    self._bgq[3]= 3
    self._bgq[4]= 4
    self._bgq[5]= 5
    self._bgq[6]= 6
    self._bgq[7]= 7
    self:InitView()
end

function CardItem:InitView()
    self._data = {}
    self._imgq={}
    self._containerLockGo = self:FindChildGO('Container')
    self._containerNum = self:FindChildGO("Container_Num")
    self._containerStars = self:FindChildGO("Container_Stars")
    self._imageIconGo = self:FindChildGO('Image_Icon')
    self._imgq[3] = self:FindChildGO("itembg/Image_0")
    self._imgq[4] = self:FindChildGO("itembg/Image_1")
    self._imgq[5] = self:FindChildGO("itembg/Image_2")
    self._imgq[6] = self:FindChildGO("itembg/Image_3")
    self._imgq[7] = self:FindChildGO("itembg/Image_4")

    self._starState1Rect = self:GetChildComponent("Container_Stars/Image_State1", "RectTransform")
    self._starState1Image = self:GetChildComponent("Container_Stars/Image_State1", "ImageWrapper")
    self._oneStarWidth = self._starState1Rect.sizeDelta.x/5
    self._oneStarHeight = self._starState1Rect.sizeDelta.y

    self._textName = self:GetChildComponent('Text_Name', 'TextMeshWrapper')
    self._starNum = self:AddChildComponent('Container_Num', XArtNumber)  
    self._textLock = self:GetChildComponent("Container/Text_Unlock","TextMeshWrapper")
end

function CardItem:OnDestroy()
end

function CardItem:OnRefreshData(data)
    local q = data:GetQuality()
    self._textName.text = data:GetName()
    GameWorld.AddIcon(self._imageIconGo,data:GetIcon(),nil,true)
    self:SeleQualityBg(self._bgq[q])
    self._containerLockGo:SetActive(data:IsLock())
    self._textLock.text = data:GetLevelDesc()

    local star = data._star
    if star<=5 then
        self._containerStars:SetActive(true)
        self._containerNum:SetActive(false)
        self._starState1Image:SetAllDirty()
        self._starState1Rect.sizeDelta = Vector3.New(self._oneStarWidth*star, self._oneStarHeight, 0)
    else
        self._starNum:SetNumber(star)  
        self._containerStars:SetActive(false)
        self._containerNum:SetActive(true)
    end
end

function CardItem:SeleQualityBg(quality)
    for k,v in pairs(self._imgq)do
        if k==quality then
            self._imgq[k]:SetActive(true)
        else
            self._imgq[k]:SetActive(false)
        end
    end
end
