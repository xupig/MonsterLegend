-- CardBagListItem.lua
require "UIComponent.Extend.ItemGrid"
local CardBagListItem = Class.CardBagListItem(ClassTypes.UIListItem)
CardBagListItem.interface = GameConfig.ComponentsConfig.Component
CardBagListItem.classPath = "Modules.ModuleCard.ChildComponent.CardBagListItem"
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager

function CardBagListItem:Awake()
    self._containerItem = self:FindChildGO("Container_content")
    self._imgbg = self:FindChildGO("Container_content/Image_Bg")
    local a = GUIManager.AddItem(self._containerItem, 1)
    self._item = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    local btnArgs = {}
    btnArgs.forSale = true
    self._item:ActivateTipsByItemData(btnArgs)
end

function CardBagListItem:OnDestroy()
   
end

--override
function CardBagListItem:OnRefreshData(data)
    if data==-1 then
        -- self._imgbg:SetActive(true)
        self._item:SetItemData(nil)
        return
    end
    -- self._imgbg:SetActive(false)
    self._item:SetItemData(data)
end


