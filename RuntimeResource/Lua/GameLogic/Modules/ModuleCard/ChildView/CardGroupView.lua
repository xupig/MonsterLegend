--CardGroupView.lua
require"Modules.ModuleCard.ChildComponent.CardGroupListItem"
require "UIComponent.Extend.TipsCom"
local CardGroupView = Class.CardGroupView(ClassTypes.BaseComponent)
CardGroupView.interface = GameConfig.ComponentsConfig.Component
CardGroupView.classPath = "Modules.ModuleCard.ChildView.CardGroupView"
local CardData =  PlayerManager.PlayerDataManager.cardData
local UIList = ClassTypes.UIList
local CardGroupListItem = ClassTypes.CardGroupListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardManager = PlayerManager.CardManager
local TipsCom = ClassTypes.TipsCom

function CardGroupView:Awake()
    self._update = function() self:Update() end
    self:InitView()
end
function CardGroupView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(CardGroupListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(20, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._btDesGo = self:FindChildGO("Button_Des")
    self._csBH:AddClick(self._btDesGo,function() self:btnDesClick() end)
    self._btAddGo = self:FindChildGO("Button_Add")
    self._csBH:AddClick(self._btAddGo,function() self:btnAddClick() end)
    self._btBagGo = self:FindChildGO("Button_Bag")
    self._csBH:AddClick(self._btBagGo,function() self:btnBagClick() end)

    self._tipsButton = self:FindChildGO("Button_Des")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)
    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(75293))
    self._tipsGO:SetActive(false)
end

function CardGroupView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function CardGroupView:Open()
    self._listView:SetScrollRectState(true)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CARD_GROUP, self._update)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PKG_CARD_PIECE, self._update)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._update)
    self:Update()
end
function CardGroupView:btnDesClick()

end
function CardGroupView:btnAddClick()
    local d1=CardManager:GetCardsAttDes()
    local d2=CardManager:GetGroupsAttDes()
    -- LoggerHelper.Log(d1)
    EventDispatcher:TriggerEvent(GameEvents.AttClick,{{d1,75524},{d2,75525}})
end
function CardGroupView:btnBagClick()
    local data = CardManager:GetBagPiece()
    EventDispatcher:TriggerEvent(GameEvents.BagClick,data)
end
function CardGroupView:Close()
    self._listView:SetScrollRectState(false)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CARD_GROUP, self._update)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PKG_CARD_PIECE, self._update)
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._update)
end

function CardGroupView:Update()
    local d = CardData:GetCardGroupItemDatas()
    --self._list:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)
    self._list:SetDataList(d, 1)--使用协程加载
    --self._list:SetDataList(d)
end

-- function CardGroupView:OnContentListAllItemCreated()
-- end

function CardGroupView:OnDestory()

end

