--CardAttView.lua
require"Modules.ModuleCard.ChildComponent.CardAttListItem1"
local CardAttView = Class.CardAttView(ClassTypes.BaseComponent)
CardAttView.interface = GameConfig.ComponentsConfig.Component
CardAttView.classPath = "Modules.ModuleCard.ChildView.CardAttView"
local UIList = ClassTypes.UIList
local UIComplexList = ClassTypes.UIComplexList
local CardAttListItem1 = ClassTypes.CardAttListItem1
local HideScale = Vector3.New(0,1,1)
function CardAttView:Awake()
    self._base.Awake(self)
    self._items={}
    self:InitView()
end

function CardAttView:InitView()
    local scrollView, list = UIComplexList.AddScrollViewComplexList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(CardAttListItem1)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 0)
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)
    self._btCloseGo = self:FindChildGO("Button_Close")
    self._csBH:AddClick(self._btCloseGo,function() self:BtnCloseClick() end)
    self._tipsGo = self:FindChildGO("Text_Tips")
end


function CardAttView:BtnCloseClick()
    self._listView:SetScrollRectState(false)
    self.transform.localScale = HideScale
end
function CardAttView:Open(data)
    self._listView:SetScrollRectState(true)
   self:Update(data)
end

function CardAttView:Close()
end

function CardAttView:Update(data)   
    if table.isEmpty(data[1]) and table.isEmpty(data[2]) then
        self._tipsGo:SetActive(true)
        return
    end
    self._tipsGo:SetActive(false)
    self._list:RemoveAll()
    self._list:AppendItemDataList(data)
end

function CardAttView:OnDestory()
    self._csBH = nil
end