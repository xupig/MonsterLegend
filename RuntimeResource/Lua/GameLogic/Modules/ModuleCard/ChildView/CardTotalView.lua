--CardTotalView.lua
require "Modules.ModuleCard.ChildView.CardUpView"
require "Modules.ModuleCard.ChildComponent.CardTotalListItem"
require "Modules.ModuleCard.ChildComponent.CardTotalButtonListItem"
require "UIComponent.Extend.TipsCom"
require "Modules.ModuleCard.ChildComponent.CardFirstItem"
require "Modules.ModuleCard.ChildComponent.CardSecondItem"
local CardTotalView = Class.CardTotalView(ClassTypes.BaseComponent)
local CardTotalListItem = ClassTypes.CardTotalListItem
local CardTotalButtonListItem = ClassTypes.CardTotalButtonListItem

local CardFirstItem = ClassTypes.CardFirstItem
local CardSecondItem = ClassTypes.CardSecondItem

CardTotalView.interface = GameConfig.ComponentsConfig.Component
CardTotalView.classPath = "Modules.ModuleCard.ChildView.CardTotalView"

local UINavigationMenu = ClassTypes.UINavigationMenu
local CardData =  PlayerManager.PlayerDataManager.cardData
local CardDataHelper = GameDataHelper.CardDataHelper
local UIList = ClassTypes.UIList
local CardManager=PlayerManager.CardManager
local UIToggle = ClassTypes.UIToggle
local TipsCom = ClassTypes.TipsCom
local CardUpView = ClassTypes.CardUpView
local ShowScale = Vector3.New(1,1,1)
function CardTotalView:Awake()
    self._curIndex = 0
    self._isquick = true
    self._isdelay = false
    self:InitView()
end

function CardTotalView:InitView()  
    self._upview = self:AddChildLuaUIComponent('Container_Tips',CardUpView)
    self._upviewGo = self:FindChildGO("Container_Tips")

    self._btnClose=self:FindChildGO("Container_Tips/Button_Close")
    self._csBH:AddClick(self._btnClose,function() self:BtnCloseClick()end)

    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList_Items')
    self._Itemlist = list
    self._scrollview1 = scrollview
    self._Itemlist:SetItemType(CardTotalListItem)
    self._Itemlist:SetPadding(5,0,0,0)
    self._Itemlist:SetGap(20,30)
    self._Itemlist:SetDirection(UIList.DirectionLeftToRight,4,-1)
    local itemClickedCB = 
	function(index)
		self:OnItemClicked(index)
    end
    self._Itemlist:UseObjectPool(true)
    self._Itemlist:SetItemClickedCB(itemClickedCB)
    self._Itemlist:SetOnAllItemCreatedCB(function() self:OnContentListAllItemCreated() end)

    self._btAddGo = self:FindChildGO("Button_Add")
    self._csBH:AddClick(self._btAddGo,function() self:btnAddClick() end)
    self._btBagGo = self:FindChildGO("Button_Bag")
    self._csBH:AddClick(self._btBagGo,function() self:btnBagClick() end)

    local txt = self:GetChildComponent("Button_Bag/Text", "TextMeshWrapper")
    txt.text = LanguageDataHelper.GetContent(75528)

    self._updateList = function() self:Update() end

    local toggle = UIToggle.AddToggle(self.gameObject,"Toggle_Up")
    toggle:SetOnValueChangedCB(function(state) self:OnToggleValueChanged(state) end)
    toggle:SetIsOn(false)
    self._tipsButton = self:FindChildGO("Button_Des")
    self._csBH:AddClick(self._tipsButton,function() self:OnTipsButtonClick() end)
    self._tipsGO = self:FindChildGO("Container_Introduce")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Introduce", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(75293))
    self._tipsGO:SetActive(false)

    local scroll, menu = UINavigationMenu.AddScrollViewMenu(self.gameObject, "Container_List/NavigationMenu")
    self._menu = menu
	self._scrollPage = scroll
	self._menu:SetFirstLevelItemType(CardFirstItem)
	self._menu:SetSecondLevelItemType(CardSecondItem)
	self._menu:SetPadding(0, 0, 0, 0)
	self._menu:SetGap(5, 0)
	self._menu:SetOnlyOneFItemExpand(true)
    self._menu:SetFirstLevelMenuItemClickedCB(function(idx) self:OnMenuFItemClicked(idx) end)
    self._menu:SetSecondLevelMenuItemClickedCB(function(fIndex, sIndex) self:OnMenuSItemClicked(fIndex, sIndex) end)
    self._menu:UseObjectPool(true)
    self._menu:SetOnAllItemCreatedCB(function() self:OnAllTypeListItemCreated() end)

end

function CardTotalView:OnAllTypeListItemCreated()
    self._menu:SelectFItem(0)
    self._selectItem = self._menu:GetSItem(0, 0)
    self._selectItem:SetSelect(true)
end

function CardTotalView:OnMenuFItemClicked(idx)
    if self._fIndex and self._fIndex ~= idx then
        self._selectedFItem = self._menu:GetFItem(self._fIndex)
        self._selectedFItem:SetSelectImage(false)
    end
    self._fIndex = idx
    self._selectedFItem = self._menu:GetFItem(idx)
    self._selectedFItem:ToggleSetSelect()
end

function CardTotalView:OnMenuSItemClicked(fIndex, sIndex)
    self._fIndex = fIndex
    self._sIndex = sIndex
    self._otherId = self._menuData[fIndex+1][sIndex+2].id
    if self._selectItem ~= nil then 
        self._selectItem:SetSelect(false)    
    end
    self._selectItem = self._menu:GetSItem(fIndex, sIndex)
    self._selectItem:SetSelect(true)
    
    self:UpdateList()
end

function CardTotalView:BtnCloseClick()
    self._isdelay = false
    self._upview:BtnCloseClick()
    self:Update()
end

function CardTotalView:OnTipsButtonClick()
    self._tipsGO:SetActive(true)
end

function CardTotalView:OnToggleValueChanged(state)
	self._isquick = not self._isquick
end

function CardTotalView:btnBagClick()
    local data = CardManager:GetBagPiece()
    EventDispatcher:TriggerEvent(GameEvents.BagClick,data)
end

function CardTotalView:UpdateList()
    if self._selectItem then
        local items = CardData:CreateCardItem(self._selectItem:GetItemData())
        self._Itemlist:SetDataList(items, 2)
    end
end


function CardTotalView:OnContentListAllItemCreated()
 
end

function CardTotalView:OnRefreshBtnRed()
    if self._menu then
        local firstlen = self._menu:GetFirstLevelLength()
        for i=0,firstlen-1 do
            local sectlen = self._menu:GetSecondLevelLength(i)
            local firstitem = self._menu:GetFItem(i)
            if firstitem then
                firstitem:RefreshRed()
            end
            for j=0,sectlen-1 do
                local secitem = self._menu:GetSItem(i,j)
                secitem:RefreshRed()
            end
        end
    end
end

function CardTotalView:SetItemState(index)
    local len = self._list0:GetLength()
    for i=0,len-1 do
        local item =self._list0:GetItem(i)
        if index==i then
            item:SetState(1)
        else
            item:SetState(0)
        end
    end
end

function CardTotalView:btnAddClick()
    local d1=CardManager:GetCardsAttDes()
    local d2=CardManager:GetGroupsAttDes()
    EventDispatcher:TriggerEvent(GameEvents.AttClick,{{d1,75524},{d2,75525}})
end

function CardTotalView:OnItemClicked(index)
    local item = self._Itemlist:GetDataByIndex(index)
    local id=item._id
    local star=item._star
    if self._isquick then
        CardManager:CardUpLevel(id)        
        return
    end
    if not self._upviewGo.activeSelf then
        self._upviewGo:SetActive(true)
    end
    self._isdelay=true
    self._upview:Open(item)
    self._upviewGo.transform.localScale = ShowScale
end

function CardTotalView:Open()
    self._scrollPage:SetScrollRectState(true)
    self._scrollview1:SetScrollRectState(true)
    self:AddEventListeners()
    self._menuData = CardDataHelper:GetCardTotal()
    self._menu:SetDataList(self._menuData,4)
    -- self._menu:SelectFItem(0)
    local ids = self._menuData[1][2]
    local items = CardData:CreateCardItem(ids)
    self._Itemlist:SetDataList(items, 2)
    
end

function CardTotalView:Close()
    self._isdelay=false
    self._scrollPage:SetScrollRectState(false)
    self._scrollview1:SetScrollRectState(false)
    self:RemoveEventListeners()
end

function CardTotalView:Update()
    if not self._isdelay then
        self:UpdateList()
        self:OnRefreshBtnRed()
    end
end

function CardTotalView:AddEventListeners()
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._updateList)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_PKG_CARD_PIECE, self._updateList)
	GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_LEVEL, self._updateList)	
   
end

function CardTotalView:RemoveEventListeners()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._updateList)	
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_PKG_CARD_PIECE, self._updateList)	
	GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_LEVEL, self._updateList)		
end

function CardTotalView:OnDestory()
end