--CardUpView.lua
require "UIComponent.Extend.ItemGrid"
require"Modules.ModuleCard.ChildComponent.CardItem"
require"Modules.ModuleCard.ChildComponent.CardUpAttListItem"
require"Modules.ModuleCard.ChildComponent.CardCurAttListItem"
local CardUpView = Class.CardUpView(ClassTypes.BaseComponent)
CardUpView.interface = GameConfig.ComponentsConfig.Component
CardUpView.classPath = "Modules.ModuleCard.ChildView.CardUpView"
local CardItem = ClassTypes.CardItem
local CardUpAttListItem = ClassTypes.CardUpAttListItem
local CardCurAttListItem = ClassTypes.CardCurAttListItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local CardLevelDataHelper = GameDataHelper.CardLevelDataHelper
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local UIList = ClassTypes.UIList
local CardManager=PlayerManager.CardManager
local ItemGrid = ClassTypes.ItemGrid
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local CardData = PlayerManager.PlayerDataManager.cardData
local UIParticle = ClassTypes.UIParticle
local HideScale = Vector3.New(0,1,1)
local XGameObjectTweenScale = GameMain.XGameObjectTweenScale

function CardUpView:Awake()
    self._base.Awake(self)
    self._data={}
    self._items={}
    self._isUping = false
    self._isFxing=false
    self._update = function() self:Update()end
    self._updatacost = function() self:UpdataCost()end
    self:InitView()
end

function CardUpView:InitView()
    self._btnUp=self:FindChildGO("Button_Up")
    self._container1=self:FindChildGO("Container_1")
    self._container2=self:FindChildGO("Container_2")
    self._containerIcon=self:FindChildGO("Container_Icon")
    self._scroViewGo = self:FindChildGO("ScrollViewList")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
	self._list = list
	self._listView = scrollView 
    self._listView:SetHorizontalMove(false)
    self._listView:SetVerticalMove(true)
    self._list:SetItemType(CardUpAttListItem)
    self._list:SetPadding(10, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:UseObjectPool()
    self._list:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._items[1]=self:AddChildLuaUIComponent("Container_1/Container_Cur",CardItem)
    self._items[2]=self:AddChildLuaUIComponent("Container_1/Container_Next",CardItem)
    self._itemfull=self:AddChildLuaUIComponent("Container_2/Container_Full",CardItem)
    self._csBH:AddClick(self._btnUp,function() self:BtnUpClick()end) 
    local a = GUIManager.AddItem(self._containerIcon, 1)
    self._icon = UIComponentUtil.AddLuaUIComponent(a, ItemGrid)
    self._icon:ActivateTipsById()

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_2/ScrollViewList")
	self._list1 = list
	self._listView1 = scrollView 
    self._listView1:SetHorizontalMove(false)
    self._listView1:SetVerticalMove(true)
    self._list1:SetItemType(CardCurAttListItem)
    self._list1:SetPadding(10, 0, 0, 0)
    self._list1:SetGap(0, 0)
    self._list1:UseObjectPool()
    self._list1:SetDirection(UIList.DirectionTopToDown, 1, -1)


    self._fx = UIParticle.AddParticle(self.gameObject, "Container_Fx/fx_ui_30034")
    self._fx:SetLifeTime(2)
    self._fx:Stop()

    self._StartUpGo = self:FindChildGO("Image_StarUp")
    self._StartUpGo.transform.localScale = Vector3.zero

    self._animScaleStarUp = self:AddChildComponent('Image_StarUp', XGameObjectTweenScale)
end

function CardUpView:BtnUpClick()
    if not self._isUping then
        self._isUping = true
        CardManager:CardUpLevel(self._cardid)
    end
    CardManager:SetUpIng(true)
end

function CardUpView:ShowStarUp()
    if not self._isFxing  then
        self._isFxing=true
        self._animScaleStarUp:SetFromToScaleOnce(0.3, 1, 1, function()  
                    self._isFxing=false
                    self._StartUpGo.transform.localScale = Vector3.zero
                end)
        self._fx:Play()
    end
end

function CardUpView:BtnCloseClick()
    self:Close()
    self._listView:SetScrollRectState(false)
    self.transform.localScale=HideScale
end
function CardUpView:Open(d)
    self._listView:SetScrollRectState(true)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._update)
    EventDispatcher:AddEventListener(GameEvents.CardUpdate,self._updatacost)
    self:SetView(d)
end

function CardUpView:SetView(data)
    self._data=data
    if data~=nil then
        local d0 = data
        local d1 = {}
        local f = CardLevelDataHelper:isFullLevel(data._id,data._star)
        local curatts=CardManager:GetCarAtt(data._id,data._star)
        if f then
            self._container1:SetActive(false)
            self._container2:SetActive(true)
            self._itemfull:OnRefreshData(data)
            self._btnUp:SetActive(false)
            self._containerIcon:SetActive(false)
            local d=CardManager:GetCardsUpAtt(curatts)
            local attlistdata={}
            for k,v in pairs(d) do
                attlistdata[k]={v[1],}
            end
            self._list1:SetDataList(d)
            self._scroViewGo:SetActive(false)
            return
        else
            self._containerIcon:SetActive(true)
            self._btnUp:SetActive(true)
            d0 = data
            d1 = CardData:GetCardItemData(data._id,data._star+1)
            self._container1:SetActive(true)
            self._container2:SetActive(false)
            self._items[1]:OnRefreshData(d0)
            self._items[2]:OnRefreshData(d1)
        end
        self._scroViewGo:SetActive(true)
        self._cardid = data._id
        self:SetCost()        
        local nextatts=CardManager:GetCarAtt(d1._id,d1._star)
        local data1=CardManager:GetCardsUpAtt(curatts)
        local data2=CardManager:GetCardsUpAtt(nextatts)
        local attlistdata={}
        for k,v in pairs(data1) do
            attlistdata[k]={data1[k],data2[k]}
        end
        self._list:SetDataList(attlistdata)
    end
end

function CardUpView:UpdataCost()
    self:SetCost()
    self._isUping=false
end

function CardUpView:SetCost()
    if self._data then
        local nextatts = CardData:GetCardItemData(self._data._id,self._data._star+1)
        local cost=CardLevelDataHelper:GetCardCost(self._data._id,self._data._star)
        if cost then
            local costid=0
            local num=0
            costid,num=next(cost)
            self._icon:SetItem(costid)
            local c = CardManager:GetPiece(costid)
            local msg = ""
            if c<num then
                msg = BaseUtil.GetColorString(c.."/"..num,ColorConfig.H)
            else
                msg = BaseUtil.GetColorString(c.."/"..num,ColorConfig.J)
            end
            self._icon:SetCountString(msg)
        end
    end
end

function CardUpView:Close()
    self._isUping=false
    self._isFxing = false
    CardManager:SetUpIng(false)
    CardManager:RefreshCardTotalEvent()
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_CARD_LIST, self._update)	
    EventDispatcher:RemoveEventListener(GameEvents.CardUpdate,self._updatacost)
end

function CardUpView:Update()
    local cardbag = GameWorld.Player().card_list
    local star = cardbag[self._data._id]
    local item = CardData:GetCardItemData(self._data._id,star)  
    self:SetView(item)
    self:ShowStarUp()
end

function CardUpView:OnDestory()
    self._csBH = nil
end