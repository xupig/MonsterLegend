--CardBagView.lua
require "Modules.ModuleCard.ChildComponent.CardBagListItem"
require "Modules.ModuleCard.ChildComponent.CardBagDropItem"
local CardBagView = Class.CardBagView(ClassTypes.BaseComponent)
local CardBagListItem = ClassTypes.CardBagListItem
CardBagView.interface = GameConfig.ComponentsConfig.Component
CardBagView.classPath = "Modules.ModuleCard.ChildView.CardBagView"
local CardData =  PlayerManager.PlayerDataManager.cardData
local UIList = ClassTypes.UIList
-- local UINavigationMenu = ClassTypes.UINavigationMenu
local CardBagDropItem = ClassTypes.CardBagDropItem
local CardManager=PlayerManager.CardManager
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
-- local HidePosition = Vector3.New(0,4000,0)
local HideScale = Vector3.New(0,1,1)

function CardBagView:Awake()
    self._index = {75518,75519,75520,75521,75522,75523}
    self._flage = false
    self:InitView()
end

function CardBagView:InitView()  
    local scrollview,list = UIList.AddScrollViewList(self.gameObject,'ScrollViewList')
    self._list0 = list
    self._scrollview = scrollview
    self._list0:SetItemType(CardBagListItem)
    self._list0:SetPadding(0,0,0,0)
    self._list0:SetGap(10,20)
    self._list0:SetDirection(UIList.DirectionLeftToRight,8,-1)
    local btnClose = self:FindChildGO("Button_Close")
    self._csBH:AddClick(btnClose,function() self:BtnCloseClick()end)
    self._textdes = self:GetChildComponent('Container_Grade/Text_FilterName','TextMeshWrapper')
    self._textdes.text = LanguageDataHelper.GetContent(75523)
    self._texttips = self:GetChildComponent('Text_Des','TextMeshWrapper')
    self._texttips.text = LanguageDataHelper.GetContent(75292)
    local btnSele = self:FindChildGO("Container_Grade/Button_Expand")
    self._ImgContract = self:FindChildGO("Container_Grade/Image_Contract")
    self._ImgExpand = self:FindChildGO("Container_Grade/Image_Expand")
    self._csBH:AddClick(btnSele,function() self:BtnSeleClick()end)

    self._containerscro = self:FindChildGO("Container_Grade/Container_List")
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Grade/Container_List/ScrollViewList_Type")
    self._list1 = list
    self._scrollView1 = scrollView
    self._list1:SetItemType(CardBagDropItem)
    self._list1:SetPadding(0, 0, 0, 0)
    self._list1:SetGap(0, 6)
    self._list1:SetDirection(UIList.DirectionTopToDown, 1, 1)
    local itemClickedCB =                
	function(idx)
		self:OnListItemClicked(idx)
	end
    self._list1:SetItemClickedCB(itemClickedCB)
    self._containerscro:SetActive(false)
    self._textTitle = self:GetChildComponent("Text_Title","TextMeshWrapper")
    self._textTitle.text=LanguageDataHelper.GetContent(75528)
end

function CardBagView:OnListItemClicked(idx)
    self._textdes.text = LanguageDataHelper.GetContent(self._index[idx+1])
    self._containerscro:SetActive(false)
    self:BtnSeleClick()
    local item = self._list1:GetItem(idx)
    local quality = item:GetQ()
    local d ={}
    if quality==0 then
        d = CardManager:GetAllBagPiece()
    else
        d = CardManager:GetBagPieceByQ(quality)
    end
    self._list0:RemoveAll()
    --self._list0:SetDataList(d)
    self._list0:SetDataList(d, 5)--使用协程加载
end

function CardBagView:BtnSeleClick()
    self._flage = not self._flage
    self._ImgExpand:SetActive(self._flage)
    self._ImgContract:SetActive(not self._flage)
    self._containerscro:SetActive(self._flage)
    local d = CardManager:GetBagDrop()
    self._list1:SetDataList(d)
end

function CardBagView:BtnCloseClick()
    self._scrollview:SetScrollRectState(false)
    -- self.gameObject.transform.anchoredPosition=HidePosition
    self.gameObject.transform.localScale= HideScale
end
function CardBagView:Open(data)
    self._scrollview:SetScrollRectState(true)
    local d = CardManager:GetAllBagPiece()
    self._list0:RemoveAll()
    --self._list0:SetDataList(d)
    self._list0:SetDataList(d, 5)--使用协程加载
    local d = CardManager:GetBagDrop()
    self._list1:RemoveAll()
    self._list1:SetDataList(d)
end

function CardBagView:Close()
end

function CardBagView:Update(data)
  
end

function CardBagView:OnDestory()
end