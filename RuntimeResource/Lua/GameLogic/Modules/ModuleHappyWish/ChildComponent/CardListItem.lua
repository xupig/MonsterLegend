-- CardListItem.lua
local CardListItem = Class.CardListItem(ClassTypes.UIListItem)
CardListItem.interface = GameConfig.ComponentsConfig.Component
CardListItem.classPath = "Modules.ModuleCardLuck.ChildComponent.CardListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local public_config = GameWorld.public_config
local UIList = ClassTypes.UIList
local CardLuckManager = PlayerManager.CardLuckManager
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function CardListItem:__ctor__()
    self._state=nil
end

function CardListItem:Awake()
    self:InitItem()


    if self._state==nil then
        return
    end
    self:SetState(self._state)

end

function CardListItem:OnDestroy()

end

function CardListItem:InitItem()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)

    self._imgBg1 = self:FindChildGO("Image_Bg_1")
    self._imgBg2 = self:FindChildGO("Image_Bg_2")
    self._imgBgCrown = self:FindChildGO("Image_Crown")
end

function CardListItem:OnRefreshData(data)
    if data then
        local positionList = CardLuckManager:GetPositionInfo()
        ---LoggerHelper.Error("OpenRankView:CloseView()"..PrintTable:TableToStr(positionList))
            if positionList[data._posId]~=nil then
                if positionList[data._posId] == -1 then
                    self._imgBgCrown:SetActive(true)
                    self._imgBg1:SetActive(false)
                    self._imgBg2:SetActive(true)
                else
                   --LoggerHelper.Error("OpenRankView:CloseView()"..positionList[data._posId])
                   self._imgBg1:SetActive(false)
                   self._imgBgCrown:SetActive(false)
                   self._imgBg2:SetActive(true)
                   self._itemIcon:ActivateTipsById()
                   self._itemIcon:SetItem(tonumber(positionList[data._posId]), 1)
                   --self._itemIcon:SetItem(tonumber(3103), 1)
                end 
            else
                self._imgBg1:SetActive(true)
                self._imgBg2:SetActive(false)
                self._imgBgCrown:SetActive(false)
            end
        

        -- local itemId = data._id
        -- local num = data._num
        --SendOpenCardReq
        self:UpdateRed()
    end
end

function CardListItem:UpdateRed()

end


	



function CardListItem:SetState(state)

end