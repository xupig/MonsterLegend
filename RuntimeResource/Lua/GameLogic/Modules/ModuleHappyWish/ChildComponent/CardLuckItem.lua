-- CardLuckItem.lua
local CardLuckItem = Class.CardLuckItem(ClassTypes.BaseComponent)

CardLuckItem.interface = GameConfig.ComponentsConfig.PointableComponent
CardLuckItem.classPath = "Modules.ModuleCardLuck.ChildComponent.CardLuckItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local WelfareDayRewardDataHelper = GameDataHelper.WelfareDayRewardDataHelper
local RectTransform = UnityEngine.RectTransform
local TipsManager = GameManager.TipsManager

local DailyActivityExManager = PlayerManager.DailyActivityExManager
local WelfareManager = PlayerManager.WelfareManager
local UIParticle = ClassTypes.UIParticle
--override
function CardLuckItem:Awake()

end

--override
function CardLuckItem:OnDestroy()
    self._canGetImage = nil
    self._alreadyGetContainer = nil
    self._itemContainer = nil
    self._icon = nil
    self._pointText = nil
    self._fx_ui_30011:Stop()
end

--override
function CardLuckItem:OnPointerDown(pointerEventData)
end

--override
function CardLuckItem:OnPointerUp(pointerEventData)
    --LoggerHelper.Log("CardLuckItem:pointerEventData")
    local levelRewardInfo = WelfareManager:GetCumulativeSignInfo() 
    --LoggerHelper.Log("CardLuckItem:levelRewardInfo"..levelRewardInfo)
    if self._index  == levelRewardInfo+1 then
        if WelfareManager:GetTotalSignDayNum() >= WelfareDayRewardDataHelper:GetDay(self._id) then
            WelfareManager:SendCumulativeRewardReq(self._id)
        else
            TipsManager:ShowItemTipsById(self._itemId)
        end
    else
        TipsManager:ShowItemTipsById(self._itemId)
    end


    
--     -- if self._puFunc ~= nil then
--     -- pointerEventData = Global.ChangeScreenClickPosition(pointerEventData)
--     local status = self:GetStatus()
--     if status == ServiceActivityConfig.None then
--         TipsManager:ShowItemTipsById(self._itemId)
--     elseif status == ServiceActivityConfig.CAN_GET then
--         DailyActivityExManager:GetReward(self._id)
--     elseif status == ServiceActivityConfig.ALREADY_GET then
--         end
-- -- end
end

function CardLuckItem:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function CardLuckItem:StopFx()
    self._fx_ui_30011:Stop()
end

function CardLuckItem:SetPointerUpCB(func)
    self._puFunc = func
end

function CardLuckItem:InitData(id,length,index)
    if id then
        self._id = id
        self._index = index
        local rewards = WelfareDayRewardDataHelper:GetReward(id)
        local day = WelfareDayRewardDataHelper:GetDay(id)
        local max = WelfareDayRewardDataHelper:GetMaxDay()

        local point = 0
        local ids = WelfareDayRewardDataHelper:GetAllId()
        local needIds = {}
        for i, id in ipairs(ids) do
            local levelTab = WelfareDayRewardDataHelper:GetLv(id)
            local sumId = {}  
            if GameWorld.Player().level >= levelTab[1] and GameWorld.Player().level <= levelTab[2] then
                table.insert(needIds, id)
            end
        end

        for i,v in ipairs(needIds) do
            if day == WelfareDayRewardDataHelper:GetDay(v) then
                point = max/(#needIds)*i
            end
        end


        self:RefreshStatus()
  
        self._itemId = rewards[1][1]
        if WelfareDayRewardDataHelper:GetBindFlag(id) == 1 then
            self._icon:SetBind(true)
        else
            self._icon:SetBind(false)
        end
        self._icon:SetItem(rewards[1][1], rewards[1][2], 0, true)
        self._pointText.text = day
        self.gameObject:GetComponent(typeof(RectTransform)).transform.localPosition = Vector3.New(length * point / max, 0, 0)
        self:RefreshStatus()
    end
end



function CardLuckItem:RefreshStatus()
    local levelRewardInfo = WelfareManager:GetCumulativeSignInfo()
    --LoggerHelper.Log("CardLuckItem:levelRewardInfo"..levelRewardInfo.."self._id"..self._id)
    if self._index  <= levelRewardInfo then
         self._alreadyGetContainer:SetActive(true)
    elseif self._index  > levelRewardInfo then
        self._alreadyGetContainer:SetActive(false)
    end

    if self._index  == levelRewardInfo+1 then
        if WelfareManager:GetTotalSignDayNum() >= WelfareDayRewardDataHelper:GetDay(self._id) then
            self._canGetImage:SetActive(true)
            self:PlayFx()
        else
            self._canGetImage:SetActive(false)
            self:StopFx()
        end
    else
        self._canGetImage:SetActive(false)
        self:StopFx()
    end
    -- local status = self:GetStatus()
    -- if status == DailyConfig.None then
    --     self._canGetImage:SetActive(false)
    --     self._alreadyGetContainer:SetActive(false)
    -- elseif status == DailyConfig.CAN_GET then
    --     self._canGetImage:SetActive(true)
    --     self._alreadyGetContainer:SetActive(false)
    -- elseif status == DailyConfig.ALREADY_GET then
    --     self._alreadyGetContainer:SetActive(true)
    --     self._canGetImage:SetActive(false)
    -- end
end

function CardLuckItem:GetStatus()
    -- local point = WelfareDayRewardDataHelper:GetCostPoint(self._id)
    -- local rewardsRecord = GameWorld.Player().daily_activity_point_reward
    -- local dailyPoints = GameWorld.Player().daily_activity_point
    -- if rewardsRecord and rewardsRecord[self._id] then
    --     return DailyConfig.ALREADY_GET
    -- else
    --     if dailyPoints >= point then
    --         return DailyConfig.CAN_GET
    --     else
    --         return DailyConfig.None
    --     end
    -- end
    -- return DailyConfig.None
end
