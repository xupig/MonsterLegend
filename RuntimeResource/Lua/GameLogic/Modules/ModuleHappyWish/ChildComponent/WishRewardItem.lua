-- WishRewardItem.lua
local WishRewardItem = Class.WishRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
WishRewardItem.interface = GameConfig.ComponentsConfig.Component
WishRewardItem.classPath = "Modules.ModuleHappyWish.ChildComponent.WishRewardItem"
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function WishRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function WishRewardItem:OnDestroy() 

end

--override
function WishRewardItem:OnRefreshData(data)
    if data then
        --LoggerHelper.Log("WishRewardItem:OnRefreshData(data)"..PrintTable:TableToStr(data))
        -- local reward = OpenCardRewardDataHelper:GetReward(id)
        -- LoggerHelper.Error("WishRewardItem:OnRefreshData(id)"..PrintTable:TableToStr(reward))
        self._itemIcon:SetItem(data[1], data[2][1], 0, true)
        self._itemIcon:ActivateTipsById()
    end
end