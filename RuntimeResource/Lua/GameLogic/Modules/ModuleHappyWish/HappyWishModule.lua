--CardLuckModule.lua
HappyWishModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function HappyWishModule.Init()
	GUIManager.AddPanel(PanelsConfig.HappyWish,"Panel_HappyWish",GUILayer.LayerUIPanel,"Modules.ModuleHappyWish.HappyWishPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return HappyWishModule
