﻿
local ClassTypes = ClassTypes
local HappyWishPanel = Class.HappyWishPanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GUIManager = GameManager.GUIManager

local UIToggleGroup = ClassTypes.UIToggleGroup
local UIToggle = ClassTypes.UIToggle
local UIList = ClassTypes.UIList
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local OpenCardRewardDataHelper = GameDataHelper.OpenCardRewardDataHelper
local CardLuckManager = PlayerManager.CardLuckManager
local public_config = GameWorld.public_config
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local PromisDisplayDataHelper = GameDataHelper.PromisDisplayDataHelper
local PromisImportantDataHelper = GameDataHelper.PromisImportantDataHelper
local HappyWishManager = PlayerManager.HappyWishManager
local TimerHeap = GameWorld.TimerHeap
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local DateTimeUtil = GameUtil.DateTimeUtil
local ActorModelComponent = GameMain.ActorModelComponent
local UIParticle = ClassTypes.UIParticle
local TransformDataHelper = GameDataHelper.TransformDataHelper
local SettingType = GameConfig.SettingType
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local PlayerModelComponent = GameMain.PlayerModelComponent
local SortOrderedRenderAgent = GameMain.SortOrderedRenderAgent
local Move_Y = 20


require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

require "Modules.ModuleHappyWish.ChildComponent.WishRewardItem"
local WishRewardItem = ClassTypes.WishRewardItem


function HappyWishPanel:Awake()
     self:InitCom()
     self:InitView()
     self._happyWishResp = function()self:RefreshView() end 
     self._dayUpdateResp = function()self:RefreshView() end
end

function HappyWishPanel:InitCom()
    self._btnClose = self:FindChildGO("Container_Total/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:OnCloseWishClick() end)

    self._btnWish = self:FindChildGO("Container_Wish/Button_Wish")
    self._csBH:AddClick(self._btnWish, function()self:OnWishClick() end)

    self._btnAutoWish = self:FindChildGO("Container_Wish/Button_AutoWish")
    self._csBH:AddClick(self._btnAutoWish, function()self:OnAutoWishClick() end)

    self._btnStopWish = self:FindChildGO("Container_Wish/Button_StopWish")
    self._csBH:AddClick(self._btnStopWish, function()self:OnStopWishClick() end)


    self._btnWishWrapper = self:GetChildComponent("Container_Wish/Button_Wish", "ButtonWrapper")
    self._btnAutoWishWrapper = self:GetChildComponent("Container_Wish/Button_AutoWish", "ButtonWrapper")
    self._btnStopWishWrapper = self:GetChildComponent("Container_Wish/Button_StopWish", "ButtonWrapper")
    
    self._txtWishProgress = self:GetChildComponent("Container_Wish/Text_Percent", 'TextMeshWrapper')

    self._txtWishTime = self:GetChildComponent("Container_Wish/Text_Time", 'TextMeshWrapper')

    self._itemContainer = self:FindChildGO("Container_Wish/Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)

    self._itemBigContainer = self:FindChildGO("Container_Wish/Container_Icon_Big")

    self._leftModelGO = self:FindChildGO('Container_Wish/Container_Model')
    self._leftModelComponent = self:AddChildComponent('Container_Wish/Container_Model', ActorModelComponent)

    self._playerActorComponent = self:AddChildComponent('Container_Wish/Container_PlayerModel', PlayerModelComponent)
    -- self._playerActorComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._fx1 = UIParticle.AddParticle(self.gameObject, "Container_Wish/Container_Fx/FirstCharge")
    self._fx1:Play(true)
    self._btnStopWish:SetActive(false)
    self._btnAutoWish:SetActive(true)

    self._wishBar = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Wish/ProgressBar")
    self._wishBar:SetProgress(0)
    self._wishBar:ShowTweenAnimate(true)
    self._wishBar:SetMutipleTween(true)
    self._wishBar:SetIsResetToZero(false)
    self._wishBar:SetTweenTime(0.2)

    self._fx_Container = self:FindChildGO("Container_Wish/Container_Fx_Icon")
    self._fx_ui_30011 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30011")
    self:StopFx()


    local moveTo = self._itemBigContainer.transform.localPosition
    moveTo.y = moveTo.y + Move_Y
    self._tweenPosBig = self:AddChildComponent('Container_Wish/Container_Icon_Big', XGameObjectTweenPosition)
    self._tweenPosBig:SetToPosTween(moveTo, 2, 4, 4, nil)
end

function HappyWishPanel:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function HappyWishPanel:StopFx()
    self._fx_ui_30011:Stop()
end

function HappyWishPanel:OnWishClick()
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = GlobalParamsHelper.GetMSortValue(865)[1][2]  
    GUIManager.ShowDailyTips(SettingType.HAPPY_WISH_NEED, function() HappyWishManager:SendWishReq() end, function() self:OnStopWishClick() end, LanguageDataHelper.CreateContent(81587,argsTable), true) 
end


function HappyWishPanel:OnAutoWishClick()
    if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]-GlobalParamsHelper.GetMSortValue(865)[1][2] >=0 then
        self._inAuto = true
        self._btnStopWish:SetActive(true)
        self._btnAutoWish:SetActive(false)
        self:OnWishClick()
    else 
       --LoggerHelper.Error("self:OnAutoWishClick")
        self:OnWishClick()
        self:OnStopWishClick()
       
    end
end

function HappyWishPanel:OnStopWishClick()
    --LoggerHelper.Error("self:OnStopWishClick")
    if self._inAuto then
        self._inAuto = false
        TimerHeap:DelTimer(self._autoTimer)
        self._btnStopWish:SetActive(false)
        self._btnAutoWish:SetActive(true)
    end
end


function HappyWishPanel:OnCloseTicketClick()
    --CardLuckManager:SendOpenCardReq(self._curIndex+1)
end

function HappyWishPanel:ToggeleStateInit()
   -- self._toggleIntrest:
end


function HappyWishPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.HappyWishResp, self._happyWishResp)
    EventDispatcher:AddEventListener(GameEvents.OnDayUpdate,self._dayUpdateResp)
end

function HappyWishPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.HappyWishResp, self._happyWishResp)
    EventDispatcher:RemoveEventListener(GameEvents.OnDayUpdate,self._dayUpdateResp)
end




function HappyWishPanel:Start()

end

function HappyWishPanel:UpdateFunOpen()


end

function HappyWishPanel:OnCloseWishClick()
    GUIManager.ClosePanel(PanelsConfig.HappyWish)
end

function HappyWishPanel:OnClose()
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
    GameWorld.ShowPlayer():ShowModel(3)
    self._iconBg = nil   
    self:OnStopWishClick()
    self:RemoveEventListeners()
end

function HappyWishPanel:RefreshView()
    local wishInfo = HappyWishManager:GetWishLuck()
    --local max = PromisDisplayDataHelper:GetMaxPromis(1)
    local displayId = HappyWishManager:GetNeedId()
    local max = PromisDisplayDataHelper:GetMaxPromis(displayId)
    local wishEndInfo = HappyWishManager:GetWishEnd()
    if tonumber(wishEndInfo) == 1 then
        self._btnWishWrapper.interactable = false
        self._btnAutoWishWrapper.interactable = false
        self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
        self._txtWishProgress.text = "0".."/"..max
    else
        self._btnWishWrapper.interactable = true
        self._btnAutoWishWrapper.interactable = true
        self._btnStopWishWrapper.interactable = true
        self._wishBar:SetProgress(wishInfo/max)
        self._txtWishProgress.text = wishInfo.."/"..max
    end


    if GameWorld.Player()[ItemConfig.MoneyTypeMap[public_config.MONEY_TYPE_COUPONS]]-GlobalParamsHelper.GetMSortValue(865)[1][2] >=0 then
        if self._inAuto then
            if self._autoTimer then
                TimerHeap:DelTimer(self._autoTimer)
                self._autoTimer = nil
            end
            self._autoTimer = TimerHeap:AddSecTimer(0.5,0,0,function ()
                self:OnWishClick()
            end)  
        end
    else 
        if self._inAuto~=nil then
            self:OnWishClick()
        end
        self:OnStopWishClick()
    end

end



function HappyWishPanel:InitView()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Wish/ScrollViewList_Item")
    self._cardList = list
    self._cardView = scrollview
    self._cardList:SetItemType(WishRewardItem)
    self._cardList:SetPadding(5, 0, 0, 0)
    self._cardList:SetGap(25, 25)
    self._cardList:SetDirection(UIList.DirectionLeftToRight, 6, 1)
    local itemBTClickedCB =
        function(index)
            self:OnCardItemBTClicked(index)
        end
    self._cardList:SetItemClickedCB(itemBTClickedCB)

end

function HappyWishPanel:OnItemBTClicked(index)

end

function HappyWishPanel:OnCardItemBTClicked(index)

end



function HappyWishPanel:GuideGift(funId)

end




--override
function HappyWishPanel:OnDestroy()
    self._fx_ui_30011:Stop()
end

--override
function HappyWishPanel:OnShow(data)
    self._iconBg = self:FindChildGO("Container_Total/Image")
    GameWorld.AddIcon(self._iconBg,13512)
    -- self:RefreshView()
    self:AddEventListeners()
    self:PlayFx()
    local displayId = HappyWishManager:GetNeedId()
    local itemList = PromisDisplayDataHelper:GetReward(displayId)
    --LoggerHelper.Error("itemList:"..PrintTable:TableToStr(itemList))
    self._cardList:SetDataList(itemList)

    local importantIconId =  PromisImportantDataHelper:GetImportant(displayId)
    self._itemIcon:SetItem(importantIconId, 0, 0, true)
    self._itemIcon:ActivateTipsById()
    --Container_Icon

    local iconList = PromisImportantDataHelper:GetDisplay(displayId)
    --LoggerHelper.Error("iconList"..PrintTable:TableToStr(iconList))
    --self._iconWear:SetItem(, nil)     
   
    GameWorld.ShowPlayer():ShowModel(1)
    self._playerActorComponent:SetStartSetting(Vector3(0, 0, -300), Vector3(0, 180, 0), 1)

    self._iconType = iconList[1]
    if iconList[1] == 1 then
        self._leftModelGO:SetActive(true)
        self._itemBigContainer:SetActive(false)
        self._playerActorComponent:Hide()
        local partnerTransformCfg = TransformDataHelper.GetTreasureTotalCfg(iconList[2])
        local vocationGroup = math.floor(GameWorld.Player().vocation/100)
        local uiModelCfgId = partnerTransformCfg.model_vocation[vocationGroup]
        local modelInfo = TransformDataHelper.GetUIModelViewCfg(uiModelCfgId)
        local modelId = modelInfo.model_ui
        local posx = modelInfo.pos[1]
        local posy = modelInfo.pos[2]
        local posz = modelInfo.pos[3]
        local rotationx = modelInfo.rotation[1]
        local rotationy = modelInfo.rotation[2]
        local rotationz = modelInfo.rotation[3]
        local scale = modelInfo.scale
        self._leftModelComponent:SetStartSetting(Vector3(posx, posy, posz), Vector3(rotationx, rotationy, rotationz), scale)
        self._leftModelComponent:LoadModel(modelId)
    elseif iconList[1] == 0 then
        self._leftModelGO:SetActive(false)
        self._itemBigContainer:SetActive(true)
        self._playerActorComponent:Hide()
        GameWorld.AddIcon(self._itemBigContainer,iconList[2])
    elseif iconList[1] == 2 then
        -- LoggerHelper.Error("bigPic[2] =====" .. tostring(bigPic[2]))
        self._playerActorComponent:Show()
        self._leftModelGO:SetActive(false)
        self._itemBigContainer:SetActive(false)
        self._effect = iconList[2]
        GameWorld.ShowPlayer():SetCurEffect(self._effect)
        GameWorld.ShowPlayer():SetCurWing(0)
        TimerHeap:AddTimer(200, 0, 0, function() SortOrderedRenderAgent.ReorderAll() end)
    end

    local leftTime = HappyWishManager:GetLeftTime()
    if leftTime > 0 then
        if self._timer then
            TimerHeap:DelTimer(self._timer)
            self._timer = nil
        end
        self:UpdateTimeLeft()
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()self:OnBackToActivityTime() end)
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateHappy)
        self._txtWishTime.text = LanguageDataHelper.CreateContent(76951)
        self._btnWishWrapper.interactable = false
        self._btnAutoWishWrapper.interactable = false
        self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
    end

  
    local wishEndInfo = HappyWishManager:GetWishEnd()
    if tonumber(wishEndInfo) == 1 then
        self._btnWishWrapper.interactable = false
        self._btnAutoWishWrapper.interactable = false
        self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
    else
        self._btnWishWrapper.interactable = true
        self._btnAutoWishWrapper.interactable = true
        self._btnStopWishWrapper.interactable = true
    end
    self:RefreshView()
    --LoggerHelper.Error("HappyWishManager:OnPlayerEnterWorld()"..PrintTable:TableToStr(itemList))
end

function HappyWishPanel:SetData(data)
    --self:RefreshView()
end

function HappyWishPanel:ShowView(index, data)

end

function HappyWishPanel:UpdateTimeLeft()
    self._leftTime = HappyWishManager:GetLeftTime()
    local argsTableRank = LanguageDataHelper.GetArgsTable()
    argsTableRank["0"] = DateTimeUtil.FormatParseTimeStr(self._leftTime%(24*60*60))
    if self._leftTime <= 24*60*60 then
        self._txtWishTime.text = LanguageDataHelper.CreateContent(81653, argsTableRank)
    else
        self._txtWishTime.text = LanguageDataHelper.CreateContent(81652, argsTableRank)
    end
end

function HappyWishPanel:OnBackToActivityTime()
    if (self._leftTime > 1) then
        self:UpdateTimeLeft()
    else
        EventDispatcher:TriggerEvent(GameEvents.UpdateHappy)
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
        self._txtWishTime.text = LanguageDataHelper.CreateContent(76951)
        self._btnWishWrapper.interactable = false
        self._btnAutoWishWrapper.interactable = false
        self._btnStopWishWrapper.interactable = false
        self._wishBar:SetProgress(0)
    end
end


function HappyWishPanel:BaseShowModel()
    if self._iconType == 0 then
        self._leftModelGO:SetActive(false)
        self._itemBigContainer:SetActive(true)
        self._playerActorComponent:Hide()
    elseif self._iconType == 1 then
        self._leftModelGO:SetActive(true)
        self._itemBigContainer:SetActive(false)
        self._playerActorComponent:Hide()
    elseif self._iconType == 2 then
        self._playerActorComponent:Show()
        self._leftModelGO:SetActive(false)
        self._itemBigContainer:SetActive(false)
        GameWorld.ShowPlayer():SetCurEffect(self._effect)
        GameWorld.ShowPlayer():SetCurWing(0)
    end
end

function HappyWishPanel:BaseHideModel()
    self._leftModelGO:SetActive(false)
    self._itemBigContainer:SetActive(false)
    self._playerActorComponent:Hide()
    GameWorld.ShowPlayer():SyncShowPlayerFacade()
end