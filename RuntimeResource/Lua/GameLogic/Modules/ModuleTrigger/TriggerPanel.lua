-- TriggerPanel.lua
local TriggerPanel = Class.TriggerPanel(ClassTypes.BasePanel)
local GUIManager = GameManager.GUIManager
local FlyTriggerManager = PlayerManager.FlyTriggerManager
local TimerHeap = GameWorld.TimerHeap

function TriggerPanel:Awake()
	self._btnFlyTrigger = self:FindChildGO("Button_Trigger")
	self._csBH:AddClick(self._btnFlyTrigger, function()self:OnTriggerClick() end)
end

function TriggerPanel:OnShow(data)
	self._iconBg = self:FindChildGO('Image_Bg')
    GameWorld.AddIcon(self._iconBg,3315)
	EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleTrigger.TriggerPanel")
	self._data = data
	self._timer = TimerHeap:AddSecTimer(10, 0, 0, function()self:OnTriggerClick()  end)
end

function TriggerPanel:OnTriggerClick()
	EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_fly_trigger_ui_button")
	
	--FlyTriggerManager:OnStart(self._data[1],self._data[2],self._data[3],self._data[4])
	self._iconBg=nil

	if self._timer then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
	GUIManager.ClosePanel(PanelsConfig.Trigger)
end


