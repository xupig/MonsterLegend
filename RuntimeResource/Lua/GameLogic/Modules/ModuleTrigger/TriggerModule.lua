-- TriggerModule.lua
TriggerModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TriggerModule.Init()
    GUIManager.AddPanel(PanelsConfig.Trigger,"Panel_Trigger",GUILayer.LayerUIPanel,"Modules.ModuleTrigger.TriggerPanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return TriggerModule