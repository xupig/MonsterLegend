--EnemyAvatarInfoModule.lua
EnemyAvatarInfoModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function EnemyAvatarInfoModule.Init()
     GUIManager.AddPanel(PanelsConfig.EnemyAvatarInfo,"Panel_EnemyAvatarInfo",GUILayer.LayerUIMain,"Modules.ModuleEnemyAvatarInfo.EnemyAvatarInfoPanel",true,false, nil, false, PanelsConfig.NOT_DESTROY)
end

return EnemyAvatarInfoModule