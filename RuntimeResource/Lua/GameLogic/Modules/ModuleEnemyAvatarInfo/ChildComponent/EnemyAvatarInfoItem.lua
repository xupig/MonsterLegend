local EnemyAvatarInfoItem = Class.EnemyAvatarInfoItem(ClassTypes.BaseComponent)

EnemyAvatarInfoItem.interface = GameConfig.ComponentsConfig.PointableComponent
EnemyAvatarInfoItem.classPath = "Modules.ModuleEnemyAvatarInfo.ChildComponent.EnemyAvatarInfoItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local public_config = GameWorld.public_config
local GUIManager = GameManager.GUIManager
local EntityConfig = GameConfig.EntityConfig
local RoleDataHelper = GameDataHelper.RoleDataHelper

function EnemyAvatarInfoItem:Awake()
    self:InitView()
    self:InitCallBack()
end

--override
function EnemyAvatarInfoItem:OnDestroy() 

end

function EnemyAvatarInfoItem:OnEnable() 
    self:AddEventListeners()
end

function EnemyAvatarInfoItem:OnDisable() 
    self:RemoveEventListeners()
end

function EnemyAvatarInfoItem:InitView()
    self._icon = self:FindChildGO("Image_Mask/Container_Icon")
    self._bgImage = self:FindChildGO("Image_BG1")
    self._hpGO = self:FindChildGO("Image_HP")
    self._hpImage = self._hpGO:GetComponent("ImageWrapper")
    self._attackTextGO = self:FindChildGO("Text_Attack")
    self._attackText = self:GetChildComponent("Text_Attack", "TextMeshWrapper")
    self._attackText.text = "正在攻击你"

    self._nameText = self:GetChildComponent("Text_Name", "TextMeshWrapper")
end

function EnemyAvatarInfoItem:InitCallBack()
    self._onHPChange = function() self:OnHPChange() end
    self._onPlayerTargetIdChange = function(id) self:OnPlayerTargetIdChange(id) end
end

function EnemyAvatarInfoItem:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.Refresh_Target_Id, self._onPlayerTargetIdChange)
end

function EnemyAvatarInfoItem:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.Refresh_Target_Id, self._onPlayerTargetIdChange)
end

function EnemyAvatarInfoItem:RefreshAvatarId(eid)
    if eid > 0 then
        self.entity = GameWorld.GetEntity(eid)
        if self.entity ~= nil then
            self:OnHPChange()
            self._nameText.text = self.entity.name
            self._icon:SetActive(true)
            local icon = RoleDataHelper.GetHeadIconByVocation(self.entity.vocation)
            GameWorld.AddIcon(self._icon, icon)
            self.entity:AddPropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChange)
            self.entity:AddPropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChange)
        end
    else
        if self.entity ~= nil then
            self.entity:RemovePropertyListener(EntityConfig.PROPERTY_CUR_HP, self._onHPChange)
            self.entity:RemovePropertyListener(EntityConfig.PROPERTY_MAX_HP, self._onHPChange)
            self.entity = nil
        end
        self._nameText.text = ""
        self._hpImage.fillAmount = 0
        self._attackTextGO:SetActive(false)
        self._icon:SetActive(false)
        self._bgImage:SetActive(true)
    end
    self:OnPlayerTargetIdChange()
end

--state 0:未攻击 1:攻击
function EnemyAvatarInfoItem:RefreshAvatarAttackState(state)
    self._attackTextGO:SetActive(state == 1)
end

function EnemyAvatarInfoItem:OnHPChange()
    if self.entity ~= nil then
        self._hpImage.fillAmount = self.entity.cur_hp/self.entity.max_hp
    end
end

function EnemyAvatarInfoItem:OnPlayerTargetIdChange()
    local id = GameWorld.Player():GetTargetId()
    if self.entity ~= nil and self.entity.id == id then
        self._bgImage:SetActive(false)
    else
        self._bgImage:SetActive(true)
    end
end

function EnemyAvatarInfoItem:OnPointerDown(pointerEventData)
    if self.entity ~= nil then
        GameWorld.Player():SetTargetId(self.entity.id)
    end
end

function EnemyAvatarInfoItem:OnPointerUp(pointerEventData)

end