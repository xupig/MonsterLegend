require "Modules.ModuleEnemyAvatarInfo.ChildComponent.EnemyAvatarInfoItem"

local EnemyAvatarInfoPanel = Class.EnemyAvatarInfoPanel(ClassTypes.BasePanel)

local GUIManager = GameManager.GUIManager
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local EnemyAvatarInfoItem = ClassTypes.EnemyAvatarInfoItem
local MapDataHelper = GameDataHelper.MapDataHelper

local Max_Count = 3

--override
function EnemyAvatarInfoPanel:Awake()
    self:InitView()
    self:InitCallBack()
    self.canShow = false
end

--override
function EnemyAvatarInfoPanel:OnDestroy()

end

function EnemyAvatarInfoPanel:OnShow(data)
    self:AddEventListeners()
    self:RefreshView()
end

function EnemyAvatarInfoPanel:OnClose()
    self:RemoveEventListeners()
    self.canShow = false
end

function EnemyAvatarInfoPanel:InitCallBack()
    self._onAvatarIdChange = function(index, eid) self:OnAvatarIdChange(index, eid) end
    self._onAvatarStateChange = function(index, state) self:OnAvatarStateChange(index, state) end
    self._onEnterMap = function(mapId)self:OnEnterMap(mapId) end
end

function EnemyAvatarInfoPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_ENEMY_AVATAR_ID_CHANGE, self._onAvatarIdChange)
    EventDispatcher:AddEventListener(GameEvents.ON_ENEMY_AVATAR_STATE_CHANGE, self._onAvatarStateChange)
    EventDispatcher:AddEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function EnemyAvatarInfoPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_ENEMY_AVATAR_ID_CHANGE, self._onAvatarIdChange)
    EventDispatcher:RemoveEventListener(GameEvents.ON_ENEMY_AVATAR_STATE_CHANGE, self._onAvatarStateChange)
    EventDispatcher:RemoveEventListener(GameEvents.ENTER_MAP, self._onEnterMap)
end

function EnemyAvatarInfoPanel:InitView()
    self._items = {}
    self._itemGOs = {}
    for i=1,Max_Count do
        self._itemGOs[i] = self:FindChildGO("Container_Item"..i)
        self._items[i] = self:AddChildLuaUIComponent("Container_Item"..i, EnemyAvatarInfoItem)
        self._itemGOs[i]:SetActive(false)
    end
end

function EnemyAvatarInfoPanel:RefreshView()

end

function EnemyAvatarInfoPanel:OnEnterMap(mapId)
    self:Reset()
    self.canShow = MapDataHelper.GetPvpShowMode(mapId) == 1
end

function EnemyAvatarInfoPanel:OnAvatarIdChange(index, eid)
    if not self.canShow then
        return
    end
    if eid > 0 then
        self._itemGOs[index]:SetActive(true)
    else
        self._itemGOs[index]:SetActive(false)
    end
    self._items[index]:RefreshAvatarId(eid)
end

function EnemyAvatarInfoPanel:OnAvatarStateChange(index, state)
    if not self.canShow then
        return
    end
    self._items[index]:RefreshAvatarAttackState(state)
end

function EnemyAvatarInfoPanel:Reset()
    for i=1,Max_Count do
        self._itemGOs[i]:SetActive(false)
    end
end

function EnemyAvatarInfoPanel:WinBGType()
    return PanelWinBGType.NoBG
end