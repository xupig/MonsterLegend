-- OpenRankBuyItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local OpenRankBuyItem = Class.OpenRankBuyItem(ClassTypes.UIListItem)
OpenRankBuyItem.interface = GameConfig.ComponentsConfig.Component
OpenRankBuyItem.classPath = "Modules.ModuleOpenRank.ChildComponent.OpenRankBuyItem"

require  "Modules.ModuleOpenRank.ChildComponent.RankRewardItem"
local UIList = ClassTypes.UIList
local RankRewardItem = ClassTypes.RankRewardItem
local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local WelfareRankRewardDataHelper = GameDataHelper.WelfareRankRewardDataHelper
local WelfareRankSellDataHelper = GameDataHelper.WelfareRankSellDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local public_config = GameWorld.public_config
local XImageFlowLight = GameMain.XImageFlowLight
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager
local TipsManager = GameManager.TipsManager

function OpenRankBuyItem:Awake()
    self:InitItem()
end

function OpenRankBuyItem:OnDestroy()

end

function OpenRankBuyItem:InitItem()

    self._goButtonFlowLight = self:AddChildComponent("Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Button_Get_Reward")
    self._btnAnnounceRewardGetWrapper = self:GetChildComponent("Button_Get_Reward", "ButtonWrapper")
    self._imgRed = self:FindChildGO("Button_Get_Reward/Image_Red")
    self._txtDesc = self:GetChildComponent("Text_Desc", "TextMeshWrapper")

    self._txtDiscount = self:GetChildComponent("Text", "TextMeshWrapper")

    self._txtOrigin = self:GetChildComponent("Text_Desc_Origin", "TextMeshWrapper")
    self._txtCurrent = self:GetChildComponent("Text_Desc_Current", "TextMeshWrapper")
    self._txtLimit = self:GetChildComponent("Text_Desc_Limit", "TextMeshWrapper")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
    self._itemIcon:ActivateTipsById()

    self._originContainer = self:FindChildGO("Container_Icon_Origin")
    self._curContainer = self:FindChildGO("Container_Icon_Cur")
end



function OpenRankBuyItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

function OpenRankBuyItem:AddFriendClick()

end

function OpenRankBuyItem:OnGetRewardClick()
    if self._id then
        -- local btnArgs = {}
        -- self.price = WelfareRankSellDataHelper:GetPrice(self._id)[1][2]
        -- self.buyLimitCount = WelfareRankSellDataHelper:GetNum(self._id)
        -- self.moneyType = WelfareRankSellDataHelper:GetPrice(self._id)[1][1]
        ServiceActivityManager:BuyOpenItem(self._id,1)
        -- btnArgs.openRankBuy = {self._id, self.price, self.buyLimitCount, self.moneyType} --20单价,10限购数量
        -- local itemId = WelfareRankSellDataHelper:GetItem(self._id)[1][1]
        -- TipsManager:ShowItemTipsById(itemId,btnArgs)
        --ServiceActivityManager:SendOpenSeverGetRewardReq(self.giftType)
    end
end

function OpenRankBuyItem:OnRefreshData(id)
    if id then
        self._id = id
        self.giftType = WelfareRankSellDataHelper:GetType(id)
        self._imgRed:SetActive(false)

        if ServiceActivityManager:GetOpenRank(self.giftType)>0 then
            self._btnAnnounceRewardGetWrapper.interactable = true
            self._goButtonFlowLight.enabled = true 
        else
            self._btnAnnounceRewardGetWrapper.interactable = false 
            self._goButtonFlowLight.enabled = false
        end

        local rewardData = WelfareRankSellDataHelper:GetItem(id)
        

        --LoggerHelper.Error("OpenRankBuyItem:OnRefreshData"..PrintTable:TableToStr(rewardData))
        if self._itemIcon then
            self._itemIcon:SetItem(tonumber(rewardData[1][1]), tonumber(rewardData[1][2]))
        end

        -- local rank = WelfareRankSellDataHelper:GetRank(id)
        self._txtDesc.text = ItemDataHelper.GetItemNameWithColor(rewardData[1][1])
        local originData = WelfareRankSellDataHelper:GetOrig(id)
        local curData = WelfareRankSellDataHelper:GetPrice(id)
        GameWorld.AddIcon(self._originContainer, ItemDataHelper.GetIcon(originData[1][1]))
        GameWorld.AddIcon(self._curContainer, ItemDataHelper.GetIcon(curData[1][1]))
        
        local argsRankTable = LanguageDataHelper.GetArgsTable()
        argsRankTable["0"] = originData[1][2]
        self._txtOrigin.text = LanguageDataHelper.CreateContent(80883,argsRankTable)

        local argsRankTable2 = LanguageDataHelper.GetArgsTable()
        argsRankTable2["0"] = curData[1][2]
        self._txtCurrent.text = LanguageDataHelper.CreateContent(80884,argsRankTable2)


        local argsRankTable3 = LanguageDataHelper.GetArgsTable()
        local curNumAlready = GameWorld.Player().open_server_buy_info[id] or 0
        argsRankTable3["0"] = (WelfareRankSellDataHelper:GetNum(id) - curNumAlready).."/"..WelfareRankSellDataHelper:GetNum(id)
        self._txtLimit.text = LanguageDataHelper.CreateContent(80885,argsRankTable3)

        local argsRankTable4 = LanguageDataHelper.GetArgsTable()
        argsRankTable4["0"] = math.modf((curData[1][2]/originData[1][2])*10)
        self._txtDiscount.text = LanguageDataHelper.CreateContent(80922,argsRankTable4)
    end
end
