-- DayRewardItem.lua
local DayRewardItem = Class.DayRewardItem(ClassTypes.UIListItem)

DayRewardItem.interface = GameConfig.ComponentsConfig.Component
DayRewardItem.classPath = "Modules.ModuleOpenRank.ChildComponent.DayRewardItem"

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

local Welfare7dayRewardDataHelper = GameDataHelper.Welfare7dayRewardDataHelper

--override
function DayRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)  
    self._icon:ActivateTipsById()
end

--override
function DayRewardItem:OnDestroy() 
    self._itemContainer = nil
    self._icon = nil
end

function DayRewardItem:OnRefreshData(data)
    self._icon:SetItem(data.id, data.num)
end