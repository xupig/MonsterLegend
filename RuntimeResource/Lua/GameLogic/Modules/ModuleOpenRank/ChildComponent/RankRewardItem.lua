-- RankRewardItem.lua
local RankRewardItem = Class.RankRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
RankRewardItem.interface = GameConfig.ComponentsConfig.Component
RankRewardItem.classPath = "Modules.ModuleOpenRank.ChildComponent.RankRewardItem"

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function RankRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function RankRewardItem:OnDestroy() 

end

--override
function RankRewardItem:OnRefreshData(data)
    if data then
        --LoggerHelper.Log("RankRewardItem:OnRefreshData(data)"..PrintTable:TableToStr(data))
        local itemId = data._id
        local num = data._num
        local isBind = data._isBind

        self._itemIcon:ActivateTipsById()
        self._itemIcon:SetItem(itemId, num)
        if isBind == 1 then
            self._itemIcon:SetBind(true)
        else
            self._itemIcon:SetBind(false)
        end

    end
end