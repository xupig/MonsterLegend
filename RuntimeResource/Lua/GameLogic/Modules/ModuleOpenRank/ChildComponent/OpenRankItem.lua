-- OpenRankItem.lua
require "UIComponent.Extend.ItemGrid"
local ItemGrid = ClassTypes.ItemGrid
require "UIComponent.Extend.ItemGridEx"
local ItemGridEx = ClassTypes.ItemGridEx
local OpenRankItem = Class.OpenRankItem(ClassTypes.UIListItem)
OpenRankItem.interface = GameConfig.ComponentsConfig.Component
OpenRankItem.classPath = "Modules.ModuleOpenRank.ChildComponent.OpenRankItem"

require  "Modules.ModuleOpenRank.ChildComponent.RankRewardItem"
local UIList = ClassTypes.UIList
local RankRewardItem = ClassTypes.RankRewardItem
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIComponentUtil = GameUtil.UIComponentUtil
local WelfareRankRewardDataHelper = GameDataHelper.WelfareRankRewardDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local public_config = GameWorld.public_config
local XImageFlowLight = GameMain.XImageFlowLight

function OpenRankItem:Awake()
    self:InitItem()
end

function OpenRankItem:OnDestroy()

end

function OpenRankItem:InitItem()
    self._notCompleteImgGo = self:FindChildGO("Container_State/Image_NotComplete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Img_Already_Get")

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._imgRed = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    self._txtDesc = self:GetChildComponent("Text_Desc", "TextMeshWrapper")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self:InitView()
end

function OpenRankItem:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "RewardViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(RankRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(10, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._alreadyGetImgGo:SetActive(false)
    self:SetButtonActive(false)
    --self._getRewardButton:SetActive(false)
    self._imgRed:SetActive(false)
    self._notCompleteImgGo:SetActive(false)
end

function OpenRankItem:SetButtonActive(flag)
    self._getRewardButton:SetActive(flag)    
    self._goButtonFlowLight.enabled = flag
end

function OpenRankItem:AddFriendClick()

end

function OpenRankItem:OnGetRewardClick()
    if self.giftType then
        ServiceActivityManager:SendOpenSeverGetRewardReq(self.giftType)
    end
end

function OpenRankItem:OnRefreshData(id)
    if id then
        self.giftType = WelfareRankRewardDataHelper:GetType(id)
        local rank = WelfareRankRewardDataHelper:GetRank(id)
        local rewardData = WelfareRankRewardDataHelper:GetReward(id)
        local target = WelfareRankRewardDataHelper:GetTargets(id)    
        local typeLast = WelfareRankRewardDataHelper:GetTypeLast(id) 
        local desc = LanguageDataHelper.CreateContent(WelfareRankRewardDataHelper:GetDesc(id))
        if ServiceActivityData:GetRewardGrade() then
            --LoggerHelper.Log("ServiceActivityData:GetRewardGrade()"..ServiceActivityData:GetRewardGrade())
            --LoggerHelper.Log("ServiceActivityData:GetIsGetReward()"..ServiceActivityData:GetIsGetReward())
        end

        local _iconList = {}
        for i, v in ipairs(rewardData) do
            if v[1] == GameWorld.Player():GetVocationGroup() then
                for i = 1, #v[2] do
                    if i%3 == 0 then
                        local iconId =  v[2][i-2]
                        local num =  v[2][i-1]
                        local isBind =   v[2][i]
                        local _iconItem = {_id = iconId, _num = num,_isBind = isBind}
                        table.insert( _iconList, _iconItem)                   
                    end
       
                end
            end
        end
        --LoggerHelper.Log("vocation" .. GameWorld.Player().vocation)
        --LoggerHelper.Log("rewardData" .. PrintTable:TableToStr(_iconList))


        --LoggerHelper.Log("WelfareRankRewardDataHelper:GetReward"..PrintTable:TableToStr(rewardData))

 

        if ServiceActivityData:GetRewardGrade() == typeLast then
            if ServiceActivityData:GetIsGetReward() == 1 then
                self._alreadyGetImgGo:SetActive(true)
                self:SetButtonActive(false)
                --self._getRewardButton:SetActive(false)
                self._imgRed:SetActive(false)
                self._notCompleteImgGo:SetActive(false)
            elseif ServiceActivityData:GetIsGetReward() == 0 then
                self._alreadyGetImgGo:SetActive(false)
                self._imgRed:SetActive(true)
                self:SetButtonActive(true)
                --self._getRewardButton:SetActive(true)
                self._notCompleteImgGo:SetActive(false)
            else
                self._alreadyGetImgGo:SetActive(false)
                self._imgRed:SetActive(false)
                self:SetButtonActive(false)
                --self._getRewardButton:SetActive(false)
                self._notCompleteImgGo:SetActive(true)
            end
        else
            
            local leftTime = ServiceActivityManager:GetOpenRank(ServiceActivityData:GetCurOpenType())
            if leftTime>0 then
                self._alreadyGetImgGo:SetActive(false)
                self._imgRed:SetActive(false)
                self:SetButtonActive(false)
                --self._getRewardButton:SetActive(false)
                self._notCompleteImgGo:SetActive(false)
            else
                self._alreadyGetImgGo:SetActive(false)
                self._imgRed:SetActive(false)
                self:SetButtonActive(false)
                --self._getRewardButton:SetActive(false)
                if ServiceActivityData:GetRewardGrade() then
                    --LoggerHelper.Error("ServiceActivityData:GetRewardGrade()"..ServiceActivityData:GetRewardGrade())
                    if typeLast>=ServiceActivityData:GetRewardGrade() then
                        self._notCompleteImgGo:SetActive(false)
                    else
                        self._notCompleteImgGo:SetActive(true)
                    end
                else

                end

            end


        end

        self._list:SetDataList(_iconList)
  
        --LoggerHelper.Log("OpenRankItem:OnRefreshDatatarget" .. PrintTable:TableToStr(target))
        if rank then
            local argsTableBlack = LanguageDataHelper.GetArgsTable()
            if rank[1] == rank[2] then
                argsTableBlack["0"] = rank[1]
            else
                argsTableBlack["0"] = rank[1].."-"..rank[2]
            end
            argsTableBlack["1"] = target[1]
            if ServiceActivityData:GetCurOpenType() == public_config.OPEN_SERVER_RANK_PET or ServiceActivityData:GetCurOpenType() == public_config.OPEN_SERVER_RANK_HORSE then
                argsTableBlack["2"] = target[2]  
            end
            
            self._txtDesc.text = LanguageDataHelper.CreateContentByString(desc, argsTableBlack)
        else
            local argsTableBlack = LanguageDataHelper.GetArgsTable()
            argsTableBlack["0"] = target[1]
            if ServiceActivityData:GetCurOpenType() == public_config.OPEN_SERVER_RANK_PET or ServiceActivityData:GetCurOpenType() == public_config.OPEN_SERVER_RANK_HORSE then
                argsTableBlack["1"] = target[2]  
            end
            self._txtDesc.text = LanguageDataHelper.CreateContentByString(desc, argsTableBlack)
        end
    end
end
