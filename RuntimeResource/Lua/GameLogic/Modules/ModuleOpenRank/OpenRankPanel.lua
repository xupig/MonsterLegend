-- OpenRankPanel.lua
local OpenRankPanel = Class.OpenRankPanel(ClassTypes.BasePanel)

require "Modules.ModuleOpenRank.ChildView.OpenRankView"

local OpenRankView = ClassTypes.OpenRankView

local DateTimeUtil = GameUtil.DateTimeUtil
local GUIManager = GameManager.GUIManager
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local ServiceActivityType = GameConfig.EnumType.ServiceActivityType
local WelfareRankWayDataHelper = GameDataHelper.WelfareRankWayDataHelper
local ServiceActivityManager = PlayerManager.ServiceActivityManager
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local ServiceActivityData = PlayerManager.PlayerDataManager.serviceActivityData
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local OpenRankType = GameConfig.EnumType.OpenRankType
local public_config = GameWorld.public_config


function OpenRankPanel:Awake()
    self:InitComps()
    self:InitCallBack()
    self._onRefreshTab = function()self:RefreshTab() end
end


function OpenRankPanel:InitText()
end

--初始化回调
function OpenRankPanel:InitCallBack()
    -- self:SetTabClickAfterUpdateSecTabCB(function(index)
    --     self:SelectTab(index)
    -- end)
    self:SetSecondTabClickCallBack(function(index)
        self:SelectSecTab(index)
    end)
    
    self:SetCustomizeFunctionOpenTextCB(function(index, secIndex)
        self:OpenTips(index, secIndex)
    end)
end

-- function OpenRankPanel:SelectTab(index)
--     ServiceActivityManager:UpdateOpenRank()
--     if ServiceActivityManager:GetIsOpenRanking() == true then
--     else
--         self:SetFirstTabActive(2, false)
--     end
--     if FunctionOpenDataHelper:CheckFunctionOpen(public_config.FUNCTION_ID_DEMON_RING) then
--     else
--         self:SetFirstTabActive(1, false)
--     end
--     if ServiceActivityManager:GetIsOpenActivity() then
--     else
--         self:SetFirstTabActive(3, false)
--         self:SetFirstTabActive(4, false)
--         self:SetFirstTabActive(5, false)
--     end
--     if self._selectedIndex == 2 then
--         ServiceActivityManager:UpdateOpenRank()
--         for i = OpenRankType.Level, OpenRankType.Combat do
--             if ServiceActivityManager:GetOpenRank(i) == -1 then
--                 --LoggerHelper.Log("OpenRankPanel:SetSecTabFunctionActive========== .." .. i)
--                 self:SetSecTabFunctionActive(i, false)
--             else
--                 self:SetSecTabFunctionActive(i, true)
--             end
--         end
--         local index = ServiceActivityManager:GetCurOpenRankIndex()
--         if tonumber(index)~=0 then
--             self:OnSecTabClick(index)
--         end
--     end
-- end
function OpenRankPanel:SelectSecTab(secIndex)
    if ServiceActivityManager:GetIsOpenRanking() == true then
        self._selectedView:SetSuitType(secIndex)
        ServiceActivityManager:UpdateOpenRank()
    end
end

function OpenRankPanel:OpenTips(index, secIndex)
    self._serTimeSetting = GlobalParamsHelper.GetParamValue(758)
    self._openRankTitle = GlobalParamsHelper.GetParamValue(813)

    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = LanguageDataHelper.CreateContent(self._openRankTitle[secIndex])
    argsTable["1"] = self._serTimeSetting[secIndex][1]
    argsTable["2"] = self._serTimeSetting[secIndex][2]
    GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76980, argsTable))
end

function OpenRankPanel:InitComps()
    self:InitView("Container_OpenRank", OpenRankView, 1, 1)
    self:InitView("Container_OpenRank", OpenRankView, 1, 2)
    self:InitView("Container_OpenRank", OpenRankView, 1, 3)
    self:InitView("Container_OpenRank", OpenRankView, 1, 4)
    self:InitView("Container_OpenRank", OpenRankView, 1, 5)
    self:InitView("Container_OpenRank", OpenRankView, 1, 6)

--self:SetSecondTabClickCallBack(self._secTabCb)
end



function OpenRankPanel:OnShow(data)
    self:RightBgIsActive(false)
    self:UpdateFunctionOpen()
    self:OnShowSelectTab(data)

    ServiceActivityManager:UpdateOpenRank()
    for i = OpenRankType.Level, OpenRankType.Combat do
        if ServiceActivityManager:GetOpenRank(i) == -1 then
            self:SetSecTabFunctionActive(i, false)
        else
            self:SetSecTabFunctionActive(i, true)
        end
    end
    local index = ServiceActivityManager:GetCurOpenRankIndex()
    if tonumber(index) ~= 0 then
        self:OnSecTabClick(index)
    end
end

function OpenRankPanel:OnClose()

end



function OpenRankPanel:StructingViewInit()
    return true
end



function OpenRankPanel:WinBGType()
    return PanelWinBGType.RedNewBG
end
