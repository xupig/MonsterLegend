-- OpenRankModule.lua
OpenRankModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function OpenRankModule.Init()
    GUIManager.AddPanel(PanelsConfig.OpenRank, "Panel_OpenRank", GUILayer.LayerUIPanel, "Modules.ModuleOpenRank.OpenRankPanel", true, false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)    
end

return OpenRankModule