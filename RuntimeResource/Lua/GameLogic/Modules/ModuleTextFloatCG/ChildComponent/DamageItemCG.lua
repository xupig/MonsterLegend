local DamageItemCG = Class.DamageItemCG(ClassTypes.BaseLuaUIComponent)

local ImageWrapper = UIExtension.ImageWrapper
local XImageFloat = GameMain.XImageFloat
local Color = UnityEngine.Color

DamageItemCG.interface = GameConfig.ComponentsConfig.Component
DamageItemCG.classPath = "Modules.ModuleTextFloatCG.ChildComponent.DamageItemCG"

function DamageItemCG:Awake()
    self._image = self.gameObject:GetComponentsInChildren(typeof(ImageWrapper))
    self._rectTransform = self.transform
    self.Name = ""
    self._imageFloat = self.gameObject:AddComponent(typeof(XImageFloat))
end

function DamageItemCG:Show(position,type,callBack)
    self._imageFloat:StartFloat(position,type,callBack)
end

function DamageItemCG:Hide()
    self._rectTransform.localPosition = DamageCGManager.HIDE_POSITION
end

function DamageItemCG:SetScale(scale)
    self._rectTransform.localScale = scale
end

function DamageItemCG:GetScale()
    return self._rectTransform.localScale
end

function DamageItemCG:SetPivot(pivot)
    self._rectTransform.pivot = pivot
end

function DamageItemCG:GetPivot()
    return self._rectTransform.pivot
end

function DamageItemCG:SetPosition(position)
    self._rectTransform.localPosition = position
end

function DamageItemCG:GetPosition()
    return self._rectTransform.localPosition
end

function DamageItemCG:GetSize()
    return self._rectTransform.sizeDelta
end

function DamageItemCG:SetAlpha(alpha)
    self._imageFloat:SetAlpha(alpha)
end