--TextFloatCGModule.lua
TextFloatCGModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function TextFloatCGModule.Init()
    GUIManager.AddPanel(PanelsConfig.TextFloatCG, "Panel_TextFloatCG", GUILayer.LayerUICG, "Modules.ModuleTextFloatCG.TextFloatCGPanel", true, false)
end

return TextFloatCGModule