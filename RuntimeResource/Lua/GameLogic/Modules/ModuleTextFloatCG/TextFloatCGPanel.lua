--TextFloatCGPanel.lua
local TextFloatCGPanel = Class.TextFloatCGPanel(ClassTypes.BasePanel)

local DamageCGManager = GameManager.DamageCGManager

function TextFloatCGPanel:Awake()
    self.panelBH = self:GetComponent('LuaUIPanel')
    local numberGo = self:FindChildGO("Container_BattleTextFloat/Container_number")
    local wordGo = self:FindChildGO("Container_BattleTextFloat/Container_word")
    local damageGo = self:FindChildGO("Container_BattleTextFloat/Container_damage")
    local underTransform = self:FindChildGO("Container_BattleTextFloat/Container_layer1").transform
    local topTransform = self:FindChildGO("Container_BattleTextFloat/Container_layer2").transform
    DamageCGManager:SetPanelInfo(numberGo,damageGo,wordGo,underTransform,topTransform)
end

function TextFloatCGPanel:OnDestroy() 
    self.panelBH = nil
end