--WelfareSignItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareSignItem = Class.WelfareSignItem(UIListItem)
WelfareSignItem.interface = GameConfig.ComponentsConfig.Component
WelfareSignItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareSignItem"

local WelfareMonthRewardDataHelper = GameDataHelper.WelfareMonthRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local XArtNumber = GameMain.XArtNumber
local DateTimeUtil = GameUtil.DateTimeUtil
local WelfareManager = PlayerManager.WelfareManager
local TipsManager = GameManager.TipsManager
-- local ItemDataHelper = GameDataHelper.ItemDataHelper
local UIParticle = ClassTypes.UIParticle
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager


function WelfareSignItem:Awake()
    self._base.Awake(self)
    self._iconContainer = self:FindChildGO("Container_Icon")
    self._icon = ItemManager():GetLuaUIComponent(nil, self._iconContainer)
    self._imgReward = self:FindChildGO("Image_Bg")
    self._txtReward = self:GetChildComponent("Image_Bg/Text_Num", 'TextMeshWrapper')
    
    self._alreadyGet = self:FindChildGO("Container_Already_Get")
    self._imgDayBg = self:FindChildGO("Container_Already_Get/Image_Bg")
    self._imgDayComplete = self:FindChildGO("Container_Already_Get/Image_Complete")
    
    self._fx_Container = self:FindChildGO("Container_Fx")
    self._fx_ui_30011 = UIParticle.AddParticle(self._fx_Container, "fx_ui_30011")
    self:StopFx()
    
    self._canGet = self:FindChildGO("Image_Can_Get")
    self._onClick = function(pointerEventData)self:OnPointerUp(pointerEventData) end
    self._icon:SetPointerUpCB(self._onClick)
end

function WelfareSignItem:OnDestroy()
    self._iconContainer = nil
    self._icon = nil
    self._fx_ui_30011:Stop()
end

function WelfareSignItem:PlayFx()
    self._fx_ui_30011:Play(true, true)
end

function WelfareSignItem:StopFx()
    self._fx_ui_30011:Stop()
end


--override
function WelfareSignItem:OnPointerDown(pointerEventData)
end

--override
function WelfareSignItem:OnPointerUp(pointerEventData)
    local timeDay = DateTimeUtil.Now()
    local day = WelfareMonthRewardDataHelper:GetDay(self.signId)
    if day ~= timeDay["day"] then
        TipsManager:ShowItemTipsById(self._itemId)
    else
        local daily = WelfareManager:GetDailySignInfo()
        if daily[day] == 1 then
            TipsManager:ShowItemTipsById(self._itemId)
        else
            WelfareManager:SendDailyBonusReq()
        end
    end


end

function WelfareSignItem:SetPointerUpCB(func)
end

--override
function WelfareSignItem:OnRefreshData(id)
    if id then
        self.signId = id
        local vipDesc = WelfareMonthRewardDataHelper:GetVipDesc(id)
        local reward = WelfareMonthRewardDataHelper:GetReward(id)
        self._itemId = reward[1][1]
        self._icon:SetItem(reward[1][1], reward[1][2], 0, true)
        if WelfareMonthRewardDataHelper:GetBindFlag(id) == 1 then
            self._icon:SetBind(true)
        else
            self._icon:SetBind(false)
        end
        
        if vipDesc~=0 then
            --LoggerHelper.Log("WelfareSignItem:OnRefreshData(id)"..vipDesc)
            self._imgReward:SetActive(true)
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = WelfareMonthRewardDataHelper:GetVipLevel(id)
            local rewardDesc = LanguageDataHelper.CreateContent(vipDesc, argsTable)
            self._txtReward.text = rewardDesc
        else
            self._imgReward:SetActive(false)
        end
        local timeDay = DateTimeUtil.Now()
        
        local day = WelfareMonthRewardDataHelper:GetDay(id)
        if day < timeDay["day"] then
            self._alreadyGet:SetActive(true)
            self._imgDayBg:SetActive(true)
            self._imgDayComplete:SetActive(false)
        else
            self._alreadyGet:SetActive(false)
            self._imgDayBg:SetActive(false)
            self._imgDayComplete:SetActive(false)
        end
        
        
        local daily = WelfareManager:GetDailySignInfo()
        if day ~= timeDay["day"] then
            self:StopFx()
            self._canGet:SetActive(false)
            
            if daily[day] == 1 then
                self._alreadyGet:SetActive(true)
                self._imgDayBg:SetActive(true)
                self._imgDayComplete:SetActive(true)
            end
        else
            if daily[day] == 1 then
                self:StopFx()
                self._canGet:SetActive(false)
                self._alreadyGet:SetActive(true)
                self._imgDayBg:SetActive(true)
                self._imgDayComplete:SetActive(true)
            else
                self:PlayFx()
                self._canGet:SetActive(true)
            end
        end
    
    
    end
end

function WelfareSignItem:SetCurrent()
end



function WelfareSignItem:OnItemClick(pointerEventData)
end
