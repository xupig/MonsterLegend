--WelfareVipGiftItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareVipGiftItem = Class.WelfareVipGiftItem(UIListItem)
WelfareVipGiftItem.interface = GameConfig.ComponentsConfig.Component
WelfareVipGiftItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareVipGiftItem"

require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpRewardItem"
local WelfareLevelUpRewardItem = ClassTypes.WelfareLevelUpRewardItem

local XImageFlowLight = GameMain.XImageFlowLight
local UIList = ClassTypes.UIList
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local VIPLevelGiftManager = PlayerManager.VIPLevelGiftManager
local CommonConfig = GameConfig.CommonConfig

function WelfareVipGiftItem:Awake()
    self._base.Awake(self)
    self._txtLevel = self:GetChildComponent("Text", "TextMeshWrapper")--不管
    self._vipIcon = self:FindChildGO("Container_Icon");
    self._notCompleteImgGo = self:FindChildGO("Container_State/Img_Not_Complete")--已错过
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Img_Already_Get")--已领取
    -- self._notLevelUp = self:FindChildGO("Container_State/Img_Not_Level")--未达成

    -- 可领取
    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._goButtonTxt = self:GetChildComponent("Container_State/Button_Get_Reward/Text", "TextMeshWrapper")
    self._goButtonWrap = self:GetChildComponent("Container_State/Button_Get_Reward", "ButtonWrapper")
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._red = self:FindChildGO("Container_State/Button_Get_Reward/Image_Red")
    self._txtLeft = self:GetChildComponent("Text_Left", "TextMeshWrapper")--剩余
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)

    -- self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(false)
    -- self._getRewardComplete = function(id)self:GetRewardComplete(id) end
    self:InitView()
    self._viewInited = true
    self._dataInited = false
end

function WelfareVipGiftItem:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(WelfareLevelUpRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._alreadyGetImgGo:SetActive(false)
    -- self:SetRewardButtonActive(true)
    self._notCompleteImgGo:SetActive(false)
end

function  WelfareVipGiftItem:SetRewardButtonActive(flag)
    self._getRewardButton:SetActive(flag)
    self._goButtonFlowLight.enabled = flag
end

function WelfareVipGiftItem:OnDestroy()
    self._viewInited = false
    self._dataInited = false
end


--override
function WelfareVipGiftItem:OnRefreshData(data)   
    self._data = data
    local giftId = data.id
    self._list:SetDataList(VIPLevelGiftManager:GetRewardById(giftId))
    GameWorld.AddIcon(self._vipIcon, data.icon)
    self._dataInited = true
    self:SetGiftStatus()
end

function WelfareVipGiftItem:SetGiftStatus()
    if self._viewInited and self._dataInited then
        local giftId = self._data.id
        local level = VIPLevelGiftManager:GetGiftLevel(giftId)
        local collectType = VIPLevelGiftManager:GetCollectStatus(giftId)
        self._notCompleteImgGo:SetActive(collectType==CommonConfig.VIP_GIFT_TYPE_MISS)
        self._alreadyGetImgGo:SetActive(collectType==CommonConfig.VIP_GIFT_TYPE_COLLECTED)
        self:UpdateRewardBtn(collectType, level)

        local hasServerCount, serverCount, localCount = VIPLevelGiftManager:GetServerCount(giftId)
        if hasServerCount then
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = tostring(localCount - serverCount)
            self._txtLeft.text = LanguageDataHelper.CreateContent(81687, argsTable)
        else
            self._txtLeft.text = ""
        end
    end
end

function WelfareVipGiftItem:OnGetRewardClick()
    if self._dataInited then
        local giftId = self._data.id 
        VIPLevelGiftManager:GetVipLevelGift(giftId)

        local isRequst, arg1, arg2 = VIPLevelGiftManager:GetServerCount(giftId)
        if isRequst then
            VIPLevelGiftManager:GetVipLevelHistory()
        end
    end
end

function WelfareVipGiftItem:UpdateRewardBtn(_type, level)
    if _type == CommonConfig.VIP_GIFT_TYPE_CANCOLL then--可领取
        self._goButtonFlowLight.enabled = true
        self._goButtonTxt.text = LanguageDataHelper.CreateContent(123)
        self._getRewardButton:SetActive(true)
        self._goButtonWrap:SetInteractable(true)
        self._red:SetActive(true)
    elseif _type == CommonConfig.VIP_GIFT_TYPE_UNFINISH then--未完成
        self._goButtonFlowLight.enabled = false
        self._getRewardButton:SetActive(true)
        self._goButtonWrap:SetInteractable(false)
        local args = LanguageDataHelper.GetArgsTable()
        args['0'] = level
        self._goButtonTxt.text = LanguageDataHelper.CreateContent(81689, args)
        self._red:SetActive(false)
    else
        self._goButtonFlowLight.enabled = false
        self._goButtonTxt.text = LanguageDataHelper.CreateContent(123)
        -- self._goButtonWrap:SetInteractable(true)
        self._getRewardButton:SetActive(false)

    end
end

