--WelfareOnlineItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareOnlineItem = Class.WelfareOnlineItem(UIListItem)
WelfareOnlineItem.interface = GameConfig.ComponentsConfig.Component
WelfareOnlineItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareOnlineItem"

require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpRewardItem"
local WelfareLevelUpRewardItem = ClassTypes.WelfareLevelUpRewardItem
local OnlineRewardDataHelper = GameDataHelper.OnlineRewardDataHelper
local UIList = ClassTypes.UIList
local WelfareManager = PlayerManager.WelfareManager
local WelfareData = PlayerManager.PlayerDataManager.welfareData
local XImageFlowLight = GameMain.XImageFlowLight


function WelfareOnlineItem:Awake()
    self._base.Awake(self)
    self._txtLevel = self:GetChildComponent("Text", "TextMeshWrapper")
    self._notCompleteImgGo = self:FindChildGO("Container_State/Img_Not_Complete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Img_Already_Get")
    self._notLevelUp = self:FindChildGO("Container_State/Img_Not_Level")

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._txtLeft = self:GetChildComponent("Text_Left", "TextMeshWrapper")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self._getRewardComplete = function(id)self:GetRewardComplete(id) end
    self:InitView()
end

function WelfareOnlineItem:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(WelfareLevelUpRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._alreadyGetImgGo:SetActive(false)
    self:SetRewardButtonActive(true)
    --self._getRewardButton:SetActive(true)
    --self._goButtonFlowLight.enabled = true
    self._notCompleteImgGo:SetActive(false)
end

function  WelfareOnlineItem:SetRewardButtonActive(flag)
    self._getRewardButton:SetActive(flag)
    self._goButtonFlowLight.enabled = flag
end

function WelfareOnlineItem:OnDestroy()

end

function WelfareOnlineItem:OnGetRewardClick()
    if self.giftId then
        WelfareManager:SendOnlineRewardReq(self.giftId)
    end
end

function WelfareOnlineItem:GetRewardComplete(id)

end

--override
function WelfareOnlineItem:OnRefreshData(id)
    if id then
        self.giftId = id
        local level = OnlineRewardDataHelper:GetTime(id)
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = level
        self._txtLevel.text = LanguageDataHelper.CreateContent(81667, argsTable)
        local rewardData = OnlineRewardDataHelper:GetReward(id)
    
        local _iconList = {}
        for i, v in ipairs(rewardData) do
            -- if v[1] == GameWorld.Player():GetVocationGroup() then
                --for i = 1, #v[2] do
                   -- if i%3 == 0 then
                        local iconId =  v[1]
                        local num =  v[2]
                        local isBind =   1
                        local _iconItem = {_id = iconId, _num = num,_isBind = isBind}
                        table.insert( _iconList, _iconItem)                   
                  --  end
       
                --end
            --end
        end

        self._list:SetDataList(_iconList)
        local onlineTime = WelfareManager:GetDailyOnlineMin()
        local needTime =  OnlineRewardDataHelper:GetTime(id)

        --LoggerHelper.Error(" OpenRankView:CloseView()"..onlineTime)
        if onlineTime >= needTime then
            self._notLevelUp:SetActive(false)
            self._alreadyGetImgGo:SetActive(false)
            self:SetRewardButtonActive(true)
            --self._getRewardButton:SetActive(true)
            self._notCompleteImgGo:SetActive(false)
        else
            self._notLevelUp:SetActive(true)
            self._alreadyGetImgGo:SetActive(false)
            self:SetRewardButtonActive(false)
            --self._getRewardButton:SetActive(false)
            self._notCompleteImgGo:SetActive(false)
        end


        local levelReward = WelfareManager:GetOnlineRewardInfo()
        if levelReward[id] == 1 then
            self._alreadyGetImgGo:SetActive(true)
            self:SetRewardButtonActive(false)
            --self._getRewardButton:SetActive(false)
            self._notCompleteImgGo:SetActive(false)
            self._notLevelUp:SetActive(false)
        end
    end
end


function WelfareOnlineItem:OnButtonClick()
end
