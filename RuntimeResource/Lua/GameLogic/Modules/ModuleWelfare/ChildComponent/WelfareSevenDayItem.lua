--WelfareSevenDayItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareSevenDayItem = Class.WelfareSevenDayItem(UIListItem)
WelfareSevenDayItem.interface = GameConfig.ComponentsConfig.Component
WelfareSevenDayItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareSevenDayItem"


require "Modules.ModuleWelfare.ChildComponent.WelfareRewardItem"
local WelfareRewardItem = ClassTypes.WelfareRewardItem
local WelfareConfig = GameConfig.WelfareConfig
local XArtNumber = GameMain.XArtNumber
local UIList = ClassTypes.UIList

function WelfareSevenDayItem:Awake()
    self._base.Awake(self)
    self._textDay = self:AddChildComponent("Container_Day", XArtNumber)
    self._textDisable = self:GetChildComponent("Text_Disable", "TextWrapper")
    self._button = self:FindChildGO("Button_Fetch")
    self._csBH:AddClick(self._button, function() self:OnButtonClick() end)
    self._buttonWrapper = self._button:GetComponent("ButtonWrapper")
    self._textDisable.text = ""
    self._button:SetActive(false)
    self._buttonWrapper.interactable = false

    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    list:SetItemType(WelfareRewardItem)
    list:SetPadding(0, 0, 0, 0)
    list:SetGap(0, 0) -- topToDownGap, leftToRightGap
    list:SetDirection(UIList.DirectionLeftToRight, -1, 1) -- 横向，纵向增长量 -1为无限增长
    scrollView:SetVerticalMove(false) --垂直
    scrollView:SetHorizontalMove(false) --水平
    self._list = list
    self._scrollView = scrollView

end

function WelfareSevenDayItem:OnDestroy()
end

--override
function WelfareSevenDayItem:OnRefreshData(data)   
    self._data = data
    self._list:SetDataList(data.rewardList)
    self._textDay:SetString(data.id)

    local state = data:GetState()
    if state == WelfareConfig.REWARD_STATE.rewarded then
        self._textDisable.text = "已领取"
        self._button:SetActive(false)
    elseif state == WelfareConfig.REWARD_STATE.canReward then
        self._textDisable.text = ""
        self._button:SetActive(true)
        self._buttonWrapper.interactable = true
    elseif state == WelfareConfig.REWARD_STATE.noReward then
        self._textDisable.text = ""
        self._button:SetActive(true)
        self._buttonWrapper.interactable = false
    end
end


function WelfareSevenDayItem:OnButtonClick()
    if not self._data then
        return
    end
   PlayerManager.WelfareManager:FetchSevenDay(self._data.id)
end

