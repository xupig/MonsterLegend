--WelfareLevelUpItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareLevelUpItem = Class.WelfareLevelUpItem(UIListItem)
WelfareLevelUpItem.interface = GameConfig.ComponentsConfig.Component
WelfareLevelUpItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpItem"

require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpRewardItem"
local WelfareLevelUpRewardItem = ClassTypes.WelfareLevelUpRewardItem
local WelfareLevelUpDataHelper = GameDataHelper.WelfareLevelUpDataHelper
local UIList = ClassTypes.UIList
local WelfareManager = PlayerManager.WelfareManager
local WelfareData = PlayerManager.PlayerDataManager.welfareData
local XImageFlowLight = GameMain.XImageFlowLight


function WelfareLevelUpItem:Awake()
    self._base.Awake(self)
    self._txtLevel = self:GetChildComponent("Text", "TextMeshWrapper")
    self._notCompleteImgGo = self:FindChildGO("Container_State/Img_Not_Complete")
    self._alreadyGetImgGo = self:FindChildGO("Container_State/Img_Already_Get")
    self._notLevelUp = self:FindChildGO("Container_State/Img_Not_Level")

    self._goButtonFlowLight = self:AddChildComponent("Container_State/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container_State/Button_Get_Reward")
    self._txtLeft = self:GetChildComponent("Text_Left", "TextMeshWrapper")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self._getRewardComplete = function(id)self:GetRewardComplete(id) end
    self:InitView()
end

function WelfareLevelUpItem:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(WelfareLevelUpRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._alreadyGetImgGo:SetActive(false)
    self:SetRewardButtonActive(true)
    --self._getRewardButton:SetActive(true)
    --self._goButtonFlowLight.enabled = true
    self._notCompleteImgGo:SetActive(false)
end

function  WelfareLevelUpItem:SetRewardButtonActive(flag)
    self._getRewardButton:SetActive(flag)
    self._goButtonFlowLight.enabled = flag
end

function WelfareLevelUpItem:OnDestroy()

end

function WelfareLevelUpItem:OnGetRewardClick()
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_welfare_gift_reward_button")
    if self.giftId then
        WelfareManager:SendLevelUpRewardReq(self.giftId)
    end
end

function WelfareLevelUpItem:GetRewardComplete(id)

end

--override
function WelfareLevelUpItem:OnRefreshData(id)
    if id then
        self.giftId = id
        local level = WelfareLevelUpDataHelper:GetLevel(id)
        local argsTable = LanguageDataHelper.GetArgsTable()
        argsTable["0"] = level
        self._txtLevel.text = LanguageDataHelper.CreateContent(76600, argsTable)
        local rewardData = WelfareLevelUpDataHelper:GetReward(id)
        local _iconList = {}
        for i, v in ipairs(rewardData) do
            if v[1] == GameWorld.Player():GetVocationGroup() then
                for i = 1, #v[2] do
                    if i%3 == 0 then
                        local iconId =  v[2][i-2]
                        local num =  v[2][i-1]
                        local isBind =   v[2][i]
                        local _iconItem = {_id = iconId, _num = num,_isBind = isBind}
                        table.insert( _iconList, _iconItem)                   
                    end
       
                end
            end
        end
        --LoggerHelper.Log("vocation" .. GameWorld.Player().vocation)
        --LoggerHelper.Log("rewardData" .. PrintTable:TableToStr(_iconList))
        self._list:SetDataList(_iconList)
        
        if GameWorld.Player().level < level then
            self._notLevelUp:SetActive(true)
            self._alreadyGetImgGo:SetActive(false)
            self:SetRewardButtonActive(false)
            --self._getRewardButton:SetActive(false)
            self._notCompleteImgGo:SetActive(false)
        else
            self._notLevelUp:SetActive(false)
            self._alreadyGetImgGo:SetActive(false)
            self:SetRewardButtonActive(true)
            --self._getRewardButton:SetActive(true)
            self._notCompleteImgGo:SetActive(false)
        end

        
        
        local leftNumData = WelfareData:GetLevelUpListData()
        local rewardCount = WelfareLevelUpDataHelper:GetGetCount(id)
        if rewardCount and rewardCount > 0 and leftNumData then
            --LoggerHelper.Log("rewardCount2222" .. PrintTable:TableToStr(leftNumData))
            local argsTable = LanguageDataHelper.GetArgsTable()
            argsTable["0"] = rewardCount
            for k, v in pairs(leftNumData) do
                if k == self.giftId then
                    argsTable["0"] = rewardCount - v
                    if rewardCount - v <= 0 then
                        self._alreadyGetImgGo:SetActive(false)
                        self:SetRewardButtonActive(false)
                        --self._getRewardButton:SetActive(false)
                        self._notCompleteImgGo:SetActive(true)
                        self._notLevelUp:SetActive(false)
                    end
                end
            end
            self._txtLeft.text = LanguageDataHelper.CreateContent(76601, argsTable)
        else
            self._txtLeft.text = ""
        end
        
        local levelReward = WelfareManager:GetLevelUpRewardInfo()
        if levelReward[id] == 1 then
            self._alreadyGetImgGo:SetActive(true)
            self:SetRewardButtonActive(false)
            --self._getRewardButton:SetActive(false)
            self._notCompleteImgGo:SetActive(false)
            self._notLevelUp:SetActive(false)
        end
    end
end


function WelfareLevelUpItem:OnButtonClick()
end
