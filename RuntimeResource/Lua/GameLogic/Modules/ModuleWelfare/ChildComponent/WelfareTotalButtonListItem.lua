-- WelfareTotalButtonListItem.lua
local WelfareTotalButtonListItem = Class.WelfareTotalButtonListItem(ClassTypes.UIListItem)
WelfareTotalButtonListItem.interface = GameConfig.ComponentsConfig.Component
WelfareTotalButtonListItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareTotalButtonListItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareManager = PlayerManager.WelfareManager
local PrayManager = PlayerManager.PrayManager
local public_config = GameWorld.public_config
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager

function WelfareTotalButtonListItem:__ctor__()
    self._state=nil
end

function WelfareTotalButtonListItem:Awake()
    self:InitItem()
    self._refreshRed = function()self:UpdateRed() end
    EventDispatcher:AddEventListener(GameEvents.WelfareRedUpdate, self._refreshRed)


    if self._state==nil then
        return
    end
    self:SetState(self._state)

end

function WelfareTotalButtonListItem:OnDestroy()
    EventDispatcher:RemoveEventListener(GameEvents.WelfareRedUpdate, self._refreshRed)
end

function WelfareTotalButtonListItem:InitItem()

  self._imgRed = self:FindChildGO("Image_Red")
  self._unselected = self:FindChildGO("UnSelected")
  self._selected = self:FindChildGO("Selected")
  self._text1 = self:GetChildComponent("UnSelected/Text","TextMeshWrapper")
  self._text2 = self:GetChildComponent("Selected/Text","TextMeshWrapper")
  self._imgRed:SetActive(false)
end

function WelfareTotalButtonListItem:OnRefreshData(data)
    if data then
        --LoggerHelper.Log("WelfareTotalButtonListItem:OnRefreshData"..PrintTable:TableToStr(data))
        -- self._index = data[1]
        -- self._unselected:SetActive(true)
        -- self._selected:SetActive(false)
        self._text1.text = data._title
        self._text2.text = data._title
        self._funId =  data._funId
        self:UpdateRed()
    end
end

function WelfareTotalButtonListItem:UpdateRed()
    if  self._funId == public_config.FUNCTION_ID_WELFARE_SIGN then
        if WelfareManager:GetDailyIsShowRed() then
            self._imgRed:SetActive(true)
        else
            self._imgRed:SetActive(false)
        end
    elseif  self._funId == public_config.FUNCTION_ID_WELFARE_GIFT then
        if WelfareManager:GetLevelIsShowRed() then
            self._imgRed:SetActive(true)
        else
            self._imgRed:SetActive(false)
        end
    elseif self._funId == public_config.FUNCTION_ID_WELFARE_EXCHANGE then

    elseif self._funId == public_config.FUNCTION_ID_PRAY then
        self._imgRed:SetActive(PrayManager:RefreshRedPointStatus())
    elseif  self._funId == public_config.FUNCTION_ID_WELFARE_ANNOUNCE then
        if WelfareManager:GetAnnounceIsShowRed() then
            self._imgRed:SetActive(true)
        else
            self._imgRed:SetActive(false)
        end
    elseif  self._funId == public_config.FUNCTION_ID_WELFARE_ONLINE then
        --self._imgRed:SetActive(SevenDayLoginManager:IsCanGetReward())
        if WelfareManager:GetOnlineIsShowRed() then
            self._imgRed:SetActive(true)
        else
            self._imgRed:SetActive(false)
        end
    elseif self._funId == public_config.FUNCTION_ID_VIPGIFT then
        self._imgRed:SetActive(PlayerManager.VIPLevelGiftManager:GetVipGiftRedDot())
    end	
end


	



function WelfareTotalButtonListItem:SetState(state)
    -- self._state=state
    -- if state==0 then
    --     self._unselected:SetActive(true)
    --     self._selected:SetActive(false)
    -- else
    --     self._unselected:SetActive(false)
    --     self._selected:SetActive(true)
    -- end
end