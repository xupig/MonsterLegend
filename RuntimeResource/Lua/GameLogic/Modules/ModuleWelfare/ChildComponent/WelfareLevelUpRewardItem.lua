-- WelfareLevelUpRewardItem.lua
local WelfareLevelUpRewardItem = Class.WelfareLevelUpRewardItem(ClassTypes.UIListItem)

UIListItem = ClassTypes.UIListItem
WelfareLevelUpRewardItem.interface = GameConfig.ComponentsConfig.Component
WelfareLevelUpRewardItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpRewardItem"

local UIList = ClassTypes.UIList
require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function WelfareLevelUpRewardItem:Awake()
    self._itemContainer = self:FindChildGO("Container_Icon")
    self._itemIcon = ItemManager():GetLuaUIComponent(nil, self._itemContainer)
end

function WelfareLevelUpRewardItem:OnDestroy() 

end

--override
function WelfareLevelUpRewardItem:OnRefreshData(data)
    if data then
        local itemId = data._id
        local num = data._num
        local isBind = data._isBind
        self._itemIcon:ActivateTipsById()
        if data._isBind == 1 then
            self._itemIcon:SetBind(true)
        elseif  data._isBind == 0 then
            self._itemIcon:SetBind(false)
        end

        self._itemIcon:SetItem(itemId, num)
    end
end