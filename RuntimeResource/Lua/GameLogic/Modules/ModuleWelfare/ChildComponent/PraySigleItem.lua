-- PraySigleItem.lua
local PraySigleItem = Class.PraySigleItem(ClassTypes.BaseComponent)
PraySigleItem.interface = GameConfig.ComponentsConfig.Component
PraySigleItem.classPath = "Modules.ModuleWelfare.ChildComponent.PraySigleItem"
local UIComponentUtil = GameUtil.UIComponentUtil
local GUIManager = GameManager.GUIManager
local PrayData = PlayerManager.PlayerDataManager.prayData
local PrayManager = PlayerManager.PrayManager
local public_config = GameWorld.public_config
local ColorConfig = GameConfig.ColorConfig
local BaseUtil = GameUtil.BaseUtil
local XArtNumber = GameMain.XArtNumber
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function PraySigleItem:Awake()
    self._timerid = -1
    self._type = 1
    self._csBH = self:GetComponent("LuaUIComponent")
    self._updateCtri = function() self:UpdateCtril() end
    self._fightGO=nil
    self:InitView()
end

function PraySigleItem:UpdateCtril()
    if self._type ~= 1 then
        return
    end
    self._isTween = true
    local num = PrayData:GetCtri()   
    self._fightGO:SetActive(true)
    
    local moveTo = self._fightGO.transform.localPosition
    moveTo.y = 350
    self._tweenPosFight:OnceOutQuad(moveTo, 1, function()
        self._isTween = false
        self._fightGO.transform.localPosition = Vector3.New(220,246,0)
        self._fightGO:SetActive(false)
    end)

    self._fpNumber:SetNumber(num)
end
    
function PraySigleItem:InitView()
    self._fpNumber = self:AddChildComponent('Container_FightPower', XArtNumber)
    self._red = self:FindChildGO('Image_Red')
    self._contimeGo = self:FindChildGO('Container_Time')
    self._concostGo = self:FindChildGO('Container_Cost')
    self._buttonGo = self:FindChildGO('Button_Prayer')
    self._textGet = self:GetChildComponent("Text_Get", "TextMeshWrapper")
    self._textCount = self:GetChildComponent('Container_Cost/Text_Count',"TextMeshWrapper")
    self._textCostNum = self:GetChildComponent('Container_Cost/Text_CostNum',"TextMeshWrapper")
    self._textTime = self:GetChildComponent('Container_Time/Text_Time','TextMeshWrapper')
    self._buttonText = self:GetChildComponent('Button_Prayer/Text','TextMeshWrapper')
    self._csBH:AddClick(self._buttonGo,function() self:BtnClick() end)
    self._timerfun = function() self:Timer() end
    self._red:SetActive(false)
end

function PraySigleItem:BtnClick()
    if self._type==1 then
        if not self._isTween  then
            PrayManager:PrayActionGoldRpc()
        end
    else
        PrayManager:PrayActionExpRpc()
    end      
end


function PraySigleItem:OnDestroy()
    self._buttonGo = nil
    self._csBH = nil
end

function PraySigleItem:Update(data)
    self._type = data[1]
    local d = data[2]
    if self._type==1 then
        self._textCount.text = (d['vipcount']-d['finishcount'])..'/'..d['vipcount']
        local f= math.floor(d['gold']/10000)
        local msg= d['gold']/10000
        if f ==0 then
            self._textGet.text = d['gold']..LanguageDataHelper.GetContent(20) 
        else
            self._textGet.text = (msg-msg%0.1)..LanguageDataHelper.GetContent(143)..LanguageDataHelper.GetContent(20)  
        end            
        if d['type']==0 then
            self._concostGo:SetActive(true)
            self._red:SetActive(false)
            self._contimeGo:SetActive(true)
            self._textCostNum.text = d['curcost']..''    
            if self._timerid == -1 then 
                self._timerid=TimerHeap:AddSecTimer(0,1,0,self._timerfun)
            end
            self._buttonText.text = LanguageDataHelper.GetContent(202)            
        elseif d['type']==1 then
            self._concostGo:SetActive(false)            
            self._red:SetActive(true)
            self._contimeGo:SetActive(false)   
            self._textCostNum.text = '0'      
            self._buttonText.text = LanguageDataHelper.GetContent(74901)..LanguageDataHelper.GetContent(202) 
        else
            self._concostGo:SetActive(true)            
            -- self._red:SetActive(true)
            self._red:SetActive(false)
            self._contimeGo:SetActive(true)        
            self._textCostNum.text = d['curcost']..''  
            self:Timer()   
            if self._timerid == -1 then 
                self._timerid=TimerHeap:AddSecTimer(0,1,0,self._timerfun)
            end     
            self._buttonText.text = LanguageDataHelper.GetContent(202) 
        end            
    else
        self._contimeGo:SetActive(false) 
        self._textCount.text = (d['vipcount']-d['finishcount'])..'/'..d['vipcount']
        self._textCostNum.text = d['curcost']..''
        local f= math.floor(d['exp']/10000)
        local msg= d['exp']/10000
        if f==0 then
            self._textGet.text = d['exp']..LanguageDataHelper.GetContent(36)
        else
            self._textGet.text = (msg-msg%0.1)..LanguageDataHelper.GetContent(143)..LanguageDataHelper.GetContent(36)
        end  
        -- self._red:SetActive(PrayData:GetExpRed())
        self._red:SetActive(false)
    end
end

function PraySigleItem:Timer()
    local time = PrayManager:GetFreeTime()
    local h = math.floor(time/60/60)
    local f = math.floor(time/60%60)
    local s = math.floor(time%60)
    local _h=''
    local _f=''
    local _s=''
    if h<10 then 
        _h = '0'..tostring(h)
    elseif h==0 then
        _h ='00'
    else
        _h =tostring(h)        
    end
    if f<10 then 
        _f = '0'..tostring(f)
    elseif f==0 then
        _f ='00'
    else
        _f =tostring(f)        
    end
    if s<10 then 
        _s = '0'..tostring(s)
    elseif s==0 then
        _s ='00'
    else
        _s =tostring(s)        
    end
    self._textTime.text = _h..':'.._f..':'.._s
    if time==0 then
        TimerHeap:DelTimer(self._timerid)
        self._timerid=-1
        self._contimeGo:SetActive(false)
    end
end
function PraySigleItem:Close()
    TimerHeap:DelTimer(self._timerid)
    self._timerid = -1
    if self._type==1 then
        EventDispatcher:RemoveEventListener(GameEvents.PrayCtri,self._updateCtri)
        self._fightGO:SetActive(false)
    end
end

function PraySigleItem:Open()
    if self._type == 1 then
        if self._fightGO==nil then
            self._fightGO = self:FindChildGO('Container_FightPower')
            self._tweenPosFight = self:AddChildComponent('Container_FightPower', XGameObjectTweenPosition)
        end
        self._fightGO:SetActive(false)     
        EventDispatcher:AddEventListener(GameEvents.PrayCtri,self._updateCtri)
    end
end


