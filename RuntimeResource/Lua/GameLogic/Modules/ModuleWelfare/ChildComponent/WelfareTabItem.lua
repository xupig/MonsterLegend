--WelfareTabItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareTabItem = Class.WelfareTabItem(UIListItem)
WelfareTabItem.interface = GameConfig.ComponentsConfig.Component
WelfareTabItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareTabItem"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper

function WelfareTabItem:Awake()
    self._base.Awake(self)
    self._txtName = self:GetChildComponent("Text_Name", "TextWrapper")
    self._imageSelect = self:FindChildGO("Image_SelectLight")
    self._imageCanReward = self:FindChildGO("Image_News")
    self._imageCanReward:SetActive(false)
    self:SetSelect(false)
end


function WelfareTabItem:OnDestroy()
end


--override
function WelfareTabItem:OnRefreshData(data)   
    self._data = data
    self._txtName.text = LanguageDataHelper.CreateContent(data.name)
    self:SetSelect(self._state)
    self._imageCanReward:SetActive(data.canReward)
end


function WelfareTabItem:SetSelect(state)
    if state == self._state then
        return
    end
    self._state = state 
    self._imageSelect:SetActive(state)
end