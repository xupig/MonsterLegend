--WelfareRewardItem.lua
local UIListItem = ClassTypes.UIListItem
local WelfareRewardItem = Class.WelfareRewardItem(UIListItem)
WelfareRewardItem.interface = GameConfig.ComponentsConfig.Component
WelfareRewardItem.classPath = "Modules.ModuleWelfare.ChildComponent.WelfareRewardItem"

local ItemDataHelper = GameDataHelper.ItemDataHelper

require "UIComponent.Extend.ItemManager"
local ItemManager = ClassTypes.ItemManager

function WelfareRewardItem:Awake()
    self._base.Awake(self)
    self._containerItem = self:FindChildGO("Container_Item")
    self._itemManager = ItemManager()
    self._onClickFun = function(pointerEventData)self:OnItemClick(pointerEventData) end
end


function WelfareRewardItem:OnDestroy()
end

--override
function WelfareRewardItem:OnRefreshData(data)   
    self._data = data
    self._itemId = data.itemId
    -- GameWorld.AddIcon(self._containerItem, ItemDataHelper.GetItemIcon(data.itemId))

    self._itemGo = self._itemManager:GetLuaUIComponent(self._itemId, self._containerItem)
    self._itemGo:SetPointerUpCB(self._onClickFun)
    self._itemGo:SetItem(self._itemId, data.itemNum)
end

function WelfareRewardItem:OnItemClick(pointerEventData)
    local clickPos = pointerEventData.position
    -- GameManager.TipsManager:ShowTips(ItemTipsType.NORMAL,{[1]=self._itemId},{[1]=clickPos.x,[2]=clickPos.y,[3]=0})
end
