--SevenDayLoginView.lua
local SevenDayLoginView = Class.SevenDayLoginView(ClassTypes.BaseComponent)
SevenDayLoginView.interface = GameConfig.ComponentsConfig.Component
SevenDayLoginView.classPath = "Modules.ModuleWelfare.ChildView.SevenDayLoginView"

local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar
local Welfare7dayRewardDataHelper = GameDataHelper.Welfare7dayRewardDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local RectTransform = UnityEngine.RectTransform
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager
local XArtNumber = GameMain.XArtNumber
local GUIManager =  GameManager.GUIManager
local XImageFlowLight = GameMain.XImageFlowLight


require "Modules.ModuleActivity.ChildComponent.SevenDayRewardItem"
local SevenDayRewardItem = ClassTypes.SevenDayRewardItem

require "Modules.ModuleActivity.ChildComponent.DayRewardItem"
local DayRewardItem = ClassTypes.DayRewardItem

function SevenDayLoginView:Awake()
    self:InitCallback()
    self:InitViews()
end

function SevenDayLoginView:InitCallback()
    self._updateData = function() self:UpdateData() end
    self._getRewardSuccess = function(day) self:HandleGetRewardSuccess(day) end
end

function SevenDayLoginView:InitViews()
    self._dayArtNumber = self:AddChildComponent('Container_Info/Container_SevenDay', XArtNumber)
    self._infoContainer = self:FindChildGO("Container_Info/Container_Info1")
    self._rewardInfoContainer = self:FindChildGO("Container_Reward_Content/Container_Reward_Info")
    self._itemContainer = self:FindChildGO("Container_Reward_Content/Container_Item")
    self._rewardText = self:GetChildComponent("Button_Get_Reward/Text",'TextMeshWrapper')

    self._redPointGo = self:FindChildGO("Button_Get_Reward/Image_RedPoint")
    self._updateRedPointFunc = function(flag) self:SetRedPoint(flag) end    

    self._getRewardButton = self:FindChildGO("Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function() self:OnGetRewardButtonClick() end)
    self._getRewardButtonFlowLight = self:AddChildComponent("Button_Get_Reward", XImageFlowLight)

    -- self._closeButton = self:FindChildGO("Container_Bg/Button_Close")
    -- self._csBH:AddClick(self._closeButton, function() self:OnCloseButtonClick() end)

    self:InitProgressBar()
    self:InitSevenDayRewardView()
    self:InitRewardGroup()
end

function SevenDayLoginView:InitRewardGroup()
    self._rewardGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_RewardList")
    self._rewardGroup:SetItemType(DayRewardItem)

    -- self._rewardGroupItemClick = function(idx) self:RewardGroupItemClick(idx) end
    -- self._rewardGroup:SetItemClickedCB(self._rewardGroupItemClick)
end

function SevenDayLoginView:SevenDayRewardItemClick(idx)
    self._selectDay = idx
    self._dayArtNumber:SetNumber(self._selectDay)
    local icons = Welfare7dayRewardDataHelper:GetIcons(self._selectDay)
    GameWorld.AddIcon(self._infoContainer, icons[2])
    GameWorld.AddIcon(self._rewardInfoContainer, icons[3])
    GameWorld.AddIcon(self._itemContainer, icons[4])

    local group = GameWorld.Player():GetVocationGroup()
    self._rewardGroupDatas = Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, self._selectDay)
    self._rewardGroup:SetDataList(self._rewardGroupDatas)

    self:UpdateGetRewardButtonState(idx)
end

function SevenDayLoginView:UpdateGetRewardButtonState(day)
    local get_reward_day = GameWorld.Player().get_reward_day 
    local canGetNum = SevenDayLoginManager:GetCanGetRewardNum()
    local get_reward_num = SevenDayLoginManager:GetRewardDays()

    if get_reward_day[day] then 
        --已领取
        self._getRewardButton:SetActive(true)
        self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(false)
        self._getRewardButtonFlowLight.enabled = false
        --LoggerHelper.Error("已领取")
        self._rewardText.text = LanguageDataHelper.CreateContent(76982)
    else
        if day <= canGetNum then
            --可以领取
            self._getRewardButton:SetActive(true)
            self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(true)
            self._getRewardButtonFlowLight.enabled = true
            self._rewardText.text = LanguageDataHelper.CreateContent(76914)
        else
            --未达到领取条件            
            self._getRewardButton:SetActive(false)
            self._getRewardButtonFlowLight.enabled = false
        end
    end
end

function SevenDayLoginView:InitSevenDayRewardView()
    local ids = Welfare7dayRewardDataHelper:GetAllId()
    self._rewardViews = {}
    self._sevenDayRewardItemClick = function(idx) self:SevenDayRewardItemClick(idx) end
    for i,v in ipairs(ids) do
        local go = self:CopyRewardContainer()
        table.insert(self._rewardViews, UIComponentUtil.AddLuaUIComponent(go, SevenDayRewardItem))
        self._rewardViews[i]:InitData(v, self._progressBarLength)
        self._rewardViews[i]:SetPointerUpCB(self._sevenDayRewardItemClick)
    end
    self._rewardDemoContainer:SetActive(false)
end

function SevenDayLoginView:CopyRewardContainer()
    return self:CopyUIGameObject("Container_Progress/Container_Rewards/Container_Reward", "Container_Progress/Container_Rewards")
end

function SevenDayLoginView:InitProgressBar()
    self._rewardDemoContainer = self:FindChildGO("Container_Progress/Container_Rewards/Container_Reward")
    self._progressBarLength = self:FindChildGO("Container_Progress/ProgressBar/Image_Background"):GetComponent(typeof(RectTransform)).sizeDelta.x

    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject,"Container_Progress/ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(false)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
end

function SevenDayLoginView:ShowView(data)
    -- SevenDayLoginManager:RegistUpdateFunc(self._updateRedPointFunc)
    --LoggerHelper.Error("ShowView")
    self:AddEventListeners()
    self:UpdateData()
end

function SevenDayLoginView:SetData(data)
end

function SevenDayLoginView:CloseView()
    -- SevenDayLoginManager:RegistUpdateFunc(nil)
    self:RemoveEventListeners()
end

function SevenDayLoginView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.SEVEN_DAY_REFRESH, self._updateData)
    EventDispatcher:AddEventListener(GameEvents.SEVEN_DAY_GET_REWQARD_SUCCESS, self._getRewardSuccess)
end

function SevenDayLoginView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.SEVEN_DAY_REFRESH, self._updateData)
    EventDispatcher:RemoveEventListener(GameEvents.SEVEN_DAY_GET_REWQARD_SUCCESS, self._getRewardSuccess)
end

function SevenDayLoginView:UpdateData()
    local day = SevenDayLoginManager:GetRewardDays() + 1
    -- local group = GameWorld.Player():GetVocationGroup()
    local maxDays = #Welfare7dayRewardDataHelper:GetAllId()
    local canGetNum = SevenDayLoginManager:GetCanGetRewardNum()
    -- if not SevenDayLoginManager:IsCanGetReward() then
    --     day = day - 1
    --     self._rewardGroupDatas = Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, day)
    -- else
    --     self._rewardGroupDatas = Welfare7dayRewardDataHelper:GetRewardByVocationGroup(group, day)
    -- end

    -- self._rewardGroup:SetDataList(self._rewardGroupDatas)
    if self._progress ~= (canGetNum - 1) / (maxDays - 1) then
        self._progress = (canGetNum - 1) / (maxDays - 1)
        if self._progress == 0 then
            self._progress = 0.01
        end
        self._barComp:SetProgress(self._progress)
    end

    for i,v in ipairs(self._rewardViews) do
        self._rewardViews[i]:UpdateData()
    end
    self:SevenDayRewardItemClick(SevenDayLoginManager:GetSelectDay())

    -- self._dayArtNumber:SetNumber(day)
    -- local icons = Welfare7dayRewardDataHelper:GetIcons(day)
    -- GameWorld.AddIcon(self._infoContainer, icons[2])
    -- GameWorld.AddIcon(self._rewardInfoContainer, icons[3])
    -- GameWorld.AddIcon(self._itemContainer, icons[4])
end

function SevenDayLoginView:OnGetRewardButtonClick()
    SevenDayLoginManager:GetReward(self._selectDay)
end

-- function SevenDayLoginView:OnCloseButtonClick()
--     GUIManager.ClosePanel(PanelsConfig.SevenDayLogin)
-- end

function SevenDayLoginView:SetRedPoint(flag)
    self._redPointGo:SetActive(flag)
    self._getRewardButton:GetComponent("ButtonWrapper"):SetInteractable(flag)
    self._getRewardButtonFlowLight.enabled = flag
end

function SevenDayLoginView:HandleGetRewardSuccess(day)
    self._rewardViews[day]:UpdateData()
    self:SevenDayRewardItemClick(SevenDayLoginManager:GetSelectDay(day))
end