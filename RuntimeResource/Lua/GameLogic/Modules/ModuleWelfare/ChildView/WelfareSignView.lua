-- WelfareSignView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareSignView = Class.WelfareSignView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareSignView.interface = GameConfig.ComponentsConfig.Component
WelfareSignView.classPath = "Modules.ModuleWelfare.ChildView.WelfareSignView"

require "Modules.ModuleWelfare.ChildComponent.SignRewardItem"
local SignRewardItem = ClassTypes.SignRewardItem
require "UIComponent.Extend.TipsCom"

require "Modules.ModuleWelfare.ChildComponent.WelfareSignItem"


local TipsCom = ClassTypes.TipsCom
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareManager = PlayerManager.WelfareManager
local GUIManager = GameManager.GUIManager
local DateTimeUtil = GameUtil.DateTimeUtil
local XArtNumber = GameMain.XArtNumber
local ItemDataHelper = GameDataHelper.ItemDataHelper
local WelfareSignItem = ClassTypes.WelfareSignItem
local UIList = ClassTypes.UIList
local UIComponentUtil = GameUtil.UIComponentUtil
local RectTransform = UnityEngine.RectTransform
local UIScaleProgressBar = ClassTypes.UIScaleProgressBar

local WelfareMonthRewardDataHelper = GameDataHelper.WelfareMonthRewardDataHelper
local WelfareDayRewardDataHelper = GameDataHelper.WelfareDayRewardDataHelper


--override
function WelfareSignView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self._txtSign = self:GetChildComponent("Text_Sign", 'TextMeshWrapper')
    self._txtDayNum = self:GetChildComponent("Container_Progress/Text_Num", 'TextMeshWrapper')
    
    
    self._playButton = self:FindChildGO("Container_Desc/Button_Play")
    self._csBH:AddClick(self._playButton, function()self:OnPlayDescClick() end)
    
    self._tipsGO = self:FindChildGO("Container_Tips")
    self._tipsCom = self:AddChildLuaUIComponent("Container_Tips", TipsCom)
    self._tipsCom:SetText(LanguageDataHelper.CreateContentWithArgs(76611))
    self._tipsGO:SetActive(false)
    
    self._barComp = UIScaleProgressBar.AddProgressBar(self.gameObject, "Container_Progress/ProgressBar")
    self._barComp:SetProgress(0)
    self._barComp:ShowTweenAnimate(true)
    self._barComp:SetMutipleTween(true)
    self._barComp:SetIsResetToZero(false)
    self._barComp:SetTweenTime(0.2)
    
    
    self:InitViews()
    self:InitRewardView()
    self._refreshSignList = function()self:OnDailySignList() end
    self._refreshCumulativeReward = function()self:OnCumulativeReward() end
end

--override
function WelfareSignView:OnDestroy()

end

function WelfareSignView:OnPlayDescClick()
    self._tipsGO:SetActive(true)
end



function WelfareSignView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.GetDailyBonusResq, self._refreshSignList) 
    EventDispatcher:AddEventListener(GameEvents.GetCumulativeRewardResq, self._refreshCumulativeReward)
end

function WelfareSignView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.GetDailyBonusResq, self._refreshSignList)
    EventDispatcher:RemoveEventListener(GameEvents.GetCumulativeRewardResq, self._refreshCumulativeReward)
end


function WelfareSignView:ShowView(data)
    if self._scrollViewLeft then
        self._scrollViewLeft:SetScrollRectState(true)
    end
    self:OnDailySignList()
    self:AddEventListeners()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleWelfare.ChildView.WelfareSignView")
end


function WelfareSignView:CloseView()
    --LoggerHelper.Error("WelfareSignView:CloseView()")
    if self._scrollViewLeft then
        self._scrollViewLeft:SetScrollRectState(false)
    end
    self:RemoveEventListeners()
end

function WelfareSignView:OnDailySignList()
    local listId = WelfareMonthRewardDataHelper:GetAllId()
    local signList = {}
    local dayNum = DateTimeUtil.GetCurMonthTodayNum()
    
    -- LoggerHelper.Log(" DateTimeUtil.Now()" .. PrintTable:TableToStr(timeDay))
    -- local yearsday = DateTimeUtil.YearDays[timeDay["year"]]
    -- LoggerHelper.Log(" DateTimeUtil.Now()" .. PrintTable:TableToStr(yearsday))
    -- LoggerHelper.Log(" DateTimeUtil.Now()" .. )
    for i, v in ipairs(listId) do
        if i <= dayNum then
            table.insert(signList, v)
        end
    end
    local argsTable = LanguageDataHelper.GetArgsTable()
    argsTable["0"] = WelfareManager:GetTotalSignDayNum()
    local SignDesc = LanguageDataHelper.CreateContent(76609, argsTable)
    self._txtSign.text = SignDesc
    self._txtDayNum.text = WelfareManager:GetTotalSignDayNum()
    self._listLeft:SetDataList(signList)

    local timeDay = DateTimeUtil.Now()
    if  timeDay["day"]>dayNum/2 then
        self._listLeft:MoveToBottom()
    end

    self:OnCumulativeReward()
end


function WelfareSignView:InitViews()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList_Item")
    list:SetItemType(WelfareSignItem)
    list:SetPadding(20, 10, 0, 24)
    list:SetGap(12, 30)-- topToDownGap, leftToRightGap
    list:SetDirection(UIList.DirectionLeftToRight, 6, -1)-- 横向，纵向增长量 -1为无限增长
    list:SetItemClickedCB(function(idx)self:OnItemClicked(idx) end)
    self._listLeft = list
    self._scrollViewLeft = scrollView
    
    --self._welfareList:SetDataList(self.demoNavigation)
    self._rewardDemoContainer = self:FindChildGO("Container_Progress/Container_Rewards/Container_Reward")
    self._progressBarLength = self:FindChildGO("Container_Progress/ProgressBar/Image_Background"):GetComponent(typeof(RectTransform)).sizeDelta.x
end


function WelfareSignView:InitRewardView()
    local ids = WelfareDayRewardDataHelper:GetAllId()
    self._needIds = {}
    for i, id in ipairs(ids) do
        local levelTab = WelfareDayRewardDataHelper:GetLv(id)
        local sumId = {}  
        if GameWorld.Player().level >= levelTab[1] and GameWorld.Player().level <= levelTab[2] then
            table.insert(self._needIds, id)
        end
    end
    
    self._rewardViews = {}
    for i, v in pairs(self._needIds) do
        local go = self:CopyRewardContainer()
        table.insert(self._rewardViews, UIComponentUtil.AddLuaUIComponent(go, SignRewardItem))
        self._rewardViews[i]:InitData(v,self._progressBarLength,i)
    end
    
    self:OnCumulativeReward()
    self._rewardDemoContainer:SetActive(false)
end

function WelfareSignView:OnCumulativeReward()
    local dayNum = WelfareManager:GetTotalSignDayNum()
    local max = WelfareDayRewardDataHelper:GetMaxDay()

    local point = 0
    local needIds = self._needIds

    local pointEr = max/(#needIds)

    for i = 1, #needIds do
        if dayNum == WelfareDayRewardDataHelper:GetDay(needIds[i]) then
            point =  pointEr*i
        end


        if i>1 then
            if dayNum > WelfareDayRewardDataHelper:GetDay(needIds[i-1]) and dayNum < WelfareDayRewardDataHelper:GetDay(needIds[i]) then
                point =  pointEr*(i-1)+pointEr/ WelfareDayRewardDataHelper:GetDay(needIds[i])*(dayNum -  WelfareDayRewardDataHelper:GetDay(needIds[i-1]))
            end
        elseif i == 1 then
            if dayNum <  WelfareDayRewardDataHelper:GetDay(needIds[i]) then
                point =  pointEr/ WelfareDayRewardDataHelper:GetDay(needIds[i])*dayNum
                --LoggerHelper.Log("WelfareSignView:OnCumulativeReward"..point..i..PrintTable:TableToStr(needIds))
            end
        end
    end

 
    
    self._barComp:SetProgress(point / max)
    
    for i, v in ipairs(self._rewardViews) do
        v:RefreshStatus()
    end
    self._rewardDemoContainer:SetActive(false)
end



function WelfareSignView:CopyRewardContainer()
    return self:CopyUIGameObject("Container_Progress/Container_Rewards/Container_Reward", "Container_Progress/Container_Rewards")
end
