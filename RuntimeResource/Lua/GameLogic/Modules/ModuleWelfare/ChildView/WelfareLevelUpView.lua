-- WelfareLevelUpView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareLevelUpView = Class.WelfareLevelUpView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareLevelUpView.interface = GameConfig.ComponentsConfig.Component
WelfareLevelUpView.classPath = "Modules.ModuleWelfare.ChildView.WelfareLevelUpView"


local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareManager = PlayerManager.WelfareManager
local GUIManager = GameManager.GUIManager
require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpItem"
local WelfareLevelUpItem = ClassTypes.WelfareLevelUpItem
local UIList = ClassTypes.UIList

--override
function WelfareLevelUpView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitViews() 
end

--override
function WelfareLevelUpView:OnDestroy() 
    self:RemoveEventListeners()
end

function WelfareLevelUpView:OnEnable()
    self:AddEventListeners()

    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW,'Modules.ModuleWelfare.ChildView.WelfareLevelUpView')
end

function WelfareLevelUpView:OnDisable()
    self:RemoveEventListeners()
end


function WelfareLevelUpView:AddEventListeners()
    self._onWelfareLevelUpRward = function(newValue, oldValue) self:OnInfoChanged(newValue, oldValue) end
    EventDispatcher:AddEventListener(GameEvents.WELFARE_LEVEL_UP_REWARD, self._onWelfareLevelUpRward)
end

function WelfareLevelUpView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.WELFARE_LEVEL_UP_REWARD, self._onWelfareLevelUpRward)
end


function WelfareLevelUpView:InitViews()    
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    list:SetItemType(WelfareLevelUpItem)
    list:SetPadding(0, 0, 0, 0)
    list:SetGap(6, 0) -- topToDownGap, leftToRightGap
    list:SetDirection(UIList.DirectionTopToDown, 1, -1) -- 横向，纵向增长量 -1为无限增长  DirectionLeftToRight  DirectionTopToDown
    self._listLeft = list
    self._scrollViewLeft = scrollView

end


function WelfareLevelUpView:RefreshView()
    self:RefreshList()
end

function WelfareLevelUpView:OnInfoChanged(newValue, oldValue)
    self:RefreshList()
end


function WelfareLevelUpView:RefreshList()
    self._list = GameDataHelper.WelfareDataHelper:GetLevelUpList()
    -- LoggerHelper.Log({"===WelfareLevelUpView self._list = ", self._list})
    self._listLeft:SetDataList(self._list)
end