-- WelfareOnlineView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareOnlineView = Class.WelfareOnlineView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareOnlineView.interface = GameConfig.ComponentsConfig.Component
WelfareOnlineView.classPath = "Modules.ModuleWelfare.ChildView.WelfareOnlineView"

require "Modules.ModuleWelfare.ChildComponent.WelfareOnlineItem"
require "UIComponent.Base.UIScrollView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local UIList = ClassTypes.UIList
local OnlineRewardDataHelper = GameDataHelper.OnlineRewardDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local WelfareOnlineItem = ClassTypes.WelfareOnlineItem
local WelfareManager = PlayerManager.WelfareManager
local UIScrollView = ClassTypes.UIScrollView
local TimerHeap = GameWorld.TimerHeap

--override
function WelfareOnlineView:Awake()
    -- self._csBH = self:GetComponent('LuaUIComponent')
     self:InitViews()
     self._refreshLevelUpRewardList = function () self:OnRefreshLevelUpRewardList() end
end

--override
function WelfareOnlineView:OnDestroy() 
    --self:RemoveEventListeners()
end

function WelfareOnlineView:InitViews()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
   
	self._giftList = list
	self._giftlistlView = scrollView 
    self._giftlistlView:SetHorizontalMove(false)
    self._giftlistlView:SetVerticalMove(true)
    self._giftList:SetItemType(WelfareOnlineItem)
    self._giftList:SetPadding(0, 0, 0, 0)
    self._giftList:SetGap(10, 0)
    self._giftList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "Container_List/ScrollViewList")
    self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
end


function WelfareOnlineView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.UpdateOnlineLevelUpRewardList, self._refreshLevelUpRewardList)
    GameWorld.Player():AddPropertyListener(EntityConfig.PROPERTY_DAILY_ONLINE_MIN, self._refreshLevelUpRewardList)
end

function WelfareOnlineView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.UpdateOnlineLevelUpRewardList, self._refreshLevelUpRewardList)
    GameWorld.Player():RemovePropertyListener(EntityConfig.PROPERTY_DAILY_ONLINE_MIN, self._refreshLevelUpRewardList)
end


function WelfareOnlineView:ShowView()
    if self._giftlistlView then
        self._giftlistlView:SetScrollRectState(true)
    end
    self._scrollViewAttri:ResetContentPosition()
    --WelfareManager:SendGetReceivedInfoReq()
    local ids = OnlineRewardDataHelper:GetAllId()

    local levelReward = WelfareManager:GetOnlineRewardInfo()
    local worldLevel = GameWorld.Player().world_level
    local alreadyId = {}
    local noGetId = {}

    for i,id in ipairs(ids) do
        local levelTab = OnlineRewardDataHelper:GetWorldLevel(id)
        
        -- -- for i,v in ipairs(table_name) do
        -- --     print(i,v)
        -- -- end
        if worldLevel >= levelTab[1] and worldLevel <= levelTab[2] then
            if levelReward[id] == 1 then
                table.insert(alreadyId,id)
             else
                table.insert(noGetId,id)
             end
        end
    end



    local ordIds = {}
    for i,id in ipairs(noGetId) do
        table.insert(ordIds,id)
    end

    for i,id in ipairs(alreadyId) do
        table.insert(ordIds,id)
    end
    
    --LoggerHelper.Error("OpenRankView:CloseView()"..PrintTable:TableToStr(ordIds))
    self._giftList:SetDataList(ordIds)
    self:AddEventListeners()

end



function WelfareOnlineView:CloseView()
    if self._giftlistlView then
        self._giftlistlView:SetScrollRectState(false)
    end
    self:RemoveEventListeners()
end

function WelfareOnlineView:OnRefreshLevelUpRewardList()
    self:RefreshView()
end

function WelfareOnlineView:RefreshView()

    local ids = OnlineRewardDataHelper:GetAllId()
    local levelReward = WelfareManager:GetOnlineRewardInfo()
    local worldLevel = GameWorld.Player().world_level
    local alreadyId = {}
    local noGetId = {}
    --LoggerHelper.Error("levelReward"..PrintTable:TableToStr(levelReward))
    for i,id in ipairs(ids) do
        local levelTab = OnlineRewardDataHelper:GetWorldLevel(id)
        
        -- -- for i,v in ipairs(table_name) do
        -- --     print(i,v)
        -- -- end
        if worldLevel >= levelTab[1] and worldLevel <= levelTab[2] then
            if levelReward[id] == 1 then
                table.insert(alreadyId,id)
             else
                table.insert(noGetId,id)
             end
        end
    end

    local ordIds = {}
    for i,id in ipairs(noGetId) do
        table.insert(ordIds,id)
    end

    for i,id in ipairs(alreadyId) do
        table.insert(ordIds,id)
    end
    

    self._giftList:SetDataList(ordIds)
    --self._giftList:SetDataList(ids)
end

function WelfareOnlineView:OnInfoChanged(newValue, oldValue)

end


