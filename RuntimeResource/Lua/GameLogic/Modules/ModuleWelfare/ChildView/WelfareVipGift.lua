-- WelfareVipGift.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareVipGift = Class.WelfareVipGift(ClassTypes.BaseLuaUIComponent)

-- 指定实现的接口类，对应C#中实现接口的类
WelfareVipGift.interface = GameConfig.ComponentsConfig.Component
WelfareVipGift.classPath = "Modules.ModuleWelfare.ChildView.WelfareVipGift"

require "Modules.ModuleWelfare.ChildComponent.WelfareVipGiftItem"
local WelfareVipGiftItem = ClassTypes.WelfareVipGiftItem
require "UIComponent.Base.UIScrollView"
local UIScrollView = ClassTypes.UIScrollView

local UIList = ClassTypes.UIList
local UIComponentUtil = GameUtil.UIComponentUtil
local VIPLevelGiftManager = PlayerManager.VIPLevelGiftManager


--override
function WelfareVipGift:Awake()
    self:InitViews()
    self._refreshVipGiftList = function () self:RefreshView() end
end

function WelfareVipGift:InitViews()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
   
	self._vipgiftList = list
	self._vipgiftlistlView = scrollView 
    self._vipgiftlistlView:SetHorizontalMove(false)
    self._vipgiftlistlView:SetVerticalMove(true)
    self._vipgiftList:SetItemType(WelfareVipGiftItem)
    self._vipgiftList:SetPadding(0, 0, 0, 0)
    self._vipgiftList:SetGap(10, 0)
    self._vipgiftList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "Container_List/ScrollViewList")
    self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
end

function WelfareVipGift:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.OnRefreshVipGiftView, self._refreshVipGiftList)
end

function WelfareVipGift:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.OnRefreshVipGiftView, self._refreshVipGiftList)
end

--override
function WelfareVipGift:OnDestroy() 
end

function WelfareVipGift:RefreshView()
    self._listData = VIPLevelGiftManager:PackVipGiftLocalDatas()
    self._vipgiftList:SetDataList(self._listData)
end

-- 界面打开
function WelfareVipGift:ShowView()
    if self._vipgiftlistlView then
        self._vipgiftlistlView:SetScrollRectState(true)
    end
    self._scrollViewAttri:ResetContentPosition()
    self:AddEventListeners()
    VIPLevelGiftManager:GetVipLevelHistory()
    self:RefreshView()
end

-- 界面关闭
function WelfareVipGift:CloseView()
    if self._vipgiftlistlView then
        self._vipgiftlistlView:SetScrollRectState(false)
    end
    self:RemoveEventListeners()
end


function WelfareVipGift:OnRefreshLevelUpRewardList()
    if self._listData ~= nil and #self._listData > 0 then
        for index=1, #self._listData do
            local item = self._vipgiftList:GetItem(index-1)
            if item ~= nil then
                item:SetGiftStatus()
            end
        end
    end
end


