-- WelfareSevenDayView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareSevenDayView = Class.WelfareSevenDayView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareSevenDayView.interface = GameConfig.ComponentsConfig.Component
WelfareSevenDayView.classPath = "Modules.ModuleWelfare.ChildView.WelfareSevenDayView"


local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareManager = PlayerManager.WelfareManager
local GUIManager = GameManager.GUIManager
require "Modules.ModuleWelfare.ChildComponent.WelfareSevenDayItem"
local WelfareSevenDayItem = ClassTypes.WelfareSevenDayItem
local UIList = ClassTypes.UIList
local ItemDataHelper = GameDataHelper.ItemDataHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
-- local CharacterDataHelper = GameDataHelper.CharacterDataHelper
local WelfareDataHelper = GameDataHelper.WelfareDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil


--override
function WelfareSevenDayView:Awake()
    self._csBH = self:GetComponent('LuaUIComponent')
    self:InitViews() 
end

--override
function WelfareSevenDayView:OnDestroy() 
    self:RemoveEventListeners()
end

function WelfareSevenDayView:OnEnable()
    self:AddEventListeners()

    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW,'Modules.ModuleWelfare.ChildView.WelfareSevenDayView')
end

function WelfareSevenDayView:OnDisable()
    self:RemoveEventListeners()
end


function WelfareSevenDayView:AddEventListeners()
    self._onWelfareSevenDayRward = function(newValue, oldValue) self:OnInfoChanged(newValue, oldValue) end
    EventDispatcher:AddEventListener(GameEvents.WELFARE_7DAY_REWARD_TIME, self._onWelfareSevenDayRward)
end

function WelfareSevenDayView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.WELFARE_7DAY_REWARD_TIME, self._onWelfareSevenDayRward)
end


function WelfareSevenDayView:InitViews()    
	self._containerSoulManBig = self:FindChildGO("Container_Left/Container_SoulManBig")
	self._containerSoulMan = self:FindChildGO("Container_Left/Container_SoulMan")
	self._containerItem = self:FindChildGO("Container_Left/Container_Item")
	self._containerSoulManBig:SetActive(false)
	self._containerSoulMan:SetActive(false)
	self._containerItem:SetActive(false)

	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Right/ScrollViewList")
    list:SetItemType(WelfareSevenDayItem)
    list:SetPadding(0, 0, 0, 0)
    list:SetGap(6, 0) -- topToDownGap, leftToRightGap
    list:SetDirection(UIList.DirectionTopToDown, 1, -1) -- 横向，纵向增长量 -1为无限增长  DirectionLeftToRight  DirectionTopToDown
    scrollView:SetVerticalMove(true) --垂直
    scrollView:SetHorizontalMove(false) --水平
    self._listLeft = list
    self._scrollViewLeft = scrollView
end


function WelfareSevenDayView:RefreshView()
    self:RefreshList()
    self:RefreshPreview()
end

function WelfareSevenDayView:OnInfoChanged(newValue, oldValue)
    self:RefreshList()
end


function WelfareSevenDayView:RefreshList()
    self._list = WelfareDataHelper:GetSevenDayList()
    -- LoggerHelper.Log({"===WelfareSevenDayView self._list = ", self._list})
    self._listLeft:SetDataList(self._list)
end

function WelfareSevenDayView:RefreshPreview()
    local nextDay, data7 = WelfareDataHelper:GetSevenDayPreview()
    -- LoggerHelper.Log({"=== nextDay, data7= ", nextDay, data7})
    if nextDay then
        self._containerItem:SetActive(true)
        self:RefreshContainerItem(self._containerItem, nextDay)
    else
        self._containerItem:SetActive(false)
    end

    if data7 then -- 第7天
        if nextDay then
            self._containerSoulManBig:SetActive(false)
            self._containerSoulMan:SetActive(true)
            self:RefreshContainerSoulMan(self._containerSoulMan, data7)
        else
            self._containerSoulManBig:SetActive(true)
            self._containerSoulMan:SetActive(false)
            self:RefreshContainerSoulMan(self._containerSoulManBig, data7, true)
        end
    end
end


function WelfareSevenDayView:RefreshContainerItem(go, info)
    local data = info.data
    UIComponentUtil.GetChildComponent(go, "Text_Tips", "TextWrapper").text = info.str
    GameWorld.AddIcon(UIComponentUtil.FindChild(go, "Container_Item"), ItemDataHelper.GetItemIcon(data.reward_preview[2]))
    UIComponentUtil.GetChildComponent(go, "Text_Name", "TextWrapper").text = LanguageDataHelper.CreateContent(data.reward_preview[3])
end


function WelfareSevenDayView:RefreshContainerSoulMan(go, info, needText)
    UIComponentUtil.GetChildComponent(go, "Text_Tips", "TextWrapper").text = info.str
    local data = info.data
    local bottomTips = UIComponentUtil.GetChildComponent(go, "Text_Tips2", "TextWrapper")
    if needText then
        bottomTips.text = LanguageDataHelper.CreateContent(data.reward_preview[3])
    else
        bottomTips.text = LanguageDataHelper.CreateContent(60008, {["0"] = data.id - WelfareDataHelper:GetSevenDayToday()})
    end

    local roleId = data.reward_preview[2]
    GameWorld.AddIcon(UIComponentUtil.FindChild(go, "Container_Charater"), RoleDataHelper.GetBigIcon(roleId))
    -- GameWorld.AddIcon(UIComponentUtil.FindChild(go, "Container_FiveElement"), RoleConfig.AttributeIconMap[CharacterDataHelper.GetFiveElementsType(roleId)])
    -- UIComponentUtil.GetChildComponent(go, "Image_Name/Text", "TextWrapper").text = LanguageDataHelper.CreateContent(CharacterDataHelper.GetCharacterName(roleId))
    -- GameWorld.AddIcon(UIComponentUtil.FindChild(go, "Container_FiveElement"), RoleConfig.AttributeIconMap[CharacterDataHelper.GetFiveElementsType(roleId)])
end
