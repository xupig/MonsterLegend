-- WelfareExchangeView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareExchangeView = Class.WelfareExchangeView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareExchangeView.interface = GameConfig.ComponentsConfig.Component
WelfareExchangeView.classPath = "Modules.ModuleWelfare.ChildView.WelfareExchangeView"

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local WelfareManager = PlayerManager.WelfareManager
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local XImageFlowLight = GameMain.XImageFlowLight
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
--override
function WelfareExchangeView:Awake()
    -- self._csBH = self:GetComponent('LuaUIComponent')
    -- self:InitViews() 
    self._goButtonFlowLight = self:AddChildComponent("Container/Button_Get_Reward", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Container/Button_Get_Reward")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self._goButtonFlowLight.enabled = true

    self._inputText = self:GetChildComponent("Container/InputFieldMesh", 'InputFieldMeshWrapper')
end


--override
function WelfareExchangeView:OnGetRewardClick() 
        local cardKey = self._inputText.text
        local channelId = GameWorld.GetChannelId()
        local template = GameLoader.SystemConfig.GetValueInCfg('giftCodeUrl') .. 'server=%s&card=%s&account=%s&dbid=%s&channel=%s&md5=%s'
        local serverId = tostring(LocalSetting.settingData.SelectedServerID)
        local card = cardKey
        local roleDbid = tostring(GameWorld.Player().dbid)
        local accountId = tostring(GameWorld.AccountName) -- LocalSetting.settingData.UserAccount)
        --local channelId = channelId
        -- local sign = GameWorld.CreateMD5("QE)(^&LLP18_+OIkN" .. serverId .. accountId .. roleDbid .. channelId .. card)
        local sign = GameWorld.StrToMd5String(serverId .. accountId .. roleDbid .. channelId .. card .. "QE)(^&LLP18_+OIkN")
    -- LoggerHelper.Error('============rewardSign: ' .. sign)
        local url = string.format(template, serverId, card, accountId, roleDbid, channelId, sign)
        local onDone = function(result)self:OnHttpDone(result) end
        local onFail = function(result)self:OnHttpDone(result) end
        --GameWorld.LoggerHelper.Log("ActivationCodeManager:GetHttpUrl:" .. url)
        GameWorld.GetHttpUrl(url, onDone, onFail)
end

function WelfareExchangeView:OnHttpDone(result)
    --LoggerHelper.Log('result:' .. result)
    local tipsList = GlobalParamsHelper.GetParamValue(911)
    --LoggerHelper.Error("tipsList"..PrintTable:TableToStr(tipsList))
    for k,v in pairs(tipsList) do
        if tonumber(result) == k then
            GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContent(v))
        end
    end

end

--override
function WelfareExchangeView:OnDestroy() 
    --self:RemoveEventListeners()
end

function WelfareExchangeView:OnEnable()
    -- self:AddEventListeners()
    -- EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW,'Modules.ModuleWelfare.ChildView.WelfareExchangeView')
end

function WelfareExchangeView:OnDisable()
    --self:RemoveEventListeners()
end


function WelfareExchangeView:AddEventListeners()
    -- self._onWelfareLevelUpRward = function(newValue, oldValue) self:OnInfoChanged(newValue, oldValue) end
    -- EventDispatcher:AddEventListener(GameEvents.WELFARE_LEVEL_UP_REWARD, self._onWelfareLevelUpRward)
end

function WelfareExchangeView:RemoveEventListeners()
    --EventDispatcher:RemoveEventListener(GameEvents.WELFARE_LEVEL_UP_REWARD, self._onWelfareLevelUpRward)
end


function WelfareExchangeView:InitViews()    
	-- local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    -- list:SetItemType(WelfareLevelUpItem)
    -- list:SetPadding(0, 0, 0, 0)
    -- list:SetGap(6, 0) -- topToDownGap, leftToRightGap
    -- list:SetDirection(UIList.DirectionTopToDown, 1, -1) -- 横向，纵向增长量 -1为无限增长  DirectionLeftToRight  DirectionTopToDown
    -- self._listLeft = list
    -- self._scrollViewLeft = scrollView
end


function WelfareExchangeView:ShowView(data)
    
end


function WelfareExchangeView:CloseView()
    
end

function WelfareExchangeView:RefreshView()
    --self:RefreshList()
end



function WelfareExchangeView:RefreshList()
    -- self._list = GameDataHelper.WelfareDataHelper:GetLevelUpList()
    -- LoggerHelper.Log({"===WelfareExchangeView self._list = ", self._list})
    -- self._listLeft:SetDataList(self._list)
end