-- WelfareGiftView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareGiftView = Class.WelfareGiftView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareGiftView.interface = GameConfig.ComponentsConfig.Component
WelfareGiftView.classPath = "Modules.ModuleWelfare.ChildView.WelfareGiftView"

require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpItem"
require "UIComponent.Base.UIScrollView"
local LanguageDataHelper = GameDataHelper.LanguageDataHelper

local UIList = ClassTypes.UIList
local WelfareLevelUpDataHelper = GameDataHelper.WelfareLevelUpDataHelper
local RoleDataHelper = GameDataHelper.RoleDataHelper
local UIComponentUtil = GameUtil.UIComponentUtil
local WelfareLevelUpItem = ClassTypes.WelfareLevelUpItem
local WelfareManager = PlayerManager.WelfareManager
local UIScrollView = ClassTypes.UIScrollView
local TimerHeap = GameWorld.TimerHeap

--override
function WelfareGiftView:Awake()
    -- self._csBH = self:GetComponent('LuaUIComponent')
     self:InitViews()
     self._refreshLevelUpRewardList = function () self:OnRefreshLevelUpRewardList() end
end

--override
function WelfareGiftView:OnDestroy() 
    --self:RemoveEventListeners()
end

function WelfareGiftView:InitViews()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_List/ScrollViewList")
   
	self._giftList = list
	self._giftlistlView = scrollView 
    self._giftlistlView:SetHorizontalMove(false)
    self._giftlistlView:SetVerticalMove(true)
    self._giftList:SetItemType(WelfareLevelUpItem)
    self._giftList:SetPadding(0, 0, 0, 0)
    self._giftList:SetGap(10, 0)
    self._giftList:SetDirection(UIList.DirectionTopToDown, 1, -1)

    self._scroll = UIComponentUtil.FindChild(self.gameObject, "Container_List/ScrollViewList")
    self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
end


function WelfareGiftView:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.UpdateLevelUpRewardList, self._refreshLevelUpRewardList)
end

function WelfareGiftView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.UpdateLevelUpRewardList, self._refreshLevelUpRewardList)
end


function WelfareGiftView:ShowView()
    if self._giftlistlView then
        self._giftlistlView:SetScrollRectState(true)
    end
    self._scrollViewAttri:ResetContentPosition()
    WelfareManager:SendGetReceivedInfoReq()
    local ids = WelfareLevelUpDataHelper:GetAllId()

    local levelReward = WelfareManager:GetLevelUpRewardInfo()
    local alreadyId = {}
    local noGetId = {}
    for i,id in ipairs(ids) do
        if levelReward[id] == 1 then
           table.insert(alreadyId,id)
        else
           table.insert(noGetId,id)
        end
    end

    local ordIds = {}
    for i,id in ipairs(noGetId) do
        table.insert(ordIds,id)
    end

    for i,id in ipairs(alreadyId) do
        table.insert(ordIds,id)
    end
    

    self._giftList:SetDataList(ordIds)
    self:AddEventListeners()

    self._timer = TimerHeap:AddSecTimer(0.8, 0, 0, function()self:BtnGuide() end)

end

function WelfareGiftView:BtnGuide()
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleWelfare.ChildView.WelfareGiftView")
end

function WelfareGiftView:CloseView()
    if self._giftlistlView then
        self._giftlistlView:SetScrollRectState(false)
    end
    self:RemoveEventListeners()
end

function WelfareGiftView:OnRefreshLevelUpRewardList()
    self:RefreshView()
end

function WelfareGiftView:RefreshView()
    local ids = WelfareLevelUpDataHelper:GetAllId()
    local levelReward = WelfareManager:GetLevelUpRewardInfo()
    local alreadyId = {}
    local noGetId = {}
    for i,id in ipairs(ids) do
        if levelReward[id] == 1 then
           table.insert(alreadyId,id)
        else
           table.insert(noGetId,id)
        end
    end

    local ordIds = {}
    for i,id in ipairs(noGetId) do
        table.insert(ordIds,id)
    end

    for i,id in ipairs(alreadyId) do
        table.insert(ordIds,id)
    end
    

    self._giftList:SetDataList(ordIds)
    --self._giftList:SetDataList(ids)
end

function WelfareGiftView:OnInfoChanged(newValue, oldValue)

end


