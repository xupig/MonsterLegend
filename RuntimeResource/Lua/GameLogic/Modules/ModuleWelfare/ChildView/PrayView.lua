require "Modules.ModuleWelfare.ChildComponent.PraySigleItem"
local PrayView = Class.PrayView(ClassTypes.BaseComponent)
PrayView.interface = GameConfig.ComponentsConfig.Component
PrayView.classPath = "Modules.ModuleWelfare.ChildView.PrayView"
local PrayData = PlayerManager.PlayerDataManager.prayData
local PraySigleItem = ClassTypes.PraySigleItem
local PanelsConfig = GameConfig.PanelsConfig
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local GUIManager = GameManager.GUIManager
local PrayManager = PlayerManager.PrayManager
--override
function PrayView:Awake()
    self._GoldView = self:AddChildLuaUIComponent('Container_Gold',PraySigleItem)
    self._ExpView = self:AddChildLuaUIComponent('Container_Exp',PraySigleItem)
    self:InitView()
end

function PrayView:InitView()
    self._update = function() self:Update() end
end


function PrayView:ShowView(data)     
    PrayManager:InitHandleData()   
    self:AddEventListeners()    
    self:Update()
end

function PrayView:Update()
    local d1 = {}
    d1[1] = 1
    d1[2] = PrayData:GetGold()
   self._GoldView:Update(d1)
   self._GoldView:Open()
   local d2 = {}
    d2[1] = 2
    d2[2] = PrayData:GetExp()
   self._ExpView:Update(d2)
   self._ExpView:Open()
end
--override
function PrayView:OnDestroy()
	self._csBH = nil
    self:RemoveEventListeners()
end

function PrayView:CloseView()
    self:RemoveEventListeners()
    self._GoldView:Close()
    self._ExpView:Close()
end

function PrayView:AddEventListeners()
    PrayManager:AddLevelListener()
    EventDispatcher:AddEventListener(GameEvents.PrayUpdate,self._update)
end

function PrayView:RemoveEventListeners()
    PrayManager:RemoveLevelListener()
    EventDispatcher:RemoveEventListener(GameEvents.PrayUpdate,self._update)
end 

function PrayView:WinBGType()
    return PanelWinBGType.NormalBG
end