-- WelfareAnnounceView.lua
-- 普通View继承自ClassTypes.BaseLuaUIComponent, 这里与Panel作区分
local WelfareAnnounceView = Class.WelfareAnnounceView(ClassTypes.BaseLuaUIComponent)


-- 指定实现的接口类，对应C#中实现接口的类
WelfareAnnounceView.interface = GameConfig.ComponentsConfig.Component
WelfareAnnounceView.classPath = "Modules.ModuleWelfare.ChildView.WelfareAnnounceView"

require "Modules.ModuleWelfare.ChildComponent.WelfareLevelUpRewardItem"
require "UIComponent.Base.UIScrollView"
local WelfareLevelUpRewardItem = ClassTypes.WelfareLevelUpRewardItem
local UIComponentUtil = GameUtil.UIComponentUtil
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local WelfareManager = PlayerManager.WelfareManager
local DateTimeUtil = GameUtil.DateTimeUtil
local UIScrollView = ClassTypes.UIScrollView
local TimerHeap = GameWorld.TimerHeap
local WelfareNoticeRewardDataHelper = GameDataHelper.WelfareNoticeRewardDataHelper
local XImageFlowLight = GameMain.XImageFlowLight
--override
function WelfareAnnounceView:Awake()
    -- self._csBH = self:GetComponent('LuaUIComponent')
    self._goButtonFlowLight = self:AddChildComponent("Button_Get", XImageFlowLight)
    self._getRewardButton = self:FindChildGO("Button_Get")
    self._scrollMask = self:FindChildGO("Scroll_Mask")
    self._csBH:AddClick(self._getRewardButton, function()self:OnGetRewardClick() end)
    self._btnAnnounceRewardGetWrapper = self:GetChildComponent("Button_Get", "ButtonWrapper")
    self:InitView()
    --self._btnAnnounceRewardGetWrapper.interactable = false
    self:SetButtonActive(false)
    self._refreshAnnounceFun = function()self:OnRefreshAnnnounceReward() end
end

--override
function WelfareAnnounceView:OnDestroy()

end

--override
function WelfareAnnounceView:SetButtonActive(flag)
    self._btnAnnounceRewardGetWrapper.interactable = flag
    self._goButtonFlowLight.enabled = flag
end


function WelfareAnnounceView:ShowView(data)
    self:LoadNoticeFromWeb()
end

function WelfareAnnounceView:CloseView()
    self:RemoveEventListeners()
end

function WelfareAnnounceView:OnGetRewardClick()
    if self._isMoveBottom then
        WelfareManager:SendNoticeRewardReq()
    else
        GameManager.GUIManager.ShowText(1, LanguageDataHelper.CreateContentWithArgs(76625))
    end

end

function WelfareAnnounceView:AddEventListeners()

    EventDispatcher:AddEventListener(GameEvents.UpdateAnnnounceReward, self._refreshAnnounceFun)
end

function WelfareAnnounceView:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.UpdateAnnnounceReward, self._refreshAnnounceFun)
end

function WelfareAnnounceView:SetContent(isSuccessWeb,content)
    local ids = WelfareNoticeRewardDataHelper:GetAllId()
    self._canAnnounceId = {}
    for i, id in ipairs(ids) do
        if WelfareNoticeRewardDataHelper:GetUse(id) == 1 then
            self._canAnnounceId = id
        end
    end


    local rewardData = WelfareNoticeRewardDataHelper:GetReward(self._canAnnounceId)
    local rewardData = WelfareNoticeRewardDataHelper:GetReward(self._canAnnounceId)
    --LoggerHelper.Log("rewardData" .. PrintTable:TableToStr(rewardData))
    
    local _iconList = {}
    for i, v in ipairs(rewardData) do
        local iconId = v[1]
        local num = v[2]
        local isBind = 0
        local _iconItem = {_id = iconId, _num = num, _isBind = isBind}
        table.insert(_iconList, _iconItem)
    end
    
    self._list:SetDataList(_iconList)
    --self._txtDesc.text = WelfareNoticeRewardDataHelper:GetNotice(self._canAnnounceId).."\n\n"..LanguageDataHelper.CreateContent(76615)
    if isSuccessWeb then
        self._txtDesc.text = content
    else
        self._txtDesc.text = WelfareNoticeRewardDataHelper:GetNotice(self._canAnnounceId)
    end

    local announceReward = WelfareManager:GetNoticeRewardInfo()
    if self._canAnnounceId == announceReward then
        self:SetButtonActive(false)
        --self._btnAnnounceRewardGetWrapper.interactable = false
    else
        self:SetButtonActive(true)
        --self._btnAnnounceRewardGetWrapper.interactable = true
    end
    self._txtBottomGo:SetActive(false)
    self._scrollMask:SetActive(true)
    self:AddEventListeners()
    self._scrollViewAttri:ResetContentPosition()
    self._isMoveBottom = false
    self._timer = TimerHeap:AddSecTimer(0.5, 0, 0, function()self:ScrollViewCanClick() end)

end

function WelfareAnnounceView:LoadNoticeFromWeb()
    local template = GameLoader.SystemConfig.GetValueInCfg('UpdateNoticeUrl')
    if template == nil or template == '' then
        self:SetContent(false)
        return
    end
    local onDone = function(result)self:OnUpdateNoticeHttpDone(result) end
    local onFail = function(result)self:OnUpdateNoticeHttpFail(result) end
    GameWorld.DownloadString(template, onDone, onFail)
end



function WelfareAnnounceView:OnUpdateNoticeHttpDone(content)
    if type(content) ~= "string" then
        self:SetContent(false,content)
        return
    end
    
    self:SetContent(true,content)
    -- local noticeXmlData = XMLParser:ParseXmlText(content)
    -- self:NoticeFromXml(noticeXmlData)
    -- local md5 = GameWorld.CreateMD5(content)
    -- if LocalSetting.settingData.NoticeMd5 ~= md5 then
    --     LocalSetting.settingData.NoticeMd5 = md5
    --     LocalSetting:Save()
    --     EventDispatcher:TriggerEvent(GameEvents.OnShowNoticeView)
    -- end
end

function WelfareAnnounceView:OnUpdateNoticeHttpFail(content)
    if type(content) ~= "string" then
        self:SetContent(false,content)
        return
    end
    self:SetContent(false,content)

    -- local noticeXmlData = XMLParser:ParseXmlText(content)
    -- self:NoticeFromXml(noticeXmlData)
    -- local md5 = GameWorld.CreateMD5(content)
    -- if LocalSetting.settingData.NoticeMd5 ~= md5 then
    --     LocalSetting.settingData.NoticeMd5 = md5
    --     LocalSetting:Save()
    --     EventDispatcher:TriggerEvent(GameEvents.OnShowNoticeView)
    -- end
end



function WelfareAnnounceView:InitView()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "ScrollViewList")
    self._list = list
    self._listlView = scrollView
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    scrollView:SetScrollRectEnable(false)
    self._list:SetItemType(WelfareLevelUpRewardItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 0)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)
    
    self._scroll = UIComponentUtil.FindChild(self.gameObject, "ScrollView")
    self._scrollViewAttri = UIComponentUtil.AddLuaUIComponent(self._scroll, UIScrollView)
    self._scrollContent = self:FindChildGO("ScrollView/Mask/Content")
    self._rectTransform = self._scrollContent:GetComponent(typeof(UnityEngine.RectTransform))

    self._rectTransformScroll = self._scroll:GetComponent(typeof(UnityEngine.RectTransform))
    --self._scrollMask = self:FindChildGO("ScrollView/Mask")
    --self._rectTransformMask = self._scrollMask:GetComponent(typeof(UnityEngine.RectTransform))


    local rootPath = "ScrollView/Mask/Content/"
    self._txtDesc = self:GetChildComponent(rootPath .. "Text", 'TextMeshWrapper')
    self._txtDescRectTransform = self._txtDesc:GetComponent(typeof(UnityEngine.RectTransform))
    self._txtBottomGo = self:FindChildGO("TextBottomTip")
    self._txtBottomTip = self:GetChildComponent("TextBottomTip", 'TextMeshWrapper')
    

    --self._nextCB = function() self:OnRequestNext() end
    --self._scrollViewAttri:SetOnRequestNextCB(function()self:AnnounceScrollEnd() end)
    self._scrollViewAttri:SetCheckEnded(true)
    self._isMoveBottom = false
    self._scrollViewAttri:SetOnMoveEndCB(function()self:AnnounceScrollEnd() end)
--self._scrollViewAttri:SetOnMoveEndCB(function()self:AnnounceScrollEnd() end)
end

-- 滚动到最后了
function WelfareAnnounceView:AnnounceScrollEnd()
    
    self._txtBottomGo:SetActive(true)
    self._isMoveBottom = true
    --self._btnAnnounceRewardGetWrapper.interactable = true

end

function WelfareAnnounceView:ScrollViewCanClick()
    if self._txtDesc.renderedHeight > self._rectTransformScroll.sizeDelta.y then
        self._rectTransform.sizeDelta = Vector2(self._txtDesc.renderedWidth, self._txtDesc.renderedHeight)
    else
        self:AnnounceScrollEnd()
    end
    self._scrollMask:SetActive(false)
end


function WelfareAnnounceView:OnRefreshAnnnounceReward()
    local announceReward = WelfareManager:GetNoticeRewardInfo()
    if self._canAnnounceId == announceReward then
        self:SetButtonActive(false)
        --self._btnAnnounceRewardGetWrapper.interactable = false
    end
end
