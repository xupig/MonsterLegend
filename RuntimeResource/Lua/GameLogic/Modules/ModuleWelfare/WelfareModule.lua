--WelfareModule.lua
WelfareModule = {}

local PanelsConfig = GameConfig.PanelsConfig
local GUIManager = GameManager.GUIManager

function WelfareModule.Init()
	GUIManager.AddPanel(PanelsConfig.Welfare,"Panel_Welfare",GUILayer.LayerUIPanel,"Modules.ModuleWelfare.WelfarePanel",true,false, nil, false, PanelsConfig.DESTROY_ON_LEAVE_ANY_SCENE)
end

return WelfareModule
