﻿--WelfarePanel.lua
require "Modules.ModuleWelfare.ChildComponent.WelfareTotalButtonListItem"
require "Modules.ModuleWelfare.ChildView.WelfareExchangeView"
require "Modules.ModuleWelfare.ChildView.WelfareAnnounceView"
require "Modules.ModuleWelfare.ChildView.WelfareSignView"
require "Modules.ModuleWelfare.ChildView.WelfareGiftView"
--require "Modules.ModuleWelfare.ChildView.SevenDayLoginView"
require "Modules.ModuleWelfare.ChildView.PrayView"
require "Modules.ModuleWelfare.ChildView.WelfareOnlineView"
require "Modules.ModuleWelfare.ChildView.WelfareVipGift"
local ClassTypes = ClassTypes
local WelfarePanel = Class.WelfarePanel(ClassTypes.BasePanel)
local PanelsConfig = GameConfig.PanelsConfig
local PanelBGType = GameConfig.PanelsConfig.PanelBGType
local GUIManager = GameManager.GUIManager

local UIToggleGroup = ClassTypes.UIToggleGroup
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local PanelWinBGType = GameConfig.PanelsConfig.PanelWinBGType
local WelfareTotalButtonListItem = ClassTypes.WelfareTotalButtonListItem
local WelfarePanelType = GameConfig.EnumType.WelfarePanelType

local PrayView = ClassTypes.PrayView
local WelfareExchangeView = ClassTypes.WelfareExchangeView
local WelfareAnnounceView = ClassTypes.WelfareAnnounceView
local WelfareGiftView = ClassTypes.WelfareGiftView
local WelfareSignView = ClassTypes.WelfareSignView
local WelfareOnlineView = ClassTypes.WelfareOnlineView
local WelfareVipGift = ClassTypes.WelfareVipGift
--local SevenDayLoginView = ClassTypes.SevenDayLoginView
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local FunctionOpenDataHelper = GameDataHelper.FunctionOpenDataHelper
local public_config = GameWorld.public_config
local WelfareData = PlayerManager.PlayerDataManager.welfareData
local SevenDayLoginManager = PlayerManager.SevenDayLoginManager


function WelfarePanel:Awake()
    self._csBH = self:GetComponent('LuaUIPanel')
    -- self._views = {}
    -- self.welfareGoList = {}
    -- self.welfareViewList = {}
    -- self._curShowView = 0
    self:InitView()
    self:InitCom()

end

function WelfarePanel:InitCom()
    -- FUNCTION_ID_WELFARE_SIGN        = 630, --福利-每日签到
    -- FUNCTION_ID_WELFARE_GIFT        = 631, --福利-冲级豪礼
    -- FUNCTION_ID_MOUTH_INVEST        = 632,  --月卡投资
    -- FUNCTION_ID_VIP_INVEST          = 633,  --VIP投资
    -- FUNCTION_ID_LEVEL_INVEST        = 634,  --等级投资
    -- FUNCTION_ID_OPEN_CUMULATE       = 635,  --开服累充
    -- FUNCTION_ID_WELFARE_EXCHANGE    = 636, --福利-礼包兑换
    -- FUNCTION_ID_WELFARE_ANNOUNCE    = 637, --福利-更新公告

    self._welfareType = {}
    self._welfareViewType = {}
    self._welfareType[public_config.FUNCTION_ID_WELFARE_SIGN] = self:FindChildGO("Container_Sign")
    self._welfareViewType[public_config.FUNCTION_ID_WELFARE_SIGN] = self:AddChildLuaUIComponent("Container_Sign", WelfareSignView)

    self._welfareType[public_config.FUNCTION_ID_WELFARE_GIFT] = self:FindChildGO("Container_Gift")
    self._welfareViewType[public_config.FUNCTION_ID_WELFARE_GIFT] = self:AddChildLuaUIComponent("Container_Gift", WelfareGiftView)

    self._welfareType[public_config.FUNCTION_ID_VIPGIFT] = self:FindChildGO("Container_VipGift")
    self._welfareViewType[public_config.FUNCTION_ID_VIPGIFT] = self:AddChildLuaUIComponent("Container_VipGift", WelfareVipGift)

    self._welfareType[public_config.FUNCTION_ID_WELFARE_ONLINE] = self:FindChildGO("Container_Online")
    self._welfareViewType[public_config.FUNCTION_ID_WELFARE_ONLINE] = self:AddChildLuaUIComponent("Container_Online", WelfareOnlineView)

    self._welfareType[public_config.FUNCTION_ID_WELFARE_EXCHANGE] = self:FindChildGO("Container_Exchange")
    self._welfareViewType[public_config.FUNCTION_ID_WELFARE_EXCHANGE] = self:AddChildLuaUIComponent("Container_Exchange", WelfareExchangeView)

    self._welfareType[public_config.FUNCTION_ID_WELFARE_ANNOUNCE] = self:FindChildGO("Container_Announce")
    self._welfareViewType[public_config.FUNCTION_ID_WELFARE_ANNOUNCE] = self:AddChildLuaUIComponent("Container_Announce", WelfareAnnounceView)

    -- self._welfareType[public_config.FUNCTION_ID_SEVEN_LOGIN] = self:FindChildGO("Container_Seven_Login")
    -- self._welfareViewType[public_config.FUNCTION_ID_SEVEN_LOGIN] = self:AddChildLuaUIComponent("Container_Seven_Login", SevenDayLoginView)

    self._welfareType[public_config.FUNCTION_ID_PRAY] = self:FindChildGO("Container_Pray")
    self._welfareViewType[public_config.FUNCTION_ID_PRAY] = self:AddChildLuaUIComponent("Container_Pray", PrayView)

    

end

function WelfarePanel:OnShowSelectTab(data) 
    local index = -1
    if data then  
        for i=1,#self.welfareGoList do
            if self.welfareGoList[i]._funID == tonumber(data.functionId) then
                index = i
                break
            end
        end
    end

    if index == -1 then
        self:OnItemBTClicked(0)
    else
        self:OnItemBTClicked(index-1)
    end
end

function WelfarePanel:GetIndexByFunId(funID,data)
   


    -- for i,v in ipairs(self.welfareGoList) do

    -- end
end


function WelfarePanel:Start()
    --self:OnItemBTClicked(0)
end

function WelfarePanel:UpdateFunOpen()
    self.titleList = {}
    self.welfareGoList = {}
    self.welfareViewList = {}
    --self:AddTitleByOpen(public_config.FUNCTION_ID_SEVEN_LOGIN, SevenDayLoginManager:IsOpen()) --七天登录
    self:AddTitleByFuncId(public_config.FUNCTION_ID_WELFARE_SIGN)--福利-每日签到
    self:AddTitleByFuncId(public_config.FUNCTION_ID_WELFARE_GIFT)--福利-冲级豪礼s
    self:AddTitleByFuncId(public_config.FUNCTION_ID_VIPGIFT)--vip升级礼包
    self:AddTitleByFuncId(public_config.FUNCTION_ID_WELFARE_ONLINE)--福利-冲级豪礼
    --下面两个在最后
    self:AddTitleByFuncId(public_config.FUNCTION_ID_WELFARE_EXCHANGE)--福利-礼包兑换
    self:AddTitleByFuncId(public_config.FUNCTION_ID_WELFARE_ANNOUNCE)--福利-更新公告
    self:AddTitleByFuncId(public_config.FUNCTION_ID_PRAY)--祈福

end

function WelfarePanel:AddTitleByOpen(funId,isOpen)
    self:AddViewByTitle(funId,isOpen)
end

function WelfarePanel:UpdateFunView(funId,isOpen)
        self._welfareType[funId]:SetActive(false)
        if isOpen then
            self:SetViewByFuncId(funId,self._welfareType[funId],self._welfareViewType[funId])
        end
end


function WelfarePanel:AddTitleByFuncId(funId)
    local isOpen = FunctionOpenDataHelper:CheckFunctionOpen(funId)
    self:AddViewByTitle(funId,isOpen)
end

function WelfarePanel:AddViewByTitle(funId,isOpen)
    if isOpen then
        local nameId = FunctionOpenDataHelper:GetFunctionOpenName(funId)
        local funItem = {_title = LanguageDataHelper.CreateContent(nameId),_funId = funId}
        table.insert(self.titleList, funItem)
    end
    self:UpdateFunView(funId,isOpen)
end




function WelfarePanel:SetViewByFuncId(funId,viewGo,viewPage)
    --viewGo:SetActive(false)
    local goItem = {_funID = funId,_ViewGo=viewGo}
    table.insert(self.welfareGoList, goItem)

    local viewItem = {_funID = funId,_View=viewPage}
    table.insert(self.welfareViewList, viewItem)
end

function WelfarePanel:SetTitleData()
    self:UpdateFunOpen()
    for i,v in ipairs(self.welfareGoList) do
        self.welfareGoList[i]._ViewGo:SetActive(false)
    end
    self._welfareList:SetDataList(self.titleList)
end



function WelfarePanel:InitView()
    local scrollview, list = UIList.AddScrollViewList(self.gameObject, "Container_Total/ScrollViewList_Buttons")
    self._welfareList = list
    self._welfareView = scrollview
    self._welfareList:SetItemType(WelfareTotalButtonListItem)
    self._welfareList:SetPadding(5, 0, 0, 0)
    self._welfareList:SetGap(5, 0)
    self._welfareList:SetDirection(UIList.DirectionTopToDown, 1, 100)
    local itemBTClickedCB =
        function(index)
            self:OnItemBTClicked(index)
        end
    
    self._welfareList:SetItemClickedCB(itemBTClickedCB)
    

    
    self._btnClose = self:FindChildGO("Container_Total/Button_Close")
    self._csBH:AddClick(self._btnClose, function()self:OnCloseWelfareClick() end)

end

function WelfarePanel:OnItemBTClicked(index)
    self._welfareList:SetSelectedIndex(index)
    --LoggerHelper.Error("WelfarePanel:OnItemBTClicked"..PrintTable:TableToStr(self.welfareGoList))
    if self.welfareGoList[index+1] then
        self:GuideGift(self.welfareGoList[index+1]._funID)
    end

    self._curIndex = index
    local index = index+1
    local value = 1
    self:ShowView(index, value)
end

function WelfarePanel:GuideGift(funId)
    if funId == public_config.FUNCTION_ID_WELFARE_GIFT then
        --LoggerHelper.Error("guide_welfare_gift_ui_button")
        EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_welfare_gift_ui_button")
    end	
end




--override
function WelfarePanel:OnDestroy()
    self._welfareType = {}
    self._welfareViewType = {}
-- self:RemoveEventListeners()
-- self._csBH = nil
end

--override
function WelfarePanel:OnShow(data)
    --self:AddEventListeners()
    
    self._iconBg = self:FindChildGO("Container_Total/Image")
    GameWorld.AddIcon(self._iconBg,13512)
    self.welfareGoList = {}
    self.welfareViewList = {}
    self._curShowView = 1
    -- self:InitView()
    self:SetTitleData()

    self:OnShowSelectTab(data)


    -- if data then
    --     local index = data.id or 0
    --     local value = data.value
    --     self:ShowView(index, value)
    -- end
    EventDispatcher:TriggerEvent(GameEvents.ON_OPEN_VIEW, "Modules.ModuleWelfare.WelfarePanel")

   
end

function WelfarePanel:SetData(data)
    local index = data.id or 0
    local value = data.value
    self:ShowView(index, value)

end

function WelfarePanel:ShowView(index, data)
    --LoggerHelper.Error("WelfarePanel:ShowView()".. self._curShowView.."AAA"..index)
    if self._curShowView ~= -1 and index ~= self._curShowView then
        --LoggerHelper.Error("WelfarePanelXXX".. self._curShowView.."AAA"..index)
        self:CloseView()
    end


        -- if self.welfareViewList[index]._View ~= nil then
        --     self.welfareViewList[index]._View:CloseView()
        -- end
    
    
    
    self.welfareGoList[index]._ViewGo:SetActive(true)
    if self.welfareViewList[index]._View ~= nil then
        self.welfareViewList[index]._View:ShowView(data)
        self._curShowView = index
    end
end


function WelfarePanel:CloseView()
    --LoggerHelper.Error("WelfarePanel:CloseView()".. self._curShowView)
    if self._curShowView ~= -1 then
        self.welfareViewList[self._curShowView]._View:CloseView()
        self.welfareGoList[self._curShowView]._ViewGo:SetActive(false)
        self._curShowView = -1
    end
end


function WelfarePanel:WinBGType()
    return PanelWinBGType.NoBG
end

function WelfarePanel:OnCloseWelfareClick()
    self:CloseView()
    self._welfareList:RemoveAll()
    GUIManager.ClosePanel(PanelsConfig.Welfare)
    EventDispatcher:TriggerEvent(GameEvents.ON_CLICK_GUIDE_BUTTON, "guide_welfare_close_button")
end



function WelfarePanel:OnHelpButtonClicked()
-- local args = {}
-- args.content = LanguageDataHelper.CreateContent(60012)
-- GUIManager.ShowPanel(PanelsConfig.CommonTemp, {[1]=2, [2]=args})
end
