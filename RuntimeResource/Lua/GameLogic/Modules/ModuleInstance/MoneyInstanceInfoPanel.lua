--MoneyInstanceInfoPanel.lua
local MoneyInstanceInfoPanel = Class.MoneyInstanceInfoPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local XArtNumber = GameMain.XArtNumber
local InstanceConfig = GameConfig.InstanceConfig

function MoneyInstanceInfoPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function MoneyInstanceInfoPanel:InitCallback()
end

function MoneyInstanceInfoPanel:InitComps()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._titleText = self:GetChildComponent("Container_Content/Container_Info/Text_Title","TextMeshWrapper")

    self._fpNumber = self:AddChildComponent('Container_Content/Container_Info/Container_FightPower', XArtNumber)
    -- self._descText = self:GetChildComponent("Container_Info/Text_Desc","TextMeshWrapper")
end

function MoneyInstanceInfoPanel:OnShow(data)
    self:SetText(data)
end

function MoneyInstanceInfoPanel:SetData(data)
    if data == nil then return end
    local type = data.type
    if type == InstanceConfig.MONEYINSTANCE_WAVE_CHANGE then
        self:SetText(data)
    elseif type == InstanceConfig.MONEYINSTANCE_MONEY_CHANGE then
        self:SetMoney(data.money)
    end
end

function MoneyInstanceInfoPanel:OnClose()
end

function MoneyInstanceInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function MoneyInstanceInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function MoneyInstanceInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function MoneyInstanceInfoPanel:SetText(data)    
    local totalNum = #GlobalParamsHelper.GetParamValue(512)
    local nowNum = (data and data.num or 0)
    self._titleText.text = LanguageDataHelper.CreateContent(71011,{["0"] = nowNum .. "/" .. totalNum}) 
end

function MoneyInstanceInfoPanel:SetMoney(money)
    self._fpNumber:SetNumber(money)
end