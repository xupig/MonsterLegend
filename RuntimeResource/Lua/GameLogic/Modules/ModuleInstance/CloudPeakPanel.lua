--CloudPeakPanel.lua
local CloudPeakPanel = Class.CloudPeakPanel(ClassTypes.BasePanel)

local CloudPeakInstanceManager = PlayerManager.CloudPeakInstanceManager
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function CloudPeakPanel:Awake()
	self:InitCallback()
	self:InitComps()
end

function CloudPeakPanel:InitCallback()
end

function CloudPeakPanel:InitComps()
    self:InitRewardList()

	self._enterButton = self:FindChildGO("Container_CommonWindow/Container_Win/Button_Enter")
    self._csBH:AddClick(self._enterButton, function() self:OnEnter() end )

    self._closeButton = self:FindChildGO("Container_CommonWindow/Container_Win/Button_Close")
    self._csBH:AddClick(self._closeButton, function() self:OnClosePanel() end )

    self._bgIconObj = self:FindChildGO("Container_CommonWindow/Container_Win/Container_BgIcon")
    GameWorld.AddIcon(self._bgIconObj,13548)
end
	
function CloudPeakPanel:InitRewardList()
	local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/ScrollViewList")
	self._dropList = list
	self._dropListView = scrollView 
    self._dropListView:SetHorizontalMove(false)
    self._dropListView:SetVerticalMove(false)
    self._dropList:SetItemType(RewardItem)
    self._dropList:SetPadding(0, 0, 0, 0)
    self._dropList:SetGap(0, 0)
    self._dropList:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    local datas = GlobalParamsHelper.GetParamValue(626)
    self._datas = {}
    for i,v in ipairs(datas) do
        table.insert(self._datas, {datas[i],1})
    end
    self._dropList:SetDataList(self._datas)
end

function CloudPeakPanel:OnShow(data)
end

function CloudPeakPanel:SetData(data)
end

function CloudPeakPanel:OnClose()
end

function CloudPeakPanel:OnEnter()
    CloudPeakInstanceManager:EnterInstance()
end

function CloudPeakPanel:OnClosePanel()
    GUIManager.ClosePanel(PanelsConfig.CloudPeak)
end