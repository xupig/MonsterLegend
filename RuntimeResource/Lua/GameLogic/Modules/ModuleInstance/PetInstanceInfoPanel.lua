--PetInstanceInfoPanel.lua
local PetInstanceInfoPanel = Class.PetInstanceInfoPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition

function PetInstanceInfoPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function PetInstanceInfoPanel:InitCallback()
end

function PetInstanceInfoPanel:InitComps()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._titleText = self:GetChildComponent("Container_Content/Container_Info/Text_Title","TextMeshWrapper")
    -- self._descText = self:GetChildComponent("Container_Info/Text_Desc","TextMeshWrapper")
end

function PetInstanceInfoPanel:OnShow(data)
    self:SetText(data)
end

function PetInstanceInfoPanel:SetData(data)
    self:SetText(data)
end

function PetInstanceInfoPanel:OnClose()
end

function PetInstanceInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function PetInstanceInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function PetInstanceInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function PetInstanceInfoPanel:SetText(data)    
    local totalNum = #GlobalParamsHelper.GetParamValue(479)
    local nowNum = (data and data.num or 0)
    self._titleText.text = LanguageDataHelper.CreateContent(71011,{["0"] = nowNum .. "/" .. totalNum}) 
    -- self._descText.text = PetInstanceDataHelper:GetDescribe(data.instanceId)
end