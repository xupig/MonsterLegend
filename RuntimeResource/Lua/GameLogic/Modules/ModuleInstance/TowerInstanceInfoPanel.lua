--TowerInstanceInfoPanel.lua
local TowerInstanceInfoPanel = Class.TowerInstanceInfoPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local TowerDefenseDataHelper = GameDataHelper.TowerDefenseDataHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIList = ClassTypes.UIList
local TowerInstanceConfig = GameConfig.TowerInstanceConfig
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local TowerInstanceManager = PlayerManager.TowerInstanceManager
local MessageBoxType = GameConfig.EnumType.MessageBoxType
local GUIManager = GameManager.GUIManager
local StringStyleUtil = GameUtil.StringStyleUtil
local XArtNumber = GameMain.XArtNumber

require "Modules.ModuleInstance.ChildComponent.TowerListItem"
local TowerListItem = ClassTypes.TowerListItem

require "Modules.ModuleInstance.ChildComponent.TowerBuildListItem"
local TowerBuildListItem = ClassTypes.TowerBuildListItem

require "Modules.ModuleInstance.ChildComponent.TowerRewardListItem"
local TowerRewardListItem = ClassTypes.TowerRewardListItem

function TowerInstanceInfoPanel:Awake()
    self.disableCameraCulling = true
	self:InitCallback()
	self:InitComps()
end

function TowerInstanceInfoPanel:InitCallback()
    self._instanceStart = function() self:OnInstanceStart() end
    self._instanceBeginWait = function() self:OnInstanceBeginWait() end
    self._buildTowerSuccess = function() self:OnBuildTowerSuccess() end
    self._handleButtons = function(flag) self:OnHandleButtons(flag) end
end

function TowerInstanceInfoPanel:InitComps()
    self._deviation = (GUIManager.GetCutoutScreenPanelWidth() - GUIManager.PANEL_WIDTH)/2
    -- LoggerHelper.Error("self._deviation ===" .. tostring(self._deviation))

    self._dropList = self:FindChildGO("Container_List")
    self._dropBgRect = self:GetChildComponent("Container_List/Image_Bg", "RectTransform")
    self._dropBgInitSizeDelta = self._dropBgRect.sizeDelta

    self._fpNumber = self:AddChildComponent('Container_Buttons/Button_Show_Reward/Container_Num', XArtNumber)
    self._buttonContainer = self:FindChildGO("Container_Buttons")

    self:FindChildGO("Container_Buttons").transform.localPosition = self:FindChildGO("Container_Buttons").transform.localPosition + Vector3.New(self._deviation*2, 0, 0)
    self:FindChildGO("Container_List").transform.localPosition = self:FindChildGO("Container_List").transform.localPosition + Vector3.New(self._deviation*2, 0, 0)
    self:FindChildGO("Container_Tower_List").transform.localPosition = self:FindChildGO("Container_Tower_List").transform.localPosition + Vector3.New(self._deviation, 0, 0)

    self._towerTypeNum = #TowerDefenseDataHelper:GetAllId()
    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._waveNumText = self:GetChildComponent("Container_Content/Container_Info/Text_Wave","TextMeshWrapper")
    self._remainNumText = self:GetChildComponent("Container_Content/Container_Info/Text_Remain_Num","TextMeshWrapper")
    self._getExpNumText = self:GetChildComponent("Container_Content/Container_Info/Text_Get_Exp","TextMeshWrapper")
    self._escapeNumText = self:GetChildComponent("Container_Content/Container_Info/Text_Escape_Num","TextMeshWrapper")

    self._buildListContainer = self:FindChildGO("Container_Tower_List")

    self._startButtton = self:FindChildGO("Container_Buttons/Button_Start")
    self._csBH:AddClick(self._startButtton, function() self:OnStartClick() end)

    self._bossButtton = self:FindChildGO("Container_Buttons/Button_Call_Boss")
    self._csBH:AddClick(self._bossButtton, function() self:OnBossClick() end)

    self._rewardButtton = self:FindChildGO("Container_Buttons/Button_Show_Reward")
    self._csBH:AddClick(self._rewardButtton, function() self:OnRewardClick() end)

    self._rewardShadeButtton = self:FindChildGO("Container_List/Button_Shade")
    self._csBH:AddClick(self._rewardShadeButtton, function() self:OnRewardShadeClick() end)

    self:InitTowerList()
    self:InitBuildToweList()
    self:InitAttrGridLayoutGroup()
    -- self._buildListContainer:SetActive(false)
    self._buildListContainer.transform.localScale = Vector3.zero
end


function TowerInstanceInfoPanel:InitAttrGridLayoutGroup()
    self._rewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_List/Container_Drops")
    self._rewardGridLauoutGroup:SetItemType(TowerRewardListItem)
end

function TowerInstanceInfoPanel:InitBuildToweList()
    self._towerGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Tower_List/Container_Grid")
    self._towerGridLauoutGroup:SetItemType(TowerBuildListItem)
    
    self._towerGridLauoutGroup:SetDataList(TowerDefenseDataHelper:GetAllId())
    
    self._towerGridLauoutGroup:SetItemClickedCB(function(idx)
        self:BuildIconClick(idx)
    end)
end

function TowerInstanceInfoPanel:BuildIconClick(idx)
    -- LoggerHelper.Error("TowerInstanceInfoPanel:BuildIconClick(idx) ==" .. tostring(idx) .. "type ====" .. type(idx))
    local typeId = tonumber(idx) + 1
    -- if TowerInstanceManager:IsCanBuildById(typeId) then
    TowerInstanceManager:BuildTower(self._towerId, typeId)
    -- else
        -- GameManager.GUIManager.ShowText(1,LanguageDataHelper.CreateContent(76316))
    -- end
end

function TowerInstanceInfoPanel:InitTowerList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Info/ScrollViewList")
	self._list = list
	self._listlView = scrollView 
    self._listlView:SetHorizontalMove(false)
    self._listlView:SetVerticalMove(false)
    self._list:SetItemType(TowerListItem)
    self._list:SetPadding(0, 0, 0, 0)
    self._list:SetGap(0, 10)
    self._list:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    self._list:SetDataList(TowerDefenseDataHelper:GetAllId())
end

function TowerInstanceInfoPanel:OnShow(data)
    self._dropList:SetActive(false)
    self:SetData(data)
    self:AddEventListeners()
end

function TowerInstanceInfoPanel:OnClose()
    self:RemoveEventListeners()
end

function TowerInstanceInfoPanel:AddEventListeners()
    EventDispatcher:AddEventListener(GameEvents.ON_TOWER_INSTANCE_START, self._instanceStart)
    EventDispatcher:AddEventListener(GameEvents.ON_TOWER_INSTANCE_BEGIN_WAIT, self._instanceBeginWait)
    EventDispatcher:AddEventListener(GameEvents.TOWER_DEFENSE_BUILD_TOWER_SUCCESS, self._buildTowerSuccess)
    EventDispatcher:AddEventListener(GameEvents.SHOW_BUTTON_BY_MAIN_MENU, self._handleButtons)
end

function TowerInstanceInfoPanel:RemoveEventListeners()
    EventDispatcher:RemoveEventListener(GameEvents.ON_TOWER_INSTANCE_START, self._instanceStart)
    EventDispatcher:RemoveEventListener(GameEvents.ON_TOWER_INSTANCE_BEGIN_WAIT, self._instanceBeginWait)
    EventDispatcher:RemoveEventListener(GameEvents.TOWER_DEFENSE_BUILD_TOWER_SUCCESS, self._buildTowerSuccess)
    EventDispatcher:RemoveEventListener(GameEvents.SHOW_BUTTON_BY_MAIN_MENU, self._handleButtons)
end

function TowerInstanceInfoPanel:SetData(data)
    local type = data.type
    local params = data.data
    if type == TowerInstanceConfig.SHOW_INFO then
        --显示副本信息（波数、怪物数量、获得经验、道具等）
        self:SetText(params)
    elseif type == TowerInstanceConfig.SHOW_BNUILD_TOWERT_LIST then
        --显示建塔列表
        self._buildListContainer.transform.localScale = Vector3.one
        -- self._buildListContainer:SetActive(true)
        self:UpdateBuildTowerNumInfo()
        self._towerId = data.towerId
        GUIManager.ClosePanel(PanelsConfig.Remind)
    elseif type == TowerInstanceConfig.HIDE_BNUILD_TOWERT_LIST then
        --隐藏建塔列表
        self._buildListContainer.transform.localScale = Vector3.zero
        -- self._buildListContainer:SetActive(false)
        GUIManager.ShowPanel(PanelsConfig.Remind)
    elseif type == TowerInstanceConfig.UPDATE_TOWERT_LIST then
        --更新建塔数量信息
        self:UpdateTowerNumInfo()
        self:UpdateBuildTowerNumInfo()
    end
    -- LoggerHelper.Error("TowerInstanceManager:GetStage() -=========" .. tostring(TowerInstanceManager:GetStage()))
    self._startButtton:SetActive(TowerInstanceManager:GetStage() == 3)
end

function TowerInstanceInfoPanel:UpdateBuildTowerNumInfo()
    for i=1,self._towerTypeNum do
        self._towerGridLauoutGroup:GetItem(i-1):UpdateText()
    end
end

function TowerInstanceInfoPanel:UpdateTowerNumInfo()
    for i=1,self._towerTypeNum do
        self._list:GetItem(i-1):UpdateText()
    end
end

function TowerInstanceInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function TowerInstanceInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function TowerInstanceInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function TowerInstanceInfoPanel:SetText(data)    
    local totalNum = data:GetTotalNum()--data[1]--#GlobalParamsHelper.GetParamValue(687)
    local nowNum = data:GetWaveNum()
    self._waveNumText.text = nowNum .. "/" .. totalNum
    self._remainNumText.text = data:GetRemainNum()
    self._getExpNumText.text = StringStyleUtil.GetLongNumberString(data:GetExp())
    self._escapeNumText.text = data:GetEscapeNum()

    self._fpNumber:SetNumber(data:GetItemNum())
end

function TowerInstanceInfoPanel:OnStartClick()
    TowerInstanceManager:StartInstance()
end

function TowerInstanceInfoPanel:OnBossClick()
    local value
    if TowerInstanceManager:GetAutoFlag() then
        local cb = function(flag) self:OnCancle(flag) end
        value = {
            ["confirmCB"]= cb,
            ["text"] = LanguageDataHelper.CreateContent(76317),
            ["title"] = LanguageDataHelper.CreateContent(76313),
            -- ["toogleText"] = LanguageDataHelper.CreateContent(76315),
            -- ["toogleFlag"] = true,--TowerInstanceManager:GetAutoFlag(),
            ["toogleActive"] = false
        }
    else
        local cb = function(flag) self:OnBossConfirm(flag) end
        value = {
            ["confirmCB"]= cb,
            -- ["cancleButton"] = false,
            ["text"] = LanguageDataHelper.CreateContent(76314, {["0"] = GlobalParamsHelper.GetParamValue(700)}),
            ["title"] = LanguageDataHelper.CreateContent(76313),
            ["toogleText"] = LanguageDataHelper.CreateContent(76315),
            ["toogleActive"] = true,
            ["toogleFlag"] = false--TowerInstanceManager:GetAutoFlag()
        }
    end
    local msgData ={["id"] = MessageBoxType.ToggleTips,["value"]=value}
    GUIManager.ShowPanel(PanelsConfig.MessageBox, msgData)
end

function TowerInstanceInfoPanel:OnRewardClick()
    --LoggerHelper.Error("奖励数据====="  .. PrintTable:TableToStr(TowerInstanceManager:GetRewardDatas()))
    local rewardDatas = TowerInstanceManager:GetRewardDatas()
    local num = #rewardDatas
    if num == 0 or num == 1  then
        self._dropBgRect.sizeDelta = self._dropBgInitSizeDelta
    else
        self._dropBgRect.sizeDelta = self._dropBgInitSizeDelta + Vector2.New(0, (num - 1) * 32)
    end
    
    self._dropList:SetActive(true)
    self._rewardGridLauoutGroup:SetDataList(rewardDatas)
end

function TowerInstanceInfoPanel:OnRewardShadeClick()
    self._dropList:SetActive(false)
end

function TowerInstanceInfoPanel:OnInstanceStart()
    self._startButtton:SetActive(false)
end

function TowerInstanceInfoPanel:OnInstanceBeginWait()
    self._startButtton:SetActive(true)
end

function TowerInstanceInfoPanel:OnBuildTowerSuccess()
    self._buildListContainer.transform.localScale = Vector3.zero
    -- self._buildListContainer:SetActive(false)
end

function TowerInstanceInfoPanel:OnHandleButtons(flag)
    self._buttonContainer:SetActive(flag)
end

function TowerInstanceInfoPanel:OnBossConfirm(flag)
    if flag then
        TowerInstanceManager:ChangeAuto()
    else
        TowerInstanceManager:CallBoss()
    end
end

function TowerInstanceInfoPanel:OnCancle()
    TowerInstanceManager:ChangeAuto()
end