--ExpInstancePanel.lua
local ExpInstancePanel = Class.ExpInstancePanel(ClassTypes.BasePanel)

require "Modules.ModuleInstance.ChildComponent.ExpEfficiencyItem"
local ExpEfficiencyItem = ClassTypes.ExpEfficiencyItem

local UIToggleGroup = ClassTypes.UIToggleGroup
local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local ExpInstanceDataHelper = GameDataHelper.ExpInstanceDataHelper
local ItemDataHelper = GameDataHelper.ItemDataHelper
local AddRateDataHelper = GameDataHelper.AddRateDataHelper
local bagData = PlayerManager.PlayerDataManager.bagData
local DateTimeUtil = GameUtil.DateTimeUtil
local public_config = GameWorld.public_config
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local instanceData = PlayerManager.PlayerDataManager.instanceData
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceCommonManager = PlayerManager.InstanceCommonManager
local ExpInstanceManager = PlayerManager.ExpInstanceManager
local StringStyleUtil = GameUtil.StringStyleUtil
local XGameObjectTweenRotation = GameMain.XGameObjectTweenRotation
local GUIManager = GameManager.GUIManager
local INTERVAL = 1000
local guardEquipId = 7961
local guardExpRate = 50
local stop = Vector3.New(0, 0, 360)
local start = Vector3.New(0, 0, 0)

function ExpInstancePanel:Awake()
    self.disableCameraCulling = true
	self._cfgData = ExpInstanceDataHelper:GetExpInstanceCfg(1)
	self:InitCallback()
	self:InitComps()
end

function ExpInstancePanel:InitCallback()
	self._updateFun = function() self:UpdateData() end
	self._updateAddRateCB = function () self:UpdateAddRate() end
	self._updateBagCB = function (bagType) 
		self:UpdateExpEfficiency()
		if bagType == public_config.PKG_TYPE_LOADED_EQUIP then
			self:UpdateGuard()
		end
	end
	self._showButtonCB = function (state) self:ShowButtons(state) end
end

function ExpInstancePanel:InitComps()
	self._containerButtons = self:FindChildGO("Container_Buttons")
	self._btnExp = self:FindChildGO("Container_Buttons/Button_Exp")
	self._btnInspire = self:FindChildGO("Container_Buttons/Button_Inspire")

	self._csBH:AddClick(self._btnExp, function() self:OnExpClick() end )
	self._csBH:AddClick(self._btnInspire, function() self:OnInspireClick() end )

	self._imgExpLight = self:FindChildGO("Container_Buttons/Image_ExpLight")
	self._imgInspireLight = self:FindChildGO("Container_Buttons/Image_InspireLight")
	self._expLightRotate = self._imgExpLight:AddComponent(typeof(XGameObjectTweenRotation))
	self._inspireLightRotate = self._imgInspireLight:AddComponent(typeof(XGameObjectTweenRotation))

	self._containerExpTip = self:FindChildGO("Container_Buttons/Container_ExpTip")
	self._containerInspireTip = self:FindChildGO("Container_Buttons/Container_InspireTip")

	--经验副本信息
	self._btnExpand = self:FindChildGO("Button_Expand")
	self._containerInfo = self:FindChildGO("Container_Info")
	self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
	self._imgLeft = self:FindChildGO("Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Button_Expand/Image_Right")
    self._isOpen = true
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	
	self._txtWave = self:GetChildComponent("Container_Info/Text_Wave","TextMeshWrapper")
	self._txtKillNum = self:GetChildComponent("Container_Info/Text_KillNum","TextMeshWrapper")
	self._txtInspire = self:GetChildComponent("Container_Info/Text_Inspire","TextMeshWrapper")
	self._txtExpEfficiency = self:GetChildComponent("Container_Info/Text_ExpEfficiency","TextMeshWrapper")
	self._txtGuardAdd = self:GetChildComponent("Container_Info/Text_GuardAdd","TextMeshWrapper")
	self._txtExp = self:GetChildComponent("Container_Info/Text_Exp","TextMeshWrapper")

	self._txtWave.text = ""
	self._txtKillNum.text = ""
	self._txtInspire.text = ""
	self._txtExpEfficiency.text = ""
	self._txtGuardAdd.text = ""
	self._txtExp.text = ""

	--鼓舞面板
	self._containerInspire = self:FindChildGO("Container_Inspire")
	self._toggleInspireType = UIToggleGroup.AddToggleGroup(self.gameObject, "Container_Inspire/ToggleGroup_InspireType")
	self._toggleInspireType:SetOnSelectedIndexChangedCB(function(index) self:OnInspireTypeClick(index) end)
	
	self._btnConfirmInspire = self:FindChildGO("Container_Inspire/Button_Confirm")
	self._btnCancelInspire = self:FindChildGO("Container_Inspire/Button_Cancel")
	self._btnCloseInspire = self:FindChildGO("Container_Inspire/Button_Close")

	self._csBH:AddClick(self._btnConfirmInspire, function() self:OnConfirmInspire() end )
	self._csBH:AddClick(self._btnCancelInspire, function() self:OnCloseInspire() end )
	self._csBH:AddClick(self._btnCloseInspire, function() self:OnCloseInspire() end )

	self._txtTimesGold = self:GetChildComponent("Container_Inspire/Text_TimesGold","TextMeshWrapper")
	self._txtTimesDiamond = self:GetChildComponent("Container_Inspire/Text_TimesDiamond","TextMeshWrapper")
	
	self._txtTimesGold.text = self._cfgData.exp_inst_item[1][1]
	self._txtTimesDiamond.text = self._cfgData.exp_inst_item[2][1]
	local diamonIcon = self:FindChildGO("Container_Inspire/Container_DiamondIcon")
	GameWorld.AddIcon(diamonIcon,ItemDataHelper.GetIcon(public_config.MONEY_TYPE_COUPONS))
	local goldIcon = self:FindChildGO("Container_Inspire/Container_GoldIcon")
	GameWorld.AddIcon(goldIcon,ItemDataHelper.GetIcon(public_config.MONEY_TYPE_GOLD))

	--经验效率面板
	self._containerExpEfficiency = self:FindChildGO("Container_ExpEfficiency")
	local allEffectItem = ItemDataHelper.GetAllItemByEffectId(12)
	self._efficiencyItemIds = {}
	self._efficiencyItemRates = {}
	self._efficiencyItems = {}
	for i=1,3 do
		local itemCfg = allEffectItem[i]
		local rateCfgId = itemCfg.effectId[2]
		local effect = AddRateDataHelper:GetEffect(rateCfgId)
		if effect == 1 then
			table.insert(self._efficiencyItemIds, itemCfg.id )
			table.insert(self._efficiencyItemRates,AddRateDataHelper:GetEffectValue(rateCfgId))
		end
	end
	
	for i=1,3 do
		self._efficiencyItems[i] = self:AddChildLuaUIComponent("Container_ExpEfficiency/Container_Items/Container_Item"..i, ExpEfficiencyItem)
		self._efficiencyItems[i]:UpdateData(self._efficiencyItemIds[i],self._efficiencyItemRates[i])
	end

	self._btnCloseEfficiency = self:FindChildGO("Container_ExpEfficiency/Button_Close")
	self._csBH:AddClick(self._btnCloseEfficiency, function() self:OnCloseEfficiency() end )

	self._containerInspire:SetActive(false)
	self._containerExpEfficiency:SetActive(false)

	self._timeLeftInit = false
end

function ExpInstancePanel:OnShow()
	EventDispatcher:AddEventListener(GameEvents.UPDATE_EXP_INSTANCE_INFO, self._updateFun)
	EventDispatcher:AddEventListener(GameEvents.UPDATE_ADD_RATE, self._updateAddRateCB)
	EventDispatcher:AddEventListener(GameEvents.BAG_DATA_UPDATE, self._updateBagCB)
	EventDispatcher:AddEventListener(GameEvents.SHOW_BUTTON_BY_MAIN_MENU, self._showButtonCB)
	self._inspireFull = false
	self._inspireType = 1
	self._toggleInspireType:SetSelectIndex(0)
	self:CheckGuard()
	self:UpdateData()
	self:UpdateAddRate()
	self:ActivateLight()
end



function ExpInstancePanel:OnClose()
	self._timeLeftInit = false
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_EXP_INSTANCE_INFO, self._updateFun)
	EventDispatcher:RemoveEventListener(GameEvents.UPDATE_ADD_RATE, self._updateAddRateCB)
	EventDispatcher:RemoveEventListener(GameEvents.BAG_DATA_UPDATE, self._updateBagCB)
	EventDispatcher:RemoveEventListener(GameEvents.SHOW_BUTTON_BY_MAIN_MENU, self._showButtonCB)
end

function ExpInstancePanel:ActivateLight()
	self._containerExpTip:SetActive(true)
	self._imgExpLight:SetActive(true)
	self._expLightRotate.IsTweening = true
	self._expLightRotate:SetFromToRotation(start, stop, 2, 2,nil)

	self._containerInspireTip:SetActive(true)
	self._imgInspireLight:SetActive(true)
	self._inspireLightRotate.IsTweening = true
	self._inspireLightRotate:SetFromToRotation(start, stop, 2, 2,nil)
end

--检查身上，背包小恶魔
function ExpInstancePanel:CheckGuard()
	--身上有小恶魔
	if self:UpdateGuard() then
		return
	end

	local bagItems = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemInfos()
	local itemData
	local bagDemonState = 0
	for k,v in pairs(bagItems) do
		if v.cfg_id == guardEquipId then
			local timeEnd = v:GetTimeEnd()
			local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
			if timeLeft > 0 then
				bagDemonState = 1
				itemData = v
				break
			else
				bagDemonState = 2
				itemData = v
			end
		end
	end
	if bagDemonState == 1 then
		GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
			GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddEquipData(itemData)
		end)
	elseif bagDemonState == 2 then
		GUIManager.ShowPanel(PanelsConfig.NewEquipTip,nil,function ()
			GUIManager.GetPanel(PanelsConfig.NewEquipTip):AddGuardData(itemData)
		end)
	end
end

function ExpInstancePanel:UpdateGuard()
	local hasDemonEffect = false
	local loadedGuard = bagData:GetPkg(public_config.PKG_TYPE_LOADED_EQUIP):GetItemInfos()[public_config.PKG_LOADED_EQUIP_INDEX_GUARD]
	if loadedGuard and loadedGuard.cfg_id == guardEquipId then
		local timeEnd = loadedGuard:GetTimeEnd()
		local timeLeft = timeEnd - DateTimeUtil.GetServerTime()
		if timeLeft > 0 then
			hasDemonEffect = true
		end
	end
	if hasDemonEffect then
		self._txtGuardAdd.text = guardExpRate.."%"
	else
		self._txtGuardAdd.text = "0%"
	end
	return hasDemonEffect
end

function ExpInstancePanel:UpdateData()
	local info = instanceData.expInstanceInfo
	if info == nil then
		return
	end
	self._txtWave.text = tostring(info[1])
	self._txtKillNum.text = tostring(info[2])
	self._txtExp.text = StringStyleUtil.GetLongNumberString(info[3])

	self._goldInspire = info[5]
	self._diamonInspire = info[6] 
	self._inspireTime = self._goldInspire + self._diamonInspire
	local inspireRate = math.min(self._inspireTime*10,100) 
	self._txtInspire.text = inspireRate.."%"
	
	if self._showInspire then
		self:UpdateInspire()
	end
	-- if self._timeLeftInit == false and info[1] > 0 then
	-- 	local startTime = tostring(info[4])
	-- 	self._timeLeft = self._cfgData.last_time
	-- 	--LoggerHelper.Error("开始游戏")
	-- 	InstanceCommonManager:StartInstanceCountDown(self._timeLeft,startTime)
	-- 	self._timeLeftInit = true
	-- end
end

function ExpInstancePanel:UpdateInspire()
	--金币鼓舞5次自动跳到钻石鼓舞
	if self._goldInspire >= 5 and self._diamonInspire < 5 and self._inspireType == 1 then
		self._toggleInspireType:SetSelectIndex(1)
		self:OnInspireTypeClick(1)
	end
	--钻石鼓舞5次自动跳到金币鼓舞
	if self._diamonInspire >= 5 and self._goldInspire < 5 and self._inspireType == 2 then
		self._toggleInspireType:SetSelectIndex(0)
		self:OnInspireTypeClick(0)
	end
	--鼓舞10次直接关闭
	if self._lastInspire == 9  and self._diamonInspire + self._goldInspire == 10 then
		self:OnCloseInspire()
	end
	self._lastInspire = self._diamonInspire + self._goldInspire
end

function ExpInstancePanel:UpdateAddRate()
	local addRateInfo = GameWorld.Player().add_rate_info
	local expRate = 0
	for i=1,#addRateInfo do
		local cfgId = addRateInfo[i][1]
		local effect = AddRateDataHelper:GetEffect(cfgId)
		if effect == 1 and addRateInfo[i][2] + addRateInfo[i][3] - DateTimeUtil.GetServerTime() > 0 then
			expRate = AddRateDataHelper:GetEffectValue(cfgId)
		end
	end
	self._txtExpEfficiency.text = expRate.."%"
end

function ExpInstancePanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function ExpInstancePanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function ExpInstancePanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

-------------------------------------------鼓舞面板-------------------------------------------
function ExpInstancePanel:OnInspireClick()
	self._inspireLightRotate.IsTweening = false
	self._imgInspireLight:SetActive(false)
	self._containerInspireTip:SetActive(false)
	self._showInspire = true
	self._containerInspire:SetActive(true)
	self:UpdateInspire()
end

function ExpInstancePanel:OnInspireTypeClick(index)
	self._inspireType = index + 1
end

function ExpInstancePanel:OnConfirmInspire()
	if self._inspireType then
		--LoggerHelper.Error("self._inspireType"..self._inspireType)
		ExpInstanceManager:RequestExpInstanceInspire(self._inspireType)
	end
end

function ExpInstancePanel:OnCloseInspire()
	self._showInspire = false
	self._containerInspire:SetActive(false)
end

-------------------------------------------经验效率面板-------------------------------------------
function ExpInstancePanel:OnExpClick()
	self._expLightRotate.IsTweening = false
	self._imgExpLight:SetActive(false)
	self._containerExpTip:SetActive(false)
	self._containerExpEfficiency:SetActive(true)
	self:UpdateExpEfficiency()
end

function ExpInstancePanel:UpdateExpEfficiency()
	for i=1,#self._efficiencyItemIds do
		local bagNum = bagData:GetPkg(public_config.PKG_TYPE_ITEM):GetItemCountByItemId(self._efficiencyItemIds[i])
		self._efficiencyItems[i]:UpdateCount(bagNum)
		if bagNum > 0 then
			self._efficiencyItems[i]:SetActive(true)
		else
			if i == 1 then
				self._efficiencyItems[i]:ShowBuy()
			else
				self._efficiencyItems[i]:SetActive(false)
			end
		end
	end
end

function ExpInstancePanel:OnCloseEfficiency()
	self._containerExpEfficiency:SetActive(false)
end

function ExpInstancePanel:ShowButtons(state)
	self._containerButtons:SetActive(state)
end
