--CloudPeakInfoPanel.lua
local CloudPeakInfoPanel = Class.CloudPeakInfoPanel(ClassTypes.BasePanel)

local LanguageDataHelper = GameDataHelper.LanguageDataHelper
local PetInstanceDataHelper = GameDataHelper.PetInstanceDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local XGameObjectTweenPosition = GameMain.XGameObjectTweenPosition
local UIGridLayoutGroup = ClassTypes.UIGridLayoutGroup
local CloudTopAwardDataHelper = GameDataHelper.CloudTopAwardDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local InstanceConfig = GameConfig.InstanceConfig
local GUIManager = GameManager.GUIManager
local UIList = ClassTypes.UIList
local AionDataHelper = GameDataHelper.AionDataHelper
local GlobalParamsHelper = GameDataHelper.GlobalParamsHelper
local AionTowerManager = PlayerManager.AionTowerManager

require"Modules.ModuleMainUI.ChildComponent.RewardItem"
local RewardItem = ClassTypes.RewardItem

function CloudPeakInfoPanel:Awake()
    self.disableCameraCulling = true
    self._timer = nil
	self:InitCallback()
	self:InitComps()
end

function CloudPeakInfoPanel:OnDestroy()
    self._timer = nil
    self._isOpen = nil
	self._btnExpand = nil
	self._containerInfo = nil
    self._tweenPosition = nil
	self._imgLeft = nil
    self._imgRight = nil
    self._posText = nil
    self._timeText = nil
    self._killText = nil
    self._rewardGridLauoutGroup = nil
end

function CloudPeakInfoPanel:InitCallback()
end

function CloudPeakInfoPanel:InitComps()
    self._deviation = (GUIManager.canvasWidth - GUIManager.PANEL_WIDTH)/2

    self._isOpen = true
	self._btnExpand = self:FindChildGO("Container_Content/Button_Expand")
    self._csBH:AddClick(self._btnExpand, function() self:OnExpand() end )
    
	self._containerInfo = self:FindChildGO("Container_Content/Container_Info")
    self._initPosition = self._containerInfo.transform.localPosition
    self._tweenPosition = self._containerInfo:AddComponent(typeof(XGameObjectTweenPosition))
	self._imgLeft = self:FindChildGO("Container_Content/Button_Expand/Image_Left")
    self._imgRight = self:FindChildGO("Container_Content/Button_Expand/Image_Right")

    self._quitImage = self:FindChildGO("Image_Quit")
    self._quitText = self:GetChildComponent("Image_Quit/Text_Quit","TextMeshWrapper")
    self._quitImage.gameObject.transform.localPosition = self._quitImage.gameObject.transform.localPosition + Vector3.New(self._deviation, 0, 0)

    self._posText = self:GetChildComponent("Container_Content/Container_Info/Text_Pos","TextMeshWrapper")
    self._timeText = self:GetChildComponent("Container_Content/Container_Info/Text_Time","TextMeshWrapper")
    self._killText = self:GetChildComponent("Container_Content/Container_Info/Text_Need_Kill","TextMeshWrapper")
    self:InitRewardsList()
end

function CloudPeakInfoPanel:InitRewardsList()
    local scrollView, list = UIList.AddScrollViewList(self.gameObject, "Container_Content/Container_Info/ScrollViewList")
	self._rewardList = list
	self._rewardListlView = scrollView 
    self._rewardListlView:SetHorizontalMove(true)
    self._rewardListlView:SetVerticalMove(false)
    self._rewardList:SetItemType(RewardItem)
    self._rewardList:SetPadding(0, 0, 0, 0)
    self._rewardList:SetGap(0, 0)
    self._rewardList:SetDirection(UIList.DirectionLeftToRight, -1, 1)

    -- self._rewardGridLauoutGroup = UIGridLayoutGroup.AddGroup(self.gameObject, "Container_Content/Container_Info/Container_Reward")
    -- self._rewardGridLauoutGroup:SetItemType(RewardItem)
end

function CloudPeakInfoPanel:OnShow(datas)
    -- {
        --     [1] = start_time,     --开始时间
        --     [2] = duration_time , --持续时间
        --     [3] = layer,          --当前层数
        --     [4] = need_kill_num,  --需要击杀
        --     [5] = kill_num，      --已经击杀   
    -- }
    local data = datas.data
    self._startTime = data[1]
    self._endTime = self._startTime + data[2]
    self:AddTimer()
    self:SetData(datas)
end

function CloudPeakInfoPanel:AddTimer()
    if self._timer == nil then
        self._timer = TimerHeap:AddSecTimer(0, 1, 0, function()
            local time = DateTimeUtil.GetServerTime()
            if self._startTime > time then
                time = self._startTime
            end

            if self._endTime < time then
                self._timeText.text = "00:00"
                self:DelTimer()
            else
                self._timeText.text = LanguageDataHelper.CreateContent(75302,{
                    ["0"] = DateTimeUtil:ParseTime(self._endTime - time)
                }) 
            end                    
        end)
    end
end

function CloudPeakInfoPanel:DelTimer()
    if self._timer ~= nil then
        TimerHeap:DelTimer(self._timer)
        self._timer = nil
    end
end

function CloudPeakInfoPanel:SetData(datas)
    local type = datas.type
    
    if type == InstanceConfig.CLOUD_PEAK_TYPE_PASS then
        --完成副本
        self:AddQuitTimer()
    else
        self:SetText(datas)
    end
end

function CloudPeakInfoPanel:AddQuitTimer()
    if self._quitTimer == nil then
        self._quitTime = GlobalParamsHelper.GetParamValue(633)
        
        self._quitImage:SetActive(true)
        self._quitTimer = TimerHeap:AddSecTimer(0, 1, 0, function()
            if self._quitTime >= 0 then
                self._quitText.text = LanguageDataHelper.CreateContent(75300, {["0"] = self._quitTime}) 
                self._quitTime = self._quitTime - 1
            else
                self:DelQuitTimer()
            end
        end)
    end
end

function CloudPeakInfoPanel:DelQuitTimer()
    if self._quitTimer ~= nil then
        TimerHeap:DelTimer(self._quitTimer)
        self._quitTimer = nil
    end
end 


function CloudPeakInfoPanel:OnClose()
    self:DelTimer()
    self:DelQuitTimer()
    self._quitTime = GlobalParamsHelper.GetParamValue(633)
    self._quitText.text = ""
    self._quitImage:SetActive(false)
end

function CloudPeakInfoPanel:OnExpand()
    if self._isOpen then
        self:OnClosePanelAnimation()
    else
        self:OnOpenPanelAnimation()
    end
    self._isOpen = not self._isOpen
end

function CloudPeakInfoPanel:OnOpenPanelAnimation()
    self._tweenPosition.IsTweening = false
    -- self._containerInfo.transform.localPosition = Vector3.New(-280, 0, 0)
    self._tweenPosition:SetFromToPos(self._containerInfo.transform.localPosition, self._initPosition, 0.4, 1, nil)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(0, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(true)
    self._imgRight:SetActive(false)
end

function CloudPeakInfoPanel:OnClosePanelAnimation()
    self._tweenPosition.IsTweening = false
    self._tweenPosition:SetFromToPos(self._initPosition, Vector3.New(self._initPosition.x - 280, self._initPosition.y, self._initPosition.z), 0.4, 1, nil)
    -- self._containerInfo.transform.localPosition = Vector3.New(0, 0, 0)
    -- self._tweenPosition:SetToPosOnce(Vector3.New(-280, 0, 0), 0.4, nil)
    self._imgLeft:SetActive(false)
    self._imgRight:SetActive(true)
end

function CloudPeakInfoPanel:SetText(datas)
    local data = datas.data
    local pos = data[3]
    -- LoggerHelper.Error("CloudPeakInfoPanel:SetText(datas)")
    local rewardData = CloudTopAwardDataHelper:GetRewardByPos(pos)
    self:ExReward(rewardData, pos)

    self._killText.text = LanguageDataHelper.CreateContent(75303, {
        ["0"] = data[5],
        ["1"] = data[4]
    }) 
    self._posText.text = LanguageDataHelper.CreateContent(75301, {
        ["0"] = pos
    })
    
    -- self._rewardGridLauoutGroup:SetDataList(rewardData)
    self._rewardList:SetDataList(rewardData)
end

function CloudPeakInfoPanel:ExReward(data, pos)
    if pos >= 7 then
        local towerNum = AionTowerManager:GetNowPos()
        local rewardId
        for i,v in pairs(AionDataHelper:GetMailReward(towerNum)) do
            rewardId = i
            break
        end
        local rewardNum = GlobalParamsHelper.GetParamValue(853)[pos]
        local exRewardData = {}
        exRewardData[1] = rewardId
        exRewardData[2] = rewardNum
        table.insert(data, exRewardData)
    end
end